/**
 * @author Emmanuel Sanchez Castillo
 *
 */
package mx.altec.enlace.admonContrasena.command;

import java.sql.SQLException;

import mx.altec.enlace.admonContrasena.dao.DatosPeticionDAO;
import mx.altec.enlace.admonContrasena.dao.DatosSolicitudDAO;
import mx.altec.enlace.admonContrasena.dao.TransactionHandler;
import mx.altec.enlace.admonContrasena.vo.PeticionVO;
import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.utilerias.EIGlobal;


public class GenerarContrasenaCommand {

	//DAO de peticion y solictud
	private DatosSolicitudDAO solicitudDAO;
	private DatosPeticionDAO peticionDAO;
	TransactionHandler handler;
	public GenerarContrasenaCommand()
	{
        handler = new TransactionHandler();
		solicitudDAO = new DatosSolicitudDAO(handler);
		peticionDAO = new DatosPeticionDAO(handler);
	}

	public ValueObject generarContrasena(String codigoCliente, String contrasenaGen, String nombre, String aPaterno, String aMaterno,
			 String direccion, String email,String codigoArea, String telefonoMovil,String codigoArea2, String telefonoMovil2,
			 String contrato)
	{
		//crear el objeto de respues
		EIGlobal.mensajePorTrace("Inicio de generación de contrasena", EIGlobal.NivelLog.INFO);
		ValueObject result = new ValueObject();
		PeticionVO peticion = new PeticionVO();
		boolean insertOk;

		String folioSolicitud;
		String folioPeticion;

		peticion.setCodigoCliente(codigoCliente);
		peticion.setContrasena(contrasenaGen);
		peticion.setNombre(nombre);
		peticion.setPaterno(aPaterno);
		peticion.setMaterno(aMaterno);
		peticion.setDireccion(direccion);
		peticion.setEmail(email);
		peticion.setAreaCode(codigoArea);
		peticion.setTelefono(telefonoMovil);
		peticion.setAreaCodeAlternativa(codigoArea2);
		peticion.setTelefonoAlternativo(telefonoMovil2);
		peticion.setEstado("G");

		SegAltaPwd altaPwd = new SegAltaPwd();
		altaPwd.setPeticion(peticion);
		altaPwd.setPwdEncriptado(contrasenaGen);
		altaPwd.setContrato(contrato);
		altaPwd.setUsuario(codigoCliente);
		result = altaPwd.altaPwd();

		return result;
	}

	public PeticionVO datosCliente(String codigoCliente) throws Exception{
		peticionDAO = new DatosPeticionDAO();
		PeticionVO peticion = null;
		peticion = peticionDAO.datosUsuario(codigoCliente);

		if (peticion.getEmail()==null){
			peticion.setEmail("");
		}

		if (peticion.getAreaCode()==null){
			peticion.setAreaCode("");
		}

		if (peticion.getTelefono()==null){
			peticion.setTelefono("");
		}

		if (peticion.getAreaCodeAlternativa()==null){
			peticion.setAreaCodeAlternativa("");
		}

		if (peticion.getTelefonoAlternativo()==null){
			peticion.setTelefonoAlternativo("");
		}

		return peticion;
	}

}
