package mx.altec.enlace.admonContrasena.vo;

import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.utilerias.EIGlobal;


public class UtilVO {

	public static final String entrama(ValueObject vo) {
		return "|" + vo.getId() + "|" + vo.getMensaje();
	}

	/**
	 * Metodo que recibe un VO y a partir de este regresa una
	 * trama con los atributos de este VO concatenados
	 * @param vo
	 * @return Cadena entramada	Ej. 1|Mensaje|Usuario|
	 */
	public static final String entramaDatos(ValueObject vo) {
		return vo.getId() + "|" + vo.getMensaje() + "|" + vo.getUsuario()+ "|";
	}

	/**
	 * Metodo que recibe un VO y a partir de este regresa una
	 * trama con los atributos de este VO concatenados
	 * @param vo
	 * @return Cadena entramada	Ej. 1|Mensaje|Usuario|
	 */
	public static final String entramaDatosTodos(ValueObject vo) {
		return vo.getId() + "|" + vo.getMensaje() + "|" + vo.getUsuario() +
		"|" + vo.getFolioSolicitud() + "|" + vo.getEstadoSolicitud() + "|" +
		vo.getDescRechacho() + "|" + vo.getNombreUsuario() + "|" +
		vo.getAPaterno() + "|" + vo.getAMaterno() + "|";
	}

	/** Metodo que recibe una cadena y regresa los valores en un
	 * array
	 * Formato de la cadena a enviar - 	hola|mundo|cruel|jajaja|
	 */
	public static String[] desentramaS(String cadena, char caracter)
	{
		String vartmp="";
	    String regtmp="";
	    int totalCampos=0;
		//int tamanioArreglo=0;
	    String[] camposTabla;

	    regtmp = cadena;
	    for (int i = 0; i < regtmp.length(); i++)
		{
		  if (regtmp.charAt(i) == caracter)
			totalCampos++;
		}
		camposTabla = new String[totalCampos];
		for (int i = 0; i < totalCampos; i++)
		{
			if (regtmp.charAt( 0 ) == caracter)
			{
				vartmp = " ";
				regtmp = regtmp.substring(1, regtmp.length());
			}
			else
			{
				vartmp = regtmp.substring(0, regtmp.indexOf(caracter)).trim();
				if (regtmp.indexOf( caracter ) > 0)
					regtmp=regtmp.substring(regtmp.indexOf(caracter) + 1,regtmp.length());
			}
			camposTabla[i]=vartmp;
		}
		return camposTabla;
	}

	public static String leftPadCeros (String cadena,int width){
		String aux = "";
	    for ( int i = cadena.length() ; i < width ; i++)
	        aux += "0";
	    cadena = aux + cadena;
	    return cadena;
	}
	/**
	 * Definir una descripcion entendible para el usuario del estadod de la su
	 * solicitud de contraseņa
	 * @param estado
	 * @return Descripcion de la solicitud
	 */
	public static String estado(char estado){
		switch(estado){
		case 'X':
			return "SIN SOLICITUD";
		case 'V':
			return "CADUCADA";
		case 'G':
			return "GENERADA";
		case 'A':
			return "AUTORIZADA";
		case 'R':
			return "RECHAZADA";
		default:
			return "EN TRAMITE";
		}
	}

	public static String entramaDatosCliente(PeticionVO peticion){
		return peticion.getEmail() + "|" + peticion.getAreaCode() + "|" +
		peticion.getTelefono() + "|" + peticion.getAreaCodeAlternativa() + "|" +
		peticion.getTelefonoAlternativo() + "|";
	}

	public static String convierteUsr8a7(String usr) {
		String result = null;
		if(usr.length() == 7) result = usr;
		else if(usr.length() == 8) {

			if(usr.charAt(0) == '0')
				result =  usr.substring(1);
			else{
				String usuConv = usr.substring(0, 2);
				int usuConvInt = Integer.parseInt(usuConv);

				switch(usuConvInt) {
					case 10:	result = 'A' + usr.substring(2);
								break;
					case 11:	result = 'B' + usr.substring(2);
								break;
					case 12:	result = 'G' + usr.substring(2);
								break;
					case 13:	result = 'H' + usr.substring(2);
								break;
					case 14:	result = 'I' + usr.substring(2);
								break;
					case 15:	result = 'J' + usr.substring(2);
								break;
					case 16:	result = 'K' + usr.substring(2);
								break;
					case 17:	result = 'L' + usr.substring(2);
								break;
					case 18:	result = 'M' + usr.substring(2);
								break;
					case 19:	result = 'N' + usr.substring(2);
								break;
					case 20:	result = 'O' + usr.substring(2);
								break;
					case 21:	result = 'P' + usr.substring(2);
								break;
					case 22:	result = 'Q' + usr.substring(2);
								break;
					case 23:	result = 'R' + usr.substring(2);
								break;
					case 24:	result = 'T' + usr.substring(2);
								break;
					case 25:	result = 'U' + usr.substring(2);
								break;
					case 26:	result = 'V' + usr.substring(2);
								break;
					case 27:	result = 'W' + usr.substring(2);
								break;
					case 28:	result = 'X' + usr.substring(2);
								break;
					case 29:	result = 'Y' + usr.substring(2);
								break;
					default:	result = null;
								break;
				}
			}
		}
		EIGlobal.mensajePorTrace("USUARIO CONVERTIDO 8->7:'" + result + "'", EIGlobal.NivelLog.INFO);
		return result;
	}
    /*<VC FIN>*/



	public static String convierteUsr7a8(String usr) {
		System.out.println("Usuario a convertir: '" + usr + "'");
		String result = null;
		if(usr.length() == 8) result = usr;
		if(usr.length() == 7) {
			if(usr.charAt(0) >= '0' && usr.charAt(0) <= '9') result =  "0" + usr;
			else {
				switch(usr.charAt(0)) {
					case 'A':	result =  "10" + usr.substring(1);
								break;
					case 'B':	result =  "11" + usr.substring(1);
								break;
					case 'C':
					case 'D':
					case 'E':
					case 'F':	result = null;
								break;
					case 'G':	result = "12" + usr.substring(1);
								break;
					case 'H':	result = "13" + usr.substring(1);
								break;
					case 'I':	result = "14" + usr.substring(1);
								break;
					case 'J':	result = "15" + usr.substring(1);
								break;
					case 'K':	result = "16" + usr.substring(1);
								break;
					case 'L':	result = "17" + usr.substring(1);
								break;
					case 'M':	result = "18" + usr.substring(1);
								break;
					case 'N':	result = "19" + usr.substring(1);
								break;
					case 'O':	result = "20" + usr.substring(1);
								break;
					case 'P':	result = "21" + usr.substring(1);
								break;
					case 'Q':	result = "22" + usr.substring(1);
								break;
					case 'R':	result = "23" + usr.substring(1);
								break;
					case 'S':	result = null;
								break;
					case 'T':	result = "24" + usr.substring(1);
								break;
					case 'U':	result = "25" + usr.substring(1);
								break;
					case 'V':	result = "26" + usr.substring(1);
								break;
					case 'W':	result = "27" + usr.substring(1);
								break;
					case 'X':	result = "28" + usr.substring(1);
								break;
					case 'Y':	result = "29" + usr.substring(1);
								break;
					case 'Z':
					default:	result = null;
								break;
				}
			}
		}
		System.out.println("Usuario convertido: '" + result + "'");
		return result;
	}



}
