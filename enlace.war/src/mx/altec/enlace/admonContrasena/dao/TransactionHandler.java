package mx.altec.enlace.admonContrasena.dao;

import java.sql.SQLException;

public class TransactionHandler extends ConnectionHandlerImpl {

	private boolean isTransaction;

	public TransactionHandler() {
		super();
	}

	public TransactionHandler(Class caller) {
		super(caller);
	}

	public boolean closeConnection() throws SQLException {
		if (!isTransaction) {
			return super.closeConnection();
		}
		return false;
	}
	public boolean beginTransaction() {
		if (isTransaction == true) {
			return false;
		}
		isTransaction = true;
		return true;
	}

	public boolean endTransaction() throws SQLException {
		if (isTransaction) {
			if (super.commitConnection())
				return super.closeConnection();
		}

		isTransaction = false;
		return false;
	}

	public boolean rollbackTransaction() throws SQLException {
		if (isTransaction) {
			if (super.rollbackConnection())
				return super.closeConnection();
		}
		return false;
	}

	public static boolean isInTransaction(TransactionHandler handler) {
		return handler.isTransaction;
	}
}