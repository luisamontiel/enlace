package mx.altec.enlace.admonContrasena.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import mx.altec.enlace.admonContrasena.vo.PeticionTO;
import mx.altec.enlace.admonContrasena.vo.PeticionVO;
import mx.altec.enlace.admonContrasena.vo.UtilVO;


public class DatosSolicitudDAO extends ConnectionHandlerDAO {
	 private static DataSource ds;

	 public DatosSolicitudDAO()
	 {

	 }
	 public DatosSolicitudDAO(ConnectionHandler handler)
	 {
		 super(handler);
	 }

	 public Connection createiASConn ( String dbName )
     throws SQLException {
	 return createiASConn_static (dbName);
     }


     public static synchronized Connection createiASConn_static ( String dbName )
     throws SQLException {
	 Connection conn = null;
	 if(ds == null) {
	 try {
	     InitialContext ic = new InitialContext ();
			 ds = ( DataSource ) ic.lookup ( dbName );
	     conn = ds.getConnection ();
	 } catch ( NamingException ne ) {
			 //log ( "Unable create connection to: " + dbName + "; " + ne.toString () );
		 }
	 }
	 else {
		 conn = ds.getConnection ();
	 }
	 return conn;
     }

     public String seleccionaSolGenerada(String cliente, String caracter)throws SQLException, Exception {
    	 String folio="";
    	 StringBuffer sb = new StringBuffer();

    	 sb.append("Select max(foliosolicitud) as solic from nipm_solicitudes \n")
         .append(" where codigocliente = '")
    	 .append(UtilVO.convierteUsr7a8(cliente))
    	 .append("' and estado = '" + caracter + "'");
    	 Connection con= getConnection();
         Statement st= con.createStatement();
         ResultSet resul = st.executeQuery(sb.toString());

         if (resul!=null && resul.next()){
        	 int intSolic = resul.getInt("solic");
        	 folio = String.valueOf(intSolic);
         }
      	 return folio;
     }




     /**
      * @author Emmanuel Sanchez Castillo
      * @param folio
      * @param codigoCliente
      * @return
      * @throws SQLException
      * @throws Exception
      */

     public PeticionTO consultaReimprimeSolicitud( int folio, String codigoCliente)throws SQLException,Exception {

    	 PeticionTO to= new PeticionTO();
    	 String consul="";
  //  	 consul= consul + "Select eweb_nipm_peticion_reset.folioPeticion,eweb_nipm_peticion_reset.codigoCliente,eweb_nipm_peticion_reset.nombre,eweb_nipm_peticion_reset.paterno,eweb_nipm_peticion_reset.materno,";
   // 	 consul= consul +" eweb_nipm_peticion_reset.areaCode,eweb_nipm_peticion_reset.telefono,eweb_nipm_peticion_reset.areacodetelalternativo,eweb_nipm_peticion_reset.telefonoAlternativo,eweb_nipm_peticion_reset.email,eweb_nipm_peticion_reset.direccionCliente ";
   // 	 consul= consul +" from eweb_nipm_solicitudes , eweb_nipm_peticion_reset ";
   // 	 consul= consul + " Where (eweb_nipm_solicitudes.folioSolicitud =";
   // 	 consul= consul + folio + ") and (eweb_nipm_solicitudes.folioSolicitud = eweb_nipm_peticion_reset.folioPeticion ) and (eweb_nipm_solicitudes.codigoCliente='"+ codigoCliente +"') ";


        consul= "Select eweb_nipm_peticion_reset.foliopeticion,eweb_nipm_peticion_reset.codigocliente,";
        consul=consul + "eweb_nipm_peticion_reset.nombre,eweb_nipm_peticion_reset.paterno,eweb_nipm_peticion_reset.materno,";
        consul=consul + "eweb_nipm_peticion_reset.areacode,eweb_nipm_peticion_reset.telefono,TO_CHAR(eweb_nipm_solicitudes.fechasolictud,'DD/MM/YYYY') fecha,";
        consul=consul + "eweb_nipm_peticion_reset.telefonoalternativo,eweb_nipm_peticion_reset.email,eweb_nipm_peticion_reset.areacodetelalternativo,";
        consul=consul + "eweb_nipm_peticion_reset.direccioncliente from STPROD.eweb_nipm_solicitudes, STPROD.eweb_nipm_peticion_reset";
        consul=consul + " Where (eweb_nipm_solicitudes.foliosolicitud ="+folio+") and ";
        consul=consul + "(eweb_nipm_solicitudes.foliopeticion = eweb_nipm_peticion_reset.foliopeticion )";
        consul=consul + " and (eweb_nipm_solicitudes.codigocliente='"+codigoCliente+"')";
   	 try{
                    Connection con= getConnection();
                    Statement st= con.createStatement();
			ResultSet resul = st.executeQuery(consul);
 		    if (resul != null && resul.next())
		    {

		    	if(resul.getString("folioPeticion")!=null){
		    	    to.setFolioSeguridad(Integer.parseInt(resul.getString("folioPeticion")));
		    	}else{to.setFolioSeguridad(0);}

		    	if(resul.getString("codigoCliente")!=null){

		    	   to.setCodigoCliente(resul.getString("codigoCliente"));
		    	}else{to.setCodigoCliente("");}

		    	if(resul.getString("nombre")!=null){

		    	   to.setNombreCliente(resul.getString("nombre"));
		    	}else{to.setNombreCliente("");}

		    	if(resul.getString("paterno")!=null){

		    	   to.setPaterno(resul.getString("paterno"));
		    	}else{to.setPaterno("");}

		    	if(resul.getString("materno")!=null){

		    	   to.setMaterno(resul.getString("materno"));
		    	}else{to.setMaterno("");}

		    	if(resul.getString("fecha")!=null){

		    	   to.setFechaPeticion(resul.getString("fecha"));
		    	}else{to.setFechaPeticion("");}

		    	if(resul.getString("areaCode")!=null){

		    	   to.setAreaCode(resul.getString("areaCode"));
		    	}else{to.setAreaCode("");}
		    	if(resul.getString("areacodetelalternativo")!=null){

		    	   to.setAreaCodeAlter(resul.getString("areacodetelalternativo"));
		    	}else{to.setAreaCodeAlter("");}



		    	if(resul.getString("telefono")!=null){

		    	   to.setTelefono(resul.getString("telefono"));
		    	}else{to.setTelefono("");}

		    	if(resul.getString("telefonoAlternativo")!=null){

		    	   to.setTelefonoAlternativo(resul.getString("telefonoAlternativo"));
		    	}else{to.setTelefonoAlternativo("");}

		    	if(resul.getString("email")!=null){

		    	   to.setEmail(resul.getString("email"));
		    	}else{to.setEmail("");}

		    	if(resul.getString("direccioncliente")!=null){

		    	   to.setDireccionCliente(resul.getString("direccioncliente"));
		    	}else{to.setDireccionCliente("");}

		    }else{

		    	to.setFolioSeguridad(0);
		    	to.setCodigoCliente("");
		    	to.setNombreCliente("");
		    	to.setPaterno("");
		    	to.setMaterno("");
		    	to.setFechaPeticion("");
		    	to.setAreaCode("");
		    	to.setTelefono("");
		    	to.setTelefonoAlternativo("");
		    	to.setEmail("");
		    	to.setDireccionCliente("");

		    }

	    	resul.close();
 			st.close();
                  closeConnection();
 		}catch (SQLException se){

                     se.printStackTrace();
                      throw se;

 		}catch(Exception e){
                     e.printStackTrace();
 			throw e;
 		}finally{
 			closeConnection();
 		}


	   return to;
     }

     /**
      * @author Emmanuel Sanchez Castillo
      * @param to
      * @param con
      * @return
      * @throws SQLException
      */
     public String getFolioSolicitud(PeticionVO peticion)
     throws SQLException,Exception{
    	 String folioReturn="";
    	 Connection con = null;
    	 try{
    	 con = getConnection();
    	 ResultSet rs;
    	 String query ="SELECT MAX (foliosolicitud)FROM stprod.eweb_nipm_solicitudes " +
    	 		"WHERE codigocliente = '"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"'  AND tiposolicitud='R' AND tiponip='A'";


    	 PreparedStatement statement = con.prepareStatement(query);

		 rs = statement.executeQuery();
		 if(rs.next())
		 {
			//colocar todos los valores de las variables
			 folioReturn = Integer.toString(rs.getInt(1));
		 }
    	 }
		 catch(SQLException sqlE)
   	     {
			 folioReturn="-1";
   	    	 throw sqlE;
   	     }
   	     catch(Exception e)
   	     {
   	    	 folioReturn="-1";
   	    	 throw e;
   	     }
   	     finally
   	     {

   	    	closeConnection();
   	     }
         return folioReturn;
     }
     /**
      * @author Emmanuel Sanchez Castillo
      * Funcion para verificar si un cliente tiene una solicitud
      * @param peticion
      * @return
      * @throws SQLException
      * @throws Exception
      */
     public boolean existeSolicitudCliente(PeticionVO peticion)
      throws SQLException,Exception{
    	     boolean result=false;
        	 Connection con = null;
        	 try{
             con = getConnection();
        	 ResultSet rs;
        	 String query ="SELECT COUNT(foliosolicitud)FROM stprod.eweb_nipm_solicitudes " +
        	 		"WHERE codigocliente = '"+UtilVO.convierteUsr7a8(  peticion.getCodigoCliente() )+"'";

        	 //PreparedStatement statement = con.prepareStatement(query);
    		 Statement statement = con.createStatement();
    		 rs = statement.executeQuery(query);
    		 if(rs.next())
    		 {
    			//colocar todos los valores de las variables
    			if(rs.getInt(1)>0)
    				result = true;
    		 }
        	 }
    		 catch(SQLException sqlE)
       	     {
    			 result = false;
       	    	 throw sqlE;
       	     }
       	     catch(Exception e)
       	     {
       	    	 result=false;
       	    	 throw e;
       	     }
       	     finally
       	     {
       	    	closeConnection();
       	     }
             return result;
     }

    /*
     Borrar prueba de tabla segu_usuarios
     public boolean seguUsuarios()
     throws SQLException,Exception{
   	     boolean result=false;
       	 Connection con = null	;
       	 try{
            con = getConnection();
       	 ResultSet rs;
       	 String query ="SELECT COUNT(*) FROM STPROD.SEGU_USUARIOS ";


       	 //PreparedStatement statement = con.prepareStatement(query);
   		 Statement statement = con.createStatement();
   		 rs = statement.executeQuery(query);
   		 if(rs.next())
   		 {
   			//colocar todos los valores de las variables
   			if(rs.getInt(1)>0)
   				result = true;
   			System.out.println("Cantidad "+rs.getInt(1));
   		 }
       	 }
   		 catch(SQLException sqlE)
      	     {
   			 result = false;
      	    	 throw sqlE;
      	     }
      	     catch(Exception e)
      	     {
      	    	 result=false;
      	    	 throw e;
      	     }
      	     finally
      	     {
      	    	closeConnection();
      	     }
            return result;
    }*/
     /**
      * @author Emmanuel Sanchez Castillo
      * Funcion para cambiar el estado de solicitudes a bloqueado
      * @param peticion
      * @return
      */
     public boolean bloqueaSolicitudes(PeticionVO peticion)
     throws SQLException,Exception{
	     boolean result=false;
    	 Connection con = null;
    	 try{
    	 con = getConnection();

    	 int ultimoFolio=0;

    	  String querySelect ="SELECT FOLIOSOLICITUD  FROM  STPROD.EWEB_NIPM_SOLICITUDES_MOVS " +
    	 		"WHERE codigocliente = '"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"'" +
    	 		"AND FOLIOSOLICITUD = (SELECT nvl(MAX(FOLIOSOLICITUD),0)  FROM  STPROD.EWEB_NIPM_SOLICITUDES_MOVS " +
    	 		"WHERE ESTADO='G' AND codigocliente = '"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"')";

 		System.out.println("ANTES DE EJECUTAR LA BUSQUEDA DEL ULTIMO FOLIO" );

 		System.out.println(querySelect );


    	 Statement st =con.createStatement();
    	 ResultSet rs = st.executeQuery(querySelect);


    	 if(rs.next()){
    	 	ultimoFolio = rs.getInt("FOLIOSOLICITUD");
    	 }


    	 System.out.println("Renglones actulizados:" +ultimoFolio );
    	 String query ="UPDATE stprod.eweb_nipm_solicitudes SET estado='T' " +
    	 		"WHERE codigocliente = '"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"'";

    	// PreparedStatement statement = con.prepareStatement(query);
    	 Statement statement = con.createStatement();
    	 //rs = statement.executeQuery(query);
		 int rows = statement.executeUpdate(query);
		 System.out.println("Renglones actulizados:" +rows );
		 result = true;
		 PeticionVO pvo = new PeticionVO();
		 pvo.setCodigoCliente ( peticion.getCodigoCliente());

		 pvo.setPaterno (peticion.getPaterno());
		 pvo.setMaterno ( peticion.getMaterno());
		 pvo.setNombre ( peticion.getNombre());
		 pvo.setEstado("T");
		 pvo.setDireccion ( peticion.getDireccion());

		 if (ultimoFolio!=0){

		insertaHistorico(String.valueOf(ultimoFolio), pvo);
		 }
    	 }
		 catch(SQLException sqlE)
   	     {
			 result = false;
   	    	 throw sqlE;
   	     }
   	     catch(Exception e)
   	     {
   	    	 result=false;
   	    	 throw e;
   	     }
   	     finally
   	     {
   	    	closeConnection();
   	     }
         return result;
 }

     /**
      * @author Emmanuel Sanchez Castillo
      * Funcion para insertar un nuevo registro en solicitudes
      * @param peticion
      * @return
      * @throws SQLException
      * @throws Exception
      */

 	public boolean insertaSolicitud(PeticionVO peticion)
    throws SQLException,Exception {
        boolean result;


        String query ="INSERT INTO STPROD.EWEB_NIPM_SOLICITUDES (FOLIOSOLICITUD, FOLIOPETICION, TIPOSOLICITUD, IDUSUARIO, NOMBREUSUARIO, IDSUCURSALSOLICITANTE, DESCRIPCIONSUCSOL, IDSUCURSALENTREGA, DESCRIPCIONSUCENT, " +
        		"FECHASOLICTUD, CODIGOCLIENTE, PATERNO, MATERNO, NOMBRE, TIPONIP, IDCANAL, ESTADO, CONTRATO, DESCRIPCIONCONTRATO, FECHATRAMITE, CAMBIOSUCURSAL, IMPRIMESUCURSAL, ORIGENSOLICITUD, FOLIORECHAZOCANCELACION, " +
        		"DESCRIPCIONRECHAZOCANCELACION, TIPORECHAZOCANCELACION, DIRECCIONCLIENTE) " +
        		"VALUES(STPROD.EWEB_INC_NIPM_SOL.NEXTVAL,'"+peticion.getFolioPeticion()+"','R','"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"',' ',' ',' ',' ',' '," +
		"SYSDATE,'"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"','"+peticion.getPaterno()+"','"+peticion.getMaterno()+"','"+peticion.getNombre()+"','A','ENLACE','"+peticion.getEstado()+"',' ',' '," +
		"SYSDATE,' ',' ','P','',' ',' ','"+peticion.getDireccion()+"')";

   	     Connection con=null ;
   	     try{
   	    	con = getConnection();
   	     Statement statement = con.createStatement();

   	     result = statement.execute(query);
		 result =true;
   	     }
   	     catch(SQLException sqlE)
   	     {
   	    	 result = false;
   	    	 throw sqlE;
   	     }
   	     catch(Exception e)
   	     {
   	    	 result=false;
   	    	 throw e;
   	     }
   	     finally
   	     {
   	    	closeConnection();
   	     }

   	 return result;
}
    /**
     * @author Emmanuel Sanchez Castillo
     * Funcion para inserta un nuevo registo en el historico de solicutudes
     * @param peticion
     * @return
     * @throws SQLException
     * @throws Exception
     */
	public boolean insertaHistorico(String folioSolicitud,PeticionVO peticion)
    throws SQLException,Exception {
        boolean result;
        Connection con =null;
        try{
        	con = getConnection();


        String query = "INSERT INTO STPROD.EWEB_NIPM_SOLICITUDES_MOVS (FOLIO, FOLIOSOLICITUD, TIPOSOLICITUD, IDUSUARIO, NOMBREUSUARIO, IDSUCURSALSOLICITANTE, DESCRIPCIONSUCSOL, IDSUCURSALENTREGA, DESCRIPCIONSUCENT, " +
        		"FECHASOLICTUD,CODIGOCLIENTE, PATERNO, MATERNO, NOMBRE, TIPONIP, IDCANAL, ESTADO, CONTRATO, DESCRIPCIONCONTRATO, FECHATRAMITE, CAMBIOSUCURSAL, IMPRIMESUCURSAL, ORIGENSOLICITUD, " +
        	    "FOLIORECHAZOCANCELACION, DESCRIPCIONRECHAZOCANCELACION, TIPORECHAZOCANCELACION, DIRECCIONCLIENTE) " +
    	"VALUES(STPROD.EWEB_INC_NIPM_SOL_MOVS.NEXTVAL,'"+folioSolicitud+"','R','" + UtilVO.convierteUsr7a8(peticion.getCodigoCliente()) + "',' ',' ',' ',' ',' '," +
		"SYSDATE,'"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"','"+peticion.getPaterno()+"','"+peticion.getMaterno()+"','"+peticion.getNombre()+"','A','ENLACE','"+peticion.getEstado()+"',' ',' '," +
		"SYSDATE,' ',' ','P','',' ',' ','"+peticion.getDireccion()+"')";
        Statement statement = con.createStatement();

        statement.execute(query);
		 result=true;
        }
  	     catch(SQLException sqlE)
  	     {
  	    	 result = false;
  	    	 throw sqlE;
  	     }
  	     catch(Exception e)
  	     {
  	    	 result=false;
  	    	 throw e;
  	     }
  	     finally
  	     {
  	    	closeConnection();
  	     }
   	 return result;
}
	/**
	 * @author Emmanuel Sanchez Castillo
	 * Funcion para insertar un registro de bitacora solicitudes
	 * @param foliosolicitud folio de la solicitudes
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public boolean InsertaBitacora(String foliosolicitud,String estado)
	 throws SQLException,Exception{
    	 boolean result = false;

    	 String query ="INSERT INTO STPROD.EWEB_NIPM_BITACORA_SOLICITUDES (FOLIOBITACORA, FOLIOSOLICITUD, FECHATRAMITE, IDUSUARIO, NOMBREUSUARIO, ESTADO, NOTAS) " +
    	 				"VALUES (STPROD.EWEB_INC_NIPM_BIT_SOL.NEXTVAL,'"+foliosolicitud+"',SYSDATE,' ',' ','"+estado+"','SOLICITUD DEL PORTAL')";

    	 Connection con =null;
         try{
         con = getConnection();
         Statement statement = con.createStatement();

         statement.execute(query);
		 result=true;
         }
  	     catch(SQLException sqlE)
  	     {
  	    	 result = false;
  	    	 throw sqlE;
  	     }
  	     catch(Exception e)
  	     {
  	    	 result=false;
  	    	 throw e;
  	     }
  	     finally
  	     {
  	    	closeConnection();
  	     }
    	 return result;
     }


}
