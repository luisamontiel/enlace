package mx.altec.enlace.admonContrasena.dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionHandler {
	public Connection getConnection() throws Exception;
	public boolean closeConnection() throws SQLException;
}