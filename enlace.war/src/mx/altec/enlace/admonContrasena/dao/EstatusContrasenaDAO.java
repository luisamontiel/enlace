/**
 * Nombre: EstatusContrasenaDAO.java
 * Descripcion: Clase que consulta el estatus de la contraseņa de
 * 				un cliente
 * Fecha:	VC JGR 27/04/2007
 */

package mx.altec.enlace.admonContrasena.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import mx.altec.enlace.admonContrasena.vo.UtilVO;
import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.utilerias.EIGlobal;


public class EstatusContrasenaDAO {

	/**
	 * Consulta el estatus de la contraseņa
	 * @param codigoCliente
	 * @return ValueObject con los datos encontrado, si no hay datos se regresa
	 * 			en mensaje una letra N la cual indica que no ha generado una
	 * 			contraseņa
	 * @throws Exception
	 */
	public ValueObject consultaStatus(String codigoCliente) throws Exception{
		ValueObject voRegreso = new ValueObject();
		ConnectionHandlerImpl conn = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT activo, logon_fallidos, num_max_logons, fch_exp_nip, desbloq_tres_intentos FROM eweb_vignip WHERE cve_usr = '" )
		  .append(codigoCliente)
		  .append("'");
		try{
			conn = new ConnectionHandlerImpl();
			rs = conn.getConnection().createStatement().
					executeQuery(sb.toString());

			if (rs.next()){
				Date fechaExpira = rs.getDate("fch_exp_nip");
				EIGlobal.mensajePorTrace("FECHA EXPIRADA->" +
						fechaExpira + "<-", EIGlobal.NivelLog.DEBUG);
				if (new Date().compareTo(fechaExpira)>=0){
					voRegreso.setId(1);
					voRegreso.setMensaje("E");	//Contraseņa expirada
					EIGlobal.mensajePorTrace("CONTRASEŅA EXPIRADA->" +
							voRegreso.getMensaje() + "<-", EIGlobal.NivelLog.DEBUG);
				}else{
					EIGlobal.mensajePorTrace("CONTRASEŅA NO EXPIRADA", EIGlobal.NivelLog.DEBUG);
					int logonFallidos = rs.getInt("logon_fallidos");
					int numMaxLogons = rs.getInt("num_max_logons");
					int desbloqTresIntentos = rs.getInt("desbloq_tres_intentos");
					EIGlobal.mensajePorTrace("LOGONFALLIDOS->" +
							logonFallidos + "<-", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("NUMMAXLOGONS->" +
							numMaxLogons + "<-", EIGlobal.NivelLog.DEBUG);
					if (logonFallidos >= numMaxLogons){

						voRegreso.setId(1);
						//voRegreso.setMensaje("B");	//Contraseņa bloqueada por intentos fallidos
						EIGlobal.mensajePorTrace("ESTATUS CONTRASENA 1->" +
								voRegreso.getMensaje() + "<-", EIGlobal.NivelLog.DEBUG);
						if (desbloqTresIntentos==0){
							voRegreso.setMensaje("BT"); //Contraseņa BLOQUEADA
						}else{
							voRegreso.setMensaje("BP");//Contraseņa bloqueada permanentemente
						}
					}else{
						voRegreso.setId(1);
						voRegreso.setMensaje(rs.getString("activo"));
						EIGlobal.mensajePorTrace("ESTATUS CONTRASENA 2->" +
								voRegreso.getMensaje() + "<-", EIGlobal.NivelLog.DEBUG);
					}
				}
			}else{
				voRegreso.setId(2);
				voRegreso.setMensaje("N");	//No existe contraseņa para usuario
				EIGlobal.mensajePorTrace("ESTATUS CONTRASENA 3->" +
						voRegreso.getMensaje() + "<-", EIGlobal.NivelLog.DEBUG);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			conn.closeConnection();
		}
		return voRegreso;
	}


	/**
	 * Consulta en nombre corto del usuario
	 * @param vo
	 * @param codigoCliente
	 * @return ValueObject con el nombre corto del cliente
	 * @throws SQLException
	 * @throws Exception
	 */
	public ValueObject consultaUsuario(ValueObject vo, String codigoCliente)
		throws SQLException, Exception{
		ConnectionHandlerImpl conn = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();

		sb.append("SELECT nombre_corto FROM NUCL_PERSONA ")
		  .append("WHERE num_persona = '")
		  .append(codigoCliente)
		  .append("'");
		EIGlobal.mensajePorTrace("EstatusContrasenaDAO.consultaUsuario " +
				"QUERY-> " + sb.toString(), EIGlobal.NivelLog.DEBUG);
		try{
			conn = new ConnectionHandlerImpl();
			rs = conn.getConnection().createStatement().
					executeQuery(sb.toString());
			EIGlobal.mensajePorTrace("EstatusContrasenaDAO.consultaUsuario " +
					"DESPUES EJECUTAR QUERY", EIGlobal.NivelLog.DEBUG);
			if (rs.next()){
				EIGlobal.mensajePorTrace("EstatusContrasenaDAO.consultaUsuario " +
						"DENTRO DEL IF", EIGlobal.NivelLog.DEBUG);
				vo.setUsuario(rs.getString("nombre_corto"));
			}
		}catch(SQLException sqlEx){
			EIGlobal.mensajePorTrace("EstatusContrasenaDAO.consultaUsuario." +
					"SQLException-> " + sqlEx.getMessage(), EIGlobal.NivelLog.DEBUG);
			sqlEx.printStackTrace();
			throw sqlEx;
		}catch(Exception e){
			EIGlobal.mensajePorTrace("EstatusContrasenaDAO.consultaUsuario." +
					"Exception-> " + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			e.printStackTrace();
			throw e;
		}finally{
			conn.closeConnection();
		}
		return vo;
	}

	/**
	 * Consulta en nombre corto del usuario
	 * @param vo
	 * @param codigoCliente
	 * @return ValueObject con el nombre corto del cliente
	 * @throws SQLException
	 * @throws Exception
	 */
	public ValueObject consultaDatosUsuario(ValueObject vo, String codigoCliente)
		throws SQLException, Exception{
		ConnectionHandlerImpl conn = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();

		sb.append("SELECT NOMBRE, APELL_PATERNO, APELL_MATERNO ")
		  .append("FROM NUCL_PERSON_FIS ")
		  .append("WHERE NUM_PERSONA = '")
		  .append(codigoCliente)
		  .append("'");
		EIGlobal.mensajePorTrace("DATOS USUARIO QUERY-> " + sb.toString(), EIGlobal.NivelLog.DEBUG);
		try{
			conn = new ConnectionHandlerImpl();
			rs = conn.getConnection().createStatement().
					executeQuery(sb.toString());
			EIGlobal.mensajePorTrace("DESPUES EJECUTAR QUERY DATOS USUARIO", EIGlobal.NivelLog.DEBUG);
			if (rs.next()){
				EIGlobal.mensajePorTrace("DENTRO DEL IF DATOS USUARIO", EIGlobal.NivelLog.DEBUG);
				vo.setNombreUsuario(rs.getString("NOMBRE"));
				vo.setAPaterno(rs.getString("APELL_PATERNO"));
				vo.setAMaterno(rs.getString("APELL_MATERNO"));
				if (rs.getString("APELL_MATERNO")!=null &&
						!rs.getString("APELL_MATERNO").equals("")){
					vo.setUsuario(vo.getNombreUsuario() + " " + vo.getAPaterno()
					+ " " + vo.getAMaterno());
				}else{
					vo.setUsuario(vo.getNombreUsuario() + " " + vo.getAPaterno());
				}
			}
		}catch(SQLException sqlEx){
			EIGlobal.mensajePorTrace("SQL EXCEPTION DATOS USUARIO-> " +
					sqlEx.getMessage(), EIGlobal.NivelLog.DEBUG);
			sqlEx.printStackTrace();
			throw sqlEx;
		}catch(Exception e){
			EIGlobal.mensajePorTrace("EXCEPTION DATOS USUARIO-> " +
					e.getMessage(), EIGlobal.NivelLog.DEBUG);
			e.printStackTrace();
			throw e;
		}finally{
			conn.closeConnection();
		}
		return vo;
	}


	/**
	 * Consulta es estatus de la solicitud de contraseņa
	 * @param vo
	 * @param codigoCliente
	 * @return ValueObject con el folio y estado de la solicitud, si no se
	 * 			encuentran datos se regresan mensajes alusivos al resultado
	 * @throws Exception
	 */
	public ValueObject consultaSolicitud(ValueObject vo, String codigoCliente)
		throws Exception{
		ConnectionHandlerImpl conn = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT a.foliosolicitud, a.estado, ")
		  .append("a.descripcionrechazocancelacion, ")
		  .append("(TO_DATE(a.fechasolictud + b.vigencia, 'dd/mm/yyyy') - ")
		  .append("TO_DATE(SYSDATE, 'dd/mm/yyyy')) as diasVigencia ")
		  .append("FROM nipm_solicitudes a, nipm_bandejas b ")
		  .append("WHERE a.codigocliente = '")
		  .append(UtilVO.convierteUsr7a8(codigoCliente))
		  .append("' ")
		  .append("AND a.estado <> 'T' and a.estado = b.estado");
		EIGlobal.mensajePorTrace("EstatusContrasenaDAO.estatusContrasena " +
				"QUERY->" + sb.toString(), EIGlobal.NivelLog.DEBUG);
		try{
			conn = new ConnectionHandlerImpl();
			rs = conn.getConnection().createStatement().
					executeQuery(sb.toString());

			EIGlobal.mensajePorTrace("REFLEJAR CAMBIOS", EIGlobal.NivelLog.DEBUG);

			if (rs.next()){
				EIGlobal.mensajePorTrace("SI HAY REGISTROS", EIGlobal.NivelLog.DEBUG);
				String folioSolicitud = rs.getString("foliosolicitud");
				int diasVigencia = rs.getInt("diasVigencia");
				String estado = rs.getString("estado");

				EIGlobal.mensajePorTrace("DIAS VIGENCIA->" + diasVigencia, EIGlobal.NivelLog.DEBUG);

				if (estado.trim().equals("G") && diasVigencia <= 0){	//Se cancela la solicitud
					EIGlobal.mensajePorTrace("CANCELAR LA SOLICITUD->" +
							diasVigencia, EIGlobal.NivelLog.DEBUG);
					caducaSolicitud(folioSolicitud);
					vo.setFolioSolicitud(folioSolicitud);
					vo.setEstadoSolicitud("V");	//Solicitud vencida
					vo.setDescRechacho(
							rs.getString("descripcionrechazocancelacion"));
				}else{
					EIGlobal.mensajePorTrace("TOMAR DATOS SOLICITUD COMO VIENEN->"
							+ diasVigencia, EIGlobal.NivelLog.DEBUG);
					vo.setFolioSolicitud(rs.getString("foliosolicitud"));
					vo.setEstadoSolicitud(rs.getString("estado"));
					vo.setDescRechacho(
							rs.getString("descripcionrechazocancelacion"));
				}
			}else{
				EIGlobal.mensajePorTrace("SIN SOLICITUD->", EIGlobal.NivelLog.DEBUG);
				vo.setFolioSolicitud("SIN SOLICITUD");
				vo.setEstadoSolicitud("X");
				vo.setDescRechacho(null);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			conn.closeConnection();
		}
		return vo;
	}


	/**
	 * Actualiza el estatus de la contraseņa a estado I
	 * @return true en caso exitoso. false en caso de error
	 * @throws SQLException
	 * @throws Exception
	 */
	public boolean cambiaEdoContrasena(String codigoCliente)
		throws SQLException, Exception {
		boolean r = false;
		ConnectionHandlerImpl conn = null;
		try {
			conn = new ConnectionHandlerImpl();

			PreparedStatement ps = conn.getConnection().prepareStatement(
					"UPDATE eweb_vignip SET activo = ? WHERE cve_usr = ?");
			ps.setString(1, "I");
			ps.setString(2, codigoCliente);

			ps.executeUpdate();
			r = true;
		} catch (SQLException se) {
			throw se;
		} catch (Exception e) {
			throw e;
		} finally {
			conn.closeConnection();
		}
		return r;
	}


	public boolean caducaSolicitud(String folioSolicitud) throws Exception{
		boolean seCaduco = false;

		ConnectionHandlerImpl conn = null;
		conn = new ConnectionHandlerImpl();

		StringBuffer sb = new StringBuffer();

		sb.append("UPDATE NIPM_SOLICITUDES SET ESTADO='V'WHERE FOLIOSOLICITUD=")
		  .append(folioSolicitud);
		EIGlobal.mensajePorTrace("SE CADUCA SOLICITUD " +
				"QUERY->" + sb.toString(), EIGlobal.NivelLog.DEBUG);
		try{
			conn.getConnection().createStatement().executeUpdate(sb.toString());
			conn.commitConnection();
			EIGlobal.mensajePorTrace("SE CADUCA SOLICITUD EXISOSAMENTE", EIGlobal.NivelLog.DEBUG);
			seCaduco = true;
		}catch(Exception e){
			EIGlobal.mensajePorTrace("EXCEPTION EN CADUCA SOLICITUD" +
					e.getMessage(), EIGlobal.NivelLog.DEBUG);
			throw e;
		}finally{
			conn.closeConnection();
		}
		return seCaduco;
	}



	public ValueObject consultarContrato(String codigoCliente)throws SQLException, Exception{

		ValueObject vo = new ValueObject();
		ConnectionHandlerImpl conn = null;
		conn = new ConnectionHandlerImpl();
		ResultSet rs = null;
		String numCuenta="";

	 Statement st = conn.getConnection().createStatement();

	 rs = st.executeQuery("SELECT  a.num_cuenta as contrato" +
			 " FROM segu_usrperftele a,nucl_cuentas b " +
			   " WHERE a.cve_usuario=  '" + codigoCliente + "'  AND " +
			   " a.num_cuenta=b.num_cuenta " +
			   " AND b.cve_producto='TCT' AND nvl(b.estatus_cuenta,'A') <> 'C'");


	if (rs!=null && rs.next()){
		EIGlobal.mensajePorTrace("VOY A SACAR EL CONTRATO DEL RS", EIGlobal.NivelLog.DEBUG);
		numCuenta = rs.getString("contrato");
		EIGlobal.mensajePorTrace("CONTRATO EN dao ->" + numCuenta, EIGlobal.NivelLog.DEBUG);
		vo.setMensaje(numCuenta);
		vo.setId(0);
	}

		return vo;

	}

public ValueObject consultarRazon(String contrato)throws SQLException, Exception{

		ValueObject vo = new ValueObject();
		ConnectionHandlerImpl conn = null;
		conn = new ConnectionHandlerImpl();
		ResultSet rs = null;
		String cveUsuario="";
		String descripcion="";
	 Statement st = conn.getConnection().createStatement();

	 rs = st.executeQuery("SELECT num_persona, descripcion from nucl_cuentas where num_cuenta = '" + contrato + "'");
		if (rs!=null && rs.next()){
			cveUsuario = rs.getString("num_persona");
			descripcion = rs.getString("descripcion");
			vo.setMensaje(descripcion);
			vo.setNombreUsuario(cveUsuario);
			vo.setId(0);
		}
		return vo;

	}

}
