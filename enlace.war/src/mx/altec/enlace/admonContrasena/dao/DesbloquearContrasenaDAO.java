/*<VC proyecto="200710001" autor="AHO" fecha="29/04/2007" descripción="DESBLOQUEO DE CONTRASEÑA">*/
package mx.altec.enlace.admonContrasena.dao;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mx.altec.enlace.utilerias.EIGlobal;


public class DesbloquearContrasenaDAO extends ConnectionHandlerImpl {

	public long seleccionaIntentosFallidos(String usuario) throws SQLException, Exception{
		long intentos =0;
		try{
			ResultSet rs = null;
			Statement st = getConnection().createStatement();
			rs = st.executeQuery("Select NVL(DESBLOQ_TRES_INTENTOS,0) AS intentos " +
					"from EWEB_VIGNIP WHERE cve_usr = '" + usuario + "'");
			if (rs.next()){
				intentos = rs.getLong("intentos");
			}

		}catch (SQLException se){
			throw se;
		}catch(Exception e){
			throw e;
		}finally{
			closeConnection();
		}
		return intentos;
	}


	public void desbloquear(String usuario)throws SQLException, Exception{
		try{
			EIGlobal.mensajePorTrace("CONEXION CON HANDLER NUEVAMENTE", EIGlobal.NivelLog.INFO);
			 Statement st = getConnection().createStatement();
			st.execute("UPDATE EWEB_VIGNIP SET LOGON_FALLIDOS = 0, FCH_BLOQUEO = SYSDATE, " +
				"FCH_EXP_NIP = SYSDATE -1, FCH_ULT_MOD_NIP=SYSDATE, " +
				"DESBLOQ_TRES_INTENTOS=0 WHERE cve_usr = '" + usuario + "'");
			 getConnection().commit();
		}catch (SQLException se){
			throw se;
		}catch(Exception e){
			throw e;
		}finally{
			closeConnection();
		}
	}

	public String obtenContrasena(String usuario) throws SQLException, Exception {
		String contrasena = "";
		try{
			ResultSet rs = null;
			Statement st = getConnection().createStatement();
			rs = st.executeQuery("Select contrasena28 " +
					"from EWEB_VIGNIP WHERE cve_usr = '" + usuario + "'");
			if (rs.next()){
				EIGlobal.mensajePorTrace("VOY A SETTEAR SETTEAR LA CONTRASENA"  , EIGlobal.NivelLog.INFO);
				contrasena = rs.getString("contrasena28");
			}

		}catch (SQLException se){
			throw se;
		}catch(Exception e){
			throw e;
		}finally{
			closeConnection();
		}
		return contrasena;
	}


	public void actualizaBloqueo(String usuario)throws SQLException, Exception{
		try{

			 Statement st = getConnection().createStatement();
			st.execute("UPDATE EWEB_VIGNIP SET DESBLOQ_TRES_INTENTOS=1 WHERE cve_usr = '" + usuario + "'");
			 getConnection().commit();
		}catch (SQLException se){
			throw se;
		}catch(Exception e){
			throw e;
		}finally{
			closeConnection();
		}
	}


}

/*</VC>*/
