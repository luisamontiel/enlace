package mx.altec.enlace.admonContrasena.dao;

import java.sql.Connection;
import java.sql.SQLException;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


public class ConnectionHandlerImpl implements ConnectionHandler {

	private Connection con;
	private Class caller;

	public ConnectionHandlerImpl() {}

	public ConnectionHandlerImpl(Class caller) {
		this.caller= caller;
	}

	public Connection getConnection() throws SQLException {
		if (con == null) {
			try
			{
			con = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			//TODO Manejar null
			}catch(SQLException e)
			{
				e.printStackTrace();
				throw e;
			}
			con.setAutoCommit(false);
		}
		return con;
	}

	/**
	 * Metodo para liberar una conexi�n BD.<br>
	 * -Si la conexion es null no pasar� nada. <br>
	 * -Si la conexion no es null:
	 * +Si esta en una transaccion la conexion no pasar� nada (se guardara la conexion para las siguientes llamadas)<br>
	 * +Si no esta en ninguna transaccion la conexion se llamara el metodo close() de la conexion<br>
	 */
	public boolean closeConnection() throws SQLException {
		if (con == null)
			return false;
		con.setAutoCommit(true);
		con.close();
		con= null;
		return true;
	}

	/**
	 * Metodo para ejecutar un rollback a una conexion. Si la conexion no esta en una transaccion se llamara
	 * el metodo rollback.
	 */
	protected boolean rollbackConnection() throws SQLException {
		if (con == null)
			return false;
		con.rollback();
		return true;
	}

	/**
	 * Metodo para ejecutar un commit a una conexion. Si la conexion no esta en una transaccion se llamara
	 * el metodo rollback.
	 */
	protected boolean commitConnection() throws SQLException {
		if (con == null)
			return false;
		con.commit();
		return true;
	}

	public void finalize() {
		if (con == null)
			return;

		String errMsg=
			"ConnectionHandler: No se cerr� bien la conexi�n";

		if (caller != null) {
			errMsg += " en " + caller.getClass() + ".";
		} else {
			errMsg += " (no se pudo localizar el fuente del error).";
		}
		EIGlobal.mensajePorTrace(errMsg, EIGlobal.NivelLog.INFO);
	}
}
