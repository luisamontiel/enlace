package mx.altec.enlace.admonContrasena.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mx.altec.enlace.admonContrasena.vo.PeticionVO;
import mx.altec.enlace.admonContrasena.vo.UtilVO;


public class DatosPeticionDAO extends ConnectionHandlerDAO{


	public DatosPeticionDAO()
	{

	}

	public DatosPeticionDAO(ConnectionHandler handler)
	{
		super(handler);
	}

	 public String getFolioPeticion(PeticionVO peticion)
     throws SQLException,Exception{
    	 String folioReturn="";
    	 Connection con = null;
    	 try{
    	con = getConnection();
    	 ResultSet rs;
    	 String query ="SELECT MAX (foliopeticion)FROM stprod.eweb_nipm_peticion_reset " +
    	 		"WHERE codigocliente = '"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+"'";

    	 /*PreparedStatement statement = con.prepareStatement(query);
		 rs = statement.executeQuery();
		 */
    	 Statement statement = con.createStatement();
    	 rs = statement.executeQuery(query);

		 if(rs.next())
		 {
			//colocar todos los valores de las variables
			 folioReturn = Integer.toString(rs.getInt(1));
		 }
    	 }
		 catch(SQLException sqlE)
   	     {
			 folioReturn="-1";
   	    	 sqlE.printStackTrace();
   	    	 throw sqlE;
   	     }
   	     catch(Exception e)
   	     {
   	    	 folioReturn="-1";
   	    	e.printStackTrace();
   	    	 throw e;
   	     }
   	     finally
   	     {
   	    	closeConnection();
   	     }
         return folioReturn;
     }

	public boolean insertaPeticion(PeticionVO peticion)
    throws SQLException,Exception {
        boolean result;
        String query ="INSERT INTO STPROD.EWEB_NIPM_PETICION_RESET " +
 		"VALUES(STPROD.EWEB_INC_NIPM_PET_RES.NEXTVAL,'"+UtilVO.convierteUsr7a8(peticion.getCodigoCliente())+ "','"+peticion.getPaterno()+"','"+peticion.getMaterno()+"','"+peticion.getNombre()+"'," +
 				"'"+peticion.getDireccion()+"','"+peticion.getEmail()+"','"+peticion.getAreaCode()+"','"+peticion.getTelefono()+"'," +
 						"'"+peticion.getTelefonoAlternativo()+"','"+peticion.getContrasena()+"',SYSDATE,'"+peticion.getEstado()+"','"+peticion.getAreaCodeAlternativa()+"')";


        String queryDeleteDataUsr = "DELETE FROM  EWEB_DATA_USR " +
        "WHERE CVE_USUARIO = '" + peticion.getCodigoCliente() + "' ";

        String queryInsertDataUsr = "INSERT INTO EWEB_DATA_USR (CVE_USUARIO, EMAIL, LADA, CELULAR, LADAALT, CELULARALT, NOTIFICACION)" +
        "VALUES ('" + peticion.getCodigoCliente() + "','" + peticion.getEmail() + "', '" + peticion.getAreaCode() +
        "','" +peticion.getTelefono() + "','" + peticion.getAreaCodeAlternativa() + "', '" +peticion.getTelefonoAlternativo() + "' ,'')";


        Connection con = null;
        try
        {
        	con = getConnection();
        	Statement statement = con.createStatement();
        	result= statement.execute(query);

        	result = statement.execute(queryDeleteDataUsr);
        	result = statement.execute(queryInsertDataUsr);

        	result = true;
         }
        catch(SQLException sqlE)
	  	{
        	result = false;
	  	    throw sqlE;
	    }
        catch(Exception e)
	  	{
        	result=false;
	  	    throw e;
	    }
        finally
	  	{
	  	  closeConnection();
	  	}

   	   return result;
	   }


	public PeticionVO datosUsuario(String codigoCliente) throws Exception{
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT email as email, lada as areacode, celular as telefono, ")
		  .append("ladaalt as areacodetelalternativo, celularalt as telefonoalternativo ")
		  .append("FROM EWEB_DATA_USR ")
		  .append("WHERE cve_usuario = '")
		  .append(codigoCliente)
		  .append("'");

        Connection con = null;
		ResultSet rs = null;
		Statement statement = null;
		PeticionVO peticion = new PeticionVO();

		try{
        	con = getConnection();
        	statement = con.createStatement();
        	rs= statement.executeQuery(sb.toString());

			if (rs.next()){
				peticion.setEmail(rs.getString("email"));
				peticion.setAreaCode(rs.getString("areacode"));
				peticion.setTelefono(rs.getString("telefono"));
				peticion.setAreaCodeAlternativa(
						rs.getString("areacodetelalternativo"));
				peticion.setTelefonoAlternativo(rs.getString(
						"telefonoalternativo"));
			}
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			statement.close();
			rs.close();
			closeConnection();
		}
		return peticion;
	}
}
