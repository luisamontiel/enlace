package mx.altec.enlace.admonContrasena.dao;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class ConnectionHandlerDAO implements ConnectionHandler {

	protected ConnectionHandler handler;

	public ConnectionHandlerDAO() {
		handler= new ConnectionHandlerImpl(this.getClass());
	}

	public ConnectionHandlerDAO(ConnectionHandler handler)
		throws IllegalArgumentException {
		if (handler == null) {
			throw new IllegalArgumentException("El parametro handler no puede ser null. Initializa el parametro o crea una instancia ConnectionHandlerDAO sin parametro de entrada");
		}
		this.handler= handler;
	}

	public Connection getConnection() throws SQLException, Exception {
		return handler.getConnection();
	}

	public boolean closeConnection() throws SQLException {
		return handler.closeConnection();
	}


}