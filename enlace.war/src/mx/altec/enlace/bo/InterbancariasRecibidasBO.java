package mx.altec.enlace.bo;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mx.altec.enlace.beans.InterbancariasRecibidasBean;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServiceLocator;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.TranInterConstantes;

import java.text.DecimalFormat;

/**
 * 
 * @author Z712236
 *
 */
public class InterbancariasRecibidasBO {

	
	/**
	 * Consulta de transferencias interbancarias recibidas
	 * @param bean datos de entrada
	 * @param req request
	 * @param res response
	 * @return bean de resultado
	 * @throws SQLException control de excepciones
	 */
	public InterbancariasRecibidasBean consultarInterbancariasRecibidas(
			InterbancariasRecibidasBean bean, HttpServletRequest req,
			HttpServletResponse res) throws SQLException {
		
		EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java :: consultarInterbancariasRecibidas :: Iniciando Consulta", EIGlobal.NivelLog.INFO);
		InterbancariasRecibidasBean recibidasBean=null;
		
		Hashtable hs = null;
		ServicioTux tuxGlobal = new ServicioTux();
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaSalida = "";
		
		String[] fecha=bean.getFchTrasnfer().split("/");
		EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java :: consultarInterbancariasRecibidas :: bean.getFchTrasnfer" + bean.getFchTrasnfer(), EIGlobal.NivelLog.INFO);
		
  		try{
  			tramaEntrada.append(bean.getReferencia() != null ? bean.getReferencia() : "");
  			tramaEntrada.append('|');
  			tramaEntrada.append(bean.getCveRastreo() != null ? bean.getCveRastreo() : "");
  			tramaEntrada.append('|');
  			tramaEntrada.append(fecha[0]);
  			tramaEntrada.append(fecha[1]);
  			tramaEntrada.append(fecha[2]);
  			tramaEntrada.append('|');
  			tramaEntrada.append(bean.getCuenta());
  			tramaEntrada.append('|');
  			tramaEntrada.append('1');

  			EIGlobal.mensajePorTrace("InterbancariasRecibidas.java::consultarInterbancariasRecibidas:: Trama Entrada->"+ tramaEntrada.toString(), EIGlobal.NivelLog.INFO);
  			
  			hs = tuxGlobal.tranEstatDet(tramaEntrada.toString(), "TRAN_ESTAT_DET");
  			StringBuffer URL=new StringBuffer();
			if(hs != null) {
				recibidasBean=new InterbancariasRecibidasBean();
				tramaSalida = (String) hs.get("BUFFER");
				EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java::consultarInterbancariasRecibidas:: Trama Salida->"+ tramaSalida, EIGlobal.NivelLog.INFO);
				
				String[] datosTrama=tramaSalida.split("\\|");
				EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java::datosTrama->"+ datosTrama.length, EIGlobal.NivelLog.INFO);
				//Se muestra en log datos de la trama
				for(int i=0; i<datosTrama.length; i++){
					EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java::resultado ["+i+"] ->"+datosTrama[i], EIGlobal.NivelLog.INFO);
				}
				
				recibidasBean.setCodError(datosTrama[0] != null ? datosTrama[0] : "");//Codigo de error
				recibidasBean.setNumReferencia(datosTrama[7] != null ? datosTrama[7] : "");//Numero de Referencia
				recibidasBean.setRefInterbancaria("");//FIXME No se informa
				recibidasBean.setCuentaOrdenante(datosTrama[18] != null ? enmascararDigito(datosTrama[18], 4) : "");//Cuenta Ordenante
				recibidasBean.setNombreOrdenante(datosTrama[8] != null ? datosTrama[8] : "");//Nombre del ordenante
				recibidasBean.setRfcOrdenante(datosTrama[14] != null ? datosTrama[14] : "");//RFC del Ordenante
				recibidasBean.setBancoOrdenante(datosTrama[12] != null ? consultaDescripcionBanco(datosTrama[12]): "");//Banco Ordenante
				recibidasBean.setImporte(datosTrama[10] != null ? formatoImporte(datosTrama[10]) : "");//Importe
				
				String estatus="";
				if(("CO").equals(datosTrama[2])) {
					estatus="ACEPTADA Y CONFIRMADA";
				}else if(("DV").equals(datosTrama[2])) {
					estatus="DEVUELTA POR EL BANCO RECEPTOR EN EL SPEI";
				}else if(("CA").equals(datosTrama[2])) {
					estatus="RECHAZADA POR SANTANDER";
				}else if(("EN").equals(datosTrama[2])|| ("LI").equals(datosTrama[3])) {
					estatus="ACEPTADA";
				}else if(("PV").equals(datosTrama[2])|| ("PR").equals(datosTrama[3]) || ("PA").equals(datosTrama[3])) {
					estatus="EN PROCESO DE VALIDACIÓN";
				}else {
					estatus="EN PROCESO DE VALIDACIÓN";
				}
				recibidasBean.setEstatus(estatus);//se agrega el estatus segun las validaciones
				
				recibidasBean.setFechaHrConfirmacion(datosTrama[16] != null ? datosTrama[16] :"");//Fecha y Hora de Confirmacion
				recibidasBean.setCveRastreo(datosTrama[6] != null ? datosTrama[6] : "");//Clave de Rastreo
				recibidasBean.setConceptoPagoTransfer(datosTrama[15] != null ? datosTrama[15] : "");//Concepto del Pago/Transferencia
	
				/**
				 * Armado de URL para mandar al servlet
				 * 20150723|T|TR004 9127564|40014|40002|002010471800514371|2.0
				 **/
				URL.append("<a href=\"MDI_CepServlet?");
				URL.append("opcion=0");
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("fecha=");
				URL.append(bean.getFchTrasnfer());
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("cveRastreo=");
				URL.append(recibidasBean.getCveRastreo());
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("banco=");
				URL.append(datosTrama[12]);
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("ctaAbono=");
				URL.append(datosTrama[18]);
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("importe=");
				URL.append(formatoImporteS(datosTrama[10]));
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("ctaCargo=");
				if(null!=datosTrama[19]){
					EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java::Numero de cuenta de recepcion->"+ datosTrama[19] + "<-", EIGlobal.NivelLog.INFO);
					URL.append(datosTrama[19]);
				}
				URL.append("\" onclick=\"return validaNavegador(this);\">Descargar</a>");
				recibidasBean.setURL(URL.toString());
				
				if(datosTrama.length>18)
				{
				recibidasBean.setCuentaOrdenante(datosTrama[18] != null ? enmascararDigito(datosTrama[18], 4) : "");//Cuenta Ordenante
				}
				List<InterbancariasRecibidasBean> lstRecibidas=new ArrayList<InterbancariasRecibidasBean>();
				lstRecibidas.add(recibidasBean);
				recibidasBean.setLstInterbancarias(lstRecibidas);

				
				if(recibidasBean != null){
					exportar(recibidasBean, req, res);
				}
				
			}else{
				EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java::consultarInterbancariasRecibidas:: Sin resultado", EIGlobal.NivelLog.INFO);
			}

  		}catch (IOException e) {
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java::consultarInterbancariasRecibidas:: Error al crear archivo "
  					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
		}catch(Exception e){
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasBO.java::consultarInterbancariasRecibidas:: Error al crear la conexión "
  					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
  		}

		return recibidasBean;
	}

	/**
	 * Metodo para manejar el negocio para exportar el resultado
	 * asociadas al contrato.
	 *
	 * @param recibidasBean			Bean con datos de exportación
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @throws IOException			Dispara Exception
	 */
	public void exportar(InterbancariasRecibidasBean recibidasBean, HttpServletRequest req, HttpServletResponse res)
	throws IOException{
		EIGlobal.mensajePorTrace("InterbancariasRecibidas::exportar:: Entrando...", EIGlobal.NivelLog.DEBUG);

		char COMA=',';
		
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer linea = null;
		FileWriter fileExport = new FileWriter(Global.DIRECTORIO_REMOTO_WEB + "/" + session.getUserID() + ".doc");
		EIGlobal.mensajePorTrace("InterbancariasRecibidas::exportar:: Se creo el archivo.", EIGlobal.NivelLog.DEBUG);
		try{
			linea = new StringBuffer();
			linea.append(rellenar("NUMERO DE REFERENCIA",22,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("REFERENCIA INTERBANCARIA",26,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("CUENTA ORDENANTE",36,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("NOMBRE DEL ORDENANTE",51,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("RFC DEL ORDENANTE",19,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("BANCO ORDENANTE",41,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("IMPORTE",22,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("ESTATUS",15,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("FECHA Y HORA DE CONFIRMACION",29,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("CLAVE DE RASTREO",31,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("CONCEPTO DE PAGO/TRANSFERENCIA",121,' ','D'));
			linea.append(COMA);
			linea.append('\n');
			
			fileExport.write(linea.toString());
			for(int i=0; i<recibidasBean.getLstInterbancarias().size(); i++){
				InterbancariasRecibidasBean beanExport = (InterbancariasRecibidasBean)recibidasBean.getLstInterbancarias().get(i);
				linea = new StringBuffer();
				linea.append(rellenar(beanExport.getNumReferencia(),22,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getRefInterbancaria(),26,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getCuentaOrdenante(),36,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getNombreOrdenante(),51,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getRfcOrdenante(),19,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getBancoOrdenante(),41,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getImporte(),22,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getEstatus(),15,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getFechaHrConfirmacion(),29,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getCveRastreo(),31,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getConceptoPagoTransfer(),121,' ','D'));
				linea.append(COMA);
				linea.append('\n');
				
			    fileExport.write(linea.toString());
			}
			sess.setAttribute("fileExport","/Download/" + session.getUserID() + ".doc");
		}catch (IOException e2) {
			EIGlobal.mensajePorTrace("InterbancariasRecibidas::exportar:: IOException->" + e2.getMessage(), EIGlobal.NivelLog.INFO);
		}
		catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("InterbancariasRecibidas::exportar:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()+ "Linea de truene->" + lineaError[0], EIGlobal.NivelLog.INFO);
		}finally{
			fileExport.close();
			EIGlobal.mensajePorTrace("InterbancariasRecibidas::exportar:: Se cerro el archivo.", EIGlobal.NivelLog.DEBUG);
		}
	}
	
	/**
	 * Rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 * @return aux valor de respuesta
	 */
	public static String rellenar (String cad, int lon, char rel, char tip){
		StringBuffer aux = new StringBuffer();
		if(cad.length()>lon){
			return cad.substring(0,lon);
		}if('I' != tip && 'D' != tip){ 
			tip = 'I';
		}if( 'D' == tip ){
			aux.append(cad);
			//aux = cad;
		}
		for (int i=0; i<(lon-cad.length()); i++){
			aux.append("");
			aux.append(rel);
			//aux += ""+ rel;
		}
		if( 'I' == tip ){
			aux.append("");
			aux.append(cad);
			//aux += ""+ cad;
		}
		return aux.toString();
	}
	
	/**
	  * Enmascara un digito ****000
	  * @param digito valor a enmascarar
	  * @param caracteresVisibles cantidad de caracteres visibles
	  * @return enmascarado digito enmasacarado
	  */
	 public static String enmascararDigito(String digito, int caracteresVisibles){
			
		 StringBuffer enmascarado = new StringBuffer();
		 String digitosVisibles="";
		 String digitosEnmacarados="";
		 
		 digito = digito != null ? digito :"";
		 if(digito.length() > caracteresVisibles && caracteresVisibles > 0){
			 digitosVisibles=digito.substring((digito.length() - caracteresVisibles), digito.length());
			 digitosEnmacarados=digito.substring(0, (digito.length() - caracteresVisibles) );
			 for(int i=0;i<digitosEnmacarados.length(); i++){
				 enmascarado.append('*');
			 }
			 enmascarado.append(digitosVisibles);
		 }else{
			 enmascarado.append(digito);
		 }
		 return enmascarado.toString();
	}
	 
	/**
	 * Formato para importe
	 * @param importe valor de importe
	* @return importe formateado
	*/
	public String formatoImporte(String importe){
		try{
			importe=(importe==null || "".equals(importe)) ? "0" : importe;
			double valor = Double.parseDouble(importe)/100; 
			DecimalFormat format = new DecimalFormat("$###,###,###,###,##0.00");
			importe=format.format(valor);
		}catch(Exception e){
			importe="$0.00";
			EIGlobal.mensajePorTrace("InterbancariasRecibidas::formatoImporte:: error al formatear importe:: "+ e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		
		return importe;
	}
	/**
	 * Formato para importe
	 * @param importe valor de importe
	* @return importe formateado
	*/
	public String formatoImporteS(String importe){
		try{
			importe=(importe==null || "".equals(importe)) ? "0" : importe;
			double valor = Double.parseDouble(importe)/100; 
			DecimalFormat format = new DecimalFormat("###,###,###,###,##0.00");
			importe=format.format(valor);
		}catch(Exception e){
			importe="0.00";
			EIGlobal.mensajePorTrace("InterbancariasRecibidas::formatoImporte:: error al formatear importe:: "+ e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		
		return importe;
	}
	/**
	 * Metodo que realiza la consulta de la clave del banco
	 * @param banco banco a consultar
	 * @return clave numerica del banco
	 */
	public static String consultaDescripcionBanco(String banco) {
		String claveBanco="";
		ResultSet result =null;
		Statement stmt = null;
		Connection conexion=null;
		try {
			String query="SELECT NOMBRE_CORTO FROM COMU_INTERME_FIN WHERE CVE_INTERME= '"+banco+"'";
			conexion=createiASConn_static (Global.DATASOURCE_ORACLE);
			stmt = conexion.createStatement();
			result = stmt.executeQuery(query);
			EIGlobal.mensajePorTrace("TransferenciaInterbancariaDAO::consultaClaveBanco::query:" + query,EIGlobal.NivelLog.DEBUG);
			while (result.next ()) {
				try {
					claveBanco=result.getString("NOMBRE_CORTO");
				}catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
				}
			}
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				cierraConexion(conexion);
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			try{
				if(null!=conexion) {
					conexion.close();
					conexion=null;
				}
			}catch(SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return claveBanco;
	}
	/**
	 * Metodo que realiza la conexion a la Base de Datos
	 * @param dbName nombre de base de datos
	 * @return Connection conexion
	 * @throws SQLException Excepcion de Base de Datos
	 */
	public static Connection createiASConn_static ( String dbName )
		throws SQLException {
		Connection conn = null;
		ServiceLocator servLocator = ServiceLocator.getInstance();
		DataSource ds = null;
		try {
			 ds = servLocator.getDataSource(dbName);
		 }catch(NamingException e) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">, mensaje: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
		 }
		 if(null == ds) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">",EIGlobal.NivelLog.ERROR);
			 return null;
		 }
		 conn = ds.getConnection ();
		 return conn;
	}
	/**
	 * Metodo que realiza el cierre de la conexion
	 * @param conexion
	 * @throws SQLException
	 */
	public static void cierraConexion(Connection conexion)	throws SQLException{
		try{
			if(null!=conexion) {
				conexion.close();
				conexion=null;
			}
		}catch(SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}
	}
}