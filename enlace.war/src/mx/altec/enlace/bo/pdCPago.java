/*
 * pdCPago.java
 *
 * Created on 10 de abril de 2002, 03:41 PM
 */

package mx.altec.enlace.bo;

import java.util.*;

/**
 *
 * @author  Rafael Martinez Montiel
 * @version 1.0
 */
public class pdCPago implements java.io.Serializable {


    /** Holds value of property noSec. */
    private String noSec;

    /** Holds value of property noCta. */
    private String noCta;

    /** Holds value of property noOrden. */
    private String noOrden;

	/***Holds value of property Clvnoreg. */
	private String Clvnoreg;

    /** Holds value of property cveBenef. */
    private String cveBenef;

    /** Holds value of property fechaRecepcion. */
    private GregorianCalendar fechaRecepcion;

    /** Holds value of property fechaPago. */
    private GregorianCalendar fechaPago;

    /** Holds value of property instrucciones. */
    private String instrucciones;

    /** Holds value of property importe. */
    private double importe;

    /** Holds value of property fmaPago. */
    private char fmaPago;

    /** Holds value of property status. */
    private char status;

    /** Holds value of property fechaLimPago. */
    private GregorianCalendar fechaLimPago;

    /** Holds value of property fechaCancelacion. */
    private GregorianCalendar fechaCancelacion;

    /** Holds value of property fechaVencimiento. */
    private GregorianCalendar fechaVencimiento;

    /** Holds value of property fechaModificacion. */
    private GregorianCalendar fechaModificacion;

    /** Holds value of property cvePagoEnTodaSucursal. */
    private String cvePagoEnTodaSucursal;

    /** Holds value of property cveRegion. */
    private String cveRegion;

    /** Holds value of property cvePlaza. */
    private String cvePlaza;

    /** Holds value of property cveSucursal. */
    private String cveSucursal;

    /** Holds value of property refOperacion. */
    private String refOperacion;

    /** Holds value of property nombreBenefNoRegistrado. */
    private String nombreBenefNoRegistrado;

    /** Holds value of property fechaAplic. */
    private GregorianCalendar fechaAplic;

    /** Holds value of property statusDesc. */
    private String statusDesc;

    /** Creates new pdCPago */
    public pdCPago() {
    }

    /** Getter for property noSec.
     * @return Value of property noSec.
     */
    public String getNoSec() {
        return noSec;
    }

    /** Setter for property noSec.
     * @param noSec New value of property noSec.
     */
    public void setNoSec(String noSec) {
        this.noSec = noSec;
    }

    /** Getter for property noCta.
     * @return Value of property noCta.
     */
    public String getNoCta() {
        return noCta;
    }

    /** Setter for property noCta.
     * @param noCta New value of property noCta.
     */
    public void setNoCta(String noCta) {
        this.noCta = noCta;
    }

    /** Getter for property noOrden.
     * @return Value of property noOrden.
     */
    public String getNoOrden() {
        return noOrden;
    }

    /** Setter for property noOrden.
     * @param noOrden New value of property noOrden.
     */
    public void setNoOrden(String noOrden) {
        this.noOrden = noOrden;
    }

	/**Getter for property clave benef no registrado
	 *@return value of property
	 */
	 public String getClvnoreg(){
		 return Clvnoreg;
	 }

	 /**Setter for property  clave no regsitrado
	  * @Param Clvnoreg New value of property Clvnoreg
	  */
	 public void setClvnoreg(String clno){
		 this.Clvnoreg = clno;
	 }

    /** Getter for property cveBenef.
     * @return Value of property cveBenef.
     */
    public String getCveBenef() {
        return cveBenef;
    }

    /** Setter for property cveBenef.
     * @param cveBenef New value of property cveBenef.
     */
    public void setCveBenef(String cveBenef) {
        this.cveBenef = cveBenef;
    }


    /** Getter for property fechaRecepcion.
     * @return Value of property fechaRecepcion.
     */
    public GregorianCalendar getFechaRecepcion() {
        return fechaRecepcion;
    }

    /** Setter for property fechaRecepcion.
     * @param fechaRecepcion New value of property fechaRecepcion.
     */
    public void setFechaRecepcion(GregorianCalendar fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    /** Getter for property fechaPago.
     * @return Value of property fechaPago.
     */
    public GregorianCalendar getFechaPago() {
        return fechaPago;
    }

    /** Setter for property fechaPago.
     * @param fechaPago New value of property fechaPago.
     */
    public void setFechaPago(GregorianCalendar fechaPago) {
        this.fechaPago = fechaPago;
    }

    /** Getter for property instrucciones.
     * @return Value of property instrucciones.
     */
    public String getInstrucciones() {
        return instrucciones;
    }

    /** Setter for property instrucciones.
     * @param instrucciones New value of property instrucciones.
     */
    public void setInstrucciones(String instrucciones) {
        this.instrucciones = instrucciones;
    }

    /** Getter for property importe.
     * @return Value of property importe.
     */
    public double getImporte() {
        return importe;
    }

    /** Setter for property importe.
     * @param importe New value of property importe.
     */
    public void setImporte(double importe) {
        this.importe = importe;
    }

    /** Getter for property fmaPago.
     * @return Value of property fmaPago.
     */
    public char getFmaPago() {
        return fmaPago;
    }

    /** Setter for property fmaPago.
     * @param fmaPago New value of property fmaPago.
     */
    public void setFmaPago(char fmaPago) {
        this.fmaPago = fmaPago;
    }

    /** Getter for property status.
     * @return Value of property status.
     */
    public char getStatus() {
        return status;
    }

    /** Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus(char status) {
        this.status = status;
    }

    /** Getter for property fechaLimPago.
     * @return Value of property fechaLimPago.
     */
    public GregorianCalendar getFechaLimPago() {
        return fechaLimPago;
    }

    /** Setter for property fechaLimPago.
     * @param fechaLimPago New value of property fechaLimPago.
     */
    public void setFechaLimPago(GregorianCalendar fechaLimPago) {
        this.fechaLimPago = fechaLimPago;
    }

    /** Getter for property fechaCancelacion.
     * @return Value of property fechaCancelacion.
     */
    public GregorianCalendar getFechaCancelacion() {
        return fechaCancelacion;
    }

    /** Setter for property fechaCancelacion.
     * @param fechaCancelacion New value of property fechaCancelacion.
     */
    public void setFechaCancelacion(GregorianCalendar fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    /** Getter for property fechaVencimiento.
     * @return Value of property fechaVencimiento.
     */
    public GregorianCalendar getFechaVencimiento() {
        return fechaVencimiento;
    }

    /** Setter for property fechaVencimiento.
     * @param fechaVencimiento New value of property fechaVencimiento.
     */
    public void setFechaVencimiento(GregorianCalendar fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /** Getter for property fechaModificacion.
     * @return Value of property fechaModificacion.
     */
    public GregorianCalendar getFechaModificacion() {
        return fechaModificacion;
    }

    /** Setter for property fechaModificacion.
     * @param fechaModificacion New value of property fechaModificacion.
     */
    public void setFechaModificacion(GregorianCalendar fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    /** Getter for property cvePagoEnTodaSucursal.
     * @return Value of property cvePagoEnTodaSucursal.
     */
    public String getCvePagoEnTodaSucursal() {
        return cvePagoEnTodaSucursal;
    }

    /** Setter for property cvePagoEnTodaSucursal.
     * @param cvePagoEnTodaSucursal New value of property cvePagoEnTodaSucursal.
     */
    public void setCvePagoEnTodaSucursal(String cvePagoEnTodaSucursal) {
        this.cvePagoEnTodaSucursal = cvePagoEnTodaSucursal;
    }

    /** Getter for property cveRegion.
     * @return Value of property cveRegion.
     */
    public String getCveRegion() {
        return cveRegion;
    }

    /** Setter for property cveRegion.
     * @param cveRegion New value of property cveRegion.
     */
    public void setCveRegion(String cveRegion) {
        this.cveRegion = cveRegion;
    }

    /** Getter for property cvePlaza.
     * @return Value of property cvePlaza.
     */
    public String getCvePlaza() {
        return cvePlaza;
    }

    /** Setter for property cvePlaza.
     * @param cvePlaza New value of property cvePlaza.
     */
    public void setCvePlaza(String cvePlaza) {
        this.cvePlaza = cvePlaza;
    }

    /** Getter for property cveSucursal.
     * @return Value of property cveSucursal.
     */
    public String getCveSucursal() {
        return cveSucursal;
    }

    /** Setter for property cveSucursal.
     * @param cveSucursal New value of property cveSucursal.
     */
    public void setCveSucursal(String cveSucursal) {
        this.cveSucursal = cveSucursal;
    }

    /** Getter for property refOperacion.
     * @return Value of property refOperacion.
     */
    public String getRefOperacion() {
        return refOperacion;
    }

    /** Setter for property refOperacion.
     * @param refOperacion New value of property refOperacion.
     */
    public void setRefOperacion(String refOperacion) {
        this.refOperacion = refOperacion;
    }

    /** Getter for property nombreBenefNoRegistrado.
     * @return Value of property nombreBenefNoRegistrado.
     */
    public String getNombreBenefNoRegistrado() {
        return nombreBenefNoRegistrado;
    }

    /** Setter for property nombreBenefNoRegistrado.
     * @param nombreBenefNoRegistrado New value of property nombreBenefNoRegistrado.
     */
    public void setNombreBenefNoRegistrado(String nombreBenefNoRegistrado) {
        this.nombreBenefNoRegistrado = nombreBenefNoRegistrado;
    }

    /** Getter for property fechaAplic.
     * @return Value of property fechaAplic.
     */
    public GregorianCalendar getFechaAplic() {
        return fechaAplic;
    }

    /** Setter for property fechaAplic.
     * @param fechaAplic New value of property fechaAplic.
     */
    public void setFechaAplic(GregorianCalendar fechaAplic) {
        this.fechaAplic = fechaAplic;
    }

    /** Getter for property statusDesc.
     * @return Value of property statusDesc.
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /** Setter for property statusDesc.
     * @param statusDesc New value of property statusDesc.
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /** convierte el pago actual en un string
     * @return string con la representacion del pdCPago
     */
    public String toString(){
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        java.text.NumberFormat nf = java.text.NumberFormat.getInstance();
        if(nf instanceof java.text.DecimalFormat){
            ((java.text.DecimalFormat) nf ).applyPattern("#,###,###,###0.0#");
            ((java.text.DecimalFormat) nf ).setDecimalSeparatorAlwaysShown(true);
            ((java.text.DecimalFormat) nf ).setGroupingSize(3);
            ((java.text.DecimalFormat) nf ).setGroupingUsed(true);
            ((java.text.DecimalFormat) nf ).setMinimumFractionDigits(2);
            ((java.text.DecimalFormat) nf ).setMaximumFractionDigits(2);
        }
        StringBuffer ret=new StringBuffer("");
		ret.append("[");
        ret.append(noSec );
		ret.append("|");
        ret.append(noCta );
		ret.append("|");
        ret.append(noOrden );
		ret.append("|");
        ret.append(cveBenef );
		ret.append("|");
        ret.append(((null == fechaRecepcion) ? "-" : sdf.format(fechaRecepcion.getTime()) ) );
		ret.append("|");
        ret.append(((null == fechaAplic) ? "-" : sdf.format(fechaAplic.getTime()) ) );
		ret.append("|");
        ret.append(((null == fechaPago) ? "-" : sdf.format(fechaPago.getTime()) ) );
		ret.append("|");
        ret.append(instrucciones );
		ret.append("|");
        ret.append(nf.format(importe) );
		ret.append("|");
        ret.append(String.valueOf(fmaPago) );
		ret.append("|");
        ret.append(String.valueOf(status) );
		ret.append("|");
        ret.append(statusDesc );
		ret.append("|");
        ret.append(((null == fechaLimPago) ? "-" : sdf.format(fechaLimPago.getTime()) ) );
		ret.append("|");
        ret.append(((null == fechaCancelacion) ? "-" : sdf.format(fechaCancelacion.getTime()) ) );
		ret.append("|");
        ret.append(cvePagoEnTodaSucursal );
		ret.append("|");
        ret.append(cveRegion );
		ret.append("|");
        ret.append(cvePlaza );
		ret.append("|");
        ret.append(cveSucursal );
		ret.append("|");
        ret.append(refOperacion );
		ret.append("|");
        ret.append(nombreBenefNoRegistrado);
        ret.append("]");
        return ret.toString();
    }
}