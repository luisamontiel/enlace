package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

public class EI_Formato implements Serializable
 {
     public String cadena="";
     public EI_Tipo RN=new EI_Tipo();

   public boolean validaRFC(String rfc)
     {
       int fecha=6;
	   cadena="";
       switch(rfc.length())
         {
           case 13: cadena="[A-Z,a-z,�,�][A-Z,a-z,�,�][A-Z,a-z,�,�][A-Z,a-z,�,�][0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z,�,�][0-9,A-Z,a-z,�,�][0-9,A-Z,a-z,�,�]"; break;
           case 10: cadena="[A-Z,a-z,�,�][A-Z,a-z,�,�][A-Z,a-z,�,�][A-Z,a-z,�,�][0-9][0-9][0-9][0-9][0-9][0-9]"; break;
           case 12: cadena="[A-Z,a-z,�,�][A-Z,a-z,�,�][A-Z,a-z,�,�][0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z,�,�][0-9,A-Z,a-z,�,�][0-9,A-Z,a-z,�,�]"; break;
           case  9: cadena="[A-Z,a-z,�,�][A-Z,a-z,�,�][A-Z,a-z,�,�][0-9][0-9][0-9][0-9][0-9][0-9]"; break;
         }
       if(rfc.length()==9 || rfc.length()==12)
         fecha=5;
       if(!test(rfc))
         return false;
       int mes=Integer.parseInt(rfc.substring(fecha,fecha+2));
       int dia=Integer.parseInt(rfc.substring(fecha+2,fecha+4));
       if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
         {
           if(mes==2 && dia >=30)
            return false;
         }
       else
         return false;
       return true;
     }

   public boolean test(String evalua)
     {
	    Vector Items=new Vector();
        Items=separaCadena();
        if(evalua.length()!=Items.size())
          return false;
        for(int i=0;i<Items.size();i++)
          {
            RN.iniciaObjeto(',','@',Items.get(i)+"@");
            if(!cumpleCar(evalua.charAt(i)))
              return false;
          }
        return true;
     }

   public Vector separaCadena()
     {
	   Vector Items=new Vector();
       int idx=0;
       while(idx>=0)
        {
          Items.add(cadena.substring(cadena.indexOf("[",idx)+1,cadena.indexOf("]",idx)));
          idx=cadena.indexOf("[",idx+1);
        }
	   return Items;
     }

   boolean cumpleCar(char car3)
     {
        char car1=' ';
        char car2=' ';

        String strRN="";

        for(int j=0;j<=RN.totalCampos;j++)
         {
           strRN=RN.camposTabla[0][j].trim();
           if(strRN.length()>1)
            {
              if(strRN.indexOf("-")==1)
                {
                  car1=strRN.charAt(0);
                  car2=strRN.charAt(2);
                  if(car3>=car1 && car3<=car2)
                    return true;
                }
            }
           else
            {
              car1=strRN.charAt(0);
              if(car3==car1)
                return true;
            }
         }
        return false;
     }
 }