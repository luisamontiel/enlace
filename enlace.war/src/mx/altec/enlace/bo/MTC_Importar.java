package mx.altec.enlace.bo;

import java.io.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class MTC_Importar
 {
   public String cuentaPadre   ="";
   public String cuentaHija    ="";
   public int    nivelArbol    =0;
   public String descripcion   ="";
   public int diaCon           =0;
   public String horaCon       ="";
   public double saldoOp1      =0.0;
   public String invAuto       ="";
   public double saldoOp2      =0.0;

   public int err = 0;

  public void formateaLinea(String linea, String Divisa)
   {
     try
      {
        EIGlobal.mensajePorTrace("MTC_Importar.formatealinea() ...." + linea.length() + " ...... Divisa ...... " + Divisa, EIGlobal.NivelLog.INFO);
        if(linea.length()==117 && Divisa.equals("MN")){
		   //EIGlobal.mensajePorTrace("MTC_Importar - MTC_Importar(): Separando campos.", EIGlobal.NivelLog.INFO);
           cuentaPadre= linea.substring(0,16).trim();   //16
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): CuentaPadre "+cuentaPadre, EIGlobal.NivelLog.INFO);
           cuentaHija = linea.substring(16,32).trim();   //16
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): CuentaHija " +cuentaHija, EIGlobal.NivelLog.INFO);
		   // nivelArbol...
		   descripcion= linea.substring(34,74).trim(); //40
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): Descripcion "+descripcion, EIGlobal.NivelLog.INFO);
		   // diaCon ...
		   horaCon    = linea.substring(75,80).trim(); //5
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): horaCon "+horaCon, EIGlobal.NivelLog.INFO);
		   // saldoOp1
		   invAuto    = linea.substring(98,99).trim();//1
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): invAuto " + invAuto, EIGlobal.NivelLog.INFO);
		   // saldoOp2

		   String varTmp1="";
		   String varTmp2="";
		   String varTmp3="";
		   String varTmp4="";

		   varTmp1=linea.substring(32,34).trim(); //2 Nivel arbol
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): nivelArbol " + varTmp1, EIGlobal.NivelLog.INFO);
		   varTmp2=linea.substring(74,75).trim(); // 1 dia
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): diaCon " + varTmp2, EIGlobal.NivelLog.INFO);
		   varTmp3=formateaImp(linea.substring(80,98).trim());//18 SOP 1
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): saldoOp1 " + varTmp3, EIGlobal.NivelLog.INFO);
		   varTmp4=formateaImp(linea.substring(99,linea.length()).trim());
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): saldoOp2 " + varTmp4, EIGlobal.NivelLog.INFO);

		   //yhg Para la concentracion falta Agregar validacion para el sabado de
		   //17:00 a 20:00 horas. en las variables varTmp2 y horaCon
		   //Al parecer a este modulo solo entra la concentracion

           nivelArbol=Integer.parseInt(varTmp1);
		   diaCon=Integer.parseInt(varTmp2);
		   saldoOp1= new Double(varTmp3).doubleValue();
		   saldoOp2= new Double(varTmp4).doubleValue();
           err = 0;
            EIGlobal.mensajePorTrace("IMPRIME MENSAJE DE ERROR POR FECHA/HORARIO EN SABADO YHG varTmp2---->"+varTmp2, EIGlobal.NivelLog.INFO);
             EIGlobal.mensajePorTrace("IMPRIME MENSAJE DE ERROR POR FECHA/HORARIO EN SABADO YHG horaCon---->"+horaCon, EIGlobal.NivelLog.INFO);
              if(diaCon<0 || diaCon>6) err = 2;
             else if(varTmp2!=null && horaCon!=null && varTmp2.equals("6") &&
             (!horaCon.equals("17:00")  && !horaCon.equals("18:00") && !horaCon.equals("19:00") && !horaCon.equals("20:00")))
                      err = 1;
                EIGlobal.mensajePorTrace("IMPRIME MENSAJE DE ERROR POR FECHA/HORARIO EN SABADO YHG err---->"+err, EIGlobal.NivelLog.INFO);
        }else if(linea.length()==98 && Divisa.equals("USD")){
           cuentaPadre= linea.substring(0,16).trim();   //16
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): CuentaPadre "+cuentaPadre, EIGlobal.NivelLog.INFO);
           cuentaHija = linea.substring(16,32).trim();   //16
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): CuentaHija " +cuentaHija, EIGlobal.NivelLog.INFO);
		   // nivelArbol...
		   descripcion= linea.substring(34,74).trim(); //40
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): Descripcion "+descripcion, EIGlobal.NivelLog.INFO);
		   // diaCon ...
		   horaCon    = linea.substring(75,80).trim(); //5
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): horaCon "+horaCon, EIGlobal.NivelLog.INFO);
		   // saldoOp1
		   //invAuto    = linea.substring(98,99).trim();//1
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): invAuto " + invAuto, EIGlobal.NivelLog.INFO);
		   // saldoOp2

		   String varTmp1="";
		   String varTmp2="";
		   String varTmp3="";
		   String varTmp4="";

		   varTmp1=linea.substring(32,34).trim(); //2 Nivel arbol
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): nivelArbol " + varTmp1, EIGlobal.NivelLog.INFO);
		   varTmp2=linea.substring(74,75).trim(); // 1 dia
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): diaCon " + varTmp2, EIGlobal.NivelLog.INFO);
		   varTmp3=formateaImp(linea.substring(80,98).trim());//18 SOP 1
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): saldoOp1 " + varTmp3, EIGlobal.NivelLog.INFO);
		   //varTmp4=formateaImp(linea.substring(99,linea.length()).trim());
		   //EIGlobal.mensajePorTrace("MTC_Importar - formateaLinea(): saldoOp2 " + varTmp4, EIGlobal.NivelLog.INFO);




           nivelArbol=Integer.parseInt(varTmp1);
		   diaCon=Integer.parseInt(varTmp2);
		   saldoOp1= new Double(varTmp3).doubleValue();
		   //saldoOp2= new Double(varTmp4).doubleValue();

           err = 0;
                    EIGlobal.mensajePorTrace("IMPRIME MENSAJE DE ERROR POR FECHA/HORARIO EN SABADO YHG varTmp2---->"+varTmp2, EIGlobal.NivelLog.INFO);
             EIGlobal.mensajePorTrace("IMPRIME MENSAJE DE ERROR POR FECHA/HORARIO EN SABADO YHG horaCon---->"+horaCon, EIGlobal.NivelLog.INFO);
           if(diaCon<0 || diaCon>6) err = 2;
           else        if(varTmp2!=null && horaCon!=null && varTmp2.equals("6") &&
           (!horaCon.equals("17:00") && !horaCon.equals("18:00") && !horaCon.equals("19:00") && !horaCon.equals("20:00")))
                     err = 1;
          EIGlobal.mensajePorTrace("IMPRIME MENSAJE DE ERROR POR FECHA/HORARIO EN SABADO YHG 2 err---->"+err, EIGlobal.NivelLog.INFO);

		}else
			err = linea.length()+1;
       } catch(Exception e)
         {
		   EIGlobal.mensajePorTrace("MTC_Importar - MTC_Importar(): Excepcion, Error: "+e.getMessage(), EIGlobal.NivelLog.INFO);
		   EIGlobal.mensajePorTrace("MTC_Importar - MTC_Importar(): Linea Erronea: "+linea, EIGlobal.NivelLog.INFO);
           err = linea.length()+1;
         }
   }

   public boolean verificaCuenta(String[][] arrayCuentas)
    {
	  for(int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++)
	   {
	     if(arrayCuentas[indice][1].trim().equals(cuentaHija.trim()))
		  return true;
	   }
	  //EIGlobal.mensajePorTrace("MTC_Importar - verificaCuenta(): No se encontro en el arreglo", EIGlobal.NivelLog.INFO);
	  if(compruebaDigitoVerificador(cuentaHija))
		return true;
	  return false;
    }

   public boolean verificaCuentaPadre(String[][] arrayCuentas)
    {
	  if(cuentaPadre.equals("0"))
		return true;
   	  for(int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++)
	   {
	     if(arrayCuentas[indice][1].equals(cuentaPadre))
		   return true;
	   }
	  //EIGlobal.mensajePorTrace("MTC_Importar - verificaCuentaPadre(): No se encontro en el arreglo", EIGlobal.NivelLog.INFO);
	  if(compruebaDigitoVerificador(cuentaPadre))
		return true;
	  return false;
    }

   public boolean verificaSaldoOp1()
    {
      if(saldoOp1<0.0)
        return false;
      return true;
    }

   public boolean verificaSaldoOp2()
    {
      if(saldoOp2<0.0)
        return false;
      return true;
    }

   boolean compruebaDigitoVerificador(String cuenta)
	{
	  int pesos[]={4,3,2,2,7,6,5,4,3,2};
	  int total=0;
	  int residuo=0;
	  int digitoVerificador=0;

	  if(cuenta.length()!=11)
		return false;

	  for(int i=0;i<10;i++)
		total+=Integer.parseInt(cuenta.charAt(i)+"")*pesos[i];

	  residuo=total%11;
	  if(residuo==0)
		digitoVerificador=0;
	  else
	  if(residuo==1)
		return false;
	  else
	    digitoVerificador=11-residuo;

	  //EIGlobal.mensajePorTrace("MTC_Importar - compruebaDigitoVerificaor(): La cuenta: "+cuenta+" digito: "+Integer.toString(digitoVerificador), EIGlobal.NivelLog.INFO);

	  if(digitoVerificador==Integer.parseInt(cuenta.charAt(10)+""))
		return true;
	  return false;
	}

   String formateaImp(String imp)
	{
	   StringBuffer formateado=new StringBuffer("");

	   //EIGlobal.mensajePorTrace("MTC_Importar - formateaImp(): Antes imp: "+imp, EIGlobal.NivelLog.INFO);

	   if(imp.length()<4)
		 imp="0000"+imp;

	   if(imp.indexOf(".")>=0)
		 return imp;

	   formateado.append(imp.substring(0,imp.length()-2));
	   formateado.append(".");
	   formateado.append(imp.substring(imp.length()-2,imp.length()));

	   //EIGlobal.mensajePorTrace("MTC_Importar - formateaImp(): final: "+imp, EIGlobal.NivelLog.INFO);

	   return formateado.toString();
	}
}