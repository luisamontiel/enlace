package mx.altec.enlace.bo;

import java.util.Vector;
import java.io.IOException;
import java.io.File;
import java.security.*;
import java.io.RandomAccessFile;
import java.util.*; // LMM 2003 12 10

import mx.altec.enlace.utilerias.EIGlobal;

//####################################################################
//
//	Historial de Correcciones
//	9 Dic 2003
//	 El llamado a Procesar cambia el tipo de dato que recibe
//	 antes era String ahora es String Buffer
//	Proyecto: Mantenimiento Praxis
//	Por Lorenzo Muci�o Mendoza
//
//
//  1 Mar 2004
//  La logica para obtener el total del tama�o del archivo(s)
//	Proyecto: Mantenimiento Praxis
//	Por Carlos A. Maya Ramirez
//
//####################################################################

public class SuaVerificaDisco
{
	final String CDMOD="chmod 666 ";
	private static  int   suaSizeRegister = 295;
	private String        archivos[];
	private String        archivoFinal = "";

	public	static String 		  ErroresMostrar;
	public  boolean       suaError;
	public  SuaCheckSum   scsVerifica;
	public  SuaRegistro   scsRegistro;
	public  SuaImpuesto   scsImpuesto;
	public  SuaAnalizador scsAnalist;
	public  Vector        diasNoHabiles;

	public SuaVerificaDisco(String paramArchivos[], String paramArchivoFinal, Vector paramDias)
	{
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & SuaVerificaDisco &", EIGlobal.NivelLog.INFO);

		archivos = new String[paramArchivos.length];
		archivos = paramArchivos;
		diasNoHabiles = paramDias;
		archivoFinal  = paramArchivoFinal;

		suaError     = false;
		scsVerifica  = new SuaCheckSum();
		scsRegistro  = new SuaRegistro();
		scsImpuesto  = new SuaImpuesto();
		scsAnalist   = new SuaAnalizador(diasNoHabiles);

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & Termina SuaVerificaDisco &", EIGlobal.NivelLog.INFO);
	}

	public boolean Procesar()
	{
		SuaErrorDisco seError;
		int           indice;
		long          jndice;
		long          registros;
		long          tamanioArchivo = 0;
		byte          datos[];
		byte          retorno  = 13;
		byte          arrastre = 10;

		// Carlos Maya Ramirez (CMR) IM411142 01/03/2004
  		// Agregar variable para almacenar el tama�o total de los Discos
		long          totalTamanioDiscos = 0;
		int 		  numArchivos=0;
		StringBuffer strRegistro = new StringBuffer();


		Date fech = new Date();															// LMM 2003 12 10 Refrescar version en WebServer
		GregorianCalendar cal = new GregorianCalendar();		// LMM 2003 12 10 Refrescar version en WebServer

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & SuaVerificaDisco.Procesar() &", EIGlobal.NivelLog.INFO);

		suaError = false;
		seError  = new SuaErrorDisco();
		datos    = new byte[suaSizeRegister];
		SuaErrorDisco.bBanderaErrores=true;

		scsRegistro.archivoFinal = archivoFinal;

		try
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & Creando archivofinal &", EIGlobal.NivelLog.INFO);
			File drvFinalSua = new File(archivoFinal);

			//-------------------
			  RandomAccessFile fileFinalSua = new RandomAccessFile(drvFinalSua, "rw");
			  fileFinalSua.setLength(0); //para truncar el archivo en caso que exista
			//-------------------

			numArchivos=archivos.length;
			for(indice=0; indice<numArchivos; indice++)
			{
				try
				{
					// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & Leyendo Archivo : " + archivos[indice] + " &", EIGlobal.NivelLog.INFO);
					File drvSua = new File(archivos[indice]);
					RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
					tamanioArchivo = fileSua.length();

				// Carlos Maya Ramirez (CMR) IM411142 01/03/2004
  				// Sumar el tama�o del disco actual al Total de Disco
					totalTamanioDiscos += tamanioArchivo;

					if( (tamanioArchivo % suaSizeRegister)!=0 )
					{
						suaError = true;
						seError.asignaError(201, 0, archivos[indice] , false, scsAnalist.suaErrores);
						//scsAnalist.suaErrores.addElement(seError);
						return suaError;
					}
					registros = tamanioArchivo / suaSizeRegister;
					// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & Registros: " + Long.toString(registros) + " &", EIGlobal.NivelLog.INFO);

					if( archivos[indice].length() >= 12)
						scsRegistro.NombreArchivo = archivos[indice].substring(archivos[indice].length()-12);
					else
						scsRegistro.NombreArchivo = archivos[indice];

					// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & Recorriendo registros &", EIGlobal.NivelLog.INFO);

					for(jndice=0; jndice<registros; jndice++)
					{
						//proceso real
						EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # leyendo registro No : " + jndice + " #", EIGlobal.NivelLog.DEBUG);
						fileSua.read(datos, 0, suaSizeRegister );

  					//-------------------
						// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # Escribiendo registro #", 5);
						fileFinalSua.write(datos,0, suaSizeRegister );

						fileFinalSua.writeByte(retorno);
						fileFinalSua.writeByte(arrastre);
					//-------------------


						cal.setTime(fech);

					//String strRegistro = new String(datos);  // Antes String... ahora StringBuffer

//////////////						StringBuffer strRegistro = new StringBuffer(new String(datos)  );  // Antes String... ahora StringBuffer LMM 9 Dic 2003
						strRegistro.append(new String(datos));

						try {
							// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # strRegistro : " + strRegistro + " #", 5);

							// proceso 1
							// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # extraer registro #", 5);
							scsRegistro.extraerRegistro((String) strRegistro.toString () ); // Antes solo scsRegistro.extraerRegistro(strRegistro.toString () )

							// proceso 2
							// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # calculando impuesto #", 5);
							scsImpuesto.impuestoRegistro(scsRegistro);

							// proceso 3
	                        // -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # evaluando registro #", 5);
	                        if (!scsAnalist.evaluaRegistro(scsRegistro, scsImpuesto)){
	                           // proceso 4
	                           // -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # procesando checksum #", 5);
	                           scsVerifica.aplicarCheckSum((String) strRegistro.toString() ); // Antes String... ahora StringBuffer LMM 9 Dic 2003
	                           if(scsVerifica.enProceso==false)
	                        	   break;
	                        } else {
	                        	// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class # procesando checksum #", 5);
	                            scsVerifica.aplicarCheckSum((String) strRegistro.toString() ); // Antes String... ahora StringBuffer LMM 9 Dic 2003
	                           indice = numArchivos;
	                           break;
	                        }
						}
						catch(SUAException e) {
							break;
						}

						// Limpiando strRegistro
						strRegistro.delete(0, strRegistro.length());
					}
					fileSua.close();
					if(scsVerifica.enProceso==false)
					{
						if(indice!=(archivos.length-1))
						{
							seError.asignaError(1009, 0, archivos[indice] , false, scsAnalist.suaErrores);
							//scsAnalist.suaErrores.addElement(seError);
							suaError = true;
						}
						break;
					}
				} catch(IOException ioeSua ) {
					seError.asignaError(202, 0, archivos[indice] , false, scsAnalist.suaErrores);
					//scsAnalist.suaErrores.addElement(seError);
					// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class Excepcion %Procesar() >> "+ ioeSua.getMessage() + " <<", EIGlobal.NivelLog.INFO);
					break;
				}
			}
			//-------------------
			  fileFinalSua.close();
			//-------------------

			Runtime runt = Runtime.getRuntime();
			StringBuffer comando = new StringBuffer (CDMOD);
			comando.append(archivoFinal);
			Process proc = runt.exec( comando.toString() );

			/**
			 * <VC autor="GGB" fecha="18/09/2007"
			 * 		descripcion="Cerrar expl�citamente los Stream que tiene el objeto Process">
			 */
			try {
				proc.getErrorStream().close();
			} catch (Exception e) {
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace("SuaVerificaDisco.Procesar -> Mensaje: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			try {
				proc.getInputStream().close();
			} catch (Exception e) {
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace("SuaVerificaDisco.Procesar -> Mensaje: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			try {
				proc.getOutputStream().close();
			} catch (Exception e) {
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace("SuaVerificaDisco.Procesar -> Mensaje: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			/** </VC> */
			proc.destroy();
			proc = null;

			System.out.println("COMANDO : ".concat(comando.toString()));
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & COMANDO : " + comando + " &", EIGlobal.NivelLog.INFO);

		} catch(Exception ioeSua ) {
				seError.asignaError(202, 0, archivoFinal , false, scsAnalist.suaErrores);
				//scsAnalist.suaErrores.addElement(seError);
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class Excepcion %Procesar() >> "+ ioeSua.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & evaluando archivo &", EIGlobal.NivelLog.INFO);

		// proceso 5
		try {
			// Carlos Maya Ramirez (CMR) IM411142 01/03/2004
			//scsAnalist.evaluaArchivo(scsRegistro, scsImpuesto, tamanioArchivo, archivos.length);
			scsAnalist.evaluaArchivo(scsRegistro, scsImpuesto, totalTamanioDiscos, archivos.length);
		} catch(Exception E) {
			seError.asignaError(202, 0, archivos[0] , false, scsAnalist.suaErrores);
			//scsAnalist.suaErrores.addElement(seError);
			suaError = true;
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class Excepcion %Procesar() >> "+ E.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & termina evaluando archivo &", EIGlobal.NivelLog.INFO);

		if( scsVerifica.SuaCheckReturn == false )
		{
			seError.asignaError(1001, 0, archivos[0] , false, scsAnalist.suaErrores);
			//scsAnalist.suaErrores.addElement(seError);
			suaError = true;
		}
		if(!scsAnalist.suaProceder)
		{
			suaError = true;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaVerificaDisco.class & Termina SuaVerificaDisco.Procesar() &", EIGlobal.NivelLog.INFO);
		return suaError;
	}

	public String LeerErrores()
	{
		int    indice;
		int    cuantos;
		String lista;

		lista = "";
//		cuantos = scsAnalist.suaErrores.size();
//		for(indice=0; indice<cuantos; indice++)
//		{
//			SuaErrorDisco porLeer = (SuaErrorDisco)scsAnalist.suaErrores.elementAt(indice);
//			String mensaje = SuaErrorDisco.msgError( porLeer.error);
//			lista = lista + "<OPTION VALUE=\""  + mensaje  + "\"> "  +  " </OPTION>\n";
//		}
		String mensaje = ErroresMostrar;
		lista = lista + "<OPTION VALUE=\""  + mensaje  + "\"> "  +  " </OPTION>\n";
		return(lista);
	}
}