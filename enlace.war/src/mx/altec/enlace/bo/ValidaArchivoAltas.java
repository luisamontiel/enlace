package mx.altec.enlace.bo;

import static mx.altec.enlace.beans.NomPreRemesa.LNG_NO_REMESA;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.Global.NP_MQ_USUARIO;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.NomPreAlta;
import mx.altec.enlace.beans.NomPreBuilder;
import mx.altec.enlace.beans.NomPreCodPost;
import mx.altec.enlace.beans.NomPreLM1A;
import mx.altec.enlace.beans.NomPreLM1B;
import mx.altec.enlace.beans.NomPreRemesa;
import mx.altec.enlace.beans.NomPreTarjeta;
import mx.altec.enlace.bo.CatNominaConstantes;
import mx.altec.enlace.bo.CatNominaHTML;
import mx.altec.enlace.dao.NomPreEmpleadoDAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;


public class ValidaArchivoAltas{

	/**
	 *
	 */
	private static final long serialVersionUID = -1461724805348071294L;

	private static final archivoEmpleados validaciones = new archivoEmpleados("usovalidaciones", "");

	/**************************************************************************************/
	/** Metodo: validaLineaAlta
	 * @param String, int
	 * @description: Hace la validaci�n del tamano de la linea del archivo
	/*************************************************************************************/
	public NomPreAlta validaArchivoAlta(String archivoCargado, String numContrato, HttpServletRequest request) throws IOException
	{
		EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - validaArchivoAlta $ Inicio", EIGlobal.NivelLog.INFO);
		NomPreAlta bean = new NomPreAlta();

		String linea = "";
		ArrayList<Object> listaErrorCuentas = new ArrayList<Object>();
		ArrayList<Object> listaErrorLong = new ArrayList<Object>();
		ArrayList<Object> listaErrorDatos = new ArrayList<Object>();
		ArrayList<Object> listaErrores = new ArrayList<Object>();
		ArrayList<Object> listaRegistros = new ArrayList<Object>();
		ArrayList<Object> listaErrorCampo = new ArrayList<Object>();
		ArrayList<Object> listaErrorCuenta = new ArrayList<Object>();
		ArrayList<Object> listaErrorSantander = new ArrayList<Object>(); //Cambio para no permitir interbancaria Santander

		String registroLeido = "";

		String sentido = "";
		String fechaArchivo = "";
		String descContrato = "";
		String secuenciaArchivo = "";
		boolean encabezado = false, registro = false, sumario = false, bandera = false, tarjetaValida = false;

		int tipoRegisro;
		String secuenciaRegistros = "";
		String secuenciaRegistrosaux = "";
		String contrato = "";
		String num_tarjeta = "";
		String paterno = "";
		String materno = "";
		String nombre = "";
		String rfc = "";
		String num_empleado = "";
		String homoclave = "";
		String numID = ""; //VSWF ESC Nuevos campos NP
		String tipoID = ""; //VSWF ESC Nuevos campos NP
		String nacimiento = "";
		String sexo = "";
		String nacionalidad = "";
		String pais = ""; //VSWF ESC Nuevos campos NP
		String edo_civil = "";
		String calle = "";
		String numero = "";
		String numInt = ""; //VSWF ESC Nuevos campos NP
		String colonia = "";
		String delegacion = "";
		String ciudad = "";
		String estado = "";
		String cod_postal = "";
		String prefijo_tel = "";
		String telefono = "";
		String correo = "";
		String totalSumario="";
		long posicion = 0;
		int existeRelacion = 0;

		RandomAccessFile archivoAlta;

		int i = 0;

		try {
			archivoAlta = new RandomAccessFile(archivoCargado, "rw");

			listaErrorLong.add("Long");
			listaErrorCuentas.add("Cuentas");
			listaErrorDatos.add("Datos");
			listaErrorCampo.add("Campo");
			listaErrorCuenta.add("Cuenta Abono");
			listaErrorSantander.add("CodBancario");//Cambio para no permitir interbancaria Santander


			if (archivoAlta.length() > 0) {
				archivoAlta.seek(0);

//				Lee registro de encabezado
				try{
				registroLeido = archivoAlta.readLine();
				if(registroLeido.length() == 15)
				{
					if(validaCampoNumero(registroLeido.substring(0,1).trim()))
					{
						tipoRegisro = Integer.parseInt(registroLeido.substring(0,1).trim());
						if(tipoRegisro==1)
						{
							secuenciaRegistros = registroLeido.substring(1,6).trim();
							if(secuenciaRegistros.equals("00001"))
							{
								sentido = registroLeido.substring(6,7).trim();
								if(sentido.equals("E"))
								{
									fechaArchivo = registroLeido.substring(7,15);
									if(validaFecha(fechaArchivo))
									{
										encabezado = true;
										EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Registro encabezado correcto", EIGlobal.NivelLog.INFO);
									}
									else
									{
										EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Error en la fecha del encabezado", EIGlobal.NivelLog.ERROR);
										encabezado = false;
									}
								}
								else
								{
									EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Error en el sentido del archivo", EIGlobal.NivelLog.ERROR);
									encabezado = false;
								}
							}
							else
							{
								EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Error en el numero de secuencia", EIGlobal.NivelLog.ERROR);
								encabezado = false;
							}
						}
						else
						{
							EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Error en el tipo de registro", EIGlobal.NivelLog.ERROR);
							encabezado = false;
						}
					}
					else
					{
						EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Error en el tipo de registro", EIGlobal.NivelLog.ERROR);
						encabezado = false;
					}
				}
				else
				{
					EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - validaArchivoAlta $ Longitud Incorrecta", EIGlobal.NivelLog.INFO);
					listaErrorLong.add((i+1)+" encabezado");
					encabezado = false;
				}
				if(!encabezado)
					listaErrorDatos.add((i+1) + " encabezado");
				}catch(Exception e)
				{
					listaErrorDatos.add((i+1) + " encabezado");
				}

				HttpSession sess = request.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");
				contrato = session.getContractNumber();
				NomPreLM1A lm1a = new NomPreRemesaAction().consultarRemesas(contrato);
				List<String> tarjetasRecibidas = new ArrayList<String>();

				if (lm1a != null && lm1a.getDetalle() != null && lm1a.getDetalle().size() > 0) {

					Iterator<NomPreRemesa> it = lm1a.getDetalle().iterator();

					MQQueueSession mqQS = null;

					try {

						mqQS = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,  NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);

						while (it.hasNext())  {

							NomPreRemesa remesa = it.next();

							EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - LM1A: Obteniendo tarjetas de la remesa " + remesa.getNoRemesa(), EIGlobal.NivelLog.INFO);

							String paginacion = "";
							NomPreLM1B lm1bPaginacion = null;

							do {

								String respuesta = null;

								StringBuffer sb = new StringBuffer()
								.append(rellenar(contrato, 11)) 			//NUM-CONTRATO
								.append(rellenar(remesa.getNoRemesa(), LNG_NO_REMESA))	//NUM-REMESA
								.append(rellenar(paginacion, 22));

								StringBuffer trama = new StringBuffer()
								.append(rellenar(NP_MQ_TERMINAL, 4))
								.append(rellenar(NP_MQ_USUARIO, 8))
								.append(NomPreLM1B.HEADER)
								.append(sb.toString());

								respuesta = mqQS.enviaRecibeMensaje(trama.toString());

								if (respuesta != null && respuesta.length() > 0) {
									lm1bPaginacion =  build(NomPreLM1B.getFactoryInstance(), respuesta);
								} else {
									EIGlobal.mensajePorTrace("Sin trama de respuesta", EIGlobal.NivelLog.INFO);
								}

								if (lm1bPaginacion == null) break;

								if (lm1bPaginacion.getDetalle() != null && lm1bPaginacion.getDetalle().size() > 0) {

									for (NomPreTarjeta tarjeta : lm1bPaginacion.getDetalle()) {

										if (tarjeta.getNoTarjeta() != null && tarjeta.getEstadoTarjeta() != null && "2".equals(tarjeta.getEstadoTarjeta().trim())) {

											tarjetasRecibidas.add(tarjeta.getNoTarjeta().trim());
										}
									}
								}

								paginacion = lm1bPaginacion.getNoTarjetaPag();

							} while (paginacion != null && paginacion.length() > 0);
						}

					} catch (Exception e) {

						e.printStackTrace();

					} finally {

						if (mqQS != null) {
							mqQS.close();
							mqQS = null;
						}
					}

					EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - LM1B: Se encontraron " + tarjetasRecibidas.size() + " tarjetas recibidas", EIGlobal.NivelLog.INFO);

				} else {

					EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - LM1A: No se encontraron remesas para el contrato " + contrato, EIGlobal.NivelLog.INFO);
				}


				do {
					i++;
					registroLeido = archivoAlta.readLine();
					secuenciaRegistrosaux = "";
					bandera = false;
					//int x = registroLeido.length();
					//System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Longitud Correcta ," +  x);
					//System.out.println("\nCatNominaValidaInterb - validaArchivoAlta $ Longitud Correcta ," +  registroLeido);

					//Lee registros del cuerpo del archivo
					try{
					if(registroLeido.length() == 444)
					{
						if(validaCampoNumero(registroLeido.substring(0,1).trim().toUpperCase()))
						{
							tipoRegisro = Integer.parseInt(registroLeido.substring(0,1).trim().toUpperCase());
							if(tipoRegisro==2)
							{
								secuenciaRegistros = registroLeido.substring(1,6).trim().toUpperCase();
								for(int j=0; j<secuenciaRegistros.length(); j++)
								{
									//System.out.println(secuenciaRegistrosaux.length());
									if(secuenciaRegistros.charAt(j)!='0' || secuenciaRegistrosaux.length()>0)
										secuenciaRegistrosaux+=secuenciaRegistros.charAt(j);
								}
								//System.out.println(secuenciaRegistrosaux + "-compara-" + i);
								if(validaCampoNumero(secuenciaRegistrosaux))
								{
									if(Integer.parseInt(secuenciaRegistrosaux)==i+1)
									{

										if(contrato.length()==11 && validaCampoNumero(contrato) && contrato.equals(numContrato))
										{
											//"ESC Verificar como obtener la descripcion" descContrato = registroLeido.substring(17,77).trim().toUpperCase();
											num_tarjeta = registroLeido.substring(428,444).toUpperCase(); //VSWF ESC Nuevos campos NP
											if(validaCampoNumero(num_tarjeta.trim()) && num_tarjeta.trim().length()>=16)
											{
												String num_tarjeta_aux = "";
												for(int j=0; j<num_tarjeta.trim().length(); j++)
												{
													//System.out.println(secuenciaRegistrosaux.length());
													if(num_tarjeta.charAt(j)!='0' || num_tarjeta_aux.length()>0)
														num_tarjeta_aux+=num_tarjeta.charAt(j);
												}
												//NomPreLM1D lm1d = new NomPreBusquedaAction().obtenRelacionTarjetaEmpleado(contrato, num_tarjeta_aux, null, null);

												tarjetaValida = (num_tarjeta_aux != null && tarjetasRecibidas.contains(num_tarjeta_aux.trim()));

												if(tarjetaValida)
												{
													num_empleado = registroLeido.substring(6,13).trim().toUpperCase();
													cod_postal = registroLeido.substring(350,355).trim().toUpperCase();

													//Inicio VSWF ESC Nuevos campos NP
													NomPreEmpleadoDAO empleadoDAO=new NomPreEmpleadoDAO();
													NomPreCodPost npcodpostal = empleadoDAO.consultarCodigoPostal(cod_postal);

													if (cod_postal != null && cod_postal.trim().length() > 0 && validaCampoNumero(cod_postal) && npcodpostal.isCodExito()) {

														if(validaCampoNumero(num_tarjeta) && validaCampoNumero(num_empleado) && validaCampoNumero(cod_postal))
														{
															paterno = registroLeido.substring(13,43).trim().toUpperCase();
															materno = registroLeido.substring(43,63).trim().toUpperCase();
															nombre = registroLeido.substring(63,93).trim().toUpperCase();
															rfc = registroLeido.substring(93,103).trim().toUpperCase();
															numID = registroLeido.substring(106,131).trim().toUpperCase();
															sexo = registroLeido.substring(131,132).trim().toUpperCase();
															nacimiento = registroLeido.substring(132,140).trim().toUpperCase();
															nacionalidad = registroLeido.substring(140,144).trim().toUpperCase();
															pais = registroLeido.substring(144,169).trim().toUpperCase();
															edo_civil = registroLeido.substring(169,170).trim().toUpperCase();
															calle = registroLeido.substring(170,220).trim().toUpperCase();
															numero = registroLeido.substring(220,230).trim().toUpperCase();
															numInt = registroLeido.substring(230,240).trim().toUpperCase();
															colonia = registroLeido.substring(240,270).trim().toUpperCase();
															delegacion = registroLeido.substring(270,300).trim().toUpperCase();
															ciudad = registroLeido.substring(300,320).trim().toUpperCase();
															estado = registroLeido.substring(320,350).trim().toUpperCase();
															prefijo_tel = registroLeido.substring(355,360).trim().toUpperCase();
															telefono = registroLeido.substring(360,368).trim().toUpperCase();
															correo = registroLeido.substring(368,428).trim();
															homoclave = registroLeido.substring(103,106).trim().toUpperCase();

																if(validaFecha(nacimiento))
																{

																		if (validaCampoTexto(paterno) && validaCampoTexto(materno) && validaCampoTexto(nombre) && validaCampoTexto(rfc) &&
																				validaCampoTexto(nacimiento) && validaCampoTexto(sexo) && validaCampoTexto(nacionalidad) &&
																				validaCampoTexto(edo_civil) && validaCampoTexto(calle) && validaCampoTexto(colonia) &&
																				validaCampoTexto(delegacion) && validaCampoTexto(ciudad) && validaCampoTexto(pais) &&
																				validaCampoTexto(numero) && validaCampoTexto(numID) && validaCampoTexto(estado) &&
																				validaCampoTexto(numInt) && validaCampoTexto(homoclave)/* && validaCampoTexto(tipoID)*/) {

																			if(paterno.trim().equals("") || materno.trim().equals("") || nombre.trim().equals("") || rfc.trim().equals("") ||
																					nacimiento.trim().equals("") || sexo.trim().equals("") || nacionalidad.trim().equals("") || edo_civil.trim().equals("") ||
																					calle.trim().equals("") || colonia.trim().equals("") || delegacion.trim().equals("") ||
																					ciudad.trim().equals("") || numID.trim().equals("") || pais.trim().equals("") ||
																					estado.trim().equals("") || numero.trim().equals("")
																					/*|| homoclave.trim().equals("") || tipoID.trim().equals("")*/)
																				//Fin VSWF ESC Nuevos campos NP
																			{
																				EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  validaArchivoAlta $ Datos Obligatotios ", EIGlobal.NivelLog.INFO);
																				listaErrorDatos.add("Campo obligatorio en " + (i+1));
																				EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  Faltan campos obligatorios para el registro numero "+i+1, EIGlobal.NivelLog.INFO);
																				registro = false;
																				bandera = true;
																			}
																			else
																			{

																				//CGH Validacion de RFC
																				boolean rfcValido = false;
																				boolean numerico = false;
																				String mensajeRfc = "";

																				if (rfc.length() > 0 && rfc.length() == 10) {
																					String tmpRfc = rfc.substring(0, 4);
																					for (int crfc = 0; crfc < tmpRfc.length(); crfc ++) {
																			            if (Character.isDigit(tmpRfc.charAt(crfc))) {
																			            	numerico=true;
																			            	break;
																			            }
																			        }
																				}

																				if ("".equals(rfc) || rfc.length() == 0) {
																					mensajeRfc = "RFC requerido";
																				} else if(rfc.length() != 10) {
																					mensajeRfc = "RFC longitud incorrecta";
																				} else if (numerico) {
																					mensajeRfc = "RFC caracteres no v&aacute;lidos";
																				} else if(!validaciones.vFecha(rfc.substring(4),"aammdd")) {
																					mensajeRfc = "RFC fecha no corresponde al formato aa/mm/dd";
																				}  else if (!validaciones.vRFC(paterno, materno, nombre, rfc.substring(0,4))) {
																					mensajeRfc = "RFC no corresponde con el nombre";
																				} else {
																					rfcValido = true;
																				}
																				//

																				if(rfcValido) {

																					String fenaci = nacimiento.substring(6,8).trim() + nacimiento.substring(2,4).trim() + nacimiento.substring(0,2).trim();

																					EIGlobal.mensajePorTrace("Validacion de fecha de nacimiento: " + fenaci + "-" + rfc.substring(4), EIGlobal.NivelLog.INFO);

																					if (fenaci.equals(rfc.substring(4))) {

																							if( (sexo.trim().equals("F") || sexo.trim().equals("M")) && (nacionalidad.trim().equals("MEXI") || nacionalidad.trim().equals("USA") || nacionalidad.trim().equals("CANA") || nacionalidad.trim().equals("ESPA") || nacionalidad.trim().equals("FRA")) && (edo_civil.trim().equals("S") || edo_civil.trim().equals("C") || edo_civil.trim().equals("V") || edo_civil.trim().equals("U")))
																							{
																								//VSWF ESC Nuevos campos NP
																								if("".equals(correo) || (correo.length() > 0 && correo.indexOf("@")>=0 && correo.indexOf(".", correo.indexOf("@"))>=0)) {

																									registro = true;
																									listaRegistros.add(registroLeido);
																									EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Registro detalle correcto", EIGlobal.NivelLog.INFO);

																								} else {

																									EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - El correo no es v&aacute;lido ", EIGlobal.NivelLog.INFO);
																									listaErrorDatos.add((i+1)+" Correo no v&aacute;lido");
																									registro = false;
																									bandera = true;
																								}
																							}
																							else
																							{
																								EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  Los caracteres estan mal ", EIGlobal.NivelLog.INFO);
																								listaErrorDatos.add((i+1)+" detalle");
																								registro = false;
																								bandera = true;
																							}

																					} else {
																						EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  La fecha de nacimiento no concuerda con el RFC", EIGlobal.NivelLog.INFO);
																						listaErrorDatos.add((i+1)+" La fecha de nacimiento no concuerda con el RFC");
																						registro = false;
																						bandera = true;
																					}

																				} else {

																					EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  El RFC no corresponde con los datos dados en el nombre y la fecha de nacimiento", EIGlobal.NivelLog.INFO);
																					registro = false;
																					listaErrorDatos.add((i+1) + " " + mensajeRfc);
																					bandera = true;
																				}

																			}
																		}
																		else
																		{
																			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  La comprobacion de texto fallo ", EIGlobal.NivelLog.INFO);
																			listaErrorDatos.add((i+1)+" Campos informados incorrectamente");
																			registro = false;
																			bandera = true;
																		}
																}
																else
																{
																	EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  Error en el formato de fecha ", EIGlobal.NivelLog.INFO);
																	listaErrorDatos.add((i+1)+" Fecha incorrecta");
																	registro = false;
																	bandera = true;
																}

														}
														else
														{
															registro = false;
															listaErrorDatos.add((i+1)+" detalle");
															bandera = true;
														}
													} else {

														registro = false;
														listaErrorDatos.add((i+1)+" C&oacute;digo postal requerido");
														bandera = true;
													}
												}
												else
												{
													EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  No existe la tarjeta en ese contrato o esta en estado invalido", EIGlobal.NivelLog.INFO);
													registro = false;
													listaErrorDatos.add((i+1)+" Tarjeta inexistente o no recibida");
													bandera = true;
												}
											}
											else
											{
												EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  Error en la longitud del numero de Tarjeta", EIGlobal.NivelLog.INFO);
												registro = false;
												listaErrorDatos.add((i+1)+" Longitud tarjeta incorrecto");
												bandera = true;
											}
										}
										else
										{
											EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  Error en la longitud del numero de contrato", EIGlobal.NivelLog.INFO);
											registro = false;
											listaErrorDatos.add((i+1)+" Contrato incorrecto");
											bandera = true;
										}
									}
									else
									{
										EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  La numeracion de la secuencia de los registros no es correcta", EIGlobal.NivelLog.INFO);
										registro = false;
										listaErrorDatos.add((i+1)+" Secuencia incorrecta");
										bandera = true;
									}
								}
								else
								{
									EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - La numeracion de la secuencia de los registros no es correcta", EIGlobal.NivelLog.INFO);
									registro = false;
									listaErrorDatos.add((i+1)+" Secuencia incorrecta");
									bandera = true;
								}
							}
							else
							{
								EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -No es correcto el tipo de registro", EIGlobal.NivelLog.INFO);
								registro = false;
								listaErrorDatos.add((i+1)+" Tipo registro incorrecto");
								bandera = true;
							}
						}
						else
						{
							EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -No es correcto el tipo de registro", EIGlobal.NivelLog.INFO);
							registro = false;
							listaErrorDatos.add((i+1)+" Tipo registro incorrecto");
							bandera = true;
						}
					}
					else
					{
						//Lee el registro sumario
						if(registroLeido.trim().length()==11 && i>1)
						{
							if(validaCampoNumero(registroLeido.substring(0,1).trim()))
							{
								tipoRegisro = Integer.parseInt(registroLeido.substring(0,1).trim());
								if(tipoRegisro==3)
								{
									secuenciaRegistros = registroLeido.substring(1,6).trim();
									secuenciaRegistrosaux = "";
									for(int j=0; j<secuenciaRegistros.length(); j++)
									{
										if(secuenciaRegistros.charAt(j)!='0' || secuenciaRegistrosaux.length()>0)
											secuenciaRegistrosaux+=secuenciaRegistros.charAt(j);
									}
									//System.out.println(secuenciaRegistrosaux + "compara" + (i+1));
									if(validaCampoNumero(secuenciaRegistrosaux))
									{
										if(Integer.parseInt(secuenciaRegistrosaux) == (i+1))
										{
											totalSumario = registroLeido.substring(6,registroLeido.length()).trim();
											secuenciaRegistrosaux = "";
											for(int j=0; j<totalSumario.length(); j++)
											{
												if(totalSumario.charAt(j)!='0' || secuenciaRegistrosaux.length()>0)
													secuenciaRegistrosaux+=totalSumario.charAt(j);
											}
											if(validaCampoNumero(secuenciaRegistrosaux))
											{
												if(Integer.parseInt(secuenciaRegistrosaux) == i-1)
												{
													sumario = true;
													EIGlobal.mensajePorTrace("sumario correcto", EIGlobal.NivelLog.INFO);
													break;
												}
												else
												{
													listaErrorDatos.add((i+1)+" sumario");
													sumario = false;
													EIGlobal.mensajePorTrace("no coinciden los registros"+ (i-1), EIGlobal.NivelLog.INFO);
													break;
												}
											}
											else
											{
												listaErrorDatos.add((i+1)+" sumario");
												sumario = false;
												EIGlobal.mensajePorTrace("no coinciden los registros"+ (i-1), EIGlobal.NivelLog.INFO);
												break;
											}
										}
										else
										{
											listaErrorDatos.add((i+1)+" sumario");
											sumario = false;
											EIGlobal.mensajePorTrace("error numero de secuencia", EIGlobal.NivelLog.INFO);
											break;
										}
									}
									else
									{
										listaErrorDatos.add((i+1)+" sumario");
										sumario = false;
										EIGlobal.mensajePorTrace("error numero de secuencia", EIGlobal.NivelLog.INFO);
										break;
									}
								}
								else
								{
									listaErrorDatos.add((i+1)+" sumario");
									sumario = false;
									EIGlobal.mensajePorTrace("tipo de registro", EIGlobal.NivelLog.INFO);
									break;
								}
							}
							else
							{
								listaErrorDatos.add((i+1)+" sumario");
								sumario = false;
								EIGlobal.mensajePorTrace("tipo de registro", EIGlobal.NivelLog.INFO);
								break;
							}
						}
						else
						{
							sumario = false;
							registro = false;
							EIGlobal.mensajePorTrace("longitud", EIGlobal.NivelLog.INFO);
						}
					}
					if(!registro && !bandera)
						listaErrorLong.add((i+1)+" detalle");

				} catch(Exception e) {
						listaErrorDatos.add((i+2)+" detalle");
				}
				//VSWF JPH correccion inc
				posicion = archivoAlta.getFilePointer();
				} while (posicion < archivoAlta.length());
				if(!registro && !sumario)
					listaErrorLong.remove(listaErrorLong.size()-1);
				if(i==1)
					listaErrorDatos.add("No existen registros en el detalle.");
				if(!sumario)
					listaErrorLong.add((i+1) + " sumario");
				i=i-1;
				bean.setTotalRegistros(i);
				bean.setListaRegistros(listaRegistros);
			}
			else
			{
				listaErrorLong.add(" El archivo est&aacute; vacio. ");
			}
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class -  validaLineaAlta $ Longitud " + listaErrorLong.size(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - validaLineaAlta $ Datos " + listaErrorDatos.size(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - validaLineaAlta $ Cuentas " + listaErrorCuentas.size(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - validaLineaAlta $ Campo " + listaErrorCampo.size(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - validaLineaAlta $ Cuenta Abono " + listaErrorCuenta.size(), EIGlobal.NivelLog.INFO);

			archivoAlta.close();
		} catch (Exception e) {
			e.printStackTrace();
			bean.setFalloInesperado(true);
		}

		//System.out.println("CatNominaValidaImport - validaLineaAlta $ errores " + linea);
		/*
		for(int k = 2; k <= listaErrorLong.size(); k++)
		{
			if(listaErrorLong.get(k).toString().substring(0,listaErrorLong.get(k).toString().length()-7).trim().equals(listaErrorLong.get(k-1).toString().substring(0,listaErrorLong.get(k-1).toString().length()-7).trim()))
			{
				System.out.println("Se elimino el registro :" + listaErrorLong.get(k-1));
				listaErrorLong.remove(k-1);
			}
		}
		*/
		if(listaErrorLong.size() > 1)
		{
			listaErrores = listaErrorLong;
		}
		else if(listaErrorSantander.size() > 1)
		{
			listaErrores = listaErrorSantander;
		}
		else if(listaErrorDatos.size() > 1)
		{
			listaErrores = listaErrorDatos;
		}
		else if(listaErrorCuentas.size() > 1)
		{
			listaErrores = listaErrorCuentas;
		}
		else if(listaErrorCampo.size() > 1)
		{
			listaErrores = listaErrorCampo;
		}
		else if (listaErrorCuenta.size() > 1)
		{
			listaErrores = listaErrorCuenta;
		}
		bean.setListaErrores(listaErrores);
		return bean;
	}


	private <T> T build(NomPreBuilder<T> builder, String arg) {
		return builder.build(arg);
	}






	//******MODIFICA ARCHIVO INTERB****************

	/**************************************************************************************/
	/** Metodo: validaLineaModif
	 * @param String, int
	 * @description: Hace la validaci�n del tamano de la linea del archivo
	 * 				 verifica que el tamano de la linea sea solo de 11 � 18 numeros
	/*************************************************************************************/
	/*
	public CatNominaBean validaArchModifInterb(String archivoCargado, String numContrato, HttpServletRequest request) throws IOException
	{
		System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Inicio ");
		CatNominaBean bean = new CatNominaBean();
		String linea = "";
		ArrayList listaErrorCuentas = new ArrayList();
		ArrayList listaErrorLong = new ArrayList();
		ArrayList listaErrorDatos = new ArrayList();
		ArrayList listaErrores = new ArrayList();
		ArrayList listaRegistros = new ArrayList();
		ArrayList listaErrorCampo = new ArrayList();
		ArrayList listaErrorCuenta = new ArrayList();
		ArrayList listaErrorCtaInterna = new ArrayList();

		//VALIDACION DE EXISTENCIA DE RELACION CON UNA CUENTA INTERNA
		String[] arrDatosActual = new String[6];
		RandomAccessFile archivoModif;
		int i = 0;

		try {
			archivoModif = new RandomAccessFile(archivoCargado, "rw");
			String registroLeido = "";
			String numCta = "";
			String numEmpl = "";
			String deptoEmpl = "";
			String ingresoMensual = "";
			String sexo = "", calle = "", colonia = "", delegacion = "", cveEstado = "", ciudad = "", codigoPostal = "", clavePais = "", prefTelPart = "", telPart = "";
			String calleOfi = "", coloniaOfi = "", delegOfi = "", cveEstadoOfi = "", ciudadOfi = "", codPostalOfi = "", paisOfi = "", cveDireccionOfi = "", prefTelOfi = "", telOfi = "", extOfi = "";

			long posicion = 0;
			int existeRelacion = 0;

			listaErrorLong.add(new String("Long"));
			listaErrorCuentas.add(new String("Cuentas"));
			listaErrorDatos.add(new String("Datos"));
			listaErrorCampo.add(new String("Campo"));
			listaErrorCtaInterna.add(new String ("Cuenta Interna"));
			listaErrorCuenta.add(new String ("Cuenta Abono"));

			if (archivoModif.length() > 0) {
				archivoModif.seek(0);
				do {
					i++;
					registroLeido = archivoModif.readLine();

					if (registroLeido.length() == 465) {
						System.out.println("CatNominaValidaInterbn - validaArchModifInterb $ Longitud Correcta ");
						//CatNominaValidaImport valida = new CatNominaValidaImport();
						numEmpl = registroLeido.substring(18, 25).trim();
						deptoEmpl = registroLeido.substring(25, 31).trim();
						ingresoMensual = registroLeido.substring(31, 51).trim();
						sexo = registroLeido.substring(51, 52).trim();
						calle = registroLeido.substring(52, 112).trim();
						colonia = registroLeido.substring(112, 142).trim();
						delegacion = registroLeido.substring(142, 177).trim();
						cveEstado = registroLeido.substring(177, 181).trim();
						ciudad = registroLeido.substring(181, 216).trim();
						codigoPostal = registroLeido.substring(216, 221).trim();
						clavePais = registroLeido.substring(221, 225).trim();
						prefTelPart = registroLeido.substring(225, 237).trim();
						telPart = registroLeido.substring(237, 249).trim();
						calleOfi = registroLeido.substring(249, 309).trim();
						coloniaOfi  = registroLeido.substring(309, 339).trim();
						delegOfi  = registroLeido.substring(339, 374).trim();
						cveEstadoOfi  = registroLeido.substring(374, 378).trim();
						ciudadOfi  = registroLeido.substring(378, 413).trim();
						codPostalOfi  = registroLeido.substring(413, 418).trim();
						paisOfi  = registroLeido.substring(418, 422).trim();
						cveDireccionOfi  = registroLeido.substring(422, 429).trim();
						prefTelOfi  = registroLeido.substring(429, 441).trim();
						telOfi  = registroLeido.substring(441, 453).trim();
						extOfi  = registroLeido.substring(453, 465).trim();

						if (validaCampoTexto(numEmpl) && validaCampoTexto(deptoEmpl) && validaCampoNumero(ingresoMensual) && validaCampoTexto(sexo) && validaCampoTexto(calle)
								&& validaCampoTexto(colonia) && validaCampoTexto(delegacion) && validaCampoTexto(cveEstado) && validaCampoTexto(ciudad) && validaCampoTexto(codigoPostal)
								&& validaCampoTexto(clavePais) && validaCampoTexto(prefTelPart) && validaCampoNumero(telPart) && validaCampoTexto(calleOfi) && validaCampoTexto(coloniaOfi) && validaCampoTexto(delegOfi) && validaCampoTexto(cveEstadoOfi)
								&& validaCampoTexto(ciudadOfi) && validaCampoTexto(codPostalOfi) && validaCampoTexto(paisOfi) && validaCampoNumero(cveDireccionOfi) && validaCampoNumero(prefTelOfi) && validaCampoNumero(telOfi) && validaCampoNumero(extOfi))
						{
							if (!numEmpl.equals(""))
							{
								if(ingresoMensual.equals("")){
										listaErrorCampo.add(new String("Salario@" + i ));
								}

								numCta = registroLeido.substring(0, 18).trim();

								if(validaCampoNumero(numCta))
								{
									System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Correcto ");
								}
								else {
									System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos incorrectos ");
									listaErrorDatos.add(new Integer(i));
								}
								int a = numCta.length();
								System.out.println("CatNominaValidaInterb - validaArchModifInterb $ LONGITUD CTA ABONO " + a);

								if (a == 18){
									System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Valido cuenta abono ");
								}
								else{
									System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Cuenta abono incorrecta ");
									listaErrorCuenta.add(new Integer(i));
								}

								System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Num Cuenta a validar = " + numCta);

								if(!numCta.equals("")){
									System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Obligatotios ");

									CatNominaDAO dao = new CatNominaDAO();
									existeRelacion = dao.existeRegistro(numContrato, numCta);

									if(existeRelacion != -100)
									{
										if(existeRelacion>0){
											System.out.println("CatNominaValidaImport - validaLineaModif $ Relaci�n Existente - Linea VALIDA " + registroLeido );
											listaRegistros.add(new String(registroLeido));


											 // VALIDACION DE EXISTENCIA DE RELACION CON UNA CUENTA INTERNA **** inicio


											arrDatosActual = dao.obtenDatosEmplModifInterb(numContrato, numCta);

											if(!arrDatosActual[0].equals("ERROR0000"))
											{
												int Detalle = dao.existeRelacionCtaInterna(arrDatosActual, numContrato, numCta);

												if(Detalle == 0){
													System.out.println("CatNominaValidaInterb - validaLineaModif $ Relaci�n Existente - Linea VALIDA " + registroLeido );
												}
												else{
													System.out.println("CatNominaValidaInterb - validaArchModifInterb $No existe relacion ");
													listaErrorCtaInterna.add(new Integer(i));
												}
											}
											else{
												bean.setFalloInesperado(true);
											}

												 //VALIDACION DE EXISTENCIA DE RELACION CON UNA CUENTA INTERNA **** fin

										}
										else{
											System.out.println("CatNominaValidaInterb - validaArchModifInterb $No existe relacion ");
											listaErrorCuentas.add(new Integer(i));
										}
									}
									else{
										bean.setFalloInesperado(true);
										}
								    }
									else{
										System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Obligatotios ");
										listaErrorCampo.add(new String("Cuenta abono@" + i));
									}

							}
							else{
								System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Obligatotios ");
							      listaErrorCampo.add(new String("Numero de Empleado@" + i ));
							}

						}
						else
						{
							System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos incorrectos ");
							listaErrorDatos.add(new Integer(i));
						}
					}
					else {
						System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Longitud INCorrecta ");
						listaErrorLong.add(new Integer(i));
					}
					posicion = archivoModif.getFilePointer();

				} while (posicion < archivoModif.length());
				bean.setTotalRegistros(i);
				bean.setListaRegistros(listaRegistros);
			}
			System.out.println("CatNominaValidaImport - validaLineaModif $ Longitud " + listaErrorLong.size());
			System.out.println("CatNominaValidaImport - validaLineaModif $ Datos " + listaErrorDatos.size());
			System.out.println("CatNominaValidaImport - validaLineaModif $ Cuentas " + listaErrorCuentas.size());

			System.out.println("CatNominaValidaImport - validaLineaModif $ Campo " + listaErrorCampo.size());

			System.out.println("CatNominaValidaImport - validaLineaModif $ Cuenta Abono " + listaErrorCuenta.size());
			System.out.println("CatNominaValidaImport - validaLineaModif $ Cuenta Interna " + listaErrorCtaInterna.size());

			archivoModif.close();
		} catch (Exception e) {
			e.printStackTrace();
			bean.setFalloInesperado(true);
		}

		System.out.println("CatNominaValidaImport - validaLineaModif $ errores " + linea);
		if(listaErrorLong.size() > 1)
		{
			listaErrores = listaErrorLong;
		}
		else if(listaErrorDatos.size() > 1)
		{
			listaErrores = listaErrorDatos;
		}
		else if(listaErrorCuentas.size() > 1)
		{
			listaErrores = listaErrorCuentas;
		}
		else if (listaErrorCampo.size() > 1)
		{
			listaErrores = listaErrorCampo;
		}
		else if (listaErrorCuenta.size() > 1)
		{
			listaErrores = listaErrorCuenta;
		}
		else if (listaErrorCtaInterna.size() > 1)
		{
			listaErrores = listaErrorCtaInterna;
		}

		bean.setListaErrores(listaErrores);
		return bean;
	}
	*/
	public String creaDetalleAltaImp(String numContrato, ArrayList listaRegistros, String tipoCarga)
	{

		CatNominaHTML html = new CatNominaHTML();
		//CatNominaConstantes cons = new CatNominaConstantes();
		StringBuffer sb = new StringBuffer();
		int totalReg = 0;

		sb.append(html.encabezadoImporAltaInterb(tipoCarga));

		Iterator it = listaRegistros.iterator();
		String dato = "";
		while (it.hasNext()) {
			dato = (String)it.next();

			if(tipoCarga.equals(CatNominaConstantes._ALTA_INTERB))
				sb.append(separaCadenaAlta(numContrato, dato));

		}
		//System.out.println("CatNominaAction - creaDetalleAltaImp() $ termina iterator" );
		totalReg = (listaRegistros.size());

		sb.append(html.generaTablaFinalAlta(totalReg));

		return sb.toString();
	}

	public String separaCadenaAlta(String numContrato, String registro)
	{

		EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - separaCadenaAlta() $ Registro " + registro, EIGlobal.NivelLog.INFO);
		String tablaDetalle = "";
		String enteros = "";
		String decimales = "";
		String[] arrDatosAlta = new String[29];

		CatNominaHTML html = new CatNominaHTML();


	    arrDatosAlta[0] = (registro.substring(0, 18)).trim();
		arrDatosAlta[1] = (registro.substring(18, 25)).trim();
		arrDatosAlta[2] = (registro.substring(25, 31)).trim();

		//arrDatosAlta[3] = String.valueOf((registro.substring(31, 51)).trim());



		// Modificacion para Sueldos//
		enteros =  String.valueOf(registro.substring(31, 49)).trim();
		decimales =  String.valueOf(registro.substring(49, 51)).trim();

		if(decimales.equals(""))
		{
			arrDatosAlta[3] = enteros + "." + "00";
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - separaCadenaAlta() $ sueldo sin decimanles: " + arrDatosAlta[3], EIGlobal.NivelLog.INFO);
		}
		else
		{
			arrDatosAlta[3] = enteros + "." + decimales;
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - eparaCadenaAlta() $ sueldo con decimanles: " + arrDatosAlta[3], EIGlobal.NivelLog.INFO);
		}
		// Fin Modificacion para Sueldos //

		arrDatosAlta[4] = (registro.substring(51, 81)).trim();
		arrDatosAlta[5] = (registro.substring(81, 111)).trim();
		arrDatosAlta[6] = (registro.substring(111, 141)).trim();
		arrDatosAlta[7] = (registro.substring(141, 154)).trim();
		arrDatosAlta[8] = (registro.substring(154, 155)).trim();
		arrDatosAlta[9] = (registro.substring(155, 215)).trim();
		arrDatosAlta[10] = (registro.substring(215, 245)).trim();
		arrDatosAlta[11] =	(registro.substring(245, 280)).trim();
		arrDatosAlta[12] = (registro.substring(280, 284)).trim();
		arrDatosAlta[13] =	(registro.substring(284, 319)).trim();
		arrDatosAlta[14] = (registro.substring(319, 324)).trim();
		arrDatosAlta[15] =	(registro.substring(324, 328)).trim();
		arrDatosAlta[16] = (registro.substring(328, 340)).trim();
		arrDatosAlta[17] = String.valueOf((registro.substring(340, 352)).trim());
		arrDatosAlta[18] = (registro.substring(352, 412)).trim();
		arrDatosAlta[19] =	(registro.substring(412, 442)).trim();
		arrDatosAlta[20] = (registro.substring(442, 477)).trim();
		arrDatosAlta[21] =	(registro.substring(477, 481)).trim();
		arrDatosAlta[22] = (registro.substring(481, 516)).trim();
		arrDatosAlta[23] =	(registro.substring(516, 521)).trim();
		arrDatosAlta[24] =	(registro.substring(521, 525)).trim();
		arrDatosAlta[25] = String.valueOf((registro.substring(525, 532)).trim());
		arrDatosAlta[26] = String.valueOf((registro.substring(532, 544)).trim());
		arrDatosAlta[27] = String.valueOf((registro.substring(544, 556)).trim());
		arrDatosAlta[28] = String.valueOf((registro.substring(556, 568)).trim());



	/*	for(int x=0; x<arrDatosAlta.length; x++)
		{
			System.out.println("CatNominaAction - separaCadenaAlta() $ Datos separados FINALES " + arrDatosAlta[x] );
		}*/
		tablaDetalle += html.generaTablaAlta(arrDatosAlta);

		//System.out.println("CatNominaAction - separaCadenaAlta() $ Table Detalle" + tablaDetalle);

		return tablaDetalle;

	}

	/**************************************************************
	 ***  Metodo encargado de validar una cadena de caracteres	***
	 ***  de tipo alfanumerico 									***
	 **************************************************************/
	public boolean validaCampoTexto(String campo)
	{
		//System.out.println("");
		//System.out.println("CatNominaValidaImport - validaCampoTexto $ Cadena a Validar = " + campo);
		String caracteresPermitidos = "abcdefghijklmn�opqrstuvwxyz" + "ABCDEFGHIJKLMN�OPQRSTUVWXYZ"
										+ "�����" + "�����" + "0123456789" + " " + ".";
 		boolean campoValido = false;
		int carValido = 0;
		char letra;

		for (int i = 0; i < campo.length(); i++) {
			letra = campo.charAt(i);
			for (int j = 0; j < caracteresPermitidos.length(); j++) {
				if (letra == caracteresPermitidos.charAt(j))
					carValido++;
			}
		}

		if (carValido == campo.length())
			campoValido = true;

		//System.out.println("CatNominaValidaImport - validaCampoTexto $ Cadena = " + campo + "  Valida =  " + campoValido );
		return campoValido;
	}


	/**************************************************************
	 ***  Metodo encargado de validar una cadena de caracteres	***
	 ***  de tipo numerico 									***
	 **************************************************************/
	public boolean validaCampoNumero(String dato) {
		//System.out.println("");
		//String caracteresPermitidos = "0123456789" + "." + " ";
		String caracteresPermitidos = "0123456789" + " ";
		boolean datoValido = false;
		int carValido = 0;
		char letra;

		for (int i = 0; i < dato.length(); i++) {
			letra = dato.charAt(i);
			for (int j = 0; j < caracteresPermitidos.length(); j++) {
				if (letra == caracteresPermitidos.charAt(j))
					carValido++;
			}
		}

		if (carValido == dato.length())
			datoValido = true;

		//System.out.println("CatNominaValidaImport - validaCampoNumero $ Cadena = "  + dato +  " Valida = " + datoValido );
		return datoValido;
	}

	/**************************************************************************************/
	/** Metodo: cambiaNum
	 * @param String, String
	 * @description: Obtiene el valor de un entero
	/*************************************************************************************/
	public String cambiaNum(String campo)
	{
		 try{
			if(campo.equals(""))
				return "0";
			else
				return campo;
		}
		catch(Exception e){
			return "0";
		}
	}

	public String obtieneVocal(String paterno)
	{
		for(int i=1; i<=paterno.length();i++)
		{
			if(paterno.toUpperCase().charAt(i)=='A' || paterno.toUpperCase().charAt(i)=='E' || paterno.toUpperCase().charAt(i)=='I' || paterno.toUpperCase().charAt(i)=='O' || paterno.toUpperCase().charAt(i)=='U')
			{
				return String.valueOf(paterno.toUpperCase().charAt(i));
			}
		}
		return "X";
	}

	boolean validaFecha(String fecha)
	{
		try{
			int ano = Integer.parseInt(fecha.substring(4,8).trim());
			int mes = Integer.parseInt(fecha.substring(2,4).trim());
			int dia = Integer.parseInt(fecha.substring(0,2).trim());
			EIGlobal.mensajePorTrace("ValidaArchivoAltas::validaFecha:: Anio->"+ano, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ValidaArchivoAltas::validaFecha:: Mes->"+mes, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ValidaArchivoAltas::validaFecha:: Dia->"+dia, EIGlobal.NivelLog.INFO);

			if(mes>0 && mes <=12 && dia>0 && dia<=31)
			{
				if(mes==4 || mes==6 || mes==9 || mes==11)
				{
					if(dia==31)
						return false;
				}
				else
				{
					if(mes==2)
					{
						if(dia==30 || dia==31)
							return false;
					}
				}
				return true;
			}
			else
			{
				return false;
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("***ValidaArchivoAltas.class - Error al validar la fecha", EIGlobal.NivelLog.INFO);
			return false;
		}
	}
}