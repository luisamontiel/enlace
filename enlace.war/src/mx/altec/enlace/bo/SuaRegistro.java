package mx.altec.enlace.bo;

import java.util.ArrayList;

import mx.altec.enlace.utilerias.SuaUtil;

//####################################################################
//
//      Modificacion: Nuevos campos: ValidaFundemexMultas,
//                    regCincoAportacionComplementaria,
//                    regCincoInfonavitMultas,
//                    regCincoFundemexDonativo,
//      Proyecto    : SUA 2005
//      Cod         : MX2005-21100
//      Programador : Carlos Aleman Rosas.
//      clave en cod: W3xx
//####################################################################

//####################################################################
//
//	Historial de Correcciones
//	28 Nov 2003
//	Se comentan lineas para que la bitacora se reduzca
//	Proyecto: Mantenimiento Enlace / Praxis
//	Por Lorenzo Mucio Mendoza
//
//####################################################################

public class SuaRegistro
{
	public int    numeroDeRegistro;
	public int    numeroDeTrabajador;
	public int    trabajadoresConCredito;

//  VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agrego esta varible del tipo de registro para el registro 06 del SUA IMSS y Infonavit
	public long    regTresSumaMensual;
	public long    regTresSumaBimestral;
//  VSWF RRG I 07-Julio-2008 AMX-08-00644-0A
	public int    tipoRegistro;

        public boolean ValidaFundemexMultas=false;// SUA 2005

	public String archivoFinal         = "";
	public String NombreArchivo        = "";

	public String regDosImssPatronal   = "";
	public String regDosRfcEmpresa     = "";
	public String regDosPeriodoDePago  = "";
	public String regDosAnioDePago     = "";
	public String regDosFolioSua       = "";
	public String regDosRazonSocial    = "";
	public String regDosDireccion      = "";
	public String regDosPoblacion      = "";
	public String regDosEstado         = "";
	public String regDosCodigoPostal   = "";		//Nuevo
	public String regDosTelefono   	   = "";		//Nuevo
	public String regDosPrimRiesgoT    = "";		//Nuevo
	public String regDosFechaPRT	   = "";		//Nuevo
	public String regDosActividadEco   = "";		//Nuevo
	public String regDosDelegIMSS      = "";		//Nuevo
	public String regDosSubDelegIMSS   = "";		//Nuevo
	public String regDosAreaGeoSalMini = "";//W3xx
	public String regDosPorcAportInfo  = "";//W3xx
	public String regDosConvReembSubs  = "";//W3xx
	public String regDosTipoCotizacion = "";//W3xx
	public String regDosDiasCotizadosPeriodo = "";
	public String regDosTrabajadores   = "";
	public String regDosTipodeDoc      = "";		//Nuevo
	public String regDosNumdeCredito   = "";		//Nuevo
	public String regDosNombreArchivo  = "";



	public String regTresImssPatronal        			= "";
	public String regTresRFCPatron        				= "";//nuevo  IGS
	public String regTresImssTrabajador      			= "";
	public String regTresRFCTrabajador        			= "";//nuevo  IGS
	public String regTresCURPTrabajador        			= "";//nuevo  IGS
	public String regTresFecINFONAVITTrabajador        	= "";//nuevo  IGS
	public String regTresMovPeriodo       				= "";//nuevo  IGS
	public String regTresNombreTrabajador      			= "";//nuevo  IGS
	public String regTresTipoTrabajador       			= "";//nuevo  IGS
	public String regTresJornadaRedTrabajador       	= "";//nuevo  IGS
	public String regTresDiasMensualCotiza   			= "";
	public String regTresDiasBimestreCotiza  			= "";
	public String regTresPeriodoPago         			= "";

	public String regTresEnfermedadCuota         = "";
	public String regTresEnfermedadExcedente     = "";
	public String regTresEnfermedadPrestaciones  = "";
	public String regTresGastosMedicos           = "";
	public String regTresRiesgosDeTrabajo        = "";
	public String regTresInvalidezVida           = "";
	public String regTresGuarderiaPrestaciones   = "";
	public String regTresSegurosImss             = "";
	public String regTresRetiroCuota             = "";
	public String regTresRetiroRecargos          = "";
	public String regTresCesantiaVejezPatronal   = "";
	public String regTresCesantiaVejezTrabajador = "";
	public String regTresCesantiaVejezRecargos   = "";
	public String regTresAportacionVoluntaria    = "";
	public String regTresAportacionPatronal      = "";
	public String regTresAmortizacionCredito     = "";
	public String regTresCreditoInfonavit        = "";
	public String regCuatroImssPatronal          = "";
	public String regCuatroImssTrabajador        = "";
	public String regCuatroImssTrabajador2        = "";
	public String regCuatroImssTrabajador3       = "";
	public String regCuatroImssTrabajador4        = "";
	public String regCuatroImssTrabajador5        = "";
	public String regCuatroImssTrabajador6        = "";
	public String regCuatroImssTrabajador7        = "";
	public String regCuatroDiasIncidencia1		 = "";
	public String regCuatroDiasIncidencia2		 = "";
	public String regCuatroDiasIncidencia3		 = "";
	public String regCuatroDiasIncidencia4		 = "";
	public String regCuatroDiasIncidencia5		 = "";
	public String regCuatroDiasIncidencia6		 = "";
	public String regCuatroDiasIncidencia7		 = "";
	public String regTresUltSalDiaIntPer         = "";
	public String regTresDiasIncaMes             = "";
	public String regTresDiasAusenteMes          = "";
	public String regTresDiasIncaBimestre        = "";
	public String regTresDiaAusenteBimestre      = "";
	public String regTresCuotaObrera     		 = "";  //nuevo  IGS
	public String regTresPrestDineroCuotaOb      = "";//nuevo  IGS
	public String regTresGastosMedPensCuotaOb    = "";//nuevo  IGS
	public String regTresInvalidezVidaCuotaOb    = "";//nuevo  IGS
	public String regTresCensatiaVejezCuotaOb    = "";//nuevo  IGS
	public String regTresMunicipio  			 = "";//nuevo  IGS
	public ArrayList regTresConcetradoNumIMSS  = new ArrayList();//nuevo  IGS




    public String regCuatroTipoMovIncidencia1  = "";//nuevo W3xx
    public String regCuatroSalarioDiarioInt1   = "";//nuevo W3xx
	public String regCuatroTipoMovIncidencia2  = "";    //w3xx
    public String regCuatroSalarioDiarioInt2   = "";    //W3xx
    public String regCuatroTipoMovIncidencia3  = "";    //W3xx
    public String regCuatroSalarioDiarioInt3   = "";    //W3xx
    public String regCuatroTipoMovIncidencia4  = "";    //W3xx
    public String regCuatroSalarioDiarioInt4   = "";    //W3xx
    public String regCuatroTipoMovIncidencia5  = "";    //W3xx
    public String regCuatroSalarioDiarioInt5   = "";    //W3xx
    public String regCuatroTipoMovIncidencia6  = "";    //W3xx
    public String regCuatroSalarioDiarioInt6   = "";    //W3xx
    public String regCuatroTipoMovIncidencia7  = "";    //W3xx
    public String regCuatroSalarioDiarioInt7   = "";    //W3xx

	public String regCincoImssPatronal                = "";
	public String regCincoPeriodoDePago               = "";
	public String regCincoFolioSua                    = "";

	public String regCincoEnfermedadCuota             = "";
	public String regCincoEnfermedadExcedente         = "";
	public String regCincoEnfermedadPrestaciones      = "";
	public String regCincoGastosMedicos               = "";
	public String regCincoRiesgosDeTrabajo            = "";
	public String regCincoInvalidezVida               = "";
	public String regCincoGuarderiaPrestaciones       = "";
	public String regCincoSegurosImss                 = "";
	public String regCincoSegurosImssActualizacion    = "";
	public String regCincoSegurosImssRecargos         = "";
	public String regCincoRetiro                      = "";
	public String regCincoCesantiaVejez               = "";
	public String regCincoRetiroCesantiaVejez         = "";
	public String regCincoRetiroCesantiaActualizacion = "";
	public String regCincoRetiroCesantiaVejezRecargos = "";
	public String regCincoAportacionVoluntaria        = "";
	public String regCincoAportacionPatronal          = "";
        public String regCincoAportacionComplementaria    = "";//nuevo campo para version W3xxx SUA 2005
	public String regCincoAmortizacionCredito         = "";
	public String regCincoInfonavitSubtotal           = "";
	public String regCincoInfonavitActualizacion      = "";
	public String regCincoInfonavitRecargos           = "";
        public String regCincoInfonavitMultas             = "";//nuevo campo para version W3xxx SUA 2005
        public String regCincoFundemexDonativo            = "";//nuevo campo para version W3xxx SUA 2005
//      VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agrego esta varible del tipo de registro para el registro 06 del SUA IMSS y Infonavit
    public String regCincoTipoRegistro                ="";
//  VSWF RRG F 07-Julio-2008 AMX-08-00644-0A
//VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agrego esta varible del rfc para el registro 06 del SUA IMSS y Infonavit
    public String regCincoRfcEmpresa				  ="";
  //VSWF RRG F 07-Julio-2008 AMX-08-00644-0A
    public String regCincoTasaActualizacion = "";
    public String regCincoTasaRecargos = "";
    //<stefanini>
    public String regCincoFiller ="";
    //</stefanini>



	public String regSeisImssPatronal             = "";
  //VSWF RRG I 23-Mayo-2008 AMX-08-00644-0A Se agrego esta varible del rfc para el registro 06 del SUA IMSS y Infonavit
    public String regSeisRfcEmpresa				  ="";
  //VSWF RRG F 23-Mayo-2008 AMX-08-00644-0A
	public String regSeisPeriodoDePago            = "";
	public String regSeisFolioSua                 = "";
	/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
	public String regSeisSumTasaRec				  = "";
	/* Fin GAO */
	public String regSeisFechaLimite              = "";

	public String regSeisVersionSua               = "";
	public String regSeisTamanioArchivo           = "";
	public String regSeisNumeroDeDiscos           = "";
	public String regSeisTotalImss4Seguros        = "";
	public String regSeisTotalImssConcentrar      = "";
	public String regSeisTotalAportacionPatronal  = "";
	public String regSeisTotalAmortizacionCredito = "";

	/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
	public String regSeisCheckSum = "";
	public double regSeisCuotaFija = 0;
	public double regSeisCuotaExcedePatronal = 0;
	public double regSeisCuotaExcedeObrero = 0;
	public double regSeisPrestaDineroPatronal = 0;
	public double regSeisPrestaDineroObrero = 0;
	public double regSeisGastosMedicosPensioPatronal = 0;
	public double regSeisGastosMedicosPensioObrero = 0;
	public double regSeisRiesgosTrabajo = 0;
	public double regSeisInvalidezVidaPatronal = 0;
	public double regSeisInvalidezVidaObrero = 0;
	public double regSeisGuarderiaPrestaSociales = 0;
	public String regSeisFactorReversionCF = "";
	public String regSeisFactorReverExcPatronal = "";
	public String regSeisFactorReverExcObrero = "";
	public String regSeisFactorReverPDPatronal = "";
	public String regSeisFactorReverPDObero = "";
	public String regSeisFactorReverGMPPatronal = "";
	public String regSeisFactorReverGMPObero = "";
	public String regSeisFactorReverRT = "";
	public String regSeisFactorReverIVPatronal = "";
	public String regSeisFactorReverIVPObero = "";
	public String regSeisFactorReverGPS = "";
	public int regSeisFactorReverAusentismo = 0;
	public String regSeisFiller = "";
	/* Fin GAO */

	public int    T35								  = 0;
	public int    T36								  = 0;
	public int    S24								  = 0;
	public int    S25								  = 0;



	public SuaRegistro()
	{
		numeroDeRegistro       = 0;
		numeroDeTrabajador     = 0;
		trabajadoresConCredito = 0;
//	  VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agrego esta varible del tipo de registro para el registro 06 del SUA IMSS y Infonavit
		regTresSumaMensual			   = 0;
		regTresSumaBimestral          = 0;
//		  VSWF RRG I 07-Julio-2008 AMX-08-00644-0A
	}

	public int leerEntero(String dato)
	{
		int importe;
		Integer valor;
		try {
			importe = Integer.parseInt(dato.trim());
		} catch( Exception exceptionLeer ) {
			importe = 0;
			//System.out.println( exceptionLeer.getMessage() );
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class Excepcion %leerEntero() >> "+ exceptionLeer.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}
		return(importe);
	}

	public static String SubString(String dato, int inicio, int Longitud )
	{
		int fin = inicio + Longitud-1;
		inicio = inicio - 1;
		if( fin < dato.length() )
			return( dato.substring( inicio, fin) );
		else
			return( dato.substring( inicio).toUpperCase() );
	}

	public void PreparaRegistroDOS()
	{
         regDosImssPatronal   = "";
         regDosRfcEmpresa     = "";
         regDosPeriodoDePago  = "";
         regDosAnioDePago     = "";
         regDosFolioSua       = "";
         regDosRazonSocial    = "";
         regDosDireccion      = "";
         regDosPoblacion      = "";
         regDosEstado         = "";
         regDosCodigoPostal   = "";		//Nuevo
         regDosTelefono   	  = "";		//Nuevo
         regDosPrimRiesgoT    = "";		//Nuevo
         regDosFechaPRT	   	  = "";		//Nuevo
         regDosActividadEco   = "";		//Nuevo
         regDosDelegIMSS      = "";		//Nuevo
         regDosSubDelegIMSS   = "";		//Nuevo
         regDosAreaGeoSalMini = "";//W3xx
         regDosPorcAportInfo  = "";//W3xx
         regDosConvReembSubs  = "";//W3xx
         regDosTipoCotizacion = "";//W3xx
         regDosDiasCotizadosPeriodo = "";
         regDosTrabajadores   = "";
         regDosTipodeDoc      = "";		//Nuevo
         regDosNumdeCredito   = "";		//Nuevo
         regDosNombreArchivo  = "";

	}

	public void PreparaRegistroTRES()
	{
		regTresImssPatronal            = "";
		regTresImssTrabajador          = "";
		regTresDiasMensualCotiza       = "";
		regTresDiasBimestreCotiza      = "";
		regTresPeriodoPago             = "";
		regTresEnfermedadCuota         = "";
		regTresEnfermedadExcedente     = "";
		regTresEnfermedadPrestaciones  = "";
		regTresGastosMedicos           = "";
		regTresRiesgosDeTrabajo        = "";
		regTresInvalidezVida           = "";
		regTresGuarderiaPrestaciones   = "";
		regTresSegurosImss             = "";
		regTresRetiroCuota             = "";
		regTresRetiroRecargos          = "";
		regTresCesantiaVejezPatronal   = "";
		regTresCesantiaVejezTrabajador = "";
		regTresCesantiaVejezRecargos   = "";
		regTresAportacionVoluntaria    = "";
		regTresAportacionPatronal      = "";
		regTresAmortizacionCredito     = "";
		//regTresConceptoNoDefinido      = ""; se elimina por nuevo formato
		regTresCreditoInfonavit        = "";
		regTresCuotaObrera     		   = "";// nuevo IGS
		regTresPrestDineroCuotaOb      = "";// nuevo IGS
		regTresGastosMedPensCuotaOb    = "";// nuevo IGS
		regTresInvalidezVidaCuotaOb    = "";// nuevo IGS
		regTresCensatiaVejezCuotaOb    = "";// nuevo IGS
		regTresMunicipio      		   = "";// nuevo IGS
		regTresRFCTrabajador           = "";// nuevo IGS
		regTresCURPTrabajador          = "";// nuevo IGS
		regTresFecINFONAVITTrabajador  = "";// nuevo IGS
		regTresMovPeriodo       	   = "";// nuevo IGS
		regTresNombreTrabajador        = "";// nuevo IGS
		regTresTipoTrabajador          = "";// nuevo IGS
		regTresJornadaRedTrabajador    = "";// nuevo IGS
		regTresUltSalDiaIntPer         = "";//W3xx
                regTresDiasIncaMes             = "";//W3xx
                regTresDiasAusenteMes          = "";//W3xx
                regTresDiasIncaBimestre        = "";//W3xx
                regTresDiaAusenteBimestre      = "";//W3xx


	}

	public void PreparaRegistroCUATRO()
	{
		regCuatroImssPatronal      = "";
		regCuatroImssTrabajador    = "";
		regCuatroImssTrabajador2    = "";
		regCuatroImssTrabajador3    = "";
		regCuatroImssTrabajador4    = "";
		regCuatroImssTrabajador5    = "";
		regCuatroImssTrabajador6    = "";
		regCuatroImssTrabajador7    = "";
		regCuatroDiasIncidencia1	 = "";
		regCuatroDiasIncidencia2	 = "";
		regCuatroDiasIncidencia3	 = "";
		regCuatroDiasIncidencia4	 = "";
		regCuatroDiasIncidencia5	 = "";
		regCuatroDiasIncidencia6	 = "";
		regCuatroDiasIncidencia7	 = "";
        regCuatroTipoMovIncidencia1  = "";    //W3xx
        regCuatroSalarioDiarioInt1   = "";    //W3xx
        regCuatroTipoMovIncidencia2  = "";    //W3xx
        regCuatroSalarioDiarioInt2   = "";    //W3xx
        regCuatroTipoMovIncidencia3  = "";    //W3xx
        regCuatroSalarioDiarioInt3   = "";    //W3xx
        regCuatroTipoMovIncidencia4  = "";    //W3xx
        regCuatroSalarioDiarioInt4   = "";    //W3xx
        regCuatroTipoMovIncidencia5  = "";    //W3xx
        regCuatroSalarioDiarioInt5   = "";    //W3xx
        regCuatroTipoMovIncidencia6  = "";    //W3xx
        regCuatroSalarioDiarioInt6   = "";    //W3xx
        regCuatroTipoMovIncidencia7  = "";    //W3xx
        regCuatroSalarioDiarioInt7   = "";    //W3xx

	}

	public void PreparaRegistroCINCO()
	{
		regCincoImssPatronal                = "";
		regCincoPeriodoDePago               = "";
		regCincoFolioSua                    = "";
		regCincoEnfermedadCuota             = "";
		regCincoEnfermedadExcedente         = "";
		regCincoEnfermedadPrestaciones      = "";
		regCincoGastosMedicos               = "";
		regCincoRiesgosDeTrabajo            = "";
		regCincoInvalidezVida               = "";
		regCincoGuarderiaPrestaciones       = "";
		regCincoSegurosImss                 = "";
		regCincoSegurosImssActualizacion    = "";
		regCincoSegurosImssRecargos         = "";
		regCincoRetiro                      = "";
		regCincoCesantiaVejez               = "";
		regCincoRetiroCesantiaVejez         = "";
		regCincoRetiroCesantiaActualizacion = "";
		regCincoRetiroCesantiaVejezRecargos = "";
		regCincoAportacionVoluntaria        = "";
		regCincoAportacionPatronal          = "";
                regCincoAportacionComplementaria    = "";//W3xx
		regCincoAmortizacionCredito         = "";
		regCincoInfonavitSubtotal           = "";
		regCincoInfonavitActualizacion      = "";
		regCincoInfonavitRecargos           = "";
                regCincoInfonavitMultas             = "";//W3xx
                regCincoFundemexDonativo            = "";//W3xx
//   VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agrego esta varible del tipo de registro para el registro 06 del SUA IMSS y Infonavit
        regCincoTipoRegistro				="";
//      VSWF RRG F 07-Julio-2008 AMX-08-00644-0A
//VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agrego esta varible del rfc para el registro 06 del SUA IMSS y Infonavit
        regCincoRfcEmpresa 					= "";
//VSWF RRG F 07-Julio-2008 AMX-08-00644-0A

        //<stefanini>
        regCincoFiller = "";
        //</stefanini>
		regCincoTasaActualizacion			= "";
		regCincoTasaRecargos 				= "";
	}

	public void PreparaRegistroSEIS()
	{
		regSeisImssPatronal             = "";
      //VSWF RRG I 23-Mayo-2008 AMX-08-00644-0A Se agrego esta varible del rfc para el registro 06 del SUA IMSS y Infonavit
        regSeisRfcEmpresa 				= "";
      //VSWF RRG F 23-Mayo-2008 AMX-08-00644-0A
		regSeisPeriodoDePago            = "";
		regSeisFolioSua                 = "";
		regSeisFechaLimite              = "";
		regSeisVersionSua               = "";
		/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
		regSeisSumTasaRec				= "";
		/* Fin GAO */
		regSeisTamanioArchivo           = "";
		regSeisNumeroDeDiscos           = "";
		regSeisTotalImss4Seguros        = "";
		regSeisTotalImssConcentrar      = "";
		regSeisTotalAportacionPatronal  = "";
		regSeisTotalAmortizacionCredito = "";

		/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
		regSeisCheckSum = "";
		regSeisCuotaFija = 0;
		regSeisCuotaExcedePatronal = 0;
		regSeisCuotaExcedeObrero = 0;
		regSeisPrestaDineroPatronal = 0;
		regSeisPrestaDineroObrero = 0;
		regSeisGastosMedicosPensioPatronal = 0;
		regSeisGastosMedicosPensioObrero = 0;
		regSeisRiesgosTrabajo = 0;
		regSeisInvalidezVidaPatronal = 0;
		regSeisInvalidezVidaObrero = 0;
		regSeisGuarderiaPrestaSociales = 0;
		regSeisFactorReversionCF = "";
		regSeisFactorReverExcPatronal = "";
		regSeisFactorReverExcObrero = "";
		regSeisFactorReverPDPatronal = "";
		regSeisFactorReverPDObero = "";
		regSeisFactorReverGMPPatronal = "";
		regSeisFactorReverGMPObero = "";
		regSeisFactorReverRT = "";
		regSeisFactorReverIVPatronal = "";
		regSeisFactorReverIVPObero = "";
		regSeisFactorReverGPS = "";
		regSeisFactorReverAusentismo = 0;
		regSeisFiller = "";
		/* Fin GAO */
	}

	public void ProcesaRegistroDOS(String registro)
	{
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class & ProcesaRegistroDOS &", EIGlobal.NivelLog.INFO);

		regDosImssPatronal    = SubString( registro, 3,  11);
		regDosRfcEmpresa      = SubString( registro, 14, 13);
		regDosPeriodoDePago   = SubString( registro, 31,  2);
		regDosAnioDePago      = SubString( registro, 27,  4);
        regDosFolioSua        = SubString( registro, 33,  6);
        regDosRazonSocial     = SubString( registro, 39, 50);
        regDosDireccion       = SubString( registro, 89, 40);
        regDosPoblacion       = SubString( registro, 129,40);
        regDosEstado          = SubString( registro, 169, 2);
        regDosCodigoPostal    = SubString( registro, 171, 5);		//Nuevo
        regDosTelefono   	  = SubString( registro, 176, 15);		//Nuevo
        regDosPrimRiesgoT     = SubString( registro, 191, 7);		//Nuevo
        regDosFechaPRT	   	  = SubString( registro, 198, 6);		//Nuevo
        regDosActividadEco    = SubString( registro, 204, 40);		//Nuevo
        regDosDelegIMSS       = SubString( registro, 244, 2);		//Nuevo
        regDosSubDelegIMSS    = SubString( registro, 246, 2);		//Nuevo
        regDosAreaGeoSalMini  = SubString( registro, 248,1);//W3xx
        regDosPorcAportInfo   = SubString( registro, 249, 2);//W3xx
        regDosConvReembSubs   = SubString( registro, 251,1);//W3xx
        regDosTipoCotizacion  = SubString( registro, 252,1);//w3xx
        regDosDiasCotizadosPeriodo  = SubString( registro,253,7);
        regDosTrabajadores    = SubString( registro, 260, 9);
        regDosTipodeDoc       = SubString( registro, 269, 2);		//Nuevo
        regDosNumdeCredito    = SubString( registro, 271, 9);		//Nuevo
        regDosNombreArchivo   = SubString( registro, 29,  4);
        regDosNombreArchivo  += SubString( regDosImssPatronal,  3, 1);
        regDosNombreArchivo  += SubString( regDosImssPatronal,  7, 1);
        regDosNombreArchivo  += SubString( regDosImssPatronal,  9, 1);

	}

	public void ProcesaRegistroTRES(String registro)
	{
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class # ProcesaRegistroTRES #", 5);

		registro = registro.toUpperCase();
		regTresImssPatronal            = SubString( registro,    3, 11);
		////Se agrego lectura de RFC
		regTresRFCPatron	  		   = SubString( registro,   14, 13);
		regTresPeriodoPago             = SubString( registro,   27,  6);
		regTresImssTrabajador          = SubString( registro,   33, 11);
		regTresConcetradoNumIMSS.add(regTresImssTrabajador);			 //nuevo  IGS
		regTresRFCTrabajador           = SubString( registro,   44, 13);;//nuevo  IGS
		regTresCURPTrabajador          = SubString( registro,   57, 18);;//nuevo  IGS
		regTresCreditoInfonavit        = SubString( registro,   75, 10);
		regTresFecINFONAVITTrabajador  = SubString( registro,   85,  8);;//nuevo  IGS
		regTresMovPeriodo       	   = SubString( registro,   93,  2);;//nuevo  IGS
		regTresNombreTrabajador        = SubString( registro,   95, 50);;//nuevo  IGS
		regTresUltSalDiaIntPer         = SubString( registro,  145,  7);//W3xx
		regTresTipoTrabajador          = SubString( registro,  152,  1);;//nuevo  IGS
		regTresJornadaRedTrabajador    = SubString( registro,  153,  1);;//nuevo  IGS
		regTresDiasMensualCotiza       = SubString( registro,  154,  2);
		regTresDiasIncaMes             = SubString( registro,  156,  2);//W3xx
		regTresDiasAusenteMes          = SubString( registro,  158,  2);//W3xx


		regTresEnfermedadCuota         = SubString( registro,  160, 7);
		regTresEnfermedadExcedente     = SubString( registro,  167, 7);
		regTresEnfermedadPrestaciones  = SubString( registro,  174, 7);
		regTresGastosMedicos           = SubString( registro,  181, 7);
		regTresRiesgosDeTrabajo        = SubString( registro,  188, 7);
		regTresInvalidezVida           = SubString( registro,  195, 7);
		regTresGuarderiaPrestaciones   = SubString( registro,  202, 7);
		regTresSegurosImss             = SubString( registro,  209, 7);
		regTresDiasBimestreCotiza      = SubString( registro,  216,  2);
		regTresDiasIncaBimestre        = SubString( registro,  218, 2);//W3xx
		regTresDiaAusenteBimestre      = SubString( registro,  220, 2);//W3xx
		regTresRetiroCuota             = SubString( registro,  222, 7);
		regTresRetiroRecargos          = SubString( registro,  229, 7);
		regTresCesantiaVejezPatronal   = SubString( registro,  236, 7);
		regTresCesantiaVejezTrabajador = SubString( registro,  243, 7);
		regTresCesantiaVejezRecargos   = SubString( registro,  250, 7);
		regTresAportacionVoluntaria    = SubString( registro,  257, 7);
		regTresAportacionPatronal      = SubString( registro,  264, 7);
		regTresAmortizacionCredito     = SubString( registro,  271, 7);
		regTresCuotaObrera     		   = SubString( registro,  278, 3);// nuevo IGS
		regTresPrestDineroCuotaOb      = SubString( registro,  281, 3);// nuevo IGS
		regTresGastosMedPensCuotaOb    = SubString( registro,  284, 3);// nuevo IGS
		regTresInvalidezVidaCuotaOb    = SubString( registro,  287, 3);// nuevo IGS
		regTresCensatiaVejezCuotaOb    = SubString( registro,  290, 3);// nuevo IGS
		regTresMunicipio      		   = SubString( registro,  293, 3);// nuevo IGS


		// Inicio de Suma de S24 y S25
		T35 = SuaUtil.Str2int(regTresAportacionPatronal);
		T36 = SuaUtil.Str2int(regTresAmortizacionCredito);
		if (T36 == 0) {		//T36
			S24 += T35;
		}
		else {
			S25 += T35;
		}
		// Fin de Suma de S24 y S25



		regTresImssTrabajador.trim();
		if(regTresImssTrabajador.length() == 11)
			numeroDeTrabajador++;


//		  VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agrego esta varible del tipo de registro para el registro 06 del SUA IMSS y Infonavit
		if(regTresDiasMensualCotiza.length() == 2)
			regTresSumaMensual+= Long.parseLong(regTresDiasMensualCotiza);

		if(regTresDiasBimestreCotiza.length() == 2)
			regTresSumaBimestral+=Long.parseLong(regTresDiasBimestreCotiza);
//		  VSWF RRG I 07-Julio-2008 AMX-08-00644-0A
	}

	public void ProcesaRegistroCUATRO(String registro)
	{
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class & ProcesaRegistroCUATRO &", EIGlobal.NivelLog.INFO);

		regCuatroImssPatronal          = SubString( registro,    3, 11);
		regCuatroImssTrabajador        = SubString( registro,   14, 11);
		regCuatroDiasIncidencia1	   = SubString( registro,   43,  2);
		regCuatroDiasIncidencia2	   = SubString( registro,   81,  2);
		regCuatroDiasIncidencia3	   = SubString( registro,  119,  2);
		regCuatroDiasIncidencia4	   = SubString( registro,  157,  2);
		regCuatroDiasIncidencia5	   = SubString( registro,  195,  2);
		regCuatroDiasIncidencia6	   = SubString( registro,  233,  2);
		regCuatroDiasIncidencia7	   = SubString( registro,  271,  2);
		regCuatroImssTrabajador2       = SubString( registro,   52, 11);
		regCuatroImssTrabajador3       = SubString( registro,   90, 11);
		regCuatroImssTrabajador4       = SubString( registro,  128, 11);
		regCuatroImssTrabajador5       = SubString( registro,  166, 11);
		regCuatroImssTrabajador6       = SubString( registro,  204, 11);
		regCuatroImssTrabajador7       = SubString( registro,  242, 11);

		// Traer el tipo de mov. y el salario diario integrado SUA2005-MX-2005-1100
		// hasta maximo 7
		regCuatroTipoMovIncidencia1  = SubString( registro,  25,  2);
		regCuatroSalarioDiarioInt1   = SubString( registro,  45,  7);
                regCuatroTipoMovIncidencia2  = SubString( registro,  63,  2);
                regCuatroSalarioDiarioInt2   = SubString( registro,  83,  7);
                regCuatroTipoMovIncidencia3  = SubString( registro, 101,  2);
                regCuatroSalarioDiarioInt3   = SubString( registro, 121,  7);
                regCuatroTipoMovIncidencia4  = SubString( registro, 139,  2);
                regCuatroSalarioDiarioInt4   = SubString( registro, 159,  7);
                regCuatroTipoMovIncidencia5  = SubString( registro, 177,  2);
                regCuatroSalarioDiarioInt5   = SubString( registro, 197,  7);
                regCuatroTipoMovIncidencia6  = SubString( registro, 215,  2);
                regCuatroSalarioDiarioInt6   = SubString( registro, 235,  7);
                regCuatroTipoMovIncidencia7  = SubString( registro, 253,  2);
                regCuatroSalarioDiarioInt7   = SubString( registro, 273,  7);


	}

	public void ProcesaRegistroCINCO(String registro)
	{

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class & ProcesaRegistroCINCO &", EIGlobal.NivelLog.INFO);
		regCincoTipoRegistro				=SubString( registro,    1, 2);
		regCincoImssPatronal                = SubString( registro,    3, 11);
        regCincoRfcEmpresa 					= SubString( registro, 14, 13);
		regCincoPeriodoDePago               = SubString( registro,   27,  6 );
		regCincoFolioSua                    = SubString( registro,   33,  6);
		regCincoTasaActualizacion	    	= SubString( registro, 39, 10);
		regCincoTasaRecargos		    	= SubString( registro, 49, 10);
		regCincoEnfermedadCuota             = SubString( registro,   59,  9);
		regCincoEnfermedadExcedente         = SubString( registro,   68,  9);
		regCincoEnfermedadPrestaciones      = SubString( registro,   77,  9);
		regCincoGastosMedicos               = SubString( registro,   86,  9);
		regCincoRiesgosDeTrabajo            = SubString( registro,   95,  9);
		regCincoInvalidezVida               = SubString( registro,  104,  9);
		regCincoGuarderiaPrestaciones       = SubString( registro,  113,  9);
		regCincoSegurosImss                 = SubString( registro,  122, 11);
		regCincoSegurosImssActualizacion    = SubString( registro,  133,  9);
		regCincoSegurosImssRecargos         = SubString( registro,  142,  9);
		regCincoRetiro                      = SubString( registro,  151,  9);
		regCincoCesantiaVejez               = SubString( registro,  160,  9);
		regCincoRetiroCesantiaVejez         = SubString( registro,  169, 11);
		regCincoRetiroCesantiaActualizacion = SubString( registro,  180,  9);
		regCincoRetiroCesantiaVejezRecargos = SubString( registro,  189,  9);
		regCincoAportacionVoluntaria        = SubString( registro,  198, 11);
		regCincoAportacionPatronal          = SubString( registro,  209,  9);
		regCincoAmortizacionCredito         = SubString( registro,  218,  9);
		regCincoInfonavitSubtotal           = SubString( registro,  227,  9);
		regCincoInfonavitActualizacion      = SubString( registro,  236,  9);
		regCincoInfonavitRecargos           = SubString( registro,  245,  9);

		// Traer Aport.Comp. multas y donativos SUA2005-MX-2005-1100
        regCincoAportacionComplementaria    = SubString( registro,  254,  9);//S29
        regCincoInfonavitMultas             = SubString( registro,  263,  9);//S30
        regCincoFundemexDonativo            = SubString( registro,  272,  9);//S31

        //<stefanini>
        regCincoFiller						= SubString( registro,  287,  9);
        //</stefanini>
	}

	public void ProcesaRegistroSEIS(String registro)
	{
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class & ProcesaRegistroSEIS &", EIGlobal.NivelLog.INFO);

		regSeisImssPatronal             = SubString( registro, 3,   11);
      //VSWF RRG I 23-Mayo-2008 AMX-08-00644-0A Se agrego esta varible del rfc para el registro 06 del SUA IMSS y Infonavit
        regSeisRfcEmpresa = SubString(registro, 14, 13);
      //VSWF RRG F 23-Mayo-2008 AMX-08-00644-0A
		regSeisPeriodoDePago            = SubString( registro, 27,   6);
		regSeisFolioSua                 = SubString( registro, 33,   6);
		/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
		regSeisSumTasaRec				= SubString( registro, 39,   10);
		/* Fin GAO */
		regSeisTamanioArchivo           = SubString( registro, 49,  10);
		regSeisFechaLimite              = SubString( registro, 59,   8);
		regSeisNumeroDeDiscos           = SubString( registro, 67,   2);
		regSeisVersionSua               = SubString( registro, 69,   4);
		regSeisTotalImss4Seguros        = SubString( registro, 73,  12);
		regSeisTotalImssConcentrar      = SubString( registro, 85,  12);
		regSeisTotalAportacionPatronal  = SubString( registro, 97,  12);
		regSeisTotalAmortizacionCredito = SubString( registro, 109, 12);



		/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
		regSeisCheckSum = SubString( registro, 121, 19);
		regSeisCuotaFija = Double.parseDouble( SubString( registro, 140, 9) );
		regSeisCuotaExcedePatronal = Double.parseDouble( SubString( registro, 149, 9) );
		regSeisCuotaExcedeObrero = Double.parseDouble( SubString( registro, 158, 9) );
		regSeisPrestaDineroPatronal = Double.parseDouble( SubString( registro, 167, 9) );
		regSeisPrestaDineroObrero = Double.parseDouble( SubString( registro, 176, 9) );
		regSeisGastosMedicosPensioPatronal = Double.parseDouble( SubString( registro, 185, 9) );
		regSeisGastosMedicosPensioObrero = Double.parseDouble( SubString( registro, 194, 9) );
		regSeisRiesgosTrabajo = Double.parseDouble( SubString( registro, 203, 9) );
		regSeisInvalidezVidaPatronal = Double.parseDouble( SubString( registro, 212, 9) );
		regSeisInvalidezVidaObrero = Double.parseDouble( SubString( registro, 221, 9) );
		regSeisGuarderiaPrestaSociales = Double.parseDouble( SubString( registro, 230, 9) );
		regSeisFactorReversionCF = SubString( registro, 239, 3);
		regSeisFactorReverExcPatronal = SubString( registro, 242, 3);
		regSeisFactorReverExcObrero = SubString( registro, 245, 3);
		regSeisFactorReverPDPatronal = SubString( registro, 248, 3);
		regSeisFactorReverPDObero = SubString( registro, 251, 3);
		regSeisFactorReverGMPPatronal = SubString( registro, 254, 3);
		regSeisFactorReverGMPObero = SubString( registro, 257, 3);
		regSeisFactorReverRT = SubString( registro, 260, 3);
		regSeisFactorReverIVPatronal = SubString( registro, 263, 3);
		regSeisFactorReverIVPObero = SubString( registro, 266, 3);
		regSeisFactorReverGPS = SubString( registro, 269, 3);
		regSeisFactorReverAusentismo = Integer.valueOf( SubString( registro, 272, 3) ).intValue();
		regSeisFiller = SubString( registro, 275, 21);
		/* Fin GAO */
	}

	public void extraerRegistro(String registro)
	{
//		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class # extraerRegistro #<<" + registro + ">>", 5);   // 2003 11 28 LMM
		try{
			numeroDeRegistro++;
			tipoRegistro = leerEntero(SubString( registro,1,2));

//			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class # tipoderegistro : " +Integer.toString( tipoRegistro)+ " #", 5); //2003 11 28 ---- LMM
			switch(tipoRegistro)
			{
				case 2: PreparaRegistroDOS();
						ProcesaRegistroDOS(registro);
						break;
				case 3: PreparaRegistroTRES();
						ProcesaRegistroTRES(registro);
						break;
				case 4: PreparaRegistroCUATRO();
						ProcesaRegistroCUATRO(registro);
						break;
				case 5: PreparaRegistroCINCO();
						ProcesaRegistroCINCO(registro);
						break;
				case 6: PreparaRegistroSEIS();
						ProcesaRegistroSEIS(registro);
						break;
			}
		} catch(Exception E ) {
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class Excepcion %extraerRegistro() >> "+ E.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaRegistro.class # Terminar : extraerRegistro #", 5);
	}
}