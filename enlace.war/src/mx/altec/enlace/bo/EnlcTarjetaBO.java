package mx.altec.enlace.bo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.beans.FiltroConsultaBean;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.dao.DaoException;

/**
 * @author ESC
 * Clase que realiza las regals de negocio en la administraci&oacute;n y consulta de remesas y ejecuta el llamado al DAO.
 */
public interface EnlcTarjetaBO {

	/**
	 * Valida los datos de las tarjetas
	 * @param request peticion
	 * @param tarjeta numero
	 * @param contrato numero
	 * @return TarjetaContrato tarjeta
	 * @throws DaoException excepcion
	 */
	public TarjetaContrato validarDatosTarjetas(HttpServletRequest request, String tarjeta, String contrato) throws DaoException;

	/**
	 * Consulta las tarjetas de acuerdo a los filtros
	 * @param filtros filtros
	 * @param request peticion
	 * @return ArrayList arreglo de contratos
	 * @throws DaoException excepcion
	 */
	public List<TarjetaContrato> consultarDatosTarjetas(FiltroConsultaBean filtros, HttpServletRequest request) throws DaoException;

	/**
	 * Eliminar lista de tarjetas asociadas a un contrato
	 * @param tarjetasReq lista de tarjetas
	 * @param request peticion
	 * @throws DaoException excepcion
	 */
	public void eliminarTarjetas(String[] tarjetasReq, HttpServletRequest request) throws DaoException;

	/**
	 * Eliminar lista de tarjetas asociadas a un contrato
	 * @return Long secuencia
	 * @throws DaoException excepcion
	 */
	public Long obtenerSecTarjetas() throws DaoException;

	/**
	 * Agregar tarjeta a la BD
	 * @param tarjetas lista de tarjetas
	 * @param request peticion
	 * @throws BusinessException excepcion
	 */
	public void agregarTarjeta(List<TarjetaContrato> tarjetas, HttpServletRequest request) throws DaoException;

	/**
	 * Valida el archivo de tarjetas
	 * @param request peticion multipart
	 * @throws BusinessException excepcion
	 */
	public void validarArchivo(HttpServletRequest request) throws BusinessException;
}
