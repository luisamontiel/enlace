/*
 * pdPagoLinea.java
 *
 * Created on 10 de abril de 2002, 03:23 PM
 */

package mx.altec.enlace.bo;

import java.util.*;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class pdPagoLinea implements java.io.Serializable {

    /** N�mero de pago actual */
    protected long NumPago;
    /** Mapa de los pagos */
    protected HashMap Pagos;
    /** Identificador de uso */
    protected int Uso;

    /** Creates new pdPagoLinea */
    public pdPagoLinea () {
        NumPago = 0;
        Pagos = new HashMap ();
        Uso = 1;
    }

    /**
     * Constructor
     * @param lista Lista de pagos
     */
    public pdPagoLinea (ArrayList lista) {
        ListIterator liLista = lista.listIterator ();
        Pagos = new HashMap ();
        Long Llave;
        pdCPago pag;
        pdPago Temp;
        while (liLista.hasNext ()) {
            pag = (pdCPago) liLista.next ();
            Temp = new pdPago (pag);
            Llave = new Long (Temp.getNoPago ());
            Pagos.put (Llave, Temp);
        }
        Uso = 2;
    }

    /**
     * Metodo para agregar un pago al mapa
     * @param pago Pago a agregar
     */
    public void agrega (pdPago pago) {
        Long Llave = new Long (pago.getNoPago ());
        Pagos.put (Llave, pago);
    }

    /**
     * Metodo para obtener un pago
     * @param numpago N�mero de pago a obtener
     * @return Pago con el n�mero proporcionado
     */
    public pdPago getPago (long numpago) {
        Long Llave = new Long (numpago);
        return (pdPago) Pagos.get (Llave);
    }

    /**
     * Metodo para obtener el siguiente n�mero de pago
     * @return Siguiente n�mero de pago
     */
    public long getNumeroPago () {
        return NumPago++;
    }

    /**
     * Metodo para ver si el pago se puede registrar
     * @param pago N�mero del pago que se quiere verificar
     * @return true si el pago se puede registrar, false si ya ha sido registrado
     */
    public boolean verificaPago (long pago) {
        Long Llave = new Long (pago);
        if (Pagos.containsKey (Llave))
            return false;
        else
            return true;
    }

    /**
     * Metodo para obtener el tipo de transacci�n
     * @return Tipo de transacci�n 1 Registro, 2 Modificaci�n
     */
    public int getUso () {return Uso;}

}
