package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import java.util.Vector;
import mx.altec.enlace.beans.AdmTrxGP72;
import mx.altec.enlace.dao.TrxGP72DAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.Ps7;

/**
 *  Clase Bussines para manejar el procedimiento para la transaccion GP72
 *
 * @author Daniel Hernandez Perez
 * @version 1.0 Apr 10, 2012
 */
public class TrxGP72BO {

	/**
	 * Objeto contenedor de tipo AdmTrxGP72
	 */
	private AdmTrxGP72 bean;

	/**
	 * Obtener el valor de bean.
     *
     * @return bean valor asignado.
	 */
	public AdmTrxGP72 getBean() {
		return bean;
	}

	/**
	 * Metodo para realizar la liquidacion de las operaciones en Cambios 390
	 *
	 * @param cveTcOper	-	Clave de Cotizaci�n
	 * @param ctaCargo	-	Cuenta Cargo
	 * @param divisaOperante	- Divisa Operante
	 * @param contraDivisa	-	Contra divisa
	 * @param importeTotOper	- Monto Divisa
	 * @param tcPactado	-	Tipo de Cambio
	 * @param tcBase	-	Tipo de Cambio Base
	 * @param concepto	-	Concepto Operaci�n
	 * @param producto	-	Producto
	 * @param ctaAbono	-	Cuenta Abono
	 * @param importeAbono	-	Importe de Abono
	 * @param nombreBenef	-	Nombre Beneficiario para operaciones TRAN
	 * @param apPaterBenef	-	Apellido Paterno Beneficiario para operaciones TRAN
	 * @param apMaterBenef	-	Apellido Materno Beneficiario para operaciones TRAN
	 * @param paisBenef	-	Pa�s Beneficiario para operaciones TRAN
	 * @param bancoCorresponsal	-	Banco corresponsal
	 *
	 * @return TrxGP72VO	Vector contenedor de objetos de tipo TrxGP72VO
	 */
	public TrxGP72VO ejecutaLiquidacionRET(String folioMultiabono, String cveTcOper, String tipoAbono, String ctaCargo,
										   String divisaOperante, String contraDivisa, String importeTotOper, String tcPactado,
										   String tcBase, String concepto, String producto, String ctaAbono, String importeAbono,
										   String nombreBenef, String apePaternoBenef, String apeMaternoBenef,
										   String paisBenef, String bancoCorresponsal){
		EIGlobal.mensajePorTrace("TrxGP72BO::ejecutaLiquidacionRET:: Entrando....", EIGlobal.NivelLog.DEBUG);

	    MQQueueSession mqsess = null;
		TrxGP72DAO dao = new TrxGP72DAO();
		TrxGP72VO beanRes = new TrxGP72VO();

	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE,
	    								NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
    		bean = dao.consultaTrxGP72(rellenar(folioMultiabono, 10, ' ', 'D'),
    								   rellenar(cveTcOper, 7, ' ' , 'D'),
    								   rellenar(tipoAbono, 1),
	    							   rellenar(ctaCargo, 12, '0', 'I'),
	    							   rellenar(divisaOperante, 3),
	    							   rellenar(contraDivisa, 3),
	    							   rellenar(formatoCantidad(importeTotOper, 2), 17, ' ', 'D'),
	    							   rellenar(formatoCantidad(tcPactado, 6), 10, ' ', 'D'),
	    							   rellenar(formatoCantidad(tcBase, 6), 10, ' ', 'D'),
	    							   rellenar("", 10, ' ', 'I'),
	    							   rellenar("", 10, ' ', 'I'),
	    							   rellenar("", 10, ' ', 'I'),
	    							   rellenar("", 10, ' ', 'I'),
	    							   rellenar(Global.CENTRO_COSTOS_FX_ONLINE, 4, ' ', 'I'),
	    							   rellenar(Global.CENTRO_COSTOS_FX_ONLINE, 4, ' ', 'I'),
	    							   rellenar(Global.CENTRO_COSTOS_FX_ONLINE, 4, ' ', 'I'),
	    							   rellenar(concepto, 30),
	    							   rellenar(concepto, 30),
	    							   rellenar("FX", 2),
	    							   rellenar(Global.ADMUSR_MQ_USUARIO, 8, ' ', 'I'),
	    							   rellenar(producto, 4),
	    							   (producto.trim().equals("CHDO"))?rellenar(rellenar(ctaAbono, 12, '0', 'I'),20):rellenar(ctaAbono, 20),
	    							   rellenar(formatoCantidad(importeAbono ,2), 17, ' ', 'D'),
	    							   rellenar(nombreBenef, 40, ' ', 'D'),
	    							   rellenar(apePaternoBenef, 20, ' ', 'D'),
	    							   rellenar(apeMaternoBenef, 20, ' ', 'D'),
	    							   rellenar("", 16, ' ', 'D'),
	    							   rellenar(paisBenef, 3),
	    							   rellenar(bancoCorresponsal, 11, ' ', 'D'),
	    							   rellenar(concepto, 80));

		    EIGlobal.mensajePorTrace("TrxGP72BO::ejecutaLiquidacionRET:: Codigo operacion>>" + bean.getCodigoOperacion(),
		    		EIGlobal.NivelLog.DEBUG);

		    beanRes = bean.getGP72bean();
		    if(bean.getCodigoOperacion().endsWith("0000"))
		    	beanRes.setOperacionExitosa(true);
		    else if(bean.getCodigoOperacion().trim().equals("GPE0123") ||
		    		bean.getCodigoOperacion().trim().equals("GPE0162")){
		    	beanRes.setErroresReintento(true);
		    	beanRes.setOperacionExitosa(false);
		    	beanRes.setMensajeError(bean.getMensError());
		    }else if(bean.getCodigoOperacion().trim().equals("GPE0165")){
		    	beanRes.setErroresReintento(true);
		    	beanRes.setOperacionExitosa(false);
		    	beanRes.setMensajeError(bean.getMensError());
		    }else{
		    	beanRes.setOperacionExitosa(false);
		    	beanRes.setMensajeError(bean.getMensError());
		    }

	    }catch (Exception e){
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo ejecutaLiquidacionRET...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxGP72BO::ejecutaLiquidacionRET:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxGP72BO::ejecutaLiquidacionRET:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }
		EIGlobal.mensajePorTrace("TrxGP72BO::ejecutaLiquidacionRET:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return beanRes;
	}

	/**
	 * Metodo para rellenar una cantidad a decimales definidos
	 *
	 * @param cant		-	Cantidad a formatear
	 * @param decIn		-	Decimales a dar formato
	 * @return			-	Cantidad lista
	 */
	private String formatoCantidad(String cant, int decIn){
		EIGlobal.mensajePorTrace("TrxGP72BO::formatoCantidad:: cantidad recibida->" + cant, EIGlobal.NivelLog.DEBUG);
		int dec = 0;
		if(cant.indexOf(".")>0){
			EIGlobal.mensajePorTrace("TrxGP72BO::formatoCantidad:: Longitud cantidad->" + cant.length(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("TrxGP72BO::formatoCantidad:: Caracter punto->" + cant.indexOf("."), EIGlobal.NivelLog.DEBUG);
			dec = cant.length() - (cant.indexOf(".")+1);
			EIGlobal.mensajePorTrace("TrxGP72BO::formatoCantidad:: Numero de decimales->" + dec, EIGlobal.NivelLog.DEBUG);
			if(dec>=decIn)
				return cant.substring(0, cant.indexOf(".")) + cant.substring(cant.indexOf("."), cant.indexOf(".")+(decIn+1));
			else{
				for(int i=0; i<(decIn-dec); i++)
					cant += "0";
			}
		}else{
			cant += ".";
			for(int j=0; j<decIn; j++)
				cant += "0";
		}
		return cant;
	}
}