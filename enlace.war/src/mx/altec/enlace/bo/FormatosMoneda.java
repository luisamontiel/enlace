package mx.altec.enlace.bo;

import java.io.Serializable;
import java.text.*;

/**<B><I><code>FormatoMoneda</code></I></B>
* <P>Esta clase da m&eacute;todos para formatear n&uacute;ros y desplegarlos en formato num&eacute;rico.
* </P>
* @author Hugo Ruiz Zepeda
* @version 1.2.0
* @FechaCreacion 05/09/2002
* <P>Modificaciones:<BR>
* 11/10/2002 Se agreg&oacute; el m&eacute;todo <B><I>daTasFormateada</I></B><BR>.
* 06/11/2002 Se agreg&oacute; el m&eacute;todo <B><I>daNuevaTasaFormateada</I></B><BR>
* 08/11/2002 Se agreg&oacute; el m&eacute;todo <B><I>daMonedaConComas</I></B><BR>
* </P>
*/
public class FormatosMoneda extends Object implements Serializable
{
public static final String SimboloPeso="$";
public static final String representacionMinima="0.00";
public static final String mascaraTasas="00000000";
public static final String nuevaMascaraTasas="0000.0000";
public static final String mascaraMoneda="###,###,###,###,###,##0.00";
public static final int minimoEnteros=1;
public static final int posicionesCentavos=2;
private DecimalFormat df;
private int numeroEnteros;
private String complementoMascara="";
private DecimalFormat formatoTasa;

   public FormatosMoneda()
   {
      numeroEnteros=minimoEnteros;
      df=new DecimalFormat(representacionMinima);
   } // Fin constructor


   /**<B><I><code>daMonedaSinComas</code></I></B>
   *<P>Este m&eacute;todo da una cadena con un formato de moneda sin signo de moneda ni comas.
   *</P>
   */
   public String daMonedaSinComas(double cantidad)
   {
      String monedita=new String("");
      df.applyPattern(complementoMascara+representacionMinima);
      //df.applyPattern(representacionMinima);
      df.setMaximumFractionDigits(posicionesCentavos);
      df.setDecimalSeparatorAlwaysShown(true);
      //df.setMinimumIntegerDigits(minimoEnteros);
      //df.setMaximumIntegerDigits(getNumeroEnteros());
      //System.out.println(df.getGroupingSize());
      monedita=df.format(cantidad);



      return monedita;
   } // Fin m�todo daMonedaSinComas


   public int getNumeroEnteros()
   {
      return numeroEnteros;
   } // Fin m�todo getNumeroEnteros


   public void setNumeroEnteros(int numeroEnteros)
   {
      if(numeroEnteros>minimoEnteros)
      {
         this.numeroEnteros=numeroEnteros;
         creaComplementoMascara();
      } // Fin if
   } // Fin m�todo setNumeroEnteros


   private void creaComplementoMascara()
   {
      StringBuffer sb=new StringBuffer("");

      for(int i=minimoEnteros;i<numeroEnteros;i++)
      {
         sb.append("#");
      } // Fin for

      complementoMascara=new String(sb);
   } // Fin m�todo creaComplementoMascara


   private String getComplementoMascara()
   {
      return complementoMascara;
   } // Fin m�todo getComplementoMascara



   public String daTasaFormateada(String tasa)
   {
      String resultado="";
      long tasita;
      StringBuffer sb;

      if(tasa==null || tasa.length()<1)
         return resultado;

      try
      {
         tasita=Long.parseLong(tasa);
      }
      catch(NumberFormatException nfe)
      {
         return "";
      } // Fin try-catch

      formatoTasa=new DecimalFormat(mascaraTasas);

      resultado=formatoTasa.format(tasita);


      return resultado;
   } // Fin m�todo daTasaFormateada

 public String quitaPunto(String cadenaNumero)
  {
    StringBuffer sb=new StringBuffer(cadenaNumero);
    char c;

    for(int i=0;i<sb.length();i++)
    {
        c=sb.charAt(i);

        if(c=='.')
          sb.deleteCharAt(i);
    } // Fin for

    return new String(sb);
  } // Fin cadenaNumero


   /**Este m&eacute;todo da una cadena que representa a un n&uacute;mero flotante con cuatro decimales.
   */
   public String daNuevaTasaFormateada(String tasa)
   {
      String resultado="";
      DecimalFormat formatoTasa;
      float tasita;


      if(tasa==null || tasa.length()<1)
         return resultado;

      try
      {
         tasita=Float.parseFloat(tasa);
      }
      catch(NumberFormatException nfe)
      {
         return resultado;
      } // Fin try-catch


      formatoTasa=new DecimalFormat(nuevaMascaraTasas);
      resultado=formatoTasa.format(tasita);

      return resultado;
   } // Fin da NuevaMascaraTasas


   /**
   */
   public String daMonedaConComas(String cantidad, boolean incluyeSimbolo)
   {
      double dinero=0;
      String monedita;
      DecimalFormat nuevoFormato=new DecimalFormat(mascaraMoneda);


      try
      {
         dinero=Double.parseDouble(cantidad);
      }
      catch(NumberFormatException nfe)
      {
         return "";
      } // Fin try-catch

      if(incluyeSimbolo)
         monedita=new String(SimboloPeso);
      else
         monedita=new String("");

      monedita+=nuevoFormato.format(dinero);



      return monedita;
   } // Fin daMonedaConComas


} // Fin clase FormatosMoneda