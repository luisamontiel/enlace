package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import mx.altec.enlace.beans.NomPrePE68;
import mx.altec.enlace.dao.NomPrePersonasDAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

public class ConsultaDatosPersonaBO {

	public String consultarNombrePersona(String usr) {
		String nombre = "";
    	MQQueueSession mqsess = null;
		NomPrePersonasDAO dao = new NomPrePersonasDAO();
		NomPrePE68 c = new NomPrePE68();

		try {
    		mqsess = new MQQueueSession(
    				NP_JNDI_CONECTION_FACTORY, 
    				NP_JNDI_QUEUE_RESPONSE, 
    				NP_JNDI_QUEUE_REQUEST);
    		dao.setMqsess(mqsess);
    		c = dao.consultarPersona(usr);
    		nombre = c.getNombre() + " " + c.getPaterno() + " " + c.getMaterno();
		} catch (Exception x) {
		    EIGlobal.mensajePorTrace("ConsultaDatosPersonaBO - PE68 -" +
		    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
	    	} finally {
	    		try {
	    			mqsess.close();
	    		}catch(Exception x) {
				    EIGlobal.mensajePorTrace("ConsultaDatosPersonaBO - PE68 -" +
				    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
	    		}
	    	}
		EIGlobal.mensajePorTrace("Nombre obtenido: " + nombre, EIGlobal.NivelLog.DEBUG);
		return nombre.trim();
	}
}