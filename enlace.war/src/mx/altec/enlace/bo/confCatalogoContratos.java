/*
 * confCatalogoContratos.java
 *
 * Created on 30 de abril de 2002, 12:35 PM
 */

package mx.altec.enlace.bo;

import java.util.*;

/**
 * Contenedor del tipo de contratos
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class confCatalogoContratos implements java.io.Serializable {
    protected HashMap Contratos;

    /** Creates new confCatalogoContratos */
    public confCatalogoContratos () {
        Contratos = new HashMap ();
    }

    /**
     * Metodo para insertar un contrato
     * @param clave Clave del contrato
     * @param desc Descripci�n del contrato
     */
    public void agregaContrato (String clave, String desc) {
        Contratos.put (new String (clave), new String (desc));
    }

    /**
     * Metodo para obtener la descripci�n del contrato
     * @param clave Clave del contrato del que se quire la descripci�n
     * @return Descri�n del contrato
     */
    public String getDescripci�n (String clave) {
        return (String) Contratos.get (clave);
    }

    /**
     * Metodo para obtener todas las claves y descripciones de los contratos
     * @return Collection de contratos
     */
    public Collection getContratos () {
        return Contratos.values ();
    }


}
