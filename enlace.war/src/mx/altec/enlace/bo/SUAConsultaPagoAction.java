package mx.altec.enlace.bo;

import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.SUALCORM4;
import mx.altec.enlace.dao.SUAConsultaPagoDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class SUAConsultaPagoAction {
	/**
	 * Metodo para el llamado de las consultas de registros anteriores de SUALC
	 * LFER <VC> 25-Ago-2009
	 * 
	 * @param ctaCargo
	 * @param folioSua
	 * @param fechIni
	 * @param fechFin
	 * @param regPatronal
	 * @param lc
	 * @param opcion
	 * @return SUALCORM4
	 */
	public SUALCORM4 anterioresSUALC(String ctaCargo,String folioSua,String fechIni,String  fechFin,String regPatronal,String lc,int opcion){
		//opcion de busqueda
		
		SUAConsultaPagoDAO capturaDAO= new SUAConsultaPagoDAO();
		EIGlobal.mensajePorTrace("En anterioresSUALC.....................",EIGlobal.NivelLog.INFO);
		SUALCORM4  beanDao;
		List<SUALCORM4> RegCompletos= new ArrayList<SUALCORM4>();
		do{
			beanDao = capturaDAO.
				consultaSUALC(ctaCargo,folioSua,capturaDAO.formateaFecha(fechIni,1)
						,capturaDAO.formateaFecha(fechFin,1),regPatronal,lc,opcion);
			EIGlobal.mensajePorTrace("En anterioresSUALC Datos: "+ctaCargo
					+","+folioSua+","+capturaDAO.formateaFecha(fechIni,1)
					+","+capturaDAO.formateaFecha(fechFin,1)+","+regPatronal+","+lc+","+opcion
					,EIGlobal.NivelLog.INFO);
			//agregamos los registros a una lista tmp
			if(beanDao.getConsulta()!=null && beanDao.getConsulta().size()>0 ){
				for (SUALCORM4 sualcorm4 : beanDao.getConsulta()) {
					RegCompletos.add(sualcorm4);
				}
				//borramos la lista
				if(beanDao.getConsulta()!=null){
					beanDao.getConsulta().clear();
				}
			}
		}while(beanDao.getPagina() );
		//agregamos todos los registros
		if(beanDao!=null && RegCompletos.size()>0){
			beanDao.setConsulta(RegCompletos);
		}
		
		if(beanDao!=null  && beanDao.getConsulta()!=null){
			
			EIGlobal.mensajePorTrace("Datos de Consulta: "+beanDao.getConsulta().size(),EIGlobal.NivelLog.DEBUG);
		}else{
			EIGlobal.mensajePorTrace("No hay Datos.....................",EIGlobal.NivelLog.DEBUG);
			
		}
		return beanDao;
	}
	

}
