package mx.altec.enlace.bo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CatNominaValidaImport {

	ArrayList cta = new ArrayList();


	/**************************************************************
	 ***  Metodo encargado de validar una cadena de caracteres	***
	 ***  de tipo alfanumerico 									***
	 **************************************************************/
	public boolean validaCampoTexto(String campo)
	{
		System.out.println("");
		//System.out.println("CatNominaValidaImport - validaCampoTexto $ Cadena a Validar = " + campo);
		String caracteresPermitidos = "abcdefghijklmn�opqrstuvwxyz" + "ABCDEFGHIJKLMN�OPQRSTUVWXYZ"
										+ "�����" + "�����" + "0123456789" + " " + ".";
 		boolean campoValido = false;
		int carValido = 0;
		char letra;

		for (int i = 0; i < campo.length(); i++) {
			letra = campo.charAt(i);
			for (int j = 0; j < caracteresPermitidos.length(); j++) {
				if (letra == caracteresPermitidos.charAt(j))
					carValido++;
			}
		}

		if (carValido == campo.length())
			campoValido = true;

		System.out.println("CatNominaValidaImport - validaCampoTexto $ Cadena = " + campo + "  Valida =  " + campoValido );
		return campoValido;
	}


	/**************************************************************
	 ***  Metodo encargado de validar una cadena de caracteres	***
	 ***  de tipo numerico 									***
	 **************************************************************/
	public boolean validaCampoNumero(String dato) {
		System.out.println("");
		//String caracteresPermitidos = "0123456789" + "." + " ";
		String caracteresPermitidos = "0123456789" + " ";
		boolean datoValido = false;
		int carValido = 0;
		char letra;

		for (int i = 0; i < dato.length(); i++) {
			letra = dato.charAt(i);
			for (int j = 0; j < caracteresPermitidos.length(); j++) {
				if (letra == caracteresPermitidos.charAt(j))
					carValido++;
			}
		}

		if (carValido == dato.length())
			datoValido = true;

		System.out.println("CatNominaValidaImport - validaCampoNumero $ Cadena = "  + dato +  " Valida = " + datoValido );
		return datoValido;
	}
}