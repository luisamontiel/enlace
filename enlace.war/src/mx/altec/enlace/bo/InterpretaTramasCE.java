
// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * A Class class.
 * <P>
 * @author
 */
public class InterpretaTramasCE implements Serializable
{

private boolean regresoDatos;
private boolean seConecto;
private String datos;
public static final String ERROR="ERROR";
public static final String TUBO="TUBO";
public static final String OK="OK";

  /**
   * Constructor
   */
  public InterpretaTramasCE()
  {
    regresoDatos=false;
    seConecto=false;
    datos=new String("");

  } // Fin del contructor


  public boolean revisaTrama(String respuesta, int numeroEspacios)
  {
    String primeraParte;
    String aux1;
    StringTokenizer st;


    if(respuesta==null || respuesta.length()<1)
      return false;


    if(respuesta.substring(0, ERROR.length()).equalsIgnoreCase(ERROR))
      return false;

    if(respuesta.substring(0, TUBO.length()).equalsIgnoreCase(TUBO))
      return false;

    seConecto=true;

    if(numeroEspacios<1)
      return false;

    primeraParte=respuesta.substring(OK.length()+numeroEspacios);

    if(primeraParte!=null || primeraParte.length()<1)
      return false;

    if(primeraParte.trim().equalsIgnoreCase(OK))
    {
      st=new StringTokenizer(respuesta);
      st.nextToken();
      aux1=st.nextToken();

      if(aux1==null || aux1.length()<1)
        return false;

      regresoDatos=true;
      datos=aux1;

      return true;
    }
    else
    {
      return false;
    } // Fin primeraParte


  } // Fin metodo revisaTrama






  public boolean getSeConecto()
  {
    return seConecto;
  } // Fin m�todo getSeConecto


  public boolean getRegresoDatos()
  {
    return regresoDatos;
  } // Fin m�todo getRegresoDatos


  public String getDatos()
  {
    return datos;
  } // Fin m�todo getDatos


} // Fin clase InterpretaTramasCE

