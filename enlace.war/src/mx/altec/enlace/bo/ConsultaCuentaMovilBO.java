package mx.altec.enlace.bo;


import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.CtaMovilBean;
import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.dao.ConsultaCuentaMovilDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class ConsultaCuentaMovilBO {

	/**
	 * DAO Para Consultas a la BD
	 */
	ConsultaCuentaMovilDAO dao =  new ConsultaCuentaMovilDAO();

	/**
	 * Obtiene las cuentas moviles registradas en la base de datos
	 * @return listaContratos : listaContratos
	 */
	public List<CtaMovilBean> consultaCuentas(String contratoActual){
        EIGlobal.mensajePorTrace("INICIA ConsultaCuentaMovilBO ::  consultaCuentas()", EIGlobal.NivelLog.INFO);
		List<CtaMovilBean> listaContratos = new ArrayList<CtaMovilBean>();
		List<CuentaMovilBean> listaCuentas = new ArrayList<CuentaMovilBean>();
		try {
			listaCuentas = dao.consultaEdoCta(contratoActual);
		} catch (ArrayIndexOutOfBoundsException e) {
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("***Error de SQL " + e.getMessage() + " ****** Linea: "+lineaError[0], EIGlobal.NivelLog.DEBUG);
		}

		for(CuentaMovilBean dato : listaCuentas){
			CtaMovilBean beanCuentas = new CtaMovilBean();
			beanCuentas.setCtaMovil(dato.getNumeroMovil());
			beanCuentas.setTipoCuenta(dato.getTipoCuenta());
			beanCuentas.setTitular(dato.getTitular());
			beanCuentas.setNombreBanco(dato.getDescBanco());
			beanCuentas.setClaveBanco(dato.getCveBanco());

			EIGlobal.mensajePorTrace("ConsultaCuentaMovilBO -> consultaCuentas() --> MOVIL:" + beanCuentas.getCtaMovil(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ConsultaCuentaMovilBO -> consultaCuentas() -->  TIPO:" + beanCuentas.getTipoCuenta(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ConsultaCuentaMovilBO -> consultaCuentas() --> TITULAR:" + beanCuentas.getTitular(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ConsultaCuentaMovilBO -> consultaCuentas() --> BANCO:" + beanCuentas.getNombreBanco(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ConsultaCuentaMovilBO -> consultaCuentas() --> CVE BANCO:" + beanCuentas.getClaveBanco(), EIGlobal.NivelLog.INFO);
			listaContratos.add(beanCuentas);
		}

		return listaContratos;
	}

	/**
	 * @return dao : dao
	 */
	public ConsultaCuentaMovilDAO getDao() {
		return dao;
	}

	/**
	 * @param dao : dao
	 */
	public void setDao(ConsultaCuentaMovilDAO dao) {
		this.dao = dao;
	}
}