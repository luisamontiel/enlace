/**
 * VECTOR - Bussines Object para procesamiento de solicitudes de PDF
 */
package mx.altec.enlace.bo;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;
import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.beans.SolPDFBean;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.DescargaEstadoCuentaDAO;
import mx.altec.enlace.dao.NominaDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import static mx.altec.enlace.utilerias.NominaConstantes.*;
/**
 *
 * @author FWS - Vector
 * @since  Marzo 2015
 * Clase BO de registro de solicitud de informe de PDF Nomima
 *
 */
public class NominaBO {

	public NominaBO() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @author FSW - Vector
	 * @since  Marzo 2015
	 * @param request
	 * @param solPDFBean
	 * @return
	 */
	public boolean registraSolcitudPDF(HttpServletRequest request, SolPDFBean  solPDFBean)
	{
		NominaDAO dao = null;
		boolean resultado = false;
		dao = new NominaDAO();
		ServicioTux st = null;
		Hashtable ht = null;
		SolPDFBean solPDFBeanAnt = null;
		Date fchPermitida = null;
		String convertido = "";
		DateFormat fecha = null;
		EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: Entrando",	EIGlobal.NivelLog.INFO);
		try {

			fchPermitida = dao.consultaFecha();
			if(esFechaValida(solPDFBean.getFchPago(), fchPermitida))
			{
				if(fchFueraDeRango(solPDFBean.getFchPago(), 90))
				{
					solPDFBeanAnt = dao.consultaSolicitudPDF(solPDFBean);

					if(solPDFBeanAnt == null){
						EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: No existe la solicitud se inserta",	EIGlobal.NivelLog.INFO);

						try {
							EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: Generando Referencia",	EIGlobal.NivelLog.INFO);
							st = new ServicioTux();
							ht =  st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							solPDFBean.setReferencia( (Integer) ht.get("REFERENCIA"));
							EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: COD_ERROR{" + ht.get("COD_ERROR") + "]"
									+ " REFERENCIA [" +solPDFBean.getReferencia() + "] Referencia",	EIGlobal.NivelLog.INFO);

						} catch (Exception e1) {

							solPDFBean.setReferencia(0);
						EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: Excepcion" + e1.getMessage(),	EIGlobal.NivelLog.ERROR);
						}finally {
							ht = null;
						}


						resultado = dao.registraSolcitudPDF(solPDFBean);
						if(resultado){
							solPDFBean.setCodError(COD_SOLPDF_EXITO);
							solPDFBean.setMensaje(MSG_SOLPDF_EXITO);
						}else {
							solPDFBean.setCodError(COD_SOLPDF_ERROR_GEN);
							solPDFBean.setMensaje(MSG_SOLPDF_ERROR_GEN);
						}
					}else{
						EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: Ya se registro una solicitud para este archivo",	EIGlobal.NivelLog.INFO);
						solPDFBean.setCodError(COD_SOLPDF_ERROR_DUP);
						solPDFBean.setMensaje(MSG_SOLPDF_ERROR_DUP);
					}
				} else {
					EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: Fecha fuera de Rango",	EIGlobal.NivelLog.INFO);
					solPDFBean.setCodError("0012");
					solPDFBean.setMensaje("Solo se pueden solicitar informes de N&oacute;mina si la fecha de aplicacion es menor a 90 d&iacuteas");
				}
			}else{
				fecha = new SimpleDateFormat("dd/MM/yyyy");
				convertido = fecha.format(fchPermitida);
				EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: La fecha de aplicacion de la nomina es menor a " + convertido,	EIGlobal.NivelLog.INFO);
				solPDFBean.setCodError("0011");
				solPDFBean.setMensaje("Solo se pueden solicitar informes de n&oacute;mina cuya fecha aplicaci&oacute;n fue despues del " + convertido);
			}

		} catch (SQLException e) {

			solPDFBean.setCodError(COD_SOLPDF_ERROR_SQL);
			solPDFBean.setMensaje( MSG_SOLPDF_ERROR_SQL + " [ " + e.getSQLState() + "]");
			resultado = false;
			EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF " + e.getMessage(),	EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF " + e,	EIGlobal.NivelLog.ERROR);
			
		} finally {

			bitacoriza(request,solPDFBean );
		}

		EIGlobal.mensajePorTrace("NominaBO :: registraSolcitudPDF :: Saliendo [" + resultado + "]",	EIGlobal.NivelLog.INFO);
		return resultado;

	}
	/**
	 * Metodo para consultar comprobantes
	 * @since 28/05/2015
	 * @param solPDFBean filtros
	 * @return Map<Integer,SolPDFBean> Mapa con la informacion de la consulta
	 */
	public HashMap<Integer,SolPDFBean> consultaComprobantesPDF(SolPDFBean solPDFBean){

		HashMap<Integer,SolPDFBean> solicitudesPDF = null;

		NominaDAO dao = null;

		try{
			dao = new NominaDAO();

			solicitudesPDF = dao.consultaComprobantesPDF(solPDFBean);

		} catch (SQLException e) {
			solPDFBean.setCodError(COD_SOLPDF_ERROR_SQL);
			solPDFBean.setMensaje( MSG_CONCOM_ERROR_SQL + " [ " + e.getSQLState() + "]");		
			EIGlobal.mensajePorTrace("NominaBO :: consultaComprobantesPDF :: ERROR " + e.getMessage(),	EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("NominaBO :: consultaComprobantesPDF :: ERROR " + e,	EIGlobal.NivelLog.ERROR);			
		}
		
		return solicitudesPDF;
	
	}
	
	/**
	 * Funcion de registro de solicitud de informe PDF
	 * @author  FSW-Vector
	 * @since   Marzo 2015
	 * @param   request peticion
	 * @param   solPDFBean bean de registro de solicitud
	 */	
	public void bitacoriza(HttpServletRequest request, SolPDFBean solPDFBean){

		BitaTCTBean beanTCT = new BitaTCTBean();

		final HttpSession sess = request.getSession(false);
		final BaseResource ses = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("NominaBO :: bitacoriza :: Entrando",	EIGlobal.NivelLog.INFO);
		BitaHelper bh = new BitaHelperImpl(request, ses, sess);
		beanTCT = bh.llenarBeanTCT(beanTCT);
		beanTCT.setCuentaOrigen("");
		beanTCT.setCuentaDestinoFondo("");
		beanTCT.setReferencia(solPDFBean.getReferencia());
		beanTCT.setTipoOperacion(solPDFBean.getTipoOper());
		beanTCT.setCodError(solPDFBean.getCodError());
		beanTCT.setUsuario(solPDFBean.getUsuarioAlta());
		EIGlobal.mensajePorTrace("NominaBO :: bitacoriza :: Saliendo",	EIGlobal.NivelLog.INFO);
		
	}
	/**
	 * Metodo para crear pista
	 * @since 28/05/2015
	 * @param request peticion
	 * @param idFlujo identificador del flujo
	 * @param idPaso numero de paso
	 * @return BitaTransacBean bean con la informacion a bitacorizar
	 */
	public BitaTransacBean creaPista(HttpServletRequest request, String idFlujo, String idPaso){
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute("session");

		BitaHelper bh = new BitaHelperImpl(request, session,sess);


		bh.incrementaFolioFlujo(idFlujo);
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(idPaso);
		bt.setContrato(session.getContractNumber());
		bt.setIdFlujo(idFlujo);
		if(null != session.getToken().getSerialNumber()) {
			bt.setIdToken(session.getToken().getSerialNumber());
		}

		return bt;

	}
	/**
	 * Metodo para registrar la pista
	 * @since 28/05/2015
	 * @param bt bean con la informacion a registrar
	 */
	public void registraPista(BitaTransacBean bt){
		if (("ON").equals(Global.USAR_BITACORAS.trim())){
			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch (SQLException e){
				EIGlobal.mensajePorTrace("***NominaPagosBO.class &Error de excepcion SQL"+e, EIGlobal.NivelLog.INFO);
			}catch(Exception e){
				EIGlobal.mensajePorTrace("***NominaPagosBO.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
			}
		}
	}

	/**
	 *
	 * @param cuentasDescargaBean bean
	 * @return boolean se inserto o no
	 * @throws BusinessException exepcion a manejar
	 * Funcion de registro de solicitud de descarga de estado de cuenta
	 */

	public boolean insertaRegistroDescarga(CuentasDescargaBean cuentasDescargaBean) throws BusinessException {
		boolean cargado=false;
		int resultado = 0;
		NominaDAO dao = null;
				try{
					dao = new NominaDAO();
					resultado = dao.insertaRegistroDescargaPDF(cuentasDescargaBean);

				}catch(SQLException e){
					cargado = false;
					EIGlobal.mensajePorTraceExcepcion(e);
					EIGlobal.mensajePorTrace("NominaBO :: insertaRegistroDescarga ::",EIGlobal.NivelLog.ERROR);
				}


				if (resultado > 0){
					cargado=true;
				}else {
					cargado=false;
				}

		return cargado;
	}

	/**
	 *
	 * @param fchPermitida
	 * @param fechaAplicacion
	 * @return
	 * Valida la FECHA Inicio para solicitud de informes de Nomina
	 */

	public boolean esFechaValida (Date fechaAplicacion, Date fchPermitida)
	{
		boolean result = true;

		EIGlobal.mensajePorTrace("***NominaPagosBO.class : Fecha Permitida "+fchPermitida.toString(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***NominaPagosBO.class : Fecha Aplicaci�n "+fechaAplicacion.toString(), EIGlobal.NivelLog.INFO);

		if(fechaAplicacion.before(fchPermitida))
		{
			result = false;
		}
		return result;
	}


	/**
	 *
	 * @param fchAplic
	 * @param dias
	 * @return true or false
	 * Valida que la fecha de aplicacion de la nomina este en un rago menor a 90 d�as
	 * sobre la fecha actual pasa poder solicitar un informe de N�mina
	 */
	public static boolean fchFueraDeRango(Date fchAplic, int dias) {

		  	System.out.println(fchAplic);

		  	Date fechaSistema = new Date();
		  	System.out.println(fechaSistema);
		  	// Obtiene la diferencia en dias entre el rango de fechas inicial y final.
		  	long diferencia = (fechaSistema.getTime() - fchAplic.getTime()) / (1000 * 60 * 60 * 24);
		  	System.out.println("Diferencia: "+ diferencia);
		  	if (Math.round(diferencia) >= dias) {
		  		System.out.println("Fuera de rango: "+ diferencia);
				return false;
		  	} else {
		  		System.out.println("Dentro de rango: "+ diferencia);
				return true;
		  	}
	}
}