package mx.altec.enlace.bo;

import mx.altec.enlace.utilerias.EIGlobal;

//####################################################################
//
//      Modificacion: validar Nueva version sua y fechalimite
//                    para aceptar version anterior.
//      Proyecto    : SUA 2005
//      Cod         : MX2005-21100
//      Programador : Carlos Aleman Rosas.
//      clave en cod: W3xx
//#################################################################
public class SuaImpuesto
{
	private static double divisorSUA = 100;

	public double impTresEnfermedadCuota         = 0.0;
	public double impTresEnfermedadExcedente     = 0.0;
	public double impTresEnfermedadPrestaciones  = 0.0;
	public double impTresGastosMedicos           = 0.0;
	public double impTresRiesgosDeTrabajo        = 0.0;
	public double impTresInvalidezVida           = 0.0;
	public double impTresGuarderiaPrestaciones   = 0.0;
	public double impTresSegurosImss             = 0.0;
	public double impTresRetiroCuota             = 0.0;
	public double impTresRetiroRecargos          = 0.0;
	public double impTresCesantiaVejezPatronal   = 0.0;
	public double impTresCesantiaVejezTrabajador = 0.0;
	public double impTresCesantiaVejezRecargos   = 0.0;
	public double impTresAportacionVoluntaria    = 0.0;
	public double impTresAportacionPatronal      = 0.0;
	public double impTresAmortizacionCredito     = 0.0;

	public double impTresCampos24y25R5_AporPatronal =00.0;
	public double impTresCampos24y25R5_AmorCredito  =00.0;

//	public double impTresConceptoNoDefinido      = 0; se elimina por nuevo formato
	public double impTresCreditoInfonavit        = 0.0;

	public double impCuatroSalarioDiarioInt      = 0.0;      // Sal. Dia. Int. SUA2005-MX-2005-1100

	public double impCincoEnfermedadCuota             = 0.0;
	public double impCincoEnfermedadExcedente         = 0.0;
	public double impCincoEnfermedadPrestaciones      = 0.0;
	public double impCincoGastosMedicos               = 0.0;
	public double impCincoRiesgosDeTrabajo            = 0;
	public double impCincoInvalidezVida               = 0;
	public double impCincoGuarderiaPrestaciones       = 0;
	public double impCincoSegurosImss                 = 0;
	public double impCincoSegurosImssActualizacion    = 0;
	public double impCincoSegurosImssRecargos         = 0;
	public double impCincoRetiro                      = 0;
	public double impCincoCesantiaVejez               = 0;
	public double impCincoRetiroCesantiaVejez         = 0;
	public double impCincoRetiroCesantiaActualizacion = 0;
	public double impCincoRetiroCesantiaVejezRecargos = 0;
	public double impCincoAportacionVoluntaria        = 0;
	public double impCincoAportacionPatronal          = 0;
	public double impCincoAmortizacionCredito         = 0;
	public double impCincoInfonavitSubtotal           = 0;
	public double impCincoInfonavitActualizacion      = 0;
	public double impCincoInfonavitRecargos           = 0;

	public double impCincoAportacionComplementaria    = 0;  // Aport. Complementarias  SUA2005-MX-2005-1100
	public double impCincoInfonavitMultas             = 0;  // Multas Infonavit  SUA2005-MX-2005-1100
	public double impCincoFundemexDonativo            = 0;  // Fundamex Donativos  SUA2005-MX-2005-1100

	public double impCincoTotalSegurosImss            = 0;
	public double impCincoImporteAfore                = 0;
	public double impCincoSubtotalAfore               = 0;
	public double impCincoTotalAfore                  = 0;
	public double impCincoSubTotalInfonavit           = 0;
	public double impCincoTotalInfonavit              = 0;
	public double impCincoGranTotal                   = 0;

	public double impSeisTotalImss4Seguros            = 0;
	public double impSeisTotalImssConcentrar          = 0;
	public double impSeisTotalAportacionPatronal      = 0;
	public double impSeisTotalAmortizacionCredito     = 0;

	public double sumaD35cuando36esCeroR3             = 0;//W3xx
	public double sumaD35cuando36noCeroR3             = 0;//W3xx

	public SuaImpuesto()
	{
	}

//	public double Double.parseDouble(String dato,int decimales)
//	{
//		double importe;
//
//		int x=dato.length();
//		int len=0;
//		len=x-decimales;
//		System.out.println("----0000----"+dato);
//		dato=dato.substring(0,len)+"."+dato.substring(len);
//
//		try
//		{
//			importe = Double.valueOf(dato.trim());
//
//		}
//		catch( Exception exceptionLeer )
//		{
//			importe = 0.0;
//			EIGlobal.mensajePorTrace( "***SuaImpuesto.class Excepcion %Double.parseDouble() >> " +
//					exceptionLeer.getMessage() + " <<", EIGlobal.NivelLog.INFO);
//		}
//		System.out.println("----1111----"+importe);
//		return(importe);
//	}


	public void ProcesaRegistroDOS(SuaRegistro rsDatos) {
		EIGlobal.mensajePorTrace( "***SuaImpuesto.class & ProcesaRegistroDOS &", EIGlobal.NivelLog.INFO);
	}

	public void ProcesaRegistroTRES(SuaRegistro rsDatos) {
		EIGlobal.mensajePorTrace( "***SuaImpuesto.class # ProcesaRegistroTRES #", EIGlobal.NivelLog.DEBUG);

		impTresEnfermedadCuota         += Double.parseDouble(rsDatos.regTresEnfermedadCuota)         /divisorSUA;
		impTresEnfermedadExcedente     += Double.parseDouble(rsDatos.regTresEnfermedadExcedente)     /divisorSUA;
		impTresEnfermedadPrestaciones  += Double.parseDouble(rsDatos.regTresEnfermedadPrestaciones)  /divisorSUA;
		impTresGastosMedicos           += Double.parseDouble(rsDatos.regTresGastosMedicos)           /divisorSUA;
		impTresRiesgosDeTrabajo        += Double.parseDouble(rsDatos.regTresRiesgosDeTrabajo)        /divisorSUA;
		impTresInvalidezVida           += Double.parseDouble(rsDatos.regTresInvalidezVida)           /divisorSUA;
		impTresGuarderiaPrestaciones   += Double.parseDouble(rsDatos.regTresGuarderiaPrestaciones)   /divisorSUA;
		impTresSegurosImss             += Double.parseDouble(rsDatos.regTresSegurosImss)             /divisorSUA;

		impTresRetiroCuota             += Double.parseDouble(rsDatos.regTresRetiroCuota)             /divisorSUA;
		impTresRetiroRecargos          += Double.parseDouble(rsDatos.regTresRetiroRecargos)          /divisorSUA;
		impTresCesantiaVejezPatronal   += Double.parseDouble(rsDatos.regTresCesantiaVejezPatronal)   /divisorSUA;
		impTresCesantiaVejezTrabajador += Double.parseDouble(rsDatos.regTresCesantiaVejezTrabajador) /divisorSUA;
		impTresCesantiaVejezRecargos   += Double.parseDouble(rsDatos.regTresCesantiaVejezRecargos)   /divisorSUA;
		impTresAportacionVoluntaria    += Double.parseDouble(rsDatos.regTresAportacionVoluntaria)    /divisorSUA;

		impTresAportacionPatronal      += Double.parseDouble(rsDatos.regTresAportacionPatronal)		/divisorSUA;
		impTresAmortizacionCredito     += Double.parseDouble(rsDatos.regTresAmortizacionCredito)     /divisorSUA;
	//	impTresConceptoNoDefinido      += Double.parseDouble(rsDatos.regTresConceptoNoDefinido)      /divisorSUA;   se elimina por nuevo formato
		impTresCampos24y25R5_AporPatronal = Double.parseDouble(rsDatos.regTresAportacionPatronal)      /divisorSUA;
		impTresCampos24y25R5_AmorCredito  = Double.parseDouble(rsDatos.regTresAmortizacionCredito)     /divisorSUA;
		if( Double.parseDouble(rsDatos.regTresCreditoInfonavit) > 0)
			rsDatos.trabajadoresConCredito++;

//Nuvas validaciones W3xx
		if (impTresCampos24y25R5_AmorCredito==0)
			sumaD35cuando36esCeroR3 = sumaD35cuando36esCeroR3+impTresCampos24y25R5_AporPatronal;
		if (impTresCampos24y25R5_AmorCredito!=0)
			sumaD35cuando36noCeroR3 = sumaD35cuando36noCeroR3+impTresCampos24y25R5_AporPatronal;
//fin nuevas validaciones

	}

	public void ProcesaRegistroCUATRO(SuaRegistro rsDatos) {
		EIGlobal.mensajePorTrace( "***SuaImpuesto.class & ProcesaRegistroCUATRO &", EIGlobal.NivelLog.INFO);

		// (x/f(x) {x = '10'} SUA2005 - MX-2005-1100

		if (rsDatos.regCuatroTipoMovIncidencia1.equals("10"))
			impCuatroSalarioDiarioInt += Double.parseDouble(rsDatos.regCuatroSalarioDiarioInt1) /divisorSUA;
		if (rsDatos.regCuatroTipoMovIncidencia2.equals("10"))
			impCuatroSalarioDiarioInt += Double.parseDouble(rsDatos.regCuatroSalarioDiarioInt2) /divisorSUA;
		if (rsDatos.regCuatroTipoMovIncidencia3.equals("10"))
			impCuatroSalarioDiarioInt += Double.parseDouble(rsDatos.regCuatroSalarioDiarioInt3) /divisorSUA;
		if (rsDatos.regCuatroTipoMovIncidencia4.equals("10"))
			impCuatroSalarioDiarioInt += Double.parseDouble(rsDatos.regCuatroSalarioDiarioInt4) /divisorSUA;
		if (rsDatos.regCuatroTipoMovIncidencia5.equals("10"))
			impCuatroSalarioDiarioInt += Double.parseDouble(rsDatos.regCuatroSalarioDiarioInt5) /divisorSUA;
		if (rsDatos.regCuatroTipoMovIncidencia6.equals("10"))
			impCuatroSalarioDiarioInt += Double.parseDouble(rsDatos.regCuatroSalarioDiarioInt6) /divisorSUA;
		if (rsDatos.regCuatroTipoMovIncidencia7.equals("10"))
			impCuatroSalarioDiarioInt += Double.parseDouble(rsDatos.regCuatroSalarioDiarioInt7) /divisorSUA;
	}

	public void ProcesaRegistroCINCO(SuaRegistro rsDatos) {
		EIGlobal.mensajePorTrace( "***SuaImpuesto.class & ProcesaRegistroCINCO &", EIGlobal.NivelLog.INFO);

		impCincoEnfermedadCuota             += Double.parseDouble(rsDatos.regCincoEnfermedadCuota)             /divisorSUA;
		impCincoEnfermedadExcedente         += Double.parseDouble(rsDatos.regCincoEnfermedadExcedente)         /divisorSUA;
		impCincoEnfermedadPrestaciones      += Double.parseDouble(rsDatos.regCincoEnfermedadPrestaciones)      /divisorSUA;
		impCincoGastosMedicos               += Double.parseDouble(rsDatos.regCincoGastosMedicos)               /divisorSUA;
		impCincoRiesgosDeTrabajo            += Double.parseDouble(rsDatos.regCincoRiesgosDeTrabajo)            /divisorSUA;
		impCincoInvalidezVida               += Double.parseDouble(rsDatos.regCincoInvalidezVida)               /divisorSUA;
		impCincoGuarderiaPrestaciones       += Double.parseDouble(rsDatos.regCincoGuarderiaPrestaciones)       /divisorSUA;
		impCincoSegurosImss                 += Double.parseDouble(rsDatos.regCincoSegurosImss)                 /divisorSUA;
		impCincoSegurosImssActualizacion    += Double.parseDouble(rsDatos.regCincoSegurosImssActualizacion)    /divisorSUA;
		impCincoSegurosImssRecargos         += Double.parseDouble(rsDatos.regCincoSegurosImssRecargos)         /divisorSUA;
		impCincoRetiro                      += Double.parseDouble(rsDatos.regCincoRetiro)                      /divisorSUA;
		impCincoCesantiaVejez               += Double.parseDouble(rsDatos.regCincoCesantiaVejez)               /divisorSUA;
		impCincoRetiroCesantiaVejez         += Double.parseDouble(rsDatos.regCincoRetiroCesantiaVejez)         /divisorSUA;
		impCincoRetiroCesantiaActualizacion += Double.parseDouble(rsDatos.regCincoRetiroCesantiaActualizacion) /divisorSUA;
		impCincoRetiroCesantiaVejezRecargos += Double.parseDouble(rsDatos.regCincoRetiroCesantiaVejezRecargos) /divisorSUA;
		impCincoAportacionVoluntaria        += Double.parseDouble(rsDatos.regCincoAportacionVoluntaria)        /divisorSUA;
		impCincoAportacionPatronal          += Double.parseDouble(rsDatos.regCincoAportacionPatronal)          /divisorSUA;
		impCincoAmortizacionCredito         += Double.parseDouble(rsDatos.regCincoAmortizacionCredito)         /divisorSUA;
		impCincoInfonavitSubtotal           += Double.parseDouble(rsDatos.regCincoInfonavitSubtotal)           /divisorSUA;
		impCincoInfonavitActualizacion      += Double.parseDouble(rsDatos.regCincoInfonavitActualizacion)      /divisorSUA;
		impCincoInfonavitRecargos           += Double.parseDouble(rsDatos.regCincoInfonavitRecargos)           /divisorSUA;

		// (x/f(x) {0 <= x <= 9} SUA2005 - MX-2005-1100
		impCincoAportacionComplementaria    += Double.parseDouble(rsDatos.regCincoAportacionComplementaria)    /divisorSUA;
		impCincoInfonavitMultas             += Double.parseDouble(rsDatos.regCincoInfonavitMultas)             /divisorSUA;
		impCincoFundemexDonativo            += Double.parseDouble(rsDatos.regCincoFundemexDonativo)            /divisorSUA;

		impCincoTotalSegurosImss   = Double.parseDouble(rsDatos.regCincoSegurosImss) /divisorSUA;
		impCincoTotalSegurosImss  += impCincoSegurosImssActualizacion;
		impCincoTotalSegurosImss  += impCincoSegurosImssRecargos;

		impCincoImporteAfore   = Double.parseDouble(rsDatos.regCincoRetiroCesantiaVejez) /divisorSUA;

		impCincoSubtotalAfore  = Double.parseDouble(rsDatos.regCincoRetiroCesantiaVejez) /divisorSUA;
		impCincoSubtotalAfore += impCincoRetiroCesantiaActualizacion;
		impCincoSubtotalAfore += impCincoRetiroCesantiaVejezRecargos;

		impCincoTotalAfore  = Double.parseDouble(rsDatos.regCincoRetiroCesantiaVejez) /divisorSUA;
		impCincoTotalAfore += impCincoRetiroCesantiaActualizacion;
		impCincoTotalAfore += impCincoRetiroCesantiaVejezRecargos;
		impCincoTotalAfore += impCincoAportacionVoluntaria;
        impCincoTotalAfore += impCincoAportacionComplementaria;

		impCincoSubTotalInfonavit  = impCincoAportacionPatronal;
		impCincoSubTotalInfonavit += impCincoAmortizacionCredito;

		impCincoTotalInfonavit    = impCincoAportacionPatronal;
		impCincoTotalInfonavit   += impCincoAmortizacionCredito;
		impCincoTotalInfonavit   += impCincoInfonavitSubtotal;
		impCincoTotalInfonavit   += impCincoInfonavitActualizacion;
		impCincoTotalInfonavit   += impCincoInfonavitRecargos;

		// Sumas al total de infonavit SUA2005 - MX-2005-1100
		impCincoTotalInfonavit   += impCincoInfonavitMultas;
		impCincoTotalInfonavit   += impCincoFundemexDonativo;

		impCincoGranTotal  = Double.parseDouble(rsDatos.regCincoSegurosImss)         /divisorSUA;
		impCincoGranTotal += impCincoSegurosImssActualizacion;
		impCincoGranTotal += impCincoSegurosImssRecargos;
		impCincoGranTotal += Double.parseDouble(rsDatos.regCincoRetiroCesantiaVejez) /divisorSUA;
		impCincoGranTotal += impCincoRetiroCesantiaActualizacion;
		impCincoGranTotal += impCincoRetiroCesantiaVejezRecargos;
		impCincoGranTotal += impCincoAportacionVoluntaria;

		impCincoGranTotal += impCincoAportacionPatronal;
		impCincoGranTotal += impCincoAmortizacionCredito;
		impCincoGranTotal += impCincoInfonavitSubtotal;
		impCincoGranTotal += impCincoInfonavitActualizacion;
		impCincoGranTotal += impCincoInfonavitRecargos;

		// sumar al gran total SUA2005 - MX-2005-1100
		impCincoGranTotal += impCincoInfonavitMultas;
		impCincoGranTotal += impCincoFundemexDonativo;
        impCincoGranTotal += impCincoAportacionComplementaria;
	}

	public void ProcesaRegistroSEIS(SuaRegistro rsDatos)
	{
		EIGlobal.mensajePorTrace( "***SuaImpuesto.class & ProcesaRegistroSEIS &", EIGlobal.NivelLog.INFO);

		impSeisTotalImss4Seguros        += Double.parseDouble(rsDatos.regSeisTotalImss4Seguros)        /divisorSUA;
		impSeisTotalImssConcentrar      += Double.parseDouble(rsDatos.regSeisTotalImssConcentrar)      /divisorSUA;
		impSeisTotalAportacionPatronal  += Double.parseDouble(rsDatos.regSeisTotalAportacionPatronal)  /divisorSUA;
		impSeisTotalAmortizacionCredito += Double.parseDouble(rsDatos.regSeisTotalAmortizacionCredito) /divisorSUA;
	}

	public void impuestoRegistro(SuaRegistro regSuaDatos)
	{
		EIGlobal.mensajePorTrace( "***SuaImpuesto.class # impuestoRegistro #", EIGlobal.NivelLog.DEBUG);

		try
		{
			switch ( regSuaDatos.tipoRegistro )
			{
				case 2: ProcesaRegistroDOS(regSuaDatos);
				break;
				case 3: ProcesaRegistroTRES(regSuaDatos);
				break;
				case 4: ProcesaRegistroCUATRO(regSuaDatos);
				break;
				case 5: ProcesaRegistroCINCO(regSuaDatos);
				break;
				case 6: ProcesaRegistroSEIS(regSuaDatos);
				break;
			}
		}
		catch ( Exception E )
		{
			EIGlobal.mensajePorTrace( "***SuaImpuesto.class Excepcion %impuestoRegistro() >> "+
					E.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace( "***SuaImpuesto.class & Termina impuestoRegistro &", EIGlobal.NivelLog.INFO);
	}
}