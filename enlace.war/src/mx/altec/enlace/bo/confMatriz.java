/*
 * confMatriz.java
 *
 * Created on 30 de abril de 2002, 12:32 PM
 */

package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

/**
 * Contenedor para la respuesta de tuxedo para generar la gr�fica
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class confMatriz
{
	protected File Archivo;
	protected HashMap Documentos;
	protected String Contrato;
	protected String mesIni;
	protected String mesLim;

    /**
     * Creates new confMatriz
     */
    public confMatriz (File archivo, String contrato, String fechaIni, String fechaLim)
        throws IOException, FileNotFoundException, Exception {
        mesIni = fechaIni.substring (fechaIni.indexOf ('/') + 1, fechaIni.lastIndexOf ('/'));
        mesLim = fechaLim.substring (fechaLim.indexOf ('/') + 1, fechaLim.lastIndexOf ('/'));
        Documentos = new HashMap ();
        Contrato = contrato;
        Archivo = archivo;
        cargaDatos ();
    }

    /**
     * Metodo para cargar los datos necesarios del archivo
     */
    protected void cargaDatos () throws IOException, FileNotFoundException, Exception {
        RandomAccessFile reader = new RandomAccessFile (Archivo, "r");
        String Linea;
        Linea = reader.readLine ();
        if (Linea.indexOf ("ERROR") != -1) {
            throw new Exception ("Error en la transmisi&oacute;n.");
        }
        while ((Linea = reader.readLine ()) != null) {
            pdTokenizer tokenizer = new pdTokenizer (Linea, ";", 2);
            char estatus = 'R';
            float importe;
            String ncuenta = Contrato;
            String fechaven = null;
            String proveedor;
            if (Linea.startsWith ("1")) {
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                proveedor = tokenizer.nextToken ();
                tokenizer.nextToken ();
                importe = Float.parseFloat (tokenizer.nextToken ());
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                fechaven = tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                estatus = tokenizer.nextToken ().charAt (0);
            } else if (Linea.startsWith ("2")) {
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                proveedor = tokenizer.nextToken ();
                tokenizer.nextToken ();
                importe = Float.parseFloat (tokenizer.nextToken ());
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
            } else {
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                proveedor = tokenizer.nextToken ();
                tokenizer.nextToken ();
                tokenizer.nextToken ();
                importe = Float.parseFloat (tokenizer.nextToken ());
                estatus = tokenizer.nextToken ().charAt (0);
            }
            Contenedor temp = new Contenedor (estatus, importe, ncuenta,
                                            fechaven, proveedor);
            if (Documentos.containsKey (proveedor)) {
                ((ArrayList) Documentos.get (proveedor)).add (temp);
            } else {
                Documentos.put (proveedor, new ArrayList ());
                ((ArrayList) Documentos.get (proveedor)).add (temp);
            }
        }
    }

    /**
     * Metodo para generar los datos necesarios para armar la gr�fica
     * @param proveedor Proveedor del que se quiere generar la gr�fica
     * @return Mapa con los datos necesarios para armar la gr�fica
     */
    public HashMap generaGrafica (String proveedor, char tipo) {
        HashMap mapFechas;
        ArrayList Proveedor;
        int mesIni, mesLim;
        mesIni = Integer.parseInt (this.mesIni);
        mesLim = Integer.parseInt (this.mesLim);
        mapFechas = creaMapa (mesIni, mesLim);
        if (null == proveedor || proveedor.equals ("") || proveedor.equals ("0")) {
            Iterator iterator = Documentos.keySet ().iterator ();
            while (iterator.hasNext ()) {
                String llave = (String) iterator.next ();
                Proveedor = (ArrayList) Documentos.get (llave);
                ListIterator liProveedor = Proveedor.listIterator ();
                while (liProveedor.hasNext ()) {
                    Contenedor cont = (Contenedor) liProveedor.next ();
                    if (cont.getFechaVencimiento () != null ) {
                        llave = obtenMes (
                            cont.getFechaVencimiento ().get (Calendar.MONTH) + 1);
                        HashMap Temp = (HashMap) mapFechas.get (llave);
                        Float count = (Float) Temp.get (estatus (cont.getEstatus ()));
                        if (tipo == 'I') {
                            count = new Float (count.floatValue () + cont.getImporte ());
                            Temp.put (estatus (cont.getEstatus ()), count);
                        } else {
                            count = new Float (count.floatValue () + 1);
                            Temp.put (estatus (cont.getEstatus ()), count);
                        }
                        Long total = (Long) Temp.get ("Recibidos");
                        total = new Long (total.longValue () + 1);
                        Temp.put ("Recibidos", total);
                    }
                }
            }
        } else {
            Proveedor = (ArrayList) Documentos.get (proveedor);
            ListIterator liProveedor = Proveedor.listIterator ();
            while (liProveedor.hasNext ()) {
                Contenedor cont = (Contenedor) liProveedor.next ();
                if (cont.getFechaVencimiento () != null ) {
                    String llave = obtenMes (
                        cont.getFechaVencimiento ().get (Calendar.MONTH) + 1);
                    HashMap Temp = (HashMap) mapFechas.get (llave);
                    Float count = (Float) Temp.get (estatus (cont.getEstatus ()));
                    if (tipo == 'I') {
                        count = new Float (count.floatValue () + cont.getImporte ());
                        Temp.put (estatus (cont.getEstatus ()), count);

						//se comento para que compilara provisionalmente 13/10/2002
                        /*Long total = (Float) Temp.get ("Recibidos");
                        total = new Float (total.floatValue () + cont.getImporte ());
                        Temp.put ("Recibidos", total);*/
                    } else {
                        count = new Float (count.floatValue () + 1);
                        Temp.put (estatus (cont.getEstatus ()), count);
						//se comento para que compilara provisionalmente 13/10/2002
                        /*Long total = (Float) Temp.get ("Recibidos");
                        total = new Float (total.floatValue () + 1);
                        Temp.put ("Recibidos", total);*/
                    }
                }
            }
        }
        return mapFechas;
    }

    /**
     * Metodo para generar el mapa de fechas
     * @param mesIni Mes en que inicia la generaci�n de la gr�fica
     * @param mesLim Mes l�mite de la gr�fica
     */
    protected HashMap creaMapa (int mesIni, int mesLim) {
        HashMap map = new HashMap ();
        while (mesIni < mesLim) {
            HashMap temp = new HashMap (7);
            temp.put ("Por Pagar", new Float (0));
            temp.put ("Pagados", new Float (0));
            temp.put ("Liberados no pagados", new Float (0));
            temp.put ("Anticipados", new Float (0));
            temp.put ("Cancelados", new Float (0));
            temp.put ("Vencidos", new Float (0));
            temp.put ("Rechazados", new Float (0));
            temp.put ("Recibidos", new Float (0));
            map.put (obtenMes (mesIni++), temp);
        }
        return map;
    }

    /**
     * Metodo para obtener el mes a partir de un entero
     * @param mes Mes en entero
     * @return Nombre del mes
     */
    protected String obtenMes (int mes) {
        switch (mes) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Novimbre";
            case 12:
                return "Diciembre";
            default:
                return null;
        }
    }


    /**
     * Metodo para obtener la descripci�n del estatus
     * @param estatus Estado del pago
     * @return Descripci�n del estatus
     */
    protected String estatus (char estatus) {
        switch (estatus) {
            case 'R':
                return "Por Pagar";
            case 'P':
                return "Pagados";
            case 'L':
                return "Liberados no pagados";
            case 'A':
                return "Anticipados";
            case 'C':
                return "Cancelados";
            case 'V':
                return "Vencidos";
            case 'Z':
                return "Rechazados";
            default:
                return "Desconocido o erroneo";
        }
    }
    /**
     * Contenedor de datos del archivo
     *
     * @author Horacio Oswaldo Ferro D�az
     * @version 1.0
     */
    class Contenedor {

        protected char Estatus;
        protected float Importe;
        protected String numCuenta;
        protected GregorianCalendar fechaVencimiento;
        protected String claveProveedor;

        /**
         * Metodo para construir un contenedor
         * @param estatus Estatus del pago
         * @param importe Importe del pago
         * @param cuenta Numero de cuenta
         * @param fecha Fecha de vencimiento
         * @param clave Clave del proveedor
         */
        public Contenedor (char estatus, float importe, String ncuenta,
                            String fechaven, String proveedor) {
            Estatus = estatus;
            Importe = importe;
            numCuenta = ncuenta;
            fechaVencimiento = pdPago.getFechaString (fechaven);
            claveProveedor = proveedor;
        }

        public char getEstatus () {return Estatus;}
        public float getImporte () {return Importe;}
        public String getNumCuenta () {return numCuenta;}
        public GregorianCalendar getFechaVencimiento () {return fechaVencimiento;}
        public String getClaveProveedor () {return claveProveedor;}
    }

}
