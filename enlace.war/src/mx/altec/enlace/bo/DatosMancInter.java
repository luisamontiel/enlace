/*****************************************************************************
 Banco Santander Mexicano
 Clase: DatosMancInter (clase para el manejo de datos para mancomunidad
                         internacional)
 Fecha: 28 de enero de 2004
*****************************************************************************/
package mx.altec.enlace.bo;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class DatosMancInter implements java.io.Serializable
{
    public static final String COLOR_CLARO = "textabdatcla";
    public static final String COLOR_OBSCURO = "textabdatobs";

	private String fch_registro = "";
    private String folio_registro = "";			// DIPD
    private String tipo_operacion = "";			// DIPD		DITA
    private String fch_autorizacion = "";
    private String folio_autorizacion = "";
    private String usuario1 = "";
    private String usuario2 = "";
    private String cta_origen = "";				// DIPD		DITA
    private String cta_destino = "";			// DIPD		DITA
	private String beneficiario = "";			// DIPD		DITA
	private String divisaAbono = "";			// DIPD		DITA
	private String importeMN = "";				// DIPD		DITA
	private String importeUSD = "";				// DIPD		DITA
	private String importeDivisa = "";			//			DITA
	private String tipoCambioMN = "";			// DIPD		DITA
	private String tipoCambioDA = "";			//			DITA
	private String estatus = "";

	private String divisaCargo = "";			// DIPD		DITA
	private String concepto = "";				// DIPD		DITA
	private String cveEspecial = "";			// DIPD		DITA
	private String contraMoneda = "";			//			DITA
	private String dirOinver = "";				//			DITA
	private String claveABA = "";				//			DITA
	private String banco = "";					//			DITA
	private String ciudad = "";					//			DITA
	private String pais = "";					//			DITA
	private String sucursal = "";				//			DITA
	private String horaCotiza = "";				//			DITA
	private String titular = "";				//			DITA
	private String tipoCompraVenta = "";		// DIPD
	private String datos_comprobante = "";		// datos para la trama del comprobante
	private String titular_cargo = "";			//          DITA  agregado para la autorizacion vcl 17/05/04

    private Convertidor con = new Convertidor(); // Cambio 7to8 21/04/2004 jbg

    // Constructor vacio reglamentario
    public DatosMancInter () {}

    // Constructor que recibe todos los campos del bean
    public DatosMancInter ( String fch_registro,
						    String folio_registro,
							String tipo_operacion,
							String fch_autorizacion,
							String folio_autorizacion,
							String usuario1,
							String usuario2,
						    String cta_origen,
							String cta_destino,
							String beneficiario,
							String divisaAbono,
							String importeMN,
						    String importeUSD,
							String importeDivisa,
							String tipoCambioMN,
							String tipoCambioDA,
							String estatus,
							String divisaCargo,
							String concepto,
							String cveEspecial,
							String contraMoneda,
							String dirOinver,
							String claveABA,
							String banco,
							String ciudad,
							String pais,
							String sucursal,
							String horaCotiza,
							String titular,
							String tipoCompraVenta,
							String titular_cargo		//agregado para la autorizacion  vcl 17/05/04
						 )
	{
		this.fch_registro       = fch_registro;
		this.folio_registro     = folio_registro;
		this.tipo_operacion     = tipo_operacion;
		this.fch_autorizacion   = fch_autorizacion;
		this.folio_autorizacion = folio_autorizacion;

        // ===================================Cambio 7to8 21/04/2004 jbg
        if (usuario1.length()>0) this.usuario1 = con.cliente_7To8(usuario1);
        else this.usuario1 = usuario1;

        if (usuario2.length()>0) this.usuario2 = con.cliente_7To8(usuario2);
        else this.usuario2 = usuario2;
        //==============================================================

		this.cta_origen         = cta_origen;
		this.cta_destino        = cta_destino;
		this.beneficiario       = beneficiario;
		this.divisaAbono        = divisaAbono;
		this.importeMN          = importeMN;
		this.importeUSD         = importeUSD;
		this.importeDivisa      = importeDivisa;
		this.tipoCambioMN		= tipoCambioMN;
		this.tipoCambioDA		= tipoCambioDA;
		this.estatus            = estatus;

		this.divisaCargo        = divisaCargo;
		this.concepto			= concepto;
		this.cveEspecial		= cveEspecial;
		this.contraMoneda		= contraMoneda;
		this.dirOinver			= dirOinver;
		this.claveABA			= claveABA;
		this.banco				= banco;
		this.ciudad				= ciudad;
		this.pais				= pais;
		this.sucursal			= sucursal;
		this.horaCotiza			= horaCotiza;
		this.titular			= titular;
		this.tipoCompraVenta    = tipoCompraVenta;
		this.titular_cargo      = titular_cargo;		//agregado para la autorizacion  vcl 17/05/04
    }

    // Constructor que recibe los campos del bean en una trama
    public DatosMancInter (String trama)
	{
		pdTokenizer tokenizador = new pdTokenizer (trama, ";", 2);
		try
		{
			//  Datos que muestra en la consulta
			this.fch_registro       = tokenizador.nextToken ();
			this.folio_registro     = tokenizador.nextToken ();
			this.tipo_operacion     = tokenizador.nextToken ();
			this.fch_autorizacion   = tokenizador.nextToken ();
			this.folio_autorizacion = tokenizador.nextToken ();
			this.usuario1           = tokenizador.nextToken ();
			this.usuario2           = tokenizador.nextToken ();

            // ================================================Cambio 7to8 21/04/2004 jbg
            if (this.usuario1.length()>0) this.usuario1 = con.cliente_7To8(this.usuario1);
            if (this.usuario2.length()>0) this.usuario2 = con.cliente_7To8(this.usuario2);
            // ==========================================================================

			this.cta_origen         = tokenizador.nextToken ();
			this.cta_destino        = tokenizador.nextToken ();
			this.beneficiario       = tokenizador.nextToken ();
			this.divisaAbono        = tokenizador.nextToken ();
			this.importeMN          = tokenizador.nextToken ();
			this.importeUSD         = tokenizador.nextToken ();
			this.importeDivisa      = tokenizador.nextToken ();
			this.tipoCambioMN       = tokenizador.nextToken ();
			this.tipoCambioDA       = tokenizador.nextToken ();
			this.estatus            = tokenizador.nextToken ();

			// Datos restantes para la autorizacion
			this.divisaCargo        = tokenizador.nextToken ();
			this.concepto			= tokenizador.nextToken ();
			this.cveEspecial		= tokenizador.nextToken ();
			this.contraMoneda		= tokenizador.nextToken ();
			this.dirOinver			= tokenizador.nextToken ();
			this.claveABA			= tokenizador.nextToken ();
			this.banco				= tokenizador.nextToken ();
			this.ciudad				= tokenizador.nextToken ();
			this.pais				= tokenizador.nextToken ();
			this.sucursal			= tokenizador.nextToken ();
			this.horaCotiza			= tokenizador.nextToken ();
			this.titular			= tokenizador.nextToken ();
			this.tipoCompraVenta    = tokenizador.nextToken ();
			this.titular_cargo      = tokenizador.nextToken ();		//agregado para la autorizacion  vcl 17/05/04
		}
		catch (Exception ex) {}
    }

    // Metodo para formatear el bean en html @return String con formato en html para desplegar la consulta
    public String getHTMLFormat (int index)
	{
		String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
		String row = "";
		String Estatus = getEstatus (estatus);
		String TipoOper = "";
		String ImpMN = "";
		String ImpUSD = "";
		String ImpDivisa = "";
		String TipCambMN = "";
		String TipCambDA = "";
		String beneficiarioHTML = "";

		java.text.NumberFormat formatoMoneda = java.text.NumberFormat.getCurrencyInstance (Locale.US);

		System.out.println("fch_registro ["+ fch_registro+"]");
		System.out.println("folio_registro ["+ folio_registro+"]");
		System.out.println("Description (tipo_operacion) ["+ Description (tipo_operacion)+"]");
		System.out.println("fch_autorizacion ["+ fch_autorizacion+"]");
		System.out.println("folio_autorizacion ["+ folio_autorizacion+"]");
		System.out.println("usuario1 ["+ usuario1+"]");
		System.out.println("usuario2 ["+ usuario2+"]");
		System.out.println("cta_origen ["+ cta_origen+"]");
		System.out.println("cta_destino ["+ cta_destino+"]");
		System.out.println("beneficiario ["+ beneficiario+"]");
		System.out.println("divisaAbono ["+ divisaAbono+"]");
		System.out.println("importeMN ["+ formatoMoneda.format (Double.parseDouble (importeMN)) +"]");
		System.out.println("importeUSD ["+ formatoMoneda.format (Double.parseDouble (importeUSD)) +"]");
		System.out.println("importeDivisa ["+ formatoMoneda.format (Double.parseDouble (importeDivisa)) +"]");
		System.out.println("tipoCambioMN ["+ formatoMoneda.format (Double.parseDouble (tipoCambioMN)) +"]");
		System.out.println("tipoCambioDA ["+ formatoMoneda.format (Double.parseDouble (tipoCambioDA)) +"]");
		System.out.println("Estatus ["+ Estatus+"]");

		if(fch_registro.equals("") || fch_registro==null)
			fch_registro="&nbsp;";

		if(folio_registro.equals("") || folio_registro==null)
			folio_registro="&nbsp;";

		if(tipo_operacion.equals("") || tipo_operacion==null)
			TipoOper="&nbsp;";
		else
			TipoOper = Description(tipo_operacion);

		if(fch_autorizacion.equals("") || fch_autorizacion==null)
			fch_autorizacion="&nbsp;";

		if(folio_autorizacion.equals("") || folio_autorizacion==null)
			folio_autorizacion="&nbsp;";

        // ===============================Cambio 7to8 21/04/2004 jbg
        if(usuario1.equals("") || usuario1==null) usuario1="&nbsp;";
        else usuario1=con.cliente_7To8(usuario1);

        if(usuario2.equals("") || usuario2==null) usuario2="&nbsp;";
        else usuario2=con.cliente_7To8(usuario2);
        //==========================================================

		if(cta_origen.equals("") || cta_origen==null)
			cta_origen = "&nbsp;";

		if(cta_destino.equals("") || cta_destino==null)
			cta_destino = "&nbsp;";

		if(beneficiario.equals("") || beneficiario==null)
			beneficiarioHTML = "&nbsp;";
		else
			beneficiarioHTML = beneficiario;

		if(divisaAbono.equals("") || divisaAbono==null)
			divisaAbono = "&nbsp;";
		String tmpMN = formatoMoneda.format(Double.parseDouble(importeMN));
		if(tmpMN.equals("") || tmpMN==null)
			ImpMN = "&nbsp;";
		else
			if (tmpMN.substring (0,3).equals ("MXN"))
				ImpMN = "$ " + tmpMN.substring(3, tmpMN.length());
			else
				ImpMN = tmpMN;
		String tmpUSD = formatoMoneda.format(Double.parseDouble(importeUSD));
		if(tmpUSD.equals("") || tmpUSD==null)
			ImpUSD = "&nbsp;";
		else
			if (tmpUSD.substring (0,3).equals ("MXN"))
				ImpUSD = "$ " + tmpUSD.substring(3, tmpUSD.length());
			else
				ImpUSD = tmpUSD;
		String tmpDiv = formatoMoneda.format(Double.parseDouble(importeDivisa));
		if(tmpDiv.equals("") || tmpDiv==null)
			ImpDivisa = "&nbsp;";
		else
			if (tmpDiv.substring (0,3).equals ("MXN"))
				ImpDivisa = "$ " + tmpDiv.substring(3, tmpDiv.length());
			else
				ImpDivisa = tmpDiv;
		String tmpTCMN = formatoMoneda.format(Double.parseDouble(tipoCambioMN));
		if(tmpTCMN.equals("") || tmpTCMN==null)
			TipCambMN = "&nbsp;";
		else
			if (tmpTCMN.substring (0,3).equals ("MXN"))
				TipCambMN = "$ " + tmpTCMN.substring(3, tmpTCMN.length());
			else
				TipCambMN = tmpTCMN;
		String tmpTCDA = formatoMoneda.format(Double.parseDouble(tipoCambioDA));
		if(tmpTCDA.equals("") || tmpTCDA==null)
			TipCambDA = "&nbsp;";
		else
			if (tmpTCDA.substring (0,3).equals ("MXN"))
				TipCambDA = "$ " + tmpTCDA.substring(3, tmpTCDA.length());
			else
				TipCambDA = tmpTCDA;
		if(Estatus.equals("") || Estatus==null)
			Estatus="&nbsp;";

		row += "<tr class="+color+" align='center'>\n";
		// ----------------------------------------------------------------------------------
		if (estatus.equals ("P"))
			row += "<td><input type='checkbox' name='Folio' value='" + index + "'></td>\n";
		else
			row += "<td>&nbsp;</td>\n";
		// ----------------------------------------------------------------------------------
		row += "  <td align='center' class='" +color+ "'>" + fch_registro + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + folio_registro + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + TipoOper + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + fch_autorizacion + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + folio_autorizacion + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + usuario1 + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + usuario2 + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + cta_origen + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + cta_destino + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + beneficiarioHTML + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + divisaAbono + "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + ImpMN + "</td>\n";				//importeMN
		row += "  <td align='center' class='" +color+ "'>" + ImpUSD + "</td>\n";			//importeUSD
		row += "  <td align='center' class='" +color+ "'>" + ImpDivisa + "</td>\n";			//importeDivisa
		if((divisaCargo.trim().equals("DA") || divisaCargo.trim().equals("USD")))
			row += "  <td align='center' class='" +color+ "'>" + tipoCambioDA+ "</td>\n";
		else
			row += "  <td align='center' class='" +color+ "'>" + tipoCambioMN+ "</td>\n";
		row += "  <td align='center' class='" +color+ "'>" + Estatus + "</td>\n";
		row += "</tr>\n";
		return row;
    }

// ----------------------------------------------------------------------------------
    // Metodo privado para obtener el estatus completo
    private String getEstatus (String estatus)
	{
		if (estatus.trim ().equals ("P")) {
			return "Pendiente";
		} else if (estatus.trim ().equals ("A")) {
			return "Aceptada";
		} else if (estatus.trim ().equals ("R")) {
			return "Rechazada";
		} else if (estatus.trim ().equals ("C")) {
			return "Cancelada";
		} else if (estatus.trim ().equals ("E")) {
			return "Ejecutada";
		} else if (estatus.trim ().equals ("N")) {
			return "No Ejecutada";
		} else if (estatus.trim ().equals ("S")) {
			return "Cancelada por sistema";
		} else {
			return "";
		}
    }
// ----------------------------------------------------------------------------------
    /** Metodo para verificar si la transacci&oacute;n esta pendiente
     * @return true Si esta pendiente de lo contrario false
     */
    public boolean pendiente () {
	return estatus.equals ("P");
    }
// ----------------------------------------------------------------------------------
    /** Metodo para generar la trama de autorizaci&oacute;n internacional
     * @param usuario String con el usuario que va a autorizar la transacci&oacute;n
     * @param contrato String con el contrato del usuario
     * @param clavePerfil String con la clave del perfil del usuario
     * @return String con la trama de autorizaci&oacute;n
     */

// ----------------------------------------------------------------------------------
    public String getTramaAutorizacion (String usuario, String contrato, String clavePerfil)
	{
		String trama_entrada = "";
		String refmancomunidad = "|" + folio_registro+ "|";
		java.text.NumberFormat formatoMoneda = java.text.NumberFormat.getCurrencyInstance (Locale.US);

		cta_destino = cta_destino.trim();
		cta_origen = cta_origen.trim();
		claveABA = claveABA.trim();
		banco =  banco.trim();
		ciudad = ciudad.trim();
		pais = pais.trim();
		sucursal = sucursal.trim();
		beneficiario = beneficiario.trim();
		concepto = concepto.trim();
		titular = titular.trim();
		cveEspecial = cveEspecial.trim();
		titular_cargo = titular_cargo.trim();

		EIGlobal.mensajePorTrace("********************************", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("*  usuario regis : /"+usuario1+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  usuario autor : /"+usuario2+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  tipoCompraVta : /"+tipoCompraVenta+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  divisaCargo   : /"+divisaCargo+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  divisaAbono   : /"+divisaAbono+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  cta_origen    : /"+cta_origen+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  cta_destino   : /"+cta_destino+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  importeMN     : /"+importeMN+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  importeUSD    : /"+importeUSD+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  tipoCambioMN  : /"+tipoCambioMN+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  beneficiario  : /"+beneficiario+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  concepto      : /"+concepto+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  contraMoneda  : /"+contraMoneda+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  importeDivisa : /"+importeDivisa+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  tipoCambioDA  : /"+tipoCambioDA+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  dirOinver     : /"+dirOinver+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  claveABA      : /"+claveABA+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  claveEspecial : /"+cveEspecial+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  banco         : /"+banco+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  ciudad        : /"+ciudad+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  pais          : /"+pais+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  sucursal      : /"+sucursal+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  titular       : /"+titular+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  horaCotiza    : /"+horaCotiza+"/", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  titular_cargo : /"+titular_cargo+"/", EIGlobal.NivelLog.DEBUG);	//agregado para la autorizacion  vcl 17/05/04
		EIGlobal.mensajePorTrace("*********************************", EIGlobal.NivelLog.INFO);

		String tmpMN = formatoMoneda.format (Double.parseDouble (importeMN));
		if (tmpMN.substring(0, 3).equals ("MXN"))
			tmpMN = "$ " + tmpMN.substring(3, tmpMN.length());
		String tmpUSD = formatoMoneda.format (Double.parseDouble (importeUSD));
		if (tmpUSD.substring(0, 3).equals ("MXN"))
			tmpUSD = "$ " + tmpUSD.substring(3, tmpUSD.length());
		String tmpDiv = formatoMoneda.format (Double.parseDouble (importeDivisa));
		if (tmpDiv.substring(0, 3).equals ("MXN"))
			tmpDiv = "$ " + tmpDiv.substring(3, tmpDiv.length());
		if ( tipo_operacion.equals ("DIPD") )	// cambios de divisa
		{
			datos_comprobante =  "DIPD|" + cta_origen + " |" + cta_destino + " |" + tmpMN + " |";
			datos_comprobante += tmpUSD + " |";
			datos_comprobante += tipoCambioMN+ " |" + concepto + " |" + usuario1+ " |" + contrato;
			trama_entrada = "1EWEB|"+usuario+"|DIPD|"+contrato+"|";
			trama_entrada += usuario+"|"+clavePerfil+"|"+"CHDO@"+tipoCompraVenta+"@";
			trama_entrada += divisaCargo+"@"+divisaAbono+"@"+cta_origen+"@"+cta_destino+"@MN@";
			trama_entrada += importeMN+"@"+importeUSD+"@"+importeUSD+"@"+tipoCambioMN+"@1@d@@@@@@";
			trama_entrada += beneficiario+"@"+concepto+"@10:00@"+cveEspecial+"@ @" + refmancomunidad;
		}

		if ( tipo_operacion.equals ("DITA") )	// transferencias internacionales
		{
			datos_comprobante = "DITA|" + contrato + " |" + usuario1 + " |" + folio_registro + " |" +concepto + " |";
			datos_comprobante += titular_cargo + " |" + beneficiario + " |" + banco + " |" + pais + " |" + ciudad + " |" + divisaAbono + " |";	//sustituye titular_cargo para la autorizacion  vcl 17/05/04
			datos_comprobante += tipoCambioMN + " |" + tmpUSD + " |" + tmpDiv + " |";
			if((divisaCargo.trim().equals("DA") || divisaCargo.trim().equals("USD")) &&
				divisaAbono.trim().equals("USD"))
				datos_comprobante += "0 |" + cta_origen + " |" + cta_destino;
			else
				datos_comprobante += tmpMN + " |" + cta_origen + " |" + cta_destino;

			trama_entrada = "1EWEB|" + usuario + "|DITA|" + contrato + "|";
			trama_entrada += usuario + "|" + clavePerfil + "|TRAN@VTA" + "@" + divisaCargo + "@";
			trama_entrada += divisaAbono + "@" + cta_origen + "@" + cta_destino + "@" + contraMoneda  + "@";
			trama_entrada += importeMN + "@" + importeUSD + "@" + importeDivisa + "@" + tipoCambioMN  + "@";
			trama_entrada += tipoCambioDA  + "@" + dirOinver + "@" + claveABA + "@" + banco + "@" + ciudad + "@";
			trama_entrada += pais + "@" + sucursal + "@" + beneficiario  + "@" + concepto + "@" + horaCotiza + "@";
			trama_entrada += cveEspecial + "@" + titular_cargo + "@" + refmancomunidad;   //sustituye titular_cargo para la autorizacion  vcl 17/05/04
		}
		System.out.println("***************************************************************");
		System.out.println("TRAMA HACIA EL MANCSRVR -->/"+trama_entrada+"/");
		System.out.println("***************************************************************");
		System.out.println("datos_comprobante: /"+datos_comprobante+"/");
		System.out.println("***************************************************************");
		return trama_entrada;
    }

//----------------------------------------------------------------------------------------------------------------------------------
    /** Metodo para generar la trama de cancelaci&oacute;n
     * @param usuario String con el usuario que va a autorizar la transacci&oacute;n
     * @param contrato String con el contrato del usuario
     * @param clavePerfil String con la clave del perfil del usuario
     * @return String con la trama de autorizaci&oacute;n
     */
    public String getTramaCancelacion (String usuario, String contrato, String clavePerfil)
	{
		String fch_registro_trama="";
		String trama;

		fch_registro_trama = fch_registro.substring(0,2) + fch_registro.substring(3,5) + fch_registro.substring(6,10);
		trama = "1EWEB|" + usuario + "|CAMA|" + contrato + '|' + usuario + '|' + clavePerfil + '|';
		trama += fch_registro_trama + '@' + folio_registro +'@';

		return trama;
    }

//-----------------------------------------------------------------------------------------------------------
    /** Metodo para formatear el bean en html
     * @param index Indice del registro
     * @return String con formato en html para desplegar el resultado cuando la operacion esta esperando a ser procesada
     */
    public String getFormatoHtmlRes (int index)
	{
		java.text.NumberFormat formatoMoneda = java.text.NumberFormat.getCurrencyInstance (Locale.US);
		String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/mm/yyyy");
		Date hoy = new Date ();
		String row = "<tr>";
		row += "<td align='center' class='" + color + "'>" + cta_origen + "</td>";
		row += "<td align='center' class='" + color + "'>" + cta_destino + "</td>";
		System.out.println ("-------------------------divi. cargo: "+divisaCargo);
		System.out.println ("-------------------------divi. abono: "+divisaAbono);
		// VSWF
		String tmpUSD = formatoMoneda.format (Double.parseDouble (importeUSD));
		if (tmpUSD.substring(0, 3).equals ("MXN"))
			tmpUSD = "$ " + tmpUSD.substring(3, tmpUSD.length());
		String tmpDiv = formatoMoneda.format (Double.parseDouble (importeDivisa));
		if (tmpDiv.substring(0, 3).equals ("MXN"))
			tmpDiv = "$ " + tmpDiv.substring(3, tmpDiv.length());

		if (  divisaAbono.equals(divisaCargo) ||
			( divisaAbono.equals("DA") && divisaCargo.equals("USD") ) ||
			( divisaAbono.equals("USD") && divisaCargo.equals("DA") )
		   )  //es dolar a dolar
			row += "<td align='right' class='" + color + "'>" + tmpUSD + "</td>";
		else	// es cualquier otra combinacion
			row += "<td align='right' class='" + color + "'>" + tmpDiv + "</td>";
		row += "<td align='center' class='" + color + "'>" + folio_registro + "</td>";
		row += "<td align='center' class='" + color + "'>&nbsp;</td>";
		row += "<td align='center' class='" + color + "'>&nbsp;</td>";
		row += "<td align='center' class='" + color + "'> Por Procesar estatus </td>";
		row += "</tr>\n";
		return row;
    }

    /** Metodo para obtener la cuenta origen
     * @return String con la cuenta origen
     */
    public String getCta_origen () {return cta_origen;}

    /** Metodo para obtener la cuenta destino
     * @return String con la cuenta destino
     */
    public String getCta_destino () {return cta_destino;}

    /** Metodo para obtener el folio de registro
     * @return String con el folio de registro
     */
    public String getFolio_registro () {return folio_registro;}

    /** Metodo para obtener el importeMN
     * @return String con el importeMN
     */
    public String getImporteMN () {return importeMN;}
	/** Metodo para obtener el importeUSD
     * @return String con el importeUSD
     */
    public String getImporteUSD () {return importeUSD;}
	/** Metodo para obtener el importeDivisa
     * @return String con el importeDivisa
     */
    public String getImporteDivisa () {return importeDivisa;}

    /** Metodo para obtener el tipo de operaci&oacute;n
     * @return String con el tipo de operaci&oacute;n
     */
    public String getTipo_operacion () {return tipo_operacion;}

	 /** Metodo para obtener la trama del comprobante
     * @return String con la trama de comprobante
     */
    public String getDatosComprobante () {return datos_comprobante;}

	public String getDivisaCargo () {return divisaCargo;}
	public String getDivisaAbono () {return divisaAbono;}

	// Sustitución MA a 390 - INI
	public String getBeneficiario() { return beneficiario; }
	public String getConcepto() { return concepto; }
	public String getCveEspecial() {return cveEspecial; }
	public String getTipoCambioMN() {return tipoCambioMN;}
	public String getTipoCambioDA() {return tipoCambioDA;}
	public String getTitular_cargo() {return titular_cargo;}
	public String getPais() {return pais;}
	public String getBanco() {return banco;}
	public String getTipoCompraVenta() {return tipoCompraVenta;}
	public String getCiudad() {return ciudad;}
	public String getClaveABA() {return claveABA;}
	public String getSucursal() {return sucursal;}
	public String getUsuario1() {return usuario1;}
	public String getUsuario2() {return usuario2;}
	public String getFchRegistro() {return fch_registro;}
	// Sustitución MA a 390 - FIN



// ----------------------------------------------------------------------------------
    public static String Description (String key)
	{
		Hashtable GetMovDescription = new Hashtable ();

		if (key == null)
			return "";
		try
		{
			GetMovDescription.put ("DITA", "Transferencia Internacional");
			GetMovDescription.put ("DIPD", "Cambios(Transferencia MN-Dlls / Dlls-MN)");
		}
		catch( Exception e )
		{
			e.printStackTrace ();
			return null;
		}
		String regreso = "";
		regreso = (String) GetMovDescription.get (key);

		if(regreso == null)
			regreso = "";
		return regreso;
     }

//-----------------------------------------------------------------------------------------------------
    /** Funci&oacute;n para formatear el bean en String
     * @return String con el bean formateado
     */
    public String toString ()
	{
		return fch_registro.trim () + ';' + folio_registro.trim () + ';' +
	    tipo_operacion.trim () + ';' + fch_autorizacion.trim () + ';' +
	    folio_autorizacion.trim () + ';' + usuario1.trim () + ';' + usuario2.trim ()
	    + ';' + cta_origen.trim () + ';' + cta_destino.trim () + ';' + beneficiario.trim() + ';'+
	    divisaAbono.trim() +';'+ importeMN.trim () + ';' + importeUSD + ';' + importeDivisa + ';' +
		tipoCambioMN + ';' +estatus.trim ()+  ";\r\n";
    }
}