package mx.altec.enlace.bo;

import java.sql.ResultSet;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;

public class CatNominaLeeRS {


	/***************************************************************
	 *** M�todo que lee el resultSet de busqueda de empleados	****
	 *** y almacena los valores en un arreglo bidimensional		****
	 **************************************************************/
	public String[][] separaRSBusquedaEmpleados(ResultSet rs, int numRows)
	{
		System.out.println("");
		System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ Inicio");
		int row = 0;

	//	CatNominaConstantes cons = new CatNominaConstantes();
		String[][] arrBiDatosEmpl = new String[numRows][CatNominaConstantes._TOTAL_COL + 3];  // El +3 espara obtener el tipo de Cuenta y es estatus y banco
	//	ConexionOracle conn = new ConexionOracle();

		try
		{
			while(rs.next()){
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ Renglon: " + row);
				int colum = 0;
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa0 = " + colum);
				if(rs.getString("CUENTA_ABONO") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("CUENTA_ABONO");
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa = " + colum);
				if(rs.getString("NUM_EMPL") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("NUM_EMPL");
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa = " + colum);
				if(rs.getString("DEPTO_EMPL") != null)
					arrBiDatosEmpl[row][colum++]= rs.getString("DEPTO_EMPL");
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa3 = " + colum);
				if(rs.getString("APELL_PATERNO") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("APELL_PATERNO");
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa = " + colum);
				if(rs.getString("APELL_MATERNO") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("APELL_MATERNO");
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa = " + colum);
				if(rs.getString("NOMBRE") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("NOMBRE");
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa6 = " + colum);
				if(rs.getString("REG_FED_CAUS") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("REG_FED_CAUS");
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa = " + colum);
				if(rs.getString("SUELDO") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("SUELDO");	// ultimo cambio ++
				else
					arrBiDatosEmpl[row][colum++] = "";
				//System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ columanaaa = " + colum);
				if(rs.getString("TIPO_CUENTA") != null)
				{
					arrBiDatosEmpl[row][colum++] = rs.getString("TIPO_CUENTA"); // ultimo cambio
				}
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("ESTATUS_CUENTA") != null)
					arrBiDatosEmpl[row][colum++] = rs.getString("ESTATUS_CUENTA"); // ultimo cambio
				else
					arrBiDatosEmpl[row][colum++] = "";

				row++;  //Movemos el rengl�n
			}
		/*	for(int i= 0; i<arrBiDatosEmpl.length; i++)
			{
				for(int j=0; j<arrBiDatosEmpl[0].length;j++)
				{
					System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ ARR[7] = " + i + " " + j + " = " + arrBiDatosEmpl[i][j]);
				}
			}*/

		}
		catch (Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		System.out.println("CatNominaLeeRS - separaRSBusquedaEmpleados $ Fin");
		return arrBiDatosEmpl;
	}


	/***************************************************************
	 *** M�todo que lee el resultSet con los datos a modificar 	****
	 *** de un empleado y los guarda en en un arreglo			****
	 **************************************************************/
	//public String[] guardaRSDatosModif(ResultSet rs)
	public String[] guardaRSDatosModif(ResultSet rs, String numCuenta, String tipoCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaLeeRS - sguardaRSDatosModif $ Inicio");
		String arrDatosModif[] = null;
		int posicion = 0;

		try {
			/* jp@everis */
			//if( numCuenta.trim().length() == 11 ){
				if(tipoCuenta.equals("PROPIA")){
				System.out.println("CatNominaLeeRS - sguardaRSDatosModif Arreglo[5]");
				arrDatosModif = new String[5];
				while(rs.next())
				{
					arrDatosModif[posicion++] = rs.getString("CUENTA_ABONO");
					arrDatosModif[posicion++] = rs.getString("NUM_EMPL");
					arrDatosModif[posicion++] = rs.getString("DEPTO_EMPL");
					//arrDatosModif[posicion++] = String.valueOf(rs.getInt("SUELDO"));
					//arrDatosModif[posicion++] = String.valueOf(rs.getFloat("SUELDO"));
					arrDatosModif[posicion++] = rs.getString("SUELDO");
					System.out.println("CatNominaLeeRS - sguardaRSDatosModif suELDO[5]" + arrDatosModif[3] );
					/* jp@everis */
					arrDatosModif[posicion] = rs.getString("TIPO_CUENTA");
				}
			}
			else{
				System.out.println("CatNominaLeeRS - sguardaRSDatosModif Arreglo[30]");
				arrDatosModif = new String[30];
				while(rs.next())
				{
					/* Tabla EWEB_DET_CAT_NOM 4*/
					arrDatosModif[posicion++] = rs.getString("CUENTA_ABONO");
					arrDatosModif[posicion++] = rs.getString("NUM_EMPL");
					arrDatosModif[posicion++] = rs.getString("DEPTO_EMPL");
					//arrDatosModif[posicion++] = String.valueOf(rs.getInt("SUELDO"));
					//arrDatosModif[posicion++] = String.valueOf(rs.getFloat("SUELDO"));
					arrDatosModif[posicion++] = rs.getString("SUELDO");

					/* Tabla EWEB_CAT_NOM_EMPL 14*/
					arrDatosModif[posicion++] = rs.getString("NOMBRE");
					arrDatosModif[posicion++] = rs.getString("APELL_PATERNO");
					arrDatosModif[posicion++] = rs.getString("APELL_MATERNO");
					arrDatosModif[posicion++] = rs.getString("REG_FED_CAUS");
					arrDatosModif[posicion++] = rs.getString("SEXO");
					arrDatosModif[posicion++] = rs.getString("CALLE_NUMERO");
					arrDatosModif[posicion++] = rs.getString("COLONIA");
					arrDatosModif[posicion++] = rs.getString("DELEG_MUNICIPIO");
					arrDatosModif[posicion++] = rs.getString("CVE_ESTADO");
					arrDatosModif[posicion++] = rs.getString("CIUDAD_POBLACION");
					arrDatosModif[posicion++] = rs.getString("CODIGO_POSTAL");
					arrDatosModif[posicion++] = rs.getString("CVE_PAIS");
					arrDatosModif[posicion++] = rs.getString("PREFIJO_PARTICULAR");
					arrDatosModif[posicion++] = rs.getString("NUMERO_PARTICULAR");

					/* Tabla EWEB_CONTR_CAT_NOM 13*/
					arrDatosModif[posicion++] = rs.getString("CALLE_NUMERO_OF");
					arrDatosModif[posicion++] = rs.getString("COLONIA_OF");
					arrDatosModif[posicion++] = rs.getString("DELEG_MUN_OF");
					arrDatosModif[posicion++] = rs.getString("CVE_ESTADO_OF");
					arrDatosModif[posicion++] = rs.getString("CIUDAD_POB_OF");
					arrDatosModif[posicion++] = rs.getString("CODIGO_POSTAL_OF");
					arrDatosModif[posicion++] = rs.getString("CVE_PAIS_OF");
					arrDatosModif[posicion++] = String.valueOf(rs.getInt("CVE_DIRECCION"));
					arrDatosModif[posicion++] = String.valueOf(rs.getInt("PREFIJO"));
					arrDatosModif[posicion++] = String.valueOf(rs.getInt("NUMERO"));
					arrDatosModif[posicion++] = String.valueOf(rs.getInt("EXTENSION"));
					arrDatosModif[posicion] = rs.getString("TIPO_CUENTA");
				}
				System.out.println("CatNominaLeeRS - sguardaRSDatosModif Arreglo[inicia for]"+arrDatosModif.length);
				for(int k=0; k<arrDatosModif.length; k++){
					System.out.println(k + "->" + arrDatosModif[k] + "<");
					}
			}

		}
		catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("CatNominaLeeRS - sguardaRSDatosModif $ Fin");
		return arrDatosModif;
	}


	/***************************************************************
	 *** M�todo que lee el resultSet con los datos a modificar 	****
	 *** de un empleado con cuenta Interbancaria 				****
	 *** y los guarda en en un arreglo							****
	 **************************************************************/
	public String[] guardaRSDatosModifInterb(ResultSet rs)
	{
		System.out.println("");
		System.out.println("CatNominaLeeRS - guardaRSDatosModifInterb $ Inicio");
		String arrDatosModifInterb[] = new String[6];
		int posicion = 0;


		try {
			while(rs.next())
			{
				arrDatosModifInterb[0] = rs.getString("CUENTA_ABONO");
				arrDatosModifInterb[1] = rs.getString("NUM_EMPL");
				//arrDatosModifInterb[2] = String.valueOf(rs.getInt("SUELDO"));
				arrDatosModifInterb[2] = rs.getString("SUELDO");
				arrDatosModifInterb[3] = rs.getString("APELL_PATERNO");
				arrDatosModifInterb[4] = rs.getString("NOMBRE");
				arrDatosModifInterb[5] = rs.getString("REG_FED_CAUS");
			}
				for(int k=0; k<arrDatosModifInterb.length; k++){
					System.out.println(k + "->" + arrDatosModifInterb[k].toString() + "<");
					}

		}
		catch (Exception e){
			e.printStackTrace();
		}

		System.out.println("CatNominaLeeRS -  $ Fin");
		return arrDatosModifInterb;
	}


}