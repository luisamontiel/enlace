/*
 * BaseResource.java
 * Created on 11 de octubre de 2002, 01:49 PM
 */

package mx.altec.enlace.bo;
import java.util.*;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;
import mx.altec.enlace.beans.MensajeUSR;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;

/*
 * @author  Horacio Oswaldo Ferro Daz
 */
public class BaseResource implements java.io.Serializable/*, Runnable*/
{
	/*******************************************************/
	/* ESTADOS DE CUENTA                                   */
	/*******************************************************/
	/**
	 * Constante para definir la facultad de Configuracion de Estado de Cuenta
	 */
	public static final String FAC_EDO_CTA_IND = "SOLEDOCTAIND";
	/**
	 * Constante para definir la facultad de Configuracion de Estado de Cuenta Masivo
	 */
	public static final String FAC_EDO_CTA_MAS = "SOLEDOCTAARCH";
	/**
	 * Constante para definir la facultad de Consulta de Estado de Cuenta
	 */
	public static final String FAC_CONS_EDO_CTA = "CONSFOLIOSOLIC";
	/**
	 * Constante para definir la facultad de Solicitud Estado de Cuenta Historico
	 */
	public static final String FAC_CON_PERHIST = "SOLEDOCTAHIS";
	/**
	 * Constante para definir la facultad de Descarga de Estado de Cuenta
	 */
	public static final String FAC_EDO_CTA_DESCARGA = "DESCARGAEDOCTA";
	
	/**
	 * Constante para definir la facultad de Descarga de Estado de Cuenta
	 */
	public static final String FAC_CONS_EDO_HIST = "CONSEDOHIST";
	/*******************************************************/

	/**
	 * Constante para definir la facultad de Consulta de cuenta movil
	 */
	public static final String FAC_CONS_CTA_MOVIL = "CONSCUENTAS";
        
	/** Atributo serialVersionUID.	 */
	private static final long serialVersionUID = -8448520181157468382L;
	
    /** Bandera que indica si validar RSA para direccionamiento**/
	private boolean validaRSA = false;
	//<VC proyecto=200710001 autor=SLA fecha=09/05/2007 descripcin=varaibles de session>
	/** Atributo privado para almacenar liga de REDIR. */
	private String ligaREDIR;
	//VC
      
	/**Atributo csrftoken*/
	private String csrfToken;
	
    /** Lista que contiene las facultades del usuario */
    private ArrayList listaFacultades;

    /** Holds value of property archivoAmb. */
    private String archivoAmb;

    /** Holds value of property archivoBit. */
    private String archivoBit;

    /** Holds value of property archivoCat. */
    private String archivoCat;

    /** Holds value of property archivoEmpNom. */
    private String archivoEmpNom;

    /** Holds value of property archivoMov. */
    private String archivoMov;

    /** Holds value of property archivoPagos. */
    private String archivoPagos;

    /** Holds value of property bEPosiciones. */
    private String bEPosiciones;

    /** Holds value of property claveBanco. */
    private String claveBanco;

    /** Holds value of property contractNumber. */
    private String contractNumber;

    /** Holds value of property contratos. */
    private String contratos;

    /** Holds value of property cuentaCargo. */
    private String cuentaCargo;

    /** Holds value of property cuentasArchivo. */
    private String cuentasArchivo;

    /** Holds value of property divisaCuentaInter. */
    private String divisaCuentaInter;

    /** Holds value of property downPathManc. */
    private String downPathManc;

    /** Holds value of property elementos. */
    private String elementos;

    /** Holds value of property fechaAplicacion. */
    private String fechaAplicacion;

    /** Holds value of property fechafin. */
    private String fechafin;

    /** Holds value of property fechaini. */
    private String fechaini;

    /** Holds value of property funcionesDeMenu. */
    private String funcionesDeMenu;

    /** Holds value of property id. */
    private String id;

    /** Holds value of property importe. */
    private String importe;

    /** Holds value of property impTotalNom. */
    private String impTotalNom;

    /** Holds value of property lastSecNom. */
    private String lastSecNom;

    /** Holds value of property manCuenta. */
    private String manCuenta;

    /** Holds value of property manFchFin. */
    private String manFchFin;

    /** Holds value of property manFchIni. */
    private String manFchIni;

    /** Holds value of property manFolio. */
    private String manFolio;

    /** Holds value of property manImporte. */
    private String manImporte;

    /** Holds value of property manSQL. */
    private String manSQL;

    /** Holds value of property manUsuario. */
    private String manUsuario;

    /** Holds value of property moduloConsultar. */
    private String moduloConsultar;

    /** Holds value of property nameFileNomina. */
    private String nameFileNomina;

    /** Holds value of property nombreContrato. */
    private String nombreContrato = "";

    /** Holds value of property nombreUsuario. */
    private String nombreUsuario = "";

    /** Holds value of property nominaNumberOfRecords. */
    private String nominaNumberOfRecords;

    /** Holds value of property numeroEmpleado. */
    private String numeroEmpleado;

    /** Holds value of property password. */
    private String password;

    /** Holds value of property pIcalle. */
    private String pIcalle;

    /** Holds value of property pIciudad. */
    private String pIciudad;

    /** Holds value of property pIcolonia. */
    private String pIcolonia;

    /** Holds value of property pIcuenta. */
    private String pIcuenta;

    /** Holds value of property pIdel. */
    private String pIdel;

    /** Holds value of property pIestado. */
    private String pIestado;

    /** Holds value of property pIfolio. */
    private String pIfolio;

    /** Holds value of property pIhomoclave. */
    private String pIhomoclave;

    /** Holds value of property pIrfc. */
    private String pIrfc;

    /** Holds value of property pIrz. */
    private String pIrz;

    /** Holds value of property pItel. */
    private String pItel;

    /** Holds value of property rutaDescarga. */
    private String rutaDescarga;

    /** Holds value of property strMenu. */
    private String strMenu;

    /** Holds value of property total. */
    private String total;

    /** Holds value of property totalBit. */
    private String totalBit;

    /** Holds value of property totalRegsNom. */
    private String totalRegsNom;

    /** Holds value of property userID. */
    private String userID;

    /** Holds value of property userID8. */
    private String userID8;

    /** Holds value of property userProfile. */
    private String userProfile;

	/** Holds value of property bandManc. */
    private String bandManc;

	/** Holds value of property archMovs */
    private String archMovs;
    /** Atributo privado para almacenar la IP del cliente. */
    private String ipLogin;
    /** Atributo privado para almacenar el buc asociado al contrato. */
    private String bucContrato;
    /** Atributo privado empleado para definir si se realiza cierre de sesion. */
    private boolean cierraSesion;
    /** Atributo privado empleado para definir si hubo excepcion en cierre de sesion. */
    private boolean excepcionCierraSess;
    /** Holds value of property carrierCompleto. */
    private HashMap carrierCompleto;
    /** Atributo privado para almacenar estatus del correo del cliente. */
    private String statusCorreo;
    /** Atributo privado para almacenar estatus del celular del cliente. */
    private String statusCelular;
    /** Holds value of property campaniaActiva. */
    private boolean campaniaActiva = false;
    /** Holds value of property campaniaCtoActiva. */
    private boolean campaniaCtoActiva = false;

    /** Valor del host authentication para el manejo de Tokens */
    private String retToken = "";

    /** Atributo privado empleado para definir si se valido el token. */
    private boolean validarToken = false;

    /** Atributo privado empleado para definir si el token fue validado. */
    private boolean tokenValidado = false;

    /** Atributo privado empleado para definir si se administra token. */
    private boolean admonToken = false;

    /** Atributo privado empleado para definir si se valido la sesion. */
    private boolean validarSesion = false;

    /** Holds value of property tok. */
    private Token tok = null;

	/** EVERIS Variable para guardar en sesion el
	 ** nmero de folio de las transferencias
	 ** archivos.
	 */
	private String folioArchivo;

	/*<VC proyecto="200710001" autor="JGR" fecha="28/05/2007" descripcion="LIGA A SUPERNET">*/
	/** Atributo privado para almacenar la url de SuperNet. */
	private String urlSuperNet;
	/*<VC JGR >*/

	 //JPH requerimiento liga enlace supernet comercios
	 /** Atributo privado para almacenar las cuentas de SuperNet Comercios. */
	private HashMap<String, String> ctasSnetComercios;
	 //fin JPH

	/** Valor para ultimo acceso -> validacion de duplicidad de sesiones */
	private long ultAcceso;
	/** Atributo privado para almacenar el ultimo acceso anterior del cliente. */
	private long ultAccesoAnt;
	/** Holds value of property ultAccesoStr. */
	private String ultAccesoStr;
	/** Atributo privado para almacenar el nombre real del cliente. */
	private String nombreUsuarioReal;
	/** Atributo privado para almacenar el tiempo del thread. */
	private long tiempoThread;
	/** Atributo privado para almacenar el tiempo de la sesion del cliente. */
	private long tiempoSesion;

	/** Holds value of property sid. */
	private String sid;

	/** Holds value of property hilo. */
	transient private Thread hilo;
	/** Holds value of property seguir. */
	private boolean seguir = true;
	/** Holds value of property mensajeUSR. */
	private MensajeUSR mensajeUSR = new MensajeUSR();
        
	/** Atributo privado para almacenar el error de la transaccion. */
	private String errorTransaccion = "";
	
	/** Atributo para identificar si es contrato EPYME*/
	private boolean epyme;
    /** 
     * Metodo getter para  la bandera de contrato Epyme    
     * @return epyme de tipo booleano
     */
    public boolean isEpyme() {
		return epyme;
	}
    /**
     * Metodo setter para la bandera de contrato epyme
     * @param epyme de tipo boolean
     */
	public void setEpyme(boolean epyme) {
		this.epyme = epyme;
	}

	/** Creates a new instance of BaseResource */
    public BaseResource () {
        ultAcceso = Calendar.getInstance().getTime().getTime();
        ultAccesoAnt = ultAcceso;
        listaFacultades = new ArrayList ();
    }

    /**
     * Metodo para asignar el tiempo del thread.
     * 
     * @param tiempo : valor con el tiempo del thread.
     */
   public void setTiempoThread(long tiempo) {
       tiempoThread = tiempo;
   }

   /**
    * Metodo para almacenar el valor del atributo sid.
    * 
    * @param sid : cadena con el valor del sid.
    */
   public void setSid(String sid) {
       this.sid = sid;
   }

   /**
    * Metodo para almacenar el tiempo de la sesion del cliente.
    * 
    * @param tiempo : valor con el tiempo de la sesion del cliente.
    */
   public void setTiempoSesion(long tiempo) {
       tiempoSesion = tiempo;
   }

   /**
    * Metodo para almacenar el ultimo acceso del cliente.
    * 
    * @param tiempo : valor con el tiempo del ultimo acceso del cliente.
    */
   public void setUltAcceso(long tiempo) {
	   //System.out.println("UltAcceso: " + ultAcceso);
	   //System.out.println("UltAccesoAnt: " + ultAccesoAnt);
	   ultAccesoAnt = ultAcceso;
       ultAcceso = tiempo;
       //System.out.println("UltAcceso: " + ultAcceso);
       //System.out.println("UltAccesoAnt: " + ultAccesoAnt);
   }

   /**
    * Metodo para obtener el tiempo de actividad del cliente.
    * 
    * @return long que indica el tiempo de activadad del cliente.
    */
   public long getTiempoActivo() {
	   return ultAcceso - ultAccesoAnt;
   }

   /**
    * Metodo para almacenar el error de transaccion.
    * 
    * @param valor : cadena con el error de transaccion.
    */
	public void setErrorTransaccion(String valor) {
	       this.errorTransaccion = valor;
	}

	/**
	 * Metodo para obtener el error de transaccion.
	 * 
	 * @return String con el error de transaccion.
	 */
	public String getErrorTransaccion() {
		   return this.errorTransaccion;
	}


     /**
      * Metodo: createiASConn
      *
      * @param dbName the db name
      * @return the connection
      * @throws SQLException the SQL exception
      */
     public Connection createiASConn ( String dbName )
     throws SQLException {
	 Connection conn = null;
	 try {
	     InitialContext ic = new InitialContext ();
	     DataSource ds = ( DataSource ) ic.lookup ( dbName );
	     conn = ds.getConnection ();
	 } catch ( NamingException ne ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(ne), EIGlobal.NivelLog.INFO);
	 }
	 return conn;
     }

	/**
	 * Metodo para validar si el cliente cuenta con la facultad definida.
	 * 
	 * @param fac : String con la facultad a validar.
	 * @return boolean indicando true si cuenta con la facultad.
	 */
	public boolean getFacultad (String fac) {
//		EIGlobal.mensajePorTrace("listaFacultades.contains (" +fac + " = " + listaFacultades.contains (fac), EIGlobal.NivelLog.DEBUG);
	return listaFacultades.contains (fac);
    }

    /**
     * Metodo para almacenar la facultad definida en la lista de facultades.
     * 
     * @param fac : String con la facultad a almacenar.
     */
    public void setFacultad (String fac) {
	listaFacultades.add (fac);
    }

    /** Getter for property archivoAmb.
     * @return Value of property archivoAmb.
     */
    public String getArchivoAmb () {
	return this.archivoAmb;
    }

    /** Setter for property archivoAmb.
     * @param archivoAmb New value of property archivoAmb.
     */
    public void setArchivoAmb (String archivoAmb) {
	this.archivoAmb = archivoAmb;
    }

    /** Getter for property archivoBit.
     * @return Value of property archivoBit.
     */
    public String getArchivoBit () {
	return this.archivoBit;
    }

    /** Setter for property archivoBit.
     * @param archivoBit New value of property archivoBit.
     */
    public void setArchivoBit (String archivoBit) {
	this.archivoBit = archivoBit;
    }

    /** Getter for property archivoCat.
     * @return Value of property archivoCat.
     */
    public String getArchivoCat () {
	return this.archivoCat;
    }

    /** Setter for property archivoCat.
     * @param archivoCat New value of property archivoCat.
     */
    public void setArchivoCat (String archivoCat) {
	this.archivoCat = archivoCat;
    }

    /** Getter for property archivoEmpNom.
     * @return Value of property archivoEmpNom.
     */
    public String getArchivoEmpNom () {
	return this.archivoEmpNom;
    }

    /** Setter for property archivoEmpNom.
     * @param archivoEmpNom New value of property archivoEmpNom.
     */
    public void setArchivoEmpNom (String archivoEmpNom) {
	this.archivoEmpNom = archivoEmpNom;
    }

    /** Getter for property archivoMov.
     * @return Value of property archivoMov.
     */
    public String getArchivoMov () {
	return this.archivoMov;
    }

    /** Setter for property archivoMov.
     * @param archivoMov New value of property archivoMov.
     */
    public void setArchivoMov (String archivoMov) {
	this.archivoMov = archivoMov;
    }

    /** Getter for property archivoPagos.
     * @return Value of property archivoPagos.
     */
    public String getArchivoPagos () {
	return this.archivoPagos;
    }

    /** Setter for property archivoPagos.
     * @param archivoPagos New value of property archivoPagos.
     */
    public void setArchivoPagos (String archivoPagos) {
	this.archivoPagos = archivoPagos;
    }

    /** Getter for property bEPosiciones.
     * @return Value of property bEPosiciones.
     */
    public String getBEPosiciones () {
	return this.bEPosiciones;
    }

    /** Setter for property bEPosiciones.
     * @param bEPosiciones New value of property bEPosiciones.
     */
    public void setBEPosiciones (String bEPosiciones) {
	this.bEPosiciones = bEPosiciones;
    }

    /** Getter for property claveBanco.
     * @return Value of property claveBanco.
     */
    public String getClaveBanco () {
	return this.claveBanco;
    }

    /** Setter for property claveBanco.
     * @param claveBanco New value of property claveBanco.
     */
    public void setClaveBanco (String claveBanco) {
	this.claveBanco = claveBanco;
    }

    /** Getter for property contractNumber.
     * @return Value of property contractNumber.
     */
    public String getContractNumber () {
	return this.contractNumber;
    }

    /** Setter for property contractNumber.
     * @param contractNumber New value of property contractNumber.
     */
    public void setContractNumber (String contractNumber) {
	this.contractNumber = contractNumber;
    }

    /** Getter for property contratos.
     * @return Value of property contratos.
     */
    public String getContratos () {
	return this.contratos;
    }

    /** Setter for property contratos.
     * @param contratos New value of property contratos.
     */
    public void setContratos (String contratos) {
	this.contratos = contratos;
    }

    /** Getter for property cuentaCargo.
     * @return Value of property cuentaCargo.
     */
    public String getCuentaCargo () {
	return this.cuentaCargo;
    }

    /** Setter for property cuentaCargo.
     * @param cuentaCargo New value of property cuentaCargo.
     */
    public void setCuentaCargo (String cuentaCargo) {
	this.cuentaCargo = cuentaCargo;
    }

    /** Getter for property cuentasArchivo.
     * @return Value of property cuentasArchivo.
     */
    public String getCuentasArchivo () {
	return this.cuentasArchivo;
    }

    /** Setter for property cuentasArchivo.
     * @param cuentasArchivo New value of property cuentasArchivo.
     */
    public void setCuentasArchivo (String cuentasArchivo) {
	this.cuentasArchivo = cuentasArchivo;
    }

    /** Getter for property divisaCuentaInter.
     * @return Value of property divisaCuentaInter.
     */
    public String getDivisaCuentaInter () {
	return this.divisaCuentaInter;
    }

    /** Setter for property divisaCuentaInter.
     * @param divisaCuentaInter New value of property divisaCuentaInter.
     */
    public void setDivisaCuentaInter (String divisaCuentaInter) {
	this.divisaCuentaInter = divisaCuentaInter;
    }

    /** Getter for property downPathManc.
     * @return Value of property downPathManc.
     */
    public String getDownPathManc () {
	return this.downPathManc;
    }

    /** Setter for property downPathManc.
     * @param downPathManc New value of property downPathManc.
     */
    public void setDownPathManc (String downPathManc) {
	this.downPathManc = downPathManc;
    }

    /** Getter for property elementos.
     * @return Value of property elementos.
     */
    public String getElementos () {
	return this.elementos;
    }

    /** Setter for property elementos.
     * @param elementos New value of property elementos.
     */
    public void setElementos (String elementos) {
	this.elementos = elementos;
    }

    /** Getter for property fechaAplicacion.
     * @return Value of property fechaAplicacion.
     */
    public String getFechaAplicacion () {
	return this.fechaAplicacion;
    }

    /** Setter for property fechaAplicacion.
     * @param fechaAplicacion New value of property fechaAplicacion.
     */
    public void setFechaAplicacion (String fechaAplicacion) {
	this.fechaAplicacion = fechaAplicacion;
    }

    /** Getter for property fechafin.
     * @return Value of property fechafin.
     */
    public String getfechafin () {
	return this.fechafin;
    }

    /** Setter for property fechafin.
     * @param fechafin New value of property fechafin.
     */
    public void setfechafin (String fechafin) {
	this.fechafin = fechafin;
    }

    /** Getter for property fechaini.
     * @return Value of property fechaini.
     */
    public String getfechaini () {
	return this.fechaini;
    }

    /** Setter for property fechaini.
     * @param fechaini New value of property fechaini.
     */
    public void setfechaini (String fechaini) {
	this.fechaini = fechaini;
    }

    /** Getter for property funcionesDeMenu.
     * @return Value of property funcionesDeMenu.
     */
    public String getFuncionesDeMenu () {
	return this.funcionesDeMenu;
    }

    /** Setter for property funcionesDeMenu.
     * @param funcionesDeMenu New value of property funcionesDeMenu.
     */
    public void setFuncionesDeMenu (String funcionesDeMenu) {
	this.funcionesDeMenu = funcionesDeMenu;
    }

    /** Getter for property id.
     * @return Value of property id.
     */
    public String getId () {
	return this.id;
    }

    /** Setter for property id.
     * @param id New value of property id.
     */
    public void setId (String id) {
	this.id = id;
    }

    /** Getter for property importe.
     * @return Value of property importe.
     */
    public String getImporte () {
	return this.importe;
    }

    /** Setter for property importe.
     * @param importe New value of property importe.
     */
    public void setImporte (String importe) {
	this.importe = importe;
    }

    /** Getter for property impTotalNom.
     * @return Value of property impTotalNom.
     */
    public String getImpTotalNom () {
	return this.impTotalNom;
    }

    /** Setter for property impTotalNom.
     * @param impTotalNom New value of property impTotalNom.
     */
    public void setImpTotalNom (String impTotalNom) {
	this.impTotalNom = impTotalNom;
    }

    /** Getter for property lastSecNom.
     * @return Value of property lastSecNom.
     */
    public String getLastSecNom () {
	return this.lastSecNom;
    }

    /** Setter for property lastSecNom.
     * @param lastSecNom New value of property lastSecNom.
     */
    public void setLastSecNom (String lastSecNom) {
	this.lastSecNom = lastSecNom;
    }

    /** Getter for property manCuenta.
     * @return Value of property manCuenta.
     */
    public String getManCuenta () {
	return this.manCuenta;
    }

    /** Setter for property manCuenta.
     * @param manCuenta New value of property manCuenta.
     */
    public void setManCuenta (String manCuenta) {
	this.manCuenta = manCuenta;
    }

    /** Getter for property manFchFin.
     * @return Value of property manFchFin.
     */
    public String getManFchFin () {
	return this.manFchFin;
    }

    /** Setter for property manFchFin.
     * @param manFchFin New value of property manFchFin.
     */
    public void setManFchFin (String manFchFin) {
	this.manFchFin = manFchFin;
    }

    /** Getter for property manFchIni.
     * @return Value of property manFchIni.
     */
    public String getManFchIni () {
	return this.manFchIni;
    }

    /** Setter for property manFchIni.
     * @param manFchIni New value of property manFchIni.
     */
    public void setManFchIni (String manFchIni) {
	this.manFchIni = manFchIni;
    }

    /** Getter for property manFolio.
     * @return Value of property manFolio.
     */
    public String getManFolio () {
	return this.manFolio;
    }

    /** Setter for property manFolio.
     * @param manFolio New value of property manFolio.
     */
    public void setManFolio (String manFolio) {
	this.manFolio = manFolio;
    }

    /** Getter for property manImporte.
     * @return Value of property manImporte.
     */
    public String getManImporte () {
	return this.manImporte;
    }

    /** Setter for property manImporte.
     * @param manImporte New value of property manImporte.
     */
    public void setManImporte (String manImporte) {
	this.manImporte = manImporte;
    }

    /** Getter for property manSQL.
     * @return Value of property manSQL.
     */
    public String getManSQL () {
	return this.manSQL;
    }

    /** Setter for property manSQL.
     * @param manSQL New value of property manSQL.
     */
    public void setManSQL (String manSQL) {
	this.manSQL = manSQL;
    }

    /** Getter for property manUsuario.
     * @return Value of property manUsuario.
     */
    public String getManUsuario () {
	return this.manUsuario;
    }

    /** Setter for property manUsuario.
     * @param manUsuario New value of property manUsuario.
     */
    public void setManUsuario (String manUsuario) {
		this.manUsuario = manUsuario;
    }

    /** Getter for property moduloConsultar.
     * @return Value of property moduloConsultar.
     */
    public String getModuloConsultar () {
	return this.moduloConsultar;
    }

    /** Setter for property moduloConsultar.
     * @param moduloConsultar New value of property moduloConsultar.
     */
    public void setModuloConsultar (String moduloConsultar) {
	this.moduloConsultar = moduloConsultar;
    }

    /** Getter for property nameFileNomina.
     * @return Value of property nameFileNomina.
     */
    public String getNameFileNomina () {
	return this.nameFileNomina;
    }

    /** Setter for property nameFileNomina.
     * @param nameFileNomina New value of property nameFileNomina.
     */
    public void setNameFileNomina (String nameFileNomina) {
	this.nameFileNomina = nameFileNomina;
    }

    /** Getter for property nombreContrato.
     * @return Value of property nombreContrato.
     */
    public String getNombreContrato () {
	return this.nombreContrato;
    }

    /** Setter for property nombreContrato.
     * @param nombreContrato New value of property nombreContrato.
     */
    public void setNombreContrato (String nombreContrato) {
	this.nombreContrato = nombreContrato;
    }

    /** Getter for property nombreUsuario.
     * @return Value of property nombreUsuario.
     */
    public String getNombreUsuario () {
	return this.nombreUsuario;
    }

    /** Setter for property nombreUsuario.
     * @param nombreUsuario New value of property nombreUsuario.
     */
    public void setNombreUsuario (String nombreUsuario) {
    	EIGlobal.mensajePorTrace ( "Validando PE68 de : " + nombreUsuarioReal + " contra: " + nombreUsuario, EIGlobal.NivelLog.INFO);
    	if (nombreUsuarioReal == null || nombreUsuarioReal.indexOf("dispon") > 0 || "".equals(nombreUsuarioReal.trim())) { 
    		nombreUsuarioReal = nombreUsuario;
    	}
    	this.nombreUsuario = nombreUsuario;
    }

    /** Getter for property nominaNumberOfRecords.
     * @return Value of property nominaNumberOfRecords.
     */
    public String getNominaNumberOfRecords () {
	return this.nominaNumberOfRecords;
    }

    /** Setter for property nominaNumberOfRecords.
     * @param nominaNumberOfRecords New value of property nominaNumberOfRecords.
     */
    public void setNominaNumberOfRecords (String nominaNumberOfRecords) {
	this.nominaNumberOfRecords = nominaNumberOfRecords;
    }

    /** Getter for property numeroEmpleado.
     * @return Value of property numeroEmpleado.
     */
    public String getNumeroEmpleado () {
	return this.numeroEmpleado;
    }

    /** Setter for property numeroEmpleado.
     * @param numeroEmpleado New value of property numeroEmpleado.
     */
    public void setNumeroEmpleado (String numeroEmpleado) {
	this.numeroEmpleado = numeroEmpleado;
    }

    /** Getter for property password.
     * @return Value of property password.
     */
    public String getPassword () {
	return this.password;
    }

    /** Setter for property password.
     * @param password New value of property password.
     */
    public void setPassword (String password) {
	this.password = password;
    }

    /** Getter for property pIcalle.
     * @return Value of property pIcalle.
     */
    public String getPIcalle () {
	return this.pIcalle;
    }

    /** Setter for property pIcalle.
     * @param pIcalle New value of property pIcalle.
     */
    public void setPIcalle (String pIcalle) {
	this.pIcalle = pIcalle;
    }

    /** Getter for property pIciudad.
     * @return Value of property pIciudad.
     */
    public String getPIciudad () {
	return this.pIciudad;
    }

    /** Setter for property pIciudad.
     * @param pIciudad New value of property pIciudad.
     */
    public void setPIciudad (String pIciudad) {
	this.pIciudad = pIciudad;
    }

    /** Getter for property pIcolonia.
     * @return Value of property pIcolonia.
     */
    public String getPIcolonia () {
	return this.pIcolonia;
    }

    /** Setter for property pIcolonia.
     * @param pIcolonia New value of property pIcolonia.
     */
    public void setPIcolonia (String pIcolonia) {
	this.pIcolonia = pIcolonia;
    }

    /** Getter for property pIcuenta.
     * @return Value of property pIcuenta.
     */
    public String getPIcuenta () {
	return this.pIcuenta;
    }

    /** Setter for property pIcuenta.
     * @param pIcuenta New value of property pIcuenta.
     */
    public void setPIcuenta (String pIcuenta) {
	this.pIcuenta = pIcuenta;
    }

    /** Getter for property pIdel.
     * @return Value of property pIdel.
     */
    public String getPIdel () {
	return this.pIdel;
    }

    /** Setter for property pIdel.
     * @param pIdel New value of property pIdel.
     */
    public void setPIdel (String pIdel) {
	this.pIdel = pIdel;
    }

    /** Getter for property pIestado.
     * @return Value of property pIestado.
     */
    public String getPIestado () {
	return this.pIestado;
    }

    /** Setter for property pIestado.
     * @param pIestado New value of property pIestado.
     */
    public void setPIestado (String pIestado) {
	this.pIestado = pIestado;
    }

    /** Getter for property pIfolio.
     * @return Value of property pIfolio.
     */
    public String getPIfolio () {
	return this.pIfolio;
    }

    /** Setter for property pIfolio.
     * @param pIfolio New value of property pIfolio.
     */
    public void setPIfolio (String pIfolio) {
	this.pIfolio = pIfolio;
    }

    /** Getter for property pIhomoclave.
     * @return Value of property pIhomoclave.
     */
    public String getPIhomoclave () {
	return this.pIhomoclave;
    }

    /** Setter for property pIhomoclave.
     * @param pIhomoclave New value of property pIhomoclave.
     */
    public void setPIhomoclave (String pIhomoclave) {
	this.pIhomoclave = pIhomoclave;
    }

    /** Getter for property pIrfc.
     * @return Value of property pIrfc.
     */
    public String getPIrfc () {
	return this.pIrfc;
    }

    /** Setter for property pIrfc.
     * @param pIrfc New value of property pIrfc.
     */
    public void setPIrfc (String pIrfc) {
	this.pIrfc = pIrfc;
    }

    /** Getter for property pIrz.
     * @return Value of property pIrz.
     */
    public String getPIrz () {
	return this.pIrz;
    }

    /** Setter for property pIrz.
     * @param pIrz New value of property pIrz.
     */
    public void setPIrz (String pIrz) {
	this.pIrz = pIrz;
    }

    /** Getter for property pItel.
     * @return Value of property pItel.
     */
    public String getPItel () {
	return this.pItel;
    }

    /** Setter for property pItel.
     * @param pItel New value of property pItel.
     */
    public void setPItel (String pItel) {
	this.pItel = pItel;
    }

    /** Getter for property rutaDescarga.
     * @return Value of property rutaDescarga.
     */
    public String getRutaDescarga () {
	return this.rutaDescarga;
    }

    /** Setter for property rutaDescarga.
     * @param rutaDescarga New value of property rutaDescarga.
     */
    public void setRutaDescarga (String rutaDescarga) {
	this.rutaDescarga = rutaDescarga;
    }

    /** Getter for property strMenu.
     * @return Value of property strMenu.
     */
    public String getStrMenu () {
	return this.strMenu;
    }

    /** Setter for property strMenu.
     * @param strMenu New value of property strMenu.
     */
    public void setStrMenu (String strMenu) {
	this.strMenu = strMenu;
    }

    /** Getter for property strMenu.
     * @return Value of property strMenu.
     */
    public String getstrMenu () {
	return this.strMenu;
    }

    /** Setter for property strMenu.
     * @param strMenu New value of property strMenu.
     */
    public void setstrMenu (String strMenu) {
	this.strMenu = strMenu;
    }

    /** Getter for property total.
     * @return Value of property total.
     */
    public String gettotal () {
	return this.total;
    }

    /** Setter for property total.
     * @param total New value of property total.
     */
    public void settotal (String total) {
	this.total = total;
    }

    /** Getter for property totalBit.
     * @return Value of property totalBit.
     */
    public String getTotalBit () {
	return this.totalBit;
    }

    /** Setter for property totalBit.
     * @param totalBit New value of property totalBit.
     */
    public void setTotalBit (String totalBit) {
	this.totalBit = totalBit;
    }

    /** Getter for property totalRegsNom.
     * @return Value of property totalRegsNom.
     */
    public String getTotalRegsNom () {
	return this.totalRegsNom;
    }

    /** Setter for property totalRegsNom.
     * @param totalRegsNom New value of property totalRegsNom.
     */
    public void setTotalRegsNom (String totalRegsNom) {
	this.totalRegsNom = totalRegsNom;
    }

    /** Getter for property userID.
     * @return Value of property userID.
     */
    public String getUserID () {
	return this.userID;
    }

    /** Getter for property userID8.
     * @return Value of property userID8.
     */
    public String getUserID8 () {
	return this.userID8;
    }

    /** Getter for property bandManc.
     * @return Value of property bandManc.
     */
    public String getBandManc () {
	return this.bandManc;
    }

    /** Setter for property userID.
     * @param userID New value of property userID.
     */
    public void setUserID (String userID) {
	this.userID = userID;
    }

    /** Setter for property userID.
     * @param userID8 New value of property userID.
     */
    public void setUserID8 (String userID8) {
	this.userID8 = userID8;
    }

	/** Setter for property bandManc.
     * @param bandManc New value of property bandManc.
     */
    public void setBandManc (String bandManc) {
	this.bandManc = bandManc;
    }

    /** Getter for property userProfile.
     * @return Value of property userProfile.
     */
    public String getUserProfile () {
	return this.userProfile;
    }

    /** Setter for property userProfile.
     * @param userProfile New value of property userProfile.
     */
    public void setUserProfile (String userProfile) {
	this.userProfile = userProfile;
    }

	/** Getter for property archMovs.
     * @return Value of property archMovs.
     */
    public String getArchMovs () {
	return this.archMovs;
    }

	/** Setter for property archMovs.
     * @param archMovs New value of property archMovs
     */
    public void setArchMovs (String archMovs) {
	this.archMovs = archMovs;
    }

    /**
     * Metodo para borrar la lista de facultades.
     */
    public void borraListaFacultades () {
	listaFacultades.clear();
	  EIGlobal.mensajePorTrace("////////////////////////////////////////BaseResource: Se borro la lista de facultades", EIGlobal.NivelLog.DEBUG);
	}


	/** EVERIS Mtodo para obtener un valor al folio.
     * @return Nmero de folio de la transferencia
     */
	public String getFolioArchivo () {
	return this.folioArchivo;
    }


	/**
	 *  EVERIS Mtodo para darle un valor al folio.
	 *
	 * @param folioArchivo the folio archivo
	 */
    public void setFolioArchivo (String folioArchivo) {
	this.folioArchivo = folioArchivo;
    }

	//Facultades de usuario por perfil
	//////////////////////Perfil Basico//////////////////////////////
    /** Atributo static y final para guardar facultad de usuario. */
    public final static String FAC_ABONO_CHEQUES		 = "ABOCHPROP";
    /** Atributo static y final para guardar facultad de usuario. */
    public final static String FAC_CAMBIO_PASSWORD 	 = "CAMBCONTRA";
    /** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CARGO_CHEQUES		 = "CARCHPROP";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CARGO_INVERSION 	 = "CARCIPROP";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CONSULTA_MOVTOS 	 = "CONSMOVTOS";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CONSULTA_POSICION	 = "CONSPOSIPROP";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CONSULTA_SALDO		 = "CONSSALDOS";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_DISPONIBLE_INVERSION	 = "DISPINVPROP";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_DISPONIBLE_VALORES	 = "DISPVALPROP";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_INVERSION_DISPONIBLE	 = "INVDISPPROP";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CONSULTA_BITACORA	 = "CONSBITACORA";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_ALTA_CUENTAS		 = "TEADMALSANM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PROGRAMA_OP		 = "PROGRAMAOPER";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CONS_TRANS		 = "CONSTRANS";		//Seguimiento de Transacciones 18 05 2007 DVS
	///////////////////////// Facultades Cheque Seguridad ///////////////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BENEF_NOREG		= "TECHSLIRM";
	///////////////////////// Facultades Interbancario ///////////////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TRANSF_NOREG		 = "ABOEXTTERNREG";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TRANSF_CHEQ_NOREG	 = "ABOCHTERNORE";//cambio
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TRANSF_TARJ_NOREG	 = "ABOTCTERNORE";//cambio
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_IMPORTA_INTERBAN		 = "TEINTERM";

	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TRANSF_NOREG_INTERNAC	 = "ABOCTAINTERNREG";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TRANSF_INTERNAC 	 = "ABOCTAINTERN";

	///////////////////////// Facultades Credito  ///////////////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_TECRESALDOSC_CRED		=	"TECRESALDOSC";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CONS_MOVTOS_CRED		=	"CONSMOVTOS"; //(ya existe pero se incluye para poder usarla como todas las demas
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CONS_POSICI_TER_CRED	=	"CONSPOSITER";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CONS_SALDO_TER_CRED 	=	"CONSSALDOTER";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CONS_POSICI_CRED		=	"CONSPOSIPROP";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CONS_SALDO_CRED			=	"CONSSALDOS"; //(ya existe pero se incluye para poder usarla como todas las demas
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CONS_MOVS_TER_CRED		=	"CONSMOVTOTER";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_TECREMOVTOSC_CRED		=	"TECREMOVTOSC";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_TECREDISPOSM_CRED		=	"TECREDISPOSM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_TECREPOSICIOC_CRED		=	"TECREPOSICIOC";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_TECREPPAGOSM_CRED		=	"TECREPPAGOSM";
	/////////////////////////Facultades de mantenimiento a cuentas///////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_ALTA_CTAS_SANTAN		=	"TEADMALSANM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_ALTA_CTAS_OTROSBANC 	=	"TEADMALOTRM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_BAJA_CTAS				=	"BACTAPROP";//cambio

	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_ALTA_CTAS_INTERNAC		=	"ALTACTAINTERN";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_BAJA_CTAS_INTERNAC		=	"BAJACTAINTERN";
	///////////////////////////////FACULTADES DE TESORERIA//////////////////////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacCapConcen	=	"TETESCONCENM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacCapDisper	=	"TETESDISPERM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacCapFond		=	"TETESFONAUTM";

	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacConEstr		=	"TETESESTRUCC";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacCopEstr		=	"TETESUTILERM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacActParam 	=	"TETESACTHOSM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacConSaldos	=	"TETESSALDGRC";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FacConMovtos	=	"TETESMOVSGRC";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_OPERA_CAMREG	=	"OPERACAMBIOSREG";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_OPERA_CAMNOREG	=	"OPERACAMBINOREG";

	//////////////////////////Facultades de Impuestos Federales//////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CON_PAG_PI = "TEIMFCONPAGC";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CAP_PAG_PI = "TEIMFCAPPAGM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CAN_PAG_PI = "TEIMFCANPAGM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_ALT_CTA_PI = "TEIMFALTCTAM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String	FAC_CON_DEC_PI = "TEIMFCONDEC";

	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_ALTA_CTA_CONTRATO	 = "TECHSALTM";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BAJA_CTA_CONTRATO	 =  "TECHSBAJM";

	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_INVERSION_TERCEROS	  = "SIINVERTER";
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BANCO_TERCEROS		  = "SIBANCOTER";

	//////////////////////////Facultades de Credito Electronico//////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TECREPOSICIOC_CRED_ELEC = "TECREPOSICIOC";//Facultad para Posicin de crdito electrnico
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TECRESALDOSC_CRED_ELEC	= "TECRESALDOSC";//Facultad para realizar Consultas de Saldos de Crdito y Consultas de Lineas de Credito
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TECREDISPOSC_CRED_ELEC	= "TECREDISPOSC";//Facultad para Disposiciones de crdito electrnico
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TECREPPAGOSM_CRED_ELEC	= "TECREPPAGOSM";//Facultad para Pagos de crdito electrnico
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_TECREMOVTOSC_CRED_ELEC	= "TECREMOVTOSC";//Facultad para Movimientos
	//CONSSALDOS ya existe en las facultades de Credito "OJO"
	//////////////////////////Facultades de Base Cero//////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASCONSAL_BC  = "BASCONSAL";//Facultad para Consulta de Saldos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASEXPORT_BC  = "BASEXPORT";//Facultad para  Exportacin de Saldos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASCONDEV_BC  = "BASCONDEV";//Facultad para Consulta Devoluciones
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASAGRCTA_BC  = "BASAGRCTA";//Facultad para agregar cuentas a la estructura
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASELICTA_BC  = "BASELICTA";//Facultad para eliminar cuentas de la estructura
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASBOREST_BC  = "BASBOREST";//Facultad para eliminar esctructura
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASGUARES_BC  = "BASGUARES";//Facultad para guardar la estructura localmente
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASENVEST_BC  = "BASENVEST";//Facultad para enviar la estructura (ALTA)
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASECONEST_BC = "BASECONEST";//Facultad para consultar estructura
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASBORPRO_BC  = "BASBORPRO";//Facultad para eliminar Fechas PrograMADAS
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASPROGRA_BC  =  "BASPROGRA"; // Facultad para programacin manual de fechas
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASIMPORT_BC  =  "BASIMPORT"; // Facultad para importacin de esquema base cero
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_BASACCESO     =  "BASACCESO"; // Facultad de Acceso al Modulo Esquema Base Cero
    //////////////////////////Facultades de Pago Directo//////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRENVARC_PD = "PADIRENVARC";//Facultad de envio y creacin de archivo
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRALTMAN_PD = "PADIRALTMAN";//Facultad de alta manual
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRIMPARC_PD = "PADIRIMPARC";//Facultad de importar archivo
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRMTTOAR_PD = "PADIRMTTOAR";//Facultad de manipulacin de registros
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRMODSER_PD = "PADIRMODSER";//Facultad para modificar un archivo ya enviado
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRACTARC_PD = "PADIRACTARC";//Facultad para recuperar el archivo enviado
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRDEPURA_PD = "PADIRDEPURA";//Facultad para eliminar registros de la vista
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRCANCEL_PD = "PADIRCANCEL";//Facultad para cancelar pagos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRACATSU_PD = "PADIRACATSU";//Facultad para consultar el catlogo de sucursales
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIREXPARC_PD = "PADIREXPARC";//Facultad para exportar archivos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRMTOCUE_PD = "PADIRMTOCUE";//Facultad para mantenimiento de cuentas
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRACTCUE_PD = "PADIRACTCUE";//Facultad para actualizar las cuentas asociadas al contrato
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRACTBEN_PD = "PADIRACTBEN";//Facultad para actualizacion de Beneficiiarios
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRMTOBEN_PD = "PADIRMTOBEN";//Facultad para alta, baja y modificacion de beneficiarios
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_PADIRIMPREP_PD = "PADIRIMPREP";//Facultad para imprimir el catalogo de Beneficiarios
	//TECHSALTM y TECHSBAJM ya existen son FAC_ALTA_CTA_CONTRATO y FAC_BAJA_CTA_CONTRATO
	//////////////////////////Facultades de Confirmig//////////////////////
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCACTUARCH_CONF  = "CCACTUARCH";// Facultad para actualizacion de Archivos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCALTMANPR_CONF  = "CCALTMANPR";//Facultad para Alta a Proveedores
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCALTMANPAG_CONF = "CCALTMANPAG";//Facultad para Alta de Pagos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCBAJLOCPR_CONF  = "CCBAJLOCPR";//Facultad para Baja de Proveedores
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCBAJLOCPAG_CONF = "CCBAJLOCPAG";//Facultad para Baja de Pagos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCELIMIARCH_CONF = "CCELIMIARCH";//Facultad para para borrar archivo
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCENVARCHPR_CONF = "CCENVARCHPR";//Facultad para para enviar archivo proveedores
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCENVARCHPA_CONF = "CCENVARCHPA";//Facultad para para enviar archivo pagos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCEXPORARCH_CONF = "CCEXPORARCH";//Facultad para Exportacion de Archivos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCIMPARCHPR_CONF = "CCIMPARCHPR";//Facultad para  para Importar Archivo Proveedores
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCIMPARCHPA_CONF = "CCIMPARCHPA";//Facultad para para Importar Archivo Pagos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCIMPRREPOR_CONF = "CCIMPRREPOR";//Facultad para impresion de Reportes
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCMODLOCPR_CONF  = "CCMODLOCPR";//Facultad para modificar proveedores
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCMODLOCPAG_CONF = "CCMODLOCPAG";//Facultad para modificar pagos
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCCANCELPAG_CONF = "CCCANCELPAG";//Facultad para realizar cancelaciones
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCMODPROSER_CONF = "CCMODPROSER";//Facultad para modificar informacion de proveedor
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_CCBAJAPROV_CONF  = "CCBAJAPROV";//Facultad para dar de baja a Proveedor en servidor
	/** Atributo static y final para guardar facultad de usuario. */
	public final static String FAC_ABON_INT_NREG	= "ABOCTAINTERNREG";
	//////////////////////////Facultades de Norma43//////////////////////
	/** Facultad para alta de programaciones de consulta */
	public final static String FAC_ALPROGCONSMOV_N43  = "ALPROGCONSMOV";// Facultad para alta de programaciones de consulta
	/** Facultad para modificacion de consultas. */
	public final static String FAC_MODPROGMOV_N43	= "MODPROGMOV";//Facultad para modificacion de consultas
	/** Facultad para cancelacion de consulta. */
	public final static String FAC_CANPROGMOV_N43 = "CANPROGMOV";//Facultad para cancelacion de consulta
	/** Facultad para consultas de programaciones. */
	public final static String FAC_CONSPROGVIG_N43  = "CONSPROGVIG";//Facultad para consultas de programaciones
	/** Facultad para consultas de resultados. */
	public final static String FAC_CONSREPROG_N43 = "CONSREPROG";//Facultad para consultas de resultados
	//////////////////////////Facultades PREMIER NOMINA//////////////////////
	/** Facultad para cliente premier. */
	public final static String FAC_CLIENTE_PREMIER = "INOMHORARIOS";//Facultad para cliente premier
	/** Facultad para realizar oferta a empleado. */
	public final static String FAC_NOMINA_OFERTA = "INOMOFERTA";//Facultad para realizar oferta a empleado
	//////////////////////////Facultades de pago SAR en lnea //////////////////////
	/** Facultad para pago SAR en linea. */
	public final static String FAC_PAGO_SAR_ENL = "SARPAGOENL";//Facultad para pago SAR en lnea
	/** Facultad para cancelacin de pago de SAR. */
	public final static String FAC_CANC_SAR_ENL = "SARCANCENL";//Facultad para cancelacin de pago de SAR
	/** Facultad para consulta de pago de SAR. */
	public final static String FAC_CONS_SAR_ENL = "SARCONSENL";//Facultad para consulta de pago de SAR
	/** Facultad para reimpresin de comprobantes. */
	public final static String FAC_REIM_SAR_ENL = "SARREIMENL";//Facultad para reimpresin de comprobantes
	//////////////////////////Facultades de pago Micrositio //////////////////////
	/** Facultad Abono Pago Micrositio. */
	public final static String FAC_ABOMICRO_PAGCH = "ABOMICROPAGCH"; //Facultad Abono Pago Micrositio

	/** ****************************************************. */
	/* TESOFE					       */
	/*******************************************************/
	public final static String FAC_RECDET = "TESORECDET"; // Recaudacion Detalle
	/** Recaudacion Consulta. */
	public final static String FAC_RECCON = "TESORECCON"; // Recaudacion Consulta
	/** Importacion archivos de devolucion. */
	public final static String FAC_IMPDEV = "TESOIMPDEV"; // Importacion archivos de devolucion
	/** Importacion archivos de cancelacion. */
	public final static String FAC_IMPCAN = "TESOIMPCAN"; // Importacion archivos de cancelacion
	/** Envio archivos de devolucion. */
	public final static String FAC_ENVDEV = "TESOENVDEV"; // Envio archivos de devolucion
	/** Envio archivos de cancelacion. */
	public final static String FAC_ENVCAN = "TESOENVCAN"; // Envio archivos de cancelacion
	/** Consulta de registros. */
	public final static String FAC_CONREG = "TESOCONREG"; // Consulta de registros
	/** Consulta de archivos. */
	public final static String FAC_CONARC = "TESOCONARC"; // Consulta de archivos
	/*******************************************************/

	/*******************************************************/
	/* NUEVO MODULO ALTA DE CUENTAS                        */
	/*******************************************************/
	/** Alta de cuentas MB Prop. */
	public final static String FAC_ALTA_MB_P    = "ALTACTASANPROP";	// Alta de cuentas MB Prop
	/** Alta de cuentas MB Terceros. */
	public final static String FAC_ALTA_MB_T    = "ALTACTASANTER";		// Alta de cuentas MB Terceros
	/** Alta de cuentas BN. */
	public final static String FAC_ALTA_BN	    = "ALTACTAOTRB";		// Alta de cuentas BN
	/** Alta de cuentas BI. */
	public final static String FAC_ALTA_BI      = "ALTACTAINTER";		// Alta de cuentas BI
	/** Alta de cuentas NM. */
	public final static String FAC_ALTA_NM      = "ALTACTAMOVIL";		// Alta de cuentas NM
	/** Baja de cuentas MB. */
	public final static String FAC_BAJA_MB      = "BAJACTASAN";		// Baja de cuentas MB
	/** Baja de cuentas BN. */
	public final static String FAC_BAJA_BN      = "BAJACTAOTR";		// Baja de cuentas BN
	/** Baja de cuentas BI. */
	public final static String FAC_BAJA_BI      = "BAJACTAINTER";		// Baja de cuentas BI
	/** Autorizacion de Alta y Baja MB. */
	public final static String FAC_AUTO_AB_MB   = "AUTOCTAPROP";		// Autorizacion de Alta y Baja MB
	/** Autorizacion de Alta y Baja (otros bancos) Bn,BI. */
	public final static String FAC_AUTO_AB_OB   = "AUTOCTATER";		// Autorizacion de Alta y Baja (otros bancos) Bn,BI
	/*******************************************************/

	/*******************************************************/
	/* NOMINA INTERBANCARIA                                */
	/*******************************************************/
	public final static String FAC_NOMINT_IMPO="INOMIMPAPAG";
	public final static String FAC_NOMINT_ALTA="INOMALTAPAG";
	public final static String FAC_NOMINT_EDIT="INOMMODIPAG";
	public final static String FAC_NOMINT_BAJA="INOMBAJAPAG";
	public final static String FAC_NOMINT_ENVI="INOMENVIPAG";
	public final static String FAC_NOMINT_RECU="INOMACTUPAG";
	public final static String FAC_NOMINT_EXPO="INOMEXPOPAG";
	public final static String FAC_NOMINT_BORR="INOMBORRPAG";
	public final static String FAC_NOMINT_ACCE="INOMACCESINTER";		// Acceso al modulo
	public final static String FAC_NOMINT_CTNR="INOMENVINTERB";		// Cuentas no registradas
	/*******************************************************/


	/*******************************************************/
	/* OTPs                                                */
	/*******************************************************/
	public final static String FAC_VAL_OTP		 = "ACCESINTEROTP";
	/*******************************************************/


	/*******************************************************/
	/* SUPERUSUARIO                                        */
	/*******************************************************/
	public final static String FAC_ALTA_SUSU	 = "ALTAVINUSRSUP";
	public final static String FAC_BAJA_SUSU	 = "BAJAVINUSRSUP";
	public final static String FAC_MOD_SUSU		 = "MODFACUSRSUP";
	public final static String FAC_LYM_USR		 = "MODLMUSSUP";
	public final static String FAC_LYM_CONTR	 = "MODLMCTOSUP";
	public final static String FAC_MEDIOS_NOTI	 = "CAMMEDNOTSUP";
	/*******************************************************/


	
    public String toString () {
	return "facultades.size: " + listaFacultades.size ();
    }

    public void setSeguir(boolean seguir) {
        this.seguir = seguir;
    }

    public boolean getSeguir() {
        return seguir;
    }

    public static String getTiempo(long time) {
        String hora = new Timestamp(time).toString();
        return "to_date('" + hora.substring(0, hora.indexOf(".")) + "', 'yyyy-mm-dd HH24:MI:SS')";
    }

    public static String getTiempo() {
        return getTiempo(Calendar.getInstance().getTime().getTime());
    }


    protected void finalize() throws Throwable {
        super.finalize();
        seguir = false;
        hilo = null;
        if(listaFacultades != null) {
			listaFacultades.clear();
			listaFacultades = null;
		}
        tok = null;

    }

    public String getRetToken() {
		return retToken;
	}

    public void setRetToken(String valorRet) {
		retToken = valorRet;
	}

    public boolean getValidarToken() {
		return validarToken;
	}

    public void setValidarToken(boolean valor) {
		validarToken = valor;
	}

    public boolean getValidarSesion() {
		return validarSesion;
	}

    public void setValidarSesion(boolean valor) {
		validarSesion = valor;
	}

    public boolean getAdmonToken() {
		return admonToken;
	}

    public void setAdmonToken(boolean valor) {
		admonToken = valor;
	}

    public boolean getTokenValidado() {
		return tokenValidado;
	}

    public void setTokenValidado(boolean valor) {
		tokenValidado = valor;
	}

    public Token getToken() {
		return tok;
	}

    public void setToken(Token valor) {
		tok = valor;
	}

	public String getLigaREDIR() {
		return ligaREDIR;
	}

	public void setLigaREDIR(String ligaREDIR) {
		this.ligaREDIR = ligaREDIR;
	}

    /*<VC proyecto="200710001" autor="JGR" fecha="29/05/2007" descripcion="LIGA A SUPERNET">*/
	public String getUrlSuperNet() {
		return urlSuperNet;
	}

	public void setUrlSuperNet(String urlSuperNet) {
		this.urlSuperNet = urlSuperNet;
	}
	/*<VC*/
	//JPH vswf requerimiento liga enlace supernet comercios

	public HashMap<String, String> getCtasSnetComercios() {
		return ctasSnetComercios;
	}

	public void setCtasSnetComercios(HashMap<String, String> ctasSnetComercios) {
		this.ctasSnetComercios = ctasSnetComercios;
	}

	public void setUltAccesoStr(String ultAccesoStr) {
		this.ultAccesoStr = ultAccesoStr;
	}

	public String getUltAccesoStr() {
		return this.ultAccesoStr;
	}

	public String getNombreUsuarioReal() {
		return nombreUsuarioReal;
	}

	public void setNombreUsuarioReal(String nombreUsuarioReal) {
		this.nombreUsuarioReal = nombreUsuarioReal;
	}

	public String getIpLogin() {
		return ipLogin;
	}

	public void setIpLogin(String ipLogin) {
		this.ipLogin = ipLogin;
	}

	public MensajeUSR getMensajeUSR() {
		return mensajeUSR;
	}

	public void setMensajeUSR(MensajeUSR mensajeUSR) {
		this.mensajeUSR = mensajeUSR;
	}
	//fin

	public boolean isCierraSesion() {
		return cierraSesion;
	}

	public void setCierraSesion(boolean cierraSesion) {
		this.cierraSesion = cierraSesion;
	}

	public boolean isExcepcionCierraSess() {
		return excepcionCierraSess;
	}

	public void setExcepcionCierraSess(boolean excepcionCierraSess) {
		this.excepcionCierraSess = excepcionCierraSess;
	}

	public String getBucContrato() {
		return bucContrato;
	}

	public void setBucContrato(String bucContrato) {
		this.bucContrato = bucContrato;
	}

	public HashMap getCarrierCompleto() {
		return carrierCompleto;
	}

	public void setCarrierCompleto(HashMap carrierCompleto) {
		this.carrierCompleto = carrierCompleto;
	}

	public String getStatusCelular() {
		return statusCelular;
	}

	public void setStatusCelular(String statusCelular) {
		this.statusCelular = statusCelular;
	}

	public String getStatusCorreo() {
		return statusCorreo;
	}

	public void setStatusCorreo(String statusCorreo) {
		this.statusCorreo = statusCorreo;
	}

	public boolean isCampaniaActiva() {
		return campaniaActiva;
	}

	public void setCampaniaActiva(boolean campaniaActiva) {
		this.campaniaActiva = campaniaActiva;
	}

	public boolean isCampaniaCtoActiva() {
		return campaniaCtoActiva;
	}

	public void setCampaniaCtoActiva(boolean campaniaCtoActiva) {
		this.campaniaCtoActiva = campaniaCtoActiva;
	}

    /**
	 * @return  boolean : isValidaRSA
	 */
	public boolean isValidaRSA() {
		return validaRSA;
	}

	/**
	 * @param validaRSA : validaRSA
	 */
	public void setValidaRSA(boolean validaRSA) {
		this.validaRSA = validaRSA;
	}
	/**
	 * @return the csrfToken
	 */
	public String getCsrfToken() {
		return csrfToken;
	}
	/**
	 * @param csrfToken the csrfToken to set
	 */
	public void setCsrfToken(String csrfToken) {
		this.csrfToken = csrfToken;
	}



}