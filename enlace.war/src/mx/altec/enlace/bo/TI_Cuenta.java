package mx.altec.enlace.bo;

public class TI_Cuenta
	{

	// --- Campos ----------------------------------------------------------------------
	private String numCta;
	private String descripcion;

	// --- Constructores ---------------------------------------------------------------
	public TI_Cuenta(String numCta)
		{
		this.numCta = numCta;
		this.descripcion = "";
		}

	public TI_Cuenta(String numCta, String descripcion)
		{
		this.numCta = numCta;
		this.descripcion = descripcion;
		}

	// --- Regresa la descripci�n ------------------------------------------------------
	public String getDescripcion()
		{return descripcion;}

	// --- Modifica la descripci�n -----------------------------------------------------
	public void setDescripcion(String descripcion)
		{
		if(descripcion == null) descripcion = "";
		this.descripcion = descripcion;
		}

	// --- Regresa el n�mero de cuenta -------------------------------------------------
	public String getNumCta()
		{return numCta;}

	// --- Modifica el n�mero de cuenta ------------------------------------------------
	protected void setNumCta(String numCta)
		{this.numCta = numCta;}

	// --- regresa una trama de la cuenta ----------------------------------------------
	public String trama()
		{
		return "@" + numCta + "|" + (descripcion.equals("")?" ":descripcion);
		}

	// --- m�todo equals para indicar si se trata de la misma cuenta -------------------
	public boolean equals(Object x)
		{
		if(!(x instanceof TI_Cuenta)) return false;
		return ((TI_Cuenta)x).getNumCta().equals(getNumCta());
		}

	// --- ESTATICO: crea una cuenta a partir de una trama -----------------------------
	static public TI_Cuenta creaCuenta(String trama)
		{
		String numCuenta;
		String descripcion;
		int a, b;

		a = trama.indexOf("@");
		b = trama.indexOf("|");
		if(a!=0 || b < 1) return null;
		numCuenta = trama.substring(1,b);
		descripcion = trama.substring(b+1);
		numCuenta = numCuenta.trim();
		if(numCuenta.equals("")) return null;
		return new TI_Cuenta(numCuenta,descripcion);
		}

	}
