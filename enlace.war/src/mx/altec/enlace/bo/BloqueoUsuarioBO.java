package mx.altec.enlace.bo;

import mx.altec.enlace.dao.BloqueoUsuarioDAO;

public class BloqueoUsuarioBO {
	
	/**
	 * Valida si el usuario esta bloqueado
	 * @param contrato : contrato
	 * @param usuario : usuario
	 * @return bloqueado : bloqueado
	 */
	public boolean isBlockedUser(String contrato, String usuario){
		
		BloqueoUsuarioDAO bloqueoUsuario = new BloqueoUsuarioDAO();
		
		return bloqueoUsuario.isBlockedUser(contrato, usuario);
	}

}
