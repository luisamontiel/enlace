/*****************************************************************************
 Banco Santander Mexicano
 Clase: RespManc  -- Clase para el manejo de respuestas del servicio de autori-

 Fecha de creacion :    24 de abril de 2003	Horacio
 Fecha de Modificacion: 30 de enero de 2004 Veronica Cervantes Lara
*****************************************************************************/

package mx.altec.enlace.bo;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class RespMancSA implements java.io.Serializable
{
    private String cta_cargo = "";
    private String cta_abono = "";
    private String importe = "";
    private String folio_registro = "";
    private String folio_archivo = "";
    private String folio_auto = "";
    private String fecha_auto = "";
    private String estatus = "";

    private String cta_cargo_inter = "";		// 1 dita
    private String cta_abono_inter = "";		// 2 dita
	private String importeMN_inter = "";		// 3 dita
	private String importeUSD_inter = "";		// 4 dita
	private String importeDivisa_inter = "";
	private String tipoCambioMN_inter = "";		// 5 dita
	private String tipoCambioDA_inter = "";
	private String fecha_auto_inter = "";		// 6 dita
    private String concepto_inter = "";			// 7 dita
	private String orden_inter = "";			// 8 dita
    private String folio_auto_inter = "";		// 9 dita referencia
	private String folio_registro_inter = "";
	private String rfc_inter = "";				// 12 dita
	private String iva_inter = "";				// 13 dita
    private String usuario2_inter = "";			// 10 dita
	private String contrato_inter = "";			// 11 dita
	private String divisaAbono_inter = "";
	private String divisaCargo_inter = "";


	private String beneficiario = "";
	private String divisaAbono = "";
	private String divisaCargo = "";
	private String cveEspecial = "";
	private String banco = "";					// 14	dita
	private String ciudad = "";
	private String pais = "";
	private String sucursal = "";
    private String titular = "";

	private String estatus_inter = "";
	private String tramaok_inter = "";
	private boolean Internacional = false;
	private boolean sipare = false;
	private String referenciaSipare = "";
	private String lc = "";

   public RespMancSA () {}

    /** Constructor
     * @param datos DatosManc Bean con datos de mancomunidad para contruir bean de respuesta
     * @param fecha String con la fecha de autorizaci&oacute;n o cancelaci&oacute;n
     * @param folio String con el folio de la autorizaci&oacute;n o cancelaci&oacute;n
     * @param estatus String con el estatus final de la operaci&oacute;n
     */
    public RespMancSA (DatosMancSA datos,  String fecha_auto,
    		String folio_auto, String estatus, boolean indSipare, String refSipare)
	{
        this.cta_cargo = datos.getCta_cargo ();
        this.cta_abono = datos.getCta_abono ();
        this.folio_registro = datos.getFolio_registro ();
        this.folio_archivo = datos.getFolio_archivo();
        this.importe = datos.getImporte ();
        this.fecha_auto = fecha_auto;
        this.folio_auto = folio_auto;
        this.estatus = estatus;
        this.sipare = indSipare;
        this.referenciaSipare = refSipare;
    }


    /** Metodo para obtener el bean en formato HTML para desplegar en la pagina de respuesta
     * @param index Indice del bean actual para el formato de color
     * @return String con el bean formateado en HTML
     */
    public String getFormatoHTML (int index)
	{
		java.text.NumberFormat formatoMoneda = java.text.NumberFormat.getCurrencyInstance (Locale.US);
		String color = (index % 2) != 0 ? "textabdatcla" :  "textabdatobs";
        String row = "<tr>";
        System.out.println("CCBInternacional"+ Internacional);

		if(Internacional)
        {
	        row += "\n <td align='center' class='" + color + "'>" + cta_cargo_inter + "</td>";
	        row += "\n <td align='center' class='" + color + "'>" + cta_abono_inter + "</td>";

			System.out.println ("-------------------------divi. cargo: "+divisaCargo_inter);
			System.out.println ("-------------------------divi. abono: "+divisaAbono_inter);

			String tmpUSD = formatoMoneda.format (Double.parseDouble (importeUSD_inter));
			if (tmpUSD.substring(0, 3).equals ("MXN"))
				tmpUSD = "$ " + tmpUSD.substring(3, tmpUSD.length());
			String tmpDiv = formatoMoneda.format (Double.parseDouble (importeDivisa_inter));
			if (tmpDiv.substring(0, 3).equals ("MXN"))
				tmpDiv = "$ " + tmpDiv.substring(3, tmpDiv.length());

			if ( (  divisaAbono_inter.equals("DA") && divisaCargo_inter.equals("USD")  ) ||
			     (  divisaAbono_inter.equals("USD") && divisaCargo_inter.equals("DA")  )
			   )  //es dolar a dolar
				row += "<td align='right' class='" + color + "'>" + tmpUSD + "</td>";
			else	// es cualquier otra combinacion
				row += "<td align='right' class='" + color + "'>" + tmpDiv + "</td>";

	        row += "\n <td align='center' class='" + color + "'>" + folio_registro_inter + "</td>";

			System.out.println("CCBEstatus Internacional vcl = " + estatus_inter);
			System.out.println("CCBTramaOK = "+ tramaok_inter);

			if ( estatus_inter.equals("ENVIADA") )
			{
				System.out.println ("............... Entro por que fue ENVIADA hola angie ..........................");
				//row += "<td align='center' class=' "+ color + "'><A  href=\"javascript:GenerarComprobante("+tramaok_inter+");\">"+folio_auto_inter+"</A></TD>";

				row += "\n <td align='center' class='" + color + "'>";
				row += "\n     <A  href=\"javascript:GenerarComprobante('hdnTrama"+folio_auto_inter+"');\">"+folio_auto_inter+"</A>";
				row += "\n     <input type='hidden' name='hdnTrama"+folio_auto_inter+"' value='"+tramaok_inter+"'>";
				row += "\n</td>";
				row += "\n <td align='center' class='" + color + "'>" + fecha_auto_inter + "</td>";
				row += "\n <td align='center' class='" + color + "'><font color='green'>" + estatus_inter + "</font></td>";

				System.out.println ("ROW: \n"+row);
			}
			else
			{
				row += "\n <td align='center' class='" + color + "'>" + folio_auto_inter + "</td>";
				row += "\n <td align='center' class='" + color + "'>" + fecha_auto_inter + "</td>";
		        row += "\n <td align='center' class='" + color + "'><font color='red'>" + estatus_inter + "</font></td>";
			}
        }
		else	// si no es un resultado de mancomunidad
		{

			String tmpImp = formatoMoneda.format (Double.parseDouble (importe));
			if (tmpImp.substring(0, 3).equals ("MXN"))
				tmpImp = "$ " + tmpImp.substring(3, tmpImp.length());

	        row += "<td align='center' class='" + color + "'>" + cta_cargo + "</td>";
	        row += "<td align='center' class='" + color + "'>" + cta_abono + "</td>";
	        row += "<td align='right'  class='" + color + "'>" + tmpImp + "</td>";
	        row += "<td align='center' class='" + color + "'>" + folio_registro + "</td>";
	        row += "<td align='center' class='" + color + "'>" + folio_auto + "</td>";
	        row += "<td align='center' class='" + color + "'>" + fecha_auto + "</td>";
	        row += "<td align='center' class='" + color + "'>" + estatus + "</td>";
	        if (isSipare()) {
	        	String link = "";
	        	if (referenciaSipare.trim().length() > 0) {
					link ="<a href=\"javascript:VentanaComprobanteSUALC(";
					link += "'"+ referenciaSipare + "','"+lc+"')"; // referencia
					link += "\">" + referenciaSipare;
	        	}
	        	row += "<td align='center' class='" + color + "'>" + link + "</td>";
	        }
		}
        row += "</tr>\n";
        return row;
    }


    /** Metodo para obtener el bean en formato HTML para desplegar en la pagina de respuesta de la cancelación de nominas
     * @param index Indice del bean actual para el formato de color
     * @return String con el bean formateado en HTML
     */
    public String getFormatoHTMLCN (int index)
	{
		java.text.NumberFormat formatoMoneda = java.text.NumberFormat.getCurrencyInstance (Locale.US);
		String color = (index % 2) != 0 ? "textabdatcla" :  "textabdatobs";
        String row = "<tr>";
		String tmpImp = formatoMoneda.format (Double.parseDouble (importe));
		if (tmpImp.substring(0, 3).equals ("MXN"))
			tmpImp = "$ " + tmpImp.substring(3, tmpImp.length());
        row += "<td align='center' class='" + color + "'>" + folio_archivo + "</td>";
        row += "<td align='center' class='" + color + "'>" + folio_auto + "</td>";
        row += "<td align='center' class='" + color + "'>" + fecha_auto + "</td>";
        row += "<td align='center' class='" + color + "'>" + estatus + "</td>";
        row += "</tr>\n";
        return row;
    }


	/**
	 * @return el sipare
	 */
	public boolean isSipare() {
		return this.sipare;
	}

	/**
	 * @param sipare el sipare a establecer
	 */
	public void setSipare(boolean sipare) {
		this.sipare = sipare;
	}

	/**
	 * @return el referenciaSipare
	 */
	public String getReferenciaSipare() {
		return this.referenciaSipare;
	}

	/**
	 * @param referenciaSipare el referenciaSipare a establecer
	 */
	public void setReferenciaSipare(String referenciaSipare) {
		this.referenciaSipare = referenciaSipare;
	}
}