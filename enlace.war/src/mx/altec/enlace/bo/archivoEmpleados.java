/** Banco Santander Mexicano
 *		Clase ANom_mtto_emp
 *		@author Gerardo Salazar Vega
 *		@version 1.0
 *		fecha de creacion: 08 de Noviembre del 2000
 *		responsable: Roberto Guadalupe Resendiz Altamirano
 * 	descripcion: Clase utilizada para el manejo de archivos
 *
 *   Modificaciones:
 *       Javier D�az (Sinapsis) - 01/2004. Se a�adi� un m�todo para regresar la trama requerida
 *                                         para validar la duplicidad de archivos de Altas Masivas.
 *
 *
 *   Modificaciones:
 *       Isaac Galv�n La�ado (Sinapsis) - 06/01/2005. Cambios:
 *                   1. cadenaValidacion cambia de ser String a StringBuffer para que al momento de anexarle informaci�n
 *                      se utilice el m�todo append() de la misma clase. Acutalmente el anexo de informaci�n se realiza
 *                      con la asignaci�n += (ej. cadenaValidacion += "Informaci�n anexada";
 *                   2. Las cadenas que contienen los campos obtenidos de los registros del archivo son ahora declaradas como
 *                      est�ticas (static).
 */
/*
Christian Barrios Osornio (cbo) Q6892  09/09/04
HACER  OBLIGATORIOS EN LA CAPTURA MANUAL DE EMPLEADOS Y EN LA VALIDACI�N DE LA IMPORTACI�N DE ARCHIVOS
LOS SIGUIENTES CAMPOS EN EL  NUEVO  LAYOUT  DE ALTA  DE EMPLEADOS LOS  DATOS  SON:
DOMICILIO OFICINA,
COLONIA OFICINA,
C�DIGO POSTAL OFICINA,
PREFIJO DEL TEL�FONO OFICINA
Y TEL�FONO DE OFICINA
 */

/*******************************************************************************
 * INCIDENCIA:  Q9337
 * Autor:       Josu� Mart�nez Buenrrostro
 * Descripci�n: Eliminar la concatenaci�n con objetos tipo String
 *             (para optimizar la carga de la p�gina).
 *******************************************************************************/

package mx.altec.enlace.bo;
import java.io.*;

import java.util.StringTokenizer;
//import java.text.DecimalFormat;


import java.util.*;
import java.io.*;
import java.lang.String;
import java.text.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class archivoEmpleados {

    public String tipoRegistro = "",
            NumeroSecuencia = "",
            NumeroEmpleado = "",
            NumeroDepto  = "",
            ApellidoPaterno   = "",
            ApellidoMaterno = "",
            Nombre = "",
            RFC = "",
            Homoclave = "",
            Sexo = "",
            Nacionalidad  = "",
            EstadoCivil = "",
            NombreCorto = "",
            Domicilio = "",
            Colonia = "",
            DelegacionoMpio = "",
            CiudadoPoblacion = "",
            CodigoPostal = "",
            Pais = "",
            EstatusCasa = "",
            FechaResidencia = "",
            ClaveEnvio = "",
            DomicilioOfna = "",				//------->Este
            ColoniaOficina = "",			//------->Este
            DeloMpioOficina = "",
            CdoPobOficina = "",
            CodigoPostalOfna = "",			//------->Este
            PaisOficina = "",
            Telefono = "",					//------->Este --------->??
            Extension = "",
            IngresoMensual = "",
            NumeroCuenta = "",
            NumeroTarjeta = "",
            Sucursal = "",
            FechaIngreso="",
            ParTelefono="",
            PrefijoTelefono = "",			//------->Este
            PrefijoParTelefono = "",
            TelOfCompleto=""; // Agrego tel para tomar del archivo tanto el campo de prefijo + lada + telefono y limitarlo a 12 posic por Eric Gomez


   /*Modificado por Isaac Galvan La�ado
   *Cambio de la variable cadenaValidacion de String a StringBuffer. Originalmente: public String cadenaValidacion = "",
   *public String cadenaValidacion  = "";
   */
    public StringBuffer cadenaValidacion   = new StringBuffer();
    /*Modificado por Isaac Galvan La�ado
   *Cambio de la variable lista_empleados de String a StringBuffer. Originalmente: public String lista_empleados = "",
   *public String lista_empleados  = "";
   */
    public StringBuffer lista_empleados = new StringBuffer("").append(",");
    public int totalRegistros        = 0;
    public String archivoSalida      = "";
    int contadorErrores              = 1;
    public String asFechaTransmision = "", asTotalRegistros = "", asAceptados = "", asRechazados = "", asFechaActualizacion = "";
    RandomAccessFile archivoTrabajo;
    long posicionReg                 = 0;

    private BaseServlet BaseAppLogicEmp;
    private final int LEN_LINEA = 543;
    //BaseResource session;
    private String Banco ="";
                /*
                 *	Constructor de la clase
                 *	Recibe una instancia de la clase BaseAppLogicEmp para poder hacer llamados a otros metodos contenido en esa clase
                 *	Se encarga de crear el archivo con un objeto RandomAccessFile
                 */
    //public archivoEmpleados( String rutaArchivo,BaseServlet BApp,String banco ){
    public archivoEmpleados( String rutaArchivo,String banco ){
        //BaseAppLogicEmp = BApp;
        //modificacion para integracion
        Banco=banco;
        EIGlobal.mensajePorTrace("------------------------------------ Archivo Empleados 2------------------------------------------", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Parametro inicial rutaArchivo:"+rutaArchivo, EIGlobal.NivelLog.INFO);
        archivoSalida=IEnlace.DOWNLOAD_PATH+rutaArchivo.substring(rutaArchivo.lastIndexOf("/")+1)+"_emp_sal.doc";
        try {
            archivoTrabajo = new RandomAccessFile(rutaArchivo,"rw");

            EIGlobal.mensajePorTrace("Creando archivo: "+rutaArchivo, EIGlobal.NivelLog.INFO);
        } catch(IllegalArgumentException e) {

            EIGlobal.mensajePorTrace("1: Error al abrir el Archivo: " + e, EIGlobal.NivelLog.INFO);
        } catch(FileNotFoundException e) {

            EIGlobal.mensajePorTrace("2: Error al abrir el Archivo: " + e, EIGlobal.NivelLog.INFO);
        } catch(SecurityException e) {

            EIGlobal.mensajePorTrace("3: Error al abrir el Archivo: " + e, EIGlobal.NivelLog.INFO);
        } catch(Exception e) {

            EIGlobal.mensajePorTrace("4: Error al abrir el Archivo: " + 1, EIGlobal.NivelLog.INFO);
        }
    }
                /*
                 * Metodo lecturaArchivo(), se utiliza para leer todos los registros
                 * de un archivo y crea una tabla de html para visualizarlos
                 */
    public String lecturaArchivo() {
        String lecturaReg = "", encabezado = "";
                /*
                String strTabla   = "";
                 */
        StringBuffer strTabla   = new StringBuffer(""); /*JMB*/
        int contador      = 0;

        EIGlobal.mensajePorTrace("\nJMB: Q9337 -> Cambiado String por StringBuffer", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Entrando a lecturaArchivo()", EIGlobal.NivelLog.INFO);
        /*JMB*/
        strTabla.append("<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
        strTabla.append("<tr><td colspan=10 class=textabref>Cat&aacute;logo de Empleados</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>No Empleado</td>");
        strTabla.append("<td class=tittabdat align=center>No Depto</td><td class=tittabdat align=center>Paterno</td>");
        strTabla.append("<td class=tittabdat align=center>Materno</td><td class=tittabdat align=center>Nombre</td>");
        strTabla.append("<td class=tittabdat align=center>R.F.C.</td><td class=tittabdat align=center>Homo</td>");
        strTabla.append("<td class=tittabdat align=center>Sexo</td><td class=tittabdat align=center>Cuenta</td></tr>");

        EIGlobal.mensajePorTrace("leyendo encabezado", EIGlobal.NivelLog.INFO);
        try {
            archivoTrabajo.seek(0);
            encabezado = archivoTrabajo.readLine();

            EIGlobal.mensajePorTrace("ENCABEZADO " + encabezado, EIGlobal.NivelLog.INFO);
        }	//lectura del encabezado
        catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivo:1 " + e.getMessage(), EIGlobal.NivelLog.INFO);
            return "Error 1: "+ e.getMessage();
        }
        if (encabezado!=null) {
            do
            {
                try {
                    posicionReg = archivoTrabajo.getFilePointer();
                    lecturaReg  = archivoTrabajo.readLine();
                    if ( lecturaReg.equals( "" ) )
                        lecturaReg = archivoTrabajo.readLine();

                } catch(Exception e) {

                    EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivo: 2" + e.getMessage(), EIGlobal.NivelLog.INFO);
                    return "Error 3: "+ e.getMessage();
                }
                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("lecturaReg ="+lecturaReg, EIGlobal.NivelLog.DEBUG);
                **/
                if (lecturaReg!=null) {
                    if (lecturaReg.substring(0,1).equals("2")) {
                        lecturaCampos(lecturaReg);
                        strTabla.append(creaRegTabla(contador));
                        contador++;
                    }
                }
            } while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
            try {
                archivoTrabajo.close();
            } catch(IOException e) {

                EIGlobal.mensajePorTrace("Error cerrando el Archivo lecturaArchivo: 3" + e.getMessage(), EIGlobal.NivelLog.INFO);
                return "Error 4: "+ e.getMessage();
            }
        }
        strTabla.append("</table>");
        return strTabla.toString();
    }
                /*
                 *	Metodo leeRegistro(long posicion) es utilizado para leer los campos de un registro
                 * utiliza los metodos posicionaRegistro y lecturaCampos
                 */
    public int leeRegistro(long posicion) {

        String lecturaReg = "";
        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Entrando a leeRegistro() ", EIGlobal.NivelLog.INFO);
        */
         posicionaRegistro(posicion);
        try {
            lecturaReg = archivoTrabajo.readLine();
        } catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo(leeRegistro): " + e, EIGlobal.NivelLog.INFO);
            return -1;
        }
        lecturaCampos(lecturaReg);
        return 1;
    }
                /*
                 *	El metodo posicionaRegistro(long posicion) se encarga de posicionar
                 *	el apuntador del archivo en la posicion indicada
                 */
    public int posicionaRegistro(long posicion) {
        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Entrando a posicionaRegistro(): "+posicion, EIGlobal.NivelLog.INFO);
        */
         try {
            archivoTrabajo.seek(posicion);
        } catch(IOException e) {

            EIGlobal.mensajePorTrace("Error posicionando el Archivo: " + e, EIGlobal.NivelLog.INFO);
            return -1;
        }
        return 1;
    }
                /*
                 *	El metodo close() se utiliza para cerrar un archivo
                 *	que pertenezca a la clase
                 */
    public void close() {
        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Entrando a close()", EIGlobal.NivelLog.INFO);
         */
        try {
            archivoTrabajo.close();
        } catch(IOException e) {

            EIGlobal.mensajePorTrace("Error posicionando el Archivo: " + e, EIGlobal.NivelLog.INFO);
        }
    }
                /*
                 *	El metodo creaRegTabla(int contador) se encarga de crear un renglon de la tabla
                 *	que se va a mostrar al usuario
                 */
    public String creaRegTabla(int contador) {
        StringBuffer sTabla = new StringBuffer("");/*JMB*/
        String cadSexo = "", cNumeroEmpleado = "&nbsp;", cNumeroDepto = "&nbsp;",  cApellidoPaterno = "&nbsp;",
                cApellidoMaterno = "&nbsp;", cNombre = "&nbsp;", cRFC = "&nbsp;", cRFCtmp = "&nbsp;", cHomoclave = "&nbsp;", cNumeroCuenta = "&nbsp;";
        String sColor = "";

        if (Sexo.trim().equals("M")) {
            cadSexo="MASCULINO";
        } else if (Sexo.trim().equals("F")) {
            cadSexo="FEMENINO";
        } else {
            cadSexo="&nbsp;";
        }
        if (!NumeroEmpleado.trim().equals("")) {
            cNumeroEmpleado=NumeroEmpleado;
        }
        if (!NumeroDepto.trim().equals("")) {
            cNumeroDepto=NumeroDepto;
        }
        if (!ApellidoPaterno.trim().equals("")) {
            cApellidoPaterno=ApellidoPaterno;
        }
        if (!ApellidoMaterno.trim().equals("")) {
            cApellidoMaterno=ApellidoMaterno;
        }
        if (!Nombre.trim().equals("")) {
            cNombre=Nombre;
        }
        if (!RFC.trim().equals("")) {
            cRFC=RFC;
        }
        if (!Homoclave.trim().equals("")) {
            cHomoclave=Homoclave;
        }
        if (!NumeroCuenta.trim().equals("")) {
            cNumeroCuenta=NumeroCuenta;
        }
        if ((contador%2)==0) {
            sColor="textabdatobs";
        } else {
            sColor="textabdatcla";
        }
                /*
                sTabla= "<tr><td class='"+ sColor +"'><input name=reg_radio type=radio value="+posicionReg+"></td><td class='"+ sColor +"'>" + cNumeroEmpleado + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cNumeroDepto + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cApellidoPaterno + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cApellidoMaterno + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cNombre + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cRFC + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cHomoclave + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cadSexo + "</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cNumeroCuenta + "</td></tr>";
                 */
        sTabla.append("<tr><td class='"+ sColor +"'>" + cNumeroEmpleado + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cNumeroDepto + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cApellidoPaterno + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cApellidoMaterno + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cNombre + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cRFC + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cHomoclave + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cadSexo + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cNumeroCuenta + "</td></tr>");
        return 	sTabla.toString();
    }
                /*
                 *	El metodo lecturaCampos(String registro) se encarga de descomponer
                 *	un registro para obtener los campos individualmente
                 */
    public void lecturaCampos(String registro) {			/* Detalle del archivo */
        /**
         *		Modificado por Miguel Cortes Arellano
         *		Fecha 12/01/2004
         *		Proposito: Lectura de 4 nuevas longitudes de campo las cuales son las siguientes
         *						-Telefono de Oficina longitud 12
         *						-Fecha Ingreso a la Oficina longitud 8
         *						-Telefono Casa longitud 12
         *						-Ingreso Mensual longitud 18
         */
        tipoRegistro      = registro.substring(0,1).trim();
        NumeroSecuencia   = registro.substring(1,6).trim();
        NumeroEmpleado    = registro.substring(6,13).trim();
        NumeroDepto       = registro.substring(13,19).trim();
        ApellidoPaterno   = registro.substring(19,49).trim();
        ApellidoMaterno   = registro.substring(49,69).trim();
        Nombre            = registro.substring(69,99).trim();
        RFC               = registro.substring(99,109).trim();
        Homoclave         = registro.substring(109,112).trim();
        Sexo              = registro.substring(112,113).trim();
        Nacionalidad      = registro.substring(113,117).trim();
        EstadoCivil       = registro.substring(117,118).trim();
        NombreCorto       = registro.substring(118,144).trim();
        Domicilio         = registro.substring(144,204).trim();
        Colonia           = registro.substring(204,234).trim();
        DelegacionoMpio   = registro.substring(234,264).trim();
        CiudadoPoblacion  = registro.substring(264,284).trim();
        CodigoPostal      = registro.substring(284,289).trim();
        Pais              = registro.substring(289,293);
        EstatusCasa       = registro.substring(293,294).trim();
        FechaResidencia   = registro.substring(294,302).trim();		//21
        //Telefono de Casa (12) y Fecha de Ingreso a la Empresa (8)
        PrefijoParTelefono= registro.substring(302,308).trim(); // agrego Eric Gomez
        ParTelefono       = registro.substring(308,316).trim();
        FechaIngreso      = registro.substring(316,324).trim();
        //************************************************

        /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        16/11/2004  jbg Se mueven las variable de la Sucursal, NumeroCuenta, NumeroTarjeta
                    que estaban lineas abajo*/

        ClaveEnvio        = registro.substring(324,325).trim();
        DomicilioOfna     = registro.substring(325,385).trim();
        ColoniaOficina    = registro.substring(385,415).trim();
        DeloMpioOficina   = registro.substring(415,445).trim();
        CdoPobOficina     = registro.substring(445,465).trim();
        CodigoPostalOfna  = registro.substring(465,470).trim();
        PaisOficina       = registro.substring(470,474);
        PrefijoTelefono   = registro.substring(474,480).trim(); // agrego Eric Gomez
        Telefono		  = registro.substring(480,488).trim();
        Extension         = registro.substring(488,493).trim(); // Extension         = registro.substring(484,489).trim();
        IngresoMensual    = registro.substring(493,511).trim(); // Ingreso Mensual Bruto sin punto decimal (18)
        NumeroCuenta      = registro.substring(511,522).trim(); // 16/11/2004 jbg
        NumeroTarjeta     = registro.substring(522,538).trim(); // 16/11/2004 jbg
        Sucursal          = registro.substring(538,542).trim(); // 16/11/2004 jbg

        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        /*
        EIGlobal.mensajePorTrace(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", EIGlobal.NivelLog.DEBUG);

        EIGlobal.mensajePorTrace(" ====> tipoRegistro      : ["+ tipoRegistro      +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> NumeroSecuencia   : ["+ NumeroSecuencia   +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> NumeroEmpleado    : ["+ NumeroEmpleado    +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> NumeroDepto       : ["+ NumeroDepto       +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> ApellidoPaterno   : ["+ ApellidoPaterno   +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> ApellidoMaterno   : ["+ ApellidoMaterno   +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Nombre            : ["+ Nombre            +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> RFC               : ["+ RFC               +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Homoclave         : ["+ Homoclave         +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Sexo              : ["+ Sexo              +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Nacionalidad      : ["+ Nacionalidad      +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> EstadoCivil       : ["+ EstadoCivil       +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> NombreCorto       : ["+ NombreCorto       +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Domicilio         : ["+ Domicilio         +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Colonia           : ["+ Colonia           +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> DelegacionoMpio   : ["+ DelegacionoMpio   +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> CiudadoPoblacion  : ["+ CiudadoPoblacion  +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> CodigoPostal      : ["+ CodigoPostal      +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Pais              : ["+ Pais              +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> EstatusCasa       : ["+ EstatusCasa       +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> FechaResidencia   : ["+ FechaResidencia   +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> PrefijoParTelefono: ["+ PrefijoParTelefono+"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> ParTelefono       : ["+ ParTelefono       +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> FechaIngreso      : ["+ FechaIngreso      +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Sucursal          : ["+ Sucursal          +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> ClaveEnvio        : ["+ ClaveEnvio        +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> DomicilioOfna     : ["+ DomicilioOfna     +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> ColoniaOficina    : ["+ ColoniaOficina    +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> DeloMpioOficina   : ["+ DeloMpioOficina   +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> CdoPobOficina     : ["+ CdoPobOficina     +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> CodigoPostalOfna  : ["+ CodigoPostalOfna  +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> PaisOficina       : ["+ PaisOficina       +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> PrefijoTelefono   : ["+ PrefijoTelefono   +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Telefono          : ["+ Telefono          +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> Extension         : ["+ Extension         +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> IngresoMensual    : ["+ IngresoMensual    +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> NumeroCuenta      : ["+ NumeroCuenta      +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" ====> NumeroTarjeta     : ["+ NumeroTarjeta     +"]", EIGlobal.NivelLog.DEBUG);

        EIGlobal.mensajePorTrace(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", EIGlobal.NivelLog.DEBUG);
         */

        //Telefono de Oficina incluyendo prefijo (12)
        /**
         * Cambio la posicion de donde se va a tomar el telefono de oficina
         * incluyendo lada, logicamente se recorren todos lo campos
         * por Eric Gomez (Sinapsis) 29 / ENE / 2004
         */

        /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo lista_empleados.
         * if (NumeroEmpleado.length()>0) lista_empleados+= NumeroEmpleado+",";
         */
        if (NumeroEmpleado.length()>0) lista_empleados.append(NumeroEmpleado+",");

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace(" - -*EMP NumeroEmpleado ="+NumeroEmpleado, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" - -*EMP lista_empleados ="+lista_empleados, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" - -*EMP IngresoMensual ="+IngresoMensual, EIGlobal.NivelLog.DEBUG);
         */

        //En este punto se obtiene los digitos decimales (2) de la cantidad y se formatea con punto decimal para ser enviado al resultado
        if (IngresoMensual.length()>0) {
                        /*
                         * Si el campo no tiene el formato EEEEEEEEEEEEEEEEDD, asignar un valor no num�rico para que falle la validaci�n y rechace el registro.
                         * Agregado por Javier D�az - 03/2004.
                         */
            if ( vCampoNumericoSinMsg( IngresoMensual ) )
                IngresoMensual = IngresoMensual.substring(0,IngresoMensual.length() -2)+"."+IngresoMensual.substring(IngresoMensual.length() -2, IngresoMensual.length());
            else
                IngresoMensual = "X";
        }
        //			{
        //	  				double ingreso=0.0d;
        //				  try{
        //				      	ingreso= new Double(IngresoMensual.substring(0,IngresoMensual.length() -2)+"."+IngresoMensual.substring(IngresoMensual.length() -2), IngresoMensual.length()).doubleValue();
        //						ingreso=ingreso/100;
        //						DecimalFormat df=new DecimalFormat("#.##");
        //						IngresoMensual=""+df.format(ingreso);
        //				  } catch(NumberFormatException e){
        //						IngresoMensual="";
        //						EIGlobal.mensajePorTrace("Error convirtiendo ingreso mensual: " + e, EIGlobal.NivelLog.INFO);
        //				  }
        //			}

        /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        16/11/2004 jbg se mueven estas lineas de c�digo hacia arriba

        // NumeroCuenta      = registro.substring(507,518).trim();
        NumeroCuenta      = registro.substring(511,522).trim();
        // NumeroTarjeta     = registro.substring(518,534).trim();
        NumeroTarjeta     = registro.substring(522,538).trim();
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace(" - -**********EMP registro.length()="+registro.length(), EIGlobal.NivelLog.DEBUG);
         */

        /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        16/11/2004 jbg se elimina esta validaci�n para el nuevo layout y se mueve la
                       linea de c�digo con la asignaci�n de la Sucursal hacia arriba.
        if(registro.length() < 541)
            Sucursal = "----";
        else
            Sucursal = registro.substring(538,542).trim();
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

        /**
         * Agrego una validacion para que si la sucursal es blancos o 00000 tome la de la empresa.
         * por Eric Gomez Arellano (Sinapsis) 24 / FEB / 2004
         */
        if (Sucursal.compareTo("----")==0){ // son los menores de 520
            /*Comentado por Isaac Galv�n La�ado para depurar logs
            EIGlobal.mensajePorTrace("Obtiene la sucursal de la EMPRESA", EIGlobal.NivelLog.INFO);
             */
        } else {
            if ( Sucursal.equals("") || Sucursal.equals("0000") )
                Sucursal = "----";
        }
        /////////////////////////////////////////////////////////////////////////

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace(" - -**********EMP SUCURSAL ="+Sucursal, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" - -*EMP FECHA INGRESO ="+FechaIngreso+" And length "+FechaIngreso.length(), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" - -*EMP TEELFONO PAR ="+ParTelefono+" And length "+ParTelefono.length(), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(" - -*EMP TEELFONO OFI ="+Telefono+" And length "+Telefono.length(), EIGlobal.NivelLog.DEBUG);
        */
         //Finaliza codigo SINAPSIS

    }
    public String reporteArchivo() {
        String lecturaReg="", encabezado="";
                /*
                String strTabla="";
                 */
        StringBuffer strTabla = new StringBuffer("");/*JMB*/
        int contador=0;

        EIGlobal.mensajePorTrace("Entrando a reporteArchivo()", EIGlobal.NivelLog.INFO);
                /*
                strTabla="<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla align=center>";
                strTabla=strTabla+"<tr><td colspan=7 align=center class=tabmovtex>Reporte Detallado de Empleados</td></tr>"+
                                                "<tr><td class=tittabdat align=center>No Empleado</td><td class=tittabdat align=center>No Depto</td>"+
                                                "<td class=tittabdat align=center>Paterno</td><td class=tittabdat align=center>Materno</td>"+
                                                "<td class=tittabdat align=center>Nombre</td><td class=tittabdat align=center>R.F.C.</td>"+
                                                "<td class=tittabdat align=center>Homo</td><td class=tittabdat align=center>Sexo</td></tr>"+
                                                "<tr><td class=tittabdat align=center>Nacionalidad</td><td class=tittabdat align=center>Edo. Civil</td>"+
                                                "<td class=tittabdat align=center>Nombre Corto</td><td class=tittabdat align=center>Domicilio</td>"+
                                                "<td class=tittabdat align=center>Colonia</td><td class=tittabdat align=center>Delegacion o Municipio</td>"+
                                                "<td class=tittabdat align=center>Ciudad o Poblacion</td><td class=tittabdat align=center>CP</td></tr>"+
                                                "<tr><td class=tittabdat align=center>Pais</td><td class=tittabdat align=center>Dom. Ofi.</td>"+
                                                "<td class=tittabdat align=center>Colonia Ofi.</td><td class=tittabdat align=center>Delegacion o Municipio Of.</td>"+
                                                "<td class=tittabdat align=center>Ciudad o Poblacion Of.</td><td class=tittabdat align=center>CP Ofi.</td>"+
                                                "<td class=tittabdat align=center>Pais Ofi.</td><td class=tittabdat align=center>&nbsp;</td></tr>";
                 */
        /*JMB*/
        strTabla.append("<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla align=center>");
        strTabla.append("<tr><td colspan=7 align=center class=tabmovtex>Reporte Detallado de Empleados</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>No Empleado</td><td class=tittabdat align=center>No Depto</td>");
        strTabla.append("<td class=tittabdat align=center>Paterno</td><td class=tittabdat align=center>Materno</td>");
        strTabla.append("<td class=tittabdat align=center>Nombre</td><td class=tittabdat align=center>R.F.C.</td>");
        strTabla.append("<td class=tittabdat align=center>Homo</td><td class=tittabdat align=center>Sexo</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>Nacionalidad</td><td class=tittabdat align=center>Edo. Civil</td>");
        strTabla.append("<td class=tittabdat align=center>Nombre Corto</td><td class=tittabdat align=center>Domicilio</td>");
        strTabla.append("<td class=tittabdat align=center>Colonia</td><td class=tittabdat align=center>Delegacion o Municipio</td>");
        strTabla.append("<td class=tittabdat align=center>Ciudad o Poblacion</td><td class=tittabdat align=center>CP</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>Pais</td><td class=tittabdat align=center>Dom. Ofi.</td>");
        strTabla.append("<td class=tittabdat align=center>Colonia Ofi.</td><td class=tittabdat align=center>Delegacion o Municipio Of.</td>");
        strTabla.append("<td class=tittabdat align=center>Ciudad o Poblacion Of.</td><td class=tittabdat align=center>CP Ofi.</td>");
        strTabla.append("<td class=tittabdat align=center>Pais Ofi.</td><td class=tittabdat align=center>&nbsp;</td></tr>");

        EIGlobal.mensajePorTrace("leyendo encabezado", EIGlobal.NivelLog.INFO);
        try {
            archivoTrabajo.seek(0);
            encabezado=archivoTrabajo.readLine();
        }	//lectura del encabezado
        catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
            return "Error 1: "+ e.getMessage();
        }
        if (encabezado!=null) {
            do
            {
                try {
                    posicionReg=archivoTrabajo.getFilePointer();
                    lecturaReg=archivoTrabajo.readLine();
                    if ( lecturaReg.equals( "" ) )
                        lecturaReg = archivoTrabajo.readLine();

                } catch(Exception e) {

                    EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
                    return "Error 3: "+ e.getMessage();
                }

                //EIGlobal.mensajePorTrace("lecturaReg ="+lecturaReg, EIGlobal.NivelLog.DEBUG);
                if (lecturaReg!=null) {
                    if (lecturaReg.substring(0,1).equals("2")) {
                        lecturaCampos(lecturaReg);
                        strTabla.append(creaRegReporte(contador));
                        contador++;
                    }
                }
            } while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
            try {
                archivoTrabajo.close();
            } catch(IOException e) {

                EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
                return "Error 4: "+ e.getMessage();
            }
        }
        strTabla.append("</table>");
        return strTabla.toString();
    }

    public String creaRegReporte(int contador) {
        StringBuffer sTabla = new StringBuffer("");
        String cadSexo="",cEstadoCivil="&nbsp;", cPais="&nbsp;", cNacionalidad="&nbsp;", cPaisOficina="&nbsp;";
        String sColor="";
        if (Sexo.trim().equals("M")) {
            cadSexo="MASCULINO";
        } else if (Sexo.trim().equals("F")) {
            cadSexo="FEMENINO";
        } else {
            cadSexo="&nbsp;";
        }
        if (EstadoCivil.trim().equals("C")) {
            cEstadoCivil="CASADO";
        }
        if (EstadoCivil.trim().equals("D")) {
            cEstadoCivil="DIVORCIADO";
        }
        if (EstadoCivil.trim().equals("S")) {
            cEstadoCivil="SOLTERO";
        }
        if (EstadoCivil.trim().equals("U")) {
            cEstadoCivil="UNION LIBRE";
        }
        if (EstadoCivil.trim().equals("V")) {
            cEstadoCivil="VIUDO";
        }
        if (Nacionalidad.trim().equals("CANA")) {
            cNacionalidad="CANADIENSE";
        }
        if (Nacionalidad.trim().equals("ESPA")) {
            cNacionalidad="ESPA�OLA";
        }
        if (Nacionalidad.trim().equals("USA")) {
            cNacionalidad="ESTADOUNIDENSE";
        }
        if (Nacionalidad.trim().equals("FRA")) {
            cNacionalidad="FRANCESA";
        }
        if (Nacionalidad.trim().equals("MEXI")) {
            cNacionalidad="MEXICANA";
        }
        if (Pais.trim().equals("CANA")) {
            cPais="CANADA";
        }
        if (Pais.trim().equals("ESPA")) {
            cPais="ESPA�A";
        }
        if (Pais.trim().equals("USA")) {
            cPais="ESTADOS UNIDOS";
        }
        if (Pais.trim().equals("FRA")) {
            cPais="FRANCIA";
        }
        if (Pais.trim().equals("MEXI")) {
            cPais="MEXICO";
        }
        if (PaisOficina.trim().equals("CANA")) {
            cPaisOficina="CANADA";
        }
        if (PaisOficina.trim().equals("ESPA")) {
            cPaisOficina="ESPA�A";
        }
        if (PaisOficina.trim().equals("USA")) {
            cPaisOficina="ESTADOS UNIDOS";
        }
        if (PaisOficina.trim().equals("FRA")) {
            cPaisOficina="FRANCIA";
        }
        if (PaisOficina.trim().equals("MEXI")) {
            cPaisOficina="MEXICO";
        }
        if ((contador%2)==0) {
            sColor="textabdatobs";
        } else {
            sColor="textabdatcla";
        }
                /*
                sTabla= "<tr><td class='"+ sColor +"'>" + NumeroEmpleado + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + NumeroDepto + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + ApellidoPaterno + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + ApellidoMaterno + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + Nombre + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + RFC +"&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + Homoclave +"&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cadSexo + "</td></tr>";
                sTabla=sTabla + "<tr><td class='"+ sColor +"'>" + cNacionalidad + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cEstadoCivil + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + NombreCorto + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + Domicilio + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + Colonia + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + DelegacionoMpio + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + CiudadoPoblacion + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + CodigoPostal + "&nbsp;</td></tr>";
                sTabla=sTabla + "<tr><td class='"+ sColor +"'>" + cPais + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + DomicilioOfna + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + ColoniaOficina + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + DeloMpioOficina + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + CdoPobOficina + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + CodigoPostalOfna + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>" + cPaisOficina + "&nbsp;</td>";
                sTabla=sTabla + "<td class='"+ sColor +"'>&nbsp;</td></tr>";
                return 	sTabla;
                 */
        sTabla.append("<tr><td class='"+ sColor +"'>" + NumeroEmpleado + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + NumeroDepto + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + ApellidoPaterno + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + ApellidoMaterno + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + Nombre + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + RFC +"&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + Homoclave +"&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cadSexo + "</td></tr>");
        sTabla.append("<tr><td class='"+ sColor +"'>" + cNacionalidad + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cEstadoCivil + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + NombreCorto + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + Domicilio + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + Colonia + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + DelegacionoMpio + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + CiudadoPoblacion + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + CodigoPostal + "&nbsp;</td></tr>");
        sTabla.append("<tr><td class='"+ sColor +"'>" + cPais + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + DomicilioOfna + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + ColoniaOficina + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + DeloMpioOficina + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + CdoPobOficina + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + CodigoPostalOfna + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cPaisOficina + "&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>&nbsp;</td></tr>");
        return sTabla.toString();
    }
                /*
                 *	El metodo bajaRegistro(long posicion) se utiliza para marcar un registro
                 *	como dado de baja, utiliza la variable registroVacio para escribir
                 *	un registro vacio en lugar del registro que existia anteriormente
                 */
    public void bajaRegistro(long posicion) {
        //String registroVacio ="0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ";
        /**
         *		Modificado por Miguel Cortes Arellano
         *		Fecha: 29/12/2003
         *		Proposito: Pon un registro vacio inicializado con un 0 para substituir el registro a borrar. La longitud es de 520 caracteres
         *						ya que se adherio el campo Sucursal. Ademas el registro comienza con un 0 y se appended 519 espacios al String.
         *		Empieza codigo SINAPSIS
         **/
        //String registroVacio = rellenaCaracter("0",' ',542,true);
        String registroVacio = rellenaCaracter("0",' ',542,false); // Modificado por Javier D�az - 02/2004. Correcci�n, el 0 va adelante.

        EIGlobal.mensajePorTrace("El registro ha sido dado de baja."+registroVacio, EIGlobal.NivelLog.INFO);
        //Finaliza Codigo SINAPSIS
        posicionaRegistro(posicion);
        try {
            archivoTrabajo.writeBytes(registroVacio);
            String nextReg=rellenaCaracter(""+(tamanioArchivo()-1),'0',5,true);
            archivoTrabajo.seek(archivoTrabajo.length()-7);
            archivoTrabajo.writeBytes(nextReg);

            EIGlobal.mensajePorTrace("El registro ha sido dado de baja.", EIGlobal.NivelLog.INFO);
        } catch(Exception e) {

            EIGlobal.mensajePorTrace("Error en el procedimiento baja.", EIGlobal.NivelLog.INFO);
        }
    }
                /*
                 *	El metodo altaRegistro(String registroNuevo) se utiliza
                 *	para escribir un registro nuevo, primero ubica la posicion donde se
                 *	encuentra el registro sumario que es el ultimo en el archivo y
                 *	a partir de esa posicion escribe el nuevo registro
                 *	y obtiene los datos de la secuencia y el total de registros
                 *	para escribir el registro sumario
                 */
    public void altaRegistro(String registroNuevo) {
        try {
            String nextSec=rellenaCaracter(""+(secuenciaArchivo()+1),'0',5,true);
            String nextReg=rellenaCaracter(""+(tamanioArchivo()+1),'0',5,true);

            EIGlobal.mensajePorTrace("Longitud del registro nuevo:"+registroNuevo.length(), EIGlobal.NivelLog.INFO);
            archivoTrabajo.seek(archivoTrabajo.length()-13);
            archivoTrabajo.writeBytes(registroNuevo+"\r\n"+ "3" + nextSec + nextReg +"\r\n"); //Escribe el registro nuevo

            EIGlobal.mensajePorTrace("El registro ha sido dado de alta.", EIGlobal.NivelLog.INFO);
        } catch(IOException e) {

            if (Global.NIVEL_TRACE>0) {

                EIGlobal.mensajePorTrace("Error en el procedimiento alta. " + e, EIGlobal.NivelLog.INFO);
            }

        }
    }
                /*
                 *	El metodo modificaRegistro(long posicion,String registroModificado) recibe la posicion
                 *	del registro a modificar y la cadena que contiene el registro modificado
                 *	Escribe el registro adelante de la posicion donde se encuentra el numero
                 * de secuencia y escribe el registro modificado
                 */
    public void modificaRegistro(long posicion,String registroModificado) {
        try {
            archivoTrabajo.seek(posicion+6);
            archivoTrabajo.writeBytes(registroModificado);

            EIGlobal.mensajePorTrace("El registro ha sido modificado.", EIGlobal.NivelLog.INFO);
        } catch(Exception e) {

            EIGlobal.mensajePorTrace("Error en el procedimiento modificacion.", EIGlobal.NivelLog.INFO);
        }
    }
                /*
                 *	El metodo secuenciaArchivo() obtiene el ultimo numero de secuencia del archivo
                 *	y lo regresa como un numero entero
                 */
    public int secuenciaArchivo() {
        String secuencia="";
        int numSec=0;
        try {
            archivoTrabajo.seek(archivoTrabajo.length()-12);
            secuencia=archivoTrabajo.readLine();
            secuencia=secuencia.substring(0,5);
            try {
                numSec=Integer.parseInt(secuencia.trim());
            } catch(Exception e) {

                EIGlobal.mensajePorTrace("Error en el procedimiento secuenciaArchivo().", EIGlobal.NivelLog.INFO);
                return -1;
            }
        } catch(IOException e) {

            if (Global.NIVEL_TRACE>0) {
                EIGlobal.mensajePorTrace("Error en el procedimiento secuenciaArchivo().", EIGlobal.NivelLog.INFO);
            }

        }
        return 	numSec;
    }
                /*
                 *	El metodo tamanioArchivo() obtiene el campo del registro sumario
                 *	donde se encuentra el total de registros que contiene el archivo
                 */
    public int tamanioArchivo() {
        String totalRegistros="";
        String tipoReg="";
        int numReg=0;
        try {

            EIGlobal.mensajePorTrace("LA LONGITUD DEL ARCHIVO DE TRABAJO: (" +archivoTrabajo.length()+")" , EIGlobal.NivelLog.INFO);
            archivoTrabajo.seek(17);
            tipoReg=archivoTrabajo.readLine();

            EIGlobal.mensajePorTrace("Tamanio Archive: (" +tipoReg.length() +")" , EIGlobal.NivelLog.INFO);
            //EIGlobal.mensajePorTrace("FIRST LINE: (" +archivoTrabajo.readLine()+")" , EIGlobal.NivelLog.INFO);
            //if (tipoReg.length() == 517){
            if (tipoReg.length() == 516 || tipoReg.length() == 542){ // Modificado por Javier D�az - 04/2004. Adecuaci�n para nuevos layouts.
                archivoTrabajo.seek(archivoTrabajo.length()-7);
                totalRegistros=archivoTrabajo.readLine();
            }else {
                archivoTrabajo.seek(archivoTrabajo.length()-5);
                totalRegistros=archivoTrabajo.readLine();
            }

            try {
                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("Se intenta convertir: (" +totalRegistros+")" , EIGlobal.NivelLog.INFO);
                 */
                numReg=Integer.parseInt(totalRegistros.trim());
            } catch(Exception e) {

                EIGlobal.mensajePorTrace("Error en el procedimiento tamanioArchivo() 1: " + e , EIGlobal.NivelLog.INFO);
                return -1;
            }
        } catch(IOException e) {

            if (Global.NIVEL_TRACE>0) {
                EIGlobal.mensajePorTrace("Error en el procedimiento tamanioArchivo() : " + e , EIGlobal.NivelLog.INFO);
            }

        }
        return 	numReg;
    }
                /*
                 *	El metodo envioArchivo(String trama_inicio) se encarga de leer
                 *	el contenido de todo el archivo y devuelve un arreglo con las tramas que se
                 *	van a enviar al servicio de tuxedo
                 */
    public String[] envioArchivo(String trama_inicio) {
        String linea="", encabezado="";
        String []trama=new String[tamanioArchivo()];
        int contador=0;

        EIGlobal.mensajePorTrace("Entrando a envioArchivo()", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("leyendo encabezado", EIGlobal.NivelLog.INFO);
        try {
            archivoTrabajo.seek(0);
            encabezado=archivoTrabajo.readLine();
        }	//lectura del encabezado
        catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e, EIGlobal.NivelLog.INFO);
        }
        if (encabezado!=null) {
	    EIGlobal.mensajePorTrace("Generando buffer de envio a tuxedo", EIGlobal.NivelLog.DEBUG);
            do
            {
                try {
                    posicionReg=archivoTrabajo.getFilePointer();
                    linea=archivoTrabajo.readLine();
                    if ( linea.equals( "" ) )
                        linea = archivoTrabajo.readLine();
                } catch(Exception e) {
                    EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e, EIGlobal.NivelLog.INFO);
		    contador++;
                }
                /*Comentado por Isaac Galv�n La�ado para depurar logs
                 *EIGlobal.mensajePorTrace("linea ="+linea, EIGlobal.NivelLog.DEBUG);
                 */
                if (linea!=null) {
                    if (linea.substring(0,1).equals("2") ) {
                        lecturaCampos(linea);
                        trama[contador]= trama_inicio+creaTrama(contador);
                        /*Comentado por Isaac Galv�n La�ado para depurar logs
                        EIGlobal.mensajePorTrace("Valor trama[contador]: " + trama[contador], EIGlobal.NivelLog.DEBUG);
                         */
                        contador++;
                    }
                }
            } while (linea.substring(0,1).equals("2") || linea.substring(0,1).equals("0") || contador>trama.length);
            try {
                archivoTrabajo.close();
            } catch(IOException e) {

                EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e, EIGlobal.NivelLog.INFO);
            }
        }
        return trama;
    }

                /*
                 * M�todo para regresar la cadena <#REGS>@<NUM_EMPL>@<RFC>@...@<NUM_EMPL>@<RFC>@ donde
                 * <#REGS> es 5 si el archivo tiene al menos 5 registros � bien es la cantidad de registros,
                 * <NUM_EMPL> y <RFC> son los datos b�sicos de cada uno de los <#REGS> empleados para poder
                 * validar la duplicidad del archivo.
                 * Agregado por Javier D�az - 01/2004.
                 */
    public String tramaValidaDuplicado() {

        String linea    = "", encabezado = "";
        int    regs     = (tamanioArchivo() >= 5) ? 5 : tamanioArchivo();
        String trama    = regs + "@";
        int    contador = 0;

        EIGlobal.mensajePorTrace("Entrando a tramaValidaDuplicado()", EIGlobal.NivelLog.INFO);
        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("trama: " + trama, EIGlobal.NivelLog.INFO);
         */
        EIGlobal.mensajePorTrace("leyendo encabezado", EIGlobal.NivelLog.INFO);

        try	{
            archivoTrabajo.seek(0);
            encabezado = archivoTrabajo.readLine();
        }	//lectura del encabezado
        catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e, EIGlobal.NivelLog.INFO);
        }

        if (encabezado != null) {
            do {
                try	{
                    posicionReg = archivoTrabajo.getFilePointer();
                    linea       = archivoTrabajo.readLine();

                    if ( linea.equals( "" ) )
                        linea = archivoTrabajo.readLine();
                } catch(Exception e) {

                    EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e, EIGlobal.NivelLog.INFO);
		    return "Error 3: "+ e.getMessage();
                }
                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("linea ="+linea, EIGlobal.NivelLog.DEBUG);
                 */

                if (linea != null) {
                    if ( linea.substring(0,1).equals("2") )	{
                        lecturaCampos(linea);
                        trama = trama + NumeroEmpleado + "@" + RFC + "@";

                        /*Comentado por Isaac Galv�n La�ado para depurar logs
                        EIGlobal.mensajePorTrace("Valor trama: " + trama, EIGlobal.NivelLog.DEBUG);
                         */
                        contador++;
                    }
                }
            } while (contador < regs);
        }

        return trama;
    }


                /*
                 *	El metodo creaTrama(int contador) utiliza los campos del archivo para crear
                 *	la trama de un registro del archivo
                 */
    public String creaTrama(int contador) {
        String trama="";
        trama= "" + contador + "@" +
                NumeroEmpleado.trim()+"@" +
                NumeroDepto.trim()+"@" +
                ApellidoPaterno.trim()+"@" +
                ApellidoMaterno.trim()+"@" +
                Nombre.trim()+"@" +
                RFC.trim() +
                Homoclave.trim()+"@" +
                Sexo.trim()+"@" +
                Nacionalidad.trim()+"@" +
                EstadoCivil.trim()+"@" +
                NombreCorto.trim()+"@" +
                Domicilio.trim()+"@" +
                Colonia.trim()+"@" +
                DelegacionoMpio.trim()+"@" +
                CiudadoPoblacion.trim()+"@" +
                CodigoPostal.trim()+"@" +
                Pais.trim()+"@" +
                EstatusCasa.trim()+"@" +
                fecha_residencia()+"@" +
                /**
                 *		Modificado por Miguel Cortes Arellano
                 *		Fecha 12/01/2004
                 *		Proposito: Lectura de 4 nuevas longitudes de campo las cuales son las siguientes
                 *						-Telefono de Oficina longitud 12
                 *						-Fecha Ingreso a la Oficina longitud 8
                 *						-Telefono Casa longitud 12
                 *						-Ingreso Mensual longitud 18
                 */
                /**
                 *
                 *	Agrego Eric Gomez para tomar los prefijo de los telefono
                 *
                 */
                PrefijoParTelefono.trim()+"@" +
                ParTelefono.trim()+"@" +
                FechaIngreso.trim()+"@" +
                //*********************************************************************
                DomicilioOfna.trim()+"@" +
                ColoniaOficina.trim()+"@" +
                DeloMpioOficina.trim()+"@" +
                CdoPobOficina.trim()+"@" +
                CodigoPostalOfna.trim()+"@" +
                PaisOficina.trim()+"@" +
                ClaveEnvio.trim()+"@" +
                PrefijoTelefono.trim()+"@" +
                Telefono.trim()+"@" +
                Extension.trim()+"@" +
                IngresoMensual.trim()+"@" +
                NumeroCuenta.trim()+"@" +
                NumeroTarjeta.trim()+"@" ;

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Valor sucursal: " + Sucursal, EIGlobal.NivelLog.DEBUG);
         */
        if(!Sucursal.trim().equals("----"))
            trama += Sucursal.trim()+"@\n";
        else
            trama += "----@\n";

        return 	trama;
        //Finaliza codigo SINAPSIS
    }
    public String fecha_residencia(){
        if (FechaResidencia.trim().length()>0) return FechaResidencia.substring(0,2)+ FechaResidencia.substring(2,4)+
                FechaResidencia.substring(4);
        else return "";
    }
    ////////////////////////////////////////// metodos para archivo de salida de empleados /////////////////////////////////
                /*
                 *	El metodo lecturaArchivoSalida() se encarga de leer el archivo creado
                 *	con el servicio de recuperacion y crea una tabla de html para mostrar
                 *	el contenido del archivo
                 */
    public String lecturaArchivoSalida() {
        String lecturaReg="", encabezado="";
        StringBuffer strTabla = new StringBuffer("");
        int contador=0;

        EIGlobal.mensajePorTrace("Entrando a lecturaArchivoSalida()", EIGlobal.NivelLog.INFO);
        strTabla.append("\n<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
        strTabla.append("<tr><td colspan=10 class=textabref>Recuperaci&oacute;n de Empleados</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>&nbsp;</td><td class=tittabdat align=center align='center'>No Empleado</td>");
        strTabla.append("<td class=tittabdat align=center align='center'>No Depto</td><td class=tittabdat align=center align='center'>Paterno</td>");
        strTabla.append("<td class=tittabdat align=center align='center'>Materno</td><td class=tittabdat align=center align='center'>Nombre</td>");
        strTabla.append("<td class=tittabdat align=center align='center'>R.F.C.</td><td class=tittabdat align=center align='center'>Homo</td>");
        strTabla.append("<td class=tittabdat align=center align='center'>Sexo</td><td class=tittabdat align=center align='center'>Cuenta</td></tr>");

        EIGlobal.mensajePorTrace("leyendo encabezado", EIGlobal.NivelLog.INFO);
        try {
            archivoTrabajo.seek(0);
            encabezado=archivoTrabajo.readLine();	//lectura del encabezado

            EIGlobal.mensajePorTrace("encabezado ="+encabezado, EIGlobal.NivelLog.INFO);
        } catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida 1: " + e.getMessage(), EIGlobal.NivelLog.INFO);
            return "Error 1: "+ e.getMessage();
        }
        if (encabezado!=null && encabezado.substring(0,2).equals("OK")) {
            lecturaEncabezadoAS(encabezado);
            do
            {
                try {
                    posicionReg=archivoTrabajo.getFilePointer();
                    lecturaReg=archivoTrabajo.readLine();
                    //	if ( lecturaReg.equals( "" ) )
                    //		lecturaReg = archivoTrabajo.readLine();

                } catch(Exception e) {

                    EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida 2: " + e.getMessage(), EIGlobal.NivelLog.INFO);
                    return "Error 3: "+ e.getMessage();
                }

                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("lecturaReg ="+lecturaReg, EIGlobal.NivelLog.DEBUG);
                 */
                if (lecturaReg!=null) {
                    if(lecturaReg.trim().equals("") )
                        continue;
                    lecturaCamposAS(lecturaReg);
                    strTabla.append(creaRegAS(contador));
                    contador++;
                }
            } while (lecturaReg!=null);
            strTabla.append("</table>");
            try {
                archivoTrabajo.close();
            } catch(IOException e) {

                EIGlobal.mensajePorTrace("Error cerrando el Archivo lecturaArchivoSalida 3: " + e, EIGlobal.NivelLog.INFO);
                return "Error 4: "+ e;
            }
        } else {
            return "";
        }
        return strTabla.toString();
    }

    public String reporteArchivoSalida() {
        String lecturaReg = "", encabezado="";
        StringBuffer strTabla = new StringBuffer("");
        int contador      = 0;

        EIGlobal.mensajePorTrace("Entrando a reporteArchivoSalida()", EIGlobal.NivelLog.INFO);
        strTabla.append("<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla align=center>");
        strTabla.append("<tr><td colspan=7 align=center class=tabmovtex>Reporte Detallado de Empleados</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>No Empleado</td><td class=tittabdat align=center>No Depto</td><td class=tittabdat align=center>Paterno</td><td class=tittabdat align=center>Materno</td><td class=tittabdat align=center>Nombre</td><td class=tittabdat align=center>R.F.C.</td><td class=tittabdat align=center>Homo</td><td class=tittabdat align=center>Sexo</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>Nacionalidad</td><td class=tittabdat align=center>Edo. Civil</td><td class=tittabdat align=center>Nombre Corto</td><td class=tittabdat align=center>Domicilio</td><td class=tittabdat align=center>Colonia</td><td class=tittabdat align=center>Delegacion o Municipio</td><td class=tittabdat align=center>Ciudad o Poblacion</td><td class=tittabdat align=center>CP</td></tr>");
        strTabla.append("<tr><td class=tittabdat align=center>Pais</td><td class=tittabdat align=center>Dom. Ofi.</td><td class=tittabdat align=center>Colonia Ofi.</td><td class=tittabdat align=center>Delegacion o Municipio Of.</td><td class=tittabdat align=center>Ciudad o Poblacion Of.</td><td class=tittabdat align=center>CP Ofi.</td><td class=tittabdat align=center>Pais Ofi.</td><td class=tittabdat align=center>&nbsp;</td></tr>");

        EIGlobal.mensajePorTrace("leyendo encabezado", EIGlobal.NivelLog.INFO);
        try {
            archivoTrabajo.seek(0);
            encabezado=archivoTrabajo.readLine();	//lectura del encabezado

            EIGlobal.mensajePorTrace("encabezado ="+encabezado, EIGlobal.NivelLog.INFO);
        } catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
            return "Error 1: "+ e.getMessage();
        }
        if (encabezado!=null && encabezado.substring(0,2).equals("OK")) {
            lecturaEncabezadoAS(encabezado);
            do
            {
                try {
                    posicionReg=archivoTrabajo.getFilePointer();
                    lecturaReg=archivoTrabajo.readLine();
                    //	if ( lecturaReg.equals( "" ) )
                    //		lecturaReg = archivoTrabajo.readLine();
                } catch(Exception e) {

                    EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
                    return "Error 3: "+ e.getMessage();
                }

                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("lecturaReg ="+lecturaReg, EIGlobal.NivelLog.DEBUG);
                 */
                if (lecturaReg!=null) {
                    if(lecturaReg.trim().equals("") )
                        continue;
                    lecturaCamposAS(lecturaReg);
                    strTabla.append(creaRegReporte(contador));
                    contador++;
                }
            } while (lecturaReg!=null);
            strTabla.append("</table>");
            try {
                archivoTrabajo.close();
            } catch(IOException e) {

                EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e, EIGlobal.NivelLog.INFO);
                return "Error 4: "+ e;
            }
        } else {
            return "";
        }
        return strTabla.toString();
    }

    /*OJO-VERIFICAR-MA�ANA*/
                /*
                 *	El metodo escribeArchivoSalida() se encarga de leer el archivo creado por el servicio de recuperacion
                 *	y crea un nuevo archivo con el formato de salida establecido para el modulo de empleados
                 */
    public String escribeArchivoSalida() {
        String lecturaReg = "", encabezado="";
        //String strTabla   = "";
        int contador      = 0;
        RandomAccessFile aSalida = null;

        EIGlobal.mensajePorTrace("Entrando a escribeArchivoSalida(), creando: "+archivoSalida, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("leyendo encabezado", EIGlobal.NivelLog.INFO);
        try {
            archivoTrabajo.seek(0);
            encabezado=archivoTrabajo.readLine();	//lectura del encabezado

            EIGlobal.mensajePorTrace("encabezado ="+encabezado, EIGlobal.NivelLog.INFO);
        } catch(IOException e) {

            EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
            return "Error 1 escribeArchivoSalida: "+ e+","+archivoSalida;
        }
        if (encabezado!=null && encabezado.substring(0,2).equals("OK")) {
            lecturaEncabezadoAS(encabezado);
            try { //Si ya existe el archivo lo borra
                File archivo=new File(archivoSalida);
                if (archivo.exists()) {
                    System.out.println("Entro a borrar archivo" + archivo );
                    archivo.delete();
                }
            } catch(Exception e) {

                EIGlobal.mensajePorTrace("error creando objeto File(delete): " + e, EIGlobal.NivelLog.INFO);
            }
            try { //Creando el archivo nuevo
                //String fechaNuevo=BaseAppLogicEmp.ObtenFecha(true);

                EIGlobal.mensajePorTrace("Antes de ObtenFecha", EIGlobal.NivelLog.INFO);
                String fechaNuevo=ObtenFecha(true);
                fechaNuevo=fechaNuevo.substring(3,5)+fechaNuevo.substring(0,2)+fechaNuevo.substring(6);

                EIGlobal.mensajePorTrace("Despues de ObtenFecha", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("Creando objeto RandomAccessFile", EIGlobal.NivelLog.INFO);
                aSalida = new RandomAccessFile(archivoSalida, "rw");
                aSalida.writeBytes("100001S"+ fechaNuevo +"\r\n"); //Escribe encabezado
            } catch(IOException e) {

                EIGlobal.mensajePorTrace("Error creando archivo de Salida de empleados de nomina: escribeArchivoSalida :" + e, EIGlobal.NivelLog.INFO);
            }
            do
            {
                try {
                    //posicionReg=archivoTrabajo.getFilePointer();
                    lecturaReg=archivoTrabajo.readLine();
                    //if ( lecturaReg.equals( "" ) )
                    //	lecturaReg = archivoTrabajo.readLine();

                    /*Comentado por Isaac Galv�n La�ado para depurar logs
                    EIGlobal.mensajePorTrace("lecturaReg: >>" + lecturaReg + "<<", EIGlobal.NivelLog.DEBUG);
                     */
                } catch(Exception e) {

                    EIGlobal.mensajePorTrace("Error leyendo el Archivo: 3" + e.getMessage(), EIGlobal.NivelLog.INFO);
                    return "Error 3: "+ e.getMessage();
                }
                if (lecturaReg!=null) {
                    if(lecturaReg.trim().equals("") )
                        continue;
                    lecturaCamposAS(lecturaReg);
                    try {
                        aSalida.writeBytes("2"+rellenaCaracter(""+(contador+2),'0',5,true) +
                                rellenaCaracter(NumeroEmpleado.trim(),' ',7,true) +
                                rellenaCaracter(NumeroCuenta.trim(),' ',7,true)+"\r\n");
                    } catch (Exception e) {

                        EIGlobal.mensajePorTrace("Error escribiendo Archivo Salida: 6" + e, EIGlobal.NivelLog.INFO);
                        return "Error 6: "+ e;
                    }
                    contador++;
                }
            } while (lecturaReg!=null);
            try {
                aSalida.writeBytes("3"+ rellenaCaracter(""+(contador+2),'0',5,true) + rellenaCaracter(""+contador,'0',5,true) +"\r\n");
                aSalida.close();
            } catch (Exception e) {

                EIGlobal.mensajePorTrace("Error Archivo Salida: " + e, EIGlobal.NivelLog.INFO);
                return "Error 7: "+ e;
            }
        } else {
            return "";
        }

        EIGlobal.mensajePorTrace("Return escribeArchivoSalida():"+archivoSalida, EIGlobal.NivelLog.DEBUG);
        return archivoSalida;
    }
                /*
                 *	El metodo lecturaCamposAS(String r) recibe una cadena que representa
                 * un registro del archivo y la descompone en los campos correspondientes
                 */
    public void lecturaCamposAS(String r) {
        /* Detalle del archivo */
        NumeroEmpleado    = r.substring(0,r.indexOf(';'));
        ApellidoPaterno   = r.substring(posCar(r,';',1)+1,posCar(r,';',2));
        Nombre            = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
        RFC               = r.substring(posCar(r,';',3)+1,posCar(r,';',4));
        Sexo              = r.substring(posCar(r,';',4)+1,posCar(r,';',5));
        EstadoCivil       = r.substring(posCar(r,';',5)+1,posCar(r,';',6));
        Domicilio	      = r.substring(posCar(r,';',6)+1,posCar(r,';',7));
        Colonia		      = r.substring(posCar(r,';',7)+1,posCar(r,';',8));
        CodigoPostal	  = r.substring(posCar(r,';',8)+1,posCar(r,';',9));
        ClaveEnvio	      = r.substring(posCar(r,';',9)+1,posCar(r,';',10));
        NumeroCuenta      = r.substring(posCar(r,';',10)+1,posCar(r,';',11));
    }
                /*
                 *	El metodo lecturaEncabezadoAS(String r) lee el encabezado del archivo creado
                 *	con el servicio de salida y obtiene los datos que resumen el envio
                 */
    public void lecturaEncabezadoAS(String r) {
        /* Encabezado del archivo */
        int tot=0,acep=0,rech=0;
        asFechaTransmision= r.substring(posCar(r,';',1)+1,posCar(r,';',2));
        asTotalRegistros  = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
        asAceptados       = r.substring(posCar(r,';',3)+1,posCar(r,';',4));
        tot=Integer.parseInt(asTotalRegistros.trim());
        acep=Integer.parseInt(asAceptados.trim());
        rech=tot-acep;
        asRechazados      = ""+rech;
        asFechaActualizacion= r.substring(posCar(r,';',4)+1,posCar(r,';',5));

        EIGlobal.mensajePorTrace("asFechaTransmision="+asFechaTransmision+" ,asTotalRegistros= "+asTotalRegistros+" ,asAceptados="+asAceptados+" ,asRechazados="+asRechazados+" ,asFechaActualizacion="+asFechaActualizacion , EIGlobal.NivelLog.INFO);
    }
                /*
                 *	El metodo creaRegAS(int contador) arma un renglon de la tabla que va a presentar
                 *	el contenido del archivo de salida en pantalla
                 */
    public String creaRegAS(int contador) {
        StringBuffer sTabla = new StringBuffer("");
        String cadSexo="",cNumeroEmpleado="&nbsp;",cNumeroDepto="&nbsp;", cApellidoPaterno="&nbsp;", cApellidoMaterno="&nbsp;", cNombre="&nbsp;", cRFC="&nbsp;", cRFCtmp = "&nbsp;",cHomoclave="&nbsp;", cNumeroCuenta="&nbsp;";
        String sColor="";

        if (Sexo.trim().equals("M")) {
            cadSexo="MASCULINO";
        }
        if (Sexo.trim().equals("F")) {
            cadSexo="FEMENINO";
        }
        if (!NumeroEmpleado.trim().equals("")) {
            cNumeroEmpleado=NumeroEmpleado;
        }
        if (!ApellidoPaterno.trim().equals("")) {
            cApellidoPaterno=ApellidoPaterno;
        }
        if (!Nombre.trim().equals("")) {
            cNombre=Nombre;
        }
        if (!RFC.trim().equals("")) {
            cRFC=RFC;
        }
        if (!NumeroCuenta.trim().equals("")) {
            cNumeroCuenta=NumeroCuenta;
        }
        if ((contador%2)==0) {
            sColor="textabdatobs";
        } else {
            sColor="textabdatcla";
        }

        EIGlobal.mensajePorTrace("valor de cRFC 2>>"+ cRFC, EIGlobal.NivelLog.INFO);
        if(cRFC.length() >= 13){
            cRFCtmp = cRFC.substring(0,10);
            cHomoclave = cRFC.substring(10,13);
        }

        EIGlobal.mensajePorTrace("valor de cApellidoPaterno>>"+ cApellidoPaterno, EIGlobal.NivelLog.INFO);

        sTabla.append("<tr><td class='"+ sColor +"'>&nbsp;</td><td class='"+ sColor +"'>" + cNumeroEmpleado + "</td>");
        sTabla.append("<td class='"+ sColor +"'>&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cApellidoPaterno + "</td>");
        sTabla.append("<td class='"+ sColor +"'>&nbsp;</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cNombre + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cRFCtmp + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cHomoclave + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cadSexo + "</td>");
        sTabla.append("<td class='"+ sColor +"'>" + cNumeroCuenta + "</td></tr>\n");
        return 	sTabla.toString();
    }
                /*
                 *	El metodo posCar(String cad,char car,int cant) devuelve la posicion
                 *	de la ocurrencia cant en la cadena cad
                 */
    public int posCar(String cad,char car,int cant) {
        int result=0,pos=0;
        for (int i=0;i<cant;i++) {
            result=cad.indexOf(car,pos);
            pos=result+1;
        }
        return result;
    }
                /*
                 *	El metodo rellenaCaracter(String origen, char caracter, int cantidad, boolean izquierda)
                 *	devuelve una cadena rellenada con el caracter especificado
                 */
    public String rellenaCaracter(String origen, char caracter, int cantidad, boolean izquierda) {

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("funcion rellenaCaracter()", EIGlobal.NivelLog.INFO);
         */
        int cfin = cantidad - origen.length();
        //System.out.println("cfin="+cfin+", origen.length()="+origen.length()+", cantidad"+cantidad);
        if(origen.length() < cantidad) {
            for(int contador = 0 ; contador< cfin;contador++) {
                if(izquierda) {
                    origen = caracter + origen;
                } else {
                    origen = origen + caracter;
                }
            }
        }
        return origen;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    //									funciones para validacion
    /////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
         *	El metodo vCaracter(String campo,int nReg) valida los caracteres
         *	permitidos en el archivo
         */
    public  boolean vCaracter(String campo,int nReg) {
        boolean caracterValido=true;
        //Caracteres Validos: 32, 46, 48 To 57, 65 To 90, 97 To 122, 209, 241
        for (int i=0;i<campo.length();i++) {
            if (campo.charAt(i)!=32 && campo.charAt(i)!=46  && campo.charAt(i)!=209 && campo.charAt(i)!=241) {
                if (!(campo.charAt(i)>=48 && campo.charAt(i)<=57)) {
                    if (!(campo.charAt(i)>=65 && campo.charAt(i)<=90)) {
                        if (!(campo.charAt(i)>=97 && campo.charAt(i)<=122)) {
                            //cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Error, caracter no valido en la posicion "+i +"->"+campo.charAt(i)+", codigo="+ (int)campo.charAt(i) + numReg(nReg)+"</td></tr>\n";

                            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                              cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Error, caracter no valido, codigo="+ (int)campo.charAt(i) + numReg(nReg)+"</td></tr>\n"; // Modificado por Javier Diaz - 02/2004.  Se corrigi� el error al mostrar los caracteres inv�lidos. Ya no se muestra la posici�n del car�cter ya que ahora no coincide con el archivo original.
                            */
                             cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Error, caracter no valido, codigo="+ (int)campo.charAt(i) + numReg(nReg)+"</td></tr>\n"); // Modificado por Javier Diaz - 02/2004.  Se corrigi� el error al mostrar los caracteres inv�lidos. Ya no se muestra la posici�n del car�cter ya que ahora no coincide con el archivo original.
                            caracterValido=false;
                        }
                    }
                }
            }
        }
        return caracterValido;
    }

    //public  boolean vCaracterNumerico(String campo,int nReg)
    public  boolean vCaracterNumerico(String campo,String descCampo,int numCampo,String tipoReg,int nReg) {
        boolean caracterValido=true;
        //Caracteres Validos: 48 To 57
        for (int i=0;i<campo.length();i++) {
            if (!(campo.charAt(i)>=48 && campo.charAt(i)<=57)) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                  cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                        tipoReg + ", Error, caracter no numerico ->"+campo + "en la posicion "+ i +", codigo="+ (int)campo.charAt(i) + numReg(nReg)+"</td></tr>\n";
                */
                 cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                        tipoReg + ", Error, caracter no numerico ->"+campo + ", codigo="+ (int)campo.charAt(i) + numReg(nReg)+"</td></tr>\n"); // Modificado por Javier D�az - 02/2004. Ya no se muestra la posici�n del car�cter ya que ahora no coincide con el archivo original.
                caracterValido=false;
            }
        }
        return caracterValido;
    }

    public  boolean vCaracterNumericoDecimal(String campo,String descCampo,int numCampo,String tipoReg,int nReg) {
        boolean caracterValido=true;
        //Caracteres Validos: 48 To 57 + 46
        for (int i=0;i<campo.length();i++) {
            if (!((campo.charAt(i)>=48 && campo.charAt(i)<=57) || campo.charAt(i)==46)) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                 *       tipoReg + ", Error, caracter no numerico ->"+campo + "en la posicion "+ i +", codigo="+ (int)campo.charAt(i) + numReg(nReg)+"</td></tr>\n";
                 */
                 cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                        tipoReg + ", Error, caracter no numerico ->"+campo + ", codigo="+ (int)campo.charAt(i) + numReg(nReg)+"</td></tr>\n"); // Modificado por Javier D�az - 02/2004. Ya no se muestra la posici�n del car�cter ya que ahora no coincide con el archivo original.
                caracterValido=false;
            }
        }
        return caracterValido;
    }
        /*
         *El metodo vCampoObligatorio(String campo,String descCampo,int numCampo,String tipoReg,int nReg) checa
         *	si existe un campo obligatorio en el archivo
         */
    public  boolean vCampoObligatorio(String campo,String descCampo,int numCampo,String tipoReg,int nReg) {
        if (campo.trim().equals("")) {
            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
             *     tipoReg + ", Campo Obligatorio" + numReg(nReg)+"</td></tr>\n";
             */
            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                    tipoReg + ", Campo Obligatorio" + numReg(nReg)+"</td></tr>\n");
            return false;
        }
        return true;
    }

        /*
         *	El metodo vCampoNumericoSinMsg(String campo,String descCampo,int numCampo,String tipoReg,int nReg) se utiliza
         *	para validar que un campo sea numerico
         *   por Eric Gomez Arellano (Sinapsis) 24 / FEB / 2004
         */

    public  boolean vCampoNumericoSinMsg(String campo) {
        campo=campo.trim();
        if (convierteNum(campo)==-1){
            return false;
        }
        return true;
    }

        /*
         *	El metodo vCampoNumerico(String campo,String descCampo,int numCampo,String tipoReg,int nReg) se utiliza
         *	para validar que un campo sea numerico
         */

    public  boolean vCampoNumerico(String campo,String descCampo,int numCampo,String tipoReg,int nReg) {

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("\nJMB: Campo=" + campo, EIGlobal.NivelLog.INFO);
         */
        campo=campo.trim();
        if (convierteNum(campo)==-1) {
            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
             *tipoReg + ", No tiene formato numerico ->"+campo + numReg(nReg)+"</td></tr>\n";
             */
            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                    tipoReg + ", No tiene formato numerico" + numReg(nReg)+"</td></tr>\n"); // Modificado por Javier Diaz - 02/2004.  Se corrigi� el error al mostrar los caracteres inv�lidos.
            return false;
        }
        return true;
    }
        /*
         * El campo vCampoConsecutivo(String campo,String descCampo,int numCampo,String tipoReg,int nReg) se utiliza
         *	para validar que un numero de secuencia sea correcto segun el consecutivo
         */
    public  boolean vCampoConsecutivo(String campo,String descCampo,int numCampo,String tipoReg,int nReg) {
        int campoConsecutivo=0;
        campo=campo.trim();
        campoConsecutivo=convierteNum(campo);
        if (campoConsecutivo==-1 || campoConsecutivo!=nReg) {
            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
             *tipoReg + ", Numero de secuencia diferente de "+ nReg +" ->"+campo + numReg(nReg)+"</td></tr>\n";
             */
            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                    tipoReg + ", Numero de secuencia diferente de "+ nReg +" ->"+campo + numReg(nReg)+"</td></tr>\n");
            return false;
        }
        return true;
    }
        /*
         *	El campo vCampoNoNumerico(String campo,String descCampo,int numCampo,String tipoReg,int nReg) se utiliza
         *	para validar un campo alfabetico
         */
    public boolean vCampoNoNumerico(String campo,String descCampo,int numCampo,String tipoReg,int nReg) {
        boolean letra=true;
        campo=campo.trim();
        for (int i=0;i<campo.length();i++) {
            if (Character.isDigit(campo.charAt(i))) {
                letra=false; break;
            }
        }
        if (!letra) {
            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
             *tipoReg + ", Contiene un caracter numerico ->"+campo + numReg(nReg)+"</td></tr>\n";
             */
            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                    tipoReg + ", Contiene un caracter numerico ->"+campo + numReg(nReg)+"</td></tr>\n");
            return false;
        }
        return true;
    }


        /*
         *	El metodo vCampoLongitudSinMsg(String campo,String descCampo,int numCampo,String tipoReg,int nReg,int longitud, boolean vtrim)
         *	se utiliza para validar la longitud de un campo
         *   por Eric Gomez Arellano (Sinapsis) 24 / FEB / 2004
         */
    public  boolean vCampoLongitudSinMsg(String campo,int longitud, boolean vtrim) {
        if (vtrim) campo=campo.trim(); 		//si el ultimo parametro es verdadero hace un trim() sobre el campo
        if (campo.length() != longitud)	{
            return false;
        }
        return true;
    }


        /*
         *	El metodo vCampoLongitud(String campo,String descCampo,int numCampo,String tipoReg,int nReg,int longitud, boolean vtrim)
         *	se utiliza para validar la longitud de un campo
         */
    public  boolean vCampoLongitud(String campo,String descCampo,int numCampo,String tipoReg,int nReg,int longitud, boolean vtrim) {
        if (vtrim) campo=campo.trim(); 		//si el ultimo parametro es verdadero hace un trim() sobre el campo
        if (campo.length() != longitud) {
            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
             * tipoReg + ", La longitud es invalida" + numReg(nReg)+
             *", longitud="+ campo.length() + " ->"+ campo +".</td></tr>\n";
             */
            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo "+ numCampo +" '"+ descCampo +"' del "+
                    tipoReg + ", La longitud es invalida" + numReg(nReg)+
                    ", longitud="+ campo.length() + " ->"+ campo +".</td></tr>\n");
            return false;
        }
        return true;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    //							funciones para validacion de RFC
        /*
         *	El metodo eliminaPreposiciones(String cadena) se utiliza para eliminar las siguientes preposiciones
         *	DEL , DE , LA , LOS , LAS , Y ,	MC , MAC , VON , VAN
         */
    String eliminaPreposiciones(String cadena) {
        String resultado="", temp="";
        StringTokenizer elementos =new StringTokenizer(cadena," ");

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Ejecutando eliminaPreposiciones()", EIGlobal.NivelLog.INFO);
         */
        do
        {
            temp=elementos.nextToken();

            /*Comentado por Isaac Galv�n La�ado para depurar logs
            EIGlobal.mensajePorTrace("temp**"+ temp, EIGlobal.NivelLog.INFO);
             */
            if (!(temp.equals("DEL") || temp.equals("DE") || temp.equals("LA" ) || temp.equals("LOS") || temp.equals("LAS") || temp.equals("Y") ||
                    temp.equals("MC") || temp.equals("MAC") || temp.equals("VON") || temp.equals("VAN"))) {
                resultado+= " " +temp;
            }
        }while (elementos.hasMoreTokens());
        return resultado.trim();
    }
        /*
         *	El metodo buscaVocal(String cadena) busca la primera vocal en la cadena
         */
    String buscaVocal(String cadena) {

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Ejecutando buscaVocal()", EIGlobal.NivelLog.INFO);
         */
        for (int i=0;i<cadena.length();i++) {
            if (cadena.charAt(i)=='A' || cadena.charAt(i)=='E' ||	cadena.charAt(i)=='I' || cadena.charAt(i)=='O' ||	cadena.charAt(i)=='U') {
                return ""+cadena.charAt(i);
            }
        }
        return "";

    }

        /*
         *	El metodo letraNombre(String cadena) devuelve la primera letra del nombre
         * Si el nombre tiene mas de una palabra checa que el primer nombre no sea
         *	uno de los siguientes MARIA, MA, MA., JOSE, en este caso devuelve
         * la primera letra de la siguiente palabra
         */
    String letraNombre(String cadena) {
        String resultado="",temp="";
        StringTokenizer elementos =new StringTokenizer(cadena," ");

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Ejecutando letraNombre()", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("tokens=="+elementos.countTokens(), EIGlobal.NivelLog.INFO);
         */
        if (elementos.countTokens()==1) {
            return ""+cadena.charAt(0);
        }

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("cadena="+cadena, EIGlobal.NivelLog.INFO);
         */
        temp=elementos.nextToken();
        if (!(temp.equals("MARIA") || temp.equals("MA") || temp.equals("MA." ) || temp.equals("JOSE"))) {
            return "" +temp.charAt(0);
        }
        temp=elementos.nextToken();
        return "" +temp.charAt(0);
    }
        /*
         * El metodo frases(String f) checa que las iniciales no formen palabras altisonantes
         *	Si la cadena coincide con alguna de estas palabras, sustituye el ultimo caracter
         *	 de la cadena por una X
         */
    String frases(String f) {
         /*Comentado por Isaac Galv�n La�ado para depurar logs
          *EIGlobal.mensajePorTrace("Ejecutando frases()", EIGlobal.NivelLog.INFO);
          */

        if     (f.equals("BUEI") || f.equals("BUEY") || f.equals("CACA") || f.equals("CACO") || f.equals("CAGA") || f.equals("CAGO") ||
                f.equals("CAKA") || f.equals("CAKO") || f.equals("COGE") || f.equals("COJA") || f.equals("COJE") || f.equals("COJI") ||
                f.equals("COJO") || f.equals("CULO") || f.equals("FETO") || f.equals("GUEY") || f.equals("JOTO") || f.equals("KACA") ||
                f.equals("KACO") || f.equals("KAGA") || f.equals("KAGO") || f.equals("KOGE") || f.equals("KOJO") || f.equals("KAKA") ||
                f.equals("KULO") || f.equals("LOCA") || f.equals("LOCO") || f.equals("LOKA") || f.equals("LOKO") || f.equals("MAME") ||
                f.equals("MAMO") || f.equals("MEAR") || f.equals("MEAS") || f.equals("MEON") || f.equals("MION") || f.equals("MOCO") ||
                f.equals("MULA") || f.equals("PEDA") || f.equals("PEDO") || f.equals("PENE") || f.equals("PUTA") || f.equals("PUTO") ||
                f.equals("QULO") || f.equals("RATA") || f.equals("RUIN")) {
            return f.substring(0,3)+ "X";
        } else {
            return f;
        }
    }
        /*
         * El metodo vRFC(String paterno, String materno, String nombre, String rfc) se encarga de validar
         * el RFC utilizando el nombre, apellido paterno, apellido materno
         */
    boolean vRFC(String paterno, String materno, String nombre, String rfc) {
        String resultado = "" ;
        rfc     = rfc.trim();
        paterno = paterno.trim();
        materno = materno.trim();
        nombre  = nombre.trim();

        //EIGlobal.mensajePorTrace(paterno+" "+materno+" "+nombre, EIGlobal.NivelLog.INFO);
        if (paterno.length()==0 || nombre.length()==0 || materno.length()==0 || rfc.length()!=4) {
            return false;
        }
        paterno      = eliminaPreposiciones(paterno);
        materno      = eliminaPreposiciones(materno);
        nombre       = eliminaPreposiciones(nombre);

    /* 18/11/2004 jbg   en caso de que regresen nulos despu�s de la validaci�n
                        de preposiciones */
        if (paterno.length()==0 || materno.length()==0 || nombre.length()==0) {
            return false;
        }

        resultado    = ""+paterno.charAt(0);
        String vocal = buscaVocal(paterno.substring(1));
        if (vocal.equals("")) {
            if (paterno.length()>1) {
                resultado+=paterno.charAt(1);
            } else {
                resultado+=rfc.charAt(1);
            }
        } else {
            resultado+=vocal;
        }
        if (!(materno.length()==0)) {
            resultado+=materno.charAt(0);
        }
        resultado+=letraNombre(nombre);
        resultado=frases(resultado);

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("resultado= "+resultado, EIGlobal.NivelLog.INFO);
         */
        if (resultado.equals(rfc)) {
            return true;
        } else {
            return false;
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
         *	El metodo numReg(int numeroRegistro) devuelve una cadena
         *	con el numero de registro a validar
         */
    public  String numReg(int numeroRegistro) {
        return ", Registro No " + numeroRegistro;
    }
        /*
         *	El metodo convierteNum(String strNum) valida la conversion
         *	 de una cadena  a entero
         */
    public int convierteNum(String strNum) {
        /**
         * Agrego una validacion para la validacion del los enteros, ya que el telefono puede ser
         * mayor a 9 numeros, el el int es solo para 9 enteros de longitud por
         * Eric Gomez (Sinapsis) 28 / ENE / 2004
         */
        int cantidad=0;
        int i=0;
        String cero;

        try {
            if (strNum.length() > 9){
                /* Si la trama es mayor a 9 enteros */
                for (i = 0;i < strNum.length();i++){
                    cero = new String();
                    cero = "0";
                    cero = cero + strNum.charAt(i);

                    /*Comentado por Isaac Galv�n La�ado para depurar logs
                    EIGlobal.mensajePorTrace("convierteNum cero :"+cero, EIGlobal.NivelLog.INFO);
                     */
                    cantidad=Integer.parseInt(cero);
                }
                return cantidad;
            }else {
                cantidad=Integer.parseInt(strNum);
                return cantidad;
            }
        } catch(NumberFormatException e) {

            EIGlobal.mensajePorTrace("Error="+e, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("strNum="+strNum, EIGlobal.NivelLog.INFO);
            return -1;
        }
    }
        /*
         *	El metodo vFecha(String fecha,String formato) se utiliza para validar
         *	diferentes formatos de fecha
         */
    public  boolean vFecha(String fecha,String formato) {
        int dia=0,mes=0,aaa=0,diasMes=0;
        if (formato.equals("ddmmaaaa")) {
            try {
                dia=Integer.parseInt(fecha.substring(0,2));
                mes=Integer.parseInt(fecha.substring(2,4));
                aaa=Integer.parseInt(fecha.substring(4,8));
                if ( fecha.equals("FechaIngreso") && (aaa<1900 || aaa>2099) ) return false; // Agregado por Javier D�az - 03/2004. Se valida el rango de a�os.
            } catch(NumberFormatException e) {

                EIGlobal.mensajePorTrace("Error="+e, EIGlobal.NivelLog.INFO);
                return false;
            }
        } else if (formato.equals("mmddaaaa")) {
            try {
                mes=Integer.parseInt(fecha.substring(0,2));
                dia=Integer.parseInt(fecha.substring(2,4));
                aaa=Integer.parseInt(fecha.substring(4,8));
            } catch(NumberFormatException e) {

                EIGlobal.mensajePorTrace("Error="+e, EIGlobal.NivelLog.INFO);
                return false;
            }
        } else if (formato.equals("aammdd")) {
            try {
                aaa=Integer.parseInt(fecha.substring(0,2));
                mes=Integer.parseInt(fecha.substring(2,4));
                dia=Integer.parseInt(fecha.substring(4,6));
            } catch(NumberFormatException e) {

                EIGlobal.mensajePorTrace("Error="+e, EIGlobal.NivelLog.INFO);
                return false;
            }
        }

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Validando fecha : fecha="+fecha+", formato="+formato+", dia="+dia+", mes="+mes+", anio="+aaa , EIGlobal.NivelLog.INFO);
         */
        if (mes <1 || mes>12) {
            return false;
        }
        switch(mes) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                diasMes=31;
                break;
            case 2:
                if (aaa%4==0) {
                    diasMes=29;
                } else {
                    diasMes=28;
                }
                break;
            case 4: case 6: case 9: case 11:
                diasMes=30;
            default:
                break;
        }
        if (dia<1 || dia>diasMes) {
            return false;
        }
        return true;
    }
        /*
         *	El metodo validaEncabezado(String linea, int nReg) se utiliza para validar los campos
         *	que contiene el registro de encabezado
         */
    public void validaEncabezado(String linea, int nReg) {
        vCaracter(linea,nReg);
        if (vCampoLongitud(linea,"Registro de Encabezado",1,"Encabezado",nReg,15,false)) {
            if (vCampoObligatorio(linea.substring(1,6),"Numero de Secuencia",2,"Encabezado",nReg)) {
                if (vCampoNumerico(linea.substring(1,6),"Numero de Secuencia",2,"Encabezado",nReg)) {
                    vCampoConsecutivo(linea.substring(1,6),"Numero de Secuencia",2,"Encabezado",nReg);
                }
            }
            if (vCampoObligatorio(linea.substring(6,7),"Sentido",3,"Encabezado",nReg)) {
                if (!linea.substring(6,7).equals("E")) {
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 3 'Sentido' del Encabezado, Diferente de 'E' ->"+ linea.substring(6,7) + numReg(nReg)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 3 'Sentido' del Encabezado, Diferente de 'E' ->"+ linea.substring(6,7) + numReg(nReg)+"</td></tr>\n");
                }
            }
            if (vCampoObligatorio(linea.substring(7,15),"Fecha de Generacion",4,"Encabezado",nReg)) {
                if (!vFecha(linea.substring(7,15),"mmddaaaa")) {
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 4 'Fecha de Generacion' del Encabezado, No corresponde al formato mm/dd/aaaa ->"+ linea.substring(7,15) + numReg(nReg)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 4 'Fecha de Generacion' del Encabezado, No corresponde al formato mm/dd/aaaa ->"+ linea.substring(7,15) + numReg(nReg)+"</td></tr>\n");
                }
            }
        }
    }
        /*
         *	El metodo validaDetalle(String linea, int nReg) se utiliza para validar los campos
         *	que contienen los registros de detalle
         */
    public void validaDetalle(String linea, int nReg) {
        int num=0;
        int lenReg = 0;

        EIGlobal.mensajePorTrace("Validando"+numReg(nReg), EIGlobal.NivelLog.INFO);

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace(linea, EIGlobal.NivelLog.INFO);
         */
        vCaracter(linea,nReg);

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("Linea longitud --->>"+linea.length(), EIGlobal.NivelLog.INFO);
         */

        //if (vCampoLongitud(linea,"Registro",1,"Detalle",nReg,LEN_LINEA,false))
        //if ( linea.length() == 516 || linea.length() == 542 ) // Modificado por Javier D�az - 02/2004. La longitud de registro permitida es 516 � 542.
        //System.out.println("LGN valor de la Longitud Registro de Detalle: "+ linea.length());
        if (linea.length() == 542 ) //LGN Q6378 14-10-2004
        {
            lecturaCampos(linea);
            if (vCampoObligatorio(NumeroSecuencia,"Numero de Secuencia",2,"Detalle",nReg)) {
                if (vCampoNumerico(NumeroSecuencia,"Numero de Secuencia",2,"Detalle",nReg)) {
                    vCampoConsecutivo(NumeroSecuencia,"Numero de Secuencia",2,"Detalle",nReg);
                }
            }
            vCampoObligatorio(NumeroEmpleado,"Numero de Empleado",3,"Detalle",nReg);
            if (vCampoObligatorio(ApellidoPaterno,"Apellido Paterno",5,"Detalle",nReg)) {
                vCampoNoNumerico(ApellidoPaterno,"Apellido Paterno",5,"Detalle",nReg);
            }
            if (vCampoObligatorio(ApellidoMaterno,"Apellido Materno",6,"Detalle",nReg)) {
                vCampoNoNumerico(ApellidoMaterno,"Apellido Materno",6,"Detalle",nReg);
            }
            if (vCampoObligatorio(Nombre,"Nombre",7,"Detalle",nReg)) {
                vCampoNoNumerico(Nombre,"Nombre",7,"Detalle",nReg);
            }

            if (vCampoObligatorio(RFC,"R.F.C.",8,"Detalle",nReg)) {
                if (vCampoNoNumerico(RFC.substring(0,4),"Primeros 4 caracteres del R.F.C.",8,"Detalle",nReg)) {
                    if (vCampoLongitud(RFC,"R.F.C.",8,"Detalle",nReg,10,true)) {
                        if (!vFecha(RFC.substring(4),"aammdd")) {
                            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 8 'Fecha del R.F.C' del Detalle, No corresponde al formato aa/mm/dd ->"+ linea.substring(4) + numReg(nReg)+"</td></tr>\n";
                             */
                            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 8 'Fecha del R.F.C' del Detalle, No corresponde al formato aa/mm/dd ->"+ linea.substring(4) + numReg(nReg)+"</td></tr>\n");
                        } else if (!vRFC(ApellidoPaterno,ApellidoMaterno,Nombre,RFC.substring(0,4))) {
                            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 8 'R.F.C.' del Detalle, el calculo de los primeros 4 caracteres del R.F.C. no corresponde con el nombre->"+RFC.substring(0,4) + numReg(nReg)+"</td></tr>\n";
                             */
                            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 8 'R.F.C.' del Detalle, el calculo de los primeros 4 caracteres del R.F.C. no corresponde con el nombre->"+RFC.substring(0,4) + numReg(nReg)+"</td></tr>\n");
                        }
                    }
                }
            }

            if (!Homoclave.trim().equals("")) {
                vCampoLongitud(Homoclave,"Homoclave",9,"Detalle",nReg,3,true);

            }
            if (vCampoObligatorio(Sexo,"Sexo",10,"Detalle",nReg)) {
                if (!(Sexo.equals("F") || Sexo.equals("M"))) {
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 10 'Sexo' del Detalle, Diferente de 'F'=Femenino o 'M'=Masculino" + numReg(nReg)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 10 'Sexo' del Detalle, Diferente de 'F'=Femenino o 'M'=Masculino" + numReg(nReg)+"</td></tr>\n");
                }
            }
            if (vCampoObligatorio(Nacionalidad,"Nacionalidad",11,"Detalle",nReg)) {
                if (vCampoNoNumerico(Nacionalidad,"Nacionalidad",11,"Detalle",nReg)) {
                    //if (!(Nacionalidad.equals("CANA") || Nacionalidad.equals("ESPA") || Nacionalidad.equals("FRA ") || Nacionalidad.equals("MEXI") || Nacionalidad.equals("USA ")))
                    if (!(Nacionalidad.equals("CANA") || Nacionalidad.equals("ESPA") || Nacionalidad.equals("FRA") || Nacionalidad.equals("MEXI") || Nacionalidad.equals("USA"))) // Modificado por Javier D�az - 04/2004. Correcci�n de error.
                    {
                        /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                         *cadenaValidacion+="<tr><td><td>"+ (contadorErrores++) +"</td>Campo 11 'Nacionalidad' del Detalle no se encuentra en el catalogo ->"+ Nacionalidad + numReg(nReg)+"</td></tr>\n";
                         */
                        cadenaValidacion.append("<tr><td><td>"+ (contadorErrores++) +"</td>Campo 11 'Nacionalidad' del Detalle no se encuentra en el catalogo ->"+ Nacionalidad + numReg(nReg)+"</td></tr>\n");
                    }
                }
            }

            if (vCampoObligatorio(EstadoCivil,"Estado Civil",12,"Detalle",nReg)) {
                if (!(EstadoCivil.equals("C") || EstadoCivil.equals("S") || EstadoCivil.equals("D") || EstadoCivil.equals("V") || EstadoCivil.equals("U"))) {
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 12 'EstadoCivil' del Detalle, Diferente de 'C'=Casado, 'S'=Soltero, 'D'=Divorciado, 'V'=Viudo, 'U'=Union Libre" + numReg(nReg)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 12 'EstadoCivil' del Detalle, Diferente de 'C'=Casado, 'S'=Soltero, 'D'=Divorciado, 'V'=Viudo, 'U'=Union Libre" + numReg(nReg)+"</td></tr>\n");
                }
            }
            if (!NombreCorto.trim().equals("")) {
                vCampoNoNumerico(NombreCorto,"NombreCorto",13,"Detalle",nReg);
            }
            vCampoObligatorio(Domicilio ,"Domicilio",14,"Detalle",nReg);
            vCampoObligatorio(Colonia,"Colonia",15,"Detalle",nReg);
            if (vCampoObligatorio(CodigoPostal,"Codigo Postal",18,"Detalle",nReg)) {
                if (vCampoNumerico(CodigoPostal,"Codigo Postal",18,"Detalle",nReg)) {
                    if (vCampoLongitud(CodigoPostal,"Codigo Postal",18,"Detalle",nReg,5,true)) {
                        if (convierteNum(CodigoPostal)<1000) {
                            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 18 'Codigo Postal' invalido ->"+ CodigoPostal + numReg(nReg)+"</td></tr>\n";
                             */
                             cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 18 'Codigo Postal' invalido ->"+ CodigoPostal + numReg(nReg)+"</td></tr>\n");
                        }
                    }
                }
            }
            if (!Pais.trim().equals("")) {
                if (vCampoNoNumerico(Pais,"Pais",19,"Detalle",nReg)) {
                    if (!(Pais.equals("CANA") || Pais.equals("ESPA") || Pais.equals("FRA ") || Pais.equals("MEXI") || Pais.equals("USA "))) {
                        /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                         *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 19 'Pais' del Detalle no se encuentra en el catalogo ->"+ Pais + numReg(nReg)+"</td></tr>\n";
                         */
                        cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 19 'Pais' del Detalle no se encuentra en el catalogo ->"+ Pais + numReg(nReg)+"</td></tr>\n");
                    }
                }
            }
            if (!EstatusCasa.trim().equals("")) {
                if (!(EstatusCasa.equals("P") || EstatusCasa.equals("R"))) {
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 20 'Estatus Casa' del Detalle, Diferente de 'P'=Propia o 'R'=Rentada ->" +EstatusCasa+ numReg(nReg)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 20 'Estatus Casa' del Detalle, Diferente de 'P'=Propia o 'R'=Rentada ->" +EstatusCasa+ numReg(nReg)+"</td></tr>\n");
                }
            }
            //Christian Barrios Osornio (CBO)  Q6892 01/09/2004

            vCampoObligatorio(DomicilioOfna ,"Domicilio de la Oficina",27,"Detalle",nReg);

            //EIGlobal.mensajePorTrace("Domicilio de la Oficina= "+DomicilioOfna, EIGlobal.NivelLog.INFO);


            vCampoObligatorio(ColoniaOficina,"Colonia de la Oficina",28,"Detalle",nReg);

            //EIGlobal.mensajePorTrace("Colonia de la Oficina= "+ColoniaOficina, EIGlobal.NivelLog.INFO);


            if (vCampoObligatorio(CodigoPostalOfna,"Codigo Postal Oficina",31,"Detalle",nReg))	  //Chris---------->if (!CodigoPostalOfna.trim().equals(""))
                //Christian Barrios Osornio (CBO)  Q6892 01/09/2004

            {
                if (vCampoNumerico(CodigoPostalOfna,"Codigo Postal Oficina",31,"Detalle",nReg)) {
                    if (convierteNum(CodigoPostalOfna)<1000) {
                        /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                         *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 31 'Codigo Postal Oficina' no puede ser menor a 1000 ->"+ CodigoPostalOfna + numReg(nReg)+"</td></tr>\n";
                         */
                        cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 31 'Codigo Postal Oficina' no puede ser menor a 1000 ->"+ CodigoPostalOfna + numReg(nReg)+"</td></tr>\n");
                    }
                }
            }
            if (!FechaResidencia.trim().equals("")) {
                if (vCampoLongitud(FechaResidencia,"Fecha de Residencia",21,"Detalle",nReg,8,true)) // Agregado por Javier D�az - 03/3004. Correcci�n de error: No se validaba la longitud.
                        /*	if (!vFecha(FechaResidencia,"mmddaaaa"))
                                        {
                                        cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 21 'Fecha de Residencia' del Detalle, No corresponde al formato mm/dd/aaaa ->"+ FechaResidencia + numReg(nReg)+"</td></tr>\n";
                                    }	*/
                    // Modificado por Javier D�az - 04/2004. Se cambia el formato de la fecha de residencia a ddmmaaaa.
                    if (!vFecha(FechaResidencia,"ddmmaaaa")) {
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 21 'Fecha de Residencia' del Detalle, No corresponde al formato dd/mm/aaaa ->"+ FechaResidencia + numReg(nReg)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 21 'Fecha de Residencia' del Detalle, No corresponde al formato dd/mm/aaaa ->"+ FechaResidencia + numReg(nReg)+"</td></tr>\n");
                    }

            }

            //Se elimina la validacion para el campo clave de envio
            //if (vCampoObligatorio(ClaveEnvio,"Clave de envio",22,"Detalle",nReg))
            //	if (!(ClaveEnvio.equals("1") || ClaveEnvio.equals("2")))
            //		cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 22 'Clave Envio' del Detalle, Diferente de '1'=Casa o '2'=Oficina ->"+ ClaveEnvio + numReg(nReg)+"</td></tr>\n";

            if (!PaisOficina.trim().equals("")) {
                if (vCampoNoNumerico(PaisOficina,"Pais Oficina",32,"Detalle",nReg)) {
                    if (!(PaisOficina.equals("CANA") || PaisOficina.equals("ESPA") || PaisOficina.equals("FRA ") || PaisOficina.equals("MEXI") || PaisOficina.equals("USA "))) {
                        /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                         *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 28 'Pais Oficina' del Detalle no se encuentra en el catalogo ->'"+ PaisOficina + "' "+numReg(nReg)+"</td></tr>\n";
                         */
                        cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 28 'Pais Oficina' del Detalle no se encuentra en el catalogo ->'"+ PaisOficina + "' "+numReg(nReg)+"</td></tr>\n");
                    }
                }
            }
            // Tel�fono de Oficina
            //Christian Barrios Osornio (CBO)  Q6892 01/09/2004
            if (vCampoObligatorio(PrefijoTelefono,"Lada o Prefijo",33,"Detalle",nReg))		//Chris--------->if (!PrefijoTelefono.trim().equals(""))
                //Christian Barrios Osornio (CBO)  Q6892 01/09/2004
            {
                vCampoNumerico(PrefijoTelefono,"Lada o Prefijo",33,"Detalle",nReg);
            }


            //Christian Barrios Osornio (CBO)  Q6892 01/09/2004
            if (vCampoObligatorio(Telefono,"Telefono",34,"Detalle",nReg))		//Chris--------->if (!Telefono.trim().equals(""))
                //Christian Barrios Osornio (CBO)  Q6892 01/09/2004
            {
                vCampoNumerico(Telefono,"Telefono",34,"Detalle",nReg);
            }
            //String numCompleto=	Telefono.trim(); comento y cambio para validara la longitud del prefijo y telefono por Eric Gomez
            String numCompleto = PrefijoTelefono.trim() + Telefono.trim();
            if (numCompleto.length()>12) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campos 33 y 34 'Lada o Prefijo' y 'Telefono' del Detalle, La suma  de la longitud de estos campos no debe ser mayor a 12, miden->"+ numCompleto.length() + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campos 33 y 34 'Lada o Prefijo' y 'Telefono' del Detalle, La suma  de la longitud de estos campos no debe ser mayor a 12, miden->"+ numCompleto.length() + numReg(nReg)+"</td></tr>\n");
            }
            if (!Extension.trim().equals("")) {
                vCampoNumerico(Extension,"Extension",35,"Detalle",nReg);
            }

            // Tel�fono Particular
            if (!PrefijoParTelefono.trim().equals("")) {
                vCampoNumerico(PrefijoParTelefono,"Lada o Prefijo Particular",22,"Detalle",nReg);
            }
            if (!ParTelefono.trim().equals("")) {
                vCampoNumerico(ParTelefono,"Telefono Particular",23,"Detalle",nReg);
            }
            //numCompleto=	ParTelefono.trim();	comento y cambio para validara la longitud del prefijo y telefono por Eric Gomez
            numCompleto = PrefijoParTelefono.trim() + ParTelefono.trim();
            if (numCompleto.length()>12) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campos 22 y 23 'Lada o Prefijo Particular' y 'Telefono Particular' del Detalle, La suma  de la longitud de estos campos no debe ser mayor a 12, miden->"+ numCompleto.length() + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campos 22 y 23 'Lada o Prefijo Particular' y 'Telefono Particular' del Detalle, La suma  de la longitud de estos campos no debe ser mayor a 12, miden->"+ numCompleto.length() + numReg(nReg)+"</td></tr>\n");
            }

           /*
            * Se valida que si se informa el prefijo se informe tambi�n el n�mero y viceversa para el tel�fono de oficina.
            * Agregado por Javier D�az - 03/2004. INICIO
            */
            if (!PrefijoTelefono.trim().equals("") && Telefono.trim().equals("")) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Lada o Prefijo de Oficina' del Detalle, Si se informa el prefijo debe informarse tambien el numero" + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Lada o Prefijo de Oficina' del Detalle, Si se informa el prefijo debe informarse tambien el numero" + numReg(nReg)+"</td></tr>\n");
            }

            if (PrefijoTelefono.trim().equals("") && !Telefono.trim().equals("")) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo  'Telefono de Oficina' del Detalle, Si se informa el numero debe informarse tambien el prefijo" + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo  'Telefono de Oficina' del Detalle, Si se informa el numero debe informarse tambien el prefijo" + numReg(nReg)+"</td></tr>\n");
            }
            /* Agregado por Javier D�az - 03/2004. FIN */

            // Se valida que si se informa extensi�n, se informe tel�fono de oficina. Agregado por Javier D�az - 03/2004.
            if (!Extension.trim().equals("") && PrefijoTelefono.trim().equals("") && Telefono.trim().equals("")) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Extension' del Detalle, Si se informa la extension deben informarse tambien prefijo y numero" + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Extension' del Detalle, Si se informa la extension deben informarse tambien prefijo y numero" + numReg(nReg)+"</td></tr>\n");
            }

                   /*
                    * Se valida que si se informa el prefijo se informe tambi�n el n�mero y viceversa para el tel�fono particular.
                    * Agregado por Javier D�az - 03/2004. INICIO
                    */
            if (!PrefijoParTelefono.trim().equals("") && ParTelefono.trim().equals("")) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Lada o Prefijo Particular', Si se informa el prefijo debe informarse tambien el numero" + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Lada o Prefijo Particular', Si se informa el prefijo debe informarse tambien el numero" + numReg(nReg)+"</td></tr>\n");
            }

            if (PrefijoParTelefono.trim().equals("") && !ParTelefono.trim().equals("")) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Telefono Particular', Si se informa el numero debe informarse tambien el prefijo" + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 'Telefono Particular', Si se informa el numero debe informarse tambien el prefijo" + numReg(nReg)+"</td></tr>\n");
            }
            /* Agregado por Javier D�az - 03/2004. FIN */

            if (vCampoObligatorio(IngresoMensual,"Ingreso",32,"Detalle",nReg)) {
                if (!IngresoMensual.trim().equals("")) {
                    vCaracterNumericoDecimal(IngresoMensual,"Ingreso",32,"Detalle",nReg);
                }
            }
            if (!(NumeroCuenta.trim().equals("") || NumeroCuenta.trim().equals("0"))) {
                if (vCampoLongitud(NumeroCuenta,"Numero de Cuenta",33,"Detalle",nReg,11,true)) {
                    if (vCaracterNumerico(NumeroCuenta,"Numero de Cuenta",33,"Detalle",nReg)) {
                        //if (!BaseAppLogicEmp.validaDigitoCuenta(NumeroCuenta))
                        if (!validaDigitoCuenta(NumeroCuenta)) {
                            //modificacion para integracion

                            //EIGlobal.mensajePorTrace("BANCO "+Banco, EIGlobal.NivelLog.INFO);
                            if (Banco.equals(Global.CLAVE_SERFIN)) {
                                //if(!BaseAppLogicEmp.validaDigitoCuentaHogan(NumeroCuenta))
                                if(!validaDigitoCuentaHogan(NumeroCuenta)) {
                                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 33 'Numero de Cuenta' del Detalle, Cuenta Invalida ->"+ NumeroCuenta + numReg(nReg)+"</td></tr>\n";
                                     */
                                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 33 'Numero de Cuenta' del Detalle, Cuenta Invalida ->"+ NumeroCuenta + numReg(nReg)+"</td></tr>\n");
                                }
                            }
                        }
                    }
                }
            }
            if (!(NumeroTarjeta.trim().equals("") || NumeroTarjeta.trim().equals("0"))) {
                if (vCampoLongitud(NumeroTarjeta,"Numero de Tarjeta",34,"Detalle",nReg,16,true)) {
                    vCaracterNumerico(NumeroTarjeta,"Numero de Tarjeta",34,"Detalle",nReg);
                }
            }

            // Agregado por Javier D�az - 02/2004. Se agrega la validaci�n de la sucursal.
            //Christian Barrios Osornio (CBO)  Q6892 01/09/2004
            //if (vCampoObligatorio(Sucursal,"Sucursal",30,"Detalle",nReg))		//			if (!(Sucursal.equals("----")))
            //Christian Barrios Osornio (CBO)  Q6892 01/09/2004

            if (vCampoNumerico(Sucursal,"Sucursal",25,"Detalle",nReg)) {
                vCampoLongitud(Sucursal,"Sucursal",25,"Detalle",nReg,4,false);
            }

            if(vCampoObligatorio(FechaIngreso,"Fecha de Ingreso",24,"Detalle",nReg))
                if (vCampoLongitud(FechaIngreso,"Fecha de Ingreso",24,"Detalle",nReg,8,true))
                    if (!vFecha(FechaIngreso,"ddmmaaaa")) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 24 'Fecha de Ingreso' del Detalle, No corresponde al formato ddmmaaaa o no es una fecha valida->"+ FechaIngreso + numReg(nReg)+"</td></tr>\n";
                 */
                cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 24 'Fecha de Ingreso' del Detalle, No corresponde al formato ddmmaaaa o no es una fecha valida->"+ FechaIngreso + numReg(nReg)+"</td></tr>\n");
                    } else { // Agregado por Javier D�az - 03/2004. Se valida que la fecha de ingreso sea menor � igual a la fecha actual.
                int  dia = Integer.parseInt(FechaIngreso.substring(0,2));
                int  mes = Integer.parseInt(FechaIngreso.substring(2,4));
                int  aaa = Integer.parseInt(FechaIngreso.substring(4,8));
                java.util.Date hoy = new java.util.Date();
                String fecha_capt = aaa +
                        ((mes < 10) ? "0" : "") + mes +
                        ((dia < 10) ? "0" : "") + dia;
                String hoy_al_reves = (hoy.getYear() + 1900) +
                        ((hoy.getMonth() + 1) < 10 ? "0" : "") +
                        (hoy.getMonth() + 1) +
                        ((hoy.getDate()) < 10 ? "0" : "") + hoy.getDate();

                if (fecha_capt.compareTo(hoy_al_reves) > 0)
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 24 'Fecha de Ingreso' del Detalle, Debe ser menor o igual a la fecha de aplicacion ->"+ FechaIngreso + numReg(nReg)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 24 'Fecha de Ingreso' del Detalle, Debe ser menor o igual a la fecha de aplicacion ->"+ FechaIngreso + numReg(nReg)+"</td></tr>\n");
                    }

        }//longitud
        else {

            EIGlobal.mensajePorTrace("***LONGITUD INVALIDA", EIGlobal.NivelLog.INFO);
            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
             *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 1 'Registro de Detalle', La longitud es inv�lida " + numReg(nReg)+ ", longitud=" + linea.length() + "</td></tr>\n"; // Agregado por Javier D�az - 02/2004. Provoca el fallo de la validaci�n.
             */
            cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 1 'Registro de Detalle', La longitud es inv�lida " + numReg(nReg)+ ", longitud=" + linea.length() + "</td></tr>\n"); // Agregado por Javier D�az - 02/2004. Provoca el fallo de la validaci�n.
        }
    }
        /*
         *	El metodo validaSumario(String linea, int nReg) se utiliza para validar los campos
         *	que contiene el registro sumario
         */

    public  void validaSumario(String linea, int nReg) {
        if(linea==null) linea = "";
        vCaracter(linea,nReg);
        if (vCampoLongitud(linea,"Registro Sumario",1,"Sumario",nReg,11,false)) {
            if (vCampoObligatorio(linea.substring(1,6),"Numero de Secuencia",2,"Sumario",nReg)) {
                if (vCampoNumerico(linea.substring(1,6),"Numero de Secuencia",2,"Sumario",nReg)) {
                    vCampoConsecutivo(linea.substring(1,6),"Numero de Secuencia",2,"Sumario",nReg);
                }
            }
            if (vCampoObligatorio(linea.substring(6,11),"Total de Registros",3,"Sumario",nReg)) {
                if (vCampoNumerico(linea.substring(6,11),"Total de Registros",3,"Sumario",nReg)) {
                    int tR=0; tR=convierteNum(linea.substring(6,11).trim());
                    if (tR==-1 || tR!=(nReg-2)) {
                        /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                         *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Campo 3 'Total de Registros' del Sumario, Total de registros incorrecto ->"+
                         *           linea.substring(6,11) + ", Total= " + (nReg-2) + numReg(nReg)+"</td></tr>\n";
                         */
                        cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Campo 3 'Total de Registros' del Sumario, Total de registros incorrecto ->"+
                                linea.substring(6,11) + ", Total= " + (nReg-2) + numReg(nReg)+"</td></tr>\n");
                    } else {
                        totalRegistros=tR;
                    }
                }
            }
        }
    }
        /*
         *	el metodo validaArchivo() se utiliza para validar todos los registros del archivo
         *	encabezado, detalle y sumario
         */
    public boolean validaArchivo(HttpSession session) {
        String linea="", campo="", sumario="",ce="";
        int numeroLinea=1, cantidad=0, cont=0;
        boolean bolStop = false;

        EIGlobal.mensajePorTrace("Entrando a validaArchivo", EIGlobal.NivelLog.INFO);
        ce+="Entrando a validaArchivo";
        try {
            /*Comentado por Isaac Galv�n La�ado para depurar logs
            EIGlobal.mensajePorTrace("seek(0)", EIGlobal.NivelLog.INFO);
             */
            ce+=" seek(0)";
            archivoTrabajo.seek(0);

            EIGlobal.mensajePorTrace("readLine(), encabezado", EIGlobal.NivelLog.INFO);
            ce+=" readLine(), encabezado";
            linea = archivoTrabajo.readLine();

            //		linea = linea.substring( 0,linea.length()-1);
            validaEncabezado(linea,numeroLinea);
            numeroLinea++;
            int diffBlank = 0;
            do {
                //while (linea!=null)
                //		{
                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("CAMBIOS REV A", EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace("readLine(), detalle, noLinea="+numeroLinea, EIGlobal.NivelLog.DEBUG);
                 */
                ce+="<br>readLine(), detalle, noLinea="+numeroLinea;
                linea=archivoTrabajo.readLine();
                if(linea == null) break; //MSD

                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("Longitud en punto A: " + linea.length(), EIGlobal.NivelLog.DEBUG);
                 */

                if(linea.indexOf("----")!= -1){

                    /*Comentado por Isaac Galv�n La�ado para depurar logs
                    EIGlobal.mensajePorTrace("El valor de la linea es---> "+linea+" y la longitud es---> "+linea.length(), EIGlobal.NivelLog.INFO);
                     */
                    linea= linea.substring( 0,linea.indexOf("----") ) + "    ";
                }else{
                                /*	if(linea.length() < LEN_LINEA && linea.length()  != 11)
                                        {
                                                diffBlank = 0;
                                                diffBlank = LEN_LINEA - linea.length();
                                                for(int i = 1; i <= diffBlank; i++)
                                                        linea += " ";

                                        }
                                 */ // Comentado por Javier D�az - 02/2004. No se completa con blancos el registro.
                }

                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("Longitud en punto B: " + linea.length(), EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace("La longitud de la linea es"+linea.length(), EIGlobal.NivelLog.INFO);
                */
                if(linea==null)
                    linea = "";
                //if ( linea.equals( "" ))

                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("Longitud en punto C: " + linea.length(), EIGlobal.NivelLog.DEBUG);
                 */
                if ( linea.trim().equals( "" ) )
                    //linea = archivoTrabajo.readLine();
                    // INICIO - Modificado por Javier D�az - 04/2004. Se rechazan las l�neas en blanco.
                {
                    /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                     *cadenaValidacion+="<tr><td>"+ (contadorErrores++) +"</td><td>Error en formato de archivo: linea de registro sin informar" + numReg(numeroLinea)+"</td></tr>\n";
                     */
                    cadenaValidacion.append("<tr><td>"+ (contadorErrores++) +"</td><td>Error en formato de archivo: linea de registro sin informar" + numReg(numeroLinea)+"</td></tr>\n");
                    numeroLinea++;
                    continue;
                }
                //System.out.println("LGN valor de la Longitud Validaci�n: "+ linea.length());

                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("Longitud en punto D: " + linea.length(), EIGlobal.NivelLog.DEBUG);
                 */
                // FIN - Modificado por Javier D�az - 04/2004.
                if (linea!=null) {
                    if(linea.trim().equals("") )
                        continue;
                    //						linea=linea.substring(0,linea.length()-1);
                    if (linea.charAt(0)=='2') {
                        validaDetalle(linea,numeroLinea);
                        numeroLinea++;
                        cont++;
                        if(cont > 1000){
                             session.setAttribute("lineas",String.valueOf(numeroLinea));
                             cont = 0;
                        }
                    } else if (linea.charAt(0)=='3') {
                        sumario=linea;
                        bolStop = true;
                    }
                }
                if(linea.length()  == 11) {
                    bolStop = true;
                    session.removeAttribute("lineas");
                }

                /*Comentado por Isaac Galv�n La�ado para depurar logs
                //EIGlobal.mensajePorTrace("Longitud en punto E: " + linea.length(), EIGlobal.NivelLog.DEBUG);
                //EIGlobal.mensajePorTrace("MSD->Bolstop: " + bolStop, EIGlobal.NivelLog.INFO);
                 */

            }while (linea!=null && !bolStop);



            validaSumario(sumario,numeroLinea);

            EIGlobal.mensajePorTrace("sumario="+sumario, EIGlobal.NivelLog.INFO);

            if (cadenaValidacion.length()>0) {
                /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
                 *cadenaValidacion="<table align='center'>"+cadenaValidacion+"</table>";
                 */
                String validacion = "<table align='center'>"+cadenaValidacion.toString()+"</table>";
                cadenaValidacion = new StringBuffer("");
                cadenaValidacion.append(validacion);
                return false;
            } else {
                return true;
            }
        } catch(Exception e) {
            EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %cantidadRegArchivo()"+e.getMessage(), EIGlobal.NivelLog.INFO);

            EIGlobal.mensajePorTrace("Error validando archivo."+e, EIGlobal.NivelLog.INFO);
            /*Modificado por Isaac Galvan La�ado: Cambio de += a .append() en el atributo cadenaValidacion.
             *cadenaValidacion="<table>Error validando archivo.<br>"+e+"<br>"+ce +"</table>";
             */
            cadenaValidacion = new StringBuffer("");
            cadenaValidacion.append("<table>Error validando archivo.<br>"+e+"<br>"+ce +"</table>");
        }

        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("cadenaValidacion="+cadenaValidacion, EIGlobal.NivelLog.INFO);
         */
        return false;
    }

    public String formateaImporte(String importe){

        String importeFormateado="";
        boolean contienePunto=false;

        EIGlobal.mensajePorTrace("***ArchivoEmpleados.class &Entrando al metodo formatea importe()&", EIGlobal.NivelLog.INFO);
        int i=0;
        for (i=0; i<importe.length(); i++){
            if(importe.charAt(i)=='.'){
                contienePunto=true;
                break;
            }
        }
        if(contienePunto){
            importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
        } else
            importeFormateado=importe;

        return importeFormateado;
    }
    public String ObtenFecha( boolean formato_corto ) {
        String strFecha = "";
        java.util.Date Hoy = new java.util.Date();
        GregorianCalendar Cal = new GregorianCalendar();
        Cal.setTime(Hoy);
        int eldia;
        if(Cal.get(Calendar.DATE) <= 9){
            strFecha += "0" + Cal.get(Calendar.DATE) + "/";
        }else{
            strFecha += Cal.get(Calendar.DATE) + "/";
        }
        if (Cal.get(Calendar.MONTH)+1 <= 9){
            strFecha += "0" + (Cal.get(Calendar.MONTH)+1) + "/";
        }else{
            strFecha += (Cal.get(Calendar.MONTH)+1) + "/";
        }

        strFecha += Cal.get(Calendar.YEAR);
        if(!formato_corto) {
            strFecha = "";
            String[] dias = {"","Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"};
            String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

            strFecha = /*dias[Cal.get(Calendar.DAY_OF_WEEK)] + ", " +*/ Cal.get(Calendar.DATE) + " de " + meses[Cal.get(Calendar.MONTH)] + " de " + Cal.get(Calendar.YEAR);
        }
        return strFecha;
    }

    public boolean validaDigitoCuenta( String cuenta ) {

        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class &Entrando al metodo validaDigitoCuenta()&", EIGlobal.NivelLog.INFO);
        String cuentaCompuesta = "";
        int longitudCuenta = 11;
        int ponderado = 0;
        String ponderacion = "234567234567";
        int nCta = 0;
        int digitoPonderacion = 0;
        boolean cuentaValida = false;
        cuentaCompuesta = "21" + cuenta.trim();
        String count = cuenta.trim();
        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("***ArchivoEmpleados.class La longitud de count es : >" +count.length()+ "<" , EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El ultimo caracter de la cuenta es : >" +count.charAt( 10 )+ "<" , EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***ArchivoEmpleados.class La cuenta compuesta es : >" +cuentaCompuesta+ "<" , EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***ArchivoEmpleados.class Longitud de cuenta compuesta : (menos uno) >" +( cuentaCompuesta.length() - 1 )+ "<" , EIGlobal.NivelLog.INFO);
        */
        for ( int i = 0; i < cuentaCompuesta.length() - 1; i++ ) {
            try {
                nCta = Integer.parseInt( String.valueOf( cuentaCompuesta.charAt( i ) ) );
                digitoPonderacion = Integer.parseInt( String.valueOf( ponderacion.charAt( i ) ) );
                ponderado=ponderado + ( nCta * digitoPonderacion );

                //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class Ponderado : >" +ponderado+ "<" , EIGlobal.NivelLog.INFO);
            } catch ( NumberFormatException e ) {
                System.out.println( "Ha ocurrido un error en la conversion de numeros " + e.toString() );
            }
        }

        //Comentado por Isaac Galv�n La�ado
        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class Valor final de ponderado : >" +ponderado+ "<", EIGlobal.NivelLog.INFO);
        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El residuo de ponderado es : >" +( ponderado % longitudCuenta )+ "<" , EIGlobal.NivelLog.INFO);

        switch( ponderado%longitudCuenta ) {
            case 0:
                if ( cuentaCompuesta.endsWith( "0" ) ) {
                    cuentaValida = true;
                    break;
                } else {
                    cuentaValida = false;
                    break;
                }
            case 1:
                cuentaValida = false;
                break;
            default:
                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El residuo no es 0 ni 1", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El valor que se comparara es: " + ( longitudCuenta - ( ponderado % longitudCuenta ) ), EIGlobal.NivelLog.INFO);
                 */
                if ( ( longitudCuenta - ( ponderado % longitudCuenta ) ) == Integer.parseInt( String.valueOf( count.charAt( 10 ) ) ) ) {
                    cuentaValida = true;
                    break;
                } else {
                    cuentaValida = false;
                    break;
                }
        }

        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El metodo validaDigitoCuenta() es : >" +cuentaValida+ "<" , EIGlobal.NivelLog.INFO);
        return cuentaValida;
    }//fin del metodo para verificar que el digito verificador de las cuentas sea el correcto
    /////////////////////////////////////////////
    //modificacion para integracion
    /////////////////////////////////////////////
    public boolean validaDigitoCuentaHogan( String cuenta ) {


        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class Entrando al metodo validaDigitoCuentaHogan()", EIGlobal.NivelLog.INFO);

        String cuentaCompuesta = "";
        int longitudCuenta = 11;
        int ponderado = 0;
        String ponderacion = "4322765432";
        int nCta = 0;
        int digitoPonderacion = 0;
        boolean cuentaValida = false;
        String count = cuenta.trim();

        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class La longitud de count es : >" +count.length()+ "<", EIGlobal.NivelLog.INFO);
        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El ultimo caracter de la cuenta es : >" +count.charAt( 10 )+ "<", EIGlobal.NivelLog.INFO);

        for ( int i = 0; i < longitudCuenta-1; i++ ) {
            try {
                nCta = Integer.parseInt( String.valueOf( cuenta.charAt( i ) ) );
                digitoPonderacion = Integer.parseInt( String.valueOf( ponderacion.charAt( i ) ) );
                ponderado+=nCta*digitoPonderacion;

                //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class ponderado : >" +ponderado+ "<" , EIGlobal.NivelLog.INFO);
            } catch( NumberFormatException e ) {

                EIGlobal.mensajePorTrace("***ArchivoEmpleados.class Ha ocurrido un error en la conversion de numeros " + e.toString() , EIGlobal.NivelLog.INFO);
            }
        }


        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class Valor final de ponderado : >" +ponderado+ "<", EIGlobal.NivelLog.INFO);
        //EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El residuo de ponderado es : >" +( ponderado % longitudCuenta )+ "<" , EIGlobal.NivelLog.INFO);

        switch( ponderado%longitudCuenta ) {
            case 0:
                if ( count.endsWith( "0" ) ) {
                    cuentaValida = true;
                    break;
                } else {
                    cuentaValida = false;
                    break;
                }
            case 1:
                cuentaValida = false;
                break;
            default:
                /*Comentado por Isaac Galv�n La�ado para depurar logs
                EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El residuo no es 0 ni 1" , EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El valor que se comparara es : >" +( longitudCuenta - ( ponderado % longitudCuenta ) ) , EIGlobal.NivelLog.INFO);
                 */
                if ( ( longitudCuenta - ( ponderado % longitudCuenta ) ) == Integer.parseInt( String.valueOf( count.charAt( 10 ) ) ) ) {
                    cuentaValida = true;
                    break;
                } else {
                    cuentaValida = false;
                    break;
                }
        }
        /*Comentado por Isaac Galv�n La�ado para depurar logs
        EIGlobal.mensajePorTrace("***ArchivoEmpleados.class El metodo validaDigitoCuentaHogan() es : >" +cuentaValida+ "<", EIGlobal.NivelLog.INFO);
         */
        return cuentaValida;
    }//fin del metodo para verificar que el digito verificador




}//fin de la clase
// ******************************************* Fin clase archivoEmpleados2 ********************************************************