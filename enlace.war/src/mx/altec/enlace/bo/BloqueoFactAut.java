/*
 * Cap�tulo X
 * Procesa el bloqueo de factores de autenticacion
 * Creaci�n: 8 de julio de 2010
 * Autor: jgavila
 */

package mx.altec.enlace.bo;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.beans.DetBajaUsuarioBean;
import mx.altec.enlace.cliente.ws.bloqueopwd.BSCHUnBlockPwd;
import mx.altec.enlace.cliente.ws.bloqueopwd.BSCHUnBlockPwdService;
import mx.altec.enlace.cliente.ws.bloqueopwd.BSCHUnBlockPwdServiceLocator;
import mx.altec.enlace.cliente.ws.bloqueotoken.CanalesWS;
import mx.altec.enlace.cliente.ws.bloqueotoken.CanalesWSService;
import mx.altec.enlace.cliente.ws.bloqueotoken.CanalesWSServiceLocator;
import mx.altec.enlace.dao.ChequeDigitalDAO;
import mx.altec.enlace.dao.DetBajaUsuarioDAO;
import mx.altec.enlace.dao.FacPerfilDao;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.gwt.adminusr.shared.Facultad;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class BloqueoFactAut {

	public static final String LANG = "ES";
    public static final String APPNAME = "ENL"; // ---> Validar
    public static final String COD_MOTIVO = "4"; //Baja / Cancelacion
    public static final String CON_BLOQUEO_EXITO = "WSEG0800"; // Bloqueo Contasena

	public void bloqueaFactores(String cliente, String contrato,
			ArrayList<Facultad> listaFacultadesEx, String perfil,
			HttpServletRequest request) {
		EIGlobal.mensajePorTrace("-----> cliente:[" + cliente +"]" +
				"\n -----> contrato:[" + contrato + "]" +
				"\n -----> listaFacultadesEx:[" + listaFacultadesEx.size()+ "]" +
				"\n -----> perfil:[" + perfil + "]", EIGlobal.NivelLog.DEBUG);

		// Verificar que no est� en otro contrato de enlace
		FacPerfilDao fpDAO = new FacPerfilDao();
		GuardaGruposPerf ggp = new GuardaGruposPerf();
		if (! fpDAO.usuarioEnOtroContrato(cliente, contrato)) {
			EIGlobal.mensajePorTrace("-----> [No en otro contrato]", EIGlobal.NivelLog.DEBUG);
			String resPwd = null;
			String[] resToken = null;
			try {
				// Verificar si tiene cheque digital
				if (! tieneChequeDigital(cliente)){
					EIGlobal.mensajePorTrace("-----> [No tiene cheque digital]", EIGlobal.NivelLog.DEBUG);

					// Procesar resultados de los bloqueos
					resPwd = bloqueaContrasena(cliente);
					EIGlobal.mensajePorTrace("-----> resPwd: ["+ resPwd + "]", EIGlobal.NivelLog.DEBUG);
					resToken = bloqueaToken(cliente);
					EIGlobal.mensajePorTrace("-----> resToken: ["+ resToken + "]", EIGlobal.NivelLog.DEBUG);

					//Borra perfilacion si el perfil no est� asignado a otro cliente
					if (!fpDAO.perfilEnOtroUsuario(perfil, cliente)) {
						EIGlobal.mensajePorTrace("-----> [No perfil en otro usuario]",
								EIGlobal.NivelLog.DEBUG);
						ggp.borraPerfilacion(listaFacultadesEx, contrato, perfil,
								cliente, "baja");
					}
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaFactores() - tieneChequeDigital " +
						"Error Controlado: [" + e + "]", EIGlobal.NivelLog.ERROR);
			}
			// Graba tabla de detalle de baja
			EIGlobal.mensajePorTrace("-----> [Graba tabla de detalle de baja]", EIGlobal.NivelLog.DEBUG);
			DetBajaUsuarioBean usuario = new DetBajaUsuarioBean();
			usuario.setContrato(contrato);
			usuario.setUsuario(cliente);
			usuario.setSerieToken(recuperaSerialNumber(request));
			usuario.setEstadoToken("ERRO");
			if (resToken!= null){
				if (resToken[0].equals("0")){
					usuario.setEstadoToken("BLOQ");
				}
			}
			usuario.setEstadoNIP("ERRO");
			if (resPwd != null) {
				if (resPwd.equals(CON_BLOQUEO_EXITO)){
					usuario.setEstadoNIP("BLOQ");
				}
			}
			usuario.setFechaBaja(new Date(new java.util.Date().getTime()));
			usuario.setEstadoOperacion("T"); // Total
			usuario.setOrigen("EN"); // EN = Enlace
			DetBajaUsuarioDAO buDAO = new DetBajaUsuarioDAO();
			buDAO.registrarBaja(usuario);
			fpDAO = null;
			ggp = null;
			buDAO = null;
		}
		EIGlobal.mensajePorTrace("-----> [Termina bloqueaFactores]", EIGlobal.NivelLog.DEBUG);
	}


	/*
	 * M�todo para invocar al WS de bloqueo de contrasena
	 * Respuesta: String
	 * CON_BLOQUEO_EXITO = "WSEG0800"
	 *
	 * WebService: http://180.176.16.49/wseg/altecmx/altecmx/BSCHBlockPwd.jws
	 */
    public String bloqueaContrasena(String usrId) {
    	String result = null;
        try {

            BSCHUnBlockPwdService service = new BSCHUnBlockPwdServiceLocator();
            BSCHUnBlockPwd pwd = service.getBSCHUnBlockPwd(new URL(Global.URL_WS_BLOQ_PWD));

            EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaContrasena() - URL: ["
					+ Global.URL_WS_BLOQ_PWD + "]", EIGlobal.NivelLog.DEBUG);

			EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaContrasena() - Parametros de Entrada: ["
					+ new String[] {APPNAME, LANG, usrId, usrId, "SCST0001"} +
					"]", EIGlobal.NivelLog.INFO);

            result = pwd.userOpPwdBlock(APPNAME, LANG, usrId, usrId, "SCST0001");

			EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaContrasena() - result: ["
					+ result + "]", EIGlobal.NivelLog.INFO);
        } catch (Exception e) {
			EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaContrasena() - " +
					"Error Controlado: [" + e + "]", EIGlobal.NivelLog.ERROR);
        }
        return result;
    }

    /*
     * M�todo para invocar al WS de bloqueo de token
     * Respuesta: String[]
     * La primera posici�n indica �xito (0) o error (1) en la operaci�n,
     * la segunda posici�n traer� el folio de bloqueo en caso de �xito o
     * descripci�n/clave de error en caso de presentarse.
     *
     * WebService: http://appsqa02.mx.bsch:60080/token/CanalesWS.jws
     */
    /**
     * En el proyecto de bloqueo de token por intentos fallidos se gener&oacute;
     * una funci&oacute;n que se encarga de esto. La presente funci&oacute;n se
     * deja como est&aacute; para ser eliminada por un proyecto que haga 
     * mantenimiento a esta funci&oacute;n o al m&oacute;dulo que la invoca.
     * 
     * @see mx.altec.enlace.bo.WSBloqueoToken
     * @param usrId C&oacute;digo de cliente al que le ser&aacute; bloqueado su
     * token.
     * @return String[] Con el resultado que es devuelto por el WebService de
     * Token Manager que es invocado. 
     */
    @Deprecated
	public String[] bloqueaToken(String usrId) {
		String[] result = null;
        try {

            CanalesWSService service = new CanalesWSServiceLocator();
            CanalesWS token = service.getCanalesWS(new URL(Global.URL_WS_BLOQ_TOKEN));
            EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaToken() - URL: ["
					+ Global.URL_WS_BLOQ_TOKEN + "]", EIGlobal.NivelLog.DEBUG);

			EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaToken() - Parametros de Entrada: ["
					+ new String[] {usrId, APPNAME, LANG, COD_MOTIVO} +
					"]", EIGlobal.NivelLog.INFO);

            result = token.bloqueo(usrId, APPNAME, LANG, COD_MOTIVO);

			EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaToken() - result: ["
					+ result + "]", EIGlobal.NivelLog.INFO);
        } catch (Exception e) {
			EIGlobal.mensajePorTrace("BloqueoFacAut - bloqueaToken() - " +
					"Error Controlado: [" + e + "]", EIGlobal.NivelLog.ERROR);
        }
        return result;
	}

	/*
	 * Metodo para validar si el cliente cuenta con cheque digital
	 */
	private boolean tieneChequeDigital(String cliente) throws Exception {
		ChequeDigitalDAO cdDAO = new ChequeDigitalDAO();
		boolean res = false;
		try {
			res = cdDAO.tieneChequeDigital(cliente);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("BloqueoFacAut - tieneChequeDigital()- Error: ["
					+ e + "]", EIGlobal.NivelLog.ERROR);
			throw new Exception (e);
		} finally {
			cdDAO = null;
		}
		return res;
	}

	private String recuperaSerialNumber(HttpServletRequest request) {
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
  		String result = "";
		  if(request.getSession().getAttribute("valOTP") != null) {
	    		Token tok = session.getToken();
					result = tok.getSerialNumber();
	    	}
		  return result;
	  }
}