/**
 * Clase para manejo de informaci�n de mancomunidad
 *
 *Modificado: 17/11/2003 por Maria Elena de la Pe�a P. Incidencia: IM227985
 */
package mx.altec.enlace.bo;

import java.util.*;
import java.net.InetAddress;

import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreTarjeta;
import mx.altec.enlace.beans.NomPreLM1D.Relacion;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * @author C109834
 *
 */
public class DatosManc implements java.io.Serializable {
	/** Color claro	 */
	public static final String COLOR_CLARO = "textabdatcla";
	/** Color osbcuro	 */
    public static final String COLOR_OBSCURO = "textabdatobs";
    /**Serial version*/
    /** PNLI operacion pago nomina en linea */
    static final String PNLI ="PNLI";
    /** serialVersionUID */
	private static final long serialVersionUID = 1L;
	/** Sua Linea de captura */
    private final String ES_PAGO_SUA_LINEA_CAPTURA = "SULC";
    /** Transferencias Interbancarias */
    private final String PAGO_TRAN_INTEBANCARIAS = "DIBT";
    /** pago micrositio */
    private final String PAGO_MICROSITIO = "PMIC";
    /** pago referenciado micrositio */
    private final String PAGO_REF_MICROSITIO = "PMRF";
	
    /** Fecha Registro */
    private String fchRegistro = "";
    /** Folio Registro */
    private String folioRegistro = "";
    /** tipo operacion */
    private String tipo_operacion = "";
    /** fecha aut */
    private String fch_autorizacion = "";
    /** fecha reg */
    private String folio_autorizacion = "";
    /** usuario */
    private String usuario1 = "";
    /** usuario2 */
    private String usuario2 = "";
    /** usuario3 */
    private String cta_origen = "";
    /** cta destino */
    private String cta_destino = "";
    /** importe */
    private String importe = "";
    
    /** estatus indicador de estatus */
    private String estatus = "";
    /** operacion prog */
    private String tipo_oper_prog = "";
    /** concepto */
    private String concepto = "";
    /** referencia */
    private String referencia = ""; //MSD 11/2005 Proyecto Ley de Transparencia
    /** formaAplica */
	private String formaAplica= "";
	/** desc formaAplica */
	private String formaAplicaDesc="&nbsp;";
	/** fecha ejecucion prog */
    private String fch_eje_prog = "";
    /** tipo origen */
    private String tipo_origen = "";
    /** tipo destino */
    private String tipo_destino = "";
    /** instruccion */
    private String instruccion = "";
    /** plazo */
    private String plazo = "";
    /** titular */
    private String titular = "";
    /** banco */
    private String banco = "";
    /** beneficiario */
    private String beneficiario = "";
    /** sucursal */
    private String sucursal = "";
    /** plaza */
    private String plaza = "";
    /** medio referencia */
    private String referencia_medio = "";
    /** mensaje error */
    private String msg_error = ""; /*lcp modificacion por incidencia IMV495833*/
    /** desc usu 1 */
	private String descUsr1 = "";
	/** desc usu 2 */
	private String descUsr2 = "";
	/** usuario3 */
	private String usr3 = "";
	/** desc  usuario3 */
	private String descUsr3 = "";
	/** usuario4 */
	private String usr4 = "";
	/** desc  usuario3 */
	private String descUsr4 = "";
	/** descripcion */
	private String descripcion = "";
	/**
	 * bandPintaNL pinta el chekbox
	 */
	private boolean bandPintaNL = false;
	
	/** Campo que contiene informacion con respecto al tipo de divisa  de una transferencia */
	private String divisaSpid = "";
	
	/** Campo que contiene informacion con respecto al rfc de una transferencia */
	private String rfcSpid = "";	


   //modificacion cliente_7To8 15/03/2004 cgc.
	/**
	 * convertidor cod clientes
	 */
    private Convertidor con = new Convertidor();

    /**
     * Constructor que recibe todos los campos del bean
     * @param fechaRegistro fecha reg
     * @param folioRegistro folio reg
     * @param tipoOperacion tipo operacion
     * @param fechaAutorizacion fecha Autorizacion
     * @param folioAutorizacion folio autorizacion
     * @param usuario1 usuario1
     * @param usuario2 usuario2
     * @param cuentaOrigen cta origen
     * @param cuentaDestino cta destino
     * @param importe importe
     * @param estatus estatus
     * @param tipoOperProg tipo operacion programada
     * @param concepto concepto
     * @param fechaEjeProg fecha ejecucion programadas
     * @param tipoOrigen tipo origen
     * @param tipoDestino tipo destino
     * @param instruccion instruccion
     * @param plazo plazo
     * @param titular titular
     * @param banco banco
     * @param beneficiario beneficiario
     * @param sucursal sucursal
     * @param plaza plaza
     * @param referenciaMedio medio de referencia
     */
    public DatosManc (String fechaRegistro, String folioRegistro, String tipoOperacion,
    String fechaAutorizacion, String folioAutorizacion, String usuario1, String usuario2,
    String cuentaOrigen, String cuentaDestino, String importe, String estatus, String tipoOperProg,
    String concepto, String fechaEjeProg, String tipoOrigen, String tipoDestino, String instruccion,
    String plazo, String titular, String banco, String beneficiario, String sucursal, String plaza,
    String referenciaMedio) {
	this.fchRegistro = fechaRegistro;
	this.folioRegistro = folioRegistro;
	this.tipo_operacion = tipoOperacion;
	this.fch_autorizacion = fechaAutorizacion;
	this.folio_autorizacion = folioAutorizacion;
	if (usuario1.length()>0){
	    this.usuario1 = con.cliente_7To8(usuario1);
	}else{
	    this.usuario1 = usuario1;
	}
	if (usuario2.length()>0){
	    this.usuario2 = con.cliente_7To8(usuario2);
	}else{
	    this.usuario2 = usuario2;
	}
	this.cta_origen = cuentaOrigen;
	this.cta_destino = cuentaDestino;
	this.importe = importe;
	this.estatus = estatus;
	this.tipo_oper_prog = tipoOperProg;
	this.concepto = concepto;
	this.fch_eje_prog = fechaEjeProg;
	this.tipo_origen = tipoOrigen;
	this.tipo_destino = tipoDestino;
	this.instruccion = instruccion;
	this.plazo = plazo;
	this.titular = titular;
	this.banco = banco;
	this.beneficiario = beneficiario;
	this.sucursal = sucursal;
	this.plaza = plaza;
	this.referencia_medio = referenciaMedio;

	if(tipoOperacion.trim().equals(PAGO_TRAN_INTEBANCARIAS))
		{
		  this.formaAplica=(instruccion==null || instruccion.trim().equals(""))?"H":instruccion;
		  this.formaAplica=(this.formaAplica.equals("2") || this.formaAplica.equals("H"))?this.formaAplica:"H";
		  this.formaAplicaDesc=(this.formaAplica.equals("H"))?"Mismo d&iacute;a":"D&iacute;a Siguiente";
		}
	if(tipoOperacion.trim().equals(ES_PAGO_SUA_LINEA_CAPTURA))
	{
		this.cta_destino = "";
	}

    }

    /** Constructor que recibe los campos del bean en una trama
     * @param trama trama
     * @param contrato contrato
     * @param datosEmpleados  datos empleado
     * @param empresaMS empresa micrositio
     */
    public DatosManc (String trama, String contrato, ArrayList datosEmpleados, String empresaMS) {
	pdTokenizer tokenizador = new pdTokenizer (trama, ";", 2);
	try {
	    this.fchRegistro = tokenizador.nextToken ();
	    this.folioRegistro  = tokenizador.nextToken ();
	    tokenizador.nextToken ();
	    this.tipo_operacion = tokenizador.nextToken ();
	    this.fch_autorizacion = tokenizador.nextToken ();
	    this.folio_autorizacion = tokenizador.nextToken ();
	    this.usuario1 = tokenizador.nextToken ();
	    this.usuario2 = tokenizador.nextToken ();
	    if (this.usuario1.length()>0){
	        this.usuario1 = con.cliente_7To8(this.usuario1);
		}
	    if (this.usuario2.length()>0){
	        this.usuario2 = con.cliente_7To8(this.usuario2);
		}
	    this.cta_origen = tokenizador.nextToken ();
	    this.cta_destino = tokenizador.nextToken ();
	    this.banco = tokenizador.nextToken ();
	    this.sucursal = tokenizador.nextToken ();
	    this.plaza = tokenizador.nextToken ();
	    this.concepto = tokenizador.nextToken ();
	    this.importe = tokenizador.nextToken ();
	    this.estatus = tokenizador.nextToken ();
	    this.tipo_origen = tokenizador.nextToken ();
	    this.tipo_destino = tokenizador.nextToken ();
	    this.plazo = tokenizador.nextToken ();
	    this.instruccion = tokenizador.nextToken ();
	    tokenizador.nextToken ();
	    this.titular = tokenizador.nextToken ();
	    this.beneficiario = tokenizador.nextToken ();
	    this.referencia_medio = tokenizador.nextToken ();
	    this.tipo_oper_prog = tokenizador.nextToken ();
	    this.fch_eje_prog = tokenizador.nextToken ();
	    this.msg_error = tokenizador.nextToken (); /*lcp modificacion por incidencia IMV495833*/
	    this.descUsr1 = tokenizador.nextToken ();
	    this.descUsr2 = tokenizador.nextToken ();
	    this.usr3 = tokenizador.nextToken ();
	    this.descUsr3 = tokenizador.nextToken ();
	    this.usr4 = tokenizador.nextToken ();
	    this.descUsr4 = tokenizador.nextToken ();
	    this.descripcion = tokenizador.nextToken ();
	    if (PAGO_MICROSITIO.equals(this.tipo_operacion) || PAGO_REF_MICROSITIO.equals(this.tipo_operacion)) {
	    	this.descripcion = empresaMS;
	    }
		if(tipo_operacion.trim().equals(PAGO_TRAN_INTEBANCARIAS))
		{
			this.formaAplica=(instruccion==null || instruccion.trim().equals(""))?"H":instruccion;
		  this.formaAplica=(this.formaAplica.equals("2") || this.formaAplica.equals("H"))?this.formaAplica:"H";
		  this.formaAplicaDesc=(this.formaAplica.equals("H"))?"Mismo d&iacute;a":"D&iacute;a Siguiente";
		}

		if(tipo_operacion.trim().equals(ES_PAGO_SUA_LINEA_CAPTURA))
		{
			this.cta_destino = "";
		}

		if(tipo_operacion.trim().equals("PNIS"))
		{
			if (datosEmpleados != null) {
				for (int i = 0; i < datosEmpleados.size(); i ++){
					Relacion relacionTE = (Relacion)datosEmpleados.get(i);
					NomPreTarjeta tarjetaRes = relacionTE.getTarjeta();
					if (tarjetaRes.getNoTarjeta().trim().equals(this.cta_destino.trim())) { //Es la misma tarjeta
						NomPreEmpleado empleadoRes = relacionTE.getEmpleado();
						this.descripcion = empleadoRes.getNombreFrm() + " " + empleadoRes.getPaternoFrm() + " " + empleadoRes.getMaternoFrm();
						break;
					}
				}
			}
			EIGlobal.mensajePorTrace ("DatosManc - " +
					"descripcion de LM1D: [" + this.descripcion + "]", EIGlobal.NivelLog.INFO);
			//npbAction = null;
		}
	} catch (Exception ex) {
		EIGlobal.mensajePorTrace ("DatosManc - Error Controlado : [" +
				ex + "]", EIGlobal.NivelLog.ERROR);
	}
    }

    /**
     * Metodo para formatear el bean en html
     * @param index index
     * @return row filas
     */
    public String getHTMLFormat (int index) {
	String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
	String row = "";
	String Estatus = getEstatus (estatus);
		String TipoOper = "";
		String Imp = "";
	java.text.NumberFormat formatoMoneda = java.text.NumberFormat.getCurrencyInstance (Locale.US);

		// Modificaci�n Paula Hern�ndez
		/*System.out.println("fch_registro ["+ fch_registro+"]");
		System.out.println("folio_registro ["+ folio_registro+"]");
		System.out.println("Description (tipo_operacion) ["+ Description (tipo_operacion)+"]");
		System.out.println("fch_autorizacion ["+ fch_autorizacion+"]");
		System.out.println("folio_autorizacion ["+ folio_autorizacion+"]");
		System.out.println("usuario1 ["+ usuario1+"]");
		System.out.println("usuario2 ["+ usuario2+"]");
		System.out.println("cta_origen ["+ cta_origen+"]");
		System.out.println("cta_destino ["+ cta_destino+"]");
		System.out.println("importe ["+ formatoMoneda.format (Double.parseDouble (importe))+"]");
		System.out.println("Estatus ["+ Estatus+"]");
		System.out.println("msg_error ["+ msg_error+"]");*/

		if(fchRegistro.equals("") || fchRegistro==null){
			fchRegistro="&nbsp;";
		}
		if(folioRegistro.equals("") || folioRegistro==null){
			folioRegistro="&nbsp;";
		}
		if(tipo_operacion.equals("") || tipo_operacion==null){
			TipoOper="&nbsp;";
		}else{
			TipoOper=Description(tipo_operacion);
		}
		if(fch_autorizacion.equals("") || fch_autorizacion==null){
			fch_autorizacion="&nbsp;";
		}
		if(folio_autorizacion.equals("") || folio_autorizacion==null){
			folio_autorizacion="&nbsp;";
		}
		if(usuario1.equals("") || usuario1==null){
			usuario1="&nbsp;";
		}else{
		        usuario1=con.cliente_7To8(usuario1);
		}
		if(usuario2.equals("") || usuario2==null){
			usuario2="&nbsp;";
		}else{
	                usuario2=con.cliente_7To8(usuario2);
		}

		if(usr3.equals("") || usr3==null){
			usr3="&nbsp;";
		}else{
			usr3 = con.cliente_7To8(usr3);
		}

		if(usr4.equals("") || usr4==null){
			usr4="&nbsp;";
		}else{
			usr4 = con.cliente_7To8(usr4);
		}

		if(cta_origen.equals("") || cta_origen==null){
			cta_origen="&nbsp;";
		}
		if(cta_destino.equals("") || cta_destino==null){
			cta_destino="&nbsp;";
		}
		if(formatoMoneda.format(Double.parseDouble(importe)).equals("") || formatoMoneda.format (Double.parseDouble (importe))==null){
			Imp="&nbsp;";
		}else{
			Imp=formatoMoneda.format(Double.parseDouble (importe));
			if (Imp.substring (0,3).equals ("MXN"))
				Imp = "$ " + Imp.substring(3, Imp.length());
		}
		if(Estatus.equals("") || Estatus==null){
			Estatus="&nbsp;";
		}
		if(msg_error.equals("") || msg_error==null){  /*lcp modificacion por incidencia IMV495833*/
			msg_error="&nbsp;";
		}
		// Termina Modificaci�n PHH

	row += "<tr>\n";
	row += "  <td align='right' class='" + color + "'>";
	if ("P".equals (estatus) || "M".equals (estatus) || "U".equals(estatus) || "V".equals(estatus)) {

		if(!PNLI.trim().equals(tipo_operacion)) {
			//row += "<input type='checkbox' name='Folio' value='" + index + "'></td>\n";
			final StringBuffer br=new StringBuffer(row);
			br.append("<input type='checkbox' name='Folio' value='");
			br.append(index);
			br.append("'></td>\n");
			row=br.toString();
		}else if(PNLI.equals(tipo_operacion.trim()) && bandPintaNL) {
			//row += "<input type='checkbox' name='Folio' value='" + index + "'></td>\n";
			final StringBuffer br=new StringBuffer(row);
			br.append("<input type='checkbox' name='Folio' value='");
			br.append(index);
			br.append("'></td>\n");
			row=br.toString();
		}else {
			//row += "&nbsp;</td>\n";
			final StringBuffer br=new StringBuffer(row);
			br.append("&nbsp;</td>\n");
			row=br.toString();
		}
	}else {
	    row += "&nbsp;</td>\n";
	}
	row += "  <td align='center' class='" + color + "'>" + fchRegistro + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + folioRegistro + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + TipoOper + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + fch_autorizacion + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + folio_autorizacion + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + usuario1 + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + descUsr1 + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + usuario2 + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + descUsr2 + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + usr3 + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + descUsr3 + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + usr4 + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + descUsr4 + "</td>\n";

	row += "  <td align='right' class='" + color + "'>" + cta_origen + "</td>\n";

	if (tipo_operacion.trim().equals("PILC") ||tipo_operacion.trim().equals(ES_PAGO_SUA_LINEA_CAPTURA) || tipo_operacion.trim().equals("OCUR")) // 08-2012  se agrega validacion para linea de captura
	  {
		 row += "<td align='right' class='" + color + "'> &nbsp; </td>\n";
		 row += "<td align='right' class='" + color + "'> &nbsp; </td>\n";
 	  }
	else {
		if (tipo_operacion.trim().equals(PAGO_MICROSITIO) || tipo_operacion.trim().equals(PAGO_REF_MICROSITIO)) {
			if (concepto.lastIndexOf('@') > concepto.indexOf('@')+1){
			  row += "  <td align='right' class='" + color + "'>" + concepto.substring(concepto.indexOf('@')+1, concepto.lastIndexOf('@')) + "</td>\n";
			} else {
				row += "  <td align='right' class='" + color + "'>" + concepto + "</td>\n";
			}
		} else {
			  row += "  <td align='right' class='" + color + "'>" + cta_destino + "</td>\n";

		}
		  row += "  <td align='right' class='" + color + "'>" + descripcion + "</td>\n";
  	}

	row += "  <td align='right' class='" + color + "'>" + Imp + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + Estatus + "</td>\n";
	row += "  <td align='center' class='" + color + "'>" + formaAplicaDesc + "</td>\n";
	row += "  <td align='left' class='" + color + "'>" + msg_error + "</td>\n";
	row += "</tr>\n";
	return row;
    }

    /** Metodo privado para obtener el estatus completo
     * @param estatus con la letra que define el estatus
     * @return String con el estatus entendible
     */
    private String getEstatus (String estatus) {
	if (estatus.trim ().equals ("P")) {
	    return "Pendiente";
	} else if (estatus.trim ().equals ("A")) {
	    return "Autorizada";
	} else if (estatus.trim ().equals ("R")) {
	    return "Rechazada";
	} else if (estatus.trim ().equals ("C")) {
	    return "Cancelada";
	} else if (estatus.trim ().equals ("E")) {
	    return "Ejecutada";
	} else if (estatus.trim ().equals ("N")) {
	    return "No Ejecutada";
	} else if (estatus.trim ().equals ("M")) {
	    return "Mancomunada";
	} else if (estatus.trim ().equals ("V")) {
	    return "Verificada";
	} else if (estatus.trim ().equals ("U")) {
	    return "Pre-Autorizada";
	} else {
	    return "";
	}
    }


    /** Metodo para verificar si la transacci&oacute;n esta pendiente
     * @return true Si esta pendiente de lo contrario false
     */
    public boolean pendiente () {
    	return ("P".equals(estatus) || "M".equals(estatus) || "U".equals(estatus) || "V".equals(estatus));
    }

    /** Metodo para generar la trama de autorizaci&oacute;n
     * @param usuario String con el usuario que va a autorizar la transacci&oacute;n
     * @param contrato String con el contrato del usuario
     * @param clavePerfil String con la clave del perfil del usuario
     * @return String con la trama de autorizaci&oacute;n
     */
    public String getTramaAutorizacion(String usuario, String contrato,	String clavePerfil) {
		cta_destino = cta_destino.trim();
		cta_origen = cta_origen.trim();

		// MPP181203 IM227985
		String fch_prog_trama = "";
		if (tipo_operacion.equals("PROG")) {
			EIGlobal.mensajePorTrace("DatosManc - " + "Fecha programada: ["+ fch_eje_prog + "]", EIGlobal.NivelLog.INFO);
			fch_prog_trama = fch_eje_prog.substring(3, 5)+ fch_eje_prog.substring(0, 2)+ fch_eje_prog.substring(8, 10);
			EIGlobal.mensajePorTrace("DatosManc - " + "Fecha trama: ["+ fch_prog_trama + "]", EIGlobal.NivelLog.INFO);
			System.out.println("fch_prog_trama: " + fch_prog_trama);
		} else {
			fch_prog_trama = fch_eje_prog;
		}

		String trama = "1EWEB|" + usuario;
		if (tipo_operacion.equals("PROG")) {
			trama += "|PROG|" + contrato + '|' + usuario + '|' + clavePerfil
					+ '|' + tipo_oper_prog + '|' + fch_prog_trama + '|'
					+ cta_origen + '|' + tipo_origen;
			if (tipo_oper_prog.equals("TRAN") || tipo_oper_prog.equals("PAGT")) {
				trama += '|' + cta_destino + '|' + tipo_destino + '|' + importe
						+ '|' + "|0" + (concepto.equals("") ? "||A|" : '|' + concepto + '|')
						+ folioRegistro + '|';
				return trama;
			} else if (tipo_oper_prog.equals("CPDI")
					|| tipo_oper_prog.equals("CPID")) {
				trama += '|' + importe + '|' + folioRegistro + '|';
				return trama;
			} else if (tipo_oper_prog.equals("CPDV")) {
				String tipo_prod = "";
				char[] indicadores = new char[2];
				int opc = Integer.parseInt(instruccion);
				if (cta_destino.startsWith("62")) {
					tipo_prod = "77";
				} else {
					if (cta_destino.startsWith("67")) {
						tipo_prod = "78";
					} else {
						tipo_prod = "37";
					}
				}
				switch (opc) {
				case 1: {
					indicadores = new char[] { 'S', 'S' };
					break;
				}
				case 2: {
					indicadores = new char[] { 'S', 'N' };
					break;
				}
				case 3: {
					indicadores = new char[] { 'N', 'N' };
					break;
				}
				}
				trama += '|' + importe + '|' + cta_destino + '|' + plazo + '|'
						+ tipo_prod + '|' + indicadores[0] + '|'
						+ indicadores[1] + '|' + folioRegistro + '|';
				return trama;
			} else {
				return null;
			}
		} else {
			if (tipo_operacion.equals("PNIS")) {
				// IOV Modificacion para mancomunidad Fase II Nomina Individual
				// (PNIS)
				trama += "|PNIS|" + contrato + '|' + usuario + '|'
						+ clavePerfil + "| |" + cta_origen + '|' + importe
						+ '|' + folioRegistro + '|';
				return trama;
			}
			if (tipo_operacion.equals(PNLI)) {
				// IOV Modificacion para mancomunidad Fase II Nomina Individual
				// (PNLI)
				trama += "|PNLI|" + contrato + '|' + usuario + '|'
						+ clavePerfil + "| |" + cta_origen + '|' + importe
						+ '|' + folioRegistro + '|';
				return trama;
			}
			if (tipo_operacion.equals("OCUR")) {
				trama += "|OCUR|" + contrato + '|' + usuario + '|'
						+ clavePerfil + '|' + contrato + '@' + cta_origen
						+ "@P@" + importe + '@' + concepto + '@' + titular
						+ '@' + cta_destino + '@' + beneficiario + "@|"
						+ folioRegistro + '|';
				return trama;
			}
			trama += '|' + tipo_operacion + '|' + contrato + '|' + usuario
					+ '|' + clavePerfil + '|' + cta_origen + '|' + tipo_origen
					+ '|';

			if (tipo_operacion.equals("PILC")
					|| tipo_operacion.equals(PAGO_REF_MICROSITIO)) {
				trama += cta_destino + '|' + tipo_destino + '|' + importe + '|'
						+ concepto + '|' + folioRegistro + '|';
				return trama;
			}

			// VSWF - JGAL - 22/09/2009 - SIPARE
			if (tipo_operacion.equals(ES_PAGO_SUA_LINEA_CAPTURA)) // SUALC
			{
				EIGlobal.mensajePorTrace("JGAL - DatosManc - arma trama",
						EIGlobal.NivelLog.DEBUG);
				BaseServlet base = new BaseServlet();
				fch_autorizacion = base.ObtenFecha(true);
				folio_autorizacion = ""
						+ base.obten_referencia_operacion(Short
								.parseShort(IEnlace.SUCURSAL_OPERANTE));
				@SuppressWarnings("unused")
				String banco = "";
				EIGlobal.mensajePorTrace(
						"JGAL - DatosManc - folio_autorizacion: ["
								+ folio_autorizacion + "]",
						EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(
						"JGAL - DatosManc - fch_autorizacion: ["
								+ fch_autorizacion + "]",
						EIGlobal.NivelLog.DEBUG);

				trama = '|' /* fecha registro */+ folioRegistro + '|'
						+ contrato + '|' + tipo_operacion + '|'
						+ fch_autorizacion + '|' + folio_autorizacion + '|'
						+ usuario + '|' + cta_origen + '|'
						+ cta_destino
						+ "||||"
						+ // Banco, plaza, sucursal
						"Pago SUA Linea de Captura" + '|' + importe + '|' + "P"
						+ '|'/* estatus */+ "|||||" + titular + '|'
						+ beneficiario + "||||";
				EIGlobal.mensajePorTrace("JGAL - DatosManc - tramaSULC: ["
						+ trama + "]", EIGlobal.NivelLog.DEBUG);
				return trama;
			}

			//******************************************************************
			// *

			if (tipo_operacion.equals("TRAN") || tipo_operacion.equals("PAGT")
					|| tipo_operacion.equals(PAGO_MICROSITIO)) {
				trama += cta_destino + '|' + tipo_destino + '|' + importe + '|'
						+ (concepto.equals("") ? "||A|" : '|' + concepto + '|')
						+ folioRegistro + '|';
				return trama;
			} else if (tipo_operacion.equals("CPDI")
					|| tipo_operacion.equals("CPID")) {
				trama += importe + '|' + folioRegistro + '|';
				return trama;
			} else if (tipo_operacion.equals("CPDV")) {
				String tipo_prod = "";
				char[] indicadores = new char[2];
				int opc = Integer.parseInt(instruccion);
				if (cta_destino.startsWith("62"))
					tipo_prod = "77";
				else if (cta_destino.startsWith("67"))
					tipo_prod = "78";
				else
					tipo_prod = "37";

				switch (opc) {
				case 1: {
					indicadores = new char[] { 'S', 'S' };
					break;
				}
				case 2: {
					indicadores = new char[] { 'S', 'N' };
					break;
				}
				case 3: {
					indicadores = new char[] { 'N', 'N' };
					break;
				}
				}
				trama += importe + '|' + cta_destino + '|' + plazo + '|'
						+ tipo_prod + '|' + indicadores[0] + '|'
						+ indicadores[1] + '|' + folioRegistro + '|';
				return trama;
			} else if (tipo_operacion.equals(PAGO_TRAN_INTEBANCARIAS)) {
			

				/* MSD 11/2005 Proyecto Ley de Transparencia */
				String dirIp = "";
				if (concepto == null || concepto.trim().equals(" "))
					concepto = " @   ";
				if (concepto.indexOf('@') != -1) {
					referencia = concepto.substring(0, concepto.indexOf('@'));
					concepto = concepto.substring(concepto.indexOf('@') + 1);
					String tramaSPID[] = concepto.split("@");

					if(tramaSPID != null && tramaSPID.length > 1){
						divisaSpid = tramaSPID[1];
						if(tramaSPID[0].contains("RFC") && tramaSPID[0].contains("IVA")){
							rfcSpid = tramaSPID[0].substring(tramaSPID[0].indexOf("RFC"), tramaSPID[0].indexOf("IVA")).replace("RFC", "").trim();
						}						
					}				
				}
				
				try {
					InetAddress direccion = InetAddress.getLocalHost(); 
					dirIp = direccion.getHostAddress();
				} catch (Exception e) {
					EIGlobal.mensajePorTrace("DatosManc - Error al obtener IP "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
				}
				
				referencia = (referencia.trim().equals("")) ? " " : referencia;
				concepto = (concepto.trim().equals("")) ? "   " : concepto;

				System.out.println("Forma Aplica" + formaAplica);

				/* MSD 11/2005 Proyecto Ley de Transparencia */
				plaza = (plaza == null || plaza.trim().equals("")) ? " "
						: plaza;
				sucursal = (sucursal == null || sucursal.trim().equals("")) ? "0"
						: sucursal;
				trama += titular + '|' + cta_destino + '|' + banco + '|'
						+ beneficiario + '|' + sucursal + '|' + importe + '|'
						+ plaza + '|' + referencia_medio + '|' + concepto + '|'
						+ referencia + "|" + folioRegistro + '|'; /*
																 * MSD 11/2005
																 * Proyecto Ley
																 * de
																 * Transparencia
																 */
				/*SPID Vector 2016*/
				String dvxSpid = "".equals(divisaSpid) ? "" : divisaSpid;
				String rfcvSpid = "".equals(rfcSpid) ? " " : rfcSpid;
				trama += formaAplica + "|"+ dvxSpid +"|"+rfcvSpid+"|"+ dirIp+"|";
				
				System.out.println("ESTA ES LA TRAMA QUE LLEGA EL MANCSRVR -->"
						+ trama);
				return trama;
			
			} else {
				// C�digo Antes de Modificaci�n
				// return null;
				// C�digo Modificado
				// Comparaci�n para determinar si es una operaci�n de Prepago en
				// Mancomunidad
				// NVS IM314336 SLF JULIO IM314336
				if (tipo_operacion.equals("RG03")) {
					while (importe.indexOf('0') == 0)
						importe = importe.substring(1, importe.length());

					if (importe.indexOf('.') < 0)
						importe += ".00";
					else if ((importe.indexOf('.') - importe.length()) > -3) {
						int faltan = 3 - (importe.length() - importe
								.indexOf('.'));
						while ((faltan--) > 0)
							importe += "0";
					} else if ((importe.indexOf('.') - importe.length()) < -3)
						importe = importe
								.substring(0, importe.length()
										- (importe.length()
												- importe.indexOf('.') - 3));

					trama = "1EWEB|" + usuario;
					trama += "|RG03|" + '|' + contrato + '|' + usuario + '|'
							+ clavePerfil + '|';
					// IM314336 - NVS - 14-Abril-2005 - Reemplazo del codigo
					// comentado abajo
					trama += cta_origen.substring(0, 2);
					trama += cta_origen.substring(2, 10);
					trama += cta_origen.substring(10, 11) + '|';

					trama += cta_destino.substring(0, 2) + '|';
					trama += cta_destino.substring(2, 10) + '|';
					trama += cta_destino.substring(10, 11) + '|';
					// trama += cta_origen.substring(0,2) + '|';
					// trama += cta_origen.substring(2,10) + '|';
					// trama += cta_origen.substring(10,11) + '|';
					String fecha;

					if (fchRegistro.length() > 0)
						fecha = fchRegistro.substring(0, 2)
								+ fchRegistro.substring(3, 5)
								+ fchRegistro.substring(8, 10);
					else
						fecha = "010101";
					System.out.println("Fecha" + fecha);
					trama += fecha + '|';
					trama += importe + '|';
					// IM314336 - NVS - 08-Abril-2005
					// trama += " |" ;
					trama += folioRegistro + '|';

					System.out
							.println("ESTA ES LA TRAMA QUE LLEGA EL MANCSRVR -->"
									+ trama);
					return trama;
				} else {
					return null;
				}
				// IM314336 Aqu� termina la modificaci�n.
			}
		}
	}

    /** Metodo para generar la trama de cancelaci&oacute;n
     * @param usuario String con el usuario que va a autorizar la transacci&oacute;n
     * @param contrato String con el contrato del usuario
     * @param clavePerfil String con la clave del perfil del usuario
     * @return String con la trama de autorizaci&oacute;n
     */
    public String getTramaCancelacion (String usuario, String contrato,
    String clavePerfil) {

	String fch_registro_trama="";

	fch_registro_trama =	fchRegistro.substring(0,2) + fchRegistro.substring(3,5) + fchRegistro.substring(6,10);

	return "1EWEB|" + usuario + "|CAMA|" + contrato + '|' + usuario + '|' + clavePerfil + '|' + fch_registro_trama + '@' + folioRegistro +'@';
    }

    /** Metodo para formatear el bean en html
     * @param index Indice del registro
     * @return String con formato en html para desplegar el resultado
     */
    public String getFormatoHtmlRes (int index) {
		java.text.NumberFormat formatoMoneda = java.text.NumberFormat.getCurrencyInstance (Locale.US);
	String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/mm/yyyy");
	Date hoy = new Date ();
	String row = "<tr>";
	row += "<td align='center' class='" + color + "'>" + cta_origen + "</td>";

	if ("PILC".equals(tipo_operacion.trim()) || "OCUR".equals(tipo_operacion.trim()))
	{
		 row += "<td align='center' class='" + color + "'> &nbsp; </td>";
    }
	else if (PAGO_REF_MICROSITIO.equals(tipo_operacion.trim()))
	{
		if (concepto.lastIndexOf('@') > concepto.indexOf('@')+1){
			row += "<td align='center' class='" + color + "'>" + concepto.substring(concepto.indexOf('@')+1, concepto.lastIndexOf('@')) + "</td>";
		}
	}
	else
	  row += "<td align='center' class='" + color + "'>" + cta_destino + "</td>";
	String moneda = formatoMoneda.format (Double.parseDouble (importe));

	if (moneda.substring(0, 3).equals("MXN"))
		row += "<td align='right' class='" + color + "'>" + "$ " + moneda.substring(3, moneda.length()) + "</td>";
	else
		row += "<td align='right' class='" + color + "'>" + formatoMoneda.format (Double.parseDouble (importe)) + "</td>";
	row += "<td align='center' class='" + color + "'>" + folioRegistro + "</td>";
	row += "<td align='center' class='" + color + "'>&nbsp;</td>";
	row += "<td align='center' class='" + color + "'>&nbsp;</td>";
	row += "<td align='center' class='" + color + "'> Por Procesar estatus </td>";
	row += "</tr>\n";
	return row;
    }

    /** Metodo para obtener la cuenta origen
     * @return String con la cuenta origen
     */
    public String getCta_origen () {return cta_origen;}

    /** Metodo para obtener la cuenta destino
     * @return String con la cuenta destino
     */
    public String getCta_destino () {return cta_destino;}

    /** Metodo para obtener el folio de registro
     * @return String con el folio de registro
     */
    public String getFolio_registro () {return folioRegistro;}

    /** Metodo para obtener el importe
     * @return String con el importe
     */
    public String getImporte () {return importe;}

    /** Metodo para obtener el tipo de operaci&oacute;n
     * @return String con el tipo de operaci&oacute;n
     */
    public String getTipo_operacion () {return tipo_operacion;}

    public static String Description (String key) {
	 Hashtable GetMovDescription = new Hashtable ();

	 if (key == null)
	     return "";
	 try {
	     GetMovDescription.put ("SDCT", "Cons. Saldo Efectivo");
	     GetMovDescription.put ("SDSI", "Cons. Saldo Fondos");
	     GetMovDescription.put ("SDCR", "Cons. Saldo Tarjeta");
	     GetMovDescription.put ("MOVS", "Cons. Movimientos");
	     GetMovDescription.put ("MOVV", "Cons. Movtos. Vista");
	     GetMovDescription.put ("MOVD", "Cons. Motos. Devoluciones");
	     GetMovDescription.put ("MOVT", "Cons. Movtos. T.C.");
	     GetMovDescription.put ("POSI", "Cons. Posici�n");
	     GetMovDescription.put ("AMBI", "Act. del Ambiente");
	     GetMovDescription.put ("PASS", "Cambio de Contrase�a");
	     GetMovDescription.put ("INDI", "Cons. �ndice BMV");
	     GetMovDescription.put ("PHOY", "Cons. Emisora(d�a)");
	     GetMovDescription.put ("PMES", "Cons. Emisora(mes)");
	     GetMovDescription.put ("INNO", "Cons. Noticias");
	     GetMovDescription.put ("NOTI", "Cons. una Noticia");
	     GetMovDescription.put ("MDIN", "Cons. Mdo. de Dinero");
	     GetMovDescription.put ("TACP", "Cons. Tasas Cta. Personal");
	     GetMovDescription.put ("ENME", "Env�o Mensaje");
	     GetMovDescription.put ("AVIS", "Cons. Avisos");
	     GetMovDescription.put ("TCAM", "Cons. Tipos de Cambio");
	     GetMovDescription.put ("MENS", "Cons. de Mensajes");
	     GetMovDescription.put ("INFL", "Cons. Inflaci�n");
	     GetMovDescription.put ("CPSI", "Compra Soc. de Inversi�n");
	     GetMovDescription.put ("VTSI", "Venta Soc. de Inversi�n");
	     GetMovDescription.put ("TRAN", "Transferencia Efectivo");
	     GetMovDescription.put ("CAIB", "Dep�sito Interbancario");
	     GetMovDescription.put ("PAGT", "Dep�sito de Tarjeta");
	     GetMovDescription.put ("DICR", "Disposici�n Cr�dito");
	     GetMovDescription.put ("CATA", "Act. de Cat�logos");
	     GetMovDescription.put ("PROG", "PROG de Operaciones");
	     GetMovDescription.put ("CPRO", "Cancelaci�n Operaci�n");
	     GetMovDescription.put ("ABIT", "Act. de Bit�cora");
	     GetMovDescription.put ("CPDI", "Cheques a Inversi�n Vista");
	     GetMovDescription.put ("CPDV", "Cheques a Inversi�n a Plazo");
	     GetMovDescription.put ("CPID", "Inversi�n Vista Cheques");
	     GetMovDescription.put ("CPVD", "Inversi�n a Plazo Cheques");
	     GetMovDescription.put ("CPCC", "Cons. Contr. Plazo");
	     GetMovDescription.put ("PSER", "Pago de Servcio");
	     GetMovDescription.put ("RETJ", "Retiro de Tarjeta");
	     GetMovDescription.put ("TRTJ", "Retiro de Tarjeta");
	     GetMovDescription.put ("CPCI", "Cambio de Instrucci�n");
	     GetMovDescription.put ("DIBC", "Cotizaci�n Interbancaria");
	     GetMovDescription.put ("DIBT", "Pago Intebancario");
	     GetMovDescription.put ("DIBA", "Consulta Pago Interbancario");
	     GetMovDescription.put ("RE03", "Pago Cuota IMSS");
	     GetMovDescription.put ("RG01", "Consulta Saldo Cr�dito");
	     GetMovDescription.put ("RG02", "Movimientos de Cr�dito");
	     GetMovDescription.put ("RG03", "Prepago Cr�dito");
	     GetMovDescription.put ("RPIB", "Recuperaci�n Interbancarios");
	     GetMovDescription.put ("ALCT", "Alta de Cuentas");
	     GetMovDescription.put ("BACT", "Baja de Cuenta");
	     GetMovDescription.put ("PIMP", "Pago de Impuestos");
	     /*
		  * Mejoras de Linea de Captura
		  * Se modifica la leyenda LC por Pago Referenciado SAT para la Bitacora
		  * Inicia RRR - Indra Marzo 2014
		  */
		 GetMovDescription.put ("PILC", "Pago de Impuestos por Pago Referenciado SAT");
		 /*
		  * Fin RRR - Indra Marzo 2014
		  */
		 
		 /*
		  * Pago referenciado SAT micrositio 01/09/2014
		  * Clave de operacion PMRF 
		  * ISBAN - IOV  
		  */
		 GetMovDescription.put ("PMRF", "Pago Referencioado SAT por Micrositio");
	     GetMovDescription.put ("CIMP", "Cancelaci�n de Impuestos");
	     GetMovDescription.put ("CONI", "Consulta de Impuestos");
	     GetMovDescription.put ("INGC", "Ing. cheque seg.");
	     GetMovDescription.put ("INGA", "Ing. Arch cheques seg.");
	     GetMovDescription.put ("ERRO", "Operaci�n no Ejecutada");
	     GetMovDescription.put ("TI01", "Alta Estruct. Teso");
	     GetMovDescription.put ("TI02", "Consulta Estruct. Teso");
	     GetMovDescription.put ("TI03", "Baja Estruct. Teso");
	     GetMovDescription.put ("TI04", "Mod. Estruct. Teso");
	     GetMovDescription.put ("TI05", "Cons. Hist. Teso");
	     GetMovDescription.put ("TRAF", "Envio Archivo Rafaga");
	     GetMovDescription.put ("CRAF", "Consulta Archivo Rafaga");
	     GetMovDescription.put ("ACMA", "Act. de Firmas Mancomunadas");
	     GetMovDescription.put ("CAMA", "Cancelaci�n de Oper. Mancomunadas");
	     GetMovDescription.put ("ESMA", "Oper. Especiales Mancomunadas");
	     GetMovDescription.put ("IN07", "Act. Empleados Nomina Santander");
	     GetMovDescription.put ("IN09", "Rec. Arch. Empleados Nomina Santander");
	     GetMovDescription.put ("IN04", "Pago Nomina Santander");
	     GetMovDescription.put ("CFAC", "Act. Cat�logos Confirming");
	     GetMovDescription.put ("CFAR", "Actualizaci�n de Archivos de proveedores");
	     GetMovDescription.put ("CFAP", "Act. Cat�logos Generales");
	     GetMovDescription.put ("CFAE", "Act. Archivos de proveedores");
	     GetMovDescription.put ("CFRP", "Recepci�n de Archivo Pagos");
	     GetMovDescription.put ("CFRR", "Recepci�n de Archivo de proveedores");
	     GetMovDescription.put ("CFMR", "Modificaci�n de un proveedor");
	     GetMovDescription.put ("CFCP", "Cancelaci�n de Pago");
	     GetMovDescription.put ("CFEP", "Recuperaci�n de Archivos de Pago");
	     GetMovDescription.put ("CFER", "Recuperaci�n de Archivos de proveedores");
	     GetMovDescription.put ("CFDP", "Cons. Datos proveedor");
	     GetMovDescription.put ("CFTF", "Ac. Tasas de Factoraje");
	     GetMovDescription.put ("CFPB", "Baja Cuenta Impuestos");
	     GetMovDescription.put ("CFCA", "Cotizaci�n Doctos.");
	     GetMovDescription.put ("CFPC", "Atc. Doctos. Confirming");
	     GetMovDescription.put ("AAOP", "Alta de Archivo Pago Directo");
	     GetMovDescription.put ("CBOP", "Cons. de Benef. Pago Directo");
	     GetMovDescription.put ("CROP", "Cons Pagos Pago Directo");
	     GetMovDescription.put ("COOP", "Cons. Pagos Pago Directo");
	     GetMovDescription.put ("OAOP", "Cons. Sucuarsales Pago Directo");
	     GetMovDescription.put ("ALOP", "Reg. L�nea Pago Directo");
	     GetMovDescription.put ("ABOP", "Alta Benef. Pago Directo");
	     GetMovDescription.put ("BBOP", "Baja Benef. Pago Directo");
	     GetMovDescription.put ("MBOP", "Modif. BEnef. Pago Directo");
	     GetMovDescription.put ("AROP", "Alta/Baja Rel. Ctas. Pago Directo");
	     GetMovDescription.put ("CAOP", "Cancelaci�n Pago Directo");
	     GetMovDescription.put ("ABRE", "Alata/Baja Relaci�n Cheque Seg.");
	     GetMovDescription.put ("ACRE", "ActualizacI�n Relac�n Cheque Seg.");
	     GetMovDescription.put ("CBEN", "Actualizaci�n Beneficiarios Cheques Seg.");
	     GetMovDescription.put ("CCOH", "Consulta de Cheques Seg.");
	     GetMovDescription.put ("RE01", "Obtiene Referencia  del SUA");
	     GetMovDescription.put ("RE04", "Consulta de SUA");
	     GetMovDescription.put ("REFC", "Obtiene Referencia de Cheque Seg.");
	     GetMovDescription.put ("SEOP", "Secuencia de Archivo Orden de Pago");
	     GetMovDescription.put ("ARIM", "Envio de Declaraci�n de Pago de Impuestos");
	     GetMovDescription.put ("ACIM", "Actualizaci�n de Pago de Impuestos");
	     GetMovDescription.put ("IN05", "Ingreso de Pago N�mina");
	     GetMovDescription.put ("IN06", "Invern�mina");
	     GetMovDescription.put ("DIBP", "Pago Interbancario");
	     GetMovDescription.put ("IN02", "Alta de Empleados de Invern�mina");
	     GetMovDescription.put ("ABEN", "Alta y Baja de Beneficiario");
	     GetMovDescription.put ("AIMP", "Alta de cuenta de pago de impuestos");
	     GetMovDescription.put ("BIMP", "Baja de cuenta de pago de imp.");
	     GetMovDescription.put ("CACH", "Cancelaci�n de cheques Seg.");
	     GetMovDescription.put ("COCH", "Consulta de cheq. Seg.");
	     GetMovDescription.put ("RE02", "Elimina un archivo incompleto de SUA");
	     GetMovDescription.put ("IN01", "Aviso del n�mero de registros a enviar");
	     GetMovDescription.put ("IN03", "Verifica num reg insertados en Invern�mina");
	     GetMovDescription.put ("IN08", "Invernomina");
	     GetMovDescription.put ("MBEN", "Modificaci�n de Beneficiarios de cheques Seg.");
	     GetMovDescription.put ("TF01", "Tesorer�a de la Fed");
	     GetMovDescription.put ("BBEN", "Baja beneficiario cheq. Seg.");
	     GetMovDescription.put ("ERAF", "Ejecuta Trans. Rafaga");
	     GetMovDescription.put ("MAXB", "Saldos");
	     GetMovDescription.put ("MAXT", "Saldos");
	     GetMovDescription.put ("MAXC", "Saldos");
	     GetMovDescription.put ("CCTR", "Cambio de Contrato");
	     GetMovDescription.put ("TI01", "Alta Estruct. Teso");
	     GetMovDescription.put ("TI02", "Consulta Estruct. Teso");
	     GetMovDescription.put ("TI03", "Baja Estruct. Teso");
	     GetMovDescription.put ("TI04", "Mod. Estruct. Teso");
	     GetMovDescription.put ("TI05", "Cons. Hist. Teso");
	     GetMovDescription.put ("COMI", "Consulta de comisiones cobradas");
	     GetMovDescription.put ("OCUR", "Ordenes de pago ocurre");
	     GetMovDescription.put ("MULT", "Multicheque");
	     GetMovDescription.put ("DICO", "Cotizaci�n Internacional");
	     GetMovDescription.put ("DITA", "Transferencia Internacional");
	     GetMovDescription.put ("DIPD", "Cambios(Transferencia MN-Dlls / Dlls-MN");
	     GetMovDescription.put ("DIIF", "Consulta Transferencias Internacionales");
	     GetMovDescription.put ("DIAE", "Alta Ctas. Internacionales");
	     GetMovDescription.put ("DIBE", "Baja Ctas. Internacionales");
	     GetMovDescription.put ("DICE", "Consulta Ctas. Internacionales");
	     GetMovDescription.put ("DIDV", "Cheques a Inversi�n a Plazo D�lar");
		 GetMovDescription.put ("SATP", "Nvo Pago de Impuestos");
		 GetMovDescription.put ("SARA", "Pago de SAR");
//Getronics CP Mexico, Q25268 IKDA
		 GetMovDescription.put ("INTE", "N�mina Interbancaria");
		 //VSWF Nomina Prepago
		 GetMovDescription.put ("PNIS", "Pago Tarjeta de Pagos Santander Individual");//YHG NPRE
		 GetMovDescription.put ("PNOS", "Pago Tarjeta de Pagos Santander ");//YHG NPRE
		 GetMovDescription.put (PNLI, "Pago N"+ (char)243 +"mina en L"+ (char)237 +"nea ");
		 GetMovDescription.put ("GFOL","Generaci"+ (char)243 +"n de Folio de Mancomunidad");
		 GetMovDescription.put ("PASS","Cambio de Contrase�a");
		 GetMovDescription.put ("MODP","Modificaci"+ (char)243 +"n de Datos Personales");
		 GetMovDescription.put ("CCNM","Consulta de Cat"+ (char)225 +"logo de N"+ (char)243 +"mina Mismo Banco");
		 GetMovDescription.put ("MCNM","Modificaci"+ (char)243 +"n de Catalogo de N"+ (char)243 +"mina Mismo Banco");
		 GetMovDescription.put ("BCNM","Baja de Catalogo de N"+ (char)243 +"mina Mismo Banco");
		 GetMovDescription.put ("COST","Consulta de Seguimiento de Transferencias");
		 GetMovDescription.put ("CCNI","Consulta de Catalogo de N"+ (char)243 +"mina Interbancaria");
		 GetMovDescription.put ("BCNI","Baja de Catalogo de Nomina Interbancaria");
		 GetMovDescription.put ("ACNI","Autorizaci"+ (char)243 +"n y Cancelaci"+ (char)243 +"n de Cuentas en N"+ (char)243 +"mina Interbancaria");
		 GetMovDescription.put ("CMAN","Confirmaci"+ (char)243 +"n de Operaciones Mancomunadas");
		 GetMovDescription.put ("CGFO","Confirmaci"+ (char)243 +"n de Generaci"+ (char)243 +"n de Folio");
		 GetMovDescription.put ("CCTR","Cambio de Contrato");
		 GetMovDescription.put ("CTRA","Confirmaci"+ (char)243 +"n de transferencia Moneda Nacional");
		 GetMovDescription.put ("CTUS","Confirmaci"+ (char)243 +"n de transferencia en D"+ (char)243 +"lares");
		 GetMovDescription.put ("COCM","Confirmaci"+ (char)243 +"n de Pago Ocurre Morales");
		 GetMovDescription.put ("COCF","Confirmaci"+ (char)243 +"n de Pago Ocurre F"+ (char)237 +"sicas");
		 GetMovDescription.put ("COTI","Confirmaci"+ (char)243 +"n de Transferencias Interbancarias");
		 GetMovDescription.put ("CPTC","Confirmaci"+ (char)243 +"n de Pago de Tarjeta de Cr"+ (char)233 +"dito");
		 GetMovDescription.put ("CDTC","Confirmaci"+ (char)243 +"n Disposici"+ (char)243 +"n de Tarjeta de Cr"+ (char)233 +"dito");
		 GetMovDescription.put ("CTIN","Confirmaci"+ (char)243 +"n de Transferencias Internacionales");
		 GetMovDescription.put ("CSAT","Confirmaci"+ (char)243 +"n de Pago de Impuestos Provisionales");
		 GetMovDescription.put ("CSAE","Confirmaci"+ (char)243 +"n de Pago de Impuestos del Ejercicio");
		 GetMovDescription.put ("CSEF","Confirmaci"+ (char)243 +"n de Pago de Impuestos Entidades Federativas");
		 GetMovDescription.put ("CDPA","Confirmaci"+ (char)243 +"n de Pago de Impuestos Derechos, Productos y Aprovechamiento");
		 /*
		  * Mejoras de Linea de Captura
		  * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT para la Bitacora
		  * Inicia RRR - Indra Marzo 2014
		  */
		 GetMovDescription.put ("CPLC","Confirmaci"+ (char)243 +"n de Pago de Impuestos por Pago Referenciado SAT");
		 /*
		  * Fin RRR - Indra Marzo 2014
		  */
		 GetMovDescription.put ("CSUA","Confirmaci"+ (char)243 +"n de Pago de Impuestos SUA");
		 GetMovDescription.put ("CSIP","Confirmaci"+ (char)243 +"n de pago SUA (SIPARE)");
		 GetMovDescription.put ("CPNS","Confirmaci"+ (char)243 +"n de Pago de N"+ (char)243 +"mina Santander");
		 GetMovDescription.put ("CPNI","Confirmaci"+ (char)243 +"n de Pago de N"+ (char)243 +"mina Interbancaria");
		 GetMovDescription.put ("CPNP","Confirmaci"+ (char)243 +"n de Pago de Tarjeta de Pagos Santander ");//YHG NPRE
		 GetMovDescription.put ("CCHS","Confirmaci"+ (char)243 +"n de Liberaci"+ (char)243 +"n de Cheque Seguridad");
		 GetMovDescription.put ("CPAD","Confirmaci"+ (char)243 +"n de Liberaci"+ (char)243 +"n de Pago Directo");
		 GetMovDescription.put ("COPP","Confirmaci"+ (char)243 +"n de Pago a Proveedores");
		 GetMovDescription.put ("CCDI","Confirmaci"+ (char)243 +"n de Cambio de Divisas");
		 GetMovDescription.put ("CSAR","Confirmaci"+ (char)243 +"n de Pago SAR");
		 GetMovDescription.put ("CCFI","Confirmaci"+ (char)243 +"n de Compra de Fondos de Inversi"+ (char)243 +"n");
		 GetMovDescription.put ("CVFI","Confirmaci"+ (char)243 +"n de Venta de Fondos de Inversi"+ (char)243 +"n");
		 GetMovDescription.put ("CCHV","Confirmaci"+ (char)243 +"n de Cheque Vista");
		 GetMovDescription.put ("SULC","Pago de Linea de Captura (SIPARE)"); //VAO 08-2012
		 /**
		  * Descripcion de Operacion del Micrositio
		  */
		 GetMovDescription.put ("PMIC", "Pago Micrositio");

	 } catch( Exception e ) {
	     e.printStackTrace ();
	     return null;
	 }
	 String regreso = "";
	 regreso = (String) GetMovDescription.get (key);

	 if(regreso == null)
	     regreso = "";
	 return regreso;
     }

    /** Funci&oacute;n para formatear el bean en String
     * @return String con el bean formateado
     */
    public String toString () {
	return 	fchRegistro.trim () + ';' +
			folioRegistro.trim () + ';' +
	    	tipo_operacion.trim () + ';' +
	    	fch_autorizacion.trim () + ';' +
	    	folio_autorizacion.trim () + ';' +
	    	usuario1.trim () + ';' +
	    	descUsr1.trim () + ';' +
	    	usuario2.trim () + ';' +
	    	descUsr2.trim () + ';' +
	    	usr3.trim () + ';' +
	    	descUsr3.trim () + ';' +
	    	usr4.trim () + ';' +
	    	descUsr4.trim () + ';' +
	    	cta_origen.trim () + ';' +
	    	cta_destino.trim () + ';' +
	    	descripcion.trim () + ';' +
	    	importe.trim () + ';' +
	    	estatus.trim () + ";\r\n";
    }

	/**
	 * concepto
	 * @return el concepto concepto
	 */
	public String getConcepto() {
		return this.concepto;
	}

	/**
	 * titular 
	 * @return el titular
	 */
	public String getTitular() {
		return this.titular;
	}

	/**
	 * fecha autorizaci�n
	 * @return el fch_autorizacion
	 */
	public String getFch_autorizacion() {
		return this.fch_autorizacion;
	}

	/**
	 * folio aut
	 * @return el folio_autorizacion
	 */
	public String getFolio_autorizacion() {
		return this.folio_autorizacion;
	}

	/**
	 * establece tipo de operaci�n
	 * @param tipo_operacion
	 */
	public void setTipo_operacion(String tipo_operacion) {
		this.tipo_operacion = tipo_operacion;
	}

	/**
	 * obtiene la descripcionde un usuario
	 * @return String desc usuario1 
	 */
	public String getDescUsr1() {
		return this.descUsr1;
	}

	/**
	 * estabalece la descripcion de un usuario
	 * @param descUsr1 desc usuario1
	 */
	public void setDescUsr1(String descUsr1) {
		this.descUsr1 = descUsr1;
	}

	/**
	 * obtine la descripcion de usuario
	 * @return String usuario2
	 */
	public String getDescUsr2() {
		return this.descUsr2;
	}
	/**
	 * usauri2 
	 * @param descUsr2 usuario2
	 */
	public void setDescUsr2(String descUsr2) {
		this.descUsr2 = descUsr2;
	}

	/**
	 * obtine la descripcion de usuario
	 * @return String usuario3
	 */
	public String getUsr3() {
		return this.usr3;
	}
	
	/**
	 * usuario3
	 * @param usr3 usuario3
	 */
	public void setUsr3(String usr3) {
		this.usr3 = usr3;
	}

	/**
	 * obtine la descripcion de usuario
	 * @return String usuario3
	 */
	public String getDescUsr3() {
		return this.descUsr3;
	}
	
	/**
	 * establece la descripcion 
	 * @param descUsr3 usuario3
	 */
	public void setDescUsr3(String descUsr3) {
		this.descUsr3 = descUsr3;
	}
	
	/**
	 * usuario4
	 * @return String usuario4
	 */
	public String getUsr4() {
		return this.usr4;
	}

	/**
	 * usuario 4
	 * @param usr4 usuario4
	 */
	public void setUsr4(String usr4) {
		this.usr4 = usr4;
	}

	/**
	 * usuario 4
	 * @return String usuario4
	 */
	public String getDescUsr4() {
		return this.descUsr4;
	}
	
	/**
	 * usuario 4
	 * @param descUsr4 usuario 4
	 */
	public void setDescUsr4(String descUsr4) {
		this.descUsr4 = descUsr4;
	}
	
	/**
	 *  obtioene la descripcion de una operacion
	 * @return String descripcion
	 */
	public String getDescripcion() {
		return this.descripcion;
	}

	/**
	 * Establece descripcion
	 * @param descripcion descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * getBandPintaNL bandera para pintar chk
	 * @return bandPintaNL indica si pinta el chk
	 */
	public boolean getBandPintaNL() {
		return this.bandPintaNL;
	}
	/**
	 * setBandPintaNL bandera para pntar chk
	 * @param bandPintaNL indicador para pintar chk
	 */
	public void setBandPintaNL(boolean bandPintaNL) {
		this.bandPintaNL = bandPintaNL;
	}
	/**
	 * getEstatus obtener el valor del estatus
	 * @return estatus estatus de operacion
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * setEstatus poner valor del estatus
	 * @param estatus estatus de la operacion
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


}