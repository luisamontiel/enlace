/** 
*   Isban Mexico
*   Clase: ExportConsultaBO.java
*   Descripcion: Objeto De negocio que gestiona la funcionalidad de exportar.
*   
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Implementacion permite la gestion de transferencia en dolares(Spid). 
*/

package mx.altec.enlace.bo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConsultaCtasMovilesBean;
import mx.altec.enlace.beans.CuentaInterbancariaBean;
import mx.altec.enlace.beans.CuentaInternacionalBean;
import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.beans.CuentasSantanderBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConsultaCtasBO;
import mx.altec.enlace.bo.ConsultaCtasMovilesBO;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.export.exception.ExportarException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

public class ExportConsultaBO {
	/** Variable de apoyo para log. */
	private static final Logger LOG = Logger.getLogger(ExportConsultaBO.class);

    /**
     * El Id para la serializacion. 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Nombre del parametro que indica el tipo de archivo a exportar.
     */
    private static final String PARAM_TIPO = "tipoArchivo";
    
    /**
     * Nombre del parametro contiene el nombre del atributo se sesion donde se 
     * almacena la ruta del archivo a exportar.
     */
    private static final String PARAM_ATRIBUTO_FILE = "af"; 
    
    /**
     * Nombre del parametro contiene el nombre del atributo se sesion donde se 
     * almacena el modelo de exportacion.
     */
    private static final String PARAM_ATRIBUTO_EXPORT_MODEL = "em";
    
    /**
     * Tipo de Exportacion utilizada Bean, SubString, Separados.
     */
    private static final String PARAM_TIPO_EXPORT = "metodoExportacion";
    
    /**
     * Nombre del Bean para identificar el objeto con la informacion a exportar.
     */
    private static final String NOMBRE_BEAN_EXPORT = "nombreBeanExport";
    
    /**
     * Tipo de archivo a exportar como texto plano.
     */
    private static final String TIPO_TXT = "txt";
    
    /**
     * Tipo de archivo a exportar como separado por comas.
     */
    private static final String TIPO_CSV = "csv";
    
    /** Constante BANCO */
    private static final String BANCO = "Banco";

    /**
     * Metodo que atiende peticiones del flujo de exportacion de alta de cuentas
     * @param request Objeto inherente a la peticion 
     * @param response Objeto inherente a la respuesta 
     * @throws IOException Excepcion generica
     */
    public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String paramTipo = request.getParameter(PARAM_TIPO);
        String paramEM = request.getParameter(PARAM_ATRIBUTO_EXPORT_MODEL);
        String paramTipoExport = request.getParameter(PARAM_TIPO_EXPORT);
        String tipoExportar = TIPO_TXT;
        String TipoCuenta = request.getParameter("TipoCuenta");
        String Criterio = request.getParameter("Criterio");
        String TextoBuscar = request.getParameter("TextoBuscar");
        
        checkValue(response, PARAM_TIPO, paramTipo);
        checkValue(response, PARAM_ATRIBUTO_EXPORT_MODEL, paramEM);
        checkValue(response, PARAM_TIPO_EXPORT, paramTipoExport);
        checkValue(response, "TipoCuenta", TipoCuenta);
        checkValue(response, "Criterio", Criterio);
        checkValue(response, "TextoBuscar", TextoBuscar);
        
        LOG.debug("ExportConsultaServlet - Tipo::" + paramTipo + "::");        
        LOG.debug("ExportConsultaServlet - paramEM::" + paramEM + "::");
        LOG.debug("ExportConsultaServlet - paramTipoExport::" + paramTipoExport + "::");
        LOG.debug("ExportConsultaServlet - tipoExportar::" + tipoExportar + "::");
        LOG.debug("ExportConsultaServlet - TipoCuenta::" + TipoCuenta + "::");
        LOG.debug("ExportConsultaServlet - Criterio::" + Criterio + "::");
        LOG.debug("ExportConsultaServlet - TextoBuscar::" + TextoBuscar + "::");
        
        ExportModel em = null;
        List<?> lstBean = null;
        final HttpSession sess = request.getSession();
        final BaseResource session = (BaseResource) sess.getAttribute("session");
        final ConsultaCtasBO ConsultaExportacionBO =  new ConsultaCtasBO();
        if ("RBSantander".equals(TipoCuenta)){
			List<CuentasSantanderBean> ctasSantanderList = null;//new ArrayList<CuentasSantanderBean>();
			final CuentasSantanderBean conCtasSantanderBean = new CuentasSantanderBean();
			conCtasSantanderBean.setContrato(session.getContractNumber());
			conCtasSantanderBean.setRegIni(0);
			conCtasSantanderBean.setRegFin(Integer.MAX_VALUE);
			conCtasSantanderBean.setCriterio( (Criterio != null) ? Criterio : "" );
			conCtasSantanderBean.setTextoBuscar( (TextoBuscar != null) ? TextoBuscar.toUpperCase() : "" );
			EIGlobal.mensajePorTrace("generaArchivoExportSantander::usuario::"+session.getUserID()+"::", NivelLog.WARN);
			conCtasSantanderBean.setUSUARIO(session.getUserID8());
			ctasSantanderList = ConsultaExportacionBO.consultaCtasSantander(conCtasSantanderBean);
			EIGlobal.mensajePorTrace("generaArchivoExportSantander::"+ctasSantanderList.size()+"::", NivelLog.WARN);
			if(null!=ctasSantanderList && !ctasSantanderList.isEmpty()){
				em = new ExportModel("CuentasMismoBanco_"+session.getContractNumber()+"_"+session.getUserID8(), '|',"@")
				   .addColumn(new Column("numCuenta", "Cuenta"))
				   .addColumn(new Column("descripcion", "Propietario"))
				   .addColumn(new Column("tipoRelacCtas", "Tipo"))
				   .addColumn(new Column("CLABE", "Clave Bancaria Estandarizada"))
				   ;
				lstBean = ctasSantanderList;
			}
        }else if("RBOtros".equals(TipoCuenta)){
        	List<CuentaInterbancariaBean> ctasInterbancariasList = new ArrayList<CuentaInterbancariaBean>();
        	final CuentaInterbancariaBean conCtasInterbancariasBean = new CuentaInterbancariaBean();
        	conCtasInterbancariasBean.setContrato(session.getContractNumber());
        	conCtasInterbancariasBean.setRegIni(0);
        	conCtasInterbancariasBean.setRegFin(Integer.MAX_VALUE);
        	conCtasInterbancariasBean.setCriterio( (Criterio != null) ? Criterio : "" );
        	conCtasInterbancariasBean.setTextoBuscar( (TextoBuscar != null) ? TextoBuscar.toUpperCase() : "" );
        	ctasInterbancariasList = ConsultaExportacionBO.consultaCtasInterbancarias(conCtasInterbancariasBean);
        	EIGlobal.mensajePorTrace("generaArchivoExportInterbancarias::"+ctasInterbancariasList.size()+"::", NivelLog.WARN);
        	if(null!=ctasInterbancariasList && !ctasInterbancariasList.isEmpty()){
        		em = new ExportModel("CuentasOtrosBancos_"+session.getContractNumber()+"_"+session.getUserID8(), '|', "@", true)
        		.addColumn(new Column("numeroInterbancaria", "Numero de Cuenta"))
        		.addColumn(new Column("descripcion", "Descripci�n"))
        		.addColumn(new Column("banco", BANCO))
        		.addColumn(new Column("plaza", "Plaza"))
        		.addColumn(new Column("sucursal", "Sucursal"))
        		.addColumn(new Column("divisa", "Divisa"))
        		;
        		lstBean = ctasInterbancariasList;
        	}
        }else if("RBExtranjeros".equals(TipoCuenta)){
			List<CuentaInternacionalBean> ctasInternacionalesList = new ArrayList<CuentaInternacionalBean>();
			final CuentaInternacionalBean conCtasInternacionalesBean = new CuentaInternacionalBean();
			conCtasInternacionalesBean.setContrato(session.getContractNumber());
			conCtasInternacionalesBean.setRegIni(0);
			conCtasInternacionalesBean.setRegFin(Integer.MAX_VALUE);
			conCtasInternacionalesBean.setCriterio( (Criterio != null) ? Criterio : "" );
			conCtasInternacionalesBean.setTextoBuscar( (TextoBuscar != null) ? TextoBuscar.toUpperCase() : "" );
			ctasInternacionalesList = ConsultaExportacionBO.consultaCtasInternacionales(conCtasInternacionalesBean);
			EIGlobal.mensajePorTrace("generaArchivoExportInterbancarias::"+ctasInternacionalesList.size()+"::", NivelLog.WARN);
			if(null!=ctasInternacionalesList && !ctasInternacionalesList.isEmpty()){
				em = new ExportModel("CuentasInternacionales_"+session.getContractNumber()+"_"+session.getUserID8(), '|', "@", true)
				.addColumn(new Column("numeroInternacional", "Numero de Cuenta"))
				.addColumn(new Column("titular", "Titular"))
				.addColumn(new Column("banco", BANCO))
				.addColumn(new Column("pais", "Pa�s"))
				.addColumn(new Column("ciudad", "Ciudad"))
				.addColumn(new Column("divisa", "Divisa"))
				.addColumn(new Column("aba", "Cve.ABA"))
				.addColumn(new Column("swift", "SWIFT"))
				.addColumn(new Column("direccionBenef", "Domicilio"))
				.addColumn(new Column("ciudadBenef", "Ciudad Beneficiario"))
				.addColumn(new Column("idClienteBenef", "Numero de Identificacion"))
				.addColumn(new Column("paisBenef", "Pais Beneficiario"))
				;
				lstBean = ctasInternacionalesList;
			}
        }else if("RBMovil".equals(TipoCuenta)){
        	try {
				List<CuentaMovilBean> ctasMovilesList = new ArrayList<CuentaMovilBean>();
				final ConsultaCtasMovilesBean conCtasMovBean = new ConsultaCtasMovilesBean();
				conCtasMovBean.setContrato(session.getContractNumber());
				conCtasMovBean.setRegIni(0);
				conCtasMovBean.setRegFin(Integer.MAX_VALUE);
				conCtasMovBean.setCriterio( (Criterio != null) ? Criterio : "" );
				conCtasMovBean.setTextoBuscar( (TextoBuscar != null) ? TextoBuscar.toUpperCase() : "" );
				ctasMovilesList = ConsultaExportacionBO.consultaCtasMovil(conCtasMovBean);
				EIGlobal.mensajePorTrace("generaArchivoExportMovil::"+ctasMovilesList.size()+"::", NivelLog.WARN);
				if(null!=ctasMovilesList && !ctasMovilesList.isEmpty()){
					em = new ExportModel("CuentasMoviles_"+session.getContractNumber()+"_"+session.getUserID8(), '|',"\t")
					.addColumn(new Column("numeroMovil", "N�mero Movil"))
					.addColumn(new Column("titular", "Nombre Titular"))
					.addColumn(new Column("cveBanco", BANCO))
					.addColumn(new Column("descBanco", "Descripcion Banco"))
					;
					lstBean = ctasMovilesList;
					if(TIPO_TXT.equals(paramTipo)){
						LOG.debug("Exportaci�n por archivo movil");
						generaArchivoExport(ctasMovilesList, request, response, Criterio, TextoBuscar,"CuentasMoviles_"+session.getContractNumber()+"_"+session.getUserID8());
						LOG.debug("Exportaci�n por archivo movil - se genero archivo");
						HtmlTableExporter.copyFile(em, "CuentasMoviles_"+session.getContractNumber()+"_"+session.getUserID8(), response);
						LOG.debug("Exportaci�n por archivo movil - exporto");
						return;
					}
				}
        	} catch (ServletException e) {
        		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			
        }
        
        checkValue(response, "Export Model", em);

        if (TIPO_CSV.equals(paramTipo)) {
        	tipoExportar = TIPO_CSV;
        }
        
        if("BEAN".equals(paramTipoExport.toUpperCase().trim())) {
        	LOG.debug("Exportaci�n de tipo bean");
        	String nombreBeanExport = request.getParameter(NOMBRE_BEAN_EXPORT);
        	checkValue(response, NOMBRE_BEAN_EXPORT, nombreBeanExport);
        	try {
        		LOG.debug("lstBean"+lstBean.size());
        		HtmlTableExporter.export(tipoExportar, em, lstBean, response);
			} catch (ExportarException e) {
				new ServletException(e.getMessage());
			}
        } else {
        	LOG.debug("Exportaci�n por archivo");
        	String paramAf = request.getParameter(PARAM_ATRIBUTO_FILE);
        	checkValue(response, PARAM_ATRIBUTO_FILE, paramAf);
        	String filePath = (String) request.getSession().getAttribute(paramAf);
        	checkValue(response, "File Path", filePath);
        	
        	LOG.debug("ExportConsultaServlet - EM.existente" + em.isArchivoExistente());
        	if (TIPO_TXT.equals(paramTipo) && em.isArchivoExistente()) {
        		HtmlTableExporter.copyFile(em, filePath, response);
        	} else {
        		HtmlTableExporter.export(tipoExportar, em, filePath, response);
        	}
        }
        
        
        LOG.debug("ExportConsultaServlet - termina exportaci�n");
    }
    
    /**
     * Check value.
     *
     * @param response the response
     * @param paramName the param name
     * @param paramValue the param value
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void checkValue(HttpServletResponse response, String paramName, Object paramValue) throws IOException {
    	if (paramValue == null) {
    		response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Parametro invalido: " + paramName);
    	}
    }

    /**
     * Genera archivo de exportacion
     * @param ctasMovilesList Lista de cuenyas moviles
     * @param req Objeto de inherente a la peticion
     * @param res Objeto inherente a la Respuesta
     * @param Criterio Criterio de Exportacion
     * @param TextoBuscar Texto a Buscar
     * @param nombreArchivo Nombre de archivp
     * @throws ServletException Excepcion de servlet
     * @throws java.io.IOException Excepcion Generica
     */
     private void generaArchivoExport(List<CuentaMovilBean> ctasMovilesList, HttpServletRequest req, HttpServletResponse res, String Criterio, String TextoBuscar, String nombreArchivo) throws ServletException, java.io.IOException
	{
	    try {

			final StringBuilder moviles = new StringBuilder();
			res.setContentType("text/plain");
			res.setHeader("Content-Disposition",
			    "attachment;filename="+nombreArchivo+".txt");
			moviles.append(StringUtils.rightPad("Numero Movil", 20));
			moviles.append(StringUtils.rightPad("Nombre Titular", 40));
			moviles.append(StringUtils.rightPad(BANCO, 5));
			moviles.append(StringUtils.rightPad("Descripcion Banco", 40)).append("\n");
			for (CuentaMovilBean cuenta : ctasMovilesList) {
				moviles.append(StringUtils.rightPad(cuenta.getNumeroMovil(), 20));
				moviles.append(StringUtils.rightPad(cuenta.getTitular(), 40));
				moviles.append(StringUtils.rightPad(cuenta.getCveBanco(), 5));
				moviles.append(StringUtils.rightPad(cuenta.getDescBanco(), 40)).append("\n");
			}
			final InputStream input =  new ByteArrayInputStream(moviles.toString().getBytes("UTF8"));
			int read = 0;
			final byte[] bytes = new byte[1024];
			final OutputStream os = res.getOutputStream();

			while ((read = input.read(bytes)) != -1) {
				os.write(bytes, 0, read);
			}
			os.flush();
			os.close();
        } catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
     }

}