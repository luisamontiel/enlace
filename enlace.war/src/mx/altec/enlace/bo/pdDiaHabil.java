/*
 * pdDiaHabil.java
 *
 * Created on 5 de abril de 2002, 11:48 AM
 */

package mx.altec.enlace.bo;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class pdDiaHabil implements java.io.Serializable {

    /** Identificador de dia habil */
    protected boolean Habil;

    /** Dia del mes */
    protected short Dia;

    /** Dia de la semana */
    protected int NomDia;

    /** Creates new pdDiaHabil */
    public pdDiaHabil () {
    }

    /**
     * Metodo para crear pdDiaHabil
     * @param habil Identificador si el dia es habil
     * @param dia Dia del mes
     * @param mes Mes del a�o en numero
     */
    public pdDiaHabil (boolean habil, short dia, int ndia) {
        Habil = habil;
        Dia = dia;
        NomDia = ndia;
    }

    /**
     * Metodo para obtener el identificador de dia habil
     * @return true si el dia es habil, de lo contrario false
     */
    public boolean getHabil () {return Habil;}

    /**
     * Metodo para obtener el dia del mes
     * @return Dia del mes
     */
    public short getDia () {return Dia;}

    /**
     * Metodo para obtener el dia de la semana
     * @return Dia de la semana
     */
    public int getNomDia () {return NomDia;}

    /**
     * Metodo para establecer el identificador de dia habil
     * @param habil true si el dia es habil
     */
    public void setHabil (boolean habil) {Habil = habil;}

    /**
     * Metodo para establecer el dia del mes
     * @param dia Dia del mes
     */
    public void setDia (short dia) {Dia = dia;}

    /**
     * Metodo para establecer el dia de la semana
     * @param dia Numero de dia de la semana
     */
    public void setNomDia (int dia) {NomDia = dia;}

    /**
     * Metodo para obtener el nombre del dia de la semana por su numero
     * @param dia Numero del dia
     * @return Nombre del dia
     */
    public String getNomDia (int dia) {
        switch (dia) {
            case 1:
                return "Domingo";
            case 2:
                return "Lunes";
            case 3:
                return "Martes";
            case 4:
                return "Miercoles";
            case 5:
                return "Jueves";
            case 6:
                return "Viernes";
            case 7:
                return "Sabado";
        }
        return null;
    }

}
