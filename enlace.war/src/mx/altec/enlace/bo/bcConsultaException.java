/*
 * bcConsultaException.java
 *
 * Created on 25 de abril de 2002, 01:08 PM
 */

package mx.altec.enlace.bo;

/**
 * Contiene la informacion de error si la consulta no se pudo realizar,
 * @author Rafael Martinez Montiel
 * @version 1.0
 */
public class bcConsultaException extends java.lang.Exception {

    /**
     * Creates new <code>bcConsultaException</code> without detail message.
     */
    public bcConsultaException() {
    }

    /**
     * Constructs an <code>bcConsultaException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public bcConsultaException(String msg) {
        super(msg);
    }
}


