package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;

import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class coConsultas
{

    /** Servico de Tuxedo */
    protected ServicioTux coConsultas;
    /** Numero de contrato */
    protected String Contrato;
    /** Clave de Usuario */
    protected String Usuario;
    /** Perfil del usuario */
    protected String Perfil;
    /** Fecha de inicio*/
	protected String fechaini;
	/** Fecha de Fin*/
	protected String fechafin;

    /** Creates new pdcoConsultas */
    public coConsultas() {
        coConsultas = new ServicioTux();
    }

    /**
     * Metodo para obtener el servicio de Tuxedo
     */
    public ServicioTux getServicioTux() {return coConsultas;}

    protected File recibe(String Nom) {
        try {
        	EIGlobal.mensajePorTrace("coConsultas:recibe - Inicia", EIGlobal.NivelLog.INFO);
         	// Archivo de configuracion
            Properties rs = new Properties();
            FileInputStream in = new FileInputStream(Global.ARCHIVO_CONFIGURACION);
            
        	//IF PROYECTO ATBIA1 (NAS) FASE II

            rs.load(in);

           	boolean Respuestal = true;
            ArchivoRemoto recibeArch = new ArchivoRemoto();
           
           	if(!recibeArch.copiaCUENTAS("/" + Nom, rs.getProperty("DIRECTORIO_LOCAL"))){
				
					EIGlobal.mensajePorTrace("*** coConsultas.recibe  No se realizo la copia remota:" + Nom, EIGlobal.NivelLog.ERROR);
					Respuestal = false;
	
				}
				else {
				    EIGlobal.mensajePorTrace("*** coConsultas.recibe archivo remoto copiado exitosamente:" + Nom, EIGlobal.NivelLog.DEBUG);
				    
				}
           
           	if (Respuestal) {
                File Archivo = new File(
                    Nom);

                return Archivo;
                
           	}

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return null;
    }


    public String envia_tux() {
        try {
        	// <vc meg 05122008 cambio migracion>
            String Trama = "2EWEB|" + Usuario + "|CFAC|" + Contrato + "|"
                            + Usuario + "|" + Perfil + "|" + "2@" +fechaini +"@"
							+ fechafin + "@|";
            File ArchivoResp;
            Hashtable ht = coConsultas.web_red(Trama);
            String Buffer = (String) ht.get("BUFFER");
            ArchivoResp = recibe(Buffer);
            if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "actua"));
            return (ArchivoResp.getPath());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error al leer el archivo de respuesta";
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error con la comunicacion";
        }

    }

   /* public String actualiza_grafica (String fechaIni, String fechaLim) {
        try {
            String Trama = "2EWEB|" + Usuario + "|CFAE|" + Contrato + "|" + Usuario + "|"
                            + Perfil + "|2@" + fechaIni + "@" + fechaLim + "@|";
            System.out.println ("coConsultas.class Trama: " + Trama);
            File ArchivoResp;
            Hashtable ht = coConsultas.web_red (Trama);
            String Buffer = (String) ht.get ("BUFFER");
            System.out.println ("coConsultas.class Respuesta: " + Buffer);
            ArchivoResp = recibe(Buffer);
            if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "graf"));
            return (ArchivoResp.getPath());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error al leer el archivo de respuesta";
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error con la comunicacion";
        }
    }*/


    public void setContrato (String contrato) {Contrato = contrato;}
    public void setUsuario (String usuario) {Usuario = usuario;}
    public void setPerfil (String perfil) {Perfil = perfil;}
	// <vc meg 05122008 cambio migracion>
	public void setFechain (String fechain) {fechaini = fechain;}
	public void setFechafin (String fechafin1) {fechafin = fechafin1;}
	// <vc meg 05122008 cambio migracion>
}