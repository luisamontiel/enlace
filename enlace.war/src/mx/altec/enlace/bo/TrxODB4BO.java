package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.TrxODB4Bean;
import mx.altec.enlace.dao.GenericDAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase para invocar la transaccion ODB4
 * @author FSW-Indra
 * @sice 26/02/2015
 */
public class TrxODB4BO extends GenericDAO {

	/**
	 * NUMERO DE CLIENTE
	 */
	private String numeroCliente;
	/**
	 * Mensaje de Estatus de la Transaccion
	 */
	private String msgStatus;
	/**
	 * Codigo de Estatus de le Transaccion
	 */
	private int codStatus;

	/**
	 * Metodo encargado de realizar la ejecuci�n de la Transaccion ODB4.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @return List<TrxODB4Bean> lista con numero de cuenta y domicilio
	 */
	public List<TrxODB4Bean> ejecuta() {
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaRespuesta;
		List<TrxODB4Bean> productos = new ArrayList<TrxODB4Bean>();
		obtenerTramaEntrada(tramaEntrada);
		EIGlobal.mensajePorTrace("TrxODB4BO - TRAMA DE ENVIO(): [" +tramaEntrada.toString()+"]", EIGlobal.NivelLog.INFO);
		tramaRespuesta = invocarTransaccion(tramaEntrada.toString());
		EIGlobal.mensajePorTrace("TrxODB4BO - TRAMA DE RESPUESTA(): [" + tramaRespuesta+"]",EIGlobal.NivelLog.INFO);
		if( tramaRespuesta.indexOf("NO EXISTE  TERMINAL ACTIVO")>-1 ) {
			procesaEstatus(99, " NO HAY TERMINALES 390 PARA ATENDER LA PETICION", tramaEntrada);
			return productos;
		} else if( tramaRespuesta.indexOf("ABEND CICS")>-1 || tramaRespuesta.indexOf("failed with abend")>-1 ) {
			procesaEstatus(99, " " + traeCodError(tramaRespuesta), tramaEntrada);
			return productos;
		} else if(tramaRespuesta.indexOf("@ER") > -1) {
			EIGlobal.mensajePorTrace("ERROR EN EL SERVICIO TrxODB4BO.ejecuta", EIGlobal.NivelLog.INFO);

			if(tramaRespuesta.indexOf("ERMPE0128") > -1) {
				procesaEstatus(11, " OPCION  ERRONEO. VALORES PERMITIDOS 1/2", tramaEntrada);
				return productos;
			} else if(tramaRespuesta.indexOf("ERMPE0013") > -1) {
				procesaEstatus(12, " NO EXISTE REGISTRO PARA EL CRITERIO DE SELECCION", tramaEntrada);
				return productos;
			} else {
				procesaEstatus(2, " ERROR EN ServicioOBD4 ", tramaEntrada);
				return productos;
			}
		} else if("".equals(tramaRespuesta.trim())) {
			EIGlobal.mensajePorTrace("Error 99", EIGlobal.NivelLog.INFO);
			procesaEstatus(99, " TIMEOUT: SIN RESPUESTA 390", tramaEntrada);
			return productos;
		} else if (tramaRespuesta.indexOf("ODA0002") > -1) {
			productos = desentramaRespuesta(tramaRespuesta);
			procesaEstatus(0, " OPERACION EFECTUADA Ejecucion Exitosa con Socket", tramaEntrada);
		}
		return productos;
	}

	/**
	 * Metodo para obtener la trama de Entrada.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void obtenerTramaEntrada(StringBuffer tramaEntrada) {

		tramaEntrada.append( armaCabeceraPS7("ODB4", "�DB4", 8, ""));
		tramaEntrada.append( rellenar(numeroCliente, 8, '0', 'I'));
		EIGlobal.mensajePorTrace("TrxODB4BO: Enviando la trama: [" + tramaEntrada.toString() + "]", EIGlobal.NivelLog.INFO);

	}
	/**
	 * Metodo para desentramar la respuesta de la trx ODB4
	 * @since 02/03/2015
	 * @author FSW-Indra
	 * @param tramaRespuesta respuesta de la trx ODB4
	 * @return List<TrxODB4Bean> lista de los productos que regresa la trx
	 */
	private List<TrxODB4Bean> desentramaRespuesta(String tramaRespuesta){
		List<TrxODB4Bean> productos = null;
		//primero quitamos todos los signos raros de la trama.
    	tramaRespuesta = tramaRespuesta.replaceAll("�", "");
    	String []regConsulta = tramaRespuesta.split("DCODMDB04 P");
    	if( regConsulta.length > 1 ){
    		int totalRegistros = regConsulta.length-1;
    		productos = new ArrayList<TrxODB4Bean>();
    		TrxODB4Bean bean = null;
    		String secuencia = "";
    		for( int i=1 ; i <= totalRegistros ; i++ ){//se inicia el recorrido por la trama para obtener los datos por registro.
    			String cadenaRegistro = regConsulta[i];
    			EIGlobal.mensajePorTrace("TrxODB4BO: regConsulta"+i+": [" + regConsulta[i] + "]", EIGlobal.NivelLog.INFO);
    			secuencia = cadenaRegistro.substring(0,3);
    			if( "TDC".equals(secuencia) ){
        			bean = new TrxODB4Bean();
    				bean.setSecDomicilio(secuencia);
    				bean.setNumCuenta(cadenaRegistro.substring(3,15).trim());
    				bean.setNumeroTdc(cadenaRegistro.substring(15,31));
    				bean.setDescComercialTdc(cadenaRegistro.substring(31, 61));
    	    		bean.setCalleYNumero(cadenaRegistro.substring(61, 111));
    	    		bean.setDescLocalidad(cadenaRegistro.substring(111, 141));
    	    		bean.setDescComunidad(cadenaRegistro.substring(141, 171));
    	    		bean.setCodigoPostal(cadenaRegistro.substring(171,179));
    	    		bean.setIndConsulta(cadenaRegistro.substring(179,180));
    	    		bean.setCodigoBloqueoTdc(cadenaRegistro.substring(180,183));
    	    		productos.add(bean);
    			}
    		}
    	}
    	return productos;
	}

	/**
	 * Metodo para realizar la extraccion del Estatus sobre el resultado de la consulta.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param codigoStatus de tipo int
	 * @param msgError de tipo String
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void procesaEstatus(int codigoStatus, String msgError, StringBuffer tramaEntrada) {
		codStatus = codigoStatus;
		msgStatus = "OBD4" + rellenar(String.valueOf(this.codStatus), 4, '0', 'I') + msgError;
		EIGlobal.mensajePorTrace("TrxODB4BO.procesaEstatus: Enviando la trama: [" + tramaEntrada.toString() + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("TrxODB4BO.procesaEstatus:msgError: ["+codigoStatus +"-"+ msgError + "]", EIGlobal.NivelLog.INFO);
	}

	/**
	 * Metodo para obtener el Codigo de Error sobre la respuesta de la ODB4.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo String
	 * @return stringbuffer de tipo stringbuffer.toString
	 */
	private String traeCodError(String tramaEntrada)
	{
		StringBuffer stringbuffer = new StringBuffer("");
		int i = tramaEntrada.indexOf("@ER");
		if(i < 0){
			i = tramaEntrada.indexOf("Transaction ODB4 failed");
		}
		if(i >= 0){
			int j = i;
			boolean continua = true;
			do{
				char c = tramaEntrada.charAt(j);
				if(c < ' ' || c > '~'){
					continua = false;
				} else if(j == (tramaEntrada.length()-1)){
					continua = false;
				}
				if( continua ){
					stringbuffer = stringbuffer.append(String.valueOf(tramaEntrada.charAt(j)));
					j++;
				}
			}while(continua);
		} else	{
			stringbuffer = stringbuffer.append("ODB40000 Ejecucion Exitosa");
		}
		return stringbuffer.toString();
	}

	/**
	 * Metodo para obtener el numero de cliente
	 * @author FSW-Indra
	 * @return String numeroCliente
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * Metodo para asignar el numero de cliente
	 * @author FSW-Indra
	 * @param numeroCliente a asignar
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * Metodo que obtiene el mensaje estatus
	 * @author FSW-Indra
	 * @return String msgStatus
	 */
	public String getMsgStatus() {
		return msgStatus;
	}

	/**
	 * Metodo que asigna el mensaje Estatus
	 * @author FSW-Indra
	 * @param msgStatus a asignar
	 */
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}

	/**
	 * Metodo que obtiene el codigo de estatus
	 * @author FSW-Indra
	 * @return String codStatus
	 */
	public int getCodStatus() {
		return codStatus;
	}

	/**
	 * Metodo para asignar valor al codigo de estatus
	 * @author FSW-Indra
	 * @param codStatus a asignar
	 */
	public void setCodStatus(int codStatus) {
		this.codStatus = codStatus;
	}

}