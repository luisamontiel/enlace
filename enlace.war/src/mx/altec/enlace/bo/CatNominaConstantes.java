package mx.altec.enlace.bo;

public class CatNominaConstantes {

	/********* VALORES TABLA EWEB_CAT_NOM_EMPL ***************/
	public static final String _CAT_NOM	= "EWEB_CAT_NOM_EMPL";
	/**
	 * Constante para la literal "ID_CAT_EMPL"
	 */
	public static final String _ID_CAT	= "ID_CAT_EMPL";
	/**
	 * Constante para la literal "CUENTA_ABONO"
	 */
	public static final String _CTA_ABO = "CUENTA_ABONO";
	/**
	 * Constante para la literal "APELL_PATERNO"
	 */
	public static final String _APE_PAT	= "APELL_PATERNO";
	/**
	 * Constante para la literal "APELL_MATERNO"
	 */
	public static final String _APE_MAT	= "APELL_MATERNO";
	/**
	 * Constante para la literal "NOMBRE"
	 */
	public static final String _NOMBRE	= "NOMBRE";
	/**
	 * Constante para la literal "REG_FED_CAUS"
	 */
	public static final String _RFC	= "REG_FED_CAUS";
	/**
	 * Constante para la literal "SEXO"
	 */
	public static final String _SEXO	= "SEXO";
	/**
	 * Constante para la literal "CVE_NACIONALIDAD"
	 */
	public static final String _CVE_NAC	= "CVE_NACIONALIDAD";
	/**
	 * Constante para la literal "EDO_CIVIL"
	 */
	public static final String _EDO_CIV	= "EDO_CIVIL";
	/**
	 * Constante para la literal "NOMBRE_CORTO"
	 */
	public static final String _NOM_CORTO	= "NOMBRE_CORTO";
	/**
	 * Constante para la literal "CALLE_NUMERO"
	 */
	public static final String _CALLE_NUM	= "CALLE_NUMERO";
	/**
	 * Constante para la literal "COLONIA";
	 */
	public static final String _COLONIA	= "COLONIA";
	/**
	 * Constante para la literal "DELEG_MUNICIPIO"
	 */
	public static final String _DELEG_MUN	= "DELEG_MUNICIPIO";
	/**
	 * Constante para la literal "CVE_ESTADO"
	 */
	public static final String _CVE_EDO	= "CVE_ESTADO";
	/**
	 * Constante para la literal "CIUDAD_POBLACION"
	 */
	public static final String _CIUDAD_POB	= "CIUDAD_POBLACION";
	/**
	 * Constante para la literal "CODIGO_POSTAL"
	 */
	public static final String _CP	= "CODIGO_POSTAL";
	/**
	 * Constante para la literal "CVE_PAIS"
	 */
	public static final String _CVE_PAIS	= "CVE_PAIS";
	/**
	 * Constante para la literal "ESTATUS_DOMICILIO"
	 */
	public static final String _STAT_DOM	= "ESTATUS_DOMICILIO";
	/**
	 * Constante para la literal "FCH_RESIDENCIA"
	 */
	public static final String _FCH_RES	= "FCH_RESIDENCIA";
	/**
	 * Constante para la literal "PREFIJO_PARTICULAR"
	 */
	public static final String _PREF_PART	= "PREFIJO_PARTICULAR";
	/**
	 * Constante para la literal "PREFIJO2"
	 */
	public static final String _PREF2	= "PREFIJO2";
	public static final String _NUM_PART	= "NUMERO_PARTICULAR";
	/**
	 * Constante para la literal "FCH_CUADRE"
	 */
	public static final String _FCH_CUADRE	= "FCH_CUADRE";
	/**
	 * Constante para la literal "FCH_ACTUAL"
	 */
	public static final String _FCH_ACT	= "FCH_ACTUAL";
	/**
	 * Constante para la literal "SUC_APE_CTA"
	 */
	public static final String _SUC_APE_CTA	= "SUC_APE_CTA";
	/**
	 * Constante para la literal "FCH_INGRESO"
	 */
	public static final String _FCH_ING	= "FCH_INGRESO";
	/**
	 * Constante para la literal "CVE_USUARIO"
	 */
	public static final String _CVE_USR	= "CVE_USUARIO";
	/**
	 * Constante para la literal "ESTATUS_EMPL"
	 */
	public static final String _STA_EMPL	= "ESTATUS_EMPL";
	/**
	 * Constante para la literal "FCH_MOD_EST"
	 */
	public static final String _FCH_MOD_EST	= "FCH_MOD_EST";
	/**
	 * Constante para la literal "OBSERVACIONES"
	 */
	public static final String _OBS	= "OBSERVACIONES";
	/**
	 * Constante para la literal "CVE_USRMOD"
	 */
	public static final String _CVE_USRMOD	= "CVE_USRMOD";
	/**
	 * Constante para la literal "OBSERVACIONES2"
	 */
	public static final String _OBS2	= "OBSERVACIONES2";
	/**
	 * Constante para la literal "ESTATUS_CTA"
	 */
	public static final String _STA_CUENTA	= "ESTATUS_CTA";
	/**
	 * Constante para la literal "SEGMENTO"
	 */
	public static final String _SEGMENTO	= "SEGMENTO";
	/**
	 * Constante para la literal "REACTIVACION"
	 */
	public static final String _REACTIVA	= "REACTIVACION";
	/**
	 * Constante para la literal "IND_KIT_BCO"
	 */
	public static final String _IND_KIT_BCO	= "IND_KIT_BCO";
	/**
	 * Constante para la literal "ESTATUS_PROCE"
	 */
	public static final String _STA_PROCE = "ESTATUS_PROCE";
	/**
	 * Constante para la literal "FCH_MOD_ESTAT_PROCE"
	 */
	public static final String _FCH_MOD_STA_PROCE = "FCH_MOD_ESTAT_PROCE";


	/********* VALORES TABLA EWEB_DET_CAT_NOM ***************/
	public static final String _DET_CAT	= "EWEB_DET_CAT_NOM";
	//public static final String _CONTR	= "CONTRATO";			// En tabla eweb_contr_cat_nom
	//public static final String _CTA_ABO	= "CUENTA_ABONO";  // En tabla eweb_cat_nom_empl
	public static final String _NUM_EMPL	= "NUM_EMPL";
	public static final String _DEPTO_EMPL	= "DEPTO_EMPL";
	public static final String _SUELDO	= "SUELDO";
	public static final String _NUM_TARJ	= "NUM_TARJETA";
	public static final String _ESTATUS	= "ESTATUS";
	public static final String _TIPO_CTA_EVER	= "TIPO_CUENTA";
	public static final String _STAT_CUENTA_EVER	= "ESTATUS_CUENTA";
	public static final String _CVE_USR_MODIF_EVER	= "CVE_USR_MODIF";
	public static final String _FCH_MODIF_REG_EVER	= "FCH_MODIF_REG";
	public static final String _FCH_MODIF_STAT_EVER	= "FCH_MODIF_ESTATUS";
	public static final String _FCH_ULT_DISP_EVER	= "FCH_ULT_DISPERSION";
	public static final String _CTA_ABO_DET	= "CUENTA_ABONO";
	public static final String _CONTR_DET	= "CONTRATO";


	/********* VALORES TABLA EWEB_DIR_CAT_NOM ***************/
//	public static final String _DIR_CAT	= "EWEB_DIR_CAT_NOM";
//	public static final String _ID_DIR	= "ID_DIR_EMPL";


	/********* VALORES TABLA EWEB_CONTR_CAT_NOM ***************/
	public static final String _CONTR_CAT	= "EWEB_CONTR_CAT_NOM";
	public static final String _CONTR	= "CONTRATO";
	public static final String _CALLE_OF	= "CALLE_NUMERO_OF";
	public static final String _COL_OF	= "COLONIA_OF";
	public static final String _DELEG_OF	= "DELEG_MUN_OF";
	public static final String _EDO_OF	= "CVE_ESTADO_OF";
	public static final String _CIUDAD_OF	= "CIUDAD_POB_OF";
	public static final String _CP_OF	= "CODIGO_POSTAL_OF";
	public static final String _PAIS_OF	= "CVE_PAIS_OF";
	public static final String _CVE_DIREC = "CVE_DIRECCION";
	public static final String _PREF_OF	= "PREFIJO";
	public static final String _NUMERO_OF	= "NUMERO";
	public static final String _EXT_OF	= "EXTENSION";

	/********* VALORES TABLA EWEB_CONTR_CAT_NOM ***************/
	public static final String _SEQ_CAT_NOM	= "EWEB_SEQ_CAT_NOM_EMPL";
	public static final String _NEXT	= "NEXTVAL";
	public static final String _NUM_SEC	= "NUM_SECUENCIA";

	/*********VALORES TABLA COMU_INTERME_FIN ******************/
	public static final String _COMU_INTERME = "COMU_INTERME_FIN";
	public static final String _NOM_BANCO = "NOMBRE_CORTO";
	public static final String _NUM_BANCO = "NUM_CECOBAN";

	/********* VALORES  ***************/
	public static final String _INA	= "I";
	/**
	 * Constante para la literal "A"
	 */
	public static final String _ACT	= "A";
	/**
	 * Constante para la literal "P"
	 */
	public static final String _PEND	= "P";
	/**
	 * Constante para la literal "TOTAL"
	 */
	public static final String _TOTAL	= "TOTAL";
	/**
	 * Constante para la literal "ERRORVACIO"
	 */
	public static final String _ERROR	= "ERRORVACIO";
	/**
	 * Constante para la literal "NOREGISTROS"
	 */
	public static final String _NOREG	= "NOREGISTROS";
	/**
	 * Constante para la literal 8
	 */
	public static final int    _TOTAL_COL	= 8;		// Datos que se presentan en el resultado de la busqueda
	/**
	 * Constante para la literal "MODIF"
	 */
	public static final String _MODIF	= "MODIF";
	/**
	 * Constante para la literal "BAJA"
	 */
	public static final String _BAJA	= "BAJA";
	/**
	 * Constante para la literal "ALTA_INTERB"
	 */
	public static final String _ALTA_INTERB	= "ALTA_INTERB";
	/**
	 * Constante para la literal "INTERB"
	 */
	public static final String _CTA_INTERB	= "INTERB";
	/**
	 * Constante para la literal "PROPIA"
	 */
	public static final String _CTA_INTERNA	= "PROPIA";
	/**
	 * Constante para la literal "SANTANDER"
	 */
	public static final String _BANCO_INTERNO	= "SANTANDER";
	/**
	 * Constante para la literal "ACTIVO"
	 */
	public static final String _DESC_ACT = "ACTIVO";
	/**
	 * Constante para la literal "PENDIENTE POR ACTIVAR"
	 */
	public static final String _DESC_PEND = "PENDIENTE POR ACTIVAR";

	/**
	 * Constante para la literal "nomina.doc"
	 */
	public static final String _ARCHIVO_NOMINA = "nomina.doc";
	/**
	 * Constante para la literal "nominaExp.doc"
	 */
	public static final String _ARCHIVO_NOMINA_EXPORTAR = "nominaExp.doc";

}