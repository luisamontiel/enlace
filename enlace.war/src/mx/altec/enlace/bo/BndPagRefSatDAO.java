package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import mx.altec.enlace.beans.BndPagRefSatBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Mejoras de Linea de Captura
 * Clase de acceso a datos creada para las banderas por tipo de pago para Pago Referenciado SAT
 * Se obtiene llave de pago la bandera de cada tipode pago:
 *            Provisional, Del Ejercicio y de Entidades Federativas.
 * @author SGSC - Indra Junio 2014
 */
public class BndPagRefSatDAO extends GenericDAO {

	/**
	 * Select para obtener el valor de la bandera para los pagos de impuestos:
	 *                  Provisional, Del Ejercicio y de Entidades Federativas.
	 */
	private static final String SELECT_BND_PRS = new StringBuilder().append(
			"select VALOR_ENTERO %n").append("from STPROD.EWEB_PARAMETROS %n").append(
			"where NOMBRE_PARAM = '%s' %n").toString();

	/**
	 * Constante para mensaje de error al cerrar conexion
	 * 
	 */
	private static final String ERROR_CONEXION = "Error al cerrar conexion: [";

	/**
	 * Se ejecuta el select para obtener el valor de la bandera para los pagos de impuestos:
	 *                                Provisional, Del Ejercicio y de Entidades Federativas.
	 * 
	 * @param bndPRSBean
	 *            Bean con las banderas de cada tipo de pago
	 * @param tipPRS
	 *            String con el Tipo de Pago de Pago Referenciado SAT
	 * @return Bean con el valor de la bandera correspondiente al Tipo de Pago
	 */
	public BndPagRefSatBean consBndPagRefSAT(
			BndPagRefSatBean bndPRSBean, String tipPRS) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String nombre = "";

		if ("001".equals(tipPRS)) {
			nombre = "PRS_PROVIS";
		} else if ("005".equals(tipPRS)) {
			nombre = "PRS_EJERCI";
		} else if ("012".equals(tipPRS)) {
			nombre = "PRS_ENTFED";
		}

		String selectFull = String.format(SELECT_BND_PRS, nombre);
		EIGlobal.mensajePorTrace(
				"BndPagRefSatDAO - consBndPagRefSAT - query ["
						+ selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format(
				"BndPagRefSatDAO consBndPagRefSAT - QRY [%s]",
				selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("Estado " + nombre + " -> " + rs.getLong(1), NivelLog.DEBUG);
				if ("001".equals(tipPRS)) {
					bndPRSBean.setBndProvis(String.valueOf(rs.getLong(1)));
				} else if ("005".equals(tipPRS)) {
					bndPRSBean.setBndEjerci(String.valueOf(rs.getLong(1)));
				} else if ("012".equals(tipPRS)) {
					bndPRSBean.setBndEntFed(String.valueOf(rs.getLong(1)));
				}
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace(
						"BndPagRefSatDAO - consBndPagRefSAT() - "
								+ ERROR_CONEXION + e1 + "]", NivelLog.ERROR);
			}
		}
		return bndPRSBean;
	}

}
