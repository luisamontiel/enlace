/*
 * pdTokenizer.java
 *
 * Created on 11 de abril de 2002, 10:05 AM
 */

package mx.altec.enlace.bo;

/** Realiza la tokenizacion de una cadena, con un determinado tipo de delimitadores
 *
 * @author Rafael Martinez
 * @version 1.0
 */
public class pdTokenizer {

    /** Cadena sobre la cual se deben realizar la tokenizacion
     */
    protected String cadena;

    /** indice en el cual inicia el siguiente token
     */
    protected int index = 0;

    /** cadena que contiene los delimitadores para las operaciones
     */
    protected String delimiters = "";

    /** opcion de construccion, 0 si se tomaran los delimitadores como espacios,
     * 1 si se toman como tokens
     * 2 si se toman como separadores de campos
     */
    protected int opt = 0;


    /** Construye un tokenizador, para obtener las partes de una cadena
     * @param cadena Es la cadena sobre la cual se realizara la operacion
     * @param delimiters son los delimitadores a utilizar en la operacion
     * @param delimitOpt es la opcion de los delimitadores, dependiendo del valor que se le pase sera como se realice la operacion
     * 0 - se trataran los delimitadores como espacios y seran saltados
     * 1 - se trataran los delimitadores como tokens, y se regresara cada delimitador individual
     * 2 - se trataran los delimitadores como separadores, y se intentara obtener un token entre cada uno de los delimitadores, por lo que una llamada a nextToken con una cadena que contenga unicamente delimitadores, regresara como valor en todas sus llamadas ""
     */
    public pdTokenizer(java.lang.String cadena, java.lang.String delimiters, int delimitOpt) {
        this.cadena = cadena;
        this.delimiters = delimiters;
        this.opt = delimitOpt;
    }

    /** Construye un tokenizador, para obtener las partes de una cadena
     * @param cadena Es la cadena sobre la cual se realizara la operacion
     * @param delimiters son los delimitadores a utilizar en la operacion
     */
    public pdTokenizer(java.lang.String cadena, java.lang.String delimiters) {
        this.cadena = cadena;
        this.delimiters = delimiters;
    }

    /** Construye un tokenizador, para obtener las partes de una cadena
     * @param cadena Es la cadena sobre la cual se realizara la operacion
     */
    public pdTokenizer(java.lang.String cadena) {
        this.cadena = cadena;
    }

    /** hace la cuenta de todos los tokens dentro de la cadena y regresa el numero de veces que se puede llamar a nextToken antes de lanzar un Exception
     * @return el numero de tokens dentro de la cadena
     */
    public int countTokens() {

        int count=0;
		int tmpIndex = index;
        /*contar los tokens*/
        for(;index < cadena.length();){

            String token = "";
            try{
                token = nextToken();
            } catch (Exception e){
                return count;
            }
            switch(opt){
                case 0:{//DELIMIT_AS_SPACE
                }
                case 1:{//DELIMIT_AS_TOKEN
                }
                case 2:{//DELIMIT_AS_SEPARATOR

                }
            }
			++count;

        }
        index= tmpIndex;
        return count;
    }

    /** obtiene el siguiente token en la cadena
     * @return el siguiente token
     * @throws Exception en caso de que se haga una llamada y ya no haya mas tokens en la cadena
     */
public    String nextToken() throws Exception {

        String ret = "";

        int length=cadena.length();
        if(!(index < cadena.length())){
            throw new Exception("No more tokens in string\n");
        }
        int i=0;
        for(;i < delimiters.length();++i){
            /*calcular la pposicion del delimitador*/
            int end=cadena.indexOf(delimiters.charAt(i),index);
            end = end < 0 ? length : end;
            /*si la longitud encontrada es menor a las anteriores,
               usar la nueva*/
            if( (end - index) < length ){
                length = end - index;
            }
        }

        /*avanzar al siguiente token*/
        switch(opt){
            case 2:{//DELIMIT_AS_SEPARATOR
                ret = cadena.substring(index,index+length);
                index += length +1;
                break;
            }
            case 0:{//DELIMIT_AS_SPACE
                if(length > 0) {
                    ret = cadena.substring(index,index+length);
                }
                index += length;
                /*recorrer todos los espacios posibles*/
				if(cadena.length() > index){
	                boolean isSpace=true;
		            do{
			            for(i=0;i<delimiters.length();){
				            if(delimiters.charAt(i) == cadena.charAt(index)){
					            ++index;
						        break;
							} else {/*aseurar que solo se revise dentro de los delimitadores
								        y el valor de i sea el ultimo delimitador al
									    salir del ciclo*/
	                            ++i;
		                    }
			            }
				        /*si el delimitador no se encuentra en el la cadena en
					        la posicion de index, entonces index es el comienzo
						    del siguiente token*/
	                    if(!(delimiters.charAt(i) == cadena.charAt(index))){
		                    isSpace = false;
			            }
				     /*mientras continuen siendo espacios, avanza*/
					}while(isSpace);
				}

                break;
            }
            case 1:{//DELIMIT_AS_TOKEN
                ret = cadena.substring(index,index +1);
                ++index;
                break;
            }
        }
        return ret;
    }

    /** obtiene el siguiente token en la cadena
     * @param delimiters es una cadena que contiene los caracteres a utilizar como delimitadores
     *          llamadas subsecuentes a nextToken() utilizaran esta cadena como su delimitador
     * @return el siguiente token
     * @throws Exception en caso de que se haga una llamada y ya no haya mas tokens en la cadena
     */
    public String nextToken(String delimiters) throws Exception {
        this.delimiters = delimiters;
        return this.nextToken();
    }
}
