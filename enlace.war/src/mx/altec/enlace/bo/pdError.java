/*
 * pdError.java
 *
 * Created on 11 de abril de 2002, 04:51 PM
 */

package mx.altec.enlace.bo;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class pdError implements java.io.Serializable {

    /** Descripci�n del error */
    protected String Error;

    /** L�nea del error */
    protected long Linea;

    /** Creates new pdError */
    public pdError () {
        Error = "";
        Linea = -1;
    }

    /**
     * Crea un pdError
     * @param linea Linea donde se localiza el error
     * @param error Descripcion del error
     */
    public pdError (long linea, String error) {
        Linea = linea;
        Error = error;
    }

    /**
     * Metodo para obtener la descripci�n del error
     * @return Descripci�n del error
     */
    public String getError () {return Error;}

    /**
     * Metodo para obtener la linea donde se localiz� el error
     * @return N�mero de l�nea del error
     */
    public long getLinea () {return Linea;}

}
