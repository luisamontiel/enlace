package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

public class EI_Cuenta implements Serializable
{
	String cuenta="";
	String periodicidad="";
	String descripcion="";
	String saldoOp01="";
	String saldoOp02="";
	String invAuto="";
	String cuentaPadre="";
	String horario="";

	int tipoNivel=0;
	int numNivel=0;

	//TipoNivel: 1 ... n
	//NumNivel: 0-Padre,1_Hijo

	public String getNivelDescripcion()
	 {
		return (numNivel==0)? "Padre":"Hijo" + " Nivel "+Integer.toString(tipoNivel);
	 }

	public String getTramaSer()
	 {
		return cuenta+"@"+cuentaPadre+"@"+descripcion+"@";
	 }

}