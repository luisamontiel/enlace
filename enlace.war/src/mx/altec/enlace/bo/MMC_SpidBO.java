/**
 * ISBAN M�xico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * mx.altec.enlace.bo.MMC_SpidBO.java
 *
 * Control de versiones:
 *
 * Version 	Date	 	By 		        Company 	Description
 * ------- 	------  	-------------   -----------  ------------------------------
 * 1.0		12-16		M Fuentes	    IDS			   Creaci�n
 */

package mx.altec.enlace.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.MMC_SpidBean;
import mx.altec.enlace.dao.MMC_SpidDAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * BO para la gestion de informacion bancos SPID.
 * 
 * @author FSW IDS :::KS-JMFR:::
 */
public class MMC_SpidBO {
	
    /** Caracter de signo menos reutilizable. */
    private static final char CHAR_MENOS = '-';

    /** Caracter de signo mas reutilizable. */
    private static final char CHAR_MAS = '+';
    
    /** Caracter de signo ? reutilizable. */
    private static final char CHAR_SEP_ARRAY = '?';

    /** Caracter de signo @ reutilizable. */
    private static final char CHAR_SEP_ARRAY_AUX = '@';
    
    /** Constante ERROR*/
    private static final String ERROR = "MMC_SpidBO - Error al realizar consulta en BD";
    
    /** Constante RESULT*/
    private static final String RESULT_OK = "MMC_SpidBO - DAO result OK";
    
	/**
	 * DAO para obtener datos de BD
	 */
	private final transient MMC_SpidDAO spidDAO = new MMC_SpidDAO();
	
	/**
	 * Metodo para consultar bancos SPID
	 * FSW IDS :::KS-JMFR:::
	 * @param strOption de tipo StringBuffer.
	 */
	public void consultaBancosSPID(StringBuffer strOption){
		EIGlobal.mensajePorTrace("MMC_SpidBO - consultaBancosSPID(): INI", EIGlobal.NivelLog.INFO);
		boolean result = false;
		List<MMC_SpidBean> spidList = new ArrayList<MMC_SpidBean>();
		try {
			result = spidDAO.getBancosUSD(spidList);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(ERROR, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		
		if(result){
			EIGlobal.mensajePorTrace(RESULT_OK, EIGlobal.NivelLog.INFO);
			crearListadoSPID(spidList,strOption);//armando trama para combo dinamico
		}
		
		EIGlobal.mensajePorTrace("MMC_SpidBO - consultaBancosSPID(): END", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para consultar bancos
	 * FSW IDS :::KS-JMFR:::
	 * @param strOption de tipo StringBuffer.
	 */
	public void consultaBancos(StringBuffer strOption){
		EIGlobal.mensajePorTrace("MMC_SpidBO - consultaBancos(): INI", EIGlobal.NivelLog.INFO);
		boolean result = false;
		List<MMC_SpidBean> spidList = new ArrayList<MMC_SpidBean>();
		try {
			result = spidDAO.getBancosMxm1(spidList);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(ERROR, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		
		if(result){
			EIGlobal.mensajePorTrace(RESULT_OK, EIGlobal.NivelLog.INFO);
			crearListadoBN(spidList,strOption);//armando trama para combo dinamico
		}
		
		EIGlobal.mensajePorTrace("MMC_SpidBO - consultaBancos(): END", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para consultar bancos USD
	 * FSW IDS :::KS-JMFR:::
	 * @param strOption de tipo StringBuffer.
	 */
	public void getBancosConfirmingSPID(StringBuffer strOption){
		EIGlobal.mensajePorTrace("MMC_SpidBO - getBancosConfirmingSPID(): INI", EIGlobal.NivelLog.INFO);
		boolean result = false;
		List<MMC_SpidBean> spidList = new ArrayList<MMC_SpidBean>();
		try {
			result = spidDAO.getBancosUSD(spidList);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(ERROR, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		
		if(result){
			EIGlobal.mensajePorTrace(RESULT_OK, EIGlobal.NivelLog.INFO);
			bancosConfirming(spidList,strOption);//armando trama para combo dinamico
		}
		
		EIGlobal.mensajePorTrace("MMC_SpidBO - getBancosConfirmingSPID(): END", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para consultar bancos MXM
	 * FSW IDS :::KS-JMFR:::
	 * @param strOption de tipo StringBuffer.
	 */
	public void getBancosConfirming(StringBuffer strOption){
		EIGlobal.mensajePorTrace("MMC_SpidBO - getBancosConfirming(): INI", EIGlobal.NivelLog.INFO);
		boolean result = false;
		List<MMC_SpidBean> spidList = new ArrayList<MMC_SpidBean>();
		try {
			result = spidDAO.getBancosMxm2(spidList);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(ERROR, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		
		if(result){
			EIGlobal.mensajePorTrace(RESULT_OK, EIGlobal.NivelLog.INFO);
			bancosConfirming(spidList,strOption);//armando trama para combo dinamico
		}
		
		EIGlobal.mensajePorTrace("MMC_SpidBO - getBancosConfirming(): END", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para consultar claves interme
	 * FSW IDS :::KS-JMFR:::
	 * @return strArray de tipo String.
	 */
	public String getConfirmingInterme(){
		EIGlobal.mensajePorTrace("MMC_SpidBO - getConfirmingInterme(): INI", EIGlobal.NivelLog.INFO);
		boolean result = false;
		StringBuffer strArray = new StringBuffer();
        strArray.append("\"0\"");
        String cveInter="";
		List<MMC_SpidBean> spidList = new ArrayList<MMC_SpidBean>();
		try {
			result = spidDAO.getBancosMxm2(spidList);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(ERROR, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		
		if(result){
			EIGlobal.mensajePorTrace(RESULT_OK, EIGlobal.NivelLog.INFO);
			for (MMC_SpidBean mmc_SpidBean : spidList) {
				cveInter = mmc_SpidBean.getCveInter() == null || mmc_SpidBean.getCveInter().isEmpty() ? 
						"" 
						: 
						mmc_SpidBean.getCveInter().replaceAll("\"", "");
				strArray.append(", \"" + cveInter.trim() +"\"");
			}
		}
		EIGlobal.mensajePorTrace("MMC_SpidBO - getConfirmingInterme(): END", EIGlobal.NivelLog.INFO);
		return strArray.toString();
	}
	
	/**
	 * Metodo para crear el HTML option
	 * cveInter+cecoban-nombre?nombre@
	 * FSW IDS :::KS-JMFR:::
	 * @param strOpcion de tipo StringBuffer.
	 * @param spidList de tipo List<MMC_SpidBean>.
	 */
	private void crearListadoSPID(List<MMC_SpidBean> spidList, StringBuffer strOpcion){
		EIGlobal.mensajePorTrace("MMC_SpidBO - crearListadoSPID(): INI", EIGlobal.NivelLog.INFO);
		String cveInter="";
		String nombre = "";
		String cecoban = "";
		EIGlobal.mensajePorTrace("MMC_SpidBO - creando listado para combobox", EIGlobal.NivelLog.INFO);
		for (MMC_SpidBean mmc_SpidBean : spidList) {
			if(!"SERFI".equals(mmc_SpidBean.getCveInter()) && !"BANME".equals(mmc_SpidBean.getCveInter())){
				cveInter = mmc_SpidBean.getCveInter() == null || mmc_SpidBean.getCveInter().isEmpty() ? 
						"" 
						: 
						mmc_SpidBean.getCveInter().replaceAll("\"", "");
				nombre = mmc_SpidBean.getNombre() == null || mmc_SpidBean.getNombre().isEmpty() ? 
						"" 
						: 
						mmc_SpidBean.getNombre().replaceAll("\"", "");
				cecoban = mmc_SpidBean.getCecoban() == null || mmc_SpidBean.getCecoban().isEmpty() ? 
						"" 
						: 
						mmc_SpidBean.getCecoban().replaceAll("\"", "");
				strOpcion.append (cveInter.trim());
				strOpcion.append (CHAR_MAS);
				strOpcion.append (cecoban);
				strOpcion.append (CHAR_MENOS);
				strOpcion.append (nombre);
				strOpcion.append (CHAR_SEP_ARRAY);
				strOpcion.append (nombre);
				strOpcion.append (CHAR_SEP_ARRAY_AUX);
			}
		}
		EIGlobal.mensajePorTrace("MMC_SpidBO - crearListadoSPID(): END", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para crear el HTML option
	 * cveInter+cecoban-nombre?nombre@
	 * FSW IDS :::KS-JMFR:::
	 * @param strOpcion de tipo StringBuffer.
	 * @param spidList de tipo List<MMC_SpidBean>.
	 */
	private void crearListadoBN(List<MMC_SpidBean> spidList, StringBuffer strOpcion){
		EIGlobal.mensajePorTrace("MMC_SpidBO - crearListadoSPID(): INI", EIGlobal.NivelLog.INFO);
		String cveInter="";
		String nombre = "";
		String cecoban = "";
		EIGlobal.mensajePorTrace("MMC_SpidBO - creando listado para combobox", EIGlobal.NivelLog.INFO);
		for (MMC_SpidBean mmc_SpidBean : spidList) {
			if(!"SERFI".equals(mmc_SpidBean.getCveInter()) && !"BANME".equals(mmc_SpidBean.getCveInter())){
				cveInter = mmc_SpidBean.getCveInter() == null || mmc_SpidBean.getCveInter().isEmpty() ? 
						"" 
						: 
						mmc_SpidBean.getCveInter().replaceAll("\"", "");
				nombre = mmc_SpidBean.getNombre() == null || mmc_SpidBean.getNombre().isEmpty() ? 
						"" 
						: 
						mmc_SpidBean.getNombre().replaceAll("\"", "");
				cecoban = mmc_SpidBean.getCecoban() == null || mmc_SpidBean.getCecoban().isEmpty() ? 
						"" 
						: 
						mmc_SpidBean.getCecoban().replaceAll("\"", "");
				strOpcion.append (cveInter.trim());
				strOpcion.append (CHAR_MAS);
				strOpcion.append (cecoban);
				strOpcion.append (CHAR_MENOS);
				strOpcion.append (nombre);
				strOpcion.append (CHAR_SEP_ARRAY);
				strOpcion.append (nombre);
				strOpcion.append (CHAR_SEP_ARRAY_AUX);
			}			
		}
		EIGlobal.mensajePorTrace("MMC_SpidBO - crearListadoSPID(): END", EIGlobal.NivelLog.INFO);
	}
	
	
	/**
	 * Metodo para crear el HTML option
	 * cveInter?nombre@
	 * FSW IDS :::KS-JMFR:::
	 * @param strOpcion de tipo StringBuffer.
	 * @param spidList de tipo List<MMC_SpidBean>.
	 */
	private void bancosConfirming(List<MMC_SpidBean> spidList, StringBuffer strOpcion){
		EIGlobal.mensajePorTrace("MMC_SpidBO - bancosConfirming(): INI", EIGlobal.NivelLog.INFO);
		String cveInter="";
		String nombre = "";
		EIGlobal.mensajePorTrace("MMC_SpidBO - creando listado para combobox", EIGlobal.NivelLog.INFO);
		for (MMC_SpidBean mmc_SpidBean : spidList) {
			cveInter = mmc_SpidBean.getCveInter() == null || mmc_SpidBean.getCveInter().isEmpty() ? 
					"" 
					: 
					mmc_SpidBean.getCveInter().replaceAll("\"", "");
			nombre = mmc_SpidBean.getNombre() == null || mmc_SpidBean.getNombre().isEmpty() ? 
					"" 
					: 
					mmc_SpidBean.getNombre().replaceAll("\"", "");
			strOpcion.append (cveInter.trim());
			strOpcion.append (CHAR_SEP_ARRAY);
			strOpcion.append (nombre);
			strOpcion.append (CHAR_SEP_ARRAY_AUX);
		}
		EIGlobal.mensajePorTrace("MMC_SpidBO - bancosConfirming(): END", EIGlobal.NivelLog.INFO);
	}

}
