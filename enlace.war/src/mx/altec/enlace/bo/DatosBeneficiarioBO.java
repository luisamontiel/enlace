/**
 * 
 */
package mx.altec.enlace.bo;

import mx.altec.enlace.beans.DatosBeneficiarioBean;
import mx.altec.enlace.dao.DatosBeneficiarioDAO;

/**
 * Clase de negocio para los datos complementarios de la cuenta beneficiaria
 * @author FSW Indra
 *
 */
public class DatosBeneficiarioBO {
	
	/** Instancia de clase del DAO para cuentas internacionales**/
	transient DatosBeneficiarioDAO datosBeneficiarioDAO = new DatosBeneficiarioDAO();


	/**
	 * Realizar los datos de las cuentas internacionales
	 * @param contrato Numero de contrato
	 * @param cuenta Numero de la cuenta
	 * @return DatosBeneficiarioBean bean con los datos de respuesta
	 */
	public DatosBeneficiarioBean consultarDatosBenef (String contrato, String cuenta) {
		return datosBeneficiarioDAO.consultarDatosBenef(contrato, cuenta);
	}
	
	/**
	 * Actualiza los datos de la cuenta beneficiaria
	 * @param beneficiarioBean Bean con la informacion a almacenar
	 * @param contrato Contrato
	 * @param cuenta Cuenta
	 * @return boolean indicador de actualización
	 */
	public boolean almacenaDatosBenef (DatosBeneficiarioBean beneficiarioBean,
			String contrato, String cuenta) {
		return datosBeneficiarioDAO.almacenaDatosBenef(beneficiarioBean, contrato, cuenta);
	}
}
