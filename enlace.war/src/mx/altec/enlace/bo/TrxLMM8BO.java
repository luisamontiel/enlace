package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import mx.altec.enlace.dao.GenericDAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase para invocar la transaccion LMM8.
 * @author FSW-Indra
 * @sice 02/03/2015
 */
public class TrxLMM8BO extends GenericDAO {
	
	/**
	 * NUMERO DE TARJETA PAN
	 */
	private String numeroTarjeta;
	/**
	 * NUMERO DE TARJETA PAN
	 */
	private String numeroCliente;
	/**
	 * NUMERO DE TARJETA PAN
	 */
	private String codigoParticipa;
	/**
	 * NUMERO DE TARJETA PAN
	 */
	private String ordenParticipa;
	/**
	 * NUMERO DE TARJETA PAN
	 */
	private String entidad;
	/**
	 * SUCURSAL CUENTA DE CHEQUES
	 */
	private String sucursalCtaChq;
	/**
	 * CUENTA DE CHEQUES
	 */
	private String cuentaCheques;
	/**
	 * Mensaje de Estatus de la Transaccion
	 */
	private String msgStatus;
	/**
	 * Codigo de Estatus de le Transaccion
	 */
	private int codStatus;
	
	/**
	 * Metodo encargado de realizar la ejecuci�n de la Transaccion LMM8
	 * @since 02/03/2015
	 * @author FSW-Indra
	 */
	public void ejecuta() {
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaRespuesta;
		
		obtenerTramaEntrada(tramaEntrada);
		EIGlobal.mensajePorTrace("TrxLMM8BO - TRAMA DE ENVIO(): [" +tramaEntrada.toString()+"]", EIGlobal.NivelLog.INFO);
		tramaRespuesta = invocarTransaccion(tramaEntrada.toString());
		EIGlobal.mensajePorTrace("TrxLMM8BO - TRAMA DE RESPUESTA(): [" + tramaRespuesta+"]",EIGlobal.NivelLog.INFO);
		
		if( tramaRespuesta.indexOf("NO EXISTE  TERMINAL ACTIVO")>-1 ) {
			procesaEstatus(99, " NO HAY TERMINALES 390 PARA ATENDER LA PETICION", tramaEntrada);
			return;
		} else if( tramaRespuesta.indexOf("ABEND CICS")>-1 || tramaRespuesta.indexOf("failed with abend")>-1 ) {
			procesaEstatus(99, " " + traeCodError(tramaRespuesta), tramaEntrada);
			return;
		} else if( tramaRespuesta.indexOf("PAN DEBE SER NUM�RICO")>-1 || tramaRespuesta.indexOf("PAN NO EXISTE")>-1){
			procesaEstatus(1, " EL NUMERO DE LA TARJETA ES INCORRECTO LMM8", tramaEntrada);
			return;
		} else if( tramaRespuesta.indexOf("NO EXISTE CLIENTE")>-1 ){
			procesaEstatus(238, " NO EXISTE EL CLIENTE EN BASE DE DATOS LMM8", tramaEntrada);
			return;
		} else if( tramaRespuesta.indexOf("CTA.BENEFICIARIO NO EXISTE")>-1 ){
			procesaEstatus(238, " NO SE PUDO CONSULTAR EL CODIGOCLIENTE LMM8", tramaEntrada);
			return;
		} else if(tramaRespuesta.indexOf("@ER") > -1) {
			EIGlobal.mensajePorTrace("ERROR EN EL SERVICIO TrxLMM8BO.ejecuta", EIGlobal.NivelLog.INFO);

			if(tramaRespuesta.indexOf("ERMPE0128") > -1) {
				procesaEstatus(11, " OPCION  ERRONEO. VALORES PERMITIDOS 1/2", tramaEntrada);
				return;
			} else if(tramaRespuesta.indexOf("ERMPE0013") > -1) {
				procesaEstatus(12, " NO EXISTE REGISTRO PARA EL CRITERIO DE SELECCION", tramaEntrada);
				return;
			} else {
				procesaEstatus(2, " ERROR EN ServicioLMM8 ", tramaEntrada);
				return;
			}
		}
		tramaRespuesta = tramaRespuesta.substring( tramaRespuesta.indexOf("DCLMM0M81 P")+ 11,tramaRespuesta.length() );
		setNumeroCliente(	tramaRespuesta.substring(0, 8));
		setCodigoParticipa(	tramaRespuesta.substring(8, 10));
		setOrdenParticipa(	tramaRespuesta.substring(10, 13));
		setEntidad(			tramaRespuesta.substring(13, 17));
		setSucursalCtaChq(	tramaRespuesta.substring(17, 21));
		setCuentaCheques(	tramaRespuesta.substring(21, 33));
	}
	
	/**
	 * Metodo para obtener la trama de Entrada.
	 * @since 02/03/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void obtenerTramaEntrada(StringBuffer tramaEntrada) {
		
		tramaEntrada.append( armaCabeceraPS7("LMM8","NMM8",22,"00"));
		tramaEntrada.append( rellenar(numeroTarjeta, 22, ' ', 'D'));
		EIGlobal.mensajePorTrace("TrxLMM8BO: Enviando la trama: [" + tramaEntrada.toString() + "]", EIGlobal.NivelLog.INFO);
		
	}
	
	/**
	 * Metodo para realizar la extraccion del Estatus sobre el resultado de la consulta.
	 * @since 02/03/2015
	 * @author FSW-Indra
	 * @param codigoStatus de tipo int
	 * @param msgError de tipo String
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void procesaEstatus(int codigoStatus, String msgError, StringBuffer tramaEntrada) {
		codStatus = codigoStatus;
		msgStatus = "LMM8" + rellenar(String.valueOf(this.codStatus), 4, '0', 'I') + msgError;
		EIGlobal.mensajePorTrace("TrxLMM8BO.procesaEstatus: Enviando la trama: [" + tramaEntrada.toString() + "]", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para obtener el Codigo de Error sobre la respuesta de la LMM8
	 * @since 02/03/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo String
	 * @return stringbuffer de tipo stringbuffer.toString
	 */
	private String traeCodError(String tramaEntrada)
	{
		StringBuffer stringbuffer = new StringBuffer("");
		int i = tramaEntrada.indexOf("@ER");
		if(i < 0){
			i = tramaEntrada.indexOf("Transaction LMM8 failed");
		}
		if(i >= 0){
			int j = i;
			boolean continua = true;
			do{
				char c = tramaEntrada.charAt(j);
				if(c < ' ' || c > '~'){
					continua = false;
				} else if(j == (tramaEntrada.length()-1)){
					continua = false;
				}
				if( continua ){
					stringbuffer = stringbuffer.append(String.valueOf(tramaEntrada.charAt(j)));
					j++;
				}					
			}while(continua);
		} else {
			stringbuffer = stringbuffer.append("LMM80000 Ejecucion Exitosa");
		}
		return stringbuffer.toString();
	}	
	
	/**
	 * getMsgStatus de tipo String.
	 * @author FSW-Indra
	 * @return msgStatus de tipo String
	 */
	public String getMsgStatus() {
		return msgStatus;
	}

	/**
	 * setMsgStatus para asignar valor a msgStatus.
	 * @author FSW-Indra
	 * @param msgStatus de tipo String
	 */
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}

	/**
	 * getCodStatus de tipo int.
	 * @author FSW-Indra
	 * @return codStatus de tipo int
	 */
	public int getCodStatus() {
		return codStatus;
	}

	/**
	 * setCodStatus para asignar valor a codStatus.
	 * @author FSW-Indra
	 * @param codStatus de tipo int
	 */
	public void setCodStatus(int codStatus) {
		this.codStatus = codStatus;
	}

	/**
	 * Metodo que obtiene el numero de tarjeta
	 * @author FSW-Indra
	 * @return String numeroTarjeta
	 */
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	/**
	 * Metodo que asigna el numero de tarjeta
	 * @author FSW-Indra
	 * @param numeroTarjeta a asignar
	 */
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	/**
	 * Metodo que obtiene el numero de cliente NumeroCliente de tipo String.
	 * @author FSW-Indra
	 * @return String numeroCliente
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * Metodod que asinga el numero de cliente 
	 * @author FSW-Indra
	 * @param numeroCliente a asignar
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * getCodigoParticipa de tipo String.
	 * @author FSW-Indra
	 * @return codigoParticipa de tipo String
	 */
	public String getCodigoParticipa() {
		return codigoParticipa;
	}

	/**
	 * setCodigoParticipa para asignar valor a codigoParticipa.
	 * @author FSW-Indra
	 * @param codigoParticipa de tipo String
	 */
	public void setCodigoParticipa(String codigoParticipa) {
		this.codigoParticipa = codigoParticipa;
	}

	/**
	 * getOrdenParticipa de tipo String.
	 * @author FSW-Indra
	 * @return ordenParticipa de tipo String
	 */
	public String getOrdenParticipa() {
		return ordenParticipa;
	}

	/**
	 * setOrdenParticipa para asignar valor a ordenParticipa.
	 * @author FSW-Indra
	 * @param ordenParticipa de tipo String
	 */
	public void setOrdenParticipa(String ordenParticipa) {
		this.ordenParticipa = ordenParticipa;
	}

	/**
	 * getEntidad de tipo String.
	 * @author FSW-Indra
	 * @return entidad de tipo String
	 */
	public String getEntidad() {
		return entidad;
	}

	/**
	 * setEntidad para asignar valor a entidad.
	 * @author FSW-Indra
	 * @param entidad de tipo String
	 */
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	/**
	 * getSucursalCtaChq de tipo String.
	 * @author FSW-Indra
	 * @return sucursalCtaChq de tipo String
	 */
	public String getSucursalCtaChq() {
		return sucursalCtaChq;
	}

	/**
	 * setSucursalCtaChq para asignar valor a sucursalCtaChq.
	 * @author FSW-Indra
	 * @param sucursalCtaChq de tipo String
	 */
	public void setSucursalCtaChq(String sucursalCtaChq) {
		this.sucursalCtaChq = sucursalCtaChq;
	}

	/**
	 * getCuentaCheques de tipo String.
	 * @author FSW-Indra
	 * @return cuentaCheques de tipo String
	 */
	public String getCuentaCheques() {
		return cuentaCheques;
	}

	/**
	 * setCuentaCheques para asignar valor a cuentaCheques.
	 * @author FSW-Indra
	 * @param cuentaCheques de tipo String
	 */
	public void setCuentaCheques(String cuentaCheques) {
		this.cuentaCheques = cuentaCheques;
	}
}