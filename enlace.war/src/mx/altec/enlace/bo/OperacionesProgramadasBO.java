package mx.altec.enlace.bo;

import mx.altec.enlace.beans.OperacionesProgramadasBean;
import mx.altec.enlace.dao.ConfigMancDAO;
import mx.altec.enlace.dao.OperacionesProgramadasDAO;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaHandler;

/**
 * Clase encargada de Validar y dar de Alta las operaciones Programadas.
 * @author FSW-Indra, Optimizacion Pyme
 * @sice 11/02/2015
 *
 */
public class OperacionesProgramadasBO {
	/**
	 * Constante para la literal VALUE=
	 */
	public static final String VALUE_IGUAL = " VALUE=";

	/**
	 * Metodo para realizar el Alta de una operacion programada Interbancaria.
	 * @since 11/02/2015
	 * @author FSW-Indra
	 * @param beanEntrada de tipo OperacionesProgramadasBean
	 * @return boolean para determinar si se realiz� el Alta correctamente.
	 */
	public boolean altaOperacionesInterban(OperacionesProgramadasBean beanEntrada,HttpServletRequest req,int contador){
		OperacionesProgramadasDAO clase = new OperacionesProgramadasDAO();

		if("EXITO".equals(clase.altaOperacionProgramada(beanEntrada))){
			 EIGlobal.mensajePorTrace("Operacion Programada" +beanEntrada.getTipoOperacion(),EIGlobal.NivelLog.ERROR);

			if("DIBT".equals(beanEntrada.getTipoOperacion())){
				BaseServlet baseServlet = new BaseServlet();
				HttpSession sess = req.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");
				BitaHelper bh = new BitaHelperImpl(req, session, req.getSession());
				int referenciaBit = baseServlet.obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
				EIGlobal.mensajePorTrace("Referencia Bit Operacion Programada" +referenciaBit,EIGlobal.NivelLog.ERROR);
				sess.setAttribute("referenciaBitProgramada"+contador, referenciaBit);
				double importe=Double.parseDouble(beanEntrada.getImporte());
				bitacoraTCT(bh, session.getUserID8().toString(),referenciaBit,beanEntrada.getConcepto(),beanEntrada.getCuenta1(),beanEntrada.getCuenta2(),importe);
			}
			return true;

		}

		return false;
	}

	/**
	 * Metodo para agregar el campo y la imagen de calendario a la pantalla.
	 * @since 12/02/2015
	 * @author FSW-Indra
	 * @param nombre_campo de tipo String
	 * @param formatoFecha de tipo String
	 * @return regresa al servlet el campo y la imagen para programar la operacion
	 */
	public StringBuffer obtieneCamposFechaProgramada(String nombre_campo, String formatoFecha) {
		StringBuffer campoCalendario = new StringBuffer();
			campoCalendario.append("<INPUT ID=");
			campoCalendario.append(nombre_campo);
			campoCalendario.append(" TYPE=TEXT SIZE=15 class=tabmovtexbol NAME=");
			campoCalendario.append(nombre_campo);
			campoCalendario.append(VALUE_IGUAL);
			campoCalendario.append(formatoFecha);
			campoCalendario.append(" OnFocus=\"blur();\">");

			campoCalendario.append("<A HREF=\"javascript:WindowCalendar();\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" width=12 height=14 border=0 align=absmiddle></A>");

		return campoCalendario;
	}

	/**
	 * Metodo para obtener los valores ocultos DIA, MES y ANIO para calendario.
	 * @since 12/02/2015
	 * @author FSW-Indra
	 * @param dia de tipo String
	 * @param mes de tipo String
	 * @param anio de tipo String
	 * @param n identificador
	 * @return regresa valores ocultos para el calendario
	 */
	public StringBuffer obtieneOcultosFechaProgramada(String dia, String mes, String anio, String n) {
		StringBuffer camposFecha = new StringBuffer();

		camposFecha.append("<INPUT ID=dia").append(n).append("  TYPE=HIDDEN NAME=dia").append(n).append(VALUE_IGUAL)
			.append(dia)
			.append(" SIZE=2 MAXLENGTH=2> ")

			.append("<INPUT ID=mes").append(n).append("  TYPE=HIDDEN NAME=mes").append(n).append(VALUE_IGUAL)
			.append(mes)
			.append(" SIZE=2 MAXLENGTH=2> ")

			.append("<INPUT ID=anio").append(n).append(" TYPE=HIDDEN NAME=anio").append(n).append(VALUE_IGUAL)
			.append(anio)
			.append(" SIZE=4 MAXLENGTH=4> ");

	return camposFecha;
	}

	/**
	 * Metodo para realizar la Mancomunidad del Contrato.
	 * @since 12/03/2015
	 * @author FSW-Indra
	 * @param contrato de tipo String
	 * @return booleano para determinar el tipo de contrato
	 * @throws BusinessException de tipo Exception.
	 */
	public boolean validaContratoMancomunado(String contrato) throws BusinessException{

		ConfigMancDAO configMancomunidad = new ConfigMancDAO();
		boolean esContratoMancomunado = false;
		try {
			esContratoMancomunado = configMancomunidad.esMancomunado(contrato);
		} catch (Exception e) {
			throw new BusinessException("OPEPROG - ERROR al validar Mancomunidad.", e);
		}
		return esContratoMancomunado;

	}

	/**
	 * Bitacora TCT
	 * @param bh : BitaHelper
	 * @param usuario : String
	 */
	private void bitacoraTCT(BitaHelper bh, String usuario, int referencia, String concepto,String cuentaOrigen, String cuentaDestinoFondo,double importe ) {
		EIGlobal.mensajePorTrace( "MDI_Enviar.java Entro Bitacora", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MDI_Enviar.java usuario: "+usuario, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MDI_Enviar.java referencia:"+referencia, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MDI_Enviar.java concepto:"+concepto, EIGlobal.NivelLog.DEBUG);
		BitaTCTBean beanTCT = new BitaTCTBean ();
		beanTCT = bh.llenarBeanTCT(beanTCT);
		beanTCT.setReferencia(referencia);
		beanTCT.setTipoOperacion("PROG");
		beanTCT.setCodError("PROG0000");
		beanTCT.setOperador(usuario);
		beanTCT.setCuentaOrigen(cuentaOrigen);
		beanTCT.setCuentaDestinoFondo(cuentaDestinoFondo);
		beanTCT.setImporte(importe);
		beanTCT.setConcepto(concepto);

		try {
		      BitaHandler.getInstance().insertBitaTCT(beanTCT);
		}catch(Exception e) {
		      EIGlobal.mensajePorTrace("Cambio de Imagen - Cambio de Imagen",EIGlobal.NivelLog.ERROR);
		}
	}

}