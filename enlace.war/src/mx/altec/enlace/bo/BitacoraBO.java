package mx.altec.enlace.bo;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public final class BitacoraBO {
	

	/**
	 *constructo privado por sonar 
	 */
	private BitacoraBO(){
		
	}
	/**
	 * @param request : request
	 * @param sess : session
	 * @param bitaConstansValue : bitaConstansValue
	 * @param referencia : referencia
	 * @param cuentaEnlace : cuenta enlace
	 * @param codError : codigo de error
	 * @param concepto : concepto
	 */
	public static void bitacoraTCT(HttpServletRequest request, HttpSession sess, String bitaConstansValue, 
			int referencia, String cuentaEnlace, String cuentaDestino, String codError, String concepto) {
		try{
			EIGlobal.mensajePorTrace(">>>>> bitacotaTCT: dato: " + bitaConstansValue,	EIGlobal.NivelLog.DEBUG);
			HttpSession ses = request.getSession();
			BaseResource session = (BaseResource) ses.getAttribute("session");
	
		  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
		  	BitaTCTBean bean = new BitaTCTBean ();
		  	bean = bh.llenarBeanTCT(bean);
		  	bean.setReferencia(referencia);
		  	bean.setTipoOperacion(bitaConstansValue);
		  	//bean.setCuentaEnlace(cuentaEnlace);
		  	if ("".equals(cuentaEnlace)) {
		  		bean.setCuentaOrigen("0");
		  	} else {
		  		bean.setCuentaOrigen(cuentaEnlace);
		  	}
		  	if ("".equals(cuentaDestino)) {
		  		bean.setCuentaDestinoFondo("0");
		  	} else {
		  		bean.setCuentaDestinoFondo(cuentaDestino);
		  	}
		  	bean.setCodError(codError);
		  	bean.setConcepto(concepto);
		  	
		  	BitaHandler.getInstance().insertBitaTCT(bean);
    	}catch(SQLException e){
    		EIGlobal.mensajePorTrace("Error en bitacotaTCT : " + e,
					EIGlobal.NivelLog.DEBUG);
   		}catch(Exception e){
   			EIGlobal.mensajePorTrace("Error en bitacotaTCT : " + e,
					EIGlobal.NivelLog.DEBUG);
   		}
	}
	

	/**
	 * Bitacoriza tabla admin
	 * @param request : request
	 * @param sess : sess
	 * @param numBit : numBit
	 * @param tmp : tmp
	 * @param conIdToken : conIdToken
	 */
	public static  void bitacorizaAdmin(HttpServletRequest request, HttpSession sess, String numBit, BitaAdminBean tmp, boolean conIdToken) {
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
				
			bh.incrementaFolioFlujo(numBit.substring(0,4));
				
			BitaAdminBean ba = new BitaAdminBean();
			ba = tmp;
			ba = (BitaAdminBean)bh.llenarBean(ba);
			ba.setNumBit(numBit); 
			if(conIdToken) {
				ba.setIdToken(session.getToken().getSerialNumber());
			} else {
				ba.setIdToken("");
			}

			try {
				ba.setIdSesion(ba.getIdSesion().replace("-", ""));
				ba.setIdSesion(ba.getIdSesion().replace("_", ""));
				BitaHandler.getInstance().insertBitaAdmin(ba);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("BitacoraBO - Error al insertar en insertBitaAdmin: " + e, EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("BitacoraBO - Error al insertar en insertBitaAdmin: " + e, EIGlobal.NivelLog.ERROR);
			}
		}
	}
	
	
	

}
