 /*I05-0382520 Praxis
 *ivn Ma. Isabel Valencia Navarrete
 *Se modifica en la funci�n leeImportaci�n(S) agregando lineas para que
 *no se reporten errores falsos*/

package mx.altec.enlace.bo;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.util.StringTokenizer;
import java.util.Calendar;
import java.util.GregorianCalendar;

import mx.altec.enlace.utilerias.EIGlobal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.dao.CalendarNomina;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.Global;

public class Documento implements Serializable, Comparable
	{
	// constantes para el tipo de documento
	   public static final int FACTURA=1;
	   public static final int OTRO=2;
	   public static final int NOTA_CARGO=3;
	   public static final int NOTA_ABONO=4;

	// constantes para la naturaleza del documento
	   public static final char DEUDORA='D';
	   public static final char ACREEDORA='A';

	// ----- ATRIBUTOS ---------------------------------------------------------

	// n�mero de documento
	   private String numdocto = "";

	// tipo de documento (FACTURA, OTRO, NOTA_CARGO, NOTA_ABONO)
	   private int tipo = 0;

	// importe neto
	   private String importe_neto = "";

	// fecha de emisi�n
	   private String fch_emis = "";

	// c�digo de proveedor al que pertenece el documento
	   private String proveedor = "";

	// tipo cuenta del proveedor (interbancaria o propia)
	   private String tipoCuenta = "";

	// clave de proveedor
	   private String claveProv = "";

	// nombre del proveedor
	   private String nombreProv = "";

	//referencia capturada por el cliente Ley de Transparencia II
	   private String referencia = "";

	//concepto capturado por el cliente Ley de Transparencia II
	   private String concepto = "";

	//Forma de aplicacion requerida por el cliente Ley de Transparencia II (SPEI)
	   private String aplicacion = "";

	// --- Solo facturas  y otros ----------------

	// importe total del documento
	   private String importe = "";

	// fecha de vencimiento
	   private String fch_venc = "";


	// --- Solo notas ----------------------------

	// naturaleza del documento (DEUDORA, ACREEDORA)
	private char naturaleza = ' ';

	// factura asociada
	private String factura_asociada = "";


	// --- Recuperados ---------------------------

	// clave de status
	private String cve_status = "";

	// status
	private String status = "";

//	@jgarcia - Stefanini - Divisa de pago
	private String divisa;

	// ----- CONSTRUCTOR -------------------------------------------------------
	/** Crea un Documento con todos sus campos vac�os. */
	public Documento()
	{
		super();
		limpiaCampos();
	}

	// ----- METODOS get() y set() ---------------------------------------------
	public String getnumdocto ()
	{
		return numdocto;
	}
	public void setnumdocto (String numdoc)
	{
		numdocto=numdoc;
	}

	public int gettipo ()
	{
		return tipo;
	}
	public void settipo (int tp)
	{
		tipo=tp;
	}

	public String getimporte_neto()
	{
		return importe_neto;
	}
	public void setimporte_neto(String valor)
	{
		importe_neto=valor;
	}

	public void setfch_emis (String femis)
	{
		fch_emis=femis;
	}
	public String getfch_emis ()
	{
		return fch_emis;
	}

	public void setproveedor (String prov)
	{
		proveedor=prov;
	}
	public String getproveedor()
	{
		EIGlobal.mensajePorTrace("DOCUMENTO.class => PROVEEDOR = >" +proveedor+ "<",EIGlobal.NivelLog.DEBUG);
		return proveedor;
	}

	public void setipoCuenta (String tipoCta)
	{
		tipoCuenta=tipoCta;
	}
	public String gettipoCuenta()
	{
		return tipoCuenta;
	}

	public void setclaveProv (String prov)
	{
		claveProv=prov;
	}
	public String getclaveProv()
	{
		return claveProv;
	}

	public void setnombreProv (String prov)
	{
		nombreProv=prov;
	}
	public String getnombreProv()
	{
		EIGlobal.mensajePorTrace("DOCUMENTO.class => NOMBRE = >" +nombreProv+ "<",EIGlobal.NivelLog.DEBUG);
		return nombreProv;
	}

	public void setimporte(String valor)
	{
		importe=valor;
	}
	public String getimporte()
	{
		return importe;
	}

	public void setfch_venc (String fvenc)
	{
		fch_venc=fvenc;
	}
	public String getfch_venc ()
	{
		return fch_venc;
	}

	public void setnaturaleza (char nat)
	{
		naturaleza=nat;
	}
	public char getnaturaleza ()
	{
		return naturaleza;
	}

	public String getfactura_asociada()
	{
		return factura_asociada;
	}
	public void setfactura_asociada(String valor)
	{
		factura_asociada = valor;
	}

	public String getcve_status()
	{
		return cve_status;
	}
	public void setcve_status(String valor)
	{
		cve_status=valor;
	}

	public String getstatus()
	{
		return status;
	}
	public void setstatus(String valor)
	{
		status=valor;
	}

	//Se agrego por HGV para el proyecto de Ley de Transparencia II
	public String getreferencia()
	{
		return referencia;
	}

	public void setreferencia(String referen)
	{
		referencia = referen;
	}

	public String getconcepto()
	{
		return concepto;
	}

	public void setconcepto(String concep)
	{
		concepto = concep;
	}

	public String getaplicacion()
	{
		return aplicacion;
	}

	public void setaplicacion(String aplica)
	{
		aplicacion = aplica;
	}
	//Termina HGV

	// ----- METODOS para imprimir en el jsp -----------------------------------

	/** Regresa el n�mero de documento listo para mostrarse como c�digo HTML en un JSP. */
	public String jspNumdocto()
	{
		if(numdocto.trim().equals(""))
			return "nbsp;";
		else
			return numdocto;
	}
	/** Regresa el tipo de documento listo para mostrarse como c�digo HTML en un JSP. */
	public String jspTipo()
	{
		String strNaturaleza = ((naturaleza == 'A')?("(acreedora)"):("(deudora)"));
		if(tipo == FACTURA)    return "Factura";
	    if(tipo == OTRO)       return "Otros";
		if(tipo == NOTA_CARGO) return "Nota de cargo " + strNaturaleza;
        if(tipo == NOTA_ABONO) return "Nota de cr&eacute;dito " + strNaturaleza;
		return "&nbsp;";
	}
	/** Regresa el importe neto listo para mostrarse como c�digo HTML en un JSP. */
	public String jspImporte_neto()
	{
		if(importe_neto.trim().equals(""))
			return "&nbsp;";
		else
			return formatoMoneda(importe_neto);
	}

	/** Regresa la fecha de emisi�n lista para mostrarse como c�digo HTML en un JSP. */
	public String jspFch_emis()
	{
		if(fch_emis.trim().equals(""))
			  return "&nbsp;";
		else return fch_emis;
	}

	/** Regresa el c�digo de proveedor listo para mostrarse como c�digo HTML en un JSP. */
	public String jspProveedor()
		{
		  if(proveedor.trim().equals(""))
			  return "&nbsp;";
		  else return proveedor;
	   }

	/** Regresa la clave de proveedor listo para mostrarse como c�digo HTML en un JSP. */
	public String jspClaveProv() {if(claveProv.trim().equals("")) return "&nbsp;"; else return claveProv;}

	/** Regresa en mobre de proveedor listo para mostrarse como c�digo HTML en un JSP. */
	public String jspNombreProv() {if(nombreProv.trim().equals("")) return "no disponible"; else return nombreProv;}

	/** Regresa el importe total listo para mostrarse como c�digo HTML en un JSP. */
	public String jspImporte() {if(importe.trim().equals("")) return "&nbsp;"; else return formatoMoneda(importe);}

	/** Regresa la fecha de vencimiento lista para mostrarse como c�digo HTML en un JSP. */
	public String jspFch_venc() {if(fch_venc.trim().equals("")) return "&nbsp;"; else return fch_venc;}

	/** Regresa la naturaleza del documento lista para mostrarse como c�digo HTML en un JSP. */
	public String jspNaturaleza()
		{
			if(naturaleza == 'A') return "acreedora";
			if(naturaleza == 'D') return "deudora";
			return "&nbsp;";
		}

	/** Regresa la factura asociada lista para mostrarse como c�digo HTML en un JSP. */
	public String jspFactura_asociada() {if(factura_asociada.trim().equals("")) return "&nbsp;"; else return factura_asociada;}

	/** Regresa la descripci�n el status lista para mostrarse como c�digo HTML en un JSP. */
	public String jspStatus() {if(status.trim().equals("")) return "&nbsp;"; else return status;}

	/** Regresa el status listo para mostrarse como c�digo HTML en un JSP. */
	public String jspCve_status() {if(cve_status.trim().equals("")) return "&nbsp;"; else return cve_status;}

	public String jspAplicacion()
	{
			if (aplicacion.equals("1")) return "MISMO DIA";
			if (aplicacion.equals("2")) return "DIA SIGUIENTE";
			return "MISMO DIA";
	}
	// -------------------------------------------------------------------------

	/** Auxiliar para generar formato de moneda. */
	private String formatoMoneda(String cadena)
		{
		 if(cadena.trim().equals("")) return "&nbsp;";
		 int car=cadena.indexOf(".");
		 if(car==-1) cadena += ".00";
		 while(cadena.length()-(car=cadena.indexOf(".")) < 3)
			   cadena += "0";
		 while(cadena.length()-(car=cadena.indexOf(".")) > 3)
			   cadena = cadena.substring(0,cadena.length()-1);
		 while(cadena.length()>4 && cadena.charAt(0)=='0')
			   cadena = cadena.substring(1);
		 for(car=cadena.length()-6;car>0;car-=3)
			 cadena = cadena.substring(0,car) + "," + cadena.substring(car);
		 return "$" + cadena;
		}

	/** Coloca en ceros o cadenas vac�as los campos del objeto. */
	private void limpiaCampos()
	{
		  numdocto = "";
		  tipo = 0;
		  importe_neto ="";
		  fch_emis = "";
		  proveedor = "";
		  claveProv = "";
		  nombreProv = "";
		  importe = "";
		  fch_venc = "";
		  naturaleza = ' ';
		  factura_asociada = "";
		  referencia = ""; //variable para Ley de Transparencia II HGV
		  concepto = ""; //variable para Ley de Transparencia II HGV
		  aplicacion = "";
		  cve_status = "";
		  status = "";
	}

	/**  */
	public String tramaGuarda()
		{
		StringBuffer trama = new StringBuffer();
		 trama.append(ponEscapes(numdocto)).append("|")
		  	  .append(ponEscapes("" + tipo)).append("|")
			  .append(ponEscapes(importe_neto)).append("|")
			  .append(ponEscapes(fch_emis)).append("|")
			  .append(ponEscapes(proveedor)).append("|")
			  .append(ponEscapes(claveProv)).append("|")
			  .append(ponEscapes(nombreProv)).append("|")
			  .append(ponEscapes(importe)).append("|")
			  .append(ponEscapes(fch_venc)).append("|")
			  .append(ponEscapes("" + naturaleza)).append("|")
			  .append(ponEscapes(factura_asociada)).append("|")
			  .append(ponEscapes(cve_status)).append("|")
			  .append(ponEscapes(status)).append("|");
		    return trama.toString();
		}

	/**  */
	public boolean leeGuarda(String trama)
		{
		boolean bienLeido = true;
		int pos = 0;
		try
			{
				pos = posDelPipeDivisor(trama);
				numdocto = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				tipo = Integer.parseInt(quitaEscapes(trama.substring(0,pos))); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				importe_neto = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				fch_emis = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				proveedor = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				claveProv = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				nombreProv = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				importe = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				fch_venc = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				naturaleza = quitaEscapes(trama.substring(0,pos)).charAt(0); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				factura_asociada = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				cve_status = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
				pos = posDelPipeDivisor(trama);
				status = quitaEscapes(trama.substring(0,pos)); trama = trama.substring(pos+1);
			}
		catch(Exception e)
			{
			bienLeido = false;
			limpiaCampos();
			}
		return bienLeido;
		}

	/**  */
	private String ponEscapes(String campo)
		{
		int pos = 0;
		while((pos = campo.indexOf("\\",pos)) != -1)
			{campo = campo.substring(0,pos) + "\\" + campo.substring(pos++); pos++;}
		while((pos = campo.indexOf("|",pos)) != -1)
			{campo = campo.substring(0,pos) + "\\" + campo.substring(pos++); pos++;}
		return campo;
		}

	/**  */
	private String quitaEscapes(String campo)
		{
		int pos = 0;
		while((pos = campo.indexOf("\\\\",pos)) != -1)
			{campo = campo.substring(0,pos) + campo.substring(++pos);}
		while((pos = campo.indexOf("\\|",pos)) != -1)
			{campo = campo.substring(0,pos) + campo.substring(++pos);}
		return campo;
		}

	/**  */
	private int posDelPipeDivisor(String cadena)
		{
		int pos = 0;
		if(cadena.startsWith("|")) return 0;
		while((pos = cadena.indexOf("|",pos)) != -1)
			{if(cadena.charAt(pos-1) != '\\') break; else pos++;}
		return pos;
		}

	//--------------------------------------------------------------------------

	/** Recupera la informaci�n del objeto a partir de una l�nea de un archivo de recuperaci�n. Dicho archivo
	 * es el que devuelve un servicio de tuxedo al mandar la siguiente trama: <b></b>
	 *
	 */
	public boolean leeRecuperacion(String linea, HttpServletRequest req)
	{
		// jgarcia - Stefanini - Se crea variable de sesi�n para guardar la Cuenta de Cargo
		HttpSession sess = req.getSession();
		StringBuffer buffer = new StringBuffer(linea);
		StringTokenizer tokens = null;
		String cadSeparar = buffer.toString();
		String cadDespues = "";
		int pos = 0;
		boolean bienLeido = true;
		int cuantosTokens;
		int indSepara = 0;

		try	{
			limpiaCampos();

			/*while((pos = buffer.toString().indexOf(";;"))!=-1 && buffer.length()<100)
				buffer.insert(pos+1," ");*/

			//Se quito la validacion de arriba se agrego esta porque estaba funcionando mal
			// HGCV Ley de Transparencia II
			while (indSepara>=0)
			{
				//System.out.println("Entre al While");
				indSepara=cadSeparar.indexOf(";;");
				EIGlobal.mensajePorTrace("indSepara = >" +indSepara+ "<",EIGlobal.NivelLog.DEBUG);

				if (indSepara>=0)
				{
					if((indSepara+2)>cadSeparar.length())
						cadDespues = "";
					else
						cadDespues = cadSeparar.substring(indSepara + 2, cadSeparar.length());

					cadSeparar = cadSeparar.substring(0,indSepara)+ "; ;" + cadDespues;
					EIGlobal.mensajePorTrace("Cadena 2 = >" +cadSeparar+ "<",EIGlobal.NivelLog.DEBUG);
				}
			}
			// HGCV Ley de Transparencia II

			buffer=new StringBuffer(cadSeparar);

			EIGlobal.mensajePorTrace("Cadena Tokens Nuevo = >" +buffer.toString()+ "<",EIGlobal.NivelLog.DEBUG);

			tokens = new StringTokenizer(buffer.toString(),";");
			cuantosTokens=tokens.countTokens();

			EIGlobal.mensajePorTrace("Cuantos Tokens = >" +cuantosTokens+ "<", EIGlobal.NivelLog.INFO);

			tokens.nextToken();
			tokens.nextToken();
			proveedor = tokens.nextToken();
			tipo = Integer.parseInt(tokens.nextToken());
			numdocto = tokens.nextToken();
			importe_neto = tokens.nextToken();

			if(tipo<3) {
				importe = tokens.nextToken();
			} else {
				naturaleza = tokens.nextToken().charAt(0);
			}

			fch_emis = tokens.nextToken();

			if(tipo<3)
				fch_venc = tokens.nextToken();

		    cve_status = tokens.nextToken();
			status = tokens.nextToken();

			if(cve_status.equals(" "))
				cve_status = "";

			// HGCV Ley de Transparencia II
//			jgarcia - Stefanini - Se agregan dos tokens(Cuenta de cargo y divisa)
			if (cuantosTokens > 15)
			{
				EIGlobal.mensajePorTrace("Mas de 14 Tokens", EIGlobal.NivelLog.INFO);
				referencia = tokens.nextToken();
				concepto = tokens.nextToken();
			}

			aplicacion = tokens.nextToken();
			EIGlobal.mensajePorTrace("Aplicacion: "+aplicacion , EIGlobal.NivelLog.INFO);
//			jgarcia - Stefanini - Obtenci�n de la divisa y la cuenta de cargo
			String cuentaCargo = tokens.nextToken();
			divisa = tokens.nextToken();
			if(null != cuentaCargo)
				sess.setAttribute("cuentaCargo", cuentaCargo);
			else
				sess.setAttribute("cuentaCargo", "");
			//jgarcia 14/Oct/2009 - Se agrega un token(Forma de pago)
			tipoCuenta = tokens.nextToken();

			// HGCV Ley de Transparencia II
		} catch(Exception e) {
			limpiaCampos();
			bienLeido = false;
		}
		return bienLeido;
	}

	/** Lee una l�nea de importaci�n. Si la l�nea presenta errores, los datos
	 * no se toman en cuenta y se respetan los valores anteriores; entonces
	 * se regresa un vector conteniendo Strings de errores. Si no existen
	 * errores, se regresa un vector con tama�o cero.
	 * @return Un vector con Strings que describen errores que contiene la
	 * l�nea.
	 */
	public Vector leeImportacion(String linea)
	{
		EIGlobal.mensajePorTrace("Entre a leeImportacion() de Documento.java",EIGlobal.NivelLog.DEBUG);
		String tipoLinea = "";
		String tipoDoc = "";
		proveedor = "";
		claveProv = "";
		nombreProv = "";
		numdocto = "";
		importe_neto = "";
		referencia = "";//HGV Ley de Transparencia II
		concepto = "";//HGV Ley de Transparencia II
		aplicacion = "";//HGV Ley de Transparencia II (SPEI)
		naturaleza = ' ';
		String emision = "";
		String vencimiento = "";
		Vector errores = new Vector();
		int intLinea = 0;
		tipo = 0;

		int emis = 0;
		int venc = 0;
		GregorianCalendar hoyC = new GregorianCalendar();
		int hoy = hoyC.get(Calendar.YEAR) * 10000 + (hoyC.get(Calendar.MONTH)+1) * 100 + hoyC.get(Calendar.DATE);

   		limpiaCampos();

		// se verifica que la l�nea no sea nula ni de longitud cero
		if(linea == null || linea.length() == 0)
		{
			errores.add("La l�nea est� vac�a ");
			return errores;
		}
        EIGlobal.mensajePorTrace(" Documento::leeImportacion::LINEA = >" +linea+ "<",EIGlobal.NivelLog.INFO);
		// se verifica que la l�nea sea de alg�n tipo de l�nea conocido
		tipoLinea = linea.substring(0, 4);
		EIGlobal.mensajePorTrace("Tipo de linea = >" +tipoLinea+ "<",EIGlobal.NivelLog.INFO);
		try {
			 intLinea = Integer.parseInt(linea.substring(0,1));
			 if(intLinea != 1 && intLinea != 2) throw new Exception("");
		} catch(Exception e) {
			errores.add("No puede especificarse a qu� tipo de documento representa la l�nea: " + tipoLinea);
			return errores;
		}

		//System.out.println(" La linea mide = >" +linea.length()+ "<");
		// se verifica que la l�nea tenga la longitud correcta

		if ((intLinea==1 && linea.length() != 69 && linea.length()!= 116 && linea.length()!= 117) ||
		   (intLinea==2 && linea.length() != 62 && linea.length()!= 109))
		{
			errores.add("La l�nea tiene una longitud no v�lida: " + linea.length());
			EIGlobal.mensajePorTrace("La linea mide (Ultima modificacion) "+ linea.length(),EIGlobal.NivelLog.INFO);
			//I05-0382520 inicio lineas agregadas
			claveProv = linea.substring(1,21).trim();
			EIGlobal.mensajePorTrace("Clave del proveedor " + claveProv,EIGlobal.NivelLog.INFO);
			tipoDoc = linea.substring(21,24).trim();
			EIGlobal.mensajePorTrace("Tipo de doc " + tipoDoc,EIGlobal.NivelLog.INFO);
			numdocto = linea.substring(24,32).trim();
			EIGlobal.mensajePorTrace("Numero de documento " + numdocto,EIGlobal.NivelLog.INFO);
			importe_neto = linea.substring(32,53).trim();
			EIGlobal.mensajePorTrace("Importe neto " + importe_neto,EIGlobal.NivelLog.INFO);
			//I05-0382520 fin lineas agregadas
			 return errores;
		}

		// se leen los dem�s campos
		claveProv = linea.substring(1,21).trim();
		EIGlobal.mensajePorTrace(" Clave del proveedor " + claveProv,EIGlobal.NivelLog.INFO);
		tipoDoc = linea.substring(21,24).trim();
		EIGlobal.mensajePorTrace(" Tipo de doc " + tipoDoc,EIGlobal.NivelLog.INFO);
		numdocto = linea.substring(24,32).trim();
		EIGlobal.mensajePorTrace(" Numero de documento " + numdocto,EIGlobal.NivelLog.INFO);
		importe_neto = linea.substring(32,53).trim();
		EIGlobal.mensajePorTrace(" Importe neto " + importe_neto,EIGlobal.NivelLog.INFO);

		if(intLinea == 1)
		{
			if (linea.length() == 69 )
			{
				emision = linea.substring(53,61).trim();
				vencimiento = linea.substring(61,69).trim();
			} else {
				emision = linea.substring(53,61).trim();
				vencimiento = linea.substring(61,69).trim();
				referencia = linea.substring(69,76).trim();//HGCV Ley de Transparencia II
				concepto = linea.substring(76,116);//HGCV Ley de Transparencia II
				/*
				 * jgarcia 12/Oct/2009
				 * Forma de aplicacion ya no se utiliza
				 */
//				aplicacion = linea.substring(116,117);//HGCV Ley de Transparencia II (SPEI)
				aplicacion = "";
			}
		}

		if(intLinea != 1)
		{
			if (linea.length() == 62 )
			{
				naturaleza = linea.charAt(53);
				emision = linea.substring(54,62).trim();
			} else {
				naturaleza = linea.charAt(53);
				emision = linea.substring(54,62).trim();
				referencia = linea.substring(62,69).trim();//HGCV Ley de Transparencia II
				concepto = linea.substring(69,109);//HGCV Ley de Transparencia II
				/*
				 * jgarcia 12/Oct/2009
				 * Forma de aplicacion ya no se utiliza
				 */
//				aplicacion = linea.substring(109,110);//HGCV Ley de Transparencia II (SPEI)
				aplicacion = "";
			}
		}

		EIGlobal.mensajePorTrace(" Referencia " + referencia,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(" Concepto " + concepto,EIGlobal.NivelLog.INFO);

		// se comprueba el num de docto
		if(numdocto.equals(""))
			errores.add("El n�mero de documento es obligatorio");

		// se comprueba la fecha de emisi�n
		if(!validaFecha(emision))
			errores.add("La fecha de emisi�n no es v�lida: " + emision);
		/**
		else if(esDiaInhabil(emision))
			errores.add("Fecha de emisi�n no es un d�a h�bil : " + emision);
		*/
		else
			emis = Integer.parseInt(emision.substring(4,8) + emision.substring(2,4) + emision.substring(0,2));
		if(emis > hoy)
			errores.add("La fecha de emisi�n es mayor a la de hoy: " + emision);

		// se comprueba el tipo de documento
		try {
			tipo = Integer.parseInt(tipoDoc);
			// validaciones para facturas y otros
			if(intLinea == 1)
				{
				 if(tipo != FACTURA && tipo != OTRO)
					errores.add("El tipo de doc. no concuerda con el tipo de l�nea: " + tipoDoc);
				 if(!validaFecha(vencimiento))
					errores.add("La fecha de vencimiento no es v�lida: " + vencimiento);
				 /**
				 else if(esDiaInhabil(vencimiento))
					errores.add("Fecha de vencimiento no es un d�a h�bil : " + vencimiento);
				 */
				 else
					venc = Integer.parseInt(vencimiento.substring(4,8) + vencimiento.substring(2,4) + vencimiento.substring(0,2));
				if(emis>venc && venc != 0)
					errores.add("La fecha de emisi�n es mayor que la de vencimiento");
				if(hoy>venc && venc != 0)
					errores.add("El documento est� vencido");
				}
		// validaciones para notas
		// Incidencia IM19607
		// if(tipo != NOTA_CARGO && tipo != NOTA_ABONO)
		//   errores.add("El tipo de doc. no concuerda con el tipo de l�nea: " + tipoDoc);
		//	else if(intLinea == 2)
		//		 {
        //	      if(tipo == NOTA_CARGO || tipo == NOTA_ABONO)
	    // 	         errores.add("Opcion invalida , por el momento no se permiten importar Notas de Cargo o Notas de Credito " + tipoDoc);
	    //	      if(naturaleza != 'A' && naturaleza != 'D')
	    //		     errores.add("La naturaleza no es v�lida: " + naturaleza);
		//		 }
		//	    else {
		//		       throw new Exception("");
		//		     }
		}catch(Exception e){
			                errores.add("El tipo de documento no es v�lido: " + tipoDoc);
			               }

		// se comprueba el importe
		if(!validaImporte(importe_neto))
			{errores.add("El importe no es v�lido: " + importe_neto);}
		if(importe_neto.length() == 0)
			{errores.add("El importe es obligatorio " + importe_neto);}

		//se comprueba la referencia y el concepto para proyecto de Ley de Transparencia II
		if (!validaNumerico(referencia))
		{
			errores.add("La Referencia debe ser num�rica");
		}
		if (!validaAlfa(concepto))
		{
			errores.add("El concepto debe ser alfanum�rico, sin caracteres especiales");
		}
		// si no hubo errores, se formatean los campos necesarios
		if(errores.size()==0)
			{
			 this.fch_emis = emision.substring(0,2) + "/" + emision.substring(2,4) + "/" + emision.substring(4);
			 importe_neto = ponPuntoDecimal(importe_neto);
			  if(intLinea == 1){
				   this.importe = this.importe_neto;
				   this.fch_venc = vencimiento.substring(0,2) + "/" +
					    vencimiento.substring(2,4) + "/" + vencimiento.substring(4);
				 }
			else {
				  this.factura_asociada = "";
				 }

			if (referencia == null || referencia == "")
			{
				referencia = "0";
			}
			cve_status = "";
			status = "";
			}
	    return errores;
	 }

	/** valida una fecha de un String, con formato 'ddmmyyyy' */
	private boolean validaFecha(String fecha)
	{
	boolean valido = true;
	boolean esBisiesto = false;
	try
		{
		int dia, mes, anio;
		dia = Integer.parseInt(fecha.substring(0,2));
		mes = Integer.parseInt(fecha.substring(2,4));
		anio = Integer.parseInt(fecha.substring(4,8));
		esBisiesto = ((anio%4 == 0 && anio%100 != 0) || anio%400 == 0);
		switch(mes)
			{
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				if(dia>31) valido = false;
				break;
			case 4: case 6: case 9: case 11:
				if(dia>30) valido = false;
				break;
			case 2:
				if(esBisiesto && dia>29 || !esBisiesto && dia>28)
					valido = false;
				break;
			default:
				valido = false;
			}
		}
	catch(Exception e)
		{valido = false;}
	return valido;
	}

	/**
	 * Metodo para validar dias inhabiles
	 * @param  fecha		Fecha a validar
	 * @return valDiaInh	Regresa true  - si es fecha inhabil
	 * 								false - si la fechas es correcta
	 */
	private boolean esDiaInhabil(String fecha){
		Calendar l = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date fechaSabDom =  null;
		Vector diasIn = new Vector();
		StringBuffer fec = new StringBuffer();
		String dia = "";
		String mes = "";
		String anio = "";
		boolean esDiaInh = false;
		try{
			dia = fecha.substring(0,2);
			mes = fecha.substring(2,4);
			anio = fecha.substring(4,8);

			fechaSabDom = sdf.parse(dia + "-" + mes + "-" + anio);
			l.setTime(fechaSabDom);
			EIGlobal.mensajePorTrace("Documento::esDiaInhabil:: Fecha calendar-> " + fechaSabDom,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Documento::esDiaInhabil:: Numero de dia-> " + l.get(Calendar.DAY_OF_WEEK),EIGlobal.NivelLog.INFO);
			switch(l.get(Calendar.DAY_OF_WEEK)){
            case Calendar.MONDAY : esDiaInh = false; break;
            case Calendar.TUESDAY : esDiaInh = false; break;
            case Calendar.WEDNESDAY : esDiaInh = false; break;
            case Calendar.THURSDAY : esDiaInh = false; break;
            case Calendar.FRIDAY : esDiaInh = false; break;
            default : esDiaInh = true; break;
            }

			if(!esDiaInh){
				fec.append(fecha.substring(0,2));
				fec.append("/");
				fec.append(fecha.substring(2,4));
				fec.append("/");
				fec.append(fecha.substring(4,8));
				EIGlobal.mensajePorTrace("Documento::esDiaInhabil:: Fecha a validar->" + fec.toString(),EIGlobal.NivelLog.INFO);
				diasIn = cargarDiasInhabiles();
				for(int i=0; i<diasIn.size(); i++){
					if(fec.toString().trim().equals((diasIn.get(i)).toString().trim())){
						esDiaInh = true;
						break;
					}
				}
			}
		}catch (ParseException e) {
			EIGlobal.mensajePorTrace("Error->"+e.getMessage(),EIGlobal.NivelLog.ERROR);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("Error->"+e.getMessage(),EIGlobal.NivelLog.ERROR);
		}
		return esDiaInh;
	}

	//Funcion agregada por HGV para el proyecto de Ley de Transparencia II
	private boolean validaAlfa(String valor)
	{
		int a, total;
		boolean valida = true;
		String patron = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";

		total = valor.length();

		for(a=0;a<total && valida;a++)
		{
			if(patron.indexOf(valor.substring(a,a+1)) == -1)
				valida = false;
		}
		return valida;
	}

	//Funcion agregada por HGV para el proyecto de Ley de Transparencia II
	private boolean validaNumerico(String valor)
	{
		int a, total;
		boolean valida = true;
		String patron = "0123456789";

		total = valor.length();

		for(a=0;a<total && valida;a++)
		{
			if(patron.indexOf(valor.substring(a,a+1)) == -1)
				valida = false;
		}
		return valida;
	}

	/** valida un importe sin puntos decimales */
	private boolean validaImporte(String cadena)
		{
		String patron = "0123456789";
		int a, total;
		boolean valido = true;
		total = cadena.length();
		for(a=0;a<total;a++)
			if(patron.indexOf(cadena.substring(a,a+1))==-1)
				{ valido = false; break; }
		if(cadena.indexOf(".") != cadena.lastIndexOf(".")) valido = false;
		return valido;
		}

	/** Regresa la informaci�n del objeto en un string que representa una l�nea
	 * del archivo para enviar como parte del servicio de tuxedo para dar de
	 * alta los registros de pagos
	 */
	public String tramaEnvio()
	{
		StringBuffer trama = new StringBuffer();

		trama.append(((tipo<3)?"1":"2")+";")
			 .append(proveedor+";")
			 .append("00" + tipo + ";")
			 .append(numdocto + ";")
			 .append(importe_neto + ";")
			 .append(((tipo<3)?importe:("" + naturaleza))+";")
			 .append(quitaDiagonalesDeFecha(fch_emis) + ";");

		if(tipo == FACTURA || tipo == OTRO)
			trama.append(quitaDiagonalesDeFecha(fch_venc) + ";");

		//HGCV Ley de Transparencia II
		trama.append(referencia + ";")
		     .append(concepto + ";")
			 .append(aplicacion + ";");
		//HGCV Ley de Transparencia II

		EIGlobal.mensajePorTrace("Trama.toString = >" +trama.toString()+ "<",EIGlobal.NivelLog.INFO);
		return trama.toString();
	}

	/**@Param
	 * cuentaCargo - Cuenta a la que se le hara el cargo
	 * divisaPago - Divisa en que se va a realizar el pago
	 *
	 * Regresa la informaci�n del objeto en un string que representa una l�nea
	 * del archivo para enviar como parte del servicio de tuxedo para dar de
	 * alta los registros de pagos
	 */
	public String tramaEnvio(String cuentaCargo, String divisaPago)
	{
		StringBuffer trama = new StringBuffer();

		trama.append(((tipo<3)?"1":"2")+";")
			 .append(proveedor+";")
			 .append("00" + tipo + ";")
			 .append(numdocto + ";")
			 .append(importe_neto + ";")
			 .append(((tipo<3)?importe:("" + naturaleza))+";")
			 .append(quitaDiagonalesDeFecha(fch_emis) + ";");

		if(tipo == FACTURA || tipo == OTRO)
			trama.append(quitaDiagonalesDeFecha(fch_venc) + ";");

		//HGCV Ley de Transparencia II
		trama.append(referencia + ";")
		     .append(concepto + ";")
			 .append("" + ";");// jgarcia 13/Oct/2009 - El campo "aplicacion" ya no se utiliza.
		//HGCV Ley de Transparencia II

		/*
		 * jgarcia - Stefanini
		 * Se agrega la cuenta de cargo y la divisa que se esta manejando en la transacci�n
		 */
		trama.append(cuentaCargo + ";")
			.append(divisaPago + ";");
		EIGlobal.mensajePorTrace("Trama.toString = >" +trama.toString()+ "<",EIGlobal.NivelLog.INFO);
		return trama.toString();
	}

	/** Convierte un String de la forma 'xx/xx/xxxx' a otro sin diagonales: 'xxxxxxxxxx'. */
	private String quitaDiagonalesDeFecha(String fecha)
		{
		  if(fecha.length() < 10) return fecha;
		     return fecha.substring(0,2)+fecha.substring(3,5)+fecha.substring(6,10);
		}

	/** Regresa un String representando una l�nea del archivo de exportaci�n
	 * @param cuentaCargoEnvio  Cuenta de Cargo de la operaci�n*/
	public String tramaExportacion(String cuentaCargoEnvio)
	{
		 StringBuffer retValue = new StringBuffer ();
			 switch(tipo)
			 {
				case FACTURA:
				case OTRO:
				 retValue.append ("1")
					 .append(rellena(claveProv, 20, ' ', false))
					 .append(rellena("" + tipo, 3, '0', true))
					 .append(rellena(numdocto, 8, ' ', false))
					 .append(rellena(sinPuntoDecimal(importe_neto), 21, ' ', true))
					 .append(rellena(sinPuntoDecimal(importe), 21, ' ', true))
					 .append(quitaDiagonalesDeFecha(fch_emis))
					 .append(quitaDiagonalesDeFecha(fch_venc))
					 .append(cve_status)
					 .append(rellena(status,80,' ',false))
					 .append(rellena(referencia,7, ' ', false))
					 .append(rellena(concepto,40,' ',false))
				 	 .append(rellena(genNomApp(aplicacion),20,' ',false))
				 	 .append(rellena(cuentaCargoEnvio,20,' ',false))
				 	 .append(rellena(divisa,4,' ',false))
				 	 .append(rellena(tipoCuenta,3,'0',true));
					break;
				case NOTA_CARGO:
				case NOTA_ABONO:
				     retValue.append("2")
						 .append(rellena(claveProv, 20, ' ', false))
						 .append(rellena("" + tipo, 3, '0', true))
						 .append(rellena(numdocto, 8, ' ', false))
						 .append(rellena(sinPuntoDecimal(importe_neto), 21, ' ', true))
						 .append("" + naturaleza)
						 .append(quitaDiagonalesDeFecha(fch_emis))
						 .append(cve_status)
						 .append(rellena(status,80,' ',false));
					 break;
		}

		 return retValue.toString();
	  }

	/** A�ade caracteres de relleno
	 * @param cadena Cadena a la que se le a�anden caracreres de relleno
	 * @param lon longitud de la nueva cadena
	 * @param car caracter con el que se hace el relleno
	 * @param rellenaXizq indica por cual lado se har� el relleno: true
	 * rellena por izquierda y false por la derecha
	 * @return la nueva cadena con el relleno
	 */
	private String rellena(String cadena, int lon, char car, boolean rellenaXizq)
		{
		  int a, total = lon - cadena.length();

		  if(total < 0)
			 return cadena.substring(0,lon);

		  StringBuffer retorno = new StringBuffer(cadena);
		  if(rellenaXizq)
		     for(a=0;a < total;a++) retorno.insert(0,car);
		  else
		     for(a=0;a < total;a++) retorno.append(car);
		  return retorno.toString();
	   }
	//
	private static String sinPuntoDecimal(String moneda)
		{
		int pos = moneda.indexOf(".");
		if(pos == -1) return moneda + "00";
		while(pos+3>moneda.length()) moneda += "0";
		while(pos+3<moneda.length()) moneda = moneda.substring(0,moneda.length()-1);
		moneda = moneda.substring(0,pos) + moneda.substring(pos+1);
		while(moneda.length()>1 && moneda.startsWith("0")) moneda = moneda.substring(1);
		return moneda;
		}

	//
	private String ponPuntoDecimal(String moneda)
		{
		while(moneda.length()<3) moneda = "0" + moneda;
		int posicionPunto = moneda.length() - 2;
		return moneda.substring(0,posicionPunto) + "." + moneda.substring(posicionPunto);
		}

	/**  */
	public boolean equals(Object otro)
		{
		boolean sonIguales = false;
		if (otro.getClass().getName().equals(this.getClass().getName()))
			{
			Documento otroDocumento = (Documento)otro;
			String otroID = otroDocumento.proveedor + "|" + otroDocumento.tipo + "|" + otroDocumento.numdocto;
			String miID = proveedor + "|" + tipo + "|" + numdocto;
			sonIguales = miID.equals(otroID);
			}
		return sonIguales;
		}

	/**  */
	public int compareTo(Object otro)
		{
		int comparacion = 0;
		if(!otro.getClass().getName().equals(this.getClass().getName())) throw new ClassCastException();
		Documento otroDocumento = (Documento)otro;
		if((comparacion=proveedor.compareTo(otroDocumento.proveedor))==0)
			if((comparacion = tipo - otroDocumento.tipo)==0)
				comparacion = numdocto.compareTo(otroDocumento.numdocto);
		return comparacion;
		}



//==================================================================================================

	public static void main(String args[])
		{
		Documento x = new Documento();
		Documento y = new Documento();

		String xx;

		/* ---------- */
		x.proveedor = "12\\776|";
		x.numdocto = "1050";
		x.importe = "5250.0";
		x.importe_neto ="5000.0";
		x.fch_emis = "22/01/2003";
		x.fch_venc = "22/04/2003";
		x.tipo = FACTURA;
		x.naturaleza = 'A';
		x.factura_asociada = "xx";
		x.status = "R";
		x.cve_status = "Rechazado vilmente";
		x.claveProv = "CVE000";
		x.nombreProv = "Proveedor x";
		/* ----------- */

		System.out.println(x.tramaEnvio());
		System.out.println(x.tramaGuarda());
		xx = x.tramaGuarda();
		y.leeGuarda(xx);
		System.out.println(y.tramaEnvio());
		System.out.println(y.equals(x));
		Vector z = new Vector();
		z.add(new Documento());
		z.add(new Documento());
		z.add(new Documento());
		z.add(new Documento());
		z.add(y);
		System.out.println(""  + z.indexOf(x));
        }

	public void imprimePrueba()
	{
		 System.out.println("proveedor: " + proveedor);
		 System.out.println("numdocto: " + numdocto);
		 System.out.println("importe: " + formatoMoneda(importe));
		 System.out.println("importe_neto: " + formatoMoneda(importe_neto));
		 System.out.println("fch_emis: " + fch_emis);
		 System.out.println("fch_venc: " + fch_venc);
		 System.out.println("tipo: " + tipo + " (" + jspTipo() + ")");
		 System.out.println("naturaleza: " + naturaleza);
		 System.out.println("factura_asociada: " + factura_asociada);
		 System.out.println("status: " + status);
		 System.out.println("cve_status: " + cve_status);
	}

	public String toString()
	{
		 StringBuffer retValue = new StringBuffer();
		 retValue.append ( Documento.class.getName ()).append ("[")
		  .append ("tipo=").append ( this.tipo )
		  .append (",proveedor=").append ( this.proveedor )
		  .append (",numdocto=").append ( this.numdocto )
		  .append (",importe=").append (this.importe)
		  .append (",importe_neto=").append ( this.importe_neto )
		  .append (",fch_emis=").append ( this.fch_emis )
		  .append (",fch_venc=").append ( this.fch_venc )
		  .append (",factura_asociada=").append ( this.factura_asociada )
		  .append (",cve_status=").append ( this.cve_status )
		  .append (",status=").append ( this.status )
		  .append (",naturaleza=").append ( this.naturaleza )
		  .append (",concepto inter=").append ( this.concepto )
		  .append (",referencia inter=").append ( this.referencia )
		  .append (",forma aplicaci�n=").append ( this.aplicacion )
		  .append ("]");
		  return retValue.toString ();
	}


	/**
	 * Metodo encargado de generar el nombre de la forma aplica
	 *
	 * @param app		Generando el nombre de la aplicaci�n
	 * @return			Nombre de la forma aplica
	 */
	private String genNomApp(String app) {
		if (app.equals("1")) return "MISMO DIA";
		if (app.equals("2")) return "DIA SIGUIENTE";
		return "MISMO DIA";
	}

	public final String getDivisa() {
		if("MXN".equals(divisa) || "MN".equals(divisa))
			return "MONEDA NACIONAL";
		else if("USD".equals(divisa))
			return "DOLAR AMERICANO";
		else
			return "SIN DEFINIR";
	}

	public final String getClaveDivisa() {
			return divisa;
	}

	public final void setDivisa(String cveDivisa) {
		this.divisa = cveDivisa;
	}


	/**
	 * Metodo encargado de regresar un vector con las fechas inhabiles
	 * @return diasNoHabiles 		Vector con fechas inhabiles
	 */
	private Vector cargarDiasInhabiles() {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String dia = "";
		String mes = "";
		String anio = "";
		StringBuffer fechaJS ;
		Vector diasNoHabiles = new Vector();

		try {
			conn = new BaseServlet().createiASConn(Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select to_char(fecha, 'dd'), "
				  + "to_char(fecha, 'mm'), "
				  + "to_char(fecha, 'yyyy') "
				  + "From comu_dia_inhabil "
				  + "Where cve_pais = 'MEXI' "
				  + "And fecha>=sysdate-(365*3)";

			EIGlobal.mensajePorTrace ("Documento::cargarDiasInhabiles:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			while(rs.next()){
				dia = rs.getString(1);
				mes = rs.getString(2);
				anio = rs.getString(3);

				fechaJS=new StringBuffer("");
				fechaJS.append( dia.trim() );
				fechaJS.append("/"  );
				fechaJS.append(mes.trim() );
				fechaJS.append("/" );
				fechaJS.append(anio.trim() );

				diasNoHabiles.addElement(fechaJS.toString());

				EIGlobal.mensajePorTrace("Documento::cargarDiasInhabiles:: " + fechaJS ,EIGlobal.NivelLog.INFO);
			 }

			EIGlobal.mensajePorTrace ("Documento::cargarDiasInhabiles:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("Documento::cargarDiasInhabiles:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cargar dias inhabiles", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Documento::cargarDiasInhabiles:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"		               				
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return diasNoHabiles;
	}

}
//Antes de seleccionar esta operaci�n, debe crear � importar un archivo