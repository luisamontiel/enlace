/********************************************************************************
Archivo:	Proveedor.java
Creado:		19-Junio-2002 05:26 PM
Autor:		Hugo S�nchez Ricardez
Modificado: 29/11/2002
Version:	5.0
*********************************************************************************/
package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;
import mx.altec.enlace.utilerias.EIGlobal;

public class Proveedor implements Serializable
{

	// Declaracion de Variables
	private String Secuencia		= "";
	private String ClaveProveedor	= "";
	private String ApellidoPaterno	= "";
	private String Nombre			= "";
	private String RFC				= "";
	private String Homoclave		= "";
	private String Calle_Numero		= "";
	private String Colonia			= "";
	private String Delg_Munc		= "";
	private String Cd_Pob			= "";
	private String Estado			= "";
	private String Pais				= "";
	private String MedioConf		= "";
	private String Telefono			= "";
	private String FormaPago		= "";
	private String CodigoCliente	= "";
	private String ApellidoMaterno	= "";
	private String PersonaContacto	= "";
	private String NumeroCliente	= "";
	private String Email			= "";
	private String Lada				= "";
	private String ExtTel			= "";
	private String Fax				= "";
	private String ExtFax			= "";
	private String NumCuenta		= "";
	private String Banco			= "";
	private String Sucursal			= "";
	private String Plaza			= "";
	private String LimiteFinanc		= "";
	private String VolumenAnual_Ventas	 = "";
	private String ImporteMedio_FactEmit = "";
	private String CP					 = "";
	private String NumDeudores_Activos	 = "";
	private String status			= "";
	private String descStatus		= "";
	private String moralFisica		= "";
	private String tipoSoc			= "";
	private String LugarImpresion	= "";
    protected String FechaTransmision;	// Fecha de Transmision
	protected String FechaActualizacion;// Fecha de Actualizacion
	protected int RegRechazados;		// Registros Rechazados
	protected int RegAceptados;			// Registros Aceptados
	protected int RegTotales;			// Registros Total
	protected int lineasCorrectas = 0;	// Almacena el numero de lineas correctas en el Archivo a Importar
	protected int lineasErroneas  = 0;	// Almacena el numero de lineas erroneas en el Archivo a Importar
	protected int lineasTotales	  = 0;	// Almacena el numero de lineas totales en el Archivo a Importar

	// Constructor
	public Proveedor () {
		FechaActualizacion	= null;
		RegRechazados	= 0;
		RegAceptados	= 0;
		RegTotales		= 0;
	}

	/********************	Campos Obligatorios		********************/

	public void setClaveProveedor( String _ClaveProveedor ) {
		ClaveProveedor = _ClaveProveedor;
	}
	public String getClaveProveedor() {
		return( ClaveProveedor );
	}

	public void setApellidoPaterno( String _ApellidoPaterno ) {
		ApellidoPaterno = _ApellidoPaterno;
	}
	public String getApellidoPaterno() {
		return( ApellidoPaterno );
	}

	public void setNombre( String _Nombre ) {
		Nombre = _Nombre;
	}
	public String getNombre() {
		return( Nombre );
	}

	public void setRFC( String _RFC ) {
		RFC = _RFC;
	}
	public String getRFC() {
		return( RFC );
	}

	public void setHomoclave( String _Homoclave ) {
		Homoclave = _Homoclave;
	}
	public String getHomoclave() {
		return( Homoclave );
	}

	public void setCalle_Numero( String _Calle_Numero ) {
		Calle_Numero = _Calle_Numero;
	}
	public String getCalle_Numero() {
		return( Calle_Numero );
	}

	public void setColonia( String _Colonia ) {
		Colonia = _Colonia;
	}
	public String getColonia() {
		return( Colonia );
	}

	public void setDelg_Munc( String _Del_Munc ) {
		Delg_Munc = _Del_Munc;
	}
	public String getDelg_Munc() {
		return( Delg_Munc );
	}

	public void setCd_Pob( String _Cd_Pob ) {
		Cd_Pob = _Cd_Pob;
	}
	public String getCd_Pob() {
		return( Cd_Pob );
	}

	public void setCP( String _CP ) {
		CP = _CP;
	}
	public String getCP() {
		return( CP );
	}

	public void setEstado( String _Estado ) {
		Estado = _Estado;
	}
	public String getEstado() {
		return( Estado );
	}

	public void setPais( String _Pais ) {
		Pais = _Pais;
	}
	public String getPais() {
		return( Pais );
	}

	public void setMedioConf( String _MedioConf ) {
		MedioConf = _MedioConf;
	}
	public String getMedioConf() {
		return( MedioConf );
	}

	public void setTelefono( String _Telefono ) {
		Telefono = _Telefono;
	}
	public String getTelefono() {
		return( Telefono );
	}

	public void setFormaPago( String _FormaPago ) {
		FormaPago = _FormaPago;
	}
	public String getFormaPago() {
		return( FormaPago );
	}

	/********************	Campos NO Obligatorios		********************/
	public void setCodigoCliente( String _CodigoCliente ) {
		CodigoCliente = _CodigoCliente;
	}
	public String getCodigoCliente() {
		return( CodigoCliente );
	}

	public void setApellidoMaterno( String _ApellidoMaterno ) {
		ApellidoMaterno = _ApellidoMaterno;
	}
	public String getApellidoMaterno() {
		return( ApellidoMaterno );
	}

	public void setPersonaContacto( String _PersonaContacto ) {
		PersonaContacto = _PersonaContacto;
	}
	public String getPersonaContacto() {
		return( PersonaContacto );
	}

	public void setNumeroCliente( String _NumeroCliente ) {
		NumeroCliente = _NumeroCliente;
	}
	public String getNumeroCliente() {
		return( NumeroCliente );
	}

	public void setEmail( String _Email ) {
		Email = _Email;
	}
	public String getEmail() {
		return( Email );
	}

	public void setLada( String _Lada ) {
		Lada = _Lada;
	}
	public String getLada() {
		return( Lada );
	}

	public void setExtTel( String _ExtTel ) {
		ExtTel = _ExtTel;
	}
	public String getExtTel() {
		return( ExtTel );
	}

	public void setFax( String _Fax ) {
		Fax = _Fax;
	}
	public String getFax() {
		return( Fax );
	}

	public void setExtFax( String _ExtFax ) {
		ExtFax = _ExtFax;
	}
	public String getExtFax() {
		return( ExtFax );
	}

	public void setNumCuenta( String _NumCuenta ) {
		NumCuenta = _NumCuenta;
	}
	public String getNumCuenta() {
		return( NumCuenta );
	}

	public void setBanco( String _Banco ) {
		Banco = _Banco;
	}
	public String getBanco() {
		return( Banco );
	}

	public void setSucursal( String _Sucursal ) {
		Sucursal = _Sucursal;
	}
	public String getSucursal() {
		return( Sucursal );
	}

	public void setPlaza( String _Plaza ) {
		Plaza = _Plaza;
	}
	public String getPlaza() {
		return( Plaza );
	}

	public void setLimiteFinanc( String _LimiteFinanc ) {
		LimiteFinanc = _LimiteFinanc;
	}
	public String getLimiteFinanc() {
		return( LimiteFinanc );
	}

	public void setNumDeudores_Activos( String _NumDeudores_Activos ) {
		NumDeudores_Activos = _NumDeudores_Activos;
	}
	public String getNumDeudores_Activos() {
		return( NumDeudores_Activos );
	}

	public void setVolumenAnual_Ventas( String _VolumenAnual_Ventas ) {
		VolumenAnual_Ventas = _VolumenAnual_Ventas;
	}
	public String getVolumenAnual_Ventas() {
		return( VolumenAnual_Ventas );
	}

	public void setImporteMedio_FactEmit( String _ImporteMedio_FactEmit ) {
		ImporteMedio_FactEmit = _ImporteMedio_FactEmit;
	}
	public String getImporteMedio_FactEmit() {
		return( ImporteMedio_FactEmit );
	}

	public void setStatus( String _Status ) {
		status = _Status;
	}
	public String getStatus() {
		return( status );
	}

	public void setDescStatus( String _DescStatus ) {
		descStatus = _DescStatus;
	}
	public String getDescStatus() {
		return( descStatus );
	}

	public void setTipoSoc( String _TipoSoc ) {
		tipoSoc = _TipoSoc;
	}
	public String getTipoSoc() {
		return( tipoSoc );
	}

	public void setMoralFisica( String _MoralFisica ) {
		moralFisica = _MoralFisica;
	}
	public String getMoralFisica() {
		return( moralFisica );
	}

	public void setSecuencia( String _Secuencia) {
		Secuencia = _Secuencia;
	}

	public String getSecuencia() {
		return( Secuencia );
	}

	public void setLugarImpresion(String _LugarImpresion) {
		LugarImpresion = _LugarImpresion;
	}
	public String getLugarImpresion() {
		return( LugarImpresion );
	}

	public boolean esMoral() {
		return moralFisica.equals("M");
	}

	/********************	Datos del Archivo	********************/
	public void setFechaTransmision (String Fecha)
		{FechaTransmision = Fecha;}

	public String getFechaTransmision()
		{return FechaTransmision;}

	public void setFechaActualizacion (String Fecha)
		{FechaActualizacion = Fecha;}

    public String getFechaActualizacion()
		{return FechaActualizacion;}

	public void setRegRechazados (int _RegRechazados)
		{RegRechazados = _RegRechazados;}

	public int getRegRechazados()
		{return RegRechazados;}

	public void setRegAceptados (int _RegAceptados)
		{RegAceptados = _RegAceptados;}

	public int getRegAceptados()
		{return RegAceptados;}

	public void setRegTotales (int _RegTotales)
		{RegTotales = _RegTotales;}

	public int getRegTotales()
		{return RegTotales;}

	public void setlineasCorrectas(int _lineasCorrectas)
		{lineasCorrectas = _lineasCorrectas;}

	public int getlineasCorrectas()
		{return lineasCorrectas;}

	public void setlineasErroneas(int _lineasErroneas)
		{lineasErroneas = _lineasErroneas;}

	public int getlineasErroneas()
		{return lineasErroneas;}

	public void setlineasTotales(int _lineasTotales)
		{lineasTotales = _lineasTotales;}

	public int getlineasTotales()
		{return lineasTotales;}

	//	Cadena con los campos del proveedor separdos por un pipe (|)
	public String trama() {

		String TramaProveedor = getMoralFisica()			+ "|" +	// Persona Fisica o Moral
								getClaveProveedor()			+ "|" + // clave de Proveedor
								getApellidoPaterno()		+ "|" + // Apellido Paterno
								getApellidoMaterno()		+ "|" + // Apellido Materno
								getNombre()					+ "|" + // Nombre
								getTipoSoc()				+ "|" + // Tipo de sociedad
								getRFC()					+ "|" + // RFC
								getHomoclave()				+ "|" + // Homoclave
								getPersonaContacto()		+ "|" + // Persona de Contacto
								getNumeroCliente()			+ "|" + // Numero de Cliente
								getCalle_Numero()			+ "|" +	// Calle y numero
								getColonia()				+ "|" +	// Colonia
								getDelg_Munc()				+ "|" +	// Delegacion o municipio
								getCd_Pob()					+ "|" +	// Ciudad o poblacion
								getCP()						+ "|" + // Codigo Postal
								getEstado()					+ "|" +	// Estado
								getPais()					+ "|" +	// Pais
								getMedioConf()				+ "|" +	// Medio de Confirmacion
								getEmail()					+ "|" + // Email
								getLada()					+ "|" + // Lada
								getTelefono()				+ "|" + // Telefono
								getExtTel()					+ "|" + // Extension Telefono
								getFax()					+ "|" + // Fax
								getExtFax()					+ "|" + // Extension Fax
								getFormaPago()				+ "|" +	// Forma de pago
								getNumCuenta()				+ "|" + // Numero de Cuenta
								getBanco()					+ "|" + // Banco
								getSucursal()				+ "|" + // Sucursal
								getPlaza()					+ "|" +	// Plaza
								getLimiteFinanc()			+ "|" + // Limite de Financiamiento
								getNumDeudores_Activos()	+ "|" + // Numero de Deudores Activos
								getVolumenAnual_Ventas()	+ "|" + // Volumen Anual de Ventas
								getImporteMedio_FactEmit()	+ "|" + // Importe Medio de Facturacion Emitida
								getLugarImpresion()			+ "|" + // Lugar de Impresion
								getStatus()					+ "|" + // Estatus
								getDescStatus();				// Descripcion de Esratus
								EIGlobal.mensajePorTrace("]]]]]]]]Trama armada en proveedor :"+TramaProveedor, EIGlobal.NivelLog.INFO);

		return TramaProveedor;
	}

	// Crea una trama con la estructura adecuada para guardarlo en el Archivo de envio
	public String creaTramaEnvio(String trama, String Delimitador) {
		try
			{
			pdTokenizer tokens = new pdTokenizer(trama,Delimitador,2);

			setMoralFisica		(tokens.nextToken().trim());			// Persona Fisica o Moral
			setClaveProveedor	(tokens.nextToken().trim());			// clave de Proveedor
			setApellidoPaterno	(tokens.nextToken().trim());			// Apellido Paterno
			setApellidoMaterno	(tokens.nextToken().trim());			// Apellido Materno
			setNombre			(tokens.nextToken().trim());			// Nombre
			setTipoSoc			(tokens.nextToken().trim());			// Tipo de sociedad
			setRFC				(tokens.nextToken().trim());			// RFC
			setHomoclave		(tokens.nextToken().trim());			// Homoclave
			setPersonaContacto	(tokens.nextToken().trim());			// Persona de Contacto
			setNumeroCliente	(tokens.nextToken().trim());			// Numero de Cliente para facturaje
			setCalle_Numero		(tokens.nextToken().trim());			// Calle y numero
			setColonia			(tokens.nextToken().trim());			// Colonia
			setDelg_Munc		(tokens.nextToken().trim());			// Delegacion o municipio
			setCd_Pob			(tokens.nextToken().trim());			// Ciudad o poblacion
			setCP				(tokens.nextToken().trim());			// Codigo Postal
			setEstado			(tokens.nextToken().trim());			// Estado
			setPais				(tokens.nextToken().trim());			// Pais
			setMedioConf		(tokens.nextToken().trim());			// Medio de Confirmacion
			setEmail			(tokens.nextToken().trim());			// Email
			setLada				(tokens.nextToken().trim());			// Lada
			setTelefono			(tokens.nextToken().trim());			// Telefono
			setExtTel			(tokens.nextToken().trim());			// Extension Telefono
			setFax				(tokens.nextToken().trim());			// Fax
			setExtFax			(tokens.nextToken().trim());			// Extension Fax
			setFormaPago		(tokens.nextToken().trim());			// Forma de pago
			setNumCuenta		(tokens.nextToken().trim());			// Numero de Cuenta
			setBanco			(tokens.nextToken().trim());			// Banco
			setSucursal			(tokens.nextToken().trim());			// Sucursal
			setPlaza			(tokens.nextToken().trim());			// Plaza
			setLimiteFinanc		(tokens.nextToken().trim());			// Limite de Financiamiento
			setNumDeudores_Activos		(tokens.nextToken().trim());	// Numero de Deudores Activos
			setVolumenAnual_Ventas		(tokens.nextToken().trim());	// Volumen Anual de Ventas
			setImporteMedio_FactEmit	(tokens.nextToken().trim());	// Importe Medio de Facturacion Emitida
			setLugarImpresion	(tokens.nextToken().trim());			// Lugar de Impresion
			setStatus			(tokens.nextToken().trim());			// Estatus
			setDescStatus		(tokens.nextToken().trim());			// Descripcion de Esratus
		}
		catch(Exception e){;}

		return (generaTramaEnvio(3,";"));
	}

	// Crea un objeto Proveedor a partir de una trama que se
	// crea con el metodo trama()
	public static Proveedor creaDeTrama(String trama) {

		Proveedor ObjectProveedor = new Proveedor();
		String Persona = "";

		try	{
			pdTokenizer tokens = new pdTokenizer(trama,"|",2);

			ObjectProveedor.setMoralFisica		(Persona=tokens.nextToken());	// Persona Fisica o Moral
			ObjectProveedor.setClaveProveedor	(tokens.nextToken());	// clave de Proveedor
			ObjectProveedor.setApellidoPaterno	(tokens.nextToken());	// Apellido Paterno
			ObjectProveedor.setApellidoMaterno	(tokens.nextToken());	// Apellido Materno
			ObjectProveedor.setNombre			(tokens.nextToken());	// Nombre
			ObjectProveedor.setTipoSoc			(tokens.nextToken());	// Tipo de sociedad
			ObjectProveedor.setRFC				(tokens.nextToken());	// RFC
			ObjectProveedor.setHomoclave		(tokens.nextToken());	// Homoclave
			ObjectProveedor.setPersonaContacto	(tokens.nextToken());	// Persona de Contacto
			ObjectProveedor.setNumeroCliente	(tokens.nextToken());	// Numero de Cliente
			ObjectProveedor.setCalle_Numero		(tokens.nextToken());	// Calle y numero
			ObjectProveedor.setColonia			(tokens.nextToken());	// Colonia
			ObjectProveedor.setDelg_Munc		(tokens.nextToken());	// Delegacion o municipio
			ObjectProveedor.setCd_Pob			(tokens.nextToken());	// Ciudad o poblacion
			ObjectProveedor.setCP				(tokens.nextToken());	// Codigo Postal
			ObjectProveedor.setEstado			(tokens.nextToken());	// Estado
			ObjectProveedor.setPais				(tokens.nextToken());	// Pais
			ObjectProveedor.setMedioConf		(tokens.nextToken());	// Medio de Confirmacion
			ObjectProveedor.setEmail			(tokens.nextToken());	// Email
			ObjectProveedor.setLada				(tokens.nextToken());	// Lada
			ObjectProveedor.setTelefono			(tokens.nextToken());	// Telefono
			ObjectProveedor.setExtTel			(tokens.nextToken());	// Extension Telefono
			ObjectProveedor.setFax				(tokens.nextToken());	// Fax
			ObjectProveedor.setExtFax			(tokens.nextToken());	// Extension Fax
			ObjectProveedor.setFormaPago		(tokens.nextToken());	// Forma de pago
			ObjectProveedor.setNumCuenta		(tokens.nextToken());	// Numero de Cuenta
			ObjectProveedor.setBanco			(tokens.nextToken());	// Banco
			ObjectProveedor.setSucursal			(tokens.nextToken());	// Sucursal
			ObjectProveedor.setPlaza			(tokens.nextToken());	// Plaza
			ObjectProveedor.setLimiteFinanc		(tokens.nextToken());	// Limite de Financiamiento
			ObjectProveedor.setNumDeudores_Activos		(tokens.nextToken());	// Numero de Deudores Activos
			ObjectProveedor.setVolumenAnual_Ventas		(tokens.nextToken());	// Volumen Anual de Ventas
			ObjectProveedor.setImporteMedio_FactEmit	(tokens.nextToken());	// Importe Medio de Facturacion Emitida
			ObjectProveedor.setLugarImpresion	(tokens.nextToken());	// Lugar de Impresion
			ObjectProveedor.setStatus			(tokens.nextToken());	// Estatus
			ObjectProveedor.setDescStatus		(tokens.nextToken());	// Descripcion de Esratus

		} catch(Exception e) {;}

		return( ObjectProveedor );
	}

	public boolean equals(Proveedor p)
	{
		return getClaveProveedor().equals(p.getClaveProveedor());
	}

	// X-- fija una cadena en un ancho determinado por 'largo' -------------------------
	private String fijaCadena(String cadena, int largo) {

		if(cadena == null) return null;

		StringBuffer valor = new StringBuffer(cadena);
		String valorFinal;
		while(valor.length()<largo) valor.append(" ");
		valorFinal = valor.toString();
		if(valorFinal.length()>largo) valorFinal = valorFinal.substring(0,largo);
		return valorFinal;
	}

	// X-- devuelve una l�nea de archivo con info del objeto ---------------------------
	public String lineaArchivo() {
		StringBuffer linea;
		int longApMaterno = 30;
		int longApPaterno = 30;
		int longTipoSoc = 10;

		linea = new StringBuffer("");

		linea.append(fijaCadena(getMoralFisica(),1));					// Persona Juridica
		linea.append(fijaCadena(getClaveProveedor(),20));				// Clave de Proveedor
		if(getMoralFisica().equals("F")) {
			linea.append(fijaCadena(getApellidoPaterno(),longApPaterno));
			linea.append(fijaCadena(getApellidoMaterno(),longApMaterno));
			linea.append(fijaCadena(getNombre(),80));
		}
		else {
			linea.append(fijaCadena(getNombre(),80));
			linea.append(fijaCadena(getTipoSoc(),longTipoSoc));
		}
		linea.append(fijaCadena(getRFC(),10));
		linea.append(fijaCadena(getHomoclave(),3));
		linea.append(fijaCadena(getPersonaContacto(),80));
		linea.append(fijaCadena(getNumeroCliente(),8));
		linea.append(fijaCadena(getCalle_Numero(),60));
		linea.append(fijaCadena(getColonia(),30));
		linea.append(fijaCadena(getDelg_Munc(),35));
		linea.append(fijaCadena(getCd_Pob(),35));
		linea.append(fijaCadena(getCP(),5));
		linea.append(fijaCadena(getEstado(),4));
		linea.append(fijaCadena(getPais(),4));
		linea.append(fijaCadena(getMedioConf(),3));
		linea.append(fijaCadena(getEmail(),60));
		linea.append(fijaCadena(getLada(),12));
		linea.append(fijaCadena(getTelefono(),12));
		linea.append(fijaCadena(getExtTel(),12));
		linea.append(fijaCadena(getFax(),12));
		linea.append(fijaCadena(getExtFax(),12));
		linea.append(fijaCadena(getFormaPago(),3));
		linea.append(fijaCadena(getNumCuenta(),18));
		linea.append(fijaCadena(getBanco(),5));
		linea.append(fijaCadena(getSucursal(),4));
		linea.append(fijaCadena(getPlaza(),5));
		linea.append(fijaCadena(getLimiteFinanc(),21));
		linea.append(fijaCadena(getNumDeudores_Activos(),5));
		linea.append(fijaCadena(getVolumenAnual_Ventas(),21));
		linea.append(fijaCadena(getImporteMedio_FactEmit(),21));
		linea.append(fijaCadena(getLugarImpresion(),18));

		if(!getStatus().equals(""))	{
			linea.append(fijaCadena(getStatus(),1));
			linea.append(fijaCadena(getDescStatus(),80));
		}
		linea.append("\n");
		return linea.toString();
	}

	// --- Crea un proveedor de una l�nea de archivo
	public static Proveedor creaDeLineaArchivo(String linea) {

		if(linea == null) return null;

		int	longTipoSoc	  = 10;
		int	longApMaterno = 30;
		int	longApPaterno = 30;

		char tipo		= linea.charAt(0);

		StringBuffer lineaBuffer = new StringBuffer(linea);
		Proveedor nuevo = new Proveedor();

		nuevo.setMoralFisica(lineaBuffer.substring(0,1));					// Persona Fisica o Moral
		lineaBuffer.delete(0,1);
		nuevo.setClaveProveedor(lineaBuffer.substring(0,20));				// Clave de Proveedor
		lineaBuffer.delete(0,20);
		if(tipo == 'F') {
			nuevo.setApellidoPaterno(lineaBuffer.substring(0,longApPaterno));	// Apellido Paterno
			lineaBuffer.delete(0,longApPaterno);
			nuevo.setApellidoMaterno(lineaBuffer.substring(0,longApMaterno));	// Apellido Materno
			lineaBuffer.delete(0,longApMaterno);
			nuevo.setNombre(lineaBuffer.substring(0,80));						// Nombre
			lineaBuffer.delete(0,80);
		}
		if(tipo == 'M'){
			nuevo.setNombre(lineaBuffer.substring(0,80));						// Nombre
			lineaBuffer.delete(0,80);
			nuevo.setTipoSoc(lineaBuffer.substring(0,longTipoSoc));				// Tipo Sociedad
			lineaBuffer.delete(0,longTipoSoc);
		}
		nuevo.setRFC(lineaBuffer.substring(0,10));							// RFC
		lineaBuffer.delete(0,10);
		nuevo.setHomoclave(lineaBuffer.substring(0,3));						// HomoClave
		lineaBuffer.delete(0,3);
		nuevo.setPersonaContacto(lineaBuffer.substring(0,80));				// Persona de Contacto
		lineaBuffer.delete(0,80);
		nuevo.setNumeroCliente(lineaBuffer.substring(0,8));					// Numero de Cliente
		lineaBuffer.delete(0,8);
		nuevo.setCalle_Numero(lineaBuffer.substring(0,60));					// Calle y Numero
		lineaBuffer.delete(0,60);
		nuevo.setColonia(lineaBuffer.substring(0,30));						// Colonia
		lineaBuffer.delete(0,30);
		nuevo.setDelg_Munc(lineaBuffer.substring(0,35));					// Delegacion o Municipio
		lineaBuffer.delete(0,35);
		nuevo.setCd_Pob(lineaBuffer.substring(0,35));						// Cd o Polacion
		lineaBuffer.delete(0,35);
		nuevo.setCP(lineaBuffer.substring(0,5));							// Codigo Postal
		lineaBuffer.delete(0,5);
		nuevo.setEstado(lineaBuffer.substring(0,4));						// Estado
		lineaBuffer.delete(0,4);
		nuevo.setPais(lineaBuffer.substring(0,4));							// Pais
		lineaBuffer.delete(0,4);
		nuevo.setMedioConf(lineaBuffer.substring(0,3));						// Medio de Confirmacion
		lineaBuffer.delete(0,3);
		nuevo.setEmail(lineaBuffer.substring(0,60));						// Email
		lineaBuffer.delete(0,60);
		nuevo.setLada(lineaBuffer.substring(0,12));							// Lada
		lineaBuffer.delete(0,12);
		nuevo.setTelefono(lineaBuffer.substring(0,12));						// Telefono
		lineaBuffer.delete(0,12);
		nuevo.setExtTel(lineaBuffer.substring(0,12));						// Ext. Telefono
		lineaBuffer.delete(0,12);
		nuevo.setFax(lineaBuffer.substring(0,12));							// Fax
		lineaBuffer.delete(0,12);
		nuevo.setExtFax(lineaBuffer.substring(0,12));						// Ext. Fax
		lineaBuffer.delete(0,12);
		nuevo.setFormaPago(lineaBuffer.substring(0,3));						// Forma de Pago
		lineaBuffer.delete(0,3);
		nuevo.setNumCuenta(lineaBuffer.substring(0,18));					// Numero de Cuenta
		lineaBuffer.delete(0,18);
		nuevo.setBanco(lineaBuffer.substring(0,5));							// Banco
		lineaBuffer.delete(0,5);
		nuevo.setSucursal(lineaBuffer.substring(0,4));						// Sucursal
		lineaBuffer.delete(0,4);
		nuevo.setPlaza(lineaBuffer.substring(0,5));							// Plaza
		lineaBuffer.delete(0,5);
		nuevo.setLimiteFinanc(lineaBuffer.substring(0,21));					// Limite de Financiamiento
		lineaBuffer.delete(0,21);
		nuevo.setNumDeudores_Activos(lineaBuffer.substring(0,5));			// Numero de Deudores Activos
		lineaBuffer.delete(0,5);
		nuevo.setVolumenAnual_Ventas(lineaBuffer.substring(0,21));			// Volumen Anual de Ventas
		lineaBuffer.delete(0,21);
		nuevo.setImporteMedio_FactEmit(lineaBuffer.substring(0,21));		// Importe Medio de Facturas Emitidas
		lineaBuffer.delete(0,21);
		//nuevo.setLugarImpresion(lineaBuffer.substring(0,18));				// Lugar de Impresion
		//lineaBuffer.delete(0,18);
		/*if(lineaBuffer.length()>0) {
			nuevo.setStatus(lineaBuffer.substring(0,1));
			lineaBuffer.delete(0,1);
			nuevo.setDescStatus(lineaBuffer.substring(0,80));
			lineaBuffer.delete(0,80);
		}*/
		return nuevo;
	}
	// -- Genera la Trama que sera enviada a Tuxedo
	public String generaTramaEnvio(int Tipo, String Separador ) {
		String tramaGenerada	= "";

		if(Tipo == 0){
			tramaGenerada =								  Separador +		// Ok + Numero + Numero de Contrato
							getSecuencia()				+ Separador +		// Secuencia
						    getCodigoCliente()			+ Separador;		// Codigo del cliente
		}
		if(Tipo == 1) {
			tramaGenerada =	getSecuencia()				+ Separador +		// Secuencia
						    getCodigoCliente()			+ Separador;		// Codigo del cliente
		}
		tramaGenerada +=	getClaveProveedor()			+ Separador +		// * Clave de Proveedor
							getTipoSoc()				+ Separador +		// Tipo de Sociedad
							getPersonaContacto()		+ Separador +		// Persona de contacto en la empresa
							getNumeroCliente()			+ Separador +		// Numero de cliente para Factoraje Santander:
							getMedioConf()				+ Separador +		// Medio de Confirmacion
							getFormaPago()				+ Separador +		// Forma de pago
							getNumCuenta()				+ Separador +		// No. cta cheques
							getBanco()					+ Separador +		// Banco
							getSucursal()				+ Separador +		// Sucursal
							getPlaza()					+ Separador +		// Plaza
							getLimiteFinanc()			+ Separador +		// Limite de financiamiento:
							getNumDeudores_Activos()	+ Separador +		// Numero de Deudores Activos
							getVolumenAnual_Ventas()	+ Separador +		// Volumen Anual de Ventas
							getImporteMedio_FactEmit()	+ Separador +		// Importe medio de facturas emitidas
							getMoralFisica()			+ Separador +		// * Persona Fisica o Moral
							getApellidoPaterno()		+ Separador +		// * Apellido Paterno
							getApellidoMaterno()		+ Separador +		// Apellido Materno
							getNombre()					+ Separador +		// * Nombre
							getRFC()					+ Separador +		// * RFC
							getHomoclave()				+ Separador +		// * Homoclave
							getCalle_Numero()			+ Separador +		// * Calle y numero
							getColonia()				+ Separador +		// * Colonia
							getDelg_Munc()				+ Separador +		// * Deleg. o Municipio
							getCd_Pob()					+ Separador +		// * Ciudad o poblacion
							getCP()						+ Separador +		// * Codigo Postal
							getEstado()					+ Separador +		// Estado
							getPais()					+ Separador +		// Pais
							getLada()					+ Separador +		// * Lada o prefijo
							getTelefono()				+ Separador +		// * Numero telefonico
							getExtTel()					+ Separador +		// Extension Tel.
							getFax()					+ Separador +		// Fax
							getExtFax()					+ Separador;		// Extension Fax
		if(Tipo == 0){
			tramaGenerada += getEmail()					+ Separador +		// Email
														  Separador	+		// Estatus
														  Separador +		// Descripcion Estatus
							 getLugarImpresion()		+ Separador; 		// Lugar de Impresion
		}
		if(Tipo == 1){
			tramaGenerada += getLugarImpresion()		+ Separador +		// Lugar de Impresion
							 getEmail()					+ Separador;		// Email
		}
		else if(Tipo == 3){
			tramaGenerada += getEmail()					+ Separador +		// Email
							 getLugarImpresion()		+ Separador;		// Lugar de Impresion
		}
		return tramaGenerada;
	}

	// Separa por Tokens los datos que devuelve Tuxedo de la linea leida del archivo de Respuesta
	public String SeleccionaTokensDeRegistro(String Registro) {
		pdTokenizer tokens = new pdTokenizer(Registro,";",2);
		String TramaActualizada = null;

		if(Registro.substring(0,2).equals("OK")) {
			try	{
				tokens.nextToken();							// OK + Numero
				setFechaTransmision (tokens.nextToken());	// FechaTransmision
			}
			catch(Exception e){;}
		}
		else {
			TramaActualizada = ActualizaTrama(Registro,";",2);
			if(getStatus().equals("A"))
				{setRegAceptados(getRegAceptados() + 1);}
			else
				{setRegRechazados(getRegRechazados() + 1);}

			setRegTotales(getRegRechazados() + getRegAceptados());
		}
		return TramaActualizada;
	}

	// Inicializa los metodos set, separando los Tokens de una trama tomando como referencia
	// un delimitador
	public static Proveedor SeleccionaTokens(String Trama, String Delimitador) {


		Proveedor ObjectProveedor = new Proveedor();

		String Numero_Contrato		= "";
		String TipoSociedad			= "";
		EIGlobal.mensajePorTrace("Tama de proveedor a tokenizar :"+Trama, EIGlobal.NivelLog.INFO);

		try {
			pdTokenizer tokens = new pdTokenizer(Trama, Delimitador, 2);

			Numero_Contrato		= tokens.nextToken();					// Ok + Numero + Numero de Contrato
			ObjectProveedor.setSecuencia		(tokens.nextToken());	// Numero de Secuencia
			ObjectProveedor.setCodigoCliente	(tokens.nextToken());	// Codigo del Cliente
			ObjectProveedor.setClaveProveedor	(tokens.nextToken());	// Clave de Proveedor
			ObjectProveedor.setTipoSoc			(tokens.nextToken());	// Tipo de sociedad
			ObjectProveedor.setPersonaContacto	(tokens.nextToken());	// Persona de Contacto
			ObjectProveedor.setNumeroCliente	(tokens.nextToken());	// Numero de Cliente para Facturaje
			ObjectProveedor.setMedioConf		(tokens.nextToken());	// Medio de Confirmacion
			ObjectProveedor.setFormaPago		(tokens.nextToken());	// Forma de pago
			ObjectProveedor.setNumCuenta		(tokens.nextToken());	// Numero de Cuenta
			ObjectProveedor.setBanco			(tokens.nextToken());	// Banco
			ObjectProveedor.setSucursal			(tokens.nextToken());	// Sucursal
			ObjectProveedor.setPlaza			(tokens.nextToken());	// Plaza
			ObjectProveedor.setLimiteFinanc		(tokens.nextToken());	// Limite de Financiamiento
			ObjectProveedor.setNumDeudores_Activos		(tokens.nextToken());	// Numero de Deudores Activos
			ObjectProveedor.setVolumenAnual_Ventas		(tokens.nextToken());	// Volumen Anual de Ventas
			ObjectProveedor.setImporteMedio_FactEmit	(tokens.nextToken());	// Importe Medio de Facturacion Emitida
			ObjectProveedor.setMoralFisica		(tokens.nextToken());	// Persona Fisica o Moral
			ObjectProveedor.setApellidoPaterno	(tokens.nextToken());	// Apellido Paterno
			ObjectProveedor.setApellidoMaterno	(tokens.nextToken());	// Apellido Materno
			ObjectProveedor.setNombre			(tokens.nextToken());	// Nombre
			ObjectProveedor.setRFC				(tokens.nextToken());	// RFC
			ObjectProveedor.setHomoclave		(tokens.nextToken());	// Homoclave
			ObjectProveedor.setCalle_Numero		(tokens.nextToken());	// Calle y numero
			ObjectProveedor.setColonia			(tokens.nextToken());	// Colonia
			ObjectProveedor.setDelg_Munc		(tokens.nextToken());	// Delegacion o municipio
			ObjectProveedor.setCd_Pob			(tokens.nextToken());	// Ciudad o poblacion
			ObjectProveedor.setCP				(tokens.nextToken());	// Codigo Postal
			ObjectProveedor.setEstado			(tokens.nextToken());	// Estado
			ObjectProveedor.setPais				(tokens.nextToken());	// Pais
			ObjectProveedor.setLada				(tokens.nextToken());	// Lada
			ObjectProveedor.setTelefono			(tokens.nextToken());	// Telefono
			ObjectProveedor.setExtTel			(tokens.nextToken());	// Extension Telefono
			ObjectProveedor.setFax				(tokens.nextToken());	// Fax
			ObjectProveedor.setExtFax			(tokens.nextToken());	// Extension Fax
			ObjectProveedor.setEmail			(tokens.nextToken());	// Email
			ObjectProveedor.setDescStatus		(tokens.nextToken());	// Descripcion de Estatus
			ObjectProveedor.setStatus			(tokens.nextToken());	// Estatus
			ObjectProveedor.setLugarImpresion	(tokens.nextToken());	// Lugar de Impresion

			} catch(Exception e){;}

	return( ObjectProveedor );
	}

	// Actualiza la trama recibida, tomando como referencia un delimitador y un tipo de Actualizacion
	public String ActualizaTrama(String Trama, String Delimitador, int Tipo) {

		String Numero_Contrato		= "";
		String TramaActualizada		= "";
		String TipoPersona			= "";

		try
			{
			pdTokenizer tokens = new pdTokenizer(Trama, Delimitador, 2);

			if (Tipo == 1)
				{ Numero_Contrato		= tokens.nextToken().trim(); }	// Ok + Numero + Numero de Contrato
			setSecuencia		(tokens.nextToken().trim());			// Numero de Secuencia
			setCodigoCliente	(tokens.nextToken().trim());			// Codigo del Cliente
			setClaveProveedor	(tokens.nextToken().trim());	// Clave de Proveedor
			setTipoSoc			(tokens.nextToken().trim());	// Tipo de sociedad
			setPersonaContacto	(tokens.nextToken().trim());	// Persona de Contacto
			setNumeroCliente	(tokens.nextToken().trim());	// Numero de Cliente para Factoraje
			setMedioConf		(tokens.nextToken().trim());	// Medio de Confirmacion
			setFormaPago		(tokens.nextToken().trim());	// Forma de pago
			setNumCuenta		(tokens.nextToken().trim());	// Numero de Cuenta
			setBanco			(tokens.nextToken().trim());	// Banco
			setSucursal			(tokens.nextToken().trim());	// Sucursal
			setPlaza			(tokens.nextToken().trim());	// Plaza
			setLimiteFinanc		(tokens.nextToken().trim());	// Limite de Financiamiento
			setNumDeudores_Activos		(tokens.nextToken().trim());	// Numero de Deudores Activos
			setVolumenAnual_Ventas		(tokens.nextToken().trim());	// Volumen Anual de Ventas
			setImporteMedio_FactEmit	(tokens.nextToken().trim());	// Importe Medio de Facturacion Emitida
			setMoralFisica		(tokens.nextToken().trim());	// Persona Fisica o Moral
			setApellidoPaterno	(tokens.nextToken().trim());	// Apellido Paterno
			setApellidoMaterno	(tokens.nextToken().trim());	// Apellido Materno
			setNombre			(tokens.nextToken().trim());	// Nombre
			setRFC				(tokens.nextToken().trim());	// RFC
			setHomoclave		(tokens.nextToken().trim());	// Homoclave
			setCalle_Numero		(tokens.nextToken().trim());	// Calle y numero
			setColonia			(tokens.nextToken().trim());	// Colonia
			setDelg_Munc		(tokens.nextToken().trim());	// Delegacion o municipio
			setCd_Pob			(tokens.nextToken().trim());	// Ciudad o poblacion
			setCP				(tokens.nextToken().trim());	// Codigo Postal
			setEstado			(tokens.nextToken().trim());	// Estado
			setPais				(tokens.nextToken().trim());	// Pais
			setLada				(tokens.nextToken().trim());	// Lada
			setTelefono			(tokens.nextToken().trim());	// Telefono
			setExtTel			(tokens.nextToken().trim());	// Extension Telefono
			setFax				(tokens.nextToken().trim());	// Fax
			setExtFax			(tokens.nextToken().trim());	// Extension Fax
			setEmail			(tokens.nextToken().trim());	// Email
			setDescStatus		(tokens.nextToken().trim());	// Descripcion de Estatus
			setStatus			(tokens.nextToken().trim());	// Estatus
			setLugarImpresion	(tokens.nextToken().trim());	// Lugar de Impresion

		} catch(Exception e){;}
		if (Tipo == 1) {		//Envia
			TramaActualizada = generaTramaEnvio(Tipo,"@");
		}
		else if (Tipo == 2) {	//Recibe
			TramaActualizada = trama();
		}
		return( TramaActualizada );
	}

	// Crea un Archivo de Ambiente
	public BufferedReader CreaArchivoDeAmbiente(String Archivo) {
		BufferedReader fileAmbiente = null;
		try	{
			fileAmbiente = new BufferedReader(new FileReader(Archivo));
		}catch(IOException e){
			//EIGlobal.mensajePorTrace("Error al crear el Archivo de ambiente.", EIGlobal.NivelLog.INFO);
			//e.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al obtener el la forma de pago en la importacion", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("Proveedor::CreaArchivoDeAmbiente:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"		               				
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
		}
		return fileAmbiente;
	}

	// Lee los Datos que Regresa Tuxedo al enviar un Archivo
	public String LeerDatos_RegresoDeEnvio(String Archivo)	{
		String registroLeido	= "";

		BufferedReader fileAmbiente = CreaArchivoDeAmbiente(Archivo);

		if(fileAmbiente == null)
			{return registroLeido;}
		else {
				try {
			do {

					registroLeido = fileAmbiente.readLine();
					if (registroLeido != null) {
						if (registroLeido.indexOf("Err") != -1 || registroLeido.indexOf("ERR") != -1)
							{return null;}
						if (registroLeido.startsWith("CFRM"))
						{
							return registroLeido;
						}
						else {
							SeleccionaTokens_DatosArchivo(registroLeido);
							}
					}

			} while ( registroLeido != null );
				}catch ( IOException e ) {
					EIGlobal.mensajePorTrace("Error al leer el archivo de ambiente.", EIGlobal.NivelLog.INFO);
				} finally{// Modificacion RMM 20021218 inicio
                    if(null != fileAmbiente){
                        try{
                        	fileAmbiente.close();
                        }catch(Exception e){}
                        fileAmbiente = null;
                    }
                }// Modificacion RMM 20021218 fin
		}
		return TramaDeDatosArchivo();
	}

	// Regresa una peque�a trama de los datos que se inicializaron cuando se envio un Archivo
	public String TramaDeDatosArchivo() {
		return	getSecuencia()						+ "|" +
				Integer.toString(getRegRechazados())+ "|" +
				Integer.toString(getRegAceptados())	+ "|" +
				Integer.toString(getRegTotales())	+ "|";
	}

	// Inicializa y Selecciona los Datos que se presentaran cuando se envia un Archivo
	public void SeleccionaTokens_DatosArchivo(String LineaArchivo) {
		EIGlobal.mensajePorTrace("Linea a tokenizar :"+LineaArchivo, EIGlobal.NivelLog.INFO);
		pdTokenizer tokens = new pdTokenizer(LineaArchivo,";",2);

		if(LineaArchivo.substring(0,2).equals("OK")) {
			try	{
				String ok_Numero	= tokens.nextToken();	// OK + Numero
				setSecuencia (tokens.nextToken());			// Numero de Secuencia
			}
			catch(Exception e){;}
		}
		else {
			try	{
				ArchivoConfirming proveedor = new ArchivoConfirming();
				tokens.nextToken();									// RFC
				String Status = tokens.nextToken();					// Estatus
				if(Status.equals("A"))
					{setRegAceptados(getRegAceptados() + 1);}
				else
					{setRegRechazados(getRegRechazados() + 1);
					setStatus			(Status);
			        setDescStatus		(tokens.nextToken().trim());

					}
					setRegTotales(getRegRechazados() + getRegAceptados());
			}
			catch(Exception e){;}
		}
	}


	// Crea la lista de Proveedores y la lista de clave de Proveedores que se utilizaran para el
	// llenado de los combos en coActualizaciones.jsp
	public Vector coMtoProv_Leer(String Archivo, String tipo, int id_lectura) throws IOException
		{

		String registroLeido = "";
		String campo		 = "";
        String vacio         = "";
		Vector lista = new Vector();

		BufferedReader fileAmbiente = null;

		try
			{fileAmbiente = CreaArchivoDeAmbiente(Archivo);}
		catch(Exception e)
			{lista.add(vacio);}

		char Tipo = tipo.charAt (0);

		if(fileAmbiente != null)
			{
			try
				{
				do
					{registroLeido = fileAmbiente.readLine();}
				while (registroLeido != null && registroLeido.charAt (0) != Tipo);
				do
					{
					if(registroLeido == null || ((registroLeido.substring(0,3)).trim()).equals("Err")  || ((registroLeido.substring(0,3)).trim()).equals("ERR") || ((registroLeido.substring(0,3)).trim()).equals("TCT") )
						{lista.add(vacio);}
					else if ( registroLeido.startsWith( tipo ) )
						{
						if (tipo == "9")
							{
							switch(id_lectura)
								{
								case 1:	// lista de Proveedores
//									jgarcia
									campo = registroLeido.substring( registroLeido.indexOf( ';' , 12 ) + 1 ,
																	 registroLeido.lastIndexOf( ';' ) - 10);
									campo = campo.replace(';',' ') ;
									lista.add(campo);
                                    //EIGlobal.mensajePorTrace("Proveedor Ulises:." + campo,EIGlobal.NivelLog.INFO);

									break;
								case 2:	// lista de clave de Proveedores
//									jgarcia
									int indice = registroLeido.indexOf( ';' , 5 ) + 1;
									campo = registroLeido.substring(indice, registroLeido.indexOf(';', indice));
									lista.add(campo);
                                    //EIGlobal.mensajePorTrace("clave de Proveedor Ulises:." + campo,EIGlobal.NivelLog.INFO);
									break;
								case 3:	//lista de clave de transaccion
									campo = registroLeido.substring(4,registroLeido.indexOf( ';', 6));
									lista.add(campo);
//                                    EIGlobal.mensajePorTrace("clave de transacci�n :." + campo, EIGlobal.NivelLog.INFO);
									break;
								case 4:	//lista de tipo de Cuenta
//									jgarcia
									campo = registroLeido.substring(registroLeido.lastIndexOf(';') - 7);
									campo = campo.substring(0, 1);
									lista.add(campo);
                                    //EIGlobal.mensajePorTrace("tipo Cuenta Ulises:." + campo,EIGlobal.NivelLog.INFO);
									break;
								case 5:	//jgarcia lectura de la divisa
									campo = registroLeido.substring(registroLeido.lastIndexOf(';') - 5, registroLeido.lastIndexOf(';') - 2);
									lista.add(campo);
                                    //EIGlobal.mensajePorTrace("Divisa Ulises:." + campo,EIGlobal.NivelLog.INFO);
									break;
								case 6: //jgarcia 30/Sept/2009 lectura de tipo de proveedor (N - Nacional/I - Internacional)
									campo = registroLeido.substring(registroLeido.lastIndexOf(';') - 1, registroLeido.lastIndexOf(';'));
									lista.add(campo);
									break;
								}
							}
						}
					}
				while ( ( registroLeido = fileAmbiente.readLine() ) != null );
				}
			catch ( Exception e )
				{
				EIGlobal.mensajePorTrace("Error al cerrar el archivo de ambiente.",EIGlobal.NivelLog.INFO);
				lista.add(vacio);
				}
			finally
				{// Modificacion RMM 20021218 inicio
				if(null != fileAmbiente)
					{
					try
						{
						fileAmbiente.close();
						}
					catch(Exception e){}
					fileAmbiente = null;
					}
				}// Modificacion RMM 20021218 fin
			}
		else
			{lista.add(vacio);}
		return (lista);
		}
	}