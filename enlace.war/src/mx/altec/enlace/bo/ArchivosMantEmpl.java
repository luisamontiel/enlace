package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;

public class ArchivosMantEmpl{

    public static final int REGISTROS_POR_PAGINA = 100; // Agregado por Javier D�az - 01/2004. N�mero de registros por p�gina para la consulta.
	public String tipoRegistro = "", fecha = "", NumeroSecuencia = "", CantRegEnv = "", CantRegAce   = "";
	public String CantRegRch = "",  estatus = "", nom_arch = "", num_reg = "";
	public String NumeroEmpleado = "", ApellidoPaterno   = "",ApellidoMaterno = "", Nombre = "", NumeroCuenta = "", Importe = "", Referencia="", Edo="";
	public String RFC="", Sexo="";
	public String sec_pago = "", fch_recep = "", fch_aplic = "", cta_cargo = "", importe_aplic = "";
    public String fch_cargo = "";

	public String asFechaTransmision="", asTotalRegistros="", asAceptados="", asRechazados="", asFechaActualizacion="";
	BaseServlet BAL;
	BaseResource session;
	RandomAccessFile archivoTrabajo;
	BufferedReader archivoTrabajotmp;
	File pathArchivoNom;
	long posicionReg;
	long b			 = 0;
	long residuo	 = 0;
	HttpSession sess;
	public ArchivosMantEmpl(String rutaArchivo,HttpServletRequest request){
	EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al constructor() 1&", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class #Creando archivo: #"+rutaArchivo, EIGlobal.NivelLog.INFO);
		sess = request.getSession();
		session = (BaseResource) sess.getAttribute("session");

		posicionReg=0;
	//BAL.session.setNameFileNomina(rutaArchivo);
		sess.setAttribute("nombreArchivo",rutaArchivo);
		sess.setAttribute("session",session);

		try{
		pathArchivoNom	= new File( rutaArchivo );
		archivoTrabajotmp	= new BufferedReader( new FileReader( pathArchivoNom ) );


		} catch(IllegalArgumentException e) {
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl(1)"+e, EIGlobal.NivelLog.INFO);
		} catch(FileNotFoundException e) {
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl(2)"+e, EIGlobal.NivelLog.INFO);
		} catch(SecurityException e){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl(3)"+e, EIGlobal.NivelLog.INFO);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl(4)"+e, EIGlobal.NivelLog.INFO);
		}
	}


		public String lecturaArchivo(){
			String lecturaReg	= "", encabezado	= "";
			String strTabla		= "";
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a lecturaArchivo()&", EIGlobal.NivelLog.INFO);
			try{
				archivoTrabajo.seek(0);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
			}

			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
									"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Leyendo encabezado&", EIGlobal.NivelLog.INFO);

			try {
				encabezado=archivoTrabajo.readLine();
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajo.readLine();
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
						lecturaReg=archivoTrabajo.readLine();
						if ( lecturaReg.equals( "" ) )
							lecturaReg = archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
						if (lecturaReg.substring(0,1).equals("2")) {
							EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &El tipo de registro es 2&", EIGlobal.NivelLog.INFO);
							lecturaCampos(lecturaReg);
							strTabla=strTabla + creaRegTabla();
						}
					}
				} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			strTabla=strTabla+"</table>";
			return strTabla;
		}

		public void close(){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a close()&", EIGlobal.NivelLog.INFO);
			try {
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
			}
		}

		public void agregaRegistro(HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Se ha agregado un registro&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Existen : "+session.getTotalRegsNom()+"&", EIGlobal.NivelLog.INFO);
		}

	public String creaVariablesJS(){
			String cadenaVariables	= "";
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a creaVariablesJS()&", EIGlobal.NivelLog.INFO);
			cadenaVariables +="<script>js_tipoRegistro='"+ tipoRegistro + "' \n" +
				 "js_NumeroSecuencia=\""+ NumeroSecuencia + "\" \n" +
				 "js_NumeroEmpleado=\""+ NumeroEmpleado + "\" \n" +
				 "js_ApellidoPaterno='"+ ApellidoPaterno + "' \n" +
				 "js_ApellidoMaterno='"+ ApellidoMaterno + "' \n" +
				 "js_Nombre='"+ Nombre + "' \n" +
				 "js_NumeroCuenta='"+ NumeroCuenta + "' \n" +
				 "js_Importe='"+ formatea( Importe ) + "' \n</script>";

			return cadenaVariables;
		}
		public int leeRegistro(long posicion,String tipoArchivo){
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a leeRegistro() ", EIGlobal.NivelLog.INFO);
			String lecturaReg	= "";
			String lecturaRegtmp	= "";
			long tmp = 0;
			//posicionaRegistro(posicion);
			try{
				tipoArchivo.trim();
				archivoTrabajo.seek(41);
				lecturaRegtmp=archivoTrabajo.readLine();

			if (lecturaRegtmp.length()  == 126  ){ // Interbancaria 108 / 110

				archivoTrabajo.seek(0);
			    tmp = ((posicion-1)*128)+ 40 ;// 128 +39
				archivoTrabajo.seek(tmp);
				lecturaReg=archivoTrabajo.readLine();
			 }	else { if (lecturaRegtmp.length()  == 127 ) {// interbancaria 109 / 111
						archivoTrabajo.seek(0);
					    tmp = ((posicion-1)*129)+ 41;
						archivoTrabajo.seek(tmp);
						lecturaReg=archivoTrabajo.readLine();
						}else { EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class El archivo es incorrecto", EIGlobal.NivelLog.INFO);
						        return -1; }
					}
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}
			lecturaCampos(lecturaReg);
			return 1;
		}

		public int posicionaRegistro(long posicion){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a posicionaRegistro>>>()&", EIGlobal.NivelLog.INFO);
			try {
				archivoTrabajo.seek(((posicion-1)*129)+ 41);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}
			return 1;
		}

		public String creaRegTabla(){

			String sTabla	= "";
			posicionReg++;

			residuo=posicionReg % 2 ;
			String bgcolor	= "textabdatobs";
			if (residuo == 0){
				bgcolor		= "textabdatcla";
			}
			sTabla= "<tr><td class=\"" + bgcolor + "\" align=\"center\" nowrap><input type=radio name=seleccionaRegs value="+posicionReg+"></td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + NumeroEmpleado + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + ApellidoPaterno + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + ApellidoMaterno + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + Nombre + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + NumeroCuenta + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"right\">" + formatoMoneda(Importe)  + "&nbsp</td>";
			//sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"right\">" + formatea( agregarPunto(Importe) ) + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">&nbsp</td>";
			return sTabla;
		}

		public String  agregarPunto(String importe){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo agregaPunto()&", EIGlobal.NivelLog.INFO);
	        String importeConPunto	= "";
			//long num = 0;
			//num = Long.parseLong(Importe);
			//importe = "" + num;
	        String cadena1			= importe.substring(0,importe.length()-2);
	        cadena1					= cadena1.trim();
	        String cadena2			= importe.substring(importe.length()-2, importe.length());
	        importe					= cadena1+" "+cadena2;
	        importeConPunto			= importe.replace(' ','.');
	        return importeConPunto;
		}

		public void lecturaCampos(String registro){
			/* Detalle del archivo */
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo lecturaCampos()&", EIGlobal.NivelLog.INFO);
			try {
			tipoRegistro 	 = registro.substring(0,1);
			NumeroSecuencia   = registro.substring(1,6);
			NumeroEmpleado    = registro.substring(6,13);
			ApellidoPaterno   = registro.substring(13,43);
			ApellidoMaterno   = registro.substring(43,63);
			Nombre            = registro.substring(63,93);
			NumeroCuenta      = registro.substring(93,109);
			Importe			  = registro.substring(109,127);

//			NumeroCuenta      = registro.substring(93,104); // Cuando la longitud de la cuenta sea de 11
//			Importe = registro.substring(104,122);
			//Importe = formateaImporte(Importe);
			} catch(Exception e) {
             e.printStackTrace();
			 }
		}

	public static int registrosEnArchivo(RandomAccessFile file){
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &entrando al metodo registrosEnArchivo&()"+ file, EIGlobal.NivelLog.INFO);
		String registroLeido	= "";
		long posicion			= 0;
		int cantRegistros		= 0;
		try{
			if(file.length()>0){
				file.seek(0);
				do{
					registroLeido=file.readLine();
					posicion=file.getFilePointer();
					cantRegistros++;
				}while(posicion<file.length());
			}
		}
		catch(IOException e){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
		}
		return cantRegistros;
	}//fin del metodo registrosEnArchivo

		public int size(){return 129;}

		public String modificaDetalle(){
			String s="";
			return  s;
		}
	/**inicio del metodo completa campos numericos, el cual es una replica del metodo anterior, sin embargo,
	*se presentan dos ligeras variantes, el metodo rellena con ceros en lugar de con blancos, ademas
	*siempre se agregan los caracteres a la izquierda
    */
		public String completaNumericos(String cadena, int longitudRequerida){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo para completar campos numericos&()", EIGlobal.NivelLog.INFO);
			String ceros			= "";
			String cadenaDeRegreso  = "";
			int longitudActual=cadena.length();
			if(longitudActual!=longitudRequerida){
			    int faltantes= longitudRequerida-longitudActual;
				for(int i=0; i<faltantes; i++){
				ceros=ceros+"0";
			    }
			}
			cadenaDeRegreso=ceros+cadena;
			return cadenaDeRegreso;
		}//fin del metodo

		public String[] envioArchivo(String trama_inicio,HttpServletRequest request){

			int cantRegistros= Integer.parseInt(session.getTotalRegsNom());
			String linea	= "", encabezado	= "", tipoReg = "", formatoEnviado = "";
			String []trama=new String[iteraciones(cantRegistros)];


			int contador=0;
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a envioArchivo()&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &trama_inicio()&"+trama_inicio, EIGlobal.NivelLog.INFO);
				try {

					archivoTrabajo.seek(41);
					tipoReg=archivoTrabajo.readLine();
					System.out.println("El valor de tipoReg.length()>>><"+ tipoReg.length());

					if (tipoReg.length() == 127)
					    formatoEnviado = "unix";
						else formatoEnviado = "Microsoft";

					archivoTrabajo.seek(0);
					encabezado=archivoTrabajo.readLine();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
				}
				if (encabezado!=null){
					for(int z=0; z<trama.length; z++){
						trama[z]= trama_inicio+creaTrama(formatoEnviado,request);
						trama[z]=modificaTrama(trama[z]);
						System.out.println("trama[z])>>dentro del for"+trama[z]);
					}
					try {
						archivoTrabajo.close();
					}
					catch(IOException e){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
					}
			}

			try{
			for(int k=0;k<trama.length;k++)
				System.out.println("trama[k]>>"+trama[k]);
			} catch(Exception e) {
			}



			return trama;
		}//fin del metodo

		public String creaTrama(String formatoEnviado,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a creaTrama()&", EIGlobal.NivelLog.INFO);
			String registroDetalle="";
			String registroDetalletmp="";
			String registro="";
			registro=registro.trim();
			int numeroRegistros= Integer.parseInt(session.getTotalRegsNom());
			try{


				registroDetalle=archivoTrabajo.readLine();

				if (registroDetalle != null)
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class registroDetalle  = &"+ registroDetalle, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class registroDetalle.length() = &"+ registroDetalle.length(), EIGlobal.NivelLog.INFO);

			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
			}
			for(int a=0; a<3 ; a++){
				if(registroDetalle.startsWith("2")){
					b=b+1;
					String secuencial	= Long.toString(b);
					String cuenta		= registroDetalle.substring(93,109).trim();
					String importe		= registroDetalle.substring(109,registroDetalle.length());
					importe= eliminaCerosImporte(importe);
					importe=importe.trim();
					if(importe.length()==1)
						importe="0"+importe;
					importe=agregarPunto(importe);
					registro=registro+secuencial+"@"+cuenta+"@"+importe+"@";
					System.out.println("Valor de registro>>"+registro);
					try{
						registroDetalle=archivoTrabajo.readLine();
						System.out.println("Valor de registroDetalle>>"+registroDetalle);
						System.out.println("Valor de registroDetalle.length()>>"+registroDetalle.length());
						//EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class registroDetalle = &"  , EIGlobal.NivelLog.INFO);
					}

					catch(IOException e){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
					}
				}
				else{
					break;
				}
			}//del for
			try{
			  if (formatoEnviado.equals("unix"))
				archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-2);
				else archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-1);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class registro  &" , EIGlobal.NivelLog.INFO);
			return registro;
		}

		public int cuentaArrobas(String cadena){
			int arrobas=0;
			try{
				for(int a=0; a<cadena.length(); a++){
					if(cadena.substring(a,a+1).equals("@"))
						arrobas=arrobas+1;
				}
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
			}
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class cuentaArrobas = &" , EIGlobal.NivelLog.INFO);
			return arrobas;
		}

		public String modificaTrama(String tramaModificable){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo tramaModificable()&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &tramaModificable = &"+tramaModificable ,EIGlobal.NivelLog.DEBUG);
			String tramaDevuelta	= "";
			int valorDevuelto		= 0;
			int arrobas				= 0;
			int registrosEnTrama	= 0;
			String rEnTrama=tramaModificable.substring((posCar(tramaModificable,'@',2)+1),posCar(tramaModificable,'@',3));
			registrosEnTrama=Integer.parseInt(rEnTrama);
			if(cuentaArrobas(tramaModificable)==12)
				valorDevuelto=3;
			else if(cuentaArrobas(tramaModificable)==9)
				valorDevuelto=2;
			else if(cuentaArrobas(tramaModificable)==6)
				valorDevuelto=1;

			tramaDevuelta=tramaModificable.substring(0,posCar(tramaModificable,'@',2)+1)+valorDevuelto+tramaModificable.substring(posCar(tramaModificable,'@',3),tramaModificable.length());
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &tramaDevuelta  &" +tramaDevuelta,EIGlobal.NivelLog.DEBUG);
			return tramaDevuelta;
		}

		public String formateaImporte(String importe){

			String importeFormateado="";
			boolean contienePunto=false;
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo formatea importe()&", EIGlobal.NivelLog.INFO);
			int i=0;
			for (i=0; i<importe.length(); i++){
				if(importe.charAt(i)=='.'){
					contienePunto=true;
					break;
				}
			}
			if(contienePunto){
				importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
			}
			else
				importeFormateado=importe;

			return importeFormateado;
		}

		public int iteraciones(int records){
			int iteraciones=records/3;
			if ((records%3)!=0){
				iteraciones=iteraciones+1;
				return iteraciones;
			}
			else{
				return iteraciones;
			}
		}

		public String eliminaCerosImporte(String imp){
			while(imp.startsWith("0")){
				imp=imp.substring(1,imp.length());
			}
		return imp;
		}

	////////////////////////////////////////// metodos para archivo de salida de empleados /////////////////////////////////
		public String lecturaArchivoSalida(HttpServletRequest request){
			String lecturaReg	= "", encabezado	= "";
			String strTabla	= "";
			int contador	= 0;
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a lecturaArchivoSalida()&", EIGlobal.NivelLog.INFO);
			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
							        "<tr> "+
									"<td class=\"textabref\">Recuperacion del Archivo de Pago de N&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
									"<tr> "+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
//OK      ;26/08/2002;1;1;0;30/08/2002;12.000000;12.000000;0.000000;50000000068;0.
//000000;0.000000;80000648376;348747;
//50000000068;12.000000;R;9907448;;;


/*OK      ;17/09/2002;1;0;0;05/12/2002;12.000000;0.000000;0.000000;50000000068;0.0
00000;0.000000;80000648376;357775;


K      ;17/09/2002;1;0;0;27/09/2002;11.000000;0.000000;0.000000;50000000068;0.0
00000;0.000000;80000648376;247285;
51999000239;4.000000;R;7219786;;;*/





			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();					//lectura del encabezado
				//if ( encabezado.equals( "" ) )
				//		encabezado = archivoTrabajo.readLine();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}

			if (encabezado!=null && encabezado.substring(0,2).equals("OK")){
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setCuentaCargo(encabezado.substring(posCar(encabezado,';',9)+1,posCar(encabezado,';',10)));
				sess.setAttribute("session",session);
				//BAL.session.setCuentaCargo(encabezado.substring(posCar(encabezado,';',9)+1,posCar(encabezado,';',10)));
				lecturaEncabezadoAS(encabezado);
				do {
					try{
						posicionReg=archivoTrabajo.getFilePointer();
						lecturaReg=archivoTrabajo.readLine();
						//if ( lecturaReg.equals( "" ) )
						//	lecturaReg = archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
						return "Error 3: "+ e.getMessage();
					}
				if (lecturaReg!=null){
						lecturaCamposAS(lecturaReg);
						strTabla=strTabla + creaRegAS(contador);
						contador++;

					}
				}while (lecturaReg!=null);
				strTabla=strTabla+"</table>";
				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
				}
			}
			else return "";
			return strTabla+ "|" +contador + "||";
		}

        /** realiza la construccion de la tabla de despliegue de la consulta de archivos
         * TODO:Revisar diferencias entre <code>String lecturaArchivoSalidaConsul(String)</code>
         */
		public String lecturaArchivoSalidaConsul(){
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo lecturaArchivoSalidaConsul()&", EIGlobal.NivelLog.INFO);
			String lecturaReg	= "", encabezado	= "";
			String strTabla		= "";
			strTabla="<table width=\"395\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
									"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha</td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">Secuencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num Reg Enviados</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num Reg Aceptados</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num Reg Rehazados</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Estatus</td>"+
									"</tr>";
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Leyendo encabezado 1&", EIGlobal.NivelLog.INFO);
			try {
				encabezado=archivoTrabajotmp.readLine();
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajotmp.readLine();
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch(Exception e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 1()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			   //OK      00000616;NOMI0000;Transacci�n exitosa;
			   //encabezado = "OK      00000616;NOMI0001;Transacci�n exitosa";
				String existenDatos = encabezado.substring((posCar(encabezado,';',1))+1,posCar(encabezado,';',2));
			if (encabezado!=null && existenDatos.equals("NOMI0000")){
				do{
					try{
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class estoy dentro del do while**", EIGlobal.NivelLog.INFO);
						lecturaReg=archivoTrabajotmp.readLine();
							EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class lecturaReg 1**"+lecturaReg, EIGlobal.NivelLog.INFO);
					}

					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 2()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class lecturaReg 2**"+lecturaReg, EIGlobal.NivelLog.INFO);
							lecturaCamposConsul(lecturaReg);
							strTabla=strTabla + creaRegTablaConsul();
					}
				} while (lecturaReg!=null);
				try {
					archivoTrabajotmp.close();
				}
				catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 3()"+e, EIGlobal.NivelLog.INFO);
				}
			strTabla=strTabla+"</table>";
			}else strTabla=strTabla+"NO EXISTEN DATOS";

			return strTabla;
		}


        // Agregado por Javier D�az - 01/2004. M�todo para leer por p�ginas el archivo de salida de la consulta.
		public String lecturaArchivoSalidaConsul(int pagina){
		    EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo lecturaArchivoSalidaConsul(int)&", EIGlobal.NivelLog.INFO);
		    int    registrosPagina =  0;
			int    numRegistro     = -1;
			String lecturaReg	   = "", encabezado	= "";
			String strTabla		   = "";
			strTabla = "<table width=\"395\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
						   			"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha</td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">Secuencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num Reg Enviados</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num Reg Aceptados</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num Reg Rehazados</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Estatus</td>"+
									"</tr>";
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Leyendo encabezado 1&", EIGlobal.NivelLog.INFO);

			try {
				encabezado = archivoTrabajotmp.readLine( );
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajotmp.readLine( );
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch( Exception e ){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 1()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage( );
			}

			String existenDatos = encabezado.substring( ( posCar( encabezado, ';' , 1 ) ) + 1, posCar( encabezado, ';', 2 ) );
			if ( encabezado != null && existenDatos.equals( "NOMI0000" ) ){
				do {
					try {
						lecturaReg = archivoTrabajotmp.readLine( );
						numRegistro++;
					}

					catch( Exception e ){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 2()"+e, EIGlobal.NivelLog.INFO);
					    return "Error 3: "+ e.getMessage( );
					}

					if ( lecturaReg != null && registrosPagina < REGISTROS_POR_PAGINA && numRegistro >= ( pagina - 1) * REGISTROS_POR_PAGINA ){
					    EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class lecturaReg 2**"+lecturaReg, EIGlobal.NivelLog.INFO);
						lecturaCamposConsul( lecturaReg );
						strTabla = strTabla + creaRegTablaConsul( );
					    registrosPagina++;
					}
				} while ( lecturaReg != null && registrosPagina < REGISTROS_POR_PAGINA );

				try {
					archivoTrabajotmp.close( );
				}
				catch( Exception e ){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 3()"+e, EIGlobal.NivelLog.INFO);
					return "Error 4: "+ e.getMessage( );
				}

			    strTabla = strTabla + "</table>";
			} else
				strTabla = strTabla + "NO EXISTEN DATOS";

			return strTabla;
		}


        // Agregado por Javier D�az - 01/2004. M�todo para calcular el n�mero de registros del archivo de salida de la consulta.
		public int cantidadRegArchivoConsul( ){
		    EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo cantidadRegArchivoConsul( )&", EIGlobal.NivelLog.INFO);
			int    numRegistro     = 0;
			String lecturaReg	   = "", encabezado	= "";
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Leyendo encabezado 1&", EIGlobal.NivelLog.INFO);

            try {
    		    archivoTrabajotmp	= new BufferedReader( new FileReader( pathArchivoNom ) );
            }
			catch( Exception e ){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 1()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}

			try {
				encabezado = archivoTrabajotmp.readLine( );
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajotmp.readLine( );
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch( Exception e ){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 1()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}

			String existenDatos = encabezado.substring( ( posCar( encabezado, ';' , 1 ) ) + 1, posCar( encabezado, ';', 2 ) );
			if ( encabezado != null && existenDatos.equals( "NOMI0000" ) ){
				do {

					try {
						lecturaReg = archivoTrabajotmp.readLine( );
					}
					catch( Exception e ){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 2()"+e, EIGlobal.NivelLog.INFO);
					    return -1;
					}

					if ( lecturaReg != null )
					    numRegistro++;

				} while ( lecturaReg != null );

				try {
					archivoTrabajotmp.close( );
				}
				catch( Exception e ){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 3()"+e, EIGlobal.NivelLog.INFO);
					return -1;
				}

			}

		    EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class cantidadRegArchivoConsul( ): RESULTADO= " + numRegistro, EIGlobal.NivelLog.INFO);
			return numRegistro;
		}



        /** realiza la construccion de la tabla de consulta de archivos de Nomina.
        * TODO:Revisar diferencias entre <code>String lecturaArchivoSalidaConsul()</code>
        *@param lista_nombres_archivos ...
        * @since version 1.
        */
		public String lecturaArchivoSalidaConsul(String lista_nombres_archivos){
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo lecturaArchivoSalidaConsul(String)&"+ lista_nombres_archivos, EIGlobal.NivelLog.INFO);
		/*File pathArchivoNom	= new File( nombre_arch_salida );
		BufferedReader archivoTrabajotmp	= new BufferedReader( new FileReader( pathArchivoNom ) );*/
		//	sec_pago;fch_recep;fch_aplic;cta_cargo;estatus;nom_arch;importe_aplic;num_reg;

			String lecturaReg	= "", encabezado	= "";
			String strTabla		= "";
/*			try{
				archivoTrabajo.seek(0);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
			}*/

			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
									"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha recepci�n</td>"+
                                    "<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de cargo</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci�n</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Nombre del Archivo</td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">Secuencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num. registros</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe aplicado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Estatus</td>"+
									"</tr>";
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Leyendo encabezado&", EIGlobal.NivelLog.INFO);
			try {
				encabezado=archivoTrabajotmp.readLine();
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajotmp.readLine();
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch(Exception e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 1()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class estoy dentro del do while**", EIGlobal.NivelLog.INFO);
						//posicionReg=archivoTrabajo.getFilePointer();
						lecturaReg=archivoTrabajotmp.readLine();
						//if ( lecturaReg.equals( "" ) )
						//	lecturaReg = archivoTrabajo.readLine();
							EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class lecturaReg 1**"+lecturaReg, EIGlobal.NivelLog.INFO);
					}

					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 2()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
						//if (lecturaReg.substring(0,1).equals("2")) {
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class lecturaReg 2**"+lecturaReg, EIGlobal.NivelLog.INFO);
							//lecturaCamposConsul(lecturaReg,lista_nombres_archivos);
							strTabla=strTabla + creaRegTablaConsul();
						//}
					}
				} while (lecturaReg!=null);
				//} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try {
					archivoTrabajotmp.close();
				}
				catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 3()"+e, EIGlobal.NivelLog.INFO);
				}
				strTabla=strTabla+"</table>";
			}
			return strTabla;
		}

		public void lecturaCamposConsul(String registro){
			/* Detalle del archivo */
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo lecturaCamposConsul()&"+ registro, EIGlobal.NivelLog.INFO);
			//Formateo de fecha
			fecha		  = registro.substring(0,posCar(registro,';',1));
			if (fecha.length() >= 8)
			{
			    fecha =  fecha.substring(0,2) + "/" + fecha.substring(2,4) + "/" + fecha.substring(4,8);
            }
			NumeroSecuencia		  = registro.substring((posCar(registro,';',1))+1,posCar(registro,';',2));
            CantRegEnv			  = registro.substring((posCar(registro,';',2))+1,posCar(registro,';',3));
			CantRegAce          = registro.substring((posCar(registro,';',3))+1,posCar(registro,';',4));
            CantRegRch     = registro.substring((posCar(registro,';',4))+1,posCar(registro,';',5));
			estatus           = registro.substring((posCar(registro,';',5))+1,registro.length()-1);
		}
		public String creaRegTablaConsul(){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo creaRegTablaConsul() el bueno&", EIGlobal.NivelLog.INFO);

			String sTabla	= "";
			posicionReg++;

			residuo=posicionReg % 2 ;
			String bgcolor	= "textabdatobs";
			if (residuo == 0){
				bgcolor		= "textabdatcla";
			}

			if (estatus.startsWith("Recibido") || estatus.startsWith("Procesado")  || estatus.startsWith("En proceso")) {
			  sTabla= "<tr><td class=\"" + bgcolor + "\" align=\"center\" nowrap><input type=radio name=valor_radio value="+ sess.getAttribute("nombreArchivo") + '@' + NumeroSecuencia + '@' + fecha +"|"+estatus+"></td>";
			} else {
			  sTabla= "\n<tr>\n<td class=\"" + bgcolor + "\" align=\"center\" nowrap>&nbsp</td>";
            }

			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + fecha + "&nbsp</td>";
            sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + NumeroSecuencia + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + CantRegEnv + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + CantRegAce + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + CantRegRch + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + estatus + "&nbsp</td>";
			//Link para los archivos procesados ...

			String nom_arch_down="";
			if(nom_arch.indexOf(".")>0)
				nom_arch_down=nom_arch.substring(0,nom_arch.indexOf("."))+".mail.gz";
			else
			    nom_arch_down=nom_arch+".mail.gz";

			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Saliendo del metodo creaRegTablaConsul()&", EIGlobal.NivelLog.INFO);
			return sTabla;
		}

		public int cantidadRegArchivo(){
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Entrando a cantidadRegArchivo()", EIGlobal.NivelLog.INFO);

			String lecturaReg	= "", encabezado	= "";
			int contador	= 0;

			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();					//lectura del encabezado
			}
				catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %cantidadRegArchivo()"+e, EIGlobal.NivelLog.INFO);
				return 0;
			}
				do {
					try{
						posicionReg=archivoTrabajo.getFilePointer();
						lecturaReg=archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %cantidadRegArchivo()"+e, EIGlobal.NivelLog.INFO);
						return 0;
					}
				if (lecturaReg!=null){
						contador++;
						System.out.println("valor de contador>>>"+contador);
					}
				}while (lecturaReg!=null);

				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
				}
		return contador;
		}


		public void lecturaCamposAS(String r){
			/* Detalle del archivo */
			if(r==null)
				r = "";
//OK      ;26/08/2002;1;1;0;30/08/2002;12.000000;12.000000;0.000000;50000000068;0.
//000000;0.000000;80000648376;348747;
//50000000068;12.000000;R;9907448;;;

			try{
			 NumeroCuenta    = r.substring(0,r.indexOf(';'));
			 Importe		 = r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			 Edo             = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
			 Referencia      = r.substring(posCar(r,';',3)+1,posCar(r,';',4));
			 Sexo            = r.substring(posCar(r,';',4)+1,posCar(r,';',5));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}

		public void lecturaEncabezadoAS(String r){
			/* Encabezado del archivo */

			int tot=0,acep=0,rech=0;
			if(r==null)
				r= "";
			try{
			 asFechaTransmision= r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			 asTotalRegistros  = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
			 asAceptados       = r.substring(posCar(r,';',3)+1,posCar(r,';',4));
			 tot			   = Integer.parseInt(asTotalRegistros.trim());
			 acep			   = Integer.parseInt(asAceptados.trim());
			 rech			   = tot-acep;
			 asRechazados      = ""+rech;
			 asFechaActualizacion= r.substring(posCar(r,';',4)+1,posCar(r,';',5));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}

		public String creaRegAS(int contador){
			String sTabla="",cadSexo="",cNumeroEmpleado="&nbsp;",cNumeroDepto="&nbsp;", cApellidoPaterno="&nbsp;", cApellidoMaterno="&nbsp;", cNombre="&nbsp;", cRFC="&nbsp;", cHomoclave="&nbsp;", cNumeroCuenta="&nbsp;" ,cImporte="&nbsp;", cReferencia="&nbsp;", cEdo="&nbsp;";
			String sColor="";
				if (Sexo.trim().equals("M")) {cadSexo="MASCULINO";	}
				if (Sexo.trim().equals("F")) {cadSexo="FEMENINO";}
				if (!NumeroEmpleado.trim().equals("")) {cNumeroEmpleado=NumeroEmpleado;}
				if (!ApellidoPaterno.trim().equals("")) {cApellidoPaterno=ApellidoPaterno;}
				if (!Nombre.trim().equals("")) {cNombre=Nombre;}
				if (!RFC.trim().equals("")) {cRFC=RFC;}
				if (!NumeroCuenta.trim().equals("")) {cNumeroCuenta=NumeroCuenta;}
				if (!Importe.trim().equals("")) {cImporte=Importe;}
				if (!Referencia.trim().equals("")) {cReferencia=Referencia;}
				if (!Edo.trim().equals("")) {cEdo=Edo;}
				if ((contador%2)==0) {sColor="#FFFFFF";}
				else {sColor="#F0F0F0";}
				sTabla= "<tr><td class=\"textabdatobs\" align=\"center\" nowrap>&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + cNumeroCuenta + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"right\">" + formatea( cImporte ) + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + cReferencia + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + cEdo + "&nbsp;</td></tr>\n";
			return 	sTabla;
		}

		public String reporteArchivo(){
			String lecturaReg="", encabezado="";
			String strTabla	= "";
			int contador	= 0;
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a reporteArchivo()###&", EIGlobal.NivelLog.INFO);
			strTabla="<table align=center cellspacing=0 border=0>";
			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">"+
									"<tr> "+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a reporteArchivo()&"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
					posicionReg=archivoTrabajo.getFilePointer();
					lecturaReg=archivoTrabajo.readLine();
					}
					catch(Exception e){System.out.println("Error leyendo el Archivo: " + e.getMessage());return "Error 3: "+ e.getMessage();}
					if (lecturaReg!=null){
						if (lecturaReg.substring(0,1).equals("2")){
							lecturaCampos(lecturaReg);
							strTabla=strTabla + creaRegReporte(contador);
							contador++;
						}
					}
				} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
			try{
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl()"+e, EIGlobal.NivelLog.INFO);
				return "Error 4: "+ e.getMessage();
			}
		}
		//else return "";
		strTabla=strTabla+"</table>";
		return strTabla;
		}

		public String creaRegReporte(int contador){

			String sColor = "";
			String sTabla="";
			if ((contador%2)==0) {sColor="#FFFFFF";}
				else {sColor="#F0F0F0";}

			sTabla= "<tr><td class=\"textabdatobs\" nowrap align=\"center\">"+ NumeroEmpleado + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + ApellidoPaterno + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + ApellidoMaterno + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + Nombre + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + NumeroCuenta + "</td>";
			//sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"right\">" + agregarPunto(Importe)  + "</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"right\">" + formatoMoneda(Importe)  + "</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">"+Referencia+ "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">"+Edo+ "&nbsp;</td>";
			sTabla=sTabla + "</td></tr>";
			return 	sTabla;
		}

	public String reporteArchivoSalida(){
			String lecturaReg="", encabezado="";
			String strTabla	= "";
			int contador	= 0;
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a reporteArchivoSalida()&", EIGlobal.NivelLog.INFO);
			strTabla="<table align=center cellspacing=0 border=0>";
			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">"+
									"<tr> "+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"right\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando a reporteArchivoSalida()&"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
					posicionReg=archivoTrabajo.getFilePointer();
					lecturaReg=archivoTrabajo.readLine();
					}
					catch(Exception e){System.out.println("Error leyendo el Archivo: " + e.getMessage());return "Error 3: "+ e.getMessage();}
					if (lecturaReg!=null){
							lecturaCamposAS(lecturaReg);
							strTabla=strTabla + creaRegReporte(contador);
							contador++;
						}
				} while (lecturaReg!=null);
			try{
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %reporteArchivoSalida()"+e, EIGlobal.NivelLog.INFO);
				return "Error 4: "+ e.getMessage();
			}
		}
		//else return "";
		strTabla=strTabla+"</table>";
		return strTabla;
		}


	public int archivoExportar(){
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &entrando archivoExportar() 1&", EIGlobal.NivelLog.INFO);
		String lecturaReg	= "", encabezado	= "" , linea = "" ,lineaUltima= "";
		String archivo_salida = IEnlace.LOCAL_TMP_DIR + "/" + "EMP" + session.getUserID8() + "Nom.doc";
		BufferedWriter salida;
		try {
		salida	= new BufferedWriter( new FileWriter( archivo_salida ) );
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Leyendo encabezado&", EIGlobal.NivelLog.INFO);
			encabezado=archivoTrabajotmp.readLine();
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			if ( encabezado.equals( "" ) )
				encabezado = archivoTrabajotmp.readLine();

				if (encabezado!=null)
					{
				if(! encabezado.substring(0,2).equals("OK"))
					{
						encabezado += "\r\n";
						salida.write( encabezado ,0 ,encabezado.length() );
					}
			}//lectura del encabezado
		}catch(Exception e){
			EIGlobal.mensajePorTrace( "***ArchivosMantEmpl.class  Excepci�n en archivoExportar() >>"+e.getMessage()+"<<2", EIGlobal.NivelLog.ERROR);
			return 0;
		} // try
			if (encabezado!=null){
				do{
					try{
						linea=archivoTrabajotmp.readLine();
						if (linea!=null){
						lineaUltima = lecturaCamposExporta(linea);
						lineaUltima = lineaUltima + "\r\n";
						salida.write( lineaUltima, 0, lineaUltima.length() );
						salida.flush();
						}
					}catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 2()"+e, EIGlobal.NivelLog.INFO);
					return 0;
					}
				} while (linea!=null);
				try {
					archivoTrabajotmp.close();
					salida.close();


		         //Inicia Modif PVA 28/03/2003
		            ArchivoRemoto archR = new ArchivoRemoto();
		           if(!archR.copiaLocalARemoto("EMP" + session.getUserID8() + "Nom.doc","WEB"))
		               EIGlobal.mensajePorTrace("***ArchivosMantEmpl.java   No se creao archivo de exportacion:"+session.getUserID8() + "Nom.doc", EIGlobal.NivelLog.INFO);
		           else
		               EIGlobal.mensajePorTrace("***ArchivosMantEmpl.java  archivo de exportacion creado:"+session.getUserID8() + "Nom.doc", EIGlobal.NivelLog.INFO);
		          //Fin Modif  PVA 28/03/2003



				}
				catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class Ha ocurrido una excepcion en: %ArchivosMantEmpl 3()"+e, EIGlobal.NivelLog.INFO);
					return 0;
				}
			}
		return 1;
		}

		public String lecturaCamposExporta(String registro){
			EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo lecturaCamposExporta() registro&"+ registro, EIGlobal.NivelLog.INFO);
			String linea = "";
			try {

					String fecha		  = registro.substring(0,posCar(registro,';',1));
					String NumeroSecuencia		  = registro.substring((posCar(registro,';',1))+1,posCar(registro,';',2));
					String CantRegEnv			  = registro.substring((posCar(registro,';',2))+1,posCar(registro,';',3));
					String CantRegAce          = registro.substring((posCar(registro,';',3))+1,posCar(registro,';',4));
					String CantRegRch     = registro.substring((posCar(registro,';',4))+1,posCar(registro,';',5));
					String estatus           = registro.substring((posCar(registro,';',5))+1,registro.length()-1);

			//linea = fecha + NumeroSecuencia + CantRegEnv	+ CantRegAce + CantRegRch + estatus;
			linea = fecha + completaCampos(NumeroSecuencia, 12, "izq") + completaCampos(CantRegEnv, 12, "izq")	+ completaCampos(CantRegAce, 12, "izq") + completaCampos(CantRegRch, 12, "izq") + completaCampos(estatus, 20, "der"); // Modificado por Javier D�az - 01/2004. Se paddean los campos con blancos.
			} catch(Exception e) {
             e.printStackTrace();
			 }
	 EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo lecturaCamposExporta() linea&"+ linea, EIGlobal.NivelLog.INFO);
		return linea;
		}

		public String completaCampos(String cadena, int longRequerida, String sentido){
		EIGlobal.mensajePorTrace("***ArchivosMantEmpl.class &Entrando al metodo completaCampos() cadena&" , EIGlobal.NivelLog.INFO);
			String blancos="";
			int longitud= cadena.length();
			int faltantes= longRequerida-longitud;
		if(faltantes>0){
			for(int i=0; i<faltantes; i++){
				blancos=blancos+" ";
			}
			if(sentido.equals("izq"))
			cadena= blancos+cadena;
			else
			cadena= cadena+blancos;
		}
		return cadena;
		}

        // Agregado por Javier D�az - 01/2004. M�todo completaCamposNumericos.
		public String completaCamposNumericos(int longitudActual, int longitudRequerida){
			String ceros="0";
		    int faltantes= longitudRequerida-longitudActual;
		    for(int i=1; i<faltantes; i++){
				ceros=ceros+"0";
		    }
			return ceros;
		}//fin de metodo

		public int posCar(String cad,char car,int cant){
		int result=0,pos=0;
		for (int i=0;i<cant;i++){
			result=cad.indexOf(car,pos);
			pos=result+1;
			}
		return result;
		}

		public String formatea(String formatea){
		String decimales	= "";
		String enteros		= "";
		int position		= formatea.indexOf('.');
		if(position>=0){
			decimales		= formatea.substring(position+1, formatea.length());
			if(position>=1)
				enteros		= formatea.substring(0, position);
			if(decimales.length()>2)
				decimales	= decimales.substring(0, 2);
			if(decimales.length()==1)
				decimales	= decimales+"0";
		}
		else{
			enteros	= formatea;
			decimales="00";
		}
		return enteros+"."+decimales;
	}

	public String formatoMoneda( String cantidad )
	{
		String language = "la"; // ar
		String country  = "MX";  // AF
		Locale local    = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato  = "";
		String cantidadDecimal = "";
		String formatoFinal    = "";
		String cantidadtmp     = "";

		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.0";
			try {
				cantidadtmp =cantidad.substring(0,cantidad.length()-2);
				cantidadDecimal = cantidad.substring(cantidad.length()-2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				if (formato.substring (0,3).equals ("MXN"))
					formato=formato.substring(3,formato.length()-2);
				else
					formato=formato.substring(1,formato.length()-2);
				formatoFinal = "$ "+ formato + cantidadDecimal;
			} catch(NumberFormatException e) {
				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.INFO);
				formatoFinal="0.00";}
		if(formatoFinal==null)
			formato = "";
		return formatoFinal;
	}
}