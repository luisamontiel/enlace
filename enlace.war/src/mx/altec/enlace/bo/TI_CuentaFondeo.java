package mx.altec.enlace.bo;

import java.util.Vector;

public class TI_CuentaFondeo extends TI_Cuenta
	{

	// --- Campos ----------------------------------------------------------------------
	private double importe;
	private int fondeo;  //0: ninguno  1:unico  2:doble
	private TI_Cuenta padre;
	private int tipo;

	//este campo se usa cuando la cuenta se crea de una trama
	public String posiblePadre;

	// --- Constructores e inicializadores ---------------------------------------------
	public TI_CuentaFondeo(TI_Cuenta cuenta)
		{
		super(cuenta.getNumCta(),cuenta.getDescripcion());
		if(cuenta instanceof TI_CuentaFondeo)
			{
			importe = ((TI_CuentaFondeo)cuenta).importe;
			fondeo = ((TI_CuentaFondeo)cuenta).fondeo;
			padre = ((TI_CuentaFondeo)cuenta).padre;
			}
		else
			inicializa();
		}

	public TI_CuentaFondeo(String numCuenta)
		{
		super(numCuenta);
		inicializa();
		}

	private void inicializa()
		{
		importe = 0.0;
		fondeo = 0;
		padre = null;
		posiblePadre = "";
		}

	// --- Indica el nuevo importe -----------------------------------------------------
	public boolean setImporte(double importe)
		{
		if(importe < 0.01) return false;
		importe = ((double)(long)(importe*100))/100;
		this.importe = importe;
		return true;
		}

	// --- Devuelve el importe ---------------------------------------------------------
	public double getImporte()
		{return importe;}

	// --- Devuelve una cadena con el importe ------------------------------------------
	public String getStrImporte()
	 {
		String cadena = "" + importe;
		if(cadena.indexOf("E")!=-1)
		 {
			int orden = Integer.parseInt(cadena.substring(cadena.indexOf("E")+1));
			cadena = cadena.substring(0,cadena.indexOf("E"));
			while(cadena.length() < orden + 3)
			  cadena += "0";
			cadena = cadena.substring(0,1) + cadena.substring(2);
			cadena = cadena.substring(0,orden + 1) + "." + cadena.substring(orden + 1);
		 }
		if(cadena.indexOf(".") == cadena.length() - 2)
		  cadena += "0";
		for(int a=cadena.length()-6;a>0;a-=3)
		  cadena = cadena.substring(0,a) + "," + cadena.substring(a);
		return "$" + cadena;
	 }

	// --- Indica el sentido del fondeo ------------------------------------------------
	public boolean setFondeo(int val)
		{
		if(val>2 || val<0) return false;
		fondeo = val;
		return true;
		}

	// --- Devuelve el sentido del fondeo ----------------------------------------------
	public int getFondeo()
		{return fondeo;}

	// --- Devuelve una cadena indicando el fondeo -------------------------------------
	public String getStrFondeo()
		{
		String valor = "&nbsp;";
		switch(fondeo)
			{
			case 1: valor = "Unico"; break;
			case 2: valor = "Doble"; break;
			}
		return valor;
		}

	// --- Indica el tipo para la cuenta -----------------------------------------------
	public void setTipo(int tipo)
		{this.tipo = tipo;}

	// --- Devuelve el tipo de la cuenta -----------------------------------------------
	public int getTipo()
		{return tipo;}

	// --- Indica el nuevo padre -------------------------------------------------------
	public boolean setPadre(TI_Cuenta newPadre)
		{
		if (newPadre.getNumCta().equals(getNumCta())) return false;
		if (desciendeDe(newPadre)) return false;
		if (((TI_CuentaFondeo)newPadre).desciendeDe(this)) return false;
		padre = newPadre;
		return true;
		}

	// --- Devuelve el padre de la cuenta ----------------------------------------------
	public TI_Cuenta getPadre()
		{return padre;}

	// --- indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(String numCta)
		{
		TI_Cuenta cuenta = padre;
		boolean resultado = false;
		while(!resultado)
			{
			if(cuenta == null) break;
			if(numCta.equals(cuenta.getNumCta())) resultado = true;
			if(cuenta instanceof TI_CuentaFondeo)
				cuenta = ((TI_CuentaFondeo)cuenta).getPadre();
			else
				cuenta = null;
			}
		return resultado;
		}

	// --- Indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(TI_Cuenta posiblePadre)
		{
		return desciendeDe(posiblePadre.getNumCta());
		}

	// --- Regresa el nivel de la cuenta -----------------------------------------------
	public int nivel()
		{
		TI_Cuenta cuenta = padre;
		int resultado = 1;
		while(true)
			{
			if(cuenta == null) break;
			if(cuenta instanceof TI_CuentaFondeo)
				cuenta = ((TI_CuentaFondeo)cuenta).getPadre();
			else
				cuenta = null;
			resultado++;
			}
		return resultado;
		}

	// --- Regresa una trama conteniendo datos de la cuenta ----------------------------
	public String trama()
	 {
		StringBuffer trama = new StringBuffer("");

		trama.append("@");
		trama.append(getNumCta());
		trama.append("|");
		trama.append(((padre == null)?" ":padre.getNumCta()) );
		trama.append("|");
		trama.append(((getDescripcion()=="")?" ":getDescripcion()));
		trama.append("|");
		trama.append(importe);
		trama.append("|");
		trama.append(fondeo);

		return trama.toString();
	 }

	// --- Igual que el anterior pero tomando en cuenta a posiblePadre -----------------
	public String tramaPosible()
	 {
		StringBuffer trama = new StringBuffer("");

		trama.append("@");
		trama.append(getNumCta());
		trama.append("|");
		trama.append(((posiblePadre.equals(""))?" ":posiblePadre));
		trama.append("|");
		trama.append(((getDescripcion()=="")?" ":getDescripcion()));
		trama.append("|");
		trama.append(importe);
		trama.append("|");
		trama.append(fondeo);

		return trama.toString();
	 }

	// --- ESTATICO: Crea una nueva cuenta a partir de una trama -----------------------
	public static TI_CuentaFondeo creaCuentaFondeo(String trama)
		{
		int car;
		String strAux;
		TI_CuentaFondeo cuenta = null;

		if(trama == null) return null;
		if(!trama.substring(0,1).equals("@")) return null;
		trama = trama.substring(1);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta = new TI_CuentaFondeo(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.posiblePadre = (strAux.equals(" "))?"":strAux;
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setDescripcion(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setImporte(Double.parseDouble(strAux));
		car = trama.length();
		strAux = trama.substring(0,car);
		cuenta.setFondeo(Integer.parseInt(strAux));
		return cuenta;
		}

	// --- ESTATICO: Copia una cuenta pero con otro num de cuenta ----------------------
	public static TI_CuentaFondeo copiaCuenta(String neoNumCta, TI_CuentaFondeo cuenta)
		{

		TI_CuentaFondeo neoCuenta = new TI_CuentaFondeo(neoNumCta);
		neoCuenta.setDescripcion(cuenta.getDescripcion());
		neoCuenta.setImporte(cuenta.getImporte());
		neoCuenta.setFondeo(cuenta.getFondeo());
		if(cuenta.getPadre() != null) neoCuenta.setPadre(cuenta.getPadre());
		neoCuenta.setTipo(cuenta.getTipo());
		neoCuenta.posiblePadre = cuenta.posiblePadre;
		return neoCuenta;
		}

	}
