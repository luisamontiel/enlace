/*
 * pdArchivoTemporal.java
 *
 * Created on 3 de abril de 2002, 11:06 AM
 */

package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */

 /*-----------------------------------------------------------------------------
	Incidencia:	IM323029
	Asignado a:	Norberto Vel�zquez Sep�lveda (NVS) - Mantenimiento Praxis
	Descripci�n:El archivo de exportacion de la consulta de pago directo no se
				generaba de forma completa debido a la eliminacion de registros
				que contienen un numero de pago ya existente.
	Correci�n:	El metodo exportar necesita recibir un ArrayList, a esta clase
				se le agreg� un metodo nuevo del mismo nombre que recibe otro
				par�metro de tipo ArrayList, de modo que pueda lograr escribir
				el archivo sin eliminar datos.
	Modificado:	Noviembre 10, 2004.
-----------------------------------------------------------------------------*/

public class pdArchivoTemporal implements java.io.Serializable {
    /** Mapa para almacenar los pagos */
    protected HashMap Mapa;
    /** Nombre del archivo */
    protected String Nombre;
    /** Archivo con el que se esta trabajando */
    protected File Archivo;
    /** Numero actual de pago */
    protected String NumPago;
    /** Numero de contrato */
    protected String Contrato;
    /** Clave de Usuario */
    protected String Usuario;
    /** Numero de registros */
    protected long Registros;
    /** Numero de transmision */
    protected long NumTransmision;
    /** Fecha de transmision */
    protected GregorianCalendar FechaTransmision;
    /** Fecha de actualizacion */
    protected GregorianCalendar FechaActualizacion;
    /** Numero de registros aceptados */
    protected long RegistrosAceptados;
    /** Numero de registros rechazados */
    protected long RegistrosRechazados;
    /** Numero de referencia del archivo */
    protected String Referencia;
    /** Clave del perfil del usuario */
    protected String ClavePerfil;
    /** Bandera de archivo enviado */
    protected boolean bEnviado;

    /** Constructor para crear el objeto desde un JSP */
    public pdArchivoTemporal() {
        Mapa = new HashMap();
        NumPago = "1";
        Contrato = null;
        Usuario = null;
        Registros = 0;
        Archivo = null;
        Nombre = null;
        NumTransmision = 0;
        FechaTransmision = null;
        FechaActualizacion = null;
        RegistrosAceptados = 0;
        RegistrosRechazados = 0;
        ClavePerfil = null;
        Referencia = null;
        bEnviado = false;
    }

    /**
     * Constructor para crear el archivo con el nombre del archivo
     * @param nom Nombre del archivo
     */
    public pdArchivoTemporal(String nom) {
        Archivo = new File (Global.DIRECTORIO_LOCAL + "/" + nom);
        try {
            if (!Archivo.createNewFile ()) {
                Archivo.delete ();
                Archivo.createNewFile ();
            }
        } catch (IOException ex) {
        }
        Nombre = nom;
        Mapa = new HashMap();
        NumPago = "1";
        Contrato = null;
        Usuario = null;
        Registros = 0;
        NumTransmision = 0;
        FechaTransmision = null;
        FechaActualizacion = null;
        RegistrosAceptados = 0;
        RegistrosRechazados = 0;
        ClavePerfil = null;
        Referencia = null;
        bEnviado = false;
    }

    /**
     * Metodo para borrar el archivo de envio
     */
    public void borrarArchivo() {
        Archivo.delete();
    }

    /**
     * Metodo para agregar otro pago al archivo
     * @param pag Nuevo pago a agregar
     */
    public boolean agrega(pdPago pag) {
        if (Mapa.containsKey (pag.NoPago)) return false;
        //EIGlobal.mensajePorTrace ("pdArchivotemporal: agrega() ===> NVS - pag.NoPago=" + pag.NoPago, 2);
        Mapa.put(pag.NoPago, pag);
        Registros++;
        return true;
    }

    /**
     * Metodo para agregar un pago con su numero
     * @param pago Pago a ser agregado
     */
    public boolean agrega(pdCPago pag) {
        pdPago temp = new pdPago (pag);
        return agrega (temp);
    }

    /**
     * Metodo para obtener el siguiente numero de pago
     * @return El siguiente numero de pago valido
     */
    public String getNumPago() {
    	double d = 0.00;
    	d = Double.parseDouble(NumPago);
    	NumPago = String.valueOf(d++);
    	return NumPago;
    }

    /**
     * Metodo para obtener un detalle de pago
     * @param NumPago Numero del pago que se va a recuperar del mapa
     * @return El pago asociado al numero
     */
    public pdPago getPago(String NumPago) {
        return (pdPago) Mapa.get(NumPago);
    }

    /**
     * Metodo para crear un nuevo archivo
     * @return true si el archivo fue creado correctamente, false si ocurrio algun error
     */
    public boolean crea(String nombre) {
        try {
	    EIGlobal.mensajePorTrace ("pdArchivoTemporal --> Nombre " + Global.DIRECTORIO_LOCAL + "/" + nombre, EIGlobal.NivelLog.INFO);
            Archivo = new File(Global.DIRECTORIO_LOCAL + "/" + nombre);
            Nombre = nombre;
            return Archivo.createNewFile();
        } catch (IOException ex) {
            System.err.println (ex.getMessage());
            ex.printStackTrace(System.err);
            return false;
        }
    }

    /**
     * Metodo para modificar un pago
     */
    public void modifica(pdPago pag) {
        Mapa.remove(pag.getNoPago());
        Mapa.put(pag.getNoPago(), pag);
    }

    /**
     * Metodo para borrar un pago del archivo
     * @param NumPago Numero de pago a borrar
     */
    public void borrar(String NumPago) {
        Mapa.remove(NumPago);
        Registros--;
    }

    /**
     * Metodo para exportar el archivo en formato txt
     * @return URL del archivo exportado
     */
    public String exportar(String nombre) {
        FileWriter writer = null;
        try {
            java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
            File Exportado = new File(Global.DIRECTORIO_LOCAL + "/" + nombre + ".doc");
            boolean Creado = Exportado.createNewFile();
            if (!Creado) {
                Exportado.delete();
                Exportado.createNewFile();
            }
            writer = new FileWriter(Exportado);
            ArrayList Pagos = new ArrayList();
            Pagos.addAll(getPagos());
            ListIterator liPagos = Pagos.listIterator();
            pdPago Temp;
            String Linea;
			EIGlobal.mensajePorTrace("Se comenzara linea a linea", EIGlobal.NivelLog.DEBUG);
            while (liPagos.hasNext()) {
                // Por cada pago se genera una linea en el archivo
                Temp = (pdPago) liPagos.next();

				java.text.DecimalFormat nf = new java.text.DecimalFormat();
				nf .applyPattern("############0.00");
				nf .setMinimumFractionDigits(2);
				nf .setMaximumFractionDigits(2);
				String impor = nf.format(Temp.getImporte()).toString();
				if (impor.indexOf(".")>-1)
					impor = impor.substring(0,impor.indexOf('.')) +
									impor.substring(impor.indexOf('.') + 1, impor.length());
                Linea = "";
                Linea = Linea + quitaGuiones(Temp.getCuentaCargo())
                    + agregaEspacios(" ",16 - Temp.getCuentaCargo().length());
                Linea = Linea  + agregaEspacios("0",12 -
                    Temp.getNoPago().length())+ Temp.getNoPago();
                if(null== Temp.getFechaLib()){
                    EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaLib=" + Temp.getFechaLib() , EIGlobal.NivelLog.INFO);
                    Linea += "          ";
                } else {
                    Linea += df.format(Temp.getFechaLib().getTime ());
                }

                if(null == Temp.getFechaPago()){
                    EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaPago=" + Temp.getFechaPago() , EIGlobal.NivelLog.INFO);
                    Linea += "          ";
                } else {
                    Linea += df.format(Temp.getFechaPago().getTime ());
                }

				if(!Temp.getClaveBen().equals("") || null != Temp.getClaveBen()){
					EIGlobal.mensajePorTrace("La clave del beneficiario registrado"+Temp.getClaveBen(), EIGlobal.NivelLog.DEBUG);
					Linea = Linea + agregaEspacios(" ",13 - Temp.getClaveBen().length()) + Temp.getClaveBen();
				}else{
					if(null == Temp.getClavenoReg()){
						EIGlobal.mensajePorTrace("la clave del no registrado es null"+Temp.getClavenoReg(), EIGlobal.NivelLog.DEBUG);
					}else{
						EIGlobal.mensajePorTrace("clave del no registrado", EIGlobal.NivelLog.DEBUG);
						Linea = Linea + agregaEspacios(" ",13 - Temp.getClavenoReg().length()) + Temp.getClavenoReg();
					}
				}

				Linea = Linea + Temp.getNomBen() + agregaEspacios(" ",60 - Temp.getNomBen().length());
				if(Temp.getTodasSucursales())
					Linea = Linea + "S" + "                                        ";
				else Linea = Linea + " " + agregaEspacios(" ",40 - Temp.getClaveSucursal().length())+ Temp.getClaveSucursal();
				Linea = Linea + Temp.getFormaPago();
				Linea = Linea + agregaEspacios("0",16 - impor.length()) + impor ;
                if (Temp.getEstatusPago() == 'L') {
                    if(null == Temp.getFechaPagado() ){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaPagado=" + Temp.getFechaPagado() , EIGlobal.NivelLog.INFO);
                        Linea += "          ";
                    } else {
                        Linea += df.format(Temp.getFechaPagado().getTime ());
                    }
                } else if (Temp.getEstatusPago() == 'C') {
                    if(null == Temp.getFechaCancelacion()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaCancelacion=" + Temp.getFechaCancelacion() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    } else {
                        Linea +=df.format(Temp.getFechaCancelacion().getTime ());
                    }
                } else if (Temp.getEstatusPago() == 'V') {
                    if(null == Temp.getFechaVencimiento()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaVencimiento=" + Temp.getFechaVencimiento() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    }else {
                        Linea +=df.format(Temp.getFechaVencimiento().getTime ());
                    }
                } else if (Temp.getEstatusPago() == 'M') {
                    if(null == Temp.getFechaModificacion()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaModificacion=" + Temp.getFechaModificacion() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    } else {
                        Linea += df.format(Temp.getFechaModificacion().getTime ());
                    }
                } else {
                    if(null == Temp.getFechaPago()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaPago=" + Temp.getFechaPago() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    } else {
                        Linea +=df.format (Temp.getFechaPago ().getTime ());
                    }
                }
                Linea = Linea + agregaEspacios(" ",8 - Temp.getNumeroReferencia().length()) + Temp.getNumeroReferencia() ;
                Linea = Linea + " " + Temp.getEstatusPago() + " ";
                Linea = Linea + Temp.getDescripcionEstatus() +
                    agregaEspacios(" ",40 - Temp.getDescripcionEstatus().length());
                Linea = Linea + Temp.getConcepto() +
                    agregaEspacios(" ",60 - Temp.getConcepto().length()) + "\n";
                EIGlobal.mensajePorTrace("MSD-->20041109: " + Linea, EIGlobal.NivelLog.DEBUG);
                writer.write(Linea);
            }
			EIGlobal.mensajePorTrace("Sali del while", EIGlobal.NivelLog.DEBUG);
            writer.flush ();
            writer.close ();
            EIGlobal.mensajePorTrace("depues de cerrar", EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace("MSD-->20041109 Archivo: " + Global.DIRECTORIO_LOCAL + "/" + nombre + ".doc", EIGlobal.NivelLog.DEBUG);

            if (envia (Exportado)) {
				EIGlobal.mensajePorTrace("envia", EIGlobal.NivelLog.DEBUG);
                return Exportado.getName ();
            } else { //Req. Q15591 Se agrega Mensaje a Log.
				EIGlobal.mensajePorTrace("Regreso de metodo envia. Error al enviar el archivo", EIGlobal.NivelLog.DEBUG);
                return "Error al enviar el archivo";
            }
        } catch (FileNotFoundException ex) {
            System.err.println (ex.getMessage ());
            ex.printStackTrace (System.err);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }finally{
            if(null != writer){
                try{
                    writer.close();
                    writer = null;
                }catch(java.io.IOException ex){
                }
            }
        }
        return "Error al escribir el archivo";
    }

    /**
     * Metodo para enviar un archivo al servidor web
     * @param Archivo Objeto File del archivo a ser enviado
     * @return true si el envio se realizo correctamente, de lo contrario false
     */
    public static boolean envia (File Archivo) {
       try {
		    EIGlobal.mensajePorTrace("dentro del metodo envia", EIGlobal.NivelLog.DEBUG);
            int Respuesta = 1;
            Runtime rt = Runtime.getRuntime();
            // Archivo de configuracion

		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_LOCAL = " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_REMOTO_WEB = " + Global.DIRECTORIO_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_LOCAL = " + Global.HOST_LOCAL, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_REMOTO_WEB = " + Global.HOST_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);
			//Req. Q15591  Se corrige validacion para no copiar, se incluye comparacion de rutas.
            if (Global.HOST_LOCAL.equals (Global.HOST_REMOTO_WEB) &&
				Global.DIRECTORIO_LOCAL.equals (Global.DIRECTORIO_REMOTO_WEB) )
				{
					EIGlobal.mensajePorTrace("Envia. Hosts Remoto-Local y Rutas remota-local iguales, no se hace copia remota", EIGlobal.NivelLog.DEBUG);
					return true;
				}

		    EIGlobal.mensajePorTrace("Envia. Pasa validacion para generar copia remota", EIGlobal.NivelLog.DEBUG);
		    // 18Jul2011 JGAL - Se cambian rcp's por cp�s
/*            EIGlobal.mensajePorTrace("el valor para rt.exec= " + "rcp " + Global.DIRECTORIO_LOCAL + "/" +
				Archivo.getName() + " " + Global.USUARIO_REMOTO_WEB + "@" + Global.HOST_REMOTO_WEB + ":" + Global.DIRECTORIO_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);*/

            EIGlobal.mensajePorTrace("el valor para rt.exec= " + "cp " + Global.DIRECTORIO_LOCAL + "/" +
    				Archivo.getName() + " " + Global.DIRECTORIO_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);

/*            Process proc = rt.exec("rcp " +
                Global.DIRECTORIO_LOCAL
                + "/" + Archivo.getName() + " " +
                Global.USUARIO_REMOTO_WEB + "@" +
                Global.HOST_REMOTO_WEB + ":" +
                Global.DIRECTORIO_REMOTO_WEB); */

            Process proc = rt.exec("cp " +
                    Global.DIRECTORIO_LOCAL
                    + "/" + Archivo.getName() + " " +
                    Global.DIRECTORIO_REMOTO_WEB);

            Respuesta = proc.waitFor();
		    EIGlobal.mensajePorTrace("Metodo envia. Valor Respuesta copia remota.(Si 0 =Ok) :" + Respuesta, EIGlobal.NivelLog.DEBUG);

            if (Respuesta == 0)
                Respuesta = proc.exitValue();
            //Req. Q15591 Se indica el caso de falla de copia remota
			else
				EIGlobal.mensajePorTrace("Metodo envia. No se efectuo copia, revisar permisos usuario para cp/ o path" + Respuesta, EIGlobal.NivelLog.DEBUG);

            /**
			 * <VC autor="GGB" fecha="18/09/2007"
			 * 		descripcion="Cerrar expl�citamente los Stream que tiene el objeto Process">
			 */
			try {
				proc.getErrorStream().close();
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("pdArchivoTemporal.envia -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			try {
				proc.getInputStream().close();
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("pdArchivoTemporal.envia -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			try {
				proc.getOutputStream().close();
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("pdArchivoTemporal.envia -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			/** </VC> */

            proc.destroy();
            if (Respuesta == 0)
                return true;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return false;
    }

    /**
     * Metodo para escribir el contenido del mapa en el archivo
     * @return true si se envio correctamente el archivo de lo contrario regresa false
     */
    public boolean escribe(){
        FileWriter writer = null;
        try {
            /** Se abre el archivo para escribir en el */
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("ddMMyyyy");
            writer = new FileWriter(Archivo);
            ArrayList Pagos = new ArrayList();
            Pagos.addAll(getPagos());
            ListIterator liPagos = Pagos.listIterator();
            String Linea;
            pdPago Temp;
            while (liPagos.hasNext()) {
                /*
                 * Por cada pago se genera una linea
                 */
                Temp = (pdPago) liPagos.next();
                Linea = quitaGuiones(Temp.getCuentaCargo()) + "@";
                Linea = Linea + Temp.getNoPago() + "@";
                Linea = Linea + Temp.getImporte() + "@";
				/*Linea = Linea + Temp.getClaveBen() + "@";
				Linea = Linea + Temp.getNomBen() + "@";*/
				if(Temp.getClaveBen().equals("")){
					Linea = Linea + Temp.getClavenoReg() +"@" + Temp.getNomBen() + "@";
				}else{
					Linea = Linea + Temp.getClaveBen() + "@@";

				}
                Linea = Linea + Temp.getFormaPago() + "@";
                Linea = Linea + sdf.format (Temp.getFechaLib().getTime ()) + "@";
                Linea = Linea + sdf.format (Temp.getFechaPago().getTime ()) + "@";
                if (Temp.getTodasSucursales()) {
                    Linea = Linea + "S@";
                } else {
                    Linea = Linea + "@";
                }
                Linea = Linea + Temp.getClaveSucursal() + "@";
                Linea = Linea + "@@" + Temp.getConcepto() + "@@\n";
				System.out.println("\nLa Linea  que se va a mandar en archivo a el servicio\n  #"+Linea+"#");
                /** Se escribe la linea formateada en el archivo */
                writer.write(Linea);
            }
            writer.close ();
            return true;
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);

        }finally{
            if(null != writer){
                try{
                    writer.close();
                }catch(java.io.IOException ex){
                }
                writer = null;
            }
        }

        return false;
    }

    /**
     * Metodo para establecer el usuario
     * @param usuario Clave de usuario
     */
    public void setUsuario (String usuario) {Usuario = usuario;}

    /**
     * Metodo para establecer el numero de contrato
     * @param contrato Numero de contrato
     */
    public void setContrato (String contrato) {Contrato = contrato;}

    /**
     * Metodo para obtener los registros actuales en el archivo
     * @return Numero de registros en el archivo
     */
    public long getRegistros() {return Registros;}

    /**
     * Metodo para obtener el nombre del archivo
     * @return Nombre del archivo
     */
    public String getNombre() {return Nombre;}

    /**
     * Metodo para obtener el numero de transmision
     * @return Numero de transmision
     */
    public long getNumTransmision() {return NumTransmision;}

    /**
     * Metodo para obtener la fecha de transmision
     * @return Fecha de transmision
     */
    public GregorianCalendar getFechaTransmision() {return FechaTransmision;}

    /**
     * Metodo para obtener la fecha de actualizacion
     * @return Fecha de actualizacion
     */
    public GregorianCalendar getFechaActualizacion() {return FechaActualizacion;}

    /**
     * Metodo para obtener el numero de registros aceptados en la transaccion
     * @return Numero de registros aceptados
     */
    public long getRegistrosAceptados() {return RegistrosAceptados;}

    /**
     * Metodo para obtener el numero de registros rechazados
     * @return Numero de registros rechazados
     */
    public long getRegistrosRechazados() {return RegistrosRechazados;}

    /**
     * Metodo para obtener los pagos registrados en el archivo
     * @return Pagos en el archivo
     */
    public Collection getPagos () {return Mapa.values();}

    /**
     * Metodo para completar un registro con espacios
     * @return String con espacios para completar el registro
     */
    public String agregaEspacios(String Rel, long Espacios) {
        String Relleno = "";
        long Aux = 0;
        while (Aux < Espacios) {
            Relleno = Relleno + Rel;
            Aux++;
        }
        return Relleno;
    }

    /**
     * Metodo para obtener el numero de referencia del archivo
     * @return Numero de referencia
     */
    public String getReferencia() {return Referencia;}

    /**
     * Metodo para establecer la clave del perfil del usuario
     * @param String Clave del perfil
     */
    public void setClavePerfil(String clave) {ClavePerfil = clave;}

    /**
     * Metodo para obtener la clave del perfil del usuario
     * @return Clave del perfil
     */
    public String getClavePerfil() {return ClavePerfil;}

    /**
     * Metodo para formatear numeros de contrato y cuentas
     * @param Guiones String a formatear
     * @return String formateado
     */
    public String quitaGuiones(String Guiones) {
        String Temp = "";
        StringTokenizer stGuiones =
            new StringTokenizer(Guiones, "-");
        int Tokens = stGuiones.countTokens();
        int Count = 0;
        while (Count < Tokens) {
            Temp = Temp + stGuiones.nextToken();
            Count++;
        }
        return Temp;
    }

    /**
     * Metodo para obtener el archivo con el que se esta trabajando
     * @return Archivo actual de trabajo
     */
    public File getArchivo() {return Archivo;}

    /**
     * Metodo para establecer la fecha de actualizacion
     * @param Fecha Fecha de actualizacion
     */
    public void setFechaActualizacion(GregorianCalendar Fecha){FechaActualizacion = Fecha;}

    /**
     * Metodo para establecer la fecha de transmisi�n
     * @param fecha Fecha de transmision del Archivo
     */
    public void setFechaTransmision (GregorianCalendar fecha) {FechaTransmision = fecha;}

    /**
     * Metodo para establecer el n�mero de referencia
     * @param ref N�mero de referencia
     */
    public void setReferencia (String ref) {Referencia = ref;}

    /**
     * Metodo para destramar la respuesta de tuxedo
     * @param trama Trama de la respuesta de Tuxedo
     * @return Mensaje formateado con la respuesta
     */
    public String getRespuesta (File respuesta) {
        BufferedReader reader = null;
        try {
            reader =
                new BufferedReader (
                    new FileReader (respuesta));
            String trama = reader.readLine ();
            String Respuesta;
            if (trama.indexOf ("OK") == -1) return "Error en la transmisi�n";
            Respuesta = "Su n&uacute;mero de referencia es ";
            Referencia = trama.substring (2, 12).trim ();
            FechaTransmision = new GregorianCalendar ();
            Respuesta = Respuesta + Referencia + "\\n";
            Respuesta = Respuesta + trama.substring (12);
            while ((trama = reader.readLine ()) != null) {
                java.util.StringTokenizer tokenizer =
                    new java.util.StringTokenizer (trama, "@");
                String numPago = tokenizer.nextToken ();
                char estatus = tokenizer.nextToken ().charAt (0);
                String descEstatus = tokenizer.nextToken ();
                pdPago pago = this.getPago (numPago);
                pago.setEstatusPago (estatus);
                pago.setDescripcionEstatus (descEstatus);
            }
            if(null != reader){
                try{
                    reader.close();
                }catch(java.io.IOException ex){
                }
            }
            return Respuesta;
        } catch (IOException ex) {
            System.err.println ("Error al leer archivo de respuesta: "
                + ex.getMessage ());
            ex.printStackTrace (System.err);
            if(null != reader){
                try{
                    reader.close();
                }catch(java.io.IOException e){
                }
            }
            return "Error al leer el archivo de respuesta";
        } catch (Exception ex) {
            if(null != reader){
                try{
                    reader.close();
                }catch(java.io.IOException e){
                }
            }
            return ex.getMessage ();
        }
    }

    /**
     * Metodo para obtener la bandera de enviado
     * @return Bandera
     */
    public boolean getbEnviado () {return bEnviado;}

    /**
     * Metodo para establecer la bandera de enviado
     * @param Valor de la bandera
     */
    public void setbEnviado (boolean b) {bEnviado = b;}

    /**
     * Destructor. Evita la acumulaci�n de archivos inecesarios
     */
    protected void finalize () throws Throwable {
        Archivo.delete ();
        super.finalize ();
    }

    /** Setter for property NumTransmision.
     * @param NumTransmision New value of property NumTransmision.
     */
    public void setNumTransmision (long NumTransmision) {
        this.NumTransmision = NumTransmision;
    }

    /**
     * Metodo para exportar el archivo en formato .doc
     * @return URL del archivo exportado
     */
    public String exportar(String nombre, ArrayList Pagos) {
    	FileWriter writer = null;
    	try {
            java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
            File Exportado = new File(Global.DIRECTORIO_LOCAL + "/" + nombre + ".doc");
            boolean Creado = Exportado.createNewFile();
            if (!Creado) {
                Exportado.delete();
                Exportado.createNewFile();
            }
            writer = new FileWriter(Exportado);
            ListIterator liPagos = Pagos.listIterator();


            pdPago Temp;
			pdCPago pagoAR;

            String Linea;
			EIGlobal.mensajePorTrace("Se comenzara linea a linea", EIGlobal.NivelLog.DEBUG);
            while (liPagos.hasNext()) {
            	// Por cada pago se genera una linea en el archivo

				pagoAR=(pdCPago) liPagos.next();
				Temp = new pdPago(pagoAR);

				java.text.DecimalFormat nf = new java.text.DecimalFormat();
				nf .applyPattern("############0.00");
				nf .setMinimumFractionDigits(2);
				nf .setMaximumFractionDigits(2);
				String impor = nf.format(Temp.getImporte()).toString();
				if (impor.indexOf(".")>-1)
					impor = impor.substring(0,impor.indexOf('.')) +
							impor.substring(impor.indexOf('.') +
							1, impor.length());
                Linea = "";
                Linea = Linea + quitaGuiones(Temp.getCuentaCargo()) +
                		agregaEspacios(" ",16 - Temp.getCuentaCargo().length());
                Linea = Linea  +
                		agregaEspacios("0",12 - Temp.getNoPago().length()) +
                		Temp.getNoPago();
                if(null== Temp.getFechaVencimiento()){
                    EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaLib=" + Temp.getFechaVencimiento() , EIGlobal.NivelLog.INFO);
                    Linea += "          ";
                } else {
                    Linea += df.format(Temp.getFechaVencimiento().getTime ());
                }

                if(null == Temp.getFechaPago()){
                    EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaPago=" + Temp.getFechaPago() , EIGlobal.NivelLog.INFO);
                    Linea += "          ";
                } else {
                    Linea += df.format(Temp.getFechaPago().getTime ());
                }

				if(!Temp.getClaveBen().equals("") || null != Temp.getClaveBen()){
					EIGlobal.mensajePorTrace("La clave del beneficiario registrado"+Temp.getClaveBen(), EIGlobal.NivelLog.DEBUG);
					Linea = Linea + agregaEspacios(" ",13 - Temp.getClaveBen().length()) + Temp.getClaveBen();
				}else{
					if(null == Temp.getClavenoReg()){
						EIGlobal.mensajePorTrace("la clave del no registrado es null"+Temp.getClavenoReg(), EIGlobal.NivelLog.DEBUG);
					}else{
						EIGlobal.mensajePorTrace("clave del no registrado", EIGlobal.NivelLog.DEBUG);
						Linea = Linea + agregaEspacios(" ",13 - Temp.getClavenoReg().length()) + Temp.getClavenoReg();
					}
				}

				Linea = Linea + Temp.getNomBen() + agregaEspacios(" ",60 - Temp.getNomBen().length());
				if(Temp.getTodasSucursales())
					Linea = Linea + "S" + "                                        ";
				else Linea = Linea + " " + agregaEspacios(" ",40 - Temp.getClaveSucursal().length())+ Temp.getClaveSucursal();
				Linea = Linea + Temp.getFormaPago();
				Linea = Linea + agregaEspacios("0",16 - impor.length()) + impor ;
                if (Temp.getEstatusPago() == 'L') {
                    if(null == Temp.getFechaPagado() ){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaPagado=" + Temp.getFechaPagado() , EIGlobal.NivelLog.INFO);
                        Linea += "          ";
                    } else {
                        Linea += df.format(Temp.getFechaPagado().getTime ());
                    }
                } else if (Temp.getEstatusPago() == 'C') {
                    if(null == Temp.getFechaCancelacion()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaCancelacion=" + Temp.getFechaCancelacion() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    } else {
                        Linea +=df.format(Temp.getFechaCancelacion().getTime ());
                    }
                } else if (Temp.getEstatusPago() == 'V') {
                    if(null == Temp.getFechaVencio()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaVencimiento=" + Temp.getFechaVencio() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    }else {
                        Linea +=df.format(Temp.getFechaVencio().getTime ());
                    }
                } else if (Temp.getEstatusPago() == 'M') {
                    if(null == Temp.getFechaModificacion()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaModificacion=" + Temp.getFechaModificacion() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    } else {
                        Linea += df.format(Temp.getFechaModificacion().getTime ());
                    }
                } else {
                    if(null == Temp.getFechaPago()){
                        EIGlobal.mensajePorTrace("pdArchivoTemporal Temp.fechaPago=" + Temp.getFechaPago() , EIGlobal.NivelLog.INFO);
                        Linea +="          ";
                    } else {
                        Linea +=df.format (Temp.getFechaPago ().getTime ());
                    }
                }
                //Linea = Linea + agregaEspacios(" ",8 - Temp.getNumeroReferencia().length()) + Temp.getNumeroReferencia() ;
				/** Numero de referencia para exportacion 26/03/2007*/
				String referenciaArchivo=pagoAR.getRefOperacion();
				if(referenciaArchivo==null)
					referenciaArchivo="";
				Linea = Linea + agregaEspacios(" ",8 - referenciaArchivo.length()) + referenciaArchivo ;

                Linea = Linea + " " + Temp.getEstatusPago() + " ";
                Linea = Linea + Temp.getDescripcionEstatus() +
                    agregaEspacios(" ",40 - Temp.getDescripcionEstatus().length());
                Linea = Linea + Temp.getConcepto() +
                    agregaEspacios(" ",60 - Temp.getConcepto().length()) + "\n";
                EIGlobal.mensajePorTrace("MSD-->20041109: " + Linea, EIGlobal.NivelLog.DEBUG);
                writer.write(Linea);
            }
			EIGlobal.mensajePorTrace("Sali del while", EIGlobal.NivelLog.DEBUG);
            writer.flush ();
            writer.close ();
            EIGlobal.mensajePorTrace("depues de cerrar", EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace("MSD-->20041109 Archivo: " + Global.DIRECTORIO_LOCAL + "/" + nombre + ".doc", EIGlobal.NivelLog.DEBUG);

            if (envia (Exportado)) {
				EIGlobal.mensajePorTrace("envia", EIGlobal.NivelLog.DEBUG);
                return Exportado.getName ();
            } else { //Req. Q15591 Se agrega Mensaje a Log.
				EIGlobal.mensajePorTrace("Regreso de metodo envia. Error al enviar el archivo", EIGlobal.NivelLog.DEBUG);
                return "Error al enviar el archivo";
            }
        } catch (FileNotFoundException ex) {
            System.err.println (ex.getMessage ());
            ex.printStackTrace (System.err);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }finally{
            if(null != writer){
                try{
                    writer.close();
                    writer = null;
                }catch(java.io.IOException ex){
                }
            }
        }
        return "Error al escribir el archivo";
    }
}