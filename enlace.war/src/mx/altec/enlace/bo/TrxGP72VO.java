package mx.altec.enlace.bo;

import java.util.*;

/**
 *  Clase bean para manejar el procedimiento para la transaccion GP72
 *
 * @author Daniel Hernandez Perez
 * @version 1.0 Apr 4, 20112
 */
public class TrxGP72VO {

	/**
	 * Folio de operacion de cambio 390 Padre
	 */
	private String folioOperacionPadre = null;

	/**
	 * Folio de operacion de cambio 390 Abono
	 */
	private String folioOperacion = null;

	/**
	 * Estatus de la operacion
	 */
	private String estatusOperacion = null;

	/**
	 * Folio de Transfer
	 */
	private String folioTransfer =  null;

	/**
	 * Estatus de la operacion
	 */
	private boolean operacionExitosa = false;

	/**
	 * Mensaje de error de operacion
	 */
	private String mensajeError = null;

	/**
	 * Reintento de operacion
	 */
	private boolean erroresReintento = false;

	/**
	 * Obtener el valor de folioOperacionPadre.
     *
     * @return folioOperacionPadre valor asignado.
	 */
	public String getFolioOperacionPadre() {
		return folioOperacionPadre;
	}

	/**
     * Asignar un valor a folioOperacionPadre.
     *
     * @param folioOperacionPadre Valor a asignar.
     */
	public void setFolioOperacionPadre(String folioOperacionPadre) {
		this.folioOperacionPadre = folioOperacionPadre;
	}

	/**
     * Asignar un valor a folioOperacion.
     *
     * @param folioOperacion Valor a asignar.
     */
	public void setFolioOperacion(String folioOp){
		this.folioOperacion = folioOp;
	}

	/**
	 * Obtener el valor de folioOperacion.
     *
     * @return folioOperacion valor asignado.
	 */
	public String getFolioOperacion(){
		return this.folioOperacion;
	}

	/**
	 * Obtener el valor de estatusOperacion.
     *
     * @return estatusOperacion valor asignado.
	 */
	public String getEstatusOperacion() {
		return estatusOperacion;
	}

	/**
     * Asignar un valor a estatusOperacion.
     *
     * @param estatusOperacion Valor a asignar.
     */
	public void setEstatusOperacion(String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}

	/**
	 * Obtener el valor de operacionExitosa.
     *
     * @return operacionExitosa valor asignado.
	 */
	public boolean getOperacionExitosa() {
		return operacionExitosa;
	}

	/**
     * Asignar un valor a operacionExitosa.
     *
     * @param operacionExitosa Valor a asignar.
     */
	public void setOperacionExitosa(boolean operacionExitosa) {
		this.operacionExitosa = operacionExitosa;
	}

	/**
     * Asignar un valor a folioTransfer.
     *
     * @param folioTransfer Valor a asignar.
     */
	public void setFolioTransfer(String folioTr){
		this.folioTransfer = folioTr;
	}

	/**
	 * Obtener el valor de folioTransfer.
     *
     * @return folioTransfer valor asignado.
	 */
	public String getFolioTransfer(){
		return this.folioTransfer;
	}

	/**
	 * Obtener el valor de mensajeError.
     *
     * @return mensajeError valor asignado.
	 */
	public String getMensajeError() {
		return mensajeError;
	}

	/**
     * Asignar un valor a mensajeError.
     *
     * @param mensajeError Valor a asignar.
     */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	/**
	 * Obtener el valor de erroresReintento.
     *
     * @return erroresReintento valor asignado.
	 */
	public boolean getErroresReintento() {
		return erroresReintento;
	}

	/**
     * Asignar un valor a erroresReintento.
     *
     * @param erroresReintento Valor a asignar.
     */
	public void setErroresReintento(boolean erroresReintento) {
		this.erroresReintento = erroresReintento;
	}

}