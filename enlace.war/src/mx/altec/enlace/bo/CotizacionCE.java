// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.Serializable;
import java.text.*;
import java.util.*;

/**
 * <code><B></I>CotizacionCE</I></B><code> es un <I>bean</I> que interpreta la cadena de<BR>
 * respuesta de la trama de cotizaci�n de disposici&oacute;n <B>CETC</B> asignando cada valor a variables<BR>
 * que son recuperadas por el "jsp".<BR>
 * 06/05/2002<BR>
 * 13/05/2002 Se A&ntilde;di&oacute; el m&eacute;todo <I>setDatos</I><BR>
 * 20/05/2002 Se modific&oacute; el acceso de <I>quitaPunto</I> de privado a publico.<BR>
 * 21/05/2002 Se agregaron los m&eacute;todos para definir cuenta, l&iacute;nea de cr&eacute;dito y descripci&oacute;n.<BR>
 * 04/06/2002 Se agreg&oacute; el atributo "fechaAplicacion".<BR>
 * 03/11/2002 Se quitan los dos primeros caracteres del dato de plazo. Se reemplaz&oacute; el gui&oacute;n por la diagonal como separador de fechas.<BR>
 * 03/11/2002 Se agregan los atributos de folio y c&oacute;digo de error.<BR>
 * 04/11/2002 Se efecuta la resta en vez de truncar.<BR>
 * </P>
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.2
 */
public class CotizacionCE extends Object implements Serializable
{

/**Formato para la fecha.
*/
public static final String mascaraFecha="dd/MM/yyyy";

/**Sirve para compararar los dos primeros caracteres de la trama.
*/
public static final String OK="OK";

/**C&oacute;digo de error cuando la trama es correcta.
*/
public static final String BIEN="0000";

/**N&uacute;mero de partes de una trama correcta.
*/
public static final int numeroPartes=6;

/**Caracter por defecto usado como separador.
*/
public static final String pipe="|";

/**N&uacute;mero de partes del par&aacute;metro <I>datosCuenta</I>
*/
public static int numeroDatosCuenta=3;

/**Quita este valor al plazo.   Si en la trama de consulta indica "1", en la trama<BR>
de respuesta indica "1001", por lo que hay que restar dicho valor.
*/
public static final int MIL=1000;

/**Separador de campos.
*/
private String separador=pipe;

/**Fecha de vencimiento de la cotizaci&oacute;n
*/
private String fechaVencimiento=new String("");

/**Fecha de vencimiento de la cotizaci&oacute;n
*/
private Date vencimiento=null;

/**Fecha de apliaci&oacute;n de la cotizaci&oacute;n
*/
private Date aplicacion=null;

/**N&uacute;mero de d&iacute;as de plazo.
*/
private int plazo=0;

/**Tasa de inter&eacute;s
*/
private float tasa=0;

/**C&aacute;lculo del inter&eacute;s de la cotizaci&oacute;n
*/
private String interes="";

/**Suma de la disposici&oacute;n m&aacute;s los intereses generados.
*/
private String importeTotal="";

/**Cantidad monetaria de la disposici&oacute;n
*/
private String monto="";

/**Revisa el formato de las fechas.
*/
private SimpleDateFormat sdf=new SimpleDateFormat(mascaraFecha);

/**Este arreglo de enteros indica las posiciones de una trama correcta.
*/
private int[] posiciones={2, 8, 15, 19, 23};

/**Almacena la trama de respuesta.
*/
private String datos="";

/**Almacena la cuenta, l&iacute;nea de cr&eacute;dito y descripci&oacute;n
*/
private String datosCuenta="";

/**N&uacute;mero de cuenta.
*/
private String cuenta="";

/**N&uacute;mero de la l&iacute;nea de cr&eacute;dito asociada.
*/
private String lineaCredito="";

/**Descripci&oacute;n de la cuenta (chequera).
*/
private String descripcion="";

/**Cadena con el n&uacute;mero de d&iacute;as del plazo.
*/
private String cadenaPlazo="";

/**Cadena con la tasa de inter&eacute;s
*/
private String cadenaTasa="";

/**Cadena sin el punto decimal de la tasa de inter&eacute;s
*/
private String tasaSoloNumero="";

/**Cadena con una descripci&oacute;n del concepto de la cotizaci&oacute;n
*/
private String concepto="";

/**Fecha de aplicaci&oacute;n de la consulta de cotizaci&oacute;n
*/
private String fechaAplicacion="";


/**Guarda el n&uacute;mero de folio con el que se proceso la cotizaci&oacute;n.
*/
private String folio="";


/**Da el c&oacute;digo de error de la trama de respuesta.
*/
private String codigoError="";


/**Da el mensaje de error si la trama no contiene datos.
*/
private String mensajeError="";

/**Indica si la trama que interpret&oacute; es correcta o no.
*/
private boolean esCorrecta=false;

  /**
   * Constructor vac&iacute;o
   */
  public CotizacionCE()
  {
  } // Fin constructor


   /**Proporciona la fecha de vencimiento de la cotizaci&oacute;n como cadena.
   */
  public String getFechaVencimiento()
  {
    return fechaVencimiento;
  }

   /** <code><B><I>setFechaVencimiento</I></B></code> asigna la fecha de vencimiento, recibe la fecha como cadena, la verifica y la asigna.
   * <P>Este m&eacute;todo revisa que la cadena dada sea una fecha v&aacute;lida.
   * </P>
   * @param fecha Valor para asignar la fecha de vencimiento.
   */
  public void setFechaVencimiento(String fecha)
  {
    if(fecha!=null)
    {
      fechaVencimiento=fecha;

      try
      {
        vencimiento=sdf.parse(fecha, new ParsePosition(0));
        //fechaVencimiento=fecha;
      }
      catch(Exception e)
      {
      } // Fin try-catch

     } // Fin if
  } // Fin setFechaVencimiento


  public void setVencimiento(Date vencimiento)
  {
    if(vencimiento!=null)
    {
      this.vencimiento=vencimiento;
      fechaVencimiento=sdf.getInstance().format(vencimiento);
    } // Fin if
  } // Fin setVencimiento


  public Date getVencimiento()
  {
    return vencimiento;
  } // Fin getVencimiento


  public int getPlazo()
  {
    return plazo;
  }

  public void setPlazo(int plazo)
  {
    if(plazo>0)
    {
      this.plazo=plazo;
      Integer intPlazo=new Integer(plazo);
      cadenaPlazo=intPlazo.toString();
    } // Fin if

  } // Fin setPlazo


  public void setPlazo(String plazo)
  {
    if(plazo!=null && plazo.length()>0)
    {
      try
      {
        this.plazo=Integer.parseInt(plazo);
        cadenaPlazo=plazo;
      }
      catch(NumberFormatException nfe)
      {
      } // Fin try-catch
    } // Fin if
  } // Fom setPlazo


  public String getCadenaPlazo()
  {
    return cadenaPlazo;
  }


  public float getTasa()
  {
    return tasa;
  }





  public void setTasa(float tasa)
  {
    if(tasa>=0)
    {
      this.tasa=tasa;
      Float flTasa=new Float(tasa);
      cadenaTasa=flTasa.toString();
      tasaSoloNumero=quitaPunto(cadenaTasa);
    } // Fin if
  } // Fin setTasa


  public void setTasa(String tasa)
  {
    if(tasa!=null && tasa.length()>0)
    {
      try
      {
        this.tasa=Float.parseFloat(tasa);
        tasaSoloNumero=quitaPunto(tasa);
      }
      catch(NumberFormatException nfe)
      {

      } // Fin try-catch
    } // Fin if
  } // Fin setTasa


  public String getTasaSoloNumero()
  {
    return tasaSoloNumero;
  }

  public String getCadenaTasa()
  {
    return cadenaTasa;
  }

  /*
  public float getInteres()
  {
    return interes;
  }
  */

  public String getInteres()
  {
    return interes;
  }


  /*
  public void setInteres(float interes)
  {
    if(interes>=0)
    {
      this.interes=interes;
    }
  } // Fin setInteres
  */

  /*
  public void setInteres(String interes)
  {
    if(interes!=null)
    {
      try
      {
        this.interes=Float.parseFloat(interes);
      }
      catch(NumberFormatException nfe)
      {
      } // Fin try-catch
    } // Fin if
  } // Fin setInteres
  */


  public void setInteres(String interes)
  {
    if(interes!=null)
    {
      this.interes=interes;
    } // Fin if
  } // Fin setInteres



  /*
  public float getImporteTotal()
  {
    return importeTotal;
  }
  */

  public String getImporteTotal()
  {
    return importeTotal;
  }


  /*
  public void setImporteTotal(float importeTotal)
  {
    if(importeTotal>=0)
    {
      this.importeTotal=importeTotal;
    }
  }
  */

  public void setImporteTotal(String importeTotal)
  {
    if(importeTotal!=null)
    {
      this.importeTotal=importeTotal;
    }
  }

  /*
  public void setImporteTotal(String importeTotal)
  {
    if(importeTotal!=null)
    {
      try
      {
        this.importeTotal=Float.parseFloat(importeTotal);
      }
      catch(NumberFormatException nfe)
      {

      } // Fin try-catch
    } // Fin if
  } // Fin setImporteTotal
  */



  public String getMonto()
  {
    return monto;
  }




  public void setMonto(String monto)
  {
    if(monto!=null)
      this.monto=monto;
  }




  public String getCuenta()
  {
    return cuenta;
  }

  public String getLineaCredito()
  {
    return lineaCredito;
  }


  public String getDescripcion()
  {
    return descripcion;
  }

  public String getDatos()
  {
    return datos;
  }



 /**<code><B><I></I></B><code>Recibe la trama de respuesta de tuxedo.  Invoca a<BR>
 * <I>interpretaPC47</I> para interpretarla.
 * @param datos es la trama de respuesta de tuxedo.
 */
  public void setDatos(String datos)
  {
    if(datos!=null)
    {
      if(interpretaPC47(datos))
        this.datos=datos;
    } // Fin if
  } // Fin setDatos


  public String getSeparador()
  {
    return separador;
  } // Fin getSeparador


  public void setSeparador(String separador)
  {
    if(separador!=null)
      this.separador=separador;
  } // Fin setSeparador


  public String getDatosCuenta()
  {
      return datosCuenta;
  } // Fin getDatosCuenta


  /**<code><B><I>setDatosCuenta</I></B></code> recibe la cadena con cuenta, l&iacute;nea de cr&eacute;dito y descripci&oacute;n,<BR>
  * y los separa (por medio del <I>pipe</I>) asign&aacute;ndolos a los atributos <I>cuenta</I>, <I>lineaCredito</I> y <BR>
  * <I>descripci&oacute;n</I>.
  * @param datosCuenta Recibe el par&aacute;metro del mismo nombre, el cual dividido.
  */
  public void setDatosCuenta(String datosCuenta)
  {
    if(datosCuenta!=null)
    {
      StringTokenizer st=new StringTokenizer(datosCuenta, separador);

      if(st.countTokens()==numeroDatosCuenta)
      {
        cuenta=st.nextToken();
        lineaCredito=st.nextToken();
        descripcion=st.nextToken();
      } // Fin if
    }
    else
    {
    } // Fin if-else
  } // Fin setDatosCuenta

  /**<code><B><I>interpretaPC47</I></B></code> interpreta la trama de cotizaci&oacute;n de consulta de disposici&oacute;n <B>CECT</B>
   * <P>Formato de tramas correctas:<BR><BR>
   * OK______REFERE CODE9999|FechaVencimiento|Plazo|Tasa|interes|ImporteTotal <BR>
   * OK      202608 CEES0000|25-04-2001|1001|  10.0000|              0.27|          1,000.27 <BR><BR>
   * Regresa verdadero si la cadena tiene el formato esperado, de lo contrario regresa falso.
   * <P>
   * @param trama es la cadena de la trama del servicio de tuxedo CETC.
   */
  public boolean interpretaPC47(String trama)
  {
    String aux1;
    int tempPlazo=0;
    StringTokenizer st;
    esCorrecta=false;
    mensajeError="";

    if(trama==null || trama.length()<1)
       return false;


    aux1=trama.substring(0, posiciones[0]);

    if(!aux1.equalsIgnoreCase(OK))
      return false;

    aux1=trama.substring(posiciones[1], posiciones[2]).trim();

    if(aux1!=null)
    {
      folio=aux1;
    }

    aux1=trama.substring(posiciones[3], posiciones[4]);

    if(aux1==null || aux1.length()<1)
    {
      return false;
    }
    else
    {
      codigoError=aux1;
    } // Fin if-else aux1 no nulo


    if(!aux1.equalsIgnoreCase(BIEN))
    {
      StringTokenizer tramaError=new StringTokenizer(trama, "|");

      if(tramaError.countTokens()>1)
      {
         tramaError.nextToken();
         mensajeError=tramaError.nextToken();
      } // Fin if partes

      return false;
    } // Fin if mal


    st=new StringTokenizer(trama, "|");

    if(st.countTokens()!=6)
      return false;

    st.nextToken();

    setFechaVencimiento(st.nextToken().trim().replace('-', '/'));
    //setPlazo(st.nextToken().trim());
    /*Se quitan los dos primeros caracteres del dato correspondiente al plazo
    * seg�n como viene en el c�digo de Visual Basic.    Si en la trama de consulta
    * el plazo es de un d�a, en esta trama de respuesta dicho dato viene como 1001.
    */
    aux1=st.nextToken().trim();

    try
    {
      tempPlazo=Integer.parseInt(aux1);
      setPlazo(tempPlazo-MIL);
    }
    catch(NumberFormatException nfe)
    {
    }

    setTasa(st.nextToken().trim());
    setInteres(st.nextToken().trim());
    setImporteTotal(st.nextToken().trim());
    fechaAplicacion=sdf.format(new Date());
    esCorrecta=true;

    return true;
  } // Fin InterpretaPC47


  /**<code><B><I>quitaPunto</I></B></code> elimina el caracter que corresponde al punto "." de una cadena.<BR>
  * Dicha cadena corresponde a un n&uacute;mero flotante, como la tasa de inter&eacute;s, ya que para armar la<BR>
  * trama de disposici&oacute;n se requiere que dicho dato sea sin punto.<BR>
  * <P>Regresa una nueva cadena sin punto.
  * </P>
  * @param cadenaNumero es la cadena a la cual retirar el caracter de punto.
  */
  public String quitaPunto(String cadenaNumero)
  {
    StringBuffer sb=new StringBuffer(cadenaNumero);
    char c;

    for(int i=0;i<sb.length();i++)
    {
        c=sb.charAt(i);

        if(c=='.')
          sb.deleteCharAt(i);
    } // Fin for

    return new String(sb);
  } // Fin cadenaNumero


  public String getConcepto()
  {
    return concepto;
  }


  public void setConcepto(String concepto)
  {
    if(concepto!=null)
      this.concepto=concepto;
  }


	public void setCuenta(String cuenta)
	{
		if(cuenta!=null && cuenta.length()>0)
		{
			this.cuenta=cuenta;
		}
	} // Fin setCuenta



	public void setLineaCredito(String lineaCredito)
	{
		if(lineaCredito!=null && lineaCredito.length()>0)
		{
			this.lineaCredito=lineaCredito;
		}
	} // Fin setLineaCredito


	public void setDescripcion(String descripcion)
	{
		if(descripcion!=null && descripcion.length()>0)
		{
			this.descripcion=descripcion;
		}
	}


   public String getFechaAplicacion()
   {
      return fechaAplicacion;
   }


   public String getFolio()
   {
      return folio;
   }


   public String getCodigoError()
   {
      return codigoError;
   }

   public String getMensajeError()
   {
      return mensajeError;
   }


   public boolean esCorrecta()
   {
      return esCorrecta;
   }


   /**<code><B><I>setFechaAplicacion</I></B></code> asigna la fecha de aplicaci&oacute;n en la cual se<BR>
   * cotiza esta disposici&oacute;n.
   * @param fechaAplicacion Recibe la fecha como cadena pero no verifica el formato de la misma.
   */
   public void setFechaAplicacion(String fechaAplicacion)
   {
      if(fechaAplicacion!=null && fechaAplicacion.length()>0)
      {
         this.fechaAplicacion=fechaAplicacion;
         /*
         try
         {
            aplicacion=sdf.parse(fechaAplicacion, new ParsePosition(0));
            this.fechaAplicacion=fechaApliacion;
         }
         catch(Exception e)
         {
         } // Fin try-catch
         */
      } // Fin if
   } // Fin setFechaApliacion

} // Fin CotizacionCE

