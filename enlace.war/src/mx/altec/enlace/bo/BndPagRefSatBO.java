package mx.altec.enlace.bo;

import mx.altec.enlace.beans.BndPagRefSatBean;
import mx.altec.enlace.dao.BndPagRefSatDAO;

/**
 * Mejoras de Linea de Captura
 * Clase que consume las operaciones (BndPagRefSatDAO) relacionadas a Pago Referenciado SAT
 * Se obtiene llave de pago la bandera de cada tipode pago:
 *            Provisional, Del Ejercicio y de Entidades Federativas.
 * @author SGSC - Indra Junio 2014
 */
public class BndPagRefSatBO {

	/**
	 * Se ejecuta el select para obtener la bandera  para los pagos de impuestos:
	 *                                Provisional, Del Ejercicio y de Entidades Federativas.
	 * 
	 * @param tipPRS
	 *            String con el Tipo de Pago de Pago Referenciado SAT
	 * @return String con el valor de la bandera correspondiente al Tipo de Pago
	 */
	public String consBndPagRefSAT(String tipPRS) {
		BndPagRefSatDAO bndPRSDao = new BndPagRefSatDAO();
		BndPagRefSatBean bndPRSBean = new BndPagRefSatBean();
		bndPRSBean = bndPRSDao.consBndPagRefSAT(bndPRSBean, tipPRS);
		if ("001".equals(tipPRS)) {
			return bndPRSBean.getBndProvis();
		} else if ("005".equals(tipPRS)) {
			return bndPRSBean.getBndEjerci();
		} else if ("012".equals(tipPRS)) {
			return bndPRSBean.getBndEntFed();
		}
		return "1";
	}

}
