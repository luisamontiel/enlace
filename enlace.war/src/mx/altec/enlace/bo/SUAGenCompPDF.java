/*
 * ComprobanteSUA.java
 *
 * Created on 05 de septiembre de 2006
 *
 * Clase encarga de generar el documento del contrato
 * en formato PDF
 */

package mx.altec.enlace.bo;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;



import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author javila.
 */
public class SUAGenCompPDF {

	private String _contrato;
	private String _nombreContrato;
	private String _claveUsuario;
	private String _nombreUsuario;
	private String _cuentaCargo;
	private String _nombreCuenta;
	private String _folioSUA;
	private String _registroPatronal;
	private String _mesPago;
	private String _anioPago;
	private String _lineaCaptura;
	private double _importeTotal;
	private String _numeroOperacion;
	private String _fecha;
	private String _hora;
	private String _medioPresentacion;
	private String _tipoComprobante;
	private String _msgError;
	
	public SUAGenCompPDF() {
		super();
		this._contrato = "";
		this._nombreContrato = "";
		this._claveUsuario = "";
		this._nombreUsuario = "";
		this._cuentaCargo = "";
		this._nombreCuenta = "";
		this._folioSUA = "";
		this._registroPatronal = "";
		this._mesPago = "";
		this._anioPago = "";
		this._lineaCaptura = "";
		this._importeTotal = 0;
		this._numeroOperacion = "";
		this._fecha = "";
		this._hora = "";
		this._medioPresentacion = "";
		this._tipoComprobante = "";
		this._msgError = "";
	}

	public SUAGenCompPDF(String contrato, String nombreContrato, String claveUsuario, 
		String nombreUsuario, String folioSUA, String cuentaCargo, String nombreCuenta, 
		String registroPatronal, String mesPago, 
		String anioPago, String lineaCaptura, double importeTotal, 
		String numeroOperacion, String fecha, String hora, 
		String medioPresentacion, String tipoComprobante, String msgError) {
		super();
		this._contrato = contrato;
		this._nombreContrato = nombreContrato;
		this._claveUsuario = claveUsuario;
		this._nombreUsuario = nombreUsuario;
		this._folioSUA = folioSUA;
		this._cuentaCargo = cuentaCargo;
		this._nombreCuenta = nombreCuenta;
		this._registroPatronal = registroPatronal;
		this._mesPago = mesPago;
		this._anioPago = anioPago;
		this._lineaCaptura = lineaCaptura;
		this._importeTotal = importeTotal;
		this._numeroOperacion = numeroOperacion;
		this._fecha = fecha;
		this._hora = hora;
		this._medioPresentacion = medioPresentacion;
		this._tipoComprobante = tipoComprobante;
		this._msgError = msgError;
	}

/** El m�todo generaComprobanteSUA() genera el PDF
*/
    public ByteArrayOutputStream generaComprobanteBAOS(){
        EIGlobal.mensajePorTrace("M�todo Comprobante.generaComprobanteSUA ", 
        		EIGlobal.NivelLog.INFO);
        String nombreArchivo = _claveUsuario + "SUA" + ".pdf";
    	String archivoPDF = Global.DIRECTORIO_REMOTO_WEB + "/" + nombreArchivo; 
    	
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try{
            Document documento = new Document(PageSize.LETTER,20,20,35,20);
            PdfWriter writer = PdfWriter.getInstance(documento, baos);
           PdfWriter.getInstance(documento, 
            		new FileOutputStream(archivoPDF));
            writer.setViewerPreferences(PdfWriter.FitWindow);
            writer.setViewerPreferences(PdfWriter.HideWindowUI);
            
            documento.open();

            documento.newPage();
            Font fuente = new Font(Font.HELVETICA, 14, Font.BOLD, new Color(0xFF, 0x00, 0x00));
            PdfPCell espacio = new PdfPCell(new Paragraph("",fuente)); //Para insertar espacios
			espacio.setBorderWidth(0);

            PdfPTable tablageneral = new PdfPTable(1); 
            // Tabla general
            tablageneral.setWidthPercentage(90); // porcentaje
            tablageneral.getDefaultCell().setPadding(0);
            tablageneral.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            tablageneral.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            //Tabla encabezado 1
            PdfPTable tablaEncabezado1 = new PdfPTable(2);
            // Logo Santander
            tablaEncabezado1.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            tablaEncabezado1.setWidthPercentage(90);
			Image gif = Image.getInstance(Global.OTP_RUTA_PDF_CONTRATO.substring(0, 
					Global.OTP_RUTA_PDF_CONTRATO.lastIndexOf("/") + 1) + "glo25040c.gif");
            //gif.scaleAbsolute(220,34);
            PdfPCell cell = new PdfPCell(gif);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            tablaEncabezado1.addCell(cell);

            fuente = new Font(Font.HELVETICA, 16, Font.BOLDITALIC, new Color(30, 30, 140));
            // Espacios
            cell = new PdfPCell(new Paragraph("",fuente)); // Junto al logo
			cell.setBorderWidth(0);
			tablaEncabezado1.addCell(cell);
			
            cell = new PdfPCell(new Paragraph("",fuente)); // Un renglon
			cell.setBorderWidth(0);
			tablaEncabezado1.addCell(cell);
			tablaEncabezado1.addCell(cell);
			tablaEncabezado1.addCell(cell); // Otro renglon
			tablaEncabezado1.addCell(cell);
			tablaEncabezado1.addCell(cell); // Otro renglon
			tablaEncabezado1.addCell(cell);
			tablaEncabezado1.addCell(cell); // Otro renglon
			tablaEncabezado1.addCell(cell);
			
            // Titulo del Comprobante
            Paragraph texto = new Paragraph("Comprobante de operaci�n", fuente);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaEncabezado1.addCell(cell);
			gif = Image.getInstance(Global.OTP_RUTA_PDF_CONTRATO.substring(0, 
					Global.OTP_RUTA_PDF_CONTRATO.lastIndexOf("/") + 1) + "glo25030.PNG");
            cell = new PdfPCell(gif);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            tablaEncabezado1.addCell(cell); 
            
            //tablaEncabezado1.addCell(espacio); // Provisionalmente junto al t�tulo
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
			tablaEncabezado1.addCell(espacio);
            tablageneral.addCell(tablaEncabezado1);
			//Tabla encabezado 2
            PdfPTable tablaEncabezado2 = new PdfPTable(1);
            tablaEncabezado2.setWidthPercentage(90); // porcentaje
            tablaEncabezado2.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            fuente = new Font(Font.HELVETICA, 13, Font.BOLDITALIC, new Color(30, 30, 140));
            if (_tipoComprobante.equals("EXITO")) {
                texto = new Paragraph("Recibo Bancario de Pago de S.U.A. L�nea " +
                		"de Captura", fuente);
            } else {
            	texto = new Paragraph("Causa de rechazo que impide la recepci�n " +
            			"de la L�nea de Captura para realizar el pago", fuente);
            }
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaEncabezado2.addCell(cell);
			tablaEncabezado2.addCell(espacio);
			tablaEncabezado2.addCell(espacio);
			tablaEncabezado2.addCell(espacio);
			tablaEncabezado2.addCell(espacio);
            tablageneral.addCell(tablaEncabezado2);
            //Tabla detalle
            PdfPTable tablaDetalle1 = new PdfPTable(2);
            tablaDetalle1.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            tablaDetalle1.setWidthPercentage(90); // porcentaje
            int columnas[] = { 25,75 }; // porcentaje
            tablaDetalle1.setWidths(columnas);

            Font fuenteTitCmp = new Font(Font.HELVETICA, 11, Font.BOLD, new Color(0x00, 0x00, 0x00));
            Font fuenteDatoCmp = new Font(Font.HELVETICA, 11, Font.NORMAL, new Color(0x00, 0x00, 0x00));
            tablaDetalle1.addCell(espacio); // Inserta un renglon
            tablaDetalle1.addCell(espacio);
            //Contrato
            texto = new Paragraph("Contrato: ", fuenteTitCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalle1.addCell(cell);
            texto = new Paragraph(_contrato + " " + _nombreContrato, fuenteDatoCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDetalle1.addCell(cell);
            tablaDetalle1.addCell(espacio); // Inserta un renglon
            tablaDetalle1.addCell(espacio);
            tablaDetalle1.addCell(espacio); // Inserta un renglon
            tablaDetalle1.addCell(espacio);
			// Usuario
            texto = new Paragraph("Usuario: ", fuenteTitCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalle1.addCell(cell);
            texto = new Paragraph(_claveUsuario + " " + _nombreUsuario, fuenteDatoCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDetalle1.addCell(cell);
            tablaDetalle1.addCell(espacio); // Inserta un renglon
            tablaDetalle1.addCell(espacio);
            tablaDetalle1.addCell(espacio); // Inserta un renglon
            tablaDetalle1.addCell(espacio);
			// Cuenta de Cargo
            texto = new Paragraph("Cuenta de  \nCargo: ", fuenteTitCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalle1.addCell(cell);
            texto = new Paragraph(_cuentaCargo + " " + _nombreCuenta, fuenteDatoCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDetalle1.addCell(cell);
            tablaDetalle1.addCell(espacio); // Inserta un renglon
            tablaDetalle1.addCell(espacio);
            tablaDetalle1.addCell(espacio); // Inserta un renglon
            tablaDetalle1.addCell(espacio);
            tablageneral.addCell(tablaDetalle1);
            if (_tipoComprobante.equals("RECHAZO")) {
                PdfPTable tablaDetalle2 = new PdfPTable(1);
                tablaDetalle2.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                tablaDetalle2.setWidthPercentage(70); // porcentaje
                Font fuenteRechazo = new Font(Font.HELVETICA, 12, Font.BOLD, new Color(0x00, 0x00, 0x00));
                Font fuenteRechazo2 = new Font(Font.HELVETICA, 12, Font.BOLDITALIC, new Color(0x00, 0x00, 0x00));
                tablaDetalle2.addCell(espacio); // Inserta un renglon
                tablaDetalle2.addCell(espacio);
                
                texto = new Paragraph("Estimado Empresario:", fuenteRechazo);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetalle2.addCell(cell);
                tablaDetalle2.addCell(espacio); // Inserta un renglon
                
                texto = new Paragraph("La informaci�n presentada por usted, para " +
                		"efectuar la aportaci�n", fuenteRechazo);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetalle2.addCell(cell);
                tablaDetalle2.addCell(espacio); // Inserta un renglon
                texto = new Paragraph("de cuotas Obrero - Patronales " +
                		"muestra el error descrito a", fuenteRechazo);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetalle2.addCell(cell);
                tablaDetalle2.addCell(espacio); // Inserta un renglon
                texto = new Paragraph("continuaci�n:", fuenteRechazo);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetalle2.addCell(cell);
                tablaDetalle2.addCell(espacio); // Inserta un renglon
                tablaDetalle2.addCell(espacio);
                texto = new Paragraph("\"" + _msgError + "\"", fuenteRechazo2);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetalle2.addCell(cell);
                tablaDetalle2.addCell(espacio); // Inserta un renglon
                tablaDetalle2.addCell(espacio); // Inserta un renglon
                tablaDetalle2.addCell(espacio);
                tablaDetalle2.addCell(espacio);
                tablageneral.addCell(tablaDetalle2);
            } else {
                PdfPTable tablaDetalle3 = new PdfPTable(2);
                tablaDetalle3.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                tablaDetalle3.setWidthPercentage(90); // porcentaje
                tablaDetalle3.setWidths(columnas);
    			// folioSUA
                texto = new Paragraph("Folio SUA: ", fuenteTitCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablaDetalle3.addCell(cell);
                texto = new Paragraph(_folioSUA, fuenteDatoCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablaDetalle3.addCell(cell);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
    			// Registro Patronal
                texto = new Paragraph("Registro  \nPatronal: ", fuenteTitCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablaDetalle3.addCell(cell);
                texto = new Paragraph(_registroPatronal, fuenteDatoCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablaDetalle3.addCell(cell);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
    			// Per�odo de Pago
                texto = new Paragraph("Per�odo de  \nPago: ", fuenteTitCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablaDetalle3.addCell(cell);
                texto = new Paragraph("Mes " + _mesPago + "  A�o " + _anioPago, fuenteDatoCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablaDetalle3.addCell(cell);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
    			// L�nea de Captura
                texto = new Paragraph("L�nea Captura: ", fuenteTitCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablaDetalle3.addCell(cell);
                texto = new Paragraph(_lineaCaptura, fuenteDatoCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablaDetalle3.addCell(cell);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
    			// Importe Total:
                texto = new Paragraph("Importe Total: ", fuenteTitCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablaDetalle3.addCell(cell);
                texto = new Paragraph(FormatoMoneda.formateaMoneda(_importeTotal), fuenteDatoCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablaDetalle3.addCell(cell);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
    			//Numero de Operaci�n
                texto = new Paragraph("N�mero de  \nOperaci�n: ", fuenteTitCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablaDetalle3.addCell(cell);
                texto = new Paragraph(_numeroOperacion, fuenteDatoCmp);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablaDetalle3.addCell(cell);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
                tablaDetalle3.addCell(espacio); // Inserta un renglon
                tablaDetalle3.addCell(espacio);
                tablageneral.addCell(tablaDetalle3);
            }
            PdfPTable tablaDetalle4 = new PdfPTable(2);
            tablaDetalle4.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            tablaDetalle4.setWidthPercentage(90); // porcentaje
            tablaDetalle4.setWidths(columnas);
			//Fecha y hora
            texto = new Paragraph("Fecha y hora: ", fuenteTitCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalle4.addCell(cell);
            texto = new Paragraph(_fecha + "  " + _hora, fuenteDatoCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDetalle4.addCell(cell);
            tablaDetalle4.addCell(espacio); // Inserta un renglon
            tablaDetalle4.addCell(espacio);
            tablaDetalle4.addCell(espacio); // Inserta un renglon
            tablaDetalle4.addCell(espacio);
			//Medio Presentaci�n
            texto = new Paragraph("Medio  \nPresentaci�n: ", fuenteTitCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalle4.addCell(cell);
            texto = new Paragraph(_medioPresentacion, fuenteDatoCmp);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDetalle4.addCell(cell);
            tablaDetalle4.addCell(espacio); // Inserta un renglon
            tablaDetalle4.addCell(espacio);
            tablaDetalle4.addCell(espacio); // Inserta un renglon
            tablaDetalle4.addCell(espacio);
            tablageneral.addCell(tablaDetalle4);
            
            PdfPTable tablaPie1 = new PdfPTable(1);
            tablaPie1.setWidthPercentage(90); // porcentaje
            tablaPie1.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            tablaPie1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            Font fuentePie = new Font(Font.HELVETICA, 11, Font.NORMAL, new Color(255, 0, 0));
            texto = new Paragraph("Para cualquier duda o aclaraci�n comun�cate a:", fuentePie);
            cell = new PdfPCell(texto);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaPie1.addCell(cell);
            tablaPie1.addCell(espacio);
            tablaPie1.addCell(espacio); // Inserta un renglon
            tablaPie1.addCell(espacio);
			gif = Image.getInstance(Global.OTP_RUTA_PDF_CONTRATO.substring(0, 
					Global.OTP_RUTA_PDF_CONTRATO.lastIndexOf("/") + 1) + "superlinea.jpg");
            cell = new PdfPCell(gif);
            cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaPie1.addCell(cell); 
            tablageneral.addCell(tablaPie1);
            
            
            tablageneral.getDefaultCell().setPadding(6);
            tablageneral.addCell("");
            
            documento.add(tablageneral);
            documento.close();
        }catch(Exception e){
            EIGlobal.mensajePorTrace("Se produjo una excepcion en SUAGenCompPDF [" +
            		e + "]", EIGlobal.NivelLog.ERROR);
            e.printStackTrace();
           // baos = null;
        }
        return baos;

    }


	/**
	 * @param anioPago el anioPago a establecer
	 */
	public void setAnioPago(String anioPago) {
		this._anioPago = anioPago;
	}
	/**
	 * @param claveUsuario el claveUsuario a establecer
	 */
	public void setClaveUsuario(String claveUsuario) {
		this._claveUsuario = claveUsuario;
	}
	/**
	 * @param contrato el contrato a establecer
	 */
	public void setContrato(String contrato) {
		this._contrato = contrato;
	}
	/**
	 * @param fecha el fecha a establecer
	 */
	public void setFecha(String fecha) {
		this._fecha = fecha;
	}
	/**
	 * @param hora el hora a establecer
	 */
	public void setHora(String hora) {
		this._hora = hora;
	}
	/**
	 * @param importeTotal el importeTotal a establecer
	 */
	public void setImporteTotal(double importeTotal) {
		this._importeTotal = importeTotal;
	}
	/**
	 * @return el lineaCaptura
	 */
	public String getLineaCaptura() {
		return this._lineaCaptura;
	}
	/**
	 * @param lineaCaptura el lineaCaptura a establecer
	 */
	public void setLineaCaptura(String lineaCaptura) {
		this._lineaCaptura = lineaCaptura;
	}
	/**
	 * @return el medioPresentacion
	 */
	public String getMedioPresentacion() {
		return this._medioPresentacion;
	}
	/**
	 * @param medioPresentacion el medioPresentacion a establecer
	 */
	public void setMedioPresentacion(String medioPresentacion) {
		this._medioPresentacion = medioPresentacion;
	}
	/**
	 * @return el mesPago
	 */
	public String getMesPago() {
		return this._mesPago;
	}
	/**
	 * @param mesPago el mesPago a establecer
	 */
	public void setMesPago(String mesPago) {
		this._mesPago = mesPago;
	}
	/**
	 * @return el nombreContrato
	 */
	public String getNombreContrato() {
		return this._nombreContrato;
	}
	/**
	 * @param nombreContrato el nombreContrato a establecer
	 */
	public void setNombreContrato(String nombreContrato) {
		this._nombreContrato = nombreContrato;
	}
	/**
	 * @return el nombreUsuario
	 */
	public String getNombreUsuario() {
		return this._nombreUsuario;
	}
	/**
	 * @param nombreUsuario el nombreUsuario a establecer
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this._nombreUsuario = nombreUsuario;
	}
	/**
	 * @return el numeroOperacion
	 */
	public String getNumeroOperacion() {
		return this._numeroOperacion;
	}
	/**
	 * @param numeroOperacion el numeroOperacion a establecer
	 */
	public void setNumeroOperacion(String numeroOperacion) {
		this._numeroOperacion = numeroOperacion;
	}
	/**
	 * @return el registroPatronal
	 */
	public String getRegistroPatronal() {
		return this._registroPatronal;
	}
	/**
	 * @param registroPatronal el registroPatronal a establecer
	 */
	public void setRegistroPatronal(String registroPatronal) {
		this._registroPatronal = registroPatronal;
	}
	/**
	 * @return el folioSUA
	 */
	public String getfolioSUA() {
		return this._folioSUA;
	}
	/**
	 * @param folioSUA el folioSUA a establecer
	 */
	public void setfolioSUA(String folioSUA) {
		this._folioSUA = folioSUA;
	}

	/**
	 * @param cuentaCargo el cuentaCargo a establecer
	 */
	public void setCuentaCargo(String cuentaCargo) {
		this._cuentaCargo = cuentaCargo;
	}

	/**
	 * @param nombreCuenta el nombreCuenta a establecer
	 */
	public void setNombreCuenta(String nombreCuenta) {
		this._nombreCuenta = nombreCuenta;
	}

	/**
	 * @param tipoComprobante el tipoComprobante a establecer ("EXITO"","RECHAZO")
	 */
	public void setTipoComprobante(String tipoComprobante) {
		this._tipoComprobante = tipoComprobante;
	}

	/**
	 * @param msgError el msgError a establecer
	 */
	public void setMsgError(String msgError) {
		this._msgError = msgError;
	}
	

}