package mx.altec.enlace.bo;

import mx.altec.enlace.beans.PagoReferenciadoSatBean;
import mx.altec.enlace.dao.PagoReferenciadoSatDAO;

/**
 * Mejoras de Linea de Captura Clase que consume las operaciones
 * (PagoReferenciadoSatDAO) relacionadas a Pago Referenciado SAT Se obtiene
 * llave de pago, numero de operacion, cuenta, razon social.
 * 
 * @author RRR - Indra Marzo 2014
 */
public class PagoReferenciadoSatBO {

	/**
	 * Se ejecuta el select para obtener el numero de operacion
	 * 
	 * @param referencia
	 *            Referencia
	 * @param ctacargo
	 *            Cuenta Cargo
	 * @param contrato
	 *            Contrato
	 * @param fecha
	 *            Fecha de la operacion
	 * @return String el numero de operacion
	 */
	public String consultaNumeroDeOperacion(String referencia, String ctacargo,
			String contrato, String fecha) {
		PagoReferenciadoSatDAO pagRefSatDao = new PagoReferenciadoSatDAO();
		PagoReferenciadoSatBean pagoRefSatBean = new PagoReferenciadoSatBean();
		pagoRefSatBean.setCuentaCargo(ctacargo);
		pagoRefSatBean.setReferencia(referencia);
		pagoRefSatBean.setContrato(contrato);
		pagoRefSatBean.setFecha(fecha);
		pagoRefSatBean = pagRefSatDao.consultaNumeroDeOperacion(pagoRefSatBean);
		return pagoRefSatBean.getNumOpe();
	}

	/**
	 * Metodo que consulta la razon social de la cuenta cargo para el
	 * comprobante
	 * 
	 * @param ctacargo
	 *            Cuenta Cargo
	 * @param contrato
	 *            Contrato
	 * @return String la razon social de la cta cargo
	 */
	public String consultaRazonSocCtaCargo(String ctacargo, String contrato) {
		PagoReferenciadoSatDAO pagRefSatDao = new PagoReferenciadoSatDAO();
		PagoReferenciadoSatBean pagoRefSatBean = new PagoReferenciadoSatBean();
		pagoRefSatBean.setCuentaCargo(ctacargo);
		pagoRefSatBean.setContrato(contrato);
		pagoRefSatBean = pagRefSatDao.consultaRazonSocCtaCargo(pagoRefSatBean);
		return pagoRefSatBean.getRazonSocCtaCargo();
	}

	/**
	 * Metodo que consume la transaccion LZCP para obtener la llave de pago
	 * 
	 * @param lineaCaptura
	 *            Linea de Captura de la operacion
	 * @param numOp Numero de operacion
	 * @return String la llave de pago
	 */
	public String obtieneLlaveDePago(String lineaCaptura, String numOp) {
		PagoReferenciadoSatDAO pagRefSatDao = new PagoReferenciadoSatDAO();
		PagoReferenciadoSatBean pagoRefSatBean = new PagoReferenciadoSatBean();
		pagoRefSatBean.setLineaDeCaptura(lineaCaptura);
		pagoRefSatBean.setNumOpe(numOp);
		pagoRefSatBean = pagRefSatDao.ejecutaLZCP(pagoRefSatBean);
		
		return pagoRefSatBean.getLlaveDePago() + pagoRefSatBean.getHoraLZCP();
		
	}
}
