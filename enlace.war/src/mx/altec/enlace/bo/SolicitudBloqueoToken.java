/*
 * SolicitudBloqueoToken.java
 *
 * Created on 14 de Noviembre de 2006
 *
 * Clase para crear formato pdf para cancelacion o bloqueo de token
 */

package mx.altec.enlace.bo;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

import mx.altec.enlace.utilerias.Global;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author Praxis - Norberto Vel�zquez Sep�lveda (NVS)
 */
public class SolicitudBloqueoToken {

	private String horaBloqueo = "";
	private String folio = "";
	private String contrato = "";
    private String codCteEmpresa = "";
    private String razonSocial = "";
    private String nombre = "";
    private String codCteUsuario = "";
    private String numSerieToken = "";
    private String representante = "";
    private Calendar fecha = Calendar.getInstance();

    /*public static void main(String[] args) {
        System.out.println("Empieza la Generaci�n de PDF");
        SolicitudBloqueoToken contrato = new SolicitudBloqueoToken();
        contrato.generaSolicitudBloqueo();
    }*/

/**
 * El m�todo generaContrato() genera el PDF
*/
    public ByteArrayOutputStream generaSolicitudBloqueo() {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("M�todo generaContrato()");
        try{
            PdfReader reader = new PdfReader(Global.OTP_RUTA_PDF_SOLBLOQCANC);
            System.out.println("Pags: " + reader.getNumberOfPages());
            Document documento = new Document(PageSize.LETTER,1,1,1,1);
            PdfWriter writer = PdfWriter.getInstance(documento, baos);
            documento.open();

            PdfContentByte cb = writer.getDirectContent();

            PdfImportedPage page = writer.getImportedPage(reader, 1);
            cb.addTemplate(page, 0, 0);

            cb.beginText();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.setFontAndSize(bf, 8);

            //fecha
            cb.setTextMatrix(424, 675);
            cb.showText(fecha.get(fecha.DAY_OF_MONTH)>9?""+fecha.get(fecha.DAY_OF_MONTH):"0"+fecha.get(fecha.DAY_OF_MONTH));
            cb.setTextMatrix(487, 675);
            cb.showText(fecha.get(fecha.MONTH)>8?""+(fecha.get(fecha.MONTH)+1):"0"+(fecha.get(fecha.MONTH)+1));
            cb.setTextMatrix(548, 675);
            cb.showText(""+fecha.get(fecha.YEAR));

            //Hora de Bloqueo
            cb.setTextMatrix(400, 640);
            cb.showText(horaBloqueo);
            //Folio
            cb.setTextMatrix(450, 640);
            cb.showText(folio);
            //Contrato
            cb.setTextMatrix(30, 608);
            cb.showText(contrato);
            //Codigo de cliente de la empresa
            cb.setTextMatrix(130, 608);
            cb.showText(codCteEmpresa);
            //Nombre, Denominacion o Razon Social
            if(razonSocial.length()>0)
                cb.setTextMatrix(208, 608);
            cb.showText(razonSocial);
            //Nombre del usuario
            if(nombre.length()>0){
                if(nombre.length()<=45){
                	cb.setTextMatrix(30, 570);
                    cb.showText(nombre);
                }else{
                    int ind = 0;
                    for(int i=0;i<45;i++){
                        i = nombre.indexOf(" ",i);
                        if(Math.abs(45-i)<Math.abs(45-ind))
                            ind = i;
                    }
                    cb.setTextMatrix(25, 580);
                    cb.showText(nombre.substring(0,ind));
                    cb.setTextMatrix(25, 570);
                    cb.showText(nombre.substring(ind+1));
                }
            }
            //Codigo de cliente del usuario
            cb.setTextMatrix(208, 575);
            cb.showText(codCteUsuario);
            //Numero de serie del token
            cb.setTextMatrix(300, 575);
            cb.showText(numSerieToken);
            //Tache
            cb.setTextMatrix(522, 567);
            cb.showText("X");
            //Representante legal
            cb.setTextMatrix(((page.getWidth()/2)-(representante.length()/2)), 400);
            cb.showText(representante);

            cb.endText();

            documento.close();
        }catch(Exception e){
            e.printStackTrace();
        }
		return baos;
    }

    public void setHoraBloqueo		(String string) { horaBloqueo	= string; }
    public void setFolio			(String string) { folio			= string; }
    public void setContrato			(String string) { contrato		= string; }
    public void setCodCteEmpresa	(String string) { codCteEmpresa	= string; }
    public void setRazonSocial		(String string) { razonSocial	= string; }
    public void setNombre			(String string) { nombre		= string; }
    public void setCodCteUsuario	(String string) { codCteUsuario	= string; }
    public void setNumSerieToken	(String string) { numSerieToken	= string; }
    public void setRepresentante	(String string) { representante	= string; }
    public String getNumSerieToken	()				{ return numSerieToken;	}
}