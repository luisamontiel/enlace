/*
 * bcOperacionProgramadaComparator.java
 *
 * Created on 29 de abril de 2002, 04:09 PM
 */

package mx.altec.enlace.bo;

import mx.altec.enlace.utilerias.EIGlobal;

/** Comparator para bcOperacionProgramada
 *
 * @author Rafael Martinez Montiel
 * @version 1.0
 */
public class bcOperacionProgramadaComparator implements java.util.Comparator{

    /** Creates new bcOperacionProgramadaComparator */
    public bcOperacionProgramadaComparator() {

    }

    /** compara este un par de pdOperacionProgramada
     *@param obj es el primer pdOperacionProgramada
     *@param obj1 es el segundo pdOperacionProgramada
     *@return 1 si obj es mayor que obj1
     *0 si los objetos son iguales
     *-1 si obj1 es mayor que obj
     */
    public int compare(java.lang.Object obj, java.lang.Object obj1) {
        mx.altec.enlace.bo.bcOperacionProgramada a = (mx.altec.enlace.bo.bcOperacionProgramada) obj;
        mx.altec.enlace.bo.bcOperacionProgramada b = (mx.altec.enlace.bo.bcOperacionProgramada) obj1;
        EIGlobal.mensajePorTrace("bcOperacionProgramadaComparator:" + a + " vs " + b , EIGlobal.NivelLog.INFO);
        return a.compareTo(b);
    }

}
