/*
 * Solicitud.java
 *
 * Created on 05 de septiembre de 2006
 *
 * Clase encarga de generar el documento del contrato
 * en formato PDF
 */

package mx.altec.enlace.bo;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Vector;

import mx.altec.enlace.utilerias.Global;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author David Aguilar G�mez. Asteci.
 */
public class Solicitud {
	private Calendar fecha = Calendar.getInstance();
	private String contrato = "";
	private String codCliente = "";
    private String razonSocial = "";
    private String nomRepLegal = "";
    private boolean inicial = true;
    private Vector usuarios = new Vector();

    /*public static void main(String[] args) {
        System.out.println("Empieza la Generaci�n de PDF");
        Solicitud solicitud = new Solicitud();
        solicitud.generaSolicitud();
    }*/

/** El m�todo generaSolicitud() genera el PDF
 * @param seleccion  criterio de busqueda
 * @param datos Valores que se insertara en el PDF
 * @param dato1 Valor que se insertara en el encabezado
 * @param dato2 Valor que se insertara en el encabezado
*/
    public ByteArrayOutputStream generaSolicitud(){
        System.out.println("M�todo Solicitud.generaSolicitud");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try{
            Document documento = new Document(PageSize.LETTER,10,10,25,10);
            PdfWriter writer = PdfWriter.getInstance(documento, baos);
            //PdfWriter writer = PdfWriter.getInstance(documento, new FileOutputStream("Solicitud.pdf"));
            documento.open();

            int paginas = 1;
            int indVector = 0;
            if(usuarios.size()>0)
                paginas = usuarios.size()/15;
            if((usuarios.size()%15)>0)
                paginas++;
            for(int j=0;j<paginas;j++){
                if(j>0) documento.newPage();
                PdfPTable tablageneral = new PdfPTable(1);
                tablageneral.setWidthPercentage(90); // porcentaje
                tablageneral.getDefaultCell().setPadding(0);
                tablageneral.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                tablageneral.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));

                PdfPTable tabla = new PdfPTable(2);
                tabla.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                int columnas[] = { 65,35 }; // porcentaje
                tabla.setWidths(columnas);
				//Image gif = Image.getInstance("/netscape/server4/internet/EnlaceSitio/gifs/EnlaceMig/logo_sant.gif");
				Image gif = Image.getInstance(Global.OTP_RUTA_PDF_LOGO);
                gif.scaleAbsolute(220,34);
                PdfPCell cell = new PdfPCell(gif);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.addCell(cell);
                Font fuente = new Font(Font.HELVETICA, 12, Font.BOLD, new Color(0xFF, 0x00, 0x00));
                Paragraph texto = new Paragraph("Dispositivo Electr�nico (Token)\nBanca Electronica", fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tabla.addCell(cell);
                tablageneral.addCell(tabla);

                tablageneral.getDefaultCell().setPadding(6);
                tablageneral.addCell("");

                tabla = new PdfPTable(3);
                tabla.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.getDefaultCell().setPadding(0);
                int columnas0[] = { 30, 40, 30 }; // porcentaje
                tabla.setWidths(columnas0);
                tabla.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                tabla.addCell("");
                tabla.addCell("");
                PdfPTable tablainterna = new PdfPTable(5);
                tablainterna.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                int columnas1[] = { 30, 1, 38, 1, 30 }; // porcentaje
                tablainterna.setWidths(columnas1);
                tablainterna.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                tablainterna.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                tablainterna.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tablainterna.getDefaultCell().setPadding(1);
                tablainterna.addCell("");
                tablainterna.addCell("");
                tablainterna.addCell("");
                tablainterna.addCell("");
                tablainterna.addCell("");
                tablainterna.getDefaultCell().setPadding(0);
                tablainterna.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                fuente = new Font(Font.HELVETICA, 6, Font.NORMAL, new Color(0x00, 0x00, 0x00));
                texto = new Paragraph("D�A", fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablainterna.addCell(cell);
                tablainterna.addCell("");
                texto = new Paragraph("MES", fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablainterna.addCell(cell);
                tablainterna.addCell("");
                texto = new Paragraph("A�O", fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablainterna.addCell(cell);
                texto = new Paragraph(fecha.get(fecha.DAY_OF_MONTH)>9?""+fecha.get(fecha.DAY_OF_MONTH):"0"+fecha.get(fecha.DAY_OF_MONTH), fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablainterna.addCell(cell);
                tablainterna.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tablainterna.addCell("");
                tablainterna.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph(fecha.get(fecha.MONTH)>8?""+(fecha.get(fecha.MONTH)+1):"0"+(fecha.get(fecha.MONTH)+1), fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablainterna.addCell(cell);
                tablainterna.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tablainterna.addCell("");
                tablainterna.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph(""+fecha.get(fecha.YEAR), fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablainterna.addCell(cell);
                tabla.addCell(tablainterna);
                tablageneral.addCell(tabla);

                tablageneral.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tablageneral.getDefaultCell().setPadding(2.5f);
                tablageneral.addCell("");
                tablageneral.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                tablageneral.getDefaultCell().setPadding(0);

                tabla = new PdfPTable(3);
                float columnas2[] = { 200, 1, 150 };
                tabla.setWidths(columnas2);
                tabla.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                tabla.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                tabla.getDefaultCell().setFixedHeight(12);
                fuente = new Font(Font.HELVETICA, 6.5f, Font.NORMAL, new Color(0x00, 0x00, 0x00));
                texto = new Paragraph("  NOMBRE Y N�MERO DE SUCURSAL / AREA CENTRAL", fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.addCell("");

                tabla.getDefaultCell().setFixedHeight(12);
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                fuente = new Font(Font.HELVETICA, 6.5f, Font.BOLD, new Color(0x00, 0x00, 0x00));
                if(inicial)
                    texto = new Paragraph("( X ) SOLICITUD INICIAL                                  ( ) REPOSICION", fuente);
                else
                    texto = new Paragraph("( ) SOLICITUD INICIAL                                  ( X ) REPOSICION", fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setFixedHeight(2);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                tablageneral.addCell(tabla);

                tabla = new PdfPTable(7);
                tabla.getDefaultCell().setPadding(0);
                float columnas3[] = { 70, 1, 65, 1, 120, 1, 100 };
                tabla.setWidths(columnas3);
                tabla.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                tabla.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
                tabla.getDefaultCell().setPadding(2);
                fuente = new Font(Font.HELVETICA, 6.5f, Font.NORMAL, new Color(0x00, 0x00, 0x00));
                texto = new Paragraph("  No. CONTRATO ENLACE\n\n"+contrato, fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph("  C�DIGO DE CLIENTE\n\n"+codCliente, fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph("  NOMBRE, DENOMINACI�N O RAZ�N SOCIAL\n\n"+razonSocial, fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph("  NOMBRE DEL REPRESENTANTE LEGAL\n\n"+nomRepLegal, fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setFixedHeight(2);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell(texto);
                tablageneral.addCell(tabla);

                tablageneral.getDefaultCell().setPadding(3);
                tablageneral.addCell("");
                tablageneral.getDefaultCell().setPadding(0);

                fuente = new Font(Font.HELVETICA, 6.5f, Font.BOLD, new Color(0xFF, 0x00, 0x00));

                texto = new Paragraph("INFORMACI�N DE (LOS) USUARIOS", fuente);
                tablageneral.addCell(texto);
                fuente = new Font(Font.HELVETICA, 7, Font.NORMAL, new Color(0x00, 0x00, 0x00));

                tablageneral.getDefaultCell().setPadding(3);
                tablageneral.addCell("");
                tablageneral.getDefaultCell().setPadding(0);

                tabla = new PdfPTable(7);
                tabla.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.getDefaultCell().setPadding(1);
                int columnas4[] = { 80, 1, 50, 1, 150, 1, 70 };
                tabla.setWidths(columnas4);
                tabla.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                tabla.getDefaultCell().setFixedHeight(2);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.getDefaultCell().setFixedHeight(20);
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph("TIPO DE SOLICITUD", fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph("C�DIGO DE CLIENTE", fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph("NOMBRE DEL (LOS) USUARIO(S) AUTORIZADO(S)", fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                texto = new Paragraph("N�MERO DE FOLIO SEGURIDAD", fuente);
                tabla.addCell(texto);
                tabla.getDefaultCell().setFixedHeight(2);
                tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.addCell("");
                tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                tabla.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                fuente = new Font(Font.HELVETICA, 8, Font.NORMAL, new Color(0x00, 0x00, 0x00));
                for(int i=0;i<15;i++){
                    String datosUsu[] = {"CANCELADO","CANCELADO","CANCELADO","CANCELADO"};
                    if(indVector<usuarios.size())
                        datosUsu = (String[])usuarios.get(indVector);
                    tabla.getDefaultCell().setFixedHeight(15);
                    texto = new Paragraph(datosUsu[0], fuente);
                    tabla.addCell(texto);
                    tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                    tabla.addCell("");
                    tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                    texto = new Paragraph(datosUsu[1], fuente);
                    tabla.addCell(texto);
                    tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                    tabla.addCell("");
                    tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                    texto = new Paragraph(datosUsu[2], fuente);
                    tabla.addCell(texto);
                    tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                    tabla.addCell("");
                    tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
					texto = new Paragraph(datosUsu[3], fuente);
                    tabla.addCell(texto);
                    tabla.getDefaultCell().setFixedHeight(2);
                    tabla.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                    tabla.addCell("");
                    tabla.addCell("");
                    tabla.addCell("");
                    tabla.addCell("");
                    tabla.addCell("");
                    tabla.addCell("");
                    tabla.addCell("");
                    tabla.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                    indVector++;
                }
                tablageneral.addCell(tabla);

                tablageneral.getDefaultCell().setPadding(3);
                tablageneral.addCell("");
                tablageneral.getDefaultCell().setPadding(0);

                String str01 =  "SE DECLARA Y RATIFICA POR CONDUCTO DE EL O LOS APODERADOS LEGALES QUE FIRMAN AL CALCE, HABER LE�DO Y ENTENDIDO EL CONTRATO �NICO DE "+
                				"PRESTACI�N DE SERVICIOS OTORGADOS A TRAV�S DE MEDIOS ELECTR�NICOS QUE SE TIENEN CELEBRADO CON BANCO SANTANDER, S.A., INSTITUCI�N DE "+
                                "BANCA M�LTIPLE, GRUPO FINANCIERO SANTANDER, POR LO QUE SE HACE CONSTAR LA ENTERA Y EXPRESA CONFORMIDAD CON LAS DEFINICIONES, "+
                                "T�RMINOS Y CONDICIONES QUE AH� APARECEN. DE IGUAL FORMA, SE MANIFIESTA CONOCER Y ENTENDER LOS RIESGOS Y ALCANCES DE LOS SERVICIOS Y "+
                                "OPERACIONES QUE SE PRESTAN O REALIZAN A TRAV�S DE INTERNET, AS� COMO LAS DISPOSICIONES LEGALES QUE EXISTEN AL RESPECTO, POR LO QUE SE "+
                                "ACEPTA QUE ES ENTERA RESPONSABILIDAD DE LA EMPRESA EL USO DE INTERNET Y EN GENERAL EL USO DE MEDIOS ELECTR�NICOS. ADICIONALMENTE, "+
                                "MANIFESTAMOS ASUMIR CUALQUIER RESPONSABILIDAD SOBRE EL EQUIPO DE C�MPUTO CON EL QUE SE LLEGUE A HACER USO DEL SERVICIO ENLACE. DE "+
                                "ACUERDO A LO ANTERIOR, RECONOCEMOS QUE EL HECHO DE FIRMAR ESTE DOCUMENTO ES PRUEBA DE HABER ACEPTADO QUEDAR OBLIGADOS EN LOS "+
                                "T�RMINOS QUE SE ESTABLECEN EN EL YA MENCIONADO CONTRATO.\n";
                String str02 =  "PARA EL CASO DE SOLICITUD DE DISPOSITIVO";
                String str03 =  ", EN ESTE ACTO HACEMOS CONSTAR NUESTRA VOLUNTAD DE DESIGNAR AL USUARIO AQU� IDENTIFICADO, "+
                                "PARA QUE UTILICE UN DISPOSITIVO ELECTR�NICO IDENTIFICADO COMO \"TOKEN\", EL CUAL LES PERMITIR� GENERAR CONTRASE�AS DIN�MICAS PARA EL USO "+
                                "DE MEDIOS ELECTR�NICOS, HACI�NDONOS RESPONSABLES DE SU USO A PARTIR DE LA FECHA EN QUE TAL USUARIO RECIBA EL DISPOSITIVO CORRESPONDIENTE, "+
                                "SEG�N CONSTE EN EL FORMATO IDENTIFICADO COMO \"ACUSE DE RECIBO\" AL EFECTO ESTABLECIDO POR EL BANCO.";
                String str04 =  "PARA EL CASO DE SOLICITUD DE VINCULACION";
                String str05 =  ", EN ESTE ACTO HACEMOS CONSTAR NUESTRA VOLUNTAD DE";
                String str06 =  " AUTORIZAR QUE LOS USUARIOS AQU� IDENTIFICADOS UTILICEN EL DISPOSITIVO ELECTR�NICO IDENTIFICADO COMO \"TOKEN\" QUE LES HA SIDO ENTREGADO "+
                				"EN RELACI�N CON UN CONTRATO DE ENLACE DISTINTO AL IDENTIFICADO EN ESTE DOCUMENTO Y ";
                String str07 =  "EL CUAL LES ";
                String str08 =  "PERMITE ";
                String str09 =  "GENERAR CONTRASE�AS DIN�MICAS PARA EL USO DE MEDIOS ELECTR�NICOS, HACI�NDONOS RESPONSABLES DE SU USO ";
                String str10 =  "EN RELACI�N CON EL CONTRATO DE ENLACE AQU� IDENTIFICADO";
                String str11 =  ", A PARTIR DE ";
                String str12 =  "ESTA FECHA.  CUALQUIERA QUE SEA EL TIPO DE SOLICITUD, AUTORIZAMOS EXPRESAMENTE QUE AL PERFIL DE CADA USUARIO SE ADICIONE LA FACULTAD "+
                				"QUE LE PERMITA OPERAR HACIENDO USO DEL DISPOSITIVO.\n";
                String str13 =  "FINALMENTE, RECONOCEMOS Y ACEPTAMOS EL DERECHO QUE TIENE BANCO SANTANDER, S.A., DE DESACTIVAR LOS DISPOSITIVOS ELECTR�NICOS ASIGNADO A "+
                                "LA PERSONA A LA QUE SE REFIERE EL PRESENTE DOCUMENTO, EN CASO DE NO ENCONTRARSE EN POSIBILIDAD DE VERIFICAR FEHACIENTEMENTE LA "+
                                "PERSONALIDAD CON LA QUE COMPARECEN A LA FIRMA DE ESTE DOCUMENTO EL O LOS APODERADOS LEGALES.";

                fuente = new Font(Font.HELVETICA, 6.5f, Font.NORMAL, new Color(0x00, 0x00, 0x00));

                Chunk cacho01 = new Chunk(str01, fuente);
                Chunk cacho02 = new Chunk(str02, fuente);
                Chunk cacho03 = new Chunk(str03, fuente);
                Chunk cacho04 = new Chunk(str04, fuente);
                Chunk cacho05 = new Chunk(str05, fuente);
                Chunk cacho06 = new Chunk(str06, fuente);
                Chunk cacho07 = new Chunk(str07, fuente);
                Chunk cacho08 = new Chunk(str08, fuente);
                Chunk cacho09 = new Chunk(str09, fuente);
                Chunk cacho10 = new Chunk(str10, fuente);
                Chunk cacho11 = new Chunk(str11, fuente);
                Chunk cacho12 = new Chunk(str12, fuente);
                Chunk cacho13 = new Chunk(str13, fuente);

                texto = new Paragraph(cacho01);
                texto.add(cacho02);
                texto.add(cacho03);
                texto.add(cacho04);
                texto.add(cacho05);
                texto.add(cacho06);
                texto.add(cacho07);
                texto.add(cacho08);
                texto.add(cacho09);
                texto.add(cacho10);
                texto.add(cacho11);
                texto.add(cacho12);
                texto.add(cacho13);

                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                tablageneral.addCell(cell);

                texto = new Paragraph("REPRESENTANTE LEGAL", fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setFixedHeight(55);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablageneral.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                tablageneral.addCell(cell);

                String linea ="\n";
                for (int i=0;i<=nomRepLegal.length();i++) linea = linea + "_";

                texto = new Paragraph(nomRepLegal+linea, fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setFixedHeight(35);
                cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablageneral.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                tablageneral.addCell(cell);

                texto = new Paragraph("NOMBRE Y FIRMA", fuente);
                cell = new PdfPCell(texto);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setFixedHeight(20);
                cell.setVerticalAlignment(Element.ALIGN_TOP);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablageneral.addCell(cell);

                tablageneral.getDefaultCell().setBackgroundColor(new Color(0x00, 0x00, 0x00));
                tablageneral.getDefaultCell().setPadding(2.5f);
                tablageneral.addCell("");
                tablageneral.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
                //gif = Image.getInstance("/netscape/server4/internet/EnlaceSitio/gifs/EnlaceMig/enlace_simple.jpg");
                gif = Image.getInstance(Global.OTP_RUTA_PDF_LOGO_ENLACE);
                gif.scaleAbsolute(100,27);
                cell = new PdfPCell(gif);
                cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                tablageneral.addCell(cell);

                documento.add(tablageneral);
            }
            documento.close();
        }catch(Exception e){
	        System.out.println("Se produjo una excepcion en la clase Solicitud de Enalce Internet");
            //e.printStackTrace();
            baos = null;
        }
	return baos;
    }

	public void setCodCliente(String string) {
		codCliente = string;
	}

	public void setContrato(String string) {
		contrato = string;
    }

    public void setFecha(Calendar calendar) {
    	fecha = calendar;
    }

    public void setNomRepLegal(String string) {
    	nomRepLegal = string;
    }

    public void setRazonSocial(String string) {
    	razonSocial = string;
    }

    public void setInicial(boolean bool) {
    	inicial = bool;
    }

    public void setUsuarios(Vector vector) {
    	usuarios = vector;
    }
}