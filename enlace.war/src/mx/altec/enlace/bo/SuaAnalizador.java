package mx.altec.enlace.bo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.SuaUtil;

public class SuaAnalizador
{
	final String todo = "T";

//	private static int NoEsUnNumero = -1;
//	private static int EsUnNegativo = -2;

	public  static String clave_con = new String("3ABCDEFGHIJKLNMOPQRSTUVWXYZ"); //carecter clave nueva version W3xx
	public  Vector  suaErrores;
	Vector  diasNoHabiles;
	boolean suaProceder;
	int     tipoRegistroActivo;


	public SuaAnalizador( Vector paramDias)

	{
		suaErrores         = new Vector();
		diasNoHabiles      = paramDias;
		tipoRegistroActivo = 1;
		suaProceder        = true;
	}

	public String SubString(String dato, int inicio, int Longitud )
	{
		int fin = inicio + Longitud-1;
		inicio = inicio - 1;
		return( dato.substring( inicio, fin) );
	}


	public boolean comparaCadenas( String srcCadena, String objCadena )
	{
		boolean estado;

		estado = false;
		srcCadena  = srcCadena.trim();
		srcCadena  = srcCadena.toUpperCase();
		objCadena = objCadena.trim();
		objCadena  = objCadena.toUpperCase();

		if ( srcCadena.equals( objCadena ) )
		{
			estado = true;
		}
		else
		{
			estado = false;
		}
		return( estado );
	}

//	public long leerEntero( String dato )
//	{
//		long importe;
//		Long valor;
//
//		try
//		{
//			importe = Long.parseLong(dato.trim());
//		}
//		catch ( Exception exceptionLeer )
//		{
//			importe = 0;
//			System.out.println("Error leer entero: "+ exceptionLeer.getMessage() );
//		}
//		return(importe);
//	}


	public long EvaluaEntero( String dato )
	{
		long importe;

		try
		{
			importe = Long.parseLong( dato );
			if( importe < 0 )
				importe = -2;
		}
		catch( Exception exceptionLeer )
		{
			importe = -1;
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class Excepcion %EvaluaEntero() >> " +					exceptionLeer.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}
		return(importe);
	}

	// VAlidar el Reg. PAtronal
	public boolean EvaluaRegPatronal( String sRegPatronal )
	{
		boolean bOk = false;
		long iItem;
		int i;
		String sAbcd = new String("ABCDEFGHIJKLNMOPQRSTUVWXYZ1234567890"); //carecter clave nueva version W3xx

		try
		{
    		if (sAbcd.indexOf(sRegPatronal.substring(0,1)) >= 0)
			{
				for (i=2;i<12;i++)
					iItem = Long.parseLong(sRegPatronal.substring(i-1,i));
				bOk = true;
			}
		}
		catch( NumberFormatException exceptionLeer )
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class Excepcion %EvaluaRegPatronal() >> " + exceptionLeer.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}
		return(bOk);
	}

	public Calendar EvaluaDiaHabil( Calendar fecha )
	{
		int      indice;
		int      diaDeLaSemana;
		boolean  encontrado;

		/*// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & EvaluaDiaHabil &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Dia  " + fecha.get( fecha.DAY_OF_MONTH) + " &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Mes  " + fecha.get( fecha.MONTH) + " &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Anio " + fecha.get( fecha.YEAR) + " &", EIGlobal.NivelLog.INFO);*/

		if(diasNoHabiles!=null)
		{
			fecha.add( fecha.DAY_OF_MONTH, -1);

			do
			{
				fecha.add( fecha.DAY_OF_MONTH,  1);
/*
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Dia por evaluar &", EIGlobal.NivelLog.INFO);
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Dia  " + fecha.get( fecha.DAY_OF_MONTH ) + " &", EIGlobal.NivelLog.INFO);
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Mes  " + fecha.get( fecha.MONTH ) + " &", EIGlobal.NivelLog.INFO);
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Anio " + fecha.get( fecha.YEAR ) + " &", EIGlobal.NivelLog.INFO);*/

				diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
				encontrado = false;

				for(indice=0; indice<diasNoHabiles.size(); indice++)
				{
					String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
					String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
					String anio = Integer.toString( fecha.get( fecha.YEAR) );
					String strFecha = anio.trim() + "/" + mes.trim() + "/" + dia.trim();

					if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
					{
						// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Dia es inhabil &", EIGlobal.NivelLog.INFO);
						encontrado = true;
						break;
					}
				}
			} while( (diaDeLaSemana == Calendar.SUNDAY)   ||
					 (diaDeLaSemana == Calendar.SATURDAY) ||
					 (diaDeLaSemana == Calendar.FRIDAY)   || encontrado );
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Termina EvaluaDiaHabil &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Dia limite encontrado &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Dia  " + fecha.get( fecha.DAY_OF_MONTH ) + " &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Mes  " + fecha.get( fecha.MONTH ) + " &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Anio " + fecha.get( fecha.YEAR ) + " &", EIGlobal.NivelLog.INFO);
		return fecha;
	}

	public int EvaluaFechaExt( String strFecha, String strPeriodo, double importe )
	{
		int diaLimite = 17;
        	int diaPeriodo;
		int mesPeriodo;
		int anioPeriodo;
		int estado;

		Calendar hoy;
		Calendar fecha;
		Calendar fechaActual;
		Calendar fechaPeriodo;

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "**////////////////**//////////////////////////*SuaAnalizador.class & EvaluaFechaExt &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & strFecha & " +strFecha, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaAnalizador.class & PAGO EXTEMPORANEO DEL IMSS & " +strPeriodo, EIGlobal.NivelLog.INFO);
		estado = 0;
		hoy = new GregorianCalendar();
		fechaActual = new GregorianCalendar( hoy.get(hoy.YEAR), hoy.get(hoy.MONTH), hoy.get( hoy.DAY_OF_MONTH) );

		diaPeriodo  = SuaUtil.Str2int( SubString( strFecha, 7, 2));
		mesPeriodo  = SuaUtil.Str2int( SubString( strFecha, 5, 2));

		anioPeriodo = SuaUtil.Str2int( SubString( strFecha,  1, 4));
		fecha       = new GregorianCalendar(anioPeriodo, mesPeriodo-1, diaPeriodo);//CSA
		//fecha.add( Calendar.MONTH, -1 ); -- CSA
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & fechaActual  = "+fechaActual.getTime()+"\n", EIGlobal.NivelLog.INFO);
       	// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & fecha        = "+fecha.getTime()+"\n", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & Fecha :::::: & "+fecha.get(fecha.DAY_OF_MONTH)+"/"+fecha.get(fecha.MONTH)+"/"+fecha.get(fecha.YEAR)+"\n", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & Fecha Actual:& " + fechaActual.get(fechaActual.DAY_OF_MONTH)+"/" + fechaActual.get(fechaActual.MONTH)+"/" + fechaActual.get(fechaActual.YEAR)+"\n", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & fecha.before(fechaActual) " + fecha.before(fechaActual) + " &", EIGlobal.NivelLog.INFO);


		if(anioPeriodo==2003 && mesPeriodo-1==03&&(diaPeriodo>=17 &&diaPeriodo<=21))
			fecha = new GregorianCalendar(2003,04, 21);
		EIGlobal.mensajePorTrace("CSA - Fecha limite de pago "+fecha.getTime(),EIGlobal.NivelLog.INFO );
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & Fecha Actual:::::::  &"+fecha.get(fecha.DAY_OF_MONTH)+"/"+fecha.get(fecha.MONTH)+"/"+fecha.get(fecha.YEAR)+"\n\n\n\n\n\n\n", EIGlobal.NivelLog.INFO);

		mesPeriodo  = SuaUtil.Str2int( SubString( strPeriodo, 1, 2));
		anioPeriodo = SuaUtil.Str2int( SubString( strPeriodo, 3, 4));

		if(anioPeriodo==2003 && mesPeriodo-1==04 &&(diaPeriodo>=17 &&diaPeriodo<=21))
		{
			fechaPeriodo =  new GregorianCalendar( anioPeriodo, mesPeriodo, 21);
		}
		else
		{
			fechaPeriodo =  new GregorianCalendar( anioPeriodo, mesPeriodo, diaLimite);
		}
		//fechaPeriodo.add( Calendar.MONTH, 1 );
		//Inicia mod. AMR 11/07/2007
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & Fecha Actual:::::::  &"+fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH)+"/"+fechaPeriodo.get(fechaPeriodo.MONTH)+"/"+fechaPeriodo.get(fechaPeriodo.YEAR)+"\n\n\n\n\n\n\n", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***Se asigno & Fecha Periodo & " + fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt Viernes  &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt Sabado  &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt Domingo  &", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt Fecha Periodo es &" + fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK), EIGlobal.NivelLog.INFO);
		if( fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.FRIDAY ||
			fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.SATURDAY||
			fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) ==Calendar.SUNDAY||
			diaLimite==17)
		{
			long ltime = 0;
			Date ldate = new Date();
			ltime = fechaPeriodo.getTime().getTime();//getTimeInMillis();
			if( fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.FRIDAY )
			{
				ltime += 3*24*60*60*1000;
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt Fecha Periodo es Viernes  &", EIGlobal.NivelLog.INFO);
			}
			if( fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.SATURDAY )
			{
				ltime += 2*24*60*60*1000;
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt Fecha Periodo es sabado  &", EIGlobal.NivelLog.INFO);
			}
			if( fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.SUNDAY )
			{
				ltime += 24*60*60*1000;
				// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt Fecha Periodo es domingo  &", EIGlobal.NivelLog.INFO);
			}
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt - 1 fechaPeriodo::::: " + fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH) + "/" + fechaPeriodo.get(fechaPeriodo.MONTH)+ "/" + fechaPeriodo.get(fechaPeriodo.YEAR), EIGlobal.NivelLog.INFO);
			ldate.setTime(ltime);
			fechaPeriodo.setTime(ldate);
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***Se asigno & Fecha Periodo & " + fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt - 2 fechaPeriodo::::: " + fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH) + "/" + fechaPeriodo.get(fechaPeriodo.MONTH)+ "/" + fechaPeriodo.get(fechaPeriodo.YEAR), EIGlobal.NivelLog.INFO);
			CalendarNomina calendario = new CalendarNomina();
			calendario.diasNoHabiles = calendario.CargarDias();
			int rFechaMes = mesPeriodo;
			rFechaMes += 1;
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "**********************************rFechaMes " + rFechaMes , EIGlobal.NivelLog.INFO);
			String rFecha = fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH) + "/" + rFechaMes+ "/" + fechaPeriodo.get(fechaPeriodo.YEAR);
			for (int i=0;i < calendario.diasNoHabiles.size();i++)
			{
				String lFecha = (String) calendario.diasNoHabiles.elementAt(i);
				if(rFecha.equals(lFecha))
				{
					ltime = fechaPeriodo.getTime().getTime();//getTimeInMillis();
					ltime += 24*60*60*1000;
					ldate.setTime(ltime);
					fechaPeriodo.setTime(ldate);
					// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt - 3 fechaPeriodo::::: " + fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH) + "/" + fechaPeriodo.get(fechaPeriodo.MONTH)+ "/" + fechaPeriodo.get(fechaPeriodo.YEAR), EIGlobal.NivelLog.INFO);
					//rFechaMes = fechaPeriodo.get(fechaPeriodo.MONTH);
					//rFechaMes += 1;
					// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "**********************************rFechaMes " + rFechaMes , EIGlobal.NivelLog.INFO);
					rFecha = fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH) + "/" + rFechaMes+ "/" + fechaPeriodo.get(fechaPeriodo.YEAR);
				}
			}
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***Fecha Periodo 1 & " + fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
		}
		//Fin mod. AMR 11/07/2007

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***Fecha 1 & " + fecha.getTime(), EIGlobal.NivelLog.INFO);
		if ( (fecha.before(fechaActual)) && (fecha.before(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & fecha.before(hoy) " + fecha.before(hoy) + " &", EIGlobal.NivelLog.INFO);
			return estado = 1;
		}

		if ( fecha.after(fechaPeriodo) && (fechaActual.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & error en fechas", EIGlobal.NivelLog.INFO);
			return estado = 1;
		}

		if ( fecha.after(fechaActual) && importe <= 0 && (fecha.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & error en fechas", EIGlobal.NivelLog.INFO);
			return estado = 2;
		}

		if (  importe <= 0 && (fecha.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & error en fechas", EIGlobal.NivelLog.INFO);
			return estado = 2;
		}

		if (fecha.before(fechaActual) )
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & fecha.before(fechaActual) " + fecha.before(fechaActual) + " &", EIGlobal.NivelLog.INFO);
			return estado = 1;
		}

		if (  importe <= 0 && (fechaActual.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & error en fechas", EIGlobal.NivelLog.INFO);
			return estado = 2;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & Termina EvaluaFechaExt &", EIGlobal.NivelLog.INFO);
		return estado;
	}

	public int EvaluaFecha( String strFecha, String strPeriodo, double importe )
	{
		int diaLimite = 17;
		int diaPeriodo;
		int mesPeriodo;
		int anioPeriodo;
		int estado;

		Calendar hoy;
		Calendar fecha;
		Calendar fechaActual;
		Calendar fechaPeriodo;

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & EvaluaFecha & xxxxxxxxxxxxxINICIA ------", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & strFecha & " +strFecha, EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & strPeriodo & " +strPeriodo, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace( "***CSA SuaAnalizador.class & PAGO NORMAL DEL IMSS & " +strPeriodo, EIGlobal.NivelLog.INFO);


		estado = 0;
		hoy = new GregorianCalendar();
		//Nota AMR se movio para que funcionaran los archivos del mes pasado, regresar para produccion
		fechaActual = new GregorianCalendar( hoy.get(hoy.YEAR), hoy.get(hoy.MONTH), hoy.get( hoy.DAY_OF_MONTH) );
		//fechaActual = new GregorianCalendar( hoy.get(hoy.YEAR), hoy.get(hoy.MONTH)-1, hoy.get( hoy.DAY_OF_MONTH) );
			diaPeriodo  = SuaUtil.Str2int( SubString( strFecha, 7, 2));
        	mesPeriodo  = SuaUtil.Str2int( SubString( strFecha, 5, 2));

		anioPeriodo = SuaUtil.Str2int( SubString( strFecha,  1, 4));
		fecha = new GregorianCalendar(anioPeriodo, mesPeriodo-1, diaPeriodo); //CSA
		//fecha.add( Calendar.MONTH, -1 );  CSA
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & fechaActual  = "+fechaActual.getTime()+"\n", EIGlobal.NivelLog.INFO);
        // -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & fecha        = "+fecha.getTime()+"\n", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & fecha.before(fechaActual) " + fecha.before(fechaActual) + " &", EIGlobal.NivelLog.INFO);

		if(anioPeriodo==2003 && mesPeriodo-1==03&&(diaPeriodo>=17 &&diaPeriodo<=21))
		{
			fecha = new GregorianCalendar(2003,04, 21);
		}

		EIGlobal.mensajePorTrace("CSA - Fecha limite de pago"+fecha.getTime(),EIGlobal.NivelLog.INFO );

		mesPeriodo  = SuaUtil.Str2int( SubString( strPeriodo, 1, 2));
		anioPeriodo = SuaUtil.Str2int( SubString( strPeriodo, 3, 4));
		if(anioPeriodo==2003 && mesPeriodo-1==04)
		{
			fechaPeriodo =  new GregorianCalendar( anioPeriodo, mesPeriodo-1, 21);
			mesPeriodo = mesPeriodo - 1;
		}
		else
		{
			fechaPeriodo =  new GregorianCalendar( anioPeriodo, mesPeriodo, diaLimite);
		}
		//fechaPeriodo.add( Calendar.MONTH, 1 );
		//Inicia mod. AMR 11/07/2007
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***Se asigno & Fecha Periodo & " + fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
//		if( fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.FRIDAY )
//			fechaPeriodo.add( Calendar.DAY_OF_YEAR, 1 );
		// --------------------------------------------------------------------------
		// -----------------------------------------------------------------
		EIGlobal.mensajePorTrace(
				"LA fechaPeriodo es: " + fechaPeriodo.getTime(),
				EIGlobal.NivelLog.INFO);
		validarDiaInhabil(fechaPeriodo, mesPeriodo);
		EIGlobal.mensajePorTrace(
				"La fecha periodo despues de la validacion de dia inhabil es:"
						+ fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);

		// VAlida que sea un dia diferente a Viernes, SAbado o domingo.
		validarDiaValido(fechaPeriodo, diaLimite);
		validarDiaInhabil(fechaPeriodo, mesPeriodo);
		EIGlobal.mensajePorTrace(
				"Al terminar la validacion del dia,  la fechaPeriodo es: "
						+ fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(
				"Los datos a validar son \n importe:" + importe + "\nfecha: "
						+ fecha.getTime() + "\n fechaActual: "
						+ fechaActual.getTime() + "\n fechaPeriodo: "
						+ fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
		// -----------------------------------------------------------------------
		// -----------------------------------------------------------------------
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & Fecha Disco & " + fecha.getTime(), EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & Fecha Periodo & " + fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & Fecha Actual & " + fechaActual.getTime(), EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "Importe = " + importe, EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace(" fecha.before(fechaActual)= " + fecha.before(fechaActual), EIGlobal.NivelLog.INFO);

		if( fecha.before(fechaActual) )
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & fecha.before(hoy) " + fecha.before(hoy) +" &", EIGlobal.NivelLog.INFO);
			return estado = 1;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace(" fecha.after(fechaPeriodo)= " + fecha.after(fechaPeriodo), EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace(" fechaActual.after(fechaPeriodo)= " + fechaActual.after(fechaPeriodo), EIGlobal.NivelLog.INFO);

		if( fecha.after(fechaPeriodo) && (importe <= 0) && (fechaActual.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & fecha.after(fechaPeriodo) " + fecha.after(fechaPeriodo) + " &", EIGlobal.NivelLog.INFO);
			return estado = 2;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace(" fecha.after(fechaActual)= " + fecha.after(fechaActual), EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace(" fecha.after(fechaPeriodo)= " + fecha.after(fechaPeriodo), EIGlobal.NivelLog.INFO);

		if ( fecha.after(fechaActual) && importe <= 0 && (fecha.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & error en fechas", EIGlobal.NivelLog.INFO);
			return estado = 2;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace(" fecha.after(fechaPeriodo)= " + fecha.after(fechaPeriodo), EIGlobal.NivelLog.INFO);

		if (  importe <= 0 && (fecha.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & error en fechas", EIGlobal.NivelLog.INFO);
			return estado = 2;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "fecha.before(fechaActual) = " + fecha.before(fechaActual), EIGlobal.NivelLog.INFO);

		if (fecha.before(fechaActual) )
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & fecha.before(fechaActual) " + fecha.before(fechaActual) + " &", EIGlobal.NivelLog.INFO);
			return estado = 1;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace(" fechaActual.after(fechaPeriodo)= " + fechaActual.after(fechaPeriodo), EIGlobal.NivelLog.INFO);

		if (  importe <= 0 && (fechaActual.after(fechaPeriodo)))
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFechaExt & error en fechas", EIGlobal.NivelLog.INFO);
			return estado = 2;
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.EvaluaFecha & Termina EvaluaFecha &", EIGlobal.NivelLog.INFO);
		return estado;
	}
	/**
	 *
	 * @param fechaPeriodo
	 */
	private void validarDiaValido(Calendar fechaPeriodo, int diaLimite) {
		EIGlobal.mensajePorTrace("Al inicio de la validacion del dia es : "
				+ fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);

		if (fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.FRIDAY
				|| fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.SATURDAY
				|| fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.SUNDAY
				|| diaLimite == 17) {
			long ltime = 0;
			Date ldate = new Date();
			ltime = fechaPeriodo.getTime().getTime();// getTimeInMillis();
			if (fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.FRIDAY) {
				ltime += 3 * 24 * 60 * 60 * 1000;
				// -- Eliminada por desempe�o en Producci�n --
				// EIGlobal.mensajePorTrace(
				// "***SuaAnalizador.EvaluaFecha Fecha Periodo es Viernes  &",
				// EIGlobal.NivelLog.INFO);
			}
			if (fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.SATURDAY) {
				ltime += 2 * 24 * 60 * 60 * 1000;
				// -- Eliminada por desempe�o en Producci�n --
				// EIGlobal.mensajePorTrace(
				// "***SuaAnalizador.EvaluaFecha Fecha Periodo es sabado  &",
				// EIGlobal.NivelLog.INFO);
			}
			if (fechaPeriodo.get(fechaPeriodo.DAY_OF_WEEK) == Calendar.SUNDAY) {
				ltime += 24 * 60 * 60 * 1000;
				// -- Eliminada por desempe�o en Producci�n --
				// EIGlobal.mensajePorTrace(
				// "***SuaAnalizador.EvaluaFecha Fecha Periodo es domingo  &",
				// EIGlobal.NivelLog.INFO);
			}
			ldate.setTime(ltime);
			fechaPeriodo.setTime(ldate);
		}

		EIGlobal.mensajePorTrace("Al final de la validacion del dia es : "
				+ fechaPeriodo.getTime(), EIGlobal.NivelLog.INFO);
	}

	/**
	 * Metodo que se encarga de validar si el dia es una fecha inhabil
	 *
	 * @param fechaPeriodo
	 * @param mesPeriodo
	 */
	private void validarDiaInhabil(Calendar fechaPeriodo, int mesPeriodo) {
		// OBTIENE LOS DIAS INHABILES EN LA BD
		CalendarNomina calendario = new CalendarNomina();
		calendario.diasNoHabiles = calendario.CargarDias();
		int rFechaMes = mesPeriodo;
		rFechaMes += 1;
		long ltime = 0;
		Date ldate = new Date();

		EIGlobal.mensajePorTrace(
				"Entrando a validar dia inhabil donde fchPeriodo: "
						+ fechaPeriodo.getTime() + " y mes Periodo: "
						+ mesPeriodo, EIGlobal.NivelLog.INFO);

		String rFecha = fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH) + "/"
				+ rFechaMes + "/" + fechaPeriodo.get(fechaPeriodo.YEAR);

		for (int i = 0; i < calendario.diasNoHabiles.size(); i++) {
			String lFecha = (String) calendario.diasNoHabiles.elementAt(i);

			if (rFecha.equals(lFecha)) {

				EIGlobal.mensajePorTrace(
						"La fechaPeriodo: " + fechaPeriodo.getTime()
								+ " es INHABIL", EIGlobal.NivelLog.INFO);

				ltime = fechaPeriodo.getTime().getTime();
				ltime += 24 * 60 * 60 * 1000;
				ldate.setTime(ltime);
				fechaPeriodo.setTime(ldate);

				rFecha = fechaPeriodo.get(fechaPeriodo.DAY_OF_MONTH) + "/"
						+ rFechaMes + "/" + fechaPeriodo.get(fechaPeriodo.YEAR);
				EIGlobal.mensajePorTrace("recalculando la nueva fecha es_"
						+ rFecha, EIGlobal.NivelLog.INFO);
			}
		}
		EIGlobal.mensajePorTrace("Saliendo de la validacion de dia inhabil",
				EIGlobal.NivelLog.INFO);
	}
	//VSWF RRG I 19-Mayo-2008 AMX-08-00644-0A Se agregaron estas funciones para IMSS y Infonavit
	/*@ Metodo que valida si una cadena contiene caracteres invalidos
	 *
	 */
	//@SuppressWarnings("finally");
	public boolean tieneCaracteresValidos(String cadena)
	{
		boolean res=false;
		char c=' ';
		try
		{
			for(int i=0;i<cadena.length();i++)
			{
				c=cadena.charAt(i);
				if(( (c==32)||(c==33)||(c==34)||(c==35)||(c==36)||(c==37)||(c==38)||(c==42)||(c==46)||(c==47)||
						(c>=48 && c <=57)||(c==58)||(c==59)||(c==60)||(c==61)||(c==62)||(c==64)||(c>=65 && c <=90)||
						(c==91)||(c==92)||(c==93)||(c==95)||(c==96)||(c>=97 && c<=122)||(c==123)||(c==125) ))
				{
					res=true;
				}
				else
				{
					res=false;
					break;
				}
			}
		}
		catch (Exception e)
		{
			//System.out.println("Error en em metodo tieneCaracteresValidos: "+e.getMessage());
		}
			return res;
	}

	//VSWF RRG F 19-Mayo-2008 AMX-08-00644-0A
	public void ProcesaRegistroDOS( SuaRegistro rsDatos )
	{
		String dato = "";
		SuaErrorDisco seError    = new SuaErrorDisco();
		String AreaGeoSalarios   = new String("ABC");
		String ReembosoSubsidios = new String("SN");
		String TipoCotiza        = new String("23");

		//GAO
		String fechalarga="";
		EIGlobal.mensajePorTrace( "***SuaAnalizador.class & ENTRO AL REGISTRO DOS A VALIAR ARCHIVO", EIGlobal.NivelLog.INFO);
		try
		{
			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & ProcesaRegistroDOS &", EIGlobal.NivelLog.INFO);
			if(rsDatos.regDosImssPatronal.trim().length()!=11)
			{
				seError.asignaError(1026, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

		}
		catch(Exception e)
		{
			//System.out.println("[rsDatos.regDosImssPatronal.length()!=11]: "+e.getMessage());
		}
		try
		{
			// validar el registro patronal sua 2005
			if (!EvaluaRegPatronal(rsDatos.regDosImssPatronal.trim()))
			{
				seError.asignaError(1026, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[validar el registro patronal sua 2005]: "+e.getMessage());
		}
		try
		{
			//RFC Patr�n
			if( SuaUtil.validaRFCTrabajador(rsDatos.regDosRfcEmpresa) != 0 )
			{
				seError.asignaError(2002, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			/*if(EvaluaEntero(rsDatos.regDosTipoCotizacion) < 2 )
			{
				seError.asignaError(1014, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			if(EvaluaEntero(rsDatos.regDosTipoCotizacion) > 3 )
			{
				seError.asignaError(1014, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}*/
		}
		catch(Exception e)
		{
			//System.out.println("[RFC Patr�n]: "+e.getMessage());
		}
		try
		{
			fechalarga = rsDatos.regDosAnioDePago + rsDatos.regDosPeriodoDePago;
			if(EvaluaEntero(fechalarga) <= 199707)
			{
				seError.asignaError(1011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[EvaluaEntero(fechalarga) <= 199707]: "+e.getMessage());
		}
		try
		{
			if(EvaluaEntero(rsDatos.regDosPeriodoDePago) < 1 || EvaluaEntero(rsDatos.regDosPeriodoDePago) > 12)
			{
				seError.asignaError(1011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			/*valida arriba GAO
			if(EvaluaEntero(rsDatos.regDosPeriodoDePago) > 12 )
			{
				seError.asignaError(1011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			*/
			/*valida arriba GAO
			if(EvaluaEntero(rsDatos.regDosAnioDePago) <= 1997 )
			{
				seError.asignaError(1011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		*/
		}
		catch(Exception e)
		{
			//System.out.println("[EvaluaEntero(rsDatos.regDosPeriodoDePago) < 1 || EvaluaEntero(rsDatos.regDosPeriodoDePago) > 12]: "+e.getMessage());
		}
		try
		{
			// valida FolioSUA
			//if(EvaluaEntero(rsDatos.regDosFolioSua) < 0 )
			if (SuaUtil.ValidaFormato("888888", rsDatos.regDosFolioSua) != 0)
			{
				seError.asignaError(1068, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida FolioSUA]: "+e.getMessage());
		}
		StringBuffer frmt = new StringBuffer("");
		try
		{
			// valida Nombre o Raz�n Social
//			for(int i=0; i<50; i++)	frmt.append(todo);

//			if( SuaUtil.ValidaFormato(frmt, rsDatos.regDosRazonSocial) != 0  )
//			{
//				seError.asignaError(6002, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
//				throw new SUAException(); // suaErrores.addElement(seError);
//			}

			if (SuaUtil.ValidaNombreEmpleado(rsDatos.regDosRazonSocial) == 3){
				seError.asignaError(2100, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Nombre o Raz�n Social]: "+e.getMessage());
		}

		try
		{
			// valida Direcci�n
			frmt.delete(0, frmt.length());
			for(int i=0;i<40;i++) { frmt.append(todo); }
			if( SuaUtil.ValidaFormato(frmt.toString(), rsDatos.regDosDireccion) != 0  )
			{
				seError.asignaError(1078, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Direcci�n]: "+e.getMessage());
		}
		try
		{
			// valida Poblaci�n
			frmt.delete(0, frmt.length());
			for(int i=0;i<40;i++)
			{
				frmt.append(todo);
			}
			if( SuaUtil.ValidaFormato(frmt.toString(), rsDatos.regDosPoblacion) != 0  )
			{
				seError.asignaError(1078, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Plobalcion]: "+e.getMessage());
		}
		try
		{
			// valida Estado
			if( SuaUtil.ValidaFormato("88", rsDatos.regDosEstado) != 0  )
			{
				seError.asignaError(6004, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Estado]: "+e.getMessage());
		}
		try
		{
			// valida CodigoPostal
			if( SuaUtil.ValidaFormato("88888", rsDatos.regDosCodigoPostal) != 0  )
			{
				seError.asignaError(6005, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida CodigoPostal]: "+e.getMessage());
		}
		//31032009 AMR se quita la validacion
		//try
		//{
			// valida Tel�fono

			//13032009 Modificacion AMR: se correige validaci�n
			//if(rsDatos.regDosTelefono!=null && rsDatos.regDosTelefono.trim().length()>0 && SuaUtil.validaNumericoEspacio(rsDatos.regDosTelefono))
		//	if(rsDatos.regDosTelefono==null || (rsDatos.regDosTelefono.trim().length()>0 && !SuaUtil.validaNumericoEspacio(rsDatos.regDosTelefono.trim())))
		//	{
		//		seError.asignaError(1078, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
		//		throw new SUAException(); // suaErrores.addElement(seError);
		//	}
		//}
		//catch(Exception e)
		//{
		//	System.out.println("[valida Tel�fono]: "+e.getMessage());
		//}

		try
		{
			// valida Prima de Riesgo de Trabajo
			if( SuaUtil.ValidaFormato("8888888", rsDatos.regDosPrimRiesgoT) != 0  )
			{
				seError.asignaError(6006, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Prima de Riesgo de Trabajo]: "+e.getMessage());
		}

		try
		{
			// valida Fecha de la Prima de Riesgo de Trabajo
			if( !SuaUtil.ValidaFechaAAAAMM(rsDatos.regDosFechaPRT) && (SuaUtil.Str2int(rsDatos.regDosFechaPRT)!= 0))
			{
				seError.asignaError(1078, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Fecha de la Prima de Riesgo de Trabajo]: "+e.getMessage());
		}

		try
		{
			// valida Actividad Econ�mica
			frmt.delete(0, frmt.length());
			for(int i=0;i<40;i++) { frmt.append(todo); }
			if( SuaUtil.ValidaFormato(frmt.toString(), rsDatos.regDosActividadEco) != 0  )
			{
				seError.asignaError(1078, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Actividad Econ�mica]: "+e.getMessage());
		}

		try
		{
			// valida Delegaci�n IMSS
			if( SuaUtil.ValidaFormato("88", rsDatos.regDosDelegIMSS) != 0  )
			{
				seError.asignaError(6007, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Delegaci�n IMSS]: "+e.getMessage());
		}

		try
		{
			// valida SubDelegaci�n IMSS
			if( SuaUtil.ValidaFormato("88", rsDatos.regDosSubDelegIMSS) != 0  )
			{
				seError.asignaError(6008, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida SubDelegaci�n IMMS]: "+e.getMessage());
		}
			dato = SubString(  rsDatos.NombreArchivo, 1, 1 );
			dato = dato.toUpperCase();
		try
		{
			//if( (!dato.equals("W")) && (!dato.equals("D")) && (!dato.equals("E")) )
			/** Se validaran todos los caracteres de la "A" a la "Z" (NoCaseSensitive)**/
			if(("ABCDEFGHIJKLMNOPQRSTUVWXYZ").indexOf(dato)<0)
			{
				seError.asignaError(1006, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[(ABCDEFGHIJKLMNOPQRSTUVWXYZ).indexOf(dato)<0]: "+e.getMessage());
		}
			dato = SubString( rsDatos.NombreArchivo, 2, 7);
			dato = dato.toUpperCase();

		try
		{
			if(!dato.equals(rsDatos.regDosNombreArchivo.toUpperCase()))
			{
				seError.asignaError(2202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[!dato.equals(rsDatos.regDosNombreArchivo.toUpperCase())]: "+e);
		}

		//AMR del nombre del archivo
		try {
			if(rsDatos.NombreArchivo != null && rsDatos.NombreArchivo.trim().length() >= 8
				&& rsDatos.regDosNombreArchivo != null)
			{

				if (!rsDatos.regDosNombreArchivo.equals(rsDatos.NombreArchivo.substring(1,8)))
				{
					seError.asignaError(1011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
				else if (SuaUtil.ValidaFormato("L", rsDatos.NombreArchivo.substring(0,1)) != 0)
				{
					seError.asignaError(1011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}
			else if (SuaUtil.ValidaFormato("L", rsDatos.NombreArchivo.substring(0,1)) != 0)
			{
				seError.asignaError(1011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(rsDatos.regDosImssPatronal.equals("Z9588888107"))
			{
				seError.asignaError(1005, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[AMR del nombre del archivo Inicio]: "+e.getMessage());
		}

		//Bloque: Validacion Nuevos campos W3xx
		try {
			if(AreaGeoSalarios.indexOf(rsDatos.regDosAreaGeoSalMini.substring(0,1))<0)
			{
				//VSWF RRG I 15-Mayo-2008 AMX-08-00644-0A Se modifico el codigo de error para IMSS y Infonavit
				seError.asignaError(1059, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				//	VSWF RRG F 15-Mayo-2008 AMX-08-00644-0A
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[Validacion Nuevos campos W3xx]: "+e.getMessage());
		}

		try {
			if(EvaluaEntero(rsDatos.regDosPorcAportInfo) < 0)
			{
				seError.asignaError(1045, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[Validacion Nuevos campos W3xx - EvaluaEntero(rsDatos.regDosPorcAportInfo) < 0]: "+e.getMessage());
		}

		try {
			if(ReembosoSubsidios.indexOf(rsDatos.regDosConvReembSubs.substring(0,1))<0)
			{
				//VSWF RRG I 15-Mayo-2008 AMX-08-00644-0A Se modifico el codigo de error para IMSS y Infonavit
				seError.asignaError(1060, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				//VSWF RRG F 15-Mayo-2008 AMX-08-00644-0A
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[Validacion Nuevos campos W3xx - ReembosoSubsidios.indexOf(rsDatos.regDosConvReembSubs.substring(0,1))<0]: "+e.getMessage());
		}

		// Tipo de Cotizaci�n
		try {
			if(TipoCotiza.indexOf(rsDatos.regDosTipoCotizacion.substring(0,1))<0)
			{
				seError.asignaError(1014, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[Tipo de Cotizaci�n]: "+e.getMessage());
		}

		//	valida Total de d�as Cotizados en el Periodo
		try {
			if( SuaUtil.ValidaFormato("8888888", rsDatos.regDosDiasCotizadosPeriodo) != 0  )
			{
				seError.asignaError(1062, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Total de d�as Cotizados en el Periodo]: "+e.getMessage());
		}

		//	valida N�mero de trabajadores cotizantes
		try {
			if( SuaUtil.ValidaFormato("888888888", rsDatos.regDosTrabajadores) != 0  )
			{
				seError.asignaError(1064, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida N�mero de trabajadores cotizantes]: "+e.getMessage());
		}


		//	valida Tipo de Documento
		try {
			if( SuaUtil.ValidaFormato("88", rsDatos.regDosTipodeDoc) != 0  )
			{
				seError.asignaError(1062, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida Tipo de Documento]: "+e.getMessage());
		}

		// valida N�mero de Cr�dito
		try {
			if( SuaUtil.ValidaFormato("888888888", rsDatos.regDosNumdeCredito) != 0  )
			{
				seError.asignaError(1062, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[valida N�mero de Cr�dito]: "+e.getMessage());
		}

		//VSWF RRG I 15-Mayo-2008 AMX-08-00644-0A Se agregaron estas validaciones para IMSS y Infonavit

		try {
			dato = SubString( rsDatos.NombreArchivo, rsDatos.NombreArchivo.indexOf(".")+2, 3);
			dato = dato.toUpperCase();

			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class ExtencionArch "+dato+"", EIGlobal.NivelLog.INFO);

			if(!dato.equals("SUA") && !dato.equals("SU0") && !dato.equals("SU1") && !dato.equals("SU2") &&
				!dato.equals("SU3") && !dato.equals("SU4") && !dato.equals("SU5") && !dato.equals("SU6") &&
				!dato.equals("SU7") && !dato.equals("SU8") && !dato.equals("SU9"))
			{
				seError.asignaError(1007, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("[AMX-08-00644-0A Se agregaron estas validaciones para IMSS y Infonavit]: "+e.getMessage());
		}

		//VSWF RRG F 15-Mayo-2008
		//Fin Bloque
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Termina ProcesaRegistroDOS &", EIGlobal.NivelLog.INFO);
}

	public void ProcesaRegistroTRES( SuaRegistro rsDatos )
	{
		SuaErrorDisco seError = new SuaErrorDisco();
		EIGlobal.mensajePorTrace( "***SuaAnalizador.class & ENTRO AL REGISTRO TRES A VALIAR ARCHIVO", EIGlobal.NivelLog.INFO);
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class # ProcesaRegistroTRES #", 5);

		try
		{
			/// Nueva Validacion Modificar Variable de error
			if(!rsDatos.regDosRfcEmpresa.equals(rsDatos.regTresRFCPatron))
			{
				seError.asignaError(1079, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}


			long tiempo = System.currentTimeMillis();
//			int c=0;
//			for(int i=0;i<rsDatos.regTresConcetradoNumIMSS.size();i++)
//			{
//				if(rsDatos.regTresConcetradoNumIMSS.get(i).equals(rsDatos.regTresImssTrabajador))
//				{
//					c++;
//				}
//				if(c>1)
//				{
//					seError.asignaError(1080, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
//					throw new SUAException(); // suaErrores.addElement(seError);
//				}
//			}

			if ( !rsDatos.regTresConcetradoNumIMSS.contains(rsDatos.regTresImssTrabajador) ) {
				seError.asignaError(1080, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			//System.out.println("__________________________________________________________Tiempo de busqueda de ArrayList: " + (System.currentTimeMillis() - tiempo) );

//Modificacion AMR 04/Septiembre/2008
			if( (rsDatos.regTresRFCTrabajador.trim().length() > 0) && (SuaUtil.validaRFCTrabajador(rsDatos.regTresRFCTrabajador)!=0) )
			{
				seError.asignaError(1081, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
//fin de la modificaicon AMR 04/Septiembre/2008


// Inicia Modificacion AMR 14/08/2008  Se quita la validacion de CURP
//			if(!rsDatos.regTresCURPTrabajador.trim().equals("") && SuaUtil.ValidaFormato("LLLL888888LLLLLL88", rsDatos.regTresCURPTrabajador)!=0)
//			{
//				seError.asignaError(1082, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
//				throw new SUAException(); // suaErrores.addElement(seError);
//			}
// fin de mod amr
			if(!SuaUtil.validaNumericoEspacio(rsDatos.regTresCreditoInfonavit))
			{
				seError.asignaError(1083, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if((!SuaUtil.ValidaFechaAAAAMMDD(rsDatos.regTresFecINFONAVITTrabajador)) && (SuaUtil.Str2int(rsDatos.regTresFecINFONAVITTrabajador)!= 0 ))
			{
				seError.asignaError(1084, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("88", rsDatos.regTresMovPeriodo)!=0)
			{
				seError.asignaError(1085, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8", rsDatos.regTresTipoTrabajador)!=0)
			{
				seError.asignaError(1086, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if((SuaUtil.ValidaFormato("8", rsDatos.regTresJornadaRedTrabajador) != 0) || (SuaUtil.Str2int(rsDatos.regTresJornadaRedTrabajador) >= 7) )
			{
				seError.asignaError(1087, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			/* Validaci�n de duplicados */
//			int cont=0;
//			for (Iterator lista =  rsDatos.regTresConcetradoNumIMSS.iterator();lista.hasNext();) {
//
//				Object o = lista.next();
//
//				for (Iterator tmp =  rsDatos.regTresConcetradoNumIMSS.iterator();tmp.hasNext();)
//					if (o.equals(tmp.next()))
//						cont++;
//
//				if (cont>1){
//					seError.asignaError(2204, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
//					throw new SUAException(); // suaErrores.addElement(seError);
//				}
//
//				cont =0;
//
//			}


			if(SuaUtil.ValidaNombreEmpleado(rsDatos.regTresNombreTrabajador)==5)
			{
				seError.asignaError(2203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaNombreEmpleado(rsDatos.regTresNombreTrabajador)==1)
			{
				seError.asignaError(1088, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			if(SuaUtil.ValidaNombreEmpleado(rsDatos.regTresNombreTrabajador)==3)
			{
				seError.asignaError(1089, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			///////////////////////////////////////////
			if( SuaUtil.ValidaFormato("88",rsDatos.regTresDiasMensualCotiza) != 0 )
			{
				seError.asignaError(1290, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( SuaUtil.ValidaFormato("88",rsDatos.regTresDiasBimestreCotiza) != 0)
			{
				seError.asignaError(6012, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresEnfermedadCuota) != 0)
			{
				seError.asignaError(1291, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresEnfermedadExcedente) != 0)
			{
				seError.asignaError(1292, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresEnfermedadPrestaciones) != 0)
			{
				seError.asignaError(1293, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresGastosMedicos) != 0)
			{
				seError.asignaError(1294, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresRiesgosDeTrabajo) != 0)
			{
				seError.asignaError(1295, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresInvalidezVida) != 0)
			{
				seError.asignaError(6009, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresGuarderiaPrestaciones) != 0)
			{
				seError.asignaError(6010, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresSegurosImss) != 0)
			{
				seError.asignaError(6011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresRetiroCuota) != 0)
			{
				seError.asignaError(6015, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresRetiroRecargos) != 0)
			{
				seError.asignaError(6016, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresCesantiaVejezPatronal) != 0)
			{
				seError.asignaError(6017, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresCesantiaVejezTrabajador) != 0)
			{
				seError.asignaError(2028, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresCesantiaVejezRecargos) != 0)
			{
				seError.asignaError(6019, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresAportacionVoluntaria) != 0)
			{
				seError.asignaError(2034, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresAportacionPatronal) != 0)
			{
				seError.asignaError(1096, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresAmortizacionCredito) != 0)
			{
				seError.asignaError(2038, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( !( rsDatos.regTresImssPatronal.equals(rsDatos.regDosImssPatronal.toUpperCase() ) ) )
			{
				seError.asignaError(1072, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( !( rsDatos.regTresPeriodoPago.equals(rsDatos.regDosAnioDePago + rsDatos.regDosPeriodoDePago) ) )
			{
				seError.asignaError(1017, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}


			if( SuaUtil.ValidaFormato("88888888888",rsDatos.regTresImssTrabajador) != 0)
			{
				seError.asignaError(1018, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Bloque: Nuevas validaciones, registro 3
			if(SuaUtil.ValidaFormato("8888888",rsDatos.regTresUltSalDiaIntPer) != 0)
			{
				seError.asignaError(1048, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("88",rsDatos.regTresDiasIncaMes) != 0)
			{
				seError.asignaError(1049, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("88",rsDatos.regTresDiasAusenteMes) != 0)
			{
				seError.asignaError(1050, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("88",rsDatos.regTresDiasIncaBimestre) != 0)
			{
				seError.asignaError(1051, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.ValidaFormato("88",rsDatos.regTresDiaAusenteBimestre) != 0)
			{
				seError.asignaError(1052, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("Eror en registro 3"+e.getMessage());
		}

		//fin bloque
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class # Termina ProcesaRegistroTRES #", 5);
	}

	public void ProcesaRegistroCUATRO( SuaRegistro rsDatos )
	{
		SuaErrorDisco seError = new SuaErrorDisco();

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & ProcesaRegistroCUATRO &", EIGlobal.NivelLog.INFO);

		try
		{
			ArrayList nss = rsDatos.regTresConcetradoNumIMSS;

			if(rsDatos.regCuatroImssTrabajador != null && !rsDatos.regCuatroImssTrabajador.trim().equals(""))
			{
				//System.out.println("[Registro 4] Verificacion Trabajador 1");
				if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCuatroImssTrabajador) != 0 ||
						!nss.contains(rsDatos.regCuatroImssTrabajador))
				{
					seError.asignaError(1203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("88", rsDatos.regCuatroTipoMovIncidencia1) != 0)
				{
					seError.asignaError(1201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if (SuaUtil.ValidaFormato("88", rsDatos.regCuatroDiasIncidencia1) != 0) {
					seError.asignaError(2211, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("8888888", rsDatos.regCuatroSalarioDiarioInt1) != 0)
				{
					seError.asignaError(1202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}

			if(rsDatos.regCuatroImssTrabajador2 != null && !rsDatos.regCuatroImssTrabajador2.trim().equals(""))
			{
				//System.out.println("[Registro 4] Verificacion Trabajador 2");
				if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCuatroImssTrabajador2) != 0 ||
						!nss.contains(rsDatos.regCuatroImssTrabajador2))
				{
					seError.asignaError(1203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
				if(SuaUtil.ValidaFormato("88", rsDatos.regCuatroTipoMovIncidencia2) != 0)
				{
					seError.asignaError(1201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if (SuaUtil.ValidaFormato("88", rsDatos.regCuatroDiasIncidencia2) != 0) {
					seError.asignaError(2211, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("8888888", rsDatos.regCuatroSalarioDiarioInt2) != 0)
				{
					seError.asignaError(1202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}

			if(rsDatos.regCuatroImssTrabajador3 != null && !rsDatos.regCuatroImssTrabajador3.trim().equals("") )
			{
				//System.out.println("[Registro 4] Verificacion Trabajador 3");
				if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCuatroImssTrabajador3) != 0 ||
						!nss.contains(rsDatos.regCuatroImssTrabajador3))
				{
					seError.asignaError(1203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("88", rsDatos.regCuatroTipoMovIncidencia3) != 0)
				{
					seError.asignaError(1201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if (SuaUtil.ValidaFormato("88", rsDatos.regCuatroDiasIncidencia3) != 0) {
					seError.asignaError(2211, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("8888888", rsDatos.regCuatroSalarioDiarioInt3) != 0)
				{
					seError.asignaError(1202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}

			if(rsDatos.regCuatroImssTrabajador4 != null && !rsDatos.regCuatroImssTrabajador4.trim().equals(""))
			{
				//System.out.println("[Registro 4] Verificacion Trabajador 4");
				if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCuatroImssTrabajador4) != 0 ||
						!nss.contains(rsDatos.regCuatroImssTrabajador4))
				{
					seError.asignaError(1203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("88", rsDatos.regCuatroTipoMovIncidencia4) != 0)
				{
					seError.asignaError(1201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if (SuaUtil.ValidaFormato("88", rsDatos.regCuatroDiasIncidencia4) != 0) {
					seError.asignaError(2211, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("8888888", rsDatos.regCuatroSalarioDiarioInt4) != 0)
				{
					seError.asignaError(1202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}
			if(rsDatos.regCuatroImssTrabajador5 != null && !rsDatos.regCuatroImssTrabajador5.trim().equals(""))
			{
				//System.out.println("[Registro 4] Verificacion Trabajador 5");
				if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCuatroImssTrabajador5) != 0 ||
						!nss.contains(rsDatos.regCuatroImssTrabajador5))
				{
					seError.asignaError(1203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("88", rsDatos.regCuatroTipoMovIncidencia5) != 0)
				{
					seError.asignaError(1201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if (SuaUtil.ValidaFormato("88", rsDatos.regCuatroDiasIncidencia5) != 0) {
					seError.asignaError(2211, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("8888888", rsDatos.regCuatroSalarioDiarioInt5) != 0)
				{
					seError.asignaError(1202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}
			if(rsDatos.regCuatroImssTrabajador6 != null && !rsDatos.regCuatroImssTrabajador6.trim().equals(""))
			{
				//System.out.println("[Registro 4] Verificacion Trabajador 6");
				if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCuatroImssTrabajador6) != 0 ||
						!nss.contains(rsDatos.regCuatroImssTrabajador6))
				{
					seError.asignaError(1203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("88", rsDatos.regCuatroTipoMovIncidencia6) != 0)
				{
					seError.asignaError(1201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if (SuaUtil.ValidaFormato("88", rsDatos.regCuatroDiasIncidencia6) != 0) {
					seError.asignaError(2211, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("8888888", rsDatos.regCuatroSalarioDiarioInt6) != 0)
				{
					seError.asignaError(1202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}
			if(rsDatos.regCuatroImssTrabajador7 != null && !rsDatos.regCuatroImssTrabajador7.trim().equals(""))
			{
				//System.out.println("[Registro 4] Verificacion Trabajador 7");
				if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCuatroImssTrabajador7) != 0 ||
						!nss.contains(rsDatos.regCuatroImssTrabajador7))
				{
					seError.asignaError(1203, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("88", rsDatos.regCuatroTipoMovIncidencia7) != 0)
				{
					seError.asignaError(1201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if (SuaUtil.ValidaFormato("88", rsDatos.regCuatroDiasIncidencia7) != 0) {
					seError.asignaError(2211, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}

				if(SuaUtil.ValidaFormato("8888888", rsDatos.regCuatroSalarioDiarioInt7) != 0)
				{
					seError.asignaError(1202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}
			//	VSWF RRG I 07-Julio-2008 AMX-08-00644-0A Se agregaron estas validaciones para IMSS y Infonavit

			//Registro patronal IMSS
			if(!tieneCaracteresValidos(rsDatos.regCuatroImssPatronal))
			{
				seError.asignaError(1077, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			if( ! comparaCadenas( rsDatos.regDosImssPatronal, rsDatos.regCuatroImssPatronal ))
			{
				seError.asignaError(1070, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

		}
		catch(Exception e)
		{
			//System.out.println("Error en registro CUATRO: "+e.getMessage());
		}
		//System.out.println("ProcesaRegistroCUATRO");
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Termina ProcesaRegistroCUATRO &", EIGlobal.NivelLog.INFO);
	}

	public void ProcesaRegistroCINCO(SuaRegistro rsDatos, SuaImpuesto riTotal)
	{
		int iSuma = 0;
		int iAcumulado = 0;
		SuaErrorDisco seError = new SuaErrorDisco();

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & ProcesaRegistroCINCO &", EIGlobal.NivelLog.INFO);
		try
		{

			//Tipo de Registro
			if(!rsDatos.regCincoTipoRegistro.equals("05"))
			{
				seError.asignaError(1071, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Registro patronal IMSS
			if(!rsDatos.regDosImssPatronal.equals(rsDatos.regCincoImssPatronal))
			{
				seError.asignaError(1016, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			//RFC Patr�n Registro 05
			if(!rsDatos.regCincoRfcEmpresa.equals(rsDatos.regDosRfcEmpresa))
			{
				seError.asignaError(1073, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			//Periodo de pago
			if(!rsDatos.regCincoPeriodoDePago.equals(rsDatos.regDosAnioDePago + rsDatos.regDosPeriodoDePago))
			{
				seError.asignaError(1074, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Folio SUA
			if(!rsDatos.regDosFolioSua.equals(rsDatos.regCincoFolioSua))
			{
				seError.asignaError(1075, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			//Tasa de actualizacion empleada
			if(SuaUtil.ValidaFormato("8888888888", rsDatos.regCincoTasaActualizacion) != 0)
			{
				seError.asignaError(2005, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Tasa de recargos empleada
			if(SuaUtil.ValidaFormato("8888888888", rsDatos.regCincoTasaRecargos) != 0)
			{
				seError.asignaError(2006, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Cuota fija de enfermedad y maternindad
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoEnfermedadCuota) != 0)
			{
				seError.asignaError(2008, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.comparaImportes( riTotal.impTresEnfermedadCuota, Double.parseDouble(rsDatos.regCincoEnfermedadCuota)/100)){
				seError.asignaError(2009, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}


			//Cuota Excedente de Enfermedad y maternidad
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoEnfermedadExcedente) != 0)
			{
				seError.asignaError(2010, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.dbl2int(riTotal.impTresEnfermedadExcedente) != SuaUtil.Str2int(rsDatos.regCincoEnfermedadExcedente)){
				seError.asignaError(2011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Prestaciones en dinero de enfermedad y maternidad
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoEnfermedadPrestaciones) != 0)
			{
				seError.asignaError(2012, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			if(SuaUtil.dbl2int(riTotal.impTresEnfermedadPrestaciones) != SuaUtil.Str2int(rsDatos.regCincoEnfermedadPrestaciones)){
				seError.asignaError(2013, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Gastos medicos pensionados
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoGastosMedicos) != 0)
			{
				seError.asignaError(2014, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			if(SuaUtil.dbl2int(riTotal.impTresGastosMedicos) != SuaUtil.Str2int(rsDatos.regCincoGastosMedicos)){
				seError.asignaError(2015, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Riesgos de trabajos
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoRiesgosDeTrabajo) != 0)
			{
				seError.asignaError(2016, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.dbl2int(riTotal.impTresRiesgosDeTrabajo) != SuaUtil.Str2int(rsDatos.regCincoRiesgosDeTrabajo)){
				seError.asignaError(2017, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Invalidez y vida
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoInvalidezVida) != 0)
			{
				seError.asignaError(2018, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.comparaImportesInt(SuaUtil.dbl2int(riTotal.impTresInvalidezVida), SuaUtil.Str2int(rsDatos.regCincoInvalidezVida))){
				seError.asignaError(2019, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Guarderia y prestaciones sociales
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoGuarderiaPrestaciones) != 0)
			{
				seError.asignaError(6010, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.comparaImportesInt(SuaUtil.dbl2int(riTotal.impTresGuarderiaPrestaciones), SuaUtil.Str2int(rsDatos.regCincoGuarderiaPrestaciones))){
				seError.asignaError(2021, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S15 Subtotal de cuatro seguros imss
			if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCincoSegurosImss) != 0)
			{
				seError.asignaError(2020, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			//Suma S8 a S14
			iSuma = SuaUtil.Str2int(rsDatos.regCincoEnfermedadCuota) +
			        SuaUtil.Str2int(rsDatos.regCincoEnfermedadExcedente) +
			        SuaUtil.Str2int(rsDatos.regCincoEnfermedadPrestaciones) +
			        SuaUtil.Str2int(rsDatos.regCincoGastosMedicos) +
			        SuaUtil.Str2int(rsDatos.regCincoRiesgosDeTrabajo) +
			        SuaUtil.Str2int(rsDatos.regCincoInvalidezVida) +
			        SuaUtil.Str2int(rsDatos.regCincoGuarderiaPrestaciones);

			if(SuaUtil.Str2int(rsDatos.regCincoSegurosImss) != iSuma){
				seError.asignaError(2023, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Actualizacion de los cuatro seguros imss
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoSegurosImssActualizacion) != 0)
			{
				seError.asignaError(2024, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Regargos de los cuatro seguros imss
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoSegurosImssRecargos) != 0)
			{
				seError.asignaError(2025, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Suma S16 + S17
			iSuma = SuaUtil.Str2int(rsDatos.regCincoSegurosImssActualizacion) +
				    SuaUtil.Str2int(rsDatos.regCincoSegurosImssRecargos);

			if (SuaUtil.dbl2int(riTotal.impTresSegurosImss) != iSuma){
					seError.asignaError(2036, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S18 Retiro
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoRetiro) != 0)
			{
				seError.asignaError(2026, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if (SuaUtil.Str2int(rsDatos.regCincoRetiro) != SuaUtil.dbl2int(riTotal.impTresRetiroCuota)){
				seError.asignaError(2027, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S19 Cesantia y vejez
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoCesantiaVejez) != 0)
			{
				seError.asignaError(6017, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			iSuma = SuaUtil.dbl2int(riTotal.impTresCesantiaVejezPatronal) +
			        SuaUtil.dbl2int(riTotal.impTresCesantiaVejezTrabajador);

			if (SuaUtil.comparaImportesInt(SuaUtil.Str2int(rsDatos.regCincoCesantiaVejez),iSuma)){
				seError.asignaError(2029, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S20 Subtotal Retiros censatia y vejez
			if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCincoRetiroCesantiaVejez) != 0)
			{
				seError.asignaError(2030, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//s18+s19
			iSuma = SuaUtil.Str2int(rsDatos.regCincoRetiro) + SuaUtil.Str2int(rsDatos.regCincoCesantiaVejez);

			if (SuaUtil.Str2int(rsDatos.regCincoRetiroCesantiaVejez) != iSuma){
				seError.asignaError(2212, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S21 Actualizacion de retiro cesantia y vejez
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoRetiroCesantiaActualizacion) != 0)
			{
				seError.asignaError(2032, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S22 Recargos de retiro cesantia y vejez
			if(SuaUtil.ValidaFormato("888888888", rsDatos.regCincoRetiroCesantiaVejezRecargos) != 0)
			{
				seError.asignaError(2033, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			//s21+s22
			iSuma = SuaUtil.Str2int(rsDatos.regCincoRetiroCesantiaActualizacion)+
					SuaUtil.Str2int(rsDatos.regCincoRetiroCesantiaVejezRecargos);

			iAcumulado = SuaUtil.dbl2int(riTotal.impTresRetiroRecargos) +
						 SuaUtil.dbl2int(riTotal.impTresCesantiaVejezRecargos);

			if(SuaUtil.comparaImportesInt(iSuma,iAcumulado)){
				seError.asignaError(2037, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S23 aportaciones voluntarias
			if(SuaUtil.ValidaFormato("88888888888", rsDatos.regCincoAportacionVoluntaria) != 0)
			{
				seError.asignaError(2034, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if (SuaUtil.Str2int(rsDatos.regCincoAportacionVoluntaria) != SuaUtil.dbl2int(riTotal.impTresAportacionVoluntaria))
			{
				seError.asignaError(2035, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Inicia bloque S24 Aportaci�n patronal
			//	En las CURP, en caso de tenerla S� deberas validarla de acuerdo a Lay out, si est� en blanco no hay problema y deber�s dejarla pasar.
			//
			//	T35, Aportaci�n Patronal
			//	T36, Amortizaci�n de Cr�dito
			//
			//	T35, Cuando haya CEROS en T36, deber� quedar sumado en S24.
			//	T35, Cuando sea > a CEROS T36, deber� quedar sumado en S25.
			//	T36, Deber� quedar sumado en S26.

			// S24 y S25 Aportacion Patronal Infonavit Cuenta individual
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoAportacionPatronal) != 0 )
			{
				seError.asignaError(2216, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S25 Aportacion Patronal Infonavit para Amortizacion de Creditos
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoAmortizacionCredito) != 0 )
			{
				seError.asignaError(1095, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}


			// Valida total T35, Cuando T36=0
			if (SuaUtil.comparaImportesInt(SuaUtil.Str2int(rsDatos.regCincoAportacionPatronal), rsDatos.S24) ) {
				seError.asignaError(2213, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if (SuaUtil.comparaImportesInt(SuaUtil.Str2int(rsDatos.regCincoAmortizacionCredito), rsDatos.S25) ) {
				seError.asignaError(2214, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			// Fin bloque S24 y S25 Aportaci�n patronal



		    //S26 Amortizacion de credito infonavit
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoInfonavitSubtotal) != 0 )
			{
				seError.asignaError(2038, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if (SuaUtil.dbl2int(riTotal.impTresAmortizacionCredito) != SuaUtil.Str2int(rsDatos.regCincoInfonavitSubtotal))
			{
				seError.asignaError(2039, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S27 Actualizacion de Aportacion Patronal y Amortizacion de Credito Infonavit
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoInfonavitActualizacion) != 0 )
			{
				seError.asignaError(2040, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S28 Recargos de Aportacion Patronal y Amortizacion deCredito infonavit
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoInfonavitRecargos) != 0 )
			{
				seError.asignaError(2041, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S29 Aportaciones Complementarias
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoAportacionComplementaria) != 0 )
			{
				seError.asignaError(1053, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.dbl2int(riTotal.impCuatroSalarioDiarioInt)  != SuaUtil.Str2int(rsDatos.regCincoAportacionComplementaria))
					{
				seError.asignaError(1097, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);

					}

			//S30 Multas Infonavit
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoInfonavitMultas) != 0 )
			{
				seError.asignaError(1054, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S31 Donativos Fundemex
			if( SuaUtil.ValidaFormato("888888888",rsDatos.regCincoFundemexDonativo) != 0 )
			{
				seError.asignaError(1055, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//S33 Filler
			if(!SuaUtil.validaEspacios(rsDatos.regCincoFiller, 9))
			{
				seError.asignaError(1098, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
		}
		catch(Exception e)
		{
			//System.out.println("Error en registro CINCO: "+e.getMessage());
		}
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Termina ProcesaRegistroCINCO &", EIGlobal.NivelLog.INFO);
	}

	public void ProcesaRegistroSEIS(SuaRegistro rsDatos)
	{
		//VSWF RRG I 15-Mayo-2008 AMX-08-00644-0A Se agregaron estas validaciones para IMSS y Infonavit
		SuaErrorDisco seError = new SuaErrorDisco();

		try
		{
			//Registro patronal IMSS
			/* Cambio GAO Stefanini IT Solutions 18/07/2008
			if(!tieneCaracteresValidos(rsDatos.regSeisImssPatronal))
			Fin GAO */
			if( !rsDatos.regDosImssPatronal.equals(rsDatos.regSeisImssPatronal) )
			{
				seError.asignaError(1065, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//RFC Patr�n
			/* Cambio GAO Stefanini IT Solutions 18/07/2008
			if(!tieneCaracteresValidos(rsDatos.regSeisRfcEmpresa) )
			Fin GAO */
			if(!rsDatos.regSeisRfcEmpresa.equals(rsDatos.regDosRfcEmpresa))
			{
				seError.asignaError(1066, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			/* Cambio GAO Stefanini IT Solutions 18/07/2008
			//Periodo de pago
			if(EvaluaEntero(rsDatos.regSeisPeriodoDePago)<0))
			Fin GAO */
			if(!rsDatos.regSeisPeriodoDePago.equals(rsDatos.regDosAnioDePago+rsDatos.regDosPeriodoDePago))
			{
				seError.asignaError(6001, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			/* Cambio GAO Stefanini IT Solutions 18/07/2008
			//Folio SUA
			if(EvaluaEntero(rsDatos.regSeisFolioSua)<0)
			Fin GAO */
			if(!rsDatos.regSeisFolioSua.equals(rsDatos.regDosFolioSua))
			{
				seError.asignaError(6002, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}


			if(SuaUtil.ValidaFormato("8888888888", rsDatos.regSeisSumTasaRec) != 0)
			{
				seError.asignaError(2215, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Fecha l�mite de pago
			/* Cambio GAO Stefanini IT Solutions 18/07/2008
			if(EvaluaEntero(rsDatos.regSeisFechaLimite)<0)
			Fin GAO */
			if(!SuaUtil.ValidaFechaAAAAMMDD(rsDatos.regSeisFechaLimite))
			{
				seError.asignaError(6003, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if (SuaUtil.ValidaFormato("888888888888", rsDatos.regSeisTotalImss4Seguros) != 0) {
				seError.asignaError(2022, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if (SuaUtil.ValidaFormato("888888888888", rsDatos.regSeisTotalImssConcentrar) != 0) {
				seError.asignaError(1039, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

        	// V13 Validaci�n de "Total a depositar en cuenta INFONAVIT aportaci�n personal no es num�rico
    		if (SuaUtil.ValidaFormato("888888888888", rsDatos.regSeisTotalAportacionPatronal) != 0) {
    			seError.asignaError(1040, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
    			throw new SUAException(); // suaErrores.addElement(seError);
    		}

        	// V14 Validaci�n de "Total a depositar en cuenta INFONAVIT aportaci�n personal no es num�rico
    		if (SuaUtil.ValidaFormato("888888888888", rsDatos.regSeisTotalAmortizacionCredito) != 0) {
    			seError.asignaError(2209, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
    			throw new SUAException(); // suaErrores.addElement(seError);
    		}




			/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
			// Cuota Fija
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisCuotaFija)))
			{
				seError.asignaError(2008, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Cuota Excedente Patronal
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisCuotaExcedePatronal)))
			{
				seError.asignaError(2010, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Cuota Excedente Obrero
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisCuotaExcedeObrero)))
			{
				seError.asignaError(2011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Prestaciones en Dinero Patronal
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisPrestaDineroPatronal)))
			{
				seError.asignaError(2012, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Prestaciones en Dinero Obrero
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisPrestaDineroObrero)))
			{
				seError.asignaError(2012, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Gastos Medicos Pensionados Patronal
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisGastosMedicosPensioPatronal)))
			{
				seError.asignaError(2014, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Gastos Medicos Pensionados Obrero
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisGastosMedicosPensioObrero)))
			{
				seError.asignaError(2014, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Riesgos de Trabajo
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisRiesgosTrabajo)))
			{
				seError.asignaError(2017, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Invalidez y Vida Patronal
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisInvalidezVidaPatronal)))
			{
				seError.asignaError(2018, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Invalidez y Vida Obrero
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisInvalidezVidaObrero)))
			{
				seError.asignaError(2018, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Guarderia y Prestaciones Sociales
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisGuarderiaPrestaSociales)))
			{
				seError.asignaError(6010, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Factor de Ausentismo
			if(SuaUtil.validaNumericoVariable(Double.toString(rsDatos.regSeisFactorReverAusentismo)))
			{
				seError.asignaError(1050, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Filler
			if(rsDatos.regSeisFiller.length() != 21)
			{
				seError.asignaError(1098, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			/* Fin Modificacion GAO */
		}
		catch(Exception e)
		{
			System.out.println("Error en registro SEIS: "+e.getMessage());
		}
		//VSWF RRG F 15-Mayo-2008
		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & ProcesaRegistroSEIS &", EIGlobal.NivelLog.INFO);
	}

	public boolean evaluaArchivo( SuaRegistro rsDatos, SuaImpuesto riTotal, long totalDeArchivo, long totalDeDiscos)
	throws SUAException
	{
		EIGlobal.mensajePorTrace( "***SuaAnalizador.class & ENTRO A EVALUA ARCHVO A VALIDAR ARCHIVO &", EIGlobal.NivelLog.INFO);

		SuaErrorDisco seError = new SuaErrorDisco();
		int           errorFecha = 0;
		int           pagoExt = 0;
		boolean       hayErrores;
		double        totalImss = 0;
		double        totalInfonavit = 0;
		double        totalImpuesto = 0;
		double        totalImporte = 0;
		double        totalValor = 0;
		int			  totalesAmort=0;
		//Inicio Mod. AMR 10/07/2007
		double		  totalRecargosAct = 0;
		//Fin mod. AMR 10/07/2007

        //Despues de esta fecha solo se aceptaran vensiones W3xx
        GregorianCalendar  fecha_limite_W3xx = new GregorianCalendar(2006,02,01);
        GregorianCalendar  fecha_sistema     = new GregorianCalendar();

		// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & evaluaArchivo &", EIGlobal.NivelLog.INFO);
		hayErrores = false;

		try
		{
			//Inicio Mod. AMR 10/07/2007
			totalRecargosAct = Double.parseDouble(rsDatos.regCincoSegurosImssActualizacion);
			totalRecargosAct += Double.parseDouble(rsDatos.regCincoSegurosImssRecargos);
			totalRecargosAct += Double.parseDouble(rsDatos.regCincoRetiroCesantiaActualizacion);
			totalRecargosAct += Double.parseDouble(rsDatos.regCincoRetiroCesantiaVejezRecargos);
			totalRecargosAct += Double.parseDouble(rsDatos.regCincoInfonavitActualizacion);
			totalRecargosAct += Double.parseDouble(rsDatos.regCincoInfonavitRecargos);
			//Fin mod. AMR 10/07/2007

			totalImss  = riTotal.impTresSegurosImss;
			totalImss += riTotal.impTresRetiroRecargos;
			totalImss += riTotal.impTresCesantiaVejezRecargos;
			totalImss += riTotal.impTresAmortizacionCredito;
			//totalImss += riTotal.impTresConceptoNoDefinido;

			totalImpuesto  = riTotal.impTresEnfermedadCuota;
			totalImpuesto += riTotal.impTresEnfermedadExcedente;
			totalImpuesto += riTotal.impTresEnfermedadPrestaciones;
			totalImpuesto += riTotal.impTresGastosMedicos;
			totalImpuesto += riTotal.impTresRiesgosDeTrabajo;
			totalImpuesto += riTotal.impTresInvalidezVida;
			totalImpuesto += riTotal.impTresGuarderiaPrestaciones;

			totalInfonavit  = riTotal.impTresRetiroCuota;
			totalInfonavit += riTotal.impTresCesantiaVejezPatronal;
			totalInfonavit += riTotal.impTresCesantiaVejezTrabajador;
			totalInfonavit += riTotal.impTresAportacionPatronal;
			totalInfonavit += riTotal.impTresAmortizacionCredito;
			//totalInfonavit += riTotal.impTresConceptoNoDefinido;

			if(SuaUtil.comparaImportes( riTotal.impTresEnfermedadCuota, riTotal.impCincoEnfermedadCuota ))
			{
				seError.asignaError(2009, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.comparaImportes(riTotal.impTresEnfermedadExcedente, riTotal.impCincoEnfermedadExcedente )  ) {
				seError.asignaError(2011, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.comparaImportes(riTotal.impTresEnfermedadPrestaciones, riTotal.impCincoEnfermedadPrestaciones )  ){
				seError.asignaError(2013, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.comparaImportes(riTotal.impTresGastosMedicos, riTotal.impCincoGastosMedicos )  ) {
				seError.asignaError(2015, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(SuaUtil.comparaImportes(riTotal.impTresRiesgosDeTrabajo, riTotal.impCincoRiesgosDeTrabajo )  ) {
				seError.asignaError(2017, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}
			//se movio de la linea 2509
			//regCincoAportacionPatronal es el campo 24 R5
            /*if ( SuaUtil.dbl2int(riTotal.impCincoAportacionPatronal) != SuaUtil.dbl2int(  riTotal.sumaD35cuando36esCeroR3 ))
			{
				seError.asignaError(1056, rsDatos.numeroDeRegistro , rsDatos.NombreArchivo, false);
				throw new SUAException(); // suaErrores.addElement(seError);
			}*/
			if(SuaUtil.comparaImportes(riTotal.impTresInvalidezVida, riTotal.impCincoInvalidezVida )  ) {
				seError.asignaError(2018, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( SuaUtil.comparaImportes(riTotal.impTresGuarderiaPrestaciones, riTotal.impCincoGuarderiaPrestaciones )  ) {
				seError.asignaError(2021, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( SuaUtil.comparaImportes( totalImpuesto, riTotal.impCincoSegurosImss )  ) {
				seError.asignaError(2023, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			totalImporte  = riTotal.impCincoSegurosImssActualizacion;
			totalImporte += riTotal.impCincoSegurosImssRecargos;

			if( SuaUtil.comparaImportes( riTotal.impTresSegurosImss, totalImporte )  )
			{
				seError.asignaError(2217, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( SuaUtil.comparaImportes(  riTotal.impTresRetiroCuota, riTotal.impCincoRetiro )  )
			{
				seError.asignaError(1029, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			totalImporte  = riTotal.impTresCesantiaVejezPatronal;
			totalImporte += riTotal.impTresCesantiaVejezTrabajador;

			if( SuaUtil.comparaImportes(  totalImporte, riTotal.impCincoCesantiaVejez )  )
			{
				seError.asignaError(2029, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			totalImporte = riTotal.impCincoRetiro;
			totalImporte += riTotal.impCincoCesantiaVejez;

			if( SuaUtil.comparaImportes(  totalImporte, riTotal.impCincoRetiroCesantiaVejez )  )
			{
				seError.asignaError(2031, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			totalImporte  = riTotal.impTresRetiroRecargos;
			totalImporte += riTotal.impTresCesantiaVejezRecargos;
			totalValor    = riTotal.impCincoRetiroCesantiaActualizacion;
			totalValor   += riTotal.impCincoRetiroCesantiaVejezRecargos;

			if( SuaUtil.comparaImportes( totalImporte, totalValor ) )
			{
				seError.asignaError(2033, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( SuaUtil.comparaImportes(riTotal.impTresAportacionVoluntaria, riTotal.impCincoAportacionVoluntaria))
			{
				seError.asignaError(2035, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			totalImporte  = riTotal.impCincoAportacionPatronal;
			totalImporte += riTotal.impCincoAmortizacionCredito;

			if( SuaUtil.comparaImportes(riTotal.impTresAportacionPatronal, totalImporte) )
			{
				seError.asignaError(1033, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( SuaUtil.comparaImportes( riTotal.impTresAmortizacionCredito, riTotal.impCincoInfonavitSubtotal))
			{
				seError.asignaError(1034, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			totalImporte  = riTotal.impCincoSegurosImss;
			totalImporte += riTotal.impCincoSegurosImssActualizacion;
			totalImporte += riTotal.impCincoSegurosImssRecargos;

			if ( SuaUtil.comparaImportes(totalImporte, riTotal.impSeisTotalImss4Seguros))
			{
				seError.asignaError(2208, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			totalImporte  = riTotal.impCincoRetiroCesantiaVejez;
			totalImporte += riTotal.impCincoRetiroCesantiaActualizacion;
			totalImporte += riTotal.impCincoRetiroCesantiaVejezRecargos;
			totalImporte += riTotal.impCincoAportacionVoluntaria;
			totalImporte += riTotal.impCincoAportacionComplementaria;   // se sumas las aport. compl.  SUA2005 MX-2005-1100

			if( SuaUtil.comparaImportes( totalImporte, riTotal.impSeisTotalImssConcentrar ))
			{
				seError.asignaError(2207, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// comparar las aporta compl. con los del reg 4:Salario diario integrado :: SUA2005 MX-2005-1100
			if( SuaUtil.comparaImportes( riTotal.impCuatroSalarioDiarioInt, riTotal.impCincoAportacionComplementaria ))
			{
				seError.asignaError(1044, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Inicia mod. AMR 10/07/2007
			//errorFecha = EvaluaFecha( rsDatos.regSeisFechaLimite, rsDatos.regDosPeriodoDePago.trim() + rsDatos.regDosAnioDePago.trim(), totalImss);

			EIGlobal.mensajePorTrace( "***CSA SuaAnalizador.class Fecha Limite ----->"+ rsDatos.regSeisFechaLimite, EIGlobal.NivelLog.INFO);

			errorFecha = EvaluaFecha( rsDatos.regSeisFechaLimite, rsDatos.regDosPeriodoDePago.trim() + rsDatos.regDosAnioDePago.trim(), totalRecargosAct);



			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & errorFecha ----->"+errorFecha, EIGlobal.NivelLog.INFO);
			//Fin mod. AMR 10/07/2007

			if(errorFecha==1)
			{
				//VSWF RRG I 21-Mayo-2008 AMX-08-00644-0A Se modifico el codigo de error para SUS IMSS y Infonavit
				seError.asignaError(1069, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				//VSWF RRG F 21-Mayo-2008 AMX-08-00644-0A
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(errorFecha==2)
			{
				seError.asignaError(1003, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if( SuaUtil.comparaImportes( riTotal.impCincoAportacionPatronal, riTotal.impSeisTotalAportacionPatronal)) {	// 412
				seError.asignaError(2201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}


//			totalesAmort  = SuaUtil.Str2int(rsDatos.regCincoAmortizacionCredito);
//			totalesAmort += SuaUtil.Str2int(rsDatos.regCincoInfonavitSubtotal);
//			totalesAmort += SuaUtil.Str2int(rsDatos.regCincoInfonavitActualizacion);
//			totalesAmort += SuaUtil.Str2int(rsDatos.regCincoInfonavitRecargos);
//			totalesAmort += SuaUtil.Str2int(rsDatos.regCincoInfonavitMultas);
//			totalesAmort += SuaUtil.Str2int(rsDatos.regCincoFundemexDonativo);
//			if( SuaUtil.comparaImportesInt(totalesAmort, SuaUtil.Str2int(rsDatos.regSeisTotalAmortizacionCredito)) ) {
//				seError.asignaError(2210, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
//				throw new SUAException(); // suaErrores.addElement(seError);
//			}



//			//2201 - 412
//
//			if( SuaUtil.comparaImportes( riTotal.impCincoAportacionPatronal, riTotal.impSeisTotalAportacionPatronal))
//			{
//				seError.asignaError(2201, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
//				throw new SUAException(); // suaErrores.addElement(seError);
//			}

			//////////////////////////////////////////////////////////
//			if( SuaUtil.comparaImportes( riTotal.impCincoAportacionPatronal, riTotal.impSeisTotalAportacionPatronal))
//			{
//				seError.asignaError(1040, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
//				throw new SUAException(); // suaErrores.addElement(seError);
//			}

			totalImporte  = riTotal.impCincoAmortizacionCredito;
			totalImporte += riTotal.impCincoInfonavitSubtotal;
			totalImporte += riTotal.impCincoInfonavitActualizacion;
			totalImporte += riTotal.impCincoInfonavitRecargos;
			totalImporte += riTotal.impCincoInfonavitMultas;      // E Multas  SUA2005 MX-2005-1100
			totalImporte += riTotal.impCincoFundemexDonativo;      // E Donativos  SUA2005 MX-2005-1100

			if( SuaUtil.comparaImportes( totalImporte, riTotal.impSeisTotalAmortizacionCredito))
			{
				seError.asignaError(1041, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(((SuaUtil.Str2int(rsDatos.regDosPeriodoDePago)%2) == 1)&&(totalInfonavit > 0))
			{
				seError.asignaError(1012, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}




			//�rea geogr�fica de la Comisi�n Nacional de Salarios M�nimos
			if(!tieneCaracteresValidos(rsDatos.regDosAreaGeoSalMini))
			{

				seError.asignaError(1077, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Convenio de reembolso de subsidios
			if(!tieneCaracteresValidos(rsDatos.regDosConvReembSubs)){
				seError.asignaError(1077, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Total de d�as cotizados en el per�odo sin descontar incidencias

			//Mensual
			if(Integer.parseInt(rsDatos.regDosPeriodoDePago)%2>0)
			{
				if(rsDatos.regTresSumaMensual!=0)
				{
					if(EvaluaEntero(rsDatos.regDosDiasCotizadosPeriodo)!=rsDatos.regTresSumaMensual)
					{
						seError.asignaError(1061, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
						throw new SUAException(); // suaErrores.addElement(seError);
					}
				}
				else
				{
					if(EvaluaEntero(rsDatos.regDosDiasCotizadosPeriodo)!=rsDatos.regTresSumaBimestral)
					{
						seError.asignaError(1061, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
						throw new SUAException(); // suaErrores.addElement(seError);
					}
				}
			}
			//Bimestral
			if(Integer.parseInt(rsDatos.regDosPeriodoDePago)%2==0)
			{
				if(rsDatos.regTresSumaBimestral!=0)
				{
					if(EvaluaEntero(rsDatos.regDosDiasCotizadosPeriodo)!=rsDatos.regTresSumaBimestral)
					{
						seError.asignaError(1061, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
						throw new SUAException(); // suaErrores.addElement(seError);
					}
				}
				else
				{
					if(EvaluaEntero(rsDatos.regDosDiasCotizadosPeriodo)!=rsDatos.regTresSumaMensual)
					{
						seError.asignaError(1061, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, true, suaErrores);
						throw new SUAException(); // suaErrores.addElement(seError);
					}
				}
			}



			//VSWF RRG F 21-Mayo-2008 AMX-08-00644-0A
			if( rsDatos.numeroDeTrabajador != SuaUtil.Str2int(rsDatos.regDosTrabajadores) )
			{
				//VSWF RRG I 21-Mayo-2008 AMX-08-00644-0A Se agregaron estas validaciones para SUS IMSS y Infonavit
				seError.asignaError(103, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				//VSWF RRG I 21-Mayo-2008 AMX-08-00644-0A
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			//Inicia mod. AMR 10/07/2007
			//pagoExt = EvaluaFechaExt( rsDatos.regSeisFechaLimite, rsDatos.regDosPeriodoDePago.trim() + rsDatos.regDosAnioDePago.trim(), totalImss);
			pagoExt = EvaluaFechaExt( rsDatos.regSeisFechaLimite, rsDatos.regDosPeriodoDePago.trim() + rsDatos.regDosAnioDePago.trim(), totalRecargosAct);

			//Fin mod. AMR 10/07/2007
			if( comparaCadenas(rsDatos.regSeisVersionSua, "E100") ||
				comparaCadenas(rsDatos.regSeisVersionSua, "D100") ||
				comparaCadenas(rsDatos.regSeisVersionSua, "W100") )
			{
				if( ! (((pagoExt==0)&&(rsDatos.regSeisPeriodoDePago.compareTo("199907")<0)) ||
						((pagoExt >0)&&(rsDatos.regSeisFechaLimite.compareTo("19990801")<0))))
				{
					seError.asignaError(1005, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}

			if( comparaCadenas(rsDatos.regSeisVersionSua, "E200") ||
				comparaCadenas(rsDatos.regSeisVersionSua, "D200") ||
				comparaCadenas(rsDatos.regSeisVersionSua, "W200") )
			{
				if( ! (((pagoExt==0)&&(rsDatos.regSeisPeriodoDePago.compareTo("199906")>0)) ||
						((pagoExt >0)&&(rsDatos.regSeisFechaLimite.compareTo("19990731")>0))))
				{
					seError.asignaError(1005, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
			}
			if (rsDatos.regSeisVersionSua.equals("W400")){
				seError.asignaError(2206, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
			}

	//#########################################################################
	//        Nuevo Boque:
	//        validacion fecha limite, nueva version - W3xx -
	//        proyecto : SUA 2005
	//#########################################################################

			if (  (clave_con.indexOf(rsDatos.regSeisVersionSua.substring(0,1))<0) && (clave_con.indexOf(rsDatos.regSeisVersionSua.substring(1,2))<0)   )
			{
				if(fecha_sistema.before(fecha_limite_W3xx))
				{
					seError.asignaError(1043, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
				if(fecha_sistema.after(fecha_limite_W3xx))
				{
					seError.asignaError(1043, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
					throw new SUAException(); // suaErrores.addElement(seError);
				}
				else
				{
					// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "<<SuaDisco--Version sua aun valida>> ", EIGlobal.NivelLog.INFO);
				}
			}
			else
			{
				rsDatos.ValidaFundemexMultas=true;
			}

			/*if (SuaUtil.dbl2int( riTotal.impCincoAmortizacionCredito) != SuaUtil.dbl2int(  riTotal.sumaD35cuando36noCeroR3 ))
			{
				seError.asignaError(1057, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}*/

	//#########################################################################
	//               fin Nuevo Blque - valida fecha y version -
	//##########################################################################

			// Valida que el formato de tama�o de archivo sea num�rico
			if (SuaUtil.ValidaFormato("8888888888", rsDatos.regSeisTamanioArchivo) != 0) {
				seError.asignaError(2205, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			} else if(SuaUtil.Str2long(rsDatos.regSeisTamanioArchivo)!=totalDeArchivo)//Valida que el tama�o corresponda al calculado
			{
				seError.asignaError(2218, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			// Valida que el tama�o del disco sea num�rico
			if(SuaUtil.ValidaFormato("88",rsDatos.regSeisNumeroDeDiscos) != 0)
			{
				seError.asignaError(2200, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			} else if(SuaUtil.Str2long(rsDatos.regSeisNumeroDeDiscos)!= totalDeDiscos){

				seError.asignaError(1009, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
				throw new SUAException(); // suaErrores.addElement(seError);
			}

			if(suaErrores.size()>0)
			{
				hayErrores = true;
				suaProceder = false;
			}

			// -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class & Termina evaluaArchivo &", EIGlobal.NivelLog.INFO);
		}
		catch(Exception e)
		{
			//System.out.println("Error en evaluaArchivo "+e.getMessage());
			throw new SUAException();
		}
			return hayErrores;
	}

	public boolean evaluaRegistro(SuaRegistro rsDatos, SuaImpuesto riTotal)
	throws SUAException
    {
          SuaErrorDisco errorRegistro;
          int           tipoRegistro;
          boolean       hayErrores;

          // -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class # evaluaRegistro #", 5);

          errorRegistro = new SuaErrorDisco();
          tipoRegistro  = rsDatos.tipoRegistro;
          hayErrores    = false;
          try
          {
                // -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class # tipoRegistro #" + tipoRegistro, EIGlobal.NivelLog.INFO);
                if (tipoRegistro == 2 && tipoRegistroActivo == 1)
                {
                      ProcesaRegistroDOS(rsDatos);
                }
                else if (tipoRegistroActivo == 1 && tipoRegistro != 2)
                {
                      errorRegistro.asignaError(102, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
                      throw new SUAException(); // suaErrores.addElement(errorRegistro);
                }
                else if (tipoRegistro == 3 && (tipoRegistroActivo == 2 || tipoRegistroActivo == 3))
                {
                      ProcesaRegistroTRES(rsDatos);
                }
                else if (tipoRegistroActivo == 2 && tipoRegistro != 3)
                {
                      errorRegistro.asignaError(103, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
                      throw new SUAException(); // suaErrores.addElement(errorRegistro);
                }
                else if (tipoRegistro == 4 && (tipoRegistroActivo == 3 || tipoRegistroActivo == 4))
                {
                      ProcesaRegistroCUATRO(rsDatos);
                }
                else if (tipoRegistroActivo == 3 && tipoRegistro != 4)
                {
                      errorRegistro.asignaError(104, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
                      throw new SUAException(); // suaErrores.addElement(errorRegistro);
                }
                else if (tipoRegistro == 5 && tipoRegistroActivo == 4)
                {
                      ProcesaRegistroCINCO(rsDatos, riTotal);
                }
                else if (tipoRegistroActivo == 4 && tipoRegistro != 5)
                {
                      errorRegistro.asignaError(105, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
                      throw new SUAException(); // suaErrores.addElement(errorRegistro);
                }
                else if (tipoRegistro == 6 && tipoRegistroActivo == 5)
                {
                      ProcesaRegistroSEIS(rsDatos);
                }
                else if (tipoRegistroActivo == 5 && tipoRegistro != 6)
                {
                      errorRegistro.asignaError(106, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
                      throw new SUAException(); // suaErrores.addElement(errorRegistro);
                }

          }catch(Exception E)
          {
                // -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class Excepcion %evaluaRegistro() >> "+ E.getMessage() + " <<", EIGlobal.NivelLog.INFO);
                errorRegistro.asignaError(202, rsDatos.numeroDeRegistro, rsDatos.NombreArchivo, false, suaErrores);
                //System.out.println("Error Exception validacion "+E.getMessage());
                throw new SUAException(); // suaErrores.addElement(errorRegistro);
          }

          tipoRegistroActivo = tipoRegistro;
          if(suaErrores.size() > 0)
          {
                hayErrores = true;
                suaProceder = false;
          }

          // -- Eliminada por desempe�o en Producci�n -- EIGlobal.mensajePorTrace( "***SuaAnalizador.class # Termina evaluaRegistro #", 5);
      	  return hayErrores;
    }
	public static void main(String[] args)
	{
//		String temp = new String("/home/areyes/Escritorio/SUA/ArchivosPrueba/Multiples/W0806021.SU0");
//		String temp1 = new String("/home/areyes/Escritorio/SUA/ArchivosPrueba/Multiples/W0806021.SU1");
//		String temp2 = new String("/home/areyes/Escritorio/SUA/ArchivosPrueba/Multiples/W0806021.SU2");
//		String arch[] = {temp,temp1,temp2};
	/*	String arch[] = { args[0] };

		SuaVerificaDisco ver = new SuaVerificaDisco(arch,"/home/areyes/Escritorio/SUA/Salida/W0806031.dat", new Vector());

		long tiempo = System.currentTimeMillis();
		ver.Procesar();
		tiempo = System.currentTimeMillis()-tiempo;
		System.out.println("Tiempo de registro" + tiempo);*/
		//System.out.println("----" + SuaUtil.validaRFCTrabajador(" ura720726493"));

//		SuaErrorDisco temp = (SuaErrorDisco) ver.scsAnalist.suaErrores.get(0);
//		String lista = SuaErrorDisco.msgError(temp.error);
//		System.out.println("LISTA: "+lista);
	}

}