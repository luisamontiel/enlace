package mx.altec.enlace.bo;

public class SUAException extends Exception {

    /**
	 *
	 */
	private static final long serialVersionUID = -8580289426375652570L;

	public SUAException() {

    }

    public SUAException(String msg) {
        super(msg);
    }

}
