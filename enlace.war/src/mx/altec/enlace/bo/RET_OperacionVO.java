package mx.altec.enlace.bo;

import java.util.Vector;

import mx.altec.enlace.utilerias.Util;

/**
 *  Clase Bean para la Operaciones de RR
 *
 * @author Daniel Hern�ndez P�rez
 * @version 1.0 Mar 29, 2012
 */

public class RET_OperacionVO implements java.io.Serializable{

	/**
	 * Serial Id Version
	 */
	private static final long serialVersionUID = 2745539923929363358L;

	/**
	 * Identificador del registro consultado
	 */
	private String idRecord = "";

	/**
	 * Folio de operacion de cambios 390
	 */
	private String folioOper = "";

	/**
	 * Folio generado por Enlace para la operacion
	 */
	private String folioEnla  = "";

	/**
	 * Folio/Ticket enviado por la herramiento Reuters RET
	 */
	private String folioRet = "";

	/**
	 * Usuario/Cliente que realiza el pactado de la operacion
	 */
	private String usrRegistro = "";

	/**
	 * Clave Especial de Pactado de la operaci�n
	 */
	private String cveTcOper = "";

	/**
	 * Contrato del cliente que pacta una operaci�n en Reuters
	 */
	private String contrato = "";

	/**
	 * Usuario/Cliente que realiza la liberacion de la operacion
	 */
	private String usrAutoriza = "";

	/**
	 * Cuenta cargo a enviara a cambios 390
	 */
	private String ctaCargo = "";

	/**
	 * Descripci�n Cuenta cargo a enviara a cambios 390
	 */
	private String desCtaCargo = "";

	/**
	 * Vector de cuentas de abono.
	 */
	private Vector <RET_CuentaAbonoVO> detalleAbono = null;

	/**
	 * Importe total de la operacion
	 */
	private String importeTotOper = "";

	/**
	 * Importe total del abono de la operacion
	 */
	private String importeTotAbono = "";

	/**
	 * Divisa Operante
	 */
	private String divisaOperante = "";

	/**
	 * Contra Divisa
	 */
	private String contraDivisa = "";

	/**
	 * Operacion de Compra o Venta
	 */
	private String tipoOperacion = "";

	/**
	 * Tipo de Cambio Base del Dealing
	 */
	private String tcBase = "";

	/**
	 * Tipo de cambio pactado por el cliente en RET
	 */
	private String tcPactado = "";

	/**
	 * Tipo de Cambio Base del Dealing divisa 1
	 */
	private String tcBaseDiv1 = "";

	/**
	 * Tipo de cambio pactado por el cliente en RET Ventanilla 1
	 */
	private String tcVentanillaDiv1 = "";

	/**
	 * Tipo de Cambio Base del Dealing Divisa 2
	 */
	private String tcBaseDiv2 = "";

	/**
	 * Tipo de cambio pactado por el cliente en RET Ventanilla Divisa 2
	 */
	private String tcVentanillaDiv2 = "";

	/**
	 * Estatus de la operacion desde enlace
	 */
	private String estatusOperacion = "";

    /**
	 * Descripci�n del Estatus de la operaci�n
	 */
    private String descEstatusOperacion = "";

	/**
	 * Fecha y hora que se realizo el pactado
	 */
	private String fchHoraPactado = "";

	/**
	 * Fecha y hora que cambios 390 responde la operacion
	 */
	private String fchHoraComplemento = "";

	/**
	 * Fcha y hora que es liebrada la operacion a cambios 390
	 */
	private String fchHoraLiberado = "";

	/**
	 * Concepto de la operacion Reuters
	 */
	private String concepto = "";

	/**
	 * Variable para saber si en la colecci�n de abonos hay cuentas Mismo Banco
	 */
	private boolean existenCtaMB = false;

	/**
	 * Variable para saber si en la colecci�n de abonos hay cuentas Internacionales
	 */
	private boolean existenCtaIB = false;

	/**
     * Asignar un valor a folioOper.
     *
     * @param nombre Valor a asignar.
     */
	public void setFolioOper(String folio){
		this.folioOper = folio;
	}

	/**
	 * Obtener el valor de folioOper.
     *
     * @return folioOper valor asignado.
	 */
	public String getFolioOper(){
		return this.folioOper;
	}

	/**
     * Asignar un valor a folioEnla.
     *
     * @param nombre Valor a asignar.
     */
	public void setFolioEnla(String folioEnla){
		this.folioEnla = folioEnla;
	}

	/**
	 * Obtener el valor de folioEnla.
     *
     * @return folioEnla valor asignado.
	 */
	public String getFolioEnla(){
		return this.folioEnla;
	}

	/**
     * Asignar un valor a folioRet.
     *
     * @param nombre Valor a asignar.
     */
	public void setFolioRet(String folio){
		this.folioRet = folio;
	}

	/**
	 * Obtener el valor de folioRet.
     *
     * @return folioRet valor asignado.
	 */
	public String getFolioRet(){
		return this.folioRet;
	}

	/**
     * Asignar un valor a usrRegistro.
     *
     * @param usrRegistro Valor a asignar.
     */
	public void setUsrRegistro(String usr){
		this.usrRegistro = usr;
	}

	/**
	 * Obtener el valor de usrRegistro.
     *
     * @return usrRegistro valor asignado.
	 */
	public String getUsrRegistro(){
		return this.usrRegistro;
	}

	/**
     * Asignar un valor a usrAutoriza.
     *
     * @param nombre Valor a asignar.
     */
	public void setUsrAutoriza(String usr){
		this.usrAutoriza = usr;
	}

	/**
	 * Obtener el valor de usrAutoriza.
     *
     * @return usrAutoriza valor asignado.
	 */
	public String getUsrAutoriza(){
		return this.usrAutoriza;
	}

	/**
     * Asignar un valor a ctaCargo.
     *
     * @param nombre Valor a asignar.
     */
	public void setCtaCargo(String cta){
		this.ctaCargo = cta;
	}

	/**
	 * Obtener el valor de ctaCargo.
     *
     * @return ctaCargo valor asignado.
	 */
	public String getCtaCargo(){
		return this.ctaCargo;
	}

	/**
	 * Obtener el valor de detalleAbono.
     *
     * @return detalleAbono valor asignado.
	 */
	public Vector<RET_CuentaAbonoVO> getDetalleAbono() {
		return detalleAbono;
	}

	/**
     * Asignar un valor a detalleAbono.
     *
     * @param detalleAbono Valor a asignar.
     */
	public void setDetalleAbono(Vector<RET_CuentaAbonoVO> detalleAbono) {
		this.detalleAbono = detalleAbono;
	}

	/**
     * Asignar un valor a importeTotOper.
     *
     * @param nombre Valor a asignar.
     */
	public void setImporteTotOper(String imp){
		this.importeTotOper = imp;
	}

	/**
	 * Obtener el valor de importeTotOper.
     *
     * @return importeTotOper valor asignado.
	 */
	public String getImporteTotOper(){
		return this.importeTotOper;
	}

	/**
     * Asignar un valor a divisaOperante.
     *
     * @param nombre Valor a asignar.
     */
	public void setDivisaOperante(String divOp){
		this.divisaOperante = divOp;
	}

	/**
	 * Obtener el valor de divisaOperante.
     *
     * @return divisaOperante valor asignado.
	 */
	public String getDivisaOperante(){
		return this.divisaOperante;
	}

	/**
     * Asignar un valor a contraDivisa.
     *
     * @param nombre Valor a asignar.
     */
	public void setContraDivisa(String conDiv){
		this.contraDivisa = conDiv;
	}

	/**
	 * Obtener el valor de contraDivisa.
     *
     * @return contraDivisa valor asignado.
	 */
	public String getContraDivisa(){
		return this.contraDivisa;
	}

	/**
     * Asignar un valor a tipoOperacion.
     *
     * @param nombre Valor a asignar.
     */
	public void setTipoOperacion(String tipOp){
		this.tipoOperacion = tipOp;
	}

	/**
	 * Obtener el valor de tipoOperacion.
     *
     * @return tipoOperacion valor asignado.
	 */
	public String getTipoOperacion(){
		return this.tipoOperacion;
	}

	/**
     * Asignar un valor a tcBase.
     *
     * @param nombre Valor a asignar.
     */
	public void setTcBase(String tcB){
		this.tcBase = tcB;
	}

	/**
	 * Obtener el valor de tcBase.
     *
     * @return tcBase valor asignado.
	 */
	public String getTcBase(){
		return this.tcBase;
	}

	/**
     * Asignar un valor a tcPactado.
     *
     * @param nombre Valor a asignar.
     */
	public void setTcPactado(String tcP){
		this.tcPactado = tcP;
	}

	/**
	 * Obtener el valor de tcPactado.
     *
     * @return tcPactado valor asignado.
	 */
	public String getTcPactado(){
		return this.tcPactado;
	}

	/**
     * Asignar un valor a estatusOperacion.
     *
     * @param nombre Valor a asignar.
     */
	public void setEstatusOperacion(String estatus){
		this.estatusOperacion = estatus;
	}

	/**
	 * Obtener el valor de estatusOperacion.
     *
     * @return estatusOperacion valor asignado.
	 */
	public String getEstatusOperacion(){
		return this.estatusOperacion;
	}

	/**
     * Asignar un valor a fchHoraPactado.
     *
     * @param nombre Valor a asignar.
     */
	public void setFchHoraPactado(String fchPac){
		this.fchHoraPactado = fchPac;
	}

	/**
	 * Obtener el valor de fchHoraPactado.
     *
     * @return fchHoraPactado valor asignado.
	 */
	public String getFchHoraPactado(){
		return this.fchHoraPactado;
	}

	/**
     * Asignar un valor a fchHoraLiberado.
     *
     * @param nombre Valor a asignar.
     */
	public void setFchHoraLiberado(String fchLib){
		this.fchHoraLiberado = fchLib;
	}

	/**
	 * Obtener el valor de fchHoraLiberado.
     *
     * @return fchHoraLiberado valor asignado.
	 */
	public String getFchHoraLiberado(){
		return this.fchHoraLiberado;
	}

	/**
     * Asignar un valor a fchHoraComplemento.
     *
     * @param nombre Valor a asignar.
     */
	public void setFchHoraComplemento(String fchAut){
		this.fchHoraComplemento = fchAut;
	}

	/**
	 * Obtener el valor de fchHoraComplemento.
     *
     * @return fchHoraComplemento valor asignado.
	 */
	public String getFchHoraComplemento(){
		return this.fchHoraComplemento;
	}

	/**
     * Asignar un valor a cveTcOper.
     *
     * @param cveTcOper Valor a asignar.
     */
	public void setCveTcOper(String cveTcOper){
		this.cveTcOper = cveTcOper;
	}

	/**
	 * Obtener el valor de cveTcOper.
     *
     * @return cveTcOper valor asignado.
	 */
	public String getCveTcOper(){
		return this.cveTcOper;
	}

	/**
     * Asignar un valor a contrato.
     *
     * @param contrato Valor a asignar.
     */
	public void setContrato(String contrato){
		this.contrato = contrato;
	}

	/**
	 * Obtener el valor de contrato.
     *
     * @return contrato valor asignado.
	 */
	public String getContrato(){
		return this.contrato;
	}

	/**
     * Asignar un valor a tcBaseDiv1.
     *
     * @param tcBaseDiv1 Valor a asignar.
     */
	public void setTcBaseDiv1(String tcBaseDiv1){
		this.tcBaseDiv1 = tcBaseDiv1;
	}

	/**
	 * Obtener el valor de tcBaseDiv1.
     *
     * @return tcBaseDiv1 valor asignado.
	 */
	public String getTcBaseDiv1(){
		return this.tcBaseDiv1;
	}

	/**
     * Asignar un valor a tcVentanillaDiv1.
     *
     * @param tcVentanillaDiv1 Valor a asignar.
     */
	public void setTcVentanillaDiv1(String tcVentanillaDiv1){
		this.tcVentanillaDiv1 = tcVentanillaDiv1;
	}

	/**
	 * Obtener el valor de tcVentanillaDiv1.
     *
     * @return tcVentanillaDiv1 valor asignado.
	 */
	public String getTcVentanillaDiv1(){
		return this.tcVentanillaDiv1;
	}

	/**
     * Asignar un valor a tcBaseDiv2.
     *
     * @param tcBaseDiv2 Valor a asignar.
     */
	public void setTcBaseDiv2(String tcBaseDiv2){
		this.tcBaseDiv2 = tcBaseDiv2;
	}

	/**
	 * Obtener el valor de tcBaseDiv2.
     *
     * @return tcBaseDiv2 valor asignado.
	 */
	public String getTcBaseDiv2(){
		return this.tcBaseDiv2;
	}

	/**
     * Asignar un valor a tcVentanillaDiv2.
     *
     * @param tcVentanillaDiv2 Valor a asignar.
     */
	public void setTcVentanillaDiv2(String tcVentanillaDiv2){
		this.tcVentanillaDiv2 = tcVentanillaDiv2;
	}

	/**
	 * Obtener el valor de tcVentanillaDiv2.
     *
     * @return tcVentanillaDiv2 valor asignado.
	 */
	public String getTcVentanillaDiv2(){
		return this.tcVentanillaDiv2;
	}

	/**
     * Asignar un valor a idRecord.
     *
     * @param idRecord Valor a asignar.
     */
	public void setIdRecord(String idRecord){
		this.idRecord = idRecord;
	}

	/**
	 * Obtener el valor de idRecord.
     *
     * @return idRecord valor asignado.
	 */
	public String getIdRecord(){
		return this.idRecord;
	}

	/**
     * Asignar un valor a desCtaCargo.
     *
     * @param desCtaCargo Valor a asignar.
     */
	public void setDesCtaCargo(String desCtaCargo){
		this.desCtaCargo = desCtaCargo;
	}

	/**
	 * Obtener el valor de desCtaCargo.
     *
     * @return desCtaCargo valor asignado.
	 */
	public String getDesCtaCargo(){
		return this.desCtaCargo;
	}

	/**
     * Asignar un valor a concepto.
     *
     * @param concepto Valor a asignar.
     */
	public void setConcepto(String concepto){
		this.concepto = concepto;
	}

	/**
	 * Obtener el valor de concepto.
     *
     * @return concepto valor asignado.
	 */
	public String getConcepto(){
		return this.concepto;
	}

	/**
     * Asignar un valor a importeTotAbono.
     *
     * @param importeTotAbono Valor a asignar.
     */
	public void setImporteTotAbono(String importeTotAbono){
		this.importeTotAbono = importeTotAbono;
	}

	/**
	 * Obtener el valor de importeTotAbono.
     *
     * @return importeTotAbono valor asignado.
	 */
	public String getImporteTotAbono(){
		return this.importeTotAbono;
	}

	/**
     * Asignar un valor a existenCtaMB.
     *
     * @param existenCtaMB Valor a asignar.
     */
	public void setExistenCtaMB(boolean existenCtaMB){
		this.existenCtaMB = existenCtaMB;
	}

	/**
	 * Obtener el valor de existenCtaMB.
     *
     * @return existenCtaMB valor asignado.
	 */
	public boolean getExistenCtaMB(){
		return this.existenCtaMB;
	}

	/**
     * Asignar un valor a existenCtaIB.
     *
     * @param existenCtaIB Valor a asignar.
     */
	public void setExistenCtaIB(boolean existenCtaIB){
		this.existenCtaIB = existenCtaIB;
	}

	/**
	 * Obtener el valor de existenCtaIB.
     *
     * @return existenCtaIB valor asignado.
	 */
	public boolean getExistenCtaIB(){
		return this.existenCtaIB;
	}

	/**
     * Asignar un valor a descEstatusOperacion.
     *
     * @param descEstatusOperacion Valor a asignar.
     */
	public String getDescEstatusOperacion() {
		if(estatusOperacion.trim().equals("P")){
			return "PACTADO";
		}else if(estatusOperacion.trim().equals("C")){
			return "COMPLEMENTADO";
		}else if(estatusOperacion.trim().equals("L")){
			return "ENVIADO";
		}else if(estatusOperacion.trim().equals("D")){
			return "CANCELADO";
		}else if(estatusOperacion.trim().equals("N")){
			return "ENVIADO CON ERRORES";
		}else if(estatusOperacion.trim().equals("O")){
			return "CONFIRMADO";
		}else if(estatusOperacion.trim().equals("Estatus Operaci�n")){
			return "Estatus Operaci�n";
		}
		return "";
	}

	/**
	 * Override toString
	 */
	public String toStringLib(int color) {
		StringBuffer registro = new StringBuffer();
		String rellenoHtml = "";
		String colorFila = "textabdatobs";

		if(color==1)
			colorFila = "textabdatcla";

		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		if(!getEstatusOperacion().equals("L")){
			registro.append("<input type=radio name=listOperRET "
				+ " value=\"" + getIdRecord() + "\">");
		}
		registro.append("</td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getFchHoraPactado() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getUsrRegistro() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getDivisaOperante() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getContraDivisa() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + Util.muestraTipoOperUser(getTipoOperacion()) + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getTcPactado() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + aMoneda(getImporteTotOper()) + "&nbsp;" );
		registro.append("<br></td>");

		return registro.toString();
	}

	/**
	 * Override toString
	 */
	public String toStringCon(int color) {
		StringBuffer registro = new StringBuffer();
		String rellenoHtml = "";
		String colorFila = "textabdatobs";

		if(color==1)
			colorFila = "textabdatcla";

		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		if(getEstatusOperacion().equals("L") || getEstatusOperacion().equals("O") ||
		   getEstatusOperacion().equals("N") || getEstatusOperacion().equals("C")){
			registro.append("<input type=radio name=listOperRET "
				//+ " value=\"" + getFolioOper() + "\">");
				+ " value=\"" + getFolioEnla() + "|" + getFolioOper()+ "\">");
		}
		registro.append("</td>");
		if(getEstatusOperacion().trim().equals("L") || getEstatusOperacion().trim().equals("O")){
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("&nbsp;" + getFolioOper() + "&nbsp;" );
			registro.append("<br></td>");
		}else{
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("&nbsp;&nbsp;&nbsp;" );
			registro.append("<br></td>");
		}
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getDivisaOperante() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getContraDivisa() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + Util.muestraTipoOperUser(getTipoOperacion()) + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getTcPactado() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + aMoneda(getImporteTotOper()) + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getFchHoraPactado() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getFchHoraComplemento() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getFchHoraLiberado() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getUsrRegistro() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getUsrAutoriza() + "&nbsp;" );
		registro.append("<br></td>");

		if(getEstatusOperacion().trim().equals("N")){
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("<font color=red>");
			registro.append("&nbsp;" + getDescEstatusOperacion() + "&nbsp;" );
			registro.append("</font>");
			registro.append("<br></td>");
		}else if(getEstatusOperacion().trim().equals("L") || getEstatusOperacion().trim().equals("O")){
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("<font color=green>");
			registro.append("&nbsp;" + getDescEstatusOperacion() + "&nbsp;" );
			registro.append("</font>");
			registro.append("<br></td>");
		}else{
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("&nbsp;" + getDescEstatusOperacion() + "&nbsp;" );
			registro.append("<br></td>");
		}

		return registro.toString();
	}

	/**
	 * Metodo para formatear una cantidad a moneda
	 *
	 * @param num	-	Cadena con el numero a formatear
	 * @return		-	Formato de Cadena
	 */
	private String aMoneda(String num){
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return num;
	}

}