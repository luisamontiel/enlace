package mx.altec.enlace.bo;


import java.sql.SQLException;
import java.util.Date;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.SUALCLZS8;
import mx.altec.enlace.bita.*;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.dao.SUALineaCapturaDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class SUALineaCapturaAction {

	private final String ES_PAGO_SUA_LINEA_CAPTURA = "SULC";

	public SUALCLZS8 validacionPagoSUALC(String ctaCargo,String ctaAbono,
			String fecha,String importe,String lineacaptura,boolean validaManc
			,boolean pago,HttpServletRequest req ){

		SUALCLZS8 boValidacion=null;

		EIGlobal.mensajePorTrace("Valida Linea de captura---> validacionPagoSUALC."
				,EIGlobal.NivelLog.INFO);
		//Empieza validacion de Mancomunidad
		boolean mancomunado=false;


		if(validaManc){
			EIGlobal.mensajePorTrace("-----validaManc-----: "
							+validaManc,EIGlobal.NivelLog.INFO);
			BaseResource baseResource = (BaseResource) req.getSession()
											.getAttribute("session");
			BaseServlet servlet=new BaseServlet();
			String titular="";
			String beneficiario="";
			String referenciaMedio="";
			//Creamos trama
			String trama = fecha+"|" + servlet.obten_referencia_operacion
					(Short.parseShort(IEnlace.SUCURSAL_OPERANTE))
					+ "|" + baseResource.getContractNumber() + "|"
					+ ES_PAGO_SUA_LINEA_CAPTURA
					+"|||" 	+ BaseServlet.convierteUsr8a7(baseResource.getUserID8()) + "|" + ctaCargo +
					"|"+ Global.SUALC_CTA_ABONO+"||||"+lineacaptura+"|"
					+ importe + "||P|||H||" +titular+"|" +beneficiario+
							"|" +referenciaMedio+"|||";
			servlet=null;
			EIGlobal.mensajePorTrace( "trama : >" +trama+ "<", EIGlobal.NivelLog.DEBUG );

			String [] resultManc = llamada_valida_manc( trama);
			String coderror = resultManc[0];
			String buffer = resultManc[1];
			EIGlobal.mensajePorTrace( "resultado : [" +resultManc[0]+ "],["
					+resultManc[1]+ "]", EIGlobal.NivelLog.DEBUG );

			//Validamos codigo de mancomunidad - Lo amarra con el parametro de ejecucion de la operacion
			if(coderror.equals("MANC0000") && buffer.startsWith("0"))
			{
				mancomunado=true;
				//quitamos pipes al final de la trama
				if(resultManc[1].endsWith("||") ){
					resultManc[1]=resultManc[1].replace("||", "");
				}
				if(resultManc[1].endsWith("|") ){
					resultManc[1]=resultManc[1].replaceAll("|", "");
				}
				if  (!resultManc[1].substring(resultManc[1].length()-4,resultManc[1].length()).equals("MANC")) {
					mancomunado=false;
					EIGlobal.mensajePorTrace( "No hay mancomuniacion : "
							, EIGlobal.NivelLog.INFO );
				}
			}
		}
		//Validamos la mancomunidad

		if(mancomunado && validaManc){
			EIGlobal.mensajePorTrace("Valida Mancomunidad---> Operacion Mancomunada"
					,EIGlobal.NivelLog.INFO);
			boValidacion = new SUALCLZS8();

			boValidacion =(SUALCLZS8)(req.getSession().getAttribute("DatoLC")); //Llena la linea de Captura con los datos ya cargados

			//ejecutar mancomunidad
			//boValidacion.setCodErr("Operacion Mancomunada");
			boValidacion.setCodErr("MANC0000");
			boValidacion.setMsjErr("Operacion Mancomunada");
			boValidacion.setValidarToken(false);//no validamos token en mancomunidad
			boValidacion.setCodOperacion("");
			boValidacion.setFechaOperacion("");
			boValidacion.setHoraOperacion("");
			boValidacion.setEstatus("Operacion Mancomunada");
			EIGlobal.mensajePorTrace("Valida Mancomunidad---> Bitacorizamos."
					,EIGlobal.NivelLog.INFO);

			bitacoriza(req, "manc", boValidacion.getCodErr(),
					BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA, importe,ctaCargo,
					ctaAbono, BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA+"03",true);

			return boValidacion;
		}
		EIGlobal.mensajePorTrace("Valida Mancomunidad---> paso la validacion de Mancomunidad."
				,EIGlobal.NivelLog.INFO);

		if (pago){  	//ejecutamos operaciones si nesesidad de pedir token
			EIGlobal.mensajePorTrace("Ejecuta Pago SUA---> validacionPagoSUALC."
					,EIGlobal.NivelLog.DEBUG);
			SUALineaCapturaDAO capturaDAO= new SUALineaCapturaDAO();
			boValidacion = capturaDAO.
				pagoSUALC(ctaCargo,ctaAbono,fecha,importe, lineacaptura);
			//Bitacorizacion
			bitacoriza(req, "LZS8", boValidacion.getCodErr(),
				BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA, importe,ctaCargo,
				ctaAbono, BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA+"01",true);

			return boValidacion;
		}
		if(boValidacion==null){
			boValidacion = new SUALCLZS8();
		}
		return boValidacion;
	}


	/**
	 * Pago de SUA LC invocamos Dao encargado de la validacion de LC e Importe
	 * @param ctaAbono
	 * @param vtaCargo
	 * @param fecha
	 * @param importe
	 * @param lc
	 * @return
	 */
	public String validaLC(String lineaCaptura,String importeTotal,HttpServletRequest req){

		SUALineaCapturaDAO capturaDAO= new SUALineaCapturaDAO();

		EIGlobal.mensajePorTrace("inicia validaLC ",EIGlobal.NivelLog.INFO);
		//String validacionLC= capturaDAO.validaLC(lineaCaptura, importeTotal); //VAO 09-2012
		String validacionLC= "";
		SUALCLZS8 boValidacion = capturaDAO.validaLC(lineaCaptura, importeTotal);
		HttpSession session = req.getSession();

		session.setAttribute("DatoLC", boValidacion); //vao 09-2012

		validacionLC= boValidacion.getMsjErr();
		//bitacora
		bitacoriza(req, "LZS0",validacionLC,
				BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA, importeTotal,null,
				null, BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA+"00",true);
		EIGlobal.mensajePorTrace("Retorna validaLC ",EIGlobal.NivelLog.INFO);
		return validacionLC;
	}

	/**
	 * Metodo para la bitacorizacion
	 * parametros requeridos todos
	 * parametros indispensables request,numBit
	 * @param request
	 * @param servicio -ejemplo LZS8
	 * @param codError -de ocho posiciones
	 * @param idFlujo  -ejemplo ELCS
	 * @param importe
	 * @param numeroContrato
	 * @param ctaCargo
	 * @param numBit
	 * @param inicia
	 */
	public static void bitacoriza(HttpServletRequest request, String servicio,
			String codError,String idFlujo,String importe,String numeroContrato,
			String ctaCargo,String numBit, boolean inicia) {
		try{
		if(Global.USAR_BITACORAS.trim().equals("ON")) {
			BitaTransacBean bt = new BitaTransacBean();

			HttpSession session = request.getSession();
			BaseResource baseResource = (BaseResource) session.getAttribute("session");
			BitaHelperImpl bh = new BitaHelperImpl(request, baseResource, session);
			if(inicia){
				bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
			}
			bh.llenarBean(bt);
			bt.setTipoMoneda("");
			bt.setEstatus("A");
			bt.setContrato(baseResource.getContractNumber());
			bt.setUsr(baseResource.getUserID8());
			if(numBit==null ||numBit.length()==0){
				EIGlobal.mensajePorTrace("NumBit no es correcto...",EIGlobal.NivelLog.DEBUG);
			}
			bt.setNumBit(numBit);
			if(importe!=null ){
				try{
				bt.setImporte(Double.valueOf(importe));
				}catch (NumberFormatException e) {
					bt.setImporte(0.0);
				}
			}
			if(numeroContrato!=null){
				bt.setCctaOrig(numeroContrato);
			}
			if(ctaCargo!=null){
				bt.setCctaDest(ctaCargo);
			}
			bt.setFechaAplicacion(new Date());
			if(servicio!=null){
				bt.setServTransTux(servicio);
			}
			if(idFlujo!=null){
				bt.setIdFlujo(idFlujo);
			}
			if(codError!=null){
				if(codError.length()>8){
					if(codError.equals("Operacion Exitosa")){
						bt.setIdErr("SULC0000");
					}else{
						if(codError.length()>8){
							bt.setIdErr(codError.substring(0, 8));
						}else{
							bt.setIdErr(codError);
						}
					}
				}else{
					bt.setIdErr(codError);
				}
			}
			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
				EIGlobal.mensajePorTrace("Bitacora Exitosa",EIGlobal.NivelLog.DEBUG);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error en Bitacora",EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(e.getMessage(),EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("Error en Bitacora",EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(e.getMessage(),EIGlobal.NivelLog.ERROR);
			}

		}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("Error Desconocido en Bitacora: " +e.getMessage(),EIGlobal.NivelLog.DEBUG);
		}
	}
	/**
	 *
	 * @param trama
	 * @param servicioManc
	 * @return
	 */
    public String[] llamada_valida_manc ( String trama) {

	   	 // Trama Completa
	   	 // Los campos pueden ir vacios, dependera de la accion a realizar
	   	 //
	   	 //fch_registro,  folio_registro, contrato,  tipo_operacion,fch_autorizac,
	   	 //folio_autorizac, usuario,  cta_origen, cta_destino,banco,  sucursal,  plaza,
	   	 //concepto2,	importe,  estatus,tipo_origen,	tipo_destino,  plazo,
	   	 //instruccion,  titulos,  titular,beneficiario,  referencia_medio, op_prog, fch_prog
	   	 String result_manc[] = new String [2];
	   	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::llamada_valida_manc( " +trama+ " )",
	   			 EIGlobal.NivelLog.DEBUG);
	   	 String buffer = "";
	   	 String coderror = "";
	   	 String MANC_VALIDA_OK_CODE = "MANC0000";

	   	 Hashtable htResult = null;
	   	 ServicioTux ctRemoto = new ServicioTux ();

	   		 try {
	   		     htResult = ctRemoto.manc_valida(trama);
	   		 } catch ( java.rmi.RemoteException re ) {

	   		     EIGlobal.mensajePorTrace ("ERROR->llamada_valida_manc->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
	   		 } catch(Exception e) {}

	   	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Procesando resultado",EIGlobal.NivelLog.DEBUG);
	   	 if(htResult!=null){
	   		 coderror = ( String ) htResult.get ( "COD_ERROR" );
	   	 	buffer = ( String ) htResult.get ( "BUFFER" );
	   	 }
	   	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "COD_ERROR : >" +coderror+ "<",EIGlobal.NivelLog.DEBUG);
	   	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.DEBUG);
	   	 if ( coderror == null ) {
	   	     buffer = "Problemas con MANC_VALIDA";
	   	     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "CODERROR MANC_VALIDA NULO, ",EIGlobal.NivelLog.INFO);
	   	     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.INFO);
	   	 } else if ( coderror.equals ( MANC_VALIDA_OK_CODE ) ) {
	   		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "MANC_VALIDA OK, const = >" +MANC_VALIDA_OK_CODE+ "<",EIGlobal.NivelLog.INFO);
	   		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.INFO);
	   	 } else {
	   		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "MANC_VALIDA ERROR",EIGlobal.NivelLog.ERROR);
	   		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.ERROR);
	   	 }
	   	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Saliendo de llamada_valida_manc",EIGlobal.NivelLog.DEBUG);
	   	 result_manc[0] = coderror;
	   	 result_manc[1] = buffer;
	   	 return result_manc;
        }

    /********************************************/
    /** Metodo: llamada_bitacora
     * @return String
     * @param int -> numeroReferencia
     * @param String -> coderror1
     * @param String -> contrato
     * @param String -> usuario
     * @param String -> cuenta
     * @param double -> importe
     * @param String -> tipo_operacion
     * @param String -> cuenta2
     * @param int -> cant_titulos
     * @param String -> dispositivo2
     * @param String -> folioSUA
     */
    /********************************************/
         public String bitacoraSUALC ( int  numeroReferencia, String coderror1,
        		 String contrato, String usuario,String cuenta, double importe,
        		 String tipo_operacion, String cuenta2, int cant_titulos,
        		 String dispositivo2 ,String folioSUa) {

        	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Entrando a llamada_bitacora ", EIGlobal.NivelLog.DEBUG);
    	 String coderror    = "";
    	 Hashtable htResult = null;
    	 try {
    	     ServicioTux ctRemoto = new ServicioTux ();
    	     htResult = ctRemoto.sbitacoraSUALC( numeroReferencia, coderror1,
    	     contrato, usuario, cuenta, importe, tipo_operacion,
    	     cuenta2, cant_titulos, dispositivo2,folioSUa );

    	 } catch ( java.rmi.RemoteException re ) {
    	     EIGlobal.mensajePorTrace ("ERROR->llamada_bitacora->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
    	 } catch ( Exception e ) {
    	     EIGlobal.mensajePorTrace ("ERROR->llamada_bitacora->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
    	 }
    	 String BITACORA_OK_CODE = "BITA0000";
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Procesa resultado: " + htResult, EIGlobal.NivelLog.DEBUG);
    	 coderror = ( String ) htResult.get ( "COD_ERROR" );
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Procesa coderror: " + coderror, EIGlobal.NivelLog.DEBUG);
    	 if ( coderror == null )
    	     coderror = "BITACORA ERROR";
    	 if ( coderror.equals (BITACORA_OK_CODE ) ) {
    		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BITACORA OK, termina",EIGlobal.NivelLog.DEBUG);
    	 } else {
    		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BITACORA ERROR, termina",EIGlobal.NivelLog.ERROR);
    	 }
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Saliendo de llamada_bitacora ",EIGlobal.NivelLog.DEBUG);
    	 return coderror;
         }

}