package mx.altec.enlace.bo;
import java.util.ArrayList;

public class CatNominaBean {

	private ArrayList listaRegistros;
	private ArrayList listaErrores;
	private int totalRegistros;
	private String numContrato;
	private String descContrato;
	private String cveUsuario;
	private String numTarjeta;
	private String numCuenta;
	//private int ingresoMensual;
	private String ingresoMensual;
	private int extOfi;
	private int telOfi;
	private int prefTelOfi;
	private String paisOfi;
	private String codPostalOfi;
	private int cveDireccionOfi;
	private String cveEstadoOfi;
	private String ciudadOfi;
	private String delegOfi;
	private String coloniaOfi;
	private String calleOfi;
	private String fchResidencia;
	private String fchIngresoEmpl;
	private int telPart;
	private String prefTelPart;
	private String estatusCasa;
	private String clavePais;
	private String codigoPostal;
	private String cveEstado;
	private String ciudad;
	private String delegacion;
	private String colonia;
	private String calle;
	private String nombreCorto;
	private String edoCivil;
	private String nacionalidad;
	private String sexo;
	private String rfc;
	private String apellidoM;
	private String apellidoP;
	private String nombre;
	private String deptoEmpl;
	private String numEmpl;

	private boolean existenReg = true;
	private boolean falloInesperado = false;

	public boolean getFalloInesperado() {
		return falloInesperado;
	}
	public void setFalloInesperado(boolean falloInesperado) {
		this.falloInesperado = falloInesperado;
	}

	public int getCveDireccionOfi() {
		return cveDireccionOfi;
	}
	public void setCveDireccionOfi(int cveDireccionOfi) {
		this.cveDireccionOfi = cveDireccionOfi;
	}

	public String getCveEstadoOfi() {
		return cveEstadoOfi;
	}
	public void setLCveEstadoOfi(String cveEstadoOfi) {
		this.cveEstadoOfi = cveEstadoOfi;
	}

	public ArrayList getListaRegistros() {
		return listaRegistros;
	}
	public void setListaRegistros(ArrayList listaRegistros) {
		this.listaRegistros = listaRegistros;
	}


	public ArrayList getListaErrores() {
		return listaErrores;
	}
	public void setListaErrores(ArrayList listaErrores) {
		this.listaErrores = listaErrores;
	}

	public int getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	public boolean getExistenReg() {
		return existenReg;
	}
	public void setExistenReg(boolean existenReg) {
		this.existenReg = existenReg;
	}

	//***** Alta de Empleados con Cuenta Interbancaria ***//
	public String getNumEmpl() {
		return numEmpl;
	}
	public void setNumEmpl(String numEmpl) {
		this.numEmpl = numEmpl;
	}


	public String getDeptoEmpl() {
		return deptoEmpl;
	}
	public void setDeptoEmpl(String deptoEmpl) {
		this.deptoEmpl = deptoEmpl;
	}


	public String getNombreEmpl() {
		return nombre;
	}
	public void setNombreEmpl(String nombreEmpl) {
		this.nombre = nombreEmpl;
	}


	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}


	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}


	public String getRFC() {
		return rfc;
	}
	public void setRFC(String rfc) {
		this.rfc = rfc;
	}


	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}


	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}


	public String getEdoCivil() {
		return edoCivil;
	}
	public void setEdoCivil(String edoCivil) {
		this.edoCivil = edoCivil;
	}


	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}


	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}


	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia= colonia;
	}


	public String getDelegacion() {
		return delegacion;
	}
	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}



	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public String getClavePais() {
		return clavePais;
	}
	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}




	public String getEstatusCasa() {
		return estatusCasa;
	}
	public void setEstatusCasa(String estatusCasa) {
		this.estatusCasa = estatusCasa;
	}


	public String getPrefTelPart() {
		return prefTelPart;
	}
	public void setPrefTelPart(String prefTelPart) {
		this.prefTelPart = prefTelPart;
	}


	public int getTelPart() {
		return telPart;
	}
	public void setTelPart(int telPart) {
		this.telPart = telPart;
	}


	public String getFchIngresoEmpl() {
		return fchIngresoEmpl;
	}
	public void setFchIngresoEmpl(String fchIngresoEmpl) {
		this.fchIngresoEmpl = fchIngresoEmpl;
	}


	public String getFchResidencia() {
		return fchResidencia;
	}
	public void setFchResidencia(String fchResidencia) {
		this.fchResidencia = fchResidencia;
	}


	public String getCalleOfi() {
		return calleOfi;
	}
	public void setCalleOfi(String calleOfi) {
		this.calleOfi = calleOfi;
	}


	public String getColoniaOfi() {
		return coloniaOfi;
	}
	public void setColoniaOfi(String coloniaOfi) {
		this.coloniaOfi = coloniaOfi;
	}

	public String getDelegOfi() {
		return delegOfi;
	}
	public void setDelegOfi(String delegOfi) {
		this.delegOfi = delegOfi;
	}


	public String getCiudadOfi() {
		return ciudadOfi;
	}
	public void setCiudadOfi(String ciudadOfi) {
		this.ciudadOfi = ciudadOfi;
	}


	public String getCodPostalOfi() {
		return codPostalOfi;
	}
	public void setCodPostalOfi(String codPostalOfi) {
		this.codPostalOfi = codPostalOfi;
	}

	public String getPaisOfi() {
		return paisOfi;
	}
	public void setPaisOfi(String paisOfi) {
		this.paisOfi = paisOfi;
	}


	public int getPrefTelOfi() {
		return prefTelOfi;
	}
	public void setPrefTelOfi(int prefTelOfi) {
		this.prefTelOfi = prefTelOfi;
	}


	public int getTelOfi() {
		return telOfi;
	}
	public void setTelOfi(int telOfi) {
		this.telOfi = telOfi;
	}


	public int getExtOfi() {
		return extOfi;
	}
	public void setExtOfi(int extOfi) {
		this.extOfi = extOfi;
	}


	/*public int getIngresoMensual() {
		return ingresoMensual;
	}
	public void setIngresoMensual(int ingresoMensual) {
		this.ingresoMensual = ingresoMensual;
	}*/

	public String getIngresoMensual() {
		return ingresoMensual;
	}
	public void setIngresoMensual(String ingresoMensual) {
		this.ingresoMensual = ingresoMensual;
	}


	public String getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}


	public String getCveUsuario() {
		return cveUsuario;
	}
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario= cveUsuario;
	}

	public String getDescContrato() {
		return descContrato;
	}
	public void setDescContrato(String descContrato) {
		this.descContrato =  descContrato;
	}


	public String getNumContrato() {
		return numContrato;
	}
	public void setNumContrato(String numContrato) {
		this.numContrato= numContrato;
	}

	public String getCveEstado() {
		return cveEstado;
	}
	public void setCveEstado(String cveEstado) {
		this.cveEstado = cveEstado;
	}








}