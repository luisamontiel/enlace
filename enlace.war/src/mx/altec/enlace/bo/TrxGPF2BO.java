package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.Vector;

import mx.altec.enlace.beans.AdmTrxGPF2;
import mx.altec.enlace.dao.TrxGPF2DAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase Bussines para manejar el procedimiento para la transaccion PE80
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 28, 2011
 */
public class TrxGPF2BO {
	
	/**
	 * Objeto contenedor de tipo AdmTrxGPF2
	 */
	private AdmTrxGPF2 bean;

	/**
	 * Obtener el valor de bean.
     *
     * @return bean valor asignado.
	 */
	public AdmTrxGPF2 getBean() {
		return bean;
	}
	
	/**
	 * Metodo para obtener todo el catalogo de lo que antes era la tabla
	 * CAMB_BANCO_EU
	 * 
	 * @return catAdmTrxGPF2	Vector contenedor de objetos de tipo TrxGPF2VO
	 */
	public Vector obtenCatalogoCambBancoEU(){
		EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoCambBancoEU:: Entrando....", EIGlobal.NivelLog.DEBUG);		

		TrxGPF2DAO dao = new TrxGPF2DAO();
	    MQQueueSession mqsess = null;
	    Vector catAdmTrxGPF2 = new Vector();
	    String idBanco = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, 
	    									NP_JNDI_QUEUE_RESPONSE, 
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	do{
	    		if(bean!=null)
	    			idBanco = bean.getLlavePaginacion();
	    		
		    	bean = dao.consultaTrxGPF2(rellenar("",11,' ','D'), 
		    								   rellenar("",105,' ','D'),
		    								   rellenar("001",3,' ','D'),
		    								   rellenar("",35,' ','D'),
		    								   rellenar("A",1,' ','D'),
		    								   rellenar(idBanco,11,' ','D'));
		    	EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoCambBancoEU:: Codigo operacion>>" 
		    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
		    			    	   	
	    		for(int i=0; i<bean.getCatGpf2Bean().size(); i++){
	    			try{
	    				Long.parseLong(((TrxGPF2VO)bean.getCatGpf2Bean().get(i)).getOIdBaCo());
	    				catAdmTrxGPF2.add((TrxGPF2VO)bean.getCatGpf2Bean().get(i));
	    			}catch(Exception e){}	    			    			
	    		}
		    	
	    	}while(bean.getMasPaginacion());	    		
	    	    	
	    }catch (Exception e){
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenCatalogoCambBancoEU...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoCambBancoEU:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoCambBancoEU:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }
		EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoCambBancoEU:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return catAdmTrxGPF2;
	}
	
	/**
	 * Metodo para obtener todo el catalogo de todos los bancos internacionales
	 * 
	 * @return catAdmTrxGPF2	Vector contenedor de objetos de tipo TrxGPF2VO
	 */
	public Vector obtenCatalogoBancos(String cveAbaSwift, String cvePais, String desCiudad, String indBusqueda){
		EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoBancos:: Entrando....", EIGlobal.NivelLog.DEBUG);		

		TrxGPF2DAO dao = new TrxGPF2DAO();
	    MQQueueSession mqsess = null;
	    Vector catAdmTrxGPF2 = new Vector();
	    String idBanco = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, 
	    									NP_JNDI_QUEUE_RESPONSE, 
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	do{
	    		if(bean!=null)
	    			idBanco = bean.getLlavePaginacion();
	    		
		    	bean = dao.consultaTrxGPF2(rellenar(cveAbaSwift,11,' ','D'), 
	    								   rellenar("",105,' ','D'),
	    								   rellenar(cvePais,3,' ','D'),
	    								   rellenar(desCiudad,35,' ','D'),
	    								   rellenar(indBusqueda,1,' ','D'),
	    								   rellenar(idBanco,11,' ','D'));
		    	EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoBancos:: Codigo operacion>>" 
		    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
		    			    	   	
	    		for(int i=0; i<bean.getCatGpf2Bean().size(); i++){
	    			try{	    				
	    				catAdmTrxGPF2.add((TrxGPF2VO)bean.getCatGpf2Bean().get(i));
	    			}catch(Exception e){}	    			    			
	    		}
		    	
	    	}while(bean.getMasPaginacion());	    		
	    	    	
	    }catch (Exception e){
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenCatalogoCambBancoEU...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoBancos:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoBancos:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }					
		EIGlobal.mensajePorTrace("TrxGPF2BO::obtenCatalogoBancos:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return catAdmTrxGPF2;
	}	

}
