package mx.altec.enlace.bo;

import java.sql.SQLException;

import mx.altec.enlace.beans.AdminHorariosBean;
import mx.altec.enlace.dao.AdminHorariosDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class AdminHorariosBO {

	/**
	 * valida horario de una funcionalidad
	 * @param bean contiene los datos a registrar
	 * @return bean con horario validado
	 */
	public AdminHorariosBean validarHorario(AdminHorariosBean bean){

		//Se consulta el horario de la funcionalidad
		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: consultarHorario() :: consultando horario de fucnionalidad", EIGlobal.NivelLog.DEBUG);
		AdminHorariosDAO dao=new AdminHorariosDAO();
		AdminHorariosBean resultado=null;

		try{
			resultado=dao.consultarHorario(bean);
			resultado.setHoraActual(bean.getHoraActual());//Se setea en el resultado la hora actual que se envia en el servlet
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace("AdminHorariosBO.java::validarHorario() :: Error SQLException", EIGlobal.NivelLog.DEBUG);
		}

		if(resultado.getTieneHorarioActivo()){//Tiene activo un horario se Inicia validacion de horario

			//Se llama la funcion obtener hora Incio y Fin del dia
			resultado=obtieneHorario(resultado);

			//Se valida si la hora en la que se ingresa al modulo esta dentro del rango
	        if(resultado.isHorarioValido()){
	           resultado=validaRangoHorario(resultado);
	        }

			//Se valida si el dia en que se ingresa es inhabil
			if(resultado.isHorarioValido() && "N".equals(resultado.getDiaInhabil())){
				resultado=validaDiaInhabil(bean);
			}
		}else{//No tiene activo un hoario por lo que no se realiza validacion de horario
			resultado.setHorarioValido(true);//Como no tiene horario activo se considera como horario valido para que continue por flujo normal
			EIGlobal.mensajePorTrace("AdminHorariosBO.java :: validarHorario() :: No tiene activo un horario... continuara por flujo normal", EIGlobal.NivelLog.DEBUG);
		}

		EIGlobal.mensajePorTrace("AdminHorariosBO.java::fin validarHorario() :: ", EIGlobal.NivelLog.DEBUG);
		return resultado;
	}

	/**
	 * obtieneHorario funcion que permite obtener la hora inicio y fin del dia
	 * @param bean parametros de datos
	 * @return AdminHorariosBean resultado hora inicio y fin
	 */
	public AdminHorariosBean obtieneHorario(AdminHorariosBean bean){
		//Obtienen del resultado el dia de la semana en que se ingresa
		int dia=Integer.parseInt(bean.getDiaSistema());

		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: obtieneHorario() :: obteniendo hora inicio y fin del: "+dia, EIGlobal.NivelLog.DEBUG);

		switch (dia) {
			case 1://Domingo
				if("S".equals(bean.getDomingo())){
		            bean.setHoraInicio(bean.getHoraIniDom());
		            bean.setHoraFin(bean.getHoraFinDom());
		            bean.setHorarioValido(true);
		        }else{
		        	bean.setHorarioValido(false);
		        }
			break;
			case 2://Lunes
				if("S".equals(bean.getLunes())){
		            bean.setHoraInicio(bean.getHoraIniSem());
		            bean.setHoraFin(bean.getHoraFinSem());
		            bean.setHorarioValido(true);
		        }else{
		        	bean.setHorarioValido(false);
		        }
			break;
			case 3://Martes
				if("S".equals(bean.getMartes())){
		            bean.setHoraInicio(bean.getHoraIniSem());
		            bean.setHoraFin(bean.getHoraFinSem());
		            bean.setHorarioValido(true);
		        }else{
		        	bean.setHorarioValido(false);
		        }
			break;
			case 4://Miercoles
				if("S".equals(bean.getMiercoles())){
		            bean.setHoraInicio(bean.getHoraIniSem());
		            bean.setHoraFin(bean.getHoraFinSem());
		            bean.setHorarioValido(true);
		        }else{
		        	bean.setHorarioValido(false);
		        }
			break;
			case 5://Jueves
				if("S".equals(bean.getJueves())){
		            bean.setHoraInicio(bean.getHoraIniSem());
		            bean.setHoraFin(bean.getHoraFinSem());
		            bean.setHorarioValido(true);
		        }else{
		        	bean.setHorarioValido(false);
		        }
			break;
			case 6://Viernes
				if("S".equals(bean.getViernes())){
		            bean.setHoraInicio(bean.getHoraIniSem());
		            bean.setHoraFin(bean.getHoraFinSem());
		            bean.setHorarioValido(true);
		        }else{
		        	bean.setHorarioValido(false);
		        }
			break;
			case 7://Sabado
				if("S".equals(bean.getSabado())){
		            bean.setHoraInicio(bean.getHoraIniSab());
		            bean.setHoraFin(bean.getHoraFinSab());
		            bean.setHorarioValido(true);
		        }else{
		        	bean.setHorarioValido(false);
		        }
			break;
			default:
				bean.setHorarioValido(false);
			break;
		}
		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: obtieneHorario() :: dia: "+dia+" H_INI: "+bean.getHoraInicio()+" H_FIN: "+bean.getHoraFin()+" HR_VALIDO: "+bean.isHorarioValido(), EIGlobal.NivelLog.DEBUG);
		//Devuelve como resultado la hora inicio y fin del dia asi como la variable horarioValido en verdadero
		return bean;
	}

	/**
	 * Valida si la hora en que se ingresa al modulo esta dentro del rango
	 * @param bean parametros de datos
	 * @return AdminHorariosBean resultado horario valido
	 */
	public AdminHorariosBean validaRangoHorario(AdminHorariosBean bean){
		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: validaRangoHorario() :: validando hora de ingreso["+bean.getHoraActual()+"]", EIGlobal.NivelLog.DEBUG);

		//Obtiene la horaActual en que se ingresa al modulo asi como la hora inicio y fin configurada en el flujo
		//IMPORTATANTE Tanto la horaActual como hora inicio y fin deben tener el formato hhmi
		if(Integer.parseInt(bean.getHoraInicio()) < Integer.parseInt(bean.getHoraFin())){
			if( (Integer.parseInt(bean.getHoraActual()) >= Integer.parseInt(bean.getHoraInicio())) &&
				(Integer.parseInt(bean.getHoraActual()) <= Integer.parseInt(bean.getHoraFin())) ){
				bean.setHorarioValido(true);
			}else{
				bean.setHorarioValido(false);
			}
		}else{
			if( (Integer.parseInt(bean.getHoraActual()) >= Integer.parseInt(bean.getHoraInicio())) ||
					(Integer.parseInt(bean.getHoraActual()) <= Integer.parseInt(bean.getHoraFin())) ){
					bean.setHorarioValido(true);
				}else{
					bean.setHorarioValido(false);
				}
		}

		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: validaRangoHorario() :: DENTRO DE RANGO: "+bean.isHorarioValido(), EIGlobal.NivelLog.DEBUG);
		//Devuelve la bandera horarioActivo como verdadera si la hora de ingreso esta dentro del rango
		return bean;
	}

	/**
	 * Valida si el dia es inhabil
	 * @param bean parametros de datos
	 * @return AdminHorariosBean resultado dia inhabil
	 */
	public AdminHorariosBean validaDiaInhabil(AdminHorariosBean bean){
		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: validaDiaInhabil() :: validando dia inhabil", EIGlobal.NivelLog.DEBUG);
		AdminHorariosDAO dao=new AdminHorariosDAO();
		try{
			bean=dao.consultarDiaInhabil(bean);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace("AdminHorariosBO.java::validaDiaInhabil() :: Error SQLException", EIGlobal.NivelLog.DEBUG);
		}
		//Si encuentra el registro es inhabil
		if(Integer.parseInt(bean.getDiaInhabil()) == 0){
			bean.setHorarioValido(true);
		}else{//Si no se encuentra es habil
			bean.setHorarioValido(false);
		}

		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: validaDiaInhabil() :: DIA INHABIL:"+bean.isHorarioValido(), EIGlobal.NivelLog.DEBUG);

		return bean;
	}
	
	/**
	 * Ejecuta la validacion del nombre del servlet al que va dirigida la peticion. 
	 * @param bean Contiene los parametros necesarios para realizar la validacion.
	 * @return AdminHorariosBean Contiene el resultado de la validacion. 
	 */
	public AdminHorariosBean validaNombreServlet(AdminHorariosBean bean){
		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: validaNombreServlet() :: consultando horario de fucnionalidad", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("AdminHorariosBO.java :: validaNombreServlet() :: El nombre del servlet a validar es: " + bean.getNombreServlet(), EIGlobal.NivelLog.DEBUG);
		AdminHorariosDAO dao = new AdminHorariosDAO();
		AdminHorariosBean resultado = null;
		try{
			resultado = dao.consultaNombreServlet(bean);
		}catch(SQLException ex){
			EIGlobal.mensajePorTrace("AdminHorariosBO.java::validaNombreServlet() :: Error SQLException", EIGlobal.NivelLog.DEBUG);
		}
		
		return resultado;
	}
}