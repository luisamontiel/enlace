/*
 * pdBeneficiario.java
 *
 * Created on 27 de marzo de 2002, 12:34 PM
 */

package mx.altec.enlace.bo;

/** Contenedor para los datos del Beneficiario
 *
 * @author Rafael Martinez
 * @version 1.0
 */
public class pdBeneficiario implements java.io.Serializable {

    /** Nombre del beneficiario
     */
    protected String sNombre = "";

    /** ID del beneficiario
     */
    protected String sID = "";

    /** Fecha de alta del Beneficiario
     */
    protected String sFechaAlta = "";

    /** Creates new pdBeneficiario */
    public pdBeneficiario() {
    }

    /*coloca el nombre del beneficiario
     */
    /** setter para el nombre del beneficiario
     * @param Nombre nuevo nombre
     */
    public void setNombre(String Nombre){
        sNombre= Nombre;
    }
    /*coloca el ID del beneficiario
     */
    /** setter para el id del beneficiario
     * @param ID nuevo id
     */
    public void setID(String ID){
        sID=ID;
    }
    /*coloca la fecha de alta del beneficiario
     */
    /** setter para la fecha de alta del beneficiario
     * @param fechaAlta nueva fecha de alta
     */
    public void setFechaAlta(String fechaAlta){
        sFechaAlta = fechaAlta;
    }
    /*obtiene el nombre del beneficiario
     */
    /** Obteine el Nombre
     * @return nombre
     */
    public String getNombre(){
        return sNombre;
    }
    /*obtiene el id del beneficiario
     */
    /** Obtiene el ID
     * @return el ID
     */
    public String getID(){
        return sID;
    }
    /*obtiene la fecha de alta del beneficiario
     */
    /** regresa la fecha de alta
     * @return la fecha de alta
     */
    public String getFechaAlta(){
        return sFechaAlta;
    }
}
