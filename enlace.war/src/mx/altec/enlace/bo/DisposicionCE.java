// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.*;
import java.util.Vector;

/**Esta clase es un <I>bean</I> para la Disposici&oacute;n Vista
 * <P>
 * @author Hugo Ru&iacute;z Zepeda
 */
public class DisposicionCE implements Serializable
{
/**Almacena el n&uacute;mero de contrato.
*/
private String contrato=new String("");

/**Almacena el n&uacute;mero de usuario
*/
private String usuario=new String("");

/**Guarda el n�mero de cuenta de cheques.
*/
private String cuenta=new String("");

/**Guarda el n&uacute;mero de l&iacute;nea de cr&eacute;dito.
*/
private String lineaCredito=new String("");

/**Cantidad monetaria del importe de la disposici&oacute;n
*/
private String importe=new String("");

/**N&uacute;mero de d&iacute;as del plazo.
*/
private String plazo=new String("");

/**Descripci&oacute;n del concepto de la disposici&oacute;n
*/
private String concepto=new String("");

/**Indica si el concepto debe o no ser incluido.
*/
private boolean incluirConcepto=false;

/**Descripci&oacute;n de la cuenta de cheques.
*/
private String descripcion=new String("");

/**Tasa de inter&eacute;s de la disposici&oacute;n
*/
private String tasa=new String("");

/**Fecha en la se efect&uacute;a la disposici&oacute;n
*/
private String fechaOperacion=new String("");

/**Hora en la que se efect&uacute;a la disposici&oacute;n
*/
private String horaOperacion=new String("");

/**Contiene los errores que surgen al interpretar la trama.
*/
private Vector errores=new Vector();


  /**Constructor vac&iacute;o
   */
  public DisposicionCE()
  {
  } // Fin constructor


  public String getCuenta()
  {
    return cuenta;
  }


  public void setCuenta(String cuenta)
  {
    int num=0;

    if(cuenta!=null)
    {

      try
      {
        num=Integer.parseInt(cuenta);

        if(num>0)
        {
          this.cuenta=cuenta;
        }
        else
        {
          errores.addElement("Debe asignar un n&uacute;mero de cuenta mayor que cero\n");
        } // Fin if-else
      }
      catch(Exception e)
      {
        errores.addElement("La cuenta no es n&uacute;mero v&aacute;lido.");
      } // Fin try-catch

    } // Fin if cuenta
  } // Fin cuenta


  public String getLineaCredito()
  {
    return lineaCredito;
  }

  public void setLineaCredito(String lineaCredito)
  {
    int num=0;

    if(lineaCredito!=null)
    {

      try
      {
        num=Integer.parseInt(lineaCredito);

        if(num>0)
        {
          this.lineaCredito=lineaCredito;
        }
        else
        {
          errores.addElement("El n&uacute;mero de linea debe ser mayor que cero");
        } // Fin if-else linea
      }
      catch(Exception e)
      {
        errores.addElement("El n&uacute;mero de l&iacute;nea debe ser num&eacute;rico");
      } // Fin try-catch
    } // Fin if nulo
  } // Fin m�todo setLinea


  public String getImporte()
  {
    return importe;
  }

   /**
   */
  public void setImporte(String importe)
  {
    float num=0;

    if(importe!=null)
    {
      try
      {
        num=Float.parseFloat(importe);

        if(num>0)
        {
          this.importe=importe;
        }
        else
        {
          errores.addElement("El importe debe ser mayor que cero\n");
        } // Fin if-else
      }
      catch(Exception e)
      {
        errores.addElement("El importe debe se num&eacute;rico: "+e.getMessage());
      } // Fin if try-catch
    } // Fin if nulo
  } // Fin setImporte


  public String getPlazo()
  {
    return plazo;
  }


  public void setPlazo(String plazo)
  {
    int num=0;

    if(plazo!=null)
    {

      try
      {
        num=Integer.parseInt(plazo);

        if(num>0)
        {
          this.plazo=plazo;
        }
        else
        {
          errores.addElement("El plazo debe ser mayor que cero");
        } // Fin if
      }
      catch(Exception e)
      {
        errores.addElement("El plazo debe ser num&eacute;rico.");
      } // Fin try-catch
    } // Fin if nulo
  } // Fin setPlazo


  public String getConcepto()
  {
    return concepto;
  }

  public void setConcepto(String concepto)
  {
    if(concepto!=null)
    {
      this.concepto=concepto;
    }
    else
    {
      errores.addElement("Concepto nulo.\n");
    } //Fin if-else
  } // Fin setConcepto


  public String getDescripcion()
  {
    return descripcion;
  } // Fin getDescripcion


  public void setDescripcion(String descripcion)
  {
    if(descripcion!=null)
      this.descripcion=descripcion;
  }


  public String getTasa()
  {
    return tasa;
  }

  public void setTasa(String tasa)
  {
    if(tasa!=null)
      this.tasa=tasa;
  }


  public String getContrato()
  {
    return contrato;
  }


  public void setContrato(String contrato)
  {
    if(contrato!=null)
      this.contrato=contrato;
  }


  public String getUsuario()
  {
    return usuario;
  }


  public void setUsuario(String usuario)
  {
    if(usuario!=null)
      this.usuario=usuario;
  }


  public String getFechaOperacion()
  {
    return fechaOperacion;
  }


  public void setFechaOperacion(String fechaOperacion)
  {
    if(fechaOperacion!=null)
      this.fechaOperacion=fechaOperacion;
  }


  public String getHoraOperacion()
  {
    return horaOperacion;
  }


  public void setHoraOperacion(String horaOperacion)
  {
    if(horaOperacion!=null)
      this.horaOperacion=horaOperacion;
  }


  public Vector getErrores()
  {
    return errores;
  }

 /**<code><B><I></I></B><code> regresa una lista de errores en una cadena
 */
  public String cadenaErrores()
  {
    String acumulados=new String("");

    for(int i=0;i<errores.size();i++)
    {
      acumulados+=(String)errores.elementAt(i)+'\n';
    } // Fin for


    return acumulados;
  } // Fin cadenaErrores



} // Fin Disposicion

