/**
 * Clase para manejo de informaci�n de mancomunidad
 *
 *Modificado: 17/11/2003 por Maria Elena de la Pe�a P. Incidencia: IM227985
 */
package mx.altec.enlace.bo;

import java.text.NumberFormat;
import java.util.Locale;
import java.net.InetAddress;
import java.net.UnknownHostException;
import mx.altec.enlace.utilerias.EIGlobal;

    public class DatosMancSA implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
	public static final String COLOR_CLARO = "textabdatcla";
    public static final String COLOR_OBSCURO = "textabdatobs";
    private String fch_registro = "";
    private String tipo_operacion = "";
    private String fch_autorizacion = "";
    private String folio_archivo = "";
    private String registros = "";
    private String importe = "";
    private String usrRegistro = "";
    private String usrAutoriza1 = "";
    private String usrAutoriza2 = "";
    private String usrAutoriza3 = "";
    private String estatus = "";

    private String folio_registro = "";
    private String cta_cargo = "";
    private String descrip_cargo = "";
    private String cta_abono = "";
    private String descrip_abono = "";
    private String fch_aplicacion = "";
    private String beneficiario = "";
    private String banco = "";
    private String concepto = "";
    private String ref_interbancaria = "";
    private String forma_aplicacion = "";
    private Convertidor con = new Convertidor();

    private String referencia;
	private String fch_eje_prog;
	private String tipo_origen;
	private String tipo_oper_prog;
	private String tipo_destino;
	private String plaza;
	private String referencia_medio;
	private String sucursal;
	private String titular;
	private String aPaterno = ""; //Apellido paterno
	private String aMaterno = ""; //Apellido materno
	private String nombreEmp = ""; //Nombre del empleado
	private String contrato;
	
		/** Campo que contiene informacion con respecto al tipo de divisa  de una transferencia */
	private String divisaSpid = "";

	/** Campo que contiene informacion con respecto al rfc de una transferencia */
	private String rfcSpid = "";

    /** Constructor vacio reglamentario
     */
    public DatosMancSA () {
    }

    /** Constructor que recibe los campos del bean en una trama
     */
    public DatosMancSA (String trama, String op) {
		pdTokenizer tokenizador = new pdTokenizer (trama, ";", 2);
		try {
			if (op.equals("arch")) {
			    this.fch_registro = tokenizador.nextToken ();
			    this.tipo_operacion = tokenizador.nextToken ();
			    this.fch_autorizacion = tokenizador.nextToken ();
			    this.folio_archivo = tokenizador.nextToken ();
			    this.importe = tokenizador.nextToken ();
			    this.usrRegistro = tokenizador.nextToken ();
			    this.usrAutoriza1 = tokenizador.nextToken ();
			    this.usrAutoriza2 = tokenizador.nextToken ();
			    this.usrAutoriza3 = tokenizador.nextToken ();

			    if (!this.usrRegistro.equals(""))
			        this.usrRegistro = con.cliente_7To8(this.usrRegistro);
			    if (!this.usrAutoriza1.equals(""))
			        this.usrAutoriza1 = con.cliente_7To8(this.usrAutoriza1);
			    if (!this.usrAutoriza2.equals(""))
			        this.usrAutoriza2 = con.cliente_7To8(this.usrAutoriza2);
			    if (!this.usrAutoriza3.equals(""))
			        this.usrAutoriza3 = con.cliente_7To8(this.usrAutoriza3);
			    this.estatus = tokenizador.nextToken ();
			}else if(op.equals("opTMB")){
				this.fch_registro = tokenizador.nextToken();
				this.folio_registro = tokenizador.nextToken();
				this.tipo_operacion = tokenizador.nextToken();
				this.cta_cargo = tokenizador.nextToken();
				this.descrip_cargo = tokenizador.nextToken();
				this.cta_abono = tokenizador.nextToken();
				this.descrip_abono = tokenizador.nextToken();
				this.importe = tokenizador.nextToken();
				this.fch_aplicacion = tokenizador.nextToken();
				this.estatus = tokenizador.nextToken();
			}else if (op.equals("opTI")) {
				this.fch_registro = tokenizador.nextToken();
				this.folio_registro = tokenizador.nextToken();
				this.cta_cargo = tokenizador.nextToken();
				this.beneficiario = tokenizador.nextToken();
				this.banco = tokenizador.nextToken();
				this.importe = tokenizador.nextToken();
				this.concepto = tokenizador.nextToken();
				this.ref_interbancaria = tokenizador.nextToken();
				if(tipo_operacion.trim().equals("DIBT"))
				{
				  String instruccion = tokenizador.nextToken();
				  this.forma_aplicacion=(instruccion==null || instruccion.trim().equals(""))?"H":instruccion;
				  this.forma_aplicacion=(this.forma_aplicacion.equals("2") || this.forma_aplicacion.equals("H"))?this.forma_aplicacion:"H";
				}
				this.estatus = tokenizador.nextToken();
			}
		} catch (Exception ex) {
		}
    }

    /** Metodo para formatear el bean en html
     * @param index Indice del registro
     * @return String con formato en html para desplegar la consulta
     */
    public String getHTMLFormat (int index, DatosMancSA beanDatos) {
		String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
		StringBuffer row = new StringBuffer();
		String estatusDesc = getEstatus(beanDatos.getEstatus());
		String tipoOper = "";
		String importeFormat = "";
		NumberFormat formatoMoneda = NumberFormat.getCurrencyInstance (Locale.US);

		if(beanDatos.getFch_registro()==null || beanDatos.getFch_registro().equals(""))
			beanDatos.setFch_registro("&nbsp;");
		if(beanDatos.getTipo_operacion()==null || beanDatos.getTipo_operacion().equals(""))
			tipoOper="&nbsp;";
		else
			tipoOper=DatosManc.Description(beanDatos.getTipo_operacion());
		if(beanDatos.getFch_autorizacion()==null || beanDatos.getFch_autorizacion().equals(""))
			beanDatos.setFch_autorizacion("&nbsp;");
		if(beanDatos.getFolio_archivo()==null || beanDatos.getFolio_archivo().equals(""))
			beanDatos.setFolio_archivo("&nbsp;");
		if(formatoMoneda.format(Double.parseDouble(beanDatos.getImporte())).equals("") ||
				formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()))==null)
			importeFormat="&nbsp;";
		else{
			importeFormat=formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()));
			if (importeFormat.substring(0,3).equals("MXN"))
				importeFormat = "$ " + importeFormat.substring(3, importeFormat.length());
		}
		if(beanDatos.getUsrRegistro()==null || beanDatos.getUsrRegistro().equals(""))
			beanDatos.setUsrRegistro("&nbsp;");

		if(!beanDatos.getUsrRegistro().equals("&nbsp;"))
			beanDatos.setUsrRegistro(con.cliente_7To8(beanDatos.getUsrRegistro().trim()));

		if(beanDatos.getUsrAutoriza1()==null || beanDatos.getUsrAutoriza1().equals(""))
			beanDatos.setUsrAutoriza1("&nbsp;");
		if("X".equals(beanDatos.getUsrAutoriza1().trim())) {
			beanDatos.setUsrAutoriza1("N/A");
		}
		if(!beanDatos.getUsrAutoriza1().trim().equals("&nbsp;") && !beanDatos.getUsrAutoriza1().trim().equals("N/A")){
			beanDatos.setUsrAutoriza1(con.cliente_7To8(beanDatos.getUsrAutoriza1().trim()));
		}
		if(beanDatos.getUsrAutoriza2()==null || beanDatos.getUsrAutoriza2().equals(""))
			beanDatos.setUsrAutoriza2("&nbsp;");
		if("X".equals(beanDatos.getUsrAutoriza2().trim())) {
			beanDatos.setUsrAutoriza2("N/A");
		}
		if(!beanDatos.getUsrAutoriza2().trim().equals("&nbsp;") && !beanDatos.getUsrAutoriza2().trim().equals("N/A")){
			beanDatos.setUsrAutoriza2(con.cliente_7To8(beanDatos.getUsrAutoriza2().trim()));
		}
		if(beanDatos.getUsrAutoriza3()==null || beanDatos.getUsrAutoriza3().equals(""))
			beanDatos.setUsrAutoriza3("&nbsp;");
		if("X".equals(beanDatos.getUsrAutoriza3().trim())) {
			beanDatos.setUsrAutoriza3("N/A");
		}
		if(!beanDatos.getUsrAutoriza3().trim().equals("&nbsp;") && !beanDatos.getUsrAutoriza3().trim().equals("N/A")){
			beanDatos.setUsrAutoriza3(con.cliente_7To8(beanDatos.getUsrAutoriza3().trim()));
		}
		if(estatusDesc==null || estatusDesc.equals(""))
			estatusDesc="&nbsp;";


		row.append("<tr>\n")
		.append("  <td align='right' class='").append(color).append("'>")
		.append("<input type='radio' name='indiceArchivo' value='").append(index).append("'></td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFch_registro()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(tipoOper).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFch_autorizacion()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFolio_archivo()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getRegistros()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(importeFormat).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getUsrRegistro()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getUsrAutoriza1()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getUsrAutoriza2()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getUsrAutoriza3()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(estatusDesc).append("</td>\n")
		.append("</tr>\n");

		return row.toString();
    }

    /** Metodo para formatear el bean en html
     * @param index Indice del registro
     * @return String con formato en html para desplegar la consulta
     */
    public String getHTMLFormatTMB (int index, DatosMancSA beanDatos) {
		String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
		StringBuffer row = new StringBuffer();
		String estatusDesc = getEstatus(beanDatos.getEstatus());
		String tipoOper = "";
		String usrRegistro = "";
		String usrAut1 = "";
		String usrAut2 = "";
		String usrAut3 = "";
		String importeFormat = "";
		NumberFormat formatoMoneda = NumberFormat.getCurrencyInstance (Locale.US);

		if(beanDatos.getFch_registro()==null || beanDatos.getFch_registro().equals(""))
			beanDatos.setFch_registro("&nbsp;");
		if(beanDatos.getFolio_registro()==null || beanDatos.getFolio_registro().equals(""))
			beanDatos.setFolio_registro("&nbsp;");
		if(beanDatos.getTipo_operacion()==null || beanDatos.getTipo_operacion().equals(""))
			tipoOper="&nbsp;";
		else
			tipoOper=DatosManc.Description(beanDatos.getTipo_operacion());
		if(beanDatos.getCta_cargo()==null || beanDatos.getCta_cargo().equals(""))
			beanDatos.setCta_cargo("&nbsp;");
		if(beanDatos.getDescrip_cargo()==null || beanDatos.getDescrip_cargo().equals(""))
			beanDatos.setDescrip_cargo("&nbsp;");
		if(beanDatos.getCta_abono()==null || beanDatos.getCta_cargo().equals(""))
			beanDatos.setCta_abono("&nbsp;");
		if(beanDatos.getDescrip_abono()==null || beanDatos.getDescrip_abono().equals(""))
			beanDatos.setDescrip_abono("&nbsp;");
		if(formatoMoneda.format(Double.parseDouble(beanDatos.getImporte())).equals("") ||
				formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()))==null)
			importeFormat="&nbsp;";
		else{
			importeFormat=formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()));
			if (importeFormat.substring(0,3).equals("MXN"))
				importeFormat = "$ " + importeFormat.substring(3, importeFormat.length());
		}
		if(beanDatos.getFch_aplicacion()==null || beanDatos.getFch_aplicacion().equals(""))
			beanDatos.setFch_aplicacion("&nbsp;");
		//Linea para operaciones programadas OIVI
		if(beanDatos.getFch_eje_prog()==null || beanDatos.getFch_eje_prog().equals(""))
			beanDatos.setFch_eje_prog("&nbsp;");
			//FIN
		if(beanDatos.getUsrRegistro()==null || beanDatos.getUsrRegistro().equals(""))
			usrRegistro = "&nbsp;";
		else
			usrRegistro = con.cliente_7To8(beanDatos.getUsrRegistro().trim());
		if(beanDatos.getUsrAutoriza1()==null || beanDatos.getUsrAutoriza1().equals("") || beanDatos.getUsrAutoriza1().equals("0000000"))
			usrAut1 = "&nbsp;";
		else
			usrAut1 = con.cliente_7To8(beanDatos.getUsrAutoriza1().trim());
		if(beanDatos.getUsrAutoriza2()==null || beanDatos.getUsrAutoriza2().equals("")|| beanDatos.getUsrAutoriza2().equals("0000000"))
			usrAut2 = "&nbsp;";
		else
			usrAut2 = con.cliente_7To8(beanDatos.getUsrAutoriza2().trim());
		if(beanDatos.getUsrAutoriza3()==null || beanDatos.getUsrAutoriza3().equals("") || beanDatos.getUsrAutoriza3().equals("0000000"))
			usrAut3 = "&nbsp;";
		else
			usrAut3 = con.cliente_7To8(beanDatos.getUsrAutoriza3().trim());


		if(estatusDesc==null || estatusDesc.equals(""))
			estatusDesc="&nbsp;";


		row.append("<tr>\n")
		.append("  <td align='right' class='").append(color).append("'>");
		if( beanDatos.getEstatus().equals("M") || beanDatos.getEstatus().equals("V") || beanDatos.getEstatus().equals("U")||
				beanDatos.getEstatus().equals("P") )
			row = row.append("<input type='checkbox' name='indiceArchivo' onclick='javascript:document.FrmAutoriza.todos.checked = false' value='").append(index).append("'>");
		else
			row = row.append("&nbsp;");
		row = row.append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFch_registro()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFolio_registro()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(tipoOper).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_cargo()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getDescrip_cargo()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_abono()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getDescrip_abono()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(importeFormat).append("</td>\n");
				//Linea para poner fecha a operaciones programadas IOV
		if(beanDatos.getTipo_operacion().equals("PROG"))
			row = row.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFch_eje_prog()).append("</td>\n");
		else
   			row = row.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFch_registro()).append("</td>\n");
		//se cambio la fecha de autorizacion por fecha de registro en campo fecha aplicacion
		//beanDatos.getFch_aplicacion() --> beanDatos.getFch_registro()
		//Fin;
		row = row.append("  <td align='center' class='").append(color).append("'>").append(usrRegistro).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(usrAut1).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(usrAut2).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(usrAut3).append("</td>\n")

		.append("  <td align='center' class='").append(color).append("'>").append(estatusDesc).append("</td>\n")
		.append("</tr>\n");

		return row.toString();
    }

    /** Metodo para formatear el bean en html
     * @param index Indice del registro
     * @return String con formato en html para desplegar la consulta
     */
    public String getHTMLFormatTI (int index, DatosMancSA beanDatos) {
		String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
		StringBuffer row = new StringBuffer();
		String estatusDesc = getEstatus(beanDatos.getEstatus());
		String importeFormat = "";
		String concepto = "";
		String refInter = "";
		String usrRegistro = "";
		String usrAut1 = "";
		String usrAut2 = "";
		String usrAut3 = "";

		String formaAplica = getFormaAplica(beanDatos.getForma_aplicacion());
		NumberFormat formatoMoneda = NumberFormat.getCurrencyInstance (Locale.US);

		if(beanDatos.getFch_registro()==null || beanDatos.getFch_registro().equals(""))
			beanDatos.setFch_registro("&nbsp;");
		if(beanDatos.getFolio_registro()==null || beanDatos.getFolio_registro().equals(""))
			beanDatos.setFolio_registro("&nbsp;");
		if(beanDatos.getCta_cargo()==null || beanDatos.getCta_cargo().equals(""))
			beanDatos.setCta_cargo("&nbsp;");
		if(beanDatos.getCta_abono()==null || beanDatos.getCta_abono().equals(""))
			beanDatos.setCta_abono("&nbsp;");
		if(beanDatos.getBeneficiario()==null || beanDatos.getBeneficiario().equals(""))
			beanDatos.setBeneficiario("&nbsp;");
		if(formatoMoneda.format(Double.parseDouble(beanDatos.getImporte())).equals("") ||
				formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()))==null)
			importeFormat="&nbsp;";
		else{
			importeFormat=formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()));
			if (importeFormat.substring(0,3).equals("MXN"))
				importeFormat = "$ " + importeFormat.substring(3, importeFormat.length());
		}
		if(beanDatos.getConcepto()==null || !beanDatos.getConcepto().contains("@"))
			concepto = "&nbsp;";
		else {
			concepto = beanDatos.getConcepto();
			concepto = concepto.substring(concepto.indexOf("@")+1);
		}
		if(beanDatos.getRef_interbancaria()==null || !beanDatos.getRef_interbancaria().contains("@"))
			refInter = "&nbsp;";
		else {
			refInter = beanDatos.getRef_interbancaria();
			refInter = refInter.substring(0,refInter.indexOf("@"));
		}
		if(beanDatos.getForma_aplicacion()==null || beanDatos.getForma_aplicacion().equals(""))
			formaAplica = "&nbsp;";
		if(beanDatos.getUsrRegistro()==null || beanDatos.getUsrRegistro().equals(""))
			usrRegistro = "&nbsp;";
		else
			usrRegistro = con.cliente_7To8(beanDatos.getUsrRegistro().trim());
		if(beanDatos.getUsrAutoriza1()==null || beanDatos.getUsrAutoriza1().equals("") || beanDatos.getUsrAutoriza1().equals("0000000"))
			usrAut1 = "&nbsp;";
		else
			usrAut1 = con.cliente_7To8(beanDatos.getUsrAutoriza1().trim());
		if(beanDatos.getUsrAutoriza2()==null || beanDatos.getUsrAutoriza2().equals("")|| beanDatos.getUsrAutoriza2().equals("0000000"))
			usrAut2 = "&nbsp;";
		else
			usrAut2 = con.cliente_7To8(beanDatos.getUsrAutoriza2().trim());
		if(beanDatos.getUsrAutoriza3()==null || beanDatos.getUsrAutoriza3().equals("") || beanDatos.getUsrAutoriza3().equals("0000000"))
			usrAut3 = "&nbsp;";
		else
			usrAut3 = con.cliente_7To8(beanDatos.getUsrAutoriza3().trim());
		if(beanDatos.getEstatus()==null || beanDatos.getEstatus().equals(""))
			estatusDesc ="&nbsp;";

		row.append("<tr>\n")
		.append("  <td align='right' class='").append(color).append("'>");
		if(beanDatos.getEstatus().equals("M") || beanDatos.getEstatus().equals("V") || beanDatos.getEstatus().equals("U") || beanDatos.getEstatus().equals("P"))
			row = row.append("<input type='checkbox' name='indiceArchivo' onclick='javascript:document.FrmAutoriza.todos.checked = false' value='").append(index).append("'>");
		else
			row = row.append("&nbsp;");
			String nvoConcepto[] = concepto.split("@");
			row = row.append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFch_registro()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getFolio_registro()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_cargo()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_abono()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getBeneficiario()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getBanco()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(importeFormat).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(nvoConcepto[0]).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(refInter).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(formaAplica).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(usrRegistro).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(usrAut1).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(usrAut2).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(usrAut3).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(estatusDesc).append("</td>\n")
		.append("</tr>\n");



		return row.toString();
    }



    /** Metodo para formatear el bean en html de nominas Interbancaria
     *  Modificacion Mancomunidad Fase II LFER
     * @param index Indice del registro
     * @return String con formato en html para desplegar la consulta
     */
    public String getHTMLFormatNomiInte(int index, DatosMancSA beanDatos) {
		String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
		StringBuffer row = new StringBuffer();
		String importeFormat = "";
		String tipoCuenta = getTipoCuenta(beanDatos.getTipo_origen());

		NumberFormat formatoMoneda = NumberFormat.getCurrencyInstance (Locale.US);

		if(beanDatos.getEstatus()==null || beanDatos.getEstatus().equals("")){
			beanDatos.setEstatus("&nbsp;");
		}
		if(beanDatos.getCta_cargo()==null || beanDatos.getCta_cargo().equals("")){
			beanDatos.setCta_cargo("&nbsp;");
		}

		if(beanDatos.getBeneficiario()==null || beanDatos.getBeneficiario().equals("")){
			beanDatos.setBeneficiario("&nbsp;");
		}
		if(tipoCuenta==null || tipoCuenta.equals("")){
			tipoCuenta = "&nbsp;";
		}
		if(beanDatos.getCta_abono().trim().length()<=10){
			tipoCuenta = "Cuenta M&oacute;vil";
		}
		if(beanDatos.getCta_abono().trim().length()==18){
			tipoCuenta = "Clabe";
		}
		if(beanDatos.getCta_abono().trim().length()==16){
			tipoCuenta = "Tarjeta de D&eacute;bito";
		}
		if(beanDatos.getCta_abono().trim().length()==11){
			tipoCuenta = "Cuenta de Cheques";
		}
		if(beanDatos.getCta_abono()==null || beanDatos.getCta_abono().equals("")){
			beanDatos.setCta_abono("&nbsp;");
		}
		if(formatoMoneda.format(Double.parseDouble(beanDatos.getImporte())).equals("") ||
				formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()))==null)
			importeFormat="&nbsp;";
		else{
			importeFormat=formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()));
			if (importeFormat.substring(0,3).equals("MXN"))
				importeFormat = "$ " + importeFormat.substring(3, importeFormat.length());
		}
		if(beanDatos.getBanco()==null || beanDatos.getBanco().equals("")){
			beanDatos.setBanco("&nbsp;");
		}
		if(beanDatos.getPlaza()==null || beanDatos.getPlaza().equals("")){
			beanDatos.setPlaza("&nbsp;");
		}
		row.append("<tr>\n").append("<td>");
		if(beanDatos.getEstatus().equals("M") || beanDatos.getEstatus().equals("V") || beanDatos.getEstatus().equals("U")) {
			 row = row.append("<input type='checkbox' style = 'visibility:hidden' name='indiceArchivo'  value='").append(index).append("'>");
		}
		else {
			row = row.append("&nbsp;");
		}
		row = row.append("</td>")
		.append("\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_cargo()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getBeneficiario()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(tipoCuenta).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_abono()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(importeFormat).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getBanco()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getPlaza()).append("</td>\n")
		.append("</tr>\n");



		return row.toString();
    }

    /** Metodo para formatear el bean en html de nominas Tradicional e inmediata
     *  Modificacion Mancomunidad Fase II IOV
     * @param index Indice del registro
     * @return String con formato en html para desplegar la consulta
     */
    public String getHTMLFormatNomi(int index, DatosMancSA beanDatos) {
		String color = (index % 2) != 0 ? COLOR_CLARO : COLOR_OBSCURO;
		StringBuffer row = new StringBuffer();
		String importeFormat = "";
		String apellidoPat = "";
		String apellidoMat = "";
		String nomEmpleado = "";
		NumberFormat formatoMoneda = NumberFormat.getCurrencyInstance (Locale.US);

		pdTokenizer tokenizador = new pdTokenizer (beanDatos.getBeneficiario(), "@", 2);

		try{
			nombreEmp = tokenizador.nextToken ();
    		aPaterno = tokenizador.nextToken ();
        	aMaterno = tokenizador.nextToken ();
    	} catch (Exception ex) {}

    	apellidoPat = aPaterno;
    	apellidoMat = aMaterno;
    	nomEmpleado = nombreEmp;

		if(beanDatos.getEstatus()==null || beanDatos.getEstatus().equals("")){
			beanDatos.setEstatus("&nbsp;");
		}
		if(beanDatos.getCta_cargo()==null || beanDatos.getCta_cargo().equals("")){
			beanDatos.setCta_cargo("&nbsp;");
		}
		if(beanDatos.getTitular()==null || beanDatos.getTitular().equals("")){
			beanDatos.setTitular("&nbsp;");
		}
		//Formato para el nombre
		if(apellidoPat==null || apellidoPat.equals("")){
			apellidoPat = "&nbsp;";
		}
		if(apellidoMat==null || apellidoMat.equals("")){
			apellidoMat = "&nbsp;";
		}
		if(nomEmpleado==null || nomEmpleado.equals("")){
			nomEmpleado = "&nbsp;";
		}
		if(beanDatos.getCta_abono()==null || beanDatos.getCta_abono().equals("")){
			beanDatos.setCta_abono("&nbsp;");
		}
		if(formatoMoneda.format(Double.parseDouble(beanDatos.getImporte())).equals("") ||
				formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()))==null)
			importeFormat="&nbsp;";
		else{
			importeFormat=formatoMoneda.format(Double.parseDouble(beanDatos.getImporte()));
			if (importeFormat.substring(0,3).equals("MXN"))
				importeFormat = "$ " + importeFormat.substring(3, importeFormat.length());
		}

		row.append("<tr>\n").append("<td>");
		if(beanDatos.getEstatus().equals("M") || beanDatos.getEstatus().equals("V") || beanDatos.getEstatus().equals("U")) {
			row = row.append("<input type='checkbox' style = 'visibility:hidden' name='indiceArchivo'  value='").append(index).append("'>");
		}
		else {
			row = row.append("&nbsp;");
		}
		row = row.append("</td>")
		.append("\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_cargo()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getTitular()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(apellidoPat).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(apellidoMat).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(nomEmpleado).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(beanDatos.getCta_abono()).append("</td>\n")
		.append("  <td align='center' class='").append(color).append("'>").append(importeFormat).append("</td>\n")
		.append("</tr>\n");


		return row.toString();
    }



    /** Metodo privado para obtener el estatus completo
     * @param String con la letra que define el estatus
     * @return String con el estatus entendible
     */
    private String getEstatus (String estatus) {
	if (estatus.trim ().equals ("P")) {
	    return "Pendiente";
	} else if (estatus.trim ().equals ("A")) {
	    return "Autorizada";
	} else if (estatus.trim ().equals ("R")) {
	    return "Rechazada";
	} else if (estatus.trim ().equals ("C")) {
	    return "Cancelada";
	} else if (estatus.trim ().equals ("E")) {
	    return "Ejecutada";
	} else if (estatus.trim ().equals ("N")) {
	    return "No Ejecutada";
	} else if (estatus.trim ().equals ("M")) {
	    return "Mancomunada";
	} else if (estatus.trim ().equals ("V")) {
	    return "Verificada";
	} else if (estatus.trim ().equals ("U")) {
	    return "Pre-Autorizada";
	} else {
	    return "";
	}
    }

    /** Metodo privado para obetener el tipo de cuenta para nomina Interbancaria
     * @param String con la letra que define el estatus
     * @return String con el estatus entendible
     */

    private String getTipoCuenta(String tipoCuenta) {
    	if (tipoCuenta.trim().equals("01")) {
    		return "Cuenta de Cheques";
    	} else if (tipoCuenta.trim().equals("02")) {
    		return "Tarjeta de D&eacute;bito";
    	} else if (tipoCuenta.trim().equals("40")) {
    		return "Clabe";
    	} else {
    		return "";
    	}
    }

    private String getFormaAplica (String formaAplica) {
    	if (formaAplica.trim ().equals ("H")) {
    	    return "Mismo d&iacute;a";
    	} else if (formaAplica.trim ().equals ("2")) {
    	    return "D&iacute;a siguiente";
    	} else {
    	    return "";
    	}
    }

	/**
	 * Asigna los datos de RFC e IVA para una transaccion SPID.
	 * @param concepto trama de datos
	 */
	private void asignaDatosSPID( String concepto ) {
		/*SPID FII*/
		String tramaSPID[] = concepto.split("@");

		if (tramaSPID != null && tramaSPID.length > 1) {
			this.divisaSpid = tramaSPID[1];
			if (tramaSPID[0].contains("RFC ") && tramaSPID[0].contains(" IVA ")) {
				this.rfcSpid = tramaSPID[0].substring(tramaSPID[0].lastIndexOf("RFC "),
						tramaSPID[0].lastIndexOf(" IVA ")).replace("RFC", "").trim();
			}
		}
	}

    /** Metodo para verificar si la transacci&oacute;n esta pendiente
     * @return true Si esta pendiente de lo contrario false
     */
    public boolean pendiente () {
    	return ("P".equals(estatus) || "M".equals(estatus) || "U".equals(estatus) || "V".equals(estatus));
    }

    /** Metodo para generar la trama de autorizaci&oacute;n
     * @param usuario String con el usuario que va a autorizar la transacci&oacute;n
     * @param contrato String con el contrato del usuario
     * @param clavePerfil String con la clave del perfil del usuario
     * @return String con la trama de autorizaci&oacute;n
     */
    public String getTramaAutorizacion (String usuario, String contrato, String clavePerfil) {

		cta_abono = cta_abono.trim();
		cta_cargo = cta_cargo.trim();


		String fch_prog_trama="";
		if(tipo_operacion.equals ("PROG")) {
		    fch_prog_trama = fch_eje_prog.substring(3,5) + fch_eje_prog.substring(0,2) + fch_eje_prog.substring(8,10);
		    System.out.println("fch_prog_trama: " + fch_prog_trama);
	    } else {
		    fch_prog_trama = fch_eje_prog;
		}

		String trama = "1EWEB|" + usuario;


		if (tipo_operacion.equals ("PROG")) {
			trama += "|PROG|" + contrato + '|' + usuario + '|' + clavePerfil + '|' + tipo_oper_prog + '|' + fch_prog_trama + '|' + cta_cargo + '|' + tipo_origen;
		    if (tipo_oper_prog.equals ("TRAN") || tipo_oper_prog.equals ("PAGT")) {
		    	trama += '|' + cta_cargo + '|' + tipo_destino + '|' + importe + '|' + "|0" + (concepto.equals ("") ? "||A|" : '|' + concepto + '|') + folio_registro + '|';
		    	return trama;
		    }
		}
		else {
			trama += '|' + tipo_operacion + '|' + contrato + '|' + usuario + '|' + clavePerfil + '|' + cta_cargo + '|' + tipo_origen + '|';
			if (tipo_operacion.equals ("TRAN") || tipo_operacion.equals ("PAGT")) {
				trama += cta_abono + '|' + tipo_destino + '|' + importe + '|' + (concepto.equals ("") ? "||A|" : '|' + concepto + '|') + folio_registro + '|';
				return trama;
		    }
		    else if (tipo_operacion.equals ("DIBT")) {
				String dirIp = "";
				if(concepto==null || concepto.trim().equals(" ")) {
					  concepto=" @   ";
				}
				else if(concepto.indexOf("@") != -1) {
					referencia=concepto.substring(0,concepto.indexOf("@"));
					concepto=concepto.substring(concepto.indexOf("@")+1);
					asignaDatosSPID(concepto); /*SPID FII*/
				}
				
				/*SPID FII*/
				try {
					InetAddress direccion = InetAddress.getLocalHost();
					dirIp = direccion.getHostAddress();
				} catch (SecurityException sex) {
					EIGlobal.mensajePorTrace("Error retreiving host - " + sex.getMessage(), EIGlobal.NivelLog.ERROR);
				} catch( UnknownHostException uex ) {
					EIGlobal.mensajePorTrace("Host desconocido - " + uex.getMessage(), EIGlobal.NivelLog.ERROR);
				}
				
				referencia=(referencia.trim().equals(""))?" ":referencia;
				concepto=(concepto.trim().equals(""))?"   ":concepto;

		        plaza=(plaza==null || plaza.trim().equals(""))?" ":plaza;
		        sucursal=(sucursal==null || sucursal.trim().equals(""))?"0":sucursal;
				trama += titular + '|' + cta_abono + '|' + banco + '|' + beneficiario + '|' + sucursal + '|' + importe + '|' +  plaza + '|' + referencia_medio + '|' + concepto + '|' + referencia + "|" + folio_registro +'|';	/*MSD 11/2005 Proyecto Ley de Transparencia*/
				
				/*SPID FII*/
				trama += forma_aplicacion
                        + "|" + (this.divisaSpid == null ? "" : divisaSpid)
                        + "|" + (this.rfcSpid == null ? " " : rfcSpid)
                        + "|" + dirIp + "|";

				System.out.println("ESTA ES LA TRAMA QUE LLEGA EL MANCSRVR -->" +trama);
				return trama;
		    }
		    else {
		    	return "";
		    }
		}
		return "";
    }

    /** Metodo para generar la trama de cancelaci&oacute;n
     * @param usuario String con el usuario que va a autorizar la transacci&oacute;n
     * @param contrato String con el contrato del usuario
     * @param clavePerfil String con la clave del perfil del usuario
     * @return String con la trama de autorizaci&oacute;n
     */
    public String getTramaCancelacion (String usuario, String contrato,
    String clavePerfil) {

	String fch_registro_trama="";

	fch_registro_trama =	fch_registro.substring(0,2) + fch_registro.substring(3,5) + fch_registro.substring(6,10);

	return "1EWEB|" + usuario + "|CAMA|" + contrato + '|' + usuario + '|' + clavePerfil + '|' + fch_registro_trama + '@' + folio_registro +'@';
    }



	public String getFch_registro() {
		return fch_registro;
	}

	public void setFch_registro(String fch_registro) {
		this.fch_registro = fch_registro;
	}

	public String getTipo_operacion() {
		return tipo_operacion;
	}

	public void setTipo_operacion(String tipo_operacion) {
		this.tipo_operacion = tipo_operacion;
	}

	public String getFch_autorizacion() {
		return fch_autorizacion;
	}

	public void setFch_autorizacion(String fch_autorizacion) {
		this.fch_autorizacion = fch_autorizacion;
	}

	public String getFolio_archivo() {
		return folio_archivo;
	}

	public void setFolio_archivo(String folio_archivo) {
		this.folio_archivo = folio_archivo;
	}

	public String getRegistros() {
		return registros;
	}

	public void setRegistros(String registros) {
		this.registros = registros;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getUsrRegistro() {
		return usrRegistro;
	}

	public void setUsrRegistro(String usrRegistro) {
		this.usrRegistro = usrRegistro;
	}

	public String getUsrAutoriza1() {
		return usrAutoriza1;
	}

	public void setUsrAutoriza1(String usrAutoriza1) {
		this.usrAutoriza1 = usrAutoriza1;
	}

	public String getUsrAutoriza2() {
		return usrAutoriza2;
	}

	public void setUsrAutoriza2(String usrAutoriza2) {
		this.usrAutoriza2 = usrAutoriza2;
	}

	public String getUsrAutoriza3() {
		return usrAutoriza3;
	}

	public void setUsrAutoriza3(String usrAutoriza3) {
		this.usrAutoriza3 = usrAutoriza3;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFolio_registro() {
		return folio_registro;
	}

	public void setFolio_registro(String folio_registro) {
		this.folio_registro = folio_registro;
	}

	public String getCta_cargo() {
		return cta_cargo;
	}

	public void setCta_cargo(String cta_cargo) {
		this.cta_cargo = cta_cargo;
	}

	public String getDescrip_cargo() {
		return descrip_cargo;
	}

	public void setDescrip_cargo(String descrip_cargo) {
		this.descrip_cargo = descrip_cargo;
	}

	public String getCta_abono() {
		return cta_abono;
	}

	public void setCta_abono(String cta_abono) {
		this.cta_abono = cta_abono;
	}

	public String getDescrip_abono() {
		return descrip_abono;
	}

	public void setDescrip_abono(String descrip_abono) {
		this.descrip_abono = descrip_abono;
	}

	public String getFch_aplicacion() {
		return fch_aplicacion;
	}

	public void setFch_aplicacion(String fch_aplicacion) {
		this.fch_aplicacion = fch_aplicacion;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getRef_interbancaria() {
		return ref_interbancaria;
	}

	public void setRef_interbancaria(String ref_interbancaria) {
		this.ref_interbancaria = ref_interbancaria;
	}

	public String getForma_aplicacion() {
		return forma_aplicacion;
	}

	public void setForma_aplicacion(String forma_aplicacion) {
		this.forma_aplicacion = forma_aplicacion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFch_eje_prog() {
		return fch_eje_prog;
	}

	public void setFch_eje_prog(String fch_eje_prog) {
		this.fch_eje_prog = fch_eje_prog;
	}

	public String getTipo_origen() {
		return tipo_origen;
	}

	public void setTipo_origen(String tipo_origen) {
		this.tipo_origen = tipo_origen;
	}

	public String getTipo_oper_prog() {
		return tipo_oper_prog;
	}

	public void setTipo_oper_prog(String tipo_oper_prog) {
		this.tipo_oper_prog = tipo_oper_prog;
	}

	public String getTipo_destino() {
		return tipo_destino;
	}

	public void setTipo_destino(String tipo_destino) {
		this.tipo_destino = tipo_destino;
	}

	public String getPlaza() {
		return plaza;
	}

	public void setPlaza(String plaza) {
		this.plaza = plaza;
	}

	public String getReferencia_medio() {
		return referencia_medio;
	}

	public void setReferencia_medio(String referencia_medio) {
		this.referencia_medio = referencia_medio;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String toString () {
		StringBuffer sb = new StringBuffer();
		return 	sb.append(fch_registro.trim ()).append(';')
    	.append(tipo_operacion.trim ()).append(';')
    	.append(fch_autorizacion.trim ()).append(';')
    	.append(folio_archivo.trim ()).append(';')
		.append(registros.trim ()).append(';')
		.append(importe.trim ()).append(';')
		.append(usrRegistro.trim ()).append(';')
		.append(usrAutoriza1.trim ()).append(';')
		.append(usrAutoriza2.trim ()).append(';')
		.append(usrAutoriza3.trim ()).append(';')
		.append(estatus.trim ()).append(";\r\n")
		.toString();
    }

	public String toTMBString () {
		StringBuffer sb = new StringBuffer();
		return 	sb.append(fch_registro.trim ()).append(';')
    	.append(folio_registro.trim ()).append(';')
    	.append(tipo_operacion.trim ()).append(';')
    	.append(cta_cargo.trim ()).append(';')
		.append(descrip_cargo.trim ()).append(';')
		.append(cta_abono.trim ()).append(';')
		.append(descrip_abono.trim ()).append(';')
		.append(importe.trim ()).append(';')
		.append(fch_aplicacion.trim ()).append(';')
		.append(usrRegistro.trim ()).append(';')
		.append(usrAutoriza1.trim ()).append(';')
		.append(usrAutoriza2.trim ()).append(';')
		.append(usrAutoriza3.trim ()).append(';')
		.append(estatus.trim ()).append(";\r\n")
		.toString();
    }

	public String toTIString () {
		StringBuffer sb = new StringBuffer();
		return 	sb.append(fch_registro.trim ()).append(';')
    	.append(folio_registro.trim ()).append(';')
    	.append(cta_cargo.trim ()).append(';')
    	.append(cta_abono.trim ()).append(';')
    	.append(beneficiario.trim ()).append(';')
		.append(banco.trim ()).append(';')
		.append(importe.trim ()).append(';')
		.append(concepto.trim ()).append(';')
		.append(ref_interbancaria.trim ()).append(';')
		.append(forma_aplicacion.trim ()).append(';')
		.append(usrRegistro.trim ()).append(';')
		.append(usrAutoriza1.trim ()).append(';')
		.append(usrAutoriza2.trim ()).append(';')
		.append(usrAutoriza3.trim ()).append(';')
		.append(estatus.trim ()).append(";\r\n")
		.toString();
    }
	/**
	 * Modificacion Mancomunidad Fase II LFER
	 * @return
	 */
	public String toNomiInteString () {
		StringBuffer sb = new StringBuffer();
		return 	sb.append(cta_cargo.trim ()).append(';')
    	.append(beneficiario.trim ()).append(';')
    	.append(tipo_origen.trim ()).append(';')
    	.append(cta_abono.trim ()).append(';')
    	.append(importe.trim ()).append(';')
    	.append(banco.trim ()).append(';')
    	.append(plaza.trim ()).append(';')
		.append(estatus.trim ()).append(";\r\n")
		.toString();
    }

	/**
	 * Modificacion Mancomunidad Fase II IOV
	 * @return
	 */
	public String toNomiString () {
		StringBuffer sb = new StringBuffer();

		pdTokenizer tokenizador = new pdTokenizer (beneficiario, "@", 2);

		try{
			nombreEmp = tokenizador.nextToken ();
    		aPaterno = tokenizador.nextToken ();
        	aMaterno = tokenizador.nextToken ();
    	} catch (Exception ex) {}

		return 	sb.append(cta_cargo.trim ()).append(';')
		.append(aPaterno.trim ()).append(';')
		.append(aMaterno.trim ()).append(';')
    	.append(nombreEmp.trim ()).append(';')
        .append(titular.trim()).append(';')
    	.append(cta_abono.trim ()).append(';')
    	.append(importe.trim ()).append(';')
		.append(estatus.trim ()).append(";\r\n")
		.toString();
    }

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * Obtiene al Divisa para SPID.
	 * @return la divisa para SPID
	 */
	public String getDivisaSpid() {
		return this.divisaSpid;
	}

	/**
	 * Asigna la Divisa para SPID.
	 * @param divisaSpid nueva divisa para SPID
	 */
	public void setDivisaSpid(String divisaSpid) {
		this.divisaSpid = divisaSpid;
	}

	/**
	 * Obtiene el RFC para SPID.
	 * @return el RFC para SPID
	 */
	public String getRfcSpid() {
		return this.rfcSpid;
	}

	/**
	 * Asigna el RFC para SPID.
	 * @param rfcSpid nueva divisa para SPID
	 */
	public void setRfcSpid(String rfcSpid) {
		this.rfcSpid = rfcSpid;
	}
}