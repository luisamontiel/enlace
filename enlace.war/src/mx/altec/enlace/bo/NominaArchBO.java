package mx.altec.enlace.bo;

import mx.altec.enlace.beans.NominaArchBean;
import mx.altec.enlace.dao.NominaArchDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class NominaArchBO {
	
	/**
	 * Inserta datos del archivo de Nomina
	 * @param bean contiene los datos a registrar
	 * @return insert bandera de ejecucion de la operacion
	 */
	public boolean registraNominaArch(NominaArchBean bean){
		/**
		 * insert bandera de ejecucion
		 */
		EIGlobal.mensajePorTrace("NominaArchBO.java::inicio registraNominaArch", EIGlobal.NivelLog.DEBUG);
		boolean insert=false;
		
		NominaArchDAO dao=new NominaArchDAO();
		insert=dao.registraNominaArch(bean);
		EIGlobal.mensajePorTrace("NominaArchBO.java::fin registraNominaArch :: " + insert, EIGlobal.NivelLog.DEBUG);
		return insert;
	}

}