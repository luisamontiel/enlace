package mx.altec.enlace.bo;

import java.util.ArrayList;
import java.util.Calendar;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import mx.altec.enlace.beans.AdmUsrGL35;
import mx.altec.enlace.beans.AdmUsrGL37;
import mx.altec.enlace.gwt.adminusr.shared.PerfilPrototipo;
import mx.altec.enlace.gwt.adminusr.shared.GrupoPerfiles;
import mx.altec.enlace.gwt.adminusr.shared.Facultad;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.dao.FacPerfilDao;
import mx.altec.enlace.dao.AdmUsrGruposDAO;
import mx.altec.enlace.dao.AdmUsrPerfilesDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class InicializaGruposPerfiles {

	//private static final long MAXDIFF = 3600000;		//1 hora
	private static ArrayList<GrupoPerfiles> grupos = null;
	private static Calendar ref = null;

	private FacPerfilDao daoPerf = null;

	public InicializaGruposPerfiles() {
		daoPerf = new FacPerfilDao();
	}

	public void cierraConexiones() {
		daoPerf.cierraConexion();
	}


	public static synchronized ArrayList<GrupoPerfiles> obtenUniverso() {

		/*Calendar ref2 = Calendar.getInstance();

		if(ref == null) {
			ref = Calendar.getInstance();
		}

		long diff = ref2.getTimeInMillis() - ref.getTimeInMillis();*/

		if(grupos != null/* && diff < MAXDIFF*/) {
			return grupos;
		}

		AdmUsrGruposDAO gruposDAO = new AdmUsrGruposDAO();
		AdmUsrPerfilesDAO perfilesDAO = new AdmUsrPerfilesDAO();
		MQQueueSession mqsess = null;

		String cvePerfil = "";
		String indPag = "NO";

		try {
			mqsess = new MQQueueSession(
					NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE,
					NP_JNDI_QUEUE_REQUEST);

			perfilesDAO.setMqsess(mqsess);
			gruposDAO.setMqsess(mqsess);
			AdmUsrGL35 gl35 = gruposDAO.consultaGrupos("");
			EIGlobal.mensajePorTrace("InicializaGruposPerfiles - obtenUniverso()- " +
					"Despues de Consultar Grupos", NivelLog.DEBUG);

			if(gl35 != null && gl35.isCodExito()){ //Si se encontraron grupos

				grupos = gl35.getGrupos();
				FacPerfilDao daoFac = new FacPerfilDao();
				//Se buscan sus respectivos perfiles
				for(GrupoPerfiles grp: grupos){
					EIGlobal.mensajePorTrace("==> grp.getCveGrupo() - [" +
							grp.getCveGrupo() + "]", NivelLog.DEBUG);
					AdmUsrGL37 gl37 = perfilesDAO.consultaPerfiles(grp.getCveGrupo(),
							cvePerfil, indPag);
					EIGlobal.mensajePorTrace("InicializaGruposPerfiles - obtenUniverso()- " +
							"Despues de Consultar Periles", NivelLog.DEBUG);

					if(gl35 != null && gl35.isCodExito()){ //Si se encontraron grupos
						EIGlobal.mensajePorTrace("Continua", NivelLog.DEBUG);

						grp.setPerfiles(gl37.getPerfiles());

						for(PerfilPrototipo perf: grp.getPerfiles()) {
							EIGlobal.mensajePorTrace("Busca facultades para " + perf.getCvePerfil(), NivelLog.DEBUG);
							daoFac.cargaFacultadesPerfil(perf);
						}
					}
				}
			}
		}catch(Exception e) {
			//pintar error
		}
		finally {
			try {
				mqsess.close();
			}catch(Exception e){}
		}

		return grupos;
	}

	public ArrayList<String> seleccionActual(String usuario, String perfil) {
		ArrayList<String> seleccion = new ArrayList<String>();
		//Hashtable result = null;
		//String coderror;
		//ServicioTux servicio = new ServicioTux();
		EIGlobal.mensajePorTrace ("InicializaGruposPerfiles.seleccionActual()-> entrando",EIGlobal.NivelLog.DEBUG);
		for(GrupoPerfiles grp: grupos) {
			EIGlobal.mensajePorTrace ("InicializaGruposPerfiles.seleccionActual()-> grupos",EIGlobal.NivelLog.DEBUG);
			for(PerfilPrototipo perf : grp.getPerfiles()) {
				EIGlobal.mensajePorTrace ("InicializaGruposPerfiles.seleccionActual()-> perfiles",EIGlobal.NivelLog.DEBUG);
				if(daoPerf.tienePerfilAsignado(perfil, perf.getCvePerfil())) {
					EIGlobal.mensajePorTrace ("InicializaGruposPerfiles.seleccionActual()-> perfAsignado",EIGlobal.NivelLog.DEBUG);
					for(Facultad fac : perf.getFacultades()) {
						EIGlobal.mensajePorTrace ("InicializaGruposPerfiles.seleccionActual()-> facultades",EIGlobal.NivelLog.DEBUG);
						//result = null;
						try {
							//result = servicio.accesofac(perfil, fac.getCveFacultad());
							if(!daoPerf.tieneFacultadEx(perfil, fac.getCveFacultad(), "-")) {
								//seleccion.add(grp.getCveGrupo() + "@" + perf.getCvePerfil() + "@" + fac.getCveFacultad());
								seleccion.add((grp.getCveGrupo() + "&" + grp.getDescripcion() + "@" + perf.getCvePerfil() + "&" + perf.getDescripcion() + "@" + fac.getCveFacultad() + "&" + fac.getDescripcion()).replaceAll(",", " "));
							}
						} catch(Exception e) {
							EIGlobal.mensajePorTrace ("InicializaGruposPerfiles.seleccionActual()-> error al obtener facultad",EIGlobal.NivelLog.ERROR);
						}
						/*coderror = ( String ) result.get ( "COD_ERROR" );
						if(result != null && coderror.equals("SEGU0000")) {
							seleccion.add(grp.getCveGrupo() + "@" + perf.getCvePerfil() + "@" + fac.getCveFacultad());
							EIGlobal.mensajePorTrace ("InicializaGruposPerfiles.seleccionActual()-> [" + grp.getCveGrupo() + "@" + perf.getCvePerfil() + "@" + fac.getCveFacultad() + "]",EIGlobal.NivelLog.DEBUG);
						}*/
					}
				}
			}
		}
		EIGlobal.mensajePorTrace ("Pintando Seleccion Actual:",EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace (seleccion.toString(),EIGlobal.NivelLog.DEBUG);
		return seleccion;
	}

}