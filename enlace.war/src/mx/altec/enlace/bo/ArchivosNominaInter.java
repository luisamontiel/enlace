/*
					session.setNameFileNomina( nomArchNomina );
					session.setNumerosEmpleado( "" );
					session.setfacultad( "" );

					sess.getAttribute("nombreArchivo")
					sess.getAttribute("numeroEmp");  checar el set cuando le subo a la session
					sess.getAttribute("facultadesN")
*/
package mx.altec.enlace.bo;
import java.util.*;
import java.io.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;

public class ArchivosNominaInter{
	public String tipoRegistro = "", NumeroSecuencia = "", NumeroEmpleado = "", ApellidoPaterno   = "",
	ApellidoMaterno = "", Nombre = "", NumeroCuenta = "", Importe = "", Referencia="", Edo="";
	public String RFC="", Sexo="";
	public String NombreEmpleado	= "";
	public String NombreCompleto	= "";
	public String NoUtilizado		= "";
    public String ClaveBanco		= "";
	public String PlazaBanxico		= "";
	public String Status			= "";
	public String referencia		= "";

	public String asFechaTransmision="", asTotalRegistros="", asAceptados="", asRechazados="", asFechaActualizacion="";
	BaseServlet BAL;
	BaseResource session;

	RandomAccessFile archivoTrabajo;
	long posicionReg = 0;
	long b	= 0;
	long residuo	 = 0;

	public ArchivosNominaInter( String rutaArchivo,HttpServletRequest request){
		EIGlobal.mensajePorTrace("***ArchivosNominaInter.class #ruta archivo: #"+rutaArchivo, EIGlobal.NivelLog.DEBUG);
		HttpSession sess = request.getSession();
		session = (BaseResource) sess.getAttribute("session");
		sess.removeAttribute("session");
		//session.setNameFileNomina(rutaArchivo);
		sess.setAttribute("nombreArchivo",rutaArchivo);
		sess.setAttribute("session",session);

		try{
			archivoTrabajo = new RandomAccessFile(rutaArchivo,"rw");
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class #Creando archivo: #"+rutaArchivo, EIGlobal.NivelLog.INFO);
		} catch(IllegalArgumentException e) {
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en1: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
		} catch(FileNotFoundException e) {
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en2: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
		} catch(SecurityException e){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en3: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en4: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
		}
	}

		public String lecturaArchivo(){
			String lecturaReg	= "", encabezado	= "";
			String strTabla		= "";
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a lecturaArchivo()&", EIGlobal.NivelLog.INFO);
			try{
				archivoTrabajo.seek(0);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en5: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
			}

			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de N&oacute;mina Interbancaria</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
									"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">Nombre Completo</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Cuenta</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Clave del Banco</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Plaza Banxico</td>"+
									"</tr>";

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Leyendo encabezado&", EIGlobal.NivelLog.INFO);

			try {
				encabezado=archivoTrabajo.readLine();
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajo.readLine();
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en6: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
						lecturaReg=archivoTrabajo.readLine();
						if ( lecturaReg.equals( "" ) )
							lecturaReg = archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en7: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
						return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
						if (lecturaReg.substring(0,1).equals("2")) {
							EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &El tipo de registro es 2&", EIGlobal.NivelLog.INFO);
							lecturaCampos(lecturaReg);

							strTabla=strTabla + creaRegTabla();
						}
					}
				} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en8: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			strTabla=strTabla+"</table>";
			return strTabla;
		}

		public void close(){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a close()&", EIGlobal.NivelLog.INFO);
			try {
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en9: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
			}
		}

		public void agregaRegistro(HttpServletRequest request){
			HttpSession sess = request.getSession();
	        BaseResource session = (BaseResource) sess.getAttribute("session");
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Se ha agregado un registro&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Existen : "+session.getTotalRegsNom()+"&", EIGlobal.NivelLog.INFO);
		}

		public String creaVariablesJS()
		{
		String cadenaVariables="";
		EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a creaVariablesJS()&", EIGlobal.NivelLog.INFO);
		cadenaVariables +="<script>js_tipoRegistro='"+ tipoRegistro + "' \n" +
						"js_NumeroSecuencia=\""+ NumeroSecuencia + "\" \n" +
						"js_NombreEmpleado=\""+ NombreCompleto + "\" \n" +
						"js_NumeroCuenta='"+ NumeroCuenta + "' \n" +
						"js_Importe='"+ formatea(Importe) + "' \n" +
						"js_ClaveBanco='"+ ClaveBanco + "' \n" +
						"js_PlazaBanxico='"+ PlazaBanxico + "' \n</script>";
		return cadenaVariables;
		}

		public int leeRegistro(long posicion, String tipoArchivo){

			String lecturaReg	= "";
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a leeRegistro()&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &posicion &"+ Long.toString(posicion), EIGlobal.NivelLog.INFO);
			posicionaRegistro(posicion);
			try{
				lecturaReg=archivoTrabajo.readLine();
				if ( lecturaReg.trim().equals("") )
						lecturaReg = archivoTrabajo.readLine();
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &La linea leida es:"+lecturaReg+"&", EIGlobal.NivelLog.INFO);
				if(lecturaReg.length()<100) {
					if(
					lecturaReg.indexOf("MANC0010")>0) {
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class ERROR NO EXISTE FOLIO EN MANCOMUNIDAD &", EIGlobal.NivelLog.DEBUG);
				}
				return -1;
			}

			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en10: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}
			lecturaCampos(lecturaReg);
			return 1;
		}

		public int posicionaRegistro(long posicion){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a posicionaRegistro()&", EIGlobal.NivelLog.INFO);
			try {
			//clabe *****
			archivoTrabajo.seek(((posicion-1)*111)+41);
			//	archivoTrabajo.seek(((posicion-1)*109)+41);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en11: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}
			return 1;
		}

		public String creaRegTabla(){

		    EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a creaRegTabla()&", EIGlobal.NivelLog.INFO);
			String sTabla	= "";
			posicionReg++;
			residuo=posicionReg % 2 ;

			String bgcolor="textabdatobs";
			if (residuo == 0){
				bgcolor="textabdatcla";
			}
	try{
			sTabla=sTabla+"<tr><td class=\""+bgcolor+"\" align=\"center\" nowrap><input type=radio name=seleccionaRegs value="+posicionReg+"></td>";
    		sTabla=sTabla+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+NombreCompleto+"&nbsp;</td>";
			sTabla=sTabla+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+NumeroCuenta+"&nbsp;</td>";
			//sTabla=sTabla+"<td class=\""+bgcolor+"\" nowrap align=\"right\">"+agregarPunto(Importe)+"&nbsp;</td>";
			sTabla=sTabla+"<td class=\""+bgcolor+"\" nowrap align=\"right\">"+formatoMoneda(Importe)+"&nbsp;</td>";
			sTabla=sTabla+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+ClaveBanco+"&nbsp;</td>";
			sTabla=sTabla+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+PlazaBanxico+"&nbsp;</td>";
	} catch (Exception e) {
	e.printStackTrace();
	}
			return sTabla;
		}

	    public String  agregarPunto(String importe){
		EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando al metodo agregarPunto()&"+ importe, EIGlobal.NivelLog.INFO);
		String importeConPunto	= "";
		String cadena1			= importe.substring(0,importe.length()-2);
		cadena1					= cadena1.trim();
		String cadena2			= importe.substring(importe.length()-2, importe.length());
		importe					= cadena1+" "+cadena2;
		importeConPunto			= importe.replace(' ','.');
		if(importeConPunto.startsWith("..")) importeConPunto=".0"+cadena2.trim();
		EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Saliendo del metodo agregarPunto()&", EIGlobal.NivelLog.INFO);
		return importeConPunto;
		}
		//clabe *****************
		public void lecturaCampos(String registro){
			/* Detalle del archivo */
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando al metodo lecturaCampos()&"+ registro, EIGlobal.NivelLog.INFO);
			try {
			tipoRegistro		= registro.substring(0,1);
            NumeroSecuencia		= registro.substring(1,6);
   			NombreCompleto		= registro.substring(6,56);
            NoUtilizado			= registro.substring(56,61);


   			/*NumeroCuenta		= registro.substring(61,72);
	        Importe				= registro.substring(72,90);
       		ClaveBanco			= registro.substring(90,95);
	        PlazaBanxico		= registro.substring(95,100); */


   			NumeroCuenta		= registro.substring(61,81);
	        Importe				= registro.substring(81,99);
       		ClaveBanco			= registro.substring(99,104);
	        PlazaBanxico		= registro.substring(104,109);
			//*********************
			} catch(Exception e) {
                     e.printStackTrace();
			  }

			 EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Se asignaron los valores con importe "+Importe, EIGlobal.NivelLog.INFO);
		}

		public void altaRegistro(String registroNuevo){
			try{
				archivoTrabajo.seek(archivoTrabajo.length()-31);
				String ultimoRegistro=archivoTrabajo.readLine();
				if (ultimoRegistro.equals(""))
				{
					archivoTrabajo.seek(archivoTrabajo.length()-32);
					ultimoRegistro=archivoTrabajo.readLine();
					}
				archivoTrabajo.seek(archivoTrabajo.length()-31);
				archivoTrabajo.writeBytes(registroNuevo+"\r\n"+ultimoRegistro+"\r\n");

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &El registro ha sido dado de alta&", EIGlobal.NivelLog.INFO);
			}
			catch(IOException e) {
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en12: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
			}
		}

		public void edicionRegistros(long posicion, String registroEditado,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Entrando al metodo edicionRegistros()", EIGlobal.NivelLog.INFO);
			HttpSession sess = request.getSession();
	        BaseResource session = (BaseResource) sess.getAttribute("session");
			//clabe *******
			//String registroVacio	="0                                                                                                   ";
			String registroVacio	="0                                                                                                            ";
			if (posicion>1){
				try{ // clabe *********
					//archivoTrabajo.seek(((posicion-1)*102)+41+6);
					archivoTrabajo.seek(((posicion-1)*111)+41+6);
					archivoTrabajo.writeBytes(registroEditado);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en13: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			else{
				try{
					archivoTrabajo.seek(41+6);
					archivoTrabajo.writeBytes(registroEditado);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en14: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}
		}

		public void bajaRegistro(long numRegistro,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando al metodo bajaRegistro() numRegistro&"+ numRegistro, EIGlobal.NivelLog.INFO);
			HttpSession sess = request.getSession();
	        BaseResource session = (BaseResource) sess.getAttribute("session");
			RandomAccessFile archivoOriginal, archivoTemporal;
			File archivo;
			String lineaLeida="";
			String[] contenido;
			int regs=0;
			try{
				//a partir de este bloque try se lee el archivo original, se agrega cada una de sus lineas a un arreglo
				//de strings, se elimina el archivo y se construye un archivo nuevo con el mismo nombre, en este, se escriben
				//tantas lineas como elemntos tenga el arreglo, sin embargo, los registros que se han de eliminar se deben marcar
				//previamente con un cero al inicio
				archivo= new File((String)sess.getAttribute("nombreArchivo"));
				//archivo= new File(session.getNameFileNomina());
				archivoOriginal= new RandomAccessFile(archivo, "rw");
				archivoOriginal.seek(0);

				//posicionar al inicio del registro que se ha de eliminar
				//clabe **********
				archivoOriginal.seek(41+((numRegistro-1)*111)); // AQUI 129 / 102
				//archivoOriginal.seek(41+((numRegistro-1)*102)); // AQUI 129 / 102
				String registroVacio="0                                                                                                            \r\n";
			  //String registroVacio="0                                                                                                   \r\n";
				//**************************
				archivoOriginal.writeBytes(registroVacio);
				//el registro se ha sustituido por el
				//archivoOriginal es la variable que representa el archivo de donde se eliminara un registro
				contenido=new String[registrosEnArchivo(archivoOriginal)];
				archivoOriginal.seek(0);
				for(int i=0; i<contenido.length; i++){
					lineaLeida=archivoOriginal.readLine();
//					lineaLeida=lineaLeida.substring(0,lineaLeida.length()-1);
					lineaLeida=lineaLeida.substring(0,lineaLeida.length());
					contenido[i]=lineaLeida;
				}
				archivoOriginal.close();
				archivo.delete();
				//archivoTemporal=new RandomAccessFile(session.getNameFileNomina(), "rw");
				archivoTemporal=new RandomAccessFile((String) sess.getAttribute("nombreArchivo"), "rw");

				for(int a=0; a<contenido.length; a++){
					if(!contenido[a].startsWith("0")){
						archivoTemporal.writeBytes(contenido[a]+"\r\n");
						regs++;
					}
				}
				archivoTemporal.close();

			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en15: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
			}

		}//fin del metodo

	public static int registrosEnArchivo(RandomAccessFile file){
		EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &entrando al metodo registrosEnArchivo&()", EIGlobal.NivelLog.INFO);
		String registroLeido	= "";
		long posicion			= 0;
		int cantRegistros		= 0;
		try{
			if(file.length()>0){
				file.seek(0);
				do{
					registroLeido=file.readLine();
					posicion=file.getFilePointer();
					cantRegistros++;
				}while(posicion<file.length());
			}
		}
		catch(IOException e){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en16: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
		}
		return cantRegistros;
	}//fin del metodo registrosEnArchivo
		//clabe **********
		//public int size(){return 102;} //AQUI 129 - 102
		public int size(){return 111;} //AQUI 129 - 102
		public String modificaDetalle(){
			String s	= "";
			return s;
		}

	//////////DEFINICION DEL METODO PARA ACTUALIZAR EL REGISTRO SUMARIO
		public void actualizaSumario(String campo, String valorActual, boolean modificacion,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &entrando al metodo para modificar el registro sumario&()", EIGlobal.NivelLog.INFO);
			String campoModificado	= "";
			String valorModificado	= "";
			String lastRecord		= "";
			int valorSecuencial		= 0;
			int nuevoValorSecuencial= 0;
			int valorTotalRegistros = 0;
			int	valorImporteTotal	= 0;
			int valorAnteriorImporteTotal = 0;

			///////MODIFICA EL CAMPO NUMERO SECUENCIAL CUANDO SE EJECUTA UNA ALTA
			if (campo.equals("sumSecuencial") && modificacion){
				try{
					valorSecuencial= Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en17: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}

				nuevoValorSecuencial=valorSecuencial+1;
				valorModificado=String.valueOf(nuevoValorSecuencial);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setLastSecNom(valorModificado);
				sess.setAttribute("session",session);

				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord=lastRecord.substring(0,1)+valorModificado+lastRecord.substring(6,lastRecord.length());
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en18: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}

			///////MODIFICA EL CAMPO NUMERO SECUENCIAL CUANDO SE EJECUTA UNA BAJA
			if (campo.equals("sumSecuencial") && modificacion==false){
				try{
					valorSecuencial= Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en19: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}

				nuevoValorSecuencial=valorSecuencial;
				valorModificado=String.valueOf(nuevoValorSecuencial-1);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setLastSecNom(valorModificado);
				sess.setAttribute("session",session);

				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);

					lastRecord=lastRecord.substring(0,1)+valorModificado+lastRecord.substring(6,lastRecord.length());
					archivoTrabajo.writeBytes(lastRecord);
				}

				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en20: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}

			///////MODIFICA EL CAMPO TOTAL DE REGISTROS CUANDO SE EJECUTA UNA ALTA
			if (campo.equals("totalRegs") && modificacion){
				try{
					valorTotalRegistros=Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en21: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}

				valorModificado=String.valueOf(valorTotalRegistros+1);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setTotalRegsNom(valorModificado);
				sess.setAttribute("session",session);

				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord=lastRecord.substring(0,6)+valorModificado+lastRecord.substring(11,lastRecord.length());
					archivoTrabajo.writeBytes(lastRecord);
				}

				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en22: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}

			///////MODIFICA EL CAMPO TOTAL DE REGISTROS CUANDO SE EJECUTA UNA BAJA

			if (campo.equals("totalRegs") && modificacion==false){
				try{
					valorTotalRegistros=Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en23: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}

				valorModificado=String.valueOf(valorTotalRegistros-1);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setTotalRegsNom(valorModificado);
				sess.setAttribute("session",session);

				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord=lastRecord.substring(0,6)+valorModificado+lastRecord.substring(11,lastRecord.length());
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en24: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}

			///////MODIFICA EL CAMPO IMPORTE TOTAL CUANDO SE EJECUTA UNA ALTA
			if (campo.equals("impTotal") && modificacion){
				try{
					valorActual=valorActual.trim();
					valorAnteriorImporteTotal=Integer.parseInt(session.getImpTotalNom().trim());
					valorImporteTotal=Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en25: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}

				valorModificado=String.valueOf(valorAnteriorImporteTotal+valorImporteTotal);
				valorModificado=completaCamposNumericos(valorModificado.length(),18)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setImpTotalNom(valorModificado);
				sess.setAttribute("session",session);

				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord=lastRecord.substring(0,11)+valorModificado;
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en26: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}

			///////MODIFICA EL CAMPO IMPORTE TOTAL CUANDO SE EJECUTA UNA BAJA
			if (campo.equals("impTotal") && modificacion==false){
				try{
					valorActual=valorActual.trim();
					valorImporteTotal=Integer.parseInt(valorActual);
					valorAnteriorImporteTotal=Integer.parseInt(session.getImpTotalNom().trim());
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en27: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}

				valorModificado=String.valueOf(valorAnteriorImporteTotal-valorImporteTotal);
				valorModificado=completaCamposNumericos(valorModificado.length(),18)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setImpTotalNom(valorModificado);
				sess.setAttribute("session",session);

				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord=lastRecord.substring(0,11)+valorModificado;
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en28: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}


		}//FIN DEL METODO PARA MODIFICAR REGISTRO SUMARIOO


		//metodo que actualiza el campo "Numero Secuencial" para cada uno de los registros de detalle
		//esta actualizacion se llevara a cabo cuando se elimine uno de los registros
		//el metodo recibe dos argumentos, uno es el archivo dentro del cual se haran las modificaciones
		//el segundo es la cantidad de registros que debe actualizar
		public void actualizaConsecutivos(int cantRegistros,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando al metodo que actualiza consecutivos&", EIGlobal.NivelLog.INFO);
			HttpSession sess = request.getSession();
	        BaseResource session = (BaseResource) sess.getAttribute("session");
			try{
				//RandomAccessFile elArchivoTrabajo=new RandomAccessFile(session.getNameFileNomina(),"rw");
				RandomAccessFile elArchivoTrabajo=new RandomAccessFile((String)sess.getAttribute("nombreArchivo"),"rw");
				elArchivoTrabajo.seek(0);
				String regEncabezado=elArchivoTrabajo.readLine();
				if ( regEncabezado.equals( "" ) )
					regEncabezado = elArchivoTrabajo.readLine();
				for(int i=0; i<cantRegistros; i++){
					elArchivoTrabajo.writeBytes("2"+completaNumericos(String.valueOf(i+2),5));
					// clabe ********
					//elArchivoTrabajo.seek(elArchivoTrabajo.getFilePointer()+96); // AQUI 123 - 96
					elArchivoTrabajo.seek(elArchivoTrabajo.getFilePointer()+105); // AQUI 123 - 96

				}//fin de for
				elArchivoTrabajo.close();
			}
			catch(IOException exception){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en29: %ArchivosNominaInter()"+exception, EIGlobal.NivelLog.INFO);
			}
		} //fin del metodo

		public String completaCamposNumericos(int longitudActual, int longitudRequerida){
			String ceros="0";
		    int faltantes= longitudRequerida-longitudActual;
		    for(int i=1; i<faltantes; i++){
				ceros=ceros+"0";
		    }
			return ceros;
		}//fin de metodo


	//inicio del metodo completa campos numericos, el cual es una replica del metodo anterior, sin embargo,
	//se presentan dos ligeras variantes, el metodo rellena con ceros en lugar de con blancos, ademas
	//siempre se agregan los caracteres a la izquierda
		public String completaNumericos(String cadena, int longitudRequerida){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando al metodo para completar campos numericos&()", EIGlobal.NivelLog.INFO);
			String ceros	= "";
			String cadenaDeRegreso	= "";
			int longitudActual=cadena.length();
			if(longitudActual!=longitudRequerida){
			    int faltantes= longitudRequerida-longitudActual;
				for(int i=0; i<faltantes; i++){
					ceros=ceros+"0";
			    }
			}
			cadenaDeRegreso=ceros+cadena;
			return cadenaDeRegreso;
		}//fin del metodo

		public String[] envioArchivo(String trama_inicio,HttpServletRequest request){
			int cantRegistros= Integer.parseInt(session.getTotalRegsNom());
			String linea="", encabezado="" ,tipoReg = "", formatoEnviado = "";
			String []trama=new String[iteraciones(cantRegistros)];
			int contador=0;

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a envioArchivo()&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &trama_inicio()&"+trama_inicio, EIGlobal.NivelLog.INFO);

				try {


					archivoTrabajo.seek(41);
					tipoReg=archivoTrabajo.readLine();
					if (tipoReg.length() == 109)
					    formatoEnviado = "unix";
						else formatoEnviado = "Microsoft";
					archivoTrabajo.seek(0);
					encabezado=archivoTrabajo.readLine();
					/*if ( encabezado.equals( "" ) )
						encabezado = archivoTrabajo.readLine();*/
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en30: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
				if (encabezado!=null){
					for(int z=0; z<trama.length; z++){
						trama[z]= trama_inicio+creaTrama(formatoEnviado,request);
						trama[z]=modificaTrama(trama[z]);

					}
					try {
						archivoTrabajo.close();
					}
					catch(IOException e){
						EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en31: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
					}
			}
			return trama;
		}//fin del metodo
// pendiente
		public String creaTrama(String formatoEnviado,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a creaTrama()&", EIGlobal.NivelLog.INFO);
			String registroDetalle="";
			String registro="";
			registro=registro.trim();
			int numeroRegistros= Integer.parseInt(session.getTotalRegsNom());
			try{
				registroDetalle=archivoTrabajo.readLine();
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class registroDetalle = &" + registroDetalle + "&", EIGlobal.NivelLog.INFO);
				/*if ( registroDetalle.equals( "" ) )
						registroDetalle = archivoTrabajo.readLine();*/
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en32: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
			}
			for(int a=0; a<3 ; a++){
				if(registroDetalle.startsWith("2")){
					//String secuencial=Integer.toString(a+1);
					b=b+1;
					String secuencial=Long.toString(b);
					//String cuenta= registroDetalle.substring(98,109);
//		   			NumeroCuenta		=registro.substring(61,72);
//	        Importe				=registro.substring(72,90);
					String cuenta= registroDetalle.substring(93,111).trim();
					//aqui
					String importe=registroDetalle.substring(111,registroDetalle.length());
					importe	= eliminaCerosImporte(importe);
					importe	= importe.trim();
					if(importe.length()==1)
						importe="0"+importe;
					importe=agregarPunto(importe);
					registro=registro+secuencial+"@"+cuenta+"@"+importe+"@";

					try{
						registroDetalle=archivoTrabajo.readLine();
						EIGlobal.mensajePorTrace("***ArchivosNominaInter.class registroDetalle = &" + registroDetalle + "&", EIGlobal.NivelLog.INFO);
						/*if ( registroDetalle.equals( "" ) )
							registroDetalle = archivoTrabajo.readLine();*/
					}

					catch(IOException e){
						EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en33: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
					}
				}

				else{
					break;
				}
			}

			try{
			  if (formatoEnviado.equals("unix"))
				archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-2);
				else archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-1);
			//	archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-2);
			//	archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-1);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en34: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class registro = &" + registro + "&", EIGlobal.NivelLog.INFO);
			return registro;
		}


		public int cuentaArrobas(String cadena){
			int arrobas=0;
			try{
				for(int a=0; a<cadena.length(); a++){
					if(cadena.substring(a,a+1).equals("@"))
						arrobas=arrobas+1;
				}
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en35: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class cuentaArrobas = &" + arrobas + "&", EIGlobal.NivelLog.INFO);
			return arrobas;

		}

		public String modificaTrama(String tramaModificable){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando al metodo tramaModificable()&", EIGlobal.NivelLog.DEBUG);

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &tramaModificable = &" + tramaModificable + "&", EIGlobal.NivelLog.DEBUG);

			String tramaDevuelta="";
			int valorDevuelto=0;
			int arrobas=0;
			int registrosEnTrama=0;
			String rEnTrama=tramaModificable.substring((posCar(tramaModificable,'@',2)+1),posCar(tramaModificable,'@',3));
			registrosEnTrama=Integer.parseInt(rEnTrama);
			if(cuentaArrobas(tramaModificable)==12)
				valorDevuelto=3;
			else if(cuentaArrobas(tramaModificable)==9)
				valorDevuelto=2;
			else if(cuentaArrobas(tramaModificable)==6)
				valorDevuelto=1;

			tramaDevuelta=tramaModificable.substring(0,posCar(tramaModificable,'@',2)+1)+valorDevuelto+tramaModificable.substring(posCar(tramaModificable,'@',3),tramaModificable.length());

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &tramaDevuelta = &" + tramaDevuelta + "&", EIGlobal.NivelLog.DEBUG);

			return tramaDevuelta;
		}

		public String formateaImporte(String importe){
			String importeFormateado	= "";
			boolean contienePunto		= false;
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando al metodo formatea importe()&", EIGlobal.NivelLog.INFO);
			int i	= 0;
			for (i=0; i<importe.length(); i++){
				if(importe.charAt(i)=='.'){
					contienePunto	= true;
					break;
				}
			}
			if(contienePunto){
				importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
			}
			else
				importeFormateado=importe;

			return importeFormateado;
		}

		public int iteraciones(int records){
			int iteraciones=records/3;
			if ((records%3)!=0){
				iteraciones=iteraciones+1;
				return iteraciones;
			}
			else{
				return iteraciones;
			}
		}


		public String eliminaCerosImporte(String imp){
			while(imp.startsWith("0")){
				imp=imp.substring(1,imp.length());
			}
		return imp;
		}


	////////////////////////////////////////// metodos para archivo de salida de empleados /////////////////////////////////
		public String lecturaArchivoSalida(HttpServletRequest request){
			String lecturaReg="", encabezado="";
			String strTabla="";
			int contador=0;
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a lecturaArchivoSalida()&", EIGlobal.NivelLog.INFO);
			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
							        "<tr> "+
									"<td class=\"textabref\">Recuperacion del Archivo de Pago de N&oacute;mina Interbancaria</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
									"<tr> "+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Cuenta</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Status</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Clave del Banco</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Plaza Banxico</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"</tr>";
			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();					//lectura del encabezado
				//if ( encabezado.equals( "" ) )
				//		encabezado = archivoTrabajo.readLine();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en36: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}

			if (encabezado!=null && encabezado.substring(0,2).equals("OK"))
			 {
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setCuentaCargo(encabezado.substring(posCar(encabezado,';',9)+1,posCar(encabezado,';',10)));
				sess.setAttribute("session",session);
				lecturaEncabezadoAS(encabezado);

				do {
					try{
						posicionReg=archivoTrabajo.getFilePointer(); // posiblemente es cancelar esta l�nea
						lecturaReg=archivoTrabajo.readLine();
						//if ( lecturaReg.equals( "" ) )
						//	lecturaReg = archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en37: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
						return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
						lecturaCamposAS(lecturaReg);
						strTabla=strTabla + creaRegAS(contador);
						contador++;
					}
				}while (lecturaReg!=null);
				strTabla=strTabla+"</table>";
				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en38: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			else return "";
			return strTabla+ "|" +contador + "||";
		}

		public void lecturaCamposAS(String r){
			/* Detalle del archivo */
			if(r==null)
				r = "";
		try{
			 NumeroCuenta   = r.substring(0,r.indexOf(';'));
			 Importe		= r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			 referencia		= r.substring(posCar(r,';',3)+1,posCar(r,';',4));
			 Status         = r.substring(posCar(r,';',5)+1,posCar(r,';',6));
			 ClaveBanco     = r.substring(posCar(r,';',6)+1,posCar(r,';',7));
			 PlazaBanxico   = r.substring(posCar(r,';',7)+1,posCar(r,';',8));
			 Nombre         = r.substring(posCar(r,';',8)+1,posCar(r,';',9));
		} catch(Exception e) {
				e.printStackTrace();
			}

		}

		public void lecturaEncabezadoAS(String r){
			/* Encabezado del archivo */
			int tot	= 0,acep = 0,rech = 0;
			if(r==null)
				r= "";
		try{
			 asFechaTransmision= r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			 asTotalRegistros  = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
			 asAceptados       = r.substring(posCar(r,';',3)+1,posCar(r,';',4));
			 tot=Integer.parseInt(asTotalRegistros.trim());
			 acep=Integer.parseInt(asAceptados.trim());
			 rech=tot-acep;
			 asRechazados      = ""+rech;
			 asFechaActualizacion= r.substring(posCar(r,';',4)+1,posCar(r,';',5));
		 } catch(Exception e) {
			e.printStackTrace();
			}
		}

		public String creaRegAS(int contador){
			String sTabla="", cNumeroCuenta="&nbsp;", cImporte="&nbsp;", cStatus="&nbsp;", cClaveBanco="&nbsp;", cPlazaBanxico="&nbsp;", cNombreCompleto="&nbsp;";

			String sColor="";
			int residuo=0;
			residuo=contador % 2 ;

			sColor="textabdatcla";
				if (residuo == 0){
					sColor="textabdatobs";
				}

				if (!NumeroCuenta.trim().equals("")) {cNumeroCuenta=NumeroCuenta;}
				if (!Importe.trim().equals("")) {cImporte=Importe;}
				if (!Status.trim().equals("")) {cStatus=Status;}
				//if (!referencia.trim().equals("")) {cReferencia=referencia;}
				if (!ClaveBanco.trim().equals("")) {cClaveBanco=ClaveBanco;}
				if (!PlazaBanxico.trim().equals("")) {cPlazaBanxico=PlazaBanxico;}
				if (!Nombre.trim().equals("")) {cNombreCompleto=Nombre;}

				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + cNumeroCuenta +"&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"right\">" + formatea(cImporte)+"&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + cStatus + "&nbsp;</td>";
				if(referencia==null || referencia.trim().equals("") )
					referencia = "&nbsp;";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + referencia + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + cClaveBanco + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + cPlazaBanxico + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + cNombreCompleto + "&nbsp;</td></tr>\n";
			return 	sTabla;
		}

		public String reporteArchivo(){
			String lecturaReg="", encabezado="";
			String strTabla="";
			int contador=0;
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a reporteArchivo()&", EIGlobal.NivelLog.INFO);
			strTabla="<table align=center cellspacing=0 border=0>";
			/*strTabla=strTabla+"<tr><td colspan=9 align=center><b>Pago de Nomina</b></td></tr>"+
						"<tr><td bgcolor=red>No.Empleado</td><td bgcolor=red>Paterno</td><td bgcolor=red>Materno"+
						"</td><td bgcolor=red>Nombre</td><td bgcolor=red>Cuenta</td>"+
						"<td bgcolor=red>Importe</td><td bgcolor=red>Ref</td><td bgcolor=red>Edo</td>";*/

			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de N&oacute;mina Interbancaria</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">"+
									"<tr> "+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">Nombre Completo</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Cuenta</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Clave del Banco</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Plaza Banxico</td>"+
									"</tr>";

			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
				//if ( encabezado.equals( "" ) )
				//		encabezado = archivoTrabajo.readLine();
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a reporteArchivo()&"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}

			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Revisando el encabezado " + encabezado, EIGlobal.NivelLog.INFO);

			if (encabezado!=null){
				do{
					try{posicionReg=archivoTrabajo.getFilePointer();
					lecturaReg=archivoTrabajo.readLine();

					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Se leyo: " + lecturaReg, EIGlobal.NivelLog.INFO);

				//	if ( lecturaReg.equals( "" ) )
				//		lecturaReg = archivoTrabajo.readLine();
					}
					catch(Exception e){
									EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
									return "Error 3: "+ e.getMessage();
									}
					if (lecturaReg!=null){
						if (lecturaReg.substring(0,1).equals("2")){
							lecturaCampos(lecturaReg);
							 strTabla+= creaRegReporte(contador);
							contador++;
						}
					}
				} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
			try{
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en39: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				return "Error 4: "+ e.getMessage();
			}
		}
		strTabla=strTabla+"</table>";
		return strTabla;
		}


		public String creaRegReporteI(int contador){ //AQUI
			String sTabla="";

			String sColor="";
			int residuo=0;
			residuo=contador % 2 ;

			sColor="textabdatcla";
				if (residuo == 0){
					sColor="textabdatobs";
				}

				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + NumeroCuenta +"&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + formatoMoneda(Importe)+"&nbsp;</td>";
				//sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + formatea(Importe)+"&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + Status + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + ClaveBanco + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + PlazaBanxico  + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\""+sColor+"\" nowrap align=\"center\">" + Nombre + "&nbsp;</td></tr>\n";
			return 	sTabla;


		}

		public String creaRegReporte(int contador){
		       EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a creaRegReporte()& contador = "+contador, EIGlobal.NivelLog.INFO);
			String sTabla="";


			String sColor="";
			int residuo=0;
			residuo=contador % 2 ;

			sColor="textabdatcla";
				if (residuo == 0){
					sColor="textabdatobs";
				}


    		sTabla=sTabla+"<td class=\""+sColor+"\" nowrap align=\"center\">"+NombreCompleto+"&nbsp;</td>";
			sTabla=sTabla+"<td class=\""+sColor+"\" nowrap align=\"center\">"+NumeroCuenta+"&nbsp;</td>";
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a creaRegReporte()& formatear: "+Importe, EIGlobal.NivelLog.INFO);
			sTabla=sTabla+"<td class=\""+sColor+"\" nowrap align=\"right\">"+Importe  +"&nbsp;</td>";
			//sTabla=sTabla+"<td class=\""+sColor+"\" nowrap align=\"right\">"+agregarPunto(Importe)  +"&nbsp;</td>";
			sTabla=sTabla+"<td class=\""+sColor+"\" nowrap align=\"center\">"+ClaveBanco+"&nbsp;</td>";
			sTabla=sTabla+"<td class=\""+sColor+"\" nowrap align=\"center\">"+PlazaBanxico+"&nbsp;</td></tr>";

			//sTabla=sTabla + "</td></tr>";
			return 	sTabla;
		}

	public String reporteArchivoSalida()
		{
		String lecturaReg	= "", encabezado	= "";
		String strTabla	= "";
		int contador	= 0;
		EIGlobal.mensajePorTrace("***ArchivosNominaInter.class &Entrando a reporteArchivoSalida()&", EIGlobal.NivelLog.INFO);
		strTabla="<table align=center cellspacing=0 border=0>";
				strTabla="<table width=\"640\" border=\"0\" cellpadding=\"0\" align=\"center\">" +
				"<tr> " +
				"<td class=\"textabref\">Recuperacion del Archivo de Pago de N&oacute;mina Interbancaria</td>" +
				"</tr>" +
				"</table>" +
				"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
				"<tr> " +
				"<td width=\"55\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Cuenta</td>" +
				"<td width=\"70\" class=\"tittabdat\" align=\"right\">Importe</td>" +
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Status</td>" +
				"<td width=\"100\" class=\"tittabdat\" align=\"center\">Clave del Banco</td>" +
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Plaza Banxico</td>" +
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Nombre</td>" +
				"</tr>";

		try
			{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();	//lectura del encabezado
				//if ( encabezado.equals( "" ) )
				//	encabezado = archivoTrabajo.readLine();
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en40: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
		if (encabezado!=null && encabezado.substring(0,2).equals("OK"))
			{
				lecturaEncabezadoAS(encabezado);
				do
					{
						try
							{
								posicionReg=archivoTrabajo.getFilePointer();
								lecturaReg=archivoTrabajo.readLine();
								//if ( lecturaReg.equals( "" ) )
								//	lecturaReg = archivoTrabajo.readLine();
							}
						catch(Exception e)
							{
							EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en41: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);											return "Error 3: "+ e.getMessage();
							}
						if (lecturaReg!=null)
							{
								lecturaCamposAS(lecturaReg);
								strTabla=strTabla + creaRegReporteI(contador);
								contador++;
							}
					} while (lecturaReg!=null);
				//strTabla=strTabla+"</table>";
				try
					{
						archivoTrabajo.close();
					}
				catch(IOException e)
					{
						EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Ha ocurrido una excepcion en42: %ArchivosNominaInter()"+e, EIGlobal.NivelLog.INFO);
						return "Error 4: "+ e;
					}
			}
		else
			{
				return "";
			}
		strTabla=strTabla+"</table>";
		return strTabla;
		}



		public String formatea(String formatea){
		String decimales	= "";
		String enteros		= "";
		int position= formatea.indexOf('.');

		if(position>=0){
			decimales= formatea.substring(position+1, formatea.length());
			if(position>=1)
				enteros=formatea.substring(0, position);
			if(decimales.length()>2)
				decimales=decimales.substring(0, 2);
			if(decimales.length()==1)
				decimales=decimales+"0";
		}
		else{
			enteros=formatea;
			decimales="00";
		}
		return enteros+"."+decimales;
	}


		public String lecturaArchivoRegreso(){

			String contenidoArch	= "";
			String contenidoArchtmp	= "";
			long bytesLeidos		= 0;
			long longitudArch		= 0;
			boolean leyendo			= true;
			try{
				longitudArch	= archivoTrabajo.length();
			}
			catch(IOException e){
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Excepcion en: " + e.toString(), EIGlobal.NivelLog.INFO);
			}


			for(int i=0; i<Integer.parseInt(session.getTotalRegsNom())+2; i++){
				try{

					 contenidoArchtmp = archivoTrabajo.readLine();
 					 if (contenidoArchtmp != null)
					 contenidoArch =contenidoArch +  contenidoArchtmp;

				/*	if (contenidoArch.length () > 24){

						if((!contenidoArch.startsWith("OK"))&&(!contenidoArch.substring(16,24).equals("INTN0000")))
							{
							contenidoArch = "Su transacci�n no puede ser atendida en este momento.";
						}
					}else { contenidoArch = "Su transacci�n no puede ser atendida en este momento.";
					}*/
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Excepcion en43: " + e.toString(), EIGlobal.NivelLog.INFO);
				}
			}

			return contenidoArch;
		}


		public int posCar(String cad,char car,int cant){
		int result=0,pos=0;

		for (int i=0;i<cant;i++){
			result=cad.indexOf(car,pos);
			pos=result+1;
			}
		return result;
		}


	public RandomAccessFile traeArchivo(String nombreNuevo) throws IOException
		{
		RandomAccessFile archivoDevuelto=new RandomAccessFile(nombreNuevo, "rw");;
		String registroLeido	= "";
		try
			{
			archivoTrabajo.seek(0);
			registroLeido	= "";
			do
				{
				registroLeido=archivoTrabajo.readLine();
				archivoDevuelto.writeBytes(registroLeido);
				}
			while(registroLeido.startsWith("1")||registroLeido.startsWith("2")||registroLeido.startsWith("3"));
			archivoDevuelto.close();
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Excepcion en: " + e.toString(), EIGlobal.NivelLog.INFO);
			}
		return archivoDevuelto;
		}

		public String leeEncabezadoImportado(){
			String encabezadoImportado	= "";
			try{
				archivoTrabajo.seek(0);
				encabezadoImportado=archivoTrabajo.readLine();
//				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Excepcion en: " + e.toString(), EIGlobal.NivelLog.INFO);
			}
			return encabezadoImportado;
		}

		public String[] leeDetalleImportado(){
			String encabezadoImportado	= "";
			String[] detalleImportado=null;
			try{
				detalleImportado	= new String[Integer.parseInt(session.getTotalRegsNom())];
				archivoTrabajo.seek(0);
				encabezadoImportado	= archivoTrabajo.readLine();
				for(int i=0; i<Integer.parseInt(session.getTotalRegsNom()); i++){
					detalleImportado[i]	= archivoTrabajo.readLine();
				}
				archivoTrabajo.close();
			}
			catch(IOException e){
			detalleImportado=new String[0];
			EIGlobal.mensajePorTrace("***ArchivosNominaInter.class Excepcion en: " + e.toString(), EIGlobal.NivelLog.INFO);
			}
			return detalleImportado;
		}
	public String formatoMoneda( String cantidad )
	{
		String language = "la"; // ar
		String country  = "MX";  // AF
		Locale local    = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato  = "";
		String cantidadDecimal = "";
		String formatoFinal    = "";
		String cantidadtmp     = "";

		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.0";
			try {
				cantidadtmp =cantidad.substring(0,cantidad.length()-2);
				cantidadDecimal = cantidad.substring(cantidad.length()-2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				if (formato.substring (0,3).equals ("MXN"))
					formato=formato.substring(3,formato.length()-2);
				else
					formato=formato.substring(1,formato.length()-2);
				formatoFinal = formato + cantidadDecimal;
			} catch(NumberFormatException e) {
				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.INFO);
				formatoFinal="0.00";}
		if(formatoFinal==null)
			formato = "";
		return formatoFinal;
	}
}