package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;

import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class ConsultaDocum
{

    /** Servico de Tuxedo */
    protected ServicioTux ConsultaDocum;
    /** Numero de contrato */
    protected String Contrato;
    /** Clave de Usuario */
    protected String Usuario;
    /** Perfil del usuario */
    protected String Perfil;
	/** Fecha Inicial */
	protected String Fecha1;
	/** Fecha final */
	protected String Fecha2;

    /** Creates new pdCombosConf */
    public ConsultaDocum() {
        ConsultaDocum = new ServicioTux();
    }

    /**
     * Metodo para obtener el servicio de Tuxedo
     */
    public ServicioTux getServicioTux() {return ConsultaDocum;}

    protected File recibe(String Nombre) {
        try {
        	EIGlobal.mensajePorTrace("ConsultaDocum:recibe - Inicia", EIGlobal.NivelLog.INFO);
        	//IF PROYECTO ATBIA1 (NAS) FASE II
        	 Properties rs = new Properties();
             FileInputStream in = new FileInputStream(Global.ARCHIVO_CONFIGURACION);

			 rs.load(in);
           
           	boolean Respuestal = true;
            ArchivoRemoto recibeArch = new ArchivoRemoto();
           
           	if(!recibeArch.copiaCUENTAS("/" + Nombre, rs.getProperty("DIRECTORIO_LOCAL"))){
				
					EIGlobal.mensajePorTrace("*** ConsultaDocum.recibe  No se realizo la copia remota:" + Nombre, EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** ConsultaDocum.recibe archivo remoto copiado exitosamente:" + Nombre, EIGlobal.NivelLog.DEBUG);
				    
				}
           
            if (Respuestal) {
                File Archivo = new File(
                		Nombre);
                return Archivo;   
           	}
       	   
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return null;
    }
   /* 2TCT|1001851|CFAE|80000648376|1001851|1001851A|2@04/03/2002@10/05/2002@|*/

    public String envia_tux() {
        try {
            String Trama = "2EWEB|" + Usuario + "|CFAE|" + Contrato + "|"
                            + Usuario + "|" + Perfil + "|" + 2 + "@" + Fecha1 + "@"
							+ Fecha2 + "@|";
            File ArchivoResp;
            Hashtable ht = ConsultaDocum.web_red(Trama);
            String Buffer = (String) ht.get("BUFFER");
            ArchivoResp = recibe(Buffer);
            if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "actua"));
            return (ArchivoResp.getPath());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error al leer el archivo de respuesta";
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error con la comunicacion";
        }

    }

    /* Regresa el valor de la propiedad importe*/
	/*public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        if(importe != null){

            this.importe = importe.trim();
        } else {
               System.out.println("coCFiltro:setImporte - null assignment attempted");
               this.importe = "";
        }
    }


	public void fill(HttpServletRequest req) {

        try{
            setFechaIni( (String) req.getParameter("fechaIni"));
            setFechaFin( (String) req.getParameter("fechaFin"));

            if(null == fechaIni && null == fechaFin ){
                coCFiltro filtro = (pdCFiltro) req.getSession().getAttribute("cocFiltro");
                if(null != filtro){
                    this.fechaFin = filtro.fechaFin;
                    this.fechaIni = filtro.fechaIni;
                    this.importe = filtro.importe;
                    this.mensaje = filtro.mensaje;
                    return;
                }
            }

            setImporte( (String) req.getParameter("importe"));

        } catch (Exception e){
            e.printStackTrace();
        }

    }    */



   /* public String actualiza_consulta (String fechaIni, String fechaLim) {
        try {
            String Trama = "2EWEB|" + Usuario + "|CFAE|" + Contrato + "|" + Usuario + "|"
                            + Perfil + "|2@" + fechaIni + "@" + fechaLim + "@|";
            System.out.println ("ConsultaDocum.class Trama: " + Trama);
            File ArchivoResp;
            Hashtable ht = ConsultaDocum.web_red (Trama);
            String Buffer = (String) ht.get ("BUFFER");
            System.out.println ("ConsultaDocum.class Respuesta: " + Buffer);
            ArchivoResp = recibe(Buffer);
            if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "graf"));
            return (ArchivoResp.getPath());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error al leer el archivo de respuesta";
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error con la comunicacion";
        }
    }*/


    public void setContrato (String contrato) {Contrato = contrato;}
    public void setUsuario (String usuario) {Usuario = usuario;}
    public void setPerfil (String perfil) {Perfil = perfil;}
	public void setFecha1 (String fecha1) {Fecha1 = fecha1;}
	public void setFecha2 (String fecha2) {Fecha2 = fecha2;}
}