package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

/**<B><I><code>InterpretaCEAV<code></I></B> Esta clase revisa si existen avisos de vencimientos de cr&eacute;dito electr&oacute;nico.
<P>
   10/12/2002 <BR>
   13/12/2002 Se modific&oacute; el separador de campos.<BR>
</P>
@author Hugo Ru&iacute;z Zepeda
@version 1.1.0
*/
public class InterpretaCEAV
{
public static final String version="1.1.0";
public static final String BIEN="0000";
private static final String OK="OK";
private static final int[] posiciones={4, 7};
private String tramaRespuesta="";
private String nombreArchivo="";
private String folio="";
private String codigoError="";
private String mensajeError="";
private boolean esCorrecta=false;
private int renglonesLeidos=0;
private String datosEnTabla="";
private String separador;

   public InterpretaCEAV()
   {
      separador="|";
   }


   public boolean interpretaTramaRespuesta()
   {
      String aux, aux1;
      StringTokenizer st;
      inicializa();

      if(tramaRespuesta==null || tramaRespuesta.length()<1)
         return false;

      st=new StringTokenizer(tramaRespuesta, " ");

      if(st.countTokens()<3)
         return false;

      aux=st.nextToken();

      if(!aux.equalsIgnoreCase(OK))
         return false;

      folio=st.nextToken();

      aux=st.nextToken();

      if(aux==null || aux.length()<1)
         return false;

      try
      {
         aux1=aux.substring(posiciones[0], posiciones[1]+1);

         if(aux1==null)
            return false;

         codigoError=aux1;

         StringTokenizer buscaError=new StringTokenizer(tramaRespuesta, "|");

         if(buscaError.countTokens()<2)
            return false;

         buscaError.nextToken();

         if(!aux1.equalsIgnoreCase(BIEN))
         {
            mensajeError=buscaError.nextToken();
            return false;
         } // Fin if mal

         nombreArchivo=buscaError.nextToken();

      }
      catch(ArrayIndexOutOfBoundsException aioobe)
      {
         return false;
      } // Fin try-catch


      esCorrecta=true;
      return esCorrecta;
   } // Fin m�todo interpretaTramaRespuesta


   /**<code><B><I>leeArchivo</I></B></code> se encarga de la lectura del archivo cuyo nombre est&aacute;<BR>
   * contenido en <I>nombreArchivo</I>, leyendo rengl&oacute;n por rengl&oacute;n.
   */
  public boolean leeArchivo()
  {
    File archivo;
    BufferedReader bf=null;
    String linea;
    renglonesLeidos=0;
    datosEnTabla="";
    boolean resultadoArchivo=false;

    archivo=new File(getNombreArchivo());
    //lineas=new Vector();
    //cadenaDatos=new String();

    if(!archivo.isFile())
      return false;

    try
    {
      bf=new BufferedReader(new FileReader(archivo));

      while(bf.ready())
      {
        linea=bf.readLine();


        if(linea!=null && linea.length()>0)
        {
          renglonesLeidos++;
          datosEnTabla+=construyeRenglon(linea);
        } // Fin if
      } // Fin while

      resultadoArchivo=true;
    }
    catch(IOException iox)
    {
      //return false;
      resultadoArchivo=false;
    }
    finally
    {
      if(bf!=null)
      {
         try
         {
            bf.close();
         }
         catch(IOException iox02)
         {
         } // Fin try-catch
      } // Fin if bf
    } // Fin try-catch-finally



    //return true;
    return resultadoArchivo;
  } // Fin leeArchivo


   private String construyeRenglon(String linea)
   {
      StringBuffer renglonTabla=new StringBuffer("");
      StringTokenizer st;

      if(linea==null || linea.length()<1)
         return "";

      st=new StringTokenizer(linea, getSeparador());

      if(renglonesLeidos%2!=0)
      {
         //renglon impar
         renglonTabla.append(InterpretaPC46.renglonObscuro);

         while(st.hasMoreTokens())
         {
            renglonTabla.append(InterpretaPC46.fondoObscuroIzquierdo);
            renglonTabla.append(st.nextToken());
            renglonTabla.append("</TD>");
         } // Fin while

      }
      else
      {
         //renglon par
         renglonTabla.append(InterpretaPC46.renglonClaro);

         while(st.hasMoreTokens())
         {
            renglonTabla.append(InterpretaPC46.fondoClaroIzquierdo);
            renglonTabla.append(st.nextToken());
            renglonTabla.append("</TD>");
         } // Fin while

      } // Fin if

      renglonTabla.append("</TR>");

      return renglonTabla.toString();
   } // Fin m�todo construyeRenglon


   private void inicializa()
   {
      esCorrecta=false;
      renglonesLeidos=0;
      datosEnTabla="";
      nombreArchivo="";
      codigoError="";
      folio="";
      mensajeError="";
   } // Fin m�todo inicializa


   public void setTramaRespuesta(String trama)
   {
      if(trama!=null && trama.length()>0)
         tramaRespuesta=trama;
   }

   public String getTramaRespuesta()
   {
      return tramaRespuesta;
   }

   public String getNombreArchivo()
   {
      return nombreArchivo;
   }

   public String getFolio()
   {
      return folio;
   }

   public String getCodigoError()
   {
      return codigoError;
   }

   public String getMensajeError()
   {
      return mensajeError;
   }

   public boolean esCorrecta()
   {
      return esCorrecta;
   }


   public int getRenglonesLeidos()
   {
      return renglonesLeidos;
   }

   public String getSeparador()
   {
      return separador;
   }


   public void separador(String separador)
   {
      if(separador!=null && separador.length()>0)
         this.separador=separador;
   }

   public String getDatosEnTabla()
   {
      return datosEnTabla;
   }
} // Fin clase IntrepretaCEAV