package mx.altec.enlace.bo;

import mx.altec.enlace.utilerias.EIGlobal;


/**
 * @author vector
 *
 */
public class transferenciaUPL {
	

	/**
	 * Variable para Cuenta_Cargo 
	 */
	public long Cuenta_Cargo = 0;
	/**
	 * Cuenta_Abono
	 */
	public long Cuenta_Abono = 0;
	
	/**
	 * Importe
	 */
	public double Importe = 0.0;
	
	/**
	 * ImporteCadena
	 */
	public String ImporteCadena = "";
	
	/**
	 * Concepto
	 */
	public String Concepto = "";
	
	/**
	 * Fecha
	 */
	public String Fecha = "";
	
	/**
	 * fechaDia
	 */
	public String fechaDia = "";
	
	/**
	 * fechaMes
	 */
	public String fechaMes = "";
	
	/**
	 * fechaAnio
	 */
	public String fechaAnio = "";
	
	/**
	 * edoCF
	 */
	public String edoCF = ""; // *****Cambio para comprobante fiscal
	
	/**
	 * rfc
	 */
	public String rfc = ""; // *****Cambio para comprobante fiscal
	
	/**
	 * iva
	 */
	public String iva = ""; // *****Cambio para comprobante fiscal
	
	/**
	 * Iva
	 */
	public float Iva = 0; // *****Cambio para comprobante fiscal

	String cuentaC = "";
	
	String cuentaA = "";
	
	/**
	 * err
	 */
	public int err = 0;

	/**
	 * @param cadena
	 * @return boolean
	 */
	boolean isNumber(String cadena) {
		for (int i = 0; i < cadena.length(); i++){
			if (!Character.isDigit(cadena.charAt(i))){
				return false;
			}
		}
		return true;
	}

	
	/**
	 * @param linea
	 * @param tipo
	 */
	public transferenciaUPL(String linea, String tipo) {
		// String linea=""; /***//*IM87104*/
		try {
			EIGlobal.mensajePorTrace("***transferenciaUPL.class  entrando",
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferenciaUPL.class  linea longitud  "
							+ linea.length(), EIGlobal.NivelLog.DEBUG);
			// linea = lin.trim();

			if (linea.length() > 123) // *****Cambio para comprobante fiscal
			{
				err = linea.length();
			} else {
				cuentaC = linea.substring(0, 16);
				cuentaC = cuentaC.trim();
				if (cuentaC.length() == 11 && isNumber(cuentaC)) {
					Cuenta_Cargo = Long.parseLong(cuentaC);
				}
				/***/
				/* IM87104 */else {
					err = -4;
					EIGlobal.mensajePorTrace(
							"***transferencia.transferenciaUPL->cuentaC = "
									+ cuentaC, EIGlobal.NivelLog.DEBUG);
					return;
				}
				EIGlobal.mensajePorTrace(
						"***transferencia.transferenciaUPL->cuentaC = "
								+ cuentaC, EIGlobal.NivelLog.DEBUG);

				cuentaA = linea.substring(16, 32);
				cuentaA = cuentaA.trim();
				/**
				 * Se agrego validacion para cuentas moviles de 10 digitos
				 * */
				if ((cuentaA.length() == 11 || cuentaA.length() == 16 || cuentaA
						.length() == 10)
						&& isNumber(cuentaA)) {
					Cuenta_Abono = Long.parseLong(cuentaA);
				}
				/***/
				/* IM87104 */else {
					err = -5;
					EIGlobal.mensajePorTrace(
							"***transferencia.transferenciaUPL->cuentaA = "
									+ cuentaA, EIGlobal.NivelLog.DEBUG);
					return;
				}
				EIGlobal.mensajePorTrace(
						"***transferencia.transferenciaUPL->cuentaA = "
								+ cuentaA, EIGlobal.NivelLog.DEBUG);

				String punto = linea.substring(42, 43);
				if (!punto.equals(".")) {
					err = -2;
					return;
				}

				String importe = linea.substring(32, 45);
				/***/
				ImporteCadena = importe.trim();
				Importe = new Double(importe.trim()).doubleValue();

				if (linea.length() > 45) {
					// *******************************************
					// inicia Cambio para comprobante fiscal
					if (tipo.equals("1")) // se va a leer con nuevo formato
					{
						if (linea.length() != 93) // 93 es la longitud del
													// formato viejo
						{
							Concepto = linea.substring(45, 85);
							/***/
							/* IM87104 */if (isNumber(linea.substring(85, 93))
									&& linea.substring(85, 93).trim().length() == 8) /***/
							// modificacion para validar si la fecha es
							// nuemerica y de longitud correcta
							{
								fechaDia = linea.substring(85, 87);
								fechaMes = linea.substring(87, 89);
								fechaAnio = linea.substring(89, 93);
								Fecha = fechaDia + "/" + fechaMes + "/"
										+ fechaAnio;
								if (linea.charAt(93) == 'S') {
									rfc = linea.substring(94, 107)
											.toUpperCase();
									iva = linea.substring(107, linea.length());
									// Inicio GAE
									if (!existePuntoImporteIva()) {
										/*
										 * System.out.println("transferenciaUPL::transferenciaUPL:: "
										 * +
										 * "El importe IVA debe tener punto decimal"
										 * );
										 */
										err = -6;
										return;
									} else if (!validaFormatoImporteIva()) {
										/*
										 * System.out.println("transferenciaUPL::transferenciaUPL:: "
										 * +
										 * "Solo se permiten 12 Enteros en el importe IVA"
										 * );
										 */
										err = -7;
										return;
									} else {
										Iva = new Float(iva).floatValue();
									}
									// Fin GAE
								} else {
									edoCF = "N";
									rfc = "";
									iva = "";
								}
							} else {
								err = -3;
								/***/
								/* IM87104 */
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->Fecha = "
												+ Fecha,
										EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->fechaDia = "
												+ fechaDia,
										EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->fechaMes = "
												+ fechaMes,
										EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->fechaAnio = "
												+ fechaAnio,
										EIGlobal.NivelLog.DEBUG);
								return;
							}
						} else {
							err = -1;
							return;
						}
					} else // se va a leer con viejo formato
					{
						if (linea.length() == 93) // 93 es la longitud del
													// formato viejo
						{
							Concepto = linea.substring(45, 85);
							/***/
							/* IM87104 */if (isNumber(linea.substring(85, 93))
									&& linea.substring(85, 93).trim().length() == 8) /***/
							// modificacion para validar si la fecha es
							// nuemerica y de longitud correcta
							{
								fechaDia = linea.substring(85, 87);
								fechaMes = linea.substring(87, 89);
								fechaAnio = linea.substring(89, 93);
								Fecha = fechaDia + "/" + fechaMes + "/"
										+ fechaAnio;
								edoCF = "N";
								rfc = "";
								iva = "";
								err = 0;
							} else {
								err = -3;
								/***/
								/* IM87104 */
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->Fecha = "
												+ Fecha,
										EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->fechaDia = "
												+ fechaDia,
										EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->fechaMes = "
												+ fechaMes,
										EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace(
										"***transferencia.transferenciaUPL->fechaAnio = "
												+ fechaAnio,
										EIGlobal.NivelLog.DEBUG);
								return;
							}
						} else {
							err = -1;
							return;
						}
					}
				} else {
					Concepto = "TRANSFERENCIA EN EFECTIVO";
					edoCF = "N";
					rfc = "";
					iva = "";
					err = 0;
				}

				Concepto = ((Concepto.trim().length() < 1) ? "TRANSFERENCIA EN EFECTIVO"
						: Concepto.trim().toUpperCase());
			}

			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->cuentaC = " + cuentaC,
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->cuentaA = " + cuentaA,
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->ImporteCadena = "
							+ ImporteCadena, EIGlobal.NivelLog.DEBUG);
			EIGlobal
					.mensajePorTrace(
							"***transferencia.transferenciaUPL->Concepto = "
									+ Concepto, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->Fecha = " + Fecha,
					EIGlobal.NivelLog.DEBUG);
			EIGlobal
					.mensajePorTrace(
							"***transferencia.transferenciaUPL->fechaDia = "
									+ fechaDia, EIGlobal.NivelLog.DEBUG);
			EIGlobal
					.mensajePorTrace(
							"***transferencia.transferenciaUPL->fechaMes = "
									+ fechaMes, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->fechaAnio = "
							+ fechaAnio, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->edoCF = " + edoCF,
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->rfc = " + rfc,
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(
					"***transferencia.transferenciaUPL->iva = " + iva,
					EIGlobal.NivelLog.DEBUG);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace("***transferenciaUPL.class " + e,
					EIGlobal.NivelLog.ERROR);
			err = linea.length();
		}
		EIGlobal.mensajePorTrace("***transferenciaUPL.class  valor de err= "
				+ Integer.toString(err), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***transferenciaUPL.class  saliendo",
				EIGlobal.NivelLog.DEBUG);
	}

	
	/**
	 * @return boolean
	 */
	public boolean existePuntoImporteIva() {
		int indPunto = 0;
		try {
			/**
			 * System.out.println("transferenciaUPL::existePuntoImporteIva:: " +
			 * "IVA= " + iva);
			 */
			indPunto = iva.indexOf(".");
			/**
			 * System.out.println("transferenciaUPL::existePuntoImporteIva:: " +
			 * "Posición Punto decimal= " + indPunto);
			 */
			if (indPunto > -1){
				return true;
			}else{
				return false;
			}

		} catch (Exception e) {
			/**
			 * System.out.println("transferenciaUPL::existePuntoImporteIva:: Error->"
			 * + e.getMessage());
			 */
		}
		return false;
	}

	
	/**
	 * @return boolean
	 */
	public boolean validaFormatoImporteIva() {
		int indPunto = 0;
		try {
			/**
			 * System.out.println("transferenciaUPL::existePuntoImporteIva:: " +
			 * "IVA= " + iva);
			 */
			indPunto = iva.indexOf(".");
			/**
			 * System.out.println("transferenciaUPL::validaFormatoImporteIva:: "
			 * + "Posición Punto decimal= " + indPunto);
			 */
			if (indPunto > 12){
				return false;
			}
			return true;
		} catch (Exception e) {
			/**
			 * System.out.println("transferenciaUPL::validaFormatoImporteIva:: "
			 * + "Error->" + e.getMessage());
			 */
		}
		return false;
	}
}