/**
 * Isban Mexico
 *   Clase: DescargaEstadoCuentaBO.java
 *   Descripcion:
 *
 *   Control de Cambios:
 *   1.0 27/06/2013 Stefanini - Creacion
 *   2.0 2013-2014	FSW-Indra - Se redefine la solucion y se aplican los cambios acorde al nuevo flujo
 *   2.1 13/03/2015 FSW-Indra - Cambios para TDC
 */
package mx.altec.enlace.bo;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceException;

import mx.altec.enlace.beans.CuentaODD4Bean;
import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.beans.DY03Bean;
import mx.altec.enlace.beans.DescargaEDCBean;
import mx.altec.enlace.beans.EstadoCuentaBean;
import mx.altec.enlace.beans.ODD4Bean;
import mx.altec.enlace.beans.OW42Bean;
import mx.altec.enlace.beans.TrxODB4Bean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.cliente.ws.cfdiondemand.PeriodCFDI;
import mx.altec.enlace.cliente.ws.cfdiondemand.PeriodsCFDIService;
import mx.altec.enlace.cliente.ws.cfdiondemand.PeriodsCFDIService_Service;
import mx.altec.enlace.cliente.ws.cfdiondemand.RequestWSPeriods;
import mx.altec.enlace.cliente.ws.cfdiondemand.ResponseWSPeriods;
import mx.altec.enlace.cliente.ws.cfdiondemand.SOAPExceptionImpl;
import mx.altec.enlace.cliente.ws.pdfondemand.RequestWS;
import mx.altec.enlace.cliente.ws.pdfondemand.ResponseWS;
import mx.altec.enlace.cliente.ws.pdfondemand.ValidaExistenciaPDFService;
import mx.altec.enlace.cliente.ws.pdfondemand.ValidaExistenciaPDFService_Service;
import mx.altec.enlace.dao.DescargaEstadoCuentaDAO;
import mx.altec.enlace.dao.ODD4DAO;
import mx.altec.enlace.dao.OW42DAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Clase para obtener informacion de Estado de Cuenta de Tarjeta de Credito
 */
public class DescargaEstadoCuentaBO {
	/** Constante para WS de CFDI Tipo de Comprobante **/
	private static final int WS_TIPO_COMPROBANTE = 0;
	/** Constante para WS de CFDI creditos**/
	private static final String WS_TIPO_CREDITO = "002";
	/** Constante Para LOG **/
	private static final String LOG_TAG = "[EdcPdfXml]:::DescargaEstadoCuentaBO-";
	/** Variable random para llave de Descarga **/
	private static SecureRandom random = new SecureRandom();
	/** Objeto para realizar las operaciones en base de datos */
	private transient final DescargaEstadoCuentaDAO estadoCuentaDAO = new DescargaEstadoCuentaDAO();
	/** Objeto para realizar las operaciones de la transaccion ODD4*/
	private transient final ODD4DAO txODD4 = new ODD4DAO();
	/** Objeto para realizar las operaciones de la transaccion OW42 */
	private transient final OW42DAO txOW42 = new OW42DAO();

	/**
	 * Consulta estatus estado cuenta.
	 * @author FSW-Indra
	 * @param estadoCuenta the estado cuenta
	 * @return the dy03 bean
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public DY03Bean consultaEstatusEstadoCuenta(EstadoCuentaBean estadoCuenta) throws BusinessException {
		EIGlobal.mensajePorTrace(String.format("%s Ejecutando servicio de consulta de estado de cuenta...", LOG_TAG)
				, EIGlobal.NivelLog.DEBUG);
		return estadoCuentaDAO.consultaEstatusEstadoCuenta(estadoCuenta);
	}

	/**
	 * Consulta cuentas.
	 * @author FSW-Indra
	 * @param contrato the contrato
	 * @param formato the formato
	 * @param cuenta the cuenta
	 * @param descripcion the descripcion
	 * @return the list
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public List<CuentasDescargaBean> consultaCuentas(String contrato, String formato, String cuenta, String descripcion) throws BusinessException {
		EIGlobal.mensajePorTrace(String.format("%s Ejecutando servicio de consulta de cuentas para descarga...",LOG_TAG)
				, EIGlobal.NivelLog.DEBUG);
		if(contrato == null || formato == null) {
			EIGlobal.mensajePorTrace(String.format("%s Contrato/Formato no especificado...",LOG_TAG)
					, EIGlobal.NivelLog.ERROR);
			throw new BusinessException("EDOCTABO001 - Debe especificar un numero de contrato/formato de descarga...");
		}
		return estadoCuentaDAO.consultaCuentas(contrato, formato, cuenta, descripcion);
	}

	/**
	 * @author FSW-Indra
	 * @param cuenta Cuenta a Consultar
	 * @return the list
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public List<String> consultaPeriodosWS(String cuenta) throws BusinessException {
		/**	List<String> Periodos=new ArrayList<String>();
		return Periodos;*/
                return new ArrayList<String>();
	}

	/**
	 * Devuelve los 18 periodos anteriores.
	 * @author FSW-Indra
	 * @return La lista de los periodos
	 */
	public List<String> obtenPeriodosAnteriores(){
		List<String> periodosList=new ArrayList<String>();
		SimpleDateFormat sFormat2 = new SimpleDateFormat("yyyyMM", Locale.US);
		Calendar calendar = Calendar.getInstance();

		for(int i = 0  ; i < 18 ;i++ ) {
			calendar.add(Calendar.MONTH, -1);
			String ultPeriodo = sFormat2.format(calendar.getTime());
			String periodosRecientes=String.format("%s:::%s",ultPeriodo,
												String.format("%s-%s",estadoCuentaDAO.obtenerNombreCortoMes(Integer.valueOf(ultPeriodo.substring(4,6))),
																		ultPeriodo.substring(0,4)) );
			periodosList.add(periodosRecientes);
		}
		return periodosList;
	}

	/**
	 * @author FSW-Indra
	 * @param cuentasDescargaBean cuentas
	 * @return CuentasDescargaBean informacion de cuentas
	 * @throws SOAPExceptionImpl En caso de un error con la operacion
	 * @throws BusinessException manejo de excepcion
	 */
	public CuentasDescargaBean invocaCFDIPeriodsWS(CuentasDescargaBean cuentasDescargaBean) throws SOAPExceptionImpl, BusinessException {

			EIGlobal.mensajePorTrace(String.format("%s...Entra a metodo [InvocaCFDIPeriodsWS]", LOG_TAG), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(String.format("%sCuenta Seleccionada [%s]", LOG_TAG,cuentasDescargaBean.getCuenta()), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(String.format("%sTipo de XML Seleccionado [%s]", LOG_TAG,cuentasDescargaBean.getTipoXML()), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(String.format("%sTipo de Edc Seleccionado [%s]", LOG_TAG,cuentasDescargaBean.getTipoEdoCta()), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(String.format("%sInstancia PeriodsCFDIService_Service", LOG_TAG), EIGlobal.NivelLog.DEBUG);
			try {
				EIGlobal.mensajePorTrace(String.format("%s URL de Web Service: [%s]", LOG_TAG, Global.periodsCFDIServiceWsdlLocation), NivelLog.DEBUG);
				PeriodsCFDIService_Service service = new PeriodsCFDIService_Service();

				EIGlobal.mensajePorTrace(String.format("%s Recuperando el puerto del siguiente servicio %s",LOG_TAG,service)
						,EIGlobal.NivelLog.DEBUG);
				PeriodsCFDIService periodsCFDIService=service.getPeriodsCFDIServicePort();

				RequestWSPeriods requestWSPeriods=new RequestWSPeriods();
				requestWSPeriods.setCuentaFolioTDC(cuentasDescargaBean.getCuenta());
				requestWSPeriods.setIngresoEgreso(cuentasDescargaBean.getTipoXML());
				requestWSPeriods.setNumPeriodos(cuentasDescargaBean.getNumPeridos());
				requestWSPeriods.setSerie(cuentasDescargaBean.getSerie());
				requestWSPeriods.setTipoComprobante(WS_TIPO_COMPROBANTE);
				requestWSPeriods.setTipoCuenta(cuentasDescargaBean.getTipoEdoCta());
				ResponseWSPeriods responseWSPeriods=periodsCFDIService.consultPeriods(requestWSPeriods);




                EIGlobal.mensajePorTrace(String.format("%s NUMERO DE PERIODOS RECUPERADOS INTEGRAL [%s]<-------------",responseWSPeriods.getListaPeriodos().size(), LOG_TAG, Global.periodsCFDIServiceWsdlLocation), NivelLog.DEBUG);


		   //Si no se encontraron periodos en el integral se buscan en cartera
			if(responseWSPeriods.getListaPeriodos().size()==0 &&
				 requestWSPeriods.getCuentaFolioTDC().length()==11)
			 {
			        EIGlobal.mensajePorTrace(String.format("%s No existen periodos es tipo de cuenta Integral, ahora busca en Cr�ditos <-------------", LOG_TAG, Global.periodsCFDIServiceWsdlLocation), NivelLog.DEBUG);
			        requestWSPeriods.setTipoCuenta(WS_TIPO_CREDITO);
				  	responseWSPeriods=periodsCFDIService.consultPeriods(requestWSPeriods);
					EIGlobal.mensajePorTrace(String.format("%s NUMERO DE PERIODOS RECUPERADOS CARTERA [%s]<-------------",responseWSPeriods.getListaPeriodos().size(), LOG_TAG, Global.periodsCFDIServiceWsdlLocation), NivelLog.DEBUG);

			 }

		 EIGlobal.mensajePorTrace(String.format("%sRESULTADO CON TIPO DE CUENTA-->[%s]",LOG_TAG,requestWSPeriods.getTipoCuenta()),EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(String.format("%sBean Respuesta = %s " +
													   "\nCodigo Respuesta = %s " +
													   "\nDescripcion de la Respuesta = %s" +
													   "\nNumero de Periodos = %s.",LOG_TAG,responseWSPeriods,
																							responseWSPeriods.getCodigoRespuesta(),
																							responseWSPeriods.getDescripcionCodigo(),
																							responseWSPeriods.getListaPeriodos().size())
						,EIGlobal.NivelLog.DEBUG);
				cuentasDescargaBean.setCodigoRespuesta(responseWSPeriods.getCodigoRespuesta());
				cuentasDescargaBean.setDescripcionCodigo(responseWSPeriods.getDescripcionCodigo());
				cuentasDescargaBean.setNumPeridos(responseWSPeriods.getListaPeriodos().size());
				cuentasDescargaBean.setPeriodCFDI(responseWSPeriods.getListaPeriodos());



				for (PeriodCFDI periodCFDI : cuentasDescargaBean.getPeriodCFDI() ) {
					EIGlobal.mensajePorTrace(String.format("%s################################",LOG_TAG),EIGlobal.NivelLog.DEBUG);
				//	EIGlobal.mensajePorTrace(String.format("%sCuenta         => %s",LOG_TAG,periodCFDI.getCuentaFolioTDC()),EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(String.format("%sFolio UUID     => %s",LOG_TAG,periodCFDI.getFolioUUID()),EIGlobal.NivelLog.DEBUG);
				//	EIGlobal.mensajePorTrace(String.format("%sIngreso/Egreso => %s",LOG_TAG,periodCFDI.getIngresoEgreso()),EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(String.format("%sPeriodo        => %s",LOG_TAG,periodCFDI.getPeriodo()),EIGlobal.NivelLog.DEBUG);
				//	EIGlobal.mensajePorTrace(String.format("%sSerie          => %s",LOG_TAG,periodCFDI.getSerie()),EIGlobal.NivelLog.DEBUG);
				//	EIGlobal.mensajePorTrace(String.format("%sTipo Comprbante=> %s",LOG_TAG,periodCFDI.getTipoComprobante()),EIGlobal.NivelLog.DEBUG);
				}
			} catch (WebServiceException e) {
				EIGlobal.mensajePorTrace("DescargaEstadoCuentaBO - controlExcepcion() - "+ "Se controla detalle WebServiceException::: [" +new Formateador().formatea(e) + "]", EIGlobal.NivelLog.ERROR);
				//controlExcepcion();
                                 throw new BusinessException(e);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("DescargaEstadoCuentaBO - controlExcepcion() - "+ "Se controla detalle Exception::: [" +new Formateador().formatea(e) + "]", EIGlobal.NivelLog.ERROR);
				//controlExcepcion();
	                          throw new BusinessException(e);
			}

			return (cuentasDescargaBean);


	}

	/**
	 *
	 * @param cuentasDescargaBean Bean con las cuentas
	 * @author FSW-Indra
	 * @return respuesta
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public String[] invocaWebServicePDF(CuentasDescargaBean cuentasDescargaBean) throws BusinessException {
		String[] respuesta={"0","NA"};
		/** Creando instancia del WebService */
		try {
			EIGlobal.mensajePorTrace(String.format("%s URL de Web Service: [%s]", LOG_TAG, Global.validaExistenciaPDFWsdlLocation), NivelLog.DEBUG);
			ValidaExistenciaPDFService_Service service=new ValidaExistenciaPDFService_Service();
			EIGlobal.mensajePorTrace(String.format("%s Recuperando el puerto del servicio [%s]",LOG_TAG,service),
					NivelLog.INFO);
			ValidaExistenciaPDFService ports=service.getValidaExistenciaPDFServicePort();

			/** Llenado del Bean para envar al WebService **/
			RequestWS requestWS=new RequestWS();								//Cheques			//TDC
			requestWS.setAplicacion(cuentasDescargaBean.getTipoEdoCta()); 		//"001"				//"003"
			requestWS.setBranch(cuentasDescargaBean.getBranch()); 				//" "				//" "
			requestWS.setFolio(cuentasDescargaBean.getFolioSeleccionado());		//"00441348"		//"00000000"
			requestWS.setNumCliente(cuentasDescargaBean.getCodigoCliente());	//"00076329"		//"0"
			requestWS.setNumCuenta(cuentasDescargaBean.getCuenta());			//"60506542848"		//"5471460035081640"
			requestWS.setPais(cuentasDescargaBean.getPais());					//"MX"				//"MX"
			requestWS.setPeriodo(cuentasDescargaBean.getPeriodoSeleccionado());	//"201301"			//"201409"
			requestWS.setTipo(cuentasDescargaBean.getTipo());					//"00000001"		//"00000003"
			EIGlobal.mensajePorTrace(String.format("%s Llenado del Bean para envar al WS [%s]",LOG_TAG,requestWS.toString()),NivelLog.INFO);
			EIGlobal.mensajePorTrace(String.format("%s Aplicacion [%s]\nFolio [%s]\nNumCte [%s]\nNumTcta [%s]\nPais [%s]\nPeriodo [%s]\nTipo [%s]\n",
					LOG_TAG,requestWS.getAplicacion(),requestWS.getFolio(),requestWS.getNumCliente(),requestWS.getNumCuenta(),
					requestWS.getPais(),requestWS.getPeriodo(),requestWS.getTipo()),NivelLog.DEBUG);


			/** Llamando al WebService **/
			ResponseWS responseWS;
			try {
				responseWS = ports.validaExistencia(requestWS);
				EIGlobal.mensajePorTrace(String.format("%s Llamando al WS [%s]",LOG_TAG,responseWS.toString()),NivelLog.INFO);
				respuesta[0]=String.valueOf(responseWS.getCodigoRespuesta());
				respuesta[1]=responseWS.getDescripcionCodigo();
			} catch (mx.altec.enlace.cliente.ws.pdfondemand.SOAPExceptionImpl e) {
				EIGlobal.mensajePorTrace(String.format("%s BusinessException [%s]",LOG_TAG,e.getMessage()), NivelLog.INFO);
				/** throw new BusinessException(e.getMessage()); **/
			}
		} catch (WebServiceException e) {
			EIGlobal.mensajePorTrace(String.format("El Web Service definido [%s] no responde a la solicitud, se lanza excepcion controlada de negocio", Global.validaExistenciaPDFWsdlLocation), EIGlobal.NivelLog.DEBUG);
			throw new WebServiceException();
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(String.format("Ocurrio un error inesperado al consumir el Web Service [%s], se lanza excepcion controlada de negocio", Global.validaExistenciaPDFWsdlLocation), EIGlobal.NivelLog.DEBUG);
			throw new WebServiceException();
		}
		return respuesta;
	}

	/**
	 * Metodo para rellenar a la derecha
	 * @author FSW-Indra
	 * @param string cadena
	 * @param length longitud
	 * @param padString caracter que reemplazara
	 * @return String cadena modificada
	 */
	public String rPad(String string,int length,String padString) {
	    return (string==null) ? ""
	    		:(string.length()<=length) ? String.format("%-"+length+"s",string).replace(" ", padString)
	    				:string.substring(0,length);
	}
	/**
	 * Metodo para rellenar a la izquierda
	 * @author FSW-Indra
	 * @param string cadena
	 * @param length longitud
	 * @param padString caracter que reemplazara
	 * @return String cadena modificada
	 */
	public String lPad(String string,int length,String padString) {
	    return (string==null) ? ""
	    		:(string.length()<=length) ? String.format("%"+length+"s",string).replace(" ", padString)
	    				:string.substring(0,length);
	}

	/**
	 * Metodo que inoca el DAO para insertar en BD la informacion del edc que se va a descargar
	 * @author FSW-Indra
	 * @param cuentasDescargaBean Cuentas Bean
	 * @return boolean cargado
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public boolean insertaRegistroDescarga(CuentasDescargaBean cuentasDescargaBean) throws BusinessException {
		boolean cargado=false;
		switch (cuentasDescargaBean.getFormato()) {
			case 1 :
				if (estadoCuentaDAO.insertaRegistroDescargaPDF(cuentasDescargaBean)>0){
					cargado=true;
				}else {
					cargado=false;
				}
				break;
			case 2 :
				if (estadoCuentaDAO.insertaRegistroDescarga(cuentasDescargaBean)>0){
					cargado=true;
				}else {
					cargado=false;
				}
				break;
			default : throw new BusinessException(String.format("Formato incorrecto %s",cuentasDescargaBean.getFormato()));
		}
		return cargado;
	}

	/**
	 * Genera llave de 40 posiciones para usarse en el brinco.
	 * @author FSW-Indra
	 * @return key
	 */
	public String genKey() {
		int size=40, bytes=200;
		String key=new BigInteger(bytes,random).toString(32).toUpperCase();
		if (key.length()==size) {
			return key;
		}else if (key.length()<size){
				return key.concat(new BigInteger(bytes,random).toString(32).toUpperCase()).substring(0,size);
		}else {
			return key.substring(0,size);
		}
	}

	/**
	 * Obtencion de la informacion del cliente
	 * @author FSW-Indra
	 * @param numeroCuenta Cuenta de la que se desea obtener la informacion
	 * @return ejecucion de la transaccion ODD4
	 * @throws BusinessException En caso de que exista un error en la consulta
	 */
	public ODD4Bean obtenerInformacionCliente(String numeroCuenta)
	throws BusinessException {
		final ODD4Bean odd4Bean = new ODD4Bean();
		EIGlobal.mensajePorTrace(String.format("%s NCta --> %s",LOG_TAG,numeroCuenta), NivelLog.DEBUG);
		if(numeroCuenta.indexOf("BME") == 0){
			numeroCuenta = String.format("B%s", numeroCuenta.substring(3));
		}
		try {
			odd4Bean.setNumeroCuenta(numeroCuenta);
			EIGlobal.mensajePorTrace("NumeroCuentas ["+odd4Bean.getNumeroCuenta()+"] ",EIGlobal.NivelLog.DEBUG);
		} catch (ValidationException e) {
			EIGlobal.mensajePorTrace("Error "+e.toString(), NivelLog.DEBUG);
		}
		return txODD4.consultarODD4(odd4Bean);
	}

	/**
	 * Obtencion de los periodos a traves de la transaccion OW42
	 * @author FSW-Indra
	 * @param numeroCuenta Cuenta de la que se van a obtener los estados de cuenta
	 * @param numeroCliente cliente del que se van a obtener los estados de cuenta
	 * @return Objeto que contiene los periodos de la cuenta
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public OW42Bean obtenerPeriodos(String numeroCuenta,String numeroCliente) throws BusinessException  {
			return txOW42.consultarOW42(numeroCuenta,numeroCliente);
	}

	/**
	 * Bitacoriza Admin para Descarga Estado de Cuetna PDF
	 * @author FSW-Indra
	 * @param request : request
	 * @param httpSession : httpSession
	 * @param ipoOP : ipoOP
	 * @param IdTab : IdTab
	 * @param Tx : Tx
	 * @param bitaConstant : bitaConstant
	 * @param token : token
	 */
	public void bitaAdmin(HttpServletRequest request, HttpSession httpSession,
			String ipoOP, String IdTab, String Tx, String bitaConstant,boolean token){
		BitaAdminBean bean = new BitaAdminBean();
		bean.setTipoOp(ipoOP);
		bean.setIdTabla(IdTab);
		bean.setServTransTux(Tx);
		bean.setReferencia(100000);
		BitacoraBO.bitacorizaAdmin(request, httpSession, bitaConstant, bean, token);
	}

	/**
	 * Agrega los datos comunes de las paginas
	 * @author FSW-Indra
	 * @param request Objeto con la informacion de la peticion
	 * @param session Objeto con los datos de la sesion
	 */
	public void agregarDatosComunes(HttpServletRequest request, BaseResource session){
		EIGlobal.mensajePorTrace(
				String.format("%s public void agregarDatosComunes", LOG_TAG), NivelLog.DEBUG);
		request.setAttribute("NumContrato",(session.getContractNumber() == null) ? "" : session.getContractNumber());
		request.setAttribute("NomContrato",(session.getNombreContrato() == null) ? "" : session.getNombreContrato());
		request.setAttribute("NumUsuario",(session.getUserID8() == null) ? "" : session.getUserID8());
		request.setAttribute("NomUsuario",(session.getNombreUsuario() == null) ? "" : session.getNombreUsuario());
		EIGlobal.mensajePorTrace(String.format("%s Setting Attributes NumContrato[%s] NomContrato[%s] NumUsuario,[%s] NomUsuario[%s]",
				LOG_TAG,request.getAttribute("NumContrato"),request.getAttribute("NomContrato"),
				request.getAttribute("NumUsuario"),request.getAttribute("NomUsuario")),
				NivelLog.DEBUG);
	}

	/**
	 * valida la cuenta del usuario
	 * @author FSW-Indra
	 * @param numeroCuenta Numero de la cuenta
	 * @param request Objeto con la informacion de la peticion
	 * @return false or true
	 * @throws BusinessException En caso de un error en la operacion
	 */
	public boolean validaCuenta(String numeroCuenta, HttpServletRequest request) throws BusinessException  {
		EIGlobal.mensajePorTrace(String.format("%s >>>>> Inicia validaCuenta <<<<<",LOG_TAG), NivelLog.DEBUG);
		//final EstadoCuentaHistoricoBO estadoCuentaBO = new EstadoCuentaHistoricoBO();
		final HttpSession httpSession = request.getSession();

			if(numeroCuenta == null){
				throw new BusinessException("No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el N&uacute;mero de Cuenta.");
			}
			List<CuentaODD4Bean> listaOdd4Bean = (List<CuentaODD4Bean>) httpSession.getAttribute("cuentasRelacionadas");
			for(CuentaODD4Bean bean : listaOdd4Bean){
				String cuenta1 = estadoCuentaDAO.formatCuenta(bean.getNumeroCuenta().trim());
				String cuenta2 = numeroCuenta;
				EIGlobal.mensajePorTrace(String.format("%s Compara %s equals %s",LOG_TAG,cuenta1,cuenta2), NivelLog.DEBUG);
				if(cuenta1.equalsIgnoreCase(cuenta2)){
					EIGlobal.mensajePorTrace(String.format("%s MATCH %s = %s",LOG_TAG,cuenta1,cuenta2), NivelLog.DEBUG);
					return true;
				}
			}

		return false;
	}

	/**
	 * Metodo controlExcepcion que controla cuando se presenta una posible excepcion
	 */
	public void controlExcepcion() {
	    try {
	    	  throw new BusinessException(String.format("El Web Service [%s] no responde a la solicitud ", Global.periodsCFDIServiceWsdlLocation));
	    } catch (BusinessException e) {
	    	EIGlobal.mensajePorTrace("DescargaEstadoCuentaBO - controlExcepcion() - "+ "Se controla detalle BusinessException::: [" +new Formateador().formatea(e) + "]", EIGlobal.NivelLog.ERROR);
	    }
	  }

	/**
	 * Metodo que obtiene los datos para Tarjeta de Credito y llena el bean de descarga
	 * @author FSW-Indra
	 * @param numTarjeta numero de tarjeta
	 * @return DescargaEDCBean bean con informacion
	 * @throws BusinessException excepcion a manejar
	 */
	public DescargaEDCBean obtenDatosTDC(String numTarjeta) throws BusinessException {
		final DescargaEDCBean edcBean = new DescargaEDCBean();
		String contrato20Pos="";

		edcBean.setNumTarjeta(numTarjeta);
		/** Invoca Trx LMM8**/
		TrxLMM8BO lmm8 = new TrxLMM8BO();
		lmm8.setNumeroTarjeta(edcBean.getNumTarjeta());
		lmm8.ejecuta();
		EIGlobal.mensajePorTrace(
				String.format("%s Se ejecuta Trx LMM8 NumCte[%s] CodPart[%s] OrdenPart[%s] Ent[%s] Suc[%s] CtaCheq[%s] CodStat[%s] MsgAviso[%s]",
				LOG_TAG,
				lmm8.getNumeroCliente(),
				lmm8.getCodigoParticipa(),
				lmm8.getOrdenParticipa(),
				lmm8.getEntidad(),
				lmm8.getSucursalCtaChq(),
				lmm8.getCuentaCheques(),
				lmm8.getCodStatus(),
				lmm8.getMsgStatus()), NivelLog.INFO);
		edcBean.setNumCliente(lmm8.getNumeroCliente());
		edcBean.setCodParticipante(lmm8.getCodigoParticipa());
		if ( !"TI".equalsIgnoreCase(edcBean.getCodParticipante()) ) {
			throw new BusinessException("Estimado cliente, la descarga de Estados de Cuenta solo se puede efectuar para las tarjetas de Titulares");
		}
		if ( lmm8.getMsgStatus()!=null || "".equals(lmm8.getMsgStatus()) ) {
			throw new BusinessException(String.format("Error durante llamado a la Transaccion LMM8 [%s]",lmm8.getMsgStatus()));
		}

		/** Invoca Trx MPB8 **/
		TrxMPB8BO mpb8 = new TrxMPB8BO();
		mpb8.setPanTarjeta(edcBean.getNumTarjeta());
		mpb8.ejecuta();
		if (mpb8.getMsgStatus().indexOf("MPB80000") == -1) {
			throw new BusinessException(String.format("Error durante llamado a la Transaccion MPB8 [%s]",mpb8.getMsgStatus()));
		}
		edcBean.setCodEntidad((mpb8.getCodigoEntidad()!=null || "null".equalsIgnoreCase(mpb8.getCodigoEntidad()))
				? mpb8.getCodigoEntidad() : lPad("",4,"0") );
		edcBean.setCentAlta((mpb8.getCentroAltaCtaTarjeta()!=null || "null".equalsIgnoreCase(mpb8.getCentroAltaCtaTarjeta()))
				? mpb8.getCentroAltaCtaTarjeta() : lPad("",4,"0") );
		edcBean.setNumCuenta((mpb8.getCuentaTarjeta()!=null || "null".equalsIgnoreCase(mpb8.getCuentaTarjeta()))
				? mpb8.getCuentaTarjeta() : lPad("",12,"0") );
		EIGlobal.mensajePorTrace(String.format("%s TrxMPB8 PAN[%s] CodEnt[%s] CentAlt[%s] NumCta[%s] CodEst[%s] MsgStat [%s]",
				LOG_TAG,
				edcBean.getNumTarjeta(),
				edcBean.getCodEntidad(),
				edcBean.getCentAlta(),
				edcBean.getNumCuenta(),
				mpb8.getCodStatus(),
				mpb8.getMsgStatus()), NivelLog.INFO);

		/** Invoca Trx PE47 **/
		TrxPE47BO pe47 = new TrxPE47BO();
		pe47.setNumeroCliente(edcBean.getNumCliente());
		pe47.ejecuta();
		if (pe47.getMsgStatus().indexOf("PE470000") == -1) {
			throw new BusinessException(String.format("Error durante llamado a la Transaccion PE47 [%s]",pe47.getMsgStatus()));
		}
		edcBean.setPrimerApellido( (pe47.getApellidoPaterno()!=null) ? pe47.getApellidoPaterno().trim() : "" );
		edcBean.setSegundoApellido( (pe47.getApellidoMaterno()!=null) ? pe47.getApellidoMaterno().trim() : "" );
		edcBean.setNombrePersona((pe47.getNombreTitular()!=null) ? pe47.getNombreTitular().trim() : "" );
		edcBean.setTipoPersona(pe47.getTipoPersona());
		EIGlobal.mensajePorTrace(String.format("%s TrxPE47 PrimerApellido[%s] SegundoApellido[%s] NombrePersona[%s] TipoPersona[%s] CodEst[%s] MsgStat [%s]",
				LOG_TAG,
				edcBean.getPrimerApellido(),
				edcBean.getSegundoApellido(),
				edcBean.getNombrePersona(),
				edcBean.getTipoPersona(),
				pe47.getCodStatus(),
				pe47.getMsgStatus()),NivelLog.INFO);
		boolean bandera = true;
		/** Invoca Trx ODB4 **/
		TrxODB4BO odb4=new TrxODB4BO();
		odb4.setNumeroCliente(edcBean.getNumCliente());
		List<TrxODB4Bean> odb4List=odb4.ejecuta();
		/*if (odb4.getCodStatus() != 0) {
			throw new BusinessException(String.format("Error durante llamado a la Transaccion ODB4 [%s]",odb4.getMsgStatus()));
		}*/
		for (TrxODB4Bean trx : odb4List){
			EIGlobal.mensajePorTrace(LOG_TAG+" Reccorriendo domicilios Tarjeta["+
					edcBean.getNumTarjeta()+"]==["+trx.getNumeroTdc()+"] && Cuenta["+
					edcBean.getNumCuenta()+"]=["+trx.getNumCuenta()+"]", NivelLog.DEBUG);
			if (edcBean.getNumTarjeta().equals(trx.getNumeroTdc())
					&& edcBean.getNumCuenta().equals(trx.getNumCuenta())&&bandera) {
				EIGlobal.mensajePorTrace(LOG_TAG+"Hubo una coincidencia se carga Informacion al bean", NivelLog.DEBUG);
				edcBean.setCalle(trx.getCalleYNumero());
				edcBean.setLocalidad(trx.getDescLocalidad());
				edcBean.setComunidad(trx.getDescComunidad());
				edcBean.setCodPostal(trx.getCodigoPostal());
				bandera=false;
			}
		}

		contrato20Pos=String.format("%s%s%s",edcBean.getCodEntidad(),edcBean.getCentAlta(),edcBean.getNumCuenta());
		EIGlobal.mensajePorTrace(String.format("%s Obten Tarjetas Relacionadas al contrato [%s]",LOG_TAG,contrato20Pos), NivelLog.DEBUG);
		TrxMPPCBO mppc = new TrxMPPCBO();
		mppc.consultaTarjetas(contrato20Pos);
		edcBean.setTarjetasRel(mppc.getTarjetas());

		return edcBean;
	}

}