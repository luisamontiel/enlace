package mx.altec.enlace.bo;

/**
 *  Clase bean para manejar el procedimiento para la transaccion GPF2
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 27, 2011
 */
public class TrxGPF2VO {
	
	/**
	 * Id del banco corresponsal
	 */
	private String OIdBaCo = null;
	
	/**
	 * Descripcion Corresponsal
	 */
	private String ODesCor = null;
	
	/**
	 * C�digo del Pa�s
	 */
	private String OCtry   = null;
	
	/**
	 * Nombre del Pa�s
	 */
	private String ODesCtry = null;
	
	/**
	 * Nombre de la ciudad
	 */
	private String OTwn = null;

	/**
	 * Obtener el valor de OIdBaCo.
     *
     * @return OIdBaCo valor asignado.
	 */
	public String getOIdBaCo() {
		return OIdBaCo;
	}

	/**
     * Asignar un valor a OIdBaCo.
     *
     * @param OIdBaCo Valor a asignar.
     */
	public void setOIdBaCo(String idBaCo) {
		OIdBaCo = idBaCo;
	}

	/**
	 * Obtener el valor de ODesCor.
     *
     * @return ODesCor valor asignado.
	 */
	public String getODesCor() {
		return ODesCor;
	}

	/**
     * Asignar un valor a ODesCor.
     *
     * @param ODesCor Valor a asignar.
     */
	public void setODesCor(String desCor) {
		ODesCor = desCor;
	}

	/**
	 * Obtener el valor de OCtry.
     *
     * @return OCtry valor asignado.
	 */
	public String getOCtry() {
		return OCtry;
	}

	/**
     * Asignar un valor a OCtry.
     *
     * @param OCtry Valor a asignar.
     */
	public void setOCtry(String ctry) {
		OCtry = ctry;
	}

	/**
	 * Obtener el valor de ODesCtry.
     *
     * @return ODesCtry valor asignado.
	 */
	public String getODesCtry() {
		return ODesCtry;
	}

	/**
     * Asignar un valor a ODesCtry.
     *
     * @param ODesCtry Valor a asignar.
     */
	public void setODesCtry(String desCtry) {
		ODesCtry = desCtry;
	}

	/**
	 * Obtener el valor de OTwn.
     *
     * @return OTwn valor asignado.
	 */
	public String getOTwn() {
		return OTwn;
	}

	/**
     * Asignar un valor a OTwn.
     *
     * @param OTwn Valor a asignar.
     */
	public void setOTwn(String twn) {
		OTwn = twn;
	}
	
	/**
	 * Override toString
	 */
	public String toString() {
		StringBuffer registro = new StringBuffer();		
		registro.append("OIdBaCo->" + getOIdBaCo());
		registro.append("ODesCor->" + getODesCor());
		registro.append("OCtry->" + getOCtry());
		registro.append("ODesCtry->" + getODesCtry());
		registro.append("OTwn->" + getOTwn());	
		return registro.toString();
	}
	
}
