package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;

import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class consDoc
{

    /** Servico de Tuxedo */
    protected ServicioTux consDoc;
    /** Numero de contrato */
    protected String Contrato;
    /** Clave de Usuario */
    protected String Usuario;
    /** Perfil del usuario */
    protected String Perfil;

    /** Creates new pdconsDoc */
    public consDoc() {
        consDoc = new ServicioTux();
    }

    /**
     * Metodo para obtener el servicio de Tuxedo
     */
    public ServicioTux getServicioTux() {return consDoc;}

    protected File recibe(String Nombre) {
        try {
        	EIGlobal.mensajePorTrace("consDoc:recibe - Inicia", EIGlobal.NivelLog.INFO);
        	//IF PROYECTO ATBIA1 (NAS) FASE II
        	Properties rs = new Properties();
        	FileInputStream in = new FileInputStream(Global.DIRECTORIO_LOCAL+"/1001851.conca");
            rs.load(in);
           //IF PROYECTO ATBIA1 (NAS) FASE II
            
           	boolean Respuestal = true;
            ArchivoRemoto recibeArch = new ArchivoRemoto();
           
           	if(!recibeArch.copiaCUENTAS("/" + Nombre, rs.getProperty("DIRECTORIO_LOCAL"))){
				
					EIGlobal.mensajePorTrace("*** consDoc.recibe  No se realizo la copia remota:" + Nombre, EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** consDoc.recibe archivo remoto copiado exitosamente:" + Nombre, EIGlobal.NivelLog.DEBUG);
				    
				}
         
         
        	if (Respuestal) {
                File Archivo = new File(
                		Nombre);
                return Archivo;  
           	}
     	
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return null;
    }


    public String envia_atux(String Trama) {
        try {
            //String Trama = "2EWEB|" + Usuario + "|CFAE|" + Contrato + "|"
            //                + Usuario + "|" + Perfil + "|" + "2@" + 04/03/2002 +
			//				"@" + 10/05/2002 + "@|";
            File ArchivoResp;
            Hashtable ht = consDoc.web_red(Trama);
            String Buffer = (String) ht.get("BUFFER");
            ArchivoResp = recibe(Buffer);
            if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "actual"));
            return (ArchivoResp.getPath());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error al leer el archivo de respuesta";
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error con la comunicacion";
        }
    }

    public void setContrato (String contrato) {Contrato = contrato;}
    public void setUsuario (String usuario) {Usuario = usuario;}
    public void setPerfil (String perfil) {Perfil = perfil;}
}