package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import mx.altec.enlace.beans.AdmTrxPE80;
import mx.altec.enlace.dao.TrxPE80DAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;
import static mx.altec.enlace.utilerias.NomPreUtil.*;

/**
 *  Clase Bussines para manejar el procedimiento para la transaccion PE80
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Sep 22, 2010
 */
public class TrxPE80BO {

	/**
	 * Objeto contenedor de tipo AdmTrxPE80
	 */
	private transient AdmTrxPE80 bean;
	/** constante para Linea de truene-> */
	private transient final String LINEA_TRUENE = "Linea de truene->";
	/** constante para Cuenta-> */
	private transient final String CUENTA = "Cuenta->";
	/** constante para <DATOS GENERALES> */
	private transient final String DATOS_GENERALES = "<DATOS GENERALES>";

	/**
	 * Obtener el valor de bean.
     *
     * @return bean valor asignado.
	 */
	public AdmTrxPE80 getBean() {
		return bean;
	}	
	
	/**
	 * Metodo para obtener la razon social de ejecutar la transaccion PE80
	 * 
	 * @param numCuenta			Numero de cuenta
	 * @return razonSocial		Razon Social
	 */
	public String[] obtenTitularCtaCargo(String numCuenta) {
		EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Entrando....", EIGlobal.NivelLog.DEBUG);		

		TrxPE80DAO dao = new TrxPE80DAO();
		TrxPE80VO beanRes = new TrxPE80VO();
	    MQQueueSession mqsess = null;
	    bean = null;
	    String[] titularCuenta = new String[4]; //INDRA-SWIFT
	    String razonSocial = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, 
	    									NP_JNDI_QUEUE_RESPONSE, 
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	bean = dao.intervinientesContrato(rellenar("",8,' ','D'), 
	    									  rellenar("",4,' ','D'),
	    									  rellenar("",4,' ','D'),
	    									  rellenar(numCuenta,12,' ','D'),
	    									  rellenar("",2,' ','D'),
	    									  rellenar("",4,' ','D'),
	    									  rellenar("",2,' ','D'),
	    									  rellenar("",3,'0','I'),
	    									  rellenar("",1,' ','D'));
	    	EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Codigo operacion>>" 
	    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
	    	beanRes = bean.getPe80bean();
	    	EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Nombre>>" 
	    			+ beanRes.getPeNomPer(), EIGlobal.NivelLog.DEBUG);
	    	EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Apellido Paterno>>" 
	    			+ beanRes.getPePriApe(), EIGlobal.NivelLog.DEBUG);	    	
	    	EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Apellido Materno>>" 
	    			+ beanRes.getPeSegApe(), EIGlobal.NivelLog.DEBUG);	    	
	    		titularCuenta[0] = beanRes.getPeNomPer().trim();
	    		titularCuenta[1] = beanRes.getPePriApe().trim();
	    		titularCuenta[2] = beanRes.getPeSegApe().trim();
	    		titularCuenta[3] = beanRes.getPeNumPer().trim(); //INDRA-SWIFT
	    }catch (Exception e){	    	
	    	titularCuenta[0] = "";
    		titularCuenta[1] = "";
    		titularCuenta[2] = ""; 
    		titularCuenta[3] = "";
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenRazonSocial...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ DATOS_GENERALES
		               				+ CUENTA + numCuenta
						 			+ LINEA_TRUENE + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ DATOS_GENERALES
			               				+ CUENTA + numCuenta
							 			+ LINEA_TRUENE + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }					
		EIGlobal.mensajePorTrace("TrxPE80BO::obtenRazonSocial:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return titularCuenta;
	}
	
	/**
	 * Metodo para obtener la razon social de ejecutar la transaccion PE80
	 * 
	 * @param numCuenta			Numero de cuenta
	 * @return razonSocial		Razon Social
	 */
	public String obtenRazonSocial(String numCuenta) {
		EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Entrando....", EIGlobal.NivelLog.DEBUG);		

		TrxPE80DAO dao = new TrxPE80DAO();
		TrxPE80VO beanRes = new TrxPE80VO();
	    MQQueueSession mqsess = null;
	    bean = null;
	    String[] titularCuenta = new String[3];
	    String razonSocial = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, 
	    									NP_JNDI_QUEUE_RESPONSE, 
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	bean = dao.intervinientesContrato(rellenar("",8,' ','D'), 
	    									  rellenar("",4,' ','D'),
	    									  rellenar("",4,' ','D'),
	    									  rellenar(numCuenta,12,' ','D'),
	    									  rellenar("",2,' ','D'),
	    									  rellenar("",4,' ','D'),
	    									  rellenar("",2,' ','D'),
	    									  rellenar("",3,'0','I'),
	    									  rellenar("",1,' ','D'));
	    	EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Codigo operacion>>" 
	    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
	    	beanRes = bean.getPe80bean();
	    	EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: Valor esperado>>" 
	    			+ beanRes.getPeNomPer(), EIGlobal.NivelLog.DEBUG);
	    	if("F".equals(beanRes.getPeTipPer().trim())){
	    		titularCuenta[0] = beanRes.getPeNomPer().trim();
	    		titularCuenta[1] = beanRes.getPePriApe().trim();
	    		titularCuenta[2] = beanRes.getPeSegApe().trim(); 
	    	}else {
	    		titularCuenta[0] = beanRes.getPeNomPer().trim();
	    		titularCuenta[1] = "";
	    		titularCuenta[2] = ""; 
	    	}
	    	razonSocial = titularCuenta[0] + " " + titularCuenta[1] + " " + titularCuenta[2];
	    	    	
	    }catch (Exception e){	    	
	    	titularCuenta[0] = "";
    		titularCuenta[1] = "";
    		titularCuenta[2] = ""; 
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenRazonSocial...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ DATOS_GENERALES
		               				+ CUENTA + numCuenta
						 			+ LINEA_TRUENE + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxPE80BO::obtenTitularCtaCargo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ DATOS_GENERALES
			               				+ CUENTA + numCuenta
							 			+ LINEA_TRUENE + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }					
		EIGlobal.mensajePorTrace("TrxPE80BO::obtenRazonSocial:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return razonSocial;
	}

}