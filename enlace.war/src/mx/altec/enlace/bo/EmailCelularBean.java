package mx.altec.enlace.bo;

public class EmailCelularBean {

	private String lada = "";
	private String noCelular = "";
	private String carrier = "";
	private String correo = "";
	private String timeStampCel = "";
	private String timeStampMail = "";
	private String estatus = "";
	
	
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getLada() {
		return lada;
	}
	public void setLada(String lada) {
		this.lada = lada;
	}
	public String getNoCelular() {
		return noCelular;
	}
	public void setNoCelular(String noCelular) {
		this.noCelular = noCelular;
	}
	public String getTimeStampCel() {
		return timeStampCel;
	}
	public void setTimeStampCel(String timeStampCel) {
		this.timeStampCel = timeStampCel;
	}
	public String getTimeStampMail() {
		return timeStampMail;
	}
	public void setTimeStampMail(String timeStampMail) {
		this.timeStampMail = timeStampMail;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
}
