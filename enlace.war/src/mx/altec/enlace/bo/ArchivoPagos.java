package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;
import java.lang.*;
import java.io.*;
import java.text.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;

import mx.altec.enlace.dao.CombosConf;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class ArchivoPagos
{
	//Declaracion de variables
	public String tipoRegistro = "", NumeroSecuencia = "", ClaveProveedor="", Proveedor="",
	TipoDocumento="",NumeroDocumento="",ImporteDocumento="",AsociadoFactura="",Importe="",
	FechaEmision="", FechaVencimiento="",Naturaleza="",Estatus="",Observaciones="",ClaveDocumento="";
	public String NoAlcanza ="";
    public StringBuffer cadenaValidacion   = new StringBuffer ("");
	public int totalRegistros        = 0;
	public String archivoSalida      = "", lista_Pagos = "",lista_claves="",NotaRechazada = "",NotasDuplicadas="",FacturasDuplicadas="";
	int contadorErrores              = 1;
	public String asFechaTransmision = "", asTotalRegistros = "", asAceptados = "", asRechazados = "", asFechaActualizacion = "";
	RandomAccessFile archivoTrabajo=null, ArchivoFinal=null;
	long posicionReg                 = 0;
	private BaseServlet BaseAppLogicEmp;
    private String Banco ="";
	private String Archivo1 ="";
	protected String Contrato;
    protected String Usuario;
    protected String Perfil;
	com.kivasoft.IContext Contexto;
	public float ImporteArchivo,SaldoArchivo;
	public int contadorRecuperado=0,RecuperadoR=0,RecuperadoA=0, tamanioImportado=0, ImportadoR=0, ImportadoA=0,ErroresImportacion=0;

    //Constructor
	public ArchivoPagos( String rutaArchivo,BaseServlet BApp,String banco )
	    {
		 BaseAppLogicEmp = BApp;
		 Banco=banco;
		 Archivo1 = rutaArchivo;
		 archivoSalida=IEnlace.DOWNLOAD_PATH+rutaArchivo.substring(rutaArchivo.lastIndexOf("/")+1);
		 try
			{
                archivoTrabajo = new RandomAccessFile(rutaArchivo,"rw");
			}
		catch(IllegalArgumentException e)
			{
				EIGlobal.mensajePorTrace("1: Error al abrir el Archivo: " + e, EIGlobal.NivelLog.INFO);
			}
		catch(FileNotFoundException e)
			{
				EIGlobal.mensajePorTrace("2: Error al abrir el Archivo: " + e, EIGlobal.NivelLog.INFO);
			}
		catch(SecurityException e)
			{
				EIGlobal.mensajePorTrace("3: Error al abrir el Archivo: " + e, EIGlobal.NivelLog.INFO);
			}
		catch(Exception e)
			{
				EIGlobal.mensajePorTrace("4: Error al abrir el Archivo: " + 1, EIGlobal.NivelLog.INFO);
			}
		}
	//Lectura y construccion de tabla HTML
	public String lecturaArchivo()
	{
		String lecturaReg = "", encabezado = "";
		StringBuffer strTabla   = new StringBuffer ("");
		int contador      = 0;
		strTabla = new StringBuffer ("<table width=1250 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
		strTabla.append ("<tr><td colspan=10 class=textabref>Registro de Pagos</td></tr>");
		strTabla.append ("<tr><td class=tittabdat align=center>&nbsp;</td><td class=tittabdat align=center>Clave de prov.</td>");
		strTabla.append ("<td class=tittabdat align=center>Nombre o Raz&oacute;n Social</td><td class=tittabdat align=center>Tipo de docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>N&uacute;mero de docto.</td><td class=tittabdat align=center>Importe de docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>Factura asociada</td><td class=tittabdat align=center>Importe a pagar</td>");
		strTabla.append ("<td class=tittabdat align=center>Fecha de emisi&oacute;n</td><td class=tittabdat align=center>Fecha Vencimiento</td>");
        strTabla.append ("<td class=tittabdat align=center>Estatus</td>"+"<td class=tittabdat align=center>Observaciones</td></tr>");
        try
			{
				archivoTrabajo.seek(0);
				encabezado = archivoTrabajo.readLine();
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}

		if (encabezado!=null)
			{
				do
					{
						try
							{
								posicionReg = archivoTrabajo.getFilePointer();
								lecturaReg  = archivoTrabajo.readLine();
							}
						catch(Exception e)
							{
  							    EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
								return "Error 3: "+ e.getMessage();
							}
						EIGlobal.mensajePorTrace("lecturaReg ="+lecturaReg, EIGlobal.NivelLog.DEBUG);
						if (lecturaReg!=null)
							{
								if ((lecturaReg.substring(0,1).equals("2"))&&(!lecturaReg.substring(1,2).equals("3")))
									{
										lecturaCampos(lecturaReg);
    									strTabla=new StringBuffer (strTabla + creaRegTabla(contador));
										contador++;
									}
    						}
					} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try
					{
						archivoTrabajo.close();
					}
				catch(IOException e)
					{
						EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
						return "Error 4: "+ e.getMessage();
					}

			}
			strTabla = new StringBuffer (strTabla+"</table>");
		    return strTabla.toString();
	}



 public String  asigno( String Clave) {
		String tipo="9";
		String NombreProveedor = new String();
		CombosConf archivo1 = new CombosConf();
		archivo1.setUsuario (Usuario);
		archivo1.setContrato (Contrato);
		archivo1.setPerfil (Perfil);
		//archivo1.getServicioTux ().setContext (Contexto);
		String Archivo = archivo1.envia_tux();
		String clave = Clave;
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		Vector lista = new Vector();
		RandomAccessFile fileAmbiente=null;

		try
		{
			File archivo=new File(Archivo);
			File Renombrado = new File(Global.DOWNLOAD_PATH+"confirming.pag");
			if (archivo.exists())
				{
				archivo.renameTo(Renombrado);
				}
			}
		catch(Exception e)
			{
				EIGlobal.mensajePorTrace("error en el objeto File(Neteo): " + e, EIGlobal.NivelLog.INFO);
			}


		try {
			fileAmbiente= new RandomAccessFile(Global.DOWNLOAD_PATH +"confirming.pag","r");
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace( "Error al leer el archivo de ambiente, e1 " + e, EIGlobal.NivelLog.INFO);
		}

	do {
			try {
				registroLeido = fileAmbiente.readLine();
				} catch ( IOException e ) {
				EIGlobal.mensajePorTrace( "Error al leer el archivo de ambiente, e2 " + e, EIGlobal.NivelLog.INFO);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		try {
			do {
				if ( registroLeido.startsWith( tipo ) ) {
					if (tipo != "9") {
						    campo =	registroLeido.substring(
							registroLeido.indexOf( ';' , 2 ) + 1 ,
							registroLeido.lastIndexOf( ';' ) );
						    lista.add(campo);
						} else {
						campo = registroLeido.substring(4,
                                registroLeido.indexOf( ';', 6));
                        if (campo.equals(clave))
						{
							campo = registroLeido.substring(
							registroLeido.indexOf( ';' , 12 ) + 1 ,
							registroLeido.lastIndexOf( ';' ) - 2);
						    campo = campo.replace(';',' ') ;
							NombreProveedor = campo;
   						}
							lista.add(campo);
					}
				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				EIGlobal.mensajePorTrace( "Error al leer los registros del archivo de ambiente", EIGlobal.NivelLog.ERROR);
		}
		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace( "Error al cerrar el archivo de ambiente", EIGlobal.NivelLog.ERROR);
		}
		return NombreProveedor;
	}


	public void lecturaCampos(String registro)
		{
		tipoRegistro 	  = registro.substring(0,1).trim();
		TipoDocumento     = registro.substring(1,2).trim();
		ClaveDocumento    = registro.substring(22,25).trim();
		if (TipoDocumento.equals("1"))
		{

			ClaveProveedor    = registro.substring(2,22).trim();
			Proveedor = asigno(ClaveProveedor);
			if ((ClaveDocumento).equals("001")) ClaveDocumento = "Factura";
			if ((ClaveDocumento).equals("002")) ClaveDocumento = "Otros";
			NumeroDocumento  = registro.substring(25,32).trim();
			ImporteDocumento = registro.substring(33,53).trim();
			FechaEmision=registro.substring(54,62).trim();
			FechaVencimiento=registro.substring(62,70).trim();
			Naturaleza ="";
			AsociadoFactura ="";
			Importe =registro.substring(79,89).trim();
			if (NumeroDocumento.length()>0) lista_Pagos+=TipoDocumento.trim()+","+NumeroDocumento.trim()+","+ClaveProveedor.trim()+";";
			EIGlobal.mensajePorTrace("Lista de la claves nuevas"+lista_Pagos, EIGlobal.NivelLog.INFO);
		}
		if (TipoDocumento.equals("2"))
		{
			ClaveProveedor    = registro.substring(2,22).trim();
			Proveedor = asigno(ClaveProveedor);
			if ((ClaveDocumento).equals("003")) ClaveDocumento = "Notas de Cargo";
			if ((ClaveDocumento).equals("004")) ClaveDocumento = "Notas de Credito";
			NumeroDocumento  = registro.substring(25,32).trim();
			ImporteDocumento = registro.substring(33,53).trim();
			FechaEmision=registro.substring(54,62).trim();
			FechaVencimiento="";
			Naturaleza       = registro.substring(70,71).trim();
			AsociadoFactura =  registro.substring(71,78).trim();
			Importe = "";
			if (NumeroDocumento.length()>0) lista_Pagos+=TipoDocumento.trim()+","+NumeroDocumento.trim()+","+ClaveProveedor.trim()+";";
			EIGlobal.mensajePorTrace("Lista de la claves nuevas"+lista_Pagos, EIGlobal.NivelLog.INFO);
		}
	}



public void lecturaCamposYa(String registro)
		{
		tipoRegistro 	  = registro.substring(0,1).trim();
		TipoDocumento     = registro.substring(1,2).trim();
		ClaveDocumento    = registro.substring(22,25).trim();
		if (TipoDocumento.equals("1"))
		{
			ClaveProveedor    = registro.substring(2,22).trim();
			Proveedor = asigno(ClaveProveedor);
			if ((ClaveDocumento).equals("001")) ClaveDocumento = "Factura";
			if ((ClaveDocumento).equals("002")) ClaveDocumento = "Otros";
			NumeroDocumento  = registro.substring(25,32).trim();
			ImporteDocumento = registro.substring(33,53).trim();
			FechaEmision=registro.substring(54,62).trim();
			FechaVencimiento=registro.substring(62,70).trim();
			Naturaleza ="";
			AsociadoFactura ="";
			Importe =registro.substring(79,89).trim();
			Estatus=registro.substring(100,101).trim();
			Observaciones=registro.substring(101,170).trim();
		}
		if (TipoDocumento.equals("2"))
		{
			ClaveProveedor    = registro.substring(2,22).trim();
			Proveedor = asigno(ClaveProveedor);
			if ((ClaveDocumento).equals("003")) ClaveDocumento = "Notas de Cargo";
			if ((ClaveDocumento).equals("004")) ClaveDocumento = "Notas de Credito";
			NumeroDocumento  = registro.substring(25,32).trim();
			ImporteDocumento = registro.substring(33,53).trim();
			FechaEmision=registro.substring(54,62).trim();
			FechaVencimiento="";
			Naturaleza       = registro.substring(70,71).trim();
			AsociadoFactura =  registro.substring(71,78).trim();
			Importe = "";
			Estatus=registro.substring(100,101).trim();
			Observaciones=registro.substring(101,170).trim();
		}
	}

public void lecturaCamposImportado(String registro)
		{
		TipoDocumento     = registro.substring(0,1).trim();
		ClaveDocumento    = registro.substring(21,24).trim();

		if (TipoDocumento.equals("1"))
		{

			ClaveProveedor    = registro.substring(1,21).trim();
			NumeroDocumento  = registro.substring(24,32).trim();
			ImporteDocumento = registro.substring(32,53).trim();
			FechaEmision=registro.substring(53,61).trim();
			FechaVencimiento=registro.substring(61,69).trim();
			Naturaleza ="";
			AsociadoFactura ="";
		}
		if (TipoDocumento.equals("2"))
		{
			ClaveProveedor    = registro.substring(2,21).trim();
			NumeroDocumento  = registro.substring(25,32).trim();
			ImporteDocumento = registro.substring(32,53).trim();
			FechaEmision=registro.substring(53,61).trim();
			FechaVencimiento="";
			Naturaleza       = registro.substring(69,70).trim();
			AsociadoFactura =  registro.substring(70,77).trim();
			Importe = "";


		}

}

//Cortar la longuitud de una cadena a 40 caracteres como maximo
	 public String corta(String c) {
	 String aux;
	    aux = "";
	 /* if (c.length()  < 100)
		  return c;
	  else
	   {
         for(int i= 0; i < 100; i++)
		   aux+= String.valueOf(c.charAt(i));
	     return aux;
	   }
    }*/
    return c.substring(0,(c.length()<100)?c.length():100);
	}

//lectura de un registro del Archivo
	public int leeRegistro(long posicion)
	    {
			String lecturaReg = "";
			posicionaRegistro(posicion);
			try
			{
				lecturaReg = archivoTrabajo.readLine();
			}
    		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo(leeRegistro): " + e, EIGlobal.NivelLog.INFO);
				return -1;
			}
			lecturaCampos(lecturaReg);
		    return 1;
        }

		//Para posicionar el apuntador sobre el registro
		public int posicionaRegistro(long posicion)
		{
		EIGlobal.mensajePorTrace("Entrando a posicionaRegistro(): "+posicion, EIGlobal.NivelLog.INFO);
		try
			{
				archivoTrabajo.seek(posicion);
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error posicionando el Archivo: " + e, EIGlobal.NivelLog.INFO);
				return -1;
			}
		return 1;
		}

		//Cerrado del Archivo
		public void close()
		{
			try
				{
					archivoTrabajo.close();
				}
			catch(IOException e)
				{
					EIGlobal.mensajePorTrace("Error posicionando el Archivo: " + e, EIGlobal.NivelLog.INFO);
				}
		}

		//Crea un renglo para la tabla HTML
		public String creaRegTabla(int contador) {
		StringBuffer sTabla = new StringBuffer ("");
		String cClaveProveedor = "&nbsp;", cNombreProveedor = "&nbsp;",  cClaveDocumento = "&nbsp;",
    		   cNumeroDocumento = "&nbsp;", cImporteDocumento = "&nbsp;", cFacturaAsociada = "&nbsp;",
			   cImporte = "&nbsp;", cFechaEmision = "&nbsp;",cFechaVencimiento = "&nbsp;",cNaturaleza  = "&nbsp;",
			   cEstatus = "&nbsp;",cObservaciones = "&nbsp;";
		String sColor = "";
		String pesos = "$";
		String  Importe1="",Importe2="";
		java.text.NumberFormat comita = java.text.NumberFormat.getCurrencyInstance(java.util.Locale.ENGLISH);
 	   	if (!ClaveProveedor.trim().equals(""))
			{
				cClaveProveedor=ClaveProveedor;
			}
		if (!Proveedor.trim().equals(""))
			{
				cNombreProveedor=Proveedor;
			}
		if (!ClaveDocumento.trim().equals(""))
			{
				cClaveDocumento=ClaveDocumento;
			}
		if (!NumeroDocumento.trim().equals(""))
			{
				cNumeroDocumento=NumeroDocumento;
			}
		if (!AsociadoFactura.trim().equals(""))
			{
				cFacturaAsociada=AsociadoFactura;
			}
		if (!Importe.trim().equals(""))
			{
				cImporte=Importe;
				Importe2 = comita.format(Double.parseDouble(cImporte));

			}
		if (!ImporteDocumento.trim().equals(""))
			{
				cImporteDocumento=ImporteDocumento;
				Importe1=  comita.format(Double.parseDouble(cImporteDocumento));
			}
		if (!Naturaleza.trim().equals(""))
			{
				cNaturaleza=Naturaleza;
			}
		if (!FechaEmision.trim().equals(""))
			{
				cFechaEmision=FechaEmision;
			}
		if (!FechaVencimiento.trim().equals(""))
			{
				cFechaVencimiento=FechaVencimiento;
			}
		if (!Estatus.trim().equals(""))
			{
				cEstatus=Estatus;
			}
		if (!Observaciones.trim().equals(""))
			{
				cObservaciones=Observaciones;
			}
		if ((contador%2)==0)
			{
				sColor="textabdatobs";
			}
		else
			{
				sColor="textabdatcla";
			}


		if (TipoDocumento.equals("1"))
		{
			DecimalFormat formato = new DecimalFormat("#0.00");
			sTabla= new StringBuffer ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'><input name=reg_radio type=radio value=");
			sTabla.append (posicionReg);
			sTabla.append ("></td><td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveProveedor);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cNombreProveedor);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveDocumento);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cNumeroDocumento);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(Importe1.substring (0,1).equals ("$")))
	    		 if (Importe1.substring (0,3).equals ("MXN"))
	    			 sTabla.append (Importe1.substring(3, Importe1.length()));
	    		 else
	    			 sTabla.append (Importe1.substring(1, Importe1.length()));
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cFacturaAsociada);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(Importe2.substring (0,1).equals ("$")))
	    		 if (Importe2.substring (0,3).equals ("MXN"))
	    			 sTabla.append (Importe2.substring(3, Importe2.length()));
	    		 else
	    			 sTabla.append (Importe2.substring(1, Importe2.length()));
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaEmision);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaVencimiento);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cEstatus);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cObservaciones);
			sTabla.append ("</td></tr>");
		}
		if (TipoDocumento.equals("2"))
		{
			DecimalFormat formato = new DecimalFormat("#0.00");
			sTabla= new StringBuffer ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'><input name=reg_radio type=radio value=");
			sTabla.append (posicionReg);
			sTabla.append ("></td><td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveProveedor);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cNombreProveedor);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveDocumento);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cNumeroDocumento);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(Importe1.substring (0,1).equals ("$")))
	    		 if (Importe1.substring (0,3).equals ("MXN"))
	    			 sTabla.append (Importe1.substring(3, Importe1.length()));
	    		 else
	    			 sTabla.append (Importe1.substring(1, Importe1.length()));
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cFacturaAsociada);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cImporte);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaEmision);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaVencimiento);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cEstatus);
			sTabla.append ("</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cObservaciones);
			sTabla.append ("</td></tr>");
		}

		return 	sTabla.toString();
		}

		//Para Tamanio de archivo

        public int tamanioArchivo()
		{
		String totalRegistros="";
		int numReg=0;
		try
			{
				archivoTrabajo.seek(archivoTrabajo.length()-7);
				totalRegistros=archivoTrabajo.readLine();
				try
					{
						numReg=Integer.parseInt(totalRegistros.trim());
					}
				catch(Exception e)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento tamanioArchivo() 1: " + e , EIGlobal.NivelLog.INFO);
						return -1;
    				}
			}
		catch(IOException e)
			{
				if (Global.NIVEL_TRACE>0)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento tamanioArchivo() 2", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("Mensaje= " + e, EIGlobal.NivelLog.INFO);
					}
			}
		return 	numReg;
		}

		/*
		*	El metodo  obtiene el ultimo numero de secuencia del archivo
		*	y lo regresa como un numero entero
		*/
		public int secuenciaArchivo()
		{
		String secuencia="";
		int numSec=0;
		try
			{
				archivoTrabajo.seek(archivoTrabajo.length()-12);
				secuencia=archivoTrabajo.readLine();
				secuencia=secuencia.substring(0,5);
				try
					{
						numSec=Integer.parseInt(secuencia.trim());
					}
				catch(Exception e)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento secuenciaArchivo().", EIGlobal.NivelLog.INFO);
						return -1;
					}
			}
		catch(IOException e)
			{
				if (Global.NIVEL_TRACE>0)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento secuenciaArchivo().", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace(" Mensaje= "+e, EIGlobal.NivelLog.INFO);
					}
			}
		return 	numSec;
		}

		/*
		*	El metodo rellenaCaracter(String origen, char caracter, int cantidad, boolean izquierda)
		*	devuelve una cadena rellenada con el caracter especificado
		*/
    public String rellenaCaracter(String origen, char caracter, int cantidad, boolean izquierda)
	  {
	  int cfin = cantidad - origen.length();
	  if(origen.length() < cantidad)
			{
			  for(int contador = 0 ; contador< cfin;contador++)
					{
			  		if(izquierda)
							{
							origen = caracter + origen;
							}
			  		else
							{
							origen = origen + caracter;
							}
					}
			}
	  return origen;
	  }

///******Alta
		public void altaRegistro(String registroNuevo)
		{
		try
			{
				String nextSec=rellenaCaracter(""+(secuenciaArchivo()+1),'0',5,true);
				String nextReg=rellenaCaracter(""+(tamanioArchivo()+1),'0',5,true);
				archivoTrabajo.seek(archivoTrabajo.length()-13);
				archivoTrabajo.writeBytes(registroNuevo+"\r\n"+ "3" + nextSec + nextReg +"\r\n"); //Escribe el registro nuevo
			}
		catch(IOException e)
			{
				if (Global.NIVEL_TRACE>0)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento alta.", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace(" Mensaje= "+e, EIGlobal.NivelLog.INFO);
					}
			}
		}


//formato de impresion
        public String reporteArchivoSalida()
		{
		String lecturaReg = "", encabezado="";
		StringBuffer strTabla   = new StringBuffer ("");
		int contador      = 0;
		EIGlobal.mensajePorTrace("Entrando a reporteArchivoSalida()", EIGlobal.NivelLog.INFO);
		strTabla=new StringBuffer ("<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
		strTabla=new StringBuffer (strTabla.toString());
		strTabla.append ("<tr><td colspan=7 align=center class=tabmovtex>Reporte De Pagos</td></tr>");
		strTabla.append ("<tr><td class=tittabdat align=center>Clave de Provedor</td>");
		strTabla.append ("<td class=tittabdat align=center>Nombre o razon social</td>");
		strTabla.append ("<td class=tittabdat align=center>Tipo de Documento</td>");
		strTabla.append ("<td class=tittabdat align=center>Numero de Docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>Importe de Docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>Factura asociada</td>");
		strTabla.append ("<td class=tittabdat align=center>Importe a pagar</td>");
		strTabla.append ("<td class=tittabdat align=center>Fecha de Emision</td></tr>");
		strTabla.append ("<tr><td class=tittabdat align=center>Fecha de Vencimiento</td>");
		strTabla.append ("<td class=tittabdat align=center>Estatus</td>");
		strTabla.append ("<td class=tittabdat align=center>Observaciones</td>");
		try
			{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();	//lectura del encabezado
				EIGlobal.mensajePorTrace("encabezado ="+encabezado, EIGlobal.NivelLog.INFO);
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
		if (encabezado!=null && encabezado.substring(0,2).equals("OK"))
		{
				lecturaEncabezadoAS(encabezado);
				do
					{
						try
							{
								posicionReg=archivoTrabajo.getFilePointer();
								lecturaReg=archivoTrabajo.readLine();
							}
						catch(Exception e)
							{
								EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
								return "Error 3: "+ e.getMessage();
							}
						if (lecturaReg!=null)
							{
								if(lecturaReg.trim().equals("") )
									continue;
								lecturaCamposAS(lecturaReg);
								strTabla=new StringBuffer (strTabla + creaRegReporte(contador));
								contador++;
							}
					} while (lecturaReg!=null);
				strTabla=new StringBuffer (strTabla+"</table>");
				try
					{
					archivoTrabajo.close();
					}
				catch(IOException e)
					{
					EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e, EIGlobal.NivelLog.INFO);
						return "Error 4: "+ e;
					}
			}
		else
			{
				return "";
			}
		return strTabla.toString();
		}


		public void lecturaEncabezadoAS(String r)
		{

			int tot=0,acep=0,rech=0;
			asFechaTransmision   = r.substring(posCar(r,';',1)+1,r.length());
			asFechaActualizacion = r.substring(posCar(r,';',1)+1,r.length());
		}



//****Valida detalle

	public void validaDetalle(String linea, int nReg)
	{
		int num=0;
		EIGlobal.mensajePorTrace("Validando"+numReg(nReg), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(linea, EIGlobal.NivelLog.INFO);
		vCaracter(linea,nReg);
		if (vCampoLongitud(linea,"Registro",1,"Detalle",nReg,78,false))
			{
			lecturaCamposImportado(linea);
			vCampoObligatorio(TipoDocumento,"Tipo de documento",3,"Detalle",nReg);
			if (vCampoObligatorio(NumeroDocumento,"Numero de Documento",3,"Detalle",nReg))
			{
				vCampoNumerico(NumeroDocumento,"Numero de Documento",2,"Detalle",nReg);
			}
			if (vCampoObligatorio(ImporteDocumento,"Importe de Documento",3,"Detalle",nReg))
			{
				vCampoNumerico(ImporteDocumento,"Importe de Documento",2,"Detalle",nReg);
			}
			vCampoObligatorio(ClaveProveedor,"Clave del proveedor",3,"Detalle",nReg);
			vCampoObligatorio(FechaEmision,"Fecha de emision",3,"Detalle",nReg);
			if (TipoDocumento.equals("1"))
			{
				vCampoObligatorio(FechaVencimiento,"Fecha de Vencimiento",3,"Detalle",nReg);
			}
			if (TipoDocumento.equals("2"))
			{
				vCampoObligatorio(AsociadoFactura,"Factura Asociada",3,"Detalle",nReg);
				vCampoObligatorio(Naturaleza,"Naturaleza",3,"Detalle",nReg);
			}


		}//longitud
	}

	//**Validando archivo
	public boolean validaArchivo()
	{
	String linea="", campo="", sumario="";
	StringBuffer ce = new StringBuffer ("");
	int numeroLinea=1, cantidad=0;
	ce.append ("Entrando a validaArchivo");
	try
		{
		EIGlobal.mensajePorTrace("seek(0)", EIGlobal.NivelLog.INFO);
		ce.append (" seek(0)");
		archivoTrabajo.seek(0);

	do {
				EIGlobal.mensajePorTrace("readLine(), detalle, noLinea="+numeroLinea, EIGlobal.NivelLog.DEBUG);
				ce.append ("<br>readLine(), detalle, noLinea="+numeroLinea);
				linea=archivoTrabajo.readLine();
				if(linea==null)
					linea = "";
				if ( linea.equals( "" ) )
					linea = archivoTrabajo.readLine();
					if (linea!=null)
						{
						if(linea.trim().equals("") )
							continue;
						validaDetalle(linea,numeroLinea);
						numeroLinea++;
						tamanioImportado++;

						}
				}while (linea!=null);
			if (cadenaValidacion.toString().length()>0)
				{
					cadenaValidacion=new StringBuffer ("<table align='center'>"+cadenaValidacion+"</table>");
					return false;
				 }
			else
				{
					return true;
				}
		}
	catch(Exception e)
		{
		e.printStackTrace();
		EIGlobal.mensajePorTrace("Error validando archivo."+e, EIGlobal.NivelLog.INFO);
		cadenaValidacion=new StringBuffer ("<table>Error validando archivo.<br>"+e+"<br>"+ce +"</table>");
		}
	EIGlobal.mensajePorTrace("cadenaValidacion="+cadenaValidacion, EIGlobal.NivelLog.INFO);
	return false;
	}
	//***valida longuitud de campo
	public  boolean vCampoLongitud(String campo,String descCampo,int numCampo,String tipoReg,int nReg,int longitud, boolean vtrim)
	{
	if (vtrim) campo=campo.trim(); 		//si el ultimo parametro es verdadero hace un trim() sobre el campo
	if (campo.length() != longitud)
		{
			cadenaValidacion.append ("<tr><td>");
			cadenaValidacion.append ((contadorErrores++));
			cadenaValidacion.append ("</td><td>Campo ");
			cadenaValidacion.append (numCampo);
			cadenaValidacion.append (" '");
			cadenaValidacion.append (descCampo);
			cadenaValidacion.append ("' del ");
			cadenaValidacion.append (tipoReg);
			cadenaValidacion.append (", La longitud es invalida");
			cadenaValidacion.append (numReg(nReg));
			cadenaValidacion.append (", longitud=");
			cadenaValidacion.append (campo.length());
			cadenaValidacion.append (" ->");
			cadenaValidacion.append (campo);
			cadenaValidacion.append (".</td></tr>\n");
			return false;
		}
	return true;
	}
//*Numero de registro a validar
public  String numReg(int numeroRegistro)
	{
	return ", Registro No " + numeroRegistro;
	}
//*Para Validar un caracter
public  boolean vCaracter(String campo,int nReg)
	{
	boolean caracterValido=true;
	//Caracteres Validos: 32, 46, 48 To 57, 65 To 90, 97 To 122, 209, 241
	for (int i=0;i<campo.length();i++)
		{
			if (campo.charAt(i)!=32 && campo.charAt(i)!=46  && campo.charAt(i)!=209 && campo.charAt(i)!=241)
				{
					if (!(campo.charAt(i)>=48 && campo.charAt(i)<=57))
						{
							if (!(campo.charAt(i)>=65 && campo.charAt(i)<=90))
								{
									if (!(campo.charAt(i)>=97 && campo.charAt(i)<=122))
										{
											cadenaValidacion.append ("<tr><td>");
											cadenaValidacion.append ((contadorErrores++));
											cadenaValidacion.append ("</td><td>Error, caracter no valido en la posicion ");
											cadenaValidacion.append (i);
											cadenaValidacion.append ("->");
											cadenaValidacion.append (campo.charAt(i));
											cadenaValidacion.append (", codigo=");
											cadenaValidacion.append ((int)campo.charAt(i));
											cadenaValidacion.append (numReg(nReg));
											cadenaValidacion.append ("</td></tr>\n");
											caracterValido=false;
										}
								}
						}
				}
		}
	return caracterValido;
	}

public void modificaRegistro(long posicion,String registroModificado, String RegistroOriginal)
		{
			TipoDocumento     = RegistroOriginal.substring(1,2).trim();
			String CambioTipo = registroModificado.substring(1,2).trim();
			if (TipoDocumento.equals("2")&&CambioTipo.equals("2"))
			{
				bajaRegistro(posicion, RegistroOriginal);
				AsociadoFactura  =  registroModificado.substring(71,78).trim();
				Neteo(AsociadoFactura,registroModificado);
			}
			if (TipoDocumento.equals("2")&&CambioTipo.equals("1"))
			{
				try
					{
						archivoTrabajo.seek(posicion);
						archivoTrabajo.writeBytes(registroModificado);
					}
				catch(Exception e)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento modificacion.", EIGlobal.NivelLog.INFO);
					}
				AsociadoFactura  =  RegistroOriginal.substring(71,78).trim();
				ImporteDocumento =  RegistroOriginal.substring(33,53).trim();
				Naturaleza       =  RegistroOriginal.substring(70,71).trim();
				ClaveProveedor   =  RegistroOriginal.substring(2,22).trim();
				Desneteo(AsociadoFactura,ImporteDocumento,Naturaleza,ClaveProveedor);
			}
			if (TipoDocumento.equals("1")&&CambioTipo.equals("2"))
			{
				try
					{
						archivoTrabajo.seek(posicion);
						archivoTrabajo.writeBytes(registroModificado);
					}
				catch(Exception e)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento modificacion.", EIGlobal.NivelLog.INFO);
					}
				AsociadoFactura  =  registroModificado.substring(71,78).trim();
				Neteo(AsociadoFactura,registroModificado);
			}
			if (TipoDocumento.equals("1")&&CambioTipo.equals("1"))
			{
				try
					{
						archivoTrabajo.seek(posicion);
						archivoTrabajo.writeBytes(registroModificado);
					}
				catch(Exception e)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento modificacion.", EIGlobal.NivelLog.INFO);
					}
			}

		}



 //Neteo de una Nota con su factura correspondiente
 public void Neteo(String facturaAsociada,String nota){
	 String lecturaReg = "", encabezado = "", Numero = "" , Cadena= "";
	 String Neto   = "";
	 String Nota = nota;
	 String sFacturaAsociada = facturaAsociada;
	 float ImportePagar = 0;
	 int contador      = 0;
	 Hashtable Documentos = new Hashtable(2);
	 try
	 {
		 archivoTrabajo.seek(0);
		 encabezado = archivoTrabajo.readLine();
	 }
	 catch (IOException e)
	 {
		 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
	 }
	 if (encabezado!=null)
	 {
		 do
		 {
			 try
			 {
			 	posicionReg = archivoTrabajo.getFilePointer();
				lecturaReg  = archivoTrabajo.readLine();
			 }
			 catch (Exception e)
			 {
				 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			 }
			 if (lecturaReg!=null)
			 {
				 if (lecturaReg.substring(0,1).equals("2"))
				 {
					Numero  = lecturaReg.substring(1,2).trim()+lecturaReg.substring(2,21).trim()+lecturaReg.substring(25,32).trim();
					Documentos.put(Numero,lecturaReg);
				 }
			 }

		 }
		 while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
		 try
		 {
			 archivoTrabajo.close();
		 }
		 catch (IOException e)
		 {
			 EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
		 }
	 }
	 ClaveProveedor   = Nota.substring(2,22);
     Neto = (String)Documentos.get("1"+ClaveProveedor.trim()+sFacturaAsociada.trim());
	 String ImporteNeto = Neto.substring(79,89).trim();
	 ClaveProveedor   = Nota.substring(2,22);
	 NumeroDocumento  = Nota.substring(25,32);
	 ClaveDocumento   = Nota.substring(22,25);
	 ImporteDocumento = Nota.substring(33,53);
	 Naturaleza       = Nota.substring(70,71);
	 EIGlobal.mensajePorTrace("Naturaleza :"+Naturaleza+":", EIGlobal.NivelLog.INFO);
	 if ((Float.parseFloat(ImporteDocumento.trim()) > Float.parseFloat(ImporteNeto.trim()))&&(Naturaleza.equals("D")))
	 {
		 NoAlcanza = "RECHAZADA";
		 return ;
	 }
	  if (Naturaleza.equals("A"))
		 {
			  ImportePagar = Float.parseFloat(ImporteNeto.trim()) + Float.parseFloat(ImporteDocumento.trim());

		 }
		 if (Naturaleza.equals("D"))
		 {
			  ImportePagar = Float.parseFloat(ImporteNeto.trim()) - Float.parseFloat(ImporteDocumento.trim());
		 }
		 Neto = Neto.substring(0,79)+ImportePagar;
		 Neto = checaLongitud(Neto,100);
		 Documentos.put("1"+ClaveProveedor.trim()+sFacturaAsociada.trim(),Neto);
		 try
			{
			File archivo=new File(Archivo1);
			if (archivo.exists())
				{
				archivo.delete();
				}
			}
		catch(Exception e)
			{
				EIGlobal.mensajePorTrace("error en el objeto File(Neteo): " + e, EIGlobal.NivelLog.INFO);
			}
		  try
		 {
			String cadenaNueva="100001E"+"\r\n30000200000\r\n";
			ArchivoFinal = new RandomAccessFile(Archivo1,"rw");
			ArchivoFinal.writeBytes(cadenaNueva);
		 }
		 catch (IOException e)
	 {
			 EIGlobal.mensajePorTrace("Error creando archivo de Pagos en neteo: creaArchivoPagos :" + e, EIGlobal.NivelLog.INFO);
		 }
		 archivoTrabajo=ArchivoFinal;
		 Set set = Documentos.entrySet();
		 Iterator i = set.iterator();
		 while(i.hasNext())
			 {
			   Map.Entry me = (Map.Entry)i.next();
			   altaRegistro((String)me.getValue());
			 }
		altaRegistro(Nota);

 }
///Recuperacion de variables de ambiente
    public void setContrato (String contrato) {Contrato = contrato; }
    public void setUsuario (String usuario) {Usuario = usuario; }
    public void setPerfil (String perfil) {Perfil = perfil; }
	public void setContexto (com.kivasoft.IContext contexto ) {Contexto = contexto;}

///Creacion de la table HTML de un archivo recuperado
	public String lecturaArchivoSalida()
		{
		String lecturaReg="", encabezado="";
		StringBuffer strTabla=new StringBuffer ("");
		contadorRecuperado=0;
		RecuperadoR=0;
		RecuperadoA=0;
		int contador =0;
		EIGlobal.mensajePorTrace("Entrando a lecturaArchivoSalida()", EIGlobal.NivelLog.INFO);
		strTabla = new StringBuffer ("<table width=1250 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
		strTabla.append ("<tr><td colspan=10 class=textabref>Registro de Pagos</td></tr>");
		strTabla.append ("<tr><td class=tittabdat align=center>&nbsp;</td><td class=tittabdat align=center>Clave de prov.</td>");
		strTabla.append ("<td class=tittabdat align=center>Nombre o Raz&oacute;n Social</td><td class=tittabdat align=center>Tipo de docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>N&uacute;mero de docto.</td><td class=tittabdat align=center>Importe de docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>Factura asociada</td><td class=tittabdat align=center>Importe a pagar</td>");
		strTabla.append ("<td class=tittabdat align=center>Fecha de emisi&oacute;n</td><td class=tittabdat align=center>Fecha Vencimiento</td>");
        strTabla.append ("<td class=tittabdat align=center>Estatus</td>"+"<td class=tittabdat align=center>Observaciones</td></tr>");
		try
			{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();	//lectura del encabezado
				EIGlobal.mensajePorTrace("encabezado ="+encabezado, EIGlobal.NivelLog.INFO);
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida 1: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
		if (encabezado!=null && encabezado.substring(0,2).equals("OK"))
			{
				lecturaEncabezadoAS(encabezado);
				do
					{
						try
							{
								posicionReg=archivoTrabajo.getFilePointer();
								lecturaReg=archivoTrabajo.readLine();
							}
						catch(Exception e)
							{
								EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida AsociaFactura: " + e.getMessage(), EIGlobal.NivelLog.INFO);
								return "Error 3: "+ e.getMessage();
							}
						if (lecturaReg!=null)
							{
						 	if ((lecturaReg.substring(0,1).equals("1"))||(lecturaReg.substring(0,1).equals("2")))
									{
										lecturaCamposAS(lecturaReg);
								        strTabla=new StringBuffer (strTabla + creaRegAs(contador));
								        contadorRecuperado++;
										contador++;
									if (Estatus.equals("R"))
									{
										RecuperadoR++;
									}
									if (Estatus.equals("A"))
									{
										RecuperadoA++;
									}

								}

							}
					} while (lecturaReg!=null);
				strTabla=new StringBuffer (strTabla+"</table>");
				try
					{
						archivoTrabajo.close();
					}
				catch(IOException e)
					{
						EIGlobal.mensajePorTrace("Error cerrando el Archivo lecturaArchivoSalida 3: " + e, EIGlobal.NivelLog.INFO);
						return "Error 4: "+ e;
					}
			}
		else
			{
				return "";
			}
		return strTabla.toString();
		}
///Lectura y tokenising de un registro de archivo recuperado
		public void lecturaCamposAS(String r)
		{
		TipoDocumento     = r.substring(0,r.indexOf(';'));
		if (TipoDocumento.equals("1"))
		{
			if (TipoDocumento.equals("1"))
			{
				ClaveDocumento = "Facturas";
			}
			if (TipoDocumento.equals("2"))
			{
				ClaveDocumento = "Otros";
			}
			NumeroSecuencia   = r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			ClaveProveedor    = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
			Proveedor = asigno(ClaveProveedor);
			NumeroDocumento   = r.substring(posCar(r,';',4)+1,posCar(r,';',5));
			ImporteDocumento  = r.substring(posCar(r,';',5)+1,posCar(r,';',6));
			Importe	          = r.substring(posCar(r,';',6)+1,posCar(r,';',7));
			FechaEmision	  = r.substring(posCar(r,';',7)+1,posCar(r,';',8));
			FechaVencimiento  = r.substring(posCar(r,';',8)+1,posCar(r,';',9));
			Estatus	          = r.substring(posCar(r,';',9)+1,posCar(r,';',10));
			Observaciones     = r.substring(posCar(r,';',10)+1,posCar(r,';',11));
		}
		if (TipoDocumento.equals("2"))
		{
			if (TipoDocumento.equals("3"))
			{
				ClaveDocumento = "Nota de Cargo";
			}
			if (TipoDocumento.equals("4"))
			{
				ClaveDocumento = "Nota de credito";
			}
			NumeroSecuencia   = r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			ClaveProveedor    = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
			Proveedor = asigno(ClaveProveedor);
			NumeroDocumento   = r.substring(posCar(r,';',4)+1,posCar(r,';',5));
			ImporteDocumento  = r.substring(posCar(r,';',5)+1,posCar(r,';',6));
			FechaEmision	  = r.substring(posCar(r,';',7)+1,posCar(r,';',8));
			Estatus			  = r.substring(posCar(r,';',8)+1,posCar(r,';',9));
			Observaciones	  = r.substring(posCar(r,';',9)+1,posCar(r,';',10));
			Importe	          = "";
			FechaVencimiento  = "";
			AsociadoFactura  = FacturaAsociada(NumeroDocumento,ClaveProveedor);
		}

		}
//Generacion de registro para la tabla de archivo recuperado
		public String creaRegAs(int contador) {
		StringBuffer sTabla = new StringBuffer ("");
		String  cClaveProveedor = "&nbsp;", cNombreProveedor = "&nbsp;",  cClaveDocumento = "&nbsp;",
    		   cNumeroDocumento = "&nbsp;", cImporteDocumento = "&nbsp;", cFacturaAsociada = "&nbsp;",
			   cImporte = "&nbsp;", cFechaEmision = "&nbsp;",cFechaVencimiento = "&nbsp;",cNaturaleza  = "&nbsp;",
			   cEstatus = "&nbsp;",cObservaciones = "&nbsp;";
		String sColor = "";
		String pesos = "$";
		String Importe1="",Importe2="";
		java.text.NumberFormat comita = java.text.NumberFormat.getCurrencyInstance(java.util.Locale.ENGLISH);

 	   	if (!ClaveProveedor.trim().equals(""))
			{
				cClaveProveedor=ClaveProveedor;
			}
		if (!Proveedor.trim().equals(""))
			{
				cNombreProveedor=Proveedor;
			}
		if (!ClaveDocumento.trim().equals(""))
			{
				cClaveDocumento=ClaveDocumento;
			}
		if (!NumeroDocumento.trim().equals(""))
			{
				cNumeroDocumento=NumeroDocumento;
			}
		if (!AsociadoFactura.trim().equals(""))
			{
				cFacturaAsociada=AsociadoFactura;
			}
		if (!Importe.trim().equals(""))
			{
				cImporte=Importe;
				Importe2 = comita.format(Double.parseDouble(cImporte));
			}
		if (!ImporteDocumento.trim().equals(""))
			{
				cImporteDocumento=ImporteDocumento;
				Importe1=  comita.format(Double.parseDouble(cImporteDocumento));
			}
		if (!Naturaleza.trim().equals(""))
			{
				cNaturaleza=Naturaleza;
			}
		if (!FechaEmision.trim().equals(""))
			{
				cFechaEmision=FechaEmision;
			}
		if (!FechaVencimiento.trim().equals(""))
			{
				cFechaVencimiento=FechaVencimiento;
			}
		if (!Estatus.trim().equals(""))
			{
				cEstatus=Estatus;
			}
		if (!Observaciones.trim().equals(""))
			{
				cObservaciones=Observaciones;
			}
		if ((contador%2)==0)
			{
				sColor="textabdatobs";
			}
		else
			{
				sColor="textabdatcla";
			}

			String cantidad = "",cantidad2 = "";


		if (TipoDocumento.equals("1"))
		{
			DecimalFormat formato = new DecimalFormat("#0.00");
			sTabla= new StringBuffer ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'><input name=reg_radio type=radio value=");
			sTabla.append (posicionReg);
			sTabla.append ("></td><td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveProveedor);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cNombreProveedor);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveDocumento);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cNumeroDocumento);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(Importe1.substring (0,1).equals ("$")))
	    		 if (Importe1.substring (0,3).equals ("MXN"))
	    			 sTabla.append (Importe1.substring(3, Importe1.length()));
	    		 else
	    			 sTabla.append (Importe1.substring(1, Importe1.length()));
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cFacturaAsociada);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(Importe2.substring (0,1).equals ("$")))
	    		 if (Importe2.substring (0,3).equals ("MXN"))
	    			 sTabla.append (Importe2.substring(3, Importe2.length()));
	    		 else
	    			 sTabla.append (Importe2.substring(1, Importe2.length()));
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaEmision);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaVencimiento);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cEstatus);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cObservaciones);
			sTabla.append ("</td></tr>");
		}

		if (TipoDocumento.equals("2"))
		{
			DecimalFormat formato = new DecimalFormat("#0.00");
			sTabla= new StringBuffer ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'><input name=reg_radio type=radio value=");
			sTabla.append (posicionReg);
			sTabla.append ("></td><td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveProveedor);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cNombreProveedor);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cClaveDocumento);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cNumeroDocumento);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(Importe1.substring (0,1).equals ("$")))
	    		 if (Importe1.substring (0,3).equals ("MXN"))
	    			 sTabla.append (Importe1.substring(3, Importe1.length()));
	    		 else
	    			 sTabla.append (Importe1.substring(1, Importe1.length()));
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cFacturaAsociada);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (cImporte);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaEmision);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (cFechaVencimiento);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cEstatus);
			sTabla.append ("</td>");
			sTabla=new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (cObservaciones);
			sTabla.append ("</td></tr>");
		}


		return 	sTabla.toString();
		}


	public int posCar(String cad,char car,int cant)
		{
		int result=0,pos=0;
		for (int i=0;i<cant;i++)
			{
				result=cad.indexOf(car,pos);
				pos=result+1;
			}
		return result;
		}
/////Asociacion de una nota con su factura
	public String FacturaAsociada(String nota, String sProveedor){
		String lecturaReg="", encabezado="";
		String Nota="";
		try
			{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida 1: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
		if (encabezado!=null && encabezado.substring(0,2).equals("OK"))
			{

				do
					{
						try
							{
								posicionReg=archivoTrabajo.getFilePointer();
								lecturaReg=archivoTrabajo.readLine();
							}
						catch(Exception e)
							{
								EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida AsociaFactura: " + e.getMessage(), EIGlobal.NivelLog.INFO);
								return "Error 3: "+ e.getMessage();
							}
						EIGlobal.mensajePorTrace("lecturaReg ="+lecturaReg, EIGlobal.NivelLog.DEBUG);
						if (lecturaReg!=null)
							{
							 if (lecturaReg.substring(0,1).equals("3"))
									{
								         if (((nota.trim()).equals(lecturaReg.substring(posCar(lecturaReg,';',4)+1,posCar(lecturaReg,';',5)).trim()))&&(sProveedor.trim().equals(lecturaReg.substring(posCar(lecturaReg,';',2)+1,posCar(lecturaReg,';',3)).trim())))
										{
											Nota=lecturaReg.substring(posCar(lecturaReg,';',3)+1,posCar(lecturaReg,';',4));
										}
								    }
							}
					 } while (lecturaReg!=null);
			}
		else
			{
				return "";
			}
		return Nota;
	}
//***Genera archivo envio
	public void ArchivoEnvio(){
	 String lecturaReg = "", encabezado = "", Numero = "" , Cadena= "";
	 int contador      = 0;
	 String Neteo = "";
	 Hashtable Documentos = new Hashtable(2);//**Hashtable donde se introducira la informacion
	 try
	 {
		 archivoTrabajo.seek(0);
		 encabezado = archivoTrabajo.readLine();
	 }
	 catch (IOException e)
	 {
		 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
	 }
	 if (encabezado!=null)
	 {
		 do
		 {
			 try
			 {
			 	posicionReg = archivoTrabajo.getFilePointer();
				lecturaReg  = archivoTrabajo.readLine();
			 }
			 catch (Exception e)
			 {
				 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			 }
			 if (lecturaReg!=null)
			 {
				 if (lecturaReg.substring(0,1).equals("2"))
				 {
					Numero  = lecturaReg.substring(25,32).trim();
					Documentos.put(Numero,lecturaReg);
				 }
			 }

		 }
		 while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
		 try
		 {
			 archivoTrabajo.close();
		 }
		 catch (IOException e)
		 {
			 EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
		 }
	 }


	  try
	 	{
		//Si existe el archivo lo borra
		File archivo=new File(Global.DOWNLOAD_PATH +"Envio");
        EIGlobal.mensajePorTrace("Antes de Borrar " + Archivo1 , EIGlobal.NivelLog.INFO);
		if (archivo.exists())
	    	{
	 		archivo.delete();
			}
		}
	catch(Exception e)
		{
	    	EIGlobal.mensajePorTrace("error en el objeto File(Neteo): " + e, EIGlobal.NivelLog.INFO);
		}
      try
     {
		ArchivoFinal = new RandomAccessFile(Global.DOWNLOAD_PATH+"Envio","rw");
     }
     catch (IOException e)
     {
		 EIGlobal.mensajePorTrace("Error creando archivo de Pagos en neteo: creaArchivoPagos :" + e, EIGlobal.NivelLog.INFO);
     }

	 //archivoTrabajo=ArchivoFinal;
	 Set set = Documentos.entrySet();
	 Iterator i = set.iterator();
	 String RegistroEnvio;
	 while(i.hasNext())
		 {
	       Map.Entry me = (Map.Entry)i.next();
		   RegistroEnvio = (String)me.getValue();
			tipoRegistro 	  = RegistroEnvio.substring(0,1).trim();
			TipoDocumento     = RegistroEnvio.substring(1,2).trim();
			if (TipoDocumento.equals("1"))
			{
				TipoDocumento     = RegistroEnvio.substring(1,2).trim();
				ClaveDocumento    = RegistroEnvio.substring(22,25).trim();
				ClaveProveedor    = RegistroEnvio.substring(2,22).trim();
				NumeroDocumento   = RegistroEnvio.substring(25,32).trim();
				ImporteDocumento  = RegistroEnvio.substring(33,53).trim();
				FechaEmision      = RegistroEnvio.substring(54,62).trim();
				FechaVencimiento  = RegistroEnvio.substring(62,70).trim();
				Naturaleza ="";
				AsociadoFactura ="";
				Importe           = RegistroEnvio.substring(79,89).trim();
				RegistroEnvio ="1;"+ClaveProveedor+";"+ClaveDocumento+";"+NumeroDocumento+";"+ImporteDocumento+";"+Importe+";"+FechaEmision+";"+FechaVencimiento+";";
				 try
		        {
		        ArchivoFinal.seek(ArchivoFinal.length());
		        ArchivoFinal.writeBytes(RegistroEnvio+"\r\n");
		        }
				catch (IOException e)
				{
				}
				ImporteArchivo = ImporteArchivo + Float.parseFloat(ImporteDocumento);
				SaldoArchivo = SaldoArchivo + Float.parseFloat(Importe);
			}
			if (TipoDocumento.equals("2"))
			{
				TipoDocumento     = RegistroEnvio.substring(1,2).trim();
				ClaveDocumento    = RegistroEnvio.substring(22,25).trim();
				ClaveProveedor    = RegistroEnvio.substring(2,22).trim();
				NumeroDocumento   = RegistroEnvio.substring(25,32).trim();
				ImporteDocumento  = RegistroEnvio.substring(33,53).trim();
				FechaEmision      = RegistroEnvio.substring(54,62).trim();
				FechaVencimiento  = RegistroEnvio.substring(62,70).trim();
				Naturaleza        = RegistroEnvio.substring(70,71).trim();
				AsociadoFactura ="";
				Importe           = RegistroEnvio.substring(79,89).trim();
				AsociadoFactura =  RegistroEnvio.substring(71,78).trim();
				RegistroEnvio ="2;"+ClaveProveedor+";"+ClaveDocumento+";"+NumeroDocumento+";"+ImporteDocumento+";"+Naturaleza+";"+FechaEmision+";";
				Neteo		  ="3;"+ClaveProveedor+";"+AsociadoFactura+";"+NumeroDocumento+";"+ClaveDocumento+";"+ImporteDocumento+";"+Naturaleza+";";
				 try
		        {
		        ArchivoFinal.seek(ArchivoFinal.length());
		        ArchivoFinal.writeBytes(RegistroEnvio+"\r\n"+Neteo+"\r\n");
		        }
				catch (IOException e)
				{
				}
		}
		 }

 }

 public String checaLongitud(String cadena,int longitud){
	int diferencia=longitud-cadena.length();
	if (cadena.length()<longitud){
		for(int i=0;i<diferencia;i++)
			cadena =cadena +" ";
	}
	return cadena;
}


public String SustraeEncabezado()
	{
		String encabezado="";
		try
		{
			archivoTrabajo.seek(0);
			encabezado=archivoTrabajo.readLine();	//lectura del encabezado
			EIGlobal.mensajePorTrace("encabezado ="+encabezado, EIGlobal.NivelLog.INFO);
		}
		catch(IOException e)
		{
			EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida 1: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			return "Error 1: "+ e.getMessage();
		}
		return encabezado;
    }


//baja de registro
		public void bajaRegistro(long posicion, String RegistroActual)
		{
			String registroVacio ="0                                                                                                  ";
			TipoDocumento     = RegistroActual.substring(1,2).trim();
			if (TipoDocumento.equals("1"))
			{
				posicionaRegistro(posicion);
				try
					{
						archivoTrabajo.writeBytes(registroVacio);
						String nextReg=rellenaCaracter(""+(tamanioArchivo()-1),'0',5,true);
						archivoTrabajo.seek(archivoTrabajo.length()-7);
						archivoTrabajo.writeBytes(nextReg);
					}
				catch(Exception e)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento baja.", EIGlobal.NivelLog.INFO);
					}
			}
			if (TipoDocumento.equals("2"))
			{
				ImporteDocumento =  RegistroActual.substring(33,53).trim();
				Naturaleza       =  RegistroActual.substring(70,71).trim();
				AsociadoFactura  =  RegistroActual.substring(71,78).trim();
				ClaveProveedor   =  RegistroActual.substring(2,22).trim();
				posicionaRegistro(posicion);
				try
					{
						archivoTrabajo.writeBytes(registroVacio);
						String nextReg=rellenaCaracter(""+(tamanioArchivo()-1),'0',5,true);
						archivoTrabajo.seek(archivoTrabajo.length()-7);
						archivoTrabajo.writeBytes(nextReg);
					}
				catch(Exception e)
					{
						EIGlobal.mensajePorTrace("Error en el procedimiento baja.", EIGlobal.NivelLog.INFO);
					}
				Desneteo(AsociadoFactura,ImporteDocumento,Naturaleza,ClaveProveedor);
			}

		}


public String reporteArchivo()
		{
		String lecturaReg="", encabezado="";
		StringBuffer strTabla=new StringBuffer ("");
		int contador=0;
		EIGlobal.mensajePorTrace("Entrando a reporteArchivo()", EIGlobal.NivelLog.INFO);
		strTabla=new StringBuffer ("<table width=610 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
		strTabla=new StringBuffer (strTabla.toString());
		strTabla.append ("<tr><td colspan=7 align=center class=tabmovtex>Reporte de Pagos</td></tr>");
		strTabla.append ("<tr><td class=tittabdat align=center>Clave de Provedor</td>");
		strTabla.append ("<td class=tittabdat align=center>Nombre o razon social</td>");
		strTabla.append ("<td class=tittabdat align=center>Tipo de Documento</td>");
		strTabla.append ("<td class=tittabdat align=center>Numero de Docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>Importe de Docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>Factura asociada</td>");
		strTabla.append ("<td class=tittabdat align=center>Importe a pagar</td>");
		strTabla.append ("<td class=tittabdat align=center>Fecha de Emision</td></tr>");
		strTabla.append ("<tr><td class=tittabdat align=center>Fecha de Vencimiento</td>");
		strTabla.append ("<td class=tittabdat align=center>Estatus</td>");
		strTabla.append ("<td class=tittabdat align=center>Observaciones</td>");

		try
			{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
			}	//lectura del encabezado
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
		if (encabezado!=null)
			{
				do
					{
						try
							{
								posicionReg=archivoTrabajo.getFilePointer();
								lecturaReg=archivoTrabajo.readLine();
								if ( lecturaReg.equals( "" ) )
									lecturaReg = archivoTrabajo.readLine();

							}
						catch(Exception e)
							{
								EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
								return "Error 3: "+ e.getMessage();
							}
						if (lecturaReg!=null)
							{
							if (lecturaReg.substring(0,1).equals("2"))
									{
										lecturaCamposYa(lecturaReg);
										strTabla=new StringBuffer (strTabla + creaRegReporte(contador));
										contador++;
									}
							}
					} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try
					{
						archivoTrabajo.close();
					}
				catch(IOException e)
					{
						EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
						return "Error 4: "+ e.getMessage();
					}
			}
		strTabla=new StringBuffer (strTabla+"</table>");
		return strTabla.toString();
		}
	//crea reporte 13
	public String creaRegReporte(int contador)
		{
		StringBuffer sTabla=new StringBuffer ("");
		String sColor="";

		if ((contador%2)==0)
			{
				sColor="textabdatobs";
			}
		else
			{
				sColor="textabdatcla";
			}
		java.text.NumberFormat comita = java.text.NumberFormat.getCurrencyInstance(java.util.Locale.ENGLISH);
		String cantidad = "",cantidad2 = "";
		String pesos = "$";
		if (ClaveDocumento.equals("Factura")||ClaveDocumento.equals("Otros"))
		{
			cantidad = comita.format(Double.parseDouble(ImporteDocumento));
			cantidad2 = comita.format(Double.parseDouble(Importe));
			sTabla= new StringBuffer ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (ClaveProveedor);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (Proveedor);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (ClaveDocumento);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (NumeroDocumento);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			 if (!(cantidad.substring (0,1).equals ("$")))
	    		 if (cantidad.substring (0,3).equals ("MXN"))
			 		 sTabla.append (pesos+cantidad.substring(3, cantidad.length()));
	    		 else
	    			 sTabla.append (pesos+cantidad.substring(1, cantidad.length()));
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (AsociadoFactura);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(cantidad2.substring (0,1).equals ("$")))
	    		 if (cantidad2.substring (0,3).equals ("MXN"))
			 		 sTabla.append (cantidad2.substring(3, cantidad2.length()));
	    		 else
	    			 sTabla.append (cantidad2.substring(1, cantidad2.length()));
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (FechaEmision);
			sTabla.append ("</td></tr>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'align=center>");
			sTabla.append (FechaVencimiento);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (Estatus);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (Observaciones);
			sTabla.append ("&nbsp;</td></tr>");
		}
		else
		{
			cantidad = comita.format(Double.parseDouble(ImporteDocumento));
			sTabla= new StringBuffer ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (ClaveProveedor);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (Proveedor);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (ClaveDocumento);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append ( NumeroDocumento);
			sTabla.append ( "&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (pesos);
			 if (!(cantidad.substring (0,1).equals ("$")))
	    		 if (cantidad.substring (0,3).equals ("MXN"))
			 		 sTabla.append (cantidad.substring(3, cantidad.length()));
	    		 else
	    			 sTabla.append (cantidad.substring(1, cantidad.length()));
			sTabla.append ( "&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ( "<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (AsociadoFactura);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=right>");
			sTabla.append (Importe);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("' align=center>");
			sTabla.append (FechaEmision);
			sTabla.append ( "</td></tr>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<tr><td class='");
			sTabla.append (sColor);
			sTabla.append ("'align=center>");
			sTabla.append (FechaVencimiento);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (Estatus);
			sTabla.append ("&nbsp;</td>");
			sTabla= new StringBuffer (sTabla.toString());
			sTabla.append ("<td class='");
			sTabla.append (sColor);
			sTabla.append ("'>");
			sTabla.append (Observaciones);
			sTabla.append ("&nbsp;</td></tr>");
		}

		return 	sTabla.toString();
		}

	public  boolean vCampoObligatorio(String campo,String descCampo,int numCampo,String tipoReg,int nReg)
	{
	if (campo.trim().equals(""))
		{
			cadenaValidacion.append ("<tr><td>");
			cadenaValidacion.append ((contadorErrores++));
			cadenaValidacion.append ("</td><td>Campo ");
			cadenaValidacion.append (numCampo);
			cadenaValidacion.append (" '");
			cadenaValidacion.append (descCampo);
			cadenaValidacion.append ("' del ");
			cadenaValidacion.append (tipoReg);
			cadenaValidacion.append (", Campo Obligatorio");
			cadenaValidacion.append (numReg(nReg));
			cadenaValidacion.append ("</td></tr>\n");
			return false;
		}
	return true;
	}

	public  boolean vCampoNumerico(String campo,String descCampo,int numCampo,String tipoReg,int nReg)
	{
	campo=campo.trim();
	if (convierteNum(campo)==-1)
		{
			cadenaValidacion.append ("<tr><td>");
			cadenaValidacion.append ((contadorErrores++));
			cadenaValidacion.append ("</td><td>Campo ");
			cadenaValidacion.append (numCampo);
			cadenaValidacion.append (" '");
			cadenaValidacion.append (descCampo);
			cadenaValidacion.append ("' del ");
			cadenaValidacion.append (tipoReg);
			cadenaValidacion.append (", No tiene formato numerico ->");
			cadenaValidacion.append (campo);
			cadenaValidacion.append (numReg(nReg));
			cadenaValidacion.append ("</td></tr>\n");
			return false;
		}
	return true;
	}

	public int convierteNum(String strNum)
	{
	int cantidad=0;
	try
		{
		cantidad=Integer.parseInt(strNum);
		return cantidad;
		}
	catch(NumberFormatException e)
		{
		EIGlobal.mensajePorTrace("Error="+e, EIGlobal.NivelLog.INFO);
			return -1;
		}
	}


//lectura archivo importado
public String lecturaArchivoImportado()
	{
		String lecturaReg = "", encabezado = "";
		StringBuffer strTabla   = new StringBuffer ("");
		int contador      = 0;
		strTabla = new StringBuffer ("<table width=1250 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
		strTabla.append ("<tr><td colspan=10 class=textabref>Registro de Pagos</td></tr>");
		strTabla.append ("<tr><td class=tittabdat align=center>&nbsp;</td><td class=tittabdat align=center>Clave de prov.</td>");
		strTabla.append ("<td class=tittabdat align=center>Nombre o Raz&oacute;n Social</td><td class=tittabdat align=center>Tipo de docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>N&uacute;mero de docto.</td><td class=tittabdat align=center>Importe de docto.</td>");
		strTabla.append ("<td class=tittabdat align=center>Factura asociada</td><td class=tittabdat align=center>Importe a pagar</td>");
		strTabla.append ("<td class=tittabdat align=center>Fecha de emisi&oacute;n</td><td class=tittabdat align=center>Fecha Vencimiento</td>");
        strTabla.append ("<td class=tittabdat align=center>Estatus</td>"+"<td class=tittabdat align=center>Observaciones</td></tr>");
		   try
			{
				archivoTrabajo.seek(0);
				encabezado = archivoTrabajo.readLine();
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo importado: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
		if (encabezado!=null)
			{
				try
				{
					archivoTrabajo.seek(0);
				}
				catch (IOException e)
				{
				}


				do
					{
						try
							{
								posicionReg = archivoTrabajo.getFilePointer();
								lecturaReg  = archivoTrabajo.readLine();
							}
						catch(Exception e)
							{
  							    EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
								return "Error 3: "+ e.getMessage();
							}
						if (lecturaReg!=null)
							{
								if ((lecturaReg.substring(0,1).equals("2"))&&(!lecturaReg.substring(1,2).equals("3")))
									{
										lecturaCamposYa(lecturaReg);
    									strTabla=new StringBuffer (strTabla + creaRegTabla(contador));
										contador++;
									}
    						}
					} while (lecturaReg!=null);
				try
					{
						archivoTrabajo.close();
					}
				catch(IOException e)
					{
						EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
						return "Error 4: "+ e.getMessage();
					}

			}
			strTabla = new StringBuffer (strTabla+"</table>");
		    return strTabla.toString();
	}

//Relacionado con
	public String RegistroActual(long posicion)
	    {
			String lecturaReg = "";
			posicionaRegistro(posicion);
			try
			{
				lecturaReg = archivoTrabajo.readLine();
			}
    		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo(leeRegistro): " + e, EIGlobal.NivelLog.INFO);
			}
			return lecturaReg;
        }
//Desnetear
 public void Desneteo(String facturaAsociada,String ImporteNota,String Naturaleza, String sClaveProveedor){
	 String lecturaReg = "", encabezado = "", Numero = "" , Cadena= "";
	 String Neto   = "";
	 String FacturaAsociada = facturaAsociada;
	 float ImportePagar = 0;
	 int contador      = 0;
	 Hashtable Documentos = new Hashtable(2);
	 try
	 {
		 archivoTrabajo.seek(0);
		 encabezado = archivoTrabajo.readLine();
	 }
	 catch (IOException e)
	 {
		 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
	 }
	 if (encabezado!=null)
	 {
		 do
		 {
			 try
			 {
			 	posicionReg = archivoTrabajo.getFilePointer();
				lecturaReg  = archivoTrabajo.readLine();
			 }
			 catch (Exception e)
			 {
				 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			 }
			 if (lecturaReg!=null)
			 {
				 if (lecturaReg.substring(0,1).equals("2"))
				 {
					Numero  = lecturaReg.substring(1,2).trim()+lecturaReg.substring(2,21).trim()+lecturaReg.substring(25,32).trim();
					Documentos.put(Numero,lecturaReg);
				 }
			 }

		 }
		 while (lecturaReg.substring(0,1).equals("2"));
		 try
		 {
			 archivoTrabajo.close();
		 }
		 catch (IOException e)
		 {
			 EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
		 }
	 }

     Neto = (String)Documentos.get("1"+sClaveProveedor+FacturaAsociada.trim());

	 String ImporteNeto = Neto.substring(79,89).trim();
	 ImporteDocumento = ImporteNota;
	 if ((Float.parseFloat(ImporteDocumento.trim()) > Float.parseFloat(ImporteNeto.trim()))&&(Naturaleza.equals("D")))
	 {
		 EIGlobal.mensajePorTrace("Importe de nota Invalido ", EIGlobal.NivelLog.INFO);
		 return ;
	 }
	 if (Naturaleza.equals("A"))
	 {
		  ImportePagar = Float.parseFloat(ImporteNeto.trim()) - Float.parseFloat(ImporteDocumento.trim());

	 }
	 if (Naturaleza.equals("D"))
	 {
		  ImportePagar = Float.parseFloat(ImporteNeto.trim()) + Float.parseFloat(ImporteDocumento.trim());
	 }
	 Neto = Neto.substring(0,78)+" "+ImportePagar;
	 Neto = checaLongitud(Neto,100);
	 Documentos.put("1"+sClaveProveedor+FacturaAsociada.trim(),Neto);
     try
	 	{
		File archivo=new File(Archivo1);
 		if (archivo.exists())
	    	{
	 		archivo.delete();
			}
		}
	catch(Exception e)
		{
	    	EIGlobal.mensajePorTrace("error en el objeto File(Neteo): " + e, EIGlobal.NivelLog.INFO);
		}
      try
     {
		String cadenaNueva="100001E"+"\r\n30000200000\r\n";
		ArchivoFinal = new RandomAccessFile(Archivo1,"rw");
		ArchivoFinal.writeBytes(cadenaNueva);
	}
     catch (IOException e)
     {
		 EIGlobal.mensajePorTrace("Error creando archivo de Pagos en neteo: creaArchivoPagos :" + e, EIGlobal.NivelLog.INFO);
     }
	 archivoTrabajo=ArchivoFinal;
	 Set set = Documentos.entrySet();
	 Iterator i = set.iterator();
	 while(i.hasNext())
		 {
	       Map.Entry me = (Map.Entry)i.next();
		   altaRegistro((String)me.getValue());
		 }

 }
 //***De la asociacion de documento obtiene la nota de una factura
  public String NotaAsociada(String factura, String sProveedor){
		String lecturaReg="", encabezado="";
		String Nota="";
		try
			{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida 1: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
		if (encabezado!=null)
			{
				do
					{
						try
							{
								posicionReg=archivoTrabajo.getFilePointer();
								lecturaReg=archivoTrabajo.readLine();
							}
						catch(Exception e)
							{
								EIGlobal.mensajePorTrace("Error leyendo el Archivo lecturaArchivoSalida AsociaFactura: " + e.getMessage(), EIGlobal.NivelLog.INFO);
								return "Error 3: "+ e.getMessage();
							}
						if (lecturaReg!=null)
							{
							 if (lecturaReg.substring(1,2).equals("2"))
									{
								         if (((factura.trim()).equals(lecturaReg.substring(71,78).trim()))&&(sProveedor.trim().equals(lecturaReg.substring(2,22).trim())))
										{
											Nota =  Nota + lecturaReg.substring(25,32).trim();
										}
								    }
							}
					 } while (lecturaReg!=null);
			}
		return Nota;
	}
//****Realiza el neteo de un documento importado
	public void NeteoImportado(String  copiaOriginal){
		String lecturaReg = "", encabezado = "", Numero = "" , Cadena= "",sTipo ="",sProveedor="", Clave ="", sImporte="";
		String Nota="",llave ="",sFactura ="", sSaldo = "", sEstatus = "", sObservaciones = "", sImporteNota="",sNaturaleza="";
		String ImporteTrans = "";
		float ImportePagar = 0;
		Hashtable Documentos        = new Hashtable(2);
		Hashtable DocumentoFactura	= new Hashtable(2);
		Hashtable DocumentoNota	    = new Hashtable(2);
		Hashtable DuplicadosFactura = new Hashtable(2);
		Hashtable DuplicadosNotas   = new Hashtable(2);
		ErroresImportacion=0;
		File archivo =null,ParaPath=null;

		try
		{
			archivoTrabajo.seek(0);
			encabezado = archivoTrabajo.readLine();
		}
		catch (IOException e)
		{
			EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
		}
		if (encabezado!=null)
		{
			try
			{
				archivoTrabajo.seek(0);
			}
			catch (IOException e)
			{
				EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			do
			{
				try
				{
					posicionReg = archivoTrabajo.getFilePointer();
					lecturaReg  = archivoTrabajo.readLine();
				 }
				 catch (Exception e)
				 {
					 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				 }
				 if (lecturaReg!=null)
				 {
					    sTipo      = lecturaReg.substring(0,1).trim();
						if (sTipo.equals("1"))
						{
							sProveedor = lecturaReg.substring(1,21).trim();
							Numero     = lecturaReg.substring(24,32).trim();
							sImporte   = lecturaReg.substring(32,53).trim();
							Clave = sTipo+sProveedor+Numero;
							sImporte = checaLongitud(sImporte,21);
							ImporteTrans = lecturaReg+sImporte;
							if (DocumentoFactura.containsKey(Clave))
							{
								FacturasDuplicadas = FacturasDuplicadas + Numero+",";
								DuplicadosFactura.put(Clave,ImporteTrans);
							}
							else
							{
								ImporteTrans=checaLongitud(ImporteTrans,179);
								DocumentoFactura.put(Clave,ImporteTrans);
							}

						}
						if (sTipo.equals("2"))
						{
							sProveedor = lecturaReg.substring(1,21).trim();
							Numero     = lecturaReg.substring(24,32).trim();
							Clave = sTipo+sProveedor+Numero;
							if (DocumentoNota.containsKey(Clave))
							{
								NotasDuplicadas = NotasDuplicadas + Numero+",";
								DuplicadosNotas.put(Clave,lecturaReg);
							}
							else DocumentoNota.put(Clave,lecturaReg);

						}

				 }

			 }
			 while (lecturaReg!=null);
			 try
			 {
				 archivoTrabajo.close();
			 }
			 catch (IOException e)
			 {
				 EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			 }
		 }

			 try
				{
				archivo=new File(Archivo1);
				if (archivo.exists())
					{
					archivo.delete();
					}
				}

			catch(Exception e)
				{
					EIGlobal.mensajePorTrace("error en el objeto File(Neteo): " + e, EIGlobal.NivelLog.INFO);
				}
			archivo=new File(Archivo1);
			  try
			 {
				String cadenaNueva="100001E"+"\r\n30000200000\r\n";
				ArchivoFinal = new RandomAccessFile(archivo,"rw");
				ArchivoFinal.writeBytes(cadenaNueva);
			}
			 catch (IOException e)
			 {
				 EIGlobal.mensajePorTrace("Error creando archivo de Pagos en neteo: creaArchivoPagos :" + e, EIGlobal.NivelLog.INFO);
			 }
			 archivoTrabajo=ArchivoFinal;

			 Set set = DocumentoNota.entrySet();
			 Iterator i = set.iterator();
			String sProveedor2;
		    String Numero2;
			while(i.hasNext())
				 {
				   Map.Entry me = (Map.Entry)i.next();
				   Nota = (String)me.getValue();
				   sProveedor2 = Nota.substring(1,21).trim();
				   Numero2     = Nota.substring(70,77).trim();
				   llave = "1"+sProveedor2.trim()+Numero2.trim();

				   if (DocumentoFactura.containsKey(llave))
				   {
					   sFactura = (String)DocumentoFactura.get(llave);
					   sSaldo = sFactura.substring(78,98).trim();
					   sImporteNota = Nota.substring(32,53).trim();
					   sNaturaleza  = Nota.substring(69,70).trim();
					   if ((Float.parseFloat(sImporteNota.trim()) > Float.parseFloat(sSaldo.trim()))&&(sNaturaleza.equals("D")))
						 {
							 sEstatus = "R";
							 sObservaciones = "el importe de la factura a asociar es insuficiente";
							 ErroresImportacion=ErroresImportacion+1;
						 }
						else
						 {
							if (sNaturaleza.equals("A"))
							 {
								  ImportePagar = Float.parseFloat(sSaldo.trim()) + Float.parseFloat(sImporteNota.trim());

							 }
							 if (sNaturaleza.equals("D"))
							 {
								  ImportePagar = Float.parseFloat(sSaldo.trim()) - Float.parseFloat(sImporteNota.trim());

							 }
						 }
						sFactura = sFactura.substring(0,78)+ImportePagar;
						sFactura = checaLongitud(sFactura,179);
						DocumentoFactura.put(llave,sFactura);
						sObservaciones = checaLongitud(sObservaciones,81);
						Nota = checaLongitud(Nota,98);
						Nota = Nota + sEstatus+sObservaciones;
						DocumentoNota.put("2"+Nota.substring(1,21).trim()+Nota.substring(24,32).trim(),Nota);
				   }
				   else
					   {
					   sEstatus = "R";
					   sObservaciones = "No exite la factura con la que pretende netear";
					   ErroresImportacion=ErroresImportacion+1;
					   sObservaciones = checaLongitud(sObservaciones,81);
				   	   Nota = checaLongitud(Nota,99);
					   Nota = Nota + sEstatus+sObservaciones;
					   DocumentoNota.put("2"+Nota.substring(1,21).trim()+Nota.substring(24,32).trim(),Nota);
					   }

				 }
			 Set set2 = DocumentoFactura.entrySet();
			 Iterator i2 = set2.iterator();
			 String Formateado2;
			 while(i2.hasNext())
				 {
				   Map.Entry me2 = (Map.Entry)i2.next();
				   Formateado2 = "2"+(String)me2.getValue();
				   altaRegistro(Formateado2);
				 }
			 Set set3 = DocumentoNota.entrySet();
			 Iterator i3 = set3.iterator();
			 String Formateado3;
			while(i3.hasNext())
				 {
				   Map.Entry me3 = (Map.Entry)i3.next();
				   Formateado3 = "2"+(String)me3.getValue();
				   altaRegistro(Formateado3);
				 }

			Set set4 = DuplicadosFactura.entrySet();
			Iterator i4 = set4.iterator();
			String Formateado4;
			while(i4.hasNext())
				 {
				   Map.Entry me4 = (Map.Entry)i4.next();
				   sEstatus = "R";
				   sObservaciones = "Su factura aparece al menos dos veces";
				   ErroresImportacion=ErroresImportacion+1;
				   sObservaciones = checaLongitud(sObservaciones,80);
				   Formateado4 = "2"+(String)me4.getValue();
				   Formateado4 = checaLongitud(Formateado4,98);
				   Formateado4 = Formateado4 + sEstatus+sObservaciones;
				   altaRegistro(Formateado4);
				 }

			Set set5 = DuplicadosNotas.entrySet();
			Iterator i5 = set5.iterator();
			String Formateado5;
			while(i5.hasNext())
				 {
				   Map.Entry me5 = (Map.Entry)i5.next();
				   sEstatus = "R";
				   ErroresImportacion=ErroresImportacion+1;
				   sObservaciones = "Su Nota aparece al menos dos veces";
				   sObservaciones = checaLongitud(sObservaciones,80);
				   Formateado5 = "2"+(String)me5.getValue();
				   Formateado5 = checaLongitud(Formateado5,98);
				   Formateado5 = Formateado5 + sEstatus+sObservaciones;
				   altaRegistro(Formateado5);
				 }
			ParaPath=new File(copiaOriginal);

			try
				{

					archivo.lastModified();
					ParaPath.getPath();
					ParaPath.getParent();
					System.out.println("Ultima modificacion :"+archivo.lastModified());
					System.out.println("El path de extraccion :"+ParaPath.getPath());
					System.out.println("El padre :"+ParaPath.getParent());
				}
			catch(Exception e)
				{
					EIGlobal.mensajePorTrace("error en el objeto File(Neteo): " + e, EIGlobal.NivelLog.INFO);
				}

		System.out.println("Errores de Importacion :" +ErroresImportacion);
	}

public void FormatoExportacion(String ElTipo){
		 String lecturaReg = "", encabezado = "", Numero = "" , Cadena= "";
		   Hashtable Documentos = new Hashtable(2);
		 try
		 {
			 archivoTrabajo.seek(0);
			 encabezado = archivoTrabajo.readLine();
		 }
		 catch (IOException e)
		 {
			 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
		 }
		 if (encabezado!=null)
		 {
			 do
			 {
				try
				 {
					posicionReg = archivoTrabajo.getFilePointer();
					lecturaReg  = archivoTrabajo.readLine();
				 }
				 catch (Exception e)
				 {
					 EIGlobal.mensajePorTrace("Error leyendo el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
				 }
				 if (lecturaReg!=null)
				 {
					if (lecturaReg.substring(0,1).equals("2"))
					 {
						Numero  = lecturaReg.substring(1,2).trim()+lecturaReg.substring(2,21).trim()+lecturaReg.substring(25,32).trim();
						 if (ElTipo.equals("importado"))
						 {
							Documentos.put(Numero,lecturaReg.substring(1,170));
						 }
						 else
					     {
							 Documentos.put(Numero,lecturaReg.substring(1,100));
						 }

					 }
				 }

			 }
			 while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
			 try
			 {
				 archivoTrabajo.close();
			 }
			 catch (IOException e)
			 {
				 EIGlobal.mensajePorTrace("Error cerrando el Archivo: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			 }
		 }
		 try
			{
			File archivo=new File(Archivo1);
			if (archivo.exists())
				{
				archivo.delete();
				}
			}
		catch(Exception e)
			{
				EIGlobal.mensajePorTrace("error en el objeto File(Exportacion): " + e, EIGlobal.NivelLog.INFO);
			}
		  try
		 {
   		    ArchivoFinal = new RandomAccessFile(Archivo1,"rw");
		 }
		 catch (IOException e)
		 {
			 EIGlobal.mensajePorTrace("Error creando archivo A Exportar" + e, EIGlobal.NivelLog.INFO);
		 }
		 archivoTrabajo=ArchivoFinal;
		 Set set = Documentos.entrySet();
		 Iterator i = set.iterator();
		 while(i.hasNext())
			 {
			   Map.Entry me = (Map.Entry)i.next();
			    try
		        {
		         ArchivoFinal.seek(ArchivoFinal.length());
		         ArchivoFinal.writeBytes((String)me.getValue()+"\r\n");
		        }
				catch (IOException e)
				{
					 EIGlobal.mensajePorTrace("Error escribiendo en el archivo a exportar" + e, EIGlobal.NivelLog.INFO);
				}

			 }

	}
}