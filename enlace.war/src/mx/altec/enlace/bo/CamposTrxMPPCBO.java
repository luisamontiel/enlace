/**
 * 
 */
package mx.altec.enlace.bo;

import java.util.List;

import mx.altec.enlace.dao.GenericDAO;


/**
 * @author FSW-Indra
 * @sice 23/04/2015
 *
 */
public class CamposTrxMPPCBO extends GenericDAO {
	
	/**
	 * IND. TIPO TARJETA
	 */
	private String indicadorTipoTDC;
	/**
	 * DESCR.TIPO TJT REDUC 
	 */
	private String descripcionTipoReduc;
	/**
	 * NUMERO DE TARJETA
	 */
	private String numeroTDC;
	/**
	 * FECHA DE ALTA
	 */
	private String fechaAlta;
	/**
	 * IND.SITUACION TJT
	 */
	private String indicadorSituacion;
	/**
	 * DES.SITUACION TJT
	 */
	private String descripcionSituacion;
	/**
	 * NOMBRE ESTAMPACION
	 */
	private String nombreEstampacion;
	/**
	 * CODIGO DE BLOQUEO
	 */
	private String codigoBloqueo;
	/**
	 * FECHA DE BAJA
	 */
	private String fechaBaja;
	/**
	 * DESCRIPCION BLOQUEO
	 */
	private String descripcionBloqueo;
	/**
	 * TIPO DE CONSULTA
	 */
	private String tipoCuenta;
	/**
	 * NUMERO DE CLIENTE
	 */
	private String numeroCliente;
	/**
	 * Mensaje de Estatus de la Transaccion
	 */
	private String msgStatus;
	/**
	 * Codigo de Estatus de le Transaccion
	 */
	private int codStatus;
	
	/**
	 * Tarjetas Lista que almacenara los objetos con las tarjetas relacionadas
	 */
	private List<String> tarjetas;
	
	/**
	 * getIndicadorTipoTDC de tipo String.
	 * @author FSW-Indra
	 * @return indicadorTipoTDC de tipo String
	 */
	public String getIndicadorTipoTDC() {
		return indicadorTipoTDC;
	}
	/**
	 * setIndicadorTipoTDC para asignar valor a indicadorTipoTDC.
	 * @author FSW-Indra
	 * @param indicadorTipoTDC de tipo String
	 */
	public void setIndicadorTipoTDC(String indicadorTipoTDC) {
		this.indicadorTipoTDC = indicadorTipoTDC;
	}
	/**
	 * getDescripcionTipoReduc de tipo String.
	 * @author FSW-Indra
	 * @return descripcionTipoReduc de tipo String
	 */
	public String getDescripcionTipoReduc() {
		return descripcionTipoReduc;
	}
	/**
	 * setDescripcionTipoReduc para asignar valor a descripcionTipoReduc.
	 * @author FSW-Indra
	 * @param descripcionTipoReduc de tipo String
	 */
	public void setDescripcionTipoReduc(String descripcionTipoReduc) {
		this.descripcionTipoReduc = descripcionTipoReduc;
	}
	/**
	 * getNumeroTDC de tipo String.
	 * @author FSW-Indra
	 * @return numeroTDC de tipo String
	 */
	public String getNumeroTDC() {
		return numeroTDC;
	}
	/**
	 * setNumeroTDC para asignar valor a numeroTDC.
	 * @author FSW-Indra
	 * @param numeroTDC de tipo String
	 */
	public void setNumeroTDC(String numeroTDC) {
		this.numeroTDC = numeroTDC;
	}
	/**
	 * getFechaAlta de tipo String.
	 * @author FSW-Indra
	 * @return fechaAlta de tipo String
	 */
	public String getFechaAlta() {
		return fechaAlta;
	}
	/**
	 * setFechaAlta para asignar valor a fechaAlta.
	 * @author FSW-Indra
	 * @param fechaAlta de tipo String
	 */
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	/**
	 * getIndicadorSituacion de tipo String.
	 * @author FSW-Indra
	 * @return indicadorSituacion de tipo String
	 */
	public String getIndicadorSituacion() {
		return indicadorSituacion;
	}
	/**
	 * setIndicadorSituacion para asignar valor a indicadorSituacion.
	 * @author FSW-Indra
	 * @param indicadorSituacion de tipo String
	 */
	public void setIndicadorSituacion(String indicadorSituacion) {
		this.indicadorSituacion = indicadorSituacion;
	}
	/**
	 * getDescripcionSituacion de tipo String.
	 * @author FSW-Indra
	 * @return descripcionSituacion de tipo String
	 */
	public String getDescripcionSituacion() {
		return descripcionSituacion;
	}
	/**
	 * setDescripcionSituacion para asignar valor a descripcionSituacion.
	 * @author FSW-Indra
	 * @param descripcionSituacion de tipo String
	 */
	public void setDescripcionSituacion(String descripcionSituacion) {
		this.descripcionSituacion = descripcionSituacion;
	}
	/**
	 * getNombreEstampacion de tipo String.
	 * @author FSW-Indra
	 * @return nombreEstampacion de tipo String
	 */
	public String getNombreEstampacion() {
		return nombreEstampacion;
	}
	/**
	 * setNombreEstampacion para asignar valor a nombreEstampacion.
	 * @author FSW-Indra
	 * @param nombreEstampacion de tipo String
	 */
	public void setNombreEstampacion(String nombreEstampacion) {
		this.nombreEstampacion = nombreEstampacion;
	}
	/**
	 * getCodigoBloqueo de tipo String.
	 * @author FSW-Indra
	 * @return codigoBloqueo de tipo String
	 */
	public String getCodigoBloqueo() {
		return codigoBloqueo;
	}
	/**
	 * setCodigoBloqueo para asignar valor a codigoBloqueo.
	 * @author FSW-Indra
	 * @param codigoBloqueo de tipo String
	 */
	public void setCodigoBloqueo(String codigoBloqueo) {
		this.codigoBloqueo = codigoBloqueo;
	}
	/**
	 * getFechaBaja de tipo String.
	 * @author FSW-Indra
	 * @return fechaBaja de tipo String
	 */
	public String getFechaBaja() {
		return fechaBaja;
	}
	/**
	 * setFechaBaja para asignar valor a fechaBaja.
	 * @author FSW-Indra
	 * @param fechaBaja de tipo String
	 */
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	/**
	 * getDescripcionBloqueo de tipo String.
	 * @author FSW-Indra
	 * @return descripcionBloqueo de tipo String
	 */
	public String getDescripcionBloqueo() {
		return descripcionBloqueo;
	}
	/**
	 * setDescripcionBloqueo para asignar valor a descripcionBloqueo.
	 * @author FSW-Indra
	 * @param descripcionBloqueo de tipo String
	 */
	public void setDescripcionBloqueo(String descripcionBloqueo) {
		this.descripcionBloqueo = descripcionBloqueo;
	}
	/**
	 * getTipoCuenta de tipo String.
	 * @author FSW-Indra
	 * @return tipoCuenta de tipo String
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 * setTipoCuenta para asignar valor a tipoCuenta.
	 * @author FSW-Indra
	 * @param tipoCuenta de tipo String
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	/**
	 * getNumeroCliente de tipo String.
	 * @author FSW-Indra
	 * @return numeroCliente de tipo String
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}
	/**
	 * setNumeroCliente para asignar valor a numeroCliente.
	 * @author FSW-Indra
	 * @param numeroCliente de tipo String
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * getMsgStatus de tipo String.
	 * @author FSW-Indra
	 * @return msgStatus de tipo String
	 */
	public String getMsgStatus() {
		return msgStatus;
	}

	/**
	 * setMsgStatus para asignar valor a msgStatus.
	 * @author FSW-Indra
	 * @param msgStatus de tipo String
	 */
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}

	/**
	 * getCodStatus de tipo int.
	 * @author FSW-Indra
	 * @return codStatus de tipo int
	 */
	public int getCodStatus() {
		return codStatus;
	}

	/**
	 * setCodStatus para asignar valor a codStatus.
	 * @author FSW-Indra
	 * @param codStatus de tipo int
	 */
	public void setCodStatus(int codStatus) {
		this.codStatus = codStatus;
	}

	/**
	 * setTarjetas para asignar la lista de tarjetas relacionadas
	 * @author FSW - Indra
	 * @param tarjetas el tarjetas a establecer
	 */
	public void setTarjetas(List<String> tarjetas) {
		this.tarjetas = tarjetas;
	}

	/**
	 * getTarjetas para obtener la lista de tarjetas relacionadas
	 * @author FSW - Indra
	 * @return el tarjetas
	 */
	public List<String> getTarjetas() {
		return tarjetas;
	}
}
