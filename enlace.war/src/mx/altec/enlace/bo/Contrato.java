/*
 * Contrato.java
 *
 * Created on 31 de agosto de 2006
 *
 * Clase encarga de generar el documento del contrato
 * en formato PDF
 */

package mx.altec.enlace.bo;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.Hashtable;

import mx.altec.enlace.utilerias.Global;


/**
 *
 * @author David Aguilar G�mez. Asteci.
 */
public class Contrato {
    private String contrato = "";
    private String razonSocial = "";
    private String nombre = "";
    private String direccion = "";
    private String colonia = "";
    private String cpEstado = "";
	//private float tam = 0;
    /*public static void main(String[] args) {
        System.out.println("Empieza la Generaci�n de PDF");
        Contrato2 contrato = new Contrato2();
        contrato.tam = Float.parseFloat(args[0]);
        contrato.generaContrato("12345678","DAVID AGUILAR GOMEZ.");

    }*/

/** El m�todo generaContrato(String, String, String, String) genera el PDF
*   @param contrato  numero de contrato
*   @param direccion direccion de la empresa
*   @param empresa nombre de la empresa
*   @param nombre nombre del representante
*/
    public ByteArrayOutputStream generaContrato(){
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("M�todo Contrato.generaContrato");
        try{
            PdfReader reader = new PdfReader(Global.OTP_RUTA_PDF_CONTRATO);
			//PdfReader reader = new PdfReader("/productos/ias60/ias/APPS/enlaceMig/enlaceMig/Contrato.pdf");
            System.out.println("Pags: " + reader.getNumberOfPages());

            Document documento = new Document(PageSize.LETTER,1,1,1,1);
            PdfWriter writer = PdfWriter.getInstance(documento, baos);
            //PdfWriter writer = PdfWriter.getInstance(documento, new FileOutputStream("Contrato.pdf"));
            documento.open();

            PdfContentByte cb = writer.getDirectContent();

            PdfImportedPage page = writer.getImportedPage(reader, 4);
            cb.addTemplate(page, 0, 0);

            cb.beginText();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.setFontAndSize(bf, 8);
            cb.setTextMatrix(470, 717.5f);
            cb.showText(contrato);
            cb.endText();


            documento.newPage();
            page = writer.getImportedPage(reader, 2);
            cb.addTemplate(page, 0, 0);

            documento.newPage();
            page = writer.getImportedPage(reader, 3);
            cb.addTemplate(page, 0, 0);

            documento.newPage();
            page = writer.getImportedPage(reader, 4);
            cb.addTemplate(page, 0, 0);

            documento.newPage();
            page = writer.getImportedPage(reader, 5);
            cb.addTemplate(page, 0, 0);

            documento.newPage();
            page = writer.getImportedPage(reader, 6);
            cb.addTemplate(page, 0, 0);
            cb = writer.getDirectContent();
            cb.beginText();
            cb.setFontAndSize(bf, 6);
            cb.setTextMatrix(340, 525);
            cb.showText(direccion);
            cb.setTextMatrix(340, 518);
            cb.showText(colonia);
            cb.setTextMatrix(340, 511);
            cb.showText(cpEstado);
            cb.setFontAndSize(bf, 7);
            if(razonSocial.length()>0)
                cb.setTextMatrix(((page.getWidth()/4)*3)-((razonSocial.length()/2)*4.4f), 399);
            cb.showText(razonSocial);
            if(nombre.length()>0){
                if(nombre.length()<=60){
                    cb.setTextMatrix(((page.getWidth()/4)*3)-((nombre.length()/2)*4.4f), 304);
                    cb.showText(nombre);
                }else{
                    int ind = 0;
                    for(int i=0;i<60;i++){
                        i = nombre.indexOf(" ",i);
                        if(Math.abs(60-i)<Math.abs(60-ind))
                            ind = i;
                    }
                    cb.setTextMatrix(((page.getWidth()/4)*3)-((nombre.substring(0,ind).length()/2)*4.4f), 315);
                    cb.showText(nombre.substring(0,ind));
                    cb.setTextMatrix(((page.getWidth()/4)*3)-((nombre.substring(ind).length()/2)*4.4f), 304);
                    cb.showText(nombre.substring(ind+1));
                }
            }
            cb.endText();

            documento.close();
        }catch(Exception e){
            e.printStackTrace();
        }
		return baos;
    }

    public void setContrato(String string)
    {
            contrato = string;
    }
    public void setRazonSocial(String string)
    {
            razonSocial = string;
    }
    public void setNombre(String string)
    {
            nombre = string;
    }
    public void setDireccion(String string)
    {
            direccion = string;
    }
    public void setColonia(String string)
    {
            colonia = string;
    }
    public void setCpEstado(String string)
    {
            cpEstado = string;
    }
}