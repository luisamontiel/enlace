package mx.altec.enlace.bo;
/**
 *
 * @author Z096114 Emmanuel Sanchez Castillo
 * @version 1.0
 * @Fecha 12 Noviembre 2010
 * @proyecto 201001500 - EBEE (Estrategia de banca electronica Enlace
 *
 */
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.GL51Tran;
import mx.altec.enlace.beans.GL52Tran;
import mx.altec.enlace.beans.GLFacultad;
import mx.altec.enlace.beans.GLFacultadCta;
import mx.altec.enlace.dao.GLTranDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class GLTranAction {
	
	private static int MAX_RELLAMADOS = 10;

	public boolean consultaFacultadesGL(GL51Tran glFacultad ,GL52Tran glFacultadCta)throws Exception{
		EIGlobal.mensajePorTrace("ENTRA consultaFacultadesGL", EIGlobal.NivelLog.INFO);
		boolean result = false;
		int rellamados = 0;
		
		GLTranDAO glTran = new GLTranDAO();

		ArrayList<GLFacultad> listaFacultades = new ArrayList<GLFacultad>(0);
		glFacultad.setFacultades(listaFacultades);
		do{
			glFacultad.setCodMsg("");
		result = glTran.consultaFacultad(glFacultad);
		rellamados ++;
		}while(GLTranDAO.CODRES_OK_PAG.equals(glFacultad.getCodMsg())&& rellamados < MAX_RELLAMADOS );


		ArrayList<GLFacultadCta> listaFacultadesCta = new ArrayList<GLFacultadCta>(0);
		glFacultadCta.setFacultadesCta(listaFacultadesCta);
		rellamados = 0;
		do{
			glFacultadCta.setCodMsg("");
			result = glTran.consultaFacultadCta(glFacultadCta);
			rellamados ++;
		}while(GLTranDAO.CODRES_OK_PAG.equals(glFacultadCta.getCodMsg()) && rellamados < MAX_RELLAMADOS);

		EIGlobal.mensajePorTrace("SALE consultaFacultadesGL", EIGlobal.NivelLog.INFO);

		return result;

	}


}
