package mx.altec.enlace.bo;

import java.io.*;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;

public class ValidaArchivoPagos{

	/** Constante para BaseServlet */
	public static BaseServlet BAL;
	/** Variable para Acceso a archivo de pagos */
	RandomAccessFile archivoPagos;

	//Los metodos que se encuentran a continuacion son necesarios para validar los valores de los campos
	//contenidos en un archivo de pago de Nomina, estas validaciones se llevaran a cabo cuando:
	//a) El archivo se este importando
	//b) Se este dando de alta algun registro en el archivo de pagos
	//c) Se realicen modificaciones a cualquiera de los registros existentes

	//La definicion del las validaciones se inicia con dos metodos que vefrifican:
	//1. que no existan numeros en un campo de texto(validaLetras)
	//2. que no existan letras en un campo que solo admite numeros(validaDigito)

    /**
     * Metodo para validar si una cadena contiene letras
     * @param cadenaQueRecibo Cadena a evaluar
     * @return boolean Resultado de validacion
     */
    public boolean validaLetras (String cadenaQueRecibo){
		//EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaLetras&", EIGlobal.NivelLog.INFO);
        String cadenaParaEvaluar;
        cadenaParaEvaluar = cadenaQueRecibo.trim();
        int longitudArray;
        longitudArray     = cadenaParaEvaluar.length();
        char [] charArreglo;
        charArreglo       = new char [longitudArray];
        boolean contieneNumero = false;
        boolean cadenaValida = true;

        for (int i=0; i<charArreglo.length; i++){
            charArreglo[i]=cadenaParaEvaluar.charAt(i);
                if (!Character.isLetter(charArreglo[i])&&!Character.isWhitespace(charArreglo[i])){
				    contieneNumero=true;
					break;
				}
				if(Character.isLowerCase(charArreglo[i])){
				    contieneNumero=true;
					break;
				}
        }

		if (contieneNumero){
			cadenaValida =  false;
        }

		return cadenaValida;
   }
    //fin del metodo validaLetras

    /**
     * Metodo para validar si la cadena tiene numeros
     * @param cadena Cadena a validar
     * @return boolean Resultado de validacion
     */
    public boolean validaDigito(String cadena){
				EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaDigito&", EIGlobal.NivelLog.INFO);
        String cadenaRecibida;
		if(cadena==null) {
			cadena = "";
		}
        cadenaRecibida     = cadena.trim();
        int longitudDelArreglo;
        longitudDelArreglo = cadenaRecibida.length();
        char [] miArreglo;
        miArreglo          = new char [longitudDelArreglo];
        boolean contieneLetra = false;
        boolean cadenaValida = true;

        for (int i=0; i<miArreglo.length; i++){
		            miArreglo[i]=cadenaRecibida.charAt(i);
                if (Character.isLetter(miArreglo[i])){
                    contieneLetra=true;
                    break;
                }
        }
   		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &saliendo del metodo validaDigito&", EIGlobal.NivelLog.INFO);

        if (contieneLetra){
        	cadenaValida =  false;
        }

        return cadenaValida;
    } // fin del metodo validaDigito

	/**
	 * Q05-0044906 - Praxis NVS - 051117
	 * Metodo para validar si la cadena contiene caracteres validos
	 * @param cadena Cadena a validar
	 * @return boolean Resultado de validacion
	 */
	public boolean validaConcepto(String cadena){
		String caracteresValidos="ABCDEFGHIJKLMNOPQRSTUVWXYZ.,-0123456789 ";
		char chr = '0';

	    for (int i = 0; i<cadena.length(); i++) {
			chr = cadena.charAt(i);
			if (caracteresValidos.indexOf(chr) == -1) {
				EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &El concepto contiene caracteres no validos&", EIGlobal.NivelLog.INFO);
				return false;
			}
		}
		return true;
    }// Fin del metodo validaConcepto


	//A continuacion se encuentran los metodos que validan uno a uno los valores de los campos existentes en el archivo,
	//las validaciones se dividen de acuerdo al tipo de registro que se este validando, quedadndo esta clasificacion
	//de la siguiente forma: registro de encabezado, registros de detalle y registro sumario

    /**
     * Tipo de Registro, debe ser uno
     * @param tipoRegistro Tipo de registro
     * @return boolean Resultado de validacion
     */
    public boolean validaEncTipoReg( String tipoRegistro){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaEncTipoReg&", EIGlobal.NivelLog.INFO);
		String tipoReg;
        tipoReg= tipoRegistro;
        int tipo;
	    boolean valido=false;

		try{
			tipo= Integer.parseInt(tipoReg);
            if (tipo == 1){
			    valido= true;
            }
            else{
                valido= false;
            }
        }
        catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaEncTipoReg()"+e, EIGlobal.NivelLog.INFO);
        }
    return valido;
    }//fin del metodo validaEncTipoReg()


	/**
	 * Numero de Secuencia, en el encabezado, debe ser uno.
	 * @param primerNumSec Numero secuencial
	 * @return boolean Resultado de validacion
	 */
	public boolean validaPrimerNumSec(String primerNumSec)
	{
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaPrimerNumSec&", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaPrimerNumSec&  + numero: ("+primerNumSec+")", EIGlobal.NivelLog.INFO);
		String primNumSec;
        primNumSec= primerNumSec.trim();
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaPrimerNumSec&  + numero: ("+primNumSec+")", EIGlobal.NivelLog.INFO);
        int primoNumeroSecuencial=0;
        boolean banderaNumSecuencial = false;

	    try{
	        primoNumeroSecuencial= Integer.parseInt(primNumSec);
		    }
	    catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaPrimerNumSec()"+e, EIGlobal.NivelLog.INFO);
	    }
        if (primoNumeroSecuencial == 1){
        	banderaNumSecuencial = true;
        }

        return banderaNumSecuencial;
    }//fin del metodo validaPrimerNumSec


    /**
     * Sentido, debe ser una letra E mayuscula
     * @param sentido Cadena de sentido
     * @return boolean Validacion sentido
     */
    public boolean validaSentido(String sentido){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaSentido&", EIGlobal.NivelLog.INFO);
        String sent;
        boolean banderaSentido = false;
        sent= sentido;
        if ("E".equals(sent)){
        	banderaSentido = true;
        }

        return banderaSentido;
    }//fin del metodo validaSentido

    /**
     * Fecha de Generacion y fecha de aplicacion.
     * @param fecha Fecha de entrada
     * @return boolean Validacion fecha
     */
    public boolean fechaValida(String fecha){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo fechaValida&"+ fecha, EIGlobal.NivelLog.INFO);
	    String fechaParaValidar;
		int month=0;
	    int day=0;
		int year=0;
	    boolean diaValido, mesValido, yearValido;
		boolean fechaCorrecta =false;
	    fechaParaValidar= fecha;

	    try{
		    month=Integer.parseInt(fechaParaValidar.substring(0,2));
	    }
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %fechaValida()"+e, EIGlobal.NivelLog.INFO);
	    }
		try{
			day=Integer.parseInt(fechaParaValidar.substring(2,4));
	    }
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %fechaValida()"+e, EIGlobal.NivelLog.INFO);
	    }
		try{
			year=Integer.parseInt(fechaParaValidar.substring(4,8));
	    }
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %fechaValida()"+e, EIGlobal.NivelLog.INFO);
	    }

	/*	java.util.Date midia = new java.util.Date();
		System.out.println("ValidaArchivoPagos.class "+((int)(midia.getYear()+1900)));
        if (  year <(midia.getYear()+1900) ||year !=(midia.getYear()+1900)  ){
			System.out.println("La fecha fue Igual o mayor ok");
            yearValido= false;
        }
*/
/*
        if (year<1980 || year > 2002){// quitar esto para tomar cualquier a�o
            yearValido= false;        //
        }                             //
        else{*/
            yearValido=true;
        //}
        if (month<1 || month>12){
            mesValido=false;
        }
        else{
            mesValido=true;
        }

        if(day < 1 || day > regresaDiaMes(month, year)){
        	diaValido=false;
        }else{
        	diaValido=true;
        }

        if (yearValido && mesValido && diaValido){
            fechaCorrecta= true;
        }else {
            fechaCorrecta =false;
        }

		return fechaCorrecta;
	 }//fin del metodo fechaValida

	/**
	 * Metodo para validar la cuenta
	 * @param cuenta Cuenta de entrada
	 * @return boolean Validacion de cuenta
	 */
	public boolean validaCuenta(String cuenta){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaCuenta()&", EIGlobal.NivelLog.INFO);
		String cuentaSinBlancos="";
		boolean cuentaValida = true;
		cuentaSinBlancos=cuenta.trim();
		if (cuentaSinBlancos.length()<10 || cuentaSinBlancos.length()>11) {
			cuentaValida = false;
		}

		return cuentaValida;
    }

	/**
	 * Metodo para validar cuenta de dolares
	 * @param cuenta Cuenta de entrada
	 * @return boolean Validacion de cuenta
	 */
	public boolean validaCuentaDolares(String cuenta){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaCuentaDolares()&", EIGlobal.NivelLog.INFO);
		String cuentaSinBlancos = "";
		String dolares			= "";
		boolean cuentaValida = true;
		cuentaSinBlancos=cuenta.trim();
		if(cuentaSinBlancos.length() >= 11)
		  dolares = cuentaSinBlancos.substring(0, 2);
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo dolares&"+ dolares, EIGlobal.NivelLog.INFO);

		if("82".equals(dolares) || "82".equals(dolares) || "49".equals(dolares)  ) {
			cuentaValida = false;
		}

		return cuentaValida;
	}

	/**
	 * Metodo para validar si la cuenta es de dolares
	 * @param cuenta Cuenta de entrada
	 * @return boolean Validacion
	 */
	public boolean esCuentaDeDolares(String cuenta)
	 {
		String dolares = "";
		boolean esDolares = false;
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo esCuentaDeDolares()&", EIGlobal.NivelLog.INFO);
		if(cuenta.trim().length() >= 11) {
		  dolares = cuenta.trim().substring(0, 2);
		}

		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class & esCuentaDeDolares():& prefijo cuenta"+ dolares, EIGlobal.NivelLog.INFO);
		if("82".equals(dolares) || "83".equals(dolares) || "49".equals(dolares) || "50".equals(dolares) ) {
			esDolares = true;
		}

		return esDolares;
	}


    /**
     * Numero de cuenta, longitud ==11  //clabe todo el metodo
     * @param cuenta Cuenta de entrada
     * @param tamCuenta Longitud de cuenta
     * @return boolean Validacion
     */
    public boolean validaCuentainter(String cuenta, int tamCuenta){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaCuentaInter()&"+ tamCuenta, EIGlobal.NivelLog.INFO);
		String cuentaSinBlancos="";
		boolean cuentaValida = true;
		cuentaSinBlancos=cuenta.trim();

		if (cuentaSinBlancos.length()!= tamCuenta) {
			cuentaValida = false;
		}

		return cuentaValida;
    }
	//********************
	//Metodos para validar los campos del registro de detalle

	//tipo de registro
	//en todos los registros del encabezado, debe ser 2
    public boolean validaDetalleTipoReg(String tipoRegSum){
				EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaCuenta()&", EIGlobal.NivelLog.INFO);
        String tipoRegistroSum;
        tipoRegistroSum= tipoRegSum;
        int tipo=0;
        try{
            tipo = Integer.parseInt(tipoRegistroSum);
        }
        catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaDetalleTipoReg()"+e, EIGlobal.NivelLog.INFO);
        }
        if (tipo == 2){
            return true;
        }
        else{
            return false;
        }
    }// fin del metodo validaDetalleTipoReg

    //numero de secuencia
    //debe iniciar en dos

    public boolean validaNumSec(int position, String numSecuencial){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaNumSec()&", EIGlobal.NivelLog.INFO);
        String numeroSec;
        numeroSec= numSecuencial.trim();
        int arrayPosition=0;
        int secuentialNumber=0;
        int valorParaComparar=0;
        arrayPosition= position;
        try{
            //secuentialNumber= Integer.parseInt(numSecuencial);
			secuentialNumber= Integer.parseInt(numeroSec);
        }
        catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaNumSec()"+e, EIGlobal.NivelLog.INFO);
        }
        valorParaComparar=arrayPosition +2;
		   if (secuentialNumber==valorParaComparar){
		        return true;
        }
        else{
		       return false;
        }
    }//fin del metodo validaNumSec

    //numero de empleado.
    //verificar que exista y que no contenga letras.
    public boolean validaNumEmpleado(String numEmpleado){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaNumEmpleado()&", EIGlobal.NivelLog.INFO);
		String numeroEmpleado;
        numeroEmpleado= numEmpleado;
        if (numeroEmpleado==""){
            return false;
        }
        else{

            return true;
        }
    }//fin del metodo

	//metodo para validar que no se repitan los numeros de empleado
	public boolean verificaDuplicidadNumeroEmpleado(String[] arrayNumerosEmpleado, String numeroParaComparar){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo verificaDuplicidadNumeroEmpleado()&", EIGlobal.NivelLog.INFO);
		int ocurrencias=0;
		for(int a=0; a<arrayNumerosEmpleado.length; a++){
			if(arrayNumerosEmpleado[a].equals(numeroParaComparar)){
				ocurrencias=ocurrencias+1;
			}
		}
		if(ocurrencias>1)
			return true;
		else
			return false;
	}
	//fin del metodo

	//se verifica que las cuentas que se dan de alta no existan en el archivo
	public boolean verificaDuplicidadNuevoNumeroEmpleado(String[] numerosEmpleado, String numeroNuevo){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo verificaDuplicidadNuevoNumeroEmpleado()&", EIGlobal.NivelLog.INFO);
		boolean numeroDuplicado=false;

		for(int i=0; i<numerosEmpleado.length; i++){
			if(numerosEmpleado[i].equals(numeroNuevo)){
				numeroDuplicado=true;
				break;
			}
		}
		return numeroDuplicado;
	}
	//fin del metodo

    //metodo para validar el apellido paterno, el materno y el nombre.
    public boolean validaNombres(String name){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaNombres()&", EIGlobal.NivelLog.INFO);
		String elNombre;
        elNombre= name;

        if (validaLetras(elNombre.trim())){
            return true;
        }
        else{
            return false;
        }
    }

	//metodo para validar que no se repitan los numeros de cuenta
	public boolean verificaDuplicidadCuentas(String[] arrayCuentas, String cuentaParaComparar){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo verificaDuplicidadCuentas()1&", EIGlobal.NivelLog.INFO);
		int ocurrencias=0;
		for(int a=0; a<arrayCuentas.length; a++){
			if(arrayCuentas[a].equals(cuentaParaComparar)){
				ocurrencias=ocurrencias+1;
			}
		}
		if(ocurrencias>1)
			return true;
		else
			return false;
	}
	//fin del metodo

	//se verifica que las cuentas que se dan de alta no existan en el archivo
	public boolean verificaDuplicidadCuentasNuevas(String[] cuentas, String cuentaNueva){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo verificaDuplicidadCuentasNuevas()&", EIGlobal.NivelLog.INFO);
		boolean cuentaDuplicada=false;
		for(int i=0; i<cuentas.length; i++){
			if(cuentas[i].equals(cuentaNueva)){
				cuentaDuplicada=true;
				break;
			}
		}
		return cuentaDuplicada;
	}
	//fin del metodo

   //metodo para validar el digito verificador del numero de cuenta

   public boolean validaDigitoCuenta(String cuenta){
	   	 EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaDigitoCuenta()&", EIGlobal.NivelLog.INFO);
       String cuentaCompuesta="";
       int longitudCuenta= 11;
       int ponderado=0;
       String ponderacion="234567234567";
       int nCta=0;
       int digitoPonderacion=0;
       boolean cuentaValida=false;
	   String count = "";
	    try{
			cuentaCompuesta="21"+cuenta.trim();
		   count =cuenta.trim();
		  }
	    catch(NumberFormatException e){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaDigitoCuenta()"+e, EIGlobal.NivelLog.INFO);
		cuentaValida=false;
		 }

       for(int i=0; i<cuentaCompuesta.length()-1; i++){

		try{
			nCta=Integer.parseInt(String.valueOf(cuentaCompuesta.charAt(i)));
		        digitoPonderacion=Integer.parseInt(String.valueOf(ponderacion.charAt(i)));
		        ponderado=ponderado+(nCta*digitoPonderacion);
		    }
		 catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaDigitoCuenta()"+e, EIGlobal.NivelLog.INFO);

		 }
       }
       switch(ponderado%longitudCuenta){
           case 0:
               if (cuentaCompuesta.endsWith("0")){
                   cuentaValida=true;
                   break;
               }
               else{
                   cuentaValida=false;
                   break;
               }
           case 1:
               cuentaValida=false;
               break;
           default:
               if ((longitudCuenta-(ponderado%longitudCuenta))==Integer.parseInt(String.valueOf(count.charAt(10)))){
                   cuentaValida=true;
                   break;
               }
               else{
                   cuentaValida=false;
                   break;
               }
       }
    return cuentaValida;
   }

    //metodo para validar el importe
    public boolean validaImporte(String importeOriginal){
	   EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaImporte()&", EIGlobal.NivelLog.INFO);
		boolean importeValido=false;
		try{
			double importe=Double.valueOf(importeOriginal).doubleValue();
			importeValido=true;
		}
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaImporte()"+e, EIGlobal.NivelLog.INFO);
		}
		return importeValido;
	}

	//Metodos para validar los campos en el registro sumario.

	//Tipo de regsitro, debe ser 3
    public boolean validaSumTipoReg(String tipo){
	   EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaSumTipoReg()&", EIGlobal.NivelLog.INFO);
        String tipoReg;
        tipoReg= tipo;
        int tipoRegistro;
        boolean valido=false;
		try{
        	tipoRegistro = Integer.parseInt(tipoReg);
	        if (tipoRegistro == 3){
	            valido= true;
        	}
	        else{
	            valido= false;
	        }
		}
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaSumTipoReg()"+e, EIGlobal.NivelLog.INFO);
		}
		return valido;
    }//fin del metodo validaSumTipoReg


	//Numero de secuencia
	//Debe ser el mismo numero de registros
    public boolean validaUltimoNumeroSecuencial(String lastSecuenceNumber, int totalRegs){
	   EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaUltimoNumeroSecuencial()&", EIGlobal.NivelLog.INFO);
        String ultimoSecuencial;
        ultimoSecuencial= lastSecuenceNumber.trim();
        int lastNumber=0;
        int registrosTotales=0;
	    try{
		    lastNumber= Integer.parseInt(ultimoSecuencial);
	    }
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaUltimoNumeroSecuencial()"+e, EIGlobal.NivelLog.INFO);
	    }
		registrosTotales= totalRegs;
        if (lastNumber==registrosTotales){
            return true;
        }
        else{
             return false;
        }
    }
	//total de registros
    //metodo para validar el total de registros
    public boolean validaTotalDeRegistros(String totalRecords, int realTotal){
	   EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaTotalDeRegistros()&", EIGlobal.NivelLog.INFO);
		String maybeTotal;
        maybeTotal= totalRecords.trim();
        int theTotal=0;
        int totalTotal=0;
        try{
            theTotal= Integer.parseInt(maybeTotal);
        }
        catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %validaTotalDeRegistros()"+e, EIGlobal.NivelLog.INFO);
			return false;
        }

        totalTotal=realTotal-2;
        if (theTotal==totalTotal){
            return true;
        }
        else{
            return false;
        }
    }

	//importe total
    public boolean validaImporteTotal(String[] importes, String importeFinal){
	   EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaImporteTotal()&", EIGlobal.NivelLog.INFO);
		double impFinal=0;
		for(int z=0; z<importes.length; z++){
			impFinal=impFinal+Double.valueOf(importes[z]).doubleValue();
		}
		if(Double.valueOf(importeFinal).doubleValue()==impFinal)
			return true;
		else
			return false;
    }//fin del metodo

	//metodo constructor
	public ValidaArchivoPagos(BaseServlet TempBAL, String fileName){
        BAL=TempBAL;
		try{
			archivoPagos= new RandomAccessFile(fileName,"rw");

		}
		catch(Exception e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %ValidaArchivoPagos()"+e, EIGlobal.NivelLog.INFO);
		}
	}//fin del constructor

	public boolean validaTarjeta(String cuenta){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaCuenta()&", EIGlobal.NivelLog.INFO);
		String cuentaSinBlancos="";
		cuentaSinBlancos=cuenta.trim();
		if (cuentaSinBlancos.length()!=22)
			return false;
		else
			return true;
    }

	/**
	 * GAE
	 * @param month		Mes a probar
	 * @param year		A�o a probar
	 * @return numDias  Metodo que regresa el dia final de un mes en cualquier
	 * a�o, tomando en cuenta que el a�o sea bisiesto
	 */
	public int regresaDiaMes(int month, int year) {
		int numDias = 0;
		if ((year % 4) == 0) {
			switch (month) {
				case 2:
					numDias = 29;
					break;
				case 4:
					numDias = 30;
					break;
				case 6:
					numDias = 30;
					break;
				case 9:
					numDias = 30;
					break;
				case 11:
					numDias = 30;
					break;
				default:
					numDias = 31;
					break;
			}
		} else {
			switch (month) {
				case 2:
					numDias = 28;
					break;
				case 4:
					numDias = 30;
					break;
				case 6:
					numDias = 30;
					break;
				case 9:
					numDias = 30;
					break;
				case 11:
					numDias = 30;
					break;
				default:
					numDias = 31;
					break;
			}
		}
		return numDias;
	}


public void cerrar() {
	try {
		archivoPagos.close();
	}catch(Exception e) {}
}

protected void finalize() throws Throwable {
	try {
		archivoPagos.close();
	}catch(Exception e) {}
	super.finalize();
}

}//fin de la clase