/*
 * pdCalendario.java
 *
 * Created on 5 de abril de 2002, 12:28 PM
 */

package mx.altec.enlace.bo;

import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public abstract class pdCalendario {

    /**
     * Metodo para obtener los siguientes 3 meses con dias habiles e inhabiles
     * @param Hoy Fecha a partir de la cual se creara el calendario
     * @param diasInhabiles Lista con los dias inhabiles
     * @return Lista con los dias y meses
     */
    public static ArrayList getCalendario (GregorianCalendar Hoy, ArrayList diasInhabiles) {
        //GregorianCalendar fechaLimite = new GregorianCalendar(Hoy.get(Hoy.YEAR)+1,Hoy.get(Hoy.MONTH),Hoy.get(Hoy.DAY_OF_MONTH));
        //return getCalendario(diasInhabiles,Hoy,fechaLimite);
        ArrayList Calendario = new ArrayList ();
        GregorianCalendar fecha = (GregorianCalendar) Hoy.clone ();
        pdDiaHabil DiaTemp;
        pdMes MesTemp;
        int MesesExtra = 3;
        int MesAct = fecha.get (GregorianCalendar.MONTH);
        int DiasExtra = Hoy.get (GregorianCalendar.DAY_OF_MONTH);
        GregorianCalendar fechaLimite = new GregorianCalendar (Hoy.get(GregorianCalendar.YEAR)+1 ,
            Hoy.get (GregorianCalendar.MONTH)  ,
            Hoy.get (GregorianCalendar.DAY_OF_MONTH)+1 );
		EIGlobal.mensajePorTrace("\nfechalimite  "+java.text.SimpleDateFormat.getInstance().format(fechaLimite.getTime() ), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("fecha  "+java.text.SimpleDateFormat.getInstance().format(fecha.getTime())+"\n", EIGlobal.NivelLog.DEBUG);
        if (diasInhabiles == null) return null;
        MesTemp = new pdMes (fecha.get (GregorianCalendar.MONTH),
                                fecha.get (GregorianCalendar.YEAR),
                                new ArrayList ());
		/*
		 Modificacion para que devuelva un a�o completo en Modulopago directo
		 17/12/2002
		*/
//        while (fechaLimite.get (GregorianCalendar.DAY_OF_YEAR) != fecha.get (GregorianCalendar.DAY_OF_YEAR)) {
	while ( fecha.before(fechaLimite)){
		/*
		 Fin de la modificacion 17/12/2002
		*/
			if (MesAct != fecha.get (GregorianCalendar.MONTH)) {
                Calendario.add (MesTemp);
                MesAct = fecha.get (GregorianCalendar.MONTH);
                MesTemp = new pdMes (fecha.get (GregorianCalendar.MONTH),
                                        fecha.get (GregorianCalendar.YEAR),
                                        new ArrayList ());
            }
            if (!contiene (diasInhabiles, fecha)) {
                if ((fecha.get (GregorianCalendar.DAY_OF_WEEK) == 1) ||
                    (fecha.get (GregorianCalendar.DAY_OF_WEEK) == 7)) {
                    DiaTemp = new pdDiaHabil (false, (short) fecha.get(GregorianCalendar.DAY_OF_MONTH),
                                fecha.get (GregorianCalendar.DAY_OF_WEEK));
                } else {
                    DiaTemp = new pdDiaHabil (true, (short) fecha.get (GregorianCalendar.DAY_OF_MONTH),
                                fecha.get (GregorianCalendar.DAY_OF_WEEK));
                }
            } else {
                DiaTemp = new pdDiaHabil (false, (short) fecha.get(GregorianCalendar.DAY_OF_MONTH),
                            fecha.get (GregorianCalendar.DAY_OF_WEEK));
            }
            MesTemp.agregaDia (DiaTemp);
            fecha.set (GregorianCalendar.DAY_OF_YEAR, fecha.get (GregorianCalendar.DAY_OF_YEAR) + 1);
        }
        if (!Calendario.contains (MesTemp)) Calendario.add (MesTemp);
        return Calendario;

    }

    /**
     * Metodo para obtener el siguiente dia habil
     * @param Hoy Dia a partir del cual se buscara el siguiente dia habil
     * @param diasInhabiles Lista con los dias inhabiles
     * @return Siguiente dia habil
     */
    public static GregorianCalendar getSigDiaHabil (GregorianCalendar Hoy, ArrayList diasInhabiles) {
        GregorianCalendar Fecha = (GregorianCalendar) Hoy.clone ();
        if (diasInhabiles == null) return null;
        boolean diaInhabil = true;
        System.out.println("voy al While con fecha a comparar "+java.text.SimpleDateFormat.getInstance().format(Fecha.getTime()));
		//
        while (diaInhabil) {
            if (Fecha.get (GregorianCalendar.DAY_OF_WEEK) == 1) {
				System.out.println("dia de la semana = 1");
                Fecha.set (GregorianCalendar.DAY_OF_YEAR, Fecha.get (GregorianCalendar.DAY_OF_YEAR) + 1);
            } else if (Fecha.get (GregorianCalendar.DAY_OF_WEEK) == 7) {
				System.out.println("Dia de la semana = 7");
                Fecha.set (GregorianCalendar.DAY_OF_YEAR, Fecha.get (GregorianCalendar.DAY_OF_YEAR) + 2);
            }
            if (contiene(diasInhabiles,Fecha)) {
			//if(diasInhabiles.contains(Fecha)){
				System.out.println("verifica");
                Fecha.set (GregorianCalendar.DAY_OF_YEAR, Fecha.get (GregorianCalendar.DAY_OF_YEAR) + 1);
				System.out.println("SE sumo un dia a la fecha");
            } else {
                diaInhabil = false;
				System.out.println(" no Contiene");
            }
        }
        return Fecha;
    }

    /**
     * Metodo para obtener el dia habil anterior
     * @param Dia Dia tentativo
     * @param diasInhabiles Lista de dias inhabiles
     * @return Dia anterior habil
     */
    public static GregorianCalendar getAntDiaHabil (GregorianCalendar Dia, ArrayList diasInhabiles) {
        GregorianCalendar Fecha = (GregorianCalendar) Dia.clone ();
        if (diasInhabiles == null) return null;
        boolean diaInhabil = true;
        while (diaInhabil) {
            if (Fecha.get (GregorianCalendar.DAY_OF_WEEK) == 1) {
                Fecha.set (GregorianCalendar.DAY_OF_YEAR, Fecha.get (GregorianCalendar.DAY_OF_YEAR) - 2);
            } else if (Fecha.get (GregorianCalendar.DAY_OF_WEEK) == 7) {
                Fecha.set (GregorianCalendar.DAY_OF_YEAR, Fecha.get (GregorianCalendar.DAY_OF_YEAR) - 1);
            }
            //if (diasInhabiles.contains (Fecha)) {
			if(contiene(diasInhabiles, Fecha)){
                Fecha.set (GregorianCalendar.DAY_OF_YEAR, Fecha.get (GregorianCalendar.DAY_OF_YEAR) - 1);
            } else {
                diaInhabil = false;
            }
        }
        return Fecha;
    }

      /**
     * Metodo para ver si un dia se encuentra en una lista de dias
     * @param diasInhabiles Lista con los dias inhabiles
     * @param fecha Fecha que se quiere buscar en la lista
     * @return true si la fecha esta en la lista de lo contrario false
     */
    protected static boolean contiene (java.util.List diasInhabiles, GregorianCalendar fecha) {
        ListIterator liDiasInhabiles = diasInhabiles.listIterator ();
        GregorianCalendar fechaComp;
        while (liDiasInhabiles.hasNext ()) {
            fechaComp = (GregorianCalendar) liDiasInhabiles.next ();
            if ((fechaComp.get(fechaComp.YEAR) == fecha.get(fecha.YEAR) )&&
                (fechaComp.get (GregorianCalendar.DAY_OF_YEAR) ==
                fecha.get (GregorianCalendar.DAY_OF_YEAR)) ){
                //System.out.println("Contiene:" + java.text.SimpleDateFormat.getInstance().format(fecha.getTime()) + " - "
                //    + java.text.SimpleDateFormat.getInstance().format(fechaComp.getTime()));
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo para obtener un calendario con las fechas habiles en un periodo de tiempo definido RMM 20021213
     * @param diasInhabiles Lista con los dias inhabiles
     * @param fechaIni fecha inicial para el calendario
     * @param fechaFin fecha final para el calendario
     * @return Lista con los dia habiles
     */
     public static ArrayList getCalendario(java.util.List diasInhabiles, GregorianCalendar fechaIni, GregorianCalendar fechaFin){
         ArrayList retValue = new ArrayList();
         GregorianCalendar fecha = (GregorianCalendar) fechaIni.clone ();
        pdDiaHabil DiaTemp;
        pdMes MesTemp;
        int MesesExtra = 3;
        int MesAct = fecha.get (GregorianCalendar.MONTH);;
        int DiasExtra = fechaIni.get (GregorianCalendar.DAY_OF_MONTH);
        GregorianCalendar fechaLimite = (GregorianCalendar) fechaFin.clone();
		EIGlobal.mensajePorTrace("fechaFin  "+java.text.SimpleDateFormat.getInstance().format(fechaLimite.getTime() ) , EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("fechaIni  "+java.text.SimpleDateFormat.getInstance().format(fecha.getTime())+"\n" , EIGlobal.NivelLog.DEBUG);
        if (diasInhabiles == null) return null;


        MesTemp = new pdMes (fecha.get (GregorianCalendar.MONTH),
                                fecha.get (GregorianCalendar.YEAR),
                                new ArrayList ());
        while (fechaLimite.get (GregorianCalendar.DAY_OF_YEAR)
                != fecha.get (GregorianCalendar.DAY_OF_YEAR) ) {

		//EIGlobal.mensajePorTrace("Limite  "+fechaLimite.get (GregorianCalendar.DAY_OF_YEAR)+" vs "+fecha.get (GregorianCalendar.DAY_OF_YEAR) , EIGlobal.NivelLog.DEBUG);

            if (MesAct != fecha.get (GregorianCalendar.MONTH)) {
                //System.out.println("MesAct != fecha");
                retValue.add (MesTemp);
                MesAct = fecha.get (GregorianCalendar.MONTH);
                MesTemp = new pdMes (fecha.get (GregorianCalendar.MONTH),
                                        fecha.get (GregorianCalendar.YEAR),
                                        new ArrayList ());
            }
            //System.out.println( java.text.SimpleDateFormat.getInstance().format(fecha.getTime()));
            if (!contiene (diasInhabiles, fecha)) {
                //System.out.println("! contiene(inh,fecha)," );
                if ((fecha.get (GregorianCalendar.DAY_OF_WEEK) == 1) ||
                    (fecha.get (GregorianCalendar.DAY_OF_WEEK) == 7)) {
                    DiaTemp = new pdDiaHabil (false, (short) fecha.get(GregorianCalendar.DAY_OF_MONTH),
                                fecha.get (GregorianCalendar.DAY_OF_WEEK));
                } else {
                    DiaTemp = new pdDiaHabil (true, (short) fecha.get (GregorianCalendar.DAY_OF_MONTH),
                                fecha.get (GregorianCalendar.DAY_OF_WEEK));
                }
            } else {
                DiaTemp = new pdDiaHabil (false, (short) fecha.get(GregorianCalendar.DAY_OF_MONTH),
                            fecha.get (GregorianCalendar.DAY_OF_WEEK));
            }
            MesTemp.agregaDia (DiaTemp);
            fecha.set (GregorianCalendar.DAY_OF_YEAR, fecha.get (GregorianCalendar.DAY_OF_YEAR) +1);
        }
        if (!retValue.contains (MesTemp)) retValue.add (MesTemp);
        return retValue;
     }


}
