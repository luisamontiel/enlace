package mx.altec.enlace.bo;

import java.net.*;
import java.io.*;

public class classConfigura
{
	private static String ARCHIVO_CFG = "";

	public  classConfigura(String archivo)
	{
		ARCHIVO_CFG = archivo;
	}

	public static String actualiza(String pParametro, String pTipoDato)
	{
		String linea     =" ";
		String Respuesta = "";
		String valor     = "";
		String datos     = "";
		int contadorS1 = 0;
		int signo;

		try {
					File ar = new File(ARCHIVO_CFG);
					FileReader conf = new FileReader(ar);
					BufferedReader fi = new BufferedReader(conf);
		    do{
			    signo = -1;
				linea = fi.readLine();
				if(linea!= null)
				{
					linea = linea.trim();
					if(linea.length()>1)
					{
						if (linea.charAt(0) != '#')
							signo = linea.indexOf("=");
						if (signo != -1)
						{
							valor = linea.substring(0, signo).trim().toUpperCase();
							datos = linea.substring(signo+1, linea.length()).trim();
							if (valor.equals(pParametro))
							{
								if(pTipoDato.equals("unico"))
								{
									Respuesta = datos;
									break;
								}
								else if(pTipoDato.equals("trama"))
								{
									Respuesta += datos + "!";
									contadorS1++;
								}
							}
						}
					}
				}
			}while (linea != null);

		} catch (FileNotFoundException e) {
			System.out.println("ALERTA... no existe el archivo de configuracion...");
		} catch (IOException e) {
			System.out.println("Error en lectura de Configuracion");
		} catch (Exception e) {
			System.out.println(e);
		}

		if(pTipoDato.equals("trama"))
		{
			Respuesta = contadorS1 + "!" + Respuesta;
		}
		return Respuesta;
	}

	public static String[] desentramaC(String buffer, char Separador)
	{
	  String bufRest;

	  int cont;
	  String[] arregloSal=null;
	  try {
	  int intNoCont = Integer.parseInt(buffer.substring(0,buffer.indexOf(Separador)));
	  //log("numero de elementos de desentramaC : " + intNoCont );
	  arregloSal= new String[intNoCont+1];
	  arregloSal[0] = buffer.substring(0,buffer.indexOf(Separador));
	  bufRest = buffer.substring(buffer.indexOf(Separador) + 1,buffer.length());
	  for(cont=1;cont<=intNoCont;cont++)
	  {
		arregloSal[cont] = bufRest.substring(0,bufRest.indexOf(Separador));
		bufRest = bufRest.substring(bufRest.indexOf(Separador) + 1,bufRest.length());
	  }
	  }catch(Exception s) {
	   System.out.println(">>>>>>>>>> desentrama Exception: "+s.toString()+" <<<<<<<<<<<");
	  arregloSal= new String[1];
	  arregloSal[0]="0";
	  }
	  return arregloSal;
	}

}
