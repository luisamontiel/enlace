package mx.altec.enlace.bo;

import java.util.Vector;

//####################################################################
//
//      Modificacion: Se agregan mensajes de error para nuevos importes
//      Proyecto    : SUA 2005
//      Cod         : MX2005-21100
//      Programador : Carlos Aleman Rosas.
//      clave en cod: W3xx
//#################################################################
public class SuaErrorDisco {
	public String nombreArchivo;

	public int error;

	public int numeroRegistro;

	public boolean abortar;
	public static boolean bBanderaErrores;

	public SuaErrorDisco() {
		nombreArchivo = "";
		error = 0;
		numeroRegistro = 0;
		abortar = true;
	}

	public SuaErrorDisco(int paramError, int paramNumero, String paramNombre,
			boolean paramAbortar) {
		nombreArchivo = new String(paramNombre);
		error = paramError;
		numeroRegistro = paramNumero;
		abortar = paramAbortar;
	}

	public void asignaError(int paramError, int paramNumero,
			String paramNombre, boolean paramAbortar, Vector suaErrores) {
		nombreArchivo = new String(paramNombre);
		numeroRegistro = paramNumero;
		abortar = paramAbortar;

		if (bBanderaErrores) {
			error = paramError;
			SuaVerificaDisco.ErroresMostrar=msgError(error);

			/** Modificación para funcionamiento de manejo de errores */
			suaErrores.addElement(this);//SuaVerificaDisco.ErroresMostrar);

			System.out.println("________________" + msgError(error)); // Desarrollo
			bBanderaErrores=false;
		}

	}

	static public String msgError(int error) {
		switch (error) {
		case 102:
			return ("Error en el formato de la informaci&oacute;n, se esperaba registro de Encabezado de Patr&oacute;n");
		case 103:
			return ("Error en el formato de la informaci&oacute;n, se esperaba registro del trabajador");
		case 104:
			return ("Error en el formato de la informaci&oacute;n, se esperaba registro de Movimientos del trabajador");
		case 105:
			return ("Error en el formato, se espera registro del Sumario");
		case 106:
			return ("Error en el formato de la informaci&oacute;n, se esperaba registro de Validaci&oacute;n");
		case 201:
		case 202:
			return ("Error al leer el archivo");
		case 1001:
			return ("Checksum inv&aacute;lido, la informaci&oacute;n ha sido alterada desde su generaci&oacute;n por el SUA");
		case 1002:
			return ("La versi&oacute;n del archivo SUA no es v&aacute;lida");
		case 1003:
			//return ("Ha caducado la fecha l&iacute;mite de pago, generar recargos y actualizaci&oacute;n");
			return ("El mes del per&iacute;odo de pago es non y contiene importes de RCV y Vivienda");
		case 1004:
			return ("La fecha l&iacute;mite de pago ha transcurrido");
		case 1005:
			return ("La versi&oacute;n del SUA es inv&aacute;lida");
		case 1006:
			return ("El nombre del archivo de pago no corresponde a los datos que contiene");
		case 1007:
			return ("El archivo no fue generado por el SUA");
		case 1008:
			return ("El pago fue presentado sin importes");
		case 1009:
			return ("La cantidad de discos no corresponde con los esperados");
		case 1010:
			return ("Folio duplicado");
		case 1011:
			return ("Per&iacute;odo de pago inv&aacute;lido");
		case 1012:
			return ("El mes del per&iacute;odo de pago es non y contiene importes de RCV y Vivienda");
		case 1013:
			return ("El folio tiene caracteres en blanco");
		case 1014:
			return ("El dato de Tipo de Cotizaci&oacute;n no es num&eacute;rico");
		case 1015:
			return ("Existen diferencias en el n&uacute;nmero de trabajadores registrados");
		case 1016:
			return ("Error en el registro patronal del sumario, no coincide con el encabezado");
		case 1017:
			return ("El per&iacute;odo de pago no coincide con el Registro 02");
		case 1018:
			return ("El dato de No de Seguridad Social del trabajador no es num&eacute;rico");
		case 1019:
			return ("Existen trabajadores donde el dato d&iacute;as cotizados no es num&eacute;rico");
		case 1020:
			return ("Existen trabajadores con importes no num&eacute;ricos o negativos");
		case 1021:
			return ("Existen incidencias con diferente registro patronal");
		case 1022:
			return ("Existen incidencias donde el NSS no contiene 11 posiciones o no es num&eacute;rico");
		case 1023:
			return ("El sumario contiene diferente registro patronal");
		case 1024:
			return ("El sumario contiene diferente per&iacute;odo de pago");
		case 1025:
			return ("El sumario contiene diferente folio");
		case 1026:
			return ("El Registro Patronal no puede estar vac&iacute;o o no contiene 11 posiciones");
		case 1027:
			return ("La suma de los parciales no coinciden con el importe del sumario 4 RSS");
		case 1028:
			return ("La suma de los parciales no coinciden con el importe del sumario de act. y recargos de 4 RSS");
		case 1029:
			return ("La suma de los parciales no coinciden con el importe del sumario del ramo de retiro");
		case 1031:
			return ("La suma de los parciales no coincide con el importe del sumario de act. y recargos de RCV");
		case 1032:
			return ("La suma de los parciales no coincide con el importe del sumario de voluntaria");
		case 1033:
			return ("La suma de los parciales no coincide con el importe del sumario de Vivienda");
		case 1034:
			return ("La suma de los parciales no coincide con el importe del sumario de Vivienda");
		case 1035:
			return ("El formato de validaci&oacute;n contiene un registro patronal diferente");
		case 1036:
			return ("El formato de validaci&oacute;n contiene un per&iacute;odo de pago diferente");
		case 1037:
			return ("El formato de validaci&oacute;n contiene un folio diferente");
		case 1038:
			return ("No coincide el formato de validaci&oacute;n con el sumario en 4RSS");
		case 1039:
			return ("El dato de Total a depositar en cuenta IMSS RCV (Banxico) no es num&eacute;rico");
		case 1040:
			return ("El dato de Total a depositar en la cuenta INFONAVIT (Banxico) Aportaci&oacute;n Patronal para cuenta individual no es num&eacute;rico");
		case 1041:
			return ("El Total a depositar en cuenta INFONAVIT Amortizaci&oacute;n de Cr&eacute;ditos no coincide con el Sumario");
		case 1042:
			return ("Fecha l&iacute;mite de pago sin datos");
		case 1043:
			return ("Versi&oacute;n SUA Inv&aacute;lida");
		case 1044:
		case 1045:
			return ("El dato de Porcentaje de Aportaci&oacute;n Patronal INFONAVIT no es num&eacute;rico");
		case 1046:
			return ("Error en dato de Reembolso de Subsidios");
		case 1047:
			return ("Error en dato Tipo de Cotizaci&oacute;n");
		case 1048:
			return ("El dato de &Uacute;ltimo salario diario integrado del per&iacute;odo no es num&eacute;rico");
		case 1049:
			return ("El dato de D&iacute;as de incapacidad en el mes no es num&eacute;rico");
		case 1050:
			return ("El dato de D&iacute;as de ausentismo en el mes no es num&eacute;rico");
		case 1051:
			return ("El dato de D&iacute;as de incapacidad en el bimestre no es num&eacute;rico");
		case 1052:
			return ("El dato de D&iacute;as de ausentismo en el bimestre no es num&eacute;rico");
		case 1053:
			return ("El dato de Aportaciones Complementarias no es num&eacute;rico");
		case 1054:
			return ("El dato de Multas Infonavit no es num&eacute;rico");
		case 1055:
			return ("El dato de Donativo Fundemex no es num&eacute;rico");
		case 1056:
			return ("El Importe de las aportaciones Patronal INFONAVIT de los registros 03 no coinciden contra el sumario");
		case 1057:
			return ("El Importe de aportaciones Patronal INFONAVIT de amortizaci&oacute;n de cr&eacute;dito de los registros 03 no coinciden contra el sumario");
		case 1059:
			return ("Valor inv&aacute;lido del &aacute;rea geogr&aacute;fica");
		case 1060:
			return ("Valor inv&aacute;lido del Convenio de Reembolso");
		case 1061:
			return ("El total de los d&iacute;as cotizados no coincide con el detalle de los trabajadores");
		case 1062:
			return ("El dato de d&iacute;as cotizados en el per&iacute;odo sin descontar incidencias no es num&eacute;rico");
		case 1063:
			return ("N&uacute;mero de trabajadores cotizantes no coincide con el detalle de trabajadores");
		case 1064:
			return ("El dato de N&uacute;mero de trabajadores cotizantes no es num&eacute;rico");
		case 1065:
			return ("Error en el registro patronal de validaci&oacute;n, no coincide con el encabezado");
		case 1066:
			return ("Error en RFC del patr&oacute;n, no coincide con el Registro 02");
		case 1067:
			return ("Error en el per&iacute;odo de validaci&oacute;n, no coincide con el encabezado");
		case 1068:
			return ("El dato de Folio SUA no es num&eacute;rico");
		case 1069:
			return ("La fecha l&iacute;mite de pago ha expirado. El rango de fechas es inv&aacute;lido, favor de verificarlo");
		case 1070:
			return ("Error en el registro patronal, no coincide con el encabezado");
		case 1071:
			return ("Error en el formato de la informaci&oacute;n, se esperaba registro Sumario");
		case 1072:
			return ("Error en el registro patronal no coincide con el Registro 02");
		case 1073:
			return ("Error en el RFC del sumario, no coincide con el encabezado");
		case 1074:
			return ("Error en el per&iacute;odo del sumario, no coincide con el encabezado");
		case 1075:
			return ("Error en el Folio SUA del sumario, no coincide con el encabezado");
		case 1077:
			return ("El dato contiene caracteres inv&aacute;lidos");
		case 1078:
			return ("El dato no es num&eacute;rico");
		case 1079:
			return ("Error en el RFC del patr&oacute;n, no coincide con el Registro 02");
		case 1080:
			return ("Registro de Trabajador Duplicado");
		case 1081:
			return ("El RFC del trabajador presenta caracteres inv&aacute;lidos");
		case 1082:
			return ("Error en el formato de Clave Unica del Registro de Poblaci&oacute;n");
		case 1083:
			return ("Error en el formato del Registro numero de Credito INFONAVIT");
		case 1084:
			return ("Error en el formato del Registro  Fecha de inicio del descuento del Credito INFONAVIT");
		case 1085:
			return ("El dato de n&uacute;mero de movimientos en el per&iacute;odo no es num&eacute;rico");
		case 1086:
			return ("El dato de tipo de Trabajador no es num&eacute;rico");
		case 1087:
			return ("Valor de Jornada /Semana reducida es inv&aacute;lido");
		case 1088:
			return ("El Nombre del Trabajador no contiene los dos signos de pesos");
		case 1089:
			return ("Existe doble espacio en el nombre entre palabras");
		case 1290:
			return ("El dato de D&iacute;as cotizados en el mes no es num&eacute;rico");
		case 1291:
			return ("El dato de Cuota Fija de Enfermedad y Maternidad no es num&eacute;rico");
		case 1292:
			return ("El dato de Cuota Excedente de Enfermedad y Maternidad no es num&eacute;rico");
		case 1293:
			return ("El dato de Prestaciones en Dinero de Enfermedad y Maternidad no es num&eacute;rico");
		case 1294:
			return ("El dato de Gastos M&eacute;dicos Pensionados no es num&eacute;rico");
		case 1295:
			return ("El dato de Riesgos de Trabajo no es num&eacute;rico");

		case 1200:
			return ("El dato de N&uacute;mero de Seguridad Social del Trabajador no es num&eacute;rico");
		case 1201:
			return ("El dato de Tipo de movimiento o incidencia no es num&eacute;rico");
		case 1202:
			return ("El dato de Salario Diario Integrado no es num&eacute;rico");
		case 1203:
			return ("El dato de N&uacute;mero de Seguridad Social del Trabajador no es num&eacute;rico");
		case 1090:
			return ("El dato Multas INFONAVIT no es num&eacute;rico");
		case 1091:
			return ("El dato Aportaciones complementarias no es num&eacute;rico");
		 //////////////////////////////////////////////////////////////////////////////////////////////////
		case 1092: return("El dato Recargos de aportaci&oacute;n patronal no es num&eacute;rico");
		case 1093: return("El dato Actualizaci&oacute;n de aportaci&oacute;n patronal no es num&eacute;rico");
		case 1094: return("El dato Actualizaci&oacute;n de aportaci&oacute;n patronal no es num&eacute;rico");
		case 1095: return("El dato de Aportaci&oacute;n Patronal INFONAVIT para Amortizaci&oacute;n de Cr&eacute;ditos no es num&eacute;rico");
		case 1096: return("El dato de Aportaci&oacute;n Patronal Infonavit no es num&eacute;rico");
		case 1097: return("La suma de las Aportaciones Complementarias no coincide con el detalle de los movimientos");
		case 1098: return("El dato FILLER no contiene espacios");
		case 1300: return("El Registro Patronal no puede estar vac&iacute;o o no contiene 11 posiciones"); //
		case 2001: return("El dato registro patronal IMSS no es num&eacute;rico");
		case 2002: return("El dato RFC Patr&oacute;n contiene caracteres inv&aacute;lidos");
		case 2003: return("El dato per&iacute;odo contiene caracteres inv&aacute;lidos");
		case 2004: return("El dato Folio SUA contiene caracteres inv&aacute;lidos");
		case 2005: return("El dato de Tasa de actualizaci&oacute;n empleada no es num&eacute;rico");
		case 2006: return("El dato de Tasa de recargos empleada no es num&eacute;rico");
		case 2008: return("El dato de Cuota Fija de Enfermedad y Maternidad no es num&eacute;rico");
		case 2009: return("La suma de los parciales no coinciden con el importe del sumario de C. Fija");
		case 2010: return("El dato de Cuota Excedente de Enfermedad y Maternidad no es num&eacute;rico");
		case 2011: return("La suma de los parciales no coinciden con el importe del sumario de C. Exced.");
		case 2012: return("El dato de Prestaciones en Dinero de Enfermedad y Maternidad no es num&eacute;rico");
		case 2013: return("La suma de los parciales no coinciden con el importe del sumario de PD.");
		case 2014: return("El dato de Gastos M&eacute;dicos Pensionados de Enfermedad y Maternidad no es num&eacute;rico");
		case 2015: return("La suma de los parciales no coinciden con el importe del sumario de GMP (Art. 25)");
		case 2016: return("El dato de Riesgos de Trabajo no es num&eacute;rico");
		case 2017: return("La suma de los parciales no coinciden con el importe del sumario de RT");
		case 2018: return("El dato de Invalidez y Vida no es num&eacute;rico");
		case 2019: return("La suma de los parciales no coinciden con el importe del sumario de IV");
		case 2020: return("El dato de Subtotal de cuatro Seguros IMSS no es num&eacute;rico");
		case 2021: return("La suma de los parciales no coinciden con el importe del sumario de GPS");
		case 2022: return("El dato de Total a depositar en cuenta IMSS 4RSS (Banxico) no es num&eacute;rico");
		case 2023: return("La suma de los parciales no coincide con el importe del sumario de 4 RSS");
		case 2024: return("El dato de Actualizaci&oacute;n de los cuatro Seguros IMSS no es num&eacute;rico");
		case 2025: return("El dato de los recargos de los cuatro seguros IMSS no es num&eacute;rico");
		case 2036: return("La suma de los parciales no coinciden con el importe del sumario de act. y rec. de 4 RSS");
		case 2026: return("El dato de Retiro no es num&eacute;rico");
		case 2027: return("La suma de los parciales no coinciden con el importe del sumario del Ramo de Retiro");
		case 2028: return("El dato de Cesant&iacute;a y Vejez cuota trabajador no  es num&eacute;rico");
		case 2029: return("La suma de los parciales no coinciden con el importe del sumario del Ramo de CV");
		case 2030: return("El dato de Subtotal Retiro, Cesant&iacute;a y Vejez no es num&eacute;rico");
		case 2031: return("La suma de los parciales no coinciden con el importe del sumario del Ramo de CV");
		case 2032: return("El dato de Actualizaci&oacute;n de Retiro, Cesant&iacute;a y Vejez no es num&eacute;rico");
		case 2033: return("El dato de Recargos de Retiro, Cesant&iacute;a y Vejez no es num&eacute;rico");
		case 2034: return("El dato de Aportaciones Voluntarias no es num&eacute;rico");
		case 2035: return("La suma de los parciales no coinciden con el importe del sumario de Aportaciones Voluntarias");
		case 2037: return("La suma de los parciales no coinciden con el importe del sumario de act. y rec. de RCV");
		case 2038: return("El dato de Amortizaci&oacute;n de Cr&eacute;dito Infonavit no es num&eacute;rico");
		case 2039: return("La suma de Amortizaci&oacute;n de Cr&eacute;dito INFONAVIT no coincide con el detalle de trabajadores");
		case 2040: return("El dato de Actualizaci&oacute;n de Aportaci&oacute;n Patronal y Amortizaci&oacute;n INFONAVIT no es num&eacute;rico");
		case 2041: return("El dato de Recargos de Aportaci&oacute;n Patronal y Amortizaci&oacute;n INFONAVIT no es num&eacute;rico");
		case 2100: return("Existe doble espacio en la raz&oacute;n social entre palabras");


		case 2200: return ("El dato de N&uacute;mero de discos no es num&eacute;rico");
		case 2201: return ("El Total a depositar en la cuenta INFONAVIT Aportaci&oacute;n Patronal no coincide con el Sumario");
		case 2202: return ("El nombre del archivo de pago no corresponde a los generados por el SUA");
		case 2203: return ("El dato de Nombre del Trabajador tiene caracteres inv&aacute;lidos");
		case 2204: return ("Pago duplicado");
		case 2205: return ("El dato de Tama&ntilde;o del archivo en bytes no es num&eacute;rico");
		case 2206: return ("La versi&oacute;n del SUA es inv&aacute;lida, la versi&oacute;n del disco es W400");
		case 2207: return ("El Total a depositar en cuenta IMSS RCV no coincide con el Sumario");
		case 2208: return ("No coincide el formato de validaci&oacute;n con el sumario en 4RSS");
		case 2209: return ("El dato de Total a depositar en cuenta INFONAVIT (Banxico) Amortizaci&oacute;n de Cr&eacute;ditos, actualizaciones y recargos no es num&eacute;rico");
		case 2210: return ("El importe de Amortizaci&oacute;n de Cr&eacute;dito INFONAVIT de los registros 03 no coincide contra el sumario");
		case 2211: return ("El dato de D&iacute;as de incidencia no es num&eacute;rico");
		case 2212: return ("La suma de Subtotal RCV no coincide con el Sumario");
		case 2213: return ("La suma de Aportaci&oacute;n Patronal INFONAVIT para Cuenta Individual no coincide con el detalle de trabajadores"); //
		case 2214: return ("La suma de Aportaci&oacute;n patronal INFONAVIT para Amortizaci&oacute;n de cr&eacute;ditos no coincide con el detalle de trabajadores"); //
		case 2215: return ("El dato de suma de Tasas de Recargos y Actualizaci&oacute;n que se aplic&oacute; no es num&eacute;rico");
		case 2216: return ("El dato de Aportaci&oacute;n Patronal INFONAVIT para Cuenta Individual no es num&eacute;rico");
		case 2217: return ("La suma de los parciales no coinciden con el importe del sumario de act. y rec. de 4 RSS");
		case 2218: return ("Tama&ntilde;o del archivo no corresponde con el originalmente escrito");
		/* Cambio GAO Stefanini IT Solutions 18/07/2008 */
		case 6001: return("El per&iacute;odo de pago no coincide con el Registro 02");
		case 6002: return("El Folio SUA de validaci&oacute;n, no coincide con el encabezado");
		case 6003: return("Error en formato de fecha l&iacute;mite de pago");
		case 6004: return("El dato de Entidad Federativa no es num&eacute;rico");
		case 6005: return("El dato de C&oacute;digo Postal no es num&eacute;rico");
		case 6006: return("El dato de Prima de Riesgo de Trabajo no es num&eacute;rico");
		case 6007: return("El dato de Delegaci&oacute;n IMSS no es num&eacute;rico");
		case 6008: return("El dato de Subdelegaci&oacute;n IMSS no es num&eacute;rico");
		case 6009: return("El dato de Invalidez y Vida no es num&eacute;rico");
		case 6010: return("El dato de Guarder&iacute;a y Prestaciones Sociales no es num&eacute;rico");
		case 6011: return("El dato de Actualizaci&oacute;n y Recargos de los cuatro Seguros IMSS no es num&eacute;rico");
		case 6012: return("El dato de D&iacute;as cotizados en el bimestre no es num&eacute;rico");
		case 6015: return("El dato de Retiro no es num&eacute;rico");
		case 6016: return("El dato de Actualizaci&oacute;n y Recargos de Retiro no es num&eacute;rico");
		case 6017: return("El dato de Cesant&iacute;a y Vejez cuota patronal no es num&eacute;rico");
		case 6019: return("El dato de Actualizaci&oacute;n y Recargos de Cesant&iacute;a y Vejez no es num&eacute;rico");

		/* Fin GAO */

			default:
			 return("Error no definido");
		}
	}
}