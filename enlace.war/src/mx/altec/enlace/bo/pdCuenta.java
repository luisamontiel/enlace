/*
 * pdCuentas.java
 *
 * Created on 4 de abril de 2002, 12:13 PM
 */

package mx.altec.enlace.bo;
/**
 *
 * @author  Rafael Martinez Montiel
 * @version 1.0
 */
public class pdCuenta implements java.io.Serializable{

    /** Holds value of property noCta. */
    private String noCta;

    /** Holds value of property fechaAlta. */
    private String fechaAlta;

    /** Holds value of property noRef. */
    private String noRef;

    /** Getter for property noCta.
     * @return Value of property noCta.
     */
    public String getNoCta() {
        return noCta;
    }

    /** Setter for property noCta.
     * @param noCta New value of property noCta.
     */
    public void setNoCta(String noCta) {
        this.noCta = noCta;
    }

    /** Getter for property fechaAlta.
     * @return Value of property fechaAlta.
     */
    public String getFechaAlta() {
        return fechaAlta;
    }

    /** Setter for property fechaAlta.
     * @param fechaAlta New value of property fechaAlta.
     */
    public void setFechaAlta(String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /** Getter for property noRef.
     * @return Value of property noRef.
     */
    public String getNoRef() {
        return noRef;
    }

    /** Setter for property noRef.
     * @param noRef New value of property noRef.
     */
    public void setNoRef(String noRef) {
        this.noRef = noRef;
    }

}
