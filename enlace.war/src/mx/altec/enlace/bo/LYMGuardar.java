package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import java.util.ArrayList;

import mx.altec.enlace.beans.AdmLyMGL24;
import mx.altec.enlace.beans.AdmLyMGL26;
import mx.altec.enlace.dao.AdmLyMLimitesContratoDAO;
import mx.altec.enlace.dao.AdmLyMLimitesOperacionDAO;
import mx.altec.enlace.gwt.adminlym.shared.BeanAdminLyM;
import mx.altec.enlace.gwt.adminlym.shared.LimitesContrato;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Util;

public class LYMGuardar {

	private BeanAdminLyM beanIni;
	private BeanAdminLyM beanFin;
	private int totalOperaciones;

	private LimitesContrato limFin;
	private AdmLyMGL24 resContrato;

	private ArrayList<LimitesOperacion> agregados;
	private ArrayList<LimitesOperacion> modificados;
	private ArrayList<LimitesOperacion> borrados;
	private ArrayList<AdmLyMGL26> resUsuario;


	public AdmLyMGL24 getResContrato() {
		return resContrato;
	}

	public ArrayList<AdmLyMGL26> getResUsuario() {
		return resUsuario;
	}

	public LimitesContrato getLimFin() {
		return limFin;
	}

	public ArrayList<LimitesOperacion> getAgregados() {
		return agregados;
	}

	public ArrayList<LimitesOperacion> getModificados() {
		return modificados;
	}

	public ArrayList<LimitesOperacion> getBorrados() {
		return borrados;
	}

	public LYMGuardar(BeanAdminLyM beanIni, BeanAdminLyM beanFin) {
		this.beanFin = beanFin;
		this.beanIni = beanIni;
		totalOperaciones = 0;
		resUsuario = new ArrayList<AdmLyMGL26>();
	}

	public int guardaLYM() {
		if(!comparaLYMContratos().equals("") && limFin != null) {
			EIGlobal.mensajePorTrace("Bean de Contrato", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Opcion <" + limFin.getOpcion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Tipo Oper <" + limFin.getTipoOperacion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Contrato <" + limFin.getContrato() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Monto <" + limFin.getMonto() + ">", EIGlobal.NivelLog.DEBUG);
			//guarda el l�mite de contrato

			AdmLyMLimitesContratoDAO limCtr = new AdmLyMLimitesContratoDAO();
	    	MQQueueSession mqsess = null;
	    	resContrato = null;
	    	try {
	    		mqsess = new MQQueueSession(
	    				NP_JNDI_CONECTION_FACTORY,
	    				NP_JNDI_QUEUE_RESPONSE,
	    				NP_JNDI_QUEUE_REQUEST);
	    		limCtr.setMqsess(mqsess);
	    		resContrato = limCtr.mantenimientoLimitesMontos(limFin.getOpcion(), limFin.getTipoOperacion(), "", limFin.getContrato(), Util.rellenaCeros(limFin.getMonto(), 17));
	    		EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim contrato - " +
			    		"Respuesta: [" + resContrato.getCodigoOperacion() +  "]", EIGlobal.NivelLog.DEBUG);
	    	} catch (Exception x) {
			    EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim contrato -" +
		    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
	    	} finally {
	    		try {
	    			mqsess.close();
	    		}catch(Exception x) {
				    EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim contrato -" +
				    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
	    		}
	    	}

		}
		else {
			limFin = null;
		}
		guardaLYMUsuarios();
		EIGlobal.mensajePorTrace("Total de Operaciones aplicadas <" + totalOperaciones + ">", EIGlobal.NivelLog.DEBUG);
		return 0;
	}

	public int getTotalOperaciones() {
		return totalOperaciones;
	}

	private void guardaLYMUsuarios() {
		agregados = beanFin.getListaLimitesAgregados();
		modificados = beanFin.getListaLimitesModificados();
		borrados = beanFin.getListaLimitesEliminados();



		AdmLyMLimitesOperacionDAO limOpe = new AdmLyMLimitesOperacionDAO();
    	MQQueueSession mqsess = null;
    	AdmLyMGL26 res = null;
    	try {
    		mqsess = new MQQueueSession(
    				NP_JNDI_CONECTION_FACTORY,
    				NP_JNDI_QUEUE_RESPONSE,
    				NP_JNDI_QUEUE_REQUEST);
    		limOpe.setMqsess(mqsess);
    	} catch (Exception x) {
		    EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim usuario -" +
	    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
		    return;	//implementar c�d error.
    	}finally{ //CSA
    		if(mqsess!=null){
		    	try {
					mqsess.close();
				}catch(Exception x) {
				    EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim contrato -" +
				    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
				}
    		}
		}

		for(LimitesOperacion agregado: agregados) {
			//agregar
			agregado.setOpcion("AL");
			EIGlobal.mensajePorTrace("Bean de Usuario Agregado", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Opcion <" + agregado.getOpcion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Contrato <" + agregado.getContrato() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Usuario <" + agregado.getUsuario() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Grupo <" + agregado.getClaveOperacion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Monto <" + agregado.getLimiteOperacion() + ">", EIGlobal.NivelLog.DEBUG);
			//guarda el l�mite que corresponda.
			res = limOpe.mantenimientoLimitesOperacion(agregado.getOpcion(), agregado.getContrato(), agregado.getUsuario(), agregado.getClaveOperacion(), Util.rellenaCeros(agregado.getLimiteOperacion(), 17));
			resUsuario.add(res);
    		EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim contrato - " +
		    		"Respuesta: [" + res.getCodigoOperacion() +  "]", EIGlobal.NivelLog.DEBUG);

		}
		for(LimitesOperacion modificado: modificados) {
			modificado.setOpcion("MO");
			EIGlobal.mensajePorTrace("Bean de Usuario Modificado", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Opcion <" + modificado.getOpcion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Contrato <" + modificado.getContrato() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Usuario <" + modificado.getUsuario() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Grupo <" + modificado.getClaveOperacion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Monto <" + modificado.getLimiteOperacion() + ">", EIGlobal.NivelLog.DEBUG);
			res = limOpe.mantenimientoLimitesOperacion(modificado.getOpcion(), modificado.getContrato(), modificado.getUsuario(), modificado.getClaveOperacion(), Util.rellenaCeros(modificado.getLimiteOperacion(), 17));
			resUsuario.add(res);
    		EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim contrato - " +
		    		"Respuesta: [" + res.getCodigoOperacion() +  "]", EIGlobal.NivelLog.DEBUG);

		}
		for(LimitesOperacion borrado: borrados) {
			borrado.setOpcion("BA");
			EIGlobal.mensajePorTrace("Bean de Usuario Borrado", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Opcion <" + borrado.getOpcion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Contrato <" + borrado.getContrato() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Usuario <" + borrado.getUsuario() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Grupo <" + borrado.getClaveOperacion() + ">", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Monto <" + borrado.getLimiteOperacion() + ">", EIGlobal.NivelLog.DEBUG);
			res = limOpe.mantenimientoLimitesOperacion(borrado.getOpcion(), borrado.getContrato(), borrado.getUsuario(), borrado.getClaveOperacion(), Util.rellenaCeros(borrado.getLimiteOperacion(), 17));
			resUsuario.add(res);
    		EIGlobal.mensajePorTrace("LYMGuardar - actualiza lim contrato - " +
		    		"Respuesta: [" + res.getCodigoOperacion() +  "]", EIGlobal.NivelLog.DEBUG);

		}
		

	}

	private String comparaLYMContratos() {
		LimitesContrato limIni = beanIni.getLimitesContrato();
		limFin = beanFin.getLimitesContrato();
		if(limIni.isLimiteActivo() && limFin.isLimiteActivo()) {
			if(!limIni.getMonto().equals(limFin.getMonto())) {
				limFin.setOpcion("MO");
				totalOperaciones++;
				return "MO";
			}
			else {
				return "";
			}
		}
		else if(!limIni.isLimiteActivo() && limFin.isLimiteActivo()) {
			limFin.setOpcion("AL");
			totalOperaciones++;
			return "AL";
		}
		else if(limIni.isLimiteActivo() && !limFin.isLimiteActivo()) {
			limFin.setOpcion("BA");
			totalOperaciones++;
			return "BA";
		}
		else {
			return "";
		}
	}
}