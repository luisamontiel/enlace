package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

public class ObtieneFacturas
{
	private String Proveedor = "";
	private String Archivo = "";
	long posicionReg                 = 0;
	RandomAccessFile archivoTrabajo;
	public ObtieneFacturas(String RutaArchivo)
	{
		Archivo = RutaArchivo;

		try
		{
			archivoTrabajo = new RandomAccessFile(Archivo,"r");
		}
		catch (IOException e)
		{
		}

	}
	public Vector Facturas () throws IOException {
		String registroLeido = "";
		String encabezado = "";
		String campo = "";
		String lecturaReg = "";
		Vector lista = new Vector();
		 try
			{
				archivoTrabajo.seek(0);
				encabezado = archivoTrabajo.readLine();
			}
		catch(IOException e)
			{
				System.out.println("Error leyendo el Archivo: ");

			}
		if (encabezado!=null)
		{
			do
			{
				try
				{
					posicionReg = archivoTrabajo.getFilePointer();
					lecturaReg  = archivoTrabajo.readLine();
				}
				catch (Exception e)
				{
					System.out.println("Error leyendo el archivo");
				}
				if (lecturaReg!=null)
				{
					if (lecturaReg.substring(1,2).equals("1"))
					{
						campo = lecturaReg.substring(2,22).trim();
						lista.add(campo);
					}
				}
			}
			while (lecturaReg.substring(0,1).equals("2"));
		}
		try {
			archivoTrabajo.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		return (lista);

	}

}
