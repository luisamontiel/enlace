package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;
import java.text.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.netscape.jsp.tags.conditional.Case;

import mx.altec.enlace.beans.SolPDFBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import static mx.altec.enlace.utilerias.NominaConstantes.*;

/** Worker class, crea el despliegue de la consulta de archivos de nomina
 *@version 1.5 Modificaciones para Optimizaciones Nomina
 */
public class ArchivosNomina{


	/**
	 * Variables Registro, Secuencia, Empleado, Apellido
	 */
	public String tipoRegistro = "", NumeroSecuencia = "", NumeroEmpleado = "", ApellidoPaterno   = "",
	ApellidoMaterno = "", Nombre = "", NumeroCuenta = "", Importe = "", Referencia="", Edo="";
	/**
	 * Variables RFC, Sexo
	 */
	public String RFC="", Sexo="";
	/**
	 * Variables varias
	 */
	public String sec_pago = "", fch_recep = "", fch_aplic = "", cta_cargo = "", estatus = "", nom_arch = "",
				  importe_aplic = "", num_reg = "", hra_aplic = "";
    public String fch_cargo = "";

    public String num_tarjeta =  "";
    public String importe_sinformato="";
	public String asFechaTransmision="", asTotalRegistros="", asAceptados="", asRechazados="", asFechaActualizacion="";
	BaseServlet BAL;
	/**
	 * Session
	 */	 
	BaseResource session;
	/** Archovo de trabajo*/
	RandomAccessFile archivoTrabajo;
	/** archivo de trabajo temporal */
	BufferedReader archivoTrabajotmp;
	/** path de archivo */
	File pathArchivoNom;
	long posicionReg;
	long b			 = 0;
	long residuo	 = 0;

	/*FWS Vector - Solicitud de informes PDF*/
	/**
	 * Variable global para tipo de nomina
	 */
	private String tipoNomina;
	private HashMap< Integer,SolPDFBean> solicitudesPDF;

	/**
	 * Variable para el mapa de las solicitudes
	 */
	public HashMap<Integer,SolPDFBean> getSolicitudesPDF() {
		return solicitudesPDF;
	}

    /**
     * FWS Vector - Solicitud de informes PDF
     * @param solicitudesPDF
     */
	public void setSolicitudesPDF(HashMap<Integer,SolPDFBean> solicitudesPDF) {
		this.solicitudesPDF = solicitudesPDF;
	}


	/**
	 * FWS Vector - Solicitud de informes PDF
	 * @return tipoNomina
	 */
	public String getTipoNomina() {
		return tipoNomina;
	}

    /**
     * FWS Vector - Solicitud de informes PDF
     * @param tipoNomina
     */
	public void setTipoNomina(String tipoNomina) {
		this.tipoNomina = tipoNomina;
	}


	public ArchivosNomina(String rutaArchivo,HttpServletRequest request){
	EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al constructor() 1&", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("***ArchivosNomina.class #Creando archivo: #"+rutaArchivo, EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
		session = (BaseResource) sess.getAttribute("session");
		sess.removeAttribute("session");

		posicionReg=0;
	//BAL.session.setNameFileNomina(rutaArchivo);
		sess.setAttribute("nombreArchivo",rutaArchivo);
		sess.setAttribute("session",session);

		try{
		pathArchivoNom	= new File( rutaArchivo );
		archivoTrabajotmp	= new BufferedReader( new FileReader( pathArchivoNom ) );

		solicitudesPDF =  (HashMap <Integer,SolPDFBean>)sess.getAttribute(VAR_SES_SOl_PDF);

		if(solicitudesPDF != null){
			EIGlobal.mensajePorTrace("OpcionesNomina.java :: ArchivosNomina.class :: Numero de soliciutdes " + solicitudesPDF.size(), EIGlobal.NivelLog.DEBUG);
		}

		} catch(IllegalArgumentException e) {
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina(1)"+e.getMessage(), EIGlobal.NivelLog.INFO);
		} catch(FileNotFoundException e) {
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina(2)"+e.getMessage(), EIGlobal.NivelLog.INFO);
		} catch(SecurityException e){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina(3)"+e.getMessage(), EIGlobal.NivelLog.INFO);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina(4)"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
		tipoNomina="";
	}


		public String lecturaArchivo(){
			String lecturaReg	= "", encabezado	= "";
			String strTabla		= "";
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a lecturaArchivo()&", EIGlobal.NivelLog.INFO);
			try{
				archivoTrabajo.seek(0);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
			}

			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
									"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Leyendo encabezado&", EIGlobal.NivelLog.INFO);

			try {
				encabezado=archivoTrabajo.readLine();
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajo.readLine();
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
						lecturaReg=archivoTrabajo.readLine();
						if ( lecturaReg.equals( "" ) )
							lecturaReg = archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
						if (lecturaReg.substring(0,1).equals("2")) {
							EIGlobal.mensajePorTrace("***ArchivosNomina.class &El tipo de registro es 2&", EIGlobal.NivelLog.INFO);
							lecturaCampos(lecturaReg);
							strTabla=strTabla + creaRegTabla();
						}
					}
				} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			strTabla=strTabla+"</table>";
			return strTabla;
		}

		public void close(){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a close()&", EIGlobal.NivelLog.INFO);
			try {
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
			}
		}

		public void agregaRegistro(HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Se ha agregado un registro&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Existen : "+session.getTotalRegsNom()+"&", EIGlobal.NivelLog.INFO);
		}

	public String creaVariablesJS(){
			String cadenaVariables	= "";
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a creaVariablesJS()&", EIGlobal.NivelLog.INFO);
			cadenaVariables +="<script>js_tipoRegistro='"+ tipoRegistro + "' \n" +
				 "js_NumeroSecuencia=\""+ NumeroSecuencia + "\" \n" +
				 "js_NumeroEmpleado=\""+ NumeroEmpleado + "\" \n" +
				 "js_ApellidoPaterno='"+ ApellidoPaterno + "' \n" +
				 "js_ApellidoMaterno='"+ ApellidoMaterno + "' \n" +
				 "js_Nombre='"+ Nombre + "' \n" +
				 "js_NumeroCuenta='"+ NumeroCuenta + "' \n" +
				 "js_Importe='"+ formatea( Importe ) + "' \n</script>";

			return cadenaVariables;
		}
		public int leeRegistro(long posicion,String tipoArchivo){
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a leeRegistro() ", EIGlobal.NivelLog.INFO);
			String lecturaReg	= "";
			String lecturaRegtmp	= "";
			long tmp = 0;
			//posicionaRegistro(posicion);
			try{
				tipoArchivo.trim();
				archivoTrabajo.seek(41);
				lecturaRegtmp=archivoTrabajo.readLine();

			if (lecturaRegtmp.length()  == 126  ){ // Interbancaria 108 / 110

				archivoTrabajo.seek(0);
			    tmp = ((posicion-1)*128)+ 40 ;// 128 +39
				archivoTrabajo.seek(tmp);
				lecturaReg=archivoTrabajo.readLine();
			 }	else { if (lecturaRegtmp.length()  == 127 ) {// interbancaria 109 / 111
						archivoTrabajo.seek(0);
					    tmp = ((posicion-1)*129)+ 41;
						archivoTrabajo.seek(tmp);
						lecturaReg=archivoTrabajo.readLine();
						}else { EIGlobal.mensajePorTrace("***ArchivosNomina.class El archivo es incorrecto", EIGlobal.NivelLog.INFO);
						        return -1; }
					}
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}
			lecturaCampos(lecturaReg);
			return 1;
		}

		public int posicionaRegistro(long posicion){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a posicionaRegistro>>>()&", EIGlobal.NivelLog.INFO);
			try {
				archivoTrabajo.seek(((posicion-1)*129)+ 41);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				return -1;
			}
			return 1;
		}

		public String creaRegTabla(){

			String sTabla	= "";
			posicionReg++;

			residuo=posicionReg % 2 ;
			String bgcolor	= "textabdatobs";
			if (residuo == 0){
				bgcolor		= "textabdatcla";
			}
			sTabla= "<tr><td class=\"" + bgcolor + "\" align=\"center\" nowrap><input type=radio name=seleccionaRegs value="+posicionReg+"></td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + NumeroEmpleado + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + ApellidoPaterno + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + ApellidoMaterno + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + Nombre + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + NumeroCuenta + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"right\">" + formatoMoneda(Importe)  + "&nbsp</td>";
			//sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"right\">" + formatea( agregarPunto(Importe) ) + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">&nbsp</td>";
			return sTabla;
		}

		public String  agregarPunto(String importe){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo agregaPunto()&", EIGlobal.NivelLog.INFO);
	        String importeConPunto	= "";
			//long num = 0;
			//num = Long.parseLong(Importe);
			//importe = "" + num;
	        String cadena1			= importe.substring(0,importe.length()-2);
	        cadena1					= cadena1.trim();
	        String cadena2			= importe.substring(importe.length()-2, importe.length());
	        importe					= cadena1+" "+cadena2;
	        importeConPunto			= importe.replace(' ','.');
	        return importeConPunto;
		}

		public void lecturaCampos(String registro){
			/* Detalle del archivo */
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCampos()&", EIGlobal.NivelLog.INFO);
			try {
			tipoRegistro 	 = registro.substring(0,1);
			NumeroSecuencia   = registro.substring(1,6);
			NumeroEmpleado    = registro.substring(6,13);
			ApellidoPaterno   = registro.substring(13,43);
			ApellidoMaterno   = registro.substring(43,63);
			Nombre            = registro.substring(63,93);
			NumeroCuenta      = registro.substring(93,109);
			Importe			  = registro.substring(109,127);

//			NumeroCuenta      = registro.substring(93,104); // Cuando la longitud de la cuenta sea de 11
//			Importe = registro.substring(104,122);
			//Importe = formateaImporte(Importe);
			} catch(Exception e) {
				EIGlobal.mensajePorTrace("***ERROR"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			 }
		}

		public void altaRegistro(String registroNuevo){
			try{
				archivoTrabajo.seek(archivoTrabajo.length()-31);
				String ultimoRegistro=archivoTrabajo.readLine();

				if (ultimoRegistro.equals("")){
					archivoTrabajo.seek(archivoTrabajo.length()-32);
					ultimoRegistro=archivoTrabajo.readLine();
				}

				archivoTrabajo.seek(archivoTrabajo.length()-31);
				archivoTrabajo.writeBytes(registroNuevo+"\r\n"+ultimoRegistro+"\r\n");
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &El registro ha sido dado de alta&", EIGlobal.NivelLog.INFO);
			}
			catch(IOException e) {
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
			}
		}

		public void edicionRegistros(long posicion, String registroEditado, int tamReg){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Entrando al metodo edicionRegistros()", EIGlobal.NivelLog.INFO);

			if (posicion>1){
				try{

					if (tamReg == 127){
					// nuevo
					archivoTrabajo.seek(41+6+((posicion-1)*129)); // 129 por que ya tiene el final de linea
					}else {
					//importado aqui
					archivoTrabajo.seek(40+6+((posicion-1)*128));
					}
					//archivoTrabajo.seek(((posicion-1)*127)+41+6);
					archivoTrabajo.writeBytes(registroEditado);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}

			}
			else{
				try{
   					if (tamReg == 127){
					// nuevo
					archivoTrabajo.seek(41 + 6);
					}else {
					//importado aqui
					archivoTrabajo.seek(40 + 6);
					}
					//archivoTrabajo.seek(41+6);
					//archivoTrabajo.seek(41);
					archivoTrabajo.writeBytes(registroEditado);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
			}
		}

		public void bajaRegistro(long numRegistro, int tamReg,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo bajaRegistro()&", EIGlobal.NivelLog.INFO);
			HttpSession sess = request.getSession();
	        BaseResource session = (BaseResource) sess.getAttribute("session");
			RandomAccessFile archivoOriginal, archivoTemporal;
			File archivo;
			String lineaLeida	= "";
			String[] contenido;
			int regs=0;
			String lecturaReg = "";
			try{
				//a partir de este bloque try se lee el archivo original, se agrega cada una de sus lineas a un arreglo
				//de strings, se elimina el archivo y se construye un archivo nuevo con el mismo nombre, en este, se escriben
				//tantas lineas como elemntos tenga el arreglo, sin embargo, los registros que se han de eliminar se deben marcar
				//previamente con un cero al inicio
				//archivo= new File(BAL.session.getNameFileNomina());
				archivo= new File((String)sess.getAttribute("nombreArchivo"));
				archivoOriginal= new RandomAccessFile(archivo, "rw");
				archivoOriginal.seek(0);
				//posicionar al inicio del registro que se ha de eliminar
				String  registroVacio = "";
				if (tamReg == 127){
				// nuevo
				archivoOriginal.seek(41+((numRegistro-1)*129));
				registroVacio="0                                                                                                                              \r\n";
				}else {
				//importado aqui
				archivoOriginal.seek(40+((numRegistro-1)*128));
	   		    registroVacio="0                                                                                                                             \r\n";
				}
				archivoOriginal.writeBytes(registroVacio);
				//el registro se ha sustituido por el
				//EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo bajaRegistro()&", EIGlobal.NivelLog.INFO);
				//archivoOriginal es la variable que representa el archivo de donde se eliminara un registro
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &archivoOriginal&"+ archivoOriginal, EIGlobal.NivelLog.INFO);
				contenido=new String[registrosEnArchivo(archivoOriginal)];
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &contenido&"+ contenido, EIGlobal.NivelLog.INFO);
				archivoOriginal.seek(0);
				for(int i=0; i<contenido.length; i++){
					lineaLeida=archivoOriginal.readLine();
//					lineaLeida=lineaLeida.substring(0,lineaLeida.length()-1);
					  lineaLeida=lineaLeida.substring(0,lineaLeida.length());

					contenido[i]=lineaLeida;
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &contenido[i]&"+ contenido[i], EIGlobal.NivelLog.INFO);
				}
				archivoOriginal.close();
				archivo.delete();
				//archivoTemporal=new RandomAccessFile(BAL.session.getNameFileNomina(), "rw");
				archivoTemporal=new RandomAccessFile((String) sess.getAttribute("nombreArchivo"), "rw");

				for(int a=0; a<contenido.length; a++){
					if(!contenido[a].startsWith("0")){
						archivoTemporal.writeBytes(contenido[a]+"\r\n");
						regs++;
					}
				}
				archivoTemporal.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
			}
		}//fin del metodo

	public static int registrosEnArchivo(RandomAccessFile file){
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &entrando al metodo registrosEnArchivo&()"+ file, EIGlobal.NivelLog.INFO);
		String registroLeido	= "";
		long posicion			= 0;
		int cantRegistros		= 0;
		try{
			if(file.length()>0){
				file.seek(0);
				do{
					registroLeido=file.readLine();
					posicion=file.getFilePointer();
					cantRegistros++;
				}while(posicion<file.length());
			}
		}
		catch(IOException e){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
		}
		return cantRegistros;
	}//fin del metodo registrosEnArchivo

		public int size(){return 129;}

		public String modificaDetalle(){
			String s="";
			return  s;
		}

	//////////DEFINICION DEL METODO PARA ACTUALIZAR EL REGISTRO SUMARIO
	//nuevo
	//	public void actualizaSumario(String campo, String valorActual, boolean modificacion){
	//importado
		public void actualizaSumario(String campo, String valorActual, boolean modificacion, long totalReg,int tamReg,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &entrando al metodo para modificar el registro sumario&()", EIGlobal.NivelLog.INFO);
			String campoModificado		= "";
			String valorModificado		= "";
			String lastRecord			= "";
			int valorSecuencial			= 0;
			int nuevoValorSecuencial	= 0;
			int valorTotalRegistros		= 0;
			int	valorImporteTotal		= 0;
			int valorAnteriorImporteTotal=0;

		///////MODIFICA EL CAMPO NUMERO SECUENCIAL CUANDO SE EJECUTA UNA ALTA
			if (campo.equals("sumSecuencial") && modificacion){
				try{
					valorSecuencial= Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}

				nuevoValorSecuencial=valorSecuencial+1;
				valorModificado=String.valueOf(nuevoValorSecuencial);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setLastSecNom(valorModificado);
				sess.setAttribute("session",session);

				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord=lastRecord.substring(0,1)+valorModificado+lastRecord.substring(6,lastRecord.length());
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			///////MODIFICA EL CAMPO NUMERO SECUENCIAL CUANDO SE EJECUTA UNA BAJA
			if (campo.equals("sumSecuencial") && modificacion==false){

				try{
					valorSecuencial= Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
				nuevoValorSecuencial=valorSecuencial;
				valorModificado=String.valueOf(nuevoValorSecuencial-1);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setLastSecNom(valorModificado);
				sess.setAttribute("session",session);
				try{
				if (tamReg == 127){
					//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
				}else {
				   	//importado
					archivoTrabajo.seek(0);
					long tmp = (totalReg * 128)+ 40 ;// 128 +39
					archivoTrabajo.seek(tmp);
				}

					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) ){
						lastRecord = archivoTrabajo.readLine();
					EIGlobal.mensajePorTrace("***ArchivosNomina.class &La linea leida es:3>>>>"+lastRecord+"&", EIGlobal.NivelLog.INFO);
				}
					if (tamReg == 127){
						//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
					}else {
					   	//importado
						archivoTrabajo.seek(0);
						long tmp = (totalReg * 128)+ 40 ;// 128 +39
						archivoTrabajo.seek(tmp);
					}

						lastRecord=lastRecord.substring(0,1)+valorModificado+lastRecord.substring(6,lastRecord.length());
						archivoTrabajo.writeBytes(lastRecord);


				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}

			}

			///////MODIFICA EL CAMPO TOTAL DE REGISTROS CUANDO SE EJECUTA UNA ALTA

			if (campo.equals("totalRegs") && modificacion){
				try{
					valorTotalRegistros=Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
				valorModificado=String.valueOf(valorTotalRegistros+1);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setTotalRegsNom(valorModificado);
				sess.setAttribute("session",session);
				try{
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();
					archivoTrabajo.seek(archivoTrabajo.length()-31);
					lastRecord=lastRecord.substring(0,6)+valorModificado+lastRecord.substring(11,lastRecord.length());
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			///////MODIFICA EL CAMPO TOTAL DE REGISTROS CUANDO SE EJECUTA UNA BAJA

			if (campo.equals("totalRegs") && modificacion==false){
				try{
					valorTotalRegistros=Integer.parseInt(valorActual);
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
				valorModificado=String.valueOf(valorTotalRegistros-1);
				valorModificado=completaCamposNumericos(valorModificado.length(),5)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setTotalRegsNom(valorModificado);
				sess.setAttribute("session",session);
				try{
				if (tamReg == 127 ){
					//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
				}else {
				   	//importado
					archivoTrabajo.seek(0);
					long tmp = (totalReg * 128)+ 40 ;// 128 +39
					archivoTrabajo.seek(tmp);
				}
					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();

				if (tamReg == 127 ){
					//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
				}else {
				   	//importado
					archivoTrabajo.seek(0);
					long tmp = (totalReg * 128)+ 40 ;// 128 +39
					archivoTrabajo.seek(tmp);
				}
						lastRecord=lastRecord.substring(0,6)+valorModificado+lastRecord.substring(11,lastRecord.length());
						archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			///////MODIFICA EL CAMPO IMPORTE TOTAL CUANDO SE EJECUTA UNA ALTA

			if (campo.equals("impTotal") && modificacion){
				try{
					valorActual=valorActual.trim();
					valorAnteriorImporteTotal=Integer.parseInt(session.getImpTotalNom().trim());
					valorImporteTotal=Integer.parseInt(valorActual);

	}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}

				valorModificado=String.valueOf(valorAnteriorImporteTotal+valorImporteTotal);
				valorModificado=completaCamposNumericos(valorModificado.length(),18)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setImpTotalNom(valorModificado);
				sess.setAttribute("session",session);
				try{
				if (tamReg == 127 || tamReg == 0){ // cero por el Alta
					//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
				}else {
				   	//importado
					archivoTrabajo.seek(0);
					long tmp = (totalReg * 128)+ 40 ;// 128 +39
					//long tmp = ( ( totalReg - 1 )* 128)+ 40 ;// 128 +39
					archivoTrabajo.seek(tmp);
				}
					lastRecord = archivoTrabajo.readLine();

					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();


				if (tamReg == 127 || tamReg == 0){ //cero por el Alta
					//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
				}else {
				   	//importado
					archivoTrabajo.seek(0);
					long tmp = (totalReg * 128)+ 40 ;// 128 +39
					archivoTrabajo.seek(tmp);
				}
					lastRecord=lastRecord.substring(0,11)+valorModificado;
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
			}
			///////MODIFICA EL CAMPO IMPORTE TOTAL CUANDO SE EJECUTA UNA BAJA
			if (campo.equals("impTotal") && modificacion==false){
				try{
					valorActual=valorActual.trim();
					valorImporteTotal=Integer.parseInt(valorActual);
					valorAnteriorImporteTotal=Integer.parseInt(session.getImpTotalNom().trim());
				}
				catch(NumberFormatException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
				valorModificado=String.valueOf(valorAnteriorImporteTotal-valorImporteTotal);
				valorModificado=completaCamposNumericos(valorModificado.length(),18)+valorModificado;
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setImpTotalNom(valorModificado);
				sess.setAttribute("session",session);

				try{

				if (tamReg == 127){
					//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
				}else {
				   	//importado
					archivoTrabajo.seek(0);
					long tmp = (totalReg * 128)+ 40 ;// 128 +39
					archivoTrabajo.seek(tmp);
				}

					lastRecord = archivoTrabajo.readLine();
					if ( lastRecord.equals( "" ) )
						lastRecord = archivoTrabajo.readLine();

				if (tamReg == 127){
					//nuevo
						archivoTrabajo.seek(archivoTrabajo.length()-31);
				}else {
				   	//importado
					archivoTrabajo.seek(0);
					long tmp = (totalReg * 128)+ 40 ;// 128 +39
					archivoTrabajo.seek(tmp);
				}
					lastRecord=lastRecord.substring(0,11)+valorModificado;
					archivoTrabajo.writeBytes(lastRecord);
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
			}

		}//FIN DEL METODO PARA MODIFICAR REGISTRO SUMARIOO

		/**metodo que actualiza el campo "Numero Secuencial" para cada uno de los registros de detalle
		*esta actualizacion se llevara a cabo cuando se elimine uno de los registros
		*el metodo recibe dos argumentos, uno es el archivo dentro del cual se haran las modificaciones
		*el segundo es la cantidad de registros que debe actualizar
        */
		public void actualizaConsecutivos(int cantRegistros,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo que actualiza consecutivos&", EIGlobal.NivelLog.INFO);
			HttpSession sess = request.getSession();
	        BaseResource session = (BaseResource) sess.getAttribute("session");
			try{
				//RandomAccessFile elArchivoTrabajo=new RandomAccessFile(BAL.session.getNameFileNomina(),"rw");
				RandomAccessFile elArchivoTrabajo=new RandomAccessFile((String)sess.getAttribute("nombreArchivo"),"rw");
				elArchivoTrabajo.seek(0);
				String regEncabezado=elArchivoTrabajo.readLine();
				if ( regEncabezado.equals( "" ) )
						regEncabezado = elArchivoTrabajo.readLine();
				for(int i=0; i<cantRegistros; i++){
					elArchivoTrabajo.writeBytes("2"+completaNumericos(String.valueOf(i+2),5));
					elArchivoTrabajo.seek(elArchivoTrabajo.getFilePointer()+123);
				}//fin de for
				elArchivoTrabajo.close();
			}
			catch(IOException exception){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+exception, EIGlobal.NivelLog.INFO);
			}
		} //fin del metodo

		public String completaCamposNumericos(int longitudActual, int longitudRequerida){
			String ceros="0";
		    int faltantes= longitudRequerida-longitudActual;
		    for(int i=1; i<faltantes; i++){
				ceros=ceros+"0";
		    }
			return ceros;
		}//fin de metodo

	/**inicio del metodo completa campos numericos, el cual es una replica del metodo anterior, sin embargo,
	*se presentan dos ligeras variantes, el metodo rellena con ceros en lugar de con blancos, ademas
	*siempre se agregan los caracteres a la izquierda
    */
		public String completaNumericos(String cadena, int longitudRequerida){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo para completar campos numericos&()", EIGlobal.NivelLog.INFO);
			String ceros			= "";
			String cadenaDeRegreso  = "";
			int longitudActual=cadena.length();
			if(longitudActual!=longitudRequerida){
			    int faltantes= longitudRequerida-longitudActual;
				for(int i=0; i<faltantes; i++){
				ceros=ceros+"0";
			    }
			}
			cadenaDeRegreso=ceros+cadena;
			return cadenaDeRegreso;
		}//fin del metodo

		public String[] envioArchivo(String trama_inicio,HttpServletRequest request){

			int cantRegistros= Integer.parseInt(session.getTotalRegsNom());
			String linea	= "", encabezado	= "", tipoReg = "", formatoEnviado = "";
			String []trama=new String[iteraciones(cantRegistros)];


			int contador=0;
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a envioArchivo()&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &trama_inicio()&"+trama_inicio, EIGlobal.NivelLog.INFO);
				try {

					archivoTrabajo.seek(41);
					tipoReg=archivoTrabajo.readLine();

					if (tipoReg.length() == 127)
					    formatoEnviado = "unix";
						else formatoEnviado = "Microsoft";

					archivoTrabajo.seek(0);
					encabezado=archivoTrabajo.readLine();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				}
				if (encabezado!=null){
					for(int z=0; z<trama.length; z++){
						trama[z]= trama_inicio+creaTrama(formatoEnviado,request);
						trama[z]=modificaTrama(trama[z]);
					}
					try {
						archivoTrabajo.close();
					}
					catch(IOException e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
					}
			}

			try{
			for(int k=0;k<trama.length;k++){}

			} catch(Exception e) {
			}



			return trama;
		}//fin del metodo

		public String creaTrama(String formatoEnviado,HttpServletRequest request){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a creaTrama()&", EIGlobal.NivelLog.INFO);
			String registroDetalle="";
			String registroDetalletmp="";
			String registro="";
			registro=registro.trim();
			int numeroRegistros= Integer.parseInt(session.getTotalRegsNom());
			try{


				registroDetalle=archivoTrabajo.readLine();

				if (registroDetalle != null)
				EIGlobal.mensajePorTrace("***ArchivosNomina.class registroDetalle  = &"+ registroDetalle, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosNomina.class registroDetalle.length() = &"+ registroDetalle.length(), EIGlobal.NivelLog.INFO);

			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
			}
			for(int a=0; a<3 ; a++){
				if(registroDetalle.startsWith("2")){
					b=b+1;
					String secuencial	= Long.toString(b);
					String cuenta		= registroDetalle.substring(93,109).trim();
					String importe		= registroDetalle.substring(109,registroDetalle.length());
					importe= eliminaCerosImporte(importe);
					importe=importe.trim();
					if(importe.length()==1)
						importe="0"+importe;
					importe=agregarPunto(importe);
					registro=registro+secuencial+"@"+cuenta+"@"+importe+"@";
					EIGlobal.mensajePorTrace("***ArchivosNomina.class >>Valor de registro>>"+registro, EIGlobal.NivelLog.INFO);
					try{
						registroDetalle=archivoTrabajo.readLine();
						EIGlobal.mensajePorTrace("***ArchivosNomina.class >>Valor de registroDetalle>>"+registroDetalle, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("***ArchivosNomina.class>>Valor de registroDetalle.length()>>"+registroDetalle.length(), EIGlobal.NivelLog.INFO);
					}

					catch(IOException e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
					}
				}
				else{
					break;
				}
			}//del for
			try{
			  if (formatoEnviado.equals("unix"))
				archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-2);
				else archivoTrabajo.seek(archivoTrabajo.getFilePointer()-(registroDetalle.length())-1);
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace("***ArchivosNomina.class registro  &" , EIGlobal.NivelLog.INFO);
			return registro;
		}

		public int cuentaArrobas(String cadena){
			int arrobas=0;
			try{
				for(int a=0; a<cadena.length(); a++){
					if(cadena.substring(a,a+1).equals("@"))
						arrobas=arrobas+1;
				}
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
			}
			EIGlobal.mensajePorTrace("***ArchivosNomina.class cuentaArrobas = &" , EIGlobal.NivelLog.INFO);
			return arrobas;
		}

		public String modificaTrama(String tramaModificable){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo tramaModificable()&", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &tramaModificable = &"+tramaModificable , EIGlobal.NivelLog.DEBUG);
			String tramaDevuelta	= "";
			int valorDevuelto		= 0;
			int arrobas				= 0;
			int registrosEnTrama	= 0;
			String rEnTrama=tramaModificable.substring((posCar(tramaModificable,'@',2)+1),posCar(tramaModificable,'@',3));
			registrosEnTrama=Integer.parseInt(rEnTrama);
			if(cuentaArrobas(tramaModificable)==12)
				valorDevuelto=3;
			else if(cuentaArrobas(tramaModificable)==9)
				valorDevuelto=2;
			else if(cuentaArrobas(tramaModificable)==6)
				valorDevuelto=1;

			tramaDevuelta=tramaModificable.substring(0,posCar(tramaModificable,'@',2)+1)+valorDevuelto+tramaModificable.substring(posCar(tramaModificable,'@',3),tramaModificable.length());
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &tramaDevuelta  &" +tramaDevuelta, EIGlobal.NivelLog.DEBUG);
			return tramaDevuelta;
		}

		public String formateaImporte(String importe){

			String importeFormateado="";
			boolean contienePunto=false;
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo formatea importe()&", EIGlobal.NivelLog.INFO);
			int i=0;
			for (i=0; i<importe.length(); i++){
				if(importe.charAt(i)=='.'){
					contienePunto=true;
					break;
				}
			}
			if(contienePunto){
				importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
			}
			else
				importeFormateado=importe;

			return importeFormateado;
		}

		public int iteraciones(int records){
			int iteraciones=records/3;
			if ((records%3)!=0){
				iteraciones=iteraciones+1;
				return iteraciones;
			}
			else{
				return iteraciones;
			}
		}

		public String eliminaCerosImporte(String imp){
			while(imp.startsWith("0")){
				imp=imp.substring(1,imp.length());
			}
		return imp;
		}

	////////////////////////////////////////// metodos para archivo de salida de empleados /////////////////////////////////
		public String lecturaArchivoSalida(HttpServletRequest request){
			String lecturaReg	= "", encabezado	= "";
			String strTabla	= "";
			int contador	= 0;
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a lecturaArchivoSalida()&", EIGlobal.NivelLog.INFO);
			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
							        "<tr> "+
									"<td class=\"textabref\">Recuperacion del Archivo de Pago de N&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
									"<tr> "+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
//OK      ;26/08/2002;1;1;0;30/08/2002;12.000000;12.000000;0.000000;50000000068;0.
//000000;0.000000;80000648376;348747;
//50000000068;12.000000;R;9907448;;;


/*OK      ;17/09/2002;1;0;0;05/12/2002;12.000000;0.000000;0.000000;50000000068;0.0
00000;0.000000;80000648376;357775;


K      ;17/09/2002;1;0;0;27/09/2002;11.000000;0.000000;0.000000;50000000068;0.0
00000;0.000000;80000648376;247285;
51999000239;4.000000;R;7219786;;;*/





			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();					//lectura del encabezado
				//if ( encabezado.equals( "" ) )
				//		encabezado = archivoTrabajo.readLine();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}

			if (encabezado!=null && encabezado.substring(0,2).equals("OK")){
				HttpSession sess = request.getSession();
				session = (BaseResource) sess.getAttribute("session");
				sess.removeAttribute("session");
				session.setCuentaCargo(encabezado.substring(posCar(encabezado,';',9)+1,posCar(encabezado,';',10)));
				sess.setAttribute("session",session);
				//BAL.session.setCuentaCargo(encabezado.substring(posCar(encabezado,';',9)+1,posCar(encabezado,';',10)));
				lecturaEncabezadoAS(encabezado);
				do {
					try{
						posicionReg=archivoTrabajo.getFilePointer();
						lecturaReg=archivoTrabajo.readLine();
						//if ( lecturaReg.equals( "" ) )
						//	lecturaReg = archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
						return "Error 3: "+ e.getMessage();
					}
				if (lecturaReg!=null){
						lecturaCamposAS(lecturaReg);
						strTabla=strTabla + creaRegAS(contador);
						contador++;

					}
				}while (lecturaReg!=null);
				strTabla=strTabla+"</table>";
				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
				}
			}
			else return "";
			return strTabla+ "|" +contador + "||";
		}


//Consulta de nomina

//		public String lecturaArchivo(){
        /** realiza la construccion de la tabla de despliegue de la consulta de archivos
         * TODO:Revisar diferencias entre <code>String lecturaArchivoSalidaConsul(String)</code>
         *@since ver 1
         */
		public String lecturaArchivoSalidaConsul(){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaArchivoSalidaConsul()&", EIGlobal.NivelLog.INFO);
			String lecturaReg	= "", encabezado	= "";
			String strTabla		= "";

			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
									"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha recepci�n</td>"+
                                    "<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de cargo</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Hora de aplicaci&oacute;n</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Nombre del Archivo</td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">Secuencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num. registros</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe aplicado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Estatus</td>";

									/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
									if(VAL_TIPO_SOLPDF_TR.equals(tipoNomina)){
										strTabla = strTabla + "<td width=\"70\" class=\"tittabdat\" align=\"center\">Comprobante</td>";
									}

									strTabla = strTabla +"</tr>";

			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Leyendo encabezado 1&", EIGlobal.NivelLog.INFO);
			try {
				encabezado=archivoTrabajotmp.readLine();
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajotmp.readLine();
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch(Exception e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 1()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			   //OK      00000616;NOMI0000;Transacci�n exitosa;
			   //encabezado = "OK      00000616;NOMI0001;Transacci�n exitosa";
				String existenDatos = encabezado.substring((posCar(encabezado,';',1))+1,posCar(encabezado,';',2));
			if (encabezado!=null && existenDatos.equals("NOMI0000")){
				do{
					try{
						//EIGlobal.mensajePorTrace("***ArchivosNomina.class estoy dentro del do while**", EIGlobal.NivelLog.INFO);
						//posicionReg=archivoTrabajo.getFilePointer();
						lecturaReg=archivoTrabajotmp.readLine();
						//if ( lecturaReg.equals( "" ) )
						//	lecturaReg = archivoTrabajo.readLine();
						//	EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 1**"+lecturaReg, EIGlobal.NivelLog.INFO);
					}

					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 2()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
						//if (lecturaReg.substring(0,1).equals("2")) {
					//EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 2**"+lecturaReg, EIGlobal.NivelLog.INFO);
							lecturaCamposConsul(lecturaReg);
							strTabla=strTabla + creaRegTablaConsul();
						//}
					}
				} while (lecturaReg!=null);
				//} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try {
					archivoTrabajotmp.close();
				}
				catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 3()"+e, EIGlobal.NivelLog.INFO);
				}
			strTabla=strTabla+"</table>";
			}else strTabla=strTabla+"NO EXISTEN DATOS";

			return strTabla;
		}

        /** realiza la construccion de la tabla de consulta de archivos de Nomina.
        * TODO:Revisar diferencias entre <code>String lecturaArchivoSalidaConsul()</code>
        *@param lista_nombres_archivos ...
        * @since version 1.
        */
		public String lecturaArchivoSalidaConsul(String lista_nombres_archivos){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaArchivoSalidaConsul(String)&"+ lista_nombres_archivos, EIGlobal.NivelLog.INFO);
			String lecturaReg	= "", encabezado	= "";
			String strTabla		= "";

			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
									"<tr> "+
									"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha recepci�n</td>"+
                                    "<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de cargo</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci�n</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Hora de aplicaci&oacute;n</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Nombre del Archivo</td>"+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">Secuencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Num. registros</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe aplicado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Estatus</td>";

									/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
									if(VAL_TIPO_SOLPDF_TR.equals(tipoNomina)){
										strTabla = strTabla + "<td width=\"70\" class=\"tittabdat\" align=\"center\">Comprobante</td>";
									}

									strTabla = strTabla +"</tr>";
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Leyendo encabezado&", EIGlobal.NivelLog.INFO);
			try {
				encabezado=archivoTrabajotmp.readLine();
				if ( encabezado.equals( "" ) )
					encabezado = archivoTrabajotmp.readLine();
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			}	//lectura del encabezado
			catch(Exception e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 1()"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
						//EIGlobal.mensajePorTrace("***ArchivosNomina.class estoy dentro del do while**", EIGlobal.NivelLog.INFO);
						//posicionReg=archivoTrabajo.getFilePointer();
						lecturaReg=archivoTrabajotmp.readLine();
						
						
						//if ( lecturaReg.equals( "" ) )
						//	lecturaReg = archivoTrabajo.readLine();
						//	EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 1**"+lecturaReg, EIGlobal.NivelLog.INFO);
					}

					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 2()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
					}

					if (lecturaReg!=null){
						//if (lecturaReg.substring(0,1).equals("2")) {
					EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 2**"+lecturaReg, EIGlobal.NivelLog.INFO);
							lecturaCamposConsul(lecturaReg,lista_nombres_archivos);
							strTabla=strTabla + creaRegTablaConsul();
						//}
					}
				} while (lecturaReg!=null);
				//} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
				try {
					archivoTrabajotmp.close();
				}
				catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 3()"+e, EIGlobal.NivelLog.INFO);
				}
				strTabla=strTabla+"</table>";
			}
			return strTabla;
		}

		public void lecturaCamposConsul(String registro){
			/* Detalle del archivo */
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCamposConsul()&"+ registro, EIGlobal.NivelLog.INFO);
		//	sec_pago;fch_recep;fch_aplic;cta_cargo;estatus;nom_arch;importe_aplic;num_reg;

			try {
			sec_pago		  = registro.substring(0,posCar(registro,';',1));
			fch_recep		  = registro.substring((posCar(registro,';',1))+1,posCar(registro,';',2));
			if (fch_recep.length() >= 8){
			    fch_recep		  =  fch_recep.substring(0,2) + "/" + fch_recep.substring(2,4) + "/" + fch_recep.substring(4,8);
            }
            fch_cargo		  = registro.substring((posCar(registro,';',2))+1,posCar(registro,';',3));
			if (fch_cargo.length() >= 8) {
			    fch_cargo		  = fch_cargo.substring(0,2) + "/" + fch_cargo.substring(2,4) + "/" + fch_cargo.substring(4,8);
            }
			fch_aplic		  = registro.substring((posCar(registro,';',3))+1,posCar(registro,';',4));
			if (fch_aplic.length() >= 8) {
			    fch_aplic		  = fch_aplic.substring(0,2) + "/" + fch_aplic.substring(2,4) + "/" + fch_aplic.substring(4,8);
            }
			hra_aplic		  = registro.substring((posCar(registro,';',4))+1,posCar(registro,';',5));
			cta_cargo		  = registro.substring((posCar(registro,';',5))+1,posCar(registro,';',6));
            estatus			  = registro.substring((posCar(registro,';',6))+1,posCar(registro,';',7));
			nom_arch          = registro.substring((posCar(registro,';',7))+1,posCar(registro,';',8));
            importe_aplic     = registro.substring((posCar(registro,';',8))+1,posCar(registro,';',9));
            importe_sinformato=importe_aplic;

			num_reg           = registro.substring((posCar(registro,';',9))+1,registro.length()-1);
			EIGlobal.mensajePorTrace("***num_reg "+ num_reg , EIGlobal.NivelLog.INFO);
			importe_aplic     = formatoMoneda(importe_aplic);

			} catch(Exception e) {
				EIGlobal.mensajePorTrace("***lecturaCamposConsul ERROR: "+ e.getMessage() , EIGlobal.NivelLog.ERROR);
			 }
		}

		public void lecturaCamposConsul(String registro,String lista_nombres_archivos){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCamposConsul() registro&"+ registro, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCamposConsul() lista_nombres_archivos&"+ lista_nombres_archivos, EIGlobal.NivelLog.INFO);
		//	sec_pago;fch_recep;fch_aplic;cta_cargo;estatus;nom_arch;importe_aplic;num_reg;
			sec_pago		  = registro.substring(0,posCar(registro,';',1));
			try {
			fch_recep		  = registro.substring((posCar(registro,';',1))+1,posCar(registro,';',2));
			if (fch_recep.length() >= 8) {
			    fch_recep		  =  fch_recep.substring(0,2) + "/" + fch_recep.substring(2,4) + "/" + fch_recep.substring(4,8);
            }
            fch_cargo		  = registro.substring((posCar(registro,';',2))+1,posCar(registro,';',3));
			if (fch_cargo.length() >= 8) {
			    fch_cargo		  = fch_cargo.substring(0,2) + "/" + fch_cargo.substring(2,4) + "/" + fch_cargo.substring(4,8);
            }
			fch_aplic		  = registro.substring((posCar(registro,';',3))+1,posCar(registro,';',4));
			if (fch_aplic.length() >= 8) {
			    fch_aplic		  = fch_aplic.substring(0,2) + "/" + fch_aplic.substring(2,4) + "/" + fch_aplic.substring(4,8);
            }
			hra_aplic		  = registro.substring((posCar(registro,';',4))+1,posCar(registro,';',5));
			cta_cargo		  = registro.substring((posCar(registro,';',5))+1,posCar(registro,';',6));
            estatus			  = registro.substring((posCar(registro,';',6))+1,posCar(registro,';',7));
			nom_arch          = registro.substring((posCar(registro,';',7))+1,posCar(registro,';',8));
            importe_aplic     = registro.substring((posCar(registro,';',8))+1,posCar(registro,';',9));
			num_reg           = registro.substring((posCar(registro,';',9))+1,registro.length()-1  );
			importe_sinformato=importe_aplic;

			importe_aplic     = formatoMoneda(importe_aplic);

			//voy a buscar en al lista el nom_arch si esta ponerlo en estatus cancelado
			if ( lista_nombres_archivos.indexOf(nom_arch) >= 0 )
				estatus = "Cancelado";// Modificacion RMM 20030522

			//if (nom_arch.equals(archivo_buscar))
			//	estatus = "C";


			} catch(Exception e) {
             EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %cantidadRegArchivo()"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			 }
		}


		public String creaRegTablaConsul(){
			String sTabla	= "";
			posicionReg++;


			residuo=posicionReg % 2 ;
			String bgcolor	= "textabdatobs";
			if (residuo == 0){
				bgcolor		= "textabdatcla";
			}

/********************************************************************************

Estatus

********************************************************************************/

			if (estatus.startsWith("Recibido") || estatus.startsWith("Procesado")) {
			  sTabla= "<tr><td class=\"" + bgcolor + "\" align=\"center\" nowrap><input type=radio name=valor_radio value="+ nom_arch + '@' + sec_pago + '@' + fch_recep + '@' + fch_aplic+ '@' + cta_cargo+ '@' + importe_sinformato+ '@' + estatus+"|"+estatus+"></td>";
			} else {
			  sTabla= "\n<tr>\n<td class=\"" + bgcolor + "\" align=\"center\" nowrap>&nbsp</td>";
            }

			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + fch_recep + "&nbsp</td>";
            sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + fch_cargo + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + fch_aplic + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + hra_aplic + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + cta_cargo + "&nbsp</td>";

			//Link para los archivos procesados ...

			String nom_arch_down="";
			if(nom_arch.indexOf(".")>0)
				nom_arch_down=nom_arch.substring(0,nom_arch.indexOf("."))+".mail.gz";
			else
			    nom_arch_down=nom_arch+".mail.gz";
			/*
			if (estatus.equals("Procesado") )
			   sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"left\"><a href=\"javascript:js_descargaArchivo('"+nom_arch_down+"');\">"   + nom_arch + "&nbsp</a></td>";
			else
				*/
			   sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"left\">"   + nom_arch  + "&nbsp</td>";

			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\"> " + sec_pago + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + num_reg  + "&nbsp</td>";
			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"right\">"  + importe_aplic  + "&nbsp</td>";

			sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"left\">"   + estatus + "&nbsp</td>\n\n";


			/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
			if(VAL_TIPO_SOLPDF_TR.equals(tipoNomina)){
				String datos = "";
				int secuencia = 0;
				SolPDFBean solPDFBean = null;
				String estatusSol = " ";
				try{
				if(estatus.startsWith("Procesado") && solicitudesPDF != null){

					datos =  sec_pago + "@" + fch_aplic + "@" + fch_recep+ "@";
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Validando archivo procesado", EIGlobal.NivelLog.DEBUG);

					secuencia =  Integer.parseInt(sec_pago.trim());
					solPDFBean = solicitudesPDF.get(secuencia);
					if(solPDFBean != null){
						estatusSol =  solPDFBean.getEstatus();

						if("S".equals(estatusSol)){
							estatusSol = "Solicitado";
						}if("P".equals(estatusSol)){
							estatusSol = "Procesado";
						}if("R".equals(estatusSol)){
							estatusSol = "Rechazado";
						}if("N".equals(estatusSol)){
							estatusSol = " ";
						}if("D".equals(estatusSol)){
							estatusSol = "Disponible";
						}
					}
				}
				}catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %cantidadRegArchivo()"+e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
				sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"left\">";
				if("Disponible".equals(estatusSol)){
					sTabla=sTabla +"<a href = \"javascript:js_descargaPDF('" + datos+ "');\" >" + estatusSol +"</a>";
				}else if("Rechazado".equals(estatusSol)){
					sTabla=sTabla +"<a href = \"javascript:cuadroDialogo('" + solPDFBean.getObservaciones() + "',1);\">" + estatusSol + "</a>";
				}else{
					sTabla=sTabla +  estatusSol;
				}
				sTabla=sTabla + "&nbsp</td>\n\n";



			}
			sTabla=sTabla + "</tr>";
			return sTabla;
		}

		public int cantidadRegArchivo(){
		EIGlobal.mensajePorTrace("***ArchivosNomina.class Entrando a cantidadRegArchivo()", EIGlobal.NivelLog.INFO);

			String lecturaReg	= "", encabezado	= "";
			int contador	= 0;

			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();					//lectura del encabezado
			}
				catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %cantidadRegArchivo()"+e, EIGlobal.NivelLog.INFO);
				return 0;
			}
				do {
					try{
						posicionReg=archivoTrabajo.getFilePointer();
						lecturaReg=archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %cantidadRegArchivo()"+e, EIGlobal.NivelLog.INFO);
						return 0;
					}
				if (lecturaReg!=null){
						contador++;
					}
				}while (lecturaReg!=null);

				try {
					archivoTrabajo.close();
				}
				catch(IOException e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %lecturaArchivoSalida"+e, EIGlobal.NivelLog.INFO);
				}
		return contador;
		}

		public void lecturaCamposAS(String r){
			/* Detalle del archivo */
			if(r==null)
				r = "";
//OK      ;26/08/2002;1;1;0;30/08/2002;12.000000;12.000000;0.000000;50000000068;0.
//000000;0.000000;80000648376;348747;
//50000000068;12.000000;R;9907448;;;

			try{
			 NumeroCuenta    = r.substring(0,r.indexOf(';'));
			 Importe		 = r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			 Edo             = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
			 Referencia      = r.substring(posCar(r,';',3)+1,posCar(r,';',4));
			 Sexo            = r.substring(posCar(r,';',4)+1,posCar(r,';',5));
			} catch(Exception e) {
				EIGlobal.mensajePorTrace("***ERROR"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}

		public void lecturaEncabezadoAS(String r){
			/* Encabezado del archivo */

			int tot=0,acep=0,rech=0;
			if(r==null)
				r= "";
			try{
			 asFechaTransmision= r.substring(posCar(r,';',1)+1,posCar(r,';',2));
			 asTotalRegistros  = r.substring(posCar(r,';',2)+1,posCar(r,';',3));
			 asAceptados       = r.substring(posCar(r,';',3)+1,posCar(r,';',4));
			 tot			   = Integer.parseInt(asTotalRegistros.trim());
			 acep			   = Integer.parseInt(asAceptados.trim());
			 rech			   = tot-acep;
			 asRechazados      = ""+rech;
			 asFechaActualizacion= r.substring(posCar(r,';',4)+1,posCar(r,';',5));
			} catch(Exception e) {
				EIGlobal.mensajePorTrace("***ERROR"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}

		public String creaRegAS(int contador){
			String sTabla="",cadSexo="",cNumeroEmpleado="&nbsp;",cNumeroDepto="&nbsp;", cApellidoPaterno="&nbsp;", cApellidoMaterno="&nbsp;", cNombre="&nbsp;", cRFC="&nbsp;", cHomoclave="&nbsp;", cNumeroCuenta="&nbsp;" ,cImporte="&nbsp;", cReferencia="&nbsp;", cEdo="&nbsp;";
			String sColor="";
				if (Sexo.trim().equals("M")) {cadSexo="MASCULINO";	}
				if (Sexo.trim().equals("F")) {cadSexo="FEMENINO";}
				if (!NumeroEmpleado.trim().equals("")) {cNumeroEmpleado=NumeroEmpleado;}
				if (!ApellidoPaterno.trim().equals("")) {cApellidoPaterno=ApellidoPaterno;}
				if (!Nombre.trim().equals("")) {cNombre=Nombre;}
				if (!RFC.trim().equals("")) {cRFC=RFC;}
				if (!NumeroCuenta.trim().equals("")) {cNumeroCuenta=NumeroCuenta;}
				if (!Importe.trim().equals("")) {cImporte=Importe;}
				if (!Referencia.trim().equals("")) {cReferencia=Referencia;}
				if (!Edo.trim().equals("")) {cEdo=Edo;}
				if ((contador%2)==0) {sColor="#FFFFFF";}
				else {sColor="#F0F0F0";}
				sTabla= "<tr><td class=\"textabdatobs\" align=\"center\" nowrap>&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + cNumeroCuenta + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"right\">" + formatea( cImporte ) + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + cReferencia + "&nbsp;</td>";
				sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + cEdo + "&nbsp;</td></tr>\n";
			return 	sTabla;
		}

		public String reporteArchivo(){
			String lecturaReg="", encabezado="";
			String strTabla	= "";
			int contador	= 0;
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a reporteArchivo()###&", EIGlobal.NivelLog.INFO);
			strTabla="<table align=center cellspacing=0 border=0>";
			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">"+
									"<tr> "+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a reporteArchivo()&"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
					posicionReg=archivoTrabajo.getFilePointer();
					lecturaReg=archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class>>Error leyendo el Archivo: "+e.getMessage(), EIGlobal.NivelLog.ERROR);
						return "Error 3: "+ e.getMessage();

					}
					if (lecturaReg!=null){
						if (lecturaReg.substring(0,1).equals("2")){
							lecturaCampos(lecturaReg);
							strTabla=strTabla + creaRegReporte(contador);
							contador++;
						}
					}
				} while (lecturaReg.substring(0,1).equals("2") || lecturaReg.substring(0,1).equals("0"));
			try{
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina()"+e, EIGlobal.NivelLog.INFO);
				return "Error 4: "+ e.getMessage();
			}
		}
		//else return "";
		strTabla=strTabla+"</table>";
		return strTabla;
		}

		public String creaRegReporte(int contador){

			String sColor = "";
			String sTabla="";
			if ((contador%2)==0) {sColor="#FFFFFF";}
				else {sColor="#F0F0F0";}

			sTabla= "<tr><td class=\"textabdatobs\" nowrap align=\"center\">"+ NumeroEmpleado + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + ApellidoPaterno + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + ApellidoMaterno + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + Nombre + "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">" + NumeroCuenta + "</td>";
			//sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"right\">" + agregarPunto(Importe)  + "</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"right\">" + formatoMoneda(Importe)  + "</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">"+Referencia+ "&nbsp;</td>";
			sTabla=sTabla + "<td class=\"textabdatobs\" nowrap align=\"center\">"+Edo+ "&nbsp;</td>";
			sTabla=sTabla + "</td></tr>";
			return 	sTabla;
		}

	public String reporteArchivoSalida(){
			String lecturaReg="", encabezado="";
			String strTabla	= "";
			int contador	= 0;
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a reporteArchivoSalida()&", EIGlobal.NivelLog.INFO);
			strTabla="<table align=center cellspacing=0 border=0>";
			strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
							        "<tr> "+
									"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
									"</tr>"+
									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">"+
									"<tr> "+
									"<td width=\"55\" class=\"tittabdat\" align=\"center\">No. de empleado</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido paterno</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Apellido materno</td>"+
									"<td width=\"100\" class=\"tittabdat\" align=\"center\">Nombre</td>"+
									"<td width=\"70\" class=\"tittabdat\" align=\"center\">Cuenta</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"right\">Importe</td>"+
									"<td width=\"80\" class=\"tittabdat\" align=\"center\">Referencia</td>"+
									"<td class=\"tittabdat\" align=\"center\" width=\"50\">Estado</td>"+
									"</tr>";
			try{
				archivoTrabajo.seek(0);
				encabezado=archivoTrabajo.readLine();
			}	//lectura del encabezado
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando a reporteArchivoSalida()&"+e, EIGlobal.NivelLog.INFO);
				return "Error 1: "+ e.getMessage();
			}
			if (encabezado!=null){
				do{
					try{
					posicionReg=archivoTrabajo.getFilePointer();
					lecturaReg=archivoTrabajo.readLine();
					}
					catch(Exception e){
						EIGlobal.mensajePorTrace("***ArchivosNomina.class>>Error leyendo el Archivo: "+e.getMessage(), EIGlobal.NivelLog.ERROR);
						return "Error 3: "+ e.getMessage();
					}
					if (lecturaReg!=null){
							lecturaCamposAS(lecturaReg);
							strTabla=strTabla + creaRegReporte(contador);
							contador++;
						}
				} while (lecturaReg!=null);
			try{
				archivoTrabajo.close();
			}
			catch(IOException e){
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %reporteArchivoSalida()"+e, EIGlobal.NivelLog.INFO);
				return "Error 4: "+ e.getMessage();
			}
		}
		//else return "";
		strTabla=strTabla+"</table>";
		return strTabla;
		}


	public int archivoExportar(){
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &entrando archivoExportar() 1&", EIGlobal.NivelLog.INFO);
		String lecturaReg	= "", encabezado	= "" , linea = "" ,lineaUltima= "";
		String archivo_salida = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() + "Nom.doc";
		BufferedWriter salida;
		try {
		salida	= new BufferedWriter( new FileWriter( archivo_salida ) );
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Leyendo encabezado&", EIGlobal.NivelLog.INFO);
			encabezado=archivoTrabajotmp.readLine();
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
			if ( encabezado.equals( "" ) )
				encabezado = archivoTrabajotmp.readLine();

				if (encabezado!=null)
					{
				if(! encabezado.substring(0,2).equals("OK"))
					{
						encabezado += "\r\n";
						salida.write( encabezado ,0 ,encabezado.length() );
					}
			}//lectura del encabezado
		}catch(Exception e){
			EIGlobal.mensajePorTrace( "***ArchivosNomina.class  Excepci�n en archivoExportar() >>"+e.getMessage()+"<<2", EIGlobal.NivelLog.ERROR);
			return 0;
		} // try
			if (encabezado!=null){
				do{
					try{
						linea=archivoTrabajotmp.readLine();
						if (linea!=null){
						lineaUltima = lecturaCamposExporta(linea);
						lineaUltima = lineaUltima + "\r\n";
						salida.write( lineaUltima, 0, lineaUltima.length() );
						salida.flush();
						}
					}catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 2()"+e, EIGlobal.NivelLog.INFO);
					return 0;
					}
				} while (linea!=null);
				try {
					archivoTrabajotmp.close();
					salida.close();


		         //Inicia Modif PVA 28/03/2003
		            ArchivoRemoto archR = new ArchivoRemoto();
		           if(!archR.copiaLocalARemoto(session.getUserID8() + "Nom.doc","WEB"))
		               EIGlobal.mensajePorTrace("***ArchivosNomina.java   No se creao archivo de exportacion:"+session.getUserID8() + "Nom.doc", EIGlobal.NivelLog.INFO);
		           else
		               EIGlobal.mensajePorTrace("***ArchivosNomina.java  archivo de exportacion creado:"+session.getUserID8() + "Nom.doc", EIGlobal.NivelLog.INFO);
		          //Fin Modif  PVA 28/03/2003



				}
				catch(Exception e){
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 3()"+e, EIGlobal.NivelLog.INFO);
					return 0;
				}
			}
		return 1;
		}

		public String lecturaCamposExporta(String registro){
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCamposExporta() registro&"+ registro, EIGlobal.NivelLog.INFO);
			String linea = "" , sec_pagoExp = "", fch_recepExp = "", fch_aplicExp = "",cta_cargoExp = "",estatusExp = "", nom_archExp = "",
			importe_aplicExp ="",num_regExp = "", hra_aplicExp ="";
		//	sec_pago;fch_recep;fch_aplic;cta_cargo;estatus;nom_arch;importe_aplic;num_reg;
			try {
			fch_recepExp	  = registro.substring((posCar(registro,';',1))+1,posCar(registro,';',2));
			fch_aplicExp	  = registro.substring((posCar(registro,';',2))+1,posCar(registro,';',3));
			cta_cargoExp	  = registro.substring((posCar(registro,';',3))+1,posCar(registro,';',4));
			sec_pagoExp		  = registro.substring(0,posCar(registro,';',1));
			estatusExp		  = registro.substring((posCar(registro,';',4))+1,posCar(registro,';',5));
			nom_archExp       = registro.substring((posCar(registro,';',5))+1,posCar(registro,';',6));
			importe_aplicExp  = registro.substring((posCar(registro,';',6))+1,posCar(registro,';',7));
			num_regExp        = registro.substring((posCar(registro,';',7))+1,posCar(registro,';',8));
			sec_pagoExp		  = completaCampos(sec_pagoExp,5,"izq");
			nom_archExp       = completaCampos(nom_archExp,40,"izq");
			importe_aplicExp  = completaCampos(importe_aplicExp,20,"izq");
			num_regExp        = completaCampos(num_regExp,15,"izq");

			linea = fch_recepExp + fch_aplicExp + cta_cargoExp + nom_archExp + sec_pagoExp + num_regExp + importe_aplicExp  + estatusExp;
			} catch(Exception e) {
				EIGlobal.mensajePorTrace("***ERROR"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			 }
	 EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCamposExporta() linea&"+ linea, EIGlobal.NivelLog.INFO);
		return linea;
		}

		public String completaCampos(String cadena, int longRequerida, String sentido){
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo completaCampos() cadena&" , EIGlobal.NivelLog.INFO);
			String blancos="";
			int longitud= cadena.length();
			int faltantes= longRequerida-longitud;
		if(faltantes>0){
			for(int i=0; i<faltantes; i++){
				blancos=blancos+" ";
			}
			if(sentido.equals("izq"))
			cadena= blancos+cadena;
			else
			cadena= cadena+blancos;
		}
		return cadena;
		}

		public int posCar(String cad,char car,int cant){
		int result=0,pos=0;
		for (int i=0;i<cant;i++){
			result=cad.indexOf(car,pos);
			pos=result+1;
			}
		return result;
		}

		public String formatea(String formatea){
		String decimales	= "";
		String enteros		= "";
		int position		= formatea.indexOf('.');
		if(position>=0){
			decimales		= formatea.substring(position+1, formatea.length());
			if(position>=1)
				enteros		= formatea.substring(0, position);
			if(decimales.length()>2)
				decimales	= decimales.substring(0, 2);
			if(decimales.length()==1)
				decimales	= decimales+"0";
		}
		else{
			enteros	= formatea;
			decimales="00";
		}
		return enteros+"."+decimales;
	}

	public String formatoMoneda( String cantidad )
	{
		String language = "la"; // ar
		String country  = "MX";  // AF
		Locale local    = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato  = "";
		String cantidadDecimal = "";
		String formatoFinal    = "";
		String cantidadtmp     = "";

		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.0";
			try {
				cantidadtmp =cantidad.substring(0,cantidad.length()-2);
				cantidadDecimal = cantidad.substring(cantidad.length()-2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				if (formato.substring (0,3).equals ("MXN"))
					formato=formato.substring(3,formato.length()-2);
				else
					formato=formato.substring(1,formato.length()-2);
				formatoFinal = "$ "+ formato + cantidadDecimal;
			} catch(NumberFormatException e) {
				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.INFO);
				formatoFinal="0.00";}
		if(formatoFinal==null)
			formato = "";
		return formatoFinal;
	}


	public String lecturaArchivoSalidaConsulInd()
	{
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaArchivoSalidaConsul()&", EIGlobal.NivelLog.INFO);
		String lecturaReg	= "", encabezado	= "";
		String strTabla		= "";

		strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
		        "<tr> "+
				"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
				"</tr>"+
				"</table>"+
				"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
				"<tr> "+
				"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
				"<td width=\"55\" class=\"tittabdat\" align=\"center\">Secuencia</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha recepci�n</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Hora de aplicaci&oacute;n</td>"+
				"<td width=\"100\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Estatus</td>"+
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe aplicado</td>"+
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Empleado</td>"+
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Tarjeta</td>"+
				"</tr>";

		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Leyendo encabezado 1&", EIGlobal.NivelLog.INFO);
		try
		{
			encabezado=archivoTrabajotmp.readLine();
			if (encabezado.equals(""))
					encabezado = archivoTrabajotmp.readLine();
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 1()"+e, EIGlobal.NivelLog.INFO);
			return "Error 1: "+ e.getMessage();
		}

		String existenDatos = encabezado.substring((posCar(encabezado,';',1))+1,posCar(encabezado,';',2));
		if (encabezado!=null && existenDatos.equals("NOMI0000"))
		{
			do
			{
				try
				{
					EIGlobal.mensajePorTrace("***ArchivosNomina.class estoy dentro del do while**", EIGlobal.NivelLog.INFO);
					lecturaReg=archivoTrabajotmp.readLine();
					EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 1**"+lecturaReg, EIGlobal.NivelLog.INFO);
				}
				catch(Exception e)
				{
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 2()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
				}

				if (lecturaReg!=null)
				{
					EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 2**"+lecturaReg, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***ArchivosNomina.class>>lecturaReg: "+lecturaReg, EIGlobal.NivelLog.INFO);
					lecturaCamposConsulInd(lecturaReg);
					strTabla=strTabla + creaRegTablaConsulInd();
				}
			}while(lecturaReg!=null);

			try
			{
				archivoTrabajotmp.close();
			}
			catch(Exception e)
			{
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 3()"+e, EIGlobal.NivelLog.INFO);
			}
			strTabla=strTabla+"</table>";
		}
		else
		{
			strTabla=strTabla+"NO EXISTEN DATOS";
		}
		return strTabla;
	}

	public String lecturaArchivoSalidaConsulInd(String lista_nombres_archivos)
	{
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaArchivoSalidaConsul()&", EIGlobal.NivelLog.INFO);
		String lecturaReg	= "", encabezado	= "";
		String strTabla		= "";

		strTabla="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
		        "<tr> "+
				"<td class=\"textabref\">Pago de n&oacute;mina</td>"+
				"</tr>"+
				"</table>"+
				"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">" +
				"<tr> "+
				"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
				"<td width=\"55\" class=\"tittabdat\" align=\"center\">Secuencia</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha recepci�n</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Hora de aplicaci&oacute;n</td>"+
				"<td width=\"100\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>"+
				"<td width=\"70\" class=\"tittabdat\" align=\"center\">Estatus</td>"+
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe aplicado</td>"+
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Emplaedo</td>"+
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Tarjeta</td>"+
				"</tr>";

		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Leyendo encabezado 1&", EIGlobal.NivelLog.INFO);
		try
		{
			encabezado=archivoTrabajotmp.readLine();
			if (encabezado.equals(""))
					encabezado = archivoTrabajotmp.readLine();
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &El metodo readLine() se ha ejecutado correctamente&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNomina.class &Encabezado: &"+encabezado, EIGlobal.NivelLog.INFO);
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 1()"+e, EIGlobal.NivelLog.INFO);
			return "Error 1: "+ e.getMessage();
		}

		String existenDatos = encabezado.substring((posCar(encabezado,';',1))+1,posCar(encabezado,';',2));
		if (encabezado!=null && existenDatos.equals("NOMI0000"))
		{
			do
			{
				try
				{
					EIGlobal.mensajePorTrace("***ArchivosNomina.class estoy dentro del do while**", EIGlobal.NivelLog.INFO);
					lecturaReg=archivoTrabajotmp.readLine();
					EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 1**"+lecturaReg, EIGlobal.NivelLog.INFO);
				}
				catch(Exception e)
				{
					EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 2()"+e, EIGlobal.NivelLog.INFO);
					return "Error 3: "+ e.getMessage();
				}

				if (lecturaReg!=null)
				{
					EIGlobal.mensajePorTrace("***ArchivosNomina.class lecturaReg 2**"+lecturaReg, EIGlobal.NivelLog.INFO);

					lecturaCamposConsulInd(lecturaReg,lista_nombres_archivos);
					strTabla=strTabla + creaRegTablaConsulInd();
				}
			} while (lecturaReg!=null);

			try
			{
				archivoTrabajotmp.close();
			}
			catch(Exception e)
			{
				EIGlobal.mensajePorTrace("***ArchivosNomina.class Ha ocurrido una excepcion en: %ArchivosNomina 3()"+e, EIGlobal.NivelLog.INFO);
			}
			strTabla=strTabla+"</table>";
		}
		return strTabla;
	}

	public void lecturaCamposConsulInd(String registro)
	{
		/* Detalle del archivo */
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCamposConsul()&"+ registro, EIGlobal.NivelLog.INFO);

        sec_pago = registro.substring(0,posCar(registro,';',1));
		try
		{
			fch_recep = registro.substring((posCar(registro,';',1))+1,posCar(registro,';',2));
			if (fch_recep.length() >= 8)
			{
				fch_recep = fch_recep.substring(0,2) + "/" + fch_recep.substring(2,4) + "/" + fch_recep.substring(4,8);
			}
			fch_aplic = registro.substring((posCar(registro,';',2))+1,posCar(registro,';',3));
			if (fch_aplic.length() >= 8)
			{
				fch_aplic = fch_aplic.substring(0,2) + "/" + fch_aplic.substring(2,4) + "/" + fch_aplic.substring(4,8);
			}
			hra_aplic = registro.substring((posCar(registro,';',3))+1,posCar(registro,';',4));
			cta_cargo = registro.substring((posCar(registro,';',4))+1,posCar(registro,';',5));
			estatus = registro.substring((posCar(registro,';',5))+1,posCar(registro,';',6));
			importe_aplic = registro.substring((posCar(registro,';',6))+1,posCar(registro,';',7));
			NumeroEmpleado = registro.substring((posCar(registro,';',7))+1,posCar(registro,';',8));
			num_tarjeta = registro.substring((posCar(registro,';',8))+1,registro.length());
			importe_sinformato=importe_aplic;

			EIGlobal.mensajePorTrace("***num_reg "+ num_reg , EIGlobal.NivelLog.INFO);
			importe_aplic     = formatoMoneda(importe_aplic);
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("***ERROR"+e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
	}

	public void lecturaCamposConsulInd(String registro,String lista_nombres_archivos)
	{
		/* Detalle del archivo */
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo lecturaCamposConsul()&"+ registro, EIGlobal.NivelLog.INFO);

        sec_pago = registro.substring(0,posCar(registro,';',1));
		try
		{
			fch_recep = registro.substring((posCar(registro,';',1))+1,posCar(registro,';',2));
			if (fch_recep.length() >= 8)
			{
				fch_recep = fch_recep.substring(0,2) + "/" + fch_recep.substring(2,4) + "/" + fch_recep.substring(4,8);
			}
			fch_aplic = registro.substring((posCar(registro,';',2))+1,posCar(registro,';',3));
			if (fch_aplic.length() >= 8)
			{
				fch_aplic = fch_aplic.substring(0,2) + "/" + fch_aplic.substring(2,4) + "/" + fch_aplic.substring(4,8);
			}
			hra_aplic = registro.substring((posCar(registro,';',3))+1,posCar(registro,';',4));
			cta_cargo = registro.substring((posCar(registro,';',4))+1,posCar(registro,';',5));
			estatus = registro.substring((posCar(registro,';',5))+1,posCar(registro,';',6));
			importe_aplic = registro.substring((posCar(registro,';',6))+1,posCar(registro,';',7));
			NumeroEmpleado = registro.substring((posCar(registro,';',7))+1,posCar(registro,';',8));
			num_tarjeta = registro.substring((posCar(registro,';',8))+1,registro.length());
			importe_sinformato=importe_aplic;

			EIGlobal.mensajePorTrace("***num_reg "+ num_reg , EIGlobal.NivelLog.INFO);
			importe_aplic     = formatoMoneda(importe_aplic);
			//voy a buscar en al lista el nom_arch si esta ponerlo en estatus cancelado
			if (lista_nombres_archivos.indexOf(sec_pago) >= 0 )
				estatus = "Cancelado";// Modificacion RMM 20030522

			EIGlobal.mensajePorTrace("***ArchivosNomina.class >>"+sec_pago + "busca en :"+lista_nombres_archivos, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ArchivosNomina.class>>Resultado :"+lista_nombres_archivos.indexOf(sec_pago), EIGlobal.NivelLog.INFO);
		} catch(Exception e)
		{
			EIGlobal.mensajePorTrace("***ERROR"+e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
	}

	public String creaRegTablaConsulInd()
	{
		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Entrando al metodo creaRegTablaConsul() el bueno&", EIGlobal.NivelLog.INFO);
		String sTabla	= "";
		posicionReg++;

		residuo=posicionReg % 2 ;
		String bgcolor	= "textabdatobs";
		if (residuo == 0)
		{
			bgcolor		= "textabdatcla";
		}

/********************************************************************************

Estatus

********************************************************************************/

		if (estatus.startsWith("Recibido") || estatus.startsWith("Procesado"))
		{
		  sTabla= "<tr><td class=\"" + bgcolor + "\" align=\"center\" nowrap><input type=radio name=valor_radio value="+ NumeroEmpleado + '@' + sec_pago + '@' + fch_recep+ '@' + fch_aplic+ '@' + cta_cargo+ '@' + num_tarjeta+ '@' + importe_sinformato+ '@' + estatus +"|"+estatus+"></td>";
		}
		else
		{
		  sTabla= "\n<tr>\n<td class=\"" + bgcolor + "\" align=\"center\" nowrap>&nbsp</td>";
        }

		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + sec_pago  + "&nbsp</td>";
		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + fch_recep + "&nbsp</td>";
        sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + fch_aplic + "&nbsp</td>";
		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + hra_aplic + "&nbsp</td>";
		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + cta_cargo + "&nbsp</td>";
		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"left\">"   + estatus + "&nbsp</td>";
		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"right\">"  + importe_aplic + "&nbsp</td>";
		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + NumeroEmpleado + "&nbsp</td>";
		sTabla=sTabla + "<td class=\"" + bgcolor + "\" nowrap align=\"center\">" + num_tarjeta + "&nbsp</td>\n</tr>\n";


		EIGlobal.mensajePorTrace("***ArchivosNomina.class &Saliendo del metodo creaRegTablaConsul()&", EIGlobal.NivelLog.INFO);
		return sTabla;
	}
}