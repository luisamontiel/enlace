/**
 * 
 */
package mx.altec.enlace.bo;

import java.util.HashMap;

import mx.altec.enlace.beans.DatosBeneficiarioBean;
import mx.altec.enlace.dao.OperacionesInternacionalesDAO;
import mx.altec.enlace.utilerias.EIGlobal;

import org.json.simple.JSONObject;

/**
 * @author FSW Indra
 *
 */
public class OperacionesInternacionalesBO {

	/** Operacion misma divisa */ 
	transient public static final String OP_MISMA_DIVISA = "12101";
	/** Operacion cambiaria */
	transient public static final String OP_CAMBIARIA = "12105";
	
	
	/**
	 * Envia operaciones internacionales a eTransferPlus
	 * @param beneficiarioBean Bean con datos del beneficiario
	 * @param cveTransfe Clave de transferencia
	 * @param referencia Referencia
	 * @param usuario Usuario
	 * @param cuentaCargo Cuenta cargo
	 * @param nombreOrd Nombre ordenante
	 * @param importeDA Importe
	 * @param cveDivisaOrd Clave divisa ordenante
	 * @param apPaternoOrd Apellido paterno ordenante
	 * @param apMaternoOrd Apellido materno ordenante
	 * @param buc Clave de usuario
	 * @param cuentaAbono Cuenta abono
	 * @param tipoCambioOperacion Tipo de cambio de operacion
	 * @param importeFinal Importe final 
	 * @return String[] datos de la respuesta de operacion
	 */
	public String[] enviaOperacionInternacional (DatosBeneficiarioBean beneficiarioBean,
			String cveTransfe, String referencia, String usuario,  
			String cuentaCargo, String nombreOrd, String importeDA, String cveDivisaOrd, 
			String apPaternoOrd, String apMaternoOrd, String buc, String cuentaAbono, 
			String tipoCambioOperacion, String importeFinal, String ciudad, String concepto, String cveEspecial) {
		HashMap<String, String> operacion = new HashMap<String, String>();
		String[] resultado = new String[3];
		OperacionesInternacionalesDAO internacionalesDAO = new OperacionesInternacionalesDAO();
		Double importeCargo;
		
		EIGlobal.mensajePorTrace ("OperacionesInternacionalesBO::enviaOperacionInternacional :: -> importeDA:" + importeDA, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("OperacionesInternacionalesBO::enviaOperacionInternacional :: -> importeFinal:" + importeFinal, EIGlobal.NivelLog.INFO);
		
		operacion.put("SERVICIO", "PROCESAOPERACION");
		operacion.put("CVE_TRANSFE", cveTransfe);
		operacion.put("CVE_EMPRESA", "0014");
		operacion.put("CVE_PTO_VTA", "0981");
		operacion.put("REFERENCIA_MED", referencia);
		operacion.put("CVE_USUARIO_CAP", usuario);
		operacion.put("CVE_USUARIO_SOL", usuario);
		operacion.put("CVE_PTO_VTA_ORD", "0981");
		operacion.put("NUM_CUENTA_ORD", cuentaCargo);
		operacion.put("TIPO_CUENTA_ORD", "101");
		operacion.put("NOMBRE_ORD", nombreOrd); //PENOMPE
		//importeCargo=calculaImporteCargo(beneficiarioBean.getCveDivisa(),cveDivisaOrd,importeDA,tipoCambioOperacion);
		if (cveDivisaOrd.equalsIgnoreCase("MXP")){
			operacion.put("IMPORTE_CARGO", importeFinal); //directa y cambiaria
		}else if(cveDivisaOrd.equalsIgnoreCase("USD")&&beneficiarioBean.getCveDivisa().equalsIgnoreCase("USD")){
			operacion.put("IMPORTE_CARGO", importeDA);
		}else{
			operacion.put("IMPORTE_CARGO", importeFinal); //directa y cambiaria
		}
		//operacion.put("IMPORTE_CARGO", importeCargo.toString());
		operacion.put("CVE_DIVISA_ORD", cveDivisaOrd); //directa y cambiaria
		operacion.put("A_PATERNO_ORD", apPaternoOrd); //PEPRIAP
		operacion.put("A_MATERNO_ORD", apMaternoOrd); //PESEGAP
		operacion.put("ID_CLIENTE_ORD", buc); //PENUMPE
		operacion.put("TIPO_ID_CLIENTE_ORD", "BUC");
		operacion.put("CVE_PAIS_INST_REC", beneficiarioBean.getCvePais());
		operacion.put("NOMBRE_INST_REC", beneficiarioBean.getDescBanco());
		operacion.put("NUM_CUENTA_REC", cuentaAbono);
		operacion.put("TIPO_CUENTA_REC", "999");
		operacion.put("NOMBRE_BEN_REC", beneficiarioBean.getDescTitular());
		operacion.put("IMPORTE_ABONO", importeDA); //directa y cambiaria
		operacion.put("ID_CLIENTE_REC", beneficiarioBean.getIdCliente());
		operacion.put("TIPO_ID_CLIENTE_REC", "999");
		operacion.put("CIUDAD_REC", beneficiarioBean.getCiudad());
		operacion.put("A_PATERNO_BEN_REC", "");
		operacion.put("A_MATERNO_BEN_REC", "");
		operacion.put("DIR_BEN_REC", beneficiarioBean.getDireccion());
		operacion.put("CVE_PAIS_REC", beneficiarioBean.getCvePaisBenef());
		
		if (OP_MISMA_DIVISA.equals(cveTransfe)) {
			operacion.put("CVE_DIVISA_REC", beneficiarioBean.getCveDivisa());
			operacion.put("CVE_MEDIO_ENT", "EWEB");			
			operacion.put("ID_PARTE_INST_REC",beneficiarioBean.getCveAba());
			operacion.put("BIC_INST_REC", beneficiarioBean.getCveSwift());
			operacion.put("CIUDAD_INST_REC", ciudad);
			operacion.put("INF_REMESAS",concepto.trim());			
		}else {
			operacion.put("CVE_DIVISA_REC", beneficiarioBean.getCveDivisa()); 
			operacion.put("EQUIVALENTE_MN", importeFinal);//dato que se env�a actualmente - importeFinal
			operacion.put("CVE_PTO_VTA_REC", "0981");
			operacion.put("TIPO_CAMBIO", tipoCambioOperacion);
			operacion.put("CVE_MEDIO_ENT", "CAMB");
			operacion.put("ID_CANAL_CAMBIOS", "EN");
			operacion.put("CIUDAD_INST_REC", beneficiarioBean.getDescCiudad());
			operacion.put("BIC_INST_REC", beneficiarioBean.getCveSwift());
			//operacion.put("BIC_INST_REC", "BSJUARBJ");
			operacion.put("ID_PARTE_INST_REC", beneficiarioBean.getCveAba());
			operacion.put("INF_REMESAS",concepto.trim());	
			operacion.put("CVE_DESECHABLE",cveEspecial.trim());	
		}
		
		JSONObject.toJSONString(operacion);
		EIGlobal.mensajePorTrace ("OperacionesInternacionalesBO::enviaOperacionInternacional :: -> JSON:" + JSONObject.toJSONString(operacion), EIGlobal.NivelLog.INFO);
		
		resultado = internacionalesDAO.enviaOperacionInternacional(JSONObject.toJSONString(operacion));
		return resultado;
	}
}


