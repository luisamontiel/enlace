package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import mx.altec.enlace.beans.AdmTrxB485;
import mx.altec.enlace.beans.AdmTrxPE80;
import mx.altec.enlace.dao.TrxB485DAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase Bussines para manejar el procedimiento para la transaccion B485
 *
 * @author Fernando Gurrola Herrera
 * @version 1.0 Sep 27, 2010
 */

public class TrxB485BO {

	/**
	 * Objeto contenedor de tipo AdmTrxPE80
	 */
	private AdmTrxB485 bean;

	/**
	 * Obtener el valor de bean.
     *
     * @return bean valor asignado.
	 */
	public AdmTrxB485 getBean() {
		return bean;
	}


	/**
	 * Metodo para obtener la cuenta CLABE de ejecutar la transaccion B485
	 *
	 * @param cuenta			Numero de cuenta
	 * @return cuentaClabe		Cuenta CLABE
	 */
	public String obtenCtaClabe(String cuenta) {
		EIGlobal.mensajePorTrace("TrxB485BO::obtenCtaClabe:: Entrando....", EIGlobal.NivelLog.DEBUG);

		TrxB485DAO dao = new TrxB485DAO();
		TrxB485VO beanRes = new TrxB485VO();
	    MQQueueSession mqsess = null;
	    bean = null;
	    String clabe = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
	    									NP_JNDI_QUEUE_RESPONSE,
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	bean = dao.claveBancariaEstandarizada(rellenar(cuenta,23,' ','D'));
	    	EIGlobal.mensajePorTrace("TrxB485BO::obtenCtaClabe:: Codigo operacion>>"
	    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
	    	beanRes = bean.getB485bean();
	    	EIGlobal.mensajePorTrace("TrxB485BO::obtenCtaClabe:: Valor esperado>>"
	    			+ beanRes.getClabe(), EIGlobal.NivelLog.DEBUG);
	    	clabe = beanRes.getClabe();
	    }catch (Exception e){
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenCtaClabe...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxB485BO::obtenCtaClabe:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
		               				+ "Cuenta->" + cuenta
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxB485BO::obtenCtaClabe:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
			               				+ "Cuenta->" + cuenta
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }
		EIGlobal.mensajePorTrace("TrxB485BO::obtenCtaClabe:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return clabe;
	}

	/**
	 * Metodo para obtener la divisa de la cuenta de ejecutar la transaccion B485
	 *
	 * @param cuenta			Numero de cuenta
	 * @return divisaCta		Divisa de la cuenta
	 */
	public String obtenDivisaCta(String cuenta){
		EIGlobal.mensajePorTrace("TrxB485BO::obtenDivisaCta:: Entrando....", EIGlobal.NivelLog.DEBUG);

		TrxB485DAO dao = new TrxB485DAO();
		TrxB485VO beanRes = new TrxB485VO();
	    MQQueueSession mqsess = null;
	    bean = null;
	    String divisaCta = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
	    									NP_JNDI_QUEUE_RESPONSE,
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	bean = dao.claveBancariaEstandarizada(rellenar(cuenta,23,' ','D'));
	    	EIGlobal.mensajePorTrace("TrxB485BO::obtenDivisaCta:: Codigo operacion>>"
	    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
	    	beanRes = bean.getB485bean();
	    	EIGlobal.mensajePorTrace("TrxB485BO::obtenDivisaCta:: Valor esperado>>"
	    			+ beanRes.getDivisa(), EIGlobal.NivelLog.DEBUG);
	    	divisaCta = beanRes.getDivisa();
	    }catch (Exception e){
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenDivisaCta...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxB485BO::obtenDivisaCta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
		               				+ "Cuenta->" + cuenta
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxB485BO::obtenDivisaCta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
			               				+ "Cuenta->" + cuenta
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }
		EIGlobal.mensajePorTrace("TrxB485BO::obtenDivisaCta:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return divisaCta;
	}

}