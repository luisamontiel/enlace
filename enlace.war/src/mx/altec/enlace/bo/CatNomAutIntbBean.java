package mx.altec.enlace.bo;

public class CatNomAutIntbBean {


	private char estatus;
	private String cuenta;
	private String nombre;
	private long fechaReg;
	private long fechaAut;
	private int folioReg;
	private String usrReg;
	private String usrAut;
	private char operacion;
	private String banco;
	private int folioAut;
	private String observaciones;

	public int getFolioAut() {
		return this.folioAut;
	}

	public void setFolioAut(int folioAut) {
		this.folioAut = folioAut;
	}

	public int getFolioReg() {
		return this.folioReg;
	}

	public void setFolioReg(int folioReg) {
		this.folioReg = folioReg;
	}

	public long getFechaAut() {
		return this.fechaAut;
	}

	public void setFechaAut(long fechaAut) {
		this.fechaAut = fechaAut;
	}

	public long getFechaReg() {
		return this.fechaReg;
	}

	public void setFechaReg(long fechaReg) {
		this.fechaReg = fechaReg;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getBanco() {
		return this.banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getUsrAut() {
		return this.usrAut;
	}

	public void setUsrAut(String usrAut) {
		this.usrAut = usrAut;
	}

	public String getUsrReg() {
		return this.usrReg;
	}

	public void setUsrReg(String usrReg) {
		this.usrReg = usrReg;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public char getOperacion() {
		return this.operacion;
	}

	public void setOperacion(char operacion) {
		this.operacion = operacion;
	}

	public char getEstatus() {
		return this.estatus;
	}

	public void setEstatus(char estatus) {
		this.estatus = estatus;
	}

	protected void finalize() throws Throwable {
		super.finalize();
		cuenta = null;
		nombre = null;
		usrReg = null;
		usrAut = null;
		banco = null;
		observaciones = null;
	}

	public boolean equals(Object obj) {
		if(obj instanceof CatNomAutIntbBean) {
			if(((CatNomAutIntbBean) obj).getFolioReg() == (this.folioReg)) {
				return true;
			}
		}
		return false;
	}

	public String toString() {
		StringBuffer app = new StringBuffer();
		app.append("Estatus: " + estatus);
		app.append(", Cuenta: " + cuenta);
		app.append(", Nombre: " + nombre);
		app.append(", FechaReg: " + fechaReg);
		app.append(", FechaAut: " + fechaAut);
		app.append(", FolioReg: " + folioReg);
		app.append(", UsrReg: " + usrReg);
		app.append(", UsrAut: " + usrAut);
		app.append(", Operacion: " + operacion);
		app.append(", Banco: " + banco);
		app.append(", folioAut: " + folioAut);
		app.append(", Observaciones: " + observaciones);
		return app.toString();
	}
}