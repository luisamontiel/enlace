/*
 * ChesChequeConsultaFormat.java
 *
 * Created on 13 de enero de 2003, 10:34 AM
 */

package mx.altec.enlace.ches;

import java.util.Date;
import java.util.GregorianCalendar;
import java.text.ParsePosition;

import mx.altec.enlace.utilerias.EIGlobal;


/** Format para ChesCheque cuando se realizan consultas de cheques
 * @author Rafael Martinez Montiel
 */
public class ChesChequeConsultaFormat extends java.text.Format {
    public static final String CLASS_NAME = ChesChequeConsultaFormat.class.getName ();
    /** instancia de la clase
     */
    private static ChesChequeConsultaFormat singleton = null;
    /** Formateador para las fechas
     */
    static java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("yyyy-MM-dd");

    /** Obtiene un a instancia del ChesChequeConsultaFormat
     * @return instancia
     */
    public static ChesChequeConsultaFormat getInstance (){
        if(null == singleton){
            singleton = new ChesChequeConsultaFormat ();
        }
        return singleton;

    }

    /** Creates a new instance of ChesChequeConsultaFormat */
    protected ChesChequeConsultaFormat () {
    }

    /** No implementado
     * @param obj objeto que se intentara formatear
     * @param stringBuffer destino de la operacion
     * @param fieldPosition Objeto para llevar un control de los campos
     * @return Cadena representando el ChesCheque
     */
    public StringBuffer format (Object obj, StringBuffer stringBuffer, java.text.FieldPosition fieldPosition) {
        return new StringBuffer ("");
    }

    /** Obtiene un ChesCheque desde una cadena de consulta de cheques
     * @param str Cadena dese la cual se intentara recuperar un ChesCheqe
     * @param parsePosition Objeto para llevar un control sobre los caracteres utilizados. Si se reconoce un ches cheque, se define index como el ultimo caracter leido, de lo contrario se define errorIndex
     * @return ChesCheque o null si ocurrio algun error
     */
    public Object parseObject (String str, ParsePosition parsePosition){
        ChesCheque retValue = null;
        ParsePosition pp = new ParsePosition ( parsePosition.getIndex () );
        //        System.out.println("pp="+pp);
        //        System.out.println("str=" + str.substring ( pp.getIndex () ) );

        Long numCuenta = parseLong ( str,pp );
        //        System.out.println("pp="+pp);
        //        System.out.println("numCheque=" + numCheque);
        if(null == numCuenta ){
            parsePosition.setErrorIndex ( pp.getErrorIndex () );
            return retValue;
        }

        Long numCheque = parseLong ( str,pp );
        //        System.out.println("pp="+pp);
        //        System.out.println("numCheque=" + numCheque);
        if(null == numCheque ){
            parsePosition.setErrorIndex ( pp.getErrorIndex () );
            return retValue;
        }
        //        System.out.println("str=" + str.substring ( pp.getIndex () ) );
        Long importe = parseLong ( str,pp);
        //        System.out.println("pp="+pp);
        //        System.out.println("importe=" + importe);
        if(null == importe){
            parsePosition.setErrorIndex ( pp.getErrorIndex () );
            return retValue;
        }
        //        System.out.println("str=" + str.substring ( pp.getIndex () ) );
        Date libDate = parseDate ( str,pp);
        //        System.out.println("pp="+pp);
        //        System.out.println("libDate=" + libDate);
        if(null == libDate){
            parsePosition.setErrorIndex ( pp.getErrorIndex () );
            return retValue;
        }
        //        System.out.println("str=" + str.substring ( pp.getIndex () ) );
        Date limDate = parseDate ( str,pp);
        //        System.out.println("pp="+pp);
        //        System.out.println("limDate=" + limDate);
        if(null == limDate){
            parsePosition.setErrorIndex ( pp.getErrorIndex () );
            return retValue;
        }
        ParsePosition ppDate = new ParsePosition ( pp.getIndex () );
        //        System.out.println("str=" + str.substring ( pp.getIndex () ) );
        String sDate = parseString ( str,pp);
        //        System.out.println("pp="+pp);
        //        System.out.println("sDate=" + sDate);
        if(null == sDate){
            parsePosition.setErrorIndex ( pp.getErrorIndex () );
            return retValue;
        }
        //        System.out.println("str=" + str.substring ( pp.getIndex () ) );
        String status = parseString ( str,pp);
        //        System.out.println("pp="+pp);
        //        System.out.println("status=" + status);
        if(null == status){
            parsePosition.setErrorIndex ( pp.getErrorIndex () );
            return retValue;
        }
        Date date= null;
        if( status.substring (0,1).equals ("C") || status.substring (0,1).equals ("L") ){
            date = parseDate ( str, ppDate );
//                        System.out.println("ppDate="+ppDate);
            if(null == date){
                parsePosition.setErrorIndex ( ppDate.getErrorIndex () );
                return retValue;
            }
        }
//                System.out.println("pp="+pp);
        retValue = new ChesCheque ();
        retValue.setNumCuenta ( numCuenta.longValue() );
        retValue.setNumCheque ( numCheque.longValue () );
        retValue.setImporte ( importe.longValue () );
        GregorianCalendar cal = new GregorianCalendar ();
        cal.setTime ( libDate );
        retValue.setFechaLibramiento ( cal );
        cal = new GregorianCalendar ();
        cal.setTime ( limDate );
        retValue.setFechaLimPago ( cal );
        if(status.substring (0,1).equals ("C") ){
            cal = new GregorianCalendar ();
            cal.setTime ( date );
            retValue.setFechaCancelacion ( cal );
        }else if(status.substring (0,1).equals ("L") ){
            cal = new GregorianCalendar ();
            cal.setTime ( date );
            retValue.setFechaPago ( cal );
        }
        parsePosition.setIndex ( pp.getIndex () );
        retValue.setStatus ( status );
        return retValue;
    }


     Long parseLong (String str, ParsePosition parsePosition){
        Long retValue = null;
        ParsePosition pp = new ParsePosition ( parsePosition.getIndex () );
        String value = parseString ( str, pp );

        if(null == value || value.equals ( "" ) ){
            if(pp.getErrorIndex () >= 0){
                parsePosition.setErrorIndex ( pp.getErrorIndex () );
            }else{
                parsePosition.setErrorIndex ( pp.getIndex () );
            }
            return null;
        }
        try{
            retValue = new Long ( value );
            parsePosition.setIndex ( pp.getIndex () );
        }catch(NumberFormatException ex){
            parsePosition.setErrorIndex ( parsePosition.getIndex () );
            return null;
        }

        return retValue;
    }

     String parseString (String str, ParsePosition parsePosition){
        String retValue = "";
        int startIndex = parsePosition.getIndex ();
        if(parsePosition.getIndex ()  == str.length ()){
            return retValue;
        }
        int endIndex = str.length ();
        if(-1 != str.indexOf ('|',startIndex) ){
            endIndex = str.indexOf ( '|', startIndex );
        }

        if( 0 < endIndex - startIndex){
            retValue = str.substring ( startIndex,endIndex );
            if(-1 != str.indexOf ('|',startIndex) ){
                parsePosition.setIndex ( endIndex +1 );
            }else{
                parsePosition.setIndex ( endIndex );
            }
        }else if( endIndex == startIndex ){
            parsePosition.setIndex ( startIndex + 1 );
        }

        return retValue;
    }

     Date parseDate (String str, ParsePosition parsePosition){
        ParsePosition pp = new ParsePosition ( parsePosition.getIndex () );
        String date = parseString (str,pp );
        if(date.length () != 10 ){
            parsePosition.setErrorIndex ( parsePosition.getIndex () );
            return null;
        }
        Date retValue = null;
        try{
            retValue = sdf.parse ( date );
            parsePosition.setIndex ( parsePosition.getIndex () + date.length () + 1 );
        }catch(java.text.ParseException ex){
            parsePosition.setErrorIndex ( parsePosition.getIndex () );
        }
        return retValue;
    }

    /** Obtiene un ChesCheque desde una cadena de consulta de cheques
     * @param str Cadena desde la cual se intentara recuperar el ChesCheque
     * @throws ParseException En caso de ocurrir algun error
     * @return ChesCheque representado por la cadena
     */
    public  ChesCheque parse ( String str )
    throws java.text.ParseException{
        ParsePosition parsePosition = new ParsePosition (0);
        ChesCheque retValue = (ChesCheque)parseObject ( str , parsePosition );
        if(null == retValue ){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": errorIndex" + parsePosition.getErrorIndex () , EIGlobal.NivelLog.INFO);

            throw new java.text.ParseException ("No es una cadena que se pueda formatear como un Cheque",parsePosition.getErrorIndex ());
        }
        return retValue;
    }



}
