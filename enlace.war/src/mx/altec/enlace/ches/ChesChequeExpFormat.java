/*
 * ChesChequeExpFormat.java
 *
 * Created on 13 de enero de 2003, 05:43 PM
 */

package mx.altec.enlace.ches;

/** Format para ChesCheque en archivos exportados
 * @author Rafael Martinez Montiel
 */
public class ChesChequeExpFormat extends java.text.Format {

    /** Longitud del campo de cuenta
     */
    public static final int CUENTA_LEN = 16;
    /** Longitud del campo numero de cheque
     */
    public static final int NUM_CHEQUE_LEN = 7;
    /** Longitud del campo de la clave del beneficiario
     */
    public static final int CVE_BENEF_LEN = 13;

    /** longitud del campo de importe
     */
    public static final int IMPORTE_LEN = 16;
    /** longitud del campo de fecha de liberacion
     */
    public static final int FECHA_LIB_LEN = 10;
    /** longitud del campo de fecha limite de pago
     */
    public static final int FECHA_LIM_PAGO = 10;
    /** longitud del campo de status
     */
    public static final int STATUS_LEN = 1;
    /** longitud total del registro
     */
    public static final int TOTAL_LEN = CUENTA_LEN + NUM_CHEQUE_LEN + CVE_BENEF_LEN
    + IMPORTE_LEN + FECHA_LIB_LEN + FECHA_LIM_PAGO + STATUS_LEN;

    /** instancia de la clase
     */
    static ChesChequeExpFormat singleton = null;

    /** obtiene una instancia de ChesChequeExpFormat
     * @return instancia
     */
    public static ChesChequeExpFormat getInstance(){
        if(null == singleton){
            singleton = new ChesChequeExpFormat();
        }
        return singleton;
    }

    /** Formateador para las fechas
     */
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ( "dd/MM/yyyy" );
    /** Creates a new instance of ChesChequeExpFormat */
    protected ChesChequeExpFormat () {
    }

    /** Formatea un ChesCheque para exportar
     * @param obj ChesCheque a formatear
     * @param stringBuffer destino de la operacion
     * @param fieldPosition Objeto para llevar un control de los campos
     * @return Cadena representando al ChesCheque
     */
    public StringBuffer format (Object obj, StringBuffer stringBuffer, java.text.FieldPosition fieldPosition) {
	String fechaLimitePago="";

        ChesCheque cheque = (ChesCheque)obj;
        stringBuffer.append ( addPaddingRight( Long.toString (cheque.getNumCuenta ()), CUENTA_LEN ,' ' ) );
        stringBuffer.append ( addPaddingRight( Long.toString (cheque.getNumCheque ()), NUM_CHEQUE_LEN,' ' ) );
        stringBuffer.append ( addPaddingRight( ((null == cheque.getCveBenef ())?"":cheque.getCveBenef ()), CVE_BENEF_LEN,' ' ) );
        stringBuffer.append ( addPaddingLeft ( Long.toString (cheque.getImporte()), IMPORTE_LEN,'0' ) );
        stringBuffer.append ( sdf.format ( cheque.getFechaLibramiento ().getTime () ) );

		fechaLimitePago=sdf.format ( cheque.getFechaLimPago ().getTime () );
		if (fechaLimitePago.equals("31/12/9999")) fechaLimitePago="          ";
		stringBuffer.append (fechaLimitePago);

        stringBuffer.append ( cheque.getStatus () );
        return stringBuffer;
    }

    /** No implementado
     * @param str cadena desde la que se recuperara un ChesCheque
     * @param parsePosition objeto para llevar un control de los caracteres utilizados
     * @return ChesCheque
     */
    public Object parseObject (String str, java.text.ParsePosition parsePosition) {
        return new Object();
    }

    /** Obtiene una cadena representando un ChesCheque
     * @param cheque Cheque a formatear
     * @return Cadena representando el ChesCheque
     */
    public String format( ChesCheque cheque){
        StringBuffer retValue = new StringBuffer( TOTAL_LEN );
        retValue = format( cheque, retValue ,new java.text.FieldPosition( 0 ) );
        return retValue.toString ();
    }

    /** A�ade caracteres de relleno a la izquierda
     * @param str cadena a la que se le a�adiran caracteres de relleno
     * @param length longitud desdeada de la cadena
     * @param padding caracter a utilizar como relleno
     * @return StringBuffer con el resultado de la operacion
     */
    StringBuffer addPaddingLeft( String str, int length, char padding ){
        StringBuffer retValue = new StringBuffer ( length );
        while(retValue.length () + str.length () < length ){
            retValue.append (padding);
        }
        retValue.append ( str );
        return retValue;
    }

    /** A�ade caracteres de relleno a la derecha de la cadena
     * @param str cadena a la que se le desea a�adir caracteres
     * @param length longitud deseada de la la cadena
     * @param padding caracter a utilizar como relleno
     * @return StringBuffer con el resultado de la operacion
     */
    StringBuffer addPaddingRight( String str, int length, char padding ){
        StringBuffer retValue = new StringBuffer ( length );
        retValue.append ( str );
        while(retValue.length () < length ){
            retValue.append (padding);
        }
        return retValue;
    }

}
