/*
 * ChesArchivoImportadoConsFormat.java
 *
 * Created on 15 de enero de 2003, 12:08 PM
 */

package mx.altec.enlace.ches;
/** Format para obtener ChesArchivoImportado a partir de una consulta
 * @author Rafael Martinez Montiel
 */
public class ChesArchivoImportadoConsFormat extends java.text.Format {


    /** formateador para las fechas de archivo
     */
    public static java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ( "dd/MM/yyyy" );

    /** instancia de la clase
     */
    private static ChesArchivoImportadoConsFormat singleton =  null;

    /** Obtiene una instancia del ChesArchivoImportadoConsFormat
     * @return instancia de la clase
     */
    public static ChesArchivoImportadoConsFormat getInstance (){
        if(null == singleton){
            singleton = new ChesArchivoImportadoConsFormat ();
        }
        return singleton;
    }
    /** Creates a new instance of ChesArchivoImportadoConsFormat */
    protected ChesArchivoImportadoConsFormat () {
    }

    /** Obtiene un ChesArchivoImportado a partir de una cadena
     * @param str Cadena a partir de la cual se intentara recuperar el ChesArchivoImportado
     * @param parsePosition Objeto para llevar el control de el parsing.
     * si ocurre algun error se define el errorIndex y el index no se modifica
     * si se reconoce un ChesArchivoImportado se define index como el ultimo caracter leido
     * @return ChesArchivoImportado, null en caso de error
     */
    public Object parseObject (String str, java.text.ParsePosition parsePosition) {
        ChesArchivoImportado retValue = new ChesArchivoImportado ();
        java.util.StringTokenizer tok =
        new java.util.StringTokenizer ( str.substring ( parsePosition.getIndex () ), "|" );
        if( 6 > tok.countTokens ()){
            parsePosition.setErrorIndex ( 0 );
            return null;
        }
        int index = parsePosition.getIndex ();



        try{
            java.util.GregorianCalendar cal = new java.util.GregorianCalendar ();
            cal.setTime ( sdf.parse ( tok.nextToken () ) );
            retValue.setFechaEnvio (  cal );
        }catch(java.text.ParseException ex){
            parsePosition.setErrorIndex ( index );
            return null;
        }
        index = str.indexOf ( '|' ,index  +1);



        retValue.setFileName ( tok.nextToken () );
        index = str.indexOf ( '|' ,index  +1);
        try{
            retValue.setNumSecuencia ( Long.parseLong ( tok.nextToken () ) );
        }catch(NumberFormatException ex){
            parsePosition.setErrorIndex ( index );
            return null;
        }
        index = str.indexOf ('|',index  +1 );



        try{
            retValue.setNumRegistros ( Long.parseLong ( tok.nextToken () ) );
        }catch(NumberFormatException ex){
            parsePosition.setErrorIndex ( index );
            return null;
        }
        index = str.indexOf ('|',index +1);



        try{
            java.math.BigDecimal bigDec = new java.math.BigDecimal ( tok.nextToken ().trim () );
            bigDec = bigDec.multiply ( new java.math.BigDecimal ( 100 ) );
            //            System.out.println("bigDec=" + bigDec.toString ());
            retValue.setImporteTotal ( bigDec.toBigInteger () );
            //            System.out.println("bigInt=" + retValue.getImporteTotal ().toString ());
        }catch(NumberFormatException ex){
            parsePosition.setErrorIndex ( index );
            return null;
        }
        index = str.indexOf ('|',index +1);



        retValue.setStatus ( tok.nextToken () );
        index += retValue.getStatus ().length ();

        parsePosition.setIndex ( index );



        return retValue;
    }

    /** No implementado en este momento
     * @param obj Objeto que se intentara formatear
     * @param stringBuffer StringBuffer que se ocupara como destino
     * @param fieldPosition indicador para los campos
     * @return stringBuffer con el resultado
     */
    public StringBuffer format (Object obj, StringBuffer stringBuffer, java.text.FieldPosition fieldPosition) {
        return stringBuffer;
    }

    /** Obtiene un ChesArchivoImportado desde una cadena de  archivo de consulta.
     * @param str String desde el cual se intentara reconocer el ChesArchivoImportado
     * @throws ParseException En caso de no poder recuperar un ChesArchivoImportado
     * @return ChesArchivoImportado representando la cadena
     */
    public ChesArchivoImportado parse ( String str)
    throws java.text.ParseException{
        java.text.ParsePosition  parsePosition = new java.text.ParsePosition (0);
        ChesArchivoImportado retValue = (ChesArchivoImportado) parseObject ( str,parsePosition);
        if(null == retValue){
            throw new java.text.ParseException ("No se reconoce el formato del Archivo, " + str,parsePosition.getErrorIndex () );
        }
        return retValue;
    }

}
