/*
 * ChesCheque.java
 *
 * Created on 3 de enero de 2003, 10:19 AM
 */

package mx.altec.enlace.ches;

import java.util.GregorianCalendar;
import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import mx.altec.enlace.utilerias.EIGlobal;


/** Es la representacion de un Cheque de seguridad
 * @author Rafael Martinez Montiel
 */
public class ChesCheque implements java.io.Serializable {

    /** Longitud del Numero de cuenta en el layout
     */
    public static final int NUM_CUENTA_LEN = 16;
    /** Longitud del Numero de cheque en el layout
     * was 8
     */
    public static final int NUM_CHEQUE_LEN = 7;
    /** Longitud del importe en el layout
     */
    public static final int IMPORTE_LEN = 16;
    /** Longitud de la fecha limite de pago en el layout
     */
    public static final int FECHA_LIM_PAGO_LEN = 10;
    /** Longitud de la fecha de libramiento
     */
    public static final int FECHA_LIBRAMIENTO_LEN = 10;
    /** Longitud de la clave de beneficiario
     */
    public static final int CVE_BENEF_LEN = 13;
    /** Longidud del nombre del beneficiario
     */
    public static final int NOMBRE_BENEF_LEN = 60;
    /** Longitud total de la linea
     */
    public static final int LEN_TOTAL = NUM_CUENTA_LEN
    + NUM_CHEQUE_LEN
    + CVE_BENEF_LEN
    + NOMBRE_BENEF_LEN
    + IMPORTE_LEN
    + FECHA_LIBRAMIENTO_LEN
    + FECHA_LIM_PAGO_LEN;


    /** Instancia unica de ChesCheque
     */
    public static ChesCheque singleton = null;

    /** SimpleDateFormat para la representacion de fechas
     */
    public static java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");

    /** Obtiene una instancia de ChesCheque
     * @return instancia
     */
    public static ChesCheque getInstance (){
        if(null == singleton){
            singleton = new ChesCheque ();
            sdf.setLenient (false);
        }
        return singleton;
    }

    /** Holds value of property numCuenta. */
    private long numCuenta;

    /** Holds value of property numCheque. */
    private long numCheque;

    /** Holds value of property importe. */
    private long importe;

    /** Holds value of property fechaLimPago. */
    private GregorianCalendar fechaLimPago;

    /** Holds value of property nomBenef. */
    private String nomBenef;

    /** Holds value of property cveBenef. */
    private String cveBenef;

    /** Holds value of property fechaLibramiento. */
    private GregorianCalendar fechaLibramiento;

    /** Holds value of property status. */
    private String status;

    /** Holds value of property fechaPago. */
    private GregorianCalendar fechaPago;

    /** Holds value of property fechaCancelacion. */
    private GregorianCalendar fechaCancelacion;

    /** Creates a new instance of ChesCheque */
    public ChesCheque () {
    }

    /** Formatea un ChesCheque para importacion
     * @deprecated se envia el buffer original, no se reconstruye el archivo
     * @param cheque Cheque a formatear
     * @return Cadena representando al cheque
     */
    public static String format ( ChesCheque cheque ){
        StringBuffer retValue = new StringBuffer ();
        retValue = format (cheque, retValue, new java.text.FieldPosition (0) );
        return retValue.toString ();
    }
    /** Obtiene una cadena con formato representando a un ChesCheque
     * @deprecated Se envia el buffer original, no se reconstrulle el archivo
     * @param obj Cheque a formatear
     * @param stringBuffer destino de la operacion
     * @param fieldPosition Objeto para llevar un control de los campos
     * @return StringBuffer representando el ChesCheque
     */
    public static StringBuffer format (Object obj, StringBuffer stringBuffer, java.text.FieldPosition fieldPosition) {
        ChesCheque cheque = null;
        try{
            cheque = (ChesCheque )obj;
        }catch(ClassCastException ex){
            throw new IllegalArgumentException ("No se puede dar formato al objeto:" + obj.toString ());
        }
        if(null == cheque){
            throw new IllegalArgumentException ("No se puede dar formato al objeto:" + obj.toString ());
        }
        //System.out.println (cheque.toString ());
        StringBuffer retValue = new StringBuffer ();
        retValue.append (addPaddingLeft (Long.toString (cheque.numCuenta),'0',NUM_CUENTA_LEN) );
        retValue.append (addPaddingLeft ( Long.toString (cheque.numCheque),'0',NUM_CHEQUE_LEN) );
        retValue.append ( addPaddingLeft ( cheque.cveBenef,'0',CVE_BENEF_LEN ) );
        retValue.append ( addPadding ( cheque.nomBenef,' ',NOMBRE_BENEF_LEN ) );
        retValue.append (addPaddingLeft (Long.toString (cheque.importe),'0',IMPORTE_LEN) );
        retValue.append ( sdf.format ( cheque.fechaLibramiento.getTime () ) );
        retValue.append ( sdf.format ( cheque.fechaLimPago.getTime () ) );

        stringBuffer.append ( retValue );
        return stringBuffer;
    }

    /** Obtiene un ChesCheque desde una cadena
     * @param str Cadena desde la cual se reconocera un ChesCheque
     * @param parsePosition Objeto para llevar un control de los caracteres utilizados.
     * Si se reconoceun Ches Cheque se define index como el ultimo caracter leido, de lo contrario se define errorIndex
     * @return ChesCheque representando la cadena, null si ocurre algun error
     */
    public Object parseObject (String str, java.text.ParsePosition parsePosition){
        ChesCheque retValue = new ChesCheque ();
        //Obtener el numero de cuenta
        int index = parsePosition.getIndex ();
        //        try{
        //            retValue.setNumCuenta (str.substring (index,index + NUM_CUENTA_LEN ) );
        //        }catch(IndexOutOfBoundsException ex){
        //            parsePosition.setErrorIndex ( index  );
        //            return null;
        //        }
        index += NUM_CUENTA_LEN;

        //obtener el numero de cheque
        try{
            retValue.setNumCheque (Long.parseLong (str.substring (index,index + NUM_CHEQUE_LEN ).trim () ));

        }catch(IndexOutOfBoundsException ex){
            parsePosition.setErrorIndex ( index  );
            return null;
        }catch(NumberFormatException ex){
            parsePosition.setErrorIndex ( index );
            return null;
        }
        index += NUM_CHEQUE_LEN;

        //obtener el importe
        try{
            long ln = Long.parseLong (str.substring (index,index + IMPORTE_LEN ).trim () );
            //            System.out.println("ln="+ln);
            retValue.setImporte (ln);
        }catch(IndexOutOfBoundsException ex){
            parsePosition.setErrorIndex ( index  );
            return null;
        }catch(NumberFormatException ex){
            parsePosition.setErrorIndex ( index );
            return null;
        }
        index += IMPORTE_LEN;
        //obtener la fecha l�mite de pago
        try{
            GregorianCalendar cal = new GregorianCalendar ();
            cal.setTime (sdf.parse (str.substring (index,index +  FECHA_LIM_PAGO_LEN)) );
            retValue.setFechaLimPago ( cal );
        }catch(IndexOutOfBoundsException ex){
            parsePosition.setErrorIndex ( index  );
            return null;
        }catch(ParseException ex){
            parsePosition.setErrorIndex ( index );
            return null;
        }
        index += FECHA_LIM_PAGO_LEN;

        parsePosition.setIndex (index);
        return retValue;
    }

    /** Obtiene un ChesCheque a partir de una cadena
     * @param str cadena a partir de la cual se generara el ChesCheque
     * @throws ParseException En caso de existir algun error
     * @return ChesCheque
     */
    public ChesCheque parse (String str) throws ParseException{
        java.text.ParsePosition parsePosition = new java.text.ParsePosition (0);
        ChesCheque retValue = (ChesCheque) parseObject ( str, parsePosition );
        if(null == retValue ){
            ////            System.out.println("errorIndex=" + parsePosition.getErrorIndex ());
            StringBuffer msg = new StringBuffer ("Error al recuperar el cheque, ");
            msg.append (" posicion=" + parsePosition.getErrorIndex () );
            msg.append (" Cadena=\'" + str + "'");
            throw new ParseException (msg.toString (),parsePosition.getErrorIndex () );
        }
        return retValue;
    }

    /** Getter for property numCuenta.
     * @return Value of property numCuenta.
     */
    public long getNumCuenta () {
        return this.numCuenta;
    }

    /** Setter for property numCuenta.
     * @param numCuenta New value of property numCuenta.
     */
    public void setNumCuenta (long numCuenta) {
        this.numCuenta = numCuenta;
    }

    /** Getter for property numCheque.
     * @return Value of property numCheque.
     */
    public long getNumCheque () {
        return this.numCheque;
    }

    /** Setter for property numCheque.
     * @param numCheque New value of property numCheque.
     */
    public void setNumCheque (long numCheque) {
        this.numCheque = numCheque;
    }

    /** Getter for property importe.
     * @return Value of property importe.
     */
    public long getImporte () {
        return this.importe;
    }

    /** Setter for property importe.
     * @param importe New value of property importe.
     */
    public void setImporte (long importe) {
        this.importe = importe;
    }

    /** Getter for property fechaLimPago.
     * @return Value of property fechaLimPago.
     */
    public GregorianCalendar getFechaLimPago () {
        return this.fechaLimPago;
    }

    /** Setter for property fechaLimPago.
     * @param fechaLimPago New value of property fechaLimPago.
     */
    public void setFechaLimPago (GregorianCalendar fechaLimPago) {
        this.fechaLimPago = fechaLimPago;
    }

    /** Obtiene una representacion en String de este ChesCheque
     * @return Cadena representando este ChesCheque
     */
    public String toString (){
        StringBuffer retValue = new StringBuffer (this.getClass ().getName () + "[");
        retValue.append ("numCuenta=" + numCuenta + ",");
        retValue.append ("numCheque=" + numCheque + ",");
        //        retValue.append ("cveBenef=" + cveBenef + ",");
        //        retValue.append ("nombBenef=" + nomBenef + ",");
        long integerPart = importe / 100;
        long decPart =importe % 100;
        retValue.append ("importe=" + integerPart + "." + decPart + ",");

        retValue.append ("fechaLibramiento=" + sdf.format (fechaLibramiento.getTime () )+ ",");
        retValue.append ("fechaLimPago=" + sdf.format (fechaLimPago.getTime ()));
        retValue.append (",status=" + this.status  );
        return retValue.toString () + "]";
    }

    /** a�ade caracteres de relleno a la derecha de la cadena
     * @param string cadena a la cual se intentara a�adir caracteres de relleno
     * @param padding caracter a utilizar como relleno
     * @param length longitud deseada de la cadena
     * @return StringBuffer con el resultado de la operacion
     */
    static StringBuffer addPadding ( String string, char padding, int length){
        StringBuffer retValue = new StringBuffer ( length );
        retValue.append (string);
        while(retValue.length () < length ){
            retValue.append (padding);
        }
        return retValue;
    }

    /** A�ade caracteres a la izquierda
     * @param string cadena a la cual se le a�adiran caracteres de relleno
     * @param padding caracter a utilizar como relleno
     * @param length longitud desdeada de la cadena
     * @return StringBuffer con el resultado de la operacion
     */
    static StringBuffer addPaddingLeft ( String string, char padding, int length){
        StringBuffer retValue = new StringBuffer ( length );
        while(retValue.length () + string.length () < length ){
            retValue.append (padding);
        }
        retValue.append (string);

        return retValue;
    }

    /** Getter for property nomBenef.
     * @return Value of property nomBenef.
     */
    public String getNomBenef () {
        return this.nomBenef;
    }

    /** Setter for property nomBenef.
     * @param nomBenef New value of property nomBenef.
     */
    public void setNomBenef (String nomBenef) {
        this.nomBenef = nomBenef;
    }

    /** Getter for property cveBenef.
     * @return Value of property cveBenef.
     */
    public String getCveBenef () {
        return this.cveBenef;
    }

    /** Setter for property cveBenef.
     * @param cveBenef New value of property cveBenef.
     */
    public void setCveBenef (String cveBenef) {
        this.cveBenef = cveBenef;
    }

    /** Getter for property fechaLibramiento.
     * @return Value of property fechaLibramiento.
     */
    public GregorianCalendar getFechaLibramiento () {
        return this.fechaLibramiento;
    }

    /** Setter for property fechaLibramiento.
     * @param fechaLibramiento New value of property fechaLibramiento.
     */
    public void setFechaLibramiento (GregorianCalendar fechaLibramiento) {
        this.fechaLibramiento = fechaLibramiento;
    }

    /** Obtiene un ChesCheque desde una cadena
     * @param str cadena desde la que se intentara reconocer un ches cheque
     * @param diasInhabiles Lista con los dias inhabiles para hacer la validacion de fechas
     * @throws ChesValidacionException En case de existir al menos un error de validacion
     * @return ChesCheque representado en la cadena
     */
    public static ChesCheque parseChesCheque ( String str,java.util.List diasInhabiles)
    throws ChesValidacionException{
        ChesCheque retValue = new ChesCheque ();
        java.util.List errors = new java.util.LinkedList ();
        if(str.length () != LEN_TOTAL){
            errors.add ("Longitud no coincide con layout, se obtuvo "
            + String.valueOf ( str.length ()) + " se espera " + String.valueOf ( LEN_TOTAL) );
            if( str.length () < LEN_TOTAL ){
                throw new ChesValidacionException ("Error en la linea ", errors );
            }
        }

        // rellenar los campos
        try{
            retValue.setNumCuenta ( Long.parseLong (str.substring (0,NUM_CUENTA_LEN).trim () ) );
        }catch(NumberFormatException ex){
            errors.add ( "Numero de cuenta no es numerico, " + str.substring (0,NUM_CUENTA_LEN) );
        }

        str = str.substring  (NUM_CUENTA_LEN);
        try{
            retValue.setNumCheque ( Long.parseLong (str.substring (0,NUM_CHEQUE_LEN).trim () ) );
        }catch(NumberFormatException ex){
            errors.add ("Numero de cheque no es numerico");
        }
        str = str.substring ( NUM_CHEQUE_LEN );
        retValue.setCveBenef (str.substring (0,CVE_BENEF_LEN) );
        //        if(retValue.getCveBenef ().trim ().equals ( "" )){
        //            errors.add ( "La clave del beneficiario es obligatoria" );
        //        }
        str = str.substring (CVE_BENEF_LEN);
        retValue.setNomBenef (str.substring (0,NOMBRE_BENEF_LEN) );
        //        // COMMENTED OUT por eliminarse el campo nombre de beneficiario, pero se
        //        // continua manteniendo el campo en el layout
        //        if(retValue.getNomBenef ().trim ().equals ("") ){
        //            errors.add ("Nombre de beneficiario es obligatorio" );
        //        }
        //        try{
        //            isValidChar ( retValue.getNomBenef () );
        //        }catch(ChesValidacionException ex){
        //            ChesValidacionException e = new ChesValidacionException ( "Caracteres invalidos en el nombre del beneficiario", ex.parseExceptions () );
        //            errors.add ( e );
        //        }

        str = str.substring (NOMBRE_BENEF_LEN);
        try{
            retValue.setImporte ( Long.parseLong ( str.substring (0,IMPORTE_LEN).trim () ) );
        }catch(NumberFormatException ex){
            errors.add ( "Importe no es numerico" );
        }
        str = str.substring ( IMPORTE_LEN );
        try{
            GregorianCalendar cal = new GregorianCalendar ();
            cal.setTime (sdf.parse ( str.substring (0,FECHA_LIBRAMIENTO_LEN) ) );
            retValue.setFechaLibramiento ( cal );

            //NVS 051206
            if(!esFechaValida(str.substring (0,FECHA_LIBRAMIENTO_LEN))){
                errors.add ( "Fecha libramiento invalida o inexistente" );
            }
            else if( diasInhabiles.contains ( cal.getTime () )){
                errors.add ( "Fecha libramiento es inhabil" );
            }

            GregorianCalendar today = new GregorianCalendar ();
            cal = new GregorianCalendar ( today.get ( today.YEAR), today.get (today.MONTH),today.get (today.DATE));

            if( retValue.getFechaLibramiento ().before ( (cal  ) ) ){
                errors.add ( "Fecha libramiento no puede ser menor a " +  sdf.format ( cal.getTime () ) );
            } else{
                cal.add (GregorianCalendar.MONTH, 3);
                if( retValue.getFechaLibramiento ().after ( cal ) ){
                    errors.add ( "Fecha libramiento no puede ser mayor a " + sdf.format ( cal.getTime () ) );
                }
            }
        }catch(ParseException ex){
            errors.add ( "Fecha libramiento en formato no reconocido" );
        }
        str = str.substring ( FECHA_LIBRAMIENTO_LEN );
        try{
            GregorianCalendar cal = new GregorianCalendar ();
            cal.setTime (sdf.parse ( str.substring (0,FECHA_LIBRAMIENTO_LEN) ) );
            retValue.setFechaLimPago ( cal );

            //NVS 051206
            if(!esFechaValida(str.substring (0,FECHA_LIBRAMIENTO_LEN))){
                errors.add ( "Fecha limite de pago invalida o inexistente" );
            }
            else if( diasInhabiles.contains ( cal.getTime () )){
                errors.add ( "Fecha limite de pago es inhabil" );
            }

            if( retValue.getFechaLimPago ().before (retValue.getFechaLibramiento ())){
                errors.add ( "Fecha limite de pago no puede ser menor a fecha de libramiento " + sdf.format ( retValue.getFechaLibramiento ().getTime () ) );
            }
			/*
			else {
                cal = new GregorianCalendar ();
                cal.add ( GregorianCalendar.MONTH, 6 );
                if( retValue.getFechaLimPago ().after ( cal ) ){
                    //                    errors.add ( "Fecha limite de pago no puede ser menor a " +  sdf.format ( cal.getTime () ) );// Modificacion RMM 20030129 COMMENT OUT
                    errors.add ( "Fecha limite de pago no puede ser mayor a " +  sdf.format ( cal.getTime () ) );// Modificacion RMM 20030129 Correccion a mensaje desplegado
                }
            }*/

        }catch(ParseException ex){
            errors.add ( "Fecha limite de pago en formato no reconocido" );
        }

        if(! errors.isEmpty () ){
            throw new ChesValidacionException ("Errores en la linea ", errors );
        }

        return retValue;
    }

    // NVS 051206
    /** Valida que la fecha que se ha ingresado sea una fecha correcta
     * @return true si la cadena es correcta
     * @param fecha Cadena con la fecha a validar tipo mm/MM/yyyy
     */
	public static boolean esFechaValida(String fecha){
		boolean esValida = false;
		int dia = Integer.parseInt(fecha.substring(0,2));
		int mes = Integer.parseInt(fecha.substring(3,5)) - 1;
		int anio = Integer.parseInt(fecha.substring(6,10));

		Calendar c = Calendar.getInstance();
		c.set(anio, mes, dia);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String sdfs = sdf.format(c.getTime());

		if(fecha.equals(sdfs)) esValida = true;

		EIGlobal.mensajePorTrace("*** ChesCheque.esFechaValida() - fecha = " + fecha + " return " + esValida, EIGlobal.NivelLog.DEBUG);

		return esValida;
	}

    /** Valida que contenga solo caracteres aceptados
     * @return true si la cadena es correcta
     * @param str Cadena a validar
     * @throws ChesValidacionException En caso de existir almenos un caracter invalido */
    public static boolean isValidChar (String str )throws ChesValidacionException {
        String msg_error = "";
        int     i     = 0;
        char     letra = 0;
        boolean isValid = true;
        java.util.List errors = new java.util.LinkedList ();

        for(i=0; i<str.length (); ++i) {
            letra = str.charAt (i);
            //System.out.println ("letra "+letra+" "+i + " value=" + Integer.toString ( letra )  + "  other=" + (char)i);
            if( !( (letra==32) || (letra==45) || (letra>=47 && letra <=57) || (letra>=65 && letra <=90) || (letra>=97 && letra<=122) || (letra==209) || (letra==241) ) ) {
                errors.add ("Caracter invalido en la posicion: " + (i+1) );
                isValid = false;
            }
        }

        if(! errors.isEmpty ()){
            throw new ChesValidacionException ("Caracteres Invalidos" , errors );
        }
        return isValid;

    }

    /** Getter for property status.
     * @return Value of property status.
     */
    public String getStatus () {
        return this.status;
    }

    /** Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus (String status) {
        this.status = status;
    }

    /** Getter for property fechaPago.
     * @return Value of property fechaPago.
     */
    public GregorianCalendar getFechaPago () {
        return this.fechaPago;
    }

    /** Setter for property fechaPago.
     * @param fechaPago New value of property fechaPago.
     */
    public void setFechaPago (GregorianCalendar fechaPago) {
        this.fechaPago = fechaPago;
    }

    /** Getter for property fechaCancelacion.
     * @return Value of property fechaCancelacion.
     */
    public GregorianCalendar getFechaCancelacion () {
        return this.fechaCancelacion;
    }

    /** Setter for property fechaCancelacion.
     * @param fechaCancelacion New value of property fechaCancelacion.
     */
    public void setFechaCancelacion (GregorianCalendar fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

}
