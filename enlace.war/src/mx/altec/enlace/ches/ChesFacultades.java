/*
 * Facultades.java
 *
 * Created on 3 de enero de 2003, 04:58 PM
 */

package mx.altec.enlace.ches;

/** Facultades para el modulo de Chequera de Seguridad
 * @author Rafael Martinez Montiel
 */
public class ChesFacultades {

    /** Facultad de envio de archivos
     */
   public static final  String FAC_ENVIO = "TECHSENVM";
   /** Facultad de envio de archivos
    */
   public static final  String FAC_ARCHIVO = "TECHSCAPM";

}
