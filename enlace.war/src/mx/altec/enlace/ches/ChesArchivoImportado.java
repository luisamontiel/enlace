/*
 * ChesArchivoImportado.java
 *
 * Created on 3 de enero de 2003, 01:19 PM
 */

package mx.altec.enlace.ches;
import java.util.List;

import mx.altec.enlace.utilerias.EIGlobal;


/** Contiene los registros del archivo importado por el usuario.
 * No se representa como un archivo fisico, pero puede ser escrito mediante los metodos internos
 * @author Rafael Martinez Montiel
 */
public class ChesArchivoImportado implements java.io.Serializable {
    /** Nombre de la clase para rapida referencia
     */
    public static final String CLASS_NAME = ChesArchivoImportado.class.getName ();
    /** Formateador para la parte decimal del importe
     *
     */
    public static java.text.DecimalFormat dec = new java.text.DecimalFormat ("00");

    /** Lista con los ChesCheque que se obtuvieron del archivo
     */
    private List cheques = new java.util.LinkedList ();

    /** Es el importe total del archivo
     */
    private java.math.BigInteger importeTotal = java.math.BigInteger.valueOf (0);

    /** instancia de la clase
     */
    private static ChesArchivoImportado singleton = null;

    /** Holds value of property fileName. */
    private String fileName;

    /** Holds value of property numSecuencia. */
    private long numSecuencia;

    /** Holds value of property status. */
    private String status;

    /** Holds value of property fechaEnvio. */
    private java.util.GregorianCalendar fechaEnvio;

    /** Holds value of property bufferOriginal. */
    private String bufferOriginal;

    /** Holds value of property numRegistros. */
    private long numRegistros;

    /** Obteien una instancia unica de ChesArchivoImportado
     * @return Instancia unica de ChesArchivoImportado
     */
    public static ChesArchivoImportado getInstance (){
        if(null == singleton){
            singleton = new ChesArchivoImportado ();
        }
        return singleton;
    }

    /** Creates a new instance of ChesArchivoImportado */
    public ChesArchivoImportado () {
    }


    /** Obtiene un ChesArchivoImportado desde una cadena
     * @return ChesArchivoImportado reflejando el contenido de la cadena pasada como parametro
     * @param diasInhabiles Lista con los dias inhabiles
     * @param str cadena a partir de la cual se intentará realizar el parse
     * @throws ChesValidacionException En caso de existir errores en el archivo
     */
    public static ChesArchivoImportado parse (String str, List diasInhabiles, java.io.File errorFile )
    throws ChesValidacionException, java.io.IOException {
        ChesArchivoImportado retValue = new ChesArchivoImportado ();
        //java.io.BufferedReader reader = new java.io.BufferedReader (new java.io.StringReader (str) );
		java.io.BufferedReader reader = new java.io.BufferedReader (new java.io.FileReader (str) );
        int linea = 0;
        String reg = null;
        //        List errors = new java.util.LinkedList ();
        long errors = 0;
        java.io.BufferedWriter errorWriter = null;
        try{
            while(null != (reg = reader.readLine ())){
                try{
                    ++linea;
                    ChesCheque cheque = ChesCheque.parseChesCheque ( reg , diasInhabiles);
                    //retValue.cheques.add ( cheque );

                    retValue.importeTotal = retValue.importeTotal.add ( java.math.BigInteger.valueOf (cheque.getImporte ()) );
                    ++retValue.numRegistros;
                    //System.out.println("importe =" +  cheque.getImporte() +" importeTotal =" + retValue.importeTotal );
                }catch(ChesValidacionException e){
                    if(null == errorWriter){
                        errorWriter = new java.io.BufferedWriter ( new java.io.FileWriter ( errorFile ) );
                    }
                    errors += e.parseExceptions ().size ();
                    try{
                        errorWriter.write ( "Errores en linea " + linea );
                        errorWriter.newLine ();
                        errorWriter.write ( e.parseExceptionsAsString ("\t") );
                    }catch(java.io.IOException exc){
                        EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al escribir en el archivo de errores, " + exc.getMessage (), EIGlobal.NivelLog.ERROR);
                        throw exc;
                    }
                    //                    ChesValidacionException exception = new ChesValidacionException (e.getMessage () + linea,e.parseExceptions ());
                    //                    errors.add ( exception );
                }
            }
        }catch(java.io.IOException ex){
            mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace ( "Error al leer el archivo importado " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            ex.printStackTrace ();
            retValue = null;
        }finally{
            if( null != errorWriter ){
                try{
                    errorWriter.close ();
                }catch ( java.io.IOException e){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al cerrar el archivo de errores, " +  e.getMessage (), EIGlobal.NivelLog.ERROR);
                }
                errorWriter = null;
            }

        }
        if( 0 < errors ){
            throw new ChesValidacionException ("Error al leer el archivo importado");
        }
        return retValue;
    }

    /** Getter for property cheques.
     * @return Value of property cheques.
     */
    public List getCheques () {
        return this.cheques;
    }

    /** Setter for property cheques.
     * @param cheques New value of property cheques.
     */
    public void setCheques (List cheques) {
        this.cheques = cheques;
    }

    /** representa este objeto como un String
     * @return string representando el objeto
     */
    public String toString (){
        StringBuffer retValue = new StringBuffer (this.getClass ().getName () + "[");
        retValue.append ( "cheques=" + this.cheques);
        retValue.append ( ",importeTotal=" + this.importeTotalAsString () );

        return retValue.toString () + "]";
    }

    /** Escribe el contenido de este ChesArchivoImportado a un BufferedWriter, respetando el layout definido.
     * @param writer Writer que se utilizara para representar el objeto
     * @throws IOException En caso de que ocurra algun error de IO
     */
    public void write (java.io.BufferedWriter writer)
    throws java.io.IOException{
        java.util.ListIterator li = cheques.listIterator ();
        while(li.hasNext ()){
            writer.write ( ChesCheque.format ((ChesCheque) li.next () ) );
            if(li.hasNext ()){
                writer.write ('\n');
            }
        }
    }

    /** Getter for property importeTotal
     * @return importeTotal del ChesArchivoImportado
     */
    public java.math.BigInteger getImporteTotal (){
        return this.importeTotal;
    }

    /** Getter for property fileName.
     * @return Value of property fileName.
     */
    public String getFileName () {
        return this.fileName;
    }

    /** Setter for property fileName.
     * @param fileName New value of property fileName.
     */
    public void setFileName (String fileName) {
        this.fileName = fileName;
    }

    /** Getter for property numSecuencia.
     * @return Value of property numSecuencia.
     */
    public long getNumSecuencia () {
        return this.numSecuencia;
    }

    /** Setter for property numSecuencia.
     * @param numSecuencia New value of property numSecuencia.
     */
    public void setNumSecuencia (long numSecuencia) {
        this.numSecuencia = numSecuencia;
    }

    /** Getter for property status.
     * @return Value of property status.
     */
    public String getStatus () {
        return this.status;
    }

    /** Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus (String status) {
        this.status = status;
    }

    /** Getter for property fechaEnvio.
     * @return Value of property fechaEnvio.
     */
    public java.util.GregorianCalendar getFechaEnvio () {
        return this.fechaEnvio;
    }

    /** Setter for property fechaEnvio.
     * @param fechaEnvio New value of property fechaEnvio.
     */
    public void setFechaEnvio (java.util.GregorianCalendar fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    /** Obteine el importe total como string formateado
     * @return String formateado
     */
    public String importeTotalAsString (){
        StringBuffer retValue = new StringBuffer ();
        String sImporte = this.importeTotal.toString ();
        if(sImporte.length () < 3){
            retValue.append ("0.");
            try{
                retValue.append ( dec.format ( Integer.parseInt (sImporte) ) );
            }catch(NumberFormatException ex){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al reconocer el numero," + ex.getMessage (), EIGlobal.NivelLog.INFO);
                ex.printStackTrace ();
            }
        }else {
            String parteEntera = sImporte.substring (0,sImporte.length () - 2 );
            int dif = parteEntera.length () % 3;
            StringBuffer sbParteEntera = new StringBuffer ();
            for(int c = 0;c<parteEntera.length ();++c,--dif){
                if ( (0 == dif) ) {
                    if (0 != c) sbParteEntera.append ( ',');
                    dif +=3;
                }
                sbParteEntera.append ( parteEntera.charAt (c) );
            }
            retValue.append ( sbParteEntera );

            retValue.append ('.');
            retValue.append ( dec.format ( Integer.parseInt (sImporte.substring ( sImporte.length () - 2 ) ) ) );
        }
        return retValue.toString ();
    }

    /** Getter for property bufferOriginal.
     * @return Value of property bufferOriginal.
     */
    public String getBufferOriginal () {
        return this.bufferOriginal;
    }

    /** Setter for property bufferOriginal.
     * @param bufferOriginal New value of property bufferOriginal.
     */
    public void setBufferOriginal (String bufferOriginal) {
        this.bufferOriginal = bufferOriginal;
    }

    /** Getter for property numRegistros.
     * @return Value of property numRegistros.
     */
    public long getNumRegistros () {
        return this.numRegistros;
    }

    /** Setter for property numRegistros.
     * @param numRegistros New value of property numRegistros.
     */
    public void setNumRegistros (long numRegistros) {
        this.numRegistros = numRegistros;
    }

    /** Setter for property importeTotal.
     * @param importeTotal New value of property importeTotal.
     */
    public void setImporteTotal (java.math.BigInteger importeTotal) {
        this.importeTotal = importeTotal;
    }

}