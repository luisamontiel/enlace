/*
 * ChesResultadoCancelacion.java
 *
 * Created on 14 de enero de 2003, 10:44 AM
 */

package mx.altec.enlace.ches;

/** Contiene el resultado de la cancelacion de un cheque
 * @author Rafael Martinez Montiel
 */
public class ChesResultadoCancelacion implements java.io.Serializable {

    /** Holds value of property numCheque. */
    private String numCheque;

    /** Holds value of property numCuenta. */
    private String numCuenta;

    /** Holds value of property descripcion. */
    private String descripcion;

    /** Creates a new instance of ChesResultadoCancelacion */
    public ChesResultadoCancelacion () {
    }

    /** Getter for property numCheque.
     * @return Value of property numCheque.
     */
    public String getNumCheque () {
        return this.numCheque;
    }

    /** Setter for property numCheque.
     * @param numCheque New value of property numCheque.
     */
    public void setNumCheque (String numCheque) {
        this.numCheque = numCheque;
    }

    /** Getter for property numCuenta.
     * @return Value of property numCuenta.
     */
    public String getNumCuenta () {
        return this.numCuenta;
    }

    /** Setter for property numCuenta.
     * @param numCuenta New value of property numCuenta.
     */
    public void setNumCuenta (String numCuenta) {
        this.numCuenta = numCuenta;
    }

    /** Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public String getDescripcion () {
        return this.descripcion;
    }

    /** Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion (String descripcion) {
        this.descripcion = descripcion;
    }

}
