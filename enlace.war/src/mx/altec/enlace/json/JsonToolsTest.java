package mx.altec.enlace.json;

import mx.altec.enlace.json.parser.JSONParserException;
import mx.altec.enlace.utilerias.EIGlobal;
/**
 * Clase que permite probar el mensaje de envio a tuxedo
 * @author gguerrero
 *
 */
public class JsonToolsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * Se crea el mensaje tuxedo
		 */
		MsgTuxedo msgRequest = new MsgTuxedo();
		/**
		 * Se indica el servicio a consultar
		 */
		msgRequest.setSvc("TUBO");
		/**
		 * Se agregan los parametros de entrada
		 */
		msgRequest.addFldEnv("12345", "DATO1");
		msgRequest.addFldEnv("31255", "DATO2");
		msgRequest.addFldEnv("99999", new Integer(500));
		/**
		 * Se indican los parametros que se esperan recibir
		 */
		msgRequest.addFldRec("11111", "");
		msgRequest.addFldRec("77777", "");
		/**
		 * Imprime la cadena JSON que será enviada a Tuxedo a partir de la configuración anterior
		 */
		EIGlobal.mensajePorTrace(msgRequest.toString(), EIGlobal.NivelLog.INFO);
		
		/**
		 * Cadena dummie de respuesta de Tuxedo
		 */
		String strResponse="{\"msgRec\":{\"fldsRec\":{\"40963\": \"REFE0000\", \"8197\": 247}}}";
		
		/**
		 * Se declara la variable que contendra los valores de respuesta de Tuxedo
		 */
		MsgTuxedo msgResponse = null;
		
		try {
			/**
			 * Se parsea la cadena regresada por tuxedo en un bean de mensaje que permite obtener los valores de respuesta
			 */
			msgResponse = new MsgTuxedo(strResponse);
		} catch (JSONParserException e) {
			// En caso de ocurrir un error en el formato de la cadena regresada por tuxedo se lanza JSONParserException
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		
		
		try {
			/**
			 * Se obtienen los valores de respuesta como un tipo de dato en particular
			 */
			EIGlobal.mensajePorTrace(msgResponse.getFldRecAsInteger("8197").toString(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(msgResponse.getFldRecAsDouble("40963").toString(), EIGlobal.NivelLog.INFO);
		} catch (MsgTuxedoException e) {
//			// En caso de que algun parametro no exista en la respuesta tuxedo o no sea posible convertir al tipo seleccionadao se lanza MsgTuxedoException
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		
		
		
	}

}
