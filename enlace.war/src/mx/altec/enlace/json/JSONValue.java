/*
 * $Id: JSONValue.java,v 1.1 2006/04/15 14:37:04 platform Exp $
 * Created on 2006-4-15
 */
package mx.altec.enlace.json;

import java.io.Reader;
import java.io.StringReader;

import mx.altec.enlace.json.parser.JSONParser;
import mx.altec.enlace.json.parser.JSONParserException;



/**
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONValue {
	/**
	 * parse into java object from input source.
	 * @param in
	 * @return instance of : JSONObject,JSONArray,String,Boolean,Long,Double or null
	 */
	public static Object parse(Reader in) throws JSONParserException{
		JSONParser parser=new JSONParser();
		return parser.parse(in);

	}

	public static Object parse(String s) throws JSONParserException{
		StringReader in=new StringReader(s);
		return parse(in);
	}
	
	protected void finalize() throws Throwable{
        super.finalize();
	}

}