package mx.altec.enlace.json.parser;

import mx.altec.enlace.utilerias.Global;

public class JSONParserException extends Exception {

	private static final long serialVersionUID = 436526501740545231L;
	public JSONParserException() {
		super(Global.JSON_TUX_PARSER_EXCEPTION);
	}
	public JSONParserException(String message) {
		super(Global.JSON_TUX_PARSER_EXCEPTION + " detalle : " + message);
	}
	public JSONParserException(Throwable cause) {
		super(cause);
	}
	public JSONParserException(String message, Throwable cause) {
		super(Global.JSON_TUX_PARSER_EXCEPTION + " detalle : " + message, cause);
	}

}
