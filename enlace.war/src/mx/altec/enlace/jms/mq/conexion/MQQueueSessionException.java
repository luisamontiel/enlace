/**
 * 
 */
package mx.altec.enlace.jms.mq.conexion;

/**
 * @author rgutierrez
 */
public class MQQueueSessionException extends Exception {
	private static final long serialVersionUID = 6858218594743141419L;
	/**
	 * Constructor por default.
	 *
	 */
	public MQQueueSessionException() {
		
	}
	/**
	 * Constructor heredado.
	 * @param message
	 */
	public MQQueueSessionException(String message) {
		super(message);
	}
	/**
	 * Constructor heredado 
	 * @param e
	 */
	public MQQueueSessionException(Exception exception)
	{
		super(exception);
	}
	/**
	 * Constructor heredado.
	 * @param cause
	 */
	public MQQueueSessionException(Throwable cause) {
		super(cause);
	}
	/**
	 * Constructor heredado.
	 * @param message
	 * @param cause
	 */
	public MQQueueSessionException(String message, Throwable cause) {
		super(message, cause);
	}
}
