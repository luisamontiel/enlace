/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * GeneraComprobanteResponse.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO de respuesta para el tranporte de informacion de la generacion de comprobante con sello digital.
 *
 * @author FSW Everis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generaComprobanteResponse", propOrder = {
    "_return"
})
public class GeneraComprobanteResponse {

    /** La variable refund. */
    @XmlElement(name = "return")
    protected BeanRespuestaGeneracion refund;

    /**
     * Obtiene el valor de la variable refund.
     *
     * @return el refund
     */
    public BeanRespuestaGeneracion getRefund() {
        return refund;
    }

    /**
     * Coloca el valor de refund.
     *
     * @param value es el nuevo valor de refund
     */
    public void setRefund(BeanRespuestaGeneracion value) {
        this.refund = value;
    }

}
