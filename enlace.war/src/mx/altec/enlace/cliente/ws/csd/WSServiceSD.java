/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * WSServiceSD.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import java.util.List;

import javax.xml.ws.soap.SOAPFaultException;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase de utileria para crear instancea del WS Service de sellos digitales
 * 
 * @author FSW Everis
 * 
 */
public final class WSServiceSD {

    /**
     * Variable estatica para instancea de la clase
     */
    private static WSServiceSD wsService = new WSServiceSD();

    /**
     * Variable estatica para clase de servicio del WS
     */
    private static ComprobanteSelloDigitalWSService service;

    /**
     * Variable estatica para interface del WS
     */
    private static ComprobanteSelloDigitalWSImpl wsImpl;

    /**
     * Constructor privado
     */
    private WSServiceSD() {

    }

    
    /**
     * Crea untancea de la clase, patron singleton
     * 
     * @return intancea de la clase
     */
     public static WSServiceSD getInstance() {
		EIGlobal.mensajePorTrace("WSServiceSD url|" + ConstantesSD.URL_WS + "|", EIGlobal.NivelLog.INFO);
        service = new ComprobanteSelloDigitalWSService();
        wsImpl = service.getComprobanteSelloDigitalWSImplPort();
        return wsService;
    }

    /**
     * Llamado a metodo que valida informacion para la generacion de comprobante
     * 
     * @param params mapa con parametros de WS
     * @return Objeto con respuesta del llamado al WS
     */
    public BeanRespuestaValidacion llamadoWSValidacion(String [] params) {
        return wsImpl.validaComprobante(
            params[ConstantesSD.PARAM_WS_VALIDACION_LIN_CAMP], 
            Double.valueOf(params[ConstantesSD.PARAM_WS_VALIDACION_IMPORTE]), 
            params[ConstantesSD.PARAM_WS_VALIDACION_REFERENCIA], 
            params[ConstantesSD.PARAM_WS_VALIDACION_FCH], 
            Integer.valueOf(params[ConstantesSD.PARAM_WS_VALIDACION_ID_CANAL]), 
            params[ConstantesSD.PARAM_WS_VALIDACION_NUM_CTA]);
    }
    
    /**
     * Llamado a metodo que genera comprobante con sello digital
     * 
     * @param params mapa con parametros de WS
     * @param camposAdic campos adicionales
     * @return Objeto con respuesta del llamado al WS
     */
    public BeanRespuestaGeneracion llamadoWSGeneracion(String [] params, String camposAdic) {
        return wsImpl.generaComprobante(
            params[ConstantesSD.PARAM_WS_VALIDACION_LIN_CAMP], 
            Double.valueOf(params[ConstantesSD.PARAM_WS_VALIDACION_IMPORTE]), 
            params[ConstantesSD.PARAM_WS_VALIDACION_REFERENCIA], 
            params[ConstantesSD.PARAM_WS_VALIDACION_FCH], 
            Integer.valueOf(params[ConstantesSD.PARAM_WS_VALIDACION_ID_CANAL]), 
            params[ConstantesSD.PARAM_WS_VALIDACION_NUM_CTA], 
            params[ConstantesSD.PARAM_WS_VALIDACION_NOMBRE],
            params[ConstantesSD.PARAM_WS_VALIDACION_DOMICILIO],
            params[ConstantesSD.PARAM_WS_VALIDACION_COLONIA],
            params[ConstantesSD.PARAM_WS_VALIDACION_C_P],
            params[ConstantesSD.PARAM_WS_VALIDACION_DELEGACION],
            params[ConstantesSD.PARAM_WS_VALIDACION_ESTADO],
            camposAdic);
    }
    
    /**
     * Llamado a metodo que genera guarda las operaciones masivas
     * 
     * @param movimientos lista con objetos de movimientos
     * @return Objeto con respuesta del llamado al WS
     */
    public Respuesta llamadoWSMasivo(List<BeanDatosOperMasiva> movimientos) {
    	Respuesta r;
    	try{
    		r = wsImpl.registraMovimiento(movimientos);
    	}catch (SOAPFaultException ex){
    		r = new Respuesta();
    		
    		r.setCodError(ex.getClass().getName());
    		r.setMsgError(ex.getMessage());
    	}
    	return r;
    }
}
