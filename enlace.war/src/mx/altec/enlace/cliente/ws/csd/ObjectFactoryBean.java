/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * ObjectFactoryBean.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * Clase de fabrica para la generacion de instanceas de objetos del cliente del WS Sellos digitales
 * FSW Everis
 */
@XmlRegistry
public class ObjectFactoryBean {
    
    /**
     * Crea una instancea del objeto ValidaComprobante
     *
     * @return nueva instancea de ValidaComprobante
     */
    public ValidaComprobante createValidaComprobante() {
        return new ValidaComprobante();
    }

    /**
     * Crea una instancea del objeto RegistraMovimientoResponse 
     *
     * @return nueva instancea de RegistraMovimientoResponse
     */
    public RegistraMovimientoResponse createRegistraMovimientoResponse() {
        return new RegistraMovimientoResponse();
    }

    /**
     * Crea una instancea del objeto RegistraMovimiento 
     *
     * @return nueva instancea de RegistraMovimiento
     */
    public RegistraMovimiento createRegistraMovimiento() {
        return new RegistraMovimiento();
    }

    /**
     * Crea una instancea del objeto GeneraComprobante 
     *
     * @return nueva instancea de GeneraComprobante
     */
    public GeneraComprobante createGeneraComprobante() {
        return new GeneraComprobante();
    }

    /**
     * Crea una instancea del objeto GeneraComprobanteResponse
     *
     * @return nueva instancea de GeneraComprobanteResponse
     */
    public GeneraComprobanteResponse createGeneraComprobanteResponse() {
        return new GeneraComprobanteResponse();
    }

    /**
     * Crea una instancea del objeto ValidaComprobanteResponse 
     *
     * @return nueva instancea de ValidaComprobanteResponse
     */
    public ValidaComprobanteResponse createValidaComprobanteResponse() {
        return new ValidaComprobanteResponse();
    }

    /**
     * Crea una instancea del objeto BeanRespuestaGeneracion 
     *
     * @return nueva instancea de BeanRespuestaGeneracion
     */
    public BeanRespuestaGeneracion createBeanRespuestaGeneracion() {
        return new BeanRespuestaGeneracion();
    }

    /**
     * Crea una instancea del objeto BeanRespuestaValidacion
     *
     * @return nueva instancea de BeanRespuestaValidacion
     */
    public BeanRespuestaValidacion createBeanRespuestaValidacion() {
        return new BeanRespuestaValidacion();
    }

    /**
     * Crea una instancea del objeto BeanDatosOperMasiva
     *
     * @return nueva instancea de BeanDatosOperMasiva
     */
    public BeanDatosOperMasiva createBeanDatosOperMasiva() {
        return new BeanDatosOperMasiva();
    }

    /**
     * Crea una instancea del objeto Response
     *
     * @return nueva instancea de Respuesta
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

}
