/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * ConstantesSD.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import java.util.Locale;

import mx.altec.enlace.utilerias.Global;

/**
 * Clase de para almacenar constantes para las operaciones de sellos digitales
 * @author FSW Everis
 *
 */
public final class ConstantesSD {

    /** Constante para almacenar una cadena vacia */
    public static final String CADENA_VACIA = "";

    /** Constante para almacenar una cadena vacia */
    public static final String CLASS = "class";
    
    /** Constante para almacenar el atributo donde se escribe el contenido html para la salida del popup */
    public static final String HTML_SD = "htmlSD";
    
    /** Constante para almacenar el modulo validacion */
    public static final String MODULO_VSD = "VSD";
    
    /** Constante para almacenar el modulo comprobante */
    public static final String MODULO_CSD = "CSD";
    
    /** Constante para almacenar pattern de split doble pipe */
    public static final String SPLIT_DOBLE_PIPE = "[|][|]";
    
    /** Constante para almacenar pattern de split simple pipe */
    public static final String SPLIT_SIMPLE_PIPE = "[|]";
    
    /** Constante para almacenar simple pipe */
    public static final String SIMPLE_PIPE = "|";
    
    /** Constante para almacenar doble pipe */
    public static final String DOBLE_PIPE = "||";
    
    /**
     * Constante que contiene el key del parametro linea de captura para el metodo valida Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_LIN_CAMP = 0;
    
    /**
     * Constante que contiene el key del parametro importe para el metodo valida Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_IMPORTE = 1;
    
    /**
     * Constante que contiene el key del parametro fecha de pago para el metodo valida Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_FCH = 3;
    
    /**
     * Constante que contiene el key del parametro referencia para el metodo valida Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_REFERENCIA = 2;
    
    /**
     * Constante que contiene el key del parametro id de canal para el metodo valida Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_ID_CANAL = 4;
    
    /**
     * Constante que contiene el key del parametro numero de cuenta para el metodo valida Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_NUM_CTA = 5;
    
    /**
     * Constante que contiene el key del parametro nombre para el metodo genera Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_NOMBRE = 7;
    
    /**
     * Constante que contiene el key del parametro domicilio para el metodo genera Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_DOMICILIO = 8;
    
    /**
     * Constante que contiene el key del parametro colonia para el metodo genera Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_COLONIA = 9;
    
    /**
     * Constante que contiene el key del parametro codigo postal para el metodo genera Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_C_P = 10;
    
    /**
     * Constante que contiene el key del parametro delegacion para el metodo genera Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_DELEGACION = 11;
    
    /**
     * Constante que contiene el key del parametro estado para el metodo genera Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_ESTADO = 12;
    
    /**
     * Constante que contiene el key del parametro numero de cuenta para el metodo valida Comprobante del WS Soap
     */
    public static final int PARAM_WS_VALIDACION_CAMPOS_ADIC = 13;
    
    /**
     * Constante que contiene el key del parametro accion correspondiente al path del servlet a llamar
     */
    public static final int PARAM_WS_VALIDACION_ACCION = 7;
    
    /**
     * Constante que contiene el key del parametro id de convenio consultado en enlace
     */
    public static final int PARAM_WS_VALIDACION_ID_CONV = 6;
    
    /**
     * Constante que contiene el indice del parametro campos adicionales de un hidden en el popUp
     */
    public static final int PARAM_POP_UP_CAMP_ADIC = 0;
    
    /**
     * Constante que contiene el indice del parametro total de campos adicionales de un hidden en el popUp
     */
    public static final int PARAM_POP_UP_TOTAL_CAMP_ADIC = 1;
    
    /**
     * Constante que contiene el indice del parametro concepto de un hidden en el popUp
     */
    public static final int PARAM_POP_UP_CONCEPTO = 2;
    
    /**
     * Constante que contiene el indice del parametro fecha de un hidden en el popUp
     */
    public static final int PARAM_POP_UP_FCH = 3;
    
    /**
     * Constante que contiene el indice del parametro accion
     */
    public static final int PARAM_POP_UP_ACCION = 4;
    
    /**
     * Constante que contiene el valor del input hidden de campos adicionales en el popUp
     */
    public static final String HIDDEN_POP_UP_CAMP_ADIC = "hdCamposAdic";
    
    /**
     * Constante que contiene el valor del input hidden del total de campos adicionales en el popUp
     */
    public static final String HIDDEN_POP_UP_TOTAL_CAMP_ADIC = "hdTotalCamposAdic";
    
    /**
     * Constante que contiene el valor del input hidden del concepto en el popUp
     */
    public static final String HIDDEN_POP_UP_CONCEPTO = "hdConcepto";
    
    /**
     * Constante que contiene el valor del input hidden de la fecha en el popUp
     */
    public static final String HIDDEN_POP_UP_FCH = "hdFecha";
    
    /**
     * Constante que contiene el valor del input hidden de la bandera sellos digitales
     */
    public static final String HIDDEN_OPER_CSD = "hdCadenaOperCSD";
    
    /** Constante ID_CANAL */
    public static final String ID_CANAL_ = "1";

    /** Constante CODE_OKSD para el codigo exitoso de respuesta del WS */
    public static final String CODE_OKSD = "OKSD0000";

    /**
     * Constante COD_RESPUESTA_CAM_ADIC para el codigo de respuesta 1, se
     * requieren campos adicionales
     */
    public static final String COD_RESPUESTA_CAM_ADIC = "1";

    /**
     * Constante COD_RESPUESTA_COMPROBANTE para el codigo de respuesta 3,
     * comprobante generado
     */
    public static final String COD_RESPUESTA_COMPROBANTE = "3";

    /**
     * Constante COD_RESPUESTA_ERROR para el codigo de respuesta 4, ocurrio un
     * error
     */
    public static final String COD_RESPUESTA_ERROR = "4";
   
    /**
     * Constante que almacena el formato para la fecha de pago
     */
    public static final String FORMATO_FCH_PAGO_WS = "yyyy-MM-dd HH:mm:ss";
    
    /**
     * Constante que almacena el formato para la fecha de pago sin formato correcto
     */
    public static final String FORMATO_FCH_PAGO_ENLACE = "dd/MM/yyyy HH:mm:ss";
    
    /** Constante que almacena el locale para Es mx. */
    public static final Locale LOCALE_MX = new Locale("ES", "mx");
    
    /** Constante que almacena el name de el parametro nombre */
    public static final String PARAM_CSD_FORM_NOMBRE = "Nombre";
    
    /** Constante que almacena el name de el parametro domicilio */
    public static final String PARAM_CSD_FORM_DOMICILIO = "Domicilio";
    
    /** Constante que almacena el name de el parametro colonia */
    public static final String PARAM_CSD_FORM_COLONIA = "Colonia";
    
    /** Constante que almacena el name de el parametro codigo postal */
    public static final String PARAM_CSD_FORM_CP = "CP";
    
    /** Constante que almacena el name de el parametro DelegacionMunicipio */
    public static final String PARAM_CSD_FORM_DELEGACION_MUNICIPIO = "DelegacionMunicipio";
    
    /** Constante que almacena el name de el parametro estado */
    public static final String PARAM_CSD_FORM_ESTADO = "Estado";
    
    /** Constante para almacenar hidden de campos adicoonales regresados por es WS */
    public static final String HIDDEN_CAMPOS_ADIC_WS = "camposAdicionales";
    
    /** Constante para almacenar hidden del numero de campos adicoonales regresados por es WS */
    public static final String HIDDEN_NUM_CAMPOS_ADIC_WS = "numCamposAdicionales";
    
    /** Constante para almacenar hidden de campos adicoonales de la operacion */
    public static final String HIDDEN_CAMPOS_ADIC_OPER = "camposOper";
    
    /** Constante para almacenar query param del modulo */
    public static final String QUERY_PARAM_MOD = "mod";
    
    /** Constante para almacenar query param de la referencia */
    public static final String QUERY_PARAM_REF = "ref";
   
    /** Constante que guarda el valor de la url del WS */
    public static final String URL_WS = Global.sDUrl;
    
    /** Constante para almacenar query param refe desde la consulta de bitacora */
    public static final String QUERY_PARAM_BITACORA_REFE = "refe";
    
    /** Constante para almacenar query param importe desde la consulta de bitacora */
    public static final String QUERY_PARAM_BITACORA_IMPORTE = "importe";
    
    /** Constante para almacenar query param fecha desde la consulta de bitacora */
    public static final String QUERY_PARAM_BITACORA_FECHA = "fecha";
    
    /** Constante para almacenar query param hora desde la consulta de bitacora */
    public static final String QUERY_PARAM_BITACORA_HORA = "hora";
    
    /** Constante para almacenar query param cta_destino desde la consulta de bitacora */
    public static final String QUERY_PARAM_BITACORA_CTA_DESTINO = "cta_destino";
    
    /** Constante para almacenar query param linea de captura desde la consulta de bitacora */
    public static final String QUERY_PARAM_BITACORA_LIN_CAP = "descripcion";
    
    /** Constante para almacenar query param linea de captura desde la consulta de bitacora */
    public static final String QUERY_PARAM_TMB_LIN_CAP = "concepto";
    
    /** Constante para almacenar un espacio */
    public static final String UN_ESPACIO = " ";
    
    /** Constante para almacenar signo igual */
    public static final String SIGNO_IGUAL = "=";
    
    /** Constante para almacenar signo doble coma */
    public static final String SIGNO_DOBLE_COMA = "\"";
    
    /** Constante para almacenar signo mayor que */
    public static final String SIGNO_MAYOR = ">";

    /** Constante para almacenar signo menos */
    public static final String SIGNO_MENOS = "-";
    
    /** Constante para almacenar la cadena style */
    public static final String STYLE = "style";
    
    /** Constante para almacenar el name space del WS sellos digitales */
    public static final String NAME_SPACE = "http://csd.isban.mx/";
    
    /** Constante para almacenar el service uri del WS sellos digitales */
    public static final String SERVICE_URI = "http://csd.isban.mx/";
    
    /** Constante para almacenar el target name space del WS sellos digitales */
    public static final String TARGET_NAME_SPACE = "http://csd.isban.mx/";
    
    /** Constante que almacena el path del servlet para TMB desde bitacora para generar el comprobante con sello digital */
    public static final String PATH_SERVLET_CSD_BITACORA = "/enlaceMig/comprobante_trans";
    
    /** Constante para guardar el path para el JSP de comprobanteSD */
    public static final String PATH_JSP_SD = "/jsp/datosComprobanteSD.jsp";
    
    /** Constante que contiene el valor del input hidden de la bandera para sellos digitales. */
    public static final String HIDDEN_CONVENIO_TRUE = "ConvenioSD";
    
    /** Constante PARAM_LC linea de captura.*/
    public static final String PARAM_LC = "lineaCaptura";
    
    /** Constante PARAM_IMPORTE importe.*/
    public static final String PARAM_IMPORTE = "importe";
    
    /** Constante PARAM_REFERENCIA referencia.*/
    public static final String PARAM_REFERENCIA = "folioOper";
    
    /** Constante PARAM_FECHA fecha.*/
    public static final String PARAM_FECHA = "fecha";
    
    /** Constante PARAM_CTA_DESTINO cuenta destino.*/
    public static final String PARAM_CTA_DESTINO = "cuentaAbono";
    
    /** Constantes PARAM_CONVENIO convenio.*/
    public static final String PARAM_CONVENIO = "convenio";
    
    /** Constante que almacena el path del servlet para Enlace Micrositio para generar el comprobante con sello digital */
    public static final String PATH_SERVLET_MICROSITIOCSD = "/enlaceMig/ComprobanteMicrositioSD";
    
    /** Constante para almacena la cadena tipo de dato adicional Fecha */
    public static final String TIPO_DATO_FECHA = "Fecha";
    
    /** Constante para almacena la cadena tipo de dato adicional Double */
    public static final String TIPO_DATO_DOUBLE = "Double";
    
    /**Constante MS_PARAM_ID_CONVENIO almacena el id convenio */
    public static final String MS_PARAM_ID_CONVENIO = "idconvenio";
    
    /** Constantes fecha del evento.*/
    public static final String FECHA_ENVENTO = "Fecha del Evento";
    
    /** Constantes Fecha de Escrituracion.*/
    public static final String FECHA_ESCRITURA = "Fecha de Escrituración";    
        
    /** Constantes error.*/
    public static final String COD_ERROR = "Ocurri\u00F3 un en generar el comprobante.";
    
        
    /** Constante 
    
    /**
     * Constructor privado
     */
    private ConstantesSD() {
    }
}
