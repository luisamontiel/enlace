/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * ObjectFactory.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * Clase de fabrica para la generacion de instanceas de objetos del cliente del WS Sellos digitales
 * FSW Everis
 */
@XmlRegistry
public class ObjectFactory {
    
    /** La Constante GENERA_COMPROBANTE_RESPONSE_QNAME. */
    private final static QName GENERA_COMPROBANTE_RESPONSE_QNAME = new QName(ConstantesSD.SERVICE_URI, "generaComprobanteResponse");
    
    /** La Constante VALIDA_COMPROBANTE_RESPONSE_QNAME. */
    private final static QName VALIDA_COMPROBANTE_RESPONSE_QNAME = new QName(ConstantesSD.SERVICE_URI, "validaComprobanteResponse");
    
    /** La Constante REGISTRA_MOVIMIENTO_QNAME. */
    private final static QName REGISTRA_MOVIMIENTO_QNAME = new QName(ConstantesSD.SERVICE_URI, "registraMovimiento");
    
    /** La Constante REGISTRA_MOVIMIENTO_RESPONSE_QNAME. */
    private final static QName REGISTRA_MOVIMIENTO_RESPONSE_QNAME = new QName(ConstantesSD.SERVICE_URI, "registraMovimientoResponse");
    
    /** La Constante VALIDA_COMPROBANTE_QNAME. */
    private final static QName VALIDA_COMPROBANTE_QNAME = new QName(ConstantesSD.SERVICE_URI, "validaComprobante");
    
    /** La Constante GENERA_COMPROBANTE_QNAME. */
    private final static QName GENERA_COMPROBANTE_QNAME = new QName(ConstantesSD.SERVICE_URI, "generaComprobante");

    /**
     * Crea una nueva instancea del objeto GeneraComprobanteResponse 
     *
     * @param value el parametro value correspondiente al objeto a instancear
     * @return el objeto instanceado JAXB element< genera comprobante response>
     */
    @XmlElementDecl(namespace = ConstantesSD.NAME_SPACE, name = "generaComprobanteResponse")
    public JAXBElement<GeneraComprobanteResponse> createGeneraComprobanteResponse(GeneraComprobanteResponse value) {
        return new JAXBElement<GeneraComprobanteResponse>(GENERA_COMPROBANTE_RESPONSE_QNAME, GeneraComprobanteResponse.class, null, value);
    }

    /**
     * Crea una nueva instancea del objeto ValidaComprobanteResponse 
     *
     * @param value el parametro value correspondiente al objeto a instancear
     * @return el objeto instanceado JAXB element< valida comprobante response>
     */
    @XmlElementDecl(namespace = ConstantesSD.NAME_SPACE, name = "validaComprobanteResponse")
    public JAXBElement<ValidaComprobanteResponse> createValidaComprobanteResponse(ValidaComprobanteResponse value) {
        return new JAXBElement<ValidaComprobanteResponse>(VALIDA_COMPROBANTE_RESPONSE_QNAME, ValidaComprobanteResponse.class, null, value);
    }

    /**
     * Crea una nueva instancea del objeto RegistraMovimiento 
     *
     * @param value el parametro value correspondiente al objeto a instancear
     * @return el objeto instanceado JAXB element< registra movimiento>
     */
    @XmlElementDecl(namespace = ConstantesSD.NAME_SPACE, name = "registraMovimiento")
    public JAXBElement<RegistraMovimiento> createRegistraMovimiento(RegistraMovimiento value) {
        return new JAXBElement<RegistraMovimiento>(REGISTRA_MOVIMIENTO_QNAME, RegistraMovimiento.class, null, value);
    }

    /**
     * Crea una nueva instancea del objeto RegistraMovimientoResponse 
     *
     * @param value el parametro value correspondiente al objeto a instancear
     * @return el objeto instanceado JAXB element< registra movimiento response>
     */
    @XmlElementDecl(namespace = ConstantesSD.NAME_SPACE, name = "registraMovimientoResponse")
    public JAXBElement<RegistraMovimientoResponse> createRegistraMovimientoResponse(RegistraMovimientoResponse value) {
        return new JAXBElement<RegistraMovimientoResponse>(REGISTRA_MOVIMIENTO_RESPONSE_QNAME, RegistraMovimientoResponse.class, null, value);
    }

    /**
     * Crea una nueva instancea del objeto ValidaComprobante 
     *
     * @param value el parametro value correspondiente al objeto a instancear
     * @return el objeto instanceado JAXB element< valida comprobante>
     */
    @XmlElementDecl(namespace = ConstantesSD.NAME_SPACE, name = "validaComprobante")
    public JAXBElement<ValidaComprobante> createValidaComprobante(ValidaComprobante value) {
        return new JAXBElement<ValidaComprobante>(VALIDA_COMPROBANTE_QNAME, ValidaComprobante.class, null, value);
    }

    /**
     * Crea una nueva instancea del objeto GeneraComprobante 
     *
     * @param value el parametro value correspondiente al objeto a instancear
     * @return el objeto instanceado JAXB element< genera comprobante>
     */
    @XmlElementDecl(namespace = ConstantesSD.NAME_SPACE, name = "generaComprobante")
    public JAXBElement<GeneraComprobante> createGeneraComprobante(GeneraComprobante value) {
        return new JAXBElement<GeneraComprobante>(GENERA_COMPROBANTE_QNAME, GeneraComprobante.class, null, value);
    }

}
