/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * BeanRespuestaValidacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO para el tranporte de informacion de la respuesta validacion de comprobante con sello digital
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beanRespuestaValidacion", propOrder = {
    "campAdicionales",
    "codRespuesta",
    "compSelloDigital",
    "nombreConcepto",
    "numCampAdicionales",
    "selloDigital"
})
public class BeanRespuestaValidacion
    extends Respuesta
{

    /** La variable camp adicionales. */
    protected String campAdicionales;
    
    /** La variable cod respuesta. */
    protected String codRespuesta;
    
    /** La variable comp sello digital. */
    protected String compSelloDigital;
    
    /** La variable nombre concepto. */
    protected String nombreConcepto;
    
    /** La variable num camp adicionales. */
    protected Integer numCampAdicionales;
    
    /** La variable sello digital. */
    protected String selloDigital;

    /**
     * Obtiene el valor de la variable camp adicionales.
     *
     * @return el camp adicionales
     */
    public String getCampAdicionales() {
        return campAdicionales;
    }

    /**
     * Coloca el valor de camp adicionales.
     *
     * @param value es el nuevo valor de camp adicionales
     */
    public void setCampAdicionales(String value) {
        this.campAdicionales = value;
    }

    /**
     * Obtiene el valor de la variable cod respuesta.
     *
     * @return el cod respuesta
     */
    public String getCodRespuesta() {
        return codRespuesta;
    }

    /**
     * Coloca el valor de cod respuesta.
     *
     * @param value es el nuevo valor de cod respuesta
     */
    public void setCodRespuesta(String value) {
        this.codRespuesta = value;
    }

    /**
     * Obtiene el valor de la variable comp sello digital.
     *
     * @return el comp sello digital
     */
    public String getCompSelloDigital() {
        return compSelloDigital;
    }

    /**
     * Coloca el valor de comp sello digital.
     *
     * @param value es el nuevo valor de comp sello digital
     */
    public void setCompSelloDigital(String value) {
        this.compSelloDigital = value;
    }

    /**
     * Obtiene el valor de la variable nombre concepto.
     *
     * @return el nombre concepto
     */
    public String getNombreConcepto() {
        return nombreConcepto;
    }

    /**
     * Coloca el valor de nombre concepto.
     *
     * @param value es el nuevo valor de nombre concepto
     */
    public void setNombreConcepto(String value) {
        this.nombreConcepto = value;
    }

    /**
     * Obtiene el valor de la variable num camp adicionales.
     *
     * @return el num camp adicionales
     */
    public Integer getNumCampAdicionales() {
        return numCampAdicionales;
    }

    /**
     * Coloca el valor de num camp adicionales.
     *
     * @param value es el nuevo valor de num camp adicionales
     */
    public void setNumCampAdicionales(Integer value) {
        this.numCampAdicionales = value;
    }

    /**
     * Obtiene el valor de la variable sello digital.
     *
     * @return el sello digital
     */
    public String getSelloDigital() {
        return selloDigital;
    }

    /**
     * Coloca el valor de sello digital.
     *
     * @param value es el nuevo valor de sello digital
     */
    public void setSelloDigital(String value) {
        this.selloDigital = value;
    }

}
