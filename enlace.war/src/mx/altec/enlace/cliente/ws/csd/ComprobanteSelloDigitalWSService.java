/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * ComprobanteSelloDigitalWSService.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase java para cliente de Web Service Soap de sellos digitales .
 *
 * @author FSW Everis
 */
@WebServiceClient(name = "ComprobanteSelloDigitalWSService", targetNamespace = "http://csd.isban.mx/", wsdlLocation = "ConstantesSD.URL_WS")
public class ComprobanteSelloDigitalWSService
    extends Service
{

    /** La Constante COMPROBANTESELLODIGITALWSSERVICE_WSDL_LOCATION. */
    private final static URL COMPROBANTESELLODIGITALWSSERVICE_WSDL_LOCATION;
    
    /** La Constante COMPROBANTESELLODIGITALWSSERVICE_EXCEPTION. */
    private final static WebServiceException COMPROBANTESELLODIGITALWSSERVICE_EXCEPTION;
    
    /** La Constante COMPROBANTESELLODIGITALWSSERVICE_QNAME. */
    private final static QName COMPROBANTESELLODIGITALWSSERVICE_QNAME = new QName("http://csd.isban.mx/", "ComprobanteSelloDigitalWSService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            EIGlobal.mensajePorTrace("ComprobanteSelloDigitalWSService bloque static, URL|" + ConstantesSD.URL_WS + "|", EIGlobal.NivelLog.INFO);
            url = new URL(ConstantesSD.URL_WS);
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        COMPROBANTESELLODIGITALWSSERVICE_WSDL_LOCATION = url;
        COMPROBANTESELLODIGITALWSSERVICE_EXCEPTION = e;
    }

    /**
     * Contructor de comprobante sello digital ws service.
     */
    public ComprobanteSelloDigitalWSService() {
        super(getWsdlLocation(), COMPROBANTESELLODIGITALWSSERVICE_QNAME);
    }

    /**
     * Contructor de comprobante sello digital ws service.
     *
     * @param wsdlLocation el parametro wsdl location
     */
    public ComprobanteSelloDigitalWSService(URL wsdlLocation) {
        super(wsdlLocation, COMPROBANTESELLODIGITALWSSERVICE_QNAME);
    }

    /**
     * Contructor de comprobante sello digital ws service.
     *
     * @param wsdlLocation el parametro wsdl location
     * @param serviceName el parametro service name
     */
    public ComprobanteSelloDigitalWSService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /**
     * Obtiene el valor de la variable comprobante sello digital ws impl port.
     *
     * @return     returns ComprobanteSelloDigitalWSImpl
     */
    @WebEndpoint(name = "ComprobanteSelloDigitalWSImplPort")
    public ComprobanteSelloDigitalWSImpl getComprobanteSelloDigitalWSImplPort() {
        return super.getPort(new QName("http://csd.isban.mx/", "ComprobanteSelloDigitalWSImplPort"), ComprobanteSelloDigitalWSImpl.class);
    }


    /**
     * Obtiene el objeto URL del Web Service.
     *
     * @return El url
     */
    private static URL getWsdlLocation() {
        if (COMPROBANTESELLODIGITALWSSERVICE_EXCEPTION != null) {
            throw COMPROBANTESELLODIGITALWSSERVICE_EXCEPTION;
        }
        return COMPROBANTESELLODIGITALWSSERVICE_WSDL_LOCATION;
    }

}
