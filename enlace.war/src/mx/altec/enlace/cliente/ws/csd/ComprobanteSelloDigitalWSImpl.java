/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * BeanDatosOperMasiva.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * 
 * Interface para definicion de metodos para el cliente del WS sellos digitales.
 * @author FSW Everis
 */
@WebService(name = "ComprobanteSelloDigitalWSImpl", targetNamespace = ConstantesSD.TARGET_NAME_SPACE)
@XmlSeeAlso({
    ObjectFactory.class, ObjectFactoryBean.class
})
public interface ComprobanteSelloDigitalWSImpl {


    /**
     * Validacion de comprobante con sello digital
     * @param ref referencia referente a la operacion
     * @param numCuenta numero de cuenta destino referente a la operacion
     * @param fec fecha referente a la operacion
     * @param linCap linea de captura referente a la operacion
     * @param imp importe referente a la operacion
     * @param idCanal identificador del canal referente a la operacion
     * @return BeanRespuestaValidacion objeto de respuesta de la operacion
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "validaComprobante", targetNamespace = ConstantesSD.TARGET_NAME_SPACE, className = "mx.isban.csd.ValidaComprobante")
    @ResponseWrapper(localName = "validaComprobanteResponse", targetNamespace = ConstantesSD.TARGET_NAME_SPACE, className = "mx.isban.csd.ValidaComprobanteResponse")
    @Action(input = "http://csd.isban.mx/ComprobanteSelloDigitalWSImpl/validaComprobanteRequest", output = "http://csd.isban.mx/ComprobanteSelloDigitalWSImpl/validaComprobanteResponse")
    public BeanRespuestaValidacion validaComprobante(
        @WebParam(name = "linCap", targetNamespace = "")
        String linCap,
        @WebParam(name = "imp", targetNamespace = "")
        double imp,
        @WebParam(name = "ref", targetNamespace = "")
        String ref,
        @WebParam(name = "fec", targetNamespace = "")
        String fec,
        @WebParam(name = "idCanal", targetNamespace = "")
        int idCanal,
        @WebParam(name = "numCuenta", targetNamespace = "")
        String numCuenta);

    /**
     * Generacion de comprobante con sello digital
     * @param linCap linea referente a la operacion
     * @param domicilio referente a la operacion
     * @param imp importe de la operacion
     * @param colonia referente a la operacion
     * @param nombre referente a la operacion
     * @param ref referencia referente a la operacion
     * @param fec fecha referente a la operacion
     * @param camposAdicionales referente a la operacion
     * @param cp codigo postal referente a la operacion
     * @param estado referente a la operacion
     * @param numCta numero de cuenta destino referente a la operacion
     * @param idCanal identificador del canal referente a la operacion
     * @param delegacion referente a la operacion
     * @return BeanRespuestaGeneracion objeto con respuesta de la operacion
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "generaComprobante", targetNamespace = ConstantesSD.TARGET_NAME_SPACE, className = "mx.isban.csd.GeneraComprobante")
    @ResponseWrapper(localName = "generaComprobanteResponse", targetNamespace = ConstantesSD.TARGET_NAME_SPACE, className = "mx.isban.csd.GeneraComprobanteResponse")
    @Action(input = "http://csd.isban.mx/ComprobanteSelloDigitalWSImpl/generaComprobanteRequest", output = "http://csd.isban.mx/ComprobanteSelloDigitalWSImpl/generaComprobanteResponse")
    public BeanRespuestaGeneracion generaComprobante(
        @WebParam(name = "linCap", targetNamespace = "")
        String linCap,
        @WebParam(name = "imp", targetNamespace = "")
        double imp,
        @WebParam(name = "ref", targetNamespace = "")
        String ref,
        @WebParam(name = "fec", targetNamespace = "")
        String fec,
        @WebParam(name = "idCanal", targetNamespace = "")
        int idCanal,
        @WebParam(name = "numCta", targetNamespace = "")
        String numCta,
        @WebParam(name = "nombre", targetNamespace = "")
        String nombre,
        @WebParam(name = "domicilio", targetNamespace = "")
        String domicilio,
        @WebParam(name = "colonia", targetNamespace = "")
        String colonia,
        @WebParam(name = "cp", targetNamespace = "")
        String cp,
        @WebParam(name = "delegacion", targetNamespace = "")
        String delegacion,
        @WebParam(name = "estado", targetNamespace = "")
        String estado,
        @WebParam(name = "camposAdicionales", targetNamespace = "")
        String camposAdicionales);

    /**
     * Guardar listado de movimientos
     * @param movimientos lista de movimientos a registrar
     * @return Respuesta objeto con respuesta de la operacion
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "registraMovimiento", targetNamespace = ConstantesSD.TARGET_NAME_SPACE, className = "mx.isban.csd.RegistraMovimiento")
    @ResponseWrapper(localName = "registraMovimientoResponse", targetNamespace = ConstantesSD.TARGET_NAME_SPACE, className = "mx.isban.csd.RegistraMovimientoResponse")
    @Action(input = "http://csd.isban.mx/ComprobanteSelloDigitalWSImpl/registraMovimientoRequest", output = "http://csd.isban.mx/ComprobanteSelloDigitalWSImpl/registraMovimientoResponse")
    public Respuesta registraMovimiento(
        @WebParam(name = "movimientos", targetNamespace = "")
        List<BeanDatosOperMasiva> movimientos);

}
