/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * GeneraComprobante.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DTO para el tranporte de informacion de la generacion de comprobante con sello digital
 * @author FSW Everis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generaComprobante", propOrder = {
    "linCap",
    "imp",
    "ref",
    "fec",
    "idCanal",
    "numCta",
    "nombre",
    "domicilio",
    "colonia",
    "cp",
    "delegacion",
    "estado",
    "camposAdicionales"
})
public class GeneraComprobante {

    /** La variable lin cap. */
    protected String linCap;
    
    /** La variable imp. */
    protected double imp;
    
    /** La variable ref. */
    protected String ref;
    
    /** La variable fec. */
    protected String fec;
    
    /** La variable id canal. */
    protected int idCanal;
    
    /** La variable num cta. */
    protected String numCta;
    
    /** La variable nombre. */
    protected String nombre;
    
    /** La variable domicilio. */
    protected String domicilio;
    
    /** La variable colonia. */
    protected String colonia;
    
    /** La variable cp. */
    protected String cp;
    
    /** La variable delegacion. */
    protected String delegacion;
    
    /** La variable estado. */
    protected String estado;
    
    /** La variable campos adicionales. */
    protected String camposAdicionales;

    /**
     * Obtiene el valor de la variable lin cap.
     *
     * @return el lin cap
     */
    public String getLinCap() {
        return linCap;
    }

    /**
     * Coloca el valor de lin cap.
     *
     * @param value es el nuevo valor de lin cap
     */
    public void setLinCap(String value) {
        this.linCap = value;
    }

    /**
     * Obtiene el valor de la variable imp.
     *
     * @return el imp
     */
    public double getImp() {
        return imp;
    }

    /**
     * Coloca el valor de imp.
     *
     * @param value es el nuevo valor de imp
     */
    public void setImp(double value) {
        this.imp = value;
    }

    /**
     * Obtiene el valor de la variable ref.
     *
     * @return el ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * Coloca el valor de ref.
     *
     * @param value es el nuevo valor de ref
     */
    public void setRef(String value) {
        this.ref = value;
    }

    /**
     * Obtiene el valor de la variable fec.
     *
     * @return el fec
     */
    public String getFec() {
        return fec;
    }

    /**
     * Coloca el valor de fec.
     *
     * @param value es el nuevo valor de fec
     */
    public void setFec(String value) {
        this.fec = value;
    }

    /**
     * Obtiene el valor de la variable id canal.
     *
     * @return el id canal
     */
    public int getIdCanal() {
        return idCanal;
    }

    /**
     * Coloca el valor de id canal.
     *
     * @param value es el nuevo valor de id canal
     */
    public void setIdCanal(int value) {
        this.idCanal = value;
    }

    /**
     * Obtiene el valor de la variable num cta.
     *
     * @return el num cta
     */
    public String getNumCta() {
        return numCta;
    }

    /**
     * Coloca el valor de num cta.
     *
     * @param value es el nuevo valor de num cta
     */
    public void setNumCta(String value) {
        this.numCta = value;
    }

    /**
     * Obtiene el valor de la variable nombre.
     *
     * @return el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Coloca el valor de nombre.
     *
     * @param value es el nuevo valor de nombre
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la variable domicilio.
     *
     * @return el domicilio
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * Coloca el valor de domicilio.
     *
     * @param value es el nuevo valor de domicilio
     */
    public void setDomicilio(String value) {
        this.domicilio = value;
    }

    /**
     * Obtiene el valor de la variable colonia.
     *
     * @return el colonia
     */
    public String getColonia() {
        return colonia;
    }

    /**
     * Coloca el valor de colonia.
     *
     * @param value es el nuevo valor de colonia
     */
    public void setColonia(String value) {
        this.colonia = value;
    }

    /**
     * Obtiene el valor de la variable cp.
     *
     * @return el cp
     */
    public String getCp() {
        return cp;
    }

    /**
     * Coloca el valor de cp.
     *
     * @param value es el nuevo valor de cp
     */
    public void setCp(String value) {
        this.cp = value;
    }

    /**
     * Obtiene el valor de la variable delegacion.
     *
     * @return el delegacion
     */
    public String getDelegacion() {
        return delegacion;
    }

    /**
     * Coloca el valor de delegacion.
     *
     * @param value es el nuevo valor de delegacion
     */
    public void setDelegacion(String value) {
        this.delegacion = value;
    }

    /**
     * Obtiene el valor de la variable estado.
     *
     * @return el estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Coloca el valor de estado.
     *
     * @param value es el nuevo valor de estado
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la variable campos adicionales.
     *
     * @return el campos adicionales
     */
    public String getCamposAdicionales() {
        return camposAdicionales;
    }

    /**
     * Coloca el valor de campos adicionales.
     *
     * @param value es el nuevo valor de campos adicionales
     */
    public void setCamposAdicionales(String value) {
        this.camposAdicionales = value;
    }
}