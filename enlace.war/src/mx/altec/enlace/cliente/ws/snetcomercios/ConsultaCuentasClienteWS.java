/**
 * ConsultaCuentasClienteWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package mx.altec.enlace.cliente.ws.snetcomercios;

public interface ConsultaCuentasClienteWS extends java.rmi.Remote {
    public java.lang.String[] consultaCuentasCliente(java.lang.String codCliente) throws java.rmi.RemoteException;
}
