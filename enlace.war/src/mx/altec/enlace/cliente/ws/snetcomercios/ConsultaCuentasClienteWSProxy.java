package mx.altec.enlace.cliente.ws.snetcomercios;

public class ConsultaCuentasClienteWSProxy implements mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS {
  private String _endpoint = null;
  private mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS consultaCuentasClienteWS = null;
  
  public ConsultaCuentasClienteWSProxy() {
    _initConsultaCuentasClienteWSProxy();
  }
  
  private void _initConsultaCuentasClienteWSProxy() {
    try {
      consultaCuentasClienteWS = (new mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSServiceLocator()).getConsultaCuentasClienteWSPort();
      if (consultaCuentasClienteWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)consultaCuentasClienteWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)consultaCuentasClienteWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (consultaCuentasClienteWS != null)
      ((javax.xml.rpc.Stub)consultaCuentasClienteWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS getConsultaCuentasClienteWS() {
    if (consultaCuentasClienteWS == null)
      _initConsultaCuentasClienteWSProxy();
    return consultaCuentasClienteWS;
  }
  
  public java.lang.String[] consultaCuentasCliente(java.lang.String codCliente) throws java.rmi.RemoteException{
    if (consultaCuentasClienteWS == null)
      _initConsultaCuentasClienteWSProxy();
    return consultaCuentasClienteWS.consultaCuentasCliente(codCliente);
  }
  
  
}