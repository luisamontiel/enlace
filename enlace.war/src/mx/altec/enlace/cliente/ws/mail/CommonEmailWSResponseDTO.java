/**
 * CommonEmailWSResponseDTO.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.mail;

public class CommonEmailWSResponseDTO  {
    private java.lang.String codigoError;
    private java.lang.String idMensaje;
    private java.lang.String msgError;

    public CommonEmailWSResponseDTO() {
    }

    public java.lang.String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(java.lang.String codigoError) {
        this.codigoError = codigoError;
    }

    public java.lang.String getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(java.lang.String idMensaje) {
        this.idMensaje = idMensaje;
    }

    public java.lang.String getMsgError() {
        return msgError;
    }

    public void setMsgError(java.lang.String msgError) {
        this.msgError = msgError;
    }

}
