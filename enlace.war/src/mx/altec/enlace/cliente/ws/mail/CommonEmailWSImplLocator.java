/**
 * CommonEmailWSImplLocator.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.mail;

import mx.altec.enlace.utilerias.Global;


public class CommonEmailWSImplLocator extends com.ibm.ws.webservices.multiprotocol.AgnosticService implements com.ibm.ws.webservices.multiprotocol.GeneratedService, mx.altec.enlace.cliente.ws.mail.CommonEmailWSImpl {

    public CommonEmailWSImplLocator() {
        super(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
           "http://ws.email.common.santander.com/",
           "CommonEmailWSImpl"));

        context.setLocatorName("mx.altec.enlace.cliente.ws.mail.CommonEmailWSImplLocator");
    }

    public CommonEmailWSImplLocator(com.ibm.ws.webservices.multiprotocol.ServiceContext ctx) {
        super(ctx);
        context.setLocatorName("mx.altec.enlace.cliente.ws.mail.CommonEmailWSImplLocator");
    }

    // Utilizar para obtener la clase de proxy para commonEmailServiceImplPort
    private final java.lang.String commonEmailServiceImplPort_address = Global.URL_SERVICIO_EMAIL;//"http://180.176.33.161:3080/Common-Email-WS/CommonEmailWSImpl";

    public java.lang.String getCommonEmailServiceImplPortAddress() {
        if (context.getOverriddingEndpointURIs() == null) {
            return commonEmailServiceImplPort_address;
        }
        String overriddingEndpoint = (String) context.getOverriddingEndpointURIs().get("CommonEmailServiceImplPort");
        if (overriddingEndpoint != null) {
            return overriddingEndpoint;
        }
        else {
            return commonEmailServiceImplPort_address;
        }
    }

    private java.lang.String commonEmailServiceImplPortPortName = "CommonEmailServiceImplPort";

    // The WSDD port name defaults to the port name.
    private java.lang.String commonEmailServiceImplPortWSDDPortName = "CommonEmailServiceImplPort";

    public java.lang.String getCommonEmailServiceImplPortWSDDPortName() {
        return commonEmailServiceImplPortWSDDPortName;
    }

    public void setCommonEmailServiceImplPortWSDDPortName(java.lang.String name) {
        commonEmailServiceImplPortWSDDPortName = name;
    }

    public mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl getCommonEmailServiceImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getCommonEmailServiceImplPortAddress());
        }
        catch (java.net.MalformedURLException e) {
            return null; // es poco probable ya que URL se ha validado en WSDL2Java
        }
        return getCommonEmailServiceImplPort(endpoint);
    }

    public mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl getCommonEmailServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl _stub =
            (mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl) getStub(
                commonEmailServiceImplPortPortName,
                (String) getPort2NamespaceMap().get(commonEmailServiceImplPortPortName),
                mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl.class,
                "mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImplPortBindingStub",
                portAddress.toString());
        if (_stub instanceof com.ibm.ws.webservices.engine.client.Stub) {
            ((com.ibm.ws.webservices.engine.client.Stub) _stub).setPortName(commonEmailServiceImplPortWSDDPortName);
        }
        return _stub;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                return getCommonEmailServiceImplPort();
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("WSWS3273E: Error: No hay ninguna implementación de apéndice para la interfaz:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        String inputPortName = portName.getLocalPart();
        if ("CommonEmailServiceImplPort".equals(inputPortName)) {
            return getCommonEmailServiceImplPort();
        }
        else  {
            throw new javax.xml.rpc.ServiceException();
        }
    }

    public void setPortNamePrefix(java.lang.String prefix) {
        commonEmailServiceImplPortWSDDPortName = prefix + "/" + commonEmailServiceImplPortPortName;
    }

    public javax.xml.namespace.QName getServiceName() {
        return com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "CommonEmailWSImpl");
    }

    private java.util.Map port2NamespaceMap = null;

    protected synchronized java.util.Map getPort2NamespaceMap() {
        if (port2NamespaceMap == null) {
            port2NamespaceMap = new java.util.HashMap();
            port2NamespaceMap.put(
               "CommonEmailServiceImplPort",
               "http://schemas.xmlsoap.org/wsdl/soap/");
        }
        return port2NamespaceMap;
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            String serviceNamespace = getServiceName().getNamespaceURI();
            for (java.util.Iterator i = getPort2NamespaceMap().keySet().iterator(); i.hasNext(); ) {
                ports.add(
                    com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                        serviceNamespace,
                        (String) i.next()));
            }
        }
        return ports.iterator();
    }

    public javax.xml.rpc.Call[] getCalls(javax.xml.namespace.QName portName) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
        if  (portName.getLocalPart().equals("CommonEmailServiceImplPort")) {
            return new javax.xml.rpc.Call[] {
                createCall(portName, "sendHTMLMessage", "null"),
                createCall(portName, "sendHTMLMessageExt", "null"),
            };
        }
        else {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
    }
}
