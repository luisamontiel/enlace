package mx.altec.enlace.cliente.ws.mail;


import mx.altec.enlace.beans.EnvioEmailBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;

public class CommonEnvioEmailServiceImpl {

	private static CommonEmailServiceImplProxy email = null;
	static {
		email= new CommonEmailServiceImplProxy();
	}
	public CommonEmailWSResponseDTO enviaEmail (EnvioEmailBean envioEmailBean) {

		//CommonEmailServiceImplProxy email= new CommonEmailServiceImplProxy() ;
		CommonEmailWSResponseDTO resultado = new CommonEmailWSResponseDTO();
		String mensaje = "";

		if (envioEmailBean != null) {
			mensaje = "<![CDATA[".concat(envioEmailBean.getMsgBody()).concat("]]>");

			try{
				resultado = email.sendHTMLMessage(envioEmailBean.getRequestId(), envioEmailBean.getTemplate(), envioEmailBean.getAppId(),
												envioEmailBean.getFrom(), envioEmailBean.getFromName(), envioEmailBean.getTo(),
												envioEmailBean.getToName(), envioEmailBean.getSubject(), mensaje, envioEmailBean.getSentDate(),
												envioEmailBean.getBounceAddress(), envioEmailBean.getCodigoCliente(), envioEmailBean.getCentrCostos(),
												envioEmailBean.getURLAttachment(), envioEmailBean.getAttachmentName());

				EIGlobal.mensajePorTrace("CodigoError -> " + resultado.getCodigoError(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("IdMensaje ->" + resultado.getIdMensaje(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("MsgError ->" + resultado.getMsgError(), EIGlobal.NivelLog.INFO);
			} catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);

			}
		}

		return resultado;
	}

}