/**
 * CommonEmailServiceImplPortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.mail;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

public class CommonEmailServiceImplPortBindingStub extends com.ibm.ws.webservices.engine.client.Stub implements mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl {
    public CommonEmailServiceImplPortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws com.ibm.ws.webservices.engine.WebServicesFault {
        if (service == null) {
            super.service = new com.ibm.ws.webservices.engine.client.Service();
        }
        else {
            super.service = service;
        }
        super.engine = ((com.ibm.ws.webservices.engine.client.Service) super.service).getEngine();
        initTypeMapping();
        super.cachedEndpoint = endpointURL;
        super.connection = ((com.ibm.ws.webservices.engine.client.Service) super.service).getConnection(endpointURL);
        super.messageContexts = new com.ibm.ws.webservices.engine.MessageContext[2];
        this.setTimeout(Global.TIMEOUT_WS);
    }

    private void initTypeMapping() {
        javax.xml.rpc.encoding.TypeMapping tm = super.getTypeMapping(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
        java.lang.Class javaType = null;
        javax.xml.namespace.QName xmlType = null;
        javax.xml.namespace.QName compQName = null;
        javax.xml.namespace.QName compTypeQName = null;
        com.ibm.ws.webservices.engine.encoding.SerializerFactory sf = null;
        com.ibm.ws.webservices.engine.encoding.DeserializerFactory df = null;
        javaType = java.lang.String[][].class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://jaxb.dev.java.net/array", "stringArrayArray");
        compQName = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "item");
        compTypeQName = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://jaxb.dev.java.net/array", "stringArray");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArraySerializerFactory.class, javaType, xmlType, compQName, compTypeQName);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArrayDeserializerFactory.class, javaType, xmlType, compQName, compTypeQName);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "commonEmailWSResponseDTO");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = java.lang.String[].class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://jaxb.dev.java.net/array", "stringArray");
        compQName = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "item");
        compTypeQName = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArraySerializerFactory.class, javaType, xmlType, compQName, compTypeQName);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArrayDeserializerFactory.class, javaType, xmlType, compQName, compTypeQName);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

    }

    private static final com.ibm.ws.webservices.engine.description.OperationDesc _sendHTMLMessageOperation0;
    static {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "requestId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "template"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "from"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "fromName"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "to"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "toName"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "subject"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "msgBody"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "sentDate"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "bounceAddress"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codigoCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "centrCostos"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "URLAttachment"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "attachmentName"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partName","string");
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[4].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[5].setOption("partName","string");
        _params0[5].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[6].setOption("partName","string");
        _params0[6].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[7].setOption("partName","string");
        _params0[7].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[8].setOption("partName","string");
        _params0[8].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[9].setOption("partName","string");
        _params0[9].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[10].setOption("partName","string");
        _params0[10].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[11].setOption("partName","string");
        _params0[11].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[12].setOption("partName","string");
        _params0[12].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[13].setOption("partName","string");
        _params0[13].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[14].setOption("partName","string");
        _params0[14].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "responseDTO"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "commonEmailWSResponseDTO"), mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO.class, true, false, false, false, true, false);
        _returnDesc0.setOption("partName","commonEmailWSResponseDTO");
        _returnDesc0.setOption("partQNameString","{http://ws.email.common.santander.com/}commonEmailWSResponseDTO");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _sendHTMLMessageOperation0 = new com.ibm.ws.webservices.engine.description.OperationDesc("sendHTMLMessage", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "sendHTMLMessage"), _params0, _returnDesc0, _faults0, "");
        _sendHTMLMessageOperation0.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "sendHTMLMessage"));
        _sendHTMLMessageOperation0.setOption("targetNamespace","http://ws.email.common.santander.com/");
        _sendHTMLMessageOperation0.setOption("buildNum","cf10631.06");
        _sendHTMLMessageOperation0.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "CommonEmailWSImpl"));
        _sendHTMLMessageOperation0.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "sendHTMLMessageResponse"));
        _sendHTMLMessageOperation0.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "CommonEmailServiceImpl"));
        _sendHTMLMessageOperation0.setUse(com.ibm.ws.webservices.engine.enumtype.Use.LITERAL);
        _sendHTMLMessageOperation0.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
    }

    private int _sendHTMLMessageIndex0 = 0;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getsendHTMLMessageInvoke0(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_sendHTMLMessageIndex0];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CommonEmailServiceImplPortBindingStub._sendHTMLMessageOperation0);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            mc.setEncodingStyle(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
            mc.setProperty(com.ibm.ws.webservices.engine.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
            mc.setProperty(com.ibm.ws.webservices.engine.WebServicesEngine.PROP_DOMULTIREFS, Boolean.FALSE);
            super.primeMessageContext(mc);
            super.messageContexts[_sendHTMLMessageIndex0] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO sendHTMLMessage(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getsendHTMLMessageInvoke0(new java.lang.Object[] {requestId, template, appId, from, fromName, to, toName, subject, msgBody, sentDate, bounceAddress, codigoCliente, centrCostos, URLAttachment, attachmentName}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(wsf), EIGlobal.NivelLog.INFO);
        	EIGlobal.mensajePorTrace("WEB SERVICE ENVIO EMAIL 1-->" + wsf.getMessage(),EIGlobal.NivelLog.INFO);
          //  Exception e = wsf.getUserException();
            //throw wsf;
        }
        catch (Exception e) {
        	EIGlobal.mensajePorTrace("WEB SERVICE ENVIO EMAIL 2  -->" + e.getMessage(),EIGlobal.NivelLog.INFO);
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
        try {
            return (mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO.class);
        }
    }

    private static final com.ibm.ws.webservices.engine.description.OperationDesc _sendHTMLMessageExtOperation1;
    static {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params1 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "requestId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "template"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "from"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "fromName"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "to"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "toName"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "cc"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "subject"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "msgBody"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "sentDate"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "alternativeMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "charset"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "bounceAddress"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "headers"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://jaxb.dev.java.net/array", "stringArrayArray"), java.lang.String[][].class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codigoCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "costos"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "URLAttachment"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "attachmentName"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params1[0].setOption("partName","string");
        _params1[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[1].setOption("partName","string");
        _params1[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[2].setOption("partName","string");
        _params1[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[3].setOption("partName","string");
        _params1[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[4].setOption("partName","string");
        _params1[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[5].setOption("partName","string");
        _params1[5].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[6].setOption("partName","string");
        _params1[6].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[7].setOption("partName","string");
        _params1[7].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[8].setOption("partName","string");
        _params1[8].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[9].setOption("partName","string");
        _params1[9].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[10].setOption("partName","string");
        _params1[10].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[11].setOption("partName","string");
        _params1[11].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[12].setOption("partName","string");
        _params1[12].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[13].setOption("partName","string");
        _params1[13].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[14].setOption("partName","stringArrayArray");
        _params1[14].setOption("partQNameString","{http://jaxb.dev.java.net/array}stringArrayArray");
        _params1[15].setOption("partName","string");
        _params1[15].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[16].setOption("partName","string");
        _params1[16].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[17].setOption("partName","string");
        _params1[17].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[18].setOption("partName","string");
        _params1[18].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc1 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "responseDTO"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "commonEmailWSResponseDTO"), mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO.class, true, false, false, false, true, false);
        _returnDesc1.setOption("partName","commonEmailWSResponseDTO");
        _returnDesc1.setOption("partQNameString","{http://ws.email.common.santander.com/}commonEmailWSResponseDTO");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults1 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _sendHTMLMessageExtOperation1 = new com.ibm.ws.webservices.engine.description.OperationDesc("sendHTMLMessageExt", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "sendHTMLMessageExt"), _params1, _returnDesc1, _faults1, "");
        _sendHTMLMessageExtOperation1.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "sendHTMLMessageExt"));
        _sendHTMLMessageExtOperation1.setOption("targetNamespace","http://ws.email.common.santander.com/");
        _sendHTMLMessageExtOperation1.setOption("buildNum","cf10631.06");
        _sendHTMLMessageExtOperation1.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "CommonEmailWSImpl"));
        _sendHTMLMessageExtOperation1.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "sendHTMLMessageExtResponse"));
        _sendHTMLMessageExtOperation1.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.email.common.santander.com/", "CommonEmailServiceImpl"));
        _sendHTMLMessageExtOperation1.setUse(com.ibm.ws.webservices.engine.enumtype.Use.LITERAL);
        _sendHTMLMessageExtOperation1.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
    }

    private int _sendHTMLMessageExtIndex1 = 1;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getsendHTMLMessageExtInvoke1(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_sendHTMLMessageExtIndex1];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CommonEmailServiceImplPortBindingStub._sendHTMLMessageExtOperation1);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            mc.setEncodingStyle(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
            mc.setProperty(com.ibm.ws.webservices.engine.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
            mc.setProperty(com.ibm.ws.webservices.engine.WebServicesEngine.PROP_DOMULTIREFS, Boolean.FALSE);
            super.primeMessageContext(mc);
            super.messageContexts[_sendHTMLMessageExtIndex1] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO sendHTMLMessageExt(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String cc, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String alternativeMsg, java.lang.String charset, java.lang.String bounceAddress, java.lang.String[][] headers, java.lang.String codigoCliente, java.lang.String costos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getsendHTMLMessageExtInvoke1(new java.lang.Object[] {requestId, template, appId, from, fromName, to, toName, cc, subject, msgBody, sentDate, alternativeMsg, charset, bounceAddress, headers, codigoCliente, costos, URLAttachment, attachmentName}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO.class);
        }
    }

}