/**
 * CommonEmailWSImpl.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.mail;

public interface CommonEmailWSImpl extends javax.xml.rpc.Service {
    public mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl getCommonEmailServiceImplPort() throws javax.xml.rpc.ServiceException;

    public java.lang.String getCommonEmailServiceImplPortAddress();

    public mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl getCommonEmailServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
