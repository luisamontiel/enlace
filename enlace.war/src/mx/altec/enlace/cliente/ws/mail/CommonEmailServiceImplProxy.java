package mx.altec.enlace.cliente.ws.mail;

public class CommonEmailServiceImplProxy implements mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl {
  private boolean _useJNDI = true;
  private String _endpoint = null;
  private mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl __commonEmailServiceImpl = null;
  
  public CommonEmailServiceImplProxy() {
    _initCommonEmailServiceImplProxy();
  }
  
  private void _initCommonEmailServiceImplProxy() {
  
  if (_useJNDI) {
    try{
      javax.naming.InitialContext ctx = new javax.naming.InitialContext();
      __commonEmailServiceImpl = ((mx.altec.enlace.cliente.ws.mail.CommonEmailWSImpl)ctx.lookup("java:comp/env/service/CommonEmailWSImpl")).getCommonEmailServiceImplPort();
      }
    catch (javax.naming.NamingException namingException) {}
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  if (__commonEmailServiceImpl == null) {
    try{
      __commonEmailServiceImpl = (new mx.altec.enlace.cliente.ws.mail.CommonEmailWSImplLocator()).getCommonEmailServiceImplPort();
      }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  if (__commonEmailServiceImpl != null) {
    if (_endpoint != null)
      ((javax.xml.rpc.Stub)__commonEmailServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    else
      _endpoint = (String)((javax.xml.rpc.Stub)__commonEmailServiceImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
  }
  
}


public void useJNDI(boolean useJNDI) {
  _useJNDI = useJNDI;
  __commonEmailServiceImpl = null;
  
}

public String getEndpoint() {
  return _endpoint;
}

public void setEndpoint(String endpoint) {
  _endpoint = endpoint;
  if (__commonEmailServiceImpl != null)
    ((javax.xml.rpc.Stub)__commonEmailServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
  
}

public mx.altec.enlace.cliente.ws.mail.CommonEmailServiceImpl getCommonEmailServiceImpl() {
  if (__commonEmailServiceImpl == null)
    _initCommonEmailServiceImplProxy();
  return __commonEmailServiceImpl;
}

public mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO sendHTMLMessage(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException{
  if (__commonEmailServiceImpl == null)
    _initCommonEmailServiceImplProxy();
  return __commonEmailServiceImpl.sendHTMLMessage(requestId, template, appId, from, fromName, to, toName, subject, msgBody, sentDate, bounceAddress, codigoCliente, centrCostos, URLAttachment, attachmentName);
}

public mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO sendHTMLMessageExt(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String cc, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String alternativeMsg, java.lang.String charset, java.lang.String bounceAddress, java.lang.String[][] headers, java.lang.String codigoCliente, java.lang.String costos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException{
  if (__commonEmailServiceImpl == null)
    _initCommonEmailServiceImplProxy();
  return __commonEmailServiceImpl.sendHTMLMessageExt(requestId, template, appId, from, fromName, to, toName, cc, subject, msgBody, sentDate, alternativeMsg, charset, bounceAddress, headers, codigoCliente, costos, URLAttachment, attachmentName);
}


}