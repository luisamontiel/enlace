/** 
*   Isban Mexico
*   Clase: ConsultaCertificadoResponse.java
*   Descripcion: Objeto de tranferencia de informacion de los datos de respuesta 
*   de la consulta del certificado.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.cliente.ws.etransfernal;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Objeto de tranferencia de informacion de los datos de respuesta de la consulta del certificado
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaCertificadoResponse", propOrder = {
    "respuesta"
})
public class ConsultaCertificadoResponse implements Serializable {

    /** Serial Version Universal ID. */
    private static final long serialVersionUID = 6679090136626764019L;

	/**
	 * La respuesta
	 */
    protected BeanResConsultaCertificado respuesta;

    /**
     * Obtiene el valor de la propiedad respuesta.
     * 
     * @return la respuesta
     *     
     */
    public BeanResConsultaCertificado getRespuesta() {
        return respuesta;
    }

    /**
     * Define el valor de la propiedad respuesta.
     * 
     * @param value es el nuevo valor de la respuesta
     *     
     */
    public void setRespuesta(BeanResConsultaCertificado value) {
        this.respuesta = value;
    }

}
