/** 
*   Isban Mexico
*   Clase: BeanDatosTitular.java
*   Descripcion: Objeto de tranferencia de informacion de los datos de titular
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.cliente.ws.etransfernal;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import mx.altec.enlace.utilerias.FielConstants;


/**
 * Objeto de tranferencia de informacion de los datos de titular
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beanDatosTitular", propOrder = {
    "codPostal",
    "curp",
    "direccion",
    "email",
    "estado",
    "localidad",
    "nombreOrganizacion",
    "nombreTitular",
    "numPasaporteIfe",
    "pais",
    "rfc"
})
public class BeanDatosTitular implements Serializable {

    /** Serial Version Universal ID. */
    private static final long serialVersionUID = -5058525329699079365L;

	/**
	 * El codigo postal
	 */
    protected String codPostal = FielConstants.CADENA_VACIA;
    /**
	 * La curp
	 */
    protected String curp = FielConstants.CADENA_VACIA;
    /**
	 * La direccion
	 */
    protected String direccion = FielConstants.CADENA_VACIA;
    /**
	 * El correo electronico
	 */
    protected String email = FielConstants.CADENA_VACIA;
    /**
	 * El estado
	 */
    protected String estado = FielConstants.CADENA_VACIA;
    /**
	 * La localidad
	 */
    protected String localidad = FielConstants.CADENA_VACIA;
    /**
	 * El nombre de la organizacion
	 */
    protected String nombreOrganizacion = FielConstants.CADENA_VACIA;
    /**
	 * El nombre del titular
	 */
    protected String nombreTitular = FielConstants.CADENA_VACIA;
    /**
	 * El numero de pasaporte
	 */
    protected String numPasaporteIfe = FielConstants.CADENA_VACIA;
    /**
	 * El pais
	 */
    protected String pais = FielConstants.CADENA_VACIA;
    /**
	 * El rfc
	 */
    protected String rfc = FielConstants.CADENA_VACIA;

    /**
     * Obtiene el valor de la propiedad codPostal.
     * 
     * @return el codigo postal
     *     
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     * Define el valor de la propiedad codPostal.
     * 
     * @param value es el nuevo valor del codigo postal
     *     
     */
    public void setCodPostal(String value) {
        this.codPostal = value;
    }

    /**
     * Obtiene el valor de la propiedad curp.
     * 
     * @return la curp
     *     
     */
    public String getCurp() {
        return curp;
    }

    /**
     * Define el valor de la propiedad curp.
     * 
     * @param value es el nuevo valor de la curp
     *     
     */
    public void setCurp(String value) {
        this.curp = value;
    }

    /**
     * Obtiene el valor de la propiedad direccion.
     * 
     * @return la direccion
     *     
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     * 
     * @param value es el nuevo valor de la direccion
     *     
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return el correo electronico
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value es el nuevo valor del email
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return el estado
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value es el nuevo valor del estado
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad localidad.
     * 
     * @return la localidad
     *     
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * Define el valor de la propiedad localidad.
     * 
     * @param value es el nuevo valor de la localidad
     *     
     */
    public void setLocalidad(String value) {
        this.localidad = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOrganizacion.
     * 
     * @return el nombre de la organizacion
     *     
     */
    public String getNombreOrganizacion() {
        return nombreOrganizacion;
    }

    /**
     * Define el valor de la propiedad nombreOrganizacion.
     * 
     * @param value es el nuevo valor del nombre de la organizacion
     *     
     */
    public void setNombreOrganizacion(String value) {
        this.nombreOrganizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreTitular.
     * 
     * @return el nombre del titular
     *     
     */
    public String getNombreTitular() {
        return nombreTitular;
    }

    /**
     * Define el valor de la propiedad nombreTitular.
     * 
     * @param value es el nuevo valor del nombre del titular
     *     
     */
    public void setNombreTitular(String value) {
        this.nombreTitular = value;
    }

    /**
     * Obtiene el valor de la propiedad numPasaporteIfe.
     * 
     * @return el numero de pasaporte
     *     
     */
    public String getNumPasaporteIfe() {
        return numPasaporteIfe;
    }

    /**
     * Define el valor de la propiedad numPasaporteIfe.
     * 
     * @param value es el nuevo valor del numero de pasaporte
     *     
     */
    public void setNumPasaporteIfe(String value) {
        this.numPasaporteIfe = value;
    }

    /**
     * Obtiene el valor de la propiedad pais.
     * 
     * @return el pais
     *     
     */
    public String getPais() {
        return pais;
    }

    /**
     * Define el valor de la propiedad pais.
     * 
     * @param value es el nuevo valor del pais
     *     
     */
    public void setPais(String value) {
        this.pais = value;
    }

    /**
     * Obtiene el valor de la propiedad rfc.
     * 
     * @return el RFC
     *     
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * Define el valor de la propiedad rfc.
     * 
     * @param value es el nuevo valor del RFC
     *     
     */
    public void setRfc(String value) {
        this.rfc = value;
    }

    /**
     * Complementa los campos nulos.
     */
    public void rellenaNulos() {
        if (codPostal == null) {
            codPostal = FielConstants.CADENA_VACIA;
        }
        if (curp == null) {
            curp = FielConstants.CADENA_VACIA;
        }
        if (direccion == null) {
            direccion = FielConstants.CADENA_VACIA;
        }
        if (email == null) {
            email = FielConstants.CADENA_VACIA;
        }
        if (estado == null) {
            estado = FielConstants.CADENA_VACIA;
        }
        if (localidad == null) {
            localidad = FielConstants.CADENA_VACIA;
        }
        if (nombreOrganizacion == null) {
            nombreOrganizacion = FielConstants.CADENA_VACIA;
        }
        if (nombreTitular == null) {
            nombreTitular = FielConstants.CADENA_VACIA;
        }
        if (numPasaporteIfe == null) {
            numPasaporteIfe = FielConstants.CADENA_VACIA;
        }
        if (pais == null) {
            pais = FielConstants.CADENA_VACIA;
        }
        if (rfc == null) {
            rfc = FielConstants.CADENA_VACIA;
        }
    }

}
