/** 
*   Isban Mexico
*   Clase: BeanReqConsultaCertificado.java
*   Descripcion: Objeto de tranferencia de informacion de los datos de la 
*   peticion de la consulta del certificado.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.cliente.ws.etransfernal;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Objeto de tranferencia de informacion de los datos de la peticion de la consulta del certificado
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beanReqConsultaCertificado", propOrder = {
    "numCertificado"
})
public class BeanReqConsultaCertificado implements Serializable {

    /** Serial Version Universal ID. */
    private static final long serialVersionUID = -1425143126246691944L;

	/**
	 * El numero de certificado
	 */
    protected String numCertificado;

    /**
     * Obtiene el valor de la propiedad numCertificado.
     * 
     * @return el numero de certificado
     *     
     */
    public String getNumCertificado() {
        return numCertificado;
    }

    /**
     * Define el valor de la propiedad numCertificado.
     * 
     * @param value es el nuevo valor del numero de certificado
     *     
     */
    public void setNumCertificado(String value) {
        this.numCertificado = value;
    }

}
