/** 
*   Isban Mexico
*   Clase: ConsultaCertificadoService.java
*   Descripcion: Clase de servicio encargada de consumir el servicio web de FIEL .
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.cliente.ws.etransfernal;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;


/**
 * Clase de servicio para consumir servicio web de FIEL .
 *
 * @author FSW Everis
 */
@WebServiceClient(name = FielConstants.Q_SERVICE, targetNamespace = FielConstants.NAMESPACE, wsdlLocation = "FielConstants.WS_URL")
public class ConsultaCertificadoService
    extends Service
{

	/** La Constante CONSULTACERTIFICADOSERVICE_WSDL_LOCATION. */
    private final static URL CONSULTACERTIFICADOSERVICE_WSDL_LOCATION;
    /** La Constante CONSULTACERTIFICADOSERVICE_EXCEPTION. */
    private final static WebServiceException CONSULTACERTIFICADOSERVICE_EXCEPTION;
    /** La Constante CONSULTACERTIFICADOSERVICE_QNAME. */
    private final static QName CONSULTACERTIFICADOSERVICE_QNAME = new QName(FielConstants.NAMESPACE, FielConstants.Q_SERVICE);

    static {
        URL url = null;
        WebServiceException e = null;
        try {
        	EIGlobal.mensajePorTrace("ConsultaCertificadoService bloque static, URL|" + FielConstants.WS_URL + "|", EIGlobal.NivelLog.INFO);
            url = new URL(FielConstants.WS_URL);
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        CONSULTACERTIFICADOSERVICE_WSDL_LOCATION = url;
        CONSULTACERTIFICADOSERVICE_EXCEPTION = e;
    }

    /**
     * Constructor vacio de la clase
     */
    public ConsultaCertificadoService() {
        super(obtenerWsdlLocation(), CONSULTACERTIFICADOSERVICE_QNAME);
    }
    
    /**
     * Constructor sobrecargado de la clase
     * @param wsdlLocation ubicacion de wsdl
     */
    public ConsultaCertificadoService(URL wsdlLocation) {
        super(wsdlLocation, CONSULTACERTIFICADOSERVICE_QNAME);
    }

    /**
     * Constructor sobrecargado de la clase
     * @param wsdlLocation ubicacion de wsdl
     * @param serviceName nombre de servicio
     */
    public ConsultaCertificadoService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /**
     * Metodo para obtener el port
     * @return WSConsultaCertificado interface
     */
    @WebEndpoint(name = FielConstants.Q_PORT)
    public WSConsultaCertificado getWSConsultaCertificadoImplPort() {
        return super.getPort(new QName(FielConstants.NAMESPACE, FielConstants.Q_PORT), WSConsultaCertificado.class);
    }

    /**
     * Metodo para obtener la ubicacion del WSDL
     * @return URL ubicacion del WSDL
     */
    private static URL obtenerWsdlLocation() {
        if (CONSULTACERTIFICADOSERVICE_EXCEPTION!= null) {
            throw CONSULTACERTIFICADOSERVICE_EXCEPTION;
        }
        return CONSULTACERTIFICADOSERVICE_WSDL_LOCATION;
    }

}
