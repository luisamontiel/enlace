
package mx.altec.enlace.cliente.ws.cfdiondemand;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultPeriodsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultPeriodsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseWSPeriodsCFDI" type="{http://ws.CFDIOndemand.isban.mx/}responseWSPeriods" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultPeriodsResponse", propOrder = {
    "responseWSPeriodsCFDI"
})
public class ConsultPeriodsResponse {

	/** Variable responseWSPeriodsCFDI **/
    protected ResponseWSPeriods responseWSPeriodsCFDI;

    /**
     * Gets the value of the responseWSPeriodsCFDI property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseWSPeriods }
     *     
     */
    public ResponseWSPeriods getResponseWSPeriodsCFDI() {
        return responseWSPeriodsCFDI;
    }

    /**
     * Sets the value of the responseWSPeriodsCFDI property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseWSPeriods }
     *     
     */
    public void setResponseWSPeriodsCFDI(ResponseWSPeriods value) {
        this.responseWSPeriodsCFDI = value;
    }

}
