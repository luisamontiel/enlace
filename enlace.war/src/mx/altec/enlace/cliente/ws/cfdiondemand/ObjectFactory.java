
package mx.altec.enlace.cliente.ws.cfdiondemand;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.isban.cfdiondemand.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/** Constante QNAME Url **/
	private final static String QNAME="http://ws.CFDIOndemand.isban.mx/";
	/** Constante QNAME ConsultPeriodsResponse **/
	//private final static QName _ConsultPeriodsResponse_QNAME = new QName(QNAME, "consultPeriodsResponse");
	private final static QName _CONSULTPERIODSRESPONSE_QNAME = new QName(QNAME, "consultPeriodsResponse");
	/** Constante QNAME SOAPException **/
	private final static QName _SOAPEXCEPTION_QNAME = new QName(QNAME, "SOAPException");
	/** Constante QNAME ConsultPeriods **/
	private final static QName _CONSULTPERIODS_QNAME = new QName(QNAME, "consultPeriods");

    // Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.altec.enlace.cliente.ws.cfdiondemand */
    //public ObjectFactory() { }

    /**
     * Create an instance of {@link PeriodCFDI }
     * @return PeriodCFDI : PeriodCFDI
     */
    public PeriodCFDI createPeriodCFDI() {
        return new PeriodCFDI();
    }

    /**
     * Create an instance of {@link ConsultPeriodsResponse }
     * @return ConsultPeriodsResponse : ConsultPeriodsResponse
     */
    public ConsultPeriodsResponse createConsultPeriodsResponse() {
        return new ConsultPeriodsResponse();
    }

    /**
     * Create an instance of {@link ConsultPeriods }
     * @return ConsultPeriods : ConsultPeriods
     */
    public ConsultPeriods createConsultPeriods() {
        return new ConsultPeriods();
    }

    /**
     * Create an instance of {@link ResponseWSPeriods }
     * @return ResponseWSPeriods : ResponseWSPeriods
     */
    public ResponseWSPeriods createResponseWSPeriods() {
        return new ResponseWSPeriods();
    }

    /**
     * Create an instance of {@link RequestWSPeriods }
     * @return RequestWSPeriods : RequestWSPeriods
     */
    public RequestWSPeriods createRequestWSPeriods() {
        return new RequestWSPeriods();
    }

    /**
     * Create an instance of {@link SOAPException }
     * @return SOAPException : SOAPException
     */
    public SOAPException createSOAPException() {
        return new SOAPException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultPeriodsResponse }{@code >}}
     * @param value : value
     * @return JAXBElement : JAXBElement
     */
    @XmlElementDecl(namespace = QNAME, name = "consultPeriodsResponse")
    public JAXBElement<ConsultPeriodsResponse> createConsultPeriodsResponse(ConsultPeriodsResponse value) {
        return new JAXBElement<ConsultPeriodsResponse>(_CONSULTPERIODSRESPONSE_QNAME, ConsultPeriodsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPException }{@code >}}
     * @param value : value
     * @return JAXBElement : JAXBElement
     */
    @XmlElementDecl(namespace = QNAME, name = "SOAPException")
    public JAXBElement<SOAPException> createSOAPException(SOAPException value) {
        return new JAXBElement<SOAPException>(_SOAPEXCEPTION_QNAME, SOAPException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultPeriods }{@code >}}
     * @param value : value
     * @return JAXBElement : JAXBElement
     */
    @XmlElementDecl(namespace = QNAME, name = "consultPeriods")
    public JAXBElement<ConsultPeriods> createConsultPeriods(ConsultPeriods value) {
        return new JAXBElement<ConsultPeriods>(_CONSULTPERIODS_QNAME, ConsultPeriods.class, null, value);
    }

}
