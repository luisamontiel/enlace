
package mx.altec.enlace.cliente.ws.cfdiondemand;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for responseWSPeriods complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="responseWSPeriods">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoRespuesta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="descripcionCodigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaPeriodos" type="{http://ws.CFDIOndemand.isban.mx/}periodCFDI" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responseWSPeriods", propOrder = {
    "codigoRespuesta",
    "descripcionCodigo",
    "listaPeriodos"
})
public class ResponseWSPeriods {

	/** Variable codigoRespuesta **/
    protected Integer codigoRespuesta;
    /** Variable descripcionCodigo **/
    protected String descripcionCodigo;
    /** Variable listaPeriodos **/
    @XmlElement(nillable = true)
    protected List<PeriodCFDI> listaPeriodos;

    /**
     * Gets the value of the codigoRespuesta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Sets the value of the codigoRespuesta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoRespuesta(Integer value) {
        this.codigoRespuesta = value;
    }

    /**
     * Gets the value of the descripcionCodigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionCodigo() {
        return descripcionCodigo;
    }

    /**
     * Sets the value of the descripcionCodigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionCodigo(String value) {
        this.descripcionCodigo = value;
    }

    /**
     * Gets the value of the listaPeriodos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaPeriodos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaPeriodos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PeriodCFDI }
     * 
     * @return List : lista
     * 
     */
    public List<PeriodCFDI> getListaPeriodos() {
        if (listaPeriodos == null) {
            listaPeriodos = new ArrayList<PeriodCFDI>();
        }
        return this.listaPeriodos;
    }

    /**
     * Sets the value of the listaPeriodos property.
     * 
     * @param listaPeriodos
     *     allowed object is
     *     {@link List<PeriodCFDI> }
     *     
     */
	public void setListaPeriodos(List<PeriodCFDI> listaPeriodos) {
		this.listaPeriodos = listaPeriodos;
	}

}
