
package mx.altec.enlace.cliente.ws.pdfondemand;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validaExistenciaResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="validaExistenciaResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseWS" type="{http://ws.estadocuenta.isban.mx/}responseWS" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validaExistenciaResponse", propOrder = {
    "responseWS"
})
public class ValidaExistenciaResponse {

	/** Variable responesWS **/
    protected ResponseWS responseWS;

    /**
     * Gets the value of the responseWS property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseWS }
     *     
     */
    public ResponseWS getResponseWS() {
        return responseWS;
    }

    /**
     * Sets the value of the responseWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseWS }
     *     
     */
    public void setResponseWS(ResponseWS value) {
        this.responseWS = value;
    }

}
