
package mx.altec.enlace.cliente.ws.pdfondemand;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.isban.estadocuenta.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	
	/** Constante QNAME con la URL **/
	private final static String QNAME = "http://ws.estadocuenta.isban.mx/";
	/** Constante _SOAPEXCEPTION QNAME **/
    private final static QName _SOAPEXCEPTION_QNAME = new QName(QNAME, "SOAPException");
    /** Constante _VALIDAEXISTENCIARESPONSE QNAME **/
    private final static QName _VALIDAEXISTENCIARESPONSE_QNAME = new QName(QNAME, "validaExistenciaResponse");
    /** Constante _VALIDAEXISTENCIA QNAME **/
    private final static QName _VALIDAEXISTENCIA_QNAME = new QName(QNAME, "validaExistencia");

    // ** Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.altec.enlace.cliente.ws.pdfondemand*/
    //public ObjectFactory() {}

    /**
     * Create an instance of {@link ResponseWS }
     * @return ResponseWS : Objeto del tipo ResponseWS
     */
    public ResponseWS createResponseWS() {
        return new ResponseWS();
    }

    /**
     * Create an instance of {@link SOAPException }
     * @return SOAPException : Objeto de Excepcion 
     */
    public SOAPException createSOAPException() {
        return new SOAPException();
    }

    /**
     * Create an instance of {@link RequestWS }
     * @return RequestWS : Objeto de tipo RequestWS
     */
    public RequestWS createRequestWS() {
        return new RequestWS();
    }

    /**
     * Create an instance of {@link ValidaExistencia }
     * @return ValidaExistencia : Objeto del tipo ValidaExistencia
     */
    public ValidaExistencia createValidaExistencia() {
        return new ValidaExistencia();
    }

    /**
     * Create an instance of {@link ValidaExistenciaResponse }
     * @return ValidaExistenciaResponse : Objeto del tipo ValidaExistenciaResponse
     */
    public ValidaExistenciaResponse createValidaExistenciaResponse() {
        return new ValidaExistenciaResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPException }{@code >}}
     * @param value : value
     * @return JAXBElement : Objeto JAXBElement
     */
    @XmlElementDecl(namespace = QNAME, name = "SOAPException")
    public JAXBElement<SOAPException> createSOAPException(SOAPException value) {
        return new JAXBElement<SOAPException>(_SOAPEXCEPTION_QNAME, SOAPException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaExistenciaResponse }{@code >}}
     * @param value : value
     * @return JAXBElement : Objeto JAXBElement 
     */
    @XmlElementDecl(namespace = QNAME, name = "validaExistenciaResponse")
    public JAXBElement<ValidaExistenciaResponse> createValidaExistenciaResponse(ValidaExistenciaResponse value) {
        return new JAXBElement<ValidaExistenciaResponse>(_VALIDAEXISTENCIARESPONSE_QNAME, ValidaExistenciaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaExistencia }{@code >}}
     * @param value : value
     * @return JAXBElement : Objeto JAXBElement 
     */
    @XmlElementDecl(namespace = QNAME, name = "validaExistencia")
    public JAXBElement<ValidaExistencia> createValidaExistencia(ValidaExistencia value) {
        return new JAXBElement<ValidaExistencia>(_VALIDAEXISTENCIA_QNAME, ValidaExistencia.class, null, value);
    }

}
