
package mx.altec.enlace.cliente.ws.pdfondemand;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


/**
 * This class was generated by the JAXWS SI.
 * JAX-WS RI 2.0_02-b08-fcs
 * Generated source version: 2.0
 * 
 */
@WebServiceClient(name = "ValidaExistenciaPDFService", targetNamespace = "http://impl.ws.ValidaExistenciaPDFOndemand.isban.mx/", wsdlLocation = "Global.validaExistenciaPDFWsdlLocation")
public class ValidaExistenciaPDFService_Service
    extends Service
{
	/** Constante URL **/
    private final static URL VALIDAEXISTENCIAPDFSERVICE_WSDL_LOCATION;

    static {
        URL url = null;
        try {
        	//url = new URL("https://mxfrvlecudes002.dev.mx.corp:1444/WSVEPDFOndemand/ValidaExistenciaPDFService/WEB-INF/wsdl/ValidaExistenciaPDFService.wsdl");
        	EIGlobal.mensajePorTrace("::: [EDCPDFXML] ::: ValidaExistenciaPDFService_Service : URL de Web Service: [" + Global.validaExistenciaPDFWsdlLocation + "]", NivelLog.DEBUG);
            url = new URL(Global.validaExistenciaPDFWsdlLocation);
            EIGlobal.mensajePorTrace("URL Servicio ["+url+"]", EIGlobal.NivelLog.INFO);
        } catch (MalformedURLException e) {
            //e.printStackTrace();
        	EIGlobal.mensajePorTrace("::: [EDCPDFXML] ::: ValidaExistenciaPDFService_Service - Se presento una Excepcion: "+e, NivelLog.DEBUG);
        }
        VALIDAEXISTENCIAPDFSERVICE_WSDL_LOCATION = url;
    }

    /**
     * Constructor de la clase
     * @param wsdlLocation : wsdlLocation
     * @param serviceName : serviceName
     */
    public ValidaExistenciaPDFService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /**
     * Constructor default de la clase
     */
    public ValidaExistenciaPDFService_Service() {
        super(VALIDAEXISTENCIAPDFSERVICE_WSDL_LOCATION, new QName("http://impl.ws.ValidaExistenciaPDFOndemand.isban.mx/", "ValidaExistenciaPDFService"));
    }

    /**
     * Metodo para obtener el puerto del llamad del WS.
     * @return returns ValidaExistenciaPDFService
     */
    @WebEndpoint(name = "ValidaExistenciaPDFServicePort")
    public ValidaExistenciaPDFService getValidaExistenciaPDFServicePort() {
    	ValidaExistenciaPDFService validaExistenciaPDFService = super.getPort(new QName("http://impl.ws.ValidaExistenciaPDFOndemand.isban.mx/", "ValidaExistenciaPDFServicePort"), ValidaExistenciaPDFService.class);
    	((BindingProvider) validaExistenciaPDFService).getRequestContext().put("com.sun.xml.internal.ws.connect.timeout", 30000);
    	((BindingProvider) validaExistenciaPDFService).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", 30000);
    	((BindingProvider) validaExistenciaPDFService).getRequestContext().put(com.ibm.websphere.webservices.jaxws.Constants.ASYNC_TIMEOUT_MILLISECONDS, 30000);
        return validaExistenciaPDFService;
    }

}
