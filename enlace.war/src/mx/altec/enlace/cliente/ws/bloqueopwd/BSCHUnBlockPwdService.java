/**
 * BSCHUnBlockPwdService.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.bloqueopwd;

public interface BSCHUnBlockPwdService extends javax.xml.rpc.Service {
    public BSCHUnBlockPwd getBSCHUnBlockPwd() throws javax.xml.rpc.ServiceException;

    public java.lang.String getBSCHUnBlockPwdAddress();

    public BSCHUnBlockPwd getBSCHUnBlockPwd(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
