package mx.altec.enlace.cliente.ws.bloqueopwd;

public class BSCHUnBlockPwdProxy implements BSCHUnBlockPwd {
  private boolean _useJNDI = true;
  private String _endpoint = null;
  private BSCHUnBlockPwd __bSCHUnBlockPwd = null;
  
  public BSCHUnBlockPwdProxy() {
    _initBSCHUnBlockPwdProxy();
  }
  
  private void _initBSCHUnBlockPwdProxy() {
  
  if (_useJNDI) {
    try{
      javax.naming.InitialContext ctx = new javax.naming.InitialContext();
      __bSCHUnBlockPwd = ((BSCHUnBlockPwdService)ctx.lookup("java:comp/env/service/BSCHUnBlockPwdService")).getBSCHUnBlockPwd();
      }
    catch (javax.naming.NamingException namingException) {}
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  if (__bSCHUnBlockPwd == null) {
    try{
      __bSCHUnBlockPwd = (new BSCHUnBlockPwdServiceLocator()).getBSCHUnBlockPwd();
      }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  if (__bSCHUnBlockPwd != null) {
    if (_endpoint != null)
      ((javax.xml.rpc.Stub)__bSCHUnBlockPwd)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    else
      _endpoint = (String)((javax.xml.rpc.Stub)__bSCHUnBlockPwd)._getProperty("javax.xml.rpc.service.endpoint.address");
  }
  
}


public void useJNDI(boolean useJNDI) {
  _useJNDI = useJNDI;
  __bSCHUnBlockPwd = null;
  
}

public String getEndpoint() {
  return _endpoint;
}

public void setEndpoint(String endpoint) {
  _endpoint = endpoint;
  if (__bSCHUnBlockPwd != null)
    ((javax.xml.rpc.Stub)__bSCHUnBlockPwd)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
  
}

public BSCHUnBlockPwd getBSCHUnBlockPwd() {
  if (__bSCHUnBlockPwd == null)
    _initBSCHUnBlockPwdProxy();
  return __bSCHUnBlockPwd;
}

public java.lang.String userOpPwdUnBlock(java.lang.String appId, java.lang.String lang, java.lang.String usrId, java.lang.String usrOprId, java.lang.String tipoOper) throws java.rmi.RemoteException{
  if (__bSCHUnBlockPwd == null)
    _initBSCHUnBlockPwdProxy();
  return __bSCHUnBlockPwd.userOpPwdUnBlock(appId, lang, usrId, usrOprId, tipoOper);
}

public java.lang.String cambiarEstatus(java.lang.String appId, java.lang.String lang, java.lang.String usrId, int status, java.lang.String codWS) throws java.rmi.RemoteException{
  if (__bSCHUnBlockPwd == null)
    _initBSCHUnBlockPwdProxy();
  return __bSCHUnBlockPwd.cambiarEstatus(appId, lang, usrId, status, codWS);
}

public java.lang.String userOpPwdBlock(java.lang.String appId, java.lang.String lang, java.lang.String usrId, java.lang.String usrOprId, java.lang.String tipoOper) throws java.rmi.RemoteException{
  if (__bSCHUnBlockPwd == null)
    _initBSCHUnBlockPwdProxy();
  return __bSCHUnBlockPwd.userOpPwdBlock(appId, lang, usrId, usrOprId, tipoOper);
}


}