/**
 * BSCHUnBlockPwdServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.bloqueopwd;

public class BSCHUnBlockPwdServiceLocator extends com.ibm.ws.webservices.multiprotocol.AgnosticService implements com.ibm.ws.webservices.multiprotocol.GeneratedService, BSCHUnBlockPwdService {

    public BSCHUnBlockPwdServiceLocator() {
        super(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
           "http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws",
           "BSCHUnBlockPwdService"));

        context.setLocatorName("BSCHUnBlockPwdServiceLocator");
    }

    public BSCHUnBlockPwdServiceLocator(com.ibm.ws.webservices.multiprotocol.ServiceContext ctx) {
        super(ctx);
        context.setLocatorName("BSCHUnBlockPwdServiceLocator");
    }

    // Utilizar para obtener la clase de proxy para BSCHUnBlockPwd
    private final java.lang.String BSCHUnBlockPwd_address = "http://180.176.16.49/wseg/axis/altecmx/BSCHUnBlockPwd.jws";

    public java.lang.String getBSCHUnBlockPwdAddress() {
        if (context.getOverriddingEndpointURIs() == null) {
            return BSCHUnBlockPwd_address;
        }
        String overriddingEndpoint = (String) context.getOverriddingEndpointURIs().get("BSCHUnBlockPwd");
        if (overriddingEndpoint != null) {
            return overriddingEndpoint;
        }
        else {
            return BSCHUnBlockPwd_address;
        }
    }

    private java.lang.String BSCHUnBlockPwdPortName = "BSCHUnBlockPwd";

    // The WSDD port name defaults to the port name.
    private java.lang.String BSCHUnBlockPwdWSDDPortName = "BSCHUnBlockPwd";

    public java.lang.String getBSCHUnBlockPwdWSDDPortName() {
        return BSCHUnBlockPwdWSDDPortName;
    }

    public void setBSCHUnBlockPwdWSDDPortName(java.lang.String name) {
        BSCHUnBlockPwdWSDDPortName = name;
    }

    public BSCHUnBlockPwd getBSCHUnBlockPwd() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getBSCHUnBlockPwdAddress());
        }
        catch (java.net.MalformedURLException e) {
            return null; // es poco probable ya que URL se ha validado en WSDL2Java
        }
        return getBSCHUnBlockPwd(endpoint);
    }

    public BSCHUnBlockPwd getBSCHUnBlockPwd(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        BSCHUnBlockPwd _stub =
            (BSCHUnBlockPwd) getStub(
                BSCHUnBlockPwdPortName,
                (String) getPort2NamespaceMap().get(BSCHUnBlockPwdPortName),
                BSCHUnBlockPwd.class,
                "BSCHUnBlockPwdSoapBindingStub",
                portAddress.toString());
        if (_stub instanceof com.ibm.ws.webservices.engine.client.Stub) {
            ((com.ibm.ws.webservices.engine.client.Stub) _stub).setPortName(BSCHUnBlockPwdWSDDPortName);
        }
        return _stub;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (BSCHUnBlockPwd.class.isAssignableFrom(serviceEndpointInterface)) {
                return getBSCHUnBlockPwd();
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("WSWS3273E: Error: No hay ninguna implementación de apéndice para la interfaz:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        String inputPortName = portName.getLocalPart();
        if ("BSCHUnBlockPwd".equals(inputPortName)) {
            return getBSCHUnBlockPwd();
        }
        else  {
            throw new javax.xml.rpc.ServiceException();
        }
    }

    public void setPortNamePrefix(java.lang.String prefix) {
        BSCHUnBlockPwdWSDDPortName = prefix + "/" + BSCHUnBlockPwdPortName;
    }

    public javax.xml.namespace.QName getServiceName() {
        return com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwdService");
    }

    private java.util.Map port2NamespaceMap = null;

    protected synchronized java.util.Map getPort2NamespaceMap() {
        if (port2NamespaceMap == null) {
            port2NamespaceMap = new java.util.HashMap();
            port2NamespaceMap.put(
               "BSCHUnBlockPwd",
               "http://schemas.xmlsoap.org/wsdl/soap/");
        }
        return port2NamespaceMap;
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            String serviceNamespace = getServiceName().getNamespaceURI();
            for (java.util.Iterator i = getPort2NamespaceMap().keySet().iterator(); i.hasNext(); ) {
                ports.add(
                    com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                        serviceNamespace,
                        (String) i.next()));
            }
        }
        return ports.iterator();
    }

    public javax.xml.rpc.Call[] getCalls(javax.xml.namespace.QName portName) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
        if  (portName.getLocalPart().equals("BSCHUnBlockPwd")) {
            return new javax.xml.rpc.Call[] {
                createCall(portName, "userOpPwdUnBlock", "userOpPwdUnBlockRequest"),
                createCall(portName, "userOpPwdBlock", "userOpPwdBlockRequest"),
                createCall(portName, "cambiarEstatus", "cambiarEstatusRequest"),
            };
        }
        else {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
    }
}
