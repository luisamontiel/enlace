/**
 * CanalesWSService.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf160638.12 v101006191000
 */

package mx.altec.enlace.cliente.ws.bloqueotoken;

public interface CanalesWSService extends javax.xml.rpc.Service {
    public CanalesWS getCanalesWS() throws javax.xml.rpc.ServiceException;

    public java.lang.String getCanalesWSAddress();

    public CanalesWS getCanalesWS(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}