/**
 * CanalesWSServiceInformation.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf160638.12 v101006191000
 */

package mx.altec.enlace.cliente.ws.bloqueotoken;

import mx.altec.enlace.utilerias.Global;

public class CanalesWSServiceInformation implements com.ibm.ws.webservices.multiprotocol.ServiceInformation {

    private static java.util.Map operationDescriptions;
    private static java.util.Map typeMappings;

    static {
         initOperationDescriptions();
         initTypeMappings();
    }

    private static void initOperationDescriptions() {
        operationDescriptions = new java.util.HashMap();

        java.util.Map inner0 = new java.util.HashMap();

        java.util.List list0 = new java.util.ArrayList();
        inner0.put("activacion", list0);

        com.ibm.ws.webservices.engine.description.OperationDesc activacion0Op = _activacion0Op();
        list0.add(activacion0Op);

        java.util.List list1 = new java.util.ArrayList();
        inner0.put("activarToken", list1);

        com.ibm.ws.webservices.engine.description.OperationDesc activarToken1Op = _activarToken1Op();
        list1.add(activarToken1Op);

        java.util.List list2 = new java.util.ArrayList();
        inner0.put("bloquearToken", list2);

        com.ibm.ws.webservices.engine.description.OperationDesc bloquearToken2Op = _bloquearToken2Op();
        list2.add(bloquearToken2Op);

        java.util.List list3 = new java.util.ArrayList();
        inner0.put("bloqueo", list3);

        com.ibm.ws.webservices.engine.description.OperationDesc bloqueo3Op = _bloqueo3Op();
        list3.add(bloqueo3Op);

        java.util.List list4 = new java.util.ArrayList();
        inner0.put("consultaContrato", list4);

        com.ibm.ws.webservices.engine.description.OperationDesc consultaContrato4Op = _consultaContrato4Op();
        list4.add(consultaContrato4Op);

        java.util.List list5 = new java.util.ArrayList();
        inner0.put("consultaEstatus", list5);

        com.ibm.ws.webservices.engine.description.OperationDesc consultaEstatus5Op = _consultaEstatus5Op();
        list5.add(consultaEstatus5Op);

        java.util.List list6 = new java.util.ArrayList();
        inner0.put("consultaListaEstatus", list6);

        com.ibm.ws.webservices.engine.description.OperationDesc consultaListaEstatus6Op = _consultaListaEstatus6Op();
        list6.add(consultaListaEstatus6Op);

        java.util.List list7 = new java.util.ArrayList();
        inner0.put("contrato", list7);

        com.ibm.ws.webservices.engine.description.OperationDesc contrato7Op = _contrato7Op();
        list7.add(contrato7Op);

        java.util.List list8 = new java.util.ArrayList();
        inner0.put("desBloquearToken", list8);

        com.ibm.ws.webservices.engine.description.OperationDesc desBloquearToken8Op = _desBloquearToken8Op();
        list8.add(desBloquearToken8Op);

        java.util.List list9 = new java.util.ArrayList();
        inner0.put("desBloqueo", list9);

        com.ibm.ws.webservices.engine.description.OperationDesc desBloqueo9Op = _desBloqueo9Op();
        list9.add(desBloqueo9Op);

        java.util.List list10 = new java.util.ArrayList();
        inner0.put("estatus", list10);

        com.ibm.ws.webservices.engine.description.OperationDesc estatus10Op = _estatus10Op();
        list10.add(estatus10Op);

        java.util.List list11 = new java.util.ArrayList();
        inner0.put("solicitud", list11);

        com.ibm.ws.webservices.engine.description.OperationDesc solicitud11Op = _solicitud11Op();
        list11.add(solicitud11Op);

        java.util.List list12 = new java.util.ArrayList();
        inner0.put("solicitudToken", list12);

        com.ibm.ws.webservices.engine.description.OperationDesc solicitudToken12Op = _solicitudToken12Op();
        list12.add(solicitudToken12Op);

        operationDescriptions.put("CanalesWS",inner0);
        operationDescriptions = java.util.Collections.unmodifiableMap(operationDescriptions);
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _activacion0Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc activacion0Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "ActivacionReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        activacion0Op = new com.ibm.ws.webservices.engine.description.OperationDesc("activacion", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Activacion"), _params0, _returnDesc0, _faults0, null);
        activacion0Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        activacion0Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        activacion0Op.setOption("inputName","ActivacionRequest");
        activacion0Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ActivacionResponse"));
        activacion0Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        activacion0Op.setOption("buildNum","cf160638.12");
        activacion0Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        activacion0Op.setOption("outputName","ActivacionResponse");
        activacion0Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        activacion0Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ActivacionRequest"));
        activacion0Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return activacion0Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _activarToken1Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc activarToken1Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "activarTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        activarToken1Op = new com.ibm.ws.webservices.engine.description.OperationDesc("activarToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "activarToken"), _params0, _returnDesc0, _faults0, null);
        activarToken1Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        activarToken1Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        activarToken1Op.setOption("inputName","activarTokenRequest");
        activarToken1Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "activarTokenResponse"));
        activarToken1Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        activarToken1Op.setOption("buildNum","cf160638.12");
        activarToken1Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        activarToken1Op.setOption("outputName","activarTokenResponse");
        activarToken1Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        activarToken1Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "activarTokenRequest"));
        activarToken1Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return activarToken1Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _bloquearToken2Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc bloquearToken2Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codMotivo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "bloquearTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        bloquearToken2Op = new com.ibm.ws.webservices.engine.description.OperationDesc("bloquearToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "bloquearToken"), _params0, _returnDesc0, _faults0, null);
        bloquearToken2Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        bloquearToken2Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        bloquearToken2Op.setOption("inputName","bloquearTokenRequest");
        bloquearToken2Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "bloquearTokenResponse"));
        bloquearToken2Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        bloquearToken2Op.setOption("buildNum","cf160638.12");
        bloquearToken2Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        bloquearToken2Op.setOption("outputName","bloquearTokenResponse");
        bloquearToken2Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        bloquearToken2Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "bloquearTokenRequest"));
        bloquearToken2Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return bloquearToken2Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _bloqueo3Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc bloqueo3Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codMotivo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "BloqueoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        bloqueo3Op = new com.ibm.ws.webservices.engine.description.OperationDesc("bloqueo", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Bloqueo"), _params0, _returnDesc0, _faults0, null);
        bloqueo3Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        bloqueo3Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        bloqueo3Op.setOption("inputName","BloqueoRequest");
        bloqueo3Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "BloqueoResponse"));
        bloqueo3Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        bloqueo3Op.setOption("buildNum","cf160638.12");
        bloqueo3Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        bloqueo3Op.setOption("outputName","BloqueoResponse");
        bloqueo3Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        bloqueo3Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "BloqueoRequest"));
        bloqueo3Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return bloqueo3Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaContrato4Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc consultaContrato4Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "consultaContratoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        consultaContrato4Op = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaContrato", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "consultaContrato"), _params0, _returnDesc0, _faults0, null);
        consultaContrato4Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        consultaContrato4Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        consultaContrato4Op.setOption("inputName","consultaContratoRequest");
        consultaContrato4Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaContratoResponse"));
        consultaContrato4Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        consultaContrato4Op.setOption("buildNum","cf160638.12");
        consultaContrato4Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        consultaContrato4Op.setOption("outputName","consultaContratoResponse");
        consultaContrato4Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        consultaContrato4Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaContratoRequest"));
        consultaContrato4Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return consultaContrato4Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaEstatus5Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc consultaEstatus5Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "consultaEstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        consultaEstatus5Op = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaEstatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "consultaEstatus"), _params0, _returnDesc0, _faults0, null);
        consultaEstatus5Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        consultaEstatus5Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        consultaEstatus5Op.setOption("inputName","consultaEstatusRequest");
        consultaEstatus5Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaEstatusResponse"));
        consultaEstatus5Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        consultaEstatus5Op.setOption("buildNum","cf160638.12");
        consultaEstatus5Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        consultaEstatus5Op.setOption("outputName","consultaEstatusResponse");
        consultaEstatus5Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        consultaEstatus5Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaEstatusRequest"));
        consultaEstatus5Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return consultaEstatus5Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaListaEstatus6Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc consultaListaEstatus6Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lista"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _params0[0].setOption("partName","ArrayOf_xsd_string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "consultaListaEstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOfArrayOf_xsd_string"), java.lang.String[][].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOfArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOfArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        consultaListaEstatus6Op = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaListaEstatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "consultaListaEstatus"), _params0, _returnDesc0, _faults0, null);
        consultaListaEstatus6Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        consultaListaEstatus6Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        consultaListaEstatus6Op.setOption("inputName","consultaListaEstatusRequest");
        consultaListaEstatus6Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaListaEstatusResponse"));
        consultaListaEstatus6Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        consultaListaEstatus6Op.setOption("buildNum","cf160638.12");
        consultaListaEstatus6Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        consultaListaEstatus6Op.setOption("outputName","consultaListaEstatusResponse");
        consultaListaEstatus6Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        consultaListaEstatus6Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaListaEstatusRequest"));
        consultaListaEstatus6Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return consultaListaEstatus6Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _contrato7Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc contrato7Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "ContratoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        contrato7Op = new com.ibm.ws.webservices.engine.description.OperationDesc("contrato", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Contrato"), _params0, _returnDesc0, _faults0, null);
        contrato7Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        contrato7Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        contrato7Op.setOption("inputName","ContratoRequest");
        contrato7Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ContratoResponse"));
        contrato7Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        contrato7Op.setOption("buildNum","cf160638.12");
        contrato7Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        contrato7Op.setOption("outputName","ContratoResponse");
        contrato7Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        contrato7Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ContratoRequest"));
        contrato7Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return contrato7Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _desBloquearToken8Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc desBloquearToken8Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "desBloquearTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        desBloquearToken8Op = new com.ibm.ws.webservices.engine.description.OperationDesc("desBloquearToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "desBloquearToken"), _params0, _returnDesc0, _faults0, null);
        desBloquearToken8Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        desBloquearToken8Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        desBloquearToken8Op.setOption("inputName","desBloquearTokenRequest");
        desBloquearToken8Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloquearTokenResponse"));
        desBloquearToken8Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        desBloquearToken8Op.setOption("buildNum","cf160638.12");
        desBloquearToken8Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        desBloquearToken8Op.setOption("outputName","desBloquearTokenResponse");
        desBloquearToken8Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        desBloquearToken8Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloquearTokenRequest"));
        desBloquearToken8Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return desBloquearToken8Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _desBloqueo9Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc desBloqueo9Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "desBloqueoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        desBloqueo9Op = new com.ibm.ws.webservices.engine.description.OperationDesc("desBloqueo", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "desBloqueo"), _params0, _returnDesc0, _faults0, null);
        desBloqueo9Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        desBloqueo9Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        desBloqueo9Op.setOption("inputName","desBloqueoRequest");
        desBloqueo9Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloqueoResponse"));
        desBloqueo9Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        desBloqueo9Op.setOption("buildNum","cf160638.12");
        desBloqueo9Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        desBloqueo9Op.setOption("outputName","desBloqueoResponse");
        desBloqueo9Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        desBloqueo9Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloqueoRequest"));
        desBloqueo9Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return desBloqueo9Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _estatus10Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc estatus10Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "EstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        estatus10Op = new com.ibm.ws.webservices.engine.description.OperationDesc("estatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Estatus"), _params0, _returnDesc0, _faults0, null);
        estatus10Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        estatus10Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        estatus10Op.setOption("inputName","EstatusRequest");
        estatus10Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "EstatusResponse"));
        estatus10Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        estatus10Op.setOption("buildNum","cf160638.12");
        estatus10Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        estatus10Op.setOption("outputName","EstatusResponse");
        estatus10Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        estatus10Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "EstatusRequest"));
        estatus10Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return estatus10Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _solicitud11Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc solicitud11Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codContratoEnlace"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCuentaCargo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codClienteEmpresa"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[4].setOption("partName","string");
        _params0[5].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[5].setOption("partName","string");
        _params0[6].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[6].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "SolicitudReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        solicitud11Op = new com.ibm.ws.webservices.engine.description.OperationDesc("solicitud", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Solicitud"), _params0, _returnDesc0, _faults0, null);
        solicitud11Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        solicitud11Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        solicitud11Op.setOption("inputName","SolicitudRequest");
        solicitud11Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "SolicitudResponse"));
        solicitud11Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        solicitud11Op.setOption("buildNum","cf160638.12");
        solicitud11Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        solicitud11Op.setOption("outputName","SolicitudResponse");
        solicitud11Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        solicitud11Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "SolicitudRequest"));
        solicitud11Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return solicitud11Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _solicitudToken12Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc solicitudToken12Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codContratoEnlace"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCuentaCargo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codClienteEmpresa"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[4].setOption("partName","string");
        _params0[5].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[5].setOption("partName","string");
        _params0[6].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[6].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "solicitudTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        solicitudToken12Op = new com.ibm.ws.webservices.engine.description.OperationDesc("solicitudToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "solicitudToken"), _params0, _returnDesc0, _faults0, null);
        solicitudToken12Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        solicitudToken12Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        solicitudToken12Op.setOption("inputName","solicitudTokenRequest");
        solicitudToken12Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "solicitudTokenResponse"));
        solicitudToken12Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        solicitudToken12Op.setOption("buildNum","cf160638.12");
        solicitudToken12Op.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        solicitudToken12Op.setOption("outputName","solicitudTokenResponse");
        solicitudToken12Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        solicitudToken12Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "solicitudTokenRequest"));
        solicitudToken12Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return solicitudToken12Op;

    }


    private static void initTypeMappings() {
        typeMappings = new java.util.HashMap();
        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"),
                         java.lang.String[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOfArrayOf_xsd_string"),
                         java.lang.String[][].class);

        typeMappings = java.util.Collections.unmodifiableMap(typeMappings);
    }

    public java.util.Map getTypeMappings() {
        return typeMappings;
    }

    public Class getJavaType(javax.xml.namespace.QName xmlName) {
        return (Class) typeMappings.get(xmlName);
    }

    public java.util.Map getOperationDescriptions(String portName) {
        return (java.util.Map) operationDescriptions.get(portName);
    }

    public java.util.List getOperationDescriptions(String portName, String operationName) {
        java.util.Map map = (java.util.Map) operationDescriptions.get(portName);
        if (map != null) {
            return (java.util.List) map.get(operationName);
        }
        return null;
    }

}