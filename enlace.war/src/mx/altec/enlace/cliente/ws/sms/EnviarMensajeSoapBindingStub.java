/**
 * EnviarMensajeSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.sms;

import mx.altec.enlace.utilerias.Global;

public class EnviarMensajeSoapBindingStub extends com.ibm.ws.webservices.engine.client.Stub implements mx.altec.enlace.cliente.ws.sms.EnviarMensaje {
    public EnviarMensajeSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws com.ibm.ws.webservices.engine.WebServicesFault {
        if (service == null) {
            super.service = new com.ibm.ws.webservices.engine.client.Service();
        }
        else {
            super.service = service;
        }
        super.engine = ((com.ibm.ws.webservices.engine.client.Service) super.service).getEngine();
        initTypeMapping();
        super.cachedEndpoint = endpointURL;
        super.connection = ((com.ibm.ws.webservices.engine.client.Service) super.service).getConnection(endpointURL);
        super.messageContexts = new com.ibm.ws.webservices.engine.MessageContext[1];
        this.setTimeout(Global.TIMEOUT_WS);
    }

    private void initTypeMapping() {
        javax.xml.rpc.encoding.TypeMapping tm = super.getTypeMapping(com.ibm.ws.webservices.engine.Constants.URI_SOAP11_ENC);
        java.lang.Class javaType = null;
        javax.xml.namespace.QName xmlType = null;
        javax.xml.namespace.QName compQName = null;
        javax.xml.namespace.QName compTypeQName = null;
        com.ibm.ws.webservices.engine.encoding.SerializerFactory sf = null;
        com.ibm.ws.webservices.engine.encoding.DeserializerFactory df = null;
    }

    private static final com.ibm.ws.webservices.engine.description.OperationDesc _enviarMensajeOperation0;
    static {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "destino"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "claveProveedor"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "numeroCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "cveMensaje"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "fecha"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "mensaje"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partName","string");
        _params0[0].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[1].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[2].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[3].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[4].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[5].setOption("partName","string");
        _params0[5].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "enviarMensajeReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partName","string");
        _returnDesc0.setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _enviarMensajeOperation0 = new com.ibm.ws.webservices.engine.description.OperationDesc("enviarMensaje", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://msgEnvio.pampa.santander.com", "enviarMensaje"), _params0, _returnDesc0, _faults0, "");
        _enviarMensajeOperation0.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "enviarMensajeRequest"));
        _enviarMensajeOperation0.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _enviarMensajeOperation0.setOption("outputName","enviarMensajeResponse");
        _enviarMensajeOperation0.setOption("targetNamespace",Global.URL_SERVICIO_SMS);
        _enviarMensajeOperation0.setOption("buildNum","cf10631.06");
        _enviarMensajeOperation0.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "EnviarMensajeService"));
        _enviarMensajeOperation0.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "enviarMensajeResponse"));
        _enviarMensajeOperation0.setOption("inputName","enviarMensajeRequest");
        _enviarMensajeOperation0.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _enviarMensajeOperation0.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "EnviarMensaje"));
        _enviarMensajeOperation0.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _enviarMensajeOperation0.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
    }

    private int _enviarMensajeIndex0 = 0;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getenviarMensajeInvoke0(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_enviarMensajeIndex0];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(EnviarMensajeSoapBindingStub._enviarMensajeOperation0);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_enviarMensajeIndex0] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String enviarMensaje(java.lang.String destino, java.lang.String claveProveedor, java.lang.String numeroCliente, java.lang.String cveMensaje, java.lang.String fecha, java.lang.String mensaje) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getenviarMensajeInvoke0(new java.lang.Object[] {destino, claveProveedor, numeroCliente, cveMensaje, fecha, mensaje}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        } 
        try {
            return (java.lang.String) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String.class);
        }
    }

}
