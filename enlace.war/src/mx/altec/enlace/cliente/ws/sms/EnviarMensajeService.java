/**
 * EnviarMensajeService.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.sms;

public interface EnviarMensajeService extends javax.xml.rpc.Service {
    public mx.altec.enlace.cliente.ws.sms.EnviarMensaje getEnviarMensaje() throws javax.xml.rpc.ServiceException;

    public java.lang.String getEnviarMensajeAddress();

    public mx.altec.enlace.cliente.ws.sms.EnviarMensaje getEnviarMensaje(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
