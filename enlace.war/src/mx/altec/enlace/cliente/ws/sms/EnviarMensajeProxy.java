package mx.altec.enlace.cliente.ws.sms;

public class EnviarMensajeProxy implements mx.altec.enlace.cliente.ws.sms.EnviarMensaje {
  private boolean _useJNDI = true;
  private String _endpoint = null;
  private mx.altec.enlace.cliente.ws.sms.EnviarMensaje __enviarMensaje = null;
  
  public EnviarMensajeProxy() {
    _initEnviarMensajeProxy();
  }
  
  private void _initEnviarMensajeProxy() {
  
  if (_useJNDI) {
    try{
      javax.naming.InitialContext ctx = new javax.naming.InitialContext();
      __enviarMensaje = ((mx.altec.enlace.cliente.ws.sms.EnviarMensajeService)ctx.lookup("java:comp/env/service/EnviarMensajeService")).getEnviarMensaje();
      }
    catch (javax.naming.NamingException namingException) {}
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  if (__enviarMensaje == null) {
    try{
      __enviarMensaje = (new mx.altec.enlace.cliente.ws.sms.EnviarMensajeServiceLocator()).getEnviarMensaje();
      }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  if (__enviarMensaje != null) {
    if (_endpoint != null)
      ((javax.xml.rpc.Stub)__enviarMensaje)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    else
      _endpoint = (String)((javax.xml.rpc.Stub)__enviarMensaje)._getProperty("javax.xml.rpc.service.endpoint.address");
  }
  
}


public void useJNDI(boolean useJNDI) {
  _useJNDI = useJNDI;
  __enviarMensaje = null;
  
}

public String getEndpoint() {
  return _endpoint;
}

public void setEndpoint(String endpoint) {
  _endpoint = endpoint;
  if (__enviarMensaje != null)
    ((javax.xml.rpc.Stub)__enviarMensaje)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
  
}

public mx.altec.enlace.cliente.ws.sms.EnviarMensaje getEnviarMensaje() {
  if (__enviarMensaje == null)
    _initEnviarMensajeProxy();
  return __enviarMensaje;
}

public java.lang.String enviarMensaje(java.lang.String destino, java.lang.String claveProveedor, java.lang.String numeroCliente, java.lang.String cveMensaje, java.lang.String fecha, java.lang.String mensaje) throws java.rmi.RemoteException{
  if (__enviarMensaje == null)
    _initEnviarMensajeProxy();
  return __enviarMensaje.enviarMensaje(destino, claveProveedor, numeroCliente, cveMensaje, fecha, mensaje);
}


}