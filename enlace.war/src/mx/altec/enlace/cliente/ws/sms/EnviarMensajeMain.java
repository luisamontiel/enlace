package mx.altec.enlace.cliente.ws.sms;

import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSProxy;
import mx.altec.enlace.utilerias.EIGlobal;

public class EnviarMensajeMain {
	
	public static void main(String args[]) {
		
		EnviarMensajeProxy enviarMensajeProxy = new EnviarMensajeProxy();
		//EnviarMensajeServiceLocator serviceLocator = new EnviarMensajeServiceLocator();
		//EnviarMensaje enviarMensaje = null;
		String resultado = "";
		
		try {
			resultado = enviarMensajeProxy.enviarMensaje("5516871211", "TELCEL", "001", "001", "01-06-2010 10:35:10", "Prueba");
			
			//enviarMensaje = serviceLocator.getEnviarMensaje();
			//resultado = enviarMensaje.enviarMensaje("5516871211", "TELCEL", "001", "001", "01-06-2010 10:35:10", "Prueba");
			
		} catch (RemoteException e) {
			System.out.println("Excepcion al recuperar el metodo del web service ->" + e);
			System.out.println("Causa ->" + e.getCause());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Excepcion ->" + e);
			System.out.println("Causa ->" + e.getCause());
		}
		
		System.out.println("resultado ->" + resultado);
	}

}
