/*
* Nombre: BitaTransacBean.java
* Modifico: GRE VSWF
* Fecha:    25/Abril/2008
  Descripcion: Se agrego la propiedad para el beneficiario.
*/

package mx.altec.enlace.bita;

import java.util.Date;

public class BitaTransacBean extends BitaBean {


	private Date fechaProgramada;
	private String tipoMoneda;
	private long numTit;
	private double importe;
	private double tipoCambio;
	private String idErr;

	private String bancoDest;
	private String contrato;
	private String cctaOrig;
	private String cctaDest;
	private Date fechaAplicacion;
	private String estatus;
	private String nombreArchivo;
	private String beneficiario;

	public String getBeneficiario(){
		return beneficiario;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}
	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getBancoDest() {
		return bancoDest;
	}
	public void setBancoDest(String bancoDest) {
		this.bancoDest = bancoDest;
	}
	public String getCctaDest() {
		return cctaDest;
	}
	public void setCctaDest(String cctaDest) {
		this.cctaDest = cctaDest;
	}
	public String getCctaOrig() {
		return cctaOrig;
	}
	public void setCctaOrig(String cctaOrig) {
		this.cctaOrig = cctaOrig;
	}

	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public Date getFechaProgramada() {
		return fechaProgramada;
	}
	public void setFechaProgramada(Date fechaProgramada) {
		this.fechaProgramada = fechaProgramada;
	}
	public String getIdErr() {
		return idErr;
	}
	public void setIdErr(String idErr) {
		this.idErr = idErr;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public long getNumTit() {
		return numTit;
	}
	public void setNumTit(long numTit) {
		this.numTit = numTit;
	}
	public double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setBeneficiario (String beneficiario){
		this.beneficiario = beneficiario;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

}
