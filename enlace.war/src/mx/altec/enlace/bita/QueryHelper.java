/*
* Nombre:      QueryHelper.java
* Modifico:    GRE VSWF
* Fecha:       25/Abril/2008
* Descripcion: Se agrego la propiedad para el beneficiario para la bitacora
*              de transacciones.
*/
package mx.altec.enlace.bita;

import java.util.Collection;
import java.util.Iterator;
import mx.altec.enlace.servlets.BaseServlet;

import mx.altec.enlace.bita.Utilerias;


public class QueryHelper {

	public String createBulkQuery(Collection beans){
		StringBuffer sb = new StringBuffer("");

		if (beans.size()!=1){
		 sb = new StringBuffer("BEGIN \n");
		}

		Iterator i = beans.iterator();

		while (i.hasNext()){
			Object o = i.next();

			if (o instanceof BitaAdminBean) {
				BitaAdminBean aBean = (BitaAdminBean) o;
				sb.append(createAdminQuery(aBean));
			}
			if (o instanceof BitaTransacBean) {
				BitaTransacBean tBean = (BitaTransacBean) o;
				sb.append(createTransacQuery(tBean));
			}
			if (beans.size()!=1){
				sb.append("; \n");
			}else{

			}
		}

		if (beans.size()!=1){
			sb.append(" \n END;");
		}
			return sb.toString();
	}


	public String createAdminQuery(BitaAdminBean ba){
		StringBuffer sb = new StringBuffer("");

		sb.append("INSERT INTO EWEB_ADMIN_BITACORA (FOLIO_BIT, FOLIO_FLUJO, ID_FLUJO, NUM_BIT,  \n")
		.append("TIPO_OP, REFERENCIA, HORA, DIR_IP, ID_TOKEN, SERV_TRANS_TUX, CANAL, USR, ID_WEB, \n")
		.append("ID_SESION, CAMPO, TABLA, FECHA, DATO, DATO_NVO, NOMBRE_HOST_WEB, ID_TABLA, USR_OPER) \n ")
		.append("VALUES( ") .append(ba.getFolioBit())
		.append(", ") .append(ba.getFolioFlujo())
		.append(", '") .append(ba.getIdFlujo())
		.append("', '") .append(ba.getNumBit())
		.append("', '") .append(ba.getTipoOp())
		.append("', ") .append(ba.getReferencia())
		.append(", '") .append(ba.getHora())
		.append("', '") .append(ba.getDirIp())
		.append("','") .append(ba.getIdToken())
		.append("','") .append(ba.getServTransTux())
		.append("','") .append(ba.getCanal())
		.append("','") .append(BaseServlet.convierteUsr7a8(ba.getUsr()))
		.append("','") .append(ba.getIdWeb())
		.append("','") .append(ba.getIdSesion())
		.append("','") .append(ba.getCampo())
		.append("','") .append(ba.getTabla())
		.append("',TO_DATE('") .append(Utilerias.formatDateToString(ba.getFecha(), "yyyy-MM-dd"))
		.append("','yyyy-MM-dd')")
		.append(",'") .append(ba.getDato())
		.append("','") .append(ba.getDatoNvo())
		.append("','") .append(ba.getNombreHostWeb())
		.append("','") .append(ba.getIdTabla())
		.append("','") .append(BaseServlet.convierteUsr7a8(ba.getUsr()))
		.append("') \n");
		return sb.toString();
	}

	public String createTransacQuery(BitaTransacBean bt){
		StringBuffer sb = new StringBuffer("");
		sb.append("INSERT INTO EWEB_TRAN_BITACORA (FOLIO_BIT, FOLIO_FLUJO, ID_FLUJO, NUM_BIT, \n")
		.append("FECHA, FECHA_PROG, TIPO_MONEDA, \n")
		.append("NUM_TIT, REFERENCIA, IMPORTE, TIPO_CAMBIO, ID_ERR, HORA, DIR_IP, COD_CLIENTE, \n")
		.append("ID_TOKEN, BANCO_DEST, SERV_TRANS_TUX, CANAL, USR, CONTRATO, ID_WEB, ID_SESION, CCTA_ORIG, \n")
		.append("CCTA_DEST, NOMBRE_HOST_WEB, FECHA_DE_APLICACION, ESTATUS_DE_LA_OPERACION, NOMBRE_ARCH, BENEFICIARIO) \n ")
		.append(" VALUES (").append(bt.getFolioBit())
		.append(", ") .append(bt.getFolioFlujo()) .append(",")
		.append(Utilerias.entreComillas(bt.getIdFlujo())) .append(",")
		.append(Utilerias.entreComillas(bt.getNumBit())) .append(",")
		.append("TO_DATE('").append(Utilerias.formatDateToString(bt.getFecha(), "yyyy-MM-dd"))
		.append("','yyyy-MM-dd')")
		.append(",TO_DATE('") .append(Utilerias.formatDateToString(bt.getFechaProgramada(), "yyyy-MM-dd"))
		.append("','yyyy-MM-dd')") .append(",")
		.append(Utilerias.entreComillas(bt.getTipoMoneda())) .append(",")
		.append(bt.getNumTit())
		.append(",").append(bt.getReferencia())
		.append(",").append(bt.getImporte())
		.append(",").append(bt.getTipoCambio()) .append(",")
		.append(Utilerias.entreComillas(bt.getIdErr())) .append(",")
		.append(Utilerias.entreComillas(bt.getHora())) .append(",")

		.append(Utilerias.entreComillas(bt.getDirIp())) .append(",")
		.append(Utilerias.entreComillas(BaseServlet.convierteUsr7a8(bt.getCodCliente()))) .append(",")
		.append(Utilerias.entreComillas(bt.getIdToken())) .append(",")
		.append(Utilerias.entreComillas(bt.getBancoDest())) .append(",")

		.append(Utilerias.entreComillas(bt.getServTransTux())) .append(",")
		.append(Utilerias.entreComillas(bt.getCanal())) .append(",")
		.append(Utilerias.entreComillas(BaseServlet.convierteUsr7a8(bt.getUsr()))) .append(",")
		.append(Utilerias.entreComillas(bt.getContrato())) .append(",")
		.append(Utilerias.entreComillas(bt.getIdWeb())) .append(",")
		.append(Utilerias.entreComillas(bt.getIdSesion())) .append(",")
		.append(Utilerias.entreComillas(bt.getCctaOrig())) .append(",")
		.append(Utilerias.entreComillas(bt.getCctaDest())) .append(",")
		.append(Utilerias.entreComillas(bt.getNombreHostWeb())) .append(",")



		.append("TO_DATE('").append(Utilerias.formatDateToString(bt.getFechaAplicacion(), "yyyy-MM-dd"))
		.append("','yyyy-MM-dd')") .append(",")
		.append(Utilerias.entreComillas(bt.getEstatus())) .append(",")
		.append(Utilerias.entreComillas(bt.getNombreArchivo())).append(",")
		.append(Utilerias.entreComillas(bt.getBeneficiario()))
		.append(")");
		return sb.toString();
	}
	public String createTCTBitacora(BitaTCTBean b)
	{
		StringBuffer bf = new StringBuffer();
		bf.append("INSERT INTO TCT_BITACORA ( REFERENCIA, U_VERSION, NUM_CUENTA, FECHA, HORA, TIPO_OPERACION, IMPORTE, NOTITULOS, CUENTA_ORIGEN,")
		.append("CUENTA_DESTINO_FONDO, CUENTA_ENLACE, USUARIO, COD_ERROR, TIPO_CTA_DESTINO, PLAZA_ORIGEN, PLAZA_DESTINO, PLAZA_ENLACE, RUTA_ORIGEN,")
		.append("RUTA_DESTINO, RUTA_ENLACE, MEDIO_ENTREGA, OPERADOR, PROTECCION1, PROTECCION2,PROTECCION3, DISPOSITIVO1, DISPOSITIVO2,CONCEPTO)")
		.append("VALUES (")
		.append(b.getReferencia()).append(",")
		.append(Utilerias.entreComillas(b.getUVersion())).append(",")
		.append(Utilerias.entreComillas(b.getNumCuenta())).append(",")
		.append("TO_DATE('").append(Utilerias.formatDateToString(b.getFecha(), "yyyy-MM-dd")).append("','yyyy-MM-dd'),")
		.append(b.getHora()).append(",")
		.append(Utilerias.entreComillas(b.getTipoOperacion())).append(",")
		.append(b.getImporte()).append(",")
		.append(b.getNoTitulos()).append(",")
		.append(Utilerias.entreComillas(b.getCuentaOrigen())).append(",")
		.append(Utilerias.entreComillas(b.getCuentaDestinoFondo())).append(",")
		.append(Utilerias.entreComillas(b.getCuentaEnlace())).append(",")
		.append(Utilerias.entreComillas(BaseServlet.convierteUsr7a8(b.getUsuario()))).append(",")
		.append(Utilerias.entreComillas(b.getCodError())).append(",")
		.append(Utilerias.entreComillas(b.getTipoCtaDestino())).append(",")
		.append(Utilerias.entreComillas(b.getPlazaOrigen())).append(",")
		.append(Utilerias.entreComillas(b.getPlazaDestino())).append(",")
		.append(Utilerias.entreComillas(b.getPlazaEnlace())).append(",")
		.append(b.getRutaOrigen()).append(",")
		.append(b.getRutaDestino()).append(",")
		.append(b.getRutaEnlace()).append(",")
		.append(Utilerias.entreComillas(b.getMedioEntrega())).append(",")
		.append(Utilerias.entreComillas(BaseServlet.convierteUsr7a8(b.getOperador()))).append(",")
		.append(b.getProteccion1()).append(",")
		.append(b.getProteccion2()).append(",")
		.append(b.getProteccion3()).append(",")
		.append(Utilerias.entreComillas(b.getDispositivo1())).append(",")
		.append(Utilerias.entreComillas(b.getDispositivo2())).append(",")
		.append(Utilerias.entreComillas(b.getConcepto())).append(")");

		/*String sql ="INSERT INTO TCT_BITACORA ( " +
				"REFERENCIA, U_VERSION, NUM_CUENTA," +
				"FECHA, HORA, TIPO_OPERACION," +
				"IMPORTE, NOTITULOS, CUENTA_ORIGEN," +
				"CUENTA_DESTINO_FONDO, CUENTA_ENLACE, USUARIO," +
				"COD_ERROR, TIPO_CTA_DESTINO, PLAZA_ORIGEN," +
				"PLAZA_DESTINO, PLAZA_ENLACE, RUTA_ORIGEN," +
				"RUTA_DESTINO, RUTA_ENLACE, MEDIO_ENTREGA," +
				"OPERADOR, PROTECCION1, PROTECCION2," +
				"PROTECCION3, DISPOSITIVO1, DISPOSITIVO2,CONCEPTO)" +
				"VALUES (" + b.getReferencia()+ " , , , , , , , , ," +
				" , , , , , , , , , , , ," +
				" , , , , , , )";*/
		return bf.toString();
	}
}