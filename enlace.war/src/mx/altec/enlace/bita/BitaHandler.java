package mx.altec.enlace.bita;

import java.sql.Connection;
import java.sql.SQLException;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

public class BitaHandler {


	/** Atributo privado y estatico para almacenar la instancia de la clase. */
	private static BitaHandler instance = new BitaHandler();

	/**
	 * Metodo para obtener la instancia de la clase.
	 * 
	 * @return BitaHandler, instancia de la clase.
	 */
	public static BitaHandler getInstance(){
		return instance;
	}

	/**
	 * Metodo para realizar la insercion en base de datos de las pistas de
	 * auditoria de acuerdo al parametro enviado.
	 * 
	 * @param beanBitacora : bean con los datos necesarios para realizar el guardado. 
	 * @return boolean, indicando en true que el guardado se realizo.
	 * @throws SQLException excepcion al ejecutar la sentencia sql.
	 * @throws Exception excepcion general en caso de algun error.
	 */
	public boolean insertBita(BitaBean beanBitacora) throws SQLException, Exception{
		if ("ON".equals(Global.USAR_BITACORAS) && beanBitacora != null){
			String query = "";
			Connection conexion = null;
			ConnectionHelper ch = null;

			if (beanBitacora instanceof BitaTransacBean){
				query = new QueryHelper().createTransacQuery((BitaTransacBean)beanBitacora);
			} else if(beanBitacora instanceof BitaAdminBean){
				query = new QueryHelper().createAdminQuery((BitaAdminBean)beanBitacora);
			}
			EIGlobal.mensajePorTrace("Query admin ->" + query, EIGlobal.NivelLog.DEBUG);
			if(query != null && query.length() > 0){
					try{
					ch = new ConnectionHelper(Global.DATASOURCE_ORACLE2);
					conexion = ch.getConnection();
					conexion.setAutoCommit(false);
					ch.ejecutaQuery(query);
					conexion.commit();
					conexion.setAutoCommit(true);

					}catch(SQLException se){
						conexion.rollback();
						EIGlobal.mensajePorTrace(new Formateador().formatea(se), EIGlobal.NivelLog.ERROR);
					}catch(Exception e){
						conexion.rollback();
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
					}finally{
						ch.cerrarConexion();
					}
				}
			}

		return true;
	}

	/**
	 * Metodo para realizar la insercion en base de datos de la bitacora de
	 * operaciones de acuerdo al parametro enviado.
	 * 
	 * @param b : Bean con los datos necesarios para realizar el guardado.
	 * @return boolean, indicando en true que el guardado se realizo.
	 * @throws SQLException excepcion al ejecutar la sentencia sql.
	 * @throws Exception excepcion general en caso de algun error.
	 */
	public  boolean insertBitaTCT(BitaTCTBean b) throws SQLException, Exception{
		
		
		if ("ON".equals(Global.USAR_BITACORAS)){
		
			String query = "";
			if (b!=null){

				if (b instanceof BitaTCTBean){
					BitaTCTBean bt = b;
					Connection c = null;
					ConnectionHelper ch = null;

					try{
						if(bt.getReferencia()>0){//La referencia debe ser mayor a cero
							query = new QueryHelper().createTCTBitacora(bt);
							ch = new ConnectionHelper(Global.DATASOURCE_ORACLE,1);
							c = ch.getConnection();
							c.setAutoCommit(false);
							ch.ejecutaQuery(query);
							c.commit();
							c.setAutoCommit(true);
						}
						else
						{
							EIGlobal.mensajePorTrace("La referencia["+bt.getReferencia()+"] no es correcta, No inserta en TCT_BITACORA ->", EIGlobal.NivelLog.ERROR);
						}

					}catch(SQLException se){
						c.rollback();
						EIGlobal.mensajePorTrace(new Formateador().formatea(se), EIGlobal.NivelLog.ERROR);
					}catch(Exception e){
						c.rollback();
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
					}finally{
						ch.cerrarConexion();
					}
				}

			}
		}
		return true;
	}


	/**
	 * Metodo para realizar el guardado de las pistas de auditoria 
	 * transaccionales.
	 * 
	 * @param b : Bean con los datos necesarios para realizar el guardado.
	 * @return boolean, indicando en true que el guardado se realizo. 
	 * @throws SQLException excepcion al ejecutar la sentencia sql.
	 * @throws Exception excepcion general en caso de algun error.
	 */
	public boolean insertBitaTransac(BitaTransacBean b) throws SQLException, Exception{
		return(insertBita(b));
	}

	/**
	 * Metodo para realizar el guardado de las pistas de auditoria 
	 * administrativas.
	 * 
	 * @param b : Bean con los datos necesarios para realizar el guardado.
	 * @return boolean, indicando en true que el guardado se realizo.
	 * @throws SQLException excepcion al ejecutar la sentencia sql.
	 * @throws Exception excepcion general en caso de algun error.
	 */
	public boolean insertBitaAdmin(BitaAdminBean b) throws SQLException, Exception{
		return(insertBita(b));
	}

}