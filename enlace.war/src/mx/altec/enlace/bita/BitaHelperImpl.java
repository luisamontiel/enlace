package mx.altec.enlace.bita;



import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.Global;



public class BitaHelperImpl implements BitaHelper {
	private BaseResource baseResource=null;
	private HttpServletRequest req=null;
	private HttpSession sesion=null;


	public BitaHelperImpl(HttpServletRequest req,
			BaseResource baseResource,
			HttpSession sesion){
		this.baseResource = baseResource;
		this.req = req;

		this.sesion = sesion;

	}




	public long retrieveFolioFlujo(){
		if(this.req.getSession().getAttribute(BitaConstants.SESS_FOLIO_FLUJO)!=null){
		return Long.parseLong((String)this.req.getSession().getAttribute(BitaConstants.SESS_FOLIO_FLUJO));
		}else{return 0;}
	}

	public String retrieveIdFlujo(){
		return (String)this.req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO);
	}

	public java.util.Date retrieveFecha(){
		return new java.util.Date();
	}
	public String retrieveHora(){
		return ObtenHora();
	}
	public String retrieveDirIp(){
		return BaseServlet.obtenerIPCliente(req);
		//return (req.getHeader("iv-remote-address")!= null && !"".equals(req.getHeader("iv-remote-address").trim()) ? req.getHeader("iv-remote-address") : req.getRemoteAddr());
	}
	public String retrieveIdToken(){
		return baseResource.getToken().getSerialNumber();
	}
	public String retrieveCanal(){
		return "Enlace";
	}
	public String retrieveUsr(){
		return baseResource.getUserID8();
	}
	public String retrieveIdWeb(){
		return Global.SERVER_INSTANCIA;

	}
	public String retrieveIdSesion(){
		return this.sesion.getId();
	}
	public String retrieveNombreHostWeb(){
		return req.getServerName();
	}


	public String retrieveTransacCodCliente(){
		return baseResource.getUserID8();
	}

	public long incrementaFolioFlujo(String idFlujo){

		long nextFolioFlujo = 0;
		try{

			ConnectionHelper ch = new ConnectionHelper(Global.DATASOURCE_ORACLE2);
			nextFolioFlujo = ch.nextFolioFlujo();
			ch.cerrarConexion();
			this.req.getSession().setAttribute(BitaConstants.SESS_FOLIO_FLUJO, String.valueOf(nextFolioFlujo));
			this.req.getSession().setAttribute(BitaConstants.SESS_ID_FLUJO, idFlujo);


		}catch (Exception e){
			e.printStackTrace();
		}

		return nextFolioFlujo;
	}


	public long incrementaFolioBitacora(){
		long nextFolioBit = 0;
		try{

			ConnectionHelper ch = new ConnectionHelper(Global.DATASOURCE_ORACLE2);
			nextFolioBit = ch.nextFolioBitacora();
			ch.cerrarConexion();

		}catch (Exception e){
			e.printStackTrace();
		}finally
		{

		}
		return nextFolioBit;
	}



	public BitaBean llenarBean(BitaBean b){
		b.setFolioBit(this.incrementaFolioBitacora());
		b.setFolioFlujo(this.retrieveFolioFlujo());
    	b.setIdFlujo(this.retrieveIdFlujo());
		b.setFecha(this.retrieveFecha());
		b.setHora(this.retrieveHora());
		b.setDirIp(this.retrieveDirIp());
		b.setCanal(this.retrieveCanal());
		b.setUsr(this.retrieveUsr());
		b.setIdWeb(this.retrieveIdWeb());
		b.setIdSesion(this.retrieveIdSesion());
		b.setNombreHostWeb(this.retrieveNombreHostWeb());

		if (b instanceof BitaTransacBean) {
			b.setCodCliente(this.retrieveTransacCodCliente());
		}else{
			b.setCodCliente(" ");
		}

		return b;
	}

	/**
	 * Regresa un Date con un String validado anteriormente y con un formato de forma "dd/MM/yyyy"
	 * @param String con el formato: dd/MM/yyyy
	 * @author Edgar Albedi
	 * @return la fecha formateada
	 */
	public static Date MdyToDate(String cadena) {
		if (cadena.length() == 10) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setLenient(false);
			Date fechaDate = null;
			try {
				fechaDate = sdf.parse(cadena);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return fechaDate;
		}

		return null;
	}


	public String getDBJNDIKey(){
		return Global.DATASOURCE_ORACLE2;
	}

	 private String ObtenHora () {
		 String hora = "";
		 String[] AM_PM = {"AM","PM"};
		 int minutos =0;
		 int segundos =0;
		 int horas =0;
		 String sminutos ="";
		 String ssegundos ="";

		 java.util.Date Hoy = new java.util.Date ();
		 GregorianCalendar Cal = new GregorianCalendar ();
		 Cal.setTime (Hoy);
		 if (AM_PM[Cal.get (Calendar.AM_PM)].equals ("AM")) {
		     segundos = Cal.get (Calendar.SECOND);
		     ssegundos =((segundos < 10 )?"0":"") + segundos;
			 minutos=Cal.get (Calendar.MINUTE);
		     sminutos=((minutos < 10) ? "0" : "") + minutos;
		     horas = (Cal.get (Calendar.HOUR)%12);
		     hora = ((horas < 10 ? "0" : "") + horas)+":"+ sminutos+ ":"+ssegundos;
		 }
		 else {
			 segundos = Cal.get (Calendar.SECOND);
		     ssegundos =((segundos < 10 )?"0":"") + segundos;
		     minutos=Cal.get (Calendar.MINUTE);
		     sminutos=((minutos < 10) ? "0" : "") + minutos;
		     hora =  ((Cal.get (Calendar.HOUR)%12)+12)+":"+ sminutos + ":"+ssegundos;

		 }
		 return hora;
	     }
	 private String ObtenHoraSinFormato () {
		 String hora = "";
		 String[] AM_PM = {"AM","PM"};
		 int minutos =0;
		 String sminutos ="";
		 java.util.Date Hoy = new java.util.Date ();
		 GregorianCalendar Cal = new GregorianCalendar ();
		 Cal.setTime (Hoy);
		 if (AM_PM[Cal.get (Calendar.AM_PM)].equals ("AM")) {
		     minutos=Cal.get (Calendar.MINUTE);
		     sminutos=((minutos < 10) ? "0" : "") + minutos;
		     hora =  ((Cal.get (Calendar.HOUR)%12))+ sminutos;
		 }
		 else {
		     minutos=Cal.get (Calendar.MINUTE);
		     sminutos=((minutos < 10) ? "0" : "") + minutos;
		     hora =  ((Cal.get (Calendar.HOUR)%12)+12)+ sminutos;
		 }
		 return hora;
	     }
	 /**
		 *
		 * Llena el bean de la bitacora de usuario
		 * @author Emmanuel Sanchez Castillo
		 * Fecha de creaci�n : Mar 25, 2008
		 * Modificaci�n:
		 * @param BitaTCTBean
		 */
	 	public BitaTCTBean llenarBeanTCT(BitaTCTBean b )
	 	{
	 	   try
	 	   {
	 		//b.setReferencia(-1);
	 		b.setUVersion("!");
	 		b.setNumCuenta(baseResource.getContractNumber());
	 		b.setFecha(new Date());
	 		b.setHora(ObtenHoraSinFormato());
	 		//b.setTipoOperacion(BitaConstants.ER_REDIR_SALTO);
	 		b.setImporte(0);
	 		b.setNoTitulos(0);
	 		b.setCuentaOrigen("0");
	 		b.setCuentaDestinoFondo("0");
	 		b.setCuentaEnlace("0");
	 		b.setUsuario(baseResource.getUserID8());
	 		b.setCodError("TCT_0000");
	 		b.setTipoCtaDestino("0");
	 		b.setPlazaOrigen("0");
	 		b.setPlazaDestino("0");
	 		b.setPlazaEnlace("0");
	 		b.setRutaOrigen(0);
	 		b.setRutaDestino(0);
	 		b.setRutaEnlace(0);
	 		b.setMedioEntrega("EWEB");
	 		b.setOperador(baseResource.getUserID8());
	 		b.setProteccion1(0);
	 		b.setProteccion2(0);
	 		b.setProteccion3(0);
	 		b.setDispositivo1("0");
	 		b.setDispositivo2("0");
	 		//b.setConcepto("Cierre Sesi�n Redir");
	 	   }catch(Exception e)
	 	   {
	 		   e.printStackTrace();
	 	   }
	 	   return b;
	 	}

}