package mx.altec.enlace.filtros;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.beans.AdminHorariosBean;
import mx.altec.enlace.bo.AdminHorariosBO;

public class OperatingScheduleFilter extends BaseServlet implements Filter {

	/** El objeto serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** Mapa que contiene los codigos que se deben de utilizar para los servlets indicados. */
	private final transient Map clavesOperacion;

	/**
	 * Constructor
	 */
	public OperatingScheduleFilter(){
		clavesOperacion = new HashMap();
		clavesOperacion.put("PagoMicrositioRef", "PMRF");
	}

	  /**
	 * @param filterConfig FilterConfig
	 * @throws ServletException ex
	 */
	  public void init(FilterConfig filterConfig)
		throws ServletException
	  {
		  EIGlobal.mensajePorTrace("***OperatingScheduleFilter :: initialized ", EIGlobal.NivelLog.INFO);
	  }

	  /**
	   * Metodo destroy.
	   */
	  public void destroy()
	  {
	  }

  /**
	* Intercepta las peticiones de los usuarios y valida que las operaciones solicitadas se encuentren en horacio valido.
	* @param request Request
	* @param response Response
	* @param filterChain FilterChain
	* @throws ServletException ex
	* @throws IOException ex
	*/
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException
  {
	  	EIGlobal.mensajePorTrace("***OperatingScheduleFilter :: doFilter()", EIGlobal.NivelLog.INFO);

		HttpServletRequest reqHttp = (HttpServletRequest)request;
		HttpServletResponse resHttp = (HttpServletResponse)response;
		String servletInvoca = reqHttp.getServletPath();
		String []tokens = servletInvoca.split("/");
		servletInvoca = tokens[tokens.length - 1];
		String idOperacion = reqHttp.getParameter("flujo");
		/** Se valida si el nombre del servlet esta en la tabla EWEB_ADMIN_HORARIOS,
		 **     - Si no esta continua normal
		 **     - Si esta, valida el horario, en caso de ser correcto el horario
		 **       continua normal, en caso contrario mandara pantalla de fuera de horario	*/
		EIGlobal.mensajePorTrace("OperatingScheduleFilter :: doFilter() :: "
				+ "Se validara horario para servlet: [" + servletInvoca
				+ "], idFlujo: [" + idOperacion + "]", EIGlobal.NivelLog.INFO);
		AdminHorariosBean beanValidaHorario = new AdminHorariosBean();
		if ((idOperacion != null) || (clavesOperacion.get(servletInvoca) != null)){
			beanValidaHorario.setIdFunc(clavesOperacion.get(servletInvoca) != null ? (String)clavesOperacion.get(servletInvoca) : idOperacion);
			EIGlobal.mensajePorTrace("OperatingScheduleFilter :: doFilter() :: El ID de Operacion utilizado para validar el horario es: "
				+ beanValidaHorario.getIdFunc(), EIGlobal.NivelLog.INFO);
		}else{
			beanValidaHorario.setNombreServlet(servletInvoca);
			EIGlobal.mensajePorTrace("OperatingScheduleFilter :: doFilter() :: En esta peticion no se incluye un ID de operacion.", EIGlobal.NivelLog.INFO);
		}

		final AdminHorariosBO bo = new AdminHorariosBO();
		String horaActual = ObtenHora().substring(0, 5);
		String[] horaSistema = horaActual.split(":");
		StringBuffer hora = new StringBuffer();
		hora.append(horaSistema[0].trim());
		hora.append(horaSistema[1].trim());
		EIGlobal.mensajePorTrace("OperatingScheduleFilter :: doFilter() :: horaActual: " + horaActual + " con formato[" + hora.toString()
			+ "]", EIGlobal.NivelLog.INFO);
		beanValidaHorario.setHoraActual(hora.toString());

		if("ETIN".equals(beanValidaHorario.getIdFunc())
				|| "MDI_Interbancario".equals(beanValidaHorario.getIdFunc())){
			EIGlobal.mensajePorTrace("OperatingScheduleFilter :: doFilter() :: La operacion sin validacion de horario es: "
									+ beanValidaHorario.getIdFunc(), EIGlobal.NivelLog.INFO);
			beanValidaHorario.setHorarioValido(true);
		}else{

			beanValidaHorario = bo.validarHorario(beanValidaHorario);
		}

		boolean despliegaPaginaError = !beanValidaHorario.isHorarioValido();

		if(despliegaPaginaError) {
			EIGlobal.mensajePorTrace("***OperatingScheduleFilter :: doFilter() :: " + " Horario no valido para peticion de servlet " + servletInvoca, EIGlobal.NivelLog.INFO);
			resHttp.sendRedirect("operacionFueraHorario");
		} else {
			EIGlobal.mensajePorTrace("***OperatingScheduleFilter :: doFilter() :: " + " Se procede con el flujo normal de la operacion.", EIGlobal.NivelLog.INFO);
			filterChain.doFilter(request, response);
		}
  }

}