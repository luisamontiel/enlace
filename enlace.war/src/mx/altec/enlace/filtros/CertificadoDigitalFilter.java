package mx.altec.enlace.filtros;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.CertificadoDigitalBO;
import mx.altec.enlace.bo.CertificadoDigitalBOImpl;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.RelacionFlujos;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.mx.isban.crypto.manager.BusinessDecrypter;

/***
 * **Servlet Filter implementation class CertificadoDigitalFilter 
 */
public class CertificadoDigitalFilter implements Filter  {

	
	/*** MMC_ALTA ****/
	static final String MMC_ALTA = "MMC_Alta";
	/**cadena fileName***/
	static final String COMTO_PROVEEDOR = "coMtoProveedor";
	/**cadena fileName***/
	static final String COPAGOS = "coPagos";
	/**cadena fileName***/
	static final String NOMINTERREG = "CatNomInterbRegistro";
	/**cadena fileName***/
	static final String PDREGISTRO = "pdRegistro";
	/**cadena fileName***/
	static final String PREVALIDADOR_SERVLET= "PrevalidadorServlet";
	/**cadena fileName***/
	static final String MDI_ORDEN_PAGO_OCURRE= "MDI_Orden_Pago_Ocurre";
	/**canena mensajeCifrado***/
	static final String TRANSFERENCIA = "transferencia";
	/**canena mensajeCifrado**/
	static final String SERVLET_CIFRADO = "ServletInvocaCifrado";
	/**canena de Error mensajeCifrado**/
	static final String MENSAJE_ERRROR = "mensajeCifradoError";
	/**canena de opcion****/
	static final String OPCION = "opcion";
	/**cadena fileName***/
	static final String CATALOGO_NOMINA_EMPL = "CatalogoNominaEmpl";
	/**
	 *Serial generado
	 */
	private static final long serialVersionUID = 7655854103690069111L;
	/**cadena fileName***/
	private static final String CADENA_FILE_NAME = "fileName";
	/**canena mensajeCifrado**/
	private static final String CADENA_MESAJECIFRADO = "mensajeCifrado";

	/** {@inheritDoc} */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/** {@inheritDoc} */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
			HttpServletRequest reqHttp = null;
			BaseResource session = null;
			CertificadoDigitalWrapper requestWrapper = null;
			
		try{ 
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: " + this.getClass().getName(), EIGlobal.NivelLog.INFO);

			reqHttp = (HttpServletRequest)request;
			session = (BaseResource) reqHttp.getSession().getAttribute("session");
			requestWrapper = new CertificadoDigitalWrapper(reqHttp);

			String nombreCifrado = "", nombreFile = "", servletInvoca = reqHttp.getServletPath();

			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: -->Nombre request:" + servletInvoca, EIGlobal.NivelLog.INFO);
			nombreFile = servletInvoca.contains(MMC_ALTA) || servletInvoca.contains(COMTO_PROVEEDOR ) || servletInvoca.contains(COPAGOS ) || servletInvoca.contains(NOMINTERREG )
			|| servletInvoca.contains(PDREGISTRO ) ? "fileNameUpload" : CADENA_FILE_NAME;

			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: -->FILE NAME:" + nombreFile, EIGlobal.NivelLog.INFO);
			request.setAttribute(CADENA_MESAJECIFRADO, "");
			
			if(ServletFileUpload.isMultipartContent(reqHttp)) {
				evaluaFilter(request, response, chain, reqHttp, session,
						requestWrapper, nombreCifrado, servletInvoca);

			} else {
				
				String mensaje1 = requestWrapper.getSession().getAttribute(MENSAJE_ERRROR) != null 
				? requestWrapper.getSession().getAttribute(MENSAJE_ERRROR).toString() : "";
				
				if(mensaje1 != null   && "true".equals(mensaje1)) {
						requestWrapper.getSession().removeAttribute(MENSAJE_ERRROR);
						String mensaje01 = requestWrapper.getSession().getAttribute("CODIGO_ERROR") != null 
						? requestWrapper.getSession().getAttribute("CODIGO_ERROR").toString() : "";
						requestWrapper.getSession().removeAttribute("CODIGO_ERROR");
						if(mensaje01 != null && ( "ER0001".equals(mensaje01) || "ER0002".equals(mensaje01))){
							String cadenaMensaje = "ER0001".equals(mensaje01) ? "El Certificado del Cliente no existe" : "El certificado del Canal no existe";
							requestWrapper.getSession().removeAttribute(MENSAJE_ERRROR);
							requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "0|".concat(cadenaMensaje));
						}else if(mensaje01 != null && ( "ER0013".equals(mensaje01) || "ER0011".equals(mensaje01))){
							String cadenaMensaje = "ER0013".equals(mensaje01) ? "Estimado cliente le informamos que la importaci�n de su archivo, no se pudo ejecutar, debido a que el certificado con el que  se cifro se encuentra revocado en Enlace" : "Estimado cliente le informamos que la importaci�n de su archivo, no se pudo ejecutar, debido a que el certificado con el que  se cifro se encuentra revocado ante la CA (Entidad Certificadora)";
							requestWrapper.getSession().removeAttribute(MENSAJE_ERRROR);
							requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "0|".concat(cadenaMensaje));
						}else if(mensaje01 != null && "ER0012".equals(mensaje01)){
							String cadenaMensaje = "Estimado cliente le informamos que su certificado esta Cancelado";
							requestWrapper.getSession().removeAttribute(MENSAJE_ERRROR);
							requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "0|".concat(cadenaMensaje));
						}else{
							//requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "0|No se pudo realizar la operaci�n de descifrado, el contenido del archivo no tiene un formato de cifrado valido aseg�rese de realizar el cifrado de su archivo con la Herramienta de Cifrado/Descifrado de Santander de la secci�n Administraci�n y Control>Certificado Digital. Para m�s informaci�n cont�ctenos al 01 800 509 5000");
							requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "0|No se pudo descifrar, ya que no tiene un formato v�lido de cifrado. Favor de cifrar con la herramienta proporcionada. (Secci�n de Administraci�n y Control). Para m�s informes cont�ctenos al 01 80 509 5000");
						}
						chain.doFilter(request, response);
						
				} else if ( validarServicio(request, requestWrapper)){
					chain.doFilter(request, response);
				   }
					else {
					chain.doFilter(request, response);
				}
			}
		}
		catch(Exception e){
			requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "0|No se puede realizar la operaci�n en este momento. Favor de intentarlo m�s tarde.");
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			chain.doFilter(request, response);
		}
	}

	/** {@inheritDoc} */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Ejecuta el descifrado del archivo en caso de contener
	 * @param request : request
	 * @param response : response
	 * @param chain : chain filterChain
	 * @param reqHttp : reqHttp
	 * @param session : session
	 * @param requestWrapper : requestWrapper
	 * @param nombreCifrado : nombreCifrado
	 * @param servletInvoca : servletInvoca
	 * @throws IOException : Excepcion generada
	 * @throws ServletException : Excepcion generada
	 */
	@SuppressWarnings("unchecked")
	private void evaluaFilter(ServletRequest request, ServletResponse response,
			FilterChain chain, HttpServletRequest reqHttp,
			BaseResource session, CertificadoDigitalWrapper requestWrapper,
			String nombreCifrado, String servletInvoca) throws IOException,
			ServletException {
		CertificadoDigitalUtilerias utilerias = new CertificadoDigitalUtilerias();
		if(!validarServicio(request, requestWrapper)) {
			chain.doFilter(request, response);
			return;
		}

		BaseServlet bs = new BaseServlet();
		HttpSession sess = requestWrapper.getSession();
		int referenciaBit = bs.obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		
		FileItem fileItem = obtieneInputStream(reqHttp);
		
		if(fileItem == null || fileItem.getContentType() == null || "".equals(fileItem.getName())  ) {
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> Archivo null"  , EIGlobal.NivelLog.INFO);
			chain.doFilter(request, response);
			return;
		}

		InputStream inStream = fileItem.getInputStream(), respuesta =  null;
		Map<String,Object> respMapa = null;
		try {
			
			BusinessDecrypter decryptor = new BusinessDecrypter();
			/** Se invoca el jar de Decifrado con los siguientes parametros:
			* Archivo Cifrado en objeto ImputStream
			* N�mero de Cliente - BUC
			* N�mero de contrato a 11 posiciones
			* Identificador del Canal, en este caso es una constante [ ENLA ]
			**/
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Ejecutando JAR CryptographyUtility::::: m�todo: decryptFile( [ " + session.getUserID8() + "] [ " + session.getContractNumber() + " ] [ ENLA ]" + ")" , EIGlobal.NivelLog.INFO);
			
			respMapa =  decryptor.decryptFile(inStream, session.getUserID8(),session.getContractNumber(), "ENLA");
			
			if(respMapa != null){
				if("OK0000".equals((String)respMapa.get("codeError"))){
					respuesta = (InputStream) respMapa.get("fileDecrypted");
					nombreCifrado = stream2file(respuesta, fileItem.getName(), servletInvoca);
				}else{
					EIGlobal.mensajePorTrace(">>>>>>>>>>> ERROR::::: " + respMapa.get("codeError") +" ::::"+  respMapa.get("message"), EIGlobal.NivelLog.INFO);
					reqHttp.getSession().setAttribute("CODIGO_ERROR", (String)respMapa.get("codeError"));
					bitacorizaProceso(requestWrapper, sess, referenciaBit,"0001",fileItem.getName(),"R");
					utilerias.controlArchivoInvalido(reqHttp, response, requestWrapper);
					return;
				}
			}else{
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro:::[ Ocurrio un error inesperado en CryptographyUtility ].... "  , EIGlobal.NivelLog.INFO);
				utilerias.controlArchivoInvalido(reqHttp, response, requestWrapper);
				bitacorizaProceso(requestWrapper, sess, referenciaBit,"0001",fileItem.getName(),"R");
				return;
			}
			
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro::: nombre completo nuevo::::: " + nombreCifrado , EIGlobal.NivelLog.INFO);
		} catch (ClassNotFoundException e) {
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro::: error : Ocurrio un error en el descifrado.... "  , EIGlobal.NivelLog.INFO);
			bitacorizaProceso(requestWrapper, sess, referenciaBit,"0001",fileItem.getName(),"R");
			utilerias.controlArchivoInvalido(reqHttp, response, requestWrapper);
			return;
		}finally{
			if(respuesta != null){
				inStream.close();
			}
			if(respuesta != null){
				respuesta.close();
			}
			
		}

		if(servletInvoca.contains(MMC_ALTA ) || servletInvoca.contains(COMTO_PROVEEDOR ) || servletInvoca.contains(COPAGOS ) || servletInvoca.contains(NOMINTERREG )
				|| servletInvoca.contains(PDREGISTRO )) {
			requestWrapper.addParameter("fileNameUpload", nombreCifrado);
		} else {
			requestWrapper.addParameter(CADENA_FILE_NAME, nombreCifrado);
		}

		if(servletInvoca.contains(TRANSFERENCIA ) || servletInvoca.contains("MDI_Interbancario" )
				|| servletInvoca.contains(MDI_ORDEN_PAGO_OCURRE ) || servletInvoca.contains(PREVALIDADOR_SERVLET ) || servletInvoca.contains("ImportNominaInter")) {

			HashMap<String,String> formParameters  = new HashMap<String, String>();
			
			formParameters = (HashMap<String,String>) reqHttp.getAttribute("formParameters");

			formParameters.remove(CADENA_FILE_NAME);
			formParameters.put(CADENA_FILE_NAME, nombreCifrado);

			requestWrapper.setAttribute("formParameters", formParameters);
			requestWrapper.getSession().setAttribute("ArchImporta", nombreCifrado);
		}

		bitacorizaProceso(requestWrapper, sess, referenciaBit,"0000",fileItem.getName(),"A");
		String opcion = reqHttp.getParameter(OPCION);
		if( "7".equals(opcion) || "10".equals(opcion) ) {
			requestWrapper.setAttribute("CAD_ERROR", true);
			requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "");
		}	
		chain.doFilter(requestWrapper, response);
	} 
	
	/**
	 * Metodo para copiar archivos 
	 * @param in : archvio a copiar
	 * @param nombreCifrado : nombre del archivo
	 * @param servletInvoca : servlet que entro al filtro
	 * @return archivo clonado
	 * @throws IOException : exception
	 */
	private  String stream2file (InputStream in, String nombreCifrado, String servletInvoca) throws IOException {
		File tempFile = null;
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro::: stream2file:::: nombreCifrado:::: " + nombreCifrado , EIGlobal.NivelLog.INFO);

		String nombre = "";

		if(servletInvoca.contains(MMC_ALTA) ||  servletInvoca.contains(COMTO_PROVEEDOR )  || servletInvoca.contains(COPAGOS ) || servletInvoca.contains(NOMINTERREG )
				|| servletInvoca.contains(PDREGISTRO )) {
			tempFile = new File( IEnlace.LOCAL_TMP_DIR + "/" +nombreCifrado);
			tempFile.deleteOnExit();

			String fileNameBk = nombreCifrado;
			String fileName = fileNameBk;

			for(int x = 1;tempFile.exists() && x <= 1000; x++) {	//para generar nombre de archivo distinto si existe
				fileName = x + fileNameBk;
				tempFile = new File( IEnlace.LOCAL_TMP_DIR + "/" + fileName);
				EIGlobal.mensajePorTrace(">>>>>>>>>>> El archivo Existe 01:::: fileName:::: " + fileName , EIGlobal.NivelLog.INFO);

			}

			nombre = IEnlace.LOCAL_TMP_DIR + "/" + fileName;
			EIGlobal.mensajePorTrace(">>>>>>>>>>>Nombre del Archivo Final 01:::: fileName:::: " + fileName , EIGlobal.NivelLog.INFO);

		}else {

			String fileNameBk = nombreCifrado;
			String fileName = fileNameBk;


			tempFile = new File(IEnlace.LOCAL_TMP_DIR +"/"+ fileName);

			for(int x = 1;tempFile.exists() && x <= 1000; x++) {	//para generar nombre de archivo distinto si existe
				fileName =  x + fileNameBk;
				tempFile = new File( IEnlace.LOCAL_TMP_DIR + "/" + fileName);
				EIGlobal.mensajePorTrace(">>>>>>>>>>> El archivo Existe 03:::: fileName:::: " + fileName , EIGlobal.NivelLog.INFO);

			}

			nombre = fileName;
			EIGlobal.mensajePorTrace(">>>>>>>>>>>Nombre del Archivo Final 02:::: fileName:::: " + fileName , EIGlobal.NivelLog.INFO);
		} 

		FileOutputStream out = new FileOutputStream(tempFile); 
		IOUtils.copy(in, out);
		if(out != null){
			out.close();
		}

		return nombre;
	}
	
	/**
	 * metodo para obtener el archivo del multipar
	 * @param request : httservletprequest
	 * @return FileItem
	 */
	@SuppressWarnings("unchecked")
	private FileItem obtieneInputStream (HttpServletRequest request) {

		List parametros = null;
		Iterator it;
		FileItem fileItem = null;
		FileItem fileArchivo = null;

		HashMap<String,String> formParameters  = null;

		ServletFileUpload sfp = new ServletFileUpload(new DiskFileItemFactory());
		
		try {
			parametros = sfp.parseRequest(request);
			formParameters = new HashMap<String,String>();
		} catch (FileUploadException e1) {
			EIGlobal.mensajePorTrace("\n >>>>>>>>>>>>>>>> Filtro Error::>" + e1 ,EIGlobal.NivelLog.DEBUG);
		}

		if(parametros == null) {
			return null;
		}

		it = parametros.iterator();
		while(it.hasNext())
		{

			fileItem = (FileItem)it.next();

			if(fileItem.isFormField())
			{
				EIGlobal.mensajePorTrace("\n >>>>>>>>>>>>>>>> Filtro campo<" + fileItem.getFieldName() + "> valor <" + fileItem.getString() + ">",EIGlobal.NivelLog.DEBUG);
				Object obj = null;
				obj = formParameters.get(fileItem.getFieldName());
				if (obj == null) {
					formParameters.put(fileItem.getFieldName(),fileItem.getString());
				}	
			}else if(fileItem.getContentType() != null) {
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro::: -------------------> ContentType :::  " + fileItem.getContentType(), EIGlobal.NivelLog.INFO);
				fileArchivo = fileItem;
			}
		}
		request.setAttribute("formParameters",formParameters);
		return fileArchivo;

	}


	/**
	 * Validacion de banderas para el servicio
	 * @param req : ServletRequest
	 * @param requestWrapper : CertificadoDigitalWrapper request de la peticion
	 * @return : true si tiene servicio de cifrado
	 */
	private boolean validarServicio(ServletRequest req, CertificadoDigitalWrapper requestWrapper ) {

//		BaseServlet baseServlet = new BaseServlet();
		boolean sevicioActivo = false, isServiceAlive = false, validarServicio = false;
		CertificadoDigitalBO certificadoBO = new CertificadoDigitalBOImpl();
		HttpServletRequest reqHttp = (HttpServletRequest)req;

		BaseResource session=(BaseResource) reqHttp.getSession().getAttribute("session");

		try {
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> contrato number:"+ session.getContractNumber() == null ? "No hay sesion activa " : session.getContractNumber(), EIGlobal.NivelLog.INFO);
			sevicioActivo = certificadoBO.servicioActivo();
			int idflujo = regresaIdFlujo(reqHttp);
			String servletInvoca = reqHttp.getServletPath();
			EIGlobal.mensajePorTrace(">>>>>>>>>>> SERVLETTT::: ------------------->  ::: "+servletInvoca , EIGlobal.NivelLog.INFO);
//			if(!baseServlet.verificaFacultad ( "IMPARCHCIFRADO" ,reqHttp)) {
//
//				return false;
//			}
			//se requiere para validar si tiene servicio y notificar al usuario
			isServiceAlive = certificadoBO.tieneServicioImportacion(session.getContractNumber(),idflujo);

			if(sevicioActivo) {
				if(isServiceAlive) {
					validarServicio = true;
					EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra validarServicio::: -------------------> ContentType :::  " , EIGlobal.NivelLog.INFO);
					if(servletInvoca.contains(CATALOGO_NOMINA_EMPL)) {
						String opcion = reqHttp.getParameter(OPCION);
						EIGlobal.mensajePorTrace(">>>>>>>>>>>OPCION::: -------------------> OPCION :::  " +opcion, EIGlobal.NivelLog.INFO);
						if( "7".equals(opcion) || "10".equals(opcion) ) {
							servletInvoca = servletInvoca.concat("Baja");
							EIGlobal.mensajePorTrace(">>>>>>>>>>> SERVLET::: ------------------->  ::: "+opcion , EIGlobal.NivelLog.INFO);
							requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "");
						} else if ("6".equals(opcion) || "9".equals(opcion)) {
							requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "1|Estimado cliente le informamos que cuenta con el servicio de cifrado, favor de importar su archivo de esta manera.");
							EIGlobal.mensajePorTrace(">>>>>>>>>>> opcion del CatalogoNominaEmpl::: ------------------->  ::: "+opcion , EIGlobal.NivelLog.INFO);
						}
						
					}else {
						requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "1|Estimado cliente le informamos que cuenta con el servicio de cifrado, favor de importar su archivo de esta manera.");
					}
				}
			}  else{
				if(isServiceAlive) {
					validarServicio = false;
					requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "1|Estimado cliente le informamos que por contingencia en el canal, se recibir� su archivo sin cifrar.");
				}
			}

			if(req.getContentType()==null) {
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> reqHttp.getContentType()==null"  , EIGlobal.NivelLog.INFO);
				return false;

			}else{
				requestWrapper.setAttribute(CADENA_MESAJECIFRADO, "");
			}

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra SQLException::: -------------------> ContentType :::  " , EIGlobal.NivelLog.INFO);
			req.setAttribute(CADENA_MESAJECIFRADO, "1|Estimado cliente le informamos que por contingencia en el canal, se recibir� su archivo sin cifrar.");

		}

		return validarServicio;

	}

	/**
	 * Obtiene el id del flujo
	 * @param req : ServletRequest
	 * @return id del flujo
	 */
	private int regresaIdFlujo(ServletRequest req){

		HttpServletRequest reqHttp = (HttpServletRequest)req;
		String servletInvoca = reqHttp.getServletPath();
		EIGlobal.mensajePorTrace(">>>>>>>>>>> servletInvoca: -->  :::: servletInvoca ::: " +  servletInvoca , EIGlobal.NivelLog.INFO);

		int idFlujo = 0;
		String flujoTranferencia = reqHttp.getSession().getAttribute(SERVLET_CIFRADO) != null 
			? reqHttp.getSession().getAttribute(SERVLET_CIFRADO).toString() : ""; 
		
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> servletInvoca: -->  :::: servletInvocaCifrado ::: " +  flujoTranferencia , EIGlobal.NivelLog.INFO);	

		if(servletInvoca.contains(TRANSFERENCIA)) {
			String trans = reqHttp.getParameter("trans");
			String ventana = reqHttp.getParameter("ventana");
			String divisa = reqHttp.getParameter("divisa");
			String cifrado = reqHttp.getParameter("cifrado");

			EIGlobal.mensajePorTrace(">>>>>>>>>>> servletInvoca: -->  :::: flujo transferenci ::: " + "cifrado::"+cifrado+ " trans::"+trans+" ventana::"+ventana+" divisa::"+divisa, EIGlobal.NivelLog.INFO);	

			if("0".equals(ventana) || "2".equals(cifrado)) {
				servletInvoca = servletInvoca.concat("Nac");
			} else if("4".equals(ventana) || "1".equals(cifrado)) {
				servletInvoca = servletInvoca.concat("Dol");
			} else if("3".equals(ventana) || "1".equals(ventana)) {
				servletInvoca = servletInvoca.concat("Int");
			} 

			reqHttp.getSession().setAttribute(SERVLET_CIFRADO, servletInvoca);
			

		} else if(servletInvoca.contains(CATALOGO_NOMINA_EMPL)) {
			String opcion = reqHttp.getParameter(OPCION);
			if("6".equals(opcion) || "7".equals(opcion) ) {
				servletInvoca = servletInvoca.concat("Baja");
			} else if("9".equals(opcion) || "10".equals(opcion)) {
				servletInvoca = servletInvoca.concat("Mod");
			}
			//reqHttp.getSession().setAttribute("ServletInvocaCifrado", servletInvoca);

		} else if(servletInvoca.contains(MDI_ORDEN_PAGO_OCURRE)){
			String personalidad = reqHttp.getParameter("personalidad");
			if("1".equals(personalidad)) {
				servletInvoca = servletInvoca.concat("Fis");
			} else if("2".equals(personalidad)) {
				servletInvoca = servletInvoca.concat("Mor");
			}
		}

		String [] arreglo = servletInvoca.split("\\/");
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> regresaIdFlujo :::: servletInvoca ::: " +  arreglo[2] , EIGlobal.NivelLog.INFO);
		String idRelacion = RelacionFlujos.getValue(arreglo[2]) != null ? RelacionFlujos.getValue(arreglo[2]) : "0000" ;
		idFlujo = Integer.parseInt(idRelacion.trim());
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> regresaIdFlujo :::: idFlujo ::: " +  idFlujo , EIGlobal.NivelLog.INFO);


		return idFlujo;

	}
	
	
	/**
	 * @param request : request
	 * @param sess : sess
	 * @param referencia : referencia de la operacion
	 * @throws ServletException 
	 * @throws IOException 
	 */
	private void bitacorizaProceso(HttpServletRequest request, HttpSession sess, int referencia,String codigoError,String nombreArchivo,String status) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> :::: bitacorizaProceso" , EIGlobal.NivelLog.INFO);
		CertificadoDigitalUtilerias utilerias = new CertificadoDigitalUtilerias();
		String idFlujoBitacora = utilerias.obtieneIdBitacora(request);
		String numBit = idFlujoBitacora.concat("01");
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> ::: bitacorizaProceso --> id bitacora ::: " + idFlujoBitacora , EIGlobal.NivelLog.INFO);
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			String idFlujoEntrada = (String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO);
			BaseResource session = (BaseResource) sess.getAttribute("session");
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			Date hoy = new Date();
			
			bh.incrementaFolioFlujo(idFlujoBitacora);
			
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setIdFlujo(idFlujoBitacora);
			bt.setNumBit(numBit);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}	
			bt.setReferencia(referencia);
			String codError=idFlujoBitacora + codigoError;
			bt.setIdErr(codError);
			bt.setIdToken(session.getToken().getSerialNumber());				
			bt.setFechaAplicacion(hoy);
			bt.setEstatus(status);
			bt.setNombreArchivo(nombreArchivo);
			
			request.getSession().setAttribute(BitaConstants.SESS_ID_FLUJO, idFlujoEntrada);
			

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("CertificadoDigitalFilter - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CertificadoDigitalFilter - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
			
			try {
 				BitaTCTBean beanTCT = new BitaTCTBean ();

 				beanTCT = bh.llenarBeanTCT(beanTCT);

				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion(idFlujoBitacora);
				
				beanTCT.setCodError(idFlujoBitacora.concat(codigoError));
				beanTCT.setUsuario(session.getUserID8());
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("CertificadoDigitalFilter - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("CertificadoDigitalFilter - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> ::: FIN ---> bitacorizaProceso" , EIGlobal.NivelLog.INFO);
	}

}