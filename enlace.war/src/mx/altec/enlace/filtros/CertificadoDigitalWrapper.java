package mx.altec.enlace.filtros;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import mx.altec.enlace.utilerias.EIGlobal;

public class CertificadoDigitalWrapper extends HttpServletRequestWrapper {
	
	/**CONSTANTE_UNCHECKED**/
	private static final String CONSTANTE_UNCHECKED = "unchecked";
	
	
	
	/***Hash de parametros**/
	@SuppressWarnings(CONSTANTE_UNCHECKED)
	private final transient  Map params;
	
	
	
	/**
	 * constructor
	 * @param request : HttpServletRequest
	 */
	@SuppressWarnings(CONSTANTE_UNCHECKED)
	public CertificadoDigitalWrapper(HttpServletRequest request) {
		
		super(request);
		params = new  HashMap(request.getParameterMap()); 
	}

	@Override
	public String getParameter(final String name) {
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> getParameteWrapper ----------->>>> dato buscado>>> " + name + "valor::: " + params.get(name), EIGlobal.NivelLog.INFO);
		
		try {
			return (String) params.get(name);
		} catch (ClassCastException e) {
			String [] arreglo = (String[]) params.get(name);
			EIGlobal.mensajePorTrace("getParameteWrapper ----------->>>> dato buscado> exception>> " + name + "valor::: " +arreglo[0], EIGlobal.NivelLog.INFO);
			return String.valueOf(arreglo[0]);
		}
	}
	

	/** Metodo para agregar parametros al request 
	 * @param name : nombre
	 * @param value : valor
	 */
	@SuppressWarnings(CONSTANTE_UNCHECKED)
	public void addParameter(String name, String value) {
		
		params.put(name, value);
		
		Iterator it = params.entrySet().iterator();

	    while (it.hasNext()) {
	        Map.Entry e = (Map.Entry)it.next();
	        EIGlobal.mensajePorTrace("-------------------- addParameter >>>>>> [ " + e.getKey() + " = " + e.getValue()+ " ]", EIGlobal.NivelLog.INFO);
	   }
	}
	
	/**
	 * Metodo para agregar parametros
	 * @param key : nombre
	 * @param values : valor
	 */
	@SuppressWarnings(CONSTANTE_UNCHECKED)
	public void setParameter(String key, String[] values){

		params.put(key, values);

	}


	
	@Override
	public void setAttribute(String name, Object o) {
		super.setAttribute(name, o);
	}
	
	
}
