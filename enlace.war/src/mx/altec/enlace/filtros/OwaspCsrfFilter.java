package mx.altec.enlace.filtros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;

/** 
 *   Isban M�xico
 *   Clase: OwaspCsrfFilter.java
 *   Descripci�n: Filtro de seguridad CSRF.
 *
 *   Control de Cambios:
 *   1.0 16/10/2017 Everis - Validaci�n token CSRF
 */
public class OwaspCsrfFilter extends BaseServlet implements Filter{

	/**
	 * @param filterConfig FilterConfig
	 * @throws ServletException ex
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: initialized ", EIGlobal.NivelLog.INFO);
	}	


	/**
	 * Intercepta las peticiones de intrusion y valida que el token .
	 * @param request Request
	 * @param response Response
	 * @param filterChain FilterChain
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: doFilter()", EIGlobal.NivelLog.INFO);

		HttpServletRequest reqHttp = (HttpServletRequest)request;
		HttpServletResponse resHttp = (HttpServletResponse)response;
		String servletInvoca = reqHttp.getServletPath();

		BaseResource baseSession = (BaseResource) reqHttp.getSession().getAttribute("session");

		String enlacetk = reqHttp.getParameter("enlacetk");
		/*Validar que la peticion sea POST*/
		if("POST".equalsIgnoreCase(reqHttp.getMethod())){
			EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: servletInvoca ||"+servletInvoca+"||", EIGlobal.NivelLog.INFO);			
			EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: enlacetk ||"+enlacetk+"||", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: CSRF_TOKEN ||"+baseSession.getCsrfToken()+"||", EIGlobal.NivelLog.INFO);

			if(enlacetk != null && !"null".equals(enlacetk) && reqHttp.getParameter("enlacetk").equals(baseSession.getCsrfToken())) {
				EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: doFilter() :: " + " Se procede con el flujo normal de la operacion.", EIGlobal.NivelLog.INFO);
				filterChain.doFilter(request, response);
			} else {
				EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: doFilter() :: " + "Operacion no permitida " + servletInvoca, EIGlobal.NivelLog.INFO);
				resHttp.sendRedirect("operacionCsrf");
			}
		}else {
			/*Seguir con el flujo normal*/
			EIGlobal.mensajePorTrace("***OwaspCsrfFilter :: doFilter() || No fue un metodo POST", EIGlobal.NivelLog.INFO);
			filterChain.doFilter(request, response);
		}


	}
}
