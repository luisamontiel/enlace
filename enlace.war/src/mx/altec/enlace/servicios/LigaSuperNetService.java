/**
 * Clase: LigaSuperNetService.java
 * Fecha: 29/05/2007
 * VC JGR
 * Clase que invoca a un servlet - WS para verificar si el usuario tiene
 * privilegios para operar con el contrato actual en SuperNet
*/
package mx.altec.enlace.servicios;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.HttpTimeoutHandler;


//VC ESC optimizacion URL Connection
public class LigaSuperNetService {

	public String pintaLigaSupernet(String usuario, String contrato){

		String resultado = "";
		//Se hace la llamada al servlet que a su vez hace la llamada al
		//webservice que verifica si se tiene derechos para operar en supernet
		//bajo el mismo contrato
		java.io.BufferedReader input = null;
		try {
			usuario = BaseServlet.convierteUsr7a8(usuario);
			input = runService(Global.LIGA_SUPERNET, usuario, contrato);
			EIGlobal.mensajePorTrace("->DESPUES DEL LLAMADO A RUNSERVICE LIGA<-", EIGlobal.NivelLog.DEBUG);
			resultado = input.readLine();

			EIGlobal.mensajePorTrace("RESULTADO LIGA->" + resultado, EIGlobal.NivelLog.INFO);

		} catch (MalformedURLException e){
			EIGlobal.mensajePorTrace("MALFORMEDURLEXCEPTION EN LIGA->" +
					e.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("IOEXCEPTION EN LIGA->" +
					e.getMessage(), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if(input != null) {
					input.close();
					input = null;
				}
			} catch(Exception e) {
				EIGlobal.mensajePorTrace("Error al cerrar input->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				input = null;
			}
		}
		return resultado;
	}


//VSWF RRG 08-Dic-2008 I Se modifico el metodo para quitar la classe deprecada, para migracion Enlace
	private java.io.BufferedReader runService(String strUrl, String usuario, String
			contrato) throws MalformedURLException, IOException{
		String postParams="?usuario="+usuario+"&contrato=" +
							contrato + "&origen=E";
		EIGlobal.mensajePorTrace("Llamando al servicio para pintar la liga :< "+strUrl+postParams+">", EIGlobal.NivelLog.INFO);
		HttpTimeoutHandler xHTH = new HttpTimeoutHandler(Integer.parseInt(Global.CONNECTION_TIMEOUT));
		URL url = new URL((URL)null, strUrl + postParams , xHTH);
		URLConnection urlConn = url.openConnection();
		return new java.io.BufferedReader(new InputStreamReader(urlConn.getInputStream()));
	}
//	VSWF RRG 08-Dic-2008 F
}