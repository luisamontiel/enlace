package mx.altec.enlace.servicios;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.naming.NamingException;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.FMLTable;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.json.MsgTuxedo;
import mx.altec.enlace.json.MsgTuxedoException;
import mx.altec.enlace.json.parser.JSONParserException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 */
  public class ServicioTux implements Serializable {

  /**
   * debug  nivel de trace
   */
  private boolean debug = false;

  /**
   * dataSource  coneccion oracle
   */
  private String dataSource = Global.DATASOURCE_TUXEDO;

  /**
   * MsgTuxedo mensaje
   */
  MsgTuxedo msgTuxedo = null;

  /**
   * MQQueueSession  session
   */
  MQQueueSession mqTux = null;

/**
 * cuentas_modulo  cuentas del modulo
 * @param trama  trama a ejecutar
 * @return resultado  resultado del servicio ejecutado
 * @throws Exception  control de excepciones
 */
  public Hashtable cuentas_modulo( String trama )  throws Exception {


		EIGlobal.mensajePorTrace("ServicioTux::cuentas_modulo", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[CUENTAS_MODULO]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("trama:[" + trama+ "]", EIGlobal.NivelLog.INFO);

		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("CUENTAS_MODULO");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());

			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("COD_ERROR",msgResponse.getFldRecAsString(FMLTable.COD_ERROR)  );
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}
/**
 * cuentas_modulo1  cunetas del modulo
 * @param trama  trama a ejecutar
 * @return resultado  resultado del servicio ejecutado
 * @throws Exception  control de excepciones
 */
  public Hashtable cuentas_modulo1( String trama )  throws Exception {

		EIGlobal.mensajePorTrace("ServicioTux::cuentas_modulo1", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[CUENTAS_MODULO]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("trama:[" + trama+ "]", EIGlobal.NivelLog.INFO);

		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("CUENTAS_MODULO");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("COD_ERROR",msgResponse.getFldRecAsString(FMLTable.COD_ERROR)  );
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}//fin cuentas 1
/**
 * consulta de tasas
 * @param buffer  control del buffer
 * @param medioentrega  medio de entrega
 * @return resultado  resultado de consulta de tasas
 * @throws Exception  control de excepciones
 */
	public Hashtable consultaTasas(String buffer, String medioentrega) throws Exception {

		EIGlobal.mensajePorTrace("ServicioTux::consultaTasas", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[ConsTasa]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("buffer:[" + buffer+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("medioentrega:[" + medioentrega+ "]", EIGlobal.NivelLog.INFO);
		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("ConsTasa");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,buffer);
			msgTuxedo.addFldEnv(FMLTable.MEDIO_ENTREGA,medioentrega);
			msgTuxedo.addFldRec(FMLTable.OUTPUTDATA, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());

			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("BUFFER",msgResponse.getFldRecAsString(FMLTable.OUTPUTDATA));
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
			EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return resultado;
	}
/**
 * validacion de usuario
 * @param usuario  usuario del cliente
 * @param password  password del usuario
 * @return resultado  resultado de validacion
 * @throws Exception  control de excepciones
 */
	public Hashtable validaUsuario(String usuario,String password)
		throws Exception
	{
		EIGlobal.mensajePorTrace("ServicioTux::validaUsuario", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[VALIDA_USUARIO]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("usuario:[" + usuario+ "]", EIGlobal.NivelLog.INFO);

		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("VALIDA_USUARIO");
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,IEnlace.SUCURSAL_OPERANTE);
			msgTuxedo.addFldEnv(FMLTable.USUARIO,usuario);
			msgTuxedo.addFldEnv(FMLTable.CONTRASENA,password);

			msgTuxedo.addFldRec(FMLTable.OUTPUTDATA, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());

			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("COD_ERROR",msgResponse.getFldRecAsString(FMLTable.COD_ERROR));
				resultado.put("CVE_PTO_VTA",msgResponse.getFldRecAsString(FMLTable.CVE_PTO_VTA));

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}
/**
 * jppm, llamado al nuevo servicio de validacin de contraseas SEG_VALIDAPWD
 * @param trama  trama valida password
 * @return reultado  resultado de validacion de password
 * @throws Exception  control de excepciones
 */
	public Hashtable validaPassword(String trama)
		throws Exception
	{

		EIGlobal.mensajePorTrace("ServicioTux::validaPassword", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SEG_VALIDAPWD]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("trama:[" + trama+ "]", EIGlobal.NivelLog.INFO);

		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SEG_VALIDAPWD");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());

			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("BUFFER",msgResponse.getFldRecAsString(FMLTable.BUFFER)  );
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}
/**
 * 200529800 Recomendaciones CNBV - Praxis - NVS
 * Llamado al servicio SEG_LONGPWD
 * @param trama  longitud de password
 * @return resultado  resultado de longitud depassword
 * @throws Exception  control de excepciones
 */
	public Hashtable longitudPassword(String trama)
		throws Exception
	{

		EIGlobal.mensajePorTrace("ServicioTux::longitudPassword", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SEG_LONGPWD]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("trama:[" + trama+ "]", EIGlobal.NivelLog.INFO);
		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SEG_LONGPWD");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");



			//Envio y recepcion de Mensajes por MQ

			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());

			}catch(Exception e)
			{
				strError = e.getMessage();
				e.printStackTrace();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("BUFFER",msgResponse.getFldRecAsString(FMLTable.BUFFER)  );
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}
/**
 * jppm, llamado al nuevo servicio de validacin de contraseas SEG_CAMBIAPWD
 * @param trama  trama para ambiar password
 * @return resultado  resultado del cambio de password
 * @throws Exception  control de excepciones
 */
	public Hashtable CambiaPassword(String trama)
		throws Exception
	{

		EIGlobal.mensajePorTrace("ServicioTux::CambiaPassword", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SEG_CAMBIAPWD]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("trama:[" + trama+ "]", EIGlobal.NivelLog.INFO);
		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SEG_CAMBIAPWD");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());

			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("BUFFER",msgResponse.getFldRecAsString(FMLTable.BUFFER)  );
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
				throw (Exception)e;
			}
			EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return resultado;
	}
/**
 * validacion de NIP
 * @param usuario  usuario de session
 * @param password  password de actual
 * @param pwdnuevo  password nuevo
 * @return resultado  resultado de validacion de NIP
 * @throws Exception  control de excepciones
 */
	public Hashtable validaNIPSerfin(String usuario, String password, String pwdnuevo)
		throws Exception
	{
		EIGlobal.mensajePorTrace("ServicioTux::validaNIPSerfin", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[VALIDACION_NIP_ETAS]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("usuario:[" + usuario+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("pwdnuevo:[" + pwdnuevo+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("password:[" + password+ "]", EIGlobal.NivelLog.INFO);

		Hashtable resultado = new Hashtable(1);
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("VALIDACION_NIP_ETAS");
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,IEnlace.SUCURSAL_OPERANTE);
			msgTuxedo.addFldEnv(FMLTable.NUM_PERSONA,usuario);
			msgTuxedo.addFldEnv(FMLTable.CONTRASENA,pwdnuevo);
			msgTuxedo.addFldEnv(FMLTable.CONTRASENA_NVA,password);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				resultado.put("BUFFER",msgResponse.getFldRecAsString(FMLTable.BUFFER));
				resultado.put("COD_ERROR",msgResponse.getFldRecAsString(FMLTable.COD_ERROR));

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}
/**
 * metodo para obtener cuentas
 * @param usuario usuario de session
 * @return resultado resultado de obtencion de cuentas
 * @throws Exception control de excepciones
 */
	public String obtenerCuentas( String usuario ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::obtenerCuentas", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[EI_CONT_ASOC]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("usuario:[" + usuario+ "]", EIGlobal.NivelLog.INFO);

		Hashtable resultado = new Hashtable(1);
		String clave;
		String codigo;
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("EI_CONT_ASOC");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,usuario);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				clave = msgResponse.getFldRecAsString(FMLTable.BUFFER);
				codigo = msgResponse.getFldRecAsString(FMLTable.COD_ERROR);

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return clave;

	}
/**
 * metodo tarea perfil
 * @param usuario usuario de session
 * @param password password del usuario
 * @param contrato contrato del ciente
 * @return resultado respuesta tarea perfil
 * @throws Exception control de excepciones
 */
	public String [] traePerfil( String usuario, String password, String contrato ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::traePerfil", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[CONEXION_TELE]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("usuario:[" + usuario+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("password:[" + usuario+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("contrato:[" + contrato+ "]", EIGlobal.NivelLog.INFO);

		String [] resultado;
		String codigo, clave;
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("CONEXION_TELE");
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,IEnlace.SUCURSAL_OPERANTE);
			msgTuxedo.addFldEnv(FMLTable.USUARIO,usuario);
			msgTuxedo.addFldEnv(FMLTable.CONTRASENA,password);
			msgTuxedo.addFldEnv(FMLTable.CUENTA,contrato);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.CLAVE_PERFIL, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

			//Envio y recepcion de Mensajes por MQ
			try {

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				codigo = msgResponse.getFldRecAsString(FMLTable.COD_ERROR);
				clave = msgResponse.getFldRecAsString(FMLTable.CLAVE_PERFIL);

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		resultado = new String [] {codigo, clave};
		EIGlobal.mensajePorTrace("resultado :[" + resultado+ "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}
/**
 * metodo tarea de ambiente
 * @param usuario usuario de session
 * @param contrato contrato del cliente
 * @return resultado respuesta del ambiente
 * @throws Exception control de excepciones
 */
	public String traeAmbiente( String usuario, String contrato ) throws Exception {
		Hashtable htResult = sreferencia( IEnlace.SUCURSAL_OPERANTE );

		EIGlobal.mensajePorTrace("ServicioTux::traeAmbiente", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SAMBIENTE]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("usuario:[" + usuario+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("contrato:[" + contrato+ "]", EIGlobal.NivelLog.INFO);

		String codigo;
		String respuesta ="";
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		String NombreArchivo = IEnlace.REMOTO_TMP_DIR + "/" + usuario + IEnlace.ARCHIVO_AMBIENTE;
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SAMBIENTE");
			msgTuxedo.addFldEnv(FMLTable.CONTRATO,contrato);
			msgTuxedo.addFldEnv(FMLTable.REFERENCIA, (( Integer ) htResult.get( "REFERENCIA" ) ).intValue());
			msgTuxedo.addFldEnv(FMLTable.USUARIO,usuario);
			msgTuxedo.addFldEnv(FMLTable.ARCHIVO,NombreArchivo);
			msgTuxedo.addFldEnv(FMLTable.VERSION,IEnlace.VERSION);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
			//Envio y recepcion de Mensajes por MQ
			try {


				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw e;
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}

			//extraccion de parametros del mensaje de JSON
			try {
				codigo = msgResponse.getFldRecAsString(FMLTable.COD_ERROR);

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				throw (Exception)e;
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
			NombreArchivo = null;
		}
		return codigo;
	}
/**
 * metodo de saldo
 * @param cuenta cuenta del cliente
 * @param plaza_origen paza de origen
 * @param ruta_prod ruta de producto
 * @param referencia referencia de cliente
 * @param tipo_operacion tipo de operacion
 * @return resultado respuesta del saldo
 */
	public Hashtable saldo( String cuenta, String plaza_origen,
			String ruta_prod, String referencia, String tipo_operacion ) {

		EIGlobal.mensajePorTrace("ServicioTux::saldo", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SALDO]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("cuenta:[" + cuenta+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("plaza_origen:[" + plaza_origen + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ruta_prod:[" + ruta_prod+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("referencia:[" + referencia+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("tipo_operacion:[" + tipo_operacion+ "]", EIGlobal.NivelLog.INFO);



		String respuesta ="";
		Hashtable htResultado = new Hashtable( 10 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SALDO");
			msgTuxedo.addFldEnv(FMLTable.CUENTA,cuenta);
			msgTuxedo.addFldEnv(FMLTable.PLAZA_ORIGEN, plaza_origen );
			msgTuxedo.addFldEnv(FMLTable.RUTA_PROD,ruta_prod);
			msgTuxedo.addFldEnv(FMLTable.REFERENCIA,referencia);
			msgTuxedo.addFldEnv(FMLTable.TIPO_OPERACION,tipo_operacion);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");
			msgTuxedo.addFldRec(FMLTable.IMPORTE1, "");
			msgTuxedo.addFldRec(FMLTable.IMPORTE2, "");
			msgTuxedo.addFldRec(FMLTable.IMPORTE5, "");
			msgTuxedo.addFldRec(FMLTable.NOMBRE, "");


			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());

			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {

				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				htResultado.put( "IMPORTE1", new Double(msgResponse.getFldRecAsDouble(FMLTable.IMPORTE1)  ) );
				htResultado.put( "IMPORTE2", new Double(msgResponse.getFldRecAsDouble(FMLTable.IMPORTE2) ) );
				htResultado.put( "IMPORTE5", new Double(msgResponse.getFldRecAsDouble(FMLTable.IMPORTE5) ) );
				htResultado.put( "NOMBRE", msgResponse.getFldRecAsString(FMLTable.NOMBRE) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo de obtencion de referencia
 * @param suc_opera sucursal operante
 * @return resultado devuelve la referencia
 * @throws Exception control de excepciones
 */
	public Hashtable sreferencia( String suc_opera ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::sreferencia", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SREFERENCIA]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("suc_opera:[" + suc_opera + "]", EIGlobal.NivelLog.INFO);


		String respuesta ="";
		Hashtable htResultado = new Hashtable( 10 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SREFERENCIA");
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,IEnlace.SUCURSAL_OPERANTE);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.REFERENCIA, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {
				msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
				msgTuxedo.addFldRec(FMLTable.REFERENCIA, "");
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				htResultado.put( "REFERENCIA", new Integer (msgResponse.getFldRecAsString(FMLTable.REFERENCIA)));

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo manc_valida
 * @param bufferIn buffer inicial
 * @return repsuesta despuesta de manc_valida
 * @throws Exception control de excepciones
 */
	public Hashtable manc_valida( String bufferIn )
		throws Exception
	{
		EIGlobal.mensajePorTrace("ServicioTux::manc_valida", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[MANC_VALIDA]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("bufferIn:[" + bufferIn + "]", EIGlobal.NivelLog.INFO);


		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("MANC_VALIDA");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,bufferIn);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {

				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER));

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo manc_validaSULC
 * @param bufferIn buffer inicial
 * @param lineaCapSUA linea de acptura SUA
 * @return resultado respuesta de manc_valida
 * @throws Exception control de excepciones
 */
	public Hashtable manc_validaSULC( String bufferIn, String lineaCapSUA )
	throws Exception {
	EIGlobal.mensajePorTrace("ServicioTux::manc_validaSULC", EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("Servicio:[MANC_VALIDA]", EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("bufferIn:[" + bufferIn + "]", EIGlobal.NivelLog.INFO);


	String respuesta ="";
	Hashtable htResultado = new Hashtable( 2 );
	msgTuxedo = new MsgTuxedo();
	MsgTuxedo msgResponse = null;
	String strError = "";
	try {
		//Generar Mensaje JSON para invocacion tuxedo
		msgTuxedo.setSvc("MANC_VALIDA");
		msgTuxedo.addFldEnv(FMLTable.BUFFER,bufferIn);
		msgTuxedo.addFldEnv(FMLTable.FML_BUFFER,lineaCapSUA);

		msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
		msgTuxedo.addFldRec(FMLTable.BUFFER, "");

		//Envio y recepcion de Mensajes por MQ
		try {
			mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
			respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
		}catch(Exception e)
		{
			strError = e.getMessage();
			EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

		}
		//parseo de mensaje JSON
		try {
			msgResponse = new MsgTuxedo(respuesta);
		} catch (JSONParserException e) {
			strError = e.getMessage();
			EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

		}

		//extraccion de parametros del mensaje de JSON
		try {

			htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
			htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER));

		} catch (MsgTuxedoException e) {
			strError = e.getMessage();
			EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
		}
	} finally {
		mqTux.close();
		mqTux = null;
		respuesta = null;
		msgResponse = null;
		strError = null;;
		msgTuxedo = null;
	}
	return htResultado;
}
/**
 * metodo de bitacora
 * @param numeroReferencia valor de referencia
 * @param coderror1 codigo de error
 * @param contrato contrato del cliente
 * @param usuario usuario de session
 * @param cuenta cuenta de cliente
 * @param importe valor del importe
 * @param tipo_operacion tipo de operacion a ejecutar
 * @param cuenta2 valor cuenta 2
 * @param cant_titulos cantidad de titulos
 * @param dispositivo2 valor de dispositivo
 * @return resultado respuesta de bitacora
 * @throws Exception control de excepciones
 */
	public Hashtable sbitacora( int  numeroReferencia, String coderror1, String contrato, String usuario, String cuenta, double importe,
								String tipo_operacion, String cuenta2, int cant_titulos, String dispositivo2 )
		throws Exception
	{
		EIGlobal.mensajePorTrace("ServicioTux::sbitacora", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SBITACORA]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("Trama BITACORA = >>>"+"EWEB"+"|"+contrato+"|"+tipo_operacion+"|"+numeroReferencia+"|"+importe+"|"+usuario+"|"+coderror1+"|"+cuenta+"|"+cuenta2+"|"+"390"+"|"+"390"+"|"+"0"+"|"+cant_titulos+"|"+dispositivo2+"<<<", EIGlobal.NivelLog.ERROR);

		String respuesta ="";
		String err_cod = "";

		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SBITACORA");
			msgTuxedo.addFldEnv(FMLTable.MEDIO_ENTREGA,"EWEB");
			msgTuxedo.addFldEnv(FMLTable.CONTRATO,contrato);
			msgTuxedo.addFldEnv(FMLTable.TIPO_OPERACION,tipo_operacion);
			msgTuxedo.addFldEnv(FMLTable.REFERENCIA,numeroReferencia);
			msgTuxedo.addFldEnv(FMLTable.IMPORTE,importe);
			msgTuxedo.addFldEnv(FMLTable.USUARIO,usuario);
			msgTuxedo.addFldEnv(FMLTable.COD_ERROR,coderror1);
			msgTuxedo.addFldEnv(FMLTable.CUENTA,cuenta );
			msgTuxedo.addFldEnv(FMLTable.RUTA,(short)390 );
			msgTuxedo.addFldEnv(FMLTable.CANT_TITULOS,cant_titulos);
			msgTuxedo.addFldEnv(FMLTable.CUENTA2,cuenta2);
			msgTuxedo.addFldEnv(FMLTable.DISPOSITIVO2,dispositivo2);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}

			//extraccion de parametros del mensaje de JSON
			try {

				err_cod = msgResponse.getFldRecAsString(FMLTable.COD_ERROR);
				EIGlobal.mensajePorTrace ("ERR_COD ServicioTux.java.. = >>>" + err_cod + "<<<", EIGlobal.NivelLog.INFO);
				if(err_cod ==null || err_cod == "" ){
					err_cod ="Error en el servicio Tuxedo Bitacora 999";}
				htResultado.put( "COD_ERROR", err_cod );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;


	}
/**
 * metodo serror
 * @param coderror codigo de error
 * @return respuesta valor de error
 * @throws Exception control de excepciones
 */
	public Hashtable serror( String coderror ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::serror", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[SERROR]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("coderror:[" + coderror + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable(1);
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SERROR");
			msgTuxedo.addFldEnv(FMLTable.COD_ERROR,coderror);
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,IEnlace.SUCURSAL_OPERANTE);

			msgTuxedo.addFldRec(FMLTable.MSG_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}

			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "MSG_ERROR", msgResponse.getFldRecAsString(FMLTable.MSG_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo acceso de facultad y cuenta
 * @param cuenta cuenta del cliente
 * @param clave_perfil clave del peril
 * @param clave_facultad clave de facutlad
 * @param importe vaor del importe
 * @return resultado repsueata de accesofaccta
 * @throws Exception control de excepciones
 */
	public Hashtable accesofaccta( String cuenta, String clave_perfil, String clave_facultad,
			double importe ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::accesofaccta", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[ACCESOFACCTA]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("cuenta:[" + cuenta + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("clave_perfil:[" + clave_perfil + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("clave_facultad:[" + clave_facultad + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("importe:[" + importe + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";

		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("ACCESOFACCTA");

			msgTuxedo.addFldEnv(FMLTable.CUENTA,cuenta);
			msgTuxedo.addFldEnv(FMLTable.CLAVE_PERFIL,clave_perfil);
			msgTuxedo.addFldEnv(FMLTable.CLAVE_FACULTAD,clave_facultad);
			msgTuxedo.addFldEnv(FMLTable.IMPORTE,importe);
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,IEnlace.SUCURSAL_OPERANTE);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			msgResponse = null;
			strError = null;
			respuesta = null;
		}
		return htResultado;
	}
/**
 * metodo accesofac
 * @param cuenta cuenta del cliente
 * @param claveFacultad clave de facultad
 * @param clavePerfil clave de perfil
 * @param importe valor del importe
 * @param sucursalOperante sucursal operante
 * @return resultado respuesta de acceso de facultad
 * @throws Exception control de excepciones
 */
	public Hashtable accesofac(String cuenta, String claveFacultad, String clavePerfil, double importe, String sucursalOperante)
	throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::accesofac", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[ACCESOFAC]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("cuenta:[" + cuenta + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("clave_perfil:[" + clavePerfil + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("clave_facultad:[" + claveFacultad + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("importe:[" + importe + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("sucursal:[" + sucursalOperante + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";

		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("ACCESOFAC");

			msgTuxedo.addFldEnv(FMLTable.CUENTA,cuenta);
			msgTuxedo.addFldEnv(FMLTable.CLAVE_PERFIL,clavePerfil);
			msgTuxedo.addFldEnv(FMLTable.CLAVE_FACULTAD,claveFacultad);
			msgTuxedo.addFldEnv(FMLTable.IMPORTE,importe);
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA, sucursalOperante);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			msgResponse = null;
			strError = null;
			respuesta = null;
		}
		return htResultado;
	}

/**
 * metodo acceso facultad
 * @param clave_perfil clave de perfil
 * @param clave_facultad calve de facultad
 * @return respuesta devuelve acceso de facultad
 * @throws Exception control de excepciones
 */
	public Hashtable accesofac( String clave_perfil, String clave_facultad) throws Exception {

		EIGlobal.mensajePorTrace("ServicioTux::accesofac", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[ACCESOFAC]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("clave_perfil:[" + clave_perfil + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("clave_facultad:[" + clave_facultad + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";

		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("ACCESOFAC");

			msgTuxedo.addFldEnv(FMLTable.CLAVE_PERFIL,clave_perfil);
			msgTuxedo.addFldEnv(FMLTable.CLAVE_FACULTAD,clave_facultad);
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,IEnlace.SUCURSAL_OPERANTE);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				EIGlobal.mensajePorTrace ( "COD_ERROR::TUXEDO::" + msgResponse.getFldRecAsString(FMLTable.COD_ERROR) + ":" , EIGlobal.NivelLog.DEBUG);

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			msgResponse = null;
			strError = null;
			respuesta = null;
		}
		return htResultado;
	}


/**
 * metodo de programacion
 * @param fecha_aplicacion fecha de aplicacion
 * @param cta_operacion1 cuenta de operacion
 * @param tipo_cta_origen tipo demcuenta origen
 * @param tipo_cta_destino tipo de cuenta destino
 * @param cta_operacion2 cuneta de operacion 2
 * @param numeroReferencia numero de referencia
 * @param contrato contrato del cliente
 * @param tipo_operacion tipo de operacion
 * @param usuario usuario de session
 * @param importe valor del importe
 * @param clave_perfil clave del perfil
 * @return respuesta respuesta de programacion
 * @throws Exception control de excepciones
 */
	public Hashtable programacion( String fecha_aplicacion, String cta_operacion1,
			String tipo_cta_origen, String tipo_cta_destino,
			String cta_operacion2, int numeroReferencia, String contrato,
			String tipo_operacion, String usuario, double importe,
			String clave_perfil ) throws Exception {

		EIGlobal.mensajePorTrace("ServicioTux::programacion", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[PROGRAMACION]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("tipo_operacion:[" + tipo_operacion + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("contrato:[" + contrato + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("usuario:[" + usuario + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("fecha_aplicacion:[" + fecha_aplicacion + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("cta_operacion1:[" + cta_operacion1 + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("tipo_cta_origen:[" + tipo_cta_origen + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("tipo_cta_destino:[" + tipo_cta_destino + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("importe:[" + importe + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("numeroReferencia:[" + numeroReferencia + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("clave_perfil:[" + clave_perfil + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("cta_operacion2:[" + cta_operacion2 + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("PROGRAMACION");

			msgTuxedo.addFldEnv(FMLTable.TIPO_OPERACION,tipo_operacion);
			msgTuxedo.addFldEnv(FMLTable.CONTRATO,contrato);
			msgTuxedo.addFldEnv(FMLTable.USUARIO,usuario);
			msgTuxedo.addFldEnv(FMLTable.FECHA,fecha_aplicacion);
			msgTuxedo.addFldEnv(FMLTable.CUENTA,cta_operacion1);
			msgTuxedo.addFldEnv(FMLTable.TIPO_CREDITO,tipo_cta_origen);
			msgTuxedo.addFldEnv(FMLTable.TIPO_CTA_DESTINO,tipo_cta_destino);
			msgTuxedo.addFldEnv(FMLTable.IMPORTE,importe);
			msgTuxedo.addFldEnv(FMLTable.CANT_TITULOS,0);
			msgTuxedo.addFldEnv(FMLTable.REFERENCIA,numeroReferencia);
			msgTuxedo.addFldEnv(FMLTable.CLAVE_PERFIL,clave_perfil);
			msgTuxedo.addFldEnv(FMLTable.CUENTA2,cta_operacion2);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo de cancelacion
 * @param trama trama de servicio a ejecutar
 * @return respuesta respuesta de cancelacion
 * @throws Exception control de excepciones
 */
	public Hashtable manc_cancela( String trama ) throws Exception {
		EIGlobal.mensajePorTrace ("TuxedoGlobalEJB.java::manc_cancela( " + trama + " )::MANC_CANCELA" , EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("ServicioTux::manc_cancela", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[MANC_CANCELA]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("MANC_CANCELA");

			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo de actualizacion de estatus
 * @param trama trma de actualizacion de estatus
 * @return respuesta respuesta de actualizacion de estatus
 * @throws Exception control de excepciones
 */
	public Hashtable manc_act_est( String trama )  throws Exception {
		EIGlobal.mensajePorTrace ("TuxedoGlobalEJB.java::manc_act_est( " + trama + " )::MANC_ACT_EST" , EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("ServicioTux::manc_act_est", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[MANC_ACT_EST]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("MANC_ACT_EST");

			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}

	/**
	 * Metodo encargado de comunicarse directamente con el vfilesrvr para la validaci�n
	 * de las cuentas y duplicados de transferencias
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable validaTransferencias(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::manc_act_est", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[servicio]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);

		try{

			msgTuxedo = new MsgTuxedo();

			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}

	/**
	 * Metodo encargado de comunicarse directamente con el vfilesrvr para la validaci�n
	 * de las cuentas y duplicados de transferencias, que aplica para operaciones transaccionales
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @param sessionMQ 		Session que contiene conexi�n MQ
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable validaCuentasMax(String trama, String servicio, MQQueueSession sessionMQ) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::validaTransferenciasMax", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[servicio]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);

		try{
			// Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo = new MsgTuxedo();

			// Asignaci�n de servicio
			msgTuxedo.setSvc(servicio);

			// Parametros de entrada
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			// Parametros de salida
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			// Envio de Mensajes por MQ
			sessionMQ.enviaMensaje(msgTuxedo.toString());

			// Recepcion de Mensajes por MQ
			respuesta = sessionMQ.recibeMensaje();
			if("TIMEOUT".equals(respuesta)){
				throw new MQQueueSessionException("MQQueueSessionExceptionTimeOut");
			}

			// Parseo de mensaje JSON
			msgResponse = new MsgTuxedo(respuesta);

			// Extraccion de parametros del mensaje de JSON
			htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
			htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}catch(Exception e){
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}finally{
				sessionMQ = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
		}
		return htResultado;
	}

/**
 * consulta de cliente
 * @param trama trama de ejecucion
 * @param servicio servicio a ejecutar
 * @return respuesta respuesta de cliente
 * @throws Exception control de excepciones
 */
	public Hashtable cliente(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::manc_act_est", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[servicio]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);

		try{

			msgTuxedo = new MsgTuxedo();

			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo tct_red
 * @param trama servicio a ejecutar
 * @return respuesta respuesta de tct_red
 * @throws Exception control de excepciones
 */
	public Hashtable tct_red( String trama ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::tct_red", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[TCT_RED]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("TCT_RED");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo web_red
 * @param trama servicio de ejecusion
 * @return respuesta respueta de servicio ejecutado
 * @throws Exception control de excepciones
 */
	public Hashtable web_red( String trama )
		throws Exception
	{
		EIGlobal.mensajePorTrace("ServicioTux::web_red", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[WEB_RED]", EIGlobal.NivelLog.INFO);
		LYMValidador valida = null;
		try {
			valida = new LYMValidador();
			String resultadoLyM = valida.validaLyM(trama);
			valida.cerrar();
			EIGlobal.mensajePorTrace("LYM_VALIDA resultado: " + resultadoLyM, EIGlobal.NivelLog.INFO);
			if(resultadoLyM.indexOf("ALYM0000") == -1) {
				Hashtable htResultado = new Hashtable( 2 );
				htResultado.put( "BUFFER", resultadoLyM );
				htResultado.put("COD_ERROR", resultadoLyM.substring(0, 8));
				return htResultado;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(valida != null) {
				valida.cerrar();
			}
		}


		if (trama.indexOf("|PASS|") == -1)
		{
			EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		}

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("WEB_RED");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				EIGlobal.mensajePorTrace ( "resultado WEB_RED::" + htResultado + ":" , EIGlobal.NivelLog.ERROR);

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}

	/**
	 * Metodo que ejecuta servicio Tuxedo para generar un archivo de detalle para Nomina en Linea
	 *
	 * @param trama					   Trama de entrada
	 * @param servicio				   Servicio a llamar
	 * @param sessionMQ 			   Session que contiene conexi�n MQ
	 * @return htResultado			   HashTable con respuesta
	 * @throws JMSException     	   Error de comunicaci�n
	 * @throws NamingException 		   Error de comunicacion
	 * @throws MQQueueSessionException Error de comunicacion
	 * @throws Exception			   Error de comunicacion
	 */
	public Hashtable webRedLN( String trama ) throws NamingException, JMSException, MQQueueSessionException
	{
		EIGlobal.mensajePorTrace("ServicioTux::web_red_ln()::ejecutando WEB_RED para exportar archivo", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[WEB_RED]", EIGlobal.NivelLog.INFO);

		//trama = trama.replaceAll("||", "| |");
		if (trama.indexOf("|PASS|") == -1)
		{
			EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		}

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("WEB_RED");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			try {
			//Envio y recepcion de Mensajes por MQ
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(JMSException e){
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO:: JMSException" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}catch(NamingException e){
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO:: NamingException" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}/**catch(MQQueueSessionException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO:: MQQueueSessionException" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}**/
				//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
				EIGlobal.mensajePorTrace ( "resultado WEB_RED::" + htResultado + ":" , EIGlobal.NivelLog.ERROR);

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: CARGA_FAC_PERF
	 * Metodo encargado de obtener las facultades por perfil
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable cargaFacPerfil(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::cargaFacPerfil", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::cargaFacPerfil - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: PERF_ASIG
	 * Metodo encargado de validar si el perfil est� ya asignado
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable tienePerfilAsignado(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::tienePerfilAsignado", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "tienePerfilAsignado - ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "tienePerfilAsignado - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "tienePerfilAsignado - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "tienePerfilAsignado - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::tienePerfilAsignado - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: FAC_EXTRA
	 * Metodo encargado de validar si el perfil tiene facultades extraordinarias
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable tieneFacultadEx(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::tieneFacultadEx", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "tieneFacultadEx - ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "tieneFacultadEx - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "tieneFacultadEx - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "tieneFacultadEx - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::tieneFacultadEx - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: OBTEN_PERF_USR
	 * Metodo encargado de recuperar el perfil del usuario
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable getPerfilUsuario(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::getPerfilUsuario", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "getPerfilUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "getPerfilUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "getPerfilUsuario - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "getPerfilUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::getPerfilUsuario - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: BAJA_FAC_USR
	 * Metodo encargado de dar de baja una facultad
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable bajaFacultad(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::bajaFacultad", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaFacultad - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaFacultad - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "bajaFacultad - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaFacultad - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::bajaFacultad - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: BAJA_PERFPROT
	 * Metodo encargado de baja un perfil protipo
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable bajaPerfProt(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::bajaPerfProt", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaPerfProt - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaPerfProt - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "bajaPerfProt - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaPerfProt - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::bajaPerfProt - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: BAJA_PERFIL
	 * Metodo encargado de baja un perfil
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable bajaPerfil(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::bajaPerfil", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaPerfil - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaPerfil - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "bajaPerfil - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaPerfil - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::bajaPerfil - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: BAJA_USUARIO
	 * Metodo encargado de baja un perfil
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable bajaUsuario(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::bajaUsuario", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "bajaUsuario - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "bajaUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::bajaUsuario - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: ALTA_PERFUSR
	 * Metodo encargado de alta un perfil
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable altaPerfilUsr(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::altaPerfilUsr", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfilUsr - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfilUsr - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "altaPerfilUsr - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfilUsr - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::altaPerfilUsr - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: ALTA_PERFPROT
	 * Metodo encargado de alta un perfil prototipo
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable altaPerfProt(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::altaPerfProt", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfProt - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfProt - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "altaPerfProt - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfProt - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::altaPerfProt - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: ALTA_FAC_EXTRA
	 * Metodo encargado de alta facultades extraordinarias
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable altaFacExtraord(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::altaFacExtraord", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaFacExtraord - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaFacExtraord - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "altaFacExtraord - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaFacExtraord - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::altaFacExtraord - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: ALTA_PERFIL
	 * Metodo encargado de alta de perfil
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable altaPerfil(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::altaPerfil", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaFacExtraord - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfil - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "altaPerfil - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaPerfil - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::altaPerfil - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: ALTA_USUARIO
	 * Metodo encargado de alta de usuario
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable altaUsuario(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::altaUsuario", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "altaUsuario - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "altaUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::altaUsuario - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: USR_OTR_CONTR
	 * Metodo encargado de verificar si el usuario esta asignado a otro contrato
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable usuarioEnOtroContrato(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::usuarioEnOtroContrato", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "usuarioEnOtroContrato - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "usuarioEnOtroContrato - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "usuarioEnOtroContrato - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "usuarioEnOtroContrato - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::usuarioEnOtroContrato - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

	/**
	 * Servidor: admusrsrvr
	 * Servicio: PERF_OTR_USR
	 * Metodo encargado de verificar si el perfil esta asignado a otro usuario
	 *
	 * @param trama				Trama de entrada
	 * @param servicio			Servicio a llamar
	 * @return htResultado		HashTable con respuesta
	 * @throws Exception		Error de comunicaci�n
	 */
	public Hashtable perfilEnOtroUsuario(String trama, String servicio) throws Exception {
		String strError = "";
		String respuesta ="";
		MsgTuxedo msgResponse = null;
		Hashtable htResultado = new Hashtable( 2 );
		EIGlobal.mensajePorTrace("ServicioTux::perfilEnOtroUsuario", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
		try{
			msgTuxedo = new MsgTuxedo();
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc(servicio);
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,
						Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			} catch(Exception e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "perfilEnOtroUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "perfilEnOtroUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "perfilEnOtroUsuario - ERROR::RESPUESTA::" +
						respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "perfilEnOtroUsuario - ERROR::TUXEDO::" +
						strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		}finally{
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		EIGlobal.mensajePorTrace("ServicioTux::perfilEnOtroUsuario - Termina", EIGlobal.NivelLog.DEBUG);
		return htResultado;
	}

/**
 * metodo norma43
 * @param trama servicio a ejecutar
 * @return repsuesta repsuesta de norma
 * @throws Exception control de excepciones
 */
	public Hashtable norma43( String trama )
		throws Exception
	{

		EIGlobal.mensajePorTrace("ServicioTux::norma43", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[N43_ALTA_CONS]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 1 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("N43_ALTA_CONS");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo cuentas asociadas
 * @param trama servicio a ejecutar
 * @return respuesta respuesta de cuentas asociadas
 * @throws Exception control de excepciones
 */
	public Hashtable ei_ctas_asoc( String trama )  throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::ei_ctas_asoc", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[EI_CTAS_ASOC]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("EI_CTAS_ASOC");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 * metodo basicp
 * @param buffer valor de buffer
 * @return respuesta respuesta de buffer
 * @throws Exception control de excepciones
 */
	public Hashtable basicp( String buffer ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::basicp", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[BASICP]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("buffer:[" + buffer + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("BASICP");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,buffer);
			msgTuxedo.addFldEnv(FMLTable.MEDIO_ENTREGA,"TCT");

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}

	/**
	 * Tubop.
	 *
	 * @param tipo_operacion the tipo_operacion
	 * @param tipo_prod the tipo_prod
	 * @param contrato_inversion the contrato_inversion
	 * @param indicador1 the indicador1
	 * @param indicador2 the indicador2
	 * @param folio the folio
	 * @param numeroreferencia the numeroreferencia
	 * @return the hashtable
	 * @throws Exception the exception
	 */
	public Hashtable tubop( String tipo_operacion, String tipo_prod,
			String contrato_inversion, String indicador1, String indicador2,
			String folio, int numeroreferencia ) throws Exception {
		EIGlobal.mensajePorTrace("ServicioTux::tubop", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[TUBOP]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("tipo_operaciona :[" +tipo_operacion+ "]", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("tipo_prod :[" +tipo_prod+ "]", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("contrato_inversion :[" +contrato_inversion+ "]", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("indicador1 :[" + indicador1+ "]", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("indicador2 :[" + indicador2+ "]", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("folio :[" + folio+ "<", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("numeroreferencia :[" + numeroreferencia+ "]", EIGlobal.NivelLog.ERROR);

		String respuesta ="";
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("TUBOP");
			msgTuxedo.addFldEnv(FMLTable.TIPO_OPERACION,tipo_operacion);
			msgTuxedo.addFldEnv(FMLTable.TIPO_PROD,tipo_prod);
			msgTuxedo.addFldEnv(FMLTable.CUENTA,contrato_inversion);
			msgTuxedo.addFldEnv(FMLTable.CONTRATO,folio);
			msgTuxedo.addFldEnv(FMLTable.INDICADOR1,indicador1);
			msgTuxedo.addFldEnv(FMLTable.INDICADOR2,indicador2);
			msgTuxedo.addFldEnv(FMLTable.USUARIO,"0000000");
			msgTuxedo.addFldEnv(FMLTable.RUTA,394);
			msgTuxedo.addFldEnv(FMLTable.SUC_OPERA,"981");


			msgTuxedo.addFldRec(FMLTable.BUFFER, "");
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );
				htResultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}


/**
 * metodo TUBOCT
 * @param producto valor producto
 * @param subproducto valor subproducto
 * @param tipo_operacion tipo de operacion
 * @param campo_aux campo auxiliar
 * @param campo_aux2 campo auxiliar 2
 * @return resultado respuesta de TUBOCT
 * @throws Exception control de excepciones
 */
	public Hashtable TUBOCT ( String producto, String subproducto, String tipo_operacion, String campo_aux, String campo_aux2)
		throws Exception
	{

				EIGlobal.mensajePorTrace("ServicioTux::tubop", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Servicio:[TUBOP]", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("tipo_operacion :[" +tipo_operacion+ "]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("subproducto :[" +subproducto+ "]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("producto :[" +producto+ "]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("campo_aux :[" +campo_aux+ "]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("campo_aux2 :[" +campo_aux2+ "]", EIGlobal.NivelLog.ERROR);


				String respuesta ="";

				Hashtable resultado = new Hashtable( 2 );
				msgTuxedo = new MsgTuxedo();
				MsgTuxedo msgResponse = null;
				String strError = "";
				try {
					//Generar Mensaje JSON para invocacion tuxedo
					msgTuxedo.setSvc("TUBOP");

					if(!"".equals(producto.trim())){
						msgTuxedo.addFldEnv(FMLTable.CVE_PROD,producto);}

					if(!"".equals(subproducto.trim())){
						msgTuxedo.addFldEnv(FMLTable.TIPO_PROD,subproducto);}


					msgTuxedo.addFldEnv(FMLTable.TIPO_OPERACION,tipo_operacion);

					if(!"".equals(campo_aux.trim())&&!"".equals(campo_aux2.trim()))
					{
						msgTuxedo.addFldEnv(FMLTable.CAMPO_AUX,campo_aux);
						msgTuxedo.addFldEnv(FMLTable.CAMPO_AUX2,campo_aux2);
					}

					msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");
					msgTuxedo.addFldRec(FMLTable.FMLSTRING, "");
					msgTuxedo.addFldRec(FMLTable.TIPO_SOC, "");
					msgTuxedo.addFldRec(FMLTable.PAIS_ORIGEN, "");
					msgTuxedo.addFldRec(FMLTable.SECTOR, "");
					msgTuxedo.addFldRec(FMLTable.CAMPO_AUX2, "");

					//Envio y recepcion de Mensajes por MQ
					try {
						mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

						respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
					}catch(Exception e)
					{
						strError = e.getMessage();
						EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
					}
					//parseo de mensaje JSON
					try {
						msgResponse = new MsgTuxedo(respuesta);
					} catch (JSONParserException e) {
						strError = e.getMessage();
						EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

					}
					//extraccion de parametros del mensaje de JSON
					try {

						if( msgResponse.getFldRecAsString(FMLTable.COD_ERROR) != null )
						{
							  resultado.put( "COD_ERROR", msgResponse.getFldRecAsString(FMLTable.COD_ERROR) );
							  EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.COD_ERROR:"+ msgResponse.getFldRecAsString(FMLTable.COD_ERROR), EIGlobal.NivelLog.ERROR);
						}else{
							  resultado.put( "COD_ERROR", "");
							  EIGlobal.mensajePorTrace("***TUBOCT	OUTPUT.outputBlock.COD_ERROR5: nullo", EIGlobal.NivelLog.ERROR);

						}
						if( msgResponse.getFldRecAsString(FMLTable.FMLSTRING) != null )
						{
							  resultado.put( "FMLSTRING", msgResponse.getFldRecAsString(FMLTable.FMLSTRING) );

						}else{
							  resultado.put( "FMLSTRING","");
							  EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.FMLSTRING: nullo", EIGlobal.NivelLog.ERROR);
						}
						if( msgResponse.getFldRecAsString(FMLTable.TIPO_SOC) != null )
						{
							resultado.put( "TIPO_SOC", msgResponse.getFldRecAsString(FMLTable.TIPO_SOC) );
							EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.TIPO_SOC:"+msgResponse.getFldRecAsString(FMLTable.TIPO_SOC), EIGlobal.NivelLog.ERROR);
						}else
						{
							  resultado.put( "TIPO_SOC","");
							  EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.TIPO_SOC: nullo", EIGlobal.NivelLog.ERROR);
						}
						if( msgResponse.getFldRecAsString(FMLTable.PAIS_ORIGEN) != null )
						{
							  resultado.put( "PAIS_ORIGEN", msgResponse.getFldRecAsString(FMLTable.PAIS_ORIGEN) );
							  EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.PAIS_ORIGEN:"+ msgResponse.getFldRecAsString(FMLTable.PAIS_ORIGEN), EIGlobal.NivelLog.ERROR);
						}else{
							  resultado.put( "PAIS_ORIGEN","");
							  EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.PAIS_ORIGEN: nullo", EIGlobal.NivelLog.ERROR);
						}
						if( msgResponse.getFldRecAsString(FMLTable.SECTOR) != null )
						{
							  resultado.put( "SECTOR", msgResponse.getFldRecAsString(FMLTable.SECTOR) );
							  EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.SECTOR:"+msgResponse.getFldRecAsString(FMLTable.SECTOR), EIGlobal.NivelLog.ERROR);
						}else{
							   resultado.put( "SECTOR", "");
							   EIGlobal.mensajePorTrace("***TUBOCT	OUTPUT.outputBlock.SECTOR: nullo", EIGlobal.NivelLog.ERROR);
						}
						if( msgResponse.getFldRecAsString(FMLTable.CAMPO_AUX2) != null )
						{
							resultado.put( "CAMPO_AUX2", msgResponse.getFldRecAsString(FMLTable.CAMPO_AUX2) );
						    	EIGlobal.mensajePorTrace("***TUBOCT  OUTPUT.outputBlock.CAMPO_AUX2:"+ msgResponse.getFldRecAsString(FMLTable.CAMPO_AUX2), EIGlobal.NivelLog.ERROR);
						}else
						{
							   resultado.put( "CAMPO_AUX2","");
							   EIGlobal.mensajePorTrace("***TUBOCT	OUTPUT.outputBlock.CAMPO_AUX2: nullo", EIGlobal.NivelLog.ERROR);
						}

					} catch (MsgTuxedoException e) {
						strError = e.getMessage();
						EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
					}
				} finally {
					mqTux.close();
					mqTux = null;
					respuesta = null;
					msgResponse = null;
					strError = null;;
					msgTuxedo = null;
				}
			    EIGlobal.mensajePorTrace("***TUBOCT  SALIENDO DEL TUBOCT***", EIGlobal.NivelLog.ERROR);
			    return resultado;
		}



/**
 * //200529800 Recomendaciones CNBV - Praxis - NVS
 * //Llamado al servicio SEG_LONGPWD
 * @param cdgCliente campo codigo de cliente
 * @return resultado respuesta de datos del cliente
 * @throws Exception control de excepciones
 */
	public Hashtable datosCliente(String cdgCliente)
		throws Exception
	{

		EIGlobal.mensajePorTrace("ServicioTux::datosCliente", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Servicio:[BVPF_TRADUC]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("cdgCliente:[" + cdgCliente + "]", EIGlobal.NivelLog.INFO);

		String respuesta ="";
		String trama = (cdgCliente + "|F");
		Hashtable htResultado = new Hashtable( 2 );
		msgTuxedo = new MsgTuxedo();
		MsgTuxedo msgResponse = null;
		String strError = "";
		try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("BVPF_TRADUC");
			msgTuxedo.addFldEnv(FMLTable.CVE_OPERACION,"DB2_BASI_CON");
			msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);
			msgTuxedo.addFldEnv(FMLTable.MEDIO_ENTREGA,"FML");

			msgTuxedo.addFldRec(FMLTable.BUFFER, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}
			//extraccion de parametros del mensaje de JSON
			try {
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
		} finally {
			mqTux.close();
			mqTux = null;
			respuesta = null;
			msgResponse = null;
			strError = null;;
			msgTuxedo = null;
		}
		return htResultado;
	}
/**
 ** Everis - 02/05/07 - Solucion de Raz
 **
 ** Nuevo llamado al servicio tuxedo para enviar archivos de los diferentes
 ** mdulos en formato gzip.
 * @param trama trama que se ejecuta
 * @return resultado resultado de envio de datos
 * @throws Exception control de excepciones
 */
		public Hashtable envia_datos( String trama ) throws Exception
		{

			EIGlobal.mensajePorTrace("ServicioTux::envia_datos", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[ENV_DAT_TRANS]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("cdgCliente:[" + trama + "]", EIGlobal.NivelLog.INFO);

			String respuesta ="";
			Hashtable htResultado = new Hashtable( 1 );
			msgTuxedo = new MsgTuxedo();
			MsgTuxedo msgResponse = null;
			String strError = "";
			try {
				//Generar Mensaje JSON para invocacion tuxedo
				msgTuxedo.setSvc("ENV_DAT_TRANS");
				msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

				msgTuxedo.addFldRec(FMLTable.BUFFER, "");

				//Envio y recepcion de Mensajes por MQ
				try {
					mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

					respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				}catch(Exception e)

				{
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

				}
				//parseo de mensaje JSON
				try {
					msgResponse = new MsgTuxedo(respuesta);
				} catch (JSONParserException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

				}
				//extraccion de parametros del mensaje de JSON
				try {
					htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

				} catch (MsgTuxedoException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				}
			} finally {
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
			return htResultado;
		}
/**
 **	Everis - 02/05/07 - Solucion de Raz
 **
 **	Nuevo llamado al servicio tuxedo para arrancar con el proceso de transferencias
 **     en enlace
 * @param trama que se ejecuta
 * @return resultado respuesta de inicio de proceso
 * @throws Exception control de excepciones
 */
		public Hashtable inicia_proceso( String trama ) throws Exception
		{

			EIGlobal.mensajePorTrace("ServicioTux::inicia_proceso", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[INI_PRC_TRANS]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("cdgCliente:[" + trama + "]", EIGlobal.NivelLog.INFO);

			String respuesta ="";
			Hashtable htResultado = new Hashtable( 1 );
			msgTuxedo = new MsgTuxedo();
			MsgTuxedo msgResponse = null;
			String strError = "";
			try {
				//Generar Mensaje JSON para invocacion tuxedo
				msgTuxedo.setSvc("INI_PRC_TRANS");
				msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

				msgTuxedo.addFldRec(FMLTable.BUFFER, "");

				//Envio y recepcion de Mensajes por MQ
				try {
					mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

					respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				}catch(Exception e)
				{
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

				}
				//parseo de mensaje JSON
				try {
					msgResponse = new MsgTuxedo(respuesta);
				} catch (JSONParserException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

				}
				//extraccion de parametros del mensaje de JSON
				try {
					htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

				} catch (MsgTuxedoException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				}
			} finally {
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
			return htResultado;
		}
/**
 **	Everis - 02/05/07 - Solucion de Raz
 **
 **	Nuevo llamado al servicio tuxedo para la generacin de folios para las
 **     trnasferencias por archivo de Enlace
 * @param trama trama que se ejecuta
 * @return resultado respuesta de alta de folio
 * @throws Exception control de excepciones
 */
		public Hashtable alta_folio( String trama ) throws Exception
		{

			EIGlobal.mensajePorTrace("ServicioTux::alta_folio", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[ALT_FOL_TRANS]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("cdgCliente:[" + trama + "]", EIGlobal.NivelLog.INFO);

			String respuesta ="";
			Hashtable htResultado = new Hashtable( 1 );
			msgTuxedo = new MsgTuxedo();
			MsgTuxedo msgResponse = null;
			String strError = "";
			try {
				//Generar Mensaje JSON para invocacion tuxedo
				msgTuxedo.setSvc("ALT_FOL_TRANS");
				msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

				msgTuxedo.addFldRec(FMLTable.BUFFER, "");

				//Envio y recepcion de Mensajes por MQ
				try {
					mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

					respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				}catch(Exception e)
				{
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

				}
				//parseo de mensaje JSON
				try {
					msgResponse = new MsgTuxedo(respuesta);
				} catch (JSONParserException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

				}
				//extraccion de parametros del mensaje de JSON
				try {
					htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

				} catch (MsgTuxedoException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				}
			} finally {
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
			return htResultado;
		}
/**
 * The session context is not serializable, and should be
 * cached in a transient member variable
 * @param trama trama que se ejecuta
 * @param tamanio valor tamanio
 * @param remoteresp respuesta remota
 * @param localpath ruta local
 * @return resultado respuesta de web_red_mod
 * @throws Exception control de excepciones
 */
	    public String web_red_mod( String trama, int tamanio, String remoteresp, String localpath )
	    throws Exception {

		String resultado = null;
		int timeout= Math.round((tamanio*15)/25000);
		if(timeout < 14){
			timeout=14;}
		int sec=0;
		boolean finish=false;

		try {
		    EIGlobal.mensajePorTrace("Iniciando espera...", EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Iniciando Servicio web_red_mod...", EIGlobal.NivelLog.DEBUG);
		    Hashtable hm = web_red(trama);

		    while(!finish){
		    	if(copyRemoteFile(localpath, localpath+remoteresp)){
				EIGlobal.mensajePorTrace("Archivo Encontrado, intentando copia", EIGlobal.NivelLog.DEBUG);
				File file = new File(localpath+remoteresp);
				try{
					EIGlobal.mensajePorTrace("Iniciando Lectura de resultado", EIGlobal.NivelLog.DEBUG);
					FileInputStream fis = new FileInputStream(file);

					java.io.BufferedReader dis = new java.io.BufferedReader(new InputStreamReader(fis));

					resultado = dis.readLine();
					if(resultado==null){
						EIGlobal.mensajePorTrace("El resultado es nulo", EIGlobal.NivelLog.ERROR);
						resultado="error";
					}else{
						EIGlobal.mensajePorTrace("El resultado es: "+resultado, EIGlobal.NivelLog.DEBUG);
					}
				}catch(Exception resex){
					EIGlobal.mensajePorTrace("Error al leer el resultado", EIGlobal.NivelLog.ERROR);
					resultado="error";
				}
				EIGlobal.mensajePorTrace("Borrando archivo local: ", EIGlobal.NivelLog.DEBUG);
				if(file.exists()){
					file.delete();
				}
				EIGlobal.mensajePorTrace("Terminando Espera.... ", EIGlobal.NivelLog.DEBUG);
				break;
			}
			try{
		    		Thread.sleep(60000);
			}catch(Exception threadex){
				EIGlobal.mensajePorTrace( "Error parando thread", EIGlobal.NivelLog.ERROR);
			}
			if(sec>timeout){
				EIGlobal.mensajePorTrace("Se termin� el tiempo de espera", EIGlobal.NivelLog.DEBUG);
				break;
				}
			else{
				sec++;}
		    }

	        }  catch (Exception e1) {

	            resultado = "error";
	        } finally {

	            return resultado;
	        }
	    }
/**
 * metodo de copia remota de archivo
 * @param path rua de archivo
 * @param remoteresp respuesta remota
 * @return resultado respuesta de copia remota de archivo
 */
  public boolean copyRemoteFile(String path, String remoteresp){
  	boolean ready=true;
  	EIGlobal.mensajePorTrace("ServicioTux:copyRemoteFile - recibe - Inicia", EIGlobal.NivelLog.INFO);

	String command="";
	try{

		//IF PROYECTO ATBIA1 (NAS) FASE II

       	boolean Respuestal = true;
        ArchivoRemoto recibeArch = new ArchivoRemoto();

       	if(!recibeArch.copiaCUENTAS(remoteresp, path)){

				EIGlobal.mensajePorTrace("*** ServicioTux.copyRemoteFile  No se realizo la copia remota:" + remoteresp, EIGlobal.NivelLog.ERROR);
				Respuestal = false;

			}
			else {
			    EIGlobal.mensajePorTrace("*** ServicioTux.copyRemoteFile archivo remoto copiado exitosamente:" + remoteresp, EIGlobal.NivelLog.DEBUG);

			}

       	if (Respuestal == true){
            return true;
		}

	}catch(Exception exception){
		EIGlobal.mensajePorTrace("-!COMANDO[" + command + "] RESULT[" + exception.getMessage() + "]", EIGlobal.NivelLog.DEBUG);
		ready= false;
	}
	return ready;
  }
/**
 **	05/06/08 - Cat�logo de N�mina
 **
 **	Nuevo llamado al servicio tuxedo para baja de Empleados
 **	Formato Trama contrato@secuencia@
 * @param trama trama que se ejecuta
 * @return resultado respuesta de cancelacion de empleado
 * @throws Exception control de excepciones
 */
  public Hashtable canc_empl( String trama ) throws Exception
		{

			EIGlobal.mensajePorTrace("ServicioTux::canc_empl", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[CANC_EMPL]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);


			String respuesta ="";
			Hashtable htResultado = new Hashtable( 1 );
			msgTuxedo = new MsgTuxedo();
			MsgTuxedo msgResponse = null;
			String strError = "";
			try {
				//Generar Mensaje JSON para invocacion tuxedo
				msgTuxedo.setSvc("CANC_EMPL");
				msgTuxedo.addFldEnv(FMLTable.BUFFER,trama);

				msgTuxedo.addFldRec(FMLTable.BUFFER, "");

				//Envio y recepcion de Mensajes por MQ
				try {
					mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

					respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				}catch(Exception e)
				{
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);

				}
				//parseo de mensaje JSON
				try {
					msgResponse = new MsgTuxedo(respuesta);
				} catch (JSONParserException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

				}
				//extraccion de parametros del mensaje de JSON
				try {
					htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.BUFFER) );

				} catch (MsgTuxedoException e) {
					strError = e.getMessage();
					EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				}
			} finally {
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
				mqTux.close();
				mqTux = null;
			}
			return htResultado;
		}

		/**
		 * finalize.
		 *
		 * @throws Throwable the throwable
		 */
		protected void finalize() throws Throwable
		{
			super.finalize();
			msgTuxedo = null;
			if(mqTux != null) {
				mqTux.close();
			}
			mqTux = null;
		}
/**
 * metodo sbitacoraSUALC
 * @param numeroReferencia valor de referencia
 * @param coderror1 dodigo de error
 * @param contrato contrato del cliente
 * @param usuario valor usuario
 * @param cuenta valor de cuenta
 * @param importe valor de importe
 * @param tipo_operacion tipo de operacion
 * @param cuenta2 contrato
 * @param cant_titulos cantidad de titulos
 * @param dispositivo2 dispositivo 2
 * @param folioSUA valor de folio SUA
 * @return respuesta respuesta de bitacora
 * @throws Exception control de excepciones
 */
		public Hashtable sbitacoraSUALC( int  numeroReferencia, String coderror1, String contrato, String usuario, String cuenta, double importe,
				String tipo_operacion, String cuenta2, int cant_titulos, String dispositivo2 ,String folioSUA)
			throws Exception
			{
			EIGlobal.mensajePorTrace("ServicioTux::sbitacora", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[SBITACORA]", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace ("Trama BITACORA = >>>"+"EWEB"+"|"+contrato+"|"+tipo_operacion+"|"+numeroReferencia+"|"+importe+"|"+usuario+"|"+coderror1+"|"+cuenta+"|"+cuenta2+"|"+"390"+"|"+"390"+"|"+"0"+"|"+cant_titulos+"|"+dispositivo2+"|"+ folioSUA +"<<<", EIGlobal.NivelLog.ERROR);

			//String codigo;
			String respuesta ="";
			String err_cod = "";

			Hashtable htResultado = new Hashtable( 2 );
			msgTuxedo = new MsgTuxedo();
			MsgTuxedo msgResponse = null;
			String strError = "";
			try {
			//Generar Mensaje JSON para invocacion tuxedo
			msgTuxedo.setSvc("SBITACORA");

			msgTuxedo.addFldEnv(FMLTable.MEDIO_ENTREGA,"EWEB");
			msgTuxedo.addFldEnv(FMLTable.CONTRATO,contrato);
			msgTuxedo.addFldEnv(FMLTable.TIPO_OPERACION,tipo_operacion);
			msgTuxedo.addFldEnv(FMLTable.REFERENCIA,numeroReferencia);
			msgTuxedo.addFldEnv(FMLTable.IMPORTE,importe);
			msgTuxedo.addFldEnv(FMLTable.USUARIO,usuario);
			msgTuxedo.addFldEnv(FMLTable.COD_ERROR,coderror1);
			msgTuxedo.addFldEnv(FMLTable.CUENTA,cuenta );
			msgTuxedo.addFldEnv(FMLTable.RUTA,(short)390 );
			msgTuxedo.addFldEnv(FMLTable.CANT_TITULOS,cant_titulos);
			msgTuxedo.addFldEnv(FMLTable.PROTECCION, folioSUA);
			msgTuxedo.addFldRec(FMLTable.COD_ERROR, "");

			//Envio y recepcion de Mensajes por MQ
			try {
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				EIGlobal.mensajePorTrace("msgTuxedo.toString()=["+msgTuxedo.toString()+"]", EIGlobal.NivelLog.INFO);
			}catch(Exception e)
			{
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			//parseo de mensaje JSON
			try {
				msgResponse = new MsgTuxedo(respuesta);
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);

			}

			//extraccion de parametros del mensaje de JSON
			try {

				err_cod = msgResponse.getFldRecAsString(FMLTable.COD_ERROR);
				EIGlobal.mensajePorTrace ("ERR_COD ServicioTux.java.. = >>>" + err_cod + "<<<", EIGlobal.NivelLog.INFO);
				if(err_cod ==null || err_cod == "" ){
					err_cod ="Error en el servicio Tuxedo Bitacora 999";}
				htResultado.put( "COD_ERROR", err_cod );

			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
			}
			} finally {
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
			return htResultado;
		}



		/**
		 * Consulta el detalle de transferencias
		 * @param trama datos de entrada
		 * @param servicio tipo de operacion
		 * @return tabla de resultado
		 * @throws Exception descripcion excepcion
		 */
		public Hashtable tranEstatDet(String trama, String servicio) throws Exception {
			String strError = "";
			String respuesta ="";
			MsgTuxedo msgResponse = null;
			Hashtable htResultado = new Hashtable( 2 );
			EIGlobal.mensajePorTrace("ServicioTux::**TRAN_ESTAT_DET***", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[servicio]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);

			try{
				// Generar Mensaje JSON para invocacion tuxedo
				msgTuxedo = new MsgTuxedo();

				// Asignaci�n de servicio
				msgTuxedo.setSvc(servicio);

				// Parametros de entrada
				msgTuxedo.addFldEnv(FMLTable.CARRAY,trama);
				msgTuxedo.addFldRec(FMLTable.CARRAY,"");

				// Envio de Mensajes por MQ
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				// Recepcion de Mensajes por MQ
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				if(respuesta.equals("TIMEOUT")){
					throw new MQQueueSessionException("MQQueueSessionExceptionTimeOut");
				}

				// Parseo de mensaje JSON
				msgResponse = new MsgTuxedo(respuesta);

				// Extraccion de parametros del mensaje de JSON
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.CARRAY) );
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}catch(Exception e){
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}finally{
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
			return htResultado;
		}

		//**********Inicia TCS **********
		/**
		 * Consulta el detalle de transferencias
		 * @param trama datos de entrada
		 * @param servicio tipo de operacion
		 * @return tabla de resultado
		 * @throws Exception descripcion excepcion
		 */
		public Hashtable tranInfoDet(String trama, String servicio) throws Exception {
			String strError = "";
			String respuesta ="";
			MsgTuxedo msgResponse = null;
			Hashtable htResultado = new Hashtable( 2 );
			EIGlobal.mensajePorTrace("ServicioTux::**TRAN_INFO_DET***", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[servicio]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);
			
			try{
				// Generar Mensaje JSON para invocacion tuxedo
				msgTuxedo = new MsgTuxedo();

				// Asignaci�n de servicio
				msgTuxedo.setSvc(servicio);

				// Parametros de entrada
				msgTuxedo.addFldEnv(FMLTable.CARRAY,trama);
				msgTuxedo.addFldRec(FMLTable.CARRAY,"");
				EIGlobal.mensajePorTrace("Se se obtiene msgTuxedo  ======>  "+msgTuxedo.toString(), EIGlobal.NivelLog.DEBUG);
				// Envio de Mensajes por MQ
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				//mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
				// Recepcion de Mensajes por MQ
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				EIGlobal.mensajePorTrace("Se respuesta MQ  ======>  "+respuesta, EIGlobal.NivelLog.DEBUG);
				if(respuesta.equals("TIMEOUT")){
					throw new MQQueueSessionException("MQQueueSessionExceptionTimeOut");
				}

				// Parseo de mensaje JSON
				msgResponse = new MsgTuxedo(respuesta);

				// Extraccion de parametros del mensaje de JSON
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.CARRAY) );
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}finally{
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
			return htResultado;
		}

		//**********FIN TCS **********
		
		

		/**
		 * M�todo encargado de llamar al servicio TRAN_TRANSFE
		 *
		 * @param trama				Trama de entrada
		 * @param servicio			Servicio a llamar
		 * @return htResultado		HashTable con respuesta
		 * @throws Exception		Error de comunicaci�n
		 */
		public Hashtable tranTransfe(String trama, String servicio) throws Exception {
			String strError = "";
			String respuesta ="";
			MsgTuxedo msgResponse = null;
			Hashtable htResultado = new Hashtable( 2 );
			EIGlobal.mensajePorTrace("ServicioTux::TRAN_TRANSFE", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Servicio:[servicio]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("trama:[" + trama + "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("servicio:[" + servicio + "]", EIGlobal.NivelLog.INFO);

			try{
				// Generar Mensaje JSON para invocacion tuxedo
				msgTuxedo = new MsgTuxedo();

				// Asignaci�n de servicio
				msgTuxedo.setSvc(servicio);

				// Parametros de entrada
				msgTuxedo.addFldEnv(FMLTable.CARRAY,trama);
				msgTuxedo.addFldRec(FMLTable.CARRAY,"");

				// Inicializaci�n de MQ
				mqTux = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);

				// Envio y Recepci�n de Mensajes por MQ
				respuesta = mqTux.enviaRecibeMensaje(msgTuxedo.toString());
				if("TIMEOUT".equals(respuesta)){
					throw new MQQueueSessionException("MQQueueSessionExceptionTimeOut");
				}

				// Parseo de mensaje JSON
				msgResponse = new MsgTuxedo(respuesta);

				// Extraccion de parametros del mensaje de JSON
				htResultado.put( "BUFFER", msgResponse.getFldRecAsString(FMLTable.CARRAY) );
			} catch (JSONParserException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			} catch (MsgTuxedoException e) {
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}catch(Exception e){
				strError = e.getMessage();
				EIGlobal.mensajePorTrace ( "ERROR::TUXEDO::" + strError + ":" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace ( "ERROR::RESPUESTA::" + respuesta + ":" , EIGlobal.NivelLog.ERROR);
			}finally{
				mqTux.close();
				mqTux = null;
				respuesta = null;
				msgResponse = null;
				strError = null;;
				msgTuxedo = null;
			}
			return htResultado;
		}
}