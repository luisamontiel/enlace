package mx.altec.enlace.servicios;

public class NIPManagerServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	public NIPManagerServiceException() {

	}

	public NIPManagerServiceException(String arg0) {
		super(arg0);
	}
	/*public NIPManagerServiceException(Throwable arg0) {
		super(arg0);
	}
	public NIPManagerServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}*/
}

