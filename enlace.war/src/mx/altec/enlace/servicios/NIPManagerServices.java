package mx.altec.enlace.servicios;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import mx.altec.enlace.servicios.StringEncrypter;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class NIPManagerServices {
	/**
	 * @objetive Consultar el estado de un OTP
	 * @param codClient C�digo del cliente que consulta el estado de su OTP
	 * @param canal Canal donde se ejecutar� la consulta de estado
	 * @return Cadena con los resultados de la consulta de estado
	 * @author  Marcelo Fontan
	 * @throws NIPManagerServiceException
	 * @Date    4/IX/2006
	 * @version   1.0 */
	public static String consultaEstado(String codClient, String canal) throws NIPManagerServiceException{
		try {
			String [] params = {codClient, canal};
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
			//DataInputStream input = runService(Global.OTP_CONSULTARESTADO, params);
			java.io.BufferedReader input =runService(Global.OTP_CONSULTARESTADO, params);
//			VSWF RRG 08-Dic-2008 F
 			String result = extractResult(input);
			input.close();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NIPManagerServiceException(e.getMessage());
		}
	}
	/**
	 * @objetive Solicitar un OTP
	 * @param codigoCliente C�digo del cliente que solicita su OTP
	 * @param canalS Canal donde se ejecutar� la consulta de estado
	 * @param cuentaCargo parametros de cifrado
	 * @param contratoEnlace parametros de cifrado
	 * @param codigoClienteEmpresa parametros de cifrado
	 * @return Cadena con los resultados de la solicitud
	 * @author  Marcelo Fontan
	 * @throws NIPManagerServiceException
	 * @Date    4/IX/2006
	 * @version   1.0 */
	public static String solicitud(String codigoCliente,String canalS, String cuentaCargo, String contratoEnlace,
	  String codigoClienteEmpresa) throws NIPManagerServiceException {
		try {
			String [] params = {codigoCliente, canalS, cuentaCargo, contratoEnlace, codigoClienteEmpresa};
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
			//DataInputStream input = runService(Global.OTP_SOLICITAR, params);
			java.io.BufferedReader input =runService(Global.OTP_SOLICITAR, params);
//VSWF RRG 08-Dic-2008 F
 			String result = extractResult(input);
			input.close();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NIPManagerServiceException(e.getMessage());
		}
	}
	/**
	 * @objetive Activar en el sistema un OTP
	 * @param serieToken token del OTP para activarlo
	 * @param codigoClienteA C�digo del cliente que desea activar su OTP
	 * @return Cadena con los parametros cifrados
	 * @author  Marcelo Fontan
	 * @throws NIPManagerServiceException
	 * @Date    4/IX/2006
	 * @version   1.0 */
	public static String activar(String serieToken, String codigoClienteA, String Canal) throws NIPManagerServiceException {
		try {
			String [] params = {serieToken, codigoClienteA, Canal};
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
			//DataInputStream input = runService(Global.OTP_ACTIVAR, params);
			java.io.BufferedReader input =runService(Global.OTP_ACTIVAR, params);
//VSWF RRG 08-Dic-2008 F
 			String result = extractResult(input);
			input.close();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NIPManagerServiceException(e.getMessage());
		}
	}
	/**
	 * @objetive Bloquear en el sistema un OTP
	 * @param params parametros de cifrado
	 * @return Cadena con los parametros cifrados
	 * @author  Marcelo Fontan
	 * @throws EncryptionException
	 * @Date    4/IX/2006
	 * @version   1.0 */
	public static String bloquear(String codigoCliente,String canal,String motivoBloqueo) throws NIPManagerServiceException {
		try {
			String [] params = {codigoCliente, canal, motivoBloqueo};
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
			//DataInputStream input = runService(Global.OTP_BLOQUEAR, params);
			java.io.BufferedReader input =runService(Global.OTP_BLOQUEAR, params);
//VSWF RRG 08-Dic-2008 F
 			String result = extractResult(input);
			input.close();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NIPManagerServiceException(e.getMessage());
		}
	}
	/**
	 * @objetive Solicitar el folio de seguridad de un OTP
	 * @param params parametros de cifrado
	 * @return Cadena con los parametros cifrados
	 * @author  Marcelo Fontan
	 * @throws EncryptionException
	 * @Date    4/IX/2006
	 * @version   1.0 */
	public static String solFolioSeguridad(String codigoCliente,String canal) throws NIPManagerServiceException {
		try {
			String [] params = {codigoCliente, canal};
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
			//DataInputStream input = runService(Global.OTP_FOLIOSEGURIDAD, params);
			java.io.BufferedReader input =runService(Global.OTP_FOLIOSEGURIDAD, params);
//VSWF RRG 08-Dic-2008 F
 			String result = extractResult(input);
			input.close();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NIPManagerServiceException(e.getMessage());
		}
	}
	/**
	 * @objetive extrae los datos de un mensaje recibido de Nipmanager
	 * @param params parametros de cifrado
	 * @return Cadena con los parametros cifrados
	 * @author  Marcelo Fontan
	 * @throws EncryptionException
	 * @Date    4/IX/2006
	 * @version   1.0 */
//	VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
	private static String extractResult(java.io.BufferedReader input) throws IOException, EncryptionException {
//		VSWF RRG 08-Dic-2008 F
		StringEncrypter algoritmo=new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME);
		String result="";
		String str="";
		while (null != ((str = input.readLine()))) {
			if (str.lastIndexOf("|")>1) {
				result=str;
				break;
			}
			result=str;
		}
		result=algoritmo.decrypt(result);
		return result;
	}
	 /**
	 * @objetive Envia mensaje por HTTP
	 * @param params parametros de cifrado
	 * @return Cadena con los parametros cifrados
	 * @author  Marcelo Fontan
	 * @throws EncryptionException, IOException
	 * @Date    4/IX/2006
	 * @version   1.0 */
//	VSWF RRG 08-Dic-2008 I Se modifico el metodo para quitar la classe deprecada, para migracion Enlace
	private static java.io.BufferedReader runService(String strUrl, String [] params) throws EncryptionException, IOException {
		URL url= new URL(strUrl);
		URLConnection urlConn=url.openConnection();
		String tramaParaLog = "|";
//VSWF RRG 08-Dic-2008 I Se le agrego el segundo parametro al metodo decode, para migracion Enlace	
		String enc="UTF-8";
//VSWF RRG 08-Dic-2008 F
		for(int i = 0; i < params.length; i++) {
		   tramaParaLog += params[i] + "|";
		}
		EIGlobal.mensajePorTrace("URL NIP Manager: <" + strUrl + ">", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Trama para NIP Manager: " + tramaParaLog, EIGlobal.NivelLog.INFO);
		String encriptParam=encryptParameters(params);
		String postParams="t="+URLEncoder.encode(encriptParam,enc);
		urlConn.setDoInput(true);
		urlConn.setDoOutput(true);
		urlConn.setUseCaches(false);//Deshabilitar cache
		urlConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		DataOutputStream printout=new DataOutputStream(urlConn.getOutputStream());
		printout.writeBytes(postParams);
		printout.flush();
		printout.close();
		return new java.io.BufferedReader(new InputStreamReader(urlConn.getInputStream()));
	}
//	VSWF RRG 08-Dic-2008 F
	/**
	 * @objetive Obtinene un conjunto de propiedades de un archivo especificado
	 * @param params parametros de cifrado
	 * @return Cadena con los parametros cifrados
	 * @author  Marcelo Fontan
	 * @throws EncryptionException
	 * @Date    4/IX/2006
	 * @version   1.0 */
	private static String encryptParameters(String[] params) throws EncryptionException {
		StringEncrypter algoritmo = new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME);
		StringBuffer param = new StringBuffer("|");
		for (int i = 0; i < params.length; i++) {
			param.append(params[i]+"|");
		}
		return algoritmo.encrypt(param.toString());
	}
}