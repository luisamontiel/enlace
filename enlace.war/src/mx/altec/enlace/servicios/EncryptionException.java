package mx.altec.enlace.servicios;

/**
 * @objetive Envuelve excepciones particulares de encripci�n
 * @author Yves Alberto S�nchez Figueroa
 * @Date 20/VI/2006
 * @version 1.0
 */

public class EncryptionException extends Exception {
	static final long serialVersionUID = 1;

	public EncryptionException(String t) {
		super(t);
	}
}
