package mx.altec.enlace.servicios;

import javax.jms.QueueConnectionFactory;
import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.*;
import javax.sql.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 * The Class ServiceLocator.
 */
public class ServiceLocator {

    /** The instance. */
    private static ServiceLocator instance = null;

	private Map<String, QueueConnectionFactory> connFactoryMap = null;

	private Map<String, Queue> queueMap = null;

	private Map<String, DataSource> dsMap = null;

	private InitialContext ic = null;

    /**
     * Instantiates a new service locator.
     */
    private ServiceLocator() {

	try {
	    ic = new InitialContext();
		connFactoryMap = Collections.synchronizedMap(new HashMap());
		queueMap = Collections.synchronizedMap(new HashMap());
		dsMap = Collections.synchronizedMap(new HashMap());
	} catch (NamingException e) {
	    e.printStackTrace();
	    EIGlobal.mensajePorTrace("isban DataAccess Error: Hay un problema para obtener los recursos JNDI" +
		    ".(Revisar que este creados o que el archivo de configuracion sea el correcto.", EIGlobal.NivelLog.ERROR);
	}

    }

    /**
     * Gets the single instance of ServiceLocator.
     * @return single instance of ServiceLocator
     */
    public static ServiceLocator getInstance() {

	if (instance == null) {
	    instance = new ServiceLocator();
	}

	return instance;

    }

    /**
     * Gets the conn factory online.
     * @return the conn factory online
     */
    public QueueConnectionFactory getConnFactory(String nombre) throws NamingException {
      QueueConnectionFactory factory = null;
      factory = connFactoryMap.get(nombre);
      if(factory == null) {
          factory = (QueueConnectionFactory) ic.lookup(nombre);
          connFactoryMap.put(nombre, factory);
          EIGlobal.mensajePorTrace("Lookup de " + nombre + " en InitialContext. ", EIGlobal.NivelLog.INFO);
      }
      return factory;
    }

    /**
     * Gets the queue reciever online.
     * @return the queue reciever online
     */
    public Queue getQueue(String nombre) throws NamingException{
		Queue queue = null;
		queue = queueMap.get(nombre);
		if(queue == null) {
            queue = (Queue) ic.lookup(nombre);
            queueMap.put(nombre, queue);
            EIGlobal.mensajePorTrace("Lookup de " + nombre + " en InitialContext. ", EIGlobal.NivelLog.INFO);
        }
		return queue;
    }

    /**
     * Gets the DataSource.
     * @return the queue reciever online
     */
    public DataSource getDataSource(String nombre) throws NamingException {
		DataSource ds = null;
		ds = dsMap.get(nombre);
		if(ds == null) {
            ds = (DataSource) ic.lookup(nombre);
            dsMap.put(nombre, ds);
            EIGlobal.mensajePorTrace("Lookup de " + nombre + " en InitialContext. ", EIGlobal.NivelLog.INFO);
        }
		return ds;
    }

}
