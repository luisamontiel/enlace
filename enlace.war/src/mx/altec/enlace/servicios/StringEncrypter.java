package mx.altec.enlace.servicios;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.security.spec.KeySpec;
import java.util.Properties;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

import mx.altec.enlace.bo.Base64;
import mx.altec.enlace.utilerias.Global;

public class StringEncrypter {
	public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
	public static final String DES_ENCRYPTION_SCHEME = "DES";
	private static final String UNICODE_FORMAT = "UTF8";
	public static final String CLAVE_INTERFAZ_OTP = "claveOTP";
	private KeySpec keySpec;
	private SecretKeyFactory keyFactory;
	private Cipher cipher;
	/**
	 * @objetive Constructor del cifrador
	 * @param encryptionScheme esquema de cifrado
	 * @author Yves Alberto S�nchez Figueroa
	 * @Date 18/VI/2006
	 * @version 1.0
	 */
	public StringEncrypter(String encryptionScheme) throws EncryptionException {
		this(encryptionScheme, getproperties().getProperty(CLAVE_INTERFAZ_OTP));
	}
	/**
	 * @objetive Constructor del cifrador
	 * @param encryptionScheme Esquema de cifrado
	 * @param encryptionKey Llave de cifrado
	 * @author Yves Alberto S�nchez Figueroa
	 * @Date 20/VI/2006
	 * @version 1.0
	 */
	public StringEncrypter(String encryptionScheme, String encryptionKey)
			throws EncryptionException {
		if (encryptionKey == null)
			throw new IllegalArgumentException("La clave de cifrado es nula");
		if (encryptionKey.trim().length() < 24)
			throw new IllegalArgumentException("La clave de cifrado es menor a 24 caracteres");
		try {
			byte[] keyAsBytes = encryptionKey.getBytes(UNICODE_FORMAT);
			if (encryptionScheme.equals(DESEDE_ENCRYPTION_SCHEME)) {
				keySpec = new DESedeKeySpec(keyAsBytes);
			} else if (encryptionScheme.equals(DES_ENCRYPTION_SCHEME)) {
				keySpec = new DESKeySpec(keyAsBytes);
			} else {
				throw new IllegalArgumentException(
						"No se soporta el Esquema de cifrado: " + encryptionScheme);
			}
			keyFactory = SecretKeyFactory.getInstance(encryptionScheme);
			cipher = Cipher.getInstance(encryptionScheme);
		} catch (InvalidKeyException e) {
			throw new EncryptionException(e.getMessage());
		} catch (UnsupportedEncodingException e) {
			throw new EncryptionException(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException(e.getMessage());
		} catch (NoSuchPaddingException e) {
			throw new EncryptionException(e.getMessage());
		}
	}
	/**
	 * @objetive cifra una cadena con una llave determinada, codificando
	 *           internamente con el algoritmo base64
	 * @param unencryptedString  Cadena a cifrar
	 * @return String Cadena encriptada
	 * @author Yves Alberto S�nchez Figueroa
	 * @Date 20/VI/2006
	 * @version 1.0
	 */
	public String encrypt(String unencryptedString) throws EncryptionException {
		if (unencryptedString == null || unencryptedString.trim().length() == 0)
			throw new IllegalArgumentException("La cadena descifrada era nula o estaba vacia");
		try {
			SecretKey key = keyFactory.generateSecret(keySpec);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] cleartext = unencryptedString.getBytes(UNICODE_FORMAT);
			byte[] ciphertext = cipher.doFinal(cleartext);
			return Base64.encodeBytes(ciphertext);
		} catch (Exception e) {
			throw new EncryptionException(e.getMessage());
		}
	}
	/**
	 * @objetive Descifra una cadena con una llave determinada,
	 *           descodificando internamnte con el algoritmo base64
	 * @param encryptedString Cadena a descifrar
	 * @return String Cadena descifrada
	 * @author Yves Alberto S�nchez Figueroa
	 * @Date 20/VI/2006
	 * @version 1.0
	 */
	public String decrypt(String encryptedString) throws EncryptionException {
		if (encryptedString == null || encryptedString.trim().length() <= 0)
			throw new IllegalArgumentException(
					"La cadena descifrada era nula o estaba vacia");
		try {
			SecretKey key = keyFactory.generateSecret(keySpec);
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] cleartext = Base64.decode(encryptedString);
			byte[] ciphertext = cipher.doFinal(cleartext);
			return new String(ciphertext, UNICODE_FORMAT);
		} catch (Exception e) {
			throw new EncryptionException(e.getMessage());
		}
	}
	/**
	 * @objetive Obtinene un conjunto de propiedades de un archivo especificado
	 * @return  Properties	propiedades del archivo de inicializaci�n
	 * @author  Yves Alberto S�nchez Figueroa
	 * @Date    17/VI/2006
	 * @version   1.0 */
	public static Properties getproperties() {
		Properties props=new Properties();
		String llave="";
		StringBuffer stringOriginal=new StringBuffer("");
		StringEncrypter encrypter=null;
		InputStream decryptedString=null;
		try {
			llave=InetAddress.getLocalHost()+Global.OTP_NOMBRE_INI+Global.OTP_NOMBRE_INI;
			encrypter = new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME, llave);
			BufferedReader br = new BufferedReader(new FileReader(Global.OTP_RUTA_INI+Global.OTP_NOMBRE_INI));
			while (br.ready()) {
				stringOriginal.append(br.readLine()+"\n");
			}
			decryptedString = new ByteArrayInputStream(encrypter.decrypt(stringOriginal.toString()).getBytes());
			props.load(decryptedString);
		} catch(IOException ioe) {
		     System.out.println("Error reading file: "+ioe.getMessage());
		} catch(EncryptionException ee) {
		     System.out.println("Error encrypting file: "+ee.getMessage());
		}
		return props;
	}
	/**
	 * @objetive cifra el archivo ini al especificado en el archivo de configuraci�n
	 * @param  	archivoOrigen archivo origen a cifrar
	 * @return  String	C�digo del mensaje | descripci�n del mensaje, ya sea de �xito o error
	 * @author  Yves Alberto S�nchez Figueroa
	 * @Date    17/VI/2006
	 * @version   1.0 */
	public static void encrypt2File(String archivoOrigen) {
		String llave="";
		StringBuffer stringOriginal=new StringBuffer("");
		StringEncrypter encrypter=null;
		String encryptedString="";
		try {
			FileWriter fw=new FileWriter(Global.OTP_RUTA_INI+Global.OTP_NOMBRE_INI);
			llave=InetAddress.getLocalHost()+Global.OTP_NOMBRE_INI+Global.OTP_NOMBRE_INI;
			encrypter = new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME, llave);
			BufferedReader br = new BufferedReader(new FileReader(Global.OTP_RUTA_INI+archivoOrigen));
			while (br.ready()) {
				stringOriginal.append(br.readLine()+"\n");
			}
			encryptedString=encrypter.encrypt(stringOriginal.toString());
			fw.write(encryptedString);
			fw.close();
		} catch(IOException ioe) {
		     System.out.println("Error writing to file: "+ioe.getMessage());
		} catch(EncryptionException ee) {
		     System.out.println("Error encrypting: "+ee.getMessage());
		}
	}
	/**
	 * @objetive Obtinene una cadena hash SHA a 256 bits de una secuencia de bytes dada
	 * @param  	input flujo binario de datos
	 * @return  String	hash del flujo de datos
	 * @author  Yves Alberto S�nchez Figueroa
	 * @Date    25/IX/2006
	 * @version   1.0 */
	public String getSHA256(byte[] input) throws Exception {
        MessageDigest hash = MessageDigest.getInstance("SHA-256");//JCEProvider
        byte[] digest = hash.digest(input);
        return Base64.encodeBytes(digest);
    }
}