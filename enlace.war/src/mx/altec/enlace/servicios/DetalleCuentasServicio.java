package mx.altec.enlace.servicios;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.DetalleCuentasBean;
import mx.altec.enlace.beans.TSE1Trans;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.dao.DetalleCuentasDAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.DetalleCuentasException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import mx.altec.enlace.beans.TrxOD13Bean;
import mx.altec.enlace.dao.TrxOD13DAO;


public class DetalleCuentasServicio {
	private static final int DIAS_VIGENCIA = 30;
    private static final String ESTADO_PROCESADO_EN_PROCESO = "F";
    private static final String ESTADO_PENDIENTE_POR_ACTUALIZAR = "PA";

	public List<DetalleCuentasBean> obtenerCuentasNoRegistradas(List<DetalleCuentasBean> listaBuscar, String contrato)
			throws DetalleCuentasException{
		List<DetalleCuentasBean> listaRegresar = null;
		DetalleCuentasDAO dao = new DetalleCuentasDAO();
		MQQueueSession mqSession = null;

		try{

			for (int i=0; i<listaBuscar.size(); i++){
				DetalleCuentasBean bean = listaBuscar.get(i);
				EIGlobal.mensajePorTrace("Buscando cuenta - contrato " + bean.getCuenta() + " - " + contrato,   EIGlobal.NivelLog.INFO);


				boolean existe = dao.existeCuenta(bean.getCuenta().trim(), contrato.trim());

				EIGlobal.mensajePorTrace("Existe -> " + existe,   EIGlobal.NivelLog.INFO);
				if (!existe){
					if (listaRegresar==null){
						listaRegresar=new ArrayList<DetalleCuentasBean>();
					}

					/*VMB Recupera nombre del titular en la cuenta*/
					EIGlobal.mensajePorTrace("** Modulo - Alta por Archivo **", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("** DetalleCuentasServicio - obtenerCuentasNoRegistradas **", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("** Datos de la cuenta **", EIGlobal.NivelLog.DEBUG);

					EIGlobal.mensajePorTrace("Apellido Paterno: " + bean.getaPaterno().toString(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("Apellido Materno: " + bean.getaMaterno().toString(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("Nombre: " + bean.getNombre().toString(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("Numero de cuenta: " + bean.getCuenta().toString(), EIGlobal.NivelLog.DEBUG);

					if ( bean.getNombre().toString().trim()!=null && ! bean.getNombre().toString().trim().equals("")){

						if (bean.getaPaterno().toString().trim()!=null && !bean.getaPaterno().toString().trim().equals("") ||
								bean.getaMaterno().toString().trim()!=null && bean.getaMaterno().toString().trim().equals("") )
							{
							bean.setaMaterno(bean.getaMaterno().toString());
							bean.setaPaterno(bean.getaPaterno().toString());
							bean.setNombre(bean.getNombre().toString());
								EIGlobal.mensajePorTrace("Empleado - nombre completo: " + bean.getNombre() + " "
										+ bean.getaPaterno() + " "
										+ bean.getaMaterno() + " "
										, EIGlobal.NivelLog.DEBUG);
						}
					}else{
						try {

							/*Recuperar el titular con la Trx TSE1*/
							mqSession = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
							TSE1Trans respuesta = new TSE1Trans();

							     EIGlobal.mensajePorTrace("** DetalleCuentasServicio - obtenerCuentasNoRegistradas :: TSE1 Consulta Titular  **",
							    		 EIGlobal.NivelLog.DEBUG);
								 respuesta = dao.obtenerDatosCuenta(mqSession, bean.getCuenta().toString().trim());

							  if (respuesta.getRazonSocial().toString().trim()!=null && !respuesta.getRazonSocial().toString().trim().equals("")){
								 bean.setNombre((respuesta.getRazonSocial() == null ? "" : respuesta.getRazonSocial()));
								 EIGlobal.mensajePorTrace("** DetalleCuentasServicio - obtenerCuentasNoRegistradas :: TSE1 Nombre Titular: "
										 + bean.getNombre()+ " **", EIGlobal.NivelLog.DEBUG);
								 bean.setaMaterno("");
								 bean.setaPaterno("");
								 respuesta = null;
								}else
										 {
											 bean.setNombre("");
											 bean.setaMaterno("");
											 bean.setaPaterno("");
										 }

						 } catch (Exception e) {
								EIGlobal.mensajePorTrace("** DetalleCuentasServicio - obtenerCuentasNoRegistradas :: Ocurrio un error al consultar el detalle de las cuentas no registradas", EIGlobal.NivelLog.DEBUG);
								e.printStackTrace();

					        } finally {

					        	if (mqSession != null) {
					        		mqSession.close();
					        	}
					        }
					}

					listaRegresar.add(bean);
				}
			}
			dao=null;
		}catch(SQLException sqlEx){

			EIGlobal.mensajePorTrace("DetalleCuentasServicio::obtenerCuentasNoRegistradas ->" +
					"Error SQL en obtenerCuentasNoRegistradas -> " + sqlEx.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new DetalleCuentasException("Se produjo un error al obtener las cuentas no registradas");

		}catch(Exception e){

			EIGlobal.mensajePorTrace("DetalleCuentasServicio::obtenerCuentasNoRegistradas ->" +
					"Error en obtenerCuentasNoRegistradas -> " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new DetalleCuentasException("Se produjo un error al obtener las cuentas no registradas");

		}

		return listaRegresar;
	}



	public long insertarContrato( String contrato) throws DetalleCuentasException{
		DetalleCuentasDAO dao = new DetalleCuentasDAO();
		long folioContrato;
		try{
			folioContrato = dao.insertarContrato( contrato);
		}catch(SQLException sqlEx){
			EIGlobal.mensajePorTrace("DetalleCuentasServicio::insertarContrato ->" +
					"Error SQL en insertarContrato -> " + sqlEx.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new DetalleCuentasException("Se produjo un error al insertar contrato.");
		}catch(Exception e){
			EIGlobal.mensajePorTrace("DetalleCuentasServicio::insertarContrato ->" +
					" Error en insertar contrato -> " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new DetalleCuentasException("Se produjo un error al insertar contrato.");
		}
		dao=null;
		return folioContrato;
	}

	public void insertarCuenta(DetalleCuentasBean detalleCuentasBean, String contrato, long folioContrato) throws DetalleCuentasException{
		DetalleCuentasDAO dao = new DetalleCuentasDAO();
		try{
			dao.insertarCuenta(detalleCuentasBean, contrato, folioContrato);
		}catch(SQLException sqlEx){
			EIGlobal.mensajePorTrace("DetalleCuentasServicio::insertarCuenta ->" +
					"Error SQL en insertarCuenta -> " + sqlEx.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new DetalleCuentasException("Se produjo un error al insertar cuenta.");
		}catch(Exception e){
			EIGlobal.mensajePorTrace("DetalleCuentasServicio::insertarCuenta ->" +
					" Error en insertar cuenta -> " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new DetalleCuentasException("Se produjo un error al insertar cuenta.");
		}
		dao=null;
	}

	public List<DetalleCuentasBean> consultarArchivosCuentasNoRegistradas(String contrato)throws DetalleCuentasException{
    	Connection conn = null;

        try {
        	conn = getConnection();
        	DetalleCuentasDAO dao = new DetalleCuentasDAO();
        	List<DetalleCuentasBean> resultado = dao.obtenArchivos(getConnection2(), contrato, DIAS_VIGENCIA);
            return resultado;

        } catch (Exception e) {
        	EIGlobal.mensajePorTrace("DetalleCuentasServicio::consultarArchivosCuentasNoRegistradas ->" +
					" Error al consultar archivos con cuentas no registradas -> " + e.getMessage(), EIGlobal.NivelLog.ERROR);
        	throw new DetalleCuentasException("Ocurrio un error al consultar los archivos con cuentas no registradas");
        }finally {
            close(conn);
        }
    }

    public List<DetalleCuentasBean> consultarCuentasDeFolio(String contrato, int folio)throws DetalleCuentasException{
    	Connection conn = null;
        List<DetalleCuentasBean> resultado = null;
        DetalleCuentasDAO dao = null;

        try {
        	conn = getConnection();
            dao = new DetalleCuentasDAO();
            resultado = dao.obtenDetalleCuentas(conn, contrato, folio, DIAS_VIGENCIA);
        } catch (Exception e) {
        	EIGlobal.mensajePorTrace("DetalleCuentasServicio::consultarCuentasDeFolio ->" +
					" Error al consultar las cuentas no registradas -> " + e.getMessage(), EIGlobal.NivelLog.ERROR);
        	throw new DetalleCuentasException("Ocurrio un error al consultar las cuentas no registradas");
        } finally {
            close(conn);
        }

        if (resultado == null || resultado.isEmpty()) {
        	return resultado;
        }

        MQQueueSession mqSession = null;

        try {

        	mqSession = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);

        	for (DetalleCuentasBean detalle : resultado) {

        		try {
					if(detalle.getCuentaLiberar().trim().length()==10){
						TrxOD13DAO gDato = new TrxOD13DAO();
						TrxOD13Bean BeanOD13 =  new TrxOD13Bean();
						String tramaOD13 = gDato.ejecutaConsultaDescripcion(detalle.getCuentaLiberar().trim()).toString();

						EIGlobal.mensajePorTrace("CatNominaInterbDAO-> altaInterbArchivo() : TRAMA OD13: "+tramaOD13, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("CatNominaInterbDAO-> altaInterbArchivo() : NOMBRE REAL: "+tramaOD13, EIGlobal.NivelLog.INFO);

						detalle.setDescripcion(tramaOD13);
					}else{
						TSE1Trans tse1 = dao.obtenerDatosCuenta(mqSession, detalle.getCuentaLiberar());
						detalle.setDescripcion((tse1 == null || tse1.getRazonSocial() == null ? "" : tse1.getRazonSocial()));
						tse1 = null;
					}


        		} catch (Exception e) {

        			detalle.setDescripcion("");
        		}
        	}

        } catch (Exception e) {

        	throw new DetalleCuentasException("Ocurrio un error al consultar el detalle de las cuentas no registradas");

        } finally {

        	if (mqSession != null) {
        		mqSession.close();
        	}
        }

        return resultado;
    }

    public void registraCuentasLiberacion(String contrato, int folio, List<String> cuentas, BitaTCTBean bean,BitaHelper bh,int referencia)throws DetalleCuentasException{
    	Connection conn = null;
        try {
        	conn = getConnection();
            conn.setAutoCommit(false);
        	DetalleCuentasDAO dao = new DetalleCuentasDAO();

		  	bean.setReferencia(referencia);
		  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_REGISTRO_EMPLEADOS);
		  	bean.setConcepto(BitaConstants.ES_NOMINA_REGISTRO_EMPLEADOS_MSG);

            for (String cuenta : cuentas) {

            	EIGlobal.mensajePorTrace("Cuenta: " + cuenta, EIGlobal.NivelLog.DEBUG);
            	boolean resultado = dao.actualizaEstadoCuenta(conn, contrato, folio, cuenta, ESTADO_PENDIENTE_POR_ACTUALIZAR);
                bean.setNumCuenta(cuenta);
                bean = bh.llenarBeanTCT(bean);
                bean.setCodError((resultado ? "NOAL0000" : "NOAL9999"));
    		    BitaHandler.getInstance().insertBitaTCT(bean);
            }
            conn.commit();
        } catch (Exception e) {
        	EIGlobal.mensajePorTrace("DetalleCuentasServicio::registraCuentasLiberacion ->" +
					"Error al actualizar el estado de las cuentas. -> " + e.getMessage(), EIGlobal.NivelLog.ERROR);
        	throw new DetalleCuentasException("Error al actualizar el estado de las cuentas.");
        }finally {
            close(conn);
        }
    }

    private Connection getConnection() throws SQLException {
    	return BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
	}

    private Connection getConnection2() throws SQLException {
    	return BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
	}

    private static void close(Connection conn) {
        if (conn != null) {
            try {
                if (!conn.getAutoCommit()) {
                    conn.rollback();
                    conn.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
//*****TERMINA Liberacion de empleados

}