package mx.altec.enlace.export.exception;

/**
 * Excepci&oacute;n para el proceso de exportaci&oacute;n.
 */
public class ExportarException extends Exception {
	
	/**
	 * Constructor de la clase.
	 *
	 * @param message Mensaje de error para la excepci&oacute;n.
	 */
	public ExportarException(String message) {
		super(message);
	}
}