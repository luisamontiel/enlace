package mx.altec.enlace.export;

import java.io.IOException;

/**
 * Define el contrato para crear un exportador de archivos.
 * 
 * @author Stefanini
 *
 */
public interface Exportable {
    
    /**
     * Indica al exportador que hay que moverse al siguiente renglon.
     * @throws IOException si falla el movimiento al siguiente renglon
     */
    void nextRow() throws IOException;
    
    /**
     * Indica al exportador que hay que escribir la siguiente columna.
     * @param col la meta informacion de la columna a exportar
     * @param data la informacion a exportar
     * @throws IOException si falla la escritura de la columna
     */
    void writeColumn(Column col, Object data) throws IOException;
    
    /**
     * Indica al exportador que se cierre y que ha terminado la exportacion. 
     * @throws IOException si falla el cierre del exportador
     */
    void close() throws IOException;

}