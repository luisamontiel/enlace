package mx.altec.enlace.utilerias;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * The Class Util.
 */
public class Util {


	/**
	 * Formatea importe.
	 *
	 * @param importe the importe
	 * @return the string
	 */
	public static String formateaImporte(String importe) {
		String importeFmt = "";
		int i = 0;
		for(i = 0; (i < importe.length()) && (importe.charAt(i) == '0'); i++);
		if(i == importe.length()) {
			return "0.00";
		}
		importeFmt = importe.substring(i);
		if(importeFmt.length() < 1) {
			return "0.00";
		}
		if(importeFmt.length() == 2) {
			importeFmt =  "0." + importeFmt;
		}
		else if(importeFmt.length() == 1) {
			importeFmt = "0.0" + importeFmt;
		}
		else {
			importeFmt = importeFmt.substring(0, importeFmt.length() - 2) + "." + importeFmt.substring(importeFmt.length() - 2);
		}
		return importeFmt;
	}

	/**
	 * Rellena ceros.
	 *
	 * @param importe the importe
	 * @param posiciones the posiciones
	 * @return the string
	 */
	public static String rellenaCeros(String importe, int posiciones) {
		String importeAux = importe;
		double importeconv = 0;
		StringBuffer pattern = new StringBuffer(posiciones + 1);
		for(int i = 0; i < (posiciones - 2); i++) {
			pattern.append('0');
		}
		pattern.append(".00");

		try {
			importeconv = Double.parseDouble(importeAux);
		} catch(NumberFormatException e) {
			importeconv = 0;
		}

		DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
		df.applyPattern(pattern.toString());
		importeAux = df.format(importeconv);

		String tmp[] = importeAux.split("\\.");
		importeAux = tmp[0] + tmp[1];

		return importeAux;
	}

	/**
	 * Metodo para mostrar el tipo de operación de FX Online de cara al usuario
	 *
	 * @param tipoOperacion		-	Tipo de Operación
	 * @return					-	Tipo de Operación Usuario
	 */
	public static String muestraTipoOperUser(String tipoOperacion){
		if("VTA".equals(tipoOperacion.trim())) {
			return "Compra";
		} else if("CPA".equals(tipoOperacion.trim())) {
			return "Venta";
		}
		return "Tipo de Operación";
	}

	/**
	 * Metodo para mostrar el Folio de operación de FX Online de cara al usuario
	 *
	 * @param folioOper			-	Folio de Operación
	 * @param estatusOper		-	Estatus de Operación
	 * @return					-	Folio de Operación Correcto
	 */
	public static String muestraFolioOper(String estatusOper, String folioOper){
		if(estatusOper.trim().equals("L") || estatusOper.trim().equals("O")) {
			return folioOper;
		} else if(estatusOper.trim().equals("P") || estatusOper.trim().equals("C") ||
				estatusOper.trim().equals("D") || estatusOper.trim().equals("N") ) {
			return "";
		}
		return "Folio Operación";
	}

    /**
     * Obtener el valor de importeReal.
     *
     * @param importeAbono the importe abono
     * @param tipoOperacion 	-	Tipo de Operación
     * @param tcPactado the tc pactado
     * @return importeReal valor asignado.
     */
	public static String obtenImporteAbonoReal(String importeAbono, String tipoOperacion, String tcPactado) {
		String importeReal = "";
		try{
			if(tipoOperacion.trim().equals("CPA")){
				importeReal = "" + Double.parseDouble(importeAbono) * Double.parseDouble(tcPactado);
			}else if(tipoOperacion.trim().equals("VTA")){
				importeReal = importeAbono;
			}
			EIGlobal.mensajePorTrace("Util::obtenImporteAbonoReal:: Importe Real de Abono->"+importeReal, EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("Util::obtenImporteAbonoReal:: error->"+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return importeReal;
	}

	/** The Constant numberPattern. */
	private static final Pattern numberPattern = Pattern.compile("[\\d]+");

	/** The Constant datePattern. */
	private static final Pattern datePattern = Pattern.compile("\\d{2}/\\d{2}/\\d{4}");

	/** The Constant moneyPattern. */
	private static final Pattern moneyPattern = Pattern.compile("\\d+.\\d{2}");

	/**
	 * Valida el formato de la cadena de entrada.
	 *
	 * @param parametro cadena a validar
	 * @param opcion tipo de validación
	 * @return true, Si la cadena es valida
	 */
	public static boolean validaCCX(String parametro, int opcion) {
		boolean result = false;

		Pattern p = null;

		switch (opcion) {
		case 1: //Solo digitos
			p = numberPattern;
			break;
		case 3: //Formato de fecha dd/dd/dddd
			p = datePattern;
			break;
		case 4: //Numero con 2 decimales
			p = moneyPattern;
			break;
		}

		result = p.matcher(parametro).matches();
		return result ;
	}
	/**
	 * HtmlEncode Metodo para la realizacion de HtmlEncode
	 * @param str
	 * @return
	 */
	public static String HtmlEncode(String str)
	{
		return HtmlEncode(str, null);
	}
	/**
	 * HtmlEncode Metodo para la realizacion de HtmlEncode 
	 * @param str
	 * @param def
	 * @return
	 */
	public static String HtmlEncode(String str, String def)
	{
		if(str == null || str.length() == 0)
		{
			str = (def == null ? "" : def);
		}
		
		int len = str.length();
		StringBuffer out = new StringBuffer((int)(len*1.5));
		
		// Allow: a-z A-Z 0-9 SPACE , .
		// Allow (dec): 97-122 65-90 48-57 32 44 46
		
		for(int cnt = 0; cnt < len; cnt++)
		{
			char c = str.charAt(cnt);
			if( (c >= 97 && c <= 122) ||
				(c >= 65 && c <= 90 ) ||
				(c >= 48 && c <= 57 ) ||
				c == 32 || c == 44 || c == 46 )
			{
				out.append(c);
			}
			else
			{
				out.append("&#").append((int)c).append(';');
			}
		}
		
		return out.toString();
	}
}