package mx.altec.enlace.utilerias;
/**
 * 
 * @author FSW - Vector
 *  Clase de constantes de Nomina
 *
 */
public class NominaConstantes {
	
	/*Varibles para campos de la tabal EWEB_MX_PRC_SOL_NOM*/
	/**
	 * Constante para "EWEB_MX_PRC_SOL_NOM"
	 */
	public static final String SQL_TABLE_EPSN         = "EWEB_MX_PRC_SOL_NOM";
	/**
	 * Constante para "COD_CONTR_PK"
	 */
	public static final String SQL_EPSN_FLD_CONTR     = "COD_CONTR_PK";
	/**
	 * Constante para "NUM_SEC_PK"
	 */
	public static final String SQL_EPSN_FLD_SEC       = "NUM_SEC_PK";
	/**
	 * Constante para "COD_TIPO_NOM"
	 */
	public static final String SQL_EPSN_FLD_TIPO_NOM  = "COD_TIPO_NOM";
	/**
	 * Constante para "FCH_RECEP"
	 */
	public static final String SQL_EPSN_FLD_FCH_RECEP = "FCH_RECEP";
	/**
	 * Constante para "FCH_PROC"
	 */
	public static final String SQL_EPSN_FLD_FCH_PROC  = "FCH_PROC";
	/**
	 * Constante para "USR_ALTA"
	 */
	public static final String SQL_EPSN_FLD_USR       = "USR_ALTA";
	/**
	 * Constante para "COD_ESTAT"
	 */
	public static final String SQL_EPSN_FLD_ESTATUS   = "COD_ESTAT";
	/**
	 * Constante para "DSC_OBSER"
	 */
	public static final String SQL_EPSN_FLD_OBS       = "DSC_OBSER";

	/*Varibles para codigos de error y mensajes*/

	/** CONSTANTE DE RESULTADO EXISTO**/
	public static final String COD_SOLPDF_EXITO      = "0000";
	/** CONSTANTE DE RESULTADO ERROR**/
	public static final String COD_SOLPDF_ERROR_GEN  = "0002";
	/** CONSTANTE DE RESULTADO ERROR SQL**/
	public static final String COD_SOLPDF_ERROR_SQL  = "0003";
	/** CONSTANTE DE RESULTADO ERROR DATOS**/
	public static final String COD_SOLPDF_ERROR_DAT  = "0004";
	/** CONSTANTE DE RESULTADO ERROR DUPLICADO**/
	public static final String COD_SOLPDF_ERROR_DUP  = "0010";

	/** CONSTANTE DE MENSAJE EXITO**/
	public static final String MSG_SOLPDF_EXITO     = "Su informe ha sido solicitado de forma correcta.";
	/** CONSTANTE DE MENSAJE ERROR**/
	public static final String MSG_SOLPDF_ERROR_GEN = "No se ha generado correctamente la solicitud de PDF.";
	/** CONSTANTE DE MENSAJE ERROR DUPLICADO**/
	public static final String MSG_SOLPDF_ERROR_DUP = "Ya se registr&oacute; una solicitud para este archivo.";
	/** CONSTANTE DE MENSAJE ERROR DATOS**/
	public static final String MSG_SOLPDF_ERROR_DAT = "Error al generar la solicitud.";
	/** CONSTANTE DE MENSAJE ERROR PARAMETROS**/
	public static final String MSG_SOLPDF_ERROR_PARAM = "Par&aacute;metros incorrectos para atender la solicitud.";
	/** CONSTANTE DE MENSAJE ERROR EXCEPCION**/
	public static final String MSG_SOLPDF_ERROR_EXCEP = "Se a presentado un error al atender la solicitud.";

	/** CONSTANTE DE MENSAJE ERROR CONSULTA COMPROBANTES**/
	public static final String MSG_CONCOM_ERROR_DAT = "Error al consultar los comprobantes.";
	/** CONSTANTE DE MENSAJE ERROR CONSULTA COMPROBANTES SQL**/
	public static final String MSG_CONCOM_ERROR_SQL = "Error SQL al consultar los comprobantes.";

	/** CONSTANTE DE MENSAJE PDF**/
	public static final String MSG_ENCAB_SOLPDF = "Solicitud Informe en PDF.";
	/** CONSTANTE DE MENSAJE PDF ERROR SQL PDF**/
	public static final String MSG_SOLPDF_ERROR_SQL = "Se ha generado correctamente la solicitud de PDF.";

	/** CONSTANTE TIPO DE OPERACION SOLICUTD PDF NOMINA**/
	public static final String BIT_OPER_SOLPDF_TRAD = "SPNT";
	/** CONSTANTE TIPO DE OPERACION SOLICUTD PDF NOMINA EN LINEA**/
	public static final String BIT_OPER_SOLPDF_NL =   "SPNL";

	/** CONSTANTE TIPO DE OPERACION SOLICUTD PDF NOMINA**/
	public static final String BIT_OPER_DESPDF_TRAD = "DPNT";
	/** CONSTANTE TIPO DE OPERACION SOLICUTD PDF NOMINA EN LINEA**/
	public static final String BIT_OPER_DESPDF_NL =   "DPNL";


	/** CONSTANTE CODIGO DE OPERACION SOLICITAR INFORME**/
	public static final String COD_OPER_PROG_SOl_PDF    = "SolicitaPDF";
	/** CONSTANTE CODIGO DE OPERACION DESCARGAR INFORME**/
	public static final String COD_OPER_PROG_DES_PDF    = "descargaPDF";


	/**
	 * Constante para "yyyy-MM-dd"
	 */
	public static final String FOR_FECHA_AMD = "yyyy-MM-dd";
	/**
	 * Constante para "dd/MM/yyyy"
	 */
	public static final String FOR_FECHA_DMA = "dd/MM/yyyy";
	/**
	 * Constante para "S"
	 */
	public static final String VAL_ESTAT_SOLPDF_SOL = "S";
	/**
	 * Constante para "P"
	 */
	public static final String VAL_ESTAT_SOLPDF_PRC = "P";
	/**
	 * Constante para "R"
	 */
	public static final String VAL_ESTAT_SOLPDF_RCH = "R";
	/**
	 * Constante para "L"
	 */
	public static final String VAL_TIPO_SOLPDF_NL   = "L";
	/**
	 * Constante para "T"
	 */
	public static final String VAL_TIPO_SOLPDF_TR   = "T";
	/**
	 * Constante para "cuadroDialogoPagosNomina("
	 */
	public static final String VAL_FUNCION_MENSAJE  = "cuadroDialogoPagosNomina(";
	/**
	 * Constante para "infoUser"
	 */
	public static final String VAL_VAR_MENSAJE  = "infoUser";
	/**
	 * Constante para "solicitudesPDF"
	 */
	public static final String VAR_SES_SOl_PDF    = "solicitudesPDF";

	/** CONSTANTE CODIGO PARAMETRO SECUENCIA ARCHIVO NOMINA**/
	public static final String PARAM_DATOS_NL           = "secArchivo";
	/** CONSTANTE CODIGO PARAMETRO SECUENCIA ARCHIVO NOMINA LINEA**/
	public static final String PARAM_DATOS_TR           = "valor_radio";


	/** CONSTANTE CODIGO PARAMETRO ORIGEN**/
	public static final String PARAM_ORIGEN_PAG ="origen";
	/** CONSTANTE CODIGO PARAMETRO FECHA COMPLETA**/
	public static final String PARAM_FECHA_PAG ="fecha_completa";
	/** CONSTANTE CODIGO PARAMETRO FECHA HORARIO**/
	public static final String PARAM_HOR_PAG ="horario";
	/** CONSTANTE CODIGO PARAMETRO ARCHIVO NOMINA SESSION**/
	public static final String VAR_SESS_BEAN_PAG ="archivoNominaBeanSess";
	/** CONSTANTE CODIGO PARAMETRO ARCHIVO NOMINA REQUEST**/
	public static final String VAR_REQ_BEAN_PAG ="archivoNominaBeanReq";

	/** CONSTANTE CODIGO PARAMETRO CUENTAS CATALOGO SESSION**/
	public static final String VAR_SESS_CTA_CAT_PAG ="ctasCatalogo";


	/** CONSTANTE CODIGO PARAMETRO TIPO APLICACION**/
	public static final String PARAM_TIP_APLI_PAG = "rdbAplicacion";
	/** CONSTANTE SERLVET NOMINA TRAIDCIONAL**/
	public static final String PAG_TMPL_SRV_TRAD = "ImportNomina";
	/** CONSTANTE SERLVET NOMINA EN LINEA**/
	public static final String PAG_TMPL_SRV_LIN = "ImportNominaLn";

	/** CONSTANTE PANTALLA PAGOS**/
	public static final String PAG_TMPL_INICIO_PAGOS = "/jsp/NominaPagos.jsp";

	/** CONSTANTE ES_NOMINA_PAGOS_IMP_ENVIO. */
	public static final String ES_NOMINA_PAGOS_IMP_ENVIO = "ESPI";

	/** CONSTANTE ES_NOMINA_PAGOS_IMP_ENVIO_ENTRA. */
	public static final String ES_NOMINA_PAGOS_IMP_ENVIO_ENTRA = "ESPI01";


}