package mx.altec.enlace.utilerias;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import mx.altec.enlace.beans.RSABeanAUX;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.bean.ServiciosSTUResponse;
import mx.isban.rsa.conector.servicios.ServiciosAA;
import mx.isban.rsa.conector.servicios.ServiciosSTU;

public class RSAFII {
	
	/**enum para seleccionar el tipo de metodo a ejecutar**/
    public static enum valorMetodo {
    	/**query**/
    	QUERY, 
    	/**desvinculacion**/
    	DESVINCULACION, 
    	/**consulta imagenes**/
    	CONSULTA_IMAGENES,
    	/**set imangen**/
    	SET_IMAGEN_USER,
    	/**consulta preguntas**/
    	CONSULTA_PREGUNTAS, 
    	/**crear usuarios**/
    	CREAR_USUARIO, 
    	/**set pregunta**/
    	SET_PREGUNTA_USER;
    }

	
	@SuppressWarnings("unchecked")
	public Object ejecutaMetodosRSA7(RSABeanAUX rsaBeanAUX) throws InterruptedException,
			ExecutionException {

		final ExecutorService executor = Executors.newSingleThreadExecutor();

		final Future future = executor.submit(new RSAFII$7(rsaBeanAUX));

		Object respuesta = new Object();
		try {
			EIGlobal.mensajePorTrace("Started...ejecutando RSA por conector...", EIGlobal.NivelLog.ERROR);			
			respuesta = (Object) future.get(5, TimeUnit.SECONDS);
						
			EIGlobal.mensajePorTrace("Realizo conexion a RSA por conector", EIGlobal.NivelLog.ERROR);

		} catch (TimeoutException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA ejecutaAnalyzeRequest!",
					EIGlobal.NivelLog.ERROR);
			
		}
		
		executor.shutdownNow();
		return respuesta;
	}
}

/*******************************************
 *  Clase que implementa a Callable para manejo de hilos.
 */
@SuppressWarnings("unchecked")
class RSAFII$7 implements Callable {

	/**Bean**/
	private final transient RSABeanAUX bean;
	

	/********************************************************************
	 * Metodo que implementa a la Clase Callable
	 * 
	 * @param bean : bean contenedora de los datos de rsa
	 ********************************************************************/
	RSAFII$7(RSABeanAUX bean) {
		this.bean = bean;
		
	}

	@Override
	public Object call() {
		
		return ejecutaRSA(bean);
	}
	
	/**
	 * @param rsaBean : bean de datos de RSA y datos auxiliares
	 * @return Object :  respuesta
	 */
	public Object ejecutaRSA(RSABeanAUX rsaBean) {
		
		Object respuesta = new Object();
		RSAFII.valorMetodo metodo = rsaBean.getValorMetodo();
	    switch(metodo) {
	    case QUERY : respuesta =  ejecutaQueryStatus(rsaBean);//ejecutaQuery(rsaBean);
	    	break;
	    case DESVINCULACION : respuesta = ejecutaDesvincular(rsaBean);
	    	break;
	    case CONSULTA_IMAGENES : respuesta = ejecutaConsultaImagenes(rsaBean);
	    	break;
	    case SET_IMAGEN_USER : respuesta = ejecutaSetImagenUser(rsaBean);
	    	break;
	    case CONSULTA_PREGUNTAS : respuesta = ejecutaConsultaPreguntas(rsaBean);
	    	break;
	    case CREAR_USUARIO : respuesta = ejecutaCrearUsuario(rsaBean);
	    	break;
	    case SET_PREGUNTA_USER : respuesta = ejecutaSetPreguntaUser(rsaBean);
	    	break;
	    }
	    
	    return respuesta;
		
	}
	
	/**
	 * Metodo que ejecuta el el metodo query 
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return Object :  respuesta
	 */
	private ServiciosSTUResponse ejecutaQuery(RSABeanAUX bean) {
		ServiciosSTU query = new ServiciosSTU();
		ServiciosSTUResponse resp = new ServiciosSTUResponse();

	    resp = query.getSiteToUserData(bean.getRsaBean(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}
	
	/**
	 * Metodo que ejecuta el el metodo query para consultar estatus de usuario
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return Object :  respuesta
	 */
	private ServiciosAAResponse ejecutaQueryStatus(RSABeanAUX bean) {
		ServiciosAA query = new ServiciosAA();
		ServiciosAAResponse resp = new ServiciosAAResponse();
		EIGlobal.mensajePorTrace(
				"ejecutaQueryStatus",EIGlobal.NivelLog.DEBUG);
	    resp = query.getUserStatus(bean.getRsaBean(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	} 
	
	/**
	 * Metodo que ejecuta el el metodo query 
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return SiteToUserResponse :  respuesta
	 */
	private ServiciosAAResponse ejecutaDesvincular(RSABeanAUX bean) {
		ServiciosAA siteToUser = new ServiciosAA();
		ServiciosAAResponse resp = new ServiciosAAResponse();

	    resp = siteToUser.unbindAllDevices(bean.getRsaBean(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}
	
	/**
	 * Metodo que ejecuta el metodo para consultar imagenes
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return SiteToUserResponse :  respuesta
	 */
	private ServiciosSTUResponse ejecutaConsultaImagenes(RSABeanAUX bean) {
		ServiciosSTU siteToUser = new ServiciosSTU();
		ServiciosSTUResponse resp = new ServiciosSTUResponse();

	    resp = siteToUser.browseImages(bean.getRsaBean(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}
	
	/**
	 * Metodo que ejecuta el metodo para setear la  imagene del usuario
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return SiteToUserResponse :  respuesta
	 */
	private ServiciosSTUResponse ejecutaSetImagenUser(RSABeanAUX bean) {
		ServiciosSTU siteToUser = new ServiciosSTU();
		ServiciosSTUResponse resp = new ServiciosSTUResponse();

	    resp = siteToUser.setImage(bean.getRsaBean(), bean.getImageRSA(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}
	
	/**
	 * Metodo que ejecuta el metodo para setear la  pregunta del usuario
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return SiteToUserResponse :  respuesta
	 */
	private ServiciosAAResponse ejecutaSetPreguntaUser(RSABeanAUX bean) {
		ServiciosAA siteToUser = new ServiciosAA();
		ServiciosAAResponse resp = new ServiciosAAResponse();

	    resp = siteToUser.setUserQuestion(bean.getRsaBean(), bean.getArrayChallengeQuesitons(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}
	
	/**
	 * Metodo que ejecuta el metodo para setear la  imagene del usuario
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return SiteToUserResponse :  respuesta
	 */
	private ServiciosAAResponse ejecutaConsultaPreguntas(RSABeanAUX bean) {
		ServiciosAA siteToUser = new ServiciosAA();
		ServiciosAAResponse resp = new ServiciosAAResponse();

	    resp = siteToUser.browseQuestion(bean.getRsaBean(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}
	
	/**
	 * Metodo que ejecuta el metodo para crear al usuario
	 * @param bean : bean de datos de RSA y datos auxiliares
	 * @return Object :  respuesta
	 */
	private ServiciosAAResponse ejecutaCrearUsuario(RSABeanAUX bean) {
		ServiciosAAResponse resp = new ServiciosAAResponse();
		ServiciosAA servicioRSA = new ServiciosAA();

	    resp = servicioRSA.createUser(bean.getRsaBean(), UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}

}