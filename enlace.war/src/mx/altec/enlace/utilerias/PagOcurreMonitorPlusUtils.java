package mx.altec.enlace.utilerias;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.bo.BaseResource;
import mx.isban.mp.conector.ConectorMP;
/**
 * Clase utilitaria para mapeo de las operaciones de PAGOS OCURRE
 * @author FSW Everis
 *
 */
public final class PagOcurreMonitorPlusUtils {
    /**
     * constructor privado
     */
    private PagOcurreMonitorPlusUtils() {
        // not called
    }

    /**
     * Metodo que carga al mapa los valores cuando se realiza un pago ocurre
     * individual o por archivo
     * 
     * @param req Objeto con informacion inherente a la peticion
     * @param parametros informacion sobre la transaccion
     * @param session Objeto con informacion de la sesion
     */
    public static void enviarPagOcurreMP(HttpServletRequest req,
            String [] parametros, BaseResource session) {
        EIGlobal.mensajePorTrace("--------> ** Entra a enviarPagOcurreMP|"
                   + "Codigo de Error: "    +parametros[0]+ "|"
                   + "Personalidad: "+parametros[1]+ "|"                    
                   + "Importe total: " + parametros[2]+ "|"
                   + "Cuentas cargo: " + parametros[3] + "|"
                   + "Sucursal Operante: " + parametros[4] + "|"
                   + "Numero de Trx: " + parametros[5] + "|"
                   + "Moneda: " + parametros[6] + "|"
                   + "Tipo transaccion: " + parametros[7] + "|"
                   + "Referencia: " + parametros[8] + "|"
                   + "Importe indiv" + parametros[9] + "|"
                 + "Nombre cliente titular cuenta destino: " + parametros[10] + "|"
                   , EIGlobal.NivelLog.DEBUG);
        
        if ("OK".equals(parametros[0].substring(0, 2)) || "0000".equals(parametros[0].substring(13, 17))) {
            Map<Integer, String> mapa = EnlaceMonitorPlusConstants.cargaValoresDefaultTransferencia(req, session, parametros[4],parametros[3], EnlaceMonitorPlusConstants.CADENA_VACIA);
            String error = mapa.get(90000);
            if(error != null && error != EnlaceMonitorPlusConstants.CADENA_VACIA){
                EIGlobal.mensajePorTrace(" enviarPagOcurreMP Trx390 no obtuvo datos no se infroma a monitor ", EIGlobal.NivelLog.DEBUG);
                
            }else{
            Calendar fechaActual = Calendar.getInstance();
            Date date = fechaActual.getTime();
            fechaActual.setTime(date);
            
            // Codigo de transaccion 3501
            mapa.put(EnlaceMonitorPlusConstants.KEY_TRANS, EnlaceMonitorPlusConstants.COD_TRAN_OCUR);

            //  Numero de cuenta origen 3540 
            mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI, parametros[3]); 

            // Numero de Trx PT ID 3548
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_TRX, parametros[5]);

            String importeF = String.valueOf(new BigDecimal(parametros[9]).movePointRight(2).longValue());
            // monto total 3508
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_TOTAL, importeF);

            // monto efectivo 3567
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_EFEC, importeF);

            // monto cheque propio 3549
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEPROP, EnlaceMonitorPlusConstants.CADENA_CERO);

            // monto cheque ajeno 3550
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEAJE, EnlaceMonitorPlusConstants.CADENA_CERO);

            // Moneda 3516
            mapa.put(EnlaceMonitorPlusConstants.KEY_COD_MON, parametros[6]);
            
            // Clave transfer interbancaria 3543
            mapa.put(EnlaceMonitorPlusConstants.KEY_CVE_TRASNFER_INTERBANCARIA, parametros[5]);
            
            // ID de rastreo transferencias 3555
            mapa.put(EnlaceMonitorPlusConstants.KEY_ID_RASTREO_TRASFERENCIA, parametros[5]);

            // Tipo transaccion 3579
            mapa.put(EnlaceMonitorPlusConstants.KEY_TIP_TRANS, parametros[7]);
         
            // Numero de referencia 3563
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER, parametros[5]);
            
            // Numero de documento de cheque 3512
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_DOC_CHEQUE, parametros[8]);
            
            // Numero de documento de cuenta destino 3504
            mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_DEST, parametros[8]);
            
            // Divisa 17
            mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA, EnlaceMonitorPlusConstants.DIVISA_PESOS );
            
            // identificador archivo 20
            mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO, parametros[7]);
            // codigo de respuesta 3527
            mapa.put(EnlaceMonitorPlusConstants.KEY_COD_RESPUESTA, EnlaceMonitorPlusConstants.COD_RESP);
            //Nombre Cliente Titular Cta. Destino 3524
            mapa.put(EnlaceMonitorPlusConstants.KEY_NOMBRE_DESTINO, parametros[10]);
            
            EIGlobal.mensajePorTrace("-------->Se envia PagoOcurre a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
            
            // enviarPagOcurreMP: 3501, 3540, 3504, 3548, 3508, 3567, 3549, 3550, 3524, 3516, 3543, 3555, 3579, 3563, 3539, 3512, 3504, 17, 20
            ConectorMP.enviaMsg(mapa, EnlaceMonitorPlusConstants.MSG_ADF345);
            }
        }
    }
}
