package mx.altec.enlace.utilerias;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.beans.ChallengeBean;
import mx.altec.enlace.bo.BloqueoFactAut;
import mx.altec.enlace.bo.WSBloqueoToken;

import mx.altec.enlace.servlets.BaseServlet;
import mx.isban.rsa.Exception.BusinessException;
import mx.isban.rsa.aa.ws.ChallengeQuestion;
import mx.isban.rsa.aa.ws.ChallengeQuestionChallenge;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.conector.servicios.ServiciosAA;

import com.passmarksecurity.PassMarkDeviceSupportLite;

/**
 * Isban M&eacute;xico<br>
 * Descripción:  Utilidades para ejecutar la funcionalidad de CHALLENGE, 
 * cuando este sea arrojado por RSA.<br>
 *
 * Control de Cambios:<br>
 * 1.0 25/06/2013 Stefanini
 */
public class UtilidadesChallengeRSA extends BaseServlet {

	/**VERIFIED*/
	final static private String VERIFIED = "VERIFIED";
	
	/**C&oacute;digo de bloqueo de token para informar como bloqueo de seguridad*/
	final static private String COD_BLOQ_SEG_TKN = "6";
	
	/**
	 * Metodo que ejecuta el metodo para consultar la pregunta reto del usuario
	 * @param bean : bean de datos
	 * @return SiteToUserResponse : respuesta
	 */
	public ServiciosAAResponse ejecutaConsultaPreguntaReto(RSABean bean) {
		final ServiciosAA siteToUser = new ServiciosAA();
		ServiciosAAResponse resp = new ServiciosAAResponse();

	    resp = siteToUser.getChallQuestion(bean, UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}
	
	/**
	 * Metodo que ejecuta el metodo para evaluar la respuesta de la pregunta reto
	 * @param bean : bean de datos
	 * @param challengeQuestion : arreglo de preguntas
	 * @return SiteToUserResponse : respuesta
	 */
	public ServiciosAAResponse ejecutaRespuestaPreguntaReto(RSABean bean, ChallengeQuestion[]challengeQuestion ) {
		final ServiciosAA siteToUser = new ServiciosAA();
		ServiciosAAResponse resp = new ServiciosAAResponse();

	    resp = siteToUser.authenticateChallQuestion(bean, challengeQuestion, UtilidadesRSA.VERSION_RSA);
	    
	    return resp;
	}

	/**
	 * @param usuario : C&oacute;digo de cliente al que se le aplica el Deny
	 * @param actionCode : C&oacute;digo de acci&oacute;n, ejemplo "CHALLENGE"
	 * @param request : HttpServletRequest
	 */
	public void eventoDeny(String usuario,String actionCode, HttpServletRequest request) {
		final WSBloqueoToken bloqueo = new WSBloqueoToken();
		final BloqueoFactAut bloqueoFact = new BloqueoFactAut();
		final UtilidadesRSA utilidadesRSA = new UtilidadesRSA();

		final String[] resBloqTok = bloqueo.bloqueoToken(usuario, COD_BLOQ_SEG_TKN);
		final String   resBloqPwd = bloqueoFact.bloqueaContrasena(usuario);

		try{
			EIGlobal.mensajePorTrace(
					"UtilidadesChallengeRSA - eventoDeny - Resultado bloqueo tok: ["
					+ resBloqTok[0] + "]", EIGlobal.NivelLog.INFO
			);
			EIGlobal.mensajePorTrace(
					"UtilidadesChallengeRSA - eventoDeny - Resultado bloqueo pwd: ["
					+ resBloqPwd    + "]", EIGlobal.NivelLog.INFO
			);
		}catch(NullPointerException e){
			EIGlobal.mensajePorTrace(
					"UtilidadesChallengeRSA - eventoDeny - Sin respuesta bloqueo tok:", EIGlobal.NivelLog.INFO
			);
		}
		
		
		//int referenciaBit = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		//UtilidadesRSA.bitacoraTCT(request, UtilidadesRSA.CVE_DENY, referenciaBit, "RSA_0000", UtilidadesRSA.DES_DENY);
		//UtilidadesRSA.bitacorizaTransac(request, UtilidadesRSA.CVE_DENY,"");

		utilidadesRSA.mandarCorreo(actionCode, request);
	}

    /**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @param resp : Bean de respuesta del m&eacute;todo analyze
	 * @param servletLLamado : nombre del servlet el cual hizo el llamado
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 * @throws BusinessException : exception
	 * @return ChallengeBean :bean de datos del tipo challenge
	 */
	public ChallengeBean  validarRespuestaRSA(HttpServletRequest request, HttpServletResponse response, 
			ServiciosAAResponse resp , String servletLLamado) throws ServletException, IOException, BusinessException {

		final String usuario = request.getHeader("IV-USER");

		final UtilidadesChallengeRSA utilerias = new UtilidadesChallengeRSA();
		ChallengeBean challengeBean = new ChallengeBean();

		String estatusUsuario = "";
	    String actionCode = "";
	    String deviceTokenFSO = "";
	    String deviceTokenCookie = "";
	    String sessionID = "";
	    String transactionID = "";

	    if(resp.getIdentificationData() != null) {

			estatusUsuario = resp.getIdentificationData().getUserStatus() != null 
			? resp.getIdentificationData().getUserStatus().toString() : null;

		    actionCode = resp.getRiskResult().getTriggeredRule().getActionCode() != null
		    ? resp.getRiskResult().getTriggeredRule().getActionCode().toString() : null;

		    deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() != null
		    ? resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() : null;

		    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() != null
		    ? resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() : null;

	    	transactionID = resp.getIdentificationData().getTransactionId() != null
	    	? resp.getIdentificationData().getTransactionId() : null;

	    	sessionID = resp.getIdentificationData().getSessionId() != null
	    	? resp.getIdentificationData().getSessionId() : null;

	    	UtilidadesRSA.crearCookie(response, deviceTokenCookie);
			request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

			EIGlobal.mensajePorTrace(">>>>>>>> Estatus usuario: " + estatusUsuario, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(">>>>>>>> Action code: " + actionCode, EIGlobal.NivelLog.INFO);

			if(VERIFIED.equals(estatusUsuario) && "ALLOW".equals(actionCode)) {
				challengeBean.setRespuesta("SUCCESS");

			} else if(VERIFIED.equals(estatusUsuario) && "CHALLENGE".equals(actionCode)) {
				challengeBean = ejecutaChallenge(request, response, sessionID, transactionID, usuario, servletLLamado);
				challengeBean.setRespuesta("CHALLENGE");

			} else if((VERIFIED.equals(estatusUsuario) || "UNVERIFIED".equals(estatusUsuario)) && "DENY".equals(actionCode)) {
				utilerias.eventoDeny(usuario, actionCode, request);
				challengeBean.setRespuesta("DENY");

			} else if((VERIFIED.equals(estatusUsuario) && ("REVIEW".equals(actionCode)) || "LOCKOUT".equals(estatusUsuario))) {
				//envio de correo
				EIGlobal.mensajePorTrace(">>>> Entro a REVIEW O LOCKOUT", EIGlobal.NivelLog.INFO);

				actionCode = estatusUsuario;

				final UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
				utilidadesRSA.mandarCorreo(actionCode, request);
				challengeBean.setRespuesta("REVIEW");
			}else if("UNVERIFIED".equals(estatusUsuario)){
				challengeBean.setRespuesta("SUCCESS");
			}else{
				challengeBean.setRespuesta("SUCCESS");
			}
	    }
		return challengeBean;
	}

	/**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @param sessionID : id de sesi&oacute;n necesaria para ejecutar el challenge
	 * @param transactionID : id de transacci&oacute;n necesaria para ejecutar el challenge
	 * @param usuario : c&oacute;digo de usuario
	 * @param servletLlamado : Nombre del servlet que llam&oacute; a ejecutar el challenge
	 * @return ChallengeBean : bean de datos del tipo challenge
	 */
	private static ChallengeBean ejecutaChallenge(HttpServletRequest request, HttpServletResponse response,
			String sessionID, String transactionID,String  usuario, String servletLlamado) {

		final ChallengeBean challengeBean= new ChallengeBean();
		RSABean rsaBean = new RSABean();
		final UtilidadesRSA rsaUtils = new UtilidadesRSA();
		rsaBean =  rsaUtils.generaBean(request, sessionID, transactionID, usuario );
		final ServiciosAA utilerias = new ServiciosAA();

		String deviceTokenFSO = "";
	    String deviceTokenCookie = "";

		final ServiciosAAResponse  res = utilerias.getChallQuestion(rsaBean, UtilidadesRSA.VERSION_RSA);

		ChallengeQuestionChallenge preguntaUsr = new ChallengeQuestionChallenge();
		preguntaUsr = res.getChallengeQuestionChallenge();
		final ChallengeQuestion[] question = preguntaUsr.getPayload().getChallengeQuestions();
		final ChallengeQuestion questionUsr = question[0];

		EIGlobal.mensajePorTrace(">>>>>> Pregunta   " + questionUsr.getQuestionText(), EIGlobal.NivelLog.INFO);

	    deviceTokenFSO = res.getDeviceResult().getDeviceData().getDeviceTokenFSO();
	    deviceTokenCookie = res.getDeviceResult().getDeviceData().getDeviceTokenCookie();

	    UtilidadesRSA.crearCookie(response, deviceTokenCookie);
		request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

		request.setAttribute("sessionID", sessionID);
		request.setAttribute("transactionID", transactionID);
		request.setAttribute("preguntaUser", questionUsr.getQuestionText());
		request.setAttribute("preguntaUserID", questionUsr.getQuestionId());
		request.setAttribute("servletRegreso", servletLlamado);

		challengeBean.setPreguntaUser(questionUsr.getQuestionText());
		challengeBean.setPreguntaUserID(questionUsr.getQuestionId());
		challengeBean.setSeesionID(sessionID);
		challengeBean.setServletRegreso(servletLlamado);
		challengeBean.setTransactionID(transactionID);

		return challengeBean;
	}
}