package mx.altec.enlace.utilerias;

import java.util.Vector;

import mx.altec.enlace.bo.TrxGP93BO;

/**
 *  Clase Singleton para el uso adecuado del catalogo de la TRX GP93
 *  Divisas y Paises
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Nov 29, 2011
 */
public class CatDivisaPais {
	
    /**
     * Instancia estatica Singleton de CatDivisaPais.
     */
	private static CatDivisaPais INSTANCE = null;
	
	/**
	 * Lista de Divisas que se consultan con la TRX GP93
	 */
	private Vector listaDivisas = null;
	
	/**
	 * Lista de Paises que se consultan con la TRX GP93
	 */
	private Vector listaPaises= null;
	
	/**
	 * Metodo para obtener valor de listaDivisas
	 * 
	 * @return listaDivisas Lista de Divisas
	 */
	public Vector getListaDivisas(){
		return this.listaDivisas;
	}
	
	/**
	 * Metodo para obtener valor de listaPaises
	 * 
	 * @return listaPaises Lista de Paises
	 */
	public Vector getListaPaises(){
		return this.listaPaises;
	}
	
	/**
     * Constructor privado para evitar instancia.
     */
    private CatDivisaPais() {
    	TrxGP93BO gp93 = new TrxGP93BO();
    	try{
    		this.listaDivisas = gp93.obtenCatalogoDivisas();
    		this.listaPaises  = gp93.obtenCatalogoPaises();
    	}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("CatDivisaPais::CatDivisaPais:: Problemas al llenar el catalogo con la TRX GP93 ", EIGlobal.NivelLog.ERROR);			
			EIGlobal.mensajePorTrace("CatDivisaPais::CatDivisaPais:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);    		
    	}
    	
    }
	
    /**
     * Metodo estatico para crear instancia de la clase  
     */
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new CatDivisaPais();
        }
    }
 
    /**
     * Metodo getter para obtener valor de INSTANCE
     * @return INSTANCE Instancia de la clase CatDivisaPais
     */
    public static CatDivisaPais getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

}


