package mx.altec.enlace.utilerias;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;

public class NomPreUtil {

	private static final String CODIGO_EXITO = "@11";
	private static final String CODIGO_ERROR = "@ER";

	//Declaraci�n de constantes para claves de operacion TCT
	public static String ES_NP_REMESAS_CONSULTA_OPER = "CORE";
	public static String ES_NP_REMESAS_RECEPCION_OPER="RCRE";
	public static String ES_NP_REMESAS_RECEPCION_RECHAZA_OPER = "CARE";
	public static String ES_NP_TARJETA_ASIGNACION_LINEA_OPER = "ATEI";
	public static String ES_NP_TARJETA_ASIGNACION_IMPORTAR_OPER = "ATIM";
	public static String ES_NP_TARJETA_CONSULTA_OPER = "CRTE";
	public static String ES_NP_TARJETA_BAJA_OPER = "BATA";
	public static String ES_NP_TARJETA_BLOQUEO_OPER = "BLTA";
	public static String ES_NP_TARJETA_REASIGNACION_OPER = "RETA";
	public static String ES_NP_TARJETA_CONSULTA_ALTA_CANCELA_OPER = "CAAM";
	public static String ES_NP_TARJETA_CONSULTA_MODIFICA_DEMOGRAFICOS_OPER = "MOTE";
	public static String ES_NP_PAGOS_IMPORTACION_OPER = "PNOS";
	public static String ES_NP_PAGOS_INDIVIDUAL_OPER = "PNIS";
	public static String ES_NP_PAGOS_CONSULTA_CONSULTA_PI_OPER = "CPNI";
	public static String ES_NP_PAGOS_CONSULTA_CONSULTA_IE_OPER = "CNIE";
	public static String ES_NP_PAGOS_CONSULTA_CANCELA_PI_OPER = "CAPI";
	public static String ES_NP_PAGOS_CONSULTA_CANCELA_IE_OPER = "CAMP";
	private static String MSG_SIN_SERVICIO_NOMINA = "El contrato no cuenta con servicio de n&oacute;mina";
	private static String MSG_TITULO_SIN_SERVICIO_NOMINA = " Tarjeta de Pagos Santander ";//YHG NPRE

	public static boolean isCodigoExito(String string) {
		return (string != null && string.indexOf(CODIGO_EXITO) != -1);
	}

	public static boolean isCodigoError(String string) {
		return (string != null && string.indexOf(CODIGO_ERROR) != -1);
	}

	public static String getCodigoError(String string) {

		String salida = null;

		if (string != null && string.indexOf(CODIGO_ERROR) != -1) {

			int index = string.indexOf(CODIGO_ERROR) + 3;
			salida = string.substring( index, index + 8);
		}

		return salida;
	}
	public static String getMensajeError(String string) {

		String salida = null;
		if (string != null && string.indexOf(CODIGO_ERROR) != -1) {

			int index = string.indexOf(CODIGO_ERROR) + 3;
			int indexfin=string.length()-2;
			salida = string.substring(index + 8,indexfin);

		}

		return salida;
	}
	public static String rellenar(String cad, int lon) {
		if (cad == null) {
			cad = "";
		}
		return rellenar(cad, lon, ' ', 'D');
	}

	public static String rellenar(String cad, int lon, char rel, char tip) {

		cad = cad == null ? "" : cad;

		if (cad.length() > lon)
			return cad.substring(0, lon);
		if (tip != 'I' && tip != 'D')
			tip = 'I';
		String aux = "";
		if (tip == 'D')
			aux = cad;
		for (int i = 0; i < (lon - cad.length()); i++)
			aux += "" + rel;
		if (tip == 'I')
			aux += "" + cad;
		return aux;
	}

	public static String getValor(String arg, int index, int length) {

		String string = arg.substring(index, index + length);
		return string.trim();
	}

	public static String[] getDetalles(String string, String formato, int tama�o) {

		String[] result = null;

		if (string.indexOf(formato) != -1) {

			String strDetalle = string.substring(string.indexOf(formato),
				string.lastIndexOf(formato) + tama�o + formato.length());

			result = strDetalle.split(formato);

		}

		return result;
	}

	public static <T extends Serializable>String saveObjectToFile(T object, HttpSession session) {

		String result = null;
		FileOutputStream fos = null;
		ObjectOutputStream out = null;

		try {

			String fileId = "NP" + session.getId() + System.currentTimeMillis();

			fos = new FileOutputStream(Global.NP_TMP_SERIALIZACION + fileId);

			out = new ObjectOutputStream(fos);

			out.writeObject(object);

			out.close();
			fos.close();

			result = fileId;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static <T extends Serializable>T objectFromFile(String fileId) {

		T result = null;
		FileInputStream fis = null;
		ObjectInputStream in = null;

		try {

			fis = new FileInputStream(Global.NP_TMP_SERIALIZACION + fileId);

			in = new ObjectInputStream(fis);

			result = (T)in.readObject();

			in.close();

			fis.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static int obtenIndiceInicial(HttpServletRequest request) {

		String strInicio = request.getParameter("inicio");
		String accion = request.getParameter("accion");

		int inicio = 0;

		if (strInicio == null || strInicio.length() == 0 ) {
			inicio = 0;
		} else {
			inicio = Integer.parseInt(strInicio);
			inicio = "A".equals(accion) ? inicio - 50 : inicio + 50;
			if (inicio < 0) {
				inicio = 0;
			}
		}

		return inicio;
	}

	public static int obtenIndiceFinal(HttpServletRequest request, int tamano) {

		String strFin = request.getParameter("fin");
		String accion = request.getParameter("accion");

		int fin = 0;

		if (strFin == null || strFin.length() == 0) {
			if (tamano < 50) {
				fin = tamano;
			} else {
				fin = 50;
			}
		} else {
			fin = Integer.parseInt(strFin);
			fin = "A".equals(accion) ? fin - 50 : fin + 50;
			if (fin > tamano) {
				fin = tamano;
			} else if (fin % 50 != 0) {
				while (fin % 50 != 0) {
					fin++;
				}
			}
		}
		return fin;
	}

	public static void bitacoriza(HttpServletRequest request, String servicio, String codError, BitaTransacBean bt) {
		bitacoriza(request, servicio, codError, false, bt);
	}

	public static void bitacoriza(HttpServletRequest request, String servicio, String codError, boolean inicia, BitaTransacBean bt) {
		String idFlujo=request.getParameter(BitaConstants.FLUJO);
		if(Global.USAR_BITACORAS.trim().equals("ON") && idFlujo!=null && !idFlujo.trim().equals("")) {

			HttpSession session = request.getSession();
			BaseResource baseResource = (BaseResource) session.getAttribute("session");
			BitaHelperImpl bh = new BitaHelperImpl(request, baseResource, session);
			if(inicia)
				bh.incrementaFolioFlujo(idFlujo);
			bh.llenarBean(bt);
			bt.setContrato(baseResource.getContractNumber());
			bt.setUsr(baseResource.getUserID8());
			if(servicio!=null)
				bt.setServTransTux(servicio);
			if(codError!=null)
				bt.setIdErr(codError);
			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean validaServicioNominaRedireccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		BaseResource session = (BaseResource) request.getSession().getAttribute("session");

		boolean result = validaServicioNomina(session.getUserID8(), session.getContractNumber(), session.getUserProfile());

		if (!result) {

			request.setAttribute("titulomensaje", MSG_TITULO_SIN_SERVICIO_NOMINA);

			request.setAttribute("mensajecomun", MSG_SIN_SERVICIO_NOMINA);

			request.getRequestDispatcher(
				IEnlace.NOM_PRE_COMUN_MENSAJE).include(
				request, response);
		}

		return result;
	}

	public static boolean validaServicioNomina(String empleado, String contrato, String perfil) {

		String tramaSalida = "";
		boolean resultado = false;

		StringBuffer sb = new StringBuffer()
			.append("1EWEB|")
			.append(empleado)
			.append("|")
			.append("NPVI")
			.append("|")
			.append(contrato)
			.append("|")
			.append(empleado)
			.append("|")
			.append(perfil)
			.append("|")
			.append(contrato)
			.append("@ @ @ @ @ @3@ @ @ @ @ @ @| | | |");

		String tramaEntrada = sb.toString();

		logInfo("NomPreUtil: Trama a enviar [" + tramaEntrada + "]");

		tramaSalida = ejecutaServicio(tramaEntrada);

		logInfo("NomPreUtil: Trama salida: [" + tramaSalida  + "]");

		if (tramaSalida != null && tramaSalida.length() > 0) {

			resultado = tramaSalida.startsWith("NOMI0000");
		}

		return resultado;
	}

	private static String ejecutaServicio(String trama) {

		String tramaSalida = null;

		try {

			ServicioTux tuxGlobal = new ServicioTux();

			Hashtable htResult = tuxGlobal.web_red(trama);

			if (htResult != null && htResult.size() > 0) {

				tramaSalida = (String) htResult.get("BUFFER");
			}

		} catch (java.rmi.RemoteException re) {

			//re.printStackTrace();
			EIGlobal.mensajePorTrace("NomPreUtil:ejecutaServicio - " + re.getMessage(), EIGlobal.NivelLog.INFO);

		} catch (Exception e) {

			//e.printStackTrace();
			EIGlobal.mensajePorTrace("NomPreUtil:ejecutaServicio - " + e.getMessage(), EIGlobal.NivelLog.INFO);
		}

		logInfo("NomPreUtil: Trama salida [" + tramaSalida + "]");

		return tramaSalida;
	}

	public static void logInfo(String message) {
		EIGlobal.mensajePorTrace(message, EIGlobal.NivelLog.INFO);
	}

	public static void logError(String message) {
		EIGlobal.mensajePorTrace(message, EIGlobal.NivelLog.ERROR);
	}

	public static void logWarning(String message) {
		EIGlobal.mensajePorTrace(message, EIGlobal.NivelLog.WARN);
	}

	public static void logDebug(String message) {
		EIGlobal.mensajePorTrace(message, EIGlobal.NivelLog.DEBUG);
	}
}