/**
 * Isban Mexico
 *   Clase: EdoCtaConstantes.java
 *   Descripción: Clase utilizada para almacenar las constantes del modulo
 *   de Estados de Cuenta
 *
 *   Control de Cambios:
 *   1.0 12/02/2014 Isban Mexico
 *   2.0 09/12/2016 FWS TCS marreguin P/023894 SPID Liga CEP
 */

package mx.altec.enlace.utilerias;

public class TranInterConstantes {
	/**
	 * ARCHIVO_CONFIGURACION = "/proarchivapp/WebSphere8/enlace/cfg/EI.cfg";
	 */
	public static final String ARCHIVOCONFIGURACION = "/proarchivapp/WebSphere8/enlace/estatico/santander.jceks";		//desarrollo //Cambio a WAS 8
	
	//*******************INICIA TCS FSW 12/2016***********************************
	public static final String ARCHIVOCONFIGURACIONUSD = "/proarchivapp/WebSphere8/enlace/estatico/santanderUSD.jceks";		
	//*******************Fin TCS FSW 12/2016***********************************
	/**
	 *  PIPE |
	 */
	public static final String PIPE = "|";
	/**
	 * SEGUNDORELLENO &nbsp; </td>
	 */
	public static final String SEGUNDORELLENO="&nbsp; </td>";
	/**
	 * CLASSCONESPACIO "\n<th class='tittabdat'>&nbsp; "
	 */
	public static final String CLASSCONESPACIO="\n<th class='tittabdat'>&nbsp; ";
	/**
	 * CIERRE " &nbsp; </th>"
	 */
	public static final String CIERRE=" &nbsp; </th>";
	/**
	 * MODULO "Modulo"
	 */
	public static final String MODULO="Modulo";
	/**
	 * PRIMERAPERTURA="\n    <tr>";
	 */
	public static final String PRIMERAPERTURA="\n    <tr>";
	/**
	 * SEGUNDOCIERRE  "\n    </tr>"
	 */
	public static final String SEGUNDOCIERRE="\n    </tr>";
	/**
	 * CIERRETABLA="\n </table>";
	 */
	public static final String CIERRETABLA="\n </table>";
	/**
	 * DIA DIA
	 */
	public static final String DIA = "DIA";
	/**
	 * MES MES
	 */
	public static final String MES = "MES";
	/**
	 * ANIO ANIO
	 */
	public static final String ANIO = "ANIO";
	/**
	 * FECHAX FECHA
	 */
	public static final String FECHAX = "FECHA";
	/**
	 * session session
	 */
	public static final String SESSION="session";
	/**
	 * RELLENO > &nbsp;
	 */
	public static final String RELLENO="> &nbsp;";
	/**
	 *  PAGINA /jsp/Banxico.jsp
	 */
	public static final String PAGINA="/jsp/Banxico.jsp";
	/**
	 * LOG MDI_CepServlet::
	 */
	public static final String LOG = "MDI_CepServlet::";
	/**
	 * PUNTOS ::
	 */
	public static final String PUNTOS = "::";
	/**
	 * TIPO "T"
	 */
	public static final String TIPO = "T";
	/**
	 * NULL = "null"
	 */
	public static final String NULL = "null";
	/**
	 * AMPERSON = "&"
	 */
	public static final String AMPERSON = "&";
	/**
	 * FECHA = "fecha="
	 */
	public static final String FECHA = "fecha=";
	/**
	 * CLAVERASTREO = "cveRastreo="
	 */
	public static final String CLAVERASTREO = "cveRastreo=";
	/**
	 * BANCO = "banco="
	 */
	public static final String BANCO = "banco=";
	/**
	 * CTABONO = "ctaAbono="
	 */
	public static final String CTABONO = "ctaAbono=";
	/**
	 * CTACARGO = "ctaCargo="
	 */
	public static final String CTACARGO = "ctaCargo=";
	/**
	 * IMPORTE = "importe="
	 */
	public static final String IMPORTE = "importe=";
	/**
	 * ETIQUETALOG = "MDI_Consultar - tranInterbancaria.get"
	 */
	public static final String ETIQUETALOG = "MDI_Consultar - tranInterbancaria.get";
	/**
	 * ETIQUETALOGSERVLET = "MDI_CepServlet::bean.get"
	 */
	public static final String ETIQUETALOGSERVLET = "MDI_CepServlet::bean.get";
	/**
	 * URLBANXICO = "/jsp/Banxico.jsp"
	 */
	public static final String URLBANXICO = "/jsp/Banxico.jsp";
	/**
	 * ETIQUETATD="\n  <td "
	 */
	public static final String ETIQUETATD = "\n  <td ";
	/**
	 * ETIQUETATDABRE="\n <td>";
	 */
	public static final String ETIQUETATDABRE="\n <td>";
	/**
	 * ETIQUETATDCIERRA "\n </td>"
	 */
	public static final String ETIQUETATDCIERRA="\n </td>";
	/**
	 * CO = "CO"
	 */
	public static final String CO = "CO";
	/**
	 * CVEBANCO = "BANME"
	 */
	public static final String CVEBANCO = "BANME";
	/**
	 * BITACORA = "EPBA"
	 */
	public static final String BITACORA = "EPBA";
	/**
	 * CODERROR = "EPBA0000"
	 */
	public static final String CODERROR = "EPBA0000";
	/**
	 * ESTATUS = "R"
	 */
	public static final String ESTATUS = "R";
	/**
	 * TEXTOBITACORA = "Entrada al Portal Bancario"
	 */
	public static final String TEXTOBITACORA = "Entrada al Portal Bancario";
	/**
	 * String BITACORA00 = "EPBA00"
	 */
	public static final String BITACORA00 = "EPBA00";
	/**
	 * ERROR=" OCURRIO UN ERROR";
	 */
	public static final String ERROR=" OCURRIO UN ERROR";
	/**
	 * PAQUETESQL="sql.conection.close";
	 */
	public static final String PAQUETESQL="sql.conection.close";
	/**
	 * NUMERO="Numero";
	 */
	public static final String NUMERO="Numero";
	/**
	 * PAGINAX="Pagina";
	 */
	public static final String PAGINAX="Pagina";
	/**
	 *  AYUDA="s25490h"
	 */
	public static final String AYUDA="s25490h";
	/**
	 * String PUNTOYCOMA=";";
	 */
	public static final String PUNTOYCOMA=";";
	/**
	 * DIAGONALINVERTIDA="/";
	 */
	public static final String DIAGONALINVERTIDA="/";
	/**
	 * CERO="0"
	 */
	public static final String CERO="0";
	/**
	 * COMA=",";
	 */
	public static final String COMA=",";
	/**
	 * APOSTROFECOMA="',"
	 */
	public static final String APOSTROFECOMA="',";
	/**
	 * APOSTROFE="'";
	 */
	public static final String APOSTROFE="'";
	/**
	 * SERIECONTRASENA "201506"
	 */
	public static final String SERIECONTRASENA="201506";
	/**
	 * ALIAS "Banxico"
	 */
	public static final String ALIAS="Banxico";
	
	//*******************INICIA TCS FSW 12/2016***********************************
		public static final String ALIASUSD = "BanxicoUSD";
	//*******************Fin TCS FSW 12/2016***********************************
	
	/**
	 * H  "H"
	 */
	public static final String H="H";
	
	/**
	 * @author marreguin
	 * @since09/12/2016
	 * forma de liquidacion 2
	 * P/023894 SPID Liga CEP
	 */
	public static final String FORMA_LIQUIDACION_2="2";
	
	
	/**
	 * @author marreguin
	 * @since09/12/2016
	 * deposito a Banco
	 * P/023894 SPID Liga CEP
	 */
	public static final String DEPOSITO_A_BANCO="depositoABanco=";
}