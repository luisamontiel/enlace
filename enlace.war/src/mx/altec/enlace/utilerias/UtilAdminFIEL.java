/**
*   Isban Mexico
*   Clase: BOAdminFIEL.java
*   Descripcion: Objeto inherente al negocio donde se administra la FIEL consulta y
*   validacion del RFC
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis
*/

package mx.altec.enlace.utilerias;


import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import javax.naming.NamingException;
import javax.jms.JMSException;

import mx.altec.enlace.beans.BeanAdminFiel;
import mx.altec.enlace.beans.TSE1Trans;
import mx.altec.enlace.cliente.ws.etransfernal.BeanDatosTitular;
import mx.altec.enlace.cliente.ws.etransfernal.BeanResConsultaCertificado;
import mx.altec.enlace.dao.DAOClienteFiel;
import mx.altec.enlace.dao.DetalleCuentasDAO;
import mx.altec.enlace.dao.TrxOD52DAO;
import mx.altec.enlace.dao.TrxOD54DAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;

/**
 * Objeto de negocio para la logica de administracion de FIEL.
 *
 * @author FSW
 */
public final class UtilAdminFIEL {

	/** Constante INDICADOR_EXITO_OD52 **/
	private static final String INDICADOR_EXITO_OD52 = "@112345";
	
	/** Constante INDICADOR_REENVIO_OD52 **/
	private static final String INDICADOR_REENVIO_OD52 = "@212345";
	
	/** Nombre del formato de respuesta. */
    private static final String FORMATO_RESPUESTA = "ODMS054";

    /** Arreglo de posiciones iniciales de cada campo. */
	private static final int [] FRM_POSICIONES = {0,4,12,14,54,57,60,75,82,92,102,112};

	/** Longitudes de los campos del formato de salida de la OD54. */
	private static final int [] FRM_LONGITUDES = {4,8,2,40,3,3,15,7,10,10,10};

	/**
	 * Constructor privado para no instanciar la clase
	 */
	private UtilAdminFIEL() {
	}

    /**
     * Obtiene el campo indicado de la trama de salida.
     * @param numCampo numero de campo
     * @param respuesta de consulta
     * @return el valor del campo indicado o <code>null</code> si no coincide con los campos
     */
    private static String obtenCampo(int numCampo, String respuesta) {
        String campo = null;
        if (respuesta != null) {
            int posResp = respuesta.indexOf(FORMATO_RESPUESTA)+2;
            if (posResp >= 0 && numCampo >= 0
                    && numCampo < FRM_LONGITUDES.length
                    && respuesta.length() > (FRM_POSICIONES[numCampo] + FRM_LONGITUDES[numCampo])) {
            	posResp += FORMATO_RESPUESTA.length() + FRM_POSICIONES[numCampo];
                campo = respuesta.substring(posResp, posResp + FRM_LONGITUDES[numCampo]);
            }
        }
        return campo;
    }

	/**
	 * Realiza consulta de la FIEL
	 * @param beanAdminFiel datos de la FIEL
	 * @return DTO con el resultado de la operacion
	 */
    public static BeanAdminFiel consultaFielTrx(BeanAdminFiel beanAdminFiel){
    	EIGlobal.mensajePorTrace(new StringBuilder("Inicio consultaFielTrx").toString(), EIGlobal.NivelLog.INFO);

    	beanAdminFiel.setCodError(FielConstants.COD_ERRROR_KO);
    	beanAdminFiel.setMsgError(FielConstants.MSG_ERROR_002);
        String respuesta = null;

        if (null != beanAdminFiel && null != beanAdminFiel.getCuenta()
					&& beanAdminFiel.getCuenta().trim().length() >= FielConstants.ONCE_INT) {
	        	String trama = generaTramaConsulta(beanAdminFiel);
	        	if(null != trama && trama.trim().length() > FielConstants.CERO_INT){
	            	respuesta = new TrxOD54DAO().ejecutaConsulta(trama);
	        	}
        }
        if(null != respuesta && respuesta.indexOf(FORMATO_RESPUESTA) >= FielConstants.CERO_INT && respuesta.length() >= 119){

        	beanAdminFiel.setCodError(FielConstants.COD_ERRROR_OK);
        	beanAdminFiel.setMsgError(FielConstants.EMPTY_STRING);

        	//CODIGO ENTIDAD
        	EIGlobal.mensajePorTrace("CODIGO ENTIDAD: " + obtenCampo(FielConstants.CERO_INT, respuesta), EIGlobal.NivelLog.DEBUG);

        	//NUMERO DE CLIENTE
        	beanAdminFiel.setNumCliente(obtenCampo(FielConstants.UNO_INT, respuesta));
        	EIGlobal.mensajePorTrace("NUMERO DE CLIENTE: " + beanAdminFiel.getNumCliente(), EIGlobal.NivelLog.DEBUG);

        	//TIPO DEL DOCUMENTO
        	EIGlobal.mensajePorTrace("TIPO DEL DOCUMENTO: " + obtenCampo(FielConstants.DOS_INT, respuesta), EIGlobal.NivelLog.DEBUG);

        	//NUMERO DEL DOCUMENTO
        	beanAdminFiel.getDetalleConsulta().setNumSerie(obtenCampo(FielConstants.TRES_INT, respuesta));
        	EIGlobal.mensajePorTrace("NUMERO DEL DOCUMENTO: " + beanAdminFiel.getDetalleConsulta().getNumSerie(), EIGlobal.NivelLog.DEBUG);

        	//SECUENCIA DOCUMENTO
        	beanAdminFiel.setSecuenciaDocumento(obtenCampo(FielConstants.CUATRO_INT, respuesta));
        	EIGlobal.mensajePorTrace("SECUENCIA DOCUMENTO: " + beanAdminFiel.getSecuenciaDocumento(), EIGlobal.NivelLog.DEBUG);

        	//CODIGO DEL PAIS
        	beanAdminFiel.getDetalleConsulta().getDatosTitular().setPais(obtenCampo(FielConstants.CINCO_INT, respuesta));
        	EIGlobal.mensajePorTrace("CODIGO DEL PAIS: " + beanAdminFiel.getDetalleConsulta().getDatosTitular().getPais(), EIGlobal.NivelLog.DEBUG);

        	//EXPEDIDO POR
        	beanAdminFiel.getDetalleConsulta().getDatosTitular().setNombreOrganizacion(obtenCampo(FielConstants.SEIS_INT, respuesta));
        	EIGlobal.mensajePorTrace("EXPEDIDO POR: " + beanAdminFiel.getDetalleConsulta().getDatosTitular().getNombreOrganizacion(), EIGlobal.NivelLog.DEBUG);

        	//LUGAR EXPEDICION
        	beanAdminFiel.getDetalleConsulta().getDatosTitular().setEstado(obtenCampo(FielConstants.SIETE_INT, respuesta));
        	EIGlobal.mensajePorTrace("LUGAR EXPEDICION: " + beanAdminFiel.getDetalleConsulta().getDatosTitular().getEstado(), EIGlobal.NivelLog.DEBUG);

        	//FECHA EXPEDICION
        	beanAdminFiel.getDetalleConsulta().setFchEmision(obtenCampo(FielConstants.OCHO_INT, respuesta));
        	EIGlobal.mensajePorTrace("FECHA EXPEDICION: " + beanAdminFiel.getDetalleConsulta().getFchEmision(), EIGlobal.NivelLog.DEBUG);

        	//FECHA VENCIMIENTO
        	beanAdminFiel.getDetalleConsulta().setFchExpiracion(obtenCampo(FielConstants.NUEVE_INT, respuesta));
        	EIGlobal.mensajePorTrace("FECHA VENCIMIENTO: " + beanAdminFiel.getDetalleConsulta().getFchExpiracion(), EIGlobal.NivelLog.DEBUG);

        	//FECHA CONSTITUCION
        	EIGlobal.mensajePorTrace("FECHA CONSTITUCION: " + obtenCampo(FielConstants.DIEZ_INT, respuesta), EIGlobal.NivelLog.DEBUG);

        	//FIEL (NUMERO DE DOCUMENTO)
        	beanAdminFiel.setFiel(beanAdminFiel.getDetalleConsulta().getNumSerie());
		}

		EIGlobal.mensajePorTrace(new StringBuilder(32).append("Fin consultaFielTrx codError|").append(beanAdminFiel.getCodError()).append(FielConstants.BARRA).toString(), EIGlobal.NivelLog.DEBUG);
        return beanAdminFiel;
    }


    /**
     * Metodo para obtener y validar a informacion del certificado.
     *
     * @param beanAdminFiel DTO con informacion de la fiel
     * @return dto con el codigo de error resultante o la informacion requerida
     */
    public static BeanAdminFiel buscaFiel(BeanAdminFiel beanAdminFiel) {
    	EIGlobal.mensajePorTrace("BOADminFiel - buscaFiel " + beanAdminFiel.getFiel() , EIGlobal.NivelLog.INFO);
        beanAdminFiel.setCodError(FielConstants.COD_ERRROR_KO);
    	DAOClienteFiel daoClienteFiel = new DAOClienteFiel();
        beanAdminFiel = daoClienteFiel.buscaFiel(beanAdminFiel);

        if (FielConstants.COD_ERRROR_OK.equals(beanAdminFiel.getCodError())) {
        	beanAdminFiel = FielValidaciones.noEsNuloBeanAdminFiel(beanAdminFiel);

        	if (FielConstants.COD_ERRROR_OK.equals(beanAdminFiel.getCodError()) ) {

        		beanAdminFiel.setMsgError(null == beanAdminFiel.getDetalleConsulta().getMensaje() ? FielConstants.EMPTY_STRING : beanAdminFiel.getDetalleConsulta().getMensaje());

        		if(!FielConstants.ZERO_STR.equals(beanAdminFiel.getDetalleConsulta().getCodRespuesta().trim())){
        			beanAdminFiel.setCodError(FielConstants.COD_ERRROR_KO);
        		}else if(!FielConstants.ESTATUS_CERT_VALIDO.equals(beanAdminFiel.getDetalleConsulta().getEstadoCertificado().trim().toLowerCase())){
        			beanAdminFiel.setCodError(FielConstants.COD_ERRROR_KO);
        			beanAdminFiel.setMsgError(new StringBuilder("El estado del certificado no es ").append(FielConstants.ESTATUS_CERT_VALIDO).toString());
        		}else{

        		    validaTipoOperacion(beanAdminFiel);

        		}
		}

        }
        return beanAdminFiel;
    }

    /**
     * Valida si el tipo de operacion es una baja de  fiel para consultar RFC
     * @param beanAdminFiel DTO con informacion de la fiel
     */
    private static void validaTipoOperacion(BeanAdminFiel beanAdminFiel) {
        EIGlobal.mensajePorTrace("validaTipoOperacion", EIGlobal.NivelLog.INFO);
        if(null != beanAdminFiel.getIndicadorOperacion() && FielConstants.BAJA_DOCUMENTO.equals(beanAdminFiel.getIndicadorOperacion())){
            EIGlobal.mensajePorTrace("ES BAJA DE FIEL NO SE CONSULTA RFC", EIGlobal.NivelLog.INFO);
            beanAdminFiel.setCodError(FielConstants.COD_ERRROR_OK);
        }else{
            consultaRfc(beanAdminFiel);
        }
    }

/**
 * Metodo para consultar el RFC en el aplicativo de personas
 * @param beanAdminFiel DTO con informacion de la cuenta
 */
private static void consultaRfc(BeanAdminFiel beanAdminFiel){
        MQQueueSession mqSession = null;
            try {
                mqSession = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
                //Obtener NumeroCliente por cuenta TRXTSE1
                DetalleCuentasDAO daoNumper = new DetalleCuentasDAO();
                TSE1Trans respuesta = daoNumper.obtenerDatosCuenta(
                        mqSession, beanAdminFiel.getCuenta());
                if (respuesta.getNumeroPersona() != null && !respuesta.getNumeroPersona().isEmpty()) {
                    // Consulta RFC por numero de cliente TRXPE61
                    if (FielValidaciones.validaRFC(respuesta.getNumeroPersona(),
                            beanAdminFiel.getDetalleConsulta().getDatosTitular().getRfc())) {
                    	beanAdminFiel.setRfc(beanAdminFiel.getDetalleConsulta().getDatosTitular().getRfc());
                    	beanAdminFiel.setNumCliente(respuesta.getNumeroPersona());
                        beanAdminFiel.setCodError(FielConstants.COD_ERRROR_OK);
                    } else {
                        beanAdminFiel.setCodError("ERROR");
                        beanAdminFiel.setMsgError("el rfc no coincide con el devuelto por el aplicativo de personas");
                    }
                } else {
                    EIGlobal.mensajePorTrace("No se obtuvo numero de persona", EIGlobal.NivelLog.ERROR);
                    beanAdminFiel.setCodError("ERR");
                    beanAdminFiel.setMsgError("Error al obtener el codigo de cliente.");
                }
            } catch (NamingException ex) {
                EIGlobal.mensajePorTrace("Error al obtener el recurso |" + ex.getMessage(), EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTraceExcepcion(ex);
            } catch (JMSException ex) {
                EIGlobal.mensajePorTrace("Error de comunicaciones |" + ex.getMessage(), EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTraceExcepcion(ex);
            } catch (MQQueueSessionException ex) {
                EIGlobal.mensajePorTrace("Error de envio a MQ |" + ex.getMessage(), EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTraceExcepcion(ex);
            } finally {
                if (mqSession != null) {
                    mqSession.close();
                }
            }
}

    /**
     * Metodo que invoca la TRXOD52 para guardar los datos de la FIEL
     * @param detalleFiel dto con los datos de la FIEL
     * @return el dto con el resultado de la operacion
     */
    public static BeanAdminFiel administraFiel(BeanAdminFiel detalleFiel){
    	EIGlobal.mensajePorTrace(new StringBuilder("Inicio registraFiel").toString(), EIGlobal.NivelLog.INFO);
    	detalleFiel.setMsgError("Registro no exitoso");
    	detalleFiel.setCodError(FielConstants.COD_ERRROR_KO);

    	if(FielValidaciones.validaDatosEntrada(detalleFiel)){
        	String trama = generaTramaRegistro(detalleFiel);
        	EIGlobal.mensajePorTrace(new StringBuilder("registraFiel trama|").append(trama).append(FielConstants.BARRA).toString(), EIGlobal.NivelLog.INFO);
        	if(null != trama && trama.trim().length() > 0){
                String respuesta = new TrxOD52DAO().ejecutaConsulta(trama);
                EIGlobal.mensajePorTrace(new StringBuilder("registraFiel respuesta|").append(respuesta).append(FielConstants.BARRA).toString(), EIGlobal.NivelLog.INFO);
                if(null != respuesta && respuesta.indexOf(INDICADOR_EXITO_OD52) >= FielConstants.CERO_INT){
            		detalleFiel.setCodError(FielConstants.COD_ERRROR_OK);
            		detalleFiel.setMsgError("La operacion se realizo con exito");
                    EIGlobal.mensajePorTrace("registraFiel operacion realizada exitosamente" , EIGlobal.NivelLog.INFO);
				}else if(null != respuesta && respuesta.indexOf(INDICADOR_REENVIO_OD52) >= FielConstants.CERO_INT){
					detalleFiel.setMsgError("Fiel registrada previamente, favor de validar");
					EIGlobal.mensajePorTrace("registraFiel el cliente ya cuenta con fiel vigente" , EIGlobal.NivelLog.INFO);
				}
			}
		}
    	return detalleFiel;
    }


    /**
     * Arma la trama para la consulta OD52
     * @param b  El DTO con los valores para realizar la trama
     * @return La trama armada para la consulta OD52
     */
    private static String generaTramaRegistro(BeanAdminFiel b){
    	EIGlobal.mensajePorTrace(new StringBuilder("Inicio generaTramaRegistro").toString(), EIGlobal.NivelLog.INFO);
        StringBuilder trama = new StringBuilder();

        if(b != null){
            //EMPRESA DE CONEXION
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 4, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //CANAL DE OPERACION
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 2, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //CANAL DE COMERCIO
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 2, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //INDI ALTA/MODIFI/BAJ
            trama.append(NomPreUtil.rellenar(null == b.getIndicadorOperacion() ?
            		FielConstants.EMPTY_STRING : b.getIndicadorOperacion(), 1, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //NUMERO DE CLIENTE
            trama.append(NomPreUtil.rellenar(null == b.getNumCliente() ?
            		FielConstants.EMPTY_STRING : b.getNumCliente(), 8, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //TIPO DEL DOCUMENTO
            trama.append(NomPreUtil.rellenar(FielConstants.TIPO_DOCUMENTO, 2, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //NUMERO DEL DOCUMENTO
            trama.append(NomPreUtil.rellenar(null == b.getDetalleConsulta().getNumSerie() ?
            		FielConstants.EMPTY_STRING : b.getDetalleConsulta().getNumSerie(), 40, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //SECUENCIA DOCUMENTO
            trama.append(NomPreUtil.rellenar(null == b.getSecuenciaDocumento() ?
            		FielConstants.EMPTY_STRING : b.getSecuenciaDocumento(), 3, FielConstants.ZERO_CHAR, FielConstants.LEFT_OR));
            //CODIGO DEL PAIS
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 3, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //EXPEDIDO POR
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 15, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //LUGAR DE EXPEDICION
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 7, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //FECHA DE EXPEDICION
            trama.append(NomPreUtil.rellenar(null == b.getDetalleConsulta().getFchEmision() ?
            		FielConstants.EMPTY_STRING : b.getDetalleConsulta().getFchEmision(), 10, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //FECHA DE VENCIMIENTO
            trama.append(NomPreUtil.rellenar(null == b.getDetalleConsulta().getFchExpiracion() ?
            		FielConstants.EMPTY_STRING : b.getDetalleConsulta().getFchExpiracion(), 10, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
        }

        EIGlobal.mensajePorTrace(new StringBuilder("Fin generaTramaConsulta trama|").append(trama.toString()).append(FielConstants.BARRA).toString(), EIGlobal.NivelLog.INFO);
        return trama.toString();
    }

    /**
     * Arma la trama para la consulta OD54
     * @param b El DTO con los valores para realizar la trama
     * @return La trama armada para la consulta OD54
     */
    private static String generaTramaConsulta(BeanAdminFiel b){
    	EIGlobal.mensajePorTrace(new StringBuilder("Inicio generaTramaConsulta").toString(), EIGlobal.NivelLog.INFO);
        StringBuilder trama = new StringBuilder();

        if(b != null){
            //EMPRESA DE CONEXION
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 4, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //CANAL DE OPERACION
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 2, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //CANAL DE COMERCIO
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 2, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //NUMERO DE CONTRATO
            trama.append(NomPreUtil.rellenar(null == b.getCuenta() ?
            		FielConstants.EMPTY_STRING : b.getCuenta(), 20, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //TIPO DEL DOCUMENTO
            trama.append(NomPreUtil.rellenar(FielConstants.TIPO_DOCUMENTO, 2, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //NUMERO DEL DOCUMENTO
            trama.append(NomPreUtil.rellenar(null == b.getDetalleConsulta().getNumSerie() ?
            		FielConstants.EMPTY_STRING : b.getDetalleConsulta().getNumSerie(), 40, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));
            //SECUENCIA DOCUMENTO
            trama.append(NomPreUtil.rellenar(null == b.getSecuenciaDocumento() ?
            		FielConstants.EMPTY_STRING : b.getSecuenciaDocumento(), 3, FielConstants.ZERO_CHAR, FielConstants.LEFT_OR));
            //DOC CONSULTAR (V/T)
            trama.append(NomPreUtil.rellenar(FielConstants.DOC_CONSULTAR, 1, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));

            //TIPO DE DOCUMENTO REPOS
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 2, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));

            //NUMERO DE DOC REPOS
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 40, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));

            //SECUE DOCUMENT REPOS
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 3, FielConstants.ZERO_CHAR, FielConstants.LEFT_OR));

            //INDICADOR MAS DATOS
            trama.append(NomPreUtil.rellenar(FielConstants.EMPTY_STRING, 1, FielConstants.BLANK_CHAR, FielConstants.RIGHT_OR));

        }
        EIGlobal.mensajePorTrace(new StringBuilder("Fin generaTramaConsulta trama|").append(trama.toString()).append(FielConstants.BARRA).toString(), EIGlobal.NivelLog.INFO);
        return trama.toString();
    }
}