/**
 * Clase de paginacion de listas
 */
package mx.altec.enlace.utilerias;

import java.util.List;

import mx.altec.enlace.beans.SUALCORM4;

/**
 * @author lespinosa <VC> 
 * Clase utleria para paginacion.
 * 28-sept-2008
 */
public class PaginaListas {

	
	private List<SUALCORM4> listaPrincipal;
	private int paginacion;
	private int pagina_actual=0;
	private int paginas_totales=0;
	private int registro_Inicial=0;
	private int registro_Final=0;
	private int registrosTotales=0;
	/**
	 * Constructor principal de la clase de paginacion
	 * @param principalList
	 * @param paginacion
	 */
	public PaginaListas(List<SUALCORM4> principalList,int paginacion) {
		this.listaPrincipal=principalList;
		this.paginacion=paginacion;
		if(principalList!=null && principalList.size()>0){
			pagina_actual=1;
			paginas_totales = obtenPaginasTotales();
		}
	}
	/**
	 * metodo para la obtencion de las paginas totales de la lista principal 
	 * a paginar
	 * @return numero total de paginas
	 * 
	 */
	public int obtenPaginasTotales() {
		if(listaPrincipal!=null && listaPrincipal.size()>0){
			registrosTotales=listaPrincipal.size();
			if(listaPrincipal.size()>paginacion){
				if((paginacion%listaPrincipal.size())==0){
					return (listaPrincipal.size()/paginacion);
				}else{
					return ((listaPrincipal.size()/paginacion)+1);
				}
			}else{
				return 1;
			}
		}
		return 0;
	}

	
	/**
	 * metodo para regresar los primeros datos de la lista
	 * @return
	 */
	public List<SUALCORM4> inicio(){
		if(listaPrincipal.size()==0){
			return listaPrincipal;
		}
		//si la lista tiene suficientes elementos obtenemos los primero de la lista
		//segun la paginacion 
		if((listaPrincipal.size())>paginacion ){
			pagina_actual=1;
			registro_Inicial=1;
			registro_Final=paginacion;
			return listaPrincipal.subList(0, paginacion);
		}else{
			pagina_actual=1;
			registro_Inicial=1;
			registro_Final=listaPrincipal.size();
			return listaPrincipal;
		}
	}
	/**
	 * Metodo para regrear los ultimos datos de la lista
	 * @return
	 */
	public List<SUALCORM4> fin(){
		if(listaPrincipal==null || listaPrincipal.size()==0 
				|| paginas_totales==0 || paginas_totales==1){
			pagina_actual=1;
			return listaPrincipal;
		}
		//si la lista tiene suficientes elementos obtenemos los utltimos de la lista
		//segun la paginacion 
		if(paginas_totales>1){
			pagina_actual=paginas_totales;
			registro_Inicial=((listaPrincipal.size()/paginacion)*paginacion)+1;
			registro_Final=listaPrincipal.size();
			return listaPrincipal.subList(((listaPrincipal.size()/paginacion)*paginacion),listaPrincipal.size());
		}else{
			pagina_actual=paginas_totales;
			if(listaPrincipal!=null && listaPrincipal.size()>0){
				registro_Inicial=((listaPrincipal.size()/paginacion)*paginacion)+1;
				registro_Final=listaPrincipal.size();
			}
			return listaPrincipal;
		}
	}
	/**
	 * Metodo para regresar los datos siguientes de la lista 
	 * o primeros si son los ultimos
	 * @return
	 */
	public List<SUALCORM4> siguiente(){
		if(listaPrincipal==null || listaPrincipal.size()==0 
				|| paginas_totales==0 || paginas_totales==1){
			return listaPrincipal;
		}
		//generamos la lista a regresar en base a la pagina actual
		if(pagina_actual==paginas_totales){
			return inicio();
		}else if((pagina_actual-1)==paginas_totales-1){
			return fin();
		}else{
			if(paginas_totales>1){
				pagina_actual++;
				if(pagina_actual>paginas_totales || pagina_actual==paginas_totales){
					return fin(); 
				}else{
					if(listaPrincipal!=null && listaPrincipal.size()>0){
						registro_Inicial=((pagina_actual-1)*paginacion)+1;
						registro_Final=((pagina_actual)*paginacion);
					}
					return listaPrincipal.subList(((pagina_actual-1)*paginacion),((pagina_actual)*paginacion));
				}
			}else{
				return inicio();
			}
		}
	}
	/**
	 * Metodo para regresar los datos anteriores 
	 * o los ultimos si son los primeros datos de la lista
	 * @return
	 */
	public List<SUALCORM4> anterior(){
		if(listaPrincipal==null || listaPrincipal.size()==0 
				|| paginas_totales==0 || paginas_totales==1){
			return listaPrincipal;
		}
		//generamos la lista a regresar en base a la pagina actual
		if(pagina_actual==1){
			return fin();
		}else if(pagina_actual==2){
			return inicio();
		}else{
			pagina_actual--;
			if(listaPrincipal!=null && listaPrincipal.size()>0){
				registro_Inicial=((pagina_actual-1)*paginacion)+1;
				registro_Final=(pagina_actual)*paginacion;
			}
			return listaPrincipal.subList(((pagina_actual-1)*paginacion),(pagina_actual)*paginacion);
			
		}
		
	}
	public int getPagina_actual() {
		return pagina_actual;
	}
	public int getPaginas_totales() {
		return paginas_totales;
	}
	public int getRegistro_Final() {
		return registro_Final;
	}
	public int getRegistro_Inicial() {
		return registro_Inicial;
	}
	public int getRegistrosTotales() {
		return registrosTotales;
	}
	
}

