package mx.altec.enlace.utilerias;

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class CalendarioHTML
	{
	// valores para crear el c�digo
	private GregorianCalendar inicio = null, fin = null;
	private String estiloDiaLaboral = "";
	private String estiloDiaNoLaboral = "";
	private String estiloSabadoDomingo = "";
	private String anclaHTML = "";
	private Hashtable tablaInhabiles = new Hashtable();
	private String nombreMes[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo",
		"Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
		"Diciembre", "mes nuevototote"};
	private boolean habilitaPrimeroUltimo = false;

	// para guardar �ltimo c�digo generado
	private StringBuffer codigoHTML = new StringBuffer("");
	boolean cambio = false;

	// --- constructores -----------------------------------------------------
	public CalendarioHTML() {;}

	public CalendarioHTML(GregorianCalendar inicio, GregorianCalendar fin)
		{
		this.inicio = inicio;
		this.fin = fin;
		}

	public CalendarioHTML(GregorianCalendar inicio, GregorianCalendar fin, boolean habilitaPrimeroUltimo)
		{
		this.habilitaPrimeroUltimo = habilitaPrimeroUltimo;
		this.inicio = inicio;
		this.fin = fin;
		}

	// --- m�todos set -------------------------------------------------------
	public void setInicio(GregorianCalendar fecha) {inicio = fecha; cambio = true;}
	public void setFin(GregorianCalendar fecha) {fin = fecha; cambio = true;}
	public void setEstiloDiaLaboral(String cadena) {estiloDiaLaboral = cadena; cambio = true;}
	public void setEstiloDiaNoLaboral(String cadena) {estiloDiaNoLaboral = cadena; cambio = true;}
	public void setEstiloSabadoDomingo(String cadena) {estiloSabadoDomingo = cadena; cambio = true;}

	// --- m�todos get -------------------------------------------------------
	public GregorianCalendar getInicio() {return inicio;}
	public GregorianCalendar getFin() {return fin;}
	public String getEstiloDiaLaboral() {return estiloDiaLaboral;}
	public String getEstiloDiaNoLaboral() {return estiloDiaNoLaboral;}
	public String getEstiloSabadoDomingo() {return estiloSabadoDomingo;}

	// --- genera c�digo para un mes o parte de �l ---------------------------
	private String mesHTML(int mes, int anio, int diaInicio, int diaFin)
		{
		GregorianCalendar primerDia = new GregorianCalendar(anio,mes,diaInicio);
		int inicioSemana = primerDia.get(Calendar.DAY_OF_WEEK);
		int columna = inicioSemana;
		StringBuffer resultado = new StringBuffer("");
		String titulo = nombreMes[mes] + " " + anio;
		StringBuffer tramaInhabiles = new StringBuffer("");
		int inhabil = 0;
		int max = primerDia.getActualMaximum(Calendar.DAY_OF_MONTH);

		// Se obtiene la trama de inh�biles ----------------------------------
		if(tablaInhabiles.containsKey("" + anio + (mes < 10 ? "0":"") + mes))
			{tramaInhabiles.append((String)tablaInhabiles.get("" + anio + (mes < 10 ? "0":"") + mes));}
		if(tramaInhabiles.length()>0)
			{
			inhabil = Integer.parseInt(tramaInhabiles.substring(0,2));
			tramaInhabiles.delete(0, 2);
			}

		// ---
		// Se crea el ancla si es necesario ----------------------------------
		if(anclaHTML.equals("" + anio + (mes < 10 ? "0":"") + mes)) resultado.append("<A name=\"mesActual\"></A>");

		// Se crea la cabecera -----------------------------------------------
		resultado.append("<table width='300' border='0' cellspacing='0' " +
			"cellpadding='0'><tr><td><img src='/gifs/website/gau10010.gif' " +
			"width='1' height='7' alt='..' name='.'></td></tr><tr><td " +
			"class='tittabdat'><img src='/gifs/website/gau10010.gif' " +
			"width='1' height='2' alt='..' name='.'></td></tr><tr><td><img " +
			"src='/gifs/website/gau10010.gif' width='1' height='7' alt='..'" +
			" name='.'></td></tr></table><table width=300 border=0 " +
			"cellspacing=0 cellpadding=0><tr><td width=15><img " +
			"src=/gifs/EnlaceMig/gau25010.gif width=15 height=20 border=0 " +
			"alt=Cerrar></td><td class=titpag width=285 valign=top" +
			" colspan=6>" + titulo + "</td></tr></table>");

		// Se colocan las cabeceras "lunes", "martes", etc -------------------
		resultado.append("<table width=300 border=0 cellspacing=5 " +
			"cellpadding=0><TR><td class=tabtexcal align=center>Dom</td><td" +
			" class=tabtexcal align=center>Lun</td><td class=tabtexcal " +
			"align=center>Mar</td><td class=tabtexcal align=center>Mie</td>" +
			"<td class=tabtexcal align=center>Jue</td><td class=tabtexcal " +
			"align=center>Vie</td><td class=tabtexcal align=center>Sab</td>" +
			"</TR>");

		// Se colocan los espacios en blanco ---------------------------------
		resultado.append("<TR>");

		for(int i = 1; i < inicioSemana; i++)
			resultado.append("<TD class=\"" + estiloDiaLaboral + "\">&nbsp;</td>");

		// Se colocan los d�as -----------------------------------------------
		for(int i = diaInicio; i <= diaFin; i++)
			{
			if(i == inhabil)
				{
				if(tramaInhabiles.length()>0)
					{
					inhabil = Integer.parseInt(tramaInhabiles.substring(0,2));
					tramaInhabiles.delete(0, 2);
					}
				resultado.append("<TD class=\"" + estiloDiaNoLaboral + "\" align=center>" + i + "</TD>");
				}
			else
				{
				switch(columna)
					{
					case 2: case 3: case 4: case 5: case 6:
						resultado.append("<TD class=\"" + estiloDiaLaboral + "\" align=center><A href=\"javascript:dayClick(" + i + "," + mes + "," + anio + ")\">" + i + "</A></TD>");
						break;
					case 1:
						if(habilitaPrimeroUltimo && (i == 1 || i == max))
							resultado.append("<TR><TD class=\"" + estiloDiaLaboral + "\" align=center><A href=\"javascript:dayClick(" + i + "," + mes + "," + anio + ")\">" + i + "</A></TD>");
						else
							resultado.append("<TR><TD class=\"" + estiloSabadoDomingo + "\" align=center>" + i + "</TD>");
						break;
					case 7:
						if(habilitaPrimeroUltimo && (i == 1 || i == max))
							resultado.append("<TD class=\"" + estiloDiaLaboral + "\" align=center><A href=\"javascript:dayClick(" + i + "," + mes + "," + anio + ")\">" + i + "</A></TD></TR>");
						else
							resultado.append("<TD class=\"" + estiloSabadoDomingo + "\" align=center>" + i + "</TD></TR>");
					}
				}
			columna=(columna == 7)?1:columna+1;
			}

		// Se colocan los blancos finales ------------------------------------
		if(columna > 1)
			{
			for(int i = columna; i <= 7; i++) resultado.append("<TD class=\"" + estiloDiaLaboral + "\">&nbsp;</td>");
			resultado.append("</TR>");
			}

		// Se coloca el pie del calendario -----------------------------------
		resultado.append("</TABLE></CENTER><table width='300' border='0' " +
			"cellspacing='0' cellpadding='0'><tr><td class='tittabdat'>" +
			"<img src='/gifs/website/gau10010.gif' width='1' height='5'>" +
			"</td></tr></table><br>");

		return resultado.toString();
		}
	// -----------------
	private String mesHTML(int mes, int anio, int diaInicio)
		{return mesHTML(mes, anio, diaInicio, (new GregorianCalendar(anio,mes,diaInicio)).getActualMaximum(Calendar.DAY_OF_MONTH));}
	// -----------------
	private String mesHTML(int mes, int anio) {return mesHTML(mes,anio, 1);}

	// --- genera c�digo para el intervalo establecido -----------------------
	public String generaCodigo()
		{
		if(inicio == null || fin == null) return null;
		if(inicio.get(Calendar.YEAR) > fin.get(Calendar.YEAR)) return null;
		if(inicio.get(Calendar.YEAR) == fin.get(Calendar.YEAR) &&
			inicio.get(Calendar.MONTH) > fin.get(Calendar.MONTH)) return null;
		if(inicio.get(Calendar.YEAR) == fin.get(Calendar.YEAR) &&
			inicio.get(Calendar.MONTH) == fin.get(Calendar.MONTH) &&
			inicio.get(Calendar.DATE) > fin.get(Calendar.DATE)) return null;
		if(!cambio) return codigoHTML.toString();

		codigoHTML = new StringBuffer("");
		cambio = false;

		int anioI = inicio.get(Calendar.YEAR),
			anioF = fin.get(Calendar.YEAR),
			mesI = inicio.get(Calendar.MONTH),
			mesF = fin.get(Calendar.MONTH),
			diaI = inicio.get(Calendar.DATE),
			diaF = fin.get(Calendar.DATE);
		int fechaI = anioI * 100 + mesI,
			fechaF = anioF * 100 + mesF,
			fechaA = 0;

		if(fechaI == fechaF) codigoHTML.append(mesHTML(mesI,anioI,diaI,diaF));
		if(fechaI < fechaF) codigoHTML.append(mesHTML(mesI,anioI,diaI));
		for (fechaA =
			((fechaI%100<11)?(fechaI+1):((fechaI/100 + 1)*100));
			fechaA < fechaF;
			fechaA=( (fechaA%100 >= 11) ? ((fechaA/100 + 1)*100) : fechaA+1 ))
			{codigoHTML.append(mesHTML(fechaA%100,fechaA/100));}
		if(fechaI < fechaF) codigoHTML.append(mesHTML(mesF,anioF,1,diaF));
		return codigoHTML.toString();
		}

	// --- Indica las fechas de d�as inh�biles -------------------------------
	public void setInhabiles(String listaFechasSeparadasConComa)
		{
		StringTokenizer fechas = new StringTokenizer(listaFechasSeparadasConComa," ,");
		String clave, contenido, aux;
		int a, b;

		tablaInhabiles = new Hashtable();
		while(fechas.hasMoreTokens())
			{
			contenido = fechas.nextToken();
			aux = "" + (Integer.parseInt(contenido.substring(3,5)) - 1);
			if(aux.length() == 1) aux = "0" + aux;
			clave = contenido.substring(6) + aux;
			contenido = contenido.substring(0, 2);
			if(tablaInhabiles.containsKey(clave)) contenido = tablaInhabiles.get(clave) + contenido;
			tablaInhabiles.put(clave,"" + contenido);
			}
		}

	// --- Indica el ancla ---------------------------------------------------
	public void setAncla(int mes, int ann)
		{anclaHTML = "" + ann + (mes < 10 ? "0":"") + mes;}

	}