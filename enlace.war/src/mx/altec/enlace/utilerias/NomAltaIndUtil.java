package mx.altec.enlace.utilerias;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class NomAltaIndUtil.
 */
public final class NomAltaIndUtil {
	
	/** La constante SEPARADOR_PIPE. */
	public static final String SEPARADOR_PIPE = "\\|";
	
	/** La constante RELLENA_DERECHA. */
	public static final boolean RELLENA_DERECHA = true;
	
	/** La constante RELLENA_IZQUIEDA. */
	public static final boolean RELLENA_IZQUIEDA = false;

	/** La constante PARAMETROS. */
	public static final String [] PARAMETROS_ALTA_IND = {
		"numeroEmpleado|7",
		"numeroDepto|6",
		"apellidoPaterno|30",
		"apellidoMaterno|20",
		"nombreEmpleado|30",
		"rfc|10",
		"homoclave|3",
		"genero|1",
		"cveNacionalidad|4",
		"edoCivil|1",
		"nombreCorto|26",
		"domicilio|60",
		"colonia|30",
		"delegacion|30",
		"ciudad|20",
		"cp|5",
		"paisEmpleado|4",
		"estatusCasa|1",
		"fechaRecidencia|8|FECHA",
		"lada|6",
		"telefono|8",
		"fechaIngreso|8|FECHA",
		"sucursal|4",
		"claveEnvio|1",
		"domicilioOficina|60",
		"coloniaOficina|30",
		"delegacionOficina|30",
		"ciudadOficina|20",
		"cpOficina|5",
		"paisOficina|4",
		"ladaOficina|6",
		"telOficina|8",
		"extOficina|5",
		"ingresoMensual|18|IMPORTE",
		"numeroCuenta|11",
		"numeroTarjeta|16"
		};

	/**
	 * Nueva instancia nom alta ind util.
	 */
	private NomAltaIndUtil(){
		super();
	}
	
	/**
	 * Obtener empleado.
	 *
	 * @param req El objeto: req
	 * @return Objeto map
	 */
	public static Map<String, String> obtenerEmpleado(HttpServletRequest req) {
		Map<String, String> empleado = new HashMap<String, String>();
		String valor ="";
		for (String parametro : PARAMETROS_ALTA_IND) {
			parametro = parametro.split(SEPARADOR_PIPE)[0];
			valor = req.getParameter(parametro);
			empleado.put(parametro, (valor==null)?"":valor);
		}
		return empleado;
	}

	/**
	 * Rellena cadena.
	 *
	 * @param valor El objeto: valor
	 * @param longitud El objeto: longitud
	 * @param caracter El objeto: caracter
	 * @param esAlFinal El objeto: es al final
	 * @return Objeto string
	 */
	public static String rellenaCadena(String valor, int longitud, String caracter, boolean esAlFinal) {
		StringBuilder sb = new StringBuilder();
		valor = (valor==null)?"":valor;
		if (valor.length()>longitud) {
			return valor.substring(0, longitud);
		}
		if (esAlFinal) {
			sb.append(valor);
			while (sb.length()<longitud) {
				sb.append(caracter);
			}
		} else {
			int len = valor.length();
			while ((sb.length()+len)<longitud) {
				sb.append(caracter);
			}
			sb.append(valor);
		}
		return sb.toString();
	}

	/**
	 * Genera registro.
	 *
	 * @param empleado El objeto: empleado
	 * @param numeroDeElemento El objeto: numero de elemento
	 * @return Objeto string
	 */
	public static String generaRegistro(Map<String, String> empleado, Integer numeroDeElemento) {
		StringBuilder registro = new StringBuilder();
		String valor = "";
		int longitud = 0;
		registro.append("2").append(rellenaCadena(numeroDeElemento.toString(),5,"0", RELLENA_IZQUIEDA));
		for (String parametro : PARAMETROS_ALTA_IND) {
			String[] infoParam = parametro.split(SEPARADOR_PIPE);
			valor = empleado.get(infoParam[0]).trim();
			longitud = Integer.parseInt(infoParam[1]);
			if(infoParam.length==3){
				registro.append(formatearInfo(valor, infoParam[2],longitud));
			}else{				
				registro.append(rellenaCadena(valor,longitud," ", RELLENA_DERECHA));
			}
		}
		registro.append("\n");
		return registro.toString();
	}
	
	/**
	 * Formatear info.
	 *
	 * @param valor El objeto: valor
	 * @param formato El objeto: formato
	 * @param longitud El objeto: longitud
	 * @return Objeto string
	 */
	private static String formatearInfo(String valor, String formato, int longitud) {
		if ("FECHA".equals(formato)) {
			valor= valor.replaceAll("/", "");
			valor = rellenaCadena(valor,longitud," ", RELLENA_DERECHA);
		} else if("IMPORTE".equals(formato)) {
			valor = currencyToNumberWithoutDot(valor);
			valor = rellenaCadena(valor,longitud,"0", RELLENA_IZQUIEDA);
		}
		return valor;
	}
	
	/**
	 * Currency to number without dot.
	 *
	 * @param curr El objeto: curr
	 * @return Objeto string
	 */
	private static String currencyToNumberWithoutDot(String curr){
		if(curr==null||"".equals(curr)){
			return "";
		}
		curr = curr.replaceAll("\\$","");
		curr = curr.replaceAll(",","");
		curr = curr.replaceAll("\\.","");
		return curr;
	}

	/**
	 * Depura lista.
	 *
	 * @param listaEmpleados El objeto: lista empleados
	 * @param seleccion El objeto: seleccion
	 * @return Objeto list
	 */
	public static List<Map<String, String>> depuraLista(List<Map<String, String>> listaEmpleados,
			String[] seleccion) {
		List<Map<String, String>> seleccionados = new ArrayList<Map<String,String>>();
		for (String sel : seleccion) {
			seleccionados.add(listaEmpleados.get(Integer.parseInt(sel)));
		}
		return seleccionados;
	}
	
	/**
	 * Escribir respuesta.
	 *
	 * @param response El objeto: response
	 * @param respuesta El objeto: respuesta
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public static void escribirRespuestaCodigoPostal(HttpServletResponse response,
			StringBuilder respuesta) throws IOException {
		ServletOutputStream sos = null;
		try {
			sos = response.getOutputStream();
			sos.print(respuesta.toString());
			sos.flush();
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("***Error al cerrar outputstream:  [" + e.getMessage() + "]", EIGlobal.NivelLog.DEBUG);
		} finally {
			if (sos != null) {
				sos.close();
			}
		}
	}
	
}