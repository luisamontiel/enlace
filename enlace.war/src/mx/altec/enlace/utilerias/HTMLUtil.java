/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * HTMLUtil.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.utilerias;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import mx.altec.enlace.cliente.ws.csd.ConstantesSD;

/**
 * Clase de utilerias, para crear codigo html.
 * 
 * @author FSW Everis
 * 
 */
public final class HTMLUtil {

    /**
     * Constructor privado
     */
    private HTMLUtil() {
    }

    /**
     * Creacion de una equiteta HTML con atributos
     * @param idEtiqueta identificador de la etiqueta
     * @param attrs mapa de atributos de la etiqueta
     * @param contenido texto que contiene la etiqueta
     * @return Enum como resultado
     */
    public static HTMLEtiqueta crearElemento(int idEtiqueta, Map<String, String> attrs, String contenido) {

        HTMLEtiqueta etiqueta = HTMLEtiqueta.obtenerEtiqueta(idEtiqueta);
        if (etiqueta != null) {
            StringBuilder elemento = new StringBuilder(etiqueta.getAbre()).append(ConstantesSD.UN_ESPACIO);
            for (Entry<String, String> attr : attrs.entrySet()) {
                elemento.append(attr.getKey())
                    .append(ConstantesSD.SIGNO_IGUAL).append(ConstantesSD.SIGNO_DOBLE_COMA)
                    .append(attr.getValue()).append(ConstantesSD.SIGNO_DOBLE_COMA).append(ConstantesSD.UN_ESPACIO);
            }
            elemento.append(etiqueta.isSimple() ? ConstantesSD.CADENA_VACIA : ConstantesSD.SIGNO_MAYOR)
                .append(contenido);
            
            etiqueta.setEtiquetaAttr(elemento.append(etiqueta.getCierra()).toString());
        }
        return etiqueta;
    }

    /**
     * Genera mapa de atributos de una etiqueta HTML
     * @param arrayAttr arreglo de parametros
     * @return mapa de atributos
     */
    public static final Map<String, String> generarAttr(String [][] arrayAttr){
        Map<String, String> map = new HashMap<String, String>();
        for (int i = 0; arrayAttr != null &&  i < arrayAttr.length; i++) {
            map.put(arrayAttr[i][0], arrayAttr[i][1]);
        }
        return map;
    }
}
