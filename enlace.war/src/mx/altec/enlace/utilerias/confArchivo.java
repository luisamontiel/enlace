package mx.altec.enlace.utilerias;

import java.io.*;
import java.util.*;

import mx.altec.enlace.bo.confProveedor;


public class confArchivo {

	public confArchivo () {
	}

//	public void mLee ( String tipo, String persona) throws IOException {
	public ArrayList mLee ( String Archivo, String tipo) throws IOException {
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		ArrayList lista = new ArrayList ();
		RandomAccessFile fileAmbiente=null;

		try {
			File arch = new File(Archivo);
			fileAmbiente= new RandomAccessFile(arch,"r");
		} catch ( IOException e ) {
			System.out.println( "Error al leer el archivo de ambiente, e1 " + e);
		}

		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e2 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );

		try {
			do {
				if ( registroLeido.startsWith( tipo ) ) {
					if (tipo != "9") {
						campo =	registroLeido.substring(
							registroLeido.indexOf( ';' , 2 ) + 1 ,
							registroLeido.lastIndexOf( ';' ) );
						lista.add(campo);
					} else {
                                            String Temp = registroLeido.substring (5,
                                                        registroLeido.indexOf (';', 6));
						campo = registroLeido.substring(
							registroLeido.indexOf( ';' , 12 ) + 1 ,
							registroLeido.lastIndexOf( ';' ) - 2);
						campo = campo.replace(';',' ') ;
                                                confProveedor prov = new confProveedor (Temp, campo);
//						if ( registroLeido.indexOf( persona ) != -1 )
							lista.add(prov);
					}
				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		return (lista);
	}
}