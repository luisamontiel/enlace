/** 
*   Isban Mexico
*   Clase: EnlaceLynxUtils
*   Descripcion: Metodos auxiliares para procesar las peticiones hacia Lynx
*
*   Control de Cambios:
*   1.0 Julio del 2015 FSW Everis 
*/
package mx.altec.enlace.utilerias;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.CuentasDAO;
import mx.isban.conector.lynx.ConectorLynx;
import mx.isban.conector.lynx.dto.RespuestaLynxDTO;

/**
 * Clase de utilerias para la validacion y llamado hacia Lynx.
 */
public final class EnlaceLynxUtils {

    /**
     * Constante para el formato de fecha
     */
    public static final String FORMATO_FECHA = "yyyyMMddHHmmss";

    /**
     * Constructor privado
     */
    private EnlaceLynxUtils() {
    }

    /**
     * Metodo encargado de realizar el mapeo de los campos para la llamada a
     * lynx
     *
     * @param contrato valor del contrato
     * @param facultadesAltaCtas valor de las facultades
     * @param fechaHoraOper campo que contiene la fecha
     * @param cuentas mapa que contiene las cuentas
     * @param campos mapa con los campos para envio a lynx
     * @param arc objeto para leer el archivo
     * @throws IOException en caso de que ocurra un error
     */
    public static void llamadoConectorLynxAlta(String contrato, 
            Map facultadesAltaCtas, Date fechaHoraOper, 
            Map<String, String> cuentas, Map<Integer, String> campos,
            BufferedReader arc) throws IOException {
        String line;
        EIGlobal.mensajePorTrace("Entrando a llamadoConectorLynxAlta, lynx inicializado = " 
                + Global.LYNX_INICIALIZADO, EIGlobal.NivelLog.DEBUG);
        int countLineas = 0;
        int countProcesadas = 0;
        if (cuentas != null && Global.LYNX_INICIALIZADO
                && ((Boolean) facultadesAltaCtas.get("AutoCtasMB")
                || (Boolean) facultadesAltaCtas.get("AutoCtasOB"))) {
        	EIGlobal.mensajePorTrace("Cuentas a procesar: " + cuentas.size(), EIGlobal.NivelLog.DEBUG);
            // Iterar archivo de respuesta
            EI_Tipo eiTipo = new EI_Tipo();
            SimpleDateFormat fmtFecha = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, EnlaceLynxConstants.LOCALE_MX);
            campos.put(7, CuentasDAO.consultarIdUsuarioContrato(contrato));
            while ((line = arc.readLine()) != null) {
            	countLineas++;
                boolean reportarLynx = false;
                String cuenta = null;
                if (line.indexOf('@') < 0 && !line.startsWith("0;")) {
                    eiTipo.iniciaObjeto(';', '@', line + "@");
                    cuenta = eiTipo.camposTabla[0][1];
                    reportarLynx = cuentas.containsKey(cuenta) && ("P".equals(eiTipo.camposTabla[0][0]) // estatus pendiente de activar
                            || "A".equals(eiTipo.camposTabla[0][0])); // activada
                    EIGlobal.mensajePorTrace(
                            "MMC_Alta-> Lynx, valores de la linea: "
                            + ", cuenta - " + cuenta + "estatus - " + eiTipo.camposTabla[0][0]
                            , EIGlobal.NivelLog.DEBUG);
                }
                SimpleDateFormat fmtFechaIdTranUnic = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_ID_TRANS_UNI, EnlaceLynxConstants.LOCALE_MX);
                campos.put(3, fmtFechaIdTranUnic.format(new Date()) + cuenta); // Id unico de latransaccion
                if (reportarLynx) {
                	countProcesadas++;
                    llamadaAltaCuentas(fechaHoraOper, cuentas, campos,
                            fmtFecha, cuenta);
                }
            }
        }
        EIGlobal.mensajePorTrace("Saliendo llamadoConectorLynxAlta, Procesadas="
                + countProcesadas + "  de  " + (countLineas - 1), EIGlobal.NivelLog.INFO);
    }

    /**
     * Ejecuta la validacion de cuentas para enviar llamados a Lynx.
     * @param fechaHoraOper objeto de tipo fecha
     * @param cuentas mapa con las cuentas
     * @param campos mapa con los campos
     * @param fmtFecha objeto de tipo fecha
     * @param cuenta cuenta
     */
    private static void llamadaAltaCuentas(Date fechaHoraOper,
            Map<String, String> cuentas, Map<Integer, String> campos,
            SimpleDateFormat fmtFecha, String cuenta) {
        String[] detalleCta = cuentas.get(cuenta).split("[|]");
        campos.put(23, fmtFecha.format(fechaHoraOper));
        campos.put(24, campos.get(23));
        campos.put(29, cuenta);
        campos.remove(33); // Clave Swift o ABA
        campos.remove(34); // Pais Receptor
        campos.remove(35); // Ciudad Receptora
        if ("0".equals(detalleCta[0])) { // Mismo Banco
            campos.put(21, EnlaceLynxConstants.TIPOTRAN_ACTMB);
            campos.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
        } else if ("1".equals(detalleCta[0])) { // Banco Nacional
            campos.put(21, EnlaceLynxConstants.TIPOTRAN_ACTOB);
            campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERBANCARIAS);
            if(7 == detalleCta.length && "USD".equals(detalleCta[6])){
                campos.put(38, EnlaceLynxConstants.MONEDA_DOLAR);
            } else if(7 == detalleCta.length && "MXP".equals(detalleCta[6])){
                campos.put(38, EnlaceLynxConstants.MONEDA_ORIGINAL); 
            } else if(10 == detalleCta.length && "USD".equals(detalleCta[9])){
                campos.put(38, EnlaceLynxConstants.MONEDA_DOLAR);
            } else if(10 == detalleCta.length && "MXP".equals(detalleCta[9])){
                campos.put(38, EnlaceLynxConstants.MONEDA_ORIGINAL);
            } else{
                campos.put(38, EnlaceLynxConstants.CADENA_VACIA);
            }
        } else if ("2".equals(detalleCta[0])) { // Banco Internacional
            campos.put(21, EnlaceLynxConstants.TIPOTRAN_ACTBI);
            campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERNACIONALES);
            campos.put(33, detalleCta[8]); // Clave Swift o ABA
            campos.put(34, detalleCta[3]); // Pais Receptor
            campos.put(35, detalleCta[4]); // Ciudad Receptora
        } else if ("4".equals(detalleCta[0])) {//Cuentas Moviles
            if ("BANME".equals(detalleCta[5])) {
                campos.put(21, EnlaceLynxConstants.TIPOTRAN_ACMMB);
                campos.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
            } else {
                campos.put(21, EnlaceLynxConstants.TIPOTRAN_ACMOB);
                campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERBANCARIAS);
            }
        }
        EIGlobal.mensajePorTrace("Se envia mensaje a Lynx", EIGlobal.NivelLog.DEBUG);
        ConectorLynx.enviaMsg(campos, EnlaceLynxConstants.TIPO_MSG_NO_MONETARIA);
    }

    /**
     * Metodo encargado de realizar la llamada al conector de baja de cuentas.
     *
     * @param fechaHoraOper campo que contiene la fecha
     * @param cuentas mapa que contiene las cuentas
     * @param campos mapa con los campos para envio a lynx
     * @param arc objeto para leer el archivo
     * @param contrato contrato relacionado
     * @throws IOException en caso de que ocurra un error
     */
    public static void llamadoConectorLynxBaja(Date fechaHoraOper, 
            Map<String, String> cuentas, Map<Integer, String> campos, 
            BufferedReader arc, String contrato) throws IOException {
        EIGlobal.mensajePorTrace("Entrando a llamadoConectorLynxBaja, lynx inicializado = " 
                + Global.LYNX_INICIALIZADO , EIGlobal.NivelLog.DEBUG);
        int countProcesadas = 0;
        if (cuentas != null && Global.LYNX_INICIALIZADO) {
            EIGlobal.mensajePorTrace("Cuentas a procesar: " + cuentas.size(), EIGlobal.NivelLog.DEBUG);
            // Iterar archivo de respuesta
            EI_Tipo eiTipo = new EI_Tipo();
            SimpleDateFormat fmtFecha = new SimpleDateFormat(
                    EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, EnlaceLynxConstants.LOCALE_MX);
            String line;
            campos.put(7, CuentasDAO.consultarIdUsuarioContrato(contrato));
            while ((line = arc.readLine()) != null) {
                if (line.indexOf('@') < 0 && !line.startsWith("0;")) {
                    eiTipo.iniciaObjeto(';', '@', line + "@");
                    String estatus = eiTipo.camposTabla[0][0];
                    String cuenta = eiTipo.camposTabla[0][1];
                    EIGlobal.mensajePorTrace(
                            "llamadoConectorLynxBaja-> Lynx, valores de la linea: "
                            + ", cuenta - " + cuenta + ", estatus - " + estatus
                            , EIGlobal.NivelLog.DEBUG);
                    if (cuentas.containsKey(cuenta) && "P".equals(estatus)
                            || "A".equals(estatus)) { // Pendientes no se informan
                        String[] detalleCta = cuentas.get(cuenta).split("[|]");

                        campos.put(23, fmtFecha.format(fechaHoraOper));
                        campos.put(24, campos.get(23));
                        campos.put(29, cuenta);
                        campos.remove(33); // Clave Swift o ABA
                        campos.remove(34); // Pais Receptor
                        campos.remove(35); // Ciudad Receptora
                        if ("0".equals(detalleCta[0])) { // Mismo Banco
                            campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCTMB);
                            campos.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
                        } else if ("1".equals(detalleCta[0])) { // Banco Nacional
                            campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCTOB);
                            campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERBANCARIAS);
                        } else if ("2".equals(detalleCta[0])) { // Banco Internacional
                            campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCI);
                            campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERNACIONALES);
                            campos.put(33, detalleCta[9]); // Clave Swift o ABA
                            campos.put(34, detalleCta[3]); // Pais Receptor
                            campos.put(35, detalleCta[4]); // Ciudad Receptora
                        } else if ("4".equals(detalleCta[0])) {//Cuentas Moviles
                            if ("BANME".equals(detalleCta[5])) {
                                campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCMMB);
                                campos.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
                            } else {
                                campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCMOB);
                                campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERBANCARIAS);
                            }
                        }
                        SimpleDateFormat fmtFechaIdTranUnic = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_ID_TRANS_UNI, EnlaceLynxConstants.LOCALE_MX);
                        campos.put(3, fmtFechaIdTranUnic.format(new Date()) + cuenta); // Id unico de latransaccion
                        countProcesadas++;
                        EIGlobal.mensajePorTrace("Se envia mensaje a Lynx", EIGlobal.NivelLog.DEBUG);
                        ConectorLynx.enviaMsg(campos, EnlaceLynxConstants.TIPO_MSG_NO_MONETARIA);
                    }
                }
            }
        }
        EIGlobal.mensajePorTrace("Saliendo llamadoConectorLynxBaja, Procesadas="
                + countProcesadas, EIGlobal.NivelLog.INFO);
    }

    /**
     * Metodo encargado de realizar la llamada al conector de baja de cuentas.
     *
     * @param cuentas mapa que contiene las cuentas
     * @param campos mapa con los campos para envio a lynx
     * @param contrato the value of contrato
     * @param canceladas lista de cuentas a reportar
     */
    public static void llamadoConectorLynxBaja(
            Map<String, String> cuentas, Map<Integer, String> campos, 
            String contrato, List<String> canceladas) {
        EIGlobal.mensajePorTrace("Entrando a llamadoConectorLynxBaja2, lynx inicializado = " 
                + Global.LYNX_INICIALIZADO , EIGlobal.NivelLog.DEBUG);
        int countProcesadas = 0;
        if (cuentas != null && canceladas != null && Global.LYNX_INICIALIZADO) {
            EIGlobal.mensajePorTrace("Cuentas a procesar: " + canceladas.size(), 
                    EIGlobal.NivelLog.DEBUG);
            // Iterar listado de cuentas
            SimpleDateFormat fmtFecha = new SimpleDateFormat(
                    EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, EnlaceLynxConstants.LOCALE_MX);
            campos.put(7, CuentasDAO.consultarIdUsuarioContrato(contrato));
            for (String cuenta : canceladas) {
                EIGlobal.mensajePorTrace(
                        "llamadoConectorLynxBaja-> Lynx, valores de la linea: "
                        + ", cuenta - " + cuenta, EIGlobal.NivelLog.DEBUG);
                if (cuentas.containsKey(cuenta)) {
                    String[] detalleCta = cuentas.get(cuenta).split("[|]");

                    campos.put(23, fmtFecha.format(new Date()));
                    campos.put(24, campos.get(23));
                    campos.put(29, cuenta);
                    campos.remove(33); // Clave Swift o ABA
                    campos.remove(34); // Pais Receptor
                    campos.remove(35); // Ciudad Receptora
                    if ("0".equals(detalleCta[0])) { // Mismo Banco
                        campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCTMB);
                        campos.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
                    } else if ("1".equals(detalleCta[0])) { // Banco Nacional
                        campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCTOB);
                        campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERBANCARIAS);
                    } else if ("2".equals(detalleCta[0])) { // Banco Internacional
                        campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCI);
                        campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERNACIONALES);
                        campos.put(33, detalleCta[9]); // Clave Swift o ABA
                        campos.put(34, detalleCta[3]); // Pais Receptor
                        campos.put(35, detalleCta[4]); // Ciudad Receptora
                    } else if ("4".equals(detalleCta[0])) {//Cuentas Moviles
                        if ("BANME".equals(detalleCta[5])) {
                            campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCMMB);
                            campos.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
                        } else {
                            campos.put(21, EnlaceLynxConstants.TIPOTRAN_BCMOB);
                            campos.put(28, EnlaceLynxConstants.TIPOCTA_INTERBANCARIAS);
                        }
                    }
                    SimpleDateFormat fmtFechaIdTranUnic = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_ID_TRANS_UNI, EnlaceLynxConstants.LOCALE_MX);
                    campos.put(3, fmtFechaIdTranUnic.format(new Date()) + cuenta); // Id unico de latransaccion
                    countProcesadas++;
                    EIGlobal.mensajePorTrace("Se envia mensaje a Lynx", EIGlobal.NivelLog.DEBUG);
                    ConectorLynx.enviaMsg(campos, EnlaceLynxConstants.TIPO_MSG_NO_MONETARIA);
                }
            }
        }
        EIGlobal.mensajePorTrace("Saliendo llamadoConectorLynxBaja, Procesadas="
                + countProcesadas, EIGlobal.NivelLog.INFO);
    }

    /**
     * Arma y realiza la llamada al conector lynx para autorizar tranferencias.
     *
     * @param usuario valor del usuario
     * @param contrato valor del contrato
     * @param ctaOrigen valor de la cuenta origen
     * @param ctaDestino valor de la cuenta destino
     * @param importeString importe
     * @param fechaProgramada fecha programada
     * @param session variable de session
     * @param arrIvas valores de los Ivas
     * @param arrRFCs valores de los RFCs
     * @param map mapa de los valores que se enviaran al conector
     * @param indice1 valor del indice
     * @return respuestaLynx respuesta de la llamada al conector
     */
    public static RespuestaLynxDTO llamadoConectorLynxTranferencia(String usuario,
            String contrato, String ctaOrigen, String ctaDestino,
            String importeString, String fechaProgramada,
            BaseResource session, String[] arrIvas, String[] arrRFCs,
            Map<Integer, String> map, int indice1) {
        RespuestaLynxDTO respuestaLynx;
        EIGlobal.mensajePorTrace("Incio de metodo que envia operacion a lynx, llamadoConectorLynxTranferencia", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Inicio mapeo para envio a lynx en transferencias en linea", EIGlobal.NivelLog.DEBUG);
        SimpleDateFormat format = new SimpleDateFormat(
                EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, EnlaceLynxConstants.LOCALE_MX);
        StringBuilder sb = new StringBuilder();
        importeString = formateaImporte(importeString, sb);
        // Se agregan valores faltantes a mapa de valores para armar el mensaje.
        SimpleDateFormat formatoHora = new SimpleDateFormat(
                EnlaceLynxConstants.FORMATO_FCH_ID_TRANS_UNI, EnlaceLynxConstants.LOCALE_MX);
        // Se agregan valores faltantes a mapa de valores para armar el mensaje.
        map.put(3, formatoHora.format(new Date()).concat(ctaOrigen)); // Id unico de latransaccion
        map.put(6, usuario); // Identificador del cliente
        map.put(8, contrato); // Identificador de contrato
        map.put(9, session.getNombreContrato()); // Nombre de contrato
        map.put(22, EnlaceLynxConstants.FORMLIQ_MISMO_DIA);
        map.put(25, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);  // Tipo de cuenta origen
        map.put(26, ctaOrigen);//Cuenta Origen									
        map.put(26, ctaOrigen); // Numero de cuenta donde el cliente desea realizar la transaccion
        map.put(30, ctaDestino); // Numero de Cuenta a la que se hara el Abono
        map.put(31, EnlaceLynxConstants.ENT_ORIGEN); // Entidad financiera destino, a la que pertenece el beneficiario de la operacion
        map.put(32, EnlaceLynxConstants.PAIS); //Pais de la cuenta destino 
        map.put(35, format.format(new Date())); // Fecha y hora en que se envia la operacion al aplicativo para su tratamiento
        map.put(36, map.get(35)); // Fecha y hora en que se realizo la operacion
        map.put(37, EnlaceLynxConstants.TRANSFERENCIA); // Comentario asociado al pago (transferencia, traspaso de saldo, etc)
        map.put(38, importeString);//Importe de la transaccion
        map.put(40, importeString);//Importe en moneda original										
        map.put(52, EnlaceLynxConstants.TIPO_CARGO); // Tipo cargo
        map.put(54, arrRFCs[indice1]); // RFC
        map.put(55, (arrRFCs[indice1] != null && arrRFCs[indice1].trim().length()>0 ? 
                EnlaceLynxConstants.COMPROBANTE_CON_IVA : EnlaceLynxConstants.COMPROBANTE_NINGUNO)); // IVA
        map.put(65, formateaFecha(fechaProgramada, sb)); // Fecha planificada para hacer la transferencia
        // Se crea objeto de respuesta para almacenar el codigo y mensaje de respuesta de la operacion hacia lynx
        EIGlobal.mensajePorTrace("Inicia envio de mensjae a Lynx", EIGlobal.NivelLog.DEBUG);
        respuestaLynx = ConectorLynx.enviaMsg(map, EnlaceLynxConstants.TIPO_MSG_TMB);
        EIGlobal.mensajePorTrace("Termina envio de mensjae a Lynx", EIGlobal.NivelLog.DEBUG);
        return respuestaLynx;
    }

    /**
     * Metodo encargado de realizar la llamada al conector lynx para
     * transferencia del archivo.
     *
     * @param req Objeto con informacion inherente a la peticion
     * @param edoCF informacion RFC del cliente cuando es necesario
     * @param cta_origen cuenta origen
     * @param cta_destino cuenta destino
     * @param session informacion de la session
     */
    public static void llamadoConectorLynxTranferenciaArchivo(HttpServletRequest req,
            String edoCF, String cta_origen, String cta_destino,
            BaseResource session) {
        EIGlobal.mensajePorTrace("Inicio mapeo para envio a lynx en transferencias en linea", EIGlobal.NivelLog.DEBUG);
        SimpleDateFormat format = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, EnlaceLynxConstants.LOCALE_MX);
        String fchEnvio = format.format(new Date());  // Fecha y hora de envio
        // Se crea mapa para envio a conector lynx
        Map<Integer, String> map = new HashMap<Integer, String>();
        // Se cargan valores por default para el mensaje
        EnlaceLynxConstants.cargaValoresDefaultPB(map, req);
        // Se agregan valores faltantes a mapa de valores para armar el mensaje.
        SimpleDateFormat formatoHora = new SimpleDateFormat(
            EnlaceLynxConstants.FORMATO_FCH_ID_TRANS_UNI, EnlaceLynxConstants.LOCALE_MX);
        map.put(6, session.getUserID8()); // Identificador del cliente
        map.put(7, CuentasDAO.consultarIdUsuarioContrato(session.getContractNumber())); // Identificador del cliente dueno de la cuenta
        map.put(8, session.getContractNumber()); // Identificador de contrato
        map.put(9, session.getNombreContrato()); // Nombre de contrato
        map.put(26, cta_origen); // Numero de cuenta donde el cliente desea realizar la transaccion
        map.put(30, cta_destino); // Numero de Cuenta a la que se hara el Abono
        map.put(31, EnlaceLynxConstants.ENT_ORIGEN); // Entidad financiera destino, a la que pertenece el beneficiario de la operacion
        map.put(32, EnlaceLynxConstants.PAIS); //Pais de la cuenta destino 
        map.put(36, fchEnvio); // Fecha y hora en que se envia la operacion al aplicativo para su tratamiento
        map.put(37, EnlaceLynxConstants.TRANSFERENCIA); // Comentario asociado al pago (transferencia, traspaso de saldo, etc)
        map.put(48, EnlaceLynxConstants.PAIS); // Pais del movil recargado (no encontrado)
        map.put(55, ("s".equalsIgnoreCase(edoCF)) ? "1" : "0"); // RFC del cliente cuando es necesario un comprobante fiscal
        map.put(3, formatoHora.format(new Date())); // Id unico de latransaccion
        // Se crea objeto de respuesta para almacenar el codigo y mensaje de respuesta de la operacion hacia lynx
        EIGlobal.mensajePorTrace("Configuracion de Lynx esta inicializada? " + Global.LYNX_INICIALIZADO, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Inicia guardado de mensaje en Lynx", EIGlobal.NivelLog.DEBUG);
        ConectorLynx.enviarInfo(map, session.getFolioArchivo(), EnlaceLynxConstants.TIPO_MSG_TMB);
        EIGlobal.mensajePorTrace("Termina guardado de mensaje en Lynx", EIGlobal.NivelLog.DEBUG);
    }

    /**
     * Metodo encargado de realizar la llamada al conector lynx para la
     * autorizacion/cancelacion.
     *
     * @param mapaRegistrosLynx mapa con registros a informar
     * @param req request para obtener datos de la peticion
     * @param tipoSolic el tipo de solicitud Alta o Baja
     * @param totalRegistros el numero de operaciones que se quieren relizar
     * @param registroAceptados el numero de operaciones que se aceptaron
     * @param arc buffer reader con errores
     * @return respuesta con el resultado de la operacion
     */
    public static RespuestaLynxDTO llamadoConectorLynxAutorizacionCancelacion(Map<String, LynxAutCanCuentasDTO> mapaRegistrosLynx, 
    		HttpServletRequest req, String tipoSolic, int totalRegistros, String registroAceptados, BufferedReader arc) {
    	RespuestaLynxDTO respuesta = new RespuestaLynxDTO(null, EnlaceLynxConstants.ERROR_CODE_NOK, "Error al enviar operaciones a Lynx");
    	EIGlobal.mensajePorTrace("Inicia llamadoConectorLynxAutorizacionCancelacion", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(">>>> Parametros: tipoSolicitud: " + tipoSolic
            + ">>>> Parametros: tipoSolicitud: " + tipoSolic
            + ", total: " + totalRegistros
            + ", aceptados: " + registroAceptados
            , EIGlobal.NivelLog.INFO);
		String cuenta;
        boolean procesar = false;
        if (String.valueOf(totalRegistros).equals(registroAceptados)) {
            procesar = true;
        } else {
            try {
                String line = arc.readLine();
                if (line != null) {
                    while (line != null) {
                        EIGlobal.mensajePorTrace(">>>>While linea: " + line, EIGlobal.NivelLog.DEBUG);
                        cuenta = line.substring(0, line.indexOf(';'));
                        if (mapaRegistrosLynx.containsKey(cuenta)) {
                            mapaRegistrosLynx.remove(cuenta);
                            EIGlobal.mensajePorTrace(">>>> MapaLynx contiene llave cuenta con error y esta se remueve: " + cuenta, EIGlobal.NivelLog.DEBUG);
                        }
                        line = arc.readLine();
                    }
                    procesar = (mapaRegistrosLynx.isEmpty()) ? false : true;
                } else {
                    procesar = true;
                }
            } catch (IOException ioe) {
                EIGlobal.mensajePorTrace("Error en buffer reader", EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTraceExcepcion(ioe);
            }
        }

        if (procesar) {
            EIGlobal.mensajePorTrace("Procesando " + mapaRegistrosLynx.size() + " operaciones", EIGlobal.NivelLog.DEBUG);
            int errorCont = 0;
            String idUsrCta = "";
            for (Map.Entry<String, LynxAutCanCuentasDTO> entry : mapaRegistrosLynx.entrySet()) {
                if ((LynxAutCanCuentasDTO.ESTATUS_PENDIENTE_AUTORIZAR.equals(entry.getValue().getDescripcionStatus()) 
                        && LynxAutCanCuentasDTO.TIPO_OPERACION_AUTORIZACION.equals(entry.getValue().getTipoOperacion()))
                    || (LynxAutCanCuentasDTO.NOM_ESTATUS_AUTORIZAR.equals(entry.getValue().getDescripcionStatus()) 
                        && LynxAutCanCuentasDTO.TIPO_OPERACION_CANCELACION.equals(entry.getValue().getTipoOperacion())
                        && LynxAutCanCuentasDTO.TIPO_SOLICITUD_ALTA.equals(entry.getValue().getTipoAccion()))) {
                    EIGlobal.mensajePorTrace("<-- Entry --> \n Valores registro"
                            + ", Contrato: " + entry.getValue().getNumContrato()
                            + ", Clase cta: " + entry.getValue().getClaseCuenta()
                            + ", Desc. estatus: " + entry.getValue().getDescripcionStatus()
                            + ", Fch Registro: " + entry.getValue().getFechaRegistro()
                            + ", Nom. estatus: " + entry.getValue().getNomEstatus()
                            + ", Cta: " + entry.getValue().getNumCuenta()
                            + ", Origen alta: " + entry.getValue().getOrigenAlta()
                            + ", Tipo Cta: " + entry.getValue().getTipoCuenta()
                            + ", Tipo Oper.: " + entry.getValue().getTipoOperacion()
                            + ", Titular: " + entry.getValue().getTitular(), EIGlobal.NivelLog.DEBUG);
                    if (idUsrCta == null || "".equals(idUsrCta)) {
                        idUsrCta = CuentasDAO.consultarIdUsuarioContrato(entry.getValue().getNumContrato());   
                    }
                    respuesta = ConectorLynx.enviaMsg(entry.getValue().generaMapaLynx(req, entry.getValue().getNumContrato(), idUsrCta), EnlaceLynxConstants.TIPO_MSG_NO_MONETARIA);
                    errorCont = errorCont + ((EnlaceLynxConstants.ERROR_CODE_NOK.equals(respuesta.getCodError())) ? 1 : 0);
                }
            }
            respuesta.setCodError(errorCont > 0 ? EnlaceLynxConstants.ERROR_CODE_NOK : EnlaceLynxConstants.ERROR_CODE_OK);
            respuesta.setMsgError(errorCont > 0 ? "Algunos registros no se informaron a lynx." : EnlaceLynxConstants.CADENA_VACIA);
        }
        EIGlobal.mensajePorTrace("Termina llamadoConectorLynxAutorizacionCancelacion", EIGlobal.NivelLog.DEBUG);
        return respuesta;
    }
    
    /**
     * Da formato al importe para que no tenga punto decimal con las ultimas
     * dos posiciones para decimales.
     * @param importe importe a dar formato
     * @param buffer opcionalmente un builder a usar para el formato
     * @return el importe formateado
     */
    public static String formateaImporte(String importe, StringBuilder buffer) {
        StringBuilder sb;
        if (buffer != null) {
            buffer.setLength(0);
            sb = buffer;
        } else {
            sb = new StringBuilder();
        }
        int idxPunto = importe.indexOf('.'); // Formatear importe
        sb.append(importe);
        sb.append("00");
        if (idxPunto >= 0) {
            sb.setLength(idxPunto + 3);
            sb.deleteCharAt(idxPunto);
        }
        return sb.toString();
    }
    
    /**
     * Da formato a la fecha de YYYYMMDDHHMMSS considerando que viene DDMMYY.
     * @param fecha dato de fecha en formato DDMMYY
     * @param buffer opcionalmente un builder a usar para el formato
     * @return la cadena formateada
     */
    public static String formateaFecha(String fecha, StringBuilder buffer) {
        StringBuilder sb;
        if (buffer != null) {
            buffer.setLength(0);
            sb = buffer;
        } else {
            sb = new StringBuilder();
        }
        if (fecha != null && fecha.length() >= 6) {
            sb.append("20").append(fecha.substring(4, 6));
            sb.append(fecha.substring(2, 4));
            sb.append(fecha.substring(0, 2));
            sb.append("000000");
        }
        return sb.toString();
    }
    
    /**
     * Metodo que obtiene el tipo de alta por archivo
     * @param tipo el tipo de cuenta
     * @return cadena con el tipo de alta
     */
    public static String obtenerTipoAltaPorArchivo(String tipo) {
        String idTipo = EnlaceLynxConstants.CADENA_VACIA;
        if (tipo != null && tipo.length() > 0) {
            switch (tipo.charAt(0)) {
                case 'S': idTipo = "0"; break;
                case 'E': idTipo = "1"; break;
                case 'I': idTipo = "2"; break;
                case 'M': idTipo = "4"; break;
            }
        }
        return idTipo;
    }
}
