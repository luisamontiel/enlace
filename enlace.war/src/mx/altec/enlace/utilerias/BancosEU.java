package mx.altec.enlace.utilerias;

import java.util.Vector;
import mx.altec.enlace.bo.TrxGPF2BO;

/**
 *  Clase Singleton para el uso adecuado del catalogo de la TRX GPF2
 *  que reemplaza a la tabla CAMB_BANCO_EU
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 May 04, 2011
 */
public class BancosEU {
	
    /**
     * Instancia estatica Singleton de BancosEU.
     */
	private static BancosEU INSTANCE = null;
	
	/**
	 * Lista de bancos que se consultan con ls TRX GPF2
	 */
	private Vector listaBancosEU = null;
	
	/**
	 * Metodo para obtener valor de listaBancosEU
	 * @return listaBancosEU Lista de bancos EU
	 */
	public Vector getListaBancosEU(){
		return this.listaBancosEU;
	}
	   
    /**
     * Constructor privado para evitar instancia.
     */
    private BancosEU() {
    	TrxGPF2BO gpf2EU = new TrxGPF2BO();
    	try{
    		this.listaBancosEU = gpf2EU.obtenCatalogoCambBancoEU();
    	}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("BancosEU::BancosEU:: Problemas al llenar el catalogo con la TRX GPF2 ", EIGlobal.NivelLog.ERROR);			
			EIGlobal.mensajePorTrace("BancosEU::BancosEU:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);    		
    	}
    	
    }
 
    /**
     * Metodo estatico para crear instancia de la clase  
     */
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new BancosEU();
        }
    }
 
    /**
     * Metodo getter para obtener valor de INSTANCE
     * @return INSTANCE Instancia de la clase BancosEU
     */
    public static BancosEU getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

}
