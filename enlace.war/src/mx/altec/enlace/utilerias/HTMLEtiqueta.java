/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * HTMLEtiqueta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.utilerias;

/**
 * La enumeracion HTMLEtiqueta para contener equiquetas HTML.
 * FSW Everis
 */
public enum HTMLEtiqueta {
    
    /**  Equiqueta table. */
    TABLE(1, "<table", "</table>", "", false),
    
    /**  Equiqueta th. */
    TH(2, "<th", "</th>", "", false),
    
    /**  Equiqueta tr. */
    TR(3, "<tr", "</tr>", "", false),
    
    /**  Equiqueta td. */
    TD(4, "<td", "</td>", "", false),
    
    /**  Equiqueta div. */
    DIV(5, "<div", "</div>", "", false),
    
    /**  Equiqueta h3. */
    H3(6, "<h2", "</h2>", "", false),
    
    /**  Equiqueta input. */
    INPUT(7, "<input", "/>", "", true),
    
    /** etiqueta div con estilos.*/
    TDS(8, "<td class=\"label\"", "</td>", "", false);
    
    /** La variable id. */
    private int id;
    
    /** La variable abre. */
    private String abre;
    
    /** La variable cierra. */
    private String cierra;
    
    /** La variable etiquetaAttr. */
    private String etiquetaAttr;
    
    /** La variable simple. */
    private boolean simple;
    
    /**
     * Contructor de HTML etiqueta.
     *
     * @param id el parametro id
     * @param abre el parametro abre
     * @param cierra el parametro cierra
     * @param etiquetaAttr el parametro etiqueta Attr
     * @param simple el parametro simple
     */
    private HTMLEtiqueta(int id, String abre, String cierra, String etiquetaAttr, boolean simple) {
        this.id = id;
        this.abre = abre;
        this.cierra = cierra;
        this.etiquetaAttr = etiquetaAttr;
        this.simple = simple;
    }

    /**
     * Obtiene el valor de la variable id.
     *
     * @return el id
     */
    public int getId() {
        return id;
    }

    /**
     * Coloca el valor de id.
     *
     * @param id es el nuevo valor de id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Obtiene el valor de la variable abre.
     *
     * @return el abre
     */
    public String getAbre() {
        return abre;
    }

    /**
     * Coloca el valor de abre.
     *
     * @param abre es el nuevo valor de abre
     */
    public void setAbre(String abre) {
        this.abre = abre;
    }

    /**
     * Obtiene el valor de la variable cierra.
     *
     * @return el cierra
     */
    public String getCierra() {
        return cierra;
    }

    /**
     * Coloca el valor de cierra.
     *
     * @param cierra es el nuevo valor de cierra
     */
    public void setCierra(String cierra) {
        this.cierra = cierra;
    }
    
    /**
     * Obtiene el valor de la variable etiqueta attr.
     *
     * @return el etiqueta attr
     */
    public String getEtiquetaAttr() {
        return etiquetaAttr;
    }
    
    /**
     * Coloca el valor de etiqueta attr.
     *
     * @param etiquetaAttr es el nuevo valor de etiqueta attr
     */
    public void setEtiquetaAttr(String etiquetaAttr) {
        this.etiquetaAttr = etiquetaAttr;
    }

    /**
     * Checa si es simple.
     *
     * @return true, si es simple
     */
    public boolean isSimple() {
        return simple;
    }

    /**
     * Coloca el valor de simple.
     *
     * @param simple es el nuevo valor de simple
     */
    public void setSimple(boolean simple) {
        this.simple = simple;
    }

    /**
     * Obtener Enum por id.
     *
     * @param id identificador de Equiqueta a buscar
     * @return Enum encontrado
     */
    public static HTMLEtiqueta obtenerEtiqueta(int id){
        for (HTMLEtiqueta etiqueta : HTMLEtiqueta.values()) {
            if (etiqueta.getId() == id) {
                return etiqueta;
            }
        }
        return null;
    }
    
}
