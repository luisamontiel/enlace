package mx.altec.enlace.utilerias;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import mx.altec.enlace.beans.DetalleCuentasBean;
import mx.altec.enlace.bo.ValidaArchivoPagos;
import mx.altec.enlace.dao.MAC_Registro;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.servlets.BaseServlet;

/**
 * Realiza las validaciones de los layouts de los archivos
 * de carga de nomina y carga de cuentas registradas en
 * cat&aacute;logo de n&oacute;mina.
 * El atributo "errores" contiene la lista de errores
 * encontrados durante el procesamiento del archivo.
 *
 * @author Christian Israel Castro Ram�rez
 *
 */
public class DetalleCuentasValidador {
	private Vector<String> errores = null;
	private List<DetalleCuentasBean> cuentas;
	//Importe total de las cuentas del archivo de carga de nomina.
	private Double sumImporte = new Double(0);
	private Integer registrosConConcepto;
	private String contrato;
	private String usuario;
	private String perfil;

	public DetalleCuentasValidador() {
		errores = new Vector<String>();
		cuentas = new ArrayList<DetalleCuentasBean>();
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	/**
	 * De un archivo de carga de n&oacute;mina o de
	 * carga de cuentas registradas en el cat&aacute;logo de
	 * n&oacute;mina, extrae los n�meros de cuenta, nombre(s),
	 * apellido paterno y apellido materno; y los regresa en
	 * una lista de objetos de tipo DetalleCuentasBean.
	 * Previamente realiza la validaci&oacute;n del archivo y
	 * de sus cuentas.
	 *
	 * @param archivoCuentas
	 * @return
	 * @throws DetalleCuentasException
	 */
	public List<DetalleCuentasBean> obtenerCuentasArchivo(File archivo, BaseServlet baseServlet)
			throws DetalleCuentasException{
		try{
			//Valida el layout del archivo.
			validarArchivo(archivo, baseServlet);
			if(errores.size() > 0){
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Validacion exitosa del archivo.&", EIGlobal.NivelLog.INFO);
				//Valida la lista de cuentas del archivo.
				validarCuentas();
			}

			if(errores.size() > 0)
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Validacion exitosa de las cuentas.&", EIGlobal.NivelLog.INFO);
		}catch(IOException e){
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Excepcion al obtener las cuentas del archivo: "+e, EIGlobal.NivelLog.ERROR);
			errores.add("El archivo no pudo ser le�do.");
			throw new DetalleCuentasException("Ocurri� un error al procesar el archivo.");
		}

		if(errores.size() > 0){
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Hubo errores en el archivo.&", EIGlobal.NivelLog.INFO);
			throw new DetalleCuentasException("Ocurri� un error al procesar el archivo.");
		}

		return cuentas;
	}

	/**
	 * Valida que un archivo de carga de n&oacute;mina o de
	 * carga de cuentas registradas en el cat&aacute;logo de
	 * n&oacute;mina, cumplan con el layout especificado.
	 *
	 * @param archivoCuentas
	 * @return
	 * @throws IOException
	 */
	private void validarArchivo(File archivo, BaseServlet baseServlet) throws IOException{
		//Determina el tipo de archivo que es: Nomina o Cuentas Registradas.
		if(esArchivoNomina(archivo)){
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Es un archivo de carga de nomina.&", EIGlobal.NivelLog.INFO);
			//Valida el layout del archivo.
			validarLayoutArchivoNomina(archivo, baseServlet);
			//Extrae del archivo la lista cuentas contenidas.
			if(errores.size() <= 0){
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Validacion exitosa del layout del archivo de carga de nomina.&", EIGlobal.NivelLog.INFO);
				cuentas = extraerCuentasArchivoNomina(archivo);
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Se extrajeron las cuentas del archivo de carga de nomina.&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Cantidad de cuentas extraidas: "+cuentas.size(), EIGlobal.NivelLog.DEBUG);
			}
		}else if(esArchivoCuentasRegistradas(archivo)){
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Es un archivo de carga de cuentas registradas.&", EIGlobal.NivelLog.INFO);
			//Valida el layout del archivo.
			validarLayoutArchivoCuentasRegistradas(archivo);
			//Extrae del archivo la lista cuentas contenidas.
			if(errores.size() <= 0){
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Validacion exitosa del layout del archivo de carga de cuentas registradas.&", EIGlobal.NivelLog.INFO);
				cuentas = extraerCuentasArchivoCuentasRegistradas(archivo);
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Se extrajeron las cuentas del archivo de carga de cuentas registradas.&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Cantidad de cuentas extraidas: "+cuentas.size(), EIGlobal.NivelLog.DEBUG);
			}
		}else{
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &El archivo no corresponde al layout de carga de nomina, ni al layout de carga de cuentas registradas.&", EIGlobal.NivelLog.INFO);
			errores.add("El archivo no corresponde a los layouts establecidos.");
		}
	}

	/**
	 * Determina si un archivo pertenece al layout de carga de
	 * n&oacute;mina.
	 * Considera que la informaci&oacute;n de las cuentas se
	 * encuentra entre la segunda y la antepen&uacute;ltima l&iacute;nea
	 * del archivo. Si entre esas l&iacute;neas se encuentra una vac&iacute;a
	 * el archivo no ser&aacute; considerado del tipo de carga
	 * de n&oacute;mina, lo mismo si el archivo no tiene contenido.
	 * @param archivo
	 * @return
	 * @throws IOException
	 */
	private boolean esArchivoNomina(File archivo) throws IOException{
		int CARACTERES_PERMITIDOS_POR_LINEA = 129;
		int CARACTERES_PERMITIDOS_POR_LINEAB = 127;
		int LINEAS_EXCLUYENTES_INICIO_ARCHIVO = 1;
		int LINEAS_EXCLUYENTES_FIN_ARCHIVO = 1;
		int PRIMERA_LINEA_CONTENIDO = 2;
		int lineasArchivo = getNumberOfLines(archivo);
		int ultimaLineaContenido = lineasArchivo-LINEAS_EXCLUYENTES_FIN_ARCHIVO;

		if(lineasArchivo-(LINEAS_EXCLUYENTES_INICIO_ARCHIVO+LINEAS_EXCLUYENTES_FIN_ARCHIVO) <= 0){
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &El archivo de carga de nomina no tiene contenido.&", EIGlobal.NivelLog.INFO);
			return false;
		}

		BufferedReader in = new BufferedReader(new FileReader(archivo));
	    String linea = "";
	    int lineaActual = 0;
	    while ((linea = in.readLine()) != null) {
	    	lineaActual++;
	    	if((lineaActual >= PRIMERA_LINEA_CONTENIDO) &&
	    		(lineaActual <= ultimaLineaContenido) &&
	    		(linea.length() != CARACTERES_PERMITIDOS_POR_LINEA && linea.length() != CARACTERES_PERMITIDOS_POR_LINEAB)){
	    			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Linea del archivo no compatible con el layout de carga de nomina: "+lineaActual, EIGlobal.NivelLog.DEBUG);
	    			in.close();
	    			return false;
	    	}
	    }
	    in.close();
		return true;
	}

	/**
	 * Determina si un archivo pertenece al layout de carga de
	 * cuentas registradas en el cat&aacute;logo de n&oacute;mina.
	 * Considera que la informaci&oacute;n de las cuentas se
	 * encuentra en todas las l&iacute;neas del archivo
	 * Si entre esas l&iacute;neas se encuentra una vac&iacute;a
	 * el archivo no ser&aacute; considerado del tipo de carga
	 * de cuentas registradas en el cat&aacute;logo de n&oacute;mina,
	 * lo mismo si el archivo no tiene contenido.
	 * @param archivo
	 * @return
	 * @throws IOException
	 */
	private boolean esArchivoCuentasRegistradas(File archivo) throws IOException{
		int CARACTERES_PERMITIDOS_POR_LINEA = 98;
		int LINEAS_EXCLUYENTES_FIN_ARCHIVO = 0;
		int PRIMERA_LINEA_CONTENIDO = 1;
		int lineasArchivo = getNumberOfLines(archivo);
		int ultimaLineaContenido = lineasArchivo-LINEAS_EXCLUYENTES_FIN_ARCHIVO;

		if(lineasArchivo < PRIMERA_LINEA_CONTENIDO){
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &El archivo de carga de cuentas registradas no tiene contenido en la primera linea.&", EIGlobal.NivelLog.INFO);
			return false;
		}

		BufferedReader in = new BufferedReader(new FileReader(archivo));
		String linea = "";
	    int lineaActual = 0;
	    while ((linea = in.readLine()) != null) {
	    	lineaActual++;
	    	EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Archivo de carga de cuentas, linea: "+lineaActual+" caracteres en la linea: "+linea.length(), EIGlobal.NivelLog.DEBUG);
	    	if((lineaActual >= PRIMERA_LINEA_CONTENIDO) &&
	    		(lineaActual <= ultimaLineaContenido) &&
	    		(linea.length() != CARACTERES_PERMITIDOS_POR_LINEA)){
	    			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Linea del archivo no compatible con el layout de carga de cuentas registradas: "+lineaActual, EIGlobal.NivelLog.DEBUG);
	    			in.close();
	    			return false;
	    	}
	    }
	    in.close();
		return true;
	}

	/**
	 * Obtiene el n&uacute;mero de l&iacute;neas de un archivo.
	 * @param archivo
	 * @return
	 * @throws IOException
	 */
	private int getNumberOfLines(File archivo) throws IOException{
		BufferedReader in = new BufferedReader(new FileReader(archivo));
		int numberOfLines = 0;
		while ((in.readLine()) != null) {
			numberOfLines++;
		}
		in.close();
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Numero de lineas en el archivo: "+numberOfLines, EIGlobal.NivelLog.DEBUG);
		return numberOfLines;
	}

	/**
	 * Valida el layout de un archivo de carga de n&oacute;mina
	 * utilizando una r&eacute;plica de la funci&oacute;n ejecutaValidaArchivo
	 * de la clase ImportNomina.
	 * @param archivoNomina
	 * @param baseServlet
	 * @throws IOException
	 */
	private void validarLayoutArchivoNomina(File archivoNomina, BaseServlet baseServlet) throws IOException{
		FileReader reader = new FileReader(archivoNomina);
		BufferedReader arch = new BufferedReader(reader);
		String registroEnc	= arch.readLine();
		ValidaArchivoPagos validaArchivo = new ValidaArchivoPagos(baseServlet,archivoNomina.getAbsolutePath());
		String encNumSec = new String( registroEnc.substring( 1, 6 ) );
		registrosConConcepto = new Integer(0);

		//Validar primera linea.
		esValidaPrimeraLineaArchivoNomina(registroEnc, validaArchivo, encNumSec, baseServlet);
		//Validar cuerpo.
		esValidoCuerpoArchivoNomina(archivoNomina, arch, validaArchivo, encNumSec);
		//Validar ultima linea.
		esValidaUltimaLineaArchivoNomina(archivoNomina,arch,validaArchivo);

		if (arch!=null){
			arch.close();
		}
	}

	/**
	 * Tomado de la funcion ejecutaValidaArchivo de la clase ImportNomina.
	 * @param registroEnc
	 * @param validaArchivo
	 * @return
	 */
	private boolean esValidaPrimeraLineaArchivoNomina(String registroEnc, ValidaArchivoPagos validaArchivo,
													  String encNumSec, BaseServlet baseServlet){
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Validando primera linea del archivo de carga de nomina.&", EIGlobal.NivelLog.INFO);
		boolean archivoValido = true;
		String encTipoReg = "";
		String encSentido = "";
		String encFechGen = "";
		String encCuenta = "";
		String encFechApli = "";
		try{


			encTipoReg	= new String( registroEnc.substring( 0, 1 ) );
		    if ( validaArchivo.validaDigito( encTipoReg ) == false ||
		    		validaArchivo.validaEncTipoReg( encTipoReg ) == false ) {
		    	errores.add("Linea 1 - Error en el primer Caracter del encabezado, se esperaba 1.");
				archivoValido = false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el Tipo registro encabezado&", EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class en encNumSec a: ("+encNumSec+")", EIGlobal.NivelLog.INFO);
			encNumSec = encNumSec.trim();
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class en encNumSec b: ("+encNumSec+")", EIGlobal.NivelLog.INFO);
			if ( validaArchivo.validaDigito( encNumSec ) == false ||
				validaArchivo.validaPrimerNumSec( encNumSec ) == false ) {
				errores.add("Linea 1 - Error en el n&uacute;mero secuencial del encabezado, se esperaba 1.");
				archivoValido=false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en n&uacute;mero secuencial del encabezado&", EIGlobal.NivelLog.INFO);
			}

			encSentido = new String(registroEnc.substring(6,7));
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class  encSentido:"+encSentido, EIGlobal.NivelLog.INFO);
			if (validaArchivo.validaLetras(encSentido)==false||validaArchivo.validaSentido(encSentido)==false){
				errores.add("Linea 1 - Error en el caracter de sentido del encabezado, se esperaba E.");
				archivoValido = false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el sentido del encabezado, se esperaba 'E'&", EIGlobal.NivelLog.INFO);
			}

			encFechGen = new String(registroEnc.substring(7,15));
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &encFechGen:"+encFechGen, EIGlobal.NivelLog.INFO);
			if (validaArchivo.validaDigito(encFechGen)==false||validaArchivo.fechaValida(encFechGen)==false){
				errores.add("Linea 1 - Error en la fecha de generacion del archivo.");
				archivoValido	= false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en la fecha de generacion del archivo&", EIGlobal.NivelLog.INFO);
			}

			//Validacion de la cuenta en contrato, si no pertenece al contrato
			encCuenta = new String( registroEnc.substring( 15,31 ) );
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &encCuenta:"+ encCuenta, EIGlobal.NivelLog.INFO);
			ValidaCuentaContrato valCtas = new ValidaCuentaContrato();
			String datoscta[]	= null;

			datoscta = valCtas.obtenDatosValidaCuenta(getContrato(),
			  		  								  		   getUsuario(),
			  		  								  		   getPerfil(),
			  		  								  		   encCuenta.trim(),
			  		  								  		   IEnlace.MEnvio_pagos_nomina);

			if(datoscta == null){
				errores.add("Linea 1 - la cuenta (" + encCuenta + ") no existe en el contrato o no est� disponible para el servicio de n�mina. .");
				archivoValido	= false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en la cuenta de cargo&", EIGlobal.NivelLog.INFO);
			}

			encFechApli = new String(registroEnc.substring(registroEnc.length() - 8,registroEnc.length()));
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &encFechApli:", EIGlobal.NivelLog.INFO);
			if (validaArchivo.validaDigito(encFechApli)==false||validaArchivo.fechaValida(encFechApli)==false){
				errores.add("Linea 1 - Error en la fecha de aplicacion del archivo.");
				archivoValido=false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en la fecha de aplicacion&", EIGlobal.NivelLog.INFO);
			}
		}catch(Exception e){
			errores.add("Linea 1 - Error de formato  en el archivo.");
			archivoValido=false;
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
		}
		return archivoValido;
	}

	/**
	 * Tomado de la funcion ejecutaValidaArchivo de la clase ImportNomina.
	 * @param archivoNomina
	 * @return
	 * @throws IOException
	 */
	private boolean esValidoCuerpoArchivoNomina(
			File archivoNomina, BufferedReader arch, ValidaArchivoPagos validaArchivo,
			String encNumSec)
			throws IOException{
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Validando cuerpo del archivo de carga de nomina.&", EIGlobal.NivelLog.INFO);
		boolean archivoValido = true;
		String nomArchNomina = archivoNomina.getAbsolutePath();
		int cuentaDeRegistros = registrosEnArchivo(nomArchNomina);
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Se obtuvo la cantidad de registros en el archivo.&", EIGlobal.NivelLog.INFO);
		int longitudArreglo = cuentaDeRegistros - 2;
		String[][] arrayDetails;
		arrayDetails = new String[longitudArreglo][9];
		Vector cantidadRegistros    = new Vector();
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Valor de encNumSec: "+encNumSec, EIGlobal.NivelLog.DEBUG);
		/* Detalle del archivo */
		if ( encNumSec != null ) {
			cantidadRegistros.addElement( encNumSec );
		}

		int longTotalDetalle	= 0;
		String registroDet		= "";  //jtg saco la declaracion de variables del for
		String tipoRegistro		= "";
		String NumeroSecuencia	= "";
		String NumeroEmpleado	= "";
		String ApellidoPaterno	= "";
		String ApellidoMaterno	= "";
		String Nombre			= "";
		String NumeroCuenta		= "";
		String Importe			= "";
		double num				= 0.0;
		int contadorLinea		= 1;
		/*************** Nomina Concepto de Archvo */
		String Concepto         = "";
		boolean registroContieneConcepto=false;
		registrosConConcepto= 0;

		Vector<?> listaIDConceptos=getIDConceptos();
		/*************** Nomina Concepto de Archvo */
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Se obtuvo la lista de idConceptos.&", EIGlobal.NivelLog.INFO);

		for (int i= 0; i < arrayDetails.length; i++){
			try{
				try{
					registroDet		= arch.readLine();
					if(registroDet==null) //jtg   si es nulo no puedo obtener los datos
						continue;
					if(! (registroDet.length() == 127  || registroDet.length()==129) ){
						errores.add("Linea "+(i+2)+" - Error en el tama�o del Registro.");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);
						continue;
					}

					tipoRegistro	= new String ( registroDet.substring(0,1)		);
					NumeroSecuencia	= new String ( registroDet.substring(1,6)		);
					NumeroEmpleado	= new String ( registroDet.substring(6,13)		);
					ApellidoPaterno	= new String ( registroDet.substring(13,43)		);
					ApellidoMaterno	= new String ( registroDet.substring(43,63)		);
					Nombre			= new String ( registroDet.substring(63,93)		);
					NumeroCuenta	= new String ( registroDet.substring(93,109)	);
					Importe			= new String ( registroDet.substring(109,127)	);
					/*************** Nomina Concepto de Archvo */
					if(registroDet.length()>127){
					   Concepto        = new String ( registroDet.substring(127)    );
					   registrosConConcepto++;
					   registroContieneConcepto=true;
					}
					/*************** Nomina Concepto de Archvo */
					num = Double.parseDouble(Importe);
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Importe de la cuenta: "+num, EIGlobal.NivelLog.DEBUG);
					sumImporte = sumImporte + num;
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Se modifica valor de sumImporte, nuevo valor: "+sumImporte, EIGlobal.NivelLog.DEBUG);
				}catch(Exception e) {
					e.printStackTrace();
				}

				arrayDetails[i][0] = tipoRegistro;
				if (validaArchivo.validaDigito( arrayDetails[i][0] ) == false ||
						validaArchivo.validaDetalleTipoReg( arrayDetails[i][0] ) == false ) {
					errores.add("Linea "+(i+2)+" - Error en el tipo de Registro del detalle.");
				    archivoValido=false;
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);
				}

				arrayDetails[i][1]=NumeroSecuencia;
				if (validaArchivo.validaDigito(arrayDetails[i][1])==false||validaArchivo.validaNumSec(i, arrayDetails[i][1])==false){
					errores.add("Linea "+(i+2)+" - Error en el n&uacute;mero secuencial del detalle.");
				    archivoValido=false;
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el n&uacute;mero secuencial del detalle&", EIGlobal.NivelLog.INFO);
				}
				arrayDetails[i][2]=NumeroEmpleado;
				arrayDetails[i][3]=ApellidoPaterno;
				arrayDetails[i][4]=ApellidoMaterno;
				arrayDetails[i][5]=Nombre;
				arrayDetails[i][6]=NumeroCuenta;
				if (validaArchivo.validaCuenta(arrayDetails[i][6])==false)
				{
					errores.add("Linea "+(i+2)+" - Error en el n&uacute;mero de Cuenta.");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el n&uacute;mero de Cuenta&", EIGlobal.NivelLog.INFO);
				}
	            //******fin de modificacion para integracion

				/*************** Nomina Concepto de Archvo */
				if(validaArchivo.validaDigito(Concepto)==false ||
					  (registroContieneConcepto && Concepto.trim().equals("")))
				{
					errores.add("Linea "+(i+2)+" - Identificador no v&aacute;lido en el campo Concepto.");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el n&uacute;mero de Concepto&", EIGlobal.NivelLog.INFO);
				}
				else/* Validacion en archivo  */
			    if(listaIDConceptos!=null && Concepto.trim().length()>0 && listaIDConceptos.indexOf(Concepto)<0)
			    {
			    	EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class & El Concepto no esta registrado.", EIGlobal.NivelLog.INFO);
			    	errores.add("Linea "+(i+2)+" - La clave del Concepto no est&aacute; Registrada.");
				    archivoValido=false;
			    }
				/*************** Nomina Concepto de Archvo */

				arrayDetails[i][7]=Importe;
				if (validaArchivo.validaDigito(arrayDetails[i][7])==false||arrayDetails[i][7].endsWith(" ")){
					errores.add("Linea "+(i+2)+" - Error en el Importe.");
					archivoValido=false;
				}
				else{
					String ctaAux1="";
					ctaAux1=arrayDetails[i][6].substring(0,11);
					if(ctaAux1.trim().length()!=11&&ctaAux1.trim().length()!=10){
						errores.add("Linea "+(i+2)+" - Error en la Cuenta, no est&aacute; justificada correctamente.");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en la cuenta no esta justificada correctamente&", EIGlobal.NivelLog.DEBUG);
					 }
				}

				if (Importe.trim().length() == 0){
					errores.add("Linea "+(i+2)+" - Debe especificar importe.");
					archivoValido=false;
				}
				cantidadRegistros.addElement(arrayDetails[i][1]);
				String registroDetalleCompleto;
				registroDetalleCompleto=arrayDetails[i][0]+arrayDetails[i][1]+arrayDetails[i][2]+arrayDetails[i][3]+arrayDetails[i][4]+arrayDetails[i][5]+arrayDetails[i][6]+arrayDetails[i][7];
				longTotalDetalle= registroDetalleCompleto.length();
			}catch(Exception e){
				e.printStackTrace();
				archivoValido=false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error de excepcion "+e, EIGlobal.NivelLog.DEBUG);
			}
			contadorLinea++;
			if((contadorLinea%1000)==0)
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class Importando el registro ### "+contadorLinea, EIGlobal.NivelLog.INFO);
		}//del for

		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class Registros con concepto ### "+registrosConConcepto, EIGlobal.NivelLog.DEBUG);

		return archivoValido;
	}

	/**
	 * Tomado de la funcion registrosEnArchivo de la clase ImportNomina.
	 * @param pathArchivoNom
	 * @return
	 * @throws IOException
	 */
	private int registrosEnArchivo(String pathArchivoNom)
		throws IOException{
		EIGlobal.mensajePorTrace( "***DetalleCuentasValidador.class &Entrando al metodo registrosEnArchivo 35&", EIGlobal.NivelLog.INFO);
		pathArchivoNom = pathArchivoNom.trim();
		BufferedReader arch = null;
		FileReader reader = null;
		reader  = new FileReader( pathArchivoNom );
		arch	= new BufferedReader( reader );

		String registroLeido = "";
		long posicion		 = 0;
		int cantRegistros    = 0;
		try {
			do {
				registroLeido = arch.readLine();
					if (registroLeido != null)
						cantRegistros++;
					} while( registroLeido != null );
		} catch(IllegalArgumentException e) {
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class Ha ocurrido una excepcion en: %registrosEnArchivo(1)"+e, EIGlobal.NivelLog.INFO);
		} catch(FileNotFoundException e) {
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class Ha ocurrido una excepcion en: %registrosEnArchivo(2)"+e, EIGlobal.NivelLog.INFO);
		} catch(SecurityException e){
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class Ha ocurrido una excepcion en: %registrosEnArchivo(3)"+e, EIGlobal.NivelLog.INFO);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class Ha ocurrido una excepcion en: %registrosEnArchivo(4)"+e, EIGlobal.NivelLog.INFO);
		}

		//***** everis Cerrando BufferedReader 08/05/2008  ..inicio
		finally {
			try {
		if (arch!=null){
			arch.close();
		}
			}catch(IOException e) {
				e.printStackTrace();
			}
			try {
		if(reader != null) {
			reader.close();
		}
			}catch(IOException e) {
				e.printStackTrace();
			}
		}

		EIGlobal.mensajePorTrace("***registrosEnArchivo() cantRegistros: "+cantRegistros, EIGlobal.NivelLog.INFO);
		return cantRegistros;
	}

	/**
	 * Tomado de la funcion getIDConceptos de la clase ImportNomina.
	 * @return
	 */
	private Vector getIDConceptos()
	{
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Obteniendo IDs de conceptos.&", EIGlobal.NivelLog.INFO);
	   EI_Query BD= new EI_Query();
	   Vector listaIDConceptos=new Vector();

	   String sqlIDConcepto  = "SELECT id_concepto from INOM_CONCEPTOS order by id_concepto";
	   listaIDConceptos =BD.ejecutaQueryCampo(sqlIDConcepto);
	   EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Se obtuvieron "+listaIDConceptos.size()+" IDs de conceptos.", EIGlobal.NivelLog.INFO);
	   if( listaIDConceptos==null )
		 {
			listaIDConceptos=new Vector();
			EIGlobal.mensajePorTrace( "DetalleCuentasValidador - getIDConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "DetalleCuentasValidador - getIDConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);
			for(int a=1;a<14 ;a++ )
			 {
			   String dato=(a<10)?"0"+a:""+a;
			   listaIDConceptos.add(dato);
			 }
		 }
	   EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Saliendo de obtener IDs de conceptos.&", EIGlobal.NivelLog.INFO);
	   return listaIDConceptos;
	}

	/**
	 * Tomado de la funcion ejecutaValidaArchivo de la clase ImportNomina.
	 * @param archivoNomina
	 * @return
	 * @throws IOException
	 */
	private boolean esValidaUltimaLineaArchivoNomina(
			File archivoNomina, BufferedReader arch, ValidaArchivoPagos validaArchivo) throws IOException{
		EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Validando ultima linea de archivo de carga de nomina.&", EIGlobal.NivelLog.INFO);
		boolean archivoValido = true;
		double sumTotal = 0;
		String nomArchNomina = archivoNomina.getAbsolutePath();
		int registrosLeidos = registrosEnArchivo(nomArchNomina);
		//Registro Sumario
		String registroSum = arch.readLine();
		if(registroSum != null) {
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &registroSum&"+registroSum.length(), EIGlobal.NivelLog.DEBUG);
		}
		else {
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &registroSum& viene nulo.", EIGlobal.NivelLog.ERROR);
		}
		String sumTipoReg  = "";
		String sumNumSec   = "";
		String sumTotRegs  = "";
		String sumImpTotal = "";

        try{
			if (registroSum == null || registroSum.length() != 29){
				errores.add("Linea Final- Error en la longitud del sumario.");
			    archivoValido=false;
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en la longitud del sumario&", EIGlobal.NivelLog.INFO);
			}else {
				sumTipoReg=new String(registroSum.substring(0,1));
			    if (validaArchivo.validaDigito(sumTipoReg)==false||validaArchivo.validaSumTipoReg(sumTipoReg)==false){
					errores.add("Linea Final - Error en el n&uacute;mero de registro.");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el n&uacute;mero de registro del sumario&", EIGlobal.NivelLog.INFO);
			    }

			    sumNumSec= new String(registroSum.substring(1,6));
			    if(validaArchivo.validaDigito(sumNumSec) && validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)){
//			    	session.setLastSecNom(sumNumSec);
			    }
			    if (validaArchivo.validaDigito(sumNumSec)==false||validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)==false){
			    	errores.add("Linea Final - Error en el n&uacute;mero secuencial del sumario.");
			    	archivoValido=false;
			    	EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el n&uacute;mero secuencial del sumario&", EIGlobal.NivelLog.INFO);
			    }

			    sumTotRegs = new String(registroSum.substring(6,11));
			    if(validaArchivo.validaDigito(sumTotRegs) && validaArchivo.validaTotalDeRegistros(sumTotRegs, registrosLeidos)){
//			    	session.setTotalRegsNom(sumTotRegs);
				}
			    if(validaArchivo.validaDigito(sumTotRegs)==false||validaArchivo.validaTotalDeRegistros(sumTotRegs, registrosLeidos)==false){
			    	errores.add("Linea Final - Error en el total de registros del sumario.");
			    	archivoValido=false;
			    	EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el total de registros&", EIGlobal.NivelLog.INFO);
				}

			    sumImpTotal = new String(registroSum.substring(11,29));
			    if (validaArchivo.validaDigito(sumImpTotal)  ){
//			    	session.setImpTotalNom(sumImpTotal);
			    }
			    validaArchivo.cerrar();

				/*************** Nomina Concepto de Archvo */
//				if(registrosConConcepto==0)
//				 {
//					if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
//						request.getSession().removeAttribute("conceptoEnArchivo");
//					request.getSession().setAttribute("conceptoEnArchivo","false");
//				 }e
				// registros de ddetalle existentes en el archivo de pago de

				int longitudArreglo = registrosLeidos - 2;
				// La longitud del arreglo esta determinada por la cantidad dnomina
				if ( longitudArreglo < 0 )
					longitudArreglo = 0;

				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Total de Registros en Archivo: "+longitudArreglo, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Total de Registros con Concepto: "+registrosConConcepto, EIGlobal.NivelLog.INFO);
//				if(registrosConConcepto!=0 && registrosConConcepto!= (Integer.parseInt(session.getNominaNumberOfRecords())) )
				if(registrosConConcepto!=0 && registrosConConcepto!= longitudArreglo )
				{
					 errores.add("Error: Si los registros incluyen Concepto debe especificarlo en todos.");
					 archivoValido=false;
					 EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Si los registros incluyen concepto debe especificarlo en todos.&", EIGlobal.NivelLog.INFO);
				 }
				/*************** Nomina Concepto de Archvo */

				sumTotal = Double.parseDouble(sumImpTotal);
				EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Se compara sumTotal: "+sumTotal+" con sumImporte: "+sumImporte, EIGlobal.NivelLog.DEBUG);
				/* Suma de los importes */
				if (sumTotal != sumImporte){
					errores.add("Linea Final - Error en el Importe total.");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error en el Importe total&", EIGlobal.NivelLog.INFO);
				}
			} //else != 29
        }catch(Exception e){
        	errores.add("Linea 1 - Error de formato  en el archivo.");
			archivoValido=false;
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
		}
        //***** everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..inicio
        finally{
        	if(null != validaArchivo){
        		try{
        			validaArchivo.cerrar();
        		}catch(Exception e){
        		}
        		validaArchivo = null;
        	}
        }
        //****  everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..fin
        return archivoValido;
	}

	/**
	 * Valida lo siguiente:<br/>
	 * Que los caracteres del 1 al 18 de cada l�nea sean num�ricos.<br/>
	 * Que los caracteres del 19 al 48 de cada l�nea sean alfanum�ricos y sin espacios vac�os.<br/>
	 * Que los caracteres del 49 al 68 de cada l�nea sean alfanum�ricos y sin espacios vac�os.<br/>
	 * Que los caracteres del 69 al 98 de cada l�nea sean alfanum�ricos y sin espacios vac�os.
	 *
	 * @param archivoCuentasRegistradas
	 * @throws IOException
	 */
	private void validarLayoutArchivoCuentasRegistradas(File archivoCuentasRegistradas)
		throws IOException{
		BufferedReader in = new BufferedReader(new FileReader(archivoCuentasRegistradas));
	    String linea;
	    int lineaActual = 0;
	    while ((linea = in.readLine()) != null) {
	    	lineaActual++;
	    	String numeroCuenta = linea.substring(0,18).trim();
	    	String aPaterno = linea.substring(18,48).trim();
	    	String aMaterno = linea.substring(48,68).trim();
	    	String nombre = linea.substring(68,98).trim();

	    	//Valida que los caracteres del 1 al 18 de cada l�nea sean num�ricos.
	    	if(esNuloVacio(numeroCuenta)){
	    		errores.add("El n�mero de cuenta en la l�nea "+lineaActual+
	    				" del archivo, no debe de ser un valor vac�o.");
	    	}else if(!esLong(numeroCuenta)){
	    		errores.add("El n�mero de cuenta en la l�nea "+lineaActual+
	    				" del archivo, debe de ser num�rico.");
	    	}
			//Valida que los caracteres del 19 al 48 de cada l�nea sean alfanum�ricos y sin espacios vac�os.
	    	if(esNuloVacio(aPaterno))
	    		errores.add("El apellido paterno en la l�nea "+lineaActual+
	    				" del archivo, no debe de ser un valor vac�o.");
			//Valida que los caracteres del 49 al 68 de cada l�nea sean alfanum�ricos y sin espacios vac�os.
    		if(esNuloVacio(aMaterno))
	    		errores.add("El apellido materno en la l�nea "+lineaActual+
	    				" del archivo, no debe de ser un valor vac�o.");
			//Valida que los caracteres del 69 al 98 de cada l�nea sean alfanum�ricos y sin espacios vac�os.
    		if(esNuloVacio(nombre))
	    		errores.add("El nombre en la l�nea "+lineaActual+
	    				" del archivo, no debe de ser un valor vac�o.");
	    }
	    in.close();
	}

	/**
	 * Determina si una cadena es nula o vac�a.
	 * @param cadena
	 * @return
	 */
	private boolean esNuloVacio(String cadena){
		return (cadena == null || cadena.equals("") ||
				cadena.trim().equals(""));
	}

	/**
	 * Determina si el contenido de una cadena es de
	 * tipo Long.
	 * @param cadena
	 * @return
	 */
	private boolean esLong(String cadena){
		try{
			Long.parseLong(cadena.trim());
		}catch(NumberFormatException e){
			return false;
		}
		return true;
	}

	/**
	 * Extrae los datos de un archivo de carga de n&oacute;mina
	 * y forma una lista de objetos de tipo DetalleCuentasBean.
	 *
	 * @param archivoNomina
	 * @return
	 * @throws IOException
	 */
	private List<DetalleCuentasBean> extraerCuentasArchivoNomina(File archivoNomina) throws IOException{
		int LINEAS_EXCLUYENTES_FIN_ARCHIVO = 1;
		int PRIMERA_LINEA_CONTENIDO = 2;
		int lineasArchivo = getNumberOfLines(archivoNomina);
		int ultimaLineaContenido = lineasArchivo-LINEAS_EXCLUYENTES_FIN_ARCHIVO;
		List<DetalleCuentasBean> cuentas = new ArrayList<DetalleCuentasBean>(0);
		BufferedReader in = new BufferedReader(new FileReader(archivoNomina));
	    String linea;
	    int lineaActual = 0;

	    while ((linea = in.readLine()) != null) {
	    	lineaActual++;
	    	if((lineaActual >= PRIMERA_LINEA_CONTENIDO) &&
	    		(lineaActual <= ultimaLineaContenido)){
	    		DetalleCuentasBean detalleCuenta = new DetalleCuentasBean();
	    		String numeroCuenta = linea.substring(93,109).trim();
	    		String aPaterno = linea.substring(13,43).trim();
	    		String aMaterno = linea.substring(43,63).trim();
	    		String nombre = linea.substring(63,93).trim();

		    	detalleCuenta.setCuenta(numeroCuenta);
		    	detalleCuenta.setaMaterno(aMaterno);
		    	detalleCuenta.setaPaterno(aPaterno);
		    	detalleCuenta.setNombre(nombre);

		    	if(!cuentas.contains(detalleCuenta)){
		    		cuentas.add(detalleCuenta);
	    		}
	    	}
	    }
	    in.close();
		return cuentas;
	}

	/**
	 * Extrae los datos de un archivo de carga de cuentas
	 * registradas en el cat&aacute;logo de n&oacute;mina
	 * y forma una lista de objetos de tipo DetalleCuentasBean.
	 *
	 * @param archivoCuentasRegistradas
	 * @return
	 * @throws IOException
	 */
	private List<DetalleCuentasBean> extraerCuentasArchivoCuentasRegistradas(
			File archivoCuentasRegistradas) throws IOException{
		List<DetalleCuentasBean> cuentas = new ArrayList<DetalleCuentasBean>();
		BufferedReader in = new BufferedReader(new FileReader(archivoCuentasRegistradas));
		String linea;

		while ((linea = in.readLine()) != null) {
			DetalleCuentasBean detalleCuenta = new DetalleCuentasBean();
			String numeroCuenta = linea.substring(0,18).trim();
	    	String aPaterno = linea.substring(18,48).trim();
	    	String aMaterno = linea.substring(48,68).trim();
	    	String nombre = linea.substring(68,98).trim();

	    	detalleCuenta.setCuenta(numeroCuenta);
	    	detalleCuenta.setaMaterno(aMaterno);
	    	detalleCuenta.setaPaterno(aPaterno);
	    	detalleCuenta.setNombre(nombre);

	    	if(!cuentas.contains(detalleCuenta)){
	    		cuentas.add(detalleCuenta);
	    	}
		}

		return cuentas;
	}

	/**
	 * Invoca a la funci&oacute;n validaCuenta de la clase
	 * MAC_Registro para validar cada una de las cuentas.
	 * @param cuentas
	 * @return Lista de errores encontrados en la validaci&oacute;n
	 */
	@SuppressWarnings("unchecked")
	private void validarCuentas(){
		String TIPO_CUENTA_CLABE = "40";
		int TAM_CUENTA_CLABE = 18;

		for(DetalleCuentasBean detalleCuenta : cuentas){
			MAC_Registro validadorCuenta = new MAC_Registro();
			validadorCuenta.inicializa();
			validadorCuenta.setCuenta(detalleCuenta.getCuenta());

			if(detalleCuenta.getCuenta().length() == TAM_CUENTA_CLABE)
				validadorCuenta.setTipoCuenta(TIPO_CUENTA_CLABE);

			validadorCuenta.validaCuenta();
			EIGlobal.mensajePorTrace("***DetalleCuentasValidador.class &Para la cuenta "+detalleCuenta.getCuenta()+" tamano de cuenta a validar: "+detalleCuenta.getCuenta().length(), EIGlobal.NivelLog.DEBUG);
			errores.addAll(validadorCuenta.listaErrores);
		}
	}

	public Vector<String> getErrores(){
		return this.errores;
	}

}