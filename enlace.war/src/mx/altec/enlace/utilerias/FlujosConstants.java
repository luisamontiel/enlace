package mx.altec.enlace.utilerias;

public class FlujosConstants {
	
	/**
	 * transInterbancarias ID del flujo Transferencias Interbancarias 
	 */
	public static String transInterbancarias="DIBT";
	/**
	 * pagoProveedores ID del flujo Confirming Pago a Proveedores
	 */
	public static String pagoProveedores="ACPP";
	/**
	 * pagoMicrositioRef ID del flujo Pago Referenciado por micrositio
	 */
	public static String pagoMicrositioRef="PMRF";
	/**
	 * pagoImpLC ID del flujo Pago Referenciado Enlace
	 */
	public static String pagoImpLC="PILC";
	
}
