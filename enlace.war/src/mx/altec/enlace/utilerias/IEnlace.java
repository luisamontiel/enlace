/** 
*   Isban M�xico
*   Clase: IEnlace.java
*   Descripci�n: Servlet que maneja todo el flujo de Baja de Cuentas.
*   
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Se agregan constantes Fiel-OTP 
*/
package mx.altec.enlace.utilerias;

// Common definitions used throughout the Enlace System
public interface IEnlace {

    // Constantes para definir el id de los modulos de Enlace.
    // Estos id's se utilizan para determinar si el modulo en cuestion se
    // encuentra activo y que este pueda ser mostrado al cliente.
    /**
     * Atributo estatico y constante para definir el id del menu Modificar envio
     * de Estado de Cuenta > Individual
     */
    public static final int MOD_EDO_CTA_IND = 166;

    /**
     * Atributo estatico y constante para definir el id del menu Modificar envio
     * de Estado de Cuenta > Masivo
     */
    public static final int MOD_EDO_CTA_MASIV = 168;

    /**
     * Atributo estatico y constante para definir el id del menu Modificar envio
     * de Estado de Cuenta > Consulta
     */
    public static final int MOD_EDO_CTA_CONS = 169;

    /**
     * Atributo estatico y constante para definir el id del menu Descarga de
     * Estado de Cuenta > PDF
     */
    public static final int DESC_EDO_CTA_PDF = 171;

    /**
     * Atributo estatico y constante para definir el id del menu Descarga de
     * Estado de Cuenta > XML
     */
    public static final int DESC_EDO_CTA_XML = 172;

    /**
     * Atributo estatico y constante para definir el id del menu Periodos
     * Historicos Formato PDF > Solicitud
     */
    public static final int SOL_PER_HIST_PDF = 170;

    /**
     * Atributo estatico y constante para definir el id del menu Periodos
     * Historicos Formato PDF > Consulta
     */
    public static final int CONS_PER_HIST_PDF = 167;
    
    /** Identificador de transaccion para validacion de token en Registro Fiel. */
    public static final int FIEL_OTP_REGISTRO = 23;
    /** Identificador de transaccion para validacion de token en Modifcacion Fiel. */
    public static final int FIEL_OTP_MODIFICA = 24;
    /** Identificador de transaccion para validacion de token en Baja Fiel. */
    public static final int FIEL_OTP_BAJA = 25;
    /**Constante para la cadena <script> */
    public static final String CADENA_INICIAL_SCRIPT = "<script>";
    /**Constante para la cadena </script> */
    public static final String CADENA_FINAL_SCRIPT = "</script>";
    /**Constante para la cadena "<" */
    public static final String ABRIR_TAG = "<";
    /**Constante para la cadena ">"*/
    public static final String CERRAR_TAG = ">";
    //Trace facility constants
    /** Atributo string para guardar cadena. */
    public String TRACE_FLAG_STATE_CHILD_NAME  = "TraceFlags";
    /** Atributo string para guardar cadena. */
    public String TRACE_FLAG_NAME              = "TraceLevel";

    //Application Related Constants
    /** Atributo string para guardar cadena con el nombre de la aplicacion. */
    public String APPNAME                      = "EnlaceMig";
    /** Atributo string para guardar cadena indicando el valor de enabled. */
    public String ENABLED                      = "1";
    /** Atributo string para guardar cadena indicando el valor de disabled. */
    public String DISABLED                     = "0";
    /** Atributo string para guardar cadena para contrato invalido. */
    public String INVALID_CONTRACT             = "invalid";

    /** Atributo entero para guardar valor indicando el tiempo de la sesion. */
    public int    SESSION_TIMEOUT              = 500;

    /** Atributo string para guardar cadena indicando ruta de archivo de configuracion. */
    public String CONFIG_FILE                  = "/enlace.cfg";

    /** Atributo string para guardar cadena indicando la ip del web server. */
    public String WEB_SERVER_IP                = "148.248.50.4";
    /** Atributo string para guardar cadena indicando el usuario del web server. */
    public String WEB_SERVER_USER              = "edgaflor";//"anonymous";
    /** Atributo string para guardar cadena indicando la clave del usuario del web server. */
    public String WEB_SERVER_PASSWD            = "edgaflor";//"passwdienlace";
    /** Atributo string para guardar cadena indicando ruta de descarga del web server. */
    public String WEB_SERVER_DOWNLOADS         = "/netscape/ssl-docs/EnlaceInternet/Downloads/";

    /** Atributo string para guardar cadena indicando ruta de descarga. */
    public String DOWNLOAD_PATH                = Global.DOWNLOAD_PATH;

    /** Atributo string para guardar cadena indicando ruta NAS. */
    public String FTP_PATH                     = "/nas/suite36/docs/Downloads/";
    /** Atributo string para guardar cadena indicando directorio local de donde se leen los archivos de salida para algunos servicios de tuxedo. */
    public String LOCAL_TMP_DIR                = Global.DIRECTORIO_LOCAL;// Directorio local de donde se leen los archivos de salida para algunos servicios de tuxedo
    /** Atributo string para guardar cadena indicando directorio de salida para algunos servicios de tuxedo. */
    public String REMOTO_TMP_DIR               = Global.DIRECTORIO_LOCAL;  // Directorio de salida para algunos servicios de tuxedo
    /** Atributo string para guardar cadena indicando la extensi�n del archivo de ambiente. */
    public String ARCHIVO_AMBIENTE             = ".amb";
    /** Atributo string para guardar cadena indicando la version, la cual es utilizada para consumir algunos servicios tuxedo. */
    public String VERSION                      = "2.2";
    /** Atributo string para guardar cadena indicando mensaje de pagina no disponible. */
    public String MSG_PAG_NO_DISP              = "La p&aacute;gina que solicito no esta disponible en este momento, por favor intente m&aacute;s tarde";
    /** Atributo string para guardar cadena indicando mensaje de datso invalidos. */
    public String MSG_DATOS_INVALIDOS          = "Datos inv&aacute;lidos, vuelva a intentar";
    /** Atributo string para guardar cadena con identificador. */
    public String PERSONAL_ACCT                = "60";
    /** Atributo string para guardar cadena con identificador. */
    public String PERSONAL_INV_ACCT            = "61";
    /** Atributo string para guardar cadena con identificador. */
    public String ENTERPRISE_INV_ACCT          = "66";

    /** Atributo string para guardar cadena indicando clave de operacion de login. */
    public String LOGIN_OPERATION              = "LGIN";
    /** Atributo string para guardar cadena indicando clave de operacion exitosa de login. */
    public String LOGIN_OK_STATUS_CODE         = "LGIN0000";

    /** Atributo string para guardar cadena indicando clave de operacion de balance_inq. */
    public String BALANCE_INQ_OPERATION        = "SALD";
    /** Atributo string para guardar cadena indicando clave de operacion exitosa de balance_inq. */
    public String BALANCE_INQ_OK_STATUS_CODE   = "SALD0000";
    /** Atributo string para guardar cadena indicando clave de operacion con cta inexistente para balance_inq. */
    public String BALANCE_INQ_CTA_NO_EXISTE    = "SALD9010";

    /** Atributo string para guardar cadena indicando clave de operacion de posicion_inq. */
    public String POSICION_INQ_OPERATION       = "POSI";
    /** Atributo string para guardar cadena indicando clave de operacion exitosa de posicion_inq. */
    public String POSICION_INQ_OK_STATUS_CODE  = "POSI0000";

    /** Atributo string para guardar cadena indicando clave de operacion de activity_inq. */
    public String ACTIVITY_INQ_OPERATION       = "MOVS";
    /** Atributo string para guardar cadena indicando clave de operacion exitosa de activity_inq. */
    public String ACTIVITY_INQ_OK_STATUS_CODE  = "MOVI0000";

    /** Atributo string para guardar cadena indicando clave de sucursal operante. */
    public String SUCURSAL_OPERANTE_ENL        = "787";
    /** Atributo string para guardar cadena indicando clave de sucursal operante enlace. */
    public String SUCURSAL_OPERANTE            = "787";
    /** Atributo string para guardar cadena indicando acronimo de la aplicacion. */
    public String ACRONIMO_APLICAION           = "ENLI";
    /** Atributo string para guardar cadena indicando clave de terminal aplicacion. */
    public String TERMINAL_APLICACION          = "TUTC";
    /** Atributo string para guardar cadena indicando clave de contrato BIA. */
    public String CONTRATO_BIA                 = "BIA";

    /** Atributo string para guardar cadena indicando clave de operacion exitosa para segsrvr. */
    public String SEGSERVER_OK_CODE            = "SEGU0000";
    /** Atributo string para guardar cadena indicando clave de operacion cuando no existe perfil asociado (segsrvr). */
    public String NO_EXISTE_PERFIL_ASOCIADO    = "SEGU0105";
    /** Atributo string para guardar cadena indicando clave de perfil omision contrato bia. */
    public String PERFIL_OMISION_CONTR_BIA     = "0002548";

    /** Atributo string para guardar cadena indicando clave de operacion transfer. */
    public String TRANSFER_OPERATION           = "TRAN";
    /** Atributo string para guardar cadena indicando clave de operacion exitosa transfer. */
    public String TRANSFER_OK_CODE             = "TUBO0000";
    /** Atributo string para guardar cadena indicando clave de operacion de error generico transfer. */
    public String TRANSFER_GENERIC_ERROR       = "TUBO9999";
    /** Atributo string para guardar cadena indicando clave de clacon cargo. */
    public String TRANSFER_CLACON_CARGO        = "681";
    /** Atributo string para guardar cadena indicando clave de clacon abono. */
    public String TRANSFER_CLACON_ABONO        = "180";
    /** Atributo string para guardar cadena indicando concepto default transfer. */
    public String TRANSFER_CONCEPTO_DEFAULT    = "TRANSFERENCIA EFECTIVO";
    /**Holds the value of property INVESTMENT_OK_CODE   */
    public String INVESTMENT_OK_CODE           = "TUBO0000";
    /**Holds the value of property INVESTMENT_GENERIC_ERROR */
    public String INVESTMENT_GENERIC_ERROR     = "TUBO9999";
    /**Holds the value of property DISP_A_INVERTIDO */
    public String DISP_A_INVERTIDO             = "DI";
    /**Holds the value of property DISP_A_INVERTIDO_OPERATION   */
    public String DISP_A_INVERTIDO_OPERATION   = "CPDI";
    /**Holds the value of property INVERTIDO_A_DISP_OPERATION   */
    public String INVERTIDO_A_DISP_OPERATION   = "CPID";
    /**Holds the value of property INVESTMENT_CLACON_DI_CARGO   */
    public String INVESTMENT_CLACON_DI_CARGO   = "742";
    /**Holds the value of property INVESTMENT_CLACON_DI_ABONO   */
    public String INVESTMENT_CLACON_DI_ABONO   = "251";
    /**Holds the value of property INVESTMENT_CLACON_ID_CARGO   */
    public String INVESTMENT_CLACON_ID_CARGO   = "750";
    /**Holds the value of property INVESTMENT_CLACON_ID_ABONO   */
    public String INVESTMENT_CLACON_ID_ABONO   = "247";

    /**Holds the value of property OFFLINE_MODE_ENABLED */
    public String OFFLINE_MODE_ENABLED         = "Y";
    // Caching related constatns
    /**Holds the value of property CACHE_TIMEOUT    */
    public int     CACHE_TIMEOUT       = SESSION_TIMEOUT * 2;

    /**Holds the value of property PICKUP_FROM_CACHE    */
    public String  PICKUP_FROM_CACHE   = "0";
    /**Holds the value of property EXECUTE_REPORT   */
    public String  EXECUTE_REPORT      = "1";

    /**Holds the value of property CONTRACT_CHANGED */
    public String  CONTRACT_CHANGED    = "Y";
    /**Holds the value of property CONTRACT_UNCHANGED   */
    public String  CONTRACT_UNCHANGED  = "N";

    //Facultades de usuario por perfil

    /**Holds the value of property VERDADERO    */
    public String VERDADERO                    ="true";
    /**Holds the value of property FALSO    */
    public String FALSO                        ="false";

    ////////////////////////////Medios de entrega////////////////////////////////
    /**Holds the value of property medioEntrega1    */
    public String   medioEntrega1               =   "1EWEB";
    /**Holds the value of property medioEntrega2    */
    public String   medioEntrega2               =   "2EWEB";

//////////////////////////Facultades de Banca Internacional//////////////////////
    /**Holds the value of property FAC_ABON_INT_NREG    */
    public String  FAC_ABON_INT_NREG         = "ABOCTAINTERNREG";

    // Queries
    /**Holds the value of property CONTRACT_QUERY   */
    public String CONTRACT_QUERY = "Enlace/Queries/contratos.gxq";

    //HTML Templates
    ////////////////////////////////////////////////////////////
    /**Holds the value of property CARGATEMPLATE_TMPL   */
    public String CARGATEMPLATE_TMPL     = "/jsp/CargaPagina.jsp";
    /**Holds the value of property CONTRATOS_TMPL   */
    public String CONTRATOS_TMPL         = "/jsp/salida2.jsp";
    /**Holds the value of property ERROR_TMPL   */
    public String ERROR_TMPL             = "/jsp/error.jsp";
    /**Holds the value of property INDEX    */
    public String INDEX                  = "/index.jsp";
    /**Holds the value of property LOGIN    */
    public String LOGIN                  = "/jsp/login.jsp";
    /**Holds the value of property LOGINAN  */
    public String LOGINAN                = Global.REDIR_SAM_LOGIN_URL;
    /**Holds the value of property STATUS   */
    public String STATUS                 = Global.REDIR_SAM_LOGIN_STATUS;
    /*<VC proyecto="200710001" autor="AHO" fecha="29/04/2007" descripci�n="DESBLOQUEO DE CONTRASE�A">*/
    /**Holds the value of property LOGINDESBLOQ */
    public String LOGINDESBLOQ           = "/jsp/Desbloq14.jsp";
    /**Holds the value of property CONFIRMADESBLOQ  */
    public String CONFIRMADESBLOQ        = "/jsp/ConfirmaDesbloq.jsp";
    /**Holds the value of property CONSULTA */
    public String CONSULTA               = "/jsp/redirLoginS2.jsp";
    /*VC*/
    /*<VC proyecto="200710001" autor="AHO" fecha="29/04/2007" descripci�n="DESBLOQUEO DE CONTRASE�A">*/
    /**Holds the value of property ACTUALIZA_TOKEN2 */
    public String ACTUALIZA_TOKEN2  = "/jsp/actToken2.jsp";
    /**Holds the value of property MENSAJE_ACTUALIZA_TOKEN2 */
    public String MENSAJE_ACTUALIZA_TOKEN2  = "/jsp/actTokenMsg2.jsp";

    /*VC*/

    /*<VC proyecto="200710001" autor="SCG" fecha="29/04/2007" descripci�n="Activacion de contrase�a">*/
    /**Holds the value of property ACTIVAR_NIP_FRM  */
    public String ACTIVAR_NIP_FRM        = "/jsp/activarContrasenaFrm.jsp";
    /**Holds the value of property ACTIVAR_NIP  */
    public String ACTIVAR_NIP            = "/jsp/activarContrasena.jsp";
    /*</VC>*/
    /**Holds the value of property CERRAR   */
    public String CERRAR                 = "/jsp/cerrar.jsp";
    /**Holds the value of property TOKEN    */
    public String TOKEN                  = "/jsp/token.jsp";
    //<VC proyecto=�200710001� autor=�RRG� fecha=�28/04/2007� descripci�n=�AGREGAR CONSTANTE�>
    /**Holds the value of property TOKENMAC */
    public String TOKENMAC               = "/jsp/tokenMac.jsp";
    //</VC>
    //  <VC proyecto=�200710001� autor=�ESC� fecha=�03/05/2007� descripci�n=�AGREGAR GENERAR�>
    /**Holds the value of property GENERAR_NIP  */
    public String GENERAR_NIP               = "/jsp/generarContrasenaFrm.jsp";
    /**Holds the value of property DATOS_SOL_NIP    */
    public String DATOS_SOL_NIP             = "/jsp/datosSolicitudFrm.jsp";
    /**Holds the value of property IMPRIMIR_SOL_NIP */
    public String IMPRIMIR_SOL_NIP          = "/jsp/imprimirSolicitud.jsp";
    //</VC>
    /**pagina token solo para enrolamiento stefanini**/
    public String VALIDA_OTP_ENROLAMIENTO   = "/jsp/tokenEnrolamiento.jsp";
    /**pagina validaOTP**/
    public String VALIDA_OTP             = "/jsp/ValidaOTP.jsp";
    /**pagina ValidaOTPConfirm**/
    public String VALIDA_OTP_CONFIRM     = "/jsp/ValidaOTPConfirm.jsp";
    /** The hostauth. */
    public String HOSTAUTH               = "/jsp/hostAuth.jsp";
    /** The token micrositio. */
    public String TOKEN_MICROSITIO       = "/jsp/tokenMicrositio.jsp";
    /** The hostauth micrositio. */
    public String HOSTAUTH_MICROSITIO    = "/jsp/hostAuthMicrositio.jsp";
    /** The principal. */
    public String PRINCIPAL              = "/jsp/principal.jsp";
    /** The cambio passwd. */
    public String CAMBIO_PASSWD          = "/jsp/CambioPassword.jsp";
    /** The posicion tmpl. */
    public String POSICION_TMPL          = "/jsp/posicion_tmpl1.jsp";
    /** The posicion. */
    public String POSICION               = "/jsp/posicion.jsp";
    /** The saldo tmpl. */
    public String SALDO_TMPL             = "/jsp/Saldo.jsp";
    /** The saldos consolidados tmpl. */
    public String SALDOS_CONSOLIDADOS_TMPL = "/jsp/SaldosConsolidados.jsp";
    /** The c saldo tmpl. */
    public String C_SALDO_TMPL           = "/jsp/c_saldo.jsp";
    /** The C_ sald o_ tmp l1. */
    public String C_SALDO_TMPL1          = "/jsp/c_saldo1.jsp";
    /** The c alta con. */
    public String C_ALTA_CON             = "/jsp/alta_consulta.jsp";
    /** The c cons prog. */
    public String C_CONS_PROG            = "/jsp/consulta_prog.jsp";
    /** The c prog vig. */
    public String C_PROG_VIG             = "/jsp/alta_prog.jsp";
    /** The c modi prog. */
    public String C_MODI_PROG            = "/jsp/c_con_modi.jsp";
    /** The c modi err. */
    public String C_MODI_ERR             = "/jsp/c_con_midie.jsp";
    /** The c con resu. */
    public String C_CON_RESU             = "/jsp/c_con_resu.jsp";
    /** The c con rerr. */
    public String C_CON_RERR             = "/jsp/c_con_rerr.jsp";
    /** The altactas tmpl. */
    public String ALTACTAS_TMPL          = "/jsp/alta.jsp";
    /** The c bitacora tmpl. */
    public String C_BITACORA_TMPL        = "/jsp/CBitacora.jsp";
    /** The c movimiento tmpl. */
    public String C_MOVIMIENTO_TMPL      = "/jsp/CMovimiento.jsp";
    /** The movimientos tmpl. */
    public String MOVIMIENTOS_TMPL       = "/jsp/movimientos.jsp";
    /** The bitacora tmpl. */
    public String BITACORA_TMPL          = "/jsp/Bitacora.jsp";
    /** The oper manc tmpl. */
    public String OPER_MANC_TMPL         = "/jsp/GetAMancomunidad.jsp";
    /** The res manc tmpl. */
    public String RES_MANC_TMPL          = "/jsp/ResMancomunidad.jsp";
    /** The mis trans tmpl. */
    public String MIS_TRANS_TMPL         = "/jsp/mis_transferencias.jsp";
    /** The mis trans tarjeta tmpl. */
    public String MIS_TRANS_TARJETA_TMPL = "/jsp/mis_transferencias_tarjeta.jsp";
    /** The mis trans arch tmpl. */
    public String MIS_TRANS_ARCH_TMPL    = "/jsp/mis_transferencias_archivo.jsp";
    /** The nom emp suc tmpl. */
    public String NOM_EMP_SUC_TMPL     = "/jsp/Nom_suc_datos.jsp";
    /** The nom emp cnx tmpl. */
    public String NOM_EMP_CNX_TMPL     = "/jsp/conexion.jsp";
    /** The nom mtto csl tmpl. */
    public String NOM_MTTO_CSL_TMPL     = "/jsp/altas_masivas_cslta.jsp";
    //<VC proyecto=�200710001� autor=�RRG� fecha=�28/04/2007� descripci�n=�AGREGAR CONSTANTE�>
    /** The admin contrasena. */
    public String ADMIN_CONTRASENA  = "/jsp/consEstatusContrasena.jsp";
    //</VC>
    //<VC proyecto=�200710001� autor=�JGR� fecha=�28/04/2007� descripci�n=�AGREGAR CONSTANTE�>
    /** The consulta contrasena. */
    public String CONSULTA_CONTRASENA = "/jsp/consEstatusContrasena.jsp";
    //</VC>
    //  Micrositio Enlace
    /** The cargatemplate tmpl micrositio. */
    public String CARGATEMPLATE_TMPL_MICROSITIO  = "/jsp/CargaPagina.jsp";
    /** The contratos tmpl micrositio. */
    public String CONTRATOS_TMPL_MICROSITIO  = "/jsp/salidaMicrositio.jsp";
    /** The error tmpl micrositio. */
    public String ERROR_TMPL_MICROSITIO  = "/jsp/errorMicrositio.jsp";
    /** The principal micrositio. */
    public String PRINCIPAL_MICROSITIO  = "/jsp/principal.jsp";
    /** The login micrositio. */
    public String LOGIN_MICROSITIO  = "/jsp/loginMicrositio.jsp";
    /** The confirma tmpl micrositio. */
    public String CONFIRMA_TMPL_MICROSITIO  = "/jsp/confirmarPagoMicrositio.jsp";
    /** The comprobante tmpl micrositio. */
    public String COMPROBANTE_TMPL_MICROSITIO  = "/jsp/comprobanteMicrositio.jsp";
    /** The operacion tmpl micrositio ok. */
    public String OPERACION_TMPL_MICROSITIO_OK  = "/jsp/operacionPagoMicrositioOK.jsp";
    /** The cuentacargo login micrositio. */
    public String CUENTACARGO_LOGIN_MICROSITIO  = "/jsp/cuentaCargoMicrositio.jsp";
    /** The mi oper tmpl. */
    public String MI_OPER_TMPL           = "/jsp/mi_operacion_realizada.jsp";
    /** The mis opervista tmpl. */
    public String MIS_OPERVISTA_TMPL     = "/jsp/mis_operaciones_vista.jsp";
    /** The mi comprobante tmpl. */
    public String MI_COMPROBANTE_TMPL    = "/jsp/mi_comprobante_transferencia.jsp";
    /** The mi oper prog tmpl. */
    public String MI_OPER_PROG_TMPL      = "/jsp/mis_operaciones_programadas.jsp";
    /** The mi arch datos. */
    public String MI_ARCH_DATOS          = "/jsp/mi_archivo_datos.jsp";
    /** The mis operplazo tmpl. */
    public String MIS_OPERPLAZO_TMPL     = "/jsp/mis_operaciones_plazo.jsp";
    /** The MI s_ operin v728_ tmpl. */
    public String MIS_OPERINV728_TMPL    = "/jsp/mis_operaciones_inversion7_28.jsp";
    /** The enlace pendiente. */
    public String ENLACE_PENDIENTE       = "/jsp/pendiente.jsp";
    /** The cambio instr tmpl. */
    public String CAMBIO_INSTR_TMPL      = "/jsp/Cambio_instrumento.jsp";
    /** The camancomunidad tmpl. */
    public String CAMANCOMUNIDAD_TMPL    = "/jsp/CAMancomunidad.jsp";
    /** The camancomunidad sa tmpl. */
    public String CAMANCOMUNIDAD_SA_TMPL = "/jsp/CAMancomunidadSA.jsp";
    /** The manc especial tmpl. */
    public String MANC_ESPECIAL_TMPL     = "/jsp/MancEspecial.jsp";
    /** The getmanc especial tmpl. */
    public String GETMANC_ESPECIAL_TMPL  = "/jsp/GetAMancEspecial.jsp";
    /** The sua alta tmpl. */
    public String SUA_ALTA_TMPL          = "/jsp/SuaAlta.jsp";
    /** The consulta usuarios tmpl. */
    public String CONSULTA_USUARIOS_TMPL = "/jsp/UsuariosContrato.jsp";
    /** The nom mtto emp tmpl. */
    public String NOM_MTTO_EMP_TMPL      = "/jsp/Nom_mtto_emp.jsp";
    /** The nom emp datos tmpl. */
    public String NOM_EMP_DATOS_TMPL     = "/jsp/Nom_emp_datos.jsp";
    /** The nom emp rep tmpl. */
    public String NOM_EMP_REP_TMPL       = "/jsp/Nom_emp_reporte.jsp";
    /** The pioper real tmpl. */
    public String PIOPER_REAL_TMPL       = "/jsp/pi_operacion_realizada.jsp";
    /** The pialta ctas tmpl. */
    public String PIALTA_CTAS_TMPL       = "/jsp/PMAltaCuentasPI.jsp";
    /** The picons ctas tmpl. */
    public String PICONS_CTAS_TMPL       = "/jsp/PMConsultCuentasPI.jsp";
    /** The pipago tmpl. */
    public String PIPAGO_TMPL            = "/jsp/PMPagoPI.jsp";
    /** The picomp tmpl. */
    public String PICOMP_TMPL            = "/jsp/mi_comprobante_pi.jsp";
    /** The picons pago tmpl. */
    public String PICONS_PAGO_TMPL       = "/jsp/PMConsultPagosPI.jsp";
    /** The pipago cons tmpl. */
    public String PIPAGO_CONS_TMPL       = "/jsp/PMPagosConsultadosPI.jsp";
    /** The pipago expt tmpl. */
    public String PIPAGO_EXPT_TMPL       = "/jsp/PMPagosExportadosPI.jsp";
    /** The mto nomina tmpl. */
    public String MTO_NOMINA_TMPL        = "/jsp/MantenimientoNomina.jsp";
    /**
     * MantenimientoNominaLn: mantenimiento de nomina en linea
     */
    public String MTO_NOMINA_TMPLN       = "/jsp/MantenimientoNominaLn.jsp";
    /**
     * NominaTranenProcesoLn: mensaje de nomina en linea
     */
    public String MTO_TRANPROC_TMPLN     = "/jsp/NominaTranenProcesoLn.jsp";
    /** The mto nominainter tmpl. */
    public String MTO_NOMINAINTER_TMPL   = "/jsp/MantenimientoNominaInter.jsp";
    /** The reporte nomina tmpl. */
    public String REPORTE_NOMINA_TMPL    = "/jsp/NominaReporte.jsp";
    /**
     * NominaReporteLn: Reporte de nomina en linea
     */
    public String REPORTE_NOMINALN_TMPL  = "/jsp/NominaReporteLn.jsp";
    /** The err nomina tmpl. */
    public String ERR_NOMINA_TMPL        = "/jsp/NomErrores.jsp";
    /** The nominadatos tmpl. */
    public String NOMINADATOS_TMPL       = "/jsp/NominaDatos.jsp";
    /**
     * NominaDatosLn: importacion de nomina en linea
     */
    public String NOMINADATOSLN_TMPL     = "/jsp/NominaDatosLn.jsp";
    /**
     * ResultadoNominaLn: resultado consulta nomina en linea
     */
    public String NOMINALNRESULT_TMPL    = "/jsp/ResultadoNominaLn.jsp";
    /** The nominadatosinter tmpl. */
    public String NOMINADATOSINTER_TMPL  = "/jsp/NominaDatosInter.jsp";
    /** The tipocambiodolar tmpl. */
    public String TIPOCAMBIODOLAR_TMPL   = "/jsp/TipoCambioDolar.jsp";
    /** The tipocambiootras tmpl. */
    public String TIPOCAMBIOOTRAS_TMPL   = "/jsp/TipoCambioOtras.jsp";
    /** The TASA s1_ tmpl. */
    public String TASAS1_TMPL            = "/jsp/Tasas1.jsp";
    /** The TASA s2_ tmpl. */
    public String TASAS2_TMPL            = "/jsp/Tasas2.jsp";
    /** The TASA s3_ tmpl. */
    public String TASAS3_TMPL            = "/jsp/Tasas3.jsp";
    /** The ticonmovimientos tmpl. */
    public String TICONMOVIMIENTOS_TMPL  = "/jsp/TIConMovimientos.jsp";
    /** The tiexpmovimientos tmpl. */
    public String TIEXPMOVIMIENTOS_TMPL  = "/jsp/TIExpMovimientos.jsp";
    /** The ticonestructura tmpl. */
    public String TICONESTRUCTURA_TMPL   = "/jsp/TIConEstructura.jsp";
    /** The ticonsaldos tmpl. */
    public String TICONSALDOS_TMPL       = "/jsp/TIConSaldos.jsp";
    /** The expcatamb tmpl. */
    public String EXPCATAMB_TMPL         = "/jsp/Expcatamb.jsp";
    /** The EXPCATAM b1_ tmpl. */
    public String EXPCATAMB1_TMPL        = "/jsp/Expcatamb1.jsp";
    /** The ctas otros bancos tmpl. */
    public String CTAS_OTROS_BANCOS_TMPL = "/jsp/CtasOtrosBancos.jsp";
    /** The tjt nomina tmpl. */
    public String TJT_NOMINA_TMPL        = "/jsp/prepago/NomPredispersion.jsp";
    /** The tjt nominadatos tml. */
    public String TJT_NOMINADATOS_TML    = "/jsp/prepago/NomPreConsultadispersion.jsp";
    /** The tjt nominadatosdet tml. */
    public String TJT_NOMINADATOSDET_TML = "/jsp/prepago/NomPreConsultadispersionDetalle.jsp";
    /** The tjt nomindividual tml. */
    public String TJT_NOMINDIVIDUAL_TML  = "/jsp/prepago/NomPredispersionIndividual.jsp";

    /** The control pagos. */
    public String CONTROL_PAGOS          = "/jsp/principalcoPagos.jsp";
    /** The alta pagos. */
    public String ALTA_PAGOS             = "/jsp/AltaDePagos.jsp";
    /** The plazos dolar tmpl. */
    public String PLAZOS_DOLAR_TMPL       = "/jsp/Plazosdolar.jsp";
    /** The cambios inicial tmpl. */
    public String CAMBIOS_INICIAL_TMPL    = "/jsp/CambiosInicial.jsp";
    /** The cambios datos tmpl. */
    public String CAMBIOS_DATOS_TMPL      = "/jsp/CambiosDatos.jsp";
    /** The cambios datos arch tmpl. */
    public String CAMBIOS_DATOS_ARCH_TMPL = "/jsp/CambiosDatosARCH.jsp";
    /** The cambios cotiz tmpl. */
    public String CAMBIOS_COTIZ_TMPL        = "/jsp/CambiosCotiz.jsp";
    /** The cambios salida tmpl. */
    public String CAMBIOS_SALIDA_TMPL       = "/jsp/CambiosSalida.jsp";
    /** The cambios comprob tmpl. */
    public String CAMBIOS_COMPROB_TMPL  = "/jsp/CambiosComprob.jsp";
    /** The cambios archivo tmpl. */
    public String CAMBIOS_ARCHIVO_TMPL  = "/jsp/CambiosArchivo.jsp";

    // Ruta de descarga de archivos de Nomina
    /** The archivos nomina. */
    public String ARCHIVOS_NOMINA = "EnlaceInternet/archivosNomina/";
    /** The arch empleados nomina. */
    public String ARCH_EMPLEADOS_NOMINA = "archivosNomina/empleados/";
    /** The arch nomina pagos. */
    public String ARCH_NOMINA_PAGOS = "archivosNomina/pagos/";

    //  plantillas para banca especializada 08/02/2001
    /** The be consultasaldo tmpl. */
    public String BE_CONSULTASALDO_TMPL     = "/jsp/BE_ConsultaSaldos.jsp";
    /** The be traesaldo tmpl. */
    public String BE_TRAESALDO_TMPL         = "/jsp/BETraeSaldos.jsp";
    /** The be consulta posicion tmpl. */
    public String BE_CONSULTA_POSICION_TMPL = "/jsp/BE_ConsultaPosicion.jsp";
    /** The be posicion tmpl. */
    public String BE_POSICION_TMPL          = "/jsp/BEPosicion.jsp";

    //plantillas para transferencias multiples
    /** The transferencias multiples tmpl. */
    public String TRANSFERENCIAS_MULTIPLES_TMPL = "/jsp/TransferenciasMultiples.jsp";

    //Confirming
    /** The consulta cancelacion. */
    public String CONSULTA_CANCELACION  = "/jsp/coCancelacion.jsp";
    /** The resultado consulta. */
    public String RESULTADO_CONSULTA    = "/jsp/chesConsultas.jsp";
    /** The resultado cancelacion. */
    public String RESULTADO_CANCELACION = "/jsp/resCancelacion.jsp";
    /** The reportes esta. */
    public String REPORTES_ESTA         = "/jsp/coEstadisticas.jsp";
    /** The res reportes. */
    public String RES_REPORTES          = "/jsp/coConsultaEsta.jsp";
    /** The consulta alta proveedor. */
    public String CONSULTA_ALTA_PROVEEDOR = "/jsp/coAltaProveedor.jsp";

    //Administracion Tokens
    /** The actualiza token. */
    public String ACTUALIZA_TOKEN   = "/jsp/actToken.jsp";
    /** The sync token. */
    public String SYNC_TOKEN    = "/jsp/syncToken.jsp";
    /** The mensaje actualiza token. */
    public String MENSAJE_ACTUALIZA_TOKEN   = "/jsp/actTokenMsg.jsp";
    /** The bloquea token. */
    public String BLOQUEA_TOKEN = "/jsp/bloqToken.jsp";
    /** The mensaje bloquea token. */
    public String MENSAJE_BLOQUEA_TOKEN = "/jsp/bloqTokenMsg.jsp";
    /** The imprime contrato. */
    public String IMPRIME_CONTRATO  = "/jsp/framesContrato.jsp";
    /** The imprime solicitud. */
    public String IMPRIME_SOLICITUD = "/jsp/framesSolicitud.jsp";
    /** The imprime solicitud bloqueo. */
    public String IMPRIME_SOLICITUD_BLOQUEO = "/jsp/framesSolicitudBloqueo.jsp";
    /** The consulta token. */
    public String CONSULTA_TOKEN    = "/jsp/consToken.jsp";
    /** The menu token. */
    public String MENU_TOKEN    = "/jsp/menuToken.jsp";
    /** The reposicion token. */
    public String REPOSICION_TOKEN  = "/jsp/repoToken.jsp";
    /** The solicitud individual. */
    public String SOLICITUD_INDIVIDUAL  = "/jsp/solToken.jsp";
    /** The solicitud contrato. */
    public String SOLICITUD_CONTRATO    = "/jsp/solTokens.jsp";
    /** The impresion documentos. */
    public String IMPRESION_DOCUMENTOS  = "/jsp/solContrToken.jsp";
    /** The sesion fuera. */
    public String SESION_FUERA  = "/jsp/sessionfuera.jsp";

    //JPH VWSF 04/06/2009 REQUERIMIENTO LIGA ENLACE - SUPERNET COMERCIOS
    /** The muestra cuentas. */
    public String MUESTRA_CUENTAS = "/jsp/MuestraCuentas.jsp";

    /** The error page liga snet. */
    public String ERROR_PAGE_LIGA_SNET = "/jsp/ErrorSnetComercios.jsp";

    /** The canal enlace. */
    public String CANAL_ENLACE = "EWEB";
    //TEMPLATES TARJETAS DE PREPAGO
    /** The nom pre consulta remesa. */
    public String NOM_PRE_CONSULTA_REMESA = "/jsp/prepago/NomPreConsultaRemesas.jsp";
    /** The nom pre consulta remesa detalle. */
    public String NOM_PRE_CONSULTA_REMESA_DETALLE = "/jsp/prepago/NomPreResultadoConsultaDetalle.jsp";
    /** The nom pre consulta remesa lista. */
    public String NOM_PRE_CONSULTA_REMESA_LISTA = "/jsp/prepago/NomPreResultadoConsulta.jsp";
    /** The nom pre confirma remesa. */
    public String NOM_PRE_CONFIRMA_REMESA= "/jsp/prepago/NomPreConfirmaRemesas.jsp";
    /** The nom pre confirma remesa resultado. */
    public String NOM_PRE_CONFIRMA_REMESA_RESULTADO= "/jsp/prepago/NomPreConfirmaRemesasResultado.jsp";
    /** The nom pre confirma detalle seleccion. */
    public String NOM_PRE_CONFIRMA_DETALLE_SELECCION= "/jsp/prepago/NomPreDetalleSeleccion.jsp";
    /** The nom pre confirma remesa detalle. */
    public String NOM_PRE_CONFIRMA_REMESA_DETALLE= "/jsp/prepago/NomPreConfirmaRemesasDetalle.jsp";
    /** The nom pre comun mensaje. */
    public String NOM_PRE_COMUN_MENSAJE = "/jsp/prepago/NomPreMensaje.jsp";
    /** The nom pre exp remesas. */
    public String NOM_PRE_EXP_REMESAS = "/jsp/prepago/NomPreExpRemesas.jsp";
    /** The nom pre exp tarjetas. */
    public String NOM_PRE_EXP_TARJETAS = "/jsp/prepago/NomPreExpTarjetas.jsp";
    /** The nom pre exp estado asignacion. */
    public String NOM_PRE_EXP_ESTADO_ASIGNACION = "/jsp/prepago/NomPreExpConsultaEstadoAsignacion.jsp";
    /** The nom pre consulta estado asignacion. */
    public String NOM_PRE_CONSULTA_ESTADO_ASIGNACION = "/jsp/prepago/NomPreConsultaEstadoAsignacion.jsp";
    /** The nom pre consulta estado asignacion detalle. */
    public String NOM_PRE_CONSULTA_ESTADO_ASIGNACION_DETALLE = "/jsp/prepago/NomPreConsultaEstadoAsignacionDetalle.jsp";
    /** The nompre cancela tarjeta. */
    public String NOMPRE_CANCELA_TARJETA="/jsp/prepago/NomPreCancelaTarjeta.jsp";
    /** The nompre cancela tarjeta confirma. */
    public String NOMPRE_CANCELA_TARJETA_CONFIRMA="/jsp/prepago/NomPreCancelaImportar.jsp";
    /** The nompre alta tarjeta. */
    public String NOMPRE_ALTA_TARJETA="";
    /** The nompre reasigna tarjeta. */
    public String NOMPRE_REASIGNA_TARJETA="/jsp/prepago/NomPreReposicionTarjeta.jsp";
    /** The nompre reasigna tarjeta tras. */
    public String NOMPRE_REASIGNA_TARJETA_TRAS="/jsp/prepago/NomPreReposicionTarjetaTraspaso.jsp";
    /** The nompre modif emp. */
    public String NOMPRE_MODIF_EMP="/jsp/prepago/NomPreModificaEmpleado.jsp";
    /** The nompre modif emp confirma. */
    public String NOMPRE_MODIF_EMP_CONFIRMA="/jsp/prepago/NomPreModificaEmpleadoImportar.jsp";
    /** The nompre busca emp. */
    public String NOMPRE_BUSCA_EMP="/jsp/prepago/NomPreBusqueda.jsp";
    /** The nompre busca emp det. */
    public String NOMPRE_BUSCA_EMP_DET="/jsp/prepago/NomPreBusquedaDetalle.jsp";
    /** The nompre busca emp exp det. */
    public String NOMPRE_BUSCA_EMP_EXP_DET= "/jsp/prepago/NomPreExpTarjEmp.jsp";
    /** The nompre num registros pagina. */
    public int NOMPRE_NUM_REGISTROS_PAGINA=50;


    /** The success. */
    public int SUCCESS = 0;
    /** The failure. */
    public int FAILURE = -1;
    //public final static String DATASOURCE  = "Enlace";
    //public final static String DATASOURCE2 = "EnlaceImp";


    //modificacion para integracion
    /** The M consulta_ saldos. */
    public String MConsulta_Saldos    = "1@";
    /** The M consulta_ saldos_ be. */
    public String MConsulta_Saldos_BE = "2@";
    /** The M consulta_ bitacora. */
    public String MConsulta_Bitacora  = "3@";
    /** The M inversiones. */
    public String MInversiones        = "4@";
    /** The M dep_ inter_ abono. */
    public String MDep_Inter_Abono    = "5@";
    /** The M mant_estruc_ ti. */
    public String MMant_estruc_TI     = "6@";
    /** The M mant_estruc_ b0. */
    public String MMant_estruc_B0     = "7@";
    /** The M envio_pagos_nomina. */
    public String MEnvio_pagos_nomina = "8@";
    /** The M envio_pagos_nom_ in. */
    public String MEnvio_pagos_nom_IN = "9@";
    /** The M alta_ctas_cheq_seg. */
    public String MAlta_ctas_cheq_seg = "10@";
    /** The M mane_ctas_cheq_seg. */
    public String MMane_ctas_cheq_seg = "11@";
    /** The M sua_envio. */
    public String MSua_envio          = "12@";
    /** The M alta_ordenes_pago. */
    public String MAlta_ordenes_pago  = "13@";
    /** The M alta_ctas_pago_imp. */
    public String MAlta_ctas_pago_imp = "14@";
    /** The M envio_pago_imp. */
    public String MEnvio_pago_imp     = "15@";
    /** The M mant_gral_ctas. */
    public String MMant_gral_ctas     = "16@";
    /** The M cargo_transf_pesos. */
    public String MCargo_transf_pesos = "17@26@";
    /** The M cargo_transf_dolar. */
    public String MCargo_transf_dolar = "18@";
    /** The M abono_transf_pesos. */
    public String MAbono_transf_pesos = "19@28@";
    /** The M abono_transf_dolar. */
    public String MAbono_transf_dolar = "20@";
    /** The M dep_inter_cargo. */
    public String MDep_inter_cargo    = "21@";
    /** The M operaciones_mancom. */
    public String MOperaciones_mancom = "22@";
    /** The M credito_en_linea. */
    public String MCredito_en_linea   = "23@";
    /** The M credito_empresarial. */
    public String MCredito_empresarial= "24@";
    /** The M cargo_trans_pes_ cb. */
    public String MCargo_trans_pes_CB = "25@";
    /** The M abono_tarjeta_cred. */
    public String MAbono_tarjeta_cred = "28@";
    /** The M plazo_tradicional. */
    public String MPlazo_tradicional  = "29@";
    /** The M plazo_728. */
    public String MPlazo_728          = "30@";
    /** The M plazo_dolares. */
    public String MPlazo_dolares      = "31@";
    /** The M vista. */
    public String MVista              = "32@";
    /** The M consulta_posicion. */
    public String MConsulta_posicion  = "33@";
    /** The M cambio_instruccion. */
    public String MCambio_instruccion = "34@";
    /** The M cargo_ t i_pes_dolar. */
    public String MCargo_TI_pes_dolar = "35@36@";
    /** The M abono_ ti. */
    public String MAbono_TI           = "37@";
    /** The M cargo_cambio_pesos. */
    public String MCargo_cambio_pesos = "38@";
    /** The M cargo_cambio_dolar. */
    public String MCargo_cambio_dolar = "39@";
    /** The M abono_cambios_pesos. */
    public String MAbono_cambios_pesos= "40@";
    /** The M abono_cambio_dolar. */
    public String MAbono_cambio_dolar = "41@";
    /** The M norma43. */
    public String MNorma43            = "42@";
    //Fueron Colocados por alguien en cuentasSerfinSantander los modulos 43 y 44
    //MDisposicion_Cargo = "43@";
    //MDisposicion_Abono = "44@";
    /** The M alta proveedor_abono. */
    public String MAltaProveedor_abono= "45@";
    //Colocado modulo para consulta de cuentas: 46@
    /** The M cons_ ctas_movil. */
    public String MCons_Ctas_movil    = "46@";
    /** The M cons_ ctas_movil. */
    public String MCons_Baja_Cta      = "47@";
    //modificacion para cuentas CLABE
    /** The registros clabe. */
    public String REGISTROS_CLABE     = "100";

//constantes para la parametrizaci�n (Sol. Token)
   /** The nomina. */
public int   NOMINA=1;
        /** The nomina interbancaria. */
        public int   NOMINA_INTERBANCARIA=2;
        /** The confirming pago proveedores. */
        public int   CONFIRMING_PAGO_PROVEEDORES=3;
        /** The trans internas. */
        public int   TRANS_INTERNAS=4;
        /** The trans interbanc. */
        public int   TRANS_INTERBANC=5;
        /** The trans internac. */
        public int   TRANS_INTERNAC=6;
        /** The pago impuestos. */
        public int   PAGO_IMPUESTOS=7;
        /** The alta cuentas. */
        public int   ALTA_CUENTAS=8;
        /** The pago dir. */
        public int   PAGO_DIR=9;
        /** The sua. */
        public int   SUA=10;
        /** The sar. */
        public int   SAR=11;
        /** The pago ocurre. */
        public int   PAGO_OCURRE=12;
        /** The mancomunidad. */
        public int   MANCOMUNIDAD=13;
        /** The captura datos. */
        public int   CAPTURA_DATOS=14;
        /** The chequera linea. */
        public int CHEQUERA_LINEA = 15;
        /** The sua lc. */
        public int SUA_LC = 16;
        /** The nomina prepago. */
        public int NOMINA_PREPAGO = 17;
        /** The adm superusr. */
        public int ADM_SUPERUSR = 18;
        /** The mto medios not. */
        public int MTO_MEDIOS_NOT = 19;
        /** The transferencias fx online. */
        public int TRANSFERENCIAS_FX_ONLINE = 20;
        /** Atributo entero para guardar valor empleado para solicitud de token de Estatdos de Cuenta PDF/XML. */
        public int DISP_EDO_CTA = 22;

        /** The mas trans mismo banco. */
        public int MAS_TRANS_MISMO_BANCO      = 1001;
        /** The mas trans interbancarias. */
        public int MAS_TRANS_INTERBANCARIAS   = 1002;
        /** The mas trans internacionales. */
        public int MAS_TRANS_INTERNACIONALES  = 1003;
        /** The pago nomina. */
        public int PAGO_NOMINA = 1004;
        /** The pago nomina pre. */
        public int PAGO_NOMINA_PRE = 1005;
        /** The pago sua. */
        public int PAGO_SUA = 1006;
        /** The pago sar. */
        public int PAGO_SAR  = 1007;
        /** The not pago impuestos. */
        public int NOT_PAGO_IMPUESTOS = 1008;
        /** The alta proveedores. */
        public int ALTA_PROVEEDORES = 1009;
        /** The pago proveedores. */
        public int PAGO_PROVEEDORES = 1010;
        /** The uni trans mismo banco. */
        public int UNI_TRANS_MISMO_BANCO = 1011;
        /** The uni trans interbancarias. */
        public int UNI_TRANS_INTERBANCARIAS = 1012;
        /** The uni trans internacionales. */
        public int UNI_TRANS_INTERNACIONALES = 1013;
        /** The pago sua linea de captura. */
        public int PAGO_SUA_LINEA_DE_CAPTURA = 1014;
        /** The manc gen folio. */
        public int MANC_GEN_FOLIO = 1015;
        /** The transf tdc disposicion. */
        public int TRANSF_TDC_DISPOSICION  = 1016;//MAAM 14/JUL/2010
        /** The transf tdc pago. */
        public int TRANSF_TDC_PAGO  = 1017;//MAAM 14/JUL/2010
        /** The pago nomina inter. */
        public int PAGO_NOMINA_INTER = 1018;
        /** The not alta cuentas. */
        public int NOT_ALTA_CUENTAS = 1019;
        /** The cpaswd. */
        public int CPASWD = 1020;
        /** The not baja cuentas. */
        public int NOT_BAJA_CUENTAS = 1021;
        /** The not asignacion tarjeta. */
        public int NOT_ASIGNACION_TARJETA = 1022;
        /** The not alta empleados. */
        public int NOT_ALTA_EMPLEADOS = 1023;
        /** The not cambio divisas. */
        public int NOT_CAMBIO_DIVISAS = 1024;
        /** The not aut canc oper manc. */
        public int NOT_AUT_CANC_OPER_MANC = 1025;
        /** The not aut canc oper manc inter. */
        public int NOT_AUT_CANC_OPER_MANC_INTER = 1026;
        /** The not modif empl mas. */
        public int NOT_MODIF_EMPL_MAS = 1027;
        /** The not baja empl mas. */
        public int NOT_BAJA_EMPL_MAS = 1028;
        /** The not credito prepago. */
        public int NOT_CREDITO_PREPAGO = 1029;
        /** The not modif empl uni. */
        public int NOT_MODIF_EMPL_UNI = 1030;
        /** The not baja empl uni. */
        public int NOT_BAJA_EMPL_UNI = 1031;
        /** The not alta cuentas contctas al. */
        public int NOT_ALTA_CUENTAS_CONTCTAS_AL = 1032;
        /** The not alta cuentas catnominterbregistro. */
        public int NOT_ALTA_CUENTAS_CATNOMINTERBREGISTRO = 1033;
        /** The not admin usuarios. */
        public int NOT_ADMIN_USUARIOS = 1034;
        /** The not admin lym. */
        public int NOT_ADMIN_LYM = 1035;
        /** The alta beneficiarios pago ocurre. */
        public int ALTA_BENEFICIARIOS_PAGO_OCURRE = 1036;
        /** The aut beneficiarios pago ocurre. */
        public int AUT_BENEFICIARIOS_PAGO_OCURRE = 1037;
        /** The not beneficiarios pago ocurre. */
        public int NOT_BENEFICIARIOS_PAGO_OCURRE = 1038;
        /** The not beneficiarios pago directo. */
        public int NOT_BENEFICIARIOS_PAGO_DIRECTO = 1039;
        /** The not transferencias fx online. */
        public int NOT_TRANSFERENCIAS_FX_ONLINE = 1040;
        /** The pago sua lc. */
        public int PAGO_SUA_LC = 1041;

        /** The bloqueo token intentos. */
        public int BLOQUEO_TOKEN_INTENTOS  = 1042;
        /**
         * variable de vinculacion tarjetas para envio de correo.
         */
        public int SERV_TARJETA_VINCULA  = 1043;

        /**Variables para ventanas de parametrizacion RSA**/
        /**desvinculacion**/
        public int DESVINCULACION_DISP  = 1044;
        /**cambio imagen**/
        public int CAMBIO_IMAGEN  = 1045;
        /**cambio pregunta**/
        public int CAMBIO_PREGUNTA  = 1046;
        /**enrolamiento rsa**/
        public int ENROLAMIENT0_RSA  = 1047;

        /** The review rsa analista. */
        public int REVIEW_RSA_ANALISTA = 1048;
        /** The deny rsa analista. */
        public int DENY_RSA_ANALISTA = 1049;
        /** The review rsa cliente. */
        public int REVIEW_RSA_CLIENTE = 1050;
        /**FIN Variables para ventanas de parametrizacion RSA**/

        /** Constante para Pago MIcrositio **/
        public int NOT_PAGO_MICROSITIO = 1054;

        /**edo cta ind**/
        public int EDO_CUENTA_IND  = 1051; //1048;
        /**sol edocta historico**/
        public int SOLEDOCTAHIST  = 1052; //1049;
        /**config masiva por cta**/
        public int CONFIGMASPORCUENTA = 1053; //1050;
        
        /** The not FIEL alta .. */
        public int NOT_FIEL_ALTA = 1056;
        
        /** The not FIEL baja .. */
        public int NOT_FIEL_BAJA = 1057 ;
        
        /** The not FIEL modificacion .. */
        public int NOT_FIEL_MODIFICACION = 1058;
        
        
}