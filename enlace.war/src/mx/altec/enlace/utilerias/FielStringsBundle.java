/**
 *   Isban Mexico
 *   Clase: FielStringsBundle.java
 *   Descripcion: Clase utilitaria para obtener la propiedades
 *   que se mostraran en los titulos de las etiquetas del modulo Fiel y
 *   Alta de Cuentas.
 *
 *   Control de Cambios:
 *   1.0 22/06/2016  FSW. Everis
 */
package mx.altec.enlace.utilerias;

import java.io.FileReader;
import java.io.IOException;
import java.util.PropertyResourceBundle;

/**
 * Clase para titulos de etiquetas del modulo fiel
 * @author FSW Everis
 */
public class FielStringsBundle extends PropertyResourceBundle {

    /**
     * Construye el listado de cadenas usando la configuracion global
     * @throws IOException en caso de error al abrir el archivo
     */
    public FielStringsBundle() throws IOException {
        super(new FileReader(Global.rutaBundleFiel));
    }

}