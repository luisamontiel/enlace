/** 
*   Isban Mexico
*   Clase: EnlaceLynxConstants
*   Descripcion: Constantes comunes y metodos de apoyo para llenado de campos
*
*   Control de Cambios:
*   1.0 Julio del 2015 FSW Everis 
*/
package mx.altec.enlace.utilerias;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import mx.altec.enlace.bo.BaseResource;

/**
 * Contiene las constantes de uso comun para Lynx y metodos utilitarios de
 * carga de campos por defecto.
 */
public final class EnlaceLynxConstants {
	
	/**
	 * Constante para el valor no monetario
	 */
	public static final String TIPO_MSG_NO_MONETARIA = "NM";
	
	/**
	 * Constante para el valor de host origen
	 */
	public static final String HOST_ORIGEN_ENLACE = "7";
	
    /** Constante para el valor del id del usuario. */
    public static final String REQH_USER = "iv-user";
	
	/**
	 * Constante para el valor de la IP
	 */
	public static final String REQH_IP = "iv-remote-address";
	
	/**
	 * Constante para el valor del navegador
	 */
	public static final String REQH_NAVEGADOR = "User-Agent";
	
	/**
	 * Constante para el valor del lenguaje
	 */
	public static final String REQH_LENGUAJE = "Accept-Language";
	
	/**
	 * Constante para el valor del canal
	 */
	public static final String CANAL_INTERNET = "1";
	
	/**
	 * Constante para el valor del online on
	 */
	public static final String ONLINE_ON = "1";
	
	/**
	 * Constante que indica si Lynx debe enviar un RR de respuesta al host
	 */
	public static final String FLAG_RESPUESTA_LYNX = "1";
	
	/**
	 * Constante que indica la aplicacion enlace
	 */
	public static final String APLICACION = "ENLA";
	
	/**
	 * Constante que indica el medio de autentificacion a la aplicacion
	 */
	public static final String MEDIO_AUT_PASSWORD = "6";
	
	/**
	 * Constante que indicala opcion de Alta de cuenta mismo Banco
	 */
	public static final String ALTA_CUENTA_MISMO_BANCO = "ACTMB";
	
	/**
	 * Constante que indicala opcion de Alta de cuenta otros Bancos
	 */
	public static final String ALTA_CUENTA_OTROS_BANCOS = "ACTOB";
	
	/**
	 * Constante que indicala opcion de Alta de cuenta movil mismo Banco
	 */
	public static final String ALTA_CUENTA_MOVIL_BANCO = "ACMMB";
	/**
	 * Constante que indicala el mecanismo de firma utilizado por el cliente
	 *  al realizar la transaccion en el aplicativo Token fisico OTP
	 */
	public static final String OTP = "2";
	/**
	 * Constante que indicala el pais de origen
	 */
	public static final String PAIS = "MEX";
	/**
	 * Constante que indicala si se aplico pregunta desafio para verificacion de su acceso
	 */
	public static final String NO_APLICA_PREGUNTA_DESAFIO = "0";
	/**
	 * Constante que indicala el acceso Online, via web
	 */
	public static final String MODO_ACCESO_ONLINE_WEB = "2";
	/**
	 * Constante para el tipo de mensaje PB
	 */
	public static final String TIPO_MSG_TMB = "PB";
	
	/**
	 * Constante para el tipo de operacion
	 */
	public static final String TIPO_OPE = "18";
	
	/**
	 * Constante para la entidad origen/principal
	 */
	public static final String ENT_ORIGEN = "0014";
	
	/**
	 * Constante para la moneda orginal
	 */
	public static final String MONEDA_ORIGINAL = "484";
	
	/** Constante para la moneda dolar. */
    public static final String MONEDA_DOLAR = "840";
	
	/**
	 * Tipo de transaccion Alta de cuenta mismo Banco
	 */
	public static final String TIPOTRAN_ACTMB = "1";
	/**
	 * Tipo de transaccion Alta de cuenta otros Bancos
	 */
	public static final String TIPOTRAN_ACTOB = "2";
	/**
     * Tipo de transaccion Alta de cuenta internacional
     */
    public static final String TIPOTRAN_ACTBI = "41";
    /**
     * Tipo de transaccion Alta de cuenta movil otros bancos
     */
    public static final String TIPOTRAN_ACMOB = "40";
	/**
	 * Tipo de transaccion Alta de cuenta movil mismo Banco
	 */
	public static final String TIPOTRAN_ACMMB = "3";
	/**
	 * Tipo de transaccion Activacion de cuenta
	 */
	public static final String TIPOTRAN_ACTIVA = "39";
	/**
	 * Tipo de transaccion Baja de cuenta mismo Banco
	 */
	public static final String TIPOTRAN_BCTMB = "8";
	/**
	 * Tipo de transaccion Baja de cuenta otros Bancos
	 */
	public static final String TIPOTRAN_BCTOB = "9";
	/**
	 * Tipo de transaccion Baja de cuenta movil mismo Banco
	 */
	public static final String TIPOTRAN_BCMMB = "10";
	/**
	 * Tipo de transaccion Baja de cuenta internacional.
	 */
	public static final String TIPOTRAN_BCI = "42";
	/**
	 * Tipo de transaccion Baja de cuenta movil otros Bancos.
	 */
	public static final String TIPOTRAN_BCMOB = "43";
	/**
	 *  Tipo de cuenta MISMO BANCO
	 */
	public static final String TIPOCTA_MISMO_BANCO = "200";
	/** 
	 * Tipo de cuenta INTERBANCARIAS
	 */
	public static final String TIPOCTA_INTERBANCARIAS = "201";
	/**
	 *  Tipo de cuenta INTERNACIONALES
	 */
	public static final String TIPOCTA_INTERNACIONALES = "202";
    /**
     * Forma de liquidacion, transferencia mismo dia.
     */
    public static final String FORMLIQ_MISMO_DIA = "H";
    /**
     * Tipo de transaccion de cargo.
     */
    public static final String TIPO_CARGO = "1";
    /**
     * Comprobante requerido.
     */
    public static final String COMPROBANTE_CON_IVA = "1";
    /**
     * No requiere comprobante desglosado.
     */
    public static final String COMPROBANTE_NINGUNO = "0";
	/**
	 *  Fin de mensaje.
	 */
	public static final String FIN_MENSAJE = String.valueOf((char)0x0A);
	/**
	 * Codigo de respuesta OK
	 */
	public static final String CODRESP_OK = "000";
	/**
	 * Codigo de respuesta ERROR
	 */
	public static final String CODRESP_ERR = "999";
	/**
	 * Codigo de respuesta ERROR
	 */
	public static final String CODRESP_DEN_LYNX = "680";
	/**
	 * Constante para transferencia
	 */
	public static final String TRANSFERENCIA = "transferencia";
	 /**
	  * Constante que guarda codigo de error para operacion exitosa
	  */
	public static final String ERROR_CODE_OK = "COD_OK";
	 /**
	  * Constante que guarda codigo de error para operacion no exitosa
	  */
	public static final String ERROR_CODE_NOK = "COD_NOK";
	/**
	 * Locale comun.
	 */
	public static final Locale LOCALE_MX = new Locale("ES", "mx");
	/**
	 * Formato de fecha para id de transaccion unica
	 */
	public static final String FORMATO_FCH_ID_TRANS_UNI = "yyyyMMddHHmmssSSSSSS";
	/**
	 * Formato de fecha para envio y de operacion
	 */
	public static final String FORMATO_FCH_OPE_ENV = "yyyyMMddHHmmss";
	/**
	 * Formato de fecha para envio y de operacion
	 */
	public static final String TIPO_CLIENTE = "J";
	/**
	 * Codigo de error cuando lynx no autoriza
	 */
	public static final String COD_ERROR_LYNX_DENIEGA = "TRAN0515";
	/**
	 * Constante para el tipo de mensaje CF
	 */
	public static final String TIPO_MSG_ACTUALIZACION_RESP = "CF";
	/**
	 * Constante para el tipo de origen de del RO
	 */
	public static final String TIPO_ORIGEN_PB = "0D";
	/**
	 * Constante CERO 0
	 */
	public static final String CERO = "0";
	/**
	 * Constante CADENA_VACIA
	 */
	public static final String CADENA_VACIA = "";
	
	/**
	 * Constructor privado de la clase
	 */
	private EnlaceLynxConstants(){
	}
	
	/**
	 * Metodo que carga valores de mensajes de actualizacion de codigo de respuesta.	 
	 * @param map contenedor de la parametrizacion de los campos
	 * @param numCta numero de cuenta
	 * @param idTran identificador de la trama
	 * @return mapa con valores de mensaje CF
	 */
	public static final Map<Integer, String> cargaValoresDefaultCF( Map<Integer, String> map, String numCta, String idTran){
		SimpleDateFormat formatEnvioMsg = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, EnlaceLynxConstants.LOCALE_MX);
		map.put(1, EnlaceLynxConstants.TIPO_MSG_ACTUALIZACION_RESP);  // Tipo de mensaje
		map.put(2, EnlaceLynxConstants.HOST_ORIGEN_ENLACE);  // Identifica el sistema que comunica la operacion
		map.put(3, EnlaceLynxConstants.TIPO_ORIGEN_PB);  // Identifica el tipo de objeto origen
		map.put(4, numCta);  // Numero de cuenta, cuenta origen
		map.put(5, formatEnvioMsg.format(new Date()));  // Fecha y hora local de este mensaje
		map.put(7, idTran);
		map.put(8, FIN_MENSAJE);
		return map;
	}
	
	/**
	 * Metodo encargado de parametrizar los valores por defecto para la trama NM
	 * 
	 * @param mapa contenedor de la parametrizacion de los valores
	 * @param req Objeto con informacion inherente a la peticion
	 * @return mapa con valores de mensaje NM
	 */
	public static final Map<Integer, String> cargaValoresDefaultNM( Map<Integer, String> mapa,HttpServletRequest req){
		cargaValoresDefault(mapa, req); // Se llenan campos 2,4,10,11,12,13,15,16,17,18,19,20
		mapa.put(1, EnlaceLynxConstants.TIPO_MSG_NO_MONETARIA);  // Tipo de mensaje
	    mapa.put(11, EnlaceLynxConstants.CERO);  // Flag de repuesta requerida de Lynx
		mapa.put(22, EnlaceLynxConstants.OTP);  // Tipo de firma
		mapa.put(25, EnlaceLynxConstants.NO_APLICA_PREGUNTA_DESAFIO);  // Pregunta desafio
		mapa.put(30, EnlaceLynxConstants.ENT_ORIGEN);  // Pais origen
		mapa.put(31, EnlaceLynxConstants.PAIS);  // Pais origen
		mapa.put(37, EnlaceLynxConstants.CODRESP_OK);
		mapa.put(39, FIN_MENSAJE);
		// Se llenan campos 1,2,4,5,10,11,12,13,14,15,16,17,18,19,20,22,25,31,37,39
		return mapa;
	}

	/**
	 * Metodo encargado de parametrizar los valores por defecto para la trama PB
	 * 
	 * @param mapa contenedor de la parametrizacion de los valores
	 * @param req Objeto con informacion inherente a la peticion
	 * @return mapa con valores de mensaje PB
	 */
	public static final Map<Integer, String> cargaValoresDefaultPB( Map<Integer, String> mapa, HttpServletRequest req){
		cargaValoresDefault(mapa, req);
		mapa.put(1, EnlaceLynxConstants.TIPO_MSG_TMB);  // Tipo de mensaje
	    mapa.put(11, EnlaceLynxConstants.FLAG_RESPUESTA_LYNX);  // Flag de repuesta requerida de Lynx
		mapa.put(21, EnlaceLynxConstants.TIPO_OPE);  // Tipo de operacion
		mapa.put(23, EnlaceLynxConstants.OTP);  // Tipo de firma
		mapa.put(27, EnlaceLynxConstants.ENT_ORIGEN); // Entidad origen/principal
		mapa.put(28, EnlaceLynxConstants.PAIS);  // Pais origen
		mapa.put(41, EnlaceLynxConstants.MONEDA_ORIGINAL);  // Moneda original
		mapa.put(50, EnlaceLynxConstants.CODRESP_OK);  // Codigo de respuesta
		mapa.put(75, FIN_MENSAJE);
		// Se llenan campos 1,2,4,5,10,11,12,13,14,15,16,17,18,19,20,21,23,27,28,41,50,75
		return mapa;
		
	}


	/**
	 * Metodo encargado de parametrizar los valores defalt para tramas NM y PB
	 * 
	 * @param mapa contenedor de la parametrizacion de los valores
	 * @param req Objeto con informacion inherente a la peticion
	 * @return mapa con valores
	 */
	private static Map<Integer, String> cargaValoresDefault( 
            Map<Integer, String> mapa, HttpServletRequest req){
        BaseResource sesion = (BaseResource) req.getSession().getAttribute("session");
		mapa.put(2, EnlaceLynxConstants.HOST_ORIGEN_ENLACE);  // Host origen 
		mapa.put(4, EnlaceLynxConstants.CANAL_INTERNET);// Canal
		mapa.put(5, EnlaceLynxConstants.TIPO_CLIENTE); // Tipo de cliente
		mapa.put(6, sesion.getUserID8()); // Identificador de cliente
		mapa.put(8, sesion.getContractNumber()); // Contrato
		mapa.put(9, sesion.getNombreContrato()); // Nombre del contrato
		mapa.put(10, EnlaceLynxConstants.ONLINE_ON);  // Online Flag 
		mapa.put(12, EnlaceLynxConstants.MODO_ACCESO_ONLINE_WEB);  // Modo de acceso
		mapa.put(13, req.getSession().getId() == null ?
                EnlaceLynxConstants.CADENA_VACIA : req.getSession().getId());  // Id de session
		mapa.put(14, req.getHeader(REQH_USER));  // Usuario
		mapa.put(15, (req.getHeader(REQH_IP)!= null && !EnlaceLynxConstants.CADENA_VACIA.equals(req.getHeader(REQH_IP).trim()) ? req.getHeader(REQH_IP) : req.getRemoteAddr()));  // IP de la conexion
		mapa.put(16, req.getHeader(REQH_NAVEGADOR));  // Navegador y sistema operativo desde donde se conecta el usuario
		mapa.put(17, req.getHeader(REQH_LENGUAJE));  // Idioma del navegador
		SimpleDateFormat format = new SimpleDateFormat(FORMATO_FCH_OPE_ENV, LOCALE_MX);
		mapa.put(18, format.format(new Date(req.getSession().getCreationTime())));  // Fecha y hora de session
		mapa.put(19, EnlaceLynxConstants.APLICACION);  // Aplicacion
		mapa.put(20, EnlaceLynxConstants.MEDIO_AUT_PASSWORD);  // Medio de Autentificacion   
		// Se llenan campos 2,4,5,10,11,12,13,14,15,16,17,18,19,20
		return mapa;
	}

}
