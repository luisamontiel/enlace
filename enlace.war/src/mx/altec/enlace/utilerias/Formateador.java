package mx.altec.enlace.utilerias;

public class Formateador {
	private static final boolean ACTIVAR = false;
	/**
	 * Metodo para pintar las primeras 4 lineas de la excepcion
	 * @param e Exception
	 * @return mensaje para mostrar
	 */
	public String formatea(Exception e) {
		if (ACTIVAR == true) e.printStackTrace();
		StringBuilder mensaje = new StringBuilder();
		StackTraceElement [] array = e.getStackTrace();
		mensaje.append("exp ").append(e.getClass().toString()).append(": ").append(e.getMessage()).append(" ");

		for (int aux=0; aux<array.length && aux<4; aux++) {
			mensaje = mensaje.append(array[aux].toString()).append(" ");
		}

		return  mensaje.toString().replaceAll("Exception", "").replaceAll("Error", "").replaceAll("Excepcion", "");
	}
}