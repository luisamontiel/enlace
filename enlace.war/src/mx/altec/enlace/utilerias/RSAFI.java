package mx.altec.enlace.utilerias;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.StringTokenizer;

import mx.isban.CmpRSA.config.Configuracion;
import mx.isban.CmpRSA.fuentesWS.AnalyzeRequest;
import mx.isban.CmpRSA.fuentesWS.AnalyzeResponse;
import mx.isban.CmpRSA.fuentesWS.DeviceRequest;
import mx.isban.CmpRSA.fuentesWS.EventData;
import mx.isban.CmpRSA.fuentesWS.GenericActionType;
import mx.isban.CmpRSA.fuentesWS.GenericActionTypeList;
import mx.isban.CmpRSA.fuentesWS.IdentificationData;
import mx.isban.CmpRSA.fuentesWS.MessageHeader;
import mx.isban.CmpRSA.fuentesWS.RunRiskType;
import mx.isban.CmpRSA.fuentesWS.SecurityHeader;
import mx.isban.CmpRSA.fuentesWS.UserStatus;
import mx.isban.CmpRSA.metodos.AnalyzeRSA;
import mx.isban.CmpRSA.fuentesWS.WSUserType;
import mx.isban.CmpRSA.fuentesWS.APIType;
import mx.isban.CmpRSA.fuentesWS.RequestType;
import mx.isban.CmpRSA.fuentesWS.MessageVersion;
import mx.isban.CmpRSA.fuentesWS.AuthorizationMethod;
import mx.isban.CmpRSA.fuentesWS.EventType;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RSAFI {

	/****************************************************************
	 * Llama a API AnalyzeRequest
	 * 
	 * @param devicePrinter
	 *            String que contiene los datos del equipo del cliente
	 * @param httpAcept
	 *            Valor de encabezado http que indica que tipo de archivo es la
	 *            página cliente.
	 * @param httpAceptChars
	 *            El conjunto de caracteres del encabezado de aceptación de
	 *            HTTP.
	 * @param httpAceptEncoding
	 *            La codificación de aceptación de HTTP
	 * @param httpAceptLanguage
	 *            El idioma de aceptación de HTTP
	 * @param httpReferer
	 *            El valor del encabezado del origen de referencia de HTTP.
	 * @param ipAddr
	 *            Dirección IP del dispositivo del usuario
	 * @param httpUserAgent
	 *            La cadena de agente del usuario
	 * @param userName
	 *            Código del Cliente del usuario
	 * @param eDescription
	 *            Tipo de acceso
	 * @param aplicativo
	 *            Acceso a aplicativo
	 * @param usr8
	 *            Usuario de aplicacion
	 * @param deviceTokenCookieBrow
	 *            Valor de la cookie del usuario devicetoken
	 * @param deviceTokenCookieFSOBrow
	 *            Valor de la cookie del usuario deviceTokenFSO
	 * @throws InterruptedException
	 *             Excepcion de conexion.
	 * @throws ExecutionException
	 *             Excepcion de conexion.
	 * @return tokenArray Arreglo que contiene deviceToken y DeviceTokenFSO
	 * ***************************************************************/
	public String[] ejecutaAnalyzeRequest(String devicePrinter,
			String httpAcept, String httpAceptChars, String httpAceptEncoding,
			String httpAceptLanguage, String httpReferer, String ipAddr,
			String httpUserAgent, String userName, String eDescription,
			String usr8, String aplicativo, String deviceTokenCookieBrow,
			String deviceTokenCookieFSOBrow) throws InterruptedException,
			ExecutionException {

		final ExecutorService executor = Executors.newSingleThreadExecutor();

		final Future<String> future = executor.submit(new RSAFI$1(devicePrinter,
				httpAcept, httpAceptChars, httpAceptEncoding,
				httpAceptLanguage, httpReferer, ipAddr, httpUserAgent,
				userName, usr8, aplicativo, deviceTokenCookieBrow,
				deviceTokenCookieFSOBrow));

		final String tokenArray[] = new String[2];

		try {
			EIGlobal.mensajePorTrace("Started..", EIGlobal.NivelLog.ERROR);			
			final String cookies = future.get(1000, TimeUnit.MILLISECONDS);
			final StringTokenizer tokenCookie = new StringTokenizer(cookies,
					"#");
			int count = 0;
			while (tokenCookie.hasMoreTokens()) {
				tokenArray[count] = tokenCookie.nextToken();
				count++;
			}						
			EIGlobal.mensajePorTrace("Realizo conexion a RSA", EIGlobal.NivelLog.ERROR);

		} catch (TimeoutException e) {
			tokenArray[0]="";
			tokenArray[1]="";
			EIGlobal.mensajePorTrace("No hay conexion con RSA ejecutaAnalyzeRequest!",
					EIGlobal.NivelLog.ERROR);
		}
		executor.shutdownNow();
		return tokenArray;
	}
}
/*******************************************
 *  Clase que implementa a Callable para manejo de hilos.
 */
class RSAFI$1 implements Callable<String> {

	/**
	 * Device Print del usuario
	 */
	private transient String devicePrinter;
	/**
	 * Valor httpAcept del Header
	 */
	private final transient String httpAcept;
	/**
	 * Valor httpAceptChars del Header
	 */
	private final transient String httpAceptChars;
	/**
	 * Valor httpAceptEncoding del Header
	 */
	private final transient String httpAceptEncoding;
	/**
	 * valor httpAceptLanguage del Header
	 */
	private final transient String httpAceptLanguage;
	/**
	 * valor httpReferer del Header
	 */
	private final transient String httpReferer;
	/**
	 * Direccion IP del usuario
	 */
	private final transient String ipAddr;
	/**
	 * valor httpUserAgent del Header
	 */
	private final transient String httpUserAgent;
	/**
	 * Codigo de usuario
	 */
	private final transient String userName;
	/**
	 * Codigo de 8 digitos de usuario
	 */
	private final transient String usr8;
	/**
	 * Aplicativo del que proviene.
	 */
	private final transient String aplicativo;
	/**
	 * Valor de la Cookie en el navegador del usuario.
	 */
	private final transient String deviceTokenCookieBrow;
	/**
	 * Valor de la Cookie en el navegador del usuario.
	 */
	private final transient String deviceTokenCookieFSOBrow;

	/********************************************************************
	 * Metodo que implementa a la Clase Callable
	 * 
	 * @param devicePrinter
	 *            Deviceprint del usuario.
	 * @param httpAcept
	 *            valor httpAcept del header.
	 * @param httpAceptChars
	 *            valor httpAceptChars del header.
	 * @param httpAceptEncoding
	 *            valor httpAceptEncoding del header.
	 * @param httpAceptLanguage
	 *            valor httpAceptLanguage del header.
	 * @param httpReferer
	 *            valor httpReferer del header.
	 * @param ipAddr
	 *            Direccion ip del usuario.
	 * @param httpUserAgent
	 *            valor httpUserAgent del header.
	 * @param userName
	 *            Codigo de usuario.
	 * @param usr8
	 *            valor 8 digistos codigo usuario.
	 * @param aplicativo
	 *            Aplicativo del que proviene la peticion.
	 * @param deviceTokenCookieBrow
	 *            Valor actual de esta cookien en el navegador del usuario.
	 * @param deviceTokenCookieFSOBrow
	 *            Valor actual de esta cookien en el navegador del usuario.
	 * 
	 ********************************************************************/
	RSAFI$1(String devicePrinter, String httpAcept, String httpAceptChars,
			String httpAceptEncoding, String httpAceptLanguage,
			String httpReferer, String ipAddr, String httpUserAgent,
			String userName, String usr8, String aplicativo,
			String deviceTokenCookieBrow, String deviceTokenCookieFSOBrow) {
		this.devicePrinter = devicePrinter;
		this.httpAcept = httpAcept;
		this.httpAceptChars = httpAceptChars;
		this.httpAceptEncoding = httpAceptEncoding;
		this.httpAceptLanguage = httpAceptLanguage;
		this.httpReferer = httpReferer;
		this.ipAddr = ipAddr;
		this.httpUserAgent = httpUserAgent;
		this.userName = userName;
		this.usr8 = usr8;
		this.aplicativo = aplicativo;
		this.deviceTokenCookieBrow = deviceTokenCookieBrow;
		this.deviceTokenCookieFSOBrow = deviceTokenCookieFSOBrow;

	}

	@Override
	public String call() {
		
		iniciarConexionRSA();

		final String orgName = "Enlace";
		final String userLoginName = this.usr8;

		final DeviceRequest deviceRequest = new DeviceRequest();
		final IdentificationData identificationData = new IdentificationData();
		final MessageHeader messageHeader = new MessageHeader();
		final SecurityHeader securityHeader = new SecurityHeader();

		if (devicePrinter == null) {
			devicePrinter = "_";
		}

		String devicePrint = "";
		try {
			devicePrint = java.net.URLDecoder.decode(this.devicePrinter, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Bloque catch generado automáticamente
			EIGlobal.mensajePorTrace("Device Print Vacio",
					EIGlobal.NivelLog.ERROR);
		}

		deviceRequest.setDevicePrint(devicePrint);
		deviceRequest.setHttpAccept(this.httpAcept);
		deviceRequest.setHttpAcceptChars(this.httpAceptChars);
		deviceRequest.setHttpAcceptEncoding(this.httpAceptEncoding);
		deviceRequest.setHttpAcceptLanguage(this.httpAceptLanguage);
		deviceRequest.setHttpReferrer(this.httpReferer);
		deviceRequest.setIpAddress(this.ipAddr);
		deviceRequest.setDeviceTokenCookie(this.deviceTokenCookieBrow);
		deviceRequest.setDeviceTokenFSO(this.deviceTokenCookieFSOBrow);
		deviceRequest.setUserAgent(this.httpUserAgent);
		identificationData.setOrgName(orgName);
		identificationData.setUserLoginName(userLoginName);
		identificationData.setUserName(this.userName);
		identificationData.setUserStatus(UserStatus.NOTENROLLED);
		identificationData.setUserType(WSUserType.PERSISTENT);
		messageHeader.setApiType(APIType.DIRECT_SOAP_API);
		messageHeader.setRequestType(RequestType.ANALYZE);
		messageHeader.setVersion(MessageVersion.value1);
		securityHeader.setCallerCredential("Capitulo10!");
		securityHeader.setCallerId("rsacaller");
		securityHeader.setMethod(AuthorizationMethod.PASSWORD);

		final EventData eventData = new EventData();
		eventData.setEventType(EventType.SESSION_SIGNIN);

		if ("Enlace".equals(this.aplicativo)) {
			eventData.setEventDescription("Acceso de Enlace");
		} else {
			eventData.setEventDescription("Acceso de Micrositio Enlace");
		}

		final EventData[] eventDataList = { eventData };

		final GenericActionTypeList genericActionTypeList = new GenericActionTypeList();
		final GenericActionType[] actionTypes = { GenericActionType.SET_USER_STATUS };
		genericActionTypeList.setGenericActionTypes(actionTypes);

		String tokenArray[] = new String[2];
		tokenArray = ejecutaAnalyze(genericActionTypeList, deviceRequest,
				identificationData, messageHeader, securityHeader
				,eventDataList);
		
		return tokenArray[0] + "#" + tokenArray[1];
	}
	
	/************************************************************
	 * Metodo que hace la conexion con RSA.
	 * 
	 *
	 ************************************************************/
	public void iniciarConexionRSA() {
		HashMap<String, String> hash;
		hash = new HashMap<String, String>();
		hash.put("RSA_CALLER_CREDENTIAL", "Capitulo10!");
		hash.put("RSA_CALLER_ID", "rsacaller");
		hash.put("RSA_WS_TIMEOUT", "1000");
		hash.put("RSA_WEBSERVICE", "https://bcaempadauth.mx.corp/AdaptiveAuthentication/services/AdaptiveAuthentication");	
		hash.put("LOG_LEVEL", "ALL");
		hash.put("ID_CANAL", "default"); // "SuperNetEmp" - marca error pq no
		// esta habilitado
		
		Configuracion.init(hash);

	}
	
	/****************************************************************
	 * Funcion que se ejecuta para realizar el llamado a RSA
	 * 
	 * @param genericActionTypeList
	 *            Lista Generica de acciones.
	 * @param deviceRequest
	 *            Informacion del dispositivo.
	 * @param identificationData
	 *            Informacion de identificacion del usuario.
	 * @param messageHeader
	 *            Informacion del Header.
	 * @param securityHeader
	 *            Informacion de seguridad.
	 * @param eventDataList
	 *            Lista de eventos.
	 * 
	 * @return tokenArray Arreglo con informacion de DeviceToken y
	 *         DeviceTokenFSO
	 * 
	 *****************************************************************/
	public String[] ejecutaAnalyze(GenericActionTypeList genericActionTypeList,
			DeviceRequest deviceRequest, IdentificationData identificationData,
			MessageHeader messageHeader, SecurityHeader securityHeader,
			EventData[] eventDataList) {
		
		final boolean autoCreateUserFlag=true;

		final mx.isban.CmpRSA.metodos.AnalyzeRSA analyzeRSA = new AnalyzeRSA();
		final AnalyzeRequest analyzeRequest = new AnalyzeRequest();

		analyzeRequest.setActionTypeList(genericActionTypeList);
		analyzeRequest.setDeviceRequest(deviceRequest);
		analyzeRequest.setIdentificationData(identificationData);
		analyzeRequest.setMessageHeader(messageHeader);
		analyzeRequest.setSecurityHeader(securityHeader);
		analyzeRequest.setAutoCreateUserFlag(autoCreateUserFlag);
		analyzeRequest.setEventDataList(eventDataList);
		analyzeRequest.setRunRiskType(RunRiskType.ALL);

		AnalyzeResponse analyzeResponse = new AnalyzeResponse();

		String deviceTokenCookie = "";
		String deviceTokenFSO = "";
			analyzeResponse = analyzeRSA.executeAnalyze(analyzeRequest);

			EIGlobal.mensajePorTrace("Reason Description RSA: "
					+ analyzeResponse.getStatusHeader().getReasonDescription(),
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Reason  RSA: "
					+ analyzeResponse.getStatusHeader().getReasonCode(),
					EIGlobal.NivelLog.DEBUG);

			deviceTokenCookie = analyzeResponse.getDeviceResult().getDeviceData()
					.getDeviceTokenCookie();
			deviceTokenFSO = analyzeResponse.getDeviceResult().getDeviceData()
					.getDeviceTokenFSO();
			
		final String tokenArray[] = new String[2];

		tokenArray[0] = deviceTokenCookie;
		tokenArray[1] = deviceTokenFSO;

		return tokenArray;
	}

}