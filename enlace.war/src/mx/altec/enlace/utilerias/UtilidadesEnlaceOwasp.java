package mx.altec.enlace.utilerias;

import java.util.regex.Pattern;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import java.io.FileNotFoundException;
import java.util.UUID;
/** 
*   Isban Mexico
*   Clase: UtilidadesEnlaceOwasp.java
*   Descripcion: Utileria para vulnerabilidades OWASP.
*
*   Control de Cambios:
*   1.0 16/10/2017 Creacion-Everis 
*/
public final class UtilidadesEnlaceOwasp {
	
	
	/**
	 * Constructor privado
	 */
	private UtilidadesEnlaceOwasp(){}

	/**
	 * Valida Cross-Site Scripting 
	 * @param value cadena a validar
	 * @return String regresa la cadena a limpiar
	 */
	public static String cleanString(final String value) {
		EIGlobal.mensajePorTrace("Entrando a cleanString con Cadena ||"+value+"||", EIGlobal.NivelLog.INFO);
		String newValue = value; //se crea una nueva variable para un nuevo valor
		if(newValue != null && !"".equals(newValue)) { 
			EIGlobal.mensajePorTrace("Se realiza la limpieza de la Cadena ||"+newValue+"||", EIGlobal.NivelLog.INFO);
			newValue = newValue.replaceAll("", ""); //se remplaza con ""
			Pattern scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
			newValue = scriptPattern.matcher(newValue).replaceAll("");
			scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
			newValue = scriptPattern.matcher(newValue).replaceAll("");//hace un match segun el pattern
			scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			newValue = scriptPattern.matcher(newValue).replaceAll("");//hace un match segun el pattern
			EIGlobal.mensajePorTrace("Finaliza la limpieza de la Cadena ||"+newValue+"||", EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace("Saliendo de cleanString con Cadena ||"+newValue+"||", EIGlobal.NivelLog.INFO);
		return newValue;
	}
	
	/**
	 * Valida SQL Injection
	 * @param value cadena a validar
	 * @return String regresa un nuevo valor de la cadena
	 */	
	public static String cleanQuery(final String value) {
		EIGlobal.mensajePorTrace("Entrando a cleanQuery con Cadena ||"+value+"||", EIGlobal.NivelLog.INFO);
		String valorCadena = value; //se crea una nueva variable para el nuevo valor
        if(valorCadena != null && !"".equals(valorCadena)) {
        	EIGlobal.mensajePorTrace("Se realiza la limpieza del query ||"+valorCadena+"||", EIGlobal.NivelLog.INFO);
        	valorCadena = value.replaceAll("", ""); //se remplaza con espacio 
            Pattern scriptPattern = Pattern.compile("('(''|[^'])*')|(;)|(\\b(AND|OR|LIKE|WHERE|FROM|ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\\b)", Pattern.CASE_INSENSITIVE);
            valorCadena = scriptPattern.matcher(valorCadena).replaceAll(""); //hace un match segun el pattern
		}
        EIGlobal.mensajePorTrace("Saliendo de cleanQuery con Cadena ||"+valorCadena+"||", EIGlobal.NivelLog.INFO);
		return valorCadena;
	}
	/**
	 * Validacion de Ficheros
	 * @param file_name Nombre del archivo
	 * @param bandera Indica el tipo de archivo que ser� validado
	 * @return Boolean regresa true en caso de ser archivo valido
	 */
	public static Boolean esArchivoValido(File file_name, String bandera) {
		/*Inicia validacion de ficheros maliciosos*/
		EIGlobal.mensajePorTrace("Entrando a esArchivoValido con Cadena ||"+file_name+"||", EIGlobal.NivelLog.INFO);
        Boolean esValido            = false;
        String validar              = bandera;
        String type                 = null;
        String extensionFile        = null;
        String extensionContentType = null;
        FileInputStream in 			= null;
        Tika tika                   = new Tika();
		Metadata metadata           = new Metadata();
        try {
        	/*Se crea un nuevo fichero*/
            in = new FileInputStream(file_name);
        } catch (FileNotFoundException ex) {
        	EIGlobal.mensajePorTrace("ERROR-> FileNotFoundException en: esArchivoValido con Cadena ||"+ex.getMessage()+"||", EIGlobal.NivelLog.INFO);
        }finally {
        	if(in != null) {
        		/*cerrar el inputstream*/
        		in.close();
        	}
        }
        try {
        	/*Se realiza la validacion con apache tika*/
            tika.parse(in , metadata);
        } catch (IOException ex) {  
        	/*excepcion reportada*/
            EIGlobal.mensajePorTrace("ERROR-> IOException en: esArchivoValido con Cadena ||"+ex.getMessage()+"||", EIGlobal.NivelLog.INFO);
        }
            type = metadata.get(Metadata.CONTENT_TYPE);
            EIGlobal.mensajePorTrace("Utilidades.esArchivoValido Tipo de archivo "+type+" ", EIGlobal.NivelLog.INFO);
            if(type == null) {
            	/*tipo de fichero indeterminado*/
            	return esValido;//Si no es posible determinar el tipo de archivo, manda FALSE
            }
            int intExtensionFile        = file_name.toString().lastIndexOf('.');
            EIGlobal.mensajePorTrace("Utilidades.esArchivoValido intExtensionFile "+intExtensionFile+" ", EIGlobal.NivelLog.INFO);
            int intExtensionContentType = type.lastIndexOf('/');
            EIGlobal.mensajePorTrace("Utilidades.esArchivoValido intExtensionContentType "+intExtensionContentType+" ", EIGlobal.NivelLog.INFO);
            if (intExtensionFile >= 0 && intExtensionContentType >= 0) {
                extensionFile        = file_name.toString().substring(intExtensionFile + 1).toLowerCase();
                extensionContentType = type.substring(intExtensionContentType +1 ).toLowerCase();
                /*VALIDA AMBOS TIPOS DE ARCHIVOS (TXT, ZIP)*/
                if("A".equals(validar)) {
                	/*Validar ambos tipos de archivos*/
                    if (("txt").equals(extensionFile) && extensionContentType.contains("plain") 
                        || ("zip").equals(extensionFile) && ("zip").equals(extensionContentType) ) {
                    	EIGlobal.mensajePorTrace("El archivo |"+file_name+"| si es TXT O ZIP", EIGlobal.NivelLog.INFO);
                        esValido = true;                        
                    }
                    /*VALIDA SOLO ARCHIVOS ZIP*/
                } else if("Z".equals(validar) && ("zip").equals(extensionFile) && ("zip").equals(extensionContentType) ) {
                	/*Entra a validar zips*/
                    	EIGlobal.mensajePorTrace("El archivo /"+file_name+"/ si es ZIP", EIGlobal.NivelLog.INFO);
                        esValido = true;
                    /*VALIDA SOLO ARCHIVOS TXT*/
                } else if("T".equals(validar) && ("txt").equals(extensionFile) && extensionContentType.contains("plain") ) {
                	/*Entra a validar txt*/
                		EIGlobal.mensajePorTrace("El archivo ||"+file_name+"|| si es TXT", EIGlobal.NivelLog.INFO);
                        esValido = true;              	
                }
            } 
            /*Finaliza la validacion*/
        EIGlobal.mensajePorTrace("Saliendo de esArchivoValido con Cadena #"+esValido+"#", EIGlobal.NivelLog.INFO);
        return esValido;
    }
	
    /**
     * Generar un token aleatorio para ser usado como validador
     * CSRF
     * @return Token generado
     */
    public static String generarTokenCsrf() {
    	
    	EIGlobal.mensajePorTrace("Inicia generarTokenCsrf", EIGlobal.NivelLog.INFO);
    	String token = UUID.randomUUID().toString();
    	EIGlobal.mensajePorTrace("Finaliza generarTokenCsrf", EIGlobal.NivelLog.INFO);
    	
    	return token;
    }
	
}
