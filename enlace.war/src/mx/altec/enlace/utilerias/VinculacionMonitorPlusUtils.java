/** 
 *   Isban Mexico
 *   Clase: VinculacionMonitorPlusUtils
 *   Descripcion: Metodos auxiliares para cargar los campos de envio a 
 *   MonitorPlus
 *
 *   Control de Cambios:
 *   Diciembre del 2015 FSW Everis 
 */
package mx.altec.enlace.utilerias;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.isban.mp.conector.ConectorMP;

/**
 * Clase utilitaria para cargar los campos de envio a Monitor Plus.
 * @author FSW
 *
 */
public final class VinculacionMonitorPlusUtils {
    
    /**
     * Constructor privado de la clase
     */
    private VinculacionMonitorPlusUtils(){
    }
    
    /**
     * Envio de operacion Inclusionde un usuario a un contrato enlace
     * @param req Objeto con informacion inherente a la peticion
     * @param referencia Numero de referencia que se genero para la bitacora
     * @param sucursalOperante Indica clave de sucursal operante enlace
     * @param session objeto con informacion de la sesion
     */
    public static void enviarInclusionUsuarioContrato(HttpServletRequest req, 
            int referencia, String sucursalOperante, BaseResource session){
        EIGlobal.mensajePorTrace("----- > " + "ENTRA enviarInclusionUsuarioContrato PARA MP ** ",
                EIGlobal.NivelLog.DEBUG);
        Map<Integer, String> mapa = EnlaceMonitorPlusConstants.cargaValDefaultUsContrato(req, sucursalOperante, session);
        String error = mapa.get(90000);
        if(error != null && error != EnlaceMonitorPlusConstants.CADENA_VACIA){
            EIGlobal.mensajePorTrace(" enviarInclusionUsuarioContrato Trx390 no obtuvo datos no se informa a MP ", EIGlobal.NivelLog.DEBUG);
            
        }else{
        	if(ConectorMP.isInit()){

        // Codigo de Transaccion 3501
        mapa.put(EnlaceMonitorPlusConstants.KEY_TRANS, EnlaceMonitorPlusConstants.COD_TRAN_AUSS);
        // Tipo de Novedad 3607 
        mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_NOVEDAD, EnlaceMonitorPlusConstants.COD_TRAN_AUSS);
        // Tipo de Usuario 3587 pendiente agregar valor
        //Numero de Referencia 3563
        mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER, String.valueOf(referencia));
        // aplicacion 21
        mapa.put(EnlaceMonitorPlusConstants.KEY_APLICACION, EnlaceMonitorPlusConstants.APLICACION_ENLA);
        
        // Nva Trx: 3522, 3523, 3502, 3566, 3591, 3592, 3605, 3517, 3597, 3521, 3537, 3507, 3533
        // Default:  1, 2, 3, 4, 5, 6, 7, 9, 3505, 3518, 3519, 3536, 3532, 3506, 3750, 3752, 3753, 10 ** , .
        // DefaultUsContrato: 8, 3540, 3503, 3509, 3510, 3556, 3614, 3534, 3529, 3530
        // InclusionUsuarioContrato: 3501, ** 3607, 3587 ** , 3563
        // Ceros/ Blancos: 3515, 3610, 3615
                // No Aplica: 3600, 3601

        //Se envian los valores  al conector MonitorPlus
        EIGlobal.mensajePorTrace(" ----- > SE ENVIA MENSAJE A  MONITORPLUS ** ",
                 EIGlobal.NivelLog.DEBUG);
        ConectorMP.enviaMsg(mapa, EnlaceMonitorPlusConstants.MSG_ADF_318);
        	}else{
        		EIGlobal.mensajePorTrace("El conector no esta inicializado, no se envia informacion a MP", EIGlobal.NivelLog.DEBUG);
        	}
        }
    }
   
    /**
     * Envio de operacion actualizacion de datos de contrao o usuario 
     * @param mapa contenedor de valores a de monitor plus
     * @param req objeto de sesion inherente a la peticion
     * @param mapaDatosAnteriores mapa con valores de informacion anterior
     * @param tipoActualizacion tipo de actualizacion, de datos de usuario o de contrato
     * @param referencia referencia
     * @param session Objeto con informacion de la sesion
     */
   public static void enviarActualizacionContratoUsuario(Map<Integer, String> mapa, HttpServletRequest req, Map<Integer, String> mapaDatosAnteriores, int tipoActualizacion, int referencia, BaseResource session){
        boolean flgError = true;
        if (tipoActualizacion == EnlaceMonitorPlusConstants.MAN_CONTRATO) {
            // Valor anterior al cambio 3600
            mapa.put(EnlaceMonitorPlusConstants.KEY_DATO_ANTERIOR, mapaDatosAnteriores.get(4));
        } else if (tipoActualizacion == EnlaceMonitorPlusConstants.MAN_USUARIO) {
            // valor anterior al cambio 3600
            mapa.put(EnlaceMonitorPlusConstants.KEY_DATO_ANTERIOR, mapaDatosAnteriores.get(0));
              } else {
            flgError = false;
        }
        if (flgError && ConectorMP.isInit()) {
        	
            // Codigo de Transaccion 3501
            mapa.put(EnlaceMonitorPlusConstants.KEY_TRANS, EnlaceMonitorPlusConstants.COD_TRAN_MODP);
            //Numero de Referencia 3563
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER, String.valueOf(referencia));
            // Tipo de Novedad 3607 
            mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_NOVEDAD, EnlaceMonitorPlusConstants.COD_TRAN_MODP);
            // Aplicacion 21
            mapa.put(EnlaceMonitorPlusConstants.KEY_APLICACION, EnlaceMonitorPlusConstants.APLICACION_ENLA);
                
            //No Aplica: 3615
            // se envian los valores al conector MonitorPlus
            ConectorMP.enviaMsg(mapa, EnlaceMonitorPlusConstants.MSG_ADF_318);
        }
   }
   
   /**
    * Metodo encargado de realizar el mapeo de campos para la llamada a MonitorPlus
    * @param req Objeto con informacion inherente a la peticion
    * @param facultadesAltaCtas valor de las facultades
    * @param cuentas Mapa que contiene las cuentas
    * @param arc Objeto para leer el archivo
    * @param sucursalOperante Indica clave de sucursal operante enlace
    * @param session Objeto con informacion de la sesion
    * @param tipoDato Si es cargado por archivo
    * @throws IOException En caso de que ocurra un error
    */
   public static void enviarAltaCuentas(HttpServletRequest req,
           Map<String, Boolean> facultadesAltaCtas, Map<String, String> cuentas,
           BufferedReader arc, String sucursalOperante , BaseResource session, boolean tipoDato) throws IOException {
       EIGlobal.mensajePorTrace( "Entra a enviarAltaCuentas <---> "
               + "---> |CUENTAS A PROCESAR " + cuentas.size()
               + " --> | FACULTADES " + facultadesAltaCtas
               + " --> | CUENTAS " + cuentas 
               , EIGlobal.NivelLog.INFO);
       String line;
       int countLineas = 0;
       int countProcesadas = 0;
        boolean arcFull;
                if (tipoDato) {
                    arcFull = true;
                }else{
                    arcFull = false;
               }
                
               if (cuentas != null
               && ((Boolean) facultadesAltaCtas.get("AutoCtasMB")
               || (Boolean) facultadesAltaCtas.get("AutoCtasOB"))) {
           EI_Tipo eiTipo = new EI_Tipo();
           while ((line = arc.readLine()) != null) {
               EIGlobal.mensajePorTrace(" ARCHIVO ---> " 
                        +line, EIGlobal.NivelLog.DEBUG);
               countLineas++;
            boolean reportarMonitorPlus = false;
               String cuenta = null;
               if (line.indexOf('@') < 0 && !line.startsWith("0;")) {
                   EIGlobal.mensajePorTrace( "line archivo -->"
                           + line, EIGlobal.NivelLog.INFO);
                   eiTipo.iniciaObjeto(';', '@', line + "@");
                   cuenta = eiTipo.camposTabla[0][1];
                   reportarMonitorPlus = cuentas.containsKey(cuenta)
                           // estatus pendiente de activar
                           && ("P".equals(eiTipo.camposTabla[0][0])
                                   //activada
                                   || "A".equals(eiTipo.camposTabla[0][0]));
                   EIGlobal.mensajePorTrace( " INFORMAR A ** MP ** ---> "
                           + reportarMonitorPlus
                           + " --> | CUENTA " + cuenta
                           + " --> | ESTATUS DE LA CUENTA " + eiTipo.camposTabla[0][0] + " | "
                           , EIGlobal.NivelLog.INFO);
                   }
               if (reportarMonitorPlus) {
                   countProcesadas++;
                   llamadoAltaCuentas(req, sucursalOperante, cuentas, cuenta,session, arcFull);
                   }
               }
           }
       
       EIGlobal.mensajePorTrace(" ----- > SALIENDO DE enviarAltaCuentas, PROCESADAS = "
               + countProcesadas + "  de  " + (countLineas - 1), EIGlobal.NivelLog.INFO);
       }
   
   /**
    * Envio de operacion autorizacion y cancelacion de cuentas
    * @param mapaMonPlus Contenedor de valores parametrizados
    * @param req Objeto con informacion inherente a la peticion
    * @param totalRegistros el numero de operaciones que se quieren relizar
    * @param registroAceptados el numero de operaciones que se aceptaron
    * @param session objeto con informacion de la sesion
    * @param arc buffer reader con errores
    * @param sucursalOperante indica clave de sucursal operante enlace
    */
    public static void enviarValoresAutoCuentas(
            Map<String, Map<Integer, String>> mapaMonPlus,  HttpServletRequest req,int totalRegistros, String registroAceptados, BaseResource session, BufferedReader arc, String sucursalOperante){
        EIGlobal.mensajePorTrace("-------->enviarValoresAutCanCuentas<---------- "+"\n"+
               "Total de registros "+totalRegistros+"\n"+
               "Registros aceptados "+registroAceptados+"\n"+
               "Sucursal operante " + sucursalOperante, EIGlobal.NivelLog.DEBUG);
        String cuenta;
       boolean procesar = false;
       if (String.valueOf(totalRegistros).equals(registroAceptados)) {
           procesar = true;
       } else {
           try {
               String line = arc.readLine();
               if (line != null) {
                   while (line != null) {
                       cuenta = line.substring(0, line.indexOf(';'));
                       if (mapaMonPlus.containsKey(cuenta)) {
                           mapaMonPlus.remove(cuenta);
                       }
                       line = arc.readLine();
                   }
                   procesar ^= mapaMonPlus.isEmpty();
               } else {
                   procesar = true;
               }
           } catch (IOException ioe) {
               EIGlobal.mensajePorTrace("Error en buffer reader", EIGlobal.NivelLog.DEBUG);
               EIGlobal.mensajePorTraceExcepcion(ioe);
           }
       }

       if (procesar) {
           EIGlobal.mensajePorTrace("Procesando " + mapaMonPlus.size() + " operaciones", EIGlobal.NivelLog.DEBUG);
           for (Map.Entry<String, Map<Integer , String>> entry : mapaMonPlus.entrySet()) {
               if ((EnlaceMonitorPlusConstants.ESTATUS_PENDIENTE_AUTORIZAR.equals(entry.getValue().get(2)) 
                       && EnlaceMonitorPlusConstants.TIPO_OPERACION_AUTORIZACION.equals(entry.getValue().get(1)))
                   ) {
                   
                   Map<Integer, String> mapa = EnlaceMonitorPlusConstants.cargaValoresDefaultAltas(req, sucursalOperante, entry.getValue().get(3540), session);
                   String error = mapa.get(90000);
                   if(error != null && error != EnlaceMonitorPlusConstants.CADENA_VACIA){
                        EIGlobal.mensajePorTrace(" enviarTMultiple Trx390 no obtuvo datos no se infroma a monitor ", EIGlobal.NivelLog.DEBUG);
                        
                    }else{
                    	if(ConectorMP.isInit()){
                    //Numero de Cuenta Origen 3540
                    EIGlobal.mensajePorTrace("Cuenta origen "+entry.getValue().get(3540), EIGlobal.NivelLog.DEBUG);
                    mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI, entry.getValue().get(3540));
                   
                    //AutCanCuentas: 3540
                    
                    EIGlobal.mensajePorTrace("-------->Se envia enviarValoresAutoCuentas a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
                   ConectorMP.enviaMsg(mapa, EnlaceMonitorPlusConstants.MSG_ADF310);
                   EIGlobal.mensajePorTrace( "<---- Numero de cuenta  ----> "+entry.getValue().get(3540)+"\n"+
                           "Numero de contrato " + session.getContractNumber()
                          , EIGlobal.NivelLog.INFO);
                      ConectorMP.enviarInfo(mapa, entry.getValue().get(3540), EnlaceMonitorPlusConstants.MSG_ADF310, session.getContractNumber());
                    	}
                   	}
               }
           }
       }
    }
   
   /**
    * Metodo que valida las cuentas para enviar llamados a Monitor Plus.
    * @param req Objeto con informacion inherente a la peticion
    * @param sucursalOperante Indica clave sucursal operante enlace 
    * @param cuentas Mapa que contiene las cuentas
    * @param cuenta Numero de cuenta
    * @param session Objeto con informacion de la sesion
    * @param arcFull identifica si es agregado por archivo
    */
   private static void llamadoAltaCuentas(HttpServletRequest req, String sucursalOperante
           , Map<String, String> cuentas, String cuenta, BaseResource session, boolean arcFull) {
       EIGlobal.mensajePorTrace( "<---- ENTRA A  llamadoAltaCuentas  ----> "
               , EIGlobal.NivelLog.INFO);
       String[] detalleCta = cuentas.get(cuenta).split("[|]",-1);
          // Carga valores por defaultAltas
       Map<Integer, String> mapa = EnlaceMonitorPlusConstants.cargaValoresDefaultAltas(req, sucursalOperante,detalleCta[EnlaceMonitorPlusConstants.ALTA_CTAS_NUMCTA_ORIGEN], session);
       String error = mapa.get(90000);
       if(error != null && error != EnlaceMonitorPlusConstants.CADENA_VACIA){
            EIGlobal.mensajePorTrace(" llamadoAltaCuentas Trx390 no obtuvo datos no se infroma a monitor ", EIGlobal.NivelLog.DEBUG);
            
        }else{
        	if(ConectorMP.isInit()){
        if ("0".equals(detalleCta[0])) { // Mismo Banco
                   llenarDatosMB(arcFull, detalleCta, mapa);
               } else if ("1".equals(detalleCta[0])) { // Banco Nacional
                   llenarDatosBN(arcFull, detalleCta, mapa);  
               } else if ("2".equals(detalleCta[0])) { // Banco Internacional
                   llenarDatosBI(arcFull, detalleCta, mapa);
               } else if ("4".equals(detalleCta[0])) {//Cuentas Moviles
            	   EIGlobal.mensajePorTrace(" llamadoAltaCuentas MOVIL ", EIGlobal.NivelLog.DEBUG);
                   if ("BANME".equals(detalleCta[5])) { //mismo banco
                	   EIGlobal.mensajePorTrace(" MOVIL BN ", EIGlobal.NivelLog.DEBUG);
                       // Numero de Cuenta Origen 3540
                       mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI,
                               detalleCta[EnlaceMonitorPlusConstants.ALTA_CTAS_NUMCTA_ORIGEN]);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS,
                               EnlaceMonitorPlusConstants.TMB_BANCO_CTAAS);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_PAIS_CTAAS,
                               EnlaceMonitorPlusConstants.PAIS_BANC_CTAAS);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA,
                               EnlaceMonitorPlusConstants.DIVISA_PESOS);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA, 
                    		   EnlaceMonitorPlusConstants.ALCTAS_MOVIL_MB);
                       if (arcFull) {
                           mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
                                   EnlaceMonitorPlusConstants.IDEN_CON_ARCHIVO);
                       } else {
                           mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
                                   EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
                       }
                   } else {//interbancarias
                	   EIGlobal.mensajePorTrace(" MOVIL BN ", EIGlobal.NivelLog.DEBUG);
                       // Numero de Cuenta Origen 3540
                       mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI,
                               detalleCta[EnlaceMonitorPlusConstants.ALTA_CTAS_NUMCTA_ORIGEN]);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS,
                               detalleCta[6]);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_PAIS_CTAAS,
                               EnlaceMonitorPlusConstants.PAIS_BANC_CTAAS);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA,
                               EnlaceMonitorPlusConstants.DIVISA_PESOS);
                       mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA, 
                    		   EnlaceMonitorPlusConstants.ALCTAS_MOVIL_BN);
                       if (arcFull) {
                           mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
                                   EnlaceMonitorPlusConstants.IDEN_CON_ARCHIVO);
                       } else {
                           mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
                                   EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
                       }
                   }
               }
       
       // Default:  1, 2, 3, 4, 5, 6, 7, 9, 10, 3502, 3505, 3506, 3518, 3519, 11, 12
       // cargaValoresDefaultAltas: 8, 3501, 3503, 21, 3509, 3510, 3556, 3758, 3758, 3764, 3765, 3767, 3522      
       // llamadoAltaCuentas: 3540       
        
       EIGlobal.mensajePorTrace( "<---- SE ENVIA MENSAJE A MONITORPLUS  ----> "
               + " ** DE TIPO ** " + EnlaceMonitorPlusConstants.MSG_ADF310
               , EIGlobal.NivelLog.INFO);
       ConectorMP.enviaMsg(mapa, EnlaceMonitorPlusConstants.MSG_ADF310);
       ConectorMP.enviarInfo(mapa, cuenta, EnlaceMonitorPlusConstants.MSG_ADF310, session.getContractNumber());
         }else{
        	 EIGlobal.mensajePorTrace("El conector no esta inicializado, no se informa a MP", EIGlobal.NivelLog.DEBUG);
         }
        }
       }

   /**
    * Mapea campos para Mismo Banco
    * @param arcFull identifica si es agregado por archivo
    * @param detalleCta contiene informacion con el detalle de cuentas
    * @param mapa contiene los valores que se envian a MP
    */
   private static void llenarDatosMB(boolean arcFull, String[] detalleCta,
		Map<Integer, String> mapa) {
       // Numero de Cuenta Origen 3540
	   mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI,
	           detalleCta[EnlaceMonitorPlusConstants.ALTA_CTAS_NUMCTA_ORIGEN]);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS,
	           EnlaceMonitorPlusConstants.TMB_BANCO_CTAAS);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_PAIS_CTAAS,
	           EnlaceMonitorPlusConstants.PAIS_BANC_CTAAS);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA,
	           EnlaceMonitorPlusConstants.DIVISA_PESOS);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA, EnlaceMonitorPlusConstants.ALCTAS_MB);
	    if (arcFull) {
	        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
	                EnlaceMonitorPlusConstants.IDEN_CON_ARCHIVO);
	    }else{
	        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
	                EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
	    }
   }

   /**
    * Mapea campos para Banco Nacional
    * @param arcFull identifica si es agregado por archivo
    * @param detalleCta contiene informacion con el detalle de cuentas
    * @param mapa contiene los valores que se envian a MP
    */
   private static void llenarDatosBN(boolean arcFull, String[] detalleCta,
		Map<Integer, String> mapa) {
	// Numero de Cuenta Origen 3540
	   mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI,
	           detalleCta[EnlaceMonitorPlusConstants.ALTA_CTAS_NUMCTA_ORIGEN]);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_PAIS_CTAAS, 
	             EnlaceMonitorPlusConstants.PAIS_BANC_CTAAS);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA,
	            EnlaceMonitorPlusConstants.DIVISA_PESOS);
	   if(detalleCta[1].trim().length() == 18){
	   mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA,
	           EnlaceMonitorPlusConstants.ALCTAS_BN_CLABE);
	   }else{
	   mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA,
	           EnlaceMonitorPlusConstants.ALCTAS_BN_TARJ_DEBITO);    
	   }                    
	   if (arcFull) {
	        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
	                EnlaceMonitorPlusConstants.IDEN_CON_ARCHIVO);
            mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS, detalleCta[5]); 
	    }else{
	        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
	                EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
            mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS, detalleCta[6]); 
	   }
}

   /**
    * Mapea campos para Banco Internacional
    * @param arcFull identifica si es agregado por archivo
    * @param detalleCta contiene informacion con el detalle de cuentas
    * @param mapa contiene los valores que se envian a MP
    */
   private static void llenarDatosBI(boolean arcFull, String[] detalleCta,
		Map<Integer, String> mapa) {
	// Numero de Cuenta Origen 3540
	   mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI,
	           detalleCta[EnlaceMonitorPlusConstants.ALTA_CTAS_NUMCTA_ORIGEN]);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_PAIS_CTAAS,
	           detalleCta[3]);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA,
	           detalleCta[6]);
	   mapa.put(EnlaceMonitorPlusConstants.KEY_CVE_ABA_SWIFT,
	           detalleCta[9]); 
	   mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA, 
			   EnlaceMonitorPlusConstants.ALCTAS_BI);
	   if (arcFull) {
	        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
	                EnlaceMonitorPlusConstants.IDEN_CON_ARCHIVO);
            mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS, detalleCta[7]);
	    }else{
	        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO,
	                EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
            mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS, detalleCta[5]);
	    }
     }
   }
