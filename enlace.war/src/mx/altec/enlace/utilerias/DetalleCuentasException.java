package mx.altec.enlace.utilerias;

public class DetalleCuentasException extends Exception{
	
	private static final long serialVersionUID = 8868178121682526976L;
	private String codigo;
	
	public DetalleCuentasException(String mensaje){
		super(mensaje);
	}
	
	public DetalleCuentasException(String mensaje, String codigo){
		super(mensaje);
		this.codigo = codigo;
	}
	
	public String getCodigo(){
		return this.codigo;
	}

}
