// Decompiled by Jad v1.5.7d. Copyright 2000 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/SiliconValley/Bridge/8617/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MultipartParser.java

package mx.altec.enlace.utilerias;

import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.bo.BufferedServletInputStream;


// Referenced classes of package com.afina.servlet.multipart:
//            BufferedServletInputStream, FilePart, LimitedServletInputStream, ParamPart, 
//            Part

public class MultipartParser
{

    public MultipartParser(HttpServletRequest httpservletrequest, int i)
        throws IOException
    {
        this(httpservletrequest, i, true, true);
    }

    public MultipartParser(HttpServletRequest httpservletrequest, int i, boolean flag, boolean flag1)
        throws IOException
    {
        buf = new byte[8192];
        String s = null;
        String s1 = httpservletrequest.getHeader("Content-Type");
        String s2 = httpservletrequest.getContentType();
        if(s1 == null && s2 != null)
            s = s2;
        else
        if(s2 == null && s1 != null)
            s = s1;
        else
        if(s1 != null && s2 != null)
            s = s1.length() <= s2.length() ? s2 : s1;
        if(s == null || !s.toLowerCase().startsWith("multipart/form-data"))
            throw new IOException("Posted content type isn't multipart/form-data");
        int j = httpservletrequest.getContentLength();
        if(j > i)
            throw new IOException("Posted content length of " + j + " exceeds limit of " + i);
        String s3 = extractBoundary(s);
        if(s3 == null)
            throw new IOException("Separation boundary was not specified");
        Object obj = httpservletrequest.getInputStream();
        if(flag)
            obj = new BufferedServletInputStream(((ServletInputStream) (obj)));
        if(flag1)
            obj = new LimitedServletInputStream(((ServletInputStream) (obj)), j);
        in = ((ServletInputStream) (obj));
        boundary = s3;
        String s4 = readLine();
        if(s4 == null)
            throw new IOException("Corrupt form data: premature ending");
        if(!s4.startsWith(s3))
            throw new IOException("Corrupt form data: no leading boundary");
        else
            return;
    }

    private String extractBoundary(String s)
    {
        int i = s.lastIndexOf("boundary=");
        if(i == -1)
        {
            return null;
        } else
        {
            String s1 = s.substring(i + 9);
            s1 = "--" + s1;
            return s1;
        }
    }

    private String extractContentType(String s)
        throws IOException
    {
        String s1 = null;
        String s2 = s;
        s = s2.toLowerCase();
        if(s.startsWith("content-type"))
        {
            int i = s.indexOf(" ");
            if(i == -1)
                throw new IOException("Content type corrupt: " + s2);
            s1 = s.substring(i + 1);
        } else
        if(s.length() != 0)
            throw new IOException("Malformed line after disposition: " + s2);
        return s1;
    }

    private String[] extractDispositionInfo(String s)
        throws IOException
    {
        String as[] = new String[3];
        String s1 = s;
        s = s1.toLowerCase();
        int i = s.indexOf("content-disposition: ");
        int j = s.indexOf(";");
        if(i == -1 || j == -1)
            throw new IOException("Content disposition corrupt: " + s1);
        String s2 = s.substring(i + 21, j);
        if(!s2.equals("form-data"))
            throw new IOException("Invalid content disposition: " + s2);
        i = s.indexOf("name=\"", j);
        j = s.indexOf("\"", i + 7);
        if(i == -1 || j == -1)
            throw new IOException("Content disposition corrupt: " + s1);
        String s3 = s1.substring(i + 6, j);
        String s4 = null;
        i = s.indexOf("filename=\"", j + 2);
        j = s.indexOf("\"", i + 10);
        if(i != -1 && j != -1)
        {
            s4 = s1.substring(i + 10, j);
            int k = Math.max(s4.lastIndexOf(47), s4.lastIndexOf(92));
            if(k > -1)
                s4 = s4.substring(k + 1);
        }
        as[0] = s2;
        as[1] = s3;
        as[2] = s4;
        return as;
    }

    private String readLine()
        throws IOException
    {
        StringBuffer stringbuffer = new StringBuffer();
        int i;
        do
        {
            i = in.readLine(buf, 0, buf.length);
            if(i != -1)
                stringbuffer.append(new String(buf, 0, i, "ISO-8859-1"));
        } while(i == buf.length);
        if(stringbuffer.length() == 0)
        {
            return null;
        } else
        {
            stringbuffer.setLength(stringbuffer.length() - 2);
            return stringbuffer.toString();
        }
    }

    public Part readNextPart()
        throws IOException
    {
        if(lastFilePart != null)
        {
            lastFilePart.getInputStream().close();
            lastFilePart = null;
        }
        String s = readLine();
        if(s == null)
            return null;
        if(s.length() == 0)
            return null;
        String as[] = extractDispositionInfo(s);
        String s2 = as[1];
        String s3 = as[2];
        s = readLine();
        if(s == null)
            return null;
        String s4 = extractContentType(s);
        if(s4 != null)
        {
            String s1 = readLine();
            if(s1 == null || s1.length() > 0)
                throw new IOException("Malformed line after content type: " + s1);
        } else
        {
            s4 = "text/plain";
        }
        if(s3 == null)
            return new ParamPart(s2, in, boundary);
        if(s3.equals(""))
            s3 = null;
        lastFilePart = new FilePart(s2, in, boundary, s4, s3);
        return lastFilePart;
    }

    private ServletInputStream in;
    private String boundary;
    private FilePart lastFilePart;
    private byte buf[];
}
