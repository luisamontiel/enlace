package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmUsrGL35;

import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class AdmUsrGruposDAO extends AdmonUsuariosMQDAO {
	
	//TRANS: GL35
	private AdmUsrGL35 ejecutaGL35Base(String cveGrupo) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(cveGrupo, 2));					//CVE-GRUPO
		return consulta(AdmUsrGL35.HEADER, sb.toString(), AdmUsrGL35.getFactoryInstance());
	}
	
	public AdmUsrGL35 consultaGrupos(String cveGrupo) {
		logInfo("GL35 - Consulta de Grupos");
		return ejecutaGL35Base(cveGrupo);			
	}

}
