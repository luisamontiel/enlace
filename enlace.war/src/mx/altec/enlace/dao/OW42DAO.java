package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import javax.jms.JMSException;
import javax.naming.NamingException;

import mx.altec.enlace.beans.OW42Bean;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.Formateador;

public class OW42DAO {
	/** Id de la operacion exitosa **/
	private static final String OPERACION_EXITOSA = "OWA0008";
	/** No existen periodos **/
	private static final String NO_EXISTEN_PERIODOS = "OWA0007";
	/** Constante para log del sistema **/
	private static final String LOGTAG = "[EdcTdc:::OW42DAO]-";
	
	/**
	 * Ejecuta una consulta a la transaccion OW42
	 * @param numeroCuenta Numero de cuenta que se le pasa a la transaccion OW42
	 * @param numeroCliente Numero de cliente que se le pasa a la transaccion OW42
	 * @return Resultado obtenido de la ejecucion de la transaccion OW42	 
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public OW42Bean consultarOW42(String numeroCuenta,String numeroCliente) 
	throws BusinessException {
		EIGlobal.mensajePorTrace(LOGTAG+"::consultaOW42() Inicio", NivelLog.INFO);
		final OW42Bean ow42Bean = new OW42Bean();
		boolean ocurrioUnError = false;
		
		try {
			EIGlobal.mensajePorTrace(String.format("Numero de Cuenta [%s] Longitud [%s]",
					numeroCuenta,numeroCuenta.length()), NivelLog.INFO);
			ow42Bean.setNumeroCuenta(numeroCuenta);
			if (numeroCuenta.length()==16) {
				ow42Bean.setTipoEstadoCuenta("03");
				ow42Bean.setNumeroCliente(numeroCliente);
			}	
			final MQQueueSession ejecutor = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,NP_JNDI_QUEUE_RESPONSE,NP_JNDI_QUEUE_REQUEST);
			final String trama=ow42Bean.generarMensajeEntrada();
			final String respuesta = ejecutor.enviaRecibeMensaje(trama);
			EIGlobal.mensajePorTrace(String.format("%s::consultaOW42() Trama de envio::%s",LOGTAG, trama), NivelLog.INFO);
			EIGlobal.mensajePorTrace(String.format("%s::consultaOW42() Trama de respuesta::%s",LOGTAG, respuesta), NivelLog.INFO);
			final String[] respuestaSeparada = respuesta.split("@");
			final String codigoOperacion = respuestaSeparada[2].substring(2, 9);
			EIGlobal.mensajePorTrace(String.format("%s::consultaOW42() Codigo de Respuesta::[%s]",LOGTAG, codigoOperacion),NivelLog.INFO);
			if(OPERACION_EXITOSA.equals(codigoOperacion) || NO_EXISTEN_PERIODOS.equals(codigoOperacion)){
				ow42Bean.procesarMensajeRespuesta(respuestaSeparada);
			} else {
				throw new BusinessException("No fue posible obtener los periodos");
			}
			EIGlobal.mensajePorTrace(LOGTAG+"::consultaOW42() Fin", NivelLog.DEBUG);
		} catch (NamingException e) {
			EIGlobal.mensajePorTrace(LOGTAG+new Formateador().formatea(e), NivelLog.DEBUG);
			ocurrioUnError = true;
		} catch (JMSException e) {
			EIGlobal.mensajePorTrace(LOGTAG+new Formateador().formatea(e), NivelLog.DEBUG);
			ocurrioUnError = true;
		} catch (ValidationException e) {
			EIGlobal.mensajePorTrace(LOGTAG+new Formateador().formatea(e), NivelLog.DEBUG);
			ocurrioUnError = true;
		} catch (MQQueueSessionException e) {
			EIGlobal.mensajePorTrace(LOGTAG+new Formateador().formatea(e), NivelLog.DEBUG);
			ocurrioUnError = true;
		}
		if(ocurrioUnError){
			throw new BusinessException("SECHOW4");
		}
		return ow42Bean;
	}
}
