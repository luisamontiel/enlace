package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.*;
import mx.altec.enlace.beans.NomPreLM1I;

public class NomPreConfiguracionDAO extends NomPreMQDAO {
	
	//TRANS: LM1I
	public NomPreLM1I obtenerMontoBean(String codigoBean) {
		
		StringBuffer sb = new StringBuffer()
			.append("1")						//TIP-CONSULTA
			.append(rellenar(codigoBean, 6));	//COD-BIN
		
		logInfo("LM1I - Consulta beans");
		return consulta(NomPreLM1I.HEADER, sb.toString(), NomPreLM1I.getFactoryInstance());
	}	
	
public NomPreLM1I obtenerMontoDisp(String codigoBean) {
		
		StringBuffer sb = new StringBuffer()
			.append("2")						//TIP-CONSULTA
			.append(rellenar(codigoBean, 6));	//COD-BIN
		
		logInfo("LM1I - Consulta monto");
		return consulta(NomPreLM1I.HEADER, sb.toString(), NomPreLM1I.getFactoryInstance());
	}
}
