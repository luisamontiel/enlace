package mx.altec.enlace.dao;

import java.util.*;
import java.sql.*;

import javax.servlet.http.HttpServletRequest;
import javax.sql.*;
import javax.naming.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.servlets.coMtoProveedor;

import java.io.*;
import java.text.*;
import java.util.regex.*;

/**
 * @author abanda
 * FSW: Stefanini Mexico SA. de CV.
 *Fecha Modificaci�n: 2009/03/24
 *Se agregaron campos para manejo de divisas
 */
public class ProveedorConf implements java.io.Serializable
    {
    // ---
    public final static int FISICA = 1;
    public final static int MORAL = 2;
    public final static String NACIONAL = "N";
    public final static String INTERNACIONAL = "I";
    //Formas de pago
    public final static short FORMA_PAGO_CVE_MISMO = 1;
    public final static String FORMA_PAGO_DESC_MISMO = "Transferencia Mismo Banco";
    public final static short FORMA_PAGO_CVE_OTRO = 2;
    public final static String FORMA_PAGO_DESC_OTRO = "Transferencia Otros Bancos Nacionales";
    public final static short FORMA_PAGO_CVE_INT = 5;
    public final static String TRANSFERENCIA_NUMERO_MOVIL_MISMO_BANCO  = "Transferencia N&uacute;mero M&oacute;vil Mismo Banco";
    public final static String TRANSFERENCIA_NUMERO_MOVIL_OTROS_BANCOS_NACIONALES = "Transferencia N&uacute;mero M&oacute;vil Otros Bancos Nacionales";
    public final static String FORMA_PAGO_DESC_INT = "Transferencia Internacional";
    private static final String GET_TIPO_SOCIEDAD_BY_CODIGO_GLOBAL = "SELECT descripcion FROM comu_detalle_cd " +
    		"WHERE tipo_global='CTTSOC' AND codigo_global=";

    // ---
    public int tipo = 0;
    public String claveProveedor = "";
    public String codigoCliente = "";
    public String noTransmision = "";
    public String nombre = "";
    public String apellidoPaterno = "";
    public String apellidoMaterno = "";
    public String tipoSociedad = "";
    public String rfc = "";
    public String homoclave = "";
    public String contactoEmpresa = "";
    public String clienteFactoraje = "";
    public String calleNumero = "";
    public String colonia = "";
    public String delegMunicipio = "";
    public String cdPoblacion = "";
    public String cp = "";
    public String estado = "";
    public String pais = "";
    public String medioInformacion = "";
    public String email = "";
    public String ladaPrefijo = "";
    public String numeroTelefonico = "";
    public String extensionTel = "";
    public String fax = "";
    public String extensionFax = "";
    public String formaPago = "";
    public String cuentaCLABE = "";
    public String banco = "";
    public String sucursal = "";
    public String plaza = "";
    public String deudoresActivos = "";
    public String limiteFinanciamiento = "";
    public String volumenAnualVentas = "";
    public String importeMedioFacturasEmitidas = "";
//  Se agrego esto ------------------------------
    public String cveDivisa = "";
    public String descripcionCiudad = "";
    public String descripcionBanco = "";
    public String cveABA = "";
    public String cvePais = "";
    public String ctaBancoDestino = "";
//-----------------------------------------------
    public String claveStatus = "";
    public String status = "";
//  -----------------------------------------------
    public String canal = "";
    public String tipoAbono = "";
//	jgarcia - Origen proveedor
    public String origenProveedor = "";
//  -----------------------------------------------
//  Internacional
//  -----------------------------------------------
    public String datosAdic = "";
//  -----------------------------------------------
//  -----------------------------------------------
	/**
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * Fecha Modificaci�n: 2009/03/24
     * Se agregaron campos manejo de divisas
     * @return
     */
    // --- forma una l&iacute;nea del archivo de env&iacute;o
    public String tramaEnvio()
        {
        String trama = origenProveedor + ";" + claveProveedor + ";" + tipoSociedad + ";" + contactoEmpresa + ";" + clienteFactoraje + ";" +
            medioInformacion + ";" + formaPago + ";" + cuentaCLABE + ";" + banco + ";" + sucursal + ";" +
            plaza + ";" + limiteFinanciamiento + ";" + deudoresActivos + ";" + volumenAnualVentas + ";" +
            importeMedioFacturasEmitidas + ";" + ((tipo==ProveedorConf.MORAL)?"M":"F") + ";" +
            apellidoPaterno + ";" + apellidoMaterno + ";" + nombre + ";" + rfc + ";" + homoclave + ";" +
            calleNumero + ";" + colonia + ";" + (origenProveedor.equals(INTERNACIONAL)?estado:delegMunicipio) + ";" + cdPoblacion + ";" + cp + ";" +
            (origenProveedor.equals(INTERNACIONAL)?"":estado) + ";" + pais + ";" + ladaPrefijo + ";" + numeroTelefonico + ";" + extensionTel + ";" +
            fax + ";" + extensionFax + ";" + email + ";" + cveDivisa + ";" + descripcionCiudad + ";" +
            descripcionBanco + ";" + cveABA + ";" + ctaBancoDestino + ";" + cvePais + ";;" + tipoAbono + ";" +
            (origenProveedor.equals(INTERNACIONAL)?datosAdic+";":"");
        System.out.println("INICIA LA TRAMA DAG");
        System.out.println("["+trama+"]");
        System.out.println("TERMINA LA TRAMA DAG");
        return trama;
        }

    /**
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * Fecha Modificaci�n: 2009/03/24
     * Se agregaron campos modificaci�n
     * @param linea
     * @return
     */
    // ---
    public boolean leeLineaRecuperacion(String linea)
        {
        int pos;
        boolean resultado=true;
        while((pos = linea.indexOf(";;")) != -1) linea = linea.substring(0,pos+1) + " " + linea.substring(pos+1);
        try
            {
            StringTokenizer tokens = new StringTokenizer(linea,";");
            noTransmision = tokens.nextToken();
            codigoCliente = tokens.nextToken();
            claveProveedor = tokens.nextToken();
            tipoSociedad = tokens.nextToken();
            contactoEmpresa = tokens.nextToken();
            clienteFactoraje = tokens.nextToken();
            medioInformacion = tokens.nextToken();
            formaPago = tokens.nextToken();
            cuentaCLABE = tokens.nextToken();
            banco = tokens.nextToken();
            sucursal = tokens.nextToken();
            plaza = tokens.nextToken();
            limiteFinanciamiento = tokens.nextToken();
            deudoresActivos = tokens.nextToken();
            volumenAnualVentas = tokens.nextToken();
            importeMedioFacturasEmitidas = tokens.nextToken();
            tipo = (tokens.nextToken().equals("M")?ProveedorConf.MORAL:ProveedorConf.FISICA);
            apellidoPaterno = tokens.nextToken();
            apellidoMaterno = tokens.nextToken();
            nombre = tokens.nextToken();
            rfc = tokens.nextToken();
            homoclave = tokens.nextToken();
            calleNumero = tokens.nextToken();
            colonia = tokens.nextToken();
            delegMunicipio = tokens.nextToken();
            cdPoblacion = tokens.nextToken();
            cp = tokens.nextToken();
            estado = tokens.nextToken().trim();
            pais = tokens.nextToken();
            ladaPrefijo = tokens.nextToken();
            numeroTelefonico = tokens.nextToken();
            extensionTel = tokens.nextToken();
            fax = tokens.nextToken();
            extensionFax = tokens.nextToken();
            email = tokens.nextToken();
            status = tokens.nextToken();
            claveStatus = tokens.nextToken();
            cveDivisa = tokens.nextToken();
            descripcionCiudad = tokens.nextToken();
            descripcionBanco = tokens.nextToken();
            cveABA = tokens.nextToken();
            ctaBancoDestino = tokens.nextToken();
            /*
             * jgarcia - 01/Oct/2009
             * Se agrega lectura de clave pais, tipo de abono, origen de proveedor y
             * los datos adicionales para el caso de proveedores internacionales.
             */
            cvePais = tokens.nextToken();
        	datosAdic = tokens.nextToken();
        	tipoAbono = tokens.nextToken();
            origenProveedor = tokens.nextToken();
            if(INTERNACIONAL.equalsIgnoreCase(origenProveedor)){
            	estado = delegMunicipio;
            	delegMunicipio = "";
            }
            }
        catch(Exception e)
            {resultado = false;}
        limpiaEspacios();
        return resultado;
        }

    // ---
    /**
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * fecha modificacion 2009/03/23
     */
    public boolean leeLineaConsulta(String linea)
        {
        int pos;
        boolean resultado=true;
        while((pos = linea.indexOf("||")) != -1) linea = linea.substring(0,pos+1) + " " + linea.substring(pos+1);

        try
            {
            StringTokenizer tokens = new StringTokenizer(linea,"|");
            tokens.nextToken();
        	noTransmision = tokens.nextToken();
        	codigoCliente = tokens.nextToken();
        	claveProveedor = tokens.nextToken();
        	//tipoSociedad = rellena(tokens.nextToken(),8,' ',false);
        	tipoSociedad = tokens.nextToken();
        	contactoEmpresa = tokens.nextToken();
        	clienteFactoraje = tokens.nextToken();
        	medioInformacion = tokens.nextToken();
        	formaPago = tokens.nextToken();
        	cuentaCLABE = tokens.nextToken();
        	banco = rellena(tokens.nextToken(),5,' ',false);
        	sucursal = tokens.nextToken();
        	plaza = rellena(tokens.nextToken(),5,'0',true);
        	limiteFinanciamiento = corrigeMonto(tokens.nextToken());
        	deudoresActivos = tokens.nextToken();
        	volumenAnualVentas = corrigeMonto(tokens.nextToken());
        	importeMedioFacturasEmitidas = corrigeMonto(tokens.nextToken());
        	tipo = (tokens.nextToken().equals("M")?ProveedorConf.MORAL:ProveedorConf.FISICA);
        	apellidoPaterno = tokens.nextToken();
        	apellidoMaterno = tokens.nextToken();
        	nombre = tokens.nextToken();
        	rfc = tokens.nextToken();
        	homoclave = tokens.nextToken();
        	calleNumero = tokens.nextToken();
        	colonia = tokens.nextToken();
        	delegMunicipio = tokens.nextToken();
        	cdPoblacion = tokens.nextToken();
        	cp = tokens.nextToken();
        	estado = rellena(tokens.nextToken().trim(),4,' ',false);
        	pais = rellena(tokens.nextToken(),4,' ',false);
        	ladaPrefijo = tokens.nextToken();
        	numeroTelefonico = tokens.nextToken();
        	extensionTel = tokens.nextToken();
        	fax = tokens.nextToken();
        	extensionFax = tokens.nextToken();
        	email = tokens.nextToken();
        	status = tokens.nextToken();
        	claveStatus = tokens.nextToken();
        	tokens.nextToken();
        	cveDivisa = tokens.nextToken();
        	cveABA = tokens.nextToken();
        	descripcionCiudad = tokens.nextToken();
        	descripcionBanco = tokens.nextToken();
        	ctaBancoDestino = tokens.nextToken();
        	canal = tokens.nextToken();
        	cvePais = tokens.nextToken();
        	/*
        	 * jgarcia - 01/Oct/2009
        	 * Se agrega lectura de tipo de abono, origen de proveedor y
        	 * los datos adicionales para el caso de proveedores internacionales.
        	 */
        	datosAdic = tokens.nextToken();
        	tipoAbono = tokens.nextToken();
            origenProveedor = tokens.nextToken();
            if(INTERNACIONAL.equalsIgnoreCase(origenProveedor)){
            	estado = delegMunicipio;
            	delegMunicipio = "";            	
            }else if(NACIONAL.equalsIgnoreCase(origenProveedor)){
            	tipoSociedad = rellena(tipoSociedad,8,' ',false);
            }
        }
        catch(Exception e)
            {resultado = false;}
        limpiaEspacios();
        return resultado;
        }

    // --
    public Vector leeLineaImportacion(String linea)
        {
        return leeLineaImportacion(linea, null, null);
        }

    // --
    /**
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * fecha modificacion 2009/03/23
     */
    public Vector leeLineaImportacion(String linea, Hashtable clavesBanco, String vTProv)
        {
        //F&iacute;sicas: ancho de l&iacute;nea: 681
        //Morales: ancho de l&iacute;nea: 631
    	Vector errores = new Vector();
        int a;
        if(!linea.substring(0,1).equals("F") && !linea.substring(0,1).equals("M"))
            {
            errores.add("El formato de la l&iacute;nea no es el correcto");
            return errores;
            }
        if((linea.length()==681 && linea.substring(0,1).equals("F")) ||
        		(linea.length()==631 && linea.substring(0,1).equals("M"))) {
        	errores = leeLineaImportacionSDivisa(linea, clavesBanco);
        }
        else if ((linea.length()== 803 && linea.substring(0,1).equals("F")) ||
        		(linea.length()==803 && linea.substring(0,1).equals("M"))) {
        	/* DAG 061009 Se valida el tipo de proveedor para ejecutar diferentes metodos*/
        	if(vTProv.equalsIgnoreCase(INTERNACIONAL))
        		errores = leeLineaImportacionCDivisaInter(linea, clavesBanco);
        	else
        		errores = leeLineaImportacionCDivisa(linea, clavesBanco);
        }
        else
        	errores.add("El largo de la l&iacute;nea no es el correcto");
        return errores;
        }

    /**
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * fecha creacion 2009/03/23
     */
    public Vector leeLineaImportacionSDivisa(String linea, Hashtable clavesBanco) {

    	Vector errores = new Vector();
        int a;

        origenProveedor = NACIONAL;
        cveDivisa = "MXN";
    	if(linea.substring(0,1).equals("F")) tipo = FISICA; else tipo = MORAL;

    	if(tipo == FISICA)
            {
            claveProveedor = linea.substring(1,21).trim();
            nombre = linea.substring(81,161).trim();
            apellidoPaterno = linea.substring(21,51).trim();
            apellidoMaterno = linea.substring(51,81).trim();
            rfc = linea.substring(161,171).trim();
            homoclave = linea.substring(171,174).trim();
            contactoEmpresa = linea.substring(174,254).trim();
            clienteFactoraje = linea.substring(254,262).trim();
            calleNumero = linea.substring(262,322).trim();
            colonia = linea.substring(322,352).trim();
            delegMunicipio = linea.substring(352,387).trim();
            cdPoblacion = linea.substring(387,422).trim();
            cp = linea.substring(422,427).trim();
            estado = linea.substring(427,431).trim();
            pais = linea.substring(431,435).trim();
            medioInformacion = linea.substring(435,438).trim();
            email = linea.substring(438,498).trim();
            ladaPrefijo = linea.substring(498,510).trim();
            numeroTelefonico = linea.substring(510,522).trim();
            extensionTel = linea.substring(522,534).trim();
            fax = linea.substring(534,546).trim();
            extensionFax = linea.substring(546,558).trim();
            formaPago = linea.substring(558,561).trim();
            cuentaCLABE = linea.substring(561,581).trim();
            banco = linea.substring(581,586).trim();
            sucursal = linea.substring(586,590).trim();
            plaza = linea.substring(590,595).trim();
            deudoresActivos = linea.substring(616,621).trim();
            limiteFinanciamiento = linea.substring(595,616).trim();
            volumenAnualVentas = linea.substring(621,642).trim();
            importeMedioFacturasEmitidas = linea.substring(642,663).trim();
            }
        else
            {
            claveProveedor = linea.substring(1,21).trim();
            nombre = linea.substring(21,101).trim();
            tipoSociedad = linea.substring(101,111).trim();
            rfc = linea.substring(111,121).trim();
            homoclave = linea.substring(121,124).trim();
            contactoEmpresa = linea.substring(124,204).trim();
            clienteFactoraje = linea.substring(204,212).trim();
            calleNumero = linea.substring(212,272).trim();
            colonia = linea.substring(272,302).trim();
            delegMunicipio = linea.substring(302,337).trim();
            cdPoblacion = linea.substring(337,372).trim();
            cp = linea.substring(372,377).trim();
            estado = linea.substring(377,381).trim();
            pais = linea.substring(381,385).trim();
            medioInformacion = linea.substring(385,388).trim();
            email = linea.substring(388,448).trim();
            ladaPrefijo = linea.substring(448,460).trim();
            numeroTelefonico = linea.substring(460,472).trim();
            extensionTel = linea.substring(472,484).trim();
            fax = linea.substring(484,496).trim();
            extensionFax = linea.substring(496,508).trim();
            formaPago = linea.substring(508,511).trim();
            cuentaCLABE = linea.substring(511,531).trim();
            banco = linea.substring(531,536).trim();
            sucursal = linea.substring(536,540).trim();
            plaza = linea.substring(540,545).trim();
            deudoresActivos = linea.substring(566,571).trim();
            limiteFinanciamiento = linea.substring(545,566).trim();
            volumenAnualVentas = linea.substring(571,592).trim();
            importeMedioFacturasEmitidas = linea.substring(592,613).trim();
            }
    	// Tipo de abono por default para proveedores nacionales moneda pesos
    	tipoAbono = "01";

        // clave de proveedor
        if(claveProveedor.equals(""))
            errores.add("El campo clave de proveedor es obligatorio");
        else if(contieneCaracteresNoValidosEsp(claveProveedor))
            errores.add("El campo clave de proveedor contiene acentos o caracteres no v&aacute;lidos: " + claveProveedor);

        // c&oacute;digo de cliente
        if(contieneCaracteresNoValidos(codigoCliente))
            errores.add("El campo c&oacute;digo de cliente contiene acentos o caracteres no v&aacute;lidos: " + codigoCliente);

        // num Transmisi&oacute;n
        if(contieneCaracteresNoValidos(noTransmision))
            errores.add("El campo num. de transmisi&oacute;n contiene acentos o caracteres no v&aacute;lidos: " + noTransmision);

        // nombre o raz&oacute;n social
        if(nombre.equals(""))
            errores.add("El campo " + ((tipo == FISICA)?"nombre":"raz&oacute;n social") + " es obligatorio");
        else if(contieneCaracteresNoValidosEsp2(nombre))
            errores.add("El campo nombre contiene acentos o caracteres no v&aacute;lidos: " + nombre);

        // ap Paterno
        if(tipo == FISICA && apellidoPaterno.equals(""))
            errores.add("El campo apellido paterno es obligatorio");
        else if(tipo == FISICA && contieneCaracteresNoValidosEsp2(apellidoPaterno))
            errores.add("El campo apellido paterno contiene acentos o caracteres no v&aacute;lidos: " + apellidoPaterno);

        // ap Materno
        if(tipo == FISICA && contieneCaracteresNoValidosEsp2(apellidoMaterno))
            errores.add("El campo apellido materno contiene acentos o caracteres no v&aacute;lidos: " + apellidoMaterno);

        // tipo de sociedad
        if(tipo == MORAL && tipoSociedad.equals(""))
            errores.add("El campo tipo de sociedad es obligatorio");
        else if(tipo == MORAL && contieneCaracteresNoValidos(tipoSociedad))
            errores.add("El campo tipo de sociedad contiene acentos o caracteres no v&aacute;lidos: " + tipoSociedad);

        // rfc
        if(rfc.equals(""))
            errores.add("El campo RFC es obligatorio");
        else if(!validaRFC())
            errores.add("El campo RFC no es v&aacute;lido: " + rfc);


        // homoclave
        if(homoclave.equals(""))
            errores.add("El campo homoclave es obligatorio");
        else if(contieneCaracteresNoValidos(homoclave))
            errores.add("El campo homoclave contiene acentos o caracteres no v&aacute;lidos: " + homoclave);

        // contacto empresa
        if(contieneCaracteresNoValidos(contactoEmpresa))
            errores.add("El campo contacto en la empresa contiene acentos o caracteres no v&aacute;lidos: " + contactoEmpresa);

        // cliente factoraje
        try{if(!clienteFactoraje.equals("")) Integer.parseInt(clienteFactoraje);} catch(Exception e)
            {errores.add("El campo de n&uacute;mero de cliente para factoraje debe ser num&eacute;rico: " + clienteFactoraje);}

        // calle y n&uacute;mero
        if(calleNumero.equals(""))
            errores.add("El campo calle y n&uacute;mero es obligatorio");
        else if(contieneCaracteresNoValidos(calleNumero))
            errores.add("El campo calle y n&uacute;mero contiene acentos o caracteres no v&aacute;lidos: " + calleNumero);

        // colonia
        if(colonia.equals(""))
            errores.add("El campo colonia es obligatorio");
        else if(contieneCaracteresNoValidos(colonia))
            errores.add("El campo colonia contiene acentos o caracteres no v&aacute;lidos: " + colonia);

        // delegaci&oacute;n o municipio
        if(delegMunicipio.equals(""))
            errores.add("El campo delegaci&oacute;n o municipio es obligatorio");
        if(contieneCaracteresNoValidos(delegMunicipio))
            errores.add("El campo deleg. o municipio contiene acentos o caracteres no v&aacute;lidos: " + delegMunicipio);

        // ciudad o poblaci&oacute;n
        if(cdPoblacion.equals(""))
            errores.add("El campo ciudad o poblaci&oacute;n es obligatorio");
        if(contieneCaracteresNoValidos(cdPoblacion))
            errores.add("El campo Cd. o poblaci&oacute;n contiene acentos o caracteres no v&aacute;lidos: " + cdPoblacion);

        // c&oacute;digo postal
        if(cp.equals(""))
            errores.add("El c&oacute;digo postal es obligatorio");
        else if(cp.length()<5)
            errores.add("El c&oacute;digo postal debe contener cinco d&iacute;gitos");
        else try{Integer.parseInt(cp);} catch(Exception e)
            {errores.add("El c&oacute;digo postal debe ser num&eacute;rico: " + cp);}

        // estado
        if(estado.trim().equals(""))
            errores.add("El campo estado es obligatorio");

        // pais
        if(pais.trim().equals(""))
            errores.add("El campo pais es obligatorio");

        // medio de informaci&oacute;n
        if(medioInformacion.trim().equals(""))
            errores.add("El campo medio de informaci&oacute;n es obligatorio");
        else try{Integer.parseInt(medioInformacion.trim());} catch(Exception e)
            {errores.add("La clave de medio de informaci&oacute;n debe ser num&eacute;rica: " + medioInformacion);}

        // email
        if(!validaEmail())
            {errores.add("El email no es v&aacute;lido: " + email);}

        // lada o prefijo
        if(esNumerico(ladaPrefijo))
            errores.add("El campo lada o prefijo contiene acentos o caracteres no v&aacute;lidos: " + ladaPrefijo);

        // tel&eacute;fonno
        if(numeroTelefonico.equals(""))
            errores.add("El campo tel&eacute;fono es obligatorio");
        else if(contieneCaracteresNoValidos(numeroTelefonico))
            errores.add("El campo tel&eacute;fono contiene acentos o caracteres no v&aacute;lidos: " + numeroTelefonico);
		/*MSD Q05-29284*/
		if(ladaPrefijo.trim().length() + numeroTelefonico.trim().length() != 10)
			errores.add("La longitud de la clave lada m&aacute;s el n&uacute;mero telef&oacute;nico deben sumar 10 d&iacute;gitos.");
		if(ladaPrefijo.trim().length() != 0 && ladaPrefijo.trim().charAt(0) == '0')
			errores.add("La clave de larga distancia no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01)");
		if(numeroTelefonico.trim().length() != 0 && numeroTelefonico.trim().charAt(0) == '0')
			errores.add("El n&uacute;mero telef&oacute;nico no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01) ni de larga distancia");
		/*MSD Q05-29284*/



        // extensi&oacute;n
        if(contieneCaracteresNoValidos(extensionTel))
            errores.add("El campo extensi&oacute;n telef&oacute;nica contiene acentos o caracteres no v&aacute;lidos: " + extensionTel);

        // fax
        if(esNumerico(fax))
            errores.add("El campo fax contiene acentos o caracteres no v&aacute;lidos: " + fax);

		EIGlobal.mensajePorTrace("La forma de pago es: " +  formaPago,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("LA cuenta CLABE es: " + cuentaCLABE.trim(),EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("El BANCO        es: " + banco,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("La clave del BANCO: " + traeClaveBanco(banco),EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("PASO 1",EIGlobal.NivelLog.DEBUG);
        // forma de pago
        if(formaPago.trim().equals(""))
            errores.add("El campo forma de pago es obligatorio");
        else if(!formaPago.trim().equals("001") && !formaPago.trim().equals("002"))
        	errores.add("La forma de pago no es valida");
        else try{Integer.parseInt(formaPago.trim());} catch(Exception e)
            {errores.add("La clave de forma de pago debe ser num&eacute;rica: " + formaPago);}

        // cuenta CLABE
        try {formaPago = "" + Integer.parseInt(formaPago);} catch(Exception e)
            {errores.add("La clave de forma de pago no es v&aacute;lida"); formaPago="";}
        boolean flag = false;
        try{Long.parseLong(cuentaCLABE.trim());} catch(Exception e)
        {flag = true;}
        if(cuentaCLABE.equals(""))
            errores.add("El campo cuenta es obligatorio");
        //Inicia Modificaci&oacute;n PVA 04032003
        else if (flag)
        	errores.add("El campo cuenta debe ser num&eacute;rico: " + cuentaCLABE);
        else if ((formaPago.equals("2"))&&(cuentaCLABE.trim().length()!=18))
            errores.add("La longitud de la cuenta/CLABE debe ser de 18 d&iacute;gitos: "+cuentaCLABE);
        else if ((formaPago.equals("1"))&&(cuentaCLABE.trim().length()!=11) &&(cuentaCLABE.trim().length()!=18))
            errores.add("La longitud de la cuenta debe ser de 11 &oacute; 18 d&iacute;gitos: "    +cuentaCLABE);
        else if ((cuentaCLABE.trim().length()==18)&& !(validaBancoCLABE(cuentaCLABE.trim(),traeClaveBanco(banco))))
			errores.add("La clave del banco no coincide con la cuenta CLABE " + cuentaCLABE);
		else if ((formaPago.equals("1") || formaPago.equals("2")) && (cuentaCLABE.trim().length()==18) && !(validaDigitoVerificador(cuentaCLABE.trim())))
			errores.add("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido: " + cuentaCLABE);
        else if ((formaPago.equals("1")) && (cuentaCLABE.trim().length()==11)&&(!validaDigitoCuenta()))
            errores.add("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido: " + cuentaCLABE);

		String prefijo="";
		if(!cuentaCLABE.equals("")){
			if(cuentaCLABE.length()==11)
			  prefijo=cuentaCLABE.substring(0, 2);
			else
			  prefijo=cuentaCLABE.substring(6,8);
			if(!formaPago.equals("2") && (prefijo.equals("09") || prefijo.equals("42") || prefijo.equals("45") ||
										  prefijo.equals("46") || prefijo.equals("48") || prefijo.equals("49") ||
										  prefijo.equals("82") || prefijo.equals("83") || prefijo.equals("97") ||
										  prefijo.equals("98")) )
	            errores.add("Solo se permite registrar cuentas internas en Moneda Nacional (Pesos Mexicanos).");



	        //termina Modificacion PVA
	        else if(formaPago.length()>0 && formaPago.equals("2")
	            && clavesBanco != null)
	            {
	            String clave = cuentaCLABE.substring(0, 3);
	            while(clave.substring(0,1).equals("0") && clave.length()>1) clave = clave.substring(1);
	            if(clavesBanco.get(clave) == null)
	                errores.add("La cuenta no coincide con ningun banco");
	            else if(!((String)clavesBanco.get(clave)).equals(banco))
	                errores.add("La cuenta no coincide con el banco");
	            }
		}

        //banco
        if(clavesBanco != null && !banco.trim().equals("") && !clavesBanco.containsValue(banco))
            errores.add("la clave de banco no es v&aacute;lida");

        //sucursal
        try{if(!sucursal.equals("")) Integer.parseInt(sucursal);} catch(Exception e)
            {errores.add("El campo sucursal debe ser num&eacute;rico: " + sucursal);}

        //plaza
        if(clavesBanco != null && !plaza.trim().equals("") && clavesBanco.get("plazas") != null &&
            ((Hashtable)clavesBanco.get("plazas")).get(plaza) == null)
            errores.add("la clave de plaza no es v&aacute;lida");

        // deudores activos
        try{if(!deudoresActivos.equals("")) Integer.parseInt(deudoresActivos);} catch(Exception e)
            {errores.add("El campo de n&uacute;mero de deudores activos debe ser num&eacute;rico: " + deudoresActivos);}

        // l&iacute;mite de financiamiento
        try{if(!limiteFinanciamiento.equals("")) Long.parseLong(limiteFinanciamiento);} catch(Exception e)
            {errores.add("El l&iacute;mite de financiamiento debe ser num&eacute;rico sin punto decimal: " + limiteFinanciamiento);}

        // volumen anual ventas
        try{if(!volumenAnualVentas.equals("")) Long.parseLong(volumenAnualVentas);} catch(Exception e)
            {errores.add("El volumen anual de ventas debe ser num&eacute;rico sin punto decimal: " + volumenAnualVentas);}

        // importe medio facturas emitidas
        try {if(!importeMedioFacturasEmitidas.equals("")) Long.parseLong(importeMedioFacturasEmitidas);} catch(Exception e)
            {errores.add("El importe medio de facturas emitidas debe ser num&eacute;rico sin punto decimal: " + importeMedioFacturasEmitidas);}

        // relaci&oacute;n entre campos
        try
            {
            // medio de informaci&oacute;n
            a = Integer.parseInt(medioInformacion.trim());
            if(a!=1 && a!=2)
            	errores.add("La clave de confirmaci&oacute;n es incorrecta");
            if(a == 1 && email.trim().equals(""))
                errores.add("Se ha especificado el e-mail como medio de confirmaci&oacute;n, pero no se ha proporcionado uno");
            if(a == 2 && fax.trim().equals(""))
                errores.add("Se ha especificado el fax como medio de confirmaci&oacute;n, pero no se ha proporcionado uno");
            if(a == 2 && ladaPrefijo.trim().equals(""))
                errores.add("Se ha especificado el fax como medio de confirmaci&oacute;n, pero no se ha proporcionado una clave lada");
            /*MSD Q05-29284*/
            if(a == 2 && (ladaPrefijo.trim().length() + fax.trim().length()) != 10)
                errores.add("La longitud de la clave lada m&aacute;s el fax deben sumar 10 d&iacute;gitos.");
            if(a == 2 && (ladaPrefijo.trim().length() != 0 && ladaPrefijo.trim().charAt(0) == '0'))
                errores.add("La clave de larga distancia no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01)");
            if(a == 2 && (fax.trim().trim().length() != 0 && fax.trim().trim().charAt(0) == '0'))
                errores.add("El n&uacute;mero de FAX no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01) ni de larga distancia");
			/*MSD Q05-29284*/

            // forma de pago
            a = Integer.parseInt(formaPago.trim());
            if(a == 1 && !banco.equals("BANME") && !banco.equals("SERFI") && !banco.trim().equals(""))
                errores.add("Se ha especificado un banco distinto a Santander o Serf&iacute;n con una forma de pago interna");
            if(a == 2 && (banco.equals("BANME") || banco.equals("SERFI")))
                errores.add("Se ha especificado forma de pago interbancaria, pero se ha especificado como banco a Santander o Serf&iacute;n");
            if(a == 2 && (banco.trim().equals("") || banco.trim().equals("0")))
                errores.add("Se ha especificado forma de pago interbancaria, pero no se ha proporcionado un banco");
            //if(a == 2 && (sucursal.equals("") || sucursal.equals("0") || sucursal.equals("00000")))
                //errores.add("Se ha especificado forma de pago interbancaria, pero no se ha proporcionado una sucursal");
            //if(a == 2 && (plaza.trim().equals("") || plaza.trim().equals("0")))
                //errores.add("Se ha especificado forma de pago interbancaria, pero no se ha proporcionado una plaza");
            }
        catch(Exception e)
            {;}

        limiteFinanciamiento = colocaPuntoDecimal(limiteFinanciamiento);
        volumenAnualVentas = colocaPuntoDecimal(volumenAnualVentas);
        importeMedioFacturasEmitidas = colocaPuntoDecimal(importeMedioFacturasEmitidas);

        return errores;
        }

    ////////////////////////////////////////////////////////////////////////////////////

    /**
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * fecha creacion 2009/03/23
     */
public Vector leeLineaImportacionCDivisa(String linea, Hashtable clavesBanco) {
		System.out.println("ENTRO A LEELINEAIMPORTACIONCDIVISA");
    	Vector errores = new Vector();
        int a;
        String[] ABA = null;
        coMtoProveedor provee = new coMtoProveedor();

        // Variable para determinar si aplica la validaci�n del campo, cuando se tenga lo siguiente
        //  * Forma de Pago: 001
        //  * Forma de Pago: 002 y Divisa MXN
        // Los campos siguientes no se tomaran en cuenta
        // * cvePais
        // * ctaBancoDestino
        // * cveABA
        // * descripcionBanco
        // * descripcionCiudad
        boolean validaCampo = true;

    	if(linea.substring(0,1).equals("F")) tipo = FISICA; else tipo = MORAL;
    	origenProveedor = NACIONAL; //DAG 250909 Se agrega campo para indicar que el tipo de registro es Nacional
    	System.out.println("LA LINEA ES: "+ tipo);
    	if(tipo == FISICA)
            {
            claveProveedor = linea.substring(1,21).trim();
            nombre = linea.substring(81,161).trim();
            apellidoPaterno = linea.substring(21,51).trim();
            apellidoMaterno = linea.substring(51,81).trim();
            rfc = linea.substring(161,171).trim();
            homoclave = linea.substring(171,174).trim();
            contactoEmpresa = linea.substring(174,254).trim();
            clienteFactoraje = linea.substring(254,262).trim();
            calleNumero = linea.substring(262,322).trim();
            colonia = linea.substring(322,352).trim();
            delegMunicipio = linea.substring(352,387).trim();
            cdPoblacion = linea.substring(387,422).trim();
            cp = linea.substring(422,427).trim();
            estado = linea.substring(427,431).trim();
            pais = linea.substring(431,435).trim();
            medioInformacion = linea.substring(435,438).trim();
            email = linea.substring(438,498).trim();
            ladaPrefijo = linea.substring(498,510).trim();
            numeroTelefonico = linea.substring(510,522).trim();
            extensionTel = linea.substring(522,534).trim();
            fax = linea.substring(534,546).trim();
            extensionFax = linea.substring(546,558).trim();
            formaPago = linea.substring(558,561).trim();
            cuentaCLABE = linea.substring(561,581).trim();
            banco = linea.substring(581,586).trim();
            sucursal = linea.substring(586,590).trim();
            plaza = linea.substring(590,595).trim();
            deudoresActivos = linea.substring(616,621).trim();
            limiteFinanciamiento = linea.substring(595,616).trim();
            volumenAnualVentas = linea.substring(621,642).trim();
            importeMedioFacturasEmitidas = linea.substring(642,663).trim();
            cveDivisa = linea.substring(681, 685).trim();
            cvePais = linea.substring(685, 689).trim();
            ctaBancoDestino = linea.substring(689, 709).trim();
            cveABA = linea.substring(709, 721).trim();
            descripcionBanco = linea.substring(721, 761).trim();
            descripcionCiudad = linea.substring(761, 801).trim();
            tipoAbono = linea.substring(801, 803).trim(); //DAG 250909 Se agrega campo segun definicion de layout
            }
        else
            {
            claveProveedor = linea.substring(1,21).trim();
            nombre = linea.substring(21,101).trim();
            tipoSociedad = linea.substring(101,161).trim(); // DAG 250909 Se aumenta el tama�o a 60 caracteres y esto afecta todo el layout
            rfc = linea.substring(161,171).trim();
            homoclave = linea.substring(171,174).trim();
            contactoEmpresa = linea.substring(174,254).trim();
            clienteFactoraje = linea.substring(254,262).trim();
            calleNumero = linea.substring(262,322).trim();
            colonia = linea.substring(322,352).trim();
            delegMunicipio = linea.substring(352,387).trim();
            cdPoblacion = linea.substring(387,422).trim();
            cp = linea.substring(422,427).trim();
            estado = linea.substring(427,431).trim();
            pais = linea.substring(431,435).trim();
            medioInformacion = linea.substring(435,438).trim();
            email = linea.substring(438,498).trim();
            ladaPrefijo = linea.substring(498,510).trim();
            numeroTelefonico = linea.substring(510,522).trim();
            extensionTel = linea.substring(522,534).trim();
            fax = linea.substring(534,546).trim();
            extensionFax = linea.substring(546,558).trim();
            formaPago = linea.substring(558,561).trim();
            cuentaCLABE = linea.substring(561,581).trim();
            banco = linea.substring(581,586).trim();
            sucursal = linea.substring(586,590).trim();
            plaza = linea.substring(590,595).trim();
            deudoresActivos = linea.substring(616,621).trim();
            limiteFinanciamiento = linea.substring(595,616).trim();
            volumenAnualVentas = linea.substring(621,642).trim();
            importeMedioFacturasEmitidas = linea.substring(642,663).trim();
            cveDivisa = linea.substring(681, 685).trim();
	        cvePais = linea.substring(685, 689).trim();
	        ctaBancoDestino = linea.substring(689, 709).trim();
	        cveABA = linea.substring(709, 721).trim();
	        descripcionBanco = linea.substring(721, 761).trim();
	        descripcionCiudad = linea.substring(761, 801).trim();
            tipoAbono = linea.substring(801, 803).trim(); //DAG 250909 Se agrega campo segun definicion de layout
          }

    	EIGlobal.mensajePorTrace("Entro a Importaci�n con DIVISA",EIGlobal.NivelLog.DEBUG);
        // clave de proveedor
        if(claveProveedor.equals(""))
            errores.add("El campo clave de proveedor es obligatorio");
        else if(contieneCaracteresNoValidosEsp(claveProveedor))
            errores.add("El campo clave de proveedor contiene acentos o caracteres no v&aacute;lidos: " + claveProveedor);

        // c&oacute;digo de cliente
        if(contieneCaracteresNoValidos(codigoCliente))
            errores.add("El campo c&oacute;digo de cliente contiene acentos o caracteres no v&aacute;lidos: " + codigoCliente);

        // num Transmisi&oacute;n
        if(contieneCaracteresNoValidos(noTransmision))
            errores.add("El campo num. de transmisi&oacute;n contiene acentos o caracteres no v&aacute;lidos: " + noTransmision);

        // nombre o raz&oacute;n social
        if(nombre.equals(""))
            errores.add("El campo " + ((tipo == FISICA)?"nombre":"raz&oacute;n social") + " es obligatorio");
        else if(contieneCaracteresNoValidosEsp2(nombre))
            errores.add("El campo nombre contiene acentos o caracteres no v&aacute;lidos: " + nombre);

        // ap Paterno
        if(tipo == FISICA && apellidoPaterno.equals(""))
            errores.add("El campo apellido paterno es obligatorio");
        else if(tipo == FISICA && contieneCaracteresNoValidosEsp2(apellidoPaterno))
            errores.add("El campo apellido paterno contiene acentos o caracteres no v&aacute;lidos: " + apellidoPaterno);

        // ap Materno
        if(tipo == FISICA && contieneCaracteresNoValidosEsp2(apellidoMaterno))
            errores.add("El campo apellido materno contiene acentos o caracteres no v&aacute;lidos: " + apellidoMaterno);

        // tipo de sociedad
        if(tipo == MORAL && tipoSociedad.equals(""))
            errores.add("El campo tipo de sociedad es obligatorio");
        else if(tipo == MORAL && contieneCaracteresNoValidos(tipoSociedad))
            errores.add("El campo tipo de sociedad contiene acentos o caracteres no v&aacute;lidos: " + tipoSociedad);

        // rfc
        if(rfc.equals(""))
            errores.add("El campo RFC es obligatorio");
        else if(!validaRFC())
            errores.add("El campo RFC no es v&aacute;lido: " + rfc);


        // homoclave
        if(homoclave.equals(""))
            errores.add("El campo homoclave es obligatorio");
        else if(contieneCaracteresNoValidos(homoclave))
            errores.add("El campo homoclave contiene acentos o caracteres no v&aacute;lidos: " + homoclave);

        // contacto empresa
        if(contieneCaracteresNoValidos(contactoEmpresa))
            errores.add("El campo contacto en la empresa contiene acentos o caracteres no v&aacute;lidos: " + contactoEmpresa);

        // cliente factoraje
        try{if(!clienteFactoraje.equals("")) Integer.parseInt(clienteFactoraje);} catch(Exception e)
            {errores.add("El campo de n&uacute;mero de cliente para factoraje debe ser num&eacute;rico: " + clienteFactoraje);}

        // calle y n&uacute;mero
        if(calleNumero.equals(""))
            errores.add("El campo calle y n&uacute;mero es obligatorio");
        else if(contieneCaracteresNoValidos(calleNumero))
            errores.add("El campo calle y n&uacute;mero contiene acentos o caracteres no v&aacute;lidos: " + calleNumero);

        // colonia
        if(colonia.equals(""))
            errores.add("El campo colonia es obligatorio");
        else if(contieneCaracteresNoValidos(colonia))
            errores.add("El campo colonia contiene acentos o caracteres no v&aacute;lidos: " + colonia);

        // delegaci&oacute;n o municipio
        if(delegMunicipio.equals(""))
            errores.add("El campo delegaci&oacute;n o municipio es obligatorio");
        if(contieneCaracteresNoValidos(delegMunicipio))
            errores.add("El campo deleg. o municipio contiene acentos o caracteres no v&aacute;lidos: " + delegMunicipio);

        // ciudad o poblaci&oacute;n
        if(cdPoblacion.equals(""))
            errores.add("El campo ciudad o poblaci&oacute;n es obligatorio");
        if(contieneCaracteresNoValidos(cdPoblacion))
            errores.add("El campo Cd. o poblaci&oacute;n contiene acentos o caracteres no v&aacute;lidos: " + cdPoblacion);

        // c&oacute;digo postal
        if(cp.equals(""))
            errores.add("El c&oacute;digo postal es obligatorio");
        else if(cp.length()<5)
            errores.add("El c&oacute;digo postal debe contener cinco d&iacute;gitos");
        else try{Integer.parseInt(cp);} catch(Exception e)
            {errores.add("El c&oacute;digo postal debe ser num&eacute;rico: " + cp);}

        // estado
        if(estado.trim().equals(""))
            errores.add("El campo estado es obligatorio");

        // pais
        if(pais.trim().equals(""))
            errores.add("El campo pais es obligatorio");

        // medio de informaci&oacute;n
        if(medioInformacion.trim().equals(""))
            errores.add("El campo medio de informaci&oacute;n es obligatorio");
        else try{Integer.parseInt(medioInformacion.trim());} catch(Exception e)
            {errores.add("La clave de medio de informaci&oacute;n debe ser num&eacute;rica: " + medioInformacion);}

        // email
        if(!validaEmail())
            {errores.add("El email no es v&aacute;lido: " + email);}

        // lada o prefijo
        if(esNumerico(ladaPrefijo))
            errores.add("El campo lada o prefijo contiene acentos o caracteres no v&aacute;lidos: " + ladaPrefijo);

        // tel&eacute;fonno
        if(numeroTelefonico.equals(""))
            errores.add("El campo tel&eacute;fono es obligatorio");
        else if(contieneCaracteresNoValidos(numeroTelefonico))
            errores.add("El campo tel&eacute;fono contiene acentos o caracteres no v&aacute;lidos: " + numeroTelefonico);
		/*MSD Q05-29284*/
		if(ladaPrefijo.trim().length() + numeroTelefonico.trim().length() != 10)
			errores.add("La longitud de la clave lada m&aacute;s el n&uacute;mero telef&oacute;nico deben sumar 10 d&iacute;gitos.");
		if(ladaPrefijo.trim().length() != 0 && ladaPrefijo.trim().charAt(0) == '0')
			errores.add("La clave de larga distancia no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01)");
		if(numeroTelefonico.trim().length() != 0 && numeroTelefonico.trim().charAt(0) == '0')
			errores.add("El n&uacute;mero telef&oacute;nico no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01) ni de larga distancia");
		/*MSD Q05-29284*/



        // extensi&oacute;n
        if(contieneCaracteresNoValidos(extensionTel))
            errores.add("El campo extensi&oacute;n telef&oacute;nica contiene acentos o caracteres no v&aacute;lidos: " + extensionTel);

        // fax
        if(esNumerico(fax))
            errores.add("El campo fax contiene acentos o caracteres no v&aacute;lidos: " + fax);
        
		EIGlobal.mensajePorTrace("La forma de pago es: " +  formaPago,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("LA cuenta CLABE es: " + cuentaCLABE.trim(),EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("El BANCO        es: " + banco,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("La clave del BANCO: " + traeClaveBanco(banco),EIGlobal.NivelLog.INFO);
        // forma de pago
        if(formaPago.trim().equals(""))
            errores.add("El campo forma de pago es obligatorio");
        else if(!formaPago.trim().equals("001") && !formaPago.trim().equals("002"))
        	errores.add("La forma de pago no es valida");
        else try{Integer.parseInt(formaPago.trim());} catch(Exception e)
            {errores.add("La clave de forma de pago debe ser num&eacute;rica: " + formaPago);}

        // SE AGREGA LOGICA PARA NO TOMAR EN CUENTA CAMPOS CORRESPONDIENTES A FORMA DE PAGO MISMO BANCO y
        // FORMA DE PAGO OTROS BANCOS y DIVISA MONEDA NACIONAL
        if(formaPago.trim().equals("001") || (formaPago.trim().equals("002") && cveDivisa.trim().equals("MXN"))){
            cvePais = "";
            ctaBancoDestino = "";
            cveABA = "";
            descripcionBanco = "";
            descripcionCiudad = "";
            validaCampo = false;
        }

        boolean flag = false;
        try{Long.parseLong(cuentaCLABE.trim());} catch(Exception e)
        {flag = true;}

        // cuenta CLABE
        try {formaPago = "" + Integer.parseInt(formaPago);} catch(Exception e)
            {errores.add("La clave de forma de pago no es v&aacute;lida"); formaPago="";}
        if(cuentaCLABE.equals(""))
            errores.add("El campo cuenta es obligatorio");
        //Inicia Modificaci&oacute;n PVA 04032003
        else if(flag)
        	errores.add("El campo Cuenta Destino debe ser num&eacute;rico: " + cuentaCLABE);
        else if ((formaPago.equals("2")) && cveDivisa.trim().equals("USD") && cuentaCLABE.trim().length()!=18 && !(cuentaCLABE.trim().length()>=1 && cuentaCLABE.trim().length()<=11))
            errores.add("La longitud de la Cuenta Destino debe estar entre 1 a 11 &oacute; 18 d&iacute;gitos");
        else if ((formaPago.equals("1")) && (cuentaCLABE.trim().length()!=10) && (cuentaCLABE.trim().length()!=11) && (cuentaCLABE.trim().length()!=18))
            errores.add("La longitud de la Cuenta Destino/M&oacute;vil debe ser de 10, 11 &oacute; 18 d&iacute;gitos: "    +cuentaCLABE);
        else if ((formaPago.equals("2")) && cveDivisa.trim().equals("MXN") && (cuentaCLABE.trim().length()!=18 && cuentaCLABE.trim().length() != 10))
            errores.add("La longitud de la Cuenta Destino/M&oacute;vil debe ser de 10 &oacute 18 d&iacute;gitos: "    +cuentaCLABE);

        if ((cuentaCLABE.trim().length()==18) && formaPago.equals("1") && !(validaBancoCLABE(cuentaCLABE.trim(),traeClaveBanco(banco))))
			errores.add("La clave del banco no coincide con la cuenta CLABE " + cuentaCLABE);
        else if ((cuentaCLABE.trim().length()==18) && formaPago.equals("2") && !(validaBancoCLABE(cuentaCLABE.trim(),traeClaveBanco(banco))))
			errores.add("La clave del banco no coincide con la cuenta CLABE " + cuentaCLABE);

        if (formaPago.equals("1") && cuentaCLABE.trim().length()==11 && !validaDigitoCuenta())
            errores.add("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido: " + cuentaCLABE);
        else if (formaPago.equals("1") && cuentaCLABE.trim().length()==18 && !validaDigitoVerificador(cuentaCLABE.trim()))
			errores.add("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido: " + cuentaCLABE);
		else if (formaPago.equals("2") && cveDivisa.trim().equals("MXN") && cuentaCLABE.trim().length()==18 && !validaDigitoVerificador(cuentaCLABE.trim()))
			errores.add("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido: " + cuentaCLABE);
        else if (formaPago.equals("2") && cveDivisa.trim().equals("USD") && cuentaCLABE.trim().length()==18 && !validaDigitoVerificador(cuentaCLABE.trim()))
            errores.add("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido: " + cuentaCLABE);

        if(!cuentaCLABE.equals("")){
			String prefijo="";
			if(cuentaCLABE.length()==11)
			  prefijo=cuentaCLABE.substring(0, 2);
			else if (cuentaCLABE.length()==18)
			  prefijo=cuentaCLABE.substring(6,8);
			if(formaPago.trim().equals("1") && cveDivisa.trim().equals("USD")
					&& (prefijo.equals("08") || prefijo.equals("10") || prefijo.equals("11") || prefijo.equals("13") ||
						prefijo.equals("15") || prefijo.equals("16") || prefijo.equals("17") || prefijo.equals("18") ||
						prefijo.equals("19") || prefijo.equals("20") || prefijo.equals("21") || prefijo.equals("22") ||
						prefijo.equals("23") || prefijo.equals("24") || prefijo.equals("25") || prefijo.equals("26") ||
						prefijo.equals("27") || prefijo.equals("40") || prefijo.equals("43") || prefijo.equals("44") ||
						prefijo.equals("47") || prefijo.equals("50") || prefijo.equals("51") || prefijo.equals("52") ||
						prefijo.equals("53") || prefijo.equals("54") || prefijo.equals("55") || prefijo.equals("56") ||
						prefijo.equals("57") || prefijo.equals("58") || prefijo.equals("59") || prefijo.equals("60") ||
						prefijo.equals("61") || prefijo.equals("62") || prefijo.equals("63") || prefijo.equals("65") ||
						prefijo.equals("66") || prefijo.equals("67") || prefijo.equals("68") || prefijo.equals("69") ||
						prefijo.equals("70") || prefijo.equals("71") || prefijo.equals("72") || prefijo.equals("73") ||
						prefijo.equals("74") || prefijo.equals("75") || prefijo.equals("76") || prefijo.equals("77") ||
						prefijo.equals("78") || prefijo.equals("80") || prefijo.equals("81") || prefijo.equals("85") ||
						prefijo.equals("86") || prefijo.equals("87") || prefijo.equals("88") || prefijo.equals("89") ||
						prefijo.equals("90") || prefijo.equals("91") || prefijo.equals("92") || prefijo.equals("93") ||
						prefijo.equals("94") || prefijo.equals("95") || prefijo.equals("96") || prefijo.equals("99")) )
	            errores.add("La Divisa no corresponde a la cuenta.");
			else if(formaPago.trim().equals("1") && cveDivisa.trim().equals("MXN") && cuentaCLABE.trim().length() == 11
					&& (prefijo.equals("09") || prefijo.equals("42") || prefijo.equals("45") || prefijo.equals("46") ||
						prefijo.equals("48") || prefijo.equals("49") || prefijo.equals("82") || prefijo.equals("83") ||
						prefijo.equals("97") || prefijo.equals("98")) )
	            errores.add("La Divisa no corresponde a la cuenta.");
        }

        //termina Modificacion PVA
        else if(formaPago.length()>0 && formaPago.equals("2") && cveDivisa.trim().equalsIgnoreCase("MXN")
            && clavesBanco != null && (cuentaCLABE.trim().length() == 11 || cuentaCLABE.trim().length() == 18))
            {
            String clave = cuentaCLABE.substring(0, 3);
            while(clave.substring(0,1).equals("0") && clave.length()>1) clave = clave.substring(1);
            if(clavesBanco.get(clave) == null)
                errores.add("La cuenta no coincide con ningun banco");
            else if(!((String)clavesBanco.get(clave)).equals(banco))
                errores.add("La cuenta no coincide con el banco");
            }

		if(formaPago.equals("2")) {
        //banco
        if(clavesBanco != null && !banco.trim().equals("") && !clavesBanco.containsValue(banco))
            errores.add("la clave de banco no es v&aacute;lida");
		}

        //sucursal
        try{if(!sucursal.equals("")) Integer.parseInt(sucursal);} catch(Exception e)
            {errores.add("El campo sucursal debe ser num&eacute;rico: " + sucursal);}

        //plaza
        if(clavesBanco != null && !plaza.trim().equals("") && clavesBanco.get("plazas") != null &&
            ((Hashtable)clavesBanco.get("plazas")).get(plaza) == null)
            errores.add("la clave de plaza no es v&aacute;lida");

        // deudores activos
        try{if(!deudoresActivos.equals("")) Integer.parseInt(deudoresActivos);} catch(Exception e)
            {errores.add("El campo de n&uacute;mero de deudores activos debe ser num&eacute;rico: " + deudoresActivos);}

        // l&iacute;mite de financiamiento
        try{if(!limiteFinanciamiento.equals("")) Long.parseLong(limiteFinanciamiento);} catch(Exception e)
            {errores.add("El l&iacute;mite de financiamiento debe ser num&eacute;rico sin punto decimal: " + limiteFinanciamiento);}

        // volumen anual ventas
        try{if(!volumenAnualVentas.equals("")) Long.parseLong(volumenAnualVentas);} catch(Exception e)
            {errores.add("El volumen anual de ventas debe ser num&eacute;rico sin punto decimal: " + volumenAnualVentas);}

        // importe medio facturas emitidas
        try {if(!importeMedioFacturasEmitidas.equals("")) Long.parseLong(importeMedioFacturasEmitidas);} catch(Exception e)
            {errores.add("El importe medio de facturas emitidas debe ser num&eacute;rico sin punto decimal: " + importeMedioFacturasEmitidas);}
		EIGlobal.mensajePorTrace("CLAVE DIVISA->"+cveDivisa,EIGlobal.NivelLog.DEBUG);
        // clave divisa
        if(cveDivisa.equals("")) {
        	errores.add("La divisa no puede ser un campo vacio: ");
        }
        else if(!cveDivisa.trim().equals("MXN") && !cveDivisa.trim().equals("USD")) {
        	errores.add("La divisa solo puede tomar los valores MXN o USD");
        }

        if(formaPago.equals("2") && cveDivisa.equals("USD")) {
    	EIGlobal.mensajePorTrace("PASO4",EIGlobal.NivelLog.DEBUG);
        //clave pa�s
        if (cvePais.trim().equals("MEXI") && validaCampo) {
        	//Stefanini --JBANDA--2009/06/01
        	EIGlobal.mensajePorTrace("La clave de pais es: >>>>>>>>>>>>>>>>> " + cvePais,EIGlobal.NivelLog.INFO);
        }
        else {
        	errores.add("La clave de pa�s de la cuenta solo puede ser Mexico");
        }

        if(cvePais.trim().equals("") && validaCampo) {
            errores.add("El campo pais es obligatorio");
        }


//      banco corresponsal y clave ABA
        if(descripcionBanco.trim().equals("") && validaCampo)
        	errores.add("El Banco Corresponsal no puede ser un campo vacio");
        else {
//        	Stefanini --JBANDA--2009/06/01
        	EIGlobal.mensajePorTrace("El banco corresponsal es:>>>>>>>>>>>>> " + descripcionBanco,EIGlobal.NivelLog.INFO);
        }

//      Stefanini --JBANDA--2009/06/01
        EIGlobal.mensajePorTrace("La clave aba es:>>>>>>>>>>>>> " + cveABA,EIGlobal.NivelLog.INFO);

        if(cveABA.trim().equals("") && validaCampo) {
            errores.add("El campo clave ABA es obligatorio");
        }
        else {
        	if(validaCampo){
	        	try{
	        	//if(!cveABA.equals(""))
	        		Integer.parseInt(cveABA.trim());
	        		if(!validaABA(cveABA)){
	                	errores.add("La Clave ABA especificada no existe");
	                }else if(cveABA.length()!=9){
	                	errores.add("La Clave ABA es incorrecta");
	                }
	        	} catch(Exception e) {
	        		errores.add("La Clave ABA es incorrecta");
	        	}
        	}
        }

        /**
        String claves = provee.creaArrayClavesABA(descripcionBanco);
        System.out.println("Datos obtenidos de consultar cvesABA: >>>>>>>>>>>>>>>" + claves);
        ABA = claves.split("\\@");
        ABA = Split(claves,"\\@");
        String errorCveABA = "La Clave ABA especificada no existe";


       for(int i = 0; i < ABA.length; i++) {

        	String[] claveAba = Split(ABA[i],"\\|");
			String[] claveAba = ABA[i].split("\\|");
        	System.out.println("cveABA = >>>>>" + claveAba[0] + " bancoCorresponsal = >>>>>" + claveAba[1] + "  ciudad = >>>>>>" + claveAba[2]);
        	if(descripcionBanco.trim().equals(claveAba[1].trim())) {
        		System.out.println("La descripcion banco especificado es: " + descripcionBanco);
        	}
        	else {
        		errores.add("El Banco Corresponsal especificado no existe");
        	}

        	if(cveABA.trim().equals(claveAba[0].trim()) && descripcionBanco.trim().equals(claveAba[1].trim()) && descripcionCiudad.trim().equals(claveAba[2].trim())) {
        		errorCveABA = "";
        	}
        	else if(cveABA.trim().equals(claveAba[0].trim()) && descripcionBanco.trim().equals("")) {
        		errorCveABA = "";
        		descripcionBanco = claveAba[1];
        		descripcionCiudad = claveAba[2];
        		System.out.println("La descripcion ciudad es: " + descripcionCiudad);
        		System.out.println("La descripcion banco es: " + descripcionBanco);
        	}

        	if(descripcionCiudad.trim().equals(claveAba[2].trim())) {
        		System.out.println("La descripcion ciudad es: " + descripcionCiudad);
        	}
        	else {
        		errores.add("La ciudad especificado no existe");
        	}
        }

        if(errorCveABA.equals("")) {
        	;
        }
        else {
        	errores.add(errorCveABA);
        }*/



        //ciudad
        if(descripcionCiudad.trim().equals("") && validaCampo)
        	errores.add("La Ciudad no puede ser un campo vacio");
        }

        boolean flag2 = false;
        if(validaCampo){
	        try{Long.parseLong(ctaBancoDestino.trim());} catch(Exception e)
	        {flag2 = true;}
        }

        //Cuenta Banco Destino
        if(flag2 && formaPago.trim().equals("2") && cveDivisa.trim().equals("USD") && validaCampo)
        	errores.add("El campo cuenta banco destino debe ser num&eacute;rico: " + ctaBancoDestino);
        else if(ctaBancoDestino.length() > 20 && validaCampo)
        	errores.add("El campo cuenta banco destino debe ser de maximo 20 caracteres");
        else if (ctaBancoDestino.trim().equals("") && formaPago.trim().equals("2") && cveDivisa.equals("USD") && validaCampo)
        	errores.add("El campo cuenta banco destino no puede ser un campo vacio");

        //Tipo de Abono
        if(tipoAbono.equals(""))
        	errores.add("El campo tipo de abono no puede ser vacio");
        if(!tipoAbono.equals("")){
            if(!tipoAbono.equals("01") && !tipoAbono.equals("02"))
            	errores.add("El tipo de abono no es valido");
    	}
        try{
        	Integer.parseInt(tipoAbono);
       	}catch(Exception e){
        	errores.add("El campo tipo de abono debe ser num&eacute;rico: " + tipoAbono);
        }

        // relaci&oacute;n entre campos
        try
            {
            // medio de informaci&oacute;n
            a = Integer.parseInt(medioInformacion.trim());
            if(a!=1 && a!=2)
            	errores.add("La clave de confirmaci&oacute;n es incorrecta");
            if(a == 1 && email.trim().equals(""))
                errores.add("Se ha especificado el e-mail como medio de confirmaci&oacute;n, pero no se ha proporcionado uno");
            if(a == 2 && fax.trim().equals(""))
                errores.add("Se ha especificado el fax como medio de confirmaci&oacute;n, pero no se ha proporcionado uno");
            if(a == 2 && ladaPrefijo.trim().equals(""))
                errores.add("Se ha especificado el fax como medio de confirmaci&oacute;n, pero no se ha proporcionado una clave lada");
            /*MSD Q05-29284*/
            if(a == 2 && (ladaPrefijo.trim().length() + fax.trim().length()) != 10)
                errores.add("La longitud de la clave lada m&aacute;s el fax deben sumar 10 d&iacute;gitos.");
            if(a == 2 && (ladaPrefijo.trim().length() != 0 && ladaPrefijo.trim().charAt(0) == '0'))
                errores.add("La clave de larga distancia no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01)");
            if(a == 2 && (fax.trim().trim().length() != 0 && fax.trim().trim().charAt(0) == '0'))
                errores.add("El n&uacute;mero de FAX no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01) ni de larga distancia");
			/*MSD Q05-29284*/

            // forma de pago
            a = Integer.parseInt(formaPago.trim());
            if(a == 1 && !banco.equals("BANME") && !banco.equals("SERFI") && !banco.trim().equals(""))
                errores.add("Se ha especificado un banco distinto a Santander o Serf&iacute;n con una forma de pago interna");
            if(a == 2 && (banco.equals("BANME") || banco.equals("SERFI")))
                errores.add("Se ha especificado forma de pago interbancaria, pero se ha especificado como banco a Santander o Serf&iacute;n");
            if(a == 2 && (banco.trim().equals("") || banco.trim().equals("0")))
                errores.add("Se ha especificado forma de pago interbancaria, pero no se ha proporcionado un banco");
            //if(a == 2 && (sucursal.equals("") || sucursal.equals("0") || sucursal.equals("00000")))
                //errores.add("Se ha especificado forma de pago interbancaria, pero no se ha proporcionado una sucursal");
            //if(a == 2 && (plaza.trim().equals("") || plaza.trim().equals("0")))
                //errores.add("Se ha especificado forma de pago interbancaria, pero no se ha proporcionado una plaza");
            }
        catch(Exception e)
            {
        	//e.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al leer linea de importacion", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("ProveedorConf::leeLineaImportacionCDivisa:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);	        	
            }

        try{
        limiteFinanciamiento = colocaPuntoDecimal(limiteFinanciamiento);
        volumenAnualVentas = colocaPuntoDecimal(volumenAnualVentas);
        importeMedioFacturasEmitidas = colocaPuntoDecimal(importeMedioFacturasEmitidas);
        }catch(Exception e){
        	//e.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al leer linea de importacion", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("ProveedorConf::leeLineaImportacionCDivisa:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);	        	
        }

        return errores;
        }
///////////////////////////////////////////////////////////////////////////////////////

/**
 * @author daguilar
 * FSW: Stefanini Mexico SA. de CV.
 * fecha creacion 2009/09/25
 */
public Vector leeLineaImportacionCDivisaInter(String linea, Hashtable clavesBanco) {
	Vector errores = new Vector();
    int a;
    String[] ABA = null;
    coMtoProveedor provee = new coMtoProveedor();

	if(linea.substring(0,1).equals("F")) tipo = FISICA; else tipo = MORAL;
	origenProveedor = INTERNACIONAL;
	if(tipo == FISICA)
        {
        claveProveedor = linea.substring(1,21).trim();
        apellidoPaterno = linea.substring(21,51).trim();
        apellidoMaterno = linea.substring(51,81).trim();
        nombre = linea.substring(81,161).trim();
        rfc = linea.substring(161,181).trim();
        contactoEmpresa = linea.substring(181,261).trim();
        clienteFactoraje = linea.substring(261,269).trim();
        calleNumero = linea.substring(269,329).trim();
        cdPoblacion = linea.substring(329,364).trim();
        cp = linea.substring(364,376).trim();
        estado = linea.substring(376,411).trim();
        pais = linea.substring(411,415).trim();
        medioInformacion = linea.substring(415,418).trim();
        email = linea.substring(418,478).trim();
        ladaPrefijo = linea.substring(478,490).trim();
        numeroTelefonico = linea.substring(490,502).trim();
        extensionTel = linea.substring(502,514).trim();
        fax = linea.substring(514,526).trim();
        extensionFax = linea.substring(526,538).trim();
        formaPago = linea.substring(538,541).trim();
        cuentaCLABE = linea.substring(541,581).trim();
        descripcionBanco = linea.substring(581,621).trim();
        cveDivisa = linea.substring(621, 625).trim();
        cvePais = linea.substring(625, 629).trim();
        cveABA = linea.substring(629, 641).trim();
        descripcionCiudad = linea.substring(641, 681).trim();
        datosAdic = linea.substring(681, 701).trim(); //Clave BIC
        tipoAbono = linea.substring(701, 703).trim();
        limiteFinanciamiento = linea.substring(703,724).trim();
        deudoresActivos = linea.substring(724,729).trim();
        volumenAnualVentas = linea.substring(729,750).trim();
        importeMedioFacturasEmitidas = linea.substring(750,771).trim();
        homoclave = "";
        colonia = "";
        delegMunicipio = "";
        sucursal = "";
        plaza = "";
        banco = "";
        ctaBancoDestino = "";
        }
    else
        {
        claveProveedor = linea.substring(1,21).trim();
        nombre = linea.substring(21,101).trim();
        tipoSociedad = linea.substring(101,161).trim();
        rfc = linea.substring(161,181).trim();
        contactoEmpresa = linea.substring(181,261).trim();
        clienteFactoraje = linea.substring(261,269).trim();
        calleNumero = linea.substring(269,329).trim();
        cdPoblacion = linea.substring(329,364).trim();
        cp = linea.substring(364,376).trim();
        estado = linea.substring(376,411).trim();
        pais = linea.substring(411,415).trim();
        medioInformacion = linea.substring(415,418).trim();
        email = linea.substring(418,478).trim();
        ladaPrefijo = linea.substring(478,490).trim();
        numeroTelefonico = linea.substring(490,502).trim();
        extensionTel = linea.substring(502,514).trim();
        fax = linea.substring(514,526).trim();
        extensionFax = linea.substring(526,538).trim();
        formaPago = linea.substring(538,541).trim();
        cuentaCLABE = linea.substring(541,581).trim();
        descripcionBanco = linea.substring(581, 621).trim();
        cveDivisa = linea.substring(621, 625).trim();
        cvePais = linea.substring(625, 629).trim();
        cveABA = linea.substring(629, 641).trim();
        descripcionCiudad = linea.substring(641, 681).trim();
        datosAdic = linea.substring(681, 701).trim();
        tipoAbono = linea.substring(701, 703).trim();
        limiteFinanciamiento = linea.substring(703,724).trim();
        deudoresActivos = linea.substring(724,729).trim();
        volumenAnualVentas = linea.substring(729,750).trim();
        importeMedioFacturasEmitidas = linea.substring(750,771).trim();
        homoclave = "";
        colonia = "";
        delegMunicipio = "";
        banco = "";
        sucursal = "";
        plaza = "";
        ctaBancoDestino = "";
       }

	EIGlobal.mensajePorTrace("Entro a Importaci�n con DIVISA",EIGlobal.NivelLog.DEBUG);
    // c&oacute;digo de cliente
    if(contieneCaracteresNoValidos(codigoCliente))
        errores.add("El campo c&oacute;digo de cliente contiene acentos o caracteres no v&aacute;lidos: " + codigoCliente);

    // num Transmisi&oacute;n
    if(contieneCaracteresNoValidos(noTransmision))
        errores.add("El campo num. de transmisi&oacute;n contiene acentos o caracteres no v&aacute;lidos: " + noTransmision);

    // clave de proveedor
    if(claveProveedor.equals(""))
        errores.add("El campo clave de proveedor es obligatorio");
    else if(contieneCaracteresNoValidosEsp(claveProveedor))
        errores.add("El campo clave de proveedor contiene acentos o caracteres no v&aacute;lidos: " + claveProveedor);

    // ap Paterno
    if(tipo == FISICA && apellidoPaterno.equals(""))
        errores.add("El campo apellido paterno es obligatorio");
    else if(tipo == FISICA && contieneCaracteresNoValidosEsp2(apellidoPaterno))
        errores.add("El campo apellido paterno contiene acentos o caracteres no v&aacute;lidos: " + apellidoPaterno);

    // ap Materno
    if(tipo == FISICA && contieneCaracteresNoValidosEsp2(apellidoMaterno))
        errores.add("El campo apellido materno contiene acentos o caracteres no v&aacute;lidos: " + apellidoMaterno);

    // nombre o raz&oacute;n social
    if(nombre.equals(""))
        errores.add("El campo " + ((tipo == FISICA)?"nombre":"raz&oacute;n social") + " es obligatorio");
    else if(contieneCaracteresNoValidosEsp2(nombre))
        errores.add("El campo nombre contiene acentos o caracteres no v&aacute;lidos: " + nombre);

    // tipo de sociedad
    if(tipo == MORAL && contieneCaracteresNoValidosEsp2(tipoSociedad))
        errores.add("El campo tipo de sociedad contiene acentos o caracteres no v&aacute;lidos: " + tipoSociedad);

    // rfc
    if(rfc.equals(""))
        errores.add("El campo ID Contribuyente es obligatorio");
    else if(contieneCaracteresNoValidosEsp3(rfc))
    	errores.add("El campo ID Contribuyente contiene acentos o caracteres no v&aacute;lidos");
    /*else if(!validaRFC())
        errores.add("El campo RFC no es v&aacute;lido: " + rfc);*/

    // contacto empresa
    if(contieneCaracteresNoValidos(contactoEmpresa))
        errores.add("El campo contacto en la empresa contiene acentos o caracteres no v&aacute;lidos: " + contactoEmpresa);

    // cliente factoraje
    try{if(!clienteFactoraje.equals("")) Integer.parseInt(clienteFactoraje);} catch(Exception e)
        {errores.add("El campo de n&uacute;mero de cliente para factoraje debe ser num&eacute;rico: " + clienteFactoraje);}

    // calle y n&uacute;mero
    if(calleNumero.equals(""))
        errores.add("El campo calle y n&uacute;mero es obligatorio");
    else if(contieneCaracteresNoValidos(calleNumero))
        errores.add("El campo calle y n&uacute;mero contiene acentos o caracteres no v&aacute;lidos: " + calleNumero);

    // ciudad o poblaci&oacute;n
    if(cdPoblacion.equals(""))
        errores.add("El campo ciudad o poblaci&oacute;n es obligatorio");
    if(contieneCaracteresNoValidos(cdPoblacion))
        errores.add("El campo Cd. o poblaci&oacute;n contiene acentos o caracteres no v&aacute;lidos: " + cdPoblacion);

    // c&oacute;digo postal
    if(cp.equals(""))
        errores.add("El c&oacute;digo postal es obligatorio");
    else {
    	// Comprueba que sea un campo alfanumerico.
    	Pattern p = Pattern.compile("[^A-Za-z0-9 ]+");
    	Matcher m = p.matcher(cp);
    	if (m.find()) {
    		errores.add("El c&oacute;digo postal debe ser alfanum&eacute;rico: " + cp);
    	}
    }

    // estado
    if(estado.trim().equals(""))
        errores.add("El campo estado es obligatorio");

    // pais
    if(pais.trim().equals(""))
        errores.add("El campo pais es obligatorio");

    // medio de informaci&oacute;n
    if(medioInformacion.trim().equals(""))
        errores.add("El campo medio de informaci&oacute;n es obligatorio");
    else{    	
    	try{
    		Integer.parseInt(medioInformacion.trim());
		}catch(Exception e){
			errores.add("El campo medio de informaci&oacute;n debe ser por e-mail");
		}
		if(!medioInformacion.trim().equals("001"))
			errores.add("El campo medio de informaci&oacute;n debe ser por e-mail");
    }


    // email
    if(!validaEmail())
        {errores.add("El email no es v&aacute;lido: " + email);}

    // lada o prefijo
    if(ladaPrefijo.equals(""))
        errores.add("El campo lada o prefijo es obligatorio");
    else if(esNumerico(ladaPrefijo))
        errores.add("El campo lada o prefijo contiene acentos o caracteres no v&aacute;lidos: " + ladaPrefijo);
    if(ladaPrefijo.trim().length() != 0 && ladaPrefijo.trim().charAt(0) == '0')
		errores.add("La clave de larga distancia no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01)");

    // tel&eacute;fonno
    if(numeroTelefonico.equals(""))
        errores.add("El campo tel&eacute;fono es obligatorio");
    else if(contieneCaracteresNoValidos(numeroTelefonico))
        errores.add("El campo tel&eacute;fono contiene acentos o caracteres no v&aacute;lidos: " + numeroTelefonico);
	if(numeroTelefonico.trim().length() != 0 && numeroTelefonico.trim().charAt(0) == '0')
		errores.add("El n&uacute;mero telef&oacute;nico no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01) ni de larga distancia");

    // extensi&oacute;n
    if(contieneCaracteresNoValidos(extensionTel))
        errores.add("El campo extensi&oacute;n telef&oacute;nica contiene acentos o caracteres no v&aacute;lidos: " + extensionTel);

    // fax
    if(esNumerico(fax))
        errores.add("El campo fax contiene acentos o caracteres no v&aacute;lidos: " + fax);

    // forma de pago
    if(formaPago.trim().equals(""))
        errores.add("El campo forma de pago es obligatorio");
    else if(!formaPago.trim().equals("005"))
    	errores.add("La forma de pago no es valida");
    else try{Integer.parseInt(formaPago.trim());} catch(Exception e)
        {errores.add("La clave de forma de pago debe ser num&eacute;rica: " + formaPago);}

    // Datos adicionales - Clave BIC
    if(cvePais.trim().equals("USA"))
    	datosAdic = "";
    else if(!datosAdic.trim().equals("") && datosAdic.length()>11)
    	errores.add("La longitud de la clave BIC es incorrecta");
    else{// Comprueba que sea un campo alfanumerico.
    	Pattern p = Pattern.compile("[^A-Za-z0-9]+");
    	Matcher m = p.matcher(datosAdic);
    	if (m.find()) {
    		errores.add("EL campo clave BIC debe ser Num&eacute;rico" + datosAdic);
    	}
    }

    // cuenta CLABE
    if(cuentaCLABE.equals("")){
        errores.add("El campo cuenta es obligatorio");
    }else if (cuentaCLABE.length()>34){
    	errores.add("La longitud de la Cuenta Destino es incorrecta");
    }else {
    	// Comprueba que sea un campo alfanumerico.
    	Pattern p = Pattern.compile("[^A-Za-z0-9]+");
    	Matcher m = p.matcher(cuentaCLABE);
    	if (m.find()) {
    		errores.add("La Cuenta Destino es incorrecta: " + cuentaCLABE);
    	}
    }

    //  banco extranjero
    if(descripcionBanco.trim().equals(""))
    	errores.add("El Banco Extranjero no puede ser un campo vacio");

    // clave divisa
    if(cveDivisa.equals(""))
    	errores.add("La divisa no puede ser un campo vacio: ");
    else if(!cveDivisa.trim().equals("USD"))
    	errores.add("La divisa solo puede tomar el valor USD");

    //clave pa�s
    if(cvePais.trim().equals(""))
        errores.add("El campo pais es obligatorio");
    else if(cvePais.trim().equals("MEXI") && Integer.parseInt(formaPago.trim())==5)
    	errores.add("El Pa&iacute;s Mexico no aplica para forma de pago Internacional.");

    //  clave ABA
    if(cvePais.trim().equals("USA") && cveABA.trim().equals(""))
        errores.add("El campo clave ABA es obligatorio");
    else if(cvePais.trim().equals("USA") && !cveABA.trim().equals("")){
    	try{
    		Integer.parseInt(cveABA.trim());
    		if(!validaABA(cveABA)){
            	errores.add("La Clave ABA especificada no existe");
            }else if(cveABA.length()!=9){
            	errores.add("La Clave ABA es incorrecta");
            }
    	} catch(Exception e) {
    		errores.add("La Clave ABA es incorrecta");
    	}
    }else if(!cveABA.trim().equals("") && !cvePais.trim().equals("USA"))
    	errores.add("El campo clave ABA solo aplica para cuentas en Estados Unidos.");
    
    //ciudad
    if(descripcionCiudad.trim().equals(""))
    	errores.add("La Ciudad no puede ser un campo vacio");

    //Tipo de Abono
    if(tipoAbono.equals(""))
    	errores.add("El campo tipo de abono no puede ser vacio");
    if(!tipoAbono.equals("")){
        if(!tipoAbono.equals("01") && !tipoAbono.equals("02"))
        	errores.add("El tipo de abono no es valido");
	}
    try{
    	Integer.parseInt(tipoAbono);
   	}catch(Exception e){
    	errores.add("El campo tipo de abono debe ser num&eacute;rico: " + tipoAbono);
    }

    // l&iacute;mite de financiamiento
    try{if(!limiteFinanciamiento.equals("")) Long.parseLong(limiteFinanciamiento);} catch(Exception e)
        {errores.add("El l&iacute;mite de financiamiento debe ser num&eacute;rico sin punto decimal: " + limiteFinanciamiento);}

    // deudores activos
    try{if(!deudoresActivos.equals("")) Integer.parseInt(deudoresActivos);} catch(Exception e)
        {errores.add("El campo de n&uacute;mero de deudores activos debe ser num&eacute;rico: " + deudoresActivos);}

    // volumen anual ventas
    try{if(!volumenAnualVentas.equals("")) Long.parseLong(volumenAnualVentas);} catch(Exception e)
        {errores.add("El volumen anual de ventas debe ser num&eacute;rico sin punto decimal: " + volumenAnualVentas);}

    // importe medio facturas emitidas
    try {if(!importeMedioFacturasEmitidas.equals("")) Long.parseLong(importeMedioFacturasEmitidas);} catch(Exception e)
        {errores.add("El importe medio de facturas emitidas debe ser num&eacute;rico sin punto decimal: " + importeMedioFacturasEmitidas);}

    // relaci&oacute;n entre campos
    try{
        // medio de informaci&oacute;n
        a = Integer.parseInt(medioInformacion.trim());
        if(a == 1 && email.trim().equals(""))
            errores.add("Se ha especificado el e-mail como medio de confirmaci&oacute;n, pero no se ha proporcionado uno");
        if(a == 2 && fax.trim().equals(""))
            errores.add("Se ha especificado el fax como medio de confirmaci&oacute;n, pero no se ha proporcionado uno");
        if(a == 2 && ladaPrefijo.trim().equals(""))
            errores.add("Se ha especificado el fax como medio de confirmaci&oacute;n, pero no se ha proporcionado una clave lada");
        if(a == 2 && (ladaPrefijo.trim().length() != 0 && ladaPrefijo.trim().charAt(0) == '0'))
            errores.add("La clave de larga distancia no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01)");
        if(a == 2 && (fax.trim().trim().length() != 0 && fax.trim().trim().charAt(0) == '0'))
            errores.add("El n&uacute;mero de FAX no debe comenzar con 0 ni incluir claves de marcaci&oacute;n (01) ni de larga distancia");

    }catch(Exception e){//e.printStackTrace();
		StackTraceElement[] lineaError;
		lineaError = e.getStackTrace();
		EIGlobal.mensajePorTrace("Error al leer linea de importacion", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("ProveedorConf::leeLineaImportacionCDivisaInter:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
								+ e.getMessage()
	               				+ "<DATOS GENERALES>"
					 			+ "Linea encontrada->" + lineaError[0]
					 			, EIGlobal.NivelLog.ERROR);	        	   
    }

    try{
    limiteFinanciamiento = colocaPuntoDecimal(limiteFinanciamiento);
    volumenAnualVentas = colocaPuntoDecimal(volumenAnualVentas);
    importeMedioFacturasEmitidas = colocaPuntoDecimal(importeMedioFacturasEmitidas);
    }catch(Exception e){
    	//e.printStackTrace();
		StackTraceElement[] lineaError;
		lineaError = e.getStackTrace();
		EIGlobal.mensajePorTrace("Error al leer linea de importacion", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("ProveedorConf::leeLineaImportacionCDivisaInter:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
								+ e.getMessage()
	               				+ "<DATOS GENERALES>"
					 			+ "Linea encontrada->" + lineaError[0]
					 			, EIGlobal.NivelLog.ERROR);	     	
    }

    return errores;
    }
///////////////////////////////////////////////////////////////////////////////////////

// ---
    public String tramaExportacion()
        {
        String linea = "";
        if(tipo == FISICA)
            {
            linea += "F";
            linea += rellena(claveProveedor,20,' ',false);
            linea += rellena(apellidoPaterno,30,' ',false);
            linea += rellena(apellidoMaterno,30,' ',false);
            linea += rellena(nombre,80,' ',false);
            }
        else if(tipo == MORAL)
            {
            linea += "M";
            linea += rellena(claveProveedor,20,' ',false);
            linea += rellena(nombre,80,' ',false);
            linea += rellena(tipoSociedad.trim(),60,' ',false);
            }
        if(origenProveedor.equals(INTERNACIONAL)){
        	linea += rellena(rfc,20,' ',false);
        }else{
        	linea += rellena(rfc,10,' ',false); linea += rellena(homoclave,3,' ',false);
        }

        linea += rellena(contactoEmpresa,80,' ',false); linea += rellena(clienteFactoraje,8,'0',true);
        linea += rellena(calleNumero,60,' ',false);

        if(origenProveedor.equals(INTERNACIONAL)){
        	linea += rellena(cdPoblacion,35,' ',false);
        	linea += rellena(cp,12,' ',false);
        	linea += rellena(estado,35,' ',false);
        	linea += rellena(pais,4,' ',false);
        	linea += rellena(medioInformacion,3,'0',true);
        	linea += rellena(email,60,' ',false);
        	linea += rellena(ladaPrefijo,12,' ',false);
            linea += rellena(numeroTelefonico,12,' ',false);
            linea += rellena(extensionTel,12,' ',false);
            linea += rellena(fax,12,' ',false);
            linea += rellena(extensionFax,12,' ',false);
            linea += rellena(formaPago,3,'0',true);
            linea += rellena(cuentaCLABE,40,' ',false);
            linea += rellena(descripcionBanco,40,' ',false);
            linea += rellena(cveDivisa,4,' ',false);
            linea += rellena(cvePais,4,' ',false);
            linea += rellena(cveABA,12,' ',false);
            linea += rellena(descripcionCiudad,40,' ',false);
            linea += rellena(datosAdic,20,' ',false);
            linea += rellena(tipoAbono,2,' ',false);
            linea += rellena(quitaPuntoDecimal(limiteFinanciamiento),21,'0',true);
            linea += rellena(deudoresActivos,5,'0',true);
            linea += rellena(quitaPuntoDecimal(volumenAnualVentas),21,'0',true);
            linea += rellena(quitaPuntoDecimal(importeMedioFacturasEmitidas),21,'0',true);
            linea += rellena(claveStatus,1,' ',false);
            linea += rellena(status,80,' ',false);
            linea += rellena("",32,' ',false);
        }else{
        	linea += rellena(colonia,30,' ',false);
        	linea += rellena(delegMunicipio,35,' ',false);
        	linea += rellena(cdPoblacion,35,' ',false);
        	linea += rellena(cp,5,' ',false);
        	linea += rellena(estado,4,' ',false);
        	linea += rellena(pais,4,' ',false);
        	linea += rellena(medioInformacion,3,'0',true);
            linea += rellena(email,60,' ',false);
            linea += rellena(ladaPrefijo,12,' ',false);
            linea += rellena(numeroTelefonico,12,' ',false);
            linea += rellena(extensionTel,12,' ',false);
            linea += rellena(fax,12,' ',false);
            linea += rellena(extensionFax,12,' ',false);
            linea += rellena(formaPago,3,'0',true);
            linea += rellena(cuentaCLABE,20,' ',false);
            linea += rellena(banco,5,' ',false);
            linea += rellena(sucursal,4,' ',false);
            linea += rellena(plaza,5,' ',false);
            linea += rellena(quitaPuntoDecimal(limiteFinanciamiento),21,'0',true);
            linea += rellena(deudoresActivos,5,'0',true);
            linea += rellena(quitaPuntoDecimal(volumenAnualVentas),21,'0',true);
            linea += rellena(quitaPuntoDecimal(importeMedioFacturasEmitidas),21,'0',true);
            linea += rellena("901",18,' ',false);
            linea += rellena(claveStatus,1,' ',false);
            linea += rellena(status,80,' ',false);
            linea += rellena(cveDivisa,4,' ',false);
            linea += rellena(cvePais,4,' ',false);
            linea += rellena(ctaBancoDestino,20,' ',false);
            linea += rellena(cveABA,12,' ',false);
            linea += rellena(descripcionBanco,40,' ',false);
            linea += rellena(descripcionCiudad,40,' ',false);
            linea += rellena(tipoAbono,2,' ',false);
        }

        /* DAG 011009: Modificacion deshabilitada hasta que se indique que esta trama debe llevar el campo datosAdic
        if(origenProveedor.equals(INTERNACIONAL))
        	linea += rellena(datosAdic,40,' ',false);*/
        return linea;
        }

    public String getTipoSociedadByCodigoGlobal(String codigoGlobal) {
    	EIGlobal.mensajePorTrace ("ProveedorConf::getTipoSociedadByCodigoGlobal:: -> codigoGlobal:" + codigoGlobal + "<", EIGlobal.NivelLog.INFO);
    	Connection conn = null;
    	String tipoSociedad = "";
		try {
			conn = createiASConn (Global.DATASOURCE_ORACLE );
			StringBuilder query = new StringBuilder(GET_TIPO_SOCIEDAD_BY_CODIGO_GLOBAL);
			query.append("'");
			query.append(codigoGlobal);
			query.append("'");
			EIGlobal.mensajePorTrace ("ProveedorConf::getTipoSociedadByCodigoGlobal:: -> Query:" + query.toString() + "<", EIGlobal.NivelLog.INFO);
			ResultSet rs = conn.createStatement().executeQuery(query.toString());
			if(rs.next()) {
				tipoSociedad = rs.getString("descripcion");
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace ("ProveedorConf::getTipoSociedadByCodigoGlobal:: -> Error:" + e.getMessage(), EIGlobal.NivelLog.INFO);
		} finally {
			try {
				conn.close();
			} catch(Exception e) {
				EIGlobal.mensajePorTrace ("ProveedorConf::getTipoSociedadByCodigoGlobal:: -> Error:" + e.getMessage(), EIGlobal.NivelLog.INFO);
			}
		  }
		EIGlobal.mensajePorTrace ("ProveedorConf::getTipoSociedadByCodigoGlobal:: -> tipoSociedad:" + tipoSociedad + "<", EIGlobal.NivelLog.INFO);
		return tipoSociedad;
    }

    // ---
    /*
     * jgarcia 08/Oct/2009
     * Se agregan los campos tipoAbono y origenProveedor a la trama.
     */
    public String tramaModificacion()
        {
        String trama = "";
        trama += noTransmision + "@"; trama += codigoCliente + "@"; trama += claveProveedor + "@";
        trama += tipoSociedad + "@"; trama += contactoEmpresa + "@"; trama += clienteFactoraje + "@";
        trama += medioInformacion + "@"; trama += formaPago + "@"; trama += cuentaCLABE + "@";
        trama += banco + "@"; trama += sucursal + "@"; trama += plaza + "@";
        trama += limiteFinanciamiento + "@"; trama += deudoresActivos + "@";
        trama += volumenAnualVentas + "@"; trama += importeMedioFacturasEmitidas + "@";
        trama += ((tipo == FISICA)?"F@":"M@"); trama += apellidoPaterno + "@";
        trama += apellidoMaterno + "@"; trama += nombre + "@"; trama += rfc + "@";
        trama += homoclave + "@"; trama += calleNumero + "@"; trama += colonia + "@";
        trama += delegMunicipio + "@"; trama += cdPoblacion + "@"; trama += cp + "@";
        trama += estado.trim() + "@"; trama += pais + "@"; trama += ladaPrefijo + "@";
        trama += numeroTelefonico + "@"; trama += extensionTel + "@"; trama += fax + "@";
        trama += extensionFax + "@@"; trama += email + "@"; trama += cveDivisa.trim() + "@";
        trama += cveABA + "@"; trama += descripcionCiudad + "@"; trama += descripcionBanco + "@";
        trama += ctaBancoDestino + "@"; trama += cvePais + "@";
        trama += datosAdic + "@"; trama += tipoAbono + "@"; trama += origenProveedor + "@";
        return trama;
        }

    // ---
    private void limpiaEspacios()
        {
        if(claveProveedor.equals(" ")) claveProveedor = ""; if(codigoCliente.equals(" ")) codigoCliente = "";
        if(noTransmision.equals(" ")) noTransmision = ""; if(nombre.equals(" ")) nombre = "";
		if(apellidoPaterno.equals(" ")) apellidoPaterno = ""; if(apellidoMaterno.equals(" ")) apellidoMaterno = "";
        if(tipoSociedad.equals(" ")) tipoSociedad = ""; if(rfc.equals(" ")) rfc = "";
        if(homoclave.equals(" ")) homoclave = ""; if(contactoEmpresa.equals(" ")) contactoEmpresa = "";
        if(clienteFactoraje.equals(" ")) clienteFactoraje = ""; if(calleNumero.equals(" ")) calleNumero = "";
        if(colonia.equals(" ")) colonia = ""; if(delegMunicipio.equals(" ")) delegMunicipio = "";
        if(cdPoblacion.equals(" ")) cdPoblacion = ""; if(cp.equals(" ")) cp = "";
        if(estado.equals(" ")) estado = ""; if(pais.equals(" ")) pais = "";
        if(medioInformacion.equals(" ")) medioInformacion = ""; if(email.equals(" ")) email = "";
		if(ladaPrefijo.equals(" ")) ladaPrefijo = ""; if(numeroTelefonico.equals(" ")) numeroTelefonico = "";
        if(extensionTel.equals(" ")) extensionTel = ""; if(fax.equals(" ")) fax = "";
        if(extensionFax.equals(" ")) extensionFax = ""; if(formaPago.equals(" ")) formaPago = "";
        if(cuentaCLABE.equals(" ")) cuentaCLABE = ""; if(banco.equals(" ")) banco = "";
        if(sucursal.equals(" ")) sucursal = ""; if(plaza.equals(" ")) plaza = "";
        if(deudoresActivos.equals(" ")) deudoresActivos = "";
		if(limiteFinanciamiento.equals(" ")) limiteFinanciamiento = "";
        if(volumenAnualVentas.equals(" ")) volumenAnualVentas = "";
		if(importeMedioFacturasEmitidas.equals(" ")) importeMedioFacturasEmitidas = "";
		if(ctaBancoDestino.equals(" ")) ctaBancoDestino = "";
		if(descripcionBanco.equals(" ")) descripcionBanco = "";
		if(descripcionCiudad.equals(" ")) descripcionCiudad = "";
		if(cveDivisa.equals(" ")) cveDivisa = "";
		if(cveABA.equals(" ")) cveABA = "";
        }

    // ---
    public void quitaNulos()
        {
        if(claveProveedor == null) claveProveedor = ""; if(codigoCliente == null) codigoCliente = "";
        if(noTransmision == null) noTransmision = ""; if(nombre == null) nombre = "";
        if(apellidoPaterno == null) apellidoPaterno = ""; if(apellidoMaterno == null) apellidoMaterno = "";
        if(tipoSociedad == null) tipoSociedad = ""; if(rfc == null) rfc = "";
        if(homoclave == null) homoclave = ""; if(contactoEmpresa == null) contactoEmpresa = "";
        if(clienteFactoraje == null) clienteFactoraje = ""; if(calleNumero == null) calleNumero = "";
        if(colonia == null) colonia = ""; if(delegMunicipio == null) delegMunicipio = "";
        if(cdPoblacion == null) cdPoblacion = ""; if(cp == null) cp = ""; if(estado == null) estado = "";
        if(pais == null) pais = ""; if(medioInformacion == null) medioInformacion = "";
        if(email == null) email = ""; if(ladaPrefijo == null) ladaPrefijo = "";
        if(numeroTelefonico == null) numeroTelefonico = ""; if(extensionTel == null) extensionTel = "";
        if(fax == null) fax = ""; if(extensionFax == null) extensionFax = "";
        if(formaPago == null) formaPago = ""; if(cuentaCLABE == null) cuentaCLABE = "";
        if(banco == null) banco = ""; if(sucursal == null) sucursal = ""; if(plaza == null) plaza = "";
        if(deudoresActivos == null) deudoresActivos = "";
		if(limiteFinanciamiento == null) limiteFinanciamiento = "";
        if(volumenAnualVentas == null) volumenAnualVentas = "";
        if(importeMedioFacturasEmitidas == null) importeMedioFacturasEmitidas = "";
        if(cveDivisa == null) cveDivisa = "";
        if(descripcionCiudad == null) descripcionCiudad = "";
        if(descripcionBanco == null) descripcionBanco = "";
        if(cveABA == null) cveABA = "";
        if(ctaBancoDestino == null) ctaBancoDestino = "";
        if(claveStatus == null) claveStatus = "";
        if(status == null) status = "";
        if(canal == null) canal = "";
        if(cvePais == null) cvePais = "";
        if(datosAdic == null) datosAdic = "";
        if(tipoAbono == null) tipoAbono = "";
        if(origenProveedor == null) origenProveedor = "";
        }

    // ---
    private boolean contieneCaracteresNoValidos(String campo)
        {
        boolean resultado = false;
        String patron = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz0123456789 ";
        int a, total = campo.length();
        for(a=0;a<total && !resultado;a++) if(patron.indexOf(campo.substring(a,a+1))==-1) resultado = true;
        return resultado;
        }

    private boolean contieneCaracteresNoValidosEsp(String campo)
    {
    boolean resultado = false;
    String patron = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789& ";
    int a, total = campo.length();
    for(a=0;a<total && !resultado;a++) if(patron.indexOf(campo.substring(a,a+1))==-1) resultado = true;
    return resultado;
    }

    private boolean contieneCaracteresNoValidosEsp2(String campo)
    {
    boolean resultado = false;
    String patron = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz0123456789& ";
    int a, total = campo.length();
    for(a=0;a<total && !resultado;a++) if(patron.indexOf(campo.substring(a,a+1))==-1) resultado = true;
    return resultado;
    }
    
    private boolean contieneCaracteresNoValidosEsp3(String campo)
    {
    boolean resultado = false;
    String patron = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz0123456789&";
    int a, total = campo.length();
    for(a=0;a<total && !resultado;a++) if(patron.indexOf(campo.substring(a,a+1))==-1) resultado = true;
    return resultado;
    }

    private boolean esNumerico(String campo)
        {
        boolean resultado = false;
        String patron = "0123456789";
        int a, total = campo.length();
        for(a=0;a<total && !resultado;a++) if(patron.indexOf(campo.substring(a,a+1))==-1) resultado = true;
        return resultado;
        }

 /* DAG 250909
 * Se modifica toda la funcion para que valide la entrada de uno o mas emails.
 * */
	 private boolean validaEmail(){
	//MARL--Begin
//				String email = "NADIACORTES@BSANTANDER.COM.MX";
		boolean resultado=false;
//				String patron = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@.-_";
		int pos, total = email.length();

		if(total != 0){
			if(email.indexOf(';') != -1){
				int coma;
				resultado = true;
				while((coma=email.indexOf(';')) != -1){
					String valEmail = email.substring(0, coma);
					email = email.substring(coma+1);
					if((pos = valEmail.indexOf('@',0)) != -1) {
						if((valEmail.substring(0,pos)).length() > 0) {
							if((valEmail.substring(pos)).length() <= 1 )
								resultado = false;
						}else
							resultado = false;
					}else
						resultado = false;
				}
				if(email.length()>0){
					if((pos = email.indexOf('@',0)) != -1) {
						if((email.substring(0,pos)).length() > 0) {
							if((email.substring(pos)).length() <= 1 )
								resultado = false;
						}else
							resultado = false;
					}else
						resultado = false;
				}
			}else{
				if((pos = email.indexOf('@',0)) != -1) {
					if((email.substring(0,pos)).length() > 0) {
						if((email.substring(pos)).length() > 1 ) {
							resultado = true;
						}
					}
				}
			}
		}
		return resultado;
//MARL--End.
	}

    private boolean validaRFC()
        {
        int lon = (tipo == FISICA)?10:9;
        int let = (tipo == FISICA)?4:3;
        int a, anio, mes, dia;
        char car;
        boolean correcto = true;

        if(rfc == null) return false;
        if(rfc.length()!=lon) return false;
        if(contieneCaracteresNoValidos(rfc)) correcto = false;

        for(a=0;a<lon && correcto;a++)
            {
            car=rfc.charAt(a);
            if((a<let && !Character.isLetter(car)) || (a>=let && !Character.isDigit(car))) correcto = false;
            }

        if(tipo == FISICA)
            {
            StringBuffer resultado = new StringBuffer();
            String nombre = this.nombre.toUpperCase();
            String pat = this.apellidoPaterno.toUpperCase();
            String mat = this.apellidoMaterno.toUpperCase();
            String rfc1 = this.rfc.toUpperCase().substring(0, 4);
            StringBuffer aux = new StringBuffer();
            String token;
            if(mat.equals("")) mat = this.rfc.substring(2, 3);
            nombre=quitaPrep(nombre);
            pat=quitaPrep(pat);
            mat=quitaPrep(mat);
            if (pat.length()==0 || nombre.length()==0 || mat.length()==0) return false;
            resultado.append(pat.charAt(0));
            for (a=1;a<pat.length();a++)
                if (pat.charAt(a)== 'A' || pat.charAt(a)=='E' || pat.charAt(a)=='I' || pat.charAt(a)=='O' || pat.charAt(a)=='U' || pat.charAt(a)=='&')
                    aux.append(pat.charAt(a));
            if (aux.length()==0)
                {if (pat.length()>1) resultado.append(pat.charAt(1)); else resultado.append(rfc1.charAt(1));}
            else
                resultado.append(aux.charAt(0));
            resultado.append(mat.charAt(0));
            if(nombre.trim().indexOf(" ") == -1)
                {
                resultado.append(nombre.charAt(0));
                }
            else
                {
                aux = new StringBuffer();
                try
                    {
                    StringTokenizer tokens = new StringTokenizer(nombre);
                    while(tokens.hasMoreTokens())
                        {
                        token = tokens.nextToken();
                        if (!(token.equals("MARIA") || token.equals("MA") || token.equals("MA.") || token.equals("JOSE")))
                            aux.append(" " + token);
                        }
                    }
                catch(Exception e)
                    {
                    EIGlobal.mensajePorTrace("Error en ProveedorConf.java: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
                    }
                if(aux.length()>0) resultado.append(aux.charAt(1)); else resultado.append(rfc.charAt(3));
                }
            token = resultado.toString();
            if (token.equals("BUEI") || token.equals("BUEY") || token.equals("CACA") ||
                token.equals("CACO") || token.equals("CAGA") || token.equals("CAGO") ||
                token.equals("CAKA") || token.equals("CAKO") || token.equals("COGE") ||
                token.equals("COJA") || token.equals("COJE") || token.equals("COJI") ||
                token.equals("COJO") || token.equals("CULO") || token.equals("FETO") ||
                token.equals("GUEY") || token.equals("JOTO") || token.equals("KACA") ||
                token.equals("KACO") || token.equals("KAGA") || token.equals("KAGO") ||
                token.equals("KOGE") || token.equals("KOJO") || token.equals("KAKA") ||
                token.equals("KULO") || token.equals("LOCA") || token.equals("LOCO") ||
                token.equals("LOKA") || token.equals("LOKO") || token.equals("MAME") ||
                token.equals("MAMO") || token.equals("MEAR") || token.equals("MEAS") ||
                token.equals("MEON") || token.equals("MION") || token.equals("MOCO") ||
                token.equals("MULA") || token.equals("PEDA") || token.equals("PEDO") ||
                token.equals("PENE") || token.equals("PUTA") || token.equals("PUTO") ||
                token.equals("QULO") || token.equals("RATA") || token.equals("RUIN"))
                resultado.replace(3,4,"X");
            correcto = (resultado.toString().equals(rfc1));
            }

        try
            {
            anio = 1900 + Integer.parseInt(rfc.substring(let,let+2));
            mes = Integer.parseInt(rfc.substring(let+2,let+4)) - 1;
            dia = Integer.parseInt(rfc.substring(let+4,let+6));
            if(mes < 0 || mes > 11) correcto = false;
            GregorianCalendar fecha = new GregorianCalendar(anio,mes, 4);
    		if(mes==02 || mes==2){
    			if(dia > (fecha.getActualMaximum(Calendar.DAY_OF_MONTH)+1) ){
    				correcto = false;
    			}
    		}else  if(dia > fecha.getActualMaximum(Calendar.DAY_OF_MONTH)){
    			correcto = false;
    		}
            }
        catch(Exception e)
            {correcto = false;}
        return correcto;
        }

    // ---
    private String quitaPrep(String cadena)
        {
        StringBuffer buffer = new StringBuffer();
        String token;
EIGlobal.mensajePorTrace("ProveedorConf.class - Modif. Quita Prep. vers 1.0 4 de Marzo 2005 .", EIGlobal.NivelLog.INFO);
        try
            {
            StringTokenizer tokens = new StringTokenizer(cadena);
            while(tokens.hasMoreTokens())
                {
                token = tokens.nextToken();
                if (!(token.equals("") || token.equals("DEL") || token.equals("DE") || token.equals("LA") ||
                    token.equals("LOS") || token.equals("LAS") || token.equals("Y") || token.equals("MC") ||
                    token.equals("MAC") || token.equals("VON") && token.equals("VAN")))
                    buffer.append(" " + token);
                }
            }
        catch(Exception e)
            {
            EIGlobal.mensajePorTrace("Error en ProveedorConf.java: " + e.getMessage(),EIGlobal.NivelLog.DEBUG);
            }
          if (buffer.length()>1)

            {
        return buffer.toString().substring(1);
        }

         else {


       return "";
        }
		}

    // ---
    private boolean validaCuentaCLABE()
        {
        boolean retorno = true;

        // cuenta debe ser num&eacute;rica
        try
            {
            long cuenta = Long.parseLong(cuentaCLABE);
            int formaPago = Integer.parseInt(this.formaPago);

            // cuenta no debe ser nula
            if(cuentaCLABE.length() == 0) retorno = false;

            // longitud de la cuenta
            //Modif PVA 04032003if(formaPago == 2 && cuentaCLABE.length() != 18 || formaPago == 1 && cuentaCLABE.length() != 11) retorno = false;

            // validaci&oacute;n del d&iacute;gito verificador
            String pesos = "371371371371371371371371371";
            int DC = 0;
            String llaveCuenta = cuentaCLABE.substring(0,cuentaCLABE.length()-1);
            for(int i=0;i<llaveCuenta.length();i++)
                DC += (Integer.parseInt(llaveCuenta.substring(i,i+1)) * Integer.parseInt(pesos.substring(i,i+1))) % 10;

            DC = 10 - (DC % 10);
            DC = (DC >= 10) ? 0 : DC;
            llaveCuenta += "" + DC;

            if(!llaveCuenta.equals(cuentaCLABE)) retorno = false;

            }
        catch(Exception e)
            {retorno = false;}

        return retorno;
        }

   //Modificaci&oacute;n PVA 04-03-2003
   public boolean validaDigitoCuenta()
   {
       String cuenta = this.cuentaCLABE;
       int formaPago = Integer.parseInt(this.formaPago);



       String cuentaCompuesta="";
       int longitudCuenta= 11;
       int ponderado=0;
       String ponderacion="234567234567";
       int nCta=0;
       int digitoPonderacion=0;
       boolean cuentaValida=false;
       String count = "";
        try{
            cuentaCompuesta="21"+cuenta.trim();
           count =cuenta.trim();
          }
        catch(NumberFormatException e){
        cuentaValida=false;
         }

       for(int i=0; i<cuentaCompuesta.length()-1; i++){

        try{
            nCta=Integer.parseInt(String.valueOf(cuentaCompuesta.charAt(i)));
                digitoPonderacion=Integer.parseInt(String.valueOf(ponderacion.charAt(i)));
                ponderado=ponderado+(nCta*digitoPonderacion);
            }
         catch(NumberFormatException e){

         }
       }
       switch(ponderado%longitudCuenta){
           case 0:
               if (cuentaCompuesta.endsWith("0")){
                   cuentaValida=true;
                   break;
               }
               else{
                   cuentaValida=false;
                   break;
               }
           case 1:
               cuentaValida=false;
               break;
           default:
               if ((longitudCuenta-(ponderado%longitudCuenta))==Integer.parseInt(String.valueOf(count.charAt(10)))){
                   cuentaValida=true;
                   break;
               }
               else{
                   cuentaValida=false;
                   break;
               }
       }
    return cuentaValida;
   }


    // ---
    private String colocaPuntoDecimal(String valor)
        {
        while(valor.length()<3) valor = "0" + valor;
        valor = valor.substring(0,valor.length()-2) + "." + valor.substring(valor.length()-2);
        return valor;
        }

    // ---
    private String quitaPuntoDecimal(String valor)
        {
        int pos = valor.indexOf(".");
        if(pos == -1) {valor += "."; pos=valor.length();}
        while(pos+3>valor.length()) valor += "0";
        while(pos+3<valor.length()) valor = valor.substring(0,valor.length()-1);
        valor = valor.substring(0,pos) + valor.substring(pos+1);
        return valor;
        }

    // ---
    private String rellena(String cadena, int longitud, char car, boolean rellenaXizq)
        {
        if(cadena.length()>longitud) cadena = cadena.substring(0,longitud);
        if(rellenaXizq) while(cadena.length()<longitud) cadena = car + cadena;
        while(cadena.length()<longitud) cadena += "" + car;
        return cadena;
        }

    // ---
    private String corrigeMonto(String monto)
        {
        return colocaPuntoDecimal(quitaPuntoDecimal(monto));
        }

			//PPM Obtiene la clave (nume_cecoban) en base al nombre corto del banco
    String traeClaveBanco(String nombreCorto)
    {
      String dato="";
      String datotmp = "";
	  String totales="";

      int Existe=0;
	  int totalBancos=0;

	  Connection conn = null;
	  ResultSet cveBancoResult = null;
	  //System.out.println("El nombre corto del banco es: ...." + nombreCorto);
	  try{
	      conn = createiASConn(Global.DATASOURCE_ORACLE);

	      if(conn == null)
	        EIGlobal.mensajePorTrace("ProveedorConf.class - traeClaveBanco: No se puedo crear conexion.", EIGlobal.NivelLog.ERROR);

	      if(nombreCorto.trim().equals("")){
	    	  nombreCorto = "BANME";
	      }


		  String sqlcveBanco="Select NUM_CECOBAN from COMU_INTERME_FIN where TRIM(cve_interme) = '" + nombreCorto.trim() +"'";
		  EIGlobal.mensajePorTrace("ProveedorConf::traeClaveBanco:: Query->"+sqlcveBanco,EIGlobal.NivelLog.INFO);
		  PreparedStatement cveBancoQuery = conn.prepareStatement(sqlcveBanco);
	      if(cveBancoQuery==null)
		     EIGlobal.mensajePorTrace("ProveedorConf.class.class - traeClaveBanco: No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
		  cveBancoResult = cveBancoQuery.executeQuery();
	      if(!cveBancoResult.next())
	         EIGlobal.mensajePorTrace("ProveedorConf.class.class - traeClaveBanco: No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
		  else{
		 	  dato=cveBancoResult.getString(1);
		 	  if(dato==null)
		 	  	dato="";
		 	  else if(dato.trim().length()<3){
		 	  	for(int i=0; i<3-dato.trim().length();i++){
		 	  		datotmp = datotmp.concat("0");
		 	  	}
		 	  	datotmp = datotmp.concat(dato);
		 	  	dato = datotmp;
		 	  }
		 	}
		}catch( SQLException sqle ){
			//sqle.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = sqle.getStackTrace();
			EIGlobal.mensajePorTrace("Error al traer las claves de banco", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("ProveedorConf::traeClaveBanco:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ sqle.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);	 
		}
		finally
		  {
			try
			 {
			   conn.close();
			   cveBancoResult.close();
			 }catch(Exception e) {}
		  }
	EIGlobal.mensajePorTrace("El dato que estoy regresando de traeClaveBanco es: " + dato,EIGlobal.NivelLog.DEBUG);
	return dato;
    }

	public Connection createiASConn(String dbName) throws SQLException{
	Connection conn = null;
	try{
		InitialContext ic = new InitialContext ();
		DataSource ds = ( DataSource ) ic.lookup ( dbName );
		conn = ds.getConnection ();
	}catch( NamingException ne ){
		EIGlobal.mensajePorTrace("Unable create connection to: " + dbName + "; " + ne.toString (),EIGlobal.NivelLog.ERROR);
	}
	return conn;
	}

	//FIN-PPM Obtiene la clave (nume_cecoban) en base al nombre corto del banco

	private boolean validaBancoCLABE(String cuentaCLABE, String cveBanco){
        boolean resp = false;
		//System.out.println("Entrado a validaBancoCLABE ..............");
		//System.out.println("Cuenta:    " + cuentaCLABE);
		//System.out.println("cveBanco:  " + cveBanco);
        String claveBanco = cuentaCLABE.substring(0, 3);
		if(cveBanco!=null){
			if(cuentaCLABE.trim().substring(0,3).equals(cveBanco))
				resp=true;
		}
		return resp;
	}

	// Inicio-PPM-Sinapsis Validaci&oacute;n de cuentas CLABE, banco y d&iacute;gito verificador
	private boolean validaDigitoVerificador(String cuenta){
	    String pesos="371371371371371371371371371";
		int DC=0;
		String cuenta2=cuenta.substring(0,17);
		for(int i=0;i<cuenta2.length();i++){
	        DC += (Integer.parseInt(cuenta2.substring(i,i+1))) * (Integer.parseInt(pesos.substring(i,i+1))) % 10;
		}
		DC = 10 - (DC % 10);
		DC = (DC >= 10) ? 0 : DC;
		cuenta2+=DC;
		//System.out.println("Cuenta  ==> " + cuenta);
		//System.out.println("Cuenta2 ==> " + cuenta2);
		if(cuenta2.trim().equals(cuenta.trim()))
			return true;
		else
		   return false;
	}
	// Fin-PPM-Sinapsis Validaci&oacute;n de cuentas CLABE, banco y d&iacute;gito verificador

	/**
     * Split
     * crea un String[] de elementos contenidos en una trama separada por un car�cter especial.
     * @author marreguin
     * 12/05/2009
     * @param trama
     * @param Separador
     * @return
     */
    public static String[] Split(String trama,String Separador){
        char caracter='?';
        ArrayList Tokens= new ArrayList();
        StringBuffer token=new StringBuffer();
            for(int conta=0;conta<=trama.length()-1;conta++){
                caracter=trama.charAt(conta);
                    if(Separador.charAt(0)!= caracter){
                        token.append(caracter);
                    }else{
                        if(!token.toString().equals("")){
                            Tokens.add(token.toString());
                        }
                        token= new StringBuffer();

                    }
            }
//            if(!token.toString().equals("")){
//                Tokens.add(token.toString());
//                token= new StringBuffer();
//            }
        int resultSize=Tokens.size();
        String[] Result= new String[resultSize];
            for(int item=0;item<resultSize;item++){
                Result[item]=(String)Tokens.get(item);
                EIGlobal.mensajePorTrace("Split.Elemento ["+item+ "]---->"+Result[item],EIGlobal.NivelLog.DEBUG);
            }

        return Result;
    }

    /**
     * Metodo para validar la clave ABA del layout
     * @param cveAba		Valor de la clave ABA a validar
     *
     */
    private boolean validaABA(String cveAba){
    	Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		int count = 0;
		boolean flag = false;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select count(1) as count "
				  + "From camb_banco_eu "
				  + "Where cve_aba like '" + Integer.parseInt(cveAba) + "'";
			EIGlobal.mensajePorTrace ("ProveedorConf::validaABA:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);
			if(rs.next()){
				count = rs.getInt("count");
			}

			if(count>0){
				flag = true;
			}
			EIGlobal.mensajePorTrace ("ProveedorConf::validaABA:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("ProveedorConf::validaABA:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al validar la clave ABA", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("ProveedorConf::validaABA:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return flag;
	}

	public String getDatosAdic() {
		return datosAdic;
	}

	public void setDatosAdic(String datosAdic) {
		this.datosAdic = datosAdic;
	}

	public String getOrigenProveedor() {
		return origenProveedor;
	}

	public void setOrigenProveedor(String origenProveedor) {
		this.origenProveedor = origenProveedor;
	}
}