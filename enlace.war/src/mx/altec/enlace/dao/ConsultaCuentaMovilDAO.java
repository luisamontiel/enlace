package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

public class ConsultaCuentaMovilDAO extends GenericDAO {


	/**
	 * QUERY PARA OBTENER LAS CUENTAS MOVILES
	 */
	/*private static final String SELECT_CUENTAS = new StringBuilder().append("SELECT NOMBRE, NUM_CUENTA_EXT, CVE_INTERME ")
	 .append("FROM EWEB_CAT_NOM_EXTERNA ")
	 .append("WHERE LENGTH(trim(NUM_CUENTA_EXT))=10 ")
	 .append("AND NUM_CUENTA2='"+session.getContractNumber().trim()+"'")
	 .append("ORDER BY NUM_CUENTA_EXT").toString();*/

	/**
	 * QUERY PARA OBTNER LA DESCRIPCION DEL BANCO
	 */
	private static final String SELECT_BANCO = new StringBuilder().append("SELECT NOMBRE_CORTO FROM COMU_INTERME_FIN ")
	.append("WHERE TRIM(CVE_INTERME) = TRIM('%s')").toString();


	/**
	 * QUERY PARA OBTNER LA CLAVE DEL BANCO
	 */
	private static final String SELECT_CVEBANCO = new StringBuilder().append("SELECT NUM_CECOBAN FROM COMU_INTERME_FIN ")
	.append("WHERE TRIM(CVE_INTERME) = TRIM('%s')").toString();

	/**
	 * Realiza la consulta de las cuentas moviles registradas
	 * @return resultado : resultado
	 */
	public List<CuentaMovilBean> consultaEdoCta(String contratoActual){
		List<CuentaMovilBean> cuentacon = null;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		String query;


		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE2);

			if(conn != null){
				cuentacon  = new ArrayList<CuentaMovilBean>();
				String SELECT_CUENTAS = new StringBuilder().append("SELECT NOMBRE, NUM_CUENTA_EXT, CVE_INTERME ")
				.append("FROM EWEB_CAT_NOM_EXTERNA ")
				.append("WHERE LENGTH(trim(NUM_CUENTA_EXT))=10 ")
				.append("AND NUM_CUENTA2='"+contratoActual.trim()+"'")
				.append("ORDER BY NUM_CUENTA_EXT").toString();
				query = String.format(SELECT_CUENTAS);

				EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO ---> QUERY CUENTAS MOVILES " + query, EIGlobal.NivelLog.DEBUG);

				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				 while (result.next ()) {
					 CuentaMovilBean resultados = new CuentaMovilBean();
					 resultados.setNumeroMovil(result.getString("NUM_CUENTA_EXT"));
					 resultados.setTitular(result.getString("NOMBRE"));
					 resultados.setCveBanco(obtenCveBanco(result.getString("CVE_INTERME")));
					 resultados.setDescBanco(obtenNombreBanco(result.getString("CVE_INTERME")));
//					 resultados.setTipoCuenta(result.getString("CVE_INTERME").trim().equals("BANME")?"Santander":"Otros Bancos Nacionales");
					 resultados.setTipoCuenta("BANME".equals(result.getString("CVE_INTERME").trim())?"Santander":"Otros Bancos Nacionales");

					 EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO -> consultaCuentas() --> MOVIL:" + resultados.getNumeroMovil(), EIGlobal.NivelLog.INFO);
			         EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO -> consultaCuentas() -->  TIPO:" + resultados.getTipoCuenta(), EIGlobal.NivelLog.INFO);
			         EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO -> consultaCuentas() --> TITULAR:" + resultados.getTitular(), EIGlobal.NivelLog.INFO);
			         EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO -> consultaCuentas() --> BANCO:" + resultados.getDescBanco(), EIGlobal.NivelLog.INFO);
			         EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO -> consultaCuentas() --> CVE BANCO:" + resultados.getCveBanco(), EIGlobal.NivelLog.INFO);

					 cuentacon.add(resultados);
				 }
			}
		}catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return cuentacon;
	}

	/**
	 *
	 * Obtiene el nombre del banco
	 * @param cve_interme : cve_interme
	 * @return nombreBanco
	 */
	protected String obtenNombreBanco(String cve_interme) {
		String nombreBanco = "";
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		String query;

		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);

			if(conn != null){
				query = String.format(SELECT_BANCO,cve_interme);

				EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO ---> QUERY DESCRIPCION BANCO " + query, EIGlobal.NivelLog.DEBUG);

				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				 while (result.next ()) {
					nombreBanco = result.getString("NOMBRE_CORTO").trim();
				 }
			}
		}catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return nombreBanco;
	}


	/**
	 *
	 * Obtiene clave del banco
	 * @param cve_interme : cve_interme
	 * @return cveBanco
	 */
	protected String obtenCveBanco(String cve_interme) {
		String cveBanco = "";
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		String query;

		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);

			if(conn != null){
				query = String.format(SELECT_CVEBANCO,cve_interme);

				EIGlobal.mensajePorTrace("ConsultaCuentaMovilDAO ---> QUERY CLAVE BANCO " + query, EIGlobal.NivelLog.DEBUG);

				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				 while (result.next ()) {
					 cveBanco = result.getString("NUM_CECOBAN").trim();
				 }
			}
		}catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return cveBanco;
	}

}