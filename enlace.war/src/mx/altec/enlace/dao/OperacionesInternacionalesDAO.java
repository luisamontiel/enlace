/**
 *
 */
package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.TR_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.TR_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.TR_JNDI_QUEUE_RESPONSE;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.naming.NamingException;

import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.utilerias.EIGlobal;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author FSW Indra
 *
 */
public class OperacionesInternacionalesDAO {

	/**
	 * Envia la operacion a eTrasfer Plus
	 * @param operacion transferencia
	 * @return String[] arreglo con el resultado de la operacion
	 */
	public String[] enviaOperacionInternacional (String operacion) {
		String[] resultado = new String[3];
		MQQueueSession mQ = null;
		String respuestaMq = "";

		EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: enviaOperacionInternacional :: TRAMA ENTRADA <"
						+ operacion+ ">", EIGlobal.NivelLog.INFO);

		if (validaConexion()){
			try {
				mQ = new MQQueueSession(TR_JNDI_CONECTION_FACTORY,
				TR_JNDI_QUEUE_REQUEST , TR_JNDI_QUEUE_RESPONSE );
			}catch (NamingException e) {
				EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: enviaOperacionInternacional :: NamingException: "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}catch (JMSException e) {
				EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: enviaOperacionInternacional :: JMSException: "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}
		} else {
			resultado[0] = "";
			resultado[1] = "";
			resultado[2] = "";
			return resultado;
		}

		try {
			respuestaMq = mQ.enviaRecibeMensaje(operacion);

			EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: enviaOperacionInternacional :: TRAMA SALIDA <"
							+ respuestaMq + ">", EIGlobal.NivelLog.INFO);

			Map<String, Object> result = new HashMap<String, Object>();
			result = new ObjectMapper().readValue(respuestaMq,  new TypeReference<Map<String, String>>(){});

			resultado[0] = result.get("COD_RESP").toString();
			resultado[1] = result.get("REFERENCIA").toString();
			resultado[2] = result.get("MSG_RESP").toString();

		} catch (IOException e) {
			EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: enviaOperacionInternacional :: IOException: "
					+ e.getMessage(), EIGlobal.NivelLog.INFO);
			resultado[0] = "";
			resultado[1] = "";
			resultado[2] = "";
		} catch (MQQueueSessionException e) {
			EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: enviaOperacionInternacional :: MQQueueSessionException: "
							+ e.getMessage(), EIGlobal.NivelLog.INFO);
			resultado[0] = "";
			resultado[1] = "";
			resultado[2] = "";
		} finally {
			if (mQ != null) {
				mQ.close();
			}
		}

		return resultado;
	}

	/**
	 * Funcion para validar conexion MQ.
	 * @return boolean resultadoConexion
	 */
	private boolean validaConexion(){
		boolean resultadoConexion = false;
		boolean conexionFactory = false;
		boolean conexionRequest = false;
		boolean conexionResponse = false;

		EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: validaConexion :: TR_JNDI_CONECTION_FACTORY: " + TR_JNDI_CONECTION_FACTORY, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: validaConexion :: TR_JNDI_QUEUE_REQUEST: " + TR_JNDI_QUEUE_REQUEST, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: validaConexion :: TR_JNDI_QUEUE_RESPONSE: " + TR_JNDI_QUEUE_RESPONSE, EIGlobal.NivelLog.INFO);

		conexionFactory = (TR_JNDI_CONECTION_FACTORY != null && !"".equals(TR_JNDI_CONECTION_FACTORY)) ? true : false;
		conexionRequest = (TR_JNDI_QUEUE_REQUEST != null && !"".equals(TR_JNDI_QUEUE_REQUEST)) ? true : false;
		conexionResponse = (TR_JNDI_QUEUE_RESPONSE != null && !"".equals(TR_JNDI_QUEUE_RESPONSE)) ? true : false;

		if (conexionFactory && conexionRequest && conexionResponse) {
			resultadoConexion = true;
		}
		EIGlobal.mensajePorTrace("***OperacionesInternacionalesDAO :: validaConexion :: ResultadoConexion: " + resultadoConexion, EIGlobal.NivelLog.INFO);

		return resultadoConexion;
	}
}