package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.Global.NP_MQ_USUARIO;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import mx.altec.enlace.beans.NomPreBuilder;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

public abstract class NomPreMQDAO {

	protected <T> T consulta(String header, String valores, NomPreBuilder<T> builder) {

		T result = null;
		String respuesta = null;
		MQQueueSession sess = null; 
		try {							
			sess = new MQQueueSession(
					NP_JNDI_CONECTION_FACTORY, 
					NP_JNDI_QUEUE_RESPONSE, 
					NP_JNDI_QUEUE_REQUEST);
			
			StringBuffer trama = new StringBuffer()
				.append(rellenar(NP_MQ_TERMINAL, 4))
				.append(rellenar(NP_MQ_USUARIO, 8))
				.append(header)
				.append(valores);						
			
			EIGlobal.mensajePorTrace("Configuracion JMS NP: " + NP_JNDI_CONECTION_FACTORY + ", " + NP_JNDI_QUEUE_REQUEST + ", " + NP_JNDI_QUEUE_RESPONSE, EIGlobal.NivelLog.INFO);
			
			respuesta = sess.enviaRecibeMensaje(trama.toString());			
						
		} catch (Exception e) {
			
			EIGlobal.mensajePorTrace("Error NomPreMQDAO: " + e.getMessage(), EIGlobal.NivelLog.ERROR);	
		}finally{
			if(sess!=null){
				sess.close();
				sess = null;
			}
		}
		
		if (respuesta != null && respuesta.length() > 0) {
			
			result = builder.build(respuesta);			
			
		} else {
			
			EIGlobal.mensajePorTrace("Sin trama de respuesta", EIGlobal.NivelLog.INFO);
		}
		
		return result;
	}
	
	
}
