package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import mx.altec.enlace.beans.AdminHorariosBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

/**
 *
 * @author Z712236
 *
 */
public class AdminHorariosDAO {
	/**
	 * statement
	 */
	private static PreparedStatement statement = null;

	/**
	 * Consulta el horario de una funcionalidad
	 * @param adminHorariosBean parametro bean
	 * @return bean de resultado
	 * @throws SQLException descripcion problema
	 */
	public AdminHorariosBean consultarHorario(AdminHorariosBean adminHorariosBean) throws SQLException {
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: consultarHorario() :: Iniciando consulta de horario", EIGlobal.NivelLog.INFO);
		/**
		 * resultado
		 */
		AdminHorariosBean resultado=new AdminHorariosBean();
		/**
		 * coneccion bd
		 */
		Connection conn = null;
		/**
		 * coneccion bd
		 */
		ResultSet rs = null;
		/**
		 * coneccion bd
		 */
		String[] lstrValoresQuery_1 = new String[2];
	    String[] lstrValoresQuery_2 = new String[1];
		try {

			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);

			lstrValoresQuery_1[0] = adminHorariosBean.getNombreServlet();
			lstrValoresQuery_1[1] = "S";
			lstrValoresQuery_1[2] = " ";
			lstrValoresQuery_2[0] = adminHorariosBean.getIdFunc();
			lstrValoresQuery_2[1] = "S";
		    if(adminHorariosBean.getIdFunc() == null || "".equals(adminHorariosBean.getIdFunc().trim())){
			for(int i=0; i < lstrValoresQuery_1.length; i++)
			{
				statement.setString(i+1,lstrValoresQuery_1[i]);
			}
		    }else {
			for(int j=0; j < lstrValoresQuery_2.length; j++)
			{
				statement.setString(j+1,lstrValoresQuery_2[j]);
			}
		    }
			final String query = armarqueryConsulta(adminHorariosBean);
			statement = conn.prepareStatement(query);
			EIGlobal.mensajePorTrace("Query:: " + query, EIGlobal.NivelLog.INFO);

			//Se ejecuta consulta y se obtienen datos del resultado
			resultado=obtenerDatosResultado(rs);

		} catch (SQLException e1) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarHorario() - SQLException "+ "Se controla detalle al cerrar conexion: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
		} catch (IllegalStateException e) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarHorario() - IllegalStateException "+ "Se controla detalle al cerrar conexion.:. [" + e + "]", EIGlobal.NivelLog.ERROR);
		}catch (Exception e2) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarHorario() - Exception "+ "Se controla detalle al cerrar conexion.:. ", EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (rs != null){
					rs.close();
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarHorario() - SQLException "+ "Se controla detalle al cerrar conexion::: [" +new Formateador().formatea(e1) + "]", EIGlobal.NivelLog.ERROR);
			}catch (Exception e) {
				EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarHorario() - Exception "+ "Se controla detalle al cerrar conexion::: "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace("AdminHorariosDAO - saliendo de consultarHorario():::", EIGlobal.NivelLog.INFO);
		}
		return resultado;
	}

	/**
	 * arma query de consulta
	 * @param consulta parametro bean
	 * @return query cadena consulta
	 */
	private String armarqueryConsulta(AdminHorariosBean consulta) {
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: armarqueryConsulta() :: generando query ", EIGlobal.NivelLog.INFO);

		/**
		 * query a ejecutar
		 */
		final StringBuffer query = new StringBuffer();
		query.append( "SELECT FLG_ACTIVO AS ACTIVO, ID_FUNC_PK AS ID_FUNC, DSC_FUNC AS DESC_FUNC, " );
		query.append( "HOR_INI_SEM AS HORA_INI_SEM, HOR_FIN_SEM AS HORA_FIN_SEM, HOR_INI_SAB AS HORA_INI_SAB, " );
		query.append( "HOR_FIN_SAB AS HORA_FIN_SAB, HOR_INI_DOM AS HORA_INI_DOM, HOR_FIN_DOM AS HORA_FIN_DOM, " );
		query.append( "FLG_LUN AS LUNES, FLG_MAR AS MARTES, FLG_MIE AS MIERCOLES, FLG_JUE AS JUEVES, FLG_VIE AS VIERNES, " );
		query.append( "FLG_SAB AS SABADO, FLG_DOM AS DOMINGO, FLG_DIA_INHABIL AS DIA_INHABIL, TO_CHAR(SYSDATE, 'D') AS DIA, TO_CHAR(SYSDATE, 'HH24MI') AS HR_ACTUAL " );
		query.append( "FROM EWEB_ADMIN_HORARIOS ");
		if(consulta.getIdFunc() == null || "".equals(consulta.getIdFunc().trim())){
			query.append("WHERE DSC_ENL_CLASS=?'");
			//query.append(consulta.getNombreServlet());
			query.append("' AND FLG_ACTIVO=?");
			query.append(" AND ID_FUNC_PK=?" );
		}else {
			query.append( "WHERE ID_FUNC_PK=?'" );
			//query.append( consulta.getIdFunc() );
			query.append( "' AND FLG_ACTIVO=?" );
		}

		return query.toString();
	}

	/**
	 * Obtiene datos del resultado
	 * @param rs parametro de resultado
	 * @return bean de resultado
	 * @throws SQLException descripcion problema
	 */
	private AdminHorariosBean obtenerDatosResultado(ResultSet rs) throws SQLException{
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: obtenerDatosResultado() :: Obteniendo el resultado de la consulta", EIGlobal.NivelLog.INFO);
		/**
		 * resultado
		 */
		final AdminHorariosBean resultado = new AdminHorariosBean();

		rs = statement.executeQuery();
		try {
			if(rs.next()){
				resultado.setActivo(rs.getString("ACTIVO") != null ? rs.getString("ACTIVO") : "");
				resultado.setIdFunc(rs.getString("ID_FUNC") != null ? rs.getString("ID_FUNC") : "");
				resultado.setDescFunc(rs.getString("DESC_FUNC") != null ? rs.getString("DESC_FUNC") : "");
				resultado.setHoraIniSem(rs.getString("HORA_INI_SEM") != null ? rs.getString("HORA_INI_SEM") : "");
				resultado.setHoraFinSem(rs.getString("HORA_FIN_SEM") != null ? rs.getString("HORA_FIN_SEM") : "");
				resultado.setHoraIniSab(rs.getString("HORA_INI_SAB") != null ? rs.getString("HORA_INI_SAB") : "");
				resultado.setHoraFinSab(rs.getString("HORA_FIN_SAB") != null ? rs.getString("HORA_FIN_SAB") : "");
				resultado.setHoraIniDom(rs.getString("HORA_INI_DOM") != null ? rs.getString("HORA_INI_DOM") : "");
				resultado.setHoraFinDom(rs.getString("HORA_FIN_DOM") != null ? rs.getString("HORA_FIN_DOM") : "");
				resultado.setLunes(rs.getString("LUNES") != null ? rs.getString("LUNES") : "");
				resultado.setMartes(rs.getString("MARTES") != null ? rs.getString("MARTES") : "");
				resultado.setMiercoles(rs.getString("MIERCOLES") != null ? rs.getString("MIERCOLES") : "");
				resultado.setJueves(rs.getString("JUEVES") != null ? rs.getString("JUEVES") : "");
				resultado.setViernes(rs.getString("VIERNES") != null ? rs.getString("VIERNES") : "");
				resultado.setSabado(rs.getString("SABADO") != null ? rs.getString("SABADO") : "");
				resultado.setDomingo(rs.getString("DOMINGO") != null ? rs.getString("DOMINGO") : "");
				resultado.setDiaInhabil(rs.getString("DIA_INHABIL") != null ? rs.getString("DIA_INHABIL") : "");
				resultado.setDiaSistema(rs.getString("DIA") != null ? rs.getString("DIA") : "");
				//resultado.setHoraActual(rs.getString("HR_ACTUAL") != null ? rs.getString("HR_ACTUAL") : "");
				resultado.setTieneHorarioActivo(true);
			}else{
				resultado.setTieneHorarioActivo(false);
			}

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: obtenerDatosResultado() SQLException :: Se controla detalle al obtener los datos del ResulSet ", EIGlobal.NivelLog.INFO);
		}catch (IllegalStateException e) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: obtenerDatosResultado() Exception :: Se controla detalle al obtener los datos del ResulSet... ", EIGlobal.NivelLog.INFO);
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: obtenerDatosResultado() :: Se controla detalle al obtener los datos del ResulSet.... ", EIGlobal.NivelLog.INFO);
		}finally {
			try {
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("AdminHorariosDAO - obtenerDatosResultado() SQLException - "+ "Se controla detalle al cerrar conexion:: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
			}catch (Exception e2) {
				EIGlobal.mensajePorTrace("AdminHorariosDAO - obtenerDatosResultado() Exception - "+ "Se controla detalle al cerrar conexion::", EIGlobal.NivelLog.ERROR);
			}
		}

		return resultado;
	}

	/**
	 * consulta si el dia el inhabil
	 * @param adminHorariosBean parametro bean
	 * @return bean de resultado
	 * @throws SQLException descripcion problema
	 */
	public AdminHorariosBean consultarDiaInhabil(AdminHorariosBean adminHorariosBean) throws SQLException {
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: consultarDiaInhabil() :: Iniciando consulta de dia inhabil", EIGlobal.NivelLog.INFO);
		/**
		 * coneccion bd
		 */
		Connection conn = null;
		/**
		 * coneccion bd
		 */
		ResultSet rs = null;

		String totalRegistros="";
		try {

			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);
			final String query = armarqueryConsultaDiaIhabil();
			statement = conn.prepareStatement(query);
			EIGlobal.mensajePorTrace("Query:: " + query, EIGlobal.NivelLog.INFO);

			//Se ejecuta consulta y se obtienen datos del resultado
			rs = statement.executeQuery();
			if(rs.next()){
				totalRegistros=rs.getString("TOTAL") != null ? rs.getString("TOTAL") : "0";
			}
			EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: consultarDiaInhabil() :: totalRegistros: "+totalRegistros, EIGlobal.NivelLog.INFO);
		} catch (SQLException e1) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarDiaInhabil() - SQLException "+ "Se controla detalle al cerrar conexion: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
		} catch (IllegalStateException e) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarDiaInhabil() - IllegalStateException "+ "Se controla detalle al cerrar conexion.:. [" + e + "]", EIGlobal.NivelLog.ERROR);
		}catch (Exception e2) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarDiaInhabil() - Exception "+ "Se controla detalle al cerrar conexion.:. ", EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (rs != null){
					rs.close();
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarDiaInhabil() - SQLException "+ "Se controla detalle al cerrar conexion::: [" +new Formateador().formatea(e1) + "]", EIGlobal.NivelLog.ERROR);
			}catch (Exception e) {
				EIGlobal.mensajePorTrace("AdminHorariosDAO - consultarDiaInhabil() - Exception "+ "Se controla detalle al cerrar conexion::: "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace("AdminHorariosDAO - saliendo de consultarDiaInhabil():::", EIGlobal.NivelLog.INFO);
		}

		adminHorariosBean.setDiaInhabil(totalRegistros);
		return adminHorariosBean;
	}

	/**
	 * Arma query de consulta dia inhabil
	 * @return cadena consulta
	 */
	private String armarqueryConsultaDiaIhabil() {
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: armarqueryConsultaDiaIhabil() :: generando query ", EIGlobal.NivelLog.INFO);

		/**
		 * query a ejecutar
		 */
		final StringBuffer query = new StringBuffer();
		query.append( "SELECT COUNT(1) AS TOTAL " );
		query.append( "FROM COMU_DIA_INHABIL " );
		query.append( "WHERE FECHA=TO_DATE(SYSDATE, 'dd/mm/yy') " );
		query.append( "AND CVE_PAIS='MEX'" );

		return query.toString();
	}

	/**
	 * Realiza la consulta para verificar si el nombre de un  servlet esta asociado con la funcionalidad de validacion.
	 * @param adminHorariosBean Contiene los parametros necesarios para realizar la funcion.
	 * @return Bean que contiene el resultado de la consulta.
	 * @throws SQLException Excepcion generada en caso de ocurrir un error al ejecutar la consulta.
	 */
	public AdminHorariosBean consultaNombreServlet(AdminHorariosBean adminHorariosBean) throws SQLException {
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: consultaNombreServlet() :: Iniciando consulta de Nombre de Clase", EIGlobal.NivelLog.INFO);
		AdminHorariosBean resultado = new AdminHorariosBean();
		Connection conn = null;
		ResultSet rs = null;

		try {
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);
			final String query = armaQueryConsultaServlet(adminHorariosBean);
			statement = conn.prepareStatement(query);
			EIGlobal.mensajePorTrace("Query:: " + query, EIGlobal.NivelLog.INFO);
			rs = statement.executeQuery();
			resultado = obtenerResultadoConsultaServlet(rs);
		} catch (SQLException ex) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultaNombreServlet() - SQLException "+ "Se controla detalle al cerrar conexion: [" + ex + "]", EIGlobal.NivelLog.ERROR);
		} catch (IllegalStateException ex) {
			EIGlobal.mensajePorTrace("AdminHorariosDAO - consultaNombreServlet() - IllegalStateException "+ "Se controla detalle al cerrar conexion.:. [" + ex + "]", EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (rs != null){
					rs.close();
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace("AdminHorariosDAO - consultaNombreServlet() - SQLException "+ "Se controla detalle al cerrar conexion::: [" + ex + "]", EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace("AdminHorariosDAO - saliendo de consultarHorario():::", EIGlobal.NivelLog.INFO);
		}
		return resultado;
	}

	/**
	 * Arma el query para la consulta de nombre se Servlet.
	 * @param adminHorariosBean Bean que contiene los parametros necesarios para realizar la consulta.
	 * @return Cadena que contiene el query de la consulta de servlet.
	 */
	private String armaQueryConsultaServlet(AdminHorariosBean adminHorariosBean){
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: armaQueryConsultaServlet() :: generando query ", EIGlobal.NivelLog.INFO);
		final StringBuilder consulta = new StringBuilder();
		consulta.append("SELECT FLG_ACTIVO AS ACTIVO, ID_FUNC_PK AS ID_FUNC ")
		.append("FROM EWEB_ADMIN_HORARIOS ")
		.append("WHERE CLASS_NAME = '").append(adminHorariosBean.getNombreServlet()).append("'");

		return consulta.toString();
	}

	/**
	 * Obtiene datos del resultado retornado por la consulta de servlet.
	 * @param rs parametro de resultado
	 * @return Bean con resultado de consulta.
	 * @throws SQLException Excepcion generada al obtener los datos.
	 */
	private AdminHorariosBean obtenerResultadoConsultaServlet(ResultSet rs) throws SQLException{
		EIGlobal.mensajePorTrace("AdminHorariosDAO.java :: obtenerDatosResultado() :: Obteniendo el resultado de la consulta", EIGlobal.NivelLog.INFO);
		final AdminHorariosBean horariosBean = new AdminHorariosBean();
		if(rs.next()){
			horariosBean.setActivo(rs.getString("ACTIVO") != null ? rs.getString("ACTIVO") : "");
			horariosBean.setIdFunc(rs.getString("ID_FUNC") != null ? rs.getString("ID_FUNC") : "");
		}
		return horariosBean;
	}
}