package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConfEdosCtaArchivoDatosBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class ConfEdosCtaMasArchivoDAO extends GenericDAO  {
	
	/**SELECT_NEXT_VAL**/
	private static final String SELECT_NEXT_VAL = "select SEQ_EDOCTA_PDFXML.nextval from dual";
	/** LOG TAG**/
	private static final String LOG_TAG = "[EDCPDFXML] ::: ConfEdosCtaMasArchivoDAO ::: ";	
	/**Connection**/
	private transient Connection conn=null;
	
	/**
	 * @param st : Statement
	 */
	private static void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(
						"DetalleCuentasDAO:: Error al cerrar el statement"
								+ ex.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}
	
	/** getSecuencia
	 * @return int: secuencia
	 * @throws SQLException : exception
	 */
	private int getSecuencia () throws SQLException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		int secuencia = 0;
		try {		
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			pst = conn.prepareStatement(SELECT_NEXT_VAL);
			rs = pst.executeQuery();
			while (rs.next()) {
				secuencia = rs.getInt("NEXTVAL");
			} 
			return secuencia;
		} finally {
			try{
				if (rs!=null) {
					rs.close();
				}
				if (conn!=null){
					conn.close();
				}
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
					String.format("%s Error Cerrando Recursos %s",LOG_TAG,e1.toString()),NivelLog.ERROR);
			}			
		}
	}
	
	/**
	 * Metodo que inserta los registros
	 * @param datos : bean con datos a insertar
	 * @return boolean : booelan
	 * @throws SQLException : exception
	 */
	public Map<String, String> insertarRegistros(ConfEdosCtaArchivoDatosBean datos) throws SQLException {
		
		Map<String, String> resultado = new HashMap<String, String>();
		
		List<ConfEdosCtaArchivoBean> listaDatos = new ArrayList<ConfEdosCtaArchivoBean>();
		listaDatos = datos.getListaDatos();
		int secuencia = getSecuencia();
		PreparedStatement ps =null;
		
		conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);	
		conn.setAutoCommit(false);
		
		EIGlobal.mensajePorTrace("ConfEdosCtaMasArchivoDAO::insertar -> Conexion creada = " + conn, EIGlobal.NivelLog.INFO);		
		StringBuilder insert= new StringBuilder("");
		insert.append("INSERT INTO EWEB_EDO_CTA_CTRL (ID_EDO_CTA_CTRL, NUM_CUENTA2, ID_USUARIO, FCH_ENVIO, ESTATUS) ").
				append("VALUES (?,?,?,?,?)");
		ps=conn.prepareStatement(insert.toString());
		ps.setInt(1, secuencia);
		ps.setString(2, datos.getNumeroCuenta());
		ps.setString(3, datos.getIdUsuario());
		ps.setString(4, getFecha());
		ps.setString(5, "R");		
		ps.addBatch();
		ps.executeBatch();
		
		EIGlobal.mensajePorTrace("QUERY ["+insert+"]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("VALUES ["+secuencia+",'"+
											datos.getNumeroCuenta()+"','"+
											datos.getIdUsuario()+"','"+
											getFecha()+"',"+
											"'R'"+"]", EIGlobal.NivelLog.DEBUG);
		
		StringBuilder insertList= new StringBuilder("");
		insertList.append("INSERT INTO EWEB_EDO_CTA_DET (ID_EDO_CTA_DET, ID_EDO_CTA_CTRL , NUM_CUENTA , ")
				.append("ESTATUS, PPLS, TIPO_EDO_CTA) VALUES")
				.append(" (?,?,?,?,?,?)");
		ps=conn.prepareStatement(insertList.toString());
		int i = 1;
		EIGlobal.mensajePorTrace("QUERY ["+insertList+"]", EIGlobal.NivelLog.DEBUG);

		for(ConfEdosCtaArchivoBean bean : listaDatos) {
			ps.setInt(1, i);
			ps.setInt(2, secuencia);
			ps.setString(3, bean.getNomCuenta());
			ps.setString(4, "N");
			ps.setString(5, String.valueOf(bean.getPapaerless()).toUpperCase());
			ps.setString(6, 
					bean.getTipoMasivo());
			ps.addBatch();
			EIGlobal.mensajePorTrace("VALUES ["+i+","+
					secuencia+",'"+
					bean.getNomCuenta()+"',"+
					"'N'"+",'"+
					String.valueOf(bean.getPapaerless()).toUpperCase()+"',"+
					bean.getTipoMasivo()+"]", EIGlobal.NivelLog.DEBUG);
			i++;
		}
		EIGlobal.mensajePorTrace("ConfEdosCtaMasArchivoDAO::insertar -> INSERT EWEB_CONTROL_EMP OK\n", EIGlobal.NivelLog.INFO);
		ps.executeBatch();
		EIGlobal.mensajePorTrace("Se insertaron ["+ps.getUpdateCount()+"] registros", EIGlobal.NivelLog.DEBUG);
		conn.commit();
		cierraConexion(conn);
		close(ps);
		resultado.put("secuencia", String.valueOf(secuencia));
		EIGlobal.mensajePorTrace("Secuencia ["+secuencia+"]", EIGlobal.NivelLog.DEBUG);
		return (HashMap<String, String>) resultado;
	}	
	
	/**
	 * @return fecha : fecha del sistema
	 */
	public String getFecha() {
		Date fechaHrAct = new Date();
	    SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy", Locale.US);
		
		return fdate.format(fechaHrAct);
	}
	
	
}