package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.sql.Connection;
import java.sql.PreparedStatement;

import mx.altec.enlace.beans.AsignacionBean;
import mx.altec.enlace.beans.LMXC;
import mx.altec.enlace.beans.LMXD;
import mx.altec.enlace.beans.LMXE;
import mx.altec.enlace.beans.LMXF;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * @author ESC
 * Clase que ejecuta el llamado a las transacciones LMXC, LMXD, LMXE y LMX8 mediante conexion MQ
 */
public class AdmonRemesasDAO extends AdmonUsuariosMQDAO {

	private static final String textoLog = AdmonRemesasDAO.class.getName();
	private PreparedStatement statement = null;
	private Connection conn = null;


	/**
	 * Metodo para generar la consulta de remesas
	 * @param bean para la transacci&oacute;n LMXC
	 * @return lista de remesas
	 */
	public LMXC consultaRemesas(LMXC bean) {
		StringBuffer sb = new StringBuffer()
		.append(rellenar(bean.getEntidad(), 4)) 			//ENTIDAD
		.append(rellenar(bean.getContratoEnlace(), 12, '0', 'I'))	//CONTRATO-ENLACE
		.append(rellenar(bean.getCtroDistribucion(), 6))	//CONTRATO-DISTRIBUCION
		.append(rellenar(bean.getNumeroRemesa(), 12))	//NUM-REMESA
		.append(rellenar(bean.getEstadoRemesa(), 1))	//ESTADO-REMESA
		.append(rellenar(bean.getFechaInicio(), 10))	//FECHA-INICIO
		.append(rellenar(bean.getFechaFinal(),10))		//FECHA-FIN
		.append(rellenar(bean.getCtoEnlacePag(), 12, '0', 'I'))
		.append(rellenar(bean.getCtroDistribucionPag(),6))
		.append(rellenar(bean.getNumRemesaPag(),12));
		EIGlobal.mensajePorTrace(textoLog + "LMXC - Consulta Remesas", EIGlobal.NivelLog.INFO);
		return consulta(LMXC.HEADER, sb.toString(), LMXC.getFactoryInstance());
	}

	/**
	 * Metodo para recibir las remesas
	 * @param bean para la transacci&oacute;n LMXD
	 * @return exito operaci&oacute;n
	 */
	public LMXD recibirRemesa(LMXD bean) {
		StringBuffer sb = new StringBuffer()
		.append(rellenar(bean.getEntidad(), 4)) 			//ENTIDAD
		.append(rellenar(bean.getContratoEnlace(), 12, '0', 'I'))	//CONTRATO-ENLACE
		.append(rellenar(bean.getCtroDistribucion(), 6))	//CONTRATO-DISTRIBUCION
		.append(rellenar(bean.getNumeroRemesa(), 12))	//NUM-REMESA
		.append(rellenar(bean.getEstadoRemesa(), 1));	//ESTADO-REMESA
		EIGlobal.mensajePorTrace(textoLog + "LMXD - Recibir Remesa", EIGlobal.NivelLog.INFO);
		return consulta(LMXD.HEADER, sb.toString(), LMXD.getFactoryInstance());
	}

	/**
	 * Metodo para realizar la consulta del detalle de remesas
	 * @param bean para la transacci&oacute;n LMXE
	 *
	 */
	public LMXE consultaDetalleRemesa(LMXE bean) {
		StringBuffer sb = new StringBuffer()
		.append(rellenar(bean.getEntidad(), 4)) 			//ENTIDAD
		.append(rellenar(bean.getContratoEnlace(), 12, '0', 'I'))	//CONTRATO-ENLACE
		.append(rellenar(bean.getCtroDistribucion(), 6))	//CONTRATO-DISTRIBUCION
		.append(rellenar(bean.getNumeroRemesa(), 12))	//NUM-REMESA
		.append(rellenar(bean.getEstadoTarjeta(), 1))	//ESTADO-TARJETA
		.append(rellenar(bean.getTarjeta(), 22));	//TARJETA
		EIGlobal.mensajePorTrace(textoLog + "LMXE - consulta Detalle Remesa", EIGlobal.NivelLog.INFO);
		return consulta(LMXE.HEADER, sb.toString(), LMXE.getFactoryInstance());
	}

	/**
	 * Metodo para la cancelacion de tarjetas
	 * @param bean para la transacci&oacute;n LMX8
	 *
	 */
	public LMXF cancelarTarjeta(LMXF bean) {
		StringBuffer sb = new StringBuffer()
		.append(rellenar(bean.getEntidad(), 4)) 			//ENTIDAD
		.append(rellenar(bean.getContratoEnlace(), 12, '0', 'I'))	//CONTRATO-ENLACE
		.append(rellenar(bean.getCtroDistribucion(), 6))	//CONTRATO-DISTRIBUCION
		.append(rellenar(bean.getNumeroRemesa(), 12))	//NUMERO REMESA
		.append(rellenar(bean.getTarjeta(), 22))	//TARJETA
		.append(rellenar(bean.getEstadoTarjeta(), 1));	//ESTADO-TARJETA


		EIGlobal.mensajePorTrace(textoLog + "LMXF - cancelar Tarjeta", EIGlobal.NivelLog.INFO);
		return consulta(LMXF.HEADER, sb.toString(), LMXF.getFactoryInstance());
	}

	public int cancelartarjetaEnlace(
			AsignacionBean consulta) throws Exception {

		EIGlobal.mensajePorTrace("AdmonRemesasDAO - cancelartarjetaEnlace() - Cnaclenando Tarjeta Enlace", EIGlobal.NivelLog.INFO);

		int resultado = 0;

		try {
			/************ ACTUALIZA ESTATUS EN DETALLE ************/
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			String query = armarquerydetalle(consulta);
			statement = conn.prepareStatement(query.toString());
			EIGlobal.mensajePorTrace("Query: " + query, EIGlobal.NivelLog.INFO);
			resultado = statement.executeUpdate(query);

			conn.commit();

		} catch (Exception e) {
			EIGlobal.mensajePorTrace("AdmonRemesasDAO - cancelartarjetaEnlace() dbName= "+ Global.DATASOURCE_ORACLE2 + "Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (Exception e1) {
				EIGlobal.mensajePorTrace("AdmonRemesasDAO - cancelartarjetaEnlace() - "+ "Error al cerrar conexion: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace("AdmonRemesasDAO - cancelartarjetaEnlace() - update", EIGlobal.NivelLog.INFO);
		}

		return resultado;
	}

	private String armarquerydetalle(AsignacionBean consulta) {
		String query = "";
		query = "update eweb_nomn3_detalle"
				+ " set estatus='D'"
				+ " where num_cuenta2='"+consulta.getContrato().trim()+"'" +
				  " and no_tarjeta='"+consulta.getTarjeta().trim()+"'";

		return query;
	}
}