package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import mx.altec.enlace.beans.AsignacionBean;
import mx.altec.enlace.beans.ConsultaAsignacionTEResBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class ConsultaAsignacionDAO extends BaseServlet{

	private PreparedStatement statement = null;
	private Connection conn = null;
	String query = "";

	private ResultSet Consulta() throws Exception {
		ResultSet rs = null;
		rs = statement.executeQuery();
		return rs;
	}

	private int Inserta() throws Exception {
		int resultado = 0;
		resultado = statement.executeUpdate();
		return resultado;
	}

	public ArrayList<ConsultaAsignacionTEResBean> consultaAsignacionTE(
			AsignacionBean consulta) throws Exception {
		ArrayList<ConsultaAsignacionTEResBean> respuesta = new ArrayList<ConsultaAsignacionTEResBean>();
		EIGlobal.mensajePorTrace(
				"ConsultaAsignacionDAO - consultaAsignacionTE()",
				EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		boolean res = false;
		String fecha = "";
		try {
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			query = armarquery(consulta);
			statement = conn.prepareStatement(query.toString());
			EIGlobal.mensajePorTrace("Query armado" + query,
					EIGlobal.NivelLog.INFO);
			rs = Consulta();
			while (rs.next()) {
				// fecha=rs.getString("SYSDATE");
				ConsultaAsignacionTEResBean r = new ConsultaAsignacionTEResBean();
				// r.setFolio(rs.getString("CUENTA_ABONO"));

				r.setFolio(rs.getString("FOLIO"));
				r.setCuenta(rs.getString("NUMCUENTA") != null ? rs.getString("NUMCUENTA") : "");
				r.setNombre(rs.getString("NOMBREEMPLEADO"));
				r.setNumEmpleado(rs.getString("NUMEMPL"));
				r.setRemesa(rs.getString("REMESA"));

				String estatus = rs.getString("ESTATUS");
				String reactivacion = rs.getString("REACTIVACION");
				String estat = "";
				if (estatus != null ) {
					if(reactivacion!=null){
						if (estatus.equals("R") && reactivacion.equals("V"))
							estat = "PREVALIDADA";
						if (estatus.equals("R") && reactivacion.equals("X"))
							estat = "RECHAZADA";
						if (estatus.equals("R") && reactivacion.equals("R"))
							estat = "REACTIVADA";
						if (estatus.equals("R") && reactivacion.equals("P"))
							estat = "PARCIALMENTE PREVALIDADO";
						if (estatus.equals("D"))
							estat = "CANCELADA";
						if (estatus.equals("E"))
							estat = "ENVIADA a 390";
						if (estatus.equals("P"))
							estat = "ASIGNADA";
						if (estatus.equals("X"))
							estat = "RECHAZADA por 390";
					}else{
						if (estatus.equals("R"))
							estat = "RECIBIDA";
						if (estatus.equals("D"))
							estat = "CANCELADA";
					}
//					if (estatus.equals("R"))
//						estat = "Recibida";
//					else if (estatus.equals("X"))
//						estat = "Rechazada";
//					else if (estatus.equals("E"))
//						estat = "Enviada";
//					else if (estatus.equals("P"))
//						estat = "Asignada";
//					else if (estatus.equals("D"))
//						estat = "Cancelada";
//					else if (estatus.equals("L"))
//						estat = "Recibida 390";
//					else if (estatus.equals("V"))
//						estat = "Prevalidada";
//					else
//						estat = estatus;
				}
				r.setEstatus(estat);
				//r.setUsuario(rs.getString("USUARIO"));
				/************ Se encripta valor usuario *****************/
				String usuario = rs.getString("USUARIO");
				if(usuario.trim().length()==7)
					usuario=" "+usuario;
				if ( estatus.equals("P") ){
					r.setUsuarioAsterisc("*****" + usuario.substring(6));
					r.setUsuario(usuario);
				}else{
					r.setUsuarioAsterisc("");
					r.setUsuario("");
				}
				/********************************************************/
				if (r.getUsuario() == null) {
					r.setUsuario("");
				}

				if (r.getNumEmpleado() == null) {
					r.setNumEmpleado("");
				}
				if (r.getRemesa() == null) {
					r.setRemesa("");
				}
				r.setTarjeta(rs.getString("NUMTARJETA"));
				if (rs.getString("BUC") != null) {
					String buc = rs.getString("BUC");
					if (buc.length() > 5) {
						r.setBucAsterisc("*****" + buc.substring(5));
					}
					r.setBuc(buc);
				} else {
					r.setBuc("");
				}

				try {
					Date fechab = rs.getDate("FECHA");
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					r.setFecha(df.format(fechab));
				} catch (Exception e) {
					r.setFecha("");
				}

				respuesta.add(r);
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(
					"ConsultaAsignacionDAO - consultaAsignacionTE() dbName= "
							+ Global.DATASOURCE_ORACLE2 + "Error: [" + e + "]",
					EIGlobal.NivelLog.ERROR);

		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (Exception e1) {
				EIGlobal.mensajePorTrace(
						"ConsultaAsignacionDAO - consultaAsignacionTE() - "
								+ "Error al cerrar conexion: [" + e1 + "]",
						EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace(
					"ConsultaAsignacionDAO - consultaAsignacionTE() - fin fecha:"
							+ fecha, EIGlobal.NivelLog.INFO);
		}
		return respuesta;
	}

	private String armarquery(AsignacionBean consulta) {
		String query = "";
		query = "select nuc.num_persona as buc,det.secuencia as folio, det.no_remesa as remesa, det.num_empl as numEmpl,"
				+ " nombre||' '||apell_paterno||' '||apell_materno as nombreEmpleado,det.no_tarjeta as numTarjeta,det.no_cuenta"
				+ " as numcuenta,det.estatus, det.reactivacion ,con.fch_recepcion as fecha,det.cve_usuario as usuario from nucl_cuentas nuc inner join eweb_nomn3_detalle det"
				+ " on DET.Num_cuenta2=nuc.num_cuenta  inner join EWEB_NOMN3_control con on con.num_cuenta2 =DET.Num_cuenta2 and "
				+ "con.secuencia=det.secuencia " +
				  "where det.num_cuenta2='"+consulta.getContrato()+"'";

		if (consulta.getCancelada() || consulta.getEnProceso()
				|| consulta.getProcesada() || consulta.getRecibida()) {
			boolean coma = false;

			query += " and det.estatus in (";
			if (consulta.getCancelada()) {
				query += "'R'";
				coma = true;
			}
			if (consulta.getEnProceso()) {
				if (coma) {
					query += ",'X'";
				} else {
					query += "'X'";
					coma = true;

				}
			}
			if (consulta.getProcesada()) {
				if (coma) {
					query += ",'P'";
				} else {
					query += "'P'";
					coma = true;

				}
			}
			if (consulta.getRecibida()) {
				if (coma) {
					query += ",'E'";
				} else {
					query += "'E'";
					coma = true;

				}
			}
			query += ")";
		}
		if (!consulta.getFolio().equals("")) {
			query += " and det.secuencia ='" + consulta.getFolio() + "'";
		}
		if (!consulta.getNumEmpleado().equals("")) {
			query += " and det.num_empl ='" + consulta.getNumEmpleado()+ "'";
		}
		if (!consulta.getRemesa().equals("")) {
			query += " and det.no_remesa ='" + consulta.getRemesa() + "'";
		}
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
		if (consulta.getFechaIni() != null) {
			query += " and CON.FCH_RECEPCION>= to_date('"
			  	  + formatoDeFecha.format(consulta.getFechaIni())
				  + "', 'DD/MM/YYYY')";
		}
		if (consulta.getFechaFin() != null) {
			query += " and CON.FCH_RECEPCION<= to_date('"
				  + formatoDeFecha.format(consulta.getFechaFin())
				  + "', 'DD/MM/YYYY')+1";
		}
		return query;
	}

	/**
	 * Metodo encargado de regresar los dias validos a cargar.
	 *
	 * @param formato					Formato de Consulta
	 * @return diasInhabiles			Vector con dias inhabiles
	 * @throws Exception				Error de tipo SQLException
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	public Vector CargarDias(int formato)throws Exception{
		String query = null;
		ResultSet rs = null;
		Vector diasInhabiles = new Vector();
		Connection conn= null;
		PreparedStatement sDup = null;

		EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::CargarDias:: Entrando a CargaDias", EIGlobal.NivelLog.DEBUG);
		try{
			conn = createiASConn (Global.DATASOURCE_ORACLE );
			query  = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') "
				   + "From comu_dia_inhabil "
				   + "Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2) ";
			sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();
			while(rs.next()){
				String dia  = rs.getString(1);
				String mes  = rs.getString(2);
				String anio = rs.getString(3);
				if(formato == 0){
	                  dia  =  Integer.toString( Integer.parseInt(dia) );
	                  mes  =  Integer.toString( Integer.parseInt(mes) );
	                  anio =  Integer.toString( Integer.parseInt(anio) );
				}
				String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
				diasInhabiles.addElement(fecha);
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::CargarDias:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return diasInhabiles;
	}

	/**
	 * Metodo para consultar el estado del archivo a cancelar.
	 *
	 * @param numFolio
	 * @param numContrato
	 * @return
	 */
	public String validaFolio(String numFolio, String numContrato) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String estatus = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE2);
			sDup = conn.createStatement();
			query = "Select a.estatus as estatus "
				  + "From eweb_nomn3_control a "
				  + "Where a.num_cuenta2 = '" + numContrato + "' "
				  + "And a.secuencia = " + numFolio;

			EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::validaFolio:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				estatus = rs.getString("estatus")!=null?rs.getString("estatus"):"";
			}
			estatus = estatus!=null?estatus:"";

			EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::validaFolio:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::validaFolio:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
			estatus = "";
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar el archivo", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("ConsultaAsignacionDAO::validaFolio:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return estatus;
	}

	/**
	 * Metodo para cancelar un folio.
	 *
	 * @param numFolio
	 * @param numContrato
	 * @return
	 */
	public boolean cancelaFolio(String numFolio, String numContrato) {
		Connection conn = null;
		Statement sDup = null;
		int i = 0;
		String query = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE2);
			sDup = conn.createStatement();

			query = "update eweb_nomn3_detalle set "
				  + "estatus = 'D' "
				  + "Where num_cuenta2 = '" + numContrato + "' "
				  + "And secuencia = " + numFolio;

			EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::cancelaFolio:: -> Query Detalle:" + query, EIGlobal.NivelLog.INFO);
			i = sDup.executeUpdate(query);

			EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::cancelaFolio:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
			if(i>0){
				i = 0;
				query = "";
				query = "update eweb_nomn3_control set "
					  + "estatus = 'D' "
					  + "Where num_cuenta2 = '" + numContrato + "' "
					  + "And secuencia = " + numFolio;

				EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::cancelaFolio:: -> Query Control:" + query, EIGlobal.NivelLog.INFO);
				i = sDup.executeUpdate(query);

				EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::cancelaFolio:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
				if(i>0)
					return true;
				else
					return false;
			}else
				return false;

		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("ConsultaAsignacionDAO::cancelaFolio:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			return false;
		}finally{
			try{
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al actualizar el archivo", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("ConsultaAsignacionDAO::cancelaFolio:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
	}
}