/**
 * ISBAN M�xico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * mx.altec.enlace.dao.MMC_SpidDAO.java
 *
 * Control de versiones:
 *
 * Version 	Date	 	By 		        Company 	Description
 * ------- 	------  	-------------   -----------  ------------------------------
 * 1.0		12-16		M Fuentes	    IDS			   Creaci�n
 */

package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import mx.altec.enlace.beans.MMC_SpidBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * Clase para consultar catalogos SPID.
 * 
 * @author FSW IDS :::KS-JMFR:::
 */
public class MMC_SpidDAO {
	
	/**
	 * Variable para generar los querys a las tablas de spid
	 */
	private transient String query = null;

	/**
	 * Objeto que realiza la conexion a la BD.
	 */
	private transient Connection conexion;
	/**
	 * Objeto ResultSet que recuperara la informacion de la tabla
	 */
	private transient ResultSet resultSet;

	/**
	 * Objeto statement el cual recibe parametros para la consulta.
	 */
	private transient PreparedStatement preparedStatement;

	
	/**
	 * Metodo para generar el listado de bancos USD SPID.
	 * 
	 * @author FSW IDS :::KS-JMFR:::
	 * @param spidList de tipo List<MMC_SpidBean>.
	 * @return true / false
	 * 			  de tipo boolean
	 * @throws SQLException
	 * 			  Excepcion lanzada en caso de errores SQL.
	 */
	public boolean getBancosUSD(List<MMC_SpidBean> spidList) throws SQLException {
		EIGlobal.mensajePorTrace("MMC_SpidDAO - getBancosUSD() - INI", EIGlobal.NivelLog.INFO);
		boolean resp = false;
		if(isConexionOK()){
			try {
				query = "SELECT CIF.CVE_INTERME, CIF.NOMBRE_CORTO, CIF.NUM_CECOBAN "//campos
						+"FROM TRAN_SPID_INTERME TSI, COMU_INTERME_FIN CIF "//tablas
						+"WHERE TSI.CVE_INTERME = CIF.CVE_INTERME AND NUM_CECOBAN<>0 AND FCH_BAJA IS NULL "//filtros
						//+"WHERE TSI.CVE_INTERME = CIF.CVE_INTERME "//filtros
						//+"WHERE TSI.CVE_INTERME = CIF.CVE_INTERME AND NUM_CECOBAN<>0 "//filtros
						+"ORDER BY CIF.NOMBRE_CORTO";//orden
				preparedStatement = conexion.prepareStatement(query);
				EIGlobal.mensajePorTrace("getBancosUSD query: "+ query, EIGlobal.NivelLog.DEBUG);
				resultSet = preparedStatement.executeQuery();
				EIGlobal.mensajePorTrace("getBancosUSD: Ejecucion _Exitosa", EIGlobal.NivelLog.DEBUG);
				MMC_SpidBean spidBean;
				while(resultSet.next()) {
					spidBean = new MMC_SpidBean();
					spidBean.setCveInter(resultSet.getString("CVE_INTERME"));
					spidBean.setNombre(resultSet.getString("NOMBRE_CORTO"));
					spidBean.setCecoban(resultSet.getString("NUM_CECOBAN"));
					spidList.add(spidBean);
				}
				if(!spidList.isEmpty()){
					resp = true;
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("getBancosUSD: Error al ejecutar: "
								+ query + " : "
								+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			}finally {
				if(!cerrarConexionBD()){
					EIGlobal.mensajePorTrace("getBancosUSD: SQL_ERROR Error al cerrar conexion", EIGlobal.NivelLog.ERROR);
				}else{
					EIGlobal.mensajePorTrace("getBancosUSD: ��Conexion cerrada!!", EIGlobal.NivelLog.INFO);
				}
			}
		}
		EIGlobal.mensajePorTrace("MMC_SpidDAO - getBancosUSD() - END", EIGlobal.NivelLog.INFO);
		return resp;
	}
	
	/**
	 * Metodo para generar el listado de bancos MXM con filtro de FCH_BAJA.
	 * 
	 * @author FSW IDS :::KS-JMFR:::
	 * @param spidList de tipo List<MMC_SpidBean>.
	 * @return true / false
	 * 			  de tipo boolean
	 * @throws SQLException
	 * 			  Excepcion lanzada en caso de errores SQL.
	 */
	public boolean getBancosMxm1(List<MMC_SpidBean> spidList) throws SQLException {
		EIGlobal.mensajePorTrace("MMC_SpidDAO - getBancosMxm1() - INI", EIGlobal.NivelLog.INFO);
		boolean resp = false;
		if(isConexionOK()){
			try {
				query = "SELECT CVE_INTERME, NOMBRE_CORTO, NUM_CECOBAN "//campos
						+"FROM COMU_INTERME_FIN "//tabla
						+"WHERE NUM_CECOBAN<>0 AND FCH_BAJA IS NULL "//filtros
						+"ORDER BY NOMBRE_CORTO";//orden
				preparedStatement = conexion.prepareStatement(query);
				EIGlobal.mensajePorTrace("getBancosMxm1 Query: "+ query, EIGlobal.NivelLog.DEBUG);
				resultSet = preparedStatement.executeQuery();
				EIGlobal.mensajePorTrace("getBancosMxm1: Ejecucion _Exitosa", EIGlobal.NivelLog.DEBUG);
				MMC_SpidBean spidBean;
				while(resultSet.next()) {
					spidBean = new MMC_SpidBean();
					spidBean.setCveInter(resultSet.getString("CVE_INTERME"));
					spidBean.setNombre(resultSet.getString("NOMBRE_CORTO"));
					spidBean.setCecoban(resultSet.getString("NUM_CECOBAN"));
					spidList.add(spidBean);
				}
				if(!spidList.isEmpty()){
					resp = true;
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("getBancosMxm1: Error al ejecutar: "
								+ query + " : "
								+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			}finally {
				if(!cerrarConexionBD()){
					EIGlobal.mensajePorTrace("getBancosMxm1: SQL_ERROR Error al cerrar conexion", EIGlobal.NivelLog.ERROR);
				}else{
					EIGlobal.mensajePorTrace("getBancosMxm1: ��Conexion cerrada!!", EIGlobal.NivelLog.INFO);
				}
			}
		}
		EIGlobal.mensajePorTrace("MMC_SpidDAO - getBancosMxm1() - END", EIGlobal.NivelLog.INFO);
		return resp;
	}
	
	/**
	 * Metodo para generar el listado de bancos MXM sin filtro de FCH_BAJA.
	 * 
	 * @author FSW IDS :::KS-JMFR:::
	 * @param spidList de tipo List<MMC_SpidBean>.
	 * @return true / false
	 * 			  de tipo boolean
	 * @throws SQLException
	 * 			  Excepcion lanzada en caso de errores SQL.
	 */
	public boolean getBancosMxm2(List<MMC_SpidBean> spidList) throws SQLException {
		EIGlobal.mensajePorTrace("MMC_SpidDAO - getBancosMxm2() - INI", EIGlobal.NivelLog.INFO);
		boolean resp = false;
		if(isConexionOK()){
			try {
				query = "SELECT CVE_INTERME, NOMBRE_CORTO, NUM_CECOBAN "//campos
						+"FROM COMU_INTERME_FIN "//tabla
						+"WHERE NUM_CECOBAN<>0 "//filtro
						+"ORDER BY NOMBRE_CORTO";//orden
				preparedStatement = conexion.prepareStatement(query);
				EIGlobal.mensajePorTrace("getBancosMxm2 Query: "+ query, EIGlobal.NivelLog.DEBUG);
				resultSet = preparedStatement.executeQuery();
				EIGlobal.mensajePorTrace("getBancosMxm2: Ejecucion _Exitosa", EIGlobal.NivelLog.DEBUG);
				MMC_SpidBean spidBean;
				while(resultSet.next()) {
					spidBean = new MMC_SpidBean();
					spidBean.setCveInter(resultSet.getString("CVE_INTERME"));
					spidBean.setNombre(resultSet.getString("NOMBRE_CORTO"));
					spidBean.setCecoban(resultSet.getString("NUM_CECOBAN"));
					spidList.add(spidBean);
				}
				if(!spidList.isEmpty()){
					resp = true;
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("getBancosMxm2: Error al ejecutar: "
								+ query + " : "
								+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			}finally {
				if(!cerrarConexionBD()){
					EIGlobal.mensajePorTrace("getBancosMxm2: SQL_ERROR Error al cerrar conexion", EIGlobal.NivelLog.ERROR);
				}else{
					EIGlobal.mensajePorTrace("getBancosMxm2: ��Conexion cerrada!!", EIGlobal.NivelLog.INFO);
				}
			}
		}
		EIGlobal.mensajePorTrace("MMC_SpidDAO - getBancosMxm2() - END", EIGlobal.NivelLog.INFO);
		return resp;
	}
	
	/**
	 * Se encarga de crear la conexion a la Base de datos.
	 * 
	 * @author FSW IDS :::KS-JMFR:::
	 * @return Resultado de la conexion.
	 */
	private boolean isConexionOK() {
		boolean inicializado = false;
		try {
			conexion = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE3);
			// conexion.setAutoCommit(false);
			inicializado = true;
			EIGlobal.mensajePorTrace(
					"MMC_SpidDAO - inicializarConexionBD: conexion exitosa con DS: "
							+ Global.DATASOURCE_ORACLE3, EIGlobal.NivelLog.INFO);
		} catch (SQLException e) {
			EIGlobal
					.mensajePorTrace(
							"MMC_SpidDAO - inicializarConexionBD: Error al inicializar la conexion utilizando DS: "
									+ Global.DATASOURCE_ORACLE + e.getMessage(),
							EIGlobal.NivelLog.ERROR);
		}
		return inicializado;
	}
	

	/**
	 * Cierra las conexiones a la BD
	 * 
	 * @author FSW IDS :::KS-JMFR:::
	 * @return resultado de la operacion.
	 */
	private boolean cerrarConexionBD() {
		boolean cerrado = false;
		EIGlobal.mensajePorTrace(
				"MMC_SpidDAO - cerrarConexionBD: Cerrando la conexion",
				EIGlobal.NivelLog.INFO);
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (resultSet != null) {
				resultSet.close();
			}
			if (conexion != null) {
				conexion.close();
			}
			cerrado = true;
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"MMC_SpidDAO - cerrarConexionBD: Error al cerrar la conexion: "
							+ Global.DATASOURCE_ORACLE + e.getMessage(),
					EIGlobal.NivelLog.ERROR);
		}
		return cerrado;
	}


}
