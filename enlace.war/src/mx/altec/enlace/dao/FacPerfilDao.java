package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.ibm.websphere.rsadapter.WSCallHelper;


import mx.altec.enlace.gwt.adminusr.shared.Facultad;
import mx.altec.enlace.gwt.adminusr.shared.PerfilPrototipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import oracle.jdbc.OracleConnectionWrapper;
import oracle.jdbc.driver.*;





public class FacPerfilDao {

	Connection interna = null;
	OracleConnection conn = null;
	WSCallHelper helper = null;


	public FacPerfilDao() {
		try {
			interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);
	
			interna.setAutoCommit(false);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao - Problema al abrir conexion DB-> "+e,EIGlobal.NivelLog.ERROR);
		}

	}

	public void cierraConexion() {
		commit();
		try {
			interna.close();
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao - Problema al cerrar conexion DB-> "+e,EIGlobal.NivelLog.ERROR);
		}
	}

	public boolean commit() {
		try {
			interna.commit();
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao - Problema al hacer commit-> "+e,EIGlobal.NivelLog.ERROR);
			return false;
		}
		return true;
	}

	public boolean rollback() {
		try {
			interna.rollback();
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao - Problema al hacer rollback-> "+e,EIGlobal.NivelLog.ERROR);
			return false;
		}
		return true;
	}



	public void cargaFacultadesPerfil(PerfilPrototipo perfil) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		ResultSet rs = null;

		//Objetos para almacenar query y sus resultados
		Facultad bean = null;

		try {

			//Obtener conexion
			//String query = "select a.cve_facultad, b.descripcion from segu_facperfprot a, segu_facultades b where a.cve_perfil_prot = '" + perfil.getCvePerfil() + "' and a.cve_facultad = b.cve_facultad";
			String query = "select a.cve_facultad, b.descripcion from segu_facperfprot a, segu_facultades b " +
			               "where a.cve_perfil_prot = '" + perfil.getCvePerfil() + "' and a.cve_facultad = b.cve_facultad";
			EIGlobal.mensajePorTrace ("=============================",EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace ("-FacPerfilDao- perfil.getCvePerfil()-> [" + perfil.getCvePerfil() + "]",EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace ("-FacPerfilDao- query-> [" + query + "]",EIGlobal.NivelLog.DEBUG);
			
  		    ps = interna.prepareStatement(query);
			
  		   // OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
			
			
			//ps2.setFixedCHAR(1, perfil.getCvePerfil());
			//Ejecutar query
			rs = ps.executeQuery();
			EIGlobal.mensajePorTrace ("Despues de ejecutar query...", EIGlobal.NivelLog.DEBUG);
			//Leer resultados
			while (rs.next()) {
				EIGlobal.mensajePorTrace ("Entro al while..", EIGlobal.NivelLog.DEBUG);
				bean = new Facultad();
				bean.setCveFacultad(rs.getString("cve_facultad").trim());
				EIGlobal.mensajePorTrace ("-FacPerfilDao- Facultad-> [" + bean.getCveFacultad() + "]",EIGlobal.NivelLog.DEBUG);
				bean.setDescripcion(rs.getString("descripcion").trim());
				EIGlobal.mensajePorTrace ("-FacPerfilDao- Descripcion-> [" + bean.getDescripcion() + "]",EIGlobal.NivelLog.DEBUG);
				perfil.agregaFacultad(bean);
				EIGlobal.mensajePorTrace ("Facultad leida [" + bean.getCveFacultad() + "]",EIGlobal.NivelLog.DEBUG);
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.getFacultadesPerfil - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
			}
	}

	/**
	 * Consulta de facultades por perfil v�a Tuxedo
	 * @param perfil		Perfil Prototipo
	 */

	public void _cargaFacultadesPerfil(PerfilPrototipo perfil) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - cargaFacultadesPerfil (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		Facultad bean = null;

		try{
			tramaEntrada = perfil.getCvePerfil();

			hs =  tuxGlobal.cargaFacPerfil(tramaEntrada, "CARGA_FAC_PERF");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - cargaFacultadesPerfil "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				String facultades[] = tramaSalida.split("\\|");

				for (int idx = 0; idx < facultades.length; idx ++) {
					String facDsc[] = facultades[idx].split("@");
					bean = new Facultad();
					bean.setCveFacultad(facDsc[0].trim());
					EIGlobal.mensajePorTrace ("FacPerfilDao - Facultad-> [" +
							bean.getCveFacultad() + "]",EIGlobal.NivelLog.DEBUG);
					bean.setDescripcion(facDsc[1].trim());
					EIGlobal.mensajePorTrace ("FacPerfilDao - Descripcion-> [" +
							bean.getDescripcion() + "]",EIGlobal.NivelLog.DEBUG);
					perfil.agregaFacultad(bean);
				}
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - cargaFacultadesPerfil - Error al obtener Facultades ["
									+ e.getMessage() + "] "
		               				+ "Perfil: [" + perfil.getCvePerfil() + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
	}

	public boolean tienePerfilAsignado(String perfil, String cvePerfilProt) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean respuesta = false;

		try {

			//Obtener conexion

			//String query = "select cve_perfil_prot from segu_asgperfprot where cve_perfil_prot = '" + cvePerfilProt + "' and cve_perfil = '" + perfil +"'";
			String query = "select cve_perfil_prot from segu_asgperfprot " +
			               "where cve_perfil_prot = '" + cvePerfilProt + "' and cve_perfil = '" + perfil + "'";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
		
			//Pasar parametros al query
			//ps2.setFixedCHAR(1, cvePerfilProt);
			//ps2.setFixedCHAR(2, perfil);

			//Ejecutar query
			rs = ps.executeQuery();

			//Leer resultados
			if (rs.next()) {
				respuesta = true;
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.tienePerfilAsignado - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/**
	 * tienePerfilAsignado v�a Tuxedo
	 * @param perfil,
	 * @param PerfilPrototipo
	 */

	public boolean _tienePerfilAsignado(String perfil, String cvePerfilProt) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - tienePerfilAsignado (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		boolean result = false;
		if (perfil == null || perfil.trim().equals("")) {
			return false;
		}
		try{
			tramaEntrada = perfil + "|" + cvePerfilProt;

			hs =  tuxGlobal.tienePerfilAsignado(tramaEntrada, "PERF_ASIG");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - tienePerfilAsignado "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = true;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - tienePerfilAsignado - Error ["
									+ e.getMessage() + "] "
		               				+ "Perfil: [" + perfil + "]"
		               				+ "cvePerfilProt: [" + cvePerfilProt + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public boolean tieneFacultadEx(String cvePerfil, String cveFacultad, String signo) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean respuesta = false;

		try {

			//Obtener conexion

			//String query = "select count(1) as cuenta from segu_facextraord where cve_perfil = '" + cvePerfil + "' and cve_facultad = '" + cveFacultad + "' and signo = '" + signo + "'";
			String query = "select count(1) as cuenta from segu_facextraord " +
			               "where cve_perfil = '" + cvePerfil + "' and cve_facultad = '" + cveFacultad + "' and signo = '" + signo + "'";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//Pasar parametros al query
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;

			//ps2.setFixedCHAR(1, cvePerfil);
			//ps2.setFixedCHAR(2, cveFacultad);
			//ps2.setFixedCHAR(3, signo);

			//Ejecutar query
			rs = ps.executeQuery();

			//Leer resultados
			if (rs.next()) {
				if(rs.getInt("cuenta") > 0) {
					respuesta = true;
				}
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);

		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * tieneFacultadEx v�a Tuxedo
	 * @param cvePerfil,
	 * @param cveFacultad
	 * @param signo
	 */

	public boolean _tieneFacultadEx(String cvePerfil, String cveFacultad, String signo) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - tieneFacultadEx (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		boolean result = false;
		try{
			tramaEntrada = cvePerfil + "|" + cveFacultad + "|" + signo;
			hs =  tuxGlobal.tieneFacultadEx(tramaEntrada, "FAC_EXTRA");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - tieneFacultadEx "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = true;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - tieneFacultadEx - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "cveFacultad: [" + cveFacultad + "]"
		               				+ "signo: [" + signo + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}


	public String getPerfilUsuario(String usuario, String contrato) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		ResultSet rs = null;
		String respuesta = "";

		try {

			//Obtener conexion

			//String query = "select cve_perfil from segu_usrperftele where cve_usuario = '" + BaseServlet.convierteUsr8a7(usuario) + "' and num_cuenta = '" + contrato + "'";
			String query = "select cve_perfil from segu_usrperftele " +
			               "where cve_usuario = '" + BaseServlet.convierteUsr8a7(usuario) + "' and num_cuenta = '" + contrato + "'";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
			
			
			//ps2.setFixedCHAR(1, BaseServlet.convierteUsr8a7(usuario));
			//ps2.setFixedCHAR(2, contrato);

			//Ejecutar query
			rs = ps.executeQuery();

			//Leer resultados
			if (rs.next()) {
				respuesta = rs.getString("cve_perfil").trim();
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.getPerfilUsuario - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * getPerfilUsuario v�a Tuxedo
	 * @param usuario,
	 * @param contrato
	 */

	public String _getPerfilUsuario(String usuario, String contrato) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - getPerfilUsuario (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		String result = "";
		try{
			tramaEntrada = BaseServlet.convierteUsr8a7(usuario) + "|" + contrato ;
			hs =  tuxGlobal.getPerfilUsuario(tramaEntrada, "OBTEN_PERF_USR");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - getPerfilUsuario "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = tramaSalida.trim();
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - getPerfilUsuario - Error ["
									+ e.getMessage() + "] "
		               				+ "usuario: [" + usuario + "]"
		               				+ "contrato: [" + contrato + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public int bajaFacultad(String cvePerfil, String cveFacultad) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {

			//String query = "delete from segu_facextraord where cve_perfil = '" + cvePerfil + "' and cve_facultad = '" + cveFacultad + "'";
			String query = "delete from segu_facextraord " +
			               "where cve_perfil = '" + cvePerfil + "' and cve_facultad = '" + cveFacultad + "'";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;

			//ps2.setFixedCHAR(1, cvePerfil);
			//ps2.setFixedCHAR(2, cveFacultad);

			
			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.bajaFacultad - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
			respuesta = -1;
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * bajaFacultad v�a Tuxedo
	 * @param cvePerfil,
	 * @param cveFacultad
	 */

	public int _bajaFacultad(String cvePerfil, String cveFacultad) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - bajaFacultad (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil + "|" + cveFacultad;
			hs =  tuxGlobal.bajaFacultad(tramaEntrada, "BAJA_FAC_USR");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - bajaFacultad "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - bajaFacultad - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "cveFacultad: [" + cveFacultad + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public int bajaPerfProt(String cvePerfil, String cvePerfProt) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {


			//String query = "delete from segu_asgperfprot where cve_perfil = '" + cvePerfil + "' and cve_perfil_prot = '" + cvePerfProt + "'";
			String query = "delete from segu_asgperfprot " +
			               "where cve_perfil = '" + cvePerfil + "' and cve_perfil_prot = '" + cvePerfProt + "'";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
			
			//ps2.setFixedCHAR(1, cvePerfil);
			//ps2.setFixedCHAR(2, cvePerfProt);

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.bajaFacultad - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
			respuesta = -1;
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * bajaPerfProt v�a Tuxedo
	 * @param cvePerfil,
	 * @param cvePerfProt
	 */

	public int _bajaPerfProt(String cvePerfil, String cvePerfProt) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - bajaPerfProt (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil + "|" + cvePerfProt;
			hs =  tuxGlobal.bajaPerfProt(tramaEntrada, "BAJA_PERFPROT");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - bajaPerfProt "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - bajaPerfProt - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "cvePerfProt: [" + cvePerfProt + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public int bajaPerfil(String cvePerfil) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {


			//String query = "delete from segu_perfiles where cve_perfil = '" + cvePerfil + "'";
			String query = "delete from segu_perfiles where cve_perfil = '" + cvePerfil + "'";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
			
			//ps2.setFixedCHAR(1, cvePerfil);

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.bajaFacultad - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * bajaPerfil v�a Tuxedo
	 * @param cvePerfil,
	 */

	public int _bajaPerfil(String cvePerfil) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - bajaPerfil (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil;
			hs =  tuxGlobal.bajaPerfil(tramaEntrada, "BAJA_PERFIL");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - bajaPerfil "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - bajaPerfil - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public int bajaUsuario(String cvePerfil, String usuario, String contrato) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {

			//String query = "delete from segu_usrperftele where cve_perfil = '" + cvePerfil + "' and cve_usuario = '" + usuario + "' and num_cuenta = '" + contrato + "'";
			String query = "delete from segu_usrperftele " +
			               "where cve_perfil = '" + cvePerfil + "' and cve_usuario = '" + usuario + "' and num_cuenta = '" + contrato + "'";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
	
			//ps2.setFixedCHAR(1, cvePerfil);
			//ps2.setFixedCHAR(2, usuario);
			//ps2.setFixedCHAR(3, contrato);

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.bajaFacultad - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * bajaUsuario v�a Tuxedo
	 * @param cvePerfil,
	 * @param usuario,
	 * @param contrato
	 */

	public int _bajaUsuario(String cvePerfil, String usuario, String contrato) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - bajaUsuario (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil + "|" + BaseServlet.convierteUsr8a7(usuario) + "|" + contrato;
			hs =  tuxGlobal.bajaUsuario(tramaEntrada, "BAJA_USUARIO");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - bajaUsuario "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - bajaUsuario - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "usuario: [" + usuario + "]"
		               				+ "contrato: [" + contrato + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}


	public int altaPerfil(String cvePerfil, String cveUsuario, String contrato) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {

			//REVISAR QUERY CON DATOS FIJOS DE TABLA
			//String query = "insert into segu_usrperftele (cve_usuario, num_cuenta, u_version, cve_perfil, usr_mig) values('" + cveUsuario + "', '" + contrato + "', '!', '" + cvePerfil + "', null)";
			String query = "insert into segu_usrperftele (cve_usuario, num_cuenta, u_version, cve_perfil, usr_mig) " +
			               "values('" + cveUsuario + "', '" + contrato + "', '!', '" + cvePerfil + "', null)";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
		
			//ps2.setFixedCHAR(1, cveUsuario);
			//ps2.setFixedCHAR(2, contrato);
			//ps2.setFixedCHAR(3, cvePerfil);

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.altaPerfil - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * altaPerfil v�a Tuxedo
	 * @param cvePerfil,
	 * @param cveUsuario,
	 * @param contrato
	 */

	public int _altaPerfil(String cvePerfil, String cveUsuario, String contrato) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - altaPerfil (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil + "|" + BaseServlet.convierteUsr8a7(cveUsuario) + "|" + contrato;
			hs =  tuxGlobal.altaPerfilUsr(tramaEntrada, "ALTA_PERFUSR");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - altaPerfil "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - altaPerfil - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "cveUsuario: [" + cveUsuario + "]"
		               				+ "contrato: [" + contrato + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public int altaPerfProt(String cvePerfil, String cvePerfProt) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {

			//REVISAR QUERY CON DATOS FIJOS DE TABLA
			//String query = "insert into segu_asgperfprot values('" + cvePerfil + "', '" + cvePerfProt + "', '!', 'S')";
			String query = "insert into segu_asgperfprot values('" + cvePerfil + "', '" + cvePerfProt + "', '!', 'S')";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
	
			//ps2.setFixedCHAR(1, cvePerfil);
			//ps2.setFixedCHAR(2, cvePerfProt);

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.bajaFacultad - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * altaPerfProt v�a Tuxedo
	 * @param cvePerfil,
	 * @param cvePerfProt,
	 */

	public int _altaPerfProt(String cvePerfil, String cvePerfProt) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - altaPerfProt (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil + "|" + cvePerfProt;
			hs =  tuxGlobal.altaPerfProt(tramaEntrada, "ALTA_PERFPROT");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - altaPerfProt "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - altaPerfProt - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "cvePerfProt: [" + cvePerfProt + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}


	public int altaFacExtraord(String cvePerfil, String cveFacultad, String signo) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {

			//REVISAR QUERY CON DATOS FIJOS DE TABLA
			//String query = "insert into segu_facextraord values('" + cvePerfil + "', '" + cveFacultad + "', '!', '" + signo + "', 0, null, null, null)";
			String query = "insert into segu_facextraord " +
			               "values('" + cvePerfil + "', '" + cveFacultad + "', '!', '" + signo + "', 0, null, null, null)";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
	
			//ps2.setFixedCHAR(1, cvePerfil);
			//ps2.setFixedCHAR(2, cveFacultad);
			//ps2.setFixedCHAR(3, signo);

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.bajaFacultad - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * altaFacExtraord v�a Tuxedo
	 * @param cvePerfil,
	 * @param cveFacultad,
	 * @param signo
	 */

	public int _altaFacExtraord(String cvePerfil, String cveFacultad, String signo) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - altaFacExtraord (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil + "|" + cveFacultad + "|" + signo ;
			hs =  tuxGlobal.altaFacExtraord(tramaEntrada, "ALTA_FAC_EXTRA");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - altaFacExtraord "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - altaFacExtraord - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "cveFacultad: [" + cveFacultad + "]"
		               				+ "signo: [" + signo + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public ArrayList<String> getPerfilesSimilaresExistentes(String cveUsuario) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		ResultSet res = null;
		ArrayList<String> respuesta = new ArrayList<String>();
		try {

			//REVISAR QUERY CON DATOS FIJOS DE TABLA
			String query = "select cve_perfil from segu_perfiles where cve_perfil like '" + cveUsuario + "%' order by cve_perfil";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
	
			//ps2.setFixedCHAR(1, cveUsuario + "%");

			//Ejecutar query
			res = ps.executeQuery();
			while (res.next()) {
				respuesta.add(res.getString("cve_perfil"));
			}
			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.altaPerfil - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}


	public int altaPerfil(String cvePerfil, String descripcion) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {

			//REVISAR QUERY CON DATOS FIJOS DE TABLA
			//String query = "insert into segu_perfiles (cve_perfil, u_version, bloqueado, descripcion) values('" + cvePerfil + "', '!', 'N', '" + descripcion + "')";
			String query = "insert into segu_perfiles (cve_perfil, u_version, bloqueado, descripcion) " +
			               "values('" + cvePerfil + "', '!', 'N', '" + descripcion + "')";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
		
			//ps2.setFixedCHAR(1, cvePerfil);
			//ps2.setFixedCHAR(2, descripcion);

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.altaPerfil - Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
			respuesta = -1;
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * altaPerfil v�a Tuxedo
	 * @param cvePerfil,
	 * @param descripcion
	 */

	public int _altaPerfil(String cvePerfil, String descripcion) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - altaPerfil (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = cvePerfil + "|" + descripcion ;
			hs =  tuxGlobal.altaPerfil(tramaEntrada, "ALTA_PERFIL");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - altaPerfil "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - altaPerfil - Error ["
									+ e.getMessage() + "] "
		               				+ "cvePerfil: [" + cvePerfil + "]"
		               				+ "descripcion: [" + descripcion + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}


	public int altaUsuario(String usuario) {
		//Definicion de objetos para conexion
		PreparedStatement ps = null;
		int respuesta = 0;

		try {

			//REVISAR QUERY CON DATOS FIJOS DE TABLA
			//String query = "insert into segu_usuarios (cve_usuario, contrasena, cve_pto_vta, tipo_de_usuario) values('" + BaseServlet.convierteUsr8a7(usuario) + "', ' ', '0981', 'E')";
			String query = "insert into segu_usuarios (cve_usuario, contrasena, cve_pto_vta, tipo_de_usuario) " +
			               "values('" + BaseServlet.convierteUsr8a7(usuario) + "', ' ', '0981', 'E')";
			EIGlobal.mensajePorTrace ("Query <" + query + ">",EIGlobal.NivelLog.DEBUG);
			//Ejecutar query
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;

			//ps2.setFixedCHAR(1, BaseServlet.convierteUsr8a7(usuario));

			//Ejecutar query
			respuesta = ps.executeUpdate();

			//Leer resultados
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.altaPerfil - Problema al insertar usuario-> "+e,EIGlobal.NivelLog.ERROR);
			respuesta = -1;
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * altaUsuario v�a Tuxedo
	 * @param usuario
	 */

	public int _altaUsuario(String usuario) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - altaUsuario (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		int result = 0;
		try{
			tramaEntrada = BaseServlet.convierteUsr8a7(usuario);
			hs =  tuxGlobal.altaUsuario(tramaEntrada, "ALTA_USUARIO");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - altaUsuario "
									+ "Codigo de Error [" + CodError + "]"
									+ "tramaSalida [" + tramaSalida + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000") && tramaSalida!=null){
				result = 1;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - altaUsuario - Error ["
									+ e.getMessage() + "] "
		               				+ "usuario: [" + usuario + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
			result = -1;
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}


	public boolean usuarioEnOtroContrato(String usuario, String contrato) {
		// Definicion de objetos para conexion
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean respuesta = false;
		// Estructura query
		try {
			String query = "select num_cuenta from segu_usrperftele where " +
						   "cve_usuario = '" + BaseServlet.convierteUsr8a7(usuario) + "' and num_cuenta <> '" + contrato + "'";
			EIGlobal.mensajePorTrace ("FacPerfilDao.usuarioEnOtroContrato - Query: [" +
					query + "]" , EIGlobal.NivelLog.DEBUG);
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;

			//ps2.setFixedCHAR(1, BaseServlet.convierteUsr8a7(usuario));
			//ps2.setFixedCHAR(2, contrato);


			//Ejecutar query
			rs = ps.executeQuery();
			// Leer resultados
			if (rs.next()) {
				respuesta = true;
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.usuarioEnOtroContrato - " +
					"Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * usuarioEnOtroContrato v�a Tuxedo
	 * @param usuario,
	 * @param contrato
	 */

	public boolean _usuarioEnOtroContrato(String usuario, String contrato) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - usuarioEnOtroContrato (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
		String CodError = "";
		boolean result = false;
		try{
			tramaEntrada = BaseServlet.convierteUsr8a7(usuario) + "|" + contrato;
			hs =  tuxGlobal.usuarioEnOtroContrato(tramaEntrada, "USR_OTR_CONTR");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - usuarioEnOtroContrato "
									+ "Codigo de Error [" + CodError + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000")){
				result = true;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - usuarioEnOtroContrato - Error ["
									+ e.getMessage() + "] "
		               				+ "usuario: [" + usuario + "]"
		               				+ "contrato: [" + contrato + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

	public boolean perfilEnOtroUsuario(String perfil, String usuario) {
		// Definicion de objetos para conexion
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean respuesta = false;
		// Estructura query
		try {
			String query = "select count(1) as cuenta from segu_usrperftele where " +
					       "cve_perfil = '" + perfil + "'";
			EIGlobal.mensajePorTrace ("FacPerfilDao.perfilEnOtroUsuario - Query: [" +
					query + "]" , EIGlobal.NivelLog.DEBUG);
			ps = interna.prepareStatement(query);
			
			//OraclePreparedStatement ps2 = (OraclePreparedStatement) ps;
			
			//ps2.setFixedCHAR(1, perfil);

			//Ejecutar query
			rs = ps.executeQuery();
			// Leer resultados
			if (rs.next()) {
				EIGlobal.mensajePorTrace ("FacPerfilDao.perfilEnOtroUsuario - cuenta: [" +
						rs.getInt("cuenta") + "]" , EIGlobal.NivelLog.INFO);
				if (rs.getInt("cuenta") > 1)
					respuesta = true;
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace ("FacPerfilDao.perfilEnOtroUsuario - " +
					"Problema al obtener datos-> "+e,EIGlobal.NivelLog.ERROR);
			respuesta = true;
		} finally{
			try{
				//Cerrar conexiones
				if(ps!=null){
					ps.close();
					ps=null;}
				}catch(Exception e1){}
		}
		return respuesta;
	}

	/*
	 * perfilEnOtroUsuario v�a Tuxedo
	 * @param perfil,
	 * @param usuario
	 */
	public boolean _perfilEnOtroUsuario(String perfil, String usuario) {
		EIGlobal.mensajePorTrace ("*---------------------------------------------------------------------*",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("FactPerfilDao - perfilEnOtroUsuario (Tuxedo)", EIGlobal.NivelLog.INFO);
		ServicioTux tuxGlobal = new ServicioTux ();
	    Hashtable hs = null;
	    String tramaEntrada = null;
		String CodError = "";
		boolean result = false;
		try{
			tramaEntrada = perfil + "|" + BaseServlet.convierteUsr8a7(usuario);
			hs =  tuxGlobal.perfilEnOtroUsuario(tramaEntrada, "PERF_OTR_USR");
			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
			}
			EIGlobal.mensajePorTrace("FactPerfilDao - perfilEnOtroUsuario "
									+ "Codigo de Error [" + CodError + "]"
									, EIGlobal.NivelLog.DEBUG);

			if(CodError.equals("ADMU0000")){
				result = true;
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("FacPerfilDao - perfilEnOtroUsuario - Error ["
									+ e.getMessage() + "] "
		               				+ "perfil: [" + perfil + "]"
		               				+ "usuario: [" + usuario + "]"
						 			+ "Linea de error: [" + lineaError[0] + "]"
						 			, EIGlobal.NivelLog.ERROR);
		}
		if(hs != null) hs.clear();
		EIGlobal.mensajePorTrace ("**---------------------------------------------------------------------**",
				EIGlobal.NivelLog.DEBUG);
		return result;
	}

}