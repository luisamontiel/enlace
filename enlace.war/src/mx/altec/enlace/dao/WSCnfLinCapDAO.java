package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import mx.altec.enlace.beans.WSCnfLinCapBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * WebServices canales: Clase de acceso a datos, creada para las
 * operaciones relacionadas con la validaci�n de L�nea de Captura para
 * transferencias mismo banco en moneda nacional.
 * 
 * @author SGSC - Indra Diciembre 2014
 */
public class WSCnfLinCapDAO extends GenericDAO {

	/** BNDWSLC Constante para definir el nombre del par�metro para obtener la bandera de la l�nea de captura */
	private static final String BNDWSLC = "CONCTAWS";

	/** TMOWSLC Constante para definir el nombre del par�metro para obtener el Timeout del WS de validaci�n de l�nea de captura */
	private static final String TMOWSLC = "TMOUTLC";

	/** URLWSLC Constante para definir el nombre del par�metro para obtener la URL del WS de validaci�n de l�nea de captura */
	private static final String URLWSLC = "URLWSLC";

	/**
	 * Select para obtener el valor de la bandera para el empleo del Web Service
	 *                  de validaci�n de la l�nea de captuara.
	 */
	private static final String SELECT_BND_WSLC = new StringBuilder().append(
			"select VALOR_ENTERO %n").append("from STPROD.EWEB_PARAMETROS %n").append(
			"where NOMBRE_PARAM = '%s' %n").toString();

	/**
	 * Select para confirmar si la cuenta se encuentra dada de alta en la tabla EWEB_CUENTAS_WS,
	 *                  para el empleo del Web Service de validaci�n de la l�nea de captuara.
	 */
	private static final String SELECT_CTA_WSLC = new StringBuilder().append(
			"select COUNT(1) %n").append("from STPROD.EWEB_CUENTAS_WS %n").append(
			"where NUM_CUENTA = '%s' %n").toString();

	/**
	 * Select para obtener el valor del Timeout para el empleo del Web Service
	 *                  de Validaci�n de la L�nea de Captuara.
	 */
	private static final String SELECT_TMO_WSLC = new StringBuilder().append(
			"select VALOR_ENTERO %n").append("from STPROD.EWEB_PARAMETROS %n").append(
			"where NOMBRE_PARAM = '%s' %n").toString();

	/**
	 * Select para obtener la URL para la invocaci�n del Web Service
	 *                  de Validaci�n de la L�nea de Captuara.
	 */
	private static final String SELECT_URL_WSLC = new StringBuilder().append(
			"select VALOR_CADENA %n").append("from STPROD.EWEB_PARAMETROS %n").append(
			"where NOMBRE_PARAM = '%s' %n").toString();

	/**
	 * Constante para mensaje de error al cerrar conexion
	 * 
	 */
	private static final String ERROR_CONEXION = "Error al cerrar conexion: [";

	/**
	 * Se ejecutan los procesos para obtener el valor de la bandera que controla
	 *                                la validci�n de la L�nea de Captura, as� como
	 *                                si la cuenta debe validarse.
	 * 
	 * @param cnfWSLCBean Bean que contiene la configuraci�n del Web Service
	 * @param cuenta cuenta que se debe validar en el cat�logo correspondiente.
	 * @return Bean con el valor de la bandera y si la cuent esta o n� en el cat�logo.
	 */
	public WSCnfLinCapBean consCnfWSLinCap(WSCnfLinCapBean cnfWSLCBean, String cuenta) {
		cnfWSLCBean.setBndLinCap(consBndWSLinCap());
		cnfWSLCBean.setEsCtaLinCap(consCtaWSLinCap(cuenta));
		cnfWSLCBean.setTmoWSLinCap(consTmoWSLinCap());
		cnfWSLCBean.setUrlWSLinCap(consURLWSLinCap());
		return cnfWSLCBean;
	}

	/**
	 * Se ejecuta el select para obtener el valor de la bandera el empleo del
	 *                                Web Service para Validar la l�nea de Captura.
	 * 
	 * @return Boolean con el valor de la bandera de Validaci�n de L�nea de Captura
	 */
	public Boolean consBndWSLinCap() {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
    Boolean valor = false;

		String selectFull = String.format(SELECT_BND_WSLC, BNDWSLC);
		EIGlobal.mensajePorTrace("WSCnfLinCapDAO - consBndWSLinCap - query [" + selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format("WSCnfLinCapDAO consBndWSLinCap - QRY [%s]",selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("WSCnfLinCapDAO consBndWSLinCap : " + BNDWSLC + " Estado -> " + rs.getLong(1), NivelLog.DEBUG);
				if(!"0".equals(String.valueOf(rs.getLong(1)))){
					valor = true;
				}
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),NivelLog.INFO);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("WSCnfLinCapDAO - consBndWSLinCap() - " + ERROR_CONEXION + e1 + "]", NivelLog.ERROR);
			}
		}
		return valor;
	}

	/**
	 * Se ejecuta el select para saber si la cuenta se encuentra dada de alta en el cat�logo correspondiente
	 *                                de las cuentas a validar contra la l�nea de Captura.
	 * 
	 * @param cuenta String Cuenta a ser consultada en el cat�logo correspondiente
	 * @return Boolean que es verdadero en caso de existir la cuenta en el cat�logo, y flso de lo contrario.
	 */
	public Boolean consCtaWSLinCap(String cuenta) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
    Boolean valor = false;

		String selectFull = String.format(SELECT_CTA_WSLC, cuenta);
		EIGlobal.mensajePorTrace("WSCnfLinCapDAO - consCtaWSLinCap - query [" + selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format("WSCnfLinCapDAO consCtaWSLinCap - QRY [%s]",selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("WSCnfLinCapDAO consCtaWSLinCap : " + cuenta + " Existe -> " + rs.getLong(1), NivelLog.DEBUG);
				if(!"0".equals(String.valueOf(rs.getLong(1)))){
					valor = true;
				}
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),NivelLog.INFO);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("BndPagRefSatDAO - consCtaWSLinCap() - " + ERROR_CONEXION + e1 + "]", NivelLog.ERROR);
			}
		}
		return valor;
	}

	/**
	 * Se ejecuta el select para obtener el valor del Timeout para la invocaci�n
	 *                                del WebService de Validaci�n de L�nea de Captura.
	 * 
	 * @return Long con el valor del Timeout para el Web Service para la Validaci�n de L�nea de Captura
	 */
	public Long consTmoWSLinCap() {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
    Long valor = Long.valueOf(1000);

		String selectFull = String.format(SELECT_TMO_WSLC, TMOWSLC);
		EIGlobal.mensajePorTrace("WSCnfLinCapDAO - consTmoWSLinCap - query [" + selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format("WSCnfLinCapDAO consTmoWSLinCap - QRY [%s]",selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("WSCnfLinCapDAO consTmoWSLinCap : " + TMOWSLC + " Valor -> " + rs.getLong(1), NivelLog.DEBUG);
				valor = rs.getLong(1);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),NivelLog.INFO);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("WSCnfLinCapDAO - consTmoWSLinCap() - " + ERROR_CONEXION + e1 + "]", NivelLog.ERROR);
			}
		}
		return valor;
	}

	/**
	 * Se ejecuta el select para obtener la URL para la invocaci�n del WebService
	 *                                de Validaci�n de L�nea de Captura.
	 * 
	 * @return String con la URL del Web Service para la Validaci�n de L�nea de Captura
	 */
	public String consURLWSLinCap() {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
    String valor = "";

		String selectFull = String.format(SELECT_URL_WSLC, URLWSLC);
		EIGlobal.mensajePorTrace("WSCnfLinCapDAO - consURLWSLinCap - query [" + selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format("WSCnfLinCapDAO consURLWSLinCap - QRY [%s]",selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("WSCnfLinCapDAO consURLWSLinCap : " + URLWSLC + " Valor -> " + rs.getString(1), NivelLog.DEBUG);
				valor = rs.getString(1);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),NivelLog.INFO);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("WSCnfLinCapDAO - consURLWSLinCap() - " + ERROR_CONEXION + e1 + "]", NivelLog.ERROR);
			}
		}
		return valor;
	}

}
