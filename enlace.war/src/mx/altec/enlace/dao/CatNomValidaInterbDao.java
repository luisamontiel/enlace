package mx.altec.enlace.dao;

import java.sql.*;
import javax.sql.*;
import java.util.ArrayList;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class CatNomValidaInterbDao
{
		private PreparedStatement statement =null;
		private Connection conn=null;
		private PreparedStatement statement2 =null;
		private Connection conn2=null;
		String query;


	public boolean existeEnCatalogo(String contrato, String cuenta) {

		ResultSet rs = null;
		boolean result = false;
		try {
			switch(cuenta.trim().length()) {
				case 11:	//CHEQUES SANTANDER
							query = "SELECT COUNT(1) AS CUENTA FROM EWEB_CAT_NOM_EXTERNA WHERE NUM_CUENTA2 = '" + contrato + "' and substr(num_cuenta_ext, 7, 11) =  '" + cuenta + "' and substr(num_cuenta_ext, 0, 3) =  '014'";
							break;
				default:	//CLABE
							query = "SELECT COUNT(1) AS CUENTA FROM EWEB_CAT_NOM_EXTERNA WHERE NUM_CUENTA2 = '" + contrato + "' and NUM_CUENTA_EXT =  '" + cuenta + "'";
							break;
			}

			statement = conn.prepareStatement (query);
			rs = Consulta();
			if(rs.next()) {
				if(rs.getInt("CUENTA") == 1) {
					result = true;
				}
			}
		} catch(Exception e) {
		} finally {
			try {
				statement.close();
				statement = null;
			} catch(Exception ex) {}
			try {
				rs.close();
				rs = null;
			} catch(Exception ex) {}

		}
		return result;
	}


	public void Crear() throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion-----------PreparedStatament", EIGlobal.NivelLog.INFO);
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
    	}

	public void Cerrar() {
    		try {
				conn.close();
				conn = null;
			} catch(Exception e) {}
    	}

	private ResultSet Consulta() throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement.executeQuery();
		return rs;
		}

	protected void finalize() throws Throwable {
		super.finalize();
		this.Cerrar();
	}

}