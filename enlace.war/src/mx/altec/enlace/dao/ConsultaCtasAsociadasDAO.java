package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConsultaCuentasBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

@SuppressWarnings("unused")
public class ConsultaCtasAsociadasDAO extends GenericDAO {

	/** Constante Persona FISICA **/
	private static final char FISICA = 'F';
	/** Constante Persona MORAL**/
	private static final char MORAL  = 'M';

	/* Tipos de Cuenta en NUCL_CUENTAS */
	/** Producto chequera tradicional **/
	private static final char CH_TRADICIONAL = '1';
	/** Producto  Chequera Productiva**/
	private static final char CH_PRODUCTIVA	 = '2';
	/** Producto Chequera Maestra Fisica **/
	private static final char CH_MAESTRA_FIS = '3';
	/** Producto Chequera Maestra Empresarial **/
	private static final char CH_MAESTRA_EMP = '4';
	/** Producto Cheguera **/
	private static final char CH_PROGRESO	 = '5';
	/** Producto Chequera personal **/
	private static final char CH_PERSONAL	 = '7';
	/** Producto Chequera empresarial **/
	private static final char CH_EMPRESARIAL = '8';
	/** Producto Tarjeta **/
	private static final char TARJETA		 = 'T';
	/** Producto Tarjeta Invernomina **/
	private static final char TJ_INVERNOMINA = 'I';
	/** Producto Contrato Invernomina **/
	private static final char CONTRATO_INV   = 'C';
	/** Producto Cuenta de Ahorro **/
	private static final char CTA_AHORRO     = 'A';
	/** Producto Cuenta Concentradora **/
	private static final char CTA_CONCENTRADORA = 'O';
	/** Producto Cuenta Liquedadora **/
	private static final char CTA_LIQUIDADORA   = 'L';

	/* Tipos de Cuenta Manejados en Enlace */
	/** Producto CHEQUERA **/
	private static final int CHEQUERA		= 1;
	/** Producto PERSONAL **/
	private static final int PERSONAL		= 2;
	/** Producto EMPRESARIAL **/
	private static final int EMPRESARIAL	= 3;
	/** Producto CONTRATO_CB **/
	private static final int CONTRATO_CB	= 4;
	/** Producto TARJETA_CREDITO **/
	private static final int TARJETA_CREDITO= 5;
	/** Producto DOLARES **/
	private static final int DOLARES 		= 6;
	/** Producto CONCENTRADORA **/
	private static final int CONCENTRADORA 	= 7;
	/** Producto AHORRO **/
	private static final int AHORRO 		= 8;
	/** Producto LIQUIDADORA **/
	private static final int LIQUIDADORA	= 9;
	/** Producto CREDITO **/
	private static final int CREDITO		= 0;

	/** Constante ERROR del ResultSet  **/
	private static final String ERROR_RS= "Error al cerrar el resultset";
	/** Constante Attributo del Request Descripcion **/
	private static final String REQ_ATT_DESCRIPCION = "N_DESCRIPCION";
	/** Constante Attributo del Request Personalidad Juridica **/
	private static final String REQ_ATT_N_PER_JUR = "N_PER_JUR";
	/** Constante Attributo del Request Numero Cuenta Serfin **/
	private static final String REQ_ATT_NUM_CUENTA_SERFIN = "NUM_CUENTA_SERFIN";
	/** Cosntante para el Log **/
	private static final String LOG_TAG="[EDCPDFXML] ::: ConsultaCtasAsociadasDAO ::: ";

	/** Query tipocuenta en tabla Nucl **/
	private static final String QUERY_BUSCA_NUCL = " Select nrc.N_TIPO_CUENTA, nrc.N_PER_JUR, nrc.N_PRODUCT, nvl(tpe.CVE_PROD_ENL,-1) CVE_PROD_ENL "+
												   " From Nucl_Relac_Ctas nrc, Tct_Prod_Enlace tpe "+
												   " Where nrc.n_Product = tpe.Cve_Prod_Org(+) "+
												   " And nrc.Num_Cuenta = '%s' " +
												   " And nrc.Num_Cuenta2 = '%s' ";
	/** Query Buca Cve Prod en TCT */
	private static final String QUERY_BUSCA_TCT = " Select cve_prod_org, cve_prod_enl "+
												  " From tct_prod_enlace2 ";

	/** Query Cuenta Modulos **/
	private static final String QUERY_CUENTA_TCT = "Select CVE_MODULO, TIPO_CUENTA From Tct_Ctas_Modulo ORDER BY CVE_MODULO,TIPO_CUENTA";

	private static final String QUERY_TIPO_TC = "SELECT  TIPO_RELAC_CTAS from Nucl_Relac_Ctas WHERE "+
												" Num_Cuenta = '%s' " +
												" And Num_Cuenta2 = '%s' ";



	/**Variable local conn de tipo Connection la cual alojara la conexion a BD */
	private Connection conn = null;

	/**Variable boolean para manejar las operaciones de terceros del modulo 23 */
	private boolean terceros = false;

	private Map<Integer, String> cuentasModulo = null;
	private Map<String, String> productoEnlace = null;


	/**
	 * Metodo que devuelve las Cuentas totales.
	 * @param contrato : contrato del cliente
	 * @param cvePerfil : clave del Perfil del cliente
	 * @param usuario : valor del usuario en sesion
	 * @return List<ConsultaCuentasBean> lista con la cuentas
	 * @throws SQLException Manejo del error SQL
	 */


	public List<ConsultaCuentasBean>  getTarjetasCredito (String contrato, String cvePerfil,
			String usuario)throws SQLException  {

		List<ConsultaCuentasBean> listaFinal = new ArrayList<ConsultaCuentasBean>();
		Map<String, ConsultaCuentasBean> listaRegresa=
			getCuentasTotales ( contrato,  cvePerfil, usuario,"24,28",true);

		for(Map.Entry<String, ConsultaCuentasBean> entry : listaRegresa.entrySet())
		{
			ConsultaCuentasBean detalle=entry.getValue();
			EIGlobal.mensajePorTrace("-->"+detalle.getNumCuenta(), EIGlobal.NivelLog.DEBUG);

			if(detalle.getNumCuenta().length() == 16 || detalle.getNumCuenta().length() == 15 ){
			 {
				 listaFinal.add(detalle);
			 }

		}
		}
		EIGlobal.mensajePorTrace("ista listaFinal 1 Tarjetas de Credito-->"+listaFinal.size(), EIGlobal.NivelLog.INFO);
		listaRegresa=null;
		return listaFinal;

	}

	public List<ConsultaCuentasBean>  getCuentasTotales (String contrato, String cvePerfil,
			String usuario)  throws SQLException{


		List<ConsultaCuentasBean> listaFinal = new ArrayList<ConsultaCuentasBean>();
		Map<String, ConsultaCuentasBean> listaTC = new TreeMap<String,ConsultaCuentasBean>();
		Map<String, ConsultaCuentasBean> listaBME = new TreeMap<String,ConsultaCuentasBean>();
		Map<String, ConsultaCuentasBean> listaCheques = new TreeMap<String,ConsultaCuentasBean>();
		Map<String, ConsultaCuentasBean> listaTCDepurada = new TreeMap<String,ConsultaCuentasBean>();
		Map<String, ConsultaCuentasBean> listaRegresa=
			getCuentasTotales ( contrato,  cvePerfil,usuario,"1, 2, 4, 23, 24, 28", true);


		for(Map.Entry<String, ConsultaCuentasBean> entry : listaRegresa.entrySet()){
			ConsultaCuentasBean detalle=entry.getValue();
			EIGlobal.mensajePorTrace("Cuentas-->"+detalle.getNumCuenta(), EIGlobal.NivelLog.DEBUG);

			if(detalle.getNumCuenta().length() == 11)
			{
				listaCheques.put(detalle.getNumCuenta(), detalle);
			}

			else if(detalle.getNumCuenta().length() == 16 || detalle.getNumCuenta().length() == 15 )
			{
				listaTC.put(detalle.getNumCuenta(), detalle);
				EIGlobal.mensajePorTrace("1.- Tc (a)-->"+detalle.getNumCuenta(), EIGlobal.NivelLog.DEBUG);
			}
			else if( detalle.getNumCuenta().startsWith("BME"))
			{
				listaBME.put(detalle.getNumCuenta(), detalle);
			}



		}

		EIGlobal.mensajePorTrace("*******************lista que lista Cheques-->"+listaCheques.size(), EIGlobal.NivelLog.INFO);
		for(Map.Entry<String, ConsultaCuentasBean> entry : listaCheques.entrySet()){
			listaFinal.add(entry.getValue());
		}

		EIGlobal.mensajePorTrace("*******************lista que lista BME-->"+listaBME.size(), EIGlobal.NivelLog.INFO);
		for(Map.Entry<String, ConsultaCuentasBean> entry : listaBME.entrySet()){
			listaFinal.add(entry.getValue());
		}

		EIGlobal.mensajePorTrace("****lista Completa Mov TC-->"+listaTC.size(), EIGlobal.NivelLog.INFO);

		try{
		listaTCDepurada=depuraTarjetasTerceros(listaTC,contrato);
		}
		catch(Exception e ){
			EIGlobal.mensajePorTrace("*****---ERROR AL DEPURAR TARJETASD->"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			e.printStackTrace();

		}


		EIGlobal.mensajePorTrace("****lista depurada Mov TC-->"+listaTCDepurada.size(), EIGlobal.NivelLog.INFO);

		for(Map.Entry<String, ConsultaCuentasBean> entry : listaTCDepurada.entrySet()){
			EIGlobal.mensajePorTrace("2.- Tc (b)-->"+entry.getValue().getNumCuenta(), EIGlobal.NivelLog.DEBUG);
			listaFinal.add(entry.getValue());
		}


		listaCheques=null;
		listaTC=null;
		listaBME=null;
		listaRegresa=null;
		listaTCDepurada=null;

		EIGlobal.mensajePorTrace("*******************Tma�o lista que listaFinal**************************************"+listaFinal.size(), EIGlobal.NivelLog.INFO);


		return listaFinal;
	}


	public Map<String, ConsultaCuentasBean>  depuraTarjetasTerceros(Map<String, ConsultaCuentasBean> listaTC,String contrato )throws SQLException
	{
		Map<String, ConsultaCuentasBean> listaTCDepurada = new TreeMap<String,ConsultaCuentasBean>();
		conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);

		for(Map.Entry<String, ConsultaCuentasBean> entry : listaTC.entrySet()){
			ConsultaCuentasBean objCuenta=entry.getValue();
			if(esCuentaPropia(objCuenta.getNumCuenta(),contrato))
			{
			  listaTCDepurada.put(objCuenta.getNumCuenta(),objCuenta);

				EIGlobal.mensajePorTrace("**SI ES PROPIA, AGREGA TARJETA-->"+objCuenta.getNumCuenta(), EIGlobal.NivelLog.INFO);

			}
		}

		cierraConexion(conn);
		return listaTCDepurada;

	}
	public Map<String, ConsultaCuentasBean>   getCuentasTotales (String contrato, String cvePerfil,
			String usuario,String modulosConsultar,boolean forzarFiltro) throws SQLException {

		try{
		EIGlobal.mensajePorTrace("***Entrando a  getCuentasTotales "+ contrato +" "+ cvePerfil + " "+ usuario, EIGlobal.NivelLog.ERROR);

		String facultadGenerica = "";
		Set<ConsultaCuentasBean> listaSet = new HashSet<ConsultaCuentasBean>();
		List<ConsultaCuentasBean> lista = new ArrayList<ConsultaCuentasBean>();
		//List<ConsultaCuentasBean> listaRegresa = new ArrayList<ConsultaCuentasBean>();
		List<ConsultaCuentasBean> modulos = new ArrayList<ConsultaCuentasBean>();

		conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
		modulos.addAll(getModulos(modulosConsultar));

		if(forzarFiltro)
		{
		// Se agrega falcultad de consulta de movimientos
			ConsultaCuentasBean datoAd = new ConsultaCuentasBean();
			datoAd.setCveModulo(28);
			datoAd.setTipoRecuperacion("1");
			datoAd.setRestric("0");
			datoAd.setCveFacultad("CONSMOVTOS");
			modulos.add(datoAd);
		}



		String banco = getTipoBanco(contrato);
		int i = 0;

			EIGlobal.mensajePorTrace("***************************************modulos--->"+ modulos.size(), EIGlobal.NivelLog.ERROR);
		while( i < modulos.size()){

				EIGlobal.mensajePorTrace("*********************Ciclo****"+i+"**************************" , EIGlobal.NivelLog.ERROR);

			ConsultaCuentasBean dato = modulos.get(i);

			EIGlobal.mensajePorTrace("Iteracion "+i +" Modulo ->"+dato.getCveModulo()+"|"+dato.getNumCuenta()
					, EIGlobal.NivelLog.DEBUG);

			/*
			 * Se agrega el modulo en la funcion getExiste (usuario, contrato, cvePerfil, cveFacultad, modulo)
			 * AMR. C109836
			 */
			facultadGenerica = getExiste(usuario, contrato, cvePerfil, dato.getCveFacultad(), dato.getCveModulo());

				EIGlobal.mensajePorTrace("*********************facultadGenerica****"+facultadGenerica+"**************************" , EIGlobal.NivelLog.ERROR);

				if("1".equals(dato.getTipoRecuperacion()) || "2".equals(dato.getTipoRecuperacion()))
				{
				listaSet.addAll(getCuentas1(banco, dato.getRestric(), dato.getCveModulo(),
						facultadGenerica, contrato, dato.getCveFacultad(),
						cvePerfil, usuario));
			}
				if("1".equals(dato.getTipoRecuperacion()))
				{
				ConsultaCuentasBean dato2 = dato;
			        if (23!=dato.getCveModulo()){
            int siguiente = i+1 < modulos.size() ? ++i :i;
            dato2 = modulos.get(siguiente);
				}
				EIGlobal.mensajePorTrace("*** Entra validacion de TERCEROS: " +banco+"|"+dato2.getRestric()+"|"+
						dato2.getCveModulo()+"|"+facultadGenerica+"|"+contrato+"|"+dato2.getCveFacultad()
						, EIGlobal.NivelLog.INFO);
				/*
				 * Se agrega el modulo en la funcion getExiste (usuario, contrato, cvePerfil, cveFacultad, modulo)
				 *  adicional se enciende la bandera de operacion para terceros
				 * AMR. C109836
				 */
				terceros = true;
				facultadGenerica = getExiste(usuario, contrato, cvePerfil, dato2.getCveFacultad(), dato.getCveModulo());
				/*
				 *	Se agrega la restriccion en la cadena de la funcion getCuentas2
				 *	AMR. C109836
				 */
				listaSet.addAll(getCuentas2(banco, dato2.getRestric(), dato2.getCveModulo(), facultadGenerica, contrato,
						dato2.getCveFacultad(), cvePerfil, usuario));

			}

			i++;

			}
		EIGlobal.mensajePorTrace("***************************************FIN DE CICLO--->", EIGlobal.NivelLog.INFO);


		lista.addAll(listaSet);
		EIGlobal.mensajePorTrace("************************Inicia sqlValidaCuentasPorModulo***********listaInicial**************************->"+lista.size(), EIGlobal.NivelLog.INFO);



		Map<String, ConsultaCuentasBean> listaFinal=sqlValidaCuentasPorModulo(lista);

		cierraConexion(conn);

		EIGlobal.mensajePorTrace("************************Ternmina sqlValidaCuentasPorModulo********listaFinal*****************************->"+listaFinal.size(), EIGlobal.NivelLog.INFO);


		listaSet = null;
		lista = null;
		modulos = null;
		return listaFinal;
		}
		catch(Exception e ){
			EIGlobal.mensajePorTrace("*****---ERROR AL RECUPERAR CUENTAS->"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
			return null;

		}

	}


	/**
	 * Metodo para recuperar los modulos.
	 * @return Set<ConsultaCuentasBean> lista de los modulos a revisar
	 */
	private List<ConsultaCuentasBean> getModulos(String modulos) {
		EIGlobal.mensajePorTrace("***Entrando a  getModulos", EIGlobal.NivelLog.DEBUG);

		ResultSet rs=null;
		PreparedStatement pst = null;
		List<ConsultaCuentasBean> lista = new ArrayList<ConsultaCuentasBean>();

		try {
			StringBuilder query = new StringBuilder();
			query.append("SELECT B.CVE_MODULO ,B.TIPO_RECUPERACION ");
			query.append(",COUNT(DISTINCT A.CVE_MODULO) RESTRIC ,C.CVE_FACULTAD ");
			query.append("FROM TCT_MODULOS_ENLACE B INNER JOIN TCT_PROD_MODULO A ");
			query.append("ON A.CVE_MODULO(+) = B.CVE_MODULO INNER JOIN TCT_FAC_MODULO C ");
			query.append("ON B.CVE_MODULO = C.CVE_MODULO WHERE B.CVE_MODULO IN (");
			query.append(modulos);
			query.append(")");
			query.append("GROUP BY B.CVE_MODULO ,TIPO_RECUPERACION ,C.CVE_FACULTAD ");
			query.append(",DECODE(B.CVE_MODULO,'24','P',C.TIPO_RELAC_CTAS) ORDER BY B.CVE_MODULO ");
			EIGlobal.mensajePorTrace("***ConsultaCtasAsociadasDAO getModulos Query: " + query.toString(), EIGlobal.NivelLog.DEBUG);

			pst = conn.prepareStatement(query.toString());
			rs = pst.executeQuery();

			while (rs.next()) {

				ConsultaCuentasBean dato = new ConsultaCuentasBean();
				dato.setCveModulo(rs.getInt("CVE_MODULO"));
				dato.setTipoRecuperacion(rs.getString("TIPO_RECUPERACION"));
				dato.setRestric(rs.getString("RESTRIC"));
				dato.setCveFacultad(rs.getString("CVE_FACULTAD").trim());
				lista.add(dato);
			}
			EIGlobal.mensajePorTrace("***Modulos Query: "+query.toString(), EIGlobal.NivelLog.DEBUG);

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s getModulos() [%s]",LOG_TAG,e.getMessage()),
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				rs.close();
				pst.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(String.format("%s %s getModulos {finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
						EIGlobal.NivelLog.ERROR);
			}
		}
		return lista;
	}



	/**
	 * MEtodo para recuperar el tipo de Banco.
	 * @param contrato : contrato del cliente
	 * @return String : tipo de banco del contrato
	 */
	private String getTipoBanco (String contrato) {

		EIGlobal.mensajePorTrace("***Entrando a  getTipoBanco", EIGlobal.NivelLog.DEBUG);
		PreparedStatement pst = null;
		ResultSet rs = null;
		String tipoBanco = "";

		try {
			StringBuilder query = new StringBuilder();
			query.append("SELECT BANCO ");
			query.append("FROM TCT_CUENTAS_TELE ");
			query.append("WHERE NUM_CUENTA = '%s' ");

			String queryF = String.format(query.toString(), contrato);
			pst = conn.prepareStatement(queryF);
			rs = pst.executeQuery();

			while (rs.next()) {
				tipoBanco = rs.getString("BANCO").trim();
			}
			EIGlobal.mensajePorTrace("***Tipo Banco Query: "+query.toString(), EIGlobal.NivelLog.DEBUG);

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s getTipoBanco [%s]",LOG_TAG,e.getMessage()),
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				rs.close();
				pst.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(String.format("%s %s getTipoBanco {finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
						EIGlobal.NivelLog.ERROR);
			}
		}
		return tipoBanco;
	}

	/**
	 * Obtiene la facultad generica
	 * @param usuario : usuario en sesion
	 * @param contrato : contrato del cliente
	 * @param cvePerfil : clave del Perfil del usuario
	 * @param cveFacultad : clave de la Facultad del contrato
	 * @return String FacultadGenerica generada de la consulta
	 */
	private String getExiste(String usuario, String contrato, String cvePerfil, String cveFacultad, int modulo) {
		EIGlobal.mensajePorTrace("************Entrando a  getExiste", EIGlobal.NivelLog.DEBUG);
		PreparedStatement pst = null;
		ResultSet rs = null;
		String facultadGenerica = "";

		try {
			StringBuilder queryExiste = new StringBuilder();
			queryExiste.append("SELECT SUM(EXISTE) EXISTE FROM (SELECT COUNT(C.CVE_FACULTAD) EXISTE ");
			queryExiste.append("FROM  SEGU_USRPERFTELE A, SEGU_ASGPERFPROT B, SEGU_FACPERFPROT C ");
			queryExiste.append("WHERE A.CVE_USUARIO     = '%s' ");
			queryExiste.append("AND   A.NUM_CUENTA      = '%s' ");
			queryExiste.append("AND   A.CVE_PERFIL      = '%s' ");
			queryExiste.append("AND   A.CVE_PERFIL      = B.CVE_PERFIL AND   B.CVE_PERFIL_PROT = C.CVE_PERFIL_PROT ");
			queryExiste.append("AND   C.CVE_FACULTAD    = '%s' ");
			queryExiste.append("AND   C.CVE_FACULTAD NOT IN ");
			queryExiste.append("(SELECT A.CVE_FACULTAD FROM   SEGU_FACEXTRAORD A ");
			queryExiste.append("WHERE  A.CVE_PERFIL      = '%s' ");
			queryExiste.append("AND    A.CVE_FACULTAD = '%s' ");
			queryExiste.append("AND    A.SIGNO           = '-') HAVING COUNT(C.CVE_FACULTAD) > 0 ");
			queryExiste.append("UNION ");
			queryExiste.append("SELECT COUNT(A.CVE_FACULTAD) EXISTE FROM  SEGU_FACEXTRAORD A ");
			queryExiste.append("WHERE A.CVE_PERFIL      = '%s' ");
			queryExiste.append("AND   A.CVE_FACULTAD  = '%s' ");
			queryExiste.append("AND   A.SIGNO           = '+' HAVING COUNT(A.CVE_FACULTAD) > 0) ");
			queryExiste.append("HAVING SUM(EXISTE) > 0 ");

			String queryF = String.format(queryExiste.toString(), usuario, contrato, cvePerfil, cveFacultad,
					cvePerfil, cveFacultad, cvePerfil, cveFacultad);

			pst = conn.prepareStatement(queryF);
			rs = pst.executeQuery();

			while (rs.next()) {
				facultadGenerica = rs.getString("EXISTE").trim();
				EIGlobal.mensajePorTrace("--> Valor de Facultad Generica: "+facultadGenerica, EIGlobal.NivelLog.DEBUG);

			}
			EIGlobal.mensajePorTrace("***Existe Query: "+queryF, EIGlobal.NivelLog.DEBUG);

			if(23==modulo && terceros){
					facultadGenerica = "";
					terceros = false;
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s getExiste {finally} [%s]",LOG_TAG,e.getMessage()),
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				rs.close();
				pst.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(String.format("%s %s getExiste {finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
						EIGlobal.NivelLog.ERROR);
			}
		}
		return facultadGenerica;
	}

	/**
	 * Metodo para obtener las cuentas
	 * @param tipoBanco : tipo de Banco del contrato
	 * @param restric : restricccion de la consulta
	 * @param cveModulo : clave Modulo del contrato
	 * @param facultadGenerica : facultad Generica del contrato
	 * @param contrato : numero de contrato
	 * @param cveFacultad : clave Facultad del contrato
	 * @param cvePerfil : clave Perfil de cliente
	 * @param usuario : usuario en sesion
	 * @return Set<ConsultaCuentasBean> cuentas resultantes de la consulta
	 * @throws SQLException Manejo del error SQL
	 */
	private Set<ConsultaCuentasBean> getCuentas1 (String tipoBanco, String restric, int cveModulo,
			String facultadGenerica, String contrato, String cveFacultad, String cvePerfil,
			String usuario) throws SQLException {

		EIGlobal.mensajePorTrace("***Entrando a  getCuentas1", EIGlobal.NivelLog.DEBUG);

		String cadenaBanco = " ,' ' NUM_CUENTA_SERFIN ";
		String restriccion = "";
		String restricProd = "";
		boolean caso = false;
		Set<ConsultaCuentasBean> lista = new HashSet<ConsultaCuentasBean>();

		if("014".equals(tipoBanco)){
			EIGlobal.mensajePorTrace("***Tipo banco 014 asigna cadenaBanco", EIGlobal.NivelLog.DEBUG);
			cadenaBanco = " , 'NINGUNA' NUM_CUENTA_SERFIN ";
			int res = Integer.parseInt(restric);
			if(res > 0){
				EIGlobal.mensajePorTrace("***Si restric > 0 asigna restriccion y restricProd", EIGlobal.NivelLog.DEBUG);
				restriccion = " , TCT_PROD_MODULO Y ";
				restricProd = " AND Y.CVE_SUBPRODUCTO = SUBSTR(B.NUM_CUENTA,1,2) AND Y.CVE_MODULO = "
					.concat(String.valueOf(cveModulo)).concat(" ");
			}
		}else if("003".equals(tipoBanco)){
			EIGlobal.mensajePorTrace("***Tipo banco 003 asigna cadenaBanco y restriccion", EIGlobal.NivelLog.DEBUG);
			cadenaBanco = " , Z.NUM_CUENTA_SERFIN ";
			restriccion = " , TCT_CUENTAS_SERFIN Z ";

			if(!"".equals(facultadGenerica)){
				EIGlobal.mensajePorTrace("***Si existe facultad asigna restricProd", EIGlobal.NivelLog.DEBUG);
				restricProd = " AND Z.NUM_CUENTA(+) = B.NUM_CUENTA ";
			}else{
				EIGlobal.mensajePorTrace("***No existe facultad asigna restricProd", EIGlobal.NivelLog.DEBUG);
				restricProd = " AND Z.NUM_CUENTA(+) = A.NUM_CUENTA ";
				caso = true;
			}

		}

		if(!"".equals(facultadGenerica)){
			EIGlobal.mensajePorTrace("***Si existe facultad se ejecuta query 1", EIGlobal.NivelLog.DEBUG);
			lista.addAll(getCuentasc1(cadenaBanco,restriccion,restricProd,contrato,cveFacultad,cvePerfil, 1, caso, cveModulo));
		}else{
			EIGlobal.mensajePorTrace("***No existe facultad se ejecuta query 2", EIGlobal.NivelLog.DEBUG);
			lista.addAll(getCuentasc2(cadenaBanco,restriccion,restricProd,contrato,cveFacultad,cvePerfil,usuario,1,cveModulo));

		}
		return lista;

	}

	/**
	 * Se modifica la funcion para que reciba la restriccion, se le agrega validacion en el banco = 014
	 *
	 * @param tipoBanco : tipo de Banco del contrato
	 * @param restric : restricccion de la consulta
	 * @param cveModulo : Clave del Modulo
	 * @param facultadGenerica : facultad Generica del contrato
	 * @param contrato : numero de contrato
	 * @param cveFacultad : clave Facultad del contrato
	 * @param cvePerfil : clave Perfil de cliente
	 * @param usuario : usuario en sesion
	 * @return Set<ConsultaCuentasBean> cuentas resultantes de la consulta
	 * @throws SQLException Manejo del error SQL
	 */
	private Set<ConsultaCuentasBean> getCuentas2 (String tipoBanco, String restric, int cveModulo,
			String facultadGenerica, String contrato, String cveFacultad, String cvePerfil,
			String usuario) throws SQLException {

		EIGlobal.mensajePorTrace("***Entrando a  getCuentas2", EIGlobal.NivelLog.DEBUG);

		String cadenaBanco = " ,' ' NUM_CUENTA_SERFIN ";
		String restriccion = "";
		String restricProd = "";
		boolean caso = false;
		Set<ConsultaCuentasBean> lista = new HashSet<ConsultaCuentasBean>();

		if("014".equals(tipoBanco)){
			EIGlobal.mensajePorTrace("***Tipo banco 014 asigna cadenaBanco", EIGlobal.NivelLog.DEBUG);
			cadenaBanco = " , 'NINGUNA' NUM_CUENTA_SERFIN ";
			int res = Integer.parseInt(restric);
			if(res > 0){
				EIGlobal.mensajePorTrace("***Si restric > 0 asigna restriccion y restricProd", EIGlobal.NivelLog.DEBUG);
				restriccion = " , TCT_PROD_MODULO Y ";
				restricProd = " AND Y.CVE_SUBPRODUCTO = SUBSTR(B.NUM_CUENTA,1,2) AND Y.CVE_MODULO = "
					.concat(String.valueOf(cveModulo)).concat(" ");
			}
		}else if("003".equals(tipoBanco)){
			EIGlobal.mensajePorTrace("***Tipo banco 003 asigna cadenaBanco y restriccion", EIGlobal.NivelLog.DEBUG);
			cadenaBanco = " , Z.NUM_CUENTA_SERFIN ";
			restriccion = " , TCT_CUENTAS_SERFIN Z ";

			if(!"".equals(facultadGenerica)){
				EIGlobal.mensajePorTrace("***Si existe facultad asigan restricProd ", EIGlobal.NivelLog.DEBUG);
				restricProd = " AND Z.NUM_CUENTA(+) = B.NUM_CUENTA ";
			}else{
				EIGlobal.mensajePorTrace("***Si existe facultad asigan restricProd", EIGlobal.NivelLog.DEBUG);
				restricProd = " AND Z.NUM_CUENTA(+) = A.NUM_CUENTA ";
				caso = true;
			}

		}

		if(!"".equals(facultadGenerica)){
			EIGlobal.mensajePorTrace("***Si existe facultad se ejecuta query 1", EIGlobal.NivelLog.DEBUG);
			lista.addAll(getCuentasc1(cadenaBanco, restriccion,
					restricProd, contrato,
					cveFacultad, cvePerfil, 2, caso, cveModulo));
		}else{
			EIGlobal.mensajePorTrace("***No existe facultad se ejecuta query 2", EIGlobal.NivelLog.DEBUG);
			lista.addAll(getCuentasc2 (cadenaBanco, restriccion, restricProd,
					contrato, cveFacultad, cvePerfil, usuario, 2, cveModulo));

		}
		return lista;

	}

	/**
	 * Metodo para obtener las cuentas.
	 *
	 * @param cadenaBanco : cadena que indica campos a consultar
	 * @param restriccion : restriccion tabla de la cual se realizara una consulta de campo
	 * @param restricProd : restriccion de Producto de la consulta
	 * @param contrato : contrato del cliente
	 * @param cveFacultad : clave Facultad del contrato
	 * @param cvePerfil : clave Perfil de cliente
	 * @param tipo : tipo de consulta que se realizara
	 * @param caso : condicion para la consulta
	 * @param cveModulo : Clave del Modulo
	 * @return Set<ConsultaCuentasBean> cuentas resultantes de la consulta
	 * @throws SQLException Manejo del error SQL
	 */
	private Set<ConsultaCuentasBean> getCuentasc1 (String cadenaBanco,
			String restriccion, String restricProd,String contrato,
			String cveFacultad, String cvePerfil, int tipo, boolean caso, int cveModulo) throws SQLException {

		EIGlobal.mensajePorTrace("***Entrando a  getCuentasc1", EIGlobal.NivelLog.DEBUG);

		String identificador = tipo == 1 ? "'P'" : "'T'";
		PreparedStatement pst = null;
		ResultSet rs = null;
		Set<ConsultaCuentasBean> lista = new HashSet<ConsultaCuentasBean>();

		try {
			StringBuilder query = new StringBuilder();
			query.append("SELECT B.NUM_CUENTA, B.N_DESCRIPCION, B.N_PRODUCT, B.N_SUBPROD, B.N_PER_JUR, ");
			query.append(identificador);
			query.append(" TIPO_RELAC_CTAS ");
			query.append(cadenaBanco);
			query.append(" FROM NUCL_RELAC_CTAS B ");
			query.append(restriccion);
			query.append(" WHERE B.NUM_CUENTA2 = '%s' AND LENGTH(TRIM(B.NUM_CUENTA)) <> 10 AND B.NUM_CUENTA NOT IN (SELECT A.NUM_CUENTA FROM SEGU_FACCTAPERF A ");
			if(caso){
				query.append(restriccion);
			}
			query.append(" WHERE (A.CVE_FACULTAD = '%s' OR A.CVE_FACULTAD = 'CONSSALDOS' OR A.CVE_FACULTAD = 'CONSMOVTOS') AND A.CVE_PERFIL = '%s' AND A.SIGNO = '-'");

			if(!caso){
				query.append(")");
			}
			query.append(" AND B.TIPO_RELAC_CTAS = ");
			query.append(identificador);
			query.append(restricProd);
			query.append(" UNION SELECT TRIM(C.NUM_CUENTA_EXT), C.NOMBRE,'TARJCRED', 'TARJCRED', 'F', 'X', 'NINGUNA' FROM NUCL_CTA_EXTERNA C WHERE C.NUM_CUENTA = '%s' AND LENGTH(TRIM(C.NUM_CUENTA_EXT)) IN (15,16)");
			if(tipo == 2){
				query.append(" ORDER BY 1 ");
			}
			String queryF = String.format(query.toString(), contrato, cveFacultad, cvePerfil, contrato);
			pst = conn.prepareStatement(queryF);

			EIGlobal.mensajePorTrace("Query getCuentasc1: "+queryF, EIGlobal.NivelLog.DEBUG);
			rs = pst.executeQuery();

			while (rs.next()) {

				ConsultaCuentasBean dato = new ConsultaCuentasBean();
				dato.setNumCuenta(rs.getString("NUM_CUENTA").trim());
				dato.setNDescripcion( rs.getString(REQ_ATT_DESCRIPCION)!= null ? rs.getString(REQ_ATT_DESCRIPCION).trim():"SIN DESCIRPCION" );
				dato.setNProduct(rs.getString("N_PRODUCT"));
				dato.setNSubprod(rs.getString("N_SUBPROD"));
				dato.setNPerJur( rs.getString(REQ_ATT_N_PER_JUR)!=null ? rs.getString(REQ_ATT_N_PER_JUR).trim() : null );
				dato.setTipoRelacCtas(rs.getString("TIPO_RELAC_CTAS").trim());
				dato.setNumCuentaSerfin(rs.getString(REQ_ATT_NUM_CUENTA_SERFIN)!=null ? rs.getString(REQ_ATT_NUM_CUENTA_SERFIN).trim() : null);
				/**
				 * @author Z702596 - Ignacio R.
				 * Cambios necesarios para recuperar los datos de modulo y contrato para el metodo sql_determinaTipoCuenta,
				 * el cual filtra las cuentas segun su tipo y su permisos por modulo.
				 **/
				dato.setCveModulo(cveModulo);
				dato.setNumContrato(contrato);

				lista.add(dato);
			}

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s getCuentasc1 [%s]",LOG_TAG,e.getMessage()),
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				rs.close();
				pst.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(String.format("%s %s getCuentasc1 {finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
						EIGlobal.NivelLog.ERROR);
			}
		}
		return lista;
	}

	/**
	 * Metodo para obtener las cuentas.
	 *
	 * @param cadenaBanco : cadena que indica campos a consultar
	 * @param restriccion : restriccion tabla de la cual se realizara una consulta de campo
	 * @param restricProd : restriccion de Producto de la consulta
	 * @param contrato : contrato del cliente
	 * @param cveFacultad : clave Facultad del contrato
	 * @param cvePerfil : clave Perfil de cliente
	 * @param usuario : usuario en sesion
	 * @param tipo : tipo de consulta que se realizara
	 * @param cveModulo : Clave del Modulo
	 * @return Set<ConsultaCuentasBean> cuentas resultantes de la consulta
	 * @throws SQLException Manejo del error SQL
	 */
	private Set<ConsultaCuentasBean> getCuentasc2 (String cadenaBanco,
			String restriccion, String restricProd,String contrato,
			String cveFacultad, String cvePerfil, String usuario, int tipo, int cveModulo) throws SQLException {

		EIGlobal.mensajePorTrace("***Entrando a  getCuentasc2", EIGlobal.NivelLog.DEBUG);

		String identificador = tipo == 1 ? "'P'" : "'T'";
		ResultSet rs = null;
		PreparedStatement pst = null;
		Set<ConsultaCuentasBean> lista = new HashSet<ConsultaCuentasBean>();

		try {

			StringBuilder query = new StringBuilder();
			query.append("SELECT A.NUM_CUENTA, B.N_DESCRIPCION, B.N_PRODUCT, B.N_SUBPROD, B.N_PER_JUR, ");
			query.append(identificador);
			query.append(" TIPO_RELAC_CTAS ");
			query.append(cadenaBanco);
			query.append(" FROM SEGU_FACCTAPERF A, NUCL_RELAC_CTAS B, SEGU_USRPERFTELE C ");
			query.append(restriccion);
			//---------Inicia cambio para fac CONSSALDOS y CONSMOVTOS
			//query.append(" WHERE A.CVE_FACULTAD = '%s' ");
			query.append(" WHERE LENGTH(TRIM(B.NUM_CUENTA)) <> 10 ");
			query.append(" AND B.NUM_CUENTA NOT IN (SELECT A.NUM_CUENTA FROM SEGU_FACCTAPERF A ");
			query.append(" WHERE A.CVE_PERFIL = '%s' AND A.SIGNO = '-' ");
			query.append(" AND (A.CVE_FACULTAD = '%s' OR A.CVE_FACULTAD = 'CONSSALDOS' OR A.CVE_FACULTAD = 'CONSMOVTOS'");
			query.append("))");
			query.append(" AND A.NUM_CUENTA = B.NUM_CUENTA AND C.CVE_USUARIO = '%s' ");
			//query.append(" AND A.NUM_CUENTA = B.NUM_CUENTA AND A.SIGNO = '+' AND C.CVE_USUARIO = '%s' ");
			//---------Termina cambio para fac CONSSALDOS y CONSMOVTOS
			query.append(" AND C.NUM_CUENTA = '%s'  AND C.NUM_CUENTA = B.NUM_CUENTA2 AND C.CVE_PERFIL = '%s' ");
			query.append(" AND C.CVE_PERFIL = A.CVE_PERFIL ");
			query.append(restricProd);
			query.append(" UNION SELECT TRIM(C.NUM_CUENTA_EXT), C.NOMBRE,'TARJCRED', 'TARJCRED', 'F', 'X', 'NINGUNA' FROM NUCL_CTA_EXTERNA C WHERE C.NUM_CUENTA = '%s' AND LENGTH(TRIM(C.NUM_CUENTA_EXT)) IN (15,16)");
			query.append(" ORDER BY 1 ");
			//query.append(" And sql_determinatipocuenta(b.num_cuenta,b.num_cuenta2,"+cveModulo+") = 1 ");
			//EIGlobal.mensajePorTrace("Query getCuentasc2: "+query, EIGlobal.NivelLog.DEBUG);

			//String queryF = String.format(query.toString(), cveFacultad, usuario, contrato, cvePerfil);
			String queryF = String.format(query.toString(), cvePerfil, cveFacultad, usuario, contrato, cvePerfil, contrato);
			EIGlobal.mensajePorTrace("Query getCuentasc2: "+queryF, EIGlobal.NivelLog.INFO);

			pst = conn.prepareStatement(queryF);
			rs = pst.executeQuery();
				EIGlobal.mensajePorTrace("Query getCuentasc2 [Se ejecuto] ", EIGlobal.NivelLog.DEBUG);

			while (rs.next()) {
					EIGlobal.mensajePorTrace("Query getCuentasc2 [Se ejecuto] iterando resultado ", EIGlobal.NivelLog.DEBUG);
				ConsultaCuentasBean dato = new ConsultaCuentasBean();
				dato.setNumCuenta(rs.getString("NUM_CUENTA").trim());
				dato.setNDescripcion( rs.getString(REQ_ATT_DESCRIPCION)!= null ? rs.getString(REQ_ATT_DESCRIPCION).trim():"SIN DESCRIPCION" );
				dato.setNProduct(rs.getString("N_PRODUCT"));
				dato.setNSubprod(rs.getString("N_SUBPROD"));
				dato.setNPerJur( rs.getString(REQ_ATT_N_PER_JUR)!=null ? rs.getString(REQ_ATT_N_PER_JUR).trim() : null );
				dato.setTipoRelacCtas(rs.getString("TIPO_RELAC_CTAS").trim());
				dato.setNumCuentaSerfin(rs.getString(REQ_ATT_NUM_CUENTA_SERFIN)!=null ? rs.getString(REQ_ATT_NUM_CUENTA_SERFIN).trim() : null);
				/**
				 * @author Z702596 - Ignacio R.
				 * Cambios necesarios para recuperar los datos de modulo y contrato para el metodo sql_determinaTipoCuenta,
				 * el cual filtra las cuentas segun su tipo y su permisos por modulo.
				 **/
				dato.setCveModulo(cveModulo);
				dato.setNumContrato(contrato);

				lista.add(dato);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace(String.format("%s getCuentasc2 - YYY--- ERROR ORACLE [%s]",LOG_TAG,e.getMessage()),
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {

				rs.close();
				pst.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
				EIGlobal.mensajePorTrace(String.format("%s %s  - YYY 2--- getCuentasc2 {finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
						EIGlobal.NivelLog.ERROR);
			}
		}
		return lista;
	}



	/**
	 * Metodo para validar las cuentas por modulo.
	 * @param lista : Bean ConsultaCuentasBean que contiene la informacion de la cuenta.
	 * @return List<ConsultaCuentasBean> lista Validada
	 */
	private Map<String, ConsultaCuentasBean> sqlValidaCuentasPorModulo(List<ConsultaCuentasBean> lista) {
		EIGlobal.mensajePorTrace(String.format("[%s] Elementos en [List<ConsultaCuentasBean> lista] al entrar al metodo [sqlValidaCuentasPorModulo]", lista.size())
				, EIGlobal.NivelLog.DEBUG);
		Map<String, ConsultaCuentasBean> listaAux = new TreeMap<String,ConsultaCuentasBean>();
		for(ConsultaCuentasBean dato: lista){

			if(!listaAux.containsKey(dato.getNumCuenta()))
			{ int tipoCuenta=sqlDeterminaTipoCuenta(dato.getNumContrato(), dato.getNumCuenta(), dato.getCveModulo());
				if(tipoCuenta==1 ) {
						listaAux.put(dato.getNumCuenta(),dato);
				}
			}
			else
			{
				EIGlobal.mensajePorTrace("************La cuenta y se encuentra en la lista Final--->"+dato.getNumCuenta(), EIGlobal.NivelLog.DEBUG);

			}
		}
		return listaAux;
	}

	/**
	 * @param contrato : Contrato a comparar
	 * @param cuenta : Cuenta seleccionada
	 * @param modulo : Modulo a evaluar
	 * @return boolean
	 */
	private int sqlDeterminaTipoCuenta(String contrato,String cuenta, int modulo) {
		EIGlobal.mensajePorTrace("Contrato ["+contrato+"] Cuenta ["+cuenta+"] Modulo ["+modulo+"]",EIGlobal.NivelLog.DEBUG);
		String sqlCuentaAltair = cuenta,
			sqlContrato = contrato,
			sqlCveProdEnlace2 = null,
			queryF="";
		boolean existeModulo = false;
		int Result=0,
			locResult = 0,

			locIntTipoCuenta = CHEQUERA,
			sqlTipoCuentaNullValor = 0,
			sqlPersonaJuridicaNullValor = 0,
			sqlCveProductoNullValor = 0,
			sqlCveProdEnlaceNullValor = 0;
		EIGlobal.mensajePorTrace(
				"##########################################################\n" +
				"sqlCuentaAltair <<"+sqlCuentaAltair+">>",
				EIGlobal.NivelLog.DEBUG);


		existeModulo=consultaCuentasModulo(modulo);
		EIGlobal.mensajePorTrace("************************existeModulo->"+modulo+"<--"+existeModulo, EIGlobal.NivelLog.DEBUG);



		if (!existeModulo) {
			Result = 1;
		}
		else {
	    String[] resultado=consultaDetalleCuenta(sqlContrato, sqlCuentaAltair);


		if (resultado!=null) {
			/* Se asignan valores a las variables xxxNullValor si el query viene con valores nulos*/
			if ( resultado[0] == null ) {
				sqlTipoCuentaNullValor=-1; }
			if ( resultado[1] == null ) {
				sqlPersonaJuridicaNullValor = -1; }
			if ( resultado[2] == null ) {
				sqlCveProductoNullValor = -1; }
			if ( resultado[3]!=null && "-1".equals(resultado[3]) ) {
				sqlCveProdEnlaceNullValor = -1; }

			EIGlobal.mensajePorTrace(String.format("resultado[[[0]]]-TipoCuenta---------------->[%s]\n" +
												   "resultado[[[1]]]-Persona------------------->[%s]\n" +
												   "resultado[[[2]]]-CveProducto--------------->[%s]\n" +
												   "resultado[[[3]]]-CveProdEnlace------------->[%s]\n" +
												   "sqlCveProductoNullValor--->[%s]\n" +
												   "sqlCveProdEnlaceNullValor->[%s]\n" +
												   "sqlTipoCuentaNullValor---->[%s]",
				resultado[0],resultado[1],resultado[2],resultado[3],sqlCveProductoNullValor,
				sqlCveProdEnlaceNullValor,sqlTipoCuentaNullValor),
				EIGlobal.NivelLog.DEBUG);
			/* Inicia validacion por cve Producto */
			if (sqlCveProductoNullValor == -1){
				/* Para N_PRODUCT==null tipo es CHEQUERA */
				locIntTipoCuenta=CHEQUERA;
			}
			else {
				resultado[2]=resultado[2].trim();
				//sqlPersonaJuridica=sqlPersonaJuridica;
				if ( resultado[2].length()>0 ) {
					if (sqlCveProdEnlaceNullValor == -1) {
						EIGlobal.mensajePorTrace(
								String.format("No se tiene CveProdEnlace de TCT_PROD_ENLACE\n" +
										"Tipo de cuenta..<<%s>>\n" +
										"CH_PERSONAL.....<<%s>>\n" +
										"SqlCveProducto..<<%s>>",resultado[0],CH_PERSONAL,resultado[2]),
								EIGlobal.NivelLog.DEBUG);
						if (resultado[0]!=null && resultado[0].charAt(0)== CH_PERSONAL ){
							EIGlobal.mensajePorTrace("CH_PERSONAL", EIGlobal.NivelLog.DEBUG);
							/* Si Tipocuenta es 7 CH_PERSONAL validar si es F-Personal y si es M-Empresarial*/
							if (resultado[1]!=null && resultado[1].charAt(0) == FISICA)
							{ locIntTipoCuenta = PERSONAL; }
							else if (resultado[1]!=null && resultado[1].charAt(0) == MORAL) {
								locIntTipoCuenta = EMPRESARIAL; }
						}
						else {
							/* Si no, se busca en la tabla tct_prod_enlace2 x sqlTipoCuenta*/

							locIntTipoCuenta=consultaProductosEnlace(resultado[0],locIntTipoCuenta);

						}
					}
					else {
						locIntTipoCuenta = Integer.valueOf(resultado[3]);
					}
				}
				else {

					locIntTipoCuenta = CHEQUERA;
				}
			}
		}



		EIGlobal.mensajePorTrace("************************locIntTipoCuenta->"+locIntTipoCuenta, EIGlobal.NivelLog.DEBUG);

			Result=consultaCuentasModulo( modulo, locIntTipoCuenta);


		}
		return Result;
	}

	/**
	 * @return objeto de conexion
	 */
	public Connection getConn() {
		return conn;
	}

	/**
	 * @param conn : objeto de conexion
	 */
	public void setConn(Connection conn) {
		this.conn = conn;
	}


	public int consultaProductosEnlace(String cveProducto, int cveProductoEnlaceInt)
	{

   //EIGlobal.mensajePorTrace("*******************************consultaProductosEnlace*********************", EIGlobal.NivelLog.ERROR);


	/** Query Buca Cve Prod en TCT */
    String cveProductoEnlace=null;
   // int cveProductoEnlaceInt=0;

	if(productoEnlace==null)
	{
		productoEnlace=new HashMap<String, String>();

				ResultSet rs = null;
				PreparedStatement pst = null;


			//	String sqlCveProdEnlace2 = null;
				try {
					pst = conn.prepareStatement(QUERY_BUSCA_TCT);
					rs = pst.executeQuery();
					EIGlobal.mensajePorTrace("***Query consultaProductosEnlace: "+QUERY_BUSCA_TCT, EIGlobal.NivelLog.INFO);
					while (rs.next()) {

						productoEnlace.put(rs.getString(1), rs.getString(2));
					//	sqlCveProdEnlace2=rs.getString("cve_prod_enl");
					}

				//return sqlCveProdEnlace2;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%s sqlDeterminaTipoCuenta {1} [%s]",LOG_TAG,e.getMessage()),
						EIGlobal.NivelLog.ERROR);
				//return sqlCveProdEnlace2;
			} finally {
				try {
					rs.close();
					pst.close();
				} catch (SQLException ex) {
					EIGlobal.mensajePorTrace(String.format("%s %s sqlDeterminaTipoCuenta {1}{finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
							EIGlobal.NivelLog.ERROR);
				}
		}
	}
			else
			{
			  if(productoEnlace.containsKey(cveProducto))
			  {
				cveProductoEnlace= productoEnlace.get(cveProducto);

				if (cveProductoEnlace!=null)
				 {
					cveProductoEnlace=cveProductoEnlace.trim();

					if(cveProductoEnlace.length()>0)
						cveProductoEnlaceInt=Integer.valueOf(cveProductoEnlace);
				 }

			  }
			}



	EIGlobal.mensajePorTrace("***Obtiene Producto cveProducto: "+cveProducto+": cveProductoEnlace:"+cveProductoEnlaceInt, EIGlobal.NivelLog.DEBUG);
	return cveProductoEnlaceInt;

}




	public void cargaDeOracleCuentasModulo()
	{

		//EIGlobal.mensajePorTrace("************************67*******cargaDeOracleCuentasModulo*********************", EIGlobal.NivelLog.ERROR);

			cuentasModulo=new  HashMap<Integer, String>();

			String queryF = String.format(QUERY_CUENTA_TCT);
			ResultSet rs = null;
			PreparedStatement pst = null;
			int sqlContador=0;
			int idActualAnterior=-1;
			int idActualActual=-1;
			String subModulos=null;
			String subModulosAcum=null;
			try {
				pst = conn.prepareStatement(queryF);
				rs = pst.executeQuery();
			EIGlobal.mensajePorTrace("Query a consultaCuentasModulo: " + queryF, EIGlobal.NivelLog.DEBUG);
				while (rs.next()) {

					idActualActual=rs.getInt(1);
					subModulos=rs.getString(2);


					if(idActualAnterior==-1)
					{

						idActualAnterior=idActualActual;
						subModulosAcum=subModulos;
					}
					else
					{

						if(idActualActual==idActualAnterior)
						{
							subModulosAcum=subModulosAcum+"-"+subModulos;
						}
						else
						{
							cuentasModulo.put(idActualAnterior, subModulosAcum);
							EIGlobal.mensajePorTrace("-----Agreganod a Lista-----> id["+idActualAnterior+"]"+"--->" + subModulosAcum, EIGlobal.NivelLog.DEBUG);
							idActualAnterior=idActualActual;
							subModulosAcum=subModulos;



						}
					}


					//sqlContador=rs.getInt(1);
				}
				cuentasModulo.put(idActualAnterior, subModulosAcum);
				EIGlobal.mensajePorTrace("-----Agrega Ultimo elemento  a Lista-----> id["+idActualAnterior+"]"+"--->" + subModulosAcum, EIGlobal.NivelLog.DEBUG);

			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%s sqlDeterminaTipoCuenta {2} [%s]",LOG_TAG,e.getMessage()),
						EIGlobal.NivelLog.ERROR);

			} finally {
				EIGlobal.mensajePorTrace("Registros en Tct_Ctas_Modulo: " + cuentasModulo.size(), EIGlobal.NivelLog.DEBUG);
				try {
					rs.close();
					pst.close();
				} catch (SQLException ex) {
					EIGlobal.mensajePorTrace(String.format("%s %s sqlDeterminaTipoCuenta {2}{finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
							EIGlobal.NivelLog.ERROR);
				}
			}





	}

	/*Valida el tipo de cuenta resultante con los tipos que tienen permiso al modulo en particular*/

	public boolean consultaCuentasModulo(int modulo)
	{
		if(cuentasModulo==null)
		cargaDeOracleCuentasModulo();

		return cuentasModulo.containsKey(modulo);



}

	/*Valida el tipo de cuenta resultante con los tipos que tienen permiso al modulo en particular*/

	public int consultaCuentasModulo(int modulo, int tipoCuenta)
	{


		String tipoCuentas="";
		String tipoCuentaParam=String.valueOf(tipoCuenta);
		if(cuentasModulo==null)
			cargaDeOracleCuentasModulo();


		EIGlobal.mensajePorTrace("<<---Modulo--->"+modulo, EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("<<--tipoCuenta--->"+tipoCuenta, EIGlobal.NivelLog.ERROR);
			if(cuentasModulo.containsKey(modulo))
			{
				tipoCuentas= cuentasModulo.get(modulo);



				if(tipoCuentas!=null)
				{
					String[] tipoCuentasAsociadas=tipoCuentas.split("-");

					  for(int i=0;i<tipoCuentasAsociadas.length;i++)
					  {


						  if(tipoCuentasAsociadas[i]!=null
								  && tipoCuentasAsociadas[i].equalsIgnoreCase(tipoCuentaParam))
						  {
							  EIGlobal.mensajePorTrace("<<--tipoCuenta--->Coincide--->"+tipoCuentasAsociadas[i]+"<-- con-->"+tipoCuentaParam, EIGlobal.NivelLog.ERROR);
							  return 1;
						  }
					  }


				}
			}
			return 0;


}

	public boolean esCuentaPropia(String cuenta,String contrato){

		//EIGlobal.mensajePorTrace("**************************2*****consultaDetalleCuente*********************", EIGlobal.NivelLog.ERROR);

				ResultSet rs = null;
				PreparedStatement pst = null;
				String queryF = String.format(QUERY_TIPO_TC,cuenta,contrato);
				String resultado=null;
				try {
					pst = conn.prepareStatement(queryF);
					rs = pst.executeQuery();
					EIGlobal.mensajePorTrace("***Query: "+queryF, EIGlobal.NivelLog.DEBUG);
					if (rs.next()) {
						resultado=rs.getString("TIPO_RELAC_CTAS");

					}
					if(resultado!=null && resultado.trim().equalsIgnoreCase("P"))
					{
					    return true;
					}
					else
					{
						return false;
					}


				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(String.format("%s sqlDeterminaTipoCuenta [%s]",LOG_TAG,e.getMessage()),
							EIGlobal.NivelLog.ERROR);
					return false;
				} finally {
					try {
						rs.close();
						pst.close();
					} catch (SQLException ex) {
						EIGlobal.mensajePorTrace(String.format("%s %s sqlDeterminaTipoCuenta {finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
								EIGlobal.NivelLog.ERROR);
					}
				}


			}
	public String[] consultaDetalleCuenta(String contrato, String cuenta){

//EIGlobal.mensajePorTrace("**************************2*****consultaDetalleCuente*********************", EIGlobal.NivelLog.ERROR);

		ResultSet rs = null;
		PreparedStatement pst = null;
		String queryF = String.format(QUERY_BUSCA_NUCL,cuenta,contrato);
		String[]resultado=null;
		try {
			pst = conn.prepareStatement(queryF);
			rs = pst.executeQuery();
			EIGlobal.mensajePorTrace("***Query: "+queryF, EIGlobal.NivelLog.DEBUG);
			if (rs.next()) {
				resultado=new String[4];
				resultado[0]=rs.getString("N_TIPO_CUENTA");
				resultado[1]=rs.getString(REQ_ATT_N_PER_JUR);
				resultado[2]=rs.getString("N_PRODUCT");
				resultado[3]=rs.getString("CVE_PROD_ENL");
			}
			return resultado;
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s sqlDeterminaTipoCuenta [%s]",LOG_TAG,e.getMessage()),
					EIGlobal.NivelLog.ERROR);
			return resultado;
		} finally {
			try {
				rs.close();
				pst.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(String.format("%s %s sqlDeterminaTipoCuenta {finally} [%s]",LOG_TAG,ERROR_RS,ex.getMessage()),
						EIGlobal.NivelLog.ERROR);
			}
		}

	}



}