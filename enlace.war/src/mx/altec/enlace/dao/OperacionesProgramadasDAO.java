package mx.altec.enlace.dao;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.lang.CharSet;

import mx.altec.enlace.beans.OperacionesProgramadasBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
/**
 * Clase encargada de Acceso a Datos para la tabla TCT_PROGRAMACION.
 * @author FSW-Indra
 * @sice 12/02/2015
 *
 */
public class OperacionesProgramadasDAO extends GenericDAO {
	
	/**
	 * Constante para el carater ,.
	 */
	private static final String COMA = ",";
	/**
	 * Constante para el carater '.
	 */
	private static final String TILDE = "'";
	/**
	 * Constante para la cadena CHAR.
	 */
	private static final String TIPO_DATO_CHAR = "CHAR";
	/**
	 * Constante para la cadena DATE.
	 */
	private static final String TIPO_DATO_DATE = "DATE";
	/**
	 * Constante para el carater 0.
	 */
	private static final String CERO = "0";
	/**
	 * Constante para la cadena EXITO.
	 */
	private static final String EXITO = "EXITO";
	/**
	 * Constante para la cadena ERROR
	 */
	private static final String ERROR = "ERROR";
	
	
	/**
	 * Metodo para realiza el Alta de la operacion programadas de la tabla TCT_PROGRAMACION.
	 * @since 12/02/2015
	 * @author FSW-Indra
	 * @param beanEntrada de tipo OperacionesProgramadasBean
	 * @return regresa String con el resultado de la operacion
	 */
public String altaOperacionProgramada(OperacionesProgramadasBean beanEntrada){
	
	EIGlobal.mensajePorTrace("Entra al metodo OperacionesProgramadasDAO.altaOperacionProgramada", EIGlobal.NivelLog.INFO);
	StringBuffer queryAlta = new StringBuffer();
	Connection conn = null;
	Statement stmt = null;
	
	
	queryAlta.append("INSERT INTO TCT_PROGRAMACION VALUES (");
	queryAlta.append(getValoresAlta(beanEntrada));
	queryAlta.append(") ");
	
	EIGlobal.mensajePorTrace("OperacionesProgramadasDAO.altaOperacionProgramada-Query[" +
			queryAlta.toString() + 
			"]", NivelLog.INFO);
	try {
		conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
		stmt = conn.createStatement();
		EIGlobal.mensajePorTrace("_a__ Antes Encoding", NivelLog.INFO);
		stmt.executeUpdate(new String(queryAlta.toString().getBytes("ISO-8859-1"), "UTF-8"));
		if (stmt.getUpdateCount() > 0){
			return EXITO;
		}		
	} catch (SQLException e) {
		EIGlobal.mensajePorTrace(new Formateador().formatea(e),
				NivelLog.INFO);
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		try {
			stmt.close();
			conn.close();
		} catch (SQLException e1) {
			EIGlobal.mensajePorTrace(
					"OperacionesProgramadasDAO.altaOperacionProgramada-" + e1.getMessage() + e1 + "]",
					NivelLog.ERROR);
		}
	}

	return ERROR;
}

/**
 * Metodo para generar los VALUES de la sentencia INSERT  a la tabla TCT_PROGRAMACION.
 * @since 11/02/2015
 * @author FSW-Indra
 * @param beanEntrada de tipo getCuenta2
 * @return regresa la cadena VALUES para el Alta en la TCT_PROGRAMACION
 */
private StringBuffer getValoresAlta(OperacionesProgramadasBean beanEntrada){
	StringBuffer queryValues = new StringBuffer().append("(select max(referencia) + 1 from TCT_PROGRAMACION),");
		
	if(beanEntrada.getFechaAplicacion() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getFechaAplicacion(), TIPO_DATO_DATE));
	}
	if(beanEntrada.getReferenciaBit() != null){
		queryValues.append(beanEntrada.getReferenciaBit()).append(COMA);
	}else{
		queryValues.append(agregaValorCero());
	}
	if(beanEntrada.getTipoOperacion() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getTipoOperacion(), TIPO_DATO_CHAR));
	}
	if(beanEntrada.getNumCuenta() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getNumCuenta(), TIPO_DATO_CHAR));
	}
	if(beanEntrada.getUsuario() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getUsuario(), TIPO_DATO_CHAR));
	}
	if(beanEntrada.getCuenta1() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getCuenta1(), TIPO_DATO_CHAR));
	}else{
		queryValues.append(agregaValorCero());
	}
	if(beanEntrada.getCuenta2() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getCuenta2(), TIPO_DATO_CHAR));
	}else{
		queryValues.append(agregaValorCero());
	}
	if(beanEntrada.getTipoCtaOrigen() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getTipoCtaOrigen(), TIPO_DATO_CHAR));
	}
	if(beanEntrada.getTipoCtaDestino() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getTipoCtaDestino(), TIPO_DATO_CHAR));
	}
	if(beanEntrada.getImporte() != null){
		queryValues.append(beanEntrada.getImporte()).append(COMA);
	}else{
		queryValues.append(agregaValorCero());
	}
	if(beanEntrada.getTitulos() != null){
		queryValues.append(beanEntrada.getTitulos()).append(COMA);
	}else{
		queryValues.append(agregaValorCero());
	}
	if(beanEntrada.getCvePerfil() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getCvePerfil(), TIPO_DATO_CHAR));
	}
	if(beanEntrada.getConcepto() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getConcepto() + "|" + beanEntrada.getDescripcionCtaOrigen() + "|" + beanEntrada.getCveBancoCtaDestino() + "|" + beanEntrada.getPlazaCtaDestino() + "|" + beanEntrada.getSucursalCtaDestino()+ "|" + beanEntrada.getRFC()+ "|" + beanEntrada.getIva()+"|"+beanEntrada.getDivisa()+"|"+beanEntrada.getDireccionIP()+"|"+beanEntrada.getCveOperacionSPID(), TIPO_DATO_CHAR));
	}else{
		queryValues.append("PROGRAMADA INTERBANCARIA").append(COMA);
	}
	if(beanEntrada.getFchRegistro() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getFchRegistro(), TIPO_DATO_DATE));
	}
	if(beanEntrada.getFormaAplicacion() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getFormaAplicacion(), TIPO_DATO_CHAR));
	}
	if(beanEntrada.getDescripcionCtaDestino() != null){
		queryValues.append(completaCadenaValoresAlta(beanEntrada.getDescripcionCtaDestino(), TIPO_DATO_CHAR));
	}
//	finValores = completaCadenaValoresAlta(beanEntrada.getRefInterb(), TIPO_DATO_CHAR);
	if(beanEntrada.getRefInterb() != null && "".equals(beanEntrada.getRefInterb().trim())){
		queryValues.append("null");
	}else{
		queryValues.append(beanEntrada.getRefInterb());
	}
	
	

	return queryValues;
}

/**
 * Metodo para generar la cadena del Insert a tct_programacion.
 * @since 11/02/2015
 * @author FSW-Indra
 * @param valorInsert de tipo String
 * @param tipoValor define si es cadena o fecha
 * @return regresa la cadena con el formato de Insert 
 */
private String completaCadenaValoresAlta(String valorInsert, String tipoValor) {
	StringBuffer cadenaValorInsert = new StringBuffer();

	if("CHAR".equals(tipoValor)){
		cadenaValorInsert.append(TILDE);
		cadenaValorInsert.append(valorInsert);
		cadenaValorInsert.append(TILDE);
	} else if("DATE".equals(tipoValor)){
		cadenaValorInsert.append("TO_DATE('");
		cadenaValorInsert.append(valorInsert);
		cadenaValorInsert.append("','dd/mm/yyyy')");
	}
	
	cadenaValorInsert.append(COMA);
	return cadenaValorInsert.toString();	
	
}

/**
 * Metodo para agregar valor por defecto 0 seguido de coma.
 * @since 12/02/2015
 * @author FSW-Indra
 * @return cadenaVacia de tipo StringBuffer.
 */
private StringBuffer agregaValorCero(){
	StringBuffer cadenaVacia = new StringBuffer();
		cadenaVacia.append(CERO);
		cadenaVacia.append(COMA);
	return cadenaVacia;
}
	


}