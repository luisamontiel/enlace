package mx.altec.enlace.dao;

import mx.altec.enlace.beans.NomPreLM1A;
import mx.altec.enlace.beans.NomPreLM1C;
import static mx.altec.enlace.utilerias.NomPreUtil.*;
import static mx.altec.enlace.beans.NomPreRemesa.LNG_NO_REMESA;

public class NomPreRemesaDAO extends NomPreMQDAO {
		
	private static final String CADENA_TARJETAS_VACIO = rellenar(" ", 1150);
	
	//TRANS: LM1A
	public NomPreLM1A consultarRemesas(String noContrato, String noRemesa,
			String estadoRemesa, String paginacion) {	
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(noContrato, 11)) 			//NUM-CONTRATO							
			.append(rellenar(noRemesa, LNG_NO_REMESA))	//NUM-REMESA
			.append(rellenar(estadoRemesa,1))			//COD-ESTADO-REM
			.append(rellenar(paginacion, 20));			//NUM-REM-PAG (PAG) 
		
		logInfo("LM1A - Consulta de remesa");
		
		return consulta(NomPreLM1A.HEADER, sb.toString(), NomPreLM1A.getFactoryInstance());		
	}
	
	//TRANS: LM1C
	public NomPreLM1C aprobarRemesa(String noContrato, String noRemesa) {
		//AR		
		
		StringBuffer sb = new StringBuffer()
			.append("AR")								//COD-ACCION
			.append(rellenar(noContrato, 11))			//NUM-CONTRATO				
			.append(rellenar(noRemesa, LNG_NO_REMESA)) 	//NUM-REMESA
			.append(CADENA_TARJETAS_VACIO);				//NUM-TARJETAS-1/10									
		
		logInfo("LM1C/AR - Aprobar remesa");
		
		return consulta(NomPreLM1C.HEADER, sb.toString(), NomPreLM1C.getFactoryInstance());
	}
	
	//TRANS: LM1C
	public NomPreLM1C rechazarRemesa(String noContrato, String noRemesa) {
		//RR
		
		StringBuffer sb = new StringBuffer()
			.append("RR")									//COD-ACCION
			.append(rellenar(noContrato, 11))			//NUM-CONTRATO				
			.append(rellenar(noRemesa, LNG_NO_REMESA)) 	//NUM-REMESA
			.append(CADENA_TARJETAS_VACIO);				//NUM-TARJETAS-1/10											
		
		logInfo("LM1C/RR - Rechazar remesa");
		
		return consulta(NomPreLM1C.HEADER, sb.toString(), NomPreLM1C.getFactoryInstance());
	}
}
