package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.TR_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.TR_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.TR_JNDI_QUEUE_RESPONSE;
//import mx.altec.enlace.utilerias.Util;
import mx.altec.enlace.servlets.BaseServlet;
import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.naming.NamingException;

import mx.altec.enlace.beans.InterbancariasRecibidasBean;
import mx.altec.enlace.beans.InternacRecibidasBean;
import mx.altec.enlace.bo.InterbancariasRecibidasBO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.utilerias.EIGlobal;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author Vector
 *
 */
public class InternacRecibidasDAO {

	/** El objeto LITERAL_TRAN. */
	private static final String LITERAL_TRAN = "PROCESAESTATUSRECEP";

	/** El objeto LITERAL_JSON. */
	private static final String LITERAL_TIPO = "JSON";

	/** El objeto  COMA. */
	private static final String COMA = ",";

	/** El objeto  COMA SIMPLE. */
	private static final String COMA_SIMPLE = "'";

	/** El objeto  SIGNO IGUAL. */
	private static final String SIGNO_IGUAL = "=";

	/**
	 * Consultar recibidas.
	 *
	 * @param trama the trama
	 * @return the internac recibidas bean
	 */
	public InternacRecibidasBean consultarRecibidas(String fecha, String cuenta, String referencia) {
		InternacRecibidasBean resultado = null;

		MQQueueSession mQ = null;
		String respuestaMq = "";
		StringBuilder tramaEntrada = new StringBuilder();
		EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: consultarRecibidas :: Inicia armado de trama", EIGlobal.NivelLog.INFO);
		tramaEntrada.append("{")
		.append("SERVICIO")
		.append(SIGNO_IGUAL)
		.append(COMA_SIMPLE)
		.append(InterbancariasRecibidasBO.rellenar(LITERAL_TRAN, 19, ' ', 'D'))
		.append(COMA_SIMPLE)
		.append(COMA)
		.append("TIPO_RESP")
		.append(SIGNO_IGUAL)
		.append(COMA_SIMPLE)
		.append(InterbancariasRecibidasBO.rellenar(LITERAL_TIPO, 4, ' ', 'D'))
		.append(COMA_SIMPLE)
		.append(COMA)
		.append("FCH_CAPTURA")
		.append(SIGNO_IGUAL)
		.append(COMA_SIMPLE)
		.append(InterbancariasRecibidasBO.rellenar(fecha, 10, ' ', 'D'))
		.append(COMA_SIMPLE)
		.append(COMA)
		.append("CUENTA")
		.append(SIGNO_IGUAL)
		.append(COMA_SIMPLE)
		.append(InterbancariasRecibidasBO.rellenar(cuenta, 35, ' ', 'D'))
		.append(COMA_SIMPLE)
		.append(COMA)
		.append("REFERENCIA")
		.append(SIGNO_IGUAL)
		.append(COMA_SIMPLE)
		.append(InterbancariasRecibidasBO.rellenar(referencia, 13, ' ', 'D'))
		.append(COMA_SIMPLE)
		.append("}");

		EIGlobal.mensajePorTrace(
				"***InternacRecibidasDAO :: consultarRecibidas :: TRAMA ENTRADA <"
						+ tramaEntrada.toString() + ">",
				EIGlobal.NivelLog.INFO);

		if (validaConexion()){
			try {
				mQ = new MQQueueSession(TR_JNDI_CONECTION_FACTORY,
				TR_JNDI_QUEUE_REQUEST , TR_JNDI_QUEUE_RESPONSE );
			}catch (NamingException e) {
				EIGlobal.mensajePorTrace(
						"***InternacRecibidasDAO :: consultarRecibidas :: NamingException: "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}catch (JMSException e) {
				EIGlobal.mensajePorTrace(
						"***InternacRecibidasDAO :: consultarRecibidas :: JMSException: "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}
		} else {
			resultado = new InternacRecibidasBean();
			resultado.setCodRespuesta("SIF");
			return resultado;
		}

		try {
			respuestaMq = mQ.enviaRecibeMensaje(tramaEntrada.toString());

			EIGlobal.mensajePorTrace(
					"***InternacRecibidasDAO :: consultarRecibidas :: TRAMA SALIDA <"
							+ respuestaMq + ">", EIGlobal.NivelLog.INFO);

			resultado = new InternacRecibidasBean();
			List<InternacRecibidasBean> lstResultado = new ArrayList<InternacRecibidasBean>();

			if (respuestaMq != null && respuestaMq.length() > 0) {
				EIGlobal.mensajePorTrace(
						"***InternacRecibidasDAO :: consultarRecibidas :: OBTIENE DATOS ",
						EIGlobal.NivelLog.INFO);

				lstResultado.add(desentrama(respuestaMq));
				resultado.setLstTransferRecibidas(lstResultado);

			} else {
				EIGlobal.mensajePorTrace(
						"***InternacRecibidasDAO :: consultarRecibidas :: REGRESA VACIA ",
						EIGlobal.NivelLog.INFO);
				resultado.setCodRespuesta("NOK");
			}

		} catch (MQQueueSessionException e) {
			EIGlobal.mensajePorTrace(
					"***InternacRecibidasDAO :: consultarRecibidas :: MQQueueSessionException: "
							+ e.getMessage(), EIGlobal.NivelLog.INFO);
		} finally {
			if (mQ != null) {
				mQ.close();
			}
		}

		return resultado;
	}

	/**
	 * Desentrama.
	 *
	 * @param tramaMq the trama mq
	 * @return the internac recibidas bean
	 */
	private InternacRecibidasBean desentrama(String tramaMq){
		EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: consultarRecibidas :: desentrama :: Inicio ", EIGlobal.NivelLog.INFO);
		InternacRecibidasBean consulta = null;

		consulta = new InternacRecibidasBean();
		List<InternacRecibidasBean> lstConsulta = new ArrayList<InternacRecibidasBean>();
		BaseServlet bs = new BaseServlet();
			if(tramaMq != null || !"".equals(tramaMq)){
				JSONObject tramaJson;
				try{
					tramaJson = (JSONObject)new JSONParser().parse(tramaMq);
					consulta.setCodRespuesta(tramaJson.get("CODIGO_RES").toString().substring(0, 2));
					String codigoRes = tramaJson.get("CODIGO_RES").toString().substring(0, 2);

					EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: consultarRecibidas :: GET COD RESPUESTA <"+consulta.getCodRespuesta()+">", EIGlobal.NivelLog.INFO);

					String cuentaOrd = "";

					if("OK".equals(codigoRes)){
						consulta.setReferencia(tramaJson.get("REFERENCIA").toString());
						consulta.setCuentaDestino(tramaJson.get("NUM_CUENTA_REC").toString());
						consulta.setNomCtaDestino(tramaJson.get("NOMBRE_BEN_REC").toString());
						cuentaOrd = tramaJson.get("NUM_CUENTA_ORD").toString();
						consulta.setNombreOrdenante(tramaJson.get("NOMBRE_ORD").toString());
						consulta.setBancoOrdenante(tramaJson.get("NOMBRE_BANCO_ORD").toString());
						consulta.setPaisOrdenante(tramaJson.get("DESC_PAIS_ORD").toString());
						consulta.setCiudadOrdenante(tramaJson.get("CIUDAD_ORD").toString());
						//consulta.setImporte("$ " + Util.formateaImporte(tramaJson.get("IMPORTE_ABONO").toString()));
						consulta.setImporte(bs.FormatoMoneda(Double.parseDouble(tramaJson.get("IMPORTE_ABONO").toString())));
						consulta.setDivisa(tramaJson.get("CVE_DIVISA_ORD").toString());
						consulta.setTipoCambio(tramaJson.get("TIPO_CAMBIO").toString());
						consulta.setEstatus(tramaJson.get("CVE_ESTATUS").toString());
						consulta.setDescripcionEstatus(tramaJson.get("DESC_ESTATUS").toString());
						consulta.setFechaHora(tramaJson.get("FCH_ENV_CONF").toString());
						consulta.setConceptoPago(tramaJson.get("INF_EMISOR_REC").toString());
						consulta.setDescripcionPais(tramaJson.get("CVE_PAIS_ORD").toString());
						consulta.setCuentaOrdenante(InterbancariasRecibidasBO.enmascararDigito(cuentaOrd, 4));
					}else{
						consulta.setCodRespuesta("NOK");
					}

				}catch(ParseException e){
					EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: desentrama :: Error "
		  					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: consultarRecibidas :: LISTA ", EIGlobal.NivelLog.INFO);
				 lstConsulta.add(consulta);
				 consulta.setLstTransferRecibidas(lstConsulta);
			}

		return consulta;
	}

	/**
	 * Funcion para validar conexion MQ.
	 *
	 * @return boolean resultadoConexion
	 */
	private boolean validaConexion(){
		boolean resultadoConexion = false;
		boolean conexionFactory = false;
		boolean conexionRequest = false;
		boolean conexionResponse = false;

		EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: validaConexion :: TR_JNDI_CONECTION_FACTORY: " + TR_JNDI_CONECTION_FACTORY, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: validaConexion :: TR_JNDI_QUEUE_REQUEST: " + TR_JNDI_QUEUE_REQUEST, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: validaConexion :: TR_JNDI_QUEUE_RESPONSE: " + TR_JNDI_QUEUE_RESPONSE, EIGlobal.NivelLog.INFO);

		conexionFactory = (TR_JNDI_CONECTION_FACTORY != null && !"".equals(TR_JNDI_CONECTION_FACTORY)) ? true : false;
		conexionRequest = (TR_JNDI_QUEUE_REQUEST != null && !"".equals(TR_JNDI_QUEUE_REQUEST)) ? true : false;
		conexionResponse = (TR_JNDI_QUEUE_RESPONSE != null && !"".equals(TR_JNDI_QUEUE_RESPONSE)) ? true : false;

		if (conexionFactory && conexionRequest && conexionResponse) resultadoConexion = true;
		EIGlobal.mensajePorTrace("***InternacRecibidasDAO :: validaConexion :: ResultadoConexion: " + resultadoConexion, EIGlobal.NivelLog.INFO);

		return resultadoConexion;
	}
}