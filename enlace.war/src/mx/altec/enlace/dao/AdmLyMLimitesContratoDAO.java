package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmLyMGL24;
import mx.altec.enlace.beans.AdmLyMGL25;

import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class AdmLyMLimitesContratoDAO extends AdmonUsuariosMQDAO {
	
	//TRANS: GL24
	private AdmLyMGL24 ejecutaGL24Base(String opcion, String tipoOperacion, 
			String cveGrupo, String contrato, String monto) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(opcion, 2))					//CVE-GRUPO
			.append(rellenar(tipoOperacion, 1))
			.append(rellenar(cveGrupo, 2))
			.append(rellenar(contrato, 11))
			.append(rellenar(monto, 17));
		return consulta(AdmLyMGL24.HEADER, sb.toString(), AdmLyMGL24.getFactoryInstance());
	}
	
	public AdmLyMGL24 mantenimientoLimitesMontos(String opcion, String tipoOperacion, 
			String cveGrupo, String contrato, String monto) {
		logInfo("GL24 - Mantenimiento de Limites por Contrato");
		return ejecutaGL24Base(opcion, tipoOperacion, cveGrupo, contrato, monto);			
	}

	//TRANS: GL25
	private AdmLyMGL25 ejecutaGL25Base(String tipoOperacion, 
			String cveGrupo, String contrato) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(tipoOperacion, 1))
			.append(rellenar(cveGrupo, 2))
			.append(rellenar(contrato, 11));
		return consulta(AdmLyMGL25.HEADER, sb.toString(), AdmLyMGL25.getFactoryInstance());
	}
	
	public AdmLyMGL25 consultaLimitesMontos(String tipoOperacion, 
			String cveGrupo, String contrato) {
		logInfo("GL25 - Consulta de Limites por Contrato");
		return ejecutaGL25Base(tipoOperacion, cveGrupo, contrato);			
	}

}
