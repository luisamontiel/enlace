package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.sql.DataSource;

import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.servicios.ServiceLocator;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;

public class GenericDAO {

	/**
	 * Crea la conexion a BD
	 * @param dbName : dbName
	 * @return conn : conn
	 * @throws SQLException : Manejo de excepcion
	 */
	public static Connection createiASConnStatic ( String dbName )
    throws SQLException {

    	 Connection conn = null;
    	 ServiceLocator servLocator = ServiceLocator.getInstance();
    	 DataSource ds = null;
    	 try {
    		 ds = servLocator.getDataSource(dbName);
    	 } catch(NamingException e) {
    		 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">, mensaje: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
    	 }
    	 if(ds == null) {
    		 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">",EIGlobal.NivelLog.ERROR);
    		 return null;
    	 }
    	 conn = ds.getConnection ();
    	 return conn;
    }
	
	/**
	 * Se realiza el cierre de conexion
	 * @param conexion : conexion
	 * @throws SQLException : manejo de Excepcion
	 */
	public static void cierraConexion(Connection conexion)	throws SQLException{
		try{
			
			if(conexion!=null){
				conexion.close();
				conexion=null;
		    }
		}catch(SQLException e){		
			throw new SQLException("Error en el Servicio de Datos."+ new Formateador().formatea(e));
		}/*catch(Exception e){		
			throw new SQLException("Error en el Servicio de Datos."+ new Formateador().formatea(e));
		}*/
	}
	
	/**
	 * M�todo generico para la invocaci�n a las trx para los estados de cuenta
	 * @param trama : Trama armada para un servicio especifico
	 * @return respuesta : Respuesta de la trx
	 */
	public String invocarTransaccion(String trama) {
		String respuesta = "";
		StringBuilder tramaFinal = new StringBuilder();
		
		try {
			tramaFinal.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(trama);
			
			EIGlobal.mensajePorTrace("Trama Estados de Cuenta ->" + tramaFinal.toString(), EIGlobal.NivelLog.INFO);
			
			respuesta = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST)
							.enviaRecibeMensaje(tramaFinal.toString());
			
			EIGlobal.mensajePorTrace("Respuesta de Estados de Cuenta ->" + respuesta, EIGlobal.NivelLog.INFO);
			
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		
		return respuesta;
	}
	
	/**
	 * Metodo Generico para crear las cabeceras al invocar alguna transaccion.
	 * 
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param servicio de tipo String
	 * @param espejo de tipo String
	 * @param longitud de tipo String
	 * @param teclaFuncion de tipo String
	 * @return cabecera de tipo String
	 */
	public static String armaCabeceraPS7(String servicio, String espejo, int longitud, String teclaFuncion) {
   	
		StringBuffer cabecera = new StringBuffer();
		
		//cabecera.append("    ");
		//cabecera.append(ADMUSR_MQ_USUARIO);                     // 8  Usuario CICS
		cabecera.append(rellenar(servicio,4,' ','I'));          // 4  C�digo transacci�n
		cabecera.append(rellenar(Integer.toString(longitud+32),4,'0','I'));  // 4  Longitud del mensaje de entrada (cabecera + contenido)
		cabecera.append('1');                                   // 1  1=Altamira gestina el commit, 0=Altamira no gestiona el commit
		cabecera.append("12345");                               // 5  N�mero de secuencia
		cabecera.append('1');                                   // 1  1=incorporar datos 2=autorizaci�n 4=reanudar conversaci�n 5=contin�a conversaci�n 6=autorizaci�n de transacci�n en conversaci�n
		cabecera.append('O');                                   // 1  O=on line F=off line
		cabecera.append(rellenar(teclaFuncion,2,'0','I'));      // 2  00=enter 01..12=Fnn 13..=ShiftF1..F24 99=Clear
		cabecera.append('N');                                   // 1  N=no impresora S=s� impresora
		cabecera.append('2');                                   // 1  2=formato @DC 1=mapas @PA y@LI

   		return cabecera.toString();
   }


}
