/**
 * 
 */
package mx.altec.enlace.dao;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Berenice Mesquitillo
 * Fecha: 26/junio/2009
 * proyecto: 200813700 Nomina Prepago
 * Descripción: DAO para el uso de estados para la consulta y actualización de datos demográficos
 */
public class NomPreEstadosDAO {
	
	private static final Map<String, String> mapEstados=new LinkedHashMap<String, String>();
	
	static{
	
		mapEstados.put("AGSC", "AGUASCALIENTES");
		mapEstados.put("BCN", "BAJA CALIFORNIA NORTE");
		mapEstados.put("BCS", "BAJA CALIFORNIA SUR");
		mapEstados.put("CAMP", "CAMPECHE");
		mapEstados.put("CHIS", "CHIAPAS");
		mapEstados.put("CHIH", "CHIHUAHUA");		
		mapEstados.put("COAH", "COAHUILA");
		mapEstados.put("COL", "COLIMA");
		mapEstados.put("DF", "DISTRITO FEDERAL");
		mapEstados.put("DGO", "DURANGO");
		mapEstados.put("MEX", "ESTADO DE MEXICO");
		mapEstados.put("GTO", "GUANAJUATO");		
		mapEstados.put("GRO", "GUERRERO");
		mapEstados.put("HGO", "HIDALGO");
		mapEstados.put("JAL", "JALISCO");		
		mapEstados.put("MICH", "MICHOACAN");
		mapEstados.put("MOR", "MORELOS");
		mapEstados.put("NAY", "NAYARIT");
		mapEstados.put("NL", "NUEVO LEON");
		mapEstados.put("OAX", "OAXACA");
		mapEstados.put("PUE", "PUEBLA");
		mapEstados.put("QRO", "QUERETARO");
		mapEstados.put("QROO", "QUINTANA ROO");
		mapEstados.put("SLP", "SAN LUIS POTOSI");
		mapEstados.put("SIN", "SINALOA");		
		mapEstados.put("SON", "SONORA");
		mapEstados.put("TAB", "TABASCO");
		mapEstados.put("TAMPS", "TAMAULIPAS");
		mapEstados.put("TLAX", "TLAXCALA");
		mapEstados.put("VER", "VERACRUZ");
		mapEstados.put("YUC", "YUCATAN");
		mapEstados.put("ZAC", "ZACATECAS");
		
	}
	
	public static Map<String,String> consultaEstados(){
		
		return NomPreEstadosDAO.mapEstados;
	}	
}
