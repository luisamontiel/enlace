package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.Global.NP_MQ_USUARIO;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.SUALCORM4;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

public class SUAConsultaPagoDAO {



	public static final String HEADERORM4="ORM401431123451O00N2";

	public static final String LC =
		"                                                     ";
	//variables para respuesta exito y error
	private static final String CODIGO_ERROR="@ER";
	private static final String CODIGO_EXITO="@11";
	/*** Copy para transacion ORM4 Salida****/
	private static final String COPYNCM4S="@DCORNCM4S P";

	/**
	 *  Metodo para la obtencion de los datos de consulta de SUALC
	 * @param ctaCargo
	 * @param folioSua
	 * @param fechaIni
	 * @param fechaFin
	 * @param regPatronal
	 * @param opcion
	 * @return SUALCORM4
	 */
	public SUALCORM4 consultaSUALC(String ctaCargo,String folioSua,String fechaIni,String  fechaFin,String regPatronal,String lc,int opcion){
		StringBuffer mensajeMQ= new StringBuffer();
		mensajeMQ.append(rellenar(NP_MQ_TERMINAL, 4))
			.append(rellenar(NP_MQ_USUARIO, 8))
			.append(HEADERORM4);
		//cuenta cargo
		if(opcion==1 || opcion==3){
			mensajeMQ.append(rellenar(ctaCargo, 20));
		}else{
			mensajeMQ.append("                    ");
		}
		//Folio SUA
		if(opcion==2){
			mensajeMQ.append(rellenar(folioSua,6));
		}else{
			mensajeMQ.append("      ");
		}
		//fecha Inicio -fechaFin
		if(opcion==3){
			mensajeMQ.append(rellenar(formateaFecha(fechaIni,1),10));
			mensajeMQ.append(rellenar(formateaFecha(fechaFin,1),10));
		}else{
			mensajeMQ.append("          ");
			mensajeMQ.append("          ");
		}
		//registro Patronal
		if(opcion==4){
			mensajeMQ.append(rellenar(regPatronal,11));
		}else{
			mensajeMQ.append("           ");
		}
		//agregamos la opcion de la busqueda
		mensajeMQ.append(opcion);

		if(lc!=null && lc.length()>0){
			mensajeMQ.append(lc);
		}else{
			mensajeMQ.append(LC);
		}

		String respuesta=null;
		String strError;

		EIGlobal.mensajePorTrace("Trama consulta SUALC: [" +mensajeMQ.toString()+"]",EIGlobal.NivelLog.INFO);
		MQQueueSession mqTux = null;
		try {
			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE,
					NP_JNDI_QUEUE_REQUEST);
			respuesta = mqTux.enviaRecibeMensaje(mensajeMQ.toString());

			EIGlobal.mensajePorTrace("RespuestaMQConsultaSUALC: [" + respuesta+"]",EIGlobal.NivelLog.INFO);

		}catch(Exception e){
			strError = e.getMessage();
			EIGlobal.mensajePorTrace ( "ERROR::MQ::" + strError + ":" , EIGlobal.NivelLog.ERROR);
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}
		return  desentramaConsultaORM4(respuesta) ;
	}
	/**
	 * @author lespinosa <VC> 01-09-2009
	 * Desentramado para ORM4 Consulta Pagos LC SUA
	 *
	 * @param trama
	 * @return SUALCORM4
	 */
	private SUALCORM4 desentramaConsultaORM4(String trama) {
		SUALCORM4 orm4= new SUALCORM4();
		if(trama==null){
			orm4.setMsjErr("Ocurrio un error al realizar la conexion a MQ");
		}
		//Validamos que la respuesta sea correcta
		if(trama!=null && trama.contains(CODIGO_EXITO)){
			//buscamos la informacion de respuesta
			if(trama.contains(COPYNCM4S)){
				//desentrama copy CAM
				String copys[] = trama.substring(trama.indexOf(COPYNCM4S)+12).split(COPYNCM4S);
				List<SUALCORM4>  consulta= new ArrayList<SUALCORM4>();
				for(String copyMCam:copys){
					SUALCORM4 orm4interno= new SUALCORM4();
					orm4interno.setCuentaCargo(copyMCam.substring(0,20));  //Cuenta Cargo   0-20 (20)
					orm4interno.setRegPatronal(copyMCam.substring(20,31)); //Reg patronal 20-31 (11)
					orm4interno.setFolioSua(copyMCam.substring(31,37));  //fol SUA 31-37 (6)
					orm4interno.setPeriodoPago(copyMCam.substring(37,43));//periodo    37-43 (6)
					orm4interno.setLinSua(copyMCam.substring(43,96)); //LC    43-(96)(53)
					orm4interno.setImporteTotal(copyMCam.substring(96,112)); //importe   96-112(16)
					orm4interno.setNumeroMovimiento(copyMCam.substring(112,121));//numMov        112-121(9)
					orm4interno.setUsuarioRegPago(copyMCam.substring(121,129)); //userReg        121-129 (8)
					orm4interno.setTimestamp(copyMCam.substring(129,155));//timestamp  129-155(25)
					consulta.add(orm4interno);
				}
				if(trama.contains("@AVORA0003 FIN DE DATOS")){
					//Son unicos datos
					orm4.setPagina(false);
				}else{
					if(orm4.getConsulta().size()>0){
						orm4.setPagina(true);
					}
					//Paginacion
				}
				orm4.setConsulta(consulta);
				orm4.setMsjErr("Operacion Exitosa");
				orm4.setCodErr("Operacion Exitosa");
			}else{
				orm4.setMsjErr("No se recuperaron datos");
				orm4.setCodErr("No se recuperaron datos");
			}
		}else if (trama!=null && trama.contains(CODIGO_ERROR)){
			//buscar el codigo de error
			if (trama.indexOf(CODIGO_ERROR) != -1) {
				//Validar si se envia el mensaje o codigo o ambos
				int index = trama.indexOf(CODIGO_ERROR) + 3;

				int indexfinCE=trama.length()-2;
				String msjErr=trama.substring(index + 8,indexfinCE);
				if(msjErr.contains("@")){
					msjErr = msjErr.substring(0,msjErr.indexOf("@")-2);
				}
				orm4.setMsjErr(msjErr);
				orm4.setCodErr(trama.substring( index, index + 8));
				orm4.setCuentaCargo("");  //Cuenta Cargo   0-20 (20)
				orm4.setRegPatronal(""); //Reg patronal 20-31 (11)
				orm4.setFolioSua("");  //fol SUA 31-37 (6)
				orm4.setPeriodoPago("");//periodo    37-43 (6)
				orm4.setLinSua(""); //LC    43-(96)(53)
				orm4.setImporteTotal(""); //importe   96-112(16)
				orm4.setNumeroMovimiento("");//numMov        112-121(9)
				orm4.setUsuarioRegPago(""); //userReg        121-129 (8)
				orm4.setTimestamp("");//timestamp  129-155(25)
			}
		}
		return orm4;

	}




	/**
	 * funcion para formateo de fecha
	 * @param fechaGiro
	 * @return String
	 */
	public String formateaFecha(String fechaGiro,int orden) {
		if(fechaGiro!=null && fechaGiro.contains("/")){
			String fecha[]=fechaGiro.split("/");
			if(fecha.length==3){
				if(orden==1){
					return (fecha[2]+"-"+fecha[1]+"-"+fecha[0]);
				}else if(orden==2){
					return (fecha[0]+"-"+fecha[1]+"-"+fecha[2]);
				}else if(orden==3){
					return (fecha[2]+fecha[1]+fecha[0]);
				}else{
					return (fecha[0]+fecha[1]+fecha[2]);
				}
			}else{
				return fechaGiro;
			}
		}
		return fechaGiro;
	}

}