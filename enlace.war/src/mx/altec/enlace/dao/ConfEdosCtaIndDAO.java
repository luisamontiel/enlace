package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

/**
 * @author Stefanini
 *
 */
public class ConfEdosCtaIndDAO extends GenericDAO {
	
	/** Constante para encabezado de PS7 para la trx ODD4 */
	public static final String HEADER_ODD4 = "ODD403341123451O00N2";
	/** Constante para encabezado de PS7 para la trx DY01 */
	public static final String HEADER_DY01 = "DY0101271123451O00N2";
	/** Constante para encabezado de PS7 para la trx OW42 */
	public static final String HEADER_OW42 = "OW4200671123451O00N2";
	/** Constante para encabezado de PS7 para la trx ODB2 */
	public static final String HEADER_ODB2 = "ODB200461123451O00N2";
	/** Constante para canal de enlace */
	public static final String CANAL = "05";
	/** Constante para entidad */
	public static final String ENTIDAD = "0014";
	/** Constante para tipo de cuenta */
	public static final String TIPO_CTA = "001";
	/** Constante para mensaje de query */
	private static final String QUERY = " query: [";
	/** Constante para mensaje de error al cerrar conexion */
	private static final String ERROR_CONEXION = "Error al cerrar conexion: [";
	/** Constante para coma y espacio en querys */
	private static final String COMA_SPACE = "', '";
	/** LOG TAG**/
	private static final String LOG_TAG = "[EDCPDFXML] ::: ConfEdosCtaIndDAO ::: ";
	/**
	 * Sentencia insert para la tabla de detalle (PYME 03/03/2015 FSW Indra)
	 */
	private static final String INSERTCMP1 = new StringBuilder(
			"insert into EWEB_EDO_CTA_DET (ID_EDO_CTA_DET, ID_EDO_CTA_CTRL, NUM_CUENTA, ")
			.append("ESTATUS, DESC_ERROR, PPLS, COD_CLTE, TIPO_EDO_CTA")
			.toString();
	/**
	 * Metodo para consultar si ya existe una configuracion el mismo dia de PDF
	 * 
	 * @param seqDomicilio : Secuencia de domicilio a consultar
	 * @param codCliente : Codigo de cliente a consultar
	 * @return Date : Fecha de la configuracion previa.
	 */
	public Date consultaConfigPreviaSeqDom(String seqDomicilio,
			String codCliente) {
		//StringBuilder query = new StringBuilder();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Date fechaConfig = null;
		String query=String.format("SELECT A.FCH_ENVIO, A.ID_EDO_CTA_CTRL, A.NUM_CUENTA2" +
				" FROM EWEB_EDO_CTA_CTRL A" +
				" INNER JOIN EWEB_EDO_CTA_DET B ON A.ID_EDO_CTA_CTRL = B.ID_EDO_CTA_CTRL" +
				" WHERE B.NUM_SEC = '%s'" +
				" AND B.COD_CLTE = '%s'" +
				" AND A.ESTATUS = 'P' AND B.ESTATUS = 'A'" +
				" AND A.FCH_ENVIO = TRUNC(SYSDATE)",seqDomicilio,codCliente);	
		/*query.append("select A.FCH_ENVIO from EWEB_EDO_CTA_CTRL A ")
			.append("inner join EWEB_EDO_CTA_DET B on A.ID_EDO_CTA_CTRL=B.ID_EDO_CTA_CTRL ")
			.append("where B.NUM_SEC='").append(seqDomicilio)
			.append("' and B.COD_CLTE='").append(codCliente)
				.append("' and A.ESTATUS='P' ")
			.append(" and trunc(A.FCH_ENVIO)=to_date(SYSDATE, 'dd/MM/yy')");*/
		EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - consultaConfigPreviaSeqDom -" + QUERY
						+ query + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format("%s consultaConfigPreviaSeqDom - QRY [%s]",LOG_TAG,query),NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if(rs.next()) {
				EIGlobal.mensajePorTrace("setFechaConfig ->" + rs.getDate(1),
						NivelLog.DEBUG);
				fechaConfig = rs.getDate(1);
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		}finally {
			try{
				rs.close();
				stmt.close();
				conn.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - consultaConfigPreviaSeqDom() - "+ERROR_CONEXION + e1 + "]",
						NivelLog.ERROR);
			}
		}
		return fechaConfig;
	}
	
	/**
	 * Metodo para consultar el folio de operacion
	 * 
	 * @return int : folio
	 */
	public int consultaFolioSeq() {
		String query = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int folio = 0;
		query = "select SEQ_EDOCTA_PDFXML.nextval from dual";
		EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - consultaFolioSeq -"
				+ QUERY + query + "]", NivelLog.INFO);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if(rs.next()) {
				folio = rs.getInt(1);
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		}finally {
			try{
				stmt.close();
				conn.close();
				rs.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
						"ConfEdosCtaIndDAO - consultaFolioSeq() - "
								+ ERROR_CONEXION + e1 + "]",
						NivelLog.ERROR);
			}
		}
		return folio;
	}
	
	/**
	 * Metodo para insertar la nueva configuacin elegida
	 * 
	 * @param confEdosCtaIndBean : Bean de la informacion de entrada
	 */
	public void insertaConfiguracion(ConfEdosCtaIndBean confEdosCtaIndBean) {
		StringBuilder query = new StringBuilder();
		Connection conn = null;
		Statement stmt = null;
		query.append(
				"insert into EWEB_EDO_CTA_CTRL (ID_EDO_CTA_CTRL, FCH_ENVIO, ESTATUS, ID_USUARIO, NUM_CUENTA2) values (")
				.append(confEdosCtaIndBean.getFolioOp())
				.append(", TRUNC(SYSDATE), 'P', '")
				.append(confEdosCtaIndBean.getCodCliente()).append(COMA_SPACE)
				.append(confEdosCtaIndBean.getContrato()).append("')");
		
		EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - insertaConfiguracion -"
				+ QUERY + query + "]", NivelLog.INFO);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			stmt.executeUpdate(query.toString());
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		} finally {
			try {
				stmt.close();
				conn.close();
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace(
						"ConfEdosCtaIndDAO - insertaConfiguracion() - "
								+ ERROR_CONEXION + e1 + "]",
						NivelLog.ERROR);
			}
		}
	}

	/**
	 * Metodo para insertar la nueva configuacin elegida
	 * Se modifica Metodo para tomar encuenta cuando se configura paperles para
	 * una TDC PYME 03/03/2015 FSW Indra 
	 * @param confEdosCtaIndBean : Bean de la informacion de entrada
	 * @param edoCtaDisponible : Estatus de estado de cuenta disponible
	 * @param suscripPaperless : Estatus de suscripcion a paperless
	 */
	public void insertaDetalle(ConfEdosCtaIndBean confEdosCtaIndBean,
			String edoCtaDisponible, String suscripPaperless) {		
		StringBuilder insertCmp = new StringBuilder();
		StringBuilder insertValues = new StringBuilder("values (1, ").append(confEdosCtaIndBean.getFolioOp())
			.append(", '").append(confEdosCtaIndBean.getCuenta())			
			.append("', 'A', '").append(edoCtaDisponible)
			.append(COMA_SPACE).append(suscripPaperless)
			.append("', '")
			.append(confEdosCtaIndBean.getCodClientePersonas())
			.append("'");		
		insertCmp.append(INSERTCMP1);
		if( confEdosCtaIndBean.getUso() != null && "TDC".equals(confEdosCtaIndBean.getUso()) ){
			insertCmp.append(", NUM_CONTR_TDC_FK");
			insertValues.append(", '003', '")
				.append(confEdosCtaIndBean.getContrato())
				.append("'");
		} else {
			insertValues.append(", '001'");
		}
		if(confEdosCtaIndBean.getSecDomicilio() != null) {
			insertCmp.append(", NUM_SEC)");
			insertValues.append(", '")
				.append(confEdosCtaIndBean.getSecDomicilio())
				.append("')");
		} else {
			insertCmp.append(")");
			insertValues.append(")");
		}	
		String insertComplet = new StringBuilder(insertCmp)
				.append(insertValues).toString();
		EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - insertaDetalle -" + QUERY
				+ insertComplet + "]", NivelLog.INFO);
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			stmt.executeUpdate(insertComplet);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		}finally {
			try{
				stmt.close();
				conn.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
						"ConfEdosCtaIndDAO - insertaDetalle() - "
								+ ERROR_CONEXION + e1 + "]",
						NivelLog.ERROR);
			}
		}
	}
	
	/**
	 * Metodo para consultar si ya existe una configuracion el mismo dia para la
	 * cuenta
	 * @param seqDomicilio : seqDomicilio
	 * @param codCliente : codCliente
	 * @return confEdosCtaIndBean : Bean de la informacion
	 */
	public ConfEdosCtaIndBean consultaConfigPreviaEdoCta(String seqDomicilio,String codCliente) {
		//StringBuilder query = new StringBuilder();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		String query=String.format("SELECT A.FCH_ENVIO, A.ID_EDO_CTA_CTRL, A.NUM_CUENTA2" +
				" FROM EWEB_EDO_CTA_CTRL A" +
				" INNER JOIN EWEB_EDO_CTA_DET B ON A.ID_EDO_CTA_CTRL = B.ID_EDO_CTA_CTRL" +
				" WHERE B.NUM_SEC = '%s'" +
				" AND B.COD_CLTE = '%s'" +
				" AND A.ESTATUS = 'P' AND B.ESTATUS = 'A'" +
				" AND A.FCH_ENVIO = TRUNC(SYSDATE)",seqDomicilio,codCliente);	

		/*
		query.append(
				"select A.FCH_ENVIO, A.ID_EDO_CTA_CTRL, A.NUM_CUENTA2 from EWEB_EDO_CTA_CTRL A ")
		.append("inner join EWEB_EDO_CTA_DET B on A.ID_EDO_CTA_CTRL=B.ID_EDO_CTA_CTRL ")
			.append("where B.NUM_CUENTA='").append(cuenta)
			.append("' and B.TIPO_EDO_CTA='001'")
			.append(" and trunc(A.FCH_ENVIO)=to_date(SYSDATE, 'dd/MM/yy')");
		*/
		EIGlobal.mensajePorTrace(String.format("%s consultaConfigPreviaEdoCta - QRY [%s]",LOG_TAG,query),NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if(rs.next()) {
				EIGlobal.mensajePorTrace(
						"Fecha de Config de la cuenta ->" + rs.getDate(1),
						NivelLog.DEBUG);
				confEdosCtaIndBean.setFechaConfig(rs.getDate(1));
				confEdosCtaIndBean.setFolioOp(String.valueOf(rs.getInt(2)));
				confEdosCtaIndBean.setContrato(rs.getString(3));
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		}finally {
			try{
				stmt.close();
				conn.close();
				rs.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
						"ConfEdosCtaIndDAO - consultaConfigPreviaEdoCta() - "
								+ ERROR_CONEXION + e1 + "]",
						NivelLog.ERROR);
			}
		}
		return confEdosCtaIndBean;
	}
	
	/**
	 * Metodo para consultar la descripcion de una cuenta
	 * 
	 * @param cuenta : Cuenta a consular informacion
	 * @return String : Descripcion
	 */
	public String consultarDescCuenta(String cuenta) {
		StringBuilder query = new StringBuilder();
		String descripcion = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		query.append("select N_DESCRIPCION from NUCL_RELAC_CTAS ")
				.append("where NUM_CUENTA='").append(cuenta).append("'");
		EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - obtenerDescCuenta -"
				+ QUERY + query + "]", NivelLog.INFO);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query.toString());
			if(rs.next()) {
				descripcion = rs.getString(1);
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		}finally {
			try{
				stmt.close();
				conn.close();
				rs.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
						"ConfEdosCtaIndDAO - consultarDescCuenta() - "
								+ ERROR_CONEXION + e1 + "]",
						NivelLog.ERROR);
			}
		}
		return descripcion;
	}

	/**
	 * Metodo para ejecutar trx ODD4
	 * 
	 * @param cuenta : Cuenta a consultar
	 * @return ConfEdosCtaIndBean : Bean de la informacion
	 */
	public ConfEdosCtaIndBean ejecutaODD4(String cuenta) {
		StringBuilder tramaODD4 = null;
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		String respuestaODD4 = "";
		int count = 0;
		if(cuenta.length()>1 && "BME".equals(cuenta.substring(0, 3))) {
			cuenta = "B".concat(cuenta.substring(3));
}
		do{
			tramaODD4 = new StringBuilder();
			if(count == 0) {
				tramaODD4.append(HEADER_ODD4).append(rellenar(cuenta,12));
				tramaODD4.append(" ");
				EIGlobal.mensajePorTrace("tramaODD4 Y--->"+tramaODD4+"<--",
						NivelLog.INFO);
				confEdosCtaIndBean.setCuentas(new ArrayList<String>());
			}else {
				tramaODD4
						.append(HEADER_ODD4)
						.append(rellenar(confEdosCtaIndBean.getCuenta(), 12))
						.append('S')
						.append(ENTIDAD)
						.append(rellenar(confEdosCtaIndBean.getOficina(), 4))
						.append(rellenar(confEdosCtaIndBean.getProducto(), 2))
						.append(rellenar(confEdosCtaIndBean.getSubproducto(), 4))
						.append(rellenar(confEdosCtaIndBean.getUso(), 3))
						.append(rellenar(confEdosCtaIndBean.getCodCliente(), 8))
						.append(rellenar(confEdosCtaIndBean.getSecDomicilio(),
								3));
			}
			respuestaODD4 = invocarTransaccion(tramaODD4.toString());
			confEdosCtaIndBean = desentramaODD4(confEdosCtaIndBean, respuestaODD4, count);
			count++;
		}while("PEA8001".equals(confEdosCtaIndBean.getCodRetorno()));
		return confEdosCtaIndBean;
	}
	
	/**
	 * Metodo para ejecutar trx ODB2
	 * 
	 * @param confEdosCtaIndBean : Bean con la informacion de entrada
	 * @param suscripPaperless : Estatus de suscripcion a paperless
	 * @return ConfEdosCtaIndBean : Bean de la informacion
	 */
	public ConfEdosCtaIndBean ejecutaODB2(
			ConfEdosCtaIndBean confEdosCtaIndBean, String suscripPaperless) {
		if ("S".equals(suscripPaperless)) {
			suscripPaperless = "N";
		} else {
			suscripPaperless = "S";
		}
		StringBuilder tramaODB2 = new StringBuilder();
		ConfEdosCtaIndBean beanRes = null;
		String respuestaODB2 = "";
		EIGlobal.mensajePorTrace("EConfEdosCtaIndDAO:ODD4:CodClientePersonas->" + confEdosCtaIndBean.getCodClientePersonas(),NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("EConfEdosCtaIndDAO:ODD4:getSecDomicilio->" + confEdosCtaIndBean.getSecDomicilio(),NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("EConfEdosCtaIndDAO:ODD4:envio a domicilio->" + suscripPaperless,NivelLog.DEBUG);		
		
		tramaODB2.append(HEADER_ODB2)
				.append(rellenar(confEdosCtaIndBean.getCodClientePersonas(), 8))
				.append(rellenar(confEdosCtaIndBean.getSecDomicilio(), 3))
				.append(rellenar(suscripPaperless, 1)).append("04");
		respuestaODB2 = invocarTransaccion(tramaODB2.toString());
		EIGlobal.mensajePorTrace("EConfEdosCtaIndDAO:ODD4:tramaODB2->" + tramaODB2.toString(),NivelLog.DEBUG);
		beanRes = desentramaODB2(respuestaODB2);
		confEdosCtaIndBean.setCodRetorno(beanRes.getCodRetorno());
		confEdosCtaIndBean.setDescRetorno(beanRes.getDescRetorno());
		return confEdosCtaIndBean;
	}

	/**
	 * Metodo para desentramar la trx ODD4
	 * 
	 * @param confEdosCtaIndBean : Bean
	 * @param respuesta : Trama de respuesta
	 * @param count : Numero de rellamado
	 * @return ConfEdosCtaIndBean : Bean de la informacion
	 */
	private ConfEdosCtaIndBean desentramaODD4(ConfEdosCtaIndBean confEdosCtaIndBean, String respuesta, int count) {
		//String tramaCodigo="", tramaDatos="", cuenta=null;
		respuesta = (respuesta!=null) ? respuesta.trim() : "";
		List<String> listCuentas = confEdosCtaIndBean.getCuentas();
		String[] arrayTrama = (respuesta.length() > 0) ? respuesta.split("[@]") : null;
		String tramaCodigo = (arrayTrama != null && arrayTrama.length >= 3) ? arrayTrama[2] : "";
		String tramaDatos = (arrayTrama != null && arrayTrama.length >= 4) ? arrayTrama[3] : ""; 
		/*
		if(respuesta != null) {
			respuesta = respuesta.trim();
			if(respuesta.length() > 0) {
				arrayTrama = respuesta.split("[@]");
			}
			if (arrayTrama != null && arrayTrama.length >= 3) {
				tramaCodigo = arrayTrama[2];
			}
			if (arrayTrama != null && arrayTrama.length >= 4) {
				tramaDatos = arrayTrama[3];
			}
		}
		*/
		EIGlobal.mensajePorTrace(String.format("ConfEdosCtaIndDAO:desentramaODD4 tramaCodigo [%s]\n" +
											   "ConfEdosCtaIndDAO:desentramaODD4 tramaDatos  [%s]",tramaCodigo,tramaDatos), NivelLog.DEBUG);
		if(tramaCodigo.length() >= 9) {
			confEdosCtaIndBean.setCodRetorno(tramaCodigo.substring(2,9).trim());
			confEdosCtaIndBean.setDescRetorno(tramaCodigo.substring(9, tramaCodigo.length()-2).trim());
		}
		if(count == 0 &&tramaDatos.length() >= 313) {
			if (confEdosCtaIndBean.getCodCliente()==null) {
				//EIGlobal.mensajePorTrace("--->Codigo Cliente nulo, se asigna del llamado a ODD4", NivelLog.DEBUG);
				confEdosCtaIndBean.setCodCliente(tramaDatos.substring(41,49).trim());
			}
			if (confEdosCtaIndBean.getSecDomicilio()==null) {
				//EIGlobal.mensajePorTrace("--->Secuencia Domicilio nulo, se asigna del llamado a ODD4", NivelLog.DEBUG);
				confEdosCtaIndBean.setSecDomicilio(tramaDatos.substring(49,52).trim());
			}
			if (confEdosCtaIndBean.getIdPaperless()==null) {
				//EIGlobal.mensajePorTrace("--->Indicador Paperless nulo, se asigna del llamado a ODD4", NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("--->Indicador Paperless nulo, se asigna del llamado a ODD4: [" + tramaDatos.substring(232,233).trim() + "]", NivelLog.DEBUG);
				confEdosCtaIndBean.setIdPaperless(tramaDatos.substring(232,233).trim());
			}						
			confEdosCtaIndBean.setCuenta(tramaDatos.substring(11,23).trim());
			confEdosCtaIndBean.setOficina(tramaDatos.substring(28,32).trim());
			confEdosCtaIndBean.setProducto(tramaDatos.substring(32,34).trim());
			confEdosCtaIndBean.setSubproducto(tramaDatos.substring(34,38).trim());
			confEdosCtaIndBean.setUso(tramaDatos.substring(38,41).trim());
			confEdosCtaIndBean.setCalle(tramaDatos.substring(52,102).trim());
			confEdosCtaIndBean.setNoExterior(tramaDatos.substring(102,117).trim());
			confEdosCtaIndBean.setNoInterior(tramaDatos.substring(117,132).trim());
			confEdosCtaIndBean.setColonia(tramaDatos.substring(132,162).trim());
			confEdosCtaIndBean.setCiudad(tramaDatos.substring(162,192).trim());
			confEdosCtaIndBean.setDelegMunic(tramaDatos.substring(192,222).trim());
			confEdosCtaIndBean.setCodPostal(tramaDatos.substring(222,230).trim());
			confEdosCtaIndBean.setEstado(tramaDatos.substring(230,232).trim());
			confEdosCtaIndBean.setNombre(tramaDatos.substring(233,273).trim());
			confEdosCtaIndBean.setApPaterno(tramaDatos.substring(273,293).trim());
			confEdosCtaIndBean.setApMaterno(tramaDatos.substring(293,313).trim());
			if("S".equals(confEdosCtaIndBean.getIdPaperless())) {
				confEdosCtaIndBean.setSuscripPaperless(false);
			} else {
				confEdosCtaIndBean.setSuscripPaperless(true);
			}
			EIGlobal.mensajePorTrace("--->confEdosCtaIndBean.isSuscripPaperless: [" + confEdosCtaIndBean.isSuscripPaperless() + "]", NivelLog.DEBUG);
		}else if(count >= 0 && tramaDatos.length() >= 52){
			if (confEdosCtaIndBean.getCodCliente()==null) {
				//EIGlobal.mensajePorTrace("--->Codigo Cliente nulo, se asigna del re-llamado a ODD4", NivelLog.DEBUG);
				confEdosCtaIndBean.setCodCliente(tramaDatos.substring(41,49).trim());
			}
			if (confEdosCtaIndBean.getSecDomicilio()==null) {
				confEdosCtaIndBean.setSecDomicilio(tramaDatos.substring(49,52).trim());
			}
			if (confEdosCtaIndBean.getIdPaperless()==null) {
				confEdosCtaIndBean.setIdPaperless(tramaDatos.substring(232,233).trim());
			}						
			confEdosCtaIndBean.setCuenta(tramaDatos.substring(11,23).trim());
			confEdosCtaIndBean.setOficina(tramaDatos.substring(28,32).trim());
			confEdosCtaIndBean.setProducto(tramaDatos.substring(32,34).trim());
			confEdosCtaIndBean.setSubproducto(tramaDatos.substring(34,38).trim());
			confEdosCtaIndBean.setUso(tramaDatos.substring(38,41).trim());
		}
		for(int aux=0; aux<arrayTrama.length; aux++) {
			if(arrayTrama[aux].indexOf("DCODMDD41") > -1) {
				String cuenta = arrayTrama[aux].substring(11,23).trim();
				if (cuenta.indexOf('B') == 0) {
					listCuentas.add("BME".concat(cuenta.substring(1, cuenta.length())));
				} else if (cuenta.startsWith("0")) {
					listCuentas.add(cuenta.substring(1, cuenta.length()));
				} else {
					listCuentas.add(cuenta);
				}
			}
		}
		confEdosCtaIndBean.setCuentas(listCuentas);
		return confEdosCtaIndBean;
	}	
	
	
	/**
	 * Metodo para desentramar la trx ODB2
	 * @param respuesta : Trama de respuesta
	 * @return ConfEdosCtaIndBean : Bean de la informacion
	 */
	private ConfEdosCtaIndBean desentramaODB2(String respuesta) {
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		String tramaDatos = "";
		String[] arrayTrama = null;
		if(respuesta != null) {
			respuesta = respuesta.trim();
			if(respuesta.length() > 0) {
				arrayTrama = respuesta.split("[@]");
			}
			if (arrayTrama != null && arrayTrama.length >= 3) {
				tramaDatos = arrayTrama[2];
			}
		}
		if(tramaDatos.length() >= 9) {
			confEdosCtaIndBean.setCodRetorno(tramaDatos.substring(2,9).trim());
			EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - desentramaODB2 :: codigo de error: ["
							+ confEdosCtaIndBean.getCodRetorno() + "]",
					NivelLog.DEBUG);
			confEdosCtaIndBean.setDescRetorno(tramaDatos.substring(9, tramaDatos.length()-1).trim());
			EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - desentramaODB2 :: descripcion de error: ["
					+ confEdosCtaIndBean.getDescRetorno() + "]",
			NivelLog.DEBUG);
		}
		return confEdosCtaIndBean;
	}
}