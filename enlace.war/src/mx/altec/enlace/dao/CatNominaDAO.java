package mx.altec.enlace.dao;

import java.sql.*;
import javax.sql.*;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.bo.CatNominaConstantes;
import mx.altec.enlace.bo.CatNominaLeeRS;
import mx.altec.enlace.bo.CatNominaBean;

/* everis JARA - Ambientacion - Extiende de Base Servlet para acceder a metodo createiASConn */
public class CatNominaDAO //extends BaseServlet
{
		/* everis JARA - Ambientacion - Agregado PreparedStatement y Connection */
		private PreparedStatement statement =null;
		private Connection conn=null;
		/*Conexion GFI*/
		private PreparedStatement statement2 =null;
		private Connection conn2=null;
		String query = "";


	/***************************************************************
	 *** M�todo que realiza la b�squeda de empleados 	 		****
	 *** con base en los parametros introducidos				****
	 **************************************************************/
	/*** Ultima Modificacion: 04 Jun 2008 						****
	 *** Autor:Emmanuel Sanchez Castillo (ESC)			 		****
	 *** Modificaci�n:se modifica la consulta para conderar 	****
	 *** el estado pendiente activar							****
	 *** Se agrega a la consulta el campo estatus				****
	 ***************************************************************/
	public String[][] buscaEmpleado(String numContrato, String radioValue, String valorBusqueda, int totalReg)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - buscaEmpleado $ Inicio");
		System.out.println("CatNominaDAO - buscaEmpleado $ El contrato es: " + numContrato);
		System.out.println("CatNominaDAO - buscaEmpleado $ El filtro de la busqueda es: " + radioValue);
		System.out.println("CatNominaDAO - buscaEmpleado $ El valor de la busqueda es: " + valorBusqueda);

		String filtroBusqueda = "";
		ResultSet rs = null;
		String[][] arrBiDatosQuery = null;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle conn = new ConexionOracle();
		CatNominaBean bean = new CatNominaBean();

		try
		{
			//Crear(); Prepared Statement
			String tablasBusq = CatNominaConstantes._CAT_NOM + ", " + CatNominaConstantes._DET_CAT;
			filtroBusqueda = generaFiltroBusqueda(numContrato, radioValue, valorBusqueda);

			//Obtenemos el total de registros que traer� la consulta
			//totalReg = cuentaQueryBusqueda(filtroBusqueda, tablasBusq);
			System.out.println("CatNominaDAO - buscaEmpleado $ El total de registros son =" + totalReg);

			// Se construye query para las b�squeda de empleados.
			query = "SELECT " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO + ", " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._NUM_EMPL + ", "
					+ CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._DEPTO_EMPL + ", " + CatNominaConstantes._APE_PAT
					+ ", " + CatNominaConstantes._APE_MAT + ", " + CatNominaConstantes._NOMBRE + ", " + CatNominaConstantes._SUELDO + ", " + CatNominaConstantes._RFC + ", " + CatNominaConstantes._TIPO_CTA_EVER
					+ ", " + CatNominaConstantes._STAT_CUENTA_EVER
					+ " FROM " + tablasBusq + " WHERE " + filtroBusqueda
					+ " AND (" + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'"
					+ " OR "   + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._PEND + "')" //VC ESC
					+ " ORDER BY " + CatNominaConstantes._APE_PAT + " ASC";

			System.out.println("CatNominaDAO - buscaEmpleado $ El query BUSCA a realizar es =" + query);
			Crear(query);

			if(totalReg>0){
				//rs = Consulta(query);
				rs = Consulta();
				CatNominaLeeRS leeRS = new CatNominaLeeRS();
				arrBiDatosQuery = leeRS.separaRSBusquedaEmpleados(rs, totalReg); // Guardamos los resultados en un arreglo Bidimensional
				buscarBancos(arrBiDatosQuery);
				System.out.println("CatNominaAction - consultaEmpleados $ El tama� del arreglo es: " + arrBiDatosQuery.length + " Renglones");
			}
			else {
				arrBiDatosQuery = new String[1][1];
				arrBiDatosQuery[0][0] = CatNominaConstantes._ERROR;
				bean.setExistenReg(false);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			arrBiDatosQuery = new String[1][1];
			arrBiDatosQuery[0][0] = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}
		System.out.println("Resultado: " + arrBiDatosQuery[0][0]);
		System.out.println("CatNominaDAO - buscaEmpleado $ Fin");
		return arrBiDatosQuery;
	}


	/***************************************************************
	 *** M�todo que realiza la b�squeda de empleados 	 		****
	 *** con base en los parametros introducidos				****
	 *** Ultima Modificacion: 04 Jun 2008 						****
	 *** Autor:Emmanuel Sanchez Castillo (ESC)			 		****
	 *** Modificaci�n:se modifica la consulta para conderar 	****
	 *** el estado pendiente activar					 		****
	 ***************************************************************/
	public int cuentaQueryBusqueda(String filtroBusqueda, String tablasBusqueda)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - cuentaQueryBusqueda $ Inicio");
		System.out.println("CatNominaDAO - cuentaQueryBusqueda $ El filtro es: " + filtroBusqueda);

		ResultSet rs = null;
		int totalReg = 0;
		//CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle conn = new ConexionOracle();
		//VC ESC -
		query = "SELECT COUNT(*) AS " + CatNominaConstantes._TOTAL + " FROM " + tablasBusqueda +
				" WHERE " + filtroBusqueda + " AND (" + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'"+
				" OR " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._PEND + "')";

		System.out.println("CatNominaDAO - cuentaQueryBusqueda $ El query COUNT a realizar es =" + query);

		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				totalReg = rs.getInt("TOTAL");
			}
		}
		catch (Exception e){
			e.printStackTrace();
			totalReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - cuentaQueryBusqueda $ Fin");
		return totalReg;
	}


	/***************************************************************
	 *** M�todo que obtiene los datos de un empleado 	 		****
	 *** cuando se solicita realizar una modificaci�n en l�nea	****
	 **************************************************************/
	public String[] obtenDatosEmplModif(String numContrato, String numCuenta, String tipoCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - obtenDatosEmplModif $ Inicio");
		System.out.println("CatNominaDAO - obtenDatosEmplModif $ El numContrato es:" + numContrato);
		System.out.println("CatNominaDAO - obtenDatosEmplModif $ El numCuenta es:" + numCuenta);
		System.out.println("CatNominaDAO - obtenDatosEmplModif $ El tipoCuenta es:" + tipoCuenta);

		//CatNominaConstantes cons = new CatNominaConstantes();
		CatNominaLeeRS resulSet = new CatNominaLeeRS();
		//ConexionOracle = new ConexionOracle();
		String[] datosModificar = null;
		ResultSet rs = null;

		//String tipoCuenta = "";

		try {
			//Crear();
			/* jp@everis*/
			//if(  numCuenta.trim().length() == 11  ){
			if( tipoCuenta.equals("PROPIA")){
				query = "SELECT " + CatNominaConstantes._DEPTO_EMPL + ", " + CatNominaConstantes._SUELDO + ", " + CatNominaConstantes._CTA_ABO + ", " + CatNominaConstantes._NUM_EMPL
							+ ", " + CatNominaConstantes._TIPO_CTA_EVER
							+ " FROM " + CatNominaConstantes._DET_CAT + " WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND "
							+ CatNominaConstantes._CTA_ABO + " = '" + numCuenta + "'";
			}
			else{
				query = "SELECT " + "CATNOM."+CatNominaConstantes._CTA_ABO + ", " + "CATNOM."+CatNominaConstantes._NUM_EMPL + ", " + "CATNOM."+CatNominaConstantes._DEPTO_EMPL
				+ ", " + "CATNOM."+CatNominaConstantes._SUELDO + ", " + "NOMEMPL."+CatNominaConstantes._NOMBRE + ", " + "NOMEMPL."+CatNominaConstantes._APE_PAT
				+ ", " + "NOMEMPL."+CatNominaConstantes._APE_MAT + ", " + "NOMEMPL."+CatNominaConstantes._RFC + ", " + "NOMEMPL."+CatNominaConstantes._SEXO
				+ ", " + "NOMEMPL."+CatNominaConstantes._CALLE_NUM + ", " + "NOMEMPL."+CatNominaConstantes._COLONIA + ", " + "NOMEMPL."+CatNominaConstantes._DELEG_MUN
				+ ", " + "NOMEMPL."+CatNominaConstantes._CVE_EDO + ", " + "NOMEMPL."+CatNominaConstantes._CIUDAD_POB + ", " + "NOMEMPL."+CatNominaConstantes._CP
				+ ", " + "NOMEMPL."+CatNominaConstantes._CVE_PAIS + ", " + "NOMEMPL."+CatNominaConstantes._PREF_PART + ", " + "NOMEMPL."+CatNominaConstantes._NUM_PART
				+ ", " + "CONTRNOM."+CatNominaConstantes._CALLE_OF + ", " + "CONTRNOM."+CatNominaConstantes._COL_OF + ", " + "CONTRNOM."+CatNominaConstantes._DELEG_OF
				+ ", " + "CONTRNOM."+CatNominaConstantes._EDO_OF + ", " + "CONTRNOM."+CatNominaConstantes._CIUDAD_OF + ", " + "CONTRNOM."+CatNominaConstantes._CP_OF
				+ ", " + "CONTRNOM."+CatNominaConstantes._PAIS_OF + ", " + "CONTRNOM."+CatNominaConstantes._CVE_DIREC + ", " + "CONTRNOM."+CatNominaConstantes._PREF_OF
				+ ", " + "CONTRNOM."+CatNominaConstantes._NUMERO_OF + ", " + "CONTRNOM."+CatNominaConstantes._EXT_OF + ", " + "CATNOM."+CatNominaConstantes._TIPO_CTA_EVER
				+ " FROM " + CatNominaConstantes._DET_CAT + " CATNOM, " + CatNominaConstantes._CAT_NOM + " NOMEMPL, " + CatNominaConstantes._CONTR_CAT + " CONTRNOM "
				+ " WHERE CATNOM." + CatNominaConstantes._CONTR + " = '" + numContrato.trim() + "' AND "
				+ "CATNOM."+CatNominaConstantes._CTA_ABO + " = '" + numCuenta.trim() + "' AND "
				+ "CATNOM."+CatNominaConstantes._CTA_ABO + " = NOMEMPL." + CatNominaConstantes._CTA_ABO + " AND "
				+ "CATNOM."+ CatNominaConstantes._CONTR + " = CONTRNOM." + CatNominaConstantes._CONTR;
			}

			/* jp@everis*/

			Crear(query);

			System.out.println("CatNominaDAO - obtenDatosEmplModif $ El query que trae datos del empleado: " + query);
			//rs = Consulta(query);
			rs = Consulta();
			/*jp@everis*/
			//datosModificar = resulSet.guardaRSDatosModif(rs);
			datosModificar = resulSet.guardaRSDatosModif(rs,numCuenta, tipoCuenta);

		}
		catch (Exception e){
			e.printStackTrace();
			datosModificar = new String[1];
			datosModificar[0] = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - obtenDatosEmplModif $ FIN");
		return datosModificar;
	}


	/******************************************************************
	 *** M�todo que realiza la actualizaci�n de los datos en la 	***
	 *** las tablas del nuevo cat�logo de n�mina. 	 	 			***
	 *****************************************************************/
	public boolean actualizaDatosEmpl(String[] datosEmpleados, String usuario)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - actualizaDatosEmpl $ Inicio");
		System.out.println("CatNominaDAO - actualizaDatosEmpl $ El numContrato es:" + datosEmpleados[3]);
		System.out.println("CatNominaDAO - actualizaDatosEmpl $ El numCuenta es:" + datosEmpleados[4]);

	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ResultSet rs = null;
		String query = "";
		String query1 = "";
		String query2 = "";
		String query3 = "";
		boolean valReturn = true;

		//ConexionOracle = new ConexionOracle();

		try
		{
			//Crear();

			/* jp@everis */
			if( datosEmpleados.length == 5 ){
				System.out.println("PROPIETARIO");
				query = "UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._NUM_EMPL + " = '" + datosEmpleados[0] + "', " +
					CatNominaConstantes._DEPTO_EMPL + " = '" + datosEmpleados[1] + "', " + CatNominaConstantes._SUELDO + " = " + datosEmpleados[2] + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '" + usuario + "', " + CatNominaConstantes._FCH_MODIF_REG_EVER + " = " + "SYSDATE" +
					" WHERE " + CatNominaConstantes._CONTR + " = '" + datosEmpleados[3] + "' AND " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[4] +
					"' AND " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'";

				System.out.println("CatNominaDAO - generaQueryActualiaEmpl $ Query de Modificaci�n = " + query);
				Crear(query);
				//Inserta(query);
				Inserta();
			}
			else{
				System.out.println("Interbancaria");
				query1 = "UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._NUM_EMPL + " = '" + datosEmpleados[0] + "', " +
						CatNominaConstantes._DEPTO_EMPL + " = '" + datosEmpleados[1] + "', " + CatNominaConstantes._SUELDO + " = " + datosEmpleados[2] + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '" + usuario + "', " + CatNominaConstantes._FCH_MODIF_REG_EVER + " = " + "SYSDATE" +
						" WHERE " + CatNominaConstantes._CONTR + " = '" + datosEmpleados[3].trim() + "' AND " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[29].trim() + "'";
				query2 = "UPDATE " + CatNominaConstantes._CAT_NOM + " SET " + CatNominaConstantes._APE_PAT + " = '" + datosEmpleados[5] + "', " +
						CatNominaConstantes._APE_MAT + " = '" + datosEmpleados[6] + "', " + CatNominaConstantes._SEXO + " = '" + datosEmpleados[8] + "', " +
						CatNominaConstantes._CALLE_NUM + " = '" + datosEmpleados[9] + "', " + CatNominaConstantes._COLONIA + " = '" + datosEmpleados[10] + "', " +
						CatNominaConstantes._DELEG_MUN + " = '" + datosEmpleados[11] + "', " + CatNominaConstantes._CVE_EDO + " = '" + datosEmpleados[12] + "', " +
						CatNominaConstantes._CIUDAD_POB + " = '" + datosEmpleados[13] + "', " + CatNominaConstantes._CP + " = '" + datosEmpleados[14] + "', " +
						CatNominaConstantes._CVE_PAIS + " = '" + datosEmpleados[15] + "', " + CatNominaConstantes._PREF_PART + " = '" + datosEmpleados[16] + "', " +
						CatNominaConstantes._NUM_PART + " = '" + datosEmpleados[17] + "' " +
						" WHERE " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[29].trim() + "'";
				query3 = "UPDATE " + CatNominaConstantes._CONTR_CAT + " SET " + CatNominaConstantes._CALLE_OF + " = '" + datosEmpleados[18] + "', " +
						CatNominaConstantes._COL_OF + " = '" + datosEmpleados[19] + "', " + CatNominaConstantes._DELEG_OF + " = '" + datosEmpleados[20] + "', " +
						CatNominaConstantes._EDO_OF + " = '" + datosEmpleados[21] + "', " + CatNominaConstantes._CIUDAD_OF + " = '" + datosEmpleados[22] + "', " +
						CatNominaConstantes._CP_OF + " = '" + datosEmpleados[23] + "', " + CatNominaConstantes._PAIS_OF + " = '" + datosEmpleados[24] + "', " +
						CatNominaConstantes._CVE_DIREC + " = '" + datosEmpleados[25] + "', " + CatNominaConstantes._PREF_OF + " = '" + datosEmpleados[26] + "', " +
						CatNominaConstantes._NUMERO_OF + " = '" + datosEmpleados[27] + "', " + CatNominaConstantes._EXT_OF + " = '" + datosEmpleados[28] + "' " +
						" WHERE " + CatNominaConstantes._CONTR + " = '" + datosEmpleados[3].trim() + "'";

				System.out.println(query1);
				System.out.println(query2);
				System.out.println(query3);

				Crear(query1);
				//Inserta(query1);
				Inserta();
				Crear(query2);
				//Inserta(query2);
				Inserta();
				Crear(query3);
				//Inserta(query3);
				Inserta();
			}
			/* jp@everis */
		}
		catch (Exception e) {
			e.printStackTrace();
			valReturn = true;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		return valReturn;

	}


	/******************************************************************
	 *** M�todo que realiza la baja l�gica de la cuenta de un		***
	 *** empleado													***
	 *****************************************************************/
	public int bajaCuentaContrato(String numContrato, String numCuenta, String usuario)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - bajaCuentaContrato $ Inicio");
		System.out.println("CatNominaDAO - bajaCuentaContrato $ El numContrato es:" + numContrato);
		System.out.println("CatNominaDAO - bajaCuentaContrato $ El numCuenta es:" + numCuenta);
		System.out.println("CatNominaDAO - bajaCuentaContrato $ El usuario es:" + usuario);

	//	CatNominaConstantes cons = new CatNominaConstantes();

		int updateBaja = 0;
		//ConexionOracle = new ConexionOracle();

		try {
			//Crear();
			query = "UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._INA + "', " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '" + usuario + "', " + CatNominaConstantes._FCH_MODIF_STAT_EVER + " = " + "SYSDATE" + " WHERE "
				+ CatNominaConstantes._CONTR + " = '"+numContrato+"' AND " + CatNominaConstantes._CTA_ABO + " ="+ numCuenta;
			Crear(query);

			System.out.println("CatNominaDAO - bajaCuentaContrato $ Query a ejectarse = " + query);
			//updateBaja = Inserta(query);
			updateBaja = Inserta();
		}
		catch (Exception e){
			e.printStackTrace();
			updateBaja = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - bajaCuentaContrato $ FIN");
		return updateBaja;
	}


	/***************************************************************
	 *** Verifica la existencia de una cuenta interbancaria		****
	 *** en el nuevo cat�logo									****
	 **************************************************************/
	public int buscaCuenta(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - buscaCuenta $ Inicio");
		System.out.println("CatNominaDAO - buscaCuenta $ El numContrato es:" + numContrato);
		System.out.println("CatNominaDAO - buscaCuenta $ El numCuenta es:" + numCuenta);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int cuentaReg = 0;

		//ConexionOracle = new ConexionOracle();

		query = "SELECT * FROM " + CatNominaConstantes._DET_CAT + " WHERE " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = " + numContrato + " AND "
				+ CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO + " = " + numCuenta;

		System.out.println("CatNominaDAO - buscaCuenta $ El query busca Cuenta: " + query);

		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()) {
				cuentaReg++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			cuentaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - buscaCuenta $ Relaciones Existentes: " + cuentaReg);
		return cuentaReg;
	}


	/***************************************************************
	 *** Inserta el alta de empleado con cuenta Interbancaria	****
	 *** en el nuevo cat�logo de n�mina		TABLA EWEB_CAT_NOM	****
	 **************************************************************/
	public int altaInterbOnlineCatNom(CatNominaBean bean)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - altaInterbOnlineCatNom $ Inicio");

	//	CatNominaConstantes cons = new CatNominaConstantes();
		int insertaReg = 0;
		int numSecuencia = 0;

		numSecuencia = generaQuerySecuencia();

		//ConexionOracle = new ConexionOracle();
		try {
			//Crear();
			if(numSecuencia != -100)
			{
				query = "INSERT INTO " + CatNominaConstantes._CAT_NOM + "(" + CatNominaConstantes._ID_CAT + ", " + CatNominaConstantes._CTA_ABO + ", " +
								 CatNominaConstantes._APE_PAT + ", " + CatNominaConstantes._APE_MAT + ", " + CatNominaConstantes._NOMBRE + ", " +
								 CatNominaConstantes._RFC + ", " + CatNominaConstantes._SEXO + ", " + CatNominaConstantes._CALLE_NUM + ", " +  CatNominaConstantes._COLONIA + ", " +
								 CatNominaConstantes._DELEG_MUN + ", " +  CatNominaConstantes._CVE_EDO + ", " +  CatNominaConstantes._CIUDAD_POB + ", " + CatNominaConstantes._CP + ", " +
								 CatNominaConstantes._CVE_PAIS + ", " + CatNominaConstantes._PREF_PART + ", " + CatNominaConstantes._NUM_PART + ", " + CatNominaConstantes._STA_PROCE + ", " + CatNominaConstantes._FCH_MOD_STA_PROCE + ") " +
								 " values (" + numSecuencia + ", '" + bean.getNumCuenta()+ "', '" +
								 bean.getApellidoP()+ "', '" + bean.getApellidoM() + "', '" + bean.getNombreEmpl() + "', '" +
								 bean.getRFC()+ "', '" + bean.getSexo() + "', '" + bean.getCalle() + "', '" + bean.getColonia() + "', '" +
								 bean.getDelegacion() + "', '" + bean.getCveEstado() + "', '" + bean.getCiudad() + "', '" + bean.getCodigoPostal() + "', '" +
								 bean.getClavePais() + "', '" + bean.getPrefTelPart() + "', " + bean.getTelPart() +", " + "'A'" + ", " + "SYSDATE" +")";


				System.out.println("CatNominaDAO - altaInterbOnlineCatNom $ Query de Insercion Interb" + query);
				Crear(query);
				//insertaReg = Inserta(query);
				insertaReg = Inserta();
			}
			else
				insertaReg = numSecuencia;
		}
		catch (Exception e) {
			e.printStackTrace();
			insertaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}
		System.out.println("CatNominaDAO - altaInterbOnlineCatNom $ Relaciones Existentes: " + insertaReg);
		return insertaReg;
	}

	/***************************************************************
	 *** Inserta el alta de empleado con cuenta Interbancaria	****
	 *** en el nuevo cat�logo de n�mina		TABLA EWEB_DET_CAT	****
	 **************************************************************/
	public int altaInterbOnlineDetCat(CatNominaBean bean)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - altaInterbOnlineDetCat $ Inicio");

	//	CatNominaConstantes cons = new CatNominaConstantes();
		int insertaReg = 0;

		//ConexionOracle = new ConexionOracle();

		try {
		//	Crear();

			query = "INSERT INTO " + CatNominaConstantes._DET_CAT + "(" + CatNominaConstantes._CONTR + ", " + CatNominaConstantes._CTA_ABO + ", " +
							 CatNominaConstantes._NUM_EMPL + ", " + CatNominaConstantes._DEPTO_EMPL + ", " + CatNominaConstantes._SUELDO + ", " +
							 CatNominaConstantes._TIPO_CTA_EVER + ", " + CatNominaConstantes._STAT_CUENTA_EVER + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + ", " +CatNominaConstantes._FCH_MODIF_REG_EVER + ") " +
							 " values ('" + bean.getNumContrato() + "', '" + bean.getNumCuenta()+ "', '" +
							 bean.getNumEmpl()+ "', '" + bean.getDeptoEmpl() + "', '" + bean.getIngresoMensual() + "', '" +
							 CatNominaConstantes._CTA_INTERB + "', '" + CatNominaConstantes._PEND + "', '" + bean.getCveUsuario() + "', " + "SYSDATE" + ")";

			System.out.println("CatNominaDAO - altaInterbOnlineDetCat $ Query de Insercion Interb" + query);
			Crear(query);
		//	insertaReg = Inserta(query);
			insertaReg = Inserta();
		}
		catch (Exception e) {
			e.printStackTrace();
			insertaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - altaInterbOnlineDetCat $ Alta en DET_CAT: " + insertaReg);
		return insertaReg;
	}

	/***************************************************************
	 *** Verifica la existencia de un contrato en la tabla 		****
	 *** EWEB_CONTR_CAT_NOM
	 **************************************************************/
	public int buscaContrato(String numContrato)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - buscaContrato $ Inicio");
		System.out.println("CatNominaDAO - buscaContrato $ El numContrato es:" + numContrato);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int cuentaReg = 0;

		//ConexionOracle = new ConexionOracle();

		query = "SELECT * FROM " + CatNominaConstantes._CONTR_CAT + " WHERE " + CatNominaConstantes._CONTR + " = " + numContrato;

		System.out.println("CatNominaDAO - buscaContrato $ El query busca Cuenta: " + query);

		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next())
			{
				cuentaReg++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			cuentaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}


		System.out.println("CatNominaDAO - buscaContrato $ Existe contrato: " + cuentaReg);
		return cuentaReg;
	}
	/***************************************************************
	 *** Inserta el alta de empleado con cuenta Interbancaria	****
	 *** en el nuevo cat�logo de n�mina		TABLA EWEB_CONTR_CAT_NOM	****
	 **************************************************************/
	public int altaInterbOnlineContrCat(CatNominaBean bean)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - altaInterbOnlineContrCat $ Inicio");

	//	CatNominaConstantes cons = new CatNominaConstantes();
		int insertaReg = 0;

		//ConexionOracle = new ConexionOracle();

		try {
			//Crear();
		query = "INSERT INTO " + CatNominaConstantes._CONTR_CAT + "(" + CatNominaConstantes._CONTR + ", " + CatNominaConstantes._CALLE_OF + ", " +
						 CatNominaConstantes._COL_OF + ", " + CatNominaConstantes._DELEG_OF + ", " +  CatNominaConstantes._EDO_OF + ", " +  CatNominaConstantes._CIUDAD_OF + ", " +
						 CatNominaConstantes._CP_OF+ ", " + CatNominaConstantes._PAIS_OF + ", " + CatNominaConstantes._CVE_DIREC + ", " + CatNominaConstantes._PREF_OF + ", " +
						 CatNominaConstantes._NUMERO_OF + ", " + CatNominaConstantes._EXT_OF + ") " +
						 " values ('" + bean.getNumContrato() + "', '" + bean.getCalleOfi()+ "', '" +
						 bean.getColoniaOfi() + "', '" + bean.getDelegOfi() + "', '" + bean.getCveEstadoOfi() + "', '" + bean.getCiudadOfi() + "', '" +
						 bean.getCodPostalOfi() + "', '" + bean.getPaisOfi() + "', " + bean.getCveDireccionOfi() + ", " + bean.getPrefTelOfi() + ", " +
						 bean.getTelOfi() + ", " + bean.getExtOfi()+ ")";


		System.out.println("CatNominaDAO - altaInterbOnlineContrCat $ Query de Insercion Interb " + query);
		Crear(query);
		//insertaReg = Inserta(query);
		insertaReg = Inserta();
		}
		catch (Exception e) {
			e.printStackTrace();
			insertaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}


		System.out.println("CatNominaDAO - altaInterbOnlineContrCat $ Relaciones Existentes: " + insertaReg);
		return insertaReg;
	}

	/***************************************************************
	 *** Genera un n�mero desde la secuencia para insertarlo en	****
	 *** el cat�logo de empleados								****
	 **************************************************************/
	public int generaQuerySecuencia()
	{
		System.out.println("");
		System.out.println("CatNominaDAO - generaQuerySecuencia $ Inicio");

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int numSecuencia = 0;

		//ConexionOracle = new ConexionOracle();


		try {
		//	Crear();
			query = "SELECT " + CatNominaConstantes._SEQ_CAT_NOM + "." + CatNominaConstantes._NEXT + " AS " + CatNominaConstantes._NUM_SEC + " FROM DUAL";
			System.out.println("CatNominaDAO - altaInterbOnlineContrCat $ Query de Insercion Interb" + query);
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()) {
				numSecuencia = Integer.parseInt(rs.getString(CatNominaConstantes._NUM_SEC));
				System.out.println("CatNominaDAO - ggeneraQuerySecuencia $ Secuencia: " + numSecuencia);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			numSecuencia = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

	//	System.out.println("CatNominaDAO - generaQuerySecuencia $ Numero de Secuencia: " + numSecuencia);
		return numSecuencia;
		}


	/******************************************************************
	 *** M�todo que genera el filtro para la b�squeda de empleados	***
	 ***  con base en la opcion elegida en el front. 				***
	 ******************************************************************/
	public String generaFiltroBusqueda(String numContrato, String radioValue, String valorBusqueda)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - generaFiltroBusqueda $ Inicio");
		System.out.println("CatNominaDAO - generaFiltroBusqueda $ El numContrato es: " + numContrato);
		System.out.println("CatNominaDAO - generaFiltroBusqueda $ El filtro de la busqueda es: " + radioValue);
		System.out.println("CatNominaDAO - generaFiltroBusqueda $ El valor de la busqueda es: " + valorBusqueda);

		String filtroBusqueda = "";
	//	CatNominaConstantes cons = new CatNominaConstantes();

		// Generamos secuencia para el WHERE dependiendo del filtro seleccionado
		valorBusqueda = (valorBusqueda != null ? valorBusqueda.trim() : "");
		if(radioValue.equals("id"))
			filtroBusqueda = CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._DET_CAT + "."
					+  CatNominaConstantes._NUM_EMPL + " = '" + valorBusqueda + "' AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO
					+ " = " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO ;

		else if(radioValue.equals("nombre"))
			filtroBusqueda = CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._DET_CAT + "."
			+  CatNominaConstantes._CTA_ABO + " = " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO
			//+ " AND UPPER(" + cons._CAT_NOM + "." + cons._NOMBRE
			//+  ") = UPPER('" + valorBusqueda + "') ";
			+ " AND " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._NOMBRE + " like '%" + valorBusqueda + "%' ";
			//" = '" + valorBusqueda + "' ";
			//+  ") like UPPER('%" + valorBusqueda + "%') ";

		else if(radioValue.equals("apellido"))
			filtroBusqueda = CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._DET_CAT + "."
			+  CatNominaConstantes._CTA_ABO + " = " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO
			//+ " AND (UPPER(" + cons._CAT_NOM + "." + cons._APE_PAT
			//+  ") = UPPER('" + valorBusqueda + "') OR UPPER(" + cons._CAT_NOM + "." + cons._APE_MAT +  ") = UPPER('" + valorBusqueda +"')) ";
			+ " AND (" + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._APE_PAT +  " like '%" + valorBusqueda + "%' OR " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._APE_MAT +  " like '%" + valorBusqueda +"%') ";
			//+  ") like UPPER('%" + valorBusqueda + "%') OR UPPER(" + cons._CAT_NOM + "." + cons._APE_MAT +  ") like UPPER('%" + valorBusqueda +"%')) ";

		else if(radioValue.equals("rfc"))
			filtroBusqueda = CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._DET_CAT + "."
			+  CatNominaConstantes._CTA_ABO + " = " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO
			//+ " AND UPPER(" + cons._CAT_NOM + "." + cons._RFC
			//+  ") = UPPER('" + valorBusqueda + "') ";
			+ " AND " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._RFC
			+  " = '" + valorBusqueda + "' ";
			//+  ") like UPPER('%" + valorBusqueda + "%') ";

		else if(radioValue.equals("numCuenta"))
			filtroBusqueda = CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._DET_CAT + "."
			+ CatNominaConstantes._CTA_ABO +  " = '" + valorBusqueda + "' AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO + " = "
			+ CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO;
		else if(radioValue.equals("todos"))
			filtroBusqueda = CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._DET_CAT + "."
			+ CatNominaConstantes._CTA_ABO + " = " 	+ CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO;

	//	System.out.println("CatNominaDAO - generaFiltroBusqueda $ el filtro es: " + filtroBusqueda);

		return filtroBusqueda;
	}


	/******************************************************************
	 *** verificar la existencia de las cuentas de empleados dentro	***
	 *** del cat�logo de n�mina para realizar modificaciones por 	***
	 *** archivo. 													***
	 ******************************************************************/
	public int existeRelacion(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - existeRelacion $ Inicio");
		System.out.println("CatNominaDAO - existeRelacion $ El numContrato es: " + numContrato);
		System.out.println("CatNominaDAO - existeRelacion $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		int existeReg = 0;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();


		query = "SELECT COUNT(*) AS " + CatNominaConstantes._TOTAL + " FROM " + CatNominaConstantes._DET_CAT +
				" WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato +
				"' AND " + CatNominaConstantes._CTA_ABO + " = '" + numCuenta +
				"' AND " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'";
		System.out.println("CatNominaDAO - existeRelacion $ El query para existencia de relaci�n es: " + query);


		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				existeReg = rs.getInt("TOTAL");
			}
			System.out.println("CatNominaDAO - existeRelacion $ El query para existencia de relaci�n es: " + existeReg);
		}
		catch (Exception e){
			e.printStackTrace();
			existeReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - existeRelacion $ Fin");
		return existeReg;
	}

	/**********************************************************************
	 *** Obtiene el tipo de cuenta del cual se busca realizar una 		***
	 *** modificaci�n de datos de empleados
	 **********************************************************************/
	public String obtenTipoCuenta(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - obtenTipoCuenta $ Inicio");
		System.out.println("CatNominaDAO - obtenTipoCuenta $ El numContrato es: " + numContrato);
		System.out.println("CatNominaDAO - obtenTipoCuenta $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		String tipoCuenta = "";
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();

		try {
			query = "SELECT " + CatNominaConstantes._TIPO_CTA_EVER + "  FROM " + CatNominaConstantes._DET_CAT +
					" WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._CTA_ABO + " = '" + numCuenta + "'";
			System.out.println("CatNominaDAO - obtenTipoCuenta $ El query del tipo de Cuenta es: " + query);

			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()){
				tipoCuenta = rs.getString("TIPO_CUENTA");
			}
			System.out.println("CatNominaDAO - obtenTipoCuenta $ El query para existencia de relaci�n es: " + tipoCuenta);
		}
		catch (Exception e){
			e.printStackTrace();
			tipoCuenta = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - obtenTipoCuenta $ Fin");
		return tipoCuenta;
	}

	/******************************************************************************
	 *** M�todo que verifica si el empleado con cuenta interbancaria tiene  	***
	 *** alguna cuenta interna asociado para poder o no realizar modificaciones	***
	 *** completas.  															***
	 ******************************************************************************/
	public int existeInternaEmplInterb(String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - existeInternaEmplInterb $ Inicio");
		System.out.println("CatNominaDAO - existeInternaEmplInterb $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		int totalInternas = 0;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();

		String[] datosPerson = obtenDatosPerson(numCuenta);

		try
		{
			if(datosPerson[0].equals("ERROR0000"))
			{
				query = "SELECT COUNT(1) as CUENTA FROM " + CatNominaConstantes._CAT_NOM + ", " + CatNominaConstantes._DET_CAT +
					" WHERE "  + CatNominaConstantes._CAT_NOM + "." + 	CatNominaConstantes._CTA_ABO + " = " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO +
					" AND " + CatNominaConstantes._NOMBRE + " = '" + datosPerson[0] +
					"' AND " + CatNominaConstantes._APE_PAT + " = '" + datosPerson[1] + "' AND " + CatNominaConstantes._RFC + " = '" + datosPerson[2] +
					"' AND " + CatNominaConstantes._TIPO_CTA_EVER + " = '" + CatNominaConstantes._CTA_INTERNA + "' AND " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'";
					System.out.println("CatNominaDAO - existeInternaEmplInterb $ El query que verifica si hay alguna cuenta interna " + query);

					//Crear();
					Crear(query);
					//rs = Consulta(query);
					rs = Consulta();

					while(rs.next()){
						totalInternas = rs.getInt("CUENTA");
					}
					System.out.println("CatNominaDAO - existeInternaEmplInterb $ El query para existencia de relaci�n es: " + totalInternas);
			}
			else
				totalInternas = -100;
		}
		catch (Exception e){
			e.printStackTrace();
			totalInternas = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - existeInternaEmplInterb $ Fin");
		return totalInternas;
	}


	/*************************************************************************
	 *** Obtiene el nombre, apellido paterno y rfc del empleado con cuenta  ***
	 *** interbancaria para buscar una relaci�n con una cuenta interna		***
	 *************************************************************************/
	public String[] obtenDatosPerson(String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - obtenDatosPerson $ Inicio");
		System.out.println("CatNominaDAO - obtenDatosPerson $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		String[] datosPerson = new String[3];
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();

		try {
			query = "  SELECT " + CatNominaConstantes._APE_PAT + ", " + CatNominaConstantes._NOMBRE + ", " + CatNominaConstantes._RFC +
					" FROM " + CatNominaConstantes._CAT_NOM + " WHERE " + CatNominaConstantes._CTA_ABO + " = '" + numCuenta + "'";
			System.out.println("CatNominaDAO - obtenDatosPerson $ El query que trae nombre, apellido, rfc: " + query);

			//Crear();
			Crear(query);
//			rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				datosPerson[0] = rs.getString("NOMBRE");
				datosPerson[1] = rs.getString("APELL_PATERNO");
				datosPerson[2] = rs.getString("REG_FED_CAUS");
			}
			System.out.println("CatNominaDAO - obtenDatosPerson $ El NOMBRE es: " + datosPerson[0]);
			System.out.println("CatNominaDAO - obtenDatosPerson $ El APELLIDO es: " + datosPerson[1]);
			System.out.println("CatNominaDAO - obtenDatosPerson $ El RFC es: " + datosPerson[2]);
		}
		catch (Exception e){
			e.printStackTrace();
			datosPerson[0] = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - obtenDatosPerson $ Fin");
		return datosPerson;
	}


	//ALTA INTERBANCARIA POR ARCHIVO


	/******************************************************************
	 *** M�todo que realiza la actualizaci�n de los datos en la 	***
	 *** las tablas del nuevo cat�logo de n�mina. 	 	 			***
	 *****************************************************************/
	//public void altaInterbArchivo(String[] datosEmpleados, String numContrato, String cveUsr)
	public boolean altaInterbArchivo(String[] datosEmpleados, String numContrato, String cveUsr)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - altaInterbArchivo $ Inicio");
		System.out.println("CatNominaDAO - altaInterbArchivo $ El numContrato es:" + numContrato);
		System.out.println("CatNominaDAO - altaInterbArchivo $ La cveUsr es:" + cveUsr);
		System.out.println("CatNominaDAO - altaInterbArchivo $ El numCuenta es:" + datosEmpleados[0]);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int numSecuencia = 0;
		boolean resul = false;
		String[] datosModificar = null;
		CatNominaLeeRS resulSet = new CatNominaLeeRS();
		int existeReg = 0;
		String existe = "";
		boolean retorno = false;

		numSecuencia = generaQuerySecuencia();

		//ConexionOracle = new ConexionOracle();



		try
		{
			//Crear();

			if (numSecuencia != -100)
			{

				String query = "SELECT " + CatNominaConstantes._CONTR + " FROM " + CatNominaConstantes._CONTR_CAT + " WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "'";
				System.out.println("*** CatalogoAlta - lecturaCam $ Contrato de query " + query);
				Crear(query);
				//rs = Consulta(query);

				rs = Consulta();



				/*rs.next();
				existe = rs.getString("CONTRATO");*/

				while(rs.next()){
					existe = rs.getString("CONTRATO");

				}

				System.out.println("CatNominaDAO - existeRelacion $ El query para existencia de relaci�n es: " + existe);



				//if(existe != numContrato)
				//if(existe.equals(numContrato))



				//if(existe.equals(numContrato))
				if(!(existe.trim()).equals(numContrato))
				{
					System.out.println("*** CatalogoAlta - lecturaCam $ El registro No existe " );
					String sql2 = "INSERT INTO " + CatNominaConstantes._CONTR_CAT + "(" + CatNominaConstantes._CONTR + "," + CatNominaConstantes._CALLE_OF + "," + CatNominaConstantes._COL_OF + "," + CatNominaConstantes._DELEG_OF + "," + CatNominaConstantes._EDO_OF + "," + CatNominaConstantes._CIUDAD_OF + "," + CatNominaConstantes._CP_OF + "," + CatNominaConstantes._PAIS_OF + "," +
					CatNominaConstantes._CVE_DIREC + "," + CatNominaConstantes._PREF_OF + "," + CatNominaConstantes._NUMERO_OF + "," + CatNominaConstantes._EXT_OF + ")" +
					 " values ('" + numContrato + "', '" + datosEmpleados[18] + "', '" + datosEmpleados[19] + "', '" + datosEmpleados[20] + "', '" + datosEmpleados[21] + "', '" + datosEmpleados[22] + "', '" + datosEmpleados[23] + "', '"+
					 datosEmpleados[24] + "', " + datosEmpleados[25] + ", " + datosEmpleados[26] + ", " + datosEmpleados[27] + ", " + datosEmpleados[28] +")";

					System.out.println("*** CatalogoAlta - lecturaCam 2 <<<< $ " + sql2);


					Crear(sql2);
					//int inser2 = Inserta(sql2);
					int inser2 = Inserta();
					System.out.println("*** CatalogoAlta - lecturaCam Entra a insertar Contrato$ ");
					System.out.println("*** CatalogoAlta - lecturaCam $ " + sql2);

				}


				String sql1 = "INSERT INTO " + CatNominaConstantes._CAT_NOM + "(" + CatNominaConstantes._ID_CAT + "," + CatNominaConstantes._CTA_ABO + "," + CatNominaConstantes._APE_PAT + "," + CatNominaConstantes._APE_MAT + "," + CatNominaConstantes._NOMBRE + "," + CatNominaConstantes._RFC + "," + CatNominaConstantes._SEXO + "," + CatNominaConstantes._CALLE_NUM + "," +
				CatNominaConstantes._COLONIA + "," + CatNominaConstantes._DELEG_MUN + "," + CatNominaConstantes._CVE_EDO + "," + CatNominaConstantes._CIUDAD_POB + "," + CatNominaConstantes._CP + "," + CatNominaConstantes._CVE_PAIS + "," + CatNominaConstantes._PREF_PART + "," + CatNominaConstantes._NUM_PART + ", "+ CatNominaConstantes._STA_PROCE + ", " + CatNominaConstantes._FCH_MOD_STA_PROCE + ")" +
				 " values (" + numSecuencia + ", '" + datosEmpleados[0] + "', '" + datosEmpleados[4] + "', '" + datosEmpleados[5] + "', '" + datosEmpleados[6] + "', '" + datosEmpleados[7] + "', '" + datosEmpleados[8] + "', '"+
				 datosEmpleados[9] + "', '" + datosEmpleados[10] + "', '" + datosEmpleados[11] + "', '" + datosEmpleados[12] + "', '" + datosEmpleados[13] + "', '" + datosEmpleados[14]+ "', '" + datosEmpleados[15] + "', '" + datosEmpleados[16] + "', " + datosEmpleados[17] + ", " + "'A'" + ", " + "SYSDATE" +")";

				System.out.println("*** CatalogoAlta - lecturaCam 1<<<< $ " + sql1);

				Crear(sql1);
	//			int inser = Inserta(sql1);
				int inser = Inserta();
				System.out.println("*** CatalogoAlta - lecturaCam $ " + sql1);

				if (inser != 0){

					String sql3 = "INSERT INTO " + CatNominaConstantes._DET_CAT + "(" + CatNominaConstantes._CONTR_DET + ","+ CatNominaConstantes._CTA_ABO_DET + ","+ CatNominaConstantes._DET_CAT+ "." + CatNominaConstantes._NUM_EMPL + "," + CatNominaConstantes._DET_CAT+ "." + CatNominaConstantes._DEPTO_EMPL + "," + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._SUELDO + ", " + CatNominaConstantes._TIPO_CTA_EVER + ", " + CatNominaConstantes._STAT_CUENTA_EVER + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + ", " + CatNominaConstantes._FCH_MODIF_REG_EVER + ")" +
					 " values ('" + numContrato +"', '" + datosEmpleados[0] + "', '" + datosEmpleados[1] + "', '" + datosEmpleados[2] + "', " + datosEmpleados[3] + ", " + "'INTERB'" + ", "+"'P'" + ", '" + cveUsr + "', " + "SYSDATE" +")";

					System.out.println("*** CatalogoAlta - lecturaCam $ " + sql3);

					Crear(sql3);
					//int inser3 = Inserta(sql3);
					int inser3 = Inserta();
					System.out.println("*** CatalogoAlta - lecturaCam $ " + sql3);

				}else {
					System.out.println("*** CatalogoAlta - lecturaCam $ No se realizo al Alta ");
				}


				System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaDAO) 1 = true");
				retorno=true;
			}
			else
				retorno = false;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaDAO) 2 = false");
			retorno =false;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaDAO) Correcto");
				System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaDAO)" + retorno);

				}
		return retorno;

	}






	/******************************************************************
	 *** verificar la existencia de las cuentas de empleados dentro	***
	 *** del cat�logo de n�mina para realizar Alta por         	    ***
	 *** archivo Interbancaria.										***
	 ******************************************************************/
	public int existeRegistro(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - existeRegistro $ Inicio");
		System.out.println("CatNominaDAO - existeRegistro $ El numContrato es: " + numContrato);
		System.out.println("CatNominaDAO - existeRegistro $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		int existeReg = 0;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();


		query = "SELECT COUNT(*) AS " + CatNominaConstantes._TOTAL + " FROM " + CatNominaConstantes._DET_CAT +
				" WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato +
				"' AND " + CatNominaConstantes._CTA_ABO + " = '" + numCuenta + "'";
		System.out.println("CatNominaDAO - existeRegistro $ El query para existencia de relaci�n es: " + query);


		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				existeReg = rs.getInt("TOTAL");
			}
			System.out.println("CatNominaDAO - existeRegistro $ El query para existencia de relaci�n es: " + existeReg);
		}
		catch (Exception e){
			e.printStackTrace();
			existeReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - existeRegistro $ Fin");
		return existeReg;
	}












	// MODIFICACION ARCHIVO INTERBANCARIA

	/***************************************************************
	 *** M�todo que obtiene los datos de un empleado 	 		****
	 *** cuando se solicita realizar una modificaci�n por archivo	****
	 **************************************************************/
	public String[] obtenDatosEmplModifInterb(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - obtenDatosEmplModifInterb $ Inicio");
		System.out.println("CatNominaDAO - obtenDatosEmplModifInterb $ El numContrato es:" + numContrato);
		System.out.println("CatNominaDAO - obtenDatosEmplModifInterb $ El numCuenta es:" + numCuenta);

		//CatNominaConstantes cons = new CatNominaConstantes();
		CatNominaLeeRS resulSet = new CatNominaLeeRS();
		//ConexionOracle = new ConexionOracle();
		String[] datosModificar = null;
		ResultSet rs = null;

		try {
			//Crear();


			query = " SELECT " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO_DET + ", " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._NUM_EMPL + ", " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._SUELDO + ", "
			+ CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._NOMBRE + ", " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._APE_PAT + ", " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._RFC + " FROM " + CatNominaConstantes._DET_CAT +", "+ CatNominaConstantes._CAT_NOM + ", "+ CatNominaConstantes._CONTR_CAT
			+ " WHERE " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO + " = '" + numCuenta + "' AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = " + CatNominaConstantes._CONTR_CAT + "." + CatNominaConstantes._CONTR + " AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO + " = " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO;


			System.out.println("CatNominaDAO - obtenDatosEmplModifInterb $ El query que trae datos del empleado: " + query);
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			datosModificar = resulSet.guardaRSDatosModifInterb(rs);
		}
		catch (Exception e){
			e.printStackTrace();
			datosModificar = new String[1];
			datosModificar[0] = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - obtenDatosEmplModifInterb $ FIN");
		return datosModificar;
	}







	/***************************************************************
	 *** M�todo que busca si existe alguna relaci�n    	 		****
	 *** con una cuenta Interna (Para cuentas Interbancarias)  	****
	 **************************************************************/
	public int existeRelacionCtaInterna(String[] arrDatosModifInterb, String numContrato, String numCuenta )
	{
		System.out.println("");
		System.out.println("CatNominaDAO - existeRelacionCtaInterna $ Inicio");


		//CatNominaConstantes cons = new CatNominaConstantes();
		CatNominaLeeRS resulSet = new CatNominaLeeRS();
		//ConexionOracle = new ConexionOracle();
		String[] datosModificar = null;
		ResultSet rs = null;
		int existeReg = 0;

		query = "SELECT COUNT(*) AS " + CatNominaConstantes._TOTAL + " FROM " + CatNominaConstantes._CAT_NOM + ", " + CatNominaConstantes._DET_CAT
			+ " WHERE " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._NOMBRE +" = '" + arrDatosModifInterb[4] + "' AND " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._APE_PAT + " = '" + arrDatosModifInterb[3] + "' AND " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._RFC + " = '" + arrDatosModifInterb[5] + "' AND "
			+ CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO_DET + " NOT IN '"+ numCuenta + "' AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._TIPO_CTA_EVER + " = " + "'PROPIA'" + " AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO_DET + " = " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO;


					System.out.println("CatNominaDAO - existeRelacionCtaInterna $ El query verifica no existencia de cuenta Interna: " + query);

		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()){
				existeReg = rs.getInt("TOTAL");
			}

			System.out.println("CatNominaDAO - existeRelacionCtaInterna $ El query verifica no existencia de cuenta Interna: " + existeReg);

		}
		catch (Exception e){
			e.printStackTrace();
			existeReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaDAO - existeRelacionCtaInterna $ FIN");
		return existeReg;
	}




	/******************************************************************
	 *** M�todo que realiza la actualizaci�n de los datos 		 	***
	 *** con cuenta Interbancaria en								***
	 *** las tablas del nuevo cat�logo de n�mina. 	 	 			***
	 *****************************************************************/
	//public void actualizaDatosArchInterb(String[] datosEmpleados, String numContrato, String cveUsr)
	public boolean actualizaDatosArchInterb(String[] datosEmpleados, String numContrato, String cveUsr)
	{
		System.out.println("");
		System.out.println("CatNominaDAO - actualizaDatosArchInterb $ Inicio");
		System.out.println("CatNominaDAO - actualizaDatosArchInterb $ El numContrato es:" + numContrato);
		System.out.println("CatNominaDAO - actualizaDatosArchInterb $ La cveUsr:" + cveUsr);
		System.out.println("CatNominaDAO - actualizaDatosArchInterb $ El numCuenta es:" + datosEmpleados[0]);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		boolean varReturn = true;

		//ConexionOracle = new ConexionOracle();

		try
		{
			//Crear();


			String sql1 = " UPDATE " + CatNominaConstantes._CONTR_CAT + " SET " + CatNominaConstantes._CALLE_OF + " = '" + datosEmpleados[14] + "', " + CatNominaConstantes._COL_OF +" = '" + datosEmpleados[15] + "', " + CatNominaConstantes._DELEG_OF + " = '" + datosEmpleados[16]+ "', " + CatNominaConstantes._EDO_OF +" = '" + datosEmpleados[17]+ "', " + CatNominaConstantes._CIUDAD_OF + " = '" + datosEmpleados[18] +"', " + CatNominaConstantes._CP_OF + " = '" + datosEmpleados[19] +"', " +
			CatNominaConstantes._PAIS_OF + " = '" + datosEmpleados[20] + "', " +	CatNominaConstantes._CVE_DIREC + " = '" + datosEmpleados[21] + "', " + CatNominaConstantes._PREF_OF + " = '" + datosEmpleados[22] + "', " + CatNominaConstantes._NUMERO_OF + " = '" + datosEmpleados[23] +"', " + CatNominaConstantes._EXT_OF + " = '" + datosEmpleados[24] +
			"' WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "'";

			Crear(sql1);
			//Inserta(sql1);
			Inserta();
			System.out.println("CatNominaDAO - generaQueryActualiaEmpl $ Query de Modificaci�n = " + sql1);

			String sql2 = " UPDATE " + CatNominaConstantes._CAT_NOM + " SET " +  CatNominaConstantes._SEXO + " = '" + datosEmpleados[4] + "', " + CatNominaConstantes._CALLE_NUM + " = '" + datosEmpleados[5] + "', " + CatNominaConstantes._COLONIA + " = '" + datosEmpleados[6] + "', " + CatNominaConstantes._DELEG_MUN + " = '" + datosEmpleados[7] + "', " +
			CatNominaConstantes._CVE_EDO + " = '" + datosEmpleados[8] + "', " + CatNominaConstantes._CIUDAD_POB + " = '" + datosEmpleados[9] + "', " + CatNominaConstantes._CP + " = '" + datosEmpleados[10] + "', " + CatNominaConstantes._CVE_PAIS + " = '" + datosEmpleados[11] + "', " + CatNominaConstantes._PREF_PART + " = '" + datosEmpleados[12] + "', " + CatNominaConstantes._NUM_PART + " = '" + datosEmpleados[13] +
			"' WHERE " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[0] + "'";

			Crear(sql2);
			//Inserta(sql2);
			Inserta();
			System.out.println("CatNominaDAO - generaQueryActualiaEmpl $ Query de Modificaci�n = " + sql2);



			String sql3 = " UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._NUM_EMPL + " = '" + datosEmpleados[1] + "', " +
					CatNominaConstantes._DEPTO_EMPL + " = '" + datosEmpleados[2] + "', " + CatNominaConstantes._SUELDO + " = " + datosEmpleados[3] + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '"+ cveUsr + "', " + CatNominaConstantes._FCH_MODIF_REG_EVER + " = " + "SYSDATE" +
					" WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[0] + "'";

			Crear(sql3);
			//Inserta(sql3);
			Inserta();
			System.out.println("CatNominaDAO - generaQueryActualiaEmpl $ Query de Modificaci�n = " + sql3);


		}
		catch (Exception e)
		{
			e.printStackTrace();
			varReturn = false;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}
		return varReturn;
	}



/*
    private void Crear()throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion-----------", EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO);
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO4);
			statement = conn.createStatement();
			//statement = conn.preparedStatement();
    	}
*/
	//PreparedStatement
	private void Crear(String query)throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion-----------PreparedStatament", EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO);
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO);
			statement = conn.prepareStatement (query);
			//statement = conn.preparedStatement();
    	}

	//	PreparedStatement
	/***************************************************************
	 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
	 *** Metodo para crear la conexion con ds gfi   			****
	 **************************************************************/
	private void Crear2(String query)throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion 2-----------PreparedStatament", EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.INFO);
    		conn2 = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.INFO);
			statement2 = conn2.prepareStatement (query);
			//statement = conn.preparedStatement();
    }


/*
	private ResultSet Consulta(String query)throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta -----------", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement.executeQuery(query);
		return rs;
		}*/


	//PreparedStatement
	private ResultSet Consulta()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement.executeQuery();
		return rs;
		}

	/***************************************************************
	 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
	 *** Metodo para crear statement							****
	 **************************************************************/
	private ResultSet Consulta2()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta 2 -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement2.executeQuery();
		return rs;
		}

/*	private int Inserta(String query)throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando UPDATE/INSERT -----------", EIGlobal.NivelLog.INFO);
		int resultado = 0;
		resultado = statement.executeUpdate(query);
		return resultado;
		}*/

	private int Inserta()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando UPDATE/INSERT -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		int resultado = 0;
		resultado = statement.executeUpdate();
		return resultado;
		}

	public String buscarBancos(String arrBiDatosQuery [][])
	{
		System.out.println("");
		System.out.println("CatNominaDAO - buscaBanco	  $ Inicio");


		ResultSet rs = null;
		String nomBanco ="";
		String numBanco ="";

		//CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();
		if(arrBiDatosQuery != null && arrBiDatosQuery[0][0].equals("ERROR0000"))
		{
			return "";
		}


		for (int i = 0 ; i< arrBiDatosQuery.length ; i++)
		{

			if(CatNominaConstantes._CTA_INTERNA.equals(arrBiDatosQuery[i][8])){
				arrBiDatosQuery[i][10] = CatNominaConstantes._BANCO_INTERNO;
			}else if (CatNominaConstantes._CTA_INTERB.equals(arrBiDatosQuery[i][8])){
				numBanco = obtenCLABE(arrBiDatosQuery[i][0]);
				query = "SELECT  " + CatNominaConstantes._NOM_BANCO+ " FROM " + CatNominaConstantes._COMU_INTERME +
						" WHERE " + CatNominaConstantes._NUM_BANCO + " = '" + numBanco + "'";

				System.out.println("CatNominaDAO - generaQuerybuscarBanco $ Query de Modificaci�n = " + query);
				try {

						Crear2(query);
						rs = Consulta2();
					while(rs.next()){
					nomBanco = rs.getString(CatNominaConstantes._NOM_BANCO);
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
			finally{
				arrBiDatosQuery[i][10] = nomBanco;
					try{
						if(rs!=null){
							rs.close();
							rs=null;}
						if(statement2!=null){
							statement2.close();
							statement2=null;}
						if(conn2!=null){
							conn2.close();
							conn2=null;}
						}catch(Exception e1){
							EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}
			}
		}
		/*try{

			if(conn2!=null){
				conn2.close();
				conn2=null;}
			}catch(Exception e1){
				EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
				}*/
		System.out.println("CatNominaDAO - buscaBanco $ Fin");
		return "";
	}

	private String obtenCLABE(String numCuenta)
	{
		String clabe = "";
		int j = 0;
		if(numCuenta != null && numCuenta.length() >= 3)
		{
			clabe = numCuenta.substring(0,3);
			//quitamos los ceros
			for( j = 0; j < clabe.length(); j++)
			{
				if(clabe.charAt(j) != '0')
					break;
			}
			clabe =  clabe.substring(j);
		}
		System.out.println("Cuenta -> " + numCuenta + " CLABE -> " + clabe);
		return clabe ;
	}
}
