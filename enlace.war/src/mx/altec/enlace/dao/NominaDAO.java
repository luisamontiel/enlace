package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NominaConstantes.SQL_EPSN_FLD_CONTR;
import static mx.altec.enlace.utilerias.NominaConstantes.SQL_EPSN_FLD_ESTATUS;
import static mx.altec.enlace.utilerias.NominaConstantes.SQL_EPSN_FLD_OBS;
import static mx.altec.enlace.utilerias.NominaConstantes.SQL_EPSN_FLD_SEC;
import static mx.altec.enlace.utilerias.NominaConstantes.SQL_EPSN_FLD_TIPO_NOM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import mx.altec.enlace.beans.ArchivoNominaDetBean;
import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.beans.SolPDFBean;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import static mx.altec.enlace.utilerias.NominaConstantes.*;
/**
 * 
 * @author FSW - Vector
 *  DAO de registro de solicitudes de informe de nomina PDF
 *
 */
public class NominaDAO extends GenericDAO {
	
	
	/**Constante SQL**/
	private final static String QUERY_INSERT_SOL_PDF = "INSERT INTO EWEB_MX_PRC_SOL_NOM ( COD_CONTR_PK,  NUM_SEC_PK, COD_TIPO_NOM, FCH_RECEP, FCH_ALTA, USR_ALTA, COD_ESTAT) "
											  + " values(?,?,?,?,sysdate,?,?)";
	/**Constante SQL**/
	private final static String QUERY_SELECT_SOL_PDF = "SELECT COD_CONTR_PK,NUM_SEC_PK,COD_ESTAT,FCH_PROC, COD_ESTAT ,DSC_OBSER FROM EWEB_MX_PRC_SOL_NOM ";
	
	/** FCHSOLINF Constante para definir el nombre del parámetro para obtener la fecha inicio de solicitud de informe */
	private static final String FCHSOLINF = "FCHSOLINF";
	
	/**
	 * Select para obtener la URL para la invocación del Web Service
	 *                  de Validación de la Línea de Captuara.
	 */
	private static final String SELECT_FECHA = new StringBuilder().append(
			"select VALOR_ENTERO %n").append("from STPROD.EWEB_PARAMETROS %n").append(
			"where NOMBRE_PARAM = '%s' %n").toString();
	
	/**Constante SQL**/
	private final static String QUERY_CATALOG_NOMINA = "SELECT A.CUENTA_ABONO CUENTA, B.NUM_EMPL CLAVE,  nvl(APELL_PATERNO,' ') PATERNO, NVL(APELL_MATERNO,' ') MATERNO, nvl(NOMBRE,' ') NOMBRE "
			+ "	FROM EWEB_CAT_NOM_EMPL@LWEBGFSS A, EWEB_DET_CAT_NOM@LWEBGFSS  B "
			+ "WHERE B.CONTRATO = '%s' AND B.CUENTA_ABONO = A.CUENTA_ABONO AND ESTATUS_CUENTA = 'A' "
			+ "	ORDER BY APELL_PATERNO ASC"; 
	
	/**Constante SQL**/
	private final static String QUERY_CATALOG_NOMINA_COMBO = "SELECT A.CUENTA_ABONO CUENTA, B.NUM_EMPL CLAVE,  nvl(APELL_PATERNO,' ') PATERNO, NVL(APELL_MATERNO,' ') MATERNO, nvl(NOMBRE,' ') NOMBRE "
		+ "	FROM EWEB_CAT_NOM_EMPL@LWEBGFSS A, EWEB_DET_CAT_NOM@LWEBGFSS  B "
		+ "WHERE B.CONTRATO = '%s' AND A.CUENTA_ABONO LIKE %s  and nvl(APELL_PATERNO,' ') || NVL(APELL_MATERNO,' ') || nvl(NOMBRE,' ') like %s  AND B.CUENTA_ABONO = A.CUENTA_ABONO AND ESTATUS_CUENTA = 'A' "
		+ "	ORDER BY APELL_PATERNO ASC"; 
	
	/** Constante SQL Brinco PDF **/
	private static final String SQL_INSERTA_BRINCO_PDF= "INSERT INTO STPROD.eweb_edocta_segu(LLAVE_DESCARGA, COD_CLTE, NUM_CUENTA, FORMATO, PERIODO, TIPO_EDO_CTA, FOLIO_UUID)"
													 + " values( '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
	/**
	 * constante para la literal " = '"
	 */
	private static final String IGUAL = " = '";
	/**
	 * Constructor
	 */
	public NominaDAO() {
		EIGlobal.mensajePorTrace("NominaDAO :: Entrando",	EIGlobal.NivelLog.INFO);
	}
	
/**
	 * Se ejecuta el select para obtener la FECHA Inicio para solicitud de informes de Nomina
	 * @return String con la Fecha
	 */
	public Date consultaFecha() {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String valor = "";
		Date fchPermitida = null;
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyyMMdd");
		
		String selectFull = String.format(SELECT_FECHA, FCHSOLINF);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaFecha :: Entrando - query [" + selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format("NominaDAO :: consultaFecha :: - QRY [%s]",selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("NominaDAO :: consultaFecha :: : " + FCHSOLINF + " Valor -> " + rs.getString(1), NivelLog.DEBUG);
				valor = rs.getString(1);
			}

			if(null != valor && "" != valor)
				fchPermitida = formatoDelTexto.parse(valor);
			else
				fchPermitida = new Date();

		} catch (ParseException ex) {
			EIGlobal.mensajePorTrace("***NominaDAO.class : ConsultaFecha : &Error en el pareso de fechas "+ex, EIGlobal.NivelLog.INFO);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"NominaDAO :: consultaFecha :: Error SQL al consultar la solicitud "+e.getMessage(),
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("NominaDAO :: consultaFecha ::- Error al cerrar conexion:[" + e1 + "]", NivelLog.ERROR);
			}
		}
		return fchPermitida;
	}
	
	
	/**
	 * Funcion de registro de solicitud de informe PDF
	 * @author  FSW-Vector
	 * @since   Marzo 2015
	 * @param solPDFBean filtros
	 * @return SolPDFBean bean con informacion
	 * @throws SQLException excepcion a manejar
	 */
	
	public SolPDFBean consultaSolicitudPDF(SolPDFBean solPDFBean)throws SQLException{
	
		SolPDFBean bean = null ;
		
		Connection conn = null;
		Statement st = null;		
		ResultSet rs = null;
		String query = "";
		EIGlobal.mensajePorTrace("NominaDAO :: consultaSolicitudPDF :: Entrando",	EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaSolicitudPDF :: Contrato     ["+ solPDFBean.getContrato() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaSolicitudPDF :: Secuencia    ["+ solPDFBean.getSecuencia() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaSolicitudPDF :: Tipo Nomina  ["+ solPDFBean.getTipoNomina() +"]" ,EIGlobal.NivelLog.INFO);
		
		query = QUERY_SELECT_SOL_PDF
				+ "WHERE "
				+  SQL_EPSN_FLD_CONTR    + IGUAL + solPDFBean.getContrato() + "' AND "
				+  SQL_EPSN_FLD_SEC      + " = " + solPDFBean.getSecuencia() + "  AND "
				+  SQL_EPSN_FLD_TIPO_NOM + IGUAL + solPDFBean.getTipoNomina() + "'";
		        
		EIGlobal.mensajePorTrace("NominaDAO :: consultaSolicitudPDF :: Query  ["+ query +"]" ,EIGlobal.NivelLog.DEBUG);
		
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			st = conn.createStatement();
			
			 rs = st.executeQuery(query);
			 
			 if(rs.next()){
				 bean = new SolPDFBean();
				 bean.setContrato(rs.getString(SQL_EPSN_FLD_CONTR));
				 bean.setSecuencia(rs.getInt(SQL_EPSN_FLD_SEC));
				 bean.setEstatus(rs.getString(SQL_EPSN_FLD_ESTATUS));
			 } 	
		} catch (SQLException e) {			
			EIGlobal.mensajePorTrace(
					"NominaDAO :: consultaSolicitudPDF :: Error SQL al consultar la solicitud "+e.getMessage(),
					EIGlobal.NivelLog.ERROR);
			throw e;
		} finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(st != null){
					st.close();
				}
				if(conn != null){
					conn.close();
					conn=null;
				}				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"NominaDAO :: consultaSolicitudPDF :: Error al cerrar las conexiones --"+e.getMessage(),
						EIGlobal.NivelLog.ERROR);
			}
		}
		
		EIGlobal.mensajePorTrace("NominaDAO :: consultaSolicitudPDF :: Saliendo  " + (bean != null), 	EIGlobal.NivelLog.INFO);		
		return bean;
	}
	
	/**
	 * 
	 * @author  FSW-Vector
	 * @since   Marzo 2015
	 * @param solPDFBean informacion a registrar
	 * @return boolean se registro bien o no
	 * @throws SQLException excepcion a manejar
	 */
	
	public boolean registraSolcitudPDF(SolPDFBean  solPDFBean)throws SQLException{
		
		Connection conn = null;
		PreparedStatement  ps = null;
		boolean resultado = false;
		
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Entrando",	EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Contrato     ["+ solPDFBean.getContrato() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Secuencia    ["+ solPDFBean.getSecuencia() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Tipo Nomina  ["+ solPDFBean.getTipoNomina() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Fch Pago     ["+ solPDFBean.getFchPago() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Usuario Alta ["+ solPDFBean.getUsuarioAlta() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Estatus      ["+ solPDFBean.getEstatus() +"]" ,EIGlobal.NivelLog.INFO);		
		
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Query  ["+ QUERY_INSERT_SOL_PDF +"]" ,EIGlobal.NivelLog.DEBUG);
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			ps = conn.prepareStatement(QUERY_INSERT_SOL_PDF);
			ps.setString(1, solPDFBean.getContrato());
			ps.setInt(2, solPDFBean.getSecuencia());
			ps.setString(3,  solPDFBean.getTipoNomina());
			ps.setDate(4, new java.sql.Date(solPDFBean.getFchPago().getTime()));
			ps.setString(5,  solPDFBean.getUsuarioAlta());
			ps.setString(6,  solPDFBean.getEstatus());
			
			 ps.execute();	
			conn.commit();
			resultado = true;
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"NominaDAO :: registraSolcitudPDF :: Error SQL al insertar la solicitud " + e.getMessage(),
					EIGlobal.NivelLog.ERROR);
			throw e;
		} finally{
			try {
				
				if(ps != null){
					ps.close();
				}
				if(conn != null){
					 conn.close();
					 conn = null;
				}				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"NominaDAO :: registraSolcitudPDF :: Error al cerrar las conexiones --" + e.getMessage(),
						EIGlobal.NivelLog.ERROR);
			}
		}
		
		EIGlobal.mensajePorTrace("NominaDAO :: registraSolcitudPDF :: Saliendo  " + resultado ,	EIGlobal.NivelLog.INFO);
		return resultado;
	}
	/**
	 * Funcion que recupera todos los comprobantes de PDF
	 * @author  FSW-Vector
	 * @since   Marzo 2015
	 * @param   solPDFBean filtros para la consulta
	 * @return HashMap< Integer,SolPDFBean> mapa con la informacion consultada
	 * @throws  SQLException excepcion a manejar
	 */
	
	public HashMap< Integer,SolPDFBean> consultaComprobantesPDF(SolPDFBean solPDFBean)throws SQLException{
	
		SolPDFBean bean = null ;
		
		Connection conn = null;
		Statement st = null;		
		ResultSet rs = null;
		String query = "";
		HashMap < Integer,SolPDFBean> solicitudesPDF  = null; 
		
		EIGlobal.mensajePorTrace("NominaDAO :: consultaComprobantesPDF :: Entrando",	EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaComprobantesPDF :: Contrato     ["+ solPDFBean.getContrato() +"]" ,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaComprobantesPDF :: Tipo Nomina  ["+ solPDFBean.getTipoNomina() +"]" ,EIGlobal.NivelLog.INFO);
		
		query = QUERY_SELECT_SOL_PDF
				+ "WHERE "
				+  SQL_EPSN_FLD_CONTR    + IGUAL + solPDFBean.getContrato() + "' AND "				
				+  SQL_EPSN_FLD_TIPO_NOM + IGUAL + solPDFBean.getTipoNomina() + "'"
		        +  " ORDER BY " + SQL_EPSN_FLD_SEC;
		EIGlobal.mensajePorTrace("NominaDAO :: consultaComprobantesPDF :: Query  ["+ query +"]" ,EIGlobal.NivelLog.INFO);		
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			st = conn.createStatement();
			
			 rs = st.executeQuery(query);
			 
			 solicitudesPDF = new HashMap< Integer, SolPDFBean>();
			 while(rs.next()){
				 bean = new SolPDFBean();
				 bean.setContrato(rs.getString(SQL_EPSN_FLD_CONTR));
				 bean.setSecuencia(rs.getInt(SQL_EPSN_FLD_SEC));
				 bean.setEstatus(rs.getString(SQL_EPSN_FLD_ESTATUS));
				 bean.setObservaciones(rs.getString(SQL_EPSN_FLD_OBS));				 
				 solicitudesPDF.put(bean.getSecuencia(),bean);
				 
				 EIGlobal.mensajePorTrace("NominaDAO :: consultaComprobantesPDF :: Contrato  ["+ bean.getContrato() +"]" + "Secuencia [" + bean.getSecuencia()+"]" ,EIGlobal.NivelLog.DEBUG);
			 }			 		
			
		} catch (SQLException e) {					
			EIGlobal.mensajePorTrace(
					"NominaDAO :: consultaComprobantesPDF :: Error SQL al consultar la solicitud "+e.getMessage(),
					EIGlobal.NivelLog.ERROR);
			throw e;

		} finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(st != null){
					st.close();
				}
				if(conn != null){
					conn.close();
					conn = null;
				}				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"NominaDAO :: consultaSolicitudPDF :: Error al cerrar las conexiones --",
						EIGlobal.NivelLog.ERROR);
			}
		}		
		EIGlobal.mensajePorTrace("NominaDAO :: consultaSolicitudPDF :: Saliendo  " + (bean != null), 	EIGlobal.NivelLog.INFO);		
		return solicitudesPDF;
	}
	
	/**
	 * Funcion que recupera un empleado en base al contrato
	 * @author  FSW-Vector
	 * @since   Marzo 2015
	 * @param   contrato filtro para la consulta
	 * @return HashMap< String, ArchivoNominaDetBean> mapa con la informacion consultada
	 * @throws  SQLException excepcion a manejar
	 */
	public HashMap< String, ArchivoNominaDetBean> consultaCtasCatalogo(String  contrato)throws SQLException{
		
		ArchivoNominaDetBean bean = null ;
		
		Connection conn = null;
		Statement st = null;		
		ResultSet rs = null;
		String query = "";
		HashMap < String,ArchivoNominaDetBean> cuentasCatalogo  = null; 
		
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Entrando",	EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Contrato     ["+ contrato +"]" ,EIGlobal.NivelLog.INFO);
		
		query = String.format(QUERY_CATALOG_NOMINA, contrato);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Query  ["+ query +"]" ,EIGlobal.NivelLog.INFO);		
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			st = conn.createStatement();
			
			 rs = st.executeQuery(query);
			 
			 cuentasCatalogo = new HashMap< String, ArchivoNominaDetBean>();
			 while(rs.next()){
				 bean = new ArchivoNominaDetBean();
				 bean.setCuentaAbono(rs.getString("CUENTA").trim());
				 bean.setIdEmpleado(rs.getString("CLAVE").trim());
				 bean.setAppPaterno(rs.getString("PATERNO").trim());
				 bean.setAppMaterno(rs.getString("MATERNO").trim());
				 bean.setNombre(rs.getString("NOMBRE").trim());
				 cuentasCatalogo.put(bean.getCuentaAbono(),bean);				 
				 EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Contrato  ["+ contrato +"]" + "Secuencia [" + bean.getCuentaAbono()+"]" ,EIGlobal.NivelLog.DEBUG);
			 }			 		
			
		} catch (SQLException e) {					
			EIGlobal.mensajePorTrace(
					"NominaDAO :: consultaCtasCatalogo :: Error SQL al consultar la solicitud "+e.getMessage(),
					EIGlobal.NivelLog.ERROR);
			throw e;
		} finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(st != null){
					st.close();
				}
				if(conn != null){
					conn.close();
					conn = null;
				}				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"NominaDAO :: consultaCtasCatalogo :: Error al cerrar las conexiones --"+e.getMessage(),
						EIGlobal.NivelLog.ERROR);
			}
		}		
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Saliendo  " + (bean != null), 	EIGlobal.NivelLog.INFO);		
		return cuentasCatalogo;
	}
	
	/**
	 * Consulta de empleados
	 * @since 18/04/2015
	 * @param contrato contrato en sesion
	 * @param cuenta filtro cuenta cargo
	 * @param descripcion descripcion cuenta
	 * @return Listado de cuentas empleado que coincidan con el filtro.
	 * @throws SQLException Excepcion SQL
	 */
	public List< ArchivoNominaDetBean> consultaCtasCatalogo(String  contrato, String  cuenta, String  descripcion)throws SQLException{
		
		ArchivoNominaDetBean bean = null ;
		
		Connection conn = null;
		Statement st = null;		
		ResultSet rs = null;
		String query = "";
		List < ArchivoNominaDetBean> cuentasCatalogo  = null; 
		
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Entrando",	EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Contrato     ["+ contrato +"]" ,EIGlobal.NivelLog.INFO);
		
		query = String.format(QUERY_CATALOG_NOMINA_COMBO, contrato, "'%" + cuenta + "%'" + " || '%' ", "'%" + descripcion + "%'" + " || '%' ");
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Query  ["+ query +"]" ,EIGlobal.NivelLog.INFO);		
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			st = conn.createStatement();
			
			 rs = st.executeQuery(query);
			 
			 cuentasCatalogo = new ArrayList< ArchivoNominaDetBean>();
			 while(rs.next()){
				 bean = new ArchivoNominaDetBean();
				 bean.setCuentaAbono(rs.getString("CUENTA").trim());
				 bean.setIdEmpleado(rs.getString("CLAVE").trim());
				 bean.setAppPaterno(rs.getString("PATERNO").trim());
				 bean.setAppMaterno(rs.getString("MATERNO").trim());
				 bean.setNombre(rs.getString("NOMBRE").trim());
				 cuentasCatalogo.add(bean);				 
				 EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Contrato  ["+ contrato +"]" + "Secuencia [" + bean.getCuentaAbono()+"]" ,EIGlobal.NivelLog.DEBUG);
			 }			 		
			
		} catch (SQLException e) {					
			EIGlobal.mensajePorTrace(
					"NominaDAO :: consultaCtasCatalogo :: Error SQL al consultar la solicitud "+e.getMessage(),
					EIGlobal.NivelLog.ERROR);
			throw e;
		} finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(st != null){
					st.close();
				}
				if(conn != null){
					conn.close();
					conn = null;
				}				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"NominaDAO :: consultaCtasCatalogo :: Error al cerrar las conexiones --",
						EIGlobal.NivelLog.ERROR);
			}
		}		
		EIGlobal.mensajePorTrace("NominaDAO :: consultaCtasCatalogo :: Saliendo  " + (bean != null), 	EIGlobal.NivelLog.INFO);		
		return cuentasCatalogo;
	}
	
	/**
	 * Inserta Registro Descarga PDF
	 * @param cuentasDescargaBean : cuentasDescargaBean 
	 * @return int : Numero de Registros insertados
	 * @throws SQLException : Manejo de la Excepcion.
	 */
	public int insertaRegistroDescargaPDF(CuentasDescargaBean cuentasDescargaBean) throws SQLException {
		int registrosInsertados=0;
		Connection conn=null;
		PreparedStatement pst=null;
		EIGlobal.mensajePorTrace("NominaDAO :: insertaRegistroDescargaPDF :: Entrando",EIGlobal.NivelLog.INFO	);
		EIGlobal.mensajePorTrace("NominaDAO :: insertaRegistroDescargaPDF :: Llave " +	cuentasDescargaBean.getLlaveDescarga(), EIGlobal.NivelLog.INFO);
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			conn.setAutoCommit(false);
			String query=String.format(SQL_INSERTA_BRINCO_PDF,cuentasDescargaBean.getLlaveDescarga(),
															cuentasDescargaBean.getCodigoCliente(),
															cuentasDescargaBean.getCuenta(),
															cuentasDescargaBean.getTipo(),
															cuentasDescargaBean.getPeriodoSeleccionado(),
															cuentasDescargaBean.getTipoEdoCta(),															
															cuentasDescargaBean.getFolioSeleccionado());
			pst = conn.prepareStatement(query);
			EIGlobal.mensajePorTrace("NominaDAO :: insertaRegistroDescargaPDF :: insertaBrincoPDF %s " + query, NivelLog.INFO);
			registrosInsertados=pst.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("NominaDAO :: insertaRegistroDescargaPDF :: Ocurrio un error en insertaRegistroDescargaPDF " + e.toString(),NivelLog.DEBUG);
			throw e;
		} finally {
			try {
				pst.close();
				conn.close();				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("NominaDAO :: insertaRegistroDescargaPDF :: Error al cerrar objeto :"  + e.toString(),NivelLog.DEBUG);
		}
		}		
		EIGlobal.mensajePorTrace("NominaDAO :: insertaRegistroDescargaPDF :: Termina ejecucion de metodo insertaRegistroDescargaPDF()", NivelLog.INFO);
		return registrosInsertados;
	}

}