package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.HashMap;
import java.util.Hashtable;

import mx.altec.enlace.bo.EmailCelularBean;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;

public class EmailCelularDAO {
	public static final String HEADERODB5 = "ODB501001123451O00N2";
	public static final String HEADERODB6 = "ODB601201123451O00N2";

	public EmailCelularBean consultaCelular(String persona, String uso) {

		EmailCelularBean emailCelularBean = new EmailCelularBean();

		StringBuffer trama = new StringBuffer();
		String respuesta = "";
		MQQueueSession mqsesion = null;
		
		try {
			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB5)
				.append("C").append(rellenar(persona,8,'0','I')).append(rellenar(uso, 3));

			EIGlobal.mensajePorTrace("Trama para ODB5 ->" + trama.toString(), EIGlobal.NivelLog.INFO);

			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			EIGlobal.mensajePorTrace("Respuesta de ODB5 para consulta ->" + respuesta, EIGlobal.NivelLog.INFO);

			emailCelularBean = desentramaODB5(respuesta);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR LA CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return emailCelularBean;
	}

	public EmailCelularBean consultaCorreo(String persona, String uso) {

		EmailCelularBean emailCelularBean = new EmailCelularBean();

		StringBuffer trama = new StringBuffer();
		String respuesta = "";
		
		MQQueueSession mqsesion = null;
		
		try {
			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB6)
				.append("C").append(rellenar(persona,8,'0','I')).append(rellenar(uso, 3));

			EIGlobal.mensajePorTrace("Trama para ODB6 ->" + trama.toString(), EIGlobal.NivelLog.INFO);

			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			EIGlobal.mensajePorTrace("Respuesta de ODB6 para consulta ->" + respuesta, EIGlobal.NivelLog.INFO);

			emailCelularBean = desentramaODB6(respuesta);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return emailCelularBean;
	}

	public EmailCelularBean consultaCelular(String persona) {

		EmailCelularBean emailCelularBean = new EmailCelularBean();

		StringBuffer trama = new StringBuffer();
		String respuesta = "";
		MQQueueSession mqsesion = null;
		
		try {
			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB5)
				.append("C").append(rellenar(persona,8,'0','I')).append("003");

			EIGlobal.mensajePorTrace("Trama para ODB5 ->" + trama.toString(), EIGlobal.NivelLog.INFO);
			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			EIGlobal.mensajePorTrace("Respuesta de ODB5 para consulta ->" + respuesta, EIGlobal.NivelLog.INFO);

			emailCelularBean = desentramaODB5(respuesta);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return emailCelularBean;
	}

	public EmailCelularBean consultaCorreo(String persona) {

		EmailCelularBean emailCelularBean = new EmailCelularBean();

		StringBuffer trama = new StringBuffer();
		String respuesta = "";

		MQQueueSession mqsesion = null;
		
		try {
			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB6)
				.append("C").append(rellenar(persona,8,'0','I')).append("003");

			EIGlobal.mensajePorTrace("Trama para ODB6 ->" + trama.toString(), EIGlobal.NivelLog.INFO);

			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			EIGlobal.mensajePorTrace("Respuesta de ODB6 para consulta ->" + respuesta, EIGlobal.NivelLog.INFO);

			emailCelularBean = desentramaODB6(respuesta);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return emailCelularBean;
	}

	public EmailCelularBean desentramaODB5(String respuesta) {

		EmailCelularBean emailCelularBean = new EmailCelularBean();
		int posicion = 0;

		if (respuesta.contains("ODA0002")) {
			posicion = respuesta.indexOf("PC")+13;
			emailCelularBean.setLada(respuesta.substring(posicion, posicion+7).trim());
			emailCelularBean.setNoCelular(respuesta.substring(posicion+7, posicion+15).trim());
			emailCelularBean.setCarrier(respuesta.substring(posicion+15, posicion+30).trim());
			emailCelularBean.setTimeStampCel(respuesta.substring(posicion+30, posicion+56).trim());
		}

		if (respuesta.length() >= 17) {
			emailCelularBean.setEstatus(respuesta.substring(10, 17));
		} else {
			emailCelularBean.setEstatus("Error");
		}

		EIGlobal.mensajePorTrace("Estatus de ODB5 ->" + emailCelularBean.getEstatus(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Lada de ODB5 ->" + emailCelularBean.getLada(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("NoCelular de ODB5 ->" + emailCelularBean.getNoCelular(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Carrier de ODB5 ->" + emailCelularBean.getCarrier(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("TimeStampCel de ODB5 ->" + emailCelularBean.getTimeStampCel(), EIGlobal.NivelLog.DEBUG);

		return emailCelularBean;
	}

	public EmailCelularBean desentramaODB6(String respuesta) {

		EmailCelularBean emailCelularBean = new EmailCelularBean();
		int posicion = 0;

		if (respuesta.contains("ODA0002")) {
			posicion = respuesta.indexOf("PC")+13;

			emailCelularBean.setCorreo(respuesta.substring(posicion, posicion+50).trim());
			emailCelularBean.setTimeStampMail(respuesta.substring(posicion+50, posicion+76).trim());
		}

		if (respuesta.length() >= 17) {
			emailCelularBean.setEstatus(respuesta.substring(10, 17));
		} else {
			emailCelularBean.setEstatus("Error");
		}

		EIGlobal.mensajePorTrace("Estatus de ODB6 ->" + emailCelularBean.getEstatus(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Correo de ODB6 ->" + emailCelularBean.getCorreo(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("TimeStampMail de ODB6 ->" + emailCelularBean.getTimeStampMail(), EIGlobal.NivelLog.DEBUG);

		return emailCelularBean;
	}

	public String actualizaCelular(EmailCelularBean emailCelularBean, String persona) {
		String lada = "";
		String celular = "";
		String carrier = "";
		String respuesta = "";
		String estatus = "Error";

		StringBuffer trama = new StringBuffer();
		MQQueueSession mqsesion = null;
		
		EIGlobal.mensajePorTrace("Persona->" + persona, EIGlobal.NivelLog.INFO);
		try {
			persona = rellenar(persona,8,'0','I');
			lada = rellenar(emailCelularBean.getLada(), 7);
			celular = rellenar(emailCelularBean.getNoCelular(), 8);
			carrier = rellenar(emailCelularBean.getCarrier(), 15);

			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB5)
			.append("M").append(persona).append("003").append(lada).append(celular).append(carrier);
			//.append("M").append(persona).append("004").append(lada).append(celular).append(carrier);

			EIGlobal.mensajePorTrace("Trama para ODB5 ->" + trama.toString(), EIGlobal.NivelLog.INFO);
			//CSA
			mqsesion =new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST); 
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			if (respuesta.contains("ODA0007")) {
				estatus = "Alta exitosa";
			}
			if (respuesta.contains("ODA0008")) {
				estatus = "Modificacion exitosa";
			}
			if (respuesta.contains("ODE0079")) {
				estatus = "Persona Inexistente";
			}

			EIGlobal.mensajePorTrace("Respuesta de ODB5 para modificacion ->" + respuesta, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Estatus en la modificacion de ODB5 ->" + estatus, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return estatus;
	}

	public String actualizaCorreo(EmailCelularBean emailCelularBean, String persona) {
		String correo = "";
		String respuesta = "";
		String estatus = "Error";

		StringBuffer trama = new StringBuffer();

		MQQueueSession mqsesion = null;
		try {
			persona = rellenar(persona,8,'0','I');
			correo = rellenar(emailCelularBean.getCorreo(), 50);

			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB6)
			.append("M").append(persona).append("003").append(correo);
			//.append("M").append(persona).append("004").append(correo);

			EIGlobal.mensajePorTrace("Trama para ODB6 ->" + trama.toString(), EIGlobal.NivelLog.INFO);

			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			if (respuesta.contains("ODA0007")) {
				estatus = "Alta exitosa";
			}
			if (respuesta.contains("ODA0008")) {
				estatus = "Modificacion exitosa";
			}
			if (respuesta.contains("ODE0079")) {
				estatus = "Persona Inexistente";
			}

			EIGlobal.mensajePorTrace("Respuesta de ODB6 para modificacion ->" + respuesta, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Estatus en la modificacion de ODB6 ->" + estatus, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return estatus;
	}

	public String actualizaCelular(EmailCelularBean emailCelularBean, String persona, String tipoOp) {
		String lada = "";
		String celular = "";
		String carrier = "";
		String respuesta = "";
		String estatus = "Error";

		StringBuffer trama = new StringBuffer();
		//CSA
		MQQueueSession mqsesion = null;
		
		EIGlobal.mensajePorTrace("Persona->" + persona, EIGlobal.NivelLog.INFO);
		try {
			persona = rellenar(persona,8,'0','I');
			lada = rellenar(emailCelularBean.getLada(), 7);
			celular = rellenar(emailCelularBean.getNoCelular(), 8);
			carrier = rellenar(emailCelularBean.getCarrier(), 15);

			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB5)
			.append(tipoOp).append(persona).append("003").append(lada).append(celular).append(carrier);

			EIGlobal.mensajePorTrace("Trama para ODB5 ->" + trama.toString(), EIGlobal.NivelLog.INFO);
			//CSA
			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			if (respuesta.contains("ODA0007")) {
				estatus = "Alta exitosa";
			}
			if (respuesta.contains("ODA0008")) {
				estatus = "Modificacion exitosa";
			}
			if (respuesta.contains("ODE0079")) {
				estatus = "Persona Inexistente";
			}

			EIGlobal.mensajePorTrace("Respuesta de ODB5 para modificacion ->" + respuesta, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Estatus en la modificacion de ODB5 ->" + estatus, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
					mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return estatus;
	}

	public String actualizaCorreo(EmailCelularBean emailCelularBean, String persona, String tipoOp) {
		String correo = "";
		String respuesta = "";
		String estatus = "Error";

		StringBuffer trama = new StringBuffer();
		MQQueueSession mqsesion = null;
		
		try {
			persona = rellenar(persona,8,'0','I');
			correo = rellenar(emailCelularBean.getCorreo(), 50);

			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8)).append(HEADERODB6)
			.append(tipoOp).append(persona).append("003").append(correo);

			EIGlobal.mensajePorTrace("Trama para ODB6 ->" + trama.toString(), EIGlobal.NivelLog.INFO);

			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

			if (respuesta.contains("ODA0007")) {
				estatus = "Alta exitosa";
			}
			if (respuesta.contains("ODA0008")) {
				estatus = "Modificacion exitosa";
			}
			if (respuesta.contains("ODE0079")) {
				estatus = "Persona Inexistente";
			}

			EIGlobal.mensajePorTrace("Respuesta de ODB6 para modificacion ->" + respuesta, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Estatus en la modificacion de ODB6 ->" + estatus, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return estatus;
	}


	public static HashMap consultaCarrier() {
		StringBuffer trama = new StringBuffer();
		String respuesta = "";
		String [] arrayDatos = null;
		String resultado = "";
		String clave = "";
		String valor = "";
		int posicion = 0;

		HashMap hm = new HashMap();

		MQQueueSession mqsesion = null;
		
		try {
			trama.append(rellenar(NP_MQ_TERMINAL,4)).append(rellenar(ADMUSR_MQ_USUARIO,8))
					.append("TCGG00881123451O00N212070014E                       4");

			EIGlobal.mensajePorTrace("Trama para TCGG ->" + trama.toString(), EIGlobal.NivelLog.INFO);

			mqsesion = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqsesion.enviaRecibeMensaje(trama.toString());

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			if(mqsesion!=null){
				try{
				mqsesion.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		EIGlobal.mensajePorTrace("Respuesta de TCGG en consulta ->" + respuesta, EIGlobal.NivelLog.INFO);

		if (respuesta.contains("TCA0006")) {
			hm.put("RES", "CORRECTO");
			arrayDatos = respuesta.split("@");
			if (arrayDatos!=null && arrayDatos.length>0) {
				for (int aux=0; aux<arrayDatos.length-1; aux++) {
					resultado = arrayDatos[aux];
					if (resultado.indexOf("P12070014E")>0) {
						posicion = resultado.indexOf("P12070014E");
						clave = resultado.substring(posicion+10, posicion+13).trim();
						valor = resultado.substring(posicion+13, resultado.length()-1).trim();
						hm.put(clave, valor);
					}
				}
			}

		} else {
			hm.put("RES", "INCORRECTO");
		}

		EIGlobal.mensajePorTrace("Keys de TCGG ->" + hm.keySet(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Valores de TCGG ->" + hm.values(), EIGlobal.NivelLog.DEBUG);

		return hm;
	}

}