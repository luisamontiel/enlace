package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmTrxB485;
import mx.altec.enlace.beans.AdmTrxPE80;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase de conexiones para manejar el procedimiento para la transaccion B485
 *
 * @author Fernando Gurrola Herrera
 * @version 1.0 Sep 27, 2010
 */
public class TrxB485DAO extends AdmonUsuariosMQDAO {

	/**
	 * Metodo encargado de generar los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion B485
	 * @param numCuenta			Numero de cuenta
	 * @return AdmTrxB485		Instancia de tipo AdmTrxB485
	 */
	private AdmTrxB485 ejecutaB485(String numCuenta) {

		StringBuffer sb = new StringBuffer();
		sb.append(numCuenta);

    	EIGlobal.mensajePorTrace("TrxB485DAO::ejecutaB485:: Armando trama:" + sb.toString()
    			, EIGlobal.NivelLog.DEBUG);

		return consulta(AdmTrxB485.HEADER, sb.toString(), AdmTrxB485.getFactoryInstance());
	}

	/**
	 * Metodo encargado de recibir los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion B485
	 * @param numCuenta			Numero de cuenta
	 * @return AdmTrxB485		Instancia de tipo AdmTrxB485
	 */
	public AdmTrxB485 claveBancariaEstandarizada(String numCuenta) {
    	EIGlobal.mensajePorTrace("TrxB485DAO::claveBancariaEstandarizada:: Llegando a metodo claveBancariaEstandarizada"
    			, EIGlobal.NivelLog.DEBUG);
		return ejecutaB485(numCuenta);
	}


}