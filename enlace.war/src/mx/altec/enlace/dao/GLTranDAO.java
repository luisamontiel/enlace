package mx.altec.enlace.dao;
/**
 *
 * @author Z096114 Emmanuel Sanchez Castillo
 * @version 1.0
 * @Fecha 12 Noviembre 2010
 * @proyecto 201001500 - EBEE (Estrategia de banca electronica Enlace
 *
 */
import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.GL51Tran;
import mx.altec.enlace.beans.GL52Tran;
import mx.altec.enlace.beans.GLFacultad;
import mx.altec.enlace.beans.GLFacultadCta;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;

import mx.altec.enlace.utilerias.EIGlobal;

public class GLTranDAO {

	private final String CODERR_SINDATOS = "GL0999";
	private final String MENSAJE_SINDATOS = "SIN DATOS DE RESPUESTA";
	public final String CODTRAN_OK = "11";
	
	public static final String TRAN_CON_FAC ="GL50";
	public static final String TRAN_CON_FAC_CTA ="GL63";

	public static final String FORMATO_GL50_DATOS = "@DCGLM5020 P";
	public static final String FORMATO_GL50_PAG = "@DCGLM5030 P";

	public static final String FORMATO_GL63_DATOS = "@DCGLM6320 P";
	public static final String FORMATO_GL63_PAG = "@DCGLM6330 P";


	public static final String CODRES_OK = "GLA8000";
	public static final String CODRES_OK_PAG = "GLA8001";
	public static final String CODRES_OK_SIN_DATOS = "GLA0008";
	public static final String INDICADOR_PAG = "SI";

	public boolean consultaFacultad(GL51Tran glFacultad)throws Exception {

		EIGlobal.mensajePorTrace("Entra consultaFacultad", EIGlobal.NivelLog.INFO);

		boolean result = false;
		String respuesta = null;
		StringBuffer tramaBuf = null;
		String trama = null;

		try {

			tramaBuf = new StringBuffer();

			tramaBuf.append(rellenar(glFacultad.getContrato() , 20))
			.append(rellenar(glFacultad.getUsuario(),8))
			.append(rellenar(glFacultad.getNivel(),1))
			.append(rellenar(glFacultad.getAgrupacion(),3))
			.append(rellenar(glFacultad.getIndicadorPag(),2))
			.append(rellenar(glFacultad.getFacultadPag(),16))
			.append(rellenar(glFacultad.getUsuario(),8));

			trama = creaTrama(NP_MQ_TERMINAL, ADMUSR_MQ_USUARIO, TRAN_CON_FAC, tramaBuf.toString());

			EIGlobal.mensajePorTrace("Configuracion JMS NP: " + NP_JNDI_CONECTION_FACTORY + ", " + NP_JNDI_QUEUE_REQUEST + ", " + NP_JNDI_QUEUE_RESPONSE, EIGlobal.NivelLog.INFO);

			respuesta = new MQQueueSession(
					NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE,
					NP_JNDI_QUEUE_REQUEST)
					.enviaRecibeMensaje(trama);

		} catch (Exception e) {

			EIGlobal.mensajePorTrace("Error NomPreMQDAO: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}

		if (respuesta != null && respuesta.length() > 0) {

			EIGlobal.mensajePorTrace("RESPUESTA ["  + respuesta + "]", EIGlobal.NivelLog.INFO );

			parseTramaFacultad(respuesta, glFacultad);

		} else {

			EIGlobal.mensajePorTrace("Sin trama de respuesta", EIGlobal.NivelLog.INFO);
		}

		EIGlobal.mensajePorTrace("Sale consultaFacultad", EIGlobal.NivelLog.INFO);
		return result;
	}

	public boolean consultaFacultadCta(GL52Tran glFacultadCta) throws Exception{

		EIGlobal.mensajePorTrace("Entra consultaFacultadCta", EIGlobal.NivelLog.INFO);


		boolean result = false;
		String respuesta = null;
		StringBuffer tramaBuf = null;
		String trama = null;

		try {

		tramaBuf = new StringBuffer();

			tramaBuf.append(rellenar(glFacultadCta.getContrato() , 20))
			.append(rellenar(glFacultadCta.getUsuario(),8))
			.append(rellenar(glFacultadCta.getIndicadorPag(),2))			
			.append(rellenar(glFacultadCta.getFacultadPag(),16))
			.append(rellenar(glFacultadCta.getUsuario(),8));

			trama = creaTrama(NP_MQ_TERMINAL, ADMUSR_MQ_USUARIO, TRAN_CON_FAC_CTA, tramaBuf.toString());

			EIGlobal.mensajePorTrace("Configuracion JMS NP: " + NP_JNDI_CONECTION_FACTORY + ", " + NP_JNDI_QUEUE_REQUEST + ", " + NP_JNDI_QUEUE_RESPONSE, EIGlobal.NivelLog.INFO);

			respuesta = new MQQueueSession(
					NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE,
					NP_JNDI_QUEUE_REQUEST)
					.enviaRecibeMensaje(trama.toString());

		} catch (Exception e) {

			EIGlobal.mensajePorTrace("Error NomPreMQDAO: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}

		if (respuesta != null && respuesta.length() > 0) {

			EIGlobal.mensajePorTrace("RESPUESTA ["  + respuesta + "]", EIGlobal.NivelLog.INFO );

			parseTramaFacultadCta(respuesta, glFacultadCta);

		} else {

			EIGlobal.mensajePorTrace("Sin trama de respuesta", EIGlobal.NivelLog.INFO);
		}

		EIGlobal.mensajePorTrace("Sale consultaFacultadCta", EIGlobal.NivelLog.INFO);

		return result;
	}

	public void parseTramaFacultad(String trama,GL51Tran glFacultad)throws Exception{

		EIGlobal.mensajePorTrace("Entra parseTramaFacultad", EIGlobal.NivelLog.INFO);
		System.out.println("TRAMA ENTRADA [" + trama + "]");


		String []arrayTrama;
		String tramaDatos;
		//CompaniasVO tmpVO = null;
		if(trama != null){
			trama.trim();
			if("".equals(trama)){

				glFacultad.setCodMsg(CODERR_SINDATOS);
				glFacultad.setMensaje(MENSAJE_SINDATOS);

			}else {
				arrayTrama = trama.split("[@]");
				if(arrayTrama != null && arrayTrama.length >= 3){

					glFacultad.setCodMsg(arrayTrama[2].substring(2,9));
					glFacultad.setMensaje( arrayTrama[2].substring(10,arrayTrama[2].length()-1));

					//System.out.println("COD ERROR [" + getCodError() + "]" + "[" + getMensajeError()  + "]" );
					EIGlobal.mensajePorTrace("COD ERROR [" +glFacultad.getCodMsg() + "]" +
							"COD ERROR [" +glFacultad.getMensaje() + "]"
							,EIGlobal.NivelLog.INFO);

					if(arrayTrama[1].indexOf(CODTRAN_OK) != -1){

						if(glFacultad.getCodMsg().equals(CODRES_OK) ||
						   glFacultad.getCodMsg().equals(CODRES_OK_PAG)){

							String arrayDatos [] = trama.split(FORMATO_GL50_DATOS);


							GLFacultad facultad ;
							for(int i = 1; i < arrayDatos.length; i++){
								tramaDatos = arrayDatos[i];
								facultad = new GLFacultad();
								facultad.setClaveFacultad(tramaDatos.substring(0,16).trim());

								EIGlobal.mensajePorTrace("CLAVE FAC [" +  facultad.getClaveFacultad() +  "]",EIGlobal.NivelLog.INFO);
								glFacultad.getFacultades().add(facultad);
							}
						}
					}

					if(glFacultad.getCodMsg().equals(CODRES_OK_PAG)){
						EIGlobal.mensajePorTrace("HAY MAS DATOS HACER REPAGINADO [" + trama + "]",EIGlobal.NivelLog.INFO);
						 String arrayDatosPag [] = trama.split(FORMATO_GL50_PAG);

						 if(arrayDatosPag.length > 0){
							 tramaDatos = arrayDatosPag[1];
							 EIGlobal.mensajePorTrace("DATOS [" + tramaDatos+ "]",EIGlobal.NivelLog.DEBUG);
							 glFacultad.setIndicadorPag(INDICADOR_PAG);
							 glFacultad.setFacultadPag(tramaDatos.substring(0,16));
						 }

					}

				}else{
					System.out.println("RESPUESTA " + trama );
				}
			}
		}

		EIGlobal.mensajePorTrace("Sale parseTramaFacultad", EIGlobal.NivelLog.INFO);
	}

	public void parseTramaFacultadCta(String trama,GL52Tran glFacultadCta)throws Exception{

		EIGlobal.mensajePorTrace("Entra parseTramaFacultadCta", EIGlobal.NivelLog.INFO);
		System.out.println("TRAMA ENTRADA [" + trama + "]");
		
		glFacultadCta.setCodMsg("");
		glFacultadCta.setMensaje("");


		String []arrayTrama;
		String tramaDatos;
		//CompaniasVO tmpVO = null;
		if(trama != null){
			trama.trim();
			if("".equals(trama)){

				glFacultadCta.setCodMsg(CODERR_SINDATOS);
				glFacultadCta.setMensaje(MENSAJE_SINDATOS);

			}else {
				arrayTrama = trama.split("[@]");
				if(arrayTrama != null && arrayTrama.length >= 3){

					glFacultadCta.setCodMsg(arrayTrama[2].substring(2,9));
					glFacultadCta.setMensaje( arrayTrama[2].substring(10,arrayTrama[2].length()-1));

					EIGlobal.mensajePorTrace("COD ERROR [" +glFacultadCta.getCodMsg() + "]" +
							"COD ERROR [" +glFacultadCta.getMensaje() + "]"
							,EIGlobal.NivelLog.INFO);


					//System.out.println("COD ERROR [" + getCodError() + "]" + "[" + getMensajeError()  + "]" );

					if(arrayTrama[1].indexOf(CODTRAN_OK) != -1){

						if(glFacultadCta.getCodMsg().equals(CODRES_OK) ||
								glFacultadCta.getCodMsg().equals(CODRES_OK_PAG)){

							String arrayDatos [] = trama.split(FORMATO_GL63_DATOS);


							GLFacultadCta facultadCta ;
							for(int i = 1; i < arrayDatos.length; i++){
								tramaDatos = arrayDatos[i];
								facultadCta = new GLFacultadCta();
								
								facultadCta.setClaveFacultad(tramaDatos.substring(0,16).trim());					


								EIGlobal.mensajePorTrace( "CLAVE FAC [" +  facultadCta.getClaveFacultad() +  "]"
										,EIGlobal.NivelLog.INFO);


								glFacultadCta.getFacultadesCta().add(facultadCta);
							}

							if(glFacultadCta.getCodMsg().equals(CODRES_OK_PAG)){
								 String arrayDatosPag [] = trama.split(FORMATO_GL63_PAG);
								 if(arrayDatosPag.length > 0){
									 tramaDatos = arrayDatosPag[1];
									 glFacultadCta.setIndicadorPag(INDICADOR_PAG);									 
									 glFacultadCta.setFacultadPag(tramaDatos.substring(0,16));
								 }
							}
						}
					}

				}else{
					System.out.println("RESPUESTA " + trama );
				}
			}
		}

		EIGlobal.mensajePorTrace("Sale parseTramaFacultadCta", EIGlobal.NivelLog.INFO);
	}

	public static String creaTrama(String terminal,
			String usuario,String transaccion,String trama)
	{
		StringBuffer tramaBuffer = new StringBuffer();
		tramaBuffer.append(rellenar(terminal, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(usuario, 8, ' ', 'D'));
		tramaBuffer.append(rellenar(transaccion, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(Integer.toString(trama.length() + 32), 4, '0', 'I'));
		tramaBuffer.append("1123451O00N2");
		tramaBuffer.append(trama);

		return tramaBuffer.toString();
	}


}