package mx.altec.enlace.dao;



import mx.altec.enlace.beans.TrxOD13Bean;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * DAO que efect&uacute;a transacciones mediante la OD13
 * @author Armando Montoya Hernandez
 * @since 29/07/2014
 * */
public class TrxOD13DAO extends GenericDAO {

//	public static final String HEADEROD13 		= "OD1300431000011O00N2";
	/**Encabezado de la trama de env&iacute;o*/
	public static final String HEADEROD13 		= "OD1300431123451O00N2";
	/**Caracter de inicio de trama para env&iacute;o*/
	public static final String INICIO0D13 		= " ";
	/**Literal requerida por la trama para hacer la consulta por n&uacute;mero m&oacute;vil*/
	public static final String LITERAL 			= "R";
	/**Indicador de error en la ejecuci&oacute;n de la trama*/
	private static final String ERROR 			= "ER";
	/**Indicador de &eacute;xito en la ejecuci&oacute;n de la trama*/
	private static final String EXITO 			= "AV";

	/**
	 * Verifica si existe la informaci&oacute;n de la cuenta relacionada al n&uacute;mero m&oacute;vil
	 * @param numeroMovil 	N&uacute;mero m&oacute;vil de M&eacute;xico a 10 d&iacute;gitos
	 * @return 				Instancia de TrxOD13Bean con los valores de error dentro del bean
	 * */
	public TrxOD13Bean ejecutaConsulta(String numeroMovil){

		StringBuffer cadena= new StringBuffer()
		.append(HEADEROD13)
		.append(numeroMovil)
		.append(LITERAL);

        String respuesta= null;

		EIGlobal.mensajePorTrace("TrxOD13DAO - armaTrama(): [" +cadena.toString()+"]", EIGlobal.NivelLog.INFO);
		respuesta = invocarTransaccion(cadena.toString());
		EIGlobal.mensajePorTrace("TrxOD13DAO - armaTrama() - RESULTADO TRAMA : [" + respuesta+"]",EIGlobal.NivelLog.INFO);
		return desentramaOD13(respuesta);
	}

	/**
	 * Verifica si existe la informaci&oacute;n de la cuenta relacionada al n&uacute;mero m&oacute;vil
	 * @param numeroMovil 	N&uacute;mero m&oacute;vil de M&eacute;xico a 10 d&iacute;gitos
	 * @return 				Instancia de TrxOD13Bean con los valores de error dentro del bean
	 * */
	public String ejecutaConsultaDescripcion(String numeroMovil){

		StringBuffer cadena= new StringBuffer()
		.append(HEADEROD13)
		.append(numeroMovil)
		.append(LITERAL);

        String respuesta= null;

		EIGlobal.mensajePorTrace("TrxOD13DAO - armaTrama(): [" +cadena.toString()+"]", EIGlobal.NivelLog.INFO);
		respuesta = invocarTransaccion(cadena.toString());
		EIGlobal.mensajePorTrace("TrxOD13DAO - armaTrama() - RESULTADO TRAMA : [" + respuesta+"]",EIGlobal.NivelLog.INFO);
		return desentramaOD13Descripcion(respuesta);
	}

	/**
	 * Realiza el desentramado de la respuesta de la OD13 proveniente de 390
	 * @param trama		La trama que proviene de 390
	 * @return 			Instancia de TrxOD13Bean con los valores de error dentro del bean
	 * */
	 private TrxOD13Bean desentramaOD13(String trama){
		 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Inicio", EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Validando Trama", EIGlobal.NivelLog.INFO);
		 TrxOD13Bean oTrama = new TrxOD13Bean();
		 if( "".equals(trama) || null == trama ){
			 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Trama Erronea: ", EIGlobal.NivelLog.INFO);
			 oTrama.setError(true);
			 oTrama.setMensaje("");
			 oTrama.setCuenta("");
			 oTrama.setDivisa("");
			 oTrama.setPersona("");
			 oTrama.setRfc("");
			 oTrama.setNombre("");
			 oTrama.setApPaterno("");
			 oTrama.setApMaterno("");
			 oTrama.setErrorMessage("Respuesta Vacia");
		 }
		 else{
			 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Trama valida, iniciando separacion", EIGlobal.NivelLog.INFO);

			 /**Separar la trama con el caracter arroba (@)*/
			 String[] splittedFields = trama.split("@");

			 /**Si ha sucedido algun error entonces en el elemento [2] deber�a encontrarse
			  * la informaci�n que permite identificarlo.*/
			 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Separacion de trama en "+splittedFields.length+" elementos", EIGlobal.NivelLog.INFO);
			 for(String item: splittedFields){
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Item de trama "+item, EIGlobal.NivelLog.INFO);
			 }
			 oTrama.setError(!splittedFields[2].trim().startsWith(EXITO));
			 if(!oTrama.isError()){
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Llenando Bean: Trama", EIGlobal.NivelLog.INFO);
				 oTrama.setMensaje(trama.substring(0,55));
				 oTrama.setCuenta(trama.substring(55,56));
				 oTrama.setDivisa(trama.substring(55,56));
				 oTrama.setPersona(trama.substring(55,56));
				 oTrama.setRfc(trama.substring(55,56));
				 oTrama.setNombre(trama.substring(55,56));
				 oTrama.setApPaterno(trama.substring(55,56));
				 oTrama.setApMaterno(trama.substring(55,56));
				 oTrama.setErrorMessage("Transaccion Exitosa");
			/**Asegurarse se hacer un double check en caso de errores*/
			 }else if(splittedFields[2].trim().startsWith(ERROR)){
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Trama Erronea: ", EIGlobal.NivelLog.INFO);
				 oTrama.setError(true);
				 oTrama.setMensaje(trama.substring(17,35));
				 oTrama.setCuenta("");
				 oTrama.setDivisa("");
				 oTrama.setPersona("");
				 oTrama.setRfc("");
				 oTrama.setNombre("");
				 oTrama.setApPaterno("");
				 oTrama.setApMaterno("");
				 oTrama.setErrorMessage("Transaccion Erronea");
			 }
		 }
		 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13(): Fin", EIGlobal.NivelLog.INFO);
		 return oTrama;
	 }

	 /**
	 * Realiza el desentramado de la respuesta de la OD13 proveniente de 390
	 * @param trama		La trama que proviene de 390
	 * @return 			Instancia de TrxOD13Bean con los valores de error dentro del bean
	 * */
	 private String desentramaOD13Descripcion(String trama){
		 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Inicio", EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Validando Trama", EIGlobal.NivelLog.INFO);
		 TrxOD13Bean oTrama = new TrxOD13Bean();
		 String descripcion="";
		 if( "".equals(trama) || null == trama ){
			 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Trama Erronea: ", EIGlobal.NivelLog.INFO);
			 oTrama.setError(true);
			 oTrama.setMensaje("");
			 oTrama.setCuenta("");
			 oTrama.setDivisa("");
			 oTrama.setPersona("");
			 oTrama.setRfc("");
			 oTrama.setNombre("");
			 oTrama.setApPaterno("");
			 oTrama.setApMaterno("");
			 oTrama.setErrorMessage("Respuesta Vacia");
		 }
		 else{
			 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Trama valida, iniciando separacion", EIGlobal.NivelLog.INFO);

			 /**Separar la trama con el caracter arroba (@)*/
			 String[] splittedFields = trama.split("@");

			 /**Si ha sucedido algun error entonces en el elemento [2] deber�a encontrarse
			  * la informaci�n que permite identificarlo.*/
			 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Separacion de trama en "+splittedFields.length+" elementos", EIGlobal.NivelLog.INFO);
			 String tramaNombre="";

			 int i=0;
			 for(String item: splittedFields){
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Item de trama "+item, EIGlobal.NivelLog.INFO);
				 if(i==3){
					tramaNombre=item;
				 }
				 i++;
			 }
			 oTrama.setError(!splittedFields[2].trim().startsWith(EXITO));
			 if(!oTrama.isError()){
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Llenando Bean: Trama", EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Trama:"+trama, EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Trama Nombre:"+tramaNombre, EIGlobal.NivelLog.INFO);
				 descripcion=tramaNombre.substring(47,87).trim()+' '+tramaNombre.substring(87,107).trim()+' '+tramaNombre.substring(107,127).trim();
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Nombre:"+tramaNombre.substring(47,87).trim(), EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Apellido:"+tramaNombre.substring(87,107).trim(), EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Materno:"+tramaNombre.substring(107,126).trim(), EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Descripcion:"+descripcion, EIGlobal.NivelLog.INFO);

			/**Asegurarse se hacer un double check en caso de errores*/
			 }else if(splittedFields[2].trim().startsWith(ERROR)){
				 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Trama Erronea: ", EIGlobal.NivelLog.INFO);
				 oTrama.setError(true);
				 oTrama.setMensaje(trama.substring(17,35));
				 oTrama.setCuenta("");
				 oTrama.setDivisa("");
				 oTrama.setPersona("");
				 oTrama.setRfc("");
				 oTrama.setNombre("");
				 oTrama.setApPaterno("");
				 oTrama.setApMaterno("");
				 oTrama.setErrorMessage("Transaccion Erronea");
			 }
		 }
		 EIGlobal.mensajePorTrace("TrxOD13DAO - desentramaOD13Descripcion(): Fin", EIGlobal.NivelLog.INFO);
		 return descripcion;
	 }


}