package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmTrxGP93;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase de conexiones para manejar el procedimiento para la transaccion GP93
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Nov 22, 2011
 */
public class TrxGP93DAO extends AdmonUsuariosMQDAO{
	
	/**
	 * Metodo encargado de generar los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion GP93
	 * 
	 * @param tipoCatalogo		P - Pais o D - Divisa
	 * @param codPaisDivisa		Clave del pa�s o divisa
	 * @param llavePag			Llave de paginaci�n
	 * 
	 * @return AdmTrxGP93		Instancia de tipo AdmTrxGP93
	 */
	private AdmTrxGP93 ejecutaGP93(String tipoCatalogo, String codPaisDivisa, String llavePag) {
		
		StringBuffer sb = new StringBuffer();
		sb.append(tipoCatalogo)					
		  .append(codPaisDivisa)
		  .append(llavePag);			
		
    	EIGlobal.mensajePorTrace("TrxGP93DAO::ejecutaGP93:: Armando trama:" + sb.toString()
    			, EIGlobal.NivelLog.DEBUG);
		
		return consulta(AdmTrxGP93.HEADER, sb.toString(), AdmTrxGP93.getFactoryInstance());
	}
	
	/**
	 * Metodo encargado de recibir los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion GP93
	 * 
	 * @param tipoCatalogo		P - Pais o D - Divisa
	 * @param codPaisDivisa		Clave del pa�s o divisa
	 * @param llavePag			Llave de paginaci�n
	 * 
	 * @return AdmTrxGP93		Instancia de tipo AdmTrxGP93
	 */
	public AdmTrxGP93 consultaTrxGP93(String tipoCatalogo,  String codPaisDivisa, String llavePag) {
    	EIGlobal.mensajePorTrace("TrxGP93DAO::consultaTrxGP93:: Llegando a metodo para consulta de divisas y paises"
    			, EIGlobal.NivelLog.DEBUG);
		return ejecutaGP93(tipoCatalogo, codPaisDivisa, llavePag);			
	}

}
