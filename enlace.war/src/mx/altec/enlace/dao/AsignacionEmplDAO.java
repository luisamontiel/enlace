package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.altec.enlace.beans.EmpleadoBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

@SuppressWarnings("serial")
public class AsignacionEmplDAO extends BaseServlet {

	private static final String QUERY_PAISES = "SELECT cve_pais,descripcion "
			+ "from comu_paises order by descripcion";

	private static AsignacionEmplDAO asignacionEmplDAO = null;

	/**
	 * Variable Connection para uso transaccional
	 */
	private Connection conn = null;

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y poner commit en false.
	 *
	 * @return conn				Connection de la transacci�n.
	 */
	public Connection initTransac(){
		try{
			conn = createiASConn (Global.DATASOURCE_ORACLE2 );
		}catch(Exception e){
			EIGlobal.mensajePorTrace("AsignacionEmplDAO::initConnTransac:: Problemas al crear la conexi�n", EIGlobal.NivelLog.INFO);
		}
		return conn;
	}

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta commit.
	 *
	 */
	public void closeTransac(){
		try{
			this.conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("AsignacionEmplDAO::closeConnTransac:: Problemas al cerrar la conexi�n", EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Estatus con el que se guarda en BD.
	 */
	private static String ESTATUS_R = "R";

	/**
	 * COnstructor clase.
	 */
	private AsignacionEmplDAO() {

	}

	/**
	 * MEtodo evalua singleton
	 *
	 * @return
	 */
	public static AsignacionEmplDAO getSingleton() {
		if (asignacionEmplDAO == null) {
			asignacionEmplDAO = new AsignacionEmplDAO();
		}
		return asignacionEmplDAO;
	}

	/**
	 * constante vacio.
	 */
	private static String VACIO = "";

	/**
	 * Metodo para insertar la carga individual de empleado
	 *
	 * @param empleadoBean
	 * @param UsuarioEnlace
	 * @param tipoAlta
	 * @param tipoNomina
	 * @param numRegistros
	 * @param numRegistrosIns
	 * @return
	 */
	public synchronized Map<String, String> altaEmpleado(
			EmpleadoBean empleadoBean, String UsuarioEnlace, String tipoAlta,
			String tipoNomina, Integer numRegistros, Integer numRegistrosIns) {
		Map<String, String> mapExito = new HashMap<String, String>();
		String mensaje = "";
		EIGlobal.mensajePorTrace("AsignacionEmplDAO --->  "
				+ empleadoBean.getNombreEmpleado(), NivelLog.INFO);

		String numCuenta = obtenerSucursal(empleadoBean.getContrato());

		empleadoBean.setSucursalControl(numCuenta);

		this.altaEmpleadoControl(empleadoBean, UsuarioEnlace, tipoAlta,
				tipoNomina, numRegistros, numRegistrosIns);
		String idSecuencia = this.obtenSecuencia();

		mensaje = this.altaEmpleadoDetalle(idSecuencia, empleadoBean,
				UsuarioEnlace, tipoAlta, tipoNomina);
		mapExito.put("idSecuencia", idSecuencia);
		mapExito.put("mensaje", mensaje);
		return mapExito;
	}

	/**
	 * Metodo para insertar la carga de empleados masiva
	 *
	 * @param lstEmpleados
	 * @param UsuarioEnlace
	 * @param tipoAlta
	 * @param tipoNomina
	 * @param numRegistros
	 * @param numRegistrosIns
	 * @param numContrato
	 * @return
	 */

	public synchronized Map<String, String> altaEmpleadoMasiva(
			List<EmpleadoBean> lstEmpleados, String UsuarioEnlace,
			String tipoAlta, String tipoNomina, Integer numRegistros,
			Integer numRegistrosIns, String numContrato) {

		Map<String, String> mapExito = new HashMap<String, String>();
		String mensaje = "";
		EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> altaEmpleadoMasiva "
				+ lstEmpleados.size(), NivelLog.INFO);

		String numCuenta = obtenerSucursal(lstEmpleados.get(0).getContrato());

		lstEmpleados.get(0).setSucursalControl(numCuenta);

		lstEmpleados.get(0).setContrato(numContrato);
		lstEmpleados.get(0).setUsuarioSession(UsuarioEnlace);
		lstEmpleados.get(0).setEstatus(ESTATUS_R);

		this.altaEmpleadoControl(lstEmpleados.get(0), UsuarioEnlace, tipoAlta,
				tipoNomina, numRegistros, numRegistrosIns);
		String idSecuencia = this.obtenSecuencia();

		for (EmpleadoBean empleado : lstEmpleados) {

			mensaje = this.altaEmpleadoMasivaDetalle(idSecuencia, empleado,
					UsuarioEnlace, tipoAlta, tipoNomina);

		}

		mapExito.put("idSecuencia", idSecuencia);
		mapExito.put("mensaje", mensaje);
		return mapExito;
	}

	/**
	 * Metodo para hacer el insert en la tabla de control
	 *
	 * @param empleadoBean
	 * @param UsuarioEnlace
	 * @param tipoAlta
	 * @param tipoNomina
	 * @param numRegistros
	 * @param numRegistrosIns
	 * @return
	 */
	private String altaEmpleadoControl(EmpleadoBean empleadoBean,
			String UsuarioEnlace, String tipoAlta, String tipoNomina,
			Integer numRegistros, Integer numRegistrosIns) {

		Connection con = null;
		PreparedStatement consulta = null;

		String mensaje = null;

		try {

			con = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> "
					+ "altaEmpleadoControl --> con " + con, NivelLog.INFO);

			StringBuilder query = new StringBuilder();

			query
					.append(" INSERT INTO EWEB_NOMN3_CONTROL ")
					.append(" (NUM_CUENTA2, SECUENCIA, ")
					.append(" FCH_RECEPCION, NUM_REGISTROS, NUM_REGS_INS, ")
					.append(" FCH_INI_APLIC, FCH_FIN_APLIC, ESTATUS, ")
					.append(" FCH_ACTUAL, CVE_USUARIO, MEDIO_ENTREGA, ")
					.append(" SUCURSAL, REACTIVACION, TIPO_ALTA, TIPO_NOMINA )")
					.append(
							" VALUES (?, SEQ_ALTA_NOMN3.NEXTVAL, SYSDATE, ?, ?, ")
					.append(" SYSDATE, '', ?,  '', ?, ?,  ?, ?,  ?, ?)");
			consulta = con.prepareStatement(query.toString());
			consulta.setString(1, empleadoBean.getContrato());
			consulta.setInt(2, numRegistros);
			consulta.setInt(3, numRegistrosIns);

			consulta.setString(4, empleadoBean.getEstatus());
			consulta.setString(5, " ");
			consulta.setString(6, "EWEB");
			consulta.setString(7, empleadoBean.getSucursalControl());
			consulta.setString(8, "");
			consulta.setString(9, tipoAlta);
			consulta.setString(10, tipoNomina);

			EIGlobal.mensajePorTrace(
					"AsignacionEmplDAO ---> altaEmpleadoControl " + query,
					NivelLog.INFO);

			consulta.executeQuery();
			con.commit();
			mensaje = "EXITO";
			EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> "
					+ "altaEmpleadoControl --> Termina Insert " + con,
					NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace("Ocurrio un error en : " + e,
					NivelLog.INFO);
			mensaje = "ocurrio error";
			return null;
		} finally {
			try {
				if (consulta != null) {
					consulta.close();
					consulta = null;
				}
				if (con != null) {
					con.close();
					con = null;
				}
			} catch (Exception e1) {
				EIGlobal.mensajePorTrace(
						"AsignacionEmplDAO ---> altaEmpleadoControl() - "
								+ "Error al cerrar conexion: [" + e1 + "]",
						EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace(
					"AsignacionEmplDAO ---> altaEmpleadoControl():",
					EIGlobal.NivelLog.INFO);
		}

		return mensaje;
	}

	/**
	 * Obtiene la secuencia para insertar en tabla detalle.
	 *
	 * @return idSecuencia
	 */
	private String obtenSecuencia() {
		String idSecuencia = null;

		Connection con = null;
		PreparedStatement consulta = null;
		ResultSet resultado = null;

		try {

			con = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> "
					+ "obtenSecuencia ", NivelLog.INFO);

			StringBuilder query = new StringBuilder();

			query.append(" SELECT max(secuencia) as SECUENCIA ").append(
					" FROM EWEB_NOMN3_CONTROL");

			consulta = con.prepareStatement(query.toString());

			EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> obtenSecuencia "
					+ query, NivelLog.INFO);

			resultado = consulta.executeQuery();
			con.commit();
			System.out.println(resultado);
			while (resultado.next()) {
				EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> "
						+ "Obtiene sec while ", NivelLog.INFO);
				idSecuencia = (resultado.getString("SECUENCIA"));
				EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> idSecuencia "
						+ idSecuencia, NivelLog.INFO);
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("Ocurrio un error en : " + e,
					NivelLog.INFO);
			return null;
		} finally {
			try {
				if (consulta != null) {
					consulta.close();
					consulta = null;
				}
				if (con != null) {
					con.close();
					con = null;
				}
			} catch (Exception e1) {
				EIGlobal.mensajePorTrace(
						"AsignacionEmplDAO ---> obtenSecuencia() - "
								+ "Error al cerrar conexion: [" + e1 + "]",
						EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace(
					"AsignacionEmplDAO ---> obtenSecuencia():",
					EIGlobal.NivelLog.INFO);
		}

		return idSecuencia;

	}

	/**
	 * Metodo para insertar el datelle de los empleados
	 *
	 * @param idSecuencia
	 * @param empleadoBean
	 * @param UsuarioEnlace
	 * @param tipoAlta
	 * @param tipoNomina
	 * @return
	 */

	private String altaEmpleadoDetalle(String idSecuencia,
			EmpleadoBean empleadoBean, String UsuarioEnlace, String tipoAlta,
			String tipoNomina) {

		EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> altaEmpleadoDetalle ",
				NivelLog.INFO);

		Connection con = null;
		PreparedStatement consulta = null;
		ResultSet resultado = null;
		String mensaje = null;

		try {

			con = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> "
					+ "altaEmpleadoDetalle ", NivelLog.INFO);

			EIGlobal.mensajePorTrace("#$#$#$#$#$#$#$#$#$# ---> " +empleadoBean.getCveEstado()
					, NivelLog.INFO);

			StringBuilder query = new StringBuilder();

			query
					.append("INSERT INTO EWEB_NOMN3_DETALLE ")
					.append(
							"(NUM_CUENTA2, SECUENCIA, NUM_SEC, NUM_EMPL, APELL_PATERNO, ")
					.append(
							" APELL_MATERNO, NOMBRE, REG_FED_CAUS, HOMOCLAVE, TIPO_DOCTO, ")
					.append(
							" NO_IDENTIFICACION, SEXO, FCH_NACIMIENTO, CVE_NACIONALIDAD, ")
					.append(
							" PAIS_NACIMIENTO, CVE_ESTADO, ENTIDAD_EXT, EDO_CIVIL, CALLE, ")
					.append(" NUMERO_EXT, NUMERO_INT, COLONIA, COD_POSTAL, ")
					.append(" COD_POSTAL_TRABAJO, LADA, TELEFONO, SUCURSAL,  ")
					.append(
							" CODIGO_CLIENTE, NO_TARJETA, NO_CUENTA, ESTATUS, NO_REMESA,  ")
					.append(
							" REACTIVACION, ENVIAR_CATALOGO, OBSERVACIONES, OBSERVACIONES2, CVE_USUARIO)")
					.append(
							" VALUES(?, ?, 1, ?, ?, ?, ?, ")
					.append(" ?, ?, ?, ?, ?, to_Date(?, 'DD/MM/YYYY'), ?, ")
					.append(" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ")
					.append(" ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

			consulta = con.prepareStatement(query.toString());
			consulta.setString(1, empleadoBean.getContrato());
			consulta.setInt(2, Integer.valueOf(idSecuencia));
			consulta.setString(3, empleadoBean.getNoEmpleado());
			consulta.setString(4, empleadoBean.getAppPaterno());
			consulta.setString(5, empleadoBean.getAppMaterno());
			consulta.setString(6, empleadoBean.getNombreEmpleado());
			consulta.setString(7, empleadoBean.getRfc());
			consulta.setString(8, empleadoBean.getHomoclave());
			consulta.setString(9, empleadoBean.getTipoDocto());
			consulta.setString(10, empleadoBean.getNoIdentificacion());
			consulta.setString(11, empleadoBean.getSexo());
			consulta.setString(12, empleadoBean.getFechaNac());
			consulta.setString(13, empleadoBean.getCveNacionalidad());
			consulta.setString(14, empleadoBean.getPaisNacimiento());
			consulta.setString(15, empleadoBean.getCveEstado());
			consulta.setString(16, empleadoBean.getEntidadExtranjera());
			consulta.setString(17, empleadoBean.getEdoCivil());
			consulta.setString(18, empleadoBean.getCalle());
			consulta.setString(19, empleadoBean.getNoExterior());
			consulta.setString(20, empleadoBean.getNoInterior());
			consulta.setString(21, empleadoBean.getColonia());
			consulta.setString(22, empleadoBean.getCodPostal());
			consulta.setString(23, empleadoBean.getCodPostalTrab());
			consulta.setString(24, empleadoBean.getLada());
			consulta.setString(25, empleadoBean.getTelefono());
			consulta.setString(26, empleadoBean.getSucursal());
			consulta.setString(27, empleadoBean.getCodigoCliente());
			consulta.setString(28, empleadoBean.getNumTarjeta());
			consulta.setString(29, empleadoBean.getNumCuenta());
			consulta.setString(30, empleadoBean.getEstatus());
			consulta.setString(31, empleadoBean.getRemesa());
			consulta.setString(32, VACIO);
			consulta.setString(33, VACIO);
			consulta.setString(34, VACIO);
			consulta.setString(35, VACIO);
			consulta.setString(36, " ");

			EIGlobal.mensajePorTrace(
					"AsignacionEmplDAO ---> altaEmpleadoDetalle " + query,
					NivelLog.INFO);

			resultado = consulta.executeQuery();
			con.commit();
			mensaje = "EXITO";
			System.out.println(resultado);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace("Ocurrio un error en : " + e,
					NivelLog.INFO);
			mensaje = "ocurrio error";
			return null;
		} finally {
			try {
				if (consulta != null) {
					consulta.close();
					consulta = null;
				}
				if (con != null) {
					con.close();
					con = null;
				}
			} catch (Exception e1) {
				EIGlobal.mensajePorTrace(
						"AsignacionEmplDAO ---> altaEmpleadoDetalle() - "
								+ "Error al cerrar conexion: [" + e1 + "]",
						EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace(
					"AsignacionEmplDAO ---> altaEmpleadoDetalle():",
					EIGlobal.NivelLog.INFO);
		}

		return mensaje;
	}

	/**
	 * Metodo para insertar el datelle de los empleados
	 *
	 * @param idSecuencia
	 * @param empleadoBean
	 * @param UsuarioEnlace
	 * @param tipoAlta
	 * @param tipoNomina
	 * @return
	 */

	private String altaEmpleadoMasivaDetalle(String idSecuencia,
			EmpleadoBean empleadoBean, String UsuarioEnlace, String tipoAlta,
			String tipoNomina) {

		EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> altaEmpleadoDetalle ",
				NivelLog.INFO);

		Connection con = null;
		PreparedStatement consulta = null;
		ResultSet resultado = null;
		String mensaje = null;

		try {

			con = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			EIGlobal.mensajePorTrace("AsignacionEmplDAO ---> "
					+ "altaEmpleadoDetalle ", NivelLog.INFO);



			StringBuilder query = new StringBuilder();

			query
					.append("INSERT INTO EWEB_NOMN3_DETALLE ")
					.append(
							"(NUM_CUENTA2, SECUENCIA, NUM_SEC, NUM_EMPL, APELL_PATERNO, ")
					.append(
							" APELL_MATERNO, NOMBRE, REG_FED_CAUS, HOMOCLAVE, TIPO_DOCTO, ")
					.append(
							" NO_IDENTIFICACION, SEXO, FCH_NACIMIENTO, CVE_NACIONALIDAD, ")
					.append(
							" PAIS_NACIMIENTO, CVE_ESTADO, ENTIDAD_EXT, EDO_CIVIL, CALLE, ")
					.append(" NUMERO_EXT, NUMERO_INT, COLONIA, COD_POSTAL, ")
					.append(" COD_POSTAL_TRABAJO, LADA, TELEFONO, SUCURSAL,  ")
					.append(
							" CODIGO_CLIENTE, NO_TARJETA, NO_CUENTA, ESTATUS, NO_REMESA,  ")
					.append(
							" REACTIVACION, ENVIAR_CATALOGO, OBSERVACIONES, OBSERVACIONES2, CVE_USUARIO)")
					.append(
							" VALUES(?, ?, ?, ?, ?, ?, ?, ")
					.append(" ?, ?, ?, ?, ?, to_Date(?, 'DD/MM/YYYY'), ?, ")
					.append(" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ")
					.append(" ?, ?, ?, ?, ?, ?, ?, ?, ?) ");


			EIGlobal.mensajePorTrace("empleadoBean.getContrato()			"+empleadoBean.getContrato(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("idSecuencia							"+idSecuencia, NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getSecuencia()			"+empleadoBean.getSecuencia(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getNoEmpleado()			"+empleadoBean.getNoEmpleado(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getAppPaterno()			"+empleadoBean.getAppPaterno(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getAppMaterno()			"+empleadoBean.getAppMaterno(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getNombreEmpleado()		"+empleadoBean.getNombreEmpleado(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getRfc()					"+empleadoBean.getRfc(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getHomoclave()			"+empleadoBean.getHomoclave(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getTipoDocto()			"+empleadoBean.getTipoDocto(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getNoIdentificacion()	"+empleadoBean.getNoIdentificacion(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getSexo()				"+empleadoBean.getSexo(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getFechaNac()			"+empleadoBean.getFechaNac(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getCveNacionalidad()		"+empleadoBean.getCveNacionalidad(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getPaisNacimiento()		"+empleadoBean.getPaisNacimiento(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getCveEstado()			"+empleadoBean.getCveEstado(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getEntidadExtranjera()	"+empleadoBean.getEntidadExtranjera(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getEdoCivil()			"+empleadoBean.getEdoCivil(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getCalle()				"+empleadoBean.getCalle(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getNoExterior()			"+empleadoBean.getNoExterior(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getNoInterior()			"+empleadoBean.getNoInterior(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getColonia()				"+empleadoBean.getColonia(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getCodPostal()			"+empleadoBean.getCodPostal(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getCodPostalTrab()		"+empleadoBean.getCodPostalTrab(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getLada()				"+empleadoBean.getLada(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getTelefono()			"+empleadoBean.getTelefono(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getSucursal()			"+empleadoBean.getSucursal(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getCodigoCliente()		"+empleadoBean.getCodigoCliente(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getNumTarjeta()			"+empleadoBean.getNumTarjeta(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getNumCuenta()			"+empleadoBean.getNumCuenta(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getEstatus()				"+empleadoBean.getEstatus(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("empleadoBean.getRemesa()				"+empleadoBean.getRemesa(), NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("VACIO									"+VACIO, NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("VACIO									"+VACIO, NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("VACIO									"+VACIO, NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("VACIO									"+VACIO, NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("VACIO									"+empleadoBean.getCveUsuario(), NivelLog.DEBUG);


			consulta = con.prepareStatement(query.toString());
			consulta.setString(1, empleadoBean.getContrato());
			consulta.setInt(2, Integer.valueOf(idSecuencia));
			consulta.setString(3, empleadoBean.getSecuencia());

			consulta.setString(4, empleadoBean.getNoEmpleado());
			consulta.setString(5, empleadoBean.getAppPaterno());
			consulta.setString(6, empleadoBean.getAppMaterno());
			consulta.setString(7, empleadoBean.getNombreEmpleado());
			consulta.setString(8, empleadoBean.getRfc());
			consulta.setString(9, empleadoBean.getHomoclave());
			consulta.setString(10, empleadoBean.getTipoDocto());
			consulta.setString(11, empleadoBean.getNoIdentificacion());
			consulta.setString(12, empleadoBean.getSexo());
			consulta.setString(13, empleadoBean.getFechaNac());
			consulta.setString(14, empleadoBean.getCveNacionalidad());
			consulta.setString(15, empleadoBean.getPaisNacimiento());
			consulta.setString(16, empleadoBean.getCveEstado());
			consulta.setString(17, empleadoBean.getEntidadExtranjera());
			consulta.setString(18, empleadoBean.getEdoCivil());
			consulta.setString(19, empleadoBean.getCalle());
			consulta.setString(20, empleadoBean.getNoExterior());
			consulta.setString(21, empleadoBean.getNoInterior());
			consulta.setString(22, empleadoBean.getColonia());
			consulta.setString(23, empleadoBean.getCodPostal());
			consulta.setString(24, empleadoBean.getCodPostalTrab());
			consulta.setString(25, empleadoBean.getLada());
			consulta.setString(26, empleadoBean.getTelefono());
			consulta.setString(27, empleadoBean.getSucursal());
			consulta.setString(28, empleadoBean.getCodigoCliente());
			consulta.setString(29, empleadoBean.getNumTarjeta());
			consulta.setString(30, empleadoBean.getNumCuenta());
			consulta.setString(31, empleadoBean.getEstatus());
			consulta.setString(32, empleadoBean.getRemesa());
			consulta.setString(33, VACIO);
			consulta.setString(34, VACIO);
			consulta.setString(35, VACIO);
			consulta.setString(36, VACIO);
			consulta.setString(37, " ");

			EIGlobal.mensajePorTrace(
					"AsignacionEmplDAO ---> altaEmpleadoDetalle " + query,
					NivelLog.INFO);

			resultado = consulta.executeQuery();
			con.commit();
			mensaje = "EXITO";
			System.out.println(resultado);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace("Ocurrio un error en : " + e,
					NivelLog.INFO);
			mensaje = "ocurrio error";
			return null;
		} finally {
			try {
				if (consulta != null) {
					consulta.close();
					consulta = null;
				}
				if (con != null) {
					con.close();
					con = null;
				}
			} catch (Exception e1) {
				EIGlobal.mensajePorTrace(
						"AsignacionEmplDAO ---> altaEmpleadoDetalle() - "
								+ "Error al cerrar conexion: [" + e1 + "]",
						EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace(
					"AsignacionEmplDAO ---> altaEmpleadoDetalle():",
					EIGlobal.NivelLog.INFO);
		}

		return mensaje;
	}

	/**
	 *
	 * @param sqlTipoQuery
	 * @return
	 * @throws Exception
	 */
	public String ObtenCatalogo() {
		String strOpcion = "";
		Connection conn = null;
		PreparedStatement TipoQuery = null;
		ResultSet TipoResult = null;

		EIGlobal.mensajePorTrace("ENTRA ObtenCatalogo PAISES " + QUERY_PAISES,
				NivelLog.INFO);

		try {
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if (conn == null) {
				EIGlobal.mensajePorTrace(
						"Error al intentar crear la conexion.",
						EIGlobal.NivelLog.INFO);
				return "";
			}

			TipoQuery = conn.prepareStatement(QUERY_PAISES);
			if (TipoQuery == null) {
				EIGlobal.mensajePorTrace("No se pudo crear Query.",
						EIGlobal.NivelLog.INFO);
				return "";
			}

			TipoResult = TipoQuery.executeQuery();
			if (TipoResult == null) {
				EIGlobal.mensajePorTrace("No se pudo crear ResultSet.",
						EIGlobal.NivelLog.INFO);
				return "";
			}

			strOpcion += "<option CLASS='tabmovtex'>MEXICO</option>\n";

			while (TipoResult.next()) {
				if (!TipoResult.getString(2).trim().equals("MEXICO")) {
					strOpcion += "<option CLASS='tabmovtex'>"
							+ TipoResult.getString(2) + "</option> \n";
				}
			}

		} catch (SQLException sqle) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle),
					EIGlobal.NivelLog.INFO);
			return "";
		} finally {
			try {
				TipoResult.close();
				conn.close();
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e),
						EIGlobal.NivelLog.INFO);
			}
		}

		EIGlobal.mensajePorTrace("SALE ObtenCatalogo PAISES " + strOpcion,
				NivelLog.INFO);
		return strOpcion;
	}

	/**
	 * Metodo para obtener la sucursal para insertarla en control
	 *
	 * @param nuCuenta
	 * @return
	 */
	private String obtenerSucursal(String nuCuenta) {

		Connection con = null;
		PreparedStatement consulta = null;
		ResultSet resultado = null;
		String sucursalControl = "";

		try {

			con = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);

			EIGlobal.mensajePorTrace("O B T E N E R  S U C U R S A L --->",
					NivelLog.INFO);

			StringBuilder query = new StringBuilder();

			query.append(" SELECT suc_ape_cta FROM inom_empresa ").append(
					"  WHERE num_cuenta2 =" + nuCuenta);

			consulta = con.prepareStatement(query.toString());

			resultado = consulta.executeQuery();
			con.commit();

			while (resultado.next()) {

				sucursalControl = (resultado.getString("suc_ape_cta"));
			}

		} catch (Exception e) {
			EIGlobal.mensajePorTrace("ERROR OBTENIENDO SUCURSAL : " + e,
					NivelLog.INFO);
		}

		return sucursalControl;
	}

    /**
     * M�todo para verificar que la tarjeta no se encuentre ya en proceso de asignaci�n.
     *
     */
	public boolean verificaTarjetaAsignacion(String numContrato, String numTarjeta){
		EIGlobal.mensajePorTrace ("AsignacionEmplDAO::verificaTarjetaAsignacion:: Entrando a verificaTarjetaAsignacion",
				EIGlobal.NivelLog.DEBUG);
		String query = null;
		ResultSet rs = null;
		int existeTarjeta = 0;
		String estatus = null;
		String reactivacion = null;

		try{
			Statement sDup = this.conn.createStatement();
			query  = "SELECT a.estatus as estatus, "
				   + "a.reactivacion as reactivacion "
				   + "FROM EWEB_NOMN3_DETALLE a "
				   + "WHERE a.NUM_CUENTA2 ='" + numContrato.trim() + "' "
				   + "AND a.NO_TARJETA ='" + numTarjeta.trim() + "' "
				   + "AND a.ESTATUS in ('R', 'E')";

			EIGlobal.mensajePorTrace ("AsignacionEmplDAO::verificaTarjetaAsignacion:: query->"+query, EIGlobal.NivelLog.DEBUG);
			rs = sDup.executeQuery(query);
			if(rs.next()){
				estatus = rs.getString("estatus");
				reactivacion = rs.getString("reactivacion");
				if(estatus.trim().equals("R") && (reactivacion!=null && reactivacion.trim().equals("X")))
					existeTarjeta = 0;
				else
					existeTarjeta = 1;
			}else
				existeTarjeta = 0;

			rs.close();
			sDup.close();

			if(existeTarjeta==0)
				return false;
			else
				return true;

		}catch(Exception e){
			EIGlobal.mensajePorTrace ("AsignacionEmplDAO::verificaTarjetaAsignacion:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return false;
	}

}