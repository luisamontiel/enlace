package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmTrxGP72;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase de conexiones para manejar el procedimiento para la transaccion GP71
 *
 * @author Daniel Hernandez Perez
 * @version 1.0 Apr 09, 2012
 *
 **/
public class TrxGP72DAO extends AdmonUsuariosMQDAO{

	/**
	 * Metodo encargado de generar los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion GP72
	 *
	 * @param cveCot		Clave de Cotización
	 * @param tipCtaCarg 	Tipo de Cuenta Cargo
	 * @param ctaCargo		Cuenta Cargo
	 * @param divisa		Divisa
	 * @param montoDivisa	Monto Divisa
	 * @param tipCamb		Tipo de Cambio
	 * @param tipCambBase	Tipo de Cambio Base
	 * @param cenOp			Centro Operante
	 * @param cenOri		Centro Origen
	 * @param cenDest		Centro Destino
	 * @param obserCar		Observacion de Cargo
	 * @param obserAbon		Observacion de Abono
	 * @param canal			Canal  - Constante
	 * @param usuario		Usuario
	 * @param producto		Producto
	 * @param tipCtaAbon	Tipo de Cuenta Abono
	 * @param ctaAbon		Cuenta Abono
	 * @param importeAbono	Importe Abono
	 * @param nombreBenef	Nombre Beneficiario
	 * @param apPatBenef	Ap Pat Beneficiario
	 * @param apMatBenef	Ap Mat Beneficiario
	 * @param rfcBenef		RFC Beneficiario
	 * @param paisDest		Pais Beneficiario
	 * @param bcoCorr		Banco Corresponsal
	 * @param detalleOp		Detalle de Operación
	 *
	 * @return AdmTrxGP72		Instancia de tipo AdmTrxGP72
	 */
	private AdmTrxGP72 ejecutaGP72(String folioMultiabono, String cveTcOper, String tipoAbono,
			String cuentaCargo, String divisaOperante, String contraDivisa,
			String importeTotOper, String tcPactado, String tcBase,
			String tcBaseDiv1, String tcVentDiv1, String tcBaseDiv2,
			String tcVentDiv2, String centroOperante, String centroOrigen,
			String centroDestino, String observacionCargo, String observacionAbono,
			String canal, String usuario, String producto,
			String cuentaAbono, String importeAbono,
			String nombreBenef, String apPaterBenef, String apMaterBenef,
			String rfcBenef, String paisBenef, String bancoCorresponsal,
			String detalleOper) {

		StringBuffer sb = new StringBuffer();
					 sb.append(folioMultiabono)
					   .append(cveTcOper)
					   .append(tipoAbono)
					   .append(cuentaCargo)
					   .append(divisaOperante)
					   .append(contraDivisa)
					   .append(importeTotOper)
					   .append(tcPactado)
					   .append(tcBase)
					   .append(tcBaseDiv1)
					   .append(tcVentDiv1)
					   .append(tcBaseDiv2)
					   .append(tcVentDiv2)
					   .append(centroOperante)
					   .append(centroOrigen)
					   .append(centroDestino)
					   .append(observacionCargo)
					   .append(observacionAbono)
					   .append(canal)
					   .append(usuario)
					   .append(producto)
					   .append(cuentaAbono)
					   .append(importeAbono)
					   .append(nombreBenef)
					   .append(apPaterBenef)
					   .append(apMaterBenef)
					   .append(rfcBenef)
					   .append(paisBenef)
					   .append(bancoCorresponsal)
					   .append(detalleOper);

    	EIGlobal.mensajePorTrace("TrxGP72DAO::ejecutaGP72:: Armando trama:" + sb.toString()
    			, EIGlobal.NivelLog.INFO);

		return consulta(AdmTrxGP72.HEADER, sb.toString(), AdmTrxGP72.getFactoryInstance());
	}

	/**
	 * Metodo encargado de recibir los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion GP71
	 *
	 * @param cveCot		Clave de Cotización
	 * @param tipCtaCarg 	Tipo de Cuenta Cargo
	 * @param ctaCargo		Cuenta Cargo
	 * @param divisa		Divisa
	 * @param montoDivisa	Monto Divisa
	 * @param tipCamb		Tipo de Cambio
	 * @param tipCambBase	Tipo de Cambio Base
	 * @param cenOp			Centro Operante
	 * @param cenOri		Centro Origen
	 * @param cenDest		Centro Destino
	 * @param obserCar		Observacion de Cargo
	 * @param obserAbon		Observacion de Abono
	 * @param canal			Canal  - Constante
	 * @param usuario		Usuario
	 * @param producto		Producto
	 * @param tipCtaAbon	Tipo de Cuenta Abono
	 * @param ctaAbon		Cuenta Abono
	 * @param importeAbono	Importe Abono
	 * @param nombreBenef	Nombre Beneficiario
	 * @param apPatBenef	Ap Pat Beneficiario
	 * @param apMatBenef	Ap Mat Beneficiario
	 * @param rfcBenef		RFC Beneficiario
	 * @param paisDest		Pais Beneficiario
	 * @param bcoCorr		Banco Corresponsal
	 * @param detalleOp		Detalle de Operación
	 *
	 * @return AdmTrxGP72		Instancia de tipo AdmTrxGP72
	 */
	public AdmTrxGP72 consultaTrxGP72(String folioMultiabono, String cveTcOper, String tipoAbono,
			String cuentaCargo, String divisaOperante, String contraDivisa,
			String importeTotOper, String tcPactado, String tcBase,
			String tcBaseDiv1, String tcVentDiv1, String tcBaseDiv2,
			String tcVentDiv2, String centroOperante, String centroOrigen,
			String centroDestino, String observacionCargo, String observacionAbono,
			String canal, String usuario, String producto,
			String cuentaAbono, String importeAbono,
			String nombreBenef, String apPaterBenef, String apMaterBenef,
			String rfcBenef, String paisBenef, String bancoCorresponsal,
			String detalleOper) {

    	EIGlobal.mensajePorTrace("TrxGP72DAO::consultaTrxGP72:: Llegando a metodo para pactar operaciones Reuters a Cambios 390"
    			, EIGlobal.NivelLog.DEBUG);
		return ejecutaGP72(folioMultiabono, cveTcOper, tipoAbono, cuentaCargo,  divisaOperante,  contraDivisa,
				 		   importeTotOper,  tcPactado,  tcBase, tcBaseDiv1,  tcVentDiv1,  tcBaseDiv2,
				 		   tcVentDiv2,  centroOperante,  centroOrigen, centroDestino,  observacionCargo,
				 		   observacionAbono, canal,  usuario,  producto, cuentaAbono,
				 		   importeAbono, nombreBenef,  apPaterBenef,  apMaterBenef, rfcBenef,  paisBenef,
				 		   bancoCorresponsal, detalleOper);
	}
}