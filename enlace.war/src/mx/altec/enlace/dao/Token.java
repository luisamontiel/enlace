package mx.altec.enlace.dao;

import java.sql.*;
import java.util.Calendar;

import javax.sql.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.servlets.BaseServlet;

import java.io.*;
import com.vasco.utils.*;

public class Token implements Serializable {

	/** The usuario. */
	private String usuario;

	/** The dpdata. */
	private byte[] dpdata;

	/** The ret code. */
	private String retCode;

	/** The serial number. */
	private String serialNumber;

	/** The status. */
	private int status;

	/** The mode. */
	private int mode;

	/** The esquema token. */
	private boolean esquemaToken;

	/** The contrato. */
	private String contrato;

	/** The intentos. */
	private int intentos;


	/** The Constant TOK_VALIDA_PWD. */
	public static final int TOK_VALIDA_PWD = 0;	//para validar contrase�a din�mica

	/** The Constant TOK_SINCRONIZA. */
	public static final int TOK_SINCRONIZA = 1;	//para sincronizar token

	/** The wrapper. */
	private static AAL2Wrap wrapper = null;

	public Token(String usuario) {
		this.usuario =  BaseServlet.convierteUsr7a8(usuario);	//c�digo temporal hasta que se migre token_enlace
		this.retCode = "";
		status = -1;
		setMode(TOK_VALIDA_PWD);
		contrato ="";
	}


	public boolean inicializa(Connection conn) {
		 boolean resultado = false;
		Statement sDup=null;
		ResultSet rs =null;

		 try {
				sDup = conn.createStatement();
				String query = "SELECT DPDATA, STATUS_TOKEN, substr(serialnumber, 1, 10) as SERIALNUMBER, I_FALLIDOS FROM TOKEN_ENLACE WHERE USERNAME = '" + usuario + "'";
				EIGlobal.mensajePorTrace ("Token - inicializa -> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
				rs  = sDup.executeQuery(query);
				while(rs.next()) {
					dpdata = (rs.getString("DPDATA")).getBytes();
					setStatus(rs.getString("STATUS_TOKEN"));
					setSerialNumber(rs.getString("SERIALNUMBER"));
					setIntentos(rs.getInt("I_FALLIDOS"));
					resultado = true;
				}
			}
			catch(SQLException e) {
				e.printStackTrace();
				EIGlobal.mensajePorTrace ("Token - inicializa -> Problemas para cargar info del Token para " + usuario, EIGlobal.NivelLog.INFO);
				resultado = false;
			}finally{
					try{
						if (rs!=null){
							rs.close();
							rs=null;
						}
						if (sDup!=null){
							sDup.close();
							sDup=null;
						}
					}catch(Exception e){
						EIGlobal.mensajePorTrace ("Token - inicializa ->Error al cerrar el Statement", EIGlobal.NivelLog.INFO);
					}
			}

		EIGlobal.mensajePorTrace ("*****\n*****esquemaToken = " + esquemaToken + "\n*****", EIGlobal.NivelLog.INFO);


			return resultado;
	}

	public void esquemaToken(Connection conn, boolean tieneFacultad) {
	}

	private static synchronized int valida(String token, Token tok) {
		int retVal = -1;
		String[] info        = new String[15];
        int      MyOldLTS    = 0;
        int      MyNewLTS    = 0;
        int      MyDifLTS    = 0;
        int      MyModLTS    = 0;
		String usos = null;
		int numUsos = 0;


		if(wrapper == null) wrapper = new AAL2Wrap();
		if(tok.dpdata != null) {
			String challenge = wrapper.AAL2GenerateChallenge(tok.dpdata, tok.getParms());

			/* Se recuperan los datos iniciales del Token */
            DigipassInfo MyInfo = wrapper.AAL2GetTokenInfo(tok.dpdata, tok.getParms());
            String[] RetInfo = {
                    MyInfo.getInfo(MyInfo.USE_COUNT),
                    MyInfo.getInfo(MyInfo.LAST_TIME_USED),
                    MyInfo.getInfo(MyInfo.LAST_TIME_SHIFT),
                    MyInfo.getInfo(MyInfo.ERROR_COUNT)
            };
            info=RetInfo;

            /* Se despliegan los datos iniciales del Token */
            EIGlobal.mensajePorTrace ("Token.class - valida() -> Datos iniciales del Token: \n "
            		+ "BLOB LOCAL:[" + new String(tok.dpdata) +"] \n "
            		+ "USE_COUNT:[" + info[0]+"] \n "
            		+ "LAST_TIME_USED: ["+ info[1] + "] \n "
            		+ "LAST_TIME_SHIFT: [" + info[2] +"] \n "
            		+ "ERROR_COUNT: [" + info[3] + "] \n", EIGlobal.NivelLog.INFO);

            /* Guarda el valor anterior a la validacion de LAST_TIME_SHIFT */
            MyOldLTS = Integer.parseInt(MyInfo.getInfo(MyInfo.LAST_TIME_SHIFT));

            /* Obtiene la cantidad de usos del Token antes de la validacion*/
			usos= MyInfo.getInfo(DigipassInfo.USE_COUNT) ;
			numUsos =  Integer.parseInt(usos);

			/* Valida el Token*/
			retVal = wrapper.AAL2VerifyPassword(tok.dpdata, tok.getParms(), token, challenge);

			/* Se recuperan los datos del Token posterior a la validacion*/
			MyInfo = wrapper.AAL2GetTokenInfo(tok.dpdata, tok.getParms());
            String[] RetInfo2 = {
                    MyInfo.getInfo(MyInfo.USE_COUNT),
                    MyInfo.getInfo(MyInfo.LAST_TIME_USED),
                    MyInfo.getInfo(MyInfo.LAST_TIME_SHIFT),
                    MyInfo.getInfo(MyInfo.ERROR_COUNT)
            };
            info=RetInfo2;

            /* Guarda el valor posteiora la validacion de LAST_TIME_SHIFT */
            MyNewLTS = Integer.parseInt(MyInfo.getInfo(MyInfo.LAST_TIME_SHIFT));

            /* Valida si el Token es nuevo comparando el numero de usos previo a la validacion */
			if ( numUsos != 0  ){
				/* Si el Token no es nuevo aplica algoritmo Delta/2 para asignarlo al LAST_TIME_SHIFT */
				MyDifLTS = MyNewLTS - MyOldLTS;
				MyModLTS = MyOldLTS + MyDifLTS/2;
				int retorno = wrapper.AAL2SetTokenProperty(tok.dpdata, tok.getParms(), AAL2Wrap.LAST_TIME_SHIFT, MyModLTS);

				EIGlobal.mensajePorTrace ("Token.class - valida() -> Datos despues de validar del Token: \n "
			        		+ "BLOB LOCAL:[" + new String(tok.dpdata) +"] \n "
		            		+ "Retorno:[" + retorno +"] \n "
		            		+ "USE_COUNT:[" + info[0]+"] \n "
		            		+ "LAST_TIME_USED: ["+ info[1] + "] \n "
		            		+ "LAST_TIME_SHIFT: [" + info[2] +"] \n "
		            		+ "ERROR_COUNT: [" + info[3] + "] \n", EIGlobal.NivelLog.INFO);

		        if ( retorno  > 0 ) {
		            EIGlobal.mensajePorTrace ("Token.class - valida() -> Error en SetTokenProperty: \n "
		            		+ "RetCode():[" + wrapper.getRetCode() +"] \n "
		            		+ "LastError():[" + wrapper .getLastError()+"] \n ", EIGlobal.NivelLog.INFO);
                }
				 
				 // Se actualiza en memoria el valor del blob para posteriormente actualizarlo en la BD:
				//dpdata = tok.dpdata;
				
				EIGlobal.mensajePorTrace ("Token.class - valida() -> Delta: \n "
						+ "MyOldLTS:[" + MyOldLTS +"] \n "
						+ "MyNewLTS: ["+ MyNewLTS + "] \n "
						+ "MyDifLTS: [" + MyDifLTS +"] \n "
            			+ "MyModLTS: [" + MyModLTS + "] \n ", EIGlobal.NivelLog.INFO);
			} else {
				EIGlobal.mensajePorTrace ("Token.class - valida() -> TOKEN NUEVO", EIGlobal.NivelLog.INFO);
			}

			MyInfo = wrapper.AAL2GetTokenInfo(tok.dpdata, tok.getParms());
            String[] RetInfo3 = {
                    MyInfo.getInfo(MyInfo.USE_COUNT),
                    MyInfo.getInfo(MyInfo.LAST_TIME_USED),
                    MyInfo.getInfo(MyInfo.LAST_TIME_SHIFT),
                    MyInfo.getInfo(MyInfo.ERROR_COUNT)
            };
            info=RetInfo3;

            EIGlobal.mensajePorTrace ("Token.class - valida() -> Datos finales del Token: \n "
	           		+ "BLOB LOCAL:[" + new String(tok.dpdata) +"] \n "
            		+ "USE_COUNT:[" + info[0]+"] \n "
            		+ "LAST_TIME_USED: ["+ info[1] + "] \n "
            		+ "LAST_TIME_SHIFT: [" + info[2] +"] \n "
            		+ "ERROR_COUNT: [" + info[3] + "] \n", EIGlobal.NivelLog.INFO);

			tok.retCode = wrapper.getStringReturnHostCode();
		}
		return retVal;
	}

	public boolean validaToken(Connection conn, String contDin) {
		boolean resultado = false;
		String token = contDin;
		token = token == null ? "" : token;
		inicializa(conn);
		int retVal = -1;

		retVal = valida(token, this);
		resultado = retVal == 0 ? true : false;

		EIGlobal.mensajePorTrace ("Token - validaToken -> Resultado: " + resultado, EIGlobal.NivelLog.INFO);
		if(retVal == 0) {
			EIGlobal.mensajePorTrace ("Token - validaToken -> Actualiza BLOB: [" + dpdata + "]", EIGlobal.NivelLog.INFO);
			updateBlob(conn);
		}
		EIGlobal.mensajePorTrace("Token - validaToken -> contDin" + contDin, EIGlobal.NivelLog.DEBUG);
		return resultado;
	}
	private boolean updateBlob(Connection conn) {
		boolean resultado = false;
		Statement sDup=null;
		try {
			sDup = conn.createStatement();
			String query = "UPDATE TOKEN_ENLACE SET DPDATA = '" + new String(dpdata) + "' WHERE USERNAME = '" + usuario + "'";
			EIGlobal.mensajePorTrace ("Token - updateBlob -> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
			if(sDup.executeUpdate(query) == 1) {
				resultado = true;
			}
			//sDup.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace ("Token - updateBlob -> Problemas para actualizar blob para " + usuario, EIGlobal.NivelLog.INFO);
			resultado = false;	//temporal
		}
		finally{
				try{
					if (sDup!=null){
						sDup.close();
						sDup=null;
					}
				}catch(Exception e){
					EIGlobal.mensajePorTrace ("Token - updateBlob -> Error al cerrar el Statement", EIGlobal.NivelLog.INFO);
				}
		}
		return resultado;

	}
	public boolean tieneToken(Connection conn) {

		boolean resultado = true;
		Statement sDup = null;
		ResultSet rs = null;
     	try {
			sDup = conn.createStatement();
	     	String query = "SELECT COUNT(1) AS CUENTA FROM TOKEN_ENLACE WHERE USERNAME = '" + usuario + "'";
	     	EIGlobal.mensajePorTrace ("Token - tieneToken -> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
	     	rs  = sDup.executeQuery(query);
	     	while(rs.next()) {
				if(rs.getInt("CUENTA") != 1) {
					resultado = false;
				}
				EIGlobal.mensajePorTrace ("Token - tieneToken -> cuenta:   : " + rs.getInt("CUENTA") + ", usuario: " + usuario, EIGlobal.NivelLog.INFO);
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace ("Token - tieneToken -> Problemas para obtener datos para " + usuario, EIGlobal.NivelLog.INFO);
			resultado = false; //temporal
		}
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;
					}
					if (sDup!=null){
						sDup.close();
						sDup=null;
					}
				}catch(Exception e){
					EIGlobal.mensajePorTrace ("Token - updateBlob -> Error al cerrar el Statement", EIGlobal.NivelLog.INFO);
				}
		}

		return resultado;

	}
	public String getRetCode() {
		return retCode;
	}

	public void setStatus(String value) {
		System.out.println("Status a insertar: " + value);
		if(value == null || value.equals("")) {
			value =  "-1";				//se considera que no tiene token
		}
		try {
			int valor = Integer.parseInt(value.trim());
			switch(valor) {
				case 0:					//entregado
				case 1:					//activo
				case 2:					//bloqueado
						status = valor;
						break;
				case 3:
						status = valor;
						break;
				case 4:                  //bloqueado por inactividad
						status = valor;
				break;

				default: status = -1;
						 break;
			}
		} catch(NumberFormatException e) {
			status = -1;
		}
	}
	public int getStatus() {
		return status;
	}

	public boolean getEsquemaToken() {
		return esquemaToken;
	}

	public void setContrato(String contr) {
		System.out.println("Contrato a insertar: " + contr);
		contrato = contr;
	}

	public void setMode(int mode) {
		switch(mode) {
			case TOK_VALIDA_PWD:
			case TOK_SINCRONIZA:	this.mode = mode;
									break;
			default:				throw new RuntimeException("Modo para token inexistente: " + mode);
		}
	}
	private KernelParms getParms() {
		KernelParms parms = null;
		switch(mode) {
			case TOK_VALIDA_PWD:	parms = new KernelParms(2,24,0,0,1,0,0,0,0,6,0,100,0,0,0x7FFFFF,0,0,0,0);
									break;
			case TOK_SINCRONIZA:	parms = new KernelParms(100,24,0,0,1,0,0,0,0,6,0,100,0,0,0x7FFFFF,0,0,0,0);
									break;
			default:				throw new RuntimeException("Modo para token inexistente: " + mode);
		}
		return parms;
	}

	private void setSerialNumber(String serial) {
		this.serialNumber = serial;
	}

	public String getSerialNumber() {
		return serialNumber;
	}


/******************************************************************************/
/** Metodo: consultaTokenExistente
 * @return boolean (false = no existe en table, true =  si existe en tabla)
 * @param String -> usuario
 */
/******************************************************************************/
	private void consultaTokenExistente(Connection conn) {
		esquemaToken = true;
	}

/******************************************************************************/
/** Metodo: agregaListaTokens
 * @return boolean (false = no se agreg� token a table, true = si fue agregado)
 * @param String -> usuario
 */
/******************************************************************************/
	private boolean agregaListaTokens(Connection conn) {
		int rows = 0;
		boolean result = false;
		Statement sDup=null;

		try {
			sDup = conn.createStatement();
	     	String query = "INSERT INTO eweb_esquema_token VALUES ('" + usuario + "', '" + contrato + "')";
	     	EIGlobal.mensajePorTrace ("BaseServlet - agregaListaTokens -> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
	     	rows = sDup.executeUpdate(query);
	     	conn.commit();
			result = true;
		}
		catch(SQLException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace ("BaseServlet - agregaListaTokens -> Problemas para insertar usuario a eweb_esquema_token, ", EIGlobal.NivelLog.INFO);
			result = false;
		}
		finally{
				try{
					if (sDup!=null){
						sDup.close();
						sDup=null;
					}
				}catch(Exception e){
					EIGlobal.mensajePorTrace ("Token - agregaListaTokens -> Error al cerrar el Statement", EIGlobal.NivelLog.INFO);
				}
		}


		return result;
	}


	public boolean incrementaIntentos(Connection conn) {
		int rows = 0;
		boolean resultado = false;
		Statement sDup=null;

		try {
			sDup = conn.createStatement();
			String query = "UPDATE TOKEN_ENLACE SET I_FALLIDOS = NVL(I_FALLIDOS, 0) + 1 WHERE USERNAME = '" + usuario + "'";
			EIGlobal.mensajePorTrace ("Token - updateBlob -> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
			rows = sDup.executeUpdate(query);
	     	conn.commit();
			EIGlobal.mensajePorTrace ("Token - updateBlob -> res: <" + rows + ">", EIGlobal.NivelLog.INFO);
			resultado = true;
		}
		catch(SQLException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace ("Token - updateBlob -> Problemas para actualizar blob para " + usuario, EIGlobal.NivelLog.INFO);
			resultado = false;
		}
		finally{
				try{
					if (sDup!=null){
						sDup.close();
						sDup=null;
					}
				}catch(Exception e){
					EIGlobal.mensajePorTrace ("Token - updateBlob -> Error al cerrar el Statement", EIGlobal.NivelLog.INFO);
				}
		}
		return resultado;
	}
	public boolean reinicializaIntentos(Connection conn) {
		int rows = 0;
		boolean resultado = false;
		Statement sDup=null;

		try {
			sDup = conn.createStatement();
			String query = "UPDATE TOKEN_ENLACE SET I_FALLIDOS = 0, ULTIMA_VALIDACION = Current_Timestamp WHERE USERNAME = '" + usuario + "'";
			EIGlobal.mensajePorTrace ("Token - updateBlob -> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
			rows = sDup.executeUpdate(query);
			conn.commit();
			resultado = true;
		}
		catch(SQLException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace ("Token - updateBlob -> Problemas para actualizar blob para " + usuario, EIGlobal.NivelLog.INFO);
			resultado = false;
		}
		finally{
				try{
					if (sDup!=null){
						sDup.close();
						sDup=null;
					}
				}catch(Exception e){
					EIGlobal.mensajePorTrace ("Token - updateBlob -> Error al cerrar el Statement", EIGlobal.NivelLog.INFO);
				}
		}
		return resultado;
	}
	public void setIntentos(int intentos) {
		this.intentos = intentos;
	}


	public int getIntentos() {
		return intentos;
	}

}