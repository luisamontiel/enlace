package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import mx.altec.enlace.bo.RET_ControlConsOper;
import mx.altec.enlace.bo.RET_CuentaAbonoVO;
import mx.altec.enlace.bo.RET_OperacionVO;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 *  Clase para el negocio de acceso a base de datos para la consulta de operaciones
 *  Reuters pactadas del d�a
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 10, 2012
 */
public class RET_ConsultaOperDAO extends BaseServlet{

	/**
	 * Numero id de version serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Variable Connection para uso transaccional
	 */
	private Connection conn = null;

	/**
	 * Variable Statement para realizar batch
	 */
	private Statement stmt = null;

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y poner commit en false.
	 *
	 * @return conn				Connection de la transacci�n.
	 */
	public Connection initTransac(){
		try{
			conn = createiASConn (Global.DATASOURCE_ORACLE2);
			conn.setAutoCommit(false);
			stmt = conn.createStatement();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_ConsultaOperDAO::initConnTransac:: Problemas al crear la conexi�n", EIGlobal.NivelLog.INFO);
		}
		return conn;
	}

	/**
	 * Metodo para ejecutar una transacci�n en batch
	 * la transacci�n y poner commit en false.
	 *
	 * @return ejecBatch				Arreglo con ejecuci�n del batch
	 */
	public int[] execTransac(){
		int[] ejecBatch = null;
		try{
			ejecBatch = this.stmt.executeBatch();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_ConsultaOperDAO::initConnTransac:: Problemas al ejecutar el batch->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
		return ejecBatch;
	}

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta commit.
	 *
	 */
	public void closeTransac(){
		try{
			this.conn.commit();
			this.stmt.close();
			this.conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_ConsultaOperDAO::closeConnTransac:: Problemas al cerrar la conexi�n->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta rollback.
	 *
	 */
	public void closeTransacError(){
		try{
			this.conn.rollback();
			this.stmt.close();
			this.conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_ConsultaOperDAO::closeConnTransac:: Problemas al cerrar la conexi�n con errores->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo para realizar la consulta de operaciones reuters
	 *
	 * @param Where				    Condici�n de consulta
	 * @return OperacionesRETCons	Lista de Operaciones a exportar
	 */
	public Vector consulta(String Where, int indice)
	throws Exception {
		String query = null;
		ResultSet rs = null;
		RET_OperacionVO bean = null;
		int ind = 0;
		int numRegistros = 0;
		Vector OperacionesRETCons = new Vector();

		EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consulta:: Entrando a consulta", EIGlobal.NivelLog.DEBUG);
		try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query  = "Select a.FOLIO_ENLA as folioEnla,"
				   + "a.FOLIO_OPER_LIQ as folioOperLiq,"
				   + "a.DIVISA_OPERANTE as divisaOperante,"
				   + "a.CONTRA_DIVISA as contraDivisa,"
				   + "a.TIPO_OPERACION as tipoOperacion,"
				   + "a.TC_PACTADO as tcPactado,"
				   + "a.IMPORTE_TOT_OPER as importeTotOper,"
				   + "to_char(a.FCH_HORA_PACTADO, 'DD-MM-YYYY HH24:MI:SS') as fchHrPactado, "
				   + "to_char(a.FCH_HORA_COMPLEMENT, 'DD-MM-YYYY HH24:MI:SS') as fchHrComplement, "
				   + "to_char(a.FCH_HORA_LIBERADO, 'DD-MM-YYYY HH24:MI:SS') as fchHrLiberado, "
				   + "a.USR_REGISTRO as usrRegistro,"
				   + "a.USR_AUTORIZA as usrAutoriza,"
				   + "a.ESTATUS_OPERACION as estatusOperacion "
				   + "From EWEB_OPER_REUTERS a "
				   + Where
			       + " order by a.FCH_HORA_PACTADO asc";
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consulta:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query,ResultSet.TYPE_SCROLL_INSENSITIVE,
	        		ResultSet.CONCUR_UPDATABLE);
			rs = sDup.executeQuery();
			ind = getRecordDesde(indice);
			if(ind>0){
				rs.absolute(ind);
			}

			while(rs.next() && numRegistros<RET_ControlConsOper.MAX_REGISTROS_PAG){
				bean = new RET_OperacionVO();
				bean.setFolioEnla(rs.getString("folioEnla")!=null?rs.getString("folioEnla"):"");
				bean.setFolioOper(rs.getString("folioOperLiq")!=null?rs.getString("folioOperLiq"):"");
				bean.setDivisaOperante(rs.getString("divisaOperante")!=null?rs.getString("divisaOperante"):"");
				bean.setContraDivisa(rs.getString("contraDivisa")!=null?rs.getString("contraDivisa"):"");
				bean.setTipoOperacion(rs.getString("tipoOperacion")!=null?rs.getString("tipoOperacion"):"");
				bean.setTcPactado(rs.getString("tcPactado")!=null?rs.getString("tcPactado"):"");
				bean.setImporteTotOper(rs.getString("importeTotOper")!=null?rs.getString("importeTotOper"):"");
				bean.setFchHoraPactado(rs.getString("fchHrPactado")!=null?rs.getString("fchHrPactado"):"");
				bean.setFchHoraComplemento(rs.getString("fchHrComplement")!=null?rs.getString("fchHrComplement"):"");
				bean.setFchHoraLiberado(rs.getString("fchHrLiberado")!=null?rs.getString("fchHrLiberado"):"");
				bean.setUsrRegistro(rs.getString("usrRegistro")!=null?rs.getString("usrRegistro"):"");
				bean.setUsrAutoriza(rs.getString("usrAutoriza")!=null?rs.getString("usrAutoriza"):"");
				bean.setEstatusOperacion(rs.getString("estatusOperacion")!=null?rs.getString("estatusOperacion"):"");
				if(!bean.getEstatusOperacion().trim().equals("L") && !bean.getEstatusOperacion().trim().equals("O"))
					bean.setFolioOper(" ");

				bean.setIdRecord("" + numRegistros++);
				OperacionesRETCons.add(bean);
			}

			rs.close();
			sDup.close();
			conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consulta:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return OperacionesRETCons;
	}

	/**
	 * Metodo para realizar la consulta del detalle de una operacione reuters
	 *
	 * @param Where				    Condici�n de consulta
	 * @return OperacionesRETCons	Lista de Operaciones a exportar
	 */
	public Vector consultaDetalle(String Where, int indice)
	throws Exception {
		String query = null;
		ResultSet rs = null;
		RET_CuentaAbonoVO bean = null;
		int ind = 0;
		int numRegistros = 0;
		Vector OperacionesRETCons = new Vector();

		EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consultaDetalle:: Entrando a consulta", EIGlobal.NivelLog.DEBUG);
		try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query  = "Select a.FOLIO_OPER_ABON as folioOperAbono,"
				   + "a.FOLIO_TRANSFER as folioTransfer,"
				   + "a.CUENTA_ABONO as cuentaAbono,"
				   + "a.IMPORTE as importe,"
				   + "a.TIPO_CUENTA as tipoCuenta,"
				   + "a.ESTATUS_OPERACION as estatusOperacion "
				   + "From EWEB_MONTOS_REUTERS a "
				   + Where;
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consultaDetalle:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query,ResultSet.TYPE_SCROLL_INSENSITIVE,
	        		ResultSet.CONCUR_UPDATABLE);
			rs = sDup.executeQuery();
			ind = getRecordDesde(indice);
			if(ind>0){
				rs.absolute(ind);
			}

			while(rs.next() && numRegistros<RET_ControlConsOper.MAX_REGISTROS_PAG){
				bean = new RET_CuentaAbonoVO();
				bean.setFolioOperAbono(rs.getString("folioOperAbono")!=null?rs.getString("folioOperAbono"):"");
				bean.setFolioTransfer(rs.getString("folioTransfer")!=null?rs.getString("folioTransfer"):"");
				bean.setNumCuenta(rs.getString("cuentaAbono")!=null?rs.getString("cuentaAbono"):"");
				bean.setImporteAbono(rs.getString("importe")!=null?rs.getString("importe"):"");
				bean.setTipoCuenta(rs.getString("tipoCuenta")!=null?rs.getString("tipoCuenta"):"");
				bean.setEstatusOperacion(rs.getString("estatusOperacion")!=null?rs.getString("estatusOperacion"):"");

				bean.setIdRegistro(numRegistros);
				OperacionesRETCons.add(bean);
				numRegistros++;
			}

			rs.close();
			sDup.close();
			conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consultaDetalle:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return OperacionesRETCons;
	}

	/**
	 * Metodo encargado de identificar el registro inicial por pagina
	 *
	 * @param indice			Identificador de la pagina
	 * @return 					El valor del registro inicial de la pagina
	 */
	public int getRecordDesde(int indice) {
		return ((indice-1) * RET_ControlConsOper.MAX_REGISTROS_PAG);
	}

	/**
	 * Metodo funcional encargado de obtener el registro final por pagina
	 *
	 * @param indice				Identificador de la pagina
	 * @param numTotalPags			Total de Paginas
	 * @param numTotalRegistros		Total de registros
	 * @return						El valor del registro final de la pagina
	 */
	public int getRecordHasta(int indice, int numTotalPags, int numTotalRegistros) {
		if(indice==numTotalPags){
			return numTotalRegistros;
		}else{
			return indice * RET_ControlConsOper.MAX_REGISTROS_PAG;
		}
	}

	/**
     * Metodo encargado de determinar el numero total de paginas de un query
     * ejecutado
     *
     * @param bean				Objeto contenedor de ConDevTesofeVo
     * @param where 			Condicion de la consulta entrante
     * @return numTotalPags 	Total de paginas del query
     */
    public int getNumPages(String where)
    throws Exception {

    	ResultSet rs = null;
    	String query = null;
    	double numTotalRegs = 0;
    	int numTotalPags = 0;

    	try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query = "Select count(*) as numTotalRegs "
		          + "from EWEB_OPER_REUTERS a ";
			query = query + where;
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::getNumPages:: Query->" + query, EIGlobal.NivelLog.DEBUG);

			if(rs.next()){
				numTotalRegs = rs.getDouble("numTotalRegs");
				numTotalPags = new Double(Math.ceil(numTotalRegs
							  / RET_ControlConsOper.MAX_REGISTROS_PAG)).intValue();
			}

    	}catch (Exception e) {
    		throw new Exception("Error: de Base de datos, total de paginas");
		}
		return numTotalPags;
    }

    /**
     * Metodo encargado de determinar el numero total de registros de un query
     * ejecutado
     *
     * @param bean				Objeto contenedor de ConDevTesofeVo
     * @param where 			Condicion de la consulta entrante
     * @return numTotalRegs 	Total de registros
     */
	public int getNumRecords(String where)
	throws Exception {

		ResultSet rs = null;
    	String query = null;
    	int numTotalRegs = 0;

    	try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query = "Select count(*) as numTotalRegs "
		          + "from EWEB_OPER_REUTERS a ";
			query = query + where;
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::getNumRecords:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();

			if(rs.next()){
				numTotalRegs = rs.getInt("numTotalRegs");
			}

    	}catch (Exception e) {
    		throw new Exception("Error: de Base de datos, total de registros");
		}
		return numTotalRegs;
    }

	/**
     * Metodo encargado de determinar el numero total de paginas de un query
     * ejecutado
     *
     * @param bean				Objeto contenedor de ConDevTesofeVo
     * @param where 			Condicion de la consulta entrante
     * @return numTotalPags 	Total de paginas del query
     */
    public int getNumPagesDetalle(String where)
    throws Exception {

    	ResultSet rs = null;
    	String query = null;
    	double numTotalRegs = 0;
    	int numTotalPags = 0;

    	try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query = "Select count(*) as numTotalRegs "
		          + "from EWEB_MONTOS_REUTERS a ";
			query = query + where;
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::getNumPages:: Query->" + query, EIGlobal.NivelLog.DEBUG);

			if(rs.next()){
				numTotalRegs = rs.getDouble("numTotalRegs");
				numTotalPags = new Double(Math.ceil(numTotalRegs
							  / RET_ControlConsOper.MAX_REGISTROS_PAG)).intValue();
			}

    	}catch (Exception e) {
    		throw new Exception("Error: de Base de datos, total de paginas");
		}
		return numTotalPags;
    }

    /**
     * Metodo encargado de determinar el numero total de registros de un query
     * ejecutado
     *
     * @param bean				Objeto contenedor de ConDevTesofeVo
     * @param where 			Condicion de la consulta entrante
     * @return numTotalRegs 	Total de registros
     */
	public int getNumRecordsDetalle(String where)
	throws Exception {

		ResultSet rs = null;
    	String query = null;
    	int numTotalRegs = 0;

    	try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query = "Select count(*) as numTotalRegs "
		          + "from EWEB_MONTOS_REUTERS a ";
			query = query + where;
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::getNumRecords:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();

			if(rs.next()){
				numTotalRegs = rs.getInt("numTotalRegs");
			}

    	}catch (Exception e) {
    		throw new Exception("Error: de Base de datos, total de registros");
		}
		return numTotalRegs;
    }

	/**
	 * Metodo Privado para regresar un bean con los titulos de la
	 * exportaci�n
	 *
	 * @return RET_OperacionVO	-	Objeto Contenedor de Tipo RET_OperacionVO
	 */
	private RET_OperacionVO cabeceraExport(){
		RET_OperacionVO bean = new RET_OperacionVO();
		bean.setFolioEnla("Folio Enlace");
		bean.setFolioRet("Folio Reuters");
		bean.setFolioOper("Folio Operaci�n");
		bean.setUsrRegistro("Usuario Registro");
		bean.setUsrAutoriza("Usuario Autoriza");
		bean.setCtaCargo("Cuenta Cargo");
		bean.setImporteTotOper("Importe Operaci�n");
		bean.setDivisaOperante("Divisa Operante");
		bean.setContraDivisa("Contra Divisa");
		bean.setTipoOperacion("Tipo de Operaci�n");
		bean.setTcPactado("Tipo de Cambio");
		bean.setConcepto("Concepto Operaci�n");
		bean.setEstatusOperacion("Estatus Operaci�n");
		bean.setFchHoraPactado("Fecha y Hora de Pactado");
		bean.setFchHoraComplemento("Fecha y Hora de Complementado");
		bean.setFchHoraLiberado("Fecha y Hora de Liberado");
		return bean;

	}

	/**
	 * Metodo para realizar la exportaci�n de las operaciones reuters
	 *
	 * @param Where				    Condiciones de consulta
	 * @return OperacionesRETCons	Lista de Operaciones a exportar
	 */
	public Vector exporta(String Where)
	throws Exception {
		String query = null;
		ResultSet rs = null;
		RET_OperacionVO bean = null;
		int ind = 0;
		int numRegistros = 0;
		Vector OperacionesRETCons = new Vector();

		EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::exporta:: Entrando a consulta", EIGlobal.NivelLog.DEBUG);
		try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query  = "Select a.FOLIO_ENLA as folioEnla,"
				   + "a.FOLIO_RET as folioRet,"
				   + "a.FOLIO_OPER_LIQ as folioOperLiq,"
				   + "a.USR_REGISTRO as usrRegistro,"
				   + "a.USR_AUTORIZA as usrAutoriza,"
				   + "a.CTA_CARGO as ctaCargo,"
				   + "a.IMPORTE_TOT_OPER as importeTotOper,"
				   + "a.DIVISA_OPERANTE as divisaOperante,"
				   + "a.CONTRA_DIVISA as contraDivisa,"
				   + "a.TIPO_OPERACION as tipoOperacion,"
				   + "a.TC_PACTADO as tcPactado,"
				   + "a.CONCEPTO as concepto,"
				   + "a.ESTATUS_OPERACION as estatusOperacion,"
				   + "to_char(a.FCH_HORA_PACTADO, 'DD-MM-YYYY HH24:MI:SS') as fchHrPactado,"
				   + "to_char(a.FCH_HORA_COMPLEMENT, 'DD-MM-YYYY HH24:MI:SS') as fchHrComplement,"
				   + "to_char(a.FCH_HORA_LIBERADO, 'DD-MM-YYYY HH24:MI:SS') as fchHrLiberado "
				   + "From EWEB_OPER_REUTERS a "
				   + Where;
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::exporta:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();

			OperacionesRETCons.add(cabeceraExport());
			while(rs.next()){
				bean = new RET_OperacionVO();
				bean.setFolioEnla(rs.getString("folioEnla")!=null?rs.getString("folioEnla"):"");
				bean.setFolioRet(rs.getString("folioRet")!=null?rs.getString("folioRet"):"");
				bean.setFolioOper(rs.getString("folioOperLiq")!=null?rs.getString("folioOperLiq"):"");
				bean.setUsrRegistro(rs.getString("usrRegistro")!=null?rs.getString("usrRegistro"):"");
				bean.setUsrAutoriza(rs.getString("usrAutoriza")!=null?rs.getString("usrAutoriza"):"");
				bean.setCtaCargo(rs.getString("ctaCargo")!=null?rs.getString("ctaCargo"):"");
				bean.setImporteTotOper(rs.getString("importeTotOper")!=null?rs.getString("importeTotOper"):"");
				bean.setDivisaOperante(rs.getString("divisaOperante")!=null?rs.getString("divisaOperante"):"");
				bean.setContraDivisa(rs.getString("contraDivisa")!=null?rs.getString("contraDivisa"):"");
				bean.setTipoOperacion(rs.getString("tipoOperacion")!=null?rs.getString("tipoOperacion"):"");
				bean.setTcPactado(rs.getString("tcPactado")!=null?rs.getString("tcPactado"):"");
				bean.setConcepto(rs.getString("concepto")!=null?rs.getString("concepto"):"");
				bean.setEstatusOperacion(rs.getString("estatusOperacion")!=null?rs.getString("estatusOperacion"):"");
				bean.setFchHoraPactado(rs.getString("fchHrPactado")!=null?rs.getString("fchHrPactado"):"");
				bean.setFchHoraComplemento(rs.getString("fchHrComplement")!=null?rs.getString("fchHrComplement"):"");
				bean.setFchHoraLiberado(rs.getString("fchHrLiberado")!=null?rs.getString("fchHrLiberado"):"");
				bean.setIdRecord("" + numRegistros++);
				OperacionesRETCons.add(bean);
			}

			rs.close();
			sDup.close();
			conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::exporta:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}

		return OperacionesRETCons;
	}

	/**
	 * Metodo encargado de regresar los dias validos a cargar.
	 *
	 * @param formato					Formato de Consulta
	 * @return diasInhabiles			Vector con dias inhabiles
	 * @throws Exception				Error de tipo SQLException
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	public Vector CargarDias(int formato)throws Exception{
		String query = null;
		ResultSet rs = null;
		Vector diasInhabiles = new Vector();
		Connection conn= null;
		PreparedStatement sDup = null;

		EIGlobal.mensajePorTrace ("ConsAltaBenefPagOcurreDAO::CargarDias:: Entrando a CargaDias", EIGlobal.NivelLog.DEBUG);
		try{
			conn = createiASConn (Global.DATASOURCE_ORACLE );
			query  = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') "
				   + "From comu_dia_inhabil "
				   + "Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2) ";
			 sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();
			while(rs.next()){
				String dia  = rs.getString(1);
				String mes  = rs.getString(2);
				String anio = rs.getString(3);
				if(formato == 0){
	                  dia  =  Integer.toString( Integer.parseInt(dia) );
	                  mes  =  Integer.toString( Integer.parseInt(mes) );
	                  anio =  Integer.toString( Integer.parseInt(anio) );
				}
				String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
				diasInhabiles.addElement(fecha);
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::CargarDias:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return diasInhabiles;
	}

}