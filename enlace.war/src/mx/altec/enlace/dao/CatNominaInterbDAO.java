package mx.altec.enlace.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ibm.ws.rsadapter.jdbc.WSJdbcCallableStatement;
import com.ibm.ws.rsadapter.jdbc.WSJdbcConnection;
import com.ibm.ws.rsadapter.jdbc.WSJdbcUtil;


import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.jdbc.driver.OracleConnection;


import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailContainer;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.bo.CatNominaConstantes;
import mx.altec.enlace.bo.CatNominaInterbConstantes;
import mx.altec.enlace.bo.CatNominaInterbBean;
import mx.altec.enlace.bo.RespInterbBean;

/* everis JARA - Ambientacion - Extiende de Base Servlet para acceder a metodo createiASConn */
public class CatNominaInterbDAO //extends BaseServlet
{
		/* everis JARA - Ambientacion - Agregado PreparedStatement y Connection */
		private PreparedStatement statement =null;
		private Connection conn=null;
		/*Conexion GFI*/
		private PreparedStatement statement2 =null;
		private Connection conn2=null;
		String query = "";
	

	/***************************************************************
	 *** Metodo que realiza la buusqueda de empleados 	 		****
	 *** con base en los parametros introducidos				****
	 **************************************************************/
	/*** Ultima Modificacion: 04 Jun 2008 						****
	 *** Autor:Emmanuel Sanchez Castillo (ESC)			 		****
	 *** Modificacion:se modifica la consulta para conderar 	****
	 *** el estado pendiente activar							****
	 *** Se agrega a la consulta el campo estatus				****
	 ***************************************************************/
	public String[][] buscaEmpleado(String numContrato, String radioValue, String valorBusqueda, int totalReg)
	{
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - El contrato es: " + numContrato, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - El filtro de la busqueda es: " + radioValue, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - El valor de la busqueda es: " + valorBusqueda, EIGlobal.NivelLog.DEBUG);

		String filtroBusqueda = "";
		ResultSet rs = null;
		String[][] arrBiDatosQuery = null;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle conn = new ConexionOracle();
		CatNominaInterbBean bean = new CatNominaInterbBean();

		try
		{
			//Crear(); Prepared Statement
			String tablasBusq = CatNominaInterbConstantes._CAT_NOM;
			filtroBusqueda = generaFiltroBusqueda(numContrato, radioValue, valorBusqueda);

			//Obtenemos el total de registros que traera la consulta
			//totalReg = cuentaQueryBusqueda(filtroBusqueda, tablasBusq);
			EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - El total de registros son =" + totalReg, EIGlobal.NivelLog.DEBUG);

			// Se construye query para las busqueda de empleados.
			query = "SELECT " + CatNominaInterbConstantes._CONTRATO + ", "
					+ CatNominaInterbConstantes._NUM_CTAE + ", "
					+ CatNominaInterbConstantes._CVE_INT + ", "
					+ CatNominaInterbConstantes._ULT_VER + ", "
					+ CatNominaInterbConstantes._NOM + ", "
					+ CatNominaInterbConstantes._PZA_BAN + ", "
					+ CatNominaInterbConstantes._SUC + ", "
					+ CatNominaInterbConstantes._TPO_CTA + " "
					+ " FROM " + tablasBusq + " WHERE " + filtroBusqueda
					+ " ORDER BY " + CatNominaInterbConstantes._NOM  + " ASC";

			EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - El query BUSCA a realizar es =" + query, EIGlobal.NivelLog.DEBUG);
			// VSWF - JGAL - Temporal, se debe llamar al metodo crear
			Crear(query);
			if(totalReg>0){
				//rs = Consulta(query);
				rs = Consulta();
				arrBiDatosQuery = separaRSBusquedaEmpleados(rs, totalReg); // Guardamos los resultados en un arreglo Bidimensional
				// buscarBancos(arrBiDatosQuery);
				EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - El tama�o del arreglo es: " + arrBiDatosQuery.length +
						" Renglones", EIGlobal.NivelLog.DEBUG);
			}
			else {
				arrBiDatosQuery = new String[1][1];
				arrBiDatosQuery[0][0] = CatNominaConstantes._ERROR;
				bean.setExistenReg(false);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			arrBiDatosQuery = new String[1][1];
			arrBiDatosQuery[0][0] = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.ERROR);
						}
				}
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - Resultado: " + arrBiDatosQuery[0][0], EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [buscaEmpleado] - Fin", EIGlobal.NivelLog.DEBUG);
		return arrBiDatosQuery;
	}


	/***************************************************************
	 *** Metodo que lee el resultSet de busqueda de empleados	****
	 *** y almacena los valores en un arreglo bidimensional		****
	 **************************************************************/
	public String[][] separaRSBusquedaEmpleados(ResultSet rs, int numRows)
	{
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [separaRSBusquedaEmpleados] - Inicio ", EIGlobal.NivelLog.DEBUG);

		int row = 0;
		String[][] arrBiDatosEmpl = new String[numRows][CatNominaInterbConstantes._TOTAL_COL];
		try
		{
			while(rs.next()){
				int colum = 0;
				if(rs.getString("NUM_CUENTA2") != null)  // 0
					arrBiDatosEmpl[row][colum++] = rs.getString("NUM_CUENTA2");
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("NUM_CUENTA_EXT") != null) // 1
					arrBiDatosEmpl[row][colum++] = rs.getString("NUM_CUENTA_EXT");
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("CVE_INTERME") != null) // 2
					arrBiDatosEmpl[row][colum++]= rs.getString("CVE_INTERME");
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("U_VERSION") != null) // 3
					arrBiDatosEmpl[row][colum++] = rs.getString("U_VERSION");
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("NOMBRE") != null)  //4
					arrBiDatosEmpl[row][colum++] = rs.getString("NOMBRE");
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("PLAZA_BANXICO") != null)  //5
					arrBiDatosEmpl[row][colum++] = rs.getString("PLAZA_BANXICO");
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("SUCURSAL") != null)  // 6
					arrBiDatosEmpl[row][colum++] = rs.getString("SUCURSAL");
				else
					arrBiDatosEmpl[row][colum++] = "";
				if(rs.getString("TIPO_CUENTA") != null)  // 7
					arrBiDatosEmpl[row][colum++] = rs.getString("TIPO_CUENTA");	// ultimo cambio ++
				else
					arrBiDatosEmpl[row][colum++] = "";
				String nombre_corto = "";
				EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [separaRSBusquedaEmpleados] - Cuenta: " +
						rs.getString("NUM_CUENTA2") + ", Tipo de Cuenta: " + rs.getString("TIPO_CUENTA") + ", Interme: " +
						rs.getString("CVE_INTERME"), EIGlobal.NivelLog.DEBUG);
				if(rs.getString("TIPO_CUENTA") != null )
					if(rs.getString("TIPO_CUENTA").equals("40") && rs.getString("NUM_CUENTA_EXT").length() >= 3)
						nombre_corto = obtenNombreBanco(Integer.parseInt(rs.getString("NUM_CUENTA_EXT").substring(0, 3)));
					else
						nombre_corto = obtenNombreBanco(rs.getString("CVE_INTERME"));
					EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [separaRSBusquedaEmpleados] - Nombre Corto: " +
								nombre_corto, EIGlobal.NivelLog.DEBUG);

				arrBiDatosEmpl[row][colum++] = nombre_corto;	// ultimo cambio ++
				row++;  //Movemos el renglon
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return arrBiDatosEmpl;
	}




	protected String obtenNombreBanco(int cveBanco) {
		ResultSet rs = null;
		String nombreBanco = "";
		try {
			query = "SELECT " + CatNominaInterbConstantes._NOM_BANCO + " FROM " + CatNominaInterbConstantes._COMU_INTERME +
			" WHERE " + CatNominaInterbConstantes._NUM_BANCO + " = " + cveBanco;
			Crear2(query);
			rs = Consulta2();
			if(rs.next()) {
				nombreBanco = rs.getString(CatNominaInterbConstantes._NOM_BANCO);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement2!=null){
						statement2.close();
						statement2=null;}
					if(conn2!=null){
						conn2.close();
						conn2=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
						}
				}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenNombreBanco: " + nombreBanco, EIGlobal.NivelLog.INFO);
		return nombreBanco;
	}

	protected String obtenNombreBanco(String cveBanco) {
		ResultSet rs = null;
		String nombreBanco = "";
		int cveBancoInt = 0;

		try {
		    cveBancoInt = Integer.parseInt(cveBanco);
		    return obtenNombreBanco(cveBancoInt);
		}
		catch (NumberFormatException e) {}


		try {
			query = "SELECT " + CatNominaInterbConstantes._NOM_BANCO + " FROM " + CatNominaInterbConstantes._COMU_INTERME +
			" WHERE CVE_INTERME = '" + cveBanco + "'";
			Crear2(query);
			rs = Consulta2();
			if(rs.next()) {
				nombreBanco = rs.getString(CatNominaInterbConstantes._NOM_BANCO);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement2!=null){
						statement2.close();
						statement2=null;}
					if(conn2!=null){
						conn2.close();
						conn2=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
						}
				}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenNombreBanco: " + nombreBanco, EIGlobal.NivelLog.INFO);
		return nombreBanco;
	}


	public String[] obtenDatosBanco(int cveBanco) {
		ResultSet rs = null;
		String[] dscBanco = {"", ""};
		try {
			query = "SELECT " + CatNominaInterbConstantes._NOM_BANCO + ", " + CatNominaInterbConstantes._INTERME
			+ " FROM " + CatNominaInterbConstantes._COMU_INTERME +
			" WHERE " + CatNominaInterbConstantes._NUM_BANCO + " = " + cveBanco;
			Crear2(query);
			rs = Consulta2();
			if(rs.next()) {
				dscBanco[0] = rs.getString(CatNominaInterbConstantes._NOM_BANCO);
				dscBanco[1] = rs.getString(CatNominaInterbConstantes._INTERME);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement2!=null){
						statement2.close();
						statement2=null;}
					if(conn2!=null){
						conn2.close();
						conn2=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
						}
				}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenDatosBanco: [" + dscBanco[0] + "," + dscBanco[1] + "]", EIGlobal.NivelLog.INFO);
		return dscBanco;
	}

	public String obtenListaBancos() {
		ResultSet rs = null;
		StringBuffer lstTmp = new StringBuffer();
		lstTmp.append("|");
		try {
			query = "SELECT cve_interme, num_cecoban from comu_interme_fin where num_cecoban<>0 order by num_cecoban";
			Crear2(query);
			rs = Consulta2();
			while (rs.next()) {
				if(!rs.getString("CVE_INTERME").equals("SERFI") && !rs.getString("CVE_INTERME").equals("BANME"))
				  {
					lstTmp.append(rs.getString("NUM_CECOBAN") + "|");
				  }
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement2!=null){
						statement2.close();
						statement2=null;}
					if(conn2!=null){
						conn2.close();
						conn2=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
						}
				}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenListaBancos: [" + lstTmp.toString() + "]", EIGlobal.NivelLog.INFO);
		return lstTmp.toString();
	}

	public HashMap obtenListaInterme(HashMap listaCecoban) {
		// Si forma = "INTERME" la llave es la clave Interme (String)
		// Si forma = "CECOBAN" la llave es el num cecoban
		ResultSet rs = null;
		Integer numCecoban = null;
		String cveInterme = "";
		HashMap lista = new HashMap();
		try {
			query = "SELECT cve_interme, num_cecoban from comu_interme_fin where num_cecoban<>0 order by num_cecoban";
			Crear2(query);
			rs = Consulta2();
			while (rs.next()) {
				numCecoban = new Integer(rs.getString("NUM_CECOBAN"));
				cveInterme = rs.getString("CVE_INTERME");
				listaCecoban.put(numCecoban, cveInterme );
				lista.put(cveInterme, numCecoban);
			}
			
		}
		catch (Exception e) {
			lista = new HashMap();
			listaCecoban = new HashMap();
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenListaInterme - Exception:  ["+ e + "]", EIGlobal.NivelLog.ERROR);
		}
		finally{
			try{
				if(rs!=null){
					rs.close();
					rs=null;}
				if(statement2!=null){
					statement2.close();
					statement2=null;}
				if(conn2!=null){
					conn2.close();
					conn2=null;}
			}catch(Exception e1){
					EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenListaInterme - size: CECOBAN [" + listaCecoban.size() + "] e INTERME : [" + lista.size() + "]", EIGlobal.NivelLog.INFO);
		return lista;
	}

/*
 * public String traeListaBancos()
{
	String lista = "";
   String sqlQuery = "SELECT cve_interme, num_cecoban from comu_interme_fin where num_cecoban<>0 order by num_cecoban";

	Hashtable result=null;

	EI_Query BD= new EI_Query();

	result=BD.ejecutaQuery(sqlQuery);
	if(result==null) return lista;
	String[] columns=new String[BD.getNumeroColumnas()];

	EIGlobal.mensajePorTrace("CatNominaInterbDAO - traeListaBancos(): Numero de datos. " + result.size() +" Columnas. " + BD.getNumeroColumnas(), EIGlobal.NivelLog.ERROR);
	StringBuffer lstTmp = new StringBuffer();
	lstTmp.append("|");
	if(BD.getNumeroColumnas()==2)
	  {
		for(int i=0;i<result.size();i++)
		  {
			columns=(String[])result.get(""+i);

			if(!columns[0].trim().equals("SERFI") && !columns[0].trim().equals("BANME"))
			  {
				lstTmp.append(columns[1] + "|");
			  }
		  }
	  }
	return lstTmp.toString();
 }

 */

	/***************************************************************
	 *** M?todo que realiza la b?squeda de empleados 	 		****
	 *** con base en los parametros introducidos				****
	 *** Ultima Modificacion: 04 Jun 2008 						****
	 *** Autor:Emmanuel Sanchez Castillo (ESC)			 		****
	 *** Modificaci?n:se modifica la consulta para conderar 	****
	 *** el estado pendiente activar					 		****
	 ***************************************************************/
	public int cuentaQueryBusqueda(String filtroBusqueda, String tablasBusqueda)
	{
	    EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [cuentaQueryBusqueda] - El filtro es: " + filtroBusqueda, EIGlobal.NivelLog.DEBUG);
		ResultSet rs = null;
		int totalReg = 0;
		query = "SELECT COUNT(*) AS " + CatNominaInterbConstantes._TOTAL + " FROM " + tablasBusqueda +
				" WHERE " + filtroBusqueda;

		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [cuentaQueryBusqueda] - El query COUNT a realizar es =" +
				query, EIGlobal.NivelLog.DEBUG);
		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				totalReg = rs.getInt("TOTAL");
			}
		}
		catch (Exception e){
			e.printStackTrace();
			totalReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - [cuentaQueryBusqueda] - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
						}
				}

		System.out.println("CatNominaInterbDAO - cuentaQueryBusqueda $ Fin");
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [cuentaQueryBusqueda] - Fin", EIGlobal.NivelLog.DEBUG);
		return totalReg;
	}



	/******************************************************************
	 *** M?todo que realiza la actualizaci?n de los datos en la 	***
	 *** las tablas del nuevo cat?logo de n?mina. 	 	 			***
	 *****************************************************************/
	public boolean actualizaDatosEmpl(String[] datosEmpleados, String usuario)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - actualizaDatosEmpl $ Inicio");
		System.out.println("CatNominaInterbDAO - actualizaDatosEmpl $ El numContrato es:" + datosEmpleados[3]);
		System.out.println("CatNominaInterbDAO - actualizaDatosEmpl $ El numCuenta es:" + datosEmpleados[4]);

	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ResultSet rs = null;
		String query = "";
		String query1 = "";
		String query2 = "";
		String query3 = "";
		boolean valReturn = true;

		//ConexionOracle = new ConexionOracle();

		try
		{
			//Crear();

			/* jp@everis */
			if( datosEmpleados.length == 5 ){
				System.out.println("PROPIETARIO");
				query = "UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._NUM_EMPL + " = '" + datosEmpleados[0] + "', " +
					CatNominaConstantes._DEPTO_EMPL + " = '" + datosEmpleados[1] + "', " + CatNominaConstantes._SUELDO + " = " + datosEmpleados[2] + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '" + usuario + "', " + CatNominaConstantes._FCH_MODIF_REG_EVER + " = " + "SYSDATE" +
					" WHERE " + CatNominaConstantes._CONTR + " = '" + datosEmpleados[3] + "' AND " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[4] +
					"' AND " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'";

				System.out.println("CatNominaInterbDAO - generaQueryActualiaEmpl $ Query de Modificaci?n = " + query);
				Crear(query);
				//Inserta(query);
				Inserta();
			}
			else{
				System.out.println("Interbancaria");
				query1 = "UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._NUM_EMPL + " = '" + datosEmpleados[0] + "', " +
						CatNominaConstantes._DEPTO_EMPL + " = '" + datosEmpleados[1] + "', " + CatNominaConstantes._SUELDO + " = " + datosEmpleados[2] + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '" + usuario + "', " + CatNominaConstantes._FCH_MODIF_REG_EVER + " = " + "SYSDATE" +
						" WHERE " + CatNominaConstantes._CONTR + " = '" + datosEmpleados[3].trim() + "' AND " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[29].trim() + "'";
				query2 = "UPDATE " + CatNominaConstantes._CAT_NOM + " SET " + CatNominaConstantes._APE_PAT + " = '" + datosEmpleados[5] + "', " +
						CatNominaConstantes._APE_MAT + " = '" + datosEmpleados[6] + "', " + CatNominaConstantes._SEXO + " = '" + datosEmpleados[8] + "', " +
						CatNominaConstantes._CALLE_NUM + " = '" + datosEmpleados[9] + "', " + CatNominaConstantes._COLONIA + " = '" + datosEmpleados[10] + "', " +
						CatNominaConstantes._DELEG_MUN + " = '" + datosEmpleados[11] + "', " + CatNominaConstantes._CVE_EDO + " = '" + datosEmpleados[12] + "', " +
						CatNominaConstantes._CIUDAD_POB + " = '" + datosEmpleados[13] + "', " + CatNominaConstantes._CP + " = '" + datosEmpleados[14] + "', " +
						CatNominaConstantes._CVE_PAIS + " = '" + datosEmpleados[15] + "', " + CatNominaConstantes._PREF_PART + " = '" + datosEmpleados[16] + "', " +
						CatNominaConstantes._NUM_PART + " = '" + datosEmpleados[17] + "' " +
						" WHERE " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[29].trim() + "'";
				query3 = "UPDATE " + CatNominaConstantes._CONTR_CAT + " SET " + CatNominaConstantes._CALLE_OF + " = '" + datosEmpleados[18] + "', " +
						CatNominaConstantes._COL_OF + " = '" + datosEmpleados[19] + "', " + CatNominaConstantes._DELEG_OF + " = '" + datosEmpleados[20] + "', " +
						CatNominaConstantes._EDO_OF + " = '" + datosEmpleados[21] + "', " + CatNominaConstantes._CIUDAD_OF + " = '" + datosEmpleados[22] + "', " +
						CatNominaConstantes._CP_OF + " = '" + datosEmpleados[23] + "', " + CatNominaConstantes._PAIS_OF + " = '" + datosEmpleados[24] + "', " +
						CatNominaConstantes._CVE_DIREC + " = '" + datosEmpleados[25] + "', " + CatNominaConstantes._PREF_OF + " = '" + datosEmpleados[26] + "', " +
						CatNominaConstantes._NUMERO_OF + " = '" + datosEmpleados[27] + "', " + CatNominaConstantes._EXT_OF + " = '" + datosEmpleados[28] + "' " +
						" WHERE " + CatNominaConstantes._CONTR + " = '" + datosEmpleados[3].trim() + "'";

				System.out.println(query1);
				System.out.println(query2);
				System.out.println(query3);

				Crear(query1);
				//Inserta(query1);
				Inserta();
				Crear(query2);
				//Inserta(query2);
				Inserta();
				Crear(query3);
				//Inserta(query3);
				Inserta();
			}
			/* jp@everis */
		}
		catch (Exception e) {
			e.printStackTrace();
			valReturn = true;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		return valReturn;

	}


	/******************************************************************
	 *** M?todo que realiza la baja l?gica de la cuenta de un		***
	 *** empleado													***
	 *****************************************************************/
	public int bajaCuentaContrato(String numContrato, String numCuenta, String usuario)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - bajaCuentaContrato $ Inicio");
		System.out.println("CatNominaInterbDAO - bajaCuentaContrato $ El numContrato es:" + numContrato);
		System.out.println("CatNominaInterbDAO - bajaCuentaContrato $ El numCuenta es:" + numCuenta);
		System.out.println("CatNominaInterbDAO - bajaCuentaContrato $ El usuario es:" + usuario);

	//	CatNominaConstantes cons = new CatNominaConstantes();

		int updateBaja = 0;
		//ConexionOracle = new ConexionOracle();

		try {
			//Crear();
			query = "UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._INA + "', " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '" + usuario + "', " + CatNominaConstantes._FCH_MODIF_STAT_EVER + " = " + "SYSDATE" + " WHERE "
				+ CatNominaConstantes._CONTR + " = '"+numContrato+"' AND " + CatNominaConstantes._CTA_ABO + " ="+ numCuenta;
			Crear(query);

			System.out.println("CatNominaInterbDAO - bajaCuentaContrato $ Query a ejectarse = " + query);
			//updateBaja = Inserta(query);
			updateBaja = Inserta();
		}
		catch (Exception e){
			e.printStackTrace();
			updateBaja = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - bajaCuentaContrato $ FIN");
		return updateBaja;
	}


	/***************************************************************
	 *** Verifica la existencia de una cuenta interbancaria		****
	 *** en el nuevo cat?logo									****
	 **************************************************************/
	public int buscaCuenta(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - buscaCuenta $ Inicio");
		System.out.println("CatNominaInterbDAO - buscaCuenta $ El numContrato es:" + numContrato);
		System.out.println("CatNominaInterbDAO - buscaCuenta $ El numCuenta es:" + numCuenta);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int cuentaReg = 0;

		//ConexionOracle = new ConexionOracle();

		query = "SELECT * FROM " + CatNominaConstantes._DET_CAT + " WHERE " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CONTR + " = " + numContrato + " AND "
				+ CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO + " = " + numCuenta;

		System.out.println("CatNominaInterbDAO - buscaCuenta $ El query busca Cuenta: " + query);

		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()) {
				cuentaReg++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			cuentaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - buscaCuenta $ Relaciones Existentes: " + cuentaReg);
		return cuentaReg;
	}


	/***************************************************************
	 *** Inserta el alta de empleado con cuenta Interbancaria	****
	 *** en el nuevo cat?logo de n?mina		TABLA EWEB_CAT_NOM	****
	 **************************************************************/
	public int altaInterbOnlineCatNom(CatNominaInterbBean bean)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - altaInterbOnlineCatNom $ Inicio");

	//	CatNominaConstantes cons = new CatNominaConstantes();
		int insertaReg = 0;
		int numSecuencia = 0;

		numSecuencia = generaQuerySecuencia();

		//ConexionOracle = new ConexionOracle();
		try {
			//Crear();
			if(numSecuencia != -100)
			{
				query = "INSERT INTO " + CatNominaConstantes._CAT_NOM + "(" + CatNominaConstantes._ID_CAT + ", " + CatNominaConstantes._CTA_ABO + ", " +
								 CatNominaConstantes._APE_PAT + ", " + CatNominaConstantes._APE_MAT + ", " + CatNominaConstantes._NOMBRE + ", " +
								 CatNominaConstantes._RFC + ", " + CatNominaConstantes._SEXO + ", " + CatNominaConstantes._CALLE_NUM + ", " +  CatNominaConstantes._COLONIA + ", " +
								 CatNominaConstantes._DELEG_MUN + ", " +  CatNominaConstantes._CVE_EDO + ", " +  CatNominaConstantes._CIUDAD_POB + ", " + CatNominaConstantes._CP + ", " +
								 CatNominaConstantes._CVE_PAIS + ", " + CatNominaConstantes._PREF_PART + ", " + CatNominaConstantes._NUM_PART + ", " + CatNominaConstantes._STA_PROCE + ", " + CatNominaConstantes._FCH_MOD_STA_PROCE + ") " +
								 " values (" + numSecuencia + ", '" + bean.getNumCuenta()+ "', '" +
								 bean.getApellidoP()+ "', '" + bean.getApellidoM() + "', '" + bean.getNombreEmpl() + "', '" +
								 bean.getRFC()+ "', '" + bean.getSexo() + "', '" + bean.getCalle() + "', '" + bean.getColonia() + "', '" +
								 bean.getDelegacion() + "', '" + bean.getCveEstado() + "', '" + bean.getCiudad() + "', '" + bean.getCodigoPostal() + "', '" +
								 bean.getClavePais() + "', '" + bean.getPrefTelPart() + "', " + bean.getTelPart() +", " + "'A'" + ", " + "SYSDATE" +")";


				System.out.println("CatNominaInterbDAO - altaInterbOnlineCatNom $ Query de Insercion Interb" + query);
				Crear(query);
				//insertaReg = Inserta(query);
				insertaReg = Inserta();
			}
			else
				insertaReg = numSecuencia;
		}
		catch (Exception e) {
			e.printStackTrace();
			insertaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}
		System.out.println("CatNominaInterbDAO - altaInterbOnlineCatNom $ Relaciones Existentes: " + insertaReg);
		return insertaReg;
	}

	/***************************************************************
	 *** Inserta el alta de empleado con cuenta Interbancaria	****
	 *** en el nuevo cat?logo de n?mina		TABLA EWEB_DET_CAT	****
	 **************************************************************/
	public int altaInterbOnlineDetCat(CatNominaInterbBean bean)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - altaInterbOnlineDetCat $ Inicio");

	//	CatNominaConstantes cons = new CatNominaConstantes();
		int insertaReg = 0;

		//ConexionOracle = new ConexionOracle();

		try {
		//	Crear();

			query = "INSERT INTO " + CatNominaConstantes._DET_CAT + "(" + CatNominaConstantes._CONTR + ", " + CatNominaConstantes._CTA_ABO + ", " +
							 CatNominaConstantes._NUM_EMPL + ", " + CatNominaConstantes._DEPTO_EMPL + ", " + CatNominaConstantes._SUELDO + ", " +
							 CatNominaConstantes._TIPO_CTA_EVER + ", " + CatNominaConstantes._STAT_CUENTA_EVER + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + ", " +CatNominaConstantes._FCH_MODIF_REG_EVER + ") " +
							 " values ('" + bean.getNumContrato() + "', '" + bean.getNumCuenta()+ "', '" +
							 bean.getNumEmpl()+ "', '" + bean.getDeptoEmpl() + "', '" + bean.getIngresoMensual() + "', '" +
							 CatNominaConstantes._CTA_INTERB + "', '" + CatNominaConstantes._PEND + "', '" + bean.getCveUsuario() + "', " + "SYSDATE" + ")";

			System.out.println("CatNominaInterbDAO - altaInterbOnlineDetCat $ Query de Insercion Interb" + query);
			Crear(query);
		//	insertaReg = Inserta(query);
			insertaReg = Inserta();
		}
		catch (Exception e) {
			e.printStackTrace();
			insertaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - altaInterbOnlineDetCat $ Alta en DET_CAT: " + insertaReg);
		return insertaReg;
	}

	/***************************************************************
	 *** Verifica la existencia de un contrato en la tabla 		****
	 *** EWEB_CONTR_CAT_NOM
	 **************************************************************/
	public int buscaContrato(String numContrato)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - buscaContrato $ Inicio");
		System.out.println("CatNominaInterbDAO - buscaContrato $ El numContrato es:" + numContrato);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int cuentaReg = 0;

		//ConexionOracle = new ConexionOracle();

		query = "SELECT * FROM " + CatNominaConstantes._CONTR_CAT + " WHERE " + CatNominaConstantes._CONTR + " = " + numContrato;

		System.out.println("CatNominaInterbDAO - buscaContrato $ El query busca Cuenta: " + query);

		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next())
			{
				cuentaReg++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			cuentaReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}


		System.out.println("CatNominaInterbDAO - buscaContrato $ Existe contrato: " + cuentaReg);
		return cuentaReg;
	}
	/***************************************************************
	 *** Inserta el alta de empleado con cuenta Interbancaria	****
	 *** en el nuevo cat?logo de n?mina		TABLA EWEB_CONTR_CAT_NOM	****
	 **************************************************************/
/*	public int altaInterbOnlineContrCat(CatNominaBean bean)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - altaInterbOnlineContrCat $ Inicio");

	//	CatNominaConstantes cons = new CatNominaConstantes();
		int insertaReg = 0;

		//ConexionOracle = new ConexionOracle();

		try {
			//Crear();
		query = "INSERT INTO " + CatNominaConstantes._CONTR_CAT + "(" + CatNominaConstantes._CONTR + ", " + CatNominaConstantes._CALLE_OF + ", " +
						 CatNominaConstantes._COL_OF + ", " + CatNominaConstantes._DELEG_OF + ", " +  CatNominaConstantes._EDO_OF + ", " +  CatNominaConstantes._CIUDAD_OF + ", " +
						 CatNominaConstantes._CP_OF+ ", " + CatNominaConstantes._PAIS_OF + ", " + CatNominaConstantes._CVE_DIREC + ", " + CatNominaConstantes._PREF_OF + ", " +
						 CatNominaConstantes._NUMERO_OF + ", " + CatNominaConstantes._EXT_OF + ") " +
						 " values ('" + bean.getNumContrato() + "', '" + bean.getCalleOfi()+ "', '" +
						 bean.getColoniaOfi() + "', '" + bean.getDelegOfi() + "', '" + bean.getCveEstadoOfi() + "', '" + bean.getCiudadOfi() + "', '" +
						 bean.getCodPostalOfi() + "', '" + bean.getPaisOfi() + "', " + bean.getCveDireccionOfi() + ", " + bean.getPrefTelOfi() + ", " +
						 bean.getTelOfi() + ", " + bean.getExtOfi()+ ")";


		System.out.println("CatNominaInterbDAO - altaInterbOnlineContrCat $ Query de Insercion Interb " + query);
		Crear(query);
		//insertaReg = Inserta(query);
		insertaReg = Inserta();
		}
		catch (Exception e) {
			e.printStackTrace();
			insertaReg = -100;
		}
		//* everis JARA - Ambientacion - Cerrando Conexion
		finally{
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}


		System.out.println("CatNominaInterbDAO - altaInterbOnlineContrCat $ Relaciones Existentes: " + insertaReg);
		return insertaReg;
	}
*/
	/***************************************************************
	 *** Genera un n?mero desde la secuencia para insertarlo en	****
	 *** el cat?logo de empleados								****
	 **************************************************************/
	public int generaQuerySecuencia()
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - generaQuerySecuencia $ Inicio");

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int numSecuencia = 0;

		//ConexionOracle = new ConexionOracle();


		try {
		//	Crear();
			query = "SELECT " + CatNominaConstantes._SEQ_CAT_NOM + "." + CatNominaConstantes._NEXT + " AS " + CatNominaConstantes._NUM_SEC + " FROM DUAL";
			System.out.println("CatNominaInterbDAO - altaInterbOnlineContrCat $ Query de Insercion Interb" + query);
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()) {
				numSecuencia = Integer.parseInt(rs.getString(CatNominaConstantes._NUM_SEC));
				System.out.println("CatNominaInterbDAO - ggeneraQuerySecuencia $ Secuencia: " + numSecuencia);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			numSecuencia = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

	//	System.out.println("CatNominaInterbDAO - generaQuerySecuencia $ Numero de Secuencia: " + numSecuencia);
		return numSecuencia;
		}


	/******************************************************************
	 *** M?todo que genera el filtro para la b?squeda de empleados	***
	 ***  con base en la opcion elegida en el front. 				***
	 ******************************************************************/
	public String generaFiltroBusqueda(String numContrato, String radioValue, String valorBusqueda)
	{
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [generaFiltroBusqueda] - El numContrato es: " + numContrato,EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [generaFiltroBusqueda] - El filtro de la busqueda es: " + radioValue,EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - $ [generaFiltroBusqueda] - El valor de la busqueda es: " + valorBusqueda,EIGlobal.NivelLog.DEBUG);
		String filtroBusqueda = "";
	//	CatNominaConstantes cons = new CatNominaConstantes();

		// Generamos secuencia para el WHERE dependiendo del filtro seleccionado
		valorBusqueda = (valorBusqueda != null ? valorBusqueda.trim() : "");
		if(radioValue.equals("nombre"))
			filtroBusqueda = CatNominaInterbConstantes._CAT_NOM + "." + CatNominaInterbConstantes._CONTRATO + " = '" + numContrato + "' AND "
			+ CatNominaInterbConstantes._CAT_NOM + "." + CatNominaInterbConstantes._NOM + " like '%" + valorBusqueda + "%' ";
		else if(radioValue.equals("numCuenta"))
			filtroBusqueda = CatNominaInterbConstantes._CAT_NOM + "." + CatNominaInterbConstantes._CONTRATO + " = '" + numContrato + "' AND " + CatNominaInterbConstantes._CAT_NOM + "."
			+ CatNominaInterbConstantes._NUM_CTAE +  " = '" + valorBusqueda + "'";
		else if(radioValue.equals("todos"))
			filtroBusqueda = CatNominaInterbConstantes._CAT_NOM + "." + CatNominaInterbConstantes._CONTRATO + " = '" + numContrato + "'";

/*		filtroBusqueda += " AND to_Number(substr(" + CatNominaInterbConstantes._CAT_NOM + "." + CatNominaInterbConstantes._NUM_CTAE + ", 1, 3)) = to_Number(" +
			CatNominaInterbConstantes._COMU_INTERME + "." + CatNominaInterbConstantes._NUM_BANCO + ")"; */
		return filtroBusqueda;
	}
	//AND to_number(substr(num_cuenta_ext, 1, 3)) = B.NUM_CECOBAN

	/******************************************************************
	 *** verificar la existencia de las cuentas de empleados dentro	***
	 *** del cat?logo de n?mina para realizar modificaciones por 	***
	 *** archivo. 													***
	 ******************************************************************/
	public int existeRelacion(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - existeRelacion $ Inicio");
		System.out.println("CatNominaInterbDAO - existeRelacion $ El numContrato es: " + numContrato);
		System.out.println("CatNominaInterbDAO - existeRelacion $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		int existeReg = 0;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();


		query = "SELECT COUNT(*) AS " + CatNominaConstantes._TOTAL + " FROM " + CatNominaConstantes._DET_CAT +
				" WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato +
				"' AND " + CatNominaConstantes._CTA_ABO + " = '" + numCuenta +
				"' AND " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'";
		System.out.println("CatNominaInterbDAO - existeRelacion $ El query para existencia de relaci?n es: " + query);


		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				existeReg = rs.getInt("TOTAL");
			}
			System.out.println("CatNominaInterbDAO - existeRelacion $ El query para existencia de relaci?n es: " + existeReg);
		}
		catch (Exception e){
			e.printStackTrace();
			existeReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - existeRelacion $ Fin");
		return existeReg;
	}

	/**********************************************************************
	 *** Obtiene el tipo de cuenta del cual se busca realizar una 		***
	 *** modificaci?n de datos de empleados
	 **********************************************************************/
	public String obtenTipoCuenta(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - obtenTipoCuenta $ Inicio");
		System.out.println("CatNominaInterbDAO - obtenTipoCuenta $ El numContrato es: " + numContrato);
		System.out.println("CatNominaInterbDAO - obtenTipoCuenta $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		String tipoCuenta = "";
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();

		try {
			query = "SELECT " + CatNominaConstantes._TIPO_CTA_EVER + "  FROM " + CatNominaConstantes._DET_CAT +
					" WHERE " + CatNominaConstantes._CONTR + " = " + numContrato + " AND " + CatNominaConstantes._CTA_ABO + " = " + numCuenta;
			System.out.println("CatNominaInterbDAO - obtenTipoCuenta $ El query del tipo de Cuenta es: " + query);

			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()){
				tipoCuenta = rs.getString("TIPO_CUENTA");
			}
			System.out.println("CatNominaInterbDAO - obtenTipoCuenta $ El query para existencia de relaci?n es: " + tipoCuenta);
		}
		catch (Exception e){
			e.printStackTrace();
			tipoCuenta = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - obtenTipoCuenta $ Fin");
		return tipoCuenta;
	}

	/******************************************************************************
	 *** M?todo que verifica si el empleado con cuenta interbancaria tiene  	***
	 *** alguna cuenta interna asociado para poder o no realizar modificaciones	***
	 *** completas.  															***
	 ******************************************************************************/
	public int existeInternaEmplInterb(String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - existeInternaEmplInterb $ Inicio");
		System.out.println("CatNominaInterbDAO - existeInternaEmplInterb $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		int totalInternas = 0;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();

		String[] datosPerson = obtenDatosPerson(numCuenta);

		try
		{
			if(datosPerson[0].equals("ERROR0000"))
			{
				query = "SELECT COUNT(1) as CUENTA FROM " + CatNominaConstantes._CAT_NOM + ", " + CatNominaConstantes._DET_CAT +
					" WHERE "  + CatNominaConstantes._CAT_NOM + "." + 	CatNominaConstantes._CTA_ABO + " = " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO +
					" AND " + CatNominaConstantes._NOMBRE + " = '" + datosPerson[0] +
					"' AND " + CatNominaConstantes._APE_PAT + " = '" + datosPerson[1] + "' AND " + CatNominaConstantes._RFC + " = '" + datosPerson[2] +
					"' AND " + CatNominaConstantes._TIPO_CTA_EVER + " = '" + CatNominaConstantes._CTA_INTERNA + "' AND " + CatNominaConstantes._STAT_CUENTA_EVER + " = '" + CatNominaConstantes._ACT + "'";
					System.out.println("CatNominaInterbDAO - existeInternaEmplInterb $ El query que verifica si hay alguna cuenta interna " + query);

					//Crear();
					Crear(query);
					//rs = Consulta(query);
					rs = Consulta();

					while(rs.next()){
						totalInternas = rs.getInt("CUENTA");
					}
					System.out.println("CatNominaInterbDAO - existeInternaEmplInterb $ El query para existencia de relaci?n es: " + totalInternas);
			}
			else
				totalInternas = -100;
		}
		catch (Exception e){
			e.printStackTrace();
			totalInternas = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - existeInternaEmplInterb $ Fin");
		return totalInternas;
	}


	/*************************************************************************
	 *** Obtiene el nombre, apellido paterno y rfc del empleado con cuenta  ***
	 *** interbancaria para buscar una relaci?n con una cuenta interna		***
	 *************************************************************************/
	public String[] obtenDatosPerson(String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - obtenDatosPerson $ Inicio");
		System.out.println("CatNominaInterbDAO - obtenDatosPerson $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		String[] datosPerson = new String[3];
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();

		try {
			query = "  SELECT " + CatNominaConstantes._APE_PAT + ", " + CatNominaConstantes._NOMBRE + ", " + CatNominaConstantes._RFC +
					" FROM " + CatNominaConstantes._CAT_NOM + " WHERE " + CatNominaConstantes._CTA_ABO + " = '" + numCuenta + "'";
			System.out.println("CatNominaInterbDAO - obtenDatosPerson $ El query que trae nombre, apellido, rfc: " + query);

			//Crear();
			Crear(query);
//			rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				datosPerson[0] = rs.getString("NOMBRE");
				datosPerson[1] = rs.getString("APELL_PATERNO");
				datosPerson[2] = rs.getString("REG_FED_CAUS");
			}
			System.out.println("CatNominaInterbDAO - obtenDatosPerson $ El NOMBRE es: " + datosPerson[0]);
			System.out.println("CatNominaInterbDAO - obtenDatosPerson $ El APELLIDO es: " + datosPerson[1]);
			System.out.println("CatNominaInterbDAO - obtenDatosPerson $ El RFC es: " + datosPerson[2]);
		}
		catch (Exception e){
			e.printStackTrace();
			datosPerson[0] = "ERROR0000";
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - obtenDatosPerson $ Fin");
		return datosPerson;
	}


	//ALTA INTERBANCARIA POR ARCHIVO


	/******************************************************************
	 *** M?todo que realiza la actualizaci?n de los datos en la 	***
	 *** las tablas del nuevo cat?logo de n?mina. 	 	 			***
	 *****************************************************************/
	//public void altaInterbArchivo(String[] datosEmpleados, String numContrato, String cveUsr)
	public boolean altaInterbArchivo(String[] datosEmpleados, String numContrato, String cveUsr)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - altaInterbArchivo $ Inicio");
		System.out.println("CatNominaInterbDAO - altaInterbArchivo $ El numContrato es:" + numContrato);
		System.out.println("CatNominaInterbDAO - altaInterbArchivo $ La cveUsr es:" + cveUsr);
		System.out.println("CatNominaInterbDAO - altaInterbArchivo $ El numCuenta es:" + datosEmpleados[0]);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		int numSecuencia = 0;
		boolean resul = false;
		String[] datosModificar = null;
		//CatNominaLeeRS resulSet = new CatNominaLeeRS();
		int existeReg = 0;
		String existe = "";
		boolean retorno = false;

		numSecuencia = generaQuerySecuencia();

		//ConexionOracle = new ConexionOracle();



		try
		{
			//Crear();

			if (numSecuencia != -100)
			{

				String query = "SELECT " + CatNominaConstantes._CONTR + " FROM " + CatNominaConstantes._CONTR_CAT + " WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "'";
				System.out.println("*** CatalogoAlta - lecturaCam $ Contrato de query " + query);
				Crear(query);
				//rs = Consulta(query);

				rs = Consulta();



				/*rs.next();
				existe = rs.getString("CONTRATO");*/

				while(rs.next()){
					existe = rs.getString("CONTRATO");

				}

				System.out.println("CatNominaInterbDAO - existeRelacion $ El query para existencia de relaci?n es: " + existe);



				//if(existe != numContrato)
				//if(existe.equals(numContrato))



				//if(existe.equals(numContrato))
				if(!(existe.trim()).equals(numContrato))
				{
					System.out.println("*** CatalogoAlta - lecturaCam $ El registro No existe " );
					String sql2 = "INSERT INTO " + CatNominaConstantes._CONTR_CAT + "(" + CatNominaConstantes._CONTR + "," + CatNominaConstantes._CALLE_OF + "," + CatNominaConstantes._COL_OF + "," + CatNominaConstantes._DELEG_OF + "," + CatNominaConstantes._EDO_OF + "," + CatNominaConstantes._CIUDAD_OF + "," + CatNominaConstantes._CP_OF + "," + CatNominaConstantes._PAIS_OF + "," +
					CatNominaConstantes._CVE_DIREC + "," + CatNominaConstantes._PREF_OF + "," + CatNominaConstantes._NUMERO_OF + "," + CatNominaConstantes._EXT_OF + ")" +
					 " values ('" + numContrato + "', '" + datosEmpleados[18] + "', '" + datosEmpleados[19] + "', '" + datosEmpleados[20] + "', '" + datosEmpleados[21] + "', '" + datosEmpleados[22] + "', '" + datosEmpleados[23] + "', '"+
					 datosEmpleados[24] + "', " + datosEmpleados[25] + ", " + datosEmpleados[26] + ", " + datosEmpleados[27] + ", " + datosEmpleados[28] +")";

					System.out.println("*** CatalogoAlta - lecturaCam 2 <<<< $ " + sql2);


					Crear(sql2);
					//int inser2 = Inserta(sql2);
					int inser2 = Inserta();
					System.out.println("*** CatalogoAlta - lecturaCam Entra a insertar Contrato$ ");
					System.out.println("*** CatalogoAlta - lecturaCam $ " + sql2);

				}


				String sql1 = "INSERT INTO " + CatNominaConstantes._CAT_NOM + "(" + CatNominaConstantes._ID_CAT + "," + CatNominaConstantes._CTA_ABO + "," + CatNominaConstantes._APE_PAT + "," + CatNominaConstantes._APE_MAT + "," + CatNominaConstantes._NOMBRE + "," + CatNominaConstantes._RFC + "," + CatNominaConstantes._SEXO + "," + CatNominaConstantes._CALLE_NUM + "," +
				CatNominaConstantes._COLONIA + "," + CatNominaConstantes._DELEG_MUN + "," + CatNominaConstantes._CVE_EDO + "," + CatNominaConstantes._CIUDAD_POB + "," + CatNominaConstantes._CP + "," + CatNominaConstantes._CVE_PAIS + "," + CatNominaConstantes._PREF_PART + "," + CatNominaConstantes._NUM_PART + ", "+ CatNominaConstantes._STA_PROCE + ", " + CatNominaConstantes._FCH_MOD_STA_PROCE + ")" +
				 " values (" + numSecuencia + ", '" + datosEmpleados[0] + "', '" + datosEmpleados[4] + "', '" + datosEmpleados[5] + "', '" + datosEmpleados[6] + "', '" + datosEmpleados[7] + "', '" + datosEmpleados[8] + "', '"+
				 datosEmpleados[9] + "', '" + datosEmpleados[10] + "', '" + datosEmpleados[11] + "', '" + datosEmpleados[12] + "', '" + datosEmpleados[13] + "', '" + datosEmpleados[14]+ "', '" + datosEmpleados[15] + "', '" + datosEmpleados[16] + "', " + datosEmpleados[17] + ", " + "'A'" + ", " + "SYSDATE" +")";

				System.out.println("*** CatalogoAlta - lecturaCam 1<<<< $ " + sql1);

				Crear(sql1);
	//			int inser = Inserta(sql1);
				int inser = Inserta();
				System.out.println("*** CatalogoAlta - lecturaCam $ " + sql1);

				if (inser != 0){

					String sql3 = "INSERT INTO " + CatNominaConstantes._DET_CAT + "(" + CatNominaConstantes._CONTR_DET + ","+ CatNominaConstantes._CTA_ABO_DET + ","+ CatNominaConstantes._DET_CAT+ "." + CatNominaConstantes._NUM_EMPL + "," + CatNominaConstantes._DET_CAT+ "." + CatNominaConstantes._DEPTO_EMPL + "," + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._SUELDO + ", " + CatNominaConstantes._TIPO_CTA_EVER + ", " + CatNominaConstantes._STAT_CUENTA_EVER + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + ", " + CatNominaConstantes._FCH_MODIF_REG_EVER + ")" +
					 " values ('" + numContrato +"', '" + datosEmpleados[0] + "', '" + datosEmpleados[1] + "', '" + datosEmpleados[2] + "', " + datosEmpleados[3] + ", " + "'INTERB'" + ", "+"'P'" + ", '" + cveUsr + "', " + "SYSDATE" +")";

					System.out.println("*** CatalogoAlta - lecturaCam $ " + sql3);

					Crear(sql3);
					//int inser3 = Inserta(sql3);
					int inser3 = Inserta();
					System.out.println("*** CatalogoAlta - lecturaCam $ " + sql3);

				}else {
					System.out.println("*** CatalogoAlta - lecturaCam $ No se realizo al Alta ");
				}


				System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaInterbDAO) 1 = true");
				retorno=true;
			}
			else
				retorno = false;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaInterbDAO) 2 = false");
			retorno =false;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaInterbDAO) Correcto");
				System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaInterbDAO)" + retorno);

				}
		return retorno;

	}






	/******************************************************************
	 *** verificar la existencia de las cuentas de empleados dentro	***
	 *** del cat?logo de n?mina para realizar Alta por         	    ***
	 *** archivo Interbancaria.										***
	 ******************************************************************/
	public int existeRegistro(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - existeRegistro $ Inicio");
		System.out.println("CatNominaInterbDAO - existeRegistro $ El numContrato es: " + numContrato);
		System.out.println("CatNominaInterbDAO - existeRegistro $ El numCuenta es: " + numCuenta);

		ResultSet rs = null;
		int existeReg = 0;
	//	CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();


		query = "SELECT COUNT(*) AS " + CatNominaConstantes._TOTAL + " FROM " + CatNominaConstantes._DET_CAT +
				" WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato +
				"' AND " + CatNominaConstantes._CTA_ABO + " = '" + numCuenta + "'";
		System.out.println("CatNominaInterbDAO - existeRegistro $ El query para existencia de relaci?n es: " + query);


		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();
			while(rs.next()){
				existeReg = rs.getInt("TOTAL");
			}
			System.out.println("CatNominaInterbDAO - existeRegistro $ El query para existencia de relaci?n es: " + existeReg);
		}
		catch (Exception e){
			e.printStackTrace();
			existeReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - existeRegistro $ Fin");
		return existeReg;
	}












	// MODIFICACION ARCHIVO INTERBANCARIA







	/***************************************************************
	 *** M?todo que busca si existe alguna relaci?n    	 		****
	 *** con una cuenta Interna (Para cuentas Interbancarias)  	****
	 **************************************************************/
	public int existeRelacionCtaInterna(String[] arrDatosModifInterb, String numContrato, String numCuenta )
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - existeRelacionCtaInterna $ Inicio");


		//CatNominaConstantes cons = new CatNominaConstantes();
	//	CatNominaLeeRS resulSet = new CatNominaLeeRS();
		//ConexionOracle = new ConexionOracle();
		String[] datosModificar = null;
		ResultSet rs = null;
		int existeReg = 0;

		query = "SELECT COUNT(*) AS " + CatNominaConstantes._TOTAL + " FROM " + CatNominaConstantes._CAT_NOM + ", " + CatNominaConstantes._DET_CAT
			+ " WHERE " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._NOMBRE +" = '" + arrDatosModifInterb[4] + "' AND " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._APE_PAT + " = '" + arrDatosModifInterb[3] + "' AND " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._RFC + " = '" + arrDatosModifInterb[5] + "' AND "
			+ CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO_DET + " NOT IN '"+ numCuenta + "' AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._TIPO_CTA_EVER + " = " + "'PROPIA'" + " AND " + CatNominaConstantes._DET_CAT + "." + CatNominaConstantes._CTA_ABO_DET + " = " + CatNominaConstantes._CAT_NOM + "." + CatNominaConstantes._CTA_ABO;


					System.out.println("CatNominaInterbDAO - existeRelacionCtaInterna $ El query verifica no existencia de cuenta Interna: " + query);

		try {
			//Crear();
			Crear(query);
			//rs = Consulta(query);
			rs = Consulta();

			while(rs.next()){
				existeReg = rs.getInt("TOTAL");
			}

			System.out.println("CatNominaInterbDAO - existeRelacionCtaInterna $ El query verifica no existencia de cuenta Interna: " + existeReg);

		}
		catch (Exception e){
			e.printStackTrace();
			existeReg = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}

		System.out.println("CatNominaInterbDAO - existeRelacionCtaInterna $ FIN");
		return existeReg;
	}




	/******************************************************************
	 *** M?todo que realiza la actualizaci?n de los datos 		 	***
	 *** con cuenta Interbancaria en								***
	 *** las tablas del nuevo cat?logo de n?mina. 	 	 			***
	 *****************************************************************/
	//public void actualizaDatosArchInterb(String[] datosEmpleados, String numContrato, String cveUsr)
	public boolean actualizaDatosArchInterb(String[] datosEmpleados, String numContrato, String cveUsr)
	{
		System.out.println("");
		System.out.println("CatNominaInterbDAO - actualizaDatosArchInterb $ Inicio");
		System.out.println("CatNominaInterbDAO - actualizaDatosArchInterb $ El numContrato es:" + numContrato);
		System.out.println("CatNominaInterbDAO - actualizaDatosArchInterb $ La cveUsr:" + cveUsr);
		System.out.println("CatNominaInterbDAO - actualizaDatosArchInterb $ El numCuenta es:" + datosEmpleados[0]);

		//CatNominaConstantes cons = new CatNominaConstantes();
		ResultSet rs = null;
		boolean varReturn = true;

		//ConexionOracle = new ConexionOracle();

		try
		{
			//Crear();


			String sql1 = " UPDATE " + CatNominaConstantes._CONTR_CAT + " SET " + CatNominaConstantes._CALLE_OF + " = '" + datosEmpleados[14] + "', " + CatNominaConstantes._COL_OF +" = '" + datosEmpleados[15] + "', " + CatNominaConstantes._DELEG_OF + " = '" + datosEmpleados[16]+ "', " + CatNominaConstantes._EDO_OF +" = '" + datosEmpleados[17]+ "', " + CatNominaConstantes._CIUDAD_OF + " = '" + datosEmpleados[18] +"', " + CatNominaConstantes._CP_OF + " = '" + datosEmpleados[19] +"', " +
			CatNominaConstantes._PAIS_OF + " = '" + datosEmpleados[20] + "', " +	CatNominaConstantes._CVE_DIREC + " = '" + datosEmpleados[21] + "', " + CatNominaConstantes._PREF_OF + " = '" + datosEmpleados[22] + "', " + CatNominaConstantes._NUMERO_OF + " = '" + datosEmpleados[23] +"', " + CatNominaConstantes._EXT_OF + " = '" + datosEmpleados[24] +
			"' WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "'";

			Crear(sql1);
			//Inserta(sql1);
			Inserta();
			System.out.println("CatNominaInterbDAO - generaQueryActualiaEmpl $ Query de Modificaci?n = " + sql1);

			String sql2 = " UPDATE " + CatNominaConstantes._CAT_NOM + " SET " +  CatNominaConstantes._SEXO + " = '" + datosEmpleados[4] + "', " + CatNominaConstantes._CALLE_NUM + " = '" + datosEmpleados[5] + "', " + CatNominaConstantes._COLONIA + " = '" + datosEmpleados[6] + "', " + CatNominaConstantes._DELEG_MUN + " = '" + datosEmpleados[7] + "', " +
			CatNominaConstantes._CVE_EDO + " = '" + datosEmpleados[8] + "', " + CatNominaConstantes._CIUDAD_POB + " = '" + datosEmpleados[9] + "', " + CatNominaConstantes._CP + " = '" + datosEmpleados[10] + "', " + CatNominaConstantes._CVE_PAIS + " = '" + datosEmpleados[11] + "', " + CatNominaConstantes._PREF_PART + " = '" + datosEmpleados[12] + "', " + CatNominaConstantes._NUM_PART + " = '" + datosEmpleados[13] +
			"' WHERE " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[0] + "'";

			Crear(sql2);
			//Inserta(sql2);
			Inserta();
			System.out.println("CatNominaInterbDAO - generaQueryActualiaEmpl $ Query de Modificaci?n = " + sql2);



			String sql3 = " UPDATE " + CatNominaConstantes._DET_CAT + " SET " + CatNominaConstantes._NUM_EMPL + " = '" + datosEmpleados[1] + "', " +
					CatNominaConstantes._DEPTO_EMPL + " = '" + datosEmpleados[2] + "', " + CatNominaConstantes._SUELDO + " = " + datosEmpleados[3] + ", " + CatNominaConstantes._CVE_USR_MODIF_EVER + " = '"+ cveUsr + "', " + CatNominaConstantes._FCH_MODIF_REG_EVER + " = " + "SYSDATE" +
					" WHERE " + CatNominaConstantes._CONTR + " = '" + numContrato + "' AND " + CatNominaConstantes._CTA_ABO + " = '" + datosEmpleados[0] + "'";

			Crear(sql3);
			//Inserta(sql3);
			Inserta();
			System.out.println("CatNominaInterbDAO - generaQueryActualiaEmpl $ Query de Modificaci?n = " + sql3);


		}
		catch (Exception e)
		{
			e.printStackTrace();
			varReturn = false;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}
		return varReturn;
	}



/*
    private void Crear()throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion-----------", EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO);
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO4);
			statement = conn.createStatement();
			//statement = conn.preparedStatement();
    	}
*/
	//PreparedStatement
	private void Crear(String query)throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion-----------PreparedStatament", EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO);
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.INFO);
			statement = conn.prepareStatement (query);
			//statement = conn.preparedStatement();
    	}

	//	PreparedStatement
	/***************************************************************
	 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
	 *** Metodo para crear la conexion con ds gfi   			****
	 **************************************************************/
	private void Crear2(String query)throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion 2-----------PreparedStatament", EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.INFO);
    		conn2 = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.INFO);
			statement2 = conn2.prepareStatement (query);
			//statement = conn.preparedStatement();
    }


/*
	private ResultSet Consulta(String query)throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta -----------", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement.executeQuery(query);
		return rs;
		}*/


	//PreparedStatement
	private ResultSet Consulta()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement.executeQuery();
		return rs;
		}

	/***************************************************************
	 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
	 *** Metodo para crear statement							****
	 **************************************************************/
	private ResultSet Consulta2()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta 2 -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement2.executeQuery();
		return rs;
		}

/*	private int Inserta(String query)throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando UPDATE/INSERT -----------", EIGlobal.NivelLog.INFO);
		int resultado = 0;
		resultado = statement.executeUpdate(query);
		return resultado;
		}*/

	private int Inserta()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando UPDATE/INSERT -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		int resultado = 0;
		resultado = statement.executeUpdate();
		statement.close();
		statement = null;
		return resultado;
		}

	private int Inserta2()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando UPDATE/INSERT (2) -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		int resultado = 0;
		resultado = statement2.executeUpdate();
		statement2.close();
		statement2 = null;
		EIGlobal.mensajePorTrace("--------- Realizo UPDATE/INSERT (2), actualizo: -----------" + resultado, EIGlobal.NivelLog.INFO);
		return resultado;
		}
	public String buscarBancos(String arrBiDatosQuery [][])
	{
		EIGlobal.mensajePorTrace("CatNominaInterbDAO - buscaBanco	  $ Inicio" ,EIGlobal.NivelLog.DEBUG);

		ResultSet rs = null;
		String nomBanco ="";
		String numBanco ="";

		//CatNominaConstantes cons = new CatNominaConstantes();
		//ConexionOracle = new ConexionOracle();
		if(arrBiDatosQuery != null && arrBiDatosQuery[0][0].equals("ERROR0000"))
		{
			return "";
		}


		for (int i = 0 ; i< arrBiDatosQuery.length ; i++)
		{

			if(CatNominaConstantes._CTA_INTERNA.equals(arrBiDatosQuery[i][8])){
				arrBiDatosQuery[i][10] = CatNominaConstantes._BANCO_INTERNO;
			}else if (CatNominaConstantes._CTA_INTERB.equals(arrBiDatosQuery[i][8])){
				numBanco = obtenCLABE(arrBiDatosQuery[i][0]);
				query = "SELECT  " + CatNominaConstantes._NOM_BANCO+ " FROM " + CatNominaConstantes._COMU_INTERME +
						" WHERE " + CatNominaConstantes._NUM_BANCO + " = '" + numBanco + "'";

				System.out.println("CatNominaInterbDAO - generaQuerybuscarBanco $ Query de Modificaci?n = " + query);
				try {

						Crear(query);
						rs = Consulta();
					while(rs.next()){
					nomBanco = rs.getString(CatNominaConstantes._NOM_BANCO);
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
			finally{
				arrBiDatosQuery[i][10] = nomBanco;
					try{
						if(rs!=null){
							rs.close();
							rs=null;}
						if(statement!=null){
							statement.close();
							statement=null;}
						if(conn!=null){
							conn.close();
							conn=null;}
						}catch(Exception e1){
							EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
						}
				}
			}
		}
		/*try{

			if(conn2!=null){
				conn2.close();
				conn2=null;}
			}catch(Exception e1){
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
				}*/
		System.out.println("CatNominaInterbDAO - buscaBanco $ Fin");
		return "";
	}

	private String obtenCLABE(String numCuenta)
	{
		String clabe = "";
		int j = 0;
		if(numCuenta != null && numCuenta.length() >= 3)
		{
			clabe = numCuenta.substring(0,3);
			//quitamos los ceros
			for( j = 0; j < clabe.length(); j++)
			{
				if(clabe.charAt(j) != '0')
					break;
			}
			clabe =  clabe.substring(j);
		}
		System.out.println("Cuenta -> " + numCuenta + " CLABE -> " + clabe);
		return clabe ;
	}

	/******************************************************************
	 *** Metodo que realiza la actualizacion de los datos en la 	***
	 *** las tablas del nuevo catalogo de nomina.
	 *** VSWF - JGAL								 	 			***
	 *****************************************************************/
	public RespInterbBean altaCuentasInterb(String archivo, String numContrato, String cveUsr, String facultad)
	{
		Vector cuentasAlta = new Vector();
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - altaInterbArchivo $ $ El numContrato es:" + numContrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - altaInterbArchivo $ La cveUsr es:" + cveUsr, EIGlobal.NivelLog.INFO);
		long lote = 0;
		int enviados = 0;
		int autorizados = 0;
		int pendientes = 0;
		int rechazados = 0;
		//Genera archivo para informar detalle
		String nombreArchivoDetalle  = IEnlace.DOWNLOAD_PATH + cveUsr + ".daib"; //detalle alta interbancaria
		File pathArchivoDetalle = null;
		RandomAccessFile archivoDetalle = null;
		BufferedReader archImp = null;
		HashMap listaCecoban= new HashMap(); 
		HashMap listaInterme = obtenListaInterme(listaCecoban);
		//HashMap listaInterme = obtenListaInterme("INTERME");
		//HashMap listaCecoban = obtenListaInterme("CECOBAN");
		//CSA ***********************************************
		List<MAC_Interb> listaRegistros = new Vector();
		ArrayList<String> listaCuentas = new ArrayList<String>();
		BigDecimal []listaResultados = null;
		Integer id = null;
		String msj1,msj2 = null;
		String queryInsert = null;
		Statement statementB = null;
		int ID_ALTAS = 1;
		int ID_CUENTA = 2;
		int ID_SOLICITUD = 3;
		String CADENA_ALTAS = "ALTAS";
		String CADENA_CUENTAS = "CUENTA";
		String CADENA_SOLICITUD = "SOLICITUD";
		String tipo = null;
		//********************************
		//ARSHashMap listaCecoban = obtenListaInterme("INTERME");
		try
		{
			pathArchivoDetalle = new File(nombreArchivoDetalle.toString());
			archivoDetalle = new RandomAccessFile(pathArchivoDetalle, "rw");
			archivoDetalle.setLength(0);
			archImp = new BufferedReader( new FileReader(archivo) );
		 	MAC_Interb registro = null; //=new MAC_Interb();
			EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - grabaCuentas -Archivo abierto", EIGlobal.NivelLog.INFO);
			lote = generaLote();
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			
			
			String line = archImp.readLine();

    		EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Lote: [" + lote + "]", EIGlobal.NivelLog.INFO);
    		int inser = 0;
    		while (line != null) {
    			registro=new MAC_Interb();
    			listaRegistros.add(registro);
    			
				registro.parse(line);
				EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - registro.cveBanco: [" + registro.cveBanco + "]", EIGlobal.NivelLog.INFO);
				// Valida la clave de banco; si es alta manual viene numerica, si es por lotes viene como String (la clave Interme)
				try {
					registro.setCveInterme((String)listaCecoban.get(new Integer(registro.cveBanco.trim())));
				} catch (NumberFormatException e) {
					registro.setCveInterme(registro.cveBanco.trim());
					Integer cve = (Integer) listaInterme.get(registro.cveInterme.trim());
					if(cve == null) {
						registro.setClaveBanco("");
					}
					else {
						registro.setClaveBanco(cve.toString());
					}
				}
				enviados ++;
				EIGlobal.mensajePorTrace("CatNominaInterbDAO-> altaInterbArchivo() : Cuenta guardada: "+registro.cuenta, EIGlobal.NivelLog.INFO);
				listaCuentas.add(registro.cuenta);
				
				line = archImp.readLine();
			}
    		
    	
    		if(listaCuentas.size() >0){
    			EIGlobal.mensajePorTrace("Existen cuentas por procesar : " + listaCuentas.size(), EIGlobal.NivelLog.INFO);
    			listaResultados = ejecutarStore(numContrato, listaCuentas);
    		
    			if((listaResultados!= null) &&(listaResultados.length >0)){
    				
    				EIGlobal.mensajePorTrace("CatNominaInterbDAO-> altaInterbArchivo() :se obtuvieron datos, se incia el proceso de insercion",EIGlobal.NivelLog.INFO);
    				//Se crea el statement que almacenara todos las inserciones
    				statementB = conn.createStatement();
    				//conn.setAutoCommit(false);
    				for(int i=0;i<listaResultados.length;i++){
    					
    					id = listaResultados[i].intValue();
    					EIGlobal.mensajePorTrace("CatNominaInterbDAO-> altaInterbArchivo() : El id encontrado es :"+id, EIGlobal.NivelLog.INFO);
    					
    					if(id!= null){
    						registro =  (MAC_Interb) listaRegistros.get(i);
    						
    						if(id == ID_ALTAS){ // Este es para la tercer opcion
    							
    							EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Se realiza la Alta", EIGlobal.NivelLog.INFO);
    							queryInsert = generaQuery(registro, numContrato, cveUsr, facultad, lote);
    							
    							//statementB.addBatch(queryInsert);
    							EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - El Query: [" + queryInsert + "]", EIGlobal.NivelLog.INFO);
    							if (facultad.equals("AUTO")) {
    								autorizados ++;
    								archivoDetalle.writeBytes("|" + "AUTORIZADA" + "|" + registro.cuenta + "|" + "LA CUENTA SE ENCUENTRA PENDIENTE DE ACTIVAR" + "|" + "\r\n");
    							} else {
    								pendientes ++;
    								archivoDetalle.writeBytes("|" + "PENDIENTE" + "|" + registro.cuenta + "|" + "LA CUENTA SE ENCUENTRA PENDIENTE DE AUTORIZAR" + "|" + "\r\n");
    							}
    							EmailContainer ecBN = new EmailContainer();
    							ecBN.setNumCuenta(registro.cuenta);
    							ecBN.setTitular(registro.nombreTitular);
    							cuentasAlta.add(ecBN);
    							
    							
    						}else{ // Este es para la primer y segunda opcion CUENTA y SOLICITUD
    							if(id == ID_CUENTA){
    								msj1 = "Ya existe la cuenta";
    								msj2 = "LA CUENTA FUE RECHAZADA POR CUENTA EXISTENTE";
    								tipo = CADENA_CUENTAS;
    							}else{
    								msj1 = "Hay solicitudes pendientes";
    								msj2 = "LA CUENTA FUE RECHAZADA POR SOLICITUDES PENDIENTES";
    								tipo = CADENA_SOLICITUD;
    							}
    							EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - "+msj1, EIGlobal.NivelLog.ERROR);
    							queryInsert = generaQueryRechazo(registro, numContrato, cveUsr, facultad, "A", tipo);
    							EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - El Query de Rechazo: [" + queryInsert + "]", EIGlobal.NivelLog.INFO);
    							rechazados ++;
    							archivoDetalle.writeBytes("|" + "RECHAZADA" + "|" + registro.cuenta + "|" + msj2 + "|" + "\r\n");
    							EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Rechazo por cuenta ", EIGlobal.NivelLog.INFO);
    						}
    						
    						statementB.addBatch(queryInsert);
    					}
    				}
    			
    				EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Inicia la Insercion de los registros", EIGlobal.NivelLog.INFO);
    				int []resultadosTransaccion = statementB.executeBatch();
    				EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Termina la Insercion de los registros", EIGlobal.NivelLog.INFO);
    				EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Validando insercion de datos", EIGlobal.NivelLog.INFO);
    				for(int i = 0;i<resultadosTransaccion.length;i++){
    					if(resultadosTransaccion[i] != 1){
    						EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - resultado "+ (i+1) + " : "+resultadosTransaccion[i], EIGlobal.NivelLog.INFO);
    						EIGlobal.mensajePorTrace( ">>>>Error de la insersion de la cuenta : "+listaCuentas.get(i), EIGlobal.NivelLog.INFO);
    					
    					}
    				}
    				
    				conn.commit();
    				EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - No hay excepci�n", EIGlobal.NivelLog.INFO);
    			}else{
    				EIGlobal.mensajePorTrace("El store no devolvio ninguna lista de respuesta", EIGlobal.NivelLog.INFO);
    			}
    		}else{
    			EIGlobal.mensajePorTrace("No se registro ninguna cuenta para ejecutar el store, por favor validar el archivo", EIGlobal.NivelLog.INFO);
    		}
	
		}
		catch (Exception e)
		{
			EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Excepci�n: "+ e.getMessage(), EIGlobal.NivelLog.INFO);
			e.printStackTrace();
			lote = 0;
			if(conn!=null){
				try {
					conn.rollback();
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> altaInterbArchivo() - Excepci�n: "+ e.getMessage(), EIGlobal.NivelLog.INFO);
					e1.printStackTrace();
				}
			}
		}
		finally{
			try{
				if(statementB!=null){
					statementB.close();
					statementB=null;}
				if(conn!=null){
					conn.close();
					conn=null;}
				archImp.close();
				archivoDetalle.close();
			}catch(Exception e1){
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1.getMessage(),EIGlobal.NivelLog.INFO);
			}
			listaInterme = null;
			listaCecoban = null;
			EIGlobal.mensajePorTrace("CatNominaInterbDAO-> altaInterbArchivo() - Regresa: " + lote, EIGlobal.NivelLog.INFO);
		}
		RespInterbBean resp = new RespInterbBean(enviados, autorizados, pendientes, rechazados, lote, nombreArchivoDetalle, cuentasAlta);
		return resp;
	}

	/**
	 * Se encarga de ejecutar el store ALTAS_CUENTAS_INTERB_SP
	 * 
	 * @param numContrato Numero de contrato de la cuenta a consultar.
	 * @param listaCuentas Lista de las cuentas a consultar.
	 * @return Lista del estado de las cuentas
	 * @throws Exception
	 */
	private BigDecimal[] ejecutarStore(String numContrato,
			ArrayList<String> listaCuentas) throws Exception {
		
		String sql = "BEGIN PKG_ALTA_CUENTAS_INTERB.ALTAS_CUENTAS_INTERB_SP(?,?,?); END; ";
		Connection conexionNativa = null;
		OracleCallableStatement call = null;
		BigDecimal []resultados = null;
		int tamanioArre = 0;
		String []cuentas = null;
		
		EIGlobal.mensajePorTrace("ejecutarStore()--- Contrato  -->"+numContrato,EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ejecutarStore()--- Numero de cuentas -->"+listaCuentas,EIGlobal.NivelLog.INFO);

			
		cuentas =  obtenerArregloCuentas(listaCuentas);
		
		
		if(cuentas != null){
			try{
				
				tamanioArre = cuentas.length;
				EIGlobal.mensajePorTrace("CatNominaInterbDAO-> ejecutarStore():Numero de cuentas a enviar: " + tamanioArre,EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("CatNominaInterbDAO-> ejecutarStore(): Preparando llamado al store : "+sql,EIGlobal.NivelLog.INFO);
				
				conexionNativa = (Connection) WSJdbcUtil.getNativeConnection((WSJdbcConnection) conn);

				EIGlobal.mensajePorTrace("hizo la conexion y creo la nativa exitosamente "+conexionNativa, EIGlobal.NivelLog.INFO);
				call= (OracleCallableStatement) conexionNativa.prepareCall(sql);
				EIGlobal.mensajePorTrace("Se creo el objeto Callable correctamente "+call.getClass().getName(), EIGlobal.NivelLog.INFO);
				//Agrega el numero de contrato
				call.setString(1, numContrato);
				//Agrega el objeto T_Array creado
				//los parametros de este metodo son
				//numero parametro, objetos, tama�o maximo, tama�o actual, tipoDato, tama�o tipo dato
				call.setPlsqlIndexTable(2, cuentas , tamanioArre, tamanioArre, OracleTypes.VARCHAR, 50);

				//Resgistra el parametro OUT, es decir el que va a regresar el store
				// los parametros de este metodo son 
				//indice, tama�oMaximo, tipo, numeroElementos
				call.registerIndexTableOutParameter(3,tamanioArre, OracleTypes.NUMBER, tamanioArre);
				
				//ejecuta el store
				call.execute();
				
				EIGlobal.mensajePorTrace("CatNominaInterbDAO-> ejecutarStore(): Se ejecuto el store correctamente",EIGlobal.NivelLog.INFO);
				
				//Obtiene los resultados
				resultados = (BigDecimal[]) call.getPlsqlIndexTable(3);
				if(resultados!=null){
					for(int i = 0;i<resultados.length;i++){
						EIGlobal.mensajePorTrace("CatNominaInterbDAO-> ejecutarStore(): valor de retorno "+(i+1)+" --> "+resultados[i],EIGlobal.NivelLog.INFO);
					}
				}
				
				EIGlobal.mensajePorTrace("CatNominaInterbDAO-> ejecutarStore() : Se obtuvo los resultados del store, se devolvio un array de tama�o: "+(resultados!=null?resultados.length:"ERROR-No Regreso ningun array"),EIGlobal.NivelLog.INFO);
				
			}catch(Exception e){
				EIGlobal.mensajePorTrace("CatNominaInterbDAO-> ejecutarStore()  Ocurrio una excepcion en la ejecucion del proceso : "+e.getMessage(),EIGlobal.NivelLog.INFO);
				e.printStackTrace();
				throw e;
			}finally{
				if(call != null){
					call.close();
				}
			}
		}
		return resultados;
	}


	private String[] obtenerArregloCuentas(ArrayList<String> listaCuentas) {
		
		String[] cuentas = null;
		int tama = 0;
		EIGlobal.mensajePorTrace("obtenerArregloCuentas() --> Inicia el paso de la informacion de la lista al Array ", EIGlobal.NivelLog.INFO);
		
		if((listaCuentas!=null)&&(listaCuentas.size()>0)){
			tama = listaCuentas.size();
			cuentas = new String[tama];
			
			for(int i=0;i<tama;i++){
				cuentas[i] = listaCuentas.get(i);
			}	
			EIGlobal.mensajePorTrace("obtenerArregloCuentas() --> Numero de cuentas procesadas: " + cuentas.length, EIGlobal.NivelLog.INFO);
		}else{
			EIGlobal.mensajePorTrace("obtenerArregloCuentas() --> La lista no existe o no tiene elementos", EIGlobal.NivelLog.INFO);
		}
		return cuentas;
	}


	/* VSWF - JGAL
	 * 13 Julio de 2009
	 * Metodo para realizar la cancelacion de cuentas de nomina interbancaria
	 */
	public RespInterbBean bajaCuentasInterb(String archivo, String numContrato, String cveUsr, String facultad)
	{
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - bajaInterbArchivo $ Inicio", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - bajaInterbArchivo $ El numContrato es:" + numContrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - bajaInterbArchivo $ La cveUsr es:" + cveUsr, EIGlobal.NivelLog.INFO);
		int inser = 0;
		int enviados = 0;
		int autorizados = 0;
		int pendientes = 0;
		int rechazados = 0;
		String nombreArchivoDetalle  = IEnlace.DOWNLOAD_PATH + cveUsr + ".dbib"; //detalle baja interbancaria
		File pathArchivoDetalle = null;
		RandomAccessFile archivoDetalle = null;
		BufferedReader archImp = null;
		HashMap listaCecoban= new HashMap();
		HashMap listaInterme = obtenListaInterme(listaCecoban); 
		//HashMap listaInterme = obtenListaInterme("INTERME");
		//HashMap listaCecoban = obtenListaInterme("CECOBAN");
		try
		{
			pathArchivoDetalle = new File(nombreArchivoDetalle.toString());
			archivoDetalle = new RandomAccessFile(pathArchivoDetalle, "rw");
			archivoDetalle.setLength(0);
			archImp = new BufferedReader( new FileReader(archivo) );
		 	MAC_Interb registro=new MAC_Interb();
			EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaCuentasInterb() - grabaCuentas -Archivo abierto", EIGlobal.NivelLog.INFO);

			String line = archImp.readLine();
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			while (line != null) {
				registro.parse(line);
				EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - registro.cveBanco: [" + registro.cveBanco + "]", EIGlobal.NivelLog.INFO);
				try {
					registro.setCveInterme((String)listaCecoban.get(new Integer(registro.cveBanco.trim())));
				} catch (NumberFormatException e) {
					registro.setCveInterme(registro.cveBanco.trim());
					registro.setClaveBanco(listaInterme.get(registro.cveInterme.trim()).toString());
				}
				enviados ++;
				if (!existeCuenta(numContrato, registro.cuenta)) {
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - No existe la cuenta", EIGlobal.NivelLog.ERROR);
					StringBuffer query = new StringBuffer();
					query.append(generaQueryRechazo(registro, numContrato, cveUsr, facultad, "B", "CUENTA"));
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - El Query de Rechazo: [" + query + "]", EIGlobal.NivelLog.INFO);
					rechazados ++;
					archivoDetalle.writeBytes("|" + "RECHAZADA" + "|" + registro.cuenta + "|" + "LA CUENTA FUE RECHAZADA POR CUENTA INEXISTENTE" + "|" + "\r\n");
					statement = conn.prepareStatement (query.toString());
					inser = Inserta();
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - Rechazo por cuenta - No hay excepci�n", EIGlobal.NivelLog.INFO);
				} else if (existenSolicitudes(numContrato, registro.cuenta/*, "B"*/)) {
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - Hay solicitudes pendientes", EIGlobal.NivelLog.ERROR);
					StringBuffer query = new StringBuffer();
					query.append(generaQueryRechazo(registro, numContrato, cveUsr, facultad, "B", "SOLICITUD"));
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - El Query de Rechazo: [" + query + "]", EIGlobal.NivelLog.INFO);
					rechazados ++;
					archivoDetalle.writeBytes("|" + "RECHAZADA" + "|" + registro.cuenta + "|" + "LA CUENTA FUE RECHAZADA POR SOLICITUDES PENDIENTES" + "|" + "\r\n");
					statement = conn.prepareStatement (query.toString());
					inser = Inserta();
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - Rechazo por cuenta - No hay excepci�n", EIGlobal.NivelLog.INFO);
				} else {
					if (facultad.equals("AUTO")) { // Borra la cuenta de la tabla principal
						realizaBajaCuenta(numContrato, registro.cuenta);
						autorizados ++;
						archivoDetalle.writeBytes("|" + "AUTORIZADA" + "|" + registro.cuenta + "|" + "LA CUENTA SE ENCUENTRA PENDIENTE DE ACTIVAR" + "|" + "\r\n");
					} else {
						pendientes ++;
						archivoDetalle.writeBytes("|" + "PENDIENTE" + "|" + registro.cuenta + "|" + "LA CUENTA SE ENCUENTRA PENDIENTE DE AUTORIZAR" + "|" + "\r\n");
					}
					// Da de alta la solicitud de baja
					StringBuffer query = new StringBuffer();
					query.append(generaQueryBaja(registro, numContrato, cveUsr, facultad));
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - El Query: [" + query + "]", EIGlobal.NivelLog.INFO);
					statement = conn.prepareStatement (query.toString());
					try {
					inser = Inserta();
					} catch (Exception e) {
						EIGlobal.mensajePorTrace("CatNominaInterbDAO-> bajaInterbArchivo() - Excepci�n: [" + e + "]", EIGlobal.NivelLog.ERROR);
					}
				}
				line = archImp.readLine();
			}
		}
		catch (Exception e)
		{
			EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - Excepcion: " + e, EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
			inser = 0;
		}
		finally{
			try{
				if(statement!=null){
					statement.close();
					statement=null;}
				if(conn!=null){
					conn.close();
					conn=null;}
				archImp.close();
				archivoDetalle.close();
			}catch(Exception e1){
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.INFO);
			}
			listaInterme = null;
			listaCecoban = null;
			EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivo() - regresa: " + inser, EIGlobal.NivelLog.INFO);
		}
		RespInterbBean resp = new RespInterbBean(enviados, autorizados, pendientes, rechazados, inser, nombreArchivoDetalle);
		return resp;
	}

	/* VSWF - JGAL
	 * 21 Julio de 2009
	 * Metodo para realizar la baja de cuentas de nomina interbancaria On Line
	 */

	public RespInterbBean bajaCuentasInterbOL(String numContrato, String cuentas, String cveUsr, String facultad)
	{
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO -> bajaCuentasInterbOL $ Inicio", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO -> bajaCuentasInterbOL $ El numContrato es:" + numContrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO -> bajaCuentasInterbOL $ La cveUsr es:" + cveUsr, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO -> bajaCuentasInterbOL $ Las cuentas son: " + cuentas, EIGlobal.NivelLog.INFO);
		int inser = 0;
		int enviados = 0;
		int autorizados = 0;
		int pendientes = 0;
		int rechazados = 0;
		String nombreArchivoDetalle  = IEnlace.DOWNLOAD_PATH + cveUsr + ".dbol"; //detalle baja online
		File pathArchivoDetalle = null;
		RandomAccessFile archivoDetalle = null;
		BufferedReader archImp = null;
		try
		{
			pathArchivoDetalle = new File(nombreArchivoDetalle.toString());
			archivoDetalle = new RandomAccessFile(pathArchivoDetalle, "rw");
			archivoDetalle.setLength(0);
		 	MAC_Interb registro=new MAC_Interb();
			StringTokenizer tokens = new StringTokenizer(cuentas, "|");
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

    		EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() ", EIGlobal.NivelLog.INFO);
			while (tokens.hasMoreTokens()) {
				String cuenta = tokens.nextToken().trim();
	    		EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() - cuenta :[" + cuenta+ "]", EIGlobal.NivelLog.INFO);
				registro = obtenCuentaMAC(numContrato, cuenta);
				EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() - registro.cveBanco: [" + registro.cveBanco + "]", EIGlobal.NivelLog.INFO);
				if (!existeCuenta(numContrato, registro.cuenta)) {
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() - No existe la cuenta", EIGlobal.NivelLog.ERROR);
					StringBuffer query = new StringBuffer();
					query.append(generaQueryRechazo(registro, numContrato, cveUsr, facultad, "B", "CUENTA"));
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivoOL() - El Query de Rechazo: [" + query + "]", EIGlobal.NivelLog.INFO);
					rechazados ++;
					archivoDetalle.writeBytes("|" + "RECHAZADA" + "|" + registro.cuenta + "|" + "LA CUENTA FUE RECHAZADA POR CUENTA INEXISTENTE" + "|" + "\r\n");
					statement = conn.prepareStatement (query.toString());
					inser = Inserta();
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivoOL() - Rechazo por cuenta - No hay excepci�n", EIGlobal.NivelLog.INFO);
				} else if (existenSolicitudes(numContrato, registro.cuenta/*, "B"*/)) {
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() - Hay solicitudes pendientes", EIGlobal.NivelLog.ERROR);
					StringBuffer query = new StringBuffer();
					query.append(generaQueryRechazo(registro, numContrato, cveUsr, facultad, "B", "SOLICITUD"));
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivoOL() - El Query de Rechazo: [" + query + "]", EIGlobal.NivelLog.INFO);
					rechazados ++;
					archivoDetalle.writeBytes("|" + "RECHAZADA" + "|" + registro.cuenta + "|" + "LA CUENTA FUE RECHAZADA POR SOLICITUDES PENDIENTES" + "|" + "\r\n");
					statement = conn.prepareStatement (query.toString());
					inser = Inserta();
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO-> bajaInterbArchivoOL() - Rechazo por cuenta - No hay excepci�n", EIGlobal.NivelLog.INFO);
				} else {
					if (facultad.equals("AUTO")) { // Borra la cuenta de la tabla principal
						realizaBajaCuenta(numContrato, registro.cuenta);
						autorizados ++;
						archivoDetalle.writeBytes("|" + "AUTORIZADA" + "|" + registro.cuenta + "|" + "LA CUENTA SE AUTORIZO Y SE DIO DE BAJA EN EL SISTEMA" + "|" + "\r\n");
					} else {
						pendientes ++;
						archivoDetalle.writeBytes("|" + "PENDIENTE" + "|" + registro.cuenta + "|" + "LA CUENTA SE ENCUENTRA PENDIENTE DE AUTORIZAR" + "|" + "\r\n");
					}
					// Da de alta la solicitud de baja
					StringBuffer query = new StringBuffer();
					query.append(generaQueryBaja(registro, numContrato, cveUsr, facultad));
					EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() - El Query: [" + query + "]", EIGlobal.NivelLog.INFO);
					statement = conn.prepareStatement (query.toString());
					try {
						inser = Inserta();
					} catch (Exception e) {
						EIGlobal.mensajePorTrace("CatNominaInterbDAO -> bajaCuentasInterbOL() - Excepci�n: [" + e + "]", EIGlobal.NivelLog.ERROR);
					}
				}
			}
		}
		catch (Exception e)
		{
			EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() - Excepcion: " + e, EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
			inser = 0;
		}
		finally{
			try{
				if(statement!=null){
					statement.close();
					statement=null;}
				if(conn!=null){
					conn.close();
					conn=null;}
				archivoDetalle.close();
			}catch(Exception e1){
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO -> bajaCuentasInterbOL - Error al cerrar conexi?n-> "+e1,EIGlobal.NivelLog.INFO);
			}
			EIGlobal.mensajePorTrace( "CatNominaInterbDAO -> bajaCuentasInterbOL() - regresa: " + inser, EIGlobal.NivelLog.INFO);
		}
		RespInterbBean resp = new RespInterbBean(enviados, autorizados, pendientes, rechazados, inser, nombreArchivoDetalle);
		return resp;
	}


	private String generaQuery(MAC_Interb registro, String contrato, String usuario, String facultad, long lote) {
		String estatus = "I";
		String observ = "PENDIENTE POR AUTORIZAR";
		String tipoSol = "A";
		if (facultad.equals("AUTO")) {
			estatus = "P";
			observ = "PENDIENTE POR ACTIVAR";
		}
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO " + CatNominaInterbConstantes._CAT_EXT + " (");
		sql.append(CatNominaInterbConstantes._EXT_CNT + "," + CatNominaInterbConstantes._EXT_CTA + ",");
		sql.append(CatNominaInterbConstantes._EXT_STS + "," + CatNominaInterbConstantes._EXT_TIP + ",");
		sql.append(CatNominaInterbConstantes._EXT_SUC + "," + CatNominaInterbConstantes._EXT_INT + ",");
		sql.append(CatNominaInterbConstantes._EXT_PLZ + "," + CatNominaInterbConstantes._EXT_FOLE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FOLA + "," + CatNominaInterbConstantes._EXT_PRSE + ",");
		sql.append(CatNominaInterbConstantes._EXT_PRSA + "," + CatNominaInterbConstantes._EXT_FCHE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FCHA + "," + CatNominaInterbConstantes._EXT_TIT + ",");
		sql.append(CatNominaInterbConstantes._EXT_OBS + "," + CatNominaInterbConstantes._EXT_LOT + ") ");
		sql.append(" VALUES (");
		sql.append("'" + contrato + "', '" + registro.cuenta.trim() + "', ");
		sql.append("'" + estatus + "', '" + tipoSol + "', ");
		sql.append("null, '" + registro.cveInterme.trim() + "', ");
		sql.append("'" + registro.cvePlaza.trim() + "', " + CatNominaInterbConstantes._SEQ_EXT + ".NEXTVAL, ");
		if (facultad.equals("AUTO")) // Folio Autorizacion
			sql.append(CatNominaInterbConstantes._SEQ_EXT + ".NEXTVAL, ");
		else
			sql.append("null, ");
		sql.append("'" + usuario + "', ");
		if (facultad.equals("AUTO")) //Usuario Autoriza
			sql.append("'" + usuario + "', ");
		else
			sql.append("null, ");
		sql.append("SYSDATE, ");
		if (facultad.equals("AUTO")) //Fecha Autoriza
			sql.append("SYSDATE, ");
		else
			sql.append("null, ");
		sql.append("'" + registro.nombreTitular.trim() + "', '" + observ + "', ");

		sql.append("" + lote + ") ");
		return sql.toString();
	}

	private String generaQueryRechazo(MAC_Interb registro, String contrato, String usuario, String facultad, String tipoSol, String motivo) {
		String estatus = "R";
		String observ = "RECHAZADO POR CUENTA ";
		if (motivo.equals("CUENTA")) { // El motivo es que la cuenta existe/no existe en la tabla principal eweb_cat_nom_externa
			if (tipoSol.equals("A")) {
				observ = observ + "YA EXISTENTE";
			} else {
				observ = observ + "INEXISTENTE";
			}
		} else { // El motivo es que la cuenta tiene solicitudes pendientes o por autorizar en la tabla principal eweb_cat_nom_aut_ext
			observ = "RECHAZADO POR SOLICITUDES PENDIENTES";
		}
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO " + CatNominaInterbConstantes._CAT_EXT + " (");
		sql.append(CatNominaInterbConstantes._EXT_CNT + "," + CatNominaInterbConstantes._EXT_CTA + ",");
		sql.append(CatNominaInterbConstantes._EXT_STS + "," + CatNominaInterbConstantes._EXT_TIP + ",");
		sql.append(CatNominaInterbConstantes._EXT_SUC + "," + CatNominaInterbConstantes._EXT_INT + ",");
		sql.append(CatNominaInterbConstantes._EXT_PLZ + "," + CatNominaInterbConstantes._EXT_FOLE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FOLA + "," + CatNominaInterbConstantes._EXT_PRSE + ",");
		sql.append(CatNominaInterbConstantes._EXT_PRSA + "," + CatNominaInterbConstantes._EXT_FCHE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FCHA + "," + CatNominaInterbConstantes._EXT_TIT + ",");
		sql.append(CatNominaInterbConstantes._EXT_OBS + "," + CatNominaInterbConstantes._EXT_LOT + ") ");
		sql.append(" VALUES (");
		sql.append("'" + contrato + "', '" + registro.cuenta.trim() + "', ");
		sql.append("'" + estatus + "', '" + tipoSol + "', ");
		sql.append("null, '" + registro.cveInterme.trim() + "', ");
		sql.append("'" + registro.cvePlaza.trim() + "', " + CatNominaInterbConstantes._SEQ_EXT + ".NEXTVAL, ");
		sql.append("null, ");
		sql.append("'" + usuario + "', ");
		if (facultad.equals("AUTO")) //Usuario Autoriza
			sql.append("'" + usuario + "', ");
		else
			sql.append("null, ");
		sql.append("SYSDATE, ");
		if (facultad.equals("AUTO")) //Fecha Autoriza
			sql.append("SYSDATE, ");
		else
			sql.append("null, ");
		sql.append("'" + registro.nombreTitular.trim() + "', '" + observ + "', ");

		sql.append("" + null + ") ");
		return sql.toString();
	}

	private String generaQueryBaja(MAC_Interb registro, String contrato, String usuario, String facultad) {
		String estatus = "I";
		String observ = "PENDIENTE POR AUTORIZAR";
		String tipoSol = "B";
		if (facultad.equals("AUTO")) {
			estatus = "A";
			observ = "CUENTA DADA DE BAJA";
		}
		StringBuffer sql = new StringBuffer();
		EIGlobal.mensajePorTrace("facultad "+facultad+" cuenta = "+registro.cuenta+" cveInterme ="+registro.cveInterme+" registro.cvePlaza ="+registro.cvePlaza+ " registro.nombreTitular = "+registro.nombreTitular, EIGlobal.NivelLog.INFO);
		sql.append("INSERT INTO " + CatNominaInterbConstantes._CAT_EXT + " (");
		sql.append(CatNominaInterbConstantes._EXT_CNT + "," + CatNominaInterbConstantes._EXT_CTA + ",");
		sql.append(CatNominaInterbConstantes._EXT_STS + "," + CatNominaInterbConstantes._EXT_TIP + ",");
		sql.append(CatNominaInterbConstantes._EXT_SUC + "," + CatNominaInterbConstantes._EXT_INT + ",");
		sql.append(CatNominaInterbConstantes._EXT_PLZ + "," + CatNominaInterbConstantes._EXT_FOLE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FOLA + "," + CatNominaInterbConstantes._EXT_PRSE + ",");
		sql.append(CatNominaInterbConstantes._EXT_PRSA + "," + CatNominaInterbConstantes._EXT_FCHE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FCHA + "," + CatNominaInterbConstantes._EXT_TIT + ",");
		sql.append(CatNominaInterbConstantes._EXT_OBS + "," + CatNominaInterbConstantes._EXT_LOT + ") ");
		sql.append(" VALUES (");
		sql.append("'" + contrato + "', '" + registro.cuenta.trim() + "', ");
		sql.append("'" + estatus + "', '" + tipoSol + "', ");
		sql.append("null, '" + registro.cveInterme.trim() + "', ");
		sql.append("'" + registro.cvePlaza.trim() + "', " + CatNominaInterbConstantes._SEQ_EXT + ".NEXTVAL, ");
		if (facultad.equals("AUTO")) // Folio Autorizacion
			sql.append(CatNominaInterbConstantes._SEQ_EXT + ".NEXTVAL, ");
		else
			sql.append("null, ");
		sql.append("'" + usuario + "', ");
		if (facultad.equals("AUTO")) //Usuario Autoriza
			sql.append("'" + usuario + "', ");
		else
			sql.append("null, ");
		sql.append("SYSDATE, ");
		if (facultad.equals("AUTO")) //Fecha Autoriza
			sql.append("SYSDATE, ");
		else
			sql.append("null, ");
		sql.append("'" + registro.nombreTitular.trim() + "', '" + observ + "', ");
		sql.append("null)");
		return sql.toString();
	}



/*	private String generaQueryBajaLote(MAC_Interb registro, String contrato, String usuario, String facultad, long lote) {
		String estatus = "I";
		String observ = "PENDIENTE POR AUTORIZAR";
		String tipoSol = "B";
		if (facultad.equals("AUTO")) {
			estatus = "P";
			observ = "PENDIENTE POR ACTIVAR";
		}

		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO " + CatNominaInterbConstantes._CAT_EXT + " (");
		sql.append(CatNominaInterbConstantes._EXT_CNT + "," + CatNominaInterbConstantes._EXT_CTA + ",");
		sql.append(CatNominaInterbConstantes._EXT_STS + "," + CatNominaInterbConstantes._EXT_TIP + ",");
		sql.append(CatNominaInterbConstantes._EXT_SUC + "," + CatNominaInterbConstantes._EXT_INT + ",");
		sql.append(CatNominaInterbConstantes._EXT_PLZ + "," + CatNominaInterbConstantes._EXT_FOLE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FOLA + "," + CatNominaInterbConstantes._EXT_PRSE + ",");
		sql.append(CatNominaInterbConstantes._EXT_PRSA + "," + CatNominaInterbConstantes._EXT_FCHE + ",");
		sql.append(CatNominaInterbConstantes._EXT_FCHA + "," + CatNominaInterbConstantes._EXT_TIT + ",");
		sql.append(CatNominaInterbConstantes._EXT_OBS + "," + CatNominaInterbConstantes._EXT_LOT + ") ");
		sql.append(" VALUES (");
		sql.append("'" + contrato + "', '" + registro.cuenta.trim() + "', ");
		sql.append("'" + estatus + "', '" + tipoSol + "', ");
		sql.append("null, '" + registro.cveInterme.trim() + "', ");
		sql.append("'" + registro.cvePlaza.trim() + "', " + CatNominaInterbConstantes._SEQ_EXT + ".NEXTVAL, ");
		if (facultad.equals("AUTO")) // Folio Autorizacion
			sql.append(CatNominaInterbConstantes._SEQ_EXT + ".NEXTVAL, ");
		else
			sql.append("null, ");
		sql.append("'" + usuario + "', ");
		if (facultad.equals("AUTO")) //Usuario Autoriza
			sql.append("'" + usuario + "', ");
		else
			sql.append("null, ");
		sql.append("SYSDATE, ");
		if (facultad.equals("AUTO")) //Fecha Autoriza
			sql.append("SYSDATE, ");
		else
			sql.append("null, ");
		sql.append("'" + registro.nombreTitular.trim() + "', '" + observ + "', ");
		sql.append("null) ");
		return sql.toString();
	}   */

	/***************************************************************
	 *** Genera un numero desde la secuencia LOTE para insertarlo en	****
	 *** EWEB_CAT_NOM_AUT_EXT											****
	 **************************************************************/
	public int generaLote()
	{
		ResultSet rs = null;
		int numSecuencia = 0;
		try {
			query = "SELECT " + CatNominaInterbConstantes._SEQ_LOT + ".NEXTVAL AS " + CatNominaInterbConstantes._LOTE + " FROM DUAL";
			Crear(query);
			rs = Consulta();
			if(rs.next()) {
				numSecuencia = Integer.parseInt(rs.getString(CatNominaInterbConstantes._LOTE));
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - generaLoteok: " + numSecuencia,EIGlobal.NivelLog.INFO);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			numSecuencia = -100;
		}
		/* everis JARA - Ambientacion - Cerrando Conexion */
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaInterbDAO - Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
						}
				}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - generaLote: " + numSecuencia,EIGlobal.NivelLog.INFO);
		return numSecuencia;
		}

	/***************************************************************
	 *** VSWF - JGAL
	 *** Verifica si existe la cuenta en la tabla EWEB_CAT_NOM_EXTERNA	****
	 *** NOTA: No abre ni cierra conexion ni statement porque es
	 *** invocada desde un ciclo que realiza esas tareas				****
	 **************************************************************/
	public boolean existeCuenta(String contrato, String cuenta)
	{
		boolean res = false;
		ResultSet rs = null;
		int regs = 0;
		try {
			//query = "SELECT COUNT(1) as registros FROM " + CatNominaInterbConstantes._CAT_NOM + " WHERE " +
			query = "SELECT " + CatNominaInterbConstantes._NUM_CTAE + " FROM " + CatNominaInterbConstantes._CAT_NOM + " WHERE " +
				"trim(" + CatNominaInterbConstantes._CONTRATO + ") =  ? AND " +
				"trim(" + CatNominaInterbConstantes._NUM_CTAE + ") = ? AND rownum = 1";

			statement = conn.prepareStatement (query.toString());
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - contrato: [" + contrato + "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - cuenta: [" + cuenta + "]", EIGlobal.NivelLog.INFO);

			statement.setString(1, contrato);
			statement.setString(2, cuenta);
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - query: [" + query + "]", EIGlobal.NivelLog.INFO);
			rs = Consulta();
			if(rs.next()) {
				//regs = rs.getInt("registros");
				regs = 1;
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta -: " + cuenta + ", regs: " + regs, EIGlobal.NivelLog.INFO);
			}
			//statement.close();
			//statement = null;
			//rs.close();
			//rs = null;
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - se cierran objetos", EIGlobal.NivelLog.INFO);
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - Excepcion: " + e, EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
					EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - se cierra statement", EIGlobal.NivelLog.INFO);
				}
				if (rs != null) {
					rs.close();
					rs = null;
					EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - se cierra rs", EIGlobal.NivelLog.INFO);
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - falla controlada al cerrar objetos [" + e + "]", EIGlobal.NivelLog.INFO);
			}
		}
		if (regs > 0) {
			res = true;
		}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - resultado: " + res,EIGlobal.NivelLog.INFO);
		return res;
	}


	/***************************************************************
	 *** VSWF - JGAL
	 *** Verifica si existe la cuenta en la tabla EWEB_CAT_NOM_EXTERNA
	 *** en cuyo caso regresa un MAC_Interb								****
	 *** NOTA: No abre ni cierra conexion ni statement porque es
	 *** invocada desde un ciclo que realiza esas tareas				****
	 **************************************************************/
	public MAC_Interb obtenCuentaMAC(String contrato, String cuenta)
	{
		MAC_Interb res = new MAC_Interb();
		ResultSet rs = null;
		try {
			StringBuffer query = new StringBuffer();
			query.append("SELECT ");
			query.append(CatNominaInterbConstantes._NOM + ", ");
			query.append(CatNominaInterbConstantes._PZA_BAN + ", ");
			query.append(CatNominaInterbConstantes._INTERME + ", ");
			query.append(CatNominaInterbConstantes._NUM_CTAE + " ");
			query.append("FROM " + CatNominaInterbConstantes._CAT_NOM + " WHERE ");
			query.append(CatNominaInterbConstantes._CONTRATO + " = '" + contrato + "' AND ");
			query.append(CatNominaInterbConstantes._NUM_CTAE + " = '" + cuenta + "'");
			statement = conn.prepareStatement(query.toString());
			rs = Consulta();
			if (rs.next()) {
				res.setNombreTitular(rs.getString(CatNominaInterbConstantes._NOM).trim());
				res.setClavePlaza(rs.getString(CatNominaInterbConstantes._PZA_BAN).trim());
				res.setCveInterme(rs.getString(CatNominaInterbConstantes._INTERME).trim());
				res.setCuenta(rs.getString(CatNominaInterbConstantes._NUM_CTAE).trim());
			}
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - Excepcion: " + e, EIGlobal.NivelLog.ERROR);
		}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existeCuenta - resultado: " + res,EIGlobal.NivelLog.INFO);
		return res;
	}


	/***************************************************************************************
	 *** VSWF - JGAL
	 *** Verifica si existen solicitudes con la cuenta en la tabla EWEB_CAT_NOM_AUT_EXT	****
	 *** NOTA: No abre ni cierra conexion ni statement porque es
	 *** invocada desde un ciclo que realiza esas tareas								****
	 ***************************************************************************************/
	public boolean existenSolicitudes(String contrato, String cuenta /*, String tipoMvt*/)
	{
		boolean res = false;
		ResultSet rs = null;
		int regs = 0;
		try {
			StringBuffer query = new StringBuffer();
			//query.append("SELECT COUNT(1) as registros FROM " + CatNominaInterbConstantes._CAT_EXT + " WHERE ");
			query.append("SELECT " + CatNominaInterbConstantes._EXT_CTA + " FROM " + CatNominaInterbConstantes._CAT_EXT + " WHERE ");

			query.append("trim(" + CatNominaInterbConstantes._EXT_CNT + ") = ? AND ");
			query.append("trim(" + CatNominaInterbConstantes._EXT_CTA + ") = ? ");
			/*query.append(CatNominaInterbConstantes._EXT_TIP + " = '" + tipoMvt + "' ");*/
			query.append("AND (" + CatNominaInterbConstantes._EXT_STS + " = 'P' OR ");
			query.append(CatNominaInterbConstantes._EXT_STS + " = 'I') AND rownum = 1 ");

			statement = conn.prepareStatement (query.toString());

			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - contrato: [" + contrato + "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - cuenta: [" + cuenta + "]", EIGlobal.NivelLog.INFO);
			statement.setString(1, contrato);
			statement.setString(2, cuenta);
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - query: [" + query + "]", EIGlobal.NivelLog.INFO);
			rs = Consulta();
			if(rs.next()) {
				//regs = rs.getInt("registros");
				regs = 1;
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes: " + cuenta + ", solicitudes:" + regs, EIGlobal.NivelLog.INFO);
			}
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - Excepcion: " + e, EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
					EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - se cierra statement", EIGlobal.NivelLog.INFO);
				}
				if (rs != null) {
					rs.close();
					rs = null;
					EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - se cierra rs", EIGlobal.NivelLog.INFO);
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - falla controlada al cerrar objetos [" + e + "]", EIGlobal.NivelLog.INFO);
			}
		}
		if (regs > 0) {
			res = true;
		}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - existenSolicitudes - resultado: " + res,EIGlobal.NivelLog.INFO);
		return res;
	}

	/***************************************************************************************
	 *** VSWF - JGAL
	 *** Elimina una cuenta de la tabla eweb_cat_nom_externa
	 *** NOTA: No abre ni cierra conexion ni statement porque es
	 *** invocada desde un ciclo que realiza esas tareas								****
	 ***************************************************************************************/

	private int realizaBajaCuenta(String contrato, String cuenta) {
		int res = 0;
		try {
			StringBuffer query = new StringBuffer();
			query.append("DELETE FROM " + CatNominaInterbConstantes._CAT_NOM + " ");
			query.append("WHERE " + CatNominaInterbConstantes._CONTRATO + " = '" + contrato + "' AND ");
			query.append(CatNominaInterbConstantes._NUM_CTAE + " = '" + cuenta + "'");

			statement = conn.prepareStatement (query.toString());

			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - realizaBajaCuenta - query: [" + query + "]", EIGlobal.NivelLog.INFO);
			res = Inserta();
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - realizaBajaCuenta - Excepcion: " + e, EIGlobal.NivelLog.ERROR);
		}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - realizaBajaCuenta - resultado: " + res,EIGlobal.NivelLog.INFO);
		return res;
	}


	public ArrayList<String[]> obtenCuentas(String contrato) {
		ResultSet rs = null;
		ArrayList resultado = new ArrayList();
		try {
			StringBuffer query = new StringBuffer();
			query.append("SELECT ");
			query.append(CatNominaInterbConstantes._NOM + ", ");
			query.append(CatNominaInterbConstantes._PZA_BAN + ", ");
			query.append(CatNominaInterbConstantes._INTERME + ", ");
			query.append(CatNominaInterbConstantes._NUM_CTAE + " ");
			query.append("FROM " + CatNominaInterbConstantes._CAT_NOM + " WHERE ");
			query.append(CatNominaInterbConstantes._CONTRATO + " = '" + contrato + "' ");
			query.append("ORDER BY " + CatNominaInterbConstantes._NUM_CTAE);
			Crear(query.toString());
			rs = Consulta();
			while (rs.next()) {
				String[] datosCuentas = {
					rs.getString(CatNominaInterbConstantes._NOM),
					rs.getString(CatNominaInterbConstantes._PZA_BAN),
					rs.getString(CatNominaInterbConstantes._INTERME),
					rs.getString(CatNominaInterbConstantes._NUM_CTAE)
				};
				resultado.add(datosCuentas);
			}
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenCuentas -Error al cerrar conexion-> "+e,EIGlobal.NivelLog.ERROR);
			resultado = new ArrayList();
		}
		finally{
			try{
				if(rs!=null){
					rs.close();
					rs=null;}
				if(statement!=null){
					statement.close();
					statement=null;}
				if(conn!=null){
					conn.close();
					conn=null;}
			}catch(Exception e1){
				EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenCuentas -Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace ("CatNominaInterbDAO - obtenCuentas: [" + resultado.size() + "]", EIGlobal.NivelLog.INFO);
		return resultado;
	}

}