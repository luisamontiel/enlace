package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;

import javax.jms.JMSException;
import javax.naming.NamingException;

import mx.altec.enlace.beans.ODD4Bean;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class ODD4DAO {
	/** Id de una operacion exitosa, ya no existen mas registros */
	private static final String OPERACION_EXITOSA = "AVODA0002"; //"ODA0002";
	/** Id de operacion exitosa, existen mas registros */
	private static final String EXISTEN_MAS_REGISTROS = "AVPEA8001";
	/** Constante para Log */
	private static final String LOG_TAG = "[EDCPDFXML] ::: ODD4DAO ::: ";
	
	/**
	 * Ejecuta una consulta a la transaccion ODD4
	 * @param bean Objeto con la informacion de la cuenta
	 * @return Objeto con la informacion obtenida de la transaccion ODD4
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public ODD4Bean consultarODD4(ODD4Bean bean) throws BusinessException {
		EIGlobal.mensajePorTrace(LOG_TAG + "::consultaODD4() Inicio", NivelLog.INFO);
		try {
			final MQQueueSession ejecutor = new MQQueueSession(	NP_JNDI_CONECTION_FACTORY,NP_JNDI_QUEUE_RESPONSE,NP_JNDI_QUEUE_REQUEST);			
			final String respuesta = ejecutor.enviaRecibeMensaje(bean.generarMensajeEntrada());			
			final String[] respuestaSeparada = respuesta.substring(0,  respuesta.length()-1).split("@");
			EIGlobal.mensajePorTrace(String.format("%s:::consultaODD4() Trama de envio::%s\n" +
												   "%s:::consultaODD4() Trama de respuesta::%s",LOG_TAG,bean.generarMensajeEntrada(),LOG_TAG,respuesta), 
												   NivelLog.INFO);			
			String mensajeRespuesta = "",codigoOperacion = respuestaSeparada[2].substring(0, 9);
				
			EIGlobal.mensajePorTrace(String.format("Codigo Operacion Evaluado %s",codigoOperacion), NivelLog.DEBUG);
			if(OPERACION_EXITOSA.equals(codigoOperacion)){
				bean.procesarMensajeRespuesta(respuesta);
				bean.setCodigoOperacion(codigoOperacion);
			} else if(EXISTEN_MAS_REGISTROS.equals(codigoOperacion)){
				bean.setCodigoOperacion(codigoOperacion);
				bean.procesarMensajeRespuesta(respuesta);				
				do{	
					final String respuestaRellamado = ejecutor.enviaRecibeMensaje(bean.generarMensajeEntradaRellamado());
					final String[] respuestaSeparadaRellamado = respuestaRellamado.split("@");
					EIGlobal.mensajePorTrace(String.format("%s:::consultaODD4() Trama de envio::%s\n" +
														   "%s:::consultaODD4() Trama de respuesta::%s",
														   LOG_TAG, bean.generarMensajeEntradaRellamado(),LOG_TAG, respuestaRellamado), NivelLog.INFO);
					try {
						codigoOperacion = respuestaSeparadaRellamado[2].substring(0, 9);
						
						if(codigoOperacion != null && codigoOperacion.trim().equals(EXISTEN_MAS_REGISTROS) 
								|| codigoOperacion.trim().equals(OPERACION_EXITOSA)) {
							mensajeRespuesta = respuestaSeparadaRellamado[3];
						} else if (codigoOperacion != null && codigoOperacion.trim().equals("ERODE0065")) {
							bean.setTieneProblemaDatos(true);
							bean.setMensajeProblemaDatos("Por incongruencia de datos en la secuencia de domicilio no fue posible obtener el total de cuentas");
							codigoOperacion=OPERACION_EXITOSA;
							EIGlobal.mensajePorTrace(String.format("%s mensajeRespuesta Error de Datos en 390[%s]",LOG_TAG,respuestaRellamado), NivelLog.DEBUG);
						} else {
							bean.setTieneProblemaDatos(true);		
							codigoOperacion=OPERACION_EXITOSA;
							bean.setMensajeProblemaDatos(respuestaRellamado);
							EIGlobal.mensajePorTrace(String.format("%s mensajeRespuesta Error de Datos en 390 No Identificado[%s]",LOG_TAG,respuestaRellamado), NivelLog.DEBUG);
						}
					} catch (ArrayIndexOutOfBoundsException e) {						
						EIGlobal.mensajePorTrace(String.format("%s mensajeRespuesta Error de Datos e[%s]",LOG_TAG,respuestaRellamado), NivelLog.DEBUG);
						EIGlobal.mensajePorTraceExcepcion(e);
					}
					EIGlobal.mensajePorTrace(String.format("%s mensajeRespuesta[%s]",LOG_TAG,mensajeRespuesta), NivelLog.DEBUG);
					bean.procesarMensajeRespuestaRellamado(respuestaRellamado);					
				} while(EXISTEN_MAS_REGISTROS.equals(codigoOperacion));
			} else {
				// Si la transaccion responde con codigo de operacion diferente de exito.
				bean.setCodigoOperacion(codigoOperacion);
				bean.setMensajeOperacion(respuestaSeparada[2].substring(10, respuestaSeparada[2].length() - 1 ));
				EIGlobal.mensajePorTrace(String.format("Se recibio un error por parte de la transaccion ODD4, codigo de operacion: [%s]", bean.getCodigoOperacion()), NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(String.format("Mensaje de la operacion ODD4: [%s]", bean.getMensajeOperacion()), NivelLog.DEBUG);
			}			
		} catch (NamingException e) {
			EIGlobal.mensajePorTrace(String.format("%s BusinessException %s",LOG_TAG,new Formateador().formatea(e)), NivelLog.INFO);
			throw new BusinessException("Ocurrio un BusinessException",e);
		} catch (JMSException e) {
			EIGlobal.mensajePorTrace(String.format("%s JMSException %s",LOG_TAG,new Formateador().formatea(e)), NivelLog.INFO);
			throw new BusinessException("Ocurrio un JMSException",e);
		} catch (ValidationException e) {
			EIGlobal.mensajePorTrace(String.format("%s ValidationException %s",LOG_TAG,new Formateador().formatea(e)), NivelLog.INFO);
			throw new BusinessException("Ocurrio un ValidationException",e);
		} catch (MQQueueSessionException e) {
			EIGlobal.mensajePorTrace(String.format("%s MQQueueSessionException %s",LOG_TAG,new Formateador().formatea(e)), NivelLog.INFO);
			throw new BusinessException("Ocurrio un MQQueueSessionException",e);
		} catch (ArrayIndexOutOfBoundsException e) {
			EIGlobal.mensajePorTrace(String.format("%s ArrayIndexOutOfBoundsException %s",LOG_TAG,new Formateador().formatea(e)), NivelLog.INFO);
			throw new BusinessException("Ocurrio un ArrayIndexOutOfBoundsException",e);
		}		
		EIGlobal.mensajePorTrace(String.format("%s bean.getCuenta[%s]\n consultaODD4() Fin", LOG_TAG, bean.getCuentas().toString()),NivelLog.INFO);
		return bean;
	}
}
