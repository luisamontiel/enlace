/**
 *
 */
package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mx.altec.enlace.beans.DatosBeneficiarioBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * Clase de acceso a base de datos  para los datos complementarios de la cuenta beneficiaria
 * @author FSW Indra
 *
 */
public class DatosBeneficiarioDAO extends GenericDAO{

	/**
	 * Realizar los datos de las cuentas internacionales
	 * @param contrato Numero de contrato
	 * @param cuenta Numero de la cuenta
	 * @return DatosBeneficiarioBean bean con los datos de respuesta
	 */
	public DatosBeneficiarioBean consultarDatosBenef (String contrato, String cuenta) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		DatosBeneficiarioBean datosCtaInter = new DatosBeneficiarioBean();

		try {
			conn= createiASConnStatic (Global.DATASOURCE_ORACLE3 );
			sDup = conn.createStatement();
			query = "Select DIR_BEN_REC, CIUDAD_REC, ID_CLIENTE_REC, CVE_PAIS, " +
					"DESC_BANCO, DESC_TITULAR, CVE_DIVISA, CVE_PAIS_BENEF, " +
					"CVE_ABA, CVE_SWIFT, DESC_CIUDAD " +
					"From EWEB_CTAS_INTER " +
					"Where NO_CTA ='" + cuenta + "' " +
					"And NO_CONTRATO = '" + contrato + "'";

			EIGlobal.mensajePorTrace ("DatosBeneficiarioDAO::consultarDatosBenef :: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				datosCtaInter.setDireccion(rs.getString("DIR_BEN_REC")!=null?rs.getString("DIR_BEN_REC").trim():"");
				datosCtaInter.setCiudad(rs.getString("CIUDAD_REC")!=null?rs.getString("CIUDAD_REC").trim():"");
				datosCtaInter.setIdCliente(rs.getString("ID_CLIENTE_REC")!=null?rs.getString("ID_CLIENTE_REC").trim():"");
				datosCtaInter.setCvePais(rs.getString("CVE_PAIS")!=null?rs.getString("CVE_PAIS").trim():"");
				datosCtaInter.setDescBanco(rs.getString("DESC_BANCO")!=null?rs.getString("DESC_BANCO").trim():"");
				datosCtaInter.setDescTitular(rs.getString("DESC_TITULAR")!=null?rs.getString("DESC_TITULAR").trim():"");
				datosCtaInter.setCveDivisa(rs.getString("CVE_DIVISA")!=null?rs.getString("CVE_DIVISA").trim():"");
				datosCtaInter.setCvePaisBenef(rs.getString("CVE_PAIS_BENEF")!=null?rs.getString("CVE_PAIS_BENEF").trim():"");
				datosCtaInter.setCveAba(rs.getString("CVE_ABA")!=null?rs.getString("CVE_ABA").trim():"");
				datosCtaInter.setCveSwift(rs.getString("CVE_SWIFT")!=null?rs.getString("CVE_SWIFT").trim():"");
				datosCtaInter.setDescCiudad(rs.getString("DESC_CIUDAD")!=null?rs.getString("DESC_CIUDAD").trim():"");
			}

			EIGlobal.mensajePorTrace ("DatosBeneficiarioDAO::consultarDatosBenef:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("DatosBeneficiarioDAO::consultarDatosBenef:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
				rs.close();
				sDup.close();
				conn.close();
			}catch(SQLException e1){
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar los datos de cuentas internacionales", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("DatosBeneficiarioDAO::consultarDatosBenef:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return datosCtaInter;
	}

	/**
	 * Actualiza los datos de la cuenta beneficiaria
	 * @param beneficiarioBean Bean con la informacion a almacenar
	 * @param contrato Contrato
	 * @param cuenta Cuenta
	 * @return boolean indicador de actualización
	 */
	public boolean almacenaDatosBenef (DatosBeneficiarioBean beneficiarioBean,
			String contrato, String cuenta) {
		Connection conn = null;
		Statement sDup = null;
		int rs = 0;
		boolean resultado = false;
		String query = null;

		try {
			conn= createiASConnStatic (Global.DATASOURCE_ORACLE3 );
			sDup = conn.createStatement();

			query = "Update EWEB_CTAS_INTER set " +
					"DIR_BEN_REC='" + beneficiarioBean.getDireccion() + "'" +
					", CIUDAD_REC='" + beneficiarioBean.getCiudad() + "'" +
					", ID_CLIENTE_REC='" + beneficiarioBean.getIdCliente() + "'" +
					", CVE_PAIS_BENEF='" + beneficiarioBean.getCvePaisBenef() + "' " +
					"Where NO_CTA ='" + cuenta + "' " +
					"And NO_CONTRATO = '" + contrato + "'";

			EIGlobal.mensajePorTrace ("DatosBeneficiarioDAO::almacenaDatosBenef :: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeUpdate(query);

			if(rs > 0){
				resultado = true;
			}

			EIGlobal.mensajePorTrace ("DatosBeneficiarioDAO::almacenaDatosBenef:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("DatosBeneficiarioDAO::almacenaDatosBenef:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
				sDup.close();
				conn.close();
			}catch(SQLException e1){
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al actualizar los datos de cuentas internacionales", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("DatosBeneficiarioDAO::almacenaDatosBenef:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}
}
