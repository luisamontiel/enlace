package mx.altec.enlace.dao;

import java.sql.Connection;
import java.util.List;

import mx.altec.enlace.beans.FiltroConsultaBean;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.beans.TrxLM1JBean;

/**
 * @author ESC
 * Clase que realiza la conexi&oacuten a la BD
 */
public interface EnlcTarjetaDAO {
	
	/**
	 * Metodo para iniciar transaccion con la conecction
	 * @return
	 */
	public Connection initTransac();
	
	/**
	 * Metodo para cerrar transaccion con la conecction
	 * @return
	 */
	public void closeTransac();
	
	/**
	 * Metodo que consulta tarjetas en la BD EWEB_VINC_TARJETACON de acuerdo a los filtros
	 * @param filtros para ejecutar la consulta
	 * @return TarjetaContrato bean
	 * @throws DaoException controlada
	 */
	public List<TarjetaContrato> consultarDatosTarjetas(FiltroConsultaBean filtros)throws DaoException;
	
	/**
	 * Metodo que elimina tarjetas en la BD EWEB_VINC_TARJETACON de acuerdo a los parametros
	 * @param tarjeta a eliminar
	 * @return int entero del resultado de la eliminacion
	 * @throws DaoException controlada 
	 */
	public int eliminarTarjeta(TarjetaContrato tarjeta)throws DaoException;
	
	
	/**
	 * Metodo que agrega una tarjeta a la BD EWEB_VINC_TARJETACON
	 * @param tarjeta que se agrega
	 * @return int resultado del insert
	 * @throws DaoException controlada
	 */
	public int agregarTarjetaContrato(TarjetaContrato tarjeta)throws DaoException;
	
	/**
	 * Metodo encargado de regresar el id de la secuencia de alta de cuentas
	 * @return idLote					Id del lote de carga
	 * @throws DaoException				Error de tipo DaoException
	 */
	public Long getIdLoteAltaSequence()throws DaoException;
	
	/**
	 * Metodo para generar la consulta de tarjetas
	 * @param bean para la transacci&oacute;n LM1J
	 * @return lista de tarjetas
	 */
	public TrxLM1JBean consultaLM1J(TrxLM1JBean bean);
}
