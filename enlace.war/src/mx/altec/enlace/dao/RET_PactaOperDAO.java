package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


/**
 *  Clase para el negocio de acceso a base de datos para la consulta de operaciones
 *  Reuters pactadas del d�a
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 10, 2012
 * 		Control de cambios:
 * @author TCS
 * @version 1.1 Ago 10, 2016 Se agregan tratamiento FLAME 
 */
public class RET_PactaOperDAO extends BaseServlet{

	/**
	 * Numero id de version serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Variable Connection para uso transaccional
	 */
	private Connection conn = null;

	/**
	 * Variable Statement para realizar batch
	 */
	private Statement stmt = null;

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y poner commit en false.
	 *
	 * @return conn				Connection de la transacci�n.
	 */
	public Connection initTransac(){
		try{
			conn = createiASConn (Global.DATASOURCE_ORACLE4);
			conn.setAutoCommit(false);
			stmt = conn.createStatement();
		}catch(SQLException e){
			EIGlobal.mensajePorTrace("RET_PactaOperDAO::initConnTransac:: Problemas al crear la conexi�n", EIGlobal.NivelLog.INFO);
		}
		return conn;
	}

	/**
	 * Metodo para ejecutar una transacci�n en batch
	 * la transacci�n y poner commit en false.
	 *
	 * @return ejecBatch				Arreglo con ejecuci�n del batch
	 */
	public int[] execTransac(){
		int[] ejecBatch = null;
		try{
			ejecBatch = this.stmt.executeBatch();
		}catch(SQLException e){
			EIGlobal.mensajePorTrace("RET_PactaOperDAO::initConnTransac:: Problemas al ejecutar el batch->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
		return ejecBatch;
	}

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta commit.
	 *
	 */
	public void closeTransac(){
		try{
			this.conn.commit();
			this.stmt.close();
			this.conn.close();
		}catch(SQLException e){
			EIGlobal.mensajePorTrace("RET_PactaOperDAO::closeConnTransac:: Problemas al cerrar la conexi�n->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta rollback.
	 *
	 */
	public void closeTransacError(){
		try{
			this.conn.rollback();
			this.stmt.close();
			this.conn.close();
		}catch(SQLException e){
			EIGlobal.mensajePorTrace("RET_PactaOperDAO::closeConnTransac:: Problemas al cerrar la conexi�n con errores->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}

    /**
     * M�todo que realiza el registro de los atributos del cargo de una operaci�n reuters
     *
     * @param numContrato 		Numero de Contrato
     * @param numUsuario		Numero de Usuario
     * @return exito			Bandera que indica que no hubo problemas con la actualizaci�n del registro
     */
	public boolean guardaContratoRET(String numContrato, String numUsuario) {
		boolean exito = false;
		int i = 0;

		try {
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE4);
			Statement sDup = conn.createStatement();
			StringBuffer query = new StringBuffer("");

			query.append("Update USERS Set ");
			query.append("FAX = '" + numContrato + "' ");
			query.append("Where USERNAME = '" + numUsuario + "' ");
			EIGlobal.mensajePorTrace("RET_PactaOperDAO::guardaContratoRET:: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			i = sDup.executeUpdate(query.toString());
			if(i>0)
				exito = true;

			sDup.close();
			EIGlobal.mensajePorTrace("RET_PactaOperDAO::guardaContratoRET:: Saliendo...", EIGlobal.NivelLog.DEBUG);
			}catch (Exception e){
	    		EIGlobal.mensajePorTrace("RET_PactaOperDAO::guardaContratoRET:: Se controlo problema al registrar el contrato en RET, "
	    				+ "tipo de error->" + e.getMessage(),EIGlobal.NivelLog.INFO);
	    	}
		return exito;
	}
	
	/**
	 * Metodo que valida si el usuario es cliente FLAME
	 * @param numContrato numero de contrato cliente enlace
	 * @return clienteFlame respuesta cliente
	 */
	public boolean isFlameClient(String numContrato) {
		
		EIGlobal.mensajePorTrace("RET_PactaOperDAO::isFlameClient::valida si el cliente es FLAME", EIGlobal.NivelLog.DEBUG);
		
		ResultSet rs = null;
		boolean contrato = false;
		String query = null;
		conn=null;
		
		EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consulta:: Entrando a consulta", EIGlobal.NivelLog.DEBUG);
		try{
			conn = createiASConn (Global.DATASOURCE_ORACLE3);
			query  = "select num_contr_pk "
				   + "From BEW_MX_MAE_CLIENTESFLAME "
			       + "Where num_contr_pk = '" + numContrato + "' ";
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::isFlameClient:: Query-> " + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query,ResultSet.TYPE_SCROLL_INSENSITIVE,
	        		ResultSet.CONCUR_UPDATABLE);
			rs = sDup.executeQuery();

			while(rs.next()){
				contrato = rs.getBoolean("num_contr_pk");
			}
			sDup.close();
		}catch(SQLException e){
			EIGlobal.mensajePorTrace ("RET_ConsultaOperDAO::consulta:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		} finally {
			try {
				rs.close();
			} catch (SQLException e1) {
			}
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
		EIGlobal.mensajePorTrace("RET_PactaOperBO::isFlameClient::El cliente es flame: "+contrato, EIGlobal.NivelLog.DEBUG);
		
		return contrato;
		
	}
}
