/** 
*   Isban Mexico
*   Clase: ConsultaCuentasDAO.java
*   Descripcion: Objeto de acceso a datos para la consulta de cuentas.
*   
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Implementacion permite la gestion de transferencia en dolares(Spid). 
*/
package mx.altec.enlace.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import mx.altec.enlace.beans.CuentaBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


/**
 * The Class ConsultaCuentasDAO.
 */
public class ConsultaCuentasDAO {
	
	/** The Constant QUERY_PLAZA. */
	private static final String QUERY_PLAZA = "SELECT DESCRIPCION FROM COMU_PLA_BANXICO WHERE PLAZA_BANXICO = ";
	
	/** The Constant QUERY_BANCO. */
	private static final String QUERY_BANCO = "SELECT NOMBRE_CORTO FROM COMU_INTERME_FIN WHERE NUM_CECOBAN <> 0 AND CVE_INTERME = ";

	/** The Constant QUERY_PAIS. */
	private static final String QUERY_PAIS = "SELECT DESCRIPCION FROM COMU_PAISES WHERE CVE_PAIS = ";
	
	/** The Constant QUERY_MONEDA. */
	private static final String QUERY_DIVISA = "SELECT DESCRIPCION FROM COMU_MON_DIVISA WHERE CVE_DIVISA = ";
	
	/**
	 * Consulta cuentas.
	 * @param archivo						El archivo
	 * @param modulo						El modulo
	 * @param argParametrosNoContemplados	Los parametros no contemplados
	 * @return								La lista de cuentas
	 * @throws DaoException					La excepcion en caso de que ocurra un error
	 */
	public List<CuentaBean> consultaCuentas(String archivo, String modulo, String[] argParametrosNoContemplados) throws DaoException {
		EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: v20150401_1848", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: consultaCuentas", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: archivo:" + archivo, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: modulo:" + modulo, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(" ConsultaCuentasDAO- argParametrosNoContemplados -" + argParametrosNoContemplados[0] + "-", EIGlobal.NivelLog.DEBUG);
		List<CuentaBean> listaCuentas = new ArrayList<CuentaBean>();
		
		BufferedReader entrada = null;
		try {
			entrada = new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR + "/" + archivo));
			String linea = "";
			CuentaBean cuenta = new CuentaBean();
			
			if(argParametrosNoContemplados[0].equals("true") && modulo.equals(IEnlace.MDep_inter_cargo)){
				EIGlobal.mensajePorTrace(" ES valor SIN_CUENTA-" + argParametrosNoContemplados[0] + "-", EIGlobal.NivelLog.DEBUG);
				cuenta.setNumCuenta("SIN CUENTA");
				cuenta.setDescripcion("SIN TITULAR");
				cuenta.setTipoCuenta("P");
				cuenta.setTipoProd("1");
				listaCuentas.add(cuenta);
			}
			
			while((linea  = entrada.readLine()) != null ) {
				cuenta = new CuentaBean();
				
				if(listaCuentas.size() >= 25) { //Valida si excede las 25 cuentas...
					cuenta.setNumCuenta("");
					cuenta.setDescripcion("La consulta gener� m�s de 25 resultados, debe ser m�s espec�fico en el filtro");
					cuenta.setTipoCuenta("");
					listaCuentas.add(cuenta);
					return listaCuentas;
				}
				
				int numeroSeparadores = StringUtils.countMatches(linea, "@");
				//EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: numeroSeparadores:" + numeroSeparadores, EIGlobal.NivelLog.DEBUG);
				String[] datos = StringUtils.splitPreserveAllTokens(linea, '@'); //FIXME Corte de acuerdo a tipo
				EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: datosArray:" + ToStringBuilder.reflectionToString(datos), EIGlobal.NivelLog.DEBUG);
				//Valida formato de linea de acuerdo al modulo
				//Extrae datos de la cuenta y los almacena en un DTO
				if(!
					(
					(modulo.equals(IEnlace.MEnvio_pago_imp) && numeroSeparadores != 17) ||
					(modulo.equals(IEnlace.MDep_Inter_Abono) || modulo.equals(IEnlace.MCons_Baja_Cta) && numeroSeparadores != 5 ) ||
					(numeroSeparadores != 8)
					)
				) {
					EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: Validacion modulo 1", EIGlobal.NivelLog.DEBUG);
					if(modulo.equals(IEnlace.MAbono_TI)) { //Cuentas bancos extranjeros
						EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: bancos extranjeros", EIGlobal.NivelLog.DEBUG);
						armaCuentaBancosExtranjeros(cuenta, datos);
					} else if(modulo.equals(IEnlace.MDep_Inter_Abono)) { //Cuentas otros bancos
						EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: otros bancos", EIGlobal.NivelLog.DEBUG);
						armaCuentaOtrosBancos(cuenta, datos);
					} else if(modulo.equals(IEnlace.MCons_Ctas_movil)) { //Cuentas moviles
						EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: moviles", EIGlobal.NivelLog.DEBUG);
						armaCuentaMoviles(cuenta, datos);
					} else {
						EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: otras cuentas", EIGlobal.NivelLog.DEBUG);
						armacuentaOtros(cuenta, datos);
					}
					listaCuentas.add(cuenta);
				} else if(modulo.equals(IEnlace.MDep_Inter_Abono) || modulo.equals(IEnlace.MCons_Baja_Cta)) { // Cuentas bancos nacionales
					EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: Validacion modulo 2", EIGlobal.NivelLog.DEBUG);
					armaCuentaBancosNacionales(cuenta, datos);
					listaCuentas.add(cuenta);
				} else if(modulo.equals(IEnlace.MAbono_transf_pesos)) { // Cuentas mismo banco moneda nacional
					EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: Validacion modulo 3", EIGlobal.NivelLog.DEBUG);
					armacuentaMismoBancoMonedaNacional(cuenta, datos);
					listaCuentas.add(cuenta);
				}
			}
		} catch (FileNotFoundException e) {
			throw new DaoException("* :: ConsultaCuentasDAO :: No se encontro archivo de cuentas...", e);
		} catch (IOException e) {
			throw new DaoException("* :: ConsultaCuentasDAO :: No se puede leer el archivo de cuentas", e);
		} finally {
			if(entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					EIGlobal.mensajePorTrace("* :: ConsultaCuentasDAO :: Error al cerrar buffer de lectura de archivo de cuentas...", EIGlobal.NivelLog.ERROR);
				}
			}
		}
		return listaCuentas;
	}
	
	/**
	 * Arma cuenta moviles.
	 * @param cuenta the cuenta
	 * @param datos the datos
	 */
	private void armaCuentaMoviles(CuentaBean cuenta, String[] datos) {
		cuenta.setNumCuenta(datos[0]);
		cuenta.setTipoCuenta(datos[1]);
		cuenta.setBanco(getBanco(datos[1]));
		cuenta.setDescripcion(datos[2]);
	}

	/**
	 * Armacuenta otros.
	 * @param cuenta the cuenta
	 * @param datos the datos
	 */
	private void armacuentaOtros(CuentaBean cuenta, String[] datos) {
		cuenta.setNumCuenta(datos[0]);
		cuenta.setDescripcion(datos[2]);
		cuenta.setTipoProd(datos[6]);
		cuenta.setTipoCuenta(datos[7]);
	}

	/**
	 * Armacuenta mismo banco moneda nacional.
	 * @param cuenta the cuenta
	 * @param datos the datos
	 */
	private void armacuentaMismoBancoMonedaNacional(CuentaBean cuenta, String[] datos) {
		cuenta.setNumCuenta(datos[0]);
		cuenta.setDescripcion(datos[2]);
		cuenta.setTipoProd(datos[6]);
		cuenta.setTipoCuenta(datos[7]);
	}
	
	/**
	 * Arma cuenta bancos nacionales.
	 * @param cuenta the cuenta
	 * @param datos the datos
	 */
	private void armaCuentaBancosNacionales(CuentaBean cuenta, String[] datos) {
		cuenta.setNumCuenta(datos[0]);
		cuenta.setBanco(getBanco(datos[1]));
		cuenta.setDescripcion(datos[2]);
		cuenta.setPlaza(getPlaza(datos[3]));
		cuenta.setSucursal(datos[4]);
		cuenta.setTipoProd(datos[1]);
		cuenta.setTipoCuenta(datos[3]);
		cuenta.setDivisa(datos.length >= 6 ? datos[5] : "");		
	}

	/**
	 * Arma cuenta otros bancos.
	 * @param cuenta the cuenta
	 * @param datos the datos
	 */
	private void armaCuentaOtrosBancos(CuentaBean cuenta, String[] datos) {
		cuenta.setNumCuenta(datos[0]);
		cuenta.setDescripcion(datos[2]);
		cuenta.setBanco(getBanco(datos[5]));
//		cuenta.setTipoProd(datos[5]);
		cuenta.setPlaza(getPlaza(datos[4]));
		cuenta.setSucursal(datos[3]);
	}

	/**
	 * Arma cuenta bancos extranjeros.
	 * @param cuenta the cuenta
	 * @param datos the datos
	 */
	private void armaCuentaBancosExtranjeros(CuentaBean cuenta, String[] datos) {
		cuenta.setNumCuenta(datos[0]);
		cuenta.setDescripcion(datos[7]); 
		cuenta.setBanco(getBanco(datos[6]));
		cuenta.setPais(getPais(datos[2]));
		cuenta.setDivisa(getMoneda(datos[3]));
		cuenta.setCiudad(datos[5]);
		cuenta.setTipoProd(datos[4]);
	}
	
	/**
	 * Gets the plaza.
	 * @param cvePlaza the cve plaza
	 * @return the plaza
	 */
	private String getPlaza(String cvePlaza) {
		EIGlobal.mensajePorTrace( "ConsultaCuentasDAO - getPlaza " + cvePlaza, EIGlobal.NivelLog.DEBUG);
		return consultaCatalogo(QUERY_PLAZA, cvePlaza);
	}
	
	/**
	 * Gets the banco.
	 * @param cveBanco the cve banco
	 * @return the banco
	 */
	private String getBanco(String cveBanco) {
		EIGlobal.mensajePorTrace( "ConsultaCuentasDAO - getBanco " + cveBanco, EIGlobal.NivelLog.DEBUG);
		return consultaCatalogo(QUERY_BANCO, cveBanco);
	}
	
	/**
	 * Gets the pais.
	 * @param cvePais the cve pais
	 * @return the pais
	 */
	private String getPais(String cvePais) {
		EIGlobal.mensajePorTrace( "ConsultaCuentasDAO - getPais " + cvePais, EIGlobal.NivelLog.DEBUG);
		return consultaCatalogo(QUERY_PAIS, cvePais);
	}
	
	/**
	 * Gets the moneda.
	 * @param cveMoneda the cve moneda
	 * @return the moneda
	 */
	private String getMoneda(String cveMoneda) {
		EIGlobal.mensajePorTrace( "ConsultaCuentasDAO - getMoneda " + cveMoneda, EIGlobal.NivelLog.DEBUG);
		return consultaCatalogo(QUERY_DIVISA, cveMoneda);
	}
	
	/**
	 * Obtiene el nombre de la plaza.
	 * @param qry the qry
	 * @param filtro the filtro
	 * @return the plaza
	 */
	private String consultaCatalogo(String qry, String filtro) {
		String result = filtro;
		PreparedStatement stm = null;
		ResultSet resultset = null;
		Connection conn = null;

		try {
			conn = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			stm = conn.prepareStatement(qry + "'" + filtro + "'", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//stm.setString(1, filtro);
			
			resultset = stm.executeQuery();
			
			if(resultset != null && resultset.first()) {
				result = resultset.getString(1).trim();
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("consultaCatalogo: Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("consultaCatalogo: Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
				e.printStackTrace();
			}
		}
		return result;
	}
}