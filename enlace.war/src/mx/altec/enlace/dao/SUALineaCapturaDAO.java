package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.Global.NP_MQ_USUARIO;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import mx.altec.enlace.beans.SUALCLZS8;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

public class SUALineaCapturaDAO {


	public static final String HEADERLZS0="LZS002151123451O00N2";
	public static final String HEADERLZS8="LZS802981123451O00N2";

	//variables para LZS8
	public static final String CVE_OPER_CARGO="2547";
	public static final String CVE_OPER_ABONO="0254"; //JGAL
	public static final String LITERAL_CARGO =
		"                                                                 ";
	public static final String TRAMA48= new StringBuffer()
		.append("                                                ").toString();
	public static final String TRAMA95= new StringBuffer()
		.append("                                                  ")
		.append("                                             ").toString();

	//variables para respuesta exito y error
	private static final String CODIGO_ERROR="@ER";
	private static final String CODIGO_EXITO="@11";

	/** copy "@DCLZMCAM P" */
	private static final String COPYMCAM ="@DCLZMCAB  P";

	private static final String COPYLZM0 ="@DCLZM0S0S P";

	/**
	 * @author lespinosa <VC> 26-Ag0-2009
	 * Metodo para invocar la transaccion para la validacin de la Linea de captura
	 * @param lineaCaptura
	 * @param importeTotal
	 * @return String con respuesta de validacion
	 */
	public SUALCLZS8 validaLC(String lineaCaptura, String importeTotal) {

		StringBuffer mensajeMQ= new StringBuffer()
			.append(rellenar(NP_MQ_TERMINAL, 4))
			.append(rellenar(NP_MQ_USUARIO, 8))
			.append(HEADERLZS0)
			.append(rellenar(lineaCaptura, 53))//LC
			.append("                    ")//cta cargo
			.append(rellenar(importeTotal.replace(".",""), 15,'0','I'))//imp Tot
			.append(TRAMA95);
		String respuesta=null;

		EIGlobal.mensajePorTrace("Trama valida-LC: [" +mensajeMQ.toString()+"]", EIGlobal.NivelLog.INFO);
		
		MQQueueSession mqTux = null;
		try {
			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			respuesta = mqTux.enviaRecibeMensaje(mensajeMQ.toString());
			EIGlobal.mensajePorTrace("RespuestaMQ valida-LC: [" + respuesta+"]",EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			String strError;
			strError = e.getMessage();
			EIGlobal.mensajePorTrace ( "ERROR::MQ::" + strError + ":" , EIGlobal.NivelLog.ERROR);
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}
		//return desentramaValidacionLC(respuesta);
		return desentramaValidacionLC(respuesta);
	}

	private SUALCLZS8 desentramaValidacionLC(String trama) {

			SUALCLZS8 beanlzs8= new SUALCLZS8();
			if(trama==null || trama.equals("")){
				//"Ocurrio un error al realizar la conexion a MQ"
				beanlzs8.setMsjErr("Ocurrio un error al realizar la conexion a MQ");
				beanlzs8.setReferencia("Error Inesperado en la conexion");
			}
			//Validamos que la respuesta sea correcta
			if(trama!=null && trama.contains(CODIGO_EXITO)){
				//buscamos la informacion de respuesta
				//String copyMCam;
				String copyMCam = trama.substring(trama.indexOf(COPYLZM0)+12);
				if(trama.contains(COPYLZM0)){
					//desentrama copy CAM
					copyMCam = trama.substring(trama.indexOf(COPYLZM0)+12);
					beanlzs8.setLineaCap(copyMCam.substring(0,53));//	LINEA DE CAPTURA SUA  //20 bLANCOS
					beanlzs8.setCuentaCargo(copyMCam.substring(53,73));//CUENTA CARGO    	     
					beanlzs8.setImporteTotal(copyMCam.substring(74,88));//IMPORTE TOTAL        
					beanlzs8.setRegPatronal(copyMCam.substring(88,99));//REGISTRO PATRONAL    
					beanlzs8.setPeriodoPago(copyMCam.substring(99,105));//PERIODO DE PAGO     
					EIGlobal.mensajePorTrace("VAO VAL LC: [" + copyMCam.substring(99,107)+"]",EIGlobal.NivelLog.INFO);
					beanlzs8.setOrigenArchivo(copyMCam.substring(105,106));//ORIGEN DEL ARCHIVO   
					beanlzs8.setFolioSua((copyMCam.substring(106,113)));//FOLIO SUA            
					beanlzs8.setFechaVencimiento((copyMCam.substring(113,123)));//FECHA VENCIMIENTO    
					beanlzs8.setImporteImss(copyMCam.substring(123,138));//IMPORTE IMSS         
					beanlzs8.setImporteRCV(copyMCam.substring(138,153));//IMPORTE RCV          
					beanlzs8.setImporteVivienda(copyMCam.substring(153,168));//IMPORTE VIVIENDA     
					beanlzs8.setImporteACV(copyMCam.substring(168,183));//IMPORTE ACV          
					beanlzs8.setMedioPresentacion("INTERNET");
					beanlzs8.setReferencia("");//FolioSUA
				}

				beanlzs8.setMsjErr("OK");
				beanlzs8.setCodErr("Operacion Exitosa");
				beanlzs8.setEstatus("Operacion Exitosa");
			}else if (trama!=null && trama.contains(CODIGO_ERROR)){
				//buscar el codigo de error
				if (trama.indexOf(CODIGO_ERROR) != -1) {
					//Validar si se envia el mensaje o codigo o ambos
					int index = trama.indexOf(CODIGO_ERROR) + 10;
					int indexfinCE=trama.length()-2;

					String msjErr=trama.substring(index,indexfinCE);

					if(msjErr.contains("@"))msjErr = msjErr.substring(0,msjErr.indexOf("@")-2);

					beanlzs8.setMsjErr(msjErr);
					beanlzs8.setCodErr(trama.substring(index, index + 8));
					beanlzs8.setEstatus("Operacion No Exitosa - " + msjErr);
					beanlzs8.setCodOperacion("");
					beanlzs8.setFechaOperacion("");
					beanlzs8.setHoraOperacion("");
					beanlzs8.setImporteTotal("");
					beanlzs8.setRegPatronal("");
					beanlzs8.setPeriodoPago("");
					beanlzs8.setFolioSua("");
					beanlzs8.setReferencia("Imprimir Error");
					beanlzs8.setOrigenArchivo("");
					beanlzs8.setFechaVencimiento("");
					beanlzs8.setImporteImss("");
					beanlzs8.setImporteRCV("");
					beanlzs8.setImporteVivienda("");
					beanlzs8.setImporteACV("");
				}
			}
		//---------------VAO 09-2012 -----------------------------
		return beanlzs8;
	}

	/**
	 * Metodo para el pago de SUA LC
	 * @author lespinosa <VC> LFER
	 *
	 * @param ctaCargo
	 * @param ctaAbono
	 * @param fechaGiro
	 * @param importe
	 * @param lineaCaptura
	 * @return
	 */
	public SUALCLZS8 pagoSUALC(String ctaCargo, String ctaAbono,String fechaGiro,String importe ,String lineaCaptura){
		StringBuffer mensajeMQ= new StringBuffer()
			.append(rellenar(NP_MQ_TERMINAL, 4))
			.append(rellenar(NP_MQ_USUARIO, 8))
			.append(HEADERLZS8)
			.append(rellenar(ctaCargo, 23))//cta-cargo-div
			.append("          ")//fehca-giro
			.append(rellenar(ctaAbono, 23))//cta-abono-div
			.append(rellenar(importe.replace(".",""), 15,'0','I'))//importe
			.append(CVE_OPER_CARGO)//clave oper. cargo
			.append("         ") //numerocheque
			.append(CVE_OPER_ABONO) //Clave oper abono //JGAL
			.append(LITERAL_CARGO)//litral cargo
			.append(rellenar(lineaCaptura,65))//literal abono
			.append(TRAMA48)//
			;
		String respuesta=null;
		MQQueueSession mqTux = null;
		EIGlobal.mensajePorTrace("Trama SUALC: [" +mensajeMQ.toString()+"]", EIGlobal.NivelLog.INFO);
		try {
			 mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE,
					NP_JNDI_QUEUE_REQUEST);
			respuesta = mqTux.enviaRecibeMensaje(mensajeMQ.toString());
			EIGlobal.mensajePorTrace("RespuestaMQ: [" + respuesta+"]",EIGlobal.NivelLog.INFO);

		}catch(Exception e)
		{

			String strError = e.getMessage();
			EIGlobal.mensajePorTrace ( "ERROR::MQ::" + strError + ":" , EIGlobal.NivelLog.ERROR);
		}
		finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}
		return  desentramaLZS8(respuesta) ;
	}

	/**
	 * Metodo para el desentramado de la respuesta de la transaccion LZS8
	 * @param respuesta
	 * @return string respuesta
	 */
	private SUALCLZS8 desentramaLZS8(String trama){
		SUALCLZS8 beanlzs8= new SUALCLZS8();
		if(trama==null){
			beanlzs8.setMsjErr("Ocurrio un error al realizar la conexion a MQ");
			beanlzs8.setReferencia("Error Inesperado en la conexion");
		}
		//Validamos que la respuesta sea correcta
		if(trama!=null && trama.contains(CODIGO_EXITO)){
			//buscamos la informacion de respuesta
			//String copyMCom;
			String copyMCam;
			if(trama.contains(COPYMCAM)){
				//desentrama copy CAM
				copyMCam = trama.substring(trama.indexOf(COPYMCAM)+12);

				//beanlzs8.setCodOperacion(copyMCam.substring(58,62));  //codigoOperacion   58-62 (4)
				beanlzs8.setFechaOperacion(copyMCam.substring(115,125)); //Fecha Operacion  115-125 ()
				beanlzs8.setHoraOperacion(copyMCam.substring(231,236));  //Hora Operacion   231-236 ()
				beanlzs8.setImporteTotal(copyMCam.substring(236,251));//importeTotal        236-251(15)
				beanlzs8.setRegPatronal(copyMCam.substring(251,262)); //regisPatronal       251-262(11)
				beanlzs8.setPeriodoPago(copyMCam.substring(262,268)); //periodoPago         262-268(6)
				beanlzs8.setOrigenArchivo(copyMCam.substring(268,269)); //Origen Archivo    268-269(1)
				beanlzs8.setFolioSua(copyMCam.substring(269,276));    //folio sua           269-276(7)
				beanlzs8.setFechaVencimiento(copyMCam.substring(276,286));//FechaVencimiento 276-286(10)
				beanlzs8.setImporteImss(copyMCam.substring(286,301));//Importe IMSS         286-301(15)
				beanlzs8.setImporteRCV(copyMCam.substring(301,316));//Importe RCV           301-316(15)
				beanlzs8.setImporteVivienda(copyMCam.substring(316,331));//Importe Vivienda 316-331(15)
				beanlzs8.setImporteACV(copyMCam.substring(331,346));//Importe ACV            331-346(15)
				beanlzs8.setMedioPresentacion("INTERNET");
				beanlzs8.setReferencia(copyMCam.substring(269,276));//FolioSUA
				beanlzs8.setCodOperacion(copyMCam.substring(222,231));  //Num Movimiento   222-131 (9)
			}

			beanlzs8.setMsjErr("Operacion Exitosa");
			beanlzs8.setCodErr("Operacion Exitosa");
			beanlzs8.setEstatus("Operacion Exitosa");
		}else if (trama!=null && trama.contains(CODIGO_ERROR)){
			//buscar el codigo de error
			if (trama.indexOf(CODIGO_ERROR) != -1) {
				//Validar si se envia el mensaje o codigo o ambos
				int index = trama.indexOf(CODIGO_ERROR) + 3;

				int indexfinCE=trama.length()-2;
				String msjErr=trama.substring(index + 8,indexfinCE);
				if(msjErr.contains("@")){
					msjErr = msjErr.substring(0,msjErr.indexOf("@")-2);
				}
				beanlzs8.setMsjErr(msjErr);
				beanlzs8.setCodErr(trama.substring( index, index + 8));
				beanlzs8.setEstatus("Operacion No Exitosa - " + msjErr);
				beanlzs8.setCodOperacion("");
				beanlzs8.setFechaOperacion("");
				beanlzs8.setHoraOperacion("");
				beanlzs8.setImporteTotal("");
				beanlzs8.setRegPatronal("");
				beanlzs8.setPeriodoPago("");
				beanlzs8.setFolioSua("");
				beanlzs8.setReferencia("Imprimir Error");
				beanlzs8.setOrigenArchivo("");
				beanlzs8.setFechaVencimiento("");
				beanlzs8.setImporteImss("");
				beanlzs8.setImporteRCV("");
				beanlzs8.setImporteVivienda("");
				beanlzs8.setImporteACV("");

			}
		}
		return beanlzs8;
	}

	/**
	 * funcion para formateo de fecha
	 * @param fechaGiro
	 * @return
	 */
	public String formateaFecha(String fechaGiro,int orden) {
		String fecha[]=fechaGiro.split("/");
		if(fecha.length==3){
			if(orden==1){
				return (fecha[2]+"-"+fecha[1]+"-"+fecha[0]);
			}else if(orden==2){
				return (fecha[0]+"-"+fecha[1]+"-"+fecha[2]);
			}else if(orden==3){
				return (fecha[2]+fecha[1]+fecha[0]);
			}else{
				return (fecha[0]+fecha[1]+fecha[2]);
			}
		}else{
			return fechaGiro;
		}
	}
}