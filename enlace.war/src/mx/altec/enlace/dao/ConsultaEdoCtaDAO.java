package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.beans.ConsEdoCtaDetalleBean;
import mx.altec.enlace.beans.ConsEdoCtaResultadoBean;
import mx.altec.enlace.beans.ConsultaEdoCtaBean;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

public class ConsultaEdoCtaDAO extends GenericDAO {
	
	/**
	 * Constante codigo para PDF
	 */
	public static final String FORMATO_PDF = "1";
	/**
	 * Constante para "TIPO_EDO_CTA"
	 */
	public static final String TIPO_EDO_CTA = "TIPO_EDO_CTA";
	/**
	 * Constante para "NUM_CONTR_TDC_FK"
	 */
	public static final String NUM_CONTR_TDC_FK = "NUM_CONTR_TDC_FK";
	/**
	 * SELECT_DETALLE
	 */
	private static final String SELECT_DETALLE = new StringBuilder().append("SELECT ID_EDO_CTA_DET, DET.ID_EDO_CTA_CTRL, DET.NUM_CUENTA, RELAC.NUM_CUENTA2, NUM_SEC, DET.ESTATUS, DESC_ERROR,") 
												 .append("PPLS, COD_CLTE, NVL(DET.TIPO_EDO_CTA,DECODE(LENGTH(TRIM(DET.NUM_CUENTA)),'16','003','001')) TIPO_EDO_CTA, ")
												 .append(" NVL(NUM_CONTR_TDC_FK,' ') NUM_CONTR_TDC_FK, NVL(RELAC.N_DESCRIPCION,'SIN DESCRIPCI&Oacute;N') N_DESCRIPCION ")
												 .append("FROM  EWEB_EDO_CTA_CTRL CTRL, EWEB_EDO_CTA_DET DET ")
												 .append("LEFT OUTER JOIN NUCL_RELAC_CTAS RELAC ON RELAC.NUM_CUENTA = DET.NUM_CUENTA ")
												 .append(" AND NUM_CUENTA2 = '%s' ")
												 .append("WHERE DET.ID_EDO_CTA_CTRL = CTRL.ID_EDO_CTA_CTRL ")
												 .append(" AND CTRL.ID_EDO_CTA_CTRL = %s ")
												 .append(" AND CTRL.NUM_CUENTA2 = '%s' ")
												 .append("ORDER BY ID_EDO_CTA_DET ASC").toString();
	
	
	/**
	 * SELECT_EDOCTA_PAGINADO
	 */
	private static final String SELECT_EDOCTA_PAGINADO = new StringBuilder(
								"SELECT REG.*, TOTAL FROM %n").append(
							    "(SELECT ROWNUM AS INDICE, EDOCTA.* %n").append(
							    "    FROM ( %n").append(
							    "         SELECT ID_EDO_CTA_CTRL, NUM_CUENTA2, ID_USUARIO,  %n").append( 
							    "		  		 TO_CHAR(FCH_ENVIO,'DD/MM/YYYY') FCH_ENVIO, ESTATUS %n").append( 
							    "         FROM EWEB_EDO_CTA_CTRL %n").append(
							    "		  %s %n").append(					
							    "         ) EDOCTA %n").append(
							    "    WHERE ROWNUM <= %s %n").append(
							    ") REG, (SELECT COUNT(*) AS TOTAL FROM EWEB_EDO_CTA_CTRL %s) TODOS %n").append( 
							    "WHERE INDICE > %s").toString();
	
	/**
	 * SELECT_DET_PAGINADO
	 */
	private static final String SELECT_DET_PAGINADO = new StringBuilder(
								"SELECT REG.*, TOTAL FROM %n").append(
							    "(SELECT ROWNUM AS INDICE, EDOCTA.* %n").append(
							    "    FROM ( SELECT ID_EDO_CTA_DET, DET.ID_EDO_CTA_CTRL, %n").append(
							    "				   DET.NUM_CUENTA, RELAC.NUM_CUENTA2, NUM_SEC, DET.ESTATUS, DESC_ERROR, %n").append(					    
							    "                  PPLS, COD_CLTE, NVL(DET.TIPO_EDO_CTA,DECODE(LENGTH(TRIM(DET.NUM_CUENTA)),'16','003','001')) TIPO_EDO_CTA, %n").append(
								"				   NVL(NUM_CONTR_TDC_FK,' ') NUM_CONTR_TDC_FK, NVL(RELAC.N_DESCRIPCION,'SIN DESCRIPCI&Oacute;N') N_DESCRIPCION %n").append(
							    "           FROM  EWEB_EDO_CTA_CTRL CTRL, EWEB_EDO_CTA_DET DET %n").append(
								"			LEFT OUTER JOIN NUCL_RELAC_CTAS RELAC ON RELAC.NUM_CUENTA = DET.NUM_CUENTA %n").append(
								"			AND NUM_CUENTA2 = '%s' %n").append(
							    "           WHERE DET.ID_EDO_CTA_CTRL = CTRL.ID_EDO_CTA_CTRL %n").append(
							    "				  AND CTRL.ID_EDO_CTA_CTRL = %s  %n").append(
								"				  AND CTRL.NUM_CUENTA2 = '%s'  %n").append(
							    "           ORDER BY ID_EDO_CTA_DET %n").append(
							    "        ) EDOCTA %n").append(
							    "    WHERE ROWNUM <= %s  %n").append(
							    ") REG, (SELECT COUNT(*) AS TOTAL %n" ).append(
							    "		 FROM EWEB_EDO_CTA_DET WHERE ID_EDO_CTA_CTRL = %s) TODOS %n").append(
							    "WHERE INDICE > %s  %n").toString();
	
	/**
	 * ESTATUS
	 */
	private static final String ESTATUS = "ESTATUS";
	
	/**
	 * PPLS
	 */
	private static final String PPLS = "PPLS";
	
	/**
	 * Realiza la consulta de edo cta
	 * @param consultaEdoCtaBean : consultaEdoCtaBean
	 * @return listaResultado : listaResultado
	 */
	public List<ConsEdoCtaResultadoBean> consultaEdoCta(ConsultaEdoCtaBean consultaEdoCtaBean){
		
		List<ConsEdoCtaResultadoBean> listaResultado = null;
		ResultSet result =null;
		PreparedStatement stmt = null;		
		Connection conn = null;
		String query;
		String where;

		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			
			if(conn != null){
			
				listaResultado  = new ArrayList<ConsEdoCtaResultadoBean>();
				where = armaCriterio(consultaEdoCtaBean);				
				query = String.format(SELECT_EDOCTA_PAGINADO, 
											 where, consultaEdoCtaBean.getRegFin(), 
											 where, consultaEdoCtaBean.getRegIni());
				
				EIGlobal.mensajePorTrace("QUERY PAGINADO A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
				
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
						
				 while (result.next ()) {
					 ConsEdoCtaResultadoBean resultado = new ConsEdoCtaResultadoBean();
					 EIGlobal.mensajePorTrace("Si existe informacion -----------> " , EIGlobal.NivelLog.DEBUG);
					 resultado.setIdEdoCta(result.getString("ID_EDO_CTA_CTRL"));
//                     EIGlobal.mensajePorTrace("ID_EDO_CTA_CTRL: " + resultado.getIdEdoCta(), EIGlobal.NivelLog.DEBUG);
					 resultado.setContrato(result.getString("NUM_CUENTA2"));
//                     EIGlobal.mensajePorTrace("NUM_CUENTA2: " + resultado.getContrato(), EIGlobal.NivelLog.DEBUG);
					 resultado.setUsuarioSolicitante(result.getString("ID_USUARIO")!=null?result.getString("ID_USUARIO"):"");
//					 EIGlobal.mensajePorTrace("ID_USUARIO: " + resultado.getUsuarioSolicitante(), EIGlobal.NivelLog.DEBUG);
					 // resultado.setNombreArchivo(result.getString("NOM_ARCH"));
					 resultado.setFechaEnvio(result.getString("FCH_ENVIO"));
//                     EIGlobal.mensajePorTrace("FCH_ENVIO: " + resultado.getFechaEnvio(), EIGlobal.NivelLog.DEBUG);
					 resultado.setEstatus("R".equals(result.getString(ESTATUS))?"Recibido":
						 				  "E".equals(result.getString(ESTATUS))?"Enviado":
						 				  "P".equals(result.getString(ESTATUS))?"Procesado":""); 
//                     EIGlobal.mensajePorTrace("ESTATUS: " + resultado.getEstatus(), EIGlobal.NivelLog.DEBUG);
					 resultado.setTotalRegistros(result.getInt("TOTAL"));
//                     EIGlobal.mensajePorTrace("TOTAL: " + resultado.getTotalRegistros(), EIGlobal.NivelLog.DEBUG);
					 
					 
					 listaResultado.add(resultado);
				 }
			}
	
		} catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return listaResultado;
	}
	
	
	/**
	 * Se encarga de armar los criterios de busqueda
	 * @param consultaEdoCtaBean : consultaEdoCtaBean
	 * @return String : where
	 */
	private String armaCriterio(ConsultaEdoCtaBean consultaEdoCtaBean){
		
		StringBuilder where = new StringBuilder();
		where.append("WHERE NUM_CUENTA2 = '").append(consultaEdoCtaBean.getContrato()).append("'");
		
		if(consultaEdoCtaBean.getFolio()!= null && !"".equals(consultaEdoCtaBean.getFolio())){
			where.append(" AND ID_EDO_CTA_CTRL = ").append(consultaEdoCtaBean.getFolio());
		}
		
		if(consultaEdoCtaBean.getEstatus() != null && !"".equals(consultaEdoCtaBean.getEstatus())){
			where.append(" AND ESTATUS = '").append(consultaEdoCtaBean.getEstatus()).append("'");
		}
		
		if(consultaEdoCtaBean.getUsuarioSolicitnate() != null && !"".equals(consultaEdoCtaBean.getUsuarioSolicitnate())){
			where.append(" AND ID_USUARIO = '").append(consultaEdoCtaBean.getUsuarioSolicitnate()).append("'");
		}
		
		if(consultaEdoCtaBean.getFechaInicio() != null && !"".equals(consultaEdoCtaBean.getFechaInicio())){
			where.append(" AND FCH_ENVIO >= TO_DATE('").append(consultaEdoCtaBean.getFechaInicio()).append("','DD/MM/YYYY')")
			.append(" AND FCH_ENVIO <= TO_DATE('").append(consultaEdoCtaBean.getFechaFin()).append("','DD/MM/YYYY')");
		}
		
		where.append(" ORDER BY 1 ASC");
		
		return where.toString();
		
	}
	
	
	/**
	 * Realiza la consulta del detalle de edo cta
	 * @param consultaEdoCtaBean : consultaEdoCtaBean
	 * @return detalle : detalle
	 */
	public List<ConsEdoCtaDetalleBean> consultaEdoCtaDetalle(ConsultaEdoCtaBean consultaEdoCtaBean){
		
		List<ConsEdoCtaDetalleBean> detalle = null;
		ResultSet result =null;
		PreparedStatement stmt = null;		
		Connection conn = null;
		String query;
		
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			
			if(conn != null){
			
				detalle  = new ArrayList<ConsEdoCtaDetalleBean>();
				query = String.format(SELECT_DET_PAGINADO, consultaEdoCtaBean.getContrato(), 
														   consultaEdoCtaBean.getFolio(),
														   consultaEdoCtaBean.getContrato(),
														   consultaEdoCtaBean.getRegFin(), 
														   consultaEdoCtaBean.getFolio(), 
														   consultaEdoCtaBean.getRegIni());
				EIGlobal.mensajePorTrace("QUERY PAGINADO A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();						
				detalle = llenarLista(result);
			}
	
		} catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}		
		return detalle;		
	}
	/**
	 * Metodo para llenar lista de bean con el resultado de la consulta
	 * creado por sonar
	 * @since 24/04/2015
	 * @author FSW-Indra
	 * @param result resultado de la consulta
	 * @return List<ConsEdoCtaDetalleBean> lista con la informacion
	 */
	private List<ConsEdoCtaDetalleBean> llenarLista(ResultSet result){
		List<ConsEdoCtaDetalleBean> detalle = new ArrayList<ConsEdoCtaDetalleBean>();
		try{
			while (result.next()) {
				ConsEdoCtaDetalleBean resultado = new ConsEdoCtaDetalleBean();
				String estatus=result.getString(ESTATUS).trim();
				 resultado.setIdEdoCtaDet(result.getString("ID_EDO_CTA_DET"));
				 resultado.setIdEdoCtaCtrl(result.getString("ID_EDO_CTA_CTRL"));
				 resultado.setCodClte(result.getString("COD_CLTE"));
		         resultado.setDescError( (result.getString("DESC_ERROR") != null) ? result.getString("DESC_ERROR") : " " );
				 resultado.setDescCta(result.getString("N_DESCRIPCION"));
				 resultado.setEstatus("V".equals(estatus)?"Validado":
					  				  "A".equals(estatus)?"Aceptado":
					  				  "X".equals(estatus)?"Rechazado":
					  				  "N".equals(estatus)?"Pendiente":"");
		        resultado.setNumCuenta(result.getString("NUM_CUENTA"));
		        resultado.setNumContrato(result.getString("NUM_CUENTA2"));
				 resultado.setNumSec(result.getString("NUM_SEC"));
				 resultado.setPpls("S".equals(result.getString(PPLS))?"Si":"N".equals(result.getString(PPLS))?"No":"No Aplica");
				 resultado.setTipoEdoCta(result.getString(TIPO_EDO_CTA).trim());
				 resultado.setContratoTDC(
						result.getString(NUM_CONTR_TDC_FK) != null ? 
								"0".equals(result.getString(NUM_CONTR_TDC_FK)) ? "" : result.getString(NUM_CONTR_TDC_FK) 
								: " ");
				 resultado.setTotalRegistros(result.getInt("TOTAL"));	
				 if("X".equals(estatus) && resultado.getDescError().trim().length()==0)
				 {
					 resultado.setDescError("Cambio No Aplicado, Intente Nuevamente");
				 }
				 detalle.add(resultado);
			}
		} catch(SQLException slqe) {
			EIGlobal.mensajePorTrace("ERROR" + slqe.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		return detalle;
	}
	
	/**
	 * Realiza la consulta del detalle de edo cta
	 * @param consultaEdoCtaBean : consultaEdoCtaBean
	 * @return detalle : detalle
	 */
	public List<ConsEdoCtaDetalleBean> consultaEdoCtaDetalleTotal(ConsultaEdoCtaBean consultaEdoCtaBean){
		
		List<ConsEdoCtaDetalleBean> detalle = null;
		ResultSet result =null;
		PreparedStatement stmt = null;		
		Connection conn = null;
		String query;
		
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			
			if(conn != null){
			
				detalle  = new ArrayList<ConsEdoCtaDetalleBean>();
				query = String.format(SELECT_DETALLE, consultaEdoCtaBean.getContrato(),
									  consultaEdoCtaBean.getFolio(), consultaEdoCtaBean.getContrato());
				
				EIGlobal.mensajePorTrace("QUERY TOTALIZADOR A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
						
				 while (result.next ()) {
					 ConsEdoCtaDetalleBean resultado = new ConsEdoCtaDetalleBean();
					 resultado.setIdEdoCtaDet(result.getString("ID_EDO_CTA_DET"));
					 resultado.setIdEdoCtaCtrl(result.getString("ID_EDO_CTA_CTRL"));
					 resultado.setCodClte(result.getString("COD_CLTE"));
					 resultado.setDescError(result.getString("DESC_ERROR"));
					 resultado.setDescCta(result.getString("N_DESCRIPCION"));
					 //resultado.setDispLine("S".equals(result.getString("DISP_LINE").trim())?"Si":"No");
					 resultado.setEstatus("V".equals(result.getString(ESTATUS).trim()) ? "Validado" :
		  				  "A".equals(result.getString(ESTATUS).trim()) ? "Aceptado" :
		  				  "X".equals(result.getString(ESTATUS).trim()) ? "Rechazado" :
                                                  "N".equals(result.getString(ESTATUS).trim())?"Pendiente":"");
					 resultado.setNumCuenta(result.getString("NUM_CUENTA"));
					 resultado.setNumContrato(result.getString("NUM_CUENTA2"));
					 resultado.setNumSec(result.getString("NUM_SEC"));
					 resultado.setPpls("S".equals(result.getString(PPLS))?"Si":"N".equals(result.getString(PPLS))?"No":"No Aplica");
					 //resultado.setTipoEdoCta("001".equals(result.getString(TIPO_EDO_CTA).trim())?"Estado de Cuenta Integral":"");
					 resultado.setTipoEdoCta(result.getString(TIPO_EDO_CTA).trim());
					 //resultado.setTipoFrmt("1".equals(result.getString("TIPO_FRMT").trim())?"PDF":"XML");
					 //PYME 2015 FSW Indra se agregan validacion para no pinta 0
					 resultado.setContratoTDC(
								result.getString(NUM_CONTR_TDC_FK) != null ? 
										"0".equals(result.getString(NUM_CONTR_TDC_FK)) ? "" : result.getString(NUM_CONTR_TDC_FK) 
										: "");
					 detalle.add(resultado);
				 }
			}
	
		} catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		
		return detalle;
		
	}
	/**
	 * Metodo para cosultar el estatus de la cuenta para edo de cta PDF
	 * @param cuenta La cuenta seleccionada
	 * @return ConfEdosCtaIndBean Bean de la informacion
	 */
	public ConfEdosCtaIndBean mostrarCuentasPDF(String cuenta) {
		
              ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
                ConfEdosCtaIndDAO confEdosCtaIndDAO = new ConfEdosCtaIndDAO();
		confEdosCtaIndBean = confEdosCtaIndDAO.ejecutaODD4(cuenta);
		EIGlobal.mensajePorTrace("confEdosCtaIndDAO.ejecutaODD4("+cuenta+").getCodRetorno() = ["+confEdosCtaIndBean.getCodRetorno()+"]",
				EIGlobal.NivelLog.DEBUG);
		if("ODA0002".equals(confEdosCtaIndBean.getCodRetorno())) {
			confEdosCtaIndBean.setDomCompleto(confEdosCtaIndBean.getCalle().trim().concat(" #").concat(confEdosCtaIndBean.getNoExterior().trim())
					.concat("-").concat(confEdosCtaIndBean.getNoInterior().trim()).concat(" COL. ").concat(confEdosCtaIndBean.getColonia().trim())
					.concat(" ").concat(confEdosCtaIndBean.getDelegMunic().trim()).concat(", ").concat(confEdosCtaIndBean.getEstado().trim()));
			
			confEdosCtaIndBean.setPeriodoDisp("");
			confEdosCtaIndBean.setFechaConfig(confEdosCtaIndDAO.consultaConfigPreviaSeqDom(confEdosCtaIndBean.getSecDomicilio(), 
					confEdosCtaIndBean.getCodCliente()));
		}
		return confEdosCtaIndBean;
            
		
	}

}
