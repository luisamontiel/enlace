/** 
*   Isban Mexico
*   Clase: DAOTrxPE61.java
*   Descripcion: Objeto de acceso a la transaccion PE61 de consulta de documentos.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.dao;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Ps7;

/**
 * Objeto de acceso a la transaccion PE61 de consulta de documentos.
 * @author FSW
 */
public class DAOTrxPE61 extends GenericDAO{
    
    /** Numero de campo de respuesta del NUMERO_DOCUMENTO. */
    public static final int FMT_RESP_CAMPO_NUMDOCTO = 3;

    /** Arreglo de posiciones iniciales de cada campo. */
    private static final int [] FRM_POSICIONES = {0,4,12,14,34,36,51,61,62,63,66,76,83,95,103,203,303};
    
    /** Longitudes de los campos del formato de salida de la PE61. */
    private static final int [] FRM_LONGITUDES = {4,8,2,20,2,15,10,1,1,3,10,7,12,8,100,100,26};
    
    /**Encabezado de la trama de env&iacute;o*/
    private static final String HEADERLZMAD01 = "PE6100641123451O00N2";

    /** Nombre del formato de respuesta. */
    private static final String FORMATO_RESPUESTA = "PEM2340";
    
    /** Indicador de tipo de documento de RFC*/
    private static final String INDICADOR_TIPO_DOC = "R";
    
    /** Guarda la trama de respuesta de la transaccion.*/
    private transient String respuesta;

    /**
     * Ejecuta la transaccion.
     * @param cliente numero de cliente
     * @param numDoc numero de documento
     * @param secuenciaDoc secuencia del documento
     * @return <code>true</code> si se ejecuto exitosamente la transaccion
     */ 
    public boolean ejecutaConsulta(String cliente, String numDoc, 
    		String secuenciaDoc) {
        EIGlobal.mensajePorTrace("DAOTrxPE61 - armaTrama(): [" + cliente + "] - ["
                + INDICADOR_TIPO_DOC +"]", EIGlobal.NivelLog.INFO);
        StringBuilder trama = new StringBuilder(70);
        trama.append(HEADERLZMAD01)
                .append(Ps7.rellenar(cliente, 8))
                .append(Ps7.rellenar(INDICADOR_TIPO_DOC, 2))
                .append(Ps7.rellenar(numDoc, 20))
                .append(Ps7.rellenar(secuenciaDoc, 2));

        EIGlobal.mensajePorTrace("DAOTrxPE61 - armaTrama(): [" + trama.toString() +"]", EIGlobal.NivelLog.DEBUG);
        respuesta = invocarTransaccion(trama.toString());
        EIGlobal.mensajePorTrace("DAOTrxPE61 - armaTrama() - RESULTADO TRAMA : [" + respuesta+"]",EIGlobal.NivelLog.DEBUG);
        return respuesta != null && respuesta.contains(FORMATO_RESPUESTA);
    }
    
    /**
     * Obtiene el campo indicado de la trama de salida.
     * @param numCampo numero de campo
     * @return el valor del campo indicado o <code>null</code> si no coincide con los campos
     */
    public String obtenCampo(int numCampo) {
        String campo = null;
        if (this.respuesta != null) {
            int posResp = respuesta.indexOf(FORMATO_RESPUESTA);
            if (posResp >= 0 && numCampo >= 0
                    && numCampo < FRM_LONGITUDES.length
                    && respuesta.length() > (FRM_POSICIONES[numCampo] + FRM_LONGITUDES[numCampo])) {
            	posResp += 9 + FRM_POSICIONES[numCampo];
                campo = this.respuesta.substring(posResp, posResp + FRM_LONGITUDES[numCampo]);
            }
        }
        return campo;
    }
}
