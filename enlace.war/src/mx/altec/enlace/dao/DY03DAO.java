/**
 * Isban Mexico
 *   Clase: DY03DAO.java
 *   Descripci�n: 
 *
 *   Control de Cambios:
 *   1.0 12/04/2013 Stefanini - Creaci�n
 */
package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import java.util.ArrayList;

import javax.jms.JMSException;
import javax.naming.NamingException;

import mx.altec.enlace.beans.DY03Bean;
import mx.altec.enlace.beans.DY03DetallePeriodoBean;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * The Class DY03DAO.
 */
public class DY03DAO {
	
	/** The Constant OPERACION_EXITOSA. */
	private static final String OPERACION_EXITOSA = "DYA0004";
	
	/**
	 * Ejecutar d y03.
	 *
	 * @param bean the bean
	 * @return the d y03 bean
	 * @throws BusinessException the business exception
	 */
	public DY03Bean ejecutarDY03(DY03Bean bean) throws BusinessException{
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY03() Inicio", EIGlobal.NivelLog.DEBUG);
		try {
			MQQueueSession ejecutor = 
					new MQQueueSession(
							NP_JNDI_CONECTION_FACTORY, 
							NP_JNDI_QUEUE_RESPONSE, 
							NP_JNDI_QUEUE_REQUEST);
			
			EIGlobal.mensajePorTrace(
					String.format("%s:::consultaDY03() Trama de envio::%s", 
							this.getClass().getName(), 
							bean.generarMensajeEntrada()), 
					EIGlobal.NivelLog.DEBUG);
			
			String respuesta = 
					ejecutor.enviaRecibeMensaje(bean.generarMensajeEntrada());
			
			//Desarrollo
			//String respuesta = "@112345@AVDYA0004 CONSULTA EXISTOSA NO HAY MAS DATOS�@DCDYM0S30 P 6000060034412013030014785245245036000060034420130347960000600344-201303.PDF 1.5 MB �";
			
			EIGlobal.mensajePorTrace(
					String.format("%s:::consultaDY03() Trama de respuesta::[%s]", 
							this.getClass().getName(), respuesta), 
					EIGlobal.NivelLog.DEBUG);
			
			String[] respuestaSeparada = respuesta.split("@");
			String codigoOperacion = respuestaSeparada[2].substring(0, 9);
			String mensajeRespuesta = respuestaSeparada[2].substring(10, respuestaSeparada[2].length() - 2);
			
			if(!(codigoOperacion != null && codigoOperacion.contains(OPERACION_EXITOSA))){
				throw new BusinessException(codigoOperacion + ": " + mensajeRespuesta);
			}
			
			bean.setCodigoRespuesta(codigoOperacion);
			bean.setDescripcionRespuesta(mensajeRespuesta);
			bean.setDetallePeriodo(new ArrayList<DY03DetallePeriodoBean>());

			EIGlobal.mensajePorTrace("Tama�o respuesta DY03: " + respuestaSeparada.length, EIGlobal.NivelLog.DEBUG);
			for(int i=3; i<respuestaSeparada.length; i++){
				String detalle = respuestaSeparada[i];
				EIGlobal.mensajePorTrace("DY03 [" +  i + "]:" + detalle, EIGlobal.NivelLog.DEBUG);
				bean.procesarDetalleRespuesta(detalle.substring(11));
			}
		} catch (BusinessException e) {
			EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY03()-BusinessException: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw (BusinessException)e;
		} catch (MQQueueSessionException e) {
			EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY03()-MQQueueSessionException: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new BusinessException(e.getMessage(), e);
		} catch (ValidationException e) {
			EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY03()-ValidationException: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new BusinessException(e.getMessage(), e);
		} catch (NamingException e) {
			EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY03()-NamingException: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new BusinessException(e.getMessage(), e);
		} catch (JMSException e) {
			EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY03()-JMSException: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new BusinessException(e.getMessage(), e);
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY03() Fin", EIGlobal.NivelLog.DEBUG);
		return bean;
	}
}
