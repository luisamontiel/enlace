package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmLyMGL21;

import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class AdmLyMGpoOperacionDAO extends AdmonUsuariosMQDAO {
	
	//TRANS: GL21
	private AdmLyMGL21 ejecutaGL21Base(String cveGrupo) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(cveGrupo, 2));					//CVE-GRUPO
		return consulta(AdmLyMGL21.HEADER, sb.toString(), AdmLyMGL21.getFactoryInstance());
	}
	
	public AdmLyMGL21 consultaGpoOperacion(String cveGrupo) {
		logInfo("GL21 - Consulta de Grupos de Operacion");
		return ejecutaGL21Base(cveGrupo);			
	}

}
