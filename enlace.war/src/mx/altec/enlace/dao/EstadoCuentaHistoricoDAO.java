package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import mx.altec.enlace.beans.DY02Bean;
import mx.altec.enlace.beans.EstadoCuentaHistoricoBean;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class EstadoCuentaHistoricoDAO extends GenericDAO {
	/** Constante SQL para obtener la secuencia del folio **/
	private static final String SQL_OBTENER_FOLIO_SECUENCIA = " SELECT SEQ_EDOCTA_HIST.nextval siguienteValor from dual ";
	/** Constante SQL para insertar en la tabla de historiales **/
	private static final String SQL_INSERTAR_EN_HISTORIAL = 
			new StringBuilder()
				.append(" Insert Into Eweb_Edo_Cta_Hist(Id_Edo_Cta_Hist,Num_Cuenta,Periodo,Num_Sec,Fch_Sol,Num_Cuenta2,Id_Usuario,Estatus,Tipo_Edo_Cta) ")
				.append(" Values (?,?,?,?, TRUNC(Sysdate),?,?,'P',?) ").toString();
	/** Constante Codigo de operacion exitosa **/
	private static final String SQL_CODE_OK = "100";
	/** Constante Descripcion del codigo OK **/
	private static final String SQL_CODE_DESC = "REGISTRO INSERTADO";
	/** Constante Codigo de Error **/
	private static final String SQL_CODE_ERROR = "301";
	/** Constante Descripcion del codigo de Error**/
	private static final String SQL_CODE_ERRORD= "NO SE PUDO INSERTAR EN BD - ERROR GENERICO";
	/** Constante del Folio de Error **/
	private static final int FOLIO_ERROR = -1;
	/** Constante de error en registro de historico en DB **/
	private static final String SECH201 = "SECH201";
	/** Constante de error al obtener la secuencia de DB **/
	private static final String SECH003 = "SECH003";
	/** Constante SQL para validar si existe solicitud previa en la tabla eweb_edo_cta_hist. **/
	private static final String SQL_CONSULTA_HIST = new StringBuilder()
			.append("SELECT COUNT(1) AS SOLICITUD FROM EWEB_EDO_CTA_HIST ")
			.append("WHERE NUM_CUENTA = '%s' ")
			.append("AND PERIODO = '%s' ")
			.append("AND ESTATUS = 'P'").toString();	
	/** Atributo static y final para guardar cadena constante (log). */
	private static final String PREFIJO_SOL_PREVIA = "EstadoCuentaHistoricoDAO - existeSolicitudPrevia";
	/** Log Tag **/
	private static final String LOG_TAG = "[PDFXML] ::: EstadoCuentaHistoricoDAO ::: ";
	
	
	/**
	 * Registra las peticiones de estado de cuenta historico
	 * @param estadosCuenta : Los estados de cuenta historicos que se desean obtener
	 * @return List : Lista de estados de cuenta historicos cargados.
	 * @throws BusinessException : En caso de que exista un error con la operacion
	 */
	public List<EstadoCuentaHistoricoBean> registrarPeticion(List<EstadoCuentaHistoricoBean> estadosCuenta) 
			throws BusinessException {
		
		EIGlobal.mensajePorTrace(String.format("%sInicia --> metodo registrarPeticion() ",LOG_TAG), NivelLog.INFO);		
		boolean dyok = false;
		final DY02DAO dy02DAO = new DY02DAO();
		List <EstadoCuentaHistoricoBean> estadosCuentaList = new ArrayList<EstadoCuentaHistoricoBean>();	
		for(EstadoCuentaHistoricoBean estadoCuenta : estadosCuenta){
			final DY02Bean dy02Bean = new DY02Bean();
			//Opcion = A
			dy02Bean.setCodigoCliente(estadoCuenta.getCliente());
			dy02Bean.setCuenta(estadoCuenta.getCuentaEstadoCuenta());
			dy02Bean.setFormato(estadoCuenta.getFormato());
			dy02Bean.setPeriodo(estadoCuenta.getPeriodo());
			dy02Bean.setTipoEDC(estadoCuenta.getTipoEDC());
			//Canal = 05
			dy02Bean.setFolioPeticion(String.valueOf(estadoCuenta.getNumeroSecuenciaHistorica()));
			dy02Bean.setFolioOndemand(estadoCuenta.getFolioOndemand());
			dy02Bean.setFolioUUID(StringUtils.EMPTY);
			//Ejecuta txn DY02.
			try{
				dy02DAO.ejecutarDY02(dy02Bean);
				dyok=true;
			}catch (BusinessException e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
				dyok=false;
			}finally {
				//Almacena resultado de la llamada a DY02.
				estadoCuenta.setCodigoRespuestaDY02(dy02Bean.getCodigoRespuesta());
				estadoCuenta.setDescripcionRespuestaDY02(dy02Bean.getDescripcionRespuesta());
				EIGlobal.mensajePorTrace(String.format("Almacena salida de DY02 [%s] [%s]",
										 estadoCuenta.getCodigoRespuestaDY02(),
										 estadoCuenta.getDescripcionRespuestaDY02()),EIGlobal.NivelLog.DEBUG);
			}
			//Almacena folio guardado en base de datos.
			if (dyok && "DYA0001".equals(estadoCuenta.getCodigoRespuestaDY02())) {
				try {
					estadoCuenta.setFolioPeticion(registrarEnHistorico(estadoCuenta));	
					estadoCuenta.setCodigoRespuestaBD(SQL_CODE_OK);
					estadoCuenta.setDescripcionRespuestaBD(SQL_CODE_DESC);
					EIGlobal.mensajePorTrace(String.format("Inserta en BD folio[%s] Salida [%s] [%s]",
							 estadoCuenta.getFolioPeticion(),
							 estadoCuenta.getCodigoRespuestaBD(),
							 estadoCuenta.getDescripcionRespuestaBD()),	EIGlobal.NivelLog.DEBUG);	
				}catch (BusinessException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace(String.format("Error en BD folio[%s] Salida [%s] [%s].",
							estadoCuenta.getFolioPeticion(),
							estadoCuenta.getCodigoRespuestaBD(),
							estadoCuenta.getDescripcionRespuestaBD()), EIGlobal.NivelLog.INFO);
				}
			}else {
				estadoCuenta.setFolioPeticion(FOLIO_ERROR);
				estadoCuenta.setCodigoRespuestaBD(SQL_CODE_ERROR);
				estadoCuenta.setDescripcionRespuestaBD(SQL_CODE_ERRORD);
			}			
			//Agregamos a la lista el bean con toda la informacion. 
			estadosCuentaList.add(estadoCuenta); 
		}	
		return estadosCuentaList;
	}
	
	/**
	 * Realiza la operacion de insercion en la tabla de historicos
	 * @param estadoCuenta : Objeto con la informacion del estado de cuenta
	 * @throws BusinessException : En caso de un error con la operacion
	 * @return : int 
	 */
	private int registrarEnHistorico(EstadoCuentaHistoricoBean estadoCuenta) throws BusinessException {
		Connection con = null;
		PreparedStatement sentencia = null;
		boolean ocurrioUnError = false;
		int folio = 0;
		try {
			con = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			
			EIGlobal.mensajePorTrace(
					String.format("EstadoCuentaHistoricoDAO ---> registrarEnHistorico ::: Numero Secuencia --> [%s]", 
							estadoCuenta.getNumeroSecuenciaHistorica()), NivelLog.INFO);
			EIGlobal.mensajePorTrace(
					String.format("EstadoCuentaHistoricoDAO ---> registrarEnHistorico ::: Cuenta Estado Cuenta --> [%s]", 
							estadoCuenta.getCuentaEstadoCuenta()), NivelLog.INFO);
			EIGlobal.mensajePorTrace(
					String.format("EstadoCuentaHistoricoDAO ---> registrarEnHistorico ::: Periodo --> [%s]", 
							estadoCuenta.getPeriodo()), NivelLog.INFO);
			EIGlobal.mensajePorTrace(
					String.format("EstadoCuentaHistoricoDAO ---> registrarEnHistorico ::: Formato --> [%s]", 
							estadoCuenta.getFormato().obtenerIdFormato()), NivelLog.INFO);
			EIGlobal.mensajePorTrace(
					String.format("EstadoCuentaHistoricoDAO ---> registrarEnHistorico ::: Numero Secuencia Domicilio --> [%s]", 
							estadoCuenta.getNumeroSecuenciaDomicilio()), NivelLog.INFO);
			EIGlobal.mensajePorTrace(
					String.format("EstadoCuentaHistoricoDAO ---> registrarEnHistorico ::: Cuenta Enlace --> [%s]", 
							estadoCuenta.getCuentaEnlace()), NivelLog.INFO);
			EIGlobal.mensajePorTrace(
					String.format("EstadoCuentaHistoricoDAO ---> registrarEnHistorico ::: Usuario Enlace --> [%s]", 
							estadoCuenta.getUsuarioEnlace()), NivelLog.INFO);
			/*
			EIGlobal.mensajePorTrace(
				String.format(SQL_INSERTAR_EN_HISTORIAL.replaceAll("?","%s"),estadoCuenta.getNumeroSecuenciaHistorica(),
				estadoCuenta.getCuentaEstadoCuenta(),estadoCuenta.getPeriodo(),Integer.valueOf( estadoCuenta.getNumeroSecuenciaDomicilio() ),
				estadoCuenta.getCuentaEnlace(),	estadoCuenta.getUsuarioEnlace(),estadoCuenta.getTipoEDC()),NivelLog.DEBUG);
			*/
			sentencia = con.prepareStatement(SQL_INSERTAR_EN_HISTORIAL);
			sentencia.setInt(1, estadoCuenta.getNumeroSecuenciaHistorica());
			sentencia.setString(2, estadoCuenta.getCuentaEstadoCuenta());
			sentencia.setString(3, estadoCuenta.getPeriodo());
			sentencia.setInt(4, Integer.valueOf( estadoCuenta.getNumeroSecuenciaDomicilio() ) );
			sentencia.setString(5, estadoCuenta.getCuentaEnlace());
			sentencia.setString(6, estadoCuenta.getUsuarioEnlace());
			sentencia.setString(7, estadoCuenta.getTipoEDC());

			EIGlobal.mensajePorTrace("EstadoCuentaHistoricoDAO ---> altaEmpleadoControl " + SQL_INSERTAR_EN_HISTORIAL,NivelLog.INFO);
			sentencia.executeQuery();
			con.commit();
			EIGlobal.mensajePorTrace("EstadoCuentaHistoricoDAO ---> altaEmpleadoControl --> Termina Insert " + con,NivelLog.INFO);
			folio=estadoCuenta.getNumeroSecuenciaHistorica();
			
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
			folio=FOLIO_ERROR;
			estadoCuenta.setCodigoRespuestaBD((String.valueOf(e.getErrorCode()).length() > 0 ) ? String.valueOf(e.getErrorCode()) : "SECH003");
			estadoCuenta.setDescripcionRespuestaBD(e.getLocalizedMessage());
			ocurrioUnError=true;
		} finally {
			try {
				sentencia.close();
				con.close();				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%s - Error al cerrar objeto : [%s]",LOG_TAG,e.toString()),NivelLog.DEBUG);
			}
			//limpiarRecursosConexion(con, sentencia, null);
		}		
		if(ocurrioUnError){
			folio=FOLIO_ERROR;
			throw new BusinessException(SECH003);
		}		
		return folio;
	}
	
	/**
	 * Obtiene la secuencia para insertar en tabla detalle.
	 *
	 * @return int : La secuencia obtenida
	 * @throws BusinessException : En caso de un error con la operacion
	 */
	public int obtenerSecuencia() throws BusinessException {
		EIGlobal.mensajePorTrace("EstadoCuentaHistoricoDAO ---> Se ejecuta metodo obtenerSecuencia() ", NivelLog.INFO);
		
		Connection con = null;
		PreparedStatement consulta = null;
		ResultSet resultado = null;
		int valorSecuencia = 0;
		try {
			con = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			consulta = con.prepareStatement(SQL_OBTENER_FOLIO_SECUENCIA);

			EIGlobal.mensajePorTrace("EstadoCuentaHistoricoDAO ---> obtenSecuencia " + SQL_OBTENER_FOLIO_SECUENCIA, NivelLog.INFO);
			resultado = consulta.executeQuery();
			con.commit();
			
			if (resultado.next()) {
				EIGlobal.mensajePorTrace(
						"EstadoCuentaHistoricoDAO ---> Se obtiene el siguiente valor de la secuencia ", 
						NivelLog.INFO);
				
				valorSecuencia = resultado.getInt("siguienteValor");
				EIGlobal.mensajePorTrace(
						String.format("EstadoCuentaHistoricoDAO ---> Valor obtenido [%s]", valorSecuencia), 
						NivelLog.INFO);
			} else {
				EIGlobal.mensajePorTrace(
						"EstadoCuentaHistoricoDAO ---> No fue posible obtener un valor de la secuencia ", 
						NivelLog.INFO);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("Ocurrio un error en : " + e, NivelLog.INFO);
			throw new BusinessException(SECH201,e);
		} finally {
			try {
				resultado.close();
				consulta.close();
				con.close();				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%s - Error al cerrar objeto : [%s]",LOG_TAG,e.toString()),NivelLog.DEBUG);
			}
			//limpiarRecursosConexion(con, consulta, resultado);
		}		
		EIGlobal.mensajePorTrace(String.format("%s Termina metodo obtenerSecuencia()",LOG_TAG), NivelLog.INFO);		
		if(valorSecuencia == 0){
			throw new BusinessException(SECH201);
		}		
		return valorSecuencia;
	}

	/**
	 * Verifica si dentro de la tabla eweb_edo_cta_hist ya existe el registro de
	 * la solicitud de estado de cuenta pdf con base en los parametros recibidos.
	 * 
	 * @param cuenta : string con el numero de cuenta solicitado.
	 * @param periodo : string con el periodo solicitado (yyyymm).
	 * @return boolean : indica si existe (true) o no existe (false) el 
	 *         periodo solicitado en la tabla eweb_edo_cta_hist.
	 */
	public boolean existeSolicitudPrevia(String cuenta, String periodo) {
		EIGlobal.mensajePorTrace(String.format("%sInicio de metodo existeSolicitudPrevia",LOG_TAG), EIGlobal.NivelLog.INFO);
		boolean existeSolicitudPrevia = false;
		String count = null;
		int solicitud = 0;
		ResultSet result =null;
		PreparedStatement stmt = null;		
		Connection conn = null;
		String query;
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			if (conn != null) {
				query = String.format(SQL_CONSULTA_HIST, cuenta, periodo);
				EIGlobal.mensajePorTrace(PREFIJO_SOL_PREVIA+ " :: Query para consulta de solicitud historica: ["+ query + "]", 
						EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				
				if(result.next()) {
					count = result.getString("SOLICITUD");
					EIGlobal.mensajePorTrace(PREFIJO_SOL_PREVIA+ " :: Resultado de query: [" + count + "] ",
							EIGlobal.NivelLog.DEBUG);
					solicitud = Integer.parseInt(count);
					if (solicitud > 0) {
						existeSolicitudPrevia = true;
						EIGlobal.mensajePorTrace(PREFIJO_SOL_PREVIA+ " :: existeSolicitudPrevia: ["+ existeSolicitudPrevia + "] ",
								EIGlobal.NivelLog.DEBUG);
					} else {
						existeSolicitudPrevia = false;
						EIGlobal.mensajePorTrace(PREFIJO_SOL_PREVIA+ " :: existeSolicitudPrevia: ["+ existeSolicitudPrevia + "] ",
								EIGlobal.NivelLog.DEBUG);
					}
				}
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(PREFIJO_SOL_PREVIA+ " :: Error al obtener los datos "+ new Formateador().formatea(e),
					EIGlobal.NivelLog.ERROR);			
		} finally {
			try {
				result.close();
				stmt.close();
				conn.close();				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%sError al cerrar objeto [%s]",LOG_TAG,e.toString()),NivelLog.DEBUG);
			}			
		}		
		EIGlobal.mensajePorTrace(String.format("%sFin de metodo existeSolicitudPrevia",LOG_TAG), EIGlobal.NivelLog.INFO);
		return existeSolicitudPrevia;
	}
}