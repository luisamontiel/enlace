/** 
*   Isban Mexico
*   Clase: TrxOD52DAO.java
*   Descripcion: Objeto de datos  que comprueba la existencia de informacion 
*   para el documento electronico relacionado con el numero de cliente en la 
*   transaccion OD52.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.dao;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;

public class TrxOD52DAO  extends GenericDAO {

    /**Encabezado de la trama de env&iacute;o*/
    public static final String HEADEROD52         = "OD5201391123451O00N2";
    /**Caracter de inicio de trama para env&iacute;o*/
    public static final String INICIO0D54         = " ";

    
    /**
     * Verifica si existe la informacion del documento electronico relacionado con el numero de cliente
     * @param trama 	   trama con datos del documento a consultar
     * @return             cadena con datos de la consulta
     */
    public String ejecutaConsulta(String trama){

        StringBuffer cadena = new StringBuffer().append(HEADEROD52).append(trama);

        String respuesta = FielConstants.EMPTY_STRING;

        EIGlobal.mensajePorTrace("TrxOD52DAO - ejecutaConsulta(): [" +cadena.toString()+"]", EIGlobal.NivelLog.INFO);
        respuesta = invocarTransaccion(cadena.toString());
        EIGlobal.mensajePorTrace("TrxOD52DAO - ejecutaConsulta() - RESULTADO TRAMA : [" + respuesta+"]",EIGlobal.NivelLog.INFO);
        return respuesta;
    }
    
}
