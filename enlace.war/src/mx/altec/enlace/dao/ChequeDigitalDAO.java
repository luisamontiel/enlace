package mx.altec.enlace.dao;

import java.sql.*;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class ChequeDigitalDAO 
{
		private PreparedStatement statement =null;
		private Connection conn=null;
		String query = "";


		private ResultSet Consulta()throws Exception{
			ResultSet rs = null;
			rs = statement.executeQuery();
			return rs;
		}

		private int Inserta()throws Exception{
			int resultado = 0;
			resultado = statement.executeUpdate();
			return resultado;
		}

		/******************************************************************
		 *** Metodo que realiza la consulta del Usuario en Cheque Digital
		 *** TMR_CHEQUE_DIGITAL
		 * @throws Exception 
		 *****************************************************************/
		
		public boolean tieneChequeDigital(String cliente) throws Exception{
			EIGlobal.mensajePorTrace ("ChequeDigitalDAO - tieneChequeDigital() - " +
					"Cliente: [" + cliente + "]", EIGlobal.NivelLog.INFO);
			ResultSet rs = null;
			boolean res = false;
			try {
	    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE);
	    		
				query = "SELECT NUMEROCLIENTE FROM TMR_CHEQUE_DIGITAL " +
				"WHERE NUMEROCLIENTE = '" + cliente + "' AND CANAL = 'E'";
				statement = conn.prepareStatement (query.toString());
				EIGlobal.mensajePorTrace ("ChequeDigitalDAO - tieneChequeDigital() - " +
						"query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
				rs = Consulta();
				if(rs.next()) {
					res = true;
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ChequeDigitalDAO - tieneChequeDigital() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ChequeDigitalDAO - tieneChequeDigital() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace("ChequeDigitalDAO - tieneChequeDigital() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return res;
		}
		
		
}