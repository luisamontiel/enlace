package mx.altec.enlace.dao;

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.sql.*;
import java.io.Serializable;
import javax.sql.*;
import javax.naming.*;


import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class CalendarNomina implements Serializable
{
   public Vector diasNoHabiles;
   Vector diasNoHabilesJS = new Vector();

	//public static BaseServlet BAL;

    public CalendarNomina(BaseServlet TempBAL)
	{
        //BAL=TempBAL;
	}

    public CalendarNomina()
	{

	}


    public Calendar evaluaDiaHabil(Calendar fecha){
      int      indice;
      int      diaDeLaSemana;
      boolean  encontrado;

             if(diasNoHabiles!=null)
             {
               fecha.add( fecha.DAY_OF_MONTH, -1);
               do{
                   fecha.add( fecha.DAY_OF_MONTH,  1);

                   diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
                   encontrado = false;
                   for(indice=0; indice<diasNoHabiles.size(); indice++)
                   {
                      String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
                      String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
                      String anio = Integer.toString( fecha.get( fecha.YEAR) );
                      String strFecha = dia.trim() + "/" + mes.trim() + "/" + anio.trim();
                      if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
                      {
					  EIGlobal.mensajePorTrace("***CalendarioNomina.class &Dia es inhabil&", EIGlobal.NivelLog.INFO);
                         encontrado = true;
                         break;
                      }
                   }
               }while( (diaDeLaSemana == Calendar.SUNDAY)   ||
                       (diaDeLaSemana == Calendar.SATURDAY) ||
                       encontrado );
             }

         return fecha;
   }

     public String ObtenFecha(boolean tipo){
	      StringBuffer strFecha = new StringBuffer("");
		  Date Hoy = new Date();
	      GregorianCalendar Cal = new GregorianCalendar();
		  Cal.setTime(Hoy);
	     if(tipo)
		  {
			if(Cal.get(Calendar.DATE) <= 9)
			 {
			   strFecha.append("0" );
			   strFecha.append(Cal.get(Calendar.DATE) );
			   strFecha.append("/");
			 }
			else
			 {
				strFecha.append(Cal.get(Calendar.DATE) );
				strFecha.append("/");
			 }
			if(Cal.get(Calendar.MONTH)+1 <= 9)
			 {
				strFecha.append("0" );
				strFecha.append((Cal.get(Calendar.MONTH)+1) );
				strFecha.append("/");
			 }
			else
			 {
			   strFecha.append((Cal.get(Calendar.MONTH)+1) );
			   strFecha.append("/");
			 }
			strFecha.append(Cal.get(Calendar.YEAR));
          }
        else
		 {
         String[] dias = { "Domingo",
                           "Lunes",
                           "Martes",
                           "Miercoles",
                           "Jueves",
                           "Viernes",
                           "Sabado"};
         String[] meses = {"Enero",
                           "Febrero",
                           "Marzo",
                           "Abril",
                           "Mayo",
                           "Junio",
                           "Julio",
                           "Agosto",
                           "Septiembre",
                           "Octubre",
                           "Noviembre",
                           "Diciembre"};

         strFecha.append(dias[Cal.get(Calendar.DAY_OF_WEEK)-1] );
		 strFecha.append(", " );
		 strFecha.append(Cal.get(Calendar.DATE) );
		 strFecha.append(" de " );
		 strFecha.append(meses[Cal.get(Calendar.MONTH)] );
		 strFecha.append(" de " );
		 strFecha.append(Cal.get(Calendar.YEAR));
       }
      return strFecha.toString();
    }


     public Connection createiASConn ( String dbName )
     throws SQLException {
         Connection conn = null;
         try {
             InitialContext ic = new InitialContext ();
             DataSource ds = ( DataSource ) ic.lookup ( dbName );
             conn = ds.getConnection ();
         } catch ( NamingException ne ) {
		 					  EIGlobal.mensajePorTrace("***Unable create connection to: " + dbName + "; " + ne.toString () , EIGlobal.NivelLog.INFO);
         }
         return conn;
     }




    public Vector CargarDias()
	 {
       Connection Conexion=null;
       PreparedStatement psDias=null;
       ResultSet rsDias=null;
	   Vector diasInhabiles = null;
       String sqlDias="";
       String SuaDatabase = "IEnlace.BASE_DATOS";
	   boolean    estado;

       diasInhabiles = new Vector();
       sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha>=sysdate-(365*3)";

	   try
		{
			InitialContext ic = new InitialContext ();
			DataSource ds = ( DataSource ) ic.lookup ( Global.DATASOURCE_ORACLE );
			Conexion = ds.getConnection ();

			EIGlobal.mensajePorTrace("Calendar Nomina La conexion se realizo: " + Conexion, EIGlobal.NivelLog.INFO);

			psDias = Conexion.prepareStatement( sqlDias );
			rsDias = psDias.executeQuery();

			String dia = "";
			String mes = "";
			String anio = "";
			StringBuffer fechaJS ;
			StringBuffer fecha ;

			while( rsDias.next() )
			 {
				dia = rsDias.getString(1);
				mes = rsDias.getString(2);
				anio = rsDias.getString(3);

				fechaJS=new StringBuffer("");
				fechaJS.append( dia.trim() );
				fechaJS.append("/"  );
				fechaJS.append(mes.trim() );
				fechaJS.append("/" );
				fechaJS.append(anio.trim() );

				diasNoHabilesJS.addElement(fechaJS.toString());
				dia  =  Integer.toString( Integer.parseInt(dia) );
				mes  =  Integer.toString( Integer.parseInt(mes) );
				anio =  Integer.toString( Integer.parseInt(anio) );

				fecha=new StringBuffer("");
				fecha.append(dia.trim() );
				fecha.append("/"  );
				fecha.append(mes.trim() );
				fecha.append("/" );
				fecha.append(anio.trim());

				diasInhabiles.addElement(fecha.toString());

				EIGlobal.mensajePorTrace("Calendar Nomina generando fechas: " + fecha + " " + fechaJS, EIGlobal.NivelLog.INFO);
			 }

		}catch ( NamingException ne )
		  {EIGlobal.mensajePorTrace("***Unable create connection to: " +  Global.DATASOURCE_ORACLE + "; " + ne.toString () , EIGlobal.NivelLog.INFO);}
		catch ( SQLException sqle )
		 {sqle.printStackTrace();}
	    finally
		 {
			try
			  {
				 rsDias.close();
				 Conexion.close();
			  }catch(Exception e) {}
		 }
        return(diasInhabiles);
    }


public String armaArregloFechas(){
      StringBuffer VarFechaHoy = new StringBuffer("");
      VarFechaHoy.append( "\n  dia   = new Array("  );
	  VarFechaHoy.append(fechaConsulta("DIA")   );
	  VarFechaHoy.append(","   );
	  VarFechaHoy.append(fechaConsulta("DIA")   );
	  VarFechaHoy.append(");");

      VarFechaHoy.append("\n  mes   = new Array("  );
	  VarFechaHoy.append(fechaConsulta("MES")   );
	  VarFechaHoy.append(","   );
	  VarFechaHoy.append(fechaConsulta("MES")   );
	  VarFechaHoy.append(");");

      VarFechaHoy.append("\n  anio  = new Array("  );
	  VarFechaHoy.append(fechaConsulta("ANIO")  );
	  VarFechaHoy.append(","   );
	  VarFechaHoy.append(fechaConsulta("ANIO")  );
	  VarFechaHoy.append(");");

      VarFechaHoy.append("\n\n");

      VarFechaHoy.append("\n  dia1  = new Array("  );
	  VarFechaHoy.append(fechaConsulta("DIA")   );
	  VarFechaHoy.append(","   );
	  VarFechaHoy.append(fechaConsulta("DIA")   );
	  VarFechaHoy.append(");");

      VarFechaHoy.append("\n  mes1  = new Array("  );
	  VarFechaHoy.append(fechaConsulta("MES")   );
	  VarFechaHoy.append(","   );
	  VarFechaHoy.append(fechaConsulta("MES")   );
	  VarFechaHoy.append(");");

      VarFechaHoy.append("\n  anio1 = new Array("  );
	  VarFechaHoy.append(fechaConsulta("ANIO")  );
	  VarFechaHoy.append(","   );
	  VarFechaHoy.append(fechaConsulta("ANIO")  );
	  VarFechaHoy.append(");");

      VarFechaHoy.append("\n  Fecha = new Array('" );
	  VarFechaHoy.append(fechaConsulta("FECHA") );
	  VarFechaHoy.append("','" );
	  VarFechaHoy.append(fechaConsulta("FECHA") );
	  VarFechaHoy.append("');");

      return VarFechaHoy.toString();
 }

public String armaDiasInhabilesJS(){
      int indice       = 0;
      StringBuffer resultado = new StringBuffer("");

      if(diasNoHabilesJS!=null && diasNoHabilesJS.size() > 0) {
	      resultado.append(diasNoHabilesJS.elementAt(indice).toString());
          for(indice=1; indice<diasNoHabilesJS.size(); indice++)
		   {
	          resultado.append(", " );
			  resultado.append(diasNoHabilesJS.elementAt(indice).toString());
           }
      }

      return resultado.toString();
   }

   public String fechaConsulta(String tipo){
      StringBuffer strFecha = new StringBuffer("");
      Date Hoy = new Date();
      GregorianCalendar Cal = new GregorianCalendar();
      Cal.setTime(Hoy);
      if(tipo.equals("FECHA")){
         if(Cal.get(Calendar.DATE) <= 9)
		  {
           strFecha.append("0" );
		   strFecha.append(Cal.get(Calendar.DATE) );
		   strFecha.append("/");
		  }
         else
		  {
           strFecha.append(Cal.get(Calendar.DATE) );
		   strFecha.append("/");
		  }
         if(Cal.get(Calendar.MONTH)+1 <= 9)
		  {
           strFecha.append("0" );
		   strFecha.append((Cal.get(Calendar.MONTH)+1) );
		   strFecha.append("/");
		  }
         else
		  {
           strFecha.append((Cal.get(Calendar.MONTH)+1) );
		   strFecha.append("/");
		  }
         strFecha.append(Cal.get(Calendar.YEAR));
        }
      if(tipo.equals("DIA"))
        strFecha.append(Cal.get(Calendar.DATE));
      if(tipo.equals("MES"))
        strFecha.append((Cal.get(Calendar.MONTH)+1));
      if(tipo.equals("ANIO"))
        strFecha.append(Cal.get(Calendar.YEAR));

      return strFecha.toString();
    }

    public Calendar calculaFechaFin(){
	     Calendar fechaFin;
		 Calendar hoy      = new GregorianCalendar();
	     Calendar fechaHoy = new GregorianCalendar( hoy.get(hoy.YEAR), hoy.get(hoy.MONTH), hoy.get( hoy.DAY_OF_MONTH) );

		 fechaHoy.add( Calendar.MONTH, 3 );
	     fechaFin = evaluaDiaHabil(fechaHoy);

     return fechaFin;
	}

    protected void finalize() throws Throwable {
    	super.finalize();
    	   if(diasNoHabiles != null) {
    		   diasNoHabiles.clear();
    		   diasNoHabiles = null;
    	   }
    	   if(diasNoHabilesJS != null) {
    		   diasNoHabilesJS.clear();
    		   diasNoHabilesJS = null;
    	   }
    	   //BAL = null;
    }
}