/** 
*   Isban Mexico
*   Clase: TrxLZADDAO.java
*   Descripcion: clase de respuesta para almacenamiento y transporte de registros.
*
*   Control de Cambios:
*   1.0 Marzo 2016 FSW Everis 
*/
package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.TrxLZADBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;


/**
 * Clase para efectuar transaccion a LZAD a 390
 * @author FSW Everis
 *
 */
public class TrxLZADDAO extends GenericDAO{

    
    /** Constante con inforrmacion de formato 345.*/
    private static final int [] FRM_345 = {14,2,4,30,1,1,17,8,12,12,5,5,80,80,80,80,12,12,12,80,8,4,2,4,11,11,8};
    
    /** Constante con informacion del formato 318.*/
    private static final int [] FRM_318 = {12,80,2,4,11,11,30,1,1,17,5,14,8};
    
    /** Constante con informacion de formato 310.*/
    private static final int [] FRM_310 = {14,8,2,4,80,80,80,12,12,12,80,4,8,30,1,1,5};
    
    /**Encabezado de la trama de env&iacute;o*/
    private static final String HEADERLZMAD01 = "LZAD00851123451O00N2";

    /** Datos fijos para multicanalidad. */
    private static final String MULTICANALIDAD = "        ";
    
    /**Indicador de error en la ejecuci&oacute;n de la trama*/
    private static final String ERROR = "@ER";
        
    /** Constante tipo de registro 345*/
    private static final String TIPO_REG_345 = "345";
    
    /** Constante tipo de registro 345*/
    private static final String TIPO_REG_318 = "318";
    
    /** Constante tipo de registro 345*/
    private static final String TIPO_REG_310 = "310";
    
    /** Constante para informacion de respuesta 345.*/
    private static final String RESP_TRX390_345 = "LZM0345";
    
    /** Constante para informacion de respuesta 345.*/
    private static final String RESP_TRX390_318 = "LZM0318";

    /** Constante para informacion de respuesta 345.*/
    private static final String RESP_TRX390_310 = "LZM0310";
    
    /** Constante para informacion de respuesta 345.*/
    private static final int TAMANIO_345 = 595;
    
    /** Constante para informacion de respuesta 345.*/
    private static final int TAMANIO_318 = 196;

    /** Constante para informacion de respuesta 345.*/
    private static final int TAMANIO_310 = 433;

    /**
     * Ejecuta transaccion a 390
     * @param tipoRegMP tipo de registro AFD
     * @param ctaOrigen cuenta origen
     * @param ctaDestino cuenta destino 
     * @return la respuesta desentramada
     */
    public TrxLZADBean ejecutaConsulta(String tipoRegMP, String ctaOrigen, String ctaDestino){
        EIGlobal.mensajePorTrace("TrxLZADDAO - armaTrama(): [" +ctaOrigen+"] + [" +ctaDestino+"]", EIGlobal.NivelLog.INFO);
        StringBuffer cadena= new StringBuffer()
        .append(HEADERLZMAD01)
        .append(MULTICANALIDAD)
        .append(tipoRegMP)
        .append(ctaOrigen)
        .append(ctaDestino);

        String respuesta= null;
        
        EIGlobal.mensajePorTrace("TrxLZADDAO - armaTrama(): [" +cadena.toString()+"]", EIGlobal.NivelLog.INFO);
        respuesta = invocarTransaccion(cadena.toString());
        EIGlobal.mensajePorTrace("TrxLZADDAO - armaTrama() - RESULTADO TRAMA : [" + respuesta+"]",EIGlobal.NivelLog.INFO);
        return desentramaLZAD(respuesta,tipoRegMP);
    }
    
    /**
     * Realiza el desentramado de la respuesta
     * @param trama que proviene de 390
     * @param tipoRegMP tipo de registro AFD
     * @return el bean con la lista de campos obtenidos
     */
    private TrxLZADBean desentramaLZAD(String trama, String tipoRegMP){
        EIGlobal.mensajePorTrace("TrxLZADDAO - desentramaLZAD(): Inicio", EIGlobal.NivelLog.INFO);
        String formatoResp = "";
        int [] arrayFmt = null;
        int tamanioArray = 0;
        if(TIPO_REG_345.equals(tipoRegMP.trim())){
            EIGlobal.mensajePorTrace("TrxLZADDAO - tipoRegMP " +tipoRegMP.trim() , EIGlobal.NivelLog.INFO);
            formatoResp = RESP_TRX390_345;
            arrayFmt = FRM_345;
            tamanioArray = TAMANIO_345;
        }else if(TIPO_REG_318.equals(tipoRegMP.trim())){
            EIGlobal.mensajePorTrace("TrxLZADDAO - tipoRegMP " +tipoRegMP.trim() , EIGlobal.NivelLog.INFO);
            formatoResp = RESP_TRX390_318;
            arrayFmt = FRM_318;
            tamanioArray = TAMANIO_318;
        }else if(TIPO_REG_310.equals(tipoRegMP.trim())){
            EIGlobal.mensajePorTrace("TrxLZADDAO - tipoRegMP " +tipoRegMP.trim() , EIGlobal.NivelLog.INFO);
            formatoResp = RESP_TRX390_310;
            arrayFmt = FRM_310;
            tamanioArray = TAMANIO_310;
        }
        TrxLZADBean oTrama = new TrxLZADBean();
        if( "".equals(trama) || null == trama ){
            oTrama.setCodError("CODERROR");
            oTrama.setMensajeError("Trama Erronea");
            EIGlobal.mensajePorTrace("TrxLZADDAO - desentramaLZAD(): Trama Erronea: ", EIGlobal.NivelLog.INFO);
        }else{

            EIGlobal.mensajePorTrace("TrxLZADDAO - desentramaLZAD(): Trama valida, iniciando separacion" + trama, EIGlobal.NivelLog.INFO);
            
            if(trama.indexOf(formatoResp)>-1 ){
                 obtenTrama(trama, formatoResp, arrayFmt, tamanioArray, oTrama);
                 }else if(trama.indexOf(ERROR) > -1) {
                
                 oTrama.setCodError("ERQCE0000");
                 oTrama.setMensajeError("OCURRIO ERROR");

                 EIGlobal.mensajePorTrace("TrxLZADDAO - desentramaLZAD(): Trama Erronea:  OCURRIO ERROR", EIGlobal.NivelLog.INFO);
             
            }else if("".equals(trama.trim())) {
                 oTrama.setCodError("SINRESP390");
                 oTrama.setMensajeError("OCURRIO ERROR");

                EIGlobal.mensajePorTrace("TrxLZADDAO - desentramaLZAD(): SIN RESPUESTA 390", EIGlobal.NivelLog.INFO);
            }
            
     
        }

     EIGlobal.mensajePorTrace("TrxLZADDAO - desentramaLZAD(): Fin " , EIGlobal.NivelLog.INFO);
     return oTrama;

    }

    /**
     * Obtiene trama
     * @param trama que proviene de 390
     * @param formatoResp formato de respuesta
     * @param arrayFmt formato AFD
     * @param tamanioArray tamanio del arreglo segun formato
     * @param oTrama transporta informacion obtenida de la transaccion
     */
	private void obtenTrama(String trama, String formatoResp, int[] arrayFmt,
			int tamanioArray, TrxLZADBean oTrama) {
		String tramaResp = trama.substring(trama.indexOf(formatoResp)+9, trama.length());
		 EIGlobal.mensajePorTrace("TrxLZADDAO - desentramaLZAD() Trama obtenida de " + (trama.indexOf(formatoResp)+8)
		         +" a "+ trama.length() +" || Trama con datos :" + tramaResp+ "|", EIGlobal.NivelLog.INFO);
		 List<String> arrayCam = new ArrayList<String>();
		 int pos = 0;
		 try {
		     EIGlobal.mensajePorTrace("TrxLZADDAO - tipo registro: [ " + formatoResp +"] tamanio Respuesta |" + tramaResp.length(), EIGlobal.NivelLog.INFO);
		     if(tamanioArray <= tramaResp.length()){
		         EIGlobal.mensajePorTrace("TrxLZADDAO  obtener campos- Tamanio FMT : [ " + tamanioArray +"] tamanio Trama Respuesta |" + tramaResp.length() + "|", EIGlobal.NivelLog.INFO);
		        
		         for (int i = 0; i < arrayFmt.length; i++) {
		             arrayCam.add(tramaResp.substring(pos, pos + arrayFmt[i]));
		             //EIGlobal.mensajePorTrace(" pos " +i+" CADENA |" + tramaResp.substring(pos, pos + arrayFmt[i]) + "|",EIGlobal.NivelLog.INFO)
		             pos += arrayFmt[i];
		         }
		         oTrama.setCampos(arrayCam);
		     }
		 }catch(ArrayIndexOutOfBoundsException e){
		     EIGlobal.mensajePorTrace("ArrayIndexOutOfBoundsException " + e.getMessage(), EIGlobal.NivelLog.INFO); 
		 }catch (StringIndexOutOfBoundsException e) {
		     EIGlobal.mensajePorTrace("StringIndexOutOfBoundsException " + e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}
}
