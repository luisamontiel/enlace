/**
 * Clase revisar ClavePemex
 * Everis
 * clase implementada para el mentenimiento con folio 1681
 * Cobranza en l�nea Pemex micrositioEnlace
 * consulta la base de datos de claves de pemex en busca de la clave introducida por el usuario.
 */
package mx.altec.enlace.servlets;

//import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mx.altec.enlace.utilerias.Global;

/*import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
*/

	public class revisarClavePemex extends BaseServlet {

		private  String servidor = "180.176.34.12";
		private  String puerto = "1521";
		private  String nombreBD = "DGFI";
		private  String driver = "oracle.jdbc.driver.OracleDriver";
		private  String url = "";
		private  String user = "biatux";
		private  String pass = "biatux";

			public  Connection conn = null;
			private  Statement stmt = null;
			private  int consultasql = 0;
			ResultSet rs;
			/**
			public void Crear() {
				this.url= "jdbc:oracle:thin:@"+servidor+":"+puerto+":"+nombreBD;
				try {
					System.out.println("Everis-----------Conectando a la base de datos");
					Class.forName(driver);
					conn = DriverManager.getConnection(url, user,pass);
					//conn = DriverManager.getConnection(url);
					stmt = conn.createStatement();
					//stmt.setFetchDirection(ResultSet.TYPE_SCROLL_INSENSITIVE);
					} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}*/

			public void Crear() {
				try {
					conn = createiASConn (Global.DATASOURCE_ORACLE );
					stmt = conn.createStatement();
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			}

			public void Cerrar() {
				try {
					stmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					System.err.println("Error al Cerrar.");
				}

			}

			public int Inserta(String sql) throws SQLException{
					consultasql = stmt.executeUpdate(sql);
					return consultasql;
			}

			public  ResultSet Consulta(String sql) {
				try {
					System.out.println("Everis-----------Preparando consulta a la B.D.");
					System.out.println("Query->"+sql);
					rs = stmt.executeQuery(sql);
				} catch (SQLException e) {
					e.printStackTrace();
					System.err.println("Error en la Consulta.");
				}
				return rs;
			}



		}

