package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.*;

public class coMantenimiento extends HttpServlet
{
	public void Graficas (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException
	{
		//evalTemplate ("/jsp/pdLinea.jsp", request, response);
		// CombosConf combos = new CombosConf ();
		//combos.getServicioTux ().setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		//request.setAttribute ("arch_combos", combos);
		gotoPage ("/jsp/mantenimiento2.jsp", request, response);
	}

	  /** Handles the HTTP <code>GET</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        Graficas(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        Graficas(request, response);
    }

	private void gotoPage (String address, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher= request.getRequestDispatcher(address);
		dispatcher.forward(request,response);
	}
}
