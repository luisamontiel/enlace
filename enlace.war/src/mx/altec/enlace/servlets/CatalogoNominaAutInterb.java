package mx.altec.enlace.servlets;


import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;

import javax.sql.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.Convertidor;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.CatNomAutIntbBean;
import mx.altec.enlace.dao.CatNomAutInterbDao;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailContainer;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

//VSWF


public class CatalogoNominaAutInterb extends BaseServlet
{
    public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
        defaultAction( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
        defaultAction( request, response );
    }

    public void defaultAction( HttpServletRequest request, HttpServletResponse response )
        throws IOException, ServletException {
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");


        String modulo = "";
        modulo = (String) request.getParameter("Modulo");
        if ( (modulo == null) || ( modulo.equals("")))
             modulo = "0";

		log( "CatalogoNominaAutInterb.java::defaultAction() Modulo="+modulo );

        boolean sesionvalida = SesionValida( request, response );

            String fileOut       = "";
            String tramaEntrada  = "";
            String tramaSalida   = "";
            String coderror      = "";

		if ( sesionvalida ) {

            request.setAttribute("Fecha", ObtenFecha());
            request.setAttribute("Hora", ObtenHora());
            request.setAttribute("MenuPrincipal", session.getStrMenu());
            request.setAttribute("newMenu", session.getFuncionesDeMenu());
            //request.setAttribute( "Encabezado", CreaEncabezado( "Consulta de Cuentas","Administracion y control &gt; Cuentas Relacionadas &gt; Consulta", request ) );

            //String StrDeArchivo = getFileData( session.getArchivoAmb() );

            if ( modulo.equals( "0" ) ) {	//CONSULTACUENTAS

				EIGlobal.mensajePorTrace( "***ConCtas_AL orev antes"+request.getParameter("prev"), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***ConCtas_AL next"+request.getParameter("next"), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***ConCtas_AL total"+request.getParameter("total"), EIGlobal.NivelLog.INFO);
				int total=0,prev=0,next=0;

				if  ((request.getParameter("prev")!=null) &&
					 (request.getParameter("next")!=null) &&
					 (request.getParameter("total")!=null)) {
					prev =  Integer.parseInt(request.getParameter("prev"));
					next =  Integer.parseInt(request.getParameter("next"));
					total = Integer.parseInt(request.getParameter("total"));
				}
				else {
					EIGlobal.mensajePorTrace( "***ConCtas_AL todos nullos", EIGlobal.NivelLog.INFO);
					llamado_servicioCtasInteg(IEnlace.MMant_gral_ctas," "," "," ",session.getUserID8()+".ambci", request);
					total=getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
					EIGlobal.mensajePorTrace("Chris getnumberlinesCtasInteg   linea 78", EIGlobal.NivelLog.INFO);
					prev=0;
					  //EIGlobal.mensajePorTrace("Entro al else");
					  EIGlobal.mensajePorTrace("Global.NUM_REGISTROS_PAGINA "+Global.NUM_REGISTROS_PAGINA, EIGlobal.NivelLog.INFO);
					 if (total>Global.NUM_REGISTROS_PAGINA)
					  next=Global.NUM_REGISTROS_PAGINA;
					 else
					  next=total;
			   	}
			   	EIGlobal.mensajePorTrace( "***ConCtas_AL orev depues"+prev, EIGlobal.NivelLog.INFO);
			   	EIGlobal.mensajePorTrace( "***ConCtas_AL next"+next, EIGlobal.NivelLog.INFO);
			   	EIGlobal.mensajePorTrace( "***ConCtas_AL total"+total, EIGlobal.NivelLog.INFO);
				String[][] arrayCuentas = ObteniendoCtasInteg(prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
				EIGlobal.mensajePorTrace("Chris ObteniendoCtasInteg   linea 92", EIGlobal.NivelLog.INFO);

				//TODO BIT CU5061, entrada al flujo de Consulta Cuentas
				/*VSWF HGG I*/
				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try {
						BitaHelper bh = new BitaHelperImpl(request, session, sess);
						if (request.getParameter(BitaConstants.FLUJO)!=null){
							bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
						}else{
							bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
						}



						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EA_CUENTAS_CONSULTA_ENTRA);
						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}

						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
				}
				/*VSWF HGG F*/
				consultaCuentas( request, response, arrayCuentas, total, prev, next);
            }
            else if ( modulo.equals( "1" ) ) {	 //CONSULTACLAVES

				EIGlobal.mensajePorTrace( "***ConCtas_AL orev antes"+request.getParameter("prev"), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***ConCtas_AL next"+request.getParameter("next"), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***ConCtas_AL total"+request.getParameter("total"), EIGlobal.NivelLog.INFO);
				int total=0,prev=0,next=0;

			   	if  ((request.getParameter("prev")!=null) &&
					 (request.getParameter("next")!=null) &&
					 (request.getParameter("total")!=null)) {
				  	prev =  Integer.parseInt(request.getParameter("prev"));
				  	next =  Integer.parseInt(request.getParameter("next"));
				  	total = Integer.parseInt(request.getParameter("total"));
			  	}
			  	else {
					EIGlobal.mensajePorTrace( "***ConCtas_AL todos nullos", EIGlobal.NivelLog.INFO);
					llamado_servicioCtasInteg(IEnlace.MMant_gral_ctas," "," "," ",session.getUserID8()+".ambci", request);
					EIGlobal.mensajePorTrace("Chris llamado_servicioCtasInteg   linea 114", EIGlobal.NivelLog.INFO);
					total=getnumberlinesCtasIntegClabe(IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
					EIGlobal.mensajePorTrace("Chris getnumberlinesCtasIntegClabe   linea 116", EIGlobal.NivelLog.INFO);

					prev=0;

					if (total>Global.NUM_REGISTROS_PAGINA) {
						next=Global.NUM_REGISTROS_PAGINA;
					}
				 	else {
						next=total;
					}
				}
				EIGlobal.mensajePorTrace( "***ConCtas_AL orev depues"+prev, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***ConCtas_AL next"+next, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***ConCtas_AL total"+total, EIGlobal.NivelLog.INFO);
				String[][] arrayCuentasClabe = ObteniendoCtasIntegClabe(prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
				EIGlobal.mensajePorTrace("Chris ObteniendoCtasIntegClabe   linea 129", EIGlobal.NivelLog.INFO);
				consultaClaves( request, response, arrayCuentasClabe, total, prev, next);
            }
			else if ( modulo.equals( "2" ) ) {	//INICIO
				EIGlobal.mensajePorTrace("Entrando a CatalogoNominaAutInterb >> InicioConsCtas()", EIGlobal.NivelLog.INFO);
				try {

					InicioConsCtas(request, response);

				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
            }
			else if ( modulo.equals( "3" ) ) {
				EIGlobal.mensajePorTrace("Entrando a CatalogoNominaAutInterb >> generaTablaConsultar()", EIGlobal.NivelLog.INFO);
				try {
					generaTablaConsultar(request, response);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
            }
			else if ( modulo.equals( "4" ) ) {
				EIGlobal.mensajePorTrace("Entrando a CatalogoNominaAutInterb >> generaTablaConsultarRegistrosPorPaginacion()", EIGlobal.NivelLog.INFO);
			 	generaTablaConsultarRegistrosPorPaginacion(request, response);
            }
			else if ( modulo.equals( "5" ) ) {
				EIGlobal.mensajePorTrace("Entrando a CatalogoNominaAutInterb >> Realizando Alta o Bajas()", EIGlobal.NivelLog.INFO);
				if(session.getFacultad(session.FAC_AUTO_AB_OB)) {
				 	EjecutaAltaBajaRechazo(request, response);
					try{
						EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

						int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
	    				String claveOperacion = BitaConstants.AUTORIZACION_CANCELACION_CUENTAS;
	    				String concepto = BitaConstants.CONCEPTO_AUTORIZACION_CANCELACION_CUENTAS;

	    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
	    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

	    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

					} catch(Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
				}
				else {
					request.setAttribute( "MsgError", "No cuenta con facultades para Autorizar o Cancelar Cuentas.");
					evalTemplate( IEnlace.ERROR_TMPL, request, response );
				}
            }
		} else {
            request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
            evalTemplate( IEnlace.ERROR_TMPL, request, response );
        }
    }// fin del del default

    public int consultaCuentas( HttpServletRequest request, HttpServletResponse response, String[][] arrayCuentas, int total,int prev, int next)
            throws ServletException, IOException {
            String strSalida   = "";
            String strSalida1  = "";
            String strTipo     = "";
            String strProd     = "";
            String tipo_cuenta = "";
            int contador       = 1;
            boolean band       = true;


                for ( int indice = 1; indice <= Integer.parseInt( arrayCuentas[0][0] ); indice ++ ) {
                    EIGlobal.mensajePorTrace( "Cuentas consultaCuentas=" + arrayCuentas[ indice ][1], EIGlobal.NivelLog.INFO );


                    if ( band == true ) {
                        int residuo=contador % 2 ;
                        String bgcolor = "textabdatobs";
                        contador++;
                        if ( residuo == 0 ) {
                            bgcolor="textabdatcla";
                        }
                        if ( arrayCuentas[indice][2].equals( "P" ) ) {
                            strTipo = "Propia";
                        } else if ( arrayCuentas[indice][2].equals( "T" ) ) {
                            strTipo = "Terceros";
                        } else strTipo = "No Registrada";

                        strSalida = strSalida + "<TR><TD class=" + bgcolor +
                                "><CENTER>&nbsp; " + arrayCuentas[indice][1] +
                                "</CENTER></TD>" + "<TD class=" + bgcolor +
                                " aling=left>&nbsp;" + arrayCuentas[indice][4] + "</TD>"+
                                "<TD class=" + bgcolor + "><CENTER>&nbsp; " + strTipo +
                                "</CENTER></TD></TR> \n";
                } // del if
            } //del for

            String encabezaMenu ="<TD class=tittabdat align=\"center\">Cuenta</TD>"+
                                 "<TD class=tittabdat align=\"center\">Propietario</TD>"+
                                 "<TD class=tittabdat align=\"center\">Tipo</TD>";
             String encabezaMenu1 = "";
             String encabezaMenuU = "";
             String strSalidaU    = "";
             String claveCuen     = "";
            request.setAttribute( "encabezadosTabla", encabezaMenu );
            request.setAttribute( "encabezadosTablaU", encabezaMenuU );
            request.setAttribute( "encabezadosTabla1", encabezaMenu1 );
            request.setAttribute( "Mensaje1", strSalida);
            request.setAttribute( "MensajeU",  strSalidaU);
            request.setAttribute( "Mensaje2", strSalida1 );
            // Quitar cunado ya este el servicio en implantaci�n
            request.setAttribute( "boton","<a href=javascript:ValidaForma(document.frmPrincipal);><img border=0 name=Aceptar src=/gifs/EnlaceMig/Verclave.gif alt=VerclaBe>");
            request.setAttribute( "ContUser", ObtenContUser(request));
            request.setAttribute( "Encabezado", CreaEncabezado( "Consulta de Cuentas","Administraci&oacute;n y control &gt; Cuentas Relacionadas &gt; Consulta", "s26060h", request ) );
                //***********************************************
            //modificaci�n para integraci�n pva 06/03/2002
            //***********************************************
            String paginacion="";
			//EIGlobal.mensajePorTrace("*****total "+total);
            EIGlobal.mensajePorTrace("NUM_REGISTROS_PAGINA "+Global.NUM_REGISTROS_PAGINA, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("prev " +prev, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("next " +next, EIGlobal.NivelLog.INFO);
            if (total>Global.NUM_REGISTROS_PAGINA)
            {
              paginacion="<table  width=450 align=center>";
               if(prev > 0)
                paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:history.back();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";
              if(next < total)
              {
                if((next + Global.NUM_REGISTROS_PAGINA) <= total)
                paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(next -prev).toString() + " ></A></td></TR>";
                else
                paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(total-next).toString() + " ></A></td></TR>";
              }
              paginacion+="</table><br><br>";
              EIGlobal.mensajePorTrace( "***paginacion :"+paginacion, EIGlobal.NivelLog.INFO);
            }
            request.setAttribute("total", ""+total);
            request.setAttribute("prev", ""+prev);
            request.setAttribute("next", ""+next);
            request.setAttribute("paginacion", paginacion);
            request.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
            //***********************************************
            evalTemplate( IEnlace.PRINCIPAL, request, response );
        return 1;
    }


	public int consultaClaves( HttpServletRequest request, HttpServletResponse response, String[][] arrayCuentasClabe, int total,int prev, int next)
		throws ServletException, IOException
	{
			HttpSession sess = request.getSession ();
			BaseResource session = (BaseResource) sess.getAttribute ("session");

			ServicioTux tuxGlobal = new ServicioTux();
            //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
            String strSalida            = "";
            String strSalida1           = "";
            String strSalidaU           = "";
            String strTipo              = "";
            String strProd              = "";
            String tipo_cuenta          = "";
            String fileOut              = "";
            String tramaEntrada         = "";
            String tramaSalida          = "";
            String path_archivo         = "";
            String nombre_archivo       = "";
            String trama_salidaRedSrvr  = "";
            String retCodeRedSrvr       = "";
            String registro             = "";
            String claveCuen            = "";
            //Christian Barrios Osornio CBO 21/12/2004 Q21952
            String [] CtasClabe[]         =new String[Integer.valueOf(arrayCuentasClabe[0][0]).intValue()][4];
            //Christian Barrios Osornio CBO 21/12/2004 Q21952
            boolean bandera             = true;
            boolean band                = true;
            int contador = 1;

            fileOut = session.getUserID8() + ".tmp";
            EI_Exportar ArcSal;
        try
             {
            ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
            ArcSal.creaArchivo();

                for ( int indice = 1; indice <= Integer.parseInt( arrayCuentasClabe[0][0] ); indice ++ ) {
                    EIGlobal.mensajePorTrace( "Cuentas=" + arrayCuentasClabe[ indice ][1], EIGlobal.NivelLog.INFO );
                    ArcSal.escribeLinea(arrayCuentasClabe[ indice ][1]+ "\n" );          //+ "\r\n"
                    }
            ArcSal.cierraArchivo();
            ArchivoRemoto archR = new ArchivoRemoto();
            if(!archR.copiaLocalARemoto(fileOut))
             EIGlobal.mensajePorTrace("***CatalogoNominaAutInterb.class No se pudo crear archivo para exportar claves", EIGlobal.NivelLog.INFO);
            }catch(Exception ioeSua )
            {
              bandera = false;
             despliegaPaginaError("Servicio no disponible por el momento" ,"Consulta de Cuentas","Administracion y control &gt; Cuentas Relacionadas &gt; Consulta y Autorizaci&oacute;n","s26060h", request, response);
             EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
              return 1;
             }

            tramaEntrada = "2EWEB|" + session.getUserID8() + "|ECBE|" + session.getContractNumber() + "|" + session.getUserID8() +
                           "|" + session.getUserProfile() + "|";

             EIGlobal.mensajePorTrace( "***CatalogoNominaAutInterb.class >> tecb :" + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);
       try{
         Hashtable hs = tuxGlobal.web_red(tramaEntrada);
         tramaSalida = (String) hs.get("BUFFER");
         EIGlobal.mensajePorTrace( "***CatalogoNominaAutInterb.class trama_salida >>" + tramaSalida + "<<", EIGlobal.NivelLog.DEBUG);
       }catch( java.rmi.RemoteException re ){
    	   EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
       } catch (Exception e) {
    	   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
       }

         boolean errorFile  = false;
        ArcSal.borraArchivo();
        BufferedReader fileSua = null;// Modificacion RMM 20021218
       try
         {
            path_archivo = tramaSalida;
            nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());

            EIGlobal.mensajePorTrace( "***CatalogoNominaAutInterb.class & nombre_archivo_cb:" + nombre_archivo + " &", EIGlobal.NivelLog.INFO);

            // Copia Remota
            ArchivoRemoto archR = new ArchivoRemoto();
            if(!archR.copia(nombre_archivo))
             {
                EIGlobal.mensajePorTrace( "***CatalogoNominaAutInterb.class & consultaClaves: No se realizo la copia remota. &", EIGlobal.NivelLog.INFO);
             }
            else
             {
                EIGlobal.mensajePorTrace( "***CatalogoNominaAutInterb.class & consultaClaves: Copia remota OK. &", EIGlobal.NivelLog.INFO);
             }

            path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo; // +"0";

            EIGlobal.mensajePorTrace( "***CatalogoNominaAutInterb.class & path_archivo_cb:" + path_archivo + " &", EIGlobal.NivelLog.INFO);

            File drvSua = new File(path_archivo);
            fileSua = new BufferedReader(new FileReader(drvSua) );// Modificacion RMM 20021218

               int indice = 1;
              while( indice <= Integer.parseInt( arrayCuentasClabe[0][0]) && (registro = fileSua.readLine() ) != null && (bandera = true ) ){
                    EIGlobal.mensajePorTrace( "Cuentas=" + arrayCuentasClabe[ indice ][1], EIGlobal.NivelLog.INFO );

                    if( registro == null )
                        break;

            String cadAux = registro.substring(0, 6).trim();
            String elError= registro.substring(16, registro.trim().length());
            EIGlobal.mensajePorTrace( "***CatalogoNominaAutInterb.class & consultaClaves: cadAux. & "+ cadAux, EIGlobal.NivelLog.INFO);

            if (! ( cadAux.equals("CORR00") || cadAux.equals("ERROR3") )  )
              {

                        int residuo=contador % 2 ;
                        String bgcolor = "textabdatobs";
                        contador++;
                        if ( residuo == 0 ) {
                            bgcolor="textabdatcla";
                        }
                        if ( arrayCuentasClabe[indice][2].equals( "P" ) ) {
                            strTipo = "Propia";
                        } else if ( arrayCuentasClabe[indice][2].equals( "T" ) ) {
                            strTipo = "Terceros";
                        } else strTipo = "No Registrada";


    claveCuen   = claveCuen + "<tr><TD class=" + bgcolor +  "><CENTER>&nbsp; " + EIGlobal.BuscarToken(registro, '|', 1) +
                  "</CENTER></td>" + "<td class=" + bgcolor +   " aling=left>" + EIGlobal.BuscarToken(registro, '|', 3) + "</TD>" +
                  "<TD class=" + bgcolor +  " aling=left>&nbsp;" + arrayCuentasClabe[indice][4] + "</TD>"+
                  "<TD class=" + bgcolor + "><CENTER>&nbsp; " + strTipo + "</CENTER></TD></tr> \n";

    //Christian Barrios Osornio CBO 21/12/2004 Q21952
		CtasClabe[indice-1][0]=EIGlobal.BuscarToken(registro, '|', 1);
		CtasClabe[indice-1][1]=EIGlobal.BuscarToken(registro, '|', 3);
		CtasClabe[indice-1][2]=arrayCuentasClabe[indice][4];
		CtasClabe[indice-1][3]=arrayCuentasClabe[indice][2];

    //Christian Barrios Osornio CBO 21/12/2004 Q21952

                }else
             {
             bandera = false;
             despliegaPaginaError(elError ,"Consulta de Cuentas","Administracion y control &gt; Cuentas Relacionadas &gt; Consulta", "s26060h", request, response);
            }
            indice ++;
                } // del while
                fileSua.close();
//Christian Barrios Osornio CBO 21/12/2004 Q21952

if(CreaCtasClabeImpor(CtasClabe, Global.DIRECTORIO_LOCAL+ "/"+"CuentasClabe"+nombre_archivo+ ".txt"))
{
request.setAttribute("Nombre_Archivo","CuentasClabe"+nombre_archivo+ ".txt");
//request.setAttribute("Nombre_Archivo", IEnlace.DOWNLOAD_PATH+"CuentasClabe"+nombre_archivo+ ".txt");
//EIGlobal.mensajePorTrace(IEnlace.DOWNLOAD_PATH+"CuentasClabe"+nombre_archivo+ ".txt" , EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace(Global.DIRECTORIO_LOCAL + "/"+"CuentasClabe"+nombre_archivo+ ".txt" , EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("CuentasClabe"+nombre_archivo+ ".txt", EIGlobal.NivelLog.INFO);
	}
else{
request.setAttribute("Nombre_Archivo","No se pudo exportar el archivo");

}
//TODO BIT CU5061, se conecta al servicio CUENTAS_MODULO, Consulta de la Clabe Bancaria Estandarizada
  /*VSWF HGG I*/
if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
	try{
	  BitaHelper bh = new BitaHelperImpl(request, session, sess);

	  BitaTransacBean bt = new BitaTransacBean();
	  bt = (BitaTransacBean)bh.llenarBean(bt);
	  bt.setNumBit(BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_CLABE);
	  if (session.getContractNumber() != null) {
		  bt.setContrato(session.getContractNumber().trim());
	  }
	  if(tramaSalida != null){
		  if(tramaSalida.substring(0,2).equals("OK")){
			  bt.setIdErr("ECBE0000");
		  }else if(tramaSalida.length() > 8){
			  bt.setIdErr(tramaSalida.substring(0,8));
		  }else{
			  bt.setIdErr(tramaSalida.trim());
		  }
	  }
	  if (nombre_archivo != null) {
		bt.setNombreArchivo("CuentasClabe" + nombre_archivo + ".txt");
	}
	  bt.setServTransTux("ECBE");
	  BitaHandler.getInstance().insertBitaTransac(bt);

	} catch (SQLException e) {
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	} catch (Exception e) {
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	}
}
	 /*VSWF HGG F*/

//Christian Barrios Osornio CBO 21/12/2004 Q21952
         }catch(Exception ioeSua )
            {
              bandera = false;
              despliegaPaginaError("Servicio no disponible por el momento" ,"Consulta de Cuentas","Administracion y control &gt; Cuentas Relacionadas &gt; Consulta","s26060h", request, response);
              EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
              //return 1;// Modificacion RMM 20021218 commented out
             }finally{// Modificacion RMM 20021218 inicio
                if(null != fileSua){
                    try{
                        fileSua.close();
                    }catch(Exception e){
                    	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                    }
                }
                fileSua = null;
             }// Modificacion RMM 20021218 fin

        if (bandera){

            String encabezaMenuU= "<TD class=tittabdat align=\"center\">Cuenta</TD>";

          String encabezaMenu1= "<TD class=tittabdat align=\"center\">Clave Bancaria Estandarizada</TD>"+
                                  "<TD class=tittabdat align=\"center\">Propietario</TD>"+
                                  "<TD class=tittabdat align=\"center\">Tipo</TD>";
            String encabezaMenu = "";
            request.setAttribute( "encabezadosTabla", encabezaMenu );
            request.setAttribute( "encabezadosTablaU", encabezaMenuU );
            request.setAttribute( "encabezadosTabla1", encabezaMenu1 );
            request.setAttribute( "Mensaje1",  strSalida);
            request.setAttribute( "MensajeU",  strSalidaU);
            request.setAttribute( "Mensaje2",  claveCuen);
            request.setAttribute( "boton","");
            request.setAttribute( "ContUser", ObtenContUser(request));
            request.setAttribute( "Encabezado", CreaEncabezado( "Consulta de Cuentas","Administracion y control &gt; Cuentas Relacionadas &gt; Consulta", request ) );

            //***********************************************
            //modificaci�n para integraci�n pva 06/03/2002
            //***********************************************
            String paginacion="";
            if (total>Global.NUM_REGISTROS_PAGINA)
            {
              paginacion="<table  width=450 align=center>";
               if(prev > 0)
                paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:history.back();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";
              if(next < total)
              {
                if((next + Global.NUM_REGISTROS_PAGINA) <= total)
                  paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante1();\">Siguientes " + new Integer(next -prev).toString() + " ></A></td></TR>";
                else
                  paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante1();\">Siguientes " + new Integer(total-next).toString() + " ></A></td></TR>";
              }
              paginacion+="</table><br><br>";
              EIGlobal.mensajePorTrace( "***paginacion :"+paginacion, EIGlobal.NivelLog.INFO);
            }
            request.setAttribute("total", ""+total);
            request.setAttribute("prev", ""+prev);
            request.setAttribute("next", ""+next);
            request.setAttribute("paginacion", paginacion);
            request.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
            //***********************************************
            evalTemplate( IEnlace.PRINCIPAL, request, response );
                }
            return 1;
            }

     public int getnumberlinesCtasIntegClabe(String filename)
    {
        EIGlobal.mensajePorTrace("***getnumberlinesCtasInteg ", EIGlobal.NivelLog.INFO);
        int j        = 0;
        String linea = "";
        String dato  = "";
        String datos[] =null;
        int separadores=0;
        BufferedReader entrada = null;// Modificacion RMM 20021218
        try{
            entrada= new BufferedReader(new FileReader(filename)); // Modificacion RMM 20021218

            while((linea = entrada.readLine())!=null){
                if(linea.trim().equals(""))
                    linea = entrada.readLine();
                if(linea == null)
                    break;
                separadores=cuentaseparadores(linea);
                datos = desentramaC(""+separadores+"@"+linea,'@');
                int tamCuenta = dato.length();
                String claveProducto= datos[7];
				if(claveProducto.equals("1") || claveProducto.equals("2") || claveProducto.equals("3") || claveProducto.equals("6") || claveProducto.equals("8")  )
//MHG (IM224718)     if(claveProducto.equals("1"))
                    {
                    j++;
                  }
             }
            entrada.close();
        }catch(Exception e){
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            //return 0;    // Modificacion RMM 20021218 commented out
            j = 0;// Modificacion RMM 20021218 mas abajo return j;
        } finally{// Modificacion RMM 20021218 inicio
            if(null != entrada){
                try{
                    entrada.close();
                }catch(Exception e){
                	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                }
                entrada = null;
            }
        }// Modificacion RMM 20021218 fin
        EIGlobal.mensajePorTrace("***leer numero de lineas "+j, EIGlobal.NivelLog.INFO);
        return j;
    }

	 public String[][] ObteniendoCtasIntegClabe(int inicio, int fin, String archivo)
	 {
		EIGlobal.mensajePorTrace("***Entrando a  ObteniendoCtasInteg ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***inicio "+inicio, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***fin  "+fin, EIGlobal.NivelLog.INFO);
		String[][] arrcuentas =null;
		String datos[] =null;
		String linea = "";
		int separadores=0;
		BufferedReader entrada = null;// Modificacion RMM 20021218 asignar a null
		int indice=1;
		int contador=0;

		arrcuentas = new String[fin+1][9];
		arrcuentas[0][1] = "NUM_CUENTA";
		arrcuentas[0][2] = "TIPO_RELAC_CTAS";
		arrcuentas[0][3] = "TIPO_CUENTA";
		arrcuentas[0][4] = "NOMBRE_TITULAR";
        arrcuentas[0][5] = "CUENTA_HOGAN";
		arrcuentas[0][6] = "FM";
		arrcuentas[0][7] = "CVE_PRODUCTO_S";
		arrcuentas[0][8] = "CVE_SUBPRODUCTO_S";

		try
		{
		    entrada = new BufferedReader( new FileReader(archivo) );
			while ( ( linea = entrada.readLine() ) != null )
			{
 		        separadores=cuentaseparadores(linea);
				datos = desentramaC(""+separadores+"@"+linea,'@');
                String claveProducto= datos[7];
				if(claveProducto.equals("1") || claveProducto.equals("2") || claveProducto.equals("3") || claveProducto.equals("6") || claveProducto.equals("8")  )
//MHG (IM224718) if(claveProducto.equals("1"))
                        {
		    		if ((contador>=inicio)&&(contador<fin))
		            {
                           if(separadores!=5)
					       {
					         arrcuentas[indice][0]="8";
					         arrcuentas[indice][1]=datos[1]; //numero cta
    					     arrcuentas[indice][2]=datos[8]; //propia o de terceros
                             //EIGlobal.mensajePorTrace("**8 "+datos[8] , EIGlobal.NivelLog.INFO);
		    			     arrcuentas[indice][3]=datos[7]; //tipo cta
			    		     //EIGlobal.mensajePorTrace("**7 "+datos[7] , EIGlobal.NivelLog.INFO);
                             if (datos[2].equals("NINGUNA"))
					           arrcuentas[indice][4]=datos[3]; //descripcion
   					         else
                             arrcuentas[indice][4]=datos[2]+"  "+datos[3];
					        //EIGlobal.mensajePorTrace("**3 "+datos[3] , EIGlobal.NivelLog.INFO);
    					     if (datos[2].equals("NINGUNA"))
	    				       arrcuentas[indice][5]="";
		    			     else
			    		       arrcuentas[indice][5]=datos[2]; //cuenta hogan
   				    	     //EIGlobal.mensajePorTrace("**2 "+datos[2] , EIGlobal.NivelLog.INFO);
					         arrcuentas[indice][6]=datos[6]; //FM
   					         //EIGlobal.mensajePorTrace("**6 "+datos[6] , EIGlobal.NivelLog.INFO);
					         arrcuentas[indice][7]=datos[4]; //FM
   					        //EIGlobal.mensajePorTrace("**4 "+datos[4] , EIGlobal.NivelLog.INFO);
					        arrcuentas[indice][8]=datos[5];
					        //EIGlobal.mensajePorTrace("**5 "+datos[5] , EIGlobal.NivelLog.INFO);
					        }
        					   else //interbancarios
		        			   {
                                 arrcuentas[indice][1]=datos[1];
						         arrcuentas[indice][2]=datos[2];
        						 arrcuentas[indice][3]=datos[3];
		        				 arrcuentas[indice][4]=datos[4];
				        		 arrcuentas[indice][5]=datos[5];
                               }
                               indice++;
			            }
                  	    contador++;
                } // del if de claveProducto
	        }  //while
			entrada.close();
        } catch ( Exception e ) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} finally{// Modificacion RMM 20021218 inicio
            if(null != entrada){
                try{
                    entrada.close();
                }catch(Exception e){
                	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                }
                entrada = null;
            }
        }// Modificacion RMM 20021218 fin
        indice--;
        arrcuentas[0][0] = ""+indice;
		EIGlobal.mensajePorTrace("**Sali del arrcuentas "+arrcuentas[0][0], EIGlobal.NivelLog.INFO);
		return arrcuentas;
	 }



/*Christian Barrios Osornio CBO 21/12/2004 Q21952
Nuevo m�todo  para escribir las cuentas clave en un archivo de exportar en formato .txt Q21952*/


boolean CreaCtasClabeImpor(String datos[][], String nombre_archivo){

   String linea= new String();
   PrintWriter out;
   boolean f=true;

EIGlobal.mensajePorTrace("Chris ruta= "+nombre_archivo, EIGlobal.NivelLog.INFO);

try{

     out=new PrintWriter(new FileWriter(nombre_archivo));


      for(int i=0; i<datos.length;i++){
   	   StringBuffer buf= new StringBuffer(datos[i][0].trim());

   	   if(buf.length()<16){
   		   int r=16-buf.length();
   		   for(int j=0;j<r;j++){
   			   buf.append(" ");
   			   }
   	   }

   	   linea=buf.toString();

          buf=new StringBuffer(datos[i][1].trim());

          if(buf.length()<20){
   		   int r=20-buf.length();
   		   for(int j=0;j<r;j++){
   			   buf.append(" ");
   			   }
   		   }

   	   linea=linea+buf.toString();

          buf=new StringBuffer(datos[i][2].trim());

   	          if(buf.length()<40){
   	   		   int r=40-buf.length();
   	   		   for(int j=0;j<r;j++){
   	   			   buf.append(" ");
   	   			   }
   		       }

   	   linea=linea+buf.toString();

          if (datos[i][3].trim().equals( "P" ) ) {
              linea=linea + "01";
          } else if (datos[i][3].trim().equals( "T" ) ) {
              linea=linea + "02";
          } else linea=linea + "03";


   		out.println(linea);


   }//Fin del For

   out.flush();
out.close();

f=envia(new File(nombre_archivo));

EIGlobal.mensajePorTrace("Chris  CreaCtasClabeImpo   f="+f, EIGlobal.NivelLog.INFO);
 }//try

catch(Exception e){
	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("No se pudo crear el archivo......CreaCtasClabeImpor" , EIGlobal.NivelLog.INFO);
	f=false;
	EIGlobal.mensajePorTrace("Chris  CreaCtasClabeImpo   f="+f, EIGlobal.NivelLog.INFO);

	}


return f;




  }//Fin CreaCtasClabeImpor
/*Christian Barrios Osornio CBO 21/12/2004 Q21952
Nuevo m�todo  para escribir las cuentas clave en un archivo de exportar en formato .txt Q21952*/

/*Christian Barrios Osornio CBO 21/12/2004 Q21952
Nuevo m�todo  para copiar el archivo CuentasClabe+nombre_archivo+.txt en ambos servidores Q21952*/
boolean envia (File Archivo) {
       try {
		    EIGlobal.mensajePorTrace("dentro del metodo envia de CatalogoNominaAutInterb.java", EIGlobal.NivelLog.DEBUG);

           	boolean Respuestal = true;
           	ArchivoRemoto envArch = new ArchivoRemoto();


		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_LOCAL = " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_REMOTO_WEB = " + Global.DIRECTORIO_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_LOCAL = " + Global.HOST_LOCAL, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_REMOTO_WEB = " + Global.HOST_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);
			//Req. Q15591  Se corrige validacion para no copiar, se incluye comparacion de rutas.


		    EIGlobal.mensajePorTrace("Envia. Pasa validacion para generar copia remota", EIGlobal.NivelLog.DEBUG);
		  //IF PROYECTO ATBIA1 (NAS) FASE II

           	if(!envArch.copiaLocalARemoto(Archivo.getName(),"WEB")){

					EIGlobal.mensajePorTrace("*** CatalogoNominaAutInterb.envia  no se pudo copiar archivo remoto_web:" + Archivo.getName(), EIGlobal.NivelLog.ERROR);
					Respuestal = false;

				}
				else {
				    EIGlobal.mensajePorTrace("*** CatalogoNominaAutInterb.envia archivo remoto copiado exitosamente:" + Archivo.getName(), EIGlobal.NivelLog.DEBUG);

				}

           	if (Respuestal) {
			       return true;
			}

		   } catch (Exception ex) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
        }
        return false;
    }

	public void InicioConsCtas(  HttpServletRequest request, HttpServletResponse response) throws SQLException, Exception {
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String strInhabiles = armaDiasInhabilesJS();
		StringBuffer varFechaHoy=new StringBuffer();

		//Obtiene los usuarios de las cuentas asociadas al contrato.
		String[][] arrayUsuarios = TraeUsuarios(request);
		String Usuarios          = "";
		String NoUsuario         = "";

		long UserAux = 0;
		long numero1 = 0;
		long numero2 = 0;

        //modificacion para cliente 7to8 cgc 11/03/2004
           Convertidor con = new Convertidor();
           for (int z = 1; z <= Integer.parseInt( arrayUsuarios[0][0].trim() ); z++ )
 	      arrayUsuarios[z][1]=con.cliente_7To8(arrayUsuarios[z][1]);
 	   //fin de la modificacion 11/03/2004 cgc.

			Usuarios = "";

			for ( int indice = 1; indice <= Integer.parseInt( arrayUsuarios[0][0].trim() ); indice++ )
			{
				Usuarios += "<OPTION value = " + arrayUsuarios[indice][1] + ">" + arrayUsuarios[indice][1] + " " + arrayUsuarios[indice][2] + "\n";
			}

			String  datesrvr  = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">\n";
					datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">\n";
					datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">\n";

			request.setAttribute("Bitfechas", datesrvr);

			request.setAttribute("Usuarios",Usuarios);

		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb:  Entrando InicioConsCtas() -->> Cuentas Nuevo", EIGlobal.NivelLog.INFO);

  		/************************************************** Arreglos de fechas para 'Hoy' ... */
		varFechaHoy.append("  /*Fecha De Hoy*/");
		varFechaHoy.append("\n  dia=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes=new Array(");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n\n  /*Fechas Seleccionadas*/");
		varFechaHoy.append("\n  dia1=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes1=new Array(");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio1=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  Fecha=new Array('");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("','");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("');");

		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb Enviando strInhabiles + varFechaHoy...."+ strInhabiles  +varFechaHoy , EIGlobal.NivelLog.DEBUG);

        EIGlobal EIG=new EIGlobal(session , getServletContext() , this  );

		request.setAttribute("DiasInhabiles",strInhabiles);
		request.setAttribute("VarFechaHoy",varFechaHoy.toString());
		request.setAttribute("DiaHoy",EIG.fechaHoy("dd/mm/aaaa"));
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Filtro de consulta de cuentas por autorizar","Servicios &gt; N&oacute;mina Interbancaria &gt Registro de Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n","s55310h",request));
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb Enviando las nuevas variables a Cont_Cons.jsp....", EIGlobal.NivelLog.INFO);
		//TODO BIT CU5091, inicio de flujo Autorizaci�n
		 /*VSWF IHG I*/
		  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
			BitaHelper bh = new BitaHelperImpl(request, session, sess);

			if (request.getParameter(BitaConstants.FLUJO)!=null){
				bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
			}else{

				bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			}
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CUENTAS_AUTO_CANC_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		  }
		 /*VSWF HGG F*/

		evalTemplate ("/jsp/CatNomAutIntbInit.jsp", request, response);
	}

    public void generaTablaConsultar(  HttpServletRequest request, HttpServletResponse response)throws SQLException, Exception
	{

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
        request.setAttribute ("Fecha", ObtenFecha ());

		//String tramaServicio="";
		//String Result="";
		//String pipe="|";
		String nombreArchivo="";
		String nombreArchivoExportacion="";
		String exportar="";
		String mensajeError="";
		String arcLinea="";

			/* Variables de ambiente */
		String usuario=(session.getUserID8()==null)?"":session.getUserID8();
		String clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		String contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		String contratoConsulta=contrato;

		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos contrato:"+contrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos contratoConsulta:"+contratoConsulta, EIGlobal.NivelLog.INFO);

        // Filtro por usuario Opcion
        String usuarioOpcion = (String) request.getParameter ("usuarioOpcion");
        if(usuarioOpcion != null && (usuarioOpcion.equals("") || usuarioOpcion.equals(" "))) {
			usuarioOpcion = null;
		}


		// Campo en el cual se hara la consulta
       String tipoConsulta = (String) request.getParameter ("Registro");

        // Filtro por fecha
		String Fecha1_01 = (String) request.getParameter("Fecha1_01");
		String Fecha1_02 = (String) request.getParameter("Fecha1_02");

        // Folio de registro
        String folioReg = (String) request.getParameter ("Folio_Registro");
        if(folioReg != null && (folioReg.equals("") || folioReg.equals(" "))) {
			folioReg = null;
		}


        // Cuenta
        String fcuenta = (String) request.getParameter ("FCuenta");
        if(fcuenta != null && (fcuenta.equals("") || fcuenta.equals(" "))) {
			fcuenta = null;
		}

        // Filtro por Estatus
		String tipoEstatus = null;
		String estatus[] = null;
		if (request.getParameter ("TipoEstatus")!= null)
		 {
		      tipoEstatus = (String)request.getParameter ("TipoEstatus");

		      /*tipoEstatus = tipoEstatus.replaceAll("'", "");
		      estatus = tipoEstatus.split(",");*/
		 }
	else
	     {
            tipoEstatus= "" + (request.getParameter ("ChkAuto") == null ? "" : request.getParameter ("ChkAuto")) +
              (request.getParameter ("ChkRech") == null ? "" : request.getParameter ("ChkRech")) +
              (request.getParameter ("ChkCanc") == null ? "" : request.getParameter ("ChkCanc")) +
              (request.getParameter ("ChkPend") == null ? "" : request.getParameter ("ChkPend")) ;
           if (tipoEstatus == null || tipoEstatus.length () == 0)
              tipoEstatus = " ";

           if (tipoEstatus.endsWith (","))
               tipoEstatus = tipoEstatus.substring (0, tipoEstatus.length () - 1);

		      /*tipoEstatus = tipoEstatus.replaceAll("'", "");
		      estatus = tipoEstatus.split(",");*/

          }

        // Filtro por Tipo Operacion

		String tipoOperAB = null;
		if (request.getParameter ("tipoOperAB")!= null) {
		      tipoOperAB= (String)request.getParameter ("tipoOperAB");
		      int alta = tipoOperAB.indexOf("'A'");
		      int baja = tipoOperAB.indexOf("'B'");

		      if(alta == -1 && baja != -1) {
				tipoOperAB = "B";
			  }
		      else if(alta != -1 && baja == -1) {
				tipoOperAB = "A";
			  }
		      else {
				tipoOperAB = null;
			  }

		}
		else {
  		      tipoOperAB = "" + (request.getParameter ("Alta") == null ? "" : request.getParameter ("Alta")) +
              (request.getParameter ("Baja") == null ? "" : request.getParameter ("Baja"));
              if (tipoOperAB == null || tipoOperAB.length () == 0)
              tipoOperAB = " ";

  		      if (tipoOperAB.endsWith (",")) {
                  tipoOperAB = tipoOperAB.substring (0, tipoOperAB.length () - 1);
			  }

		      int alta = tipoOperAB.indexOf("'A'");
		      int baja = tipoOperAB.indexOf("'B'");

		      if(alta == -1 && baja != -1) {
				tipoOperAB = "B";
			  }
		      else if(alta != -1 && baja == -1) {
				tipoOperAB = "A";
			  }
		      else {
				tipoOperAB = null;
			  }

		}


/***********Session*****************************/

		request.getSession().setAttribute("usuarioOpcion",usuarioOpcion);

		request.getSession().setAttribute("Registro",tipoConsulta);

		request.getSession().setAttribute("Fecha1_01",Fecha1_01);
		request.getSession().setAttribute("Fecha1_02",Fecha1_02);

		request.getSession().setAttribute("Folio_Registro",folioReg);

		request.getSession().setAttribute("FCuenta",fcuenta);

		request.getSession().setAttribute("TipoEstatus",tipoEstatus);

		request.getSession().setAttribute("tipoOperAB",tipoOperAB);

/*****************************************/

		Fecha1_01=formateaCamposfecha(Fecha1_01);
		Fecha1_02=formateaCamposfecha(Fecha1_02);

		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos usuarioOpcion:"+usuarioOpcion, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos tipoConsulta:"+tipoConsulta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos Fecha1_01:"+Fecha1_01, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos Fecha1_02:"+Fecha1_02, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos folioReg:"+folioReg, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos fcuenta:"+fcuenta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos tipoEstatus:"+tipoEstatus, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>Campos Recibidos tipo operacion Alta o Baja:"+tipoOperAB, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >>>>El directorio al que van los archivos es: " + Global.DIRECTORIO_REMOTO_CUENTAS , EIGlobal.NivelLog.INFO);

		int totalRegistros=0;
		int registrosPorPagina=0;
		int totalPaginas=0;
		int pagina=0;

		//ArchivoRemoto archR = new ArchivoRemoto();

		CatNomAutInterbDao dao = new CatNomAutInterbDao();
		ArrayList<CatNomAutIntbBean> lista = dao.obtenSolicitudes(session.getContractNumber(),
														 usuarioOpcion,
														 tipoConsulta,
														 Fecha1_01,
														 Fecha1_02,
														 folioReg,
														 fcuenta,
														 tipoEstatus,
														 tipoOperAB);

		request.getSession().setAttribute("lista2", lista);


		//TODO BIT CU5091, Consulta de cuentas
		/*VSWF HGG I*/
        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{

			/*if(Result!=null || !Result.equals("null")){
				EI_Tipo temp=new EI_Tipo();
				if(Result.trim().substring(0,2).equals("OK")){
					temp.iniciaObjeto('@','|',Result);
					String archivos=temp.camposTabla[0][2];
					if(archivos.indexOf(";")>0){
						nombreArchivoExportacion=archivos.substring(archivos.indexOf(';')+1);
					 }
				}
			}*/

			BitaHelper bh = new BitaHelperImpl(request, session, sess);

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CUENTAS_AUTO_CANC_CONSULTA_CUENTA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			/*if(Result != null){
				if(Result.substring(0,2).equals("OK")){
					bt.setIdErr("CT030000");
				}else if(Result.length() > 8){
					bt.setIdErr(Result.substring(0,8));
				}else{
					bt.setIdErr(Result.trim());
				}
			}
			if (nombreArchivoExportacion != null) {
				bt.setNombreArchivo(nombreArchivoExportacion);
			}
			bt.setServTransTux("CT03");*/

			BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
        }
		/*VSWF HGG T*/


	if(lista == null) {
		despliegaPaginaErrorURL("<br>Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento.<br>Por favor intente mas tarde.","Autorizaci&oacute;n y Cancelaci&oacute;n ","Servicios &gt; N&oacute;mina Interbancaria &gt Registro de Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n","s55310h",request,response);
		return;
	}
	else {

				/*nombreArchivoExportacion=archivos.substring(archivos.indexOf(';')+1);

				 if(archR.copiaCUENTAS(nombreArchivoExportacion,Global.DIRECTORIO_LOCAL))
				  {
						nombreArchivoExportacion=nombreArchivoExportacion.substring(nombreArchivoExportacion.lastIndexOf("/")+1);
						if(archR.copiaLocalARemoto(nombreArchivoExportacion,"WEB"))
						 {
						   EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Se copio a WEB-A: "+ nombreArchivoExportacion, EIGlobal.NivelLog.INFO);
						   exportar= "<a href='/Download/"+nombreArchivoExportacion+"' border=0><img src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar' border=0></a>";
						 }
						else
						 EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> No se pudo realizar la copia remota al WEB del archivo exportacion:"+nombreArchivoExportacion, EIGlobal.NivelLog.INFO);

				 }
				 else
				 EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> No se pudo realizar la copia remota del archivo exportacion de tuxedo:"+nombreArchivoExportacion, EIGlobal.NivelLog.INFO);*/

			   //nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
			   totalRegistros=lista.size();
			   if(totalRegistros>0)
				 {
					EIGlobal.mensajePorTrace( "inicio pagina "+pagina, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "inicio totalRegistros "+totalRegistros, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "inicio registrosPorPagina "+registrosPorPagina, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "inicio totalPaginas "+totalPaginas, EIGlobal.NivelLog.INFO);


				   registrosPorPagina=Global.MAX_REGISTROS;
				   totalPaginas=calculaPaginas(totalRegistros,registrosPorPagina);
				   EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Generando la pantalla (Registros)", EIGlobal.NivelLog.INFO);

					EIGlobal.mensajePorTrace( "calculo pagina "+pagina, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "calculo totalRegistros "+totalRegistros, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "calculo registrosPorPagina "+registrosPorPagina, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "calculo totalPaginas "+totalPaginas, EIGlobal.NivelLog.INFO);

				   generaTablaConsultarRegistros(lista,Fecha1_01,Fecha1_02,contratoConsulta,pagina,totalRegistros,registrosPorPagina,totalPaginas,exportar,request,response);
				 }
				else if(totalRegistros<0)
				 {
					EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> No se pudo abrir el archivo ... (Registros)", EIGlobal.NivelLog.INFO);
					despliegaPaginaErrorURL("La <i><font color=blue>transacci&oacute;n</font></i> solicitada no se llevo a cabo correctamente.<br>Por favor intente mas tarde.","Consulta de Cuentas","Administraci&oacute;n y control &gt; Cuentas Relacionadas &gt Consulta y Autorizaci&oacute;n","s55310h",request,response);
					return;
				 }
				else if(totalRegistros==0)
				 {
					EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Archivo encontrado pero sin registros ... (Registros)", EIGlobal.NivelLog.INFO);
					despliegaPaginaErrorURL("No se encontraron <font color=green>registros</font> para el tipo de b&uacute;squeda seleccionado.","Autorizaci&oacute;n y Cancelaci&oacute;n","Servicios &gt; N&oacute;mina Interbancaria &gt Registro de Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n","s55310h",request,response);
					return;
				 }
		}

}

 public void generaTablaConsultarRegistros(ArrayList<CatNomAutIntbBean> lista,String Fecha1_01,String Fecha1_02,String contratoConsulta,int pagina,int totalRegistros,int registrosPorPagina,int totalPaginas,String exportar,HttpServletRequest request,HttpServletResponse response) throws ServletException, java.io.IOException
   {
      HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

  	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Objetos creados para la consulta de archivos pagina "+pagina, EIGlobal.NivelLog.INFO);
  	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Objetos creados para la consulta de archivos registrosPorPagina "+registrosPorPagina, EIGlobal.NivelLog.INFO);
  	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Objetos creados para la consulta de archivos totalRegistros "+totalRegistros, EIGlobal.NivelLog.INFO);

	  String numRegpage="";
	  String [] strRegistros ;
	  //String consulta="";

	  StringBuffer tablaRegistros=new StringBuffer("");
	  StringBuffer botones=new StringBuffer("");
	   String chkBox="";
	   String[] titulos={"",
			   "Tipo Operaci&oacute;n",
			   /*"Tipo Cuenta",*/
		       "Cuenta",
		       "Titular",
			   "Banco",
		       "Fecha Registro",
			   "Folio Registro",
		       "Fecha Autorizaci&oacute;n",
			   "Folio Autorizaci&oacute;n",
			   "Usuario Registr&oacute;",
			   "Usuario Autoriz&oacute;",
			   "Estado",
			   "Observaciones",};
      //int[] datos={12,4,9,1,2,3,10,4,6,5,11,7,8,0,12};
      int[] datos={12,4,8,1,2,9,3,5,4,10,6,7,0,11};
      //int[] datos={13,4,9,1,2,3,10,4,6,5,11,7,8,0,12};
      /* INICIO everis JARA Alta de Cuentas se agrego el 13*/
	  /* int[] values={13,0,9,1,2,3,10,4,6,5,11,7,8,0,12};*/
	  int[] values={12,0,8,1,2,9,3,5,4,10,6,7,12,11};
	  /* FIN everis JARA Alta de Cuentas */
	  int[] align={0,0,0,0,0,0,0,0,0,0,0,0,0};


			/*linea.append(bean.getEstatus());
			linea.append(bean.getCuenta());
			linea.append(bean.getNombre());
			linea.append(bean.getFechaReg());
			linea.append(bean.getFechaAut());
			linea.append(bean.getFolioReg());
			linea.append(bean.getUsrReg());
			linea.append(bean.getUsrAut());
			linea.append(bean.getOperacion());
			linea.append(bean.getBanco());
			linea.append(bean.getFolioAut());
			linea.append(bean.getObservaciones());*/


	  EI_Tipo regSel=new EI_Tipo();
	  EI_Tipo headSel=new EI_Tipo();

	  /*EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> generaTablaConsultarRegistros()-> Se leyo el archivo.", EIGlobal.NivelLog.INFO);

	  consulta=obtenDatos(nombreArchivo);

		*/

	  strRegistros=seleccionaRegistrosDeArchivo(lista,pagina,registrosPorPagina,totalRegistros);
	  //EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> nombreArchivo"+nombreArchivo , EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> pagina"+pagina , EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> registrosPorPagina"+registrosPorPagina, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> totalRegistros"+totalRegistros , EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Registros 0>>"+strRegistros[0] +"<<", EIGlobal.NivelLog.INFO);
	  regSel.iniciaObjeto(';','@',strRegistros[0]);
	  formateaCamposT(regSel,0,1,9);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Objetos creados para la consulta de archivos..."+regSel.chkBox, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Registros 1>>"+strRegistros[1] +"<<", EIGlobal.NivelLog.INFO);
	  regSel.chkBox=strRegistros[1];
	  tablaRegistros.append(regSel.generaTablaCHK(titulos,datos,values,align));
	  numRegpage=strRegistros[2];//para saber cuantos registros tengo por cada pagina
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Registros 2>>"+strRegistros[2] +"<<", EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> Objetos creados", EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb-> registros Pendientes en Pagina actual numRegpage: "+numRegpage, EIGlobal.NivelLog.INFO);

	  if(totalRegistros>registrosPorPagina)
	   {
		  EIGlobal.mensajePorTrace("CatalogoNominaAutInterb >> generaTablaConsultarRegistros()-> Estableciendo links para las paginas", EIGlobal.NivelLog.INFO);
		  botones.append("\n <br>");
		  botones.append("\n <table width=620 border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
		  EIGlobal.mensajePorTrace("CatalogoNominaAutInterb >> generaTablaConsultarRegistros()-> Pagina actual = "+pagina, EIGlobal.NivelLog.INFO);
		  if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
		  if(totalPaginas>=2)
		   {
			 for(int i=1;i<=totalPaginas;i++)
			  if(pagina+1==i)
			   {
				 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
				 botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a> ");
			   }
		   }
		  if(totalRegistros>((pagina+1)*registrosPorPagina))
			 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");

		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");
	   }

		request.setAttribute("Fecha1_01",Fecha1_01);
		request.setAttribute("Fecha1_02",Fecha1_02);
		//request.setAttribute("nombreArchivo",nombreArchivo);
		request.setAttribute("Exportar",exportar);
		request.setAttribute("contratoConsulta",contratoConsulta);
		request.setAttribute("totalPaginas",Integer.toString(totalPaginas));
		request.setAttribute("totalRegistros",Integer.toString(totalRegistros));
		request.setAttribute("numRegpage",numRegpage);
		request.setAttribute("registrosPorPagina",Integer.toString(registrosPorPagina));
		request.setAttribute("pagina",Integer.toString(pagina+1));
		request.setAttribute("tablaRegistros",tablaRegistros.toString());
		request.setAttribute("Botones",botones.toString());
		request.setAttribute("Pagina","1");

		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Autorizaci&oacute;n y Cancelaci&oacute;n","Servicios &gt; N&oacute;mina Interbancaria &gt Registro de Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n","s55320h",request));
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb Enviando las nuevas variables a  Cont_ConsTabla.jsp....", EIGlobal.NivelLog.INFO);
		evalTemplate ("/jsp/CatNomAutIntbTabla.jsp", request, response);
   }


/************************************ Arma dias inhabiles   *********************************/
/*************************************************************************************/
  String armaDiasInhabilesJS()
   {
      String resultado="";
      Vector diasInhabiles;

	  int indice=0;

      diasInhabiles=CargarDias(1);
      if(diasInhabiles!=null)
	   {
		 resultado=diasInhabiles.elementAt(indice).toString();
		 for(indice=1; indice<diasInhabiles.size(); indice++)
  		   resultado += ", " + diasInhabiles.elementAt(indice).toString();
       }
      if(diasInhabiles != null) {
		  diasInhabiles.clear();
		  diasInhabiles = null;
	  }
      return resultado;
	}

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas
  * @author CSA se actualiza para el manejo de cierre a base de Datos.
  * @since 17/11/2013
*************************************************************************************/
  public Vector CargarDias(int formato)
	{

	  EIGlobal.mensajePorTrace(" ContCtas_A>> Entrando a CargaDias ", EIGlobal.NivelLog.INFO);

	  Connection  Conexion = null;
	  PreparedStatement qrDias = null;
	  ResultSet rsDias = null;
	  String     sqlDias;
	  Vector diasInhabiles = new Vector();

	  boolean    estado;

	  sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2)";
	  EIGlobal.mensajePorTrace("ContCtas_A>> Query"+sqlDias,EIGlobal.NivelLog.DEBUG);
	  try
	   {
		 Conexion = createiASConn(Global.DATASOURCE_ORACLE);
		 if(Conexion!=null)
		  {
		    qrDias = Conexion.prepareStatement(sqlDias);
		    if(qrDias!=null)
		     {
		  	  rsDias = qrDias.executeQuery();
		       if(rsDias!=null)
		        {
		          while( rsDias.next())
					{
		              String dia  = rsDias.getString(1);
		              String mes  = rsDias.getString(2);
		              String anio = rsDias.getString(3);
		              if(formato == 0)
		               {
		                  dia  =  Integer.toString( Integer.parseInt(dia) );
		                  mes  =  Integer.toString( Integer.parseInt(mes) );
		                  anio =  Integer.toString( Integer.parseInt(anio) );
		               }

		              String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
		              diasInhabiles.addElement(fecha);
					  /********************/
		              }
		       }
			  else
		         EIGlobal.mensajePorTrace("ContCtas_A>>  Cannot Create ResultSet",EIGlobal.NivelLog.WARN);
		    }
		   else
		      EIGlobal.mensajePorTrace("ContCtas_A>>  Cannot Create Query",EIGlobal.NivelLog.WARN);
		 }
		else
		 EIGlobal.mensajePorTrace("ContCtas_A>> No se pudo crear la conexion",EIGlobal.NivelLog.WARN);
	   }catch(SQLException sqle ){
		   EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	   } finally {
		   EI_Query.cierraConexion(rsDias, qrDias, Conexion);
	   }
	 return(diasInhabiles);
	}


/*************************************************************************************/
/******************************************************************* fecha Consulta  */
/*************************************************************************************/
   String generaFechaAnt(String tipo)
    {
      String strFecha = "";
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);

	   while(Cal.get(Calendar.DAY_OF_WEEK) == 1 || Cal.get(Calendar.DAY_OF_WEEK) == 7) {
           Cal.add(Calendar.DATE, -1);
       }

      if(tipo.equals("FECHA")) {
         if(Cal.get(Calendar.DATE) <= 9)
           strFecha += "0" + Cal.get(Calendar.DATE) + "/";
         else
           strFecha += Cal.get(Calendar.DATE) + "/";

         if(Cal.get(Calendar.MONTH)+ 1 <= 9)
           strFecha += "0" + (Cal.get(Calendar.MONTH) + 1) + "/";
         else
           strFecha += (Cal.get(Calendar.MONTH) + 1) + "/";

         strFecha+=Cal.get(Calendar.YEAR);
        }

      if(tipo.equals("DIA"))
        strFecha += Cal.get(Calendar.DATE);

      if(tipo.equals("MES"))
        strFecha += (Cal.get(Calendar.MONTH) + 1);

      if(tipo.equals("ANIO"))
        strFecha += Cal.get(Calendar.YEAR);

      return strFecha;
    }
/*************************************************************************************/
/************************************************************* formatea Campos Fecha */
/*************************************************************************************/
	public String formateaCamposfecha(String fec)
	 {
		String fecha=fec;
		if(fec.trim().length()==10)
			fecha=fec.substring(6) + fec.substring(3,5) + fec.substring(0, 2);
		return fecha;
	 }

	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		param3="s55310h";

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,request ) );

		request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "CatalogoNominaAutInterb?Modulo=2");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}
/*************************************************************************************/
/****************************************** obtenDatos *********************************/
/*************************************************************************************/
  public String obtenDatos(String Archivo) throws IOException
	{
	  String linea="";
	  StringBuffer result=new StringBuffer("");

	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));

		  while((linea=arc.readLine())!=null)
			 result.append(linea+"@");
		  arc.close();
		}catch(IOException e)
		 {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			result = new StringBuffer("ERR");
		 }
	  return result.toString();
	}

/*************************************************************************************/
/*************************************************************************************/
/***************************************************************** obtenTotalLineas  */
/*************************************************************************************/
  public int obtenTotalLineas(String Archivo,HttpServletRequest req, HttpServletResponse res) throws IOException
    {
      int num=0;
	  int ind=0;
//	  int pos=(tipoFechaConsulta.equals("D"))?5:2;

	  double importeT=0;
	  double importeTotal=0;

	  String linea="";
	  String imp="";

	  EI_Tipo t=new EI_Tipo();

	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));
		  while((linea=arc.readLine())!=null)
		   {
				 java.util.StringTokenizer tokenizer = new java.util.StringTokenizer (linea,";");
//				 EIGlobal.mensajePorTrace("CatalogoNominaAutInterb >> obtenTotalLineas()->- linea" + linea, EIGlobal.NivelLog.INFO);
			 	 num++;
		   }
		  arc.close();
		}catch(IOException e)
		{
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			num=-1;
		}
      return num;
    }

/*************************************************************************************/
/**************************************************** Regresa el total de paginas    */
/*************************************************************************************/
   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }
/*************************************************************************************/
/*************************************************************************************/
/***************************************************************** obtenTitulos  */
/*************************************************************************************/
  public String obtenTitulos(String Archivo) throws IOException
    {
      String line="";
	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));
		  line=arc.readLine()+"@";
		  if(line==null)
			 return "";
		}catch(IOException e)
		 {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			line="";
		 }
      return line;
    }

  public String[] seleccionaRegistrosDeArchivo(ArrayList<CatNomAutIntbBean> lista,int pagina,int regPorPagina,int totalReg) throws IOException
	{
	  StringBuffer linea=new StringBuffer("");
	  String box="";
	  StringBuffer result=new StringBuffer("");
	  String [] resultx=new String[3];

	  int numregpage=0;
	  int inicio=0;
	  int fin=0;

	  inicio=(pagina*regPorPagina);
	  fin=inicio+regPorPagina;
	  if(fin>totalReg)
		fin=totalReg;


	  //BufferedReader arc=new BufferedReader(new FileReader(Archivo));

//	  linea=arc.readLine(); // Linea de datos para la tabla (Titulos y numero de datos)
	EIGlobal.mensajePorTrace("Inicio: "+ inicio, EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("Fin: "+ fin, EIGlobal.NivelLog.DEBUG);
	Calendar cal = Calendar.getInstance();
	long time;
	int minuto;
	  for(int i=inicio;i<fin;i++) {
  			EIGlobal.mensajePorTrace("Actual: "+ i, EIGlobal.NivelLog.DEBUG);

			CatNomAutIntbBean bean = lista.get(i);
			linea.setLength(0);
			linea.append(bean.getEstatus());
			linea.append(";");
			linea.append(bean.getCuenta());
			linea.append(";");
			linea.append(bean.getNombre());
			linea.append(";");
			time = bean.getFechaReg();
			if(time != 0) {
				cal.setTimeInMillis(time);
				linea.append(cal.get(Calendar.DAY_OF_MONTH));
				linea.append("/");
				linea.append(cal.get(Calendar.MONTH) + 1);
				linea.append("/");
				linea.append(cal.get(Calendar.YEAR));
				linea.append(" ");
				linea.append(cal.get(Calendar.HOUR_OF_DAY));
				linea.append(":");
				minuto = cal.get(Calendar.MINUTE);
				if(minuto < 10) {
					linea.append('0');
				}
				linea.append(minuto);
			}
			//linea.append(bean.getFechaReg());
			linea.append(";");
			time = bean.getFechaAut();
			if(time != 0) {
				cal.setTimeInMillis(time);
				linea.append(cal.get(Calendar.DAY_OF_MONTH));
				linea.append("/");
				linea.append(cal.get(Calendar.MONTH) + 1);
				linea.append("/");
				linea.append(cal.get(Calendar.YEAR));
				linea.append(" ");
				linea.append(cal.get(Calendar.HOUR_OF_DAY));
				linea.append(":");
				minuto = cal.get(Calendar.MINUTE);
				if(minuto < 10) {
					linea.append('0');
				}
				linea.append(minuto);
			}
			//linea.append(bean.getFechaAut());
			linea.append(";");
			linea.append(bean.getFolioReg());
			linea.append(";");
			linea.append(bean.getUsrReg());
			linea.append(";");
			if(bean.getUsrAut() != null) {
				linea.append(bean.getUsrAut());
			}
			linea.append(";");
			if(bean.getOperacion() == 'A') {
				linea.append("Alta");
			}
			else {
				linea.append("Baja");
			}
			linea.append(";");
			linea.append(bean.getBanco());
			linea.append(";");
			linea.append(bean.getFolioAut());
			linea.append(";");
			linea.append(bean.getObservaciones());
			linea.append(";");
			result.append(linea);
			result.append("@");


			if(linea.charAt(0)!='I' && linea.charAt(0)!='P')
			{
			box=box +"0";
			}
			else
			{
			box=box +"1";
			numregpage++;
			}
		}

	  String numreg= numregpage +"";
	  //arc.close();
	  EIGlobal.mensajePorTrace("CatalogoNominaAutInterb >> seleccionaRegistrosDeArchivo() result.toString(): "+result.toString(), EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("CatalogoNominaAutInterb >> seleccionaRegistrosDeArchivo() box: "+box, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("CatalogoNominaAutInterb >> seleccionaRegistrosDeArchivo() numreg: "+numreg, EIGlobal.NivelLog.INFO);
	  resultx[0]=result.toString();
	  resultx[1]=box;
	  resultx[2]=numreg;
	  return resultx;
	}

/*************************************************************************************/
/**************************** generaTablaConsultarRegistrosPorPaginacion************************/
/*************************************************************************************/
 public void generaTablaConsultarRegistrosPorPaginacion( HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> generaTablaConsultarRegistrosPorPaginacion()-> Entrando a paginacion de archivo", EIGlobal.NivelLog.INFO);

	  String Fecha1_01=(String)req.getParameter("Fecha1_01");
	  String Fecha1_02=(String)req.getParameter("Fecha1_02");
	  String nombreArchivo=(String)req.getParameter("nombreArchivo");
	  String contratoConsulta=(String)req.getParameter("contratoConsulta");
	  String exportar=(String)req.getParameter("Exportar");
	  String chkNombre=(String)req.getParameter("chkNombre");

      int totalRegistros=Integer.parseInt((String)req.getParameter("totalRegistros"));
	  int registrosPorPagina=Integer.parseInt((String)req.getParameter("registrosPorPagina"));
	  int totalPaginas=Integer.parseInt((String)req.getParameter("totalPaginas"));
	  int pagina=Integer.parseInt((String)req.getParameter("pagina"));

	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> generaTablaConsultarRegistrosPorPaginacion()-> Busqueda por registros ", EIGlobal.NivelLog.INFO);

	  ArrayList<CatNomAutIntbBean> lista = (ArrayList) req.getSession().getAttribute("lista2");
      generaTablaConsultarRegistros(lista,Fecha1_01,Fecha1_02,contratoConsulta,pagina,totalRegistros,registrosPorPagina,totalPaginas,exportar,req,res);
  }
/*************************************************************************************/
/****************************************formateaCamposT  *******************************/
/*************************************************************************************/
   public void formateaCamposT(EI_Tipo Tipo,int Est_Oper,int tipo_cta, int Tipo_Sol)
	{

	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> formateaCamposT()-> Formateando estatus operaci�n del objeto tabla: "+Est_Oper, EIGlobal.NivelLog.INFO);

	  for(int i=0;i<Tipo.totalRegistros;i++)
		{
		/*EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> formateaCamposT()-> i,Est_Oper:"+Tipo.camposTabla[i][Est_Oper], EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> formateaCamposT()-> i,tipo_cta:"+Tipo.camposTabla[i][tipo_cta], EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> formateaCamposT()-> i,Tipo_Sol:"+Tipo.camposTabla[i][Tipo_Sol], EIGlobal.NivelLog.INFO);*/

		  if(! (Tipo.camposTabla[i][Est_Oper]==null || Tipo.camposTabla[i][Est_Oper].trim().equals("")) )
			  {
					if(Tipo.camposTabla[i][Est_Oper].trim().equals("A"))
						Tipo.camposTabla[i][Est_Oper]="Ejecutada";
					else if(Tipo.camposTabla[i][Est_Oper].trim().equals("P"))
					  Tipo.camposTabla[i][Est_Oper]="Pendiente por activar";
					else
					if(Tipo.camposTabla[i][Est_Oper].trim().equals("I"))
					  Tipo.camposTabla[i][Est_Oper]="Pendiente por autorizar";
					else
					if(Tipo.camposTabla[i][Est_Oper].trim().equals("R"))
					  Tipo.camposTabla[i][Est_Oper]="Rechazado";
					else
					if(Tipo.camposTabla[i][Est_Oper].trim().equals("C"))
					  Tipo.camposTabla[i][Est_Oper]="Cancelado";

			  }

		  if(! (Tipo.camposTabla[i][tipo_cta]==null || Tipo.camposTabla[i][tipo_cta].trim().equals("")) )
			  {
	  				if(Tipo.camposTabla[i][tipo_cta].trim().equals("I"))
					  Tipo.camposTabla[i][tipo_cta]="Internacional";
					else
					if(Tipo.camposTabla[i][tipo_cta].trim().equals("E"))
					  Tipo.camposTabla[i][tipo_cta]="Externa";
					else
					if(Tipo.camposTabla[i][tipo_cta].trim().equals("P"))
					  Tipo.camposTabla[i][tipo_cta]="Propia";
					else
					if(Tipo.camposTabla[i][tipo_cta].trim().equals("T"))
					  Tipo.camposTabla[i][tipo_cta]="Tercera";
			  }

		 if(! (Tipo.camposTabla[i][Tipo_Sol]==null || Tipo.camposTabla[i][Tipo_Sol].trim().equals("")) )
			  {
	  				if(Tipo.camposTabla[i][Tipo_Sol].trim().equals("A"))
					  Tipo.camposTabla[i][Tipo_Sol]="Alta";
					else
					if(Tipo.camposTabla[i][Tipo_Sol].trim().equals("B"))
					  Tipo.camposTabla[i][Tipo_Sol]="Baja";
			  }

		/*for(int m=0;m<14;m++)
			  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> formateaCamposT()->"+i+","+m+">>"+Tipo.camposTabla[i][m]+"<<", EIGlobal.NivelLog.INFO);*/

	  EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> formateaCamposT()-> Campos formateados ...." + Tipo.camposTabla[i][Est_Oper], EIGlobal.NivelLog.INFO);
		}
	}

/*************************************************************************************/
/********************************** EjecutaAltaBajaRechazo*********************************/
/*************************************************************************************/
public int EjecutaAltaBajaRechazo( HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {

	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");

   	 String tramaServicio="";
	 String Result="";
	 String usuario="";
	 String contrato="";
	 String clavePerfil="";
  	 String tipo="";
	 String arcLinea="";
	 String medioEntrega	="";
	 String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/" + session.getUserID8()+".cctts";
	 String pipe="|";
	 String strMensaje="";
	 String MsgErrorR1="";
	 String MsgErrorR2="";
	 String registroRechazados="";
 	 String registroAceptados="";
	 String line="";
	 String Fecha1_01=(String)req.getParameter("Fecha1_01");
	 String Fecha1_02=(String)req.getParameter("Fecha1_02");
	 String totalRegistros=(String)req.getParameter("totalRegistros");
	 String numRegpage=(String)req.getParameter("numRegpage");
	 String chkNombre=(String)req.getParameter("chkNombre");
	 String tipoOper = req.getParameter( "tipoOper" );
	 String top="";
	 String srvrnombre="";
	 String estatus = "X";
	 boolean nomCanc = false;
	 boolean nomAut = false;
	 boolean nomAct = false;
	 boolean usrDup = false;
	 int respuesta[] = null;

	 /* INICIO everis JARA Alta de Cuentas */
	 String idLote = "0";
	 /* FIN everis JARA Alta de Cuentas */

	 /**GAE**/
	 //Vector detalleCuentas = new Vector();
	 String nomEstatus = "";
	 //String nomAut = "Pendiente por activar";
	 String tipoSolic = "";
	 /**FIN GAE**/

	 EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> EjecutaAltaBajaRechazo()-> Tipo Operacion: "+ tipoOper, EIGlobal.NivelLog.INFO);
     EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> EjecutaAltaBajaRechazo()-> Recibiendo trama Registros: "+ chkNombre, EIGlobal.NivelLog.INFO);

	int envioDeArchivo=0;
	boolean consulta=false;

		ServicioTux TuxGlobal = new ServicioTux();
		//TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		medioEntrega="2EWEB";

        EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
		ArchivoRemoto archR = new ArchivoRemoto();

		EI_Tipo TCTArchivo= new EI_Tipo();
		TCTArchivo.iniciaObjeto(chkNombre);

		int	regenviados=0;

		ArrayList<CatNomAutIntbBean> listaTotal = (ArrayList<CatNomAutIntbBean>) sess.getAttribute("lista2");

		if(listaTotal == null) {
			strMensaje="<i><font color=red>Error</font></i> Hubo un problema al obtener los datos.<br><br> Por favor realice la operaci&oacute;n <font color=green> nuevamente </font>.";
			despliegaPaginaErrorURL(strMensaje,"Autorizaci&oacute;n y Cancelaci&oacute;n","Administraci&oacute;n y control &gt; Cuentas &gt; Autorizaci&oacute;n y Cancelaci&oacute;n","s55320h", req, res);
			return 1;
		}

		EIGlobal.mensajePorTrace( "Cantidad de registros a procesar: " + listaTotal.size(), EIGlobal.NivelLog.DEBUG);


		ArrayList<CatNomAutIntbBean> lista = new ArrayList<CatNomAutIntbBean>();

		CatNomAutIntbBean tester = new CatNomAutIntbBean();
		CatNomAutIntbBean bean = null;;
		for(int i = 0; i < TCTArchivo.totalRegistros; i++) {
			EIGlobal.mensajePorTrace( "Fila " + i , EIGlobal.NivelLog.DEBUG);
			/*for(int j = 0; j < TCTArchivo.camposTabla[i].length; j++) {
				EIGlobal.mensajePorTrace( "Registro " + j + ": <" + TCTArchivo.camposTabla[i][j] + ">", EIGlobal.NivelLog.INFO);
			}*/
			tester.setFolioReg(Integer.parseInt(TCTArchivo.camposTabla[i][6]));	//asignar el folio;
			EIGlobal.mensajePorTrace( "Folio a procesar:" + tester.getFolioReg() , EIGlobal.NivelLog.DEBUG);
			bean = listaTotal.get(listaTotal.indexOf(tester));
			EIGlobal.mensajePorTrace( "Se encontro el bean:" + bean.toString() , EIGlobal.NivelLog.DEBUG);
			lista.add(bean);
			switch(bean.getEstatus()) {
				case 'I':	//Pendiente se autoriza o cancela
							if(tipoOper.trim().equals("A")) {
								EIGlobal.mensajePorTrace( "Usuario Registro:<" + bean.getUsrReg() + ">", EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace( "Usuario Firmado: <" + session.getUserID8()  + ">", EIGlobal.NivelLog.DEBUG);
								if(bean.getUsrReg().equals(session.getUserID8())) {
									usrDup = true;
								}
								else {
									bean.setUsrAut(session.getUserID8());
								}
							}
							break;
				case 'C':	//Cancelada	ya no se puede modificar este estatus;
							nomCanc = true;
							break;
				case 'P':	//Pendiente por Activar
							if (tipoOper.trim().equals("A")) {
								nomAut = true;
							}
							break;
				case 'A':	//Activa
							nomAct = true;
							break;
				default:	//Status Erroneo
							EIGlobal.mensajePorTrace("Se ha presentado un estatus invalido.", EIGlobal.NivelLog.WARN);
							break;

			}

		}

		if(nomAut){
			EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> Tratando de autorizar cuentas ya autorizadas.", EIGlobal.NivelLog.INFO);
			strMensaje="<i><font color=red>Error</font></i> No es posible autorizar cuentas ya autorizadas.<br><br> Por favor realice la operaci&oacute;n <font color=green> nuevamente </font>.";
			despliegaPaginaErrorURL(strMensaje,"Autorizaci&oacute;n y Cancelaci&oacute;n","Administraci&oacute;n y control &gt; Cuentas &gt; Autorizaci&oacute;n y Cancelaci&oacute;n","s55320h", req, res);
			return 1;
		}
		else if(nomCanc) {
			EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> Tratando de cancelar cuentas ya autorizadas.", EIGlobal.NivelLog.INFO);
			strMensaje="<i><font color=red>Error</font></i> No es posible cancelar cuentas ya canceladas.<br><br> Por favor realice la operaci&oacute;n <font color=green> nuevamente </font>.";
			despliegaPaginaErrorURL(strMensaje,"Autorizaci&oacute;n y Cancelaci&oacute;n","Administraci&oacute;n y control &gt; Cuentas &gt; Autorizaci&oacute;n y Cancelaci&oacute;n","s55320h", req, res);
			return 1;
		}
		else if(nomAct) {
			EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> Tratando de autorizar/cancelar cuentas ya activas.", EIGlobal.NivelLog.INFO);
			strMensaje="<i><font color=red>Error</font></i>No es posible autorizar o cancelar cuentas activas.<br><br> Por favor realice la operaci&oacute;n desde el m&oacute;dulo de Altas / Bajas.";
			despliegaPaginaErrorURL(strMensaje,"Autorizaci&oacute;n y Cancelaci&oacute;n","Administraci&oacute;n y control &gt; Cuentas &gt; Autorizaci&oacute;n y Cancelaci&oacute;n","s55320h", req, res);
			return 1;
		}
		else if(usrDup) {
			EIGlobal.mensajePorTrace( "CatalogoNominaAutInterb >> Tratando de autorizar con usuario que registro.", EIGlobal.NivelLog.INFO);
			strMensaje="<i><font color=red>Error</font></i>No es posible autorizar cuentas con el usuario que las registr&oacute;.<br><br> Por favor realice la operaci&oacute;n desde el m&oacute;dulo de Altas / Bajas.";
			despliegaPaginaErrorURL(strMensaje,"Autorizaci&oacute;n y Cancelaci&oacute;n","Administraci&oacute;n y control &gt; Cuentas &gt; Autorizaci&oacute;n y Cancelaci&oacute;n","s55320h", req, res);
			return 1;
		}

		EIGlobal.mensajePorTrace( "Tomamos en total " + lista.size() + " registros", EIGlobal.NivelLog.INFO);
		listaTotal.clear();
		sess.removeAttribute("lista2");

		if(tipoOper.trim().equals("A")) {
			top="Autorizaci&oacute;n";
			tipo="A";
			estatus = "P";

		}
		else if (tipoOper.trim().equals("R")) {
			top="Cancelaci&oacute;n";
			tipo="C";
			estatus = "C";
		}
		CatNomAutInterbDao dao = new CatNomAutInterbDao();
		respuesta = dao.autorizaSolicitudes(lista, estatus, session.getContractNumber(), nombreArchivo);



	 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
		BitaHelper bh = new BitaHelperImpl(req, session, sess);

		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.EA_CUENTAS_AUTO_CANC_AUTORIZA_ALTA_BAJA);
		if (session.getContractNumber() != null) {
			bt.setContrato(session.getContractNumber().trim());
		}
		/*if(Result != null){
			if(Result.substring(0,2).equals("OK")){
				bt.setIdErr("CT040000");
			}else if(Result.length() > 8){
				bt.setIdErr(Result.substring(0,8));
			}else{
				bt.setIdErr(Result.trim());
			}
		}*/
		bt.setServTransTux("CT04");

		BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
	 }
		/*VSWF HGG F*/



			req.setAttribute("strMensaje","La solicitud fue exitosa.");
			req.setAttribute("nombreArchivoImp",nombreArchivo);
			req.setAttribute("nombreArchivoDetalle",nombreArchivo);
			req.setAttribute("registroAceptados","" + respuesta[0]);
			req.setAttribute("registroRechazados","" + respuesta[1]);
			//req.setAttribute("totalRegistros","" + respuesta[2]);
			req.setAttribute("totalRegistros","" + lista.size());
			req.setAttribute("numRegpage",numRegpage);
			req.setAttribute("tipoOper",tipoOper);


			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Autorizaci&oacute;n y Cancelaci&oacute;n","Administraci&oacute;n y control &gt; Cuentas &gt Consulta y Autorizaci&oacute;n","s55330h", req));

			EIGlobal.mensajePorTrace("CatalogoNominaAutInterb >> agregaCuentas(): Finalizando ......", EIGlobal.NivelLog.WARN);
			//evalTemplate("/jsp/Cont_ConsEnvio.jsp", req, res);
			evalTemplate("/jsp/CatNomAutInterbEnv.jsp", req, res);

 return 1;
}


/*********************************************************************************************/
/***********************************NRN -- Getronics Q12535 Fin nuevo****************************/
/*********************************************************************************************/
}// de la clase