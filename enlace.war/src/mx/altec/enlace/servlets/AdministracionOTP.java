/** Banco Santander Mexicano
*   Clase              AdministracionOTP
*   @autor             David Aguilar Gomez.
*   @version           1.0
*   @date 			 : 18/Ago/2006
*   descripcion      : Administra los OTPs del sistema enlace*/
package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.servicios.NIPManagerServiceException;
import mx.altec.enlace.servicios.NIPManagerServices;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class AdministracionOTP extends BaseServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	String accion = request.getParameter("accion");
	HttpSession sess = request.getSession();
	BaseResource session=(BaseResource) sess.getAttribute("session");
	boolean session_ok=true;

	session_ok=SesionValidaOTP(request, response);
	if (session_ok) {

		String IP = " ",fechaHr = "";			//variables locales al metodo
		IP = request.getRemoteAddr();			//ObtenerIP
		java.util.Date fechaHrAct = new java.util.Date();
	    SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");//formato fecha
		String sid = sess.getId();
		fechaHr =	"["+fdate.format(fechaHrAct) +
					"] IP: " + IP +
					", SID: " + sid +
					" USR: " + session.getUserID8() +
					" CONTRATO: " + session.getContractNumber();				//asignacion de fecha y hora

		if(accion.equals("solToken")) {
			System.out.println("Solicitud Individual de Token");
			String respuesta = "";
			try {
				respuesta = NIPManagerServices.consultaEstado(session.getUserID8(), "E");
			}
			catch(Exception e) {
				e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
			}
			System.out.println("Estado consultaEstado: " + respuesta);
			StringTokenizer stok = new StringTokenizer(respuesta, "|");
			int codError = -1;
			String status = "";
			if(stok.hasMoreTokens()) {
				codError = Integer.parseInt(stok.nextToken());
			}
			if(codError == 0 && stok.hasMoreTokens()) { // salio bien
				stok.nextToken();	//ignorar el status de NIP Manager
				if(stok.hasMoreTokens()) {
					status = stok.nextToken();
				}
			}
			else if(stok.hasMoreTokens()) {
				status = stok.nextToken();
			}
			System.out.println("codError: " + codError);
			System.out.println("status: " + status);
			if(codError == 0) {
				String folio = "";
				switch(Integer.parseInt(status)) {
					case 3:
					case 8:
							try {
								respuesta = NIPManagerServices.solicitud(session.getUserID8(), "E", obtenCuenta(session.getContractNumber()).trim(), session.getContractNumber(),convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
							}
							catch(Exception e) {
								e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
							}
							System.out.println("Estado solicitud: " + respuesta);
							stok = new StringTokenizer(respuesta, "|");
							codError = -1;
							if(stok.hasMoreTokens()) {
								codError = Integer.parseInt(stok.nextToken());
							}
							if(codError == 0 && stok.hasMoreTokens()) { // sali bien
								folio = stok.nextToken();
							}else if(stok.hasMoreTokens()) {
								folio = stok.nextToken();
							}
							System.out.println("codError: " + codError);
							System.out.println("folio: " + folio);
							break;
			}
			if(codError == 0){
				try {
					respuesta = NIPManagerServices.solFolioSeguridad(session.getUserID8(), "E");
				}
				catch(Exception e) {
					e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
				}
				System.out.println("Estado solicitud: " + respuesta);
				stok = new StringTokenizer(respuesta, "|");
				codError = -1;
				folio = "";
				if(stok.hasMoreTokens()) {
					codError = Integer.parseInt(stok.nextToken());
				}
				if(codError == 0 && stok.hasMoreTokens()) { // salio bien
					folio = stok.nextToken();
				}else if(stok.hasMoreTokens()) {
					folio = stok.nextToken();
				}
				Vector vec = new Vector();
				String tipoSol = "";
				if(!status.equals("1")) {
					tipoSol = "SOLICITUD DE VINCULACION";
					folio = "*NO APLICA*";
				}
				else{
					if (folio != "")
						tipoSol = "SOLICITUD DE DISPOSITIVO";
				}
				String cliente[] = { tipoSol, session.getUserID8(), (String)sess.getAttribute("nameUsu"), folio};
				vec.add(cliente);
				sess.setAttribute("clientes", vec);
				sess.setAttribute("representante", "" + request.getParameter("representante"));
				request.setAttribute("codError", "" + codError);
				request.setAttribute("status", "" + status);
				sess.setAttribute("solInicial", ("" + esSolInicial(session.getUserID(), session.getContractNumber())));
				if(codError == 1){
					request.setAttribute("mensajeError", "cuadroDialogo('ERROR: " + folio + "', 4);");
					evalTemplate( IEnlace.SOLICITUD_INDIVIDUAL, request, response );
				}else{
					evalTemplate( IEnlace.IMPRESION_DOCUMENTOS, request, response );
				}
			}else{
				request.setAttribute("codError", "" + codError);
				request.setAttribute("status", "" + status);
				request.setAttribute("mensajeError", "cuadroDialogo('ERROR: " + folio + "', 4);");
				evalTemplate( IEnlace.SOLICITUD_INDIVIDUAL, request, response );
			}
		}
		else{
			if(status.startsWith("NO EXISTE")){
				try {
					respuesta = NIPManagerServices.solicitud(session.getUserID8(), "E", obtenCuenta(session.getContractNumber()).trim(), session.getContractNumber(),convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
				}
				catch(Exception e) {
					e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
				}
				System.out.println("Estado solicitud: " + respuesta);
				stok = new StringTokenizer(respuesta, "|");
				codError = -1;
				String folio = "";
				if(stok.hasMoreTokens()) {
					codError = Integer.parseInt(stok.nextToken());
				}
				if(codError == 0 && stok.hasMoreTokens()) { // salio bien
					folio = stok.nextToken();
				}else if(stok.hasMoreTokens()) {
					folio = stok.nextToken();
				}
				System.out.println("codError: " + codError);
				System.out.println("folio: " + folio);
				if(codError == 0){
					Vector vec = new Vector();
					String tipoSol = "";
					if(!status.equals("1")) {
						tipoSol = "SOLICITUD DE VINCULACION";
						folio = "*NO APLICA*";
					}
					else{
						if (folio != "")
							tipoSol = "SOLICITUD DE DISPOSITIVO";
					}
					String cliente[] = { tipoSol, session.getUserID8(), (String)sess.getAttribute("nameUsu"), folio};
					//String cliente[] = { convierteUsr7a8(session.getUserID()), (String)sess.getAttribute("nameUsu"), folio};
					vec.add(cliente);
					sess.setAttribute("clientes", vec);
					sess.setAttribute("representante", "" + request.getParameter("representante"));
					request.setAttribute("codError", "" + codError);
					request.setAttribute("status", "" + status);
					evalTemplate( IEnlace.IMPRESION_DOCUMENTOS, request, response );
				}
				else{
					request.setAttribute("codError", "" + codError);
					request.setAttribute("status", "" + status);
					request.setAttribute("mensajeError", "cuadroDialogo('ERROR: " + folio + "', 4);");
					evalTemplate( IEnlace.SOLICITUD_INDIVIDUAL, request, response );
				}
			}
			else{
				request.setAttribute("codError", "" + codError);
				request.setAttribute("status", "" + status);
				request.setAttribute("mensajeError", "cuadroDialogo('ERROR: " + status + "', 4);");
				evalTemplate( IEnlace.SOLICITUD_INDIVIDUAL, request, response );
			}
		}

		EIGlobal.mensajePorTrace(fechaHr + " Solicitud Token: " + codError + "/" + status, EIGlobal.NivelLog.INFO);
		}

		if(accion.equals("solTokens")) {
			System.out.println("Se hace la solicitud de los Tokens por contrato");
			String [][] datos = (String [][]) sess.getAttribute("datos");
			sess.removeAttribute("datos");
			Vector vec = new Vector();
			Vector vecErrores = new Vector();
			for(int i = 0; i < datos.length; i++) {
				String respuesta = "";
				int codError = -1;
				String folio = "";
				StringTokenizer stok;
				if(request.getParameter("objeto"+datos[i][0])!=null){
					if(datos[i][4].equals("3") || datos[i][4].equals("8") || datos[i][4].startsWith("NO EXISTE")){
						try {
							respuesta = NIPManagerServices.solicitud(convierteUsr7a8(datos[i][0]), "E", obtenCuenta(session.getContractNumber()).trim(), session.getContractNumber(),convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
						}
						catch(Exception e) {
							e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
						}
						System.out.println("Estado solicitud : " + respuesta);
						stok = new StringTokenizer(respuesta, "|");
						codError = -1;
						if(stok.hasMoreTokens()) {
							codError = Integer.parseInt(stok.nextToken());
						}
						if(codError == 0 && stok.hasMoreTokens()) { // sali bien
							folio = stok.nextToken();
						}else if(stok.hasMoreTokens()) {
								folio = stok.nextToken();
						}
						System.out.println("codError: " + codError);
						System.out.println("folio: " + folio);
						if(codError != 0){
							request.setAttribute("mensajeError", "cuadroDialogo('Algunas de las solicitudes no fueron exitosas revise los datos.', 4);");
							String cliente[] = { convierteUsr7a8(datos[i][0]), datos[i][1], folio};
							vecErrores.add(cliente);
							continue;
						}
					}
					try {
						respuesta = NIPManagerServices.solFolioSeguridad(convierteUsr7a8(datos[i][0]), "E");
					}catch(Exception e) {
						e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
					}
					System.out.println("Estado solicitud: " + respuesta);
					stok = new StringTokenizer(respuesta, "|");
					codError = -1;
					folio = "";
					if(stok.hasMoreTokens()) {
						codError = Integer.parseInt(stok.nextToken());
					}
					if(codError == 0 && stok.hasMoreTokens()) { // salio bien
						folio = stok.nextToken();
					}else if(stok.hasMoreTokens()) {
						folio = stok.nextToken();
					}
					if(codError == 1 && folio.startsWith("PROBLEMA CON NIP")){
						String cliente[] = { convierteUsr7a8(datos[i][0]), datos[i][1], folio};
						vecErrores.add(cliente);
						continue;
					}
					else if(datos[i][4].equals("7")){
						String cliente[] = { convierteUsr7a8(datos[i][0]), datos[i][1], "Usuario bloqueado"};
						vecErrores.add(cliente);
						continue;
					}

					String tipoSol = "";
					if(!datos[i][4].equals("1") && !datos[i][4].startsWith("NO EXISTE")) {
						tipoSol = "SOLICITUD DE VINCULACION";
						folio = "*NO APLICA*";
					}
					else{
						if (folio != "")
							tipoSol = "SOLICITUD DE DISPOSITIVO";
					}
					String cliente[] = { tipoSol, convierteUsr7a8(datos[i][0]), datos[i][1], folio};
					vec.add(cliente);
				}
			}
			sess.setAttribute("clientes", vec);
			sess.setAttribute("errores", vecErrores);
			sess.setAttribute("representante", "" + request.getParameter("representante"));
			evalTemplate( IEnlace.IMPRESION_DOCUMENTOS, request, response );
			EIGlobal.mensajePorTrace(fechaHr + " Solicitud Tokens masiva", EIGlobal.NivelLog.INFO);
		}

		if(accion.equals("repoToken")){
			System.out.println("Se hace la solicitud de la reposicion del Token");
			String respuesta = "";
			try {
				respuesta = NIPManagerServices.consultaEstado(session.getUserID8(), "E");
			}
			catch(Exception e) {
   				e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
			}
			System.out.println("Estado: " + respuesta);
			StringTokenizer stok = new StringTokenizer(respuesta, "|");
			int codError = -1;
			String status = "";
			if(stok.hasMoreTokens()) {
				codError = Integer.parseInt(stok.nextToken());
			}
			if(codError == 0 && stok.hasMoreTokens()) { // salio bien
				stok.nextToken();	//ignorar el status de NIP Manager
				if(stok.hasMoreTokens())
					status = stok.nextToken();
			}
			else if(stok.hasMoreTokens()) {
				status = stok.nextToken();
			}
			if(codError == 0) {
				switch(Integer.parseInt(status)) {
								//Unico caso para hacer la reposicion
					case 7:		status = "Bloqueado";
								try {
									respuesta = NIPManagerServices.solicitud(session.getUserID8(), "E", obtenCuenta(session.getContractNumber()).trim(), session.getContractNumber(),convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
								}
								catch(Exception e) {
									e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";
								}
								System.out.println("Estado solicitud: " + respuesta);
								stok = new StringTokenizer(respuesta, "|");
								codError = -1;
								String folio = "";
								if(stok.hasMoreTokens()) {
									codError = Integer.parseInt(stok.nextToken());
								}
								if(codError == 0 && stok.hasMoreTokens()) { // salio bien
									folio = stok.nextToken();
								}else if(stok.hasMoreTokens()) {
										folio = stok.nextToken();
								}
								System.out.println("codError: " + codError);
								System.out.println("folio: " + folio);
								if(codError == 0){
									Vector vec = new Vector();
									String tipoSol = "";

									if (folio != "")
										tipoSol = "SOLICITUD DE DISPOSITIVO";

									//String cliente[] = {convierteUsr7a8(session.getUserID()), (String)sess.getAttribute("nameUsu"), folio};
									String cliente[] = { tipoSol, session.getUserID8(), (String)sess.getAttribute("nameUsu"), folio};
									vec.add(cliente);
									sess.setAttribute("clientes", vec);
									sess.setAttribute("representante", "" + request.getParameter("representante"));
									request.setAttribute("codError", "" + codError);
									request.setAttribute("status", "" + status);
									evalTemplate( IEnlace.IMPRESION_DOCUMENTOS+"?origen=repoToken", request, response );
								}else{
									request.setAttribute("codError", "" + codError);
									request.setAttribute("status", "" + status);
									request.setAttribute("mensajeError", "cuadroDialogo('ERROR: " + folio + "', 4);");
									evalTemplate( IEnlace.REPOSICION_TOKEN, request, response );
								}
								break;
					default:	request.setAttribute("mensajeError", "cuadroDialogo('Solamente se puede solicitar reposici&oacute;n de Token si &eacute;ste ha sido bloqueado previamente.', 4);");
								evalTemplate( IEnlace.REPOSICION_TOKEN, request, response );
								return;
				}
			}else{
				request.setAttribute("mensajeError", "cuadroDialogo('ERROR: " + status + "', 4);");
				evalTemplate( IEnlace.REPOSICION_TOKEN, request, response );
				return;
			}

		EIGlobal.mensajePorTrace(fechaHr + " Reposicin Token: " + codError + "/" + status, EIGlobal.NivelLog.INFO);
		}
		if(accion.equals("actToken")) {
			String respuesta="";
			String mensageError="";
			boolean tokenValido=false;
			boolean serieValida=false;
			Token tok = new Token(session.getUserID8());
			MigracionOTP migracionOTP=new MigracionOTP();
			System.out.println("Serie pantalla: <" + (String) request.getParameter("serie") + ">");
			String serieDb = migracionOTP.obtenNoSerie(session.getUserID8());
			System.out.println("Serie db: <" + serieDb + ">");
			serieValida=((String) request.getParameter("serie")).equals(serieDb);
			if(!serieValida) {
					mensageError="El n&uacute;mero de serie que ingres&oacute; no coincide con el que tiene asignado.";
					request.setAttribute("mensajeError", "cuadroDialogo('"+mensageError+"', 3);");
					evalTemplate(IEnlace.ACTUALIZA_TOKEN, request, response);//ACTUALIZA_TOKEN
					return;
			}
			Connection conn = null;
			try {
				conn=createiASConn(Global.DATASOURCE_ORACLE);
				session.setValidarToken(tok.inicializa(conn));
				session.setToken(tok);
				tok.setMode(Token.TOK_SINCRONIZA);
				EIGlobal.mensajePorTrace("Status Token: "+tok.getStatus(), EIGlobal.NivelLog.INFO);
				tokenValido=tok.validaToken(conn, request.getParameter("clave").toString());
			} catch(SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (tokenValido) {
				try {
					respuesta=NIPManagerServices.activar(request.getParameter("serie").toString(), session.getUserID8(), "E");
				} catch(NIPManagerServiceException e) {
					e.printStackTrace();
					respuesta = "|1|PROBLEMA CON NIP MANAGER|";
				}
				System.out.println("Respuesta NIP Manager: " + respuesta);
				StringTokenizer stok = new StringTokenizer(respuesta, "|");
				if ((stok.countTokens()==1) && stok.nextToken().equals("0")) {
					evalTemplate( IEnlace.MENSAJE_ACTUALIZA_TOKEN, request, response);//MENSAJE_ACTUALIZA_TOKEN
				} else if (stok.countTokens()==2) {
					request.setAttribute("mensajeError", "cuadroDialogo('"+ stok.nextToken() +"', 4);");
					evalTemplate( IEnlace.ACTUALIZA_TOKEN, request, response);//ACTUALIZA_TOKEN
				} else {
					request.setAttribute("mensajeError", "cuadroDialogo('Ocurri&oacute; un error en la activaci&oacute;n', 4);");
					evalTemplate( IEnlace.ACTUALIZA_TOKEN, request, response);//ACTUALIZA_TOKEN
				}
			} else {
				//intentoFallido(session.getUserID());
				request.setAttribute("Fecha", ObtenFecha());
				sess.setAttribute("session", session);
				if(usuarioBloqueado(session.getUserID8())) {
					request.setAttribute("mensajeError", "cuadroDialogo(\"Usuario Bloqueado.\", 3);");
					log("LoginServlet.java::token()->resDup: "+ cierraDuplicidad(session.getUserID8()));
					sess.invalidate();
					evalTemplate(IEnlace.LOGIN, request, response);
				} else {
					if(!tokenValido) mensageError="Contrase&ntilde;a incorrecta ";
					//if(!serieValida) mensageError+="N&uacute;mero de serie inv&aacute;lido";
					request.setAttribute("mensajeError", "cuadroDialogo('"+mensageError+"', 3);");
					evalTemplate( IEnlace.ACTUALIZA_TOKEN, request, response);//ACTUALIZA_TOKEN
				}
			}

		EIGlobal.mensajePorTrace(fechaHr + " Activacion Token: " + " SERIAL: " +  ((String) request.getParameter("serie")), EIGlobal.NivelLog.INFO);
		}
		if(accion.equals("bloqToken"))
		{
			System.out.println("Se hace el bloqueo del Token");
			String motivo = (String) request.getParameter("motivo");
			String respuesta = "";
			try {
				respuesta = NIPManagerServices.bloquear(session.getUserID8(), "E", motivo);
			}catch(Exception e) { e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";}
			System.out.println("Respuesta NIP Manager: " + respuesta);
			StringTokenizer stok = new StringTokenizer(respuesta, "|");
			int codError = -1;
			String folioBloqueo = "";
			if(stok.hasMoreTokens()) {
				codError = Integer.parseInt(stok.nextToken());
			}
			if(stok.hasMoreTokens()) { // salio bien
				folioBloqueo = stok.nextToken();
			}
			if(codError == 0) {
				sess.setAttribute("folio", folioBloqueo);
				Vector vec = new Vector();
				String cliente[] = {session.getUserID8(), (String)sess.getAttribute("nameUsu"), folioBloqueo};
				vec.add(cliente);
				sess.setAttribute("clientes", vec);
				sess.setAttribute("representante", "" + request.getParameter("representante"));

				long time=System.currentTimeMillis();
				Date fecha=new Date(time);
				int hora=fecha.getHours();

				int minutos=fecha.getMinutes();
				String minutosStr  =Integer.toString(minutos);
				if (minutos<10){ minutosStr = "0" + minutosStr; }
				int segundos=fecha.getSeconds();
				String segundosStr  =Integer.toString(segundos);
				if (segundos<10){ segundosStr = "0" + segundosStr; }
				sess.setAttribute("horaBloqueo", hora+":"+minutosStr+":"+segundosStr);
				evalTemplate( IEnlace.MENSAJE_BLOQUEA_TOKEN, request, response );
			}
			else {
				request.setAttribute("mensajeError", "cuadroDialogo('ERROR: " + folioBloqueo + "', 4);");
				evalTemplate( IEnlace.BLOQUEA_TOKEN, request, response );
			}

			EIGlobal.mensajePorTrace(fechaHr + " Bloqueo Token: " + codError + "/" + folioBloqueo, EIGlobal.NivelLog.INFO);
		}
		if(accion.equals("solicitud"))
		{
		System.out.println("Se entrega la solicitud del Token");
		evalTemplate( IEnlace.IMPRIME_SOLICITUD+"?solicitud="+request.getParameter("solicitud"), request, response );
		}
		if(accion.equals("solicitudBloqueo"))
		{
		System.out.println("Se entrega la solicitud del Token");
		evalTemplate( IEnlace.IMPRIME_SOLICITUD_BLOQUEO, request, response );
		}
		if(accion.equals("contrato"))
		{
		System.out.println("Se entrega el contrato del Token");
		ServicioTux ctRemoto = new ServicioTux();
		//ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		Hashtable htResult = null;
		try {
			System.out.println("Enviando Trama...");
			htResult = ctRemoto.datosCliente(convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
			System.out.println("Trama Enviada... obteniendo respuesta");
			System.out.println("Resultado: <" + (String) htResult.get ("BUFFER") + ">");
		} catch (Exception e) {
			e.printStackTrace (); htResult = null;
		}
		if(htResult != null){
			String resp = ((String) htResult.get ("BUFFER")) + ">";
			String respDes[] = desentramaS(resp, '|');
			if(respDes.length > 0 && respDes[0].equals("DB2S0000")){
				sess.setAttribute("direccion", respDes[13]);
				sess.setAttribute("colonia", respDes[15]);
				sess.setAttribute("cpestado", respDes[14]+" "+respDes[17]+" "+obtenEstado(respDes[14]));
			}else{
				System.out.println("Error: "+(String) htResult.get ("BUFFER"));
			}
		}else
			System.out.println("Hubo problema con tuxedo......");
		evalTemplate( IEnlace.IMPRIME_CONTRATO, request, response );
		}


	} else {
		evalTemplate( IEnlace.SESION_FUERA, request, response );
	}
}

/**
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 * @param contrato
 * @return
 */
private String obtenCuenta(String contrato) {

	Connection conn = null;
	Statement sDup = null;
	ResultSet rs = null;
	 String cuenta = "";
	 try {
		conn = createiASConn ( Global.DATASOURCE_ORACLE );
		sDup = conn.createStatement();
		String query = "SELECT cta_comis_grales FROM tct_cuentas_tele WHERE num_cuenta = '"+ contrato +"'";
		EIGlobal.mensajePorTrace ("AdministracionOTP - obtenCuenta-> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
		rs  = sDup.executeQuery(query);
		while(rs.next()) {
			cuenta = rs.getString("cta_comis_grales");
		}

	 }
	 catch(SQLException e) {
		e.printStackTrace();
		EIGlobal.mensajePorTrace ("AdministracionOTP - obtenCuenta -> Problemas para cargar la cuenta del contrato " + contrato, EIGlobal.NivelLog.INFO);
	 }finally {
		EI_Query.cierraConexion(rs, sDup, conn);
	}
	 return cuenta;
}

/**
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 * @param contrato
 * @return
 */
private String obtenUsuarioEmpresa(String contrato) {

	Connection conn = null;
	Statement sDup = null;
	ResultSet rs = null;
	 String usuarioEmpresa = "";
	 try {
		conn = createiASConn ( Global.DATASOURCE_ORACLE );
		sDup = conn.createStatement();
		String query = "SELECT num_persona FROM nucl_cuentas WHERE num_cuenta = '"+ contrato +"'";
		EIGlobal.mensajePorTrace ("AdministracionOTP - obtenUsuarioEmpresa-> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
		rs  = sDup.executeQuery(query);
		while(rs.next()) {
			usuarioEmpresa = rs.getString("num_persona");
		}
		sDup.close();
		conn.close();
	 }
	 catch(SQLException e) {
		e.printStackTrace();
		EIGlobal.mensajePorTrace ("AdministracionOTP - obtenUsuarioEmpresa -> Problemas para cargar el usuarioEmpresa " + contrato, EIGlobal.NivelLog.INFO);
	 }finally {
		 EI_Query.cierraConexion(rs, sDup, conn);
	 }
	 return usuarioEmpresa;
}

/**
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 * @param cp
 * @return
 */
private String obtenEstado(String cp) {
	Connection conn = null;
	Statement sDup = null;
	ResultSet rs = null;
	 String estado = "";
	 try {
		conn = createiASConn ( Global.DATASOURCE_ORACLE );
		sDup = conn.createStatement();
		String query = "SELECT desc_estado FROM comu_estado WHERE '"+ cp +"' BETWEEN cp_inicio AND cp_final";
		EIGlobal.mensajePorTrace ("AdministracionOTP - obtenEstado-> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
		rs  = sDup.executeQuery(query);
		while(rs.next()) {
			estado = rs.getString("desc_estado");
		}

	 }
	 catch(SQLException e) {
		e.printStackTrace();
		EIGlobal.mensajePorTrace ("AdministracionOTP - obtenEstado -> Problemas para cargar el codigo postal " + cp, EIGlobal.NivelLog.INFO);
	 } finally {
		 EI_Query.cierraConexion(rs, sDup, conn);
	 }
	 return estado;
}
/**
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 * @param usuario
 * @param contrato
 * @return
 */
private boolean esSolInicial(String usuario, String contrato) {
	Connection conn = null;
	Statement sDup = null;
	ResultSet rs  = null;
	 boolean valor = true;
	 try {
		conn = createiASConn ( Global.DATASOURCE_ORACLE );
		sDup = conn.createStatement();
		String query = "SELECT count(1) as cuenta FROM eweb_esquema_token WHERE contrato = '"+ contrato +"' and cve_usr = '" + usuario + "'";
		EIGlobal.mensajePorTrace ("AdministracionOTP - esSolInicial-> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
		rs  = sDup.executeQuery(query);
		if(rs.next()) {
			valor = (rs.getInt("cuenta") == 0);
		}
		sDup.close();
		conn.close();
	 }
	 catch(SQLException e) {
		e.printStackTrace();
		EIGlobal.mensajePorTrace ("AdministracionOTP - obtenCuenta -> Problemas para cargar la cuenta del contrato " + contrato, EIGlobal.NivelLog.INFO);
	 }finally {
		 EI_Query.cierraConexion(rs, sDup, conn);
	 }
	 return valor;
}
}