package mx.altec.enlace.servlets;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.util.*;
import java.lang.reflect.*;
import java.sql.*;
import java.io.*;

public class EI_Plazas extends BaseServlet
{
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		String strPlazas="";
		String totales="";
		String Error="SUCCESS";

		Connection conn = null;
		ResultSet plazaResult = null;
		ResultSet contResult = null;

		int totalPlazas = 0;
		int salida=0;

		EIGlobal.mensajePorTrace("EI_Plazas - execute(): Entrando a Catalogo de Plazas.", EIGlobal.NivelLog.INFO);
		try
		 {
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null)
			 {
				EIGlobal.mensajePorTrace("EI_Plazas - execute(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }

			/************************************************** Contar registros de plazas .... */
			totales = "Select count(*) from comu_pla_banxico";
			PreparedStatement contQuery = conn.prepareStatement(totales);
			if(contQuery == null)
			 {
				EIGlobal.mensajePorTrace("EI_Plazas - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }
			contResult = contQuery.executeQuery();
			if(contResult==null)
			 {
				EIGlobal.mensajePorTrace("EI_Plazas - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }
			if(contResult.next())
			  totalPlazas = Integer.parseInt(contResult.getString(1));
			else
			  totalPlazas = 0;

			EIGlobal.mensajePorTrace("EI_Plazas - execute(): Total de Plazas : >" +Integer.toString(totalPlazas)+ "<", EIGlobal.NivelLog.INFO);

			/************************************************** Contar registros de bancos .... */
			String sqlplaza="Select * from comu_pla_banxico order by descripcion";
			PreparedStatement plazaQuery = conn.prepareStatement(sqlplaza);
			if(plazaQuery == null)
			 {
				EIGlobal.mensajePorTrace("EI_Plazas - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }
			plazaResult = plazaQuery.executeQuery();
			if(plazaResult==null)
			 {
				EIGlobal.mensajePorTrace("EI_Plazas - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }
			EIGlobal.mensajePorTrace("EI_Plazas prueba - Termino de generar el Result.", EIGlobal.NivelLog.ERROR);

			if(Error.equals("SUCCESS"))
			 {
				String arcLinea = "";
				String nombreArchivo = IEnlace.DOWNLOAD_PATH + "EI_Plazas.html";

				EI_Exportar ArcSal = new EI_Exportar(nombreArchivo);

				if(ArcSal.recuperaArchivo())
				{
					EIGlobal.mensajePorTrace("EI_Plazas - Entro a ArcSal.recuperaArchivo() .", EIGlobal.NivelLog.ERROR);
					arcLinea ="\n<html>";
					arcLinea+="\n<head><title>Plazas</title>";
					arcLinea+="<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>";
					arcLinea+="\n</head>";
					/*-->LCP inicia modificacion por incidencia IM159775*/
					arcLinea+="\n<script language='javaScript'>";
					arcLinea+="\n	var guardado = '';";
					arcLinea+="\n	function fnGuardaAnterior()";
					arcLinea+="\n	{";
					arcLinea+="\n		if(guardado.length == 0)";
					arcLinea+="\n			guardado = window.opener.document.transferencia.Descripcion.value;";
					arcLinea+="\n	}";
					arcLinea+="\n</script>";
					/*LCP - termina modificacion por incidencia IM159775 <--*/
					arcLinea+="\n<body bgcolor='#FFFFFF'>";
					arcLinea+="\n <form name=Forma>";
					arcLinea+="\n   <table border=0>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n        <th class='tittabdat'> Seleccione una plaza </th>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n        <td class='tabmovtex'>";
					/*--> LCP se agrega el OnChange por incidencia IM159775 <--*/
					arcLinea+="\n         <select NAME=SelPlaza size=20 OnChange='javascript:fnGuardaAnterior();opener.Selecciona(1);' class='tabmovtex'>\n";
					/*LCP - termina <--*/
					if(!ArcSal.escribeLinea(arcLinea))
						EIGlobal.mensajePorTrace("EI_Plazas - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);

					plazaResult.next();
					for(int i=0; i<totalPlazas; i++)
					//while(plazaResult.next())
					{
						arcLinea="\n          <option value="+plazaResult.getString(1).trim()+">"+plazaResult.getString(3).trim()+"</option>";
						if(!ArcSal.escribeLinea(arcLinea))
							EIGlobal.mensajePorTrace("EI_Plazas - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
						plazaResult.next();
					}

					EIGlobal.mensajePorTrace("EI_Plazas - execute(): Se escribieron las plazas...", EIGlobal.NivelLog.INFO);

					//while(!ArcSal.eof())
					//{
					//	if(!ArcSal.escribeLinea("                                                                                  "))
					//	EIGlobal.mensajePorTrace("EI_Plazas - execute(): Algo salio mal eof()", EIGlobal.NivelLog.INFO);
					//}

					arcLinea+="\n         </select>";
					arcLinea+="\n        </td>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n          <td><br></td>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n        <td align=center>";
					arcLinea+="\n         <table border=0 align=center cellspacing=0 cellpadding=0>";
					arcLinea+="\n          <tr>";
					arcLinea+="\n           <td>";
					/*--> LCP inicia. IM159775 Se agrega funcionalidad a bot�n de Cancelar*/
					arcLinea+="\n            <a href='javascript:opener.Selecciona(1); window.close();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.opener.document.transferencia.Descripcion.value=guardado;window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>";
					/*--> LCP termina. IM159775 Se agrega funcionalidad a bot�n de Cancelar*/
					arcLinea+="\n           </td>";
					arcLinea+="\n          </tr>";
					arcLinea+="\n         </table>";
					arcLinea+="\n        </td>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n    </table>\n </form>\n</body>\n</html>";

					if(!ArcSal.escribeLinea(arcLinea))
						EIGlobal.mensajePorTrace("EI_Plazas - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);

					ArcSal.cierraArchivo();

					//###### Cambiar....
					//*************************************************************
					ArchivoRemoto archR = new ArchivoRemoto();

					if(!archR.copiaLocalARemoto("EI_Plazas.html","WEB"))
						EIGlobal.mensajePorTrace("EI_Plazas - execute(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);
					//**************************************************************
				} else {
					strPlazas+="\n<p><br><table border=0 width=90% align=center>";
					strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
					strPlazas+="\n<tr><th bgcolor=red><font color=white>En este momento no se pudo generar cat&aacunte;logo. Por favor intente m&aacute;s tarde.<font></td></tr>";
					strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
					strPlazas+="\n</table>";
				}
			 }
		    else
			 {
				strPlazas+="\n<p><br><table border=0 width=90% align=center>";
				strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
				strPlazas+="\n<tr><th bgcolor=red><font color=white>Su transacci&oacute;n no puede ser atendida. Por favor intente m&aacute;s tarde.<font></td></tr>";
				strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
				strPlazas+="\n</table>";
			 }
		}catch( SQLException sqle )
		 {sqle.printStackTrace();}
		 catch(Exception e)
		 {e.printStackTrace();}
		 finally
		  {
			try
			 {
			   conn.close();
			   contResult.close();
			   plazaResult.close();
			 }catch(Exception e) {}
		  }

		EIGlobal.mensajePorTrace("EI_Plazas - el valor de strPlazas : >" +strPlazas+ "<", EIGlobal.NivelLog.INFO);

		req.setAttribute("Splazas", strPlazas);
		evalTemplate("/jsp/EI_Plazas.jsp", req, res);

		EIGlobal.mensajePorTrace("EI_Plazas - execute(): Saliendo de Catalogo de Plazas.", EIGlobal.NivelLog.INFO);
	}
}