package mx.altec.enlace.servlets;

import java.util.*;
import java.util.Date;
import java.sql.*;
import javax.servlet.http.*;
import javax.servlet.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

//09/01/07

/**<B><I><code>MCL_Movimientos</code></I></B>
* <P>Modificaciones:
* 01/10/2002 Se cambiaron vocales acentuadas por su c&oacute;digo de escape.<BR>
* </P>
*/
public class MCL_Movimientos extends BaseServlet {

	/**  Constante para el valor session. */
	private static final String SESSION = "session";

	 /** El objeto pipe. */
 	String pipe="|";

     /**
      * Setup output.
      *
      * @return Objeto int
      */
     public int setupOutput()
      {
        java.util.Date curDate = new java.util.Date();
        return 0;
      }


    /**
     * Do get.
     *
     * @param req El objeto: req
     * @param res El objeto: res
     * @throws ServletException La servlet exception
     * @throws IOException Una excepcion de I/O ha ocurrido.
     */
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

		defaultAction( req, res );
	}


	/**
	 * Do post.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException {
		defaultAction( req, res );
	}

	/**
	 * Default action.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		/* C�digo para debuger de clase */
		Enumeration eParameters = req.getParameterNames();
		while ( eParameters.hasMoreElements() ) {
			String sParameter = ( String ) eParameters.nextElement();
			log( "defaultAction()::" +  sParameter + " = " + req.getParameter( sParameter ) );
		}
		//System.out.println("MCL_Movimientos.Version:1.0");

        String contrato     = "";
		String usuario      = "";
		String clavePerfil  = "";
		String tipoModulo   = "";
		String titulo1      = "";
		String titulo2      = "";
		String arcAyuda     = "";
		String tipoCuenta   = "";

		String modulo       = "";
		String cveMov       = "";
		String importeDe      = "0.0";
		String importeA      = "0.0";
		String referencia   = " ";
		String facConMov    = "";
		String tipoArchivoExport    = req.getParameter("tipoArchivoExport");
		if(tipoArchivoExport!=null&&!("".equals(tipoArchivoExport))){
			exportar(req, res);
			return;
		}

		EI_Tipo Seleccion      = new EI_Tipo(this);
		EI_Tipo FacArchivo     = new EI_Tipo(this);
		EI_Tipo TCTArchivo     = new EI_Tipo(this);

		boolean existenCuentas = false;

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Entrando a Consulta Movtos para Pos-Linea. v1.0.0", EIGlobal.NivelLog.DEBUG);
		modulo = (String) req.getParameter( "Modulo" );
		String flujo = (String)req.getParameter(BitaConstants.FLUJO);

		boolean sesionvalida = SesionValida( req, res );

		//		09/01/07
		/*
		 * VSWF-BMB-I
		 */
		BitaHelper bh = new BitaHelperImpl(req, session, sess);
		BitaTransacBean bt = new BitaTransacBean();
		/*
		 * VSWF-BMB-F
		 */
		if(sesionvalida)
		 {
		   req.removeAttribute("_MovTasaAnual"); //TasaAnualizada
		   //Variables de sesion ...
		   contrato = session.getContractNumber();
		   usuario = session.getUserID8();
		   clavePerfil = session.getUserProfile();
		   if(session.getFacultad(session.FAC_CONS_MOVTOS_CRED)){
  		     facConMov = "true";
		   }else{
			 facConMov = "false";
		   }
		   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Fac Movientos: " + facConMov, EIGlobal.NivelLog.INFO);

		   tipoModulo=( String ) req.getParameter("Tipo");
		   req.setAttribute("Tipo", tipoModulo);
		   tipoCuenta=( String ) req.getParameter("Cuenta");
		   cveMov=( String ) req.getParameter("cveMov");
		   importeDe=( String ) req.getParameter("Importe");
		   importeA=( String ) req.getParameter("ImporteA");
		   if (importeDe==null) {
			   importeDe=( String ) req.getParameter("ImporteDe");
		   }
		   referencia=( String ) req.getParameter("Referencia");

		   if(modulo==null){
			   modulo="0";
		   }
		   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Cuenta: "+tipoCuenta, EIGlobal.NivelLog.INFO);
		   EIGlobal.mensajePorTrace("MCL_Movimientos - tipoModulo:"+tipoModulo, EIGlobal.NivelLog.INFO);

           //modificacion para integracion
		   if(tipoModulo!=null)
		   {
		     if("1".equals(tipoModulo)){
		    	 session.setModuloConsultar(IEnlace.MAbono_tarjeta_cred);
		     }else{
		    	 session.setModuloConsultar(IEnlace.MCredito_en_linea);
		     }
		   }

		   // Cuenta|Descripcion|tipoRelacion|tipoCuenta| @
		   String varSel=(String)req.getParameter("radTabla");
		   String varFac=(String)req.getParameter("FacArchivo");
		   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Seleccion: "+varSel, EIGlobal.NivelLog.INFO);
		   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Facultades: "+varFac, EIGlobal.NivelLog.INFO);

		   varSel=(varSel==null)?"":varSel;
		   varFac=(varFac==null)?"":varFac;

		   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Armando objetos.", EIGlobal.NivelLog.DEBUG);
		   Seleccion.iniciaObjeto(varSel);
		   FacArchivo.iniciaObjeto(';','@',varFac);

		   if(Seleccion.totalRegistros>=1){
			   tipoCuenta=Seleccion.camposTabla[0][3];
		   }
		   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): tipo Cuenta Encontrada: "+tipoCuenta, EIGlobal.NivelLog.INFO);
		   if(importeDe==null||"".equals(importeDe.trim())){
			   importeDe="0.00";
		   }
		   if(importeA==null||"".equals(importeA.trim())){
			   importeA="0.00";
		   }
			EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): El importe de: " + importeDe, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): El importe a:"+importeA, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): El tipoCuenta "+tipoCuenta, EIGlobal.NivelLog.INFO);

			//MCL_Movimientos?Tipo=1&Cuenta=5&Modulo=0 //Tarjetas
			//MCL_Movimientos?Tipo=2&Cuenta=0&Modulo=0 //Lineas
			if("0".equals(modulo))
			 {
			   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Consulta de movimientos sin cuenta. "+tipoCuenta, EIGlobal.NivelLog.INFO);
			   if("5".equals(tipoCuenta))
				{
				  titulo1="Consultas de Movimientos de Tarjeta de Cr&eacute;dito";
				  titulo2="Consultas &gt Movimientos &gt Tarjeta de Cr&eacute;dito";
				  arcAyuda="s25230h";
				}
			   else
				{
				  titulo1="Consultas de Movimientos de L&iacute;nea de Cr&eacute;dito";
				  titulo2="Tesorer&iacute;a &gt Cr&eacute;dito &gt Cr&eacute;dito en L&iacute;nea &gt Consultas &gt L&iacute;nea de Cr&eacute;dito";
				  arcAyuda="s26290h";
				}
			 }
			else
			 {
			   EIGlobal.mensajePorTrace("MCL_Movimientos - execute(): Consulta de movimientos seleccionando cuenta. "+tipoCuenta+ " campos "+Seleccion.camposTabla[0][3], EIGlobal.NivelLog.INFO);
			   if("5".equals(Seleccion.camposTabla[0][3]) ||  "5".equals(tipoCuenta) )
				{
				  titulo1="Consultas de Movimientos de Tarjeta de Cr&eacute;dito";
				  titulo2="Consultas &gt Movimientos &gt Tarjeta de Cr&eacute;dito";
				  arcAyuda="s25230h";
				}
			   else
				{
				  titulo1="Consultas de Movimientos de L&iacute;nea de Cr&eacute;dito";
				  titulo2="Tesorer&iacute;a &gt Cr&eacute;dito &gt Cr&eacute;dito en L&iacute;nea &gt Consultas &gt L&iacute;nea de Cr&eacute;dito";
				  arcAyuda="s26290h";
				}
			 }

		   if("true".equals(facConMov.trim()))
			{
			  if("0".equals(modulo)){
				//BIT CU1121, CU3071 inicia flujo, CU3051 continua flujo por movimientos, CU3061 continua flujo por movimientos
					/**
					 * 09/01/07
					 * 15/Enero/2007	Modificaci�n para el modulo de Tesorer�a
					 * VSWF
					 */
				  /*
					 * VSWF-BMB-I
					 */
				  if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
				  try{
					  if(req.getParameter(BitaConstants.FLUJO)!=null){
						   bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
					   }else if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
							   equals(BitaConstants.EC_SALDO_CUENTA_TARJETA)||
							   (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
							   equals(BitaConstants.EC_POSICION_TARJETA)){
						   bh.incrementaFolioFlujo(BitaConstants.EC_MOV_TARJETA);
					   }
					  else{
						   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					   }
	      			if(
						!req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals
							  (BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)   &&
						!req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals
							  (BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)
					   ){
	      				bh.incrementaFolioFlujo(BitaConstants.EC_MOV_TARJETA);
	      			}

						if (flujo != null && flujo.equals(BitaConstants.EC_MOV_TARJETA)){
							//BIT CU1121
							bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.EC_MOV_TARJETA_ENTRA);
							bt.setContrato((contrato == null)?" ":contrato);
							bt.setCctaOrig((varSel == null)?" ":varSel);
						}
						/*
						 * VSWF-BMB-F
						 */

						else {
							/**

							 * VSWF - FVC - I
							 */
								if (((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
										equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)){
									bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
									bt = (BitaTransacBean)bh.llenarBean(bt);
									bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_ENTRA);
									bt.setContrato((contrato == null)?" ":contrato);
								}
								if (req.getParameter(BitaConstants.FLUJO) != null && ((String)req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)){
									bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
									bt = (BitaTransacBean)bh.llenarBean(bt);
									bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_ENTRA);
									bt.setContrato((contrato == null)?" ":contrato);
								}
								else{
									if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != null && (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
										bt = (BitaTransacBean)bh.llenarBean(bt);
										bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_ENTRA);
										bt.setContrato((contrato == null)?" ":contrato);
									}
									else{
										if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != null && (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)){
											bt = (BitaTransacBean)bh.llenarBean(bt);
											bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION);
											bt.setContrato((contrato == null)?" ":contrato);
										}
									}
								}
								/**
								 * VSWF - FVC - F
								 */
							}
						 BitaHandler.getInstance().insertBitaTransac(bt);
				  }catch(SQLException e){
  					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
				  }
					 /**
					     * VSWF
					     */


				iniciaMovimientos(FacArchivo,Seleccion,tipoCuenta,titulo1,titulo2,arcAyuda,tipoModulo,usuario, req, res );
			  }else{
				 if("5".equals(Seleccion.camposTabla[0][3])){
					 generaTablaMovimientosTarjeta(cveMov,referencia,importeDe,importeA,FacArchivo,Seleccion, titulo1,titulo2,arcAyuda,req, res, contrato, usuario ,clavePerfil,tipoModulo);
				 }else{
					 generaTablaMovimientosLinea(cveMov,referencia,importeDe,importeA,FacArchivo,Seleccion,titulo1,titulo2,arcAyuda,usuario,contrato,clavePerfil,tipoModulo, req, res );
				 }
			   }
			}else{
				despliegaPaginaError( "No tiene facultad para revisar Movimientos.",titulo1, titulo2,arcAyuda, req, res );
			}
		 }else if(sesionvalida){
			req.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		 }
		EIGlobal.mensajePorTrace( "MCL_Movimientos - execute(): Saliendo de Consulta Movtos para Pos-Linea.", EIGlobal.NivelLog.DEBUG);
	}

/**
 * **********************************************************************************.
 *
 * @param FacArchivo El objeto: fac archivo
 * @param Seleccion El objeto: seleccion
 * @param tipoCuenta El objeto: tipo cuenta
 * @param titulo1 El objeto: titulo1
 * @param titulo2 El objeto: titulo2
 * @param arcAyuda El objeto: arc ayuda
 * @param tipoModulo El objeto: tipo modulo
 * @param usuario El objeto: usuario
 * @param req El objeto: req
 * @param res El objeto: res
 * @return Objeto int
 * @throws IOException Una excepcion de I/O ha ocurrido.
 * @throws ServletException La servlet exception
 */
/************************************************** Inicio de Movimientos Modulo=0   */
/*************************************************************************************/
	public int iniciaMovimientos(EI_Tipo FacArchivo,EI_Tipo Seleccion,String tipoCuenta,String titulo1,String titulo2, String arcAyuda, String tipoModulo,String  usuario, HttpServletRequest req, HttpServletResponse res )
			throws IOException, ServletException {
		StringBuffer varFechaHoy  = new StringBuffer("");
		String cuenta       = "";
		String credito      = "";
		String descripcion  = "";
		String strCombo     = "";
		String strValue     = "";
		String cadena1="";
		String cadena2="";
		String tipoRelacion = "No Registrada.";
		String strInhabiles = armaDiasInhabilesJS();

		EIGlobal.mensajePorTrace( "MCL_Movimientos - iniciaMovimientos(): Iniciando movimientos..", EIGlobal.NivelLog.DEBUG);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		if(Seleccion.totalRegistros>0)
		 {
			cuenta = Seleccion.camposTabla[0][0];
			descripcion = Seleccion.camposTabla[0][1];
			credito = Seleccion.camposTabla[0][3];
			tipoRelacion=Seleccion.camposTabla[0][2];

			/* incidencia 518857->13872(facultades consulta de movimientos de tarjeta)
			if(Seleccion.camposTabla[0][2].trim().equals("P"))
			  tipoRelacion="Propia";
			if(Seleccion.camposTabla[0][2].trim().equals("T"))
			  tipoRelacion="Terceros";
			if(Seleccion.camposTabla[0][2].trim().equals("!"))
			  tipoRelacion="No Registrada";
			*/

			cadena1=cuenta + "|"+descripcion+"|"+tipoRelacion+"|"+"5|TARJETA DE CREDITO|"+descripcion+"| @";
			cadena2=cuenta + "|"+descripcion+"|"+tipoRelacion+"|"+"0|LINEA DE CREDITO|"+descripcion+"| @";
		 }

		req.setAttribute("CuentaText", cuenta + " " + descripcion);
		if("5".equals(tipoCuenta.trim())){
			req.setAttribute("CuentaHidden",cadena1);
		}else{
			req.setAttribute("CuentaHidden",cadena2);
		}

		varFechaHoy.append("  //Fecha De Hoy");
		varFechaHoy.append("\n  dia=new Array("+formateaFecha("DIA")+","+formateaFecha("DIA")+");");
		varFechaHoy.append("\n  mes=new Array("+formateaFecha("MES")+","+formateaFecha("MES")+");");
		varFechaHoy.append("\n  anio=new Array("+formateaFecha("ANIO")+","+formateaFecha("ANIO")+");");

		varFechaHoy.append("\n\n  //Fechas Seleccionadas");
		varFechaHoy.append("\n  dia1=new Array("+formateaFecha("DIA")+","+formateaFecha("DIA")+");");
		varFechaHoy.append("\n  mes1=new Array("+formateaFecha("MES")+","+formateaFecha("MES")+");");
		varFechaHoy.append("\n  anio1=new Array("+formateaFecha("ANIO")+","+formateaFecha("ANIO")+");");
		varFechaHoy.append("\n  Fecha=new Array('"+formateaFecha("FECHA")+"','"+formateaFecha("FECHA")+"');");

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute("DiasInhabiles",strInhabiles);

		req.setAttribute("FacArchivo",FacArchivo.strOriginal);
		req.setAttribute("radTabla",Seleccion.strOriginal);
		req.setAttribute("FechaAyer",formateaFecha("FECHA"));
		req.setAttribute("FechaPrimero","01/"+formateaFecha("MES2")+"/"+formateaFecha("ANIO"));
		req.setAttribute("VarFechaHoy",varFechaHoy.toString());
		req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		req.setAttribute("Tipo",tipoModulo);
		req.setAttribute("FechaDia",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado(titulo1,titulo2,arcAyuda,req));

		evalTemplate("/jsp/MCL_Movimientos.jsp", req, res);
		return 1;
	}

/**
 * **********************************************************************************.
 *
 * @param cveMov El objeto: cve mov
 * @param referencia El objeto: referencia
 * @param importeDe El objeto: importe de
 * @param importeA El objeto: importe a
 * @param FacArchivo El objeto: fac archivo
 * @param Seleccion El objeto: seleccion
 * @param titulo1 El objeto: titulo1
 * @param titulo2 El objeto: titulo2
 * @param arcAyuda El objeto: arc ayuda
 * @param req El objeto: req
 * @param res El objeto: res
 * @param contrato El objeto: contrato
 * @param usuario El objeto: usuario
 * @param clavePerfil El objeto: clave perfil
 * @param tipoModulo El objeto: tipo modulo
 * @return Objeto int
 * @throws IOException Una excepcion de I/O ha ocurrido.
 * @throws ServletException La servlet exception
 */
/************************************************ tabla Movimientos Tarjeta Modulo=1 */
/*************************************************************************************/
	public int generaTablaMovimientosTarjeta(String cveMov,String referencia,String importeDe,String importeA,EI_Tipo FacArchivo,EI_Tipo Seleccion,String titulo1,String titulo2,String arcAyuda, HttpServletRequest req, HttpServletResponse res, String contrato, String usuario, String clavePerfil, String tipoModulo)
			throws IOException, ServletException{

		String tipoBusqueda=( String ) req.getParameter("Busqueda");
		String strTabla     = "";
		String exportar     = "";
		StringBuilder opcionesExportar     = new StringBuilder();
		String tmpFecha     = "";
		String fechaA       = "";
		String fechaB       = "";
		String strMov       = "";
		String[] titulos    = new String[10];

		String Result		= "";
		String tipoRelacion	= "";
		String nombreArchivo ="";
		//VSWF
		String salImporte="";
		String numReferencia="";

		StringBuffer Trama;

		StringBuffer fechaTarjeta;
		StringBuffer movStrOriginal= new StringBuffer("");

		Hashtable hs   = null;

		int[] datos = new int[12];
		int[] align = new int[12];
		int[] value = {1,1};

		int numMovCargo		   = 0;
		int numMovAbono        = 0;

		float impCargoTotal    = 0;
		float impAbonoTotal    = 0;

		boolean facultades = true;

		EI_Tipo Mov            = new EI_Tipo(this);
		EI_Tipo MovHisExp      = new EI_Tipo(this);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Tabla de movimientos (Tarjeta).", EIGlobal.NivelLog.DEBUG);

		/*
		0 tmp
		1 signo
		2 fecha
		3 desc
		4 emisora
		5 no. ref
		6 importe
		7 suc
		8 hora
		9 saldo
		10 concepto
		*/

		if("HIS".equals(tipoBusqueda.trim()))
		 {
			EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Movimientos Historicos.", EIGlobal.NivelLog.DEBUG);
            arcAyuda="s25260h";
			titulos[0] = "";
			titulos[1] = "Fecha";
			titulos[2] = "Hora";
			titulos[3] = "Suc.";
			titulos[4] = "Descripci&oacute;n";
			titulos[5] = "Cargo";
			titulos[6] = "Abono";
			titulos[7] = "Saldo";
			titulos[8] = "Referencia";
			titulos[9] = "Concepto";

			datos[0] = 9;
			datos[1] = 0;
			datos[2] = 2;
			datos[3] = 8;
			datos[4] = 7;
			datos[5] = 3;
			datos[6] = 0;
			datos[7] = 6;
			datos[8] = 9;
			datos[9] = 5;
			datos[10] = 10;
			datos[11] = 10;

			align[0] = 1;
			align[1] = 1;
			align[2] = 1;
			align[3] = 0;
			align[4] = 2;
			align[5] = 2;
			align[6] = 2;
			align[7] = 1;
			align[8] = 0;
			align[9] = 1;

			fechaA = (( String ) req.getParameter("FechaIni")).trim();
			fechaB = (( String ) req.getParameter("FechaFin")).trim();
			fechaTarjeta=new StringBuffer(fechaA.substring(6,fechaA.length()));
			fechaTarjeta.append(fechaA.substring(3,5));
			fechaTarjeta.append(fechaA.substring(0,2));
			fechaTarjeta.append("|");
			fechaTarjeta.append(fechaB.substring(6,fechaB.length()));
			fechaTarjeta.append(fechaB.substring(3,5));
			fechaTarjeta.append(fechaB.substring(0,2));
			strMov = "hist�ricos";
		}
	   else
		{
			EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Movimientos del Dia.", EIGlobal.NivelLog.DEBUG);
			arcAyuda="s25250h";
			titulos[0] = "";
			titulos[1] = "Hora";
			titulos[2] = "Suc.";
			titulos[3] = "Descripci&oacute;n";
			titulos[4] = "Cargo";
			titulos[5] = "Abono";
			titulos[6] = "Saldo";
			titulos[7] = "Referencia";
			titulos[8] = "Concepto";
			titulos[9] = "";

			align[0] = 1;
			align[1] = 1;
			align[2] = 0;
			align[3] = 2;
			align[4] = 2;
			align[5] = 2;
			align[6] = 1;
			align[7] = 0;
			align[8] = 1;
			datos[0] = 8;
			datos[1] = 0;

			datos[2] = 8;
			datos[3] = 7;
			datos[4] = 3;
			datos[5] = 0;
			datos[6] = 6;
			datos[7] = 9;
			datos[8] = 5;
			datos[9] = 10;
			fechaTarjeta = new StringBuffer(EnlaceGlobal.fechaHoy("aammdd|aammdd"));
			strMov = "del d�a";
		}

	   tipoRelacion=Seleccion.camposTabla[0][2];
	   if( ("P".equals(Seleccion.camposTabla[0][2].trim()) && tieneFacultad(FacArchivo,tipoRelacion,Seleccion.camposTabla[0][0],"CONSMOVTOS",req)) ||
		   ("T".equals(Seleccion.camposTabla[0][2].trim()) && tieneFacultad(FacArchivo,tipoRelacion,Seleccion.camposTabla[0][0],"CONSMOVTOTER",req)) )
		{

		  EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Armando Trama.,", EIGlobal.NivelLog.DEBUG);
		  Trama=new StringBuffer("");
          Trama.append(IEnlace.medioEntrega2);
		  Trama.append(pipe);
          Trama.append(usuario);
		  Trama.append(pipe);
          Trama.append("MOVT");
		  Trama.append(pipe);
          Trama.append(contrato);
		  Trama.append(pipe);
          Trama.append(usuario);
		  Trama.append(pipe);
          Trama.append(clavePerfil);
		  Trama.append(pipe);
          Trama.append(Seleccion.camposTabla[0][0]);
		  Trama.append(pipe);
          Trama.append(Seleccion.camposTabla[0][2]);
		  Trama.append(pipe);
          Trama.append(fechaTarjeta.toString());

		  EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Trama entrada: "+Trama.toString(), EIGlobal.NivelLog.DEBUG);
		  ServicioTux TuxGlobal = new ServicioTux();
		  //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		  //########## Cambiar ....
		  try{
			   hs = TuxGlobal.web_red(Trama.toString());
			}catch( java.rmi.RemoteException re ){
				re.printStackTrace();
			} catch (Exception e) {}
		   if(hs!=null){
			  Result = (String) hs.get("BUFFER");
		  }

		   EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
		   if(Result==null || Result.equals("null")){
			   movStrOriginal.append("FAIL");
		   }else{
			   movStrOriginal.append(recuperaMovimientosTarjeta(cveMov,referencia,importeDe,importeA,usuario,tipoModulo,req));
		   }
		   EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Mov.strOriginal: "+movStrOriginal.toString(), EIGlobal.NivelLog.INFO);
		}else{
			facultades = false;
		}

	  //Registros para la exportacion...
	  MovHisExp.iniciaObjeto(';','@',movStrOriginal.toString());
	  Mov.iniciaObjeto(';','@',movStrOriginal.toString());

	  if(!facultades){
		  despliegaPaginaError("No tiene facultad para revisar movimientos.",titulo1,titulo2,arcAyuda,req,res);
	  }else if(Mov.totalRegistros<=0 && !movStrOriginal.toString().trim().equals("FAIL")){
		  despliegaPaginaError("No hay movimientos realizados en este periodo.",titulo1,titulo2,arcAyuda,req,res);
	  }else if(movStrOriginal.toString().trim().equals("FAIL")){
		  despliegaPaginaError("Su transacci&oacute;n no puede ser atendida en este momento.",titulo1,titulo2,arcAyuda,req,res);
	  }else{
		  String[] fechasAplicacion = new String[Mov.totalRegistros];//VSWF para bitacora
		  for(int i=0;i<Mov.totalRegistros;i++)
           {
             MovHisExp.camposTabla[i][0]=Seleccion.camposTabla[0][0];
             tmpFecha=Mov.camposTabla[i][2].trim();
             Mov.camposTabla[i][2]=tmpFecha.substring(0,2)+"/"+tmpFecha.substring(2,4)+"/"+tmpFecha.substring(4,tmpFecha.length());
             fechasAplicacion[i] = Mov.camposTabla[i][2]; //VSWF para bitacora

             if(Mov.camposTabla[i][1].trim().equals("+"))
			  {
				numMovAbono++;
                Mov.camposTabla[i][0]="0.00";
				impAbonoTotal+=new Float(Mov.camposTabla[i][6]).floatValue();
			  }
             else
              {
				numMovCargo++;
                Mov.camposTabla[i][0]=Mov.camposTabla[i][6];
                Mov.camposTabla[i][6]="0.00";
				impCargoTotal+=new Float(Mov.camposTabla[i][0]).floatValue();
              }
           }

		  EnlaceGlobal.formateaImporte(Mov,9,100);
		  EnlaceGlobal.formateaImporte(Mov,6,100);
		  EnlaceGlobal.formateaImporte(Mov,0,100);
          strTabla=Mov.generaTabla(titulos,datos,value,align);
          EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Tabla generada.", EIGlobal.NivelLog.DEBUG);

/*************************************** Generar Archivo de Exportacion *********/
          //if(MovHisExp.totalRegistros>=1 && tipoBusqueda.trim().equals("HIS"))
          DecimalFormat formateador = (DecimalFormat) DecimalFormat.getInstance();
          formateador.applyPattern(".00");
		  if(MovHisExp.totalRegistros>=1)
           {
             EI_Exportar ArcSal;

             //String nombreArchivo=usuario+".doc"; //09/01/07
             nombreArchivo=usuario+".doc";

             ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
             if(ArcSal.creaArchivo())
              {
                int c[]={0,2,8,7,3,1,6,9,10};        // Indice del elemento deseado
                int b[]={16,8,4,3,40,1,14,14,8,40};  // TODO Longitud maxima del campo para el elemento POSICIONES PARA EL ARCHIVO

				for(int i=0;i<Mov.totalRegistros;i++)
                 {
                   StringBuffer arcLinea=new StringBuffer("");
                   for(int j=0;j<9;j++){  //-------------------------
                	   if(j==6){
                		   arcLinea.append(ArcSal.formateaCampo( (formateador.format(new Double(MovHisExp.camposTabla[i][c[j]]).doubleValue()/100)) ,b[j]));
                	   }else{
                		   arcLinea.append(ArcSal.formateaCampo(MovHisExp.camposTabla[i][c[j]],b[j]));
                	   }
                   }                      //--------------------------
                   arcLinea.append("\n");
                   if(!ArcSal.escribeLinea(arcLinea.toString())){
                     EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): Algo salio mal escribiendo: "+arcLinea.toString(), EIGlobal.NivelLog.INFO);
                   }
                   else{//se agrego este else para la bitacora, si se pudo obtener los datos se bitacorizara
                	/**
           			 * VSWF - FVC - I
           			 *
           			 * 15/Enero/2007
           			 */
           			//CU3071, CU3051 continuaci�n, CU3061 continuaci�n
                 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
           			try {
           				BitaHelper bh = new BitaHelperImpl(req, session, sess);
           				BitaTransacBean bt = new BitaTransacBean();
           				bt = (BitaTransacBean)bh.llenarBean(bt);
           				/*BMB-I*/
           				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
           						equals(BitaConstants.EC_MOV_TARJETA)){
           					bt.setNumBit(BitaConstants.EC_MOV_TARJETA_CONSULTA_MOVS_TARJETA);
           				}
           				/*BMB-F*/
           				else if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
           						equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)){
           					bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_TAR);
           					}else{
           					if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
           						bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_TAR);
           					}else{
           						if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)){
           							bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_TAR);
           						}
           					}
           				}
           				/*BMB-I*/
           				salImporte=MovHisExp.camposTabla[i][7];
           				numReferencia=MovHisExp.camposTabla[i][8];
           				if(salImporte==null||salImporte.equals("")){
           					salImporte="0.00";
           				}
           				if(numReferencia==null ||numReferencia.equals("")){
           					numReferencia="0";
           				}
           				bt.setContrato((contrato == null)?" ":contrato);
           				bt.setCctaOrig((Seleccion.camposTabla[0][0] == null)?" ":Seleccion.camposTabla[0][0]);
           				bt.setImporte(Double.parseDouble((salImporte == null)?"0":salImporte));
           				bt.setReferencia(Long.parseLong((numReferencia == null)?"0":numReferencia));
           				bt.setFechaAplicacion((fechasAplicacion == null)?new Date():Utilerias.MdyToDate(fechasAplicacion[i]));
           				bt.setServTransTux("MOVT");
           				bt.setNombreArchivo((nombreArchivo == null)?" ":nombreArchivo);
           				if(Result != null){
           					if(Result.substring(0,2).equals("OK")){
           						bt.setIdErr("MOVT0000");
           					}else if (Result.length()>8){
	           				    bt.setIdErr(Result.substring(0,8));
	           				}else{
	           				    bt.setIdErr(Result.trim());
	           				}
           				}
           				/*BMB-F*/
           				BitaHandler.getInstance().insertBitaTransac(bt);
           			}catch(NumberFormatException e){
           				e.printStackTrace();
           			} catch (SQLException e) {
           				e.printStackTrace();
           			} catch (Exception e) {
           				e.printStackTrace();
           			}
           			/**
           			 * VSWF - FVC - F
           			 */
                   }
                   }
                 }
				/*if( req.getAttribute("_MovTasaAnual")!=null )
				{
					String dato = (String)req.getAttribute("_MovTasaAnual");
					ArcSal.escribeLinea("Tasa de Inter�s Anualizada que se cobra sobre el saldo promedio insoluto del periodo anterior. :"+dato.substring(dato.indexOf(":")+1) );
				}*/
                ArcSal.cierraArchivo();

                // Boton de Exportacion de Archivo.
				//*************************************************************
				ArchivoRemoto archR = new ArchivoRemoto();
				if(archR.copiaLocalARemoto(nombreArchivo,"WEB")){
					//exportar="<a id=\"bExport\"  href='/Download/"+nombreArchivo+"'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";//TODO AGREGAR RADIO BUTTONS
					exportar="<a id=\"bExport\" href=\"javascript:exportarMovimientos();\"><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
					opcionesExportar.append( "<a style=\"cursor: pointer;\" class='tabmovtex11' onclick=\"cambiaArchivoExport('txt');\">Exporta en TXT <input id=\"extTxt\" type=\"radio\" name=\"tipoExportacion\"></a>\n");
					opcionesExportar.append( "<br><a style=\"cursor: pointer;\" class='tabmovtex11' onclick=\"cambiaArchivoExport('csv');\">Exporta en XLS <input id=\"extCsv\" type=\"radio\" name=\"tipoExportacion\" checked='true'></a><br><br>\n");
					opcionesExportar.append( "<input id=\"tipoArchivoExportacion\" name=\"tipoArchivoExportacion\" type=\"hidden\" value='csv'/>\n");
				}
              }
             else
              {
                //Exportar="<b>Exportacion no disponible<b>";
                EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosTarjeta(): No se pudo crear archivo de exportacion.", EIGlobal.NivelLog.DEBUG);
              }
           }


          /**
           * VSWF - FVC - I
           */
          if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
          if(!tipoBusqueda.trim().equals("HIS"))
	          for(int i=0;i<Mov.totalRegistros;i++)
	          {
	        	  //CU3071, CU3051 continuaci�n, CU3061 continuaci�n
	        	  try {
	    			BitaHelper bh = new BitaHelperImpl(req, session, sess);
	    			BitaTransacBean bt = new BitaTransacBean();
	    			bt = (BitaTransacBean)bh.llenarBean(bt);
	    			/*BMB-I*/
       				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
       						equals(BitaConstants.EC_MOV_TARJETA)){
       					bt.setNumBit(BitaConstants.EC_MOV_TARJETA_CONSULTA_MOVS_TARJETA);
       				}
       				/*BMB-F*/
	    			else if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)){
	    				bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_TAR);
	    			}
	    			else{
	    				if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
	    					bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_TAR);
	    				}
	    				else{
	    					if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)){
	    						bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_TAR);
	    					}
	    				}
	    			}
       				/*BMB-I*/
	    			salImporte=MovHisExp.camposTabla[i][6];
       				numReferencia=MovHisExp.camposTabla[i][7];
       				if(salImporte==null||salImporte.equals("")){
       					salImporte="0.00";
       				}
       				if(numReferencia==null||numReferencia.equals("")){
       					numReferencia="0";
       				}
       				bt.setContrato((contrato == null)?" ":contrato);
       				bt.setCctaOrig((Seleccion.camposTabla[0][0] == null)?" ":Seleccion.camposTabla[0][0]);
       				bt.setImporte(Double.parseDouble((salImporte == null)?"0":salImporte));
       				bt.setReferencia(Long.parseLong((numReferencia == null)?"0":numReferencia));
       				bt.setFechaAplicacion((fechasAplicacion[i] == null)?new Date():Utilerias.MdyToDate(fechasAplicacion[i]));
       				bt.setServTransTux("MOVT");
       				bt.setNombreArchivo((nombreArchivo == null)?" ":nombreArchivo);
       				if(Result != null){
       					if(Result.substring(0,2).equals("OK")){
       						bt.setIdErr("MOVT0000");
       					}else if (Result.length()>8){
	       				    bt.setIdErr(Result.substring(0,8));
	       				}else{
	       				    bt.setIdErr(Result.trim());
	       				}
       				}
       				/*BMB-F*/
	    			BitaHandler.getInstance().insertBitaTransac(bt);
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
	           }//fin for
          }
          /**
           * VSWF - FVC - F
           */

          /*************************************** Fin de Exportacion de Archivo *********/
		    if(numMovCargo>0)
				req.setAttribute("numMovCargo","Total de cargos: "+Integer.toString(numMovCargo)+" por: "+FormatoMoneda(Float.toString(impCargoTotal/100)));
			else
				req.setAttribute("numMovCargo","Total de cargos: 0");

			if(numMovAbono>0)
				req.setAttribute("numMovAbono","Total de abonos: "+Integer.toString(numMovAbono)+" por: "+FormatoMoneda(Float.toString(impAbonoTotal/100)));
			else
				req.setAttribute("numMovAbono","Total de abonos: 0");

			req.setAttribute("Cuenta","No. de Cr&eacute;dito: "+Seleccion.camposTabla[0][0]);//+" "+Seleccion.camposTabla[0][1]);
			if(tipoBusqueda.trim().equals("HIS"))
				req.setAttribute("Fecha","Periodo del "+fechaA+" al "+fechaB);
			else
				req.setAttribute("Fecha","Fecha: "+EnlaceGlobal.fechaHoy("dd/mm/aaaa"));

			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

			if("".equals(exportar.trim()))
			  exportar="";

			req.setAttribute("opcionesExportar",opcionesExportar.toString());
			req.setAttribute("Tipo",tipoModulo);
			req.setAttribute("Busqueda",tipoBusqueda.trim());
			req.setAttribute("Exportar",exportar);
			req.setAttribute("Tabla",strTabla);
			req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));

			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado(titulo1,titulo2,arcAyuda,req));

			evalTemplate("/jsp/MCL_TablaMov.jsp",req, res);
		}
	  return 1;
	}

/**
 * **********************************************************************************.
 *
 * @param cveMov El objeto: cve mov
 * @param referencia El objeto: referencia
 * @param importeDe El objeto: importe de
 * @param importeA El objeto: importe a
 * @param FacArchivo El objeto: fac archivo
 * @param Seleccion El objeto: seleccion
 * @param titulo1 El objeto: titulo1
 * @param titulo2 El objeto: titulo2
 * @param arcAyuda El objeto: arc ayuda
 * @param usuario El objeto: usuario
 * @param contrato El objeto: contrato
 * @param clavePerfil El objeto: clave perfil
 * @param tipoModulo El objeto: tipo modulo
 * @param req El objeto: req
 * @param res El objeto: res
 * @return Objeto int
 * @throws IOException Una excepcion de I/O ha ocurrido.
 * @throws ServletException La servlet exception
 */
/************************************************ tabla Movimientos Linea Modulo=0   */
/*************************************************************************************/
	public int generaTablaMovimientosLinea(String cveMov,String referencia,String importeDe,String importeA,EI_Tipo FacArchivo,EI_Tipo Seleccion,String titulo1,String titulo2,String arcAyuda,String usuario, String contrato, String clavePerfil, String tipoModulo, HttpServletRequest req, HttpServletResponse res )
			throws IOException, ServletException {
		String tipoBusqueda = ( String ) req.getParameter("Busqueda");
		String strTabla   = "";
		String tmpFecha   = "";
		String fechaA     = "";
		String fechaB     = "";
		String strMov     = "";

		String Result     = "";
		String tipoRelacion= "";

		Hashtable hs   = null;

		String[] titulos	= new String[10];

		StringBuffer Trama;
		StringBuffer fechaLinea;
		StringBuffer movStrOriginal= new StringBuffer("");

		boolean Registros	= false;
		boolean facultades	= true;
		boolean chupes		= true;

		int[] datos = new int[12];
		int[] align = new int[12];
		int[] value = {1,1};

		int numMovCargo		= 0;
		int numMovAbono     = 0;

		float impCargoTotal    = 0;
		float impAbonoTotal    = 0;

		EI_Tipo LineaTmp	= new EI_Tipo(this);
		EI_Tipo Mov         = new EI_Tipo(this);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): Tabla de movimientos (Linea).", EIGlobal.NivelLog.DEBUG);

		/*
		0 tmp
		1 signo
		2 fecha
		3 desc
		4 emisora
		5 no. ref
		6 importe
		7 suc
		8 hora
		9 saldo
		10 concepto
		*/

		if(tipoBusqueda.trim().equals("HIS"))
		 {
			EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): Movimientos Historicos.", EIGlobal.NivelLog.DEBUG);
			titulos[0] = "";
			titulos[1] = "Fecha";
			titulos[2] = "Descripci&oacute;n";
			titulos[3] = "Cargo";
			titulos[4] = "Abono";
			titulos[5] = "Saldo";
			titulos[6] = "Referencia";
			titulos[7] = "Concepto";

			datos[0] = 6;
			datos[1] = 0;

			datos[2] = 0;
			datos[3] = 2;
			datos[4] = 3;
			datos[5] = 4;
			datos[6] = 5;
			datos[7] = 1;
			datos[8] = 2;

			align[0] = 1;
			align[1] = 0;
			align[2] = 2;
			align[3] = 2;
			align[4] = 2;
			align[5] = 1;
			align[6] = 1;

			fechaA = (( String ) req.getParameter("FechaIni")).trim();
			fechaB = (( String ) req.getParameter("FechaFin")).trim();
			fechaLinea = new StringBuffer(fechaA.substring(6,fechaA.length()));
			fechaLinea.append("-");
			fechaLinea.append(fechaA.substring(3,5));
			fechaLinea.append("-");
			fechaLinea.append(fechaA.substring(0,2));
			fechaLinea.append("@");
			fechaLinea.append(fechaB.substring(6,fechaB.length()));
			fechaLinea.append("-");
			fechaLinea.append(fechaB.substring(3,5));
			fechaLinea.append("-");
			fechaLinea.append(fechaB.substring(0,2));
			fechaLinea.append("@");
			strMov = "hist�ricos";
		}
	   else
		 {
			EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): Movimientos del Dia.", EIGlobal.NivelLog.DEBUG);
			titulos[0] = "";
			titulos[1] = "Descripci&oacute;n";
			titulos[2] = "Cargo";
			titulos[3] = "Abono";
			titulos[4] = "Saldo";
			titulos[5] = "Referencia";
			titulos[6] = "Concepto";

			datos[0] = 5;
			datos[1] = 0;

			datos[2] = 2;
			datos[3] = 3;
			datos[4] = 4;
			datos[5] = 5;
			datos[6] = 1;
			datos[7] = 2;

			align[0] = 0;
			align[1] = 2;
			align[2] = 2;
			align[3] = 2;
			align[4] = 1;
			align[5] = 1;

			fechaLinea = new StringBuffer (EnlaceGlobal.fechaHoy("aaaa-mm-dd@"));
			fechaLinea.append(EnlaceGlobal.fechaHoy("aaaa-mm-dd@"));
			strMov = "del d�a";
		}

	   tipoRelacion=Seleccion.camposTabla[0][2];
       if(tieneFacultad(FacArchivo,tipoRelacion,Seleccion.camposTabla[0][1].trim(),"TECREMOVTOSC",req))
        {
		  EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): armando Trama.", EIGlobal.NivelLog.DEBUG);
          Trama=new StringBuffer("");
          Trama.append(IEnlace.medioEntrega2);
		  Trama.append(pipe);
          Trama.append(usuario);
		  Trama.append(pipe);
          Trama.append("RG02");
		  Trama.append(pipe);
          Trama.append(contrato);
		  Trama.append(pipe);
          Trama.append(usuario);
		  Trama.append(pipe);
          Trama.append(clavePerfil);
		  Trama.append(pipe);
          Trama.append(Seleccion.camposTabla[0][0].substring(0,2) );
		  Trama.append("@");
          Trama.append(Seleccion.camposTabla[0][0].substring(2,10) );
		  Trama.append("@");
          Trama.append(Seleccion.camposTabla[0][0].substring(10,11) );
		  Trama.append("@");
          Trama.append(fechaLinea.toString());

          EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): Trama entrada: "+Trama.toString(), EIGlobal.NivelLog.DEBUG);

			//########## Cambiar ....
			ServicioTux TuxGlobal = new ServicioTux();
			//TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			try{
				hs = TuxGlobal.web_red(Trama.toString());
			}catch( java.rmi.RemoteException re ){
				re.printStackTrace();
			} catch(Exception e) {}
			if(hs != null){
				Result = (String) hs.get("BUFFER");
			}

		  EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

		  if(Result==null || Result.equals("null"))
			LineaTmp.strOriginal="OPENFAIL";
		  else
			LineaTmp.strOriginal=recuperaMovimientosLinea(usuario);
		  //EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): LineaTmp.strOriginal: "+LineaTmp.strOriginal, EIGlobal.NivelLog.INFO);
          if(LineaTmp.strOriginal.trim().equals("NOHAY"))
           chupes=true;
          else
           if(LineaTmp.strOriginal.equals("OPENFAIL") || LineaTmp.strOriginal.substring(0,5).equals("ERROR") || !LineaTmp.strOriginal.substring(0,8).equals("RGOS0000") )
            chupes=false;
          else
           {
             LineaTmp.iniciaObjeto(LineaTmp.strOriginal+"@");
             float impFormaDe=new Float(importeDe).floatValue();
             float impFormaA=new Float(importeA).floatValue();
             float impCargo=0;
             float impAbono=0;

			 for(int i=0;i<Integer.parseInt(LineaTmp.camposTabla[0][1]);i++)
              {
                Registros=false;

                if(referencia.trim().length()>0)
                 {
				   EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): Referencia ["+referencia+"] En tabla ["+LineaTmp.camposTabla[0][3+(i*6)].trim()+"]", EIGlobal.NivelLog.DEBUG);
                   if(referencia.trim().equals(LineaTmp.camposTabla[0][3+(i*6)].trim()))
					{
					  impCargoTotal=new Float(LineaTmp.camposTabla[0][5+(i*6)]).floatValue();
					  impAbonoTotal=new Float(LineaTmp.camposTabla[0][6+(i*6)]).floatValue();
					  if(impCargoTotal>0)
						numMovCargo=1;
					  if(impAbonoTotal>0)
						numMovAbono=1;

					  for(int j=0;j<6;j++)
					   {
						 movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
						 movStrOriginal.append("|");
						 Registros=true;
					   }
					}
                 }
                else
                 {
                   //Cargo 1 (cveMov) ...
                   //Abono 0 (cveMov) ...
                   //Ambos 2 (cveMov) ...

                   impCargo=new Float(LineaTmp.camposTabla[0][5+(i*6)]).floatValue();
                   impAbono=new Float(LineaTmp.camposTabla[0][6+(i*6)]).floatValue();

                   //if(cveMov.trim().equals("1") && !LineaTmp.camposTabla[0][5+(i*6)].trim().equals("0.00"))
                   if(cveMov.trim().equals("1") && impCargo>0) //Movimientos de Cargo
                    {
					  if(evaluarImportes(impFormaDe, impFormaA, tipoModulo)) //Se especifico importe, filtrar importes
					   {
						 if(esImporteAceptado(impFormaDe,impFormaA,impCargo,tipoModulo))
						  {
							impCargoTotal+=impCargo;
							numMovCargo++;
							for(int j=0;j<6;j++)
						     {
							   movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
							   movStrOriginal.append("|");
							   Registros=true;
							 }
						   }
					   }
					  else //No se especifico importe, traer todos
					   {
						 impCargoTotal+=impCargo;
						 numMovCargo++;
						 for(int j=0;j<6;j++)
						  {
							movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
							movStrOriginal.append("|");
							Registros=true;
						  }
					   }
                    }
                   else
                    //if(cveMov.trim().equals("0") && !LineaTmp.camposTabla[0][6+(i*6)].trim().equals("0.00"))
                    if(cveMov.trim().equals("0") && impAbono>0)  //Movimientos de Abono
                     {
					//   if(impForma>0&&impAbono==impForma)
                    if(evaluarImportes(impFormaDe, impFormaA, tipoModulo)){
                    	if(esImporteAceptado(impFormaDe,impFormaA,impAbono,tipoModulo))
						   {
							 numMovAbono++;
							 impAbonoTotal+=impAbono;
							 for(int j=0;j<6;j++)
							  {
							    movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
								movStrOriginal.append("|");
								Registros=true;
							  }
						 }
                    }else  //No se especifico importe, traer todos
						 {
							numMovAbono++;
							impAbonoTotal+=impAbono;
							for(int j=0;j<6;j++)
							 {
							   movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
							   movStrOriginal.append("|");
							   Registros=true;
							 }
						 }
                     }
                    else
                     if(cveMov.trim().equals("2"))  //Cargo y Abono
                      {
                        //if(LineaTmp.camposTabla[0][5+(i*6)].trim().equals("0.00"))
                        if(impCargo<=0)
                         {
						   if(evaluarImportes(impFormaDe,impFormaA,tipoModulo)) //Se especifico importe, filtrar importes // ESTO
							{
							  if(esImporteAceptado(impFormaDe,impFormaA,impAbono,tipoModulo)) // se modifico
							  {
								 impAbonoTotal+=impAbono;
								 numMovAbono++;
								 for(int j=0;j<6;j++)
								  {
									movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
									movStrOriginal.append("|");
									Registros=true;
								  }
							   }
							}
						   else  //No se especifico importe, traer todos
							{
							  impAbonoTotal+=impAbono;
							  numMovAbono++;
							  for(int j=0;j<6;j++)
							   {
								 movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
								 movStrOriginal.append("|");
								 Registros=true;
							   }
							}
                         }
                        else
                         {
						   if(evaluarImportes(impFormaDe,impFormaA,tipoModulo)) //Se especifico importe, filtrar importes
							{
							  if(esImporteAceptado(impFormaDe,impFormaA,impCargo,tipoModulo))
							   {
								 impCargoTotal+=impCargo;
								 numMovCargo++;
								 for(int j=0;j<6;j++)
								  {
									movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
									movStrOriginal.append("|");
									Registros=true;
								  }
							   }
							}
						   else //No se especifico importe, traer todos
							{
							  impCargoTotal+=impCargo;
							  numMovCargo++;
							  for(int j=0;j<6;j++)
							   {
								 movStrOriginal.append(LineaTmp.camposTabla[0][2+(i*6)+j]);
								 movStrOriginal.append("|");
								 Registros=true;
							   }
						    }
						 }
                      }
                 }
                if(Registros)
                  movStrOriginal.append(" @");
              }
           }
        }
       else
        facultades=false;

	   EIGlobal.mensajePorTrace("MCL_Movimientos - generaTablaMovimientosLinea(): Asignando valores al objeto Mov.", EIGlobal.NivelLog.DEBUG);
       Mov.iniciaObjeto(movStrOriginal.toString());

	   if(!facultades)
        despliegaPaginaError("No tiene facultad para revisar movimientos.",titulo1,titulo2,arcAyuda,req, res);
       else
       if(Mov.totalRegistros==0 && chupes)
        despliegaPaginaError("No existen Movimientos realizados en este periodo.",titulo1,titulo2,arcAyuda,req, res);
       else
        if(!chupes)
         despliegaPaginaError("Su transacci&oacute;n no puede ser atendida.",titulo1,titulo2,arcAyuda,req, res);
       else
         {
		   EnlaceGlobal.formateaImporte(Mov, 3);
		   EnlaceGlobal.formateaImporte(Mov, 4);
		   EnlaceGlobal.formateaImporte(Mov, 5); //VSWF - jgal (IAS tiene un 5)/ WAS un 3
           strTabla=Mov.generaTabla(titulos,datos,value,align);

			if(numMovCargo>0)
				req.setAttribute("numMovCargo","Total de cargos: "+Integer.toString(numMovCargo)+" por: "+FormatoMoneda(Float.toString(impCargoTotal)));
			else
				req.setAttribute("numMovCargo","Total de cargos: 0");

			if(numMovAbono>0)
				req.setAttribute("numMovAbono","Total de abonos: "+Integer.toString(numMovAbono)+" por: "+FormatoMoneda(Float.toString(impAbonoTotal)));
			else
				req.setAttribute("numMovAbono","Total de abonos: 0");

			req.setAttribute("Cuenta","No. de Cr&eacute;dito: "+Seleccion.camposTabla[0][0]);//+" "+Seleccion.camposTabla[0][1]);

			if(tipoBusqueda.trim().equals("HIS"))
				req.setAttribute("Fecha","Periodo del "+fechaA+" al "+fechaB);
			else
				req.setAttribute("Fecha","Fecha: "+EnlaceGlobal.fechaHoy("dd/mm/aaaa"));

			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

			req.setAttribute("Busqueda",tipoBusqueda.trim());
			req.setAttribute("Tabla",strTabla);
			req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));

			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado(titulo1,titulo2, arcAyuda,req));

			// CU3071, CU3051 continuaci�n, CU3061 continuaci�n
			/**
			 * VSWF - FVC - I
			 *
			 * 15/Enero/2007
			 */
			  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				   EIGlobal.mensajePorTrace("VSWF ENTRE A BITACORIZAR", EIGlobal.NivelLog.DEBUG);
			try {
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);

				bt.setContrato((contrato == null)?" ":contrato);
				EIGlobal.mensajePorTrace("VSWF SESSION: "+req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO), EIGlobal.NivelLog.INFO);
				if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)){
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_TAR);
					 EIGlobal.mensajePorTrace("VSWF NUmBit: "+BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_TAR , EIGlobal.NivelLog.INFO);
				}
				else{
					if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
						bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_TAR);
						 EIGlobal.mensajePorTrace("VSWF NUmBit: "+BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_TAR , EIGlobal.NivelLog.INFO);
					}
					else{
						if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)){
							bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_TAR);
							 EIGlobal.mensajePorTrace("VSWF NUmBit: "+BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_TAR , EIGlobal.NivelLog.INFO);
						}
					}
				}
				EIGlobal.mensajePorTrace("VSWF SESSION ANTES DE INSERTAR: "+req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO), EIGlobal.NivelLog.INFO);
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();FileWriter x;
			} catch (Exception e) {
				e.printStackTrace();
			}
			  }
			  EIGlobal.mensajePorTrace("SALI DE BITACORIZAR", EIGlobal.NivelLog.DEBUG);
			/**
			 * VSWF - FVC - F
			 */
			evalTemplate("/jsp/MCL_TablaMov.jsp", req, res);
		}
	  return 1;
	}

/**
 * Evaluar importes.
 *
 * @param impFormaDe El objeto: imp forma de
 * @param impFormaA El objeto: imp forma a
 * @param tipoModulo El objeto: tipo modulo
 * @return true, si exitoso
 */
private boolean evaluarImportes(float impFormaDe, float impFormaA, String tipoModulo) {
	if("1".equals(tipoModulo)){
		return (impFormaDe>0||impFormaA>0);
	}
	return (impFormaDe>0);
}

/**
 * Es importe aceptado.
 *
 * @param impFormaDe El objeto: imp forma de
 * @param impFormaA El objeto: imp forma a
 * @param importe El objeto: importe
 * @param tipoModulo El objeto: tipo modulo
 * @return true, si exitoso
 */
private boolean esImporteAceptado(float impFormaDe, float impFormaA,
		float importe, String tipoModulo) {
	if("1".equals(tipoModulo)){
		return (importe>=impFormaDe&&importe<=impFormaA);
	}
	return (importe==impFormaDe);
}

/**
 * **********************************************************************************.
 *
 * @param cveMov El objeto: cve mov
 * @param referencia El objeto: referencia
 * @param importeDe El objeto: importe de
 * @param importeA El objeto: importe a
 * @param usuario El objeto: usuario
 * @param tipoModulo El objeto: tipo modulo
 * @param req El objeto: req
 * @return Objeto string
 */
/****************************************** Recuperar Movimientos Tarjeta de Credito */
/*************************************************************************************/
   String recuperaMovimientosTarjeta(String cveMov,String referencia,String importeDe,String importeA,String usuario, String tipoModulo,HttpServletRequest req)
	{
     //########## Cambiar ....
     String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;
     //String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/movt";

     StringBuffer cadenaTCT = new StringBuffer("");
     String arcLinea  = "";

     boolean status   = false;
	 boolean noError  = true;
     int totalLineas  = 0;

     //Borra Tasa Anualizada
	 if ( req.getAttribute("_MovTasaAnual")!=null )
		   req.removeAttribute("_MovTasaAnual");
	 // **********************************************************  Copia Remota
     ArchivoRemoto archR = new ArchivoRemoto();
     if(!archR.copia(usuario))
   	   EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosTarjeta(): No se pudo realizar la copia remota", EIGlobal.NivelLog.ERROR);
	 // **********************************************************

     EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);

     if(ArcEnt.abreArchivoLectura())
      {
        do
         {
           arcLinea=ArcEnt.leeLinea();
           if(totalLineas==0)
            if(arcLinea.substring(0,2).equals("OK"))
             status=true;

           if(!arcLinea.equals("ERROR"))
            {
              if(arcLinea.substring(0,5).trim().equals("ERROR"))
                break;
              totalLineas++;

			  if ( arcLinea.indexOf("TASA=")>-1 )
			  {
				  //TASAANUALIZADA
				  String strTasa = InsertaPunto( arcLinea.substring(arcLinea.indexOf("TASA=")+5).trim() );
				  if (!strTasa.equals("") && !strTasa.equals("00.00"))
					  req.setAttribute("_MovTasaAnual", "Tasa  Anualizada** : "+formatea(strTasa)+"%");
				  else
					  req.removeAttribute("_MovTasaAnual");
			      //System.out.println("\n\n**>TASA ANUALIZADA CON FORMATO (Movimientos TDC): ["+strTasa+"]<**\n\n");
				  //break;
			  }

			  if ( arcLinea.indexOf("TASA=") == -1 ){
				  if(totalLineas>=2)
				   {
					 EI_Tipo LineaTmp =new EI_Tipo(this);
					 float impFormaDe=0;
					 float impFormaA=0;
					 float impArc=0;

                 LineaTmp.iniciaObjeto(';','@',arcLinea+"@");

                 if(referencia.trim().length()>0)
                  {
					EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosTarjeta(): Refe en tabla tarjeta ["+LineaTmp.camposTabla[0][4].trim()+"]"+" buscar ["+referencia.trim()+"]", EIGlobal.NivelLog.INFO);

                    if(LineaTmp.camposTabla[0][4].trim().equals(referencia.trim()))
                      cadenaTCT.append("tmp;"+arcLinea+" @");
                  }
                 else
                  {
                	 try {
                         impFormaDe=new Float(importeDe).floatValue();
                         impFormaA=new Float(importeA).floatValue();
                         impArc=new Float(LineaTmp.camposTabla[0][5].trim()).floatValue();
                         impArc=impArc/100;
     					EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosTarjeta(): ImporteDe Forma ="+Float.toString(impFormaDe)+".", EIGlobal.NivelLog.INFO);
     					EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosTarjeta(): ImporteA Forma ="+Float.toString(impFormaA)+".", EIGlobal.NivelLog.INFO);
     					EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosTarjeta(): Importe Archivo ="+Float.toString(impArc)+".", EIGlobal.NivelLog.INFO);
     					EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosTarjeta(): Tipo modulo ="+tipoModulo+".", EIGlobal.NivelLog.INFO);
     						if(cveMov.trim().equals("0") && arcLinea.substring(0,1).equals("+")){
     							cadenaTCT = filtraCadenaTCTPorImporte(impFormaDe,impFormaA,impArc,tipoModulo,arcLinea,cadenaTCT);
     						}else if(cveMov.trim().equals("1") && arcLinea.substring(0,1).equals("-")){
     							cadenaTCT = filtraCadenaTCTPorImporte(impFormaDe,impFormaA,impArc,tipoModulo,arcLinea,cadenaTCT);
     						}else if(cveMov.trim().equals("2")){
     							cadenaTCT = filtraCadenaTCTPorImporte(impFormaDe,impFormaA,impArc,tipoModulo,arcLinea,cadenaTCT);
     						}
                		 
                	 } catch (Exception e) {
                		 EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosTarjeta(): Falla al convertir importe [" + e + "]", EIGlobal.NivelLog.INFO);
                	 }
                   }
				 }
			   }
            }else{
            	noError=false;
            }
         }while(noError);
        ArcEnt.cierraArchivo();

        if(arcLinea.substring(0,5).trim().equals("ERROR") && totalLineas<=1 && !status){
        	return "FAIL";
        }
      }else{
    	  return "FAIL";
      }
     return cadenaTCT.toString();
   }

/**
 * Filtra cadena tct por importe.
 *
 * @param impFormaDe El objeto: imp forma de
 * @param impFormaA El objeto: imp forma a
 * @param impArc El objeto: imp arc
 * @param tipoModulo El objeto: tipo modulo
 * @param arcLinea El objeto: arc linea
 * @param cadenaTCT El objeto: cadena tct
 * @return Objeto string buffer
 */
	private StringBuffer filtraCadenaTCTPorImporte(float impFormaDe,
			float impFormaA, float impArc, String tipoModulo, String arcLinea,
			StringBuffer cadenaTCT) {
		if (evaluarImportes(impFormaDe, impFormaA, tipoModulo)) {
			if (esImporteAceptado(impFormaDe, impFormaA, impArc, tipoModulo)) {
				cadenaTCT.append("Tmp;" + arcLinea + " @");
			}
		} else { // No se especifico importe, traer todos
			cadenaTCT.append("Tmp;" + arcLinea + " @");
		}
		return cadenaTCT;
	}

/**
 * **********************************************************************************.
 *
 * @param usuario El objeto: usuario
 * @return Objeto string
 */
/******************************************* Recuperar Movimientos Linea de Credito  */
/*************************************************************************************/
   String recuperaMovimientosLinea(String usuario)
     {
       String arcLinea="";

       //########## Cambiar ....
       String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;
       //String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/rg02";

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 EIGlobal.mensajePorTrace("MCL_Movimientos - recuperaMovimientosLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.ERROR);
	   // **********************************************************

       EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);

       if(ArcEnt.abreArchivoLectura())
        {
          boolean noError=true;
          arcLinea=ArcEnt.leeLinea();
          if(arcLinea.substring(0,2).equals("OK"))
           {
             arcLinea=ArcEnt.leeLinea();
             if(arcLinea.equals("ERROR"))
               return "NOHAY";
           }
		  ArcEnt.cierraArchivo();
        }
       else
        return "OPENFAIL";
       return arcLinea;
    }

/**
 * **********************************************************************************.
 *
 * @param tipo El objeto: tipo
 * @return Objeto string
 */
/***************************************************************** fecha Consulta    */
/*************************************************************************************/
   String formateaFecha(String tipo)
    {
      StringBuffer strFecha = new StringBuffer ("");
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);
      do
       {
         Cal.add(Calendar.DATE,-1);
       }while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);

      if(tipo.equals("FECHA"))
       {
         if(Cal.get(Calendar.DATE) <= 9)
           strFecha.append("0" + Cal.get(Calendar.DATE) + "/");
         else
           strFecha.append(Cal.get(Calendar.DATE) + "/");
         if(Cal.get(Calendar.MONTH)+1 <= 9)
           strFecha.append("0" + (Cal.get(Calendar.MONTH)+1) + "/");
         else
           strFecha.append((Cal.get(Calendar.MONTH)+1) + "/");
         strFecha.append(Cal.get(Calendar.YEAR));
	   }

	  if(tipo.equals("MES2"))
	   {
		 if(Cal.get(Calendar.MONTH)+1 <= 9)
           strFecha.append("0" + (Cal.get(Calendar.MONTH)+1));
         else
           strFecha.append((Cal.get(Calendar.MONTH)+1));
	   }

      if(tipo.equals("DIA"))
        strFecha.append(Cal.get(Calendar.DATE));
      if(tipo.equals("MES"))
        strFecha.append((Cal.get(Calendar.MONTH)+1));
      if(tipo.equals("ANIO"))
        strFecha.append(Cal.get(Calendar.YEAR));

      return strFecha.toString();
    }

/**
 * **********************************************************************************.
 *
 * @param FacArchivo El objeto: fac archivo
 * @param tipoRelacion El objeto: tipo relacion
 * @param credito El objeto: credito
 * @param facultad El objeto: facultad
 * @param request El objeto: request
 * @return true, si exitoso
 */
/*************************************************************************************/
/*************************************************************************************/
   boolean tieneFacultad(EI_Tipo FacArchivo,String tipoRelacion,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();


EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_TECRESALDOSC_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_CONS_POSICI_TER_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_CONS_SALDO_TER_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_CONS_POSICI_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_CONS_SALDO_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_CONS_MOVS_TER_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_TECREMOVTOSC_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_TECREDISPOSM_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_TECREPOSICIOC_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_TECREPPAGOSM_CRED+ " en sesion", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): comparando Facultad "+ facultad +" en ambiente con "+session.FAC_CONS_MOVTOS_CRED+ " en sesion", EIGlobal.NivelLog.INFO);


      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.DEBUG);

	  EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
          {
            if(tipoRelacion.trim().equals("P"))
              FacultadAsociadaACuenta=true;
            else
              FacultadAsociadaACuenta=false;
          }
       }
	  EIGlobal.mensajePorTrace("MCL_Movimientos - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }

    /**
     * Facultad asociada.
     *
     * @param FacArchivo El objeto: fac archivo
     * @param credito El objeto: credito
     * @param facultad El objeto: facultad
     * @param signo El objeto: signo
     * @return true, si exitoso
     */
    boolean facultadAsociada(EI_Tipo FacArchivo,String credito,String facultad,String signo)
     {
      /*
       0 7
       1 Clave Fac
       2 Cuenta
       3 signo
      */

       if(FacArchivo.totalRegistros>=1)
        for(int i=0;i<FacArchivo.totalRegistros;i++)
         {
           if(credito.trim().equals(FacArchivo.camposTabla[i][2].trim()))
            if(FacArchivo.camposTabla[i][1].trim().equals(facultad))
             if(FacArchivo.camposTabla[i][3].trim().equals(signo))
               return true;
         }
	   EIGlobal.mensajePorTrace("MCL_Movimientos - facultadAsociada(): Facultad asociada regresa false", EIGlobal.NivelLog.DEBUG);
       return false;
     }

/**
 * **********************************************************************************.
 *
 * @return Objeto string
 */
/************************************************************* Arma dias inhabiles   */
/*************************************************************************************/
  String armaDiasInhabilesJS()
	{
      StringBuffer resultado = new StringBuffer("");
      Vector diasInhabiles;

	  int indice = 0;

      diasInhabiles = CargarDias(1);
      if(diasInhabiles!=null)
	   {
		 resultado.append(diasInhabiles.elementAt(indice).toString());
		 for(indice = 1; indice<diasInhabiles.size(); indice++)
		   {
  		     resultado.append(", " );
			 resultado.append( diasInhabiles.elementAt(indice).toString());
		   }
	   }
      return resultado.toString();
	}

/**
 * **********************************************************************************.
 *
 * @param formato El objeto: formato
 * @return Objeto vector
 */
/**************************************************** Carga los dias de las tablas   */
/*************************************************************************************/
	public Vector CargarDias(int formato)
	 {
       String sqlDias;
       Vector diasInhabiles = new Vector();
	   Connection Conexion=null;
       PreparedStatement qrDias;
       ResultSet rsDias=null;

	   boolean estado;

       sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha <= sysdate";
       try
	   {
       Conexion = createiASConn(Global.DATASOURCE_ORACLE);
       if(Conexion!=null){

         qrDias = Conexion.prepareStatement( sqlDias );
         //qrDias.setSQL(sqlDias);
         if(qrDias!=null){
        	//rsDias = Conexion.executeQuery(0, qrDias, null, null);
        	rsDias = qrDias.executeQuery();

            if(rsDias!=null){
             rsDias.next();
               do{
                  String dia  = rsDias.getString(1);
                  String mes  = rsDias.getString(2);
                  String anio = rsDias.getString(3);
                  if(formato == 0){
                     dia=Integer.toString( Integer.parseInt(dia) );
                     mes=Integer.toString( Integer.parseInt(mes) );
                     anio=Integer.toString( Integer.parseInt(anio) );
                   }
                  String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
                  diasInhabiles.addElement(fecha);
                }while(rsDias.next());
              }else
			  	EIGlobal.mensajePorTrace("MCL_Movimientos - cargarDias(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
          }else
       	  	EIGlobal.mensajePorTrace("MCL_Movimientos - cargarDias(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
       }else
       	EIGlobal.mensajePorTrace("MCL_Movimientos - cargarDias(): No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
      }catch( SQLException sqle ){
      	EIGlobal.mensajePorTrace("MCL_Movimientos - cargarDias(): Se efectuo la sig. excepcion: ", EIGlobal.NivelLog.ERROR);
      	EIGlobal.mensajePorTrace( sqle.getMessage() , EIGlobal.NivelLog.ERROR);
      }finally
		  {
			try
			 {
			   Conexion.close();
			   rsDias.close();
			 }catch(Exception e) {}
		  }

      return(diasInhabiles);
    }

    /**
     * Formatea.
     *
     * @param format El objeto: format
     * @return Objeto string
     */
    private static String formatea(String format)
    {
        int decimal = 2;
        try
        {
            BigDecimal importe = (new BigDecimal(format)).setScale(decimal, 4);
            NumberFormat formateo = NumberFormat.getNumberInstance();
            formateo.setMaximumFractionDigits(decimal);
            formateo.setMinimumFractionDigits(decimal);
            format = formateo.format(importe);
        }
        catch(Exception _ex) {
								//System.out.println("Exception: MCL_Movimientos.formatea("+format+") : ["+_ex.toString()+"]");
							 }
        return format;
    }

	/**
	 * Inserta punto.
	 *
	 * @param CANTIDAD El objeto: cantidad
	 * @return Objeto string
	 */
	private String InsertaPunto(String CANTIDAD)
    {
        String CantidadConPunto = "";
        try
        {
            CantidadConPunto = CANTIDAD.substring(0, CANTIDAD.length() - 2) + "." + CANTIDAD.substring(CANTIDAD.length() - 2);
        }
        catch(Exception e)
        {
            //System.out.println("Modulo [MCL_Movimientos.InsertaPunto]:Convirtiendo[" + CANTIDAD + "]: Exception(" + e.toString() + ")");
        }
        return CantidadConPunto;
    }

	/**
	 * Exportar.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 */
	private void exportar(HttpServletRequest request,
			HttpServletResponse response) {
		BaseResource session = (BaseResource) request.getSession().getAttribute(SESSION);
		String usuario = session.getUserID8();
		String nombreArchivo   = usuario;
		String archivoOrigen = IEnlace.DOWNLOAD_PATH + nombreArchivo +".doc";
		String tipoArchivo	= request.getParameter("tipoArchivoExport");
		EIGlobal.mensajePorTrace( "Exportando archivoOrigen: "+archivoOrigen, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "Exportando nombreArchivo: "+nombreArchivo, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "Exportando tipoArchivo: "+tipoArchivo, EIGlobal.NivelLog.DEBUG);
		ExportModel em = new ExportModel("Movimientos_"+nombreArchivo, '|',true)
	    .addColumn(new Column(0,15, "TARJETA"))
	    .addColumn(new Column(16,23, "FECHA"))
	    .addColumn(new Column(24,27, "HORA"))
	    .addColumn(new Column(28,30, "SUC"))
	    .addColumn(new Column(31,70, "DESCRIPCION"))
		.addColumn(new Column(71,71, "CARGO/ABONO"))
		.addColumn(new Column(72,85, "IMPORTE"))
		.addColumn(new Column(86,99, "SALDO"))
		.addColumn(new Column(100,107, "CONCEPTO"))
		.addColumn(new Column(108,147, "REFERENCIA"));
		try {
			HtmlTableExporter.export(tipoArchivo, em, archivoOrigen, response);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace( "Error durante la exportacion del archivo de movimientos TDC: "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
	}
}
/*1.0*/