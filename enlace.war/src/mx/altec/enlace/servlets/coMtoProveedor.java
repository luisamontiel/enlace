
/********************************************************************************
 * Archivo:	coMtoProveedor.java
 * Creado:		19-Junio-2002 05:26 PM
 * Autor:		Hugo Sanchez Ricardez
 * Modificado: 29/11/2002
 * Version:	5.0
 *********************************************************************************/
package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoConf;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Importar;
import mx.altec.enlace.bo.MMC_SpidBO;
import mx.altec.enlace.bo.Proveedor;
import mx.altec.enlace.bo.servicioConfirming;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ProveedorConf;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

@SuppressWarnings({ "serial", "rawtypes", "static-access", "unused", "unchecked" })
public class coMtoProveedor extends BaseServlet {

	/**Constante SESION*/
    private final static String SESION = "session";
    /**Constante VALIDA*/
    private final static String VALIDA = "valida";
    /**Constante ET_DATOS_GENERALES*/
    private final static String ET_DATOS_GENERALES = "<DATOS GENERALES>";
    /**Constante LINEA_ENCONTRADA*/
    private final static String LINEA_ENCONTRADA = "Linea encontrada->";
	/** Variable tipoTransaccion*/
    private transient String tipoTransaccion = "";
    /** Objeto MMC_SpidBO*/
    private transient MMC_SpidBO spidBO = new MMC_SpidBO();
    /** Variable total_regs*/
    private transient String totalRegs= "0";
    /** Variable num_total_regs*/
    private transient int numTotalRegs= -1;

    /**
     * METODO GET
     * FSW IDS :::JMFR:::
     * SONAR
     * @param request de tipo HttpServletRequest
     * @param response de tipo HttpServletResponse
     * @throws ServletException, IOException por si se lanza una excepcion.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        defaultAction(request, response);}

    /**
     * METODO POST
     * FSW IDS :::JMFR:::
     * SONAR
     * @param request de tipo HttpServletRequest
     * @param response de tipo HttpServletResponse
     * @throws ServletException, IOException por si se lanza una excepcion.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        defaultAction(request, response);}

    /**
     * METODO POR DEFAULT PARA ATENDER PETICIONES DEL FRONT.
     * FSW IDS :::JMFR:::
     * SONAR
     * @param request de tipo HttpServletRequest
     * @param response de tipo HttpServletResponse
     * @throws ServletException, IOException por si se lanza una excepcion.
     */
    public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
        //String opcion = (String) request.getParameter("opcion");
    	String opcion = getFormParameter(request,"opcion");

        if(opcion == null){
        	opcion = "1";
        }

		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESION);
		//String valida = request.getParameter( VALIDA );
	    String valida = getFormParameter(request,VALIDA);

		EIGlobal.mensajePorTrace("Nuevo Modulo de Confirming Proveedores", EIGlobal.NivelLog.INFO);

        if (SesionValida(request, response)) {

	        /******************************************Inicia validacion OTP**************************************/

	        if(validaPeticion( request,  response,session,sess,valida)){
	            EIGlobal.mensajePorTrace("\n\n\n EN validacion OTP \n\n\n", EIGlobal.NivelLog.DEBUG);
	        }
	        /******************************************Termina validacion OTP**************************************/
	        else
			{


	            if("1".equals(opcion)) {
	            	try{
	            		mtoProv(request,response);
	    			}catch(Exception e){
	    				StackTraceElement[] lineaError;
	    				lineaError = e.getStackTrace();
	    				EIGlobal.mensajePorTrace("Error al ejecutar el alta de proveedor del servlet coMtoProveedor", EIGlobal.NivelLog.ERROR);
	    				EIGlobal.mensajePorTrace("coMtoProveedor::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
	    										+ e.getMessage()
	    			               				+ ET_DATOS_GENERALES
	    							 			+ LINEA_ENCONTRADA + lineaError[0]
	    							 			, EIGlobal.NivelLog.ERROR);
	    			}
	            	return;
	            }

	            if("2".equals(opcion)) {
	            	try{
	            		modProv(request,response);
	    			}catch(Exception e){
	    				StackTraceElement[] lineaError;
	    				lineaError = e.getStackTrace();
	    				EIGlobal.mensajePorTrace("Error al ejecutar la modificacion de proveedor del servlet coMtoProveedor", EIGlobal.NivelLog.ERROR);
	    				EIGlobal.mensajePorTrace("coMtoProveedor::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
	    										+ e.getMessage()
	    			               				+ ET_DATOS_GENERALES
	    							 			+ LINEA_ENCONTRADA + lineaError[0]
	    							 			, EIGlobal.NivelLog.ERROR);
	    			}
	            	return;
	            }

			}
		}
    }

    /**
     * METODO MTOPROV
     * FSW IDS :::JMFR:::
     * SONAR
     * @param req de tipo HttpServletRequest
     * @param res de tipo HttpServletResponse
     * @throws ServletException, IOException por si se lanza una excepcion.
     */
	public void mtoProv(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		//String valida = req.getParameter( VALIDA );
    	String valida = getFormParameter(req,VALIDA);
		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESION);

            //######################## FACULTADES
			sess.setAttribute("fac_CCALTMANPR", new Boolean(verificaFacultad("CCALTMANPR",req)));
			sess.setAttribute("fac_CCALTPRINTRNAL", new Boolean(verificaFacultad("CCALTPRINTRNAL",req)));
			sess.setAttribute("fac_CCIMPARCHPR", new Boolean(verificaFacultad("CCIMPARCHPR",req)));
			sess.setAttribute("fac_CCIMPPRINTRNAL", new Boolean(verificaFacultad("CCIMPPRINTRNAL",req)));
            EIGlobal.mensajePorTrace("Se verifica la facultad CCALTMANPR->"
            		              + ((Boolean)sess.getAttribute("fac_CCALTMANPR")).toString(), EIGlobal.NivelLog.ERROR);
            EIGlobal.mensajePorTrace("Se verifica la facultad CCALTPRINTRNAL->"
            		              + ((Boolean)sess.getAttribute("fac_CCALTPRINTRNAL")).toString(), EIGlobal.NivelLog.ERROR);
            EIGlobal.mensajePorTrace("Se verifica la facultad CCIMPARCHPR->"
            		              + ((Boolean)sess.getAttribute("fac_CCIMPARCHPR")).toString(), EIGlobal.NivelLog.ERROR);
            EIGlobal.mensajePorTrace("Se verifica la facultad CCIMPPRINTRNAL->"
            		              + ((Boolean)sess.getAttribute("fac_CCIMPPRINTRNAL")).toString(), EIGlobal.NivelLog.ERROR);
            if( !verificaFacultad("CCALTMANPR",req) &&
            	!verificaFacultad("CCALTPRINTRNAL",req) &&
            	!verificaFacultad("CCIMPARCHPR",req) &&
            	!verificaFacultad("CCIMPPRINTRNAL",req)) {
                EIGlobal.mensajePorTrace("No se tiene la facultad CCALTMANPR" , EIGlobal.NivelLog.ERROR);

                String laDireccion = "document.location='SinFacultades'";
                req.setAttribute( "plantillaElegida", laDireccion);
                req.getRequestDispatcher( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

                return;
            }
            EIGlobal.mensajePorTrace("Se tiene la facultad CCALTMANPR" , EIGlobal.NivelLog.ERROR);


            // -------------------------------
            // Parche para evitar la doble llamada del Internet Explorer 6.0
            GregorianCalendar ahora = new GregorianCalendar();
            Long fechaAnt = (Long)sess.getAttribute("coMtoProvFecha");
            long fechaAhora =
                    ahora.get(Calendar.YEAR) * 10000000000L +
                    ahora.get(Calendar.MONTH) * 100000000L +
                    ahora.get(Calendar.DATE) * 1000000L +
                    ahora.get(Calendar.HOUR) * 10000L +
                    ahora.get(Calendar.MINUTE) * 100L +
                    ahora.get(Calendar.SECOND);


            EIGlobal.mensajePorTrace("\n\n\n\n" + fechaAnt + "  " + fechaAhora + "\n\n\n",EIGlobal.NivelLog.DEBUG);
            if(fechaAnt != null) {
                long numUltFecha = fechaAnt.longValue();
                if(Math.abs(numUltFecha - fechaAhora)<7) {
                    return;
                }
            }
            // -------------------------------




            req.setAttribute("MenuPrincipal", session.getStrMenu());
            req.setAttribute("newMenu",	session.getFuncionesDeMenu());
            req.setAttribute("Encabezado", CreaEncabezado("Alta de Proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200h",req) );
            InicializaCombos(req);
            iniciaEstado(req);
            iniciaPaisCta(req);
            req.setAttribute("clavesBanco", creaArrayClavesBanco());
            /**
             * FSW IDS :::JMFR:::
             * SE AGREGA LISTA DE CLAVES INTERME
             */
            req.setAttribute("clavesBancoInt", spidBO.getConfirmingInterme());

            //String strAccion = req.getParameter("accion");
            String strAccion = getFormParameter(req,"accion");
            String strOrigenProveedor = getFormParameter(req,"origenProveedor");
            String strTipoPersona = getFormParameter(req,"tipoPersona");
            EIGlobal.mensajePorTrace("\n\n\n>>>>>>accion: " + strAccion + "\n\n\n\n",EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace("\n\n\n>>>>>>ULISES: ORIGEN " + strOrigenProveedor + " * TIPO " + strTipoPersona + "\n\n\n\n",EIGlobal.NivelLog.DEBUG);
            if(strOrigenProveedor!=null){
            	EIGlobal.mensajePorTrace("PANTALLA->"+strOrigenProveedor, EIGlobal.NivelLog.ERROR);
            }else if(!verificaFacultad("CCALTMANPR",req) &&
            		  verificaFacultad("CCALTPRINTRNAL",req)){
            	strOrigenProveedor = "I";
            	EIGlobal.mensajePorTrace("PANTALLA->"+strOrigenProveedor, EIGlobal.NivelLog.ERROR);
            }else if(verificaFacultad("CCALTMANPR",req) &&
            		!verificaFacultad("CCALTPRINTRNAL",req)){
            	strOrigenProveedor = "N";
            	EIGlobal.mensajePorTrace("PANTALLA->"+strOrigenProveedor, EIGlobal.NivelLog.ERROR);
            }else if(verificaFacultad("CCALTMANPR",req) &&
            		 verificaFacultad("CCALTPRINTRNAL",req)){
            	strOrigenProveedor = "N";
            	EIGlobal.mensajePorTrace("PANTALLA->"+strOrigenProveedor, EIGlobal.NivelLog.ERROR);
            }else if(!verificaFacultad("CCALTMANPR",req) &&
            		 !verificaFacultad("CCALTPRINTRNAL",req)){
            	strOrigenProveedor = "N";
            	EIGlobal.mensajePorTrace("PANTALLA->"+strOrigenProveedor, EIGlobal.NivelLog.ERROR);
            }
            req.setAttribute("pantallaOriProveedor", strOrigenProveedor);

            debug("archivo es nulo: " + (sess.getAttribute("objetoArchivo") == null), EIGlobal.NivelLog.INFO);
            //String nombreArchivo = req.getParameter("archivo_actual");
            String nombreArchivo = getFormParameter(req,"archivo_actual");
            Vector errores = null;


            if(nombreArchivo == null) nombreArchivo = "";
            if(strAccion == null) {
                sess.setAttribute("objetoArchivo",new ArchivoConf(""));
                strAccion = "0";
            }

            ArchivoRemoto archRemoto = new ArchivoRemoto();
            ArchivoConf objArchivo = (ArchivoConf)sess.getAttribute("objetoArchivo");
            ProveedorConf prov = null;
            int numProv = 0;
            boolean pasa = true;

            if(objArchivo == null) objArchivo = new ArchivoConf("");


            EIGlobal.mensajePorTrace("\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <TTTCCC_2_7> " + strAccion + "  " + (objArchivo == null) + "  " + ">" + nombreArchivo + "<" + "\n\n\n\n",EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace("Cabecera Accion: " + strAccion, EIGlobal.NivelLog.ERROR);

            if(!strAccion.equals("8") && objArchivo.getEstado() != objArchivo.SIN_ENVIAR) {
                EIGlobal.mensajePorTrace("Cabecera Entra a m�todo mtoProv() ...", EIGlobal.NivelLog.ERROR);
                calculaYmandaAceptados(req, objArchivo, numTotalRegs);
            }

            // Facultad para Exportacion de Archivos
            if(verificaFacultad("CCEXPORARCH",req))
                req.setAttribute("urlExportar",("/Download/" + session.getUserID8() + "_coMtoExp.cfm"));
            else //TODO Implementar internacionalizaci�n
                req.setAttribute("urlExportar",("javascript:cuadroDialogo('Ud no tiene facultades para exportar archivos',3)"));
            //Subir a session accion
            req.setAttribute("accion", strAccion);
            switch(Integer.parseInt(strAccion)) {
                case 0: // --- inicio
                    EIGlobal.mensajePorTrace("Cabecera Entra en case 0 de mto ...", EIGlobal.NivelLog.ERROR);
                    evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    break;
                case 1: // --- alta
                    if(!verificaFacultad("CCALTMANPR",req) &&
                       !verificaFacultad("CCALTPRINTRNAL",req)) {
                        req.setAttribute("mensaje", "\"Ud. no tiene facultades para realizar el alta manual de proveedores.\",3");
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                        break;
                    }
                    //inicia Modif PVA 08/03/2003
                    //Si el proveedor es Internacional, muestra la ayuda correspondiente para el Alta de Proveedores Internacionales (s28200Ah).
                    //En caso contrario muestra la ayuda correspondiente para el Alta de Proveedores Nacionales (s28200Bh).
                   	if( "I".equals(strOrigenProveedor) ) {
                   		req.setAttribute("Encabezado", CreaEncabezado("Alta de Proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200Ah",req) );
                   	} else {
                   		req.setAttribute("Encabezado", CreaEncabezado("Alta de Proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200Bh",req) );
                   	}
					//fin
                    evalTemplate("/jsp/coAltaProveedor.jsp",req,res);
                    break;
                case 2: // --- modificacion
                    if(!verificaFacultad("CCMODLOCPR",req)) {
                        req.setAttribute("mensaje", "\"Ud no tiene facultades para modificar proveedores\",3");
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                        break;
                    }
                    numProv = Integer.parseInt(getFormParameter(req,"numSelec"));
                    prov=objArchivo.obtenProv(numProv);
                    req.setAttribute("pantallaOriProveedor", prov.origenProveedor);
                    req.setAttribute("Proveedor",prov);
                    //inicia Modif PVA 08/03/2003
                    req.setAttribute("Encabezado", CreaEncabezado("Alta de Proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200Ch",req) );
                    //fin
                    evalTemplate("/jsp/coAltaProveedor.jsp",req,res);
                    break;
                case 3: // --- regreso de alta
                    prov = obtenProveedorDeRequest(req);
                    objArchivo.agregaProv(prov);
                    //inicia Modif PVA 08/03/2003
                    req.setAttribute("Encabezado", CreaEncabezado("Alta de Proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200Bh",req) );
                    //fin
                    evalTemplate("/jsp/coAltaProveedor.jsp",req,res);
                    break;
                case 4: // --- regreso de modificacion
                    prov = obtenProveedorDeRequest(req);
                    numProv = Integer.parseInt(getFormParameter(req,"numSelec"));
                    objArchivo.cambiaProv(numProv,prov);
                    req.setAttribute("mensaje","\"Los datos fueron modificados SSSSS\",1");
                    evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    break;
                case 5: // --- borrar proveedor
                    if(!verificaFacultad("CCBAJLOCPR",req)) {
                        req.setAttribute("mensaje", "\"Ud no tiene facultades para borrar registros de proveedores\",3");
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                        break;
                    }
                    numProv = Integer.parseInt(getFormParameter(req,"numSelec"));
                    prov = objArchivo.borraProv(numProv);
                    req.setAttribute("mensaje","\"El proveedor fue borrado\",1");
                    evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    break;
                case 6: // --- enviar archivo

                    //interrumpe la transaccion para invocar la validacion para Alta de proveedor
                    if(  valida==null) {
                        EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transaccion esta parametrizada para solicitar la validacion con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

                        boolean solVal=ValidaOTP.solicitaValidacion(
                                session.getContractNumber(),IEnlace.CONFIRMING_PAGO_PROVEEDORES);
                      //CSA-Vulnerabilidad  Token Bloqueado-Inicio
                        int estatusToken = obtenerEstatusToken(session.getToken());
                        
                        if( session.getValidarToken() &&
                                session.getFacultad(session.FAC_VAL_OTP) &&
                                estatusToken == 1 &&
                                solVal) {
                            EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicito la validacion. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);
                            //ValidaOTP.guardaParametrosEnSession(req);
                            guardaParametrosEnSession(req);
                            ValidaOTP.validaOTP(req,res,IEnlace.VALIDA_OTP);
                        } else if(session.getValidarToken() &&
                                session.getFacultad(session.FAC_VAL_OTP) &&
                                estatusToken == 2 &&
                                solVal){
                        	cierraSesionTokenBloqueado(session, req, res);
                        } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
                            valida="1";
						}
                    }
                    // retoma el flujo
                    if( valida!=null && valida.equals("1")) {
                        if(!verificaFacultad("CCENVARCHPR",req)) {
                            req.setAttribute("mensaje", "\"Ud no tiene facultades para enviar archivos de proveedores\",3");
                            evalTemplate("/jsp/coMtoProv.jsp",req,res);
                            break;
                        }
                        objArchivo.escribeEnvio(Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + "_coMtoProv");
                        objArchivo.escribeExportacion(Global.DIRECTORIO_REMOTO_WEB +"/" + session.getUserID8() + "_coMtoExp.cfm");
                        if(!archRemoto.copiaLocalARemoto(session.getUserID8() + "_coMtoExp.cfm", "WEB" )) {
                            debug("No se pudo enviar el archivo Exportado", EIGlobal.NivelLog.INFO);}
                        pasa = enviaArchivo(req);
                        objArchivo = (ArchivoConf)req.getSession().getAttribute("objetoArchivo");
                        if(pasa) objArchivo.setEstado(objArchivo.ENVIADO);
                        try{
                        	calculaYmandaAceptados(req, objArchivo, numTotalRegs);
                        }catch(Exception e){
                        	EIGlobal.mensajePorTrace("coMtoProveedor::mtoProv:: Manejando Proceso calculaYmandaAceptados", EIGlobal.NivelLog.INFO);
                        }
                        EIGlobal.mensajePorTrace("Cabecera Entra en caso enviar archivo ...", EIGlobal.NivelLog.ERROR);
                        ValidaOTP.guardaBitacora((List)sess.getAttribute("bitacora"),tipoTransaccion);
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    }
                    break;
                case 7: // --- recuperar archivo
                    if(!verificaFacultad("CCACTUARCH",req)) {
                        req.setAttribute("mensaje", "\"Ud no tiene facultades para recuperar archivos de proveedores\",3");
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                        break;
                    }
                    pasa = recuperaArchivo(req);

                    numTotalRegs = Integer.parseInt(totalRegs);

                    objArchivo = (ArchivoConf)req.getSession().getAttribute("objetoArchivo");
                    if(pasa) {
                        objArchivo.setEstado(objArchivo.RECUPERADO);
                        objArchivo.escribeExportacion(Global.DIRECTORIO_REMOTO_WEB + "/" + session.getUserID8() + "_coMtoExp.cfm");
                        if(!archRemoto.copiaLocalARemoto(session.getUserID8() + "_coMtoExp.cfm", "WEB" )) {
                            debug("No se pudo enviar el archivo Exportado", EIGlobal.NivelLog.INFO);}
                        calculaYmandaAceptados(req, objArchivo, numTotalRegs);
                        numTotalRegs = -1;
                        if(objArchivo.getNumProveedores() == 0)
                            req.setAttribute("mensaje","\"El archivo recuperado est&aacute; vac&iacute;o o no existe\",1");
                        else
                            req.setAttribute("mensaje","\"El archivo fue recuperado con &eacute;xito\",1");
                    }
                    evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    break;
                case 8: // --- borrar archivo
                    if(!verificaFacultad("CCELIMIARCH",req)) {
                        req.setAttribute("mensaje", "\"Ud no tiene facultades para borrar archivos de proveedores\",3");
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                        break;
                    }
                    objArchivo = new ArchivoConf("");
                    sess.setAttribute("objetoArchivo",objArchivo);
                    evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    break;
                case 9: // --- importar archivo
                    if(!verificaFacultad("CCIMPARCHPR",req) &&
                       !verificaFacultad("CCIMPPRINTRNAL",req)) {
                        req.setAttribute("mensaje", "\"Ud. no tiene facultades para realizar el alta de proveedores por importacion de archivos.\",3");
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                        break;
                    }
                    objArchivo = new ArchivoConf("");



                                /* Se obiene el archivo y si viene en zip se descomprime, se cambia el nombre para
                                   y se deja listo para enviarlo a la clase de importacion */

                    EI_Importar arcImp= new EI_Importar();
                    String nombreArchivoImp="";

//                  Stefanini*19/05/2009*JAL
                    //Se comento para que realize la importacion en IAS
                    //nombreArchivoImp=arcImp.importaArchivo(req,res);

//                  Stefanini*19/05/2009*JAL
                    //Se descomenta porque solo se utiliza para WAS
                    nombreArchivoImp=getFormParameter(req,"fileNameUpload");

                    if(nombreArchivoImp.indexOf("ZIPERROR")>=0 ) {
                        despliegaPaginaError( " Imposible Importar el archivo, no es un archivo zip o esta da�ado",
                                "Alta de Proveedores en linea",
                                "Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200Dh", req, res );
                    } else {
                        //Se envia solo el nombre del archivo para validarlo...
                        /* DAG 061009 Se manda el parametro de tipo de proveedor*/
                        errores = objArchivo.leeImportacion(nombreArchivoImp, getFormParameter(req,"vTProv"));

                        if(cuentaErrores(errores)>0) {
                            objArchivo = new ArchivoConf("");
                            sess.setAttribute("objetoArchivo",objArchivo);
                            req.setAttribute("mensajeImportacion",formateaErrores(errores));
                        } else {
                            objArchivo.setNombre("IMPORTADO");
                            objArchivo.setEstado(objArchivo.IMPORTADO);
                            sess.setAttribute("objetoArchivo",objArchivo);
                            //					objArchivo.escribeEnvio(Global.DIRECTORIO_LOCAL + "/" + session.getUserID() + "_coMtoProv");
                            //					pasa = enviaArchivo(req);
                            //					objArchivo = (ArchivoConf)req.getSession().getAttribute("objetoArchivo");
                            //					if(pasa) objArchivo.setEstado(objArchivo.ENVIADO);
                            //					calculaYmandaAceptados(req, objArchivo);
                        }
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    }
                    break;
                case 10: // --- imprimir reporte
                    if(!verificaFacultad("CCIMPRREPOR",req)) {
                        req.setAttribute("mensaje", "\"Ud no tiene facultades para imprimir reportes de proveedores\",3");
                        evalTemplate("/jsp/coMtoProv.jsp",req,res);
                        break;
                    }
                    //inicia Modif PVA 08/03/2003
                    String tipoimp=(String)getFormParameter(req,"impresionDetallada");
                    if (tipoimp==null)
                        tipoimp="S";
                    //subir a session impresionDetallada
                    EIGlobal.mensajePorTrace("impresion Detallada al salir de servlet->"+tipoimp, EIGlobal.NivelLog.DEBUG);
                    req.setAttribute("impresionDetallada", tipoimp);
                    if(tipoimp.equals("S"))
                        req.setAttribute("Encabezado", CreaEncabezado("Alta de Proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200Dh",req) );
                    else
                        req.setAttribute("Encabezado", CreaEncabezado("Alta de Proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Alta de Proveedores en linea","s28200Eh",req) );

                    //fin
                    evalTemplate("/jsp/coMtoProvImp.jsp",req,res);
                    break;
                case 11:
                    objArchivo.setNombre(nombreArchivo);
                    req.setAttribute("mensaje","\"Archivo creado exitosamente\",1");
                    evalTemplate("/jsp/coMtoProv.jsp",req,res);
                    break;
                case 12: // Opcion para descargar el archivo por medio de Save As
                	String downloadingFile = (String) getFormParameter(req, "downloadingFile");
                	if(verificaFacultad("CCEXPORARCH",req)){
	                	saveAs(downloadingFile, res);
	                    objArchivo.setNombre(nombreArchivo);
	                    break;
                	}else{
                		req.setAttribute("mensaje", "\"Ud no tiene facultades para realizar la exportacion de archivos\",3");
                		evalTemplate("/jsp/coMtoProv.jsp",req,res);
                		break;
                	}

            }




            // -------------------------------
            // Parche para evitar la doble llamada del Internet Explorer 6.0
            sess.setAttribute("coMtoProvFecha", new Long(fechaAhora));
            // -------------------------------




            sess.setAttribute("objetoArchivo",objArchivo);

        //} // Termina else de validacion del OTP

    } // Termina mtoProv

    /**
     * METODO MODPROV
     * FSW IDS :::JMFR:::
     * SONAR
     * @param req de tipo HttpServletRequest
     * @param res de tipo HttpServletResponse
     * @throws ServletException, IOException por si se lanza una excepcion.
     */
    public void modProv(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESION);

		String valida = getFormParameter(req, VALIDA );
        //######################## FACULTADES CCMODLOCPR
		String facultadM="false";
        String facultadMI="false";
        String facultadB="false";
        String facultadBI="false";
        if(verificaFacultad("CCMODPROSER",req))
            facultadM="true";
        if(verificaFacultad("CCMODPROINTRNAL",req))
            facultadMI="true";
        if(verificaFacultad("CCBAJAPROV",req))
            facultadB="true";
        if(verificaFacultad("CCBJAPROINTRNAL",req))
            facultadBI="true";
        EIGlobal.mensajePorTrace("Se verifica la facultad CCMODLOCPR " +  verificaFacultad("CCMODLOCPR",req), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Se verifica la facultad CCCONSPROV " +  verificaFacultad("CCCONSPROV",req), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Se verifica la facultad CCMODPROSER " +  verificaFacultad("CCMODPROSER",req), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Se verifica la facultad CCMODPROINTRNAL " +  verificaFacultad("CCMODPROINTRNAL",req), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Se verifica la facultad CCBAJAPROV " +  verificaFacultad("CCBAJAPROV",req), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Se verifica la facultad CCBJAPROINTRNAL " +  verificaFacultad("CCBJAPROINTRNAL",req), EIGlobal.NivelLog.DEBUG);

        if( !verificaFacultad("CCCONSPROV",req) &&
            !verificaFacultad("CCMODPROSER",req) &&
            !verificaFacultad("CCMODPROINTRNAL",req) &&
            !verificaFacultad("CCBAJAPROV",req) &&
            !verificaFacultad("CCBJAPROINTRNAL",req)) {
            EIGlobal.mensajePorTrace("No se tiene la facultad CCMODLOCPR" , EIGlobal.NivelLog.ERROR);

            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute( "plantillaElegida", laDireccion);
            req.getRequestDispatcher( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

            return;
        }
        EIGlobal.mensajePorTrace("Se tiene la facultad CCCOMPPROV" , EIGlobal.NivelLog.ERROR);
        //######################## FACULTADES CCMODLOCPR


        EIGlobal.mensajePorTrace("<<coMtoProveedor>>========== > Entra a m�todo modProv() ...", EIGlobal.NivelLog.DEBUG);

        req.setAttribute("MenuPrincipal", session.getStrMenu());
        req.setAttribute("newMenu", session.getFuncionesDeMenu());
        req.setAttribute("Encabezado", CreaEncabezado("Consulta y Modificacion de proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Consulta y Modificacion de Proveedores","s28210h",req));
        InicializaCombos(req);
        iniciaEstado(req);
        iniciaPaisCta(req);
        req.setAttribute("clavesBanco", creaArrayClavesBanco());
        /**
         * FSW IDS :::JMFR:::
         * SE AGREGA LISTA DE CLAVES INTERME
         */
        req.setAttribute("clavesBancoInt", spidBO.getConfirmingInterme());

        String strAccion = getFormParameter(req,"accion");
        Vector errores = null;
        int numProv = 0;
        boolean pasa = true;

        if(strAccion == null) strAccion = "0";

        EIGlobal.mensajePorTrace("<<coMtoProveedor>>========== > accion : "+strAccion, EIGlobal.NivelLog.DEBUG);

        ArchivoConf objArchivo = null;
        ProveedorConf prov = null;

        servicioConfirming arch_combos = new servicioConfirming();
        //arch_combos.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
        arch_combos.setUsuario(session.getUserID8());
        arch_combos.setContrato(session.getContractNumber());
        arch_combos.setPerfil(session.getUserProfile());
        req.setAttribute("arch_combos", arch_combos);

        //################### Facultad Para Modificar
        req.setAttribute("facultadM",facultadM );
        req.setAttribute("facultadMI",facultadMI );
        req.setAttribute("facultadB",facultadB );
        req.setAttribute("facultadBI",facultadBI );
        //########################



        switch(Integer.parseInt(strAccion)) {
            case 0: // --- inicio

//=================================================================> jbg 06/06/03

                Proveedor  arch_p               = new Proveedor();
                Vector nomProv                  = null;
                String ArchivoResp              = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","","");

                if(ArchivoResp != null) // Se pudo crear el archivo
                {

                    nomProv = arch_p.coMtoProv_Leer(ArchivoResp,"9",1); //guarda la lista de proveedores

                    if ( nomProv.firstElement().equals("")) // si la lista esta vacia
                    {

                        req.setAttribute("MenuPrincipal", session.getStrMenu());
                        req.setAttribute("newMenu", session.getFuncionesDeMenu());
                        req.setAttribute("Encabezado", CreaEncabezado("Consulta y Modificacion de proveedores ","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Consulta y Modificacion de Proveedores","s28210h",req));
                        req.setAttribute("Mensaje", "No existen proveedores dados de alta !");
                        evalTemplate( "/jsp/EI_Mensaje2.jsp", req, res);

                    }else{  evalTemplate("/jsp/coActualizaciones.jsp",req,res); // Si hay proveedores

                    }

                } else {  // No se pudo crear el archivo por alg�n error

                    req.setAttribute("MenuPrincipal", session.getStrMenu());
                    req.setAttribute("newMenu", session.getFuncionesDeMenu());
                    req.setAttribute("Encabezado", CreaEncabezado("Consulta y Modificacion de proveedores ","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Consulta y Modificacion de Proveedores","s28210h",req));
                    req.setAttribute("Mensaje", "No se pudo realizar la consulta de proveedores en este momento, intente m&aacute;s tarde !");
                    evalTemplate( "/jsp/EI_Mensaje2.jsp", req, res);
                }
//===================================================================> jbg 06/06/03
                break;

            case 11: // --- consulta
            	if(!verificaFacultad("CCCONSPROV",req)){
                	req.setAttribute("mensaje", "\"Ud. no tiene facultades para consultar proveedores.\",3");
                	evalTemplate("/jsp/coActualizaciones.jsp",req,res);
                }else{
	                if(servicioTuxProv(req, getFormParameter(req,"Proveedor"), Integer.parseInt(getFormParameter(req,"accion")))) {
	                    //Inicia Modif PVA 08/03/2003
	                    req.setAttribute("Encabezado", CreaEncabezado("Consulta y Modificacion de proveedores en linea","Servicios &gt; Confirming &gt; Mantenimiento de Proveedores &gt; Consulta y Modificacion de Proveedores","s28210Bh",req));
	                    //termina
	                    evalTemplate("/jsp/coAltaProveedor.jsp",req,res);
	                } else {

	                    evalTemplate("/jsp/coActualizaciones.jsp",req,res);

					}
                }
                break;
            case 12: // --- modificacion
				 //interrumpe la transaccion para invocar la validacion para Alta de proveedor

                    if(  valida==null) {
                        EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transaccion esta parametrizada para solicitar la validacion con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

                        boolean solVal=ValidaOTP.solicitaValidacion(
                                session.getContractNumber(),IEnlace.CONFIRMING_PAGO_PROVEEDORES);

                        int estatusToken = obtenerEstatusToken(session.getToken());
                        if( session.getValidarToken() &&
                                session.getFacultad(session.FAC_VAL_OTP) &&
                                estatusToken == 1 &&
                                solVal) {
                            EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicito la validacion. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);
                            //ValidaOTP.guardaParametrosEnSession(req);
                            guardaParametrosEnSession(req);
                            ValidaOTP.validaOTP(req,res,IEnlace.VALIDA_OTP);
                        } else if(session.getValidarToken() &&
                                session.getFacultad(session.FAC_VAL_OTP) &&
                                estatusToken == 2 &&
                                solVal){                        
                        		cierraSesionTokenBloqueado(session,req,res);                   
                        } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
                            valida="1";
						}
                    }
                    // retoma el flujo

                    if( valida!=null && valida.equals("1")) {

						EIGlobal.mensajePorTrace("\n\n\n Modificar el usuario ... \n\n\n", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Verificar y obtener los datos del request ...", EIGlobal.NivelLog.DEBUG);
		                prov = obtenProveedorDeRequest(req);
		                servicioTuxProv(req, prov.tramaModificacion(), Integer.parseInt(getFormParameter(req,"accion")));
						ValidaOTP.guardaBitacora((List)sess.getAttribute("bitacora"),tipoTransaccion);
		                evalTemplate("/jsp/coActualizaciones.jsp",req,res);
					}

                break;
            case 13: // --- baja

				//interrumpe la transaccion para invocar la validacion para Alta de proveedor
                    if(  valida==null) {
                        EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transaccion esta parametrizada para solicitar la validacion con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

                        boolean solVal=ValidaOTP.solicitaValidacion(
                                session.getContractNumber(),IEnlace.CONFIRMING_PAGO_PROVEEDORES);

                        int estatusToken = obtenerEstatusToken(session.getToken());
                        if( session.getValidarToken() &&
                                session.getFacultad(session.FAC_VAL_OTP) &&
                                estatusToken == 1 &&
                                solVal) {
                            EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicito la validacion. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);
                            //ValidaOTP.guardaParametrosEnSession(req);
                            guardaParametrosEnSession(req);
                            ValidaOTP.validaOTP(req,res,IEnlace.VALIDA_OTP);
                        } else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) &&
                                estatusToken == 2 && solVal){                        
                        	cierraSesionTokenBloqueado(session,req,res);                        	
                        } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
                            valida="1";
						}
                    }
                    // retoma el flujo
                    if( valida!=null && valida.equals("1")) {
						EIGlobal.mensajePorTrace("\n\n\n Eliminar el usuario ... \n\n\n", EIGlobal.NivelLog.DEBUG);

		                prov = obtenProveedorDeRequest(req);
		                servicioTuxProv(req, prov.codigoCliente, Integer.parseInt(getFormParameter(req,"accion")));
		                ValidaOTP.guardaBitacora((List)sess.getAttribute("bitacora"),tipoTransaccion);
		                evalTemplate("/jsp/coActualizaciones.jsp",req,res);

					}

                break;
        }
    }

    /**
     * METODO CALCULA Y MANDA ACEPTADOS
     * FSW IDS :::JMFR:::
     * SONAR
     * @param req de tipo HttpServletRequest
     * @param objArchivo de tipo ArchivoConf
     * @param num_total_regs de tipo int.
     */
    private void calculaYmandaAceptados(HttpServletRequest req, ArchivoConf objArchivo, int num_total_regs) {
        int total=0, aceptados=0, rechazados=0, a, total_aux=0;

        total = objArchivo.getNumProveedores();
        for(a=0;a<total;a++) if(((ProveedorConf)objArchivo.obtenProv(a)).claveStatus.equals("A")) aceptados++;
        if (num_total_regs != -1) {
            for(a=0;a<total;a++) if(((ProveedorConf)objArchivo.obtenProv(a)).claveStatus.equals("R")) rechazados++;
            if ( rechazados != 0 || aceptados != 0)
                rechazados = num_total_regs - aceptados;

            req.setAttribute("regTot",""+num_total_regs);
            req.setAttribute("regTot_2",""+num_total_regs);
            EIGlobal.mensajePorTrace("Cabecera Total: " + num_total_regs + " Aceptados: " + aceptados + " Rechazados: " + rechazados, EIGlobal.NivelLog.ERROR);
        } else {
            for(a=0;a<total;a++) if(((ProveedorConf)objArchivo.obtenProv(a)).claveStatus.equals("R")) rechazados++;
            req.setAttribute("regTot",""+total);
            EIGlobal.mensajePorTrace("Cabecera entra en else total1: " +total , EIGlobal.NivelLog.ERROR);
        }

        req.setAttribute("regAce",""+aceptados);
        req.setAttribute("regRec",""+rechazados);
    }

    /**
     * METODO FORMATEA ERRORES
     * FSW IDS :::JMFR:::
     * SONAR
     * @param vector de tipo Vector
     * @return htmlCabecera de tipo String
     */
    private String formateaErrores(Vector vector) {
        Vector vectorLinea;
        String htmlTotal, htmlCabecera, htmlCuerpo, htmlPie;
        int a, b, total;

        total = cuentaErrores(vector);
        htmlTotal = "<br><br><br><b>TOTAL: " + ((total > 1)?(total + " errores"):"1 error") + "</b>";
        htmlCabecera = "<html><head><title>Estatus</title><link rel='stylesheet' href='/EnlaceMig/" +
                "consultas.css' type='text/css'></head><body bgcolor='white'><form><table border=0" +
                " width=420 class='textabdatcla' align=center><tr><th class='tittabdat'>informaci&" +
                "oacute;n del Archivo Importado</th></tr><tr><td class='tabmovtex1' align=center><" +
                "table border=0 align=center><tr><td class='tabmovtex1' align=center width=><img s" +
                "rc='/gifs/EnlaceMig/gic25060.gif'></td><td class='tabmovtex1' align='center'><br>" +
                "No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los" +
                " siguientes errores.<br><br></td></tr></table></td></tr><tr><td class='tabmovtex1'>";
        htmlCuerpo = "";
        htmlPie = "<br></td></tr></table><table border=0 align=center><tr><td align=center><br><a " +
                "href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></" +
                "a></td></tr></table></form></body></html>";

        for(a=0;a<vector.size();a++) {
            vectorLinea = (Vector)vector.get(a);
            if(vectorLinea.size()>0) htmlCuerpo += "<br><b>L&iacute;nea " + (a+1) + "</b>";
            for(b=0;b<vectorLinea.size();b++) htmlCuerpo +="<br><DD><LI>" + ((String)vectorLinea.get(b));
        }
        return htmlCabecera + htmlCuerpo + htmlTotal + htmlPie;
    }

    /**
     * METODO CUENTA ERRORES
     * FSW IDS :::JMFR:::
     * SONAR
     * @param errores de tipo Vector
     * @return num de tipo int 
     */
    private int cuentaErrores(Vector errores) {
        int num = 0, a;
        for(a=0;a<errores.size();a++) num += ((Vector)errores.get(a)).size();
        return num;
    }

    /**
     * METODO FORMATEA ERRORES
     * FSW IDS :::JMFR:::
     * SONAR
     * @param req de tipo HttpServletRequest
     * @return resultado de tipo boolean
     */
    private boolean recuperaArchivo(HttpServletRequest req) {
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESION);

        boolean resultado = true;
        ArchivoConf objArchivo = new ArchivoConf("");
        String trama;
        Hashtable ht;
        ServicioTux tuxedo = new ServicioTux();
        ArchivoRemoto archRemoto = new ArchivoRemoto();

        int el_indice = -1;

        try {
            tipoTransaccion = "CFER";
            trama = "2EWEB|" + session.getUserID8() + "|" + tipoTransaccion + "|" + session.getContractNumber() +
                    "|" + session.getUserID8() + "|" + session.getUserProfile() + "|" + getFormParameter(req,"numProv") + "@|";
            debug("trama de envio: " + trama, EIGlobal.NivelLog.INFO);
            //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
            ht = tuxedo.web_red(trama);
            trama = (String)ht.get("BUFFER");
            debug("tuxedo regresa: " + trama, EIGlobal.NivelLog.INFO);
            if(trama.indexOf('|')>0)
            	trama = trama.substring(trama.lastIndexOf('/')+1,trama.length()-1);
            else
            	trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
            if(!archRemoto.copia(trama)) {
                req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
                return false;
            }
            BufferedReader verifica = new BufferedReader(new FileReader(Global.DIRECTORIO_LOCAL + "/" + trama));
            String linea = verifica.readLine();
            if(!linea.startsWith("OK")) {
                req.setAttribute("mensaje","\"" + linea.substring(16) + "\",3");
                return false;
            } else {
                //Modificacion para obtener total de registros del Archivo en su origen.
                //el_indice = linea.lastIndexOf(';');
                EIGlobal.mensajePorTrace("Leyo de la linea Cabecera: " + linea+ "indice: " + el_indice, EIGlobal.NivelLog.INFO);
                //total_regs = linea.substring(el_indice + 1);
                EIGlobal.mensajePorTrace("Cabecera (tot_regs) " + totalRegs, EIGlobal.NivelLog.INFO);

                linea = verifica.readLine();
                totalRegs = linea;

                objArchivo.leeRecuperacion2(Global.DIRECTORIO_LOCAL + "/" + trama);
                objArchivo.setNombre("ARCHIVO RECUPERADO");

                //linea = verifica.readLine();
                //total_regs = linea;
            }
            sess.setAttribute("objetoArchivo",objArchivo);
        } catch(Exception e) {
            req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
            resultado = false;
        }

        return resultado;
    }

    /**
     * METODO ENVIA ARCHIVO
     * FSW IDS :::JMFR:::
     * SONAR
     * @param req de tipo HttpServletRequest
     * @return resultado de tipo boolean 
     */
    private boolean enviaArchivo(HttpServletRequest req) {
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESION);
        EmailSender emailSender=new EmailSender();//MAAM 10/JUL/2010

        boolean resultado = true;
        ArchivoConf objArchivo = (ArchivoConf)sess.getAttribute("objetoArchivo");
        String trama;
        String CodErrorOper="";//MAAM 09/AGO/2010
        Hashtable ht;
        ServicioTux tuxedo = new ServicioTux();
        ArchivoRemoto archRemoto = new ArchivoRemoto();

        try {
        	ProveedorConf conf = objArchivo.obtenProv(0);
            if(!archRemoto.copiaLocalARemoto(session.getUserID8()+"_coMtoProv")) {
                req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
                return false;
            }
            /* DAG 011009: Se agrega una I o una N dependiendo del origen proveedor*/
            tipoTransaccion = "CFRR";
            trama = "2EWEB|" + session.getUserID8() + "|" + tipoTransaccion + "|" + session.getContractNumber() +
                    "|" + session.getUserID8() + "|" + session.getUserProfile() + "|" +
                    Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + "_coMtoProv@" + objArchivo.getNumProveedores() + "@" +
                    objArchivo.getNombre() + conf.getOrigenProveedor()+ "@|";
            debug("trama de envio: " + trama, EIGlobal.NivelLog.INFO);
            //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
            ht = tuxedo.web_red(trama);
            trama = (String)ht.get("BUFFER");
            CodErrorOper=ht.get("COD_ERROR")+"";//MAAM 09/AGO/2010
            debug("tuxedo regresa: " + trama, EIGlobal.NivelLog.INFO);
            trama = trama.substring(trama.lastIndexOf('/')+1,trama.length()-1);
            if(!archRemoto.copia(trama)) {
                req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
                return false;
            }
            objArchivo.leeRegreso(Global.DIRECTORIO_LOCAL + "/" + trama);
            GregorianCalendar hoy = new GregorianCalendar();
            req.setAttribute("mensaje","\"Favor de recuperar el archivo con el n&uacute;mero de secuencia " +
                    objArchivo.getSecuencia() + " a partir de las " + ((hoy.get(Calendar.HOUR_OF_DAY)< 9)?"0":"") +
                    ( 1 + hoy.get(Calendar.HOUR_OF_DAY ) ) + ":00 hrs.\",1");
            sess.setAttribute("objetoArchivo",objArchivo);

            EIGlobal.mensajePorTrace("Inicio de Notificacion por Correo", EIGlobal.NivelLog.DEBUG);
            try {
				EmailDetails beanEmailDetails = new EmailDetails();

				EIGlobal.mensajePorTrace("NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("session.getModuloConsultar ->" + session.getModuloConsultar(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("session.getFolioArchivo ->" + session.getFolioArchivo(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("referencia ->" + objArchivo.getSecuencia(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("No de transmision ->" + objArchivo.getSecuencia(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("no de registros ->" + objArchivo.getNumProveedores(), EIGlobal.NivelLog.DEBUG);

				beanEmailDetails.setNumeroContrato(session.getContractNumber());
				beanEmailDetails.setRazonSocial(session.getNombreContrato());
				beanEmailDetails.setNumRef(objArchivo.getSecuencia());
				beanEmailDetails.setNumTransmision(objArchivo.getSecuencia());
				beanEmailDetails.setNumRegImportados(objArchivo.getNumProveedores());

				beanEmailDetails.setEstatusActual(CodErrorOper);
				EIGlobal.mensajePorTrace("estatus ->" + beanEmailDetails.getEstatusActual(), EIGlobal.NivelLog.DEBUG);


				//emailSender.sendNotificacion(IEnlace.ALTA_PROVEEDORES, "notificaciones_santander@hotmail.com", beanEmailDetails); //MAAM 10/JUL/2010
				if(emailSender.enviaNotificacion(CodErrorOper)){//MAAM 09/AGO/2010
					emailSender.sendNotificacion(req,IEnlace.ALTA_PROVEEDORES,beanEmailDetails);//MAAM 10/JUL/2010
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("Excepcion al enviar el correo ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}

            /////////////////////////////////////////

        } catch(Exception e) {
            req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
            resultado = false;
        }

        return resultado;
    }

    /**
     * METODO SERVICIO TUXEDO PROV
     * FSW IDS :::JMFR:::
     * SONAR
     * @param req de tipo HttpServletRequest
     * @param codigo de tipo String
     * @param tipo de tipo int
     * @return resultado de tipo boolean 
     */
    private boolean servicioTuxProv(HttpServletRequest req, String codigo, int tipo) {
        // tipo: 11 - consulta, 12 - modificacion, 13 - baja

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESION);

        boolean resultado = true;
        String trama = null;
        Hashtable ht;
        ServicioTux tuxedo = new ServicioTux();
        ArchivoRemoto archRemoto = new ArchivoRemoto();
        ProveedorConf prov = new ProveedorConf();

        try {
            switch(tipo) {
                case 11:
                    tipoTransaccion = "CFDP";
                    trama = "2EWEB|" + session.getUserID8() + "|" + tipoTransaccion + "|" + session.getContractNumber() +
                            "|" + session.getUserID8() + "|" + session.getUserProfile() + "|" + getFormParameter(req,"Proveedor") + "@|";
                    break;
                case 12:
                    tipoTransaccion = "CFMR";
                    trama = "1EWEB|" + session.getUserID8() + "|" + tipoTransaccion + "|" + session.getContractNumber() +
                            "|" + session.getUserID8() + "|" + session.getUserProfile() + "|" + codigo;
                    break;
                case 13:
                    tipoTransaccion = "CFPB";
                    trama = "1EWEB|" + session.getUserID8() + "|" + tipoTransaccion + "|" + session.getContractNumber() +
                            "|" + session.getUserID8() + "|" + session.getUserProfile() + "|" + codigo + "@|";
                    break;
            }
            debug("trama de envio: " + trama, EIGlobal.NivelLog.INFO);

            //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

            ht = tuxedo.web_red(trama);
            trama = (String)ht.get("BUFFER");
            debug("tuxedo regresa: " + trama, EIGlobal.NivelLog.INFO);

            switch(tipo) {
                case 11:
                    if(prov.leeLineaConsulta(trama))
					{
                    	req.setAttribute("pantallaOriProveedor", prov.origenProveedor);
					    req.setAttribute("Proveedor",prov);
					} else
					{
                        debug("No se leyo la linea: " + prov.tramaEnvio(), EIGlobal.NivelLog.INFO);
                        req.setAttribute("mensaje","\"La Clave de Proveedor no esta registrada en Confirming\",3");
                        resultado = false;

                    }
                    break;
                case 12:

                    req.setAttribute("mensaje","\"Los datos fueron modificados\",1");
                    break;
                case 13:
                    if(trama.substring(0,2).equals("OK"))
                        req.setAttribute("mensaje","\"El proveedor fue eliminado\",1");
                    else {
                        try {
                            req.setAttribute("mensaje","\""+trama.substring(16,trama.length())+"\",1");

                        } catch(Exception e) {
                            req.setAttribute("mensaje","\"El proveedor no fue eliminado\",1");

                        }
                    }
                    break;
            }
        } catch(Exception e) {
            req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
            resultado = false;
        }
        return resultado;
    }

    /**
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * Fecha Modificacion: 2009/03/24
     * Se agregaron campos para manejo de divisas
     * @param req
     * @return
     */
    // ---
    private ProveedorConf obtenProveedorDeRequest(HttpServletRequest req) {
        ProveedorConf prov = new ProveedorConf();
        //jgarcia - 29/Sept/2009 Se agrega obtenci�n de origen de proveedor
        String origenProv = getFormParameter(req,"origenProveedor");

        prov.tipo = (getFormParameter(req,"tipoPersona") == null)?prov.FISICA:
            ((getFormParameter(req,"tipoPersona").equals("F"))?prov.FISICA:prov.MORAL);
        prov.origenProveedor = (null != origenProv && prov.INTERNACIONAL.equalsIgnoreCase(origenProv)?
    				prov.INTERNACIONAL:prov.NACIONAL);
        prov.claveProveedor = getFormParameter(req,"vClave");
        prov.codigoCliente = getFormParameter(req,"vCodigo");
        prov.noTransmision = getFormParameter(req,"vSecuencia");
        prov.nombre = getFormParameter(req,"vNombre");
        prov.apellidoPaterno =getFormParameter(req,"vPaterno");
        prov.apellidoMaterno = getFormParameter(req,"vMaterno");
        prov.tipoSociedad =getFormParameter(req,"vTipoSoc");
        prov.rfc = getFormParameter(req,"vRFC");
        prov.homoclave = getFormParameter(req,"vHomoclave");
        prov.contactoEmpresa = getFormParameter(req,"vContacto");
        prov.clienteFactoraje = getFormParameter(req,"vFactoraje");
        prov.calleNumero = getFormParameter(req,"vCalle");
        prov.colonia = getFormParameter(req,"vColonia");
        prov.cdPoblacion = getFormParameter(req,"vCiudad");
        prov.cp = getFormParameter(req,"vCodigoPostal");
        prov.pais = getFormParameter(req,"vPais");
        /* jgarcia 29/Sept/2009
         * Si es proveedor Internacional el medio de informacion debe ser e-mail
         * y para el proveedor Nacional no existe el campo vDatosAdic.
         * En el caso del proveedor internacional en el campo de delegacion
         * se guarda el estado.
         */
        if(prov.INTERNACIONAL.equalsIgnoreCase(origenProv)){
        	prov.medioInformacion = "1";
        	prov.datosAdic = getFormParameter(req,"vDatosAdic");
        	prov.delegMunicipio = "";
        }else{
        	prov.medioInformacion = getFormParameter(req,"vMedio");
        	prov.datosAdic = "";
        	prov.delegMunicipio = getFormParameter(req,"vDelegacion");
        }
        prov.estado = getFormParameter(req,"vEstado");
        prov.email = getFormParameter(req,"vEmail");
        prov.ladaPrefijo = getFormParameter(req,"vLada");
        prov.numeroTelefonico = getFormParameter(req,"vTelefonico");
        prov.extensionTel = getFormParameter(req,"vExTelefono");
        prov.fax = getFormParameter(req,"vFax");
        prov.extensionFax = getFormParameter(req,"vExFax");
        prov.formaPago = getFormParameter(req,"vForma");
        prov.cuentaCLABE = getFormParameter(req,"vCuenta");
        prov.cveDivisa = getFormParameter(req,"vCveDivisa");
        prov.cvePais = getFormParameter(req,"vCvePais");
        prov.banco = getFormParameter(req,"vBanco");
        prov.ctaBancoDestino = getFormParameter(req,"vCtaBancoDestino");
        prov.cveABA = getFormParameter(req,"vCveABA");
        prov.descripcionBanco = getFormParameter(req,"vDescripcionBanco");
        prov.descripcionCiudad = getFormParameter(req,"vDescripcionCiudad");
        prov.deudoresActivos = getFormParameter(req,"vDeudores");
        prov.limiteFinanciamiento = getFormParameter(req,"vLimite");
        prov.volumenAnualVentas = getFormParameter(req,"vVolumen");
        prov.importeMedioFacturasEmitidas = getFormParameter(req,"vImporte");
        prov.sucursal = getFormParameter(req,"vSucursal");
        prov.plaza = getFormParameter(req,"vPlaza");
        //jgarcia - Se agrega tipo abono
        prov.tipoAbono = getFormParameter(req,"vTipoAbono");
        prov.quitaNulos();

		if(! (prov.tipo==prov.FISICA) ){
			prov.apellidoPaterno="";
			prov.apellidoMaterno="";
		 }

        return prov;
    }

    // --
    private String creaArrayClavesBanco() {
        StringBuffer strArray = new StringBuffer();
        strArray.append("\"0\"");
        Connection conn = null;
        PreparedStatement TipoQuery = null;
        ResultSet TipoResult = null;

        try {
            // Se realiza la consulta a la BD
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if(conn == null) {
            	debug("Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
            	return "";}
            TipoQuery = conn.prepareStatement("SELECT NOMBRE_CORTO, NUM_CECOBAN from COMU_INTERME_FIN where NUM_CECOBAN<>0 order by NOMBRE_CORTO");
            if(TipoQuery == null) {
            	debug("No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
            	return "";}
            TipoResult = TipoQuery.executeQuery();
            if(TipoResult == null) {
            	debug("No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
            	return "";}

            // Se obtienen los datos y se crea el array
            while(TipoResult.next()){
            	strArray.append(", \"" + TipoResult.getString(2)+"\"");
            	//strArray += ", \"" + TipoResult.getString(2)+"\"";
            }
        } catch( SQLException sqle ) {
            debug("EXCEPCION en creaArrayClavesBanco(): " + sqle.getMessage(), EIGlobal.NivelLog.INFO);
            strArray.append("");
        } finally {
            try {TipoResult.close();} catch(Exception e) {;}
            try {conn.close();} catch(Exception e) {;}
        }
        return strArray.toString();
    }

    // --
    private Hashtable clavesBanco() {
        Hashtable claves = new Hashtable();

        Connection conn = null;
        PreparedStatement TipoQuery = null;
        ResultSet TipoResult = null;

        try {
            // Se realiza la consulta a la BD
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if(conn == null) {
            	debug("Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
            	return null;}
            TipoQuery = conn.prepareStatement("SELECT cve_interme,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto");
            if(TipoQuery == null) {
            	debug("No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
            return null;}
            TipoResult = TipoQuery.executeQuery();
            if(TipoResult == null) {
            	debug("No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
            	return null;}

            // Se obtienen los datos y se crea el array
            while(TipoResult.next()) claves.put(TipoResult.getString(2),TipoResult.getString(1));
            claves.put("plazas",clavesPlaza());
        } catch( SQLException sqle ) {
            debug("EXCEPCION en creaArrayClavesBanco(): " + sqle.getMessage(), EIGlobal.NivelLog.INFO);
            claves = null;
        } finally {
            try {TipoResult.close();} catch(Exception e) {;}
            try {conn.close();} catch(Exception e) {;}
        }
        return claves;
    }


    /**
     * clavesABA()
     * Metodo para obtener las claves ABA de una cuenta
     * respecto a un banco en USA.
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * 2009/03/24
     * @return
     */

    public String creaArrayClavesABA(String bancoCorresponsal) {

    	//String strArray = "";
    	StringBuffer strArray= new  StringBuffer();
        Connection conn = null;
        PreparedStatement TipoQuery = null;
        ResultSet TipoResult = null;
        String cveABA = "";
        String ciudad = "";
        String estado = "";
        String query=null;
 EIGlobal.mensajePorTrace("\n>>>>>>Entra creaArrayClavesABA",EIGlobal.NivelLog.DEBUG);
        try {
            // Se realiza la consulta a la BD
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if(conn == null) {
            	debug("Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO);
            	return null;
            }

            if(bancoCorresponsal == null || bancoCorresponsal.equals(""))
            query="SELECT CVE_ABA,CIUDAD,ESTADO, NOMBRE FROM CAMB_BANCO_EU";
            else
            query="SELECT CVE_ABA,CIUDAD,ESTADO, NOMBRE FROM CAMB_BANCO_EU WHERE NOMBRE LIKE '" + bancoCorresponsal + "'";

            EIGlobal.mensajePorTrace("\n>>>>>>Entra creaArrayClavesABA antes de preparar query-->"+query,EIGlobal.NivelLog.DEBUG);
            	TipoQuery = conn.prepareStatement(query);
            	EIGlobal.mensajePorTrace("\n>>>>>>Entra creaArrayClavesABA despues de preparar query-->",EIGlobal.NivelLog.DEBUG);


            if(TipoQuery == null) {
            	debug("No se pudo crear Query.", EIGlobal.NivelLog.INFO);
            	return null;
            }
          EIGlobal.mensajePorTrace("\n>>>>>>Entra creaArrayClavesABA antes de ejecutar query-->",EIGlobal.NivelLog.DEBUG);
            TipoResult = TipoQuery.executeQuery();
            EIGlobal.mensajePorTrace("\n>>>>>>Entra creaArrayClavesABA despues de ejecutar query",EIGlobal.NivelLog.DEBUG);
            if(TipoResult == null) {
            	debug("No se pudo crear ResultSet.", EIGlobal.NivelLog.INFO);
            	return null;
            }
 EIGlobal.mensajePorTrace("\n>>>>>>Entra creaArrayClavesABA antes de while ",EIGlobal.NivelLog.DEBUG);

            while(TipoResult.next())
            {	strArray.append(TipoResult.getString(1)).append("|").append(TipoResult.getString(4)).append("|").append(TipoResult.getString(2)).append("@");
            //	strArray += TipoResult.getString(1) + "|" + TipoResult.getString(4) + "|" + TipoResult.getString(2) + "@";
           //  	System.out.println(ui+"--->>>>>>strArray-->"+strArray.toString());

             	}
            	EIGlobal.mensajePorTrace("\n>>>>>>Entra creaArrayClavesABA despues de while ",EIGlobal.NivelLog.DEBUG);
        } catch( SQLException sqle ) {
            debug("EXCEPCION en creaArrayClavesABA(): " + sqle.getMessage(), EIGlobal.NivelLog.INFO);
            strArray = null;
        } finally {
            try {
            	TipoResult.close();
            } catch(Exception e) {
            }
            try {
            	conn.close();
            } catch(Exception e) {
            }
        }

        	//System.out.println("\n>>>>>>Entra creaArrayClavesABA regresa metodo--->"+strArray.toString());
        return strArray.toString();
    }



    // --
    private Hashtable clavesPlaza() {
        Hashtable claves = new Hashtable();

        Connection conn = null;
        PreparedStatement TipoQuery = null;
        ResultSet TipoResult = null;

        try {
            // Se realiza la consulta a la BD
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if(conn == null) {
            	debug("Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
            	return null;}
            TipoQuery = conn.prepareStatement("SELECT plaza_banxico,descripcion from comu_pla_banxico order by plaza_banxico");
            if(TipoQuery == null) {
            	debug("No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
            return null;}
            TipoResult = TipoQuery.executeQuery();
            if(TipoResult == null) {
            	debug("No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
            	return null;}

            // Se obtienen los datos y se crea el array
            while(TipoResult.next()) claves.put(TipoResult.getString(1),TipoResult.getString(2));
        } catch( SQLException sqle ) {
            debug("EXCEPCION en creaArrayClavesBanco(): " + sqle.getMessage(), EIGlobal.NivelLog.INFO);
            claves = null;
        } finally {
            try {TipoResult.close();} catch(Exception e) {;}
            try {conn.close();} catch(Exception e) {;}
        }
        return claves;
    }


    // XX -- Inicializa los atributos que seran pasados al JSP
    private void InicializaCombos(HttpServletRequest request) {
        request.setAttribute("Estados",ObtenCatalogo("SELECT cve_estado,desc_estado FROM comu_estado ORDER by desc_estado","0"));
        request.setAttribute("Estados2",ObtenCatalogo("SELECT cve_equivalente,desc_estado FROM comu_estado ORDER by desc_estado","0"));
        request.setAttribute("USO_CTA_CHEQUE", Global.USO_CTA_CHEQUE);
        request.setAttribute("Paises", ObtenCatalogo("SELECT cve_pais,descripcion from comu_paises order by descripcion","0"));
        
        /**
         * FSW IDS JMFR 
         * OBSERVACIONES BANXICO BANCOS SPID
         */
        //Se comenta para nueva implementacion combo dinamico SPID
        //request.setAttribute("Bancos", ObtenCatalogo("SELECT cve_interme,nombre_corto,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto","0"));
        
        StringBuffer strOpcion = new StringBuffer("@");
    	EIGlobal.mensajePorTrace("coMttoProveedor - traeBancos(): INI", EIGlobal.NivelLog.INFO);
    	spidBO.getBancosConfirmingSPID(strOpcion);    
    	EIGlobal.mensajePorTrace("coMttoProveedor - BancosUSD(): strBancos: " + strOpcion.toString(), EIGlobal.NivelLog.INFO);
    	request.setAttribute("bancosUSD",strOpcion.toString());
    	strOpcion.setLength(0);
    	strOpcion.append("@");
    	spidBO.getBancosConfirming(strOpcion);    
    	EIGlobal.mensajePorTrace("coMttoProveedor - BancosMXM(): strBancos: " + strOpcion.toString(), EIGlobal.NivelLog.INFO);
    	request.setAttribute("bancosMXM",strOpcion.toString());
    	EIGlobal.mensajePorTrace("coMttoProveedor - traeBancos(): END", EIGlobal.NivelLog.INFO);
    	//FIN SPID
        
        request.setAttribute("TipoSoc",ObtenCatalogo("SELECT codigo_global,descripcion FROM comu_detalle_cd WHERE  tipo_global='CTTSOC' ORDER by descripcion","0"));
        request.setAttribute("Plazas", ObtenCatalogo("SELECT plaza_banxico,descripcion from comu_pla_banxico order by descripcion","0"));
        request.setAttribute("FormaPago",ObtenCatalogo("SELECT cve_medio_pago,descripcion from cfrm_forma_pago","1"));
        request.setAttribute("MedioInfo",ObtenCatalogo("SELECT cve_medios_conf,descripcion from cfrm_medios_conf order by descripcion","2"));
    }

    // XX -- Obtiene cualquier catalogo, mediante un query
    public String ObtenCatalogo(String sqlTipoQuery,String TipoModificado) {
        String strOpcion = "";
        Connection conn = null;
        PreparedStatement TipoQuery = null;
        ResultSet TipoResult = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if(conn == null) {
            	EIGlobal.mensajePorTrace("Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
            	return "";}

            TipoQuery = conn.prepareStatement(sqlTipoQuery);
            if(TipoQuery == null) {
            	EIGlobal.mensajePorTrace("No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
            return "";}

            TipoResult = TipoQuery.executeQuery();
            if(TipoResult == null) {
            	EIGlobal.mensajePorTrace("No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
            return "";}

            while(TipoResult.next()) {
	                if(TipoModificado.equals("1")||TipoModificado.equals("2"))//forma de pago
	                {
	                    if ((TipoResult.getString(1).equals("1"))||(TipoResult.getString(1).equals("2"))) {
	                        strOpcion += "<option CLASS='tabmovtex' value='" + TipoResult.getString(1) + "'>" + TipoResult.getString(2) + "\n";
	                    }
	                } else if(TipoModificado.equals("3")) {
	                    strOpcion += ", '" + TipoResult.getString(1) + "'";
	                } else
	                    strOpcion += "<option CLASS='tabmovtex' value='" + TipoResult.getString(1) + "'>" + TipoResult.getString(2) + "\n";
        	}
        } catch( SQLException sqle ) {
            EIGlobal.mensajePorTrace("Excepcion Error... " + sqle.getMessage(), EIGlobal.NivelLog.INFO);
            return "";
        } finally {
            try {
                TipoResult.close();
                conn.close();
            } catch(Exception e) {EIGlobal.mensajePorTrace(e.getMessage(),EIGlobal.NivelLog.ERROR);}
        }

        return strOpcion;
    }

    // --
    private void debug(String mensaje, EIGlobal.NivelLog nivel) {
        EIGlobal.mensajePorTrace("<debug coMtoProveedor.java> " + mensaje,nivel);
    }

    public void guardaParametrosEnSession(HttpServletRequest request) {

        EIGlobal.mensajePorTrace("\nDentro de guardaParametrosEnSession 945\n", EIGlobal.NivelLog.DEBUG);
        request.getSession().removeAttribute("bitacora");
        request.getSession().setAttribute("plantilla",request.getServletPath());

        HttpSession session = request.getSession(false);
        if(session != null) {
            Map<String, Object> tmp = new HashMap<String, Object>();
//          jgarcia - 29/Sept/2009 Se agrega obtenci�n de origen de proveedor
            String origenProv = (String)getFormParameter(request,"origenProveedor");

            tmp.put("opcion", getFormParameter(request, "opcion"));
            tmp.put("accion", getFormParameter(request, "accion"));
            tmp.put("archivo_actual", getFormParameter(request, "archivo_actual"));

			//Para modificacion

			String value;

			value = (String)  getFormParameter(request, VALIDA);
			if(value != null && !value.equals(null)) {
				tmp.put(VALIDA, getFormParameter(request, VALIDA));
			}

			tmp.put("numSelec", getFormParameter(request, "numSelec"));
			tmp.put("impresionDetallada", getFormParameter(request, "impresionDetallada"));
			tmp.put("numProv", getFormParameter(request, "numProv"));
			tmp.put("Proveedor", getFormParameter(request, "Proveedor"));

			tmp.put("tipoPersona",(getFormParameter(request,"tipoPersona")!=null?getFormParameter(request,"tipoPersona"):""));
			tmp.put("origenProveedor",origenProv);
			tmp.put("vClave",(getFormParameter(request,"vClave")!=null?getFormParameter(request,"vClave"):""));
			tmp.put("vCodigo",(getFormParameter(request,"vCodigo")!=null?getFormParameter(request,"vCodigo"):""));
			tmp.put("vSecuencia",(getFormParameter(request,"vSecuencia")!=null?getFormParameter(request,"vSecuencia"):""));
			tmp.put("vNombre",(getFormParameter(request,"vNombre")!=null?getFormParameter(request,"vNombre"):""));
			tmp.put("vPaterno",(getFormParameter(request,"vPaterno")!=null?getFormParameter(request,"vPaterno"):""));
			tmp.put("vMaterno",(getFormParameter(request,"vMaterno")!=null?getFormParameter(request,"vMaterno"):""));
			tmp.put("vTipoSoc",(getFormParameter(request,"vTipoSoc")!=null?getFormParameter(request,"vTipoSoc"):""));
			tmp.put("vRFC",(getFormParameter(request,"vRFC")!=null?getFormParameter(request,"vRFC"):""));
			tmp.put("vHomoclave",(getFormParameter(request,"vHomoclave")!=null?getFormParameter(request,"vHomoclave"):""));
			tmp.put("vContacto",(getFormParameter(request,"vContacto")!=null?getFormParameter(request,"vContacto"):""));
			tmp.put("vFactoraje",(getFormParameter(request,"vFactoraje")!=null?getFormParameter(request,"vFactoraje"):""));
			tmp.put("vCalle",(getFormParameter(request,"vCalle")!=null?getFormParameter(request,"vCalle"):""));
			tmp.put("vColonia",(getFormParameter(request,"vColonia")!=null?getFormParameter(request,"vColonia"):""));
			tmp.put("vCiudad",(getFormParameter(request,"vCiudad")!=null?getFormParameter(request,"vCiudad"):""));
			tmp.put("vCodigoPostal",(getFormParameter(request,"vCodigoPostal")!=null?getFormParameter(request,"vCodigoPostal"):""));
			tmp.put("vPais",(getFormParameter(request,"vPais")!=null?getFormParameter(request,"vPais"):""));
			tmp.put("vCvePais",(getFormParameter(request,"vCvePais")!=null?getFormParameter(request,"vCvePais"):""));

			/* jgarcia 29/Sept/2009
	         * Si es proveedor Internacional el medio de informacion debe ser e-mail
	         * y para el proveedor Nacional no existe el campo vDatosAdic.
	         * En el caso del proveedor internacional en el campo de delegacion
	         * se guarda el estado.
	         */
			if(ProveedorConf.INTERNACIONAL.equalsIgnoreCase(origenProv)){
				tmp.put("vMedio","1");
				tmp.put("vDatosAdic",(getFormParameter(request,"vDatosAdic")!=null?getFormParameter(request,"vDatosAdic"):""));
				tmp.put("vDelegacion","");
	        }else{
	        	tmp.put("vMedio",(getFormParameter(request,"vMedio")!=null?getFormParameter(request,"vMedio"):""));
	        	tmp.put("vDatosAdic","");
	        	tmp.put("vDelegacion",(getFormParameter(request,"vDelegacion")!=null?getFormParameter(request,"vDelegacion"):""));
	        }
			tmp.put("vEstado",(getFormParameter(request,"vEstado")!=null?getFormParameter(request,"vEstado"):""));
			tmp.put("vEmail",(getFormParameter(request,"vEmail")!=null?getFormParameter(request,"vEmail"):""));
			tmp.put("vLada",(getFormParameter(request,"vLada")!=null?getFormParameter(request,"vLada"):""));
			tmp.put("vTelefonico",(getFormParameter(request,"vTelefonico")!=null?getFormParameter(request,"vTelefonico"):""));
			tmp.put("vExTelefono",(getFormParameter(request,"vExTelefono")!=null?getFormParameter(request,"vExTelefono"):""));
			tmp.put("vFax",(getFormParameter(request,"vFax")!=null?getFormParameter(request,"vFax"):""));
			tmp.put("vExFax",(getFormParameter(request,"vExFax")!=null?getFormParameter(request,"vExFax"):""));
			tmp.put("vForma",(getFormParameter(request,"vForma")!=null?getFormParameter(request,"vForma"):""));
			tmp.put("vCuenta",(getFormParameter(request,"vCuenta")!=null?getFormParameter(request,"vCuenta"):""));
			tmp.put("vBanco",(getFormParameter(request,"vBanco")!=null?getFormParameter(request,"vBanco"):""));
			tmp.put("vSucursal",(getFormParameter(request,"vSucursal")!=null?getFormParameter(request,"vSucursal"):""));
			tmp.put("vPlaza",(getFormParameter(request,"vPlaza")!=null?getFormParameter(request,"vPlaza"):""));
			tmp.put("vDeudores",(getFormParameter(request,"vDeudores")!=null?getFormParameter(request,"vDeudores"):""));
			tmp.put("vLimite",(getFormParameter(request,"vLimite")!=null?getFormParameter(request,"vLimite"):""));
			tmp.put("vVolumen",(getFormParameter(request,"vVolumen")!=null?getFormParameter(request,"vVolumen"):""));
			tmp.put("vImporte",(getFormParameter(request,"vImporte")!=null?getFormParameter(request,"vImporte"):""));
			tmp.put("vCveDivisa",(getFormParameter(request,"vCveDivisa")!=null?getFormParameter(request,"vCveDivisa"):""));
			tmp.put("vDescripcionCiudad",(getFormParameter(request,"vDescripcionCiudad")!=null?getFormParameter(request,"vDescripcionCiudad"):""));
			tmp.put("vDescripcionBanco",(getFormParameter(request,"vDescripcionBanco")!=null?getFormParameter(request,"vDescripcionBanco"):""));
			tmp.put("vCveABA",(getFormParameter(request,"vCveABA")!=null?getFormParameter(request,"vCveABA"):""));
			tmp.put("vCtaBancoDestino",(getFormParameter(request,"vCtaBancoDestino")!=null?getFormParameter(request,"vCtaBancoDestino"):""));
			tmp.put("vTipoAbono",(getFormParameter(request,"vTipoAbono")!=null?getFormParameter(request,"vTipoAbono"):""));

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
        EIGlobal.mensajePorTrace("\nSaliendo de guardaParametrosEnSession \n", EIGlobal.NivelLog.DEBUG);
    }


    /**
     * Metodo para iniciar el combo de estado para la modificacion de proveedor
     * @param request		Request de la aplicacion.
     *
     */
    private void iniciaEstado(HttpServletRequest request){
    	Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String[] cveEstado = null;
		String[] descEstado = null;
		int count = 0;
		int i = 0;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select count(1) as count "
				  + "From comu_estado "
				  + "ORDER by desc_estado";
			EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaEstado:: -> Query:" + query, EIGlobal.NivelLog.DEBUG);
			rs = sDup.executeQuery(query);
			if(rs.next()){
				count = rs.getInt("count");
				cveEstado = new String[count];
				descEstado = new String[count];
			}

			if(count>0){
				query = "Select cve_estado as cveEstado, "
					  + "desc_estado as descEstado "
					  + "From comu_estado "
					  + "ORDER by desc_estado";

				EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaEstado:: -> Query:" + query, EIGlobal.NivelLog.DEBUG);
				rs = sDup.executeQuery(query);

				while(rs.next()){
					cveEstado[i] = rs.getString("cveEstado")!=null?rs.getString("cveEstado"):"";
					descEstado[i] = rs.getString("descEstado")!=null?rs.getString("descEstado"):"";
					i++;
				}

				request.setAttribute("cveEstado", cveEstado);
				request.setAttribute("descEstado", descEstado);
			}else{
				request.setAttribute("cveEstado", "");
				request.setAttribute("descEstado", "");
			}
			EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaEstado:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaEstado:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error consultar las claves de estado", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coMtoProveedor::iniciaEstado:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ ET_DATOS_GENERALES
							 			+ LINEA_ENCONTRADA + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
	}

    /**
     * Metodo para iniciar el combo de cve de pais para cuenta
     * @param request		Request de la aplicacion.
     *
     */
    private void iniciaPaisCta(HttpServletRequest request){
    	Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String[] cvePais = null;
		String[] descPais = null;
		int count = 0;
		int i = 0;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select count(1) as count "
				  + "From comu_paises "
				  + "ORDER by descripcion";
			EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaPaisCta:: -> Query:" + query, EIGlobal.NivelLog.DEBUG);
			rs = sDup.executeQuery(query);
			if(rs.next()){
				count = rs.getInt("count");
				cvePais = new String[count];
				descPais = new String[count];
			}

			if(count>0){
				query = "Select cve_pais as cvePais, "
					  + "descripcion as descPais "
					  + "From comu_paises "
					  + "ORDER by descripcion";

				EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaPaisCta:: -> Query:" + query, EIGlobal.NivelLog.DEBUG);
				rs = sDup.executeQuery(query);

				while(rs.next()){
					cvePais[i] = rs.getString("cvePais")!=null?rs.getString("cvePais"):"";
					descPais[i] = rs.getString("descPais")!=null?rs.getString("descPais"):"";
					i++;
				}

				request.setAttribute("cvePais", cvePais);
				request.setAttribute("descPais", descPais);
			}else{
				request.setAttribute("cvePais", "");
				request.setAttribute("descPais", "");
			}
			EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaPaisCta:: -> Finalizando proceso..", EIGlobal.NivelLog.DEBUG);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("coMtoProveedor::iniciaPaisCta:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error consultar las claves de pais", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coMtoProveedor::iniciaPaisCta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ ET_DATOS_GENERALES
							 			+ LINEA_ENCONTRADA + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
	}

    /**
     * Metodo especial para descargar un archivo en linea a una ruta especifica
     *
     * @param nameFile		Nombre del Archivo
     * @param response		Response de la peticion
     */
    private void saveAs(String nameFile, HttpServletResponse response){
	  	try{
			String newNameFile = nameFile.replaceAll("/Download/", "");
			EIGlobal.mensajePorTrace("Ruta->" + Global.DIRECTORIO_REMOTO_WEB + newNameFile, EIGlobal.NivelLog.DEBUG);
			java.io.FileInputStream archivo = new java.io.FileInputStream(Global.DIRECTORIO_REMOTO_WEB + "/" + newNameFile);
			int longitud = archivo.available();
			byte[] datos = new byte[longitud];
			archivo.read(datos);
			archivo.close();
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition","attachment;filename=" + newNameFile);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(datos);
			ouputStream.flush();
			ouputStream.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}


}
