package mx.altec.enlace.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.utilerias.EIGlobal;

/** 
*   Isban M�xico
*   Clase: OwaspManejador.java
*   Descripci�n: Manejador de errores para el filtro CSRF.
*
*   Control de Cambios:
*   1.0 16/10/2017 Everis - Validaci�n token CSRF
*/
public class OwaspManejador  extends BaseServlet{


	
    /** Numero serial de la clase */
	private static final long serialVersionUID = 1L;
	/** El objeto FLUJO TITULO. */
	private static final String FLUJO_TITULO = "Operaci�n no permitida";

	/** El objeto RUTA FLUJO. */
	private static final String RUTA_FLUJO = "Operaci�n no permitida";

	/** El objeto ID FLUJO. */
	private static final String ID_FLUJO = "Dispersion";
	
	/**
	 * Redirecciona hacia el metodo doPost.
	 * @param request Request
	 * @param response Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Envia al usuario hacia la pagina de error de la aplicacion.
	 * @param request Request
	 * @param response Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("***Operacion no permitida :: doPost() :: " + "Intento de acceso a una operacion no permitida.", EIGlobal.NivelLog.INFO);
		despliegaPaginaError("Operaci\u00f3n no permitida.",FLUJO_TITULO, RUTA_FLUJO, ID_FLUJO, request, response);
	}
}
