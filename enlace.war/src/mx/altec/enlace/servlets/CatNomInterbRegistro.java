package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;

import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Importar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.RespInterbBean;
import mx.altec.enlace.dao.CatNominaInterbDAO;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.MAC_Interb;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailContainer;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import java.sql.*;

//VSWF


public class CatNomInterbRegistro extends BaseServlet
 {
	String pipe = "|";

	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		inicioServletAltaCuentas( req, res );
	 }
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		inicioServletAltaCuentas( req, res );
	 }

/*************************************************************************************/
/************************************************** inicioServletAltaCuentas		 */
/*************************************************************************************/
	public void inicioServletAltaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - execute(): Entrando a Alta de Empleados Nomina Interbancaria.", EIGlobal.NivelLog.INFO);
		String modulo = ( String ) getFormParameter(req, "Modulo" );
		if (modulo == null) {
			modulo = "0";
			EIGlobal.mensajePorTrace("\n\n MMC_Alta - inicioServletAltaCuentas - El modulo venía en null, se le asigna \"0\"" , EIGlobal.NivelLog.WARN);
		}
		EIGlobal.mensajePorTrace("\n\nEl modulo es: " + modulo + "\n\n", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("CatNomInterbRegistro - execute(): Estableciendo session.", EIGlobal.NivelLog.INFO);
		/* JGAL  */
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - execute(): El directorio al que van los archivos es: " + Global.DIRECTORIO_REMOTO_CUENTAS , EIGlobal.NivelLog.INFO);

		HashMap facultadesAltaCtas = new HashMap ();

		if(SesionValida( req, res ))
		{
			//*****************************************Inicia validacion OTP****************
			String valida = getFormParameter(req,"valida" );
			if ( validaPeticion( req, res,session,sess,valida))
			{
				  EIGlobal.mensajePorTrace("\n\n ENTRA A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.INFO);
			}
			//*****************************************Termina validacion OTP**************************************
			else
			{
				/* JGAL  */

				// Facultades ...
				facultadesAltaCtas.put ("AltaCtasBN", new Boolean (session.getFacultad(session.FAC_ALTA_BN) ));
				facultadesAltaCtas.put ("AutoCtasOB", new Boolean (session.getFacultad(session.FAC_AUTO_AB_OB)) );

				EIGlobal.mensajePorTrace("CatNomInterbRegistro - execute(): Leyendo facultades.", EIGlobal.NivelLog.INFO);

				if(req.getSession ().getAttribute ("facultadesAltaCtas")!=null)
	 			  req.getSession().removeAttribute("facultadesAltaCtas");
				req.getSession ().setAttribute ("facultadesAltaCtas", facultadesAltaCtas);

				if (!session.getFacultad(session.FAC_ALTA_BN) /* && !session.getFacultad(session.FAC_AUTO_AB_OB)*/)
				  despliegaPaginaErrorURL("<font color=red>No</font> tiene <i>facultades</i> para dar cuentas de <font color=green>Alta</font>.","Alta de cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Alta","s55205h", req, res);
			    else
				 {
				   if( modulo.equals("0") )
				   	try {

						iniciaAlta( req, res);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}

				   if( modulo.equals("1") )
					  agregaCuentas( req, res);
				   if( modulo.equals("2") )
				    {
						EIGlobal.mensajePorTrace("\n\n\n ANTES DE VERIFICACION DE TOKEN \n\n\n", EIGlobal.NivelLog.INFO);
						//interrumpe la transaccion para invocar la validaci?n
						if(  valida==null )
						{
							EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci?n est? parametrizada para solicitar la validaci?n con el OTP \n\n\n", EIGlobal.NivelLog.INFO);

							boolean solVal=ValidaOTP.solicitaValidacion(
										session.getContractNumber(),IEnlace.ALTA_CUENTAS);

							if( session.getValidarToken() &&
								session.getFacultad(session.FAC_VAL_OTP) &&
								session.getToken().getStatus() == 1 &&
								solVal )
							{
								//VSWF-HGG -- Bandera para guardar el token en la bit?cora
								req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
								EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit? la validaci?n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);
								guardaParametrosEnSession(req);
								//ValidaOTP.guardaParametrosEnSession(req);
								ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP);
							}
							else{
                                                            ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
                                                            valida="1";
                                                        }
						}
						//retoma el flujo
						if( valida!=null && valida.equals("1"))
						{
							EIGlobal.mensajePorTrace("\n\n\n SE RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.INFO);
							enviaCuentas( req, res);
							EIGlobal.mensajePorTrace("\n\n\n FIN de RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.INFO);

						}
					}
				   if( modulo.equals("3") )
					  presentaCuentas(req,res);
				 }
			}//Termina else de valida OTP
			req.getSession().removeAttribute("CuentasListaAlta");
		} // Termina sesion valida

		EIGlobal.mensajePorTrace("CatNomInterbRegistro - execute(): Saliendo de Alta de Cuentas.", EIGlobal.NivelLog.INFO);
	}

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	public int presentaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - presentaCuentas(): IniciaAlta.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String cuentasSel=(String)getFormParameter(req,"cuentasSel");
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - presentaCuentas(): Seleccionadas "+ cuentasSel, EIGlobal.NivelLog.INFO);

		String strTramaBN=(String)getFormParameter(req,"strTramaBN");

        String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/" + session.getUserID8()+".txt";
		String arcLinea="";

		EI_Tipo BN= new EI_Tipo();

		int reg=0;
		int BNr=0;
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - ====> strTramaBN : ["+ strTramaBN + "]", EIGlobal.NivelLog.INFO);
		BN.iniciaObjeto(strTramaBN);
		for (int i = 0; i < BN.camposTabla[0].length; i++) {
			EIGlobal.mensajePorTrace("CatNomInterbRegistro - ====> indice : 0, [" + i +"] = ["+ BN.camposTabla[0][i] + "]", EIGlobal.NivelLog.INFO);
		}
		if(BN.totalRegistros>=1)
        {

          EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
          EIGlobal.mensajePorTrace("CatNomInterbRegistro - presentaCuentas(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);
          if(ArcSal.creaArchivo())
           {
             arcLinea="";
             int c[]={0,1,4,2,6,7};			// BN Indice del elemento deseado
             int b[]={20,40,5,5,5,2};		// BN Longitud maxima del campo para el elemento

             //int c[]={0,1,3,4,5,7};			// BN Indice del elemento deseado
             //int b[]={20,40,5,5,20,5};		// BN Longitud maxima del campo para el elemento


             EIGlobal.mensajePorTrace("CatNomInterbRegistro - presentaCuentas(): Exportando Cuentas Bancos nacionales.", EIGlobal.NivelLog.INFO);
             for(int i=0;i<BN.totalRegistros;i++)
              {
				if(cuentasSel.charAt(reg+i)=='1')
				  {
					arcLinea="EXTRNA";
					if (BN.camposTabla[i][4].indexOf("+") > 0) {
						BN.camposTabla[i][4]=BN.camposTabla[i][4].substring(0,BN.camposTabla[i][4].indexOf("+"));
					}
					for(int j=0;j<c.length;j++)
					  arcLinea+=ArcSal.formateaCampo(BN.camposTabla[i][c[j]],b[j]);
					arcLinea+="\n";
					if(!ArcSal.escribeLinea(arcLinea))
					  EIGlobal.mensajePorTrace("CatNomInterbRegistro - presentaCuentas()s(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
					BNr++;
				  }
              }
			 reg+=BN.totalRegistros;

             ArcSal.cierraArchivo();
           }
          else
           {
             //strTabla+="<b>Exportacion no disponible<b>";
             EIGlobal.mensajePorTrace("CatNomInterbRegistro - presentaCuentas(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.INFO);
           }
        }

	    req.setAttribute("nombreArchivoImp",nombreArchivo);
		req.setAttribute("totalRegistros",Integer.toString(BNr));
		req.setAttribute("numeroCtasNacionales",Integer.toString(BNr));
		req.setAttribute("msg", "Fueron seleccionados " +Integer.toString(BNr)+" registros para solicitud de Alta.");
		//req.setAttribute("Modulo", "2");

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Alta de Cuentas","Administraci&oacute;n y control &gt; Cuentas  &gt; Alta","s26082h", req));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

        EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): Finalizando alta.", EIGlobal.NivelLog.WARN);
		evalTemplate("/jsp/CatNomInterb_AltaArchivo.jsp", req, res);
		return 1;
	}

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	public int enviaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - enviaCuentas(): IniciaAlta.", EIGlobal.NivelLog.WARN);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String nombreArchivoImp=(String)getFormParameter(req,"nombreArchivoImp");
		String totalRegistros=(String)getFormParameter(req,"totalRegistros");
		String strMensaje="";
		String nombreArchivo="";

		EIGlobal.mensajePorTrace("CatNomInterbRegistro - enviaCuentas(): nombreArchivoImp: "+nombreArchivoImp, EIGlobal.NivelLog.WARN);
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - enviaCuentas(): totalRegistros: "+totalRegistros, EIGlobal.NivelLog.WARN);

		String usuario ="";
		String contrato="";

		long lote = 0;

			//0 Envio Correcto
		    //1 Error en Envio
			//2 Envio con errores
			//3 Envio correcto (Pero no se pudo identificar si hubo duplicidad)

		nombreArchivo=nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf('/')+1);
		EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Nombre para la copia "+nombreArchivo, EIGlobal.NivelLog.INFO);

		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Se continua con el envio del archivo", EIGlobal.NivelLog.INFO);


		EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> enviaCuentas() - grabaCuentas.", EIGlobal.NivelLog.INFO);
 		CatNominaInterbDAO dao = new CatNominaInterbDAO();
 		String facultad = "";
 		if(session.getFacultad(session.FAC_ALTA_BN))
	 		if (session.getFacultad(session.FAC_AUTO_AB_OB))
	 			facultad = "AUTO";
	 		else
	 			facultad = "ALTA";

 		RespInterbBean resp  = dao.altaCuentasInterb(nombreArchivoImp, contrato, usuario, facultad);
 		lote = resp.getLote();
 		long rechazados = resp.getRechazados();
 		dao = null;
		EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> agregaCuentas() - El LOTE: [" + lote + "]", EIGlobal.NivelLog.INFO);

		if(lote == 0){
			strMensaje="Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Por favor intente mas tarde.<br><br>";
			despliegaPaginaErrorURL(strMensaje,"Alta de cuentas","Servicios &gt; N&oacute;mina Interbancaria &gt; Registro de Cuentas &gt; Alta","s55205h", req, res);
		}
	    else
		 {
	    	lanzaEmail(req, "1", "" + resp.getAutorizados(), "" + lote, resp.getDetalleCuentasAlta(), totalRegistros);
			strMensaje="<br>La informaci&oacute;n fue <font color=blue>enviada</font> y <b>procesada</b> por el sistema. Favor de tomar nota del n&uacute;mero de lote para operaciones futuras.<br>";
			strMensaje+="<br>En caso de haberse generado rechazos favor de consultar el detalle en la opci&oacute;n de <b>Autorizaci&oacute;n y Cancelaci&oacute;n.</b><br>";
			req.setAttribute("strMensaje",strMensaje);
			req.setAttribute("nombreArchivoImp",nombreArchivoImp);
			req.setAttribute("lote", "" + lote);
			req.setAttribute("rechazados", "" + rechazados);
			req.setAttribute("totalRegistros", totalRegistros);
			req.setAttribute("respBean", resp);

			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Alta de cuentas","Servicios &gt; N&oacute;mina Interbancaria &gt; Registro de Cuentas &gt; Alta","s26084h", req));

			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

			EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): Finalizando alta.", EIGlobal.NivelLog.WARN);
			evalTemplate("/jsp/CatNomInterb_RegistroEnvio.jsp", req, res);
		 }
	  return 1;
	}

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	public int agregaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): IniciaAlta.", EIGlobal.NivelLog.WARN);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): tipoAlta." + (String)getFormParameter(req,"TipoAlta"), EIGlobal.NivelLog.WARN);

		int tipoAlta=Integer.parseInt((String)getFormParameter(req,"TipoAlta"));
		String strTramaBN="";
		String tipoCuentaBN = "";
		String numeroCuentaBN="";
		String nombreTitularBN="";
		String cvePlaza="";
		String strPlaza="";
		String cveBanco="";
		String strBanco="";
		String strSuc="";
		String bancoBN="";
		strTramaBN=(String)getFormParameter(req,"strTramaBN");
		tipoCuentaBN=(String)getFormParameter(req,"tipoCuentaBN");
		numeroCuentaBN=(String)getFormParameter(req,"numeroCuentaBN");
		nombreTitularBN=(String)getFormParameter(req,"nombreTitularBN");

		String modulo="1";

		String JSP_TEMPLATE="/jsp/CatNomInterbRegistro.jsp";

		switch (tipoAlta)
		 {
			case 0: // Banco Nacional
			    EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): Banco Nacional - TipoCuenta: [" + tipoCuentaBN + "]", EIGlobal.NivelLog.WARN);
				if(tipoCuentaBN.equals("02")) {
					bancoBN = (String)getFormParameter(req,"bancoBN");
					strSuc = (String)getFormParameter(req,"sucursalBN");
			        cvePlaza = (String)getFormParameter(req,"cvePlazaBN");
				    strPlaza = (String)getFormParameter(req,"strPlazaBN");
					cveBanco = bancoBN.substring(0,bancoBN.indexOf("-")).trim();
					strBanco = bancoBN.substring(bancoBN.indexOf("-")+1).trim();
				} else {
				   cvePlaza = numeroCuentaBN.substring(3, 6);
				   cveBanco = numeroCuentaBN.substring(0, 3);
				   CatNominaInterbDAO dao = new CatNominaInterbDAO();
				   String [] datosBco = dao.obtenDatosBanco(Integer.parseInt(cveBanco));
				   dao = null;
				   strBanco = datosBco[0];
				}
			   strTramaBN +=
					         numeroCuentaBN		+ pipe +
					         nombreTitularBN	+ pipe +
					         cvePlaza			+ pipe +
					         strPlaza			+ pipe +
					         cveBanco			+ pipe +
					         strBanco			+ pipe +
					         strSuc				+ pipe +
					         tipoCuentaBN		+ "@";

/*
 * 				   strTramaBN +=
					         numeroCuentaBN		+ pipe +
					         nombreTitularBN	+ pipe +
					         cvePlaza			+ pipe +
					         strPlaza			+ pipe +
					         cveBanco			+ pipe +
					         strBanco			+ pipe +
					         strSuc				+ pipe +
					         tipoCuentaBN		+ "@";

 */
/*				   EmailContainer ecBN = new EmailContainer();
				   ecBN.setNumCuenta(numeroCuentaBN);
				   ecBN.setTitular(nombreTitularBN);
				   if(req.getSession().getAttribute("detalleCuentasAlta") == null){
					   Vector detalleCuentasAlta = new Vector();
					   detalleCuentasAlta.add(ecBN);
					   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
					}else{
					   Vector detalleCuentasAlta = (Vector)req.getSession().getAttribute("detalleCuentasAlta");
					   detalleCuentasAlta.add(ecBN);
					   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
					} */
			       break;
			case 1: // Archivo
			       EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): Por Archivo.", EIGlobal.NivelLog.WARN);
			       boolean var = importarArchivo(req,res);
			       EIGlobal.mensajePorTrace("__________________ var " + var, EIGlobal.NivelLog.INFO);
				   if(!var)
					 modulo="0";
				   else
					 JSP_TEMPLATE="/jsp/CatNomInterb_AltaArchivo.jsp";
			       break;
		 }

		EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): Finalizando alta.", EIGlobal.NivelLog.WARN);

		String numeroCuenta = "";
		switch (tipoAlta)
		 {
			case 0: // Banco Nacional
			       numeroCuenta = numeroCuentaBN;
				   break;
		 }
		if(req.getSession().getAttribute("CuentasListaAlta") == null){
		   List cuentas = new ArrayList();
		   cuentas.add(numeroCuenta);
		   req.getSession().setAttribute("CuentasListaAlta",cuentas);
		}else{
		   List cuentas = (List)req.getSession().getAttribute("CuentasListaAlta");
		   cuentas.add(numeroCuenta);
		   req.getSession().setAttribute("CuentasListaAlta",cuentas);
		}

		req.setAttribute("Modulo", modulo);

		if(tipoAlta!=3)
		 {
			CatNominaInterbDAO dao = new CatNominaInterbDAO();
		   req.setAttribute("tablaBN", generaTablaCuentas(strTramaBN,false));
		   req.setAttribute("strTramaBN",strTramaBN);
		   req.setAttribute("listaBancos", dao.obtenListaBancos());
		   req.setAttribute("comboBancos", traeBancos().toString());
		   dao = null;
		 }

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Alta de cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Alta &gt; Agregar","s26080h", req));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

        EIGlobal.mensajePorTrace("CatNomInterbRegistro - agregaCuentas(): Finalizando alta.", EIGlobal.NivelLog.WARN);
        EIGlobal.mensajePorTrace("_______________ JSP_TEMPLATE :" + JSP_TEMPLATE,EIGlobal.NivelLog.INFO);
        evalTemplate(JSP_TEMPLATE, req, res);

		return 1;

	}

/*************************************************************************************/
/************************************************************  importarArchivo       */
/*************************************************************************************/
 boolean importarArchivo( HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException
	 {
	    /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EI_Importar arcImp= new EI_Importar();
		MAC_Interb Registro;

		String nombreArchivoImp="";
		String nombreArchivoErr="";
		String usuario="";
		String sessionId="";
		String line="";

		/**GAE**/
//		Vector detalleCuentasAlta = new Vector();
		/**FIN GAE**/

		StringBuffer erroresRegistro=new StringBuffer("");

		Vector cuentas=new Vector();

		boolean erroresEnArchivo=false;

		int totalRegistros=0;
		int numeroCtasNacionales=0;
		int err=-1;

		//nombreArchivoImp=arcImp.importaArchivo(req,res);
		nombreArchivoImp=getFormParameter(req,"fileNameUpload");
		EIGlobal.mensajePorTrace("AQUI ESTA EL MEOLLO :" + nombreArchivoImp, EIGlobal.NivelLog.INFO);
		File f = new File(nombreArchivoImp);
		EIGlobal.mensajePorTrace("EXISTE :" + f.exists(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("TAMANO :" + f.length(), EIGlobal.NivelLog.INFO);


		sessionId = (String) req.getSession().getId();
		usuario=session.getUserID8();
		nombreArchivoErr="MAC_Errores."+usuario+".html";

		HashMap facultadesAltaCtas = new HashMap ();
		facultadesAltaCtas=(HashMap)req.getSession ().getAttribute ("facultadesAltaCtas");

		EIGlobal.mensajePorTrace("****************** nombreArchivoImp : " +nombreArchivoImp +"  *************" , EIGlobal.NivelLog.INFO);
			BufferedReader archImp= new BufferedReader( new FileReader(nombreArchivoImp) );
			BufferedWriter archErr = new BufferedWriter( new FileWriter(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivoErr) );

			File arcE=new File(nombreArchivoImp);
			File arcS=new File(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivoErr);

			EIGlobal.mensajePorTrace("CatNomInterbRegistro - importarArchivo(): Buscando archivos ... " + IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivoErr, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CatNomInterbRegistro - importarArchivo(): Buscando archivos ... " + nombreArchivoImp, EIGlobal.NivelLog.INFO);

			if(arcS.exists())
				archErr.write(erroresHead());

			if( (!arcE.exists()) || arcE.length()<50 )
		     {
			   EIGlobal.mensajePorTrace("CatNomInterbRegistro - importarArchivo(): Alguno de los archivos no se encontro", EIGlobal.NivelLog.INFO);
			   if(arcS.exists())
				   archErr.write("<br>\n<b><font color=red>Error general</font></b>\n<br><DD><LI>El archivo que especific&oacute; no contiene datos o fall&oacute; el env&iacute;o.");
			   erroresEnArchivo=true;
			 }
			else
			 {
				EIGlobal.mensajePorTrace("CatNomInterbRegistro - importarArchivo(): Comenzando la validaci?n del Archivo.", EIGlobal.NivelLog.INFO);

				/*Obtener catalogos de paises,bancos,plazas,divisas	*/
				/***********************************************************************************************/
				String sqlBanco  = "SELECT cve_interme from comu_interme_fin where num_cecoban<>0 order by cve_interme";
				String sqlPlaza  = "select PLAZA_BANXICO from comu_pla_banxico order by PLAZA_BANXICO";

				EI_Query BD= new EI_Query();
				Registro = new MAC_Interb();

				Registro.listaBancos =BD.ejecutaQueryCampo(sqlBanco);
				Registro.listaPlazas =BD.ejecutaQueryCampo(sqlPlaza);
				if( Registro.listaBancos==null ||
					Registro.listaPlazas==null )
				 {
				   EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
				   erroresEnArchivo=true;
				   erroresRegistro=new StringBuffer("");
				   erroresRegistro.append("<br>\n<b>General:</b>\n<br>");
				   erroresRegistro.append("<DD><LI>Alguno de los cat&aacute;logos no se pudo cargar y el archivo no se puede validar correctamente. Intente m?s tarde porfavor.");
				 }
				while( (line=archImp.readLine())!= null )
				  {
					if(line.length()==Registro.lenBN && !((Boolean)facultadesAltaCtas.get("AltaCtasBN")).booleanValue() )
					  {
						EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Sin facultad para Alta BN", EIGlobal.NivelLog.INFO);
						erroresEnArchivo=true;
						erroresRegistro=new StringBuffer("");
						erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
						erroresRegistro.append("<DD><LI>");
						erroresRegistro.append("No tiene <font color=blue>FACULTAD</font> para dar de alta cuentas de Bancos Externos");

						if(arcS.exists())
						  archErr.write(erroresRegistro.toString());
					  }
					else
					  {
						err = Registro.parse(line);
						EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Vector " + Registro.listaErrores.size() + " err "+ err, EIGlobal.NivelLog.INFO);

						if(err>0)
						  {
							 erroresEnArchivo=true;
							 erroresRegistro=new StringBuffer("");
							 erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
							 for(int i=0;i<Registro.listaErrores.size();i++)
							  {
								erroresRegistro.append("<DD><LI>");
								erroresRegistro.append(Registro.listaErrores.get(i).toString());
							  }
							 if(arcS.exists())
								archErr.write(erroresRegistro.toString());
						  }
						 else
						  {
							 if(cuentas.indexOf(Registro.cuenta)>=0)
							  {
								 EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> La cuenta ya existe en el archivo", EIGlobal.NivelLog.INFO);
								 erroresEnArchivo=true;
								 erroresRegistro=new StringBuffer("");
								 erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
								 erroresRegistro.append("<DD><LI>");
								 erroresRegistro.append("La cuenta ya <font color=green>existe</font> en el archivo: " + Registro.cuenta);

								 if(arcS.exists())
									archErr.write(erroresRegistro.toString());
							  }
							 else{
								 EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Cargando bean para email", EIGlobal.NivelLog.INFO);
								 EmailContainer ec = new EmailContainer();
								 ec.setNumCuenta(Registro.cuenta);
								 ec.setTitular(Registro.nombreTitular);
								 EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> cuenta email->" + ec.getNumCuenta(), EIGlobal.NivelLog.INFO);
								 //detalleCuentasAlta.add(ec);

								 cuentas.add(Registro.cuenta);
							 }
						  }
					  }

				    if(Registro.tipoRegistro.equals(Registro.EXT))
					  numeroCtasNacionales++;
					totalRegistros++;
					Registro.listaErrores.clear();
				  }
//				req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
			 }
			//TODO BIT CU5071, importar archivo de Alta de cuentas
			/*VSWF HGG I*/
			/*if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try {
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EA_CUENTAS_ALTA_IMPORTAR_ARCHIVO);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if (nombreArchivoImp != null) {
					bt.setNombreArchivo(nombreArchivoImp);
				}

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			}*/
			/*VSWF HGG F*/
			EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Cerrando los archivos .. ", EIGlobal.NivelLog.INFO);

			if(arcS.exists())
				archErr.write(erroresFoot());

			archErr.flush();
			archErr.close();
			archImp.close();
			cuentas.clear();
			cuentas = null;
			archImp = null;
			archErr = null;

	   if(erroresEnArchivo)
		 {
		   /* Nombre de archivo de errores para descarga en web. Sin path */
		   ArchivoRemoto archR = new ArchivoRemoto();
		   if(archR.copiaLocalARemoto(nombreArchivoErr,"WEB"))
			 req.setAttribute("ErroresEnArchivo","js_despliegaErrores('"+nombreArchivoErr+"');");
		   else
			 {
			   req.setAttribute("ErroresEnArchivo","cuadroDialogo('Durante la importaci&oacute;n se encontraron algunos errores en el archivo, por favor verifiquelo e intente nuevamente.', 3);");
			   EIGlobal.mensajePorTrace("CatNomInterbRegistro-> exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
			 }
		   return false;
		 }
		else
		 {
		   req.setAttribute("nombreArchivoImp",nombreArchivoImp);
		   req.setAttribute("totalRegistros",Long.toString(totalRegistros));
		   req.setAttribute("numeroCtasNacionales",Long.toString(numeroCtasNacionales));
		   req.setAttribute("msg", "Se importaron exitosamente " +Long.toString(totalRegistros)+" registros");
		 }

	   EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> Se validaron los registros: " + totalRegistros, EIGlobal.NivelLog.INFO);
	   return true;
	 }

/*************************************************************************************/
/************************************************************  generaTablaCuentasMB  */
/*************************************************************************************/
 String generaTablaCuentas(String strCtaTmp,boolean titulo)
	{
		if(strCtaTmp.trim().equals(""))
			return "";

		String [] titulos={"",
					   "Cuenta",
					   "Titular",
					   "Plaza",
					   "Banco"};

		int[] datos={5,2,0,1,2,5,6};
		int[] values={0,0,1,2,3,4,5,6};
		int[] align={1,0,0,0,1,0};

		if(titulo)
		 {
		   datos[0]=2;
		   titulos[0]="Cuentas Mismo Banco";
		 }
		else
		 {
		   datos[0]=4;
		   titulos[0]="Cuentas Bancos Nacionales";
		 }

		EI_Tipo ctaMB=new EI_Tipo();

		ctaMB.iniciaObjeto(strCtaTmp);
		return ctaMB.generaTabla(titulos,datos,values,align);
	}


/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	public int iniciaAlta( HttpServletRequest req, HttpServletResponse res)
		throws SQLException, Exception
	{
		EIGlobal.mensajePorTrace("CatNomInterbRegistro - iniciaAlta(): IniciaAlta.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		req.setAttribute("Modulo", "0");
		CatNominaInterbDAO dao = new CatNominaInterbDAO();

		req.setAttribute("comboBancos", traeBancos().toString());
		req.setAttribute("listaBancos", dao.obtenListaBancos());
		dao = null;

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Alta de Empleados Nomina Interbancaria","Servicios &gt; N&oacute;mina Interbancaria &gt; Registro de Cuentas &gt; Alta","s26080h", req));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		//TODO BIT CU5071, inicia el flujo de Alta Cuentas
		/*VSWF HGG I*/
/*		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			if(getFormParameter(req,BitaConstants.FLUJO) != null){
				bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.FLUJO));
			}else{
				bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			}
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CUENTAS_ALTA_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			BitaHandler.getInstance().insertBitaTransac(bt);

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}*/
		/*VSWF HGG F*/
        EIGlobal.mensajePorTrace("CatNomInterbRegistro - iniciaAlta(): Finalizando alta.", EIGlobal.NivelLog.WARN);
		evalTemplate("/jsp/CatNomInterbRegistro.jsp", req, res);
		return 1;
	}


/*************************************************************************************/
/****************************************************************** trae Divisas     */
/*************************************************************************************/
public StringBuffer traeBancos()
 {
	StringBuffer strOpcion=new StringBuffer("");
    String sqlQuery = "SELECT cve_interme,nombre_corto, num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto";

	Hashtable result=null;

	EI_Query BD= new EI_Query();

	result=BD.ejecutaQuery(sqlQuery);
	if(result==null) return strOpcion;
	String[] columns=new String[BD.getNumeroColumnas()];

	EIGlobal.mensajePorTrace("CatNomInterbRegistro - traeBancos(): Numero de datos. " + result.size() +" Columnas. " + BD.getNumeroColumnas(), EIGlobal.NivelLog.WARN);

	if(BD.getNumeroColumnas()==3)
	  {
		for(int i=0;i<result.size();i++)
		  {
			columns=(String[])result.get(""+i);

//			if(!columns[0].trim().equals("SERFI") && !columns[0].trim().equals("BANME"))
//			  {
				strOpcion.append ("<option CLASS='tabmovtex' value='");
				strOpcion.append (columns[0]);
				strOpcion.append ("+");
				strOpcion.append (columns[2]);
				strOpcion.append ("-");
				strOpcion.append (columns[1]);
				strOpcion.append ("'>");
				strOpcion.append (columns[1]);
				strOpcion.append ("\n");
//			  }
		  }
	  }
	return strOpcion;
  }

public String traeListaBancos()
{
	String lista = "";
   String sqlQuery = "SELECT cve_interme, num_cecoban from comu_interme_fin where num_cecoban<>0 order by num_cecoban";

	Hashtable result=null;

	EI_Query BD= new EI_Query();

	result=BD.ejecutaQuery(sqlQuery);
	if(result==null) return lista;
	String[] columns=new String[BD.getNumeroColumnas()];

	EIGlobal.mensajePorTrace("CatNomInterbRegistro - traeListaBancos(): Numero de datos. " + result.size() +" Columnas. " + BD.getNumeroColumnas(), EIGlobal.NivelLog.WARN);
	StringBuffer lstTmp = new StringBuffer();
	lstTmp.append("|");
	if(BD.getNumeroColumnas()==2)
	  {
		for(int i=0;i<result.size();i++)
		  {
			columns=(String[])result.get(""+i);

//			if(!columns[0].trim().equals("SERFI") && !columns[0].trim().equals("BANME"))
//			  {
				lstTmp.append(columns[1] + "|");
//			  }
		  }
	  }
	return lstTmp.toString();
 }

public String[] traeBanco(String clave)
{
	String[] dscBanco = {"", ""};
   String sqlQuery = "SELECT nombre_corto, cve_interme from comu_interme_fin where num_cecoban = " + clave;

	Hashtable result=null;

	EI_Query BD= new EI_Query();

	result=BD.ejecutaQuery(sqlQuery);
	if(result==null) return dscBanco;
	String[] columns=new String[BD.getNumeroColumnas()];

	EIGlobal.mensajePorTrace("CatNomInterbRegistro - traeBanco(): Numero de datos. " + result.size() +" Columnas. " + BD.getNumeroColumnas(), EIGlobal.NivelLog.WARN);
	StringBuffer lstTmp = new StringBuffer();
	if(BD.getNumeroColumnas()==2)
	  {
		for(int i=0;i<result.size();i++)
		  {
			columns=(String[])result.get(""+i);
				dscBanco[0] = columns[0];
				dscBanco[1] = columns[1];
		  }
	  }
	return dscBanco;
 }

/*************************************************************************************/
/********************************************************************* erroresHead   */
/*************************************************************************************/
 public String erroresHead()
	{

	  StringBuffer strH=new StringBuffer("");

	  strH.append("<html>");
	  strH.append("<head>\n<title>Estatus de Importaci&oacute;n</title>\n");
	  strH.append("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	  strH.append("</head>");
	  strH.append("<body bgcolor='white'>");
	  strH.append("<form>");
	  strH.append("<table border=0 width=420 class='textabdatcla' align=center>");
	  strH.append("<tr><th class='tittabdat'>Errores en archivo de importaci&oacute;n</th></tr>");
	  strH.append("<tr><td class='texencconbol' align=center>");
	  strH.append("<table border=0 align=center width='100%'><tr><td class='texencconbol' align=center width=60>");
	  strH.append("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
	  strH.append("<td class='texencconbol' align='center'><br>No se llevo a cabo la importaci&oacute;n del archivo seleccionado<br>ya que se encontraron los siguientes errores.<br><br>");
	  strH.append("</td></tr></table>");
	  strH.append("</td></tr>");
	  strH.append("<tr><td class='texencconbol'>");

	  return strH.toString();
	}

/*************************************************************************************/
/********************************************************************* erroresFoot   */
/*************************************************************************************/
 public String erroresFoot()
	{

	  StringBuffer strF=new StringBuffer("");

	  EIGlobal.mensajePorTrace("CatNomInterbRegistro - erroresFoot(): Escribiendo final de errores.", EIGlobal.NivelLog.INFO);

	  strF.append("</td></tr>");
	  strF.append("<tr><td class='texencconbol' ><br>Revise el <font color=blue>archivo</font> e intente nuevamente.<br><br>");
	  strF.append("</td></tr>");
	  strF.append("</table>");
	  strF.append("<table border=0 align=center >");
	  strF.append("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	  strF.append("</table>");
	  strF.append("</form>");
	  strF.append("</body>\n</html>");

	  return strF.toString();
	}

/*************************************************************************************/
/********************************************************** despliegaPaginaErrorURL  */
/*************************************************************************************/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		param3="s55205h";

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,request ) );

		request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "CatNomInterbRegistro?Modulo=0");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}

	public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().removeAttribute("bitacora");

        HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();

			EIGlobal.mensajePorTrace("valida-> " + getFormParameter(request,"valida"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("Modulo-> " + getFormParameter(request,"Modulo"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("nombreArchivoImp-> " + getFormParameter(request,"nombreArchivoImp"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("totalRegistros-> " + getFormParameter(request,"totalRegistros"), EIGlobal.NivelLog.INFO);

			String value;

			value = (String) getFormParameter(request,"valida");
			if(value != null && !value.equals(null)) {
				tmp.put("valida",getFormParameter(request,"valida"));
			}

			value = (String) getFormParameter(request,"Modulo");
			if(value != null && !value.equals(null)) {
            	tmp.put("Modulo",getFormParameter(request,"Modulo"));
			}

			value = (String) getFormParameter(request,"nombreArchivoImp");
			if(value != null && !value.equals(null)) {
            	tmp.put("nombreArchivoImp",getFormParameter(request,"nombreArchivoImp"));
			}

			value = (String) getFormParameter(request,"totalRegistros");
			if(value != null && !value.equals(null)) {
            	tmp.put("totalRegistros",getFormParameter(request,"totalRegistros"));
			}

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
		EIGlobal.mensajePorTrace("\n\n\n Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
    }

	/**
	 * GAE
	 * Metodo encargado de disparar la rutina de confirmaci?n via Email
	 *
	 * @param req					Request del sistema
	 * @param datoSpecial			Trama final
	 * @param regAceptados			Registros Rechazados
	 */
	private void lanzaEmail(HttpServletRequest req, String datoSpecial, String regAceptados, String idLote, Vector detalleCuentasAlta, String numReg){
		try{
		EmailSender es = new EmailSender();
		HttpSession ses = req.getSession();

		BaseResource session = (BaseResource) ses.getAttribute("session");
		String numContrato = session.getContractNumber();
		String descContrato = session.getNombreContrato();
		String numUsuario = session.getUserID8();
//		Vector detalleCuentas = (Vector)req.getSession().getAttribute("detalleCuentasAlta");

		EIGlobal.mensajePorTrace("lanzaEmail-> datoSpecial: [" + datoSpecial+ "]" , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("lanzaEmail-> regAceptados: [" + regAceptados+ "]" , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("lanzaEmail-> idLote: [" + idLote+ "]" , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("lanzaEmail-> detalleCuentas: [" + detalleCuentasAlta.size() + "]" , EIGlobal.NivelLog.INFO);
		if(datoSpecial.equals("1") && detalleCuentasAlta.size() > 0 && Integer.parseInt(regAceptados)>0){
			//es.confirmaEmail(detalleCuentas, numContrato, descContrato, numUsuario, idLote);
			es.confirmaEmailInterb(detalleCuentasAlta, numContrato, descContrato, req, idLote, session, numReg);
			req.getSession().removeAttribute("detalleCuentasAlta");
		}else{
			EIGlobal.mensajePorTrace("lanzaEmail-> Notificación no hecha ", EIGlobal.NivelLog.INFO);
		}
		}catch(Exception e)
		{EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

		}
	}


 }