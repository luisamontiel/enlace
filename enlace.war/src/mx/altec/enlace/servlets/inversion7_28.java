package mx.altec.enlace.servlets;

import java.util.*;
import java.io.IOException;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.sql.*;
import java.io.*;
import java.lang.*;

public class inversion7_28 extends BaseServlet {

        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
                defaultAction( request, response );
        }
        public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
                defaultAction( request, response );
        }
        public void defaultAction( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {

          boolean sesionvalida = SesionValida( request, response );
      EIGlobal.mensajePorTrace("***inversion7_28.class Entrando a execute ", EIGlobal.NivelLog.INFO);
      //request.setAttribute("MenuPrincipal", session.getstrMenu());
      //request.setAttribute("Fecha", ObtenFecha());

	  HttpSession sess = request.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");
     if (session!=null) {
             if(session.getStrMenu()!=null)
                          request.setAttribute("MenuPrincipal", session.getStrMenu());
                 else
                          request.setAttribute("MenuPrincipal", "");
                 if(session.getFuncionesDeMenu()!=null)
                      request.setAttribute("newMenu",       session.getFuncionesDeMenu());
                 else
                      request.setAttribute("newMenu",       "");
                 request.setAttribute("Encabezado", CreaEncabezado("Plazos", "Plazos",request));
          }

      //request.setAttribute("MenuPrincipal", session.getStrMenu());
      //request.setAttribute("newMenu", session.getFuncionesDeMenu());
      //request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n 7/28", "Operaciones inversi&oacute;n 7/28"));

      if (session.getFacultad(session.FAC_DISPONIBLE_INVERSION))
      {
        String funcion_llamar=request.getParameter("ventana");
        EIGlobal.mensajePorTrace("***inversion7_28.class funcion_llamar "+funcion_llamar, EIGlobal.NivelLog.INFO);
        if (funcion_llamar.equals("0"))
          pantalla_inicial_inversion7_28( request, response );
        else if (funcion_llamar.equals("1"))
          pantalla_contrato_inversion7_28(request, response);
        else if (funcion_llamar.equals("2"))
          ejecucion_operaciones_inversion7_28( request, response );
                else if (funcion_llamar.equals("3"))
                {
          consultar_tasas(request,response,request.getParameter("subproducto"),request.getParameter("producto"),request.getParameter("opcion"));
                }
                  //*******************
       } else if (sesionvalida){
             //***********************
          //request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
          //evalTemplate(IEnlace.ERROR_TMPL,(ITemplateData) null, map);
                  despliegaPaginaError("Lo sentimos, la p&aacute;gina que usted solicit&oacute; no est&aacute; disponible ya que usted no tiene privilegios de acceso a este servicio","Plazo","Plazo","s26170h",
                                request, response );
       }
    }



     public void pantalla_inicial_inversion7_28( HttpServletRequest request, HttpServletResponse response )
                        throws ServletException, IOException {

      EIGlobal.mensajePorTrace("***inversion7_28.class Entrando a pantalla_inicial_inversion7_28 ", EIGlobal.NivelLog.INFO);

      int salida=0;
      boolean fac_programadas;
      String campos_fecha="";

 	  HttpSession sess = request.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

	  String strInhabiles =diasInhabilesDesp();//armaDiasInhabilesJS();
      boolean cadena_getFacCOpProg=session.getFacultad(session.FAC_PROGRAMA_OP);

      fac_programadas=cadena_getFacCOpProg;
      String fecha_hoy="";
      fecha_hoy=ObtenFecha();

      campos_fecha=poner_campos_fecha(fac_programadas);
      request.setAttribute("diasInhabiles",diasInhabilesDesp());
      request.setAttribute("tabla","");
      request.setAttribute("fecha_hoy",fecha_hoy);
      request.setAttribute("ContUser",ObtenContUser(request));
      request.setAttribute("campos_fecha",campos_fecha);
      request.setAttribute("tabla_plazos","\""+Obtener_plazos()+"\"");
      request.setAttribute("tabla_instrumentos","\""+Obtener_instrumentos()+"\"");


      if (fac_programadas==false)
         request.setAttribute("fac_programadas1","0");
      else
         request.setAttribute("fac_programadas1","1");
      request.setAttribute("MenuPrincipal", session.getStrMenu());
      request.setAttribute("newMenu", session.getFuncionesDeMenu());
      request.setAttribute("Encabezado", CreaEncabezado("Plazos","Tesorer&iacute;a &gt; Inversiones &gt; Plazo ","s26170h",request));
      request.setAttribute("pantalla_contratos","<option value=0>Seleccione Contrato </option>");
      request.setAttribute("pantalla_instrumentos","<option value=0>Seleccione Tipo de inversi&oacute;n</option>");
      request.setAttribute("WEB_APPLICATION",Global.WEB_APPLICATION);

	  session.setModuloConsultar(IEnlace.MPlazo_728+IEnlace.MPlazo_dolares);
      evalTemplate( IEnlace.MIS_OPERINV728_TMPL, request, response );

    }



	  public void pantalla_contrato_inversion7_28( HttpServletRequest request, HttpServletResponse response )
                        throws ServletException, IOException {

      EIGlobal.mensajePorTrace("***inversion7_28.class Entrando a pantalla_contrato_inversion7_28 ", EIGlobal.NivelLog.INFO);
	  String cuenta= request.getParameter ("cuenta");
	  String tipo=   request.getParameter ("ctatipre");
	  String descrip= request.getParameter ("descrip");
	  String divisa = request.getParameter ("ctatipro");
      EIGlobal.mensajePorTrace("***inversion7_28.class cuenta "+cuenta, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("***inversion7_28.class tipo "+tipo, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("***inversion7_28.class descrip "+descrip, EIGlobal.NivelLog.INFO);
      EIGlobal.mensajePorTrace("***inversion7_28.class divisa "+divisa, EIGlobal.NivelLog.INFO);

	  HttpSession sess = request.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

	  if (divisa==null)
		  divisa="1";


      int salida=0;
      boolean fac_programadas;
      String campos_fecha="";
      String strInhabiles =diasInhabilesDesp();//armaDiasInhabilesJS();
      boolean cadena_getFacCOpProg=session.getFacultad(session.FAC_PROGRAMA_OP);
      fac_programadas=cadena_getFacCOpProg;
      String fecha_hoy="";
      fecha_hoy=ObtenFecha();
	  String instrumentos=Obtener_instrumentos();



      campos_fecha=poner_campos_fecha(fac_programadas);
      request.setAttribute("diasInhabiles",diasInhabilesDesp());
      request.setAttribute("tabla","");
      request.setAttribute("fecha_hoy",fecha_hoy);
      request.setAttribute("ContUser",ObtenContUser(request));
      request.setAttribute("campos_fecha",campos_fecha);
      request.setAttribute("tabla_plazos","\""+Obtener_plazos()+"\"");
      request.setAttribute("tabla_instrumentos","\""+instrumentos+"\"");


      if (fac_programadas==false)
         request.setAttribute("fac_programadas1","0");
      else
         request.setAttribute("fac_programadas1","1");
      request.setAttribute("MenuPrincipal", session.getStrMenu());
      request.setAttribute("newMenu", session.getFuncionesDeMenu());
      request.setAttribute("Encabezado", CreaEncabezado("Plazos","Tesorer&iacute;a &gt; Inversiones &gt; Plazo ","s26170h",request));


	  String contratos=Obtener_Contratos_Inversion(cuenta);
	  //System.out.println("contratos "+contratos);
      if(contratos==null)
	  {
        request.setAttribute("pantalla_contratos","<option value=0>Seleccione Contrato </option>");
        request.setAttribute("pantalla_instrumentos","<option value=0>Seleccione Tipo de inversi&oacute;n</option>");
	    request.setAttribute("ArchivoErr","cuadroDialogo(\"No existen contratos de inversi&oacute;n asociados a su cuenta \", 3);");

	  }
	  else
	  {
		request.setAttribute("pantalla_contratos",obten_selec_contratos(contratos));
		String datoscta[]=obten_datos_primer_contrato(contratos);
        request.setAttribute("pantalla_instrumentos",obten_selec_inst_pri_contrato(instrumentos,datoscta[0],datoscta[1],divisa));
        request.setAttribute("producto",datoscta[0]);
        request.setAttribute("subproducto",datoscta[1]);
		if(divisa.equals("6"))
            request.setAttribute("moneda","USD");
		else
            request.setAttribute("moneda","MN");
        request.setAttribute("destino","\""+cuenta+"|"+tipo+"|"+descrip+"|"+divisa+"|"+"\"");
		request.setAttribute("textdestino","\""+cuenta+"  "+descrip+"\"");
      }


	  request.setAttribute("WEB_APPLICATION",Global.WEB_APPLICATION);

	  session.setModuloConsultar(IEnlace.MPlazo_728+IEnlace.MPlazo_dolares);

      evalTemplate( IEnlace.MIS_OPERINV728_TMPL, request, response );

    }





   public void  ejecucion_operaciones_inversion7_28( HttpServletRequest request,HttpServletResponse response ) throws ServletException, IOException {

       EIGlobal.mensajePorTrace("***inversion7_28.class Entrando a ejecucion_operaciones_inversion7_28 ", EIGlobal.NivelLog.INFO);

	   HttpSession sess = request.getSession();
       BaseResource session = (BaseResource) sess.getAttribute("session");

       String mensaje_salida="";
       String coderror="";
       String coderror_operacion="";
       String clave_perfil="";
       String contrato="";
       String tipo_operacion="";
       String usuario="";
       String cta_destino="";
       String importe_string="";
       double importe=0.0;
       String opcion="";
       String sfecha_programada="";
       String bandera_fecha_programada="1";
       String fecha_programada_b="";
       int salida=0;
       Double importedbl;
       boolean programada=false;
       String tipo_cta_destino="";
       String cadena_cta_destino="";
       int indice=0;
       String fecha="";
       String fecha_hoy="";
       String tipo_cuenta="";
       String tipo_prod="";

       String indicador1="";
       String indicador2="";
       String num_promotor="";
       String plazo="";
       String instr="";
       String contrato_inversion="";
       String salida_buffer="";
       String cont_cuentas="";
       String arrcont_cuentas="";
	   String divisa="";

       String[] arrCuentas=new String[4];
       String trama_entrada="";
       String instrumento="";
	   String producto="";
	   String tipo_inversion="";



           //TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();

           ServicioTux tuxGlobal = new ServicioTux();

           //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

//         Hashtable htResult = null;

           String  sreferencia="";
       String mensajeerror="";
           String tramaok="";


       cadena_cta_destino = request.getParameter ("destino");
       contrato_inversion = request.getParameter ("contratos");
       importe_string     = request.getParameter ("importe");
       plazo              = request.getParameter ("plazo");
       instr              = request.getParameter ("inst1");
       fecha              = request.getParameter ("fecha");
	   producto           = request.getParameter ("producto");
	   instrumento        = request.getParameter ("instrumento");
	   fecha_hoy=ObtenFecha();

       contrato_inversion = contrato_inversion.substring(0,contrato_inversion.indexOf("|"));


        EIGlobal.mensajePorTrace("***inversion7_28.class cadena_cta_destino:"+cadena_cta_destino, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***inversion7_28.class cont_cuentas:"+contrato_inversion, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***inversion7_28.class importe_string:"+importe_string, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***inversion7_28.class plazo:"+plazo, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***inversion7_28.class instr:"+instr, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***inversion7_28.class fecha:"+fecha, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***inversion7_28.class producto:"+producto, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***inversion7_28.class subproducto:"+instrumento, EIGlobal.NivelLog.INFO);


        String trama="";
        String[] resultManc={""};

       contrato=session.getContractNumber();
       usuario=session.getUserID();
       clave_perfil=session.getUserProfile();


        mensaje_salida="<table width=760 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
        mensaje_salida=mensaje_salida+"<TR>";
        mensaje_salida=mensaje_salida+"<td width=80 class=tittabdat align=center>Cuenta</td>";
        mensaje_salida=mensaje_salida+"<td width=80 class=tittabdat align=center>Contrato</td>";
        mensaje_salida=mensaje_salida+" <td class=tittabdat align=center width=65>Tipo de Inversi&oacute;n</td>";
        mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=60>Plazo</td>";
        mensaje_salida=mensaje_salida+" <td class=tittabdat align=center width=65>Tasa</td>";
                mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=150>Instrucci&oacute;n al vencimiento</td>";
        mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=90>Importe</td>";
                mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=75>Fecha de aplicaci&oacute;n</td>";
                mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=66>Estatus</td>";
        mensaje_salida=mensaje_salida+" <td class=tittabdat align=center width=65>Referencia</td>";

                mensaje_salida=mensaje_salida+"</TR>";

      // cadena_cta_destino="65500362415|P|BRIGHTSTAR DE MEXICO S.A|";
       //    contrato_inversion="44000814144";
       arrCuentas= desentramaC("4|" + cadena_cta_destino,'|');
       cta_destino = arrCuentas[1];
       tipo_cta_destino=arrCuentas[2];
	   divisa=arrCuentas[4];

       if (divisa.equals("6"))
          tipo_operacion="DIDV";
	   else
		  tipo_operacion="CPDV";

       importe = new Double (importe_string).doubleValue();

       sfecha_programada=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());

       String manc_fecha_prog=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());

       programada=checar_si_es_programada(fecha);

       //tipo_prod="78";
       tipo_prod=producto+instrumento;

       if (instr.equals("1")) {
           indicador1="S";
           indicador2="S";
       } else if (instr.equals("2")) {
           indicador1="S";
           indicador2="N";
       } else if (instr.equals("3")) {
           indicador1="N";
           indicador2="N";
       }

       if (programada==false)
            trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_destino+"|"+tipo_cta_destino+"|"
                          +importe_string+"|"+contrato_inversion+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";
       else
            trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+cta_destino+"|"+tipo_cta_destino+"|"
                          +importe_string+"|"+contrato_inversion+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";

       EIGlobal.mensajePorTrace("***inversion7_28 WEB_RED-inversion7_28>"+trama_entrada, EIGlobal.NivelLog.DEBUG);

           try {
                Hashtable htResult = tuxGlobal.web_red( trama_entrada );
        coderror = ( String ) htResult.get( "BUFFER" );
           } catch ( java.rmi.RemoteException re ) {
                re.printStackTrace();
           } catch (Exception e) {}
                EIGlobal.mensajePorTrace("***inversion7_28 coderror normal<<"+coderror, EIGlobal.NivelLog.DEBUG);


           //coderror="OK|CABO0000|1231321231231231212312312312312312|312312312312312312312131231231987212308071230987123";

           if (coderror == null)  {
                                despliegaPaginaError("Proceso fuera de servicio. ","Inversi&oacute;n7_28","Inversi&oacute;n7_28","s26170h",request,response);
           }else {
                                EIGlobal.mensajePorTrace("***inversion7_28 simulado<<"+coderror, EIGlobal.NivelLog.DEBUG);
                                coderror_operacion="";
                                salida_buffer="";
                                if ((coderror.length()>=16)&&(!(coderror.substring(0,4).equals("MANC"))))
                                                sreferencia=coderror.substring(8,16);
                                sreferencia=sreferencia.trim();
                                EIGlobal.mensajePorTrace("***inversion7_28 sreferencia<<"+sreferencia, EIGlobal.NivelLog.DEBUG);

                                mensaje_salida=mensaje_salida+"<TR>";
                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs nowrap align=center>"+cta_destino+"</TD>";
                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs nowrap align=center>"+contrato_inversion+"</TD>";
                                tipo_inversion=Obtener_tipo_inversion(producto,instrumento);
								mensaje_salida=mensaje_salida+"<td class=textabdatobs align=center width=65>"+tipo_inversion+"</td>";
	                            instrumento=instrumento.substring(2,instrumento.length());
                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center>"+ plazo +" dias </TD>";
                                //mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center>"+obten_tasa(Integer.parseInt(plazo),importe,producto,instrumento,request)+"%</TD>";
                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center>"+obten_tasa_BD07(Integer.parseInt(plazo),importe,producto,instrumento,request)+"%</TD>";
								if (instr.equals("1"))
                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left>Reinvertir Capital e interes</TD>";
                                else if (instr.equals("2"))
                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left>Reinvertir Capital</TD>";
                                else if (instr.equals("3"))
                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left>Transferir Capital e intereses</TD>";

                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=right nowrap>"+FormatoMoneda(importe_string)+"</TD>";
                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+fecha+"</TD>";

                                if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n Mancomunada</TD>";
                                else if ((coderror.substring(0,2).equals("OK"))&&(programada==true))
                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n programada</TD>";
                                else if ((coderror.substring(0,2).equals("OK"))&&(programada==false))
                                {
                                                coderror_operacion=coderror.substring(coderror.indexOf("|")+1,coderror.length());
                                                coderror_operacion=coderror_operacion.substring(0,coderror_operacion.indexOf("|"));
                                                if (coderror_operacion.equals("CABO0000"))
                                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Tranferencia aceptada</TD>";
                                                else
                                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";
                                }
                                else
                                {
                                                if (programada==true)
                                                        mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n NO programada </TD>";
                                                else
                                                {
                                                        if (coderror.substring(0,4).equals("MANC"))
                                                                        mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(8,coderror.length())+"</TD>";
                                                        else if ((coderror.length()>32)&&(!(coderror.substring(0,4).equals("MANC"))))
                                                                        mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(32,coderror.length())+"</TD>";
                                                        else
                                                                        mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";
                                                }
                                }
                                if (coderror.substring(0,2).equals("OK"))
                                {
                                                EIGlobal.mensajePorTrace("***inversion7_28 simulado<<"+coderror, EIGlobal.NivelLog.DEBUG);

                                                if (programada==false)
                                                {
                                                                if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
                                                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
                                                                else
                                                                {
                                                                                coderror_operacion=coderror.substring(coderror.indexOf("|")+1,coderror.length());
                                                                                salida_buffer=coderror_operacion;
                                                                                coderror_operacion=coderror_operacion.substring(0,coderror_operacion.indexOf("|"));
                                                                                salida_buffer=salida_buffer.substring(salida_buffer.indexOf("|")+1,salida_buffer.length());
                                                                                salida_buffer=salida_buffer.substring(0,salida_buffer.indexOf("|"));

                                                            EIGlobal.mensajePorTrace("***inversion7_28 coderror_operacion<<"+coderror_operacion, EIGlobal.NivelLog.DEBUG);

                                                                                if (coderror_operacion.equals("CABO0000"))
                                                                                {
                                                                                           // mensaje_salida=mensaje_salida+"<TD ALIGN=RIGHT><A HREF=\"/cgi-bin/gx.cgi/AppLogic+EnlaceMig.comprobante_trans?tipo=4&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cta_destino+"&cta_destino="+contrato_inversion+"&plazo="+plazo+"&enlinea=s\">"+sreferencia+"</A></TD>";
                                                                                           tramaok+="1|5|"+sreferencia.trim()+"|"+importe_string+"|"+cta_destino+"|"+contrato_inversion+"| |"+plazo+"|"+tipo_inversion+"@";
                                                                                           mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value, 1);\" >"+sreferencia+"</A></TD>";

                                                                                }
                                                                                else
                                                                               mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";

                                                                 }
                                                  }
                                                  else
                                                mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";

                           }
                           else if (coderror.substring(0,4).equals("MANC"))
                              mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
                           else
                             mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";



       mensaje_salida=mensaje_salida+"</TR>";
       mensaje_salida=mensaje_salida+"</TABLE>";
           mensaje_salida+="<table width=760 border=0 cellspacing=2 cellpadding=0><tr><td valign=top align=left class=textabref width=372>Si desea obtener su comprobante, d&eacute; click en el n&uacute;mero de referencia subrayado.</td></tr></table>";

       request.setAttribute("titulo","Inversiones 7-28 Procesadas");
       request.setAttribute("mensaje_salida",mensaje_salida);
       request.setAttribute("fecha_hoy",fecha_hoy);
       //request.setAttribute("ContUser",ObtenContUser());
       //request.setAttribute("MenuPrincipal", session.getstrMenu());
       request.setAttribute("Fecha", ObtenFecha());
       request.setAttribute("MenuPrincipal", session.getStrMenu());
       request.setAttribute("newMenu", session.getFuncionesDeMenu());
       request.setAttribute("Encabezado", CreaEncabezado("Plazos","Tesorer&iacute;a &gt; Inversiones &gt; Plazo ","s26180h",request));
       request.setAttribute("tramaok","\""+tramaok+"\"");
       request.setAttribute("contadorok","1");
       request.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
       request.setAttribute("datosusuario","\""+session.getUserID()+"  "+session.getNombreUsuario()+"\"");
       request.setAttribute("banco",session.getClaveBanco());
       //System.out.println("banco "+session.getClaveBanco());
       String banco=session.getClaveBanco();
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //HttpSession ses = request.getSession();
       sess.setAttribute("banco",session.getClaveBanco());
       sess.setAttribute("titulo","Inversiones Plazo Procesadas");
       sess.setAttribute("mensaje_salida",mensaje_salida);
       sess.setAttribute("fecha_hoy",fecha_hoy);
       //ses.setAttribute("ContUser",ObtenContUser());
       //ses.setAttribute("MenuPrincipal", session.getstrMenu());
       sess.setAttribute("Fecha", ObtenFecha());
       sess.setAttribute("MenuPrincipal", session.getStrMenu());
       sess.setAttribute("newMenu", session.getFuncionesDeMenu());
       sess.setAttribute("Encabezado", CreaEncabezado("Plazos","Tesorer&iacute;a &gt; Inversiones &gt; Plazo ","s26180h",request));
       sess.setAttribute("tramaok","\""+tramaok+"\"");
       sess.setAttribute("contadorok","1");
       sess.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
       sess.setAttribute("datosusuario","\""+session.getUserID()+"  "+session.getNombreUsuario()+"\"");
       sess.setAttribute("web_application",Global.WEB_APPLICATION);

       
       		//<vswf:meg cambio de NASApp por Enlace 08122008>
           response.sendRedirect(response.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+IEnlace.MI_OPER_TMPL));


       //evalTemplate(IEnlace.MI_OPER_TMPL, request, response );
        } //El codigo de error no fue nulo

    }






        public int consultar_tasas(HttpServletRequest req,HttpServletResponse res,String subproducto, String producto,String opcion) throws ServletException, IOException
    {
        String codigo="";
                String titulo="";

   	    if(subproducto.length()==4)
		  subproducto=subproducto.substring(2, 4);

		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

	    EIGlobal EnlaceGlobal = new EIGlobal(session,getServletContext(),this);


                if ((subproducto!=null)&&(producto!=null))
		        {
				//if  (EnlaceGlobal.llamada_consulta_tasas_plazos_por_servicio(subproducto+"|"+producto+"|"))
		        //{
                  if(opcion.equals("V"))
		          {
				    titulo="Tasas Vista";
				    //codigo=tabla_vista(subproducto);
				    codigo=tabla_vista_BD07(subproducto,producto,req);
				  }
				  else
		          {
				    titulo="Tasas Inversiones";
				    //codigo=tabla_inversiones(subproducto);
				    codigo=tabla_inversiones_BD07(subproducto,producto,req);
		          }
				//}
                }

				EIGlobal.mensajePorTrace("***consultar_tasas:"+codigo, EIGlobal.NivelLog.DEBUG);
                req.setAttribute("codigo",codigo);
                req.setAttribute("titulo",titulo);
        evalTemplate("/jsp/tasas_inversion.jsp",req,res);
                return 0;
    }


        //Hace consulta para obtener las tasas de plazo
    public String tabla_inversiones(String subproducto)
                throws ServletException, IOException
   {
        EIGlobal.mensajePorTrace("***tabla_inversiones", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***subproducto:"+subproducto, EIGlobal.NivelLog.DEBUG);

		String cadena_resultante="";
        String tipo_persona="";

		if (subproducto.equals("43")||subproducto.equals("62")||subproducto.equals("64")||subproducto.equals("69"))
			tipo_persona="F";
		else if (subproducto.equals("42")||subproducto.equals("48"))
			tipo_persona="J";
        else
			tipo_persona="T";
         BufferedReader in=null;
        try
        {
           String linea="";
           String datos[]=null;
           in=new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR+ "/tasas.dat" ));

           cadena_resultante+="<TR>";
           cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Plazo</TD>";//Plazo
           cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Menor</TD>";//rango_menor
           cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Mayor</TD>";//rango_mayor
           cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Tasa</TD>";//tasa
           cadena_resultante+="</TR>";

           while ((linea=in.readLine())!=null)
           {
              //sprintf( linea,"%s|%s|%s|%f|%f|%f|",fecha_aux,element->instrumento,element->persona_juridica,element->plazo,element->rango_menor,element->rango_mayor,element->tasa);
              datos = desentramaC("7|"+linea,'|');
              if(datos!=null)
              {
                 if ((datos[3].equals(tipo_persona))&&
                     (datos[2].substring(0,1).equals("-")) )
				 {
                     cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+datos[4] + "</TD>";//rango_menor
					 cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+datos[5] + "</TD>";//rango_menor
                     cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+datos[6] + "</TD>";//rango_mayor
                     cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+datos[7] + "</TD>";//tasa
                     cadena_resultante+="</TR>";
                  }
              }
           }
        }
        catch(IOException e)
        {
              EIGlobal.mensajePorTrace("***inversion7_28.class: IOException"+e, EIGlobal.NivelLog.ERROR);
        }
        catch(Exception e)
        {
              EIGlobal.mensajePorTrace("***inversion7_28 exception de otro tipo "+e, EIGlobal.NivelLog.ERROR);
        }
		finally
		 {
              try
			  {
				  in.close();
			  }
			  catch(Exception e)
			  {
				   e.printStackTrace();
              }
         }

       //EIGlobal.mensajePorTrace("***inversion7_28 cadena_resultante "+cadena_resultante, EIGlobal.NivelLog.ERROR);

       return cadena_resultante;
   }



        //Hace consulta para obtener las tasas de plazo
    public String tabla_vista(String subproducto)
                throws ServletException, IOException
    {
        EIGlobal.mensajePorTrace("***tabla_vista", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***subproducto:"+subproducto, EIGlobal.NivelLog.DEBUG);

		String cadena_resultante="";
		String tipo_persona="";
		if (subproducto.equals("61")) //este es ek subproducto para cuentas tipo 60
			tipo_persona="F";
		else
			tipo_persona="J";
         BufferedReader in=null;
        try
        {
           String linea="";
           String datos[]=null;
           in=new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR+ "/tasas.dat" ));

           cadena_resultante+="<TR>";
           cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Menor</TD>";//rango_menor
           cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Mayor</TD>";//rango_mayor
           cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Tasa</TD>";//tasa
           cadena_resultante+="</TR>";

           while ((linea=in.readLine())!=null)
           {
              //sprintf( linea,"%s|%s|%s|%f|%f|%f|",fecha_aux,element->instrumento,element->persona_juridica,element->plazo,element->rango_menor,element->rango_mayor,element->tasa);
              datos = desentramaC("7|"+linea,'|');
              if(datos!=null)
              {
                 if ((datos[3].equals(tipo_persona))&&
                     (datos[2].substring(0,1).equals("-")) )
				 {
                     cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+datos[5] + "</TD>";//rango_menor
                     cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+datos[6] + "</TD>";//rango_mayor
                     cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+datos[7] + "</TD>";//tasa
                     cadena_resultante+="</TR>";
                  }
              }
           }
        }
        catch(IOException e)
        {
              EIGlobal.mensajePorTrace("***inversion7_28.class: IOException"+e, EIGlobal.NivelLog.ERROR);
        }
        catch(Exception e)
        {
              EIGlobal.mensajePorTrace("***inversion7_28 exception de otro tipo "+e, EIGlobal.NivelLog.ERROR);
        }
		finally
		 {
              try
			  {
				  in.close();
			  }
			  catch(Exception e)
			  {
				   e.printStackTrace();
              }
         }

       //EIGlobal.mensajePorTrace("***inversion7_28 cadena_resultante "+cadena_resultante, EIGlobal.NivelLog.ERROR);


       return cadena_resultante;
   }


   public String Obtener_plazos()
   {
      String cadena_resultante="";

      EIGlobal.mensajePorTrace("***Entrando a Obtener_plazos ", EIGlobal.NivelLog.INFO);
          String  coderror          = "";
      String  cadena_consulta   = "";
      String  buffer            = "";
      String  query_realizar    = "";
      int     contador          = 0;
      Connection conn           = null;
          PreparedStatement ps      = null;
          ResultSet rs              = null;

          try {
                conn = createiASConn( Global.DATASOURCE_ORACLE );
                        query_realizar="Select producto, subproducto, plazo_ini, plazo_fin ";
                        query_realizar=query_realizar + " from tct_plazos ";
                        EIGlobal.mensajePorTrace("***vista.class Query"+query_realizar, EIGlobal.NivelLog.ERROR);
                        ps = conn.prepareStatement( query_realizar );
                        rs = ps.executeQuery();
                        cadena_resultante = "";
                         EIGlobal.mensajePorTrace("***query_realizar "+query_realizar, EIGlobal.NivelLog.INFO);

                        while ( rs.next() ) {
                          cadena_resultante = cadena_resultante + rs.getString(1) +"|";
                          cadena_resultante = cadena_resultante + rs.getString(2) +"|";
                          cadena_resultante = cadena_resultante + rs.getString(3) +"|";
                          cadena_resultante = cadena_resultante + rs.getString(4) +"@";
                          contador++;
                         EIGlobal.mensajePorTrace("***contador "+contador, EIGlobal.NivelLog.INFO);

                        }
                        cadena_resultante = contador +"@"+cadena_resultante;
                        //rs.close();
                } catch ( SQLException sqle ) {
                        sqle.printStackTrace();
                }
				finally
	            {
                  try
			      {
                    rs.close();
					ps.close();
                    conn.close();
                  }
				  catch(Exception e)
			      {
			         e.printStackTrace();
			      }
                }

        return cadena_resultante;
   }


   public String Obtener_instrumentos()
   {
      String cadena_resultante="";
          EIGlobal.mensajePorTrace("***Entrando a Obtener_instrumentos ", EIGlobal.NivelLog.INFO);
          String  coderror          = "";
      String  cadena_consulta   = "";
      String  buffer            = "";
      String  query_realizar    = "";
      int     contador          = 0;
      Connection conn           = null;
          PreparedStatement ps      = null;
          ResultSet rs              = null;

          try {
                conn = createiASConn( Global.DATASOURCE_ORACLE );
                        query_realizar="Select producto, subproducto, descripcion";
                        query_realizar=query_realizar + " from tct_instrumentos_plazo";
                        EIGlobal.mensajePorTrace("***vista.class Query"+query_realizar, EIGlobal.NivelLog.ERROR);
                        ps = conn.prepareStatement( query_realizar );
                        rs = ps.executeQuery();
                        cadena_resultante = "";

                        while ( rs.next() ) {
                          cadena_resultante = cadena_resultante + rs.getString(1) +"|";
                          cadena_resultante = cadena_resultante + rs.getString(2) +"|";
                          cadena_resultante = cadena_resultante + rs.getString(3) +"@";
                          contador++;
                        }
                        cadena_resultante = contador +"@"+cadena_resultante;
                        //rs.close();
        } catch ( SQLException sqle ) {
                sqle.printStackTrace();
        }
		finally
	    {
                  try
			      {
                    rs.close();
					ps.close();
                    conn.close();
                  }
				  catch(Exception e)
			      {
			         e.printStackTrace();
			      }
         }
         return cadena_resultante;
   }


    public double obten_tasa(int plazo,double importe,String producto,String subproducto,HttpServletRequest request)
   {
      EIGlobal.mensajePorTrace("***vista obten_tasa ", EIGlobal.NivelLog.ERROR);
	  HttpSession sess = request.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");


	  double tasaplicar=0.0;
	  double rangomenor=0.0;
	  double rangomayor=0.0;

	  if(subproducto.length()==4)
		subproducto=subproducto.substring(2, 4);

      String tipo_persona="";

	  if (subproducto.equals("43")||subproducto.equals("62")||subproducto.equals("64")||subproducto.equals("69"))
		tipo_persona="F";
	  else if (subproducto.equals("42")||subproducto.equals("48"))
			tipo_persona="J";
      else
			tipo_persona="T";

	  EIGlobal EnlaceGlobal = new EIGlobal(session,getServletContext(),this);

       BufferedReader in=null;
	  try
      {

           EnlaceGlobal.llamada_consulta_tasas_plazos_por_servicio(subproducto+"|"+producto+"|");

		   String linea="";
           String datos[]=null;
           in=new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR+ "/tasas.dat" ));

           while ((linea=in.readLine())!=null)
           {
              //sprintf( linea,"%s|%s|%s|%f|%f|%f|",fecha_aux,element->instrumento,element->persona_juridica,element->plazo,element->rango_menor,element->rango_mayor,element->tasa);
              datos = desentramaC("7|"+linea,'|');
              if(datos!=null)
              {
                 if ((datos[3].equals(tipo_persona))&&
                     (datos[2].substring(0,1).equals("-")) )
				 {
                     rangomenor=new Double(datos[5]).doubleValue();
					 rangomayor=new Double(datos[6]).doubleValue();
                     if ( (plazo==Integer.parseInt(datos[4]))&&
						  (importe>=rangomenor&&importe<=rangomayor) )

				       tasaplicar=new Double(datos[7]).doubleValue();
                  }
              }
           }
      }
      catch(IOException e)
      {
              EIGlobal.mensajePorTrace("***vista.class: IOException"+e, EIGlobal.NivelLog.ERROR);
      }
      catch(Exception e)
      {
              EIGlobal.mensajePorTrace("***vista de otro tipo "+e, EIGlobal.NivelLog.ERROR);
      }
	  finally
		 {
              try
			  {
				  in.close();
			  }
			  catch(Exception e)
			  {
				   e.printStackTrace();
              }
         }

       EIGlobal.mensajePorTrace("***vista "+tasaplicar, EIGlobal.NivelLog.ERROR);
       return tasaplicar;
   }



  private String Obtener_Contratos_Inversion(String cuenta)
   {
       String contratos_inversion=null;
       String aux2_contratos_inversion="";
       int contador_contratos=0;
	   int contador_contratos_considerar=0;
	   String Cuentas_operar[]=null;
	   int siguiente=0;
       String cadena_retorno_cuentas=llamada_cuentas_para_inversion7_28(cuenta);
       EIGlobal.mensajePorTrace("***cuentasSerfinSantander  "+cadena_retorno_cuentas, EIGlobal.NivelLog.ERROR);

       if (cadena_retorno_cuentas.substring(0,8).equals("POSI0000"))
       {
               try
               {
                 cadena_retorno_cuentas=cadena_retorno_cuentas.substring(9,cadena_retorno_cuentas.length());
                 //System.out.println("cadena_retorno "+cadena_retorno_cuentas);
				 contador_contratos=Integer.parseInt(cadena_retorno_cuentas.substring(0,cadena_retorno_cuentas.indexOf("|")));
                 //System.out.println("contador "+contador_contratos);

				 if (contador_contratos>0)
                 {
                    cadena_retorno_cuentas=cadena_retorno_cuentas.substring(cadena_retorno_cuentas.indexOf("|")+1,cadena_retorno_cuentas.length());
                    //System.out.println("cadena_retorno "+cadena_retorno_cuentas);
 					cadena_retorno_cuentas=String.valueOf(contador_contratos*6)+"|"+cadena_retorno_cuentas;
                    //System.out.println("cadena_retorno "+cadena_retorno_cuentas);
					Cuentas_operar = desentramaC(cadena_retorno_cuentas+"|",'|');
                    siguiente=1;
 				    aux2_contratos_inversion="";
                    for(int i=1;i<=contador_contratos;i++)
                    {

                      aux2_contratos_inversion+=Cuentas_operar[siguiente]+"|"+Cuentas_operar[siguiente+1]+"|"+Cuentas_operar[siguiente+2]+"@";
                      //System.out.println("aux2_contratos_inversion "+aux2_contratos_inversion);
					  contador_contratos_considerar++;
                      siguiente=siguiente+6;
                    }

                    //System.out.println("contador_contratos_considerar "+contador_contratos_considerar);

                    if (contador_contratos_considerar>0)
					{
				       contratos_inversion="";
					   contratos_inversion+=contador_contratos_considerar+"@";
					   contratos_inversion+=aux2_contratos_inversion;
                      //System.out.println("contratos_considerar "+contratos_inversion);

					}

				 }
              }
			  catch(Exception e)
              {
                  EIGlobal.mensajePorTrace("***inversion7_28o.class "+e, EIGlobal.NivelLog.ERROR);
              }
		}
        //EIGlobal.mensajePorTrace("***Contratos_inversion "+contratos_inversion, EIGlobal.NivelLog.INFO);
		return contratos_inversion;

   }

   private String llamada_cuentas_para_inversion7_28( String cuenta ) {
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander Entrando a " +
				"llamada_cuentas_para_inversion7_28 cuenta", EIGlobal.NivelLog.DEBUG );


		String coderror;
		String tramaRegresoServicio = "";
		//TuxedoGlobal tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo() ;
	   ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
		//*****************************
       int reqStatus = 0;
       int nSize = 0;
   		//*****************************
	    Hashtable htResult = null;
		String POSI_OK_CODE="POSI0000";
		String trama_enviar = "";
		String buffer = "";
		trama_enviar = cuenta + "|C|";



		try {
		/*************************************
			try{
					Hashtable hs = tuxGlobal.web_red(trama_enviar);
					tramaRegresoServicio = (String) hs.get("BUFFER");
					System.out.println("trama regreso servicio: "+tramaRegresoServicio);

					//reqStatus = Integer.parseInt((String) hs.get("BUFFER"));
					}catch( java.rmi.RemoteException re ){
						re.printStackTrace();
				} catch (Exception e) {}

		//**************************************/
	    	htResult = tuxGlobal.basicp( trama_enviar );

	    } catch ( java.rmi.RemoteException re ) {
	    	re.printStackTrace();
	    } catch (Exception e) {}

		coderror = ( String ) htResult.get( "COD_ERROR" );
		//System.out.println( coderror );
		EIGlobal.mensajePorTrace("***inversion7_28 BASICP<<" + coderror, EIGlobal.NivelLog.DEBUG );
		if ( coderror == null ) {
			buffer = "Problemas con BASICP";
			EIGlobal.mensajePorTrace("***inversion7_28 buffer:"+buffer, EIGlobal.NivelLog.INFO);

		} else if ( coderror.equals( POSI_OK_CODE ) ) {
				buffer = ( String ) htResult.get( "BUFFER" );
				buffer = coderror + "|" + buffer;
		} else {
				buffer = coderror;
		}

		EIGlobal.mensajePorTrace("***inversion7_28 buffer:<<"+buffer, EIGlobal.NivelLog.INFO);
		//buffer="POSI0000|2|44000814144|03|0044|65500362415|65500362415|0|67500362415|03|0067|65500362415|65500362415|0";

		return buffer;
	}

	public String obten_selec_contratos(String contratos)
    {
	    EIGlobal.mensajePorTrace("***inversion7_28 obten_selec_contratos", EIGlobal.NivelLog.INFO);

		String trama="";
		String datos[]=null;
		try
        {

		   int contador=Integer.parseInt(contratos.substring(0,contratos.indexOf("@")));
		   contratos=contratos.substring(contratos.indexOf("@")+1,contratos.length());
		   for(int i=1;i<=contador;i++)
		   {
  			   datos=desentramaC("3|"+contratos.substring(0,contratos.indexOf("@"))+"|",'|');
			   trama+="<option value="+datos[1]+"|"+datos[2]+"|"+datos[3]+">"+datos[1]+"</option>";
               if(i<contador)
			     contratos=contratos.substring(contratos.indexOf("@")+1,contratos.length());
           }

        }
		catch(Exception e)
		{
		  trama="";
        }
        EIGlobal.mensajePorTrace("***inversion7_28 trama:"+trama, EIGlobal.NivelLog.INFO);

		return trama;

    }


    public String[] obten_datos_primer_contrato(String contratos)
    {
	    EIGlobal.mensajePorTrace("***inversion7_28 obten_datos_primer_contrato ", EIGlobal.NivelLog.INFO);
	    EIGlobal.mensajePorTrace("***inversion7_28 contratos "+contratos, EIGlobal.NivelLog.INFO);

		String datos[]=new String[2];
		try
        {
		   //System.out.println("contratos "+contratos);
		   contratos=contratos.substring(contratos.indexOf("@")+1,contratos.length());//quita contador
  		   //System.out.println("contratos "+contratos);
		   contratos=contratos.substring(0,contratos.indexOf("@"));
   		   //System.out.println("contratos "+contratos);
		   String cta= contratos.substring(0,contratos.indexOf("|"));
   		   //System.out.println("cta "+cta);
		   contratos=contratos.substring(contratos.indexOf("|")+1,contratos.length());
   		   //System.out.println("contratos "+contratos);
		   String producto=contratos.substring(0,contratos.indexOf("|"));
   		   //System.out.println("producto "+producto);
		   String subproducto=contratos.substring(contratos.indexOf("|")+1,contratos.length());
   		   //System.out.println("subproducto "+subproducto);
		   datos[0]=producto;
		   datos[1]=subproducto;
        }
		catch(Exception e)
        {
           datos[0]="";
		   datos[1]="";
		}
		return datos;
    }

    public String obten_selec_inst_pri_contrato(String instrumentos,String producto,String subproducto,String divisa)
    {
	    EIGlobal.mensajePorTrace("***inversion7_28 obten_selec_inst_pri_contrato", EIGlobal.NivelLog.INFO);
	    EIGlobal.mensajePorTrace("***inversion7_28 producto:"+producto, EIGlobal.NivelLog.INFO);
	    EIGlobal.mensajePorTrace("***inversion7_28 subproducto:"+subproducto, EIGlobal.NivelLog.INFO);
	    EIGlobal.mensajePorTrace("***inversion7_28 divisa:"+divisa, EIGlobal.NivelLog.INFO);


		String trama="";
		String datos[]=null;
		try
        {

		   int contador=Integer.parseInt(instrumentos.substring(0,instrumentos.indexOf("@")));
		   //System.out.println("contador "+contador);
		   instrumentos=instrumentos.substring(instrumentos.indexOf("@")+1,instrumentos.length());
		   //System.out.println("instrumentos "+instrumentos);
		   for(int i=1;i<=contador;i++)
		   {
  			   datos=desentramaC("3|"+instrumentos.substring(0,instrumentos.indexOf("@"))+"|",'|');
			   //System.out.println("datos[1] "+datos[1]);
			   //System.out.println("datos[2] "+datos[2]);
			   //System.out.println("datos[3] "+datos[3]);

       	       if(producto.equals(datos[1]))
               {
			      if(divisa.equals("6"))
				  {
				   if(datos[2].equals("0042")) //d�lares
				   {
					 if(subproducto.equals(datos[2]))
			          trama+="<option value="+datos[2]+" SELECTED>"+datos[3]+"</option>";
				     else
   			          trama+="<option value="+datos[2]+">"+datos[3]+"</option>";
                    }

				  }
				  else
				  {
					if(!datos[2].equals("0042")) //d�lares
					{
					 if(subproducto.equals(datos[2]))
			          trama+="<option value="+datos[2]+" SELECTED>"+datos[3]+"</option>";
				     else
   			          trama+="<option value="+datos[2]+">"+datos[3]+"</option>";
                    }
				  }

			   }
			   if(i<contador)
			     instrumentos=instrumentos.substring(instrumentos.indexOf("@")+1,instrumentos.length());
           }

        }
		catch(Exception e)
		{
		  trama="";
        }
        //EIGlobal.mensajePorTrace("***inversion7_28 trama:"+trama, EIGlobal.NivelLog.INFO);

		return trama;


    }


	String Obtener_tipo_inversion(String producto,String instrumento)
    {
               String cadena_resultante="";
          EIGlobal.mensajePorTrace("***Entrando a Obtener_instrumentos ", EIGlobal.NivelLog.INFO);
          String  coderror          = "";
      String  cadena_consulta   = "";
      String  buffer            = "";
      String  query_realizar    = "";
      int     contador          = 0;
      Connection conn           = null;
          PreparedStatement ps      = null;
          ResultSet rs              = null;

          try {
                conn = createiASConn( Global.DATASOURCE_ORACLE );
                        query_realizar="Select descripcion";
                        query_realizar=query_realizar + " from tct_instrumentos_plazo";
                        query_realizar=query_realizar + " where producto='"+producto+"'  and   subproducto='"+instrumento+"'";
                        EIGlobal.mensajePorTrace("***vista.class Query"+query_realizar, EIGlobal.NivelLog.ERROR);
                        ps = conn.prepareStatement( query_realizar );
                        rs = ps.executeQuery();
                        cadena_resultante = "";

                        while ( rs.next() ) {
                          cadena_resultante = cadena_resultante + rs.getString(1) ;

                        }
                        //rs.close();
        } catch ( SQLException sqle ) {
                sqle.printStackTrace();
        }
		finally
	    {
                  try
			      {
                    rs.close();
					ps.close();
                    conn.close();
                  }
				  catch(Exception e)
			      {
			         e.printStackTrace();
			      }
         }
         return cadena_resultante;



    }



/****************************************************/
/*													*/
/*													*/
/*	HACE CONSULTA PARA OBTENER LAS TASAS DE PLAZO	*/
/*													*/
/*													*/
/****************************************************/
 public String tabla_vista_BD07(String subproducto, String producto,HttpServletRequest request) throws ServletException, IOException
{
		EIGlobal.mensajePorTrace("*****     INVERSION 7-28 tabla_vista_BD07      *****", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***INVERSION 7_28 tabla_vista_BD07 subproducto: "+subproducto, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***INVERSION 7_28 tabla_vista_BD07 producto: "+producto, EIGlobal.NivelLog.DEBUG);

		String cadena_resultante="";
		String tipo_persona="";

		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


		EIGlobal EnlaceGlobal = new EIGlobal(session,getServletContext(),this);
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) BAL.getServletContext()).getContext());

		Hashtable htResult = null;
		String CODE_ERROR_OK="TUBO0000";
		String coderror = "";
		String FMLString = "";
		String campo_41204 = "";
		String campo_41205 = "";
		String campo_41206 = "";
		String campo_41265 = "";
		String FMLString_aux = "";
		int continua = 0;
		int cont = 0;

		java.text.NumberFormat formato_num = java.text.NumberFormat.getInstance();
		formato_num.setMinimumFractionDigits(5);
		formato_num.setMaximumFractionDigits(5);




		if (subproducto.trim().equals("61")||subproducto.trim().equals("0061")) //este es ek subproducto para cuentas tipo 60
			tipo_persona="F";
		else
			tipo_persona="J";

		cadena_resultante+="<TR>";
		cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Menor</TD>";//rango_menor
    	cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Mayor</TD>";//rango_mayor
    	cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Tasa</TD>";//tasa
    	cadena_resultante+="</TR>";

		if(subproducto.trim().length()==2)
		    if(!subproducto.substring(0,1).trim().equals("00"))
			    subproducto = "00" + subproducto.trim();


		/********************************************************************************/
		/*							PRIMER LLAMADO										*/
		/********************************************************************************/

		 try
		{

			htResult = tuxGlobal.TUBOCT( producto,subproducto, "BD07A", "", "");
			coderror = ( String ) htResult.get("COD_ERROR");
            EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

	        if(coderror == null)
	            coderror = "";

    		if(coderror.trim().equals(CODE_ERROR_OK))
			{
			    EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
		        EIGlobal.mensajePorTrace("***********   RESULTADO DEL 1ER LLAMADO AL TUBOCT   ************************", EIGlobal.NivelLog.ERROR);
		        EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
                FMLString = ( String ) htResult.get("FMLSTRING");
                campo_41204 = ( String ) htResult.get("TIPO_SOC");
		    	EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 TIPO_SOC: " + campo_41204 , EIGlobal.NivelLog.ERROR);
		    	campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
		    	EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 PAIS_ORIGEN: " + campo_41205 , EIGlobal.NivelLog.ERROR);
			    campo_41206 = ( String ) htResult.get("SECTOR");
			    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 SECTOR: " + campo_41206 , EIGlobal.NivelLog.ERROR);
                campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
			    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 CAMPO_AUX2: " + campo_41265 , EIGlobal.NivelLog.ERROR);
			}
	        else
	        {
                EIGlobal.mensajePorTrace("***INVERSION 7_28 tabla_vista_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
			    return "-1";
	        }
		}
		 catch (Exception e)
		{
			EIGlobal.mensajePorTrace("***INVERSION 7_28 tabla_vista_BD07 Error en el primer llamado al TUBOCT: " + e.toString(), EIGlobal.NivelLog.DEBUG);
		}



		/********************************************************************************/
		/*							SIGUIENTES LLAMADOS 								*/
		/********************************************************************************/
		if( (campo_41204.trim().equals("") && campo_41206.trim().equals("MXP"))
        || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP")) )
            continua = 1;
        else
            continua = 0;

		 while(continua==1 && cont<4)
		{
		    FMLString_aux = FMLString_aux + FMLString.substring(0,FMLString.length() - 21);

            coderror = "";

 	 		 try
			{
				htResult = tuxGlobal.TUBOCT(producto,subproducto,"BD07A",campo_41206,campo_41265);

				FMLString = "";
		        campo_41204 = "";
		        campo_41205 = "";
		        campo_41206 = "";
		        campo_41265 = "";

				coderror = ( String ) htResult.get("COD_ERROR");
				EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

                if(coderror == null)
                    coderror = "";

                if(coderror.trim().equals(CODE_ERROR_OK))
                {
			    	EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
		            EIGlobal.mensajePorTrace("***********   RESULTADO DEL 2DO. LLAMADO AL TUBOCT   ************************", EIGlobal.NivelLog.ERROR);
		            EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
                    FMLString = ( String ) htResult.get("FMLSTRING");
                    campo_41204 = ( String ) htResult.get("TIPO_SOC");
				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 TIPO_SOC: " + campo_41204 , EIGlobal.NivelLog.ERROR);
				    campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 PAIS_ORIGEN: " + campo_41205 , EIGlobal.NivelLog.ERROR);
				    campo_41206 = ( String ) htResult.get("SECTOR");
				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 SECTOR: " + campo_41206 , EIGlobal.NivelLog.ERROR);
			    	campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
			    	EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 CAMPO_AUX2: " + campo_41265 , EIGlobal.NivelLog.ERROR);
				}
				else
				{
	    	        EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
					return "-1";
				}
			}
			 catch (Exception e)
			{
				EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 Error en el 2do llamado al TUBOCT: " + e.toString(), EIGlobal.NivelLog.DEBUG);
			}

			if(	(campo_41204.trim().equals("") && campo_41206.trim().equals("MXP")) || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP"))	)
				continua = 1;
			else
				continua = 0;
		    cont = cont + 1;
		}

		FMLString_aux = FMLString_aux + FMLString;
		FMLString = FMLString_aux;

		if(FMLString == null)
		    FMLString = "";


    if(!FMLString.trim().equals(""))
    {
		//Variables para contar los registros
		int noproductos = 0;
		int nocampos = 0;
		int records_serfin = 11;
		int noregistros = 0;

		 for(int i=0; i < FMLString.length();)
		{
			if (FMLString.substring(i,i+1).trim().equals("|"))
				nocampos++;                            //CONTABILIZA LOS PIPES EN LA RESPUESTA
			if (FMLString.substring(i,i+1).trim().equals("@"))
				noproductos++;                         //CONTABILIZA LAS ARROBAS (PRODUCTOS) EN LA RESPUESTA
            i++;
		}

		noregistros = nocampos / records_serfin;    	//OBTIENE EL NUMERO DE REGISTROS EFECTIVOS

		//Variables para separar los datos
		int arrobas = 0;
		int campos = 0, j=0;
		String dato = "";
		int posicionador = 0;

		//Variables para guardar los datos
		String sub_prod = "";
		String instrumento = "";
		String personalidad = "";
		String plazo = "";
		String plazo_nuevo = "";
		String plazo_ant = "";
		double imp_min = 0;
		double imp_max = 0;
		double tasa_min = 0;
		double tasa_min_aux = 0;
		double tasa_max = 0;
		double tasa = 0;


		 for(int i=0; i < noregistros;i++)
		{
			sub_prod = "";
			instrumento = "";
			personalidad = "";
			plazo = "";
			plazo_nuevo = "";
		   	plazo_ant = "";
			imp_min  = 0;
			imp_max = 0;
			tasa_min = 0;
		   	tasa_min_aux = 0;
		   	tasa_max = 0;
		   	tasa = 0;
		   	campos = 0;

		   	 for (j = posicionador;j < FMLString.length();)         //BARRE CARACTER X CARACTER DE LA RESPUESTA
		   	{
		   	 	 if (FMLString.substring(j,j+1).trim().equals("|")||FMLString.substring(j,j+1).trim().equals("@"))
		   		{                       // PREGUNTA SI EL CARACTER ES UN PIPE PARA CONTABILIZAR UN CAMPO
		   			campos++;

          		     switch(campos)
		   			{
		   				case 1:
		   				        //EIGlobal.mensajePorTrace("***************************************************", EIGlobal.NivelLog.ERROR);
                                sub_prod = dato;
                                //EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 SUB_PRODUCTO: " + sub_prod, EIGlobal.NivelLog.ERROR);
          						break;
		   				case 4:
		   						imp_min = new Double(dato).doubleValue();
		   						//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 IMP_MIN: " + imp_min , EIGlobal.NivelLog.ERROR);
		   						break;
		   				case 5:
		   						imp_max = new Double(dato).doubleValue();
		   						//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 IMP_MAX: " + imp_max , EIGlobal.NivelLog.ERROR);
          						break;
		   				case 6:
		   						tasa_min = new Double(dato).doubleValue();
		   						tasa_min_aux = new Double(dato).doubleValue();
		   						//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 TASA_MIN: " + tasa_min , EIGlobal.NivelLog.ERROR);
          						break;
		   				case 7:
		   						tasa_max = new Double(dato).doubleValue();
		   						//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 TASA_MAX: " + tasa_max , EIGlobal.NivelLog.ERROR);
          						break;
		   				case 8:
		   				        personalidad = dato;
								//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 PERSONALIDAD: " + personalidad , EIGlobal.NivelLog.ERROR);
          						break;
		   				case 9:
		   				       	plazo = dato;
								//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 PLAZO: " + plazo , EIGlobal.NivelLog.ERROR);
          						break;
					}
					dato = "";                                     // CORTA LA CADENA PARA Q UNICAMENTE CONTENGA EL DATO

					if (FMLString.substring(j,j+1).trim().equals("@"))
					{
		   				arrobas++;
		   				posicionador = j+2;                                 //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
		   				break;
					}
				}
				 else
                    dato = dato + FMLString.substring(j,j+1);                       //ASIGNA UN CARACTER A LA VARIABLE, MIENTRAS NO SEA UN PIPE

				 if(campos == records_serfin)
				{
					posicionador = j+1;                                 //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
					break;
		   		}
		   		j++;

			} //Fin FOR (para obtener campos * registro)

		   	if (tasa_min_aux < 0.0001)
		   		tasa_min_aux = 0.0;

		   		//Calcula el valor del instrumento en base al numero del subproducto

		   		 switch(Integer.parseInt(sub_prod))
		   		{
		   			case 21:
		   					instrumento = "6";
		   					break;
		   			case 43:
		   					instrumento = "5";
		   					break;
		   			case 47:
		   					instrumento = "5";
		   					break;
		   			case 44:
		   					instrumento = "14";
		   					break;
		   			case 42:
		   					instrumento = "16";
							break;
					case 15:
							instrumento = "15";
		   					break;
		   			case 52:
		   					instrumento = "7";
		   					break;
		   			case 53:
		   					instrumento = "8";
		   					break;
		   			case 54:
		   					instrumento = "8";
		   					break;
		   			case 55:
		   					instrumento = "9";
		   					break;
		   			case 57:
		   					instrumento = "6";
		   					break;
		   			case 40:
		   					instrumento = "17";
		   					break;
		   			case 60:
		   					instrumento = "1";
		   					break;
		   			case 61:
		   					instrumento = "2";
		   					plazo = "V";
		   					break;
					case 65:
							instrumento = "1";
		   					break;
		   			case 66:
		   					instrumento = "2";
		   					plazo = "V";
		   					break;
		   			case 69:
		   					instrumento = "13";
		   					break;
		   			case 70:
		   					instrumento = "3";
		   					break;
		   			case 79:
		   					instrumento = "11";
		   					break;
					case 62:
		   					instrumento = "4";
		   					break;
		   			case 67:
		   					instrumento = "4";
		   					break;
		   			case 82:
		   					instrumento = "18";
		   					break;
		   			case 83:
		   					instrumento = "19";
		   					break;
		   			case 64:
		   					instrumento = "12";
		   					break;
				}


				// Valida si el subproducto es diferente de 44
				// Tasa maxima es CERO
				 if (!sub_prod.trim().equals("44"))
				{
		   			tasa_min = tasa_min_aux;
		   			tasa_max = 0;
		   		}

		        // Valida si el plazo es mayor a 366 dias,
		        // Dividir la tasa /2
	            if (!plazo.trim().equals("V"))
	            if (Integer.parseInt(plazo) >= 366)
	            	tasa_min = tasa_min/2;

				if (!sub_prod.trim().equals("44"))
					tasa_max = tasa_min_aux;  // todos los productos excepto '34'


		        //Copia de funci�n CrearArchivo
		   		 if ( imp_max > 0.0 && imp_min >= 0.0 )
		        {
		        	// pegar lineas de tasa neta y tasa bruta a archivo "tasas.dat"
		        	if(  Integer.parseInt(sub_prod) == 44 || Integer.parseInt(sub_prod) == 69 ||
		                 Integer.parseInt(sub_prod) == 70 || Integer.parseInt(sub_prod) == 62 ||
		                 Integer.parseInt(sub_prod) == 67 ) {

						plazo_nuevo = plazo;
						plazo_ant = plazo;

						if (!plazo.trim().equals("V"))
						 switch(Integer.parseInt(plazo_nuevo))
						{
		                	case 1:
		                			plazo_nuevo = "4";
		                			break;
		                    case 5:
		                    		plazo_nuevo = "13";
		                           	break;
		                    case 14:
									plazo_nuevo = "23";
									break;
							case 24:
									plazo_nuevo = "39";
		                    		break;
		                    case 40:
		                    		plazo_nuevo = "75";
		                    		break;
		                    case 76:
		                    		plazo_nuevo = "134";
		                    		break;
		                    case 135:
		                    		plazo_nuevo = "249";
		                    		break;
		                    case 250:
		                    		plazo_nuevo = "365";
		                    		break;
		                    case 366:
		                    		plazo_nuevo = "371";
		                    		break;
						};

						//Si es tasa neta
			            plazo = plazo_nuevo;
		                tasa = tasa_min;

		   				//Si es tasa bruta
		   				instrumento = "-" + instrumento;
		   				plazo = plazo_ant;
		   				tasa = tasa_max;
					}
					 else
					{
						//Si es tasa neta
						tasa = tasa_min;

						//Si es tasa bruta
		   				instrumento = "-" + instrumento;
		   				tasa = tasa_max;
					}


 					 if ((personalidad.trim().equals(tipo_persona)) && (instrumento.trim().substring(0,1).trim().equals("-")) )
					{
					    try
					    {
					        cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+ formato_num.format(imp_min) + "</TD>";//rango_menor
						    cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+ formato_num.format(imp_max) + "</TD>";//rango_mayor
				            cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+ formato_num.format(tasa) + "</TD>";//tasa
				            cadena_resultante+="</TR>";
				        }
				         catch (Exception e)
				        {
					        EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_vista_BD07 Error al dar formato a los datos: " + e.toString(), EIGlobal.NivelLog.DEBUG);
				        }
					}
				}
            }//Fin FOR (para conteo registros)
        }
		return cadena_resultante;
}



/****************************************************/
/*													*/
/*													*/
/*	HACE CONSULTA PARA OBTENER LAS TASAS DE PLAZO	*/
/*													*/
/*													*/
/****************************************************/
 public String tabla_inversiones_BD07(String subproducto, String producto,HttpServletRequest request)throws ServletException, IOException
{
		EIGlobal.mensajePorTrace("*****     INVERSION 7_28 tabla_inversiones_BD07      *****", EIGlobal.NivelLog.DEBUG);
	    EIGlobal.mensajePorTrace("***INVERSION 7_28 tabla_inversiones_BD07 subproducto: " + subproducto, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***INVERSION 7_28 tabla_inversiones_BD07 producto: " + producto, EIGlobal.NivelLog.DEBUG);

		String cadena_resultante="";
	    String tipo_persona="";

		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


	    EIGlobal EnlaceGlobal = new EIGlobal(session,getServletContext(),this);
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) BAL.getServletContext()).getContext());

		Hashtable htResult = null;
		String CODE_ERROR_OK="TUBO0000";
		String coderror = "";
		String FMLString = "";
		String campo_41204 = "";
		String campo_41205 = "";
		String campo_41206 = "";
		String campo_41265 = "";
		String FMLString_aux = "";
		int continua = 0;
		int cont = 0;

		java.text.NumberFormat formato_num = java.text.NumberFormat.getInstance();
		formato_num.setMinimumFractionDigits(5);
		formato_num.setMaximumFractionDigits(5);

		if (subproducto.trim().equals("43")||subproducto.trim().equals("62")||subproducto.trim().equals("64")||subproducto.trim().equals("69")
		||subproducto.trim().equals("0043")||subproducto.trim().equals("0062")||subproducto.trim().equals("0064")||subproducto.trim().equals("0069"))
			tipo_persona="F";
		else
			if (subproducto.trim().equals("42")||subproducto.trim().equals("48")
		    ||subproducto.trim().equals("0042")||subproducto.trim().equals("0048"))
				tipo_persona="J";
	    	else
				tipo_persona="T";

		cadena_resultante+="<TR>";
		cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Plazo</TD>";//Plazo
		cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Menor</TD>";//rango_menor
		cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Rango Mayor</TD>";//rango_mayor
		cadena_resultante+="<TD ALIGN=RIGHT class=tittabdat>Tasa</TD>";//tasa
		cadena_resultante+="</TR>";

		if(subproducto.trim().length()==2)
		    if(!subproducto.substring(0,1).trim().equals("00"))
			    subproducto = "00" + subproducto.trim();


		/********************************************************************************/
		/*							PRIMER LLAMADO										*/
		/********************************************************************************/

			 try
			{

				htResult = tuxGlobal.TUBOCT( producto,subproducto, "BD07A", "", "");
				coderror = ( String ) htResult.get("COD_ERROR");
				EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

				if(coderror==null)
				    coderror = "";

                if(coderror.trim().equals(CODE_ERROR_OK))
				{
                    EIGlobal.mensajePorTrace("****************************************************************", EIGlobal.NivelLog.ERROR);
		            EIGlobal.mensajePorTrace("***********   RESULTADO DEL 1ER. LLAMADO AL TUBOCT   ***********", EIGlobal.NivelLog.ERROR);
		            EIGlobal.mensajePorTrace("****************************************************************", EIGlobal.NivelLog.ERROR);
                    FMLString = ( String ) htResult.get("FMLSTRING");
                    campo_41204 = ( String ) htResult.get("TIPO_SOC");
				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 TIPO_SOC: " + campo_41204 , EIGlobal.NivelLog.ERROR);
				    campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 PAIS_ORIGEN: " + campo_41205 , EIGlobal.NivelLog.ERROR);
				    campo_41206 = ( String ) htResult.get("SECTOR");
				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 SECTOR: " + campo_41206 , EIGlobal.NivelLog.ERROR);
        		    campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 CAMPO_AUX2: " + campo_41265 , EIGlobal.NivelLog.ERROR);
            	}
		        else
				{
                    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
					return "-1";
				}
			}
			 catch (Exception e)
			{
				EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 Error en el 1er llamado al TUBOCT: " + e.toString(), EIGlobal.NivelLog.DEBUG);
			}


		/********************************************************************************/
		/*							SIGUIENTES LLAMADOS 								*/
		/********************************************************************************/
			if(	(campo_41204.trim().equals("") && campo_41206.trim().equals("MXP")) || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP"))	)
				continua = 1;
			else
			    continua = 0;

		  	 while(continua==1 && cont<4)
			{
				FMLString_aux = FMLString_aux + FMLString.substring(0,FMLString.length() - 21);

                coderror = "";

	 	 		 try
				{
					htResult = tuxGlobal.TUBOCT( producto,subproducto, "BD07A", campo_41206, campo_41265);

				    FMLString = "";
		            campo_41204 = "";
		            campo_41205 = "";
		            campo_41206 = "";
		            campo_41265 = "";

					coderror = ( String ) htResult.get("COD_ERROR");
					EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

					if(coderror == null)
					    coderror = "";

				 	if(coderror.trim().equals(CODE_ERROR_OK))
					{
					    EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
		                EIGlobal.mensajePorTrace("***********   RESULTADO DEL 2DO. LLAMADO AL TUBOCT   ************************", EIGlobal.NivelLog.ERROR);
		                EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
                        FMLString = ( String ) htResult.get("FMLSTRING");
                        campo_41204 = ( String ) htResult.get("TIPO_SOC");
					    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 TIPO_SOC: " + campo_41204 , EIGlobal.NivelLog.ERROR);
					    campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
					    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 PAIS_ORIGEN: " + campo_41205 , EIGlobal.NivelLog.ERROR);
					    campo_41206 = ( String ) htResult.get("SECTOR");
					    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 SECTOR: " + campo_41206 , EIGlobal.NivelLog.ERROR);
					    campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
					    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 CAMPO_AUX2: " + campo_41265 , EIGlobal.NivelLog.ERROR);
					}
    				else
    				{
    				    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
					    return "-1";
    				}

				}
				 catch (Exception e)
				{
					EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 Error en el 2do llamado al TUBOCT: "+e.toString(), EIGlobal.NivelLog.DEBUG);
				}

				if(	(campo_41204.trim().equals("") && campo_41206.trim().equals("MXP")) || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP"))	)
					continua = 1;
				else
					continua = 0;
			    cont = cont + 1;
			}


		FMLString_aux = FMLString_aux + FMLString;
		FMLString = FMLString_aux;

		if(FMLString==null)
		    FMLString = "";

    if(!FMLString.trim().equals(""))
    {
		//Variables para contar los registros
		int noproductos = 0;
		int nocampos = 0;
		int records_serfin = 11;
		int noregistros = 0;

		 for(int i=0; i < FMLString.length();)
		{
			if (FMLString.substring(i,i+1).trim().equals("|"))
				nocampos++;                            //CONTABILIZA LOS PIPES EN LA RESPUESTA
			if (FMLString.substring(i,i+1).trim().equals("@"))
				noproductos++;                         //CONTABILIZA LAS ARROBAS (PRODUCTOS) EN LA RESPUESTA
			i++;
		}

		noregistros = nocampos / records_serfin;    	//OBTIENE EL NUMERO DE REGISTROS EFECTIVOS


		//Variables para separar los datos
		int arrobas = 0;
		int campos = 0, j=0;
		String dato = "";
		int posicionador = 0;

		//Variables para guardar los datos
		String sub_prod = "";
		String instrumento = "";
		String personalidad = "";
		String plazo = "";
		String plazo_nuevo = "";
		String plazo_ant = "";
		double imp_min = 0;
		double imp_max = 0;
		double tasa_min = 0;
		double tasa_min_aux = 0;
		double tasa_max = 0;
		double tasa = 0;

 		 for(int i=0; i < noregistros;i++)
		{
			sub_prod = "";
			instrumento = "";
			personalidad = "";
			plazo = "";
			plazo_nuevo = "";
			plazo_ant = "";
			imp_min  = 0;
			imp_max = 0;
			tasa_min = 0;
			tasa_min_aux = 0;
			tasa_max = 0;
			tasa = 0;

			campos = 0;

			 for (j = posicionador;j < FMLString.length();)         //BARRE CARACTER X CARACTER DE LA RESPUESTA
			{
				if (FMLString.substring(j,j+1).trim().equals("|")||FMLString.substring(j,j+1).trim().equals("@"))
				{                       // PREGUNTA SI EL CARACTER ES UN PIPE PARA CONTABILIZAR UN CAMPO
					campos++;
					 switch(campos)
					{
				   		case 1:
				   				//EIGlobal.mensajePorTrace("****************************************************************", EIGlobal.NivelLog.ERROR);
				   				sub_prod = dato;
				   				//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 SUB_PROD: " + sub_prod , EIGlobal.NivelLog.ERROR);
				   				break;
				   	 	case 4:
				   				imp_min = new Double(dato).doubleValue();
				   				//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 IMP_MIN: " + imp_min, EIGlobal.NivelLog.ERROR);
				   				break;
				   		case 5:
				   				imp_max = new Double(dato).doubleValue();
				   				//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 IMP_MAX: " + imp_max, EIGlobal.NivelLog.ERROR);
				   				break;
				   		case 6:
				   				tasa_min = new Double(dato).doubleValue();
				   				tasa_min_aux = new Double(dato).doubleValue();
				   				//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 TASA_MIN: " + tasa_min , EIGlobal.NivelLog.ERROR);
				   				break;
				   		case 7:
				   				tasa_max = new Double(dato).doubleValue();
				   				//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 TASA_MAX: " + tasa_max , EIGlobal.NivelLog.ERROR);
				   				break;
				   		case 8:
				   				personalidad = dato;
				   				//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 PERSONALIDAD: " + personalidad, EIGlobal.NivelLog.ERROR);
				   				break;
				   		case 9:
				   				plazo = dato;
				   				//EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 PLAZO: " + plazo, EIGlobal.NivelLog.ERROR);
				   				break;
				   	}
                    dato = "";                                     // CORTA LA CADENA PARA Q UNICAMENTE CONTENGA EL DATO

                    if (FMLString.substring(j,j+1).trim().equals("@"))
					{
				   		arrobas++;
				   		posicionador = j+2;                                 //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
				   		break;
				   	}
				}
				else
                    dato = dato + FMLString.substring(j,j+1);                       //ASIGNA UN CARACTER A LA VARIABLE, MIENTRAS NO SEA UN PIPE

				 if(campos == records_serfin)
				{
				    posicionador = j+1;                                 //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
				   	break;
				}
                j++;
			} //Fin FOR (para obtener campos * registro)


		    if (tasa_min_aux < 0.0001)
				tasa_min_aux = 0.0;


			//Calcula el valor del instrumento en base al numero del subproducto
			 switch(Integer.parseInt(sub_prod))
			{
				case 21:
						instrumento = "6";
						break;
				case 43:
						instrumento = "5";
						break;
				case 47:
						instrumento = "5";
						break;
				case 44:
						instrumento = "14";
						break;
				case 42:
						instrumento = "16";
						break;
				case 15:
						instrumento = "15";
						break;
				case 52:
						instrumento = "7";
						break;
				case 53:
						instrumento = "8";
						break;
				case 54:
						instrumento = "8";
						break;
				case 55:
						instrumento = "9";
						break;
				case 57:
						instrumento = "6";
						break;
				case 40:
						instrumento = "17";
						break;
				case 60:
						instrumento = "1";
				 		break;
				case 61:
						instrumento = "2";
						plazo = "V";
				 		break;
				case 65:
						instrumento = "1";
						break;
				case 66:
						instrumento = "2";
						plazo = "V";
						break;
				case 69:
						instrumento = "13";
				 		break;
				 case 70:
				 		instrumento = "3";
				 		break;
				 case 79:
				  		instrumento = "11";
				  		break;
				 case 62:
				 		instrumento = "4";
				 		break;
				 case 67:
				 		instrumento = "4";
				 		break;
				 case 82:
				 		instrumento = "18";
				 		break;
				 case 83:
				 		instrumento = "19";
				 		break;
				 case 64:
				 		instrumento = "12";
				 		break;
			}


			// Valida si el subproducto es diferente de 44
			// Tasa maxima es CERO
			 if (!sub_prod.trim().equals("44"))
			{
				tasa_min = tasa_min_aux;
				tasa_max = 0;
			}

			// Valida si el plazo es mayor a 366 dias,
			// Dividir la tasa /2
			if (!plazo.trim().equals("V"))
			    if (Integer.parseInt(plazo) >= 366)
				    tasa_min = tasa_min/2;

			if (!sub_prod.trim().equals("44"))
				tasa_max = tasa_min_aux;  // todos los productos excepto '34'

			//Copia de funci�n CrearArchivo
			 if ( imp_max > 0.0 && imp_min >= 0.0 )
			{
				// pegar lineas de tasa neta y tasa bruta a archivo "tasas.dat"
				if( 	Integer.parseInt(sub_prod) == 44 ||Integer.parseInt(sub_prod) == 69 ||
						Integer.parseInt(sub_prod) == 70 || Integer.parseInt(sub_prod) == 62 ||
						Integer.parseInt(sub_prod) == 67 )
				{
					plazo_nuevo = plazo;
					plazo_ant = plazo;

					if (!plazo.trim().equals("V"))
					 switch(Integer.parseInt(plazo_nuevo))
					{
				    	case 1:
				    			plazo_nuevo = "4";
				        		break;
				        case 5:
				        		plazo_nuevo = "13";
				        		break;
				        case 14:
				        		plazo_nuevo = "23";
				                break;
				        case 24:
				        		plazo_nuevo = "39";
				        		break;
				        case 40:
				        		plazo_nuevo = "75";
				        		break;
				        case 76:
				        		plazo_nuevo = "134";
				        		break;
				        case 135:
				        		plazo_nuevo = "249";
				        		break;
				        case 250:
				        		plazo_nuevo = "365";
				        		break;
				        case 366:
				        		plazo_nuevo = "371";
				        		break;
					};

				    //Si es tasa neta
				    plazo = plazo_nuevo;
					tasa = tasa_min;

				   	//Si es tasa bruta
				   	instrumento = "-" + instrumento;
				   	plazo = plazo_ant;
				   	tasa = tasa_max;
				}
				 else
				{
					//Si es tasa neta
					tasa = tasa_min;

					//Si es tasa bruta
				   	instrumento = "-" + instrumento;
				   	tasa = tasa_max;
				}

				 if ((personalidad.trim().equals(tipo_persona)) && (instrumento.trim().substring(0,1).trim().equals("-")) )
				{
		        	if(plazo.trim().equals("V"))
		        	    cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+ plazo + "</TD>";//rango_menor
		        	else
		        	    cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+ Integer.parseInt(plazo) + "</TD>";//rango_menor
	                try
	                {
					    cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+  formato_num.format(imp_min) + "</TD>";//rango_menor
					    cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+ formato_num.format(imp_max) + "</TD>";//rango_mayor
					    cadena_resultante+="<TD ALIGN=RIGHT class=textabdatcla>"+ formato_num.format(tasa) + "</TD>";//tasa
                	    cadena_resultante+="</TR>";
                	}
                	 catch (Exception e)
				    {
					    EIGlobal.mensajePorTrace("***INVERSION 7-28 tabla_inversiones_BD07 Error al dar formato a los datos: " + e.toString(), EIGlobal.NivelLog.DEBUG);
                    }
				}
		    }
        }//Fin FOR (para conteo registros)
    }
	return cadena_resultante;
}





 public double obten_tasa_BD07(int plazo,double importe,String producto,String subproducto,HttpServletRequest request)throws ServletException, IOException
{
	EIGlobal.mensajePorTrace("*****     INVERSION 7-28 obten_tasa_BD07      *****", EIGlobal.NivelLog.ERROR);
    EIGlobal.mensajePorTrace("*****INVERSION 7-28 obten_tasa_BD07 plazo: " + plazo, EIGlobal.NivelLog.ERROR);
    EIGlobal.mensajePorTrace("*****INVERSION 7-28 obten_tasa_BD07 importe: " + importe, EIGlobal.NivelLog.ERROR);
    EIGlobal.mensajePorTrace("*****INVERSION 7-28 obten_tasa_BD07 producto: " + producto, EIGlobal.NivelLog.ERROR);
    EIGlobal.mensajePorTrace("*****INVERSION 7-28 obten_tasa_BD07 subproducto: " + subproducto, EIGlobal.NivelLog.ERROR);

	double tasaplicar=0.0;
	double rangomenor=0.0;
	double rangomayor=0.0;

	HttpSession sess = request.getSession();
    BaseResource session = (BaseResource) sess.getAttribute("session");


	EIGlobal EnlaceGlobal = new EIGlobal(session,getServletContext(),this);
	ServicioTux tuxGlobal = new ServicioTux();
	//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) BAL.getServletContext()).getContext());
	//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	Hashtable htResult = null;
	String CODE_ERROR_OK="TUBO0000";
	String coderror = "";
	String FMLString = "";
	String campo_41204 = "";
	String campo_41205 = "";
	String campo_41206 = "";
	String campo_41265 = "";
	String FMLString_aux = "";
	int continua = 0;
    int cont = 0;

	if(subproducto.length()==4)
		subproducto=subproducto.substring(2, 3);

    String tipo_persona="";

	if (subproducto.trim().equals("43")||subproducto.trim().equals("62")||subproducto.trim().equals("64")||subproducto.trim().equals("69")
	||subproducto.trim().equals("0043")||subproducto.trim().equals("0062")||subproducto.trim().equals("0064")||subproducto.trim().equals("0069"))
		tipo_persona="F";
	else
		if (subproducto.trim().equals("42")||subproducto.trim().equals("48")
		||subproducto.trim().equals("0042")||subproducto.trim().equals("0048"))
			tipo_persona="J";
      	else
			tipo_persona="T";


	if(subproducto.trim().length()==2)
		    if(!subproducto.substring(0,1).trim().equals("00"))
			    subproducto = "00" + subproducto.trim();

	/********************************************************************************/
	/*							PRIMER LLAMADO										*/
	/********************************************************************************/

	 try
	{
		htResult = tuxGlobal.TUBOCT( producto,subproducto, "BD07A", "", "");
		coderror = ( String ) htResult.get("COD_ERROR");
        EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

        if(coderror == null)
            coderror = "";

	 	if(coderror.trim().equals(CODE_ERROR_OK))
		{
		    EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
		    EIGlobal.mensajePorTrace("***********   RESULTADO DEL 1ER. LLAMADO AL TUBOCT   ************************", EIGlobal.NivelLog.ERROR);
		    EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
            FMLString = ( String ) htResult.get("FMLSTRING");
        	campo_41204 = ( String ) htResult.get("TIPO_SOC");
	    	EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 TIPO_SOC: " + campo_41204 , EIGlobal.NivelLog.ERROR);
		    campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
		    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 PAIS_ORIGEN: " + campo_41205 , EIGlobal.NivelLog.ERROR);
		    campo_41206 = ( String ) htResult.get("SECTOR");
		    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 SECTOR: " + campo_41206 , EIGlobal.NivelLog.ERROR);
		    campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
		    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 CAMPO_AUX2: " + campo_41265 , EIGlobal.NivelLog.ERROR);
		}
        else
        {
            EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
			return -1;
		}
	}
	catch (Exception e)
	{
		EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 Error en el 1er llamado al TUBOCT: " + e.toString(), EIGlobal.NivelLog.DEBUG);
	}



	/********************************************************************************/
	/*							SIGUIENTES LLAMADOS 								*/
	/********************************************************************************/
	if(	(campo_41204.trim().equals("") && campo_41206.trim().equals("MXP")) || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP"))	)
		continua = 1;
    else
        continua = 0;

	 while(continua==1 && cont<4)
	{
		FMLString_aux = FMLString_aux + FMLString.substring(0,FMLString.length() - 21);

        coderror = "";

 	 	 try
		{
			htResult = tuxGlobal.TUBOCT( producto,subproducto, "BD07A", campo_41206, campo_41265);

	        FMLString = "";
	        campo_41204 = "";
	        campo_41205 = "";
	        campo_41206 = "";
	        campo_41265 = "";

			coderror = ( String ) htResult.get("COD_ERROR");
			EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

            if(coderror==null)
                coderror = "";

	        if(coderror.trim().equals(CODE_ERROR_OK))
			{
                EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
		        EIGlobal.mensajePorTrace("***********   RESULTADO DEL 2DO. LLAMADO AL TUBOCT   ************************", EIGlobal.NivelLog.ERROR);
		        EIGlobal.mensajePorTrace("*************************************************************", EIGlobal.NivelLog.ERROR);
                FMLString = ( String ) htResult.get("FMLSTRING");
    		    campo_41204 = ( String ) htResult.get("TIPO_SOC");
			    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 TIPO_SOC: " + campo_41204 , EIGlobal.NivelLog.ERROR);
			    campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
			    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 PAIS_ORIGEN: " + campo_41205 , EIGlobal.NivelLog.ERROR);
			    campo_41206 = ( String ) htResult.get("SECTOR");
			    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 SECTOR: " + campo_41206 , EIGlobal.NivelLog.ERROR);
			    campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
			    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 CAMPO_AUX2: " + campo_41265 , EIGlobal.NivelLog.ERROR);
            }
			else
			{
			    EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
				return -1;
			}
		}
		 catch (Exception e)
		{
			EIGlobal.mensajePorTrace("***INVERSION 7-28 obten_tasa_BD07 Error en el 2do llamado al TUBOCT: "+e.toString(), EIGlobal.NivelLog.DEBUG);
		}

		if((campo_41204.trim().equals("") && campo_41206.trim().equals("MXP")) || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP"))    )
			continua = 1;
		else
			continua = 0;
		cont = cont + 1;
	}

	FMLString_aux = FMLString_aux + FMLString;
	FMLString = FMLString_aux;

	if(FMLString == null)
	    FMLString = "";

    if(!FMLString.trim().equals(""))
    {

	//Variables para contar los registros
	int noproductos = 0;
	int nocampos = 0;
	int records_serfin = 11;
	int noregistros = 0;

	 for(int i=0; i < FMLString.length();)
	{
		if (FMLString.substring(i,i+1).trim().equals("|"))
			nocampos++;                            //CONTABILIZA LOS PIPES EN LA RESPUESTA
		if (FMLString.substring(i,i+1).trim().equals("@"))
			noproductos++;                         //CONTABILIZA LAS ARROBAS (PRODUCTOS) EN LA RESPUESTA
		i++;
	}

	noregistros = nocampos / records_serfin;    	//OBTIENE EL NUMERO DE REGISTROS EFECTIVOS

	//Variables para separar los datos
	int arrobas = 0;
	int campos = 0, j=0;
	String dato = "";
	int posicionador = 0;

	//Variables para guardar los datos
	String sub_prod = "";
	String instrumento = "";
	String personalidad = "";
	String plazo_may = "";
	String plazo_nuevo = "";
	String plazo_ant = "";
	double imp_min = 0;
	double imp_max = 0;
	double tasa_min = 0;
	double tasa_min_aux = 0;
	double tasa_max = 0;
	double tasa = 0;


	 for(int i=0; i < noregistros;i++)
	{
		sub_prod = "";
		instrumento = "";
		personalidad = "";
		plazo_may = "";
		plazo_nuevo = "";
		plazo_ant = "";
		imp_min  = 0;
		imp_max = 0;
		tasa_min = 0;
		tasa_min_aux = 0;
		tasa_max = 0;
		tasa = 0;

		campos = 0;

		 for (j = posicionador;j < FMLString.length();)         //BARRE CARACTER X CARACTER DE LA RESPUESTA
		{
			 if (FMLString.substring(j,j+1).trim().equals("|")||FMLString.substring(j,j+1).trim().equals("@"))
			{                       // PREGUNTA SI EL CARACTER ES UN PIPE PARA CONTABILIZAR UN CAMPO
		   		 campos++;
		   		 switch(campos)
		   		{
		   			case 1:
		   					sub_prod = dato;
		   					break;
		   			case 4:
		   					imp_min = new Double(dato).doubleValue();
		   					break;
		   			case 5:
		   					imp_max = new Double(dato).doubleValue();
		   					break;
		   			case 6:
		   				    tasa_min = new Double(dato).doubleValue();
		   				    tasa_min_aux = new Double(dato).doubleValue();
		   				    break;
					case 7:
							tasa_max = new Double(dato).doubleValue();
							break;
		   			case 8:
		   					personalidad = dato;
		   					break;
		   			case 9:
		   					plazo_may = dato;
		   				    break;
				}
				dato = "";                                     // CORTA LA CADENA PARA Q UNICAMENTE CONTENGA EL DATO

                if (FMLString.substring(j,j+1).trim().equals("@"))
				{
		   			arrobas++;
		   			posicionador = j+2;                                 //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
					break;
				}
			}
			else
                dato = dato + FMLString.substring(j,j+1);                       //ASIGNA UN CARACTER A LA VARIABLE, MIENTRAS NO SEA UN PIPE

			if(campos == records_serfin)
			{
		   		posicionador = j+1;                                 //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
		   		break;
		   	}
		   	j++;
		} //Fin FOR (para obtener campos * registro)


		if (tasa_min_aux < 0.0001)
		   tasa_min_aux = 0.0;


		//Calcula el valor del instrumento en base al numero del subproducto
		 switch(Integer.parseInt(sub_prod))
		{
			case 21:
					instrumento = "6";
					break;
			case 43:
		   			instrumento = "5";
		   			break;
		   	case 47:
		   			instrumento = "5";
		   			break;
		   	case 44:
		   			instrumento = "14";
		   			break;
		   	case 42:
		   			instrumento = "16";
		   			break;
		   	case 15:
		   			instrumento = "15";
		   			break;
		   	case 52:
		   			instrumento = "7";
		   			break;
		   	case 53:
		   			instrumento = "8";
		   			break;
		   	case 54:
		   			instrumento = "8";
		   			break;
		   	case 55:
		   			instrumento = "9";
		   			break;
		   	case 57:
		   			instrumento = "6";
		   			break;
		   	case 40:
		   			instrumento = "17";
		   			break;
		   	case 60:
		   			instrumento = "1";
		   			break;
		   	case 61:
		   			instrumento = "2";
		   			plazo_may = "V";
		   			break;
		   	case 65:
		   			instrumento = "1";
		   			break;
		   	case 66:
		   			instrumento = "2";
		   			plazo_may = "V";
		   			break;
		   	case 69:
		   			instrumento = "13";
		   			break;
		   	case 70:
		   			instrumento = "3";
		   			break;
		   	case 79:
		   			instrumento = "11";
		   			break;
		   	case 62:
		   			instrumento = "4";
		   			break;
		   	case 67:
		   			instrumento = "4";
		   			break;
		   	case 82:
		   			instrumento = "18";
		   			break;
		   	case 83:
		   			instrumento = "19";
		   			break;
		   	case 64:
		   			instrumento = "12";
		   			break;
		}


		// Valida si el subproducto es diferente de 44
		// Tasa maxima es CERO
		 if (!sub_prod.trim().equals("44"))
		{
			tasa_min = tasa_min_aux;
			tasa_max = 0;
		}

		// Valida si el plazo es mayor a 366 dias,
		// Dividir la tasa /2
		if (!plazo_may.trim().equals("V"))
		    if (Integer.parseInt(plazo_may) >= 366)
			    tasa_min = tasa_min/2;

		if (!sub_prod.trim().equals("44"))
			tasa_max = tasa_min_aux;  // todos los productos excepto '34'

		 if ( imp_max > 0.0 && imp_min >= 0.0 )
		{
		    if(	Integer.parseInt(sub_prod) == 44 || Integer.parseInt(sub_prod) == 69 ||
		     	Integer.parseInt(sub_prod) == 70 || Integer.parseInt(sub_prod) == 62 ||
		     	Integer.parseInt(sub_prod) == 67 ) {

				plazo_nuevo = plazo_may;
		   		plazo_ant = plazo_may;

                if (!plazo_may.trim().equals("V"))
		    	 switch(Integer.parseInt(plazo_nuevo))
		    	{
		    		case 1:
		    				plazo_nuevo = "4";
		    				break;
		            case 5:
		            		plazo_nuevo = "13";
							break;
		            case 14:
		            		plazo_nuevo = "23";
		            		break;
		            case 24:
		            		plazo_nuevo = "39";
		            		break;
		            case 40:
		            		plazo_nuevo = "75";
		            		break;
		            case 76:
		            		plazo_nuevo = "134";
		            		break;
		            case 135:
		            		plazo_nuevo = "249";
		                    break;
		            case 250:
		            		plazo_nuevo = "365";
		            		break;
		            case 366:
		            		plazo_nuevo = "371";
		            		break;
				};

		        //Si es tasa neta
		        plazo_may = plazo_nuevo;
		        tasa = tasa_min;

		   		//Si es tasa bruta
		   		instrumento = "-" + instrumento;
		   		plazo_may = plazo_ant;
		   		tasa = tasa_max;
			}
			 else
			{
				//Si es tasa neta
				tasa = tasa_min;

		   		//Si es tasa bruta
		   		instrumento = "-" + instrumento;
		   		tasa = tasa_max;
		    }

			 if ((personalidad.trim().equals(tipo_persona)) && (instrumento.trim().substring(0,1).trim().equals("-")) )
			{
            	rangomenor = imp_min;
				rangomayor = imp_max;
            	if ( (plazo == Integer.parseInt(plazo_may)) && (importe>=rangomenor&&importe<=rangomayor) )
					tasaplicar = new Double(tasa).doubleValue();
            }
	    }
	}//Fin FOR (para conteo registros)

    }
	return tasaplicar;
}

}




