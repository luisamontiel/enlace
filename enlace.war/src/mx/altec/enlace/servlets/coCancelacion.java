package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.servicioConfirming;
import mx.altec.enlace.dao.CombosConf;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;



/* Bitacora de Cambios
 * LORENA MERCADO REYES (LMR) PRAXIS
 *        - FOLIO INC. Q1087  (18/05/2004) Cambio para el filtro por fecha, que anteriormente se hacia por Emision
 *          en este archivo y en el cfrmsrvrpa.pc lo hacia por fecha de vencimiento, ahora en ambos es el mismo filtro
 *        - FOLIO INC. Q2959  (18/05/2004) Incluir contador para controlar que cuando la consulta tenga 100 o mas
 *          registros muestre un mensaje para que se exporte a un archivo
 */

public class coCancelacion extends BaseServlet
	{

	/**
	* Cadena session usada como nombre de variable de sesion
	*/
	private final static String CADENA_SESSION = "session";
	/**
	* Cadena para log de excepciones
	*/
	private final static String CADENA_PROBLEMA = "coCancelacion::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->";
	/**
	* Cadena para log de datos generales
	*/
	private final static String DATOS_GENERALES = "<DATOS GENERALES>";

	/** Peticion GET
	* @param request : datos de la peticion web
	* @param response : respuesta de la peticion
	* @throws ServletException : ServletException
	* @throws IOException : ServletException
	*/
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{defaultAction(request, response);}

	/** Peticion POST 
	* @param request datos de la peticion web
	* @param response respuesta de la peticion
	* @throws ServletException, IOException excepciones si es que aplican
	*/
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{defaultAction(request, response);}

	/** Punto de atencion para todas las peticiones, todas las solicitudes entran a este metodo 
	 * y este direcciona al metodo adecuado para su atencion
	* @author 'FSW STEFANINI'
	* @param req : req
	* @param res : res
	* @throws ServletException : ServletException
    * @throws IOException : IOException
	*/
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{

		// si la sesion no es valida, no se permite entrar al modulo
		if (!SesionValida(req, res))
			{
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, req, res);
			return;
			}

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute (CADENA_SESSION);
		HttpSession sess = req.getSession();

		//######################## FACULTADES
//		EIGlobal.mensajePorTrace("Se verifica la facultad CCCANCELPAG" ,EIGlobal.ERROR);
		EIGlobal.mensajePorTrace("Se verifica la facultad CCCANCELPAG" ,EIGlobal.NivelLog.ERROR);
		sess.setAttribute("fac_CCCONSPAG", new Boolean(verificaFacultad("CCCONSPAG",req)));
		sess.setAttribute("fac_CCCANCELPAG", new Boolean(verificaFacultad("CCCANCELPAG",req)));
		sess.setAttribute("fac_CCANCELPGINTRNAL", new Boolean(verificaFacultad("CCANCELPGINTRNAL",req)));
        EIGlobal.mensajePorTrace("Se verifica la facultad CCCONSPAG->"
        		              + ((Boolean)sess.getAttribute("fac_CCCONSPAG")).toString(), EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Se verifica la facultad CCCANCELPAG->"
        		              + ((Boolean)sess.getAttribute("fac_CCCANCELPAG")).toString(), EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Se verifica la facultad CCANCELPGINTRNAL->"
	              + ((Boolean)sess.getAttribute("fac_CCANCELPGINTRNAL")).toString(), EIGlobal.NivelLog.INFO);
		if( !verificaFacultad("CCCONSPAG",req) &&
            !verificaFacultad("CCCANCELPAG",req) &&
            !verificaFacultad("CCANCELPGINTRNAL",req))
			{
//			   EIGlobal.mensajePorTrace("No se tiene la facultad CCCANCELPAG" ,1);
			   EIGlobal.mensajePorTrace("No se tiene la facultad CCCANCELPAG" ,EIGlobal.NivelLog.ERROR);

			   String laDireccion = "document.location='SinFacultades'";

			   req.setAttribute( "plantillaElegida", laDireccion);
			   req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

			   return;
			}
		EIGlobal.mensajePorTrace("Se tiene la facultad CCCANCELPAG" ,EIGlobal.NivelLog.ERROR);
		 String inicialCveDivisa="";

		// Se asignan atributos para pasar al jsp

          req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu",       session.getFuncionesDeMenu());
		req.setAttribute("diasInhabiles", diasInhabilesAnt());
		req.setAttribute("Encabezado",    CreaEncabezado("Consulta y cancelaci&oacute;n de pagos", "Confirming &gt; Consulta y cancelaci&oacute;n de pagos","s28300h",req));
		session.setModuloConsultar(IEnlace.MCargo_transf_pesos);
		// se redirige el control segun el tipo de operacion
		String opcion = req.getParameter("tipoReq");
		if(opcion == null){
//			TODO: BIT CU 4351, El cliente entra al flujo
			/*
    		 * VSWF ARR -I
    		 */
			if ("ON".equals(Global.USAR_BITACORAS.trim())){
				try{
				BitaHelper bh = new BitaHelperImpl(req, session,req.getSession ());
				if (req.getParameter(BitaConstants.FLUJO)!=null){
				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				  }else{
				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				  }
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_CONS_CANC_ENTRA);
				bt.setContrato(session.getContractNumber());

				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet coCancelacion", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(CADENA_PROBLEMA
											+ e.getMessage()
				               				+ DATOS_GENERALES
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}catch(Exception e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet coCancelacion", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(CADENA_PROBLEMA
											+ e.getMessage()
				               				+ DATOS_GENERALES
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}
			}
    		/*
    		 * VSWF ARR -F
    		 */
			filtro(req,res);
		}else if("1".equals(opcion)){
			try{
			consulta("",req,res);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar la consulta servlet coCancelacion", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(CADENA_PROBLEMA
										+ e.getMessage()
			               				+ DATOS_GENERALES
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}else if("2".equals(opcion)){
			try{
			cancela(req,res);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar la cancelacion del servlet coCancelacion", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(CADENA_PROBLEMA
										+ e.getMessage()
			               				+ DATOS_GENERALES
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}else if("3".equals(opcion)){
			try{
			exporta(req,res);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar la exportacion del servlet coCancelacion", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(CADENA_PROBLEMA
										+ e.getMessage()
			               				+ DATOS_GENERALES
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		}

	/** Codigo encargado de mostrar el filtro para las consultas
	* @author 'FSW STEFANINI'
	* @param req : req
	* @param res : res
	* @throws ServletException : ServletException
	* @throws IOException : IOException
	*/
	private void filtro(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource)req.getSession ().getAttribute (CADENA_SESSION);

		// Combo de proveedores detallado confirming fase II
        servicioConfirming arch_combos = new servicioConfirming();
        arch_combos.setUsuario(session.getUserID8());
        arch_combos.setContrato(session.getContractNumber());
        arch_combos.setPerfil(session.getUserProfile());
        req.setAttribute("arch_combos", arch_combos);

		// Se preparan atributos para enviar al jsp
		req.setAttribute("Lista",listaProv(req));
		req.setAttribute("Anio", ObtenAnio());
		req.setAttribute("Mes", ObtenMes());
		req.setAttribute("Dia", ObtenDia());

		// se pasa el control al jsp
		evalTemplate("/jsp/coCancelacion.jsp",req,res);
		}


	/** Codigo encargado de atender las consultas realizadas por los clientes desde enlace
	* @author 'FSW STEFANINI'
	* @param mensaje el mensaje de respuesta al usuario
	* @param req datos de la peticion web
	* @param res respuesta de la peticion
	* @throws ServletException, IOException excepciones si es que aplican
	*/
	private void consulta(String mensaje, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		String trama = "";
		StringBuffer info = new StringBuffer("");
		StringBuffer infoUSD = new StringBuffer("");
		int totalUSD = 0;
		int totalMXN = 0;
		Vector notasCanceladas = new Vector();
		String razonSoc ="";

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute (CADENA_SESSION);

		if(!verificaFacultad("CCCONSPAG",req)){
		      req.setAttribute("mensaje", "\"Ud no tiene facultades para consultar pagos.\",3");
		      filtro(req, res);
			  return;
		}
		// Se obtienen y configuran parametros
		String parReg = req.getParameter("Reg");
		String parArch = req.getParameter("Arch");
		String parFecha1 = req.getParameter("fecha1");
		String parFecha2 = req.getParameter("fecha2");
		String parTDoc[] = req.getParameterValues("TDoc");
		String parImporte = req.getParameter("Importe");
		String parProveedor = req.getParameter("Proveedor");
		String divisa = req.getParameter("Divisa");
		String cuentaCargo = req.getParameter("textCuentas");
		String parEstatus[] = req.getParameterValues("Estatus");
		String parNumDoc = req.getParameter("numDocumento");
		// DAG 091009 Nuevo parametro de busqueda
		String parFormaPago = req.getParameter("vForma");
        //LMR Q2959 20/05/2004
		int total_registros=0;
        //LMR Q2959 20/05/2004
		//JIRM STEFANINI 23/MAR/2009 VAR PAR AEL FILTRO DIVISA

		if(parReg == null) {parReg = "5";}
		if(parArch == null) {parArch = "";}
		if(parTDoc == null) {parTDoc = new String[0];;}
		if(parFecha1 == null) {parFecha1 = "";}
		if(parFecha2 == null) {parFecha2 = "";}
		if(parImporte == null) {parImporte = "";}
		if(divisa == null) {divisa = "";}
		if(cuentaCargo == null) {cuentaCargo ="";}
		if(parEstatus == null) {parEstatus = new String[0];}
		if(parProveedor == null) {parProveedor = "";}
		if(parNumDoc == null) {parNumDoc = "";}

		if("1".equals(divisa)){
			divisa="MXN";
		}else if("2".equals(divisa)){
			divisa="USD";
		}else if("0".equals(divisa)){
			divisa="";
		}

		trama = "2EWEB|" + session.getUserID8() + "|CFAE|" + session.getContractNumber() + "|" + session.getUserID8() + "|" +
				session.getUserProfile() + "|" + parReg + "@";
		if("5".equals(parReg)){
			trama += parArch;
		} else {
			trama += parFecha1 + "@" + parFecha2;
		}
		trama += "@|";
		debug("Trama de envio: " + trama, EIGlobal.NivelLog.DEBUG);

		// Se envia la consulta a Tuxedo
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		Hashtable ht = null;
		try
			{ht = tuxGlobal.web_red(trama);}
		catch(Exception e)
			{
			debug("Excepcion al enviar trama a tuxedo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}
		trama = (String) ht.get("BUFFER");
		debug("tuxedo regresa:" + trama, EIGlobal.NivelLog.DEBUG);

		// Se recupera el archivo de resultados de la consulta mediante copia remota
		trama = trama.substring(trama.lastIndexOf('/')+1);
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		if(archRemoto.copia(trama))
			{
			debug("Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +trama;
			}
		else
			{
			debug("Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		debug("///////////////////////// Nombre Viejo: "+ trama, EIGlobal.NivelLog.INFO);
		debug("///////////////////////// Nombre Nuevo: "+ Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas", EIGlobal.NivelLog.INFO);

		// Se renombra el archivo
		File nombreViejo = new File(trama);
		File nombreNuevo = new File(Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas");
		if(nombreViejo.renameTo(nombreNuevo))
			{
			debug("Archivo renombrado", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas";
			}
		else
			{
			debug("Error al renombrar archivo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		// se construye la linea que indica en el jsp la seleccion de consulta
		String sel = ("5".equals(parReg))?("Archivo: "+parArch):("Movimientos del " + parFecha1 + " al " + parFecha2);

		// Se obtiene lista de proveedores
		String lista[][] = listaProv(req);

		// --------------------- Se procesa el archivo ----------------------
		FileWriter exportacion = new FileWriter(Global.DIRECTORIO_REMOTO_WEB + "/" + session.getUserID8() + ".doc");
		FileWriter exportacionAntes = new FileWriter(Global.DIRECTORIO_REMOTO_WEB + "/" + session.getUserID8() + "_act"+ ".doc");
		BufferedReader entrada = new BufferedReader(new FileReader(trama));

debug("///////////////////////////trama: "+trama, EIGlobal.NivelLog.INFO);
		String linea;
		StringBuffer lin = new StringBuffer();
		String lineaAntes;
		String campos[] = new String[42];
		String parClaveEstatus = "";
		StringTokenizer parser;
		int pos;
		boolean tieneDatos = false;
		boolean cancelable = false;
		entrada.readLine();
		while((linea=entrada.readLine()) != null)
			{
debug("///////////////////////////linea: "+linea, EIGlobal.NivelLog.INFO);
			while((pos=linea.indexOf(";;")) != -1){
				linea = linea.substring(0,pos+1) + " " + linea.substring(pos+1);
			}
			parser = new StringTokenizer(linea,";");
			cancelable = false;
			try
				{
				for(pos=0;parser.hasMoreTokens();pos++) campos[pos] = parser.nextToken();
				for(;pos<42;pos++) campos[pos] = "";
				}
			catch(Exception e)
				{
				debug("Excepcion al parsear linea", EIGlobal.NivelLog.INFO);
				error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
				return;
				}
			/**
			for(int xx=0; xx<35; xx++)
				System.out.print(" " +xx+ "= >" +campos[xx]+ "<");
			*/
			EIGlobal.mensajePorTrace("Se asigna la Razon Social", EIGlobal.NivelLog.DEBUG);
			razonSoc = campos[41];
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);

			// En este punto se capturan los numeros de las notas canceladas
			if("2".equals(campos[0]) && "C".equals(campos[11])){
				notasCanceladas.add(campos[3]+"|"+campos[1]+"|"+campos[4]);
			}

			// AQUI ES EL LUGAR PARA APLICAR FILTROS
			if(!"3".equals(campos[0]))
				{
				int car;
				String aux;
				// filtro de fechas y proveedor
				if("6".equals(parReg))
				{
					if(!"".equals(parProveedor) && !parProveedor.equals(campos[3])){
						continue;
					}
					// LMR - INC Q1087 - 18/02/2004 - PRAXIS
					// para filtrar por fecha de vencimiento (campo 10)
					// y no de emision (campo 9)
					// linea anterior -> aux=(campos[0].equals("1"))?campos[9]:campos[7];
					aux=("1".equals(campos[0]))?campos[10]:campos[7];
					if(!(fechaMayor(aux,parFecha1) && fechaMenor(aux,parFecha2))){
						continue;
					}

					if(!"".equals(parNumDoc) && !parNumDoc.trim().equals(campos[4])){
						continue;
					}
				}

				// filtro por estatus
				aux="";
				if("1".equals(campos[0]))
				{
					if("P".equals(campos[18]) && "R".equals(campos[19]))
						{aux="R"; cancelable = true;}
					else if("C".equals(campos[18]) && "R".equals(campos[19]))
						{aux="R";}
					else if("V".equals(campos[18]) && "R".equals(campos[19]))
						{aux="V";}
					else if("P".equals(campos[18]) && "C".equals(campos[19]))
						{aux="C";}
					else if("C".equals(campos[18]) && "C".equals(campos[19]))
						{aux="C";}
					else if("V".equals(campos[18]) && "C".equals(campos[19]))
						{aux="C";}
					else if("C".equals(campos[18]) && "L".equals(campos[19]))
						{aux="L";}
					else if("C".equals(campos[18]) && "P".equals(campos[19]))
						{aux="P";}
					else if("C".equals(campos[18]) && "A".equals(campos[19]))
						{aux="A";}
					else if("C".equals(campos[18]) && "Z".equals(campos[19]))
						{aux="Z";}
					else if("D".equals(campos[18]) && "Z".equals(campos[19]))
						{aux="Z";}
					else if("D".equals(campos[18]) && "C".equals(campos[19]))
						{aux="Z";}
					else if("P".equals(campos[18]) && "A".equals(campos[19]))
						{aux="A";}
					else if("P".equals(campos[18]) && "S".equals(campos[19]))
						{aux="R";}
					else if("V".equals(campos[18]) && "A".equals(campos[19]))
						{aux="V";}
					else if("V".equals(campos[18]) && "C".equals(campos[19]))
						{aux="V";}
				}
				else
					{
					aux = campos[11]; 
					if("R".equals(campos[11])){
					 cancelable = true;
					}
				}

				for(car=0;car<parEstatus.length;car++){
					if(aux.equals(parEstatus[car])){
						break;
					}
				}
				campos[34] = campos[19]; // <-- se necesita el status del pago mas abajo (fechas de op)
				campos[19] = aux;
				if(parEstatus.length>0 && car>=parEstatus.length){
					continue;
				}

				// filtro por tipo de documentos
				for(car=0;car<parTDoc.length;car++){
					if(campos[1].equals(parTDoc[car])){
						break;
					}
				}
				if(parTDoc.length>0 && car==parTDoc.length){
					continue;
				}

				// filtro de importe
				if(!"".equals(parImporte))
					{
					aux=("1".equals(campos[0]))?campos[7]:campos[5];
					int x = (int)(Float.parseFloat(aux)*100);
					int y = (int)(Float.parseFloat(parImporte)*100);
					if(x!=y) continue;
					}
//				JIRM STEFANINI 23/MAR/2009 FILTRO DIVISA
					//	 filtro de divisa
				if(!"".equals(divisa)){

					String aux2=campos[24];
					if("USD".equals(divisa.trim())){
						aux="USD";
					}
					else{
						aux="MXN";
					}
					if(!aux.trim().equals(aux2.trim())){
						continue;
					}

				}

//				JIRM STEFANINI 23/MAR/2009 FILTRO CUENTA DE CARGO
//				 filtro cuenta de cargo
				if(!"".equals(cuentaCargo))
					{
					   aux=campos[23];
					   String aux2=cuentaCargo.substring(0,11);
					   EIGlobal.mensajePorTrace("*****************************************************************",EIGlobal.NivelLog.DEBUG);
					   EIGlobal.mensajePorTrace("SOY EL SUBESTRRING ------***********"+aux2,EIGlobal.NivelLog.DEBUG);
					   EIGlobal.mensajePorTrace("*****************************************************************",EIGlobal.NivelLog.DEBUG);
					if(!aux.trim().equals(aux2.trim())) continue;
				    }
				}

//			DAG 091009 Filtro para Forma de pago
//			 filtro forma de pago
				if(parFormaPago !=null && !"-1".equals(parFormaPago)){
					if(!parFormaPago.equals(campos[25])){
						continue;
					}
				}

			// Con una sola vez que pase el filtro, se tendran datos para mostrar
			if(!"3".equals(campos[0])){
				tieneDatos = true;
			}

			// se rearreglan las fechas para acomodar la fecha de operaqcion en al campo adecuado segun el status
			if("1".equals(campos[0]))
				{
				if(campos[34].length() == 0){
					campos[34]="x"; // <-- solo para asegurar long 1
				}
				char statusPago = campos[34].charAt(0);
				switch(statusPago)
					{
					case 'R': campos[11]=campos[10]; break;
					case 'P': case 'C': case 'A': case 'Z': campos[11]=campos[11]; break;// lo se, es redundante, solo para 'recalcar'
					case 'L': campos[11]=campos[12]; break;
					case 'V': campos[11]=campos[13]; break;
					default: campos[11]=campos[10];
					}
				}
			// Se arman las lineas para escribir en el archivo de exportacion
			if("1".equals(campos[0]))
				{
				linea=rellenar(getClave(campos[3],lista),20,' ','D') + rellenar(razonSoc,80,' ','D') +
					rellenar(campos[1],3,'0','I') + " " + rellenar(campos[4],20,' ','D') +
					rellenar(aplana(campos[7]),21,'0','I') + voltea(campos[10]) + voltea(campos[11]) + rellenar(campos[19], 1, ' ', 'D') +
					rellenar(campos[17],12,' ','I') + rellenar(campos[15],80,' ','D') +
					rellenar(campos[21],7,'0','I') +
					rellenar(campos[22],40,' ','D') +
					// DAG 151009 Modificacion Confirming Fase II
					rellenar(genNomApp(campos[6]),13,' ','D') +
					rellenar(campos[23],20,' ','D') +
					rellenar(campos[24],4,' ','D') +
					rellenar(campos[25],3,'0','I') +
					rellenar(campos[38],30,' ','I') +
					rellenar("",135,' ','I');
				lineaAntes = rellenar(getClave(campos[3],lista),20,' ','D') + rellenar(razonSoc,80,' ','D') +
							 rellenar(campos[1],3,'0','I') + " " + rellenar(campos[4],8,' ','D') +
							 rellenar(aplana(campos[7]),21,'0','I') + voltea(campos[10]) + voltea(campos[11]) + rellenar(campos[19], 1, ' ', 'D') +
							 rellenar(campos[17],12,' ','I') + rellenar(campos[15],80,' ','D') +
							 rellenar(campos[21],7,'0','I') +
							 rellenar(campos[22],40,' ','D') +
							 rellenar(genNomApp(campos[6]),13,' ','D');
				exportacionAntes.write(lineaAntes + "\n");
				exportacion.write(linea + "\n");
				}
			else if("2".equals(campos[0]))
				{
				linea=rellenar(getClave(campos[3],lista),20,' ','D') + rellenar(razonSoc,80,' ','D') +
					rellenar(campos[1],3,'0','I') + " " + rellenar(campos[4],20,' ','D') +
					rellenar(aplana(campos[7]),21,'0','I') + voltea(campos[10]) + voltea(campos[11]) + rellenar(campos[19], 1, ' ', 'D') +
					rellenar(campos[17],12,' ','I') + rellenar(campos[15],80,' ','D') +
					rellenar(campos[21],7,'0','I') +
					rellenar(campos[22],40,' ','D') +
					// DAG 151009 Modificacion Confirming Fase II
					rellenar(genNomApp(campos[6]),13,' ','D') +
					rellenar(campos[23],20,' ','D') +
					rellenar(campos[24],4,' ','D') +
					rellenar(campos[25],3,'0','I') +
					rellenar(campos[38],30,' ','I') +
					rellenar("",135,' ','I');
				lineaAntes = rellenar(getClave(campos[3],lista),20,' ','D') + rellenar(razonSoc,80,' ','D') +
							 rellenar(campos[1],3,'0','I') + " " + rellenar(campos[4],8,' ','D') +
							 rellenar(aplana(campos[7]),21,'0','I') + voltea(campos[10]) + voltea(campos[11]) + rellenar(campos[19], 1, ' ', 'D') +
							 rellenar(campos[17],12,' ','I') + rellenar(campos[15],80,' ','D') +
							 rellenar(campos[21],7,'0','I') +
							 rellenar(campos[22],40,' ','D') +
							 rellenar(genNomApp(campos[6]),13,' ','D');
				exportacionAntes.write(lineaAntes + "\n");
				exportacion.write(linea + "\n");
				}


			// Se arman las tramas de informacion para pasar al jsp
			if(!"3".equals(campos[0]))
				{
				if(cancelable) //campos[19].equals("R")
					{linea = campos[2]+"@"+campos[3]+"@"+campos[1]+"@"+campos[4]+"|";}
				else
					{linea = " |";}
				linea +=getClave(campos[3],lista)+"|";
				}
			else{
				linea = "-|";
			}
			if("1".equals(campos[0]))
				{
				linea += razonSoc + "|" + ((campos[1].equals("1"))?"Factura":"Otros") + "|"+campos[4 ] + "|" +
					 aMoneda(campos[7]) + "|" +campos[24] + "|" + campos[10] + "|" + campos[11] + "|" +
					 ((campos[19].equals("A")||campos[19].equals("P")||campos[19].equals("Z")||campos[19].equals("D"))?("s"+campos[17]):("n"+campos[17])) + "|" +
				 	 getStatus(campos[19]) + "|" + campos[15] + "|" + campos[21] + "|" + campos[22] + "|" + campos[6] + "|"+ campos[23] + "|";
					 parClaveEstatus += ", '" + campos[6] + "'" ;
				}
			else if("2".equals(campos[0]))
				{
				linea += razonSoc+"|Nota de " + (("3".equals(campos[1]))?"cargo":"cr&eacute;dito")+" "+
					(campos[6].equals("A")?"(acreedora)":"(deudora)")+"|"+campos[4]+
					"|"+aMoneda(campos[5])+"|"+campos[24]+"|&nbsp;|"+campos[8]+"|"+
					 ((campos[19].equals("A")||campos[19].equals("P")||campos[19].equals("Z")||campos[19].equals("D"))?("s"+campos[10]):("n"+campos[10])) + "|" +
					getStatus(campos[19])+"|"+campos[9]+"|" + "|" + campos[21] + "|" + campos[22] + "|" + campos[6] + "|"+ campos[23] + "|";
					parClaveEstatus += ", '" + campos[6] + "'";
				}
			else
				{
				if(notasCanceladas.indexOf(campos[3]+"|"+campos[1]+"|"+campos[5])==-1){
					linea += campos[5]+"|"+campos[3]+"|1|"+campos[4];
				} else{
					linea += "XXX|XXX|XXX|XXX|";
				}
			}
			/* DAG 121009 Se agrega el valor forma de pago para mostrar en la consulta*/
			if("1".equals(campos[25])){
				linea += "Mismo banco|";
			}else if("2".equals(campos[25])){
				linea += "Otros bancos|";
			}else if("5".equals(campos[25])){
				linea += "Internacional|";
			}else{
				linea += " |";
			}
			//Se a�ade la clave de Rastreo
			if(!"".equals(campos[38].trim())){
				linea += campos[38] + "|";
			}else{
				linea += "En Proceso" + "|";
			}
			//LFER 28/11/2012 --> Sustitucion H2H Corporativo se agrega canal
			lin.append(linea);
			if (campos[40] == null) {
				lin.append(" |");
			} else {
				lin.append(campos[40]).append('|');
			}
			linea = lin.toString();
			StringBuilder cadena = new StringBuilder("");
			//LFER 28/11/2012 --> Sustitucion H2H Corporativo
			if(campos[19].equals("A") ||campos[19].equals("P") || campos[19].equals("Z") || campos[19].equals("D") ||
			   campos[19].equals("L")){
			
				for(int xx=0; xx<40; xx++) cadena.append(campos[xx]+";");
				{
				cadena.append(razonSoc)
				.append(";");
				}
				cadena.append("|");
				
			}else
			 {			
				cadena.append(" |");
			 }
			linea += cadena.toString();
			
			EIGlobal.mensajePorTrace("LINEA-->"+linea,EIGlobal.NivelLog.DEBUG);
			if(campos[24].equals("USD")){
				infoUSD.append(linea+"\n");
				totalUSD++;
			}else{
				info.append(linea+"\n");
				totalMXN++;
			}
			lin.delete(0, lin.length());
			/* DAG 121009 */
			//LMR Q2959 20/05/2004
			total_registros++;
			//LMR Q2959 20/05/2004

			//debug("#####################################################################", EIGlobal.NivelLog.INFO);
			//debug("info para pantalla: "+info.toString(), EIGlobal.NivelLog.INFO);
			//debug("#####################################################################", EIGlobal.NivelLog.INFO);
//			TODO: BIT CU 4351, A4
			/*
			 * VSWF ARR -I
			 */

			if (Global.USAR_BITACORAS.trim().equals("ON")){
				try{
//					if (parImporte==null || parImporte.equals("") )
//						parImporte="0.00";
				BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_CONS_CANC_ACT_ARCH_PAGO_PROVEED);
				bt.setContrato(session.getContractNumber());
				bt.setServTransTux("CFAE");
				bt.setNombreArchivo(session.getUserID8() + ".doc");
				if(trama!=null){
					 if(trama.substring(0,2).equals("OK")){
						   bt.setIdErr("CONI0000");
					 }else if(trama.length()>8){
						  bt.setIdErr(trama.substring(0,8));
					 }else{
						  bt.setIdErr(trama.trim());
					 }
					}
				if(campos[7]!=null && !campos[7].equals(""))
					bt.setImporte(Double.parseDouble(campos[7]));
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al ejecutar consulta servlet coCancelacion", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("coCancelacion::consulta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ e.getMessage()
				               				+ DATOS_GENERALES
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}catch(Exception e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al ejecutar consulta servlet coCancelacion", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("coCancelacion::consulta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ e.getMessage()
				               				+ DATOS_GENERALES
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}
			}
			/*
			 * VSWF ARR -F
			 */


			}


		entrada.close();
		exportacionAntes.close();
		exportacion.close();
//		debug("informacion a mandar:\n" + info.toString(), EIGlobal.NivelLog.INFO);
//		if(!archRemoto.copiaLocalARemoto (session.getUserID() + ".doc", "WEB" ))
//			{debug("No se pudo enviar el archivo Exportado", EIGlobal.NivelLog.INFO);}
		if(tieneDatos) parClaveEstatus = parClaveEstatus.substring(2);

		// Si al final no se tienen lineas para mostrar, se muestra un aviso
		if(!tieneDatos) {error("La consulta realizada no contiene datos",req,res); return;}

		// Se envian las opciones de consulta al jsp para que no se pierdan
		req.setAttribute("Reg", parReg);
		req.setAttribute("Arch", parArch);
		req.setAttribute("fecha1", parFecha1);
		req.setAttribute("fecha2", parFecha2);
		req.setAttribute("TDoc", parTDoc);
		req.setAttribute("Importe", parImporte);
		req.setAttribute("Proveedor", parProveedor);
		req.setAttribute("Estatus", parEstatus);
		//LMR Q2959 20/05/2004
		req.setAttribute("Total",new Integer(total_registros));
		//LMR Q2959 20/05/2004

		// Se asignan atributos para el jsp
		req.setAttribute("ExportacionAntes","/Download/" + session.getUserID8() + "_act" + ".doc");
		req.setAttribute("Exportacion","/Download/" + session.getUserID8() + ".doc");
		req.setAttribute("Mensaje",mensaje);
		req.setAttribute("Sel",sel);
		req.setAttribute("Info",info.toString());
		// DAG 131009 Nuevas variables por Fase II
		req.setAttribute("InfoUSD",infoUSD.toString());
		req.setAttribute("totalUSD",""+totalUSD);
		req.setAttribute("totalMXN",""+totalMXN);
		// DAG 131009
		req.setAttribute("claveEstatus", parClaveEstatus);
		//inicia Modif PVA 08/03/2003
		req.setAttribute("Encabezado",    CreaEncabezado("Consulta y cancelaci&oacute;n de pagos", "Confirming &gt; Consulta y cancelaci&oacute;n de pagos","s28300Bh",req));
		//fin

		evalTemplate("/jsp/coCancelacionCon.jsp",req,res);
		}


	/** Codigo encargado de realizar la cancelacion de los pagos que haya seleccionado el cliente
	* @author 'FSW STEFANINI'
	* @param req datos de la peticion web
	* @param res respuesta de la peticion
	* @throws ServletException, IOException excepciones si es que aplican
	*/
	private void cancela(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute (CADENA_SESSION);
		HttpSession sess = req.getSession();

		String trama = "";
		String tramaBase = "1EWEB|" + session.getUserID8() + "|CFCP|" + session.getContractNumber() + "|" + session.getUserID8() + "|" +
				session.getUserProfile() + "|";
		String mensaje = "";
		String numDoc;
		String token;
		String tokenFac;
		String tokenx;
		String tokenFacx;

		// si no se tienen facultades, no se permite cancelar
		if (!verificaFacultad("CCCANCELPAG",req) &&
				!verificaFacultad("CCANCELPGINTRNAL",req))
			{
				consulta("1Ud. no tiene facultades para cancelar pagos.", req, res);
				return;
			}

		// Se obtienen y configuran parametros
		String parReg = req.getParameter("Reg");
		String parArch = req.getParameter("Arch");
		String parFecha1 = req.getParameter("fecha1");
		String parFecha2 = req.getParameter("fecha2");
		String parTDoc[] = req.getParameterValues("TDoc");
		String parImporte = req.getParameter("Importe");
		String parProveedor = req.getParameter("Proveedor");
		String parEstatus[] = req.getParameterValues("Estatus");
		String parCancelaciones = req.getParameter("Cancelaciones");

		if(parReg == null){ parReg = "1"; }
		if(parArch == null){ parArch = ""; }
		if(parTDoc == null){ parTDoc = new String[0];; }
		if(parFecha1 == null){ parFecha1 = ""; }
		if(parFecha2 == null){ parFecha2 = ""; }
		if(parImporte == null){ parImporte = ""; }
		if(parEstatus == null){ parEstatus = new String[0]; }
		if(parProveedor == null){ parProveedor = ""; }
		if(parCancelaciones == null){ parCancelaciones = "";}

		req.setAttribute("Reg", parReg);
		req.setAttribute("Arch", parArch);
		req.setAttribute("fecha1", parFecha1);
		req.setAttribute("fecha2", parFecha2);
		req.setAttribute("TDoc", parTDoc);
		req.setAttribute("Importe", parImporte);
		req.setAttribute("Proveedor", parProveedor);
		req.setAttribute("Estatus", parEstatus);
		req.setAttribute("Cancelaciones", parCancelaciones);

		Hashtable ht = null;
		StringTokenizer cancelacion = new StringTokenizer(parCancelaciones,"\n");
		StringTokenizer cancelacionFac = new StringTokenizer(parCancelaciones,"\n");
		StringTokenizer cancelFac = null;

		// para cada cuenta a cancelar:
		try
			{
			//Ciclo para validar facultades
			while(cancelacionFac.hasMoreTokens())
			{
				tokenx = cancelacionFac.nextToken();
				while(tokenx.substring(tokenx.length()-1).equals("\r")) tokenx=tokenx.substring(0,tokenx.length()-1);
				cancelFac = new StringTokenizer(tokenx,"|");
				tokenFacx = cancelFac.nextToken();

				if((tokenFacx.equals("Mismo banco") || tokenFacx.equals("Otros bancos"))&&
						!verificaFacultad("CCCANCELPAG",req)){
					consulta("1Ud. no tiene facultades para cancelar pagos nacionales.", req, res);
					return;
				}else if(tokenFacx.equals("Internacional") &&
						!verificaFacultad("CCANCELPGINTRNAL",req)){
					consulta("1Ud. no tiene facultades para cancelar pagos internacionales.", req, res);
					return;
				}
			}

			while(cancelacion.hasMoreTokens())
				{
				// Se crea la trama para Tuxedo
				token = cancelacion.nextToken();
				while(token.substring(token.length()-1).equals("\r")) token=token.substring(0,token.length()-1);
				cancelFac = new StringTokenizer(token,"|");
				tokenFac = cancelFac.nextToken();

				EIGlobal.mensajePorTrace("FormaPago->"+tokenFac, EIGlobal.NivelLog.DEBUG);

				token = cancelFac.nextToken();
				trama = tramaBase + token + "@|";
//				debug("Trama de envio: " + trama, EIGlobal.NivelLog.DEBUG);

				// Se recupera el numero de documento
				numDoc = trama.substring(0,trama.lastIndexOf("@"));
				numDoc = numDoc.substring(numDoc.lastIndexOf("@")+1);

				// Se envia la consulta a Tuxedo
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				ht = tuxGlobal.web_red(trama);
				trama = (String)ht.get("BUFFER");
//				debug("tuxedo regresa:" + trama, EIGlobal.NivelLog.DEBUG);
				if(!trama.startsWith("OK"))
					{
					error(trama.substring(16),req,res);
					return;
					}
				else
					{//mensaje = "1Documento " + numDoc + " cancelado<br>";
					   mensaje = "1Cancelacion exitosa<br>";
					}
				}
			}
		catch(Exception e)
			{
//			debug("Excepcion al enviar trama a tuxedo: " + e, EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}
//		TODO: BIT CU 4351, A8
		/*
		 * VSWF ARR -I
		 */
		if (Global.USAR_BITACORAS.trim().equals("ON")){
			try{
			BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_CONS_CANC_CANC_PAGO_PROVEED);
			bt.setContrato(session.getContractNumber());
			bt.setServTransTux("CFCP");
			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch (SQLException e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar cancelacion servlet coCancelacion", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coCancelacion::cancela:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ DATOS_GENERALES
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar cancelacion servlet coCancelacion", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coCancelacion::cancela:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ DATOS_GENERALES
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		/*
		 * VSWF ARR -F
		 */
		consulta(mensaje,req,res);
		}
	/** Codigo encargado de exportart a un archivo de texto plano el detalle de pagos mostrados en pantalla
	* @author 'FSW STEFANINI'
	* @param req datos de la peticion web
	* @param res respuesta de la peticion
	* @throws ServletException, IOException excepciones si es que aplican
	*/
	private void exporta(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		String downloadingFile = "";
		try{
			String tipoExportacion = (String) getFormParameter(req, "tipoExportacion");
			EIGlobal.mensajePorTrace("coCancelacion::exporta:: Se exporta->"+tipoExportacion, EIGlobal.NivelLog.DEBUG);
			if(tipoExportacion.equals("SD")){
				downloadingFile = (String) getFormParameter(req, "fileExportacionAntes");
			}else if(tipoExportacion.equals("CD")){
				downloadingFile = (String) getFormParameter(req, "fileExportacion");
			}
			saveAs(downloadingFile, res);
		}catch (Exception e) {
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al exportar archivo de consulta de pagos", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("coCancelacion::exporta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(DATOS_GENERALES, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
		}
	}


	/** Imprime un mensaje al log */
//	private void debug(String mensaje, EIGlobal nivel)
//		{
//		EIGlobal.mensajePorTrace("<DEBUG coCancelacion.java> " + mensaje, nivel);
//		}

	/** Imprime un mensaje al log
	* @param mensaje el menssaje a imprimir
	* @param nivel el nivel de log
	*/
	private void debug(String mensaje, EIGlobal.NivelLog nivel)
		{
		EIGlobal.mensajePorTrace("<DEBUG coCancelacion.java> " + mensaje, nivel);
		}

	/** Muestra una pagina de error con la descripcion indicada
	* @param mensaje el mensaje de error
	* @param request datos de la peticion web
	* @param response respuesta de la peticion
	*/
	private void error(String mensaje,HttpServletRequest request, HttpServletResponse response)
		{
		try
			{
			despliegaMensaje(mensaje,
				"Consulta y cancelaci&oacute;n de pagos",
				"Confirming &gt; Consulta y cancelaci&oacute;n de pagos",
				request,
				response);
			}
		catch(Exception e){
//			debug("Excepcion al intentar mostar la pantalla de error", EIGlobal.NivelLog.INFO);
			}
		}

	/** Devuelve una lista con los nombres de proveedores ([ren][1]) y sus respectivos codigos ([ren][0])
	* @param req datos de la peticion web
	* @return arreglo de string
	*/
	private String[][] listaProv(HttpServletRequest req)
		{

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute (CADENA_SESSION);

		// Se prepara el objeto CombosConf usado para consultas a Tuxedo
		CombosConf arch_combos = new CombosConf();
		//arch_combos.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		arch_combos.setUsuario(session.getUserID8());
		arch_combos.setContrato(session.getContractNumber());
		arch_combos.setPerfil(session.getUserProfile());

		Vector lineas = new Vector();
		String [][] lista = null;
		BufferedReader entrada = null;
		String linea = null;
		String campos[] = new String[6];
		StringTokenizer parser = null;

		try
			{
				debug("*********Entrando a CombosConf*******", EIGlobal.NivelLog.INFO);
			/* Q1346 JEV Inicio Getronics CP */
			String archivo = arch_combos.envia_tux_Cancelacion(session.getUserID8());
			debug("***archivo de regreso= "+archivo, EIGlobal.NivelLog.INFO);
			if(archivo.indexOf(session.getUserID8())<0)
				return null;
			/* Q1346 JEV FIN Getronics CP */

			entrada = new BufferedReader(new FileReader(archivo));
			int car;

			while((linea=entrada.readLine())!=null)
			{
				if(!linea.startsWith("9")){
					 continue;
				}
				while((car=linea.indexOf(";;"))!=-1){
					linea = linea.substring(0,car+1) + " " + linea.substring(car+1);
				}
				lineas.add(linea);
			}
			lista = new String[lineas.size()][3];
			for(car=0;car<lineas.size();car++)
				{
				linea=(String)lineas.get(car);
				parser = new StringTokenizer(linea,";");
				parser.nextToken();
				for(int x=0;x<6;x++){
					campos[x]=parser.nextToken();
				}
				if("F".equals(campos[0])){
					campos[3] += " " + campos[4] + " " + campos[5];
				}
				lista[car][0]=campos[1];
				lista[car][1]=campos[3];
				lista[car][2]=campos[2];
			}
		}
		catch(Exception e){
			debug("Excepcion en listaProv", EIGlobal.NivelLog.INFO);
		}

		// se ordena el arreglo por burbuja
		int a, b;
		for(a=0;a<lista.length-1;a++)
			for(b=a;b<lista.length;b++)
				if(lista[a][1].compareTo(lista[b][1])>0)
					{
					linea = lista[a][2]; lista[a][2] = lista[b][2]; lista[b][2] = linea;
					linea = lista[a][1]; lista[a][1] = lista[b][1]; lista[b][1] = linea;
					linea = lista[a][0]; lista[a][0] = lista[b][0]; lista[b][0] = linea;
					}

		return lista;
		}

	/** Devuelve el nombre de un proveedor segun su codigo
	* @param clave la clave del proveedor
	* @param lista un arreglo de tipo lista con los proveedores
	* @return el nombre del proveedor
	*/
	/*
	private String getProv(String clave, String[][] lista)
		{
		String prov = "no disponible";
		int pos;
		for(pos=0;pos<lista.length;pos++)
			{if(clave.equals(lista[pos][0])) {prov=lista[pos][1]; break;}}
		return prov;
		}*/
	/** Devuelve la clve de un proveedor segun su codigo
	* @param clave la clave del proveedor
	* @param lista un arreglo de tipo lista con los proveedores
	* @return la clave del proveedor
	*/
	private String getClave(String clave, String[][] lista)
		{
		String prov = "no disponible";
		int pos;
		for(pos=0;pos<lista.length;pos++)
			{if(clave.equals(lista[pos][0])) {prov=lista[pos][2]; break;}}
		return prov;
		}

	/** Devuelve el estatus del documento segun el tipo indicado
	* @param tipoDoc tipo de documento
	* @return el estatus del documento
	*/
	private String getStatus(String tipoDoc)
		{
		if("R".equals(tipoDoc.trim())){
			tipoDoc = "Por Pagar";
		}else if("L".equals(tipoDoc.trim())){
			tipoDoc = "Liberado";
		}else if("P".equals(tipoDoc.trim())){
			tipoDoc = "Pagado";
		}else if("A".equals(tipoDoc.trim())){
			tipoDoc = "Anticipado";
		}else if("C".equals(tipoDoc.trim())){
			tipoDoc = "Cancelado";
		}else if("Z".equals(tipoDoc.trim())){
			tipoDoc = "Rechazado";
		}else if("V".equals(tipoDoc.trim())){
			tipoDoc = "Vencido";
		}else if("D".equals(tipoDoc.trim())){
			tipoDoc = "Devuelto";
		}else{
			tipoDoc = "no especificado";}
		return tipoDoc;
		}

	/** Indica si 'mayor' es mayor a 'menor'
	* @param mayor el valor mayor
	* @param menor el valor menor
	* @return true si es una fecha mayor
	*/
	private boolean fechaMayor(String mayor, String menor)
		{
		int max = Integer.parseInt(mayor.substring(6,10) + mayor.substring(3,5) + mayor.substring(0,2));
		int min = Integer.parseInt(menor.substring(6,10) + menor.substring(3,5) + menor.substring(0,2));
		return max>=min;
		}

	/** Indica si 'menor' es mayor a 'mayor'
	* @param menor el valor menor
	* @param mayor el valor mayor
	* @return true si es una fecha menor
	*/
	private boolean fechaMenor(String menor, String mayor)
		{
		int max = Integer.parseInt(mayor.substring(6,10) + mayor.substring(3,5) + mayor.substring(0,2));
		int min = Integer.parseInt(menor.substring(6,10) + menor.substring(3,5) + menor.substring(0,2));
		return max>=min;
		}

	/** Devuelve un texto que representa un valor monetario
	* @param num el numero
	* @return cadena en formato de moneda
	*/
	private String aMoneda(String num)
		{
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()){ num = num.substring(0,num.length()-1);}
		while(pos+3>num.length()){ num += "0";}
		while(num.length()<4){ num = "0" + num;}
		for(int y=num.length()-6;y>0;y-=3){ num = num.substring(0,y) + "," + num.substring(y);}
		return num;
		}

	/** Prepara una importe para colocarlo en un archivo de exportacion
	* @param num el numero
	* @return elimina el formateo de moneda
	*/
	private String aplana(String num)
		{
		String aux = "";
		num=aMoneda(num);
		for(int x=0;x<num.length();x++){ if(",$.".indexOf(num.substring(x,x+1)) == -1){ aux += num.substring(x,x+1);}}
		return aux;
		}

	/** Prepara una fecha para colocarla en un archivo de exportacion
	* @param fecha la fecha
	* @return invierte el formato de la fecha
	*/
	private String voltea(String fecha)
		{
		if(fecha.length()<10){ return "        ";}
		return fecha.substring(0,2) + fecha.substring(3,5) + fecha.substring(6,10);
		}

	/** rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 * @return la cadena rellenada
	 */
	public static String rellenar (String cad, int lon, char rel, char tip)
		{
		String aux = "";
		if(cad.length()>lon){ return cad.substring(0,lon);}
		if(tip!='I' && tip!='D'){ tip = 'I';}
		if( tip=='D' ){ aux = cad;}
		for (int i=0; i<(lon-cad.length()); i++){ aux += ""+ rel;}
		if( tip=='I' ){ aux += ""+ cad;}
		return aux;
		}

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	/**
	* despliega un mensaje
	* @param mensaje el mensaje
	* @param param1 parametro 1
	* @param param2 parametro 2
	* @param request datos de la peticion web
	* @param response datos de la respuesta
	* @throws ServletException, IOException la excpecion si es que aplica
	*/
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{

		// parche de Ramon Tejada
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(CADENA_SESSION);
		EIGlobal Global = new EIGlobal( session , getServletContext() , this );

		String contrato_ = session.getContractNumber();

		if(contrato_==null){ contrato_ ="";}

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		request.setAttribute( "URL", "coCancelacion");

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s28300Bh", request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}

	/**
	 * Metodo encargado de generar el nombre de la forma aplica
	 *
	 * @param app		Generando el nombre de la aplicaci�n
	 * @return			Nombre de la forma aplica
	 */
	private String genNomApp(String app) {
		if ("1".equals(app)){ return "MISMO DIA";}
		if ("2".equals(app)){ return "DIA SIGUIENTE";}
		return "MISMO DIA";
	}

    /**
     * Metodo especial para descargar un archivo en linea a una ruta especifica
     *
     * @param nameFile		Nombre del Archivo
     * @param response		Response de la peticion
     */
    private void saveAs(String nameFile, HttpServletResponse response){
	  	try{
			String newNameFile = nameFile.replaceAll("/Download/", "");
			EIGlobal.mensajePorTrace("coCancelacion::saveAs:: Ruta->" + Global.DIRECTORIO_REMOTO_WEB + newNameFile, EIGlobal.NivelLog.DEBUG);
			java.io.FileInputStream archivo = new java.io.FileInputStream(Global.DIRECTORIO_REMOTO_WEB + "/" + newNameFile);
			int longitud = archivo.available();
			byte[] datos = new byte[longitud];
			archivo.read(datos);
			archivo.close();
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition","attachment;filename=" + newNameFile);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(datos);
			ouputStream.flush();
			ouputStream.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("saveAs exception:"+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
	}

	}