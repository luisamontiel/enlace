package mx.altec.enlace.servlets;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


/**
 * @author C109834
 *
 */
public class logout extends BaseServlet
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1854220825327759014L;
	/**
	 * Clave de operacion Micrositio
	 */
	private final static String CLAVE_MICROSITIO = "PMIC";
	/**
	 * Codigo de Exito de la operacion Micrositio
	 */
	private final static String COD_EXITO_MICROSITIO = "PMIC0000";

	
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
		defaultAction( request, response );
	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace( "logout.java::defaultAction()::", EIGlobal.NivelLog.DEBUG);
		/*<VC proyecto="200710001" autor="JGR" fecha="21/05/2007" descripci�n="RECUPERAMOS LA PLANTILLA">*/
		
		boolean operacionOK = false;
		if(request.getParameter("operacionOK") != null) {
			if ("S".equals(request.getParameter("operacionOK"))) {
				operacionOK = true;	
			}
			EIGlobal.mensajePorTrace("logout.java::defaultAction() - llama a logoutMicrositio", EIGlobal.NivelLog.DEBUG);
			logoutMicrositio(request, response, operacionOK);
			return;
		}

		
		EIGlobal.mensajePorTrace("PLANTILLA EN LOGOUT->" +
				request.getSession().getAttribute("plantillaSAM"), EIGlobal.NivelLog.DEBUG);

		if (request.getSession().getAttribute("plantillaSAM") != null && !((String)
			request.getSession().getAttribute("plantillaSAM")).equals("")){

			request.setAttribute("plantillaSAM",
					request.getSession().getAttribute("plantillaSAM"));
		}

		EIGlobal.mensajePorTrace("PLANTILLA EN LOGOUT 2->" +
				request.getAttribute("plantillaSAM"), EIGlobal.NivelLog.DEBUG);
		/*<VC TERMINA>*/
		boolean cerrar = false;
		if(request.getParameter("cerrar") != null && request.getParameter("cerrar").equals("S")) {
			cerrar = true;
		}

		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource)ses.getAttribute("session");
		/*<VC proyecto="200710001" autor="JGR" fecha="30/05/2007" descripci�n="DIRECCIONAMOS A SUPERNET">*/
		String destino = (String)request.getParameter("destino");

		//INICIA ADICI�N DE LOGICA PARA USUARIO ADMIN SUPERNET COMERCIOS
		String usrSnetComAdmin = null;
		if((String)request.getParameter("usrSnetComAdmin")!=null)
			usrSnetComAdmin = (String)request.getParameter("usrSnetComAdmin");
		else if((String)ses.getAttribute("usrSnetComAdmin")!=null)
			usrSnetComAdmin = (String)ses.getAttribute("usrSnetComAdmin");
		else
			usrSnetComAdmin = "";
		Vector ctasSnetComAdmin = (Vector) ses.getAttribute("ctasSnetComAdmin")!=null?(Vector)ses.getAttribute("ctasSnetComAdmin"):new Vector();
		//FINALIZA ADICI�N DE LOGICA PARA USUARIO ADMIN SUPERNET COMERCIOS

		destino = (destino==null?"":destino.trim());
		EIGlobal.mensajePorTrace("Destino: "+ destino, EIGlobal.NivelLog.DEBUG);

		/*<VC TERMINA>*/
		if(session == null) {
						ses.removeAttribute("session");
						ses.setMaxInactiveInterval( 0 );
						ses.invalidate();

						if(!cerrar) {
							evalTemplate( IEnlace.LOGIN, request, response );
						}
						else{
							evalTemplate( IEnlace.CERRAR, request, response );
						}
						log( "Sesi�n vencida...");
						return;
		}

		if (destino!=null && destino.equals("supernet")){
			String urlSupernet = Global.URL_SUPERNET+"?cliente="+session.getUserID8()+"&contrato=" + Global.PREF_CONTR_SNET_EMPR + session.getContractNumber();
			request.setAttribute("snetuser",session.getUserID8());

			request.setAttribute("snetcont", session.getContractNumber());
			EIGlobal.mensajePorTrace("Saliendo de enlace y diriendose a supernet: " + urlSupernet, EIGlobal.NivelLog.DEBUG );
			cierraDuplicidad(session.getUserID8());
			ses.removeAttribute("session");
			ses.setMaxInactiveInterval( 0 );
			ses.invalidate();
			request.setAttribute("urlredirsnet", urlSupernet);
			evalTemplate (Global.URL_SUPERNET,request, response);
			//response.sendRedirect(urlSupernet);
			return;
		}


		/*
		 * Se evalua si se tiene mas de una cuenta para operar en supernet comercios
		 * JPH VSWF 04/06/2009
		 * se valida si el destino es supernet comercios
		 */
		EIGlobal.mensajePorTrace("DESTINO DE SALIDA " + destino, EIGlobal.NivelLog.INFO);
		if(destino!=null && destino.equals("supernetComer")){

			int esAdmin = 1;
			HashMap<String , String> cuentasComercios = session.getCtasSnetComercios();
			//sacamos los valores del HasMap.
			String sessionID = 	request.getSession().getId();
			//validamos que venga de seleccionar una cuenta
			String valCuenta = (String) request.getParameter("lstCuentas");
			boolean exito = false;
			Hashtable referencia = new Hashtable();
			Hashtable resultBit = new Hashtable();
			ServicioTux st = new ServicioTux();
			String codError = "";
			int numRef = 0;

			//INICIA FLUJO ADMINISTRADOR SUPERNET COMERCIOS
			if(usrSnetComAdmin.equals("admin")){
				//FLUJO 1. EL USUARIO ADMINISTRADOR ELIGIO CUENTA Y SE REDIRIGE A SUPERNET COMERCIOS
				if(ctasSnetComAdmin.size()>0){
					EIGlobal.mensajePorTrace("logout::defaultAction:: Se eligio un comercio,"
							               + " se redirecciona a Supernet Comercios.", EIGlobal.NivelLog.INFO);
					try{
						StringBuffer cuentas = new StringBuffer();
						for(int k=0; k<ctasSnetComAdmin.size(); k++){
							String cuenta = (String)ctasSnetComAdmin.get(k);
							cuentas.append(cuenta + "|");
						}
						//SE OBTIENE REFERNCIA PARA REGISTRAR EN BITACORA
						try{
							EIGlobal.mensajePorTrace("logout::defaultAction:: Insertando informacion en bitacora." + session.getContractNumber(), EIGlobal.NivelLog.INFO);
							//SE INSERTA EN BITACORA TIPO OPERACION CCSC
							referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							numRef = (Integer)referencia.get("REFERENCIA");
							codError=(String)referencia.get("COD_ERROR");
							resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), valCuenta, 0, "CCSC", "", 0, "");
							//SE INSERTA EN BITACORA TIPO OPERACION CSEN
							referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							numRef = (Integer)referencia.get("REFERENCIA");
							codError=(String)referencia.get("COD_ERROR");
							resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), valCuenta, 0, "CSEN", "", 0, "");
							//SE INSERTA EN BITACORA TIPO OPERACION ESEN
							referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							numRef = (Integer)referencia.get("REFERENCIA");
							codError=(String)referencia.get("COD_ERROR");
							resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), valCuenta, 0, "ESEN", "", 0, "");
							EIGlobal.mensajePorTrace("logout::defaultAction:: Informacion guardada en bitacora con resultado --->" + resultBit, EIGlobal.NivelLog.INFO);
							//SE INSERTA EN BITACORA TRANSACCIONAL
							EIGlobal.mensajePorTrace("logout::defaultAction:: Se realiza insertbitatransac() --->", EIGlobal.NivelLog.INFO);
							BitaTransacBean bt = new BitaTransacBean();
							BitaHelper bh = new BitaHelperImpl(request, (BaseResource) request.getSession ().getAttribute ("session"),request.getSession());
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.REDIRECCION_SUPERNET_COMERCIOS);
							bt.setContrato(session.getContractNumber());
							BitaHandler.getInstance().insertBitaTransac(bt);
							EIGlobal.mensajePorTrace("logout::defaultAction:: Bitacorizacion exitosa --->", EIGlobal.NivelLog.INFO);
						}catch(Exception e){
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("logout::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE, "
												   + "AL GENERAR LA BITACORA->" + e.getMessage()
												   + "<DATOS GENERALES>"
												   + "Usuario->" + session.getUserID8()
												   + "Contrato->" + session.getContractNumber()
												   + "Linea de truene->" + lineaError[0]
									               , EIGlobal.NivelLog.ERROR);
						}

						//SE GUARDA LA INFORMACION EN TABLA DE ORACLE DE SUPERNET COMERCIOS.
						exito = insertaDatosSnetComercios(sessionID, session.getUserID8(), session.getNombreUsuario(), session.getContractNumber(), IEnlace.CANAL_ENLACE, valCuenta, cuentas.toString(), esAdmin);
						if(exito){
							String urlSuperNetComercios = Global.URL_SUPERNET_COMERCIOS+"?jsessionid=" + sessionID;
							cierraDuplicidad(session.getUserID8());
							ses.removeAttribute("session");
							ses.setMaxInactiveInterval( 0 );
							ses.invalidate();
							request.setAttribute("urlSnetComer", urlSuperNetComercios);
							//evalTemplate(Global.URL_SUPERNET_COMERCIOS,request,response);
							evalTemplate("/jsp/redirSupCom.jsp",request,response);
							return;
						}else{
							EIGlobal.mensajePorTrace("logout::defaultAction:: No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
							request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
							evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
							return;
						}
					} catch (Exception e){
						EIGlobal.mensajePorTrace("logout::defaultAction:: No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
						request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
						evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
						return;
					}

				}else{//FLUJO 2. EL USUARIO ADMINISTRADOR TIENE UNA O MAS CUENTAS DE COMERCIOS
					String optionCuentas = "";
					EIGlobal.mensajePorTrace("logout::defaultAction:: Cargando muestra cuentas nivel "
										   + "administrador Supernet Comercios", EIGlobal.NivelLog.INFO);
					int total = getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/"+ session.getUserID8()+".ambci");
					String[][] arrayCuentas = ObteniendoTodasCtasInteg (total,IEnlace.LOCAL_TMP_DIR + "/"+ session.getUserID8()+".ambci");
					String cuentasEnlace = "";
					Vector comerciosAdmin = null;
					HashMap mapCtasEnlace = new HashMap();
					EIGlobal.mensajePorTrace("logout::defaultAction:: Empezando el Match Cuenta enlace y Cuenta Comercios", EIGlobal.NivelLog.DEBUG);
					for(int i=0; i<arrayCuentas.length; i++){
						cuentasEnlace +=  "'" + arrayCuentas[i][1] + "'";
						if(i<(arrayCuentas.length-1))
							cuentasEnlace +=  ",";
						mapCtasEnlace.put(arrayCuentas[i][1], arrayCuentas[i][4]);
					}

					comerciosAdmin = getComerciosAdmin(cuentasEnlace);
					EIGlobal.mensajePorTrace("logout::defaultAction:: Armado la pantalla de selecci�n de comercio", EIGlobal.NivelLog.DEBUG);
					if(comerciosAdmin.size()==1){//FLUJO 3. EL USUARIO ADMINISTRADOR TIENE UNA CUENTA DE COMERCIOS
						try{
							EIGlobal.mensajePorTrace("logout::defaultAction:: Solo se tiene un comercio", EIGlobal.NivelLog.DEBUG);
							String cuenta = (String) comerciosAdmin.get(0);
							//se obtiene referencia para registrar en bitacora.
							try{
								EIGlobal.mensajePorTrace("logout::defaultAction:: Insertando informacion en bitacora." + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								//SE INSERTA EN BITACORA TIPO OPERACION CCSC
								referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
								numRef = (Integer)referencia.get("REFERENCIA");
								codError=(String)referencia.get("COD_ERROR");
								resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), cuenta, 0, "CCSC", "", 0, "");
								//SE INSERTA EN BITACORA TIPO OPERACION CSEN
								referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
								numRef = (Integer)referencia.get("REFERENCIA");
								codError=(String)referencia.get("COD_ERROR");
								resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), cuenta, 0, "CSEN", "", 0, "");
								//SE INSERTA EN BITACORA TIPO OPERACION ESEN
								referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
								numRef = (Integer)referencia.get("REFERENCIA");
								codError=(String)referencia.get("COD_ERROR");
								resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), cuenta, 0, "ESEN", "", 0, "");
								EIGlobal.mensajePorTrace("logout::defaultAction:: Informacion guardada en bitacora con resultado --->" + resultBit, EIGlobal.NivelLog.INFO);
								//se inserta en bitacora transaccional

								EIGlobal.mensajePorTrace("logout::defaultAction:: Se realiza insertbitatransac() --->", EIGlobal.NivelLog.INFO);
								BitaTransacBean bt = new BitaTransacBean();
								BitaHelper bh = new BitaHelperImpl(request, (BaseResource) request.getSession ().getAttribute ("session"),request.getSession());
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.REDIRECCION_SUPERNET_COMERCIOS);
								bt.setContrato(session.getContractNumber());
								BitaHandler.getInstance().insertBitaTransac(bt);
								EIGlobal.mensajePorTrace("logout::defaultAction:: Bitacorizacion exitosa --->", EIGlobal.NivelLog.INFO);
							}catch(Exception e){
								StackTraceElement[] lineaError;
								lineaError = e.getStackTrace();
								EIGlobal.mensajePorTrace("logout::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE, "
													   + "AL GENERAR LA BITACORA->" + e.getMessage()
													   + "<DATOS GENERALES>"
													   + "Usuario->" + session.getUserID8()
													   + "Contrato->" + session.getContractNumber()
													   + "Linea de truene->" + lineaError[0]
										               , EIGlobal.NivelLog.ERROR);
							}

							//inserta informacion en base de datos de supernet comercios.
							exito = insertaDatosSnetComercios(sessionID, session.getUserID8(), session.getNombreUsuario(), session.getContractNumber(), IEnlace.CANAL_ENLACE, cuenta, cuenta, esAdmin);
							if(exito){
								String urlSuperNetComercios = Global.URL_SUPERNET_COMERCIOS+"?jsessionid=" + sessionID;
								cierraDuplicidad(session.getUserID8());
								ses.removeAttribute("session");
								ses.setMaxInactiveInterval( 0 );
								ses.invalidate();
								request.setAttribute("urlSnetComer", urlSuperNetComercios);
								//evalTemplate(Global.URL_SUPERNET_COMERCIOS,request,response);
								evalTemplate("/jsp/redirSupCom.jsp",request,response);
								return;
							}else{
								EIGlobal.mensajePorTrace("logout::defaultAction:: No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
								request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
								evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
								return;
							}
						} catch (Exception e){
							EIGlobal.mensajePorTrace("logout::defaultAction:: No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
							request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
							evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
							return;
						}
					}else if (comerciosAdmin.size()>1){//FLUJO 4. EL USUARIO ADMINISTRADOR TIENE MAS DE UNA CUENTA DE COMERCIOS Y DEBE ELEGIRLA
						EIGlobal.mensajePorTrace("logout::defaultAction:: Se tiene mas de un comercio", EIGlobal.NivelLog.DEBUG);
						for(int j=0; j<comerciosAdmin.size(); j++){
						optionCuentas += "<OPTION Value = \"" + comerciosAdmin.get(j) + "\">" + comerciosAdmin.get(j) + " " + mapCtasEnlace.get(comerciosAdmin.get(j)) + " </OPTION>";
						}
						request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Seleccione Cuenta", "Supernet Comercios &gt; Selecci&oacute;n de Cuenta" ,"s60040h",request));
						ses.setAttribute("usrSnetComAdmin", usrSnetComAdmin);
						ses.setAttribute("ctasSnetComAdmin", comerciosAdmin);
						request.setAttribute("ctasComercios", optionCuentas);
						evalTemplate(IEnlace.MUESTRA_CUENTAS,request,response);
						return;
					}else {
						EIGlobal.mensajePorTrace("logout::defaultAction:: Flujo Administrador no tiene comercios asociados.", EIGlobal.NivelLog.DEBUG);
						request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No existen comercios asociados.\",3);" );
						evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
						return;
					}
				}
				//TERMINA FLUJO ADMINISTRADOR SUPERNET COMERCIOS

			}else if( (valCuenta!=null && !valCuenta.equals("")) && (cuentasComercios!=null && cuentasComercios.size()>1 )){
				esAdmin = 0;
				// SI ENTRA AQUI ES POR QUE VIENE DE LA PAGINA DE SELECCION DE CUENTA
				EIGlobal.mensajePorTrace("se selecciono la cuenta... se procede a redireccionar a supernet...", EIGlobal.NivelLog.INFO);
				try {

					StringBuffer cuentas = new StringBuffer();
					Set s=null;
					s = cuentasComercios.entrySet();
					Iterator iter = s.iterator();
					while (iter.hasNext()){
						Entry entry = (Entry)iter.next();
						String cuenta = (String)entry.getKey();
						cuentas.append(cuenta + "|");

					}
//					se obtiene referencia para registrar en bitacora.
					try{
						EIGlobal.mensajePorTrace("INSERTANDO INFORMACION EN BITACORA." + session.getContractNumber(), EIGlobal.NivelLog.INFO);
						//SE INSERTA EN BITACORA TIPO OPERACION CCSC
						referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
						numRef = (Integer)referencia.get("REFERENCIA");
						codError=(String)referencia.get("COD_ERROR");
						resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), valCuenta, 0, "CCSC", "", 0, "");
						//SE INSERTA EN BITACORA TIPO OPERACION CSEN
						referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
						numRef = (Integer)referencia.get("REFERENCIA");
						codError=(String)referencia.get("COD_ERROR");
						resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), valCuenta, 0, "CSEN", "", 0, "");
						//SE INSERTA EN BITACORA TIPO OPERACION ESEN
						referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
						numRef = (Integer)referencia.get("REFERENCIA");
						codError=(String)referencia.get("COD_ERROR");
						resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), valCuenta, 0, "ESEN", "", 0, "");


						EIGlobal.mensajePorTrace("INFORMACION GUARDADA EN BITACORA CON RESULTADO --->" + resultBit, EIGlobal.NivelLog.INFO);
						//se inserta en bitacora transaccional

						EIGlobal.mensajePorTrace("SE REALIZA insertBitaTransac() --->", EIGlobal.NivelLog.INFO);
						BitaTransacBean bt = new BitaTransacBean();
						BitaHelper bh = new BitaHelperImpl(request, (BaseResource) request.getSession ().getAttribute ("session"),request.getSession());
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.REDIRECCION_SUPERNET_COMERCIOS);
						bt.setContrato(session.getContractNumber());
						BitaHandler.getInstance().insertBitaTransac(bt);
						EIGlobal.mensajePorTrace("BITACORIZACION EXITOSA --->", EIGlobal.NivelLog.INFO);
					}catch(Exception e){
						StackTraceElement[] lineaError;
						lineaError = e.getStackTrace();
						EIGlobal.mensajePorTrace("logout::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE, "
											   + "AL GENERAR LA BITACORA->" + e.getMessage()
											   + "<DATOS GENERALES>"
											   + "Usuario->" + session.getUserID8()
											   + "Contrato->" + session.getContractNumber()
											   + "Linea de truene->" + lineaError[0]
								               , EIGlobal.NivelLog.ERROR);
					}

					//SE GUARDA LA INFORMACION EN TABLA DE ORACLE DE SUPERNET COMERCIOS.
					exito = insertaDatosSnetComercios(sessionID, session.getUserID8(), session.getNombreUsuario(), session.getContractNumber(), IEnlace.CANAL_ENLACE, valCuenta, cuentas.toString(), esAdmin);
					if(exito){
						String urlSuperNetComercios = Global.URL_SUPERNET_COMERCIOS+"?jsessionid=" + sessionID;
						cierraDuplicidad(session.getUserID8());
						ses.removeAttribute("session");
						ses.setMaxInactiveInterval( 0 );
						ses.invalidate();
						request.setAttribute("urlSnetComer", urlSuperNetComercios);
						//evalTemplate(Global.URL_SUPERNET_COMERCIOS,request,response);
						evalTemplate("/jsp/redirSupCom.jsp",request,response);
						return;
					}else{
						EIGlobal.mensajePorTrace("No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
						request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
						evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
						return;
					}
				} catch (Exception e){
					EIGlobal.mensajePorTrace("No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
					request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
					evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
					return;
				}
			}else if(cuentasComercios!=null && cuentasComercios.size()==1){
				//preguntamos si solo existe una cuenata afiliada a supernet comercios.
				EIGlobal.mensajePorTrace("Se redireccionara directamente a supernet comercios hay una sola cuenta", EIGlobal.NivelLog.INFO);

				try {
					Set s=null;
					s = cuentasComercios.entrySet();
					Iterator iter = s.iterator();
					String cuenta = "";
					while (iter.hasNext()){
						Entry entry =(Entry) iter.next();
						cuenta =  (String)entry.getKey();
					}


					//se obtiene referencia para registrar en bitacora.
					try{
						EIGlobal.mensajePorTrace("INSERTANDO INFORMACION EN BITACORA." + session.getContractNumber(), EIGlobal.NivelLog.INFO);
						//SE INSERTA EN BITACORA TIPO OPERACION CCSC
						referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
						numRef = (Integer)referencia.get("REFERENCIA");
						codError=(String)referencia.get("COD_ERROR");
						resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), cuenta, 0, "CCSC", "", 0, "");
						//SE INSERTA EN BITACORA TIPO OPERACION CSEN
						referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
						numRef = (Integer)referencia.get("REFERENCIA");
						codError=(String)referencia.get("COD_ERROR");
						resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), cuenta, 0, "CSEN", "", 0, "");
						//SE INSERTA EN BITACORA TIPO OPERACION ESEN
						referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
						numRef = (Integer)referencia.get("REFERENCIA");
						codError=(String)referencia.get("COD_ERROR");
						resultBit = st.sbitacora(numRef, codError, session.getContractNumber(), session.getUserID8(), cuenta, 0, "ESEN", "", 0, "");
						EIGlobal.mensajePorTrace("INFORMACION GUARDADA EN BITACORA CON RESULTADO --->" + resultBit, EIGlobal.NivelLog.INFO);
						//se inserta en bitacora transaccional

						EIGlobal.mensajePorTrace("SE REALIZA insertBitaTransac() --->", EIGlobal.NivelLog.INFO);
						BitaTransacBean bt = new BitaTransacBean();
						BitaHelper bh = new BitaHelperImpl(request, (BaseResource) request.getSession ().getAttribute ("session"),request.getSession());
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.REDIRECCION_SUPERNET_COMERCIOS);
						bt.setContrato(session.getContractNumber());
						BitaHandler.getInstance().insertBitaTransac(bt);
						EIGlobal.mensajePorTrace("BITACORIZACION EXITOSA --->", EIGlobal.NivelLog.INFO);
					}catch(Exception e){
						StackTraceElement[] lineaError;
						lineaError = e.getStackTrace();
						EIGlobal.mensajePorTrace("logout::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE, "
											   + "AL GENERAR LA BITACORA->" + e.getMessage()
											   + "<DATOS GENERALES>"
											   + "Usuario->" + session.getUserID8()
											   + "Contrato->" + session.getContractNumber()
											   + "Linea de truene->" + lineaError[0]
								               , EIGlobal.NivelLog.ERROR);
					}

					//inserta informacion en base de datos de supernet comercios.
					exito = insertaDatosSnetComercios(sessionID, session.getUserID8(), session.getNombreUsuario(), session.getContractNumber(), IEnlace.CANAL_ENLACE, cuenta, cuenta, esAdmin);
					if(exito){
						String urlSuperNetComercios = Global.URL_SUPERNET_COMERCIOS+"?jsessionid=" + sessionID;
						cierraDuplicidad(session.getUserID8());
						ses.removeAttribute("session");
						ses.setMaxInactiveInterval( 0 );
						ses.invalidate();
						request.setAttribute("urlSnetComer", urlSuperNetComercios);
						//evalTemplate(Global.URL_SUPERNET_COMERCIOS,request,response);
						evalTemplate("/jsp/redirSupCom.jsp",request,response);
						return;
					}else{
						EIGlobal.mensajePorTrace("No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
						request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
						evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
						return;
					}
				} catch (Exception e){
					EIGlobal.mensajePorTrace("No se pudo generar el enlace con Supernet Comercios.", EIGlobal.NivelLog.ERROR);
					request.setAttribute ( "mensajeLigaSnet", "cuadroDialogo(\"No se pudo generar el enlace con Supernet Comercios.\",3);" );
					evalTemplate(IEnlace.ERROR_PAGE_LIGA_SNET,request,response);
					return;
				}
			}else if(cuentasComercios!=null && cuentasComercios.size()>1 ){
				EIGlobal.mensajePorTrace("Se tienen varias cuentas redireccion para seleccionar una", EIGlobal.NivelLog.INFO);
				String optionCuentas = "";
				Set s=null;
				s = cuentasComercios.entrySet();
				Iterator iter = s.iterator();
				while(iter.hasNext()){
					Entry entry =(Entry) iter.next();
					optionCuentas += "<OPTION Value = \"" + entry.getKey() + "\">" + entry.getKey() + " " +entry.getValue().toString() + " </OPTION>";
				}
				request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Seleccione Cuenta", "Supernet Comercios &gt; Selecci&oacute;n de Cuenta" ,"s60040h",request));
				request.setAttribute("ctasComercios", optionCuentas);
				ses.setAttribute("usrSnetComAdmin", usrSnetComAdmin);
				evalTemplate(IEnlace.MUESTRA_CUENTAS,request,response);
				return;
			}


		}
		//FIN JPH VSWF

		/*<VC proyecto="200710001" autor="JGR" fecha="03/05/2007" descripci�n="ADMON CONTRASE�A">*/
		//Si el origen es de pantallas de administracion de contrase�a, entonces finaliza la session
		String cerraradminPwd = request.getParameter("origen");
		if (cerraradminPwd !=null ) {
			ses.removeAttribute("session");
			ses.setMaxInactiveInterval( 0 );
			ses.invalidate();
			evalTemplate( IEnlace.LOGIN, request, response );
			return;
		}
		/*<VC TERMINA>*/
		String nombreFileNomina ="";
		boolean sessionValida = false;
		try{
		sessionValida = SesionValida( request, response );
		} catch(Exception e) {
			sessionValida = false;

		}
		boolLogin = false;
		try{
		request.setAttribute("MensajeLogin", "" );
		request.setAttribute( "Fecha", ObtenFecha() );
		}catch(Exception e) {}


		if( sessionValida ) {
			String nombreArchivoEmp = session.getArchivoEmpNom();

			if ( nombreArchivoEmp != null )	{
				try {
					//Si existe el archivo lo borra
					File archivo = new File( nombreArchivoEmp );
					if ( archivo.exists() )
						archivo.delete();
				} catch ( Exception e ) {
					EIGlobal.mensajePorTrace( "error en el objeto File(logout): " + e.getMessage(), EIGlobal.NivelLog.ERROR);
					//e.printStackTrace();
				}
			}
			session.setArchivoEmpNom( null );

			nombreFileNomina = session.getNameFileNomina();

			if ( nombreFileNomina != null )	{
				try {
					//Si existe el archivo lo borra
					File archivo = new File( nombreFileNomina );
					if ( archivo.exists() )
						archivo.delete();
				} catch ( Exception e ) {
					EIGlobal.mensajePorTrace("error en el objeto File(logout): " + e, EIGlobal.NivelLog.ERROR);
				}
			}

                int resDup = cierraDuplicidad(session.getUserID8());
                session.setSeguir(false);
                //session.guardaHash();
                //session.borraHash();
                ses.setAttribute("session", session);
                log( "logout.java::defaultAction() -> resDup: " + resDup );
                log( "logout.java::defaultAction() -> seguir: " + session.getSeguir() );



			session.setNameFileNomina( null );
			session.setstrMenu( null );
			session.setContractNumber( null );
			session.setUserID( null );
			session.setUserID8( null );
			session = null;

			//ses.setMaxInactiveInterval( 0 );
			log( "logout.java::invalidate:session" );
			ses.removeAttribute("session");
			ses.setMaxInactiveInterval( 0 );
			ses.invalidate();
			//session.invalidate();

			if(!cerrar) {
				evalTemplate( IEnlace.LOGIN, request, response );
			}
			else {
				evalTemplate( IEnlace.CERRAR, request, response );
			}
		}
	}
	
	/**
	 * @param request
	 * @param response
	 * @param operacionOK
	 * @throws IOException
	 * @throws ServletException
	 */
	public void logoutMicrositio( HttpServletRequest request, HttpServletResponse response, boolean operacionOK ) 
		throws IOException, ServletException {
		final String LOG_METODO = "logout - logoutMicrositio - ";
		int cierraDuplicidad = 0;
		EIGlobal.mensajePorTrace(LOG_METODO + "inicio", EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource)ses.getAttribute("session");
		if(session == null || !operacionOK) {
			ses.removeAttribute("session");
			ses.setMaxInactiveInterval( 0 );
			ses.invalidate();
			if(session != null) {
				cierraDuplicidad = cierraDuplicidad(session.getUserID8());	
			}
			EIGlobal.mensajePorTrace(LOG_METODO + "Se detruy� la sesi�n, saliendo del Micrositio - ", EIGlobal.NivelLog.INFO);
			evalTemplate( IEnlace.CERRAR, request, response );
			return;
		} else if (operacionOK){
			if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
				EIGlobal.mensajePorTrace(LOG_METODO + "Pistas de Auditoria Operacion Boton Salir",EIGlobal.NivelLog.DEBUG);
				try{
					BitaHelper bh = new BitaHelperImpl(request, session, ses);
					// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
					bt.setIdFlujo(BitaConstants.ME_OPERACION_MICROSITIO);
					bt.setEstatus("A");
					bt.setContrato(session.getContractNumber());
					bt.setUsr(session.getUserID8());
					String importe = (String)request.getParameter("importe");
					String cuentaCargo = (String)request.getParameter("cuentaCargo");
					String cuentaAbono = (String)request.getParameter("cuentaAbono");
					if(importe != null && !"".equals(importe.trim())){
						try{
						bt.setImporte(Double.valueOf(importe));
						}catch (NumberFormatException e) {
							bt.setImporte(0.0);
						}
					}
					if(cuentaCargo!=null){
						bt.setCctaOrig(cuentaCargo);
					}
					if(cuentaAbono!=null){
						bt.setCctaDest(cuentaAbono);
					}
					bt.setFechaAplicacion(new Date());
					bt.setServTransTux(logout.CLAVE_MICROSITIO);
					bt.setNumBit(BitaConstants.ME_BTN_SALIR);
					bt.setIdErr(logout.COD_EXITO_MICROSITIO);
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					if (session.getUserID8() != null) {
						bt.setCodCliente(session.getUserID8().trim());
					}
					if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
							&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
									.equals(BitaConstants.VALIDA)) {
							bt.setIdToken(session.getToken().getSerialNumber());
					}
					//Inserto en Bitacoras
					EIGlobal.mensajePorTrace(LOG_METODO + "bitacorizando: [" + BitaConstants.ME_BTN_SALIR  + "]", EIGlobal.NivelLog.INFO);
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(LOG_METODO + "Exception controlada [" + e + "]", EIGlobal.NivelLog.INFO);
				}
			}
			cierraDuplicidad = cierraDuplicidad(session.getUserID8());
			ses.removeAttribute("session");
			ses.setMaxInactiveInterval( 0 );
			ses.invalidate();
			EIGlobal.mensajePorTrace(LOG_METODO + "Saliendo y diriendose a url del cliente - cierraDuplicidad[" + cierraDuplicidad +"]", EIGlobal.NivelLog.DEBUG );
			evalTemplate( IEnlace.CERRAR, request, response );
			return;
		}
	}


}