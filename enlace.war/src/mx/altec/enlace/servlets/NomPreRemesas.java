package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.NomPreLM1A;
import mx.altec.enlace.beans.NomPreLM1B;
import mx.altec.enlace.beans.NomPreLM1C;
import mx.altec.enlace.beans.NomPreTarjeta;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.NomPreRemesaAction;
import mx.altec.enlace.utilerias.IEnlace;
import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class NomPreRemesas extends BaseServlet {

	private static final long serialVersionUID = 1L;

	private static final String RECEPCION = "inirecep";
	private static final String RECEPCION_CONSULTA = "consultaRecep";
	private static final String RECEPCION_RECHAZAR = "rechazar";
	private static final String RECEPCION_CONFIRMAR_TODO = "confirmatodo";
	private static final String RECEPCION_DETALLE = "recepcion";
	private static final String CONSULTA = "iniconsul";
	private static final String CONSULTA_GENERAL = "consulta";
	private static final String CONSULTA_DETALLE = "detalle";
	private static final String CONFIRMAR_RECEPCION = "confirmar";
	private static final String CONSULTA_DETALLE_SELECCION = "detallesel";
	private static final String SER_ARCH_CONSULTA_REM = "archconrem";
	private static final String SER_ARCH_DETALLE_REM = "archdetrem";
	private static final String SER_ARCH_RECEPCION_REM = "archrecrem";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (!SesionValida(request, response)) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
			return;
		}

		BaseResource session = obtenerBaseResource(request);

		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		String operacion = request.getParameter("operacion");

		//CGH Valida servicio de nomina
		if (!validaServicioNominaRedireccion(request, response)) {

			if (RECEPCION.equals(operacion) || RECEPCION_CONSULTA.equals(operacion) ||
					RECEPCION_DETALLE.equals(operacion) || CONFIRMAR_RECEPCION.equals(operacion) ||
					CONSULTA_DETALLE_SELECCION.equals(operacion)) {

				generaEncabezadoRecepcion(request);

			} else {

				generaEncabezadoConsulta(request);
			}

			return;
		}

		if (RECEPCION_CONSULTA.equals(operacion) ||
			RECEPCION_DETALLE.equals(operacion) ||
			CONSULTA_DETALLE_SELECCION.equals(operacion) ||
			CONFIRMAR_RECEPCION.equals(operacion)) {

			if (!(RECEPCION_CONSULTA.equals(operacion) && request.getParameter("inicial") != null)) {
				if (request.getSession().getAttribute(SER_ARCH_RECEPCION_REM) == null) {
					operacion = RECEPCION;
				}
			}
		}

		if (CONSULTA_GENERAL.equals(operacion)) {
			if (request.getParameter("inicial") == null && request.getSession().getAttribute(SER_ARCH_CONSULTA_REM) == null) {
				operacion = CONSULTA;
			}
		}

		if (CONSULTA_DETALLE.equals(operacion)) {
			if (request.getParameter("inicial") == null && request.getSession().getAttribute(SER_ARCH_DETALLE_REM) == null) {
				operacion = CONSULTA;
			}
		}

		if (CONSULTA.equals(operacion)) {

			generaEncabezadoConsulta(request);

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_REMESAS_CONSULTA_ENTRA);
			bitacoriza(request, null, null, true, bt);

			request.getRequestDispatcher(IEnlace.NOM_PRE_CONSULTA_REMESA)
					.include(request, response);

		} else if (CONSULTA_GENERAL.equals(operacion)) {

			generaEncabezadoConsulta(request);
			consultar(request, response);

		} else if (CONSULTA_DETALLE.equals(operacion)) {

			generaEncabezadoConsulta(request);
			detalle(request, response);

		} else if (RECEPCION.equals(operacion)) {

			generaEncabezadoRecepcion(request);

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_REMESAS_RECEPCION_ENTRA);
			bitacoriza(request, null, null, true, bt);

			request.getRequestDispatcher(IEnlace.NOM_PRE_CONFIRMA_REMESA)
					.include(request, response);

		} else if (RECEPCION_CONSULTA.equals(operacion)) {

			generaEncabezadoRecepcion(request);
			consultaRecepcion(request, response);

		} else if (RECEPCION_RECHAZAR.equals(operacion)) {

			generaEncabezadoRecepcion(request);
			rechazar(request, response);

		} else if (RECEPCION_CONFIRMAR_TODO.equals(operacion)) {

			generaEncabezadoRecepcion(request);
			confirmarTodo(request, response);

		} else if (RECEPCION_DETALLE.equals(operacion)) {

			generaEncabezadoRecepcion(request);
			recepcion(request, response);

		} else if (CONSULTA_DETALLE_SELECCION.equals(operacion)) {

			detalleSeleccion(request, response);

		} else if (CONFIRMAR_RECEPCION.equals(operacion)) {

			generaEncabezadoRecepcion(request);
			confirmarRecepcion(request, response);
		}
	}

	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void generaEncabezadoRecepcion(HttpServletRequest request) {
		generaEncabezado(request, "Recepci&oacute;n de remesas", "Servicios > Tarjeta de Pagos Santander  > Remesas > Recepci&oacute;n");//YHG NPRE
	}

	private void generaEncabezadoConsulta(HttpServletRequest request) {
		generaEncabezado(request, "Consulta de remesas", "Servicios > Tarjeta de Pagos Santander > Remesas > Consultas");//YHG NPRE
	}

	private void generaEncabezado(HttpServletRequest request, String titulo, String ruta) {
		request.setAttribute("Encabezado",	CreaEncabezado(titulo, ruta, "s37140h", request));
	}

	private BaseResource obtenerBaseResource(HttpServletRequest request) {

		HttpSession session = request.getSession();

		return (BaseResource) session.getAttribute("session");
	}

	private void confirmarRecepcion(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String mensaje = "";
		HttpSession session = request.getSession();

		String noRemesa = request.getParameter("noRemesa");
		String noContrato = obtenerBaseResource(request).getContractNumber();

		String archivo = session.getAttribute(SER_ARCH_RECEPCION_REM).toString();
		NomPreLM1B lm1b = objectFromFile(archivo);

		List<String> seleccionados = (List<String>) session.getAttribute("aceptados");

		List<NomPreTarjeta> aceptados = obtenerTarjetaRemesaPorEstado(lm1b.getDetalle(), seleccionados, "A");
		List<NomPreTarjeta> rechazados = obtenerTarjetaRemesaPorEstado(lm1b.getDetalle(), seleccionados, "R");

		NomPreLM1C lm1c = new NomPreRemesaAction().confirmarDetalleRemesa(noContrato, noRemesa, aceptados, rechazados);

		String codError = lm1c == null ? " " : lm1c.getCodigoOperacion();

		BitaTransacBean bt = new BitaTransacBean();
		bt.setNumBit(BitaConstants.ES_NP_REMESAS_RECEPCION_CONFIRMA);
		bitacoriza(request, "LM1C", codError, bt);

		request.setAttribute("titulomensaje", "Recepci&oacute;n de Remesas");

		if (lm1c != null && lm1c.isCodExito()) {

			mensaje = "Se ha realizado correctamente la recepci&oacute;n de la remesa " + noRemesa;
			try
			{
				int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
				if(codError==null || codError.trim().equals("")){
					codError=" ";
				}
				String bitacora = llamada_bitacora(referencia,codError,noContrato,obtenerBaseResource(request).getUserID8()," ",0.0, ES_NP_REMESAS_RECEPCION_OPER," ",0," ");
			}catch(Exception e){

				logError("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago");
			}

		} else {

			mensaje = "Ha ocurrido un error al realizar la recepci&oacute;n de la remesa " + noRemesa;
		}

		request.setAttribute("mensajecomun", mensaje);
		request.setAttribute("mensajeregreso", "NomPreRemesas?operacion=inirecep&"+BitaConstants.FLUJO+"="+BitaConstants.ES_NP_REMESAS_RECEPCION);

		session.removeAttribute(SER_ARCH_RECEPCION_REM);
		session.removeAttribute("aceptados");

		request.getRequestDispatcher(
				IEnlace.NOM_PRE_COMUN_MENSAJE).include(
				request, response);
	}

	private List<NomPreTarjeta> obtenerTarjetaRemesaPorEstado(List<NomPreTarjeta> detalle, List<String> seleccionados, String estado){

		List<NomPreTarjeta> resultado = null;

		if (detalle != null && detalle.size() > 0) {

			resultado =  new ArrayList<NomPreTarjeta>();

			for (NomPreTarjeta tarjeta : detalle) {

				boolean contiene = seleccionados.contains(tarjeta.getNoTarjeta());

				if ("A".equals(estado) && contiene) {
					resultado.add(tarjeta);
				} else if ("R".equals(estado) && !contiene) {
					resultado.add(tarjeta);
				}
			}
		}

		return resultado;
	}

	private void detalleSeleccion(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		String archivo = session.getAttribute(SER_ARCH_RECEPCION_REM).toString();
		NomPreLM1B lm1b = objectFromFile(archivo);

		List<String> seleccionados = (List<String>) session.getAttribute("aceptados");
		String estado = request.getParameter("estado");

		List<NomPreTarjeta> detalle = obtenerTarjetaRemesaPorEstado(lm1b.getDetalle(), seleccionados, estado);

		request.setAttribute("detalle", detalle);
		request.setAttribute("remesa", lm1b.getRemesa());

		request.getRequestDispatcher(
				IEnlace.NOM_PRE_CONFIRMA_DETALLE_SELECCION).include(
				request, response);
	}

	private void recepcion(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String[] tarjetas = request.getParameterValues("tarjetas");
		String noRemesa = request.getParameter("noRemesa");

		if (tarjetas != null && tarjetas.length > 0) {

			logInfo("Detallando remesa :" + noRemesa);
			logInfo("Tarjetas: " + Arrays.toString(tarjetas));

			HttpSession session = request.getSession();

			String archivo = session.getAttribute(SER_ARCH_RECEPCION_REM).toString();
			NomPreLM1B lm1b = objectFromFile(archivo);

			session.setAttribute("aceptados", (List<String>) Arrays.asList(tarjetas));

			request.setAttribute("noaceptados", tarjetas.length);
			request.setAttribute("norechazado", lm1b.getDetalle().size() - tarjetas.length);
			request.setAttribute("lm1b", lm1b);

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_REMESAS_RECEPCION_RESUMEN);
			bitacoriza(request, null, null, bt);

			request.getRequestDispatcher(
					IEnlace.NOM_PRE_CONFIRMA_REMESA_DETALLE).include(
					request, response);
		}
	}

	private void rechazar(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String noRemesa = request.getParameter("noRemesa");
		String noContrato = obtenerBaseResource(request).getContractNumber();
		String mensaje = null;
		String codErrBit=" ";
		if (noRemesa != null && noRemesa.length() > 0) {

			logInfo("Rechazando remesa :" + noRemesa);

			NomPreLM1C lm1c = new NomPreRemesaAction().rechazarRemesa(noContrato, noRemesa); //////////////////////////////////

			if(lm1c!=null && lm1c.getCodigoOperacion()!=null && !lm1c.getCodigoOperacion().trim().equals("")){
				codErrBit=lm1c.getCodigoOperacion();
			}
			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_REMESAS_RECEPCION_RECHAZA);
			bitacoriza(request, "LM1C", codErrBit, bt);

			request.setAttribute("titulomensaje", "Recepci&oacute;n de Remesas");

			if (lm1c != null && lm1c.isCodExito()) {

				mensaje = "Se ha realizado correctamente el rechazo de la remesa " + noRemesa;
				try
				{
					int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
					String bitacora = llamada_bitacora(referencia,codErrBit,noContrato,obtenerBaseResource(request).getUserID8()," ",0.0, ES_NP_REMESAS_RECEPCION_RECHAZA_OPER," ",0," ");
				}catch(Exception e){

					logError("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago");
				}

			} else {

				mensaje = "Ha ocurrido un error al realizar el rechazo de la remesa " + noRemesa;
			}

			request.setAttribute("mensajecomun", mensaje);
			request.setAttribute("mensajeregreso", "NomPreRemesas?operacion=inirecep&"+BitaConstants.FLUJO+"="+BitaConstants.ES_NP_REMESAS_RECEPCION);

			request.getRequestDispatcher(
					IEnlace.NOM_PRE_COMUN_MENSAJE).include(
					request, response);
		}
	}

	private void confirmarTodo(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String noRemesa = request.getParameter("noRemesa");
		String noContrato = obtenerBaseResource(request).getContractNumber();
		String mensaje = null;
		String codErrBit=" ";

		if (noRemesa != null && noRemesa.length() > 0) {

			logInfo("Confirmando remesa :" + noRemesa);

			NomPreLM1C lm1c = new NomPreRemesaAction().confirmarRemesa(noContrato, noRemesa); //////////////////////////////////
			if(lm1c!=null && lm1c.getCodigoOperacion()!=null && !lm1c.getCodigoOperacion().trim().equals("")){
				codErrBit=lm1c.getCodigoOperacion();
			}

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_REMESAS_RECEPCION_CONFIRMA);
			bitacoriza(request, "LM1C", codErrBit, bt);


			request.setAttribute("titulomensaje", "Recepci&oacute;n de Remesas");

			if (lm1c != null && lm1c.isCodExito()) {

				mensaje = "Se ha realizado correctamente la confirmaci&oacute;n de la remesa " + noRemesa;
				try
				{
					int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));


					String bitacora = llamada_bitacora(referencia,codErrBit,noContrato,obtenerBaseResource(request).getUserID8()," ",0.0, ES_NP_REMESAS_RECEPCION_OPER," ",0," ");
				}catch(Exception e){
					logError("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago");
				}

			} else {

				mensaje = "Ha ocurrido un error al realizar la confirmaci&oacute;n de la remesa " + noRemesa;
			}

			request.setAttribute("mensajecomun", mensaje);
			request.setAttribute("mensajeregreso", "NomPreRemesas?operacion=inirecep&"+BitaConstants.FLUJO+"="+BitaConstants.ES_NP_REMESAS_RECEPCION);

			request.getRequestDispatcher(
					IEnlace.NOM_PRE_COMUN_MENSAJE).include(
					request, response);
		}
	}

	private void consultaRecepcion(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String noRemesa = request.getParameter("noRemesa");
		String salida = request.getParameter("salida");
		final String noContrato = obtenerBaseResource(request).getContractNumber();
		String CodErr="";
		String include = null;

		HttpSession session = request.getSession();

		NomPreLM1B lm1b = null;

		if (request.getParameter("inicial") != null && noRemesa != null && noRemesa.length() > 0) {

			lm1b = new NomPreRemesaAction().buscarTarjetas(noContrato, noRemesa);

			String estadoRemesa = null;

			if (lm1b != null && lm1b.getRemesa() != null) {

				logInfo("Estado remesa: " + lm1b.getRemesa().getEstadoRemesa());

				estadoRemesa = lm1b.getRemesa().getEstadoRemesa().toUpperCase().trim();
			}

			logInfo("Estado remesa: " + estadoRemesa + " " + (estadoRemesa != null ? estadoRemesa.length() : ""));

			if (lm1b != null && lm1b.getRemesa() != null && "ENVIADA".equals(estadoRemesa)) {

				lm1b.getRemesa().setNoRemesa(noRemesa);

				String archivo = saveObjectToFile(lm1b, session);

				session.setAttribute(SER_ARCH_RECEPCION_REM, archivo);

				BitaTransacBean bt = new BitaTransacBean();
				bt.setContrato(noContrato);
				bt.setNumBit(BitaConstants.ES_NP_REMESAS_RECEPCION_CONSULTA);
				if(lm1b!=null && lm1b.getCodigoOperacion()!=null){
					CodErr=lm1b.getCodigoOperacion();
				}

				bitacoriza(request, "LM1B", CodErr, bt);
				//TODO BITACORA TCT "ESRR"

			} else {

				lm1b = null;
			}

		} else {

			String archivo = session.getAttribute(SER_ARCH_RECEPCION_REM).toString();

			lm1b = objectFromFile(archivo);
		}

		request.setAttribute("lm1b", lm1b);

		if ("excel".equals(salida)) {

			configurarSalidaReporte(response, "tarjetasRemesa");
			include = IEnlace.NOM_PRE_EXP_TARJETAS;

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_REMESAS_RECEPCION_CONSULTA);
			bt.setNombreArchivo("tarjetasRemesa.xls");
			if(lm1b!=null && lm1b.getCodigoOperacion()!=null){
				CodErr=lm1b.getCodigoOperacion();
			}
			bitacoriza(request, "LM1B", CodErr, bt);

		} else {

			include = IEnlace.NOM_PRE_CONFIRMA_REMESA_RESULTADO;
		}

		request.getRequestDispatcher(include).include(request, response);
	}

	private void detalle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		final String[] estados = request.getParameterValues("chkEstado");
		final String noTarjeta = request.getParameter("noTarjeta");

		String noRemesa = request.getParameter("noRemesa");
		String salida = request.getParameter("salida");
		final String noContrato = obtenerBaseResource(request).getContractNumber();
		String codErr="";

		if (noRemesa != null && noRemesa.length() > 0) {

			logInfo("Listando detalle de remesa " + noContrato + " - " + noRemesa);

			HttpSession session = request.getSession();
			NomPreLM1B lm1b = null;
			String include = null;
			String archivo = null;

			if (request.getParameter("inicial") != null) {

				if ("true".equals(request.getAttribute("filtro"))) {

					lm1b = new NomPreRemesaAction().buscaTarjetaPorEstado(noContrato, noRemesa, noTarjeta, estados);

				} else {

					lm1b = new NomPreRemesaAction().buscarTarjetas(noContrato, noRemesa);
				}

				archivo = saveObjectToFile(lm1b, session);

				session.setAttribute(SER_ARCH_DETALLE_REM, archivo);

				if (lm1b != null) {

					BitaTransacBean bt = new BitaTransacBean();
					bt.setNumBit(BitaConstants.ES_NP_REMESAS_CONSULTA_CONSULTA);
					bt.setContrato(noContrato);
					if(lm1b!=null && lm1b.getCodigoOperacion()!=null){
						codErr=lm1b.getCodigoOperacion();
					}
					bitacoriza(request, "LM1B", codErr, bt);
				}

				/*try
				{
					int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
					String bitacora = llamada_bitacora(referencia,lm1b.getCodigoOperacion(),noContrato,obtenerBaseResource(request).getUserID()," ",0.0,NomPreUtil.ES_NP_REMESAS_CONSULTA_OPER," ",0," ");
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
				}*/
			} else {

				archivo = session.getAttribute(SER_ARCH_DETALLE_REM).toString();

				lm1b = objectFromFile(archivo);
			}

			request.setAttribute("lm1b", lm1b);

			if ("excel".equals(salida)) {

				configurarSalidaReporte(response, "detalleRemesa");
				include = IEnlace.NOM_PRE_EXP_TARJETAS;

				BitaTransacBean bt = new BitaTransacBean();
				bt.setNumBit(BitaConstants.ES_NP_REMESAS_CONSULTA_CONSULTA);
				bt.setNombreArchivo("detalleRemesa.xls");
				if(lm1b!=null && lm1b.getCodigoOperacion()!=null){
					codErr=lm1b.getCodigoOperacion();
				}
				bitacoriza(request, "LM1B", codErr, bt);

			} else {

				include = IEnlace.NOM_PRE_CONSULTA_REMESA_DETALLE;
			}

			request.getRequestDispatcher(include).include(request, response);
		}
	}

	private void consultar(final HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String noRemesa = request.getParameter("noRemesa");
		String noTarjeta = request.getParameter("noTarjeta");
		String salida = request.getParameter("salida");

		NomPreRemesaAction action = new NomPreRemesaAction();
		HttpSession session = request.getSession();

		String noContrato = obtenerBaseResource(request).getContractNumber();;
		String include = null;
		String archivo = null;
		String codErrBit=" ";

		if ((noRemesa == null || "".equals(noRemesa))
				&& (noTarjeta == null || "".equals(noTarjeta))) {

			NomPreLM1A lm1a = null;

			if (request.getParameter("inicial") != null) {

				lm1a = action.consultarRemesas(noContrato);

				archivo = saveObjectToFile(lm1a, session);

				session.setAttribute(SER_ARCH_CONSULTA_REM, archivo);

				BitaTransacBean bt = new BitaTransacBean();
				bt.setContrato(noContrato);
				bt.setNumBit(BitaConstants.ES_NP_REMESAS_CONSULTA_CONSULTA);
				if(lm1a!=null && lm1a.getCodigoOperacion()!=null && !lm1a.getCodigoOperacion().trim().equals("")){
					codErrBit=lm1a.getCodigoOperacion();
				}
				bitacoriza(request, "LM1A", codErrBit, bt);

				try
				{
					int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
					String bitacora = llamada_bitacora(referencia,codErrBit,noContrato,obtenerBaseResource(request).getUserID8()," ",0.0, ES_NP_REMESAS_CONSULTA_OPER," ",0," ");
				}catch(Exception e){

					logError("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ");
				}

			} else {

				archivo = session.getAttribute(SER_ARCH_CONSULTA_REM).toString();

				lm1a = objectFromFile(archivo);
			}

			logInfo("Listando remesas para contrato " + noContrato);

			request.setAttribute("lm1a", lm1a);

			if ("excel".equals(salida)) {

				configurarSalidaReporte(response, "remesas");
				include = IEnlace.NOM_PRE_EXP_REMESAS;

				if (lm1a != null) {

					BitaTransacBean bt = new BitaTransacBean();
					bt.setNumBit(BitaConstants.ES_NP_REMESAS_CONSULTA_CONSULTA);
					bt.setNombreArchivo("remesas.xls");
					if(lm1a!=null && lm1a.getCodigoOperacion()!=null && !lm1a.getCodigoOperacion().trim().equals("")){
						codErrBit=lm1a.getCodigoOperacion();
					}
					bitacoriza(request, "LM1A",codErrBit, bt);
				}

			} else {

				include = IEnlace.NOM_PRE_CONSULTA_REMESA_LISTA;
			}

			request.getRequestDispatcher(include).include(request,response);

		} else {

			request.setAttribute("filtro", "true");
			detalle(request, response);
		}
	}

	private void configurarSalidaReporte(HttpServletResponse response, String nombreArchivo) throws IOException {

		response.setContentType("application/vnd.ms-excel");

		response.setHeader("Content-disposition", "attachment; filename="
				+ nombreArchivo + ".xls");
	}
}