/*
 * AutorizaAMancetalleSA.java
 *
 * Created on 16 de septiembre de 2012
 *
 *
 */

package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosMancSA;
import mx.altec.enlace.bo.RespManc;
import mx.altec.enlace.bo.RespMancSA;
import mx.altec.enlace.bo.TransferenciaInterbancariaBUS;
import mx.altec.enlace.dao.ConfigMancDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

//VSWF


/**
 *
 * @author
 * @version
 */
public class AutorizaAMancDetalleSA extends BaseServlet {

	/**
	 * Initializes the servlet.
     */
    public void init (ServletConfig config) throws ServletException {
	super.init (config);

    }

    /** Destroys the servlet.
     */
    public void destroy () {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    	String next = request.getParameter ( "Next" );
    	HttpSession sessionHttp = request.getSession();
    	if (SesionValida (request, response)) {
		    BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
		    Integer Autoriza = null;//(Integer) request.getSession ().getAttribute ("Autoriza");

		    EIGlobal.mensajePorTrace("같같같같같같같� Autoriza sesion ->" + request.getSession ().getAttribute ("Autoriza"), EIGlobal.NivelLog.DEBUG);

			////System.out.println("  TC A U T O R I Z A:" + Autoriza);
			/******************************************Inicia validacion OTP**************************************/
				String valida = request.getParameter ( "valida" );

				if ( next==null && validaPeticion( request, response,session,request.getSession (),valida)) {
					  EIGlobal.mensajePorTrace("\n\n ENTRO A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.DEBUG);
				}
				/******************************************Termina validacion OTP**************************************/
				else {

					int aut = 2;
					boolean valBitacora = true;

					if( next==null && valida==null) {
						String []datos=request.getParameterValues("indiceArchivo");
						EIGlobal.mensajePorTrace( "-----------datos- [" + datos + "]", EIGlobal.NivelLog.INFO);
						sessionHttp.setAttribute("FolioParam",datos);
						java.util.ArrayList notifica = new java.util.ArrayList ();
						notifica.add("");
						request.getSession().setAttribute ("datosNotificacion",notifica);

						boolean solVal=ValidaOTP.solicitaValidacion(session.getContractNumber(),IEnlace.MANCOMUNIDAD);

						if( session.getValidarToken() &&
							session.getFacultad(session.FAC_VAL_OTP) &&
							session.getToken().getStatus() == 1 &&
							solVal ) {
							//VSWF-HGG -- Bandera para guardar el token en la bitacora
							request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
							ValidaOTP.guardaParametrosEnSession(request);

							EIGlobal.mensajePorTrace("TOKEN MANCOMUNIDAD", EIGlobal.NivelLog.INFO);
							request.getSession().removeAttribute("mensajeSession");

							EIGlobal.mensajePorTrace("같같같같같같같� TipoOp ->" + request.getParameter("TipoOp"), EIGlobal.NivelLog.DEBUG);
						    String nominasAttr = (String)request.getSession().getAttribute("nominasAttr");
						    EIGlobal.mensajePorTrace("같같� nominasAttr 같같� [" + nominasAttr + "]", EIGlobal.NivelLog.DEBUG);

							if (request.getParameter("TipoOp")!=null && request.getParameter("TipoOp").toString().trim().equals("1")) {
								ValidaOTP.mensajeOTP(request, "AutorizaManc_A_SA");
							} else  if (request.getParameter("TipoOp")!=null && request.getParameter("TipoOp").toString().trim().equals("2")
									&& nominasAttr != null && (nominasAttr.equals("NOMI") || nominasAttr.equals("NOIT"))) {
								ValidaOTP.mensajeOTP(request, "AutorizaManc_C_SA_NOMI");
							} else {
								ValidaOTP.mensajeOTP(request, "AutorizaManc_C_SA");
							}

							ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
						}else {
							ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
							valida="1";
							valBitacora = false;

						}
					}

				//retoma el flujo
					if( (next!=null ||(valida!=null && valida.equals("1")))) {
						if (valBitacora) {
							try{
								EIGlobal.mensajePorTrace( "*************************Entro cOdigo bitacorizaciOn--->", EIGlobal.NivelLog.INFO);

								HttpSession sessionBit = request.getSession(false);
								Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
								Map tmpAtributos = (HashMap) sessionBit.getAttribute("atributosBitacora");

								String registros[] = (String []) sessionHttp.getAttribute("FolioParam");
								double importeDouble = 0;

								if (tmpParametros!=null && tmpAtributos!=null) {
									Enumeration enumer = request.getParameterNames();

									while(enumer.hasMoreElements()) {
										String name = (String)enumer.nextElement();
									}

									enumer = request.getAttributeNames();

									while(enumer.hasMoreElements()){
										String name = (String)enumer.nextElement();
									}
								}

								short suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
								int numeroReferencia  = obten_referencia_operacion(suc_opera);
								String claveOperacion = BitaConstants.CTK_OPERACIONES_MANC;;
								String concepto = BitaConstants.CTK_CONCEPTO_OPERACIONES_MANC;
								String token = "0";
								if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
								{token = request.getParameter("token");}

								sessionBit.removeAttribute("parametrosBitacora");
								sessionBit.removeAttribute("atributosBitacora");

								EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);

								String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

							} catch(Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						}

						try {
							EIGlobal.mensajePorTrace("같같같같같같같� Autoriza param ->" + request.getParameter("Autoriza"), EIGlobal.NivelLog.DEBUG);
							aut = Integer.parseInt (request.getParameter ("Autoriza"));
						} catch (NumberFormatException ex) {}

						if (Autoriza == null) {
							Autoriza = new Integer (aut);
						}

						request.getSession().setAttribute ("Autoriza", Autoriza);
						String operacion = "";
						if (Autoriza.intValue () == 1) {
							operacion = "Autorizaci&oacute;n";
						} else {
							operacion = "Cancelaci&oacute;n";
						}

						request.getSession ().setAttribute ("Next", next);

						request.getSession ().setAttribute ("MenuPrincipal", session.getStrMenu ());
						request.getSession ().setAttribute ("newMenu", session.getFuncionesDeMenu ());
						request.getSession ().setAttribute("Encabezado", CreaEncabezado(""+ operacion +" de Operaciones Mancomunadas",
						"Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y "+ operacion,
						"s26000h", request));
						request.getSession ().setAttribute ("operacion", operacion);
						autorizaOperaciones (request, response);
						EIGlobal.mensajePorTrace("\n\n\n FIN de RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.DEBUG);
					}
				}
			}

}


    /** Metodo para autorizar las operaciones seleccionadas en la consulta de mancomunidad
     * @param request Objeto request del servlet
     * @param response Objeto response del servlet
     */
    private void autorizaOperaciones (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {

    	BaseResource session = (BaseResource) request.getSession().getAttribute("session");
    	EIGlobal.mensajePorTrace("같같같같같같같� FolioParam ->" + request.getSession().getAttribute("FolioParam"), EIGlobal.NivelLog.DEBUG);
    	EIGlobal.mensajePorTrace("같같같같같같같� ResultadosMancDetalle ->" + request.getSession().getAttribute("ResultadosMancDetalle"), EIGlobal.NivelLog.DEBUG);
    	ArrayList listResDetalle = (ArrayList) request.getSession().getAttribute("ResultadosMancDetalle");
    	String[] indices=(String[])request.getSession().getAttribute("FolioParam");
    	request.getSession().removeAttribute("FolioParam");

    	ArrayList<RespMancSA> porProcesar = new ArrayList<RespMancSA>();

    	int aut = 2;
	    try {
	    	aut = ((Integer) request.getSession ().getAttribute ("Autoriza")).intValue ();
	    } catch (NumberFormatException ex) {}

	    String nominasAttr = (String)request.getSession().getAttribute("nominasAttr");
	    EIGlobal.mensajePorTrace("같같� nominasAttr 같같� [" + nominasAttr + "]", EIGlobal.NivelLog.DEBUG);
	    if (aut == 2 && nominasAttr != null && (nominasAttr.equals("NOMI") || nominasAttr.equals("NOIT"))){ // Es cancelaci�n de Nomina
	    	indices = new String[] {"0"}; // Forza a entrar en ciclo para enviar cancelaci�n completa
	    	EIGlobal.mensajePorTrace("같같� [Es Cancelacion de Nomina] 같같�", EIGlobal.NivelLog.DEBUG);
	    }
	    EIGlobal.mensajePorTrace("같같� indices.length [" + indices.length + "] 같같�", EIGlobal.NivelLog.DEBUG);
	    for (int aux=0; aux<indices.length; aux++) {
    		EIGlobal.mensajePorTrace("같같� indices [" + indices[aux] + "] 같같�", EIGlobal.NivelLog.DEBUG);
    		DatosMancSA registro = (DatosMancSA) listResDetalle.get(Integer.parseInt(indices[aux]));
    		String trama = "";
    		EIGlobal.mensajePorTrace("Trama Autorizacion ->" + trama, EIGlobal.NivelLog.DEBUG);
    		EIGlobal.mensajePorTrace("같같� registro [" + registro.toString() + "] 같같�", EIGlobal.NivelLog.DEBUG);
    		ServicioTux servicio = new ServicioTux();
    		Hashtable hs = null;
    		if(aut == 1) {
	    		try {
	    			trama = registro.getTramaAutorizacion(session.getUserID8(), session.getContractNumber(), session.getUserProfile());
	    			hs = servicio.web_red(trama);
	    		} catch(Exception e) {
	    			EIGlobal.mensajePorTrace("Error al ejecutar WEB_RED: " + trama, EIGlobal.NivelLog.ERROR);
	    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
	    		}
    		}
    		else {
    			try {
    				trama = registro.getTramaCancelacion(session.getUserID8(), session.getContractNumber(), session.getUserProfile());
	    			hs = servicio.web_red(trama);
	    		} catch(Exception e) {
	    			EIGlobal.mensajePorTrace("Error al ejecutar WEB_RED: " + trama, EIGlobal.NivelLog.ERROR);
	    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
	    		}
    		}
	        String respuesta = (String) hs.get ("BUFFER");
	        String codError = (String) hs.get ("COD_ERROR");
	        String estatus = "";
	        String folio_auto = "";
	        String fecha_auto = "";

		    EIGlobal.mensajePorTrace("AutorizaAMancomunidad - codError: [" +
					codError + "]",EIGlobal.NivelLog.DEBUG);

		    if (respuesta == null || respuesta.equals ("")) {
				estatus = "Error en el servicio";
				folio_auto = "&nbsp;";
				fecha_auto = "&nbsp;";
		    }
		    else {
		    	ConfigMancDAO dao = new ConfigMancDAO();
	    		try {
					DatosMancSA dm = (DatosMancSA) request.getSession().getAttribute("datosArchivo");
	    			dao.invalidaUsuarios(dm.getFolio_archivo());
	    		}catch(Exception e) {
	    		    EIGlobal.mensajePorTrace("Error al invalidar usuarios.",EIGlobal.NivelLog.ERROR);
	    		}

		    	if (registro.getTipo_operacion ().equals ("TRAN")) {

		    		if (respuesta.substring (0, 2).equals ("OK")) {

						estatus = "Operaci&oacute;n Ejecutada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.equals("MANC0022")) {

						estatus = "Operaci&oacute;n Verificada";
						//folio_auto = "&nbsp;";//respuesta.substring (8);
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.equals("MANC0023")) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						//folio_auto = "&nbsp;";//respuesta.substring (8);
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.startsWith("MANC")) {
						estatus = (codError.length () > 8) ? codError.substring (8) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (respuesta.length () > 16) ? respuesta.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
				}
				else if (registro.getTipo_operacion ().equals ("DIBT")) {

				    if (respuesta.substring (0, 2).equals ("OK")){

				    	estatus = "Operaci&oacute;n Ejecutada";
					 	folio_auto = respuesta.substring (8, 16);

					    EIGlobal.mensajePorTrace("\n AUTORIZACION MANCOMUNIDAD  fecha_auto-->"+fecha_auto, EIGlobal.NivelLog.DEBUG);
					    EIGlobal.mensajePorTrace("\n AUTORIZACION MANCOMUNIDAD  folio_auto-->"+folio_auto, EIGlobal.NivelLog.DEBUG);
					    EIGlobal.mensajePorTrace("\n AUTORIZACION MANCOMUNIDAD  estatus-->"+estatus, EIGlobal.NivelLog.DEBUG);

				          	if( aut == 1) {
						     EIGlobal.mensajePorTrace("\n AUTORIZACION MANCOMUNIDAD SPEI AutorizaAMancDetalleSA-->"+folio_auto, EIGlobal.NivelLog.DEBUG);
							TransferenciaInterbancariaBUS TransInterbancariaBUS=TransferenciaInterbancariaBUS.getInstance();
							TransInterbancariaBUS.registraTransferencia(trama, respuesta);
					              }

				            }
					else if(codError.equals("MANC0022")) {

						estatus = "Operaci&oacute;n Verificada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.equals("MANC0023")) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.startsWith("MANC")) {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (respuesta.length () > 16) ? respuesta.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}

				}
				else if (registro.getTipo_operacion().equals("PROG")) {

					if (respuesta.substring (0, 2).equals("OK")) {

						estatus = "Operaci&oacute;n Ejecutada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.equals("MANC0022")) {

						estatus = "Operaci&oacute;n Verificada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.equals("MANC0023")) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.startsWith("MANC")) {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (respuesta.length () > 16) ? respuesta.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
				}
				else if (registro.getTipo_operacion().equals("PAGT")) {

					if (respuesta.substring (0, 2).equals("OK")) {

						estatus = "Operaci&oacute;n Ejecutada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.equals("MANC0022")) {

						estatus = "Operaci&oacute;n Verificada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.equals("MANC0023")) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						folio_auto = respuesta.substring (8, 16);
					}
					else if(codError.startsWith("MANC")) {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (respuesta.length () > 16) ? respuesta.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
				}
				else if (aut == 2 && nominasAttr != null && (nominasAttr.equals("NOMI") || nominasAttr.equals("NOIT"))) {
					if (respuesta.substring (0, 2).equals("OK")) {
						estatus = "Operaci&oacute;n Ejecutada";
						folio_auto = respuesta.substring (8, 16);
					} else {
						estatus = (respuesta.length () > 16) ? respuesta.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
				} else {
					folio_auto = respuesta.substring (8, 16);
					estatus = (respuesta.length () > 16) ? respuesta.substring (16) :
						"Operaci&oacute;n no ejecutada";
				}


		    }
			if(fecha_auto==null || fecha_auto.trim().length()==0)
	        	fecha_auto= ObtenFecha(true);

		    porProcesar.add(new RespMancSA(registro, fecha_auto, folio_auto, estatus, false, ""));
		    session.setBandManc(Integer.toBinaryString(porProcesar.size()));

		    if (Global.USAR_BITACORAS.equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession(false));
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EA_MANCOM_CONS_AUTO_ARCH_REALIZA_OPERACIONES);

				if(codError!=null ){
					if(codError.substring(0,2).equals("OK")){
						bt.setIdErr("OK");
					}else if(codError.length()>8){
						bt.setIdErr(codError.substring (0, 8));
					}else{
						bt.setIdErr(codError.trim());
					}
				}else{
					bt.setIdErr(" ");
				}
				if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
					&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
							.equals(BitaConstants.VALIDA)) {
					bt.setIdToken(session.getToken().getSerialNumber());
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}

				try {
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			}

		    if (aut == 2 && nominasAttr != null && nominasAttr.equals("NOMI")) {
		    	break; //Rompe el ciclo si es Cancelaci�n de Nomina
		    }

    	}//fin del FOR

    	request.getSession ().setAttribute ("OperacionesMancomunadas", porProcesar);
		String banderaManc = "";
		banderaManc = session.getBandManc();
		request.setAttribute("banderaManc",session.getBandManc ());

    	request.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
    	request.getRequestDispatcher ("/jsp/ResMancomunidadSA.jsp").forward (request, response);


    }



    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo () {
	return "Short description";
    }

}