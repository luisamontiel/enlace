package mx.altec.enlace.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.beans.ConfEdosCtaTcIndBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.ConfEdosCtaIndBO;
import mx.altec.enlace.bo.ConfEdosCtaTcIndBO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;

/*Inicia - EVERIS VALIDACION CSS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION CSS */
import org.apache.commons.lang.StringUtils;


/** 
*   Isban M�xico
*   Clase: ConfEdosCtaTcIndServlet.java
*   Descripci�n: configuracion de paperless para estado de cuenta individual.
*
*   Control de Cambios:
*   1.0 24/02/2015 FSW-Indra - creaci�n
*   1.1 16/10/2017 Everis - Limpieza Cross Site Scripting
*/
public class ConfEdosCtaTcIndServlet extends BaseServlet {

	/** Constante serial version */
	private static final long serialVersionUID = 3634258462003593871L;
	/** Constante mensaje de error por facultades */
	private static final String MSJ_ERROR_FAC = "Usuario sin facultades para operar con este servicio";
	/** Constante codigo de exito de ODD4 */
	private static final String COD_EXITO = "ODA0002";
	/** Constante para titulo de pantalla */
	private static final String TITULO_PANTALLA = "Estados de Cuenta";
	/** Constante para posicion de menu */
	private static final String POS_MENU = "Estados de Cuenta &gt; Modificar estado de env&iacute;o del estado de Cuenta Impreso &gt; Individual";
	/** Constante para estado de cuenta disponible */
	private static final String EDO_CTA_DISP = "edoCtaDisponible";
	/** Constante para archivo de ayuda **/
	private static final String ARCHIVO_AYUDA = "aEDC_ConfigInd";
	/** Constante para session de BaseResource */
	private static final String BASE_RESOURCE = "session";
	/** Constante para Secuencia de Domicilio **/
	private static final String SEQ_DOM = "hdSeqDomicilio";
	/** Constante para Estado de Cuenta Disponible **/
	private static final String EDOCTA_DISP = "hdEdoCtaDisponible";
	/** Constante para Suscripcion a Paperless **/
	private static final String SUSCR_PAPER = "hdSuscripPaperless";
	/** Constante para eval template **/
	private static final String EVAL_TEMPLATE="/jsp/confEdosCtasTcIndividual.jsp";
	/** Constante para Cadena hdCodCliente **/
    private static final String CAD_HDCOD_CLIENTE="hdCodCliente";
	/** Constante para suscripPaperless */
	private static final String SUS_PAPERLESS = "suscripPaperless";
	
	private static final String VALIDA_CAMBIO_PAPERLESS="validaCambioPaperless";
	/** Constante para tarjeta */
	private static final String TARJETA = "txtTarjeta";
	/** Constante que indica tipo cuenta */
	private static final String TCUENTA = "001";
	/** Constante que indica tipo tarjeta */
	private static final String TTARJETA = "003";
	/** Constante que indica la literal flujoOp */
	private static final String FLUJOOP = "flujoOp";
	/** Error generico al consultar transacciones */
	private static final String ERROR_CONSULTA = "Ocurri&oacute; un error al consultar el estado de env&iacute;o del estado de Cuenta Impreso";
	/** Error generico al consultar transacciones */
	private static final String ERROR_ACTUALIZA = "Ocurri&oacute; un error al actualizar el estado de env&iacute;o  \n";
	/** Constante para la literal CONS */
	private static final String CONS = "CONS";
	/** Constante para la literal "tipoOp" */
	private static final String TIPO_OP = "tipoOp";
	/** Constante para la literal "segmentoDom" */
	private static final String SEGMENTODOM = "segmentoDom";
	/** Constante para la literal "cuenta" */
	private static final String CUENTA = "cuenta";
	/** Constante para la literal "codCliente" */
	private static final String CODCLIENTE = "codCliente";
	/** Constante para la literal "domicilio" */
	private static final String DOMICILIO = "domicilio";
	/** Constante para la literal "fechaConfigValida"  */
	private static final String FECHACONFIGVALIDA = "fechaConfigValida";
	/** Constante para la literal "credito"  */
	private static final String CREDITO = "credito";
	/** Constante para la literal "contrato"  */
	private static final String CONTRATO = "contrato";
	/** Constante para la literal "descripcion"  */
	private static final String DESCRIPCION = "descripcion";





	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws IOException Excepcion
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
    	HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(BASE_RESOURCE);
		ConfEdosCtaTcIndAux confEdosCtaTcIndAux = new ConfEdosCtaTcIndAux();
		String flujo = request.getParameter(FLUJOOP);

		boolean sesionvalida = SesionValida( request, response );

		if(sesionvalida) {
			if(session.getFacultad(BaseResource.FAC_EDO_CTA_IND)) {
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("Encabezado", CreaEncabezado(TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, request));
				setAtributos(request);
				if ((flujo==null || "".equals(flujo))) {
					//[IECP-00] Entra a pantalla principal de Modificacion de Envio de Estado de Cuenta Individual
					final BitaAdminBean bean = new BitaAdminBean();
					bean.setTipoOp("");
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, request.getSession(), BitaConstants.EA_INGRESA_CONFIG_EDO_CTA_IND, bean, false);
					request.setAttribute(FLUJOOP,CONS);
					request.setAttribute("opcion","");
					confEdosCtaTcIndAux.ctasTarjetas(request, response);
					evalTemplate(EVAL_TEMPLATE, request, response);
				} else if( "CTATARJETACOMBO".equals(flujo) ) {
					request.setAttribute("opcion","1");
					request.setAttribute(FLUJOOP,CONS);
					confEdosCtaTcIndAux.ctasTarjetas(request, response);
					evalTemplate(EVAL_TEMPLATE, request, response);
				} else if( CONS.equals(flujo) ) {
					consultar(request, response);
				} else if( "EDIT".equals(flujo) ) {
					configuraSuscripcionPaperless(request, response);
				} else if( "EXPCTA".equals(flujo) || "EXPTARJ".equals(flujo) ) {
					confEdosCtaTcIndAux.exportar(request, response);
				}
			} else {
				despliegaPaginaErrorURL(MSJ_ERROR_FAC, TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, null, request, response);
				return;
			}
		}
    }
    /**
     * Metodo para setear valores en vacio al request
     * @since 05/03/2015
     * @author FSW-Indra
     * @param request peticion
     */
    public void setAtributos(HttpServletRequest request){
		/*Inicia - EVERIS VALIDACION CSS */
    	EIGlobal.mensajePorTrace("Validar CSS para el parametro [tipo_op]", EIGlobal.NivelLog.INFO);
    	request.setAttribute(TIPO_OP, request.getParameter(TIPO_OP) != null ? UtilidadesEnlaceOwasp.cleanString(request.getParameter(TIPO_OP)) : "");
    	/*Finaliza - EVERIS VALIDACION CSS */
		request.setAttribute(SUS_PAPERLESS, request.getParameter(SUS_PAPERLESS) != null ? request.getParameter(SUS_PAPERLESS) : "");
    	request.setAttribute(SEGMENTODOM, request.getParameter(SEGMENTODOM) != null ? request.getParameter(SEGMENTODOM) : "");
    	request.setAttribute(CUENTA, request.getParameter(CUENTA) != null ? request.getParameter(CUENTA) : "");
    	request.setAttribute(CODCLIENTE, request.getParameter(CODCLIENTE) != null ? request.getParameter(CODCLIENTE) : "");
    	request.setAttribute(DOMICILIO, request.getParameter(DOMICILIO) != null ? request.getParameter(DOMICILIO) : "");
    	request.setAttribute(FECHACONFIGVALIDA, request.getParameter(FECHACONFIGVALIDA) != null ? request.getParameter(FECHACONFIGVALIDA) : "");
    	request.setAttribute(CREDITO, request.getParameter(CREDITO) != null ? request.getParameter(CREDITO) : "");
    	request.setAttribute(CONTRATO, request.getParameter(CONTRATO) != null ? request.getParameter(CONTRATO) : "");
    	request.setAttribute(DESCRIPCION, request.getParameter(DESCRIPCION) != null ? request.getParameter(DESCRIPCION) : "");
    	request.setAttribute(DOMICILIO, request.getParameter(DOMICILIO) != null ? request.getParameter(DOMICILIO) : "");
    	request.setAttribute("msgExcede", request.getParameter("msgExcede") != null ? request.getParameter("msgExcede") : "");
    	request.setAttribute("msgOption", "");
    }
    /**
     * Realiza las consultas de la cuentas o tarjetas
     * @param request : request
     * @param response : response
     * @throws IOException : manejo de excepcion
	 * @throws ServletException : manejo de excepcion
     */
    private void consultar( HttpServletRequest request, HttpServletResponse response )
    		throws IOException, ServletException {
		ConfEdosCtaIndBO confEdosCtaIndBO = new ConfEdosCtaIndBO();
		ConfEdosCtaTcIndBO confEdosCtaTcIndBO = new ConfEdosCtaTcIndBO();
    	ConfEdosCtaIndBean confEdosCtaIndBean = null;
    	ConfEdosCtaTcIndBean confEdosCtaTcIndBean = null;

		String cuenta = request.getParameter("hdnCuentasTarjetas") != null ? request
				.getParameter("hdnCuentasTarjetas") : "";
		String tarjeta = "";

		SimpleDateFormat sFormat = new SimpleDateFormat("dd-MM-yy", Locale.US);
		String fechaHoy = sFormat.format(new Date());
		String tipoOp = "";

		if (cuenta.trim().length() == 16) {
			tarjeta = cuenta.trim();
			cuenta = "";
			tipoOp = TTARJETA;
		} else {
			cuenta = cuenta.trim();
			tarjeta = "";
			tipoOp = TCUENTA;
		}

		if(TCUENTA.equals(tipoOp)){
			//Es por cuenta
			confEdosCtaIndBean = confEdosCtaIndBO.mostrarCuentasPDF(cuenta);
			request = setValores("CTA1", confEdosCtaIndBean, request, null, null);
			if(confEdosCtaIndBean.isTieneProblemaDatos()){
				request.setAttribute("MENSAJE_PROBLEMA_DATOS", confEdosCtaIndBean.getMensajeProblemaDatos());
			}
			request = setValores("CTA2", confEdosCtaIndBean, request, cuenta, null);
			if(confEdosCtaIndBean.getFechaConfig() != null) {
				request.setAttribute(FECHACONFIGVALIDA,
						!StringUtils.equals(fechaHoy, sFormat.format(confEdosCtaIndBean.getFechaConfig())) ? "SI" : "NO");
			} else {
				request.setAttribute(FECHACONFIGVALIDA, "SI");
			}
			if(COD_EXITO.equals(confEdosCtaIndBean.getCodRetorno()) || "DYE0021".equals(confEdosCtaIndBean.getCodRetorno())
					|| ("ODE0065".equals(confEdosCtaIndBean.getCodRetorno()) && confEdosCtaIndBean.getCuentas().size() > 0 )) {
				request.getSession().setAttribute("cuentas", confEdosCtaIndBean.getCuentas());
				evalTemplate(EVAL_TEMPLATE, request, response);
			}else {
				EIGlobal.mensajePorTrace("CodRetorno ["+confEdosCtaIndBean.getCodRetorno()+"]",
						EIGlobal.NivelLog.DEBUG);
				despliegaPaginaErrorURL(confEdosCtaIndBean.getDescRetorno() == null || "".equals(confEdosCtaIndBean.getDescRetorno()) ? ERROR_CONSULTA
						: confEdosCtaIndBean.getDescRetorno(), TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,"ConfEdosCtaTcIndServlet", request, response);
				return;
			}
		}else if(TTARJETA.equals(tipoOp)){
			//Es por tarjeta
			confEdosCtaTcIndBean = confEdosCtaTcIndBO.mostrarTarjetasPDF(tarjeta);

			if(confEdosCtaTcIndBean.isTieneProblemaDatos()){
				EIGlobal.mensajePorTrace("CodRetorno ["+confEdosCtaTcIndBean.getCodRetorno()+"]",
						EIGlobal.NivelLog.DEBUG);
				despliegaPaginaErrorURL(confEdosCtaTcIndBean.getDescRetorno() == null || "".equals(confEdosCtaTcIndBean.getDescRetorno()) ? ERROR_CONSULTA
						: confEdosCtaTcIndBean.getDescRetorno(), TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA, "ConfEdosCtaTcIndServlet",request, response);
				return;
			}
			request = setValores("TARJETA1", null, request, tarjeta, confEdosCtaTcIndBean);
			if(confEdosCtaTcIndBean.getFechaConfig() != null) {
				request.setAttribute(FECHACONFIGVALIDA,
						!StringUtils.equals(fechaHoy, sFormat.format(confEdosCtaTcIndBean.getFechaConfig())) ? "SI" : "NO");
			} else {
				request.setAttribute(FECHACONFIGVALIDA, "SI");
			}
			request.getSession().setAttribute("tarjetas", confEdosCtaTcIndBean.getTarjetas());
			evalTemplate(EVAL_TEMPLATE, request, response);
		}

    }
    /**
     * Metodo para setear valores al request
     * @since 10/04/2015
     * @author FSW-Indra
     * @param tipo de set
     * @param confEdosCtaIndBean valores para cuenta
     * @param request informacion
     * @param ctaTarjeta cuenta o tarjeta
     * @param confEdosCtaTcIndBean valores para la tarjeta
     * @return HttpServletRequest request
     */
	private HttpServletRequest setValores(String tipo,
			ConfEdosCtaIndBean confEdosCtaIndBean, HttpServletRequest request,
			String ctaTarjeta, ConfEdosCtaTcIndBean confEdosCtaTcIndBean) {
    	if( "CTA1".equals(tipo) ){
    		request.setAttribute(SEGMENTODOM, confEdosCtaIndBean.getSecDomicilio());
			request.setAttribute(DOMICILIO, confEdosCtaIndBean.getDomCompleto());
			request.setAttribute(EDO_CTA_DISP, confEdosCtaIndBean.isEdoCtaDisponible());
			request.setAttribute(SUS_PAPERLESS, confEdosCtaIndBean.isSuscripPaperless());
			request.setAttribute(CODCLIENTE, confEdosCtaIndBean.getCodCliente());
    	} else if( "CTA2".equals(tipo) ){
    		request.setAttribute(CUENTA, ctaTarjeta);
			request.setAttribute(FLUJOOP,"EDIT");
			request.setAttribute(TIPO_OP,TCUENTA);
    	} else if( "TARJETA1".equals(tipo) ){
    		request.setAttribute(CONTRATO, confEdosCtaTcIndBean.getCodent() + "-" + confEdosCtaTcIndBean.getCentalt() + "-" + confEdosCtaTcIndBean.getContrato());
			request.setAttribute(CREDITO, ctaTarjeta);
			request.setAttribute(DESCRIPCION, confEdosCtaTcIndBean.getNombreComp());
			EIGlobal.mensajePorTrace("confEdosCtaTcIndBean.getDomCompleto["+confEdosCtaTcIndBean.getDomCompleto()+"]", EIGlobal.NivelLog.DEBUG);
			request.setAttribute(DOMICILIO, confEdosCtaTcIndBean.getDomCompleto());
			request.setAttribute(CODCLIENTE, confEdosCtaTcIndBean.getCodCliente());
			request.setAttribute(SUS_PAPERLESS, confEdosCtaTcIndBean.isSuscripPaperless());
		
			request.setAttribute(TARJETA, ctaTarjeta);
			request.setAttribute(FLUJOOP,"EDIT");
			request.setAttribute(TIPO_OP,TTARJETA);
			
			if(confEdosCtaTcIndBean.getTipoPersona()!=null && confEdosCtaTcIndBean.getTipoPersona().equalsIgnoreCase("F"))
					{
						request.setAttribute(VALIDA_CAMBIO_PAPERLESS, "S");
					}
			else
				{
					request.setAttribute(VALIDA_CAMBIO_PAPERLESS, "N");
				}
    	}
    	return request;
    }

	/**
     * Metodo que configura la suscripcion a paperless
     * @param request peticion del cliente
     * @param response respuesta
     * @throws IOException excepcion a manejar
     * @throws ServletException excepcion a manejar
     */
    private void configuraSuscripcionPaperless(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    	ConfEdosCtaTcIndBO confEdosCtaTcIndBO = new ConfEdosCtaTcIndBO();
		ConfEdosCtaTcIndBean confEdosCtaTcIndBean = new ConfEdosCtaTcIndBean();
		/*Inicia - EVERIS VALIDACION CSS */
		EIGlobal.mensajePorTrace("Validar CSS para el parametro [tipo_op]", EIGlobal.NivelLog.INFO);
    	String tipoOp = UtilidadesEnlaceOwasp.cleanString(request.getParameter(TIPO_OP)),
    		aceptaContrato = request.getParameter("hdACAP"),
    		valida = request.getParameter("valida"),
    		tarjetaCuenta = "", bitaC_ECP="";;
		/*Finaliza - EVERIS VALIDACION CSS */

    	if( "001".equals(tipoOp) ){
    		tarjetaCuenta = request.getParameter("hdCuenta");
    	} else if( "003".equals(tipoOp) ){
    		tarjetaCuenta = request.getParameter("hdTarjeta");
    	}
    	boolean aceCon = false, bajaPaperless = true;
    	ConfEdosCtaTcIndAux edoCtaTcIndAux = new ConfEdosCtaTcIndAux();

		String[] checkboxPaperless = request.getParameterValues(SUS_PAPERLESS);

		bajaPaperless = (checkboxPaperless != null) ? false : true;

		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(BASE_RESOURCE);

		if( !validaPeticion(request, response, session, request.getSession(), valida) ) {
			aceCon = true;
			if(valida == null && aceCon) {
				//Se valida configuracion previa.... se Envia seqDomicilio y codCliente.
				edoCtaTcIndAux.validaConfPrevia(request, response, tarjetaCuenta);
				boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.DISP_EDO_CTA);
				EIGlobal.mensajePorTrace(this + "-----> 1 -validaAceptaContrato--><--", EIGlobal.NivelLog.INFO);
				if(session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP) &&
					session.getToken().getStatus() == 1 && solVal) {

					EIGlobal.mensajePorTrace(this + "-----> A -validaAceptaContrato--><--", EIGlobal.NivelLog.INFO);

					edoCtaTcIndAux.validaAceptaContrato(request, aceptaContrato,
							aceCon, valida, tarjetaCuenta, bajaPaperless);
					ValidaOTP.guardaParametrosEnSession(request);
					request.getSession().removeAttribute("mensajeSession");
					ValidaOTP.mensajeOTP(request, "ConfigEdoCuenta");
					ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
				}else {
					EIGlobal.mensajePorTrace(this + "-----> B -validaAceptaContrato--><--", EIGlobal.NivelLog.INFO);
					valida = "1";
					edoCtaTcIndAux.validaAceptaContrato(request, aceptaContrato,
							aceCon, valida, tarjetaCuenta, bajaPaperless);
				}
			}
			if(valida!=null && "1".equals(valida) && aceCon) {
				EIGlobal.mensajePorTrace(this + "-----> C -validaAceptaContrato--><--", EIGlobal.NivelLog.INFO);
				edoCtaTcIndAux.bitacorizaAdmin(request, confEdosCtaTcIndBean, BitaConstants.EA_CONFIG_PDF_EDO_CTA_IND,true);
				edoCtaTcIndAux.bitacoriza(request, tarjetaCuenta, BitaConstants.CONF_CONFIG_EDO_CUENTA_IND, BitaConstants.CONCEPTO_CONF_CONFIG_EDO_CUENTA_IND);
				confEdosCtaTcIndBean = confEdosCtaTcIndBO.configuraPaperless(
											request.getParameter(SUS_PAPERLESS),
											request.getParameter(SEQ_DOM),
											tarjetaCuenta.concat("@").concat(tipoOp),
											session.getUserID8(),
											request.getParameter(CAD_HDCOD_CLIENTE),
											request.getParameter(EDOCTA_DISP),
											request.getParameter(SUSCR_PAPER),
											session.getContractNumber(),
											request.getParameter("hdContrato").replaceAll("-", ""));
				if("S".equals(request.getParameter(SUS_PAPERLESS)) &&
						!"S".equals(request.getParameter(SUSCR_PAPER))) {
					//[IECP-02] Finaliza la Inscripcion Estado de Cuenta a Paperless
					bitaC_ECP=BitaConstants.CONFIG_IND_EDO_CUENTA_PLESS;
					// Realiza guardado de bitacora de operaciones [IECP].
					edoCtaTcIndAux.bitacoriza(request, tarjetaCuenta,
							BitaConstants.EA_ALTA_INSCRIPCION_EDO_CTA_PAPERLESS,
							BitaConstants.INSCRIPCION_EDO_CUENTA_PAPERLESS);
				} else {
					//[IECP-02] Finaliza la baja de la Inscripcion Estado de Cuenta a Paperless
					bitaC_ECP=BitaConstants.CONFIG_IND_EDO_CUENTA_BPLESS;
					// Realiza guardado de bitacora de operaciones [BICP].
					edoCtaTcIndAux.bitacoriza(request, tarjetaCuenta,
							BitaConstants.EDO_CUENTA_IND_BAJA_PAPERLESS,
							BitaConstants.CONCEPTO_BAJA_PAPERLESS);
				}
				if ( "OK".equals(confEdosCtaTcIndBean.getCodRetorno())
						|| confEdosCtaTcIndBean.getCodRetorno().contains("ODA") ) {
					edoCtaTcIndAux.notificaOkOperacion(request, response, confEdosCtaTcIndBean, bitaC_ECP);
					request.setAttribute(FLUJOOP,CONS);
					request.setAttribute("opcion","");
					edoCtaTcIndAux.ctasTarjetas(request, response);
					evalTemplate(EVAL_TEMPLATE, request, response);
				}else {
					despliegaPaginaErrorURL(confEdosCtaTcIndBean.getDescRetorno() == null || "".equals(confEdosCtaTcIndBean.getDescRetorno()) ? ERROR_ACTUALIZA + confEdosCtaTcIndBean.getMensajeError()
							: confEdosCtaTcIndBean.getDescRetorno(), TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,"ConfEdosCtaTcIndServlet", request, response);
					return;
				}
				ValidaOTP.reestableceParametrosEnSession(request);
			}
		}
    }

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws IOException Excepcion
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
		processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws IOException Excepcion
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
		processRequest (request, response);
    }

}