package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



public class pdModifBenef extends BaseServlet{
    String fecha_hoy = "";
    String strFecha = "";
    String strDebug="";
    String VarFechaHoy="";
    short suc_opera = (short)787;

	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		String usuario = "";
		String contrato = "";
		String clave_perfil = "";

		if( SesionValida( req, res )){
			usuario = session.getUserID8();
		    contrato = session.getContractNumber();
			clave_perfil = session.getUserProfile();
		    suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			EIGlobal.mensajePorTrace( "***pdModifBenef.class  &Inicia clase&", EIGlobal.NivelLog.INFO);
			String ventana = (String) req.getParameter("ventana");
			EIGlobal.mensajePorTrace( "***pdModifBenef.class  &ventana:"+ventana+"&", EIGlobal.NivelLog.INFO);
			if(ventana.equals("0"))
		    {
    		   if (session.getFacultad(session.FAC_PADIRMTOBEN_PD))
			   {
//    			 TODO: BIT CU 4301, El cliente entra al flujo
   				/*
   	    		 * VSWF ARR -I
   	    		 */
    			   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
    			   try{

   				BitaHelper bh = new BitaHelperImpl(req, session,sess);
   				if (req.getParameter(BitaConstants.FLUJO)!=null){
				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				  }else{
				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				  }
   				BitaTransacBean bt = new BitaTransacBean();
   				bt = (BitaTransacBean)bh.llenarBean(bt);
   				bt.setNumBit(BitaConstants.ES_PAGO_DIR_MAN_BENEF_MODIF_ENTRA);
   				if(contrato!=null && !contrato.equals(""))
   					bt.setContrato(contrato);

   				BitaHandler.getInstance().insertBitaTransac(bt);
   				}catch (SQLException e){
   					e.printStackTrace();
   				}catch(Exception e){
   					e.printStackTrace();
   				}
    			   }
   	    		/*
   	    		 * VSWF ARR -F
   	    		 */
			     inicia(usuario,clave_perfil,contrato, req, res );
			   }
			   else
               {
                req.setAttribute("Error","No tiene facultad para modificar beneficiarios");
 	              despliegaPaginaErrorURL("No tiene facultad para modificar beneficiarios", "Modificaci&oacute;n de Beneficiarios", "Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n", "s31072h", "pdModifBenef?ventana=0", req, res);
			   }
		    }
			if(ventana.equals("1"))
		    {
       		    modificar(clave_perfil, contrato, usuario, req, res );
		    }
		}
		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &Termina clase&", EIGlobal.NivelLog.INFO);

    }

	/*-----------------------------------------------------------------------*/
	public int inicia(String usuario,String clave_perfil,String contrato, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		int EXISTE_ERROR = 0;
		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &inicia m�todo inicia()&", EIGlobal.NivelLog.INFO);
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		EXISTE_ERROR =0;
		String cadenaBeneficiarios = listarBeneficiarios(contrato,usuario, clave_perfil, suc_opera,req,res);
		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);
		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);

		req.setAttribute("FechaHoy", ObtenFecha(false));
		req.setAttribute("ContUser",ObtenContUser(req));
		req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Modificaci&oacute;n de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n","s31072h",req) );

		if (EXISTE_ERROR == 1 )
			despliegaPaginaError(cadenaBeneficiarios,"Modificaci&oacute;n de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n", req, res);
		else
			evalTemplate("/jsp/pdModifBenef.jsp", req, res );

		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &termina m�todo inicia()&", EIGlobal.NivelLog.INFO);
		return 1;
	}
	/*-----------------------------------------------------------------------------*/
	public int modificar(String clave_perfil,String contrato, String usuario,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &inicia m�todo modificar()&", EIGlobal.NivelLog.INFO);

		String CveBeneficiario=(String) req.getParameter("cboCveBeneficiario");
		String Beneficiario=(String) req.getParameter("txtBeneficiario");
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		StringBuffer loc_trama = new StringBuffer("");
		String glb_trama = "";
		StringBuffer trama_entrada=new StringBuffer("");
		String trama_salida="";
		String cabecera = "1EWEB";
		String operacion = "MBOP";
		String nombre_benef="";
		String resultado="";
		String mensaje = "";

		int primero=0;
		int ultimo=0;
		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &clave de beneficiario:"+CveBeneficiario+"&", EIGlobal.NivelLog.INFO);
		//TuxedoGlobal tuxedoGlobal;
				ServicioTux tuxedoGlobal = new ServicioTux();
				//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		fecha_hoy=ObtenFecha();

		if ( CveBeneficiario.trim().equals("") )
		{
		    mensaje = "Debe seleccionar una cuenta.";
		}
		else
		{
          if (Beneficiario.trim().equals(""))
	      {
		     mensaje = "Cadena no valida";
		  }
		  else
		  {
			  CveBeneficiario = CveBeneficiario.substring(0,CveBeneficiario.indexOf("@"));
		      loc_trama.append( formatea_contrato(contrato) );
			  loc_trama.append("@" );
			  loc_trama.append(CveBeneficiario );
			  loc_trama.append("@" );
			  loc_trama.append(Beneficiario.trim() );
			  loc_trama.append("@" );
		      trama_entrada.append( cabecera );
			  trama_entrada.append( "|" );
			  trama_entrada.append(usuario );
			  trama_entrada.append("|" );
			  trama_entrada.append(operacion );
			  trama_entrada.append("|" );
			  trama_entrada.append(formatea_contrato(contrato) );
			  trama_entrada.append("|" );
			  trama_entrada.append(usuario );
			  trama_entrada.append("|" );
		      trama_entrada.append( usuario );
			  trama_entrada.append("|" );
			  trama_entrada.append(loc_trama.toString());
			  EIGlobal.mensajePorTrace( "***pdModifBenef.class  trama de entrada >>"+trama_entrada.toString()+"<<", EIGlobal.NivelLog.DEBUG);
			  EIGlobal.mensajePorTrace( "***pdModifBenef.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
			  try{
			  	Hashtable hs = tuxedoGlobal.web_red(trama_entrada.toString());
		      	trama_salida = (String) hs.get("BUFFER");
			  	EIGlobal.mensajePorTrace( "***pdModifBenef.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);
			  }catch( java.rmi.RemoteException re ){
			  	re.printStackTrace();
			  } catch (Exception e) {}
			  if(trama_salida.startsWith("OK")){
		          primero = trama_salida.indexOf( (int) '@');
		          ultimo  = trama_salida.lastIndexOf( (int) '@');
		          resultado = trama_salida.substring(primero+1, ultimo);
		          mensaje = resultado;
//		        TODO: BIT CU 4301, A4
		  		/*
		  		 * VSWF ARR -I
		  		 */
		  		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		  		try{
		  		BitaHelper bh = new BitaHelperImpl(req, session,sess);
		  		BitaTransacBean bt = new BitaTransacBean();
		  		bt = (BitaTransacBean)bh.llenarBean(bt);
		  		bt.setNumBit(BitaConstants.ES_PAGO_DIR_MAN_BENEF_MODIF_MODIF_BENEF);
		  		bt.setContrato(contrato);
		  		bt.setServTransTux(operacion);
		  		if(trama_salida!=null){
		  			 if(trama_salida.substring(0,2).equals("OK")){
		  				   bt.setIdErr("MBOP0000");
		  			 }else if(trama_salida.length()>8){
		  				  bt.setIdErr(trama_salida.substring(0,8));
		  			 }else{
		  				  bt.setIdErr(trama_salida.trim());
		  			 }
		  			}
		  		BitaHandler.getInstance().insertBitaTransac(bt);
		  		}catch (SQLException e){
		  			e.printStackTrace();
		  		}catch(Exception e){
		  			e.printStackTrace();
		  		}
		  		}
		  		/*
		  		 * VSWF ARR -F
		  		 */
		      }
			  else
			  {
		          mensaje = trama_salida;
		      }
		   }
		}


		String cadenaBeneficiarios = listarBeneficiarios(contrato,usuario, clave_perfil, suc_opera,req,res);

		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);

		req.setAttribute("FechaHoy", ObtenFecha(false));
		req.setAttribute("ContUser",ObtenContUser(req));
		req.setAttribute("Fecha", ObtenFecha(true));
		req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
		req.setAttribute("mensaje_salida", mensaje);

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Modificaci&oacute;n de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n","s31072h",req) );

	    despliegaPaginaErrorURL(mensaje,"Modificaci&oacute;n de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n","s31072h","pdModifBenef?ventana=0",req,res);

		return 1;
	}
	/*------------------------------------------------------------------------*/
	String formatea_contrato (String ctr_tmp)
	 {
		/*
		int i=0;
		String temporal="";
		String caracter="";

		for (i = 0 ; i < ctr_tmp.length() ; i++){
		    caracter = ctr_tmp.substring(i, i+1);
		    if (caracter.equals("-")){
		        // se elimina el gui�n
		    }else{
		    	temporal + = caracter ;
		    }
		}
		return temporal;
		*/
		return ctr_tmp.replace('-','\0');
	}

	/*--------------------------------------------------------*/

	public String listarBeneficiarios(String contrato,String usuario,  String clave_perfil, short sucOpera,HttpServletRequest req,HttpServletResponse	res ){

		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &inicia m�todo ListarBeneficiarios()&", EIGlobal.NivelLog.INFO);
		int    indice;
		StringBuffer encabezado          = new StringBuffer("");
		String tramaEntrada        = "";
		String tramaSalida         = "";
		String path_archivo        = "";
		String nombre_archivo      = "";
		String trama_salidaRedSrvr = "";
		String retCodeRedSrvr      = "";
		String registro            = "";
		StringBuffer listaBeneficiarios  = new StringBuffer("");
		String listaNombreBeneficiario = "";
		//TuxedoGlobal tuxGlobal;
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		/*
		 * VSWF
		 */

				HttpSession sess = req.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");
		/*
		 *
		 */

		String medio_entrega  = "2EWEB";
		String tipo_operacion = "CBOP";
		encabezado.append(medio_entrega );
		encabezado.append("|" );
		encabezado.append(usuario );
		encabezado.append("|" );
		encabezado.append(tipo_operacion );
		encabezado.append("|" );
		encabezado.append(contrato );
		encabezado.append("|");
		encabezado.append(usuario );
		encabezado.append("|" );
		encabezado.append(clave_perfil );
		encabezado.append("|");

		tramaEntrada = encabezado.toString() + contrato + "@";

		strDebug += "\ntecb :" + tramaEntrada;

		EIGlobal.mensajePorTrace( "***pdModifBenef.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***pdModifBenef.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

		try{
			Hashtable hs = tuxGlobal.web_red(tramaEntrada);
			tramaSalida = (String) hs.get("BUFFER");
		}catch( java.rmi.RemoteException re ){
			re.printStackTrace();
		} catch(Exception e) {}
		strDebug += "\ntscb:" + tramaSalida;
		EIGlobal.mensajePorTrace( "***pdModifBenef.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
		path_archivo = tramaSalida;
		nombre_archivo = EIGlobal.BuscarToken(path_archivo, '/', 5);
		path_archivo = Global.DOWNLOAD_PATH + nombre_archivo;

		strDebug += "\npath_cb:" + path_archivo;
		log("\npath_cb:" + path_archivo);

		boolean errorFile=false;
		try
		 {
			// Copia Remota
			EIGlobal.mensajePorTrace( "***pdModifBenef.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
			ArchivoRemoto archivo_remoto = new ArchivoRemoto();

			if(!archivo_remoto.copia( nombre_archivo ) )
			{
				// error al copiar
				EIGlobal.mensajePorTrace( "***pdModifBenef.class  &error en remote shell ...&", EIGlobal.NivelLog.INFO);
			}
			else
			{
				// OK
				EIGlobal.mensajePorTrace( "***pdModifBenef.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			}

		    File drvSua = new File(path_archivo);
		    RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
		    trama_salidaRedSrvr = fileSua.readLine();
		    retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
		    if (retCodeRedSrvr.equals("OK"))
		     {
				EIGlobal.mensajePorTrace( "***pdModifBenef.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);
		       while((registro = fileSua.readLine()) != null){
			       	/////
					if( registro.trim().equals(""))
						registro = fileSua.readLine();
					if( registro == null )
						break;
					listaBeneficiarios.append( "<option value=\"" );
					listaBeneficiarios.append(EIGlobal.BuscarToken(registro, ';', 1) );
					listaBeneficiarios.append("\"> " );
					listaBeneficiarios.append(EIGlobal.BuscarToken(registro, '@', 1) );
					listaBeneficiarios.append(" ");
					listaBeneficiarios.append(EIGlobal.BuscarToken(registro, '@', 2) );
					listaBeneficiarios.append("</option>\n");
		        }
		     }
		    fileSua.close();

		 }catch(Exception ioeSua )
		    {
			   EIGlobal.mensajePorTrace( "***pdModifBenef.class  Excepci�n en listarBeneficiarios() >>"+ioeSua.getMessage()+"<<",EIGlobal.NivelLog.ERROR );
		       return "";
		    }
		EIGlobal.mensajePorTrace( "***pdModifBenef.class  &termina m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
		return(listaBeneficiarios.toString());
	}

} // fin clase