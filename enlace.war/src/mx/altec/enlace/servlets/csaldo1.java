/*Banco Santander Mexicano
  Clase csaldo1  Creacion de la pantalla inicial de consulta de saldos..
  @Autor:
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 28/02/2001 - Quitar codigo muerto
                                       09/04/2001 - Modificacion para la presentacion de las pantallas
                                       07/03/2002 - Cambio para integracion
  modificaci�n: Daniel C�zares Beltr�n 05/09/2002.
           Se agreg� el Tipo de Cuenta en el VALUE del CheckBox.
                 Esto para que en la siguiente Consulta se pueda saber
                 la moneda de la Cuenta seleccionada.
  modificacion:Radam�s Cerecero Carre�n 15/09/2003 - Funcionalidad para Norma 43
 */
package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EpymeBO;
import mx.altec.enlace.cliente.ws.impl.WSSNETClient;
import mx.altec.enlace.cliente.ws.impl.WSSNETClientImpl;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
/*Inicia - EVERIS VALIDACION SQL INJECTION*/
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION SQL INJECTION*/
/**
 *  Se realizan cambios para el proyecto de M022948 - P023257 Men� Reducido PyME
 * @author Stefanini
 *
 */
public class csaldo1 extends BaseServlet {
	/**
	 * TEXTHTML etiqueta para texto "text/html"
	 */
	private static final String TEXTHTML = "text/html";
	/**
	 * SESSION etiqueta para texto "session"
	 */
	private static final String SESSION = "session";
	/**
	 * TRAZALOGSESION etiqueta para texto "\n\n\n-----------------------------------\n\n\nNULO EN EL session de la HTTPSESION"
	 */
	private static final String TRAZALOGSESION = "\n\n\n-----------------------------------\n\n\nNULO EN EL session de la HTTPSESION";
	/**
	 * AMBI etiqueta para texto ".ambci"
	 */
	private static final String AMBI = ".ambci";
	/**
	 * MENUPRINCIPAL etiqueta para texto "MenuPrincipal"
	 */
	private static final String MENUPRINCIPAL = "MenuPrincipal";
	/**
	 * NEWMENU etiqueta para texto "newMenu"
	 */
	private static final String NEWMENU = "newMenu";
	/**
	 * ENCABEZADO etiqueta para texto "Encabezado"
	 */
	private static final String ENCABEZADO = "Encabezado";
	/**
	 * CUENTAS etiqueta para texto "cuentas"
	 */
	private static final String CUENTAS = "cuentas";
	/**
	 * CODIGOEE etiqueta para texto "codigoEE"
	 */
	private static final String CODIGOEE = "codigoEE";
	/**
	 * CODIGOFF etiqueta para texto "codigoFF"
	 */
	private static final String CODIGOFF = "codigoFF";
	/**
	 * CODIGOXX etiqueta para texto "codigoXX"
	 */
	private static final String CODIGOXX = "codigoXX";
	/**
	 * PLANTILLAELEGIDA etiqueta para texto "plantillaElegida"
	 */
	private static final String PLANTILLAELEGIDA = "plantillaElegida";
	/**
	 * DIAGONAL etiqueta para texto "/"
	 */
	private static final String DIAGONAL = "/";
	/**
	 * Implementa doGet del servlet
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doGet ( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
        res.setContentType(TEXTHTML);
		defaultAction ( req, res );
	}
	/**
	 * Implementa doPost del servlet
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doPost ( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
        res.setContentType(TEXTHTML);
		defaultAction ( req, res );
	}
	/**
	 * Metodo default de la clase
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void defaultAction ( HttpServletRequest req, HttpServletResponse res )
		throws IOException, ServletException
	{
		HttpSession sess = req.getSession();
		String pre = "";
		String nex = "";
		String tota = "";

		String funcion_llamar	= (String) req.getParameter("prog");
		String funcion_alta = (String) req.getParameter("prog1");
		String actualiza_valor = (String) req.getParameter("prog2");

		String can = (String) req.getAttribute("can");
		String canV = (String) req.getParameter("c");
		String menV = (String) req.getParameter("m");

		if (funcion_llamar==null) {
			funcion_llamar = "0";
		}
		if (actualiza_valor!=null) {
			if(!"5".equals(actualiza_valor)) {
				actualiza_valor="0";
			}
		}else {
			actualiza_valor = "0";
		}
		if (canV==null) {
			canV="";
		}
		if (menV==null) {
			menV="";
		}
		if (can == null) {
			can="";
		}
		EIGlobal.mensajePorTrace("***csaldo1.defaultAction()-> funcion_llamar = " +funcion_llamar+ "<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***csaldo1.defaultAction()-> Validando cierre session = <", EIGlobal.NivelLog.DEBUG);

		if(sess != null){
			BaseResource br = (BaseResource) sess.getAttribute(SESSION);
			br.setExcepcionCierraSess(excepCierreSession(br.getContractNumber(), br.getUserID8()));
			EIGlobal.mensajePorTrace("***csaldo1.defaultAction()-> Validando cierre session =" + br.isExcepcionCierraSess() + " <", EIGlobal.NivelLog.DEBUG);
		}


		if ("0".equals(funcion_llamar))
		{
			 // <vswf RRG cambio nombre de la variable enum a enum1 05122008>
         java.util.Enumeration enum1 = sess.getAttributeNames() ;
 		 //String valparam[] = null;
		 while(enum1.hasMoreElements() ) {
			try{
			//valparam = req.getParameterValues(((String)parametros.nextElement()));
			//for(int j=0;j<valparam.length;j++)
				EIGlobal.mensajePorTrace((String)enum1.nextElement(), EIGlobal.NivelLog.DEBUG);
			}catch(Exception e) {}
		 }


 		 BaseResource session = (BaseResource) sess.getAttribute(SESSION);
         //Usuario session = (Usuario) sess.getAttribute ("usuario");
         //Facultades facs = (Facultades) sess.getAttribute ("facultadesEnlace");

		 if(session ==null) {
			EIGlobal.mensajePorTrace(TRAZALOGSESION, EIGlobal.NivelLog.DEBUG);
		 }

			boolean sesionvalida = SesionValida(req, res);
			if (!sesionvalida) {
				return;
			}

		 if(session != null) {

			 EIGlobal.mensajePorTrace("***csaldo1.defaultAction()-> Validando contrato epyme" ,
					 EIGlobal.NivelLog.INFO);
			 EpymeBO epymeBO=new EpymeBO();

			 if(req.getParameter("menuPyme")!=null){
				EIGlobal.mensajePorTrace("***csaldo1::defaultAction()::estatusMenuPyme::" +(String)req.getParameter("menuPyme"), EIGlobal.NivelLog.INFO);
			    String menuPyme = (String)req.getParameter("menuPyme") ;
			    String opcion = "";
                menuPyme=epymeBO.actualizaMenu(session.getContractNumber(),session.getUserID8(),menuPyme);

                if("A".equals(menuPyme)) {
                	session.setEpyme(true);
                	opcion="1";
                }else {
                	session.setEpyme(false);
                	opcion="2";
                }

                EIGlobal.mensajePorTrace("***csaldo1.defaultAction()-> CARGANDO NUEVAMENTE EL MENU, PARA TOMAR LOS CAMBIOS DE MENU RECORTADO::" +session.isEpyme()+"::",EIGlobal.NivelLog.INFO);
                escribeBitacoraPyme(req, "U", opcion,session.getContractNumber(),session.getUserID8());
                String [][] arregloLocations2 = asignaURLS (req, session);
				String nuevoMenu2 = creaFuncionMenu ( arregloLocations2, req );
				/*Stefanini - Modificacion Menu Epyme*/
				String elMenu2 = creaCodigoMenu (arregloLocations2,req);
				sess.setAttribute ("funcionesMenu",nuevoMenu2);
				session.setFuncionesDeMenu ( nuevoMenu2 );
				sess.setAttribute ("StrMenu",elMenu2);
				session.setStrMenu ( elMenu2 );

			 }
			 else{
				 boolean esMenuReducido = epymeBO.esMenuReducido(session.getContractNumber(),session.getUserID8());
				 session.setEpyme(esMenuReducido);
				 EIGlobal.mensajePorTrace("***csaldo1.defaultAction()-> contrato Epyme = [" + session.getContractNumber() + "][ " + esMenuReducido + "]" , EIGlobal.NivelLog.INFO);
			 }

		 }

         //***********************************************
         //modificaci�n para integraci�n pva 06/03/2002
         //***********************************************
         pre = req.getParameter("prev");
         nex = req.getParameter("next");
         tota = req.getParameter("total");

         EIGlobal.mensajePorTrace("***csaldo1 prev " + pre, EIGlobal.NivelLog.DEBUG);
         EIGlobal.mensajePorTrace("***csaldo1 next " + nex, EIGlobal.NivelLog.DEBUG);
         EIGlobal.mensajePorTrace("***csaldo1 total " + tota, EIGlobal.NivelLog.DEBUG);

         int total=0,prev=0,next=0;
         String paginacion="";
         if ((pre != null) && (nex != null) && (tota != null)) {
            try {
               prev = Integer.parseInt(pre);
               next = Integer.parseInt(nex);
               total = Integer.parseInt(tota);
            } catch (NumberFormatException e) {
               e.printStackTrace();
            }
         } else {

            EIGlobal.mensajePorTrace("***csaldo1 todos nullos", EIGlobal.NivelLog.DEBUG);
            try {
               EIGlobal.mensajePorTrace("el userID: " + session.getUserID8(), EIGlobal.NivelLog.INFO);
            } catch (Exception e) {
               e.printStackTrace();
            }
			EIGlobal.mensajePorTrace("Tomamos las cuentas", EIGlobal.NivelLog.DEBUG);
            llamado_servicioCtasInteg (IEnlace.MConsulta_Saldos," "," "," ",session.getUserID8 ()+AMBI,req);
            total=getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+AMBI);
			//EIGlobal.mensajePorTrace(total, EIGlobal.NivelLog.INFO);
            prev=0;

            if (total>Global.NUM_REGISTROS_PAGINA) {
                next=Global.NUM_REGISTROS_PAGINA;
            } else {
                      next=total;
                    }
         }

		 /*
         EIGlobal.mensajePorTrace ( "***csaldo1 orev"+prev, EIGlobal.NivelLog.INFO);
         EIGlobal.mensajePorTrace ( "***csaldo1 next"+next, EIGlobal.NivelLog.INFO);
         EIGlobal.mensajePorTrace ( "***csaldo1 total"+total, EIGlobal.NivelLog.INFO);
		 */

         //***********************************************


		 if(session.getUrlSuperNet() == null || "".equals(session.getUrlSuperNet())) {
			 /*TODO <VC proyecto="200710001" autor="JGR" fecha="28/05/2007" descripci�n="LIGA A SUPERNET">*/
//			 LigaSuperNetService liga = new LigaSuperNetService();
			 EIGlobal.mensajePorTrace("ID USER->" + session.getUserID8(), EIGlobal.NivelLog.INFO);
			 EIGlobal.mensajePorTrace("CONTRATO->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
			 try {
				 String tramaLiga = "false";
					 //liga.pintaLigaSupernet(session.getUserID8(), session.getContractNumber());
				 EIGlobal.mensajePorTrace("TRAMA LIGA->" + tramaLiga, EIGlobal.NivelLog.DEBUG);
				 tramaLiga = (tramaLiga==null?"":tramaLiga.toLowerCase());
				 if ("true".equals(tramaLiga)){
					 EIGlobal.mensajePorTrace("->EXITO LIGA <-" + tramaLiga, EIGlobal.NivelLog.DEBUG);
					 session.setUrlSuperNet(tramaLiga);
					 //PINTA LIGA
				 }else{
					 EIGlobal.mensajePorTrace("->ERROR LIGA<-" + tramaLiga, EIGlobal.NivelLog.ERROR);
					 session.setUrlSuperNet("false");
					 //NO SE PINTA LIGA PARA ESE CONTRATO
				 }
			 }catch(Exception e){
				 EIGlobal.mensajePorTrace("EXCEPTION EN LIGA->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				 e.printStackTrace();
			 }
			 /*<VC JGR >*/
		 }

         log ("csaldo.java::defaultAction()");
         try{
         req.setAttribute (MENUPRINCIPAL, session.getStrMenu ());
         req.setAttribute (NEWMENU, session.getFuncionesDeMenu ());

         /*se realiza la validacion de las cuentas que contiene el contrato
          * Se invoca al web service para consular las cuentas y verificar que existan en
          * Supernet Comercios
          * JPH VSWF 03/06/2009
          * Requerimiento Liga Enlace - Supernet Comercios
          */
         boolean facSnetComAdmin = verificaFacultad("ADMSNETCOMER", req);
         if(!facSnetComAdmin){
	     		String [] ctasAfiliadas = null;
	    	 	WSSNETClient cliente = new WSSNETClientImpl();

	    	 	HashMap<String, String> ctasSnetComercios = null;
	    	 	// validar que ya tengamos la cuentas cargadas en sesion para que no se invoque a los servicios nuevamente.
	     		//esto por que por aqui pasa el flujo cada ves que se realiza una paginacion.
	     		//JPH VSWF 31/07/2009
	     		ctasSnetComercios = (HashMap)sess.getAttribute("ctasSnet");
	     		EIGlobal.mensajePorTrace("VALOR DE ctasSnetComercios   "+ctasSnetComercios, EIGlobal.NivelLog.INFO);
	     		if(ctasSnetComercios==null || ctasSnetComercios.size()<=0){
	     			EIGlobal.mensajePorTrace("SE INICIA VALIDACION DE CUENTAS CON SUPERNET COMERCIOS.", EIGlobal.NivelLog.INFO);
	    	 	String[][] arrayCuentas = ObteniendoTodasCtasInteg (total,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+AMBI);
	    	 	ctasSnetComercios = new HashMap<String, String>();

	    	 	EIGlobal.mensajePorTrace("Realizando busqueda de cuentas afiliadas a supernet comercios", EIGlobal.NivelLog.INFO);
	    	 	//se convierte el usuario de 7 a 8 posiciones y se consulta el usuario
	    	 	ctasAfiliadas = cliente.consultaCuentasCliente(convierteUsr7a8(session.getUserID8()));

	    	 	if((arrayCuentas !=null)&&(ctasAfiliadas!=null && ctasAfiliadas.length>0)){
	    	 		//primer for para recorrer las cuentas de enlace
	    	 		EIGlobal.mensajePorTrace("SE OBTUVIERON CUENTAS AFILIADAS A SNET COMERCIOS", EIGlobal.NivelLog.INFO);
		       	 	for (int indice2 = 1;indice2<=Integer.parseInt (arrayCuentas[0][0]);indice2++){
		       	 		String cuentaEnlace = arrayCuentas[indice2][1];


		       	 		//segundo for para recorrer las cuentas obtenidas en supernet comercion
		       	 		for(int i=0;i<ctasAfiliadas.length;i++){
		       	 			String cuentaComercios = ctasAfiliadas[i];
		       	 			if(cuentaEnlace.trim().equals(cuentaComercios.trim())){
		       	 				EIGlobal.mensajePorTrace("SE ENCONTRO UNA CUENTA AFILIADA", EIGlobal.NivelLog.INFO);
		       	 				ctasSnetComercios.put(cuentaComercios, arrayCuentas[indice2][4]);
		       	 				break;
		       	 			}
		       	 		}//fin for interno
		       	 	}//fin for externo
	    	 	}
	    	 	EIGlobal.mensajePorTrace("TOTAL DE CUENTAS AFILIADAS A SUPERNET COMERCIOS.---------------> " + ctasSnetComercios.size(), EIGlobal.NivelLog.INFO);
	    	 	session.setCtasSnetComercios(ctasSnetComercios);
	     		}else{
	     			session.setCtasSnetComercios(ctasSnetComercios);
	     			EIGlobal.mensajePorTrace("YA ESTA CARGADA LA LISTA DE CUENTAS AFILIADAS A SNET COMERCIOS--> "+session.getCtasSnetComercios().size(), EIGlobal.NivelLog.INFO);
	     		}
	         //FIN MODIFICACION JPH VSWF
	         //############################################################################################
         }else{
        	 EIGlobal.mensajePorTrace( "NO SE EJECUTO LA VALIDACION DE CUENTAS CON SUPERNET COMERCIOS,"
        			                 + " PORQUE EL USUARIO ES ADMINISTRADOR.", EIGlobal.NivelLog.INFO);
         }


         req.setAttribute (ENCABEZADO, CreaEncabezado ( "Consulta de Saldos de cuentas de cheques",
         "Consultas &gt; Saldos &gt; Por cuenta &gt; Cheques", "s25010h",req ) );
	     req.setAttribute ("can", can);
		 } catch(Exception e) { EIGlobal.mensajePorTrace("Error en al try de atributos",EIGlobal.NivelLog.ERROR);
		 	                 e.printStackTrace();}

         /* Modificacion Validacion de facultades 19/09/2002
         */
        EIGlobal.mensajePorTrace( "\n\n\t\tsession distinto a null:  " + (session != null), EIGlobal.NivelLog.DEBUG);

        boolean despliegaPagMedioNot = false;


        EIGlobal.mensajePorTrace ("ENTRO A C_SALDO_TMPL", EIGlobal.NivelLog.INFO);

        session = (BaseResource) req.getSession().getAttribute (SESSION);

        verificaSuperUsuario(session.getContractNumber(), req);

        EIGlobal.mensajePorTrace("Es SuperUsuario " + session.getUserID8() + " ->" + session.getMensajeUSR().isSuperUsuario(), EIGlobal.NivelLog.INFO);

        subeDatosDeContacto(session.getUserID8(), req);

        if (/*verificaFacultad("CAMMEDNOTSUP", req) && */session.getMensajeUSR().isSuperUsuario()) {
        	if (!"X".equals(session.getStatusCelular()) && !"X".equals(session.getStatusCorreo()) &&
        			"".equals(session.getMensajeUSR().getMedioNotSuperUsuario()) &&
        			"".equals(session.getMensajeUSR().getLadaSuperUsuario()) &&
        			"".equals(session.getMensajeUSR().getNoCelularSuperUsuario()) &&
        			"".equals(session.getMensajeUSR().getEmailSuperUsuario())) {

        		despliegaPagMedioNot = true;
        	}
        }

        if (despliegaPagMedioNot) {
        	EIGlobal.mensajePorTrace("Debido a falta de datos para medios de notificacion, se redirige a la pantalla para actualizar datos", EIGlobal.NivelLog.INFO);
        	res.sendRedirect("ConfigMedioNot?opcion=2");
        	return;
        }


         if(session.getFacultad ( session.FAC_CONSULTA_SALDO )) {

            String strSalida2 = "";
            String Contrato = session.getContractNumber ();
			EIGlobal.mensajePorTrace("csaldos1  contrato = "+Contrato, EIGlobal.NivelLog.INFO);

            String[][] arrayCuentas = ObteniendoCtasInteg (prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+AMBI);
            //***********************************************
            int residuo = 0;
            int contador=1;
            //CCB 17 Jan 2003 - Agregue dos variables de sesi�n
            TIComunUsd tipoCta = new TIComunUsd();
            sess.setAttribute("ctasUSD", "");
            sess.setAttribute("ctasMN", "");

            //TODO BIT CU1011
            /*
			 * 02/ENE/07
			 * VSWF-BMB-I
			 */
            if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
            try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				/*Inicia - EVERIS VALIDACION SQL INJECTION*/
				EIGlobal.mensajePorTrace("Validar SQLINJECTION para el parametro [flujo]", EIGlobal.NivelLog.INFO);
	            if(UtilidadesEnlaceOwasp.cleanQuery(req.getParameter(BitaConstants.FLUJO))!=null){
	            	bh.incrementaFolioFlujo((String)UtilidadesEnlaceOwasp.cleanQuery(req.getParameter(BitaConstants.FLUJO)));
				/*Finaliza - EVERIS VALIDACION SQL INJECTION*/
	            }else{
	            	bh.incrementaFolioFlujo(BitaConstants.EC_SALDO_CUENTA_CHEQUE);
	            }
	    		BitaTransacBean bt = new BitaTransacBean();
	    		bt = (BitaTransacBean)bh.llenarBean(bt);
	    		bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_CHEQUE_ENTRA);
	    		if(Contrato!=null){
	    			bt.setContrato(Contrato);
	    		}
    		BitaHandler.getInstance().insertBitaTransac(bt);
    		}catch(SQLException e){
    			e.printStackTrace();
    		}catch(Exception e){
    			e.printStackTrace();
    		}
            }
            /*
			 * VSWF-BMB-F
			 */
            for (int indice2 = 1;indice2<=Integer.parseInt (arrayCuentas[0][0]);indice2++){
                contador++;
                residuo = contador % 2;
                if (residuo == 0) {
                    strSalida2 = strSalida2 + "<tr  bgcolor=#CCCCCC><td align=right class=textabdatobs><input type=checkbox name=cta value=\""+arrayCuentas[indice2][1] +"@"+arrayCuentas[indice2][2]+"@"+arrayCuentas[indice2][4]+"@" + arrayCuentas[indice2][3] + "@" + contador + "@" + "\"></td><td class=textabdatobs>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatobs>&nbsp;"+arrayCuentas[indice2][4]+"</td></tr>";
                } else {
                    strSalida2 = strSalida2 + "<tr  bgcolor=#EBEBEB><td align=right class=textabdatcla><input type=checkbox name=cta value=\""+arrayCuentas[indice2][1] +"@"+arrayCuentas[indice2][2]+"@"+arrayCuentas[indice2][4]+"@" + arrayCuentas[indice2][3] + "@" + contador + "@" + "\"></td><td class=textabdatcla>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatcla>&nbsp;"+arrayCuentas[indice2][4]+"</td></tr>";
                }
          		if(tipoCta.cuentasUsd(arrayCuentas[indice2][1]))
          		{
          			sess.setAttribute("ctasUSD", "Existe");
          		}
          		else
          		{
          			sess.setAttribute("ctasMN", "Existe");
          		}
                //}//if
            }//for

            req.setAttribute (CUENTAS,strSalida2);
            //***********************************************
            //modificaci�n para integraci�n pva 06/03/2002
            //***********************************************

            if (total>Global.NUM_REGISTROS_PAGINA){
                paginacion="<table  width=450 align=center>";

                if(prev > 0) {
                    //paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:history.back();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";
                    paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:atras();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";
                }
                if(next < total) {
                    if((next + Global.NUM_REGISTROS_PAGINA) <= total) {
                        paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer (next -prev).toString () + " ></A></td></TR>";
                    } else {
                        paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer (total-next).toString () + " ></A></td></TR>";
                    }
                }

                paginacion+="</table><br><br>";
                EIGlobal.mensajePorTrace ( "***paginacion :"+paginacion, EIGlobal.NivelLog.DEBUG);

            }

            req.setAttribute ("total", String.valueOf(total));
            req.setAttribute ("prev", String.valueOf(prev));
            req.setAttribute ("next", String.valueOf(next));
            req.setAttribute ("paginacion", paginacion);
			EIGlobal.mensajePorTrace("Paginacion "+paginacion, EIGlobal.NivelLog.DEBUG);
            req.setAttribute ("varpaginacion", String.valueOf(Global.NUM_REGISTROS_PAGINA));

            //***********************************************
			EIGlobal.mensajePorTrace("***csaldo1 INICIO = " +funcion_llamar, EIGlobal.NivelLog.DEBUG);
            evalTemplate ( IEnlace.C_SALDO_TMPL, req, res);

         }else {

            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
            res.setContentType(TEXTHTML);
            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

         } //fin else 1

		}
		else if  (funcion_llamar.equals("1")) //inicio REGISTRO   /*********************************************/
		{
			 // <vswf RRG cambio nombre de la variable enum a enum1 05122008>
		 java.util.Enumeration enum1 = sess.getAttributeNames() ;
		 while(enum1.hasMoreElements() ) {
			try{
				EIGlobal.mensajePorTrace((String)enum1.nextElement(), EIGlobal.NivelLog.DEBUG);
			}catch(Exception e) {}
		 }

		 boolean sesionvalida1 = SesionValida ( req, res );
		 if (! sesionvalida1 ) { return; }

		 BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		 String CodigoMensajeB = "1";
		 String codigoEE = "";
		 String codigoFF = "";
		 String codigoXX = "";
		 String codigoEE1 = "";
		 String codigoXX1 = "";
		 String tot = "";
		 String ant = "";
		 String sig = "";
		 String lado = "0";
		 int indice = 1;
		 String sessionIdN43 = (String) req.getSession().getId();
		 lado= (String)req.getParameter("Lado");
		 if (lado==null) {
			 lado="0";
		 }

		 if(session ==null) {
			 EIGlobal.mensajePorTrace(TRAZALOGSESION, EIGlobal.NivelLog.DEBUG);
		 }

		 if(session.getFacultad ( session.FAC_CONSPROGVIG_N43 )) {
//				TODO CU1091
			    /*
				 * 05/ENE/07
				 * VSWF-BMB-I
				 */
			  if ("ON".equals(Global.USAR_BITACORAS.trim())){
			 try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);
					/*Inicia - EVERIS VALIDACION SQL INJECTION*/
					EIGlobal.mensajePorTrace("Validar SQLINJECTION para el parametro [flujo]", EIGlobal.NivelLog.INFO);
				    if(UtilidadesEnlaceOwasp.cleanQuery(req.getParameter(BitaConstants.FLUJO))!=null){
					/*Finaliza - EVERIS VALIDACION SQL INJECTION*/
				    	EIGlobal.mensajePorTrace("Validar SQLINJECTION para el parametro [flujo]", EIGlobal.NivelLog.INFO);
				    	bh.incrementaFolioFlujo((String)UtilidadesEnlaceOwasp.cleanQuery(req.getParameter(BitaConstants.FLUJO)));
				    }else{
				    	bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				    }
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_REGISTRO_ENTRA);
					if(session.getContractNumber()!=null){
						bt.setContrato(session.getContractNumber());
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch (Exception e){
					e.printStackTrace();
				}
			  }
			    /*
				 * VSWF-BMB-F
				 */

		  if ("0".equals(funcion_alta)) {
			codigoEE = (String)req.getParameter(CODIGOEE);
			codigoFF = (String)req.getParameter(CODIGOFF);
			codigoXX = (String)req.getParameter(CODIGOXX);

			//paginacion de programaciones vigentes
			tot = (String)req.getParameter("tot");
			ant = (String)req.getParameter("ant");
			sig=  (String)req.getParameter("sig");

			if (tot==null)
			{tot="";}
			if (ant==null)
			{ant="";}
			if (sig==null)
			{sig="";}
			EIGlobal.mensajePorTrace( "++++++1 T="+tot+",A="+ant+",S="+sig+", Lado="+lado, EIGlobal.NivelLog.INFO);
			if (codigoEE==null)
			{codigoEE="";}
			if (codigoFF==null)
			{codigoFF="";}
			if (codigoXX==null)
			{codigoXX="1";}

			EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***INICIA CRegCon.java > Consulta", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);

			/**************************************************************/
			/* LLAMADO AL SERVICIO DE CONSULTA DE PROGRAMACIONES VIGENTES */
			/**************************************************************/
			String tramaentrada = "";
			boolean bandera = false;
			Hashtable htResult;
			String coderror = "";
			String CodigoMensaje = "";
			String CodigoArchivo = "";
			String CodigoPath = "";

			try
			{
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Vigentes", EIGlobal.NivelLog.DEBUG);
				// LLAMADO AL SERVICIO TUXEDO
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				tramaentrada = "2EWEB|" + session.getUserID8() + "|CPRV|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
				tramaentrada = tramaentrada + session.getContractNumber() + "@" + session.getUserID8() + "@" + sessionIdN43 +"@|";
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Vigentes > " + tramaentrada, EIGlobal.NivelLog.INFO);

				//Llama a la Consulta de Programaciones Vigentes.
				htResult = tuxGlobal.web_red( tramaentrada );
				coderror = ( String ) htResult.get( "BUFFER" );

				if (Global.NIVEL_TRACE>1)
					EIGlobal.mensajePorTrace( "CRegCon.java > Consulta > CODIGO ERROR = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);

				if (coderror.substring(0,8).equals("NORM0000"))
				{
					String[] Salida = desentramaC( "4@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( Salida[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Consulta > Salida > "+Salida[indice], EIGlobal.NivelLog.INFO);
					if(Salida[indice].length()>0 )
						{
							if (indice == 3)
							{
								CodigoPath = Salida[indice];
							}
							if (indice == 4)
							{
								CodigoArchivo = Salida[indice];
							}
						EIGlobal.mensajePorTrace("CRegCon.java > Consulta > Salida > "+CodigoPath+","+CodigoArchivo, EIGlobal.NivelLog.INFO);

						}//if
					}//for
					bandera = true;
					CodigoMensajeB = "1";
					codigoEE1 = "";
					codigoXX1 = "";
				}
				else if ("NORM0013".equals(coderror.substring(0,8))) //else de OK
				{
					String[] Salida = desentramaC( "4@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( Salida[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Consulta > Salida > "+Salida[indice], EIGlobal.NivelLog.INFO);
					if(Salida[indice].length()>0 )
						{
							if (indice == 2)
							{
								CodigoMensaje = Salida[indice];
							}
						EIGlobal.mensajePorTrace("CRegCon.java > Consulta > Salida > "+CodigoMensaje, EIGlobal.NivelLog.INFO);

						}//if
					}//for
					bandera = false;
					CodigoMensajeB = "2";
					codigoEE1 = "La cuenta no esta asociada a una inversi�n vista";
					codigoEE1=codigoEE1.replace(' ', '_');
					codigoXX1 = "6";

				}//fin else consulta
				else if (coderror.substring(0,8).equals("NORM0502")) //else de OK
				{
					String[] Salida = desentramaC( "4@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( Salida[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Consulta > Salida > "+Salida[indice], EIGlobal.NivelLog.INFO);
					if(Salida[indice].length()>0 )
						{
							if (indice == 2)
							{
								CodigoMensaje = Salida[indice];
							}
						EIGlobal.mensajePorTrace("CRegCon.java > Consulta > Salida > "+CodigoMensaje, EIGlobal.NivelLog.INFO);

						}//if
					}//for
					bandera = false;
					CodigoMensajeB = "2";
					codigoEE1 = CodigoMensaje;
					codigoEE1=codigoEE1.replace(' ', '_');
					codigoXX1 = "6";

				}//fin
				else
				{
					CodigoMensaje = "Servicio no Disponible por el Momento.";
					bandera = false;
					CodigoMensajeB = "2";
					codigoEE1 = CodigoMensaje;
					codigoEE1=codigoEE1.replace(' ', '_');
					codigoXX1 = "6";

				}//fin else consulta
			} catch( Exception e ){
				/*despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP,
						"Consulta de Saldos de cuentas de cheques", "", req, res ); */
			}

			EIGlobal.mensajePorTrace( "CRegCon.java > Consulta > ARMA", EIGlobal.NivelLog.DEBUG);
			req.setAttribute(MENUPRINCIPAL, session.getStrMenu());
			req.setAttribute(NEWMENU, session.getFuncionesDeMenu());
			req.setAttribute(ENCABEZADO, CreaEncabezado("Alta de Consulta de Programaciones","Consultas &gt; Movimientos &gt; Chequeras &gt; Programados  &gt; Registros","s36010h",req));
			req.setAttribute("codigoM",CodigoMensajeB);
			req.setAttribute("codigoA",CodigoArchivo);
			req.setAttribute("codigoP",CodigoPath);
			req.setAttribute("codigoE",codigoEE);
			req.setAttribute("codigoF",codigoFF);
			req.setAttribute("codigoX",codigoXX);
			req.setAttribute("codigoE1",codigoEE1);
			req.setAttribute("codigoX1",codigoXX1);
			req.setAttribute("can",canV);
			req.setAttribute("mensaje",menV);
			req.setAttribute("tot",tot);
			req.setAttribute("ant",ant);
			req.setAttribute("sig",sig);
			EIGlobal.mensajePorTrace( "CRegCon.java > Consulta > ARMA 1 > "+bandera, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "++++++2 E="+codigoEE+",F="+codigoFF+",X="+codigoXX, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "++++++2 E="+canV+",F="+menV, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "++++++3 T="+tot+",A="+ant+",S="+sig, EIGlobal.NivelLog.DEBUG);
			//TODO CU1091
		    /*
			 * 05/ENE/07
			 * VSWF-BMB-I
			 */
			  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_REGISTRO_TUX_PROG_VIG);
				if(session.getContractNumber()!=null){
					bt.setContrato(session.getContractNumber());
				}
				if(coderror!=null){
					if(coderror.substring(0,2).equals("OK")){
						bt.setIdErr("CPRV0000");
					}else if(coderror.length()>8){
						bt.setIdErr(coderror.substring(0,8));
					}else{
						bt.setIdErr(coderror.trim());
					}
				}
				bt.setServTransTux("CPRV");
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
			  }
		    /*
			 * VSWF-BMB-F
			 */

		   }//fin else prog1

			if (CodigoMensajeB.equals("2") && funcion_alta.equals("0")) /**  SI NO TIENE PROGRAMACIONES VIGENTES **/
			{
				evalTemplate( IEnlace.C_CONS_PROG, req, res );
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta > FALSE", EIGlobal.NivelLog.DEBUG);
			}
			if ("1".equals(CodigoMensajeB) && "0".equals(funcion_alta)) /**  SI TIENE PROGRAMACIONES VIGENTES **/
			{
				//
				evalTemplate( IEnlace.C_PROG_VIG, req, res );
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta > TRUE", EIGlobal.NivelLog.DEBUG);
			}
			else if ("1".equals(funcion_alta) || "2".equals(funcion_alta))   /**  PANTALLA DE ALTA DE PROGRAMACIONES **/
			{
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta > ALTA", EIGlobal.NivelLog.DEBUG);

				 boolean sesionvalida = SesionValida ( req, res );

				//actualizacion de datos
				String gperiodo = "";
				String ghorario = "";
				String gtipomov = "";
				String gChksvc = "";
				String gChkvista = "";
				String gimporte = "";
				String gfecha1 = "";
				String gformato = "";
				String greporte = "";
				String gcanal = "";
				String gdiasemana = "";
				String emails = "";

				 if (! sesionvalida ) { return; }

				if (funcion_alta.equals("2"))
				{
					codigoEE = (String)req.getParameter(CODIGOEE);
					codigoEE = codigoEE.replace('_', ' ');
					codigoXX = (String)req.getParameter(CODIGOXX);
					codigoXX = codigoXX.replace('_', ' ');
					EIGlobal.mensajePorTrace( "***************************** "+codigoEE+","+codigoXX, EIGlobal.NivelLog.DEBUG);
					if (codigoEE==null)
					{codigoEE="";}
					if (codigoXX==null)
					{codigoXX="";}
				}

		         int total=0;

	            EIGlobal.mensajePorTrace ( "***CRegCon todos nullos", EIGlobal.NivelLog.DEBUG);
				try{
				     EIGlobal.mensajePorTrace("el userID: "+session.getUserID8(), EIGlobal.NivelLog.INFO);
				   } catch(Exception e) {
					                      e.printStackTrace();
				                        }
				EIGlobal.mensajePorTrace("Tomamos las cuentas", EIGlobal.NivelLog.DEBUG);
	            llamado_servicioCtasInteg1 (IEnlace.MNorma43," "," "," ",session.getUserID8 ()+"N43.ambci",req);
	            total=getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+"N43.ambci");

		         log ("CRegCon.java::defaultAction()");
		         try{
		         req.setAttribute (MENUPRINCIPAL, session.getStrMenu ());
		         req.setAttribute (NEWMENU, session.getFuncionesDeMenu ());
		         req.setAttribute (ENCABEZADO, CreaEncabezado ( "Registro de Programaciones de Consulta de Movimientos",
		         "Consultas &gt; Movimientos &gt; Chequeras &gt; Programados &gt; Registro", "s36010h",req ) );
				 } catch(Exception e) { e.printStackTrace();}

		        EIGlobal.mensajePorTrace( "\n\n\t\tsession distinto a null:  " + (session != null), EIGlobal.NivelLog.DEBUG);
		         if(session.getFacultad ( session.FAC_CONSULTA_SALDO )) {

		            indice = 1;
		            String strSalida2 = "";
		            String Contrato = session.getContractNumber ();
					EIGlobal.mensajePorTrace("CRegCon  contrato = "+Contrato, EIGlobal.NivelLog.DEBUG);

					String[][] arrayCuentas = ObteniendoCtasInteg1 (0,total,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+"N43.ambci");
		            //***********************************************
		            int residuo = 0;
					String monedaN = "";
					String monedaN1 = "";
					String descN = "";
					String descN1 = "";
		            int contador=1;
					int indice2 = 1;
		            for (indice2 = 1;indice2<=Integer.parseInt (arrayCuentas[0][0]);indice2++){
		                contador++;
		                residuo = contador % 2;

						if ("MXP".equals(arrayCuentas[indice2][9]))
						{monedaN = "MN";
						 monedaN1 = "MN&nbsp;&nbsp;";}
						else
						{monedaN = "USD";
						 monedaN1 = "USD&nbsp;";}
						if (arrayCuentas[indice2][9].equals("####"))
						{monedaN = "_";
						 monedaN1 = "_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}
						if(arrayCuentas[indice2][4].equals("") || arrayCuentas[indice2][4]==null)
						{
							descN="_";
							descN1="_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						}
						else
						{
							descN=arrayCuentas[indice2][4];
							descN1=arrayCuentas[indice2][4];
						}

		                /*if (residuo == 0) {
							//strSalida2 = strSalida2 + "<tr  bgcolor=#CCCCCC><td align=right class=textabdatobs><input type=checkbox name=cta onclick=val_todas(); value=\""+arrayCuentas[indice2][1] +"$"+arrayCuentas[indice2][4]+"$"+monedaN+"$" +"\"></td><td class=textabdatobs>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatobs>&nbsp;"+descN+"</td><td class=textabdatobs>&nbsp;"+monedaN+"</td></tr>";
							//strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracter(arrayCuentas[indice2][1],'0',16,true)+"&nbsp;&nbsp;"+descN1+"</option>";
							//modificacion 03/12/2003
							strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracterHTML(arrayCuentas[indice2][1],16)+"&nbsp;&nbsp;"+descN1+"</option>";
							strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracterHTML(arrayCuentas[indice2][1],16)+"&nbsp;&nbsp;"+descN1+"</option>";
		                } else {
							//strSalida2 = strSalida2 + "<tr  bgcolor=#EBEBEB><td align=right class=textabdatcla><input type=checkbox name=cta onclick=val_todas(); value=\""+arrayCuentas[indice2][1] +"$"+arrayCuentas[indice2][4]+"$"+monedaN+"$" +"\"></td><td class=textabdatcla>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatcla>&nbsp;"+descN+"</td><td class=textabdatobs>&nbsp;"+monedaN+"</td></tr>";
							//strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracter(arrayCuentas[indice2][1],'0',16,true)+"&nbsp;&nbsp;"+descN1+"</option>";
		                }  */
		                strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracterHTML(arrayCuentas[indice2][1],16)+"&nbsp;&nbsp;"+descN1+"</option>";
		            }//for
		            EIGlobal.mensajePorTrace("CRegCon  lado = "+lado+"<", EIGlobal.NivelLog.DEBUG);
		           if ("1".equals(lado)){
		                 req.setAttribute ("cuentas1",strSalida2);
		                 req.setAttribute (CUENTAS,"");
		            }
		            else {
		            	req.setAttribute (CUENTAS,strSalida2);
		            	req.setAttribute ("cuentas1","");
		            }


					String diasInhabiles = diasInhabilesAnt ();
					String strInhabiles = armaDiasInhabilesJS();
					String datesrvr = ObtenDia() + "-" + ObtenMes() + "-" + ObtenAnio();

		  		    req.setAttribute ("DiasInhabiles",diasInhabiles);
					req.setAttribute ("diasInhabiles", strInhabiles);
					req.setAttribute ("Movfechas",datesrvr);

					//Obtiene el siguiente dia habil
					String SigDia = "";
					EIGlobal.mensajePorTrace( "++++++1 Dias Inhabiles="+strInhabiles, EIGlobal.NivelLog.DEBUG);
					strInhabiles = strInhabiles + ",25/12/2000";
					SigDia = ObtenSigDiaHabil(strInhabiles);
					if (!SigDia.substring(2,3).equals("/"))
					{
						SigDia = "0" + SigDia;
					}
					if (!"/".equals(SigDia.substring(5,6)))
					{
						SigDia = SigDia.substring(0,2)+ "0" + SigDia.substring(3,8);
					}
					EIGlobal.mensajePorTrace ( "***CRegCon : Siguiente Habil :"+SigDia, EIGlobal.NivelLog.DEBUG);
					if (!actualiza_valor.equals("5"))
					{
						req.setAttribute ("gfecha1",SigDia);
					}
					else
					 {
						req.setAttribute ("gfecha1",gfecha1);
					 }
					//Obtiene el dia 90 natural
					String dia90 = Cargar90(SigDia);
					req.setAttribute ("gdia90",dia90);
					EIGlobal.mensajePorTrace ( "***CRegCon : dia 90 :"+dia90, EIGlobal.NivelLog.DEBUG);

					//actualiza valores
					req.setAttribute ("actualiza_valor",actualiza_valor);
					if (actualiza_valor.equals("5"))
					{
					req.setAttribute ("gperiodo",gperiodo);
					req.setAttribute ("ghorario",ghorario);
					req.setAttribute ("gtipomov",gtipomov);
					if(gChksvc==null)
					{gChksvc = "";}
					if(gChkvista==null)
					{gChkvista = "";}
					if(gimporte==null)
					{gimporte = "";}
					if(emails==null)
					{emails = "";}
					req.setAttribute ("gChksvc",gChksvc);
					req.setAttribute ("gChkvista",gChkvista);
					req.setAttribute ("gimporte",gimporte);
					req.setAttribute ("gformato",gformato);
					req.setAttribute ("greporte",greporte);
					req.setAttribute ("gcanal",gcanal);
					req.setAttribute ("gdiasemana",gdiasemana);
					req.setAttribute ("gemails",emails);
					EIGlobal.mensajePorTrace( "***************************** 2 "+codigoEE+","+codigoXX, EIGlobal.NivelLog.DEBUG);
					}//fin if 5

					req.setAttribute (CODIGOEE,codigoEE);
					req.setAttribute (CODIGOXX,codigoXX);

					sess.setAttribute(CUENTAS,strSalida2);

		            //***********************************************
					EIGlobal.mensajePorTrace("***csaldo1 REGISTRO = " +funcion_llamar, EIGlobal.NivelLog.DEBUG);
					evalTemplate ( IEnlace.C_SALDO_TMPL1, req, res);

		         }else {

		            String laDireccion = "document.location='SinFacultades'";
		            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
		            res.setContentType(TEXTHTML);
		            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

				 }//fin else 1
			}// fin else alta
		 }// fin vig
		 else
		 {
            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
            res.setContentType(TEXTHTML);
            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
		 }// fin facultad de programaciones vigentes

		}//fin if Inicio
		 else if  (funcion_llamar.equals("2")) //inicio ALTA REGISTRO   /*********************************************/
		{

		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***INICIA CRegCon.java > Alta", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);

		int indice = 0;

		String tramaentrada = "";
		String tramaentrada2 = "";
		String coderror = "";
		String cuentacompleta = "";
		String fechaexpt = "";
		String CodigoMensaje = "";
		String CodigoMensajeB = "";
		String CodigoFolio = "";
		String codigoEE = "";
        String codigoFF = "";
		String codigoXX = "";

		boolean sesionvalida1 = SesionValida ( req, res );
		if (! sesionvalida1 ) { return; }

		if(session.getFacultad ( session.FAC_ALPROGCONSMOV_N43 ))
		{
			String cta_saldo;
			String emails;
			String fileOut="";
			String sessionIdN43 = (String) req.getSession().getId();
			fileOut = session.getUserID8() + sessionIdN43 + ".ctas";

			codigoEE = (String)req.getParameter(CODIGOEE);
			codigoFF = (String)req.getParameter(CODIGOFF);
			codigoXX = (String)req.getParameter(CODIGOXX);
			EIGlobal.mensajePorTrace( "++++++3 E="+codigoEE+",F="+codigoFF+",X="+codigoXX, EIGlobal.NivelLog.INFO);
			if (codigoEE==null)
			{codigoEE="";}
			if (codigoFF==null)
			{codigoFF="";}
			if (codigoXX==null)
			{codigoXX="1";}

			// Se obtienen par�metros de Norma 43
			String gperiodo = (String)req.getParameter("gperiodo1");
			String ghorario = (String)req.getParameter("ghorario1");
			String gtipomov = (String)req.getParameter("gtipomov1");
			String gChksvc = (String)req.getParameter("gChksvc");
			String gChkvista = (String)req.getParameter("gChkvista");
			String gimporte = (String)req.getParameter("gimporte1");
			String gfecha1 = (String)req.getParameter("gfecha2");
			String gformato = (String)req.getParameter("gformato1");
			String greporte = (String )req.getParameter("greporte1");
			String gcanal = (String)req.getParameter("gcanal1");
			String gdiasemana = (String)req.getParameter("gdiasemana1");

			String m = ( String ) req.getParameter( "gj" ); //Numero de Cuentas
			String mm = ( String ) req.getParameter( "gjj" ); //Numero de emails
			emails = ( String ) req.getParameter( "gemail" );

			EIGlobal.mensajePorTrace("CRegCon.java > Alta > Parametros > "+gperiodo+","+ghorario+","+gtipomov+","+gChksvc+","+gChkvista+","+gimporte+","+gfecha1+","+gformato+","+greporte+","+gcanal+",", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("CRegCon.java > Alta > Parametros > "+mm+","+emails, EIGlobal.NivelLog.DEBUG);
			gfecha1 = gfecha1.substring(0,2)+"-"+gfecha1.substring(3,5)+"-"+gfecha1.substring(6,10);

			//Arma trama de alta de Consulta Programada
			tramaentrada2 = session.getContractNumber() + "@" + session.getUserID8() + "@" + gperiodo + "@";
			if("2".equals(gperiodo))
			{
				if(gdiasemana.equals("1"))
				{tramaentrada2 = tramaentrada2  + "LUNES@";}
				if(gdiasemana.equals("2"))
				{tramaentrada2 = tramaentrada2  + "MARTES@";}
				if(gdiasemana.equals("3"))
				{tramaentrada2 = tramaentrada2  + "MIERCOLES@";}
				if(gdiasemana.equals("4"))
				{tramaentrada2 = tramaentrada2  + "JUEVES@";}
				if(gdiasemana.equals("5"))
				{tramaentrada2 = tramaentrada2  + "VIERNES@";}
			}
			else
			{tramaentrada2 = tramaentrada2  + "_@";}
			tramaentrada2 = tramaentrada2  + ghorario + "@" + gtipomov + "@";
			if(gChksvc!=null)
			{tramaentrada2 = tramaentrada2  + "0@";}
			else
			{tramaentrada2 = tramaentrada2  + "1@";}
			if(gChkvista!=null)
			{tramaentrada2 = tramaentrada2  + "0@";}
			else
			{tramaentrada2 = tramaentrada2  + "1@";}
			if(gimporte.equals(""))
			{tramaentrada2 = tramaentrada2  + "_@";}
			else
			{tramaentrada2 = tramaentrada2  + gimporte + "@";}
			tramaentrada2 = tramaentrada2  + gfecha1 + "@";
			if(gformato.equals("0"))
			{tramaentrada2 = tramaentrada2  + "TCT@";}
			else
			{tramaentrada2 = tramaentrada2  + "N43@";}
			tramaentrada2 = tramaentrada2  + greporte + "@" + gcanal + "@";
			tramaentrada2 = tramaentrada2 + m + "@";
			if(!mm.equals("0"))
			{tramaentrada2 = tramaentrada2 + mm + "@" + Global.N43_PATH_INTERNET  + "/" + fileOut + "@";}
			else
			{tramaentrada2 = tramaentrada2 + "_@" + Global.N43_PATH_INTERNET + "/" + fileOut + "@";}

			//Obtiene las cuentas en el formato NoCta@Descripcion@Moneda.
			//Todas las Cuentas son separadas por caracter "," (coma).
			//Ej: 82500005708@GRUPO BANCA ELECTRONICA@MN@,82500058464@ABENGOA MEXICO SA DE CV@MN@
			cta_saldo = ( String ) req.getParameter( "gcta_saldo" );
			cta_saldo=cta_saldo.replace('-', ' ');
			String[] arraySaldos = new String[2];
			boolean grabaArchivo = true;
			boolean bandera		   = false;

			//Se insertan las cuentas en un Arreglo de Strings. Cada cuenta en un
			//espacio del arreglo.
			EIGlobal.mensajePorTrace("CRegCon.java > Alta > m "+m+"   cta_saldo "+cta_saldo, EIGlobal.NivelLog.DEBUG);
			String[] Carreglo = desentramaC( m + "," + cta_saldo,',');
			String[] Carreglo1 = new String[6];
			//Se insertan los emails en un Arreglo de Strings. Cada email en un
			//espacio del arreglo.
			if(!mm.equals("0"))
			{
				emails = emails + ",";
				EIGlobal.mensajePorTrace("CRegCon.java > Alta > m "+mm+"   emails "+emails, EIGlobal.NivelLog.DEBUG);
				Carreglo1 = desentramaC( mm + "," + emails,',');
			}

			EI_Exportar ArcSal;
			ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			EIGlobal.mensajePorTrace("CRegCon.java > Alta > PATH > "+IEnlace.DOWNLOAD_PATH + fileOut, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***Se verificar� que el archivo no exista en caso contrario se borrara y crear� uno nuevo", EIGlobal.NivelLog.DEBUG);
			ArcSal.creaArchivo();

			Hashtable htResult;

			try
				{
				//Ciclo que recorre arreglo de cuentas para escribir en el ARCHIVO.
				for (indice = 1; indice <= Integer.parseInt( Carreglo[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Alta > Carreglo "+Carreglo[indice], EIGlobal.NivelLog.DEBUG);
					if(Carreglo[indice].length()>0 )
						{
						cuentacompleta = "1$"+Carreglo[indice];
						EIGlobal.mensajePorTrace("CRegCon.java > Alta > Escritura > cuentacompleta : "+cuentacompleta, EIGlobal.NivelLog.INFO);

						if(grabaArchivo){
								ArcSal.escribeLinea(cuentacompleta + "\r\n");
								EIGlobal.mensajePorTrace( "***CRegCon.java > Alta > Se escribio en el archivo  "+cuentacompleta, EIGlobal.NivelLog.INFO);
							}//fin
						}//if
					}//for

				if(!mm.equals("0"))
				{
					//Ciclo que recorre arreglo de emails para escribir en el ARCHIVO.
					for (indice = 1; indice <= Integer.parseInt( Carreglo1[0].trim() ); indice++ )
					{
						EIGlobal.mensajePorTrace("CRegCon.java > Alta > Carreglo1 "+Carreglo1[indice], EIGlobal.NivelLog.DEBUG);
						if(Carreglo1[indice].length()>0 )
						{
							cuentacompleta = "2$"+Carreglo1[indice]+"$";
							EIGlobal.mensajePorTrace("CRegCon.java > Alta > Escritura > cuentacompleta : "+cuentacompleta, EIGlobal.NivelLog.DEBUG);

							if(grabaArchivo){
								ArcSal.escribeLinea(cuentacompleta + "\r\n");
								EIGlobal.mensajePorTrace( "***CRegCon.java > Alta > Se enscribio en el archivo  "+cuentacompleta, EIGlobal.NivelLog.DEBUG);
							}//fin
						}//if
					}//for
				}//fin if emails

				if( grabaArchivo )
					ArcSal.cierraArchivo();

				ArchivoRemoto archR = new ArchivoRemoto();
				if(!archR.copiaN43(fileOut,Global.N43_PATH_INTERNET))
				{
					EIGlobal.mensajePorTrace("***movimientos.class No se pudo copiar archivo remoto:"+IEnlace.REMOTO_TMP_DIR+"/"+session.getUserID8(), EIGlobal.NivelLog.ERROR);
				}
				else
				{
					  EIGlobal.mensajePorTrace("***movimientos.class archivo remoto copiado:"+IEnlace.REMOTO_TMP_DIR+"/"+session.getUserID8(), EIGlobal.NivelLog.DEBUG);
				}//fin else remoto

				// LLAMADO AL SERVICIO TUXEDO
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				tramaentrada = "1EWEB|" + session.getUserID8() + "|APRM|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
				tramaentrada = tramaentrada + tramaentrada2 + "|";

				//Llama a la Alta de Consulta de Programaciones.
				htResult = tuxGlobal.web_red( tramaentrada );
				coderror = ( String ) htResult.get( "BUFFER" );

				if (Global.NIVEL_TRACE>1)
					EIGlobal.mensajePorTrace( "CRegCon.java > Alta > CODIGO ERROR = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);

				fechaexpt = ObtenFecha(true);
				fechaexpt = fechaexpt.substring(0,2)+fechaexpt.substring(3,5)+fechaexpt.substring(6,fechaexpt.length());

				if (coderror.substring(0,8).equals("NORM0000"))
				{
					bandera = true;

					String[] Salida = desentramaC( "3@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( Salida[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Alta > Salida > "+Salida[indice], EIGlobal.NivelLog.INFO);
					if(Salida[indice].length()>0 )
						{
							if (indice == 2)
							{
								CodigoMensaje = Salida[indice];
								if (CodigoMensaje.equals("Transaccion Exitosa"))
								{
									CodigoMensaje = "Programaci&oacute;n de Consulta de Movimientos Exitosa";
								}
							}
							if (indice == 3)
							{
								CodigoFolio = Salida[indice];
							}
						codigoEE = CodigoMensaje;
						codigoEE = codigoEE.replace(' ', '_');
				        codigoFF = CodigoFolio;
						codigoXX = "5";
						EIGlobal.mensajePorTrace("CRegCon.java > Alta > Salida > "+CodigoMensaje+","+CodigoFolio, EIGlobal.NivelLog.INFO);

						}//if
					}//for

					EIGlobal.mensajePorTrace( "CRegCon.java > OK > BROWSER = " +CodigoMensaje+ ", "+CodigoFolio, EIGlobal.NivelLog.INFO);
					CodigoMensajeB = "1";
				}
				else if (coderror.substring(0,8).equals("NORM0013")) //else de OK
				{
					bandera = true;

					String[] Salida = desentramaC( "3@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( Salida[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Alta > Salida Error > "+Salida[indice], EIGlobal.NivelLog.INFO);
					if(Salida[indice].length()>0 )
						{
							if (indice == 2)
							{
								CodigoMensaje = Salida[indice];
							}
							if (indice == 3)
							{
								CodigoFolio = Salida[indice];
							}
						codigoEE = "La cuenta no esta asociada a una inversi�n vista";
						codigoEE = codigoEE.replace(' ', '_');
				        codigoFF = CodigoFolio;
						codigoXX = "5";
						EIGlobal.mensajePorTrace("CRegCon.java > Alta > Salida Error > "+CodigoMensaje+","+CodigoFolio, EIGlobal.NivelLog.INFO);

						}//if
					}//for

					EIGlobal.mensajePorTrace( "CRegCon.java > OK > BROWSER = " +CodigoMensaje+ ", "+CodigoFolio, EIGlobal.NivelLog.INFO);
					CodigoMensajeB = "2";

				}//fin if OK
				else
				{
					CodigoMensaje = "Servicio no Disponible por el Momento.";
					CodigoFolio = "";
					bandera = false;
					CodigoMensajeB = "2";
					codigoEE = CodigoMensaje;
					codigoEE = codigoEE.replace(' ', '_');
			        codigoFF = CodigoFolio;
					codigoXX = "5";

				}//fin else consulta

			} catch( Exception e ){
				/*despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP,
						"Consulta de Saldos de cuentas de cheques", "", req, res ); */
			}

			req.setAttribute(MENUPRINCIPAL, session.getStrMenu());
			req.setAttribute(NEWMENU, session.getFuncionesDeMenu());
			req.setAttribute(ENCABEZADO, CreaEncabezado("Alta de Consulta de Programaciones","Consultas &gt; Movimientos &gt; Chequeras &gt; Programados  &gt; Registros","s36010h",req));
			req.setAttribute("codigoM",CodigoMensajeB);
			req.setAttribute("codigoE",codigoEE);
			req.setAttribute("codigoF",codigoFF);
			req.setAttribute("codigoX",codigoXX);
			EIGlobal.mensajePorTrace( "++++++4 E="+codigoEE+",F="+codigoFF+",X="+codigoXX, EIGlobal.NivelLog.INFO);
			//TODO CU1091
		    /*
			 * 05/ENE/07
			 * VSWF-BMB-I
			 */
			  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				if(gimporte==null||gimporte.trim().equals("")){
					gimporte="0.00";
				}
			    BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_REGISTRO_ALTA_REG_CONS_PROG);
				if(session.getContractNumber()!=null){
					bt.setContrato(session.getContractNumber());
				}
				if(coderror!=null){
					if(coderror.substring(0,2).equals("OK")){
						bt.setIdErr("APRM0000");
					}
					else if(coderror.length()>8){
						bt.setIdErr(coderror.substring(0,8));
					}else{
						bt.setIdErr(coderror.trim());
					}
				}
				bt.setImporte(Double.parseDouble(gimporte));
				bt.setServTransTux("APRM");
				bt.setEstatus("0");
				bt.setFechaProgramada(Utilerias.MdyToDate(gfecha1.replace('-', '/')));
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
			  }
		    /*
			 * VSWF-BMB-F
			 */
			evalTemplate( IEnlace.C_ALTA_CON, req, res );

		}//fin Alta
		else
		{
            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
            res.setContentType(TEXTHTML);
            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
		 }//fin alta de programaciones

		}//fin else Alta
		else if  ("3".equals(funcion_llamar)) //inicio MODIFICA   /*********************************************/
		{
			 // <vswf RRG cambio nombre de la variable enum a enum1 05122008>
		 java.util.Enumeration enum1 = sess.getAttributeNames() ;
		 while(enum1.hasMoreElements() ) {
			try{
				EIGlobal.mensajePorTrace((String)enum1.nextElement(), EIGlobal.NivelLog.DEBUG);
			}catch(Exception e) {}
		 }

		 BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		// Se obtienen par�metros para Norma 43
		int numCtasFolio = 0;//vulne
		String gperiodo = "";
		String ghorario = "";
		String gtipomov = "";
		String gChksvc = "";
		String gChkvista = "";
		String gimporte = "";
		String gfecha1 = "";
		String gformato = "";
		String greporte = "";
		String gcanal = "";
		String gdiasemana = "";
		String garchivo = "";
		String gpath = "";

		String gcuentas = ""; //Cuentas Concatenadas con ","
		String gmodifolio = "";
		String gemails = "";
		String sessionIdN43 = (String) req.getSession().getId();
		String CodigoMensajeB = "1";
		String codigoEE = "";
        String codigoFF = "";
		String codigoXX = "";
		String gcta_saldo_sig1 = "";
		String gj_sig1 = "0";
		int gj_sig2 = 0;

		String tramaentrada = "";
		boolean bandera = false;
		Hashtable htResult;
		String coderror = "";
		String CodigoMensaje = "";
		String CodigoFolio = "";
		String Vigfolio = ( String ) req.getParameter( "Vigfolio" );
		req.setAttribute("Vigfolio",Vigfolio);

		String lado = ( String ) req.getParameter("Lado");
		if (lado == null){
		    lado ="0";
		}
		int indice = 1;

		boolean sesionvalida1 = SesionValida ( req, res );
		if (! sesionvalida1 ) { return; }

		 if(session ==null)
			EIGlobal.mensajePorTrace(TRAZALOGSESION, EIGlobal.NivelLog.DEBUG);

		if(session.getFacultad ( session.FAC_MODPROGMOV_N43 ))
		{
		   if (actualiza_valor.equals("0"))
		   {
			/******************************************************************/
			/* LLAMADO AL SERVICIO DE CONSULTA DE MODIFICACION PROGRAMACIONES */
			/******************************************************************/
			try
			{
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Modificacion", EIGlobal.NivelLog.DEBUG);
				// LLAMADO AL SERVICIO TUXEDO
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				tramaentrada = "2EWEB|" + session.getUserID8() + "|CMPR|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
				tramaentrada = tramaentrada + Vigfolio + "@" + sessionIdN43 + "@|";
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Modificacion > " + tramaentrada, EIGlobal.NivelLog.INFO);

				//Llama a la consulta de Modificaciones
				htResult = tuxGlobal.web_red( tramaentrada );
				coderror = ( String ) htResult.get( "BUFFER" );

				if (Global.NIVEL_TRACE>1)
					EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Mod > Salida = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);

				if (coderror.substring(0,8).equals("NORM0000"))
				{
					String[] SalidaCorrecta = desentramaC( "18@" + coderror,'@');

					//Ciclo que recorre arreglo de SalidaCorrecta de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( SalidaCorrecta[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Consulta Mod > Salida > "+SalidaCorrecta[indice], EIGlobal.NivelLog.INFO);
					if(SalidaCorrecta[indice].length()>0 )
						{
							if (indice == 3)
							{gmodifolio = SalidaCorrecta[indice];}
							if (indice == 6)
							{gperiodo = SalidaCorrecta[indice];}
							if (indice == 7)
							{
								gdiasemana = SalidaCorrecta[indice];

								if(!gdiasemana.equals("_"))
								{
									if(gdiasemana.equals("LUNES"))
									{gdiasemana = "1";}
									if(gdiasemana.equals("MARTES"))
									{gdiasemana = "2";}
									if(gdiasemana.equals("MIERCOLES"))
									{gdiasemana = "3";}
									if(gdiasemana.equals("JUEVES"))
									{gdiasemana = "4";}
									if(gdiasemana.equals("VIERNES"))
									{gdiasemana = "5";}
								}//fin if
								else
								{
									gdiasemana = "1";
								}
							}
							if (indice == 8)
							{ghorario = SalidaCorrecta[indice];}
							if (indice == 9)
							{gtipomov = SalidaCorrecta[indice];}
							if (indice == 10)
							{gChksvc = SalidaCorrecta[indice];}
							if (indice == 11)
							{gChkvista = SalidaCorrecta[indice];}
							if (indice == 12)
							{
								if (SalidaCorrecta[indice].equals("_"))
								{
									gimporte = "";
								}
								else
								{
									gimporte = SalidaCorrecta[indice];
								}
							}
							if (indice == 13)
							{gfecha1 = SalidaCorrecta[indice];}
							if (indice == 14)
							{
								gformato = SalidaCorrecta[indice];
								if("TCT".equals(gformato))
								{gformato = "0";}
								else
								{gformato = "1";}
							}
							if (indice == 15)
							{greporte = SalidaCorrecta[indice];}
							if (indice == 16)
							{gcanal = SalidaCorrecta[indice];}
							if (indice == 17)
							{gpath = SalidaCorrecta[indice];}
							if (indice == 18)
							{garchivo = SalidaCorrecta[indice];}
						}//if
					}//for

					bandera = true;
					CodigoMensajeB = "1";
					codigoEE = "";
			        codigoFF = "";
					codigoXX = "";
				}
				else if (coderror.substring(0,8).equals("NORMXXXX")) //else de OK
				{
					String[] SalidaIn = desentramaC( "3@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( SalidaIn[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Consulta In > Salida > "+SalidaIn[indice], EIGlobal.NivelLog.INFO);
					if(SalidaIn[indice].length()>0 )
						{
							if (indice == 2)
							{CodigoMensaje = SalidaIn[indice];}
							if (indice == 3)
							{CodigoFolio = SalidaIn[indice];}
						EIGlobal.mensajePorTrace("CRegCon.java > Consulta In > Salida > "+CodigoMensaje+","+CodigoFolio, EIGlobal.NivelLog.INFO);
						}//if
					}//for
					bandera = false;
					CodigoMensajeB = "2";
					codigoEE = CodigoMensaje;
					codigoEE = codigoEE.replace(' ', '_');
			        codigoFF = CodigoFolio;
					codigoXX = "5";

				}//fin else consulta
				else
				{
					CodigoMensaje = "Servicio no Disponible por el Momento.";
					CodigoFolio = "";
					bandera = false;
					CodigoMensajeB = "2";
					codigoEE = CodigoMensaje;
					codigoEE = codigoEE.replace(' ', '_');
			        codigoFF = CodigoFolio;
					codigoXX = "5";

				}//fin else consulta
			} catch( Exception e ){
				/*despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP,
						"Consulta de Saldos de cuentas de cheques", "", req, res ); */
			}//fin catch

			EIGlobal.mensajePorTrace( "CRegCon.java > Consulta  Modifica > ARMA", EIGlobal.NivelLog.DEBUG);
			req.setAttribute(MENUPRINCIPAL, session.getStrMenu());
			req.setAttribute(NEWMENU, session.getFuncionesDeMenu());
			req.setAttribute(ENCABEZADO, CreaEncabezado("Modificaci&oacute;n de Programaciones","Consultas &gt; Movimientos &gt; Chequeras &gt; Programados  &gt; Registros &gt; Modificaci&oacute;n","s36020h",req));
			req.setAttribute("codigoM",CodigoMensajeB);
			req.setAttribute("codigoE",CodigoMensaje);
			req.setAttribute("codigoF",CodigoFolio);
			req.setAttribute(CODIGOEE,codigoEE);
			req.setAttribute(CODIGOFF,codigoFF);
			req.setAttribute(CODIGOXX,codigoXX);
		   }//fin actualiza

			if (CodigoMensajeB.equals("2")) /**  SI FALLO LA CONSULTA DE MODIFICACIONES **/
			{
				evalTemplate( IEnlace.C_MODI_ERR, req, res );
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Modifica > FALSE", EIGlobal.NivelLog.DEBUG);
			}
			else   /**  PANTALLA DE MODIFICACION DE PROGRAMACIONES **/
			{
				 int conta = 0;
				 boolean sesionvalida = SesionValida ( req, res );
				 if (! sesionvalida ) { return; }

		         int total=0;

		        EIGlobal.mensajePorTrace ( "***CRegCon todos nullos", EIGlobal.NivelLog.DEBUG);
				try{
				     EIGlobal.mensajePorTrace("el userID: "+session.getUserID8(), EIGlobal.NivelLog.INFO);
				   } catch(Exception e) {
					                      e.printStackTrace();
				                        }

				if ("0".equals(actualiza_valor))
				{
				EIGlobal.mensajePorTrace("Tomamos las cuentas de Consulta de Modificacion :"+garchivo+","+gpath, EIGlobal.NivelLog.INFO);

				ArchivoRemoto archR = new ArchivoRemoto();
				if(!archR.copiaN43Modi(garchivo,gpath)) //"/planbcae/procesos/Norma43/archivos_internet/"
				{
					EIGlobal.mensajePorTrace("***Consulta Modifica No se pudo copiar archivo remoto: "+garchivo, EIGlobal.NivelLog.ERROR);
				}
				else
				{
					  EIGlobal.mensajePorTrace("***Consulta Modifica archivo remoto copiado:"+garchivo, EIGlobal.NivelLog.DEBUG);
				}//fin else remoto

				EIGlobal.mensajePorTrace ("***Lee archivo de Cuentas y emails", EIGlobal.NivelLog.DEBUG);
				int j = 0;
				String linea = null;
				String Tipo = null;
				FileReader lee = null;
				String[] LineaCta = null;
				
				try{
					 lee= new FileReader (IEnlace.LOCAL_TMP_DIR + "/" + garchivo);
				     BufferedReader entrada= new BufferedReader (lee);
				     numCtasFolio = 0;//vulne
				     while((linea = entrada.readLine ())!=null) {
					 if ("".equals (linea.trim ())) {
					     linea = entrada.readLine ();
					 }
					 if (linea == null) {
					     break;
					 }
					 //desentrama la linea
					 Tipo = linea.substring(0, 1);
					 if (Tipo.equals("1"))
					 {
						 numCtasFolio++;//vulne
						 LineaCta = desentramaC( "4$" + linea,'$');
						 //Ciclo que recorre arreglo de Emails
						 for (indice = 1; indice <= Integer.parseInt( LineaCta[0].trim() ); indice++ )
						 {
						 EIGlobal.mensajePorTrace("CRegCon.java > Cuentas > Salida > "+LineaCta[indice], EIGlobal.NivelLog.INFO);
						 if(LineaCta[indice].length()>0 )
						 	{
											if (indice == 2) {
												gcuentas = gcuentas + LineaCta[indice] + ",";
												conta = conta + 1;
											}
									//Arma las cuentas anteriores
									if (indice == 2)
									{gcta_saldo_sig1 = gcta_saldo_sig1+LineaCta[indice]+"$";}
									if (indice == 3)
									{gcta_saldo_sig1 = gcta_saldo_sig1+LineaCta[indice]+"$";}
									if (indice == 4)
									{gcta_saldo_sig1 = gcta_saldo_sig1+LineaCta[indice]+"$,";
									 gj_sig2 = gj_sig2 + 1;}
								EIGlobal.mensajePorTrace("CRegCon.java > Cuentas > Salida > "+gcuentas, EIGlobal.NivelLog.INFO);
							}//if
						 }//for
					 }
					 else
					 {
						 LineaCta = desentramaC( "2$" + linea,'$');
						 //Ciclo que recorre arreglo de Emails
						 for (indice = 1; indice <= Integer.parseInt( LineaCta[0].trim() ); indice++ )
						 {
							 EIGlobal.mensajePorTrace("CRegCon.java > Emails > Salida > "+LineaCta[indice], EIGlobal.NivelLog.INFO);
						 if(LineaCta[indice].length()>0 )
						 	{
								if ((indice == 2) && (LineaCta[indice].contains("@")))
								{gemails = gemails + LineaCta[indice]+",";}
								EIGlobal.mensajePorTrace("CRegCon.java > Emails > Salida > "+gemails, EIGlobal.NivelLog.INFO);
							}//if
						 }//for
					 }

					 j++;
					 
				     }
				     entrada.close ();
				     
				} catch(Exception e) {
				     EIGlobal.mensajePorTrace (e.toString(), EIGlobal.NivelLog.ERROR);
				     e.printStackTrace ();
				} finally {
						 try{
							 lee.close();
						 }catch(Exception e) {
						 }
				}
				EIGlobal.mensajePorTrace ("***Termino de Desentramar archivo "+j, EIGlobal.NivelLog.DEBUG);
				}//fin actualiza
				
				
				EIGlobal.mensajePorTrace("Tomamos las cuentas", EIGlobal.NivelLog.DEBUG);
		        llamado_servicioCtasInteg1 (IEnlace.MConsulta_Saldos," "," "," ",session.getUserID8 ()+"N43.ambci",req);
		        total=getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+"N43.ambci");

		         log ("CRegCon.java::defaultAction()");
		         try{
		         req.setAttribute (MENUPRINCIPAL, session.getStrMenu ());
		         req.setAttribute (NEWMENU, session.getFuncionesDeMenu ());
		         req.setAttribute(ENCABEZADO, CreaEncabezado("Modificaci&oacute;n de Programaciones","Consultas &gt; Movimientos &gt; Chequeras &gt; Programados  &gt; Registros &gt; Modificaci&oacute;n","s36020h",req));

				 } catch(Exception e) { e.printStackTrace();}

		        EIGlobal.mensajePorTrace( "\n\n\t\tsession distinto a null:  " + (session != null), EIGlobal.NivelLog.DEBUG);
		         if(session.getFacultad ( session.FAC_CONSULTA_SALDO )) {

				    indice = 1;
		            String strSalida2 = "";
					String strSalida3 = "";
		            String Contrato = session.getContractNumber ();
					EIGlobal.mensajePorTrace("CRegCon  contrato = "+Contrato, EIGlobal.NivelLog.INFO);

		            String[][] arrayCuentas = ObteniendoCtasInteg1 (0,total,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+"N43.ambci");
		            //***********************************************
		            int residuo = 0;
					String monedaN = "";
					String monedaN1 = "";
					String descN = "";
					String descN1 = "";
					String cuentaCC = "";
					String[] SalidaIn1 = null;
		            int contador=1;
					int indice2 = 1;
					int indice3 = 1;
					int numCtasCoinciden=0;
		            for (indice2 = 1;indice2<=Integer.parseInt (arrayCuentas[0][0]);indice2++){
		                contador++;
		                residuo = contador % 2;

						if ("MXP".equals(arrayCuentas[indice2][9]))
						{monedaN = "MN";
						 monedaN1 = "MN&nbsp;&nbsp;";}
						else
						{monedaN = "USD";
						 monedaN1 = "USD&nbsp;";}
						if (arrayCuentas[indice2][9].equals("####"))
						{monedaN = "_";
						 monedaN1 = "_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}
						if(arrayCuentas[indice2][4].equals("") || arrayCuentas[indice2][4]==null)
						{
							descN="_";
							descN1="_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						}
						else
						{
							descN=arrayCuentas[indice2][4];
							descN1=arrayCuentas[indice2][4];
						}

						SalidaIn1 = desentramaC( conta + "," + gcuentas,',');

						EIGlobal.mensajePorTrace("*************************************** 2 = "+gcuentas, EIGlobal.NivelLog.INFO);
						cuentaCC = "no";
						for (indice3 = 1; indice3 <= Integer.parseInt( SalidaIn1[0].trim() ); indice3++ )
						{
							if(SalidaIn1[indice3].length()>0 )
							{
								if (SalidaIn1[indice3].equals(arrayCuentas[indice2][1]))
								{cuentaCC = "si";
								numCtasCoinciden++;//vulne
								}
							}//if
						}//for

		               // if (residuo == 0) {
						  if (cuentaCC.equals("si"))
						  {
							//strSalida2 = strSalida2 + "<tr  bgcolor=#CCCCCC><td align=right class=textabdatobs><input type=checkbox checked name=cta onclick=val_todas(); value=\""+arrayCuentas[indice2][1] +"$"+arrayCuentas[indice2][4]+"$"+monedaN+"$" +"\"></td><td class=textabdatobs>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatobs>&nbsp;"+descN+"</td><td class=textabdatobs>&nbsp;"+monedaN+"</td></tr>";
							/*   Modificado 04/12/2003    */
							//strSalida3 = strSalida3 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracter(arrayCuentas[indice2][1],'0',16,true)+"&nbsp;&nbsp;"+descN1+"</option>";
							strSalida3 = strSalida3 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracterHTML(arrayCuentas[indice2][1],16)+"&nbsp;&nbsp;"+descN1+"</option>";

							/*  Fin  Modificado 04/12/2003    */
						  }
						  else
						  {
							//strSalida2 = strSalida2 + "<tr  bgcolor=#CCCCCC><td align=right class=textabdatobs><input type=checkbox name=cta onclick=val_todas(); value=\""+arrayCuentas[indice2][1] +"$"+arrayCuentas[indice2][4]+"$"+monedaN+"$" +"\"></td><td class=textabdatobs>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatobs>&nbsp;"+descN+"</td><td class=textabdatobs>&nbsp;"+monedaN+"</td></tr>";
							// Modificado 04/12/2003
							//strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracter(arrayCuentas[indice2][1],'0',16,true)+"&nbsp;&nbsp;"+descN1+"</option>";
							strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracterHTML(arrayCuentas[indice2][1],16)+"&nbsp;&nbsp;"+descN1+"</option>";

							// Fin  Modificado 04/12/2003
						  }
		              /*  } else {
						  if (cuentaCC.equals("si"))
						  {
							//strSalida2 = strSalida2 + "<tr  bgcolor=#EBEBEB><td align=right class=textabdatcla><input type=checkbox checked name=cta onclick=val_todas(); value=\""+arrayCuentas[indice2][1] +"$"+arrayCuentas[indice2][4]+"$"+monedaN+"$" +"\"></td><td class=textabdatcla>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatcla>&nbsp;"+descN+"</td><td class=textabdatobs>&nbsp;"+monedaN+"</td></tr>";
							// Modificado 04/12/2003
							//strSalida3 = strSalida3 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracter(arrayCuentas[indice2][1],'0',16,true)+"&nbsp;&nbsp;"+descN1+"</option>";
							strSalida3 = strSalida3 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracterHTML(arrayCuentas[indice2][1],16)+"&nbsp;&nbsp;"+descN1+"</option>";
							// Fin  Modificado 04/12/2003
						  }
						  else
						  {
  						    //strSalida2 = strSalida2 + "<tr  bgcolor=#EBEBEB><td align=right class=textabdatcla><input type=checkbox name=cta onclick=val_todas(); value=\""+arrayCuentas[indice2][1] +"$"+arrayCuentas[indice2][4]+"$"+monedaN+"$" +"\"></td><td class=textabdatcla>"+ arrayCuentas[indice2][1]+"</td><td class=textabdatcla>&nbsp;"+descN+"</td><td class=textabdatobs>&nbsp;"+monedaN+"</td></tr>";
  						    // Modificado 04/12/2003
							//strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracter(arrayCuentas[indice2][1],'0',16,true)+"&nbsp;&nbsp;"+descN1+"</option>";
							strSalida2 = strSalida2 + "<option name=cta value=\""+arrayCuentas[indice2][1] +"$"+descN+"$"+monedaN+"$" +"\">"+monedaN1+rellenaCaracterHTML(arrayCuentas[indice2][1],16)+"&nbsp;&nbsp;"+descN1+"</option>";
					           // Fin  Modificado 04/12/2003
						  }
		                }*/
		            }//for
		            
		            //vulne 
		            EIGlobal.mensajePorTrace("El valor de numCtasFolio: "+numCtasFolio+" y el de numCtasCoinciden: "+numCtasCoinciden,EIGlobal.NivelLog.INFO);
					if((numCtasFolio==0)||(numCtasCoinciden==0)||(numCtasFolio!=numCtasCoinciden)){
						// vulne
						EIGlobal.mensajePorTrace("error por datos no correctos",EIGlobal.NivelLog.INFO);
						String mensaje ="Datos invalidos, por favor volver a intentar";
						req.setAttribute("codigoE", mensaje);
						evalTemplate(IEnlace.C_CON_RERR, req, res);
						return;
					}
		            
                    if (lado.equals("0")){
                    	req.setAttribute (CUENTAS,strSalida2);
                    	req.setAttribute ("cuentas1",strSalida3);
                    }
                    else if (lado.equals("1")) {
                    	req.setAttribute (CUENTAS,"");
                    	req.setAttribute ("cuentas1",strSalida2+strSalida3);
                    }
                    else if (lado.equals("2")) {
                    	req.setAttribute (CUENTAS,strSalida2+strSalida3);
                    	req.setAttribute ("cuentas1","");
   		           	}
		           /* req.setAttribute (CUENTAS,strSalida2);
					req.setAttribute ("cuentas1",strSalida3);*/

					String diasInhabiles = diasInhabilesAnt ();
					String strInhabiles = armaDiasInhabilesJS();
					String datesrvr = ObtenDia() + "-" + ObtenMes() + "-" + ObtenAnio();

		  		    req.setAttribute ("DiasInhabiles",diasInhabiles);
					req.setAttribute ("diasInhabiles", strInhabiles);
					req.setAttribute ("Movfechas",datesrvr);

					//Obtiene el siguiente dia habil
					String SigDia = "";
					EIGlobal.mensajePorTrace( "++++++1 Dias Inhabiles="+strInhabiles, EIGlobal.NivelLog.DEBUG);
					strInhabiles = strInhabiles + ",25/12/2000";
					SigDia = ObtenSigDiaHabil(strInhabiles);
					if (!"/".equals(SigDia.substring(2,3)))
					{
						SigDia = "0" + SigDia;
					}
					if (!SigDia.substring(5,6).equals("/"))
					{
						SigDia = SigDia.substring(0,2)+ "0" + SigDia.substring(3,8);
					}
					EIGlobal.mensajePorTrace ( "***CRegCon : Siguiente Habil :"+SigDia, EIGlobal.NivelLog.DEBUG);
					//Obtiene el dia 90 natural
					String dia90 = Cargar90(SigDia);
					req.setAttribute ("gdia90",dia90);
					EIGlobal.mensajePorTrace ( "***CRegCon : dia 90 :"+dia90, EIGlobal.NivelLog.DEBUG);

					//actualiza valores
					req.setAttribute ("actualiza_valor",actualiza_valor);

					req.setAttribute ("gperiodo",gperiodo);
					req.setAttribute ("ghorario",ghorario);
					req.setAttribute ("gtipomov",gtipomov);
					if(gChksvc==null)
					{gChksvc = "";}
					if(gChkvista==null)
					{gChkvista = "";}
					if(gimporte==null)
					{gimporte = "";}
					if(gemails==null)
					{gemails = "";}
					req.setAttribute ("gChksvc",gChksvc);
					req.setAttribute ("gChkvista",gChkvista);
					req.setAttribute ("gimporte",gimporte);
					req.setAttribute ("gfecha1",gfecha1);
					//Obtiene el siguiente dia habil
					req.setAttribute ("gformato",gformato);
					req.setAttribute ("greporte",greporte);
					req.setAttribute ("gcanal",gcanal);
					req.setAttribute ("gdiasemana",gdiasemana);
					req.setAttribute ("garchivo",garchivo);
					req.setAttribute ("gcuentas",gcuentas);
					gcta_saldo_sig1=gcta_saldo_sig1.replace(' ', '-');
					req.setAttribute ("gcta_saldo_sig1",gcta_saldo_sig1);
					gj_sig1 = Integer.toString(gj_sig2);
					req.setAttribute ("gj_sig1",gj_sig1);
					req.setAttribute ("gmodifolio",gmodifolio);
					req.setAttribute ("gemails",gemails);

					sess.setAttribute(CUENTAS,strSalida2);

		            //***********************************************
					EIGlobal.mensajePorTrace("***csaldo1 REGISTRO = " +funcion_llamar, EIGlobal.NivelLog.INFO);
		            evalTemplate ( IEnlace.C_MODI_PROG, req, res);

		         }else {

		            String laDireccion = "document.location='SinFacultades'";
		            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
		            res.setContentType(TEXTHTML);
		            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

				 }//fin else 1
			}//fin else modifica
		}//fin modifica
		else
		{
            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
            res.setContentType(TEXTHTML);
            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
		 }//fin modificacion de programaciones

		}
		else if  (funcion_llamar.equals("4")) //inicio MODIFICA REGISTRO   /*********************************************/
		{

		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***INICIA CRegCon.java > Modifica ", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);

		int indice = 0;

		String tramaentrada = "";
		String tramaentrada2 = "";
		String coderror = "";
		String cuentacompleta = "";
		String fechaexpt = "";
		String CodigoMensaje = "";
		String CodigoMensajeB = "";
		String CodigoFolio = "";
		String codigoEE = "";
        String codigoFF = "";
		String codigoXX = "";

		boolean sesionvalida1 = SesionValida ( req, res );
		if (! sesionvalida1 ) { return; }

			String cta_saldo;
			String emails;
			String fileOut="";
			String sessionIdN43 = (String) req.getSession().getId();
			fileOut = session.getUserID8() + sessionIdN43 + ".ctas";

			// Se obtienen par�metros de Norma 43
			String gperiodo = (String)req.getParameter("gperiodo1");
			String ghorario = (String)req.getParameter("ghorario1");
			String gtipomov = (String)req.getParameter("gtipomov1");
			String gChksvc = (String)req.getParameter("gChksvc");
			String gChkvista = (String)req.getParameter("gChkvista");
			String gimporte = (String)req.getParameter("gimporte1");
			String gfecha1 = (String)req.getParameter("gfecha1");
			String gformato = (String)req.getParameter("gformato1");
			String greporte = (String )req.getParameter("greporte1");
			String gcanal = (String)req.getParameter("gcanal1");
			String gdiasemana = (String)req.getParameter("gdiasemana1");
			String gmodifolio = (String)req.getParameter("gmodifolio");

			String m = ( String ) req.getParameter( "gj" ); //Numero de Cuentas
			String mm = ( String ) req.getParameter( "gjj" ); //Numero de emails
			emails = ( String ) req.getParameter( "gemail" );

			EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Parametros > "+gperiodo+","+ghorario+","+gtipomov+","+gChksvc+","+gChkvista+","+gimporte+","+gfecha1+","+gformato+","+greporte+","+gcanal+","+gmodifolio, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Parametros > "+mm+","+emails, EIGlobal.NivelLog.INFO);
			gfecha1 = gfecha1.substring(0,2)+"-"+gfecha1.substring(3,5)+"-"+gfecha1.substring(6,10);

			//Arma trama de alta de Consulta Programada
			tramaentrada2 = session.getContractNumber() + "@" + session.getUserID8() + "@" + gperiodo + "@";
         if ("2".equals(gperiodo)) {
            if ("1".equals(gdiasemana)) {
               tramaentrada2 = tramaentrada2 + "LUNES@";
            }
            if ("2".equals(gdiasemana)) {
               tramaentrada2 = tramaentrada2 + "MARTES@";
            }
            if ("3".equals(gdiasemana)) {
               tramaentrada2 = tramaentrada2 + "MIERCOLES@";
            }
            if ("4".equals(gdiasemana)) {
               tramaentrada2 = tramaentrada2 + "JUEVES@";
            }
            if ("5".equals(gdiasemana)) {
               tramaentrada2 = tramaentrada2 + "VIERNES@";
            }
         }
			else
			{tramaentrada2 = tramaentrada2  + "_@";}
			tramaentrada2 = tramaentrada2  + ghorario + "@" + gtipomov + "@";
			if(gChksvc!=null)
			{tramaentrada2 = tramaentrada2  + "0@";}
			else
			{tramaentrada2 = tramaentrada2  + "1@";}
			if(gChkvista!=null)
			{tramaentrada2 = tramaentrada2  + "0@";}
			else
			{tramaentrada2 = tramaentrada2  + "1@";}
			if(gimporte.equals(""))
			{tramaentrada2 = tramaentrada2  + "_@";}
			else
			{tramaentrada2 = tramaentrada2  + gimporte + "@";}
			tramaentrada2 = tramaentrada2  + gfecha1 + "@";
			if(gformato.equals("0"))
			{tramaentrada2 = tramaentrada2  + "TCT@";}
			else
			{tramaentrada2 = tramaentrada2  + "N43@";}
			tramaentrada2 = tramaentrada2  + greporte + "@" + gcanal + "@";
			tramaentrada2 = tramaentrada2 + m + "@";
			if(!mm.equals("0"))
			{tramaentrada2 = tramaentrada2 + mm + "@" + Global.N43_PATH_INTERNET + "/" + fileOut + "@";}
			else
			{tramaentrada2 = tramaentrada2 + "_@" + Global.N43_PATH_INTERNET  + "/" + fileOut + "@";}
			tramaentrada2 = tramaentrada2 + gmodifolio + "@";

			//Obtiene las cuentas en el formato NoCta@Descripcion@Moneda.
			//Todas las Cuentas son separadas por caracter "," (coma).
			//Ej: 82500005708@GRUPO BANCA ELECTRONICA@MN@,82500058464@ABENGOA MEXICO SA DE CV@MN@
			cta_saldo = ( String ) req.getParameter( "gcta_saldo" );
			cta_saldo=cta_saldo.replace('-', ' ');
			String[] arraySaldos = new String[2];
			boolean grabaArchivo = true;
			boolean bandera		   = false;

			//Se insertan las cuentas en un Arreglo de Strings. Cada cuenta en un
			//espacio del arreglo.
			EIGlobal.mensajePorTrace("CRegCon.java > Modifica > m "+m+"   cta_saldo "+cta_saldo, EIGlobal.NivelLog.INFO);
			String[] Carreglo = desentramaC( m + "," + cta_saldo,',');
			String[] Carreglo1 = new String[6];
			//Se insertan los emails en un Arreglo de Strings. Cada email en un
			//espacio del arreglo.
			if(!mm.equals("0"))
			{
				emails = emails + ",";
				EIGlobal.mensajePorTrace("CRegCon.java > Modifica > m "+mm+"   emails "+emails, EIGlobal.NivelLog.INFO);
				Carreglo1 = desentramaC( mm + "," + emails,',');
			}

			EI_Exportar ArcSal;
			ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			EIGlobal.mensajePorTrace("CRegCon.java > Modifica > PATH > "+IEnlace.DOWNLOAD_PATH + fileOut, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***Se verificar� que el archivo no exista en caso contrario se borrara y crear� uno nuevo", EIGlobal.NivelLog.DEBUG);
			ArcSal.creaArchivo();

			Hashtable htResult;

			try
				{
				//Ciclo que recorre arreglo de cuentas para escribir en el ARCHIVO.
				for (indice = 1; indice <= Integer.parseInt( Carreglo[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Carreglo "+Carreglo[indice], EIGlobal.NivelLog.INFO);
					if(Carreglo[indice].length()>0 )
						{
						cuentacompleta = "1$"+Carreglo[indice];
						EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Escritura > cuentacompleta : "+cuentacompleta, EIGlobal.NivelLog.INFO);

						if(grabaArchivo){
								ArcSal.escribeLinea(cuentacompleta + "\r\n");
								EIGlobal.mensajePorTrace( "***CRegCon.java > Modifica > Se escribio en el archivo  "+cuentacompleta, EIGlobal.NivelLog.INFO);
							}//fin
						}//if
					}//for

				if(!"0".equals(mm))
				{
					//Ciclo que recorre arreglo de emails para escribir en el ARCHIVO.
					for (indice = 1; indice <= Integer.parseInt( Carreglo1[0].trim() ); indice++ )
					{
						EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Carreglo1 "+Carreglo1[indice], EIGlobal.NivelLog.INFO);
						if(Carreglo1[indice].length()>0 )
						{
							cuentacompleta = "2$"+Carreglo1[indice]+"$";
							EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Escritura > cuentacompleta : "+cuentacompleta, EIGlobal.NivelLog.INFO);

							if(grabaArchivo){
								ArcSal.escribeLinea(cuentacompleta + "\r\n");
								EIGlobal.mensajePorTrace( "***CRegCon.java > Modifica > Se escribio en el archivo  "+cuentacompleta, EIGlobal.NivelLog.INFO);
							}//fin
						}//if
					}//for
				}//fin if emails

				if( grabaArchivo )
					ArcSal.cierraArchivo();

				ArchivoRemoto archR = new ArchivoRemoto();
				if(!archR.copiaN43(fileOut,Global.N43_PATH_INTERNET))
				{
					EIGlobal.mensajePorTrace("***Modifica > No se pudo copiar archivo remoto:"+IEnlace.REMOTO_TMP_DIR+"/"+session.getUserID8(), EIGlobal.NivelLog.INFO);
				}
				else
				{
					  EIGlobal.mensajePorTrace("***Modifica > archivo remoto copiado:"+IEnlace.REMOTO_TMP_DIR+"/"+session.getUserID8(), EIGlobal.NivelLog.INFO);
				}//fin else remoto

				// LLAMADO AL SERVICIO TUXEDO
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				tramaentrada = "1EWEB|" + session.getUserID8() + "|MPRM|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
				tramaentrada = tramaentrada + tramaentrada2 + "|";

				//Llama a la Alta de Consulta de Programaciones.
				htResult = tuxGlobal.web_red( tramaentrada );
				coderror = ( String ) htResult.get( "BUFFER" );

				if (Global.NIVEL_TRACE>1)
					EIGlobal.mensajePorTrace( "CRegCon.java > Modifica > CODIGO ERROR = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);

				fechaexpt = ObtenFecha(true);
				fechaexpt = fechaexpt.substring(0,2)+fechaexpt.substring(3,5)+fechaexpt.substring(6,fechaexpt.length());

				if (coderror.substring(0,8).equals("NORM0000"))
				{
					bandera = true;

					String[] Salida = desentramaC( "3@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( Salida[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Salida > "+Salida[indice], EIGlobal.NivelLog.INFO);
					if(Salida[indice].length()>0 )
						{
							if (indice == 2)
							{
								CodigoMensaje = Salida[indice];
								if (CodigoMensaje.equals("Transaccion Exitosa"))
								{
									CodigoMensaje = "Modificaci&oacute;n de Consulta de Movimientos Exitosa";
								}
							}
							if (indice == 3)
							{
								CodigoFolio = Salida[indice];
							}
						EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Salida > "+CodigoMensaje+","+CodigoFolio, EIGlobal.NivelLog.INFO);

						}//if
					}//for
					EIGlobal.mensajePorTrace( "CRegCon.java > OK > BROWSER = " +CodigoMensaje+ ", "+CodigoFolio, EIGlobal.NivelLog.INFO);
					CodigoMensajeB = "1";
					codigoEE = CodigoMensaje;
					codigoEE = codigoEE.replace(' ', '_');
			        codigoFF = CodigoFolio;
					codigoXX = "5";
				}
				/*else if (coderror.substring(0,8).equals("NORM0013")) //else de OK
				{
					bandera = true;

					String[] Salida = desentramaC( "3@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( Salida[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Salida Error > "+Salida[indice], EIGlobal.NivelLog.INFO);
					if(Salida[indice].length()>0 )
						{
							if (indice == 2)
							{
								CodigoMensaje = Salida[indice];
							}
							if (indice == 3)
							{
								CodigoFolio = Salida[indice];
							}
						EIGlobal.mensajePorTrace("CRegCon.java > Modifica > Salida Error > "+CodigoMensaje+","+CodigoFolio, EIGlobal.NivelLog.INFO);

						}//if
					}//for

					EIGlobal.mensajePorTrace( "CRegCon.java > OK > BROWSER = " +CodigoMensaje+ ", "+CodigoFolio, EIGlobal.NivelLog.INFO);
					CodigoMensaje ="La cuenta no esta asociada a una inversi�n vista";
					CodigoMensajeB = "2";
					codigoEE = CodigoMensaje;
					codigoEE = codigoEE.replace(' ', '_');
			        codigoFF = CodigoFolio;
					codigoXX = "5";

				}//fin if OK
*/
				else
				{
					if (coderror.substring(0,8).equals("NORM0013"))
					    CodigoMensaje ="La&nbsp;cuenta&nbsp;no&nbsp;esta&nbsp;asociada&nbsp;a&nbsp;una&nbsp;inversi&oacute;n&nbsp;vista";
					else
					    CodigoMensaje = "Servicio no Disponible por el Momento.";
					CodigoMensaje.replace(' ', '_');
					CodigoFolio = "";
					bandera = false;
					CodigoMensajeB = "2";
					codigoEE = CodigoMensaje;
					codigoEE = codigoEE.replace(' ', '_');
			        codigoFF = CodigoFolio;
					codigoXX = "5";

				}//fin else consulta

			} catch( Exception e ){
				/*despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP,
						"Consulta de Saldos de cuentas de cheques", "", req, res ); */
			}

			req.setAttribute(MENUPRINCIPAL, session.getStrMenu());
			req.setAttribute(NEWMENU, session.getFuncionesDeMenu());
			req.setAttribute(ENCABEZADO, CreaEncabezado("Modificaci&oacute;n de Consulta de Programaciones","Consultas &gt; Movimientos &gt; Chequeras &gt; Programados  &gt; Registros &gt Modificaci&oacute;n","s36020h",req));
			req.setAttribute("codigoM",CodigoMensajeB);
			req.setAttribute("codigoE",CodigoMensaje);
			req.setAttribute("codigoF",CodigoFolio);
			req.setAttribute(CODIGOEE,codigoEE);
			req.setAttribute(CODIGOFF,codigoFF);
			req.setAttribute(CODIGOXX,codigoXX);
			// //TODO CU1091
		    /*
			 * 05/ENE/07
			 * VSWF-BMB-I
			 */   if ("ON".equals(Global.USAR_BITACORAS.trim())){
			try{
				if(gimporte==null || gimporte.trim().equals("")){
					gimporte="0.00";
				}
		    BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_REGISTRO_MODIF_PROGRAMACIONES);
			bt.setContrato(session.getContractNumber());
			bt.setImporte(Double.parseDouble(gimporte));
			bt.setFechaProgramada(Utilerias.MdyToDate(gfecha1.replace('-', '/')));
			if(coderror!=null){
				if(coderror.substring(0,2).equals("OK")){
					bt.setIdErr("MPRM0000");
				}
				else if(coderror.length()>8){
					bt.setIdErr(coderror.substring(0,8));
				}else{bt.setIdErr(coderror.trim());}
			}
			bt.setServTransTux("MPRM");
			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
			 }
		    /*
			 * VSWF-BMB-F
			 */

			evalTemplate( IEnlace.C_MODI_ERR, req, res );

		}//fin else modifica
		 else if  (funcion_llamar.equals("5")) //inicio CONSULTA RESULTADO PROGRAMACIONES   /*********************************************/
		{
			 // <vswf RRG cambio nombre de la variable enum a enum1 05122008>
		 java.util.Enumeration enum1 = sess.getAttributeNames() ;
		 while(enum1.hasMoreElements() ) {
			try{
				EIGlobal.mensajePorTrace((String)enum1.nextElement(), EIGlobal.NivelLog.DEBUG);
			}catch(Exception e) {}
		 }

		 boolean sesionvalida1 = SesionValida ( req, res );
		 if (! sesionvalida1 ) { return; }

		 BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		 if(session ==null)
			EIGlobal.mensajePorTrace(TRAZALOGSESION, EIGlobal.NivelLog.DEBUG);

		 if(session.getFacultad ( session.FAC_CONSREPROG_N43 ))
		 {
			/***************************************************************/
			/* LLAMADO AL SERVICIO DE CONSULTA DE RESULTADO PROGRAMACIONES */
			/***************************************************************/
			String tramaentrada = "";
			boolean bandera = false;
			Hashtable htResult;
			String coderror = "";
			String CodigoMensaje = "";
			String CodigoMensajeB = "";
			String CodigoFolio = "";
			String grutaarchivo = "";
			String garchivo = "";
			String sessionIdN43 = (String) req.getSession().getId();
			int indice = 1;

			//Inicializacion de Exportacion de Archivo
			String fileExporta="";
			String valorarchivo1 = "";
			fileExporta = session.getUserID8() + "resu.doc";
			boolean grabaArchivo1 = true;

			EI_Exportar ResuSal;
			ResuSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileExporta );
			EIGlobal.mensajePorTrace( "***Se verificar� que el archivo no exista en caso contrario se borrara y crear� uno nuevo", EIGlobal.NivelLog.DEBUG);
			ResuSal.creaArchivo();

			try
			{
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Resultado", EIGlobal.NivelLog.DEBUG);
				// LLAMADO AL SERVICIO TUXEDO
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				if (Global.TAM_ARCHIVO==null || Global.TAM_ARCHIVO.equals(""))
				{
					Global.TAM_ARCHIVO = "5242880";
				}

				tramaentrada = "2EWEB|" + session.getUserID8() + "|CREP|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
				tramaentrada = tramaentrada + session.getContractNumber() + "@" + session.getUserID8() + "@" + sessionIdN43 + "@" + Global.TAM_ARCHIVO + "@|";
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Resultado > " + tramaentrada, EIGlobal.NivelLog.INFO);

				//Llama a la Consulta de Resultados.
				htResult = tuxGlobal.web_red( tramaentrada );
				coderror = ( String ) htResult.get( "BUFFER" );
				if (Global.NIVEL_TRACE>1)
					EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Res > Salida = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);

				if ("NORM0000".equals(coderror.substring(0,8)))
				{
					String[] SalidaCorrecta = desentramaC( "4@" + coderror,'@');

					//Ciclo que recorre arreglo de SalidaCorrecta de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( SalidaCorrecta[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Consulta Res > Salida > "+SalidaCorrecta[indice], EIGlobal.NivelLog.INFO);
					if(SalidaCorrecta[indice].length()>0 )
						{
							if (indice == 3)
							{grutaarchivo = SalidaCorrecta[indice];}
							if (indice == 4)
							{garchivo = SalidaCorrecta[indice];}
						}//if
					}//for

					bandera = true;
					CodigoMensajeB = "1";
				}
				else if (coderror.substring(0,8).equals("NORMXXXX")) //else de OK
				{
					String[] SalidaIn = desentramaC( "4@" + coderror,'@');

					//Ciclo que recorre arreglo de Salida de Tuxedo.
					for (indice = 1; indice <= Integer.parseInt( SalidaIn[0].trim() ); indice++ )
					{
					EIGlobal.mensajePorTrace("CRegCon.java > Consulta In > Salida > "+SalidaIn[indice], EIGlobal.NivelLog.INFO);
					if(SalidaIn[indice].length()>0 )
						{
							if (indice == 2)
							{CodigoMensaje = SalidaIn[indice];}
							EIGlobal.mensajePorTrace("CRegCon.java > Consulta In > Salida > "+CodigoMensaje, EIGlobal.NivelLog.INFO);
						}//if
					}//for
					bandera = false;
					CodigoMensajeB = "2";
					CodigoMensaje = CodigoMensaje.replace(' ', '_');

				}//fin else consulta
				else
				{
					CodigoMensaje = "Servicio no Disponible por el Momento.";
					CodigoMensaje = CodigoMensaje.replace(' ', '_');
					CodigoFolio = "";
					bandera = false;
					CodigoMensajeB = "2";

				}//fin else consulta

			} catch( Exception e ){
				/*despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP,
						"Consulta de Saldos de cuentas de cheques", "", req, res ); */
			}//fin catch

			EIGlobal.mensajePorTrace( "CRegCon.java > Consulta  Resultado > ARMA", EIGlobal.NivelLog.DEBUG);
			req.setAttribute(MENUPRINCIPAL, session.getStrMenu());
			req.setAttribute(NEWMENU, session.getFuncionesDeMenu());
			req.setAttribute(ENCABEZADO, CreaEncabezado("Resultado de Consultas Programadas","Consultas &gt; Movimientos &gt; Chequeras &gt; Programados  &gt; Consulta","s36040h",req));
			req.setAttribute("codigoM",CodigoMensajeB);
			req.setAttribute("codigoE",CodigoMensaje);

			if (CodigoMensajeB.equals("2")) /**  SI FALLO LA CONSULTA DE RESULTADOS **/
			{
				evalTemplate( IEnlace.C_CON_RERR, req, res );
				EIGlobal.mensajePorTrace( "CRegCon.java > Consulta Resultado > FALSE", EIGlobal.NivelLog.DEBUG);
			}
			else   /**  PANTALLA DE CONSULTA DE RESULTADOS **/
			{
				 boolean sesionvalida = SesionValida ( req, res );
				 if (! sesionvalida ) { return; }
					//TODO CU1101
				    /*
					 * 08/ENE/07
					 * VSWF-BMB-I
					 */
				  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
						BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
						BitaHelper bh = new BitaHelperImpl(req, session, sess);
						/*Inicia - EVERIS VALIDACION SQL INJECTION*/
						EIGlobal.mensajePorTrace("Validar SQLINJECTION para el parametro [flujo]", EIGlobal.NivelLog.INFO);
					    if(UtilidadesEnlaceOwasp.cleanQuery(req.getParameter(BitaConstants.FLUJO))!=null){
						/*Finaliza - EVERIS VALIDACION SQL INJECTION*/
					    	EIGlobal.mensajePorTrace("Validar SQLINJECTION para el parametro [flujo]", EIGlobal.NivelLog.INFO);
					    	bh.incrementaFolioFlujo((String)UtilidadesEnlaceOwasp.cleanQuery(req.getParameter(BitaConstants.FLUJO)));
					    }else{
					    	bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					    }
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_CONSULTA_ENTRA);
						if(session.getContractNumber()!=null){
							bt.setContrato(session.getContractNumber());
						}
						if(coderror!=null){
							if("OK".equals(coderror.substring(0,2))){
								bt.setIdErr("CREP0000");
							}if(coderror.length()>8){
								bt.setIdErr(coderror.substring(0,8));
							}else {bt.setIdErr(coderror.trim());}
						}
						bt.setServTransTux("CREP");
						BitaHandler.getInstance().insertBitaTransac(bt);
					}catch(SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
				  }
				    /*
					 * VSWF-BMB-F
					 */

		         try{
					pre = req.getParameter ("gprev");
					nex =  req.getParameter ("gnext");
					tota = req.getParameter ("gtotal");

					EIGlobal.mensajePorTrace ( "***CRegCon orev "+pre, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ( "***CRegCon next "+nex, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ( "***CRegCon total "+tota, EIGlobal.NivelLog.INFO);
				 }catch(Exception e) {}

		         int total=0,prev=0,next=0;
		         StringBuffer paginacion=new StringBuffer();
		         if((pre!=null) &&  (nex!=null) && (tota!=null)) {
					try{
		                prev =  Integer.parseInt (pre);
		                next =  Integer.parseInt (nex);
		                total = Integer.parseInt (tota);
					} catch(Exception e) {
				           e.printStackTrace();
				    }
		         } else {

		        EIGlobal.mensajePorTrace ( "***CRegCon todos nullos", EIGlobal.NivelLog.DEBUG);
				try{
				     EIGlobal.mensajePorTrace("el userID "+session.getUserID8(), EIGlobal.NivelLog.INFO);
				   } catch(Exception e) {
					                      e.printStackTrace();
				                        }

				EIGlobal.mensajePorTrace("Tomamos el archivo de Consulta de Resultado", EIGlobal.NivelLog.DEBUG);

				ArchivoRemoto archR = new ArchivoRemoto();
				if(!archR.copiaN43Modi(garchivo,grutaarchivo)) //"/planbcae/procesos/Norma43/archivos_internet/"
				{
					EIGlobal.mensajePorTrace("***Consulta Resultados No se pudo copiar archivo remoto: "+garchivo, EIGlobal.NivelLog.INFO);
				}
				else
				{
					if(!archR.copiaLocalARemoto(garchivo,"WEB")) //"/planbcae/procesos/Norma43/archivos_internet/"
					{
						EIGlobal.mensajePorTrace("***Consulta Resultados No se pudo copiar archivo remoto: "+garchivo, EIGlobal.NivelLog.INFO);
					}//fin if
					EIGlobal.mensajePorTrace("***Consulta Resultados archivo remoto copiado:"+garchivo, EIGlobal.NivelLog.INFO);
				}//fin else remoto


				EIGlobal.mensajePorTrace("Tomamos los datos de la Consulta de Resultados", EIGlobal.NivelLog.DEBUG);
		        //llamado_servicioCtasInteg (IEnlace.MConsulta_Saldos," "," "," ",session.getUserID8 ()+AMBI,req);
		        total=getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/" + garchivo);
		        prev=0;

		            if (total>Global.NUM_REGISTROS_PAGINA1) {
		                next=Global.NUM_REGISTROS_PAGINA1;
		            } else {
		                      next=total;
		                    }
			     }//fin else 4

				 EIGlobal.mensajePorTrace ( "***CRegCon orev"+prev, EIGlobal.NivelLog.INFO);
		         EIGlobal.mensajePorTrace ( "***CRegCon next"+next, EIGlobal.NivelLog.INFO);
		         EIGlobal.mensajePorTrace ( "***CRegCon total"+total, EIGlobal.NivelLog.INFO);

		         log ("CRegCon.java::defaultAction()");
		         try{
		         req.setAttribute (MENUPRINCIPAL, session.getStrMenu ());
		         req.setAttribute (NEWMENU, session.getFuncionesDeMenu ());
		         req.setAttribute(ENCABEZADO, CreaEncabezado("Resultado de Consultas Programadas","Consultas &gt; Movimientos &gt; Chequeras &gt; Programados  &gt; Consultas","s36040h",req));

				 } catch(Exception e) { e.printStackTrace();}

		        EIGlobal.mensajePorTrace( "\n\n\t\tsession distinto a null:  " + (session != null), EIGlobal.NivelLog.INFO);
		         if(session.getFacultad ( session.FAC_CONSULTA_SALDO )) {

				    indice = 1;
		            StringBuffer strSalida2 = new StringBuffer();
		            String Contrato = session.getContractNumber ();
					EIGlobal.mensajePorTrace("CRegCon  contrato = "+Contrato, EIGlobal.NivelLog.INFO);

		            String[][] arrayCuentas = ObteniendoResultados (prev,next,IEnlace.LOCAL_TMP_DIR + "/" + garchivo);
		            //***********************************************

		            int residuo = 0;
					String liga = "Si";
		            int contador=1;

		            for (int indice2 = 1;indice2<=Integer.parseInt (arrayCuentas[0][0]);indice2++){
		            	ArchivoRemoto Ligas = new ArchivoRemoto(); //Se crea el archivo remoto dentro del for para que se restauren las variables.
		                contador++;
		                residuo = contador % 2;

						liga = "Si";
						if (arrayCuentas[indice2][7].equals("No se Pudo Generar el Archivo"))
						{liga = "No";}
						if (arrayCuentas[indice2][7].equals("Excedio la capacidad para descarga por Enlace, utilice como medio de entrega buzon"))
						{liga = "No";}

		                if (residuo == 0) {
							strSalida2.append("<tr  bgcolor=#CCCCCC><td class=textabdatobs>");
							strSalida2.append(arrayCuentas[indice2][1]);
							strSalida2.append("</td>");
							if (arrayCuentas[indice2][5].equals("3") && liga.equals("Si")) {
								strSalida2.append("<td class=textabdatobs><a name=");
								strSalida2.append(arrayCuentas[indice2][2]);
								strSalida2.append(" href=\"/Download/");
								strSalida2.append(arrayCuentas[indice2][9]);
								strSalida2.append("\" >");
								strSalida2.append(arrayCuentas[indice2][2]);
								strSalida2.append("</a></td>");
								if(!Ligas.copiaN43Modi(arrayCuentas[indice2][9],arrayCuentas[indice2][8])) {
									EIGlobal.mensajePorTrace("***Resultados Liga No se pudo copiar archivo remoto: "+arrayCuentas[indice2][9], EIGlobal.NivelLog.INFO);
								}else {
									if(!Ligas.copiaLocalARemoto(arrayCuentas[indice2][9],"WEB")) //"/planbcae/procesos/Norma43/archivos_internet/"
									{
										EIGlobal.mensajePorTrace("***Consulta Resultados No se pudo copiar archivo remoto: "+arrayCuentas[indice2][9], EIGlobal.NivelLog.INFO);
									}//fin if
									  EIGlobal.mensajePorTrace("***Resultados Liga archivo remoto copiado:"+arrayCuentas[indice2][9], EIGlobal.NivelLog.INFO);
								}//fin else remoto
							}else {
								strSalida2.append("<td class=textabdatobs>");
								strSalida2.append(arrayCuentas[indice2][2]);
								strSalida2.append("</td>");
							}
							strSalida2.append("<td class=textabdatobs>");
							strSalida2.append(arrayCuentas[indice2][3]);
							strSalida2.append("</td><td class=textabdatobs>");
							strSalida2.append(arrayCuentas[indice2][4]);
							strSalida2.append("</td>");
							if (arrayCuentas[indice2][5].equals("3")) {
								strSalida2.append("<td class=textabdatobs>Enlace</td>");
							}
							if (arrayCuentas[indice2][5].equals("2")) {
								strSalida2.append("<td class=textabdatobs>Buz&oacute;n</td>");
							}
							if (arrayCuentas[indice2][5].equals("1")) {
								strSalida2.append("<td class=textabdatobs>E-mail</td>");
							}
							if (arrayCuentas[indice2][6].equals("N43")) {
								strSalida2.append("<td class=textabdatobs>Norma 43</td>");
							}
							if (arrayCuentas[indice2][6].equals("TCT")) {
								strSalida2.append("<td class=textabdatobs>Enlace Internet</td>");
							}
							if (arrayCuentas[indice2][7].equals("_")) {
								strSalida2.append("<td class=textabdatobs>&nbsp; </td> </tr>");
							}else {
								strSalida2.append("<td class=textabdatobs>&nbsp;"+ arrayCuentas[indice2][7]+"</td> </tr>");
							}
		                } else {
		                	strSalida2.append("<tr  bgcolor=#EBEBEB><td class=textabdatcla>");
							strSalida2.append(arrayCuentas[indice2][1]);
							strSalida2.append("</td>");
							if ("3".equals(arrayCuentas[indice2][5]) && "Si".equals(liga)) {
								strSalida2.append("<td class=textabdatcla><a name=");
								strSalida2.append(arrayCuentas[indice2][2]);
								strSalida2.append(" href=\"/Download/");
								strSalida2.append(arrayCuentas[indice2][9]);
								strSalida2.append("\" >");
								strSalida2.append(arrayCuentas[indice2][2]);
								strSalida2.append("</a></td>");
								if(!Ligas.copiaN43Modi(arrayCuentas[indice2][9],arrayCuentas[indice2][8]))
								{
									EIGlobal.mensajePorTrace("***Resultados Liga No se pudo copiar archivo remoto: "+arrayCuentas[indice2][9], EIGlobal.NivelLog.INFO);
								}
								else
								{
									if(!Ligas.copiaLocalARemoto(arrayCuentas[indice2][9],"WEB")) //"/planbcae/procesos/Norma43/archivos_internet/"
									{
										EIGlobal.mensajePorTrace("***Consulta Resultados No se pudo copiar archivo remoto: "+arrayCuentas[indice2][9], EIGlobal.NivelLog.INFO);
									}//fin if
									EIGlobal.mensajePorTrace("***Resultados Liga archivo remoto copiado:"+arrayCuentas[indice2][9], EIGlobal.NivelLog.INFO);
								}//fin else remoto
							}else {
								strSalida2.append("<td class=textabdatcla>");
								strSalida2.append(arrayCuentas[indice2][2]);
								strSalida2.append("</td>");
							}
							strSalida2.append("<td class=textabdatcla>");
							strSalida2.append(arrayCuentas[indice2][3]);
							strSalida2.append("</td><td class=textabdatcla>");
							strSalida2.append(arrayCuentas[indice2][4]);
							strSalida2.append("</td>");
							if ("3".equals(arrayCuentas[indice2][5])) {
								strSalida2.append("<td class=textabdatcla>Enlace</td>");
							}
							if ("2".equals(arrayCuentas[indice2][5])) {
								strSalida2.append("<td class=textabdatcla>Buz&oacute;n</td>");
							}
							if ("1".equals(arrayCuentas[indice2][5])) {
								strSalida2.append("<td class=textabdatcla>E-mail</td>");
							}
							if ("N43".equals(arrayCuentas[indice2][6])) {
								strSalida2.append("<td class=textabdatcla>Norma 43</td>");
							}
							if ("TCT".equals(arrayCuentas[indice2][6])) {
								strSalida2.append("<td class=textabdatcla>Enlace Internet</td>");
							}
							if ("_".equals(arrayCuentas[indice2][7])) {
								strSalida2.append("<td class=textabdatcla>&nbsp; </td> </tr>");
							}else {
								strSalida2.append("<td class=textabdatcla>&nbsp;");
								strSalida2.append(arrayCuentas[indice2][7]);
								strSalida2.append("</td> </tr>");
							}
		                }//fin else residuo

						valorarchivo1 = rellenaCaracter(arrayCuentas[indice2][1],' ',12,false)+rellenaCaracter(arrayCuentas[indice2][2],'0',12,true)+
								        rellenaCaracter(arrayCuentas[indice2][3],' ',6,false) + rellenaCaracter(arrayCuentas[indice2][4],' ',11,false) +
								        rellenaCaracter(arrayCuentas[indice2][5],' ',7,false) + rellenaCaracter(arrayCuentas[indice2][6],' ',16,false) +
										rellenaCaracter(arrayCuentas[indice2][7],' ',100,false);

						if(grabaArchivo1)
						{
							ResuSal.escribeLinea(valorarchivo1 + "\r\n");
							EIGlobal.mensajePorTrace( "***Se enscribio en el archivo RESULTADO "+valorarchivo1, EIGlobal.NivelLog.INFO);
						}
		            }//for

		            req.setAttribute (CUENTAS,strSalida2.toString());

		            if (total>Global.NUM_REGISTROS_PAGINA1){
		                paginacion.append("<table  width=450 align=center>");

						if(prev > 0) {
		                    //paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:history.back();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA1+"</A></td></TR>";
		                    paginacion.append("<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:atras();\">< Anteriores ");
		                    paginacion.append(Global.NUM_REGISTROS_PAGINA1);
		                    paginacion.append("</A></td></TR>");
		                }
		                if(next < total) {
		                    if((next + Global.NUM_REGISTROS_PAGINA1) <= total) {
		                    	paginacion.append("<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes ");
		                    	paginacion.append(new Integer (next -prev).toString ());
		                    	paginacion.append(" ></A></td></TR>");
		                    } else {
	                    	paginacion.append("<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes ");
	                    	paginacion.append(new Integer (total-next).toString ());
	                    	paginacion.append(" ></A></td></TR>");
		                    }
		                }
		                paginacion.append("</table><br><br>");
		                EIGlobal.mensajePorTrace ( "***CRegCon : paginacion :"+paginacion.toString(), EIGlobal.NivelLog.INFO);
		            }

		            req.setAttribute ("total", String.valueOf(total));
		            req.setAttribute ("prev", String.valueOf(prev));
		            req.setAttribute ("next", String.valueOf(next));
		            req.setAttribute ("paginacion", paginacion.toString());
					EIGlobal.mensajePorTrace("Paginacion "+paginacion.toString(), EIGlobal.NivelLog.INFO);
		            req.setAttribute ("varpaginacion", String.valueOf(Global.NUM_REGISTROS_PAGINA1));

					String diasInhabiles = diasInhabilesAnt ();
					String strInhabiles = armaDiasInhabilesJS();
					String datesrvr = ObtenDia() + "-" + ObtenMes() + "-" + ObtenAnio();

		  		    req.setAttribute ("DiasInhabiles",diasInhabiles);
					req.setAttribute ("diasInhabiles", strInhabiles);
					req.setAttribute ("Movfechas",datesrvr);

					if( grabaArchivo1 ) {
						ResuSal.cierraArchivo();
					}

					ArchivoRemoto archResu = new ArchivoRemoto();
					if(!archResu.copiaLocalARemoto(fileExporta,"WEB")) {
						EIGlobal.mensajePorTrace("***Resultados No se pudo crear archivo para exportar Resultados", EIGlobal.NivelLog.DEBUG);
					}

					String downloadFile = "";
					if ( grabaArchivo1 ) {
						downloadFile = "/Download/" + fileExporta;
						req.setAttribute("DownLoadFile",downloadFile);
					 }

		            //***********************************************
					EIGlobal.mensajePorTrace("***csaldo1 REGISTRO = " +funcion_llamar, EIGlobal.NivelLog.INFO);
					//TODO CU1101
				    /*
					 * 08/ENE/07
					 * VSWF-BMB-I
					 */
					  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
					    BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_CONSULTA_RES_CONS_PROG);
						if(Contrato!=null){
							bt.setContrato(Contrato);
						}
						if(fileExporta!=null){
							bt.setNombreArchivo(fileExporta);
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					}catch(SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
					  }
				    /*
					 * VSWF-BMB-F
					 */
		            evalTemplate ( IEnlace.C_CON_RESU, req, res);

		         }else {

		            String laDireccion = "document.location='SinFacultades'";
		            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
		            res.setContentType(TEXTHTML);
		            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

				 }//fin else 1
			}//fin else modifica
		}//fin resultados
		else
		{
            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute( PLANTILLAELEGIDA, laDireccion);
            res.setContentType(TEXTHTML);
            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
		 }//fin resultados de consulta de programaciones

		}//fin consulta Resultados

    }//fin default
	/**
	 * Metodo que determina los dias inhabiles
	 * @return resultado String dias inhabiles
	 */
	String armaDiasInhabilesJS() {
		String resultado = "";
		Vector diasInhabiles;
		int indice = 0;
		diasInhabiles = CargarDias(1);
		if(diasInhabiles!=null && diasInhabiles.isEmpty()==false) {
			resultado = diasInhabiles.elementAt(indice).toString();
			for(indice = 1; indice<diasInhabiles.size(); indice++) {
				resultado  +=  "," + diasInhabiles.elementAt(indice).toString();
			}
		}
		return resultado;
	}//fin arma
	/**
	 * Metodo para cargar el dia 90
	 * @param habil dias habiles
	 * @return dia90 dia 90
	 */
	public String Cargar90(String habil) {
		String sqlDias;
		String dia90 = "";
		Connection Conexion = null;
		PreparedStatement qrDias;
		ResultSet rsDias;

		sqlDias = "select to_char(to_date('"+habil+"','dd/mm/yyyy') + 90, 'dd/mm/yyyy') from dual";
		try	{
			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			if(Conexion!=null) {
				qrDias = Conexion.prepareStatement( sqlDias );
				if(qrDias!=null) {
        			rsDias = qrDias.executeQuery();
					if(rsDias!=null) {
						rsDias.next();
						dia90  = rsDias.getString(1);
						rsDias.close();
					}else {
			  			EIGlobal.mensajePorTrace("puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					}
				}else {
       	  			EIGlobal.mensajePorTrace("puedo crear Query.", EIGlobal.NivelLog.ERROR);
				}
			}else {
				EIGlobal.mensajePorTrace("No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
			}
		}catch(SQLException sqle) {
			EIGlobal.mensajePorTrace("efectuo la sig. excepcion: ", EIGlobal.NivelLog.ERROR);
      		EIGlobal.mensajePorTrace( sqle.getMessage() , EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				Conexion.close();
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Cierre de conexion excepcion: ", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace( e.getMessage() , EIGlobal.NivelLog.ERROR);
			}
		}
		return(dia90);
	}//fin 90
	/**
	 * Metodo generado para cargar los dias en el vector
	 * @param formato
	 * @return
	 */
	public Vector CargarDias(int formato) {
		String sqlDias;
		Vector diasInhabiles = new Vector();
		Connection Conexion = null;
		PreparedStatement qrDias;
		ResultSet rsDias;
		sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
		try {
			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			if(Conexion!=null) {
				qrDias = Conexion.prepareStatement( sqlDias );
				//qrDias.setSQL(sqlDias);
				if(qrDias!=null) {
					rsDias = qrDias.executeQuery();
					if(rsDias!=null) {
						rsDias.next();
						do {
							String dia  = rsDias.getString(1);
							String mes  = rsDias.getString(2);
							String anio = rsDias.getString(3);
							if(formato == 0) {
								dia=Integer.toString( Integer.parseInt(dia) );
								mes=Integer.toString( Integer.parseInt(mes) );
								anio=Integer.toString( Integer.parseInt(anio) );
							}
							String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
							diasInhabiles.addElement(fecha);
						} while(rsDias.next());
						rsDias.close();
					}else {
						EIGlobal.mensajePorTrace("puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					}
				}else {
					EIGlobal.mensajePorTrace("puedo crear Query.", EIGlobal.NivelLog.ERROR);
				}
			} else {
				EIGlobal.mensajePorTrace("No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
			}
		}catch(SQLException sqle) {
			EIGlobal.mensajePorTrace("efectuo la sig. excepcion: ", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace( sqle.getMessage() , EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				Conexion.close();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("Cierre de conexion excepcion: ", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace( e.getMessage() , EIGlobal.NivelLog.ERROR);
			}
		}
		return(diasInhabiles);
	}//fin dias
	/**
	 * Metodo generado para rellenar caracter
	 * @param origen origen
	 * @param caracter caracter a rellenar
	 * @param cantidad cantidad a rellenar
	 * @param izquierda si se rellena a la izquierda
	 * @return
	 */
	public String rellenaCaracter(String origen, char caracter, int cantidad, boolean izquierda) {
		int cfin = cantidad - origen.length();
		if(origen.length() < cantidad) {
			for(int contador = 0 ; contador< cfin;contador++) {
				if(izquierda) {
					origen = caracter + origen;
				}else {
					origen = origen + caracter;
				}
			}
		}
		if(origen.length() > cantidad) {
			origen = origen.substring(0,cantidad);
		}
		return origen;
		}//fin rellena
	/**
	 * Metodo generado para rellenar caracteres HTML
	 * @param origen origen
	 * @param cantidad cantidad de relleno
	 * @return cadena rellena por caracteres html
	 */
	public String rellenaCaracterHTML(String origen,int cantidad) {
		if (origen.length()== 11) {
			origen = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+origen;
		}
		else if (origen.length() > cantidad) {
			origen = origen.substring(0,cantidad);
		}
		return origen;
	}//fin rellena
		/***************************************************************
                  fin agregado el 03/12/2003
                ***************************************************************/
	public String ObtenHora()
		{
		String hora = "";
		int horas, minutos;
		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();

		Cal.setTime(Hoy);
		horas = Cal.get(Calendar.HOUR_OF_DAY);
		minutos = Cal.get(Calendar.MINUTE);
		hora = (horas < 10 ? "0" : "") + horas + ":" + (minutos < 10 ? "0" : "") + minutos;
		return hora;
		}//fin obten
	/**
	 * metodo para cambiar / por -.
	 * @param buffer cadena completa
	 * @param Separador separador de cadena
	 * @return arregloSal cadena modificada
	 */
	public String desC ( String buffer, char Separador ) {
	 String bufRest;
	 int intNoCont = Integer.parseInt ( buffer.substring ( 0, buffer.indexOf ( Separador ) ) );
	 String arregloSal = "-";

	 bufRest = buffer.substring ( buffer.indexOf ( Separador ) + 1, buffer.length () );

	 for( int cont = 0; cont <= intNoCont; cont++ ) {
	     arregloSal = arregloSal + bufRest.substring ( 0, bufRest.indexOf ( Separador ) ) + "-";
	     bufRest = bufRest.substring ( bufRest.indexOf ( Separador) + 1, bufRest.length () );
	 }
	 return arregloSal;
     }// fin desen

	/**
	 * Metodo generado para obtener siguiente dia habil cuando sea un dia inhabil
	 * @param Inhabiles dia inhabil
	 * @return fecha dia habil
	 */
	public String ObtenSigDiaHabil(String Inhabiles) {
		EIGlobal.mensajePorTrace("***csaldo1.class Entrando al metodo ObtenSigDiaHabil()", EIGlobal.NivelLog.DEBUG);
		String diah;
		String mesh;
		String anioh;
		StringBuffer fecha = new StringBuffer();
		int contador=1;
		int DiaNumero;
		String NumeroDia;
		do
		{
			diah = getFecha("DIA",contador);
			mesh = getFecha("MES",contador);
			anioh = getFecha("ANIO",contador);
			NumeroDia = getFecha("NUMDAY",contador);
			DiaNumero = Integer.parseInt(NumeroDia);
			if (diah.length()==1) {
			   diah="0"+diah;
			}
                        if (mesh.length()==1) {
                           mesh="0"+mesh;
                        }
			fecha.append(diah);
			fecha.append(DIAGONAL);
			fecha.append(mesh);
			fecha.append(DIAGONAL);
			fecha.append(anioh);
			if(DiaNumero==Calendar.SATURDAY||DiaNumero==Calendar.SUNDAY)
			{
				fecha.append("25/12/2000");
			}
			contador++;
		} while (!esFechaHabil(fecha.toString(), Inhabiles));
		EIGlobal.mensajePorTrace("***El sig. dia habil es>"+fecha+"<", EIGlobal.NivelLog.INFO);
		return fecha.toString();
	}//fin siguiente
	/**
	 * Metodo para devolver el dia,a�o o mes de la fecha actual
	 * @param tipo dia a�o o mes
	 * @param suma dias a sumar
	 * @return strElemento fecha
	 */
	 public static String getFecha(String tipo, int suma) {
      EIGlobal.mensajePorTrace("***csaldo1.class Entrando al metodo getFecha()", EIGlobal.NivelLog.DEBUG);
      StringBuffer strElemento = new StringBuffer();
      Date Hoy = new Date();
      Calendar Cal = Calendar.getInstance();
      Cal.setTime(Hoy);
      int Dia = Cal.get(Calendar.DATE);
      int Mes = Cal.get(Calendar.MONTH);
      int anio = Cal.get(Calendar.YEAR);
      Cal.set(anio, Mes, Dia + suma);
      if ("DIA".equals(tipo)) {
         strElemento.append(Cal.get(Calendar.DATE));
      }

      if ("MES".equals(tipo)) {
         strElemento.append(Cal.get(Calendar.MONTH) + 1);
      }

      if ("ANIO".equals(tipo)) {
         strElemento.append(Cal.get(Calendar.YEAR));
      }

      if ("NUMDAY".equals(tipo)) {
         strElemento.append(Cal.get(Calendar.DAY_OF_WEEK));
         EIGlobal.mensajePorTrace("***getFecha() sale con 3>" + strElemento + "<", EIGlobal.NivelLog.INFO);
      }

      return strElemento.toString();
   }// fin actual
   /**
    *  Metodo generado para devolver verdadero si el dia es habil
    * @param fecha fecha a evaluar
    * @param Inhabiles dias inhabiles
    * @return result boolean
    */
   public static boolean esFechaHabil(String fecha, String Inhabiles) {
	   EIGlobal.mensajePorTrace("***csaldo1.class Entrando al metodo esFechaHabil() "+fecha+","+Inhabiles, EIGlobal.NivelLog.INFO);
	   String DiasInhabiles = "";
	   int index;
	   boolean result = true;
	   DiasInhabiles=Inhabiles;
	   if (!"/".equals(fecha.substring(2,3))) {
		   fecha = "0" + fecha;
	   }
	   if (!"/".equals(fecha.substring(5,6))) {
		   fecha = fecha.substring(0,2)+ "0" + fecha.substring(3,8);
	   }

	   index = DiasInhabiles.indexOf(fecha,0);
	   if(index >=0){
		   result=false;
	   }
	   return result;
   }//fin fechahabil

	/**
	 * Metodo especial para bitacorizar y grabar pistas de auditoria del cambio de menu de pyme
	 * la bitacora de Operaciones
	 * @param req Request del sistema
	 * @param estatus estatus que se grabara en la pista auditora
	 * @param bean
	 * @param opcion
	 */

    /**
     * Metodo especial para bitacorizar y grabar pistas de auditoria del cambio de menu de pyme
     * @param req Request del sistema
     * @param estatus estatus que se grabara en la pista auditora
     * @param opcion opcion si es entrada o salida del menu pyme
     */
    private void escribeBitacoraPyme(HttpServletRequest req, String estatus, String opcion,String contrato,String usuario){
		HttpSession sess = req.getSession();
		EIGlobal.mensajePorTrace("csaldo1::escribeBitacoraPyme:: Entrando ...", EIGlobal.NivelLog.DEBUG);
		if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			try{
				String mensaje="";
				String clave="";
				String datoActual="B";
				String datoNuevo="A";
				if("1".equals(opcion)){
					mensaje="Cambio a Menu Reducido Pyme";
					clave="EPYR";
				}else{
					mensaje="Cambio a Menu Completo Pyme";
					clave="EPYC";
					 datoActual="A";
					 datoNuevo="B";
				}
				/**INICIA TCT_BITACORA**/
				BitacoraBO.bitacoraTCT(req, sess, clave, obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), "", "", "TCT_0000", mensaje) ;
				/**TERMINA TCT_BITACORA**/

				/**INICIA EWEB_ADMIN_BITACORA**/
				BitaAdminBean beanAdmin = new BitaAdminBean();
				beanAdmin.setTipoOp(estatus);
				beanAdmin.setIdTabla(contrato+"-"+usuario);
				beanAdmin.setReferencia(100000);
				beanAdmin.setDato(datoActual);
				beanAdmin.setDatoNvo(datoNuevo);
				beanAdmin.setTabla("EWEB_PYME_CONTR");
				beanAdmin.setCampo("ID_ESTAT");
				if("1".equals(opcion)){
					BitacoraBO.bitacorizaAdmin(req, sess, "EMPY00", beanAdmin, true);
				}else{
					BitacoraBO.bitacorizaAdmin(req, sess, "EMPY01", beanAdmin, true);
				}
				/**TERMINA EWEB_ADMIN_BITACORA**/
			} catch (NumberFormatException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
	}
}