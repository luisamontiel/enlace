/*************************************************************************************************/
 /** Cat�logoN�minaInterb - Clase Principal para la administraci�n del nuevo Cat�logo de N�mina **/
 /** 						de empleados con cuentas interbancarias
 /** 	15/oct/2007  																			**/
 /** Nuevo Alcance																				**/
 /** 	05 - Febrero - 2008																		**/
 /** EVERIS MEXICO																				**/
 /************************************************************************************************/

package	mx.altec.enlace.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

import java.sql.*;
import javax.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/* everis JARA - Ambientacion - Librerias para Bitacora */
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.CatNominaDAO;
import mx.altec.enlace.bo.CatNominaAction;
import mx.altec.enlace.bo.CatNominaBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.CatNominaConstantes;
import mx.altec.enlace.bo.CatNominaValidaInterb;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/* everis JARA - Ambientacion - Servlet extiende de Base Servlet */
public class CatalogoNominaInterb extends BaseServlet
{
	boolean existReg = true;
	/* everis JARA - Ambientacion - Cambio de ruta de subida a /tmp/ */
	String rutadeSubida="/tmp/";
	ArrayList listaModifFinal = new ArrayList();
	ArrayList listaAltaFinal = new ArrayList();
	ArrayList listaModifInterb = new ArrayList();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		defaultAction (request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		defaultAction (request, response);
	}

	/***************************************************************
	 *** M�todo encargado de realizar los llamados a las 		****
	 *** funciones de Busqueda, Baja y Eliminaci�n de Empleados ****
	 *** con cuenta interb del nuevo Cat�logo de N�mina			****
	 **************************************************************/
	public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try{
			/* everis JARA - Ambientacion - Variables de session */
			System.out.println("-- JARA Amb INTERBANCARIA--");
			String numContrato="";
			String descContrato="";
			String usuario="";
			String clavePerfil="";

			System.out.println("");
			System.out.println("CatalogoNominaInterb $ [defaultAction] - Inicio");
			RequestDispatcher rd = null;
			String opcion = "";

			/* everis JARA - Ambientacion - Obtencion de session del BaseResource */
			System.out.println("-- JARA Amb Entorno de session--");
			HttpSession sessionHttp = request.getSession();
			BaseResource session = (BaseResource) sessionHttp.getAttribute("session");
			boolean sesionvalida=SesionValida( request, response );
			if(sesionvalida)
			{
				/* everis JARA - Ambientacion - Obtencion de variables de session */
				numContrato=(session.getContractNumber()==null)?"":session.getContractNumber();
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): contrato: "+numContrato,EIGlobal.NivelLog.INFO);

				descContrato=(session.getNombreContrato()==null)?"":session.getNombreContrato();
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): Descripcion de contrato: "+descContrato,EIGlobal.NivelLog.INFO);

				usuario=(session.getUserID8()==null)?"":session.getUserID8();
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): usuario: "+usuario,EIGlobal.NivelLog.INFO);
				clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): clavePerfil: "+clavePerfil,EIGlobal.NivelLog.INFO);
				//modulo=(String) request.getParameter("Modulo");
				//EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): el modulo es: "+modulo ,EIGlobal.NivelLog.INFO);
				/* everis JARA - Ambientacion - Se carga los atributos de men princiapal ******/
				try
					{
					request.setAttribute ("MenuPrincipal", session.getStrMenu ());
					request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
					}
				catch(Exception e)
					{
					e.printStackTrace();
					}
				/* everis JARA - Ambientacion -  Se carga los atributos de men princiapal ******/



				opcion = request.getParameter("opcion");

			//		 Si es la primera vez se carga la pagina por default
				if(opcion == null)
					opcion = "1";
				System.out.println("CatalogoNominaInterb $ [defaultAction] - Opcion = " + opcion);

				/***********************************************************
				 *** opcion 1 = Cargar p�gina de busqueda de Empleados.  ***
				 **********************************************************/
				if(opcion.equals("1"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Alta en L&iacute;nea Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Alta en L&iacute;nea", "s37140h", request ) );

					System.out.println("");
					System.out.println("CatalogoNominaInterb $ [defaultAction] - Entramos en opcion = 1");

					/* everis JARA - Ambientacion -  Redireccionando a jsp */
					evalTemplate("/jsp/CatNominaAltaOnlineInterb.jsp", request, response );

					System.out.println("CatalogoNominaInterb $ [defaultAction] - Salimos de Opcion = 1");
				}else

				/**************************************************
				 *** Opcion 2 = Realizar Busqueda de Empleados  ***
				 **************************************************/
				if(opcion.equals("2"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Alta en L&iacute;nea Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Alta en L&iacute;nea", "s37140h", request ) );

					System.out.println("");
					System.out.println("CatalogoNominaInterb $ [defaultAction] - Entramos en opcion = 2");
					//boolean usaCuenta = false;
					int usaCuenta = 0;
					boolean altaExitosa = false;
					CatNominaBean bean = new CatNominaBean();
					//String numContrato = request.getParameter("numContrato");

					// Obtebemos los valores ingresados por el usuario
					bean.setNumCuenta(request.getParameter("numCuenta"));
					bean.setNumEmpl(request.getParameter("numEmpl"));
					bean.setDeptoEmpl(request.getParameter("numDepto"));
					if(!(request.getParameter("sueldo").equals("")))
					//	bean.setIngresoMensual(Integer.parseInt(request.getParameter("sueldo")));
						bean.setIngresoMensual(request.getParameter("sueldo"));
					bean.setNombreEmpl(request.getParameter("nombre"));
					bean.setApellidoP(request.getParameter("apellidoP"));
					bean.setApellidoM(request.getParameter("apellidoM"));
					bean.setRFC(request.getParameter("rfc"));
					bean.setSexo(request.getParameter("sexo"));
					bean.setCalle(request.getParameter("calleNumero"));
					bean.setColonia(request.getParameter("colonia"));
					bean.setDelegacion(request.getParameter("delegMunicipio"));
					bean.setNombreCorto(request.getParameter("claveEstado"));
					bean.setCiudad(request.getParameter("ciudadPob"));
					bean.setCodigoPostal(request.getParameter("codigoPostal"));
					bean.setClavePais(request.getParameter("clavePais"));
					bean.setPrefTelPart(request.getParameter("prefTelCasa"));
					if(!(request.getParameter("telCasa").equals("")))
						bean.setTelPart(Integer.parseInt(request.getParameter("telCasa")));
					bean.setCalleOfi(request.getParameter("calleNumOfi"));
					bean.setColoniaOfi(request.getParameter("coloniaOfi"));
					bean.setDelegOfi(request.getParameter("delegMunOfi"));
					bean.setCveEstado(request.getParameter("claveEstadoOfi"));
					bean.setCiudadOfi(request.getParameter("ciudadPobOfi"));
					bean.setCodPostalOfi(request.getParameter("codigoPostalOfi"));
					bean.setPaisOfi(request.getParameter("clavePaisOfi"));
					if(!(request.getParameter("claveDireccion").equals("")))
					bean.setCveDireccionOfi(Integer.parseInt(request.getParameter("claveDireccion")));
					if(!(request.getParameter("prefTelOfi").equals("")))
						bean.setPrefTelOfi(Integer.parseInt(request.getParameter("prefTelOfi")));
					if(!(request.getParameter("telOfi").equals("")))
						bean.setTelOfi(Integer.parseInt(request.getParameter("telOfi")));
					if(!(request.getParameter("extOfi").equals("")))
						bean.setExtOfi(Integer.parseInt(request.getParameter("extOfi")));
					bean.setNumContrato(numContrato);
					bean.setCveUsuario(usuario);
					bean.setDescContrato(descContrato);

					System.out.println("CatalogoNominaInterb $ [defaultAction] - numContrato " + bean.getNumContrato());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - descContrato " + bean.getDescContrato());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - cveUsuario " + bean.getCveUsuario());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - numCta " + bean.getNumCuenta());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - numEmpl " + bean.getNombreEmpl());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - numDepto " + bean.getDeptoEmpl());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - sueldo" + bean.getIngresoMensual());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - nombre (" + bean.getNombreEmpl());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - apellidoP " + bean.getApellidoP());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - apellidoM " + bean.getApellidoM());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - rfc " + bean.getRFC());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - sexo " + bean.getSexo());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - calle " + bean.getCalle());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - colonia " + bean.getColonia());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - delegacion " + bean.getDelegacion());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - estado " + bean.getCveEstado());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - ciudad " + bean.getCiudad());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - cp " + bean.getCodigoPostal());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - clavePais " + bean.getClavePais());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - telCasa " + bean.getPrefTelPart());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - apellidoP " + bean.getTelPart());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - calleOfi " + bean.getCalleOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - coloniaOfi " + bean.getColoniaOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - delegOfi " + bean.getDelegOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - CveEstado " + bean.getCveEstadoOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - ciudadOfi " + bean.getCiudadOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - codigoPostal " + bean.getCodPostalOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - cveDireccionOfi " + bean.getCveDireccionOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - prefTelOfi " + bean.getPrefTelOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - telOfi " + bean.getTelOfi());
					System.out.println("CatalogoNominaInterb $ [defaultAction] - extOfi " + bean.getExtOfi());

					CatNominaValidaInterb validaInterb = new CatNominaValidaInterb();
					usaCuenta = validaInterb.validaCuentaInterb(bean.getNumContrato(), bean.getNumCuenta());

					//if(!usaCuenta)
					if (usaCuenta == -100)
					{
						System.out.println("CatalogoNominaEmplInterb $ [defaultAction] - Error Inesperado");
						generaPantallaError(request, response, "Fallo en el alta de registro Interbancario. Intente m&aacute;s Tarde.", "Alta en L&iacute;nea");
					}

					else
					{
						if(usaCuenta == 0)
						{

							CatNominaAction action = new CatNominaAction();


							/*****************************************
							 * Inicio Funcionalidad de TOKEN
							 * **************************************/

			                //interrumpe la transaccion para invocar la validaci�n para Alta de proveedor
			               /* if(  valida==null) {
			                    EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

			                    boolean solVal=ValidaOTP.solicitaValidacion(
			                            session.getContractNumber(),IEnlace.NOMINA_INTERBANCARIA);

			                    if( session.getValidarToken() &&
			                            session.getFacultad(session.FAC_VAL_OTP) &&
			                            session.getToken().getStatus() == 1 &&
			                            solVal) {
			                        EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
			                        //ValidaOTP.guardaParametrosEnSession(req);
			                        guardaParametrosEnSession(req);
			                        ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
			                    } else {
									ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
			                        valida="1";
								}
			                }
			                // retoma el flujo
			                if( valida!=null && valida.equals("1")) {*/


								altaExitosa = action.realizaAltaEmplInterb(bean);

								if(altaExitosa) {
									System.out.println("CatalogoNominaEmplInterb $ [defaultAction] - alta Exitosa");
									request.setAttribute("MensajeLogin01", " cuadroDialogo(\"Los Datos han sido guardados <br>exitosamente\", 1);");
									log ( "BITACORIZACION" );
									//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
									try{
								       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT ALTA EMPLEADO INTERB ONLINE",EIGlobal.NivelLog.INFO);
									  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
										EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
									  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
									  	BitaTCTBean beanTCT = new BitaTCTBean ();
									  	beanTCT.setReferencia(referencia);
									  	beanTCT.setTipoOperacion(BitaConstants.ES_NOMINA_ALTA_INTERBAN_LINEA);
									  	beanTCT.setConcepto(BitaConstants.ES_NOMINA_ALTA_INTERBAN_LINEA_MSG);
									  	beanTCT = bh.llenarBeanTCT(beanTCT);
									    BitaHandler.getInstance().insertBitaTCT(beanTCT);
						    		}catch(SQLException e){
						    			e.printStackTrace();
						    		}catch(Exception e){
						    			e.printStackTrace();
						    		}
						    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION ALTA EMPLEADO INTERB ONLINE",EIGlobal.NivelLog.INFO);
						    		//</VC>
									altaInterbCN(usuario, bean.getNumContrato(), request);

								}
								else {
									System.out.println("CatalogoNominaEmplInterb $ [defaultAction] - alta no exitosa");
									//request.setAttribute("MensajeLogin01", " cuadroDialogo(\"Los datos ingresados en el alta Interbancaria<br>no han podido ser registrados <br>Intente mas tarde\", 1);");
									generaPantallaError(request, response, "La informaci&oacute;n ingresada no pudo ser almacenada en la Base de Datos. Favor de intentar m�s Tarde", "Alta en L&iacute;nea");
							}

						   //}
								/*****************************************
								 * Finaliza Funcionalidad de TOKEN
								 * **************************************/
						}
						else
						{
							System.out.println("CatalogoNominaEmplInterb $ [defaultAction] - Relacion Existente");
							request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La Cuenta Interbancaria ingresada <br>ya est&aacute; registrada en el Cat&aacute;logo\", 1);");
						}
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaAltaOnlineInterb.jsp", request, response );
					}
					System.out.println("CatalogoNominaInterb $ [defaultAction] - Salimos de Opcion = 1");
				}else

				/*******************************************************************************
				 *  Opcion = 6 - Muestra pantalla de Modificaci�n Masiva de datos personales de
				 *  			 Empleados con cuenta Interbanciaria que no tienen alguna cuenta
				 *  			 interna activa en el nuevo cat�logo de n�mina
				 *******************************************************************************/
				if (opcion.equals("6"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Modificaci&oacute;n por Archivo", "s37160h", request ) );

					System.out.println("");
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Entramos Opcion = 6");
					/* everis JARA - Ambientacion -  Redireccionando a jsp */
					evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );
					System.out.println("CatalogoNominaInterb $ [defaultAction] - Salimos Opcion = 6");
				}else



				/******************************************************************
				 *  Opcion = 7 - Realiza la lectura del archivo con los registros
				 *  			que ser�n modificados con cuentas Interbancarias.
				 *  			Se realizar�n las respectivas validaciones
				 *******************************************************************/
				if(opcion.equals("7"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Modificaci&oacute;n por Archivo", "s37160h", request ) );

					System.out.println("");
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Entramos Opcion = 7");
					String tabla = "";
					CatNominaBean bean = new CatNominaBean();
					try {
						CatNominaAction action = new CatNominaAction();
						CatNominaValidaInterb validaInterb = new CatNominaValidaInterb();
						getFileInBytes(request);
						System.out.println("CatalogoNominaInterb $ [defaultAction] - Se carg� el archivo");
						String  nombreArchivo= (String)request.getAttribute("Archivo");//varible con el nombre del archivo que se esta mandando a llamar
						System.out.println("CatalogoNominaInterb $ [defaultAction] - NombreArchivo: "+ nombreArchivo);

						if(nombreArchivo!=null)
						{
							ArrayList erroresArchivoModif;
							bean = validaInterb.validaArchModifInterb("/tmp/" + nombreArchivo, numContrato, request);

							if(!bean.getFalloInesperado())
							{
								System.out.println(" --- CatalogoNominaInterb -Entramos a Fallo Inesperado != false");
								erroresArchivoModif = bean.getListaErrores();

								if(erroresArchivoModif.size() == 0)
								{
									listaModifInterb = bean.getListaRegistros();


									System.out.println(" --- CatalogoNominaInterb - *** - Tama�o de ArrayList " + listaModifInterb.size());

									if (bean.getTotalRegistros() <= 10) {

										tabla = action.creaDetalleModifImport(numContrato, bean.getListaRegistros(), CatNominaConstantes._MODIF);
										request.setAttribute("TablaRegistros", tabla);
									}
									else {
										String lineaCant = "";
													lineaCant += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
													lineaCant += "<tr><td>&nbsp;</td></tr>";
													lineaCant += "<tr>";
													lineaCant += "<td class=texfootpagneg align=center bottom=\"middle\">";
													lineaCant += "El total de registros a modificar son: " + bean.getTotalRegistros() + " <br><br>";
													lineaCant += "</td>";
													lineaCant += "</tr>";
													lineaCant += "</table>";


										//	 "<br><br> <td class='tittabdat' align=center bottom=\"middle\"> El total de registros a modificar son: "
										//						+ bean.getTotalRegistros() + "</td><br><br>";
										request.setAttribute("LineaCantidad", lineaCant);
									}
								}
								else
								{
									System.out.println("Erres archivo " + erroresArchivoModif.size());
									String Linea = "";
									Iterator it = erroresArchivoModif.iterator();
									int  i = 0;
									System.out.println("CatalogoNominaInterb  $ [defaultAction] - Validando lineas");
									while (it.hasNext()) {
										//System.out.println("CatalogoNominaInterb  $ en el while");
										if(i==0){
											it.next();
										}
										i++;

										if(erroresArchivoModif.get(0).equals("Long")){
											System.out.println("CatalogoNominaInterb  $ [defaultAction] - Datos");
											Linea += "Error en l&iacute;nea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
											request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Longitud de L&iacute;nea Incorrecta. <br><br>"
											+ Linea + ".\", 1);");

										}
										else if(erroresArchivoModif.get(0).equals("Datos")){
											System.out.println("CatalogoNominaInterb  $ [defaultAction] - Datos");
											Linea += "Error en l&iacute;nea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
											request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Tipo de dato incorrecto. <br><br>"
											+ Linea + ".\", 1);");

										}
										else if(erroresArchivoModif.get(0).equals("Cuenta")){
											System.out.println("CatalogoNominaInterb  $ [defaultAction] - Cuentas");
											Linea += "Error en l&iacute;nea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
											request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Cuenta en Existencia <br><br>"
											+ Linea + ".\", 1);");

										}
										else if(erroresArchivoModif.get(0).equals("Campo")){
											System.out.println("CatalogoNominaInterb $ [defaultAction] - Campo");

											String str= null;
											str = (String)(it.next());
											System.out.println("------ ModifArchivoInterb ... variable str:" + str);

											int pos=str.indexOf('@');

											String campo = "", registro = "";

											campo = str.substring(0, pos);
											registro = str.substring(pos+1);

											Linea += "Campo Obligatorio: <b><FONT COLOR = RED> " + campo + "</font></b>" + " en l&iacute;nea: " + registro + "  <br>";
											request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Hace falta campo obligatorio: <br><br>"
											+ Linea + ".\", 1);");

										}
										else if(erroresArchivoModif.get(0).equals("Cuenta Abono")){
											System.out.println("CatalogoNominaInterb $ [defaultAction] - Cuenta Abono");
											Linea += "Error en l&iacute;nea: <b><FONT COLOR = RED> " + (it.next()+ "</font></b><br>");
											request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> N&uacute;mero de cuenta menor a 18 d&iacute;gitos <br><br>"
											+ Linea + ".\", 1);");

										}
										else if(erroresArchivoModif.get(0).equals("Cuenta Interna")){
											System.out.println("CatalogoNominaInterb $ [defaultAction] - Cuenta Abono");
											Linea += "Error en l&iacute;nea: <b><FONT COLOR = RED> " + (it.next()+ "</font></b><br>");
											request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> No se puede realizar Modificaci&oacute;n, cuenta asociada con otra cuenta Interna. <br><br>"
											+ Linea + ".\", 1);");

										}
									}

								}
						//		/* everis JARA - Ambientacion -  Redireccionando a jsp */
						//		evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );
							}// Control de Excepciones
							else{
								generaPantallaError(request, response, "Fallo al momento de realizar validaciones contra la Base de Datos. <br> Favor de Intentar m&aacute;s tarde.", "Modificaci&oacute;n por Archivo");
							}
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					if(!bean.getFalloInesperado()){
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );
					}
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Fin Opcion 7");
				}


				/********************************************************************
				 *  Opcion = 8 - Realizar Modificaciones despu�s de haber sido		*
				 *  			validado y mostrado el detalle de la importaci�n	*
				 *  			de registros con modificaciones					  	*
				 *******************************************************************/
				else if (opcion.equals("8"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Modificaci&oacute;n por Archivo", "s37160h", request ) );

					System.out.println("");
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Inicio Opcion = 8");
					//CargaArchivo cFile = new CargaArchivo();
					//cFile.cargaArchivo(request, "/tmp/", "ModifEmpleados.txt");
					CatNominaAction action = new CatNominaAction();
					boolean re;


					/*****************************************
					 * Inicio Funcionalidad de TOKEN
					 * **************************************/

		            //interrumpe la transaccion para invocar la validaci�n para Alta de proveedor
		            /*if(  valida==null) {
		                EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

		                boolean solVal=ValidaOTP.solicitaValidacion(
		                        session.getContractNumber(),IEnlace.NOMINA_INTERBANCARIA);

		                if( session.getValidarToken() &&
		                        session.getFacultad(session.FAC_VAL_OTP) &&
		                        session.getToken().getStatus() == 1 &&
		                        solVal) {
		                    EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
		                    //ValidaOTP.guardaParametrosEnSession(req);
		                    guardaParametrosEnSession(req);
		                    ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
		                } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
		                    valida="1";
						}
		            }
		            // retoma el flujo
		            if( valida!=null && valida.equals("1")){*/


						re = action.modificaDatosArchInterb(numContrato, usuario, listaModifInterb);
						if (re) {
							request.setAttribute("MensajeLogin01"," cuadroDialogo(\"La modificaci&oacute;n se realiz&oacute; <br> exitosamente.\", 1);");
							log ( "BITACORIZACION" );
							modificaInterbCN(usuario, numContrato,  request);
							/* everis JARA - Ambientacion -  Redireccionando a jsp */
							//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
							try{
						       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT MODIFICA DATOS INTERB ONLINE",EIGlobal.NivelLog.INFO);
							  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean beanTCT = new BitaTCTBean ();
							  	beanTCT.setReferencia(referencia);
							  	beanTCT.setTipoOperacion(BitaConstants.ES_NOMINA_MOD_EMPL_INTERBAN_ARCH);
							  	beanTCT.setConcepto(BitaConstants.ES_NOMINA_MOD_EMPL_INTERBAN_ARCH_MSG);
							  	beanTCT = bh.llenarBeanTCT(beanTCT);
							    BitaHandler.getInstance().insertBitaTCT(beanTCT);
				    		}catch(SQLException e){
				    			e.printStackTrace();
				    		}catch(Exception e){
				    			e.printStackTrace();
				    		}
				    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION MODIFICA DATOS INTERB ONLINE",EIGlobal.NivelLog.INFO);
				    		//</VC>
							evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );

						}else{
							generaPantallaError(request, response, "Fallo en la realizaci�n de la operaci&oacute;n con la Base de Datos. Intente m�s Tarde", "Modificaci&oacute;n por Archivo");
						}


						System.out.println("CatalogoNominaInterb  $ [defaultAction] - Fin Opcion 8");

					//}
						/*****************************************
						 * Finaliza Funcionalidad de TOKEN
						 * **************************************/

				}else




				//		ALTA ARCHIVO INTERBANCARIA*******************

				/******************************************************************
				 *  Opcion = 12 - Muestra pantalla de Alta por Archivo Interbancaria
				 *******************************************************************/
				if (opcion.equals("12"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Alta por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Alta por Archivo", "s37150h", request ) );
					System.out.println("");
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Entramos Opcion = 12");
					/* everis JARA - Ambientacion -  Redireccionando a jsp */
					evalTemplate("/jsp/CatNominaAltaArchivoInterb.jsp", request, response );
					System.out.println("CatalogoNominaInterb $ [defaultAction] - Salimos Opcion = 12");
				}else



				/******************************************************************
				 *  Opcion = 13 - Importaci�n y validaciones de archivo para
				 *  			generar el alta por archivo Interbancaria
				 *******************************************************************/
				if (opcion.equals("13"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Alta por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Alta por Archivo", "s37150h", request ) );
					System.out.println("");
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Entramos Opcion = 13");
					String tabla = "";

					try
					{
						CatNominaAction action = new CatNominaAction();
						CatNominaValidaInterb validaInterb = new CatNominaValidaInterb();

						CatNominaBean bean = new CatNominaBean();
						getFileInBytes(request);

						System.out.println("CatalogoNominaInterb $ [defaultAction] - Se carg� el archivo");
						String  nombreArchivo= (String)request.getAttribute("Archivo");				//varible con el nombre del archivo que se esta mandando a llamar
						System.out.println("CatalogoNominaInterb $ [defaultAction] - NombreArchivo: "+ nombreArchivo);

						if(nombreArchivo!=null)
						{
							ArrayList erroresArchivo;
							//bean = action.validaArchivoAlta("/tmp/" + nombreArchivo, numContrato, request);
							bean = validaInterb.validaArchivoAlta("/tmp/" + nombreArchivo, numContrato, request);
							erroresArchivo = bean.getListaErrores();

							if(erroresArchivo.size() == 0)
							{
								listaAltaFinal = bean.getListaRegistros();

								if (bean.getTotalRegistros() <= 10)
								{
									tabla = action.creaDetalleAltaImp(numContrato, bean.getListaRegistros(), CatNominaConstantes._ALTA_INTERB);
									request.setAttribute("TablaRegistros", tabla);
								}
								else
								{
									String lineaCant = "";
												lineaCant += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
												lineaCant += "<tr><td>&nbsp;</td></tr>";
												lineaCant += "<tr>";
												lineaCant += "<td class=texfootpagneg align=center bottom=\"middle\">";
												lineaCant += "El total de registros a dar de alta son: " + bean.getTotalRegistros() + " <br><br>";
												lineaCant += "</td>";
												lineaCant += "</tr>";
												lineaCant += "</table>";

										//<br><br> El total de registros para dar de alta son: " + bean.getTotalRegistros() + " <br><br>";
									request.setAttribute("LineaCantidad", lineaCant);
								}
							}
							else
							{
								String Linea = "";
								Iterator it = erroresArchivo.iterator();


								int i = 0;
								while (it.hasNext()) {
									if(i==0){
										it.next();
									}
									i++;
									if(erroresArchivo.get(0).equals("Long")){
										System.out.println("CatalogoNominaInterb  $ [defaultAction] - Longitud");
										Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Longitud de L&iacute;nea Incorrecta. <br>"
										+ Linea + ".\", 1);");
									}
									// Codifo Bancario Santander
									else if(erroresArchivo.get(0).equals("CodBancario")){
										System.out.println("CatalogoNominaInterb  $ [defaultAction] - CodBancario");
										Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> C&oacute;digo Bancario incorrecto . <br>"
										+ Linea + ".\", 1);");
									}
									else if(erroresArchivo.get(0).equals("Datos")){
										System.out.println("CatalogoNominaInterb  $ [defaultAction] - Datos");
										Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Tipo de dato incorrecto. <br>"
										+ Linea + ".\", 1);");
									}
									else if(erroresArchivo.get(0).equals("Cuentas")){
										System.out.println("CatalogoNominaInterb  $ [defaultAction] - Cuentas");
										Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Cuenta en Existencia. <br>"
										+ Linea + ".\", 1);");
									}
									else if(erroresArchivo.get(0).equals("Campo")){
										System.out.println("CatalogoNominaInterb $ [defaultAction] - Campo");

										String str= null;
										str = (String)(it.next());
										System.out.println("------ AltaArchivoInterb ... variable str:" + str);

										int pos=str.indexOf('@');

										String campo = "", registro = "";

										campo = str.substring(0, pos);
										registro = str.substring(pos+1);

										Linea += "Campo Obligatorio: <b><FONT COLOR = RED> " + campo + "</font></b>" + " en l&iacute;nea: " + registro + "  <br>";
										request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Hace falta campo obligatorio. <br>"
										+ Linea + ".\", 1);");

									}
									else if(erroresArchivo.get(0).equals("Cuenta Abono")){
										System.out.println("CatalogoNominaInterb $ [defaultAction] - Cuenta Abono");
										System.out.println("CatalogoNominaInterb $ [defaultAction] - Valor de i" + i);
										Linea += "Error en l&iacute;nea: <b><FONT COLOR = RED> " + (it.next()+ "</font></b><br>");
										request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  N&uacute;mero de cuenta menor a 18 d&iacute;gitos. <br>"
										+ Linea + ".\", 1);");
									}
								}

							}
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					/* everis JARA - Ambientacion -  Redireccionando a jsp */
					evalTemplate("/jsp/CatNominaAltaArchivoInterb.jsp", request, response );
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Fin Opcion 13");
				}

				/********************************************************************
				 *  Opcion = 14 - Realizar el Alta despu�s de haber sido		*
				 *  			validado y mostrado el detalle de la importaci�n	*
				 *******************************************************************/
				else if (opcion.equals("14"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Alta por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Empleados > Mantenimiento Interbancario > Alta por Archivo", "s37150h", request ) );
					System.out.println("");
					System.out.println("CatalogoNominaInterb  $ [defaultAction] - Inicio Opcion = 14");

					CatNominaAction action = new CatNominaAction();
					boolean re;


					/*****************************************
					 * Inicio Funcionalidad de TOKEN
					 * **************************************/

		            //interrumpe la transaccion para invocar la validaci�n para Alta de proveedor
		            /*if(  valida==null) {
		                EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

		                boolean solVal=ValidaOTP.solicitaValidacion(
		                        session.getContractNumber(),IEnlace.NOMINA_INTERBANCARIA);

		                if( session.getValidarToken() &&
		                        session.getFacultad(session.FAC_VAL_OTP) &&
		                        session.getToken().getStatus() == 1 &&
		                        solVal) {
		                    EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
		                    //ValidaOTP.guardaParametrosEnSession(req);
		                    guardaParametrosEnSession(req);
		                    ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
		                } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
		                    valida="1";
						}
		            }
		            // retoma el flujo
		            if( valida!=null && valida.equals("1")){*/


						re = action.altaDatosArchivo(numContrato, usuario, descContrato, listaAltaFinal);
						if (re) {
							request.setAttribute("MensajeLogin01"," cuadroDialogo(\"El Alta de registros se realiz&oacute; <br> exitosamente.\", 1);");
							log ( "BITACORIZACION" );
							altaInterbCN(usuario, numContrato, request);
							//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
							try{
						       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT ALTA EMPLEADO INTERB ARCHIVO",EIGlobal.NivelLog.INFO);
							  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean beanTCT = new BitaTCTBean ();
							  	beanTCT.setReferencia(referencia);
							  	beanTCT.setTipoOperacion(BitaConstants.ES_NOMINA_ALTA_INTERBAN_ARCH);
							  	beanTCT.setConcepto(BitaConstants.ES_NOMINA_ALTA_INTERBAN_ARCH_MSG);
							  	beanTCT = bh.llenarBeanTCT(beanTCT);
							    BitaHandler.getInstance().insertBitaTCT(beanTCT);
				    		}catch(SQLException e){
				    			e.printStackTrace();
				    		}catch(Exception e){
				    			e.printStackTrace();
				    		}
				    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION ALTA EMPLEADO INTERB ARCHIVO",EIGlobal.NivelLog.INFO);
				    		//</VC>
							/* everis JARA - Ambientacion -  Redireccionando a jsp */
							evalTemplate("/jsp/CatNominaAltaArchivoInterb.jsp", request, response );

						}else{
							//request.setAttribute("MensajeLogin01"," cuadroDialogo(\"Se genero un error, El alta no se realiz&oacute;.\", 3);");
							generaPantallaError(request, response, "Fallo en el alta de Registros en la Base de Datos. Favor de Intentar m&aacute;s Tarde", "Alta por Archivo");
						}

						System.out.println("CatalogoNominaInterb  $ [defaultAction] - Fin Opcion 14");
           			//}
						/*****************************************
						 * Finaliza Funcionalidad de TOKEN
						 * **************************************/

				}
				/* everis JARA - Ambientacion - En caso de error*/
				else{
					despliegaPaginaError("Error inesperado.",
					"B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina",
					"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Cat&aacute;logo de N&oacute;mina &gt; B&uacute;squeda",
					request, response );
					}

			}/* everis JARA - Ambientacion - CERRANDO EL IF*/

		}/* everis JARA - Ambientacion - En caso de error GENERAL*/
		catch(Exception e){
	  		System.out.println("\n Hubo un error : \n" );
	  		e.printStackTrace();

	  		despliegaPaginaError("Error inesperado.",
					"Cat&aacute;logo de N&oacute;mina &gt; Mantenimiento Interbancario",
					"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Mantenimiento Interbancario",
					request, response );

	  		}


	}



	/*******************************************************************/
	/** Metodo: getFileInBytes para realizar importaciones de archivos.
	 * @return byte[]
	 * @param HttpServletRequest -> request
	/********************************************************************/
	public byte[] getFileInBytes ( HttpServletRequest request) throws IOException, SecurityException
	{
		String sFile = "";
	    System.out.println("CatalogoNominaEmpl  $ [GetFileInBytes] - Inicio ");
		//MultipartRequest mprFile = new MultipartRequest ( request, rutadeSubida, 2097152 );

		FileInputStream entrada;

		sFile = getFormParameter(request, "fileName");
	    //sFile = mprFile.getFilesystemName ( name );
	    System.out.println("CatalogoNominaEmpl  $ [GetFileInBytes] - S1 : " + sFile);
	    request.setAttribute("Archivo", sFile);


		 File fArchivo = new File ( ( rutadeSubida + "/" + sFile ).intern () );
		 byte[] aBArchivo = new byte[ ( int ) fArchivo.length () ];
		 entrada = new FileInputStream ( fArchivo );
		 byte bByte = 0;
		 try{
		 for ( int i = 0; ( ( bByte = ( byte ) entrada.read () ) != -1 ); i++ ) {
		     aBArchivo[ i ] = bByte;
		 }
			 } catch(Exception e) {
				 e.printStackTrace();
				 throw new IOException();
		 } finally {
			 entrada.close ();
			     //fArchivo.delete ();
		 }

		 //Enumeration e = mprFile.getParameterNames();
		 HashMap parametros = getFormParameters(request);
		 Iterator e = parametros.entrySet().iterator();

		 while(e.hasNext()) {
			 String parametro = (String) ((Map.Entry) e.next()).getKey();
			 //System.out.println("CatalogoNominaEmpl  $ [defaultAction] - paramtro: "+ parametro );
			 request.setAttribute(parametro, parametros.get(parametro));
		 }
		 System.out.println("CatalogoNominaEmpl  $ [getFileInBytes] - Fin ");
		 return aBArchivo;
	}


		/********************************************/
		/** Metodo: altaInterbCN
		 * Bitacorizaci�n para Alta de registro
		 */
		/********************************************/


	    	public static void altaInterbCN(String usuario, String contrato, HttpServletRequest request)
			throws ServletException, IOException{

			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");


			if(Global.USAR_BITACORAS.trim().equals("ON")){

				System.out.println("La operaci�n es un alta Interbancaria" );

				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNI");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);
				System.out.println("El usuario es:" + bt.getUsr());
				bt.setContrato(contrato);
				System.out.println("El contrato:" + bt.getContrato());


				try{

						System.out.println("BitaConstants.SESS_ID_FLUJO:" + BitaConstants.SESS_ID_FLUJO);
						bt.setNumBit(BitaConstants.EC_NOMINA_PROPIA_ALTA);
						System.out.println("Se realiza la bitacorizacion:" + bt.getNumBit());

					BitaHandler.getInstance().insertBitaTransac(bt);

				}catch (SQLException e){
					e.printStackTrace();
				}catch (Exception e){
					e.printStackTrace();
				}
			}


		}





		 /********************************************/
		/** Metodo: consultaInterbCN
		 * Bitacorizaci�n para Consulta de registro
		 */
		/********************************************/


		public void consultaInterbCN(String usuario, String contrato, HttpServletRequest request)
		throws ServletException, IOException{

			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			System.out.println("La operaci�n es una consulta Interbancaria");

			if(Global.USAR_BITACORAS.trim().equals("ON")){

				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNI");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);
				System.out.println("El usuario es:" + bt.getUsr());
				bt.setContrato(contrato);
				System.out.println("Los datos a consultar:" + bt.getContrato());


				try{

						System.out.println("BitaConstants.SESS_ID_FLUJO:" + BitaConstants.SESS_ID_FLUJO);
						bt.setNumBit(BitaConstants.EC_NOMINA_INTERB_CONSULTA);
						System.out.println("Se realiza la bitacorizacion:" + bt.getNumBit());

					BitaHandler.getInstance().insertBitaTransac(bt);

				}catch (SQLException e){
					e.printStackTrace();
				}catch (Exception e){
					e.printStackTrace();
				}
			}


		}




		/********************************************/
		/** Metodo: modificaInterbCN
		 * Bitacorizaci�n para Modificacion de registro
		 */
		/********************************************/


		public void modificaInterbCN(String usuario, String contrato, HttpServletRequest request)
			throws ServletException, IOException{

			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			System.out.println("La operaci�n es una modificacion Interbancaria");


			if(Global.USAR_BITACORAS.trim().equals("ON")){

				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNI");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);
				System.out.println("El usuario es:" + bt.getUsr());
				bt.setContrato(contrato);
				System.out.println("Los datos a consultar:" + bt.getContrato());


				try{
						System.out.println("BitaConstants.SESS_ID_FLUJO:" + BitaConstants.SESS_ID_FLUJO);

						bt.setNumBit(BitaConstants.EC_NOMINA_INTERB_MODIFICA);
						System.out.println("Se realiza la bitacorizacion:" + bt.getNumBit());

					BitaHandler.getInstance().insertBitaTransac(bt);

				}catch (SQLException e){
					e.printStackTrace();
				}catch (Exception e){
					e.printStackTrace();
				}
			}


		}

		public void generaPantallaError(HttpServletRequest request, HttpServletResponse response, String mensaje, String funcion) throws Exception
		{
			System.out.println("CatalogoNominaEmpl $ [generaPantallaError] - Inicio");
			despliegaPaginaError(mensaje,
			"Cat&aacute;logo de N&oacute;mina",
			"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Mantenimineto Interbancario &gt; " + funcion,
			request, response );
			//request.setAttribute("MensajeLogin01"," cuadroDialogo(\"Se genero un error, La b�squeda no pudo ser realizada. Intente m�s Tarde;.\", 3);");
			System.out.println("CatalogoNominaInterb  $ [generaPantallaError] - Fin");
		}







}





