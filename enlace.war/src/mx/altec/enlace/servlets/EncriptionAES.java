package mx.altec.enlace.servlets;

import java.awt.Desktop;

import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.paddings.BlockCipherPadding;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import sun.misc.BASE64Encoder;


public class EncriptionAES extends BaseServlet{

	private static final String ET_PORTAL_NEGOCIO_INTERNACIONAL = "PCIM";

	private final BlockCipher AESCipher = new AESEngine();

	//PKCS7Padding por default. se puede cambiar con el setter
	private PaddedBufferedBlockCipher pbbc = new PaddedBufferedBlockCipher(AESCipher, new PKCS7Padding());
	private KeyParameter key;
	//comentario
	public EncriptionAES(byte[] key){
		this.key = new KeyParameter(key);
	}

	public EncriptionAES(){
	}

	public void setPadding(BlockCipherPadding bcp) {
		this.pbbc = new PaddedBufferedBlockCipher(AESCipher, bcp);
	}

	public void setKey(byte[] key) {
		this.key = new KeyParameter(key);
	}

	public byte[] encrypt(byte[] input){
		try{}
		catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);		
		}		
		return processing(input, true);
	}

	public byte[] decrypt(byte[] input) {
		try{}
		catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);		
		}
		return processing(input, false);
	}

	private byte[] processing(byte[] input, boolean encrypt){

		byte[] output = null;

		try{
			pbbc.init(encrypt, key);

			output = new byte[pbbc.getOutputSize(input.length)];
			int bytesWrittenOut = pbbc.processBytes(input, 0, input.length, output, 0);
			pbbc.doFinal(output, bytesWrittenOut);
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);		
		}

		return output;
	}

	/**
	 * Metodo que obtiene el valor de la llave para la encriptacion
	 * @return valorParam				Valor de la llave encriptacion
	 */
	private String getValorLlave(){

		ResultSet rs = null;
		String query = null;
		String valorParam = null;

		try{
			Connection conn= createiASConn (Global.DATASOURCE_ORACLE);
			Statement sDup = conn.createStatement();

			// Se ejecuta el query a la base de datos para obtener el valor de la llave que se usa para encriptar
			query = "select VALOR_CADENA from STPROD.EWEB_PARAMETROS where nombre_param='LLAVEENCRI'";

			rs = sDup.executeQuery(query);

			if(rs.next()){
				valorParam = rs.getString("VALOR_CADENA");
			}

			EIGlobal.mensajePorTrace("Valor de valorParam: " + valorParam, EIGlobal.NivelLog.DEBUG);

			// Se relaliza el cierre de las conexiones
			rs.close();
			sDup.close();
			conn.close();

		}catch(Exception e){
			valorParam = "0";
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);		
		}
		return valorParam;
	}

	/**
	 * Metodo doGet existente para Servlet
	 * 
	 * @param request		Request del sistema
	 * @param response		Response del sistema 
	 * @throws ServletException	Excepcion lanzada por el servlet 
	 * @throws IOException	Excepcion lanzada por sistema
	 */
	public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Metodo Encriptar Token
	 * 
	 * @param request		Request del sistema
	 * @param response		Response del sistema
	 * @throws ServletException	Excepcion lanzada por el servlet 
	 * @throws IOException	Excepcion lanzada por sistema 
	 */
	public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String contrato = session.getContractNumber();
		String usuario = session.getUserID();

		try{
			//Se construye el token mediante el metodo de encriptacion AES
			String token_b64 = cifraTokenB64(getValorLlave(), contrato, "   ", usuario, "MEX", "ES", "   ", "   ");

			// Se escribe en el log de la aplicacion los valores de la encriptacion, contrato, usuario y token
			EIGlobal.mensajePorTrace("Llave de encriptacion" + getValorLlave(),    EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Contrato" + contrato,    EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("usuario" + usuario,    EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Token" + token_b64,    EIGlobal.NivelLog.DEBUG);

			//Codigo para invocar el metodo de Bitacorizacion y pistas de auditoria
			escribeBitacoraEncription(request, response, "A");

			// Se inicializa la variable con el valor de la URL de produccion
			String urlPortalInternacional = "https://www.santandertrade.com";

			// Invoca al metodo de cierre de duplicidad de sesion
			int resDup = cierraDuplicidad(session.getUserID8());

			// Borrado de la sesion
			sess.setAttribute("session", session);
			sess.removeAttribute("session");
			sess.setMaxInactiveInterval( 0 );
			session.setSeguir(false);
			sess.invalidate();

			// Guarda en el request la URL de Portal
			request.setAttribute("urlPortalTDP", urlPortalInternacional);
			request.setAttribute("token_b64",token_b64);

			// Se realiza direccionamiento para ejecutar pagina que realiza el sign-off y el envio a Portal
			evalTemplate("/jsp/Encriptacion.jsp",request,response);
		}catch(Exception e){
			//Codigo para invocar el metodo de Bitacorizacion y pistas de auditoria
			escribeBitacoraEncription(request, response, "R");
			
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);		
		}
	}


	/**
	 * Metodo Encriptar Token
	 * 
	 * @param key_hex		Llave para encripcion
	 * @param companyId		Valor de compañia
	 * @param companyName	Valor del nombre de compañia
	 * @param userId		Valor de usuario
	 * @param country		Valor del pais
	 * @param userName		Valor con nombre de usuario
	 * @param language		Valor del lenguaje
	 * @param userGender	Valor con el genero		 
	 * @return token 		Regresa el valor del token
	 */
	public String cifraTokenB64(String key_hex, String companyId, String companyName, String userId, String country, String language, String userName, String userGender){
		String token = null;

		try {
			// Se cambia el formato de fecha a GMT
			SimpleDateFormat formateo = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formateo.setTimeZone(TimeZone.getTimeZone("GMT"));		

			// Se inicia la construccion del XML para generar el token
			Document documento = DocumentHelper.createDocument();	
			documento.setXMLEncoding("iso-8859-1");
			Element root = documento.addElement("profile");
			root.addElement("gmtDateTime").addText(formateo.format(new Date()));
			root.addElement("companyId").addText(companyId);

			if (companyName != null && !("".equals(companyName))){
				root.addElement("companyName").addText(companyName);
			}

			root.addElement("userId").addText(userId);
			root.addElement("country").addText(country);

			if (language != null && !("".equals(language))){
				root.addElement("language").addText(language);
			}

			if (userName != null && !("".equals(userName))){
				root.addElement("userName").addText(userName);
			}

			if (userGender != null && !("".equals(userGender))){
				root.addElement("userGender").addText(userGender);
			}

			byte[] decodedHex = Hex.decode(key_hex);

			// Se realiza la encriptacion mediante el algotirmo AES
			EncriptionAES aesHelper = new EncriptionAES(decodedHex);
			token =  new BASE64Encoder().encode(aesHelper.encrypt((documento.asXML()).getBytes()));

			HttpServletRequest request=null;
			HttpServletResponse response=null;

		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return token;
	}

	/**
	 * Metodo especial para bitacorizar y grabar pistas de auditoria para la redireccion a Portal TDP
	 * la bitacora de Operaciones
	 *
	 * @param req				Request del sistema
	 * @param res				Response del sistema
	 *
	 */
	private void escribeBitacoraEncription(HttpServletRequest req, HttpServletResponse res, String estatus){
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("EncriptionAES::escribeBitacoraEncription:: Entrando ...", EIGlobal.NivelLog.DEBUG);
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				if(getFormParameter(req,BitaConstants.FLUJO) != null){
					bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.FLUJO));
				}else{
					bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				}
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);

				BitaTCTBean beanTCT = new BitaTCTBean ();
				beanTCT = bh.llenarBeanTCT(beanTCT);

				beanTCT.setTipoOperacion(ET_PORTAL_NEGOCIO_INTERNACIONAL);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					beanTCT.setNumCuenta(session.getContractNumber().trim());
				}

				if (session.getUserID8() != null) {
					bt.setCodCliente(session.getUserID8().trim());
					beanTCT.setUsuario(session.getUserID8().trim());
					beanTCT.setOperador(session.getUserID8().trim());
				}

				if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
						&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
						.equals(BitaConstants.VALIDA)) {
					bt.setIdToken(session.getToken().getSerialNumber());
				}

				//Calculando referencia
				ServicioTux tuxGlobal = new ServicioTux();
				Integer referencia = null;
				Hashtable<?, ?> hs = null;
				String errorRegreso = null;
				try {
					EIGlobal.mensajePorTrace("EncriptionAES::escribeBitacoraEncription:: Llamado al servicio SREFERENCIA",    EIGlobal.NivelLog.DEBUG);
					hs = tuxGlobal.sreferencia("901");
					errorRegreso = hs.get("COD_ERROR").toString();
					EIGlobal.mensajePorTrace("EncriptionAES::escribeBitacoraEncription:: Codigo de Error llamado->"+ errorRegreso, EIGlobal.NivelLog.DEBUG);
					referencia = (Integer) hs.get("REFERENCIA");
					EIGlobal.mensajePorTrace("EncriptionAES::escribeBitacoraEncription:: referencia->"+ referencia, EIGlobal.NivelLog.DEBUG);                       
				} catch (Exception e) {
                    	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					errorRegreso = null;
					referencia = 0;
				}

				bt.setReferencia(referencia.longValue());				//Llenado de referencia				
				bt.setIdErr(ET_PORTAL_NEGOCIO_INTERNACIONAL + "0000");	//Llenado del codigo de error
				beanTCT.setCodError(ET_PORTAL_NEGOCIO_INTERNACIONAL + "0000");
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion(ET_PORTAL_NEGOCIO_INTERNACIONAL);

				bt.setIdFlujo(ET_PORTAL_NEGOCIO_INTERNACIONAL);
				bt.setNumBit(ET_PORTAL_NEGOCIO_INTERNACIONAL + "01");
				bt.setEstatus(estatus); //Se envia estatus (Aceptada o Cancelada)

				BitaHandler.getInstance().insertBitaTransac(bt); //Llamado para pistas de auditoria
				BitaHandler.getInstance().insertBitaTCT(beanTCT); //Llamado para bitacorizacion
				EIGlobal.mensajePorTrace("EncriptionAES::escribeBitacoraEncription:: Bitacorizando " + ET_PORTAL_NEGOCIO_INTERNACIONAL, EIGlobal.NivelLog.DEBUG);

			} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
	}	
}