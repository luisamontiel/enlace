package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class ChesBenefModif extends BaseServlet{
    String fecha_hoy = "";
    String strFecha = "";
    String strDebug="";
    String VarFechaHoy="";
    short suc_opera = (short)787;

	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session"); 		String usuario = "";
		String contrato = "";
		String clave_perfil = "";

		if( SesionValida( req, res )){
			usuario = session.getUserID8();
		    contrato = session.getContractNumber();
			clave_perfil = session.getUserProfile();
		    suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &Inicia clase&", EIGlobal.NivelLog.INFO);
			String ventana = (String) req.getParameter("ventana");
			EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &ventana:"+ventana+"&", EIGlobal.NivelLog.INFO);
			if(ventana.equals("0"))
		    {
//			   if (session.getFacultad(session.FAC_BENEF_NOREG))
//			   {
				inicia(clave_perfil, contrato, usuario,  req, res );
//               }
//			   else
//               {
//                req.setAttribute("Error","No tiene facultad para dar de alta beneficiarios");
//				  despliegaPaginaError("No tiene facultad para dar de alta beneficiarios","Alta de Beneficiarios Cheque Seguridad","Servicios &gt; Cheque Seguridad &gt; Mantenimiento de Beneficiarios &gt; Alta",req,res);
//			   }
		    }
			if(ventana.equals("1"))
		    {
			    modificar(clave_perfil, contrato, usuario, req, res );
		    }
		}
		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &Termina clase&", EIGlobal.NivelLog.INFO);

    }

	/*-----------------------------------------------------------------------*/
	public int inicia(String usuario,String clave_perfil,String contrato, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
   	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		int EXISTE_ERROR = 0;
		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &inicia m�todo inicia()&", EIGlobal.NivelLog.INFO);
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		EXISTE_ERROR =0;
		String cadenaBeneficiarios = listarBeneficiarios(contrato,usuario, clave_perfil, suc_opera);
		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);
		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);

		req.setAttribute("FechaHoy", ObtenFecha(false));
		//req.setAttribute("MenuPrincipal", session.getstrMenu());
		req.setAttribute("ContUser",ObtenContUser(req));
		req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Modificaci&oacute;n de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n","s25970h",req) );

		if (EXISTE_ERROR == 1 )
			despliegaPaginaError(cadenaBeneficiarios,"Modificaci&oacute;n de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n", req, res);
		else
			evalTemplate("/jsp/ChesBenefModif.jsp", req, res );

		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &termina m�todo inicia()&", EIGlobal.NivelLog.INFO);
		return 1;
	}
	/*-----------------------------------------------------------------------------*/
	public int modificar(String clave_perfil,String contrato, String usuario,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &inicia m�todo modificar()&", EIGlobal.NivelLog.INFO);

		String CveBeneficiario=(String) req.getParameter("cboCveBeneficiario");
		String Beneficiario=(String) req.getParameter("txtBeneficiario");
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		String loc_trama = "";
		String glb_trama = "";
		String trama_entrada="";
		String trama_salida="";
		String cabecera = "1EWEB";
		String operacion = "MBEN";
		String nombre_benef="";
		String resultado="";
		String mensaje = "";

		int primero=0;
		int ultimo=0;
		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &clave de beneficiario:"+CveBeneficiario+"&", EIGlobal.NivelLog.INFO);
		//TuxedoGlobal tuxedoGlobal;
				ServicioTux tuxedoGlobal = new ServicioTux();
				//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		fecha_hoy=ObtenFecha();

		if ( CveBeneficiario.trim().equals("") ){
		    mensaje = "Debe seleccionar una cuenta.";
		}else{
		      loc_trama = formatea_contrato(contrato) + "@" + CveBeneficiario.trim() + "@" + Beneficiario.trim() + "@" ;
		      trama_entrada = cabecera + "|" + clave_perfil + "|" + operacion + "|" + contrato + "|" + clave_perfil + "|" ;
		      trama_entrada = trama_entrada + usuario + "|" + loc_trama ;
			  EIGlobal.mensajePorTrace( "***ChesBenefModif.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
			  EIGlobal.mensajePorTrace( "***ChesBenefModif.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
			  try{
			  	//tuxedoGlobal = (TuxedoGlobal) session.getEjbTuxedo();
			  	Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
		      	trama_salida = (String) hs.get("BUFFER");
			  	EIGlobal.mensajePorTrace( "***ChesBenefModif.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);
			  }catch( java.rmi.RemoteException re ){
			  	re.printStackTrace();
			  } catch (Exception e) {}
			  if(trama_salida.startsWith("OK")){
		          primero = trama_salida.indexOf( (int) '@');
		          ultimo  = trama_salida.lastIndexOf( (int) '@');
		          resultado = trama_salida.substring(primero+1, ultimo);
		          mensaje = resultado;
		      }else{
		          mensaje = trama_salida;
		      }
        }

		String cadenaBeneficiarios = listarBeneficiarios(contrato,usuario, clave_perfil, suc_opera);

		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);

		req.setAttribute("FechaHoy", ObtenFecha(false));
		req.setAttribute("ContUser",ObtenContUser(req));
		//req.setAttribute("MenuPrincipal", session.getstrMenu());
		req.setAttribute("Fecha", ObtenFecha(true));
		req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
		req.setAttribute("mensaje_salida", mensaje);

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Modificaci&oacute;n de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n","s25970h",req) );

		despliegaPaginaError(mensaje,"Modificaci&oacute;n de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Modificaci&oacute;n", req, res);
		//salida=evalTemplate("/jsp/ChesBenefModif.jsp", req, res );

		return 1;
	}
	/*------------------------------------------------------------------------*/
	String formatea_contrato (String ctr_tmp){
		int i=0;
		String temporal="";
		String caracter="";

		for (i = 0 ; i < ctr_tmp.length() ; i++){
		    caracter = ctr_tmp.substring(i, i+1);
		    if (caracter.equals("-")){
		        // se elimina el gui�n
		    }else{
		    	temporal += caracter ;
		    }
		}
		return temporal;
	}

	/*--------------------------------------------------------*/

	public String listarBeneficiarios(String contrato,String usuario,  String clave_perfil, short sucOpera){

		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &inicia m�todo ListarBeneficiarios()&", EIGlobal.NivelLog.INFO);
		int    indice;
		String encabezado          = "";
		String tramaEntrada        = "";
		String tramaSalida         = "";
		String path_archivo        = "";
		String nombre_archivo      = "";
		String trama_salidaRedSrvr = "";
		String retCodeRedSrvr      = "";
		String registro            = "";
		String listaBeneficiarios  = "";
		String listaNombreBeneficiario = "";
		//TuxedoGlobal tuxGlobal;
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		String medio_entrega  = "2EWEB";
		String tipo_operacion = "CBEN";
		encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|";
		encabezado += usuario + "|" + clave_perfil + "|";
		tramaEntrada = contrato + "@";
		tramaEntrada = encabezado + tramaEntrada;
		strDebug += "\ntecb :" + tramaEntrada;

		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

		try{
			//tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();
			Hashtable hs = tuxGlobal.web_red(tramaEntrada);
			tramaSalida = (String) hs.get("BUFFER");
		}catch( java.rmi.RemoteException re ){
			re.printStackTrace();
		} catch(Exception e) {}
		strDebug += "\ntscb:" + tramaSalida;
		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
		path_archivo = tramaSalida;
		nombre_archivo = EIGlobal.BuscarToken(path_archivo, '/', 5);
		path_archivo = mx.altec.enlace.utilerias.Global.DOWNLOAD_PATH + nombre_archivo;

		strDebug += "\npath_cb:" + path_archivo;
		log("\npath_cb:" + path_archivo);

		boolean errorFile=false;
		try
		 {
			// Copia Remota
			EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
			ArchivoRemoto archivo_remoto = new ArchivoRemoto();

			if(!archivo_remoto.copia( nombre_archivo ) )
			{
				// error al copiar
				EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &error en remote shell ...&", EIGlobal.NivelLog.INFO);
			}
			else
			{
				// OK
				EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			}

		    File drvSua = new File(path_archivo);
		    RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
		    trama_salidaRedSrvr = fileSua.readLine();
		    retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
		    if (retCodeRedSrvr.equals("OK"))
		     {
				EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);
		       while((registro = fileSua.readLine()) != null){
			       	/////
					if( registro.trim().equals(""))
						registro = fileSua.readLine();
					if( registro == null )
						break;
		          listaBeneficiarios += "<OPTION VALUE=\"" + EIGlobal.BuscarToken(registro, ';', 1) + "\"> " + EIGlobal.BuscarToken(registro, ';', 1) +"   "+ EIGlobal.BuscarToken(registro, ';', 2) +"</OPTION>\n";
		        }
		     }
		    fileSua.close();
		 }catch(Exception ioeSua )
		    {
			   EIGlobal.mensajePorTrace( "***ChesBenefModif.class  Excepci�n en listarBeneficiarios() >>"+ioeSua.getMessage()+"<<", EIGlobal.NivelLog.ERROR);
		       return "";
		    }
		EIGlobal.mensajePorTrace( "***ChesBenefModif.class  &termina m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
		return(listaBeneficiarios);
	}

} // fin clase