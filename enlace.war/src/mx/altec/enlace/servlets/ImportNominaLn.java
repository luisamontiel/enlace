//10611 Antonio Perez



/**Banco Santander Mexicano

* Clase	ImportNominaLn, Modificacion al archivo de pago de nomina

* @autor Rodrigo Godo

* @version 1.1

* fecha	de creacion: Diciembre 2000	- Enero	20001

* responsable: Roberto Guadalupe Resendiz Altamirano

* Esta clase evalua	si el archivo es nuevo o importado,	en el primer caso, se permitira	la edici�n total del archivo

* (altas, bajas	y cambios en los registros), asi como su eliminaci�n; cuando el	archivo	sea	importado, se validar�n	todos los campos

* de todos los registros de	acuerdo	a las validaciones efectuadas en  enlace internet 2.1 (version de visual basic),

* a	continuaci�n se	determinar�	la cantidad	de registros de	detalle	existentes en el archivo, si el	resultado de esta cuenta

* es de	30 o menor,	se exhibir�n los registros en una tabla	pero no	se podr�n realizar modificaciones en ellos,	en caso	contrario,

* es decir,	cuando sean	m�s	de 30 los registros	existentes,	no se presentar�n.

* La cantidad de registros puede cambiar de	acuerdo	a los caprichos	del	usuario, por lo	cual el	n�mero de registros

* que se presentar�n "ser�n	parametizables"	como dice el Roberto

* Cuando se	permita	la edici�n de registros, se	llamar�	a la plantilla NominaDatos.html

*/
package	mx.altec.enlace.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.FormatoMoneda;
import mx.altec.enlace.bo.ValidaArchivoPagos;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.IEnlace;

/**
 *
 * @author z708764
 *
 */
public class ImportNominaLn extends BaseServlet {

	/** CONSTANTE SERLVET NOMINA EN LINEA**/
	public static final String PAG_TMPL_SRV_LIN = "InicioNominaLn";
	/**
	 * SESSIONX
	 */
	static final String SESSIONX="session";
	/**
	 * ENCABEZADOX
	 */
	static final String ENCABEZADOX="Encabezado";
	/**
	 * PAGODENOMINAX
	 */
	static final String PAGODENOMINAX="Pago de N&oacute;mina en L&iacute;nea";
	/**
	 * SERVSPAGNOMINALN
	 */
	static final String SERVSPAGNOMINALN="Servicios &gt; N&oacute;mina &gt; Pagos en L&iacute;nea";
	/**
	 * MSGERROROPERACION
	 */
	static final String MSGERROROPERACION="ImportNominaLn::defaultAction:: Hubo Problemas en operacion->";
	/**
	 * MSGERROROPERACION
	 */
	static final String MSGERROR="MsgError";
	/**
	 * CONTENIDOARCHIVO contenido del archivo
	 */
	static final String CONTENIDOARCHIVO="ContenidoArchivo";
	/**
	 * IMPORTANDOARCHIVO importando archivo
	 */
	static final String IMPORTANDOARCHIVO="Importando Archivo";
	/**
	 * CENTER align centro
	 */
	static final String CENTER="\" nowrap align=\"center\">";
	/**
	 * TITULO_MSG_ERROR titulo de encabezado para mensaje de error
	 */
	static final String TITULO_MSG_ERROR="<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea ";
	/**
	 * TD renglon html
	 */
	static final String TD="&nbsp;</td>";
	/**
	 * TDCLASS td class
	 */
	static final String TDCLASS="<td class=\"";

	/**
	 * S25800H
	 */
	static final String S25800H="s25800h";
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5786710384744525375L;
	
	/**
	 * String para tipo de cargo
	 */
	private static final String TIPO_CARGO_IG = "TipoCargoIG";
	
	/**
	 * String para tipo de cargo Individual / Global para nomina en linea
	 */
	private static final String TIPO_CARGO_IG_NOM_LIN = "TipoCargoIGNomLin";
	
	/**
	 * doGet
	 * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}
	/**
	 * doPost
	 * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}
	/**
	 *  defaultAction opcion default para cuando recibe por doGet o doPost
     * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo execute()&", EIGlobal.NivelLog.INFO);
		final boolean sesionvalida = SesionValida( request, response );
		final HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
		if ( sesionvalida ) {
			request.setAttribute( "newMenu", session.getFuncionesDeMenu());
			request.setAttribute( "MenuPrincipal", session.getStrMenu());
			request.setAttribute( ENCABEZADOX, CreaEncabezado(PAGODENOMINAX,
					SERVSPAGNOMINALN,S25800H,request));
			if ((String)sess.getAttribute("facultadesN") != null) {
				request.setAttribute( "facultades", (String)sess.getAttribute("facultadesN") );
			}
			//se obtiene valor de la fecha de hoy para ser usado como valor del campo fecha de aplicacion del registro de
			//encabezado para archivos nuevos
			GregorianCalendar fechaHoy= new GregorianCalendar();
			String laFechaHoy = EIGlobal.formatoFecha(fechaHoy,"aaaammdd");
			request.setAttribute("fechaHoy",laFechaHoy);
			request.setAttribute("Fechas",		  ObtenFecha(true));
			request.setAttribute("FechaHoy",	 ObtenFecha(false) + " - 06" );
			request.setAttribute(CONTENIDOARCHIVO,"");
			//se determina la hora del servidor
			String systemHour=ObtenHora();

			// M022532 NOMINA EN LINEA - INI
	 		String tipoCargoIG ="";
			String tipoCargo = "1";
			tipoCargoIG = (String) getFormParameter(request, TIPO_CARGO_IG);
	 		Object tipoCargoObj =request.getSession().getAttribute(TIPO_CARGO_IG_NOM_LIN);
	 		if(tipoCargoIG!=null)
	 		{
	 			tipoCargo=(String)tipoCargoIG;
			}
			else
			{
				tipoCargo=(String)tipoCargoObj; 
			}
			EIGlobal.mensajePorTrace( "ImportNominaLn ::: INICIO ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
			// M022532 NOMINA EN LINEA - FIN
			request.setAttribute("horaSistema", systemHour);
			//el arreglo de bytes contenidoArchivo almacenara la informacion que el archivo contenga
			//esto es asi debido a que cuando se crea el archivo en el servidor WebServer, este se encuentra
			//vacio, asi que se debe escribir en el la informacion original
			/*******   	 GET - HELG - INICIO - 21 FEB 2005      *******/
           		if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null) {
           			request.getSession().removeAttribute("Recarga_Ses_Multiples");
           			EIGlobal.mensajePorTrace( "Import Nomina - Removiendo atributo Recarga_Ses_Multiples.", EIGlobal.NivelLog.INFO);
           		}
           		if(request.getSession().getAttribute("Ejec_Proc_Val_Pago")!=null) {
           			request.getSession().removeAttribute("Ejec_Proc_Val_Pago");
           			EIGlobal.mensajePorTrace("Import Nomina - Removiendo atributo Ejec_Proc_Val_Pago.", EIGlobal.NivelLog.INFO);
           		}
			/*******   	 GET - HELG - FIN - 21 FEB 2005      *******/
           		String operacion   = getFormParameter(request,"operacion");
           		EIGlobal.mensajePorTrace("***ImportNominaLn.class operacion&  "+operacion, EIGlobal.NivelLog.INFO);
           		if ( (null == operacion) || ( ("").equals(operacion))) {
           			operacion = "cargaArchivo";
           		}
			//instancia de ArchivoRemoto que servira para hacer la copia al servidor de web
			try{
				if ( ("cargaArchivo").equals(operacion) ) {
					// M022532 NOMINA EN LINEA - INI
			 		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
			 		EIGlobal.mensajePorTrace( "ImportNominaLn ::: cargaArchivo ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
					// M022532 NOMINA EN LINEA - FIN
					ejecutaCargaArchivo ( request, response);
				}
			}catch (IOException e) {
			    EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(MSGERROROPERACION+operacion, EIGlobal.NivelLog.INFO);
			request.setAttribute(MSGERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}
			catch (ServletException e) {
			    EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(MSGERROROPERACION+operacion, EIGlobal.NivelLog.INFO);
			request.setAttribute(MSGERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}
			try{
				if ( ( "validaArchivoImport" ).equals(operacion) ) {
					// M022532 NOMINA EN LINEA - INI
					request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
					EIGlobal.mensajePorTrace( "ImportNominaLn ::: validaArchivoImport ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
					// M022532 NOMINA EN LINEA - FIN
					ejecutaValidaArchivo( request, response);
				}
			}catch (IOException e) {
			    EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(MSGERROROPERACION+operacion, EIGlobal.NivelLog.INFO);
			request.setAttribute(MSGERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}
			catch (ServletException e) {
			    EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(MSGERROROPERACION+operacion, EIGlobal.NivelLog.INFO);
			request.setAttribute(MSGERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}
		} else {
		   request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
		   evalTemplate(IEnlace.ERROR_TMPL, request, response );
		}

	}// default




/**
 * ejecutaCargaArchivo ejecuta la carga del archivo
 * @param request  Recibe parametros
 * @param response Respuesta parametros
 * @throws IOException Excepcion en la entrada salida de archivo
 * @throws ServletException expcecion en el Servlet
 */
	public void ejecutaCargaArchivo( HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo ejecutaCargaArchivo: &", EIGlobal.NivelLog.INFO);
		final HttpSession sess = request.getSession();
	    final BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
		String nombreOriginal		= "";
		String originalName         = "";
		String nomArchNomina		= "";
		long tamanioArchivo	 	    = 0;
		String fechaAplicacionArchivo = "";
		String cuentaCargo			= "";
		boolean cuentaAsociada		= true;
		boolean errorEnZip=false;
		String sFile = "";
		BufferedOutputStream dest = null;
		FileInputStream fis = null;
		BufferedInputStream buis = null;
		ZipInputStream zis = null;
		FileOutputStream fos = null;
		String tipoCargo ="1";
 		String tipoCargoIG ="";

		try{
			//M022532 NOMINA EN LINEA - INI
			tipoCargoIG = (String) getFormParameter(request, TIPO_CARGO_IG);
	 		Object tipoCargoObj =request.getSession().getAttribute(TIPO_CARGO_IG_NOM_LIN);
	 		if(tipoCargoIG!=null)
	 		{
	 			tipoCargo=(String)tipoCargoIG;
			}
			else
			{
				tipoCargo=(String)tipoCargoObj; 
			}
	 		EIGlobal.mensajePorTrace( "ejecutaCargaArchivo  - TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO ); 
	 		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
	 		//M022532 NOMINA EN LINEA - FIN
			//se obtiene la ruta original del archivo
			nombreOriginal  = getFormParameter(request, "fileName");
            nombreOriginal = nombreOriginal.trim();
		    EIGlobal.mensajePorTrace("***ImportNominaLn.class & nombreOriginal>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);
		    EIGlobal.mensajePorTrace("***ImportNominaLn.class & nombreOriginal Tamno del archivo antes: "+request.getContentLength(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ImportNominaLn.class & Parseando objeto multipart ", EIGlobal.NivelLog.INFO);
			sFile = getFormParameter(request, "fileName");
			EIGlobal.mensajePorTrace("***ImportNominaLn.class & Fin Parser - Archivo:  "+ sFile, EIGlobal.NivelLog.INFO);

			// Si el archivo es .zip se descomprime
		    if (sFile.toLowerCase().endsWith(".zip")) {
			    EIGlobal.mensajePorTrace("***ImportNominaLn.class: El archivo viene en zip", EIGlobal.NivelLog.INFO);
           		final int BUFFER = 2048;
		        fis = new FileInputStream(IEnlace.LOCAL_TMP_DIR+"/" + sFile);
		        buis = new BufferedInputStream(fis);						//MSD 20071214
		        zis = new ZipInputStream(buis);									//MSD 20071214
		        ZipEntry entry;
		        entry = zis.getNextEntry();
		        EIGlobal.mensajePorTrace("***ChesRegistro.class: entry: "+entry, EIGlobal.NivelLog.INFO);
		        if(entry==null) {
				   EIGlobal.mensajePorTrace("***ChesRegistro.class: Entry fue null, archivo no es zip o esta da�ando ", EIGlobal.NivelLog.INFO);
				   errorEnZip=true;
				 }
		        else {
					log( "ImportNominaLn.java::Extrayendo"  + entry);
					int count;
					final byte data[] = new byte[BUFFER];
					// Se escriben los archivos a disco
					// Nombre del archivo zip
					File archivocomp = new File( IEnlace.LOCAL_TMP_DIR +"/" + sFile );
					// Nombre del archivo que incluye el zip
					sFile = entry.getName();
					fos = new FileOutputStream(IEnlace.LOCAL_TMP_DIR + "/"+sFile);
					dest = new BufferedOutputStream(fos, BUFFER);
					while ((count = zis.read(data, 0, BUFFER)) != -1) {
						dest.write(data, 0, count);
					}
					dest.flush();
					/*dest.close();
					fos.close();						//MSD 20071214*/
					// Borra el archivo comprimido
					if (archivocomp.exists()) {
					  archivocomp.delete();
				  	}
					  archivocomp = null;
					  log( "ImportNominaLn.java::Fin extraccion de: "  + entry);
				 }
			   /*zis.close();
			   buis.close();							//MSD 20071214
			   fis.close();								//MSD 20071214*/
   		     }
//		} catch(Exception e) {
//			log( "ImportNominaLn.java::defaultAction()::try:Recibe Archivo" + e.toString() );
		} finally {

			try {
				if (dest != null){
					dest.close();							//MSD 20071214
				}
			}catch(IOException e) {
				log( "ImportNominaLn.java::defaultAction()::finally:Recibe Archivo." + e.toString() );
			}
			try {
				if (fos != null){
					fos.close();							//MSD 20071214
				}
			}catch(IOException e) {
				log( "ImportNominaLn.java::defaultAction().:.finally:Recibe Archivo..." + e.toString() );
			}
			try {
				if (zis != null){
					zis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				log( "ImportNominaLn.java::defaultAction():finally:Recibe Archivo..." + e.toString() );
			}
			try {
				if (buis != null){
					buis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				log( "ImportNominaLn.java::defaultAction().::.finally:Recibe Archivo...." + e.toString() );
			}
			try {
				if (fis != null){
					fis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				log( "ImportNominaLn.java::defaultAction():::finally:Recibe Archivo....." + e.toString() );
			}
		}

		EIGlobal.mensajePorTrace("***ImportNominaLn.class After SchumiCode 01", EIGlobal.NivelLog.INFO);

		//de la ruta del archivo, se obtiene solo el nombre

		nombreOriginal = nombreOriginal.substring( nombreOriginal.lastIndexOf( '\\' ) + 1 );
		EIGlobal.mensajePorTrace("***ImportNominaLn.class After SchumiCode 02", EIGlobal.NivelLog.INFO);
		originalName   = nombreOriginal;

		EIGlobal.mensajePorTrace("***ImportNominaLn.class & nombreOriginalantes>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);

		//cuando el archivo sea importado, se eliminara su extension

		nombreOriginal= nombreOriginal.substring(0,nombreOriginal.length()-4);



		// Para reconocer los archivos pertenecientes a este modulo se

		// les asignara la extension .nom

		//nombreOriginal = nombreOriginal + ".nom";

		//32 caracteres.



		Calendar cal = Calendar.getInstance();

		nombreOriginal = session.getContractNumber() + cal.getTime().getTime();
		//nombreOriginal = sessionId;

		cal = null;

	    EIGlobal.mensajePorTrace("***ImportNominaLn.class .:. nombreOriginal_ultimo .:. nomina en linea>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);



		//String nueva_ruta= "/t/tuxedo/procesos/enlace/internet/archivos_internet/";

		//nomArchNomina  = "" + nueva_ruta + nombreOriginal;



		//nomArchNomina  = "" + IEnlace.DOWNLOAD_PATH + nombreOriginal;
	    final StringBuffer br=new StringBuffer(nomArchNomina);
	    br.append(IEnlace.DOWNLOAD_PATH);
	    br.append(nombreOriginal);
	    nomArchNomina=br.toString();

		EIGlobal.mensajePorTrace("***ImportNominaLn.class .:. nombre asigando .:. nomina en linea>"+nomArchNomina ,EIGlobal.NivelLog.INFO);




		//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

		RandomAccessFile archivoNomina = null;
		//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin




		//Si ya existe el archivo lo borra

		try{

		        EIGlobal.mensajePorTrace("***ImportNominaLn.class & Si el archivo existe, se renombra.", EIGlobal.NivelLog.INFO);

			// Renombra el archivo descargado

			File archivo = new File( IEnlace.LOCAL_TMP_DIR +"/"+ sFile );

			if (archivo.exists()) {
			   EIGlobal.mensajePorTrace("***ImportNominaLn.class & Se renombra a: "+nomArchNomina, EIGlobal.NivelLog.INFO);
			   archivo.renameTo(new File(nomArchNomina));
			} else {
			   EIGlobal.mensajePorTrace("***ImportNominaLn.class & El archivo no existe.", EIGlobal.NivelLog.INFO);
			   // throw file not found exception
			}
		} //catch(Exception e) {
		catch(SecurityException e) {
			EIGlobal.mensajePorTrace("***ImportNominaLn.class Ha ocurrido una excepcion en: %ejecutaCargaArchivo()"+e, EIGlobal.NivelLog.INFO);
		}

		ValidaCuentaContrato valCtas = null;
		try{





			EIGlobal.mensajePorTrace("***ImportNominaLn.class & Se abre nuevo archivo: "+nomArchNomina, EIGlobal.NivelLog.INFO);

			File pathArchivoNom	= new File( nomArchNomina );
			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

			//RandomAccessFile archivoNomina = new RandomAccessFile( pathArchivoNom, "r" );
			archivoNomina = new RandomAccessFile( pathArchivoNom, "r" );
			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin
			tamanioArchivo = archivoNomina.length();
			sess.setAttribute("nombreArchivo",nomArchNomina);
			request.setAttribute( "tipoArchivo","importado" );
			archivoNomina.seek(0);
			String primeralinea	= archivoNomina.readLine();

			//ARCHIVO VACIO
			if(primeralinea==null){
				despliegaPaginaError( " Imposible Importar el archivo, no contiene registros",
						PAGODENOMINAX + " en L&iacute;nea", "Importando Archivo",S25800H, request, response );
				if(archivoNomina!=null){
					EIGlobal.mensajePorTrace("***ImportNominaLn.class:: Cerrando archivoNomina", EIGlobal.NivelLog.INFO);
					archivoNomina.close();
				}
				return;
			}

			EIGlobal.mensajePorTrace("***ImportNominaLn.class &primeralinea &"+primeralinea.length(), EIGlobal.NivelLog.INFO);
			String ctaAux="";
			if(errorEnZip) {
			  despliegaPaginaError( " Imposible Importar el archivo, no es un archivo zip o esta da�ado",
					  PAGODENOMINAX, IMPORTANDOARCHIVO,S25800H, request, response );
			   EIGlobal.mensajePorTrace("***ImportNominaLn.class &primeralinea & Imposible Importar el archivo, no es un archivo zip o esta da�ado." , EIGlobal.NivelLog.INFO);
			   	//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

				archivoNomina.close();
				//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
			}
			else {
				if (primeralinea.length() !=  39) {
					despliegaPaginaError( " Imposible Importar el archivo seleccionado, la longitud del encabezado es incorrecto.",
							PAGODENOMINAX, IMPORTANDOARCHIVO,S25800H, request, response );
			//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

					archivoNomina.close();
			//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
				} else {
					//cuenta de cargo del archivo
					if(primeralinea.length()>31) {
					   cuentaCargo = primeralinea.substring( 15, 31 );
					   ctaAux=cuentaCargo.substring(0,11);
					   EIGlobal.mensajePorTrace("***ImportNominaLn.class & Cta Aux = "+ctaAux, EIGlobal.NivelLog.INFO);
					}
					cuentaCargo = cuentaCargo.trim();
					EIGlobal.mensajePorTrace("***ImportNominaLn.class &cuentaCargo &"+cuentaCargo, EIGlobal.NivelLog.INFO);
					session.setCuentaCargo( cuentaCargo );
					fechaAplicacionArchivo = new String(primeralinea.substring(primeralinea.length() - 8,primeralinea.length()));
					String fechaAplicacion=fechaAplicacionArchivo.trim().length()!= 0 ? fechaAplicacionArchivo.substring(0, 2)+"-"+fechaAplicacionArchivo.substring(2, 4)+"-"+fechaAplicacionArchivo.substring(4, 8): "";
					EIGlobal.mensajePorTrace("***ImportNominaLn.class &fechaAplicacion &"+fechaAplicacion, EIGlobal.NivelLog.INFO);
					if(!comparaFechas(fechaAplicacion)) {
						despliegaPaginaError( " La fecha de aplicaci&oacute;n del archivo debe ser igual a la fecha actual",
								PAGODENOMINAX, IMPORTANDOARCHIVO,S25800H, request, response );
						EIGlobal.mensajePorTrace("***ImportNominaLn.class & Error en la fechaAplicacionArchivo &"+fechaAplicacionArchivo, EIGlobal.NivelLog.INFO);
						return;
					}

					//***********************************************
					//modificaci�n para integraci�n pva 07/03/2002
			        //***********************************************
				    //llamado_servicioCtasInteg(IEnlace.MEnvio_pagos_nomina," "," "," ",session.getUserID()+".ambci",request);
					valCtas = new ValidaCuentaContrato();
					String datoscta[]	= null;
				    //datoscta			= BuscandoCtaInteg(cuentaCargo,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambci",IEnlace.MEnvio_pagos_nomina);
					datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
					  		  								  session.getUserID8(),
					  		  								  session.getUserProfile(),
					  		  								  cuentaCargo.trim(),
					  		  								  IEnlace.MEnvio_pagos_nomina);
					if(datoscta == null){
				       cuentaAsociada	= false;
					} else {
						cuentaAsociada = true;
						sess.setAttribute("descpCuentaCargo",datoscta[4]);
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &datoscta[4] &"+datoscta[4], EIGlobal.NivelLog.INFO);
					}
					session.setCuentasArchivo("");
					sess.setAttribute("numeroEmp","");
					if (archivoNomina!=null){
						archivoNomina.close();
					}
					if ( !cuentaAsociada ) { // aqui cuando la cuenta y el archivo estan mal
						despliegaPaginaError( " Imposible Importar el archivo seleccionado, la cuenta  (" + cuentaCargo + ")  no existe en el contrato o no est&aacute; disponible para el servicio de n&oacute;mina.",
								PAGODENOMINAX, IMPORTANDOARCHIVO,S25800H, request, response );
						session.setCuentaCargo(" ");
					}else {
						if(ctaAux.trim().length()!=11) {
						   EIGlobal.mensajePorTrace("***ImportNominaLn.class & La cuenta aux empieza con espacios, mal justificada.", EIGlobal.NivelLog.INFO);
						   despliegaPaginaError( " Imposible Importar el archivo seleccionado, la cuenta de cargo no est&aacute; justificada correctamente.",PAGODENOMINAX, IMPORTANDOARCHIVO,S25800H, request, response );
						   session.setCuentaCargo(" ");
						} else {
						   request.setAttribute("tamanioArchivo",String.valueOf(tamanioArchivo));
						   request.setAttribute("nomArchNomina",nomArchNomina);
						   request.setAttribute("originalName",originalName);
						   //M022532 NOMINA EN LINEA - INI
					 	   request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
					 	   //M022532 NOMINA EN LINEA - FIN					 	   
						   evalTemplate(IEnlace.REPORTE_NOMINALN_TMPL, request, response );
						}
					}
				} //else != 39
			}

		} catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***ImportNominaLn.class Ha ocurrido una excepcion en: %ejecutaCargaArchivo()"+e, EIGlobal.NivelLog.INFO);
		}
		//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
		finally{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
			}
            if(null != archivoNomina){
                try{
                	archivoNomina.close();
                }catch(IOException e){
                	EIGlobal.mensajePorTrace("Error al cerrar archivo", EIGlobal.NivelLog.ERROR);
			    }
                archivoNomina = null;
            }
        }
		//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
	}//fin del metodo ejecutaCargaArchivo
	/**
	 * posCar
	 * @param cad tarjeta
	 * @param car prefijo
	 * @param cant cantidad
	 * @return result resultado
	 */
	public int posCar(String cad,char car,int cant) {

				int result=0,pos=0;

				for (int i=0;i<cant;i++) {

					result=cad.indexOf(car,pos);

					pos=result+1;

				}

				return result;

			}
			//Finaliza codigo SINAPSIS
			/**
			 * ejecutaValidaArchivo ejecuta la validacion del archivo
			 * @param request  Recibe parametros
			 * @param response Respuesta parametros
			 * @throws IOException Excepcion en la entrada salida de archivo
			 * @throws ServletException expcecion en el Servlet
			 */
			public void ejecutaValidaArchivo( HttpServletRequest request, HttpServletResponse response )
				throws IOException, ServletException {
				EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo ejecutaValidaArchivo: &", EIGlobal.NivelLog.INFO);
				//M022532 NOMINA EN LINEA - INI
				String tipoCargoIG ="";
				String tipoCargo = "1";
				tipoCargoIG = (String) getFormParameter(request, TIPO_CARGO_IG);
		 		Object tipoCargoObj =request.getSession().getAttribute(TIPO_CARGO_IG_NOM_LIN);
				
		 		if(tipoCargoIG!=null)
		 		{
		 			tipoCargo=(String)tipoCargoIG;
				}
				else
				{
					tipoCargo=(String)tipoCargoObj; 
				}
		 		EIGlobal.mensajePorTrace( "EJECUTA VALIDA ARCHIVO  - TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
		 		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
				//M022532 NOMINA EN LINEA - FIN		 		
				HttpSession sess = request.getSession();
				BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
			    //***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

				//ValidaArchivoPagos validaArchivo;
				ValidaArchivoPagos validaArchivo = null;
				//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin
				String nomArchNomina		= getFormParameter(request,"nomArchNomina");
				nomArchNomina = nomArchNomina.trim();
				//validaArchivo				= new ValidaArchivoPagos( this, nomArchNomina );
				Vector erroresEnElArchivo	= new Vector();
				erroresEnElArchivo.addElement("<table>");
				boolean archivoValido		= true;
				Vector cantidadRegistros    = new Vector();
				//String[][] arrayDetails;
				StringBuffer contenidoArchivoStr	= new StringBuffer("");
				String originalName		= getFormParameter(request, "fileName");//request.getParameter("originalName");
				EIGlobal.mensajePorTrace("***ImportNominaLn.class &originalName: prueba 3 &"+originalName, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ImportNominaLn.class &nomArchNomina: &"+nomArchNomina, EIGlobal.NivelLog.INFO);
				int registros		  = 0;

				//registros= registrosEnArchivo( nomArchNomina ,request, response);

				EIGlobal.mensajePorTrace("***ImportNominaLn.class &registros&"+registros, EIGlobal.NivelLog.INFO);

				int cuentaDeRegistros = registros;

				String numerosEmpleados[] = null;

				int registrosLeidos	   = 0;

				registrosLeidos		   = cuentaDeRegistros;

				//FIN DETERMINA NUMERO DE REGISTROS

				BufferedReader arch = null;

				FileReader reader = null;


				reader  = new FileReader( nomArchNomina );
				arch	= new BufferedReader( reader );

				String registroEnc	= arch.readLine();

				String encTipoReg	= "";

				String encNumSec	= "";

				String encSentido	= "";

				String encFechGen	= "";

				String encCuenta	= "";

				String encFechApli	= "";

				validaArchivo				= new ValidaArchivoPagos( this, nomArchNomina );

				try

					{
					encTipoReg	= new String( registroEnc.substring( 0, 1 ) );

				    if ( !validaArchivo.validaDigito( encTipoReg ) || !validaArchivo.validaEncTipoReg( encTipoReg ) ) {
						erroresEnElArchivo.addElement( "<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\">Error en el primer car&aacute;cter del encabezado, se esperaba 1</td>");
						archivoValido	= false;
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el Tipo registro encabezado&", EIGlobal.NivelLog.INFO);
					   }

					encNumSec = new String( registroEnc.substring( 1, 6 ) );

					EIGlobal.mensajePorTrace("***ImportNominaLn.class en encNumSec a: ("+encNumSec+")", EIGlobal.NivelLog.INFO);

					encNumSec=encNumSec.trim();

					EIGlobal.mensajePorTrace("***ImportNominaLn.class en encNumSec b: ("+encNumSec+")", EIGlobal.NivelLog.INFO);
					if ( !validaArchivo.validaDigito( encNumSec ) || !validaArchivo.validaPrimerNumSec( encNumSec ) ) {
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del encabezado, se esperaba 1</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en n&uacute;mero secuencial del encabezado&", EIGlobal.NivelLog.INFO);
					  }
					encSentido = new String(registroEnc.substring(6,7));
					EIGlobal.mensajePorTrace("***ImportNominaLn.class  encSentido:"+encSentido, EIGlobal.NivelLog.INFO);
					 if ( !validaArchivo.validaLetras(encSentido) || !validaArchivo.validaSentido(encSentido) ) {
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el car&aacute;cter de sentido del encabezado, se esperaba E</td>");
						archivoValido	= false;
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el sentido del encabezado, se esperaba 'E'&", EIGlobal.NivelLog.INFO);
					  }
					 encFechGen = new String(registroEnc.substring(7,15));
	  				 EIGlobal.mensajePorTrace("***ImportNominaLn.class &encFechGen:"+encFechGen, EIGlobal.NivelLog.INFO);
					 if ( !validaArchivo.validaDigito(encFechGen) || !validaArchivo.fechaValida(encFechGen) ){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de generaci&oacute;n del archivo</td>");
						archivoValido	= false;
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en la fecha de generacion del archivo&", EIGlobal.NivelLog.INFO);
					  }
					 EIGlobal.mensajePorTrace("##################### ESTATUS**** "+archivoValido, EIGlobal.NivelLog.INFO);
					 //String fechaGeneracion=encFechGen.trim().length()!= 0 ? encFechGen.substring(0, 2)+"-"+encFechGen.substring(2, 4)+"-"+encFechGen.substring(4, 8) : "";
					   // La cuenta ya no se verifica pues al momento de formar el arreglo de
					   // cuentas, se ha verificado que exista y que sea propia y/o de terceros
					  encCuenta = new String( registroEnc.substring( 15,31 ) );
	  				  EIGlobal.mensajePorTrace("***ImportNominaLn.class &encCuenta:"+ encCuenta, EIGlobal.NivelLog.INFO);
					  encFechApli = new String(registroEnc.substring(registroEnc.length() - 8,registroEnc.length()));
	  				  EIGlobal.mensajePorTrace("***ImportNominaLn.class &encFechApli:", EIGlobal.NivelLog.INFO);
					   if ( !validaArchivo.validaDigito(encFechApli) || !validaArchivo.fechaValida(encFechApli) ){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de aplicaci&oacute;n del archivo</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en la fecha de aplicacion&", EIGlobal.NivelLog.INFO);
					   }
					   EIGlobal.mensajePorTrace("##################### ESTATUS "+archivoValido, EIGlobal.NivelLog.INFO);
					   String fechaAplicacion=encFechApli.trim().length()!= 0 ? encFechApli.substring(0, 2)+"-"+encFechApli.substring(2, 4)+"-"+encFechApli.substring(4, 8): "";
					   if(!comparaFechas(fechaAplicacion)) {
						   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">La fecha de aplicaci&oacute;n del archivo debe ser igual a la fecha actual</td>");
						   //FIXME
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &La fecha de aplicacion debe ser la del dia actual&", EIGlobal.NivelLog.INFO);
						}
	            }
				catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");
					archivoValido=false;
				}
	 			//EIGlobal.total_lineas_leidas(1,registros);
				session.setFechaAplicacion( encFechApli );
				//String registroEncabezadoCompleto;
				//registroEncabezadoCompleto = encTipoReg + encNumSec+encSentido + encFechGen + encCuenta + encFechApli;
				/* Detalle del archivo */
				if ( encNumSec != null ) {
					cantidadRegistros.addElement( encNumSec );
				}
				int longitudArreglo = cuentaDeRegistros - 2;
				// La longitud del arreglo esta determinada por la cantidad de
				// registros de ddetalle existentes en el archivo de pago de nomina
				if ( longitudArreglo < 0 ) {
					longitudArreglo = 0;
				}

				//el metodo setNominaNumberOfRecords() devuelve el numero de registros de detalle en el archivo



				//arrayDetails = new String[longitudArreglo][9];
				numerosEmpleados = new String[longitudArreglo];
				for(int j=0; j<longitudArreglo; j++){
					numerosEmpleados[j]="";
				}

				double sumImporte       = 0;

						String registroDet		= "";  //jtg saco la declaracion de variables del for

						String tipoRegistro		= "";

						String NumeroSecuencia	= "";

						String NumeroEmpleado	= "";

						String ApellidoPaterno	= "";

						String ApellidoMaterno	= "";

						String Nombre			= "";

						String NumeroCuenta		= "";

						String Importe			= "";

						double num				= 0.0;

						int contadorLinea		= 1;

						/*************** Nomina Concepto de Archvo */
						String Concepto         = "";
						boolean registroContieneConcepto=false;
						int registrosConConcepto=0;
						Vector listaIDConceptos=getIDConceptos();
						/*************** Nomina Concepto de Archvo */
			//if ( arrayDetails.length > Global.REG_MAX_IMPORTAR_NOMINA ){
				//for (int i= 0; i < arrayDetails.length; i++) {
				int i = 0;
				while( ((registroDet = arch.readLine()) != null)){
				  try {
					  tipoRegistro	= new String ( registroDet.substring(0,1)		);
					  if(!"2".equals(tipoRegistro)){
						  break;
					  }
					try{
						//registroDet		= arch.readLine();
						EIGlobal.mensajePorTrace("registroDet.:. "+registroDet, EIGlobal.NivelLog.DEBUG);
						if(registroDet==null){ //jtg   si es nulo no puedo obtener los datos
							continue;
						}
						if(! (registroDet.length() == 127  || registroDet.length()==129) ) {
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el tama�o del Registro </td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el tipo de Registro del detalle&"+registroDet.length(), EIGlobal.NivelLog.INFO);
							continue;
						}// else{

						//}

						NumeroSecuencia	= new String ( registroDet.substring(1,6)		);
						//NumeroEmpleado	= new String ( registroDet.substring(6,13)		);
						ApellidoPaterno	= new String ( registroDet.substring(13,43)		);
						ApellidoMaterno	= new String ( registroDet.substring(43,63)		);
						Nombre			= new String ( registroDet.substring(63,93)		);
						NumeroCuenta	= new String ( registroDet.substring(93,109)	);
						Importe			= new String ( registroDet.substring(109,127)	);
						/*************** Nomina Concepto de Archvo */
						if(registroDet.length()>127) {
						   Concepto        = new String ( registroDet.substring(127)    );
						   registrosConConcepto++;
						   registroContieneConcepto=true;
						 }
						/*************** Nomina Concepto de Archvo */
						num = Double.parseDouble(Importe);
						sumImporte = sumImporte + num;
					} catch(NumberFormatException e) {
						EIGlobal.mensajePorTrace("Excepcion tipo NumberFormatException:"+e.toString(), EIGlobal.NivelLog.DEBUG);
					}catch(IndexOutOfBoundsException e) {
						EIGlobal.mensajePorTrace("Excepcion tipo IndexOutOfBoundsException:"+e.toString(), EIGlobal.NivelLog.DEBUG);
					}
						//arrayDetails[i][0] = tipoRegistro;
						if ( !validaArchivo.validaDigito( tipoRegistro ) || !validaArchivo.validaDetalleTipoReg( tipoRegistro ) ) {
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el tipo de Registro del detalle</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);
						}
						//arrayDetails[i][1]=NumeroSecuencia;
						if ( !validaArchivo.validaDigito(NumeroSecuencia) || !validaArchivo.validaNumSec(i, NumeroSecuencia) ){
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del detalle</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el n&uacute;mero secuencial del detalle&", EIGlobal.NivelLog.INFO);
						}
						//arrayDetails[i][2]=NumeroEmpleado;
						/*if("".equals(arrayDetails[i][2].trim()) || !validaCampo(arrayDetails[i][2].toUpperCase(), 1)){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el numero de empleado</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el numero de empleado&", EIGlobal.NivelLog.INFO);
						}*/

						//arrayDetails[i][3]=ApellidoPaterno;
						EIGlobal.mensajePorTrace("***+++++ ApellidoPaterno ["+ApellidoPaterno+"]", EIGlobal.NivelLog.INFO);
						if(!"".equals(ApellidoPaterno.trim()) && !validaCampo(ApellidoPaterno.toUpperCase(), 0) ){
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Apellido Paterno</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el apellido paterno&", EIGlobal.NivelLog.INFO);

						}
						EIGlobal.mensajePorTrace("***+++++ apellido paterno& ["+ApellidoPaterno+"]", EIGlobal.NivelLog.INFO);
						//arrayDetails[i][4]=ApellidoMaterno;
						if(!"".equals(ApellidoMaterno.trim()) && !validaCampo(ApellidoMaterno.toUpperCase(), 0) ){
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Apellido Materno</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el apellido materno&", EIGlobal.NivelLog.INFO);
						}
						EIGlobal.mensajePorTrace("***+++++ apellido materno& ["+ApellidoMaterno+"]", EIGlobal.NivelLog.INFO);
						//arrayDetails[i][5]=Nombre;
						if(!"".equals(Nombre.trim()) && !validaCampo(Nombre.toUpperCase(), 0) ){
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Nombre</td>");
						    archivoValido=false;
						    EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el nombre&", EIGlobal.NivelLog.INFO);
						}
						EIGlobal.mensajePorTrace("***+++++ nombre& ["+Nombre+"]", EIGlobal.NivelLog.INFO);

						//arrayDetails[i][6]=NumeroCuenta;
						/* modificacion para integracion

						if (validaArchivo.validaDigito(arrayDetails[i][6])==false||validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false

								||validaArchivo.validaCuenta(arrayDetails[i][6])==false||validaArchivo.verificaDuplicidadCuentas(cuentasAbono, arrayDetails[i][6])){

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

							archivoValido=false;

							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el nombre&", EIGlobal.NivelLog.INFO);

						}*/

					    //EIGlobal.mensajePorTrace("***Comienza modificacion para integracion: ", EIGlobal.NivelLog.INFO);

						/*if (validaArchivo.validaDigito(arrayDetails[i][6])==false||

							validaArchivo.validaCuenta(arrayDetails[i][6])==false||validaArchivo.verificaDuplicidadCuentas(cuentasAbono, arrayDetails[i][6]))

						{*/

						/*if (validaArchivo.validaDigito(arrayDetails[i][6])==false||	validaArchivo.validaCuenta(arrayDetails[i][6])==false)

						{

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

							archivoValido=false;

							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el Numero de Cuenta&", EIGlobal.NivelLog.INFO);

						}*/
						if ("".equals(NumeroCuenta.trim()) || !validaCampo(NumeroCuenta.trim(), 2)) {
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero de Cuenta</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el n&uacute;mero de Cuenta&", EIGlobal.NivelLog.INFO);
						}
						/*if (validaArchivo.validaCuentaDolares(arrayDetails[i][6])==false)

						{

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error el Numero de Cuenta es en dolares</td>");

							archivoValido=false;

							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error el Numero de Cuenta es de dolares&", EIGlobal.NivelLog.INFO);

						}*/

						/*if ( (session.getClaveBanco().equals(Global.CLAVE_SANTANDER))&&

							 (validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false))

	                    {

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

							archivoValido=false;

							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el Numero de Cuenta&", EIGlobal.NivelLog.INFO);

	                    }

	                    if  (session.getClaveBanco().equals(Global.CLAVE_SERFIN))

	                    {

	                        if (validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false)

	                        {

								if (validaDigitoCuentaHogan(arrayDetails[i][6])==false)

								{

	                               erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

						           archivoValido=false;

							       EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);

	                            }

	                        }

	                    }*/

				        //EIGlobal.mensajePorTrace("***termina modificacion para integracion: ", EIGlobal.NivelLog.INFO);



	                    //******fin de modificacion para integracion


						/*************** Nomina Concepto de Archvo */
						if ( !validaArchivo.validaDigito(Concepto) || (registroContieneConcepto && ("").equals(Concepto.trim()))) {
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Identificador no v&aacute;lido en el campo Concepto.</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el n&uacute;mero de Concepto&", EIGlobal.NivelLog.INFO);
						} else { /* Validacion en archivo  */
							if(listaIDConceptos!=null && Concepto.trim().length()>0 && listaIDConceptos.indexOf(Concepto)<0) {
								EIGlobal.mensajePorTrace("***ImportNominaLn.class & El Concepto no esta registrado.", EIGlobal.NivelLog.INFO);
								erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">La clave del Concepto no est&aacute; Registrada.</td>");
								archivoValido=false;
							}
					    /*************** Nomina Concepto de Archvo */
						//arrayDetails[i][7]=Importe;
						//if (validaArchivo.validaDigito(arrayDetails[i][7])==false){
						if ( !validaArchivo.validaDigito(Importe) || Importe.endsWith(" ")) {
						   erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe</td>");
						   archivoValido=false;
						   //EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el importe&", EIGlobal.NivelLog.INFO);
						 } else {
							String ctaAux1="";
							ctaAux1=NumeroCuenta.substring(0,11);
							//EIGlobal.mensajePorTrace("***ImportNominaLn.class &Cuenta auxiliar abono = "+ctaAux1, EIGlobal.NivelLog.DEBUG);
							if(ctaAux1.trim().length()!=11 && ctaAux1.trim().length()!=10) {
								erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la Cuenta, no est&aacute; justificada correctamente.</td>");
								archivoValido=false;
								EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en la cuenta no esta justificada correctamente&", EIGlobal.NivelLog.DEBUG);
							 }
						 }
						if (Importe.trim().length() == 0){
							erroresEnElArchivo.addElement(TITULO_MSG_ERROR+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Debe especificar importe</td>");
							archivoValido=false;
						}
						cantidadRegistros.addElement(NumeroSecuencia);
						//String registroDetalleCompleto;
						//registroDetalleCompleto=tipoRegistro+NumeroSecuencia+NumeroEmpleado+ApellidoPaterno+ApellidoMaterno+Nombre+NumeroCuenta+Importe;
						}

						// LIMITE DE 10000 REGISTROS EN EL ARCHIVO
						if((registrosLeidos+1)>10000){
							despliegaPaginaError( "Excede el n&uacute;mero m&aacute;ximo registros, se permiten solo 10,000<br>",
									PAGODENOMINAX + " en L&iacute;nea", "Importando Archivo",S25800H, request, response );
							if(reader!=null){
								EIGlobal.mensajePorTrace("***ImportNominaLn.class:: Cerrando reader", EIGlobal.NivelLog.INFO);
								reader.close();
							}

							if(arch!=null){
								EIGlobal.mensajePorTrace("***ImportNominaLn.class:: Cerrando arch", EIGlobal.NivelLog.INFO);
								arch.close();
							}
							return;
						}
				  }
	    	  catch(Exception e) {
	    		  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	   			//erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato en el archivo � un campo esta vacio</td>");
				archivoValido=false;
	             }

	    	  contadorLinea++;
	    	  if((contadorLinea%1000)==0) {
	    		  EIGlobal.mensajePorTrace("***ImportNominaLn.class Importando el registro ### "+contadorLinea, EIGlobal.NivelLog.INFO);
	    	  }
				//EIGlobal.mensajePorTrace("***ImportNominaLn.class &# de l�nea><<>><<"+contadorLinea + "><<>><<", EIGlobal.NivelLog.INFO);

	    	  i++;
	    	  registrosLeidos++;
				}//del for
					registrosLeidos+=2;
					double sumTotal = 0 ;
					//Registro Sumario
					//String registroSum = arch.readLine();
					String registroSum = registroDet;
					if(registroSum != null) {
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &registroSum&"+registroSum.length(), EIGlobal.NivelLog.DEBUG);
					}
					else {
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &registroSum& viene nulo.", EIGlobal.NivelLog.ERROR);
					}
					String sumTipoReg  = "";
					String sumNumSec   = "";
					String sumTotRegs  = "";
					String sumImpTotal = "";
	                try {
						if (registroSum == null || registroSum.length() != 29) {
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la longitud del sumario</td>");
					    archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en la longitud del sumario&", EIGlobal.NivelLog.INFO);
						}else {
							sumTipoReg=new String(registroSum.substring(0,1));
						    if ( !validaArchivo.validaDigito(sumTipoReg) || !validaArchivo.validaSumTipoReg(sumTipoReg) ){
							  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero de registro</td>");
							  archivoValido=false;
							  EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el n&uacute;mero de registro del sumario&", EIGlobal.NivelLog.INFO);
						    }
							sumNumSec= new String(registroSum.substring(1,6));
						    if(validaArchivo.validaDigito(sumNumSec) && validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)){
							 session.setLastSecNom(sumNumSec);
						    }
						    if ( !validaArchivo.validaDigito(sumNumSec) || !validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos) ){
							  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del sumario</td>");
							  archivoValido=false;
							  EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el n&uacute;mero secuencial del sumario&", EIGlobal.NivelLog.INFO);
						    }
						    sumTotRegs = new String(registroSum.substring(6,11));
						    if(validaArchivo.validaDigito(sumTotRegs) && validaArchivo.validaTotalDeRegistros(sumTotRegs, registrosLeidos)){
							  session.setTotalRegsNom(sumTotRegs);
							}
						    if ( !validaArchivo.validaDigito(sumTotRegs) || !validaArchivo.validaTotalDeRegistros(sumTotRegs, registrosLeidos) ){
							  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el total de registros del sumario</td>");
							  archivoValido=false;

							  EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el total de registros&", EIGlobal.NivelLog.INFO);
							}
						    sumImpTotal = new String(registroSum.substring(11,29));
						    if (validaArchivo.validaDigito(sumImpTotal)  ){
							  session.setImpTotalNom(sumImpTotal);
						    }
						    validaArchivo.cerrar();
							//***** everis se Comenta el valor que se asigna a validaArchivo  RandomAccessFile 08/05/2008  ..inicio
						    //validaArchivo = null;
							//***** everis se Comenta el valor que se asigna a validaArchivo  RandomAccessFile 08/05/2008  ..fin
							/*************** Nomina Concepto de Archvo */
						    session.setNominaNumberOfRecords(Integer.toString(registrosLeidos-2));
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Total de Registros en Archivo: "+session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("***ImportNominaLn.class &Total de Registros con Concepto: "+registrosConConcepto, EIGlobal.NivelLog.INFO);
							if(registrosConConcepto==0) {
								if(request.getSession().getAttribute("conceptoEnArchivo")!=null) {
									request.getSession().removeAttribute("conceptoEnArchivo");
								}
								request.getSession().setAttribute("conceptoEnArchivo","false");
							 }
							if(registrosConConcepto!=0 && registrosConConcepto!= (Integer.parseInt(session.getNominaNumberOfRecords())) ) {
								 erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Error: </td><td class=\"textabdatobs\" nowrap align=\"center\"> Si los registros incluyen Concepto debe especificarlo en todos.</td>");
								 archivoValido=false;
								 EIGlobal.mensajePorTrace("***ImportNominaLn.class &Si los registros incluyen concepto debe especificarlo en todos.&", EIGlobal.NivelLog.INFO);
							 }
							/*************** Nomina Concepto de Archvo */
							sumTotal = Double.parseDouble(sumImpTotal);
							/* Suma de los importes */
							if (sumTotal != sumImporte){
							  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe total </td>");
							  archivoValido=false;
							  EIGlobal.mensajePorTrace("***ImportNominaLn.class &Error en el Importe total&", EIGlobal.NivelLog.INFO);
							}
						} //else != 29
	                }
	   					catch(Exception e)
					    {
	   					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">L&iacute;nea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
						//***** everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..inicio
						finally{
				            if(null != validaArchivo){
				                try{
				                	validaArchivo.cerrar();
				                }catch(Exception e){
				                	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				                }
				                validaArchivo = null;
				            }
						}
						//****  everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..fin
					erroresEnElArchivo.addElement("</table>");
					erroresEnElArchivo.trimToSize();//a partir de esta linea el vector erroresEnElArchivo contiene la tabla de errores
					//String registroSumarioCompleto;
					//registroSumarioCompleto=sumTipoReg+sumNumSec+sumTotRegs+sumImpTotal;
					if (arch!=null){

						arch.close();
					}
					cantidadRegistros.addElement(sumNumSec);
					/*}//del if

				   else {

				        despliegaPaginaError( "Imposible Importar el archivo seleccionado, tiene mas de"+  Global.REG_MAX_IMPORTAR_NOMINA + " registros.",

						PAGODENOMINAX, "Importando Archivo",s25800h, request, response );

				        archivoValido=false;

						}*/
					if (archivoValido) {
						contenidoArchivoStr=new StringBuffer("");
						contenidoArchivoStr.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
						contenidoArchivoStr.append("<tr> ");
						contenidoArchivoStr.append("<td class=\"textabref\">");
						contenidoArchivoStr.append(PAGODENOMINAX);
						contenidoArchivoStr.append("</td>");
						contenidoArchivoStr.append("</tr>");
						contenidoArchivoStr.append("</table>");
						contenidoArchivoStr.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
						contenidoArchivoStr.append("<tr> ");
						contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
						contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descripci&oacute;n</td>");
						contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
						contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
						contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
						contenidoArchivoStr.append("</tr>");
							int contador=1;
							int residuo=0;
							residuo=contador % 2 ;
							String bgcolor="textabdatobs";
							if (residuo == 0){
								bgcolor="textabdatcla";
							}
							//10272002
							/**
							*		Modificado por Miguel Cortes Arellano
							*		Fecha: 19/12/03
							*		Proposito: Crear dinamicamente los horarios premier dependiendo del usuario de la presente session.
							*		Empieza codigo SINAPSIS
							*/
							String encFechApliformato = "";
							if (encFechApli.length() >= 8 ) {
								encFechApliformato = encFechApli.substring(2,4) + "/" + encFechApli.substring(0,2) + "/" + encFechApli.substring(4,8);
							} else {
								encFechApliformato = encFechApli;
							}
							// Finaliza Codigo Sinapsis
							String sumTmp = sumImpTotal.substring(0, sumImpTotal.length() - 2) + "." + sumImpTotal.substring(sumImpTotal.length() - 2);
							//String importe1 = FormatoMoneda.formateaMoneda(new Double(sumImpTotal).doubleValue());
						    sess.setAttribute("sumTmpSession", Double.parseDouble(sumTmp.trim()));
							String importe1 = FormatoMoneda.formateaMoneda(new Double(sumTmp).doubleValue());
				    		contenidoArchivoStr.append("<tr><td class=\"" );
							contenidoArchivoStr.append(bgcolor );
							contenidoArchivoStr.append(CENTER);
							contenidoArchivoStr.append(session.getCuentaCargo() );
							contenidoArchivoStr.append(TD);
				    		contenidoArchivoStr.append(TDCLASS );
							contenidoArchivoStr.append(bgcolor );
							contenidoArchivoStr.append(CENTER);
							contenidoArchivoStr.append(sess.getAttribute("descpCuentaCargo") );
							contenidoArchivoStr.append(TD);
						    contenidoArchivoStr.append(TDCLASS );
							contenidoArchivoStr.append(bgcolor );
							contenidoArchivoStr.append(CENTER);
							contenidoArchivoStr.append(encFechApliformato );
							contenidoArchivoStr.append(TD);
							contenidoArchivoStr.append(TDCLASS );
							contenidoArchivoStr.append(bgcolor );
							contenidoArchivoStr.append(CENTER);
							contenidoArchivoStr.append(importe1);
							contenidoArchivoStr.append(TD);
							contenidoArchivoStr.append(TDCLASS );
							contenidoArchivoStr.append(bgcolor );
							contenidoArchivoStr.append(CENTER);
							contenidoArchivoStr.append(session.getNominaNumberOfRecords());
							contenidoArchivoStr.append(TD);
							contenidoArchivoStr.append("</tr>");
							//contenidoArchivoStr.append("<tr><td width=\"40\" valign=top >&nbsp;</td><td valign=middle width=\"120\"  class=textabdatcla>Folio:&nbsp;<INPUT TYPE=text SIZE=15 NAME=folio></td>");
							/*************** Nomina Concepto de Archvo */
							if(!registroContieneConcepto) {
								contenidoArchivoStr.append("		<tr>");
								contenidoArchivoStr.append("		 <td ></td>");
								contenidoArchivoStr.append("		 <td class='textabdatobs' align=center> Concepto de Pago de Archivo</td>");
								contenidoArchivoStr.append("		 <td class='textabdatobs' colspan=3>");
								contenidoArchivoStr.append("		   <select name=Concepto class='textabdatobs'>");
								contenidoArchivoStr.append(getConceptos());
								contenidoArchivoStr.append("		   </select>");
								contenidoArchivoStr.append("		 </td>");
								contenidoArchivoStr.append("		</tr>");
							 }
							//favoritos
							contenidoArchivoStr.append("		<tr>");
							contenidoArchivoStr.append("		 <td ></td>");
							contenidoArchivoStr.append("		 <td class='textabdatobs' align=center>Si desea agregar a favoritos marque esta casilla</td>");
							contenidoArchivoStr.append("		 <td class='textabdatobs' colspan=3>");
							contenidoArchivoStr.append("		   <input type=\"checkbox\" name=\"chkBoxFav\" id=\"chkBoxFav\" onclick=\"javascript:seleccionarFavoritoArch(this);\"/>");
							contenidoArchivoStr.append("		   <input type=\"text\" disabled=\"true\" value=\"\" name=\"txtFav\" id=\"txtFav\" size=\"20\" onblur=\"validaEspecialesFavorito(this,'Favoritos');\"/>");
							contenidoArchivoStr.append("<input type=\"hidden\" name=\"strCheck\" id=\"strCheck\" />");
							contenidoArchivoStr.append("		 </td>");
							contenidoArchivoStr.append("		</tr>");
							/*************** Nomina Concepto de Archvo */
							/**
							 * INICIO PYME 2015 Se comenta codigo por la unificacion de token
							 * para que no muestre boton Enviar Regresar, ahora se
							 * utilizarian los del token
							 */
							contenidoArchivoStr.append("</table></td></tr>");
//							contenidoArchivoStr.append("<br><table align=center border=0 cellspacing=0 cellpadding=0>");
//							if( session.getToken().getStatus() == 1 ){
//								contenidoArchivoStr.append("<tr><td></td><td id=\"enviar\"><a href =javascript:js_envioConOTP(); border = 0><img src = /gifs/EnlaceMig/gbo25520.gif border=0 alt=Envio></a></td>");
//							} else {
//								contenidoArchivoStr.append("<tr><td></td><td id=\"enviar\"><a href = javascript:js_envio(); border = 0><img src = /gifs/EnlaceMig/gbo25520.gif border=0 alt=Envio></a></td>");
//							}
//							contenidoArchivoStr.append("<td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td></tr>" +
//									"<tr id=\"enviar2\" style=\"visibility:hidden\" class=\"tabmovtex\">" +
//									"<td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td><tr/>" +
//									"</table></td></tr>");
							/**
							 * INICIO PYME 2015 Se comenta codigo por la unificacion de token
							 */
					} // ArchvioValido
						EIGlobal.mensajePorTrace("***ImportNominaLn.class 1&El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);
				/*			try {

					EIGlobal.mensajePorTrace("***ImportNominaLn.class &El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);

					if ( originalName.substring( originalName.length() - 4,

							originalName.length() - 3 ).equals( "." ) )

				        originalName = originalName.substring( 0, originalName.length() - 4 ) + ".nom";

					else

						originalName = originalName + ".nom";



				} catch( Exception e ) {

					EIGlobal.mensajePorTrace("***ImportNominaLn.class Ha ocurrido una excepcion al agregar extension al archivo: %execute()"+e, EIGlobal.NivelLog.INFO);

					originalName = originalName+".nom";

				}

				EIGlobal.mensajePorTrace("***ImportNominaLn.class &El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);

				descargaArchivo="document.location='"+"/Download/" + originalName + "'";*/
				String archivoName = "";
					archivoName = (String) sess.getAttribute("nombreArchivo");
				//***********************************************

				//modificaci�n para integraci�n pva 07/03/2002

	            //***********************************************
		         session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
	            //***********************************************



		//	if (bandera)

		//	{
				if ( archivoValido ) {
					/*		if ( archR.copiaLocalARemoto( archivoName.substring(archivoName.lastIndexOf("/") + 1 ), "WEB" ) ) {

								session.setRutaDescarga( "/Download/" + ( archivoName.substring(archivoName.lastIndexOf("/") + 1 ) ) );

							} else {



							}*/
							request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf('/')+1));
							sess.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf('/')+1));
							request.setAttribute(CONTENIDOARCHIVO, contenidoArchivoStr.toString());
							sess.setAttribute(CONTENIDOARCHIVO, contenidoArchivoStr.toString());
							//request.setAttribute("DescargaArchivo",session.getRutaDescarga());
							request.setAttribute("encFechApli",encFechApli);
							sess.setAttribute("encFechApli",encFechApli);
							//String msg="";
							/**
							*		Modificado por Miguel Cortes Arellano
							*		Fecha: 23/12/03
							*		Proposito: Enviar un mensaje de error al usuario en caso de que tenga facultades de cliente Premier y no se obtenga los horarios.
							*		Empieza codigo SINAPSIS
							*/
							/*if(FacultadPremier.indexOf("PremierVerdadero") != -1)//FIXME Verificar si en esta facultad se debe hacer algo
						    {
								if(HorarioPremier.indexOf("NOMI0000")!= -1)
									msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros\", 1)";
								else
									msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros. Pero, no se obtuvieron los Horarios por favor trate mas tarde.\", 1)";
					        }else{*/
									//msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros\", 1)";
									final StringBuffer msg=new StringBuffer();
									msg.append("cuadroDialogo(\"Se importaron exitosamente: ");
									msg.append(session.getNominaNumberOfRecords());
									msg.append(" registros\", 1)");
									EIGlobal.mensajePorTrace( "Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros", EIGlobal.NivelLog.INFO);
							//}
							//Finaliza Codigo SINAPSIS
							request.setAttribute("infoImportados",msg.toString());
							request.setAttribute("cantidadDeRegistrosEnArchivo",session.getNominaNumberOfRecords());
							request.setAttribute( ENCABEZADOX, CreaEncabezado(PAGODENOMINAX,SERVSPAGNOMINALN,"s25800IIh",request));
							sess.setAttribute( ENCABEZADOX, CreaEncabezado(PAGODENOMINAX,SERVSPAGNOMINALN,"s25800IIh",request));
					//		EIGlobal.total_lineas_leidas(registros,registros);
							/**
							 * SE COMENTA ESTA LINEA PARA QUE CONTINUE Y SE VAYA A  VALIDAR EL TOKEN
							 * INCIO PYME 2015 UNIFICACION DE TOKEN, AHORA SE REDIRECCIONA A
							 * OpcionesNominaLn
							 */
							EIGlobal.mensajePorTrace("Redireccionando a OpcionesNominaLn - execute()", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("1 Ln === PYME", EIGlobal.NivelLog.INFO);
							request.setAttribute("operacion", "envio");
							request.setAttribute("tipoArchivo", "enviado");
							//M022532 NOMINA EN LINEA - INI
							request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
							//M022532 NOMINA EN LINEA - FIN							
							RequestDispatcher rdSalida = getServletContext()
							.getRequestDispatcher("/enlaceMig/OpcionesNominaLn");
							rdSalida.forward( request , response );
//							evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response );
							/**
							 * FIN PYME 2015 UNIFICACION DE TOKEN
							 */
							EIGlobal.mensajePorTrace( "***ImportNominaLn.class .:. Redireccionando a .:. "+IEnlace.MTO_NOMINA_TMPLN, EIGlobal.NivelLog.INFO);
				} else {
					//String tablaErrores="";
					final StringBuffer tablaErrores=new StringBuffer();
					for(int z=0; z<erroresEnElArchivo.size(); z++){
						//tablaErrores=tablaErrores+erroresEnElArchivo.elementAt(z);
						tablaErrores.append(erroresEnElArchivo.elementAt(z));
					}
					request.setAttribute("archivoEstatus",tablaErrores.toString());
					request.setAttribute("ArchivoErr","archivoErrores();");
					request.setAttribute("archivo_actual","Error al Importar: "+archivoName.substring(archivoName.lastIndexOf('/')+1));
					request.setAttribute("Cuentas","<Option value = \"\">Error en el archivo");
					request.setAttribute(CONTENIDOARCHIVO,"");
					request.setAttribute("tipoArchivo","importadoErroneo");
					request.setAttribute("cantidadDeRegistrosEnArchivo","");
					//EIGlobal.total_lineas_leidas(registros,registros);
					//evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response );
					//EIGlobal.mensajePorTrace( "***ImportNominaLn.class .:. Redireccionando a::: "+IEnlace.MTO_NOMINA_TMPLN, EIGlobal.NivelLog.INFO);
					sess.setAttribute("archivoEstatus",tablaErrores);
					sess.setAttribute("ArchivoErr","archivoErrores();");
					String url =PAG_TMPL_SRV_LIN + "?modulo=3&submodulo=2&accion=iniciar";
					response.sendRedirect(url);
				}
				//	  } // bandera
			}//ejecutaimportar
			/**
			 * registrosEnArchivo Extrae los registros en el archivo
			 * @param pathArchivoNom Ruta archivo
			 * @param request  Recibe parametros
			 * @param response Respuesta parametros
			 * @return cantRegistros Cantidad de registros archivo
			 * @throws IOException Excepcion en la entrada salida de archivo
			 * @throws ServletException expcecion en el Servlet
			 */
			public int registrosEnArchivo(String pathArchivoNom, HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
				EIGlobal.mensajePorTrace( "***ImportNominaLn.class &Entrando al metodo registrosEnArchivo 35&", EIGlobal.NivelLog.INFO);
				pathArchivoNom = pathArchivoNom.trim();
				BufferedReader arch = null;
				FileReader reader = null;
				reader  = new FileReader( pathArchivoNom );
				arch	= new BufferedReader( reader );
				String registroLeido = "";
				int cantRegistros    = 0;
				try {
					do {
						registroLeido = arch.readLine();
						if (registroLeido != null) {
							cantRegistros++;
						}
					} while( registroLeido != null );
				} catch(IllegalArgumentException e) {
					EIGlobal.mensajePorTrace("***ImportNominaLn.class Ha ocurrido una excepcion IllegalArgumentException en: %registrosEnArchivo(1)"+e, EIGlobal.NivelLog.INFO);
				} catch(FileNotFoundException e) {
					EIGlobal.mensajePorTrace("***ImportNominaLn.class Ha ocurrido una excepcion FileNotFoundException en: %registrosEnArchivo(2)"+e, EIGlobal.NivelLog.INFO);
				} catch(SecurityException e){
					EIGlobal.mensajePorTrace("***ImportNominaLn.class Ha ocurrido una excepcion SecurityException en: %registrosEnArchivo(3)"+e, EIGlobal.NivelLog.INFO);
				} catch(IOException e) {
					EIGlobal.mensajePorTrace("***ImportNominaLn.class Ha ocurrido una excepcion IOException en: %registrosEnArchivo(4)"+e, EIGlobal.NivelLog.INFO);
				}
				//***** everis Cerrando BufferedReader 08/05/2008  ..inicio
				finally {

					try {

				if (arch!=null){
					arch.close();
				}
					}catch(IOException e) {
						EIGlobal.mensajePorTrace("IOExcepcion:"+e.toString(), EIGlobal.NivelLog.DEBUG);
					}
					try {

				if(reader != null) {
					reader.close();
				}
					}catch(IOException e) {
						EIGlobal.mensajePorTrace("IOExcepcion:"+e.toString(), EIGlobal.NivelLog.DEBUG);
					}
				}
				//****  everis Cerrando BufferedReader 08/05/2008  ..fin
				EIGlobal.mensajePorTrace("***registrosEnArchivo() cantRegistros: "+cantRegistros, EIGlobal.NivelLog.INFO);
				return cantRegistros;
			}
			/**
			 * METODO PARA AGRGAR PUNTO	DECIMAL	AL IMPORTE
			 * @param importe Importe sin punto
			 * @return importeConPunto Importe Con Punto
			 */
			public String  agregarPunto(String importe){
				EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo agregarPunto()&", EIGlobal.NivelLog.INFO);
		        String importeConPunto	= "";
		        String cadena1			= importe.substring(0,importe.length()-2);
		        cadena1				= cadena1.trim();
		        String cadena2			= importe.substring(importe.length()-2, importe.length());
		        importe				= cadena1+" "+cadena2;
		        importeConPunto		= importe.replace(' ','.');
		        if(importeConPunto.startsWith("..")) {
		        	importeConPunto	= ".0"+cadena2.trim();
				}
		        EIGlobal.mensajePorTrace("***ImportNominaLn.class &Saliendo del metodo agregarPunto()&", EIGlobal.NivelLog.INFO);
		        return importeConPunto;
			}
			/**
			 * METODO PARA FORMATEAR	EL IMPORTE CUANDO CONTENGA DECIMALES
			 * @param importe importe sin formato
			 * @return importeFormateado Importe Formateado
			 */
			public String formateaImporte(String importe){

				EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo formateaImporte()&", EIGlobal.NivelLog.INFO);
				String importeFormateado	= "";
				boolean contienePunto		= false;
				int i=0;
				for (i=0; i<importe.length(); i++){
					if(importe.charAt(i)=='.'){
						contienePunto=true;
						break;
					}
				}
				if(contienePunto) {
					importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
				}
				else{
					importeFormateado=importe;
				}
				EIGlobal.mensajePorTrace("***ImportNominaLn.class &Saliendo del metodo formateaImporte()&", EIGlobal.NivelLog.INFO);
				return importeFormateado;
			}
			/**
			 * metodo para visualizar los errores en una pagina html
			 * archivoValido=false; este metodo no se utiliza en este programa...
			 * @param archivoValido si el archivo es valido
			 * @param s mensaje del archivo
			 * @param variable error del archivo
			 * @param request  Recibe parametros
			 * @param response Respuesta parametros
			 * @throws IOException Excepcion en la entrada salida de archivo
			 * @throws ServletException expcecion en el Servlet
			 */
			public void salirDelProceso( boolean [] archivoValido, String s, String variable,

			HttpServletRequest request, HttpServletResponse response )

			throws ServletException, IOException {

				EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);

				HttpSession sess = request.getSession();

			    BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);

				StringBuffer contArchivoStr=new StringBuffer("");

			    contArchivoStr.append("<table border=0 align=center>");

				contArchivoStr.append("<tr><td colspan=9 align=center><b>Pago de Nomina</b></td></tr>");

				contArchivoStr.append("<tr bgcolor=red+><td colspan=9 align=center><b>Se han detectado errores al leer el archivo</b></td></tr>");

				contArchivoStr.append("<tr><td colspan=9 align=center><b>");
				contArchivoStr.append(s);
				contArchivoStr.append("</b></td></tr>");

			    contArchivoStr.append("<tr><td colspan=9 align=center><b>");
				contArchivoStr.append(variable);
				contArchivoStr.append("</b></td></tr>");

				contArchivoStr.append("</tr></table>");

				request.setAttribute("newMenu", session.getFuncionesDeMenu());

				request.setAttribute("MenuPrincipal", session.getStrMenu());

				request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAX,SERVSPAGNOMINALN,S25800H,request));

			    request.setAttribute("erroresEnArchivo",contArchivoStr.toString());

		//		popup de jorge

		//		despliegaPaginaError(contArchivoStr,"Pago de Nomina","Errores al momento de importar el archivo");

			    archivoValido[0]=false;

			    evalTemplate(IEnlace.ERR_NOMINA_TMPL, request, response );

			}
			/**
			 * arregloCuentas
			 * @param nameArchivo nombre del archivo
			 * @param numeroRegs numero de registros
			 * @return arreglo
			 */
			public String[] arregloCuentas(File nameArchivo, int numeroRegs){
				//public String[] arregloCuentas(String nameArchivo, int numeroRegs){
				EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo arregloCuentas &", EIGlobal.NivelLog.INFO);
				//nameArchivo = nameArchivo.trim();
				//BufferedReader archivoLectura = null;
				String lineaQueSeLeyo	 = "";
				String[] arreglo		 = new String[numeroRegs];
				//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

				RandomAccessFile archivoLectura = null;
				//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin
				try{
					//archivoLectura = new BufferedReader( new FileReader( nameArchivo ) );
					//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

					//RandomAccessFile archivoLectura=new RandomAccessFile(nameArchivo,"r");
					archivoLectura=new RandomAccessFile(nameArchivo,"r");
					//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin
					/*try{
					EIGlobal.mensajePorTrace("***ImportNominaLn.class & 4&", EIGlobal.NivelLog.INFO);
					archivoLectura.seek(41);
					lineaQueSeLeyo		= archivoLectura.readLine();
					EIGlobal.mensajePorTrace("***ImportNominaLn.class & lineaQueSeLeyo5&"+ lineaQueSeLeyo, EIGlobal.NivelLog.INFO);
					if(lineaQueSeLeyo==null) {
						lineaQueSeLeyo		= archivoLectura.readLine();
					}
					} catch(Exception e){
						e.printStackTrace();
					}*/
					archivoLectura.seek(41);
					lineaQueSeLeyo = archivoLectura.readLine();
					if (lineaQueSeLeyo.length()  == 126  ){
						archivoLectura.seek(40);
						lineaQueSeLeyo = archivoLectura.readLine();
						}else {
						    archivoLectura.seek(41);
							lineaQueSeLeyo = archivoLectura.readLine();
							}
					for(int i=0; i<arreglo.length; i++){
						if ( ("").equals(lineaQueSeLeyo) ) {
							lineaQueSeLeyo = archivoLectura.readLine();
						}
						if ( lineaQueSeLeyo == null ) {
							break;
						}
						if(lineaQueSeLeyo.length()>108) { //jtg  para evitar el error
							arreglo[i]		= lineaQueSeLeyo.substring(93,109);
						}else {
							i--;
						}
						lineaQueSeLeyo = archivoLectura.readLine();
					}
				   	//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

					archivoLectura.close();
					//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
				} catch(IOException exception) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(exception), EIGlobal.NivelLog.INFO);
				} catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
				finally{
		            if(null != archivoLectura){
		                try{
		                	archivoLectura.close();
		                }catch(IOException e){}
		                archivoLectura = null;
		            }
		        }
				//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
				return arreglo;
			}
			/**
			 * arregloNumeroEmpleado
			 * @param nameArchivo nombre del archivo
			 * @param numeroRegs numero de registros
			 * @return arregloNumeroEmpleado
			 */
			public String[] arregloNumeroEmpleado(File nameArchivo, int numeroRegs){
				//public String[] arregloNumeroEmpleado(String nameArchivo, int numeroRegs){
				EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo arregloNumeroEmpleado&", EIGlobal.NivelLog.INFO);
				//nameArchivo =nameArchivo.trim();
				//BufferedReader archivoLectura = null;
				String lineaQueSeLeyo	= "";
				String[] arregloNumeroEmpleado = new String[numeroRegs];
				//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

				RandomAccessFile archivoLectura = null;
				//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin
					try{
						//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

						//RandomAccessFile archivoLectura = new RandomAccessFile(nameArchivo,"r");
						archivoLectura = new RandomAccessFile(nameArchivo,"r");
						//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin
						//archivoLectura = new BufferedReader( new FileReader( nameArchivo ) );
						archivoLectura.seek(41);
						lineaQueSeLeyo = archivoLectura.readLine();
						if (lineaQueSeLeyo.length()  == 126  ){
							archivoLectura.seek(40);
							lineaQueSeLeyo = archivoLectura.readLine();
							}else {
							    archivoLectura.seek(41);
								lineaQueSeLeyo = archivoLectura.readLine();
								}
						for ( int i = 0; i < arregloNumeroEmpleado.length; i++ ) {
							if ( ("").equals(lineaQueSeLeyo) ) {
								lineaQueSeLeyo = archivoLectura.readLine();
							}
							if ( lineaQueSeLeyo == null ) {
								break;
							}
							if(lineaQueSeLeyo.length()> 12){
								arregloNumeroEmpleado[i] = lineaQueSeLeyo.substring( 6, 13 );
							}else {
								i--;
							}
							lineaQueSeLeyo = archivoLectura.readLine();
						}
						//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

						archivoLectura.close();
						//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
					} catch(IOException exception) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(exception), EIGlobal.NivelLog.INFO);
					} catch(Exception e){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
					//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
					finally{
			            if(null != archivoLectura){
			                try{
			                	archivoLectura.close();
			                }catch(IOException e){}
			                archivoLectura = null;
			            }
			        }
					//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
					return arregloNumeroEmpleado;
	}
			/**
			 * verificaCuentaCargo
			 * @param arrayCuentas arreglo de las cuentas
			 * @param cuentaCargo cuenta de cargo
			 * @return true ? false
			 */
			boolean verificaCuentaCargo(String[][] arrayCuentas, String cuentaCargo){
				for(int indice=1; indice<=Integer.parseInt(arrayCuentas[0][0]); indice++){
					if ((cuentaCargo).equals(arrayCuentas[indice][1].trim())&&("P").equals(arrayCuentas[indice][2].trim())) {
					return true;
					}
				}
				return false;
			}
			/**
			 * formatea
			 * @param strNum cadena con numeros
			 * @return forma ? null
			public String formatea(String strNum) {
				long num = 0;
				String forma = null;
				// Se verifica que la cadena sea un n�mero y se quitan los posible ceros a la izq.
				try {
					num = Long.parseLong(strNum);
				}catch(NumberFormatException e) {

					EIGlobal.mensajePorTrace("formatea: La cadena no es un n�mero"+e.toString(), EIGlobal.NivelLog.DEBUG);
					return null;
				}
				forma = "" + num;
				// Se a�ade el punto
				forma=forma.substring(0,forma.length()-2) + "." + forma.substring(forma.length()-2);
				return forma;
			}*/
			/**
			 * formatoMoneda
			 * @param cantidad cantidad
			 * @return formatoFinal formato de la moneda
			 */
			public String formatoMoneda( String cantidad ) {
				EIGlobal.mensajePorTrace("***Entrando a formatoMoneda ", EIGlobal.NivelLog.INFO);
			    cantidad = cantidad.trim();
				String language = "la"; // ar
				String country  = "MX";  // AF
				Locale local    = new Locale(language,  country);
				NumberFormat nf = NumberFormat.getCurrencyInstance(local);
				String formato  = "";
				String cantidadDecimal = "";
				String formatoFinal    = "";
				String cantidadtmp     = "";
				if(cantidad ==null || ("").equals(cantidad)) {
					cantidad="0.0";
				}
				if(cantidad.length() > 2){
					try {
						cantidadtmp =cantidad.substring(0,cantidad.length()-2);
						cantidadDecimal = cantidad.substring(cantidad.length()-2);
						formato = nf.format(new Double(cantidadtmp).doubleValue());
						formato=formato.substring(1,formato.length()-2);
						formatoFinal = "$ " + formato + cantidadDecimal;
					} catch(NumberFormatException e) {
						EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);
						formatoFinal= "$ " + "0.00";}
				}else {
				try {
				       if (cantidad.length() > 1){
						cantidadDecimal = cantidad.substring(cantidad.length()-2);
						formatoFinal = "$ " + "0."+ cantidadDecimal;
					   } else {
						cantidadDecimal = cantidad.substring(cantidad.length()-1);
						formatoFinal = "$ " + "0.0"+ cantidadDecimal;
						}
					} catch(NumberFormatException e) {
						EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);
						formatoFinal="$ " + "0.00";}
				}
				return formatoFinal;

	}
/**
 * getFechaString
 * @param fecha fecha entrada
 * @return GregorianCalendar ? null
 */
public GregorianCalendar getFechaString(String fecha)
	{
	  EIGlobal.mensajePorTrace("ImportNominaLn - getFechaString(): Fecha cambio: " + fecha, EIGlobal.NivelLog.INFO);
      try
		{
		  if(fecha.length()==10) {
			  int Anio = Integer.parseInt(fecha.substring(6));
			  int Mes = Integer.parseInt(fecha.substring(3,5))-1;
			  int Dia = Integer.parseInt(fecha.substring(0,2));
			  return new GregorianCalendar(Anio, Mes, Dia);
			}else {
			  EIGlobal.mensajePorTrace("ImportNominaLn - getFechaString(): Error en la longitud de la fecha.", EIGlobal.NivelLog.INFO);
			}

		} catch (Exception e)
		 {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	     }
	   return null;
     }
/**
 * Nomina Concepto de Archvo
 * @return cadenaRegreso
 */
public StringBuffer getConceptos()
	{
	   EI_Query BD= new EI_Query();
	   Hashtable listaConceptos=new Hashtable();
	   StringBuffer cadenaRegreso=new StringBuffer("");

	   String sqlConcepto  = "SELECT id_concepto,Concepto from INOM_CONCEPTOS order by id_concepto";
	   listaConceptos =BD.ejecutaQuery(sqlConcepto);
	   if( listaConceptos==null ) {
			EIGlobal.mensajePorTrace( "ImportNominaLn - getConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ImportNominaLn - getConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);

			cadenaRegreso.append("<option value='01'>Pago de N�mina. </option>\n");
			cadenaRegreso.append("<option value='02'>Pago Vacaciones. </option>\n");
			cadenaRegreso.append("<option value='03'>Pago Gratificaciones. </option>\n");
			cadenaRegreso.append("<option value='04'>Pago por Comisiones. </option>\n");
			cadenaRegreso.append("<option value='05'>Pago por Beca. </option>\n");
			cadenaRegreso.append("<option value='06'>Pago por Pensi�n. </option>\n");
			cadenaRegreso.append("<option value='07'>Pago por Subsidios. </option>\n");
			cadenaRegreso.append("<option value='08'>Otros pagos por transferencia. </option>\n");
			cadenaRegreso.append("<option value='09'>Pago por Honorarios. </option>\n");
			cadenaRegreso.append("<option value='10'>Pr�stamo. </option>\n");
			cadenaRegreso.append("<option value='11'>Pago de vi�ticos. </option>\n");
			cadenaRegreso.append("<option value='12'>Anticipo de vi�ticos. </option>\n");
			cadenaRegreso.append("<option value='13'>Fondo de ahorro. </option>\n");
		  }
		else
		  {
			EIGlobal.mensajePorTrace( "ImportNominaLn - getConceptos()-> Tama�o : "+listaConceptos.size() , EIGlobal.NivelLog.INFO);
			String []datos=new String[2];
			for (int i=0;i<listaConceptos.size();i++ )
			  {
				 datos=(String[])listaConceptos.get(String.valueOf(i));
				 cadenaRegreso.append("<option value='"+datos[0]+"'>");
				 cadenaRegreso.append(datos[1]);
				 cadenaRegreso.append("</option>\n");
			  }
		  }
	   return cadenaRegreso;
	}
/**
 * getIDConceptos
 * @return listaIDConceptos
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public Vector getIDConceptos()
	{
	   EI_Query BD= new EI_Query();
	   Vector listaIDConceptos=new Vector();;

	   String sqlIDConcepto  = "SELECT id_concepto from INOM_CONCEPTOS order by id_concepto";
	   listaIDConceptos =BD.ejecutaQueryCampo(sqlIDConcepto);
	   if( listaIDConceptos==null ) {
			listaIDConceptos=new Vector();
			EIGlobal.mensajePorTrace( "ImportNominaLn - getIDConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ImportNominaLn - getIDConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);
			for(int a=1;a<14 ;a++ ) {
				String dato=(a<10)?"0"+a:String.valueOf(a);
				listaIDConceptos.add(dato);
			}
		 }
/*
 * */
	   return listaIDConceptos;
	}
/*************** Nomina Concepto de Archvo */

/**
 * El metodo comparaFechas(String strFecha)
 * Valida que la fecha del parametro no sea mayor a la fecha actual
 * @param strFecha fecha
 * @return fActual true ? false
 */
public static boolean comparaFechas(String strFecha){
	//EIGlobal.mensajePorTrace("ImportNominaLn:: comparaFechas :: Validando fecha: "+strFecha, EIGlobal.NivelLog.DEBUG);
	String fActual="";
	try {
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("MM-dd-yyyy", new Locale("es", "MX"));
	 	Date fchActual = new Date();
		fActual=formatoDelTexto.format(fchActual);
	 } catch (Exception ex) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
	 }
	 return (fActual).equals(strFecha) ? true : false;

}
/**
 * Metodo que verifica si el archivo esta vacio
 * @param fArchivo archivo
 * @return flag bandera
 */
public boolean fileIsEmpty(File fArchivo){
	boolean flag = false;
	BufferedReader br = null;
	try{
		br = new BufferedReader(new FileReader(fArchivo));
		String strFirstLine;
		EIGlobal.mensajePorTrace("ImportNominaLn:: fileIsEmpty :: Validando Contenido de Archivo", EIGlobal.NivelLog.DEBUG);
		strFirstLine=br.readLine();

		if(strFirstLine == null){
			EIGlobal.mensajePorTrace("ImportNominaLn::fileIsEmpty:: El archivo esta vacio ", EIGlobal.NivelLog.DEBUG);
			flag = true;
		}
	}catch(Exception e){
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	}finally{
		try {
			if (br != null){
				br.close();
			}
		}catch(IOException e) {
			EIGlobal.mensajePorTrace("ImportNominaLn ::  fileIsEmpty :: problemas al cerrar archivo: "+new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
		}
	}
	return flag;
}

/**
 * Metodo encargado de validar texto y alfa
 * @param campo texto a validar
 * @param tipoValidacion alfa o solo texto
 * @return campoValido resultado validacion
 */
public static boolean validaCampo(String campo, int tipoValidacion) {

	EIGlobal.mensajePorTrace("ImportNominaLn ::  validaCampoTexto :: Entrando a validar texto ", EIGlobal.NivelLog.DEBUG);
	String caracteresPermitidos="";
	if(tipoValidacion == 0){
		caracteresPermitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ��" + " ";
	}else if(tipoValidacion == 1){
		caracteresPermitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" + " ";
	}else{
		caracteresPermitidos = "0123456789";
	}
	boolean campoValido = false;
	int carValido = 0;
	char letra;

	EIGlobal.mensajePorTrace("ImportNominaLn ::  validaCampoTexto :: recorriendo cadena: "+campo+" tipoValidacion::: "+tipoValidacion, EIGlobal.NivelLog.DEBUG);
	for (int i = 0; i < campo.length(); i++) {
		letra = campo.charAt(i);
		for (int j = 0; j < caracteresPermitidos.length(); j++) {
			if (letra == caracteresPermitidos.charAt(j)){
				carValido++;
			}
		}
	}

	if (carValido == campo.length()) {
		campoValido = true;
	}

	EIGlobal.mensajePorTrace("ImportNominaLn ::  validaCampoTexto :: campoValido: "+campoValido, EIGlobal.NivelLog.DEBUG);

	return campoValido;

}

/**
 * Verifica duplicidad de registros
 * @param arrayNumerosEmpleado arreglo numero de empleados
 * @param numeroParaComparar valor a comparar
 * @return devuelve true si hay repetidos
 */
public boolean verificaDuplicidadNumeroEmpleado(String[] arrayNumerosEmpleado, String numeroParaComparar){
	EIGlobal.mensajePorTrace("***ImportNominaLn.class &Entrando al metodo verificaDuplicidadNumeroEmpleado()& "+arrayNumerosEmpleado.length, EIGlobal.NivelLog.INFO);
	boolean var=false;
	if(arrayNumerosEmpleado.length!=0){
		int ocurrencias=0;
		for(int a=0; a<arrayNumerosEmpleado.length; a++){
			if(arrayNumerosEmpleado[a].trim().equals(numeroParaComparar)){
				ocurrencias=ocurrencias+1;
			}
		}
		if(ocurrencias>1){
			var= true;
		}else{
			var= false;
		}
	}

	return var;
}

}