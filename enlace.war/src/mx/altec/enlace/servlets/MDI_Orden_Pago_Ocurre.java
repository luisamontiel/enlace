package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.PagOcurreMonitorPlusUtils;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
//VSWF
/**Praxis
*ivn Ma. Isabel Valencia Navarrete
*25/01/2005
*Q05-0001087
*CUANDO SE REALIZA UNA TRANSFERENCIA PINTAR EN EL LOG FECHA, HR,
*DIRECCI�N IP(DE DONDE FUE REALIZADA LA TRANSFERENCIA)
*EJEMPLO:
*[20/Jan/2005 12:02:03] DIRECCI�N IP  TRAMA
*/

public class MDI_Orden_Pago_Ocurre extends BaseServlet{
	short  suc_opera			=(short)787;
 	String menuSesion			= "";
 	int    nmaxoper;
	String strInhabiles         = "";

	/**
	 * DefaultAction
	 * @param req objeto HttpServletRequest
	 * @param res objeto HttpServletResponse
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		try {
			//L�nea agregada SJR
			HttpSession sess = req.getSession();
	        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
			EIGlobal.mensajePorTrace("***MDI_Orden_Pago_Ocurre.class Entrando a execute ", EIGlobal.NivelLog.DEBUG);
			boolean sesionvalida = SesionValida( req, res ); /*Valida si la sesi�n es valida*/

			if(sesionvalida)
			{
				/******************************************Inicia validacion OTP**************************************/
				String valida = req.getParameter ( "valida" );

				if ( validaPeticion( req, res,session,sess,valida))
				{
					  EIGlobal.mensajePorTrace("\n\n ENTR� A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.DEBUG);
				}
				/******************************************Termina validacion OTP**************************************/
				else
				{
					if (req.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER) == null) {
						if (req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
							req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
									req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
						} else if (getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
							req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
									getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER));
						}
					}

					strInhabiles =diasInhabilesDesp();
					if("".equals(strInhabiles.trim()))          /* Valida los dias inhabiles*/
						strInhabiles="01/01/2002,21/03/2002";
					//System.out.println("En dias inhabiles----"+strInhabiles);
				    if (strInhabiles.trim().length()==0)
				    {
			          //System.out.println("En dias inhabiles----Error");
		              despliegaPaginaError("Servicio no disponible por el momento",req,res);
					  return;
				    }

					/*Inicializa Variables*/

					String contrato				= session.getContractNumber();
					String usuario				= session.getUserID8();
					String clave_perfil			= session.getUserProfile();
					suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
					
					String personalidadSES = req.getSession().getAttribute("personalidad") != null 
						? req.getSession().getAttribute("personalidad").toString() : "";
					String personalidad	= req.getParameter("personalidad") != null 
						? (String) req.getParameter("personalidad") : personalidadSES ;
					String opcion = req.getParameter("opcion") != null  ? (String) req.getParameter("opcion") :  "0";
					
					EIGlobal.mensajePorTrace("***MDI_Orden_Pago_Ocurre.class personalidad "+personalidad, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***MDI_Orden_Pago_Ocurre.class opcion "+opcion, EIGlobal.NivelLog.INFO);
					nmaxoper = Global.MAX_REGISTROS;  /*20 Registros*/

					if (("0".equals(opcion)) || ("1".equals(opcion)) || ("2".equals(opcion))) {
						EIGlobal.mensajePorTrace("\n\nPresentar pantalla preliminar\n\n", EIGlobal.NivelLog.DEBUG);
						Genera_Pantalla_Ocurre(contrato,usuario,clave_perfil,personalidad,opcion,req,res);


					}
					if ("3".equals(opcion)) {

						EIGlobal.mensajePorTrace("\n\n\n ANTES DE VERIFICACION DE TOKEN \n\n\n", EIGlobal.NivelLog.DEBUG);
						//interrumpe la transaccion para invocar la validaci�n



						String [] Arreglo_Cuentas_Cargo_array = req.getParameter("Arreglo_Cuentas_Cargo").toString().split("@");
						String [] Arreglo_Importes_array = req.getParameter("Arreglo_Importes").toString().split("@");
						String [] arregloestatus1_array = req.getParameter("arregloestatus1").toString().split("@");

						String Arreglo_Cuentas_Cargo_string = "";
						String Arreglo_Importes_string = "";
						String arregloestatus1_string = "";

						for (int aux=0; aux<arregloestatus1_array.length; aux++) {
							if (arregloestatus1_array[aux].equals("1")) {
								Arreglo_Cuentas_Cargo_string = Arreglo_Cuentas_Cargo_string.concat(Arreglo_Cuentas_Cargo_array[aux]).concat("@");
								Arreglo_Importes_string = Arreglo_Importes_string.concat(Arreglo_Importes_array[aux]).concat("@");
								arregloestatus1_string = arregloestatus1_string.concat(arregloestatus1_array[aux]).concat("@");
							}
						}

						req.setAttribute("Arreglo_Cuentas_Cargo_string", Arreglo_Cuentas_Cargo_string);
						req.setAttribute("Arreglo_Importes_string", Arreglo_Importes_string);
						req.setAttribute("arregloestatus1_string", arregloestatus1_string);

						///////////////////--------------challenge--------------------------
						String validaChallenge = req.getAttribute("challngeExito") != null
							? req.getAttribute("challngeExito").toString() : "";

						EIGlobal.mensajePorTrace("--------------->validaChallenge MDI_ORDEN_PAGO_OCURRE: " + validaChallenge, EIGlobal.NivelLog.DEBUG);

						if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
							EIGlobal.mensajePorTrace("--------------->Entrando challenge MDI_ORDEN_PAGO_OCURRE" + validaChallenge, EIGlobal.NivelLog.DEBUG);
							//ValidaOTP.guardaParametrosEnSession(req);
							guardaParametrosEnSession(req);

							req.getSession().setAttribute("personalidad", personalidad);

							validacionesRSA(req, res);
							return;
						}

						//Fin validacion rsa para challenge
						///////////////////-------------------------------------------------


						boolean valBitacora = true;
						if(  valida==null )
						{
							EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

							boolean solVal=ValidaOTP.solicitaValidacion(
										session.getContractNumber(),IEnlace.PAGO_OCURRE);

							if( session.getValidarToken() &&
								session.getFacultad(session.FAC_VAL_OTP) &&
								session.getToken().getStatus() == 1 &&
								solVal )
							{
								//VSWF-HGG -- Bandera para guardar el token en la bit�cora
								req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
								EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);

								guardaParametrosEnSession(req);

								req.getSession().removeAttribute("mensajeSession");
								ValidaOTP.mensajeOTP(req, "OrdenPago");

								ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
							}
							else{
	                                ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
	                                valida="1";
	                                valBitacora = false;
								}

						}


						//retoma el flujo
						if( valida!=null && valida.equals("1"))
						{
							if (valBitacora)
							{
								try{

									HttpSession sessionBit = req.getSession(false);
									Map tmpAtributos = (HashMap) sessionBit.getAttribute("atributosBitacora");
									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion ="";
									String concepto ="";
									String token = "0";
									//if (req.getParameter("token") != null && !req.getParameter("token").equals(""))
									{token = req.getParameter("token");}

									String cuentaS ="0";
									Double importeTotal = 0.0;

									if (tmpAtributos!=null) {
										Enumeration enumer = req.getAttributeNames();

										while(enumer.hasMoreElements()) {
											String name = (String)enumer.nextElement();
											System.out.println("Atributo " + name + "->" + tmpAtributos.get(name));
											EIGlobal.mensajePorTrace("Atributo " + name + "->" + tmpAtributos.get(name), EIGlobal.NivelLog.INFO);
										}

											if(tmpAtributos.get("Arreglo_Cuentas_Cargo_string")!=null)
												cuentaS = tmpAtributos.get("Arreglo_Cuentas_Cargo_string").toString();

											if(cuentaS.split("@").length >= 1){
												String arregloCtas[] = cuentaS.split("@");
												cuentaS = arregloCtas[0];
											}


												String cadena = tmpAtributos.get("Arreglo_Importes_string").toString();
												System.out.println("����������������������������������������ARREGLO IMPORTES -> " + cadena.split("@").length);

												if(cadena.split("@").length >= 1){

													String arregloImportes[] = cadena.split("@");

													for(int nImporte = 0; nImporte < arregloImportes.length; nImporte++){
														importeTotal = importeTotal + Double.parseDouble(arregloImportes[nImporte]);
													}
												}
									}

									if(personalidad !=null && personalidad.equals("1")){
										 claveOperacion = BitaConstants.CTK_PAGO_OCURRE_FISICAS;
										 concepto = BitaConstants.CTK_CONCEPTO_PAGO_OCURRE_FISICAS;
									}else{
										claveOperacion = BitaConstants.CTK_PAGO_OCURRE_MORALES;
										concepto = BitaConstants.CTK_CONCEPTO_PAGO_OCURRE_MORALES;
									}

									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuentaS--b->"+ cuentaS, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeTotal.doubleValue(), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

									sessionBit.removeAttribute("atributosBitacora");


									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuentaS,importeTotal.doubleValue(),claveOperacion,"0",token,concepto,0);
								} catch(Exception e) {
									EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

								}
							}
					        //***********

							EIGlobal.mensajePorTrace("\n\nEjecutar Transaccion\n\n", EIGlobal.NivelLog.DEBUG);
							Ejecuta_Transaccion_Ocurre(contrato,usuario,clave_perfil,personalidad,req,res);
							
							//***Elimina datos en sesion***//
							req.getSession().removeAttribute("personalidad");

						}
					}
				} // termina else de valida OTP
			} /*else //de sesion valida
			{
					req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
					despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, req, res );
			}*/
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("Excepcion en defaultAction MDI_Orden_Pago_Ocurre ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}



	}
	/**
	 * Metodo para recibir las llamadas get
	 * @param req objeto HttpServletRequest
     * @param res objeto HttpServletResponse
     * @exception ServletException excepcion a manejar
	 * @exception IOException excepcion a manejar
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}
	
	/**
	 * Metodo para recibir las llamadas post
	 * @param req objeto HttpServletRequest
     * @param res objeto HttpServletResponse
     * @exception ServletException excepcion a manejar
	 * @exception IOException excepcion a manejar
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}
	/*-----------------------------------------------------------------*/

	public void Ejecuta_Transaccion_Ocurre(String contrato, String usuario,String clave_perfil ,String personalidad,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
	EIGlobal.mensajePorTrace("***Mdi_Ordenes_Pago_Ocurre.class Entrando a Ejecucion_Operaciones_Ocurre ", EIGlobal.NivelLog.DEBUG);

	EmailSender emailSender=new EmailSender();//MAAM 12/AGO/2010
	EmailDetails emailDetails = new EmailDetails();//MAAM 12/AGO/2010

	int registros = 0;
	int Correctas = 0;
	int Longitud_RS = 0;
    boolean comuError=false;
	String Mensaje_Salida = "";
	String Trama_Entrada = "";
	String Salida_JS = "";
	String ArrCuentas[];
	String CtaCargo = "";
	String Cta_Cargo = "";
	String Tipo_Cta_Cargo= "";
	String CodError ="";
	String CodErrorOper=""; // MAAM 12/AGO/2010
	String CodErrorOperGlobal="";// MAAM 16/AGO/2010
	String numReferencias="";// YHG 16/AGO/2010
	String Mensaje="";
	String mensajeError="";
	String Codigo = "";
	String Referencia = "";
	String Arreglo_Paterno = "";
	String Arreglo_Materno = "";
	String Arreglo_Nombres = "";
	String Arreglo_Razones_Sociales = "";
	String Tipo_Operacion = "OCUR";
	String[] Paternos2 = new String[nmaxoper];
	String[] Maternos2 = new String[nmaxoper];
	String[] Nombres2 = new String[nmaxoper];
	String[] Razones_Sociales2 = new String[nmaxoper];
	String Refe = "";
	String Hora = "";
	//	VSWF DBR 18/10/2006
	String ForAlta="T"; //Modalidad de alta=T(Tradicional)
	String ForPago="E";//Forma de pago E(Efectivo)
	String TipoCom="O";//Tipo de comision O(con cargo al Ordenante)
	java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyy-MM-dd");
	Date hoy = new Date();
	String FecLib = formateador.format(hoy);//Fecha de liberacion (la del dia del registro)
	String FecLiq=FecLib; //Fecha de liquidacion
	String TipBen=""; //Tipo de beneficiario
	//FIN VSWF

	String Arreglo_Cuentas_Cargo = req.getParameter("Arreglo_Cuentas_Cargo");
	String Arreglo_Importes = req.getParameter("Arreglo_Importes");
	String Arreglo_Instrucciones = req.getParameter("Arreglo_Instrucciones");
	if ("1".equals(personalidad)) {
		Arreglo_Paterno = req.getParameter("Arreglo_Paterno");
		Arreglo_Materno = req.getParameter("Arreglo_Materno");
		Arreglo_Nombres = req.getParameter("Arreglo_Nombre");
	}else {
		Arreglo_Razones_Sociales = req.getParameter("Arreglo_Razones_Sociales");
	}
	String Arreglo_RFCs = req.getParameter("Arreglo_RFCs");
	String Arreglo_IVAs = req.getParameter("Arreglo_IVAs");
	String arregloestatus1 = req.getParameter("arregloestatus1");
	/*Desentrama los datos en Arreglos */

	emailDetails.setNumRegImportados(registros);//MAAM 12/AGO/2010
	emailDetails.setNumeroContrato(contrato);//MAAM 12/AGO/2010
	emailDetails.setRazonSocial(session.getNombreContrato());//MAAM 12/AGO/2010


	registros = Integer.parseInt(req.getParameter("registros"));
	String[] Ctas_Cargo  = desentramaC(Integer.toString(registros)+"@"+Arreglo_Cuentas_Cargo,'@');
	String[] Importes = desentramaC(Integer.toString(registros)+"@"+Arreglo_Importes,'@');
	String[] Instrucciones = desentramaC(Integer.toString(registros)+"@"+Arreglo_Instrucciones,'@');
	if ("1".equals(personalidad)) {
		Paternos2 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Paterno,'@');
		Maternos2 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Materno,'@');
		Nombres2 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Nombres,'@');
	}else {
		Razones_Sociales2 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Razones_Sociales,'@');
	}
	String[] RFCs = desentramaC(Integer.toString(registros)+"@"+Arreglo_RFCs,'@');
	String[] IVAs = desentramaC(Integer.toString(registros)+"@"+Arreglo_IVAs,'@');
	String[] Checadas = desentramaC(Integer.toString(registros)+"@"+arregloestatus1,'@');
	/*VSWF-HGG-I*/

	BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
	/*VSWF-HGG-F*/

	double ImporteTotal=0;//MAAM 12/AGO/2010
	double totalAux=0;//MAAM 12/AGO/2010

	inicializando(); /*Inicializa conexi�n con Tuxedo*/
	for(int c=1;c <= registros;c++)   {
		if (Checadas[c].equals("1")) {
			 //ArrCuentas = desentramaC("2|" + Ctas_Cargo[c],'|');
		     //Cta_Cargo = ArrCuentas[1];
		     //Tipo_Cta_Cargo=ArrCuentas[2];
			 if ("2".equals(personalidad)) {
				 Longitud_RS = Razones_Sociales2[c].length();
				 for (int a=Razones_Sociales2[c].length(); a <= 60 ; a++ )
					 Razones_Sociales2[c] = Razones_Sociales2[c] + ' ';

				Paternos2[c] = Razones_Sociales2[c].substring(0,20);
				Maternos2[c] = Razones_Sociales2[c].substring(20,40);
				Nombres2[c] = Razones_Sociales2[c].substring(40,60);
			 }
			//Trama_Entrada="1EWEB|"+usuario+"|"+Tipo_Operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+contrato+"@"+Cta_Cargo+"@"+Tipo_Cta_Cargo+"@"+Importes[c]+"@"+Paternos2[c]+"@"+Maternos2[c]+"@"+Nombres2[c]+"@"+Instrucciones[c] + "@" + RFCs[c] + "@" + IVAs[c] + "@|";
			/*MSD 15/12/2006*/
			if(Instrucciones[c].length() > 20) {
				Instrucciones[c] = Instrucciones[c].substring(0, 20);
			}
			/*MSD 15/12/2006*/

			 Trama_Entrada="1EWEB|"+usuario+"|"+Tipo_Operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+contrato+"@"+Ctas_Cargo[c]+"@P@"+Importes[c]+"@"+Paternos2[c]+"@"+Maternos2[c]+"@"+Nombres2[c]+"@"+Instrucciones[c] + "@" + RFCs[c] + "@" + IVAs[c] + "@| |";
               try{
			 EIGlobal.mensajePorTrace("@@ (Importes[c])------->"+ Importes[c], EIGlobal.NivelLog.DEBUG);//MAAM 12/AGO/2010
			 totalAux=Importes[c].equals("")?0:Double.parseDouble(Importes[c]);//MAAM 12/AGO/2010
			 ImporteTotal=ImporteTotal+totalAux;// SUMA LOS IMPORTES TOTALES DEL PAGO //MAAM 12/AGO/2010
               }catch(Exception e){
            	   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
               }


			 EIGlobal.mensajePorTrace("@@ paso sumatorias", EIGlobal.NivelLog.DEBUG);

			 ServicioTux tuxGlobal = new ServicioTux();
			// GETRONICS CP MEXICO, NAA.  EN ATENCION A LA INCIDENCIA Q1322
			StringBuffer CtasCargo = new StringBuffer("");

		   CtasCargo = (StringBuffer)req.getSession().getAttribute("CtasCargo");
          if(CtasCargo==null || CtasCargo.toString()=="")
            {
             CtasCargo= new StringBuffer("");
	        }
 	      EIGlobal.mensajePorTrace("Verificacion defaultAction"+CtasCargo.toString(), EIGlobal.NivelLog.INFO);


			 //ivn Q05-0001087 inicio lineas agregadas
			 String IP = " ",fechaHr = "";										//variables locales al m�todo
			 IP = req.getRemoteAddr();											//ObtenerIP
			 java.util.Date fechaHrAct = new java.util.Date();
			 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
			 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";				//asignaci�n de fecha y hora
			 //ivn Q05-0001087 fin lineas agregada

			 /*ivn Q05-0001087 se agreg� a la tramala variable fechaHr
			  *EIGlobal.mensajePorTrace("***Mdi_Orden_Pago_Ocurre  WEB_RED-Ocurre>>"+c+":"+Trama_Entrada, EIGlobal.NivelLog.DEBUG); */
		     EIGlobal.mensajePorTrace(fechaHr+"***Mdi_Orden_Pago_Ocurre  WEB_RED-Ocurre>>"+c+":"+Trama_Entrada, EIGlobal.NivelLog.DEBUG);
             EIGlobal.mensajePorTrace("CUENTAS_CARGO - TRAMA DE ENTRADA:"+CtasCargo.toString(), EIGlobal.NivelLog.INFO);
			 EIGlobal.mensajePorTrace("CUENTAS_CARGO - TRAMA:"+Arreglo_Cuentas_Cargo, EIGlobal.NivelLog.INFO);

     String tipo_trans=(String)req.getSession().getAttribute("Operacion");
     EIGlobal.mensajePorTrace("tipo_trans:" +tipo_trans, EIGlobal.NivelLog.INFO);

      if("Linea".equals(tipo_trans))
    {
		EIGlobal.mensajePorTrace("POR LINEA", EIGlobal.NivelLog.DEBUG);

		  if (!Arreglo_Cuentas_Cargo.equals(CtasCargo.toString()) )
			{
				EIGlobal.mensajePorTrace("LA CUENTA ABONO FUE MODIFICADA", EIGlobal.NivelLog.DEBUG);
				comuError=true;
				mensajeError="Transaccion no V�lida";
			}
   }
   else
    {
   			EIGlobal.mensajePorTrace("POR ARCHIVO", EIGlobal.NivelLog.DEBUG);

   }
    if(comuError==true)
	{
      despliegaPaginaError(mensajeError,"Ordenes de Pago Ocurre","",req,res);
      return;
	}
   		     //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		 	 try {
		       	 Hashtable hs = tuxGlobal.web_red(Trama_Entrada);
		       	 ValidaOTP.guardaBitacora((List)req.getSession().getAttribute("bitacora"),Tipo_Operacion);
				 CodError = (String) hs.get("BUFFER");
				 CodErrorOper=hs.get("COD_ERROR")+"";//MAAM 12/AGO/2010
				 CodErrorOperGlobal+=CodErrorOper+"-";
				 EIGlobal.mensajePorTrace("Respuesta Servicio Ocurre : "+ CodError, EIGlobal.NivelLog.DEBUG);

			 } catch(Exception e)	{
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 }
			//TODO BIT CU2041, BIT CU2051; ejecuta la transacci�n de: Personas F�sicas, Personas Morales
				/*
				 * VSWF-HGG-I
				 */
			 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try {
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					if (personalidad.equals("1")){
						bt.setNumBit(BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_TRANSFERENCIA);
					}

					if (personalidad.equals("2")){
						bt.setNumBit(BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_MORALES_TRANSFERENCIA);
					}

					if (contrato != null) {
						bt.setContrato(contrato);
					}
					if (Ctas_Cargo[c] != null) {
						if(Ctas_Cargo[c].length() > 20){
							bt.setCctaOrig(Ctas_Cargo[c].substring(0,20));
						}else{
							bt.setCctaOrig(Ctas_Cargo[c].trim());
						}
					}
					if(personalidad.equals("1")){
						if (Paternos2[c] != null) {
							String nombre = Paternos2[c]+""+Maternos2[c]+""+Nombres2[c];
							if (nombre.length() > 20) {
								bt.setCctaDest(nombre.substring(0, 20).trim());
							} else {
								bt.setCctaDest(nombre.trim());
							}

						}
					}else{
						if (Razones_Sociales2[c] != null) {
							if (Razones_Sociales2[c].length() > 20) {
								bt.setCctaDest(
									Razones_Sociales2[c]
										.substring(0, 20)
										.trim());
							} else {
								bt.setCctaDest(Razones_Sociales2[c].trim());
							}

						}
					}
					if(CodError != null){
						for (int a=CodError.length(); a <= 130 ; a++ )
							CodError = CodError + ' ';
						Refe = CodError.substring(0,9);
					}
					bt.setTipoMoneda("MN");
					bt.setBancoDest("SANTANDER");
					if (Importes[c] != null) {
						bt.setImporte(Double.parseDouble(Importes[c].trim()));
					}
					if(CodError!=null){
						if(CodError.substring(0,2).equals("OK")){
							bt.setIdErr("DIBT0000");
						}else if(CodError.length()>8){
							bt.setIdErr(CodError.substring(0,8));
						}else{
							bt.setIdErr(CodError.trim());
						}
					}else{
						bt.setIdErr(" ");
					}
					if (usuario != null) {
						bt.setUsr(usuario.trim());
					}
					if (Tipo_Operacion != null){
						bt.setServTransTux(Tipo_Operacion.trim());
					}
					try {
						bt.setReferencia(Long.parseLong(Refe));
					} catch(NumberFormatException e) {
						bt.setReferencia(0);
						EIGlobal.mensajePorTrace("Error en parseo de referencia: <" + Refe + ">", EIGlobal.NivelLog.ERROR);
					}
					if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
						&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
								.equals(BitaConstants.VALIDA)) {
						bt.setIdToken(session.getToken().getSerialNumber());
						req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
					}
					/*VSWF I Autor=BMB fecha=07-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario*/
					String beneficiario="";
					if(Paternos2[c]!=null && Paternos2[c]!=""){
						beneficiario=Paternos2[c];
					}
					if(Maternos2[c]!=null && Maternos2[c]!=""){
						beneficiario+=" "+Maternos2[c];
					}
					if(Nombres2[c]!=null && Nombres2[c]!=""){
						beneficiario+=" "+Nombres2[c];
					}
					bt.setBeneficiario((beneficiario==null)?" ":beneficiario.trim());
					/*VSWF F*/
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			 }
				/*
				 * VSWF-HGG-F
				 */
				 if (CodError == null)  {
					despliegaPaginaError("Proceso fuera se servicio. ","Ordenes de Pago Ocurre","",req,res);
					return;
				}
				if (CodError.substring(0,5).equals("ERROR"))
				{
					despliegaPaginaError(CodError,"Ordenes de Pago Ocurre","",req,res);
					return;
				}
				if (CodError.substring(0,2).equals("OK"))  {
					Refe = CodError.substring(8,16);
					Codigo = "00000";
					Referencia = "0000000000000";
					Mensaje = "OPERACION EJECUTADA POR MANCOMUNIDAD";
				} else {
					if (CodError.substring(0,4).equals("SEGU"))  {
						for (int a=CodError.length(); a <= 130 ; a++ )
							 CodError = CodError + ' ';
						Refe = CodError.substring(8,16);
						Codigo = "00000";
						Referencia = "0000000000000";
						Mensaje = CodError.substring(16,129);
						} else {
							for (int a=CodError.length(); a <= 130 ; a++ )
								CodError = CodError + ' ';
							Refe = CodError.substring(0,9);
							Codigo = CodError.substring(13,17);
							Referencia = CodError.substring(17,30);
							Mensaje = CodError.substring(30,129);
							if (Codigo.equals("0000"))
							Correctas++;
						}
				}
				/** Lllamado al metodo para envio a Monitor Plus */
                String [] parametros = new String[]{
                    CodError, // BUFFER 0
                    personalidad, // personalidad 1                    
                    String.valueOf(ImporteTotal), // Importe total 2
                    Ctas_Cargo[c], // Cuentas cargo 3
                    IEnlace.SUCURSAL_OPERANTE, // Sucursal Operante 4
                    Refe, // Numero de Trx o PT ID 5
                    EnlaceMonitorPlusConstants.COD_MON_NAC, // Moneda 6
                    "Linea".equals(tipo_trans) ? EnlaceMonitorPlusConstants.INDIV: EnlaceMonitorPlusConstants.MASIVA, // Tipo transaccion 7
                    Referencia, // Referencia 8
                    String.valueOf(totalAux),//importe individual por operacion 9
                    (Nombres2[c] + " " + Paternos2[c] + " " + Maternos2[c]), // Nomnre cliente titular cuenta destino 10
                    
                };
                PagOcurreMonitorPlusUtils.enviarPagOcurreMP(req, parametros, session);
                
				Mensaje_Salida=Mensaje_Salida+"<TR>";
				Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Ctas_Cargo[c] + "</TD>";
				Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=right width=100>" + FormatoMoneda(Importes[c]) + "</TD>";
				if (personalidad.equals("1"))
					Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Paternos2[c] + " " + Maternos2[c] + " " + Nombres2[c] + "</TD>";
				else
					Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Razones_Sociales2[c] + "</TD>";
				if (Instrucciones[c].trim().equals(""))
					Instrucciones[c] = ".";

				Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=230>" + Instrucciones[c] + "</TD>";
				Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=150>" + Mensaje + "</TD>";
				Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=100>" + Referencia + "</TD>";
				Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=100>" + Refe + "</TD>";
				Mensaje_Salida=Mensaje_Salida+"</TR>";
				numReferencias+=Refe+", ";
				/*Genera Codigo JS para Grabar Archivos */
				Hora = ObtenHora().substring(0, 3);
				if (personalidad.equals("1")) {
					if (c == 1)
						Salida_JS = Salida_JS + "SaveFrame.document.write(\'" + Ctas_Cargo[c] + ",," + Importes[c] + ",090," + "\"" + Instrucciones[c] + "\",\"" + Nombres2[c] + "\",\"" + Paternos2[c] + "\",\"" + Maternos2[c] + "\",,0," + Codigo + ",\"" + ObtenFecha(true) + "\",\"" + Hora + "\"," + Refe + "," + Referencia +"\')\n";
					else
						Salida_JS = Salida_JS + "SaveFrame.document.write(\'<BR>" + Ctas_Cargo[c] + ",," + Importes[c] + ",090," + "\"" + Instrucciones[c] + "\",\"" + Nombres2[c] + "\",\"" + Paternos2[c] + "\",\"" + Maternos2[c] + "\",,0," + Codigo + ",\"" + ObtenFecha(true) + "\",\"" + Hora + "\"," + Refe + "," + Referencia +"\')\n";
				} else {
					Razones_Sociales2[c] = Razones_Sociales2[c].substring(0,Longitud_RS);
					if (c == 1)
						Salida_JS = Salida_JS + "SaveFrame.document.write(\'" + Ctas_Cargo[c] + ",," + Importes[c] + ",090," + "\"" + Instrucciones[c] + "\",\"" + Razones_Sociales2[c] + "\",,0," + Codigo + ",\"" + ObtenFecha(true) + "\",\"" + Hora + "\"," + Refe + "," + Referencia +"\')\n";
					else
						Salida_JS = Salida_JS + "SaveFrame.document.write(\'<BR>" + Ctas_Cargo[c] + ",," + Importes[c] + ",090," + "\"" + Instrucciones[c] + "\",\"" + Razones_Sociales2[c] + "\",,0," + Codigo + ",\"" + ObtenFecha(true) + "\",\"" + Hora + "\"," + Refe + "," + Referencia +"\')\n";
				}
		}
	}
		try {
			if(emailSender.enviaNotificacion(CodErrorOperGlobal)){//MAAM 12/AGO/2010
				emailDetails.setNumRegImportados(registros);//MAAM 12/AGO/2010
				emailDetails.setImpTotal(Double.toString(ImporteTotal));//MAAM 12/AGO/2010
				emailDetails.setEstatusActual(CodErrorOperGlobal);
				if(numReferencias!=null)
					numReferencias=numReferencias.substring(0,numReferencias.length()-2);//elimina la ultima coma
				emailDetails.setNumRef(numReferencias);
				EIGlobal.mensajePorTrace("<><><><><><><><> enviando notificacion<><><><><>", EIGlobal.NivelLog.DEBUG);//MAAM 12/AGO/2010
				emailSender.sendNotificacion(req,IEnlace.PAGO_OCURRE,emailDetails);//MAAM 12/AGO/2010
			}
		} catch (Exception error) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(error), EIGlobal.NivelLog.INFO);//MAAM 12/AGO/2010
		}
	Salida_JS = Salida_JS.replace(' ','_'); /*Necesario por un bug del explorador al salvar archivos*/
	req.setAttribute("Mensaje_Salida",Mensaje_Salida);
	req.setAttribute("MenuPrincipal", session.getStrMenu());
	req.setAttribute("newMenu", session.getFuncionesDeMenu());
	req.setAttribute("personalidad",personalidad);
	req.setAttribute("Correctas",""+Correctas);
	req.setAttribute("Salida_JS",Salida_JS);


	if (personalidad.equals("1"))
		req.setAttribute("Encabezado", CreaEncabezado("Ordenes de Pago Ocurre","Ordenes de Pago Ocurre &gt; Personas F&iacute;sicas &gt; Resultado de la operaci&oacute;n","s30000h", req));
	else
		req.setAttribute("Encabezado", CreaEncabezado("Ordenes de Pago Ocurre","Ordenes de Pago Ocurre &gt; Personas Morales &gt; Resultado de la operaci&oacute;n","s30010h", req));

		evalTemplate("/jsp/Ocurre_Resultados.jsp", req, res);
	}
	public void Genera_Pantalla_Ocurre(String contrato, String usuario, String clave_perfil,String personalidad, String opcion, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
		EIGlobal.mensajePorTrace("***MDI_Orden_Pago_Ocurre.class Entrando a Genera_Pantalla_Ocurre ", EIGlobal.NivelLog.DEBUG);
		String strSalida	= "";
		int registros = 0;
		String fecha_hoy	= "";
		String Mensaje_Salida = "";
		String Paterno = "";
		String Materno = "";
		String Nombre = "";

         session.setModuloConsultar(IEnlace.MCargo_transf_pesos);

		fecha_hoy = ObtenFecha();


			req.setAttribute("cuentas_origen", strSalida);
			req.setAttribute("fecha_hoy",fecha_hoy);
			req.setAttribute("NMAXOPER"," "+nmaxoper);
			req.setAttribute("divisa","Pesos");
			req.setAttribute("personalidad",personalidad);
			req.setAttribute("opcion",opcion);
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("registros",""+registros);
			req.setAttribute("Mensaje_Salida",Mensaje_Salida);
			/*VSWF-HGG-I*/
			BitaHelper bh = new BitaHelperImpl(req, session, req.getSession ( false ));
			String persona = "";
			/*VSWF-HGG-F*/
			if (personalidad.equals("1"))  {
				req.setAttribute("Encabezado", CreaEncabezado("Ordenes de Pago Ocurre","Ordenes de Pago Ocurre &gt; Personas F&iacute;sicas","s30000h", req));
Paterno = "Apellido Paterno: <INPUT maxLength=20 name=Paterno size=20 tabindex=2><p>Apellido Materno:<INPUT maxLength=20 name=Materno size=20 tabindex=3></p><p>Nombre:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT maxLength=20 name=Nombre size=20 tabindex=4></p>";
//				Paterno = "<TD class=tabmovtex><BR>Apellido Paterno:</TD><TD class=tabmovtex><INPUT maxLength=20 size=20 name=Paterno tabindex=3></TD>";
//				Materno = "<TD class=tabmovtex><BR>Apellido Materno</TD><TD class=tabmovtex><INPUT maxLength=20 size=20 name=Materno tabindex=4></TD>";
//				Nombre = "<TD class=tabmovtex>Nombre<BR></TD><TD class=tabmovtex><INPUT maxLength=20 size=20 name=Nombre tabindex=5></TD>";
			}
			if (personalidad.equals("2")) {
				req.setAttribute("Encabezado", CreaEncabezado("Ordenes de Pago Ocurre","Ordenes de Pago Ocurre &gt; Personas Morales","s30010h", req));
				Paterno = "Raz�n Social:<p><textarea rows=3 name=Razon_Social cols=20 tabindex=2></textarea></p>";

//				Materno = "<TD class=tabmovtex><BR></TD><TD class=tabmovtex></TD>";
//				Nombre =  "<TD class=tabmovtex><BR></TD><TD class=tabmovtex></TD>";
			}

			if ("0".equals(opcion)) {
				req.setAttribute("Arreglo_Cuentas_Cargo","");
				req.setAttribute("Arreglo_Importes","");
				req.setAttribute("Arreglo_Instrucciones","");
				req.setAttribute("Arreglo_RFCs","");
				req.setAttribute("Arreglo_IVAs","");
				req.setAttribute("arregloestatus1","");
				req.setAttribute("Arreglo_Paterno","");
				req.setAttribute("Arreglo_Materno","");
				req.setAttribute("Arreglo_Nombre","");
				req.setAttribute("Arreglo_Razones_Sociales","");
				//TODO BIT CU2041,BIT CU2051; inicio del flujo de: Personas F�sicas, Personas Morales
				/*
				 * VSWF-HGG-I
				 */
				if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
				try {
					bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));

					if ("1".equals(personalidad))  {
						 persona = BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_ENTRA;
					}

					if ("2".equals(personalidad)) {
						 persona = BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_MORALES_ENTRA;
					}

					BitaTransacBean bt =  new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(persona);
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}

					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				}
				/*
				 * VSWF-HGG-F
				 */
			}
			if ("1".equals(opcion))     /*Genera tabla de registros ya grabados y anexa nuevos datos agregados*/
		     {
				 Genera_Transacciones_Agregadas(personalidad,req,res);
			     req.getSession().setAttribute("Operacion","Linea");


             }
			if ("2".equals(opcion))   	/*Lee Disco y extrae Mensaje_Salida y registros*/
		     {

				 req.getSession().setAttribute("Operacion","Archivo");
				 Lee_Archivo(personalidad,req,res);
             }
			req.setAttribute("Paterno",Paterno);
			req.setAttribute("Materno",Materno);
			req.setAttribute("Nombre",Nombre);
			evalTemplate("/jsp/Orden_Pago_Ocurre.jsp", req, res);
}
/**
 * Agrega a la JSP las transacciones agregadas
 *
 */
public void Genera_Transacciones_Agregadas(String personalidad,HttpServletRequest req, HttpServletResponse res) {
	EIGlobal.mensajePorTrace("***MDI_Orden_Pago_Ocurre.class Entrando a Genera_Transacciones_Agregadas ", EIGlobal.NivelLog.DEBUG);
	int registros = 0;
	String Mensaje_Salida = "";
	String nombre_campo = "";
	String Arreglo_Paterno = "";
	String Arreglo_Materno = "";
	String Arreglo_Nombre= "";
	String Arreglo_Razones_Sociales= "";
	String Arreglo_Cuentas_Cargo = "";
	String Arreglo_Importes = "";
	String Arreglo_Instrucciones = "";
	String Arreglo_RFCs = "";
	String Arreglo_IVAs = "";
	String arregloestatus1 = "";
	String[] Paternos1 = new String[nmaxoper];
	String[] Maternos1 = new String[nmaxoper];
	String[] Nombres1 = new String[nmaxoper];
	String[] Razones_Sociales1 = new String[nmaxoper];
	String Instr = "";

	registros = Integer.parseInt(req.getParameter("registros"));
	Instr = req.getParameter("Instrucciones");
	/*MSD 15/12/2006*/
	if(Instr.length() > 20) {
		Instr = Instr.substring(0, 20);
	}
	/*MSD 15/12/2006*/
	EIGlobal.mensajePorTrace("Antes:  " + Instr, EIGlobal.NivelLog.INFO);
   	 if (Instr.length() >= 41)
		     Instr = Instr.substring(0,40);
	 	EIGlobal.mensajePorTrace("Despues:  " + Instr, EIGlobal.NivelLog.INFO);

	if (registros == 0)
	{
		Arreglo_Cuentas_Cargo = req.getParameter("Cuenta_Cargo") + "@";
		Arreglo_Importes =  req.getParameter("Importe") + "@";
		Arreglo_Instrucciones = Instr + "@";
		if (personalidad.equals("1")) {
			Arreglo_Paterno = req.getParameter("Paterno") + "@";
			Arreglo_Materno = req.getParameter("Materno") + "@";
			Arreglo_Nombre =  req.getParameter("Nombre") + "@";
		}else {
			Arreglo_Razones_Sociales = req.getParameter("Razon_Social") + "@";
		}
		Arreglo_RFCs = req.getParameter("RFC") + "@";
		Arreglo_IVAs = req.getParameter("IVA") + "@";
		arregloestatus1 = "1@";
	}else{
		Arreglo_Cuentas_Cargo = (String) req.getParameter("Arreglo_Cuentas_Cargo")  +  (String)	req.getParameter("Cuenta_Cargo") + "@";
        EIGlobal.mensajePorTrace("1.1) ARREGLO DE CUENTAS_CARGO"+Arreglo_Cuentas_Cargo, EIGlobal.NivelLog.INFO);// GETRONICS CP MEXICO, NAA.  EN ATENCION A LA INCIDENCIA Q1322
		Arreglo_Importes = (String) req.getParameter("Arreglo_Importes") +  req.getParameter("Importe") + "@";
		Arreglo_Instrucciones = (String) req.getParameter("Arreglo_Instrucciones") + Instr + "@";
		if (personalidad.equals("1")) {
			Arreglo_Paterno = (String) req.getParameter("Arreglo_Paterno") + req.getParameter("Paterno") + "@";
			Arreglo_Materno = (String) req.getParameter("Arreglo_Materno") + req.getParameter("Materno") + "@";
			Arreglo_Nombre = (String) req.getParameter("Arreglo_Nombre")  + req.getParameter("Nombre") + "@";
		}else {
			Arreglo_Razones_Sociales = (String) req.getParameter("Arreglo_Razones_Sociales") + req.getParameter("Razon_Social")+ "@";
		}
		Arreglo_RFCs = (String) req.getParameter("Arreglo_RFCs") +  req.getParameter("RFC") + "@";
		Arreglo_IVAs = (String) req.getParameter("Arreglo_IVAs") +  req.getParameter("IVA") + "@";
		arregloestatus1 = (String) req.getParameter("arregloestatus1") + "1@";
	}

	/*Desentrama los datos en Arreglos */

	registros++;
	String[] Importes = desentramaC(Integer.toString(registros)+"@"+Arreglo_Importes,'@');
	String[] Ctas_Cargo  = desentramaC(Integer.toString(registros)+"@"+Arreglo_Cuentas_Cargo,'@');
	String[] Instrucciones = desentramaC(Integer.toString(registros)+"@"+Arreglo_Instrucciones,'@');
	if (personalidad.equals("1")) {
		Paternos1 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Paterno,'@');
		Maternos1 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Materno,'@');
		Nombres1 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Nombre,'@');
	}else {
		Razones_Sociales1 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Razones_Sociales,'@');
	}
	String[] RFCs = desentramaC(Integer.toString(registros)+"@"+Arreglo_RFCs,'@');
	String[] IVAs = desentramaC(Integer.toString(registros)+"@"+Arreglo_IVAs,'@');
	String[] Checadas = desentramaC(Integer.toString(registros)+"@"+arregloestatus1,'@');

	Mensaje_Salida="<table width=760 border=0 cellspacing=2 cellpadding=3>"; /*Genera HTML de la tabla de registros*/
	Mensaje_Salida+="<tr><td class=textabref colspan=8>Total de Operaciones: " + registros + "</td></tr>";
	Mensaje_Salida+="<TR>";
	Mensaje_Salida+="<TD align=left class=tittabdat width=20>Opc</TD>";
	Mensaje_Salida+="<TD align=left class=tittabdat width=200>Cuenta cargo</TD>";
	Mensaje_Salida+="<TD class=tittabdat align=center width=85 >Importe</TD>";
	Mensaje_Salida+="<TD class=tittabdat align=center width=200>Beneficiario</TD>";
	Mensaje_Salida+="<TD class=tittabdat align=center width=230>Instrucciones</TD>";
	Mensaje_Salida=Mensaje_Salida+"</TR>";

	for (int c=1;c <= registros;c++){
		nombre_campo = "Caja" + Integer.toString(registros);
		Mensaje_Salida=Mensaje_Salida+"<TR>";

		if (Checadas[c].equals("1"))
			Mensaje_Salida=Mensaje_Salida+"<TD><INPUT TYPE=CHECKBOX NAME="+nombre_campo +" checked></TD>";
		else
			Mensaje_Salida=Mensaje_Salida+"<TD><INPUT TYPE=CHECKBOX NAME="+ nombre_campo +" ></TD>";
		Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Ctas_Cargo[c] + "</TD>";
		Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=right width=100>" + FormatoMoneda(Importes[c]) + "</TD>";
		if (personalidad.equals("1"))
			Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Paternos1[c] + " " + Maternos1[c] + " " + Nombres1[c] + "</TD>";
		else
			Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Razones_Sociales1[c] + "</TD>";
		if (Instrucciones[c].trim().equals(""))
			Instrucciones[c] = ".";	/*Bug de Netscape*/

		Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=230>" + Instrucciones[c] + "</TD>";
		Mensaje_Salida=Mensaje_Salida+"</TR>";
	}
	Mensaje_Salida=Mensaje_Salida+"</Table>";
	Mensaje_Salida=Mensaje_Salida+"<p>&nbsp;</p><p align=center>";

	BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");


	if( session.getToken().getStatus() == 1 ){
		Mensaje_Salida=Mensaje_Salida+"<a href=javascript:respuesta=1;continua(); border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 tabindex=16></a>";
	} else {
		Mensaje_Salida=Mensaje_Salida+"<a href=javascript:confirmacion(); border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 tabindex=16></a>";
	}


	Mensaje_Salida=Mensaje_Salida+"<a href=javascript:scrImpresion(); border=0><img src=/gifs/EnlaceMig/gbo25240.gif border=0 tabindex=17></a>";
	Mensaje_Salida=Mensaje_Salida+"</P>";

	req.setAttribute("registros",""+registros);
	req.setAttribute("Mensaje_Salida",Mensaje_Salida);
	req.setAttribute("Arreglo_Cuentas_Cargo",Arreglo_Cuentas_Cargo);

    EIGlobal.mensajePorTrace("1.1) ARREGLO CUENTAS_CARGO "+Arreglo_Cuentas_Cargo, EIGlobal.NivelLog.INFO);// GETRONICS CP MEXICO, NAA.  EN ATENCION A LA INCIDENCIA Q1322
	EIGlobal.mensajePorTrace("REGISTROS  "+registros, EIGlobal.NivelLog.INFO);
	req.setAttribute("Arreglo_Importes",Arreglo_Importes);

	// GETRONICS CP MEXICO, NAA.  EN ATENCION A LA INCIDENCIA Q1322
    StringBuffer CtasCargo = new StringBuffer("");

	for (int c=1; c <= registros; c++)
	{
	  EIGlobal.mensajePorTrace("ctas_cargo["+registros+"] = "+Ctas_Cargo[c], EIGlobal.NivelLog.INFO);
      CtasCargo.append(Ctas_Cargo[c]); //Importes.toString();
	  CtasCargo.append("@");
	}

    req.getSession().setAttribute ("CtasCargo",CtasCargo);
	EIGlobal.mensajePorTrace("Verificacion con el programa ya arreglado"+CtasCargo.toString(), EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("===CtasCargo"+CtasCargo.toString(), EIGlobal.NivelLog.INFO);


   //NAA.

	req.setAttribute("Arreglo_Instrucciones",Arreglo_Instrucciones);
	req.setAttribute("Arreglo_Paterno",Arreglo_Paterno);
	req.setAttribute("Arreglo_Materno",Arreglo_Materno);
	req.setAttribute("Arreglo_Nombre",Arreglo_Nombre);
	req.setAttribute("Arreglo_Razones_Sociales",Arreglo_Razones_Sociales);
	req.setAttribute("Arreglo_RFCs",Arreglo_RFCs);
	req.setAttribute("Arreglo_IVAs",Arreglo_IVAs);
	req.setAttribute("arregloestatus1",arregloestatus1);
}
/**
 * Metodo que en base a un registro leido de disco, lo desentraa y acomoda en un Arreglo
 *
 */
public String[] LLenar_Variables(int n,String linea)
{
	String[] arreglo = new String[n];
	int contador = 0;
	String variable = "";
	int bandera = 0;

	//EIGlobal.mensajePorTrace("linea = " + linea, EIGlobal.NivelLog.INFO);
	for (int b=1; b<linea.length(); b++) {
		char letra;
		letra = linea.charAt(b - 1);
		if (letra == 34) {
			if (bandera == 0){
				bandera = 1;
			} else {
				arreglo[contador] = variable;
				variable = "";
				contador += 1;
				bandera = 0;
				b += 1;
			}
		} else {
			if (letra == 44)
			{
				if (bandera == 0) {
					arreglo[contador] =  variable;
					variable = "";
					contador += 1;
				} else	{
					variable = variable + linea.substring(b-1,b);
			    }
		   } else {
				variable = variable + linea.substring(b-1,b);
     	   }
	  }
  }
return arreglo;
}

/**
 * Metodo que lee un archivo y graba en el objeto req el mensaje de salida y el numero de registros leidos
 * De lo contrario genera el mensaje de Error
 */
public void Lee_Archivo(String personalidad,HttpServletRequest req, HttpServletResponse res) {
    BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
	EIGlobal.mensajePorTrace("***MDI_Orden_Pago_Ocurre.class Entrando a Lee_Archivo ", EIGlobal.NivelLog.DEBUG);
	//int registros = Integer.parseInt(req.getParameter("registros"));
	int registros = Integer.parseInt(getFormParameter(req,"registros"));
	int transerr = 0;
	int campo = 0;
	String Mensaje_Salida = "";
	String nombre_campo = "";
	String  strArchivo = "";
	String linea	= "";
	boolean bandera_comillas = false;
	boolean archivo_sin_error = true;
	String Cadena = "";
    String Arreglo_Cuentas_Cargo = "";
	String Arreglo_Importes = "";
   	String Arreglo_Instrucciones = "";
	String Arreglo_RFCs = "";
	String Arreglo_IVAs = "";
	String arregloestatus1 = "";
	String Arreglo_Paterno = "";
	String Arreglo_Materno= "";
	String Arreglo_Nombre= "";
	String Arreglo_Razones_Sociales = "";
	String[] Ctas_Cargo  = new String[nmaxoper];
	String[] Importes = new String[nmaxoper];
	String[] Instrucciones = new String[nmaxoper];
	String[] RFCs = new String[nmaxoper];
	String[] IVAs = new String[nmaxoper];
	String[] Checadas = new String[nmaxoper];
	String[] Paternos3 = new String[nmaxoper];
	String[] Maternos3 = new String[nmaxoper];
	String[] Nombres3 = new String[nmaxoper];
	String[] Razones_Sociales3 = new String[nmaxoper];
	String[] Campos_Ocurre = new String[16];
	int Lineas_Leidas = 0;
	String persona = "";
	try	{
		strArchivo 	 = new String(getFileInBytes( req ));
		EIGlobal.mensajePorTrace("Archivo Leido", EIGlobal.NivelLog.DEBUG);
	}catch(Exception e)	{
		strArchivo = "";
	}
	try {
		BufferedReader strOut = new BufferedReader(new StringReader(strArchivo));
		registros = Integer.parseInt(getFormParameter(req,"registros"));
		if (registros > 0)	{
			Arreglo_Cuentas_Cargo = getFormParameter(req,"Arreglo_Cuentas_Cargo");
			Arreglo_Importes = getFormParameter(req,"Arreglo_Importes");
	    	Arreglo_Instrucciones = getFormParameter(req,"Arreglo_Instrucciones");
			if (personalidad.equals("1")) {
				Arreglo_Paterno = getFormParameter(req,"Arreglo_Paterno");
				Arreglo_Materno = getFormParameter(req,"Arreglo_Materno");
				Arreglo_Nombre = getFormParameter(req,"Arreglo_Nombre");
			} else {
				Arreglo_Razones_Sociales = getFormParameter(req,"Arreglo_Razones_Sociales");
			}
				Arreglo_RFCs = getFormParameter(req,"Arreglo_RFCs");
				Arreglo_IVAs = getFormParameter(req,"Arreglo_IVAs");
     			arregloestatus1 = getFormParameter(req,"arregloestatus1");
		}
		//llamado_servicioCtasInteg(IEnlace.MCargo_transf_pesos," "," ","",session.getUserID8()+".amboc", req); /* Genera Archivo con Ctas del Contrato*/
		//TODO BIT CU2041,BIT CU2051; importa archivo: Personas F�sicas, Personas Morales
		/*
		 * VSWF-HGG-I
		 */
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try {
			BitaHelper bh = new BitaHelperImpl(req, session, req.getSession ( false ));
			if (personalidad.equals("1"))  {
				 persona = BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_IMPORTA_ARCHIVO;
			}

			if (personalidad.equals("2")) {
				 persona = BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_MORALES_IMPORTA_ARCHIVO;
			}

			BitaTransacBean bt =  new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(persona);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber());
			}
			if (req.getSession().getAttribute("ArchImporta") != null) {
				bt.setNombreArchivo(
					(String) req.getSession().getAttribute("ArchImporta"));
				//Archivo de Importacion
			}
			bt.setBancoDest("SANTANDER");
			bt.setTipoMoneda("MN");

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		}

		/*
		 * VSWF-HGG-F
		 */
		ValidaCuentaContrato valCtas = null;
		try{
		valCtas = new ValidaCuentaContrato();
		while((linea = strOut.readLine()) != null){
			if(linea.trim().equals(""))
				linea = strOut.readLine();
				if(linea == null)
					break;

			if (registros < nmaxoper) {
				Campos_Ocurre = LLenar_Variables(16,linea);
				Lineas_Leidas++;
				Cadena = Valida_Campos_Leidos(Campos_Ocurre,personalidad,Lineas_Leidas, req, valCtas);
					if (!Cadena.equals("")) {
						transerr++;
						archivo_sin_error=false;
						req.setAttribute("ArchivoErr",Cadena);
					}else{
						//Campos_Ocurre[0] += "|P|";
						//EIGlobal.mensajePorTrace("Campos_Ocurre [0] "+ Campos_Ocurre[0], EIGlobal.NivelLog.INFO);
						Arreglo_Cuentas_Cargo += Campos_Ocurre[0] + "@";
						Arreglo_Importes += Campos_Ocurre[2] + "@";
				    	Arreglo_Instrucciones += Campos_Ocurre[4] + "@";
						if (personalidad.equals("1")) {
							Arreglo_Paterno += Campos_Ocurre[7] + "@";  //Se invertieron. Inc 516193
							Arreglo_Materno += Campos_Ocurre[8] + "@";  //Se invertieron. Inc 516193
							Arreglo_Nombre +=  Campos_Ocurre[6] + "@";  //Se invertieron. Inc 516193
						}else {
							Arreglo_Razones_Sociales += Campos_Ocurre[6] + "@";
						}
						Arreglo_RFCs += " @";
						Arreglo_IVAs += "0@";
     					arregloestatus1 += "1@";
						registros++;
					}
				}else{
					transerr++;
					archivo_sin_error=false;
					req.setAttribute("ArchivoErr","cuadroDialogo('Solo se pueden operar hasta 30 registros', 3);");
					break;
				}
			}// Fin del While
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("MDI_Orden_Pago_Ocurre::Lee_Archivo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
			}  //fin de try-catch
	    finally
		{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
			}
		}
					if (registros > 0)	{
						Ctas_Cargo  = desentramaC(Integer.toString(registros)+"@"+Arreglo_Cuentas_Cargo,'@');
						Importes = desentramaC(Integer.toString(registros)+"@"+Arreglo_Importes,'@');
						Instrucciones = desentramaC(Integer.toString(registros)+"@"+Arreglo_Instrucciones,'@');
						if (personalidad.equals("1")) {
							Paternos3 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Paterno,'@');
							Maternos3 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Materno,'@');
							Nombres3 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Nombre,'@');
						}else {
							Razones_Sociales3 = desentramaC(Integer.toString(registros)+"@"+Arreglo_Razones_Sociales,'@');
						}
						RFCs = desentramaC(Integer.toString(registros)+"@"+Arreglo_RFCs,'@');
						IVAs = desentramaC(Integer.toString(registros)+"@"+Arreglo_IVAs,'@');
						Checadas = desentramaC(Integer.toString(registros)+"@"+arregloestatus1,'@');
						Mensaje_Salida="<table width=760 border=0 cellspacing=2 cellpadding=3>"; /*Genera HTML de la tabla de registros*/
						Mensaje_Salida+="<tr><td class=textabref colspan=8>Total de Operaciones: " + registros + "</td></tr>";
						Mensaje_Salida+="<TR>";
						Mensaje_Salida+="<TD align=left class=tittabdat width=20>Opc</TD>";
						Mensaje_Salida+="<TD align=left class=tittabdat width=200>Cuenta cargo</TD>";
						Mensaje_Salida+="<TD class=tittabdat align=center width=85 >Importe</TD>";
						Mensaje_Salida+="<TD class=tittabdat align=center width=200>Beneficiario</TD>";
						Mensaje_Salida+="<TD class=tittabdat align=center width=230>Instrucciones</TD>";
						Mensaje_Salida=Mensaje_Salida+"</TR>";

						for (int b=1;b <= registros;b++){
							nombre_campo = "Caja" + Integer.toString(registros);
							Mensaje_Salida=Mensaje_Salida+"<TR>";
							if (Checadas[b].equals("1"))
								Mensaje_Salida=Mensaje_Salida+"<TD><INPUT TYPE=CHECKBOX NAME="+nombre_campo +" checked></TD>";
							else
								Mensaje_Salida=Mensaje_Salida+"<TD><INPUT TYPE=CHECKBOX NAME="+nombre_campo +"></TD>";
							Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Ctas_Cargo[b] + "</TD>";
							Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=right width=100>" + FormatoMoneda(Importes[b]) + "</TD>";
							if (personalidad.equals("1"))
								Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Paternos3[b] + " " + Maternos3[b] + " " + Nombres3[b] + "</TD>";
							else
								Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=200>" + Razones_Sociales3[b] + "</TD>";
							Mensaje_Salida=Mensaje_Salida+"<TD class=textabdatobs align=left width=230>" + Instrucciones[b] + "</TD>";
							Mensaje_Salida=Mensaje_Salida+"</TR>";
						}
						Mensaje_Salida=Mensaje_Salida+"</Table>";
						Mensaje_Salida=Mensaje_Salida+"<p>&nbsp;</p><p align=center>";



						if( session.getToken().getStatus() == 1 ){
							Mensaje_Salida=Mensaje_Salida+"<a href=javascript:respuesta=1;continua(); border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 tabindex=16></a>";
						} else {
							Mensaje_Salida=Mensaje_Salida+"<a href=javascript:confirmacion(); border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 tabindex=16></a>";

						}

						Mensaje_Salida=Mensaje_Salida+"<a href=javascript:scrImpresion(); border=0><img src=/gifs/EnlaceMig/gbo25240.gif border=0></a>";
						Mensaje_Salida=Mensaje_Salida+"</P>";
					}
						req.setAttribute("registros",""+registros);
						req.setAttribute("Mensaje_Salida",Mensaje_Salida);
						req.setAttribute("Arreglo_Cuentas_Cargo",Arreglo_Cuentas_Cargo);
						req.setAttribute("Arreglo_Importes",Arreglo_Importes);
						req.setAttribute("Arreglo_Instrucciones",Arreglo_Instrucciones);
						if (personalidad.equals("1")) {
							req.setAttribute("Arreglo_Paterno",Arreglo_Paterno);
							req.setAttribute("Arreglo_Materno",Arreglo_Materno);
							req.setAttribute("Arreglo_Nombre",Arreglo_Nombre);
					   }else {
							req.setAttribute("Arreglo_Razones_Sociales",Arreglo_Razones_Sociales);
					   }
						req.setAttribute("Arreglo_RFCs",Arreglo_RFCs);
						req.setAttribute("Arreglo_IVAs",Arreglo_IVAs);
						req.setAttribute("arregloestatus1",arregloestatus1);
	} catch(Exception e){
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	   req.setAttribute("ArchivoErr","cuadroDialogo('Archivo de Importaci&oacute;n inv&aacute;lido', 3);");
	   archivo_sin_error=false;
	}
}

/**
 *
 * @param Cuenta
 * @param request
 * @param valCtas  Objeto para el llamado al servicio VALCTA_TR, para validar una cuenta.
 * @return
 */
public boolean Existe_Cuenta(String Cuenta, HttpServletRequest request, ValidaCuentaContrato valCtas)
{
    BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
    //String[] datoscta=BuscandoCtaInteg(Cuenta.trim(),IEnlace.LOCAL_TMP_DIR +"/"+session.getUserID8()+".amboc",IEnlace.MCargo_transf_pesos);
	String[] datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
			  session.getUserID8(),
	          session.getUserProfile(),
	          Cuenta.trim(),
	          IEnlace.MCargo_transf_pesos);
    if(datoscta!=null)
		return true;
	else
		return false;

//	String[][] arrayCuentas;=ContCtasRelacpara_mis_transferencias(contrato); /*Obtiene cuentas de Cargo*/

/*	for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++){
			tipoCuenta		= arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
			claveProducto	= arrayCuentas[indice][3];
			if (arrayCuentas[indice][1].equals(Cuenta))	{
				if (arrayCuentas[indice][2].equals("P") && !claveProducto.equals("6") &&  /* Checa q la cuenta sea en pesos
				 !claveProducto.equals("5") && !claveProducto.equals("4") &&
				 !claveProducto.equals("0") && !tipoCuenta.equals("BM")   &&
				 !tipoCuenta.equals("SI"))  {
					  chkseg  = !existeFacultadCuenta(arrayCuentas[indice][1], "CARCHPROP", "-");
					  if(chkseg == true) {
						  EIGlobal.mensajePorTrace("Cuenta Leida Encontrada "+arrayCuentas[indice][1], EIGlobal.NivelLog.INFO);
						  return chkseg; }
				}
			}
	}
	return chkseg;*/
}
	/**
	* Valida los Campos que ley� de un archivo
	 * @param valCtas  Objeto para el llamado al servicio VALCTA_TR, para validar una cuenta.
	**/
	public String Valida_Campos_Leidos(String[] Campos_Ocurre,String personalidad, int registros, HttpServletRequest req, ValidaCuentaContrato valCtas) {

		String Cadena = "";

		if (Campos_Ocurre[0].trim().equals("")) {
			Cadena = "cuadroDialogo(\"Cuenta de Cargo no existe en registro: "+registros+"\", 3);";
			return Cadena; }
		if (Campos_Ocurre[0].length() > 16) {
			Cadena = "cuadroDialogo(\"Error en Longitud de Cuenta de Cargo en registro: "+registros+"\", 3);";
			return Cadena; }
		if (Existe_Cuenta(Campos_Ocurre[0], req, valCtas) != true)  {
			Cadena = "cuadroDialogo(\"Error en Cuenta de Cargo en registro: "+registros+"\", 3);";
			return Cadena; 	}
		if (Campos_Ocurre[2].trim().equals("")) {
			Cadena = "cuadroDialogo(\"Importe no existe en registro: "+registros+"\", 3);";
			return Cadena;}
		if (Campos_Ocurre[2].length() > 18) {
			Cadena = "cuadroDialogo(\"Error en Longitud de Importe de Cargo en registro: "+registros+"\", 3);";
			return Cadena; }
		if (Campos_Ocurre[4].length() > 40) {
			Cadena = "cuadroDialogo(\"Error en Longitud de Instrucciones en registro: "+registros+"\",3);";
			return Cadena; }
		if (Campos_Ocurre[5].trim().equals("")) {
			Cadena = "cuadroDialogo(\"Personalidad no existe en registro: "+registros+"\", 3);";
			return Cadena; }
		if (!Campos_Ocurre[5].trim().equals(personalidad)) {
			Cadena = "cuadroDialogo(\"Archivo con personalidad diferente a la seleccionada en registro: "+registros+"\", 3);";
			return Cadena; }
		if (personalidad.equals("1")) {
			if (Campos_Ocurre[6].trim().equals("")) {
			Cadena = "cuadroDialogo(\"Nombre del Beneficiario no existe en registro: "+registros+"\", 3);";
			return Cadena;  }
			if (Campos_Ocurre[6].length() > 20) {
			Cadena = "cuadroDialogo(\"Error en Longitud de Nombre del Beneficiario en registro: "+registros+"\",3);";
			return Cadena; }
			if (Campos_Ocurre[7].trim().equals("")) {
			Cadena = "cuadroDialogo(\"Apellido Paterno del Beneficiario no existe en registro: "+registros+"\", 3);";
			return Cadena; }
			if (Campos_Ocurre[7].length() > 20) {
			Cadena = "cuadroDialogo(\"Error en Longitud de Apellido Paterno del Beneficiario en registro: "+registros+"\", 3);";
			return Cadena; }
			if (Campos_Ocurre[8].length() > 20) {
			Cadena = "cuadroDialogo(\"Error en Longitud de Apellido Materno del Beneficiario en registro: "+registros+"\", 3);";
			return Cadena; }
		}else{
			if (Campos_Ocurre[6].trim().equals("")) {
			Cadena = "cuadroDialogo(\"La Raz&oacute;n Social del Beneficiario no existe en registro: "+registros+"\", 3);";
			return Cadena; }
			if (Campos_Ocurre[6].length() > 60) {
			Cadena = "cuadroDialogo(\"Error en Longitud de Nombre de Empresa en registro: "+registros+"\", 3);";
			return Cadena; }
		}
			return Cadena;
	}
	public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().removeAttribute("bitacora");

        HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();

			EIGlobal.mensajePorTrace("valida-> " + request.getParameter("valida"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("personalidad-> " + request.getParameter("personalidad"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("opcion-> " + request.getParameter("opcion"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("Arreglo_Cuentas_Cargo-> " + request.getParameter("Arreglo_Cuentas_Cargo"), EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("Arreglo_Importes-> " + request.getParameter("Arreglo_Importes"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("Arreglo_Instrucciones-> " + request.getParameter("Arreglo_Instrucciones"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("Arreglo_Paterno-> " + request.getParameter("Arreglo_Paterno"), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("Arreglo_Materno-> " + request.getParameter("Arreglo_Materno"), EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("Arreglo_Nombre-> " + request.getParameter("Arreglo_Nombre"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Arreglo_Razones_Sociales-> " + request.getParameter("Arreglo_Razones_Sociales"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Arreglo_RFCs-> " + request.getParameter("Arreglo_RFCs"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Arreglo_IVAs-> " + request.getParameter("Arreglo_IVAs"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("arregloestatus1-> " + request.getParameter("arregloestatus1"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("registros-> " + request.getParameter("registros"), EIGlobal.NivelLog.INFO);

			String value;

			value = (String) request.getParameter ("valida");
			if(value != null && !value.equals(null)) {
				tmp.put("valida",request.getParameter ("valida"));
			}

            tmp.put("personalidad",request.getParameter ("personalidad"));
            tmp.put("opcion",request.getParameter("opcion"));
            tmp.put("Arreglo_Cuentas_Cargo",request.getParameter ("Arreglo_Cuentas_Cargo"));

			tmp.put("Arreglo_Importes",request.getParameter ("Arreglo_Importes"));
            tmp.put("Arreglo_Instrucciones",request.getParameter ("Arreglo_Instrucciones"));
            tmp.put("Arreglo_Paterno",request.getParameter("Arreglo_Paterno"));
            tmp.put("Arreglo_Materno",request.getParameter ("Arreglo_Materno"));

			tmp.put("Arreglo_Nombre",request.getParameter ("Arreglo_Nombre"));
			tmp.put("Arreglo_Razones_Sociales",request.getParameter ("Arreglo_Razones_Sociales"));
			tmp.put("Arreglo_RFCs",request.getParameter ("Arreglo_RFCs"));
			tmp.put("Arreglo_IVAs",request.getParameter ("Arreglo_IVAs"));
			tmp.put("arregloestatus1",request.getParameter ("arregloestatus1"));
			tmp.put("registros",request.getParameter ("registros"));

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
		EIGlobal.mensajePorTrace("\n\n\n Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
    }
}