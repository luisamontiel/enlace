/**
 * @author H�ctor Alfredo Hurtado Obreg�n
 * @desc	Servlet para la opci�n Servicios>Nomina>Catalogo de nomina>Alta por archivo
 *
 * */

package mx.altec.enlace.servlets;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.DetalleCuentasBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.DetalleCuentasServicio;
import mx.altec.enlace.utilerias.DetalleCuentasException;
import mx.altec.enlace.utilerias.DetalleCuentasValidador;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.Formateador;

/*Inicia - EVERIS VALIDACION FICHEROS MALICIOSOS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION FICHEROS MALICIOSOS */


public class PrevalidadorServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	String rutadeSubida=Global.DIRECTORIO_LOCAL;

    public PrevalidadorServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doDefault(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doDefault(request, response);
	}


	protected void doDefault(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String numContrato=null;
		HttpSession sessionHttp = request.getSession();

		BaseResource session = (BaseResource) sessionHttp.getAttribute("session");
		boolean sesionvalida=SesionValida( request, response );

		if(sesionvalida)
		{
			numContrato=(session.getContractNumber()==null)?"":session.getContractNumber();
			try {
				request.setAttribute ("MenuPrincipal", session.getStrMenu ());
				request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
			}
			catch(Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			String opcion = request.getParameter("opcion");

			/*Opcion 1 Redirigir a la pantalla prevalidador.jsp*/
			if (opcion.equals("1")){

				EIGlobal.mensajePorTrace("Redirigiendo a la pantalla de prevalidador", EIGlobal.NivelLog.INFO);
				
				request.getSession().setAttribute("prevalidadorMensaje", null);

				request.setAttribute ("Encabezado", CreaEncabezado ( "Alta de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > Empleados > " +
					"Cat&aacute;logo de N&oacute;mina > Alta por Archivo", "s37120h", request ) );
				request.getSession().setAttribute("prevalidadorMensajeError", null);
				evalTemplate("/jsp/prevalidador.jsp", request, response );
			}

			/*Opcion 2 Carga de archivo, invocar validaciones */
			if (opcion.equals("2")){

				DetalleCuentasValidador validador = new  DetalleCuentasValidador();
				DetalleCuentasServicio servicio = new DetalleCuentasServicio();
				List<DetalleCuentasBean> listaArchivo = null;

				EIGlobal.mensajePorTrace("Comenzando la importacion del archivo", EIGlobal.NivelLog.INFO);

				request.setAttribute ("Encabezado", CreaEncabezado ( "Alta de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > Empleados > " +
					"Cat&aacute;logo de N&oacute;mina > Alta por Archivo", "s37120h", request ) );


				HashMap<String, String> parametros = getFormParameters(request);

				EIGlobal.mensajePorTrace("GETFILEINBYTES OK", EIGlobal.NivelLog.INFO);

				String  nombreArchivo= parametros.get("fileName");
				String  nombreArchivoUpload= parametros.get("fileNameUpload");//varible con el nombre del archivo que se esta mandando a llamar

				EIGlobal.mensajePorTrace("Nombre archivo " + nombreArchivo, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Nombre archivo upload" + nombreArchivoUpload, EIGlobal.NivelLog.INFO);


				File f = new File(rutadeSubida+"/" +nombreArchivo);

				EIGlobal.mensajePorTrace("TOMO EL ARCHIVO > " + f, EIGlobal.NivelLog.INFO);


				EIGlobal.mensajePorTrace("Inicia valida fichero malicioso", EIGlobal.NivelLog.INFO);
				if(!UtilidadesEnlaceOwasp.esArchivoValido(f, "A")) {
				/*FINALIZA - EVERIS VALIDACION FICHEROS */	
					EIGlobal.mensajePorTrace("El archivo no es un zip o txt", EIGlobal.NivelLog.INFO);
					despliegaPaginaError("Archivo de Importaci&oacute;n inv&aacute;lido",request, response );						
					
				}else{
				
				try{

					EIGlobal.mensajePorTrace("Obtener cuentas archivo", EIGlobal.NivelLog.INFO);
					validador.setContrato(session.getContractNumber());
					validador.setUsuario(session.getUserID8());
					validador.setPerfil(session.getUserProfile());

					listaArchivo =  validador.obtenerCuentasArchivo(f, this);

					for (int i=0; i<listaArchivo.size(); i++){

						EIGlobal.mensajePorTrace(listaArchivo.get(i).getCuenta() + " " +
								listaArchivo.get(i).getaPaterno() + " " +
								listaArchivo.get(i).getaMaterno() + " " +
								listaArchivo.get(i).getNombre(), EIGlobal.NivelLog.INFO);

					}

					listaArchivo = servicio.obtenerCuentasNoRegistradas(listaArchivo, numContrato);

					/*if (listaArchivo.size()==0){
						DetalleCuentasBean a= new DetalleCuentasBean();
						a.setCuenta("13697413905");
						a.setNombre("JUAN");
						a.setaPaterno("PEREZ");
						a.setaPaterno("LOPEZ");
						a.setGuardar(false);
						listaArchivo.add(a);

						a= new DetalleCuentasBean();
						a.setCuenta("97527193851");
						a.setNombre("JOSE");
						a.setaPaterno("HERNANDEZ");
						a.setaPaterno("GONZALEZ");
						a.setGuardar(false);
						listaArchivo.add(a);

					}*/

					if (listaArchivo==null || listaArchivo.size()==0){
						String mensaje="Las cuentas ya se encuentran registradas";
						request.getSession().setAttribute("prevalidadorMensaje", mensaje);
						evalTemplate("/jsp/prevalidador.jsp", request, response );
					}else{
						request.setAttribute("listaDetalles", listaArchivo);
						request.getRequestDispatcher("/enlaceMig/DetalleCuentas").forward(request, response);
					}

				}catch(DetalleCuentasException ctasEx){
					EIGlobal.mensajePorTrace("Error al obtener las cuentas del archivo " + ctasEx.getMessage(), EIGlobal.NivelLog.DEBUG);

					EIGlobal.mensajePorTrace("Formamos el mensaje para la pantalla", EIGlobal.NivelLog.DEBUG);
					Vector<String> errores = validador.getErrores();
					int numErrores = errores.size();
					String descErrores=ctasEx.getMessage();
					descErrores += "<br>";

					int tamanio = 130;
					int i = 0;
					for (i=0; i<errores.size(); i++){
						descErrores +=errores.get(i) + "<br>";
						if (i==9){
							break;
						}
					}

					if  (numErrores>10){
						descErrores+="Existen otros " + (numErrores -10) + " errores. Favor de revisar su archivo";
					}

					tamanio += 13 * (i+3);
					EIGlobal.mensajePorTrace(descErrores, EIGlobal.NivelLog.DEBUG);
					request.getSession().setAttribute("prevalidadorMensaje", null);
					request.getSession().setAttribute("tamanioMensaje", tamanio);
					request.getSession().setAttribute("prevalidadorMensajeError", descErrores);
					evalTemplate("/jsp/prevalidador.jsp", request, response );
				}
			}


			}

			if (opcion.equals("3")){
				/*Regres� exitosamente de la pantalla de alta de registros*/

				request.getSession().setAttribute("prevalidadorMensaje", "Cuentas registradas correctamente");
				evalTemplate("/jsp/prevalidador.jsp", request, response );
			}

			if (opcion.equals("4")){
				/*Regres� con errores de la pantalla de alta de registros*/
				evalTemplate("/jsp/prevalidador.jsp", request, response );
			}
		}




	}

}
