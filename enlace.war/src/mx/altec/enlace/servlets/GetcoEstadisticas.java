package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.CombosConf;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



/**
** coEstadisticas is a blank servlet to which you add your own code.
*/
public class GetcoEstadisticas extends BaseServlet
{

    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException
    {

		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
		boolean sesionvalida = SesionValida( req, res );

		// servidor para facultar a Consultas

		if ( sesionvalida ) {
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu",       session.getFuncionesDeMenu());
			req.setAttribute("diasInhabiles", diasInhabilesAnt() );
			req.setAttribute("Encabezado",    CreaEncabezado("Reporte de estad&iacute;sticas de proveedores",
				"Confirming &gt; Estad&iacute;sticas &gt; Reportes" , req) );

			// Obtiene los usuarios de las cuentas asociadas al contrato.

			String[][] arrayUsuarios = TraeUsuarios(req);
			String Usuarios          = "";
			String NoUsuario         = "";
			long UserAux = 0;
			long numero1 = 0;
			long numero2 = 0;
			for( int i = 1; i <= Integer.parseInt( arrayUsuarios[0][0].trim() ); i++ ) {
				for( int j = i; j <= Integer.parseInt( arrayUsuarios[0][0].trim() ); j++ ) {
					numero1 = new Long(arrayUsuarios[i][1]).longValue();
					numero2 = new Long(arrayUsuarios[j][1]).longValue();
					if(numero1 > numero2) {
						Usuarios = arrayUsuarios[i][2];
						NoUsuario = arrayUsuarios[i][1];
						arrayUsuarios[i][2] = arrayUsuarios[j][2];
						arrayUsuarios[i][1] = arrayUsuarios[j][1];
						arrayUsuarios[j][2] = Usuarios;
						arrayUsuarios[j][1] = NoUsuario;
					}
				}
			}

			Usuarios = "";
			EIGlobal.mensajePorTrace( "numero de Usuarios " + arrayUsuarios[0][0], EIGlobal.NivelLog.INFO);
			for ( int indice = 1; indice <= Integer.parseInt( arrayUsuarios[0][0].trim() ); indice++ ) {
				Usuarios += "<OPTION value = " + arrayUsuarios[indice][1] + ">" +
						arrayUsuarios[indice][1] + " " + arrayUsuarios[indice][2] + "\n";
			}
			String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" +
			ObtenAnio() + "\">";
			datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
			datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
			req.setAttribute("Bitfechas",datesrvr);


			req.setAttribute("Usuarios",Usuarios);
            CombosConf arch_combos = new CombosConf ();
            //arch_combos.getServicioTux ().setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());

			//Las tres primeras lineas son utilizadas para que sea
			//posible entrar con cualquier usuario
			arch_combos.setUsuario (session.getUserID());
			arch_combos.setContrato (session.getContractNumber());
			arch_combos.setPerfil (session.getUserProfile());
			req.setAttribute ("arch_combos", arch_combos);

			session.setModuloConsultar(IEnlace.MOperaciones_mancom);
            req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
			evalTemplate(IEnlace.REPORTES_ESTA,req,res);

		} else {
			req.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}
    }

			//evalTemplate("/jsp/coConsultaEsta.jsp",req,res);


}