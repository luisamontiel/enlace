/** Banco Santander Mexicano
 *		Clase NomEmpImporta
 *		@author Gerardo Salazar Vega
 *		@version 1.0
 *		@since 15 de Noviembre del 2000
 *		@return SUCCESS resultado de la ejecucion del applogic
 *		responsable: Roberto Guadalupe Resendiz Altamirano
 *		descripcion: Applogic que realiza las operaciones
 *		importa: guarda el archivo que esta importando el usuario
 *		nuevo: crea un archivo nuevo
 *		muestra: lee el contenido de un archivo y lo muestra en pantalla
 *		llamando al template de mantenimiento de empleados
 */

package mx.altec.enlace.servlets;

import java.util.zip.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.archivoEmpleados;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
/*Inicia - EVERIS VALIDACION CSS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION CSS */

/** 
*   Isban M�xico
*   Clase: NomEmpImporta.java
*   Descripci�n: Servlet utilizado en la funcionalidad Nomina EmpImporta.
*
*   Control de Cambios:
*   1.0 16/10/2017 Everis - Limpieza Cross Site Scripting
*/
public class NomEmpImporta extends BaseServlet {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 7601871170605389237L;

	/** El objeto texto log. */
	String textoLog ="";

	/**
	 * Texto log.
	 *
	 * @param funcion El objeto: funcion
	 * @return Objeto string
	 */
	private String textoLog(String funcion){
		return textoLog = NomEmpImporta.class.getName() + "." + funcion + "::";
	}

	/**
	 * Do get.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	/**
	 * Do post.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	/**
	 * Default action.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 * @throws ServletException La servlet exception
	 */
	public void defaultAction(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {

		textoLog("defaultAction");
		boolean sesionvalida = SesionValida(request, response);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		boolean bander=true;
		
		if (sesionvalida) {

			// Obtiene los datos a mostrar en el template
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request.setAttribute("newMenu", session.getFuncionesDeMenu());
			request.setAttribute("Encabezado", CreaEncabezado("Alta de Empleados", "Servicios &gt; N&oacute;mina &gt;Alta de Empleados", "s25720h", request));
			request.setAttribute("facultades", sess.getAttribute("facultadesE"));

			EIGlobal.mensajePorTrace(textoLog + "Facultades = "+ sess.getAttribute("facultadesE"), EIGlobal.NivelLog.INFO);

			String nombreArchivoEmp = "";
			StringBuffer contenidoArchivoStr = new StringBuffer("");
			String operacion = getFormParameter(request, "operacion");

			
			if(operacion.equals("errorTipeFile")){
				bander=false;
				despliegaPaginaError("Archivo de Importaci&oacute;n inv&aacute;lido",request, response );
			}
			EIGlobal.mensajePorTrace(textoLog + "Operacion = " + operacion, EIGlobal.NivelLog.INFO);

			// Selecciona el metodo con el que va a crear el archivo
			if(operacion == null) {
				operacion = "";
			}
			if (operacion.equals("importar")) {
				// modificacion para integracion
				nombreArchivoEmp = importaArchivoEmpleados(request, response);
				request.setAttribute("Sucursal", getFormParameter(request, "Sucursal"));
				request.setAttribute("tipo_archivo", "importado");
			}
			else if (operacion.equals("nuevo")) {
				nombreArchivoEmp = creaArchivoEmpleados(request, response);
				request.setAttribute("Sucursal", getFormParameter(request, "Sucursal"));
				request.setAttribute("tipo_archivo", "nuevo");
			}
			else {
				request.setAttribute("tipo_archivo", getFormParameter(request, "tipo_archivo"));
				if (getFormParameter(request, "tipo_archivo").equals(null)){
					
					try { // Borra el archivo invalido
						File vArchivo = new File(nombreArchivoEmp);

						if (vArchivo.exists()) {
							vArchivo.delete();
						}
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(textoLog + "error creando objeto File(No valido): " + e, EIGlobal.NivelLog.INFO);
					}
					
					
					request.setAttribute("ArchivoErr","cuadroDialogo('Archivo de Importaci&oacute;n inv&aacute;lido', 3);");
				}else{
				
				nombreArchivoEmp = session.getArchivoEmpNom();
				}
			}

			archivoEmpleados archivo1 = new archivoEmpleados(nombreArchivoEmp, session.getClaveBanco());
			request.setAttribute("total_registros", "" + archivo1.tamanioArchivo());
			request.setAttribute("max_registros", "" + Global.MAX_REGISTROS);

			if (operacion.equals("importar")) {
				/* Guarda objeto BaseResource y evitar que expire la sesion */
				String sBaseFilename = saveBaseResource(session, sess.getId());

				if (sBaseFilename != null) {
					request.setAttribute("basefilename", sBaseFilename);
				}

				// Llama al metodo para validar el archivo
				if (archivo1.validaArchivo(sess)) {

					// Si el archivo es valido muestra el contenido y el mensaje del numero de registros importados
					EIGlobal.mensajePorTrace(textoLog + "Global.MAX_REGISTROS : = " + Global.MAX_REGISTROS, EIGlobal.NivelLog.INFO);
					if (archivo1.tamanioArchivo() <= Global.MAX_REGISTROS)
						contenidoArchivoStr.append(archivo1.lecturaArchivo());

					request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
					request.setAttribute("mensaje_js", "cuadroDialogo (\"Se importaron exitosamente: " + archivo1.totalRegistros + " registros\",1)");
					String liga = nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/"));

					request.setAttribute("ligaExportar", liga);
					archivo1.close();
				} else {
					/*
					 * Cuando hay errores en la validacion muestra una tabla con
					 * la descripcion de todos lo errores
					 */
					request.setAttribute("archivoEstatus", archivo1.cadenaValidacion.toString());
					request.setAttribute("ArchivoErr", "archivoErrores();");

					try { // Borra el archivo invalido
						File vArchivo = new File(nombreArchivoEmp);

						if (vArchivo.exists()) {
							vArchivo.delete();
						}
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(textoLog + "error creando objeto File(No valido): " + e, EIGlobal.NivelLog.INFO);
					}
					try { // Borra el objeto BaseResource serializado
						File baseFile = new File(sBaseFilename);

						if (baseFile.exists()) {
							baseFile.delete();
						}
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(textoLog + "error creando objeto File(No valido): " + e, EIGlobal.NivelLog.INFO);
					}
				}
			} else {
				// Coloca la liga para exportar el archivo actual
				request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
				contenidoArchivoStr.append(archivo1.lecturaArchivo());
				archivo1.close();
				request.setAttribute("lista_numeros_empleado", archivo1.lista_empleados.toString());
			}

			// Llama al template de mantenimiento
			if(bander){
				request.setAttribute("ContenidoArchivo", contenidoArchivoStr.toString());
				request.setAttribute("operacion", operacion);
				evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
				
			}else{
				request.setAttribute("ArchivoErr","cuadroDialogo('Archivo de Importaci&oacute;n inv&aacute;lido', 3);");
				despliegaPaginaError("Archivo de Importaci&oacute;n inv&aacute;lido",request, response );
			}
			
		} else if (sesionvalida) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	/**
	 * Importa archivo empleados.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 * @return Objeto string
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 * @throws ServletException La servlet exception
	 */
	@SuppressWarnings("resource")
	private String importaArchivoEmpleados(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("importaArchivoEmpleados");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer nombreArchivoEmp = new StringBuffer("");
		StringBuffer nombreArchivoEmailEmp = new StringBuffer("");
		String nombreOriginal = "";
		String nombreInicial = "";
		String strSuc = "";
		boolean flagArchivoMalicioso = true;

		try {
			EIGlobal.mensajePorTrace(textoLog + "Creando nombre archivo", EIGlobal.NivelLog.INFO);
			// Obtiene el nombre del archivo y le asigna extension .emp
			nombreOriginal = getFormParameter(request, "fileName");
			nombreArchivoEmp = new StringBuffer("");
			nombreArchivoEmp.append(IEnlace.DOWNLOAD_PATH);
			nombreArchivoEmp.append(nombreOriginal);
			nombreInicial = nombreArchivoEmp.toString();

			String usuario = session.getUserID8();

			nombreArchivoEmailEmp.append(IEnlace.DOWNLOAD_PATH);
			nombreArchivoEmailEmp.append(usuario);
			nombreArchivoEmailEmp.append(".nae");

			// Guarda el nombre del archivo en el objeto session
			EIGlobal.mensajePorTrace(textoLog + "nombreArchivoEmp :" + nombreArchivoEmp.toString(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "nombreArchivoEmailEmp :" + nombreArchivoEmailEmp.toString(), EIGlobal.NivelLog.INFO);
			File fullFile = new File(nombreArchivoEmp.toString());
			File savedFile = new File(fullFile.getName());
			/*
			 * Si el archivo viene en zip se vuelve a guardar el valor del
			 * archivo en nombreArchivoEmp con el archivo extraido. Se cambia el
			 * valor de session de la ruta del archivo apuntando al nuevo
			 * archivo
			 */
			/*INICIA - EVERIS VALIDACION FICHEROS */
			EIGlobal.mensajePorTrace("Inicia valida fichero malicioso", EIGlobal.NivelLog.INFO);
			if(!UtilidadesEnlaceOwasp.esArchivoValido(fullFile, "A")) {
			/*FINALIZA - EVERIS VALIDACION FICHEROS */	
				EIGlobal.mensajePorTrace("El archivo no es un zip o txt", EIGlobal.NivelLog.INFO);
												
				flagArchivoMalicioso=false;
				throw new Exception (" Extension diferente de zip o txt....");
			}
			
			/*INICIA - EVERIS VALIDACION FICHEROS */			
			EIGlobal.mensajePorTrace("NomEmpImporta || Inicia valida fichero malicioso", EIGlobal.NivelLog.INFO);
			
			if (UtilidadesEnlaceOwasp.esArchivoValido(fullFile, "Z")) {
			/*FINALIZA - EVERIS VALIDACION FICHEROS */
				
				
				// Obtiene el nombre del archivo y le asigna extension .emp
				nombreOriginal = uncompress(savedFile, request);
			} else {
				if(UtilidadesEnlaceOwasp.esArchivoValido(fullFile, "T")){
					nombreOriginal = extension(savedFile.getName(), request);
				
				}else{
				EIGlobal.mensajePorTrace(textoLog + "El archivo no es un zip o txt", EIGlobal.NivelLog.INFO);
				// Obtiene el nombre del archivo y le asigna extension .emp
				//nombreOriginal = renameFile(savedFile, request);
				
				flagArchivoMalicioso=false;
				throw new Exception (" Extension diferente de zip o txt....");
				}
			}

			nombreArchivoEmp = new StringBuffer("");
			nombreArchivoEmp.append(IEnlace.DOWNLOAD_PATH);
			nombreArchivoEmp.append(nombreOriginal);
			// Guarda el nombre del archivo en el objeto session
			session.setArchivoEmpNom(nombreArchivoEmp.toString());

			// Se anexan las l?neas comentadas arriba pero con el valor
			// actualizado en nombreArchivoEmp.
			EIGlobal.mensajePorTrace(textoLog + "Creando objeto File", EIGlobal.NivelLog.INFO);
			File pathArchivoEmp = new File(nombreArchivoEmp.toString());
			EIGlobal.mensajePorTrace(textoLog + "Path: [" + pathArchivoEmp.getPath() +"]" , EIGlobal.NivelLog.INFO);
			File pathArchivoMail = new File(nombreArchivoEmailEmp.toString());
			EIGlobal.mensajePorTrace(textoLog + "Escribiendo en : " + pathArchivoEmp, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "Creando objeto RandomAccessFile", EIGlobal.NivelLog.INFO);

			RandomAccessFile archivoEmpleadosImportado = new RandomAccessFile(pathArchivoEmp.getPath(), "rw");
			RandomAccessFile archivoEmpleadosMail = new RandomAccessFile(pathArchivoMail, "rw");
			archivoEmpleadosMail.setLength(0); //para truncar el archivo en caso que exista

			// Ahora lee el archivo savedFile y escribelo al
			// archivoEmpleadosImportado
			fullFile = new File(nombreInicial);
			savedFile = new File(fullFile.getName());
			BufferedReader tempBufferedReader;
			tempBufferedReader = new BufferedReader(new FileReader(IEnlace.DOWNLOAD_PATH + savedFile.getName()));
			String actualLine = "";
			boolean readFile = false;
			String valSucursal = "";
			/*Inicia - EVERIS VALIDACION CSS */
			EIGlobal.mensajePorTrace("Validar CSS para el parametro [Sucursal]", EIGlobal.NivelLog.INFO);
			valSucursal = UtilidadesEnlaceOwasp.cleanString(getFormParameter(request, "Sucursal").trim());
			/*Finaliza - EVERIS VALIDACION CSS */
			EIGlobal.mensajePorTrace(textoLog + "** valSucursal [" + valSucursal + "]", EIGlobal.NivelLog.INFO);
			/**
			 * Asumsiones, La longitud del registro sera al menos de 542 por el
			 * motivo de que la SUCURSAL es opcional el TELEFONO PARTICULAR es
			 * opcional pero la FECHA INGRESO no. Por lo tanto, la posicion del
			 * FECHA INGRESO sera precedida por blanks.
			 */
			EIGlobal.mensajePorTrace(textoLog + "Validando Sucursal en los registros--- ", EIGlobal.NivelLog.INFO);
			while (!readFile) {
				// Convert String to Bytes array
				actualLine = (String) tempBufferedReader.readLine();
				if (actualLine.length() == 11)// Se alcanza el sumario del archivo, therefore es el EOF.
				{
					archivoEmpleadosImportado.writeBytes(actualLine + "\r\n");
					readFile = true;
				} else {// Not EOF
					if (actualLine.length() == 542) {
						String strTmp1 = actualLine.substring(0, 324);
						if (valSucursal.trim().length() > 0 && actualLine.substring(324, 328).trim().length() == 0) {
							strSuc = valSucursal;
						} else {
							strSuc = actualLine.substring(324, 328);
						}
						String strTmp2 = actualLine.substring(328, 542);

						String strCuenta = actualLine.substring(515, 526);
						String strApPat = actualLine.substring(19, 49).trim();
						String strApMat = actualLine.substring(49, 69).trim();
						String strNombre = actualLine.substring(69, 99).trim();
						archivoEmpleadosMail.writeBytes(strCuenta + strApPat + " " + strApMat + " " + strNombre + "\r\n");
						archivoEmpleadosImportado.writeBytes(strTmp1 + strTmp2 + strSuc + "\r\n");
						//EIGlobal.mensajePorTrace(textoLog + "archivoEmpleadosImportado >>" + strTmp1 + strTmp2 + strSuc + "<<", EIGlobal.NivelLog.INFO);
					} else {// Encabezado
						archivoEmpleadosImportado.writeBytes(actualLine + "\r\n");
					}
				}// Not EOF
			}// End While
			EIGlobal.mensajePorTrace(textoLog + "Sucursal Validada", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "Cerrando archivo", EIGlobal.NivelLog.INFO);
			archivoEmpleadosImportado.close();
			archivoEmpleadosMail.close();
			try { // Borra el archivo /productos/ias60/ias/
				EIGlobal.mensajePorTrace(textoLog + "Borrando Archivo... :" + savedFile, EIGlobal.NivelLog.INFO);
				savedFile = new File(IEnlace.DOWNLOAD_PATH + savedFile.getName());
				if (savedFile.exists()) {
					savedFile.delete();
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(textoLog + "Error al borrar el archivo: " + savedFile + e, EIGlobal.NivelLog.INFO);
			}
			EIGlobal.mensajePorTrace(textoLog + "Nombre Original = " + nombreOriginal, EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(textoLog + "Error creando archivo de empleados de nomina" + e.getMessage(), EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
			if(flagArchivoMalicioso){
				EIGlobal.mensajePorTrace(textoLog + "error creando objeto File(delete): " + e, EIGlobal.NivelLog.INFO);
				}else{
				EIGlobal.mensajePorTrace(textoLog + " Extension diferente a txt o zip....", EIGlobal.NivelLog.INFO);
				request.setAttribute("ArchivoErr","cuadroDialogo('Archivo de Importaci&oacute;n inv&aacute;lido', 3);");
			
		}
		}
		return nombreArchivoEmp.toString();
	}

	/**
	 * El metodo creaArchivoEmpleados() se utiliza cuando se selecciona la
	 * opcion (nuevo) crea el archivo fisicamente y guarda el nombre en el
	 * objeto session el archivo contiene los registros para el encabezado y el
	 * sumario si existe un archivo con el mismo nombre lo borra y despues crea
	 * el archivo.
	 *
	 * @param file El objeto: file
	 * @param request El objeto: request
	 * @return nombreArchivoEmp: el nombre del archivo que se ha creado
	 */

	/**
	 * Este m?todo recibe un archivo de entrada y uno de salida y copia todo el
	 * contenido del archivo 1 al 2. Si el archivo 2 existe es borrado antes de
	 * que se copie el archivo 1 Al finalizar el copiado se borra el archivo 1
	 * (original). Nota: No se utiliza el m?todo renameTo de la clase File
	 * debido a que es muy dependiente del sistema operativo y no se garantiza
	 * que el archivo se remombre. Aun cuando el archivo se renombre, no se
	 * garantiza que el contenido del archivo sea copiado.
	 */
	@SuppressWarnings("unused")
	private String renameFile(File file, HttpServletRequest request) {
		textoLog("renameFile");
		//String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf("/"));
		String path = IEnlace.DOWNLOAD_PATH;
		EIGlobal.mensajePorTrace(textoLog + "Path: [" + path + "]", EIGlobal.NivelLog.INFO);
		String filename = file.getName();
		EIGlobal.mensajePorTrace(textoLog + "filename: [" + filename + "]", EIGlobal.NivelLog.INFO);
		try {
			final int BUFFER = 2048;
			BufferedOutputStream destBuff = null;
			@SuppressWarnings("resource")
			FileInputStream fis = new FileInputStream(path + file);
			byte data[] = new byte[BUFFER];
			int count;
			filename = extension(filename, request);
			EIGlobal.mensajePorTrace(textoLog + "fileName (extension): [" + filename + "]", EIGlobal.NivelLog.INFO);
			File sFile = new File(path + "/" + filename);
			EIGlobal.mensajePorTrace(textoLog + "sFile: [" + sFile + "]", EIGlobal.NivelLog.INFO);
			if (sFile.exists()) {
				sFile.delete();
			}
			FileOutputStream fos = new FileOutputStream(sFile);
			destBuff = new BufferedOutputStream(fos, BUFFER);

			while ((count = fis.read(data, 0, BUFFER)) != -1) {
				destBuff.write(data, 0, count);
			}
			destBuff.flush();
			destBuff.close();
			/*if (file.exists()) {
				EIGlobal.mensajePorTrace(textoLog + "Borrando Archivo original: ", EIGlobal.NivelLog.INFO);
				file.delete();
			}*/
			EIGlobal.mensajePorTrace(textoLog + "El archivo fue renombrado: ", EIGlobal.NivelLog.INFO);
			//file = sFile;
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(textoLog + "Error al renombrar el archivo: " + e, EIGlobal.NivelLog.INFO);
		}
		return filename;
	}

	/**
	 * Este m?todo recibe el archivo importado por el usuario (Si es un zip).
	 * Esta verificaci?n se obtiene por su extension. El m?todo descomprime el
	 * archivo y asigna el archivo extraido a file para sea procesado
	 * posteriormente.
	 *
	 * @param file El objeto: file
	 * @param request El objeto: request
	 * @return Objeto string
	 */
	@SuppressWarnings("unused")
	private String uncompress(File file, HttpServletRequest request) {
		textoLog("uncompress");
		String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf("/"));
		String filename = file.getName();
		try {
			EIGlobal.mensajePorTrace(textoLog + "El archivo viene en zip", EIGlobal.NivelLog.INFO);
			final int BUFFER = 2048;
			BufferedOutputStream dest = null;
			FileInputStream fis = new FileInputStream(file);
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
			ZipEntry entry;
			entry = zis.getNextEntry();
			EIGlobal.mensajePorTrace(textoLog + "entry: " + entry.getName(), EIGlobal.NivelLog.INFO);
			if (entry == null) {
				EIGlobal.mensajePorTrace(textoLog + "Entry fue null, archivo no es zip o est&aacute; da&ntilde;ando ", EIGlobal.NivelLog.INFO);
			} else {
				EIGlobal.mensajePorTrace(textoLog + "Extrayendo Archivo: " + entry.getName(), EIGlobal.NivelLog.INFO);
				int count;
				byte data[] = new byte[BUFFER];
				// Se escriben los archivos a disco
				// Nombre del archivo zip
				filename = extension(entry.getName(), request);
				File sFile = new File(path + "/" + filename);
				if (sFile.exists()) {
					sFile.delete();
				}
				FileOutputStream fos = new FileOutputStream(sFile);
				dest = new BufferedOutputStream(fos, BUFFER);

				while ((count = zis.read(data, 0, BUFFER)) != -1) {
					dest.write(data, 0, count);
				}
				dest.flush();
				dest.close();
				// Borra el archivo comprimido
				if (file.exists()) {
					EIGlobal.mensajePorTrace(textoLog + "Borrando Archivo Zip: ", EIGlobal.NivelLog.INFO);
					file.delete();
				}
				EIGlobal.mensajePorTrace(textoLog + "Fin de la extraccion: ", EIGlobal.NivelLog.INFO);
				file = sFile;
			}
			zis.close();
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Error al intentar extraer el archivo: ", EIGlobal.NivelLog.INFO);
		}
		return filename;
	}

	/**
	 * Este m?todo recibe el objeto BaseResource el cual cuenta con toda la
	 * informaci?n del usuario. Genera un archivo basado en el userID y la
	 * cadena _base.bak y guarda a BaseResource en el. Al momento de enviar el
	 * archvio en el servlet NomEmpOpciones se deserializa. Este proceso es
	 * necesario para evitar que la sesion expire durante el importado de
	 * archivos muy grandes.
	 *
	 * @param base El objeto: base
	 * @param sessID El objeto: sess id
	 * @return Objeto string
	 */
	private String saveBaseResource(BaseResource base, String sessID) {
		textoLog("saveBaseResource");
		String filename = "";
		try {
			filename = sessID;
			ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(IEnlace.DOWNLOAD_PATH + sessID + "_base.bak"));
			stream.writeObject(base);
			stream.flush();
			stream.close();
			EIGlobal.mensajePorTrace(textoLog + "Se guardo el object: " + filename, EIGlobal.NivelLog.INFO);
		} catch (IOException ioe) {
			filename = "";
			EIGlobal.mensajePorTrace(textoLog + "Error al serializar el archivo: " + ioe.getMessage(), EIGlobal.NivelLog.INFO);
		}
		return filename;
	}

	/**
	 * Crea archivo empleados.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 * @return Objeto string
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 * @throws ServletException La servlet exception
	 */
	private String creaArchivoEmpleados(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("creaArchivoEmpleados");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer nombreArchivoEmp = new StringBuffer("");
		String nombreOriginal = "";
		EIGlobal.mensajePorTrace(textoLog + "Entrando a creaArchivoEmpleados()", EIGlobal.NivelLog.INFO);
		nombreOriginal = getFormParameter(request, "nombre_nuevo");
		EIGlobal.mensajePorTrace(textoLog + "Creando nombre archivo", EIGlobal.NivelLog.INFO);
		nombreOriginal = extension(nombreOriginal, request);
		nombreArchivoEmp = new StringBuffer("");
		nombreArchivoEmp.append(IEnlace.DOWNLOAD_PATH);
		nombreArchivoEmp.append(nombreOriginal);

		try {
			File archivo = new File(nombreArchivoEmp.toString());
			if (archivo.exists()) {
				archivo.delete();
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "error creando objeto File(delete): " + e, EIGlobal.NivelLog.INFO);
		}

		// Creando el archivo nuevo
		try {
			StringBuffer fechaNuevo2 = new StringBuffer(ObtenFecha(true));
			StringBuffer fechaNuevo = new StringBuffer("");
			fechaNuevo.append(fechaNuevo2.substring(3, 5));
			fechaNuevo.append(fechaNuevo2.substring(0, 2));
			fechaNuevo.append(fechaNuevo2.substring(6));
			StringBuffer cadenaNueva = new StringBuffer("");
			cadenaNueva.append("100001E");
			cadenaNueva.append(fechaNuevo);
			cadenaNueva.append("\r\n30000200000\r\n");

			EIGlobal.mensajePorTrace(textoLog + "Creando objeto RandomAccessFile", EIGlobal.NivelLog.INFO);
			RandomAccessFile archivoEmpleadosNuevo = new RandomAccessFile(nombreArchivoEmp.toString(), "rw");
			archivoEmpleadosNuevo.setLength(0); //para truncar el archivo en caso que exista
			EIGlobal.mensajePorTrace(textoLog + "Escribiendo en : " + nombreArchivoEmp.toString(), EIGlobal.NivelLog.INFO);
			archivoEmpleadosNuevo.writeBytes(cadenaNueva.toString());
			EIGlobal.mensajePorTrace(textoLog + "Cerrando archivo", EIGlobal.NivelLog.INFO);
			archivoEmpleadosNuevo.close();
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(textoLog + "Error creando archivo de empleados de nomina: creaArchivoEmpleados :" + e, EIGlobal.NivelLog.INFO);
		}

		session.setArchivoEmpNom(nombreArchivoEmp.toString());
		EIGlobal.mensajePorTrace(textoLog + "setArchivoEmpNom :" + nombreArchivoEmp.toString(), EIGlobal.NivelLog.INFO);
		return nombreArchivoEmp.toString();
	}

	/**
	 * El metodo extension(nombre) le coloca al archivo la extension _emp.doc
	 * cuando no tiene extension tambien se la pone
	 *
	 * @param nombre El objeto: nombre
	 * @param request El objeto: request
	 * @return el nombre del archivo con extension _emp.doc
	 */
	private String extension(String nombre, HttpServletRequest request) {

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if (nombre.length() > 0) {
			if (nombre.indexOf('.') == -1) {
				StringBuffer nombre2 = new StringBuffer("");
				nombre2.append(nombre);
				nombre2.append(session.getUserID8());
				nombre2.append("_emp.doc");
				return nombre2.toString();
			} else {
				StringBuffer nombre2 = new StringBuffer("");
				nombre2.append(nombre.substring(0, nombre.indexOf('.')));
				nombre2.append(session.getUserID8());
				nombre2.append("_emp.doc");
				return nombre2.toString();
			}
		} else {
			StringBuffer cadena = new StringBuffer("");
			cadena.append("temp");
			cadena.append(session.getUserID8());
			cadena.append("_emp.doc");
			return cadena.toString();
		}
	}
}