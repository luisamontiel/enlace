package mx.altec.enlace.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.CepBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.TransferenciaInterbancariaBUS;
import mx.altec.enlace.bo.TrxB485BO;
import mx.altec.enlace.utilerias.AesCryptoService;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.Util;
import mx.altec.enlace.utilerias.TranInterConstantes;
import mx.altec.enlace.utilerias.UtileriasCep;

public class MDI_CepServlet extends BaseServlet{

	/**
	 * serialVersionUID serial version de compilacion
	 */
	private static final long serialVersionUID = -3980188336295303638L;
	/**
	 * Implementa doGet del servlet
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}
	/**
	 * Implementa doPost del servlet
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}
	/**
	 * Metodo default de la clase
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	private void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean sesionvalida = SesionValida( request, response );
		CepBean bean = new CepBean();

		if ( sesionvalida) {
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			StringBuffer cadenaACifrar=new StringBuffer();
			TransferenciaInterbancariaBUS instancia = new TransferenciaInterbancariaBUS();
			String CadenaCifrada  = null;
			String error="";
			String opcion = "0";
			boolean bandera = true;
			/**
			 * Trama a recibir [09/06/2015|TR204 9020248|CMCA |0021804440027810  |27.0]*
			 *                  09/06/2015|TR204 9020249|BANAM|0021804440027810  |27.0
			 *                  09/06/2015|TR004 9127564|BANAM|002010471800514371|2.0
			 * fecha=09/06/2015
			 * cveRastreo=TR204 9020249
			 * banco=BANAM
			 * ctaAbono=002010471800514371
			 * importe=2.0
			 * Banxico lo solicita de la siguiente manera
			 * 09/06/2015
			 * 20140421|T|TR-CEL-2635|40012|40044|5516243692|16.55
			 * */
			if(UtileriasCep.validaCampo(request.getParameter("opcion"))){
				opcion=(String) request.getParameter("opcion");
				EIGlobal.mensajePorTrace("MDI_CepServlet::opcion::"+opcion+"::", EIGlobal.NivelLog.INFO);
			}
			if(UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("fecha")))){
				bean.setFecha(UtileriasCep.formateaFecha((String)request.getParameter("fecha")));
			}else{
				error="La Fecha en la que se realiz&oacute; la operaci&oacute;n es nula o est&aacute; vac&iacute;a";
				bandera=false;
			}
			if(UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("cveRastreo")))){
				bean.setCveRastreo(Util.HtmlEncode(((String)request.getParameter("cveRastreo"))));
			}else{
				error="La Clave de Rastreo es nula o est&aacute; vac&iacute;a";
				bandera=false;
			}
			if(UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("banco")))){
				bean.setNomBanco(Util.HtmlEncode((String)request.getParameter("banco")));
			}else{
				error="La Clave del Banco es nula o est&aacute; vac&iacute;a";
				bandera=false;
			}
			if(UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("ctaAbono")))){
				bean.setCtaAbono(Util.HtmlEncode((String)request.getParameter("ctaAbono")));
			}else{
				error="La Cuenta de Abono es nula o est&aacute; vac&iacute;a";
				bandera=false;
			}
			if(UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("importe")))){
				bean.setImporte(Util.HtmlEncode((String)request.getParameter("importe")));
			}else{
				error="El Importe es nulo o est&aacute; vac&iacute;a";
				bandera=false;
			}
			if(UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("ctaCargo")))){
				bean.setCtaCargo(Util.HtmlEncode((String)request.getParameter("ctaCargo")));
			}

			if(opcion.equals("2") && UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("depositoABanco")))){    //FSW marreguin Liga CEP SPID 09/12/2016
				bean.setDepositoABanco(Util.HtmlEncode(request.getParameter("depositoABanco")));                            //FSW marreguin Liga CEP SPID 09/12/2016
			}																											    //FSW marreguin Liga CEP SPID 09/12/2016

			if(opcion.equals("3") && UtileriasCep.validaCampo(Util.HtmlEncode(request.getParameter("depositoABanco")))){
				bean.setDepositoABanco(Util.HtmlEncode(request.getParameter("depositoABanco")));
			}

			EIGlobal.mensajePorTrace("marreguin----------------->MDI_CepServlt (depositoABanco):-------------------->"+bean.getDepositoABanco(), EIGlobal.NivelLog.DEBUG);
			bean = instancia.consultaClaveBanco(bean);
			if(0==bean.getBancoSantander()){
				error="La Clave del Banco Santander es 0";
				bandera=false;
			}
			if(0==bean.getBanco()){
				error="La Clave del Banco es 0";
				bandera=false;
			}

			if(bandera){
				/**Armado de valores de cadena para banxico**/
				cadenaACifrar=concatenaCadena(bean,opcion);

				EIGlobal.mensajePorTrace("MDI_CepServlet::URL["+cadenaACifrar.toString()+"]", EIGlobal.NivelLog.INFO);

				/**Escribe en bitacora**/
				escribeBitacoraEnvioBanxico(request, "A", bean, opcion);

				/**Clave Cifrado**/
				String claveCifrado = "20150321";
				/**Invoca al metodo de cierre de duplicidad de sesion**/
				cierraDuplicidad(session.getUserID8());

				/**Inicia Cierre de sesion**/
				sess.setAttribute("session", session);
				sess.removeAttribute("session");
				sess.setMaxInactiveInterval( 0 );
				session.setSeguir(false);
				sess.invalidate();
				/**Termina Cierre de sesion**/

				/**guarda en sesion la url**/
				try {
					//*******************INICIA TCS FSW 12/2016***********************************
					String divisa = null;
					if ("1".equals(opcion)) {
						divisa="MXN";
					}else if("2".equals(opcion) || "3".equals(opcion)){
						divisa="USD";
					}
					CadenaCifrada=AesCryptoService.aesEncrypt(cadenaACifrar.toString(), divisa);
					EIGlobal.mensajePorTrace("MDI_CepServlet::CadenaCifrada::"+CadenaCifrada+"::", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("MDI_CepServlet::divisa::"+divisa+"::", EIGlobal.NivelLog.INFO);
					request.setAttribute("banxico",CadenaCifrada);
					//*******************INICIA TCS FSW 12/2016***********************************

					if ("USD".equals(divisa)) {
						request.setAttribute("claveCifrado", Global.sERIESUSD);
					} else {
						request.setAttribute("claveCifrado", Global.sERIE);
					}
					//*******************Fin TCS FSW 12/2016***********************************
					request.setAttribute("bancoSantander", Integer.toString(bean.getBancoSantander()));
					request.setAttribute("tipo_divisa",divisa);
					response.setContentType("application/x-www-form-urlencoded");
					evalTemplate(TranInterConstantes.URLBANXICO,request,response);
				} catch (BusinessException e) {
					EIGlobal.mensajePorTrace("BusinessException" + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
				}
			}else{
				EIGlobal.mensajePorTrace("MDI_CepServlet::no existen datos disponibles para la consulta->else", EIGlobal.NivelLog.INFO);
				despliegaPaginaErrorURL("No existen datos disponibles para su consulta.<br>"+error,"Consultas de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Consultas","s25490h",null,request,response);
				escribeBitacoraEnvioBanxico(request, "R", bean, opcion);
			}
		}else{
			EIGlobal.mensajePorTrace("MDI_CepServlet::no existen datos disponibles para la consulta->2else", EIGlobal.NivelLog.INFO);
			despliegaPaginaErrorURL("No existen datos disponibles para su consulta.","Consultas de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Consultas","s25490h",null,request,response);
			escribeBitacoraEnvioBanxico(request, "R", bean, "01");
		}
	}
	/**
	 * Metodo especial para bitacorizar y grabar pistas de auditoria para la redireccion a Portal Banxico
	 * la bitacora de Operaciones
	 * @param req Request del sistema
	 * @param estatus estatus que se grabara en la pista auditora
	 * @param bean bean que contiene los parametros para realizar la bitacorizacion de la pista auditora y la bitacora de operaciones
	 */
	private void escribeBitacoraEnvioBanxico(HttpServletRequest req, String estatus, CepBean bean, String opcion){
		HttpSession sess = req.getSession();
		EIGlobal.mensajePorTrace("MDI_CepServlet::escribeBitacoraEnvioBanxico:: Entrando ...", EIGlobal.NivelLog.DEBUG);
		if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			try{
				/**INICIA TCT_BITACORA**/
				BitacoraBO.bitacoraTCT(req, sess, TranInterConstantes.BITACORA, obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), bean.getCtaCargo(), bean.getCtaAbono(), TranInterConstantes.CODERROR, TranInterConstantes.TEXTOBITACORA) ;
				/**TERMINA TCT_BITACORA**/

				/**INICIA EWEB_ADMIN_BITACORA**/
				BitaAdminBean beanAdmin = new BitaAdminBean();
				beanAdmin.setTipoOp(estatus);
				beanAdmin.setIdTabla(" ");
				beanAdmin.setDato(bean.getCtaCargo());
				beanAdmin.setReferencia(100000);
				beanAdmin.setDato(bean.getCveRastreo());
				if("1".equals(opcion)){
					BitacoraBO.bitacorizaAdmin(req, sess, "EPBA00", beanAdmin, true);
				}else{
					BitacoraBO.bitacorizaAdmin(req, sess, "EPBA01", beanAdmin, true);
				}
				/**TERMINA EWEB_ADMIN_BITACORA**/
			} catch (NumberFormatException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
	}
	/**
	 * Metodo que realiza la concatenacion de los valores a partir del bean
	 * @param bean bean que contiene los parametros a concatenar
	 * @return StringBuffer cadena con los parametros concatenados
	 */
	private StringBuffer concatenaCadena(CepBean bean, String opcion){
		TrxB485BO b485 = new TrxB485BO();
		String cuentaClabe="";
		StringBuffer cadena=new StringBuffer();
		EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOGSERVLET+"MDI_CepServlet::bean.getFecha()::"+bean.getFecha()+"::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOGSERVLET+"CveRastreo()::"+bean.getCveRastreo()+"::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOGSERVLET+"BancoSantander()::"+bean.getBancoSantander()+"::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOGSERVLET+"Banco()::"+bean.getBanco()+"::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOGSERVLET+"CtaAbono()::"+bean.getCtaAbono()+"::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOGSERVLET+"Importe()::"+bean.getImporte()+"::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOGSERVLET+"CtaCargo()::"+bean.getCtaCargo()+"::", EIGlobal.NivelLog.INFO);
		cadena.append(bean.getFecha());
		cadena.append(TranInterConstantes.PIPE);
		cadena.append(TranInterConstantes.TIPO);
		cadena.append(TranInterConstantes.PIPE);
		cadena.append(bean.getCveRastreo());
		cadena.append(TranInterConstantes.PIPE);
		EIGlobal.mensajePorTrace("bitacora::opcion::"+opcion+"::", EIGlobal.NivelLog.INFO);

		if(("1").equals(opcion)){//Flujo de Interbancarias Recibidas
			cadena.append(bean.getBancoSantander());
			cadena.append(TranInterConstantes.PIPE);
			cadena.append(bean.getBanco());
			cadena.append(TranInterConstantes.PIPE);
			cadena.append(bean.getCtaCargo());
			EIGlobal.mensajePorTrace("bitacora::entro en opcion 1::", EIGlobal.NivelLog.INFO);
		}else if(("0").equals(opcion)) {//Flujo de consulta interbancarias SPEI
			cadena.append(bean.getBanco());
			cadena.append(TranInterConstantes.PIPE);
			cadena.append(bean.getBancoSantander());
			cadena.append(TranInterConstantes.PIPE);
//			cuentaClabe=b485.obtenCtaClabe(bean.getCtaCargo());
			EIGlobal.mensajePorTrace("Cuenta clabe::"+bean.getCtaCargo()+"::", EIGlobal.NivelLog.INFO);
			if(UtileriasCep.validaCampo(bean.getCtaCargo())){
				cadena.append(bean.getCtaCargo());
			}else{
				cadena.append("0000000000000000");
			}
			EIGlobal.mensajePorTrace("bitacora::no entro en opcion::", EIGlobal.NivelLog.INFO);
		}else if(("2").equals(opcion)) {		//Flujo de consulta interbancarias SPEI
			EIGlobal.mensajePorTrace("marreguin----------------->MDI_CepServlt (concatenaCadena):-------------------->Ejecutando opcion="+opcion, EIGlobal.NivelLog.INFO);
			cadena.append(bean.getBancoSantander());
			cadena.append(TranInterConstantes.PIPE);
			cadena.append(bean.getBanco());
			cadena.append(TranInterConstantes.PIPE);
			cadena.append(bean.getDepositoABanco());//FSW TCS marreguin 09/12/2016 >>>>>>PREGUNTAR EL CRITERIO DEL VALOR S o N<<<<<
			cadena.append(TranInterConstantes.PIPE);
			EIGlobal.mensajePorTrace("Cuenta clabe::"+bean.getCtaCargo()+"::", EIGlobal.NivelLog.INFO);
			if(UtileriasCep.validaCampo(bean.getCtaCargo())){
				cadena.append(bean.getCtaCargo());
			}else{
				cadena.append("0000000000000000");
			}
			EIGlobal.mensajePorTrace("bitacora::no entro en opcion::", EIGlobal.NivelLog.INFO);
		} else if (("3").equals(opcion)){
			//Flujo de consulta interbancarias Recibidas SPID
			EIGlobal.mensajePorTrace("marreguin----------------->MDI_CepServlt (concatenaCadena):-------------------->Ejecutando opcion="+opcion, EIGlobal.NivelLog.INFO);
			cadena.append(bean.getBanco()); //Primero se coloca la clave del banco ordenante
			cadena.append(TranInterConstantes.PIPE);
			cadena.append(bean.getBancoSantander()); // Segundo se coloca la clave del banco receptor
			cadena.append(TranInterConstantes.PIPE);
			cadena.append(bean.getDepositoABanco());//FSW TCS marreguin 09/12/2016 >>>>>>PREGUNTAR EL CRITERIO DEL VALOR S o N<<<<<
			cadena.append(TranInterConstantes.PIPE);
			EIGlobal.mensajePorTrace("Cuenta clabe::"+bean.getCtaAbono()+"::", EIGlobal.NivelLog.INFO);
			if(UtileriasCep.validaCampo(bean.getCtaAbono())){
				cadena.append(bean.getCtaAbono());	
			}else{
				cadena.append("0000000000000000");
			}
			EIGlobal.mensajePorTrace("bitacora::no entro en opcion::", EIGlobal.NivelLog.INFO);

		}
		cadena.append(TranInterConstantes.PIPE);
		cadena.append(bean.getImporte());

		EIGlobal.mensajePorTrace("marreguin----------------->MDI_CepServlt (concatenaCadena):-------------------->"+cadena, EIGlobal.NivelLog.INFO);
		return cadena;
	}
}