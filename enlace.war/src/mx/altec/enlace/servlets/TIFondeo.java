package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;
import java.rmi.*;
import javax.sql.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.TI_Cuenta;
import mx.altec.enlace.bo.TI_CuentaFondeo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.sql.*;

public class TIFondeo extends BaseServlet
	{

	// --- Entrada por m�todo GET ------------------------------------------------------
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// --- Entrada por m�todo POST -----------------------------------------------------
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// --- MAIN (Este m�todo se ejecuta en cuanto se invoca al servlet) ----------------
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		//parche para que pueda jalar multicheque
		if(getFormParameter(req, "Multicheque") != null)
			{
			consultaMulticheque unServlet = new consultaMulticheque();
			unServlet.init(getServletConfig());
			unServlet.defaultAction(req,res);
			return;
			}

		debug("Entrando a defaultAction()");

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String parAccion = (String)getFormParameter(req, "Accion"); if(parAccion == null) parAccion = "";
		String parTrama = (String)getFormParameter(req, "Trama"); if(parTrama == null) parTrama = "";
		String parCuentas = (String)getFormParameter(req, "Cuentas"); if(parCuentas == null) parCuentas = "";

        //CCB - Jan212004 - Variable para cambio de session para rdbuttons del jsp
        String Agrega = (String)getFormParameter(req, "hdnAgrega");
        TIComunUsd tipoCta = new TIComunUsd();

		int pant = 0;

        /*************CCB Modificacion para los valores de los rdButtons**/
		String moduloTI = (String)sess.getAttribute("moduloTI");
		String accesaMod = "";

		if(moduloTI==null) moduloTI="";

		if (moduloTI.equals("FONDEO"))
			accesaMod = "1";
		else
			accesaMod = "0";

		sess.setAttribute("moduloTI","FONDEO");

		if(moduloTI==null) moduloTI="";

        if(!accesaMod.equals("1"))
        {
        	tipoCta.cuentasContrato(req, res);
        }
        else
        {
            if(Agrega==null) Agrega = "";

            if (Agrega.equals("USD"))
            {
            	sess.setAttribute("ctasUSD", "Existe");
            	sess.setAttribute("ctasMN", "");
            }
            if (Agrega.equals("MN"))
            {
            	sess.setAttribute("ctasUSD", "");
            	sess.setAttribute("ctasMN", "Existe");
            }
        }

		String Divisa="";
		Divisa=(String)sess.getAttribute("Divisa");
		if(Divisa==null) Divisa="";
		if(Agrega==null){Agrega="";Divisa="";}
		if((Divisa != null || !Divisa.trim().equals("")) && moduloTI.equals("FONDEO") && Agrega.equals(""))
			Agrega=Divisa;
		else
			sess.removeAttribute("Divisa");
		sess.setAttribute("Divisa", Agrega);
		System.out.println("El valor de agrega es "+ Agrega);
		System.out.println("El valor de Divisa es "+ Divisa);
        /**************************************************************/


		if(!SesionValida(req,res)) return;

		setMensError("", req);

		if(!session.getFacultad(session.FacConEstr)) setMensError("X-PUsted no tiene facultad para consultar estructuras", req);

		session.setModuloConsultar(IEnlace.MMant_estruc_TI);
		String cuentaTemp = "";
		if(parAccion.equals("")) {parCuentas = consultaArbol(req);}
			System.out.println("Par Trama  "+ parTrama);
		if(parAccion.equals("AGREGA")) {cuentaTemp=parTrama;parCuentas = agregaCuenta(parTrama, parCuentas, req);}
		if(parAccion.equals("ELIMINA")) {cuentaTemp=parTrama;parCuentas = eliminaCuenta(parTrama, parCuentas, req);}
		if(parAccion.equals("EDITA")) {if(session.getFacultad(session.FacCapFond)) {cuentaTemp = parTrama;parTrama = tramaCuenta(parTrama, parCuentas); pant=1;} else setMensError(":-/Usted no tiene facultad para capturar datos", req);}
		if(parAccion.equals("EDITA_MODIFICA")) {parCuentas = tramaModificaCuenta(parTrama, parCuentas, req); pant=1;}
		if(parAccion.equals("EDITA_ANTERIOR")) {parTrama = tramaAnterior(parTrama, parCuentas, req); pant=1;}
		if(parAccion.equals("EDITA_SIGUIENTE")) {parTrama = tramaSiguiente(parTrama, parCuentas, req); pant=1;}
		if(parAccion.equals("COPIA")) {if(session.getFacultad(session.FacCopEstr)) pant=2; else setMensError(":-/Usted no tiene facultad para copiar estructuras", req);}
		if(parAccion.equals("COPIA_CON")) {if(session.getFacultad(session.FacCopEstr)) parCuentas = tramaConcentracion(req);}
		if(parAccion.equals("COPIA_DIS")) {if(session.getFacultad(session.FacCopEstr)) parCuentas = tramaDispersion(req);}
		if(parAccion.equals("BORRA_ARBOL")) {if(session.getFacultad(session.FacActParam)) {borraArbol(req); parCuentas = consultaArbol(req);} else setMensError(":-/Usted no tiene facultad para modificar par�metros", req);}
		if(parAccion.equals("ALTA_ARBOL")) {if(session.getFacultad(session.FacActParam)) altaArbol(parCuentas, req); else setMensError(":-/Usted no tiene facultad para modificar par�metros", req);}
		if(parAccion.equals("MODIFICA_ARBOL")) {if(session.getFacultad(session.FacActParam)) modificaArbol(parCuentas, req); else setMensError(":-/Usted no tiene facultad para modificar par�metros", req);}
		if(parAccion.equals("IMPORTA")) {parCuentas = importaDatos(req, parCuentas);}
		if(getMensError(req).length()>=3) if(getMensError(req).substring(0,3).equals("X-P")) pant = 3;

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		//req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de fondeo autom&aacute;tico","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Fondeo autom&aacute;tico","s29020h", req));
		req.setAttribute("Cuentas",parCuentas);
		req.setAttribute("Mensaje",getMensError(req));
		req.setAttribute("Trama",parTrama);
		req.setAttribute("URL","TIFondeo");

			System.out.println("Cuentas temp:  "+ cuentaTemp);
		/**
         * VSWF - FVC - I
         * 17/Enero/2007
         */
//		utilizar switch
        //TODO BIT Inicia el flujo
        //TODO BIT CU3165 Elimina registros
        //TODO BIT CU3164 Copia estructuras
        //				  Con la opci�n COPIA_CON se copia la estructura de disperci�n a Concentraci�n
        //				  Con la opci�n COPIA_DIS se copia la estructura de disperci�n a Fodeo
        //TODO BIT CU3163 Borra estructura
        //TODO BIT CU3161 Se da de ALTA la Estructura
        //TODO BIT CU3162 Modifica Estructura
        //TODO BIT CU3166 Importa Estructura desde un archivo.
		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
        int opcion = 0;
        if(parAccion.equals(""))
        	opcion = 0;
        if(parAccion.equals("AGREGA"))
        	opcion = 2;
        if(parAccion.equals("ELIMINA"))
        	opcion = 5;
        if(parAccion.equals("EDITA"))
        	opcion = 2;
        if(parAccion.equals("EDITA_MODIFICA"))
        	opcion = -1;
        if(parAccion.equals("EDITA_ANTERIOR"))
        	opcion = -1;
        if(parAccion.equals("EDITA_SIGUIENTE"))
        	opcion = -1;
        if(parAccion.equals("COPIA"))
        	opcion = -1;
        if(parAccion.equals("COPIA_CON"))
        	opcion = 4;
        if(parAccion.equals("COPIA_FON"))
        	opcion = 4;
        if(parAccion.equals("BORRA_ARBOL"))
        	opcion = 3;
        if(parAccion.equals("ALTA_ARBOL"))
        	opcion = 1;
        if(parAccion.equals("IMPORTA"))
        	opcion = 6;
        try {
	        BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
	        BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();

			if(opcion == 0 || opcion == -1){
				System.out.println("VSWF flujo: "+getFormParameter(req, BitaConstants.FLUJO));
				if(getFormParameter(req, BitaConstants.FLUJO) != null)
					bh.incrementaFolioFlujo((String)getFormParameter(req, BitaConstants.FLUJO));
				else
					bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			}
				bt = (BitaTransacBean)bh.llenarBean(bt);

	        bt.setContrato(session.getContractNumber());


	        switch (opcion) {
			case 0:
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_ENTRA);
				break;
			case 1:
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ALTA_ARBOL);
				bt.setServTransTux("TI01");
				bt.setTipoMoneda((String)sess.getAttribute("Divisa"));
				break;

			case 2:
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_MODIF_ARBOL);
				bt.setServTransTux("TI04");
				bt.setTipoMoneda((String)sess.getAttribute("Divisa"));
				System.out.println("Cuenta  "+ cuentaTemp);
				if(parAccion.equals("EDITA")){
				   if(cuentaTemp !=null && !cuentaTemp.equals(""))
					{
					   if(cuentaTemp.length()> 20)
						{
						   bt.setCctaOrig(cuentaTemp.substring(0,20));
						}
						else
						{
						   bt.setCctaOrig(cuentaTemp);
						}
					}
				}

				if(parAccion.equals("AGREGA")){
				   if(cuentaTemp !=null && !cuentaTemp.equals(""))
					{
						if(cuentaTemp.startsWith("@") && cuentaTemp.indexOf('|') > 0 && cuentaTemp.indexOf('|') < 20)
					   		bt.setCctaOrig(cuentaTemp.substring(1,cuentaTemp.indexOf('|')));
					}
				}
				break;

			case 3:
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_BORRAR_ARBOL);
				bt.setServTransTux("TI03");
				bt.setTipoMoneda((String)sess.getAttribute("Divisa"));
				break;

			case 4:
				if(parAccion.equals("COPIA_CON"))
					bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_TRAMA_COPIA_CONCENT);
				else
					bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_TRAMA_COPIA_DISP);
				bt.setTipoMoneda((String)sess.getAttribute("Divisa"));
				bt.setServTransTux("TI02");
				break;

			case 5:
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ELIMINA_REGISTROS_CONCENT);
				bt.setTipoMoneda((String)sess.getAttribute("Divisa"));
				if(cuentaTemp !=null && !cuentaTemp.equals(""))
				{
					if(cuentaTemp.length()> 20)
					{
						bt.setCctaOrig(cuentaTemp.substring(0,20));
					}
					else
					{
						bt.setCctaOrig(cuentaTemp);
					}
				}
				break;

			case 6:
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_IMPORTA_DATOS);
				//bt.setNombreArchivo(new BufferedReader(new StringReader(new String(getFileInBytes(req)))).readLine().trim());
				bt.setNombreArchivo(getFormParameter2(req,"fileName"));
				bt.setTipoMoneda((String)sess.getAttribute("Divisa"));
				break;

			default:
				break;
			}//end switch
	        if(opcion != -1){
	        	BitaHandler.getInstance().insertBitaTransac(bt);
	        }


		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		 }
        /**
         * VSWF - FVC - F
         */

		 /**
		  * VSWF - 07/Ene/2009
		  * Se asigna la variable Divisa a la sesi�n en caso de que sea nula. Esto se agrega para solventar el
		  * problema de que no encuentra la variable document.Forma.tipoEstructura dentro de TIFondeo.jsp
		  */
		 String temp = " " + (String)req.getSession().getAttribute("Divisa");
		 if ( temp.trim().equals("null") )
			 temp = " ";

		 if ( temp.trim().length() == 0 )
			 if ( Divisa.trim().length() > 0 )
			 	 req.getSession().setAttribute("Divisa",Divisa );

		switch(pant)
			{
			case 0: req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de fondeo autom&aacute;tico","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Fondeo autom&aacute;tico","s29020h", req));
					evalTemplate("/jsp/TIFondeo.jsp",req,res); break;
			case 1: req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de fondeo autom&aacute;tico","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Fondeo autom&aacute;tico","s29110h", req));
					evalTemplate("/jsp/TIEditaFondeo.jsp",req,res); break;
			case 2: req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de fondeo autom&aacute;tico","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Fondeo autom&aacute;tico","s29120h", req));
					evalTemplate("/jsp/TICopiaFondeo.jsp",req,res); break;
			case 3: despliegaMensaje(getMensError(req).substring(3),"Consulta y mantenimiento de fondeo autom&aacute;tico","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Fondeo autom&aacute;tico", req, res); break;
			}

		debug("Saliendo de defaultAction()");
		}

	// --- Agrega una cuenta -----------------------------------------------------------
	private String agregaCuenta(String trama, String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a agregaCuenta()");

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String cuenta = trama.substring(1,trama.indexOf("|"));
		if(!session.getFacultad(session.FacCapFond))
			{
			setMensError(":-/Usted no tiene facultad para capturar datos", req);
			debug("Saliendo de agregaCuenta()");
			return tramaCuentas;
			}
		if(existeCuenta(cuenta,tramaCuentas))
			{
			setMensError(":-/La cuenta ya existe dentro del arbol, no puede agregarse nuevamente", req);
			debug("Saliendo de agregaCuenta()");
			return tramaCuentas;
			}
		else
			{
			debug("Saliendo de agregaCuenta()");
			return ordenaArbol(trama + tramaCuentas, req);
			}
		}

	// --- Elimina una cuenta de la trama de cuentas -----------------------------------
	private String eliminaCuenta(String cuenta, String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a eliminaCuenta()");
		Vector cuentas;
		TI_CuentaFondeo cta;
		int a;
		boolean consistente;

		cuentas = creaCuentas(tramaCuentas, req);
		for(a=0;a<cuentas.size();a++)
			{
			cta = (TI_CuentaFondeo)cuentas.get(a);
			if(cta.getNumCta().equals(cuenta)) cuentas.remove(cta);
			}
		do
			{
			consistente = true;
			for(a=0;a<cuentas.size();a++)
				{
				cta = (TI_CuentaFondeo)cuentas.get(a);
				if(cta.getPadre() != null)
					if(cuentas.indexOf(cta.getPadre()) == -1)
						{consistente = false; cuentas.remove(cta);}
				}
			}
		while(!consistente);
		debug("Saliendo de eliminaCuenta()");
		return creaTrama(cuentas);
		}

	// --- Regresa la trama de la cuenta indicada --------------------------------------
	private String tramaCuenta(String cuenta, String tramaCuentas)
		{
		debug("Entrando a tramaCuenta()");
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String tramaFinal="";
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(numCta.equals(cuenta)) tramaFinal = "@" + tramaCta;
				}
			}
		catch(Exception e)
			{debug("Error en tramaCuenta(): " + e.toString());}
		finally
			{
			debug("Saliendo de tramaCuenta()");
			return tramaFinal;
			}
		}

	// --- Edita la trama con la nueva informaci�n -------------------------------------
	private String tramaModificaCuenta(String tramaCuenta, String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a tramaModificaCuenta()");
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String cuenta;
		String tramaFinal="";

		tramaCuenta = tramaCuenta.substring(1);
		cuenta = tramaCuenta.substring(0,tramaCuenta.indexOf("|"));
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(numCta.equals(cuenta)) tramaCta = tramaCuenta;
				tramaFinal += "@" + tramaCta;
				}
			setMensError(":-)Los datos fueron modificados", req);
			}
		catch(Exception e)
			{debug("Error en tramaModificaCuenta(): " + e.toString());}
		finally
			{
			debug("Saliendo de tramaModificaCuenta()");
			return tramaFinal;
			}
		}

	// --- Regresa la trama de la cuenta siguiente a la indicada -----------------------
	private String tramaSiguiente(String cuenta, String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a tramaSiguiente()");
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String ctaSiguiente;
		String cta;
		boolean encontrado;

		encontrado = false;
		cta = "";
		ctaSiguiente = "";
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens() && !encontrado)
				{
				tramaCta = tokens.nextToken();
				ctaSiguiente = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(cta.equals(cuenta)) encontrado = true; else cta = ctaSiguiente;
				}
			if(!encontrado) {setMensError(":-/no existe cuenta siguiente", req); ctaSiguiente = cuenta;}
			}
		catch(Exception e)
			{
			debug("Error en tramaSiguiente(): " + e.toString());
			ctaSiguiente = cuenta;
			}
		finally
			{
			debug("Saliendo de tramaSiguiente()");
			return tramaCuenta(ctaSiguiente,tramaCuentas);
			}
		}

	// --- Regresa la trama de la cuenta siguiente a la indicada -----------------------
	private String tramaAnterior(String cuenta, String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a tramaAnterior()");
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String ctaAnterior;
		String cta;
		boolean encontrado;

		encontrado = false;
		cta = "";
		ctaAnterior = "";
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens() && !encontrado)
				{
				tramaCta = tokens.nextToken();
				cta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(cta.equals(cuenta)) encontrado = true; else ctaAnterior = cta;
				}
			if(!encontrado || ctaAnterior.equals(""))
				{setMensError(":-/no existe cuenta anterior", req); ctaAnterior = cuenta;}
			}
		catch(Exception e)
			{
			debug("Error en tramaAnterior(): " + e.toString());
			ctaAnterior = cuenta;
			}
		finally
			{
			debug("Saliendo de tramaAnterior()");
			return tramaCuenta(ctaAnterior,tramaCuentas);
			}
		}

	// --- Indica si una cuenta se encuentra dentro de una trama -----------------------
	private boolean existeCuenta(String cuenta, String tramaCuentas)
		{
		debug("Entrando a existeCuenta()");
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String tramaFinal="";
		boolean existe = false;
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(numCta.equals(cuenta)) {existe = true; break;}
				}
			}
		catch(Exception e)
			{debug("Error en existeCuenta(): " + e.toString());}
		finally
			{
			debug("Saliendo de existeCuenta()");
			return existe;
			}
		}

	// --- Ordena las cuentas seg�n el arbol -------------------------------------------
	private String ordenaArbol(String tramaEntrada, HttpServletRequest req)
		{
		debug("Entrando a ordenaArbol()");
		String tramaSalida;
		Vector cuentas, ctasOrden;
		TI_CuentaFondeo ctaAux;
		int a, b;

		b = 1;
		cuentas = creaCuentas(tramaEntrada, req);
		ctasOrden = new Vector();
		while(cuentas.size() > 0)
			{
			for(a=0;a<cuentas.size();a++)
				{
				ctaAux = ((TI_CuentaFondeo)cuentas.get(a));
				if(ctaAux.nivel() == b) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
				}
			b++;
			}
		cuentas = ctasOrden;
		ctasOrden = new Vector();
		for(a=0;a<cuentas.size();a++)
			{
			ctaAux = ((TI_CuentaFondeo)cuentas.get(a));
			if(ctaAux.nivel() == 1) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
			}
		while(cuentas.size() > 0)
			{
			ctaAux = ((TI_CuentaFondeo)cuentas.get(0));
			ctasOrden.add(ctasOrden.indexOf(ctaAux.getPadre()) + 1,ctaAux);
			cuentas.remove(ctaAux);
			}
		cuentas = ctasOrden;
		tramaSalida = "";
		for(a=0;a<cuentas.size();a++) tramaSalida += ((TI_CuentaFondeo)cuentas.get(a)).trama();

		debug("Saliendo de ordenaArbol()");
		return tramaSalida;
		}

	// --- Regresa un vector de cuentas a partir de una trama --------------------------
	public Vector creaCuentas(String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a creaCuentas()");
		Vector cuentas = null;
		StringTokenizer tokens;
		TI_CuentaFondeo ctaAux1, ctaAux2;
		int a, b;

		try
			{
			cuentas = new Vector();
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				cuentas.add(TI_CuentaFondeo.creaCuentaFondeo("@" + tokens.nextToken()));
				for(a=0;a<cuentas.size();a++)
					{
					ctaAux1 = (TI_CuentaFondeo)cuentas.get(a);
					if(!ctaAux1.posiblePadre.equals(""))
						for(b=0;b<cuentas.size();b++)
							{
							ctaAux2 = (TI_CuentaFondeo)cuentas.get(b);
							if(ctaAux2.getNumCta().equals(ctaAux1.posiblePadre))
								{
								ctaAux1.setPadre(ctaAux2);
								b=cuentas.size();
								}
							}
					}
				}
			}
		catch(Exception e)
			{
			debug("Error en creaCuentas(): " + e.toString());
			cuentas = new Vector();
			setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
			}
		finally
			{
			debug("Saliendo de creaCuentas()");
			return cuentas;
			}
		}

	// --- Regresa una trama a partir de un vector de cuentas --------------------------
	public String creaTrama(Vector vectorCuentas)
		{
		debug("Entrando a creaTrama()");
		StringBuffer regreso = new StringBuffer("");
		for(int a=0;a<vectorCuentas.size();a++)
			{regreso.append(((TI_CuentaFondeo)vectorCuentas.get(a)).trama());}
		debug("Saliendo de creaTrama()");
		return regreso.toString();
		}

	// --- Indica si las cuentas de una estructura tienen todos sus datos capturados ---
	private boolean tramaLista(String trama, HttpServletRequest req)
		{
		debug("Entrando a tramaLista()");
		Vector cuentas = creaCuentas(trama, req);
		TI_CuentaFondeo cuenta;
		boolean retorno = true;
		int num;

		for(num=0;num<cuentas.size() && retorno;num++)
			{
			cuenta = (TI_CuentaFondeo)cuentas.get(num);
			if(cuenta.getFondeo() == 0 && cuenta.nivel() != 1) retorno = false;
			}
		debug("Saliendo de tramaLista() retorno = " + retorno);
		return retorno;
		}

	// --- Regresa un arreglo con las cuentas del usuario ------------------------------
	private String[][] arregloCuentas(HttpServletRequest req)
		{
		debug("Entrando a arregloCuentas()");
		int a, b, car;
		String trama = "", retorno[][] = null;
		Vector cuentasR;
		TI_CuentaFondeo cuenta = null;

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		try
			{
			String arregloCuentas[][] = (String[][])sess.getAttribute("ArregloCuentas");
			if(arregloCuentas == null)
				{
				trama = enviaTuxedo(" ", req); //<-----------------------------------------------------------------------------------------
				cuentasR = datosCuentas(trama, req);
				retorno = new String[cuentasR.size()][3];
				for(a=0;a<retorno.length;a++)
					{
					cuenta = (TI_CuentaFondeo)cuentasR.get(a);
					retorno[a][0] = cuenta.getNumCta();
					retorno[a][1] = cuenta.getDescripcion();
					retorno[a][2] = ""+cuenta.getTipo();
					}
				sess.setAttribute("ArregloCuentas",retorno);
				}
			else
				retorno = arregloCuentas;
			}
		catch(Exception e)
			{
			setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
			debug("Error en arregloCuentas():" + e.toString() + " -- trama devuelta: " + trama);
			retorno = null;
			}
		finally
			{
			debug("Saliendo de arregloCuentas()");
			return retorno;
			}
		}

	// --- Lee el archivo especificado y obtiene datos de cuentas ----------------------
	private Vector datosCuentas(String archivo, HttpServletRequest req)
		{
		debug("Entrando a datosCuentas()");
		int car;
		FileReader fuente;
		StringBuffer buffer;
		StringTokenizer separaTrama;
		Vector datos = null;
		TI_CuentaFondeo cuenta;

		try
			{
			fuente = new FileReader(archivo);
			buffer = new StringBuffer("");
			while((car = fuente.read()) != -1) {if(car != 10 && car != 13) buffer.append("" + (char)car);}
			fuente.close();
			while((car = buffer.toString().indexOf("@@")) != -1) buffer.insert(car+1," ");
			separaTrama = new StringTokenizer(buffer.toString(),"@");
			datos = new Vector();
			while(separaTrama.hasMoreTokens())
				{
				cuenta = new TI_CuentaFondeo(separaTrama.nextToken());
				separaTrama.nextToken();
				cuenta.setDescripcion(separaTrama.nextToken());
				separaTrama.nextToken(); separaTrama.nextToken(); separaTrama.nextToken();
				cuenta.setTipo(Integer.parseInt(separaTrama.nextToken()));
				separaTrama.nextToken();
				datos.add(cuenta);
				}
			}
		catch(Exception e)
			{
			datos = null;
			setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
			debug("Error en datosCuentas(): " + e.toString());
			}
		finally
			{
			debug("Saliendo de datosCuentas()");
			return datos;
			}
		}

	// --- Ejecuta una consulta a Tuxedo y regresa el aviso de Tuxedo ------------------
	private String enviaTuxedo(String tipo, HttpServletRequest req)
		{
		debug("Entrando a enviaTuxedo()");
		String trama = "";
		Hashtable ht;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		boolean criterio = false;

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

// ----------------------------------------------------- Secci�n de archivo remoto -----
			if(tipo.equals("ALTA") || tipo.equals("MODIFICACION"))
				{
				if(archRemoto.copiaLocalARemoto(session.getUserID8()+".tei"))
					debug("Copia remota realizada (local a Tuxedo)");
				else
					{
					debug("Copia remota NO realizada (local a Tuxedo)");
					setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
					}
				}
// -------------------------------------------------------------------------------------
	   //CCB - Jan212004 - Variable para trama para manejo de USD / MN
       String Divisa = getFormParameter(req, "hdnDivisa");
       if(Divisa==null && tipo.equals("CONSULTA")){
			Divisa="ALL";
  	   }
		   /*if (sess.getAttribute("ctasUSD").equals("Existe"))
		   {
		  	 if(sess.getAttribute("ctasMN").equals("Existe"))
		  	 {
		 		Divisa = "MN";
		  	 }
		  	 else
		  	 {
		   		Divisa = "USD";
		  	 }
		   }
		   else
		   {
		  	 Divisa="MN";
		   }*/

		System.out.println("DEntro de enviaTuxedo el valor de la divisa es :" + Divisa + " y el tipo es : " + tipo);

		if(tipo.equals("ALTA"))
			{trama = "1EWEB|"+session.getUserID8()+"|TI01|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tei@|"+Divisa+"|";}
		else if(tipo.equals("BAJA"))
			{trama = "1EWEB|"+session.getUserID8()+"|TI03|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@F@|"+Divisa+"|";}
		else if(tipo.equals("CONSULTA"))
			{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@F@|"+Divisa+"|";}
		else if(tipo.equals("MODIFICACION"))
			{trama = "1EWEB|"+session.getUserID8()+"|TI04|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tei@|"+Divisa+"|";}
		else if(tipo.equals("CONCENTRACION"))
			{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@C@|"+Divisa+"|";}
		else if(tipo.equals("DISPERSION"))
			{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@|"+Divisa+"|";}
		else
			//{trama = session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID()+".ctas" + "|6@|" + tipo + "| | | "; criterio = true;}
			{trama = session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".ctas" + "|6@|" + tipo + "| | | |"+Divisa+"|"; criterio = true;}
		try
			{
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			if(!criterio) {ht = tuxedo.web_red(trama);} else {ht = tuxedo.cuentas_modulo(trama);}
			trama = (String)ht.get("BUFFER");
			debug("M�todo usado: " + ((!criterio)?"web_red()":"cuentas_modulo()"));
			debug("Tuxedo regresa en BUFFER: " + trama);
			debug("Tuxedo regresa en COD_ERROR: " + (String)ht.get("COD_ERROR"));
			if(criterio)
				if (!((String)ht.get("COD_ERROR")).equals("CCTA0000"))
					{
					criterio = false;
					trama = (String)ht.get("COD_ERROR");
					//setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde");
					//debug("Error en consulta a Tuxedo");
					}
				else
					trama = session.getUserID8() + ".ctas";
// ----------------------------------------------------- Secci�n de archivo remoto -----
			if(tipo.equals("CONSULTA") || tipo.equals("CONCENTRACION") || tipo.equals("DISPERSION") || criterio)
				{
				trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
				if(archRemoto.copia(trama))
					{
					debug("Copia remota realizada (Tuxedo a local)");
					trama = Global.DIRECTORIO_LOCAL + "/" +trama;
					}
				else
					{
					debug("Copia remota NO realizada (Tuxedo a local)");
					setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
					}
				}
// -------------------------------------------------------------------------------------
			}
		catch(Exception e)
			{
			debug("Error intentando conectar a Tuxedo: " + e.toString() + " <---> " + e.getMessage() +  " <---> " + e.getLocalizedMessage());
			setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
			}
		finally
			{
			debug("Saliendo de enviaTuxedo()");
			System.out.println("La trama despues de enviaTuxedo es:  " + trama);
			return trama;
			}
		}

	// --- Construye el archivo para enviar a Tuxedo -----------------------------------
	private boolean construyeArchivo(String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a construyeArchivo()");
		boolean noError = true;
		FileWriter archivo;
		StringBuffer buffer, tramaHijoPadre, tramaCuentasHoras;
		Vector cuentas;
		int numHijoPadre, numCuentasHoras, a, b;
		TI_CuentaFondeo cuenta;

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		try
			{
			cuentas = creaCuentas(tramaCuentas, req);
			buffer = new StringBuffer("" + session.getContractNumber() + "@F@");
			numHijoPadre = 0;
			numCuentasHoras = 0;
			tramaHijoPadre = new StringBuffer("");
			tramaCuentasHoras = new StringBuffer("");
			for(b=0;b<cuentas.size();b++)
				{
				cuenta = (TI_CuentaFondeo)cuentas.get(b);
				numHijoPadre++;
				tramaHijoPadre.append(cuenta.getNumCta() + "@"
								+	((cuenta.getPadre() == null)?"0":cuenta.getPadre().getNumCta()) + "@"
								+ cuenta.getDescripcion() + "@");
				if(cuenta.nivel() > 1)
					{
					numCuentasHoras++;
					tramaCuentasHoras.append(session.getContractNumber() + "@"
							+ cuenta.getNumCta() + "@"
							+ cuenta.getImporte() + "@"
							//CCB Se cambio la D por la B para el fondeo doble a petici�n del usuario
							//+ ((cuenta.getFondeo() == 1)?"U":"D") + "@"
							+ ((cuenta.getFondeo() == 1)?"U":"B") + "@"
							+ cuenta.nivel() + "@");
					}
				}
			buffer.append("" + numHijoPadre + "@" + tramaHijoPadre
							+ numCuentasHoras + "@" + tramaCuentasHoras);
			buffer.insert(0," " + buffer.toString().length() + "@\n");
			buffer.append("\n");
			archivo = new FileWriter(IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tei");
			archivo.write("" + buffer);
			archivo.close();
			}
		catch(Exception e)
			{
			debug("Error intentando construir archivo: " + e.toString() + " <---> " + e.getMessage());
			noError = false;
			}
		finally
			{
			debug("Saliendo de construyeArchivo()");
			return noError;
			}

		}

	// --- Lee el archivo de tuxedo y regresa una trama de cuentas ---------------------
	private String leeArchivo(String nombreArchivo, HttpServletRequest req)
		{
		debug("Entrando a leeArchivo()");
		StringBuffer tramaT = new StringBuffer(""), tramaR = new StringBuffer("");
		StringTokenizer tokens;
		FileReader archivo;
		String tipoArbol, strAux;
		Vector cuentas;
		TI_CuentaFondeo cuenta;
		int car, a, b;
		String Divisa="";

		try
			{
			// Se lee del archivo al StringBuffer
			archivo = new FileReader(nombreArchivo);
			car = 0; while(car != -1) {car = archivo.read(); tramaT.append("" + (char)car);}
			archivo.close();

			// Se "corrige" la trama
			while((a = tramaT.toString().indexOf("@@")) != -1) tramaT.insert(a+1," ");

			// Se arma la trama mientras se verifica su integridad
			tipoArbol = "";
			System.out.println("La tramaT entrando a leeArchivo es : " + tramaT.toString());
			if(!tramaT.toString().substring(17,25).equals("TEIN0000"))
				{tramaR = tramaT; tramaR.insert(0,"<<ERROR>>@");}
			else
				{
				tokens = new StringTokenizer(tramaT.toString(),"@");
				cuentas = new Vector();
				for(a=0;a<3;a++) tokens.nextToken();
				Divisa = tokens.nextToken();
				car = Integer.parseInt(tokens.nextToken());
				System.out.println("////////////////////////////////////// EL car en el leeArchivo " + car);
				System.out.println("////////////////////////////////////// La divisa en el leeArchivo " + Divisa);
				HttpSession ses = req.getSession();
				if(Divisa!=null && Divisa.trim()!=""){
				  ses.setAttribute("Divisa",Divisa);
				}
				//req.setAttribute("Divisa",Divisa);
				for(a=0;a<car;a++)
					{
					tramaR.append("@" + tokens.nextToken() + "|");
					tokens.nextToken();
					tipoArbol = tokens.nextToken();
					strAux = tokens.nextToken();
					tramaR.append((strAux.equals("0"))?" ":strAux);
					tramaR.append("|" + tokens.nextToken() + "|0.0|0");
					}
				if(!tipoArbol.equals("F"))
					{tramaR.insert(0,"TRAMA_" + tipoArbol + "@");}
				else
					{
					cuentas = creaCuentas(tramaR.toString(), req);
					car = Integer.parseInt(tokens.nextToken());
					for(a=0;a<car;a++)
						{
						strAux = tokens.nextToken();
						for(b=0; !(cuenta = (TI_CuentaFondeo)cuentas.get(b)).getNumCta().equals(strAux);b++);
							{
							tokens.nextToken();
							cuenta.setImporte(Double.parseDouble(tokens.nextToken()));
							cuenta.setFondeo((tokens.nextToken().equals("U"))?1:2);
							tokens.nextToken();
							}
						}
					tramaR = new StringBuffer("");
					for(a=0;a<cuentas.size();a++) tramaR.append(((TI_CuentaFondeo)cuentas.get(a)).trama());
					tramaR.insert(0,"TRAMA_OK@");
					}
				}
			}
		catch(Exception e)
			{
			debug("Error en leeArchivo(): " + e.toString());
			tramaR = new StringBuffer("<<ERROR>>@trama en el archivo: " + tramaT.toString());
			}
		finally
			{
			debug("Saliendo de leeArchivo()");
			return tramaR.toString();
			}
		}

	// --- Borra el arbol de la base de datos ------------------------------------------
	private void borraArbol(HttpServletRequest req)
		{
		debug("Entrando a borraArbol()");
		String resultado = enviaTuxedo("BAJA", req);
		if(resultado.substring(16,24).equals("TEIN0000"))
			{setMensError("X-PLa estructura fue borrada", req);}
		else if(resultado.substring(16,20).equals("TEIN"))
			{setMensError(":-/" + resultado.substring(25,resultado.length()-1), req);}
		else
			{setMensError(":-/" + resultado.substring(16,resultado.length()), req);}
		debug("Saliendo de borraArbol()");
		}

	// --- Da de alta el arbol en la base de datos -------------------------------------
	private void altaArbol(String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a altaArbol()");
		String resultadoTuxedo;
		boolean archivoCreado;


		if(!validaCuentas(tramaCuentas, req))
    	{setMensError(":-/Error H701:Cuenta Inv&aacute;lida", req);}
		else if(!tramaLista(tramaCuentas, req))
			{setMensError(":-/No se puede guardar la estructura, Existen<BR>cuentas a las que se deben agregar datos", req);}
		else if(hayHojasEnPrimerNivel(creaCuentas(tramaCuentas, req)))
			{setMensError(":-/No se puede guardar la estructura, no puede haber cuentas de primer nivel sin hijas", req);}
		else
			{
			archivoCreado = construyeArchivo(tramaCuentas, req);
			if(!archivoCreado)
				{setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);}
			else
				{
				resultadoTuxedo = enviaTuxedo("ALTA",req);
				if(resultadoTuxedo.substring(16,24).equals("TEIN0000"))
					{setMensError("X-PLa estructura fue guardada", req);}
				else if(resultadoTuxedo.substring(16,20).equals("TEIN"))
					{setMensError(":-/" +resultadoTuxedo.substring(25,resultadoTuxedo.length()-1), req);}
				else
					{setMensError(":-/" +resultadoTuxedo.substring(16,resultadoTuxedo.length()), req);}
				}
			}
		debug("Saliendo de altaArbol()");
		}

	// --- Modifica el arbol en la base de datos ---------------------------------------
	private void modificaArbol(String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a modificaArbol()");
		String resultadoTuxedo;
		boolean archivoCreado;

		if(!validaCuentas(tramaCuentas, req))
    	{setMensError(":-/Error H701:Cuenta Inv&aacute;lida", req);}
		else if(!tramaLista(tramaCuentas, req))
			{setMensError(":-/No se puede guardar la estructura, Existen\ncuentas a las que se deben agregar datos", req);}
		else if(hayHojasEnPrimerNivel(creaCuentas(tramaCuentas, req)))
			{setMensError(":-/No se puede guardar la estructura, no puede haber cuentas de primer nivel sin hijas", req);}
		else
			{
			archivoCreado = construyeArchivo(tramaCuentas, req);
			if(!archivoCreado)
				{setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento,<BR> intente m&aacute;s tarde", req);}
			else
				{
				resultadoTuxedo = enviaTuxedo("MODIFICACION", req);
				if(resultadoTuxedo.substring(16,24).equals("TEIN0000"))
					{setMensError("X-PLa estructura fue guardada", req);}
				else if(resultadoTuxedo.substring(16,20).equals("TEIN"))
					{setMensError(":-/" +resultadoTuxedo.substring(25,resultadoTuxedo.length()-1), req);}
				else
					{setMensError(":-/" +resultadoTuxedo.substring(16,resultadoTuxedo.length()), req);}
				}
			}
		debug("Saliendo de modificaArbol()");
		}

	// --- Regresa una trama con el arbol ----------------------------------------------
	private String consultaArbol(HttpServletRequest req)
		{
		debug("Entrando a consultaArbol()");
		String[][] arregloCuentas;
		String retornoDisp, trama, aviso;
		Vector cuentas;
		TI_CuentaFondeo cuenta;
		int a, b;

		arregloCuentas = arregloCuentas(req); if (arregloCuentas == null) return "";
		retornoDisp = enviaTuxedo("CONSULTA", req);
		trama = leeArchivo(retornoDisp, req);
		System.out.println("La trama entrando a consulta archivo es:  " + trama);
		if(trama.equals("TRAMA_@")) trama = "TRAMA_OK@";
		aviso = trama.substring(0,trama.indexOf("@"));

		System.out.println("El aviso es :  " + aviso);

		trama = trama.substring(trama.indexOf("@") + 1);
		System.out.println("La trama despues del substring es :  " + trama);
		if(!aviso.equals("TRAMA_OK"))
			{
			if(trama.substring(17,21).equals("TEIN"))
				{setMensError("X-P" + trama.substring(25, trama.length()), req);}
			else
				{setMensError("X-P" + trama.substring(16, trama.length()-1), req);}
			setMensError("X-P" + trama, req);
			debug("Error en consultaArbol() -- trama devuelta: " + trama);
			debug("Saliendo de consultaArbol()");
			return "";
			}
		debug("Saliendo de consultaArbol()");
		System.out.println("La TRAMA despues de consultaArbol es ......." + trama);
		return ordenaArbol(trama, req);
		}

	// --- Regresa una trama con el arbol de concentraci�n -----------------------------
	private String tramaConcentracion(HttpServletRequest req)
		{
		debug("Entrando a tramaConcentracion()");
		String retornoTuxedo, trama, aviso;
		String[][] arregloCuentas;
		Vector cuentas;
		TI_CuentaFondeo cuenta;
		int a, b;

		retornoTuxedo = enviaTuxedo("CONCENTRACION", req); trama = leeArchivo(retornoTuxedo, req);
		if(trama.equals("TRAMA_@")) trama = "TRAMA_C@";
		aviso = trama.substring(0,trama.indexOf("@"));
		trama = trama.substring(trama.indexOf("@") + 1);
		if(!aviso.equals("TRAMA_C"))
			{
			if(trama.substring(17,21).equals("TEIN"))
				{setMensError("X-P" + trama.substring(25, trama.length()), req);}
			else
				{setMensError("X-P" + trama.substring(16, trama.length()-1), req);}
			debug("Error en tramaConcentracion() -- trama devuelta: " + trama);
			debug("Saliendo de tramaConcentracion()");
			return "";
			}

		cuentas = creaCuentas(trama, req);
		arregloCuentas = arregloCuentas(req); if (arregloCuentas == null) return "";
		trama = "";
		for(a=0;a<cuentas.size();a++)
			{
			cuenta = (TI_CuentaFondeo)cuentas.get(a);
			for(b=0;b < arregloCuentas.length && !arregloCuentas[b][0].equals(cuenta.getNumCta());b++);
			cuenta.setTipo(Integer.parseInt((b != arregloCuentas.length)?arregloCuentas[b][2]:"0"));
			trama += cuenta.trama();
			}

		debug("Saliendo de tramaConcentracion()");
		return ordenaArbol(trama, req);
		}

	// --- Regresa una trama con el arbol de dispersi�n --------------------------------
	private String tramaDispersion(HttpServletRequest req)
		{
		debug("Entrando a tramaDispersion()");
		String retornoTuxedo, trama, aviso;
		String[][] arregloCuentas;
		Vector cuentas;
		TI_CuentaFondeo cuenta;
		int a, b;

		retornoTuxedo = enviaTuxedo("DISPERSION", req); trama = leeArchivo(retornoTuxedo, req);
		if(trama.equals("TRAMA_@")) trama = "TRAMA_D@";
		aviso = trama.substring(0,trama.indexOf("@"));
		trama = trama.substring(trama.indexOf("@") + 1);
		if(!aviso.equals("TRAMA_D"))
			{
			if(trama.substring(17,21).equals("TEIN"))
				{setMensError("X-P" + trama.substring(25,trama.length()), req);}
			else
				{setMensError("X-P" + trama.substring(16,trama.length()-1), req);}
			debug("Error en tramaDispersion() -- trama devuelta: " + trama);
			debug("Saliendo de tramaDispersion()");
			return "";
			}

		cuentas = creaCuentas(trama, req);
		arregloCuentas = arregloCuentas(req); if (arregloCuentas == null) return "";
		trama = "";
		for(a=0;a<cuentas.size();a++)
			{
			cuenta = (TI_CuentaFondeo)cuentas.get(a);
			for(b=0;b < arregloCuentas.length && !arregloCuentas[b][0].equals(cuenta.getNumCta());b++);
			cuenta.setTipo(Integer.parseInt((b != arregloCuentas.length)?arregloCuentas[b][2]:"0"));
			trama += cuenta.trama();
			}

		debug("Saliendo de tramaDispersion()");
		return ordenaArbol(trama, req);
		}

	// --- Indica si una cuenta es hoja o padre ----------------------------------------
	public boolean esHoja(TI_CuentaFondeo cuenta, Vector cuentas)
		{
		debug("Entrando a esHoja()");
		TI_Cuenta ctaActual;
		boolean hoja;

		hoja = true;
		for(int a=0;a<cuentas.size() && hoja;a++)
			{
			ctaActual = ((TI_CuentaFondeo)cuentas.get(a)).getPadre();
			if(ctaActual != null) if(ctaActual.equals(cuenta)) hoja = false;
			}
		debug("Saliendo de esHoja()");
		return hoja;
		}

	// --- Indica si no hay hojas en el nivel 1 del arbol ------------------------------
	private boolean hayHojasEnPrimerNivel(Vector cuentas)
		{
		debug("Entrando a hayHojasEnPrimerNivel()");
		TI_CuentaFondeo ctaActual;
		boolean hoja;

		hoja = false;
		for(int a=0;a<cuentas.size() && !hoja;a++)
			{
			ctaActual = (TI_CuentaFondeo)cuentas.get(a);
			if(ctaActual.nivel() == 1 && esHoja(ctaActual,cuentas)) hoja = true;
			}
		debug("Saliendo de hayHojasEnPrimerNivel()");
		return hoja;
		}

	// --- Importa los datos del archivo a importar seleccionado por el usuario --------
	private String importaDatos(HttpServletRequest req, String original)
		{
		debug("Entrando a importaDatos()");
				
		int a, numLinea = 0;
		boolean errorLinea, errorGral;
		String archivo, linea, token, trama = "",
			htmlLinea, htmlCabecera, htmlCuerpo, htmlPie,
			otroArchivo,
			nuevoNombre[];
		BufferedReader flujo;
		Vector cuentas;
		TI_CuentaFondeo cuenta, nuevoPadre;

		htmlCabecera = "<html><head><title>Estatus</title><link rel='stylesheet' href='/EnlaceMig/" +
				"consultas.css' type='text/css'></head><body bgcolor='white'><form><table border=0" +
				" width=420 class='textabdatcla' align=center><tr><th class='tittabdat'>informaci&" +
				"oacute;n del Archivo Importado</th></tr><tr><td class='tabmovtex1' align=center><" +
				"table border=0 align=center><tr><td class='tabmovtex1' align=center width=><img s" +
				"rc='/gifs/EnlaceMig/gic25060.gif'></td><td class='tabmovtex1' align='center'><br>" +
				"No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los" +
				" siguientes errores.<br><br></td></tr></table></td></tr><tr><td class='tabmovtex1'>";
		htmlCuerpo = "";
		htmlPie = "<br></td></tr></table><table border=0 align=center><tr><td align=center><br><a " +
				"href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=" +
				"0></a></td></tr></table></form></body></html>";
		errorGral = false;

		//Se crea una lista con los n�meros de cuenta v�lidos para el contrato
		otroArchivo = enviaTuxedo(" ", req); // <-------------------------------------------------------------------------------------------
		cuentas = new Vector();

		try
			{
			// Se lee el archivo, y se procesa para que sus datos se lean desde un BufferedReader
			archivo = new String(getFileInBytes(req));
			flujo = new BufferedReader(new StringReader(archivo));
			numLinea++;

			// Se obtiene y se valida la cabecera del archivo
			linea = flujo.readLine().trim();
			if(linea.length() != 17)
				{htmlCuerpo = "No cumple con el Formato."; throw new Exception();}
			if(!linea.substring(16,17).equals("F"))
				{htmlCuerpo = "El arbol no es de fondeo autom&aacute;tico."; throw new Exception();}

			// Se valida y se va obteniendo la info de cada l�nea
			while((linea = flujo.readLine()) != null)
				{
				if(linea.trim().equals("")) continue;
				numLinea ++;
				errorLinea = false;
				htmlLinea = "<br><b>L&iacute;nea " + numLinea + "</b>";

				if(!linea.substring(0,16).trim().equals("vac�o"))
					{
					cuenta = new TI_CuentaFondeo(linea.substring(0,16).trim());
					if(cuentas.indexOf(cuenta)== -1) cuentas.add(cuenta);
					}

				cuenta = new TI_CuentaFondeo(linea.substring(16,32).trim());
				if((a = cuentas.indexOf(cuenta)) != -1) {cuentas.remove(a);}

				if((a = cuentas.indexOf(cuenta)) == -1)
					{
					cuenta.posiblePadre = linea.substring(0,16).trim();
					cuenta.setDescripcion(linea.substring(34,74).trim());
					cuenta.setFondeo((linea.substring(74,75).equals("U"))?1:((linea.substring(74,75).equals("B"))?2:0));
					cuenta.setImporte(Double.parseDouble(linea.substring(75).trim()));
					if(cuenta.posiblePadre.equals("vac�o")) cuenta.posiblePadre = "";
					cuentas.add(cuenta);
					}

				nuevoNombre = BuscandoCtaInteg(cuenta.getNumCta(),otroArchivo,"6@");
				if(nuevoNombre == null)
					{
					errorLinea = true;
					htmlLinea +="<br><DD><LI>La cuenta no existe: " + cuenta.getNumCta();
					}

				if(cuenta.posiblePadre.equals(""))
					{nuevoNombre = new String[2]; nuevoNombre[1] = "";}
				else
					nuevoNombre = BuscandoCtaInteg(cuenta.getNumCta(),otroArchivo,"6@");

				if(nuevoNombre == null)
					{
					errorLinea = true;
					htmlLinea +="<br><DD><LI>La cuenta padre no existe: " + cuenta.posiblePadre;
					}

				if(cuenta.getImporte()<0)
					{
					errorLinea = true;
					htmlLinea +="<br><DD><LI>El importe de dispersi&oacute;n no es correcto: " + cuenta.getImporte();
					}

				if(errorLinea) {errorGral = true; htmlCuerpo += htmlLinea;}
				}

			flujo.close();

			if(errorGral)
				{
				trama = original;
				setMensError("X-O" + htmlCabecera + htmlCuerpo + htmlPie, req);
				}
			else
				{
				for(a=0;a<cuentas.size();a++)
					{
					cuenta = (TI_CuentaFondeo)cuentas.get(a);
					nuevoNombre = BuscandoCtaInteg(cuenta.getNumCta(),otroArchivo,"6@");
					nuevoPadre = TI_CuentaFondeo.copiaCuenta(nuevoNombre[1], cuenta);
					cuentas.set(a,nuevoPadre);
					for(int x=0;x<cuentas.size();x++)
						if(((TI_CuentaFondeo)cuentas.get(x)).posiblePadre.equals(cuenta.getNumCta()))
							((TI_CuentaFondeo)cuentas.get(x)).posiblePadre = nuevoPadre.getNumCta();
					}

				trama = "";
				while(cuentas.size()>0) trama += ((TI_CuentaFondeo)cuentas.remove(0)).tramaPosible();
				setMensError(":-)Archivo importado satisfactoriamente.", req);
				}
			}
		catch(Exception e)
			{
			debug("error en importaDatos(): " + e.toString());
			if(htmlCuerpo.equals("")) htmlCuerpo = "No cumple con el Formato.";
			htmlCuerpo = "<br><b>Linea: " + numLinea + ".</b> <font color=red>" + htmlCuerpo + "</font>";
			trama = original;
			setMensError("X-O" + htmlCabecera + htmlCuerpo + htmlPie, req);
			}
		finally
			{
			debug("Saliendo de importaDatos()");
			return ordenaArbol(trama, req);
			}
		}

	//--- Toma el mensaje de error de la sesi�n ----------------------------------------
	private String getMensError(HttpServletRequest req)
		{
		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String mensError = (String)sess.getAttribute("TI_MensError"); if(mensError == null) mensError = "";
		return mensError;
		}

	//--- Indica el mensaje de error para guardar en la sesi�n -------------------------
	private void setMensError(String mensError, HttpServletRequest req)
		{
		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		sess.setAttribute("TI_MensError",mensError);
		}

	//--- Mensaje de debugeo -----------------------------------------------------------
	private void debug(String mensaje) {EIGlobal.mensajePorTrace("<DEBUG TIFondeo> " + mensaje, EIGlobal.NivelLog.INFO);}

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{

		// parche de Ram�n Tejada
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal( session , getServletContext() , this );

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		//request.setAttribute( "URL", ((request.getParameter("Accion") == null)?"javascript:history.go(-1);":"TIFondeo?Accion=&") );
		request.setAttribute( "URL", ((getFormParameter(request,"Accion") == null)?"javascript:history.go(-1);":"TIFondeo?Accion=&"));

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s29020h", request));
		sess.setAttribute("Encabezado",CreaEncabezado( param1, param2, "s29020h", request));
		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}


	 private boolean validaCuentas(String trama, HttpServletRequest req)
     { debug("trama-->"+trama);
     boolean retorno=true;


     BaseResource session = (BaseResource) req.getSession().getAttribute("session");
         ValidaCuentaContrato valCtas = null;
         try
          {
       	//YHG Agrega validacion de cuentas
       	  valCtas = new ValidaCuentaContrato();
         	   String []tramaCompleta=trama.split("@"); //separa tramas

         	  for(int i=0;i<tramaCompleta.length;i++)//ciclo de validacion de cuentas
         	  { EIGlobal.mensajePorTrace("tramaCompleta["+i+"] ..... " + tramaCompleta[i], EIGlobal.NivelLog.INFO);
         	    EIGlobal.mensajePorTrace("tramaCompleta["+i+"].indexOf('|') ..... " + (tramaCompleta[i].indexOf("|")), EIGlobal.NivelLog.INFO);
         	    if(tramaCompleta[i].indexOf("|")>-1){
	            		  String cuentaValidar=tramaCompleta[i].substring(0,tramaCompleta[i].indexOf("|"));
	            		  EIGlobal.mensajePorTrace("TIFondeo -  cuentaValidar:"+cuentaValidar+"<--", EIGlobal.NivelLog.INFO);
	            		  EIGlobal.mensajePorTrace("TIFondeo -  session:"+session+"<--", EIGlobal.NivelLog.INFO);
	            		  EIGlobal.mensajePorTrace("TIFondeo -  session.getContractNumber():"+session.getContractNumber()+"<--", EIGlobal.NivelLog.INFO);
	            		  EIGlobal.mensajePorTrace("TIFondeo -  session.getUserID8():"+session.getUserID8()+"<--", EIGlobal.NivelLog.INFO);
	            		  EIGlobal.mensajePorTrace("TIFondeo -  session.getUserProfile():"+session.getUserProfile()+"<--", EIGlobal.NivelLog.INFO);

	            		  String datoscta[] =null;
                             try{
	            		   datoscta= valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
	            				  											session.getUserID8(),
	            				                                            session.getUserProfile(),
	            				  											 cuentaValidar.trim(),
	            				  											 IEnlace.MMant_estruc_TI);
                             }
                             catch(Exception e)
                             {
                            	 EIGlobal.mensajePorTrace("TIFondeo -  ENTRO A EXCEPCION->:"+e.getMessage(), EIGlobal.NivelLog.INFO);
                            	 e.printStackTrace();

                             }
	     				 EIGlobal.mensajePorTrace("TIFondeo - datoscta: "+datoscta, EIGlobal.NivelLog.INFO);


	     				 if(datoscta==null){
	     					retorno=false;
	     					EIGlobal.mensajePorTrace("TIFondeo modificaEstructura- Entra a Break y rompe ciclo", EIGlobal.NivelLog.INFO);
	     					break;
	     				 }
         	    }
         	  }
         	  EIGlobal.mensajePorTrace("TIFondeo - retorno-->"+retorno, EIGlobal.NivelLog.INFO);
         	  return retorno;


          }

         catch (Exception e) {e.printStackTrace();}
         finally
 		{
 	    	try{
 	    		valCtas.closeTransaction();
 	    	}catch (Exception e) {
 	    		EIGlobal.mensajePorTrace("Error al cerrar la conexión a MQ", EIGlobal.NivelLog.ERROR);
 			}
 		}
     return retorno;
     }

	}