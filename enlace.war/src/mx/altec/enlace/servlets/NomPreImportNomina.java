
package	mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.FiltroConsultaBean;
import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.beans.NomPreLM1F;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.beans.TrxLM1JBean;
import mx.altec.enlace.beans.NomPreLM1D.Relacion;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.FormatoMoneda;
import mx.altec.enlace.bo.NomPreBusquedaAction;
import mx.altec.enlace.bo.NomPreConfiguracionAction;
import mx.altec.enlace.bo.ValidaArchivoPagos;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EnlcTarjetaDAO;
import mx.altec.enlace.dao.EnlcTarjetaDAOImpl;
import mx.altec.enlace.dao.NomPreEmpleadoDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;

/**
 * @author vision
 * Clase que realiza acciones de nomina prepago
 *
 */
public class NomPreImportNomina extends BaseServlet {

	/**
	 * serial
	 */
	private static final long serialVersionUID = -6846089411429093729L;
	
	/**
	 * Lista de Tarjetas Validas.
	 */
	private static List<String> listaTarjetas = new ArrayList<String>();

	/**
	 * Metodo que recibe las peticiones get
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	/**
	 * Metodo que recibe las peticiones post
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	/**
	 * Metodo default
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace("***NomImportNomina.class &Entrando al metodo execute()&", EIGlobal.NivelLog.INFO);
		boolean sesionvalida = SesionValida( request, response );
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		if ( sesionvalida )
		{
			request.setAttribute( "newMenu", session.getFuncionesDeMenu());
			request.setAttribute( "MenuPrincipal", session.getStrMenu());
			request.setAttribute( "Encabezado", CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt;Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
			if ((String)sess.getAttribute("facultadesN") != null)
			request.setAttribute( "facultades", (String)sess.getAttribute("facultadesN") );

			GregorianCalendar fechaHoy= new GregorianCalendar();
			String laFechaHoy = EIGlobal.formatoFecha(fechaHoy,"aaaammdd");

			request.setAttribute("fechaHoy",laFechaHoy);
			request.setAttribute("Fechas",ObtenFecha(true));
			request.setAttribute("FechaHoy",	 ObtenFecha(false) + " - 06" );
			request.setAttribute("ContenidoArchivo","");

			String systemHour=ObtenHora();

			request.setAttribute("horaSistema", systemHour);

			/*******   	 GET - HELG - INICIO - 21 FEB 2005      *******/
			if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
			{
				request.getSession().removeAttribute("Recarga_Ses_Multiples");
				EIGlobal.mensajePorTrace( "NomImport Nomina - Removiendo atributo Recarga_Ses_Multiples.", EIGlobal.NivelLog.INFO);
			}

			if(request.getSession().getAttribute("Ejec_Proc_Val_Pago")!=null)
			{
				request.getSession().removeAttribute("Ejec_Proc_Val_Pago");
				EIGlobal.mensajePorTrace("NomImport Nomina - Removiendo atributo Ejec_Proc_Val_Pago.", EIGlobal.NivelLog.INFO);
			}
			/*******   	 GET - HELG - FIN - 21 FEB 2005      *******/
			String operacion   = getFormParameter(request,"operacion");
			EIGlobal.mensajePorTrace("***NomImportNomina.class operacion&  "+operacion, EIGlobal.NivelLog.INFO);

			if ( (operacion == null) || ( operacion.equals("")))
	             operacion = "cargaArchivo";

			if ( operacion.equals("cargaArchivo") )
			{
				ejecutaCargaArchivo ( request, response);
			}

			if ( operacion.equals( "validaArchivoImport" ) )
			{
				ejecutaValidaArchivo( request, response);
			}

			if ( operacion.equals("individual"))
			{
				ejecutaValidaPagoIndividual(request, response);
			}
		}
		else
		{
		   request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
		   evalTemplate(IEnlace.ERROR_TMPL, request, response );
		}
	}// default

private void formateaArchivoPosicionesInicial(String nomArchNomina, String contrato) {

		File original = new File(nomArchNomina);
		File formato = new File(nomArchNomina + ".frm");
		FileWriter writer = null;
		FileReader reader = null;
		BufferedReader br = null;
		boolean exito = false;

		try {

			reader = new FileReader(original);
			writer = new FileWriter(formato);

			br = new BufferedReader(reader);
			String line = null;
			int length = 0;

			String noEmpleado = null;
			String paterno = null;
			String materno = null;
			String nombre = null;
			String noTarjeta = null;
			NomPreBusquedaAction action = new NomPreBusquedaAction();

			while ((line = br.readLine()) != null) {

				length = line.length();

				if (length == 39) {
					writer.write(line.substring(0, 31).concat(contrato).concat(line.substring(31)).concat("\n"));

				} else if (length == 127 || length == 129) {

					noEmpleado = line.substring(6, 13).trim();
					paterno = line.substring(13, 43).trim();
					materno = line.substring(43, 63).trim();
					nombre = line.substring(63, 93).trim();
					//noTarjeta = line.substring(93, 109).trim();
					noTarjeta = line.substring(93, 109);

					if (noTarjeta.length() > 0  && noEmpleado.length() == 0 ) {

						EIGlobal.mensajePorTrace("Obteniendo a travez de lm1d para tarjeta " + noTarjeta, EIGlobal.NivelLog.INFO);

						NomPreLM1D lm1d = action.obtenRelacionTarjetaEmpleado(contrato, noTarjeta, null, null);

						if (lm1d != null && lm1d.getDetalle() != null && lm1d.getDetalle().size() > 0) {

							NomPreEmpleado empleado = lm1d.getDetalle().get(0).getEmpleado();

							noEmpleado = empleado.getNoEmpleado();

						} else {

							EIGlobal.mensajePorTrace("No se puede consultar informacion para la tarjeta " + noTarjeta, EIGlobal.NivelLog.ERROR);
						}

					} else {

						EIGlobal.mensajePorTrace("Se mantiene la informacion del empleado " + noEmpleado, EIGlobal.NivelLog.INFO);
					}

						StringBuffer sb = new StringBuffer()
							.append(line.substring(0, 6))
							.append(rellenar(noEmpleado, 7))
							.append(rellenar(paterno, 30))
							.append(rellenar(materno, 20))
							.append(rellenar(nombre, 30))
							.append(rellenar(noTarjeta, 16))
							.append("      ")
							.append(line.substring(110))
							.append("\n");

						writer.write(sb.toString());

				} else {
					writer.write(line + "\n");
				}
			}

			exito = true;

		} catch (Exception e) {

			EIGlobal.mensajePorTrace("Error al modificar estructura de archivo ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);

		} finally {

			if (br != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}

			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
				}
			}

			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}

		if (exito) {

			EIGlobal.mensajePorTrace("Renombrando archivo", EIGlobal.NivelLog.INFO);

			original.delete();

			if (formato.renameTo(original)) {

				EIGlobal.mensajePorTrace("Archivo renombrado", EIGlobal.NivelLog.INFO);

			} else {

				EIGlobal.mensajePorTrace("No se puede renombrar el archivo", EIGlobal.NivelLog.ERROR);
			}
		}
	}
	 public void ejecutaCargaArchivo( HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***NomImportNomina.class &Entrando al metodo ejecutaCargaArchivo: &", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String nombreOriginal		= "";
		String originalName         = "";
		String nomArchNomina		= "";
		long tamanioArchivo	 	    = 0;
		String fechaAplicacionArchivo = "";
		String cuentaCargo			= "";
		//String descripCuentaCargo   = "";
		boolean cuentaAsociada		= true;
		//boolean bandera				= true;
		boolean errorEnZip=false;
		String sFile = "";
		BufferedOutputStream dest = null;
		FileInputStream fis = null;
		BufferedInputStream buis = null;
		ZipInputStream zis = null;
		FileOutputStream fos = null;

		try{
		    //se obtiene la ruta original del archivo
		    nombreOriginal  = getFormParameter(request, "fileName");
            nombreOriginal = nombreOriginal.trim();
		    EIGlobal.mensajePorTrace("***NomImportNomina.class & nombreOriginal>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);
		    EIGlobal.mensajePorTrace("***NomImportNomina.class & nombreOriginal Tam�o del archivo antes: "+request.getContentLength(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***NomImportNomina.class & Parseando objeto multipart ", EIGlobal.NivelLog.INFO);
			//
			// Modificacion para manejar el archivo directo de disco
			//
			// Asignacion dinamica de espacio
/*			MultipartRequest mprFile = new MultipartRequest ( request, IEnlace.LOCAL_TMP_DIR, 20971520 );
			//MultipartRequest mprFile = new MultipartRequest ( request, IEnlace.LOCAL_TMP_DIR,request.getContentLength() );
			//java.io.FileInputStream entrada;						//MSD 20071214 se comentaria porque no se usa este objeto.
			java.util.Enumeration files = mprFile.getFileNames ();
			// Se obtiene el nombre del archivo enviado
			if( files.hasMoreElements () )
			 {
			    String name = ( String ) files.nextElement ();
				sFile = mprFile.getFilesystemName ( name );
			 }
*/			sFile = getFormParameter(request, "fileName");
			EIGlobal.mensajePorTrace("***NomImportNomina.class & Fin Parser - Archivo:  "+ sFile, EIGlobal.NivelLog.INFO);

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_PAGOS_IMPORTACION_CARGA_ARCH);
			if(sFile!=null)
				bt.setNombreArchivo(sFile);
			NomPreUtil.bitacoriza(request,null, null, bt);

			// Si el archivo es .zip se descomprime
		    if (sFile.toLowerCase().endsWith(".zip"))
		      {
			    EIGlobal.mensajePorTrace("***NomImportNomina.class: El archivo viene en zip", EIGlobal.NivelLog.INFO);
           		final int BUFFER = 2048;
		        fis = new FileInputStream(IEnlace.LOCAL_TMP_DIR+"/" + sFile);
		        buis = new BufferedInputStream(fis);						//MSD 20071214
		        zis = new ZipInputStream(buis);									//MSD 20071214
		        ZipEntry entry;
		        entry = zis.getNextEntry();

		        EIGlobal.mensajePorTrace("***ChesRegistro.class: entry: "+entry, EIGlobal.NivelLog.INFO);
		        if(entry==null)
		         {
				   EIGlobal.mensajePorTrace("***ChesRegistro.class: Entry fue null, archivo no es zip o esta da�ando ", EIGlobal.NivelLog.INFO);
				   errorEnZip=true;
				 }
			   else
			     {
					log( "NomImportNomina.java::Extrayendo"  + entry);
					int count;
					byte data[] = new byte[BUFFER];
					// Se escriben los archivos a disco
					// Nombre del archivo zip
					File archivocomp = new File( IEnlace.LOCAL_TMP_DIR +"/" + sFile );
					// Nombre del archivo que incluye el zip
					sFile = entry.getName();
					fos = new FileOutputStream(IEnlace.LOCAL_TMP_DIR + "/"+sFile);
					dest = new BufferedOutputStream(fos, BUFFER);

					while ((count = zis.read(data, 0, BUFFER)) != -1)
					  {
						dest.write(data, 0, count);
					  }
					dest.flush();
					/*dest.close();
					fos.close();						//MSD 20071214*/
					// Borra el archivo comprimido
					if (archivocomp.exists()) {
					  archivocomp.delete();
				  	}
					  archivocomp = null;
					log( "NomImportNomina.java::Fin extraccion de: "  + entry);
				 }
			   /*zis.close();
			   buis.close();							//MSD 20071214
			   fis.close();								//MSD 20071214*/
   		     }
		} catch(Exception e) {
			log( "NomImportNomina.java::defaultAction()::try:Recibe Archivo" );
			e.printStackTrace();
		} finally {

			try {

				if (dest != null){
					dest.close();							//MSD 20071214
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			try {
				if (fos != null){
					fos.close();							//MSD 20071214
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			try {
				if (zis != null){
					zis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			try {
				if (buis != null){
					buis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			try {
				if (fis != null){
					fis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		EIGlobal.mensajePorTrace("***NomImportNomina.class After SchumiCode 01", EIGlobal.NivelLog.INFO);
		//de la ruta del archivo, se obtiene solo el nombre
		nombreOriginal = nombreOriginal.substring( nombreOriginal.lastIndexOf( '\\' ) + 1 );
		EIGlobal.mensajePorTrace("***NomImportNomina.class After SchumiCode 02", EIGlobal.NivelLog.INFO);
		originalName   = nombreOriginal;
		EIGlobal.mensajePorTrace("***NomImportNomina.class & nombreOriginalantes>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);
		//cuando el archivo sea importado, se eliminara su extension
		nombreOriginal= nombreOriginal.substring(0,nombreOriginal.length()-4);
		// Para reconocer los archivos pertenecientes a este modulo se
		// les asignara la extension .nom
		//nombreOriginal = nombreOriginal + ".nom";
		//32 caracteres.
		//String sessionId = (String) request.getSession().getId();
		Calendar cal = Calendar.getInstance();
		//nombreOriginal = "01234567890" + cal.getTime().getTime();
		nombreOriginal = session.getContractNumber() + cal.getTime().getTime();
		//nombreOriginal = sessionId;
		cal = null;
	    EIGlobal.mensajePorTrace("***NomImportNomina.class & nombreOriginal_ultimo ...>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);
		//String nueva_ruta= "/t/tuxedo/procesos/enlace/internet/archivos_internet/";
		//nomArchNomina  = "" + nueva_ruta + nombreOriginal;
		nomArchNomina  = "" + IEnlace.DOWNLOAD_PATH + nombreOriginal;
		EIGlobal.mensajePorTrace("***NomImportNomina.class & nombre asigando >"+nomArchNomina ,EIGlobal.NivelLog.INFO);

		//***** everis Declaracion RandomAccessFile 08/05/2008  ..inicio
		RandomAccessFile archivoNomina = null;
		//***** everis Declaracion RandomAccessFile 08/05/2008  ..fin
		//Si ya existe el archivo lo borra
		try{
		        EIGlobal.mensajePorTrace("***NomImportNomina.class & Si el archivo existe, se renombra.", EIGlobal.NivelLog.INFO);
			// Renombra el archivo descargado
			File archivo = new File( IEnlace.LOCAL_TMP_DIR +"/"+ sFile );
			if (archivo.exists())
			 {
			   EIGlobal.mensajePorTrace("***NomImportNomina.class & Se renombra a: "+nomArchNomina, EIGlobal.NivelLog.INFO);
			   archivo.renameTo(new File(nomArchNomina));
			  }
			else
			 {
			   EIGlobal.mensajePorTrace("***NomImportNomina.class & El archivo no existe.", EIGlobal.NivelLog.INFO);
			   // throw file not found exception
			 }
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("***NomImportNomina.class Ha ocurrido una excepcion en: %ejecutaCargaArchivo()"+e, EIGlobal.NivelLog.INFO);
		}
		try{
			EIGlobal.mensajePorTrace("***NomImportNomina.class & Se abre nuevo archivo: "+nomArchNomina, EIGlobal.NivelLog.INFO);

			/*
			 * Se completan los caracteres necesarios para que coincida con el archivo esperado por pampa
			 * dado que el original esta homologado con el layout de la nomina tradicional
			 * VSWF - CGH
			 */
			formateaArchivoPosicionesInicial(nomArchNomina, session.getContractNumber());

			File pathArchivoNom	= new File( nomArchNomina );
			//***** everis Declaracion RandomAccessFile 08/05/2008  ..inicio
			//RandomAccessFile archivoNomina = new RandomAccessFile( pathArchivoNom, "r" );
			archivoNomina = new RandomAccessFile(pathArchivoNom, "r");
			//archivoNomina = new RandomAccessFile( pathArchivoNom, "r" );
			//***** everis Declaracion RandomAccessFile 08/05/2008  ..fin
			tamanioArchivo = archivoNomina.length();
			if(tamanioArchivo<=0)
			{
				despliegaPaginaError( " Imposible Importar el archivo seleccionado, la longitud del archivo es incorrecto.",
				"Pago de N&oacute;mina", "Importando Archivo","s25800h", request, response );
			}
			sess.setAttribute("nombreArchivo",nomArchNomina);
			//String nombreArchivoPagoNomina = nomArchNomina.substring(nomArchNomina.lastIndexOf( "/" ) + 1 );
			request.setAttribute( "tipoArchivo","importado" );
			archivoNomina.seek(0);
			String primeralinea	= archivoNomina.readLine();
			EIGlobal.mensajePorTrace("***NomImportNomina.class &primeralinea &"+primeralinea.length(), EIGlobal.NivelLog.INFO);

			String ctaAux="";
			if(errorEnZip)
			 {
			  despliegaPaginaError( " Imposible Importar el archivo, no es un archivo zip o esta da�ado",
			  "Pago de N&oacute;mina", "Importando Archivo","s25800h", request, response );
			   EIGlobal.mensajePorTrace("***NomImportNomina.class &primeralinea & Imposible Importar el archivo, no es un archivo zip o esta da�ado." , EIGlobal.NivelLog.INFO);
			   	//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
				archivoNomina.close();
				//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
			  }
			else
			if (primeralinea.length() !=  50)
			{
			despliegaPaginaError( " Imposible Importar el archivo seleccionado, la longitud del encabezado es incorrecto.",
			"Pago de N&oacute;mina", "Importando Archivo","s25800h", request, response );
			//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
			archivoNomina.close();
			//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
			}else {
					//cuenta de cargo del archivo
					if(primeralinea.length()>31)
					 {
					   cuentaCargo = primeralinea.substring( 15, 31 );
					   ctaAux=cuentaCargo.substring(0,11);
					   EIGlobal.mensajePorTrace("***NomImportNomina.class & Cta Aux = "+ctaAux, EIGlobal.NivelLog.INFO);
					 }
					cuentaCargo = cuentaCargo.trim();
					EIGlobal.mensajePorTrace("***NomImportNomina.class &cuentaCargo &"+cuentaCargo, EIGlobal.NivelLog.INFO);
					/*ESC HD1000000085131 - RFC:49313 - Se valida la cuenta de cargo*/
					if(cuentaCargo !=null && !"null".equals(cuentaCargo))
						session.setCuentaCargo( cuentaCargo );

					fechaAplicacionArchivo = new String(primeralinea.substring(42,50));
					EIGlobal.mensajePorTrace("***NomImportNomina.class &fechaAplicacionArchivo &"+fechaAplicacionArchivo, EIGlobal.NivelLog.INFO);
					//***********************************************
					//modificacion para integracion pva 07/03/2002
			        //***********************************************
				    llamado_servicioCtasInteg(IEnlace.MEnvio_pagos_nomina," "," "," ",session.getUserID8()+".ambci",request);
					String datoscta[]	= null;
				     datoscta			= BuscandoCtaInteg(cuentaCargo,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci",IEnlace.MEnvio_pagos_nomina);
					if(datoscta == null){
				       cuentaAsociada	= false;
					} else {
					cuentaAsociada = true;
					sess.setAttribute("descpCuentaCargo",datoscta[4]);
					EIGlobal.mensajePorTrace("***NomImportNomina.class &datoscta[4] &"+datoscta[4], EIGlobal.NivelLog.INFO);
					}
					session.setCuentasArchivo("");
					sess.setAttribute("numeroEmp","");
					if (archivoNomina!=null){
						archivoNomina.close();
					}

					if ( !cuentaAsociada ) // aqui cuando la cuenta y el archivo estan mal
						{
							session.setCuentaCargo(" ");
							despliegaPaginaError( " Imposible Importar el archivo seleccionado, la cuenta  (" + cuentaCargo + ")  no existe en el contrato o no est&aacute; disponible para el servicio de n&oacute;mina.",
								"Pago Tarjeta de Pagos Santander ", "Importando Archivo","s25800h", request, response );//YHG NPRE
					}else
					 {
						if(ctaAux.trim().length()!=11)
						 {
						   EIGlobal.mensajePorTrace("***NomImportNomina.class & La cuenta aux empieza con espacios, mal justificada.", EIGlobal.NivelLog.INFO);
						   session.setCuentaCargo(" ");
						   despliegaPaginaError(
								" Imposible Importar el archivo seleccionado, la cuenta de cargo no est&aacute; justificada correctamente. ",
									"Pago de N&oacute;mina", "Importando Archivo","s25800h", request, response );						 }
						else
						 {
						   request.setAttribute("tamanioArchivo",String.valueOf(tamanioArchivo));
						   request.setAttribute("nomArchNomina",nomArchNomina);
						   request.setAttribute("originalName",originalName);
						   evalTemplate( "/jsp/prepago/NomPreNominaReporte.jsp", request, response );
						   //ejecutaValidaArchivo( request, response);
						 }
					}
				} //else != 39
		} catch(Exception e) {

			EIGlobal.mensajePorTrace("***NomImportNomina.class Ha ocurrido una excepcion en: %ejecutaCargaArchivo()" + e, EIGlobal.NivelLog.INFO);
		}
		//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
		finally{
            if(null != archivoNomina){
                try{
                	archivoNomina.close();
                }catch(Exception e){}
                archivoNomina = null;
            }
        }
		//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
	}//fin del metodo ejecutaCargaArchivo

		/**
		*		Modificado por: Miguel Cortes Arellano
		*		Fecha: 19/12/2003
		*		Proposito: Ejecutar un servicio TUXEDO y obtener los horarios premier para la presente session
		*		Empieza codigo SINAPSIS
		*/
		private String obtenHorarioPremier( HttpServletRequest request, HttpServletResponse response )
		{
				HttpSession sess = request.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");
				String usuario=session.getUserID8(), contrato=session.getContractNumber(),clavePerfil=session.getUserProfile();
				String trama_entrada="1EWEB|"+usuario+"|NOCH|"+contrato+"|"+usuario+"|"+clavePerfil+"|"+contrato+"@";
				String CodError = "";
				Hashtable htResult = null;
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				//*********************
				//Se llama al servicio de tuxedo
				try {
					htResult = tuxGlobal.web_red( trama_entrada );
				}
				catch ( java.rmi.RemoteException re ) {
					re.printStackTrace();
				}
				catch (Exception e) {}
				CodError = ( String ) htResult.get( "BUFFER" );

				EIGlobal.mensajePorTrace("Despues de llamar a WEB_RED, cod error:"+CodError, EIGlobal.NivelLog.DEBUG);

				//CODIGOERROR@DESCERROR@HORARIOS SEPARADOS POR PIPES@
				String estatus_transacion = CodError.substring(0,posCar(CodError,'@',1));
				//String mensaje_transacion = CodError.substring((posCar(CodError,'@',1))+1,posCar(CodError,'@',2));
				String Horario = CodError.substring((posCar(CodError,'@',2))+1,posCar(CodError,'@',3));
				if(estatus_transacion.equals("NOMI0000"))
					return "NOMI0000"+" "+Horario;
				else
					return "Existe un error en la obtencion de horarios por favor trate mas tarde!!!";
		}

			//metodo posCar que devuelve la posicion de un caracter en un String
			public int posCar(String cad,char car,int cant) {
				int result=0,pos=0;
				for (int i=0;i<cant;i++) {
					result=cad.indexOf(car,pos);
					pos=result+1;
				}
				return result;
			}
		//Finaliza codigo SINAPSIS

	 public void ejecutaValidaArchivo( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
			EIGlobal.mensajePorTrace("***NomImportNomina.class &Entrando al metodo ejecutaValidaArchivo: &", EIGlobal.NivelLog.INFO);
			HttpSession sess = request.getSession();
		    BaseResource session = (BaseResource) sess.getAttribute("session");
		    //***** everis Declaracion RandomAccessFile 08/05/2008  ..inicio
			//ValidaArchivoPagos validaArchivo;
			ValidaArchivoPagos validaArchivo = null;
			//***** everis Declaracion RandomAccessFile 08/05/2008  ..fin
			String nomArchNomina		= getFormParameter(request,"nomArchNomina");
			nomArchNomina = nomArchNomina.trim();
			//validaArchivo				= new ValidaArchivoPagos( this, nomArchNomina );
			Vector<String> erroresEnElArchivo	= new Vector<String>();
			erroresEnElArchivo.addElement("<table>");
			boolean archivoValido		= true;
			Vector<Object> cantidadRegistros    = new Vector<Object>();
			String[][] arrayDetails;
			StringBuffer contenidoArchivoStr	= new StringBuffer("");
			String originalName		= getFormParameter(request, "fileName");//request.getParameter("originalName");
			EIGlobal.mensajePorTrace("***NomImportNomina.class &originalName: prueba 3 &"+originalName, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***NomImportNomina.class &nomArchNomina: &"+nomArchNomina, EIGlobal.NivelLog.INFO);
			//ArchivoRemoto archR;
			//archR	= new ArchivoRemoto();
			/*************** Nomina Concepto de Archvo */
			//boolean conceptoEnArchivo=false;
			/*************** Nomina Concepto de Archvo */
			/**
			*		Modificado por Miguel Cortes Arellano
			*		Fecha: 23/12/03
			*		Proposito: Variables usadas para la obtencion del horario
			*		Empieza codigo SINAPSIS
			*/
			String HorarioPremier = "";
			String tempHorarioPremier = "";
			StringBuffer SelectHorarioPremier = new StringBuffer("");
			StringTokenizer getHorario;
			String Horario = "";
			String FacultadPremier = "";
			FacultadPremier =(String)request.getAttribute("facultades");

			//Finaliza codigo SINAPSIS
			/*
			* Hasta esta linea, se ha creado el archivo( nuevo o importado ),
			* se procedera a conocer el numero de registros
			* de detalle que contiene.
			*/
			// Se invoca al metodo registrosEnArchivo() para DETERMINAR cual es la
			// CANTIDAD DE REGISTROS EXISTENTES EN EL ARCHIVO
			//File pathArchivoNom	= new File( nomArchNomina );
			//Cambiar es metodo por buffer
			//RandomAccessFile archivo = new RandomAccessFile( pathArchivoNom, "rw");
			int registros		  = 0;
			registros= registrosEnArchivo( nomArchNomina ,request, response);
			EIGlobal.mensajePorTrace("***NomImportNomina.class &registros&"+registros, EIGlobal.NivelLog.INFO);
			int cuentaDeRegistros = registros;
			//String numerosEmpleados[] = null;
			/*try{
				numerosEmpleados = arregloNumeroEmpleado( pathArchivoNom, registros - 2 );
			} catch(Exception e) {
			e.printStackTrace();
			}*/
			//String cuentasAbono[]	  = null;
			/*try{
				cuentasAbono= arregloCuentas( pathArchivoNom, registros - 2 );
			}catch(Exception e) {
			e.printStackTrace();
			}*/
			int registrosLeidos	   = 0;
			registrosLeidos		   = cuentaDeRegistros;
			//FIN DETERMINA NUMERO DE REGISTROS
			BufferedReader arch = null;
			FileReader reader = null;
			reader  = new FileReader( nomArchNomina );
			arch	= new BufferedReader( reader );
			String registroEnc	= arch.readLine();
			String encTipoReg	= "";
			String encNumSec	= "";
			String encSentido	= "";
			String encFechGen	= "";
			String encCuenta	= "";
			String encFechApli	= "";
			validaArchivo= new ValidaArchivoPagos( this, nomArchNomina );

			try
				{
				encTipoReg	= new String( registroEnc.substring( 0, 1 ) );
			    if (validaArchivo.validaDigito( encTipoReg ) == false ||
					validaArchivo.validaEncTipoReg( encTipoReg ) == false )
			    {
					erroresEnElArchivo.addElement( "<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\">Error en el primer Caracter del encabezado, se esperaba 1</td>");
					archivoValido	= false;
					EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el Tipo registro encabezado&", EIGlobal.NivelLog.INFO);
				}
				encNumSec = new String( registroEnc.substring( 1, 6 ) );
				EIGlobal.mensajePorTrace("***NomImportNomina.class en encNumSec a: ("+encNumSec+")", EIGlobal.NivelLog.INFO);
				encNumSec=encNumSec.trim();
				EIGlobal.mensajePorTrace("***NomImportNomina.class en encNumSec b: ("+encNumSec+")", EIGlobal.NivelLog.INFO);

				if (validaArchivo.validaDigito( encNumSec ) == false || validaArchivo.validaPrimerNumSec( encNumSec ) == false )
				{
					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del encabezado, se esperaba 1</td>");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en n&uacute;mero secuencial del encabezado&", EIGlobal.NivelLog.INFO);
				}
				encSentido = new String(registroEnc.substring(6,7));
				EIGlobal.mensajePorTrace("***NomImportNomina.class  encSentido:"+encSentido, EIGlobal.NivelLog.INFO);
				 if (validaArchivo.validaLetras(encSentido)==false||validaArchivo.validaSentido(encSentido)==false)
				 {
					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el caracter de sentido del encabezado, se esperaba E</td>");
					archivoValido	= false;
					EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el sentido del encabezado, se esperaba 'E'&", EIGlobal.NivelLog.INFO);
				 }
				 encFechGen = new String(registroEnc.substring(7,15));
  				 EIGlobal.mensajePorTrace("***NomImportNomina.class &encFechGen:"+encFechGen, EIGlobal.NivelLog.INFO);
				 if (validaArchivo.validaDigito(encFechGen)==false||validaArchivo.fechaValida(encFechGen)==false)
				 {
					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de generacion del archivo</td>");
					archivoValido	= false;
					EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en la fecha de generacion del archivo&", EIGlobal.NivelLog.INFO);
				 }
				   // La cuenta ya no se verifica pues al momento de formar el arreglo de
				   // cuentas, se ha verificado que exista y que sea propia y/o de terceros
				  encCuenta = new String( registroEnc.substring( 15,31 ) );
  				  EIGlobal.mensajePorTrace("***NomImportNomina.class &encCuenta:"+ encCuenta, EIGlobal.NivelLog.INFO);
				  encFechApli = new String(registroEnc.substring(42,50));
  				  EIGlobal.mensajePorTrace("***NomImportNomina.class &encFechApli:"+encFechApli, EIGlobal.NivelLog.INFO);
				   if (validaArchivo.validaDigito(encFechApli)==false||validaArchivo.fechaValida(encFechApli)==false)
				   {
					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de aplicacion del archivo</td>");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en la fecha de aplicacion&", EIGlobal.NivelLog.INFO);
				   }
            }
			catch(Exception e)
            {
   			 erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");
			 archivoValido=false;
			 EIGlobal.mensajePorTrace("***NomImportNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
             }
 			//EIGlobal.total_lineas_leidas(1,registros);
			session.setFechaAplicacion( encFechApli );
			//String registroEncabezadoCompleto;
			//registroEncabezadoCompleto = encTipoReg + encNumSec+encSentido +
			//encFechGen + encCuenta + encFechApli;
			//int longTotalEncabezado = registroEncabezadoCompleto.length();
			/* Detalle del archivo */
			if ( encNumSec != null ) {
				cantidadRegistros.addElement( encNumSec );
			}
			int longitudArreglo = cuentaDeRegistros - 2;
			// La longitud del arreglo esta determinada por la cantidad de
			// registros de ddetalle existentes en el archivo de pago de nomina
			if ( longitudArreglo < 0 )
				longitudArreglo = 0;
			String longitudArray = Integer.toString( longitudArreglo );
			//el metodo setNominaNumberOfRecords() devuelve el numero de registros de detalle en el archivo
			session.setNominaNumberOfRecords( longitudArray );
			arrayDetails = new String[longitudArreglo][9];
			//int longTotalDetalle;
			//longTotalDetalle = 0;
			double sumImporte       = 0;
					String registroDet		= "";  //jtg saco la declaracion de variables del for
					String tipoRegistro		= "";
					String NumeroSecuencia	= "";
					String NumeroEmpleado	= "";
					String ApellidoPaterno	= "";
					String ApellidoMaterno	= "";
					String Nombre			= "";
					String NumeroCuenta		= "";
					String ctaAux1 			= "";
					String Importe			= "";
					double num				= 0.0;
					//double importeFinal		= 0.0;
					//String tmpImporte = "";
					//double tmpnum = 0.0;
					int contadorLinea		= 1;
					/*************** Nomina Concepto de Archvo */
					String Concepto         = "";
					boolean registroContieneConcepto=false;
					int registrosConConcepto=0;
					Vector listaIDConceptos=getIDConceptos();
					/*************** Nomina Concepto de Archvo */
		//if ( arrayDetails.length > Global.REG_MAX_IMPORTAR_NOMINA ){
					double montoMaximo = 0.0;
					try {
						NomPreConfiguracionAction action = new NomPreConfiguracionAction();
						montoMaximo = action.obtieneMonto(null);
						EIGlobal.mensajePorTrace("Monto Maximo :" + montoMaximo, EIGlobal.NivelLog.INFO);

						SimpleDateFormat sdf=new SimpleDateFormat("ddMMyyyy");
						BitaTransacBean bt = new BitaTransacBean();
						bt.setNumBit(BitaConstants.ES_NP_PAGOS_IMPORTACION_VALIDA_ARCH);
						if(originalName!=null)
							bt.setNombreArchivo(originalName);
						if(montoMaximo>0)
							bt.setImporte(montoMaximo);
						if(encFechApli!=null && !encFechApli.trim().equals(""))
							try {
								bt.setFechaAplicacion(sdf.parse(encFechApli));
							} catch (ParseException e1) {}

						if(encNumSec!=null && !encNumSec.trim().equals(""))
							bt.setReferencia(Long.parseLong(encNumSec));
						if(encCuenta!=null && !encCuenta.trim().equals(""))
							bt.setCctaOrig(encCuenta);
						NomPreUtil.bitacoriza(request,null, null, bt);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace("Excepcion al llamar NomPreConfiguracionAction ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					}

			
			EnlcTarjetaDAO tarjTrxBO = new EnlcTarjetaDAOImpl();
			
			tarjTrxBO.initTransac();
			for (int i= 0; i < arrayDetails.length; i++)
				{
			  try
                 {
				try{
					registroDet		= arch.readLine();
					if(registroDet==null) //jtg   si es nulo no puedo obtener los datos
						continue;
					if(! (registroDet.length() == 132  || registroDet.length()==134) )
					 {
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el tama�o del Registro </td>");
					    archivoValido=false;
						EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);
						continue;
					}// else{
					//}
					tipoRegistro	= new String ( registroDet.substring(0,1)		);
					NumeroSecuencia	= new String ( registroDet.substring(1,6)		);
					NumeroEmpleado	= new String ( registroDet.substring(6,13)		);
					ApellidoPaterno	= new String ( registroDet.substring(13,43)		);
					ApellidoMaterno	= new String ( registroDet.substring(43,63)		);
					Nombre			= new String ( registroDet.substring(63,93)		);
					NumeroCuenta	= new String ( registroDet.substring(93,115)	);
					Importe			= new String ( registroDet.substring(115,132)	);

					ctaAux1 = registroDet.substring(93,115);

					/*************** Nomina Concepto de Archvo */
					if(registroDet.length()>132)
					 {
					   Concepto        = new String (registroDet.substring(132,134));
					   registrosConConcepto++;
					   registroContieneConcepto=true;
					 }
					/*************** Nomina Concepto de Archvo */

					num = Double.parseDouble(Importe);
					sumImporte = sumImporte + num;
				} catch(Exception e) {
					EIGlobal.mensajePorTrace("Excepcion al leer archivo ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				}

					arrayDetails[i][0] = tipoRegistro;
					if ( validaArchivo.validaDigito( arrayDetails[i][0] ) == false || validaArchivo.validaDetalleTipoReg( arrayDetails[i][0] ) == false )
					{
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el tipo de Registro del detalle</td>");
					    archivoValido=false;
						EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);
					}

					arrayDetails[i][1]=NumeroSecuencia;
					if (validaArchivo.validaDigito(arrayDetails[i][1])==false || validaArchivo.validaNumSec(i, arrayDetails[i][1])==false)
					{
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del detalle</td>");
					    archivoValido=false;
						EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el n&uacute;mero secuencial del detalle&", EIGlobal.NivelLog.INFO);
					}
					arrayDetails[i][2]=NumeroEmpleado;
					/*if (NumeroEmpleado.equals("") || NumeroEmpleado == null || NumeroEmpleado.equals("       ") ){
						NumeroEmpleado = " ";
						arrayDetails[i][2]=NumeroEmpleado;
					}else {
						arrayDetails[i][2]=NumeroEmpleado;
						if (validaArchivo.verificaDuplicidadNumeroEmpleado(numerosEmpleados, arrayDetails[i][2])){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">El empleado esta duplicado</td>");
					    archivoValido=false;
						//EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el numero de empleado&", EIGlobal.NivelLog.INFO);
						}
					}*/
					arrayDetails[i][3]=ApellidoPaterno;
					/*if (arrayDetails[i][3]!="")
						if (validaArchivo.validaNombres(arrayDetails[i][3])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Apellido Paterno</td>");
							archivoValido=false;
							//EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el apellido paterno&", EIGlobal.NivelLog.INFO);
						}*/
					arrayDetails[i][4]=ApellidoMaterno;
					/*if(arrayDetails[i][4]!="")
						if (validaArchivo.validaNombres(arrayDetails[i][4])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Apellido Materno</td>");
							archivoValido=false;
							//EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el apellido materno&", EIGlobal.NivelLog.INFO);
						}*/
					arrayDetails[i][5]=Nombre;
					/*if(arrayDetails[i][5]!="")
						if (validaArchivo.validaNombres(arrayDetails[i][5])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Nombre</td>");
						    archivoValido=false;
						//EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el nombre&", EIGlobal.NivelLog.INFO);
						}*/
					arrayDetails[i][6]=NumeroCuenta;
					/* modificacion para integracion
					if (validaArchivo.validaDigito(arrayDetails[i][6])==false||validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false
							||validaArchivo.validaCuenta(arrayDetails[i][6])==false||validaArchivo.verificaDuplicidadCuentas(cuentasAbono, arrayDetails[i][6])){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");
						archivoValido=false;
						//EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el nombre&", EIGlobal.NivelLog.INFO);
					}*/
				    ////EIGlobal.mensajePorTrace("***Comienza modificacion para integracion: ", EIGlobal.NivelLog.INFO);
					/*if (validaArchivo.validaDigito(arrayDetails[i][6])==false||
						validaArchivo.validaCuenta(arrayDetails[i][6])==false||validaArchivo.verificaDuplicidadCuentas(cuentasAbono, arrayDetails[i][6]))
					{*/
					/*if (validaArchivo.validaDigito(arrayDetails[i][6])==false||	validaArchivo.validaCuenta(arrayDetails[i][6])==false)
					{
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");
						archivoValido=false;
						//EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el Numero de Cuenta&", EIGlobal.NivelLog.INFO);
					}*//*
					if (validaArchivo.validaTarjeta(arrayDetails[i][6])==false)
					{
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero de Cuenta</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el n&uacute;mero de Cuenta&", EIGlobal.NivelLog.INFO);
					}*/
					/*if (validaArchivo.validaCuentaDolares(arrayDetails[i][6])==false)
					{
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error el Numero de Cuenta es en dolares</td>");
						archivoValido=false;
						//EIGlobal.mensajePorTrace("***ImportNomina.class &Error el Numero de Cuenta es de dolares&", EIGlobal.NivelLog.INFO);
					}*/
					/*if ( (session.getClaveBanco().equals(Global.CLAVE_SANTANDER))&&
						 (validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false))
                    {
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");
						archivoValido=false;
						//EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el Numero de Cuenta&", EIGlobal.NivelLog.INFO);
                    }
                    if  (session.getClaveBanco().equals(Global.CLAVE_SERFIN))
                    {
                        if (validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false)
                        {
							if (validaDigitoCuentaHogan(arrayDetails[i][6])==false)
							{
                               erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");
					           archivoValido=false;
						       //EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);
                            }
                        }
                    }*/

			        ////EIGlobal.mensajePorTrace("***termina modificacion para integracion: ", EIGlobal.NivelLog.INFO);
                    //******fin de modificacion para integracion
					/*************** Nomina Concepto de Archvo */
					if (validaArchivo.validaDigito(Concepto)==false || (registroContieneConcepto && Concepto.trim().equals("")))
						{
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Identificador no v&aacute;lido en el campo Concepto.</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el n&uacute;mero de Concepto&", EIGlobal.NivelLog.INFO);
						}
				   else/* Validacion en archivo  */
				    if(listaIDConceptos!=null && Concepto.trim().length()>0 && listaIDConceptos.indexOf(Concepto)<0)
				      {
					    EIGlobal.mensajePorTrace("***NomImportNomina.class & El Concepto no esta registrado.", EIGlobal.NivelLog.INFO);
					    erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">La clave del Concepto no est&aacute; Registrada.</td>");
					    archivoValido=false;
					   }
				    /*************** Nomina Concepto de Archvo */
					arrayDetails[i][7]=Importe;
					//if (validaArchivo.validaDigito(arrayDetails[i][7])==false){
					if (validaArchivo.validaDigito(arrayDetails[i][7])==false || arrayDetails[i][7].endsWith(" "))
					 {
					   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe</td>");
					   archivoValido=false;
					   EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el importe&", EIGlobal.NivelLog.INFO);
					 }
					else
					 {
						/*ESC HD1000000085131 - RFC:49313 - Se valida la cuenta de abono*/
						EIGlobal.mensajePorTrace("***ImportNomina.class &Cuenta auxiliar abono = ["+ctaAux1 + "]", EIGlobal.NivelLog.DEBUG);

						EIGlobal.mensajePorTrace("***ImportNomina.class &Cuenta abono = ["+NumeroCuenta + "]", EIGlobal.NivelLog.DEBUG);

						//if(ctaAux1.trim().length()!=11)
						if(NumeroCuenta.startsWith(" "))
						 {
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la cuenta de abono, no est&aacute; justificada correctamente.</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en la cuenta no esta justificada correctamente&", EIGlobal.NivelLog.DEBUG);
						 }else{


							 if(NumeroCuenta.trim().length() != 11 &&  NumeroCuenta.trim().length() != 16){
								 //Se descomenta al subirlo es para validar el monto
								 erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap>Longitud de cuenta de abono invalida.</td>");
								 archivoValido=false;
							 }else if(!validaNumero(NumeroCuenta))
							 {
								 erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap>La cuenta de abono debe ser numerica.</td>");
								 archivoValido=false;
							 }
						 }
					 }
					if (Importe.trim().length() == 0)
					{
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Debe especificar importe</td>");
						archivoValido=false;
					}


					if (NumeroCuenta != null && NumeroCuenta.trim().length() == 16) {
						//Se descomenta al subirlo es para validar el monto
						if(Double.parseDouble(Importe.substring(0,Importe.length()-2)+"."+Importe.substring(Importe.length()-2,Importe.length())) > montoMaximo)
						{
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap>No puede pagar un importe tan alto para el empleado en la linea :"+(i+2)+"</td>");
							archivoValido=false;
						}
                 	}

					//Se valida que la tarjeta a pagar este vinculada con el contrato, consulta a la EWEB_VINC_TARJETACON
					if(NumeroCuenta.substring(0, 2).equals("54")){  //Se valida que la cuenta sea un Prepago
	
						FiltroConsultaBean filtros;
						filtros = new FiltroConsultaBean();
						filtros.setTarjeta(NumeroCuenta);
						filtros.setContrato(session.getContractNumber());
						filtros.setEstatus("A");
						List<TarjetaContrato> tarjetas;
						tarjetas = tarjTrxBO.consultarDatosTarjetas(filtros);
	
						if(tarjetas.isEmpty()){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap>La tarjeta no se encuentra vinculada al contrato en la linea :"+(i+2)+"</td>");
							archivoValido=false;
						}
					}					
					cantidadRegistros.addElement(arrayDetails[i][1]);
					//String registroDetalleCompleto;
					//registroDetalleCompleto=arrayDetails[i][0]+arrayDetails[i][1]+arrayDetails[i][2]+arrayDetails[i][3]+arrayDetails[i][4]+arrayDetails[i][5]+arrayDetails[i][6]+arrayDetails[i][7];
					//longTotalDetalle= registroDetalleCompleto.length();
		  }
    	  catch(Exception e)
             {
    		  EIGlobal.mensajePorTrace("Excepcion en arrayDetails ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
   			//erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato en el archivo o un campo esta vacio</td>");
			archivoValido=false;
			EIGlobal.mensajePorTrace("***NomImportNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.DEBUG);
		      }
			contadorLinea++;
		}//del for
		EIGlobal.mensajePorTrace("NomImportNomina:: INICIANDO CERRANDO CONEXION.", EIGlobal.NivelLog.DEBUG);
		tarjTrxBO.closeTransac();
		EIGlobal.mensajePorTrace("NomImportNomina:: FINALIZANDO CERRANDO CONEXION.", EIGlobal.NivelLog.DEBUG);
				double sumTotal = 0 ;
				//Registro Sumario
				String registroSum = arch.readLine();
				EIGlobal.mensajePorTrace("***NomImportNomina.class &registroSum&"+registroSum.length(), EIGlobal.NivelLog.INFO);
				String sumTipoReg  = "";
				String sumNumSec   = "";
				String sumTotRegs  = "";
				String sumImpTotal = "";

                try
				{
					if (registroSum.length() != 29)
					{
					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la longitud del sumario</td>");
				    archivoValido=false;
					EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en la longitud del sumario&", EIGlobal.NivelLog.INFO);
					}
					else
					{
						sumTipoReg=new String(registroSum.substring(0,1));
					    if (validaArchivo.validaDigito(sumTipoReg)==false || validaArchivo.validaSumTipoReg(sumTipoReg)==false)
					    {
						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero de registro</td>");
						  archivoValido=false;
						  EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el n&uacute;mero de registro del sumario&", EIGlobal.NivelLog.INFO);
					    }
						sumNumSec= new String(registroSum.substring(1,6));
					    if(validaArchivo.validaDigito(sumNumSec) && validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos))
					    {
						 session.setLastSecNom(sumNumSec);
					    }
					    if (validaArchivo.validaDigito(sumNumSec)==false||validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)==false)
					    {
						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del sumario</td>");
						  archivoValido=false;
						  EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el n&uacute;mero secuencial del sumario&", EIGlobal.NivelLog.INFO);
					    }
					    sumTotRegs = new String(registroSum.substring(6,11));
					    if(validaArchivo.validaDigito(sumTotRegs) && validaArchivo.validaTotalDeRegistros(sumTotRegs, cuentaDeRegistros))
					    {
						  session.setTotalRegsNom(sumTotRegs);
						}
					    if (validaArchivo.validaDigito(sumTotRegs)==false||validaArchivo.validaTotalDeRegistros(sumTotRegs, cuentaDeRegistros)==false)
					    {
						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el total de registros del sumario</td>");
						  archivoValido=false;
						  EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el total de registros&", EIGlobal.NivelLog.INFO);
						}
					    sumImpTotal = new String(registroSum.substring(11,29));
					    if (validaArchivo.validaDigito(sumImpTotal))
					    {
						  session.setImpTotalNom(sumImpTotal);
					    }
					    validaArchivo.cerrar();
						//***** everis se Comenta el valor que se asigna a validaArchivo  RandomAccessFile 08/05/2008  ..inicio
					    //validaArchivo = null;
						//***** everis se Comenta el valor que se asigna a validaArchivo  RandomAccessFile 08/05/2008  ..fin

						/*************** Nomina Concepto de Archvo */
						EIGlobal.mensajePorTrace("***NomImportNomina.class &Total de Registros en Archivo: "+session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("***NomImportNomina.class &Total de Registros con Concepto: "+registrosConConcepto, EIGlobal.NivelLog.INFO);
						if(registrosConConcepto==0)
						 {
							if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
								request.getSession().removeAttribute("conceptoEnArchivo");
							request.getSession().setAttribute("conceptoEnArchivo","false");
						 }
						if(registrosConConcepto!=0 && registrosConConcepto!= (Integer.parseInt(session.getNominaNumberOfRecords())) )
						 {
							 erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Error: </td><td class=\"textabdatobs\" nowrap align=\"center\"> Si los registros incluyen Concepto debe especificarlo en todos.</td>");
							 archivoValido=false;
							 EIGlobal.mensajePorTrace("***NomImportNomina.class &Si los registros incluyen concepto debe especificarlo en todos.&", EIGlobal.NivelLog.INFO);
						 }
						/*************** Nomina Concepto de Archvo */
						sumTotal = Double.parseDouble(sumImpTotal);
						/* Suma de los importes */
						if (sumTotal != sumImporte)
						{
						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe total </td>");
						  archivoValido=false;
						  EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en el Importe total&", EIGlobal.NivelLog.INFO);
						}
						} //else != 29
		            }
   					catch(Exception e)
				    {
   					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***NomImportNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
					}
					//***** everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..inicio
					finally{
			            if(null != validaArchivo){
			                try{
			                	validaArchivo.cerrar();
			                }catch(Exception e){}
			                validaArchivo = null;
			            }
					}
					//****  everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..fin
				erroresEnElArchivo.addElement("</table>");
				erroresEnElArchivo.trimToSize();//a partir de esta linea el vector erroresEnElArchivo contiene la tabla de errores
				//String registroSumarioCompleto;
				//registroSumarioCompleto=sumTipoReg+sumNumSec+sumTotRegs+sumImpTotal;
				//int longTotalSumario;
				//longTotalSumario = registroSumarioCompleto.length();
				if (arch!=null){
					arch.close();
				}
				cantidadRegistros.addElement(sumNumSec);
				//int total;
				//total = cantidadRegistros.size();
				// Aqui finaliza la lectura y validacion de los campos del archivo
				// es necesario determinar si se exhibiran o no los registros de detalle, solo se exhibiran cuando sean menos de
				// 30 registros y estos sean validos
				//if(Integer.parseInt(session.getNominaNumberOfRecords())<=30){
			/*}//del if
			   else {
			        despliegaPaginaError( "Imposible Importar el archivo seleccionado, tiene mas de"+  Global.REG_MAX_IMPORTAR_NOMINA + " registros.",
					"Pago de N&oacute;mina", "Importando Archivo","s25800h", request, response );
			        archivoValido=false;
					}*/
				if (archivoValido)
				 {

					contenidoArchivoStr=new StringBuffer("");
					contenidoArchivoStr.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
					contenidoArchivoStr.append("<tr> ");
					contenidoArchivoStr.append("<td class=\"textabref\">Pago de n&oacute;mina</td>");
					contenidoArchivoStr.append("</tr>");
					contenidoArchivoStr.append("</table>");
					contenidoArchivoStr.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
					contenidoArchivoStr.append("<tr> ");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descripci&oacute;n</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
					contenidoArchivoStr.append("</tr>");

						int contador=1;
						int residuo=0;
						residuo=contador % 2 ;
						String bgcolor="textabdatobs";
						if (residuo == 0){
							bgcolor="textabdatcla";
						}
						//10272002
						/**
						*		Modificado por Miguel Cortes Arellano
						*		Fecha: 19/12/03
						*		Proposito: Crear dinamicamente los horarios premier dependiendo del usuario de la presente session.
						*		Empieza codigo SINAPSIS
						*/
						String encFechApliformato = "";
						if (encFechApli.length() >= 8 )
							encFechApliformato = encFechApli.substring(2,4) + "/" + encFechApli.substring(0,2) + "/" + encFechApli.substring(4,8);
						else
							encFechApliformato = encFechApli;

						if(FacultadPremier.indexOf("PremierVerdadero") != -1)
						{
							HorarioPremier = obtenHorarioPremier(request, response);//Llama servicio tuxedo para obtener horarios premier. Un string separados por PIPES (|)
							if(HorarioPremier.indexOf("NOMI0000")!= -1){
									tempHorarioPremier = HorarioPremier.substring(9);
									getHorario = new StringTokenizer(tempHorarioPremier,"|");
									SelectHorarioPremier.append("<SELECT NAME=HorarioPremier><BR>");
									SelectHorarioPremier.append("<OPTION VALUE=\"\" SELECTED></OPTION><BR>");

									/*************************************************************************
									  Validacion de la fecha de aplicacion para determinar
									  horarios de fin de semana
									*************************************************************************/
									EIGlobal.mensajePorTrace("***NomImportNomina.class Validando el dia de aplicacion", EIGlobal.NivelLog.INFO);

									GregorianCalendar fechaAplicacion=new GregorianCalendar();
									fechaAplicacion=getFechaString(encFechApliformato);
									EIGlobal.mensajePorTrace(encFechApliformato, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***NomImportNomina.class Dia N&uacute;mero de aplicaci&oacute;n: " + fechaAplicacion.get(Calendar.DAY_OF_WEEK), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***NomImportNomina.class Mes de aplicacion: " + fechaAplicacion.get(Calendar.MONTH), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***NomImportNomina.class Dia de aplicacion: " + fechaAplicacion.get(Calendar.DATE), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***NomImportNomina.class A�o de aplicacion: " + fechaAplicacion.get(Calendar.YEAR), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***NomImportNomina.class tempHorarioPremier: " + tempHorarioPremier, EIGlobal.NivelLog.INFO);

									if(sess.getAttribute("HOR_SAT")!=null)
										sess.removeAttribute("HOR_SAT");
									if( fechaAplicacion.get(Calendar.DAY_OF_WEEK)==7)
									 {
										SelectHorarioPremier.append("<option VALUE=06:00>06:00</option><br>");
										SelectHorarioPremier.append("<option VALUE=11:00>11:00</option><br>");
										tempHorarioPremier="06:00|11:00|";
									 }
									else
								     {
										while (getHorario.hasMoreTokens())
										{//Split el ID y el horario que se obtiene en el primer elemento separado por Pipes(|). El segundo elemento viene separado por ;
											Horario = getHorario.nextToken();
											SelectHorarioPremier.append("<OPTION VALUE=");
											SelectHorarioPremier.append(Horario);
											SelectHorarioPremier.append(">");
											SelectHorarioPremier.append(Horario);
											SelectHorarioPremier.append("</OPTION><BR>");
										}
									 }
									/*************************************************************************/

									SelectHorarioPremier.append("</SELECT><BR>");
									EIGlobal.mensajePorTrace("***ImportNomina.class Nuevos tempHorarioPremier: " + tempHorarioPremier, EIGlobal.NivelLog.INFO);
									//Se inicializa una variable de session la cual mantiene los horarios pertinentes
									sess.setAttribute("SelectHorarioPremier",tempHorarioPremier);
							}
						}
						// Finaliza Codigo Sinapsis
						String sumTmp = sumImpTotal.substring(0, sumImpTotal.length() - 2) + "." + sumImpTotal.substring(sumImpTotal.length() - 2);
						//String importe1 = FormatoMoneda.formateaMoneda(new Double(sumImpTotal).doubleValue());
						String importe1 = FormatoMoneda.formateaMoneda(new Double(sumTmp).doubleValue());
			    		contenidoArchivoStr.append("<tr><td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(session.getCuentaCargo() );
						contenidoArchivoStr.append("&nbsp;</td>");

			    		contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(sess.getAttribute("descpCuentaCargo") );
						contenidoArchivoStr.append("&nbsp;</td>");

					    contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(encFechApliformato );
						contenidoArchivoStr.append("&nbsp;</td>");

						contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(importe1);
						contenidoArchivoStr.append("&nbsp;</td>");

						contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(session.getNominaNumberOfRecords());
						contenidoArchivoStr.append("&nbsp;</td>");

						contenidoArchivoStr.append("</tr>");
						contenidoArchivoStr.append("<tr><td width=\"40\" valign=top >&nbsp;</td><td valign=middle width=\"120\"  class=textabdatcla>Folio:&nbsp;<nobr/><INPUT TYPE=\"text\" SIZE=15 name=\"folio\" value=\"\" onkeypress=\"return validaNumero(event);\"></td>");
						contenidoArchivoStr.append("<td width=\"80\"  class=textabdatcla>");
						/**
						*		Modificado por Miguel Cortes Arellano
						*		Fecha: 19/12/03
						*		Proposito: Crear dinamicamente los horarios premier dependiendo del usuario de la presente session.
						*		Empieza codigo SINAPSIS
						*/
						if(HorarioPremier.indexOf("NOMI0000")!= -1&&FacultadPremier.indexOf("PremierVerdadero") != -1)
					    {
							contenidoArchivoStr.append("<center>Hora de aplicaci&oacute;n");
							contenidoArchivoStr.append(SelectHorarioPremier.toString());
							contenidoArchivoStr.append("</center></td><td colspan=2 valign=top width=\"160\"  class=textabdatcla>Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n");
				        }
						//Finaliza Codigo SINAPSIS
						contenidoArchivoStr.append("</td></tr>");

						/*************** Nomina Concepto de Archvo */

						if(!registroContieneConcepto)
					     {
							contenidoArchivoStr.append("		<tr>");
							contenidoArchivoStr.append("		 <td ></td>");
							contenidoArchivoStr.append("		 <td class='textabdatobs' align=center> Concepto de Pago de Archivo</td>");
							contenidoArchivoStr.append("		 <td class='textabdatobs' colspan=3>");
							contenidoArchivoStr.append("		   <select name=Concepto class='textabdatobs'>");
							contenidoArchivoStr.append(getConceptos());
							contenidoArchivoStr.append("		   </select>");
							contenidoArchivoStr.append("		 </td>");
							contenidoArchivoStr.append("		</tr>");
						 }

						/*************** Nomina Concepto de Archvo */

						contenidoArchivoStr.append("</table>");
						contenidoArchivoStr.append("<br><table align=center border=0 cellspacing=0 cellpadding=0>");


						if( session.getToken().getStatus() == 1 ){
							contenidoArchivoStr.append("<tr><td></td><td><A href = \"javascript:js_envioConOTP();\" border = 0><img src = /gifs/EnlaceMig/gbo25520.gif border=0 alt=Envio></a></td>");
						} else {
							contenidoArchivoStr.append("<tr><td></td><td><A href = \"javascript:js_envio();\" border = 0><img src = /gifs/EnlaceMig/gbo25520.gif border=0 alt=Envio></a></td>");
						}


						contenidoArchivoStr.append("<td><A href = javascript:js_regresarPagos(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td></tr></table></td></tr>");
				} // ArchvioValido

					EIGlobal.mensajePorTrace("***NomImportNomina.class 1&El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);

			//String descargaArchivo = "";
/*			try {
				//EIGlobal.mensajePorTrace("***ImportNomina.class &El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);
				if ( originalName.substring( originalName.length() - 4,
						originalName.length() - 3 ).equals( "." ) )
			        originalName = originalName.substring( 0, originalName.length() - 4 ) + ".nom";
				else
					originalName = originalName + ".nom";
			} catch( Exception e ) {
				//EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion al agregar extension al archivo: %execute()"+e, EIGlobal.NivelLog.INFO);
				originalName = originalName+".nom";
			}
			//EIGlobal.mensajePorTrace("***ImportNomina.class &El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);
			descargaArchivo="document.location='"+"/Download/" + originalName + "'";*/
			String archivoName = "";
				archivoName = (String) sess.getAttribute("nombreArchivo");
			//***********************************************
			//modificacion para integracion pva 07/03/2002
            //***********************************************
	         session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
            //***********************************************
	//	if (bandera)
	//	{
			if ( archivoValido )
			{
				/*		if ( archR.copiaLocalARemoto( archivoName.substring(archivoName.lastIndexOf("/") + 1 ), "WEB" ) ) {
							session.setRutaDescarga( "/Download/" + ( archivoName.substring(archivoName.lastIndexOf("/") + 1 ) ) );
						} else {
						}*/
						request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
						sess.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
						request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr.toString());
						sess.setAttribute("ContenidoArchivo",""+contenidoArchivoStr.toString());
						//request.setAttribute("DescargaArchivo",session.getRutaDescarga());

						request.setAttribute("encFechApli",encFechApli);
						sess.setAttribute("encFechApli",encFechApli);
						String msg="";
						/**
						*		Modificado por Miguel Cortes Arellano
						*		Fecha: 23/12/03
						*		Proposito: Enviar un mensaje de error al usuario en caso de que tenga facultades de cliente Premier y no se obtenga los horarios.
						*		Empieza codigo SINAPSIS
						*/
						if(FacultadPremier.indexOf("PremierVerdadero") != -1)
					    {
							if(HorarioPremier.indexOf("NOMI0000")!= -1)
								msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros\", 1)";
							else
								msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros. Pero, no se obtuvieron los Horarios por favor trate mas tarde.\", 1)";
				        }else{
								msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros\", 1)";
						}
						//Finaliza Codigo SINAPSIS

						request.setAttribute("infoImportados",msg);
						request.setAttribute("cantidadDeRegistrosEnArchivo",session.getNominaNumberOfRecords());
						request.setAttribute( "Encabezado", CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800IIh",request));//YHG NPRE
						sess.setAttribute( "Encabezado", CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800IIh",request));//YHG NPRE

						//EIGlobal.total_lineas_leidas(registros,registros);
						evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
			} else {
				//String msg="";
				String tablaErrores="";
				for(int z=0; z<erroresEnElArchivo.size(); z++)
				{
					tablaErrores=tablaErrores+erroresEnElArchivo.elementAt(z);
				}
				request.setAttribute("archivoEstatus",tablaErrores);
				request.setAttribute("ArchivoErr","archivoErrores;");
				request.setAttribute("archivo_actual","Error al Importar: "+archivoName.substring(archivoName.lastIndexOf("/")+1));
				request.setAttribute("Cuentas","<Option value = \"\">Error en el archivo");
				request.setAttribute("ContenidoArchivo","");
				request.setAttribute("tipoArchivo","importadoErroneo");
				request.setAttribute("cantidadDeRegistrosEnArchivo","");
				//EIGlobal.total_lineas_leidas(registros,registros);
				//evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response );
				evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
			}
	//	  } // bandera
}//ejecutaimportar


	// Metodo para conocer la cantidad de registros existentes en un archivo.
	//public int registrosEnArchivo(RandomAccessFile file){

 	public int registrosEnArchivo(String pathArchivoNom, HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace( "***NomImportNomina.class &Entrando al metodo registrosEnArchivo 35&", EIGlobal.NivelLog.INFO);
		pathArchivoNom = pathArchivoNom.trim();
		BufferedReader arch = null;
		FileReader reader = null;
		reader  = new FileReader( pathArchivoNom );
		arch	= new BufferedReader( reader );
		String registroLeido = "";
		//long posicion		 = 0;
		int cantRegistros    = 0;
		try {
				do {
					registroLeido = arch.readLine();
					if (registroLeido != null)
						cantRegistros++;
					} while( registroLeido != null );
				//} while( posicion < file.length() );
		} catch(IllegalArgumentException e) {
			EIGlobal.mensajePorTrace("***NomImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(1)"+e, EIGlobal.NivelLog.INFO);
		} catch(FileNotFoundException e) {
			EIGlobal.mensajePorTrace("***NomImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(2)"+e, EIGlobal.NivelLog.INFO);
		} catch(SecurityException e){
			EIGlobal.mensajePorTrace("***NomImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(3)"+e, EIGlobal.NivelLog.INFO);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("***NomImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(4)"+e, EIGlobal.NivelLog.INFO);
		}
		//***** everis Cerrando BufferedReader 08/05/2008  ..inicio
		finally {
			try {
		if (arch!=null){
			arch.close();
		}
			}catch(IOException e) {
				e.printStackTrace();
			}
			try {
		if(reader != null) {
			reader.close();
		}
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		//****  everis Cerrando BufferedReader 08/05/2008  ..fin
		EIGlobal.mensajePorTrace("***registrosEnArchivo() cantRegistros: "+cantRegistros, EIGlobal.NivelLog.INFO);
		return cantRegistros;
	}

 //METODO PARA AGRGAR PUNTO	DECIMAL	AL IMPORTE
	public String  agregarPunto(String importe){
		 EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo agregarPunto()&", EIGlobal.NivelLog.INFO);
         String importeConPunto	= "";
         String cadena1			= importe.substring(0,importe.length()-2);
         cadena1				= cadena1.trim();
         String cadena2			= importe.substring(importe.length()-2, importe.length());
         importe				= cadena1+" "+cadena2;
         importeConPunto		= importe.replace(' ','.');
		 if(importeConPunto.startsWith(".."))
			 importeConPunto	= ".0"+cadena2.trim();
		 EIGlobal.mensajePorTrace("***ImportNomina.class &Saliendo del metodo agregarPunto()&", EIGlobal.NivelLog.INFO);
         return importeConPunto;
     }

//METODO PARA FORMATEAR	EL IMPORTE CUANDO CONTENGA DECIMALES
	public String formateaImporte(String importe){
		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo formateaImporte()&", EIGlobal.NivelLog.INFO);
		String importeFormateado	= "";
		boolean contienePunto		= false;
		int i=0;
		for (i=0; i<importe.length(); i++){
			if(importe.charAt(i)=='.'){
				contienePunto=true;
				break;
			}
		}
		if(contienePunto)
			importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
		else
			importeFormateado=importe;
		EIGlobal.mensajePorTrace("***ImportNomina.class &Saliendo del metodo formateaImporte()&", EIGlobal.NivelLog.INFO);
		return importeFormateado;
	}

	//metodo para visualizar los errores en una pagina html
	//****archivoValido=false; este metodo no se utiliza en este programa...
	public void salirDelProceso( boolean [] archivoValido, String s, String variable,
			HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer contArchivoStr=new StringBuffer("");
	    contArchivoStr.append("<table border=0 align=center>");
		contArchivoStr.append("<tr><td colspan=9 align=center><b>Pago de Nomina</b></td></tr>");
		contArchivoStr.append("<tr bgcolor=red+><td colspan=9 align=center><b>Se han detectado errores al leer el archivo</b></td></tr>");
		contArchivoStr.append("<tr><td colspan=9 align=center><b>");
		contArchivoStr.append(s);
		contArchivoStr.append("</b></td></tr>");
	    contArchivoStr.append("<tr><td colspan=9 align=center><b>");
		contArchivoStr.append(variable);
		contArchivoStr.append("</b></td></tr>");
		contArchivoStr.append("</tr></table>");
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pago de Nomina","Servicios &gt; Nomina &gt; Pagos","s25800h",request));
	    request.setAttribute("erroresEnArchivo",""+contArchivoStr.toString());
//		popup de jorge
//		despliegaPaginaError(contArchivoStr,"Pago de Nomina","Errores al momento de importar el archivo");
	    archivoValido[0]=false;
		evalTemplate(IEnlace.ERR_NOMINA_TMPL, request, response );
	}

	public String[] arregloCuentas(File nameArchivo, int numeroRegs){
	//public String[] arregloCuentas(String nameArchivo, int numeroRegs){
		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo arregloCuentas &", EIGlobal.NivelLog.INFO);
		//nameArchivo = nameArchivo.trim();
		//BufferedReader archivoLectura = null;
		String lineaQueSeLeyo	 = "";
		String[] arreglo		 = new String[numeroRegs];
		//***** everis Declaracion RandomAccessFile 08/05/2008  ..inicio
		RandomAccessFile archivoLectura = null;
		//***** everis Declaracion RandomAccessFile 08/05/2008  ..fin
		try{
			//archivoLectura = new BufferedReader( new FileReader( nameArchivo ) );
			//***** everis Declaracion RandomAccessFile 08/05/2008  ..inicio
			//RandomAccessFile archivoLectura=new RandomAccessFile(nameArchivo,"r");
			archivoLectura=new RandomAccessFile(nameArchivo,"r");
			//***** everis Declaracion RandomAccessFile 08/05/2008  ..fin
			/*try{
			//EIGlobal.mensajePorTrace("***ImportNomina.class & 4&", EIGlobal.NivelLog.INFO);
			archivoLectura.seek(41);
			lineaQueSeLeyo		= archivoLectura.readLine();
			//EIGlobal.mensajePorTrace("***ImportNomina.class & lineaQueSeLeyo5&"+ lineaQueSeLeyo, EIGlobal.NivelLog.INFO);
			if(lineaQueSeLeyo==null) {
				lineaQueSeLeyo		= archivoLectura.readLine();
			}
			} catch(Exception e){
				e.printStackTrace();
			}*/
			archivoLectura.seek(41);
			lineaQueSeLeyo = archivoLectura.readLine();
			if (lineaQueSeLeyo.length()  == 126  ){
				archivoLectura.seek(40);
				lineaQueSeLeyo = archivoLectura.readLine();
				}else {
				    archivoLectura.seek(41);
					lineaQueSeLeyo = archivoLectura.readLine();
					}
				for(int i=0; i<arreglo.length; i++){
					if ( lineaQueSeLeyo.equals( "" ) )
						lineaQueSeLeyo = archivoLectura.readLine();
					if ( lineaQueSeLeyo == null )
						break;
					if(lineaQueSeLeyo.length()>108) { //jtg  para evitar el error
						arreglo[i]		= lineaQueSeLeyo.substring(93,109);
					}else {
						i--;
						}
					lineaQueSeLeyo = archivoLectura.readLine();
				}
		   	//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
			archivoLectura.close();
			//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
		} catch(IOException exception) {
			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %arregloCuentas()"+exception, EIGlobal.NivelLog.INFO);
		} catch(Exception e){
			e.printStackTrace();
		}
		//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
		finally{
            if(null != archivoLectura){
                try{
                	archivoLectura.close();
                }catch(Exception e){}
                archivoLectura = null;
            }
        }
		//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
		return arreglo;
	}

	public String[] arregloNumeroEmpleado(File nameArchivo, int numeroRegs){
	//public String[] arregloNumeroEmpleado(String nameArchivo, int numeroRegs){
	EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo arregloNumeroEmpleado&", EIGlobal.NivelLog.INFO);
	//nameArchivo =nameArchivo.trim();
	//BufferedReader archivoLectura = null;
	String lineaQueSeLeyo	= "";
	String[] arregloNumeroEmpleado = new String[numeroRegs];
	//***** everis Declaracion RandomAccessFile 08/05/2008  ..inicio
	RandomAccessFile archivoLectura = null;
	//***** everis Declaracion RandomAccessFile 08/05/2008  ..fin
		try{
			//***** everis Declaracion RandomAccessFile 08/05/2008  ..inicio
			//RandomAccessFile archivoLectura = new RandomAccessFile(nameArchivo,"r");
			archivoLectura = new RandomAccessFile(nameArchivo,"r");
			//***** everis Declaracion RandomAccessFile 08/05/2008  ..fin
			//archivoLectura = new BufferedReader( new FileReader( nameArchivo ) );
			archivoLectura.seek(41);
			lineaQueSeLeyo = archivoLectura.readLine();

			if (lineaQueSeLeyo.length()  == 126  ){
				archivoLectura.seek(40);
				lineaQueSeLeyo = archivoLectura.readLine();
				}else {
				    archivoLectura.seek(41);
					lineaQueSeLeyo = archivoLectura.readLine();
					}

			for ( int i = 0; i < arregloNumeroEmpleado.length; i++ ) {
				if ( lineaQueSeLeyo.equals( "" ) )
					lineaQueSeLeyo = archivoLectura.readLine();
				if ( lineaQueSeLeyo == null )
					break;
				if(lineaQueSeLeyo.length()> 12){
					arregloNumeroEmpleado[i] = lineaQueSeLeyo.substring( 6, 13 );
				}else {
					i--;
					}
					lineaQueSeLeyo = archivoLectura.readLine();
			}
			//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
			archivoLectura.close();
			//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
		} catch(IOException exception) {
			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %arregloNumeroEmpleado()"+exception, EIGlobal.NivelLog.INFO);
		} catch(Exception e){
			e.printStackTrace();
		}
		//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
		finally{
            if(null != archivoLectura){
                try{
                	archivoLectura.close();
                }catch(Exception e){}
                archivoLectura = null;
            }
        }
		//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin
		return arregloNumeroEmpleado;
	}

	boolean verificaCuentaCargo(String[][] arrayCuentas, String cuentaCargo){
		for(int indice=1; indice<=Integer.parseInt(arrayCuentas[0][0]); indice++)
		{
			if (arrayCuentas[indice][1].trim().equals(cuentaCargo)&&arrayCuentas[indice][2].trim().equals("P"));
			return true;
		}
		return false;
	}

	public String formatea(String strNum)
		{
		long num = 0;
		String forma = null;
		// Se verifica que la cadena sea un numero y se quitan los posible ceros a la izq.
		try
			{num = Long.parseLong(strNum);}
		catch(NumberFormatException e)
			{System.err.println("formatea: La cadena no es un n&uacute;mero"); return null;}
		forma = "" + num;
		// Se a�ade el punto
		forma=forma.substring(0,forma.length()-2) + "." + forma.substring(forma.length()-2);
		return forma;
		}

	public String formatoMoneda( String cantidad )
	{
		EIGlobal.mensajePorTrace("***Entrando a formatoMoneda ", EIGlobal.NivelLog.INFO);
	    cantidad = cantidad.trim();
		String language = "la"; // ar
		String country  = "MX";  // AF
		Locale local    = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato  = "";
		String cantidadDecimal = "";
		String formatoFinal    = "";
		String cantidadtmp     = "";

		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.0";
		if(cantidad.length() > 2)
		{
			try
			{
				cantidadtmp =cantidad.substring(0,cantidad.length()-2);
				cantidadDecimal = cantidad.substring(cantidad.length()-2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				formato=formato.substring(1,formato.length()-2);
				formatoFinal = "$ " + formato + cantidadDecimal;
			} catch(NumberFormatException e)
			{
				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);
				formatoFinal= "$ " + "0.00";
			}
		}
		else
		{
			try
			{
		       if (cantidad.length() > 1)
		       {
		    	   cantidadDecimal = cantidad.substring(cantidad.length()-2);
		    	   formatoFinal = "$ " + "0."+ cantidadDecimal;
			   }
		       else
		       {
		    	   cantidadDecimal = cantidad.substring(cantidad.length()-1);
		    	   formatoFinal = "$ " + "0.0"+ cantidadDecimal;
		       }
			} catch(NumberFormatException e)
			{
				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);
				formatoFinal="$ " + "0.00";
			}
		}
		if(formatoFinal==null)
			formato = "";
		return formatoFinal;
	}

public GregorianCalendar getFechaString(String fecha)
	{
	  EIGlobal.mensajePorTrace("ImportNomina - getFechaString(): Fecha cambio: " + fecha, EIGlobal.NivelLog.INFO);
      try
		{
		  if(fecha.length()==10)
			{
			  int Anio = Integer.parseInt(fecha.substring(6));
			  int Mes = Integer.parseInt(fecha.substring(3,5))-1;
			  int Dia = Integer.parseInt(fecha.substring(0,2));
			  return new GregorianCalendar(Anio, Mes, Dia);
			}
		  else
			{
			  EIGlobal.mensajePorTrace("ImportNomina - getFechaString(): Error en la longitud de la fecha.", EIGlobal.NivelLog.INFO);
			}

		} catch (Exception e)
		 {
			EIGlobal.mensajePorTrace("ImportNomina - getFechaString(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
	     }
	   return null;
     }

/*************** Nomina Concepto de Archvo */
public StringBuffer getConceptos()
	{
	   EI_Query BD= new EI_Query();
	   Hashtable listaConceptos=new Hashtable();
	   StringBuffer cadenaRegreso=new StringBuffer("");

	   String sqlConcepto  = "SELECT id_concepto,Concepto from INOM_CONCEPTOS order by id_concepto";
	   listaConceptos =BD.ejecutaQuery(sqlConcepto);
	   if( listaConceptos==null )
		 {
			EIGlobal.mensajePorTrace( "ImportNomina - getConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ImportNomina - getConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);

			cadenaRegreso.append("<option value='01'>Pago de N&oacute;mina. </option>\n");
			cadenaRegreso.append("<option value='02'>Pago Vacaciones. </option>\n");
			cadenaRegreso.append("<option value='03'>Pago Gratificaciones. </option>\n");
			cadenaRegreso.append("<option value='04'>Pago por Comisiones. </option>\n");
			cadenaRegreso.append("<option value='05'>Pago por Beca. </option>\n");
			cadenaRegreso.append("<option value='06'>Pago por Pensi&oacute;n. </option>\n");
			cadenaRegreso.append("<option value='07'>Pago por Subsidios. </option>\n");
			cadenaRegreso.append("<option value='08'>Otros pagos por transferencia. </option>\n");
			cadenaRegreso.append("<option value='09'>Pago por Honorarios. </option>\n");
			cadenaRegreso.append("<option value='10'>Pr&eacute;stamo. </option>\n");
			cadenaRegreso.append("<option value='11'>Pago de vi&aacute;ticos. </option>\n");
			cadenaRegreso.append("<option value='12'>Anticipo de vi&aacute;ticos. </option>\n");
			cadenaRegreso.append("<option value='13'>Fondo de ahorro. </option>\n");
		  }
		else
		  {
			EIGlobal.mensajePorTrace( "ImportNomina - getConceptos()-> Tama�o : "+listaConceptos.size() , EIGlobal.NivelLog.INFO);
			String []datos=new String[2];
			for (int i=0;i<listaConceptos.size();i++ )
			  {
				 datos=(String[])listaConceptos.get(""+i);
				 cadenaRegreso.append("<option value='"+datos[0]+"'>");
				 cadenaRegreso.append(datos[1]);
				 cadenaRegreso.append("</option>\n");
			  }
		  }
	   return cadenaRegreso;
	}

public Vector getIDConceptos()
	{
	   EI_Query BD= new EI_Query();
	   Vector listaIDConceptos=new Vector();;

	   String sqlIDConcepto  = "SELECT id_concepto from INOM_CONCEPTOS order by id_concepto";
	   listaIDConceptos =BD.ejecutaQueryCampo(sqlIDConcepto);
	   if( listaIDConceptos==null )
		 {
			listaIDConceptos=new Vector();
			EIGlobal.mensajePorTrace( "NomImportNomina - getIDConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "NomImportNomina - getIDConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);
			for(int a=1;a<14 ;a++ )
			 {
			   String dato=(a<10)?"0"+a:""+a;
			   listaIDConceptos.add(dato);
			 }
		 }
	   return listaIDConceptos;
	}

/*************** Nomina Pago Individual */

//Funcion para validar los datos del Pago individual de la nomina

	public void ejecutaValidaPagoIndividual( HttpServletRequest request, HttpServletResponse response )
	throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace("***NomImportNomina.class &Entrando al metodo ejecutaValidaArchivo: &", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		Vector<String> erroresEnElArchivo	= new Vector<String>();
		erroresEnElArchivo.addElement("<table>");
		Vector<Object> cantidadRegistros    = new Vector<Object>();
		StringBuffer contenidoArchivoStr	= new StringBuffer("");


		String HorarioPremier = "";
		String tempHorarioPremier = "";
		StringBuffer SelectHorarioPremier = new StringBuffer("");
		StringTokenizer getHorario;
		String Horario = "";
		String FacultadPremier = "";
		FacultadPremier =(String)request.getAttribute("facultades");
		int registros = 1;
		EIGlobal.mensajePorTrace("***NomImportNomina.class &registros&"+registros, EIGlobal.NivelLog.INFO);
		int cuentaDeRegistros = registros;
		int registrosLeidos	= 0;
		registrosLeidos = cuentaDeRegistros;
		boolean correcto = true;
		ValidaArchivoPagos validaArchivo = null;

		//String encTipoReg	= "1";
		String encNumSec	= "00001";
		//String encSentido	= "E";
		String encFechGen	= "";
		String encCuenta	= "";
		String encFechApli	= "";
		String encFechApliformato = "";
		NomPreLM1D lm1d;
		lm1d = null;

		try
		{
			encFechGen = (new java.util.Date()).toString();
			EIGlobal.mensajePorTrace("***NomImportNomina.class &encFechGen:"+encFechGen, EIGlobal.NivelLog.INFO);
			//if (validaArchivo.validaDigito(encFechGen)==false||validaArchivo.fechaValida(encFechGen)==false)
			//{
			//	erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de generacion del archivo</td>");
			//	correcto = false;
			//	EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en la fecha de generacion del archivo&", EIGlobal.NivelLog.INFO);
			//}
			encCuenta = getFormParameter(request, "valorCargo").toString();
			EIGlobal.mensajePorTrace("***NomImportNomina.class &encCuenta:"+ encCuenta, EIGlobal.NivelLog.INFO);

			encFechApli = getFormParameter(request, "fecha2").toString();
			EIGlobal.mensajePorTrace("***NomImportNomina.class &encFechApli:"+encFechApli, EIGlobal.NivelLog.INFO);
			//if (validaArchivo.validaDigito(encFechApli)==false||validaArchivo.fechaValida(encFechApli)==false)
			//{
			//	erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de aplicacion del archivo</td>");
			//	correcto = false;
			//	EIGlobal.mensajePorTrace("***NomImportNomina.class &Error en la fecha de aplicacion&", EIGlobal.NivelLog.INFO);
			//}
		}
		catch(Exception e)
		{
			erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");
			correcto = false;
			EIGlobal.mensajePorTrace("***NomImportNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
		}

		session.setFechaAplicacion( encFechApli );
		//String registroEncabezadoCompleto;

		if ( encNumSec != null )
		{
			cantidadRegistros.addElement( encNumSec );
		}

		String numeroEmpleado	= "";
		String apellidoPaterno	= "";
		String apellidoMaterno	= "";
		String nombre			= "";
		String numeroCuenta		= "";
		String importe			= "";
		String nombreEmpleado   = "";
		NomPreEmpleadoDAO empleadoDAO =new NomPreEmpleadoDAO();
		NomPreLM1F demografico = null;
		EnlcTarjetaDAO tarjTrxBO;
		tarjTrxBO = new EnlcTarjetaDAOImpl();

		try {

			numeroEmpleado = getFormParameter(request,"valorEmpleado").toString();

			//Para obtener el nombre y los apellidos del empleado se lanzara una transaccion en pampa
			numeroCuenta = getFormParameter(request,"valorTarjeta").toString();

			importe = getFormParameter(request,"valorImporte").toString();
			if (importe.trim().length() == 0)
			{
				erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Error</td><td class=\"textabdatobs\" nowrap>Debe especificar importe</td>");
				correcto = false;
			}
			// Se descomenta para subirlo es el monto maximo
			NomPreConfiguracionAction action = new NomPreConfiguracionAction();
			double montoMaximo = action.obtieneMonto(null);
			EIGlobal.mensajePorTrace("Monto Maximo :" + montoMaximo, EIGlobal.NivelLog.INFO);
			if(Double.parseDouble(importe) > montoMaximo)
			{
				erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Error</td><td class=\"textabdatobs\" nowrap>No puede pagar un importe tan alto para el empleado :" + numeroEmpleado + "</td>");
				correcto=false;
			}

			demografico = empleadoDAO.consultarDatosDemograficos(session.getContractNumber(), numeroEmpleado, numeroCuenta);
			if(demografico!=null && demografico.getEmpleado() != null) {

                apellidoPaterno = demografico.getEmpleado().getPaterno();

                apellidoMaterno = demografico.getEmpleado().getMaterno();

                nombre = demografico.getEmpleado().getNombre();
			}

			
			TrxLM1JBean bean = new TrxLM1JBean();
			bean.setTarjeta(numeroCuenta);
			bean = tarjTrxBO.consultaLM1J(bean);

			FiltroConsultaBean filtros;
			filtros = new FiltroConsultaBean();
			filtros.setTarjeta(numeroCuenta);
			filtros.setContrato(session.getContractNumber());
			filtros.setEstatus("A");
			List<TarjetaContrato> tarjetas;
			tarjTrxBO.initTransac();
			tarjetas = tarjTrxBO.consultarDatosTarjetas(filtros);

			if(tarjetas.isEmpty() || bean == null || !bean.isCodExito()) {

				erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Error</td><td class=\"textabdatobs\" nowrap>El estatus de la tarjeta no es valido</td>");
				correcto=false;

			}
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("Excepcion al enviar el mail ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			correcto=false;
			EIGlobal.mensajePorTrace("***NomImportNomina.class &Error de excepcion "+ e, EIGlobal.NivelLog.DEBUG);

		}finally{
			tarjTrxBO.closeTransac();
		}

		String sumTipoReg  = "3";
		String sumNumSec   = "00001";
		String sumTotRegs  = "1";
		String sumImpTotal = importe;

		nombreEmpleado = nombre +" "+ apellidoPaterno +" "+ apellidoMaterno;

		erroresEnElArchivo.addElement("</table>");
		erroresEnElArchivo.trimToSize();//a partir de esta linea el vector erroresEnElArchivo contiene la tabla de errores

		cantidadRegistros.addElement(sumNumSec);
		if (correcto)
		{
			contenidoArchivoStr=new StringBuffer("");
			contenidoArchivoStr.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
			contenidoArchivoStr.append("<tr> ");
			contenidoArchivoStr.append("<td class=\"textabref\">Pago de n&oacute;mina Indivudual</td>");
			contenidoArchivoStr.append("</tr>");
			contenidoArchivoStr.append("</table>");
			contenidoArchivoStr.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
			contenidoArchivoStr.append("<tr> ");
			contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
			contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descripci&oacute;n</td>");
			contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
			contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe</td>");
			contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Tarjeta</td>");
			contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Empleado</td>");
			contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Nombre del Empleado</td>");
			contenidoArchivoStr.append("</tr>");

			int contador=1;
			int residuo=0;
			residuo=contador % 2 ;
			String bgcolor="textabdatobs";
			if (residuo == 0)
			{
				bgcolor="textabdatcla";
			}

			if (encFechApli.length() >= 8 )
				encFechApliformato = encFechApli.substring(3,5) + "/" + encFechApli.substring(0,2) + "/" + encFechApli.substring(6,encFechApli.length());
			else
				encFechApliformato = encFechApli;
			EIGlobal.mensajePorTrace("Fecha :" + encFechApliformato, EIGlobal.NivelLog.INFO);
			if(FacultadPremier.indexOf("PremierVerdadero") != -1)
			{
				HorarioPremier = obtenHorarioPremier(request, response);//Llama servicio tuxedo para obtener horarios premier. Un string separados por PIPES (|)
				if(HorarioPremier.indexOf("NOMI0000")!= -1)
				{
					tempHorarioPremier = HorarioPremier.substring(9);
					getHorario = new StringTokenizer(tempHorarioPremier,"|");
					SelectHorarioPremier.append("<SELECT NAME=HorarioPremier><BR>");
					SelectHorarioPremier.append("<OPTION VALUE=\"\" SELECTED></OPTION><BR>");

					/*************************************************************************
					  Validacion de la fecha de aplicacion para determinar
					  horarios de fin de semana
					*************************************************************************/
					EIGlobal.mensajePorTrace("***NomImportNomina.class Validando el dia de aplicacion", EIGlobal.NivelLog.INFO);

					GregorianCalendar fechaAplicacion=new GregorianCalendar();
					fechaAplicacion=getFechaString(encFechApli);
					EIGlobal.mensajePorTrace(encFechApli, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***NomImportNomina.class Dia N&uacute;mero de aplicaci&oacute;n: " + fechaAplicacion.get(Calendar.DAY_OF_WEEK), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***NomImportNomina.class Mes de aplicacion: " + fechaAplicacion.get(Calendar.MONTH), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***NomImportNomina.class Dia de aplicacion: " + fechaAplicacion.get(Calendar.DATE), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***NomImportNomina.class A�o de aplicacion: " + fechaAplicacion.get(Calendar.YEAR), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***NomImportNomina.class tempHorarioPremier: " + tempHorarioPremier, EIGlobal.NivelLog.INFO);

					if(sess.getAttribute("HOR_SAT")!=null)
						sess.removeAttribute("HOR_SAT");
					if( fechaAplicacion.get(Calendar.DAY_OF_WEEK)==7)
					 {
						SelectHorarioPremier.append("<option VALUE=06:00>06:00</option><br>");
						SelectHorarioPremier.append("<option VALUE=11:00>11:00</option><br>");
						tempHorarioPremier="06:00|11:00|";
					 }
					else
				     {
						while (getHorario.hasMoreTokens())
						{//Split el ID y el horario que se obtiene en el primer elemento separado por Pipes(|). El segundo elemento viene separado por ;
							Horario = getHorario.nextToken();
							SelectHorarioPremier.append("<OPTION VALUE=");
							SelectHorarioPremier.append(Horario);
							SelectHorarioPremier.append(">");
							SelectHorarioPremier.append(Horario);
							SelectHorarioPremier.append("</OPTION><BR>");
						}
					 }
					/*************************************************************************/
					SelectHorarioPremier.append("</SELECT><BR>");
					EIGlobal.mensajePorTrace("***ImportNomina.class Nuevos tempHorarioPremier: " + tempHorarioPremier, EIGlobal.NivelLog.INFO);
					//Se inicializa una variable de session la cual mantiene los horarios pertinentes
					sess.setAttribute("SelectHorarioPremier",tempHorarioPremier);
				}
			}

			//String sumTmp = sumImpTotal.substring(0, sumImpTotal.length() - 2) + "." + sumImpTotal.substring(sumImpTotal.length() - 2);
			//String importe1 = FormatoMoneda.formateaMoneda(new Double(sumImpTotal).doubleValue());
			//String importe1 = FormatoMoneda.formateaMoneda(new Double(sumTmp).doubleValue());
			String importe1 = FormatoMoneda.formateaMoneda(new Double(importe).doubleValue());
    		contenidoArchivoStr.append("<tr><td class=\"" );
			contenidoArchivoStr.append(bgcolor );
			contenidoArchivoStr.append("\" nowrap align=\"center\">");
			contenidoArchivoStr.append(encCuenta);
			contenidoArchivoStr.append("&nbsp;</td>");

    		contenidoArchivoStr.append("<td class=\"" );
			contenidoArchivoStr.append(bgcolor );
			contenidoArchivoStr.append("\" nowrap align=\"center\">");
			contenidoArchivoStr.append(getFormParameter(request, "textcuenta").toString());
			contenidoArchivoStr.append("&nbsp;</td>");

		    contenidoArchivoStr.append("<td class=\"" );
			contenidoArchivoStr.append(bgcolor );
			contenidoArchivoStr.append("\" nowrap align=\"center\">");
			contenidoArchivoStr.append(encFechApli);
			contenidoArchivoStr.append("&nbsp;</td>");

			contenidoArchivoStr.append("<td class=\"" );
			contenidoArchivoStr.append(bgcolor);
			contenidoArchivoStr.append("\" nowrap align=\"center\">");
			contenidoArchivoStr.append(importe1);
			contenidoArchivoStr.append("&nbsp;</td>");

			contenidoArchivoStr.append("<td class=\"" );
			contenidoArchivoStr.append(bgcolor );
			contenidoArchivoStr.append("\" nowrap align=\"center\">");
			contenidoArchivoStr.append(numeroCuenta);
			contenidoArchivoStr.append("&nbsp;</td>");

			contenidoArchivoStr.append("<td class=\"" );
			contenidoArchivoStr.append(bgcolor );
			contenidoArchivoStr.append("\" nowrap align=\"center\">");
			contenidoArchivoStr.append(numeroEmpleado);
			contenidoArchivoStr.append("&nbsp;</td>");

			contenidoArchivoStr.append("<td class=\"" );
			contenidoArchivoStr.append(bgcolor );
			contenidoArchivoStr.append("\" nowrap align=\"center\">");
			contenidoArchivoStr.append(nombreEmpleado);
			contenidoArchivoStr.append("&nbsp;</td>");

			contenidoArchivoStr.append("</tr>");

			contenidoArchivoStr.append("<tr><td width=\"40\" valign=top >&nbsp;</td><td width=\"40\" valign=top >&nbsp;</td>");
			contenidoArchivoStr.append("<td valign=middle width=\"120\" class=textabdatcla colspan=2>Folio:&nbsp;<nobr/><INPUT TYPE=\"text\" SIZE=15 name=\"folio\" value=\"\" onkeypress=\"return validaNumero(event);\"></td>");
			contenidoArchivoStr.append("<td width=\"80\"  class=textabdatcla>");

			if(HorarioPremier.indexOf("NOMI0000")!= -1&&FacultadPremier.indexOf("PremierVerdadero") != -1)
		    {
				contenidoArchivoStr.append("<center>Hora de aplicaci&oacute;n");
				contenidoArchivoStr.append(SelectHorarioPremier.toString());
				contenidoArchivoStr.append("</center></td><td colspan=2 valign=top width=\"160\"  class=textabdatcla>Recuerde que los pagos deben realizarse con media hora de anticipaci&oacute;n");
	        }
			contenidoArchivoStr.append("</td></tr>");

			contenidoArchivoStr.append("</table>");
			contenidoArchivoStr.append("<br><table align=center border=0 cellspacing=0 cellpadding=0>");
			contenidoArchivoStr.append("<tr><td></td><td><A href = \"javascript:js_envio();\" border = 0><img src = /gifs/EnlaceMig/gbo25520.gif border=0 alt=Envio></a></td>");
			contenidoArchivoStr.append("<td><A href = javascript:js_regresarPagosInd() border = 0><img src = ../../gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td></tr></table></td></tr>");

			request.setAttribute("encFechApliformato", encFechApliformato);
			sess.setAttribute("importe1", importe);
			request.setAttribute("SelectHorarioPremier", SelectHorarioPremier.toString());
			sess.setAttribute("num_empleado", numeroEmpleado);
			sess.setAttribute("num_tarjeta", numeroCuenta);
			sess.setAttribute("num_cuenta", encCuenta);
			sess.setAttribute("paterno", apellidoPaterno);
			sess.setAttribute("materno", apellidoMaterno);
			sess.setAttribute("nombre", nombre);
		} // ArchvioValido

		EIGlobal.mensajePorTrace("***NomImportNomina.class & Antes de bitacorizar:", EIGlobal.NivelLog.INFO);
		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
		BitaTransacBean bt = new BitaTransacBean();
		String codErr=null;
		if(importe!=null)
			bt.setImporte(Double.parseDouble(importe));
		if(numeroCuenta!=null)
			bt.setCctaOrig(encCuenta);
		if(numeroCuenta!=null)
			bt.setCctaDest(numeroCuenta);
		if(nombreEmpleado!=null && numeroEmpleado!=null)
			bt.setBeneficiario(numeroEmpleado+"@"+nombreEmpleado);
		if(encFechApliformato!=null)
			try {
				bt.setFechaAplicacion(sdf.parse(encFechApliformato));
			} catch (ParseException e) {
			}
		if(lm1d!=null && lm1d.getCodigoOperacion()!=null){
			codErr=lm1d.getCodigoOperacion();
		}
		bt.setNumBit(BitaConstants.ES_NP_PAGOS_INDIVIDUAL_CONSULTA);
		NomPreUtil.bitacoriza(request,"LM1D",codErr , bt);

		//String archivoName = "";
		//archivoName = (String) sess.getAttribute("nombreArchivo");

		session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);

		if (correcto)
		{
			//request.setAttribute("archivo_actual",archivoName);
			//sess.setAttribute("archivo_actual",archivoName);
			request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr.toString());
			sess.setAttribute("ContenidoArchivo",""+contenidoArchivoStr.toString());

			request.setAttribute("encFechApli",encFechApli);
			sess.setAttribute("encFechApli",encFechApli);
			String msg="";

			if(FacultadPremier.indexOf("PremierVerdadero") != -1)
		    {
				if(HorarioPremier.indexOf("NOMI0000")!= -1)
					msg = "cuadroDialogo(\"Se capturo exitosamente: "+sumTotRegs+" registros\", 1)";
				else
					msg = "cuadroDialogo(\"Se importaron exitosamente: "+sumTotRegs+" registros. Pero, no se obtuvieron los Horarios por favor trate mas tarde.\", 1)";
	        }else
	        {
					//msg = "cuadroDialogo(\"Se realizo el pago para el empleado: "+nombreEmpleado+" exitosamente\", 1)";
			}

			request.setAttribute("infoImportados",msg);
			request.setAttribute("cantidadDeRegistrosEnArchivo",registrosLeidos);
			request.setAttribute( "Encabezado", CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800IIh",request));//YHG NPRE
			sess.setAttribute( "Encabezado", CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800IIh",request));//YHG NPRE

			evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
		}
		else
		{
			String tablaErrores="";
			for(int z=0; z<erroresEnElArchivo.size(); z++)
			{
				tablaErrores=tablaErrores+erroresEnElArchivo.elementAt(z);
				EIGlobal.mensajePorTrace(erroresEnElArchivo.get(z), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(erroresEnElArchivo.elementAt(z), EIGlobal.NivelLog.DEBUG);
			}
			EIGlobal.mensajePorTrace(tablaErrores, EIGlobal.NivelLog.DEBUG);
			request.setAttribute("archivoEstatus",tablaErrores);
			request.setAttribute("ArchivoErr","archivoErrores;");
			//request.setAttribute("archivo_actual","Error al Importar: "+archivoName.substring(archivoName.lastIndexOf("/")+1));
			request.setAttribute("Cuentas","<Option value = \"\">Error en el archivo");
			request.setAttribute("ContenidoArchivo","");
			request.setAttribute("tipoArchivo","importadoErroneo");
			request.setAttribute("cantidadDeRegistrosEnArchivo","");
			request.setAttribute( "Encabezado", CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800IIh",request));//YHG NPRE
			evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
		}
	}//ejecutaimportar

	private boolean validaNumero(String cadena){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaDigito&", EIGlobal.NivelLog.INFO);
        String cadenaRecibida;
		if(cadena==null)
			cadena = "";
        cadenaRecibida     = cadena.trim();
        int longitudDelArreglo;
        longitudDelArreglo = cadenaRecibida.length();
        char [] miArreglo;
        miArreglo          = new char [longitudDelArreglo];
        boolean contieneLetra;
        contieneLetra      = false;

        for (int i=0; i<miArreglo.length; i++){
		            miArreglo[i]=cadenaRecibida.charAt(i);
                if (!Character.isDigit(miArreglo[i])){
                    contieneLetra=true;
                    break;
                }
        }
   		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &saliendo del metodo validaDigito&", EIGlobal.NivelLog.INFO);

        if (contieneLetra){
            return false;
        }
        else{
            return true;
        }

	}

}
