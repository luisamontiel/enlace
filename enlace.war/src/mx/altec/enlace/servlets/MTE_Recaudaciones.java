package mx.altec.enlace.servlets;

import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.text.*;
import javax.naming.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.lang.reflect.*;
import java.lang.*;
import java.sql.*;

//VSWF


public class MTE_Recaudaciones extends BaseServlet
  {

	public void doGet( HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse )
	throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "MTE_Recaudaciones-> Metodo DoGet", EIGlobal.NivelLog.INFO);
		defaultAction( httpservletrequest, httpservletresponse );
	}

	public void doPost( HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse ) throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "MTE_Recaudaciones-> Metodo DoPost", EIGlobal.NivelLog.INFO);
		defaultAction( httpservletrequest, httpservletresponse );
	}

    public void defaultAction( HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse )
	 throws IOException, ServletException
	 {
	   System.out.println("Entrando a TESOFE2 Recaudaciones....");
	     boolean flag = SesionValida(httpservletrequest, httpservletresponse);
        HttpSession httpsession = httpservletrequest.getSession();
        BaseResource session = (BaseResource)httpservletrequest.getSession().getAttribute("session");


		 if(flag)
        {
			 /*************************************************PENDIENTE Cambiar la facultad*/
		     if(
			   session.getFacultad(session.FAC_RECDET) || session.getFacultad(session.FAC_RECCON) ||
			   session.getFacultad(session.FAC_IMPDEV) || session.getFacultad(session.FAC_IMPCAN) ||
			   session.getFacultad(session.FAC_ENVDEV) || session.getFacultad(session.FAC_ENVCAN) ||
			   session.getFacultad(session.FAC_CONREG) || session.getFacultad(session.FAC_CONARC)
		       )
   			 /*************************************************PENDIENTE Cambiar la facultad*/
	        {

          String operacion=(String)httpservletrequest.getParameter("operacion");
          String Modulo = (String) httpservletrequest.getParameter("Modulo");

		  if (operacion==null )
		    operacion="TESOFE2";
		  if(Modulo== null)
		     Modulo="0";


				EIGlobal.mensajePorTrace( "MTE_Recaudaciones Evaluando para descarga >>"+ flag, EIGlobal.NivelLog.INFO);
				if(operacion.trim().equals("TESOFE2") )
				{
					//TODO BIT CU 4411

					String contrato          = session.getContractNumber();
					String usuario           = session.getUserID8();
					String clavePerfil       = session.getUserProfile();
					String Result            = "";
				    String Bandera = (String)httpservletrequest.getParameter("Bandera");
		   	        String Sucursal = (String)httpservletrequest.getParameter("Sucursal");
			        String fechaIni = (String)httpservletrequest.getParameter("fechaIni");
			        String fecha_hoy = ObtenFecha(true);

					EIGlobal.mensajePorTrace( "MTE_Recaudaciones detectando Modulo: " + Modulo + " operacion: " + operacion + " contrato: " + contrato + " fecha_hoy" + fecha_hoy + " Sucursal " + Sucursal, EIGlobal.NivelLog.INFO);

				   if(Modulo.trim().equals("0"))
					Tesofe_Inicio(Result,clavePerfil, usuario, contrato,fechaIni,httpservletrequest,httpservletresponse);
				   else if (Modulo.trim().equals("1"))
		            generaTablaConsultar(Result,clavePerfil, usuario, contrato,Bandera,Sucursal,fechaIni,fecha_hoy,httpservletrequest,httpservletresponse);
				   else if (Modulo.trim().equals("2"))
				   {
						EIGlobal.mensajePorTrace("Sucursal es "+ Sucursal, EIGlobal.NivelLog.INFO);
						httpservletrequest.setAttribute("Sucursal",Sucursal);
						String[] arrSucursal=null;
						arrSucursal=desentrama(Sucursal,'|');
						generaTablaDetalle(Result,clavePerfil, usuario, contrato,Bandera,arrSucursal[0],arrSucursal[1],fechaIni,fecha_hoy,httpservletrequest,httpservletresponse);
					}

				}


         }
		 else
		 {
            String laDireccion = "document.location='SinFacultades'";
		   httpservletrequest.setAttribute( "plantillaElegida", laDireccion);
		   httpservletrequest.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(httpservletrequest,httpservletresponse);
		 }
	  }


 	 }
/*************************************************** Tesofe_Inicio_Servlet **************************/
/***************************************************       Modulo=0        **************************/
	public void Tesofe_Inicio(String Result,String clavePerfil, String usuario, String contrato, String fechaIni,HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String strInhabiles = armaDiasInhabilesJS();
		StringBuffer varFechaHoy=new StringBuffer();

		EIGlobal.mensajePorTrace( "MTE_Recaudaciones Entrando prueba Tesofe_Inicio()", EIGlobal.NivelLog.INFO);

  		/************************************************** Arreglos de fechas para 'Hoy' ... */
		varFechaHoy.append("  /*Fecha De Hoy*/");
		varFechaHoy.append("\n  dia=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes=new Array(");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n\n  /*Fechas Seleccionadas*/");
		varFechaHoy.append("\n  dia1=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes1=new Array(");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio1=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  Fecha=new Array('");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("','");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("');");
		req.setAttribute("DiasInhabiles",strInhabiles);
		req.setAttribute("VarFechaHoy",varFechaHoy.toString());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta Recaudaci&oacute;n","Servicios &gt; TESOFE &gt Recaudaciones","s55110h",req));
		EIGlobal.mensajePorTrace( "MTE_Recaudaciones Enviando las nuevas variables Tesofe....", EIGlobal.NivelLog.INFO);
		//TODO BIT CU 4411
		/*
		 * VSWF BMB -I
		 */
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
		BitaHelper bh = new BitaHelperImpl(req, session, sess);


		 if(req.getParameter(BitaConstants.FLUJO)!=null){
			   bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
		   }else{
			   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
		   }
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ES_TESOFE_RECAUDACIONES_ENTRA);
		bt.setContrato(contrato);

		try{
			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){

			e.printStackTrace();
		}
		}
		/*
		 * VSWF BMB -F
		 */
		evalTemplate ("/jsp/MTE_RecaudacionInicio.jsp", req, res);
	}

/*************************************************************************************/
/************************************************************* Arma dias inhabiles   */
/*************************************************************************************/
  String armaDiasInhabilesJS()
   {
      String resultado="";
      Vector diasInhabiles;

	  int indice=0;

      diasInhabiles=CargarDias(1);
      if(diasInhabiles!=null)
	   {
		 resultado=diasInhabiles.elementAt(indice).toString();
		 for(indice=1; indice<diasInhabiles.size(); indice++)
  		   resultado += ", " + diasInhabiles.elementAt(indice).toString();
       }
      return resultado;
	}

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 */
/*************************************************************************************/
  public Vector CargarDias(int formato)
	{

	  EIGlobal.mensajePorTrace("***MTE_Recaudaciones Entrando a CargaDias ", EIGlobal.NivelLog.INFO);

	  Connection  Conexion = null;
	  PreparedStatement qrDias = null;
	  ResultSet rsDias = null;
	  String     sqlDias;
	  Vector diasInhabiles = new Vector();

	  boolean    estado;

	  sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2)";
	  EIGlobal.mensajePorTrace("MTE_Recaudaciones Query"+sqlDias, EIGlobal.NivelLog.ERROR);
	  try
	   {
		 Conexion = createiASConn(Global.DATASOURCE_ORACLE);
		 if(Conexion!=null)
		  {
		    qrDias = Conexion.prepareStatement(sqlDias);
		    if(qrDias!=null)
		     {
		  	  rsDias = qrDias.executeQuery();
		       if(rsDias!=null)
		        {
		          while( rsDias.next())
					{
		              String dia  = rsDias.getString(1);
		              String mes  = rsDias.getString(2);
		              String anio = rsDias.getString(3);
		              if(formato == 0)
		               {
		                  dia  =  Integer.toString( Integer.parseInt(dia) );
		                  mes  =  Integer.toString( Integer.parseInt(mes) );
		                  anio =  Integer.toString( Integer.parseInt(anio) );
		               }

		              String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
		              diasInhabiles.addElement(fecha);
					  /********************/
		              }
		       }
			  else
		         EIGlobal.mensajePorTrace("MTE_Recaudaciones  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
		    }
		   else
		      EIGlobal.mensajePorTrace("MTE_Recaudaciones  Cannot Create Query", EIGlobal.NivelLog.ERROR);
		 }
		else
		 EIGlobal.mensajePorTrace("MTE_Recaudaciones Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
	   }catch(SQLException sqle ){
	   	sqle.printStackTrace();
	   } finally {
		   EI_Query.cierraConexion(rsDias, qrDias, Conexion);
	   }
	 return(diasInhabiles);
	}

/*************************************************************************************/
/******************************************************************* fecha Consulta  */
/*************************************************************************************/
   String generaFechaAnt(String tipo)
    {
      String strFecha = "";
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);

      do {
         Cal.add(Calendar.DATE, -1);
       }while(Cal.get(Calendar.DAY_OF_WEEK) == 1 || Cal.get(Calendar.DAY_OF_WEEK) == 7);

      if(tipo.equals("FECHA")) {
         if(Cal.get(Calendar.DATE) <= 9)
           strFecha += "0" + Cal.get(Calendar.DATE) + "/";
         else
           strFecha += Cal.get(Calendar.DATE) + "/";

         if(Cal.get(Calendar.MONTH)+ 1 <= 9)
           strFecha += "0" + (Cal.get(Calendar.MONTH) + 1) + "/";
         else
           strFecha += (Cal.get(Calendar.MONTH) + 1) + "/";

         strFecha+=Cal.get(Calendar.YEAR);
        }

      if(tipo.equals("DIA"))
        strFecha += Cal.get(Calendar.DATE);

      if(tipo.equals("MES"))
        strFecha += (Cal.get(Calendar.MONTH) + 1);

      if(tipo.equals("ANIO"))
        strFecha += Cal.get(Calendar.YEAR);

      return strFecha;
    }

/************************************ Consulta General Sucursales Modulo=1 ******************************/
/********************************************************************************************************/
    public int generaTablaConsultar(String Result, String clavePerfil, String usuario, String contrato, String Bandera,String Sucursal,String fechaIni,String fecha_hoy, HttpServletRequest req, HttpServletResponse res)throws ServletException, IOException
     {
       String Trama="";
 	   String identi = "0";
	   StringBuffer botones=new StringBuffer("");
	   StringBuffer tablaRegistros=new StringBuffer("");
	   String mensajeError="";
	   String tipoOperacion="TF06"; /*servicio de consulta recaudacion general.*/
       Hashtable htResult=null;
       boolean comuError=false;

	   int pagina=Integer.parseInt((String) req.getParameter("pagina"));
       int regPagina=Integer.parseInt((String) req.getParameter("Numero"));
       int fin=0;

	  /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
	   EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): Generando Fechas de consulta.", EIGlobal.NivelLog.INFO);

      /************************************** Prepara servicio recupera TCTRED llamada 1***/

		boolean RECCON=session.getFacultad(session.FAC_RECCON);
		EIGlobal.mensajePorTrace( "MTE_Recaudaciones.java Facultad RECCON >" + RECCON, EIGlobal.NivelLog.INFO);

  if(RECCON)
  {

	  EIGlobal.mensajePorTrace("La BANDERA = " + Bandera, EIGlobal.NivelLog.INFO);
	  if(Bandera.equals("0"))
		{
			EIGlobal.mensajePorTrace("Se ejecuta el llamado a TUXEDO......", EIGlobal.NivelLog.INFO);

				  /*Cabecera*/
	       Trama = IEnlace.medioEntrega2 + "|" +
		  	usuario 			         + "|" +
			tipoOperacion				 + "|" +
	       	contrato					 + "|" +
		    usuario 					 + "|" +
	       	clavePerfil 				 + "|"+
			fechaIni					 + "|" +
	   		identi						 + "|";

		    EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): Trama enviada-1 TF06: "+Trama, EIGlobal.NivelLog.DEBUG);

			ServicioTux tuxGlobal = new ServicioTux();
		   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		   try {
				htResult = tuxGlobal.web_red( Trama );
				Result = ( String ) htResult.get( "BUFFER" );
			   } catch(Exception e){}

				EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): Trama regreso 0: "+Result, EIGlobal.NivelLog.DEBUG);
		}
	 else
		{
 			Result="OK@0@"+Global.DIRECTORIO_REMOTO_TESOFE+"/"+usuario+".rtss|";
		}

/*************************************** Recuperar Recaudación TCTArchivo *********/
     String cadenaArchivo="";
     String arcLinea="";
	 StringBuffer cadenaTCT=new StringBuffer("");
     String nombreArchivo="";

     boolean status=false;
     int totalLineas=0;

	 EI_Tipo TCTArchivo = new EI_Tipo(this);
	 EI_Tipo TCTrama = new EI_Tipo(this);

   if (  ( Result == null || Result.equals("null") )  && Bandera.equals("0") )
   {
		comuError = true;
		mensajeError = "Problemas en el servicio de consulta. Por favor intente mas tarde.";
       despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones   &gt; Consultas", "s55120h",req, res);
   }
  else
   {
			if(! Result.substring(0,2).equals("OK"))
				{

					nombreArchivo=usuario;

			      		 /***********************************************************  Copia Remota*/
						   ArchivoRemoto archR = new ArchivoRemoto();
						   String arcRegreso=Global.DIRECTORIO_REMOTO + "/" + nombreArchivo;
						   if(!archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
							 {
	  					      EIGlobal.mensajePorTrace("generaTablaConsultar(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
							  mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
 				  	          despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt  TESOFE &gt; Recaudaciones  &gt; Consultas", "s55120h",req, res);
							 }
			   				   /************************************************************/
						  else
						    {
  		 		              EI_Exportar ArcEnt=new EI_Exportar(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivo);
 		  	  			      EIGlobal.mensajePorTrace("generaTablaConsultar nombreArchivo enviado a EI_Exportar: "+nombreArchivo, EIGlobal.NivelLog.DEBUG);
					          comuError = true;

						    		if(ArcEnt.abreArchivoLectura())
								     {
								 	  	boolean noError = true;
										arcLinea=ArcEnt.leeLinea();
					    			    mensajeError=arcLinea.substring(16,arcLinea.length());
									    EIGlobal.mensajePorTrace("mensajeError: "  +  mensajeError, EIGlobal.NivelLog.INFO);
										status=true;
										ArcEnt.cierraArchivo();
							            despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones &gt; Consultas", "s55120h",req, res);
    								}
						    }
				}
	   else
	      {
			   TCTrama.iniciaObjeto('@','|',Result.toString());

		       nombreArchivo = TCTrama.camposTabla[0][2].substring( TCTrama.camposTabla[0][2].lastIndexOf( "/" ) + 1 );

		   		 /***********************************************************  Copia Remota*/
					if(Bandera.equals("0"))
					{
								ArchivoRemoto archR = new ArchivoRemoto();
								String arcRegreso=Global.DIRECTORIO_REMOTO_TESOFE + "/" + nombreArchivo;
								if(!archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
		  		  		           EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
					   }
				 /************************************************************/
   		   	   EI_Exportar ArcEnt = new EI_Exportar(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivo);
			   EIGlobal.mensajePorTrace("nombreArchivo enviado a EI_Exportar: "+nombreArchivo, EIGlobal.NivelLog.DEBUG);

			   	  if(ArcEnt.abreArchivoLectura())
				     {
			   			  	    boolean noError = true;
								do
								 {
									arcLinea=ArcEnt.leeLinea();
									 status=true;

										   if(!arcLinea.equals("ERROR"))
											{
												totalLineas++;
												cadenaTCT.append(arcLinea.substring(0,arcLinea.length()-1));
												cadenaTCT.append(" @");
											}
										else
												noError=false;

				  		          }while(noError);
        			   ArcEnt.cierraArchivo();
				     }
			  EIGlobal.mensajePorTrace(">>>>>>>> Estatus lectura de archivo correcta: "+ status, EIGlobal.NivelLog.INFO);

						 if(totalLineas<=1 && !status)
						  {
							comuError = true;
		  			   	    mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
   				            EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): No se encontro el archivo.", EIGlobal.NivelLog.INFO);
						  }

				/********************************************************************************/
			  EIGlobal.mensajePorTrace("TCTArchivo.totalRegistros: "+TCTArchivo.totalRegistros, EIGlobal.NivelLog.DEBUG);

				   if(!comuError)
				     {
						       TCTArchivo.iniciaObjeto(';','@',cadenaTCT.toString());

							   String[] impG=new String[5];
							   impG[0]=sumaImportes(TCTArchivo, 2);
							   impG[1]=sumaImportes(TCTArchivo, 3);
							   impG[2]=sumaImportes(TCTArchivo, 4);
							   impG[3]=sumaImportes(TCTArchivo, 3);
							   impG[4]=sumaImportes(TCTArchivo,6);

							   if(req.getSession().getAttribute("ImporteG_0")!=null)
								   req.getSession().removeAttribute("ImporteG_0");
							   req.getSession().setAttribute("ImporteG_0",impG[4]); /* Columna importe sin SBC;0 */


							   EIGlobal.mensajePorTrace("MTE_Recaudaciones - Importe 1 en sesion: " +(String)req.getSession().getAttribute("ImporteG_0") , EIGlobal.NivelLog.INFO);

							   EIGlobal.mensajePorTrace("MTE_Recaudaciones - Importe 1 en archivo: " +impG[0] , EIGlobal.NivelLog.INFO);
							   EIGlobal.mensajePorTrace("MTE_Recaudaciones - Importe 2 en archivo: " +impG[1] , EIGlobal.NivelLog.INFO);
							   EIGlobal.mensajePorTrace("MTE_Recaudaciones - Importe 3 en archivo: " +impG[2] , EIGlobal.NivelLog.INFO);
							   EIGlobal.mensajePorTrace("MTE_Recaudaciones - Importe 4 en archivo: " +impG[3] , EIGlobal.NivelLog.INFO);
							   EIGlobal.mensajePorTrace("MTE_Recaudaciones - Importe 5 en archivo: " +impG[4] , EIGlobal.NivelLog.INFO);

						     if(TCTArchivo.totalRegistros>=1)
					        {
					          botones.append("\n <br>");
					          botones.append("\n <table border=0 align=center>");
			  			      botones.append("\n  <tr>");
			      		      botones.append("\n   <td align=center class='texfootpagneg'>");

								  if(pagina>0)
									 botones.append("&lt; <a href='javascript:BotonPagina(0);' class='texfootpaggri'>Anterior</a>&nbsp;");

								if(calculaPaginas(TCTArchivo.totalRegistros,regPagina)>=2)
								{
									  for(int i=1;i<=calculaPaginas(TCTArchivo.totalRegistros,regPagina);i++)
										if(pagina+1==i)
										{
								         botones.append("&nbsp;");
										 botones.append(Integer.toString(i));
										 botones.append(" ");
										}
									else
										{
									     botones.append("&nbsp;<a href='javascript:BotonPagina(");
										 botones.append(Integer.toString(i+3));
										 botones.append(");' class='texfootpaggri'>");
										 botones.append(Integer.toString(i));
										 botones.append("</a> ");
									     }
								}

						  EIGlobal.mensajePorTrace("MTE_Recaudaciones - calculando paginas", EIGlobal.NivelLog.INFO);

					          if(TCTArchivo.totalRegistros>((pagina+1)*regPagina))
						       {
							    botones.append("<a href='javascript:BotonPagina(1);' class='texfootpaggri'>Siguiente</a>&nbsp;");
								fin=regPagina;
					           }
						      else
							   fin=TCTArchivo.totalRegistros-(pagina*regPagina);

						EIGlobal.mensajePorTrace("MTE_Recaudaciones - colocando botones paginas", EIGlobal.NivelLog.INFO);
						botones.append("\n   </td>");
						botones.append("\n  </tr>");
						botones.append("\n </table>");

/************************************************* Encabezado de la pagina inicio...generaTablaConsultar */
						tablaRegistros.append("\n <table border=0 cellpadding=2 cellspacing=3 align=center>");
						tablaRegistros.append("\n <tr>");
						tablaRegistros.append("\n <td>");
					    tablaRegistros.append("\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>");
					    tablaRegistros.append("\n  <tr>");
						tablaRegistros.append("\n    <td class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red>");
						tablaRegistros.append(Integer.toString(TCTArchivo.totalRegistros));
//						tablaRegistros.append("</font> para el contrato: <font color=red>");
//						tablaRegistros.append(contrato);
						tablaRegistros.append("</font>&nbsp;</td>");
					    tablaRegistros.append("\n    <td align=right class='textabref'>&nbsp;<b>Registros <font color=blue>");
						tablaRegistros.append(Integer.toString((pagina*regPagina)+1));
						tablaRegistros.append(" - ");
						tablaRegistros.append(Integer.toString((pagina*regPagina)+fin));
						tablaRegistros.append("</font></b>&nbsp;</td>");
					    tablaRegistros.append("\n  </tr>");
					    tablaRegistros.append("\n  <tr>");
					    tablaRegistros.append("\n    <td align=left class='textabref'><b>&nbsp;Pagina <font color=blue>");
						tablaRegistros.append(Integer.toString(pagina+1));
						tablaRegistros.append("</font> de <font color=blue>");
						tablaRegistros.append(Integer.toString(calculaPaginas(TCTArchivo.totalRegistros,regPagina)));
						tablaRegistros.append("</font>&nbsp;</b></td>");
						tablaRegistros.append("\n\n    <td align=right class='textabref'>&nbsp;<b>Fecha de Consulta: <font color=blue>");
						tablaRegistros.append (fechaIni);
						tablaRegistros.append("</font></b>&nbsp;</td>");
					    tablaRegistros.append("\n\n  </tr>");
					    tablaRegistros.append("\n </table>");
						EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): Desplegando tabla TCTArchivo parametros: "+Integer.toString(pagina*regPagina)+","+Integer.toString(fin), EIGlobal.NivelLog.INFO);
						tablaRegistros.append("\n </td>");
						tablaRegistros.append("\n </tr>");
						tablaRegistros.append("\n <tr>");
						tablaRegistros.append("\n <td>");
						tablaRegistros.append(seleccionaRegistros(pagina*regPagina,fin,TCTArchivo,req,impG));
						tablaRegistros.append("\n </td>");
						tablaRegistros.append("\n </tr>");
						tablaRegistros.append("\n </table>");

/************************************************* Encabezado de la pagina Fin... */

						req.setAttribute("fechaIni",fechaIni);
			            req.setAttribute("Bandera",Bandera);
			            req.setAttribute("Sucursal",Sucursal);
			            req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
			            req.setAttribute("Botones",botones.toString());
			            req.setAttribute("Registros",tablaRegistros.toString());
			            req.setAttribute("Numero",Integer.toString(regPagina));
			            req.setAttribute("pagina",Integer.toString(pagina));
			            req.setAttribute("Modulo","1");
					    req.setAttribute("Imprimir","<a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir'></a>");
					    req.setAttribute("ExportarPantalla",exportarArchivo(usuario,Result,clavePerfil,contrato,fechaIni,Bandera,req,res));
					    req.setAttribute("ExportarDetalle",exportarArchDetalle(usuario,Result,clavePerfil,contrato,fechaIni,Bandera,req,res));
						req.setAttribute("MenuPrincipal", session.getStrMenu());
					    req.setAttribute("newMenu", session.getFuncionesDeMenu());
					    req.setAttribute("Encabezado", CreaEncabezado("Consulta Recaudaci&oacute;n","Servicios &gt; TESOFE &gt; Recaudaciones &gt; Consultas","s55120h",req));

			           //TODO BIT CU 4411
					    /*
					     * VSWF BMB -I
					     */
					    if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
					    try{
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.ES_TESOFE_RECAUDACIONES_CONS_REC_GENERAL);
							bt.setContrato(contrato);
							if(Result!=null){
				    			if(Result.substring(0,2).equals("OK")){
				    				bt.setIdErr(tipoOperacion.trim()+"0000");
				    			}else if(Result.length()>8){
					    			bt.setIdErr(Result.substring(0,8));
					    		}else{
					    			bt.setIdErr(Result.trim());
					    		}
				    		}
							bt.setNombreArchivo(nombreArchivo);
							bt.setServTransTux(tipoOperacion.trim());

							BitaHandler.getInstance().insertBitaTransac(bt);
						}catch(SQLException e){
							e.printStackTrace();

						}catch(Exception e){
							e.printStackTrace();

						}
					    }
						/*
						 * VSWF BMB -F
						 */
			           evalTemplate("/jsp/MTE_RecaudacionConsulta.jsp", req, res);
					}
				 else
			  	   {
					  EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): No existen Registros.", EIGlobal.NivelLog.INFO);
				  	  mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
			         despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones  &gt; Consultas", "s55120h",req, res);
				   }
		}
	else
		{
	  	  mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
		  EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): Error."+mensajeError, EIGlobal.NivelLog.INFO);
	       despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones  &gt; Consultas", "s55120h",req, res);
		}
   }
  }

 }
 else
 {
				EIGlobal.mensajePorTrace( "MTE_Recaudaciones-> No tiene facultad para realizar consulta de (Recaudaciones)", EIGlobal.NivelLog.INFO);
				despliegaPaginaErrorURL("No tiene <i><font color=red>facultad</font></i> para realizar la consulta de <font color=blue>Recaudaciones</font>.","Consultas de Recaudaci&oacute;n","Servicios &gt; TESOFE &gt; Recaudaciones &gt; Consultas ","s55120h",req,res);
 }
return 1;
}

	/***************************************** Tesofe_Detalle por Sucursal ********************************/
public int generaTablaDetalle(String Result, String clavePerfil, String usuario, String contrato,String Bandera,String Sucursal, String Importe, String fechaIni,String fecha_hoy, HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException
	{
	   EIGlobal.mensajePorTrace( "MTE_Recaudaciones, generaTablaDetalle() ....", EIGlobal.NivelLog.INFO);
	   String Trama="";
 	   String identi = "0";
	   StringBuffer botones=new StringBuffer("");
	   StringBuffer tablaRegistros=new StringBuffer("");
	   String mensajeError="";
	   String tipoOperacion="TF07"; /*servicio de consulta recaudacion Detalle.*/
       Hashtable htResult=null;
       boolean comuError=false;

        int pagina=Integer.parseInt((String) req.getParameter("pagina"));
        int regPagina=Integer.parseInt((String) req.getParameter("Numero"));
        int fin=0;

	 /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
	   EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaDetalle(): Generando Fechas de consulta.", EIGlobal.NivelLog.INFO);
     /************************************** Ejecuta servicio recupera TCTRED llamada 1***/

		boolean RECDET=session.getFacultad(session.FAC_RECDET);

        EIGlobal.mensajePorTrace( "MTE_Recaudacione.java Facultad TESORECDET >" + RECDET, EIGlobal.NivelLog.INFO);


     if(RECDET)
    {
		EIGlobal.mensajePorTrace("Se ejecuta el llamado a TUXEDO......", EIGlobal.NivelLog.INFO);
	    EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaDetalle(): Sucursal "+Sucursal, EIGlobal.NivelLog.DEBUG);

	  /*Cabecera*/

       Trama = IEnlace.medioEntrega2 + "|" +
	  	usuario 			         + "|" +
		tipoOperacion			 + "|" +
       	contrato					 + "|" +
	    usuario 					 + "|" +
       	clavePerfil 				 + "|" +
		fechaIni					 + "|" +
		Sucursal					 + "|" +
   		identi						 + "|";

	    EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaDetalle(): Trama entrada-2 TF07: "+Trama, EIGlobal.NivelLog.DEBUG);

       ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		   try {
				htResult = tuxGlobal.web_red( Trama );
				Result = ( String ) htResult.get( "BUFFER" );
				} catch(Exception e){}

	    EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaDetalle(): Trama salida 0: "+Result, EIGlobal.NivelLog.DEBUG);

/*************************************** Recuperar Recaudación TCTArchivo *********/
     String cadenaArchivo="";
     String arcLinea="";
	 StringBuffer cadenaTCT=new StringBuffer("");
     String nombreArchivo="";
     boolean status=false;
     int totalLineas=0;

	 EI_Tipo TCTArchivo = new EI_Tipo(this);
	 EI_Tipo TCTrama = new EI_Tipo(this);

		   if (  ( Result == null || Result.equals("null") )  && Bandera.equals("0") )
		   {
			 comuError = true;
			 mensajeError = "Problemas en el servicio de consulta. Por favor intente mas tarde.";
             despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones  &gt; Consultas", "s55130h",req, res);
		   }
		  else
		   {
					 if(! Result.substring(0,2).equals("OK"))
					{
						 nombreArchivo=usuario;

						 /***********************************************************  Copia Remota*/
							  ArchivoRemoto archR = new ArchivoRemoto();
							   String arcRegreso=Global.DIRECTORIO_REMOTO + "/" + nombreArchivo;
							   if(!archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
									 {
						  			      EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
										  mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
			   				  	          despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones  &gt; Consultas", "s55130h",req, res);
									 }

							  else
								    {
									 EI_Exportar ArcEnt=new EI_Exportar(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivo);
								     EIGlobal.mensajePorTrace("generaTablaDetalle nombreArchivo enviado a EI_Exportar: "+nombreArchivo, EIGlobal.NivelLog.DEBUG);
									 comuError = true;

					        			if(ArcEnt.abreArchivoLectura())
									     {
										 	  	boolean noError = true;
												arcLinea=ArcEnt.leeLinea();
		    								    mensajeError=arcLinea.substring(16,arcLinea.length());
											    EIGlobal.mensajePorTrace("mensajeError: "  +  mensajeError, EIGlobal.NivelLog.INFO);
												status=true;
												ArcEnt.cierraArchivo();
									            despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones  &gt; Consultas", "s55130h",req, res);
										 }
									}
						}
	   else
	      {
			   TCTrama.iniciaObjeto('@','|',Result.toString());
 		       nombreArchivo = TCTrama.camposTabla[0][2].substring( TCTrama.camposTabla[0][2].lastIndexOf( "/" ) + 1 );

  		 /***********************************************************  Copia Remota*/
				   ArchivoRemoto archR = new ArchivoRemoto();
				   String arcRegreso=Global.DIRECTORIO_REMOTO_TESOFE + "/" + nombreArchivo;
				   if(!archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
					 EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaDetalle(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
   		 /***********************************************************  Copia Remota*/
 		   	   EI_Exportar ArcEnt = new EI_Exportar(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivo);
			   EIGlobal.mensajePorTrace("nombreArchivo enviado a EI_Exportar: "+nombreArchivo, EIGlobal.NivelLog.DEBUG);
			   		 if(ArcEnt.abreArchivoLectura())
						 {
			   	  			boolean noError = true;
							do
							 {
								arcLinea=ArcEnt.leeLinea();
								status=true;
								   if(!arcLinea.equals("ERROR"))
									{
											totalLineas++;
											cadenaTCT.append(arcLinea.substring(0,arcLinea.length()-1));
											cadenaTCT.append(" @");
									}
								   else
								   noError=false;

		  					 }while(noError);
        					ArcEnt.cierraArchivo();
						 }
		  EIGlobal.mensajePorTrace(">>>>>>>> Estatus lectura de archivo correcta: "+ status, EIGlobal.NivelLog.INFO);

			 if(totalLineas<=1 && !status)
				  {
				    comuError = true;
  			   	    mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
				    EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): No se encontro el archivo.", EIGlobal.NivelLog.INFO);
				  }
/********************************************************************************/
     EIGlobal.mensajePorTrace("TCTArchivo.totalRegistros: "+TCTArchivo.totalRegistros, EIGlobal.NivelLog.DEBUG);

    if(!comuError)
     {
       TCTArchivo.iniciaObjeto(';','@',cadenaTCT.toString());
       if(TCTArchivo.totalRegistros>=1)
        {
		  botones.append("\n <br>");
		  botones.append("\n <table border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
          if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina(0);' class='texfootpaggri'>Anterior</a>&nbsp;");

		  if(calculaPaginas(TCTArchivo.totalRegistros,regPagina)>=2)
           {
			 for(int i=1;i<=calculaPaginas(TCTArchivo.totalRegistros,regPagina);i++)
			  if(pagina+1==i)
			   {
                 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append(" ");
			   }
			  else
			   {
	             botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i+3));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a> ");
			   }
           }
		  EIGlobal.mensajePorTrace("MTE_Recaudaciones - calculando paginas", EIGlobal.NivelLog.INFO);
          if(TCTArchivo.totalRegistros>((pagina+1)*regPagina))
           {
                botones.append("<a href='javascript:BotonPagina(1);' class='texfootpaggri'>Siguiente</a> &gt;");
                fin=regPagina;
           }
          else
               fin=TCTArchivo.totalRegistros-(pagina*regPagina);

		  EIGlobal.mensajePorTrace("MTE_Recaudaciones - colocando botones paginas", EIGlobal.NivelLog.INFO);
		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");

/************************************************* Encabezado de la pagina inicio..generaTablaDetalle. */

		  tablaRegistros.append("\n <table border=0 cellpadding=2 cellspacing=3 align=center>");
		  tablaRegistros.append("\n <tr>");
		  tablaRegistros.append("\n <td>");
          tablaRegistros.append("\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>");
          tablaRegistros.append("\n  <tr>");
		  tablaRegistros.append("\n    <td class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red>");
		  tablaRegistros.append(Integer.toString(TCTArchivo.totalRegistros));
		  tablaRegistros.append("</font> con un Importe de: <font color=red>");
		  tablaRegistros.append(Importe);
		  tablaRegistros.append("</font>&nbsp;</td>");
          tablaRegistros.append("\n    <td align=right class='textabref'>&nbsp;<b>Registros <font color=blue>");
		  tablaRegistros.append(Integer.toString((pagina*regPagina)+1));
		  tablaRegistros.append(" - ");
		  tablaRegistros.append(Integer.toString((pagina*regPagina)+fin));
		  tablaRegistros.append("</font>&nbsp;</b></td>");
          tablaRegistros.append("\n  </tr>");
          tablaRegistros.append("\n  <tr>");
          tablaRegistros.append("\n    <td align=left class='textabref'><b>&nbsp;Pagina <font color=blue>");
		  tablaRegistros.append(Integer.toString(pagina+1));
		  tablaRegistros.append("</font> de <font color=blue>");
		  tablaRegistros.append(Integer.toString(calculaPaginas(TCTArchivo.totalRegistros,regPagina)));
		  tablaRegistros.append("</font>&nbsp;</b></td>");
		  tablaRegistros.append("\n\n    <td align=right class='textabref'>&nbsp;<b>Fecha de Consulta: <font color=blue>");
		  tablaRegistros.append (fechaIni);
		  tablaRegistros.append("</font></b>&nbsp;</td>");

//		  tablaRegistros.append("\n\n  </tr>");
//		  tablaRegistros.append("\n\n  <tr>");
//		  tablaRegistros.append("\n\n    <td align=left class='textabref'>&nbsp;<b>Detalle para la Sucursal: <font color=green>");
//		  tablaRegistros.append (Sucursal);
//		  tablaRegistros.append("\n\n  </tr>");

		  tablaRegistros.append("</font></b>&nbsp;</td>");
          tablaRegistros.append("\n\n  </tr>");
          tablaRegistros.append("\n </table>");
		  EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): Desplegando tabla TCTArchivo parametros: "+Integer.toString(pagina*regPagina)+","+Integer.toString(fin), EIGlobal.NivelLog.INFO);
		  tablaRegistros.append("\n </td>");
		  tablaRegistros.append("\n </tr>");
		  tablaRegistros.append("\n <tr>");
		  tablaRegistros.append("\n <td>");
		  tablaRegistros.append(seleccionaRegDetalle(pagina*regPagina,fin,TCTArchivo,req));
		  tablaRegistros.append("\n </td>");
		  tablaRegistros.append("\n </tr>");
		  tablaRegistros.append("\n </table>");

/************************************************* Encabezado de la pagina Fin... */
			  req.setAttribute("fechaIni",fechaIni);
              req.setAttribute("Bandera",Bandera);
              //req.setAttribute("Sucursal",Sucursal);
              req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
              req.setAttribute("Botones",botones.toString());
              req.setAttribute("Registros",tablaRegistros.toString());
              req.setAttribute("Numero",Integer.toString(regPagina));
              req.setAttribute("pagina",Integer.toString(pagina));
              req.setAttribute("Modulo","2");
		      req.setAttribute("Imprimir","<a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir'></a>");
		      req.setAttribute("ExportarDetSuc",exportarArchDetSuc(usuario,Result,clavePerfil,contrato,fechaIni,Sucursal,req,res));
			  req.setAttribute("MenuPrincipal", session.getStrMenu());
		      req.setAttribute("newMenu", session.getFuncionesDeMenu());
		      req.setAttribute("Encabezado", CreaEncabezado("Consulta Recaudaci&oacute;n","Servicios &gt; TESOFE &gt; Recaudaciones &gt; Consultas","s55130h",req));

          //TODO BIT CU 4411
		      /*
		       * VSWF ARR -I
		       */
		      if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_TESOFE_RECAUDACIONES_CONS_REC_DETALLE);
				bt.setContrato(contrato);
				/*BMB-I*/
				if(Result!=null){
	    			if(Result.substring(0,2).equals("OK")){
	    				bt.setIdErr(tipoOperacion.trim()+"0000");
	    			}else if(Result.length()>8){
		    			bt.setIdErr(Result.substring(0,8));
		    		}else{
		    			bt.setIdErr(Result.trim());
		    		}
	    		}
				/*BMB-F*/
				bt.setNombreArchivo(nombreArchivo);
				bt.setServTransTux(tipoOperacion.trim());
				try{
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();

				}catch(Exception e){
					e.printStackTrace();

				}
		      }

				/*
				 * VSWF ARR -F
				 */
          evalTemplate("/jsp/MTE_RecaudacionDetalle.jsp", req, res);
        }
		  else
  	   {
		  EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): No existen Registros.", EIGlobal.NivelLog.INFO);
	  	  mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
         despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones  &gt; Consultas", "s55130h",req, res);
	   }
    }
   else
    {
	  mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
	  EIGlobal.mensajePorTrace("MTE_Recaudaciones - generaTablaConsultar(): Error."+mensajeError, EIGlobal.NivelLog.INFO);
       despliegaPaginaErrorURL(mensajeError,"Consultas de Recaudaci&oacute;n"," Servicios &gt; TESOFE &gt; Recaudaciones  &gt; Consultas", "s55130h",req, res);
    }
   }
  }

 }
 else
 {
				EIGlobal.mensajePorTrace( "MTE_Recaudaciones-> No tiene facultad para realizar la consulta detallada de (Recaudaciones)", EIGlobal.NivelLog.INFO);
				despliegaPaginaErrorURL("No tiene <i><font color=red>facultad</font></i> para realizar la consulta detallada de <font color=blue>Recaudaciones</font>.","Consultas de Recaudaci&oacute;n","Servicios &gt; TESOFE &gt  Recaudaciones &gt; Consultas ","s55120h",req,res);
 }

return 1;
}

/*************************************************************************************/
/********************************************************************* sumaImportes  */
/*************************************************************************************/
  public String sumaImportes(EI_Tipo Tipo,int importe)
	{
	  double importeT=0;
	  double importeTotal=0;

	  for(int i=0;i<Tipo.totalRegistros;i++)
		{
		  try
			{
		      if(Tipo.camposTabla[i][importe].trim().equals(""))
			    importeT=0;
			  else
				importeT=new Double(Tipo.camposTabla[i][importe]).doubleValue();
			}catch(NumberFormatException e)
			{importeT=0;}
		  importeTotal+=importeT;
	    }
	  return FormatoMoneda(importeTotal);
	}

/*************************************************************************************/
/**************************************************** Regresa el total de paginas    */
/*************************************************************************************/
   int calculaPaginas(int total,int regPagina)
    {

      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }

/*************************************************************************************/
/********************************* tabla de movimientos seleccionados por intervalo  */
/*************************************************************************************/
   String seleccionaRegistros(int ini, int fin, EI_Tipo Trans, HttpServletRequest req, String[] impG)
    {
      StringBuffer strTabla       = new StringBuffer("");
      String colorbg        = "";
	  EI_Tipo Consultas = new EI_Tipo(this);

	  /*Declaracion de valores para sumas*/
	 double impSum=0;
	 double importeSum=0;
	 double importeDevolSum=0;
	 double impDevSum=0;
	 double SBC_AntSum=0;
	 double SBC_ASum=0;
	 double SBC_DiaSum=0;
	 double SBC_DSum=0;
	 double SIACSum=0;
	 double SIACsum=0;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");
      if(Trans.totalRegistros>=1)
       {
		 Consultas.iniciaObjeto(Trans.strOriginal);

		/*Encabezado*/
         strTabla.append("\n<table border=0 align=center>");
         strTabla.append("\n<tr>");
         strTabla.append("\n<th class='tittabdat' align='center'><br><br></th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Sucursal Rec. &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Declaraciones &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Importe &nbsp; </th> ");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Importe Devol.(-) &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; S.B.C. Anterior(+) &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; S.B.C. Día(-) &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; SIAC &nbsp; </th>");
         strTabla.append("\n</tr>");

         EIGlobal.mensajePorTrace("MTE_Recaudaciones - seleccionaRegistros(): Generando la Tabla.", EIGlobal.NivelLog.INFO);

         if(fin>Trans.totalRegistros)
            fin=Trans.totalRegistros;

         EIGlobal.mensajePorTrace("MTE_Recaudaciones - seleccionaRegistros(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);
		 for(int i=ini;i<(ini+fin);i++)
          {

	   if((i%2)==0)
             colorbg="class='textabdatobs'";
           else
             colorbg="class='textabdatcla'";
             strTabla.append("\n<tr>");

			String Suc_rec = "", Declaraciones = "", importe = "", importeDevol= "", SBC_Ant= "", SBC_Dia= "", SIAC= "";

			Suc_rec				= Trans.camposTabla[i][0];
			Declaraciones		= Trans.camposTabla[i][1];
			importe				= Trans.camposTabla[i][2];
			importeDevol		= Trans.camposTabla[i][3];
			SBC_Ant				= Trans.camposTabla[i][4];
			SBC_Dia				= Trans.camposTabla[i][5];
			SIAC					= Trans.camposTabla[i][6];

			/*Pinta datos obtenidos de archivo*/

			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='center'>");
			strTabla.append("<input name=radio type=radio onClick='checar(this)' value='"+Suc_rec.trim()+"|"+FormatoMoneda(importe)+"|"+"'>");
            strTabla.append("&nbsp;</td>");

			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='center'>");
			strTabla.append(Suc_rec);
			strTabla.append("&nbsp;</td>"); /*Suc_rec*/

			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='center'>");
			strTabla.append(Declaraciones);
			strTabla.append("&nbsp;</td>"); /*Declaraciones*/


  	        importeSum = new Double(importe).doubleValue();
	        impSum += importeSum;
			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='right'>");
			strTabla.append(FormatoMoneda(importe));
			strTabla.append("&nbsp;</td>"); /*importe*/

		    importeDevolSum = new Double(importeDevol).doubleValue();
	        impDevSum += importeDevolSum;
			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='right'>");
			strTabla.append(FormatoMoneda(importeDevol));
			strTabla.append("&nbsp;</td>"); /*importe_Devol*/

		    /* Columna */
		    SBC_DiaSum = new Double(SBC_Dia).doubleValue();
	        SBC_DSum += SBC_DiaSum;
			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='right'>");
			strTabla.append(FormatoMoneda(SBC_Dia));
			strTabla.append("&nbsp;</td>"); /*SBC_Dia*/

			SBC_AntSum = new Double(SBC_Ant).doubleValue();
	        SBC_ASum += SBC_AntSum;
			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='right'>");
			strTabla.append(FormatoMoneda(SBC_Ant));
			strTabla.append("&nbsp;</td>"); /*SBC_Ant*/
			/* Columna */

		    SIACSum = new Double(SIAC).doubleValue();
	        SIACsum += SIACSum;
			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='right'>");
			strTabla.append(FormatoMoneda(SIAC));
			strTabla.append("&nbsp;</td>"); /*SIAC*/

            strTabla.append("\n</tr>");
          }
            strTabla.append("\n<tr>");
		    strTabla.append("\n<td ></td>");
		    strTabla.append("\n<td class='textabref'><br><br></td>");
			strTabla.append("\n<td class='tittabdat' align='center'> &nbsp; Importes Totales &nbsp; </td>");
            strTabla.append("\n<td class='tittabdat' align='center'> &nbsp;"+FormatoMoneda(impSum)+" &nbsp; </td>");
            strTabla.append("\n<td class='tittabdat' align='center'> &nbsp;"+FormatoMoneda(impDevSum)+" &nbsp; </td>");
			/* Columna */
            strTabla.append("\n<td class='tittabdat' align='center'> &nbsp;"+FormatoMoneda(SBC_DSum)+" &nbsp; </td>");
            strTabla.append("\n<td class='tittabdat' align='center'> &nbsp;"+FormatoMoneda(SBC_ASum)+" &nbsp;</td>");
			/* Columna */
            strTabla.append("\n<td class='tittabdat' align='center'> &nbsp;"+FormatoMoneda(SIACsum)+" &nbsp; </td>");
            strTabla.append("\n</tr>");

			strTabla.append("\n<tr>");
		    strTabla.append("\n<td ></td>");
		    strTabla.append("\n<td class='textabref'><br><br></td>");
			strTabla.append("\n<td class='textabdatobs' align='center'> &nbsp; Importes Globales &nbsp; </td>");
            strTabla.append("\n<td class='textabdatobs' align='center'> &nbsp;"+impG[0]+" &nbsp; </td>");
            strTabla.append("\n<td class='textabdatobs' align='center'> &nbsp;"+impG[1]+" &nbsp; </td>");
			/* Columna */
            strTabla.append("\n<td class='textabdatobs' align='center'> &nbsp;"+impG[3]+" &nbsp; </td>");
            strTabla.append("\n<td class='textabdatobs' align='center'> &nbsp;"+impG[2]+" &nbsp;</td>");
			/* Columna */
            strTabla.append("\n<td class='textabdatobs' align='center'> &nbsp;"+impG[4]+" &nbsp; </td>");
            strTabla.append("\n</tr>");


         strTabla.append("\n</table>");
       }
      return strTabla.toString();
    }

/*************************************************************************************/
/********************************* tabla de movimientos seleccionados por intervalo  */
/*************************************************************************************/
   String seleccionaRegDetalle(int ini, int fin, EI_Tipo Trans, HttpServletRequest req)
    {
      StringBuffer strTabla       = new StringBuffer("");
      String colorbg        = "";
	  EI_Tipo Consultas = new EI_Tipo(this);

		double importeDSum=0;
		double impDSum=0;
		double importeDdevSum=0;
		double impDdeSum=0;


	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");
      if(Trans.totalRegistros>=1)
       {
		 Consultas.iniciaObjeto(Trans.strOriginal);

		/*Encabezado*/
         strTabla.append("\n<table border=0 align=center>");
         strTabla.append("\n<tr>");
         strTabla.append("\n<th class='textabref' align='center'><br><br></th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Sucursal &nbsp; </th>");
		 strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Reg.Fed. Cont. &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Forma de Pago &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Importe Pagado &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat' align='center'> &nbsp; Importe Devuelto &nbsp; </th>");
         strTabla.append("\n</tr>");

         EIGlobal.mensajePorTrace("MTE_Recaudaciones - seleccionaRegDetalle(): Generando la Tabla Detalle.", EIGlobal.NivelLog.INFO);

         if(fin>Trans.totalRegistros)
            fin=Trans.totalRegistros;

         EIGlobal.mensajePorTrace("MTE_Recaudaciones - seleccionaRegDetalle(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);

		 for(int i=ini;i<(ini+fin);i++)
          {

	        if((i%2)==0)
             colorbg="class='textabdatobs'";
           else
             colorbg="class='textabdatcla'";
            strTabla.append("\n<tr>");

			String sucursal="", rfc = "", fpago = "", importe = "", importeDevol= "";

			sucursal	   =		Trans.camposTabla[i][0];
			rfc			   =		Trans.camposTabla[i][1];
			fpago		   =		Trans.camposTabla[i][2];
			importe        =		Trans.camposTabla[i][3];
			importeDevol   = Trans.camposTabla[i][4];

			strTabla.append("\n<td></td>");

			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='center'>");
			strTabla.append(sucursal);
			strTabla.append("&nbsp;</td>"); /*sucursal*/

			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='center'>");
			strTabla.append(rfc);
			strTabla.append("&nbsp;</td>"); /*rfc*/

			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='center'>");

			int formaPag=-1;
			try
			{
		      if(fpago.trim().equals(""))
			    formaPag=-1;
			  else
				formaPag=new Integer(fpago).intValue();
			}catch(NumberFormatException e)
			{formaPag=-1;}

			switch (formaPag)
			  {
				case    0: fpago="Efectivo"; break;
				case    5: fpago="Cheque"; break;
				case    6: fpago="S.B.C. Hoy"; break;
				case  490: fpago="Electrónico"; break;
				case 1006: fpago="S.B.C. Ant."; break;
				case 2006: fpago="Devoluciones"; break;
				default:   fpago="Indeterminado"; break;
			  }

			strTabla.append(fpago);
			strTabla.append("&nbsp;</td>"); /*fpago*/

	        importeDSum = new Double(importe).doubleValue();
	        impDSum += importeDSum;
			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='right'>");
			strTabla.append(FormatoMoneda(importe));
			strTabla.append("&nbsp;</td>"); /*importe*/

	        importeDdevSum = new Double(importeDevol).doubleValue();
	        impDdeSum += importeDdevSum;
			strTabla.append("<td ");
			strTabla.append(colorbg);
			strTabla.append(" align='right'>");
			strTabla.append(FormatoMoneda(importeDevol));
			strTabla.append("&nbsp;</td>"); /*importe_Devol*/

            strTabla.append("\n</tr>");
          }
			strTabla.append("\n<tr>");
		    strTabla.append("\n<td></td>");
		    strTabla.append("\n<td></td>");
		    strTabla.append("\n<td class='textabref'><br><br></td>");
			strTabla.append("\n<td class='tittabdat' align='center'> &nbsp; Totales &nbsp; </td>");
            strTabla.append("\n<td class='tittabdat' align='center'> &nbsp;"+FormatoMoneda(impDSum)+" &nbsp;</td>");
            strTabla.append("\n<td class='tittabdat' align='center'> &nbsp;"+FormatoMoneda(impDdeSum)+" &nbsp;</td>");
            strTabla.append("\n</tr>");

         strTabla.append("\n</table>");
       }
      return strTabla.toString();
    }


/*************************************************************************************/
/********************************************************** Exportacion de Archivo Consulta General  */
/*************************************************************************************/
 public String exportarArchivo(String usuario,String Result, String clavePerfil, String contrato,String fechaIni, String Bandera,HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	String identi = "1";
    String Trama="";
    String tipoOperacion="TF06"; /*servicio de consulta recaudacion detalle general todas las sucursales*/
    String strBoton="";
    String mensajeError="";
    String Suc_rec="";
	String arcLinea="";
	Hashtable htResult=null;
    String nombreArchivo="";
    EI_Tipo TCTrama = new EI_Tipo(this);

/*******************************************************************************************/
/************************************** Ejecuta servicio recupera TCTRED llamada 2***/
      /*Cabecera*/
       Trama = IEnlace.medioEntrega2	 + "|" +
  	   usuario 										 + "|" +
	   tipoOperacion								 + "|" +
	   contrato										 + "|" +
   	   usuario 										 + "|" +
	   clavePerfil							 		     + "|"+
	   fechaIni										     + "|" +
	   identi												 + "|";

	   EIGlobal.mensajePorTrace("MTE_Recaudaciones - exportarArchivo(): Trama entrada-2 TF06: "+Trama, EIGlobal.NivelLog.DEBUG);

       ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   try {
			htResult = tuxGlobal.web_red( Trama );
			Result = ( String ) htResult.get( "BUFFER" );
	   } catch(Exception e){}

      nombreArchivo=usuario;

	   if( Result.substring(0,2).equals("OK"))
		{
   		 /***********************************************************  Copia Remota*/

		  String arcRegreso=Global.DIRECTORIO_REMOTO_TESOFE + "/" + nombreArchivo+".rts";
   		   EIGlobal.mensajePorTrace("arcRegreso():"+arcRegreso, EIGlobal.NivelLog.INFO);
		   ArchivoRemoto archR = new ArchivoRemoto();
		  if(archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
		  {
            EIGlobal.mensajePorTrace( "MTE_Recaudaciones-> Copia remota desde tuxedo se realizo correctamente. (Archivos)", EIGlobal.NivelLog.INFO);
		   }
  	  	    if(!archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
			 {
				strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25262.gif' alt='Exportar Pantalla'></a>";
			 }
			else
			 {
				if(archR.copiaLocalARemoto(nombreArchivo+".rts","WEB"))
				  strBoton="<a href='/Download/"+nombreArchivo+".rts'><img border=0 src='/gifs/EnlaceMig/gbo25262.gif' alt='Exportar Pantalla'></a>";
				else
				  {
					EIGlobal.mensajePorTrace("MTE_Recaudaciones - exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
					strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25262.gif' alt='Exportar Pantalla'></a>";
				  }
			 }
		}
	  else
		strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25262.gif' alt='Exportar Pantalla'></a>";

 return strBoton;
 }

/*************************************************************************************/
/********************************************************** Exportacion de Archivo Consulta detallada por todas las Sucursales  */
/*************************************************************************************/
 public String exportarArchDetalle(String usuario,String Result, String clavePerfil, String contrato,String fechaIni,String Bandera,HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
    String Trama="";
    String tipoOperacion="TF08"; /*servicio de consulta recaudacion detalle especifico por todas las sucursales*/
    String strBoton="";
    String mensajeError="";
    String Suc_rec="";
	String arcLinea="";
	Hashtable htResult=null;
    String nombreArchivo="";
    EI_Tipo TCTrama = new EI_Tipo(this);

/*************************************************************************************************/
/******************************** Ejecuta servicio recupera TCTRED llamada 3**************/
      /*Cabecera*/

       Trama = IEnlace.medioEntrega2				 + "|" +
  	   usuario 													 + "|" +
	   tipoOperacion											 + "|" +
	   contrato													 + "|" +
   	   usuario 													 + "|" +
	   clavePerfil							 					     + "|" +
	   fechaIni													     + "|";

	   EIGlobal.mensajePorTrace("MTE_Recaudaciones - exportarArchDetalle(): Trama entrada-0 TF08: "+Trama, EIGlobal.NivelLog.DEBUG);

       ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   try {
			htResult = tuxGlobal.web_red( Trama );
			Result = ( String ) htResult.get( "BUFFER" );
	   } catch(Exception e){}

	nombreArchivo=usuario;

		   if( Result.substring(0,2).equals("OK"))
		{
   		 /***********************************************************  Copia Remota*/

		  String arcRegreso=Global.DIRECTORIO_REMOTO_TESOFE + "/" + nombreArchivo+".rds";
   		   EIGlobal.mensajePorTrace("arcRegreso():"+arcRegreso, EIGlobal.NivelLog.INFO);
 		   ArchivoRemoto archR = new ArchivoRemoto();
		  if(archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
		  {
            EIGlobal.mensajePorTrace( "MTE_Recaudaciones-> Copia remota desde tuxedo se realizo correctamente. (Archivos)", EIGlobal.NivelLog.INFO);
		  }
  	  	    if(!archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
			 {
				 strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25264.gif' alt='Exportar Detalle'></a>";
			 }
			else
			 {
				if(archR.copiaLocalARemoto(nombreArchivo+".rds","WEB"))
				  strBoton="<a href='/Download/"+nombreArchivo+".rds'><img border=0 src='/gifs/EnlaceMig/gbo25264.gif' alt='Exportar Detalle'></a>";
				else
				  {
					EIGlobal.mensajePorTrace("MTE_Recaudaciones - exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
	 			    strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25264.gif' alt='Exportar Detalle'></a>";
				  }
			 }
		}
	  else
	 strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25264.gif' alt='Exportar Detalle'></a>";

 return strBoton;
 }

/*************************************************************************************/
/********************************************************** Exportacion Archivo Detalle por una Sucursal  */
/*************************************************************************************/
 public String exportarArchDetSuc(String usuario,String Result, String clavePerfil, String contrato,String fechaIni,String Sucursal, HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	String identi = "1";
    String Trama="";
    String tipoOperacion="TF07"; /*servicio de consulta recaudacion detalle especifico por una sucursal*/
    String strBoton="";
    String mensajeError="";
    String Suc_rec="";
	String arcLinea="";
	Hashtable htResult=null;
    String nombreArchivo="";
    EI_Tipo TCTrama = new EI_Tipo(this);

/*************************************************************************************************/
/************************************** Ejecuta servicio recupera TCTRED llamada 3***/
      /*Cabecera*/

       Trama = IEnlace.medioEntrega2				 + "|" +
  	   usuario 													 + "|" +
	   tipoOperacion											 + "|" +
	   contrato													 + "|" +
   	   usuario 													 + "|" +
	   clavePerfil												     + "|" +
	   fechaIni													     + "|" +
       Sucursal													 + "|" +
   	   identi															 + "|";

	   EIGlobal.mensajePorTrace("MTE_Recaudaciones - exportarArchDetalle(): Trama entrada-0 TF07: "+Trama, EIGlobal.NivelLog.DEBUG);

       ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   try {
			htResult = tuxGlobal.web_red( Trama );
			Result = ( String ) htResult.get( "BUFFER" );
	   } catch(Exception e){}

        nombreArchivo=usuario;

	   if( Result.substring(0,2).equals("OK"))
		{
   		 /***********************************************************  Copia Remota*/

		  String arcRegreso=Global.DIRECTORIO_REMOTO_TESOFE + "/" + nombreArchivo+".rps";
   		   EIGlobal.mensajePorTrace("arcRegreso():"+arcRegreso, EIGlobal.NivelLog.INFO);
 		   ArchivoRemoto archR = new ArchivoRemoto();
		  if(archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
		  {
            EIGlobal.mensajePorTrace( "MTE_Recaudaciones-> Copia remota desde tuxedo se realizo correctamente. (Archivos)", EIGlobal.NivelLog.INFO);
		  }
  	  	    if(!archR.copiaTESOFE(arcRegreso,IEnlace.LOCAL_TMP_DIR))
			 {
			    strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
			 }
			else
			 {
				if(archR.copiaLocalARemoto(nombreArchivo+".rps","WEB"))
				  strBoton="<a href='/Download/"+nombreArchivo+".rps'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
				else
				  {
					EIGlobal.mensajePorTrace("MTE_Recaudaciones - exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
				    strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
				  }
			 }
		}
	  else
     strBoton="<a href='javascript:cuadroDialogo(\"Archivo de Importación NO disponible por el momento.\", 4);' ><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";

 return strBoton;
 }


/********************************************************************** ***************/
/********************************* despliegaPaginaErrorURL ******* ***************/
/********************************************************************* ***************/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,request ) );
		request.setAttribute( "URL", "MTE_Recaudaciones?operacion=TESOFE2&Modulo=0");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}
	public String[] desentrama(String cadena, char caracter)
	{

		String vartmp="";
	    String regtmp="";
	    int totalCampos=0;
		int tamanioArreglo=0;
	    String[] camposTabla;

	    regtmp = cadena;
	    for (int i = 0; i < regtmp.length(); i++ )
		{
		  if ( regtmp.charAt( i ) == caracter )
			totalCampos++;
		}
		EIGlobal.mensajePorTrace( "ConsultaUsuarios desentrama() -  Total de campos : " + Integer.toString( totalCampos ), EIGlobal.NivelLog.INFO);

		camposTabla = new String[totalCampos];

		for (int i = 0; i < totalCampos; i++ )
		{
			if ( regtmp.charAt( 0 ) == caracter )
			{
				vartmp = " ";
				regtmp = regtmp.substring( 1, regtmp.length() );
			}
			else
			{
				vartmp = regtmp.substring( 0, regtmp.indexOf( caracter )).trim();
				if ( regtmp.indexOf( caracter ) > 0 )
					regtmp=regtmp.substring( regtmp.indexOf( caracter ) + 1,regtmp.length() );
			}
			camposTabla[i]=vartmp;
			EIGlobal.mensajePorTrace( "ConsultaUsuarios desentrama() -  Elemento " + i + " es: " + camposTabla[i] , EIGlobal.NivelLog.INFO);
		}
		return camposTabla;
	}

   }