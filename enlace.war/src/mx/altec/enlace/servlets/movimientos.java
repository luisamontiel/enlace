/*Banco Santander Mexicano
  Clase movimientos  Genera el codigo para la presentacion de movimientos
  @Autor:
  @zversion: 1.0
  fecha de creacion:
  modificacion:Paulina Ventura Agustin	 28/02/2001 - Quitar codigo muerto
  modificacion:Jorge Barbosa Granados	 20/04/2001 - Remote Shell
  modificacion:Francisco Serrato Jimenez Junio/2004 - Fondos 390 (fsj)
  modificacion:Sergio Adrian L?pez Flores Agosto/2004 - Q1080 (slf)
  modificacion:Hector Cid del Prado Vazquez Enero/2006 - Ley de Transparencia II
  modificacion:Hector Cid del Prado V?zquez Marzo/2006 - Se elimino el metodo que se usaba
  // para llenar la tabla de los datos ya que esta lo estaba haciendo erroneamente, y se
  // implemento una instancia a la clase EI_Tipo.class la cual lo hace correctamente asi
  // como tambien se recorrieron los indices de la tabla a partir del 3er indice.
*/
package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.ibm.gsk.ikeyman.util.Debug;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
//04/01/07
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * The Class movimientos.
 */
public class movimientos extends BaseServlet
{
    /**
     * alignServ alignServ
     */
    static final String ALING_SERV = " align=left>&nbsp;&nbsp;&nbsp;</td>";

    /** Variable _default template. */
    static protected String _defaultTemplate ="EnlaceMig/movimientos";//modificado 10/12/2002

	/** La constante SIGNO_PESOS $. */
	private static final String SIGNO_PESOS = "$";

	/** La constante VACIO. */
	private static final String VACIO = "";

	/** La constante COMA ','. */
	private static final String COMA = ",";

	/** La constante SPACE ' '. */
	private static final String SPACE = " ";

	/** La constante ENLACE_CUENTA_PARAM. */
	private static final String ENLACE_CUENTA_PARAM = "EnlaceCuenta";

	/** La constante SESION_PARAM. */
	private static final String SESION_PARAM = "session";

	/**

	 * doGet.
	 *
	 * @param req El HttpServletRequest obtenido de la vista
	 * @param res El HttpServletResponse obtenido de la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
    public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
    {
	defaultAction( req, res );
    }


    /**
	 * doPost.
	 *
	 * @param req El HttpServletRequest enviado a la vista
	 * @param res El HttpServletResponse enviado a la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
    public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
    {
	defaultAction( req, res );
    }

    /**
     * Default action.
     *
     * @param req the req
     * @param res the res
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
	String nombrecuenta    = "";
	String cuentamov       = "";
	String tipocuenta      = "";
	double totalCargo      = 0;
	double totalAbono      = 0;
	int numeroCargos       = 0;
	int numeroAbonos       = 0;
	String separador       = ";";
	String arrcta[]        = null;

	String importeA = "";
	String importeDe = "";


	int x;
	// Se obtienen par?metros
	String prev	 = (String)req.getParameter("prev");
	String next	 = (String)req.getParameter("next");
	String tipoBanca       = (String)req.getParameter("Banca");
	String tramacuentamov  = (String)req.getParameter(ENLACE_CUENTA_PARAM);
	String tipo_movimiento = (String)req.getParameter("TMov");


	if( !SesionValida(req,res) )
	    return;
	BaseResource session = (BaseResource)req.getSession().getAttribute(SESION_PARAM);
	//Se declaran atributos necesarios
	req.setAttribute("MenuPrincipal",session.getStrMenu());
	req.setAttribute("newMenu"	,session.getFuncionesDeMenu());
	req.setAttribute("tipoBanca"	,tipoBanca);
	// Si la sesi?n no es v?lida o no se tiene facultad, se avisa y no se contin?a la ejecuci?n
	if( !session.getFacultad(session.FAC_CONSULTA_MOVTOS) )
	{

	    if("E".equals(tipoBanca))
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, "Consulta de movimientos de banca especializada", "Consultas &gt; Movimientos &gt; Banca especializada", "s25200h", req, res);
	    else
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, "Consulta de movimientos de cuenta de cheques", "Consultas &gt; Movimientos &gt; Chequeras", "s25150h", req, res);
	}

	//CAMBIO PARA EXPORTACION A CSV Y TXT
	String tipoArchivo = req.getParameter("tExportacion");
	String tipoConsulta = ( req.getParameter("tipoConsulta")!=null )? req.getParameter("tipoConsulta") : "";
	String cCuenta = ( req.getParameter("cuenta")!=null )? req.getParameter("cuenta") : "";

	EIGlobal.mensajePorTrace(" movimientos --> INICIADO EXPORTACION  TIPO ARCHIVO <"+tipoArchivo+">", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace(" movimientos --> OBTIENE CONSULTA <"+tipoConsulta+">  CUENTA EXPORT <"+cCuenta+">", EIGlobal.NivelLog.INFO);

	if(tipoArchivo != null &&!("".equals(tipoArchivo))&&"Chequeras".equals(tipoConsulta)){
		String rutaArchivo = IEnlace.LOCAL_TMP_DIR +"/"+ session.getUserID8()+"mv"+cCuenta+".doc";
		String tipoBan = ( req.getParameter("tipoCBan")!=null )? req.getParameter("tipoCBan") : "";
		EIGlobal.mensajePorTrace("movimientos --> INICIADO EXPORTACION  RUTA ARCHIVO <"+rutaArchivo+">", EIGlobal.NivelLog.INFO);
		exportarArchivo(tipoArchivo, rutaArchivo, tipoBan, res);
		return;
	}


	//******* MODIFICACION PAMPA *******
	if( tipo_movimiento==null )
	    tipo_movimiento="";
	if("PMDM".equals(tipo_movimiento))
	    pampa_consulta_detalle_movimientos_comercio(req,res);
	else
	{
	    if( tipoBanca==null || "".equals(tipoBanca))
			tipoBanca = "C";

		if( tramacuentamov == null ) {
			despliegaPaginaError("Error al procesar su solicitud", "Consulta de movimientos de cuenta de cheques", "Consultas &gt; Movimientos &gt; Chequeras", "s25150h", req, res);
			EIGlobal.mensajePorTrace ("Variable tramacuentamov vacia", EIGlobal.NivelLog.ERROR);
			return;
		}
	    if( tramacuentamov.indexOf('@')==-1 )
	    {
			cuentamov    = tramacuentamov.substring(tramacuentamov.indexOf('|')+1,tramacuentamov.lastIndexOf('|'));
			tipocuenta   = "P";
			nombrecuenta = "";
	    } else {
			arrcta	     = desentramaC("3@"+tramacuentamov,'@');

			cuentamov    = arrcta[1];
			req.setAttribute("Cuenta", cuentamov);
			tipocuenta   = arrcta[2];
			nombrecuenta = arrcta[3];
	    }
	    //******************************************

	    if("E".equals(tipoBanca))
	    {
			if("NORM".equals(req.getParameter("deldia")))
				req.setAttribute("Encabezado", CreaEncabezado("Consulta de Movimientos","Consultas &gt; Movimientos &gt; Banca especializada","s25200h",req));
			else
				req.setAttribute("Encabezado", CreaEncabezado("Consulta de Movimientos","Consultas &gt; Movimientos &gt; Banca especializada","s25150h",req));

	    } else {
			if ("NORM".equals(req.getParameter("deldia")))
				req.setAttribute("Encabezado", CreaEncabezado("Consulta de Movimientos","Consultas &gt; Movimientos &gt; Cheques","s25170h",req));
			else
				req.setAttribute("Encabezado", CreaEncabezado("Consulta de Movimientos","Consultas &gt; Movimientos &gt; Cheques","s25180h",req));

		}

	    if( prev==null && next==null )
	    {
			//consigue los movimientos
			x = movs(tipocuenta, cuentamov, req, res, tipo_movimiento);


			/*****************MOV*********************/
			int total_lineas;
			if(x==0){

				total_lineas = getnumberlines(Global.DIRECTORIO_MOV + "/" + session.getUserID8(), separador);
				EIGlobal.mensajePorTrace("Total Lineas MOVI = " + total_lineas, EIGlobal.NivelLog.DEBUG);
			}else{

			    total_lineas = getnumberlines(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8(), separador);

			}
            /****************************************/

			session.settotal(new Integer(total_lineas).toString());
			EIGlobal.mensajePorTrace("-!?movimientos.class TOTAL DE LINEAS[" + total_lineas +"]", EIGlobal.NivelLog.INFO);

			if( total_lineas>0 )
			{
				//VARIABLES PARA EL IMPORTE VECTOR
				String otherfile     = IEnlace.LOCAL_TMP_DIR + "/" +cuentamov+ "MV.fil";
				String clave_mov	 = ( String) req.getParameter("CvlMov");
				String el_importe	 = ( String) req.getParameter("Importe");
				String la_referencia = ( String) req.getParameter("Refe");

				if(el_importe == null || "".equals(el_importe)){
					el_importe = "";
				}

				//RANGO DE IMPORTES 03-2015 Mov. Chequeras

				if(!"E".equals(tipoBanca)){

					importeA = (String)req.getParameter("importeA");
					importeDe = (String)req.getParameter("importeDe");

					importeA = importeA == null?"":importeA;
					importeDe = importeDe == null?"":importeDe;
				}

				/*****************MOV*********************/

				EIGlobal.mensajePorTrace("--> movimientos Rango de importes  --> Importe <"+el_importe+">  Importe De <"+ importeDe +">  Importe A <"+importeA+">" , EIGlobal.NivelLog.DEBUG);

				if(x==0){

					x = Filters_to_file(tipoBanca,Global.DIRECTORIO_MOV + "/" + session.getUserID8(), otherfile,total_lineas,clave_mov,el_importe,la_referencia, separador, importeDe , importeA );
				}else{

					x = Filters_to_file(tipoBanca,IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8(),otherfile,total_lineas,clave_mov,el_importe,la_referencia, separador, importeDe , importeA);

				}
	            /*****************************************/

				EIGlobal.mensajePorTrace("Filters_to_File regresa: [" +x+ "]", EIGlobal.NivelLog.INFO);

				if( x==0 )
				{
					session.setArchivoMov(otherfile);
					total_lineas = getnumberlines(session.getArchivoMov());
					session.settotal(new Integer(total_lineas).toString());
					CreaArchMov carch = new CreaArchMov();
					carch.setCuenta(cuentamov);
					String tmpa= session.getArchMovs()==null?"":session.getUserID8()+"mv"+session.getArchMovs()+".doc";

					if(tmpa.trim().length()!=0)
					{   //borrar
						try {
							File anterior= new File(IEnlace.DOWNLOAD_PATH+tmpa);
							anterior.delete();
						} catch(Exception ex){  }
					} else
						EIGlobal.mensajePorTrace("Fue null "+ session.getUserID8()+"mv"+session.getArchMovs()+".doc", EIGlobal.NivelLog.INFO);



					x=carch.GeneraArchivoMovs(tramacuentamov,session.getArchivoMov(),session.getUserID8(),0,total_lineas,tipoBanca);
					/*for(int n=0;n<total_lineas;n++){

					}*/
					session.setArchMovs(cuentamov);
					if( total_lineas>1000 )
					{
						//Cambio en atenci?n a la incidencia IM919775 jppm
						StringBuffer salida=new StringBuffer();
						salida.append("<P>\n");
						salida.append("<form name=\"frmbit\" method=\"POST\">\n");
						salida.append(" <input type=\"Hidden\" name=\"total\" value=\"").append(total_lineas).append("\">\n");
						salida.append(" <input type=\"Hidden\" name=\"prev\"  value=\"").append(0).append("\">\n");
						salida.append(" <input type=\"Hidden\" name=\"next\"  value=\"").append(0).append("\">\n");
						salida.append(" <input type=\"Hidden\" name=\"Banca\" value=\"").append(tipoBanca).append("\">\n");
						salida.append("<TABLE width=760 border=0>\n");
						salida.append("  <TBODY>\n");
						salida.append("  <TR>\n");
						salida.append("    <TD class=texenccon vAlign=top align=middle>\n");
						salida.append("      <TABLE cellSpacing=0 cellPadding=0 width=430 align=center border=0>\n");
						salida.append("        <TBODY>\n");
						salida.append("        <TR><TD class=tittabdat colSpan=3><IMG height=2 alt=.. src=\"/gifs/EnlaceMig/gau25010.gif\" width=5 name=.></TD></TR>\n");
						salida.append("        <TR><TD colSpan=3><IMG height=2 alt=.. src=\"/gifs/EnlaceMig/gau25010.gif\" width=5 name=.></TD></TR>\n");
						salida.append("        <TR><TD class=tittabdat colSpan=3><IMG height=8 alt=..  src=\"/gifs/EnlaceMig/gau25010.gif\" width=5  name=.></TD></TR>\n");
						salida.append("        </TBODY>\n");
						salida.append("      </TABLE>\n");
						salida.append("      <TABLE cellSpacing=0 cellPadding=0 width=400 align=center border=0>\n");
						salida.append("        <TBODY>\n");
						salida.append("        <TR>\n");
						salida.append(" 	 <TD align=middle>\n");
						salida.append(" 	   <TABLE cellSpacing=0 cellPadding=0 width=430 align=center border=0>\n");
						salida.append(" 	   <TBODY>\n");
						salida.append(" 	     <TR><TD colSpan=3></TD></TR>\n");
						salida.append(" 	     <TR>\n");
						salida.append(" 	       <TD width=21 background=/gifs/EnlaceMig/gfo25030.gif><IMG height=2 alt=.. src=\"/gifs/EnlaceMig/gau00010.gif\" width=21 name=..></TD>\n");
						salida.append(" 	       <TD vAlign=top align=middle width=428 height=100>\n");
						salida.append(" 		 <TABLE cellSpacing=2 cellPadding=3 width=380 background=/gifs/EnlaceMig/gau25010.gif border=0>\n");
						salida.append(" 		   <TBODY>\n");
						salida.append(" 		   <TR><TD colSpan=3>&nbsp;</TD></TR>\n");
						salida.append(" 		   <TR>\n");
						salida.append(" 		      <TD class=tittabcom align=middle>\n");
						salida.append(" 			Su consulta excede mil movimientos. Para verlos por favor exp&oacute;rtelos.\n");
						salida.append(" 		      </TD>\n");
						salida.append(" 		    </TR>\n");
						salida.append(" 		   </TBODY>\n");
						salida.append(" 		 </TABLE>\n");
						salida.append(" 	       </TD>\n");
						salida.append(" 	       <TD width=21 background=/gifs/EnlaceMig/gfo25040.gif><IMG height=2 alt=.. src=\"/gifs/EnlaceMig/gau00010.gif\" width=21 name=..></TD>\n");
						salida.append(" 	     </TR>\n");
						salida.append(" 	   </TBODY>\n");
						salida.append(" 	   </TABLE>\n");
						salida.append(" 	 </TD>\n");
						salida.append("        </TR>\n");
						salida.append("        </TBODY>\n");
						salida.append("      </TABLE>\n");
						salida.append("      <TABLE cellSpacing=0 cellPadding=0 width=430 align=center border=0>\n");
						salida.append("        <TBODY>\n");
						salida.append("        <TR><TD class=tittabdat colSpan=3><IMG height=8 alt=.. src=\"/gifs/EnlaceMig/gau25010.gif\" width=5 name=.></TD></TR>\n");
						salida.append("        <TR><TD colSpan=3><IMG height=2 alt=.. src=\"/gifs/EnlaceMig/gau25010.gif\" width=5 name=.></TD></TR>\n");
						salida.append("        <TR><TD class=tittabdat colSpan=3><IMG height=2 alt=.. src=\"/gifs/EnlaceMig/gau25010.gif\" width=5 name=.></TD></TR>\n");
						salida.append("        </TBODY>\n");
						salida.append("      </TABLE>\n");
						salida.append("      <TABLE cellSpacing=0 cellPadding=0 width=430 align=center border=0>\n");
						salida.append("        <TBODY>\n");
						salida.append("        <TR>\n");
						salida.append(" 	 <TD align=middle><BR>\n");
						salida.append("<table style=\"margin-left: auto; margin-right: auto;\">\n");
						salida.append("<tr>\n");
						salida.append("<td><input type=\"radio\" value=\"txt\" name=\"tipoArchivo\" onclick=\"cambiaArchivo('txt');\"  /></td>\n");
						salida.append("<td class=\"tabmovtex11\">Exporta en TXT</td>\n");
						salida.append("</tr>\n");
						salida.append("<tr>\n");
						salida.append("<td><input type=\"radio\" value=\"csv\" name=\"tipoArchivo\" onclick=\"cambiaArchivo('csv');\" checked=\"checked\" /></td>\n");
						salida.append("<td class=\"tabmovtex11\">Exporta en XLS</td>\n");
						salida.append("</tr>\n");
						salida.append("</table>\n\n");
						salida.append(" <input id=\"tArchivo\" type=\"hidden\" name=\"tArchivo\" value=\"csv\" readonly=\"readonly\" /> \n");
						salida.append(" 	   <TABLE height=22 cellSpacing=0 cellPadding=0 width=150 border=0>\n");
						salida.append(" 	     <TBODY>\n");
						salida.append(" 	     <TR>\n");
						salida.append(" 		 <TD align=middle><A href=\"javascript:history.go(-1)\"><IMG height=22 alt=Regresar src=\"/gifs/EnlaceMig/gbo25320.gif\" width=83 border=0 name=imageField32></A></TD>\n");
						//salida.append("		   <TD align=center><A href = \"/Download/" + session.getUserID8()+"mv"+cuentamov+".doc" + "\" border=0><IMG  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></A></TD>\n");
//						salida.append(" 		 <TD align=\"center\"><A href=\"/Download/").append(session.getUserID8()).append("mv").append(cuentamov).append(".doc").append("\" border=\"0\"><IMG border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\"></A></TD>\n");
						salida.append("			<td align=\"center\"><a id=\"exportar\" href = 'javascript:Exportar();'").append("\" border=\"0\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\"></a></td>\n");
						salida.append(" 	     </TR>\n");
						salida.append(" 	     </TBODY>\n");
						salida.append(" 	   </TABLE>\n");
						salida.append(" 	 </TD>\n");
						salida.append("        </TBODY>\n");
						salida.append("      </TABLE>\n");
						salida.append("    </TD>\n");
						salida.append("        </TR>\n");
						salida.append("  </TR>\n");
						salida.append("  </TBODY>\n");
						salida.append("</TABLE>\n");
						salida.append("</form>\n");
						salida.append("</P>\n");

			    req.setAttribute("Output1",salida.toString());
			    req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);


			    evalTemplate(IEnlace.MOVIMIENTOS_TMPL, req, res);
			}
			else if( total_lineas >0  )
			    ShowMovs(nombrecuenta,numeroCargos,numeroAbonos,totalAbono,totalCargo,tipocuenta,tipoBanca,cuentamov,tramacuentamov,session.getArchivoMov(),total_lineas,((String)req.getParameter(ENLACE_CUENTA_PARAM)).trim(),req,res);
			else
			{

			    if("E".equals(tipoBanca)){
			    	despliegaPaginaError("No hay registros asociados a su consulta", "Consulta de movimientos de banca especializada","Consultas &gt; Movimientos &gt; Banca especializada","s25200h", req, res);
			    }else{
			    	despliegaPaginaError("No hay registros asociados a su consulta","Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras" ,"s25150h", req, res);
			    }
			}
		    }
		    else
		    {

			if("E".equals(tipoBanca)){
			    despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, "Consulta de movimientos de banca especializada","Consultas &gt; Movimientos &gt; Banca especializada","s25200h", req, res);
			}else{
				despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras" ,"s25150h", req, res);
			}
		    }
		}
		else
		{

		    if("E".equals(tipoBanca))
			despliegaPaginaError("No hay registros asociados a su consulta", "Consulta de movimientos de banca especializada","Consultas &gt; Movimientos &gt; Banca especializada","s25200h", req, res);
		    else
			despliegaPaginaError("No hay registros asociados a su consulta","Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras" ,"s25150h", req, res);
		}
	    }
	    else
	    {
		EIGlobal.mensajePorTrace("-!?***movimientos del " + prev + " al " + next, EIGlobal.NivelLog.INFO);
		ShowMovs(nombrecuenta,numeroCargos,numeroAbonos,totalAbono,totalCargo,tipocuenta,tipoBanca,cuentamov,tramacuentamov,session.getArchivoMov(),Integer.parseInt(req.getParameter("total")),((String)req.getParameter(ENLACE_CUENTA_PARAM)).trim(),req,res);
	    }
	}

	try {
		File movi= new File(Global.DIRECTORIO_MOV + "/" + session.getUserID8());
		//movi.delete();
		//EIGlobal.mensajePorTrace("Se Aplica Delete a = " + Global.DIRECTORIO_MOV + "/" + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Se omite Delete a = " + Global.DIRECTORIO_MOV + "/" + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
	} catch(Exception ex){  }

    }


    /**
     * Entrada.
     *
     * @return the int
     */
    public int entrada()
    {
	return 1;
    }



    /**
     * Movs.
     *
     * @param tipocuenta the tipocuenta
     * @param cuentamov the cuentamov
     * @param req the req
     * @param res the res
     * @param tipo_movimiento the tipo_movimiento
     * @return the int
     */
    public int movs	(String tipocuenta,String cuentamov,HttpServletRequest req,HttpServletResponse res,String tipo_movimiento)
    {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESION_PARAM);
		String cuenta	= "";
		String FechaIni = "";
		String FechaFin = "";
		String p_fechai = "";
		String p_fechaf = "";
		String auxfecha = "";
		//String servicio = "MOVS";
		String servicio = "MOVI";
		String fechaconsultafin = "";

		int salida  = 0;
		int anio    = 0;
		cuenta	    = (String)req.getParameter(ENLACE_CUENTA_PARAM);
		String hoy  = (String)req.getParameter("deldia");
		FechaIni    = (String)req.getParameter("FechaI");
		FechaFin    = (String)req.getParameter("FechaF");
		try {
			anio	    = 2000 + Integer.parseInt(FechaIni.substring(0,2));
			anio	    = 2000 + Integer.parseInt(FechaFin.substring(0,2));
			auxfecha    = FechaIni.substring(4,6) + "/" + FechaIni.substring(2,4) + "/" + anio;
			auxfecha    = FechaFin.substring(4,6) + "/" + FechaFin.substring(2,4) + "/" + anio;
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("FechaIni <" + FechaIni + ">", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("FechaFin <" + FechaFin + ">", EIGlobal.NivelLog.ERROR);
			try {
				despliegaPaginaError("Hubo un error al procesar su solicitud. Por favor intente nuevamente.", "Consulta de Movimientos", "Consultas &gt; Movimientos &gt; Chequeras", "s25200h", req, res);
			} catch(Exception ex) {
				EIGlobal.mensajePorTrace("Error al desplegar pagina de error", EIGlobal.NivelLog.ERROR);
			}
			return 1; //error
		}

		p_fechai    = auxfecha; // CAMBIO FECHAS
		p_fechaf    = auxfecha;
		session.setfechaini( p_fechai );
		session.setfechafin( p_fechaf );
		session.setArchivoMov( IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8());

		//if( tipo_movimiento.equals("C") )      servicio="MOVS";
		if("C".equals(tipo_movimiento)){
			servicio="MOVI";

		    /********MOV********/
			session.setArchivoMov(Global.DIRECTORIO_MOV + "/" + session.getUserID8());
			EIGlobal.mensajePorTrace("movimientos MOVI = " + Global.DIRECTORIO_MOV + "/" + session.getUserID8(), EIGlobal.NivelLog.DEBUG);

		}
		else if("D".equals(tipo_movimiento))
			servicio="MOVD";
		else if("V".equals(tipo_movimiento))
			servicio="MOVV";

		StringBuffer tramaentrada = new StringBuffer();
		String coderror = "";

		tramaentrada.append("2EWEB|").append(session.getUserID8()).append("|").append(servicio).append("|").append(session.getContractNumber()).append("|").append(session.getUserID8()).append("|").append(session.getUserProfile()).append("|").append(cuentamov).append("|").append(tipocuenta).append("|").append(FechaIni).append("|").append(FechaFin).append("|");
		EIGlobal.mensajePorTrace("movimientos TRAMA ENTRADA = " + tramaentrada.toString(), EIGlobal.NivelLog.DEBUG);

		try {
			ServicioTux tuxGlobal = new ServicioTux();
			//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

			EIGlobal.mensajePorTrace("***movimientos WEB_RED-MOVIMIENTOS>"+tramaentrada.toString(), EIGlobal.NivelLog.DEBUG);

			Hashtable hs = tuxGlobal.web_red(tramaentrada.toString());
			coderror = (String) hs.get("BUFFER");
//			BIT CU1801
		    /*
			 * 04/ENE/07
			 * VSWF-BMB-I
			 */
		    if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		    try{
		        BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
						equals(BitaConstants.EC_MOV_CHEQ_LINEA)){
				bt.setNumBit(BitaConstants.EC_MOV_CHEQ_LINEA_CONS_MOVS_CUENTA_CHEQUE);
				}
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
						equals(BitaConstants.EC_MOV_BANCA)){
				bt.setNumBit(BitaConstants.EC_MOV_BANCA_CONS_MOVS_CUENTA_CHEQUE);
				}
				if(session.getContractNumber()!=null){
					bt.setContrato(session.getContractNumber());
				}
				if(cuentamov!=null){
					if(cuentamov.length()>20){
						bt.setCctaOrig(cuentamov.substring(0,20));
					}else{
						bt.setCctaOrig(cuentamov);
					}
				}
				if(servicio!=null && !"".equals(servicio)){
					bt.setServTransTux(servicio.trim());
				}
				if(coderror!=null){
					if("OK".equals(coderror.substring(0,2))){
						bt.setIdErr(servicio.trim()+"0000");
					}else if(coderror.length()>8){
						bt.setIdErr(coderror.substring(0,8));
					}else{
						bt.setIdErr(coderror.trim());
					}
				}
				if(session.getUserID8()!=null&& cuentamov!=null){
					bt.setNombreArchivo(session.getUserID8()+"mv"+cuentamov+".doc");
				}
			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(NumberFormatException e){}
		    catch(SQLException e){

		    	EIGlobal.mensajePorTrace("Movimientos Mensaje Excepcion SQL -->" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}catch(Exception e){

				EIGlobal.mensajePorTrace("Movimientos Mensaje Excepcion -->" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}
		    }
	        /*
			 * VSWF-BMB-F
			 */

			EIGlobal.mensajePorTrace("***movimientos >>"+ coderror +"<<", EIGlobal.NivelLog.DEBUG);
		} catch(Exception re){  }

		/***************MOV*****************/

		ArchivoRemoto archivo_remoto = new ArchivoRemoto();


        /************************************/
		//CSA ARS Se elimina la ruta ya que el archivo de movimientos, vista y devoluciones se encuentra en la ruta DIRECTORIO_MOV.

		File arch=new File(Global.DIRECTORIO_MOV  +"/"+  session.getUserID8());
        if(arch.exists())
        {
			salida = 0; //OK
			EIGlobal.mensajePorTrace("Archivo MOVI = Existe . " + Global.DIRECTORIO_MOV + "/" + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
		} else {
			salida = 1; //Error
		}
        EIGlobal.mensajePorTrace("Resultado Segunda Condicion->"+salida, EIGlobal.NivelLog.DEBUG);
     //CSA
		return salida;
	}


    /**
     * Show movs.
     *
     * @param nombrecuenta the nombrecuenta
     * @param numeroCargos the numero cargos
     * @param numeroAbonos the numero abonos
     * @param totalAbono the total abono
     * @param totalCargo the total cargo
     * @param tipocuenta the tipocuenta
     * @param tipoBanca the tipo banca
     * @param cuentamov the cuentamov
     * @param tramacuentamov the tramacuentamov
     * @param filename the filename
     * @param numReg the num reg
     * @param p_cuenta the p_cuenta
     * @param req the req
     * @param res the res
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    public int ShowMovs (String nombrecuenta, int numeroCargos, int numeroAbonos, double totalAbono, double totalCargo, String tipocuenta,
						 String tipoBanca, String cuentamov, String tramacuentamov, String filename, int numReg, String p_cuenta,
						 HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
    {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESION_PARAM);
		StringBuffer tablainicial = new StringBuffer();
		StringBuffer tablaperiodo = new StringBuffer();
		StringBuffer tablatotalcargo = new StringBuffer();
		StringBuffer tablatotalabono = new StringBuffer();
		StringBuffer tablatitulos = new StringBuffer();
		String tabladatos      = "";
		StringBuffer tabla = new StringBuffer();
		int noDelim = 0, noCol = 0;
		String valIn = "";
		String tablasaldos = "";

		//PERIODO DE CONSULTA
		String fechaInicial = req.getParameter("fecha1");
		String fechaFinal   = req.getParameter("fecha2");
		if("".equals(fechaInicial) || fechaInicial == null ) fechaInicial = session.getfechaini().toString();
		if("".equals(fechaFinal)   || fechaFinal   == null ) fechaFinal   = session.getfechafin().toString();

		EIGlobal.mensajePorTrace("movimientos --> ShowMovs --> FECHA INICIAL <"+fechaInicial+"> FECHA FINAL<"+fechaFinal+"> ", EIGlobal.NivelLog.INFO);

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());



		if("".equals(tipoBanca) || tipoBanca == null )
			tipoBanca = "C";

		if("E".equals(tipoBanca)){
			req.setAttribute("Encabezado", CreaEncabezado("Consulta de movimientos de banca especializada","Consultas &gt; Movimientos &gt; Banca especializada","s25200h",req));
		}else if ( ( (String )req.getParameter("deldia") ).equals("NORM") ){
			req.setAttribute("Encabezado", CreaEncabezado("Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras","s25170h",req));
		}else{
			req.setAttribute("Encabezado", CreaEncabezado("Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras","s25180h",req));
		}
		if( "E".equals(tipoBanca) ) noDelim=11;
		else			    noDelim=9;
		if("E".equals(tipoBanca)) noCol=10;
		else			    noCol=9;

		String array_mvs[][] = readlinefromFile(tipoBanca,filename,noDelim,numReg);
		p_cuenta = p_cuenta.replace ('@',' ');

		if( ((String)req.getParameter("deldia")).equals("NORM") )
		{

			if("E".equals(tipoBanca))
			tablasaldos=TraeSaldosEspeciales(tipocuenta,cuentamov,req);
			else
			tablasaldos=TraeSaldosChequera(tipocuenta,tipoBanca,cuentamov,req );
		} else
			tablasaldos="";

		tablainicial.append(" <input id=\"tArchivo\" type=\"hidden\" name=\"tArchivo\" value=\"csv\" readonly=\"readonly\" /> \n");
		tablainicial.append("<table width=\"750\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">\n");
		tablainicial.append(" <tr>\n");
		tablainicial.append("  <td valign=\"bottom\" align=\"left\" class=\"textabref\" width=\"375\">Cuenta:").append(cuentamov).append("&nbsp;").append(nombrecuenta).append("<br></td>\n");
		GregorianCalendar calHoy = new GregorianCalendar();
		GregorianCalendar cal1	 = new GregorianCalendar();
		cal1.set(
				Integer.parseInt(session.getfechaini().substring(6,10)),
				Integer.parseInt(session.getfechaini().substring(3,5))-1,
				Integer.parseInt(session.getfechaini().substring(0,2))
			);
		GregorianCalendar cal2 = new GregorianCalendar();
		cal2.set(
				Integer.parseInt(session.getfechafin().substring(6,10)),
				Integer.parseInt(session.getfechafin().substring(3,5))-1,
				Integer.parseInt(session.getfechafin().substring(0,2))
			);
		if(
			calHoy.get(Calendar.DAY_OF_MONTH)==cal1.get(Calendar.DAY_OF_MONTH) &&
			calHoy.get(Calendar.MONTH) == cal1.get(Calendar.MONTH) &&
			calHoy.get(Calendar.YEAR) + 1900 == cal1.get(Calendar.YEAR) &&
			calHoy.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH) &&
			calHoy.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
			calHoy.get(Calendar.YEAR) + 1900 == cal2.get(Calendar.YEAR)
		  )
			tablaperiodo.append("<tr><td valign=bottom align=left class=textabref>Movimientos del d&iacute;a</td>\n");
		else

		tablaperiodo.append("<tr><td valign=bottom align=left class=textabref>Per&iacute;odo de:").append(fechaInicial).append("&nbsp;al&nbsp;").append(fechaFinal).append("</td>\n");
		tablatitulos.append("<table width=\"750\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">\n");
		tablatitulos.append("<tr>\n");

		tablatitulos.append("<TD class=\"tittabdat\" align=\"center\">Fecha</TD>\n");

		if(!"E".equals(tipoBanca) )
		{   //Eliminar columnas para Banca Especializada (fsj)
			tablatitulos.append("<TD class=\"tittabdat\" align=\"center\">Hora</TD>\n");
			tablatitulos.append("<TD class=\"tittabdat\" align=\"center\">Suc.</TD>\n");
		}//fin if

		tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Descripci&oacute;n</TD>\n");

		if("E".equals(tipoBanca))
			tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Emisora</TD>\n");
			tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Cargo</TD>\n");
			tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Abono</TD>\n");
		if(!"E".equals(tipoBanca))
			tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Saldo</TD>\n");
			tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Referencia</TD>\n");
		if(!"E".equals(tipoBanca))
			tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Concepto</TD>\n");
			tablatitulos.append("<td class=\"tittabdat\" align=\"center\">Referencia Interbancaria</TD>\n");

		double [] tCargo = new double [1];
		double [] tAbono = new double [1];
		int [] nCargos = new int [1];
		int [] nAbonos = new int [1];
		tAbono [0] = totalAbono;
		tCargo [0] = totalCargo;
		nAbonos[0] = numeroAbonos;
		nCargos[0] = numeroCargos;
		int rIni = ( req.getParameter("prev")==null ) ? 0 : Integer.parseInt(req.getParameter("prev"));
		int rFin = ( req.getParameter("next")==null ) ? Global.NUM_REGISTROS_PAGINA : Integer.parseInt(req.getParameter("next"));
		rFin = ( rFin>numReg ) ? numReg : rFin;

	if("E".equals(tipoBanca)){
			tabladatos +=armaRenBEspecial(nCargos,nAbonos,tAbono,tCargo,tipoBanca,array_mvs,rIni,rFin);
	}
	else{
			tabladatos +=armaRenNormal(nCargos,nAbonos,tAbono,tCargo,array_mvs,rIni,rFin);
	}
		totalCargo = tCargo [0];
		totalAbono = tAbono [0];
		numeroAbonos = nAbonos[0];
		numeroCargos = nCargos[0];
		tablatotalcargo.append("<td align=\"right\" class=\"textabref\" width=\"372\">Total de cargos: ").append(numeroCargos).append("  por ").append(FormatoMoneda(totalCargo)).append("</td></tr>\n");
		tablatotalabono.append("<td align=\"right\" class=\"textabref\">Total de abonos: ").append(numeroAbonos).append("  por ").append(FormatoMoneda(totalAbono)).append("</td></tr></table>\n");

		tabla.append("<Form name =\"frmbit\" method = \"POST\" Action=\"movimientos\">\n");
		tabla.append(tablasaldos);
		tabla.append(tablainicial);
		tabla.append(tablatotalcargo);
		tabla.append(tablaperiodo);
		tabla.append(tablatotalabono);
		tabla.append(tablatitulos);
		tabla.append(tabladatos);
		String valor = null;

		tabla.append("<tr><td COLSPAN=9 ALIGN=CENTER class=\"textabref\" width=\"372\">Movimientos : ").append( (rIni+1) ).append(" - ").append(rFin).append(" de ").append(numReg).append("</td></TR>\n");
		if( rIni>0 )
			tabla.append("<tr><td  COLSPAN=9 ALIGN=CENTER class=\"textabref\" width=\"372\"><A HREF=\"javascript:atras();\">< Anteriores  ").append(Global.NUM_REGISTROS_PAGINA).append("</A></td></TR>\n");
		if( rFin<numReg )
		{
			if( (rFin + Global.NUM_REGISTROS_PAGINA)<=numReg )
			tabla.append("<tr><td COLSPAN=\"9\" ALIGN=\"CENTER\" class=\"textabref\" width=\"372\"><A HREF=\"javascript:adelante();\"> Siguientes ").append(Global.NUM_REGISTROS_PAGINA).append("></A></td></TR>\n");
			else
			tabla.append("<tr><td COLSPAN=\"9\" ALIGN=\"CENTER\" class=\"textabref\" width=\"372\"><A HREF=\"javascript:adelante();\"> Siguientes ").append((numReg -rFin)).append("></A></td></TR>\n");
		}
		tabla.append("<input type=\"Hidden\" name=\"total\"	   value=\"").append(numReg)   .append("\">\n");
		tabla.append("<input type=\"Hidden\" name=\"prev\"	   value=\"").append(rIni)     .append("\">\n");
		tabla.append("<input type=\"Hidden\" name=\"next\"	   value=\"").append(rFin)     .append("\">\n");
		tabla.append("<input type=\"Hidden\" name=\"Banca\"	   value=\"").append(tipoBanca).append("\">\n");
		tabla.append("<input type=\"Hidden\" name=\"deldia\"	   value=\"").append(req.getParameter("deldia")).append("\">\n");
		tabla.append("<input type=\"hidden\" name=\"EnlaceCuenta\" value=\"").append(tramacuentamov).append("\">\n");
		tabla.append("<input type=\"Hidden\" name=\"fecha1\"	   value=\"").append(req.getParameter("fecha1")).append("\">\n");
		tabla.append("<input type=\"Hidden\" name=\"fecha2\"	   value=\"").append(req.getParameter("fecha2")).append("\">\n");
		//INICIA MODIFICACION PAMPA
		tabla.append("<input type=\"hidden\" name=\"pampa_fecha\"	   value=\"\">\n");
		tabla.append("<input type=\"hidden\" name=\"pampa_importe\"	   value=\"\">\n");
		tabla.append("<input type=\"hidden\" name=\"pampa_referencia_390\" value=\"\">\n");
		tabla.append("<input type=\"hidden\" name=\"TMov\"		   value=\"MOVS\">\n");
		//TERMINA MODIFICACION PAMPA
		tabla.append("</table>\n\n");

		//EXPORTACION
		tabla.append("<table style=\"margin-left: auto; margin-right: auto;\">\n");
		tabla.append("<tr>\n");
		tabla.append("<td><input type=\"radio\" value=\"txt\" name=\"tipoArchivo\" onclick=\"cambiaArchivo('txt');\"  /></td>\n");
		tabla.append("<td class=\"tabmovtex11\">Exporta en TXT</td>\n");
		tabla.append("</tr>\n");
		tabla.append("<tr>\n");
		tabla.append("<td><input type=\"radio\" value=\"csv\" name=\"tipoArchivo\" onclick=\"cambiaArchivo('csv');\" checked=\"checked\" /></td>\n");
		tabla.append("<td class=\"tabmovtex11\">Exporta en XLS</td>\n");
		tabla.append("</tr>\n");
		tabla.append("</table>\n\n");

		tabla.append("</Form>\n");


		StringBuffer tablabotones = new StringBuffer();
		//String tablabotones = "";

		tablabotones.append("<td align=\"center\"><a href=\"javascript:scrImpresion();\" border=\"0\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25240.gif\"></a></td>\n");
		tablabotones.append("<td align=\"center\"><a id=\"exportar\" href = 'javascript:Exportar();'").append("\" border=\"0\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\"></a></td>\n");
//		tablabotones.append("<td align=\"center\"><a href=\"/Download/").append(session.getUserID8()).append("mv").append(cuentamov).append(".doc").append("\" border=\"0\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\"></a></td>\n");
		req.setAttribute("Output1",tabla.toString());
		req.setAttribute("Output2",tablabotones.toString());
		req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
//		BIT CU1081
	    /*
		 * 04/ENE/07
		 * VSWF-BMB-I
		 */
		 if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
	    try{
	    	BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.EC_MOV_CHEQ_LINEA)){
				bt.setNumBit(BitaConstants.EC_MOV_CHEQ_LINEA_CONS_MOVS_CUENTA_CHEQUE);
			}
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.EC_MOV_BANCA)){
				bt.setNumBit(BitaConstants.EC_MOV_BANCA_CONS_MOVS_CUENTA_CHEQUE);
			}
			if(session.getContractNumber()!=null){
				bt.setContrato(session.getContractNumber());
			}
			if(cuentamov!=null){
				if(cuentamov.length()>20){
					bt.setCctaOrig(cuentamov.substring(0,20));
				}else{
					bt.setCctaOrig(cuentamov.trim());
				}

			}
				bt.setImporte(totalAbono);

			if(session.getUserID8()!=null&& cuentamov!=null){
				bt.setNombreArchivo(session.getUserID8()+"mv"+cuentamov+".doc");
			}
			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch (NumberFormatException e){}
	    catch(SQLException e){

	    	EIGlobal.mensajePorTrace("Movimientos Mensaje Excepcion SQLException -->" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){

			EIGlobal.mensajePorTrace("Mensaje Excepcion  -->" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		 }
        /*
		 * VSWF-BMB-F
		 */
		evalTemplate(IEnlace.MOVIMIENTOS_TMPL, req, res);
		return 0;
    }

    /**
     * Arma ren normal.
     *
     * @param nCargos the n cargos
     * @param nAbonos the n abonos
     * @param tAbono the t abono
     * @param tCargo the t cargo
     * @param mvs the mvs
     * @param rIni the r ini
     * @param rFin the r fin
     * @return the string
     */
    public String armaRenNormal (int[] nCargos, int[] nAbonos, double[] tAbono, double[] tCargo,
	                             String[][] mvs, int rIni, int rFin)
    {
		String fecha            = null;
		StringBuffer cadRenglon = new StringBuffer();
		String clase	        = "";
		double Cargo_Abono      = 0.0;
		double totalCargo       = 0;
		String temporal         = "";
		String concepto         = ""; //Ley de Transparencia II
		String referencia       = ""; //Ley de Transparencia II
		String campoCompleto    = ""; //Ley de Transparencia II
		double pampa_abono      = 0.0;//Modificaci?n pampa
		String pampa_fecha      = ""; //Modificaci?n pampa
		//totalCargo	        = 0;
		double totalAbono       = 0;
		int numeroCargos        = 0;
		int numeroAbonos        = 0;

		if(mvs == null || rFin >= mvs.length)
			return "<tr><td></td></tr>";

		for(int j=rIni; j<rFin; j++)
		{

			if( j%2!=0 )
			{
				cadRenglon.append("<TR>");
				clase="textabdatobs";
			} else {
				cadRenglon.append("<TR>");
				clase="textabdatcla";
			}

			if(j >= mvs.length || 1 >= mvs[0].length ) {
				cadRenglon.append("<td class=").append(clase).append("	align=center nowrap >").append("</td>");
			}
			else if( mvs[j][1] != null && mvs[j][1].trim().length()>0 )
			{
				fecha = mvs[j][1].substring(0, 2);
				fecha = mvs[j][1].substring(2,4) + "/" + fecha + "/" + mvs[j][1].substring(4,6);
				cadRenglon.append("<td class=").append(clase).append("	align=center nowrap >").append(fecha).append("</td>");
				pampa_fecha=mvs[j][1].substring(2,4)+mvs[j][1].substring(0,2)+mvs[j][1].substring(4,6);  //Modificacion  PAMPA
			} else
				cadRenglon.append("<td	class=").append(clase).append("  align=center nowrap >&nbsp;&nbsp;&nbsp;</td>");

			if(j >= mvs.length || 7 >= mvs[0].length )
				cadRenglon.append("<td class=").append(clase).append("	align=center nowrap >").append("</td>");;

			if( mvs[j][7] != null && mvs[j][7].trim().length()>0 )
				cadRenglon.append("<td class=").append(clase).append(" align=center nowrap >").append(mvs[j][7]).append("</td>");
			else
				cadRenglon.append("<td class=").append(clase).append(" align=center nowrap >&nbsp;&nbsp;</td>");

			if( mvs[j][6] != null && mvs[j][6].trim().length()>0 )
				cadRenglon.append("<td class=").append(clase).append(" align=center nowrap >").append(mvs[j][6]).append("</td>");
			else
				cadRenglon.append("<td class=").append(clase).append(" align=center nowrap>&nbsp;&nbsp;&nbsp;</td>");

			if( mvs[j][2] != null && mvs[j][2].trim().length()>0 )
				cadRenglon.append("<td class=").append(clase).append(" align=left >").append(mvs[j][2].trim()).append("</td>");
			else
				cadRenglon.append("<td class=").append(clase).append(ALING_SERV);

			try {
				String valor_ = "0.0";

				if(mvs[j][5] == null || mvs[j][5].trim().equals("") )
					valor_ = "0.0";
				else
					valor_ = mvs[j][5].trim();

				try {
					Cargo_Abono = new Double(valor_).doubleValue();
				} catch(Exception e) {
					Cargo_Abono=0.0;
				}

				Cargo_Abono = new Double(valor_).doubleValue();

				if( mvs[j][0] != null && mvs[j][0].equals("-") )
				{
					try {
						temporal = FormatoMoneda(new Double(Cargo_Abono /100).doubleValue());
					} catch(Exception e) {
						temporal = "";
					}

					if( temporal==null )
						temporal = "";

					cadRenglon.append("<td class=").append(clase).append(" align=right nowrap>").append(temporal).append("</td>");
					cadRenglon.append("<td class=").append(clase).append(" align=right nowrap>&nbsp;&nbsp;&nbsp;</td>");
					totalCargo += Cargo_Abono/100;
					numeroCargos++;
				} else {
					try {
						temporal = FormatoMoneda(new Double(Cargo_Abono /100).doubleValue())	;
						pampa_abono=Cargo_Abono; //Modificacion  PAMPA
					} catch(Exception e) {
						temporal = "";
						pampa_abono=0.0; //Modificacion  PAMPA
					}

					if( temporal==null )
						temporal = "";

					cadRenglon.append("<td class=").append(clase).append(" align=right nowrap>&nbsp;&nbsp;&nbsp;</td>");
					cadRenglon.append("<td class=").append(clase).append(" align=right nowrap>").append(temporal).append("</td>");
					totalAbono += Cargo_Abono/100;
					numeroAbonos++;
				}
			} catch (NumberFormatException e) {
				cadRenglon.append("<td class=").append(clase).append(" align=right>&nbsp;&nbsp;&nbsp;</td>");
				cadRenglon.append("<td class=").append(clase).append(" align=right>&nbsp;&nbsp;&nbsp;</td>");
			}
			try {
				String valor_ = "0.0";

				if( mvs[j][8] == null || mvs[j][8].trim().equals("") )
					valor_ = "0.0";
				else
					valor_ = mvs[j][8].trim();

				try {
					Cargo_Abono = new Double(valor_).doubleValue();
				} catch(Exception e) {
					Cargo_Abono=0.0;
				}

				if( mvs[j][8] != null && mvs[j][8].trim().length()>0 )
				{
					try {
						temporal = FormatoMoneda(new Double(Cargo_Abono/100).doubleValue());
					} catch(Exception e) {
						temporal = "";
					}

					if( temporal==null )
						temporal = "";

					cadRenglon.append("<td class=").append(clase).append(" align=right nowrap>").append(temporal).append("</td>");
				} else
					cadRenglon.append("<td class=").append(clase).append(" align=right nowrap>&nbsp;&nbsp;&nbsp;</td>");
			} catch( NumberFormatException e ) {
				cadRenglon.append("<td class=").append(clase).append(" align=right nowrap>&nbsp;&nbsp;&nbsp;</td>");
			}
			//******MODIFICACION PAMPA*******
			if( mvs[j][4] != null && mvs[j][4].trim().length()>0 )
			{
				String NumeroComercio = PAMPA_getNumComercio( mvs[j][9].trim() );

				if(mvs[j][0] != null && mvs[j][2] != null && mvs[j][0].equals("+") && mvs[j][2].trim().equals("DEP ELE PAG TC") && NumeroComercio.length()>0)
					cadRenglon.append("<td class=").append(clase).append(" align=center><a href=\"javascript:llamar_detalle(1").append(pampa_fecha).append(",").append(pampa_abono).append(",1").append(NumeroComercio).append(");\">").append(mvs[j][4].trim()).append("</a></td>");
				else
					cadRenglon.append("<td class=").append(clase).append(" align=center>").append(mvs[j][4].trim()).append("</td>");
			} else
				cadRenglon.append("<td class=").append(clase).append(" align=center>&nbsp;&nbsp;&nbsp;</td>");

			// Proyecto Ley de Transparencia II
			if ( mvs[j][9] == null || mvs[j][9].equals("") )
			{
				mvs[j][9] = "&nbsp;&nbsp;&nbsp;";
			}

			if (mvs[j][9] != null) {
				campoCompleto = mvs[j][9];
			}
			else {
				campoCompleto = "";
			}

			String[] Datos = null;
			StringBuffer concatenacion = new StringBuffer();
			EIGlobal.mensajePorTrace("movimientos.java, campoCompleto::"+campoCompleto+"::" , EIGlobal.NivelLog.DEBUG);
			if (mvs[j][2] != null && validaRespuestaTransferEnvio(mvs[j][2])) { // Presenta Referencia y concepto de Abonos
				Datos = campoCompleto.split("REF ");//mvs[j][9]
				if (campoCompleto.length() >= 40 && campoCompleto.indexOf("REF") == -1)// Concepto 40 Pos + espacio + 7 referencia + espacio + Referencia Interna 15
				{
					concepto = campoCompleto.substring(0,40);
	                cadRenglon.append("<td class=").append(clase).append(" align=left>").append(concepto).append("</td>");
	                if (campoCompleto.length() > 63){
	                	referencia = campoCompleto.substring(40, campoCompleto.length()-16);
	                    cadRenglon.append("<td class=").append(clase).append(" align=left>").append(referencia).append("</td>");
	                }else{
	                	cadRenglon.append("<td class=").append(clase).append(ALING_SERV);
	                }
				} else {
					if(Datos.length==1){//la cadena no contiene REF
					EIGlobal.mensajePorTrace("movimientos.java, la cadena no contiene REF" , EIGlobal.NivelLog.DEBUG);
					cadRenglon.append("<td class=").append(clase).append(" align=left>").append(campoCompleto).append("</td>");
					EIGlobal.mensajePorTrace("movimientos.java, 1::"+campoCompleto + "::" , EIGlobal.NivelLog.DEBUG);
					cadRenglon.append("<td class=").append(clase).append(ALING_SERV);
					EIGlobal.mensajePorTrace("movimientos.java, 1::"+ALING_SERV + "::" , EIGlobal.NivelLog.DEBUG);
				}else
				if(Datos.length==2){//el campo solo contiene una cadena Ref
					EIGlobal.mensajePorTrace("movimientos.java, el campo solo contiene una cadena REF" , EIGlobal.NivelLog.DEBUG);
					cadRenglon.append("<td class=").append(clase).append(" align=left>").append(Datos[0]).append("</td>");
					EIGlobal.mensajePorTrace("movimientos.java, 2::"+Datos[0] + "::" , EIGlobal.NivelLog.DEBUG);
					cadRenglon.append("<td class=").append(clase).append(" align=left>").append(Datos[1]).append("</td>");
					EIGlobal.mensajePorTrace("movimientos.java, 2::"+Datos[1] + "::" , EIGlobal.NivelLog.DEBUG);
				}else
				if(Datos.length>2){//el campo contiene mas de 2 ref
					EIGlobal.mensajePorTrace("movimientos.java, el campo contiene mas de una cadena REF" , EIGlobal.NivelLog.DEBUG);
					for(int i=0; i<(Datos.length-1);i++){
						concatenacion.append(Datos[i]);
						if(i<(Datos.length-2)){
							concatenacion.append("REF ");
						}
					}
					concepto = concatenacion.toString();
					referencia = Datos[Datos.length-1];
					cadRenglon.append("<td class=").append(clase).append(" align=left>").append(concepto).append("</td>");
					EIGlobal.mensajePorTrace("movimientos.java, 3::"+concepto + "::" , EIGlobal.NivelLog.DEBUG);
					cadRenglon.append("<td class=").append(clase).append(" align=left>").append(referencia).append("</td>");
					EIGlobal.mensajePorTrace("movimientos.java, 3::"+referencia + "::" , EIGlobal.NivelLog.DEBUG);
				}
					}

			} else if (mvs[j][2] != null && validaRespuestaTransferRecep(mvs[j][2])) { // Presenta Referencia y concepto de Cargos
				EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::"+mvs[j][2]+"::" , EIGlobal.NivelLog.DEBUG);


				if (campoCompleto.length() >= 40 && campoCompleto.indexOf("REF") == -1) // Concepto 40 Pos + Referencia <= 7 + h + cuentahordenante 20
				{
					EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::campo.length::" + campoCompleto.length() , EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::Entre campo.lengt>=40" , EIGlobal.NivelLog.DEBUG);
					concepto = campoCompleto.substring(0,40);
					cadRenglon.append("<td class=").append(clase).append(" align=left>").append(concepto).append("</td>");
	                if (campoCompleto.length() > 48){
	                	EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::Entre campo.lengt>67" , EIGlobal.NivelLog.DEBUG);
	                	referencia = campoCompleto.substring(40, 47);
	                	EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::campoCompleto.substring(40, 47)::"+campoCompleto.substring(40, 47)+"::" , EIGlobal.NivelLog.DEBUG);
	                	EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::campoCompleto::"+campoCompleto+"::" , EIGlobal.NivelLog.DEBUG);
	                    cadRenglon.append("<td class=").append(clase).append(" align=left>").append(referencia).append("</td>");
	                }else{
	                	EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::Entre campo.lengt!=67" , EIGlobal.NivelLog.DEBUG);
	                	cadRenglon.append("<td class=").append(clase).append(ALING_SERV);
	                }
				} else {
					Datos = campoCompleto.split("REF ");//mvs[j][9]
					EIGlobal.mensajePorTrace("movimientos.java, ::validaRespuestaTransferRecep::Entre campo.lengt<40" , EIGlobal.NivelLog.DEBUG);
					if (campoCompleto.length() >= 40 && campoCompleto.indexOf("REF") == -1)// Concepto 40 Pos + espacio + 7 referencia + espacio + Referencia Interna 15
						{
							concepto = campoCompleto.substring(0,40);
							cadRenglon.append("<td class=").append(clase).append(" align=left>").append(concepto).append("</td>");
							if (campoCompleto.length() > 48){
		                	referencia = campoCompleto.substring(40, campoCompleto.length()-16);
		                    cadRenglon.append("<td class=").append(clase).append(" align=left>").append(referencia).append("</td>");
						} else {
		                	cadRenglon.append("<td class=").append(clase).append(ALING_SERV);
		                }
					} else {
						if(Datos.length==1){//la cadena no contiene REF
						EIGlobal.mensajePorTrace("movimientos.java, la cadena no contiene REF" , EIGlobal.NivelLog.DEBUG);
						cadRenglon.append("<td class=").append(clase).append(" align=left>").append(campoCompleto).append("</td>");
						EIGlobal.mensajePorTrace("movimientos.java, 1::"+campoCompleto + "::" , EIGlobal.NivelLog.DEBUG);
						cadRenglon.append("<td class=").append(clase).append(ALING_SERV);
						EIGlobal.mensajePorTrace("movimientos.java, 1::"+ALING_SERV + "::" , EIGlobal.NivelLog.DEBUG);
					}else
					if(Datos.length==2){//el campo solo contiene una cadena Ref
						EIGlobal.mensajePorTrace("movimientos.java, el campo solo contiene una cadena REF" , EIGlobal.NivelLog.DEBUG);
						cadRenglon.append("<td class=").append(clase).append(" align=left>").append(Datos[0]).append("</td>");
						EIGlobal.mensajePorTrace("movimientos.java, 2::"+Datos[0] + "::" , EIGlobal.NivelLog.DEBUG);
						cadRenglon.append("<td class=").append(clase).append(" align=left>").append(Datos[1]).append("</td>");
						EIGlobal.mensajePorTrace("movimientos.java, 2::"+Datos[1] + "::" , EIGlobal.NivelLog.DEBUG);
					}else
					if(Datos.length>2){//el campo contiene mas de 2 ref
						EIGlobal.mensajePorTrace("movimientos.java, el campo contiene mas de una cadena REF" , EIGlobal.NivelLog.DEBUG);
						for(int i=0; i<(Datos.length-1);i++){
							concatenacion.append(Datos[i]);
							if(i<(Datos.length-2)){
								concatenacion.append("REF ");
							}
						}
						concepto = concatenacion.toString();
						referencia = Datos[Datos.length-1];
							cadRenglon.append("<td class=").append(clase).append(" align=left>").append(concepto).append("</td>");
							EIGlobal.mensajePorTrace("movimientos.java, 3::"+concepto + "::" , EIGlobal.NivelLog.DEBUG);
							cadRenglon.append("<td class=").append(clase).append(" align=left>").append(referencia).append("</td>");
							EIGlobal.mensajePorTrace("movimientos.java, 3::"+referencia + "::" , EIGlobal.NivelLog.DEBUG);
						}

					}
				}
			} else if (mvs[j][2] != null && mvs[j][2].trim().equals("APORT LC INNET")){
				concepto = "";
	            cadRenglon.append("<td class=").append(clase).append(" align=left>").append(concepto).append("</td>");
	            cadRenglon.append("<td class=").append(clase).append(ALING_SERV);
			} else{
				cadRenglon.append("<td class=").append(clase).append(" align=left>").append(campoCompleto).append("</td>");
				cadRenglon.append("<td class=").append(clase).append(ALING_SERV);
				// Proyecto Ley de Transpatencia II
			}

			cadRenglon.append("</TR>\n");
		}
		tCargo [0] = totalCargo;
		tAbono [0] = totalAbono;
		nCargos[0] = numeroCargos;
		nAbonos[0] = numeroAbonos;
		return cadRenglon.toString();
	}

	/**
     * Metodo validaRespuestaTransferEnvio para validar si la respuesta de transfer contiene leyenda Abono.
     * Se actualiza por Observaciones Banxico
     * FWS-ARS Indra 04-12-2014
     * @param cveAlfaChica clave alfa chica
     * @return boolean
     */
    private boolean validaRespuestaTransferEnvio(String cveAlfaChica){
    	if(("PAG SPEUA/INTE").equals(cveAlfaChica.trim()) || ("PA TR SPEI/TEF").equals(cveAlfaChica.trim()) || ("AN TR SPEI/TEF").equals(cveAlfaChica.trim())){
    		return true;
    	}
    	return false;
    }

    /**
     * Metodo validaRespuestaTransferRecep para validar si la respuesta de transfer contiene leyenda Abono.
     * Se actualiza por Observaciones Banxico
     * FWS-ARS Indra 04-12-2014
     * @param cveAlfaChica clave alfa chica
     * @return boolean
     */
    private boolean validaRespuestaTransferRecep(String cveAlfaChica){
    	if(("AB TR SPEI/TEF").equals(cveAlfaChica.trim()) || ("AB TRANSF TEF").equals(cveAlfaChica.trim()) ||("AN AB SPEI/TEF").equals(cveAlfaChica.trim())){
    		return true;
    	}
    	return false;
    }

    /**
     * Arma ren b especial.
     *
     * @param nCargo the n cargo
     * @param nAbono the n abono
     * @param tAbono the t abono
     * @param tCargo the t cargo
     * @param tipoBanca the tipo banca
     * @param mvs the mvs
     * @param rIni the r ini
     * @param rFin the r fin
     * @return the string
     */
    public String armaRenBEspecial
	(
	    int[]  nCargo,
	    int[]  nAbono,
	    double[]  tAbono,
	    double[]  tCargo,
	    String  tipoBanca,
	    String[][]  mvs,
	    int  rIni,
	    int  rFin
	)
    {
	StringBuffer cadRenglon=new StringBuffer();
	double Cargo_Abono  = 0.0;
	String color	    = "";
	double totalCargo   = 0;
	double totalAbono   = 0;
	int numeroCargos    = 0;
	int numeroAbonos    = 0;
	for(int j=rIni; j<rFin; j++ )
	{

		cadRenglon.append("<TR>\n");

	    if( j%2!=0 )
	    	color= "textabdatobs";
	    else
	    	color= "textabdatcla";

	    if( mvs[j][0].trim().length()>=6 ) //FECHA
	    	cadRenglon.append("<td align=\"center\" class=\"").append(color).append("\">&nbsp;").append(mvs[j][0].substring(0,2)).append("/").append(mvs[j][0].substring(2,4)).append("/").append(mvs[j][0].substring(4,6)).append("&nbsp;</td>\n"); //Formato dd/mm/aa
	    else
	    	cadRenglon.append("<td align=\"center\" class=\"").append(color).append("\">&nbsp;</td>\n");

	    //-fsj- cadRenglon.append("<td class=\"").append(color).append("\">&nbsp;</td>\n");
	    //-fsj- cadRenglon.append("<td class=\"").append(color).append("\">&nbsp;</td>\n");
	    if(mvs[j][2].trim().length()>0) //DESCRIPCION
	    	cadRenglon.append("<td align=\"left\"   class=\"").append(color).append("\">&nbsp;").append(mvs[j][2].trim()).append("&nbsp;</td>\n");
	    else
	    	cadRenglon.append("<td align=\"left\"   class=\"").append(color).append("\">&nbsp;</td>\n");

	    if( mvs[j][3].trim().length()>0 ) //EMISORA
	    	cadRenglon.append("<td align=\"center\" class=\"").append(color).append("\">&nbsp;").append(mvs[j][3].trim()).append("&nbsp;</td>\n");
	    else
	    	cadRenglon.append("<td align=\"center\" class=\"").append(color).append("\">&nbsp;</td>\n");

	    if( mvs[j][10].indexOf("-")>-1 )
	    	mvs[j][10]=mvs[j][10].substring(1);
	    	Cargo_Abono = new Double(mvs[j][10].trim()).doubleValue();

	    if(mvs[j][10].indexOf("-")>-1 )
	    {
	    	if("E".equals(tipoBanca))
	    		cadRenglon.append("<td class=\"").append(color).append("\" align=\"right\">&nbsp;").append(FormatoMoneda(Cargo_Abono)).append("&nbsp;</td>\n");
	    	else
	    		cadRenglon.append("<td class=\"").append(color).append("\" align=\"right\">&nbsp;").append(FormatoMoneda(new Double(Cargo_Abono /100).doubleValue())).append("&nbsp;</td>\n");

		cadRenglon.append("<td class=\"").append(color).append("\">&nbsp;</td>\n");
		totalCargo += Cargo_Abono;
		numeroCargos ++;
	    }//fin if
	    else
	    {
		cadRenglon.append("<td class=\"").append(color).append("\">&nbsp;</td>");
		if("E".equals(tipoBanca))
			cadRenglon.append("<td class=\"").append(color).append("\" align=\"right\">&nbsp;").append(FormatoMoneda(Cargo_Abono)).append("&nbsp;</td>\n");
		else
			cadRenglon.append("<td class=\"").append(color).append("\" align=\"right\">&nbsp;").append(FormatoMoneda(new Double(Cargo_Abono/100).doubleValue())).append("&nbsp;</td>\n");
		totalAbono += Cargo_Abono;
		numeroAbonos++;
	    }//fin else
	    //-fsj- cadRenglon.append("<td class=\"").append(color).append("\">&nbsp;</td>");

	    if(mvs[j][1].trim().length()>0)
	    	cadRenglon.append("<td class=\"").append(color).append("\" align=\"center\">&nbsp;").append(mvs[j][1].trim()).append("&nbsp;</td>\n");
	    else
	    	cadRenglon.append("<td class=\"").append(color).append("\">&nbsp;</td>\n");

	    //-fsj- cadRenglon.append("<td class=\"").append(color).append("\">&nbsp;</td>\n");
	    cadRenglon.append("</tr>\n");
	}//fin for
	tCargo [0] = totalCargo;
	tAbono [0] = totalAbono;
	nCargo [0] = numeroCargos;
	nAbono [0] = numeroAbonos;
	return cadRenglon.toString();
    }

    /**
     * Readlinefrom file.
     *
     * @param tipoBanca the tipo banca
     * @param filename the filename
     * @param Ndelim the ndelim
     * @param numReg the num reg
     * @return the string[][]
     */
    public String[][] readlinefromFile (String tipoBanca, String filename, int Ndelim, int numReg)
    {

		//***** everis Inicializando BufferedReader  08/05/2008  ..inicio
		//BufferedReader entrada;
		BufferedReader entrada = null;
	   //***** everis Inicializando BufferedReader  08/05/2008  ..fin

		StringBuffer cadenaArc = new StringBuffer();
		String linea = null;
		FileReader leer = null;
		int i = 0;
		int j = 0;
		int k = 0;

		EI_Tipo armaTabla = new EI_Tipo();

		String[][] salida = new String[numReg][Ndelim + 1];

		try
		{
			leer = new FileReader(filename);
			entrada = new BufferedReader(leer);
			linea = entrada.readLine();

			for(j=0;j<numReg;j++)
			{
				linea = entrada.readLine();
				if(linea != null) {
					cadenaArc.append(linea.trim() + "@");
				}
			}
			armaTabla.iniciaObjeto(';','@',cadenaArc.toString());
			entrada.close();
		} catch(Exception e) {

			EIGlobal.mensajePorTrace(">> readlinefromFile ---> Mensaje Excepcion  -->" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}


		//***** everis Cerrando BufferedReader 08/05/2008  ..inicio
		finally{
            if(null != entrada){
                try{
                	entrada.close();
                }catch(Exception e){}
                entrada = null;
            }
        }

		//****  everis Cerrando BufferedReader 08/05/2008  ..fin



		if("E".equals(tipoBanca))
			ordenarMovsFondos(armaTabla.camposTabla,numReg,Ndelim);//fsj
		return armaTabla.camposTabla;
	}

    /**
     * Obtener index.
     *
     * @param fch the fch
     * @return the int
     */
    private int obtenerIndex(String fch)
    {	//fsj
	String dat = "0";
	try
	{
	    int dia = Integer.parseInt( fch.substring(0,2) );
	    int mes = Integer.parseInt( fch.substring(2,4) );
	    int ani = Integer.parseInt( fch.substring(4  ) ) + 2000;
	    dat = ""+ (ani * 12 + mes) +""+ dia;
	}catch(Exception e){  }
	return Integer.parseInt(dat);
    }

    /**
     * Ordenar movs fondos.
     *
     * @param tmpR the tmp r
     * @param ren the ren
     * @param col the col
     */
    private void ordenarMovsFondos(String[][] tmpR,int ren,int col)
    {	//fsj
	for(int i=0; i<ren ;i++)
	{
	    int dat1 = obtenerIndex( tmpR[i][0] );
	    for(int j=i+1; j<ren ;j++)
	    {
		int dat2 = obtenerIndex( tmpR[j][0] );
		if( dat1>dat2 )
		{
		    String[] tmp1 = tmpR[i];
		    String[] tmp2 = tmpR[j];
		    tmpR[i] = tmp2;
		    tmpR[j] = tmp1;
		    dat1 = dat2;
		}//fin if
	    }//fin for j
	}//fin for i
    }

    /**
     * Gets the numberlines.
     *
     * @param filename the filename
     * @return the numberlines
     */
    public int getnumberlines(String filename)
    {
	int j = 0;
	String linea = null;
	FileReader lee = null;
	try
	{
	    lee = new FileReader(filename);
	    BufferedReader entrada= new BufferedReader(lee);
	    linea = entrada.readLine();
	    while( (linea=entrada.readLine())!=null )
	    {
		if( !"".equals(linea.trim()) )
		    j++;
	    }//fin while
	    entrada.close();
	}catch(Exception e)
	    {
		return 0;
	    }
	 finally{ try{ lee.close(); }catch(Exception e){  } }
	return j;
    }

    /**
     * Gets the numberlines.
     *
     * @param filename the filename
     * @param separador the separador
     * @return the numberlines
     */
    public int getnumberlines(String filename, String separador)
    {
	int j = 0;
	String linea = null;
	FileReader lee = null;
	try
	{
	    lee = new FileReader(filename);
	    BufferedReader entrada= new BufferedReader(lee);
	    linea = entrada.readLine();
	    while( (linea=entrada.readLine())!=null )
	    {
		if( !"".equals(linea.trim()) && linea.indexOf(separador)>0)
		    j++;
	    }//fin while
	    entrada.close();
	}catch(Exception e)
	    {

		EIGlobal.mensajePorTrace(">> getnumberlines --->  Excepcion  -->" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		return 0;
	    }
	 finally{ try{ lee.close(); }catch(Exception e){  } }
	return j;
    }

    /**
     * Desentrama tokens.
     *
     * @param linea the linea
     * @param tokens the tokens
     * @param separador the separador
     * @return the string[]
     */
    private String[] desentramaTokens(String linea, int tokens, String separador)
    {
	String [] salida = new String [tokens];
	int indice = linea.indexOf(separador); // String el pipe, o ;
	for(int i=0; i<tokens;i++)
	{
	    if( indice>0 )
		salida [i] = linea.substring(0,indice);
	    if( salida[i]==null )
		salida [i] = "	  ";
	    linea = linea.substring(indice+1);
	    indice = linea.indexOf(separador);
	}
	return salida;
    }

    /**
     * Filters_to_file.
     *
     * @param tipoBanca the tipo banca
     * @param filein the filein
     * @param fileout the fileout
     * @param numlineas the numlineas
     * @param clv_mov the clv_mov
     * @param p_importe the p_importe
     * @param p_referencia the p_referencia
     * @param separador the separador
     * @param importede the importede
     * @param importea the importea
     * @return the int
     */
    public int Filters_to_file(String tipoBanca, String filein, String fileout, int numlineas,
							   String clv_mov, String p_importe, String p_referencia, String separador, String importede, String importea)
    {
		EIGlobal.mensajePorTrace("Entro a Filters_to_file() ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(" JGAL tipoBanca: [" + tipoBanca + "], filein: [" + filein + "], fileout: [" + fileout +
				"], numlineas: [" + numlineas + "], clv_mov: [" + clv_mov + "], p_importe: [" + p_importe + "], p_referencia: [" +
				p_referencia + "], separador: [" + separador + "]", EIGlobal.NivelLog.INFO);

		if (p_importe==null||"".equals(p_importe)) {
			p_importe="0";
		}
		p_importe = p_importe.trim();

		importede = importede.trim();
		importea = importea.trim();

		if(p_importe.length()>0 && p_importe.indexOf(".")==-1){
			p_importe += ".00";
		}else if(importede.length() > 0 && importede.indexOf(".")==-1){
			importede += ".00";
		}else if(importea.length() > 0 && importea.indexOf(".")==-1){
			importea += ".00";
		}



		//***** everis Inicializando BufferedReader  08/05/2008  ..inicio
		//BufferedReader in;
		//BufferedWriter out;
		BufferedReader in = null;
		BufferedWriter out = null;
		//***** everis Inicializando BufferedReader  08/05/2008  ..fin

		StringTokenizer t ;
		float mi_importe = 0;
		float mi_importe1 = 0;
		long mi_referencia;
		long mi_referencia1;
		int i = 0;
		int k = 0;
		String salida[] = new String[11] ;
		String linea = "";
		String c_clave_movimiento="", c_importe="", c_referencia="";
		FileReader lee = null;
		FileWriter escribe = null;
		//VARAIBLES PARA FILTRAR RANGOS
		float importeDe = 0;
		float importeA = 0;

		mi_importe = obtenerFloat(p_importe);

		try {
			lee     = new FileReader(filein);
			escribe = new FileWriter(fileout);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("*movimientos Error Ocurrio una Excepcion " + e.toString(), EIGlobal.NivelLog.INFO);

		}

		try {
			in = new BufferedReader(lee);
			out = new BufferedWriter(escribe);
			linea = in.readLine();
			linea = linea + "\r\n";
			out.write(linea,0,linea.length());
			boolean escribir = true;
			for(i = 0; i < numlineas; i++)
			{
				escribir = true;
				linea = in.readLine();
				if("".equals(linea.trim()))
					linea = in.readLine();
				if( linea == null )
					break;
				t = new StringTokenizer(linea,separador);

				if("E".equals(tipoBanca))
				{
					int posIni = 0;
					int posFin = 0;
					posFin = linea.indexOf(separador);

					for(k=0; k<10 ;k++)
					{
						salida[k] = linea.substring(posIni,posFin);
						posIni = posFin+1;
						posFin = linea.indexOf(separador,posFin+1);
					}
					salida[k] = linea.substring(posIni);
				} else {
					for(k=0; k<10 ;k++)
					try {
						salida[k] = t.nextToken();
					} catch(Exception e) {
						salida[k] = " ";
					}
				}
				if("E".equals(tipoBanca))
				{

					c_clave_movimiento=salida[10].trim().charAt(0)+"";
					if(!c_clave_movimiento.equals("-"))
						c_clave_movimiento="+";

					c_importe=salida[10].substring(1);

					if( c_importe.trim().length()==0 )
						c_importe="-1";

					c_referencia=salida[1];
					if( c_referencia.trim().length()==0 )
						c_referencia="-1";

				} else {
					c_clave_movimiento=salida[0];
					c_importe=salida[4];
					c_referencia=salida[3];
				}

				if(clv_mov.trim().length()>0){

					if(c_clave_movimiento.trim().equals(clv_mov.trim())){
						EIGlobal.mensajePorTrace("movimientos --> VACIO   ", EIGlobal.NivelLog.INFO);
					}else{
						escribir = false;
					}
				}

				if(p_referencia.trim().length()>0)
				{
					try {
						mi_referencia1 = (long)new Long(p_referencia.trim()).longValue();
						mi_referencia  = (long)new Long(c_referencia.trim()).longValue();

						EIGlobal.mensajePorTrace("movimientos --> FILTRAR POR REFERENCIA   ARCHIVO <"+mi_referencia+"> PARAMETRO <"+mi_referencia1+">", EIGlobal.NivelLog.INFO);

						if(mi_referencia == mi_referencia1){
							EIGlobal.mensajePorTrace("movimientos --> REF VACIA  ", EIGlobal.NivelLog.INFO);
						}else{
							escribir = false;
						}
					} catch(NumberFormatException e) {
						EIGlobal.mensajePorTrace("**movimientos Error Ocurrio una Excepcion " + e.toString(), EIGlobal.NivelLog.INFO);
						e.printStackTrace();
						escribir = false;
					}
				}


				if( p_importe.length() > 0 ||(importede.length()> 0 || importea.length() > 0))
				{

					try {

						//PARAMETROS DEL USUARIO
						mi_importe = obtenerFloat(p_importe);
						importeDe = obtenerFloat(importede);
						importeA = obtenerFloat(importea);

						EIGlobal.mensajePorTrace("movimientos v1116 --> IMPORTES T. BANCA <"+tipoBanca+"> IMPORTE ARCHIVO <"+mi_importe1+">  MIN <"+importeDe+"> MAX <"+importeA+">", EIGlobal.NivelLog.INFO);

						if("E".equals(tipoBanca) ){
							mi_importe1 = (float)new Float(c_importe.trim()).floatValue();
							EIGlobal.mensajePorTrace("movimientos --> FILTRO IMPORTES B. ESPECIALIZADA IMPORTE ARCH <"+mi_importe1+"> IMPORTE PARAM <"+mi_importe+">", EIGlobal.NivelLog.INFO);
							if(mi_importe == 0){
								EIGlobal.mensajePorTrace("movimientos --> IMPORTE CERO  ", EIGlobal.NivelLog.INFO);
							}else{
								if( mi_importe == mi_importe1 ){
									EIGlobal.mensajePorTrace("movimientos --> LINEA VACIO  ", EIGlobal.NivelLog.INFO);
								}else{
									escribir = false;
								}
							}
						}

						if (("C".equals(tipoBanca) || "N".equals(tipoBanca)) && importeA >= 0 && importeDe  >= 0){
							mi_importe1 = (float)new Float(c_importe.trim()).floatValue()/100;
							if(importeA == 0.0 && importeDe == 0.0){
								EIGlobal.mensajePorTrace("movimientos --> Falso ESCRIBE LINEA  ", EIGlobal.NivelLog.INFO);
							}else{
								if(Float.compare(mi_importe1, importeDe) >= 0 &&  Float.compare(mi_importe1, importeA) <= 0  ){
									EIGlobal.mensajePorTrace("movimientos --> LINEA VACIO  ", EIGlobal.NivelLog.INFO);
								}else{
									escribir = false;
								}
							}
						}

					} catch(NumberFormatException e) {
						EIGlobal.mensajePorTrace("****movimientos Error Ocurrio una Excepcion " + e.toString(), EIGlobal.NivelLog.INFO);
						escribir = false;

					}
				}

				if(escribir){
					linea = linea + "\r\n";
					out.write(linea,0,linea.length());
					out.flush();
				}
			}//fin for

			in.close();
			out.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("****movimientos Error Ocurrio una Excepcion " + e.toString(), EIGlobal.NivelLog.INFO);

			return 1;
		} finally {
			try {
				lee.close();
				escribe.close();
			} catch(Exception e){ }


			//***** everis Cerrando BufferedReader dentro de finally 08/05/2008  ..inicio
			if(null != in){
				try{
					in.close();
				}
				catch(Exception e){
					in = null;
				}
			}

			if(null != out){
				try{
					out.close();
				}
				catch(Exception e){
					out = null;
				}
			}

		}
		return 0;
    }

    /**
     * Trae saldos chequera.
     *
     * @param tipocuenta the tipocuenta
     * @param tipoBanca the tipo banca
     * @param cuentamov the cuentamov
     * @param req the req
     * @return the string
     */
    String TraeSaldosChequera (String tipocuenta, String tipoBanca, String cuentamov, HttpServletRequest req)
    {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESION_PARAM);
		StringBuffer tramaresult= new StringBuffer();
		String tramaentrada	= "";
		double totalDisponible	= 0;
		double totalSBC 	= 0;
		double totalSaldos	= 0;
		String coderror 	= "";
		String htResult 	= "";

		if( tipoBanca==null )
			tipoBanca="";
		try
	{
	    tramaresult.append("<table width=228 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
	    ServicioTux tuxGlobal = new ServicioTux();
	    //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	    tramaentrada="1EWEB|"+session.getUserID8()+"|SDCT|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+cuentamov+"|"+tipocuenta+"|";
	    try
	    {
		Hashtable hs = tuxGlobal.web_red(tramaentrada);
		coderror = (String) hs.get("BUFFER");
	    }catch(Exception re){  }
	    if("OK".equals(coderror.substring(0,2)))
	    {
		tramaresult.append("<tr><td class=tittabdat align=right width=110>Saldo disponible:</td><td class=textabdatobs align=right width=100 nowrap>").append(FormatoMoneda(coderror.substring(16,30))).append("</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Salvo buen cobro:</td><td class=textabdatobs align=right nowrap>").append(FormatoMoneda(coderror.substring(30,44))).append("</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Saldo total:</td><td class=textabdatobs align=right nowrap>").append(FormatoMoneda(coderror.substring(44,58))).append("</td></tr>");
	    }
	    else
	    {
		tramaresult.append("<tr><td class=tittabdat align=right width=110>Saldo disponible:</td><td class=textabdatobs align=right width=100 nowrap>0.0</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Salvo buen cobro:</td><td class=textabdatobs align=right nowrap>0.0</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Saldo total:</td><td class=textabdatobs align=right nowrap>0.0</td></tr>");
	    }
	    tramaresult.append("</table>");
	}catch(Exception e){ tramaresult.append(""); }
	return tramaresult.toString();
    }

    /**
     * Trae saldos especiales.
     *
     * @param tipocuenta the tipocuenta
     * @param cuentamov the cuentamov
     * @param req the req
     * @return the string
     */
    String TraeSaldosEspeciales(String tipocuenta,String cuentamov, HttpServletRequest req)
    {
	EIGlobal.mensajePorTrace("-!?movimientos[TraeSaldosEspeciales]", EIGlobal.NivelLog.INFO);
	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute(SESION_PARAM);
	StringBuffer tramaresult=new StringBuffer();
	String contenidoArch	= "";
	String sh		= "";
	String sH		= "";
	String sHoy		= "";
	String posicion 	= "";
	String s24		= "";
	double saldo24		= 0.0;
	double saldoHoy 	= 0.0;
	String sal24		= "";
	double total=0,cantidad=0,precio=0;
	String t;
	BufferedReader archivoRespuesta = null;

	String tramaEnvio="2EWEB|"+session.getUserID8()+"|POSI|"+session.getContractNumber()+"|"+session.getUserID8()+"|" +session.getUserProfile() + "|" + cuentamov + "|" + tipocuenta;
	ServicioTux tuxGlobal = new ServicioTux();
	//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	String tramaRegresoServicio = "";
	try
	{
	    Hashtable hs = tuxGlobal.web_red(tramaEnvio);
	    tramaRegresoServicio = (String) hs.get("BUFFER");
	}catch(Exception e){  }
	String nombreArchivo=Global.DIRECTORIO_LOCAL+tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/'));
	ArchivoRemoto archRem= new ArchivoRemoto();
	archRem.copia(tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/')+1));
	FileReader lee = null;
	try{ lee = new FileReader(nombreArchivo); }catch(Exception e){	}
	try
	{
	    //Modificacion

	    //***** everis Inicializando BufferedReader   09/05/2008  ..inicio
	    //BufferedReader archivoRespuesta = new BufferedReader(lee);	//Mod
	    archivoRespuesta = new BufferedReader(lee);	//Mod
	    //***** everis Inicializando BufferedReader  09/05/2008  ..fin


	    //RandomAccessFile archivoRespuesta= new RandomAccessFile(nombreArchivo,"rw");
	    contenidoArch=lecturaArchivoSalida(archivoRespuesta);
	    tramaresult.append("<table width=228 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
	    //se dimensionaran las variables en el caso de que al servicio no conteste
	    if( !contenidoArch.startsWith("OK") )
	    {
		tramaresult.append("<tr><td class=tittabdat align=right width=110>Saldo hoy:</td><td class=textabdatobs align=right width=100 nowrap>0.0</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Saldo 24 Hrs:</td><td class=textabdatobs align=right nowrap>0.0</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Total Cartera:</td><td class=textabdatobs align=right nowrap>0.0;</td></tr>");
	    }
	    else
	    {
		posicion=contenidoArch.substring(posCar(contenidoArch,';',2),contenidoArch.length());
		sH=contenidoArch.substring(posCadena(contenidoArch,5), posCadena(contenidoArch,6));
		sH=sH.trim();
		saldoHoy=Double.valueOf(sH).doubleValue();
		s24=contenidoArch.substring(posCadena(contenidoArch,6),posCar(contenidoArch,';',1));
		saldo24=Double.valueOf(s24).doubleValue();
		for(int z=0;z<posicion.length();z+=4)
		{
		    try
		    {
			cantidad=Double.valueOf(posicion.substring(posCar(posicion,';',z+2)+1,posCar(posicion,';',z+3))).doubleValue();
			try
			{
			    precio=Double.valueOf(posicion.substring(posCar(posicion,';',z+4)+1,posCar(posicion,';',z+5))).doubleValue();
			}catch(StringIndexOutOfBoundsException e1)
			    {
				precio=Double.valueOf(posicion.substring(posCar(posicion,';',z+4)+1,posicion.length())).doubleValue();
				z=posicion.length();
			    }
			 catch(Exception e2)
			    {
				precio=Double.valueOf(posicion.substring(posCar(posicion,';',z+4)+1,posCar(posicion,';',z+5)-1)).doubleValue();
			    }
			total=total+(cantidad*precio);
		    }catch(Exception e)
			{
			    EIGlobal.mensajePorTrace("movimientos cachando Excepcion: "+ e.toString(), EIGlobal.NivelLog.INFO);
			    break;
			}
		}//fin de for
		total=saldoHoy+total;
		sHoy=FormatoMoneda(saldoHoy);
		sal24=FormatoMoneda(saldo24);
		t=FormatoMoneda(total);
		tramaresult.append("<tr><td class=tittabdat align=right width=110>Saldo hoy:</td><td class=textabdatobs align=right width=100 nowrap>").append(sHoy).append("</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Saldo 24 Hrs:</td><td class=textabdatobs align=right nowrap>").append(sal24).append("</td></tr>");
		tramaresult.append("<tr><td class=tittabdat align=right>Total Cartera:</td><td class=textabdatobs align=right nowrap>").append(t).append("</td></tr>");
	    }//fin else
	    tramaresult.append("</table>");
	}catch(Exception e)
	    {
		tramaresult.append("");
	    }
	    finally
	    {
	    	if(null != archivoRespuesta){
				try{
					archivoRespuesta.close();
				}
				catch(Exception e){
					archivoRespuesta = null;
				}
			}

			try{ lee.close();
		 	}
		 	catch(Exception e){  }
	    }
	return tramaresult.toString();
    }

    /**
     * Lectura archivo salida.
     *
     * @param archivoSalida the archivo salida
     * @return the string
     */
    public String lecturaArchivoSalida(BufferedReader archivoSalida)
    {
	String archivoLeido	= "";
	try
	{
	    while( archivoSalida.readLine()!=null )
	    {
		archivoLeido = archivoSalida.readLine();
	    }
	}catch(IOException e)
	    {
		EIGlobal.mensajePorTrace("Ha ocurrido un error al leer el archivo "+ e.toString(), EIGlobal.NivelLog.INFO);
	    }
	return archivoLeido.toString();
    }

    /**
     * Pos cadena.
     *
     * @param trama the trama
     * @param numCadena the num cadena
     * @return the int
     */
    public int posCadena(String trama, int numCadena)
    {
	String caracter    = "";
	String caracterAnt = "";
	int a = 1;
	int i = 0;
	for(i=1; i<trama.length() && a<numCadena ;i++)
	{
	    caracterAnt=String.valueOf(trama.charAt(i-1));
	    caracter=String.valueOf(trama.charAt(i));

	    if (!" ".equals(caracter) &&  " ".equals(caracterAnt)) a++;
	}
	return i-1;
    }

    /**
     * Pos car.
     *
     * @param cad the cad
     * @param car the car
     * @param cant the cant
     * @return the int
     */
    public int posCar(String cad,char car,int cant)
    {
	int result=0,pos=0;
	for(int i=0; i<cant ;i++)
	{
	    result=cad.indexOf(car,pos);
	    pos=result+1;
	}
	return result;
    }



    /**
     * Pampa_consulta_detalle_movimientos_comercio.
     *
     * @param req the req
     * @param res the res
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    int  pampa_consulta_detalle_movimientos_comercio
	(
	    HttpServletRequest req,
	    HttpServletResponse res
	)
	throws IOException, ServletException
    {
	BaseResource session = (BaseResource)req.getSession().getAttribute(SESION_PARAM);
	String tramacuentamov = (String )req.getParameter(ENLACE_CUENTA_PARAM);
	String pampa_fecha    = (String )req.getParameter("pampa_fecha");
	String pampa_importe  = (String )req.getParameter("pampa_importe");
	String pampa_referencia_390=  (String )req.getParameter("pampa_referencia_390");
	String sprimer_llamada =  (String )req.getParameter("pampa_llamada");
	boolean primera_llamada = true;
	String cuenta = "";
	String tipo_cuenta ="";
	if (tramacuentamov.indexOf("@")==-1)
	{
	    cuenta    = tramacuentamov.substring(tramacuentamov.indexOf("|")+1,tramacuentamov.lastIndexOf("|"));
	    tipo_cuenta   = "P";
	    tramacuentamov =  cuenta+"@"+tipo_cuenta+"@@";
	}
	else
	{
	    cuenta = tramacuentamov.substring(0,tramacuentamov.indexOf("@"));
	    tipo_cuenta=tramacuentamov.substring(tramacuentamov.indexOf("@")+1,tramacuentamov.length());
	    tipo_cuenta=tipo_cuenta.substring(0,tipo_cuenta.indexOf("@"));
	}
	int total_registros=0;
	int inicio=0;
	int fin=0;
	boolean llamada_exitosa=true;
	if( sprimer_llamada!=null )
	    primera_llamada=false;
	if( primera_llamada==true )
	{
	    llamada_exitosa=pampa_llamada_PMDM(cuenta,tipo_cuenta,pampa_fecha,pampa_referencia_390,pampa_importe,req,res);
	    if( !llamada_exitosa )
		despliegaPaginaError("No Existe Detalle de Movimientos por Comercio","Detalle de Movimientos por Comercio","Consultas &gt; Movimientos &gt; Chequeras" ,"s25PMDMh", req, res);
	    total_registros=getnumberlines(IEnlace.LOCAL_TMP_DIR +"/"+session.getUserID8());
	    if( total_registros==0 )
		despliegaPaginaError("No Existe Detalle de Movimientos por Comercio","Detalle de Movimientos por Comercio","Consultas &gt; Movimientos &gt; Chequeras" ,"s25PMDMh", req, res);
	    pampa_exporta_detalle_movimientos(req,res);
	    inicio=0;
	    if( total_registros>=Global.NUM_REGISTROS_PAGINA )
		fin=Global.NUM_REGISTROS_PAGINA;
	    else
		fin=total_registros;
	}
	else
	{
	    inicio  = Integer.parseInt(req.getParameter("pampa_inicio"));
	    fin = Integer.parseInt(req.getParameter("pampa_fin"));
	    total_registros=Integer.parseInt(req.getParameter("pampa_total_registros"));
	}
	if( total_registros>0 )
	    pampa_mostrar_detalle_movimientos(inicio,fin,total_registros,tramacuentamov,req,res);
	return 0;
     }

    /**
     * Pampa_llamada_ pmdm.
     *
     * @param cuenta the cuenta
     * @param tipocuenta the tipocuenta
     * @param pampa_fecha the pampa_fecha
     * @param pampa_referencia_390 the pampa_referencia_390
     * @param pampa_importe the pampa_importe
     * @param req the req
     * @param res the res
     * @return true, if successful
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    boolean pampa_llamada_PMDM
	(
	    String cuenta,
	    String tipocuenta,
	    String pampa_fecha,
	    String pampa_referencia_390,
	    String pampa_importe,
	    HttpServletRequest req,
	    HttpServletResponse res
	 )
	 throws IOException, ServletException
     {
	BaseResource session = (BaseResource)req.getSession().getAttribute(SESION_PARAM);
	String	tramaentrada = "2EWEB|" + session.getUserID8() + "|PMDM|" + session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+cuenta+"|"+tipocuenta+"|"+pampa_fecha+"|"+pampa_referencia_390+"|"+pampa_importe+"|";
	boolean salida=true;
	String coderror="";
	try
	{
	    ServicioTux tuxGlobal = new ServicioTux();
	    //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	    Hashtable hs = tuxGlobal.web_red(tramaentrada);
	    coderror = (String) hs.get("BUFFER");
	}catch(Exception re){  }
	if(
	    (coderror.equals(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8()) ) ||
	    (coderror.equals(IEnlace.REMOTO_TMP_DIR + "/" + session.getUserID8()))
	  )
	{
	    // Copia Remota
	    ArchivoRemoto archivo_remoto = new ArchivoRemoto();
	    if( !archivo_remoto.copia( session.getUserID8()) )
	    {
		salida = false; // error al copiar
	    }
	    else
	    {
		salida=true; // OK
	    }
	}
	else
	    salida=false;
	return salida;
    }

    /**
     * Pampa_mostrar_detalle_movimientos.
     *
     * @param inicio the inicio
     * @param fin the fin
     * @param total_registros the total_registros
     * @param tramacuenta the tramacuenta
     * @param req the req
     * @param res the res
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    int pampa_mostrar_detalle_movimientos
	(
	    int inicio,
	    int fin,
	    int total_registros,
	    String tramacuenta,
	    HttpServletRequest req,
	    HttpServletResponse res
	)
	throws IOException, ServletException
    {
	BaseResource session = (BaseResource)req.getSession().getAttribute(SESION_PARAM);
	BufferedReader entrada;
	int cont_gen=0;
	int cont_pantalla=0;
	String arr[] = null;
	String linea="";
	StringBuffer tabla= new StringBuffer();
	StringBuffer tablainicial= new StringBuffer();
	String clase;
	StringBuffer tablabotones = new StringBuffer();
	String archivo="";
	String []arrcta = desentramaC("3@"+tramacuenta,'@');
	String cuenta  =  arrcta[1];
	String nombrecuenta = arrcta[3];
	tablainicial.append("<table width=\"750\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">\n");
	tablainicial.append("<tr><td valign=\"bottom\" align=\"left\" class=\"textabref\" width=\"375\">Cuenta:").append(cuenta).append("&nbsp;").append(nombrecuenta).append("<br></td></tr></Table>\n");
	tabla.append("<table width=\"750\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">\n");
	tabla.append("<tr>");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">Tipo Transacci&oacute;n</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">N&uacute;mero de Tarjeta</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">Importe</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"70\">Clave transacci&oacute;n</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">Terminal ID</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">Banco Emisor</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">Referencia Banco</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">Descripci&oacute;n</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"70\">Fecha proceso</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"70\">Hora Transacci&oacute;n</TD>\n");
	tabla.append("<td class=\"tittabdat\" align=\"center\" width=\"90\">IND. Visa/Master</TD></tr>\n");
	try
	{
	    entrada= new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() ));
	    //para simulacion desarrollo entrada= new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR + "/detalle.lm05"));
	    linea = entrada.readLine();
	    while((linea=entrada.readLine())!=null)
	    {
		if( cont_gen>=inicio && cont_gen<fin )
		{
		    arr = desentramaC("11;"+linea+";",';');
		    if( cont_pantalla%2!=0 ) clase="textabdatobs";
		    else		     clase="textabdatcla";
		    if( "".equals(arr[1].trim()) )
			tabla.append("<tr><td class=").append(clase).append("  align=center nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<tr><td class=").append(clase).append("  align=center nowrap >").append(arr[1]).append("</td>\n");
		    if(  "".equals(arr[2].trim()) )
			tabla.append("<td class=").append(clase).append("  align=center nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[2]).append("</td>\n");
		    if(!isSignedNumber(arr[3]))
			tabla.append("<td class=").append(clase).append("  align=right	nowrap >$ 0.00</td>\n");
		    else
		  if(arr[3].contains("-"))arr[3] = "-" + arr[3].replace("-", "");
			tabla.append("<td class=").append(clase).append("  align=right	nowrap >$ ").append(new java.text.DecimalFormat("###,###,###,###,##0.00").format(new Double(arr[3].trim())/100)).append("</td>\n");
		    if( "".equals(arr[4].trim()) )
			tabla.append("<td class=").append(clase).append("  align=center nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[4]).append("</td>\n");
		    if("".equals(arr[5].trim())  )
			tabla.append("<td class=").append(clase).append("  align=center nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[5]).append("</td>\n");
		    if("".equals(arr[6].trim()) )
			tabla.append("<td class=").append(clase).append("  align=center nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[6]).append("</td>\n");
		    if("".equals(arr[7].trim()) )
			tabla.append("<td class=").append(clase).append("  align=center nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[7]).append("</td>\n");
		    if("".equals(arr[8].trim()) )
			tabla.append("<td class=").append(clase).append("  align=lef nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=left	nowrap >").append(arr[8]).append("</td>\n");
		    if( "".equals(arr[9].trim()) )
			tabla.append("<td class=").append(clase).append("  align=lef nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[9]).append("</td>\n");
		    if( "".equals(arr[10].trim()) )
			tabla.append("<td class=").append(clase).append("  align=lef nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[10]).append("</td>\n");
		    if( "".equals(arr[11].trim()) )
			tabla.append("<td class=").append(clase).append("  align=lef nowrap >&nbsp;&nbsp;&nbsp;</td>\n");
		    else
			tabla.append("<td class=").append(clase).append("  align=center nowrap >").append(arr[11]).append("</td></tr>\n");
		    cont_pantalla++;
		}//fin if
		cont_gen++;
	    }//fin while
	    entrada.close();
	}catch(Exception e)
	    {
		EIGlobal.mensajePorTrace("***pampa_mostrar_detalle_movimientos :"+e.toString(), EIGlobal.NivelLog.ERROR);
	    }
	tabla.append("</TABLE>\n");
	tabla.append("<table border=0 cellspacing=0 cellpadding=0 ALIGN=CENTER	bgcolor=#FFFFFF>\n");
	tabla.append("<tr><td COLSPAN=9 ALIGN=CENTER class=\"textabref\" >Movimientos : ").append(new Integer(inicio+1).toString()).append(" - ").append(new Integer(fin).toString()).append(" de ").append(total_registros).append("</td></TR>\n");
	if( inicio>0 )
	    tabla.append("<tr><td  COLSPAN=9 ALIGN=CENTER class=\"textabref\" ><A HREF=\"javascript:atras();\">< Anteriores  ").append(Global.NUM_REGISTROS_PAGINA).append("</A></td></TR>\n");
	if( fin<total_registros )
	{
	    if( (fin+Global.NUM_REGISTROS_PAGINA)<=total_registros )
		tabla.append("<tr><td  COLSPAN=9 ALIGN=CENTER class=\"textabref\" ><A HREF=\"javascript:adelante();\">Siguientes ").append(new Integer(fin -inicio).toString()).append(" ></A></td></TR>\n");
	    else
		tabla.append("<tr><td	COLSPAN=9 ALIGN=CENTER class=\"textabref\" ><A HREF=\"javascript:adelante();\">Siguientes ").append(new Integer(total_registros - fin).toString()).append(" ></A></td></TR>\n");
	}
	tabla.append("</TABLE>\n");
	tabla.append("<input type = \"Hidden\" name = \"pampa_total_registros\" value = \"").append(total_registros).append("\">\n");
	tabla.append("<input type = \"Hidden\" name = \"pampa_inicio\" value = \"").append(inicio).append("\">\n");
	tabla.append("<input type = \"Hidden\" name = \"pampa_fin\" value = \"").append(fin).append("\">\n");
	tabla.append("<input type = \"hidden\" name = \"EnlaceCuenta\" value = \"").append(tramacuenta).append("@\">\n");
	tabla.append("<input type = \"hidden\" name = \"TMov\" value = PMDM >\n");
	tabla.append("<input type = \"hidden\" name = \"pampa_llamada\" value =paginaiguiente >\n");
	tabla.append("<input type = \"Hidden\" name = \"Banca\" value = \"C\">\n");
	tablabotones.append("<td align=center><a href=\"javascript:scrImpresion();\" border=0>");
	tablabotones.append("<img	border=\"0\" src=\"/gifs/EnlaceMig/gbo25240.gif\" ></a></td>");
	tablabotones.append("<td align=center><a href = \"/Download/").append(session.getUserID8()).append("pmdm.doc").append("\" border=0>");
	tablabotones.append( "<img	border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a></td>");
	req.setAttribute("Encabezado", CreaEncabezado("Detalle de Movimientos por Comercio","Consultas &gt; Movimientos &gt; Chequeras","s25PMDMh",req));
	req.setAttribute("MenuPrincipal", session.getStrMenu());
	req.setAttribute("newMenu", session.getFuncionesDeMenu());
	req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
	req.setAttribute("tablainicial",tablainicial.toString());
	req.setAttribute("tabla",tabla.toString());
	req.setAttribute("botones",tablabotones.toString());

	evalTemplate("/jsp/pampa_det_movs_comer.jsp", req, res);
	return 0;
    }

    /**
     * Pampa_exporta_detalle_movimientos.
     *
     * @param req the req
     * @param res the res
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    int pampa_exporta_detalle_movimientos(HttpServletRequest req, HttpServletResponse res)
	throws IOException, ServletException
    {
    final StringBuilder sb;
		String[] arr = null;

		BaseResource session = (BaseResource)req.getSession().getAttribute(SESION_PARAM);
		String archivo_lectura =  IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8();
		String archivo_salida =   IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() + "pmdm.doc";
		BufferedWriter salida;
		BufferedReader lectura;
		String linea;
		try
		{
			lectura = new BufferedReader(new FileReader(archivo_lectura));
			salida  = new BufferedWriter( new FileWriter( archivo_salida ) );
			linea = lectura.readLine();
			sb	= new StringBuilder();
			while( (linea=lectura.readLine())!=null )
			{
				arr	= linea.split(";");
				for (int i = 0; i < arr.length; i++) {
					if(i != 2){
						sb.append(arr[i]);
					}else{
						if(!isSignedNumber(arr[i])){
							sb.append(new java.text.DecimalFormat("###,###,###,###,##0.00").format(new Double(0)));
						}else{
							if(arr[i].contains("-"))arr[i] = "-" + arr[i].replace("-", "");
							sb.append(new java.text.DecimalFormat("###,###,###,###,##0.00").format(new Double(arr[i].trim())/100));
						}
					}
					sb.append(i != arr.length - 1 ? ";" : "");
				}

				salida.write(sb.toString() + "\r\n");
				sb.delete(0, sb.length());
				salida.flush();
			}
			lectura.close();
			salida.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace( "***Excepci?n en >>"+e.getMessage(),EIGlobal.NivelLog.ERROR );
			return 0;
	    }
		ArchivoRemoto archR = new ArchivoRemoto();

		if( !archR.copiaLocalARemoto(session.getUserID8() +"pmdm.doc","WEB") )
			EIGlobal.mensajePorTrace("***pampa_exporta_detalle_movimientos   No se creao archivo de exportacion:"+session.getUserID8() + "pmdm.doc", EIGlobal.NivelLog.INFO);
		return 1;
	}

    /**
     * PAMP a_get num comercio.
     *
     * @param cadena the cadena
     * @return the string
     */
    String PAMPA_getNumComercio(String cadena)
    {
		int TAMANO_COMERCIO = 9;
		String resultado    = "";

		try {
			if( cadena.length()>TAMANO_COMERCIO )
				resultado = cadena.substring( cadena.length()-TAMANO_COMERCIO );
			else
				resultado = cadena;

			int iRes = Integer.parseInt(resultado);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("Exception( function PAMPA_getNumComercio() ):["+ e.toString()+"]", EIGlobal.NivelLog.DEBUG);
			resultado = "";
		}
		return resultado;
	}


	/**
	 * Checks if is signed number.
	 *
	 * @param input the input
	 * @return true, if is signed number
	 */
	private boolean isSignedNumber(String input){
		final java.util.regex.Pattern pattern;
		java.util.regex.Matcher matcher;
		boolean flag = true;

		if("".equals(input) || "-".equals(input.trim()))return false;

		pattern	= java.util.regex.Pattern.compile("[0-9|-]");
		for (char c : input.toCharArray()) {
			matcher = pattern.matcher(c + "");
		    if(!matcher.find())flag = false;
		}
		return flag;
	}

	/**
	 * Exportar archivo.
	 *
	 * @param tipoArchivo El tipo de archivo a exportar
	 * @param nombreArchivo La ruta del archivo de donde se obtienen los datos
	 * @param tipoBanca El objeto: tipo banca
	 * @param res El request de la pagina
	 */
	private void exportarArchivo(String tipoArchivo, String nombreArchivo, String tipoBanca, HttpServletResponse res){
		EIGlobal.mensajePorTrace("movimientos.java -> ARCHIVO DE ENTRADA  -->"+nombreArchivo+"<--",EIGlobal.NivelLog.DEBUG);

		ExportModel em = new ExportModel("achivoExportacion", '|', true);

		if("E".equals(tipoBanca)){
			em
			   .addColumn(new Column(0,15, "Cuenta"))//16
			   .addColumn(new Column(16,29, "Folio"))//12
			   .addColumn(new Column(38,77, "Descripcion"))//40
			   .addColumn(new Column(78,83, "Fecha"))//6
			   .addColumn(new Column(84,84, "Cargo/Abono"))//6
			   .addColumn(new Column(85,119, "Importe"));
		}else if(tipoBanca != null){
			em
			   .addColumn(new Column(0,15, "Cuenta"))// 16
			   .addColumn(new Column(16,23, "Fecha"))// 8
			   .addColumn(new Column(24,27, "Hora"))// 4
			   .addColumn(new Column(28,31, "Suc"))// 4
			   .addColumn(new Column(32,71, "Descripcion")) // 40
			   .addColumn(new Column(72,72, "Cargo/Abono")) // 1
			   .addColumn(new Column(73,86, "Importe","$#########################0.00",2)) // 14
			   .addColumn(new Column(87,100, "Saldo","$#########################0.00",2)) // 14
			   .addColumn(new Column(101,108, "Referencia")) // 8
			   .addColumn(new Column(109,176, "Concepto / Referencia Interbancaria")); //
			   //.addColumn(new Column(159,176, "Referencia interbancaria")); // 18
		}

		   try{
			   EIGlobal.mensajePorTrace("movimientos.java -> Generando Exportacion --> ",EIGlobal.NivelLog.DEBUG);

			   HtmlTableExporter.export(tipoArchivo, em, nombreArchivo, res);

		   }catch(IOException ex){
			   EIGlobal.mensajePorTrace("movimientos.java -> ERROR --> Exportar Archivo -> " + ex.getMessage(),EIGlobal.NivelLog.ERROR);
		   }
	}

	/**
	 * Obtener float.
	 *
	 * @param str El objeto que contiene un numero
	 * @return Objeto float correspondiente al numero
	 */
	private float obtenerFloat(String str) {
		float numero = 0;
		try{
			if(str==null||str.isEmpty()){
				numero = 0;
			}else{
				str = str.replace(SIGNO_PESOS, VACIO);
				str = str.replace(COMA, VACIO);
				str = str.replace(SPACE, VACIO);
				numero = Float.parseFloat(str);
			}
		}catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("Error parsear numero " + str+": "+e.getMessage(), EIGlobal.NivelLog.ERROR);
			numero = 0;
		}
		return numero;
	}
}