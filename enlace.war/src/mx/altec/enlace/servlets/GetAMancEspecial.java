package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.http.*;
import javax.servlet.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


//VSWF


public class GetAMancEspecial extends BaseServlet
{
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EmailSender emailSender=new EmailSender();
		EmailDetails emailDetails = new EmailDetails();
		boolean sesionvalida = SesionValida( request, response );
		if ( sesionvalida )
		{

			//System.out.println(" GENERA FOLIO");
			/******************************************Inicia validacion OTP**************************************/
			String valida = request.getParameter ( "valida" );

			if ( validaPeticion( request, response,session,request.getSession (),valida))
			{
				  EIGlobal.mensajePorTrace("\n\n ENTR� A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.INFO);
			}
			/******************************************Termina validacion OTP**************************************/
			else
			{
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Generaci&oacute;n de Folios", "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Generaci&oacute;n de Folios", "s26020h", request));
		  /////////////////a
		EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n", EIGlobal.NivelLog.INFO);

		///////////////////--------------challenge--------------------------
		String validaChallenge = request.getAttribute("challngeExito") != null
			? request.getAttribute("challngeExito").toString() : "";

		EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

		if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
			EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
			ValidaOTP.guardaParametrosEnSession(request);

			validacionesRSA(request, response);
			return;
		}

		//Fin validacion rsa para challenge
		///////////////////-------------------------------------------------
		//interrumpe la transaccion para invocar la validaci�n
		boolean valBitacora = true;
		if(  valida==null)
		{
		boolean solVal=ValidaOTP.solicitaValidacion(
					session.getContractNumber(),IEnlace.MANCOMUNIDAD);

		if( session.getValidarToken() &&
			session.getFacultad(session.FAC_VAL_OTP) &&
			session.getToken().getStatus() == 1 &&
			solVal )
		{
			//VSWF-HGG -- Bandera para guardar el token en la bit�cora
			request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
			EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);
			ValidaOTP.guardaParametrosEnSession(request);
			//ValidaOTP.guardaParametrosEnSession(req);

			request.getSession().removeAttribute("mensajeSession");

			ValidaOTP.mensajeOTP(request, "ManGeneraFolios");

			ValidaOTP.validaOTP(request,response, IEnlace.VALIDA_OTP_CONFIRM);
		}
		else{
				ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
				valida="1";
				valBitacora = false;
			}
		 }

	//retoma el flujo
	if( valida!=null && valida.equals("1"))
	{
		String tipo_operacion = request.getParameter("operacion");
		String cuenta         = request.getParameter("cuenta");
		String importe        = request.getParameter("importe");
		String desc_oper      = request.getParameter("desc_operacion");
		String desc_cta       = request.getParameter("desc_cta");

		log( "GetAMancEspecial.java::defaultAction()::importe = >" +importe+ "<");
		//System.out.println("OPERACION = >" +desc_oper+ "<");
		//System.out.println("CUENTA = >" +desc_cta+ "<");

		int numeroReferencia = 0;
		String cad = "";
		String result_folio = "";
		short suc_opera = 787;

		request.setAttribute("operacion",desc_oper);
		request.setAttribute("cuenta", cuenta);
		request.setAttribute("importe", FormatoMoneda(importe));

		String contrato = session.getContractNumber();
		String usuario  = session.getUserID();
		usuario = convierteUsr8a7(usuario);
		numeroReferencia = obten_referencia_operacion( suc_opera );


		if (valBitacora) {
			try{
				double importeDouble = 0;
				importeDouble = Double.valueOf(importe).doubleValue();

				String claveOperacionGeneraFolio = BitaConstants.CTK_GENERACION_FOLIO_MANC;
				String conceptoGeneraFolio = BitaConstants.CTK_CONCEPTO_GENERACION_FOLIO_MANC;
				String claveOperacionConfirTkn = BitaConstants.GENERACION_FOLIO_MANC;
				String conceptoConfirTkn = BitaConstants.CONCEPTO_GENERACION_FOLIO_MANC;

				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------importe--b->"+ importe, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------claveOperacion Genera Folio--b->"+ claveOperacionGeneraFolio, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------claveOperacion confirma con token --b->"+ claveOperacionConfirTkn, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------concepto Genera Folio--b->"+ conceptoGeneraFolio, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------concepto confirma con token --b->"+ conceptoConfirTkn, EIGlobal.NivelLog.INFO);
				try{
					String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacionGeneraFolio,"0","0",conceptoGeneraFolio,0);
				} catch(Exception e) {
					EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

				}
				if (session.getValidarToken() && session.getToken().getStatus() == 1)
				{
					String token = "0";
					if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
					{token = request.getParameter("token");}
					int numeroRef = obten_referencia_operacion( suc_opera );
					String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroRef,cuenta,importeDouble,claveOperacionConfirTkn,"0",token,conceptoConfirTkn,0);
				}
			} catch(Exception e) {
			EIGlobal.mensajePorTrace("ENTRA PROBLEMA BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.WARN);

			}
		}
        //***********


			numeroReferencia = obten_referencia_operacion( suc_opera );
			result_folio = "" + numeroReferencia;

			String trama = "051000|" + numeroReferencia + "|" + contrato + "|ESMA|||" + usuario + "|" + cuenta + "|||" + tipo_operacion + "|||" + importe + "||||||||||||";
			EIGlobal.mensajePorTrace( "trama : >" +trama+ "<", EIGlobal.NivelLog.DEBUG );

			String [] resultManc = llamada_valida_manc( trama, 0 );
			String coderror = resultManc[0];

			boolean estatusRechazado = false;
			//modif PVA 05/08/2002
			if(coderror.equals("MANC0000"))
			{
				if  (!(resultManc[1].charAt( 0 ) == '0' )){
					result_folio = "El Contrato no esta Mancomunado";
					emailDetails.setEstatusFinal("Rechazada");
					estatusRechazado = true;
				}
			} else{
				result_folio = "El Contrato no esta Mancomunado";
				emailDetails.setEstatusFinal("Rechazada");
				estatusRechazado = true;
				}
			try{
				if (emailSender.enviaNotificacion(coderror)){
					emailDetails.setNumeroContrato(contrato);
					emailDetails.setRazonSocial(session.getNombreContrato());
					emailDetails.setNumCuentaCargo(cuenta);
					emailDetails.setImpTotal( "$ " + String.valueOf(importe) );
					emailDetails.setTipo_operacion(tipo_operacion);
					emailDetails.setNumRef(result_folio);
					emailDetails.setEstatusActual(coderror);
					if (!estatusRechazado){
						emailDetails.setEstatusFinal("Aceptada");
					}
					emailSender.sendNotificacion(request,IEnlace.MANC_GEN_FOLIO,emailDetails);
					EIGlobal.mensajePorTrace("SE ENVIARON NOTIFICACIONES :: GENERACION FOLIO-->" +result_folio ,EIGlobal.NivelLog.WARN);
				}
				}
				catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			//TODO BIT CU5021, Genera el Folio y verifica mancomunidad de contrato
			/*VSWF-HGG-I*/
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession(false));
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EA_MANCOM_GEN_FOLIOS_GENERA_FOLIO);
				if(coderror!=null){
					if(coderror.substring(0,2).equals("OK")){
						bt.setIdErr("ESMA0000");
					}else if(coderror.length()>8){
					bt.setIdErr(coderror.substring(0,8));
				}else{
					bt.setIdErr(coderror.trim());
					}
				}else{
					bt.setIdErr(" ");
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				try {
					bt.setReferencia(numeroReferencia);
				} catch (NumberFormatException e) {
					bt.setReferencia(0);
				}
				if (importe != null) {
				bt.setImporte(Double.parseDouble(importe));
				}
				if (cuenta != null) {
					if(cuenta.length() > 20){
						bt.setCctaOrig(cuenta.substring(0,20));
					}else{
						bt.setCctaOrig(cuenta.trim());
					}
				}
				bt.setServTransTux("ESMA");
				if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
					&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
							.equals(BitaConstants.VALIDA)) {
					bt.setIdToken(session.getToken().getSerialNumber());
				}
				try {
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			}
			/*VSWF-HGG-F*/

			//VSWF-HGG  -  borra bandera
			request.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);

			EIGlobal.mensajePorTrace( "coderror = >" +coderror+ "<", EIGlobal.NivelLog.DEBUG );
			EIGlobal.mensajePorTrace( "result = <"+resultManc[0]+"><"+resultManc[1]+">", EIGlobal.NivelLog.DEBUG );

			request.setAttribute( "folio", result_folio );
			request.setAttribute( "cadenaresult", cad );
			request.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
			evalTemplate( IEnlace.GETMANC_ESPECIAL_TMPL, request, response );
			}

			}
		} else {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}
	}
}