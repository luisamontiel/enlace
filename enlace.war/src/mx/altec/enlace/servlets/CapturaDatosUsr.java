/********************* CONTROL DE CAMBIOS ********************************
 **	No	 		Descripción					Responsable		Fecha
 **	1		Se realiza notificación por		  EVERIS		29/11/2007
 **			cambio de Correo electrónico
************************************************************************/

package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EmailCelularBean;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailCelularDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.VinculacionMonitorPlusUtils;



/**
 *  Clase encargada de iniciar el proceso de captura de datos en enlace
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Feb 08, 2007
 */
public class CapturaDatosUsr extends BaseServlet {

	/**
	 * Numero de versión serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Opción para la cargar
	 */
	public static final int ACCION_CARGAR = 0;

	/**
	 * Opción para la modificar
	 */
	public static final int ACCION_MODIFICA = 1;

	/**
	 * Opción para saber que lo que se modifica en la pantalla es del usuario
	 */
	public static final int MAN_USUARIO = 0;

	/**
	 * Opción para saber que lo que se modifica en la pantalla es del contrato
	 */
	public static final int MAN_CONTRATO = 1;

	/**
	 * Opción para saber que se refiere al nombre del usuario
	 */
	public static final int ID_NOMBRE_USUARIO = 0;

	/**
	 * Opción para saber que se refiere al apellido paterno del usuario
	 */
	public static final int ID_PATERNO_USUARIO = 1;

	/**
	 * Opción para saber que se refiere al apellido materno del usuario
	 */
	public static final int ID_MATERNO_USUARIO = 2;




	/**
	 * Metodo encargado de iniciar la actividad de captura cuando el form action
	 * es el metodo Get
	 *
	 * @param req					Petición del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 * @throws ServletException		Error de tipo ServletException
	 * @throws IOException			Error de tipo IOException
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		inicioServletCapturaDatosUsuario(req, res);
	}

	/**
	 * Metodo encargado de iniciar la actividad de captura cuando el form action
	 * es el metodo Post
	 *
	 * @param req					Petición del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 * @throws ServletException		Error de tipo ServletException
	 * @throws IOException			Error de tipo IOException
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		inicioServletCapturaDatosUsuario(req, res);
	}

	/**
	 * Metodo general donde iniciara la acción del servlet
	 *
	 * @param req					Petición del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 */
	private void inicioServletCapturaDatosUsuario(
			HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
		HashMap beanMap = new HashMap();
		Integer opcion = null;
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String MAIL_USUARIO_NOTIF="";
		String MAIL_CONTRATO_NOTIF = "";

		//Consys 13.08.2010
		String estatusMod[] = {"NA","NA"};


		boolean sesionvalida = SesionValida(req, res);

		//Consys 09.08.2010
        EmailCelularBean emailBean = new EmailCelularBean();
        EmailCelularBean celularBean = new EmailCelularBean();
        EmailCelularDAO emailCelularDao = new EmailCelularDAO();
		//Termina Consys 09.08.2010

		//Validando la session
        if(!sesionvalida){
        	return;
        }

        /******************************************Inicia validacion OTP**************************************/

        String valida = req.getParameter ("valida");
        if(validaPeticion( req,  res,session,sess,valida)){
        	EIGlobal.mensajePorTrace(" ENTRA A VALIDAR LA PETICION ", EIGlobal.NivelLog.INFO);

        }else{/************************************Termina validacion OTP*************************************/
    		EIGlobal.mensajePorTrace("CapturaDatosUsuario::inicioServletCapturaDatosUsuario:: Entrando al servlet.", EIGlobal.NivelLog.INFO);


			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><> CONSULTANDO CARRIERS [INICIO] <><><><><><><><><><><><><><><><><><><><><><><>", EIGlobal.NivelLog.DEBUG);

			HashMap carriers = session.getCarrierCompleto();

			req.setAttribute("carriers", carriers);


			//INICIA Consys 13.08.2010 Uso de Transaccion ODB5, ODB6, TCGG para obtener Datos

			emailBean.setCorreo(session.getMensajeUSR().getEmailUsuario());
			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><>VERIFICANDO EMAIL ------->>>> " + emailBean.getCorreo(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><>ESTATUS RECIBIDO DE LA TRAMA -----------> " + emailBean.getEstatus(), EIGlobal.NivelLog.DEBUG);
			req.setAttribute("emailUsuario", emailBean.getCorreo());

			//celularBean = emailCelularDao.consultaCelular(session.getUserID8());

			if( session.getMensajeUSR().getNoCelularUsuario() != null && session.getMensajeUSR().getNoCelularUsuario() != "" &&
				session.getMensajeUSR().getLadaUsuario() != null && session.getMensajeUSR().getLadaUsuario() != "" &&
				( session.getMensajeUSR().getNoCelularUsuario().length() + session.getMensajeUSR().getLadaUsuario().length() ) == 10 ){
				celularBean.setLada(session.getMensajeUSR().getLadaUsuario());
				celularBean.setNoCelular(session.getMensajeUSR().getNoCelularUsuario());

			}else{
				EIGlobal.mensajePorTrace("<><><><><>[ MANTENIMIENTO DE DATOS ] -> Valor Incorrecto en Celular: " + session.getMensajeUSR().getLadaUsuario() + "-" + session.getMensajeUSR().getNoCelularUsuario(), EIGlobal.NivelLog.DEBUG);
			}

			celularBean.setCarrier(session.getMensajeUSR().getCompCelularUsuario());

			req.setAttribute("celularUsr", celularBean.getNoCelular());
			req.setAttribute("ladaUsr", celularBean.getLada());
			req.setAttribute("carrierCel", celularBean.getCarrier());

    		beanMap = cargaParametros(req);
    		opcion = new Integer((String)beanMap.get("opcion"));





    		//Consys 09.08.2010 Se obtiene mail Actual
    		try{
				EIGlobal.mensajePorTrace("<><><><><>[MANTENIMIENTO DE DATOS]Se obtiene el e-mail del contrato<><><><><>", EIGlobal.NivelLog.INFO);
    			MAIL_CONTRATO_NOTIF = session.getMensajeUSR().getEmailContrato();

    			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><> CONSULTANDO DATOS USUARIO <><><><><><><><><><><><><><><><><><><><><><><>" , EIGlobal.NivelLog.DEBUG);

    			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><> emailCelularBean.getCorreo()    -> " + emailBean.getCorreo() , EIGlobal.NivelLog.DEBUG);

    			MAIL_USUARIO_NOTIF = emailBean.getCorreo();

    			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><> celularBean.getNoCelular() -> " + celularBean.getNoCelular() , EIGlobal.NivelLog.DEBUG);
    			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><> celularBean.getLada() -> " + celularBean.getLada() , EIGlobal.NivelLog.DEBUG);
    			EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><> celularBean.getCarrier() -> " + celularBean.getCarrier() , EIGlobal.NivelLog.DEBUG);

    		}catch(Exception e){
    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		}
    		Map<Integer, String> datoViejo = new HashMap<Integer, String>();
    		//Termina Consys 09.08.2010
    		/**
			 * Opción cargar
			 */
			if(opcion.intValue() == ACCION_CARGAR){
				try{
					EIGlobal.mensajePorTrace("CapturaDatosUsuario::inicioServletCapturaDatosUsuario:: Opción Cargar", EIGlobal.NivelLog.INFO);
					// Datos Usuario
					req.setAttribute("idUsuario", (String)beanMap.get("cveUsuario"));
					req.setAttribute("nombreUsuario", (String)beanMap.get("nombreUsuario"));
					req.setAttribute("apePaternoUsuario", (String)beanMap.get("apePaternoUsuario"));
					req.setAttribute("apeMaternoUsuario", (String)beanMap.get("apeMaternoUsuario"));
					//req.setAttribute("emailUsuario", consultaEmailUsuario((String)beanMap.get("cveUsuario")));

					//INICIA Consys 10.08.2010 Uso de Transaccion ODB5, ODB6, TCGG para obtener Datos
					req.setAttribute("emailUsuario", emailBean.getCorreo());

					req.setAttribute("celularUsr", celularBean.getNoCelular());
					req.setAttribute("ladaUsr", celularBean.getLada());
					req.setAttribute("carrierCel", celularBean.getCarrier());


					//TERMINA Consys 10.08.2010 Uso de Transaccion ODB5, ODB6, TCGG para obtener Datos

					//Consys 09.08.2010
					EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><> CAPTURA DATOS USUARIO <><><><><><><><><><><><><><><><><><><><><><><>" , EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><>idUsuario         ----------------------------> " + (String)beanMap.get("cveUsuario"), EIGlobal.NivelLog.DEBUG);

					//Datos Contrato
					req.setAttribute("cveContrato", (String)beanMap.get("cveContrato"));
					req.setAttribute("descContrato", (String)beanMap.get("descContrato"));
					//req.setAttribute("emailContrato", consultaEmailContrato((String)beanMap.get("cveContrato")));
					req.setAttribute("emailContrato", session.getMensajeUSR().getEmailContrato());

				}catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			}


			/**
			 * Opción modificar
			 */
			if(opcion.intValue() == ACCION_MODIFICA){

				//interrumpe la transaccion para invocar la validación
				if(  valida==null){
					EIGlobal.mensajePorTrace("Interrumpe el flujo", EIGlobal.NivelLog.INFO);
					boolean solVal=ValidaOTP.solicitaValidacion((String)beanMap.get("cveContrato"),IEnlace.CAPTURA_DATOS);

					if(session.getValidarToken() &&
	                                        session.getFacultad(session.FAC_VAL_OTP) &&
	                                        session.getToken().getStatus() == 1 &&
	                                        solVal)
					{
						ValidaOTP.guardaParametrosEnSession(req);
						ValidaOTP.validaOTP(req,res,IEnlace.VALIDA_OTP);
					} else{
						ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
						valida="1";
					}
				}
				
				//Mapa para mandar parametros a MP
				Map<Integer, String> campos = EnlaceMonitorPlusConstants.cargaValDefaultUsContrato(req,IEnlace.SUCURSAL_OPERANTE, session);
				
	            //retoma el flujo
	            if( valida!=null && "1".equals(valida)){
					EIGlobal.mensajePorTrace("Retoma flujo CapturaDatosUsuario::inicioServletCapturaDatosUsuario:: Opcion Actualizar", EIGlobal.NivelLog.INFO);
					try{
						EmailDetails emailDet = new EmailDetails();
						//System.out.println("%%%%% Este es el valor  cveContrato en CapturaDatos = " + (String)beanMap.get("cveContrato"));
						//System.out.println("%%%%% Este es el valor  descContrato en CapturaDatos = " + (String)beanMap.get("descContrato"));
						emailDet.setNumeroContrato((String)beanMap.get("cveContrato"));
						emailDet.setDescContrato((String)beanMap.get("descContrato"));
						if(new Integer((String)beanMap.get("tipoMod")).intValue() == MAN_CONTRATO){
							if(session.getMensajeUSR().getEmailContrato() != null && session.getMensajeUSR().getEmailContrato() != ""){
								//Se obtienen los datos para Monitor Plus
								datoViejo.put(4 ,new StringBuilder("Mail: ")
										.append(MAIL_CONTRATO_NOTIF).toString()); 
								boolean statusUpCtr = modificaCtr(beanMap);
								indicaCtrUpd(beanMap);
								session.setCampaniaCtoActiva(false);
								EIGlobal.mensajePorTrace("<><><><><>[MANTENIMIENTO DE DATOS] Se actualiza correo de contrato? " + statusUpCtr, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("<><><><><>[MANTENIMIENTO DE DATOS] Nuevo correo: " + beanMap.get("emailContrato"), EIGlobal.NivelLog.INFO);
								if(statusUpCtr){
									session.getMensajeUSR().setEmailContrato((String)beanMap.get("emailContrato"));
								}

								/*** Everis - 29/11/2007 ***/
	 							/*** se obtiene el email de contrato actual para realizar notificaciones por cambio de datos. ***/
	 							EIGlobal.mensajePorTrace(" ** CapturaDatosUsr - inicioServlet - Enviando notificacion por el cambio del correo en datos del contrato", EIGlobal.NivelLog.INFO);
								emailDet.setMailNuevo((String)beanMap.get("emailContrato"));
								emailDet.setMailViejo(MAIL_CONTRATO_NOTIF);
                                                              	// Se agrega valor Nuevo emailContrato para MP
								campos.put(3601, new StringBuilder("Mail: ").
                                                                        append(emailDet.getMailNuevo()).toString());
                                                               
								//emailDet.setMailViejo(req.getParameter ("mailViejoContrato"));
								emailDet.setTipoDato("Cuenta de Contrato");
								boolean NOTIF_CAMBIO_MAIL = notifMailCuentaContrato( req, MAIL_USUARIO_NOTIF , MAIL_CONTRATO_NOTIF, emailDet);
								EIGlobal.mensajePorTrace(" ** CapturaDatosUsr - inicioServlet - Se envió la notificación??? " + NOTIF_CAMBIO_MAIL, EIGlobal.NivelLog.INFO);
								MAIL_CONTRATO_NOTIF = session.getMensajeUSR().getEmailContrato();
	 							/*** Everis - 29/11/2007 - FIN Contrato ***/

							}

						}else if(new Integer((String)beanMap.get("tipoMod")).intValue() == MAN_USUARIO){
								EmailCelularBean emailCelBean = new EmailCelularBean();

								EIGlobal.mensajePorTrace("<><><><><>Datos Pantalla ", EIGlobal.NivelLog.INFO);

								EIGlobal.mensajePorTrace("Email ->>>>>> "+req.getParameter("emailUsuario"), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("Lada ->>>>>> "+req.getParameter("ladaUsuario"), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("Cel ->>>>>> "+req.getParameter("celUsuario"), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("Carrier ->>>>>> "+req.getParameter("carrier"), EIGlobal.NivelLog.INFO);

								emailCelBean.setCarrier(req.getParameter("carrier"));
								emailCelBean.setCorreo(req.getParameter("emailUsuario"));
								emailCelBean.setLada(req.getParameter("ladaUsuario"));
								emailCelBean.setNoCelular(req.getParameter("celUsuario"));
								
								//se concatena celular de usuario para MP
								StringBuilder celUserNew = new StringBuilder();

								if(session.getStatusCorreo().equals("M")){
									EIGlobal.mensajePorTrace("<><><><>MANTENIMIENTO DE DATOS PERSONALES [Se realiza Modificacion de correo] session.getStatusCorreo() --> " + session.getStatusCorreo(), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Actualizando Correo <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
									estatusMod[0] = emailCelularDao.actualizaCorreo(emailCelBean, session.getUserID8(), session.getStatusCorreo());
									EIGlobal.mensajePorTrace("<><><><><><>Estatus Actualizacion Correo ----> " + estatusMod[0], EIGlobal.NivelLog.INFO);
									session.setCampaniaActiva(false);
									if (estatusMod[0] != null && estatusMod[0].contains("exitosa")) {
										celUserNew.append("Mail:  ").append(emailCelBean.getCorreo()).append(' ');
									}
								}
								if("A".equals(session.getStatusCorreo())){
									EIGlobal.mensajePorTrace("<><><><>MANTENIMIENTO DE DATOS PERSONALES [Se realiza Alta de Registro] session.getStatusCorreo() --> " + session.getStatusCorreo(), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Alta para Correo <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
									estatusMod[0] = emailCelularDao.actualizaCorreo(emailCelBean, session.getUserID8(), session.getStatusCorreo());
									EIGlobal.mensajePorTrace("<><><><><><>Estatus Alta Correo ----> " + estatusMod[0], EIGlobal.NivelLog.INFO);
									session.setCampaniaActiva(false);
									if (estatusMod[0] != null && estatusMod[0].contains("exitosa")) {
										celUserNew.append("Mail:  ").append(emailCelBean.getCorreo()).append(' ');
									}
								}
								if("M".equals(session.getStatusCelular())){
									EIGlobal.mensajePorTrace("<><><><>MANTENIMIENTO DE DATOS PERSONALES [Se realiza Modificacion de celular] session.getStatusCelular() --> " + session.getStatusCelular(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Actualizando Celular <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
									estatusMod[1] = emailCelularDao.actualizaCelular(emailCelBean, session.getUserID8(),session.getStatusCelular());
									EIGlobal.mensajePorTrace("<><><><><><>Estatus Actualizacion Celular ----> " + estatusMod[1], EIGlobal.NivelLog.INFO);
									session.setCampaniaActiva(false);
									if (estatusMod[1] != null && estatusMod[1].contains("exitosa")) {
										celUserNew.append("Celular:  ").
											append(emailCelBean.getLada()).
											append(emailCelBean.getNoCelular());
									}
								}
								if("A".equals(session.getStatusCelular())){
									EIGlobal.mensajePorTrace("<><><><>MANTENIMIENTO DE DATOS PERSONALES [Se realiza Alta de Registro de celular] session.getStatusCelular() --> " + session.getStatusCelular(), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Alta para Celular <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
									estatusMod[1] = emailCelularDao.actualizaCelular(emailCelBean, session.getUserID8(), session.getStatusCelular());
									EIGlobal.mensajePorTrace("<><><><><><>Estatus Alta Celular ----> " + estatusMod[1], EIGlobal.NivelLog.INFO);
									session.setCampaniaActiva(false);
									if (estatusMod[1] != null && estatusMod[1].contains("exitosa")) {
										celUserNew.append("Celular:  ").
											append(emailCelBean.getLada()).
											append(emailCelBean.getNoCelular());
									}
								}
								campos.put(3601, celUserNew.toString());
								if(estatusMod[0] != null && estatusMod[0] != ""){
									if(estatusMod[0].contains("exitosa")){
										EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Correo Actual se actualizo correctamente",EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Correo Actual en session es: " + session.getMensajeUSR().getEmailUsuario(),EIGlobal.NivelLog.INFO);
										session.getMensajeUSR().setEmailUsuario(emailCelBean.getCorreo());
										EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Correo Nuevo en session es: " + session.getMensajeUSR().getEmailUsuario(),EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Usuario es SuperUsuario?: " + session.getMensajeUSR().isSuperUsuario(),EIGlobal.NivelLog.INFO);
										if(session.getMensajeUSR().isSuperUsuario()){
											EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Usuario es SuperUsuario, Actualizando datos...",EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->Correo de superUsuario actual... " + session.getMensajeUSR().getEmailSuperUsuario(),EIGlobal.NivelLog.INFO);
											session.getMensajeUSR().setEmailSuperUsuario(emailCelBean.getCorreo());
											EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->Correo de superUsuario modificado... " + session.getMensajeUSR().getEmailSuperUsuario(),EIGlobal.NivelLog.INFO);
										}

										session.setStatusCorreo("M");
										//session.setCampaniaActiva(false);

									}
								}
								if(estatusMod[1] != null && estatusMod[1] != ""){
									if(estatusMod[1].contains("exitosa")){
                                                                            //Se obtienen los datos para Monitor Plus
                                                                            datoViejo.put(0 , new StringBuilder("Mail: ").        
                                                                            append(MAIL_USUARIO_NOTIF).
                                                                            append(" Celular: ").append(celularBean.getLada()).
                                                                            append(celularBean.getNoCelular()).toString());
                                                                           
										EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Celular Actual se actualizo correctamente",EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Celular Actual en session es: " + session.getMensajeUSR().getLadaUsuario() + "-" + session.getMensajeUSR().getNoCelularUsuario(),EIGlobal.NivelLog.INFO);
										session.getMensajeUSR().setNoCelularUsuario(emailCelBean.getNoCelular());
										session.getMensajeUSR().setLadaUsuario(emailCelBean.getLada());
										session.getMensajeUSR().setCompCelularUsuario((String)session.getCarrierCompleto().get(emailCelBean.getCarrier()));
										EIGlobal.mensajePorTrace( "<><><><><>Carrier en Session -> " + emailCelBean.getCarrier(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace( "<><><><><><><>[MANTENIMIENTO DE DATOS]->El Celular Nuevo en session es: " + session.getMensajeUSR().getLadaUsuario() + "-" + session.getMensajeUSR().getNoCelularUsuario(),EIGlobal.NivelLog.INFO );
										EIGlobal.mensajePorTrace( "<><><><><><><>[MANTENIMIENTO DE DATOS]->El Usuario es SuperUsuario?: " + session.getMensajeUSR().isSuperUsuario(),EIGlobal.NivelLog.INFO);
										if(session.getMensajeUSR().isSuperUsuario()){
											EIGlobal.mensajePorTrace("<><><><><><><>[MANTENIMIENTO DE DATOS]->El Usuario es SuperUsuario, Actualizando datos...",EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace( "<><><><><><><>[MANTENIMIENTO DE DATOS]->Celular de superUsuario actual... " + session.getMensajeUSR().getLadaSuperUsuario() + "-" + session.getMensajeUSR().getNoCelularSuperUsuario(), EIGlobal.NivelLog.INFO );
											session.getMensajeUSR().setNoCelularSuperUsuario(emailCelBean.getNoCelular());
											session.getMensajeUSR().setLadaSuperUsuario(emailCelBean.getLada());
											session.getMensajeUSR().setCompCelularSuperUsuario((String)session.getCarrierCompleto().get(emailCelBean.getCarrier()));

											EIGlobal.mensajePorTrace( "<><><><><><><>[MANTENIMIENTO DE DATOS]->Celular de superUsuario modificado... " + session.getMensajeUSR().getLadaSuperUsuario() + "-" + session.getMensajeUSR().getNoCelularSuperUsuario(),EIGlobal.NivelLog.INFO );
										}

										session.setStatusCelular("M");
										//session.setCampaniaActiva(false);
									}
								}

								EIGlobal.mensajePorTrace("<><><>[MANTENIMIENTO DE DATOS] -> Configurando notificacion", EIGlobal.NivelLog.INFO);

								EIGlobal.mensajePorTrace("<><><>[MANTENIMIENTO DE DATOS] -> Mail viejo: "  + MAIL_USUARIO_NOTIF, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("<><><>[MANTENIMIENTO DE DATOS] -> Mail actual: " + session.getMensajeUSR().getEmailUsuario(), EIGlobal.NivelLog.INFO);

								emailDet.setMailNuevo(session.getMensajeUSR().getEmailUsuario());
								emailDet.setMailViejo(MAIL_USUARIO_NOTIF);
								emailDet.setTipoDato("Cuenta de Usuario");
								notifMailCuentaContrato( req, MAIL_USUARIO_NOTIF , MAIL_CONTRATO_NOTIF, emailDet );
								MAIL_USUARIO_NOTIF = session.getMensajeUSR().getEmailUsuario();
								//Termina Consys 10.08.2010

						}

                                                    //Envio a Monitor Plus								            
                                                    int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
                                                    VinculacionMonitorPlusUtils.enviarActualizacionContratoUsuario(campos, req, datoViejo,
                                                        Integer.valueOf((String) beanMap.get("tipoMod")), referencia, session);

                                        
						//Datos Usuario
						req.setAttribute("idUsuario", (String)beanMap.get("cveUsuario"));
						req.setAttribute("nombreUsuario", (String)beanMap.get("nombreUsuario"));
						req.setAttribute("apePaternoUsuario", (String)beanMap.get("apePaternoUsuario"));
						req.setAttribute("apeMaternoUsuario", (String)beanMap.get("apeMaternoUsuario"));
						//req.setAttribute("emailUsuario", consultaEmailUsuario((String)beanMap.get("cveUsuario")));

						//Consys 11.08.2010
						//emailBean = emailCelularDao.consultaCorreo(session.getUserID8(),"004");
						emailBean.setCorreo(session.getMensajeUSR().getEmailUsuario());
						EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><><><><><><>EMAIL DESPUES DE MODIFICACION: "+emailBean.getCorreo(), EIGlobal.NivelLog.DEBUG);

						if( session.getMensajeUSR().getNoCelularUsuario() != null && session.getMensajeUSR().getNoCelularUsuario() != "" &&
						    session.getMensajeUSR().getLadaUsuario() != null && session.getMensajeUSR().getLadaUsuario() != "" &&
							(session.getMensajeUSR().getNoCelularUsuario().length() + session.getMensajeUSR().getLadaUsuario().length()) == 10){
							celularBean.setLada(session.getMensajeUSR().getLadaUsuario());
							celularBean.setNoCelular(session.getMensajeUSR().getNoCelularUsuario());
						}else{
							EIGlobal.mensajePorTrace("<><><><><>[ MANTENIMIENTO DE DATOS ] -> Valor Incorrecto en Celular: " + session.getMensajeUSR().getLadaSuperUsuario() + "-" + session.getMensajeUSR().getNoCelularSuperUsuario(), EIGlobal.NivelLog.DEBUG);
						}
						celularBean.setCarrier(session.getMensajeUSR().getCompCelularUsuario());
						req.setAttribute("emailUsuario", emailBean.getCorreo());
						req.setAttribute("celularUsr", celularBean.getNoCelular());
						req.setAttribute("ladaUsr", celularBean.getLada());
						req.setAttribute("carrierCel", celularBean.getCarrier());

						if(session.getMensajeUSR().isSuperUsuario()){
							session.getMensajeUSR().setNoCelularSuperUsuario(celularBean.getNoCelular());
							session.getMensajeUSR().setLadaSuperUsuario(celularBean.getLada());
							session.getMensajeUSR().setEmailSuperUsuario(emailBean.getCorreo());
							session.getMensajeUSR().setCompCelularSuperUsuario(celularBean.getCarrier());//Descripcion
						}else{
							session.getMensajeUSR().setEmailUsuario(emailBean.getCorreo());
						}

						req.getSession().setAttribute("session", session);
						//Termina Consys 11.08.2010

						//Datos Contrato
						req.setAttribute("cveContrato", (String)beanMap.get("cveContrato"));
						req.setAttribute("descContrato", (String)beanMap.get("descContrato"));
						req.setAttribute("emailContrato", session.getMensajeUSR().getEmailContrato());
						//req.setAttribute("exitoMsg", "Se actualizo el Email Exitosamente");

						if(new Integer((String)beanMap.get("tipoMod")).intValue() == MAN_USUARIO){

							String msgExito = "";

							msgExito = creaMensaje( estatusMod[0], estatusMod[1] );

							req.setAttribute("exitoMsg", msgExito);

						}else{
							req.setAttribute("exitoMsg", "Se actualizo el Email Exitosamente");
						}

						if(beanMap.get("emailUsuario")!=null && !beanMap.get("emailUsuario").toString().equals("")) {
							sess.setAttribute("email", beanMap.get("emailUsuario").toString().trim());
						}

					}catch(Exception e){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}



					try{
						EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

						int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
	    				String claveOperacion = BitaConstants.MODIFICACION_DATOS_PERSONALES;
	    				String concepto = BitaConstants.CONCEPTO_MODIFICACION_DATOS_PERSONALES;

	    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
	    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

	    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);
					} catch(Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

					}

					//*******************



	            }
			}

			req.setAttribute("newMenu", (String) beanMap.get("newMenu"));
			req.setAttribute("MenuPrincipal", (String) beanMap.get("MenuPrincipal"));
			req.setAttribute("Encabezado", (String) beanMap.get("Encabezado"));

			EIGlobal.mensajePorTrace("CapturaDatosUsuario::inicioServletCapturaDatosUsuario:: Finalizando proceso.", EIGlobal.NivelLog.INFO);
			try {
				evalTemplate("/jsp/captura_datos_usr.jsp", req, res);
			} catch (ServletException e) {
				req.setAttribute("errorMsg", "Error al tratar de actualizar el Email");
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (IOException e) {
				req.setAttribute("errorMsg", "Error al tratar de actualizar el Email");
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
       }

	}

	/**
	 * Metodo encargado de cargar en un mapa los parametros provenientes de la
	 * pantalla
	 *
	 * @param req					Petición del servidor de aplicaciones
	 * @return parametros			Mapa con Parametros llenos
	 */
	private HashMap cargaParametros(HttpServletRequest req) {
		HashMap parametros = new HashMap();
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		parametros.put("opcion", req.getParameter("opcion"));
		parametros.put("tipoMod", req.getParameter("tipoMod"));

		//Datos Usuario
		parametros.put("cveUsuario", session.getUserID8());
		parametros.put("nombreUsuario", separateName(session.getNombreUsuario())[ID_NOMBRE_USUARIO]);
		parametros.put("apePaternoUsuario", separateName(session.getNombreUsuario())[ID_PATERNO_USUARIO]);
		parametros.put("apeMaternoUsuario", separateName(session.getNombreUsuario())[ID_MATERNO_USUARIO]);
		parametros.put("emailUsuario", req.getParameter("emailUsuario"));

		//Consys 11.08.2010
		parametros.put("carriers", req.getParameter("carriers"));
		parametros.put("celularUsr", req.getParameter("celularUsr"));
		parametros.put("ladaUsr", req.getParameter("ladaUsr"));
		parametros.put("carrierCel",req.getParameter("carrierCel"));

		//Datos Contrato
		parametros.put("cveContrato", session.getContractNumber());
		parametros.put("descContrato", session.getNombreContrato());
		parametros.put("emailContrato", req.getParameter("emailContrato"));

		parametros.put("MenuPrincipal", session.getStrMenu());
		parametros.put("Encabezado", CreaEncabezado("Mant. de Datos Personales", "Administraci&oacute;n y Control &gt; Mant. de Datos Personales", "s55250h", req));
		parametros.put("newMenu", session.getFuncionesDeMenu());



		return parametros;
	}

	/**
	 * Metodo encargado de la modificación de los datos del contrato
	 *
	 * @param beanMap			Mapa con el contenido de datos provenientes de
	 * 							la pantalla
	 * @throws Exception		Error de tipo Exception
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	private boolean modificaCtr(HashMap beanMap) throws Exception{
		Connection conn= null;
		Statement sDup = null;
		boolean flag = false;
		String query = null;
		int i = 0;
		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE);
			sDup = conn.createStatement();
			query = "Update tct_cuentas_tele set "
				  + "email='" + (String)beanMap.get("emailContrato") + "' "
				  + "where num_cuenta='" + (String)beanMap.get("cveContrato") + "'";
			EIGlobal.mensajePorTrace ("CapturaDatosUsuario::modificaCtr:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			i = sDup.executeUpdate(query);

			if(i>0)
				EIGlobal.mensajePorTrace ("CapturaDatosUsuario::modificaCtr:: -> La actualización fue exitosa", EIGlobal.NivelLog.INFO);
			flag = true;
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			throw e;
		} finally {
			EI_Query.cierraStatement(sDup);
			EI_Query.cierraConnection(conn);
		}

		return flag;

	}

	/**
	 * Metodo encargado de regresar la descripción del nombre
	 *
	 * @param userName		Nombre completo
	 * @return				Arreglo con datos separados
	 */
	public String[] separateName(String userName){
		StringTokenizer name = new StringTokenizer(userName," ");
		String[] descName = new String[name.countTokens()];
		String n = "";
		String app = "";
		String apm = "";

		for(int i=0; i<descName.length; i++){
			descName[i] = name.nextToken();
		}

		for(int j=0; j<descName.length; j++){
			if(j==(descName.length-1)){
				apm = descName[j];
				break;
			}
			if(j==(descName.length-2)){
				app = descName[j];
				continue;
			}
			n += descName[j] + " ";
		}

		return new String[]{n,app,apm};
	}

	/**
	 * Metodo encargado de indicar que el contrato ha sido actualizado.
	 *
	 * @param beanMap			Mapa con las propiedades del request
	 * @throws Exception		Error de tipo Exception
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	private void indicaCtrUpd(HashMap beanMap) throws Exception{
		String query = null;
		ResultSet rs = null;
		Connection conn= null;
		Statement sDup = null;
		int i = 0;
		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select count(*) "
				  + "from eweb_contract_upd "
				  + "where num_cuenta2='" + (String)beanMap.get("cveContrato") + "'";
			EIGlobal.mensajePorTrace ("CapturaDatosUsuario::indicaCtrUpd:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				if(rs.getInt(1)==1){
					query = "Update eweb_contract_upd set "
						  + "estatus='1' "
						  + "where num_cuenta2='" + (String)beanMap.get("cveContrato") + "'";
					EIGlobal.mensajePorTrace ("CapturaDatosUsuario::indicaCtrUpd:: -> Query:" + query, EIGlobal.NivelLog.INFO);
					i = sDup.executeUpdate(query);
					if(i>0)
						EIGlobal.mensajePorTrace ("CapturaDatosUsuario::indicaCtrUpd:: -> contrato actualizado", EIGlobal.NivelLog.INFO);
				}else{
					query = "Insert into eweb_contract_upd(num_cuenta2, estatus) "
						  +	"Values('" + (String)beanMap.get("cveContrato")
						  + "','1')";
					EIGlobal.mensajePorTrace ("CapturaDatosUsuario::indicaCtrUpd:: -> Query:" + query, EIGlobal.NivelLog.INFO);
					i = sDup.executeUpdate(query);
					if(i>0)
						EIGlobal.mensajePorTrace ("CapturaDatosUsuario::indicaCtrUpd:: -> contrato insertado", EIGlobal.NivelLog.INFO);
				}
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			throw e;
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
	}

	public boolean notifMailCuentaContrato( HttpServletRequest request, String emailUser, String emailContract, EmailDetails emailDetails) throws Exception {

		EIGlobal.mensajePorTrace("notifMailCuentaContrato::inicio notifMailCuentaContrato", EIGlobal.NivelLog.INFO);

		boolean envioNotif = false;

		EmailSender emailSender=new EmailSender();

		emailSender.enviaNotificacionMantenimientoDatos( request, emailUser, emailContract, emailDetails );

		envioNotif = true;

		EIGlobal.mensajePorTrace("CapturaDatosUsuario::Fin notifMailCuentaContrato", EIGlobal.NivelLog.INFO);

		return envioNotif;

	}
	public String creaMensaje(String codODB6, String codODB5){
		String msgTrans = "";
		String celActualizado = "";
		String emailActualizado = "";
		EIGlobal.mensajePorTrace("<><><><><><><><> MANTENIMIENTO DE DATOS[Estructurando mensaje] -> Codigo de ODB5: " + codODB5, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("<><><><><><><><> MANTENIMIENTO DE DATOS[Estructurando mensaje] -> Codigo de ODB6: " + codODB6, EIGlobal.NivelLog.INFO);
		if( codODB6 != null && codODB6 != "" && codODB6.length() >= 3 )
			if(codODB6.contains("exitosa")){
				emailActualizado = "A";
			}
		if( codODB5 != null && codODB5 != "" && codODB5.length() >= 3 )
			if(codODB5.contains("exitosa")){
				celActualizado = "A";
			}
		if( codODB6 != null && codODB6 != "" )
			if(codODB6.contains("NA")){
				emailActualizado = "NA";
			}
		if( codODB5 != null && codODB5 != "" )
			if(codODB5.contains("NA")){
				celActualizado = "NA";
			}
		if( codODB6 != null && codODB6 != "" && codODB6.length() >= 3 )
			if ( codODB6.equals("Persona Inexistente") || codODB6.equals("Error") ){
				emailActualizado = "E";
			}
		if( codODB5 != null && codODB5 != "" && codODB5.length() >= 3 )
			if(codODB5.equals("Persona Inexistente") || codODB5.equals("Error")){
				celActualizado = "E";
			}
		if("NA".equals(emailActualizado)){
			if(celActualizado.equals("NA")){
				msgTrans = "No se Actualizaron sus Datos";
			}else{
				if("E".equals(celActualizado)){
					msgTrans = "No fue posible actualizar su N&uacute;mero de Celular ya que su registro en la Base de datos se encuentra incompleto.";
				}else{
					msgTrans = ("Su N&uacute;mero Celular fue actualizado correctamente. Estos datos se " +
							"utilizar&aacute;n para el env&iacute;o de notificaciones del canal Enlace Internet.");
				}
			}
		}else{
			if("A".equals(emailActualizado)){
				if("NA".equals(celActualizado)){
					msgTrans = ("Sus correo fue actualizado correctamente. Estos datos se " +
							"utilizar&aacute;n para el env&iacute;o de notificaciones del canal Enlace Internet.");
				}else{
					if("E".equals(celActualizado)){
						msgTrans = "Su Correo fue actualizado correctamente, pero no fue posible actualizar su N&uacute;mero de Celular ya que su registro en la Base de datos se encuentra incompleto.";
					}else{
						msgTrans = ("Sus datos de contacto fueron actualizados correctamente. Estos datos se " +
								"utilizar&aacute;n para el env&iacute;o de notificaciones del canal Enlace Internet.");
					}
				}
			}else{
				if("NA".equals(celActualizado)){
					msgTrans = ("No fue posible actualizar su Correo ya que su registro en la Base de datos se encuentra incompleto.");
				}else{
					if("E".equals(celActualizado)){
						msgTrans = "No fue posible actualizar sus datos ya que su registro en la Base de datos se encuentra incompleto.";
					}else{
						msgTrans = ("Su N&uacute;mero Celular fue actualizado correctamente, pero no fue posible actualizar su Correo ya que su registro en la Base de datos se encuentra incompleto.");
					}
				}
			}
		}
		return msgTrans;
	}
}