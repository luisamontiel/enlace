package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

//VSWF


public class CambioPasswd extends BaseServlet
{
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		log("CambioPasswd::defaultAction");

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		boolean sesionvalida = SesionValida( req, res );
		if ( sesionvalida )
		{
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Cambio de contrase&ntilde;a","Administraci&oacute;n y Control &gt; Cambio de contrase&ntilde;a", "s26040h", req));
		}
		//TODO BIT CU5051, entrada al flujo de cambio de Contraseña
		/*VSWF-HGG-I*/
		if (Global.USAR_BITACORAS.trim().equals("ON")){
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CAMBIO_CONTRASENA_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/*VSWF-HGG-F*/

		if( sesionvalida && session.getFacultad(session.FAC_CAMBIO_PASSWORD ) )
		{

			/*<VC proyecto="200710001" autor="TIB" fecha="07/06/2007" descripción="CAMBIO DE CONTRASENA">*/
			String param = req.getParameter("pwdSAM");

			//JGR 15/06/2007 Si el origen es Conf_cambio_nip.jsp
			if (req.getHeader("IV-USER")!=null){
				param = "1";
			}
			//JGR
			if (param!=null) {
				req.setAttribute("pwdSAM", "val");
			}
			/*</VC>*/

			evalTemplate( IEnlace.CAMBIO_PASSWD, req, res );
		} else {
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}
	}
}