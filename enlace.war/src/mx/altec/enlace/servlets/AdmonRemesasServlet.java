package mx.altec.enlace.servlets;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.AsignacionBean;
import mx.altec.enlace.beans.LMXC;
import mx.altec.enlace.beans.LMXD;
import mx.altec.enlace.beans.LMXE;
import mx.altec.enlace.beans.LMXF;
import mx.altec.enlace.beans.RemesasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.AdmonRemesasBO;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.ConsultaAsignacionDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Servlet implementation class AdmonRemesasServlet
 */
public class AdmonRemesasServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final String textoLog = AdmonRemesasServlet.class.getName();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		EIGlobal.mensajePorTrace(textoLog + "Entro al AdmonRemesasServlet",
				EIGlobal.NivelLog.INFO);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		request.setAttribute("mensaje", null);
		request.setAttribute("anioRem", cal.get(Calendar.YEAR));
		request.setAttribute("mesRem", cal.get(Calendar.MONTH) + 1);
		request.setAttribute("diaRem", cal.get(Calendar.DATE));
		if (!SesionValida(request, response)) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
			return;
		}

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());

		/*
		 * if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
		 * request.setAttribute("mensaje",
		 * "El contrato no cuenta con servicio de nómina"); return; }
		 */

		int operacion = Integer.parseInt(request.getParameter("operacion"));
		EIGlobal
				.mensajePorTrace(
						textoLog
								+ "Operación####################################################: "
								+ operacion, EIGlobal.NivelLog.INFO);
		switch (operacion) {
		case 1:
			iniciarRecepcion(request, response);
			break;
		case 2:
			iniciarConsulta(request, response);
			break;
		case 3:
			recibirRemesa(request, response);
			break;
		case 4:
			exportarRemesa(request, response);
			break;
		case 5:
			detalleRemesa(request, response);
			break;
		case 6:
			exportarTarjeta(request, response);
			break;
		case 7:
			cancelarTarjeta(request, response);
			break;
		case 9:
			detalleRemesa(request, response);
			break;
		case 11:
			exportaRemesaAdm(request, response);
			break;
		default:
			consultaTarjetas(request, response);
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public void iniciarRecepcion(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (verificaFacultad("INOMREMADM", request)) {
			EIGlobal.mensajePorTrace(textoLog + "Paso SERE01",
					EIGlobal.NivelLog.INFO);
			LMXC contrato = new LMXC();
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			String esSiguiente = request.getParameter("esSiguiente");
			String Contrato = session.getContractNumber();
			String EntidadPag = null;
			String ContratoEnlacePag = null;
			String CtroDistribucionPag = null;
			String NumeroRemesaPag = null;
			String EstadoRemesaPag = null;
			String FechaInicioPag = null;
			String FechaFinalPag = null;
			String CtoEnlacePag = null;
			String numeroRegistros = null;
			String numeroRegistrosAnt = null;
			EIGlobal.mensajePorTrace(textoLog + "esSiguiente::::::::::::: "+esSiguiente,EIGlobal.NivelLog.INFO);
			if(esSiguiente!=null && esSiguiente.trim().equals("S")){
				EntidadPag = request.getParameter("EntidadPag");
				ContratoEnlacePag = request.getParameter("ContratoEnlacePag");
				CtroDistribucionPag = request.getParameter("CtroDistribucionPag");
				NumeroRemesaPag = request.getParameter("NumeroRemesaPag");
				EstadoRemesaPag = request.getParameter("EstadoRemesaPag");
				FechaInicioPag = request.getParameter("FechaInicioPag");
				FechaFinalPag = request.getParameter("FechaFinalPag");
				CtoEnlacePag = request.getParameter("CtoEnlacePag");
				numeroRegistros = request.getParameter("numRegistros");
				numeroRegistrosAnt = request.getParameter("numRegistrosAnt");
			}

			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+Contrato,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+EntidadPag,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+ContratoEnlacePag,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+CtroDistribucionPag,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+NumeroRemesaPag,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+EstadoRemesaPag,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+FechaInicioPag,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+FechaFinalPag,EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ENTIDAD::::::::::::: "+CtoEnlacePag,EIGlobal.NivelLog.INFO);


			int numeroReg = 0;
			int numeroAnt = 0;
			if (numeroRegistros != null && !numeroRegistros.equals("")) {
				numeroReg = Integer.parseInt(numeroRegistros);
			}
			if (numeroRegistrosAnt != null && !numeroRegistrosAnt.equals("")) {
				numeroAnt = Integer.parseInt(numeroRegistrosAnt);
			}
			if(Contrato!=null && !Contrato.equals("")){
				contrato.setContratoEnlace(Contrato);
			}

			//Setteando paginacion
			if (EntidadPag != null && !EntidadPag.equals("")) {
				contrato.setEntidad(EntidadPag);
			}
			if (ContratoEnlacePag != null && !ContratoEnlacePag.equals("")) {
				contrato.setContratoEnlace(ContratoEnlacePag);
			}
			if (CtroDistribucionPag != null && !CtroDistribucionPag.equals("")) {
				contrato.setCtroDistribucion(CtroDistribucionPag);
			}
			if (NumeroRemesaPag != null && !NumeroRemesaPag.equals("")) {
				contrato.setNumeroRemesa(NumeroRemesaPag);
			}
			if (EstadoRemesaPag != null && !EstadoRemesaPag.equals("")) {
				contrato.setEstadoRemesa(EstadoRemesaPag);
			}
			if (FechaInicioPag != null && !FechaInicioPag.equals("")) {
				contrato.setFechaInicio(FechaInicioPag);
			}
			if (FechaFinalPag != null && !FechaFinalPag.equals("")) {
				contrato.setFechaFinal(FechaFinalPag);
			}
			if (CtoEnlacePag != null && !CtoEnlacePag.equals("")) {
				contrato.setCtoEnlacePag(CtoEnlacePag);
			}

			AdmonRemesasBO remesasBO = new AdmonRemesasBO();
			boolean isExcel = false;
			LMXC remBean = remesasBO.consultaRemesas(contrato, isExcel);
			if (remBean != null && remBean.getListaRemesas() != null) {
				if (numeroAnt > 0) {
					numeroAnt = numeroReg + 1;
				} else {
					numeroAnt = 1;
				}
				numeroReg = numeroReg + remBean.getListaRemesas().size();
			}
			remBean.setNumRegistros("" + numeroReg);
			remBean.setNumRegistrosAnt("" + numeroAnt);
			request.setAttribute("RemesasBean", remBean);
			request.setAttribute("newMenu", session.getFuncionesDeMenu());
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request
					.setAttribute(
							"Encabezado",
							CreaEncabezado(
									"Administraci&oacute;n de Remesas",
									"Servicios &gt; Cuenta Inmediata &gt; Remesas &gt Administraci&oacute;n",
									"", request));
			evalTemplate("/jsp/AdmonRemesas.jsp", request, response);
		} else {
			request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(
					request, response);
		}
	}

	public void iniciarConsulta(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (verificaFacultad("INOMREMCON", request)) {
			String strInhabiles = "";
			String fechaHoy = "";
			try{
				strInhabiles = armaDiasInhabilesJS();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("AdmonRemesasServlet::iniciarConsulta:: Error>" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}
			StringBuffer varFechaHoy = new StringBuffer();

			varFechaHoy.append("  /*Fecha De Hoy*/");
			varFechaHoy.append("\n  dia=new Array(");
			varFechaHoy.append(generaFechaAnt("DIA"));
			varFechaHoy.append(",");
			varFechaHoy.append(generaFechaAnt("DIA"));
			varFechaHoy.append(");");
			varFechaHoy.append("\n  mes=new Array(");
			varFechaHoy.append(generaFechaAnt("MES"));
			varFechaHoy.append(",");
			varFechaHoy.append(generaFechaAnt("MES"));
			varFechaHoy.append(");");
			varFechaHoy.append("\n  anio=new Array(");
			varFechaHoy.append(generaFechaAnt("ANIO"));
			varFechaHoy.append(",");
			varFechaHoy.append(generaFechaAnt("ANIO"));
			varFechaHoy.append(");");
			varFechaHoy.append("\n\n  /*Fechas Seleccionadas*/");
			varFechaHoy.append("\n  dia1=new Array(");
			varFechaHoy.append(generaFechaAnt("DIA"));
			varFechaHoy.append(",");
			varFechaHoy.append(generaFechaAnt("DIA"));
			varFechaHoy.append(");");
			varFechaHoy.append("\n  mes1=new Array(");
			varFechaHoy.append(generaFechaAnt("MES"));
			varFechaHoy.append(",");
			varFechaHoy.append(generaFechaAnt("MES"));
			varFechaHoy.append(");");
			varFechaHoy.append("\n  anio1=new Array(");
			varFechaHoy.append(generaFechaAnt("ANIO"));
			varFechaHoy.append(",");
			varFechaHoy.append(generaFechaAnt("ANIO"));
			varFechaHoy.append(");");
			varFechaHoy.append("\n  Fecha=new Array('");
			varFechaHoy.append(generaFechaAnt("FECHA"));
			varFechaHoy.append("','");
			varFechaHoy.append(generaFechaAnt("FECHA"));
			varFechaHoy.append("');");

			GregorianCalendar cal = new GregorianCalendar();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("es","mx"));
			fechaHoy = sdf.format(cal.getTime());

			EIGlobal.mensajePorTrace(textoLog + "Paso SECR01",
					EIGlobal.NivelLog.INFO);
			request.setAttribute("DiasInhabiles",strInhabiles);
			request.setAttribute("VarFechaHoy",varFechaHoy.toString());
			request.setAttribute("DiaHoy",fechaHoy);
			request
					.setAttribute(
							"Encabezado",
							CreaEncabezado(
									"Consulta de remesas",
									"Servicios &gt; Cuenta Inmediata &gt; Remesas &gt Consulta",
									"", request));
			evalTemplate("/jsp/ConsultaRemesas.jsp", request, response);
		} else {
			request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(
					request, response);
		}

	}

	public void recibirRemesa(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (verificaFacultad("INOMREMADM", request)) {
			LMXD remesa = new LMXD();
			StringTokenizer token = null;
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			String noContrato = session.getContractNumber();
			remesa.setContratoEnlace(noContrato);
			String folioRemesa = request.getParameter("folioRemesa");
			String numeroRemesa = null;
			String estadoRemesa = null;
			String centroDistribucion = null;
			if(folioRemesa!=null){
				token = new StringTokenizer(folioRemesa, "|");
				numeroRemesa = token.nextToken();
				estadoRemesa = request.getParameter("estado");
				centroDistribucion = token.nextToken();
			}
			//String centroDistribucion = request.getParameter("numCtroDist");
			EIGlobal.mensajePorTrace(textoLog + "numeroRemesa->"+numeroRemesa,EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "estadoRemesa->"+estadoRemesa,EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "centroDistribucion->"+centroDistribucion,EIGlobal.NivelLog.DEBUG);
			remesa.setNumeroRemesa(numeroRemesa);
			remesa.setEstadoRemesa(estadoRemesa);
			remesa.setCtroDistribucion(centroDistribucion);

			String fuiToken = request.getParameter("fuiToken");

			EIGlobal
					.mensajePorTrace(
							textoLog
									+ "Se verifica si el usuario tiene token y si la transacci&oacute;n"
									+ " est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP",
							EIGlobal.NivelLog.DEBUG);
			boolean solVal = ValidaOTP.solicitaValidacion(session
					.getContractNumber(), IEnlace.NOMINA);
			//CSA-Vulnerabilidad  Token Bloqueado-Inicio
            int estatusTokenOnline = obtenerEstatusToken(session.getToken());
            
			EIGlobal.mensajePorTrace(textoLog + "validartoken="
					+ session.getValidarToken() + "facultad="
					+ session.getFacultad(session.FAC_VAL_OTP) + "estatus"
					+ estatusTokenOnline, EIGlobal.NivelLog.DEBUG);
			if (session.getValidarToken()
					&& session.getFacultad(session.FAC_VAL_OTP)
					&& estatusTokenOnline == 1 && solVal) {

				if (fuiToken == null) {
					EIGlobal.mensajePorTrace(
							"El usuario tiene Token. \nSolicita la validaci&oacute;n. "
									+ "\nSe guardan los parametros en sesion",
							EIGlobal.NivelLog.DEBUG);
					guardaParametrosEnSession2(request, remesa);
					ValidaOTP.validaOTP(request, response,
							IEnlace.VALIDA_OTP_CONFIRM);
				}
			} else if(session.getValidarToken()
					&& session.getFacultad(session.FAC_VAL_OTP)
					&& estatusTokenOnline == 2 && solVal){                        
            		cierraSesionTokenBloqueado(session,request,response);
			}else{
        		fuiToken = "0";
        	}
			//CSA-Vulnerabilidad  Token Bloqueado-Fin
			EIGlobal.mensajePorTrace("fuitoken=########" + fuiToken,
					EIGlobal.NivelLog.DEBUG);
			LMXD remesaToken = null;
			if (fuiToken != null && fuiToken.equals("1")) {
				remesaToken = (LMXD) request.getAttribute("remesaBeanToken");
			} else if (fuiToken != null && fuiToken.equals("0")) {
				remesaToken = remesa;
			}
			Boolean esnull = remesaToken == null;
			EIGlobal.mensajePorTrace("esnull=########" + esnull,
					EIGlobal.NivelLog.DEBUG);
			if (remesaToken != null) {
				EIGlobal.mensajePorTrace(textoLog + "numeroRemesa->"+remesaToken.getNumeroRemesa(),EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(textoLog + "estadoRemesa->"+remesaToken.getEstadoRemesa(),EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(textoLog + "centroDistribucion->"+remesaToken.getCtroDistribucion(),EIGlobal.NivelLog.DEBUG);
				AdmonRemesasBO remesasBO = new AdmonRemesasBO();
				LMXD remBean = remesasBO.recibirRemesa(remesaToken);
				EIGlobal.mensajePorTrace("codigoExito=########"
						+ remBean.isCodExito(), EIGlobal.NivelLog.DEBUG);
				if (remBean.isCodExito()) {
					String paso = "";
					String clvOper = "";
					String estatusToken = request.getParameter("estatusToken");
					EIGlobal.mensajePorTrace("estatusRemesaToken=########"
							+ estatusToken, EIGlobal.NivelLog.DEBUG);
					if (estatusToken != null && estatusToken.equals("R")) {
						EIGlobal.mensajePorTrace(textoLog + "Paso SERE02",
								EIGlobal.NivelLog.INFO);
						clvOper = BitaConstants.ES_NOMINAN3_RECEP_REMESA;
						paso = "Recibida";
					} else if (estatusToken != null && estatusToken.equals("D")) {
						EIGlobal.mensajePorTrace(textoLog + "Paso SERE03",
								EIGlobal.NivelLog.INFO);
						clvOper = BitaConstants.ES_NOMINAN3_CANCE_REMESA;
						paso = "Cancelada";
					}
					request.setAttribute("mensaje", "La remesa "
							+ remesaToken.getNumeroRemesa() + " ha sido "
							+ paso + " correctamente");
//***************************************************INICIA BITACORIZACIÓN***************************************************//
					EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Entrando ...", EIGlobal.NivelLog.INFO);
					if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
						try{
							BitaHelper bh = new BitaHelperImpl(request, session, sess);

							// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);

							// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
							BitaTCTBean beanTCT = new BitaTCTBean ();
							beanTCT = bh.llenarBeanTCT(beanTCT);

							//Datos Comunes
							if (estadoRemesa != null && estadoRemesa.equals("R")){
								bt.setNumBit(BitaConstants.ES_NOMINAN3_RECEP_EJECUTA);
								bt.setIdErr(BitaConstants.ES_NOMINAN3_RECEP_REMESA + "0000");
								beanTCT.setCodError(BitaConstants.ES_NOMINAN3_RECEP_REMESA + "0000");
							}else if (estadoRemesa != null && estadoRemesa.equals("D")){
						    	bt.setNumBit(BitaConstants.ES_NOMINAN3_CANCE_EJECUTA);
						    	bt.setIdErr(BitaConstants.ES_NOMINAN3_CANCE_REMESA + "0000");
						    	beanTCT.setCodError(BitaConstants.ES_NOMINAN3_CANCE_REMESA + "0000");
							}

							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
								beanTCT.setNumCuenta(session.getContractNumber().trim());
							}

							if (session.getUserID8() != null) {
								bt.setCodCliente(session.getUserID8().trim());
								beanTCT.setUsuario(session.getUserID8().trim());
								beanTCT.setOperador(session.getUserID8().trim());
							}

							if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
											.equals(BitaConstants.VALIDA)) {
									bt.setIdToken(session.getToken().getSerialNumber());
							}

							ServicioTux tuxGlobal = new ServicioTux();
							Hashtable hs = null;
							Integer referencia = 0 ;
							String codError = null;
							hs = tuxGlobal.sreferencia("901");
							codError = hs.get("COD_ERROR").toString();
							referencia = (Integer) hs.get("REFERENCIA");
							if(codError.endsWith("0000") && referencia!=null){
								bt.setReferencia(referencia);
								beanTCT.setReferencia(referencia);
							}

							//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
							bt.setIdFlujo(clvOper);
							bt.setEstatus("A");

							//Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
							beanTCT.setTipoOperacion(clvOper);

							//Inserto en Bitacoras
							BitaHandler.getInstance().insertBitaTransac(bt);
							BitaHandler.getInstance().insertBitaTCT(beanTCT);
							EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: bitacorizando " + clvOper, EIGlobal.NivelLog.INFO);

						} catch (Exception e) {
							EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
						}
					}
//*****************************************************FIN BITACORIZACIÓN***************************************************//
				} else {
					if (estadoRemesa != null && estadoRemesa.equals("R")) {
						request.setAttribute("mensaje",
						"Se generó un error al recibir la remesa");
					} else if (estadoRemesa != null && estadoRemesa.equals("D")) {
						request.setAttribute("mensaje",
						"Se generó un error al cancelar la remesa");
					}
				}
				iniciarRecepcion(request, response);
			} else {
				request.getRequestDispatcher("/enlaceMig/SinFacultades")
						.forward(request, response);
			}
		}
	}

	public void exportarRemesa(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String include = "/jsp/RemesasExp.jsp";
		LMXC contrato = new LMXC();
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String noContrato = session.getContractNumber();
		contrato.setContratoEnlace(noContrato);
		AdmonRemesasBO remesasBO = new AdmonRemesasBO();
		boolean isExcel = true;
		LMXC remBean = remesasBO.consultaRemesas(contrato, isExcel);
		request.setAttribute("RemesasBean", remBean);
		request
				.setAttribute(
						"Encabezado",
						CreaEncabezado(
								"Consulta de remesas",
								"Servicios &gt; Cuenta Inmediata &gt; Remesas &gt Administraci&oacute;n",
								"", request));
		try {
			EIGlobal.mensajePorTrace(textoLog + "Paso SERE04",
					EIGlobal.NivelLog.INFO);
			response.setContentType("application/vnd.ms-word");
			response.setHeader("Content-disposition", "attachment; filename="
					+ session.getUserID() + ".doc");
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Error al obtener el excel",
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute("mensaje", "Error al generar el excel");
		}
		request.getRequestDispatcher(include).include(request, response);
	}

	public void exportaRemesaAdm(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String include = "/jsp/RemesasAdminExp.jsp";
		ArrayList<RemesasBean> listaRemesasAdm = null;

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		listaRemesasAdm = (ArrayList)request.getSession().getAttribute("ListaRemesasAdm");
		request.setAttribute("listaRemesasAdmin", listaRemesasAdm);
		request
			.setAttribute(
					"Encabezado",
					CreaEncabezado(
							"Consulta de remesas",
							"Servicios &gt; Cuenta Inmediata &gt; Remesas &gt Consulta",
							"", request));
		//request.getSession().removeAttribute("ListaRemesasAdm");
		try {
			EIGlobal.mensajePorTrace(textoLog + "Paso SERE04",
					EIGlobal.NivelLog.INFO);
			response.setContentType("application/vnd.ms-word");
			response.setHeader("Content-disposition", "attachment; filename="
					+ session.getUserID() + ".doc");
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Error al obtener el excel",
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute("mensaje", "Error al generar el excel");
		}
		request.getRequestDispatcher(include).include(request, response);
	}

	public void consultaTarjetas(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (verificaFacultad("INOMREMCON", request)) {
			EIGlobal.mensajePorTrace(textoLog + "Paso SERE01",
					EIGlobal.NivelLog.INFO);
			LMXC contrato = new LMXC();
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			String noContrato = session.getContractNumber();
			contrato.setContratoEnlace(noContrato);
			String tipoFecha = request.getParameter("tipoFecha");
			String inicio = request.getParameter("inicio");
			String fecha1 = request.getParameter("fecha2");
			String fecha2 = request.getParameter("fecha1");
			String folioRemesa2 = request.getParameter("folioRemesa2");
			EIGlobal.mensajePorTrace("folioRemesa2=###############"
					+ folioRemesa2, EIGlobal.NivelLog.INFO);

			String folioRemesa = request.getParameter("folioRemesa");
			String estatus = "";
			if (inicio != null && inicio.equals("inicio")) {
				if (folioRemesa != null && !folioRemesa.equals("")) {
					folioRemesa2 = folioRemesa;
				}
			}
			if (folioRemesa2 != null && !folioRemesa2.equals("")) {
				folioRemesa = folioRemesa2;
			}
			EIGlobal.mensajePorTrace("folioRemesa=#####" + folioRemesa,
					EIGlobal.NivelLog.INFO);
			if (tipoFecha != null && tipoFecha.equals("A")) {
				estatus = "R";
			} else if (tipoFecha != null && tipoFecha.equals("C")) {
				estatus = "D";
			} else if (tipoFecha != null && tipoFecha.equals("E")) {
				estatus = "E";
			}
			EIGlobal.mensajePorTrace("folioRemesa=#####" + folioRemesa,
					EIGlobal.NivelLog.INFO);
			if (folioRemesa == null || folioRemesa.equals("null")) {
				folioRemesa = "";
			}
			EIGlobal.mensajePorTrace("folioRemesa=#####" + folioRemesa,
					EIGlobal.NivelLog.INFO);
			contrato.setNumeroRemesa(folioRemesa);
			AdmonRemesasBO remesasBO = new AdmonRemesasBO();
			LMXC remBean = remesasBO.consultaRemesasVarias(contrato, tipoFecha,
					estatus, fecha1, fecha2);

			if (remBean != null && remBean.getListaRemesas() != null) {
				if (remBean.getListaRemesas().size() > 100) {
					ArrayList<RemesasBean> listacien = new ArrayList<RemesasBean>();
					for (int i = 0; i < 100; i++) {
						listacien.add(remBean.getListaRemesas().get(i));
					}
					remBean.setListaRemesas(listacien);

					request.setAttribute("mensaje",
							"El número de registros es mayor a 100");
				}
			}
			remBean.setNumeroRemesa2(folioRemesa2);
			remBean.setTipoFecha(tipoFecha);
			remBean.setFechaFinal(fecha2);
			remBean.setFechaInicio(fecha1);
			if (remBean.isCodExito() && remBean.getListaRemesas()!=null) {
				//EIGlobal.mensajePorTrace("AdmonRemesasServlet::consultaTarjetas:: Se procede a exportar el archivo.", EIGlobal.NivelLog.DEBUG);
				//try {
					//exportar(remBean, request, response);
				//} catch (Exception e) {
					//EIGlobal.mensajePorTrace("AdmonRemesasServlet::consultaTarjetas:: No se pudo generar el archivo de exportación->" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
				//}
//***************************************************INICIA BITACORIZACIÓN***************************************************//
				EIGlobal.mensajePorTrace("AdmonRemesasServlet::consultaTarjetas:: Entrando ...", EIGlobal.NivelLog.INFO);
				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
						BitaHelper bh = new BitaHelperImpl(request, session, sess);

						// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);

						// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
						BitaTCTBean beanTCT = new BitaTCTBean ();
						beanTCT = bh.llenarBeanTCT(beanTCT);

						//Datos Comunes
						bt.setNumBit(BitaConstants.ES_NOMINAN3_CONSULTA_CONSULTA);
						bt.setIdErr(BitaConstants.ES_NOMINAN3_CONSULTA_REMESA + "0000");
						beanTCT.setCodError(BitaConstants.ES_NOMINAN3_CONSULTA_REMESA + "0000");

						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
							beanTCT.setNumCuenta(session.getContractNumber().trim());
						}

						if (session.getUserID8() != null) {
							bt.setCodCliente(session.getUserID8().trim());
							beanTCT.setUsuario(session.getUserID8().trim());
							beanTCT.setOperador(session.getUserID8().trim());
						}

						ServicioTux tuxGlobal = new ServicioTux();
						Hashtable hs = null;
						Integer referencia = 0 ;
						String codError = null;
						hs = tuxGlobal.sreferencia("901");
						codError = hs.get("COD_ERROR").toString();
						referencia = (Integer) hs.get("REFERENCIA");
						if(codError.endsWith("0000") && referencia!=null){
							bt.setReferencia(referencia);
							beanTCT.setReferencia(referencia);
						}

						//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
						bt.setIdFlujo(BitaConstants.ES_NOMINAN3_CONSULTA_REMESA);
						bt.setEstatus("A");

						//Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
						beanTCT.setTipoOperacion(BitaConstants.ES_NOMINAN3_CONSULTA_REMESA);

						//Inserto en Bitacoras
						BitaHandler.getInstance().insertBitaTransac(bt);
						BitaHandler.getInstance().insertBitaTCT(beanTCT);
						EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: bitacorizando " + BitaConstants.ES_NOMINAN3_CONSULTA_REMESA, EIGlobal.NivelLog.INFO);

					} catch (Exception e) {
						EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
					}
				}
//*****************************************************FIN BITACORIZACIÓN***************************************************//

			}

			request.setAttribute("RemesasBean", remBean);
			request.getSession().setAttribute("ListaRemesasAdm", remBean.getListaRemesas());
			request
					.setAttribute(
							"Encabezado",
							CreaEncabezado(
									"Consulta de remesas",
									"Servicios &gt; Cuenta Inmediata &gt; Remesas &gt Consulta",
									"", request));
			EIGlobal.mensajePorTrace(textoLog + "Seteo Menu y Encabezado",
					EIGlobal.NivelLog.DEBUG);
			evalTemplate("/jsp/ResultadoRemesas.jsp", request, response);
		} else {
			request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(
					request, response);
		}
	}

	public void detalleRemesa(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LMXE remesa = new LMXE();
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String noContrato = session.getContractNumber();
		remesa.setContratoEnlace(noContrato);

		// variables para saber a donde regreso
		String reiniciaConta = request.getParameter("reiniciaConta");
		String tipoFecha = request.getParameter("tipoFecha");
		String fecha1 = request.getParameter("fecha2");
		String fecha2 = request.getParameter("fecha1");
		String numeroRemesa2 = request.getParameter("folioRemesa2");

		String numeroRemesa = request.getParameter("folioRemesa");
		String numeroCtroDist = request.getParameter("numCtroDist");
		String numTarjetaPag = request.getParameter("numTarjetaPag");
		String numeroRegistros = request.getParameter("numRegistros");
		String numeroRegistrosAnt = request.getParameter("numRegistrosAnt");
		String totalTarj = request.getParameter("totalTarjetas");

		StringTokenizer token=null;
		if(numeroRemesa != null){
			token=new StringTokenizer(numeroRemesa, "|");
			numeroRemesa=token.nextToken();
			numeroCtroDist=token.nextToken();
		}

		EIGlobal.mensajePorTrace(textoLog + "numeroRemesa->"+numeroRemesa,EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(textoLog + "centroDistribucion->"+numeroCtroDist,EIGlobal.NivelLog.DEBUG);

		int numeroReg = 0;
		int numeroAnt = 0;
		if (numeroRegistros != null && !numeroRegistros.equals("")) {
			numeroReg = Integer.parseInt(numeroRegistros);
		}
		if (numeroRegistrosAnt != null && !numeroRegistrosAnt.equals("")) {
			numeroAnt = Integer.parseInt(numeroRegistrosAnt);
		}
		if(reiniciaConta!=null && reiniciaConta.trim().equals("S")){
			numeroReg = 0;
			numeroAnt = 0;
		}

		if (numTarjetaPag != null && !numTarjetaPag.equals("")) {
			remesa.setTarjeta(numTarjetaPag);
		}
		remesa.setCtroDistribucion(numeroCtroDist);
		remesa.setNumeroRemesa(numeroRemesa);
		AdmonRemesasBO remesasBO = new AdmonRemesasBO();
		boolean isExcel = false;

		LMXE tarBean = remesasBO.consultaDetalleRemesa(remesa, isExcel);
		if (tarBean != null && tarBean.getListaTarjetas() != null) {
			if (tarBean.isExistenMasDatos()) {
				tarBean.getListaTarjetas().remove(
						tarBean.getListaTarjetas().size() - 1);
			}
			if (numeroAnt > 0) {
				numeroAnt = numeroReg + 1;
			} else {
				numeroAnt = 1;
			}
			numeroReg = numeroReg + tarBean.getListaTarjetas().size();
		}

		int totalTarjetas = 0;
		if (totalTarj == null || totalTarj.equals("")) {
			EIGlobal.mensajePorTrace("Entro por el total de tarjetas"
					+ tarBean.getTarjeta(), EIGlobal.NivelLog.DEBUG);
			LMXE tarBeanTot = remesasBO.consultaDetalleRemesa(remesa, true);

			if (tarBeanTot != null && tarBeanTot.getListaTarjetas() != null) {
				totalTarjetas = tarBeanTot.getListaTarjetas().size();
				EIGlobal.mensajePorTrace("Si traigo tarjetas y son"
						+ totalTarjetas + tarBean.getTarjeta(),
						EIGlobal.NivelLog.DEBUG);
			}
		} else {
			totalTarjetas = Integer.parseInt(totalTarj);
		}

		tarBean.setTotalTarjetas("" + totalTarjetas);
		tarBean.setCtroDistribucion(numeroCtroDist);
		tarBean.setNumeroRemesa(numeroRemesa);
		tarBean.setNumRegistros("" + numeroReg);
		tarBean.setNumRegistrosAnt("" + numeroAnt);
		tarBean.setFechaIniicio(fecha1);
		tarBean.setFechaFin(fecha2);
		tarBean.setTipoFecha(tipoFecha);
		tarBean.setNumRemesa2(numeroRemesa2);
		request.setAttribute("TarjetasBean", tarBean);
		request
				.setAttribute(
						"Encabezado",
						CreaEncabezado(
								"Consulta de remesas",
								"Servicios &gt; Cuenta Inmediata &gt; Remesas &gt Consulta",
								"", request));
		evalTemplate("/jsp/DetalleRemesas.jsp", request, response);
	}

	public void exportarTarjeta(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String include = "/jsp/TarjetasExp.jsp";
		LMXE contrato = new LMXE();
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String noContrato = session.getContractNumber();
		contrato.setContratoEnlace(noContrato);
		String numeroRemesa = request.getParameter("numRemesa");
		String numeroCtroDist = request.getParameter("numCtroDist");
		contrato.setCtroDistribucion(numeroCtroDist);
		contrato.setNumeroRemesa(numeroRemesa);
		AdmonRemesasBO remesasBO = new AdmonRemesasBO();
		boolean isExcel = true;
		LMXE tarBean = remesasBO.consultaDetalleRemesa(contrato, isExcel);
		request.setAttribute("TarjetasBean", tarBean);
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request
				.setAttribute(
						"Encabezado",
						CreaEncabezado(
								"Consulta de remesas",
								"Servicios &gt; Cuenta Inmediata &gt; Remesas &gt Consulta",
								"", request));
		try {
			EIGlobal.mensajePorTrace(textoLog + "Paso SECR05",
					EIGlobal.NivelLog.INFO);
			response.setContentType("application/vnd.ms-word");
			response.setHeader("Content-disposition", "attachment; filename="
					+ session.getUserID() + ".doc");
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Error al obtener el excel",
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute("mensaje", "Error al generar el excel");
		}
		request.getRequestDispatcher(include).include(request, response);
	}

	public void cancelarTarjeta(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("Entro a Cancelar Tarjeta ###########",
				EIGlobal.NivelLog.INFO);
		if (verificaFacultad("INOMREMADM", request)) {

			LMXF tarjeta = new LMXF();
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			String noContrato = session.getContractNumber();
			tarjeta.setContratoEnlace(noContrato);
			String numRemesa = request.getParameter("numRemesa");
			String numCtroDist = request.getParameter("numCtroDist");
			String numTarjeta = request.getParameter("numTarjeta");
			String folioRemesa = request.getParameter("folioRemesa");
			String folioRemesa2 = request.getParameter("folioRemesa2");
			String fechaIni = request.getParameter("fecha2");
			String fechaFin = request.getParameter("fecha1");
			String tipoFecha=request.getParameter("tipoFecha");
			String codErrBit = "";
			EIGlobal.mensajePorTrace("numRemesa=" + numRemesa + "Numctrodis"
					+ numCtroDist + "numtarjeta" + numTarjeta,
					EIGlobal.NivelLog.INFO);
			AdmonRemesasBO remesasBO = new AdmonRemesasBO();
			tarjeta.setTarjeta(numTarjeta);
			tarjeta.setNumeroRemesa(numRemesa);
			tarjeta.setCtroDistribucion(numCtroDist);
			tarjeta.setEstadoTarjeta("D");
			tarjeta.setFolioRemesa2(folioRemesa2);
			tarjeta.setFechaFin(fechaFin);
			tarjeta.setFechaIni(fechaIni);
			tarjeta.setTipoFecha(tipoFecha);

			String fuiToken = request.getParameter("fuiToken");

			EIGlobal
					.mensajePorTrace(
							textoLog
									+ "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP",
							EIGlobal.NivelLog.DEBUG);
			boolean solVal = ValidaOTP.solicitaValidacion(session
					.getContractNumber(), IEnlace.NOMINA);
			//CSA-Vulnerabilidad  Token Bloqueado-Inicio
            int estatusTokenOnline = obtenerEstatusToken(session.getToken());
            
			EIGlobal.mensajePorTrace(textoLog + "validartoken="
					+ session.getValidarToken() + "facultad="
					+ session.getFacultad(session.FAC_VAL_OTP) + "estatus"
					+ estatusTokenOnline, EIGlobal.NivelLog.DEBUG);
			if (session.getValidarToken()
					&& session.getFacultad(session.FAC_VAL_OTP)
					&& estatusTokenOnline == 1 && solVal) {

				if (fuiToken == null) {
					EIGlobal
							.mensajePorTrace(
									"El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion",
									EIGlobal.NivelLog.DEBUG);
					guardaParametrosEnSession(request, tarjeta, folioRemesa);
					ValidaOTP.validaOTP(request, response,
							IEnlace.VALIDA_OTP_CONFIRM);
				}
			}else if(session.getValidarToken()
					&& session.getFacultad(session.FAC_VAL_OTP)
					&& estatusTokenOnline == 2 && solVal){                        
            		cierraSesionTokenBloqueado(session,request,response);
			} else {
				fuiToken = "0";
			}
			//CSA-Vulnerabilidad  Token Bloqueado-Fin
			LMXF tarjetaToken = null;
			if (fuiToken != null && fuiToken.equals("1")) {
				tarjetaToken = (LMXF) request.getAttribute("tarjetaBeanToken");
			} else if (fuiToken != null && fuiToken.equals("0")) {
				tarjetaToken = tarjeta;
			}
			if (tarjetaToken != null) {
				LMXF remBean = remesasBO.cancelarTarjeta(tarjetaToken);
				EIGlobal.mensajePorTrace(textoLog
						+ "Paso SECR01################+numtarjeta"
						+ tarjetaToken.getTarjeta(), EIGlobal.NivelLog.INFO);
				if (remBean.isCodExito()) {

					/************* Cancelar Tarjeta Enlace ************/
					AdmonRemesasBO admonRemesasBO=new AdmonRemesasBO();
					AsignacionBean tarjeta1=new AsignacionBean();
					int resultado=0;
					tarjeta1.setContrato(tarjetaToken.getContratoEnlace());
					tarjeta1.setTarjeta(tarjetaToken.getTarjeta());
					try {
						EIGlobal.mensajePorTrace("AdmonRemesasServlet :: Cancelando tarjeta Enlace", EIGlobal.NivelLog.DEBUG);
						resultado=admonRemesasBO.cancelartarjetaEnlace(tarjeta1, request, response);
						request.setAttribute("mensaje", "La tarjeta "+ tarjetaToken.getTarjeta()+ " ha sido cancelada correctamente");
						EIGlobal.mensajePorTrace("AdmonRemesasServlet :: Ocurrio un error al cancelar la tarjeta en Enlace", EIGlobal.NivelLog.DEBUG);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace("Ocurrio un error al cancelar la tarjeta en Enlace", EIGlobal.NivelLog.DEBUG);
					}
//***************************************************INICIA BITACORIZACIÓN***************************************************//
					EIGlobal.mensajePorTrace("AdmonRemesasServlet::cancelarTarjeta:: Entrando ...", EIGlobal.NivelLog.INFO);
					if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
						try{
							BitaHelper bh = new BitaHelperImpl(request, session, sess);

							// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);

							// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
							BitaTCTBean beanTCT = new BitaTCTBean ();
							beanTCT = bh.llenarBeanTCT(beanTCT);

							//Datos Comunes
					    	bt.setNumBit(BitaConstants.ES_NOMINAN3_CANCE_TARJETA_CANCELA);
					    	bt.setIdErr(BitaConstants.ES_NOMINAN3_CANCE_TARJETA + "0000");
					    	beanTCT.setCodError(BitaConstants.ES_NOMINAN3_CANCE_TARJETA + "0000");

							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
								beanTCT.setNumCuenta(session.getContractNumber().trim());
							}

							if (session.getUserID8() != null) {
								bt.setCodCliente(session.getUserID8().trim());
								beanTCT.setUsuario(session.getUserID8().trim());
								beanTCT.setOperador(session.getUserID8().trim());
							}

							if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
											.equals(BitaConstants.VALIDA)) {
									bt.setIdToken(session.getToken().getSerialNumber());
							}

							ServicioTux tuxGlobal = new ServicioTux();
							Hashtable hs = null;
							Integer referencia = 0 ;
							String codError = null;
							hs = tuxGlobal.sreferencia("901");
							codError = hs.get("COD_ERROR").toString();
							referencia = (Integer) hs.get("REFERENCIA");
							if(codError.endsWith("0000") && referencia!=null){
								bt.setReferencia(referencia);
								beanTCT.setReferencia(referencia);
							}

							//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
							bt.setIdFlujo(BitaConstants.ES_NOMINAN3_CANCE_TARJETA);
							bt.setEstatus("A");

							//Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
							beanTCT.setTipoOperacion(BitaConstants.ES_NOMINAN3_CANCE_TARJETA);

							//Inserto en Bitacoras
							BitaHandler.getInstance().insertBitaTransac(bt);
							BitaHandler.getInstance().insertBitaTCT(beanTCT);
							EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: bitacorizando " + BitaConstants.ES_NOMINAN3_CANCE_TARJETA, EIGlobal.NivelLog.INFO);

						} catch (Exception e) {
							EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
						}
					}
//*****************************************************FIN BITACORIZACIÓN***************************************************//

				} else {
					request.setAttribute("mensaje",
							"Se generó un error al cancelar la tarjeta");
				}
				detalleRemesa(request, response);
			} else {
				request.getRequestDispatcher("/enlaceMig/SinFacultades")
						.forward(request, response);
			}
		}
	}

	public static void bitacoriza(HttpServletRequest request, String servicio,
			String codError, BitaTransacBean bt) {
		bitacoriza(request, servicio, codError, false, bt);
	}

	public static void bitacoriza(HttpServletRequest request, String servicio,
			String codError, boolean inicia, BitaTransacBean bt) {
		String idFlujo = request.getParameter(BitaConstants.FLUJO);
		if (Global.USAR_BITACORAS.trim().equals("ON") && idFlujo != null
				&& !idFlujo.trim().equals("")) {

			HttpSession session = request.getSession();
			BaseResource baseResource = (BaseResource) session
					.getAttribute("session");
			BitaHelperImpl bh = new BitaHelperImpl(request, baseResource,
					session);
			if (inicia)
				bh.incrementaFolioFlujo(idFlujo);
			bh.llenarBean(bt);
			bt.setContrato(baseResource.getContractNumber());
			bt.setUsr(baseResource.getUserID8());
			if (servicio != null)
				bt.setServTransTux(servicio);
			if (codError != null)
				bt.setIdErr(codError);
			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void guardaParametrosEnSession2(HttpServletRequest request,
			LMXD remesa) {

		EIGlobal.mensajePorTrace(textoLog
				+ "Dentro de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
		request.getSession()
				.setAttribute("plantilla", request.getServletPath());

		HttpSession session = request.getSession(false);

		if (session != null) {
			Map<Object, Object> tmp = new HashMap<Object, Object>();
			EIGlobal.mensajePorTrace("estadoRemesa)=#########"
					+ remesa.getEstadoRemesa(), EIGlobal.NivelLog.DEBUG);
			tmp.put("operacion", "3");
			tmp.put("estatusToken", remesa.getEstadoRemesa());
			tmp.put("fuiToken", "1");

			String value;

			value = (String) getFormParameter(request,"estado");
			if(value != null && !value.equals(null)) {
				tmp.put("estado",getFormParameter(request,"estado"));
			}

			request.setAttribute("remesaBeanToken", remesa);

			// tmp.put("Appmaterno", empleadoBean.getAppMaterno());

			session.setAttribute("parametros", tmp);

			tmp = new HashMap<Object, Object>();
			Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
		EIGlobal.mensajePorTrace(textoLog
				+ "Saliendo de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
	}

	public void guardaParametrosEnSession(HttpServletRequest request,
			LMXF tarjeta, String folioRemesa) {

		EIGlobal.mensajePorTrace(textoLog
				+ "Dentro de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
		request.getSession()
				.setAttribute("plantilla", request.getServletPath());

		HttpSession session = request.getSession(false);

		if (session != null) {
			Map<Object, Object> tmp = new HashMap<Object, Object>();

			tmp.put("operacion", "7");
			tmp.put("fuiToken", "1");
			//tmp.put("folioRemesa", tarjeta.getNumeroRemesa());
			tmp.put("folioRemesa", folioRemesa);
			tmp.put("numCtroDist", tarjeta.getCtroDistribucion());
			tmp.put("folioRemesa2", tarjeta.getFolioRemesa2());
			tmp.put("fecha2", tarjeta.getFechaIni());
			tmp.put("fecha1", tarjeta.getFechaFin());
			tmp.put("tipoFecha", tarjeta.getTipoFecha());


			request.setAttribute("tarjetaBeanToken", tarjeta);

			// tmp.put("Appmaterno", empleadoBean.getAppMaterno());

			session.setAttribute("parametros", tmp);

			tmp = new HashMap<Object, Object>();
			Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
		EIGlobal.mensajePorTrace(textoLog
				+ "Saliendo de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Metodo para manejar el negocio para exportar las remesas
	 * asociadas al contrato.
	 *
	 * @param remBean			    Bean con datos de exportación
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @throws Exception			Dispara Exception
	 */
	public void exportar(LMXC remBean, HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("AdmonRemesasServlet::exportar:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String linea = null;
		FileWriter fileExport = new FileWriter(Global.DIRECTORIO_REMOTO_WEB + "/" + session.getUserID() + ".doc");
		EIGlobal.mensajePorTrace("AdmonRemesasServlet::exportar:: Se creo el archivo.", EIGlobal.NivelLog.DEBUG);
		try{
			linea = "";
			linea += rellenar("NUMERO DE REMESA",20,' ','D')
			  	  +  rellenar("FECHA DE ALTA",20,' ','D')
			      +  rellenar("FECHA DE RECEPCION",20,' ','D')
			      +  rellenar("NUMERO DE TARJETAS",20,' ','D')
			      +  rellenar("ESTADO DE REMESA",20,' ','D');
			fileExport.write(linea + "\n");
			for(int i=0; i<remBean.getListaRemesas().size(); i++){
				RemesasBean beanExport = (RemesasBean)remBean.getListaRemesas().get(i);
				linea = "";
				linea += rellenar(beanExport.getNumRemesa(),20,' ','D')
					  +  rellenar(beanExport.getFechaAltaRem(),20,' ','D')
					  +  rellenar(beanExport.getFechaRecepRem(),20,' ','D')
					  +  rellenar(beanExport.getNumTarjetas(),20,' ','D')
					  +  rellenar(beanExport.getEstadoRemesa(),20,' ','D');
			    fileExport.write(linea + "\n");
			}
			sess.setAttribute("fileExport","/Download/" + session.getUserID() + ".doc");
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("AdmonRemesasServlet::exportar:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
								   + "<DATOS GENERALES>"
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.INFO);
		}finally{
			fileExport.close();
			EIGlobal.mensajePorTrace("AdmonRemesasServlet::exportar:: Se cerro el archivo.", EIGlobal.NivelLog.DEBUG);
		}
	}

	/**
	 * rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 */
	public static String rellenar (String cad, int lon, char rel, char tip){
		String aux = "";
		if(cad.length()>lon) return cad.substring(0,lon);
		if(tip!='I' && tip!='D') tip = 'I';
		if( tip=='D' ) aux = cad;
		for (int i=0; i<(lon-cad.length()); i++) aux += ""+ rel;
		if( tip=='I' ) aux += ""+ cad;
		return aux;
	}

	/**
	 * Metodo encargado de generar las fechas que se mostraran en el calendario
	 *
	 * @param tipo			Tipo de dato a generar
	 * @return strFecha		Fecha generada
	 */
	private String generaFechaAnt(String tipo){
		String strFecha = "";
		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();

		Cal.setTime(Hoy);

		while(Cal.get(Calendar.DAY_OF_WEEK) == 1 || Cal.get(Calendar.DAY_OF_WEEK) == 7) {
			Cal.add(Calendar.DATE, -1);
	    }

		if(tipo.equals("FECHA")) {
			if(Cal.get(Calendar.DATE) <= 9)
				strFecha += "0" + Cal.get(Calendar.DATE) + "/";
			else
				strFecha += Cal.get(Calendar.DATE) + "/";

			if(Cal.get(Calendar.MONTH)+ 1 <= 9)
				strFecha += "0" + (Cal.get(Calendar.MONTH) + 1) + "/";
			else
				strFecha += (Cal.get(Calendar.MONTH) + 1) + "/";

			strFecha+=Cal.get(Calendar.YEAR);
		}

		if(tipo.equals("DIA"))
			strFecha += Cal.get(Calendar.DATE);

		if(tipo.equals("MES"))
			strFecha += (Cal.get(Calendar.MONTH) + 1);

		if(tipo.equals("ANIO"))
	        strFecha += Cal.get(Calendar.YEAR);

		return strFecha;
	}

	/**
	 * Metodo para traer los dias inhabiles
	 * @return
	 * @throws Exception
	 */
	private String armaDiasInhabilesJS() throws Exception{
		ConsultaAsignacionDAO dao = new ConsultaAsignacionDAO();
		String resultado="";
		Vector diasInhabiles;

		int indice=0;
		try{
			diasInhabiles = dao.CargarDias(1);
			if(diasInhabiles!=null){
				resultado = diasInhabiles.elementAt(indice).toString();
				for(indice=1; indice<diasInhabiles.size(); indice++)
					resultado += ", " + diasInhabiles.elementAt(indice).toString();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resultado;
	}

}