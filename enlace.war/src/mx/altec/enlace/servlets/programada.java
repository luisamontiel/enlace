package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConsultaPaginadaBean;
import mx.altec.enlace.beans.TablaEwebNominaBean;
import mx.altec.enlace.beans.TablaTCTProgramacionBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ConsultaNominaProgramadaBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.export.exception.ExportarException;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class programada extends BaseServlet {

	/**
	 * La serial Version UID
	 */
	private static final long serialVersionUID = 8023654310893789431L;

	/**Registros por pagina**/
	private static final int REG_POR_PAGINA = 30;

	/**Valor del literal para session**/
	private static final String LITERAL_SESSION = "session";

	/**Valor del literal para idOperacionConsultar**/
	private static final String LITERAL_ID_OPERACION_C = "idOperacionConsultar";

	/**Valor del literal para TRAN**/
	private static final String LITERAL_TRAN = "TRAN";

	/**Valor del literal para PAGT**/
	private static final String LITERAL_PAGT = "PAGT";

	/**Valor del literal para valPaginaActual**/
	private static final String LITERAL_PAGINA_ACTUAL = "valPaginaActual";

	/**Valor del literal para valNextPrev**/
	private static final String LITERAL_VAL_NEXT_PREV = "valNextPrev";

	/**Valor del literal para valInicio**/
	private static final String LITERAL_VAL_INICIO = "valInicio";

	/**Valor del literal para valFin**/
	private static final String LITERAL_VAL_FIN = "valFin";

	/**Valor del literal para estatus de programadas**/
	private static final String ESTATUS = "PROGRAMADA";

	/** {@inheritDoc} **/
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		defaultAction( req, res );
	}
	/** {@inheritDoc} **/
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		defaultAction( req, res );
	}

	/**
	 * Metodo inicial del servlet
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***programada.class  &Inicia clase ...&", EIGlobal.NivelLog.INFO);

		boolean sesionvalida = SesionValida( req, res );

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);

		if (session.getFacultad(BaseResource.FAC_PROGRAMA_OP))
		{
			String menu_p = "";
			menu_p        = session.getstrMenu() == null ? "" : session.getstrMenu();

			req.setAttribute("MenuPrincipal", menu_p);
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones Programadas","Consultas &gt; Operaciones Programadas", "s25280h",req));
			req.setAttribute("Fecha", ObtenFecha());
			req.setAttribute("Hora", ObtenHora());
			req.setAttribute("ContUser",ObtenContUser(req));

			String funcion_llamar = req.getParameter("ventana");

			if ("0".equals(funcion_llamar)) {
				//Bitacoriza acceso a programacion
		    	if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
					try{
			        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
			        	if(req.getParameter(BitaConstants.FLUJO)!=null){
			        		bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			        	}else{
			        		bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			        	}
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EC_OPER_ENTRA);
						if(session.getContractNumber()!=null){
							bt.setContrato(session.getContractNumber());
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					}catch(SQLException e){
						pintarExcepciones(e);
					}catch(Exception e){
						pintarExcepciones(e);
					}
		    	}
				evalTemplate(IEnlace.MI_OPER_PROG_TMPL, req, res);

			} else if("2".equals(funcion_llamar)) {
				String tipoOperacion = req.getParameter(LITERAL_ID_OPERACION_C);
				if(LITERAL_TRAN.equals(tipoOperacion) || LITERAL_PAGT.equals(tipoOperacion) || "DIBT".equals(tipoOperacion)) {
					consultaOperacionesProgramadas(req, res);
				} else if("NOMI".equals(tipoOperacion)) {
					consultaOperacionesNomina(req, res);
				}
			} else if("5".equals(funcion_llamar)) {
				exportarArchivo(req, res);
			}

			EIGlobal.mensajePorTrace( "***programada.class  &Termina clase ...&", EIGlobal.NivelLog.INFO);
		} else if (sesionvalida) {
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, req, res);
		}
	}



	/**
	 * Metodo para exportar un archivo
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @throws IOException : exception
	 * @throws ServletException : exception
	 */
	private void exportarArchivo(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace( "***programada.class::exportarArchivo>>", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);

		final String tipoOperacion = req.getParameter(LITERAL_ID_OPERACION_C);
		final String tipoArchivo = req.getParameter("tipoArchivo");
		List<Object> lista = new ArrayList<Object>();
		String tipoExportacion = "";
		ExportModel em = null;
		String ruta_archivo_servicio = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8();

		if(LITERAL_TRAN.equals(tipoOperacion) || LITERAL_PAGT.equals(tipoOperacion) || "DIBT".equals(tipoOperacion)) {

			Map<String, Object> consultaTabla = obtenerConsultaArchivoTux(ruta_archivo_servicio);
			lista = (ArrayList<Object>)consultaTabla.get("LISTA_CONSULTA");
		} else if("NOMI".equals(tipoOperacion)) {
			ConsultaNominaProgramadaBO nominaBO = new ConsultaNominaProgramadaBO();
			Map<String, Object> consultaNomina =  nominaBO.consultarNominaProgramada(session.getContractNumber());

			lista = (ArrayList<Object>)consultaNomina.get("listaDatosNomina");
		}

		if((LITERAL_TRAN.equals(tipoOperacion) || LITERAL_PAGT.equals(tipoOperacion)) && "TXT".equals(tipoArchivo)) {
			em = new ExportModel("archivo_consulta_programacion", '|', true);
		} else {
			em = new ExportModel("archivo_consulta_programacion", '|');
			crearExportModel(em,tipoOperacion);
		}

		if("TXT".equals(tipoArchivo)) {
			tipoExportacion = "TXT";
		} else if("XLS".equals(tipoArchivo)) {
			tipoExportacion = "XLS";
		}

		try {

			if((LITERAL_TRAN.equals(tipoOperacion) || LITERAL_PAGT.equals(tipoOperacion)) && "TXT".equals(tipoArchivo)) {
				String nombreArchivo = exportarTRAN(lista, session.getUserID8());
				HtmlTableExporter.export(tipoExportacion.toLowerCase(), em, nombreArchivo, res);
			} else {
				HtmlTableExporter.export(tipoExportacion, em, lista, res);
			}


		} catch (ExportarException e) {
			pintarExcepciones(e);
		}

	}

	/**
	 * Metodo para crear un objeto del tipo ExportModel
	 * @param em : ExportModel
	 * @param tipoOperacion : String
	 */
	private void crearExportModel (ExportModel em, String tipoOperacion) {
		EIGlobal.mensajePorTrace( "***programada.class  --- crearExportModel " , EIGlobal.NivelLog.DEBUG);
		if(LITERAL_TRAN.equals(tipoOperacion) || LITERAL_PAGT.equals(tipoOperacion)) {
			String tituloCAbono = "";
			if(LITERAL_TRAN.equals(tipoOperacion)) {
				tituloCAbono = "Cuenta Abono";
			} else {
				tituloCAbono = "Tarjeta Abono";
			}
			em
	        //(Propiedad Bean, Titulo, Formato)
	        .addColumn(new Column("descripcionCuenta", "Tipo de Operaci�n     "))
	        .addColumn(new Column("cuenta1", "Cuenta Cargo"))
	        .addColumn(new Column("cuenta2", tituloCAbono))
	        .addColumn(new Column("importe", "Importe"))
	        .addColumn(new Column("fecha", "Fecha"));


		} else  if("NOMI".equals(tipoOperacion)) {
			em
	        //(Propiedad Bean, Titulo, Formato)
	        .addColumn(new Column("fechaRecepcion", "Fecha Recepci�n"))
	        .addColumn(new Column("fechaCargo", "Fecha Cargo"))
	        .addColumn(new Column("fechaAplicacion", "Fecha Aplicaci�n"))
	        .addColumn(new Column("ctaCargo", "Cuenta Cargo"))
	        .addColumn(new Column("nombreArchivo", "Nombre Archivo"))
			.addColumn(new Column("secuencia", "Secuencia"))
			.addColumn(new Column("numeroRegistros", "N�m. Registros"))
			.addColumn(new Column("importeAplic", "Importe"))
			.addColumn(new Column("estatus", "Estatus"));

		} else if("DIBT".equals(tipoOperacion)) {
			em
	        //(Propiedad Bean, Titulo, Formato)
	        .addColumn(new Column("referencia", "Numero de Referencia"))
	        .addColumn(new Column("cuenta1", "Cuenta Origen"))
	        .addColumn(new Column("cuenta2", "Cuenta Destino / M�vil"))
	        .addColumn(new Column("beneficiario", "Beneficiario"))
	        .addColumn(new Column("importe", "Importe"))
			.addColumn(new Column("fecha", "Fecha"))
			.addColumn(new Column("estatus", "Estatus"))
			.addColumn(new Column("concepto", "Concepto del Pago Interbancario"))
			.addColumn(new Column("referenciaInterbancaria", "Referencia Interbancaria"))
			.addColumn(new Column("formaAplicaion", "Forma Aplicac�on"));
		}


	}

	/**
	 * Metodo para genear el archivo original
	 * @param lista : lista de datos
	 * @param usuario : usuario
	 * @return nombre del archivo generado
	 */
	private String exportarTRAN(List<Object> lista, String usuario) {
		StringBuilder buffer = new StringBuilder();
		String arch_servicio =  IEnlace.LOCAL_TMP_DIR + "/" + usuario;
		String nombre_archivo= usuario + "prg.doc";;
		String arreglo_retorno[][]=null;
		EI_Exportar ArcSal;

		ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH + nombre_archivo);
		ArcSal.creaArchivo();

		int contador = lista.size();

		if (contador >0) {
			arreglo_retorno = obtenRegistrosArchivo(arch_servicio, ";", 7, 0, contador );
		}
		if (arreglo_retorno!=null) {

			EIGlobal.mensajePorTrace("Programada.java --> exportarTRAN :: arreglo_retorno es diferente de null ", EIGlobal.NivelLog.INFO);

			for(int i=0;i<(contador);i++)
			{
				buffer = new StringBuilder();
				buffer.append("");
				for(int j=0;j<7;j++)
				{
					if (j==0)
					{
						buffer.append(Description(arreglo_retorno[i][j])).append(";");
					} else
						buffer.append(arreglo_retorno[i][j]).append(";");
				}
				buffer.append("\r\n");
				ArcSal.escribeLinea(buffer.toString());
			}
			ArcSal.cierraArchivo();
			// doble remote shell
			ArchivoRemoto archivoExp = new ArchivoRemoto();

			if (archivoExp.copiaLocalARemoto(nombre_archivo,"WEB")) {
				EIGlobal.mensajePorTrace("copia remota ok", EIGlobal.NivelLog.INFO);
			} else {
				EIGlobal.mensajePorTrace("copia remota no fue realizada", EIGlobal.NivelLog.INFO);
			}
		}
		EIGlobal.mensajePorTrace(">>>> Ruta del archivo ::: " + IEnlace.DOWNLOAD_PATH + nombre_archivo, EIGlobal.NivelLog.INFO);

		return IEnlace.DOWNLOAD_PATH + nombre_archivo;
	}

	/**
	 * Metodo para consulta las operaciones programadas del tipo TRAN, PAGT y Interbancarias
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @return int
	 * @throws IOException : exception
	 * @throws ServletException : exception
	 */
	public int consultaOperacionesProgramadas( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace( "***programada.class  &inicia m�todo consultaOperacionesProgramadas()&", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);


		String ruta_archivo_servicio = "";
		int valInicio  = 0, valFin  = 10;
		int valPaginaActual  = req.getParameter(LITERAL_PAGINA_ACTUAL) == null ? 1 : Integer.parseInt(req.getParameter(LITERAL_PAGINA_ACTUAL).toString());
		String limpiarConsulta = req.getParameter("limpiarConsulta").toString();

		double totalImporte   = 0;

		try {
			String tipoConsulta = req.getParameter("tipoConsulta").toString();
			actualizaOperacionesProgramadas(req, tipoConsulta);
			ruta_archivo_servicio = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8();

			EIGlobal.mensajePorTrace("***programadas.class El valor de ruta_archivo_servicio >" +ruta_archivo_servicio+ "<", EIGlobal.NivelLog.INFO);

			Map<String, Object> consultaTabla = obtenerConsultaArchivoTux(ruta_archivo_servicio);

			List<TablaTCTProgramacionBean> lista = (ArrayList<TablaTCTProgramacionBean>)consultaTabla.get("LISTA_CONSULTA");
			totalImporte = consultaTabla.get("TOTAL_CONSULTA") != null ? Double.parseDouble(consultaTabla.get("TOTAL_CONSULTA").toString()) : 0;
			int pagNextPrev = "".equals(req.getParameter(LITERAL_VAL_NEXT_PREV)) ? -5 : Integer.parseInt(req.getParameter(LITERAL_VAL_NEXT_PREV).toString());

			if(valPaginaActual > 1 || pagNextPrev == -1 ) {
				valInicio = (valPaginaActual -1) * REG_POR_PAGINA;
				valFin = valPaginaActual * REG_POR_PAGINA-1;
			}  else {
				valInicio  = req.getParameter(LITERAL_VAL_INICIO) == null ? 0 : Integer.parseInt(req.getParameter(LITERAL_VAL_INICIO).toString());
				valFin = req.getParameter("LITERAL_VAL_FIN") == null ? REG_POR_PAGINA -1 : Integer.parseInt(req.getParameter("LITERAL_VAL_FIN").toString());
			}

			if("true".equals(limpiarConsulta)) {
				valInicio = 0;
				valFin = REG_POR_PAGINA -1;
				valPaginaActual = 1;
			}

			req.setAttribute("listaConsulta", lista);
			req.setAttribute("totalConsulta", totalImporte);
			req.setAttribute(LITERAL_VAL_INICIO, valInicio);
			req.setAttribute(LITERAL_VAL_FIN, valFin);
			req.setAttribute(LITERAL_PAGINA_ACTUAL, valPaginaActual);
			req.setAttribute(LITERAL_ID_OPERACION_C, req.getParameter(LITERAL_ID_OPERACION_C) == null
					? "" : req.getParameter(LITERAL_ID_OPERACION_C).toString());
			req.setAttribute("numeroRegXPagina", REG_POR_PAGINA);

		    //Bitacoriza consulta
			if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
				ConsultaNominaProgramadaBO programadaBO = new ConsultaNominaProgramadaBO();
				programadaBO.realizarBitacora(req, session, sess, totalImporte, session.getUserID8());
			}
			//Codigo de prueba
	//		ConsultaNominaProgramadaBO programadaBO = new ConsultaNominaProgramadaBO();
		//	programadaBO.FileCopy(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8(), Global.DIRECTORIO_REMOTO_WEB + "/"+ session.getUserID8()+"con_prog.txt");

		} catch (BusinessException e) {
			despliegaPaginaError("Error al realizar la consulta programada","Consulta de Operaciones Programadas","Consultas &gt; Operaciones Programadas", "s25280h", req, res);
			return 0;
		}

		/*
		 * VSWF-BMB-F
		 */
		evalTemplate(IEnlace.MI_OPER_PROG_TMPL, req, res);

        EIGlobal.mensajePorTrace("Saliendo de consultaOperacionesProgramadas", EIGlobal.NivelLog.INFO);
        return 1;
	}

	/**
	 * Metodo para mostrar la consulta de nomina
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @throws IOException : exception
	 * @throws ServletException : exception
	 */
	public void consultaOperacionesNomina( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException {

		EIGlobal.mensajePorTrace( "***programada.class::consultaOperacionesNomina()&", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);
		ConsultaNominaProgramadaBO consultaNom = new ConsultaNominaProgramadaBO();
		ConsultaPaginadaBean resultadoConsulta = new ConsultaPaginadaBean();
		String contrato = session.getContractNumber();

		int valorNextPrev = req.getParameter(LITERAL_VAL_NEXT_PREV) == null || "".equals(req.getParameter(LITERAL_VAL_NEXT_PREV).toString()) ?
				0 : Integer.parseInt(req.getParameter(LITERAL_VAL_NEXT_PREV).toString());

		int regInicio = 0;
		int regFinal = 0;
		if(valorNextPrev == 0) {
			regInicio = 1;
			regFinal = REG_POR_PAGINA;
		} else if(valorNextPrev == 1) {
			regInicio = Integer.parseInt(req.getParameter(LITERAL_VAL_INICIO).toString()) + REG_POR_PAGINA;
			regFinal = Integer.parseInt(req.getParameter(LITERAL_VAL_FIN).toString()) + REG_POR_PAGINA;
		} else if(valorNextPrev == -1) {
			regInicio = Integer.parseInt(req.getParameter(LITERAL_VAL_INICIO).toString()) - REG_POR_PAGINA;
			regFinal = Integer.parseInt(req.getParameter(LITERAL_VAL_FIN).toString()) - REG_POR_PAGINA;
		}

		try {
			resultadoConsulta = consultaNom.consultarNominaProgramadaPag(contrato, regInicio, regFinal);
		} catch (mx.altec.enlace.bo.BusinessException e) {
			despliegaPaginaError("Error al realizar la consulta programada","Consulta de Operaciones Programadas","Consultas &gt; Operaciones Programadas", "s25280h", req, res);
			return;
		}

		Map<String, Object> mapaResultado = resultadoConsulta.getResultadoConsulta();
		Double totalPaginas = Math.ceil(Double.valueOf(resultadoConsulta.getTotalRegistros()) / REG_POR_PAGINA);

		req.setAttribute("totalPaginas", String.valueOf(totalPaginas.intValue()));
		req.setAttribute(LITERAL_PAGINA_ACTUAL, regFinal / REG_POR_PAGINA);
		req.setAttribute("totalNomina", mapaResultado.get("totalGlobalNomina"));
		req.setAttribute("listaConsulta", (List<TablaEwebNominaBean> )mapaResultado.get("listaDatosNomina"));
		req.setAttribute(LITERAL_VAL_FIN, resultadoConsulta.getNumeroRegFin());
		req.setAttribute(LITERAL_VAL_INICIO, resultadoConsulta.getNumeroRegInicio());
		req.setAttribute("totalRegitros", resultadoConsulta.getTotalRegistros());
		req.setAttribute(LITERAL_ID_OPERACION_C, req.getParameter(LITERAL_ID_OPERACION_C) == null
				? "" : req.getParameter(LITERAL_ID_OPERACION_C).toString());
		req.setAttribute("numeroRegXPagina", REG_POR_PAGINA);

		if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			ConsultaNominaProgramadaBO programadaBO = new ConsultaNominaProgramadaBO();
			programadaBO.realizarBitacora(req, session, sess, Double.parseDouble(mapaResultado.get("totalGlobalNomina").toString()), "");
		}

		evalTemplate(IEnlace.MI_OPER_PROG_TMPL, req, res);

	}


	/**
	 * Metodo que genera la lista de las consultas de tipo PAGT y PAGD
	 * @param archivo : archivo creado desde tuexedo
	 * @param tipoConsulta : tipo de la consulta
	 * @return mapa de resultado
	 */

	/**
	 * Se obtiene datos del archivo
	 * @param archivo : nombre del archivo
	 * @return mapa de datos
	 */
	private Map<String, Object> obtenerConsultaArchivoTux(String archivo) {
		EIGlobal.mensajePorTrace( "***programada.class  &inicia m�todo obtenerConsultaArchivoTux()&", EIGlobal.NivelLog.INFO);
		Map<String, Object> respuesta = new  HashMap<String, Object>();
		List<TablaTCTProgramacionBean> listaConsulta = new ArrayList<TablaTCTProgramacionBean>();
		double totalConsulta = 0.0;

		try {
			String linea;
			BufferedReader entrada = new BufferedReader( new FileReader( archivo ) );
			linea = entrada.readLine();

			if(linea != null && "OK".equals(linea.substring(0,2).trim()) ) {
				linea = entrada.readLine();
				while(linea != null) {
					TablaTCTProgramacionBean consulta = llenarDatosProgramados(linea);
					totalConsulta = totalConsulta + Double.parseDouble(consulta.getImporte());
					listaConsulta.add(consulta);

					linea = entrada.readLine();
				}
			}

			respuesta.put("REGISTROS_CONSULTA", listaConsulta.size());
			respuesta.put("LISTA_CONSULTA", listaConsulta);
			respuesta.put("TOTAL_CONSULTA", String.format("%.2f", totalConsulta));

			entrada.close();
		} catch(Exception e) {
			pintarExcepciones(e);
		}

		return respuesta;
	}

	/**
	 * LLena bean de datos para operaciones programadas
	 * @param lineaArchivo : linea del archivo
	 * @return bean de datos
	 */
	private TablaTCTProgramacionBean llenarDatosProgramados(String lineaArchivo) {

		EIGlobal.mensajePorTrace( "***programada.class  &inicia m�todo llenarDatosProgramados()&", EIGlobal.NivelLog.INFO);

		TablaTCTProgramacionBean datoConsulta = new TablaTCTProgramacionBean();
		String[] datos = lineaArchivo.split("\\;");

		datoConsulta.setTipoOperacion(datos[0]);
		datoConsulta.setCuenta1(datos[1]);
		datoConsulta.setCuenta2(datos[2]);
		double importe=Double.parseDouble(datos[3]);
		NumberFormat formatter = new DecimalFormat("#0.0#");
		//datoConsulta.setImporte(String.valueOf(Double.parseDouble(datos[3])));
		datoConsulta.setImporte(formatter.format(importe));
		EIGlobal.mensajePorTrace( "***programada.class  &SetImporte:"+ String.valueOf(Double.parseDouble(datos[3])), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***programada.class  &SetImporte:"+ Double.parseDouble(datos[3]), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***programada.class  &SetImporte:"+ Double.parseDouble(datos[3]), EIGlobal.NivelLog.INFO);
		datoConsulta.setFecha(datos[5]);
		datoConsulta.setReferencia(datos[6]);
		datoConsulta.setDescripcionCuenta(Description(datos[0]));
		datoConsulta.setEstatus(ESTATUS);


		if(datos[7].indexOf('|')>-1){
			datoConsulta.setConcepto(datos[7].substring(0,40));
		}
		else{
			datoConsulta.setConcepto(datos[7]);
		}
		datoConsulta.setFormaAplicaion(datos[8]);
		datoConsulta.setReferenciaInterbancaria(datos[9]);

		//si viene informado el ultimo dato
		if(datos.length == 11) {
			datoConsulta.setBeneficiario(datos[10]);
		}

		return datoConsulta;
	}

	/**
	 * Metodo que consulta las operaciones por query
	 * @param contrato : numero de contrato
	 * @param usuario : id de usuario
	 * @param inicio : registro inicial a consultar
	 * @param fin : registro final a consultar
	 * @return arreglo de datos
	 */
	public String[][] llamadaConsultaProgramadasPorQuery(String contrato, String usuario,int inicio,int fin){
		EIGlobal.mensajePorTrace("Entrando a consulta de operaciones programadas por query", EIGlobal.NivelLog.INFO);
		String[][] operacionesgral;
		String coderror = "";
		String cadena_consulta = "";
		String buffer = "";
		StringBuilder  query_realizar = new StringBuilder();
		int contador = 0;
		ResultSet TestResult = null;
		Connection connbd = null;
		PreparedStatement TestQuery = null;
		contador = llamadaConsultaCuantosOperProg( contrato, usuario );
		EIGlobal.mensajePorTrace( "Contador " + contador, EIGlobal.NivelLog.INFO);

		if( contador > 0 ){
			EIGlobal.mensajePorTrace("1", EIGlobal.NivelLog.INFO);

			//IDataConn connbd = createDataConn(0,GX_DA_DAD_DRIVERS.GX_DA_DRIVER_ORACLE_OCI,"",IEnlace.BASE_DATOS,IEnlace.DB_USER,IEnlace.DB_USER_PWD);
			try{
				connbd = createiASConn(Global.DATASOURCE_ORACLE);

				if(connbd == null){
					EIGlobal.mensajePorTrace("Error al intertar crear la conexion", EIGlobal.NivelLog.ERROR);
					return null;
				}
				EIGlobal.mensajePorTrace("2", EIGlobal.NivelLog.INFO);
				//IQuery TestQuery = createQuery();
				query_realizar.append("Select referencia,tipo_operacion,to_char(fecha,'dd/mm/yyyy'),cuenta1,cuenta2,importe");
				query_realizar.append(" from tct_programacion");
				query_realizar.append( " where num_cuenta ='" + contrato + "' and");
				query_realizar.append( " usuario='" + usuario + "' and");
				query_realizar.append( " referencia_bit = 0");
				TestQuery = connbd.prepareStatement(query_realizar.toString());
				EIGlobal.mensajePorTrace("3", EIGlobal.NivelLog.INFO);
				//TestQuery.setSQL(query_realizar);
				if(TestQuery == null){
					EIGlobal.mensajePorTrace("Cannot Create Query", EIGlobal.NivelLog.ERROR);
					return null;
				}
				EIGlobal.mensajePorTrace("4", EIGlobal.NivelLog.INFO);
				TestResult = TestQuery.executeQuery();
				TestResult.next();
				//IResultSet TestResult = connbd.executeQuery(0,TestQuery,null,null);
				if(TestResult == null){
					EIGlobal.mensajePorTrace("Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
					return null;
				}
				EIGlobal.mensajePorTrace("5", EIGlobal.NivelLog.INFO);
				operacionesgral = new String[contador+1][7];
				operacionesgral[0][0] = String.valueOf(contador);
				operacionesgral[0][1] = "referencia";
				operacionesgral[0][2] = "Tipo_oper";
				operacionesgral[0][3] = "cta_origen";
				operacionesgral[0][4] = "cta_destino";
				operacionesgral[0][5] = "importe";
				operacionesgral[0][6] = "fecha";
				EIGlobal.mensajePorTrace("6", EIGlobal.NivelLog.INFO);

				if( inicio > 0 ){

					EIGlobal.mensajePorTrace("se mueve n posiciones", EIGlobal.NivelLog.INFO);
					//TestResult.moveTo(inicio + 1 );
					TestResult.relative( inicio + 1 );
				}

				for (int i=1;i<=(fin-inicio);i++){
					for(int j=1;j<7;j++){
						//if(TestResult.getValueString(j) != null)
						if(TestResult.getString(j) != null){
							operacionesgral[i][j] =  TestResult.getString(j).trim();

						}else {
							operacionesgral[i][j] = "" ;
						}
						EIGlobal.mensajePorTrace("operacionesgral[i][j] "+operacionesgral[i][j], EIGlobal.NivelLog.DEBUG);
					}
					//TestResult.fetchNext();
					TestResult.next();

				}
				EIGlobal.mensajePorTrace("7", EIGlobal.NivelLog.INFO);

				EIGlobal.mensajePorTrace("Saliendo de consulta de operaciones programadas por query ", EIGlobal.NivelLog.INFO);
			}catch( SQLException sqle ){
				EIGlobal.mensajePorTrace("Exepcion en llamada_consulta_programadas_por_query", EIGlobal.NivelLog.INFO);
				pintarExcepciones(sqle);
				operacionesgral= null;
			} finally{
				EI_Query.cierraConexion(TestResult, TestQuery, connbd);
			}
			return operacionesgral;

		}else {
			return null;
		}
    }

//=================================================================================================
	/**
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */

	/**
	 * Metodo de consulta operaciones programadas
     * @param contrato : contrato
     * @param usuario : usuario
     * @return int
     */
    public int llamadaConsultaCuantosOperProg(String contrato, String usuario){
    	Connection connbd = null;
    	ResultSet TestResult = null;
    	PreparedStatement TestQuery = null;
		EIGlobal.mensajePorTrace("Entrando a consulta cuantos oper prog ", EIGlobal.NivelLog.INFO);
		StringBuilder query_realizar=new StringBuilder();
		int contador=0;

		try{
			connbd = createiASConn(Global.DATASOURCE_ORACLE);
			//IDataConn connbd = createDataConn(0,GX_DA_DAD_DRIVERS.GX_DA_DRIVER_ORACLE_OCI,"",IEnlace.BASE_DATOS,IEnlace.DB_USER,IEnlace.DB_USER_PWD);
			if(connbd == null){
				EIGlobal.mensajePorTrace("Error al intertar crear la conexion", EIGlobal.NivelLog.ERROR);
				return 0;
			}
			EIGlobal.mensajePorTrace("uno", EIGlobal.NivelLog.INFO);

			//IQuery TestQuery = createQuery();

			EIGlobal.mensajePorTrace("dos", EIGlobal.NivelLog.INFO);
			query_realizar.append("Select count(*)");
			query_realizar.append(" from tct_programacion");
			query_realizar.append(" where num_cuenta ='"+contrato+"' and");
			query_realizar.append(" usuario='"+usuario+"' and");
			query_realizar.append(" referencia_bit = 0");

			TestQuery = connbd.prepareStatement(query_realizar.toString());

			EIGlobal.mensajePorTrace("tres", EIGlobal.NivelLog.INFO);
			//TestQuery.setSQL(query_realizar);
			if(TestQuery == null){
				EIGlobal.mensajePorTrace("Cannot Create Query", EIGlobal.NivelLog.ERROR);
				return 0;
			}
			EIGlobal.mensajePorTrace("cuatro", EIGlobal.NivelLog.INFO);

			//IResultSet TestResult = connbd.executeQuery(0,TestQuery,null,null);
			TestResult = TestQuery.executeQuery();
			if(TestResult == null){
				EIGlobal.mensajePorTrace("Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
				return 0;
			}

			EIGlobal.mensajePorTrace("cinco", EIGlobal.NivelLog.INFO);
			TestResult.next();
			contador =  Integer.parseInt(TestResult.getString(1).trim());
			EIGlobal.mensajePorTrace("Saliendo a consulta cuantos oper prog ", EIGlobal.NivelLog.INFO);

		}catch(SQLException sqle){
			pintarExcepciones(sqle);
			contador = 0;
		}finally{
			EI_Query.cierraConexion(TestResult,TestQuery,connbd);
		}
		return contador;
	}

	/**
	 * Metod que actualiza el archivo de operaciones programadas
	 * @param req : HttpServletRequest
	 * @return int
	 * @throws IOException : exception
	 * @throws BusinessException : exception
	 */
	public int actualizaOperacionesProgramadas(HttpServletRequest req, String tipoConsulta)
		throws IOException, BusinessException{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);

		int salida = 0; // por default OK

		StringBuilder trama_entrada  = new StringBuilder();
		String trama_salida   = "";
		String tipo_operacion = "COPR";  // prefijo del servicio ( consulta de operaciones programadas )
		String cabecera       = "2EWEB"; // medio de entrega ( regresa un archivo )
		String PERFIL   = session.getUserProfile();
		String USUARIO  = session.getUserID8();
		String CONTRATO = session.getContractNumber();

		ServicioTux tuxedoGlobal = new ServicioTux();

		if (session.getFacultad(BaseResource.FAC_PROGRAMA_OP))
		{
			try {

				EIGlobal.mensajePorTrace( "***programada.class  &facultad de consulta OK ...&", EIGlobal.NivelLog.INFO);

				trama_entrada.append(cabecera).append("|").append(USUARIO).append("|")
					.append(tipo_operacion).append("|").append(CONTRATO).append("|")
					.append(USUARIO).append("|").append(PERFIL).append("|").append(tipoConsulta);

				EIGlobal.mensajePorTrace( "***programada.class  trama de entrada >" +trama_entrada+ "<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "***programada.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

				try {
					Hashtable hs = tuxedoGlobal.web_red( trama_entrada.toString() );
					trama_salida = (String) hs.get("BUFFER");
				}catch( RemoteException re ){
					pintarExcepciones(re);
					throw new BusinessException("ERROR", re);
				}catch (Exception e) {
					pintarExcepciones(e);
					throw new BusinessException("ERROR", e);
					}

				EIGlobal.mensajePorTrace( "***programada.class  trama de salida = >" +trama_salida+ "<", EIGlobal.NivelLog.DEBUG);

				if ( trama_salida.equals( IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() ) )
				{
					// Copia Remota
					EIGlobal.mensajePorTrace( "***programada.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
					ArchivoRemoto archivo_remoto = new ArchivoRemoto();

					if(!archivo_remoto.copia( session.getUserID8() ) )
					{
						EIGlobal.mensajePorTrace( "***programada.class  &error de  remote shell ...&", EIGlobal.NivelLog.INFO);
						throw new BusinessException("ERROR");
					} else {
						salida = 0; // OK
						EIGlobal.mensajePorTrace( "***programada.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
					}
				} else
					throw new BusinessException("ERROR");
			} catch(Exception ioe ) {
				EIGlobal.mensajePorTrace( "***programada.class  Excepci�n en actualizaOperacionesPorgramadas() >>"+ioe.getMessage()+"<<",EIGlobal.NivelLog.ERROR );
				throw new BusinessException("ERROR", ioe);
			}
			//TODO  BIT CU1141
			/*
			 * 10/ENE/07
			 * VSWF-BMB-I
			 */
			 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
			try{
	        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EC_OPER_CONSULTA_OPER_PROG);
				bt.setContrato(CONTRATO);
				bt.setServTransTux(tipo_operacion.trim());
				if(trama_salida != null){
					if(trama_salida.substring(0,2).equals("OK")){
						bt.setIdErr(tipo_operacion.trim()+"0000");
					}else if(trama_salida.length()>8){
						bt.setIdErr(trama_salida.substring(0,8));
					}else{
						bt.setIdErr(trama_salida.trim());
					}
				}
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				pintarExcepciones(e);
			}catch(Exception e){
				pintarExcepciones(e);
			}
			 }
			/*
			 * 02/ENE/07
			 * VSWF-BMB-F
			 */

		} else { // no se tiene facultad
			salida = 1;
			EIGlobal.mensajePorTrace( "***programada.class  &facultad de consulta NO v�lida ...&", EIGlobal.NivelLog.INFO);
		}
		return salida;
	}


	/**
	 * Utileria para pintar el trace de la excepcion
	 * @param e : exception
	 */
	private void pintarExcepciones(Exception e) {
		EIGlobal.mensajePorTrace( "***programada.class  se genero una excepcion " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace( "***programada.class  se genero una excepcion " + e.getCause(), EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace( "***programada.class  se genero una excepcion " + e.getLocalizedMessage(), EIGlobal.NivelLog.ERROR);

	}

	public String[][] obtenRegistrosArchivo(String filename,String delim,int Ndelim,int b_ini,int b_end)
	{
		BufferedReader entrada = null;
		StringTokenizer t ;
		String linea = null;

		EIGlobal.mensajePorTrace( "\n***programada.class  &inicia m�todo obtenRegistrosArchivo()&", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***programada.class  &archivo fuente : >" +filename+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***programada.class  &inicio : >" +b_ini+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***programada.class  &fin : >" +b_end+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***programada.class  &campos : >" +Ndelim+ "<", EIGlobal.NivelLog.INFO);

		int i     = 0;
		int i_ini = b_ini;
		int i_end = b_end;
		int j     = 0;
		int k     = 0;

		String[][] salida = new String[i_end - i_ini][Ndelim];

		try {
			entrada = new BufferedReader( new FileReader( filename ) );
			linea = entrada.readLine();

			if( i_ini > 0 )
			{
				for( j=0 ; j <i_ini ; j++ ) linea = entrada.readLine();
			}

			for( j=0 ; j<( i_end - i_ini ) ; j++ )
			{
				linea = entrada.readLine();
				for( k=0 ; k < Ndelim ; k++ )
				{
					try {
						salida[j][k] = EIGlobal.BuscarToken(linea , ';' , k+1  );
					} catch(Exception e) {
						salida[j][k]=" ";
					}
				}
			}
			entrada.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace( "***programada.class  Excepci�n en obtenRegistrosArchivo() >>"+e.getMessage()+"<<",EIGlobal.NivelLog.ERROR );
		}
		String[][] straux = new String[i_end - i_ini][Ndelim];

		for( j = 0; j < (i_end - i_ini); j++ )
		{
			for( k = 0; k < Ndelim ; k++ )
			{
				straux[j][k]= salida[j][k];
			}
		}
		EIGlobal.mensajePorTrace( "***programada.class  &termina m�todo obtenRegistrosArchivo()&", EIGlobal.NivelLog.INFO);
		return straux;
/*
        try
        {
            entrada= new BufferedReader( new FileReader( filename ) );
            linea = entrada.readLine();

            if( i_ini > 0 )
            {
                for( j=0 ; j <i_ini ; j++ ){
                	if( linea.trim().equals("") ) {
                		linea = entrada.readLine();
					System.out.println(linea);
					}
                	if( linea == null||linea.trim().equals("") )
                		break;
                	linea = entrada.readLine();
					System.out.println(linea);
                }
            }

            for( j=0 ; j<( i_end - i_ini ) ; j++ )
            {
                linea = entrada.readLine();
                if( linea.trim().equals("") )
               		linea = entrada.readLine();
               	if( linea == null ||linea.trim().equals("") )
               		break;


                for( k=0 ; k < Ndelim ; k++ )
                {
                    try
                    {
                        salida[j][k] = EIGlobal.BuscarToken(linea , ';' , k+1  );
						System.out.print(" "+salida[j][k]+" ");

                    }
                    catch(Exception e)
                    {
                        salida[j][k]=" ";
                       }
                }
            }//Fin for

        }
        catch(Exception e)
        {
            EIGlobal.mensajePorTrace( "***programada.class  Excepci�n en obtenRegistrosArchivo() >>"+e.getMessage()+"<<",1 );
        } finally {
						try{
			            entrada.close();
						} catch(Exception e){}
		}

		Vector elementos_ = new Vector ();
		boolean antnull = false;

        for( j = 0; j < (i_end - i_ini); j++ )
        {
            for( k = 0; k < Ndelim ; k++ )
            {
				if(salida[j][k]!=null ) {
						elementos_.add(salida[j][k]);
				}
            }
        }
System.out.println("Tama�o ="+elementos_.size());
for(int uu=0;uu<elementos_.size();uu++) {
	System.out.println(elementos_+"  ");
}

//StringTokenizer lostokens = new StringTokenizer();

        String[][] straux = new String[i_end - i_ini][Ndelim];

        for( j = 0; j < (i_end - i_ini); j++ )
        {
            for( k = 0; k < Ndelim ; k++ )
            {
                straux[j][k]= salida[j][k];
            }
        }
        EIGlobal.mensajePorTrace( "***programada.class  &termina m�todo obtenRegistrosArchivo()&", EIGlobal.NivelLog.INFO);
        return straux;
*/
}


}