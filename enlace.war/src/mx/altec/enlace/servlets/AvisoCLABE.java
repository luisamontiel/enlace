/*Banco Santander Mexicano
  Clase AvisoCLABE muestra la pantalla de aviso de cuentas CLABE 11 digitos
  @Autor:Rafael Rosiles Soto
  @version: 1.0
  fecha de creacion: 14 de Enero de 2004
*/

package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class AvisoCLABE extends BaseServlet {
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}


    public void defaultAction (HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
    {
		System.out.println ("-------------  entrando a avisoCLAVE.java--------------");
		EIGlobal.mensajePorTrace("*** AvisoCLABE.defaultAction entrando ***", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("plantillaOriginal = >" +(String)request.getParameter("plantillaOriginal")+ "<", EIGlobal.NivelLog.INFO);
		//
		String plantilla=(String)request.getParameter("plantillaOriginal");
		String remotehost=request.getRemoteHost();
		String protocolo=Global.PROTOCOLO;
		String web_application=Global.WEB_APPLICATION;
		//<vswf:meg cambio de NASApp por Enlace 08122008>
		String url_salir=protocolo+"s://"+remotehost+"/Enlace/"+web_application+"/"+plantilla;
		String url_conactclabe=protocolo+"s://"+remotehost+"/Enlace/"+web_application+"/ConActCLABE";
		System.out.println("La URL_SALIR       ============>"+url_salir+"<");
		System.out.println("La url_conactclabe ============>"+url_conactclabe+"<");

		//
		request.setAttribute("url_salir",url_salir);
		request.setAttribute("url_conactclabe",url_conactclabe);
		request.setAttribute("plantillaOriginal", (String)request.getParameter("plantillaOriginal"));
        evalTemplate("/jsp/AvisoCLABE.jsp", request, response);
    }
}