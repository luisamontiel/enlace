/**Banco Santander Mexicano
* Clase	ImportNominaInter, Modificacion al archivo de pago de nomina
* @Alberto Hern?ndez H.
* @version 1.2
* fecha	de creacion: Julio 2002	- Octubre 2002
* responsable: Ruben Fregoso V.
* Esta clase evalua	si el archivo es nuevo o importado,	en el primer caso, se permitira	la edici?n total del archivo
* (altas, bajas	y cambios en los registros), asi como su eliminaci?n; cuando el	archivo	sea	importado, se validar?n	todos los campos
* de todos los registros de	acuerdo	a las validaciones efectuadas en  enlace internet 2.1 (version de visual basic),
* a	continuaci?n se	determinar?	la cantidad	de registros de	detalle	existentes en el archivo, si el	resultado de esta cuenta
* es de	30 o menor,	se exhibir?n los registros en una tabla	pero no	se podr?n realizar modificaciones en ellos,	en caso	contrario,
* es decir,	cuando sean	m?s	de 30 los registros	existentes,	no se presentar?n.
* La cantidad de registros puede cambiar de	acuerdo	a los caprichos	del	usuario, por lo	cual el	n?mero de registros
* que se presentar?n "ser?n	parametizables"	como dice el Roberto
* Cuando se	permita	la edici?n de registros, se	llamar?	a la plantilla NominaDatos.html
* asm modificaci?n para aceptar cuentas CLABE de 18 posiciones (Validaci?n de archivos importados) 18-1-2005
*/

package	mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ValidaArchivoPagos;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.dao.CatNomValidaInterbDao;
import mx.altec.enlace.servicios.ValidaCuentaContrato;


import java.text.*;
import java.sql.*;
import mx.altec.enlace.bo.FormatoMoneda;
/*Inicia - EVERIS VALIDACION CSS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION CSS */

/** 
*   Isban México
*   Clase: ImportNominaInter.java
*   Descripción: Servlet utilizado en la funcionalidad de Nomina Inter.
*
*   Control de Cambios:
*   1.0 16/10/2017 Everis - Limpieza Cross Site Scripting
*/
public class ImportNominaInter extends BaseServlet {

	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
	File pathArchivoNom;
	int tamanioArchivo;
	Vector diasNoHabiles;
	//Vector diasNoHabilesJS		= new Vector();
	//Vector erroresEnElArchivo	= new Vector();
	GregorianCalendar fechaHoy2	= new GregorianCalendar();
	String[] arrayCuentas;
	CalendarNomina nominaCalendar = new CalendarNomina( );
	//instancia de ArchivoRemoto que servira para hacer la copia al servidor de web
	ArchivoRemoto archR = new ArchivoRemoto();
	ValidaArchivoPagos validaArchivo;
	EIGlobal.mensajePorTrace("***ImportNominaInter.class &Entrando al metodo defaultAction()&", EIGlobal.NivelLog.INFO);
	HttpSession sess = request.getSession();
    BaseResource session = (BaseResource) sess.getAttribute("session");
	Vector erroresEnElArchivo	= new Vector();
	archR	= new ArchivoRemoto();
	tamanioArchivo			= 0;
	String originalName;
	boolean archivoValido   = true;
	boolean warnings		= false;
	boolean fileImportado	= true;
	boolean cuentaAsociada	= true;
	boolean bandera			= true;
	boolean sesionvalida	= SesionValida( request, response );
		if ( sesionvalida ) {
				archivoValido = true;
				erroresEnElArchivo.addElement("<table>");
				request.setAttribute( "newMenu", session.getFuncionesDeMenu());
				request.setAttribute( "MenuPrincipal", session.getStrMenu());
				request.setAttribute( "Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request));

					//se obtiene valor de la fecha de hoy para ser usado como valor del campo fecha de aplicacion del registro de
					//encabezado para archivos nuevos
					String laFechaHoy = EIGlobal.formatoFecha(fechaHoy2,"aaaammdd");
					request.setAttribute("fechaHoy",laFechaHoy);
					String[][] arrayDetails;
					Vector cantidadRegistros;
					/*Inicia - EVERIS VALIDACION CSS */
					EIGlobal.mensajePorTrace("Validar CSS para el parametro [textcuenta]", EIGlobal.NivelLog.INFO);
					String cuentaCargo = UtilidadesEnlaceOwasp.cleanString(getFormParameter(request,"textcuenta"));
					/*Finaliza - EVERIS VALIDACION CSS */
					EIGlobal.mensajePorTrace("***ImportNominaInter.class &el valor de cuentaCargo&"+ cuentaCargo, EIGlobal.NivelLog.INFO);
					session.setCuentaCargo( cuentaCargo );

					/*INICIA VALIDACION FICHERO MALICIOSO*/
					if(cuentaCargo.equals("errorTipeFile")){
						EIGlobal.mensajePorTrace("Error, el archivo no es ZIP o TXT", EIGlobal.NivelLog.INFO);
						despliegaPaginaError("Archivo de Importaci&oacute;n inv&aacute;lido",request, response );	
					}
					/*FINALIZA VALIDACION FICHERO MALICIOSO*/
					//variables usadas para formar el calendario que se exhibira dentro del template, los metodos cargarDias(),
					//armaDiasInhablies(), calclafechaFin() y armaArregloFechas(), son autoria de Alejandro Rada y
					//han sido modificados por Ruben Fragoso, Rodrigo Godo tan solo ha unido estos metodos en una sola clase
					//llamada CalendarNomina.class
					String VarFechaHoy  = "";
					String strInhabiles = "";

					try
					{
					  diasNoHabiles = nominaCalendar.CargarDias();
					  strInhabiles = nominaCalendar.armaDiasInhabilesJS();
					  if(diasNoHabiles != null) {
						  diasNoHabiles.clear();
						  diasNoHabiles = null;
					  }
					}catch(ArrayIndexOutOfBoundsException e)
					 {
						EIGlobal.mensajePorTrace("*** ImportNominaInter.class Error al crear los dias inhabiles", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("*** ImportNominaInter.class MSG "+e.getMessage(), EIGlobal.NivelLog.INFO);
						strInhabiles ="";
					 }

					Calendar fechaFin   = nominaCalendar.calculaFechaFin();
					VarFechaHoy			= nominaCalendar.armaArregloFechas();
					String strFechaFin	= new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();

					//variables que se crean para almacenar el numeor de cuenta seleccionado por el usuario el crear un archivo nuevo,
					//cuando se trate de un archivo importado, se debera leer el valor del campo numero de cuenta del registro de
					//encabezado y tomarlo como valor del combo exhibido
					String Cuentas = "";
					//se colocan los putString que se usaran el el calendario
					request.setAttribute("VarFechaHoy",	 VarFechaHoy);
					request.setAttribute("fechaFin",	 strFechaFin);
					request.setAttribute("diasInhabiles",         strInhabiles);
					request.setAttribute("Fechas",		  ObtenFecha(true));
					request.setAttribute("FechaHoy",	 ObtenFecha(false) + " - 06" );
					request.setAttribute("ContenidoArchivo","");
					//se determina la hora del servidor
					String systemHour=ObtenHora();
					request.setAttribute("horaSistema", systemHour);
					//el arreglo de bytes contenidoArchivo almacenara la informacion que el archivo contenga
					//esto es asi debido a que cuando se crea el archivo en el servidor WebServer, este se encuentra
					//vacio, asi que se debe escribir en el la informacion original
					byte contenidoArchivo[]     = new byte[1];
					String contenidoArchivoStr	= "";
					String operacion			= getFormParameter(request,"operacion");
					String nomArchNomina	    = "";
					String nombreOriginal       = "";

					if (operacion.equals("null"))
					    operacion = "";

					try{
						if(operacion.equals("nuevo")){//el archivo no existe, se almacenara en el servidor
							nombreOriginal  = getFormParameter(request,"nuevoArchivo");
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &El nombre del nuevo archivo es: "+nombreOriginal+"&", EIGlobal.NivelLog.INFO);
					}
					else {
						//se obtiene la ruta original del archivo
						nombreOriginal  = getFormParameter(request, "fileName");//getFormParameter(request,"MantNomina_file");
						//se obtiene el contenido del archivo
						contenidoArchivo = getFileInBytes( request );
					}
				}
				catch(Exception e) {
					log( "ImportNominaInter.java::defaultAction()::try:Recibe Archivo" );
					e.printStackTrace();
				}

				//de la ruta del archivo, se obtiene solo el nombre
				nombreOriginal = nombreOriginal.substring( nombreOriginal.lastIndexOf( '\\' ) + 1 );
				originalName = nombreOriginal;

				//cuando el archivo sea importado, se eliminara su extension
				if( originalName.endsWith( "txt" ) || originalName.endsWith( "TXT" ))
					nombreOriginal= nombreOriginal.substring(0,nombreOriginal.length()-4);

				// Para reconocer los archivos pertenecientes a este modulo se
				// les asignara la extension .nom
				nombreOriginal = nombreOriginal + ".nom";
				nomArchNomina  = "" + Global.DIRECTORIO_LOCAL + "/" + nombreOriginal;
				log( "ImportNominaInter.java::defaultAction()::nomArchNomina = " + nomArchNomina );
				//Si ya existe el archivo lo borra
				try{
					File archivo = new File( nomArchNomina );
					if (archivo.exists()) archivo.delete();
				}
				catch(Exception e) {
					EIGlobal.mensajePorTrace("***ImportNominaInter.class Ha ocurrido una excepcion en: %execute()"+e, EIGlobal.NivelLog.INFO);
				}

				try{
					// Se evalua si el nombre original(antes de eliminar la extension y sustituirla)
					// terminaba o no con .txt si es verdadera esta condicion el archivo es importado
					if ( ! (originalName.endsWith( "txt" ) || originalName.endsWith( "TXT" )) )
					 {
						cuentaAsociada=true;
						//el archivo no existe, es nuevo
						nomArchNomina="";
						GregorianCalendar fechaHoy= new GregorianCalendar();
						String fechaGenera = EIGlobal.formatoFecha(fechaHoy,"mmddaaaa");
						String fechaAplicacion = getFormParameter(request, "txtFechaLimite" );
						fechaAplicacion = fechaAplicacion.replace( '/', ' ' );
						fechaAplicacion = fechaAplicacion.substring( 3, 5 ) +
						fechaAplicacion.substring( 0, 2 ) +
						fechaAplicacion.substring( 6, fechaAplicacion.length() );
						String encabezado = "100001E" + fechaGenera + cuentaCargo  + "     " + fechaAplicacion + "\r\n";
						//instancia del string usado como registro sumario
						String sumario = "30000200000000000000000000000\r\n";
						//el archivo toma la ruta del servidor en donde ser? almacenado
						nomArchNomina = "" + Global.DIRECTORIO_LOCAL + "/" + originalName + ".nom";
						pathArchivoNom = new File( nomArchNomina );
						RandomAccessFile archivoNomina = new RandomAccessFile( pathArchivoNom, "rw" );
						archivoNomina.setLength(0); //para truncar el archivo en caso que exista
						archivoNomina.writeBytes( encabezado + sumario );
						sess.setAttribute("nombreArchivo",nomArchNomina);
						sess.setAttribute("numeroEmp","");
						session.setCuentasArchivo( "" );
						// Se construye un objeto ValidaArchivoPagos para usar sus metodos en la
						// importacion de archivos
						validaArchivo = new ValidaArchivoPagos(this, nomArchNomina);
						String nombreArchivoPagoNomina = nomArchNomina.substring(
							nomArchNomina.lastIndexOf( "/" ) + 1 );

						//se copia el archivo creado al servidor web, validar la copia
						if ( archR.copiaLocalARemoto( nombreArchivoPagoNomina ) ) { //, "WEB" ) )
							session.setRutaDescarga( "/Download/" + nombreArchivoPagoNomina );
						//la cuenta que el usuario ha seleccionado permanecera a lo largo de la sesion para que cuando el
						//archivo este listo para hacer el envio la cuenta que se vaya como cuenta de cargo sea esta
						//request.setAttribute("Cuentas",	"<Option value = " + session.getCuentaCargo() + ">" +session.getCuentaCargo() );
						request.setAttribute("textcuenta",	cuentaCargo );
						request.setAttribute( "archivo_actual",nomArchNomina.substring( nomArchNomina.lastIndexOf( "/" ) + 1 ) );
						request.setAttribute("DescargaArchivo",session.getRutaDescarga());
						archivoNomina.close();
						}
						else {
							despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP ,"Pago de N&oacute;mina Interbancaria", "Importando Archivo", "s25800h",request, response );
							bandera = false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &No se ha podido realizar la copia de doble remut", EIGlobal.NivelLog.INFO);
						}
					}
				   else
					{
						//el archivo se esta importando
						fileImportado  = true;
						pathArchivoNom = new File( nomArchNomina );
						RandomAccessFile archivoNomina = new RandomAccessFile( pathArchivoNom, "rw" );
						tamanioArchivo = contenidoArchivo.length;
						archivoNomina.write( contenidoArchivo, 0, tamanioArchivo );
						sess.setAttribute("nombreArchivo",nomArchNomina);
						validaArchivo  = new ValidaArchivoPagos( this, nomArchNomina );
						String nombreArchivoPagoNomina = nomArchNomina.substring(
						nomArchNomina.lastIndexOf( "/" ) + 1 );
						archivoNomina.seek(0);
						String primeralinea=archivoNomina.readLine();
						ValidaCuentaContrato valCtas = new ValidaCuentaContrato();
						//cuenta del archivo
						if (primeralinea == null || primeralinea.length() != 39) {
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">La longitud del encabezado debe ser 39.</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &La longitud del encabezado debe ser 39&", EIGlobal.NivelLog.INFO);
						}
						else {
							cuentaCargo = primeralinea.substring( 15, 31 );
							cuentaCargo = cuentaCargo.trim();
							EIGlobal.mensajePorTrace("***ImportNominaInter.class valor de cuentaCargo&"+ cuentaCargo, EIGlobal.NivelLog.INFO);

						   /* Para la cuenta cargo */
						   llamado_servicioCtasInteg(IEnlace.MEnvio_pagos_nom_IN," "," "," ",session.getUserID8()+".ambci",request);  
						   /* Para cuentas abono */
						   llamado_servicioCtasInteg(IEnlace.MDep_Inter_Abono," "," "," ",session.getUserID8()+".ambca",request); 

						   String datoscta[]	= null;
						   //datoscta=BuscandoCtaInteg(cuentaCargo,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambci",IEnlace.MEnvio_pagos_nom_IN);
						   datoscta=valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
																  session.getUserID8(),  
														          session.getUserProfile(),
														          cuentaCargo,
														          IEnlace.MCargo_transf_pesos);
						   if(datoscta==null)
							 cuentaAsociada	= false;
						   else
							 cuentaAsociada = true;

                                                        valCtas.closeTransaction();

							session.setCuentaCargo(cuentaCargo);
							session.setCuentasArchivo("");
							sess.setAttribute("numeroEmp","");
							archivoNomina.close();

							if ( !cuentaAsociada ){
								despliegaPaginaError( "Imposible Importar el archivo seleccionado, la cuenta  (" + cuentaCargo + ")  no existe en el contrato o no est&aacute; disponible para el servicio de n&oacute;mina.",
										"Pago de N&oacute;mina", "Importando Archivo", "s25800h",request, response );
										session.setCuentaCargo(" ");
								bandera = false;
								archivoValido=false;
								}
						}
					}

					/*
					* Hasta esta linea, se ha creado el archivo( nuevo o importado ),
					* se procedera a conocer el numero de registros
					* de detalle que contiene, en caso de que sean 30 o menos se exhibir?n
					*/

					// Se invoca al metodo registrosEnArchivo() para DETERMINAR cual es la
					// CANTIDAD DE REGISTROS EXISTENTES EN EL ARCHIVO
			if (bandera)
				{
					RandomAccessFile archivo = new RandomAccessFile( pathArchivoNom, "rw");
					int registros			 = registrosEnArchivo( archivo );
					int cuentaDeRegistros	 = registros;
					String numerosEmpleados[]= arregloNumeroEmpleado( pathArchivoNom, registros - 2 );
					String cuentasAbono[]	 = arregloCuentas( pathArchivoNom, registros - 2 );
					int registrosLeidos		 = 0;
					registrosLeidos			 = cuentaDeRegistros;
					String fechaAplicacion   = getFormParameter(request, "fechaApli" );
					session.setFechaAplicacion( fechaAplicacion );
					archivo.close();
					//FIN DETERMINA NUMERO DE REGISTROS

					//RandomAccessFile arch = new RandomAccessFile( pathArchivoNom, "rw" );
					BufferedReader arch	= new BufferedReader( new FileReader( pathArchivoNom ) );
					String registroEnc = arch.readLine();
					String encTipoReg = new String( registroEnc.substring( 0, 1 ) );
					if ( validaArchivo.validaDigito( encTipoReg ) == false || validaArchivo.validaEncTipoReg( encTipoReg ) == false ) {
						erroresEnElArchivo.addElement( "<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el primer Caracter del encabezado, se esperaba 1</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el Tipo registro encabezado&", EIGlobal.NivelLog.INFO);
					}
					//c***
					String encNumSec = "";
					if(registroEnc != null && registroEnc.length() > 0 && registroEnc.length() >= 6){
						encNumSec = new String( registroEnc.substring( 1, 6 ) );
					}
					
					if ( validaArchivo.validaDigito( encNumSec ) == false ||validaArchivo.validaPrimerNumSec( encNumSec ) == false ) {
					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el numero secuencial del encabezado, se esperaba 1</td>");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en Numero secuencial del encabezado&", EIGlobal.NivelLog.INFO);
					}
					
					String encSentido = "";
					if(registroEnc != null && registroEnc.length() >= 7){
						encSentido = new String(registroEnc.substring(6,7));
					}
					//String encSentido = new String(registroEnc.substring(6,7));
					if (validaArchivo.validaLetras(encSentido)==false||validaArchivo.validaSentido(encSentido)==false){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el caracter de sentido del encabezado, se esperaba E</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el sentido del encabezado, se esperaba 'E'&", EIGlobal.NivelLog.INFO);
					}
	//					String encFechGen = new String(registroEnc.substring(registroEnc.length()- 8,registroEnc.length()));
	
					String encFechGen = "";
					if(registroEnc != null && registroEnc.length() >= 15){
						encFechGen = new String(registroEnc.substring(7,15));
					}
					//String encFechGen = new String(registroEnc.substring(7,15));
					if (validaArchivo.validaDigito(encFechGen)==false ||validaArchivo.fechaValida(encFechGen)==false){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de generacion del archivo</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en la fecha de generacion del archivo&", EIGlobal.NivelLog.INFO);
					}
					// La cuenta ya no se verifica pues al momento de formar el arreglo de
					// cuentas, se ha verificado que exista y que sea propia y/o de terceros
					// clabe
					String encCuenta = "";
					String encFechApli = "";
					if (registroEnc.length() >= 31 )
						 encCuenta = registroEnc.substring( 15,31 );
						 else encCuenta = "";

					EIGlobal.mensajePorTrace("***ImportNominaInter.class Longitud: "+registroEnc.length(), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***ImportNominaInter.class Datos: <"+registroEnc+">", EIGlobal.NivelLog.INFO);
					if (registroEnc.length() >= 39 )
					  {
						 encFechApli = new String(registroEnc.substring(registroEnc.length() - 8,registroEnc.length()));
						if (validaArchivo.validaDigito(encFechApli)==false||validaArchivo.fechaValida(encFechApli)==false)
						  {
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de aplicacion del archivo</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en la fecha de aplicacion&", EIGlobal.NivelLog.INFO);
						  }
					  }
					 else
					  {
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de aplicaci&oacute;n del archivo.No existe</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en la fecha de aplicacion no tiene fecha&", EIGlobal.NivelLog.INFO);
					  }
					session.setFechaAplicacion( encFechApli );
					String registroEncabezadoCompleto;

					registroEncabezadoCompleto = encTipoReg + encNumSec+encSentido +
							encFechGen + encCuenta + encFechApli;
					int longTotalEncabezado = registroEncabezadoCompleto.length();

					/* Detalle del archivo */
					cantidadRegistros = new Vector();
					if ( encNumSec != null && encNumSec.length() > 0) {
						cantidadRegistros.addElement( encNumSec );
					}

					int longitudArreglo = cuentaDeRegistros - 2;
					// La longitud del arreglo esta determinada por la cantidad de
					// registros de ddetalle existentes en el archivo de pago de nomina
					if ( longitudArreglo < 0 )
						longitudArreglo = 0;

					String longitudArray = Integer.toString( longitudArreglo );
					//el metodo setNominaNumberOfRecords() devuelve el numero de registros de detalle en el archivo
					session.setNominaNumberOfRecords( longitudArray );
					arrayDetails = new String[longitudArreglo][11];
					int longTotalDetalle;
					longTotalDetalle = 0;
					double sumImporte       = 0;

					if(!operacion.equals("nuevo")){//el archivo no existe, se almacenara en el servidor
					String registroDet		= "";
					String tipoCuenta		= "";
					int tamCuenta = 0;
					String tipoRegistro		= "";
			        String NumeroSecuencia  = "";
					String NombreCompleto   = "";
			        String NoUtilizado      = "";
					String NumeroCuenta     = "";
					String Importe          = "";
		        	String ClaveBanco       = "";
				    String PlazaBanxico     = "";
				    String Concepto			= "";
				    String Referencia		= "";
					double num				= 0;
					String cveBancoCecoban  = ""; //PPM

					CatNomValidaInterbDao dao = new CatNomValidaInterbDao();
					try {
						dao.Crear();
					} catch(Exception e) {}

					for (int i= 0; i < arrayDetails.length; i++) {
					try{
						try{
						registroDet		= arch.readLine();

						if(registroDet==null) //jtg   si es nulo no puedo obtener los datos
						continue;
						if(registroDet.length()>=109)  //jtg  Para evitar el string index out of bounds
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &No hay problema la linea es igual a 109  caracteres  \n"+registroDet.length(), EIGlobal.NivelLog.INFO);
						else{
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el formato del Registro </td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);
							continue;
							}
						 cveBancoCecoban  = ""; //PPM
		       			 tipoRegistro		= new String (registroDet.substring(0,1));
				         NumeroSecuencia  = new String (registroDet.substring(1,6));
       					 NombreCompleto   = new String (registroDet.substring(6,56));
			             NoUtilizado      = new String (registroDet.substring(56,61));

      					 NumeroCuenta     = new String (registroDet.substring(61,81));
						 Importe          = new String (registroDet.substring(81,99));
		        		 ClaveBanco       = new String (registroDet.substring(99,104));
				         PlazaBanxico     = new String (registroDet.substring(104,109));
                         if(registroDet.length()>109)
						   {
							 Concepto		  = new String (registroDet.substring(109, 149));
							 Referencia		  = new String (registroDet.substring(149, 156));
                           }
                          else
						   {
                             Concepto = "";
                             Referencia = "";
                           }
						 num = Double.parseDouble(Importe);
						 sumImporte = sumImporte + num;
						} catch(Exception e) {
						e.printStackTrace();
						}
		        	    arrayDetails[i][0]=tipoRegistro;
				        if (validaArchivo.validaDigito(arrayDetails[i][0])==false||validaArchivo.validaDetalleTipoReg(arrayDetails[i][0])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el tipo de Registro del detalle</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);
						}

						arrayDetails[i][1]=NumeroSecuencia.trim();
					    if (validaArchivo.validaDigito(arrayDetails[i][1])==false||validaArchivo.validaNumSec(i, arrayDetails[i][1])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero Secuencial del Detalle</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el Numero Secuencial del Detalle&", EIGlobal.NivelLog.INFO);
		                }

						arrayDetails[i][2]=NombreCompleto.trim();
						if (arrayDetails[i][2].equals(null)||validaArchivo.validaNombres(arrayDetails[i][2])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Nombre del Empleado</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el nombre completo&", EIGlobal.NivelLog.INFO);
						}

						if (arrayDetails[i][2].equals("")){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error el Nombre del Empleado es obligatorio</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el nombre completo&", EIGlobal.NivelLog.INFO);
						}
						arrayDetails[i][4]=NumeroCuenta.trim();
						//clabe ***
						// 01 = cheques , 40 clabe , 02 tarjeta
						tipoCuenta = 	NoUtilizado.trim();
						 if (tipoCuenta.equals( "01" ) )
						     tamCuenta = 11;
							 else if (tipoCuenta.equals( "40") )
							          tamCuenta = 18;
									  else if (tipoCuenta.equals("02") )
									           tamCuenta = 16;

						 /**
						 *	 Verificar que la cuenta sea igual que la cuenta de cargo, pesos o dolares
						 */
						 if(validaArchivo.esCuentaDeDolares(cuentaCargo))
						  {
							if(!validaArchivo.esCuentaDeDolares(arrayDetails[i][4].trim()))
							 {
							   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">La cuenta Abono debe ser cuenta en Dolares</td>");
							   archivoValido=false;
							 }
						  }
						 else
						  {
							if(validaArchivo.esCuentaDeDolares(arrayDetails[i][4].trim()))
							 {
							   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">La cuenta Abono debe ser cuenta en Pesos</td>");
							   archivoValido=false;
							 }
						  }

						if (validaArchivo.validaDigito(arrayDetails[i][4])==false||
							validaArchivo.verificaDuplicidadCuentas(cuentasAbono, arrayDetails[i][4]))
                        {
						   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta 1</td>");
					       archivoValido=false;
						   EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);
						}
						if(tipoCuenta.equals("02")){
							if(arrayDetails[i][4].trim().length()!=16){
								erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Cuenta inv&aacute;lida, la cuenta debe ser de 16 posiciones </td></tr>");
								archivoValido=false;
								EIGlobal.mensajePorTrace("***ImportNominaInter.class &Cuenta inv&aacute;lida,  para tipo 02 la cuenta debe ser de 16 posiciones.", EIGlobal.NivelLog.INFO);
							}
						}else{
							if(ClaveBanco.equals("BANME") || ClaveBanco.equals("SERFI")){
								if(!(arrayDetails[i][4].trim().length()==11 || arrayDetails[i][4].trim().length()==18) ){
									erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Cuenta inv&aacute;lida, la cuenta debe ser de 11 posiciones </td></tr>");
									archivoValido=false;
									EIGlobal.mensajePorTrace("***ImportNominaInter.class &Cuenta inv&aacute;lida,  la cuenta debe ser de 11 posiciones.", EIGlobal.NivelLog.INFO);
								}

								if(arrayDetails[i][4].trim().length()==18){
										cveBancoCecoban=traeClaveBanco(ClaveBanco);
										System.out.println("La clave del banco CECOBAN es ......" + cveBancoCecoban);
										if(!(cveBancoCecoban.equals(arrayDetails[i][4].trim().substring(0,3)))){
										   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error La Cuenta/CLABE no coincide con el banco seleccionado </td></tr>");
										   archivoValido=false;
										   EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error La Cuenta/CLABE no coincide con el banco seleccionados", EIGlobal.NivelLog.INFO);
										}else{
											if(!(validaDigitoVerificador(arrayDetails[i][4].trim(), cveBancoCecoban))){
												erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error El d&iacute;gito verificador de la cuenta/CLABE no es v&aacute;lido</td></tr>");
											   archivoValido=false;
											   EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el d&iacute;gito verificador de la cuenta/CLABE no es v&aacute;lido&", EIGlobal.NivelLog.INFO);
											}
										}
	                              }
							}else{
								if(arrayDetails[i][4].trim().length()==18){
									cveBancoCecoban=traeClaveBanco(ClaveBanco);
									System.out.println("La clave del banco CECOBAN es ......" + cveBancoCecoban);
									if(!(cveBancoCecoban.equals(arrayDetails[i][4].trim().substring(0,3)))){
									   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error La Cuenta/CLABE no coincide con el banco seleccionado </td></tr>");
									   archivoValido=false;
									   EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error La Cuenta/CLABE no coincide con el banco seleccionados", EIGlobal.NivelLog.INFO);
									}else{
										if(!(validaDigitoVerificador(arrayDetails[i][4].trim(), cveBancoCecoban))){
											erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error El d&iacute;gito verificador de la cuenta/CLABE no es v&aacute;lido</td></tr>");
										   archivoValido=false;
										   EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el d&iacute;gito verificador de la cuenta/CLABE no es v&aacute;lido&", EIGlobal.NivelLog.INFO);
										}
									}
								}else{
									erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Cuenta inv&aacute;lida,  la cuenta debe ser de 18 posiciones </td></tr>");
									archivoValido=false;
									EIGlobal.mensajePorTrace("***ImportNominaInter.class &Cuenta inv&aacute;lida,  la cuenta debe ser de 18 posiciones.", EIGlobal.NivelLog.INFO);
								}
							}
						}

						EIGlobal.mensajePorTrace("***ImportNominaInter &Antes de validaArchivo.validaDigitoCuenta &"+arrayDetails[i][4].trim().length(), EIGlobal.NivelLog.INFO);

						if(arrayDetails[i][4].trim().length()==11 ){
							if ( (session.getClaveBanco().equals(Global.CLAVE_SANTANDER))&&
								 (ClaveBanco.equals("BANME"))&&
								 (validaArchivo.validaDigitoCuenta(arrayDetails[i][4])==false))
							{
								   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td></tr>");
								   archivoValido=false;
								   EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);

							}
							if ( (session.getClaveBanco().equals(Global.CLAVE_SERFIN))&&
								 (ClaveBanco.equals("SERFI")) )
							{
								if (validaArchivo.validaDigitoCuenta(arrayDetails[i][4])==false)
								{
									if (validaDigitoCuentaHogan(arrayDetails[i][4])==false)
									{
										erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");
										archivoValido=false;
										EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);
									}
								}
							}
						}

						else if(arrayDetails[i][4].trim().length()==18){
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Antes de validaArchivo.validaDigitoCuenta length18  &"+  (arrayDetails[i][4]).substring(6,17), EIGlobal.NivelLog.INFO);

                                                        EIGlobal.mensajePorTrace("***ImportNominaInter.class NVS session = "+ session, EIGlobal.NivelLog.INFO);
                                                        EIGlobal.mensajePorTrace("***ImportNominaInter.class NVS session.getClaveBanco() = "+ session.getClaveBanco(), EIGlobal.NivelLog.INFO);
							if ( (session.getClaveBanco().equals(Global.CLAVE_SANTANDER))&&
								 (ClaveBanco.equals("BANME"))&&
								 (validaArchivo.validaDigitoCuenta((arrayDetails[i][4]).substring(6,17))==false))
							{
								   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td></tr>");
								   archivoValido=false;
								   EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);

							}
							if ( (session.getClaveBanco().equals(Global.CLAVE_SERFIN))&&
								 (ClaveBanco.equals("SERFI")) )
							{
								if (validaArchivo.validaDigitoCuenta((arrayDetails[i][4].substring(6,17)))==false)
								{
									if (validaDigitoCuentaHogan((arrayDetails[i][4].substring(6,17)))==false)
									{
										erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");
										archivoValido=false;
										EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);
									}
								}
							}
						}

						/**
						* Facultad para cuentas no registradas
						* Busca la cuenta de cargo en las registradas
						* y verificar la facultad a cuentas no registradas
						*/
						Hashtable Facultades=(Hashtable)sess.getAttribute("FacultadesNominaInterbancaria");
						EIGlobal.mensajePorTrace("***InicioNominaInter.class Facultades se obtienen: " + Facultades.size(), EIGlobal.NivelLog.INFO);
						boolean Acceso=((Boolean)Facultades.get("CuentasNoReg")).booleanValue();
						EIGlobal.mensajePorTrace("***InicioNominaInter.class Acceso a cuentas no registradas:  " + Acceso, EIGlobal.NivelLog.INFO);



						if(!dao.existeEnCatalogo(session.getContractNumber(), arrayDetails[i][4].trim()) && !Acceso) {

							//String datoscta[]=null;
							//datoscta=BuscandoCtaInteg(arrayDetails[i][4].trim(),IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambca",IEnlace.MDep_Inter_Abono);
							//EIGlobal.mensajePorTrace("***InicioNominaInter.class CCBDatoscta = " + datoscta, EIGlobal.NivelLog.INFO);

							EIGlobal.mensajePorTrace("***InicioNominaInter.class La cuenta de Abono No está registrada", EIGlobal.NivelLog.INFO);
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">La cuenta de abono no est&aacute; registrada.</td></tr>");
							archivoValido=false;

							/*if(datoscta==null) {
								EIGlobal.mensajePorTrace("***InicioNominaInter.class La cuenta de Abono No está registrada", EIGlobal.NivelLog.INFO);
								erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">La cuenta de abono no est&aacute; registrada.</td></tr>");
								archivoValido=false;
							}
							else {
								EIGlobal.mensajePorTrace("***InicioNominaInter.class La cuenta de Abono No Existe y no se tiene facultad.", EIGlobal.NivelLog.INFO);
								erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Favor de registrar la cuenta de abono en el Cat&aacute;logo de Empleados.</td></tr>");
								warnings = true;
							}*/

							//EIGlobal.mensajePorTrace("***ImportNominaInter &Despues de validaArchivo.validaDigitoCuenta&", EIGlobal.NivelLog.INFO);
						}


                        //***************fin de modificacion integracion
						arrayDetails[i][5]=Importe;
						if (validaArchivo.validaDigito(arrayDetails[i][5])==false||arrayDetails[i][5].endsWith(" ")){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe</td>");
							archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el importe&", EIGlobal.NivelLog.INFO);
		                }

						arrayDetails[i][6]=ClaveBanco;
						if (arrayDetails[i][6].equals(null)||validaArchivo.validaNombres(arrayDetails[i][6])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la Clave del Banco</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el nombre&", EIGlobal.NivelLog.INFO);
						}

						arrayDetails[i][7]=PlazaBanxico;
//						if (arrayDetails[i][7].equals(null)||validaArchivo.validaNombres(arrayDetails[i][7])==false)
						if (validaArchivo.validaDigito(arrayDetails[i][7])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la Plaza Banxico</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el importe&", EIGlobal.NivelLog.INFO);
						}

						//Q05-0044906 - Praxis NVS - 051117
						arrayDetails[i][8]=Concepto;
						if (validaArchivo.validaConcepto(arrayDetails[i][8])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Concepto</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el concepto&", EIGlobal.NivelLog.INFO);
						}

						arrayDetails[i][9] = Referencia;
						if (validaArchivo.validaDigito(arrayDetails[i][9])==false){
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Concepto</td>");
						    archivoValido=false;
							EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el concepto&", EIGlobal.NivelLog.INFO);
						}

			            cantidadRegistros.addElement(arrayDetails[i][1]);
					    String registroDetalleCompleto;
		                registroDetalleCompleto=arrayDetails[i][0]+arrayDetails[i][1]+arrayDetails[i][2]+arrayDetails[i][3]+arrayDetails[i][4]+arrayDetails[i][5]+arrayDetails[i][6]+arrayDetails[i][7];
						longTotalDetalle= registroDetalleCompleto.length();
						}
    			  catch(Exception e)
                  {
   					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo o un campo esta vacio</td>");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
                  }

				}// del for

				dao.Cerrar();
			} // del if
					double sumTotal = 0 ;
				//Registro Sumario
					String registroSum = arch.readLine();
					String sumTipoReg  = "";
					String sumNumSec   = "";
					String sumTotRegs  = "";
					String sumImpTotal = "";
                try
				{
					 sumTipoReg = new String(registroSum.substring(0,1));
					if (validaArchivo.validaDigito(sumTipoReg)==false||validaArchivo.validaSumTipoReg(sumTipoReg)==false){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el numero de registro</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el numero de registro del sumario&", EIGlobal.NivelLog.INFO);
					}
					 sumNumSec= new String(registroSum.substring(1,6));
					if(validaArchivo.validaDigito(sumNumSec) && validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)){
						session.setLastSecNom(sumNumSec);
					}
					if (validaArchivo.validaDigito(sumNumSec)==false||validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)==false){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el numero secuencial del sumario</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el numero secuencial del sumario&", EIGlobal.NivelLog.INFO);
					}
					 sumTotRegs = new String(registroSum.substring(6,11));
					if(validaArchivo.validaDigito(sumTotRegs) && validaArchivo.validaTotalDeRegistros(sumTotRegs, cuentaDeRegistros)){
						session.setTotalRegsNom(sumTotRegs);
					}
					if (validaArchivo.validaDigito(sumTotRegs)==false||validaArchivo.validaTotalDeRegistros(sumTotRegs, cuentaDeRegistros)==false){
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el el total de registros del sumario</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el total de registros&", EIGlobal.NivelLog.INFO);
					}
					sumImpTotal = new String(registroSum.substring(11,29));
					if (validaArchivo.validaDigito(sumImpTotal)){
						session.setImpTotalNom(sumImpTotal);
					}
					sumTotal = Double.parseDouble(sumImpTotal);
					if (sumTotal != sumImporte){
					  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe total </td>");
					  archivoValido=false;
					  EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error en el Importe total&", EIGlobal.NivelLog.INFO);
					}
				  }
 				catch(Exception e)
                {
   					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");
					archivoValido=false;
					EIGlobal.mensajePorTrace("***ImportNominaInter.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
                }
					erroresEnElArchivo.addElement("</table>");
					erroresEnElArchivo.trimToSize();//a partir de esta linea el vector erroresEnElArchivo contiene la tabla de errores
					String registroSumarioCompleto;
					registroSumarioCompleto=sumTipoReg+sumNumSec+sumTotRegs+sumImpTotal;
					int longTotalSumario;
					longTotalSumario = registroSumarioCompleto.length();
					arch.close();

					cantidadRegistros.addElement(sumNumSec);
					int total;
					total = cantidadRegistros.size();
					if(cantidadRegistros != null) {
						cantidadRegistros.clear();
						cantidadRegistros = null;
					}
					// Aqui finaliza la lectura y validacion de los campos del archivo
					// es necesario determinar si se exhibiran o no los registros de detalle, solo se exhibiran cuando sean menos de
					// 30 registros y estos sean validos

					if(Integer.parseInt(session.getNominaNumberOfRecords())<=30){

			   			if (archivoValido){
						contenidoArchivoStr="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
									        "<tr> "+
											"<td class=\"textabref\">Pago de N&oacute;mina Interbancaria</td>"+
											"</tr>"+
											"</table>"+
											"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
											"<tr> "+
											"<td align=\"center\" class=\"tittabdat\" width=\"50\">Seleccione </td>"+
											"<td width=\"55\" class=\"tittabdat\" align=\"center\">Nombre Completo</td>"+
											"<td width=\"70\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Cuenta</td>"+
											"<td width=\"70\" class=\"tittabdat\" align=\"center\">Importe</td>"+
											"<td width=\"100\" class=\"tittabdat\" align=\"center\">Clave del Banco</td>"+
											"<td width=\"70\" class=\"tittabdat\" align=\"center\">Plaza Banxico</td>"+
											"<td width=\"70\" class=\"tittabdat\" align=\"center\">Concepto</td>"+
											"<td width=\"70\" class=\"tittabdat\" align=\"center\">Referencia Interbancaria</td>"+
											"</tr>";

						int contador=1;
						int residuo=0;

						for( int i=0; i<arrayDetails.length; i++){
							long posicionReg=i+1;

							residuo=contador % 2 ;
							String bgcolor="textabdatobs";
							if (residuo == 0){
								bgcolor="textabdatcla";
							}
                        String importe1 = FormatoMoneda.formateaMoneda(new Double(arrayDetails[i][5]).doubleValue() / 100);
                        importe1 = importe1.substring(1, importe1.length());
						contenidoArchivoStr=contenidoArchivoStr+"<tr><td class=\""+bgcolor+"\" align=\"center\" nowrap><input type=radio name=seleccionaRegs value="+posicionReg+"></td>";
			    		contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+arrayDetails[i][2]+"&nbsp;</td>";
						contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+arrayDetails[i][4]+"&nbsp;</td>";
						contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"right\">"+importe1+"&nbsp;</td>";
						contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+arrayDetails[i][6]+"&nbsp;</td>";
						contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+arrayDetails[i][7]+"&nbsp;</td>";
						contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+arrayDetails[i][8]+"&nbsp;</td>";
						contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+arrayDetails[i][9]+"&nbsp;</td>";

						contador++;
					}
					//contenidoArchivo es la variable que forma la tablaHTML en el template

					contenidoArchivoStr=contenidoArchivoStr+"</table>";
					} // ArchvioValido
				}

				else if( Integer.parseInt(session.getNominaNumberOfRecords() ) > 30 ) {

					contenidoArchivoStr="<table width=\"320\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">";
					contenidoArchivoStr=contenidoArchivoStr+"<tr align=\"center\"><td class=\"tittabdat\" align=\"center\">Pago de Nomina Interbancaria</td>";
					contenidoArchivoStr=contenidoArchivoStr+"<tr><td class=\"textabdatobs\" nowrap align=\"center\">Use una aplicacion local para ver y editar el archivo</td>";
					contenidoArchivoStr=contenidoArchivoStr+"</table>";

				}
		} //bandera
		   if ( operacion.equals( "nuevo" ) )
			 {
				EIGlobal.mensajePorTrace("***ImportNominaInter.class &El archivo es nuevo, se puede ejecutar cualquier operacion en el&", EIGlobal.NivelLog.INFO);
				request.setAttribute("tipoArchivo","nuevo");
			  }
		   else if ( operacion.equals( "importar" ) )
			  {
                 EIGlobal.mensajePorTrace("***ImportNominaInter.class ##El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);
				 request.setAttribute("tipoArchivo","importado");
			  }
			} catch( IOException e ) {
				//stem.out.println("Error creando archivo de empleados de nomina" + e.getMessage());
				EIGlobal.mensajePorTrace("***ImportNominaInter.class Ha ocurrido una excepcion creando archivo de empleados de nomina: %execute()"+e, EIGlobal.NivelLog.INFO);
			}
			String descargaArchivo = "";

			try {
				if ( originalName.substring( originalName.length() - 4,
						originalName.length() - 3 ).equals( "." ) )
			        originalName = originalName.substring( 0, originalName.length() - 4 ) + ".nom";
				else
					originalName = originalName + ".nom";
			}
			catch( Exception e ) {
				EIGlobal.mensajePorTrace("***ImportNominaInter.class Ha ocurrido una excepcion al agregar extension al archivo: %execute()"+e, EIGlobal.NivelLog.INFO);
				originalName = originalName+".nom";
			}
			descargaArchivo="document.location='"+"/Download/" + originalName + "'";
			String archivoName = "";
			archivoName = (String) sess.getAttribute("nombreArchivo");
	        session.setModuloConsultar(IEnlace.MEnvio_pagos_nom_IN);
		if (bandera)
			{
			if ( archivoValido ) {
					if ( fileImportado && cuentaAsociada ) {

						/*if(erroresEnElArchivo.size() > 0) {
							String tablaErrores="";
							for(int z=0; z<erroresEnElArchivo.size(); z++){
								tablaErrores=tablaErrores+erroresEnElArchivo.elementAt(z);
							//request.setAttribute("archivo_actual","");
							}

							request.setAttribute("archivoEstatus",tablaErrores);
							request.setAttribute("ArchivoErr","archivoErrores();");
						}*/



						if ( archR.copiaLocalARemoto( archivoName.substring(archivoName.lastIndexOf("/") + 1 ))) {
							session.setRutaDescarga( "/Download/" + ( archivoName.substring(archivoName.lastIndexOf("/") + 1 ) ) );
						} else {
						EIGlobal.mensajePorTrace("***ImportNominaInter.class No se ha podido realizar la copia de doble remut", EIGlobal.NivelLog.INFO);
						}
						request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
						request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr);
						request.setAttribute("DescargaArchivo",session.getRutaDescarga());
						request.setAttribute("textcuenta",cuentaCargo);

						String msg="";
						if ( operacion.equals( "importar" ) ) {


							if(!warnings) {
								msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros\", 1)";
								EIGlobal.mensajePorTrace("***ImportNominaInter.class %El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);
							}
							else {
								String tablaErrores="";
								String detErrores = "Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros.";
								for(int z=0; z<erroresEnElArchivo.size(); z++){
									tablaErrores=tablaErrores+erroresEnElArchivo.elementAt(z);
								}

								request.setAttribute("archivoEstatus",tablaErrores);
								request.setAttribute("detErrores",detErrores);
								request.setAttribute("ArchivoErr","archivoWarnings();");
							}


						}
						request.setAttribute("infoImportados",msg);
						if ( operacion.equals( "importar" ) ) {
						request.setAttribute("cantidadDeRegistrosEnArchivo",session.getNominaNumberOfRecords());
						}else {
						      request.setAttribute("cantidadDeRegistrosEnArchivo","");
  						 	  EIGlobal.mensajePorTrace("***ImportNominaInter.class %El archivo es nuevo, se puede ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);
							}
					}
				if(cuentaAsociada)
					evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
			} else {
				String msg="";
				String tablaErrores="";
				for(int z=0; z<erroresEnElArchivo.size(); z++){
					tablaErrores=tablaErrores+erroresEnElArchivo.elementAt(z);
				request.setAttribute("archivo_actual","");
				}

			    request.setAttribute("archivoEstatus",tablaErrores);
				request.setAttribute("ArchivoErr","archivoErrores();");

				EIGlobal.mensajePorTrace("#####################################################", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***ImportNominaInter.class Se procesan los resultados", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("#####################################################", EIGlobal.NivelLog.INFO);

				/**
				* Veriricar nombre de archivo
				request.setAttribute("archivo_actual","");
				request.setAttribute("archivo_actual","Error al Importar: "+archivoName.substring(archivoName.lastIndexOf("/")+1));
				*
				*/

				request.setAttribute("ContenidoArchivo","");
				request.setAttribute("tipoArchivo","importadoErroneo");
				request.setAttribute("cantidadDeRegistrosEnArchivo","");
				evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
			}
		  } // bandera

		}
		else if ( sesionvalida ) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response );
		}
		if(erroresEnElArchivo != null) {	//limpieza de Variables
			erroresEnElArchivo.clear();
			erroresEnElArchivo = null;
		}

	}

	// M?todo para conocer la cantidad de
	// registros existentes en un archivo.
	public int registrosEnArchivo(RandomAccessFile file){
		EIGlobal.mensajePorTrace( "***ImportNominaInter.class &Entrando al metodo registrosEnArchivo&", EIGlobal.NivelLog.INFO);
		String registroLeido = "";
		long posicion = 0;
		int cantRegistros = 0;
		try {
			if ( file.length() > 0 ) {
				file.seek( 0 );
				do {
					registroLeido = file.readLine();
					//EIGlobal.mensajePorTrace("***ImportNominaInter.class registroLeido: &" + registroLeido + "&", EIGlobal.NivelLog.INFO);
					posicion = file.getFilePointer();
					cantRegistros++;
				} while( posicion < file.length() );
			}
		} catch( IOException e ) {
			EIGlobal.mensajePorTrace( "***ImportNominaInter.class" +
					" Ha ocurrido una excepcion en: %registrosEnArchivo()" + e, EIGlobal.NivelLog.ERROR );
		}
		EIGlobal.mensajePorTrace( "***ImportNominaInter.class " +
				"&Saliendo del metodo registrosEnArchivo&", EIGlobal.NivelLog.INFO);
		return cantRegistros;
	}


 //METODO PARA AGRGAR PUNTO	DECIMAL	AL IMPORTE
	public String  agregarPunto(String importe){
		 EIGlobal.mensajePorTrace("***ImportNominaInter.class &Entrando al metodo agregarPunto()&", EIGlobal.NivelLog.INFO);
         String importeConPunto="";
         String cadena1=importe.substring(0,importe.length()-2);
         cadena1=cadena1.trim();
         String cadena2=importe.substring(importe.length()-2, importe.length());
         importe = cadena1+" "+cadena2;
         importeConPunto= importe.replace(' ','.');
		 if(importeConPunto.startsWith(".."))
			 importeConPunto=".0"+cadena2.trim();
		 EIGlobal.mensajePorTrace("***ImportNominaInter.class &Saliendo del metodo agregarPunto()&", EIGlobal.NivelLog.INFO);
         return importeConPunto;
     }


//METODO PARA FORMATEAR	EL IMPORTE CUANDO CONTENGA DECIMALES

	public String formateaImporte(String importe){
		EIGlobal.mensajePorTrace("***ImportNominaInter.class &Entrando al metodo formateaImporte()&", EIGlobal.NivelLog.INFO);
		String importeFormateado="";
		boolean contienePunto=false;
		int i=0;
		for (i=0; i<importe.length(); i++){
			if(importe.charAt(i)=='.'){
				contienePunto=true;
				break;
			}
		}
		if(contienePunto)
			importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
		else
			importeFormateado=importe;
		EIGlobal.mensajePorTrace("***ImportNominaInter.class &Saliendo del metodo formateaImporte()&", EIGlobal.NivelLog.INFO);
		return importeFormateado;
	}

	//metodo para visualizar los errores en una pagina html
	//****archivoValido=false; este metodo no se utiliza en este programa...
	public void salirDelProceso( boolean [] archivoValido, String s, String variable,
			HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace("***ImportNominaInter.class &Entrando al metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String contArchivoStr="";
	    contArchivoStr="<table border=0 align=center>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>Pago de N&oactue;mina Interbancaria";
		contArchivoStr=contArchivoStr+"<tr bgcolor=red+><td colspan=9 align=center><b>Se han detectado errores al leer el archivo</b></td></tr>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>"+s+"</b></td></tr>";
	    contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>"+variable+"</b></td></tr>";
		contArchivoStr=contArchivoStr+"</tr></table>";
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pago de Nomina Interbancaria","Servicios &gt; Nomina &gt; Pagos","s25800h",request));
	    request.setAttribute("erroresEnArchivo",""+contArchivoStr);
	    archivoValido[0]=false;
		evalTemplate(IEnlace.ERR_NOMINA_TMPL, request, response );
	}

public String[] arregloCuentas(File nameArchivo, int numeroRegs){
		EIGlobal.mensajePorTrace("***ImportNominaInter.class &Entrando al metodo arregloCuentas&", EIGlobal.NivelLog.INFO);
		String lineaQueSeLeyo	 = "";
		String[] arreglo		 = new String[numeroRegs];
		try{
			RandomAccessFile archivoLectura=new RandomAccessFile(nameArchivo,"r");
			try{
			archivoLectura.seek(41);
			lineaQueSeLeyo		= archivoLectura.readLine();
			if(lineaQueSeLeyo==null) {
				lineaQueSeLeyo		= archivoLectura.readLine();
			}
			} catch(Exception e){
				e.printStackTrace();
			}
			if(lineaQueSeLeyo!=null)
				for(int i=0; i<arreglo.length; i++){
					if(lineaQueSeLeyo.length()>80){ //  para evitar el error
					   if(i>=0 && i<numeroRegs)
						  arreglo[i]		= lineaQueSeLeyo.substring(61,81); // 72
				   }
					else{
						i--;
					}
					lineaQueSeLeyo	= archivoLectura.readLine();
					if(lineaQueSeLeyo==null)
					   break;
				}

		} catch(IOException exception){
			EIGlobal.mensajePorTrace("***ImportNominaInter.class Ha ocurrido una excepcion en: %arregloCuentas()"+exception, EIGlobal.NivelLog.INFO);
		}
		return arreglo;
	}

public String[] arregloNumeroEmpleado(File nameArchivo, int numeroRegs){
		EIGlobal.mensajePorTrace("***ImportNominaInter.class &Entrando al metodo arregloNumeroEmpleado&", EIGlobal.NivelLog.INFO);
		String lineaQueSeLeyo	= "";
		String[] arregloNumeroEmpleado = new String[numeroRegs];
		try{
			RandomAccessFile archivoLectura = new RandomAccessFile(nameArchivo,"r");
			archivoLectura.seek(41);
			lineaQueSeLeyo = archivoLectura.readLine();
			for ( int i = 0; i < arregloNumeroEmpleado.length; i++ ) {
				if ( lineaQueSeLeyo.equals( "" ) )
					lineaQueSeLeyo = archivoLectura.readLine();
				if ( lineaQueSeLeyo == null )
					break;
				//System.out.println(lineaQueSeLeyo);
				if(lineaQueSeLeyo.length()>12)
					if(i >= 0 && i < numeroRegs){
					arregloNumeroEmpleado[i] = lineaQueSeLeyo.substring( 6, 13 );
					}
				else{
					i--;
					}
					lineaQueSeLeyo = archivoLectura.readLine();

			}
		} catch(IOException exception) {

			EIGlobal.mensajePorTrace("***ImportNominaInter.class Ha ocurrido una excepcion en: %arregloNumeroEmpleado()"+exception, EIGlobal.NivelLog.INFO);
		} catch(Exception e){
			e.printStackTrace();
		}
		return arregloNumeroEmpleado;
	}

	boolean verificaCuentaCargo(String[][] arrayCuentas, String cuentaCargo){
		for(int indice=1; indice<=Integer.parseInt(arrayCuentas[0][0]); indice++){
			if (arrayCuentas[indice][1].trim().equals(cuentaCargo)&&arrayCuentas[indice][2].trim().equals("P"));
			return true;
		}
		return false;
	}
	public String completaCampos(int longitudActual, int longitudRequerida){
		//EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo completaCampos()&", EIGlobal.NivelLog.INFO);
	    String blancos =" ";
	    int faltantes= longitudRequerida-longitudActual;
	    for (int i=1; i<faltantes; i++){
		 blancos=blancos+" ";
	    }
		if(faltantes>0)
		    return blancos;
		else
			return "";
	}//fin del metodo completaCampos()
	public String formatoMoneda( String cantidad )
	{
	EIGlobal.mensajePorTrace("***Entrando a formatoMoneda ", EIGlobal.NivelLog.INFO);
	    cantidad = cantidad.trim();
		String language = "la"; // ar
		String country  = "MX";  // AF
		Locale local    = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato  = "";
		String cantidadDecimal = "";
		String formatoFinal    = "";
		String cantidadtmp     = "";

		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.0";
		EIGlobal.mensajePorTrace("***cantidad.length()>>> "+cantidad.length(), EIGlobal.NivelLog.INFO);
		if(cantidad.length() > 2){
			EIGlobal.mensajePorTrace("***cantidad1>>> "+cantidad, EIGlobal.NivelLog.INFO);
			try {
				cantidadtmp =cantidad.substring(0,cantidad.length()-2);
				cantidadDecimal = cantidad.substring(cantidad.length()-2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				formato=formato.substring(1,formato.length()-2);
				formatoFinal = formato + cantidadDecimal;
			} catch(NumberFormatException e) {
				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);
				formatoFinal="0.00";}
		}else {
		try {
		       if (cantidad.length() > 1){
				cantidadDecimal = cantidad.substring(cantidad.length()-2);
				formatoFinal = "0."+ cantidadDecimal;
			   } else {
				cantidadDecimal = cantidad.substring(cantidad.length()-1);
				formatoFinal = "0.0"+ cantidadDecimal;
				}
			} catch(NumberFormatException e) {
				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);
				formatoFinal="0.00";}
		}
		if(formatoFinal==null)
			formato = "";
		return formatoFinal;
	}

	//PPM Obtiene la clave (nume_cecoban) en base al nombre corto del banco
    String traeClaveBanco(String nombreCorto)
    {
      String dato="";
      String datotmp = "";
	  String totales="";

      int Existe=0;
	  int totalBancos=0;

	  Connection conn = null;
	  ResultSet cveBancoResult = null;
	  System.out.println("El nombre corto del banco es: ...." + nombreCorto);
	  try{
	      conn = createiASConn(Global.DATASOURCE_ORACLE);

	      if(conn == null)
	        EIGlobal.mensajePorTrace("ImportNominaInter.class - traeClaveBanco: No se puedo crear conexion.", EIGlobal.NivelLog.ERROR);

		  String sqlcveBanco="Select NUM_CECOBAN from COMU_INTERME_FIN where TRIM(cve_interme) = '" + nombreCorto.trim() +"'";
		  PreparedStatement cveBancoQuery = conn.prepareStatement(sqlcveBanco);
	      if(cveBancoQuery==null)
		     EIGlobal.mensajePorTrace("ImportNominaInter.class - traeClaveBanco: No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
		  cveBancoResult = cveBancoQuery.executeQuery();
	      if(!cveBancoResult.next())
	         EIGlobal.mensajePorTrace("ImportNominaInter.class - traeClaveBanco: No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
		  else{
		 	  dato=cveBancoResult.getString(1);
		 	  if(dato==null)
		 	  	dato="";
		 	  else if(dato.trim().length()<3){
		 	  	for(int i=0; i<3-dato.trim().length();i++){
		 	  		datotmp = datotmp.concat("0");
		 	  	}
		 	  	datotmp = datotmp.concat(dato);
		 	  	dato = datotmp;
		 	  }
		 	}
		}catch( SQLException sqle ){
			sqle.printStackTrace();
		}
		finally
		  {
			try
			 {
			   conn.close();
			   cveBancoResult.close();
			 }catch(Exception e) {}
		  }
	return dato;
    }

	private boolean validaDigitoVerificador(String cuenta, String cvebanco)
	 {
	    String pesos="371371371371371371371371371";
		int DC=0;
		String cuenta2=cuenta.substring(0,17);
		for(int i=0;i<cuenta2.length();i++)
	        DC += (Integer.parseInt(cuenta2.substring(i,i+1))) * (Integer.parseInt(pesos.substring(i,i+1))) % 10;
		DC = 10 - (DC % 10);
		DC = (DC >= 10) ? 0 : DC;
		cuenta2+=DC;
		if(cuenta2.trim().equals(cuenta.trim()))
			return true;
		return false;
	}
}