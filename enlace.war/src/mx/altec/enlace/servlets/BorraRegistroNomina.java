/**Banco Santander Mexicano
* Clase	BorraRegistroNomina, Modificacion al archivo de	pago de	nomina
* @autor Rodrigo Godo
* @version 1.1
* fecha	de creacion: Diciembre 2000	- Enero	20001
* responsable: Roberto Guadalupe Resendiz Altamirano
* descripcion: verifica	cual de	las	operaciones	de mantenimiento de	archivo	fue	seleccionada y ejecuta
				dicha operacion, para ello se utilizan los metodos ejecutaAltas(), ejecutaBajas() y
				ejecutaModificaciones()

BufferedReader entrada	= new BufferedReader( new FileReader( archivo_entrada ) );
BufferedWriter salida	= new BufferedWriter( new FileWriter( archivo_salida ) );
					BufferedReader fileAmbiente = null;

HttpServletRequest request


		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");


					session.setNameFileNomina( nomArchNomina );
					session.setNumerosEmpleado( "" );
					session.setfacultad( "" );

					sess.getAttribute("nombreArchivo")
					sess.getAttribute("numeroEmp");  checar el set cuando le subo a la session
					sess.getAttribute("facultadesN")

*/

package	mx.altec.enlace.servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.ArchivosNomina;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ValidaArchivoPagos;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class BorraRegistroNomina extends BaseServlet {
	boolean modificacionValida;
	Vector diasNoHabiles;
	Vector diasNoHabilesJS;
	GregorianCalendar fechaHoy;
	CalendarNomina nomCalendar;
	ValidaArchivoPagos validaFile;
    //String[] cuentas;

	public void doGet( HttpServletRequest request, HttpServletResponse response	)
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		boolean sesionvalida = SesionValida( request, response );
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String strOperacion		  = "";
		String[] numerosEmpleados = null;
		if ( sesionvalida ) {
			//***********************************************
			//modificación para integración pva 07/03/2002
            //***********************************************
			session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
			EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo execute()&", EIGlobal.NivelLog.INFO);
			//request.setAttribute("MenuPrincipal", session.getstrMenu());
			modificacionValida = true;
			diasNoHabilesJS = new Vector();
			fechaHoy= new GregorianCalendar();
			nomCalendar = new CalendarNomina( );
			request.setAttribute("Fecha", ObtenFecha());
			request.setAttribute("ContUser", ObtenContUser(request));
			String laFechaHoy=EIGlobal.formatoFecha(fechaHoy,"aaaammdd");
			EIGlobal.mensajePorTrace( "***BorraRegistroNomina.class &la fecha que se enviara como la fecha del sistema es: "+laFechaHoy+"&", EIGlobal.NivelLog.INFO);
			request.setAttribute( "fechaHoy", laFechaHoy );

			String VarFechaHoy = "";
			diasNoHabiles = nomCalendar.CargarDias();
			String strInhabiles = nomCalendar.armaDiasInhabilesJS();
			Calendar fechaFin = nomCalendar.calculaFechaFin();
			VarFechaHoy = nomCalendar.armaArregloFechas();
			String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();
			//String Cuentas	   = "";
			String tipo_cuenta = "";
		    //String[] cuentas;
		    //***********************************************

			//modificación para integración pva 07/03/2002
            //***********************************************
           /*
			String[][] arrayCuentas = ContCtasRelac( session.getContractNumber() );

			for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++) {
				tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
				if ( arrayCuentas[indice][2].equals( "P" ) &&
						( !tipo_cuenta.equals( "BM" ) ) &&
						( !tipo_cuenta.equals( "SI" ) ) &&
						( !tipo_cuenta.equals( "49" ) ) )
					Cuentas += "<OPTION value = " + arrayCuentas[indice][1] + ">" +
							arrayCuentas[indice][1] + "\n";
			}
			*/

			String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
			datesrvr +=	"<Input	type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
			datesrvr +=	"<Input	type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";

			String cuentaCargo = session.getCuentaCargo();
/*			cuentaCargo = "<Option value = " + cuentaCargo  + ">" + cuentaCargo;
			request.setAttribute( "Cuentas","<Option value = " +
					session.getCuentaCargo() + ">" + session.getCuentaCargo() );*/
			request.setAttribute("textcuenta",cuentaCargo);
			request.setAttribute( "VarFechaHoy",VarFechaHoy);
			request.setAttribute( "fechaFin",strFechaFin);
			request.setAttribute( "diasInhabiles",strInhabiles);
			request.setAttribute( "Fechas",ObtenFecha(true));
			request.setAttribute( "FechaHoy",ObtenFecha(false) + " - 06" );

			String descargaArchivo = session.getRutaDescarga();
			String archivoName = "";
			if ((String )sess.getAttribute("nombreArchivo") != null)
			archivoName = (String )sess.getAttribute("nombreArchivo");
			//String archivoName = session.getNameFileNomina();

			ArchivoRemoto archR = new ArchivoRemoto();

			String nombreArchivoOriginal = archivoName.substring( archivoName.lastIndexOf( "/" ) + 1 );
			archR.copiaLocalARemoto( nombreArchivoOriginal, "WEB" );
			request.setAttribute( "DescargaArchivo", session.getRutaDescarga() );

			//////se determina la hora del servidor///////////////////////////
			/**/String systemHour=ObtenHora();								//
			/**/request.setAttribute("horaSistema", systemHour);			//
			//////////////////////////////////////////////////////////////////
			//request.setAttribute("DescargaArchivo",descargaArchivo);
			if ( request.getParameter( "operacion" ).equals( "baja" ) ) {
				ejecutaBajas( request, response, strOperacion,  numerosEmpleados,cuentaCargo);
			}
			if ( request.getParameter( "operacion" ).equals( "alta" ) ) {
				ejecutaAltas( request, response, strOperacion, numerosEmpleados , cuentaCargo);
			}
			if ( request.getParameter( "operacion" ).equals( "modificacion" ) ) {
				ejecutaModificacion( request, response, strOperacion, numerosEmpleados,cuentaCargo);
			}
		} else if ( sesionvalida ) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response );
		}
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Saliendo del metodo execute()&", EIGlobal.NivelLog.INFO);
	}

	//Metodo que ejecuta bajas en el archivo
	public void ejecutaBajas( HttpServletRequest request, HttpServletResponse response, String strOperacion,String[] numerosEmpleados, String cuentaCargo)
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo ejecutaBajas: &", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String contenidoArchivoStr="";
		String strRegistro = request.getParameter("registro");
		strOperacion= request.getParameter("operacion");
		long longRegistro=0;
		try{
			longRegistro=Long.parseLong(strRegistro);
		} catch(NumberFormatException e) {
			EIGlobal.mensajePorTrace("***BorraRegistroNomina.class Ha ocurrido una excepcion en: %ejecutaBajas()>>"+e.toString()+"<<", EIGlobal.NivelLog.INFO);
		}
		String nombreArchivoEmp = (String )sess.getAttribute("nombreArchivo");
		//String nombreArchivoEmp=session.getNameFileNomina();
		validaFile=new ValidaArchivoPagos(this, nombreArchivoEmp);
		String elArchivoNomina	= "";//nomArchNomina
		String elArchivo		= "";//nombreOriginal
		//File nominaFile= new File(session.getNameFileNomina());
		File nominaFile= new File((String )sess.getAttribute("nombreArchivo"));
		int tamReg = tamRegistro (nominaFile);
		String[] cuentas=arregloCuentas(nominaFile, Integer.parseInt(session.getTotalRegsNom()),tamReg); // le falta por que esta mal al momento de guardar los datos...
		numerosEmpleados=arregloNumeroEmpleado(nominaFile, Integer.parseInt(session.getTotalRegsNom()),tamReg);
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &El archivo que se generara es: &", EIGlobal.NivelLog.INFO);
		//ArchivosNomina es la clase que representa al archivo de pago de nomina
		ArchivosNomina archivo3 = new ArchivosNomina((String)sess.getAttribute("nombreArchivo"),request);
		//se hacen las actualizaciones al sumario
		//importado
		long totalReg = Integer.parseInt(session.getTotalRegsNom());
		String Importe=request.getParameter("Importe");
		Importe=formateaImporte(Importe);
		//nuevo
		//archivo3.actualizaSumario("sumSecuencial",session.getLastSecNom(),false);
		//importado
		archivo3.actualizaSumario("sumSecuencial",session.getLastSecNom(),false,totalReg,tamReg,request);
		archivo3.actualizaSumario("totalRegs",session.getTotalRegsNom(),false,totalReg,tamReg,request);
		archivo3.actualizaSumario("impTotal",Importe,false, totalReg,tamReg,request);
		//el metodo bajaRegistro() existe dentro de la clase ArchivosNomina y debe dar de baja fisicamente al registro seleccionado
		//se usa como argumento del metodo la posicion del registro que se eliminiara
		archivo3.bajaRegistro(longRegistro,tamReg,request);
		archivo3.actualizaConsecutivos(Integer.parseInt(session.getTotalRegsNom()),request);
		contenidoArchivoStr= "" + archivo3.lecturaArchivo();
		String archivoName=(String )sess.getAttribute("nombreArchivo");
		//String archivoName=session.getNameFileNomina();

		request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
		request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr);
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Pagos","s25830h_b",request));
		request.setAttribute("facultades",sess.getAttribute("facultadesN"));
		//request.setAttribute("facultades",session.getFacultadesNomina());
		request.setAttribute("textcuenta",cuentaCargo);
		request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
		session.setCuentasArchivo(request.getParameter("CuentasArchivo"));
		sess.setAttribute("numeroEmp",request.getParameter("NumerosEmpleado"));
		//session.setNumerosEmpleado(request.getParameter("NumerosEmpleado"));

		evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response );
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Fin del metodo ejecutaBajas()&", EIGlobal.NivelLog.INFO);
	}//fin del metodo ejecutaBajas

	//metodo que da de alta registrso en el archivo
	public void ejecutaAltas( HttpServletRequest request, HttpServletResponse response, String strOperacion, String[] numerosEmpleados, String cuentaCargo )
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo ejecutaAltas()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String contenidoArchivoStr = "";
		String adicionales = "";
		String zeros = "";
		File nominaFile = new File(	(String )sess.getAttribute("nombreArchivo") );
		//File nominaFile = new File( session.getNameFileNomina() );
		int tamReg = 0;
		String espacio = "";
		//int tamReg = tamRegistro (nominaFile);
		//se forma el arreglo de cuentas que se usara para comparar que una cuenta nueva no exista previamente dentro del archivo
		String[] cuentas = arregloCuentas( nominaFile, Integer.parseInt( session.getTotalRegsNom() ),tamReg );


		//se forma el arreglo de numeros de empleado que se usara para comparar que una nuevo numero de empleado no exista previamente dentro del archivo
		numerosEmpleados = arregloNumeroEmpleado( nominaFile, Integer.parseInt( session.getTotalRegsNom() ) ,tamReg);

		strOperacion= request.getParameter("operacion");

		//validaFile = new ValidaArchivoPagos( this, session.getNameFileNomina() );
		validaFile = new ValidaArchivoPagos( this, (String )sess.getAttribute("nombreArchivo") );

		//VALIDACIONES
		//la variable NumeroEmpleado asi como otras que se invocan mediante 'valIn' provienen de cuadros de
		//dialogo existentes en el template
		String NumeroEmpleado	= request.getParameter("NumeroEmpleado");
	    String ApellidoPaterno	= request.getParameter("ApellidoPaterno");
	    String ApellidoMaterno	= request.getParameter("ApellidoMaterno");
	    String Nombre			= request.getParameter("Nombre");
		String NumeroCuenta		= request.getParameter("NumeroCuenta");
		String Importe			= request.getParameter("Importe");

		Importe					= formateaImporte(Importe, espacio);

		contenidoArchivoStr= "<br>NumeroEmpleado ="  + NumeroEmpleado +", long=" + NumeroEmpleado.length() + "<br> "+
					         "ApellidoPaterno =" +ApellidoPaterno +", long=" + ApellidoPaterno.length() + "<br> "+
					         "ApellidoMaterno =" +ApellidoMaterno +", long=" + ApellidoMaterno.length() + "<br> "+
					         "Nombre =" +Nombre +", long=" + Nombre.length() + "<br> "+
					         "NumeroCuenta =" +NumeroCuenta +", long=" + NumeroCuenta.length() + "<br> "+
					         "Importe =" +Importe +", long=" + Importe.length() + "<br>";


		//la variable consecutivo determinara el valor del campo numeroSecuenacial que existe dentro de los registros de
		//detalle, consecutivo asumira un valor que se tomara directamente de la cantidad de registros de detalle
		//que existan dentro del archivo mediante el metodo  getRegsNom()
		String consecutivo=session.getTotalRegsNom();
		int temp= Integer.parseInt(consecutivo)+2;
		consecutivo=String.valueOf(temp);
		consecutivo = completaCamposNumericos(consecutivo.length(), 5)+consecutivo;

		String cadena= "2"+ consecutivo  + NumeroEmpleado  +
					         ApellidoPaterno + ApellidoMaterno + Nombre +
					         NumeroCuenta + Importe;

		//ArchivosNomina archivo4 = new ArchivosNomina(session.getNameFileNomina(),request);
		ArchivosNomina archivo4 = new ArchivosNomina((String )sess.getAttribute("nombreArchivo"),request);

		if(modificacionValida){
			int totRegs=Integer.parseInt(session.getTotalRegsNom());   //totRegs funciona para conocer la cantidad de regs de detalle
			String registrosTotales=String.valueOf(totRegs);
			registrosTotales=completaCamposNumericos(registrosTotales.length(),5)+registrosTotales;
			//String nombreArchivoEmp=session.getNameFileNomina();
			String nombreArchivoEmp=(String )sess.getAttribute("nombreArchivo");
			//
			long longRegistro = 0;
			archivo4.actualizaSumario("sumSecuencial",consecutivo,true,longRegistro,tamReg,request);
			archivo4.actualizaSumario("totalRegs",registrosTotales,true,longRegistro,tamReg,request);
			archivo4.actualizaSumario("impTotal",Importe,true,longRegistro,tamReg,request);
			request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
			archivo4.altaRegistro(cadena);
			archivo4.agregaRegistro(request);
		}
		archivo4.close();

		//ArchivosNomina archivo5 = new ArchivosNomina(session.getNameFileNomina(),request);
		ArchivosNomina archivo5 = new ArchivosNomina((String )sess.getAttribute("nombreArchivo"),request);

		if (modificacionValida) {
			request.setAttribute("operacion",strOperacion);
			request.setAttribute("newMenu", session.getFuncionesDeMenu());
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request));
			request.setAttribute("CuentasArchivo", request.getParameter("CuentasArchivo")+NumeroCuenta.trim()+"|");
			request.setAttribute("NumerosEmpleado", request.getParameter("NumerosEmpleado")+NumeroEmpleado+"|");
			request.setAttribute("facultades",sess.getAttribute("facultadesN"));
			//request.setAttribute("facultades",session.getFacultadesNomina());

			request.setAttribute("archivoImagen","/gifs/EnlaceMig/gbo25480.gif");
			request.setAttribute("altImagen","Alta");
			request.setAttribute("facultades",sess.getAttribute("facultadesN"));
			//request.setAttribute("facultades",session.getFacultadesNomina());
			// aqui modifique
			request.setAttribute("textcuenta",cuentaCargo);
			request.setAttribute("botLimpiar","<td align=left valign=top width=76><a href=javascript:js_limpiar();><img border=0 name=boton src=/gifs/EnlaceMig/gbo25250.gif width=76 height=22 alt=Limpiar></a></td>");
			evalTemplate(IEnlace.NOMINADATOS_TMPL, request, response );
		}
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Fin del metodo ejecutaAltas()&", EIGlobal.NivelLog.INFO);
	}

	//metodo que modifica datos en el archivo
	public void ejecutaModificacion( HttpServletRequest request, HttpServletResponse response , String strOperacion, String[] numerosEmpleados, String cuentaCargo)
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo ejecutaModificacion()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String contenidoArchivoStr	= "";
	    //File nominaFile= new File(session.getNameFileNomina());
		File nominaFile= new File((String )sess.getAttribute("nombreArchivo"));

		//falta utilizar lel metodo para traer el tamReg . esto es temporal
		int tamReg = tamRegistro (nominaFile);
		String[] cuentas=arregloCuentas(nominaFile, Integer.parseInt(session.getTotalRegsNom()),tamReg);
		numerosEmpleados=arregloNumeroEmpleado(nominaFile, Integer.parseInt(session.getTotalRegsNom()),tamReg);
		strOperacion= request.getParameter("operacion");
		//validaFile=new ValidaArchivoPagos(this, session.getNameFileNomina());
		validaFile=new ValidaArchivoPagos(this, (String )sess.getAttribute("nombreArchivo"));
		String adicionales	= "";
		String zeros		= "";
		String espacio = "";
		String NumeroEmpleado	= request.getParameter("NumeroEmpleado");
	    String ApellidoPaterno	= request.getParameter("ApellidoPaterno");
	    String ApellidoMaterno	= request.getParameter("ApellidoMaterno");
	    String Nombre			= request.getParameter("Nombre");
		String NumeroCuenta		= request.getParameter("NumeroCuenta");

		String Importe			= request.getParameter("Importe");

			   if (Importe.length() == 18)
			   Importe			= formateaImporte(Importe,espacio);
			   else Importe		= formateaImporte(Importe);

		String posicion = request.getParameter("registro");
		long longRegistro=0;
		longRegistro=Long.parseLong(posicion);
		String strRegistro = session.getTotalRegsNom();
		long totalReg = 0;
		try{ totalReg  = Long.parseLong(strRegistro) ;}
		catch(NumberFormatException e) {System.out.println("Error en el formato");}
		//String nombreArchivoNom=session.getNameFileNomina();
		String nombreArchivoNom=(String )sess.getAttribute("nombreArchivo");
		ArchivosNomina archivo4 = new ArchivosNomina(nombreArchivoNom,request);
        //long secuencialtmp =  totalReg + 1;
	    //String secuencial=String.valueOf(secuencialtmp);
		String secuencial=String.valueOf(totalReg);
	    secuencial=completaCamposNumericos(secuencial.length(),5)+secuencial;
		String cadena= NumeroEmpleado+ApellidoPaterno+ApellidoMaterno+Nombre+NumeroCuenta+Importe;
		if(modificacionValida){

		String anteriorImporte	=	request.getParameter("oldImporte");
		String Import			=	request.getParameter("Importe");

				anteriorImporte	=	formateaImporte(anteriorImporte);
				Import			=	formateaImporte(Import);

		int dif=Integer.parseInt(Import.trim())-Integer.parseInt(anteriorImporte.trim());
		String diferencia		=	String.valueOf(dif);
			archivo4.actualizaSumario("impTotal",diferencia,true,totalReg,tamReg,request);
			archivo4.edicionRegistros(longRegistro, cadena,tamReg);
		}
		archivo4.close();
		ArchivosNomina archivo5 = new ArchivosNomina(nombreArchivoNom,request);
		if (modificacionValida){
			contenidoArchivoStr= "" +archivo5.lecturaArchivo();
			//String archivoName=session.getNameFileNomina();
			String archivoName=(String )sess.getAttribute("nombreArchivo");
			request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
			request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr);
			request.setAttribute("newMenu", session.getFuncionesDeMenu());
			request.setAttribute("MenuPrincipal", session.getStrMenu());

			request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request));
			request.setAttribute("facultades",sess.getAttribute("facultadesN"));
			//request.setAttribute("facultades",session.getFacultadesNomina());
			request.setAttribute("textcuenta",cuentaCargo);
			request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
			session.setCuentasArchivo(request.getParameter("CuentasArchivo"));
			sess.setAttribute("numeroEmp",request.getParameter("NumerosEmpleado"));
			//session.setNumerosEmpleado(request.getParameter("NumerosEmpleado"));


		 evalTemplate( IEnlace.MTO_NOMINA_TMPL,	request, response );
		}
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Fin del metodo ejecutaModificaciones()&", EIGlobal.NivelLog.INFO);
	} //fin del metodo ejecutaModificaciones

	//el metodo formateaImporte, permite redondear a dos decimales, y eliminar el punto
	//decimal del campo importe para almacenarlo en el archivo
	//importado
	public String formateaImporte(String importe, String espacio){
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo formteaImporte() con espacio&", EIGlobal.NivelLog.INFO);
		String importeFormateado="";
		importeFormateado=importe.substring(0,importe.indexOf('.'))+importe.substring(importe.indexOf('.')+1,importe.length());
		return " "+importeFormateado;
	}

	public String formateaImporte(String importe){
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo formteaImporte()&", EIGlobal.NivelLog.INFO);
		String importeFormateado="";
		importeFormateado=importe.substring(0,importe.indexOf('.'))+importe.substring(importe.indexOf('.')+1,importe.length());
		return importeFormateado;
	}

	//metodo que agrega espacios en blanco para completar la longitud del campo
	//alfanumerico requerido
	public String completaCampos(int longitudActual, int longitudRequerida){
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo completaCampos()&", EIGlobal.NivelLog.INFO);
	    String blancos =" ";
	    int faltantes= longitudRequerida-longitudActual;
	    for (int i=1; i<faltantes; i++){
		 blancos=blancos+" ";
	    }
		if(faltantes>0)
		    return blancos;
		else
			return "";
	}//fin del metodo completaCampos()

	//metodo que agrega espacios en blanco para completar la longitud del campo
	//numerico requerido
	public String completaCamposNumericos(int longitudActual, int longitudRequerida){
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo completaCamposNumericos()&", EIGlobal.NivelLog.INFO);
	    String ceros="0";
	    int faltantes= longitudRequerida-longitudActual;
	    for(int i=1; i<faltantes; i++){
		 ceros=ceros+"0";
	    }
		if(faltantes>0)
		    return ceros;
		else
			return "";
	}

	public boolean verificaDuplicidadCuentasNuevas(String[] cuentas, String cuentaNueva){
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo verificaDuplicidadCuentasNuevas()&", EIGlobal.NivelLog.INFO);
		boolean cuentaDuplicada	=	false;
		for(int i=0; i<cuentas.length; i++){
			if(cuentas[i].equals(cuentaNueva)){
				cuentaDuplicada	=	true;
				break;
			}
		}
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Saliendo del metodo verificaDuplicidadCuentasNuevas()&", EIGlobal.NivelLog.INFO);
		return cuentaDuplicada;
	}

//metodo para visualizar los errores en	una	pagina html
	public void salirDelProceso( String s, String variable,
			HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String contArchivoStr="";
	    contArchivoStr="<table border=1 align=center>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>Pago de N&oacute;mina</b></td></tr>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>Se han detectado errores en sus datos</b></td></tr>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>"+s+"</b></td></tr>";
	    contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>"+variable+"</b></td></tr>";
		contArchivoStr=contArchivoStr+"</tr></table>";
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Altas","s25810h",request));
		request.setAttribute("facultades",sess.getAttribute("facultadesN"));
		//request.setAttribute( "facultades", session.getFacultadesNomina() );
	    request.setAttribute( "erroresEnArchivo", "" + contArchivoStr );
		modificacionValida = false;
		evalTemplate( IEnlace.ERR_NOMINA_TMPL, request,	response );
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Saliendo del metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);

	}//fin del metodo salirDelProceso
	//importado
	// Aqui se determina la forma de leer el archivo.....126
		public int tamRegistro( File nameArchivo ) {
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo tamRegistro()&", EIGlobal.NivelLog.INFO);
		String lineaQueSeLeyo = "";
		int longitudReg       =  0;
		try {
			RandomAccessFile archivoLectura = new RandomAccessFile( nameArchivo, "r" );
			//BufferedReader archivoLectura	= new BufferedReader( new FileReader( nameArchivo ) );
			archivoLectura.seek(41);
			lineaQueSeLeyo = archivoLectura.readLine();
			longitudReg = lineaQueSeLeyo.length();

		} catch ( IOException exception ) {
			EIGlobal.mensajePorTrace( "***BorraRegistroNomina.class Ha ocurrido una excepcion en: %tamRegistro()" + exception, EIGlobal.NivelLog.ERROR );
		}
		EIGlobal.mensajePorTrace( "***BorraRegistroNomina.class &Saliendo del metodo  tamRegistro()&", EIGlobal.NivelLog.INFO);
		return longitudReg;
	}

	public String[] arregloCuentas( File nameArchivo, int numeroRegs , int tamReg) {
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo arregloCuentas()&", EIGlobal.NivelLog.INFO);
		String lineaQueSeLeyo = "";
		String[] arreglo = new String[numeroRegs];
		try {
			RandomAccessFile archivoLectura = new RandomAccessFile( nameArchivo, "r" );
			//BufferedReader archivoLectura	= new BufferedReader( new FileReader( nameArchivo ) );
		if (tamReg == 127 || tamReg == 0)	archivoLectura.seek( 41 );
		else archivoLectura.seek( 40 );

			lineaQueSeLeyo = archivoLectura.readLine();
			for ( int i = 0; i < arreglo.length; i++ ) {
			    if (lineaQueSeLeyo.length()  >= 109)
				arreglo[i] = lineaQueSeLeyo.substring(93,109);
				else
				arreglo[i] = "";
				lineaQueSeLeyo = archivoLectura.readLine();
			}
		} catch ( IOException exception ) {
			EIGlobal.mensajePorTrace( "***BorraRegistroNomina.class Ha ocurrido una excepcion en: %arregloCuentas()" + exception, EIGlobal.NivelLog.ERROR );
		}
		EIGlobal.mensajePorTrace( "***BorraRegistroNomina.class &Saliendo del metodo arregloCuentas()&", EIGlobal.NivelLog.INFO);
		return arreglo;
	}

	public String[] arregloNumeroEmpleado(File nameArchivo, int numeroRegs, int tamReg){
		EIGlobal.mensajePorTrace("***BorraRegistroNomina.class &Entrando al metodo arregloNumeroEmpleado()&", EIGlobal.NivelLog.INFO);
		String lineaQueSeLeyo="";
		String[] arregloNumeroEmpleado=new String[numeroRegs];
		try {
			RandomAccessFile archivoLectura = new RandomAccessFile(nameArchivo,"r");
			//BufferedReader archivoLectura	= new BufferedReader( new FileReader( nameArchivo ) );
			if (tamReg == 127 || tamReg == 0)	archivoLectura.seek( 41 );
			else archivoLectura.seek( 40 );

	//		archivoLectura.seek(41);
			lineaQueSeLeyo=archivoLectura.readLine();
			for(int i=0; i<arregloNumeroEmpleado.length; i++){
				arregloNumeroEmpleado[i]=lineaQueSeLeyo.substring(6,13);
				//System.out.println("cuenta que se agrego al arreglo: "+arregloNumeroEmpleado[i]);
				lineaQueSeLeyo=archivoLectura.readLine();
			}
		} catch( IOException exception ) {
			EIGlobal.mensajePorTrace( "***BorraRegistroNomina.class Ha ocurrido una excepcion en: %arregloNumeroEmpleado()"+exception, EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace( "***BorraRegistroNomina.class &Saliendo del metodo arregloNumeroEmpleado()&", EIGlobal.NivelLog.INFO);
		return arregloNumeroEmpleado;
	}
}