/**Banco Santander Mexicano
* Clase MtoNomina, Carga la plantilla para el mantenimineto del archivo de pagos
* @author Rodrigo Godo
* @version 1.1
* fecha de creacion: Diciembre 2000 - Enero 2001
* responsable: Roberto Guadalupe Resendiz Altamirano
* descripcion: Crea un objeto del tipo TemplateMapBasic, carga un calendario para exhibirse dentro del TemplateMap,
				asi como un combo con la lista de cuentas con las cuales puede operar el usuario,
				valida facultades para saber  que tipo de operaciones pueden efectuarse sobre los archivos de pago de nomina
				(creacion, importacion, altas, bajas, cambios, borrado y recuperacion de archivos,
				asi como la transmision de los pagos que habran de efectuarse).
<P>Modificaciones:<BR>
* 17/12/2002 Se llama a vencimientos de cr&eacute;dito electr&oacute;nico (Hugo Ru&iacute;z Zepeda).
<P>
*/
package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BloqueoUsuarioBO;
import mx.altec.enlace.bo.EpymeBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import java.sql.*;
import java.io.IOException;

/**
 * 
 * @author Z708764
 *
 */
public class CreaAmbiente extends BaseServlet {
	
	/**
	 * ENTROCAMBIONIP = "entro_cambio_nip"
	 */
	private static final String ENTROCAMBIONIP = "entro_cambio_nip";
	/**
	 * Implementa doGet del servlet
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException,IOException {
		defaultAction( request, response );
	}
	/**
	 * Implementa doPost del servlet
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
		defaultAction( request, response );
	}
	/**
	 * Metodo default de la clase
	 * @param request request
	 * @param response response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException {
		//<VC:CGH> Se obtiene si el origen en la aplicacion es de supernet
		String redirSupernet = (String)request.getSession().getAttribute("origensnet");
		//</VC>


		log("Entro Crea Ambiente");
		EIGlobal.mensajePorTrace( "CreaAmbiente.java::defaultAction()" , EIGlobal.NivelLog.DEBUG);
		boolean sesionvalida = SesionValida( request, response );

		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
        
        

		if ( sesionvalida )
		{
			String Contrato = request.getParameter( "lstContratos" );
			EIGlobal.mensajePorTrace("//////////////////////////////////////// CREAAMBIENTE: GETUSERID HELG", EIGlobal.NivelLog.DEBUG);

			String usr = session.getUserID8();
			String direccion = request.getParameter( "opcionElegida" );
			String entro_cambio_nip="";
			String cambiaContratos="";
			BloqueoUsuarioBO bloq = new BloqueoUsuarioBO();

			//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
			EIGlobal.mensajePorTrace("CreaAmbiente.java::isBlockedUser() valida usuario bloqueado", EIGlobal.NivelLog.DEBUG);
			if(bloq.isBlockedUser(Contrato,usr)) {
				EIGlobal.mensajePorTrace( "<<<<---USUARIO BLOQUEADO POR INACTIVIDAD--->>>> ", EIGlobal.NivelLog.DEBUG);
				cierraDuplicidad(session.getUserID8());
				request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su Usuario ha sido bloqueado por inactividad.\",3);" );
				request.setAttribute ( "Fecha", ObtenFecha () );
				evalTemplate ( IEnlace.LOGIN, request, response );
				sess.invalidate();
				return;
			}
			sess.setAttribute("bloqCreaAmbiente", "true");

			EIGlobal.mensajePorTrace("entroContratos vale: "+(String)request.getParameter("entroContratos"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("cambiaContratos vale: "+(String)request.getParameter("cambiaContratos"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("entro_cambio_nip vale: "+(String)sess.getAttribute(ENTROCAMBIONIP), EIGlobal.NivelLog.DEBUG);

			cambiaContratos=(String)request.getParameter("cambiaContratos");
			if ((null!=request.getParameter("entroContratos")) && ( null==request.getParameter("cambiaContratos") || "null".equals(cambiaContratos.trim()) || "".equals(cambiaContratos.trim()))) {
				sess.setAttribute(ENTROCAMBIONIP,null);
			}

			if (sess.getAttribute(ENTROCAMBIONIP)!=null) {
				entro_cambio_nip =(String) sess.getAttribute(ENTROCAMBIONIP);
			}else {
				sess.setAttribute(ENTROCAMBIONIP,"1");
			}

			// (Inicio) SEE 05/11/2004 Se cargan estos atributos cuando salida2.jsp
			// llama al crea ambiente.
			if (Contrato != null) {
			   sess.setAttribute("Contrato_nip",Contrato);
			   sess.setAttribute("direccion_nip",direccion);
			}else {
			   Contrato  = (String) sess.getAttribute("Contrato_nip");
			   direccion = (String) sess.getAttribute("direccion_nip");
		    }
			// (fin) SEE 05/11/2004 Se cargan estos atributos cuando salida2.jsp
			// llama al crea ambiente
			EIGlobal.mensajePorTrace("La Direccion seleccionada es = >" +direccion+ "<", EIGlobal.NivelLog.INFO);
			String AmbCod = TraeAmbiente(usr,Contrato,request);
			String[] noPerfil = TraePerfil(request);

			session.setContractNumber( Contrato );
			session.setUserProfile(noPerfil[1]);
			/*Fin de Bloque que obtiene la sesion*/
			String strSalida = "";
			String avisos = "";
			
			if(sess != null) {
				 EIGlobal.mensajePorTrace("***creaAmbiente.defaultAction()-> Validando contrato epyme" , EIGlobal.NivelLog.INFO);
				 EpymeBO epymeBO=new EpymeBO();
				 boolean esMenuReducido = epymeBO.esMenuReducido(session.getContractNumber(),session.getUserID8());
				 session.setEpyme(esMenuReducido);
				 EIGlobal.mensajePorTrace("***creaAmbiente.defaultAction()-> contrato Epyme = [" + session.getContractNumber() + "][ " + esMenuReducido + "]" , EIGlobal.NivelLog.INFO);
			 }


			/*Declaraci�n para envio de mensajes para un solo contrato   everis   27/06/2008     inicio*/
			String mensaje = muestraMensaje(request, response);
			/*Declaraci�n para envio de mensajes para un solo contrato   everis   27/06/2008     fin   */

			String vencimientosCE="";

			EIGlobal.mensajePorTrace("*** Eliminado la lista de facultades **************", EIGlobal.NivelLog.DEBUG);
			session.borraListaFacultades();
			EIGlobal.mensajePorTrace("*** Facultades eliminadas **************", EIGlobal.NivelLog.DEBUG);

			session.setExcepcionCierraSess(excepCierreSession(session.getContractNumber(), session.getUserID8()));
			session.setBucContrato(recuperaBucContrato(session.getContractNumber()));
			EIGlobal.mensajePorTrace("Usuario ->" + session.getUserID8() + ", Contrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Usuario y contrato especial para cambio de ip ->" + session.isExcepcionCierraSess(), EIGlobal.NivelLog.INFO);

            if(!FijaAmbiente(request))
			{
				EIGlobal.mensajePorTrace("***CreaAmbiente.class FijaAmbiente False", EIGlobal.NivelLog.INFO);
				boolLogin = false;
				/*sess.removeAttribute("session");
				sess.setAttribute("session",session);*/

				despliegaPaginaError("Problemas al crear el ambiente.", "Creando ambiente", "Login", request, response);
				return;
			}
			if(!verificaFacultad("ACCESINTERNET",request))
			{
				//<VC:CGH> Log para verificar el estado del origen en el canal
				EIGlobal.mensajePorTrace ("REDIR-SUPERNET:" + redirSupernet, EIGlobal.NivelLog.INFO);
				//</VC>
				EIGlobal.mensajePorTrace("***CreaAmbiente.class Facultad ACCESINTERNET False", EIGlobal.NivelLog.INFO);
				boolLogin = false;
				request.setAttribute("MensajeLogin01", " cuadroDialogo(\"No tiene facultad de operar en Enlace Internet <br> con el contrato " +Contrato+ ".\", 3);");

                int resDup = cierraDuplicidad(session.getUserID8());
                session.setSeguir(false);
                //session.guardaHash();
                //session.borraHash();
                //sess.setAttribute("session", session);
                //log( "logout.java::defaultAction() -> resDup: " + resDup );
                //log( "logout.java::defaultAction() -> seguir: " + session.getSeguir() );

				try {
					sess.invalidate();
				}catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

				//<VC:CGH> Redireccion al canal de supernet en el caso que el origen sea este y no te tengan facultades con el contrato seleccionado
				if(redirSupernet != null && "true".equals(redirSupernet)) {
					request.setAttribute("snetuser", session.getUserID8());
					request.setAttribute("snetcont", session.getContractNumber());
					EIGlobal.mensajePorTrace ("FACULTAD-REDIR: SUPERNET " + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
					evalTemplate (Global.URL_SUPERNET,request, response);
				} else {
					EIGlobal.mensajePorTrace ("FACULTAD-REDIR: ENLACE " + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
					evalTemplate (IEnlace.LOGIN,request, response);
				}
				//</VC>
				return;
			} else {
				EIGlobal.mensajePorTrace("***CreaAmbiente.class Facultad ACCESINTERNET True", EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace("***CreaAmbiente entro_cambio_nip vale: "+ entro_cambio_nip, EIGlobal.NivelLog.DEBUG);
			//(inicio)jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
			if (!entro_cambio_nip.equals("1"))
            {

				String coderror = (String) request.getParameter("coderror");
				String[] arrCodError=null;
				arrCodError=desentramaS(coderror,'@');

				if(arrCodError[0].equals("SEG_0001")) {
					request.setAttribute("bs_ca","2");
					request.setAttribute("num_dias",arrCodError[1]);
					request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Contrase&ntilde;a por expirar", "Login &gt; Contrase&ntilde;a por expirar" ,"s26032h",request));
					evalTemplate ( "/jsp/Conf_cambio_nip.jsp", request, response );
					return;
				}
				if (arrCodError[0].equals("SEG_0002"))
				{

					/*VSWF-AHO-I*/
					if (Global.USAR_BITACORAS.trim().equals("ON")){
						try {
						BitaHelper bh = new BitaHelperImpl(request, session, sess);
						bh.incrementaFolioFlujo("EACO");

						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EA_CAMBIO_CONTRASENA_ENTRA);
						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}


							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					}
					/*VSWF-AHO-F*/

					request.setAttribute("Encabezado", CreaEncabezado("Cambio de contrase&ntilde;a","Administraci&oacute;n y Control &gt; Cambio de contrase&ntilde;a", "s26040h", request));
					request.setAttribute ( "MensajeCambioPass", "cuadroDialogo(\"Su password ha expirado, le suplicamos ingresar una nueva contrase�a de 8 a 20 caracteres. \", 4);" );
					evalTemplate ( IEnlace.CAMBIO_PASSWD, request, response );
					return;
				}
			}
			/* A�adir Codigo de  asignaURL y subirlo a la sesion
			*/
			String [][] arregloLocations2 = asignaURLS (request, session);
			String nuevoMenu3 = creaFuncionMenu ( arregloLocations2, request );
			String elMenu3 = creaCodigoMenu (arregloLocations2, request);
			sess.setAttribute ("funcionesMenu",nuevoMenu3);
			session.setFuncionesDeMenu ( nuevoMenu3 );
			sess.setAttribute ("StrMenu",nuevoMenu3);
			session.setStrMenu ( elMenu3 );
			//TODO BIT CU5041, guardar el contrato seleccionado
			//TODO BIT inicio de sesion
			/*VSWF-HGG-I*/
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				log("VSWF: CreaAmbiente Guarda Contrato");
				if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != null){
					BitaHelper bh = new BitaHelperImpl(request, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					log("VSWF: Crea Ambiente " + request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.EA_CAMBIO_CONTRATO)){
						bt.setNumBit(BitaConstants.EA_CAMBIO_CONTRATO_TF_AMBIENTA_CONTRATO);
					}
					if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.E_INICIO_SESION)){
						bt.setNumBit(BitaConstants.E_INICIO_SESION_AMBIENTA_SESION);
						if(session.getToken().getSerialNumber() != null)
							bt.setIdToken(session.getToken().getSerialNumber());
					}
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					if (AmbCod != null && AmbCod.length() >= 8) {
						bt.setIdErr(AmbCod.substring(0, 8));
					}
					else if(AmbCod != null) {
						bt.setIdErr(AmbCod);
					}
					bt.setServTransTux("SAMBIENTE");
					log("VSWF: IdErr " + AmbCod);

					try {
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
				}
			}
			/*VSWF-HGG-F*/

			EIGlobal.mensajePorTrace("***CreaAmbiente.class AmbCod = &" +AmbCod+ "&", EIGlobal.NivelLog.INFO);
			if ("AMBI0000".equals(AmbCod)) {
				avisos = obtenerAvisos(request);


				/*Declaraci�n para envio de mensajes para m�s de un solo contrato   everis   27/06/2008     inicio*/
				mensaje = muestraMensaje(request, response);
				/*Declaraci�n para envio de mensajes para m�s de un solo contrato   everis   27/06/2008     fin   */


            	vencimientosCE=avisosVencimientosCE(request);
				request.setAttribute("plantillaElegida", direccion);
				request.setAttribute("muestraAvisos", avisos);


				/*Llamado para envio de mensajes para m�s de un solo contrato   everis   27/06/2008     inicio*/

				EIGlobal.mensajePorTrace("--- CreaAmbiente Mensaje Login: " + mensaje, EIGlobal.NivelLog.DEBUG);
				if(!"".equals(mensaje)) {
					request.setAttribute("MensajeLogin01", " cuadroDialogoMensaje(\" <tr><td class=textabdatobs align=center ><br> Importante leer los siguientes mensajes: </td></tr>" + mensaje + ".\", 4);");
				}

				/*Llamado para envio de mensajes para m�s de un solo contrato   everis   27/06/2008     fin*/



            	// 17/12/2002 Se agreg� que busque vencimientos (HRZ).
            	request.setAttribute("datosVencimientosCE", vencimientosCE);
				/*sess.removeAttribute("session");
				sess.setAttribute("session",session);*/

				/*
				// **************************************************************
				// Rafael Rosiles
				// Modificacion para verificar si el contrato tiene cuentas CLABE 11 digitos
				// **************************************************************
				//PPM
				int cuentaCLABE;

				EIGlobal.mensajePorTrace( "*** CreaAmbiente modificacion cambio CLABE", EIGlobal.NivelLog.INFO);

				cuentaCLABE = verificaCLABE ( request, response);

				EIGlobal.mensajePorTrace( "*** CreaAmbiente Numero de cuentas CLABE>>"+ String.valueOf(cuentaCLABE), EIGlobal.NivelLog.DEBUG);

				if (cuentaCLABE !=0)
				{
					String ladireccion;
					ladireccion = direccion.substring(direccion.indexOf("'")+1,direccion.length()-1);
					EIGlobal.mensajePorTrace( "*** CreaAmbiente direccion elegida >>" + ladireccion, EIGlobal.NivelLog.INFO);
					request.setAttribute ( "plantillaElegida", "window.open('AvisoCLABE?plantillaOriginal="+ladireccion+"','AvisoCLABE','width=380,height=210,resizable=no,toolbar=no,scrollbars=no,left=210,top=225')" );
				}
				*/
				log("Flujo normal 3");
                evalTemplate(IEnlace.CARGATEMPLATE_TMPL, request, response);
			}
 			else
			{
				EIGlobal.mensajePorTrace("***CreaAmbiente.class AmbCod != 'AMBI0000'", EIGlobal.NivelLog.DEBUG);
				/*sess.removeAttribute("session");
				sess.setAttribute("session",session);*/
				despliegaPaginaError("Problemas al crear el ambiente.", "Creando ambiente", "Login", request, response);
			}
		} else { //if (sesionvalida) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP );
			/*sess.removeAttribute("session");
			sess.setAttribute("session",session);*/
			evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}
	}
}