
/**************************************************************************************************
 **		No: 	Descripcion										Nombre			Fecha
 **
 ** 	1		Servlet que realiza las funciones para dar		Everis			28/04/07
 **				seguimiento a las transferencias directas,
 **				interbancarias y pago directo.
 *************************************************************************************************/

package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.*;

import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


public class consultaServlet extends BaseServlet
{

	/** Everis - Metodo doGet ***/
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		defaultAction( request, response );
	}


	/** Everis - Metodo doPost***/
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		defaultAction( request, response );
	}

	/*************************************************************/
	/** Metodo: defaultAction
	 * @return (execution)
	 * @param request && response
	 * @description: Realiza las funciones para busquedas, genera-
	 *				 cion de estadsticas y archivos.
	/*************************************************************/

	public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		/*******  Validamos que la sesin sea valida  *******/
		if(!SesionValida(request, response)) {
			return;
		}

		System.out.println("----------------------------------------------------------------------");
		System.out.println("-------- entramos al metdo default action - con nuevos request  -----");
		System.out.println("-----------------------------------------------------------------------");

		/****	Creacin e inicializacin de variables 	*****/
		HttpSession sessionHttp = request.getSession();						//Creamos para almacenar variables en sesion
		String entrada = request.getParameter("entrada");					//Indica la funcin dentro del servlet a realizar
		//String folioStatics = request.getParameter("folioStatics");			//Indica el folio para generar estadsticas
		String folioArchivito = request.getParameter("folio");				//Almacena folio al momento del click en el link
		String usuario = request.getParameter("usuario");					//usuario para consultar
		request.removeAttribute("URLArchivo");								//Limpia el atributo del link para descarga

		RequestDispatcher rd = null;										//Carga las pginas jsp que se manden llamar

		/*********	Inicio -  Codigo para calendarios  ************/
		String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
        datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
        datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
        System.out.println(datesrvr);
        request.setAttribute("Consfechas",datesrvr);
        request.setAttribute("diasInhabiles",diasInhabilesAnt());
		/************	Fin -  Codigo para calendarios ****************/

        String[][] arrayUsuarios = TraeUsuarios(request);
		for(int z=1;z<=Integer.parseInt(arrayUsuarios[0][0].trim());z++){
			System.out.println( "Valor del USUARIO ANTES: "+arrayUsuarios[z][1]+"&");
			arrayUsuarios[z][1] = convierteUsr7a8(arrayUsuarios[z][1]);
			System.out.println( "Valor del  DESPUES: "+arrayUsuarios[z][1]+"&");
		}
		request.setAttribute("usuarios", arrayUsuarios);



			System.out.println("------------------ La SESSION es valida ----------------");
			BaseResource session = 	(BaseResource) request.getSession ().getAttribute ("session");
			//String cveUsuario = session.getUserID();
			System.out.println("---------- obtuvo clave de usuario --> " + usuario);

			/**** Se valida que el parmetro no venga con valor nulo *****/
			if (entrada == null)
				entrada = "0";

			/**** Inicio - Se carga los atributos de men princiapal ******/
			try
			{
				request.setAttribute ("MenuPrincipal", session.getStrMenu ());
				request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
	        }
			catch(Exception e)
			{
				e.printStackTrace();
			}
			/**** Fin - Se carga los atributos de men princiapal ******/


			/********************************************************************
			 ** 	OPCION 1 --> ENTRADA DESDE EL MENU Y POR PRIMERA VEZ
			 **		@ Everis - Solucion de Raiz - mayo 07
			 ********************************************************************/
			if (entrada.equals("1") || entrada.equals("0") )
			{
				String opcAutMnc = request.getParameter("opcAutMnc");					//Indica Si se trata de una autorizacion de mancomunidad
				EIGlobal.mensajePorTrace("JGAL - opcAutMnc: [" + opcAutMnc + "]" , EIGlobal.NivelLog.INFO);
				sessionHttp.setAttribute("opcAutMnc", opcAutMnc);
				if (opcAutMnc!=null && opcAutMnc.equals("S"))
					request.setAttribute ("Encabezado", CreaEncabezado ( "Consulta de Folios", "Consulta", "s55260h", request ) );	//Clave ayuda cambiar DVS 21 05 2007
				else
					request.setAttribute ("Encabezado", CreaEncabezado ( "Seguimiento de Transferencias", "Consulta", "s55260h", request ) );	//Clave ayuda cambiar DVS 21 05 2007

				System.out.println("------- Entrada por menu ------");
				sessionHttp.removeAttribute("codigoHTML"); 									// removemos contenido en el cdigo HTML generado.
				sessionHttp.removeAttribute("folioInicialSession");
				sessionHttp.removeAttribute("folioFinalSession");
				sessionHttp.removeAttribute("fechaInicialSession");
				sessionHttp.removeAttribute("fechaFinalSession");
				//rd = request.getRequestDispatcher("/enlaceMig/jsp/consultaFolio.jsp");			// entramos a la paina principal
				rd = request.getRequestDispatcher("/jsp/consultaFolio.jsp");			// entramos a la paina principal
				rd.include(request, response);
				System.out.println("------ Entramos a pgina folioConsulta.jsp: termina entrada = 1 -------");
			}


			/******************************************************************
			 ** 	OPCION 2 --> BUSQUEDA DE FOLIOS Y GENERACION DE RESULTADOS
			 **		@ Everis - Solucion de Raiz - mayo 07
			 ******************************************************************/

			else if (entrada.equals("2") )
			{
				System.out.println("-------- Entrada por que se realiza una BUSQUEDA  -----------");
				String opcAutMnc = (String)sessionHttp.getAttribute("opcAutMnc");
				if (opcAutMnc!=null && opcAutMnc.equals("S"))
					request.setAttribute ("Encabezado", CreaEncabezado ( "Consulta de Folios", "Consulta &gt; Resultados", "s55260h", request ) );	//cambiar clave ayuda
				else
					request.setAttribute ("Encabezado", CreaEncabezado ( "Seguimiento de Transferencias", "Consulta &gt; Resultados", "s55260h", request ) );	//cambiar clave ayuda

				String resultados = ""; 								//String que contiene el cdigo HTML que ser enviado

				/********	Se obtienen valores desde el jsp  **********/
				String folioInicial = request.getParameter("folioInicial");
				String folioFinal = request.getParameter("folioFinal");
				String fechaInicial = request.getParameter("fechaInicial");
				String fechaFinal = request.getParameter("fechaFinal");

				System.out.println("-------- Se obtienen FILTROS para la BUSQUEDA  -----------");
				System.out.println("---- folioInicial: ["+ folioInicial +"]");
				System.out.println("---- folioFinal:   ["+ folioFinal + "]");
				System.out.println("---- fechaInicial: ["+ fechaInicial+ "]");
				System.out.println("---- fechaFinal:   ["+ fechaFinal+ "]");

				/*** Everis - Ingresamos los valores obtenidos para realizar el query	****/
				EIGlobal.mensajePorTrace("--------- Se ejecuta Metodo  [consultaTransferencias()]  -------", EIGlobal.NivelLog.INFO);

				resultados = consultaTransferencias (folioInicial, folioFinal, fechaInicial, fechaFinal, usuario, request);

				/*** Everis - Se suben las variables a la session, para que puedan ser utilizadas desde el JSP	****/
				sessionHttp.setAttribute("codigoHTML", resultados);

				// Validar los " "//
				if (folioInicial.equals(" "))
				{
					folioInicial = "";

				}

				if (folioFinal.equals(" "))
				{
					folioFinal = "";

				}


				try{
					EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

					int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
    				String claveOperacion = BitaConstants.CONSULTA_SEGUIMIENTO_TRANSFERENCIAS;
    				String concepto = BitaConstants.CONCEPTO_CONSULTA_SEGUIMIENTO_TRANSFERENCIAS;

    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

    				@SuppressWarnings("unused")
					String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

				} catch(Exception e) {
					EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

				}



				System.out.println("---- Las variables que envio a enoch folioInical:   ["+ folioInicial+ "]");
				System.out.println("---- Las variables que envio a enoch folioFinal:   ["+ folioFinal+ "]");
				System.out.println("---- Las variables que envio a enoch fechaInicial:   ["+ fechaFinal+ "]");
				System.out.println("---- Las variables que envio a enoch fechaFinal:   ["+ fechaFinal+ "]");
				sessionHttp.setAttribute("folioInicialSession", folioInicial);
				sessionHttp.setAttribute("folioFinalSession", folioFinal);
				sessionHttp.setAttribute("fechaInicialSession", fechaInicial);
				sessionHttp.setAttribute("fechaFinalSession", fechaFinal);
				//rd = request.getRequestDispatcher("/enlaceMig/jsp/consultaFolio.jsp");
				rd = request.getRequestDispatcher("/jsp/consultaFolio.jsp");
				rd.include(request, response);
				EIGlobal.mensajePorTrace("--------- Fin de la BUSQUEDA de FOLIOS : Fin de Entrada = 2 --------", EIGlobal.NivelLog.INFO);
			}


			/***********************************************************
			 ** 	OPCION 3 --> GENERAR ARCHIVO Y URL
			 **		@Everis - Solucion de Raiz - mayo 07
			 ***********************************************************/

			else if (entrada.equals("3"))
			{
				String tipoTransfer = request.getParameter("tipoTransferencia");
				System.out.println("-------- Entrada 3 por que se realizo un click sobre el FOLIO " + folioArchivito+ " y la transferenia es " + tipoTransfer + " -----------");
				request.setAttribute ("Encabezado", CreaEncabezado ( "Seguimiento de Transferencias", "Consulta &gt; Estadisticas Generales", "s55260h", request ) );  //cambiar clave ayuda

				boolean creado = false;							// Verifica que se haya creado el archivo
				String URLArchivo = "";							// Contiene la direccion para la descarga del archivo

				/*****	EVERIS - Se genera el archivo en el /TMP *********/
				try
				{
					EIGlobal.mensajePorTrace("----- Se realiza la construccin del ARCHIVO, con el FOLIO -->" + folioArchivito, EIGlobal.NivelLog.INFO);

					/**** Entramos al metodo generaArchivo() para verificar si se pudo crear *****/
					EIGlobal.mensajePorTrace("     ----- se EJECUTA el metodo generaArchivo() ----- ", EIGlobal.NivelLog.INFO);
					creado = generaArchivo(folioArchivito, tipoTransfer); 					// devuelve true si se creo el archivo satisfactoriamente

					if (creado == false)
					{
						EIGlobal.mensajePorTrace("----------- El archivo no ha podido ser generado ------------- ", EIGlobal.NivelLog.INFO);
						request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"El archivo NO se HA GENERADO satisfactoriamente\", 3);" );
					}

					else
					{
						EIGlobal.mensajePorTrace("----------- El archivo se ha GENERADO satisfactoriamente ------------- ", EIGlobal.NivelLog.INFO);

					}

				}
				catch (Exception e)
				{
					e.printStackTrace();
					EIGlobal.mensajePorTrace("==========	EXISTEN PROBLEMAS PARA GENERAR EL ARCHIVO ============", EIGlobal.NivelLog.INFO);
				}

				/*** Asignamos una direccin URL para la descarga del archivo *****/
				URLArchivo = "\"/Download/" + folioArchivito + "_Respuesta.doc\"";
				request.setAttribute("URLArchivo", URLArchivo);
				System.out.println("-------- El URL para la descarga del ARCHIVO es la siguiente: " + folioArchivito + "  -----------");

				/**** Se hace el reload de la pgina, manteniendo el reporte de la bsqueda ****/
				//rd = request.getRequestDispatcher("/enlaceMig/jsp/consultaFolio.jsp");
				rd = request.getRequestDispatcher("/jsp/consultaFolio.jsp");
				rd.include(request, response);
				System.out.println("-------- Fin de la entrada = 3: La generacin de Archivo  -----------");
			}


	}


	/*************************************************************/
	/** Metodo: consultaTransferencias_Folio ( Everis )
	 * @return int
	 * @param String folio
	 * @description Se obtienen los datos de las diferentes transferencias
	 * 				realizadas de acuerdo a la bsqueda
	/*************************************************************/

	public String consultaTransferencias (String folioTransInicial, String folioTransFinal, String fechaTransInicial, String fechaTransFinal, String cveUsuario, HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("--------- Se ejecuta Metodo consultaTransferencias() ------------", EIGlobal.NivelLog.INFO);

		/*** Creacin e inicializacin de variables ***/
		String codigoHTML = ""; 										//String que contiene la construccin del HTML
		//StringBuffer codigoHTML = new StringBuffer ();
		ResultSet rsFolio = null; 										// Contiene los registros devueltos por la busqueda realizada
		String folioRS = null; 											// Contiene el folio del registro
		//String cveUsuarioRS = null;										// Contiene clave de usuario del registro
		//String contratoRS = null; 										// contiene contrato del registro
		String tipoTransRS = null; 										// contiene el tipo de transferencia del registro
		java.util.Date fechaInicioRS = null;							// contiene fecha de inicio del registro
		java.util.Date fechaFinRS = null; 								// contiene fecha fin de registro
		String tiempoFinRS = null;										// contiene de tiempo de finalizacin del registro
		String numRegsRS = null; 										// numero de registros del registro
		String estadoRS = null; 										// estado del registro
		String regProcRS = null; 										// registros procesados del registro
		String regExitRS = null; 										// registros exitosos
		String time1 = null; 											// contiene tiempo de inicio
		String time2 = null; 											// contiene tiempo de finalizacion aproximada
		//String minutos1 = "";											// para cortarlos en minutos
		//String minutos2 = "";											// para cortarlos en minutos
		String avanceRS = "0";

		boolean esObscuro = true;										// bandera para colorear tabla
		int renglones = 0;												// contiene numero de registros por consulta
		HttpSession sessionHttp = request.getSession();					// pondr los valores en la sesion
		BaseResource session = (BaseResource) sessionHttp.getAttribute("session");
		String opcAutMnc = (String)sessionHttp.getAttribute("opcAutMnc");
		String filtroManc = "";
		Connection conn = null;
		Statement statementFolio = null;
		if (opcAutMnc!=null && opcAutMnc.equals("S"))
			filtroManc = " AND TIPO_TRAN = 'MNCA' ";

		String fileOut="";
		fileOut = session.getUserID8() + "cs.doc";
		boolean grabaArchivo = true;
		String cadena = "";

		EI_Exportar ArcSal;
		ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
		EIGlobal.mensajePorTrace( "***Se verificar� que el archivo no exista en caso contrario se borrara y crear� uno nuevo", EIGlobal.NivelLog.DEBUG);
		ArcSal.creaArchivo();

		/*** Everis - Parametros de Entrada ()  ***/
		EIGlobal.mensajePorTrace("------- Variables Que se ingresaron en el Metodo ------", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- FOLIO INICIAL = [" + folioTransInicial+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- FOLIO FINAL   = [" + folioTransFinal+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- FECHA INICIAL = [" + fechaTransInicial+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- FECHA FINAL   = [" + fechaTransFinal+ "]", EIGlobal.NivelLog.INFO);


		EIGlobal.mensajePorTrace("--------- Se comienzan a validar los datos para utilizarlos en el query  ---------", EIGlobal.NivelLog.INFO);

		/***** INICIO - Si los folios vienen vacios ******/
		if (folioTransFinal == null && folioTransInicial == null)
		{
			EIGlobal.mensajePorTrace("--------- Los FOLIOS vienen == NULL ---------", EIGlobal.NivelLog.INFO);
			folioTransInicial = " ";
			folioTransFinal = " ";
		}

		else if (folioTransFinal.equals("null") && folioTransInicial.equals("null"))
		{
			EIGlobal.mensajePorTrace("---------Los FOLIOS vienen equals(NULL) ---------", EIGlobal.NivelLog.INFO);
			folioTransInicial = " ";
			folioTransFinal = " ";
		}

		else if (folioTransInicial.equals("") && folioTransFinal.equals(""))
		{
			EIGlobal.mensajePorTrace("--------- Los FOLIOS vienen vacios ---------", EIGlobal.NivelLog.INFO);
			folioTransInicial = " ";
			folioTransFinal = " ";
		}
		/***** FIN - Si los folios vienen vacios ******/

		/*** Everis - Se despliegan valores finales que iran a la consulta   ***/
		EIGlobal.mensajePorTrace("--------- ESTOS SON LO VALORES FINALES QUE VAN A LA CONSULTA ----- ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- Folio Inicial = [" + folioTransInicial+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- Folio Final   = [" + folioTransFinal+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- Fecha Inicial = [" + fechaTransInicial+ "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("--------- Fecha final   = [" + fechaTransFinal+ "]", EIGlobal.NivelLog.INFO);


		/*** EVERIS 05/05/2007 - Inicia conexion hacia la base de datos de Oracle para realizar queries ***/
		try
		{
			EIGlobal.mensajePorTrace("--------- Comenzamos a realizar los QUERIES -----------", EIGlobal.NivelLog.INFO);
			conn = createiASConn (Global.DATASOURCE_ORACLE);
			statementFolio = conn.createStatement();

			/***** No hay folios ******/
			if (folioTransFinal.equals(" ") && folioTransInicial.equals(" "))
			{
				EIGlobal.mensajePorTrace("--------- LOS FOLIOS SON NULL -----------", EIGlobal.NivelLog.INFO);
				String queryFolio;


				if(cveUsuario == null || cveUsuario.equals("")) {
					queryFolio = "SELECT LPAD(FOLIO, 12, '0') AS FOLIO, TIPO_TRAN, FCH_INICIO, FCH_FIN, (FCH_FIN - FCH_INICIO)*1410 AS TMP_FIN, " +
							"NUM_REGS, ESTADO, REG_PROC, REG_EXIT, TRUNC((REG_PROC*100)/NUM_REGS) AS AVANCE  FROM EWEB_TRANSAC_PROC" +
							" WHERE CONTRATO = '" + session.getContractNumber() + "' " + filtroManc +
							" AND (TRUNC(FCH_INICIO) BETWEEN TO_DATE('" + fechaTransInicial + "', 'DD/MM/YYYY') " +
							" AND TO_DATE ('" + fechaTransFinal +"', 'DD/MM/YYYY') OR (ESTADO = 'G')) order by FOLIO desc";
				}
				else {
					queryFolio = "SELECT LPAD(FOLIO, 12, '0') AS FOLIO, TIPO_TRAN, FCH_INICIO, FCH_FIN, (FCH_FIN - FCH_INICIO)*1410 AS TMP_FIN, " +
							"NUM_REGS, ESTADO, REG_PROC, REG_EXIT, TRUNC((REG_PROC*100)/NUM_REGS) AS AVANCE  FROM EWEB_TRANSAC_PROC " +
							"WHERE CVE_USUARIO = '" + BaseServlet.convierteUsr7a8(cveUsuario) + "' " +
							" AND CONTRATO = '" + session.getContractNumber() + "' " + filtroManc +
							" AND (TRUNC(FCH_INICIO) BETWEEN TO_DATE('" + fechaTransInicial + "', 'DD/MM/YYYY') AND TO_DATE ('" + fechaTransFinal +"', 'DD/MM/YYYY') OR (ESTADO = 'G')) order by FOLIO desc";
				}
				rsFolio = statementFolio.executeQuery(queryFolio);
			}

			/****** Se busca un rango de folios  *******/
			else
			{
				EIGlobal.mensajePorTrace("--------- Los FOLIOS son un RANGO -----------", EIGlobal.NivelLog.INFO);
				String queryFolio = null;
				if(cveUsuario == null || cveUsuario.equals("")) {
					queryFolio = "SELECT LPAD(FOLIO, 12, '0') AS FOLIO, TIPO_TRAN, FCH_INICIO, FCH_FIN, (FCH_FIN - FCH_INICIO)*1410 AS TMP_FIN, " +
							"NUM_REGS, ESTADO, REG_PROC, REG_EXIT, TRUNC((REG_PROC*100)/NUM_REGS) AS AVANCE FROM EWEB_TRANSAC_PROC " +
							"WHERE CONTRATO = '" + session.getContractNumber() + "' " + filtroManc +
							" AND FOLIO BETWEEN TO_NUMBER('" + folioTransInicial + "') AND TO_NUMBER('" + folioTransFinal + "') " +
							" AND (TRUNC(FCH_INICIO) BETWEEN TO_DATE('" + fechaTransInicial + "', 'DD/MM/YYYY')" +
							" AND TO_DATE ('" + fechaTransFinal +"', 'DD/MM/YYYY') OR (ESTADO = 'G'))order by FOLIO desc";
				}
				else {
					queryFolio = "SELECT LPAD(FOLIO, 12, '0') AS FOLIO, TIPO_TRAN, FCH_INICIO, FCH_FIN, (FCH_FIN - FCH_INICIO)*1410 AS TMP_FIN, " +
							"NUM_REGS, ESTADO, REG_PROC, REG_EXIT, TRUNC((REG_PROC*100)/NUM_REGS) AS AVANCE FROM EWEB_TRANSAC_PROC " +
							"WHERE CVE_USUARIO = '" + BaseServlet.convierteUsr7a8(cveUsuario) + "' AND CONTRATO = '" + session.getContractNumber() + "' " + filtroManc +
							" AND FOLIO BETWEEN TO_NUMBER('" + folioTransInicial + "') AND TO_NUMBER('" + folioTransFinal + "') " +
							" AND (TRUNC(FCH_INICIO) BETWEEN TO_DATE('" + fechaTransInicial + "', 'DD/MM/YYYY') " +
							" AND TO_DATE ('" + fechaTransFinal +"', 'DD/MM/YYYY') OR (ESTADO = 'G'))order by FOLIO desc";
				}

				rsFolio = statementFolio.executeQuery(queryFolio);
			}


	 		// EVERIS - CARGA DE DATOS EN TABLAS
			codigoHTML += "<br></br>";
			codigoHTML += "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
			codigoHTML += "<tr>";
			if (opcAutMnc!=null && opcAutMnc.equals("S"))
				codigoHTML += "<th colspan=\"12\" class=\"tittabdat\">REPORTE DE AUTORIZACIONES REALIZADAS EN EL PER&Iacute;ODO DEL " + fechaTransInicial + " AL " + fechaTransFinal + " </th>";
			else
				codigoHTML += "<th colspan=\"12\" class=\"tittabdat\">REPORTE DE TRANSFERENCIAS REALIZADAS EN EL PER&Iacute;ODO DEL " + fechaTransInicial + " AL " + fechaTransFinal + " </th>";
			codigoHTML += "</tr>";
			codigoHTML += "<tr>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Folio</th>";
			if (opcAutMnc!=null && opcAutMnc.equals("S"))
				codigoHTML += "<th class=\"tittabdat\" align=center>Autorizaci&oacute;n</th>";
			else
				codigoHTML += "<th class=\"tittabdat\" align=center>Transferencia</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Fecha de Inicio</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Fecha de T&eacute;rmino</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Duraci&oacute;n</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Registros</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Estado</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Procesados</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Exitosos</th>";
			codigoHTML += "<th class=\"tittabdat\" align=center>Avance</th>";
			codigoHTML += "</tr>";

			/*******	 Llenamos la tabla con los datos del resulset qeu se obtienen **********/
			while (rsFolio.next())
			{
				folioRS = rsFolio.getString("FOLIO");
				tipoTransRS = rsFolio.getString("TIPO_TRAN");

				/***** Convertimos a texto las siglas del tipo de transaccion*****/
				String tipoTransCompleta = "";
				if (tipoTransRS.equals("TRAN"))
					tipoTransCompleta = "Internas";
				else if (tipoTransRS.equals("DIBT"))
					tipoTransCompleta = "Interbancarias";
				else if (tipoTransRS.equals("PAGD"))
					tipoTransCompleta = "Pago Directo";
				else if("ATIM".equals(tipoTransRS))
					tipoTransCompleta = "Tarjeta de Pagos Santander Alta por Archivo";//YHG NPRE
				else if("PNIS".equals(tipoTransRS))
					tipoTransCompleta = "Tarjeta de Pagos Santander Pago Individual";//YHG NPRE
				else if("PNOS".equals(tipoTransRS))
					tipoTransCompleta = "Tarjeta de Pagos Santander  Pago por Archivo";//YHG NPRE
				else if (tipoTransRS.equals("IN04"))
					tipoTransCompleta = "Nomina Interna";
				else if (tipoTransRS.equals("MNCA"))
					tipoTransCompleta = "Autorizaci�n de Mancomunidad";
				else if (tipoTransRS.equals("PNLI"))
					tipoTransCompleta = "Nomina en Linea";
				else
					tipoTransCompleta = "No definido";
				/***** Fin de Convertimos a texto las siglas del tipo de transaccion*****/

				fechaInicioRS = rsFolio.getDate("FCH_INICIO");
				time1 = String.valueOf(rsFolio.getTime("FCH_INICIO"));

				/***** Inicio - validamos que no vengan nulas ni vacias las fechas *****/
				EIGlobal.mensajePorTrace("--> time1: [" + time1 + "]", EIGlobal.NivelLog.INFO);
				if (time1 == null || time1.equals(""))
				{
					EIGlobal.mensajePorTrace("--------- entre en if 1 ------", EIGlobal.NivelLog.INFO);
					//time1 = time1;
				}
				else
				{
					if (!time1.equals("null"))
					{
						EIGlobal.mensajePorTrace("--------- entre en else 1 ------", EIGlobal.NivelLog.INFO);
						if (time1.length() >= 5)
							time1 = time1.substring(0, 5);
					}
				}


				fechaFinRS = rsFolio.getDate("FCH_FIN");
				time2 = String.valueOf(rsFolio.getTime("FCH_FIN"));
				EIGlobal.mensajePorTrace("--> time2: [" + time2 + "]", EIGlobal.NivelLog.INFO);
				if (time2 == null || time2.equals(""))
				{
					EIGlobal.mensajePorTrace("--------- entre en if 2 ------", EIGlobal.NivelLog.INFO);
					//time2 = time2;
				}
				else
				{
					if (!time2.equals("null"))
					{
						EIGlobal.mensajePorTrace("--------- entre en else 2 ------", EIGlobal.NivelLog.INFO);
						if (time2.length() >= 5)
							time2 = time2.substring(0, 5);
					}
				}

				tiempoFinRS = String.valueOf(rsFolio.getInt("TMP_FIN"));
				numRegsRS = String.valueOf(rsFolio.getInt("NUM_REGS"));
				estadoRS = rsFolio.getString("ESTADO");

				/***** Convertimos a texto las siglas del estado de la transaccion*****/
				String estadoCompleta = "";
				if (estadoRS.equals("T"))
					estadoCompleta = "Terminado";
				else if (estadoRS.equals("Q"))
					estadoCompleta = "Encolado";
				else if (estadoRS.equals("P"))
					estadoCompleta = "En Proceso";
				else if (estadoRS.equals("F"))
					estadoCompleta = "Fallido";
				else if (estadoRS.equals("G"))
					estadoCompleta = "Cargando Datos";
				else if (estadoRS.equals("D"))
					estadoCompleta = "Duplicado";
				else
					tipoTransCompleta = "No definido";
				/***** Fin de Convertimos a texto las siglas del estado de transaccion*****/

				regProcRS = String.valueOf(rsFolio.getInt("REG_PROC"));
				regExitRS = String.valueOf(rsFolio.getInt("REG_EXIT"));
				avanceRS = String.valueOf(rsFolio.getInt("AVANCE"));
				avanceRS = avanceRS + "%";


//				/**** Inicio -  Cdigo para no null en la fecha en pantalla ****/
//				if (fechaInicioRS == null || fechaInicioRS.equals("") || fechaInicioRS.equals("null"))
//				{
//					EIGlobal.mensajePorTrace("------- fecha de inicio == null o equals('') o equals('null') ------", EIGlobal.NivelLog.INFO);
//					fechaInicioRS = "";
//				}
//
//				if (fechaFinRS == null || fechaFinRS.equals("") || fechaFinRS.equals("null"))
//				{
//					EIGlobal.mensajePorTrace("------- fecha de fin == null o equals('') o equals('null') ------", EIGlobal.NivelLog.INFO);
//					fechaFinRS = "";
//				}
//
//				if (time1 == null || time1.equals("") || time1.equals("null"))
//				{
//					EIGlobal.mensajePorTrace("------- time1 == null o equals('') o equals('null') ------", EIGlobal.NivelLog.INFO);
//					time1 = " ";
//				}
//
//				if (time2 == null || time2.equals("") || time2.equals("null"))
//				{
//					EIGlobal.mensajePorTrace("------- time2 == null o equals('') o equals('null') ------", EIGlobal.NivelLog.INFO);
//					time2 = " ";
//				}
//				/**** Fin - Cdigo para no null en la fecha en pantalla ****/
//
//				EIGlobal.mensajePorTrace("------- Van a la tabla as FInicio ["+ fechaInicioRS+"] ------", EIGlobal.NivelLog.INFO);
//				EIGlobal.mensajePorTrace("------- Van a la tabla as FFin    ["+ fechaFinRS+"] ------", EIGlobal.NivelLog.INFO);
//				EIGlobal.mensajePorTrace("------- Van a la tabla as time1   ["+ time1+"] ------", EIGlobal.NivelLog.INFO);
//				EIGlobal.mensajePorTrace("------- Van a la tabla as time2   ["+ time2+"]------", EIGlobal.NivelLog.INFO);
//



				codigoHTML += "<tr>";

				if (esObscuro)
				{
					/*** Validamos que al archivo este terminado ***/
					if (estadoRS.equals("T"))
						codigoHTML += "<td><A href=\"consultaServlet?entrada=3&folio=" + folioRS + "&tipoTransferencia=" + tipoTransCompleta + "\" class=\"textabdatobs\">" + folioRS + "</A></td>";

						//request.setAttribute("codigoHTML", resultados);
					else
						codigoHTML += "<td class=\"textabdatobs\">" + folioRS + "</A></td>";

					codigoHTML += "<td class=\"textabdatobs\">" + tipoTransCompleta + "</td>";


					/****************	para eliminar null ***********************/
					if ((fechaInicioRS == null || fechaInicioRS.equals("") || fechaInicioRS.equals("null")) && (time1 == null || time1.equals("") || time1.equals("null")))
					{
						EIGlobal.mensajePorTrace("------- la fecha y la hora viene null ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<td class=\"textabdatobs\">      </th>";
					}
					else
					{
						EIGlobal.mensajePorTrace("------- no vienen nulos ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<td class=\"textabdatobs\">" + fechaInicioRS + "  " + time1 + "</td>";
					}


					if ((fechaFinRS == null || fechaFinRS.equals("") || fechaFinRS.equals("null")) && (time2 == null || time2.equals("") || time2.equals("null")))
					{
						EIGlobal.mensajePorTrace("------- la fecha y la hora viene null ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<th class=\"textabdatobs\" align=center>      </th>";
					}
					else
					{
						EIGlobal.mensajePorTrace("------- no vienen nulos ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<td class=\"textabdatobs\">" + fechaFinRS + "  " + time2 + "</td>";
					}
					/****************TERMINA Para eliminar null ***********************/

					codigoHTML += "<td class=\"textabdatobs\" align=right>" + tiempoFinRS + " min</td>";
					codigoHTML += "<td class=\"textabdatobs\" align=right>" + numRegsRS + "</td>";
					codigoHTML += "<td class=\"textabdatobs\">" + estadoCompleta + "</td>";
					codigoHTML += "<td class=\"textabdatobs\" align=right>" + regProcRS + "</td>";
					codigoHTML += "<td class=\"textabdatobs\" align=right>" + regExitRS + "</td>";
					codigoHTML += "<td class=\"textabdatobs\" align=right>" + avanceRS + "</td>";
					codigoHTML += "</tr>";

					esObscuro = false;
				}

				else
				{
					/*** Validamos que al archivo este terminado ***/
					if (estadoRS.equals("T"))
						codigoHTML += "<td><A href=\"consultaServlet?entrada=3&folio=" + folioRS + "&tipoTransferencia=" + tipoTransCompleta + "\" class=\"textabdatcla\">" + folioRS + "</A></td>";

						//request.setAttribute("codigoHTML", resultados);
					else
						codigoHTML += "<td class=\"textabdatcla\">" + folioRS + "</A></td>";

					codigoHTML += "<td class=\"textabdatcla\">" + tipoTransCompleta + "</td>";

					/****************	para eliminar null ***********************/
					if ((fechaInicioRS == null || fechaInicioRS.equals("") || fechaInicioRS.equals("null")) && (time1 == null || time1.equals("") || time1.equals("null")))
					{
						EIGlobal.mensajePorTrace("------- la fecha y la hora viene null ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<td class=\"textabdatcla\">      </th>";
					}
					else
					{
						EIGlobal.mensajePorTrace("------- no vienen nulos ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<td class=\"textabdatcla\">" + fechaInicioRS + "  " + time1 + "</td>";
					}


					if ((fechaFinRS == null || fechaFinRS.equals("") || fechaFinRS.equals("null")) && (time2 == null || time2.equals("") || time2.equals("null")))
					{
						EIGlobal.mensajePorTrace("------- la fecha y la hora viene null ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<th class=\"textabdatcla\" align=center>      </th>";
					}
					else
					{
						EIGlobal.mensajePorTrace("------- no vienen nulos ------", EIGlobal.NivelLog.INFO);
						codigoHTML += "<td class=\"textabdatcla\">" + fechaFinRS + "  " + time2 + "</td>";
					}
					/****************TERMINA Para eliminar null ***********************/


					codigoHTML += "<td class=\"textabdatcla\" align=right>" + tiempoFinRS + " min</td>";
					codigoHTML += "<td class=\"textabdatcla\" align=right>" + numRegsRS + "</td>";
					codigoHTML += "<td class=\"textabdatcla\">" + estadoCompleta + "</td>";
					codigoHTML += "<td class=\"textabdatcla\" align=right>" + regProcRS + "</td>";
					codigoHTML += "<td class=\"textabdatcla\" align=right>" + regExitRS + "</td>";
					codigoHTML += "<td class=\"textabdatcla\" align=right>" + avanceRS + "</td>";
					codigoHTML += "</tr>";

					esObscuro = true;

				}


				if(grabaArchivo){
					String fechaInicioString = "";
					String fechaFinString = "";
					if (fechaInicioRS != null) {
						fechaInicioString = fechaInicioRS.toString();
				}
					if (fechaFinRS != null) {
						fechaFinString = fechaFinRS.toString();
					}
					if (tipoTransCompleta.contains("N&oacute;mina")) {
						tipoTransCompleta.replaceAll("N&oacute;mina", "N�mina");
					}

					cadena = folioRS.concat(" ").concat(tipoTransCompleta).concat(" \t").concat(fechaInicioString).concat(" \t")
									.concat(fechaFinString).concat(" \t").concat(tiempoFinRS).concat(" min").concat(" \t")
									.concat(numRegsRS).concat(" \t").concat(estadoCompleta).concat(" \t")
									.concat(regProcRS).concat(" \t").concat(regExitRS).concat(" \t")
									.concat(avanceRS).concat(" \t");

					ArcSal.escribeLinea(cadena + "\r\n");
					EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  " + cadena, EIGlobal.NivelLog.DEBUG);
				}

				EIGlobal.mensajePorTrace(" ----- los valores en las tablas esel folio  [" + folioRS + "]", EIGlobal.NivelLog.INFO);
				renglones++;
			}

  			if( grabaArchivo ) {
  				ArcSal.cierraArchivo();
  			}

  			ArchivoRemoto archR = new ArchivoRemoto();
  			if(!archR.copiaLocalARemoto(fileOut,"WEB")) {
  				EIGlobal.mensajePorTrace("***consultaServlet.class No se pudo crear archivo para exportar seguimiento de transferencias", EIGlobal.NivelLog.ERROR);
  			}

			codigoHTML += "</table>";

			codigoHTML += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
			codigoHTML += "<tr><td>&nbsp;</td></tr>";
			codigoHTML += "<tr>";
			codigoHTML += "<td align=center class=texfootpagneg>";
			codigoHTML += "El total de resultados generados con base en la b&uacute;squeda realizada son:" + renglones + " <br><br>";
 			codigoHTML += "</td>";
			codigoHTML += "</tr>";

			if (renglones > 0) {
				codigoHTML += "<tr>";
				codigoHTML += "<td align=center>";
				codigoHTML += "<a href = \"/Download/" + fileOut + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a><br><br>";
	 			codigoHTML += "</td>";
				codigoHTML += "</tr>";
			}

            codigoHTML += "</table>";
			/*********	 EVERIS - FIN DE CONSTRUCCION DE HTML ***********/

			EIGlobal.mensajePorTrace("--------- Se encontraron [" + renglones + "] en su busqueda  ---------", EIGlobal.NivelLog.INFO);
			if (renglones == 0)
				request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La busqueda enviada no ha generado <br> resultado alguno.\", 3);");

			statementFolio.close();
		}

		catch (SQLException e)
		{
			e.printStackTrace();
			EIGlobal.mensajePorTrace("==========	EXISTEN PROBLEMAS PARA DEVOLVER LA CONSULTA INDICADA ============", EIGlobal.NivelLog.INFO);
		} finally {
			EI_Query.cierraConexion(rsFolio, statementFolio, conn);
		}
		return codigoHTML;

	}


	/*************************************************************/
	/** Metodo: generaArhivo ( Everis )
	 * @return boolean
	 * @param el folio del archivo a descargar
	 * @description Se obtiene el archivo con las tramas de cada uno.
	 *					de los registros del folio especificado.
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	/*************************************************************/

	public boolean generaArchivo (String folio, String tipoTransfer)
	{
		EIGlobal.mensajePorTrace("--------- ENTRAMOS AL METODO generaArchivo()  con el salto de linea -------", EIGlobal.NivelLog.INFO);

		ResultSet rsArchivo = null; 		//Obtiene registros para generar el archivo
		int renglones = 0;					// Guarda # de lineas del archivo a crear
		String respuesta = null;			// Guarda campo RESPUESTA
		boolean creado = true;				// Guarda respuesta del metodo
		String lineaEscrita = ""; 			// linea + un salto de linea
		Connection conn = null;
		Statement statementArchivo = null;

		EIGlobal.mensajePorTrace("----------  Creamos archivo a Exportar ->" + folio +"_Respuesta.doc ----------", EIGlobal.NivelLog.INFO);
		EI_Exportar archivoExportado = new EI_Exportar(IEnlace.DOWNLOAD_PATH + folio + "_Respuesta.doc");
		archivoExportado.creaArchivo();

		/*** EVERIS 05/05/2007 - Inicia conexion hacia la base de datos de Oracle ***/
		try
		{
			conn = createiASConn (Global.DATASOURCE_ORACLE);
			statementArchivo = conn.createStatement();

			/*** Everis - Consulta por un rango de folios y un rango de fechas  ***/
			EIGlobal.mensajePorTrace("--------- Ejecutamos query para traer lineas y llenar archivo  -----------", EIGlobal.NivelLog.INFO);
			String queryArchivo = "SELECT RESPUESTA FROM EWEB_TRANSAC_DETAIL WHERE FOLIO = TO_NUMBER('" + folio + "') ORDER BY NUM_LINEA ASC";
			rsArchivo = statementArchivo.executeQuery(queryArchivo);

			/*** Everis - Realizamso el llenado del archivo ***/
			EIGlobal.mensajePorTrace("--------- Comenzamos la escritura del archivo  -----------", EIGlobal.NivelLog.INFO);
			while (rsArchivo.next())
			{
				respuesta = rsArchivo.getString("RESPUESTA");


				lineaEscrita = (respuesta);
				//System.out.println("la linea >"+lineaEscrita+"< Termina con \\n?");
				if (lineaEscrita !=  null){
					//System.out.println(lineaEscrita.endsWith("\n"));
					if( !lineaEscrita.endsWith("\n") ){
						lineaEscrita += "\n";
						//System.out.println("agrego n GENERICO");
						}
					archivoExportado.escribeLinea(lineaEscrita);
				}
				else {
					lineaEscrita = "";
					//System.out.println("no se escribio la linea nulla");
				}

				/*if (tipoTransfer.equals("Pago Directo"))
				{
					lineaEscrita += "\n";
					System.out.println("agrego n PD");
				}

				else if (tipoTransfer.equals("Internas"))
				{
					lineaEscrita = lineaEscrita;
					System.out.println("NO agrego n Internas");
				}

				else if (tipoTransfer.equals("Interbancarias"))
				{
					lineaEscrita += "\n";
					System.out.println("agrego n IBancarias");
				}
				*/
				//System.out.println("Linea " + renglones + " ->" + lineaEscrita + "<-");

				renglones++;
			}
			archivoExportado.cierraArchivo();
			EIGlobal.mensajePorTrace("--------- El total de renglones escritos en el archivo son -->" + renglones + " ---------nuevo path", EIGlobal.NivelLog.INFO);

			/*** Everis - Realizando RCP del archivo 11062007 ***/
			ArchivoRemoto archR = new ArchivoRemoto();
			if(!archR.copiaLocalARemoto(folio + "_Respuesta.doc","WEB"))
				 EIGlobal.mensajePorTrace("consultaServlet - generaArchivo(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			EIGlobal.mensajePorTrace("==========	EXISTEN PROBLEMAS PARA DEVOLVER LA CONSULTA INDICADA ============", EIGlobal.NivelLog.INFO);
		} finally {
			EI_Query.cierraConexion(rsArchivo, statementArchivo, conn);
		}
		return creado;
	}

}