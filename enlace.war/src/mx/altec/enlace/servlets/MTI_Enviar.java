/**
 * Isban Mexico
 *   Clase: MTI_Enviar.java
 *   Descripci�n:
 *
 *   Control de Cambios:
 *   1.0 3/07/2013 Stefanini - Creaci�n
 */
package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.DatosBeneficiarioBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosBeneficiarioBO;
import mx.altec.enlace.bo.DatosMancInter;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.OperacionesInternacionalesBO;
import mx.altec.enlace.bo.TrxGPF2BO;
import mx.altec.enlace.bo.TrxPE80BO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * The Class MTI_Enviar.
 * VSWF
 */
public class MTI_Enviar extends BaseServlet {

    /** The Constant ERROR. */
    private static final String ERROR = "ERROR";

    /** The Constant TOTAL_TRANS. */
    private static final String TOTAL_TRANS = "TotalTrans";

    /** The PIPE. */
    private static final String PIPE = "|";
    
    /** The tipo operacion. */
    private static final String TIPO_OPERACION = "DITA";
    /** Constante para CveEspecial */
    private static final String CVE_ESPECIAL = "CveEspecial";
    /** Constante para logBitacora.txt */
    private static final String LOG_BITACORA = "logBitacora.txt";
    /** Constante para DITA0000 */
    private static final String DITA0000 = "DITA0000";
    /** Constante para 0000 */
    private static final String C_0000 = "0000";
    /** Constante para DA */
    private static final String DA = "DA";
    /** Constante para USD */
    private static final String USD = "USD";

    /** The tran exito. */
    private boolean tranExito = false; // STFQRO 31012011 Si el GPA0 trae info. se
                                       // vuelve true

    /** The tcv hora. */
    private transient String tcvHora = "";

    /** The tcv compra. */
    private transient String tcvCompra = "";

    /** The tcv venta. */
    private transient String tcvVenta = "";
    

    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
	@Override
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException {
		defaultAction( req, res );
	}

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
	@Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		defaultAction( req, res );
	}

	/**
	 * Default action.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

        /* Tiempo ... */
		String tiempoEspera = "";
		String contrato="";
		String usuario="";
	    String clavePerfil="";
		String[][] arrayCuentasCargo=null;

		int contMin1=0;
		int contSeg1=0;
		int contMin2=0;
		int contSeg2=0;
		float tiempoTotal=0;
		boolean arcExp=false;

		String enc="UTF-8";

		EI_Tipo Todas=new EI_Tipo(this);

        EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Entrando a Envio de Transferencias.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

        boolean sesionvalida = SesionValida( req, res );
        if(sesionvalida)
        {
            String valida = req.getParameter ( "valida" );
            if(validaPeticion( req,  res,session,sess,valida)){
                EIGlobal.mensajePorTrace("\n\nENTRA A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.INFO);
            }
            else
			{

	        	try {

					contrato=session.getContractNumber();

					usuario=session.getUserID8();
					clavePerfil=session.getUserProfile();

					if("Archivo de Exportacion".equals(((String) req.getParameter(TOTAL_TRANS)).trim())) {
						arcExp=true;
					} else {
						try {

							String encoded = getFormParameter(req,"encoded");
							if (encoded == null){
								encoded = "false";
							}

							if (encoded == null || "true".equals(encoded)) {
								EIGlobal.mensajePorTrace("<--------OIVI validacion del correcto encodig-------> TotalTrans [" + req.getParameter(TOTAL_TRANS) + "]", EIGlobal.NivelLog.DEBUG);
								Todas.iniciaObjeto(new String(req.getParameter(TOTAL_TRANS).getBytes("ISO-8859-1"),"UTF-8")   );
								tiempoEspera = java.net.URLDecoder.decode((String)req.getParameter("Tiempo"),enc);
							} else {
								Todas.iniciaObjeto(req.getParameter(TOTAL_TRANS));
								tiempoEspera = java.net.URLDecoder.decode((String)req.getParameter("Tiempo"),enc);
							}

						}
						catch (Exception e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}
					}

					contMin1=Integer.parseInt(tiempoEspera.substring(0,2));
					contSeg1=Integer.parseInt(tiempoEspera.substring(3,5));
					contMin2=Integer.parseInt(EnlaceGlobal.fechaHoy("tm").trim());
					contSeg2=Integer.parseInt(EnlaceGlobal.fechaHoy("ts").trim());
					if(contMin1>50 && contMin2<=50) {
					  contMin2+=60;
					}
					contMin1=(contMin1*60)+contSeg1;
					EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Tiempo Total minutos 1: "+ Integer.valueOf(contMin1), EIGlobal.NivelLog.INFO);
					contMin2=(contMin2*60)+contSeg2;
					EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Tiempo Total minutos 2: "+ Integer.valueOf(contMin2), EIGlobal.NivelLog.INFO);
					tiempoTotal=(float)((contMin2-contMin1)/60);
					EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Tiempo Total: "+ Float.valueOf(tiempoTotal), EIGlobal.NivelLog.INFO);
					if(tiempoTotal>=3.0) {
				     despliegaTiempoTerminado(req, res);
					} else {

						String validaChallenge = req.getAttribute("challngeExito") != null
							? req.getAttribute("challngeExito").toString() : "";

						EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

						if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
							EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
							ValidaOTP.guardaParametrosEnSession(req);

							validacionesRSA(req, res);
							return;
						}

						boolean valBitacora = true;
						if(  valida==null)
						{
							EIGlobal.mensajePorTrace("\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP\n", EIGlobal.NivelLog.INFO);

							boolean solVal=ValidaOTP.solicitaValidacion (
										session.getContractNumber(),IEnlace.TRANS_INTERNAC);

							String ctasNoReg = (String)req.getSession().getAttribute("NoRegistradas");
							String ctasReg = (String)req.getSession().getAttribute("Registradas");
							if ( ctasNoReg.equals("") ) {
								EIGlobal.mensajePorTrace("\nCuentas registradas, se solicita contrase�a din�mica del token: " + ctasReg + " \n", EIGlobal.NivelLog.INFO);
							} else {
								EIGlobal.mensajePorTrace("\nCuentas No registradas, no se solicita contrase�a din�mica del token : " + ctasNoReg + "\n", EIGlobal.NivelLog.INFO);
							}

							if( session.getValidarToken() &&
								    session.getFacultad(session.FAC_VAL_OTP) &&
								    session.getToken().getStatus() == 1 &&
								    solVal
								    )
							{
								//VSWF-HGG -- Bandera para guardar el token en la bit�cora
								req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);

								EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);

								ValidaOTP.guardaParametrosEnSession(req);

								req.getSession().removeAttribute("mensajeSession");
								ValidaOTP.mensajeOTP(req, "TransInternac");

								ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
							}
							else{
	                                ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
	                                valida="1";
	                                valBitacora = false;
								}
						}
						//retoma el flujo
						if( valida!=null && "1".equals(valida)){


							if (valBitacora)
							{
								try{
									HttpSession sessionBit = req.getSession(false);
									Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion = BitaConstants.CTK_TRANSFERENCIAS_INTER;
									String concepto = BitaConstants.CTK_CONCEPTO_TRANSFERENCIAS_INTER;
									String token = "0";
									if (req.getParameter("token") != null && !"".equals(req.getParameter("token")))
									{token = req.getParameter("token");}
									double importeDouble = 0;
									String cuentaCargo = "0";
									String cuentaDestino = "0";

									if (tmpParametros!=null ) {
										Enumeration enumer = req.getParameterNames();
										System.out.println("*****************************entro if ***********->");
										while(enumer.hasMoreElements()) {
											String name = (String)enumer.nextElement();
											System.out.println("Parametro " + name + "->" + tmpParametros.get(name));
											EIGlobal.mensajePorTrace("Parametro " + name + "->" + tmpParametros.get(name), EIGlobal.NivelLog.INFO);
										}

										String montoDivisa = tmpParametros.get("ImporteT").toString();
										montoDivisa = montoDivisa.substring(montoDivisa.indexOf('$')+1).trim();
										importeDouble = Double.valueOf(montoDivisa);

										cuentaCargo = tmpParametros.get("Cuentas").toString();
										if(cuentaCargo.contains("|")){
											cuentaCargo = cuentaCargo.substring(0,cuentaCargo.indexOf('|'));
										}
									}


									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuentaCargo--b->"+ cuentaCargo, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuentaAbono--b->"+ cuentaDestino, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

									sessionBit.removeAttribute("parametrosBitacora");

									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuentaCargo,importeDouble,claveOperacion,cuentaDestino,token,concepto,0);
								} catch(Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							}


							iniciaEnviar( Todas,arcExp,arrayCuentasCargo,clavePerfil,usuario,contrato, req, res );

						}
					}
				 } catch (Exception e) {
					 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			 }

         }
		else
		if(sesionvalida)
		 {
           req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
           evalTemplate(IEnlace.ERROR_TMPL, req, res);
		 }
		EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Saliendo de Envio de Transferencias.", EIGlobal.NivelLog.INFO);
    }

    /**
     * Modulo Enviar
     *
     * @param Todas the todas
     * @param arcExp the arc exp
     * @param arrayCuentasCargo the array cuentas cargo
     * @param clavePerfil the clave perfil
     * @param usuario the usuario
     * @param contrato the contrato
     * @param req the req
     * @param res the res
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void iniciaEnviar(EI_Tipo Todas, boolean arcExp,String[][] arrayCuentasCargo,String clavePerfil,String usuario,String contrato, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
    {

    	Integer referencia = 0 ;
    	ServicioTux tuxGlobal = new ServicioTux();
    	Hashtable hs = null;
    	String buffer = "";
    	Double conSaldo = 0.0;
    	Double importeDivisa = 0.0;
    	String resCodError = null;
    	String desResCodErr = null;
    	String respuesta = null;
    	String respuesta07 = "";
    	String numeroOrden="0";
    	String folioOperacion = "";
    	String[] titularCtaCargo = null;
    	String[] transfer = null;
    	String[] datosCtaInter = null;
    	String claveEspecial = "";
    	String tipoEspecial = "";
    	String tipoCambioOperacion = "";
    	String idPaisBcoCorresp = "";
    	String idBcoCorresp = "";
    	String importeFinal = "";
    	String bancoDesBita = "";
    	String ciudadDesBita = "";
    	boolean esMancomunado = false;
    	TrxGPF2BO busGPF2 = new TrxGPF2BO();
    	TrxPE80BO busPE80 = new TrxPE80BO();
    	TipoCambioEnlaceDAO tipoCambio = null;

    	String codErrorOper = "";
		String Result = "";
		String importe = "";
	    String medioEntrega="";
		String referenciaMed= "";
		String titular="";
		String divisaCargo="";
		String divisaAbono="";
		String contraMoneda="";
		String importeMN="";
		String importeDA="";
		String importeDIV="";
		String dirOinver="";
		String claveABA="";
		String tipoCambioMN="";
		String tipoCambioDA="";
		String ciudad="";
		String pais="";
		String horaCotiza="";
		String cveEspecial="";
		String fecha="";
		String rfc="";
		String iva="";
	    String cuentaCargo="";
		String cuentaAbono="";
		String beneficiario= "";
		String concepto ="";
		String banco="";
		String sucursal="";
		String Trama="";

		String nombreArchivo=usuario+".lti";
		String mensajeError="";
		String arcLinea="";
		String arcLineaSalida="";

		String Referencia="";
		String TipoError="";
		String codError="";
		String nuevaTrama="";
		String strComprobante="";

		String nombreSalida=usuario+".doc";
		String codError1="";
		String strTabla="";
		String CADENAS_DE_IMPORTES="";

		boolean comuError=false;
		boolean salidaArc=false;
		boolean noError=true;
		double importeTotal=0;
		int totalLineas=0;
		int totalRegistros=0;
		ValidaCuentaContrato valCtas = null;
		 String estatusTransMas = "";
		 String referencias = "";
		 String referenciaArchivo = "";

		EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Entrando a modificacion" , EIGlobal.NivelLog.INFO);

		String  No_Registradas="";
		String  Registradas="";
		String  Reg_and_NoReg="";
		String  Reg_Enviar="";
		String  strorig="";
		boolean cuentasiguales = true;
		String	cuenta1="", cuenta_1="", cuenta2="", cuenta_2="";

		try{
			if((String) req.getSession().getAttribute(CVE_ESPECIAL)!=null
			   && !"".equals(((String) req.getSession().getAttribute(CVE_ESPECIAL)).trim())
			   && !"null".equals(((String) req.getSession().getAttribute(CVE_ESPECIAL)).trim())){
				claveEspecial = (String) req.getSession().getAttribute(CVE_ESPECIAL);
				tipoEspecial = (String) req.getSession().getAttribute("tipoEspecial");
			}
		}catch(Exception e){}

		if(req.getSession().getAttribute("NoRegistradas")==null) {
			No_Registradas="";
		} else {
			No_Registradas=(String)req.getSession().getAttribute("NoRegistradas");
			EIGlobal.mensajePorTrace("MTI_Enviar - execute(): No_Registradas :"+No_Registradas , EIGlobal.NivelLog.INFO);
			strorig=No_Registradas;
			EIGlobal.mensajePorTrace("MTI_Enviar - execute(): No_Registradas strorig"+strorig, EIGlobal.NivelLog.INFO);
		}

		if(req.getSession().getAttribute("Registradas")==null) {
			Registradas="";
		} else {
			strorig="";
			Registradas=(String)req.getSession().getAttribute("Registradas");
			EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Registradas_1 :"+ Registradas , EIGlobal.NivelLog.INFO);
			strorig=Registradas;
			EIGlobal.mensajePorTrace("MTI_Enviar - execute(): Registradas strorig1"+ strorig , EIGlobal.NivelLog.INFO);
		}

		Reg_Enviar=Todas.strOriginal;

		EI_Tipo misCuentas=new EI_Tipo(); 
		EI_Tipo lasCuentas=new EI_Tipo();  

		misCuentas.iniciaObjeto(strorig);
		lasCuentas.iniciaObjeto(Reg_Enviar);
		HttpSession ses = req.getSession();

		EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreSalida);
		EI_Exportar ArcEnt=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);

		EI_Tipo Resultado=new EI_Tipo(this);

		ServicioTux TuxGlobal = new ServicioTux();

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		medioEntrega=IEnlace.medioEntrega1;
		referenciaMed=EnlaceGlobal.fechaHoy("ddthtmts");

		strTabla+="\n<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>";
		strTabla+="\n <tr>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Ref. Enlace &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Estatus &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Ref. Env�o &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; No. Orden &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Cuenta Origen &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Cuenta Destino &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Beneficiario &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Importe divisa &nbsp; </th>";	
		strTabla+="\n  <th class='tittabdat'>&nbsp; Tipo de Cambio &nbsp; </th>";
		strTabla+="\n  <th class='tittabdat'>&nbsp; Concepto &nbsp; </th>";
		strTabla+="\n </tr>";

		if(arcExp)
		{
			if(ArcEnt.abreArchivo())
			{
				arcLinea=ArcEnt.leeLinea();
				if(ArcSal.creaArchivo()) {
				    salidaArc=true;
				}

				ArcSal.escribeLinea(EnlaceGlobal.fechaHoy("th:tm:ts dt, dd de mt de aaaa"));
				if(!(inicializando()== 1))
				{
					arcLinea="ERROR    000000";
					EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): No pudo inicializar la conexion.", EIGlobal.NivelLog.INFO);
				}
				else
				{
					do
					{
						arcLinea=ArcEnt.leeLinea();
						if(!ERROR.equals(arcLinea))
						{
							totalRegistros++;
							totalLineas++;

							EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Trama entrada: "+arcLinea, EIGlobal.NivelLog.DEBUG);

							EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
							if(totalLineas%25==0){
								EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Linea no."+Integer.toString(totalLineas), EIGlobal.NivelLog.DEBUG);}

							codError=Result.substring(0,8);
							if("OK".equals(codError.trim()))
							{
								TipoError="ENVIADA";
								if("MANC".equals(Result.substring(Result.length()-4,Result.length())))
								{
									TipoError="MANCOMUNADA";
									Referencia=Result.substring(8,Result.length()-4);
								}
								else{
									Referencia=Result.substring(8,Result.length());}
							}
							else
							{
								Referencia=Result.substring(8,16);
								if(Result.length()>16){
									TipoError=Result.substring(16,Result.length());}
								else{
									TipoError="NO ACEPTADA";}
							}

							arcLineaSalida="\n"+Referencia+" "+TipoError + "---" + arcLinea ;
							if(salidaArc){
								if(!ArcSal.escribeLinea(arcLineaSalida)){
									EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Algo salio mal escribiendo: "+arcLineaSalida, EIGlobal.NivelLog.INFO);}}
						}
						else{
						noError=false;}
						try {
							referenciaArchivo = Result.substring(Result.lastIndexOf("|"));
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}

			  			//TODO BIT CU2101, Envia transacci�n en archivo de exportaci�n
			  			/*
			  			 * VSWF-HGG-I
			  			 */
						if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			  			try{
							BitaConstants.archivo = getServletContext().getRealPath("/") + LOG_BITACORA;
							if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
								BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_AGREGAR_REGISTRO_BIT);
								if (session.getContractNumber() != null) {
									bt.setContrato(session.getContractNumber().trim());
								}
								if(Result != null){
									if("OK".equals(Result.substring(0,2))){
										bt.setIdErr(DITA0000);
									}else if(Result.length() > 8){
										bt.setIdErr(Result.substring(0,8));
									}else{
										bt.setIdErr(Result.trim());
									}
								}
								if (nombreArchivo != null) {
									bt.setNombreArchivo(nombreArchivo.trim());

								}
								bt.setServTransTux(TIPO_OPERACION);
								if (Referencia!=null){
									try {
										bt.setReferencia(Long.parseLong(Referencia.trim()));
									} catch (NumberFormatException e) {
										bt.setReferencia(0);
									}
								}
								if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
											.equals(BitaConstants.VALIDA)) {
									bt.setIdToken(session.getToken().getSerialNumber());
								}

								BitaHandler.getInstance().insertBitaTransac(bt);
							}
						}catch(SQLException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}catch(Exception e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
						}


					}while(noError);

					req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
				}
				ArcEnt.cierraArchivo();

				desconectando();
				ArcSal.escribeLinea(EnlaceGlobal.fechaHoy("th:tm:ts dt, dd de mt de aaaa"));
				ArcSal.cierraArchivo();

				if(arcLinea.substring(0,5).trim().equals(ERROR) && totalLineas<1)
				{
				comuError=true;
				mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
				}
			}
			else
			{
			comuError=true;
			mensajeError="Problemas de comunicacion. Por favor intente mas tarde.";
			}
		} else
				if(Todas.totalRegistros>=1)
				{
					LYMValidador valida = null;

					try{
					valCtas = new ValidaCuentaContrato();

					String datosctaAbono[]=null;
					String datosctaCargo[]=null;

			        for(int i=0;i<Todas.totalRegistros;i++)
			        {
						cuentaCargo = Todas.camposTabla[i][0].trim();
						titular     = Todas.camposTabla[i][1].trim();
						cuentaAbono = Todas.camposTabla[i][2].trim();
						beneficiario = Todas.camposTabla[i][3].trim();
						importe = Todas.camposTabla[i][4].trim();
						concepto = Todas.camposTabla[i][5].trim();

						datosctaCargo = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
								  								  session.getUserID8(),
														          session.getUserProfile(),
														          cuentaCargo.trim(),
														          IEnlace.MCargo_TI_pes_dolar);


						datosctaAbono = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
								session.getUserID8(),
								session.getUserProfile(),
								cuentaAbono.trim(),
								IEnlace.MAbono_TI);

						if (datosctaCargo!=null && datosctaAbono!=null){

						    if ("6".equals(datosctaCargo[3]))
						    {
								divisaCargo =DA;
								contraMoneda=divisaCargo;
						    }
			                else
						    {
							    divisaCargo = "MN";
							    contraMoneda=divisaCargo;
					        }
						divisaAbono      = Todas.camposTabla[i][6].trim();
						pais             = Todas.camposTabla[i][8].trim();
						claveABA         = Todas.camposTabla[i][9].trim();
						ciudad           = Todas.camposTabla[i][10].trim();
						banco            = Todas.camposTabla[i][11].trim();
						fecha            = Todas.camposTabla[i][12].trim();
						rfc              = Todas.camposTabla[i][13].trim();
						iva              = Todas.camposTabla[i][14].trim();

						tipoCambioMN     = Todas.camposTabla[i][7].trim();
						tipoCambioDA     = Todas.camposTabla[i][15].trim();
						cveEspecial      = Todas.camposTabla[i][16].trim();
						horaCotiza       = Todas.camposTabla[i][17].trim();
						dirOinver        = Todas.camposTabla[i][18].trim();

						concepto=formateaCampo(concepto.trim(),60);
						if(! Todas.camposTabla[i][13].trim().equals("") )
						{
						    concepto+="+"+Todas.camposTabla[i][13];
						    concepto+=" IVA "+Todas.camposTabla[i][14];
						}
						sucursal = C_0000;

						importeMN=Todas.camposTabla[i][19].trim();
						importeDA=Todas.camposTabla[i][20].trim();
						importeDIV=Todas.camposTabla[i][21].trim();

						Trama=generaTrama(titular, divisaCargo, divisaAbono, contraMoneda,  importeMN, importeDA, importeDIV, dirOinver,
				                  claveABA, tipoCambioMN, tipoCambioDA, ciudad, pais, horaCotiza, cveEspecial, cuentaAbono,
								  cuentaCargo,beneficiario,concepto, banco, sucursal, usuario, contrato, clavePerfil, medioEntrega);


						EIGlobal.mensajePorTrace("****TRAMA****"+Trama,	EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("\n"+Trama,	EIGlobal.NivelLog.INFO);

						try {
							hs = tuxGlobal.sreferencia("901");;
							codError = hs.get("COD_ERROR").toString();
							EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",	EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hs.toString(), EIGlobal.NivelLog.INFO);
							referencia = (Integer) hs.get("REFERENCIA");
						} catch (Exception e) {
							codError = null;
							referencia = 0;
						}

						if(!claveEspecial.trim().equals("")){
							tipoCambioOperacion = tipoEspecial;
						}else{
							if(USD.equals(getDivisaTrx(cuentaCargo).trim())){
								tipoCambioOperacion = tipoCambioDA;}
							else{
								tipoCambioOperacion = tipoCambioMN;}
						}
						EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: claveEspecial->"+claveEspecial, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: tipoCambioOperacion->"+tipoCambioOperacion, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: tipoCambioDA->"+tipoCambioDA, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: tipoCambioMN->"+tipoCambioMN, EIGlobal.NivelLog.INFO);

						if (codError.endsWith(C_0000)) {

							EIGlobal.mensajePorTrace("LYM_VALIDA [DITA]", EIGlobal.NivelLog.INFO);

							if (valida == null) {
								valida = new LYMValidador();
							}

							codError = valida.validaLyM(Trama);

							EIGlobal.mensajePorTrace("LYM_VALIDA [DITA]: " + codError, EIGlobal.NivelLog.INFO);

							if(codError.indexOf("ALYM0000") != -1) {

							String trama_manc = "";
			  			    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
			  				trama_manc += sdf.format(new Date()) + "|";
			  				trama_manc += referencia.toString() +  "|";
			  				trama_manc += session.getContractNumber()+"|";
			  				trama_manc += "DITA|||";
			  				trama_manc += convierteUsr8a7(session.getUserID()) + "|";
			  				trama_manc += cuentaCargo +"|";
			  				trama_manc += cuentaAbono +"|";
			  				trama_manc += divisaAbono +"|";
			  				trama_manc += "VTA" +"|";
			  				trama_manc += divisaCargo +"|";
			  				trama_manc += banco +"|||";
			  				trama_manc += dirOinver +"||";
			  				trama_manc += "TRAN" +"|||";
			  				trama_manc += ciudad +"|";
			  				trama_manc += beneficiario +"||";
			  				trama_manc += pais +"||";
			  				trama_manc += concepto +"||";
			  				trama_manc += claveEspecial.trim() +"|";
			  				trama_manc += formatoImporte(importeMN) + "|";
			  				trama_manc += formatoImporte(importeDA) + "|";
							if((DA.equals(divisaCargo.trim()) || USD.equals(divisaCargo.trim())) && 
									USD.equals(divisaAbono.trim())){
								trama_manc += formatoImporte(importeDA) + "|";
							}else{
								trama_manc += formatoImporte(importeDIV) + "|";
							}

			  				if(!claveEspecial.trim().equals("") && "MXP".equals(getDivisaTrx(cuentaCargo).trim())){
			  					trama_manc += tipoEspecial + "|";}
			  				else{
			  					trama_manc += tipoCambioMN + "|";}

			  				if(!claveEspecial.trim().equals("") && USD.equals(getDivisaTrx(cuentaCargo).trim())){
			  					trama_manc += tipoEspecial + "|";}
			  				else{
			  					trama_manc += tipoCambioDA + "|";}
			  				trama_manc += titular+ "|";

							try {
								hs = tuxGlobal.manc_valida(trama_manc);
								codError = (String) hs.get("COD_ERROR");
								buffer = (String) hs.get("BUFFER");
							} catch (Exception e) {
								codError = null;
							}

							if (codError.endsWith(C_0000)) {
								try {
									if("0".equals(buffer.substring(0,1))){
										//FLUJO MANCOMUNADO, NO SE EJECUTA GP07
										resCodError = "OK";
										esMancomunado = true;
									}else if("1".equals(buffer.substring(0,1))){
										
											if("MN".equals(divisaCargo.trim())){
												importeDivisa = Double.parseDouble(importeMN);}
											else if(divisaCargo.trim().equals(DA)){
												importeDivisa = Double.parseDouble(importeDA);}
											EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: importeDiv->"+importeDivisa, EIGlobal.NivelLog.INFO);

												tipoCambio = new TipoCambioEnlaceDAO();
												DatosBeneficiarioBO beneficiarioBO = new DatosBeneficiarioBO(); //INDRA-SWIFT-P022574
												//OBTENER TITULAR CUENTA DE CARGO, EJECUCI�N PE80
												try{
													titularCtaCargo = busPE80.obtenTitularCtaCargo(cuentaCargo);
												}catch(Exception e){
													titularCtaCargo = new String[4]; //INDRA-SWIFT-P022574
													titularCtaCargo[0] = "";
													titularCtaCargo[1] = "";
													titularCtaCargo[2] = "";
													titularCtaCargo[3] = ""; //INDRA-SWIFT-P022574
													EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: Problemas al obtener titular de cuenta de cargo por PE80", EIGlobal.NivelLog.INFO);
												}
												idBcoCorresp = claveABA;
												EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: idBcoCorresp->"+ idBcoCorresp, EIGlobal.NivelLog.INFO);

												if(req.getSession().getAttribute("catCvePais")!=null){
													idPaisBcoCorresp = (String) req.getSession().getAttribute("catCvePais");}
												EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: pais->"+idPaisBcoCorresp, EIGlobal.NivelLog.INFO);
												
												//OBTENER LOS DATOS DE LA CUENTA INTERNACIONAL - INDRA-SWIFT-P022574-ini
												DatosBeneficiarioBean beneficiarioBean = new DatosBeneficiarioBean();
												OperacionesInternacionalesBO internacionalesBO = new OperacionesInternacionalesBO();
												try{
//													datosCtaInter = tipoCambio.obtenDatosCtaInter(session.getContractNumber(), ((DatosMancInter)o).getCta_destino());
													beneficiarioBean = beneficiarioBO
														.consultarDatosBenef(
																session.getContractNumber(), 
																cuentaAbono.trim()); 
													/*if(datosCtaInter[0].trim().equals("USA")){
														idBcoCorresp = datosCtaInter[1];}
													else{
														idBcoCorresp =  datosCtaInter[2];}*/
												}catch(Exception e){
													idBcoCorresp = "";
													EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: Problemas al obtener los datos de la cuenta internacional", EIGlobal.NivelLog.INFO);
												}
												//INDRA-SWIFT-P022574-fin
												
												if((DA.equals(divisaCargo.trim()) || USD.equals(divisaCargo.trim())) &&
														USD.equals(divisaAbono.trim())){
													//EJECUCI�N DE SERVICIO TRANSANT
													EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: OPERACION POR TRANSFER", EIGlobal.NivelLog.INFO);
													EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: PAIS BENEF::"+beneficiarioBean.getCvePaisBenef(), EIGlobal.NivelLog.INFO);
												/*if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) { //INDRA-SWIFT-P022574
														EIGlobal.mensajePorTrace("MTI_Enviar:: Va por Tuxedo", EIGlobal.NivelLog.INFO);
														transfer = tipoCambio.ejecutaServicioTransfer(referencia, cuentaCargo, titular, cuentaAbono,
																	                              beneficiario, importeDA, banco, sucursal,
																	                              pais.trim(), ciudad, idBcoCorresp.trim(), concepto, rfc,
																	                              usuario, usuario);
													}else {*/
														//INDRA-SWIFT-P022574-ini
														EIGlobal.mensajePorTrace("MTI_Enviar:: Va por MQ", EIGlobal.NivelLog.INFO);
														transfer = internacionalesBO.enviaOperacionInternacional(beneficiarioBean, 
															OperacionesInternacionalesBO.OP_MISMA_DIVISA, referencia.toString(), 
															usuario, cuentaCargo, titularCtaCargo[0], importeDA, USD, 
															titularCtaCargo[1], titularCtaCargo[2], titularCtaCargo[3], 
															cuentaAbono, "", "",req.getSession().getAttribute("strCiudad").toString(),concepto,"");
														//INDRA-SWIFT-P022574-fin
													//}
													
													if(transfer[0].endsWith(C_0000)){
														resCodError = "OK";
														codError = "DIIN0000";

														folioOperacion = transfer[1];
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: folioOperacion Transfer<<" + folioOperacion + ">>", EIGlobal.NivelLog.INFO);
														//SE LLENA VARIABLE DE ORDEN
														numeroOrden = folioOperacion;
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: orden Transfer<<" + numeroOrden + ">>", EIGlobal.NivelLog.INFO);
														importeDIV = importeDA;
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: importeDIV<<" + importeDIV + ">> y importeDA<<" + importeDA + ">>", EIGlobal.NivelLog.INFO);

														//SI NO HAY ERROR, SE BITACORIZA EN LA EWEB_BITA_INTER
														bancoDesBita = banco;
														ciudadDesBita = ciudad;
										    			if(banco.length()>40){
										    				bancoDesBita = banco.substring(0, 39);}
														if(ciudad.length()>40){
															ciudadDesBita = ciudad.substring(0, 39);}

														tipoCambio.insertEWEBBITA(session.getContractNumber(), "X", numeroOrden, referencia, "PV",
															      "PV", "VTA",
															      pais.trim(), divisaAbono, divisaCargo, "TRAN", session.getUserID8(),
												                  idBcoCorresp.trim(), cveEspecial,
												                  cuentaCargo, cuentaAbono, folioOperacion, folioOperacion, Integer.parseInt(numeroOrden),
												                  Integer.parseInt(numeroOrden), "0", importeDA, importeDIV, tipoCambioMN, tipoCambioDA,
												                  sdf.format(new Date()), sdf.format(new Date()), bancoDesBita, "", ciudadDesBita, concepto,
												                  beneficiario, "");

														//NO SE CONSULTA EL SALDO DISPONIBLE CON QUE QUEDA LA CUENTA
													}else{
														//ERROR AL EJECUTAR OPERACION POR TRANSFER
														resCodError = ERROR;
														desResCodErr = (transfer[2]!=null)?transfer[2]:"OPERACION RECHAZADA POR TRANSFER";
														codError = transfer[0];
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: RECHAZADA por Transfer->"+desResCodErr, EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: COD ERROR por Transfer->"+codError, EIGlobal.NivelLog.INFO);
													}

												}else{

													if(getDivisaTrx(cuentaCargo).trim().equals(USD)){
														importeFinal = importeDA;}
													else{
														importeFinal = importeMN;}

													EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: OPERACI�N POR CAMBIOS GP07", EIGlobal.NivelLog.INFO);
													EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: PAIS BENEF::"+beneficiarioBean.getCvePaisBenef(), EIGlobal.NivelLog.INFO);
													/*if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
														EIGlobal.mensajePorTrace("MTI_Enviar:: Va por Tuxedo", EIGlobal.NivelLog.INFO);
														respuesta = tipoCambio.getTransfInternacionales(
															cuentaCargo,
															divisaAbono,
															formatoImporte(importeDIV),
															tipoCambioOperacion,
															formatoImporte(importeFinal),//si es DLL importeDA, si es MXN importeMN,
															claveEspecial.trim(),//refClave,
															titularCtaCargo[0],//nombreOrdenante,
															titularCtaCargo[1],//apPatOrdenante,
															titularCtaCargo[2],//apMatOrdenante,
															idPaisBcoCorresp.trim(),//PABenef,
															beneficiario,//NombreBenef,
															"",//apPatBenef,
															"",//apMatBenef,
															"",//RFCBenef,
															cuentaAbono,
															idBcoCorresp.trim(),//bancoCorresponsal,
															concepto,//detalleOp,
															"0981",//centroOperante,
															"0981",//CentroOrigen,
															"0981",//centroDestino,
															concepto,//ObsCargo,*/
															//""/*ObsAbono*/);
													//}else {
														//INDRA-SWIFT-P022574-ini
														EIGlobal.mensajePorTrace("MTI_Enviar:: Va por MQ", EIGlobal.NivelLog.INFO);
														transfer = internacionalesBO.enviaOperacionInternacional(beneficiarioBean, 
																OperacionesInternacionalesBO.OP_CAMBIARIA,referencia.toString(), 
																usuario, cuentaCargo, titularCtaCargo[0], importeDIV, getDivisaTrx(cuentaCargo).trim(), 
																titularCtaCargo[1], titularCtaCargo[2], titularCtaCargo[3], 
																cuentaAbono, tipoCambioOperacion, importeFinal,
																req.getSession().getAttribute("strCiudad").toString(),concepto, claveEspecial.trim());
														//INDRA-SWIFT-P022574-fin
													//}
													EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: folioOperacion Transfer<<" + folioOperacion + ">>", EIGlobal.NivelLog.INFO);
													//INDRA-SWIFT-P022574-ini
													boolean resp= false;
													if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
														if(respuesta.substring(0, 2).trim().equals("@1")){
															resp = true;
														}
													}else {
														if(transfer[0].endsWith(C_0000)){ //INDRA-SWIFT-P022574
															resp = true;
														}
													}//INDRA-SWIFT-P022574-fin
													//if(respuesta.substring(0, 2).trim().equals("@1")){
													//if(transfer[0].endsWith(C_0000)){ //INDRA-SWIFT-P022574
													if (resp) {
														resCodError = "OK";
														codError = "DIIN0000";

														//SE LLENA VARIABLE DE FOLIO DE LIQUIDACI�N
														//INDRA-SWIFT-P022574-ini
														if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
															respuesta07 = respuesta.substring(respuesta.indexOf("DCGPM0071 P"), respuesta.length());
															folioOperacion = respuesta07.substring(406, 416);
														}else {
															folioOperacion = transfer[1];	
														}
														//INDRA-SWIFT-P022574-fin
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: folioOperacion<<" + folioOperacion + ">>", EIGlobal.NivelLog.INFO);
														//SE LLENA VARIABLE DE ORDEN
														numeroOrden = folioOperacion;
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: orden<<" + numeroOrden + ">>", EIGlobal.NivelLog.INFO);

														//SI NO HAY ERROR, SE BITACORIZA EN LA EWEB_BITA_INTER
														bancoDesBita = banco;
														ciudadDesBita = ciudad;
										    			if(banco.length()>40){
										    				bancoDesBita = banco.substring(0, 39);}
														if(ciudad.length()>40){
															ciudadDesBita = ciudad.substring(0, 39);}

														tipoCambio.insertEWEBBITA(session.getContractNumber(), "D", numeroOrden, referencia, "RT",
															      "RT", "VTA",
															      pais.trim(), divisaAbono, divisaCargo, "TRAN", session.getUserID8(),
												                  idBcoCorresp.trim(), cveEspecial,
												                  cuentaCargo, cuentaAbono, folioOperacion, folioOperacion, 0,
												                  0, importeMN, importeDA, importeDIV, tipoCambioMN, tipoCambioDA,
												                  sdf.format(new Date()), sdf.format(new Date()), bancoDesBita, "", ciudadDesBita, concepto,
												                  beneficiario, "");

														//NO SE CONSULTA EL SALDO DISPONIBLE CON QUE QUEDA LA CUENTA
													}else{
														//ERROR AL EJECUTAR GP07
														resCodError = ERROR;
														//INDRA-SWIFT-P022574-ini
														if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
															desResCodErr = respuesta.substring(18, respuesta.indexOf('�'));
															codError = respuesta.substring(10, 17);
														}else {
															desResCodErr = transfer[2];
															codError = transfer[0];	
														}
														//INDRA-SWIFT-P022574-fin
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: RECHAZADA->"+desResCodErr, EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: COD ERROR->"+codError, EIGlobal.NivelLog.INFO);
													}
												}
											}
										
									
								} catch (Exception e) {
									EIGlobal.mensajePorTrace("*** MIPD_cambios - ERROR EN LA TRANSFERENCIA  ***"+e.getMessage(), EIGlobal.NivelLog.INFO);
									resCodError = ERROR;
									desResCodErr = "PROBLEMAS AL REALIZAR LA OPERACI�N";
									codError = "DIIN9999";
								}
							}else{
								//LA RESPUESTA DEL MANC_VALIDA NO FUE EXITOSA
								resCodError = ERROR;
								desResCodErr = getDesErrManc(codError);
								EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar::"+desResCodErr, EIGlobal.NivelLog.INFO);
							}
							} else {
								desResCodErr = codError.substring(16);
								codError = codError.substring(0, 8);
								resCodError = ERROR;

								EIGlobal.mensajePorTrace("LYM_VALIDA [DITA] [ERROR] :" + codError, EIGlobal.NivelLog.INFO);
							}
						}else{
							//CASO CONTRARIO REFERENCIA INCORRECTA
							resCodError = ERROR;
							desResCodErr = "PROBLEMAS AL OBTENER LA REFERENCIA";
							EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar::PROBLEMAS AL OBTENER LA REFERENCIA", EIGlobal.NivelLog.INFO);
						}

						String IP = " ";
						String fechaHr = "";												
						IP = req.getRemoteAddr();											
						java.util.Date fechaHrAct = new java.util.Date();
						SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
						fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
						
						}
						else{
							
							if(datosctaCargo==null){
								EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): La cuenta cargo informada no es parte del entorno del cliente", EIGlobal.NivelLog.INFO);}
							if(datosctaAbono==null){
								EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): La cuenta abono informada no es parte del entorno del cliente", EIGlobal.NivelLog.INFO);}
						}

						EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Trama salida: "+respuesta, EIGlobal.NivelLog.DEBUG);

						importeTotal+=new Double(Todas.camposTabla[i][4]).doubleValue();

						String mensaje="";
						String colorbg="";
						String str="";
						String alignTxt[]={"left","center","right"};
						String refEnvio="0";

						double impTmp=0;
						int align[]={0,0,1,2,2,0};
						int c[]={0,2,3,4,7,5};
						int Error=0;
						boolean errorServ=false;

						String TipoErrorPantalla="";



						if(Registradas != null && !Registradas.equals(""))
						{
							//MSD Q05-24447 (para no registradas esto no aplica)

							cuenta1  = misCuentas.camposTabla[i][0];
							cuenta_1 = lasCuentas.camposTabla[i][0];
							cuenta2  = misCuentas.camposTabla[i][2];
							cuenta_2 = lasCuentas.camposTabla[i][2];

							EIGlobal.mensajePorTrace("MDI_Enviar - execute(): strorig "+ cuenta1 , EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar "+ cuenta_1 , EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("MDI_Enviar - execute(): strorig "+ cuenta2 , EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar "+ cuenta_2 , EIGlobal.NivelLog.INFO);


							if(cuenta1.equals(cuenta_1) && cuenta2.equals(cuenta_2) ){
								EIGlobal.mensajePorTrace("contratos o.k.", EIGlobal.NivelLog.INFO);}
							else
							{
								EIGlobal.mensajePorTrace("Se modificaron los contratos", EIGlobal.NivelLog.INFO);
								comuError=true;
								mensajeError="Transaccion no V�lida";
							}

						} /*TERMINA MOD MSD Q05-24447*/


						//TODO BIT CU2101, Envia transacci�n en archivo de exportaci�n

						if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			  			try{
							BitaConstants.archivo = getServletContext().getRealPath("/") + LOG_BITACORA;
							if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
								BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
								BitaTransacBean bt = new BitaTransacBean();
								BitaTCTBean beanTCT = new BitaTCTBean ();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_AGREGAR_REGISTRO_BIT);
								bt.setContrato(session.getContractNumber());
								if(resCodError != null){
									if("OK".equals(resCodError)){

										bt.setIdErr(DITA0000);
									}else{

										bt.setIdErr("DITA9999");
									}
								}
								if (cuentaCargo != null) {
									bt.setCctaOrig(cuentaCargo.trim());
								}
								if (cuentaAbono != null) {
									bt.setCctaDest(cuentaAbono.trim());
								}
								if (banco !=  null) {
									if (banco.length() > 13) {
										bt.setBancoDest(banco.substring(0,13));
									}else{
										bt.setBancoDest(banco.trim());
									}
								}
								if (importe!=null){

								try {
									bt.setImporte(Double.parseDouble(importe.trim()));
								} catch (NumberFormatException e) {
									bt.setImporte(0);
								}
								}
								if (tipoCambioMN != null && !tipoCambioMN.equals("")) {
									bt.setTipoCambio(Double.parseDouble(tipoCambioMN.trim()));
								}
								if (divisaAbono != null) {
									bt.setTipoMoneda(divisaAbono.trim());
								}
								bt.setServTransTux(TIPO_OPERACION);

									try{
										bt.setReferencia(Long.parseLong(referencia.toString().trim()));
										}catch (NumberFormatException e){
											bt.setReferencia(0);
										}

								if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
											.equals(BitaConstants.VALIDA)) {
									bt.setIdToken(session.getToken().getSerialNumber());
								}


								if(beneficiario!=null && beneficiario!=""){
									bt.setBeneficiario(beneficiario.trim());
								}

								BitaHandler.getInstance().insertBitaTransac(bt);

								beanTCT = bh.llenarBeanTCT(beanTCT);
								beanTCT.setCuentaOrigen(cuentaCargo);
								beanTCT.setCuentaDestinoFondo(cuentaAbono);
								beanTCT.setReferencia(referencia);
								beanTCT.setTipoOperacion(TIPO_OPERACION);
								beanTCT.setCodError(codError);
								beanTCT.setUsuario(session.getUserID8());
								if((DA.equals(divisaCargo.trim()) || USD.equals(divisaCargo.trim())) &&
										USD.equals(divisaAbono.trim())){
									beanTCT.setImporte(Double.parseDouble(importeDA.trim()));
								}else{
									beanTCT.setImporte(Double.parseDouble(importeDIV.trim()));
				  				}
							    BitaHandler.getInstance().insertBitaTCT(beanTCT);

							}
						}catch(SQLException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}catch(Exception e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}
						}

						if("OK".equals(resCodError.trim()))
						{
							EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Contesto bien.", EIGlobal.NivelLog.DEBUG);
							TipoError="ENVIADA";
							TipoErrorPantalla="<font color=blue><b>ENVIADA</b></font>";
							if(esMancomunado)		// mancomunada ok
							{
								//Modificacion para mostrar todos los comprobantes para cualquier estatus
								Error=1;
								TipoError="MANCOMUNADA";
								TipoErrorPantalla="<font color=green><b>MANCOMUNADA</b></font>";
								numeroOrden = "-------";													
								refEnvio    = "-------";													
							}
							Referencia=referencia.toString();

						}
						else
						{
							EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Hubo un error en el servicio.", EIGlobal.NivelLog.DEBUG);
							errorServ =true;
							Referencia = referencia.toString();
							numeroOrden = "-------";													
							refEnvio    = "-------";													
							TipoError=desResCodErr;
							TipoErrorPantalla="<font color=red><b>"+desResCodErr+"</b></font>";


							Error=1;
						}
						estatusTransMas = estatusTransMas + TipoError + ",";
						referencias = referencias + Referencia + ",";

						nuevaTrama+=Referencia+PIPE;
						for(int j=0;j<15;j++)
						nuevaTrama+=Todas.camposTabla[i][j]+PIPE;
						nuevaTrama+=TipoError+PIPE;
						nuevaTrama+="@";

						EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Armando la tabla.", EIGlobal.NivelLog.DEBUG);

						if((i%2)==0){
							colorbg="class='textabdatobs'";}
						else{
							colorbg="class='textabdatcla'";}

						strTabla+="\n<tr>";
						if(Error==1){
							strTabla+="<td "+colorbg+"> &nbsp;"+Referencia+"</td><td "+colorbg+"> "+TipoErrorPantalla+" </td>";}
						else
						{
							// Se arma la trama para el link del comprobante ...
							Resultado.iniciaObjeto(Result+"@");
							if(!errorServ)
							{
								refEnvio = referencia.toString();

							}
							else
							{
								numeroOrden="-------";
								refEnvio="-------";
							}
							strComprobante+="Por confirmar."+PIPE;
							strComprobante+=numeroOrden+PIPE;
							strComprobante+=refEnvio+PIPE;
							strComprobante+=Todas.camposTabla[i][5]+PIPE;
							strComprobante+=Todas.camposTabla[i][0]+" ";
							strComprobante+=Todas.camposTabla[i][1]+PIPE;
							strComprobante+=Todas.camposTabla[i][2]+" ";
							strComprobante+=Todas.camposTabla[i][3]+PIPE;
							strComprobante+=Todas.camposTabla[i][13]+PIPE;
							strComprobante+=Todas.camposTabla[i][11]+PIPE;
							strComprobante+=Todas.camposTabla[i][8]+PIPE;
							strComprobante+=Todas.camposTabla[i][10]+PIPE;
							strComprobante+=Todas.camposTabla[i][6]+PIPE;
							strComprobante+=Todas.camposTabla[i][7]+PIPE;
							if((DA.equals(divisaCargo.trim()) || USD.equals(divisaCargo.trim())) &&
									USD.equals(divisaAbono.trim())){
								strComprobante+="0"+PIPE;}
							else{
								strComprobante+=importeMN+PIPE;}
							strComprobante+=importeDA+PIPE;
							strComprobante+=importeDIV+PIPE;
							strComprobante+=Todas.camposTabla[i][14]+PIPE;
							strComprobante+=TipoError+PIPE;
							strComprobante+="@";

							EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): Se armo comprobante.", EIGlobal.NivelLog.DEBUG);

							str="despliegaDatos('"+strComprobante+"');";
							strTabla+="<td "+colorbg+"> &nbsp;<a href=\"javascript:"+str+"\">"+Referencia+"</a></td><td "+colorbg+"> "+TipoErrorPantalla+" </td>";
						}
						strTabla+="<td "+colorbg+" align=left> &nbsp;"+refEnvio+" &nbsp;</td>";
						strTabla+="<td "+colorbg+" align=left> &nbsp;"+numeroOrden+" &nbsp;</td>";

						strTabla+="<td "+colorbg+" align=left> &nbsp;"+cuentaCargo+" &nbsp;</td>";
						strTabla+="<td "+colorbg+" align=left> &nbsp;"+cuentaAbono+" &nbsp;</td>";
						strTabla+="<td "+colorbg+" align=left> &nbsp;"+beneficiario+" &nbsp;</td>";
						if((DA.equals(divisaCargo.trim()) || USD.equals(divisaCargo.trim())) &&
								USD.equals(divisaAbono.trim())){
							strTabla+="<td "+colorbg+" align=left> &nbsp;"+formatoImporte(importeDA)+" &nbsp;</td>";
						}else{
							strTabla+="<td "+colorbg+" align=left> &nbsp;"+formatoImporte(importeDIV)+" &nbsp;</td>";
						}


						if(!claveEspecial.trim().equals("")){
							tipoCambioOperacion = tipoEspecial;
						}else{
							if(getDivisaTrx(cuentaCargo).trim().equals(USD)){
								tipoCambioOperacion = tipoCambioDA;}
							else{
								tipoCambioOperacion = tipoCambioMN;}
						}
						strTabla+="<td "+colorbg+" align=left> &nbsp;"+tipoCambioOperacion+" &nbsp;</td>";

						StringTokenizer tokenConcepto = new StringTokenizer(concepto,"+");
						strTabla+="<td "+colorbg+" align=left> &nbsp;"+tokenConcepto.nextToken().trim()+" &nbsp;</td>";
						strTabla+="\n</tr>";
					}
				  }catch(Exception e){
					  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				  }finally{
				      try{
				    	  valCtas.closeTransaction();
				      }catch (Exception e) {
				    	  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				      }
				      if (valida != null) {
				    	  valida.cerrar();
				      }
				  }

					req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
					try {
						EmailSender emailSender=new EmailSender();

						EmailDetails emailDetails = new EmailDetails();

						EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><>ENTRANDO A CONFIGURACION DE NOTIFICACIONES<><><><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("<><><><>MTI_ENVIAR -> referencias: " + referencias, EIGlobal.NivelLog.INFO);
						if(referencias != null && referencias != ""){
							referencias = referencias.substring(0, referencias.length()-1);}
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> totalRegistros para Transferencia Internacional: " + totalRegistros, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> Cuenta Cargo  								    : " + Todas.camposTabla[0][0], EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> Cuenta Abono									: " + Todas.camposTabla[0][2], EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> Importe Total para Transf. Internacional  	    : " + importeTotal, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> Numero de contrato para Transf. Internacional  : " + session.getContractNumber(), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> Nombre de contrato para Transf. Internacional  : " + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> folio para Transf. Internacional               : " + referencias, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> Estatus para Transf. Internacional             : " + codErrorOper, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><>MTI_Enviar -> Estatus Mail para Transf. Internacional        : " + codError, EIGlobal.NivelLog.INFO);

						emailDetails.setNumRegImportados(Todas.totalRegistros);//
						emailDetails.setImpTotal(ValidaOTP.formatoNumero(importeTotal));
						emailDetails.setNumeroContrato(session.getContractNumber());
						emailDetails.setRazonSocial(session.getNombreContrato());
						emailDetails.setNumRef(referencias);

							if(Todas.totalRegistros > 1){

								//Paquete 4
								if(emailSender.enviaNotificacion(codError)){
									emailDetails.setEstatusActual(codError);
									emailSender.sendNotificacion(req,IEnlace.MAS_TRANS_INTERNACIONALES, emailDetails);
								}

							} else {

								emailDetails.setNumCuentaCargo(Todas.camposTabla[0][0]);
								emailDetails.setCuentaDestino(Todas.camposTabla[0][2]);
								emailDetails.setTipoTransferenciaUni(" Internacional ");

								//Paquete 4
								if(emailSender.enviaNotificacion(codError)){
									emailDetails.setEstatusActual(codError);
									emailSender.sendNotificacion(req, IEnlace.UNI_TRANS_INTERNACIONALES, emailDetails);
								}
							}
						} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}

					//Termina mail

				}


		if(comuError){
			despliegaPaginaError(mensajeError,"Env�o de Transferencias Internacionales","Transferencias &gt; Internacionales &gt; Cotizaci�n &gt; Env�o", req, res);}
		else
		{
			if(arcExp)
			{
				strTabla="Archivo de exportacion";
				Todas.strOriginal="Archivo de Exportacion";
			}
			else
			{
			   Todas.strOriginal=nuevaTrama;
			   strTabla+="\n</table>";
			}

			if(salidaArc)
			{

				ArchivoRemoto archR = new ArchivoRemoto();
				if(!archR.copiaLocalARemoto(nombreSalida,"WEB")){
				EIGlobal.mensajePorTrace("MTI_Enviar - iniciaEnviar(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);}
			}

			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
			req.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());

			req.setAttribute(TOTAL_TRANS,Todas.strOriginal);
			req.setAttribute("Tabla",strTabla);

			req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
			req.setAttribute("Archivo",usuario+".doc");

			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Env�o de Transferencias Internacionales","Transferencias &gt; Intenacionales &gt; Cotizaci�n &gt; Env�o","s25470h",req));

			ses.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			ses.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			ses.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			ses.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
			ses.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());

			ses.setAttribute(TOTAL_TRANS,Todas.strOriginal);
			ses.setAttribute("Tabla",strTabla);

			ses.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
			ses.setAttribute("Archivo",usuario+".doc");

			ses.setAttribute("MenuPrincipal", session.getStrMenu());
			ses.setAttribute("newMenu", session.getFuncionesDeMenu());
			ses.setAttribute("Encabezado", CreaEncabezado("Env�o de Transferencias Internacionales","Transferencias &gt; Intenacionales &gt; Cotizaci�n &gt; Env�o","s25470h",req));
			ses.setAttribute("web_application",Global.WEB_APPLICATION);

			if(arcExp)
			{
				req.setAttribute("totalRegistros",(String) req.getParameter("Lineas"));
				req.setAttribute("importeTotal",(String) req.getParameter("Importe"));


				//TODO BIT CU3131 Envia operaci�n DITA

				if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
				try{
					BitaConstants.archivo = getServletContext().getRealPath("/") + LOG_BITACORA;
					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
						BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DITA);
						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}
						if(Result != null){
							if(Result.substring(0,2).equals("OK")){
								bt.setIdErr(DITA0000);
							}else if(Result.length() > 8){
								bt.setIdErr(Result.substring(0,8));
							}else{
								bt.setIdErr(Result.trim());
							}
						}
						if (cuentaCargo != null) {
							bt.setCctaOrig(cuentaCargo.trim());
						}
						if (cuentaAbono != null) {
							bt.setCctaDest(cuentaAbono.trim());
						}
						if (banco !=  null) {
							if (banco.length() > 13) {
								bt.setBancoDest(banco.substring(0,13));
							}else{
								bt.setBancoDest(banco.trim());
							}
						}
						if (importe != null && !importe.equals("")) {
							bt.setImporte(Double.parseDouble(importe.trim()));
						}
						if (tipoCambioMN != null && !tipoCambioMN.equals("")) {
							bt.setTipoCambio(Double.parseDouble(tipoCambioMN.trim()));
						}
						if (divisaAbono != null) {
							bt.setTipoMoneda(divisaAbono.trim());
						}
						if (nombreArchivo != null) {
							bt.setNombreArchivo(nombreArchivo.trim());
						}
						bt.setServTransTux(TIPO_OPERACION);
						if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
							&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
									.equals(BitaConstants.VALIDA)) {
							bt.setIdToken(session.getToken().getSerialNumber());
							req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
						}

						if(beneficiario!=null && beneficiario!=""){
							bt.setBeneficiario(beneficiario.trim());
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					}
				} catch (NumberFormatException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}catch(SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				}

				evalTemplate("/jsp/MTI_EnviarArchivo.", req, res);
			}
			else
			{
				ses.setAttribute("totalRegistros","Total de Registros: "+Integer.toString(Todas.totalRegistros));
				ses.setAttribute("importeTotal","Importe Total: "+FormatoMoneda(Double.toString(importeTotal)));
				EIGlobal.mensajePorTrace("yes MTI_Enviar - termina.", EIGlobal.NivelLog.INFO);

				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
				try{
					BitaConstants.archivo = getServletContext().getRealPath("/") + LOG_BITACORA;
					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
						BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DITA);
						bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
						if(Result != null){
							if(Result.substring(0,2).equals("OK")){
								bt.setIdErr(DITA0000);
							}else if(Result.length() > 8){
								bt.setIdErr(Result.substring(0,8));
							}else{
								bt.setIdErr(Result.trim());
							}
						}
						bt.setCctaOrig((cuentaCargo == null)?" ":cuentaCargo);
						bt.setCctaDest((cuentaAbono == null)?" ":cuentaAbono);
						if (banco !=  null) {
							if (banco.length() > 13) {
								bt.setBancoDest(banco.substring(0,13));
							}else{
								bt.setBancoDest(banco.trim());
							}
						}
						bt.setImporte(Double.parseDouble((importe == null || importe.trim().equals(""))?"0":importe));
						bt.setTipoCambio(Double.parseDouble((tipoCambioMN == null || tipoCambioMN.trim().equals(""))?"0":tipoCambioMN));
						bt.setTipoMoneda((divisaAbono == null)?" ":divisaAbono);
						bt.setNombreArchivo((nombreArchivo == null)?" ":nombreArchivo);
						bt.setServTransTux(TIPO_OPERACION);
						if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
							&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
									.equals(BitaConstants.VALIDA)) {
							bt.setIdToken(session.getToken().getSerialNumber());
							req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
						}

						if(beneficiario!=null && beneficiario!=""){
							bt.setBeneficiario(beneficiario.trim());
						}

						BitaHandler.getInstance().insertBitaTransac(bt);

					}
				}catch(SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				}catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				}
				}

				evalTemplate("/jsp/MTI_Enviar.jsp", req, res);
			}
		}
	}


	/**
	 *
	 * @param titular the titular
	 * @param divisaCargo the divisa cargo
	 * @param divisaAbono the divisa abono
	 * @param contraMoneda the contra moneda
	 * @param importeMN the importe mn
	 * @param importeDA the importe da
	 * @param importeDIV the importe div
	 * @param dirOinver the dir oinver
	 * @param claveABA the clave aba
	 * @param tipoCambioMN the tipo cambio mn
	 * @param tipoCambioDA the tipo cambio da
	 * @param ciudad the ciudad
	 * @param pais the pais
	 * @param horaCotiza the hora cotiza
	 * @param cveEspecial the cve especial
	 * @param cuentaAbono the cuenta abono
	 * @param cuentaCargo the cuenta cargo
	 * @param beneficiario the beneficiario
	 * @param concepto the concepto
	 * @param banco the banco
	 * @param sucursal the sucursal
	 * @param usuario the usuario
	 * @param contrato the contrato
	 * @param clavePerfil the clave perfil
	 * @param medioEntrega the medio entrega
	 * @return the string
	 */
	String generaTrama(String titular, String divisaCargo,String divisaAbono, String contraMoneda, String importeMN, String importeDA,
	String importeDIV, String dirOinver, String claveABA, String tipoCambioMN, String tipoCambioDA, String ciudad,
	String pais, String horaCotiza, String cveEspecial,  String cuentaAbono,String cuentaCargo, String beneficiario,
	String concepto, String banco, String sucursal, String usuario, String contrato, String clavePerfil, String medioEntrega )
	{
		String regInternacional= "TRAN@VTA"    + "@" +
			divisaCargo   + "@" +
			divisaAbono   + "@" +
			cuentaCargo   + "@" +
			cuentaAbono   + "@" +
			contraMoneda  + "@" +
			importeMN     + "@" +
			importeDA     + "@" +
			importeDIV    + "@" +
			tipoCambioMN  + "@" +
			tipoCambioDA  + "@" +
			dirOinver     + "@" +
			claveABA      + "@" +
			banco         + "@" +
			ciudad        + "@" +
			pais          + "@" +
			sucursal      + "@" +
			beneficiario  + "@" +
			concepto      + "@" +
			horaCotiza    + "@" +
			cveEspecial   + "@" +
			titular       + "@";

		String cabecera= medioEntrega  + PIPE +
			usuario       + PIPE +
			TIPO_OPERACION	+ PIPE +
			contrato	    + PIPE +
			usuario 	    + PIPE +
			clavePerfil 	+ PIPE;

		return cabecera + regInternacional + "| |"; // modif para manc. inter vcl
	}

/**
 *
 * @param cuentaCargo the cuenta cargo
 * @param divisa the divisa
 */
private void tipoCambioVentanilla(String cuentaCargo, String divisa) {

	EIGlobal.mensajePorTrace("MTI_Enviar - tipoCambioVentanilla(): Entrando a tipoCambioVentanilla", EIGlobal.NivelLog.INFO);

	TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();

	try {
		EIGlobal.mensajePorTrace("CUENTA CARGO->"+cuentaCargo, EIGlobal.NivelLog.DEBUG);
		tcvCompra = tipoCambio.getCambioSugerido(cuentaCargo, "CPA", (divisa.trim().equals("MN")?"MXP":divisa), getDivisaTrx(cuentaCargo),"TRAN");
		tcvVenta  = tipoCambio.getCambioSugerido(cuentaCargo, "VTA", (divisa.trim().equals("MN")?"MXP":divisa), getDivisaTrx(cuentaCargo),"TRAN");

		Float.parseFloat(tcvCompra);
		Float.parseFloat(tcvVenta);
		tranExito = true;


	} catch (Exception e) {

		EIGlobal.mensajePorTrace("Error al tratar de obtener el tipo de cambio"+e.getMessage(), EIGlobal.NivelLog.ERROR);
		tcvCompra="";
		tcvVenta="";
		tranExito = false;
	}

			return;
		}



/**
 *
 * @param req the req
 * @param res the res
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
	public void despliegaTiempoTerminado(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
	  String strCuentas="";
	  String divisa = "";

	  tcvHora="";
	  tcvCompra="";
	  tcvVenta="";

	  HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");
	  EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	  divisa = (String)sess.getAttribute("inicialCveDivisa");
	  tipoCambioVentanilla(session.getCuentaCargo(), divisa);

	  if(tranExito==false){
		  req.setAttribute("TipoCambioError","No se pudo obtener la informaci�n del tipo de cambio.");}
	  else{
		  req.setAttribute("TipoCambioError","");}

	  req.setAttribute("TCV_Venta",tcvVenta.trim());
	  req.setAttribute("TCV_Compra",tcvCompra.trim());
	  req.setAttribute("Divisas",traeDivisas());

	  req.setAttribute("TiempoTerminado","cuadroDialogo('Ya han pasado mas de 3 minutos, por lo que el tipo de cambio pudo haber variado.\\nEs necesario realizar una nueva cotizaci�n.');");

	  req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
	  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
	  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
	  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

	  req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
	  req.setAttribute("MenuPrincipal", session.getStrMenu());
	  req.setAttribute("newMenu", session.getFuncionesDeMenu());
	  req.setAttribute("Encabezado",CreaEncabezado("Transferencias Internacionales","Transferencias &gt; Internacionales","",req));

	  evalTemplate("/jsp/MTI_Inicio.jsp", req, res);
	}

/**
 *
 * @return the string
 */
public String traeDivisas() {
	String strOpcion="";
	String totales="";
	int totalDivisas;
	PreparedStatement contQuery = null;
	PreparedStatement DivisaQuery = null;
	ResultSet contResult = null;
	ResultSet DivisaResult = null;
	Connection conn = null;

	try{
		conn = createiASConn(Global.DATASOURCE_ORACLE);

		if(conn==null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO);
			return "";
		}

		/** Contar registros de Divisas .... **********************************/
		totales="SELECT COUNT (*) FROM COMU_MON_DIVISA";
		contQuery  = conn.prepareStatement(totales);

		if(contQuery == null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): No se pudo crear Query.", EIGlobal.NivelLog.INFO);
			conn.close();
			return "";
		}

		contResult = contQuery.executeQuery();
		if(contResult==null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): No se pudo crear ResultSet.", EIGlobal.NivelLog.INFO);
			conn.close();
			try {
				contQuery.close();
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			return "";
		}

		if( !contResult.next() ){
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): No existe registro.", EIGlobal.NivelLog.ERROR);
			conn.close();
			try {
				contQuery.close();
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			return "";
		}

		totalDivisas=Integer.parseInt(contResult.getString(1));
		EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): Total de Divisas: "+Integer.toString(totalDivisas), EIGlobal.NivelLog.INFO);

		/** Traer Divisas ... *************************************************/
		String sqlDivisa="SELECT CVE_DIVISA, UPPER(DESCRIPCION) FROM COMU_MON_DIVISA ORDER BY UPPER(DESCRIPCION)";
		DivisaQuery  = conn.prepareStatement(sqlDivisa);

		if(DivisaQuery==null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
			conn.close();
			return "";
		}
		DivisaResult = DivisaQuery.executeQuery();

		if(DivisaResult==null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): No se pudo crear ResultSet.", EIGlobal.NivelLog.INFO);
			conn.close();
			try {
				DivisaQuery.close();
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			return "";
		}

		while(DivisaResult.next())
			strOpcion += "<option CLASS='tabmovtex' value='" + DivisaResult.getString(1) + "'>" + DivisaResult.getString(2) + "\n";
	}
	catch( SQLException sqle ){
		EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		return "";
	}
	finally {
		try {
			DivisaQuery.close();
			contQuery.close();
			DivisaResult.close();
			contResult.close();
			conn.close();
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
	}
	return strOpcion;
}


    /**
     * Formatea campo.
     *
     * @param campo the campo
     * @param tamanio the tamanio
     * @return the string
     */
    private String formateaCampo(String campo, int tamanio) {
        String spc = "                                                                      ";
        String campoFormateado = "";

        campo = campo.trim();

        if (campo.length() < tamanio){
            campoFormateado = campo
                    + spc.substring(0, tamanio - campo.length());}

        if (campo.length() > tamanio){
            campoFormateado = campo.substring(0, tamanio);}

        if (campo.length() == tamanio){
            campoFormateado = campo;}

        return campoFormateado;
    }

	/**
	 * Metodo para formatear el importe que necesita el servicio MANC_VALIDA.
	 *
	 * @param imp 	Cadena de importe a formatear
	 * @return 		Cadena a dos digitos
	 */
	private String formatoImporte(String imp){
		EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: cantidad recibida->" + imp, EIGlobal.NivelLog.INFO);
		String language = "la"; // ar
		String country = "MX";  // AF
		Locale local = new Locale (language,  country);
		NumberFormat nf = NumberFormat.getInstance(local);
		EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: cantidad formateada->" + nf.format(new Double (imp).doubleValue ()).replace(",",""), EIGlobal.NivelLog.INFO);
		imp = nf.format(new Double (imp).doubleValue ()).replace(",","");
		int dec = 0;
		if(imp.indexOf(".")>0){
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: Longitud cantidad->" + imp.length(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: Caracter punto->" + imp.indexOf("."), EIGlobal.NivelLog.DEBUG);
			dec = imp.length() - (imp.indexOf(".")+1);
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: Numero de decimales->" + dec, EIGlobal.NivelLog.DEBUG);
			if(dec>=2){
				return imp.substring(0, imp.indexOf(".")) + imp.substring(imp.indexOf("."), imp.indexOf(".")+3);}
			else{
				for(int i=0; i<dec; i++){
					imp += "0";}
			}
		}else{
			imp += ".00";
		}
		return imp;
	}

	/**
	 * Metodo para obtener el error de salida del servicio MANC_VALIDA
	 * cuando este no es exitoso.
	 *
	 * @param codError 	Codigo de Error de salida
	 * @return 			Descripci�n del Error
	 */
	private String getDesErrManc(String codError){
		if(codError!=null){
			if(codError.endsWith("0001")){
				return "No existe cuenta en mancomunidad";
			}else if(codError.endsWith("0002")){
				return "No existe tipo operacion en mancomunidad";
			}else if(codError.endsWith("0003")){
				return "No existe usuario en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0004")){
				return "No pudo abrir archivo en mancomunidad";
			}else if(codError.endsWith("0005")){
				return "Usuario tipo A en mancomunidad";
			}else if(codError.endsWith("0006")){
				return "Tipo de operacion no mancomunada";
			}else if(codError.endsWith("0007")){
				return "Tipo firma no autorizada en mancomunidad";
			}else if(codError.endsWith("0008")){
				return "Diferente importe en mancomunidad";
			}else if(codError.endsWith("0009")){
				return "Diferente tipo de cuenta de cargo en mancomunidad - Genere nuevo folio";
			}else if(codError.endsWith("0010")){
				return "No existe folio en mancomunidad-Verifique que sus datos sean correctos";
			}else if(codError.endsWith("0011")){
				return "Contrato-tipo de operacion no mancomunado";
			}else if(codError.endsWith("0012")){
				return "Rechazada";
			}else if(codError.endsWith("0013")){
				return "No hay registros";
			}else if(codError.endsWith("0014")){
				return "Usuarios iguales en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0015")){
				return "Diferente tipo de operacion en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0016")){
				return "Contrato no mancomunado";
			}else if(codError.endsWith("0017")){
				return "Folio no permitido en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0018")){
				return "Suma de montos autorizados menor a importe - Genere un nuevo folio";
			}else if(codError.endsWith("0019")){
				return "No se permite la operacion con cuenta en dolares";
			}else if(codError.endsWith("9999")){
				return "Servicio de mancomunidad no disponible (No se tiene conexion con la BD)";
			}
		}
	return "";
	}

	/**
	 * Metodo para obtener la divisa de la cuenta.
	 *
	 * @param cuenta the cuenta
	 * @return Divisa
	 */
	private String getDivisaTrx(String cuenta){
		String prefijo = "";
		try{
			prefijo = cuenta.substring(0,2);
			if(prefijo.trim().equals("09") || prefijo.trim().equals("42") ||
			   prefijo.trim().equals("49") || prefijo.trim().equals("82") ||
		       prefijo.trim().equals("83") || prefijo.trim().equals("97") ||
		       prefijo.trim().equals("98")){
				return USD;
		    }else if(prefijo.trim().equals("08") || prefijo.trim().equals("10") ||
					   prefijo.trim().equals("11") || prefijo.trim().equals("13") ||
					   prefijo.trim().equals("15") || prefijo.trim().equals("16") ||
					   prefijo.trim().equals("17") || prefijo.trim().equals("18") ||
					   prefijo.trim().equals("19") || prefijo.trim().equals("20") ||
					   prefijo.trim().equals("21") || prefijo.trim().equals("22") ||
					   prefijo.trim().equals("23") || prefijo.trim().equals("24") ||
					   prefijo.trim().equals("25") || prefijo.trim().equals("26") ||
					   prefijo.trim().equals("27") || prefijo.trim().equals("28") ||
					   prefijo.trim().equals("40") || prefijo.trim().equals("43") ||
					   prefijo.trim().equals("44") || prefijo.trim().equals("45") ||
					   prefijo.trim().equals("46") || prefijo.trim().equals("47") ||
					   prefijo.trim().equals("48") || prefijo.trim().equals("50") ||
					   prefijo.trim().equals("51") || prefijo.trim().equals("52") ||
					   prefijo.trim().equals("53") || prefijo.trim().equals("54") ||
					   prefijo.trim().equals("55") || prefijo.trim().equals("56") ||
					   prefijo.trim().equals("57") || prefijo.trim().equals("58") ||
					   prefijo.trim().equals("59") || prefijo.trim().equals("60") ||
					   prefijo.trim().equals("61") || prefijo.trim().equals("62") ||
					   prefijo.trim().equals("63") || prefijo.trim().equals("64") ||
					   prefijo.trim().equals("65") || prefijo.trim().equals("66") ||
					   prefijo.trim().equals("67") || prefijo.trim().equals("68") ||
					   prefijo.trim().equals("69") || prefijo.trim().equals("70") ||
					   prefijo.trim().equals("71") || prefijo.trim().equals("72") ||
					   prefijo.trim().equals("73") || prefijo.trim().equals("74") ||
					   prefijo.trim().equals("75") || prefijo.trim().equals("76") ||
					   prefijo.trim().equals("77") || prefijo.trim().equals("78") ||
					   prefijo.trim().equals("79") || prefijo.trim().equals("80") ||
					   prefijo.trim().equals("81") || prefijo.trim().equals("85") ||
					   prefijo.trim().equals("86") || prefijo.trim().equals("87") ||
					   prefijo.trim().equals("88") || prefijo.trim().equals("89") ||
					   prefijo.trim().equals("90") || prefijo.trim().equals("91") ||
					   prefijo.trim().equals("92") || prefijo.trim().equals("93") ||
					   prefijo.trim().equals("94") || prefijo.trim().equals("95") ||
					   prefijo.trim().equals("96") || prefijo.trim().equals("97") ||
					   prefijo.trim().equals("99")){
		    	return "MXP";
		    }
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("MIPD_cambios::getDivisaTrx:: Problemas..."+e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		return USD;
	}

}