/*Banco Santander Mexicano
  Clase comprobante_trans - Genera el comprobante para transf,vista,inversion 7/28,plazo
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 28/02/2001 - agregar encabezado
 */

package mx.altec.enlace.servlets;

import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.cliente.ws.csd.ConstantesSD;
import mx.altec.enlace.dao.CuentasDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.HTMLUtil;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.SelloDigitalUtils;


import java.io.IOException;

public class comprobante_trans extends mx.altec.enlace.servlets.BaseServlet {
    
    /**
     * constante de rfc
     */
     public static final String GCT_RFC = "rfc";
     /**
      * constante de iva
      */
      public static final String GCT_IVA = "iva";
      
      
    /**
     * Do get, metodo que atiende las peticiones cuando es una peticion GET
     * 
     * @param request objeto con informacion inherente de la peticion
     * @param response objeto con informacion inherente de la respuesta
     * @throws ServletException en caso de ocurrir una ServletException
     * @throws IOException en caso de ocurrir una IOException
     */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        defaultAction( request, response );
    }

    /**
     * Do post, metodo que atiende las peticiones cuando es una peticion POST
     * 
     * @param request objeto con informacion inherente de la peticion
     * @param response objeto con informacion inherente de la respuesta
     * @throws ServletException en caso de ocurrir una ServletException
     * @throws IOException en caso de ocurrir una IOException
     */
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        defaultAction( request, response );
    }

    /**
     * Metodo que atiende las peticiones cuando es una peticion GET o POST
     * 
     * @param request objeto con informacion inherente de la peticion
     * @param response objeto con informacion inherente de la respuesta
     * @throws ServletException en caso de ocurrir una ServletException
     * @throws IOException en caso de ocurrir una IOException
     */
    public void defaultAction( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {
        String idConv = ConstantesSD.CADENA_VACIA;
        char cEspacio = ' ';
        EIGlobal.mensajePorTrace("defaultAction params|" + idConv + "|"
                + request.getParameter(ConstantesSD.QUERY_PARAM_MOD) + "|",
                EIGlobal.NivelLog.INFO);
        String cuentaDestino = ConstantesSD.CADENA_VACIA;
        if (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO) != null) {
            if (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).indexOf(cEspacio) < 0) {
                cuentaDestino = request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO);
            } else {
                cuentaDestino = request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO)
                        .substring(0, request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).indexOf(cEspacio));
            }
        }
        if (ConstantesSD.MODULO_CSD.equals(request.getParameter(ConstantesSD.QUERY_PARAM_MOD)) 
                || !(idConv = CuentasDAO.consultarCuentaConv(cuentaDestino)).isEmpty()) {
            request.setAttribute(ConstantesSD.QUERY_PARAM_MOD, 
                    (request.getParameter(ConstantesSD.QUERY_PARAM_MOD) != null)
                    ? request.getParameter(ConstantesSD.QUERY_PARAM_MOD)
                    : ConstantesSD.MODULO_VSD);
            generarComprobanteSD(request, response, idConv);
        } else {
            generarComprobanteTransferencias(request, response);
        }
    }

    /*
    public int generar_comprobante_transferencias()
    {
      String contrato=session.getContractNumber();
      String desc_contrato=session.getNombreContrato();
      String fecha=ObtenFecha();
      String hora=ObtenHora();
      String usuario=session.getUserID();;
      String desc_usuario=session.getNombreUsuario();

      System.out.println("Contrato "+contrato);
      System.out.println("des_contrato "+desc_contrato);
      System.out.println("fecha "+fecha);
      System.out.println("desc_usuario "+desc_usuario);

      int salida;
      TemplateMapBasic map = new TemplateMapBasic();
      String referencia=request.getParameter("refe");
      String importe=request.getParameter("importe");
      String cta_origen=request.getParameter("cta_origen");
      String cta_destino=request.getParameter("cta_destino");
      String tipo=request.getParameter("tipo");
      String descripcion = request.getParameter( "descripcion" );
      String periodo="";
      System.out.println("referencia"+referencia);
      if (tipo.equals("4"))
        periodo=request.getParameter("plazo");
      String enlinea = request.getParameter("enlinea");
      if (enlinea.equals("n"))
      {
        fecha        = request.getParameter("fecha");
        hora         = request.getParameter("hora");
        usuario      = request.getParameter("usuario");
        desc_usuario = request.getParameter("desc_usuario");
      }
      String importe_formateado=FormatoMoneda(importe);
      request.setAttribute("contrato",contrato+"  "+desc_contrato);
      request.setAttribute("fecha",fecha);
      request.setAttribute("hora",hora);
      request.setAttribute("codigo_usuario",usuario);
      request.setAttribute("nombre_usuario",desc_usuario);
      if (tipo.equals("4"))
      {
        referencia=referencia+"     instrumento  prv    periodo "+periodo+" dias";
        request.setAttribute("referencia",referencia);
      }
      else
        request.setAttribute("referencia",referencia);

      request.setAttribute("importe",importe_formateado);
      request.setAttribute("cta_origen",cta_origen);
      request.setAttribute("cta_destino",cta_destino);
      request.setAttribute("MenuPrincipal", session.getstrMenu());

      if (tipo.equals("1"))
        request.setAttribute("desc_movimiento",request.getParameter("concepto"));
      else if (tipo.equals("2"))
        request.setAttribute("desc_movimiento","Transferencia Disponible Vista");
      else if (tipo.equals("3"))
        request.setAttribute("desc_movimiento","Transferencia Vista Disponible");
      else if (tipo.equals("4"))
        request.setAttribute("desc_movimiento","Transferencia cheque plazo");
      else if ( tipo.equals( "5" ) )
        request.setAttribute( "desc_movimiento", descripcion );

      salida=evalTemplate(IEnlace.MI_COMPROBANTE_TMPL,(ITemplateData) null, map);
      return 1;
    }
    */

    /**
     * Generacion de comprobante
     * 
     * @param request Objeto con informacion inherente de la peticion
     * @param response Objeto con informacion inherente de la respuesta
     * @throws ServletException en caso de ocurrir una ServletException
     * @throws IOException en caso de ocurrir una IOException
     */
      public void generarComprobanteTransferencias( HttpServletRequest request, HttpServletResponse response )
        throws ServletException, IOException
    {
        try {
            EIGlobal.mensajePorTrace("***comprobante_trans.class Entrando a generarComprobanteTransferencias ", EIGlobal.NivelLog.INFO);

            String fecha       = ObtenFecha();
            String hora        = ObtenHora();
            String referencia  = request.getParameter("refe");
            String importe     = request.getParameter("importe");
            String cta_origen  = request.getParameter("cta_origen");
            String cta_destino = request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO);
            String tipo        = request.getParameter("tipo");
            String descripcion = request.getParameter("descripcion" );
            String enlinea     = request.getParameter("enlinea");
            String contrato_   = request.getParameter("contrato");  //cambio jrtg
            EIGlobal.mensajePorTrace(">>>>"+contrato_+"<<<<", EIGlobal.NivelLog.INFO);

            String rfc         = request.getParameter(GCT_RFC);
            String iva         = request.getParameter(GCT_IVA);
            //String RFC      = "";
            String usuario  = "";
            String contrato = "";
            String ECF = "";

            if(validarParametosContienenXSS(request)){
                despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP, "Consulta de Bit&aacute;cora", "Consultas &gt Bit&aacute;cora &gt; Generaci&oacute;n de Comprobantes", request, response );
                return;

            }

            //System.out.println(">>>>"+contrato_+"<<<<");
            //System.out.println(request.getAttribute("contrato") );

            try {
                if(contrato_ ==null ||  "".equals(contrato_.trim()))
                {
                    HttpSession ses = request.getSession();
                    contrato_ = (String)ses.getAttribute("contratoM");
                    //System.out.println("El valor de contrato_>>"+ contrato_);
                }
            } catch (Exception e) {}


            /*java.util.Enumeration  a = request.getParameterNames();
            for (;a.hasMoreElements(); )
            {
                //System.out.println("---\n"+a.nextElement());
            }
         */
            usuario  = request.getParameter("datosusuario");
            //System.out.println("Datoscontrausuario = >" +usuario+ "<");
            contrato = request.getParameter("datoscontrato");
            //System.out.println("Datoscontrato = >" +contrato+ "<");

            try {
                //System.out.println("------------------->"+usuario +"       ---------->"+request.getParameter("desc_usuario"));
                if(request.getParameter("desc_usuario")!=null) {
                    request.setAttribute("n_usuario",request.getParameter("desc_usuario"));
                } else {
                    request.setAttribute("n_usuario",usuario);
                }
            } catch(Exception e) {
                e.printStackTrace();
                //System.out.println("\n\n###");
            }

                  if("n".equals(enlinea))
                {
                    fecha   = request.getParameter("fecha");
                    hora    = request.getParameter("hora");
                    usuario = request.getParameter("usuario")+ request.getParameter("desc_usuario");
                }
                if(descripcion==null)
                    descripcion = "transferencia";

                //EIGlobal.mensajePorTrace("***comprobante_trans.class usuario:"+comprobante, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class fecha:"+fecha+" "+hora, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class tipo:"+tipo, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class referencia:"+referencia, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class importe:"+importe, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class cta_origen:"+cta_origen, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class cta_destino:"+cta_destino, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class descripcion:"+descripcion, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class enlinea:"+enlinea, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class contato:"+contrato, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class iva:"+iva, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***comprobante_trans.class rfc:"+rfc, EIGlobal.NivelLog.INFO);


                request.setAttribute("fecha",fecha);
                request.setAttribute("hora",hora);
                request.setAttribute("usuario",usuario);
                request.setAttribute("referencia",referencia);
                request.setAttribute("importe",FormatoMoneda(importe));
                request.setAttribute("cta_origen",cta_origen);
                request.setAttribute(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO,cta_destino);
                request.setAttribute("TDC2CHQ",tipo);

                if ("0".equals(tipo)) {
                    request.setAttribute("titulo","Transferencia entre chequeras en M.N.");
                } else if ("1".equals(tipo)) {
                    request.setAttribute("titulo","Transferencia entre chequeras en D&oacute;lares");
                } else if ("2".equals(tipo)) {
                    request.setAttribute("titulo","Pago de Tarjeta de cr&eacute;dito");
                } else if ("10".equals(tipo)) {
                    request.setAttribute("titulo","Disposici&oacute;n de Tarjeta de cr&eacute;dito");
                } else if(("3".equals(tipo))||("4".equals(tipo))) {
                    request.setAttribute("titulo","Vista");
                } else if ("5".equals(tipo)) {
                    request.setAttribute("titulo","Plazo");
                } else if ("6".equals(tipo))
                {
                    request.setAttribute("titulo","Consulta General de Bit&aacute;cora");
                            //contrato = ObtenContUser();
            if(contrato_==null) {
                contrato_ = "";
            }
            if(contrato == null && contrato_!=null) {
                contrato = contrato_;
            }
            //System.out.println("CONTRATO, MANDA LLAMAR METODO ObtenContUser() " + contrato);

                }


                if (contrato_ == null && contrato != null)
                    contrato_ = contrato;
                if(contrato == null && contrato_!=null)
                contrato = contrato_;

//System.out.println("CONTRATO, MANDA LLAMAR METODO ObtenContUser() " + contrato_);
//System.out.println("CONTRATO, MANDA LLAMAR METODO ObtenContUser() " + contrato);
                request.setAttribute("contrato",contrato);
                request.setAttribute("contratoM",contrato_);

                if (enlinea.equals("n"))
                {
                    request.setAttribute( "desc_movimiento", descripcion );
                    //System.out.println(">>>>>>>>>>************ESTA DENTRO DEL ENLINEA ***************>>>>"+ descripcion);
                    if  ((request.getParameter(GCT_RFC)!=null)&&(request.getParameter(GCT_RFC).length()>0)&& //*****Cambio para comprobante fiscal
                         (request.getParameter(GCT_IVA)!=null)&&(request.getParameter(GCT_IVA).length()>0)) //*****Cambio para comprobante fiscal
                        {
                            String civa1=request.getParameter(GCT_IVA);
                            civa1=civa1.substring(0,civa1.length());
                            ECF = "<tr><td class=tittabcom align=right width=0>RFC Beneficiario:</td><td class=textabcom align=left nowrap>"+request.getParameter(GCT_RFC)+"</td></tr>";//*****Cambio para comprobante fiscal
                            ECF+= "<tr><td class=tittabcom align=right width=0>IVA:</td><td class=textabcom align=left nowrap>"+FormatoMoneda(civa1)+"</td></tr>";//*****Cambio para comprobante fiscal
                        }
                        request.setAttribute("ECF",ECF);//*****Cambio para comprobante fiscal

                } else {
                    if (tipo.equals("0")||tipo.equals("1")||tipo.equals("2")||tipo.equals("10"))
                    {
                        descripcion=request.getParameter("concepto");
                        //System.out.println("El valor de concepto"+ descripcion);
                        request.setAttribute( "desc_movimiento", descripcion.substring(0,descripcion.length()));

                         //*****Cambio para comprobante fiscal
                        if  ((request.getParameter(GCT_RFC)!=null)&&(request.getParameter(GCT_RFC).length()>0)&& //*****Cambio para comprobante fiscal
                             (request.getParameter(GCT_IVA)!=null)&&(request.getParameter(GCT_IVA).length()>0)) //*****Cambio para comprobante fiscal
                        {
                            String civa=request.getParameter(GCT_IVA);
                            civa=civa.substring(0,civa.length());
                            ECF = "<tr><td class=tittabcom align=right width=0>RFC Beneficiario:</td><td class=textabcom align=left nowrap>"+request.getParameter(GCT_RFC)+"</td></tr>";//*****Cambio para comprobante fiscal
                            ECF+= "<tr><td class=tittabcom align=right width=0>IVA:</td><td class=textabcom align=left nowrap>"+FormatoMoneda(civa)+"</td></tr>";//*****Cambio para comprobante fiscal
                        }
                        request.setAttribute("ECF",ECF);//*****Cambio para comprobante fiscal
                    }
                    else if(tipo.equals("3"))
                        request.setAttribute("desc_movimiento","Cheques - Vista ");
                    else if(tipo.equals("4"))
                        request.setAttribute("desc_movimiento","Vista - Cheques");
                    else if (tipo.equals("5"))
                    {
                        request.setAttribute("desc_movimiento","Plazo");
                        String datos_inversion="";
                        datos_inversion="<tr><td class=tittabcom align=right width=0>Plazo:</td><td class=textabcom align=left nowrap>"+request.getParameter("plazo")+"</td></tr>";
                        datos_inversion+="<tr><td class=tittabcom align=right width=0>Tipo de Inversi&oacute;n:</td><td class=textabcom align=left nowrap>"+request.getParameter("tipoinversion")+"</td></tr>";
                        request.setAttribute("inversion",datos_inversion);//*****Cambio para comprobante fiscal

                    }
                    else if (tipo.equals("7"))
                        request.setAttribute("desc_movimiento","Plazo  d&oacute;lares");

                }
            } catch(Exception e) {
                e.printStackTrace();
        }
        EIGlobal.mensajePorTrace("***ClaveBanco:"+request.getParameter("banco")+">", EIGlobal.NivelLog.INFO);
        request.setAttribute("ClaveBanco",request.getParameter("banco").trim());
        evalTemplate( IEnlace.MI_COMPROBANTE_TMPL, request, response );
    }
      
    /**
     * Generacion de comprobante con sello digital
     *
     * @param request Objeto con informacion inherente de la peticion
     * @param response Objeto con informacion inherente de la respuesta
     * @param idConv identificador del convenio
     * @throws ServletException en caso de ocurrir una ServletException
     * @throws IOException en caso de ocurrir una IOException
     */
    public void generarComprobanteSD(HttpServletRequest request, HttpServletResponse response, String idConv) throws ServletException, IOException {
        EIGlobal.mensajePorTrace("INICIA generarComprobanteSD", EIGlobal.NivelLog.INFO);
        StringBuilder msgSalida = new StringBuilder(HTMLUtil.crearElemento(
                5, HTMLUtil.generarAttr(new String[][]{{
            "style", "margin: auto; text-align: center;"}}),
                "<h3>Ocurri&oacute; un error al solicitar comprobante con sello digital, intente m&aacute;s tarde</h3>").getEtiquetaAttr());

        String modulo = SelloDigitalUtils.obtenerModulo(request);
        EIGlobal.mensajePorTrace("generarComprobanteSD modulo|" + modulo + "|", EIGlobal.NivelLog.INFO);
        if (ConstantesSD.MODULO_VSD.equals(modulo)) {
            EIGlobal.mensajePorTrace("generarComprobanteSD fecha|" + ((request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA) != null)
                    ? SelloDigitalUtils.armarFchPag(request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA).trim() + " " + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_HORA).trim())
                    : new SimpleDateFormat(ConstantesSD.FORMATO_FCH_PAGO_WS, ConstantesSD.LOCALE_MX).format(new Date())) + "|", EIGlobal.NivelLog.INFO);
            String cta;
            if (idConv != null && !idConv.isEmpty()) {
                cta = idConv;
            } else if (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).contains(" ")) {
                cta = request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO);
            } else {
                cta = request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO)
                        .substring(0, request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).indexOf(" "));
            }
            msgSalida.replace(0, msgSalida.length(), SelloDigitalUtils.validarComprobante(new String[]{
                (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_LIN_CAP) != null ? request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_LIN_CAP)
                : request.getParameter(ConstantesSD.QUERY_PARAM_TMB_LIN_CAP)),
                request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_IMPORTE),
                request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_REFE),
                (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA) != null)
                ? SelloDigitalUtils.armarFchPag(request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA).trim() + " " + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_HORA).trim())
                : new SimpleDateFormat(ConstantesSD.FORMATO_FCH_PAGO_WS, ConstantesSD.LOCALE_MX).format(new Date()),
                ConstantesSD.ID_CANAL_, cta, idConv,
                new StringBuilder(ConstantesSD.PATH_SERVLET_CSD_BITACORA).append("?").append(ConstantesSD.QUERY_PARAM_MOD).append(ConstantesSD.SIGNO_IGUAL)
                .append(ConstantesSD.MODULO_CSD).toString()}, request, response));
        } else if (ConstantesSD.MODULO_CSD.equals(modulo)) {
            StringBuilder camposFijos = new StringBuilder(request.getParameter(ConstantesSD.HIDDEN_CAMPOS_ADIC_OPER)).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_NOMBRE)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_DOMICILIO)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_COLONIA)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_CP)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_DELEGACION_MUNICIPIO)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_ESTADO)).append(ConstantesSD.SIMPLE_PIPE);

            String[] camposAdicionalesWS = request.getParameter(ConstantesSD.HIDDEN_CAMPOS_ADIC_WS).split(ConstantesSD.SPLIT_DOBLE_PIPE);
            for (String string : camposAdicionalesWS) {
                EIGlobal.mensajePorTrace("generarComprobanteSD camposAdicionalesWS|" + string + "|", EIGlobal.NivelLog.ERROR);
            }
            StringBuilder camposAdicionales = new StringBuilder(ConstantesSD.CADENA_VACIA);
            for (int i = 0; Integer.parseInt(request.getParameter(ConstantesSD.HIDDEN_NUM_CAMPOS_ADIC_WS)) > 0 && i < camposAdicionalesWS.length; i++) {
                if (camposAdicionalesWS != null && camposAdicionalesWS[i] != null
                        && camposAdicionalesWS[i].split(ConstantesSD.SPLIT_SIMPLE_PIPE).length == 3) {
                    String name = camposAdicionalesWS[i].split(ConstantesSD.SPLIT_SIMPLE_PIPE)[0];
                    EIGlobal.mensajePorTrace("generarComprobanteSD camposDinamicos campo|" + name + "| valor|" + request.getParameter(name) + "|", EIGlobal.NivelLog.ERROR);
                    camposAdicionales.append(name).append(ConstantesSD.SIMPLE_PIPE).append((ConstantesSD.TIPO_DATO_FECHA.equals(camposAdicionalesWS[i].split(ConstantesSD.SPLIT_SIMPLE_PIPE)[1]))
                            ? SelloDigitalUtils.fchAdicc(request.getParameter(name))
                            : request.getParameter(name)).append(ConstantesSD.SIMPLE_PIPE);
                }
            }
            EIGlobal.mensajePorTrace("generarComprobanteSD camposDinamicos|" + camposAdicionales.toString() + "| camposFijos|" + camposFijos.toString() + "|", EIGlobal.NivelLog.ERROR);
            msgSalida.replace(0, msgSalida.length(), SelloDigitalUtils.generarComprobante(new String[]{
                camposFijos.toString(), camposAdicionales.toString()}, request, response));
        } else {
            EIGlobal.mensajePorTrace("generarComprobanteSD no se encontro modulo", EIGlobal.NivelLog.ERROR);
        }
        request.setAttribute(ConstantesSD.HTML_SD, msgSalida.toString());
        evalTemplate(ConstantesSD.PATH_JSP_SD, request, response);
        EIGlobal.mensajePorTrace("FINALIZA generarComprobanteSD msgSalida|" + msgSalida.toString() + "|", EIGlobal.NivelLog.INFO);
    }
}
/*2.0*/
