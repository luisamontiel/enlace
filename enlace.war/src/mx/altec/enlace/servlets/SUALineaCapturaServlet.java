package mx.altec.enlace.servlets;

/**
 * Clase principal para el control de flujo de linea de captura.
 * Acciones principales:
 * inicio
 * confirmacion
 *
 */
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.SUALCLZS8;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.SUAGenCompPDF;
import mx.altec.enlace.bo.SUALineaCapturaAction;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Servlet implementation class for Servlet: SUALineaCapturaServlet.
 *
 */
 public class SUALineaCapturaServlet  extends BaseServlet{
	private static final long serialVersionUID = 6704386494464800779L;

	/**
	 * Constante para referenciar la clave de operacion de SUA por Linea de Captura
	 */
	private static final String ES_PAGO_SUA_LINEA_CAPTURA = "SULC";

  /**
   * Constante para referenciar la cuenta de cargo en la sesion
   */
	private static final String CTA_CARGO = "cargo";

	/**
	 * Constante para representar cuando el horario no aplica
	 */
	private static final String HORARIO_NA = "NA";

	/**
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction( request, response );
	}

	/**
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction( request, response );
	}

	/**
	 * Metodo principal para el manejo de Flujo de Linea de captura
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void defaultAction(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace ("Entra en SUALC.....",EIGlobal.NivelLog.INFO);
		try{
			HttpSession sessionHttp = request.getSession();
			/*Obtencion de session del BaseResource */
			BaseResource sesion=(BaseResource)sessionHttp.getAttribute("session");

			boolean sesionvalida= SesionValida( request, response );
			//validacion de session general
			if(sesionvalida ){

				String valida = getFormParameter(request,"valida" );
				EIGlobal.mensajePorTrace(" JGAL - valida: [" + valida + "]", EIGlobal.NivelLog.INFO);
				if ( validaPeticion( request, response,session,sessionHttp,valida))
				{
					  EIGlobal.mensajePorTrace("\n\n ENTRA A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.INFO);
				}
				//*****************************************Termina validacion OTP**************************************
				else
				{

					request.setAttribute("MenuPrincipal", sesion.getStrMenu());
					request.setAttribute("newMenu", sesion
	                        .getFuncionesDeMenu());

					request.getSession().setAttribute(BitaConstants.FLUJO, BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA);
					request.getSession().setAttribute(BitaConstants.SESS_ID_FLUJO, BitaConstants.ES_PAGO_SUA_LINEA_CAPTURA);

					String archivoAyuda = "s31000h";
					request.setAttribute("Encabezado", CreaEncabezado("S.U.A. L&iacute;nea de Captura",
							"Servicios &gt; S.U.A. &gt; L&iacute;nea de Captura",
							archivoAyuda, request));
					String opcion =null;
					if(request.getParameter("opcionLC")!=null){
						opcion =request.getParameter("opcionLC");
					}else{
						opcion = (String)request.getAttribute("opcionLC");
					}

					if(opcion==null || opcion.length()==0){
						opcion="1";
					}

					EIGlobal.mensajePorTrace (("SUALineaCapturaServlet--> Opcion: "+opcion),EIGlobal.NivelLog.INFO);
					/***************************************************************
					 ***  Opcion Inicial Muestra de Datos para Linea de Captura  ***
					 **************************************************************/
					if(opcion.equals("1")){
						EIGlobal.mensajePorTrace("Enlace SUALineaCapturaServlet Pagina Inicial", EIGlobal.NivelLog.INFO);
						sesion.setModuloConsultar(IEnlace.MCargo_transf_pesos);
						request.removeAttribute("beanSUALC");
						request.getSession().removeAttribute("cargoSUALC");
						request.getSession().removeAttribute("titularSUALC");
						request.getSession().removeAttribute("fechaSUALC");
						request.getSession().removeAttribute("importeSUALC");
						request.getSession().removeAttribute("lcSUALC");
						//reedireccionamos a la pagina de peticion de datos
						request.setAttribute("contrato",sesion.getContractNumber() +" " + sesion.getNombreContrato() );
						evalTemplate("/jsp/SUALineaCaptura.jsp", request, response );
					}else if(opcion.equals("2")){
						/***********************************************************
						 ***  Opcion de verificacion de datos de LC             ***
						 **    y Validacion de LC (agregado 4-sept-2009)
						 **********************************************************/
						EIGlobal.mensajePorTrace("Enlace SUALineaCapturaServlet Verificacion de datos y validacion de LC", EIGlobal.NivelLog.INFO);
						request.setAttribute("contrato",sesion.getContractNumber()
								+ " " + 	sesion.getNombreContrato());

						if (validaHorario()) {
							EIGlobal.mensajePorTrace("SUALineaCapturaServlet - defaultAction - Valida LYM", EIGlobal.NivelLog.INFO);
							LYMValidador val = new LYMValidador();
		 					String resLYM = val.validaLyM(sesion.getContractNumber(),
		 													sesion.getUserID8(), "CSLC", request.getParameter("importeCargoLC"));
		 					val.cerrar();
							if(resLYM.equals("ALYM0000")) {
								String datos[] = request.getParameter("cboCuentaCargo").split("-");
								request.setAttribute(CTA_CARGO,datos[0]);
								request.setAttribute("titular",datos[1]);
								SUALineaCapturaAction actionSUA= new SUALineaCapturaAction();
								//Validacion de LC e importe
								String validacionLC = actionSUA.validaLC(
										request.getParameter("lineaCaptura"),
										request.getParameter("importeCargoLC"),request);
								//segun resultado mostramos Botones
								if((validacionLC != null) && (validacionLC.equals("OK"))){
									request.setAttribute("valida","true");
									request.setAttribute("validaMsj",null);
								}else{
								//08-2012 se omite validacion de Errores para que muestre el Error de Mancomunidad
								//Si contiene los codigos siguientes se omite el
								//error y continua a el paso de Pago LC
								/*	if((validacionLC != null) && (validacionLC.contains("B17")
											|| validacionLC.contains("857")
											|| validacionLC.contains("780")
											|| validacionLC.contains("B25"))){
										request.setAttribute("valida","true");
										request.setAttribute("validaMsj",null);
									}else{*/
										request.setAttribute("valida","false");
										request.setAttribute("validaMsj",validacionLC);
									}
								//}

								SimpleDateFormat sdf= new SimpleDateFormat();
								sdf.applyPattern("dd/MM/yyyy");
								request.setAttribute("fecha",sdf.format(new Date()));
								request.setAttribute("importe",request.getParameter("importeCargoLC"));
								request.setAttribute("lc",request.getParameter("lineaCaptura"));
								request.setAttribute("cuentaNombre",request.getParameter("cboCuentaCargo"));

								evalTemplate("/jsp/SUAConfirmaEnvio.jsp", request, response );

							}else{
								despliegaPaginaErrorURL("<font color=red>Error </font><br> L�mites y Montos no permitidos para esta operaci&oacute;n.","S.U.A. L&iacute;nea de Captura","Servicios &gt; S.U.A. &gt; Pagos" ,"","SUALineaCapturaServlet?opcionLC=1", request, response);
							}

						} else { // Validacion de Horario
							EIGlobal.mensajePorTrace("SUALineaCapturaServlet - defaultAction - Operacion Fuera de Horario", EIGlobal.NivelLog.INFO);
							despliegaPaginaErrorURL("<font color=red>Error </font><br> Operaci&oacute;n Fuera de Horario o es d&iacute;a inh&aacute;bil.</br>","S.U.A. L&iacute;nea de Captura","Servicios &gt; S.U.A. &gt; Pagos" ,"","SUALineaCapturaServlet?opcionLC=1", request, response);
						}
					}else if(opcion.equals("3")){
						/***********************************************************
						 ***Opcion para la busqueda de registros anteriores de LC***
						 **********************************************************/
						evalTemplate("/SUAConsultaPago", request, response );
					}
					else if(opcion.equals("4")){
						/***********************************************************
						 ***  Opcion para la muestra de mensaje de la operacion ***
						 **********************************************************/

						EIGlobal.mensajePorTrace("Enlace SUALineaCapturaServlet " +
								"Mensaje de Operacion SUALC", EIGlobal.NivelLog.INFO);
						String datoscta[]=null;

						ValidaCuentaContrato valCtas = new ValidaCuentaContrato();

						EIGlobal.mensajePorTrace("Enlace SUALineaCapturaServlet " +
								"antes de validar la cta ", EIGlobal.NivelLog.DEBUG);

						datoscta = valCtas.obtenDatosValidaCuenta(sesion.getContractNumber(),
								sesion.getUserID8(),
								sesion.getUserProfile(),
						          request.getParameter(CTA_CARGO),
						          IEnlace.MCargo_transf_pesos);
						if(datoscta!=null)
						{
							//Ejecuta LLamado de Token
							// Validamos si nesesita Token...

							//	Correccion JGAL
							EIGlobal.mensajePorTrace("\n\n\n JGAL ANTES DE VERIFICACION DE TOKEN \n\n\n",
									EIGlobal.NivelLog.INFO);
							//interrumpe la transaccion para invocar la validacion
							EIGlobal.mensajePorTrace(" JGAL - valida2: [" + valida + "]", EIGlobal.NivelLog.DEBUG);

							if (valida == null)	{
								EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario" +
										" tiene token y si la transaccion esta parametrizada " +
										"para solicitar la validacion con el OTP \n\n\n",
										EIGlobal.NivelLog.INFO);

								boolean solVal=ValidaOTP.solicitaValidacion(
											sesion.getContractNumber(),IEnlace.ALTA_CUENTAS);

								EIGlobal.mensajePorTrace("\n JGAL solVal: [" + solVal +"] \n",
										EIGlobal.NivelLog.DEBUG);

								if( sesion.getValidarToken() &&
									sesion.getFacultad(BaseResource.FAC_VAL_OTP) &&
									sesion.getToken().getStatus() == 1 &&
									solVal ){

									//VSWF-HGG -- Bandera para guardar el token en la bitacora
									request.getSession().setAttribute(BitaConstants.
											SESS_BAND_TOKEN, BitaConstants.VALIDA);
									EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. " +
											"\nSolicito la validacion. \nSe guardan los parametros " +
											"en sesion \n\n\n", EIGlobal.NivelLog.INFO);

									guardaParametrosEnSession(request);
									request.getSession().removeAttribute("mensajeSession");
									EIGlobal.mensajePorTrace("antes de poner el mensaje ",EIGlobal.NivelLog.DEBUG);
									ValidaOTP.mensajeOTP(request, "SipareLC");
									ValidaOTP.validaOTP(request,response, IEnlace.VALIDA_OTP_CONFIRM);
								} else {
									EIGlobal.mensajePorTrace("\n Token deshabilitado.",
											EIGlobal.NivelLog.INFO);

	                                ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
	                                valida="1";
								}
							}
							//retoma el flujo
							EIGlobal.mensajePorTrace("Enlace SUALineaCapturaServlet " +
									"JGAL - Retoma el flujo", EIGlobal.NivelLog.INFO);

							EIGlobal.mensajePorTrace(" JGAL - valida 3: [" + valida + "]", EIGlobal.NivelLog.INFO);

							if( valida!=null && valida.equals("1"))	{
								EIGlobal.mensajePorTrace("\n\n\n SIPARE SE RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.INFO);
								//ejecutamos operaciones si nesesidad de pedir token
								String lc = getFormParameter(request, "lc");
								SUALCLZS8  beanValidaciones=null;
								//boolean enviaNotificacion = false;//modificar cuando se tengan los codigos correctos iver

								EIGlobal.mensajePorTrace("EN validacion ........llamamos Action para ejecutar pago",EIGlobal.NivelLog.DEBUG);
								request.setAttribute("Encabezado", CreaEncabezado("L&iacute;nea de Captura",
										"Servicios &gt; Nuevo Esquema de Pago de Impuestos &gt; L&iacute;nea de Captura",
										archivoAyuda, request));
									SUALineaCapturaAction actionSUA= new SUALineaCapturaAction();
									EIGlobal.mensajePorTrace("Antes de invocar Action sin Token.....................",EIGlobal.NivelLog.DEBUG);

									EIGlobal.mensajePorTrace("r valida-> " + getFormParameter(request,"valida"), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("r cargoSUALC-> " + request.getParameter(CTA_CARGO), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("r titularSUALC-> " + request.getParameter("titular"), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("r fechaSUALC-> " + request.getParameter("fecha"), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("r importeSUALC-> " + request.getParameter("importe"), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("r lcSUALC-> " + request.getParameter("lc"), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("r cuentaNombre-> " + request.getParameter("cuentaNombre"), EIGlobal.NivelLog.INFO);

									beanValidaciones = actionSUA.
										validacionPagoSUALC( //cargo-Abono-fecha-importe-LC
										request.getParameter(CTA_CARGO),Global.SUALC_CTA_ABONO,request.getParameter("fecha"),
										request.getParameter("importe"), lc,true,true,request);

									request.setAttribute("estatus",beanValidaciones.getEstatus().replace("Operacion", "Operaci&oacute;n"));
									beanValidaciones.setLineaCap(lc);
									beanValidaciones.setCuentaCargo(request.getParameter( CTA_CARGO));
									//grabaBita(beanValidaciones, sesion,request.getParameter( "importe") );
									EIGlobal.mensajePorTrace("SUALineaCapturaServlet >> Graba en Bitacora Desde Front: >>"+beanValidaciones.getMsjErr() ,EIGlobal.NivelLog.INFO);
									grabaBita(beanValidaciones, sesion,request.getParameter( "importe"), request );

									beanValidaciones.setNombreCuentaCargo(request.getParameter("cuentaNombre"));
									beanValidaciones.setTitular(request.getParameter( "titular"));
									request.getSession().setAttribute("beanSUALC",beanValidaciones);

										request.setAttribute("referencia", (beanValidaciones.getReferencia()!=null?beanValidaciones.getReferencia():""));
										EIGlobal.mensajePorTrace("Referencia: " +beanValidaciones.getReferencia(),EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("CodError: " +beanValidaciones.getCodErr(),EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("MSJError: " +beanValidaciones.getMsjErr(),EIGlobal.NivelLog.INFO);
										if((beanValidaciones.getMsjErr() != null) && (beanValidaciones.getMsjErr().contains("B17")
												|| beanValidaciones.getMsjErr().contains("857")
												|| beanValidaciones.getMsjErr().contains("780")
												|| beanValidaciones.getMsjErr().contains("B25")//Agregado el dia 14-oct.
												|| beanValidaciones.getMsjErr().contains("@ER")
												|| beanValidaciones.getMsjErr().contains("Operacion Exitosa")
												)){
											//enviaNotificacion = true;
											request.setAttribute("mensajeErr",null);//para imprimir PDF
										}else{
											request.setAttribute("mensajeErr",beanValidaciones.getMsjErr());
										}

								// Parte 2

								//if (enviaNotificacion) {

									EmailSender emailSender = new EmailSender();
									EmailDetails beanEmailDetails = new EmailDetails();
									if(emailSender.enviaNotificacion(beanValidaciones.getCodErr())){
											EIGlobal.mensajePorTrace("NumeroContrato ->" + sesion.getContractNumber(), EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace("RazonSocial ->" + sesion.getNombreContrato(), EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace("CuentaCargo ->" + request.getParameter(CTA_CARGO), EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace("Importe ->" + request.getParameter("importe"), EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace("Linea de Captura ->" + request.getParameter("lc"), EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace("codErrorOper ->" + beanValidaciones.getCodErr(), EIGlobal.NivelLog.INFO);

											beanEmailDetails.setNumeroContrato(sesion.getContractNumber());
											beanEmailDetails.setRazonSocial(sesion.getNombreContrato());
											beanEmailDetails.setNumCuentaCargo(request.getParameter("lc"));
											beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(request.getParameter("importe")));
											beanEmailDetails.setTipoPagoImp("SUA L�nea de Captura");
											beanEmailDetails.setEstatusActual(beanValidaciones.getEstatus());
											EIGlobal.mensajePorTrace("Estatus->" + beanEmailDetails.getEstatusActual(), EIGlobal.NivelLog.INFO);

											EIGlobal.mensajePorTrace("SIPARE mail->" + IEnlace.PAGO_SUA_LC, EIGlobal.NivelLog.DEBUG);

													beanEmailDetails.setEstatusActual(beanValidaciones.getCodErr());
													emailSender.sendNotificacion(request,IEnlace.PAGO_SUA_LC,beanEmailDetails);
									 }
								//} else {
									//EIGlobal.mensajePorTrace("�������������������� No manda mensaje. Resultado de la operacion ->" + beanValidaciones.getCodErr(), EIGlobal.NivelLog.INFO);
								//}

								request.setAttribute("contrato",sesion.getContractNumber()
										+ " " + 	sesion.getNombreContrato());
								request.setAttribute(CTA_CARGO, request.getParameter(CTA_CARGO));
								request.setAttribute("titular", request.getParameter("titular"));
								request.setAttribute("fecha", request.getParameter("fecha"));
								request.setAttribute("importe", request.getParameter("importe"));
								request.setAttribute("lc",request.getParameter("lc"));
								request.setAttribute("cuentaNombre",request.getParameter("cuentaNombre"));
								EIGlobal.mensajePorTrace("\n\n\n SIPARE de RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.INFO);
								request.setAttribute("usuario",sesion.getUserID8());
								evalTemplate("/jsp/SUAConfirmacion.jsp", request, response );


							}

						}else{

							despliegaPaginaErrorURL("<font color=red>Error </font><br> La cuenta " + request.getParameter(CTA_CARGO) + " no pertenece al contrato","S.U.A. L&iacute;nea de Captura","Servicios &gt; S.U.A. &gt; Pagos" ,"","SUALineaCapturaServlet?opcionLC=1", request, response);

						}

					}else if(opcion.equals("5")){
						/***********************************************************
						 ***  Opcion para la Validacion del Token                ***
						 **********************************************************/
						EIGlobal.mensajePorTrace("Validar Token en SUA LC: " + request.getParameter("token"),EIGlobal.NivelLog.INFO);
						//Iniciamos la validacion del token

						 if (validaPeticion(request, response, sesion, sessionHttp, "0")){

							//Termina la validacion del token y realizamos la operacion
							 SUALineaCapturaAction actionSua= new SUALineaCapturaAction();

							SUALCLZS8 beanValidaciones = actionSua.
								validacionPagoSUALC(
										request.getSession().getAttribute("cargoSUALC").toString(),
										Global.SUALC_CTA_ABONO,
										request.getSession().getAttribute("fechaSUALC").toString(),
										request.getSession().getAttribute("importeSUALC").toString(),
										request.getSession().getAttribute("lcSUALC").toString(),true,true,request);
							request.setAttribute("estatus",beanValidaciones.getEstatus().replace("Operacion", "Operaci&oacute;n"));
							request.setAttribute("beanSUALC",beanValidaciones);
							request.setAttribute("contrato",sesion.getContractNumber()
									+ " " + 	sesion.getNombreContrato());
							request.setAttribute(CTA_CARGO, request.getSession().getAttribute("cargoSUALC"));
							request.setAttribute("titular", request.getSession().getAttribute("titularSUALC"));
							request.setAttribute("fecha", request.getSession().getAttribute("fechaSUALC"));
							request.setAttribute("importe", request.getSession().getAttribute("importeSUALC"));
							request.setAttribute("lc",request.getSession().getAttribute("lcSUALC"));
							request.setAttribute("cuentaNombre",request.getSession().getAttribute("cuentaNombre"));
							beanValidaciones.setLineaCap(String.valueOf(request.getSession().getAttribute("lcSUALC")));
							beanValidaciones.setCuentaCargo(String.valueOf(request.getSession().getAttribute("cargoSUALC")));
							beanValidaciones.setNombreCuentaCargo(String.valueOf(request.getSession().getAttribute("nombrecargoSUALC")));
							beanValidaciones.setTitular(String.valueOf(request.getSession().getAttribute("titularSUALC")));
							request.getSession().setAttribute("beanSUALC",beanValidaciones);
							request.setAttribute("referencia", (beanValidaciones.getReferencia()!=null?beanValidaciones.getReferencia():""));
							EIGlobal.mensajePorTrace("Referencia: " +beanValidaciones.getReferencia(),EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("CodError: " +beanValidaciones.getCodErr(),EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("MSJError: " +beanValidaciones.getMsjErr(),EIGlobal.NivelLog.INFO);
							if((beanValidaciones.getMsjErr() != null) && (beanValidaciones.getMsjErr().contains("B17")
									|| beanValidaciones.getMsjErr().contains("857")
									|| beanValidaciones.getMsjErr().contains("780")
									|| beanValidaciones.getMsjErr().contains("Operacion Exitosa")
									|| beanValidaciones.getMsjErr().contains("B25")//Agregado el dia 14-oct.
									)){
									request.setAttribute("mensajeErr",null);//para imprimir PDF
							}else{
								request.setAttribute("mensajeErr",beanValidaciones.getMsjErr());
							}

							grabaBita(beanValidaciones, sesion, request.getSession().getAttribute("importeSUALC").toString(), request);
							request.setAttribute("usuario",sesion.getUserID8());
							evalTemplate("/jsp/SUAConfirmacion.jsp", request, response );
						}else{
							request.setAttribute("estatus","Token invalido");

							request.setAttribute("contrato",sesion.getContractNumber()
									+ " " + 	sesion.getNombreContrato());
							request.setAttribute(CTA_CARGO, request.getSession().getAttribute("cargoSUALC"));
							request.setAttribute("titular", request.getSession().getAttribute("titularSUALC"));
							request.setAttribute("fecha", request.getSession().getAttribute("fechaSUALC"));
							request.setAttribute("importe", request.getSession().getAttribute("importeSUALC"));
							request.setAttribute("lc",request.getSession().getAttribute("lcSUALC"));
							request.setAttribute("referencia","");
							request.setAttribute("cuentaNombre",request.getSession().getAttribute("cuentaNombre"));
							EIGlobal.mensajePorTrace("Terminamos y enviamos  SUAConfirmacion.......................", EIGlobal.NivelLog.DEBUG);
							request.setAttribute("usuario",sesion.getUserID8());
							evalTemplate("/jsp/SUAConfirmacion.jsp", request, response );
						}
					}if(opcion.equals("6")){
						/***********************************************************
						 ***  Opcion para la impresion de comrobante             ***
						 **********************************************************/
						EIGlobal.mensajePorTrace("Enlace SUALineaCapturaServlet Impresion", EIGlobal.NivelLog.INFO);
						SUALCLZS8 beanDao = (SUALCLZS8)request.getSession().getAttribute("beanSUALC");
						EIGlobal.mensajePorTrace("Enlace a SUALineaCapturaServlet", EIGlobal.NivelLog.DEBUG);
						double importeTotal;
						try{

							importeTotal=Double.valueOf(beanDao.getImporteTotal());
							importeTotal=(importeTotal/100);//por los decimales
						}catch(NumberFormatException exception){
							importeTotal=0;
						}
						String periodopagMes=null;
						String periodopagAnio=null;
						if(beanDao.getPeriodoPago()!=null && beanDao.getPeriodoPago().length()>0){
							try{
								periodopagAnio=beanDao.getPeriodoPago().substring(0,4);
								periodopagMes=beanDao.getPeriodoPago().substring(4,beanDao.getPeriodoPago().length());
							}catch(IndexOutOfBoundsException ibf){
								periodopagAnio="";
								periodopagMes="";
							}
						}else{
							periodopagAnio="";
							periodopagMes="";
						}
						SUAGenCompPDF comp;
						if(beanDao.getMsjErr().equals("Operacion Exitosa")){
							comp = new SUAGenCompPDF(
								sesion.getContractNumber(),
								sesion.getNombreContrato(),
							sesion.getUserID8(),
							sesion.getNombreUsuario(),
							beanDao.getFolioSua(),
							beanDao.getCuentaCargo(),
							beanDao.getTitular(),
							beanDao.getRegPatronal(),
							periodopagMes,
							periodopagAnio,
							beanDao.getLineaCap(),
							importeTotal,
							beanDao.getCodOperacion(),
							beanDao.getFechaOperacion(),
							beanDao.getHoraOperacion(),
							"INTERNET",
							"EXITO"
							,beanDao.getMsjErr());
						}else{
							SimpleDateFormat sdf= new SimpleDateFormat();
							sdf.applyPattern("dd/MM/yyyy hh:mm:ss");
							comp = new SUAGenCompPDF(
									sesion.getContractNumber(),
									sesion.getNombreContrato(),
								sesion.getUserID8(),
								sesion.getNombreUsuario(),
								"0000",
								beanDao.getCuentaCargo(),
								beanDao.getTitular(),
								beanDao.getLineaCap()!=null?beanDao.getLineaCap().substring(0,8):"--------",
								periodopagMes,
								periodopagAnio,
								beanDao.getLineaCap(),
								importeTotal,
								beanDao.getCodOperacion(),
								sdf.format(new Date()),
								"",
								"INTERNET",
								"RECHAZO",
								beanDao.getMsjErr());
						}

						pantCompSUA(request, response, comp.generaComprobanteBAOS());
					}

				}
			} //sesion
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("\n Hubo un error : \n"+e,EIGlobal.NivelLog.INFO);
			despliegaPaginaError("Error inesperado en el Servidor.",
	  				"S.U.A. L&iacute;nea de Captura",
					"Servicios &gt; S.U.A. &gt; L&iacute;nea de Captura",
					request, response );
	  	}

	}


	/**
	 * Valida el horario de SIPARE con datos parametrizados en los EI.cfg
	 * @return Devuelve verdadero si el servicio esta disponible
	 */
	public boolean validaHorario() {
		EIGlobal.mensajePorTrace("SUALineaCapturaServlet - validaHorario - Entra", EIGlobal.NivelLog.INFO);
		boolean continuar = true;
		Calendar hoy = new GregorianCalendar();
		EIGlobal.mensajePorTrace("SUALineaCapturaServlet - validaHorario - Hora, " +
				"Dia Actual[" + hoy.get(hoy.HOUR_OF_DAY) + "],[" + hoy.get(hoy.DAY_OF_WEEK) + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("SUALineaCapturaServlet - validaHorario - Dias Inhabiles [" +
				Global.DIAS_INHABILES_SIPARE + "]", EIGlobal.NivelLog.INFO);
		StringTokenizer diasInhabiles = new StringTokenizer(Global.DIAS_INHABILES_SIPARE, "|");
		String dia = "";
		String horaIni= "";
		String horaFin = "";
		Date ahora = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String diaActual = sdf.format(ahora);
		String diasInhabilesDB = diasInhabilesAnt();

		EIGlobal.mensajePorTrace("DIA ACTUAL " + diaActual, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("DIAS INHABILES DB ["+diasInhabilesDB+"]", EIGlobal.NivelLog.DEBUG);


		if(diasInhabilesDB.contains(diaActual)){
			EIGlobal.mensajePorTrace("NO SE PUEDE OPERAR POR SER DIA INHABIL", EIGlobal.NivelLog.DEBUG);
			continuar = false;
			return continuar;
		}
		while(diasInhabiles.hasMoreTokens()){
			dia = diasInhabiles.nextToken();
			horaIni = diasInhabiles.nextToken();
			horaFin = diasInhabiles.nextToken();
			if ((dia.equals("SABADO") && (hoy.get(hoy.DAY_OF_WEEK) == hoy.SATURDAY))){
				if (horaIni.equals(HORARIO_NA) || hoy.get(hoy.HOUR_OF_DAY) < Integer.parseInt(horaIni) ||
						hoy.get(hoy.HOUR_OF_DAY) >= Integer.parseInt(horaFin)) {
					continuar = false;
					break;
				}
			} else if ((dia.equals("DOMINGO") && (hoy.get(hoy.DAY_OF_WEEK) == hoy.SUNDAY))){
				if (horaIni.equals(HORARIO_NA) || hoy.get(hoy.HOUR_OF_DAY) < Integer.parseInt(horaIni) ||
						hoy.get(hoy.HOUR_OF_DAY) >= Integer.parseInt(horaFin)) {
					continuar = false;
					break;
				}
			} else if ((dia.equals("LUNES") && (hoy.get(hoy.DAY_OF_WEEK) == hoy.MONDAY))){
				if (horaIni.equals(HORARIO_NA) || hoy.get(hoy.HOUR_OF_DAY) < Integer.parseInt(horaIni) ||
						hoy.get(hoy.HOUR_OF_DAY) >= Integer.parseInt(horaFin)) {
					continuar = false;
					break;
				}
			} else if ((dia.equals("MARTES") && (hoy.get(hoy.DAY_OF_WEEK) == hoy.TUESDAY))){
				if (horaIni.equals(HORARIO_NA) || hoy.get(hoy.HOUR_OF_DAY) < Integer.parseInt(horaIni) ||
						hoy.get(hoy.HOUR_OF_DAY) >= Integer.parseInt(horaFin)) {
					continuar = false;
					break;
				}
			} else if ((dia.equals("MIERCOLES") && (hoy.get(hoy.DAY_OF_WEEK) == hoy.WEDNESDAY))){
				if (horaIni.equals(HORARIO_NA) || hoy.get(hoy.HOUR_OF_DAY) < Integer.parseInt(horaIni) ||
						hoy.get(hoy.HOUR_OF_DAY) >= Integer.parseInt(horaFin)) {
					continuar = false;
					break;
				}
			} else if ((dia.equals("JUEVES") && (hoy.get(hoy.DAY_OF_WEEK) == hoy.THURSDAY))){
				if (horaIni.equals(HORARIO_NA) || hoy.get(hoy.HOUR_OF_DAY) < Integer.parseInt(horaIni) ||
						hoy.get(hoy.HOUR_OF_DAY) >= Integer.parseInt(horaFin)) {
					continuar = false;
					break;
				}
			}else if ((dia.equals("VIERNES") && (hoy.get(hoy.DAY_OF_WEEK) == hoy.FRIDAY))){
				if (horaIni.equals(HORARIO_NA) || hoy.get(hoy.HOUR_OF_DAY) < Integer.parseInt(horaIni) ||
						hoy.get(hoy.HOUR_OF_DAY) >= Integer.parseInt(horaFin)) {
					continuar = false;
					break;
				}
			}
		}
		return continuar;
	}


	/**
	 * Generacion del comprobante PDF
	 * @param req request
	 * @param res response
	 * @param archivo el archivo a imprimir
	 * @throws ServletException lanza Servlet Exception
	 * @throws IOException lanza IOExecption
	 */
	private void  pantCompSUA(HttpServletRequest req, HttpServletResponse res, ByteArrayOutputStream archivo) throws ServletException, IOException
	{


		res.setHeader("Expires", "0");
		res.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		res.setHeader("Pragma", "public");
		// setting the content type
		res.setContentType("application/pdf");
		// the contentlength is needed for MSIE!!!
		res.setContentLength(archivo.size());
		// write ByteArrayOutputStream to the ServletOutputStream
		ServletOutputStream out = res.getOutputStream();
		archivo.writeTo(out);
		out.flush();
		out.close();
	}
	/**
	 * Metodo para grabar la bitacora
	 * solo si la operacion fue exitosa.
	 * @param bo bean de la transaccion
	 * @param bs instancia de BaseResource
	 * @param importe importe
	 */
	void grabaBita(SUALCLZS8 bo,BaseResource bs,String importe, HttpServletRequest req){
		try{
		if(bo!=null){
			if(bo.getMsjErr().equals("Operacion Exitosa")|| bo.getCodErr().equals("MANC0000")){
				double imp=0;
				try{
					imp=Double.parseDouble(importe);
				}catch(NumberFormatException e){
					imp=0.0;
				}catch (Exception e){
					imp=0.0;
				}
				if (bo.getCodErr().length() > 13){
					if (bo.getCodErr().toUpperCase().equals("OPERACION EXITOSA")) {
						bo.setCodErr("SULC0000");
					} else {
						bo.setCodErr(bo.getCodErr().substring(0, 13));
					}
				}
				EIGlobal.mensajePorTrace(obten_referencia_operacion
						(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)) +" - "+
						(bo.getCodErr()!=null?bo.getCodErr():"")+" - "+
						bs.getContractNumber()!=null?bs.getContractNumber():""+" - "+
						bs.getUserID8()!=null?bs.getUserID8():""+" - "+
						(bo.getCuentaCargo()!=null?bo.getCuentaCargo():"")+" - "+
						imp+" -" + ES_PAGO_SUA_LINEA_CAPTURA ,EIGlobal.NivelLog.DEBUG);
				if(bo.getFolioSua()==null || bo.getFolioSua().length()==0){
					bo.setFolioSua("0");
				}
//				SUALineaCapturaAction action= new SUALineaCapturaAction();
//				String llam=action.bitacoraSUALC(obten_referencia_operacion
//						(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),
//						(bo.getCodErr()!=null?bo.getCodErr():""),
//						bs.getContractNumber()!=null?bs.getContractNumber():"",
//						bs.getUserID8()!=null?bs.getUserID8():"",
//						(bo.getCuentaCargo()!=null?bo.getCuentaCargo():""),
//						imp,ES_PAGO_SUA_LINEA_CAPTURA, "", 0, ""
//						,bo.getFolioSua());

	    	    BitaTCTBean beanTCT = new BitaTCTBean ();
	  			BitaHelper bh =	new BitaHelperImpl(req,bs,req.getSession(false));

	  			beanTCT = bh.llenarBeanTCT(beanTCT);
	  			beanTCT.setReferencia(obten_referencia_operacion
						(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
	  			beanTCT.setCodError(bo.getCodErr()!=null?bo.getCodErr():"");
	  			beanTCT.setUsuario(bs.getUserID8()!=null?bs.getUserID8():"");
	  			beanTCT.setCuentaOrigen(bo.getCuentaCargo()!=null?bo.getCuentaCargo():"");
	  			beanTCT.setImporte(imp);
				beanTCT.setTipoOperacion(ES_PAGO_SUA_LINEA_CAPTURA);
				beanTCT.setProteccion1(Long.parseLong(bo.getFolioSua()));
				beanTCT.setConcepto(bo.getLineaCap()!=null?bo.getLineaCap():"");//VAO 12-09
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);
			    EIGlobal.mensajePorTrace("Guarda LC: " + bo.getLineaCap() ,EIGlobal.NivelLog.DEBUG);


				//EIGlobal.mensajePorTrace("-Bitacora-:" + llam,EIGlobal.NivelLog.DEBUG);
			}else{
				EIGlobal.mensajePorTrace("No se guarda bitacora operacion erronea: " + bo.getCodErr() ,EIGlobal.NivelLog.DEBUG);
			}
		}
		}catch(SQLException sqle){
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
	}


	/**
	 * Guarda los valores actuales en la sesion
	 * @param request recibe el request
	 */
	public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n SIPARE Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",request.getServletPath());

        HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();
			String value;

			value = (String) getFormParameter(request,"valida");
			if(value != null && !value.equals("null")) {
				tmp.put("valida",getFormParameter(request,"valida"));
			}

			value = (String) request.getParameter(CTA_CARGO);
			if(value != null && !value.equals("null")) {
            	tmp.put(CTA_CARGO,request.getParameter(CTA_CARGO));
			}

			value = (String) request.getParameter("titular");
			if(value != null && !value.equals("null")) {
            	tmp.put("titular",request.getParameter("titular"));
			}

			value = (String) request.getParameter("fecha");
			if(value != null && !value.equals("null")) {
            	tmp.put("fecha",request.getParameter("fecha"));
			}

			value = (String) request.getParameter("importe");
			if(value != null && !value.equals("null")) {
            	tmp.put("importe", request.getParameter("importe"));
			}

			value = (String) request.getParameter("lc");
			if(value != null && !value.equals("null")) {
            	tmp.put("lc", request.getParameter("lc"));
			}

			value = (String) request.getParameter("cuentaNombre");
			if(value != null && !value.equals("null")) {
            	tmp.put("cuentaNombre",request.getParameter("cuentaNombre"));
			}
			tmp.put("opcionLC", "4");

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
		EIGlobal.mensajePorTrace("\n\n\n JGAL Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
    }
}