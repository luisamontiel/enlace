/** Banco Santander Mexicano
 *	@author Gerardo Salazar Vega
 *	@version 1.0
 *	fecha de creacion: 15 de Noviembre del 2000
 *	@return SUCCESS resultado de la ejecucion del applogic
 * 	descripcion: Applogic que realiza las operaciones
 *	alta: guarda un registro nuevo en el archivo
 *	baja: marca un registro para baja logica en el archivo
 *	modificacion: escribe un registro que ha sido modificado
 *	lee el contenido de un archivo y lo muestra en pantalla
 *	llamando al template de mantenimiento de empleados
 */


package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.text.*;
import javax.naming.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.ArchivosMantEmpl;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.archivoEmpleados;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class NomEmpExeOpciones extends BaseServlet {

	String textoLog ="";
	private String textoLog(String funcion){
		return textoLog = NomEmpExeOpciones.class.getName() + "." + funcion + "::";
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void defaultAction(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("defaultAction");
		boolean sesionvalida = SesionValida(request, response);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("Fecha", ObtenFecha());
		request.setAttribute("ContUser", ObtenContUser(request));

		EIGlobal.mensajePorTrace(textoLog + "Inicia funcion", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(textoLog + "operacion = " + request.getParameter("operacion"), EIGlobal.NivelLog.INFO);

		if (sesionvalida) {// servidor para faciultad a Mancomunidad

			EIGlobal.mensajePorTrace(textoLog + "Antes de obtener archivo name", EIGlobal.NivelLog.INFO);
			request.setAttribute("max_registros", "" + Global.MAX_REGISTROS);
			request.setAttribute("botLimpiar", "");

			if (request.getParameter("operacion").equals("baja")) {
				String nombreArchivoEmp = session.getArchivoEmpNom();
				String ligaExport = nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/"));
				request.setAttribute("lista_numeros_empleado", request.getParameter("lista_numeros_empleado"));
				request.setAttribute("tipo_archivo", request.getParameter("tipo_archivo"));
				ejecutaBajas(request, response);
			}
			if (request.getParameter("operacion").equals("alta")) {
				String nombreArchivoEmp = session.getArchivoEmpNom();
				String ligaExport = nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/"));
				request.setAttribute("lista_numeros_empleado", request.getParameter("lista_numeros_empleado"));
				request.setAttribute("tipo_archivo", request.getParameter("tipo_archivo"));
				request.setAttribute("Encabezado", CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Empleados", "s25740h", request));
				request.setAttribute("botLimpiar", "<td align=left valign=top width=76><A HREF=javascript:js_limpiar();><img border=0 name=boton src=/gifs/EnlaceMig/gbo25250.gif width=76 height=22 alt=Limpiar prueba></a></td>");
				ejecutaAltas(request, response);
			}
			if (request.getParameter("operacion").equals("modificacion")) {
				String nombreArchivoEmp = session.getArchivoEmpNom();
				String ligaExport = nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/"));
				request.setAttribute("lista_numeros_empleado", request.getParameter("lista_numeros_empleado"));
				request.setAttribute("tipo_archivo", request.getParameter("tipo_archivo"));
				ejecutaModificacion(request, response);
			}
			if (request.getParameter("operacion").equals("obtensucursal")) {
				 //Obten el email que envia el usuario y mandalo a Tuxedo para obtencion de lista de sucursales.
				EIGlobal.mensajePorTrace(textoLog + "Antes de llamar Sucursales", EIGlobal.NivelLog.INFO);
				ejecutaObtenSucursal(request, response, request.getParameter("texto"));
			}
			if (request.getParameter("operacion").equals("enviaEmpDetalle")) {
				//Obten el email que envia el usuario y mandalo a Tuxedo para obtencion de los detalles seleccionados por el usuario.
				ejecutaObtenAltaDetalle(request, response, request.getParameter("texto"));
			}
			if (request.getParameter("operacion").equals("consulta")) {
				consultaArchivos(request, response);
			}
			if (request.getParameter("operacion").equals("regresar")) {
				inicioConsulta(request, response);
			}
			if (request.getParameter("operacion").equals("cancelar")) {
				ejecutaCancelacion(request, response);
			}
		}
		else {
			EIGlobal.mensajePorTrace(textoLog + "Antes de Mensaje de Error", EIGlobal.NivelLog.INFO);
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	/**
	 * Inicializar las variables de session en caso de seleccionar la opcion de
	 * consulta de Empleados Comienza codigo Sinapsis
	 */
	protected void inicioConsulta(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, java.io.IOException {
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer datesrvr = new StringBuffer("");
		datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvr.append(ObtenAnio());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvr.append(ObtenMes());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvr.append(ObtenDia());
		datesrvr.append("\">");
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Bitfechas", datesrvr.toString());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta de Proceso de Alta", "Servicios &gt; N&oacute;mina &gt;Consultas &gt;Alta Empleados ", "s25800conh", request));

		if (sess.getAttribute("facultadesN") != null)
			request.setAttribute("facultades", sess.getAttribute("facultadesN"));

		request.setAttribute("textcuenta", "");
		request.setAttribute("ContenidoArchivo", "");
		sess.setAttribute("nombre_arch_salida_consul", "");

		try {
			evalTemplate(IEnlace.NOM_MTTO_CSL_TMPL, request, response);
		} catch (Exception e) {
		}
	}

	/**
	 * Esta rutina obtiene los registros seleccionados por el usuario los cuales
	 * son reenviados al usuario con todas la informacion del usuario.
	 */
	public void ejecutaObtenAltaDetalle(HttpServletRequest request, HttpServletResponse response, String email) {
		textoLog("ejecutaObtenAltaDetalle");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String contrato = session.getContractNumber();
		String empleado = session.getUserID8();
		String perfil = session.getUserProfile();
		String valores_cancelar = (String) request.getParameter("valor_radio");
		EIGlobal.mensajePorTrace(textoLog + "valores_cancelar [" + valores_cancelar + "]", EIGlobal.NivelLog.INFO);
		valores_cancelar = valores_cancelar.trim();
		String archivo = valores_cancelar.substring(0, valores_cancelar.indexOf("@"));
		EIGlobal.mensajePorTrace(textoLog + "archivo = " + archivo, EIGlobal.NivelLog.INFO);
		String secuencia = valores_cancelar.substring(valores_cancelar.indexOf("@") + 1, valores_cancelar.lastIndexOf("@"));
		EIGlobal.mensajePorTrace(textoLog + "secuencia = " + secuencia, EIGlobal.NivelLog.INFO);
		String lista_nombres_archivos = (String) request.getParameter("lista_nombres_archivos");
		String arch_salida_consulta = (String) request.getParameter("nombre_arch_salida");
		String correo = email;
		request.setAttribute("Fechas", ObtenFecha(true));
		request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");

		if (contrato.equals("") || contrato == null)
			contrato = " ";
		if (archivo.equals(""))
			archivo = " ";

		ServicioTux tuxGlobal = new ServicioTux();

		Hashtable htResult = null;
		String tramaEntrada = "";
		String direccionCorreo = "";

		if (correo.indexOf("@") > 0)
			direccionCorreo = correo.substring(0, correo.indexOf("@")) + "[]" + correo.substring(correo.indexOf("@") + 1);

		EIGlobal.mensajePorTrace(textoLog + "direccionCorreo = [" + direccionCorreo.trim() + "]", EIGlobal.NivelLog.INFO);

		tramaEntrada = "1EWEB|" + empleado + "|NOA2|" + contrato + "|"
				+ empleado + "|" + perfil + "|" + contrato + "@" + secuencia
				+ "@" + direccionCorreo + "@";

		EIGlobal.mensajePorTrace(textoLog + "tramaEntrada NOA2 [" + tramaEntrada + "]", EIGlobal.NivelLog.INFO);

		try {
			htResult = tuxGlobal.web_red(tramaEntrada);
		} catch (java.rmi.RemoteException re) {
			EIGlobal.mensajePorTrace(textoLog + "ERROR "+re.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Excepci�n al ejecutar tuxGlobal.web_red", EIGlobal.NivelLog.INFO);
		}

		String tramaSalida = (String) htResult.get("BUFFER");
		EIGlobal.mensajePorTrace(textoLog + "tramaSalida [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);

		String estatus_transacion = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
		String mensaje_transacion = tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@', 2));
		request.setAttribute("MECorreo", "Empleados");
		request.setAttribute("DetalleEmpNotify", mensaje_transacion);
		try {
			evalTemplate(IEnlace.NOM_EMP_SUC_TMPL, request, response);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Excepci�n al ejecutar evalTemplate", EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Esta rutina obtiene las sucursales regresa un archivo generado en TUXEDO
	 * el cual es reenviado al usuario con todas las sucursales que tiene el
	 * banco. Los campos enviados son el ID y Nombre de la sucursal.
	 */
	public void ejecutaObtenSucursal(HttpServletRequest request, HttpServletResponse response, String email) {
		textoLog("ejecutaObtenSucursal");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String usuario = session.getUserID8(), contrato = session.getContractNumber(), clavePerfil = session.getUserProfile();
		String direccionCorreo = "";

		if (email.indexOf("@") > 0)
			direccionCorreo = email.substring(0, email.indexOf("@")) + "[]"
					+ email.substring(email.indexOf("@") + 1);

		String trama_entrada = "1EWEB|" + usuario + "|NOSC|" + contrato + "|"
				+ usuario + "|" + clavePerfil + "|" + contrato + "@"
				+ direccionCorreo + "@";

		String CodError = "";
		Hashtable htResult = null;
		ServicioTux tuxGlobal = new ServicioTux();

		try {
			htResult = tuxGlobal.web_red(trama_entrada);
		} catch (java.rmi.RemoteException re) {
			EIGlobal.mensajePorTrace(textoLog + "ERROR "+re.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Excepci�n al ejecutar tuxGlobal.web_red", EIGlobal.NivelLog.INFO);
		}
		CodError = (String) htResult.get("BUFFER");

		EIGlobal.mensajePorTrace(textoLog + "cod error = " + CodError, EIGlobal.NivelLog.INFO);

		String estatus_transacion = CodError.substring(0, posCar(CodError, '@', 1));
		String mensaje_transacion = CodError.substring((posCar(CodError, '@', 1)) + 1, posCar(CodError, '@', 2));
		request.setAttribute("MECorreo", "Sucursal");
		request.setAttribute("SucursalNotify", mensaje_transacion);
		try {
			evalTemplate(IEnlace.NOM_EMP_SUC_TMPL, request, response);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Excepci�n al ejecutar evalTemplate", EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * El metodo ejecutaModificacion(mapa) se utiliza para realizar la
	 * modificacion del registro seleccionado
	 */
	public String completaCamposNumericos(int longitudActual, int longitudRequerida) {
		String ceros = "";
		int faltantes = longitudRequerida - longitudActual;
		for (int i = 0; i < faltantes; i++) {
			ceros = ceros + "0";
		}
		return ceros;
	}

	public void ejecutaModificacion(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("ejecutaModificacion");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String contenidoArchivoStr = "";
		String NumeroEmpleado = request.getParameter("NumeroEmpleado");
		String NumeroDepto = request.getParameter("NumeroDepto");
		String ApellidoPaterno = request.getParameter("ApellidoPaterno");
		String ApellidoMaterno = request.getParameter("ApellidoMaterno");
		String Nombre = request.getParameter("Nombre");
		String nacionalidad = request.getParameter("nacionalidad");
		String RFC = request.getParameter("RFC");
		String Homoclave = request.getParameter("Homoclave");
		String sexo = request.getParameter("sexo");
		String EstadoCivil = request.getParameter("EstadoCivil");
		String NombreCorto = request.getParameter("NombreCorto");
		String Domicilio = request.getParameter("Domicilio");
		String Colonia = request.getParameter("Colonia");
		String DelegacionoMpio = request.getParameter("DelegacionoMpio");
		String CiudadoPoblacion = request.getParameter("CiudadoPoblacion");
		String CodigoPostal = request.getParameter("CodigoPostal");
		String pais = request.getParameter("pais");
		String EstatusCasa = request.getParameter("EstatusCasa");
		String FechaResidencia = request.getParameter("FechaResidencia");
		String ClaveEnvio = "0";
		String DomicilioOfna = request.getParameter("DomicilioOfna");
		String ColoniaOficina = request.getParameter("ColoniaOficina");
		String DeloMpioOficina = request.getParameter("DeloMpioOficina");
		String CdoPobOficina = request.getParameter("CdoPobOficina");
		String CodigoPostalOfna = request.getParameter("CodigoPostalOfna");
		String PaisOficina = request.getParameter("PaisOficina");
		String PrefijoTelefono = request.getParameter("PrefijoTelefono");
		String Telefono = request.getParameter("Telefono");
		String Extension = request.getParameter("Extension");
		String IngresoMensual = request.getParameter("IngresoMensual");

		IngresoMensual = IngresoMensual.trim();
		if (IngresoMensual.indexOf(".") > 0) {
			String entero = IngresoMensual.substring(0, IngresoMensual.indexOf("."));
			String decimales = IngresoMensual.substring(IngresoMensual.indexOf(".") + 1, IngresoMensual.length());
			IngresoMensual = entero + decimales + completaCamposNumericos(decimales.length(), 2);
		} else {
			IngresoMensual = IngresoMensual + "00";
		}

		String NumeroCuenta = request.getParameter("NumeroCuenta");
		String NumeroTarjeta = request.getParameter("NumeroTarjeta");
		String FechaIngreso = request.getParameter("FechaIngreso");
		String PrefijoParTelefono = request.getParameter("PrefijoParTelefono");
		String ParTelefono = request.getParameter("ParTelefono");

		/**
		 * Ejcutar sobre la sucursal del empleado la siguiente regla: 1.
		 * Verificar si el usuario Introdujo una sucursal en el campo
		 * AMESucursal el cual es opcional. a) En caso de TRUE el campo sucursal
		 * es escrito al archivo b) En caso de FALSE Se obtiene el campo entrado
		 * para la variable de session Sucursal, si este es vacio se insertan 4
		 * guiones (----) los cuales posteriormente serviran de marca para
		 * cuando se envie el archivo. El archivo sera escaneado por esta marca
		 * y se tendra que remplazar ya sea por la variable de session Sucursal
		 * o Default (Definida posteriormente).
		 */
		String Sucursal = request.getParameter("AMESucursal");

		//El usuario no entro en una sucursal
		if (Sucursal == null || Sucursal.length() <= 0 || Sucursal.equals("")) {
			// Obtener sucursal Entrada a la variable de Session Sucursal
			Sucursal = request.getParameter("Sucursal");
			// Si no sucursal entrada definir sucursal default
			if (Sucursal == null || Sucursal.length() <= 0 || Sucursal.equals(""))
				Sucursal = "----";// Este metodo es definido posteriormente
		}

		String cadena = NumeroEmpleado + NumeroDepto + ApellidoPaterno +
						ApellidoMaterno + Nombre + RFC + Homoclave + sexo +
						nacionalidad + EstadoCivil + NombreCorto + Domicilio +
						Colonia + DelegacionoMpio + CiudadoPoblacion +
						CodigoPostal + pais + EstatusCasa + FechaResidencia +
						PrefijoParTelefono + ParTelefono + FechaIngreso +
						ClaveEnvio + DomicilioOfna + ColoniaOficina +
						DeloMpioOficina + CdoPobOficina + CodigoPostalOfna +
						PaisOficina + PrefijoTelefono + Telefono +
						Extension +
						completaCamposNumericos(IngresoMensual.length(), 18) +
						IngresoMensual + NumeroCuenta + NumeroTarjeta +
						Sucursal;

		// Obtiene la posicion del registro a modificar
		String strRegistro = request.getParameter("registro");
		long longRegistro = 0;

		try {
			longRegistro = Long.parseLong(strRegistro);
		}
		catch (NumberFormatException e) {
			EIGlobal.mensajePorTrace(textoLog + "Error en el formato", EIGlobal.NivelLog.INFO);
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo realizar la modificaci&oacute;n\",1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}

		String nombreArchivoEmp = session.getArchivoEmpNom();
		EIGlobal.mensajePorTrace(textoLog + "session.getArchivoEmpNom = " + nombreArchivoEmp, EIGlobal.NivelLog.INFO);
		archivoEmpleados archivo4 = new archivoEmpleados(nombreArchivoEmp, session.getClaveBanco());
		archivo4.modificaRegistro(longRegistro, cadena);
		archivo4.close();
		archivoEmpleados archivo5 = new archivoEmpleados(nombreArchivoEmp, session.getClaveBanco());
		request.setAttribute("total_registros", "" + archivo5.tamanioArchivo());
		contenidoArchivoStr = "" + archivo5.lecturaArchivo();
		request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
		request.setAttribute("ContenidoArchivo", "" + contenidoArchivoStr);
		request.setAttribute("facultades", sess.getAttribute("facultadesE"));
		request.setAttribute("lista_numeros_empleado", archivo5.lista_empleados);
		evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
	}

	/**
	 * El metodo ejecutaBajas(mapa) se utiliza para realizar la baja del
	 * registro seleccionado
	 */
	public void ejecutaBajas(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("ejecutaBajas");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String contenidoArchivoStr = "";
		// Obtiene la posicion del registro a modificar
		String strRegistro = request.getParameter("registro");
		long longRegistro = 0;

		try {
			longRegistro = Long.parseLong(strRegistro);
		}
		catch (NumberFormatException e) {
			EIGlobal.mensajePorTrace(textoLog + "Error en el formato", EIGlobal.NivelLog.INFO);
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo realizar la baja\",1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}

		String nombreArchivoEmp = session.getArchivoEmpNom();
		EIGlobal.mensajePorTrace(textoLog + "session.getArchivoEmpNom = " + nombreArchivoEmp, EIGlobal.NivelLog.INFO);
		archivoEmpleados archivo3 = new archivoEmpleados(nombreArchivoEmp, session.getClaveBanco());
		// Ejecuta la baja del registro
		archivo3.bajaRegistro(longRegistro);
		// Realiza la lectura del archivo para mostrarlo en pantalla
		request.setAttribute("total_registros", "" + archivo3.tamanioArchivo());
		contenidoArchivoStr = "" + archivo3.lecturaArchivo();
		request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
		request.setAttribute("ContenidoArchivo", "" + contenidoArchivoStr);
		request.setAttribute("facultades", sess.getAttribute("facultadesE"));
		request.setAttribute("lista_numeros_empleado", archivo3.lista_empleados);
		evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
	}

	/**
	 * M�todo utilizado para dar de baja los empleados que ya han sido
	 * cancelados.
	 * @param request
	 * @param response
	 */
	private ArrayList ejecutaBajaEmpl(String contrato, String cuentas){
		textoLog("ejecutaBajaEmpl");
		StringTokenizer		st					= new StringTokenizer(cuentas, "|");
		String				cuentaBaja			= "";
		String				queryBaja			= "";
		String				queryConsulta		= "";
		Connection			conn				= null;
		ResultSet			rsConsultaEmpleado	= null;
		PreparedStatement 	psBajaEmpleado		= null;
		PreparedStatement 	psConsultaEmpleado	= null;
		int 				totalBajas			= 0;//Registro a dar de baja
		int 				totalBajas2			= 0;//Registro ya dado de baja con anterioridad
		ArrayList			result				= new ArrayList();

		EIGlobal.mensajePorTrace(textoLog + "Inicia Funcion.",EIGlobal.NivelLog.INFO);

		try{
			conn = createiASConn_static2(Global.DATASOURCE_ORACLE2);
			if(conn == null){
				EIGlobal.mensajePorTrace(textoLog + "Error al intentar crear la conexion.",EIGlobal.NivelLog.INFO);
				return result;
			}
			EIGlobal.mensajePorTrace(textoLog + "Conexion creada, se inhabilita autocommit.",EIGlobal.NivelLog.INFO);
			conn.setAutoCommit(false);
			queryBaja = "update eweb_det_cat_nom set estatus_cuenta = 'I' where contrato = rpad(?, 16) and cuenta_abono = rpad(?, 18) and estatus_cuenta <> 'I'";
			queryConsulta = "select count(1) from eweb_det_cat_nom where contrato = rpad(?, 16) and cuenta_abono = rpad(?, 18) and estatus_cuenta = 'I'";
			EIGlobal.mensajePorTrace(textoLog + "Se crea el PreparedStatement.",EIGlobal.NivelLog.INFO);
			psBajaEmpleado = conn.prepareStatement(queryBaja);
			psConsultaEmpleado = conn.prepareStatement(queryConsulta);


			if(psBajaEmpleado == null) {
				EIGlobal.mensajePorTrace(textoLog + "Problemas al crear PreparedStatement.",EIGlobal.NivelLog.INFO);
				return result;
			} else {
				EIGlobal.mensajePorTrace(textoLog + "PreparedStatement creado, entra a ciclo para ejecutar queries.",EIGlobal.NivelLog.INFO);
				while (st.hasMoreTokens()) {
					//EIGlobal.mensajePorTrace(textoLog + "Comienza iteraci�n",EIGlobal.NivelLog.INFO);
					cuentaBaja = st.nextToken();

					//EIGlobal.mensajePorTrace(textoLog + "contrato [" + contrato + "] cuenta = [" + cuentaBaja + "]",EIGlobal.NivelLog.INFO);

					psConsultaEmpleado.setString(1, contrato);
					psConsultaEmpleado.setString(2, cuentaBaja);
					rsConsultaEmpleado = psConsultaEmpleado.executeQuery();

					//EIGlobal.mensajePorTrace(textoLog + "Despu�s de ejecutar query",EIGlobal.NivelLog.INFO);
					if (rsConsultaEmpleado != null){
						//EIGlobal.mensajePorTrace(textoLog + "Validando",EIGlobal.NivelLog.INFO);
						rsConsultaEmpleado.next();
						totalBajas2 += rsConsultaEmpleado.getInt(1);
						//EIGlobal.mensajePorTrace(textoLog + "Termina Validaci�n",EIGlobal.NivelLog.INFO);
					}
					//EIGlobal.mensajePorTrace(textoLog + "Despu�s de validar",EIGlobal.NivelLog.INFO);

					psBajaEmpleado.setString(1, contrato);
					psBajaEmpleado.setString(2, cuentaBaja);
					totalBajas += psBajaEmpleado.executeUpdate();

					//EIGlobal.mensajePorTrace(textoLog + "totalBajas = " + totalBajas + " totalBajas2 = " + totalBajas2,EIGlobal.NivelLog.INFO);
				}
				EIGlobal.mensajePorTrace(textoLog + "Termina Ciclo",EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace(textoLog + "totalBajas = " + totalBajas + " totalBajas2 = " + totalBajas2,EIGlobal.NivelLog.INFO);

			result.add("" + totalBajas);
			result.add("" + totalBajas2);

			EIGlobal.mensajePorTrace(textoLog + "Se ejececuta commit de operaciones.",EIGlobal.NivelLog.INFO);
			conn.commit();
		}catch( SQLException sqle ){
			try {
				conn.rollback();
			}catch(SQLException sqle2){
				EIGlobal.mensajePorTrace(textoLog + "Se ha presentado un error de sql al realizar rollback", EIGlobal.NivelLog.INFO);
			}
			EIGlobal.mensajePorTrace(textoLog + "Se ha presentado un error de sql", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "ERROR "+sqle.getMessage(), EIGlobal.NivelLog.ERROR);
		}catch( Exception e ){
			EIGlobal.mensajePorTrace(textoLog + "ERROR "+e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		finally{
			try {
				EIGlobal.mensajePorTrace(textoLog + "cierra conexion y PreparedStatement",EIGlobal.NivelLog.INFO);
				psBajaEmpleado.close();
				conn.close();
			}catch (Exception e){
				EIGlobal.mensajePorTrace(textoLog + "Problemas al cerrar Statement y Connection.", EIGlobal.NivelLog.INFO);
			}
			EIGlobal.mensajePorTrace(textoLog + "Se han actualizado las cuentas.",EIGlobal.NivelLog.INFO);
			return result;
		}
	}

	/**
	 * El metodo ejecutaAltas(mapa) se utiliza para realizar el alta de un
	 * registro nuevo
	 */
	public void ejecutaAltas(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("ejecutaAltas");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String contenidoArchivoStr = "";
		String NumeroEmpleado = request.getParameter("NumeroEmpleado");
		String NumeroDepto = request.getParameter("NumeroDepto");
		String ApellidoPaterno = request.getParameter("ApellidoPaterno");
		String ApellidoMaterno = request.getParameter("ApellidoMaterno");
		String Nombre = request.getParameter("Nombre");
		String nacionalidad = request.getParameter("nacionalidad");
		String RFC = request.getParameter("RFC");
		String Homoclave = request.getParameter("Homoclave");
		String sexo = request.getParameter("sexo");
		String EstadoCivil = request.getParameter("EstadoCivil");
		String NombreCorto = request.getParameter("NombreCorto");
		String Domicilio = request.getParameter("Domicilio");
		String Colonia = request.getParameter("Colonia");
		String DelegacionoMpio = request.getParameter("DelegacionoMpio");
		String CiudadoPoblacion = request.getParameter("CiudadoPoblacion");
		String CodigoPostal = request.getParameter("CodigoPostal");
		String pais = request.getParameter("pais");
		String EstatusCasa = request.getParameter("EstatusCasa");
		String FechaResidencia = request.getParameter("FechaResidencia");
		// String ClaveEnvio = request.getParameter("ClaveEnvio");
		String ClaveEnvio = "0";
		String DomicilioOfna = request.getParameter("DomicilioOfna");
		String ColoniaOficina = request.getParameter("ColoniaOficina");
		String DeloMpioOficina = request.getParameter("DeloMpioOficina");
		String CdoPobOficina = request.getParameter("CdoPobOficina");
		String CodigoPostalOfna = request.getParameter("CodigoPostalOfna");
		String PaisOficina = request.getParameter("PaisOficina");
		String PrefijoTelefono = request.getParameter("PrefijoTelefono");
		String Telefono = request.getParameter("Telefono");
		String Extension = request.getParameter("Extension");
		String IngresoMensual = request.getParameter("IngresoMensual");
		EIGlobal.mensajePorTrace(textoLog + "Ingreso Mensual = " + IngresoMensual, EIGlobal.NivelLog.INFO);

		IngresoMensual = IngresoMensual.trim();
		if (IngresoMensual.indexOf(".") > 0) {
			String entero = IngresoMensual.substring(0, IngresoMensual.indexOf("."));
			String decimales = IngresoMensual.substring(IngresoMensual.indexOf(".") + 1, IngresoMensual.length());
			IngresoMensual = entero + decimales + completaCamposNumericos(decimales.length(), 2);
		} else {
			IngresoMensual = IngresoMensual + "00";
		}

		EIGlobal.mensajePorTrace(textoLog + "Ingreso Mensual 2 = " + IngresoMensual, EIGlobal.NivelLog.INFO);
		String NumeroCuenta = request.getParameter("NumeroCuenta");
		String NumeroTarjeta = request.getParameter("NumeroTarjeta");
		String FechaIngreso = request.getParameter("FechaIngreso");
		String PrefijoParTelefono = request.getParameter("PrefijoParTelefono");
		String ParTelefono = request.getParameter("ParTelefono");
		String nombreArchivoEmp = session.getArchivoEmpNom();
		EIGlobal.mensajePorTrace(textoLog + "session.getArchivoEmpNom=" + nombreArchivoEmp, EIGlobal.NivelLog.INFO);
		archivoEmpleados archivo4 = new archivoEmpleados(nombreArchivoEmp, session.getClaveBanco());
		request.setAttribute("operacion", request.getParameter("operacion"));
		request.setAttribute("botonOperacion", "<img border=\"0\" name=\"boton\" src=\"/gifs/EnlaceMig/gbo25480.gif\" width=\"66\" height=\"22\" alt=\"Alta\">");

		int secuencia = 0;
		String cSecuencia = "";
		// Obtiene el numero de secuencia para el registro a insertar
		secuencia = archivo4.secuenciaArchivo();

		if (secuencia != -1) {
			/**
			 * Ejcutar sobre la sucursal del empleado la siguiente regla: 1.
			 * Verificar si el usuario Introdujo una sucursal en el campo
			 * AMESucursal el cual es opcional. a) En caso de TRUE el campo
			 * sucursal es escrito al archivo b) En caso de FALSE Se obtiene el
			 * campo entrado para la variable de session Sucursal, si este es
			 * vacio se insertan 4 guiones (----) los cuales posteriormente
			 * serviran de marca para cuando se envie el archivo. El archivo
			 * sera escaneado por esta marca y se tendra que remplazar ya sea
			 * por la variable de session Sucursal o Default (Definida
			 * posteriormente).
			 */
			String Sucursal = request.getParameter("AMESucursal");
			// El usuario no entro una sucursal
			if (Sucursal == null || Sucursal.length() <= 0 || Sucursal.equals("")) {
				// Obtener sucursal Entrada a la variable de Session Sucursal
				Sucursal = request.getParameter("Sucursal");
				// Si no sucursal entrada definir sucursal default
				if (Sucursal == null || Sucursal.length() <= 0 || Sucursal.equals(""))
					Sucursal = "----";
			}
			// Arma la cadena que se va a insertar como registro nuevo

			cSecuencia = archivo4.rellenaCaracter("" + secuencia, '0', 5, true);
			String cadena = "2" + cSecuencia + NumeroEmpleado + NumeroDepto +
				ApellidoPaterno + ApellidoMaterno + Nombre + RFC + Homoclave +
				sexo + nacionalidad + EstadoCivil + NombreCorto + Domicilio +
				Colonia + DelegacionoMpio + CiudadoPoblacion + CodigoPostal +
				pais + EstatusCasa + FechaResidencia + PrefijoParTelefono +
				ParTelefono + FechaIngreso + ClaveEnvio + DomicilioOfna +
				ColoniaOficina + DeloMpioOficina + CdoPobOficina +
				CodigoPostalOfna + PaisOficina + PrefijoTelefono + Telefono +
				Extension +
				completaCamposNumericos(IngresoMensual.length(), 18) +
				IngresoMensual + NumeroCuenta + NumeroTarjeta + Sucursal;

			EIGlobal.mensajePorTrace(textoLog + "cadena = " + cadena, EIGlobal.NivelLog.INFO);

			// Realiza el alta del registro y cierra el archivo
			archivo4.altaRegistro(cadena);
			request.setAttribute("total_registros", "" + archivo4.tamanioArchivo());
			archivo4.close();
		}
		else {
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo realizar la alta\",1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}
		evalTemplate(IEnlace.NOM_EMP_DATOS_TMPL, request, response);
	}

	/**
	 * Utilizado para realizar la cancelaci�n del registro seleccionado.
	 */
	public void ejecutaCancelacion(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("ejecutaCancelacion");
		HttpSession		sess					= request.getSession();
		BaseResource	session					= (BaseResource) sess.getAttribute("session");
		String			contrato				= session.getContractNumber();
		String			empleado				= session.getUserID8();
		String			perfil					= session.getUserProfile();
		String			valor_radio				= (String) request.getParameter("valor_radio");
		if(valor_radio != null) {
			valor_radio				= valor_radio.trim();
		}
		else {
			valor_radio="";
		}
		String			archivo					= valor_radio.substring(0, valor_radio.indexOf("@"));
		String			secuencia				= valor_radio.substring(valor_radio.indexOf("@") + 1, valor_radio.lastIndexOf("@"));
		String			lista_nombres_archivos	= (String) request.getParameter("lista_nombres_archivos");
		String			arch_salida_consulta	= (String) request.getParameter("nombre_arch_salida");

		EIGlobal.mensajePorTrace(textoLog + "valor_radio = [" + valor_radio + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(textoLog + "archivo = [" + archivo + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(textoLog + "secuencia = [" + secuencia + "]", EIGlobal.NivelLog.INFO);

		if (contrato.equals("") || contrato == null)
			contrato = " ";
		if (archivo.equals(""))
			archivo = " ";

		ServicioTux tuxGlobal = new ServicioTux();

		Hashtable htResult		= null;
		String tramaEntrada		= "";
		String elMensaje		= "";
		String estatus_envio	= "";

		tramaEntrada = contrato + "@" + secuencia + "@";
		EIGlobal.mensajePorTrace(textoLog + "tramaEntrada = [" + tramaEntrada + "]", EIGlobal.NivelLog.INFO);

		try {
			htResult = tuxGlobal.canc_empl(tramaEntrada);
		} catch (java.rmi.RemoteException re) {
			EIGlobal.mensajePorTrace(textoLog + "ERROR "+re.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Excepci�n tuxGlobal.canc_empl", EIGlobal.NivelLog.INFO);
		}

		String tramaSalida = (String) htResult.get("BUFFER");
		EIGlobal.mensajePorTrace(textoLog + "tramaSalida [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);

		if (tramaSalida.indexOf("INOM") >= 0) {
			elMensaje		= tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@', 2));
			estatus_envio	= tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
			int codError	= Integer.parseInt(estatus_envio.substring(4));

			EIGlobal.mensajePorTrace(textoLog + "elMensaje : [" + elMensaje + "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(textoLog + "estatus_envio : [" + estatus_envio + "]", EIGlobal.NivelLog.INFO);

			switch (codError){
				case 1001:
					elMensaje = "El alta de empleados fue cancelada.";
					break;
				case 1002:
					elMensaje = "El proceso de generaci&oacute;n de cuentas ha comenzado, pero dichas cuentas no ser&aacute;n reflejadas en el cat&aacute;logo de empleados.";
					break;
				case 1003:
					/*ArrayList result = null;
					result = ejecutaBajaEmpl(contrato, elMensaje);

					EIGlobal.mensajePorTrace(textoLog + "obtiene datos de resultado", EIGlobal.NivelLog.INFO);
					int totalBajas	= Integer.parseInt(result.get(0).toString());
					int totalBajas2	= Integer.parseInt(result.get(1).toString());
					EIGlobal.mensajePorTrace(textoLog + "datos de resultado obtenidos", EIGlobal.NivelLog.INFO);
					if (totalBajas == 0){
						elMensaje = "La operaci&oacute;n no di&oacute; de baja registros.";
					}else if(totalBajas == 1){
						elMensaje = "Se ha ejecutado la baja de una cuenta de la transmisi&oacute;n.";
					}else if(totalBajas > 1){
						elMensaje = "Se ha ejecutado la operaci&oacute; con un total de: " + (totalBajas+totalBajas2) + " de los cuales " + totalBajas + " registros se dieron de baja y " + totalBajas2 + " se hab&iacute;an dado de baja anteriormente.";
					}*/
					elMensaje = "El estatus del registro no es v&aacute;lido para la operaci&oacute;n";
					break;
				case 1004:
					elMensaje = "La trasnmisi&oacute;n que Usted seleccion&oacute; ya se encuentra cancelada o rechazada.";
					break;
				case 1005:
					elMensaje = "Se ha presentado un error en el servidor, Favor de realizar su operaci&oacute;n m&aacute;s tarde.";
					break;
				default:
					elMensaje = "No se pudo obtener respuesta del servidor.";
			}
		}
		else{
			elMensaje = "No se pudo obtener respuesta del servidor.";
			EIGlobal.mensajePorTrace(textoLog + "estatus_envio : [" + estatus_envio + "]", EIGlobal.NivelLog.INFO);
		}

		EIGlobal.mensajePorTrace(textoLog + "Comienza a asignar atributos", EIGlobal.NivelLog.INFO);

		if (sess.getAttribute("facultadesN") != null)
			request.setAttribute("facultades", sess.getAttribute("facultadesN"));

		StringBuffer datesrvr = new StringBuffer("");
		datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvr.append(ObtenAnio());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvr.append(ObtenMes());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvr.append(ObtenDia());
		datesrvr.append("\">");
		request.setAttribute("Bitfechas", datesrvr.toString());

		EIGlobal.mensajePorTrace(textoLog + "Despues del StringBuffer", EIGlobal.NivelLog.INFO);

		request.setAttribute("infoUser", "cuadroDialogo (\"" + elMensaje + "\",1)");
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta de Proceso de Alta", "Servicios &gt; N&oacute;mina &gt;Consultas &gt;Alta Empleados ", "s25800canh", request));
		request.setAttribute("textcuenta", "");
		request.setAttribute("ContenidoArchivo", "");
		request.setAttribute("Fechas", ObtenFecha(true));
		request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");

		sess.removeAttribute("nombre_arch_salida_consul");

		EIGlobal.mensajePorTrace(textoLog + "Termina de asignar atributos", EIGlobal.NivelLog.INFO);
		try {
			//evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
			evalTemplate(IEnlace.NOM_MTTO_CSL_TMPL, request, response);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + "Excepci�n evalTemplate", EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Ejecutar la consulta generica de Alta de Empleados Basicamente se le
	 * devuelve al cliente un resumen del query enviado al servicio TUXEDO NOA1
	 * y retrae los siguientes campos despues de que TUXEDO sobre escribe el
	 * archivo enviado con los registros resultante de la consulta. Linea 1
	 * ----> Registro de Control; Linea 2..n ---->
	 * fecha;secuencia;CantidadDeRegistrosEnviados;CantidadDeRegistrosAceptados;CantidadDeRegistrosRechazados;Estatus;
	 *
	 * Nota: Este archivo es de texto semicolon separado.
	 */
	protected void consultaArchivos(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, java.io.IOException {

		textoLog("consultaArchivos");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer datesrvr = new StringBuffer("");
		datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvr.append(ObtenAnio());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvr.append(ObtenMes());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvr.append(ObtenDia());
		datesrvr.append("\">");
		String nombre_arch_salida_consul = null;
		String nombre_arch_salida;

		EIGlobal.mensajePorTrace(textoLog + "nombre_arch_salida_consul = " + nombre_arch_salida_consul, EIGlobal.NivelLog.INFO);

		//Si es la primer p�gina que se consulta, obtener la informaci�n.
		if (nombre_arch_salida_consul == null || nombre_arch_salida_consul.equals("")) {
			EIGlobal.mensajePorTrace(textoLog + "Entro a consulta de nomina", EIGlobal.NivelLog.INFO);
			request.setAttribute("newMenu", session.getFuncionesDeMenu());
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
			String sessionId = (String) request.getSession().getId();
			ServicioTux tuxGlobal = new ServicioTux();


			Hashtable htResult = null;
			String tramaEntrada = generarTramaConsultaArchivo(request, response);

			try {
				EIGlobal.mensajePorTrace(textoLog + "tramaentrada[" + tramaEntrada + "]", EIGlobal.NivelLog.INFO);
				htResult = tuxGlobal.web_red(tramaEntrada);
			} catch (java.rmi.RemoteException re) {
				EIGlobal.mensajePorTrace(textoLog + "ERROR "+re.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(textoLog + "Excepci�n al ejecutar tuxGlobal.web_red", EIGlobal.NivelLog.INFO);
			}

			if (null == htResult) {
				EIGlobal.mensajePorTrace(textoLog + "null ret despues de webred", EIGlobal.NivelLog.INFO);
				despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, "Consulta de Proceso de Alta", "Servicios &gt; N&oacute;mina &gt;Consultas &gt;Alta Empleados ", request, response);
				return;
			}

			String tramaSalida = (String) htResult.get("BUFFER");
			EIGlobal.mensajePorTrace(textoLog + "tramaSalida: [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);
			String elMensaje = "";
			String estatus_envio = "";

			if (tramaSalida == null)
				tramaSalida = "";

			if (tramaSalida.indexOf("NOMI") >= 0) {
				elMensaje = tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@', 2));
				EIGlobal.mensajePorTrace(textoLog + "elMensaje : [" + elMensaje + "]", EIGlobal.NivelLog.INFO);
				estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
				EIGlobal.mensajePorTrace(textoLog + "estatus_envio = [" + estatus_envio + "]", EIGlobal.NivelLog.INFO);
			} else {
				elMensaje = "";
				estatus_envio = "";
			}

			if (estatus_envio.equals("NOMI0000")) {
				String nombreArchivo = tramaSalida.substring(tramaSalida.lastIndexOf("/") + 1, tramaSalida.lastIndexOf("@"));

				try {
					String tipoCopia = "2"; // de /t/tuxedo... a /tmp
					boolean ok = true;

					ArchivoRemoto archR = new ArchivoRemoto();

					if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
						EIGlobal.mensajePorTrace(textoLog + "No se pudo copiar el archivo", EIGlobal.NivelLog.INFO);
				} catch (Exception ioeSua) {
					EIGlobal.mensajePorTrace(textoLog + "Excepcion " + ioeSua.getMessage(), EIGlobal.NivelLog.INFO);
				}
				// TODO: BIT CU 4131, A5
				/*VSWF ARR -I*/
				if (Global.USAR_BITACORAS.trim().equals("ON")) {
					try {
						BitaHelper bh = new BitaHelperImpl(request, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean) bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ES_NOMINA_EMP_CONS_ALTA_CONSULTA_GENERICA_ALTA);
						bt.setContrato(session.getContractNumber());
						bt.setUsr(session.getUserID8());
						bt.setNombreArchivo("EMP" + session.getUserID8() + "Nom.doc");
						bt.setServTransTux("NOA1");
						if (estatus_envio != null) {
							if (estatus_envio.substring(0, 2).equals("OK")) {
								bt.setIdErr("NOMI0000");
							} else if (estatus_envio.length() > 8) {
								bt.setIdErr(estatus_envio.substring(0, 8));
							} else {
								bt.setIdErr(estatus_envio.trim());
							}
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(textoLog + "ERROR "+e.getMessage(), EIGlobal.NivelLog.ERROR);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(textoLog + "ERROR "+e.getMessage(), EIGlobal.NivelLog.ERROR);
					}
				}
				/*VSWF -F*/
				nombre_arch_salida = IEnlace.DOWNLOAD_PATH + nombreArchivo;
				EIGlobal.mensajePorTrace(textoLog + "nombre_arch_salida = " + nombre_arch_salida, EIGlobal.NivelLog.INFO);
			} else {// del Transacci�n Exitosa
				if (elMensaje.equals(""))
					elMensaje = "Servicio no disponible por el momento.";

				request.setAttribute("Bitfechas", datesrvr.toString());
				request.setAttribute("Encabezado", CreaEncabezado("Consulta de Proceso de Alta", "Servicios &gt; N&oacute;mina &gt;Consultas &gt;Alta Empleados ", "s25800canh", request));
				String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
				request.setAttribute("infoUser", infoUser);

				if (sess.getAttribute("facultadesN") != null)
					request.setAttribute("facultades", sess.getAttribute("facultadesN"));

				request.setAttribute("textcuenta", "");
				request.setAttribute("ContenidoArchivo", "");
				evalTemplate(IEnlace.NOM_MTTO_CSL_TMPL, request, response);
				return;
			}
		} else {
			nombre_arch_salida = nombre_arch_salida_consul;
		}

		ArchivosMantEmpl archSalida = new ArchivosMantEmpl(nombre_arch_salida, request);

		// Obtener el n�mero de p�gina seleccionado por el usuario.
		int pagina = 1;
		if (null != request.getParameter("pagina")) {
			try {
				pagina = Integer.parseInt((String) request.getParameter("pagina"));
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(textoLog + "Excepci�n al ejecutar Integer.parseInt de pagina " + pagina, EIGlobal.NivelLog.INFO);
			}
		}

		EIGlobal.mensajePorTrace(textoLog + "pagina = " + pagina, EIGlobal.NivelLog.INFO);

		String contenidoArch = archSalida.lecturaArchivoSalidaConsul(pagina); // Consultar la p�gina indicada.

		// Enviar el n�mero de registros del archivo.
		if (request.getAttribute("num_regs") == null) {
			Integer num_regs = new Integer(archSalida.cantidadRegArchivoConsul());
			request.setAttribute("num_regs", num_regs.toString());
			EIGlobal.mensajePorTrace(textoLog + "num_regs = " + num_regs.toString(), EIGlobal.NivelLog.INFO);
		}

		if (contenidoArch.indexOf("NO EXISTEN DATOS") >= 0 || contenidoArch.equals("") || contenidoArch.indexOf("Error ") >= 0 || contenidoArch.startsWith("Error")) {
			String elMensaje = "";
			request.setAttribute("Bitfechas", datesrvr.toString());
			request.setAttribute("Encabezado", CreaEncabezado("Consulta de Proceso de Alta", "Servicios &gt; N&oacute;mina &gt;Consultas &gt;Alta Empleados ", "s25800canh", request));

			if (contenidoArch.indexOf("NO EXISTEN DATOS") >= 0)
				elMensaje = "NO EXISTEN DATOS PARA LA CONSULTA.";
			else
				elMensaje = "Error en la lectura del archivo, Intente m�s tarde por favor.";

			String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
			request.setAttribute("infoUser", infoUser);

			if (sess.getAttribute("facultadesN") != null)
				request.setAttribute("facultades", sess.getAttribute("facultadesN"));

			request.setAttribute("textcuenta", "");
			request.setAttribute("ContenidoArchivo", "");
			evalTemplate(IEnlace.NOM_MTTO_CSL_TMPL, request, response);
		} else {
			String arch_exportar = "";

			// Si es la primera p�gina a consultar.
			if (nombre_arch_salida_consul == null || nombre_arch_salida_consul.equals("")) {
				ArchivosMantEmpl archSalida1 = new ArchivosMantEmpl(nombre_arch_salida, request);
				int estatusExporta = archSalida1.archivoExportar();
			}

			arch_exportar = "EMP" + session.getUserID8() + "Nom.doc";

			request.setAttribute("Encabezado", CreaEncabezado("Consulta de Proceso de Alta", "Servicios &gt; N&oacute;mina &gt;Consultas &gt;Alta Empleados ", "s25800canh", request));
			request.setAttribute("ContenidoArchivo", contenidoArch);
			request.setAttribute("textcuenta", "");
			request.setAttribute("nombre_arch_salida", nombre_arch_salida);
			sess.setAttribute("nombre_arch_salida_consul", nombre_arch_salida);

			String botonestmp = "<br>"
			+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
				+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
				+ "<td><a href='javascript:fncExportar()' border=0><img src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar' border=0></a></td>"
				+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
				+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
				+ "<td><A href = javascript:js_cancelaEmpleado(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
				+ "<td></td></tr> </table>";

			request.getSession().setAttribute("em", creaExportModel());
			request.getSession().setAttribute("af", IEnlace.DOWNLOAD_PATH + arch_exportar);

			request.setAttribute("botones", botonestmp);
			evalTemplate(IEnlace.NOM_MTTO_CSL_TMPL, request, response);
		}
	}

	/**
	 * Metodo para definir el ExportModel.
	 * @since 19/03/2015
	 * @author FSW-Indra
	 * @return em de tipo ExportModel
	 */
	private ExportModel creaExportModel(){
		ExportModel em = new ExportModel("consultaProcesoAlta", '|', true)
	    .addColumn(new Column(0, 7, "Fecha"))
	    .addColumn(new Column(8, 19, "Secuencia"))
	    .addColumn(new Column(20, 31, "Num Reg Enviados"))
	    .addColumn(new Column(32, 43, "Num Reg Aceptados"))
	    .addColumn(new Column(44, 55, "Num Reg Rehazados"))
	    .addColumn(new Column(56, 75, "Estatus"));
		return em;
	}

	/**
	 * Genera la trama para la consulta de archivos de nomina a partir de los
	 * parametros seleccionados.
	 * Trama:
	 * <pre>
	 * 1EWEB|&lt;b&gt;empleado&lt;/b&gt;|NOA1|&lt;b&gt;no_contrato&lt;/b&gt;|&lt;b&gt;no_empleado&lt;/b&gt;|&lt;b&gt;perfil&lt;/b&gt;|&lt;b&gt;cadena_busqueda&lt;/b&gt;@&lt;b&gt;ruta_archivo_respuesta&lt;/b&gt;@
	 * </pre>
	 * @since version 1.2 RMM Refactored
	 */
	protected String generarTramaConsultaArchivo(HttpServletRequest request, HttpServletResponse response) {
		textoLog("generarTramaConsultaArchivo");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String usuario = session.getUserID8(), clavePerfil = session.getUserProfile();
		String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
		BaseResource baseResource = (BaseResource) request.getSession().getAttribute("session");
		String sessionId = (String) request.getSession().getId();
		EIGlobal.mensajePorTrace(textoLog + "sessionID = " + sessionId, EIGlobal.NivelLog.DEBUG);
		String rutaArch = "";
		String contrato = baseResource.getContractNumber();
		StringBuffer retValue = new StringBuffer("1EWEB|");
		retValue.append(baseResource.getUserID8()).append("|NOA1|").append(baseResource.getContractNumber()).append('|').append(baseResource.getUserID8()).append('|').append(baseResource.getUserProfile()).append('|');
		String cadeFinal = "";
		String cadContrato = "";
		String cadena_a_buscar = "";
		String fchaRecepcion = (String) request.getParameter("fecha1");
		String fchaAplicacion = (String) request.getParameter("fecha2");
		String num_secuencia = (String) request.getParameter("secuencia");
		String ChkRecibido = (String) request.getParameter("ChkRecibido");
		String ChkCancelado = (String) request.getParameter("ChkCancelado");
		String ChkEnviado = (String) request.getParameter("ChkEnviado");
		String ChkPagado = (String) request.getParameter("ChkPagado");
		retValue.append("num_cuenta2='").append(contrato).append("'");

		if (null != fchaRecepcion && !fchaRecepcion.trim().equals("")) {
			fchaRecepcion = fchaRecepcion.trim();
			retValue.append(" and fch_recepcion>=to_date('");

			if (10 >= fchaRecepcion.length()) {
				fchaRecepcion = fchaRecepcion.substring(0, 2)
						+ fchaRecepcion.substring(3, 5)
						+ fchaRecepcion.substring(6);
			}
			retValue.append(fchaRecepcion.trim()).append("','ddmmyyyy')");
		}
		if (null != fchaAplicacion && !fchaAplicacion.trim().equals("")) {
			fchaAplicacion = fchaAplicacion.trim();
			retValue.append(" and fch_recepcion<=to_date('");

			if (10 >= fchaAplicacion.length()) {
				fchaAplicacion = fchaAplicacion.substring(0, 2)
						+ fchaAplicacion.substring(3, 5)
						+ fchaAplicacion.substring(6);
			}
			retValue.append(fchaAplicacion.trim()).append("','ddmmyyyy')");
		}

		//Validaciones de estatus
		boolean filterByStatus = false;

		if (null != ChkRecibido && ChkRecibido.startsWith("R")) {
			filterByStatus = true;
			retValue.append("and estatus in ('").append(ChkRecibido.trim()).append("'");
		}
		if (null != ChkCancelado && ChkCancelado.startsWith("C")) {
			if (!filterByStatus) {
				retValue.append("and estatus in (");
				filterByStatus = true;
			} else {
				retValue.append(",");
			}
			retValue.append("'").append(ChkCancelado.trim()).append("'");
		}
		if (null != ChkEnviado && ChkEnviado.startsWith("E")) {
			if (!filterByStatus) {
				retValue.append("and estatus in (");
				filterByStatus = true;
			} else {
				retValue.append(",");
			}
			retValue.append("'L','").append(ChkEnviado.trim()).append("','B'");
		}
		if (null != ChkPagado && ChkPagado.startsWith("P")) {
			if (!filterByStatus) {
				retValue.append("and estatus in (");
				filterByStatus = true;
			} else {
				retValue.append(",");
			}
			retValue.append("'").append(ChkPagado.trim()).append("'");
		}
		if (filterByStatus) {
			retValue.append(")");
		}
		//Termina Validaciones de estatus

		if (null != num_secuencia && !num_secuencia.trim().equals("")) {
			retValue.append(" and secuencia='").append(num_secuencia.trim()).append("'");
		}

		retValue.append('@').append(new java.io.File(Global.DIRECTORIO_REMOTO_INTERNET, "EMP" + request.getSession().getId()).getAbsolutePath()).append(baseResource.getContractNumber()).append('@');
		return retValue.toString();
	}

	// metodo posCar que devuelve la posicion de un caracter en un String
	public int posCar(String cad, char car, int cant) {
		int result = 0, pos = 0;

		for (int i = 0; i < cant; i++) {
			result = cad.indexOf(car, pos);
			pos = result + 1;
		}
		return result;
	}
}