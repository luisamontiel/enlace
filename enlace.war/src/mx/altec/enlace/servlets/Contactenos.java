package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.classConfigura;
import mx.altec.enlace.utilerias.Conectar;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class Contactenos extends BaseServlet{

	String pathArchivo = Global.APP_PATH + "/EI.cfg";

    public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String   strVentana;
	    EIGlobal.mensajePorTrace( "***Contactenos.class & Entre a Contactenos &", EIGlobal.NivelLog.INFO);

		 strVentana = (String) req.getParameter("ventana");
		 EIGlobal.mensajePorTrace( "***Contactenos.class & strVentana: " + strVentana + " &", EIGlobal.NivelLog.INFO);

		 if (strVentana == null) { strVentana = "0";  }

         if(SesionValida(req, res))
           {
			 if(strVentana.equals("0"))
			  {
				 CapturaDatos(req, res);
			  }
			 if(strVentana.equals("1"))
			  {
				 EjecutaOperacion(req, res);
			  }
		   }
    }

	public int CapturaDatos(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String strTramaSelec1 = "";
		String[] arrdatos;
		String[] arropcion;
		StringBuffer Trama = new StringBuffer("");

		classConfigura conf = new classConfigura(pathArchivo);
		strTramaSelec1   = conf.actualiza("SELEC1","trama");

		arrdatos = conf.desentramaC(strTramaSelec1,'!');
		if(Integer.parseInt(arrdatos[0]) > 0)
		{
			for(int i=1; i<=Integer.parseInt(arrdatos[0]); i++)
			{
				arropcion = conf.desentramaC("3|"+arrdatos[i]+"|",'|');

				Trama.append( "<option value=\"" );
				Trama.append(arropcion[1] );
				Trama.append("|" );
				Trama.append(arropcion[2] );
				Trama.append("\">" );
				Trama.append(arropcion[3] );
				Trama.append("</option>\n");
			}
		}

		req.setAttribute("combo",          Trama.toString());
	    req.setAttribute("newMenu",	    session.getFuncionesDeMenu());
        req.setAttribute("Encabezado",	    CreaEncabezado("�C�mo podemos ayudarle?", "Cont&aacute;ctenos", "s26340h", req));
	    req.setAttribute("MenuPrincipal",  session.getStrMenu());
		req.setAttribute("nombreEmpresa",  session.getNombreContrato());
		req.setAttribute("codigoUsuario",  session.getUserID8());
		req.setAttribute("nombreUsuario",  session.getNombreUsuario());

		evalTemplate("/jsp/ContactenosCaptura.jsp", req, res);

		return 1;

	}

	public int EjecutaOperacion(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		//String RESPUESTA_TMPL = "EnlaceInternet/Templates/s51740.html";
		String strMsg		 = "";
		StringBuffer datosContacto = new StringBuffer("");

		classConfigura conf = new classConfigura(pathArchivo);
		String strMailServer  = conf.actualiza("SMTP_SERVER","unico");

		String strDescTipoCarta = (String) req.getParameter("tipocarta");
		String strProducto	    = (String) req.getParameter("producto");
		String strNombre        = session.getNombreUsuario();
		String strTelCasa       = (String) req.getParameter("telefono");
		String strEmail         = (String) req.getParameter("email");
		String strUsuario	    = (String) req.getParameter("cliente");
		String strTipoContacto  = (String) req.getParameter("tipocontacto");
		String strDescProducto  = (String) req.getParameter("descprod");
		String strComentarios   = (String) req.getParameter("comentarios");

		EIGlobal.mensajePorTrace( "***Contactenos.class & strDescProducto: " + strDescProducto + " &", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***Contactenos.class & strComentarios: " + strComentarios + " &", EIGlobal.NivelLog.INFO);

		/*if(!strDescProducto.equals("Otros"))
			strDescProducto = strDescProducto.substring(8, strDescProducto.length());*/

		EIGlobal.mensajePorTrace( "***Contactenos.class & strDescProducto: " + strDescProducto + " &", EIGlobal.NivelLog.INFO);

		if (session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
			{strProducto = strProducto.substring(0,strProducto.indexOf("|"));}
		else if (session.getClaveBanco().equals(Global.CLAVE_SERFIN))
			{strProducto = strProducto.substring(strProducto.indexOf("|")+1);}

		String de           = strEmail;
        String nombre_de    = strNombre;
        String para         = strProducto;
        String cc           = "";
        String bcc          = "";

        StringBuffer asunto = new StringBuffer("");
        StringBuffer mensaje= new StringBuffer("");

		asunto.append("Cont�ctenos: ");
		asunto.append(strDescTipoCarta );
		asunto.append(", " );
		asunto.append(strDescProducto);

		mensaje.append("Sr. Director:\n\n");
		mensaje.append(strComentarios);
		mensaje.append("\n\n\n");
		mensaje.append("Atentamente\n");
		mensaje.append(strNombre);
		mensaje.append("\n\n");
		mensaje.append("___________________________________________________\n");
		mensaje.append("Nombre del Usuario: ");
		mensaje.append(strNombre);
		mensaje.append("\n");

		if (strTipoContacto.equals("1"))
		{
			mensaje.append("Forma de Contacto : Por E-mail\n");
		}
		else
		{
			mensaje.append("Forma de Contacto : Por Tel�fono\n");
		}

		mensaje.append("Tema Seleccionado : ");
		mensaje.append(strDescProducto);
		mensaje.append("\n");
		mensaje.append("Tel�fono          : ");
		mensaje.append(strTelCasa);
		mensaje.append("\n");
		mensaje.append("___________________________________________________\n");

        String organization = "Grupo Financiero Santander Mexicano";

        StringBuffer correo       = new StringBuffer("");

		correo.append( "Organization:");
		correo.append(organization);
		correo.append("\nFrom:");
		correo.append(nombre_de );
		correo.append(" <");
		correo.append(de );
		correo.append(">");
		correo.append("\nTo:");
		correo.append(para);
		correo.append("\nCc:");
		correo.append(cc);
		correo.append("\nBc:");
		correo.append(bcc);
		correo.append("\nSubject:");
		correo.append(asunto.toString());
		correo.append("\n\n");
		correo.append(mensaje.toString());

		para   += ","+ cc +","+ bcc;

		EIGlobal.mensajePorTrace( "***Contactenos.class & mensaje: " + mensaje + " &", EIGlobal.NivelLog.INFO);

		int    recibio      = Conectar.mandar(de,para,correo.toString(),strMailServer); // Se hace la llamada a clase Conectar

		datosContacto.append( strEmail);
		if(!strEmail.equals(""))
			datosContacto.append("<BR>");
		datosContacto.append(strTelCasa);

		if(recibio == 1)
		{
			EIGlobal.mensajePorTrace( "***Contactenos.class & Correo enviado! &", EIGlobal.NivelLog.INFO);

			req.setAttribute("newMenu",	    session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal",  session.getStrMenu());
	        req.setAttribute("Encabezado",	    CreaEncabezado("", "Cont&aacute;ctenos", "s26350h", req));
			req.setAttribute("nombreUsuario",  session.getNombreUsuario());
			req.setAttribute("datosContacto",	datosContacto.toString());
			evalTemplate("/jsp/ContactenosEnvio.jsp", req, res);
		}
		else
		{
			EIGlobal.mensajePorTrace( "***Contactenos.class & El correo no pudo ser enviado! &", EIGlobal.NivelLog.INFO);
			strMsg = "� Lo sentimos !\n\nPor el momento no se ha podido enviar tu mensaje intente de nuevo m�s tarde.";
			despliegaPaginaError(strMsg, "�C�mo podemos ayudarle?", "Cont&aacute;ctenos", req, res);
		}

		return 1;

	}


	public String CreaEncabezado( String tituloPantalla, String posicionMenu, String ClaveAyuda, HttpServletRequest req ) {
		String strEncabezado="";
		String v_contrato="";
		String tit_contrato="";

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");


		if ( session.getContractNumber() != null ) {
			tit_contrato = "Contrato:";
			v_contrato = ObtenContUser(req);
			EIGlobal.mensajePorTrace("#############-----------------@@@@@@@@@---->"+v_contrato, EIGlobal.NivelLog.INFO);

		}
		StringBuffer strEnc = new StringBuffer();

		strEnc.append("<table width=");
		strEnc.append("\"760\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		strEnc.append("\n<tr>\n");
		strEnc.append("<td width=\"676\" valign=\"top\" align=\"left\">\n");
		strEnc.append("<table width=\"666\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td width=\"528\" valign=\"top\" class=\"titpag\">\n");
		strEnc.append(tituloPantalla);
		strEnc.append("</td>\n");
		strEnc.append("<td width=\"120\" align=\"right\" valign=\"bottom\" class=\"texencfec\">\n");
		strEnc.append(ObtenFecha());
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td valign=\"top\" class=\"texencrut\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gic25030.gif\" width=\"7\" height=\"13\"> \n");
		strEnc.append(posicionMenu);
		strEnc.append("</td>\n");
		strEnc.append("<td class=\"texenchor\" align=\"right\" valign=\"top\">\n");
		strEnc.append(ObtenHora());
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");
		strEnc.append("</td>\n");
		strEnc.append( "<td width=\"40\" valign=\"top\"><a href=\"javascript:FrameAyuda('" );
		strEnc.append( ClaveAyuda );
		strEnc.append("');\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('ayuda','','/gifs/EnlaceMig/gbo25171.gif',1)\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gbo25170.gif\" width=\"33\" height=\"49\" name=\"ayuda\" border=\"0\"></a></td>\n");
		strEnc.append("<td width=\"44\" valign=\"top\"><a href=\"logout\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gbo25180.gif\" width=\"44\" height=\"49\" name=\"finsesion\" border=\"0\"></a></td>\n\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");
		//Contrato
		/*strEnc.append("<table width=\"760\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td align=\"center\" valign=\"top\" class=\"texenccon\"><span class=\"texencconbol\">");
		strEnc.append(tit_contrato);
		strEnc.append("</span>\n");
		strEnc.append(v_contrato);
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");*/

		strEnc.append("<Script language = \"JavaScript\">\n");
		strEnc.append("<!--\n");
		strEnc.append("function FrameAyuda(ventana){\n");
		strEnc.append("hlp=window.open(\"/EnlaceMig/\"+ventana+\".html\" ,\"hlp\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450\");\n");
		strEnc.append("hlp.focus();\n");
		strEnc.append("}\n");
		strEnc.append("//-->\n");
		strEnc.append("</Script>\n");
		strEncabezado= strEnc.toString();

		return strEncabezado;

	}

}
