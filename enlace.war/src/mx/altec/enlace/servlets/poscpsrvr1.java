/*Banco Santander Mexicano
  Clase poscpsrvr1  Contiene la llamada al servicio para la consulta de posicion
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 03/02/2001 - Quitar codigo muerto
 */

package mx.altec.enlace.servlets;

import java.lang.reflect.Method;
import java.lang.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
//03/01/07
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class poscpsrvr1 extends BaseServlet{
    //RMM 20021210 comented out
    //String buffer    = "";
    //String[] Vista   = new String[3];
    //String[] Plazo   = new String[9];
    //String[] Plazo2d = new String[4];
    //fin commented out

    String strDisponible = "";


    //realiza la llamada al servicio WEB_RED para la consulta
	//de posicion
    public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        // RMM 20021210
        String[] Vista   = new String[3];
        String[] Plazo   = new String[9];
        String[] Plazo2d = new String[4];
        //fin RMM 20021210
       HttpSession sess = req.getSession();
       BaseResource session = (BaseResource) sess.getAttribute("session");

	   EIGlobal.mensajePorTrace("***poscpsrvr1.class Entrando a poscpsrvr1 ", EIGlobal.NivelLog.INFO);
	   boolean sesionvalida = SesionValida( req, res);

       String posi       = "";
	   String cta_posi   = "";
       String coderror   = "";
	   String trama      = "";
	   String tipocuenta = "";
       double gTotal       = 0;
	   double totalValores = 0;

	   //-------------
	   String url        = req.getParameter("vieneDeVista");
	   url = (url == null)?" javascript:history.back(); ":" vista?ventana=0 ";
	   // ------------

       String[] cta_posi2;

	  String funcion_llamar = req.getParameter("opcion");

		 if (funcion_llamar==null)
             funcion_llamar="0";

       if (sesionvalida) {
//			if(session.getFacCPosicion().trim().length()!=0){
			if(session.getFacultad(session.FAC_CONSULTA_POSICION)){
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				
				String numeroCuentaInd = req.getParameter ("cta_posi");
				 
				cta_posi2 = desentramaC(numeroCuentaInd,'|');
				
			    cta_posi = cta_posi2[1];
		        tipocuenta=cta_posi2[3];
               
			   //POST ------------
				boolean resultado = evaluarCuenta(cta_posi,session.getContractNumber());
				if(!resultado){
				    despliegaPaginaErrorURL("Operaci&oacute;n no autorizada","Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n", "s25100h", url, req, res);
					return;
				}
				//POST -------------------- 
				
				
	      trama="2EWEB|"+session.getUserID8()+"|POSI|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+cta_posi+"|"+tipocuenta+"|";

	          EIGlobal.mensajePorTrace("***poscpsrvr1 WEB_RED-POSI>>"+trama, EIGlobal.NivelLog.DEBUG);
			      try{
						Hashtable hs = tuxGlobal.web_red(trama);
						coderror = (String) hs.get("BUFFER");
						EIGlobal.mensajePorTrace("***poscpsrvr1 coderror>>"+coderror, EIGlobal.NivelLog.DEBUG);
					}catch( java.rmi.RemoteException re ){
						re.printStackTrace();
				    } catch (Exception e){}
		          if (session!=null) {
			      if(session.getStrMenu()!=null)
				     req.setAttribute("MenuPrincipal", session.getStrMenu());
	              else
				     req.setAttribute("MenuPrincipal", "");
			      if(session.getFuncionesDeMenu()!=null)
				     req.setAttribute("newMenu", session.getFuncionesDeMenu());
	              else
				     req.setAttribute("newMenu",       "");
             }

	          if (funcion_llamar==null)
		         funcion_llamar="0";

			  if(funcion_llamar.equals("0"))
				req.setAttribute("Encabezado", CreaEncabezado("Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n","s25100h",req));
	          else
		        req.setAttribute("Encabezado", CreaEncabezado("Cambio de Instrucci&oacute;n","Inversioness &gt; Cambio de Instrucci&oacute;n","s26220h",req));

			  if(  (coderror.equals(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8())) ||
			  	   (coderror.equals(IEnlace.REMOTO_TMP_DIR + "/"+ session.getUserID8()))){

					 ArchivoRemoto archR = new ArchivoRemoto();
			     if(!archR.copia(session.getUserID8())){
				    EIGlobal.mensajePorTrace("***poscpsrvr1 no se realizo la copia remota>>", EIGlobal.NivelLog.INFO);
				     }

				      trama = lecturaarchivoposi(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8(),funcion_llamar,cta_posi);
				      EIGlobal.mensajePorTrace("***poscpsrvr1 trama>>"+trama, EIGlobal.NivelLog.INFO);
				//****** mod jah 290502
				   String ArchivoPosicionLocal = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8();
			       String tramahtml = "";
				   BufferedReader entrada;
				   String linea     = "";
				   String PosHeader = "";
				   String Operacion = "";
				   double v1;
			       double v2;
			       long v3;
			       double v4;
				   String strRubro  = "";
				   String c         = "";

			   try{
				  entrada= new BufferedReader( new FileReader( ArchivoPosicionLocal ) );
		          linea=entrada.readLine();
				   if( linea.substring(0,2).equals( "OK" ) ){
		             strDisponible = linea.substring(57,71).trim();
		             gTotal += new Double(strDisponible).doubleValue();
				     while ( ( linea = entrada.readLine() ) != null ) {
	           			if ( linea.equals( "" ) )
						linea = entrada.readLine();
					if ( linea == null )
						break;
					if (linea.indexOf(';')!=-1)
						Operacion = linea.substring(0,linea.indexOf(';'));
	                else
		                Operacion = "";

					if(Operacion.equals("VISTA")){
				        Vista = desentramaC("2;" + linea + ";",';');
					    gTotal += new Double(Vista[2]).doubleValue();
	                }else if(Operacion.equals("PLAZO")){
		                strRubro ="Plazo";
			            Plazo = desentramaC("8;" + linea + ";",';');
                         Plazo2d = desentramaC("3|" +  Plazo[8], '|');

				        v1 = 0;//new Double(resultado[indice][2]).doubleValue();
					    v2 = new Double(Plazo[3]).doubleValue();
						v3 = new Long(Plazo[5]).longValue();
	                    v4 = new Double(Plazo2d[1]).doubleValue();
		                totalValores += v4;
			            gTotal += v4;
				        }//if operaciones plazo
		            }  //while
			     } // OK
				}catch(Exception e){
					  tramahtml=null;
					  EIGlobal.mensajePorTrace("poscpsrvr1 "+e, EIGlobal.NivelLog.INFO);
					  e.printStackTrace();
				   }

			  if (trama!=null){
				  if(funcion_llamar.equals("0")){ //posicion normal

					  req.setAttribute("vista",FormatoMoneda(new Double(Vista[2]).doubleValue()));
					  req.setAttribute("disponible",FormatoMoneda(new Double(strDisponible).doubleValue()));
					  req.setAttribute("cuenta",cta_posi + " " + cta_posi2[2]);
					  req.setAttribute("total",FormatoMoneda(new Double(gTotal).doubleValue()));
                      req.setAttribute("totalValores",FormatoMoneda(new Double(totalValores).doubleValue()));
                      req.setAttribute("posi",trama);

//	  	res.sendRedirect(res.encodeRedirectURL("/NASApp/enlaceMig"+IEnlace.POSICION_TMPL));
                    //TODO TODO BIT CU1051
  			        /*
  					 * 03/ENE/07
  					 * VSWF-BMB-I
  					 */
                      if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
                      try{
	  			        BitaHelper bh = new BitaHelperImpl(req, session, sess);
	  					BitaTransacBean bt = new BitaTransacBean();
	  					bt = (BitaTransacBean)bh.llenarBean(bt);
	  					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals
	  							(BitaConstants.EC_SALDO_CUENTA_CHEQUE)){
	  						bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_CHEQUE_CONS_POS_CUENTA);
	  					}else if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals
	  							(BitaConstants.EC_SALDO_CUENTA_BANCA)){
	  						bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_BANCA_CONS_POS_BANCA);
	  					}else if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals
	  							(BitaConstants.EC_SALDO_CONS_CHEQUE)){
	  						bt.setNumBit(BitaConstants.EC_SALDO_CONS_CHEQUE_CONS_POS_CUENTA);
	  					}else if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals
	  							(BitaConstants.EC_POSICION_BANCA)){
	  						bt.setNumBit(BitaConstants.EC_POSICION_BANCA_CONS_POS);
	  					}
	  					else{
	  						bt.setNumBit(BitaConstants.EC_POSICION_CHEQUERAS_CONS_POS_CHEQUERA);
	  					}
	  					if(session.getContractNumber()!=null){
	  						bt.setContrato(session.getContractNumber());
	  					}
	  					if(cta_posi.length()>20){
	  						bt.setCctaOrig(cta_posi.substring(0,20));
	  					}else{
	  						bt.setCctaOrig(cta_posi.trim());
	  					}
	  					if(coderror!=null){
			    			if(coderror.substring(0,2).equals("OK")){
			    				bt.setIdErr("POSI0000");
			    			}else if(coderror.length()>8){
		  						bt.setIdErr(coderror.substring(0,8));
		  					}else{
		  						bt.setIdErr(coderror.trim());
		  					}
	  					}
	  					bt.setServTransTux("POSI");
	  					bt.setImporte(gTotal);
	  					BitaHandler.getInstance().insertBitaTransac(bt);
  					}catch(NumberFormatException e){}
                      catch(SQLException e){
  						e.printStackTrace();
  					}catch(Exception e){
  						e.printStackTrace();
  					}
                      }
  			        /*
  					 * VSWF-BMB-F
  					 */
   				      evalTemplate(IEnlace.POSICION_TMPL, req, res);
	                  }else if (funcion_llamar.equals("1")){
		                  req.setAttribute("cuenta",cta_posi + " " + cta_posi2[2]);
			              req.setAttribute("posi",trama);
				          evalTemplate(IEnlace.CAMBIO_INSTR_TMPL, req, res);
					  }
				  }else{
					if (funcion_llamar.equals("0"))
					  despliegaPaginaErrorURL(IEnlace.MSG_PAG_NO_DISP,"Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n", "s25100h", url, req, res);
					else
   					  despliegaPaginaErrorURL("No existen Plazos","Cambio de Instrucci&oacute;n","Inversioness &gt; Cambio de Instrucci&oacute;n", "s25100h", url, req, res );
	              }
	          }else{
		          if (funcion_llamar.equals("0"))
				    despliegaPaginaErrorURL(IEnlace.MSG_PAG_NO_DISP,"Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n", "s25100h", url, req, res);
				  else
					despliegaPaginaErrorURL(IEnlace.MSG_PAG_NO_DISP,"Cambio de Instrucci&oacute;n","Inversioness &gt; Cambio de Instrucci&oacute;n", "s25100h", url, req, res);
			 }
		}else {
	          if (funcion_llamar.equals("0"))
			    despliegaPaginaErrorURL("No tiene facultad para revisar Posici&oacute;n.","Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n", "s25100h", url, req, res);
			  else
				despliegaPaginaErrorURL("No tiene facultad para revisar Posici&oacute;n.","Cambio de Instrucci&oacute;n","Inversioness &gt; Cambio de Instrucci&oacute;n", "s25100h", url, req, res);
				EIGlobal.mensajePorTrace("***poscpsrvr1.class no tiene facultades el usuario>>", EIGlobal.NivelLog.INFO);
			}
	}else{
			req.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
        }
	}

    //Lectura e interpretacion del archivo donde contiene el resultado
    //de la consulta de posicion
	//Esta consulta se puede realizar desde la consulta de posicion funcion_llamar=0
	//y desde el cambio de instrumento  funcion_llamar:1

	public String lecturaarchivoposi(String ArchivoPosicionLocal,String funcion_llamar,String ctaposicion){

       String tramahtml = "";
	   BufferedReader entrada;
        // Modificacion RMM 20021210
        String [] Vista = new String[3];
        String[] Plazo   = new String[9];
        String[] Plazo2d = new String[4];
        // fin
	   String linea     = "";
	   String PosHeader = "";
	   String Operacion = "";
        double gTotal       = 0;
	   double totalValores = 0;

	   double v1;
       double v2;
       long v3;
       double v4;

	   String strRubro = "";
	   int residuo;
	   String c = "";
	   int g = 0;

	   try{
		  entrada= new BufferedReader( new FileReader( ArchivoPosicionLocal ) );
          linea=entrada.readLine();
           if( linea.substring(0,2).equals( "OK" ) ){
             strDisponible = linea.substring(57,71).trim();

             if(funcion_llamar.equals("0")){ //posicion normal

                PosHeader = "\n<tr><td class=textabdatobs align=left>CHEQUES</td>";
                PosHeader += "\n<td class=textabdatobs>&nbsp;</td>";
                PosHeader += "\n<td class=textabdatobs>&nbsp;</td>";
                PosHeader += "\n<td class=textabdatobs>&nbsp;</td>";
                PosHeader += "\n<td class=textabdatobs align=right nowrap>" + FormatoMoneda(new Double(strDisponible).doubleValue()) + "</td></tr>";
             }
             gTotal += new Double(strDisponible).doubleValue();
             while ( ( linea = entrada.readLine() ) != null ) {
             	if ( linea.equals( "" ) )
					linea = entrada.readLine();
				if ( linea == null )
					break;
                if (linea.indexOf(';')!=-1)
					Operacion = linea.substring(0,linea.indexOf(';'));
                else
                    Operacion = "";

				if(Operacion.equals("VISTA")){
                    Vista = desentramaC("2;" + linea + ";",';');
                    gTotal += new Double(Vista[2]).doubleValue();
                }else if(Operacion.equals("PLAZO")){
                    strRubro ="Plazo";
                    Plazo = desentramaC("8;" + linea + ";",';');
                    Plazo2d = desentramaC("3|" +  Plazo[8], '|');

                    v1 = 0;//new Double(resultado[indice][2]).doubleValue();
                    v2 = new Double(Plazo[3]).doubleValue();
                    v3 = new Long(Plazo[5]).longValue();
                    v4 = new Double(Plazo2d[1]).doubleValue();
                    totalValores += v4;
                    /*   Modificación para integración 24/06/2002
                    switch (new Integer(Plazo2d[2]).intValue()){
                          case 32: strRubro = "Inversi&oacute;n D&oacute;lares";
                                   break;
                          case 34: strRubro = "Pagar&eacute; tasa especial";
                                   break;
                          case 37: strRubro = "Inversi&oacute;n 7";
                                   break;
                          case 42: strRubro = "Inversi&oacute;n D&oacute;lares";
                                   break;
                          case 44: strRubro = "Pagar&eacute; tasa especial";
                                   break;
                          case 62: strRubro = "Inversi&oacute;n Plazo Personal";
                                   break;
                          case 64: strRubro = "Cuenta UDI Personal";
                                   break;
                          case 67: strRubro = "Inversi&oacute;n Plazo Empresaria;";
                                   break;
                          case 72: strRubro = "Pagar&eacute; PRV";
                                   break;
                          case 73: strRubro = "Pagar&eacute; 1 d&iacute;a";
                                   break;
                          case 77: strRubro = "Pagar&eacute; PRV'S Personal";
                                   break;
                          case 78: strRubro = "Pagar&eacute; PRV Empresarial";
                                   break;
                    } //swich
					*/
                    strRubro ="Inversi&oacute;n Plazo";

                    gTotal += v4;
                    residuo=g % 2 ;

					if (residuo == 0)
					   c="textabdatcla";
					else
                       c="textabdatobs";

                    if(funcion_llamar.equals("0")) //posicion normal
                      tramahtml = tramahtml + "<tr><td class=" + c + " align=left>" + strRubro + " " + Integer.parseInt(Plazo[2]) + " d (" + Plazo[7] + ")</td>" +
                                               "<td class="+c+" >" + Plazo[4].trim() + "</td>" +
                                               "<td class="+c+" align=center  nowrap>"+ v2 + "</td>" +
                                               "<td class="+c+" align=center  nowrap>"+ v3 + "</td>" +
                                               "<td class="+c+" align=right>"+ FormatoMoneda(v4) + "</td></tr>" ;


                    else
                    if(funcion_llamar.equals("1")) //cambio de instrumento
                      tramahtml = tramahtml + "<tr><td class=" + c + " align=left><INPUT TYPE =RADIO NAME=CI  VALUE="+Plazo2d[3]+"|"+Plazo2d[2]+"|"+v3+"|"+ctaposicion+"|"+"></td><td class=" + c + ">&nbsp; " + strRubro + " " + Integer.parseInt(Plazo[2]) + " d (" + Plazo[7] + ")</td>" +
                                               "<td class="+c+" >" + Plazo[4].trim() + "</td>" +
                                               "<td class="+c+" align=center  nowrap>"+ v2 + "</td>" +
                                               "<td class="+c+" align=center  nowrap>"+ v3 + "</td>" +
                                               "<td class="+c+" align=right>"+ FormatoMoneda(v4) + "</td></tr>" ;

					g++;
					//if
                }//if operaciones plazo

             }//while
             residuo=g % 2 ;
			 if (residuo == 0)
			   c="textabdatcla";
			 else
               c="textabdatobs";
	         if(funcion_llamar.equals("0"))
                tramahtml = PosHeader + tramahtml +
                            "<tr><td class=" + c + " align=left>VISTA</td>" +
                            "<td class=" + c +">&nbsp;</td>" +
                            "<td class=" + c +">&nbsp;</td>" +
                            "<td class=" + c +">&nbsp;</td>" +
                            "<td class=" + c +" align=right nowrap>" + FormatoMoneda(new Double(Vista[2].trim()).doubleValue()) + "</td></tr>";

          }else{
              EIGlobal.mensajePorTrace("poscpsrvr1 error:"+linea, EIGlobal.NivelLog.INFO);
			  tramahtml=null;
          }

       }catch(Exception e){
          tramahtml=null;
		  EIGlobal.mensajePorTrace("poscpsrvr1 "+e, EIGlobal.NivelLog.INFO);
		  e.printStackTrace();
	   }
	   if ((totalValores==0)&&(funcion_llamar.equals("1")))
	      tramahtml=null;
	   return tramahtml;
    }


  /*String[] GetHeader(String buffer, char Separador){
   String bufRest;
   int i=1;
   String[] cabecera = new String[1000];
   bufRest = buffer;
   String mysubstr = null;
   while(bufRest.length()>0){
       cabecera[i] = bufRest.substring(0,bufRest.indexOf(Separador));
        bufRest = bufRest.substring(bufRest.indexOf(Separador) +1,bufRest.length());
        i++;
   }
   cabecera[0] =new Integer(i-1).toString();
   return cabecera;
  }*/

	/**
	 * Metodo que se encarga de validar si la cuenta esta relacionada al contrato.
	 * @param cuenta Cuenta a validar
	 * @param contrato Contrato a validar
	 * @return Regresa true si la cuenta pertenece al contrato.
	 */
	private boolean evaluarCuenta(String cuenta, String contrato) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		StringBuffer query = new StringBuffer();
        boolean resultado = false;
		
        if((cuenta==null)||("".equals(cuenta.trim()))){
          return true;
        }
        
        
        try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query.append("Select num_cuenta From nucl_relac_ctas Where num_cuenta2 ='");
			query.append(contrato);
			query.append("' And num_cuenta = '");
			query.append(cuenta);
			query.append("' ");

			EIGlobal.mensajePorTrace ("Validando la cuenta:: -> Query:" + query.toString(), EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query.toString());

			if(rs.next()){
				resultado=true;
			}

			EIGlobal.mensajePorTrace ("La cuenta existe:"+resultado, EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace (" Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace("SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}
 }