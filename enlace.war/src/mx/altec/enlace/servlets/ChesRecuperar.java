package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



public class ChesRecuperar extends BaseServlet{
	  //String ventana       = "";
      //String contrato      = "";
      //String usuario       = "";
      //String clavePerfil   = "";
      short sucOpera       = (short)787;

      //int salida             = 0;
	  //int totalBeneficiarios = 0;//cambio
	  //String cadenaBeneficiarios = "";//cambio
	  //String strTabla            = "";//cambio

      GregorianCalendar calHoy = new GregorianCalendar();

    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
        //ventana=(String) req.getParameter("ventana");

		EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &Inicia clase&", EIGlobal.NivelLog.INFO);

		String contrato      = "";
	    String usuario       = "";
        String clavePerfil   = "";
		int totalBeneficiarios = 0;//cambio
	    //String cadenaBeneficiarios = "";//cambio
	    //String strTabla            = "";//cambio

        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        if(SesionValida( req, res)){
        	//Variables de sesion ...
        	contrato = session.getContractNumber();
        	sucOpera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
        	usuario  = session.getUserID8();
        	clavePerfil = session.getUserProfile();
			recuperar(contrato, usuario, clavePerfil, req, res);
          }

		EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &Termina clase&", EIGlobal.NivelLog.INFO);
    }


	public int recuperar(String contrato, String usuario, String clavePerfil, HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

	  EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &inicia m�todo recuperar()&", EIGlobal.NivelLog.INFO);

      String secuencia=(String) req.getParameter("secuencia");

	  String cadenaBeneficiarios = "";
      StringBuffer loc_trama = new StringBuffer ("");
      StringBuffer trama_entrada = new StringBuffer ("");
      String trama_salida="";
      String cabecera = "2EWEB";
      String operacion = "COCH";
      StringBuffer resultado = new StringBuffer ("");
	  StringBuffer mensaje = new StringBuffer ("");
      String trama_salidaRedSrvr = "";
	  String path_archivo        = "";
      String nombre_archivo = "";
	  StringBuffer listaConsulta = new StringBuffer ("");
	  String estilo = "";
	  String encabezado = "";
	  String strExportar = "";
	  String cveBeneficiario = "";
	  String titulo = "";
	  String estatusCheque = "";
	  String fechaRecepcion = "";
 	  String strImporte = "";
	  String strDespliegaEstatus = "";
	  String imgResumen = "";
	  String fechaCorta = "";
	  String FacEnviar       = "";//cambio
	  String FacArchivo      = "";//cambio
	  float totalImporte  = 0;
	  float importeCheque = 0;
	  boolean crearTabla     = false;//cambio
	  StringBuffer strTabla = new StringBuffer ("");//cambio

      int num_registros=0;
	  int contador=0;
	  int aceptados=0;
	  int residuo= 0;

	  int maxRegistros       = Global.MAX_REGISTROS;//cambio

      HashMap mapaBeneficiarios;

      if(verificaFacultad("TECHSENVM", req))  //cambio
		FacEnviar = "true";
	  else
		FacEnviar = "false";

      if(verificaFacultad("TECHSCAPM", req)) //cambio
		FacArchivo = "true";
	  else
		FacArchivo = "false";

	  int totalBeneficiarios = 0;//cambio

	  //TuxedoGlobal tuxedoGlobal;



	  loc_trama = new StringBuffer ("num_cuenta2 = '");
	  loc_trama.append (contrato);
	  loc_trama.append ("' AND secuencia = ");
	  loc_trama.append (secuencia );
	  loc_trama.append (" order by num_cheque@");

	  trama_entrada = new StringBuffer (cabecera);
	  trama_entrada.append ("|");
	  trama_entrada.append (usuario);
	  trama_entrada.append ("|");
	  trama_entrada.append (operacion);
	  trama_entrada.append ("|");
	  trama_entrada.append (contrato);
	  trama_entrada.append ("|");
	  trama_entrada.append (usuario);
	  trama_entrada.append ("|");
	  trama_entrada = new StringBuffer (trama_entrada.toString());
	  trama_entrada.append (clavePerfil);
	  trama_entrada.append ("|");
	  trama_entrada.append (loc_trama) ;

	  EIGlobal.mensajePorTrace( "***ChesRecuperar.class  trama de entrada COCH >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);

	  EIGlobal.mensajePorTrace( "***ChesRecuperar.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
	  try{
	  	//tuxedoGlobal = (TuxedoGlobal) session.getEjbTuxedo() ;
		ServicioTux tuxedoGlobal = new ServicioTux();
		//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	  	Hashtable hs = tuxedoGlobal.web_red(trama_entrada.toString());
	  	trama_salida = (String) hs.get("BUFFER");
	  }catch( java.rmi.RemoteException re ){
	  	re.printStackTrace();
	  }catch(Exception e) {}

	  EIGlobal.mensajePorTrace( "***ChesRecuperar.class  trama de salida COCH >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);

      path_archivo = trama_salida;
      nombre_archivo = EIGlobal.BuscarToken(path_archivo, '/', 5);
	  path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;

	  // Copia Remota
	  EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
	  ArchivoRemoto archivo_remoto = new ArchivoRemoto();

	  if(!archivo_remoto.copia( nombre_archivo ) )
		{
			EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &error de remote shell ...&", EIGlobal.NivelLog.INFO);
		    mensaje = new StringBuffer ("No se pudo recuperar el archivo. Intente m&aacute;s tarde.");
			strDespliegaEstatus = "despliegaEstatus();";
			imgResumen = "/gifs/EnlaceMig/gic25060.gif";
			//despliegaPaginaError(mensaje, "Registro de cheque seguridad", "Servicios &gt; Chequera Seguridad &gt; Registro de Cheques &gt; Por Archivo");
		}
	  else
		{
			EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			boolean errorFile=false;
			try
			 {
				File fileOriginal = new File(path_archivo);
				String nombreDestino = Global.DIRECTORIO_LOCAL + "/" + usuario + ".chq";//cambio
				File fileDestino  = new File(nombreDestino);
				fileOriginal.renameTo(fileDestino);

				File drv = new File(nombreDestino);
    			RandomAccessFile file = new RandomAccessFile(drv, "r");
				StringBuffer buf = new StringBuffer();
				int cont = 0;
				trama_salidaRedSrvr = file.readLine();

				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &leyendo archivo linea 1 ... " + trama_salidaRedSrvr + "&", EIGlobal.NivelLog.INFO);

				if( trama_salidaRedSrvr.startsWith("OK"))
				 {
					//cadenaBeneficiarios = listaBenef(contrato, usuario, clavePerfil, mapaBeneficiarios);  Cambio
                     mapaBeneficiarios = listaBenef(contrato, usuario, clavePerfil, 4);
					fechaCorta = EIGlobal.formatoFecha(calHoy, "dd/mm/aaaa");


					listaConsulta  = new StringBuffer ("<table width=\"760\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> ");
					listaConsulta.append ("<tr> ");
					listaConsulta.append ("<td align=\"center\" class=\"tittabdat\" width=\"100\">Cuenta de Cargo</td>");
					listaConsulta.append ("<td align=\"center\" class=\"tittabdat\" width=\"100\">No.de cheque</td>");
					listaConsulta.append ("<td class=\"tittabdat\" align=\"center\" >Benefeciario</td>");
					listaConsulta.append ("<td class=\"tittabdat\" align=\"center\" width=\"100\">Importe</td>");
					listaConsulta.append ("<td class=\"tittabdat\" align=\"center\" width=\"100\">Fecha de recepci&oacute;n</td>");
					listaConsulta.append ("<td class=\"tittabdat\" align=\"center\" width=\"100\">Fecha de libramiento</td>");
					listaConsulta.append ("<td class=\"tittabdat\" align=\"center\">Fecha  l&iacute;mite  de pago</td>");
					listaConsulta.append ("<td class=\"tittabdat\" align=\"center\">Estatus</td>");
					listaConsulta.append ("</tr>");



					while( ( trama_salidaRedSrvr = file.readLine() ) != null ){
						//////
						if( trama_salidaRedSrvr.trim().equals(""))
		 					trama_salidaRedSrvr = file.readLine();
						if( trama_salidaRedSrvr == null )
							break;
						EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &leyendo archivo ... " + trama_salidaRedSrvr + "&", EIGlobal.NivelLog.INFO);
						residuo = contador % 2 ;
						estilo = " class='textabdatobs'>";
						num_registros++;
						if (residuo == 0)
						estilo =" class='textabdatcla'>";

						importeCheque = new Float(EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 5)).floatValue();
						totalImporte += importeCheque;

						fechaRecepcion = EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 6);
						estatusCheque  = EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 10);
						if(estatusCheque.equals("R"))
						   aceptados++;

			            listaConsulta.append ("<tr>\n");
						listaConsulta.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 2) + "</td>\n"); // cuenta de cargo
						listaConsulta.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 3) + "</td>\n"); // no. cheque
						//listaConsulta += "	<td align=center nowrap "+ estilo + BuscaBeneficiario(totalBeneficiarios, EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 4), cadenaBeneficiarios) + "</td>\n"; // Beneficiario
                        //System.err.println ("Beneficiarios: " + BuscaBeneficiario(totalBeneficiarios, EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 4), cadenaBeneficiarios) + " " + totalBeneficiarios);
                        if (mapaBeneficiarios.containsKey (EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 4))) {
                            listaConsulta.append ("	<td align=center nowrap "+ estilo + (String) mapaBeneficiarios.get (EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 4)) + "</td>\n");
                        } else {
                            listaConsulta.append ("	<td align=center nowrap "+ estilo + "Beneficiario no registrado</td>\n");
                        }
						listaConsulta.append ("	<td align=right  nowrap "+ estilo + FormatoMoneda(importeCheque) + "</td>\n");
						listaConsulta.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 6) + "</td>\n"); // Fecha de recepci�n
						listaConsulta.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 7) + "</td>\n");  // Fecha de libramiento
						listaConsulta.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 8) + "</td>\n"); // Fecha l�mite de pago
						listaConsulta.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(trama_salidaRedSrvr, ';', 11) + "</td>\n"); // Estatus
						listaConsulta.append ("</tr>\n");

					} // fin de while

				    listaConsulta.append ("</table><br>");






					if(num_registros <= maxRegistros)  //cambio
					 {
						crearTabla = true;//cambio
					 }
					else
					 {
						strTabla  = new StringBuffer ("\n<table width=\"400\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");//cambio
						strTabla.append ("\n  <tr> ");//cambio
						strTabla.append ("\n    <td align=\"center\" class=\"tittabdat\" width=\"400\">");//cambio
						strTabla.append ("        Recuperaci&oacute;n exitosa. Para ver el detalle del archivo es necesario que lo exporte.");//cambio
						strTabla.append ("\n    </td>");//cambio
						strTabla.append ("\n  </tr> ");//cambio
						strTabla.append ("\n</table><BR>");//cambio
						listaConsulta = new StringBuffer (listaConsulta.toString()+strTabla); //cambio mio
					 }

					strExportar = exportarArchivo(contrato, usuario, clavePerfil, nombreDestino, crearTabla); //cambio




			     }//fin de if de OK
				else
				 {
  					EIGlobal.mensajePorTrace( "***ChesRecuperar.class  & error en el servicio : " + trama_salidaRedSrvr + "&", EIGlobal.NivelLog.INFO);
					//despliegaPaginaError(mensaje, "Registro de cheque seguridad", "Servicios &gt; Chequera Seguridad &gt; Registro de Cheques &gt; Por Archivo");
					strDespliegaEstatus = "despliegaEstatus();";
					imgResumen = "/gifs/EnlaceMig/gic25060.gif";
					mensaje.append (trama_salidaRedSrvr.substring(8, trama_salidaRedSrvr.length() ));
				 }
			    file.close();
             }
	        catch(Exception ioe )
	         {
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  Excepci�n en recuperar() >>"+ioe.getMessage()+"<<",EIGlobal.NivelLog.ERROR);
				errorFile = true;
			 }

			if (errorFile)
			 {
				mensaje.append ("Error al leer el archivo de recuperaci&oacute;n, por favor intente mas tarde.");
				//despliegaPaginaError(mensaje, "Registro de cheque seguridad", "Servicios &gt; Chequera Seguridad &gt; Registro de Cheques &gt; Por Archivo");
				strDespliegaEstatus = "despliegaEstatus();";
				imgResumen = "/gifs/EnlaceMig/gic25060.gif";
			 }
			else
			 {
				strExportar = exportarArchivo(usuario + ".doc");
		     }

 			EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &desplegando datos en pantalla ...&", EIGlobal.NivelLog.INFO);

			req.setAttribute("transmision",        secuencia);
			req.setAttribute("regsEnviados",       String.valueOf(num_registros));
			req.setAttribute("regsAceptados",      String.valueOf(aceptados));
			req.setAttribute("regsRechazados",     String.valueOf(num_registros - aceptados));
			req.setAttribute("fechaTransmision",   fechaRecepcion);
			req.setAttribute("fechaActualizacion", fechaCorta);
			req.setAttribute("importeEnviado",     FormatoMoneda(totalImporte));
			req.setAttribute("Tabla",              listaConsulta);
			req.setAttribute("lnkExportar",        strExportar);
                        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
			req.setAttribute("newMenu",            session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal",      session.getStrMenu());
			req.setAttribute( "Encabezado",        CreaEncabezado("Registro de cheque seguridad","Servicios &gt; Chequera Seguridad &gt; Registro de Cheques &gt; Por Archivo", "s25860h", req));
			req.setAttribute("despliegaEstatus",   strDespliegaEstatus);
			req.setAttribute("resumenArchivo",     mensaje);
	        req.setAttribute("imgResumen",         imgResumen);
			req.setAttribute("tipoArchivo",        "recuperado");

			evalTemplate("/jsp/ChesPrincipal.jsp", req, res );

		}

  EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &termina m�todo recuperar()&", EIGlobal.NivelLog.INFO);
  return 1;

}

// Construye el archivo de texto para la funcion de exportar.
public String exportarArchivo(String contrato, String usuario, String clavePerfil, String archExportar, boolean crearTabla) //cambio agregado
{
  StringBuffer resultado = new StringBuffer ("");
  String registro   = "";
  double dblImporte = 0;
  long lngImporte   = 0;
  String estilo     = "";
  int residuo       = 0;
  int contador      = 0;
  String cadenaBeneficiarios = "";
  StringBuffer strTabla = new StringBuffer ("");//cambio
  int totalBeneficiarios = 0;

  EI_Exportar ArcSal;

	String nombreArchivo=usuario+".doc";
	EIGlobal.mensajePorTrace( "***ChesRecuperar.class & Archivo exportar: " + nombreArchivo + " &", EIGlobal.NivelLog.INFO);

    ArcSal=new EI_Exportar(Global.DIRECTORIO_LOCAL + "/" + nombreArchivo);
    if(ArcSal.creaArchivo())
     {
       int b[]={16,7,13,60,16,10,10};  // Longitud maxima del campo para el elemento
	   boolean errorFile = false;
	   try
		{
		  File drv = new File(archExportar);
		  RandomAccessFile file = new RandomAccessFile(drv, "r");

		  registro = file.readLine();
		  EIGlobal.mensajePorTrace( "***ChesRecuperar.class exportarArchivo() & leyendo archivo linea 1 ... " + registro + "&", EIGlobal.NivelLog.INFO);
		  if(registro.startsWith("OK"))
		   {

			 cadenaBeneficiarios = listaBenef(contrato, usuario, clavePerfil);

			 if(crearTabla)
			   {
				  strTabla  = new StringBuffer ("<table width=\"760\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> ");
				  strTabla.append ("<tr> ");
			      strTabla.append ("<td align=\"center\" class=\"tittabdat\" width=\"100\">Cuenta de Cargo</td>");
				  strTabla.append ("<td align=\"center\" class=\"tittabdat\" width=\"100\">No.de cheque</td>");
				  strTabla.append ("<td class=\"tittabdat\" align=\"center\" >Benefeciario</td>");
				  strTabla.append ("<td class=\"tittabdat\" align=\"center\" width=\"100\">Importe</td>");
				  strTabla.append ("<td class=\"tittabdat\" align=\"center\" width=\"100\">Fecha de recepci&oacute;n</td>");
				  strTabla.append ("<td class=\"tittabdat\" align=\"center\" width=\"100\">Fecha de libramiento</td>");
				  strTabla.append ("<td class=\"tittabdat\" align=\"center\">Fecha  l&iacute;mite  de pago</td>");
				  strTabla.append ("<td class=\"tittabdat\" align=\"center\">Estatus</td>");
				  strTabla.append ("</tr>");
			   }

        	 while( ( registro = file.readLine() ) != null )
		      {
                StringBuffer Linea = new StringBuffer ("");
			    String cveBeneficiario    = "";
			    String nombreBeneficiario = "";

		        EIGlobal.mensajePorTrace( "***ChesRecuperar.class exportarArchivo() & registro ... " + registro + "&", EIGlobal.NivelLog.INFO);

                Linea.append (ArcSal.formateaCampo(EIGlobal.BuscarToken(registro, ';', 2), 16));	// cuenta de cargo
		        Linea.append (ArcSal.formateaCampo(EIGlobal.BuscarToken(registro, ';', 3), 7));	// no. cheque
		        cveBeneficiario = EIGlobal.BuscarToken(registro, ';', 4);
			    Linea.append (ArcSal.formateaCampo(cveBeneficiario, 13));							// cve. beneficiario
			    nombreBeneficiario = BuscaBeneficiario(totalBeneficiarios, cveBeneficiario, cadenaBeneficiarios);
			    Linea.append (ArcSal.formateaCampo(nombreBeneficiario, 60));						// nombre beneficiario
                dblImporte = new Double(EIGlobal.BuscarToken(registro, ';', 5)).doubleValue() * 100;
                lngImporte = new Double(dblImporte).longValue();
                Linea.append (ArcSal.formateaCampoDerecha( new Long(lngImporte).toString(), 16));
			    Linea.append (ArcSal.formateaCampo(EIGlobal.BuscarToken(registro, ';', 7), 10));	// fecha libramiento
			    Linea.append (ArcSal.formateaCampo(EIGlobal.BuscarToken(registro, ';', 8), 10));	// fecha limite
				Linea.append (ArcSal.formateaCampo(EIGlobal.BuscarToken(registro, ';', 11), 60));	// fecha limite

                Linea.append ("\n");
                if(ArcSal.escribeLinea(Linea.toString()))
   		          EIGlobal.mensajePorTrace( "***ChesRecuperar.class & escribeLinea() OK. &", EIGlobal.NivelLog.INFO);
                else
		         EIGlobal.mensajePorTrace( "***ChesRecuperar.class & Fallo escribeLinea(). &", EIGlobal.NivelLog.INFO);

				if(crearTabla)
				  {
					 residuo = contador % 2 ;
					 estilo = " class='textabdatobs'>";
					 if (residuo == 0)
						estilo =" class='textabdatcla'>";

					 strTabla.append ("<tr>\n");
					 strTabla.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(registro, ';', 2) + "</td>\n");	 // cuenta de cargo
					 strTabla.append ("	<td align=right  nowrap "+ estilo + EIGlobal.BuscarToken(registro, ';', 3) + "</td>\n");	 // no. cheque
					 strTabla.append ("	<td align=left   nowrap "+ estilo + nombreBeneficiario + "</td>\n");                      // Beneficiario
					 strTabla.append ("	<td align=right  nowrap "+ estilo + FormatoMoneda(EIGlobal.BuscarToken(registro, ';', 5)) + "</td>\n"); // Importe
					 strTabla.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(registro, ';', 6) + "</td>\n");	 // Fecha de recepci�n
					 strTabla.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(registro, ';', 7) + "</td>\n");  // Fecha de libramiento
					 strTabla.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(registro, ';', 8) + "</td>\n");	 // Fecha l�mite de pago
					 strTabla.append ("	<td align=center nowrap "+ estilo + EIGlobal.BuscarToken(registro, ';', 11) + "</td>\n"); // Estatus
					 strTabla.append ("</tr>\n");

					 contador++;
				  }
              }

			  if(crearTabla)
				 strTabla.append ("</table><br>");

		   }

		  file.close();
		  if(drv.exists())
		   {
			  if(drv.delete())
                {
					EIGlobal.mensajePorTrace("***ChesRecuperar.class & exportarArchivo() drv: Archivo Eliminado: " + archExportar + " &", EIGlobal.NivelLog.INFO);
				}
		      else
			    {
					EIGlobal.mensajePorTrace("***ChesRecuperar.class & exportarArchivo() drv: No se puede Borrar el Archivo: " + archExportar + " &", EIGlobal.NivelLog.INFO);
		        }
		  }
	    }
	   catch(Exception ioe)
	    {
			EIGlobal.mensajePorTrace( "***ChesRecuperar.class  Excepci�n en exportarArchivo() >>"+ioe.getMessage()+"<<",EIGlobal.NivelLog.ERROR);
			errorFile = true;
		}

        ArcSal.cierraArchivo();
		EIGlobal.mensajePorTrace( "***ChesRecuperar.class & Continuando, Archivo cerrado &", EIGlobal.NivelLog.INFO);

		if(!errorFile)
		 {
		    ArchivoRemoto archR = new ArchivoRemoto();
            if(archR.copiaLocalARemoto(nombreArchivo,"WEB"))
		      {
		        // Boton de Exportacion de Archivo.
                resultado = new StringBuffer ("\n<a href='/Download/"+nombreArchivo+"'><img src=\"/gifs/EnlaceInternet/gbo25230.gif\"");
		        resultado.append (" border=0 alt=\"Exportar\" width=\"85\" height=\"22\" ></a>");
		        EIGlobal.mensajePorTrace( "***ChesRegistro.class & copiaLocalARemoto Exitosa &", EIGlobal.NivelLog.INFO);
              }
		    else
		        EIGlobal.mensajePorTrace( "***ChesRecuperar.class & copiaLocalARemoto no realizada &", EIGlobal.NivelLog.INFO);
		 }

     }

   return resultado.toString();

}




// Construye el archivo de texto para la funcion de exportar.

public String exportarArchivo(String nombreArchivo)
{
  StringBuffer resultado = new StringBuffer ("");
  double dblImporte = 0;
  long lngImporte = 0;

  EIGlobal.mensajePorTrace( "***ChesRecuperar.class & nombreArchivo: " + nombreArchivo + "&", EIGlobal.NivelLog.INFO);

  ArchivoRemoto archR = new ArchivoRemoto();

  if(archR.copiaLocalARemoto(nombreArchivo,"WEB"))
    {
	   // Boton de Exportacion de Archivo.
       resultado  = new StringBuffer ("\n<a href='/Download/"+nombreArchivo+"'><img src=\"/gifs/EnlaceMig/gbo25230.gif\"");
	   resultado.append (" border=0 alt=\"Exportar\" width=\"85\" height=\"22\" ></a>");
	   EIGlobal.mensajePorTrace( "***ChesRecuperar.class & copiaLocalARemoto Exitosa &", EIGlobal.NivelLog.INFO);
    }
  else
	   EIGlobal.mensajePorTrace( "***ChesRecuperar.class & copiaLocalARemoto no realizada &", EIGlobal.NivelLog.INFO);

  return resultado.toString();

}


	public String listaBenef(String contrato, String usuario, String clavePerfil){
	   EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &inicia m�todo listaBenef()&", EIGlobal.NivelLog.INFO);
       int    indice;
       StringBuffer encabezado = new StringBuffer ("");
       String tramaEntrada        = "";
       String tramaSalida         = "";
       String path_archivo        = "";
       String nombre_archivo      = "";
       String trama_salidaRedSrvr = "";
       String retCodeRedSrvr      = "";
       String registro            = "";
       StringBuffer listaBeneficiarios  = new StringBuffer ("");

       //TuxedoGlobal tuxGlobal;

       String medio_entrega  = "2EWEB";
       String tipo_operacion = "CBEN";
       encabezado  = new StringBuffer (medio_entrega);
	   encabezado.append ("|");
	   encabezado.append (usuario);
	   encabezado.append ("|");
	   encabezado.append (tipo_operacion);
	   encabezado.append ("|");
	   encabezado.append (contrato);
	   encabezado.append ("|");
       encabezado.append (usuario);
	   encabezado.append ("|");
	   encabezado.append (clavePerfil);
	   encabezado.append ("|");
       tramaEntrada = contrato + "@";
       tramaEntrada = encabezado + tramaEntrada;

       EIGlobal.mensajePorTrace( "***ChesRecuperar.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
	   EIGlobal.mensajePorTrace( "***ChesRecuperar.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
	   try{
	   	//tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   	Hashtable hs = tuxGlobal.web_red(tramaEntrada);
       	tramaSalida = (String) hs.get("BUFFER");
	   	EIGlobal.mensajePorTrace( "***ChesRecuperar.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
	   }catch( java.rmi.RemoteException re){
	   	re.printStackTrace();
	   }catch(Exception e) {}

       path_archivo = tramaSalida;
	   nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());

	   EIGlobal.mensajePorTrace( "***ChesRegistro.class & nombre_archivo:" + nombre_archivo + " &", EIGlobal.NivelLog.INFO);

	   path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;

	   EIGlobal.mensajePorTrace( "***ChesRecuperar.class & path_archivo:" + path_archivo + " &", EIGlobal.NivelLog.INFO);

       boolean errorFile=false;
       try
         {
			// Copia Remota
			EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
			ArchivoRemoto archivo_remoto = new ArchivoRemoto();

			if(!archivo_remoto.copia( nombre_archivo ) )
			{
				// error al copiar
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &error de remote shell ...&", EIGlobal.NivelLog.INFO);
			}
			else
			{
				// OK
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			}

            File drvSua = new File(path_archivo);
            RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
            trama_salidaRedSrvr = fileSua.readLine();
            retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
            if (retCodeRedSrvr.equals("OK"))
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);
             {
               while((registro = fileSua.readLine()) != null){
               		/////
					if( registro.trim().equals(""))
		 				registro = fileSua.readLine();
					if( registro == null )
						break;
                  listaBeneficiarios.append (EIGlobal.BuscarToken(registro, ';', 1) + "|" + EIGlobal.BuscarToken(registro, ';', 2) + ";");
                }
             }
            fileSua.close();
         }catch(Exception ioeSua )
            {
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  Excepci�n en listaBenf() >>"+ioeSua.getMessage()+"<<",EIGlobal.NivelLog.ERROR );
               return "";
            }

      EIGlobal.mensajePorTrace( "***ChesRecuperar.class  termina m�todo listaBenef()", EIGlobal.NivelLog.INFO);
      return(listaBeneficiarios.toString());
  }

public HashMap listaBenef(String contrato, String usuario, String clavePerfil, int a) {
    EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &inicia m�todo listaBenef()&", EIGlobal.NivelLog.INFO);
    HashMap mapaBeneficiarios = new HashMap ();
    int    indice;
    StringBuffer encabezado = new StringBuffer ("");
    String tramaEntrada        = "";
    String tramaSalida         = "";
    String path_archivo        = "";
    String nombre_archivo      = "";
    String trama_salidaRedSrvr = "";
    String retCodeRedSrvr      = "";
    String registro            = "";
    String listaBeneficiarios  = "";

    //TuxedoGlobal tuxGlobal;

    String medio_entrega  = "2EWEB";
    String tipo_operacion = "CBEN";
    encabezado  = new StringBuffer (medio_entrega);
	encabezado.append ("|");
	encabezado.append (usuario);
	encabezado.append ("|");
	encabezado.append (tipo_operacion);
	encabezado.append ("|");
	encabezado.append (contrato);
	encabezado.append ("|");
    encabezado.append (usuario);
	encabezado.append ("|");
	encabezado.append (clavePerfil);
	encabezado.append ("|");
    tramaEntrada = contrato + "@";
    tramaEntrada = encabezado + tramaEntrada;
    EIGlobal.mensajePorTrace( "***ChesRecuperar.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace( "***ChesRecuperar.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
	try{
	   	//tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   	Hashtable hs = tuxGlobal.web_red(tramaEntrada);
       	tramaSalida = (String) hs.get("BUFFER");
	   	EIGlobal.mensajePorTrace( "***ChesRecuperar.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
	   }catch( java.rmi.RemoteException re){
	   	re.printStackTrace();
	   }catch(Exception e) {}

       path_archivo = tramaSalida;
	   nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());

	   EIGlobal.mensajePorTrace( "***ChesRegistro.class & nombre_archivo:" + nombre_archivo + " &", EIGlobal.NivelLog.INFO);

	   path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;

	   EIGlobal.mensajePorTrace( "***ChesRecuperar.class & path_archivo:" + path_archivo + " &", EIGlobal.NivelLog.INFO);

       boolean errorFile=false;
       try
         {
			// Copia Remota
			EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
			ArchivoRemoto archivo_remoto = new ArchivoRemoto();

			if(!archivo_remoto.copia( nombre_archivo ) )
			{
				// error al copiar
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &error de remote shell ...&", EIGlobal.NivelLog.INFO);
			}
			else
			{
				// OK
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			}

            File drvSua = new File(path_archivo);
            RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
            trama_salidaRedSrvr = fileSua.readLine();
            retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
            if (retCodeRedSrvr.equals("OK"))
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);
             {
               while((registro = fileSua.readLine()) != null){
               		/////
					if( registro.trim().equals(""))
		 				registro = fileSua.readLine();
					if( registro == null )
						break;
                  mapaBeneficiarios.put (EIGlobal.BuscarToken(registro, ';', 1), EIGlobal.BuscarToken(registro, ';', 2));
                }
             }
            fileSua.close();
         }catch(Exception ioeSua )
            {
				EIGlobal.mensajePorTrace( "***ChesRecuperar.class  Excepci�n en listaBenf() >>"+ioeSua.getMessage()+"<<",EIGlobal.NivelLog.ERROR);
               return mapaBeneficiarios;
            }

      EIGlobal.mensajePorTrace( "***ChesRecuperar.class  termina m�todo listaBenef()", EIGlobal.NivelLog.INFO);
      return mapaBeneficiarios;
  }


	public String BuscaBeneficiario(int totalBeneficiarios, String cveBeneficiario , String cadena){
		EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &inicia m�todo BuscaBeneficiario()&", EIGlobal.NivelLog.INFO);
		String buscaClave ="";
		String subcadena ="";
		String nombreBenef = "";
		//int i=0;

	for(int i=1; i<=totalBeneficiarios; i++) //cambio
	  {
		 buscaClave = EIGlobal.BuscarToken( EIGlobal.BuscarToken(cadena , ';' , i), '|' , 4);
		 if(cveBeneficiario.equals(buscaClave))
		  {
			 nombreBenef = EIGlobal.BuscarToken( EIGlobal.BuscarToken(cadena , ';' , i), '|' , 2); //cambio
			 break;
		  }
	  }


		/*
		while(! cveBeneficiario.equals(buscaClave)) {
				buscaClave = EIGlobal.BuscarToken( EIGlobal.BuscarToken(cadena , ';' , i), '|' , EIGlobal.NivelLog.INFO);
				subcadena  = EIGlobal.BuscarToken( EIGlobal.BuscarToken(cadena , ';' , i), '|' , 2);
				i++;
			}
		*/
			EIGlobal.mensajePorTrace( "***ChesRecuperar.class  &termina m�todo BuscaBeneficiario()&", EIGlobal.NivelLog.INFO);
		//return subcadena;
		return nombreBenef;
	}



}
