package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.RET_LiberaOperBO;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.RET_OperacionVO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


/**
 *  Clase servlet para realizar el liberaci�n de operaciones con la herramienta Reuters
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 04, 2012
 */
public class RET_LiberaOper extends BaseServlet{

	/**
	 * Objeto manejador RET_LiberaOperBO
	 */
	RET_LiberaOperBO manejador = new RET_LiberaOperBO();

    /**
     * Constante accion iniciar
     */
    private static final String ACCION_INICIAR = "iniciar";

    /**
     * Constante accion siguiente
     */
    private static final String ACCION_SIGUIENTE = "siguiente";

    /**
     * Constante accion anterior
     */
    private static final String ACCION_ANTERIOR = "anterior";

    /**
     * Constante accion siguiente
     */
    private static final String ACCION_ULTIMO = "ultimo";

    /**
     * Constante accion anterior
     */
    private static final String ACCION_PRIMERO = "primero";

    /**
     * Constante accion complementar
     */
    private static final String ACCION_COMPLEMENTAR = "complementar";

    /**
     * Constante accion insertar una cuenta
     */
    private static final String ACCION_INSERTAR_CUENTA = "insertarCuenta";

    /**
     * Constante accion limpiar
     */
    private static final String ACCION_LIMPIAR = "limpiar";

    /**
     * Constante accion dar de baja el registro
     */
    private static final String ACCION_BAJA_REGISTRO = "bajaRegistro";

    /**
     * Constante accion guardar
     */
    private static final String ACCION_REGISTRAR = "registrar";

    /**
     * Constante accion liberar
     */
    private static final String ACCION_TRANSFERIR = "transferir";

	/**
	 * DoGet
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		processRequest( req, res );
	 }

	/**
	 * DoPost
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		processRequest( req, res );
	 }

	/**
	 * Metodo principal para el manejo de la apertura del applet de liberaci�n de operaciones Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws ServletException		Exception de tipo ServletException
	 * @throws IOException			Exception de tipo IOException
	 */
	public void processRequest( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException{

		EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Entrando a la liberaci�n de operaciones Reuters.", EIGlobal.NivelLog.DEBUG);
		String opcion = null;
		opcion = (String) getFormParameter(req,"opcion");
		if (opcion == null) {
			opcion = ACCION_INICIAR;
			EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Opcion null, se asigna inicio" , EIGlobal.NivelLog.ERROR);
		}
		EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Opcion: " + opcion, EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if(SesionValida(req,res)){
			//*****************************************Inicia validacion OTP****************
			String valida = getFormParameter(req,"valida" );
			if(validaPeticion( req, res,session,sess,valida)){
				EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Entro a validar peticion", EIGlobal.NivelLog.INFO);
			}else{//*****************************************Termina validacion OTP**************************************
				EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Leyendo facultades.", EIGlobal.NivelLog.INFO);

				if(!verificaFacultad("COMPFXREUTER",req) &&
				   !verificaFacultad("LIBPFXREUTER",req)){

					manejador.paginaError("<p align=\"left\">Por el momento no cuenta con Facultades para operar <font color=green>FX Online</font>,<br>"
							+ ",le pedimos se ponga en contacto con su Ejecutivo de cuenta,<br>"
							+ "su Ejecutivo Cash &oacute; con su Asesor de Inversi&oacute;n,"
							+ "<br>quienes lo podr�n apoyar para contratar <font color=green>FX Online</font>.</p>", req, res);
				}else{

					/**
					 * Opcion Iniciar
					 */
					if(opcion.equals(ACCION_INICIAR)){
					   	try {
							manejador.iniciar(req, res);
					        EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando iniciar.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperCons.jsp", req, res);
						} catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Siguiente
					 */
					if(opcion.equals(ACCION_SIGUIENTE)){
						try{
							manejador.siguiente( req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando siguiente.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperCons.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Anterior
					 */
					if(opcion.equals(ACCION_ANTERIOR)){
						try{
							manejador.anterior( req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando anterior.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperCons.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Primero
					 */
					if(opcion.equals(ACCION_PRIMERO)){
						try{
							manejador.primero( req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando primero.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperCons.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Ultimo
					 */
					if(opcion.equals(ACCION_ULTIMO)){
						try{
							manejador.ultimo( req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando ultimo.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperCons.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Complementar
					 */
					if(opcion.equals(ACCION_COMPLEMENTAR)){
						try{
							manejador.complementar( req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando complementar.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperComp.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Insertar Cuenta
					 */
					if(opcion.equals(ACCION_INSERTAR_CUENTA)){
						try{
							manejador.insertarCuenta(req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando insertarCuenta.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperComp.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Limpiar
					 */
					if(opcion.equals(ACCION_LIMPIAR)){
						try{
							manejador.limpiar( req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando limpiar.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperComp.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Baja Registro
					 */
					if(opcion.equals(ACCION_BAJA_REGISTRO)){
						try{
							manejador.bajaRegistro(req, res);
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando bajaRegistro.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_liberaOperComp.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Guardar
					 */
					if(opcion.equals(ACCION_REGISTRAR)){
						try{
							if(manejador.validaDatos(req, res)){
								manejador.registrar( req, res);
								EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando registrar.", EIGlobal.NivelLog.INFO);
								evalTemplate("/jsp/RET_liberaOperDet.jsp", req, res);
							}else{
								EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Finalizando registrar.", EIGlobal.NivelLog.INFO);
								evalTemplate("/jsp/RET_liberaOperComp.jsp", req, res);
							}
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Enviar
					 */
					if(opcion.equals(ACCION_TRANSFERIR)){
						EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Antes de verificacion de token", EIGlobal.NivelLog.INFO);
						try{
							///////////////////--------------challenge--------------------------
							String validaChallenge = req.getAttribute("challngeExito") != null
								? req.getAttribute("challngeExito").toString() : "";

							EIGlobal.mensajePorTrace("--------------->RET_LiberaOper > validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

							if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
								EIGlobal.mensajePorTrace("--------------->RET_LiberaOper > Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
								//ValidaOTP.guardaParametrosEnSession(req);
								guardaParametrosEnSession(req);
								validacionesRSA(req, res);
								return;
							}

							//Fin validacion rsa para challenge
							///////////////////-------------------------------------------------

							//interrumpe la transaccion para invocar la validaci�n
							if(valida==null ){
								EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Se verifica si el usuario tiene token y si la "
										+ "transacci�n est� parametrizada para solicitar la validaci�n con el OTP.", EIGlobal.NivelLog.INFO);
								boolean solVal=ValidaOTP.solicitaValidacion(session.getContractNumber(),IEnlace.TRANSFERENCIAS_FX_ONLINE);

								if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP)
								   && session.getToken().getStatus() == 1 && solVal){
									//Bandera para guardar el token en la bit�cora
									req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
									EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: El usuario tiene Token. Solicit� la validaci�n "
											+ "Se guardan los parametros en sesion.", EIGlobal.NivelLog.INFO);
									guardaParametrosEnSession(req);
									//ValidaOTP.guardaParametrosEnSession(req);
									ValidaOTP.mensajeOTP(req, "TransFxOnline");
									ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
								}else{
									ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
	                                valida="1";
	                            }
							}
							//retoma el flujo
							if( valida!=null && valida.equals("1")){
								EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Se retoma el flujo ", EIGlobal.NivelLog.INFO);
								manejador.transferir(req,res);
								EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Fin de retoma el flujo ", EIGlobal.NivelLog.INFO);
								evalTemplate("/jsp/RET_liberaOperRes.jsp", req, res);
							}
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.ERROR);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}
				}
			}
		EIGlobal.mensajePorTrace("RET_LiberaOper::processRequest:: Saliendo de Libera Operaciones Reuters.", EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo encargado de guardar los parametros en la session
	 *
	 * @param request			Petici�n de entrada
	 */
	public void guardaParametrosEnSession(HttpServletRequest request){
		EIGlobal.mensajePorTrace("RET_LiberaOper::guardaParametrosEnSession::Dentro de guardaParametrosEnSession ", EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().removeAttribute("bitacora");

        HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();

			EIGlobal.mensajePorTrace("RET_LiberaOper::guardaParametrosEnSession:: opcion-> " + getFormParameter(request,"opcion"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_LiberaOper::guardaParametrosEnSession:: Modulo-> " + getFormParameter(request,"Modulo"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_LiberaOper::guardaParametrosEnSession:: nombreArchivoImp-> " + getFormParameter(request,"nombreArchivoImp"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_LiberaOper::guardaParametrosEnSession:: totalRegistros-> " + getFormParameter(request,"totalRegistros"), EIGlobal.NivelLog.INFO);

			String value;

			value = (String) getFormParameter(request,"opcion");
			if(value != null && !value.equals(null)) {
				tmp.put("opcion",getFormParameter(request,"opcion"));
			}

			value = (String) getFormParameter(request,"Modulo");
			if(value != null && !value.equals(null)) {
            	tmp.put("Modulo",getFormParameter(request,"Modulo"));
			}

			value = (String) getFormParameter(request,"nombreArchivoImp");
			if(value != null && !value.equals(null)) {
            	tmp.put("nombreArchivoImp",getFormParameter(request,"nombreArchivoImp"));
			}

			value = (String) getFormParameter(request,"totalRegistros");
			if(value != null && !value.equals(null)) {
            	tmp.put("totalRegistros",getFormParameter(request,"totalRegistros"));
			}

			RET_OperacionVO bean = (RET_OperacionVO) session.getAttribute("beanOperacionRET");
			RET_LiberaOperBO bo = new RET_LiberaOperBO();
			try{
				tmp.put("Cuentas", bean.getCtaCargo());
				tmp.put("ImporteT", bean.getImporteTotOper());
				tmp.put("CveDivisa", bean.getDivisaOperante());
			}catch (Exception e) {
				EIGlobal.mensajePorTrace("RET_LiberaOper::guardaParametrosEnSession:: Error al guardar parametros RET-> " + e.getMessage(), EIGlobal.NivelLog.INFO);
			}

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
        EIGlobal.mensajePorTrace("RET_LiberaOper::guardaParametrosEnSession:: Saliendo de guardaParametrosEnSession ", EIGlobal.NivelLog.INFO);
    }

 }