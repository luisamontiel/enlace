package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConsEdoCtaDetalleBean;
import mx.altec.enlace.beans.ConsEdoCtaResultadoBean;
import mx.altec.enlace.beans.ConsultaEdoCtaBean;
import mx.altec.enlace.beans.CuentaODD4Bean;
import mx.altec.enlace.beans.ODD4Bean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ConsultaEdoCtaBO;
import mx.altec.enlace.bo.EstadoCuentaHistoricoBO;
import mx.altec.enlace.bo.TrxMPPCBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class ConsultaEdoCtaServlet extends BaseServlet {
	/**
	 * serialVersion
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * SESSION_BR
	 */
	private static final String SESSION_BR = "session";
	
	/**
	 * NUM_PAGINA
	 */
	private static final String NUM_PAGINA = "numPag";
	
	/**
	 * NUM_PAGINA_DETALLE
	 */
	private static final String NUM_PAGINA_DETALLE = "numPagDet";
	
	/**
	 * RANGO
	 */
	private static final int RANGO = 20;
	
	/**
	 * TIPO_OPER
	 */
	private static final String TIPO_OPER = "S";
	
        /** Numero de byte que se van a estar leyendo para la generacion del archivo de exportacion */
	private static final int BYTES_DOWNLOAD = 1024;
        
        /** Constante para titulo de pantalla */
 	private static final String TITULO_PANTALLA = "Estados de Cuenta";
 	
 	/** Constante para posicion de menu */
 	private static final String POS_MENU = "Estados de Cuenta &gt; Modificar env&iacute;o de Estado de Cuenta &gt; Consulta";
        
	/** Constante mensaje de error por facultades */
	private static final String MSJ_ERROR_FAC = "Usuario sin facultades para operar con este servicio";
	
	/** Constante para archivo de ayuda **/
	private static final String ARCHIVO_AYUDA = "aEDC_ConConfig";
	
	/** Id de una operacion exitosa, ya no existen mas registros */
	private static final String OPERACION_EXITOSA = "AVODA0002";
	
	/** Id de operacion exitosa, existen mas registros */
	private static final String EXISTEN_MAS_REGISTROS = "AVPEA8001";
        
	/** Instancia para la bitacora admin **/
	private transient BitaAdminBean admin;
	
	/**
	 * Get
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaServlet-> Metodo DoGet", EIGlobal.NivelLog.DEBUG);
		defaultAction( req, res );
	}

	/**
	 * Post
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaServlet-> Metodo DoPost", EIGlobal.NivelLog.DEBUG);
		defaultAction( req, res );
	}
	
	/**
	 * Metodo ejecutado en caso de get o post
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

		String opcion= (String)req.getParameter("opcion");
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaServlet opcion " + opcion, EIGlobal.NivelLog.INFO);
                
                         
		if(opcion==null){
			opcion="0";
		}
                
                StringBuffer datesrvr = new StringBuffer();
                datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
                datesrvr.append(ObtenAnio()).append("\">");
                datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
                datesrvr.append(ObtenMes()).append("\">");
                datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
                datesrvr.append(ObtenDia()).append("\">");
                
                /** String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
                       datesrvr = datesrvr + "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
                       datesrvr = datesrvr + "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
                */
                req.setAttribute("Fechas",datesrvr.toString());
                
                /** datesrvr.delete(0, datesrvr.length()); */
                
		int op = Integer.parseInt(opcion);
	    boolean sesionvalida = SesionValida( req, res);
		if ( sesionvalida )
		 {
			if(session.getFacultad(BaseResource.FAC_CONS_EDO_CTA))

			{
				switch (op){
					case 0:
						iniciaConsulta(req, res);
						break;
					case 1:
						realizaConsulta(req, res);
						break;
					case 2:
						realizaConsultaDetalle(req, res);
						break;
					case 3:
						exportaTXT(req, res);
						break;
                                /*
								try {
                                    exportaTXT(req, res);
                                } catch (NamingException ex) {
                                    Logger.getLogger(ConsultaEdoCtaServlet.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (JMSException ex) {
                                    Logger.getLogger(ConsultaEdoCtaServlet.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (ValidationException ex) {
                                    Logger.getLogger(ConsultaEdoCtaServlet.class.getName()).log(Level.SEVERE, null, ex);
                                }*/
					case 4:
						exportaTXTTDC(req,res);
						break;
				}
			}
			else{
				despliegaPaginaErrorURL(MSJ_ERROR_FAC, TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, null, req,res);
			}
		 }

	}
	
	/**
	 * Inicia la consulta de la configuracion
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws java.io.IOException : manejo de excepcion
	 */
	public void iniciaConsulta(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaServlet-> Metodo iniciaConsulta", EIGlobal.NivelLog.DEBUG);
		
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
		
		generaMenu(req, session, POS_MENU);
		
        req.setAttribute ("anio",ObtenAnio ());
        req.setAttribute ("mes",ObtenMes ());
        req.setAttribute ("dia",ObtenDia ());
        req.setAttribute("diasInhabiles", diasInhabilesAnt());
        
        admin=new BitaAdminBean();
        admin.setTipoOp("");
        admin.setReferencia(100000);
        BitacoraBO.bitacorizaAdmin(req, sess, BitaConstants.CONSULTA_EDO_CUENTA, admin, boolLogin);
		
		evalTemplate("/jsp/consultaEdoCta.jsp", req, res);
	}
	
	/**
	 * Ejecuta la consulta de solicitud de configuacion
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws java.io.IOException : manejo de excepcion
	 */
	public void realizaConsulta(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaServlet-> Metodo realizaConsulta", EIGlobal.NivelLog.DEBUG);
		
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
		
		String folio = req.getParameter("folio");
		String usuarioSol = req.getParameter("usuarioSol");
		String fechaIni = req.getParameter("fechaIni");
		String fechaFin = req.getParameter("fechaFin");
		String estatus = req.getParameter("estatus");
                String descErr = req.getParameter("descErr");
		ConsultaEdoCtaBO edoCtaBO = new ConsultaEdoCtaBO();
		ConsultaEdoCtaBean edoCtaBean = new ConsultaEdoCtaBean();

		int numPag = req.getParameter(NUM_PAGINA)!=null?Integer.parseInt(req.getParameter(NUM_PAGINA)):1;
		int totalPaginas = 1;
		int regTotal = 0;
		
		edoCtaBean.setFolio(folio);
		edoCtaBean.setUsuarioSolicitnate(usuarioSol);
		edoCtaBean.setFechaInicio(fechaIni);
		edoCtaBean.setFechaFin(fechaFin);
		edoCtaBean.setEstatus(estatus);
                edoCtaBean.setDescErr(descErr);
                
		edoCtaBean.setContrato(session.getContractNumber());
                //C109836.
                EIGlobal.mensajePorTrace("Estatus recuperado ->" + edoCtaBean.getEstatus(),
					EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("Descripcion del Error ->" + edoCtaBean.getDescErr(),
					EIGlobal.NivelLog.INFO);

		edoCtaBean.setRegIni((numPag-1)*RANGO);
		edoCtaBean.setRegFin(numPag*RANGO);
		
		List<ConsEdoCtaResultadoBean> edoCtaList = edoCtaBO.consultaEdoCta(edoCtaBean);
		
		if(edoCtaList != null && !edoCtaList.isEmpty()){
			req.setAttribute("edoCtaList", edoCtaList);
			regTotal = edoCtaList.get(0).getTotalRegistros();
		}else{
			despliegaPaginaErrorURL("No existen datos disponibles para su consulta.","Estados de Cuenta","Estados de Cuenta &gt; Consulta Solicitud de Configuraci&oacute;n",ARCHIVO_AYUDA, "ConsultaEdoCtaServlet?opcion=0",req,res);
			return;
		}
		
		if(regTotal>RANGO){
			totalPaginas = regTotal/RANGO;
			if(regTotal%RANGO > 0){
				totalPaginas++;
			}
		}
		
		req.setAttribute("totalPaginas",totalPaginas); 	
		req.setAttribute("numPag",numPag);

		req.setAttribute("folioC", folio);
		req.setAttribute("usuarioSolC",usuarioSol);
		req.setAttribute("fechaIniC", fechaIni);
		req.setAttribute("fechaFinC", fechaFin);
		req.setAttribute("estatusC", estatus);
                req.setAttribute("descErrC", descErr);
		
		generaMenu(req, session, POS_MENU);
	
		new BitaAdminBean();
		admin.setTipoOp(TIPO_OPER);
		admin.setReferencia(100000);
		BitacoraBO.bitacorizaAdmin(req, sess, BitaConstants.CONSULTA_RES_EDO_CUENTA, admin, boolLogin); 
		
		evalTemplate("/jsp/consultaEdoCtaResult.jsp", req, res);
	}
	
	/**
	 * Realiza la consulta del detalle
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws java.io.IOException : manejo de excepcion
	 */
	public void realizaConsultaDetalle(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaServlet-> Metodo realizaConsultaDetalle", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
		ConsultaEdoCtaBO edoCtaBO = new ConsultaEdoCtaBO();                
		ConsultaEdoCtaBean edoCtaBean = new ConsultaEdoCtaBean();
                //C109836. Added test
                /** ConfEdosCtaIndBO confEdosCtaIndBO = new ConfEdosCtaIndBO(); */ 
                /** ConfEdosCtaIndBean confEdosCtaIndBean = null; */ 
                //End Test.
                
		String lineasArchivo = null;
		
		String idEdoCta = req.getParameter("idEdoCta");
		
		int numPag = req.getParameter(NUM_PAGINA_DETALLE)!=null?Integer.parseInt(req.getParameter(NUM_PAGINA_DETALLE)):1;
		int totalPaginas = 1;
		int regTotal = 0;
		String tipoEDC = "";
		
		edoCtaBean.setRegIni((numPag-1)*RANGO);
		edoCtaBean.setRegFin(numPag*RANGO);
		edoCtaBean.setFolio(idEdoCta);
		edoCtaBean.setContrato(session.getContractNumber());
                //C109836. Added test
		//confEdosCtaIndBean = confEdosCtaIndBO.mostrarCuentasPDF(edoCtaBean.getContrato());
                //End Test.
                
		if("exp".equals(req.getParameter("archivo"))){
			res.setContentType("text/plain");
			res.setHeader(
					"Content-Disposition", "attachment;filename=ConsultaSolicitudConfiguracion.txt");
			lineasArchivo = edoCtaBO.consultaEdoCtaDetalleTotal(edoCtaBean);
			InputStream input;
			
			try {
				input = new ByteArrayInputStream(lineasArchivo.getBytes("UTF8"));
				
				int read = 0;
				byte[] bytes = new byte[1024];
				OutputStream os = res.getOutputStream();

				while ((read = input.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
				os.flush();
				os.close();
				return;
			} catch (UnsupportedEncodingException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
			} catch (IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
			}
		}

		List<ConsEdoCtaDetalleBean> edoCtaListDetalle = edoCtaBO.consultaEdoCtaDetalle(edoCtaBean);
		
		if(edoCtaListDetalle != null && !edoCtaListDetalle.isEmpty() ){
			regTotal = edoCtaListDetalle.get(0).getTotalRegistros();
			tipoEDC = edoCtaListDetalle.get(0).getTipoEdoCta();
		}
		
		if(regTotal>RANGO){
			totalPaginas = regTotal/RANGO;
			if(regTotal%RANGO > 0){
				totalPaginas++;
			}
		}
		
		if ("001".equalsIgnoreCase(tipoEDC)) {
			req.setAttribute("Columna1", "Secuencia de domicilio");
			req.setAttribute("Columna2", "Cuenta");
			req.setAttribute("Columna3","Descripci&oacute;n de Cuenta");
		}else if ("003".equalsIgnoreCase(tipoEDC)) {
			req.setAttribute("Columna1", "Contrato");
			req.setAttribute("Columna2", "Cr&eacute;dito");
			req.setAttribute("Columna3","Descripci&oacute;n Cr&eacute;dito");
		}
		EIGlobal.mensajePorTrace("::::::::::::::::::::::::::::::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("tipoEDC=["+tipoEDC
				+"] Col1:["+req.getAttribute("Columna1")
				+"] Col2:["+req.getAttribute("Columna2")
				+"] Col3:["+req.getAttribute("Columna3")
				+"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("::::::::::::::::::::::::::::::", EIGlobal.NivelLog.INFO);
		
		req.setAttribute("edoCtaListDetalle", edoCtaListDetalle); 
		req.setAttribute("idEdoCta",idEdoCta);
		//req.setAttribute("tipoEDC", tipoEDC);
		
		req.setAttribute("totalPaginas",totalPaginas); 	
		req.setAttribute("numPagDet",numPag); 
		
		req.setAttribute("folioC", req.getParameter("folio"));
		req.setAttribute("usuarioSolC", req.getParameter("usuarioSol"));
		req.setAttribute("fechaIniC", req.getParameter("fechaIni"));
		req.setAttribute("fechaFinC", req.getParameter("fechaFin"));
		req.setAttribute("estatusC", req.getParameter("estatus"));
        req.setAttribute("descErrC", req.getParameter("descErr"));
		
		generaMenu(req, session, POS_MENU + " &gt; Detalle");
	
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			BitacoraBO.bitacoraTCT(req, sess, BitaConstants.CONSULTA_DET_EDO_CUENTA, 
					obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),
					"", "", BitaConstants.COD_ERR_PDF_XML,
					BitaConstants.CONCEPTO_CONSULTA_EDO_CUENTA);
		}
		
		new BitaAdminBean();
		admin.setTipoOp(TIPO_OPER);
		admin.setReferencia(100000);
		//BitacoraBO.bitacorizaAdmin(req, sess, BitaConstants.CONSULTA_DETALLE_EDO_CUENTA, admin);
                BitacoraBO.bitacorizaAdmin(req, sess, BitaConstants.CONSULTA_DETALLE_EDO_CUENTA, admin, boolLogin); 
		
		evalTemplate("/jsp/consultaEdoCtaDetalle.jsp", req, res);
	}

	/**
	 * generaMenu
	 * @param req : req
	 * @param session : session
         * @param headerVal : headerVal
	 */
	private void generaMenu(HttpServletRequest req, BaseResource session, String headerVal){
		
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Estados de Cuenta",headerVal,ARCHIVO_AYUDA,req));
                
		
	}
	
	/**
	 * Metodo que exporta archivo txt
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @throws ServletException : ServletException
	 */
	//private void exportaTXT(HttpServletRequest req, HttpServletResponse res) throws NamingException, JMSException, ValidationException {
	private void exportaTXT(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		
            String numeroCuenta = req.getParameter("numSecuencaHDN");
           // boolean ocurrioUnError = false;
		try {
			final EstadoCuentaHistoricoBO estadoCuentaBO = new EstadoCuentaHistoricoBO();
			final ODD4Bean odd4Bean = estadoCuentaBO.obtenerInformacionCliente(numeroCuenta);
			//final ODD4Bean odd4Bean = estadoCuentaBO.obtenerInformacionCliente("02900455302");
			// Se valida si la transaccion devolvio un codigo de operacion diferente de exitoso.
			if (!OPERACION_EXITOSA.equals(odd4Bean.getCodigoOperacion())
					&& !EXISTEN_MAS_REGISTROS.equals(odd4Bean.getCodigoOperacion())) {
				EIGlobal.mensajePorTrace(String.format("Se muestra pagina error debido al codigo de operacion recibido por la transaccion ODD4: [%s]", odd4Bean.getCodigoOperacion()), NivelLog.DEBUG);
				despliegaPaginaErrorURL(odd4Bean.getMensajeOperacion(), TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, null, req,res);
				return;
			}
			
			res.setContentType("text/plain");
			res.setHeader(
					"Content-Disposition", 
					String.format("attachment;filename=Cuentas%s.txt", numeroCuenta));
			
			final StringBuilder tramas = new StringBuilder();
			if ("ERODE0065".equals(odd4Bean.getCodigoOperacion()) || odd4Bean.isTieneProblemaDatos()) { 
				tramas.append("Por incongruencia de datos en la secuencia de domicilio no fue posible obtener el total de cuentas");
				tramas.append("\n");
			}
			for(CuentaODD4Bean bean : odd4Bean.getCuentas()){
				if(bean.getNumeroCuenta().length() == 12) {
					if(bean.getNumeroCuenta().startsWith("00")) {
						bean.setNumeroCuenta(bean.getNumeroCuenta().replaceFirst("00", "0"));
					} else if(bean.getNumeroCuenta().startsWith("0")) {
						bean.setNumeroCuenta(bean.getNumeroCuenta().replaceFirst("0", ""));
					} else if (bean.getNumeroCuenta().startsWith("B") && !bean.getNumeroCuenta().startsWith("BME")) {
						bean.setNumeroCuenta(bean.getNumeroCuenta().replaceFirst("B", "BME"));
					}
				}
				tramas.append(bean.getNumeroCuenta());
				tramas.append("\n");
			}
			
			InputStream input = 
					new ByteArrayInputStream(tramas.toString().getBytes("UTF8"));
			
			int read = 0;
			byte[] bytes = new byte[BYTES_DOWNLOAD];
			final OutputStream os = res.getOutputStream();

			while ((read = input.read(bytes)) != -1) {
				os.write(bytes, 0, read);
			}
			os.flush();
			os.close();
		} catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			//ocurrioUnError = true;
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			//ocurrioUnError = true;
		} catch (BusinessException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			//ocurrioUnError = true;
		}

	}
	
	/**
	 * Metodo que exporta a un archivo de texto las tarjetas asociadas al contrato TDC
	 * @author FSW - Indra
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : ServletException
	 */
	private void exportaTXTTDC(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		String contrato20Pos= (request.getParameter("numSecuencaHDN")!=null) ? request.getParameter("numSecuencaHDN"): " " ;
		EIGlobal.mensajePorTrace("contrato20Pos : ["+contrato20Pos+"]", 
				EIGlobal.NivelLog.DEBUG);
		try {
			TrxMPPCBO trxMPPC = new TrxMPPCBO();
			trxMPPC.consultaTarjetas(contrato20Pos);
			
			response.setContentType("text/plain");
			response.setHeader(	"Content-Disposition", 
								String.format("attachment;filename=Tarjetas%s.txt", contrato20Pos));
			final StringBuilder tramas = new StringBuilder();
			//tramas.append(contrato20Pos);
			for (int i=0;i<trxMPPC.getTarjetas().size();i++){
				tramas.append(trxMPPC.getTarjetas().get(i));
				tramas.append("\n");
			}
			InputStream input = 
				new ByteArrayInputStream(tramas.toString().getBytes("UTF8"));
			int read = 0;
			byte[] bytes = new byte[BYTES_DOWNLOAD];
			final OutputStream os = response.getOutputStream();

			while ((read = input.read(bytes)) != -1) {
				os.write(bytes, 0, read);
			}
			os.flush();
			os.close();
			
		}catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} 		
	}
}
