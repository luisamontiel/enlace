package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.*;
import java.math.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.lang.reflect.*;


public class consultaComisiones extends BaseServlet
	{
	short sucursalOpera = (short)787;

	//=================================================================================================
	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	//=================================================================================================
	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	//=================================================================================================
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)	throws ServletException, IOException
		{
		String Trama = "";
		String Result = "";
		String usuario = "";
		String contrato = "";
		String clavePerfil = "";
		String[][] arrayCuentasCargo = null;

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("ConComision - execute(): Entrando a Consultas de Dep. Inter.", EIGlobal.NivelLog.INFO);

		if(SesionValida(req, res))
			{
			// Variables de sesion
			usuario         = session.getUserID8();
			contrato        = session.getContractNumber();
			clavePerfil     = session.getUserProfile();
			sucursalOpera   = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);


			// modificacion para integracion arrayCuentasCargo=ContCtasRelacpara_mis_transferencias(contrato);

			generaTablaComisiones(arrayCuentasCargo,Result,Trama,clavePerfil, usuario,contrato, req, res );
			}
		else
			{
			//despliegaMensaje(String mensaje, String param1, String param2, req, res);
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, req, res);
			}

		EIGlobal.mensajePorTrace("ConComision - execute(): Saliendo de Consultas de Dep. Inter.", EIGlobal.NivelLog.INFO);
		}


	/*************************************************************************************/
	/**************************************************** Regresa el total de paginas    */
	/*************************************************************************************/
	int calculaPaginas(int total,int regPagina)
		{
		return total/regPagina + ((total%regPagina != 0)?1:0);
		/*
		Double a1=new Double(total);
		Double a2=new Double(regPagina);
		double a3=a1.doubleValue()/a2.doubleValue();
		Double a4=new Double(total/regPagina);
		double a5=a3/a4.doubleValue();
		int totPag=0;

		if(a3<1.0)
			totPag=1;
		else
			{
			if(a5>1.0)
				totPag=(total/regPagina)+1;
			if(a5==1.0)
				totPag=(total/regPagina);
			}
		return totPag;
		*/
		}

	//=================================================================================================
	//#################################################################################################
	//=================================================================================================
	public int generaTablaComisiones( String[][] arrayCuentasCargo,String Result,String Trama, String clavePerfil, String usuario, String contrato, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		String totales          = "";
		String botones          = "";
		String mensajeError     = "Error inesperado.";
		String tipoOperacion    = "COMI"; //servicio de consulta de comisiones cobradas
		String tablaRegistros   = "";

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this );

		boolean comuError=false;

		String strPagina      = (String)req.getParameter("Pagina");
		String strRegPagina   = (String)req.getParameter("Numero");
		if(strPagina == null) strPagina = "0";
		if(strRegPagina == null) strRegPagina = "300";

		int fin         = 0;
		int pagina      = Integer.parseInt(strPagina);
		int regPagina   = Integer.parseInt(strRegPagina);

		//================================= llamada al servicio ===========================================

		ServicioTux TuxGlobal = new ServicioTux();
		//TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		// trama de env�o 2<MEDIO_ENTREGA>|<USUARIO>|COMI|<CONTRATO>|<USUARIO>|<PERFIL>|

		Trama = IEnlace.medioEntrega2   + "|" +
				usuario                 + "|" +
				tipoOperacion           + "|" +
				contrato                + "|" +
				usuario                 + "|" +
				clavePerfil             + "|" ;

		EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

		try
			{
			Hashtable hs = TuxGlobal.web_red(Trama);
			Result= ""+ (String) hs.get("BUFFER") +"" ;
			EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
			}
		catch( java.rmi.RemoteException re )
			{re.printStackTrace();}
		catch(Exception e)
			{;}
		//TODO  BIT CU1161
		/*
		 * 09/ENE/07
		 * VSWF-BMB-I
		 */
		  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
        	if(req.getParameter(BitaConstants.FLUJO)!=null){
        		bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
        	}else{
        		bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
        	}
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EC_INFORME_ENTRA);
			if(contrato!=null){
				bt.setContrato(contrato);
			}
			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		  }
		/*
		 * 02/ENE/07
		 * VSWF-BMB-F
		 */

		//========================== archivo del servicio =========================================

		String arcLinea         = "";
		String cadenaTCT        = "";
		String cuentaCargo      = "";
		String cadenaArchivo    = "";
		String nombreArchivo    = "";

		int     totalLineas     = 0;
		boolean status          = false;

		nombreArchivo           = IEnlace.LOCAL_TMP_DIR+"/"+usuario;
		//nombreArchivo           = IEnlace.LOCAL_TMP_DIR+"/comcob";// Simulando archivo del servicio

		//================================ Copia Remota ===========================================

		ArchivoRemoto archR = new ArchivoRemoto();

		if(!archR.copia(usuario))
			EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);

		//=========================================================================================

		EI_Tipo     TCTArchivo  = new EI_Tipo(this);
		EI_Exportar ArcEnt      = new EI_Exportar(nombreArchivo);

		if(ArcEnt.abreArchivoLectura())
			{
			boolean noError=true;
			do
				{
				arcLinea=ArcEnt.leeLinea();
				if(totalLineas==0)
					if((arcLinea.substring(0,2).equals("OK") || arcLinea.substring(16,24).equals("ENCO0000")))
						status=true;
				if(!arcLinea.equals("ERROR"))
					{
					if(arcLinea.substring(0,5).trim().equals("ERROR")) break;
					if (totalLineas == 1) cuentaCargo = arcLinea.substring(0,11).trim();
					totalLineas++;
					if(totalLineas>=3) cadenaTCT += arcLinea.substring(0,arcLinea.length()-1)+" @";
					}
				else noError=false;
				}
			while(noError);
			ArcEnt.cierraArchivo();
			if(arcLinea.substring(0,5).trim().equals("ERROR") && totalLineas<=1 && !status)
				{
				comuError       = true;
				mensajeError    = "Su transacci�n no puede ser atendida. Por favor intente mas tarde.";
				}
			}
		else
			{
			comuError       = true;
			mensajeError    = "Problemas de comunicacion. Por favor intente mas tarde.";
			}

		/********************************************************************************/

		if(!comuError)
			{
			TCTArchivo.iniciaObjeto(';','@',cadenaTCT);
			if(TCTArchivo.totalRegistros>=1)
				{
				int totalPags = calculaPaginas(TCTArchivo.totalRegistros,regPagina);


				botones+="\n <br>";
				botones+="\n <table border=0 align=center>";
				botones+="\n  <tr>";
				botones+="\n   <td align=center class='texfootpagneg'>";
				if(pagina>0) botones+="&lt; <a href='javascript:BotonPagina(" + (pagina-1) + ");' class='texfootpaggri'>Anterior</a>&nbsp;";

				if(totalPags>=2)
					{
					for(int i=0;i<totalPags;i++)
						if(pagina==i)
							botones+="&nbsp;" + (i+1) + "&nbsp;";
						else
							botones+="&nbsp;<a href='javascript:BotonPagina(" + i + ");' class='texfootpaggri'>" + (i+1) + "</a>&nbsp;";
					}

				if(pagina+1 < totalPags/*TCTArchivo.totalRegistros>((pagina+1)*regPagina)*/)
					{
					botones+="<a href='javascript:BotonPagina(" + (pagina+1) + ");' class='texfootpaggri'>Siguiente</a> &gt;";
					fin=regPagina;
					}
				else
					fin=TCTArchivo.totalRegistros-(pagina*regPagina);

				botones+="\n   </td>";
				botones+="\n  </tr>";
				botones+="\n  <tr>";
				botones+="\n    <td align=center><br><a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0></a></td>";
				botones+="\n  </tr>";
				botones+="\n </table>";


		/************************************************* Encabezado de la pagina ... */
				tablaRegistros+="\n <table border=0 cellpadding=2 cellspacing=3 align=center>";
				tablaRegistros+="\n <tr>";
				tablaRegistros+="\n <td>";

				tablaRegistros+="\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>";
				tablaRegistros+="\n  <tr>";
				tablaRegistros+="\n    <td class='textabref'><b>Cuenta de Cargo: <font color=red>"+cuentaCargo+"</font>&nbsp;</td>";
				tablaRegistros+="\n    <td align=right class='textabref'>&nbsp;</b></td>";	//Modificaciones comerciales a Enlace gsv

				tablaRegistros+="\n  </tr>";
				tablaRegistros+="\n  <tr>";
				tablaRegistros+="\n    <td align=left class='textabref'><b>Fecha de Aplicaci&oacute;n: <font color=red>"+ TCTArchivo.camposTabla[0][1] +"</font></td>";	//Modificaciones comerciales a Enlace gsv
				tablaRegistros+="\n    <td align=right class='textabref'>&nbsp;</td>";		//Modificaciones comerciales a Enlace gsv
				tablaRegistros+="\n  </tr>";
				tablaRegistros+="\n </table>";

				EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Desplegando tabla TCTArchivo parametros: "+Integer.toString(pagina*regPagina)+","+Integer.toString(fin), EIGlobal.NivelLog.INFO);

				tablaRegistros+="\n </td>";
				tablaRegistros+="\n </tr>";
				tablaRegistros+="\n <tr>";
				tablaRegistros+="\n <td>";

				tablaRegistros+=seleccionaRegistrosComisiones(pagina*regPagina,fin,TCTArchivo);

				tablaRegistros+="\n </td>";
				tablaRegistros+="\n </tr>";
				tablaRegistros+="\n </table>";

				req.setAttribute("TotalTrans",TCTArchivo.strOriginal);
				req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
				req.setAttribute("Botones",botones);
				req.setAttribute("Registros",tablaRegistros);
				req.setAttribute("Numero",Integer.toString(regPagina));
				req.setAttribute("Pagina",Integer.toString(pagina));
				req.setAttribute("Modulo","1");

				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Informe de Comisiones","Consultas &gt; Informe de Comisiones","s28000h", req));	//Modificaciones comerciales a Enlace gsv
				req.setAttribute("NumContrato",session.getContractNumber());
				req.setAttribute("NomContrato",session.getNombreContrato());
				req.setAttribute("NumUsuario",session.getUserID8());
				req.setAttribute("NomUsuario",session.getNombreUsuario());

				//TODO  BIT CU1161
				/*
				 * 09/ENE/07
				 * VSWF-BMB-I
				 */
				  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
	        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EC_INFORME_CONS_COMISIONES_COB);
				if(session.getContractNumber()!=null){
					bt.setContrato(session.getContractNumber());
				}
				if(tipoOperacion!=null){
					bt.setServTransTux(tipoOperacion.trim());
				}
				 if(Result != null){
						if(Result.substring(0,2).equals("OK")){
							bt.setIdErr(tipoOperacion.trim()+"0000");
						}else if(Result.length() >8){
							bt.setIdErr(Result.substring(0,8));
						}else {
							bt.setIdErr(Result.trim());
						}
					 }
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
				  }
				/*
				 * 02/ENE/07
				 * VSWF-BMB-F
				 */

				evalTemplate("/jsp/consultaComisiones.jsp", req, res);
				}
			else
				{
				EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): No existen Registros.", EIGlobal.NivelLog.INFO);
				this.despliegaMensaje("No se encontraron movimientos de Comisiones Cobradas realizadas.", "Informe de Comisiones","Consultas &gt; Informe de Comisiones", req, res);
				//despliegaPaginaError("No se encontraron movimientos de Comisiones Cobradas realizadas.","Consulta de Comisiones Cobradas","Consultas &gt; Comisiones &gt; Comisiones Cobradas", req, res);
				}
			}
		else
			{
			EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Error."+mensajeError, EIGlobal.NivelLog.INFO);
			this.despliegaMensaje(mensajeError, "Informe de Comisiones","Consultas &gt; Informe de Comisiones", req, res);
			//despliegaPaginaError(mensajeError,"Informe de Comisiones","Consultas &gt; Comisiones &gt; Comisiones Cobradas", req, res );
			}
		return 1;
		}


	/*************************************************************************************/
	/********************************* tabla de movimientos seleccionados por intervalo  */
	/*************************************************************************************/
	String seleccionaRegistrosComisiones(int ini, int fin, EI_Tipo Trans)
		{
		String strTabla="";
		String colorbg="";
		String centrado  ="";
		String derecha   ="";
		String izquierda ="";
		String param="";

//		String str="";
//		String strComprobante="";

		if(Trans.totalRegistros>=1)
			{
			int c[]={1,0,2,3,4};

/*
	0- Descripci�n comisi�n
	1- Fecha
	2- N�mero de operaciones
	3- Monto operado
	4- Monto comision

	Fecha de aplicacion
	Tipo de comision
	No. de operaciones
	Tarifa unitaria
	Monto de comision
*/

			strTabla+="\n<table border=0 align=center>";

			strTabla+="\n<tr>";
			//gs 24 09 - strTabla+="\n<th class='tittabdat'>&nbsp; Fecha de aplicaci&oacute;n &nbsp; </th>";
			//gs 24 09 - strTabla+="\n<th class='tittabdat'>&nbsp; Tipo de comisi&oacute;n &nbsp; </th>";
			strTabla+="\n<th class='tittabdat'>&nbsp; Operaci&oacute;n &nbsp; </th>";
			strTabla+="\n<th class='tittabdat'>&nbsp; No. Operaciones &nbsp; </th>";
			strTabla+="\n<th class='tittabdat'>&nbsp; Tarifa unitaria &nbsp; </th>";
			strTabla+="\n<th class='tittabdat'>&nbsp; Comisiones &nbsp; </th>"; //Modificaciones comerciales a Enlace gsv
			strTabla+="\n</tr>";

			EIGlobal.mensajePorTrace("ConComision - seleccionaRegistrosComisiones(): Generando la Tabla.", EIGlobal.NivelLog.INFO);
			if(fin>Trans.totalRegistros) fin=Trans.totalRegistros;
			EIGlobal.mensajePorTrace("ConComision - seleccionaRegistrosComisiones(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);
//			EnlaceGlobal.formateaImporte(Trans, EIGlobal.NivelLog.DEBUG);
//			EnlaceGlobal.formateaImporte(Trans, EIGlobal.NivelLog.INFO);
			centrado    =" align=center";
			izquierda   =" align=left";
			derecha     =" align=right";

			for(int i=ini;i<(ini+fin);i++)
				{
				if((i%2)==0) colorbg="class='textabdatobs'"; else colorbg="class='textabdatcla'";
				strTabla+="\n<tr>";

				double cantidad = extraeNumero(Trans.camposTabla[i][4]);
				Trans.camposTabla[i][4] = formateaNumero(cantidad);
				for(int j=1;j<5;j++)   //Modificaciones comerciales a Enlace gsv
					{
					if (j==2)            param = colorbg + centrado;
					else if (j==3||j==4) param = colorbg + derecha;
					else                 param = colorbg ;

//					EIGlobal.mensajePorTrace("ConComision - seleccionaRegistrosComisiones(): valor del registro " +Integer.toString(j)+" = "+Trans.camposTabla[i][c[j]], EIGlobal.NivelLog.DEBUG);

					strTabla+="<td "+param+"> &nbsp; "+Trans.camposTabla[i][c[j]]+" &nbsp; </td>";

					}
				strTabla+="\n</tr>";
				}

			if(ini+fin==Trans.totalRegistros)
				{
					String strTotal[] = calculaTotales(Trans);
					colorbg=(colorbg.equals("class='textabdatobs'"))?"class='textabdatcla'":"class='textabdatobs'";
					strTabla += "<TR><TD></TD>" +
						"<TD class='tittabdat' align=center>" + strTotal[0] + "</TD>" +
						"<TD class='tittabdat' align=center>* Subtotal</TD>" +
						"<TD class='tittabdat' align=right>" + strTotal[1] + "&nbsp;&nbsp;&nbsp;</TD>" +
						"</TR>";
					/*
						Se elimina el IVA y el total por requerimiento del usuario 08 Dic 2003

					strTabla += "<TR><TD colspan=2></TD><TD class='tittabdat' align=center>IVA</TD>" +
						"<TD  class='tittabdat'" + derecha + ">" + strTotal[2] + "&nbsp;&nbsp;&nbsp;</TD>" + "<TD></TD></TR>";
					strTabla += "<TR><TD colspan=2></TD><TD class='tittabdat' align=center>Total</TD>" +
						"<TD  class='tittabdat'" + derecha + ">" + strTotal[3] + "&nbsp;&nbsp;&nbsp;</TD>" + "<TD></TD></TR>";
					*/

					strTabla += "<TR><TD colspan=4 align=left class='textabref'>Esta consulta presenta &uacute;nicamente el informe de comisiones <br>del mes inmediato anterior</TD></TR>";
					strTabla += "<TR><TD colspan=4 align=left class='textabref'>* Importe de Comisiones m�s IVA</TD></TR>";
				}
			strTabla+="\n <tr>";
			strTabla+="\n</table>";
			}



		return strTabla;
		}


	// --- Calcula los totales de la tabla
	String[] calculaTotales(EI_Tipo x)
		{
		int pos=0, total, totalOps;
		double totalMonto;
		String[] totales={"","","",""};

		total = x.totalRegistros;
		totalOps = 0;
		totalMonto = 0.0;
		BigDecimal big_subtotal, big_iva;
		String str_iva="";

	try
	{
		for(pos=0;pos<total;pos++)
			{
			//Modificaciones comerciales a Enlace gsv
			/*
			Las comisiones con clave 13 y 20 no se suman al total de operaciones ya que son
			la anualidad y la comision minima
			*/
			if (!x.camposTabla[pos][5].trim().equals("13") && !x.camposTabla[pos][5].trim().equals("20"))
				totalOps += Integer.parseInt(x.camposTabla[pos][2].trim());
			totalMonto += extraeNumero(x.camposTabla[pos][4]);
			EIGlobal.mensajePorTrace(" -> posicion= "+pos, EIGlobal.NivelLog.INFO);
			}

		big_subtotal=new BigDecimal(totalMonto);
		totales[0] = "" + totalOps;		//total de operaciones
		totales[1] = formateaNumero(totalMonto);	//subtotal de comisiones
		big_iva = big_subtotal.multiply(new BigDecimal(".15"));
		str_iva=formateaNumero(big_iva.floatValue());
		totalMonto=totalMonto + extraeNumero(str_iva);
		totales[2] = str_iva;	//iva
		totales[3] = formateaNumero(totalMonto);	//total de comisiones

	}catch (Exception e)
		{
			EIGlobal.mensajePorTrace("\n\nError en calculaTotales : "+e, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("clave= '"+x.camposTabla[pos][5]+"'", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("num   oper= "+x.camposTabla[pos][2], EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("monto oper= "+x.camposTabla[pos][4]+"\n\n", EIGlobal.NivelLog.INFO);
			//despliegaMensaje("Por favor intente mas tarde", "Informe de Comisiones","Consultas &gt; Informe de Comisiones", req, res);
			//totales[0]= "error";
		}

		return totales;
		}

	//--- Extrae una cantidad num�rica de una cadena de texto que representa un valor monetario
	double extraeNumero(String x)
		{
		int car;
		double resultadoNum=0;

		while((car = x.indexOf("$")) != -1) x = x.substring(0,car) + x.substring(car+1);
		while((car = x.indexOf(" ")) != -1) x = x.substring(0,car) + x.substring(car+1);
		while((car = x.indexOf(",")) != -1) x = x.substring(0,car) + x.substring(car+1);
		try
		{
		resultadoNum = Double.parseDouble(x);
		}
		catch (Exception e)
		{
			EIGlobal.mensajePorTrace("\n\nError en extraeNumero: "+e, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("valor : " + x, EIGlobal.NivelLog.INFO);

		}
		return resultadoNum;
		}

	//--- Formatea un n�mero para representar un valor monetario
	String formateaNumero(double x)
		{
		int car;
		String cantidad;

		cantidad = "" + x;
		if((car = cantidad.indexOf("E")) != -1)
			{
			int exp = Integer.parseInt(cantidad.substring(car+1));
			cantidad = cantidad.substring(0,car);
			car = cantidad.indexOf(".");
			cantidad = cantidad.substring(0,car) + cantidad.substring(car+1);
			car += exp;
			while(cantidad.length()<car+1) cantidad += "0";
			cantidad = cantidad.substring(0,car) + "." + cantidad.substring(car);
			}
		car = cantidad.indexOf(".");
		while(car+3 < cantidad.length()) cantidad = cantidad.substring(0,cantidad.length()-1);
		while(cantidad.length() < car+3) cantidad += "0";

		for(int a=cantidad.length()-6;a>0;a-=3) cantidad = cantidad.substring(0,a) + "," + cantidad.substring(a);
		cantidad = "$" + cantidad;

		return cantidad;
		}

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	public void despliegaMensaje(String mensaje, String param1, String param2, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{

		// parche de Ram�n Tejada
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal( session , getServletContext() , this );

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		request.setAttribute( "URL", "javascript:history.go(-1)" );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s28000h", request));
		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		sess.setAttribute( "Encabezado", null);
		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}

	//=================================================================================================

	}