/*
 * GetAMancomunidad.java
 *
 * Created on 5 de marzo de 2003, 11:35 AM
 */

package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosManc;
import mx.altec.enlace.bo.NomPreBusquedaAction;
import mx.altec.enlace.bo.pdTokenizer;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


import java.sql.SQLException;
import java.util.*;
import java.io.*;

//VSWF


/**
 *
 * @author  Horacio Oswaldo Ferro D&iacute;az
 * @version 1.0
 * Servlet para realizar consultas de Mancomunidad usando el servicio de Tuxedo
 */
public class GetAMancomunidad extends BaseServlet {

	/** next */
    public static final int NEXT = 1;
    /** prev */
    public static final int PREV = 2;
    /** regs pag */
    public static final int REGS_PAG = 50;
    /** SESSION */
    public static final String SESSION = "session";

    /** Initializes the servlet.
     * @param config config
     * @throws ServletException ServletException
     */
    public void init (ServletConfig config) throws ServletException {
        super.init (config);

    }

    /** Destroys the servlet.
     */
    public void destroy () {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        if (SesionValida (request, response)) {
            BaseResource session =
                (BaseResource) request.getSession ().getAttribute (SESSION);

            request.getSession ().setAttribute ("MenuPrincipal", session.getStrMenu ());
            request.getSession ().setAttribute ("newMenu", session.getFuncionesDeMenu ());
            request.getSession ().setAttribute ("Encabezado", CreaEncabezado ("Consulta de Bit&aacute;cora de Mancomunidad",
                "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n",
                "s25990h", request));
            int opc = 0;
            try {
                opc = Integer.parseInt ((String) request.getParameter ("Opcion"));
            } catch (NumberFormatException ex) {
            }
            System.out.println ("Opcion: " + opc);
            switch (opc) {
                case 0: {
                    consulta (request, response);
                    break;
                }
                case 1: {
                    pagina (request, response);
                    break;
                }
            }
        }
    }

    /** Consulta de Mancomunidad
     * @param request request del servlet
     * @param response response del servlet
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    protected void consulta (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    	boolean bandPintaNL = false;
//    	int countNL = 0;
    	Vector msgCountNL = new Vector();
        request.setAttribute ("Fecha", ObtenFecha ());
        BaseResource session =
            (BaseResource) request.getSession ().getAttribute (SESSION);

        // Fechas de inicio y fin de consulta
        String fechaIni = request.getParameter ("fecha1");
        String fechaFin = request.getParameter ("fecha2");
        request.getSession ().setAttribute ("FechaIni", fechaIni);
        request.getSession ().setAttribute ("FechaFin", fechaFin);

        // Campo en el cual se hara la consulta
        String campoFecha = request.getParameter ("Registro");

        // Filtros por tipo de operaci�n
        String tipoOperacion = "" + (request.getParameter ("ChkTranf") == null ? "" : request.getParameter ("ChkTranf")) +
        	(request.getParameter ("ChkNomIn") == null ? "" : request.getParameter ("ChkNomIn")) +
        	(request.getParameter ("ChkNomLn") == null ? "" : request.getParameter ("ChkNomLn")) +
            (request.getParameter ("ChkInver") == null ? "" : request.getParameter ("ChkInver")) +
            (request.getParameter ("ChkInterb") == null ? "" : request.getParameter ("ChkInterb")) +
            (request.getParameter ("ChkCred") == null ? "" : request.getParameter ("ChkCred")) +
            (request.getParameter ("ChkServ") == null ? "" : request.getParameter ("ChkServ")) +
        	(request.getParameter ("ChkSUA") == null ? "" : request.getParameter ("ChkSUA"));
        if (tipoOperacion == null || tipoOperacion.length () == 0)
        {
            tipoOperacion = " ";
        }
        if (tipoOperacion.endsWith (","))
        {
            tipoOperacion = tipoOperacion.substring (0, tipoOperacion.length () - 1);
        }

        // Filtros por estatus de la operaci�n
        String estatus = "" +  (request.getParameter("ChkAceptadas") == null ? "" : request.getParameter ("ChkAceptadas")) +
            (request.getParameter ("ChkRechazadas") == null ? "" : request.getParameter ("ChkRechazadas")) +
            (request.getParameter ("ChkCanceladas") == null ? "" : request.getParameter ("ChkCanceladas")) +
            (request.getParameter ("ChkPendientes") == null ? "" : request.getParameter ("ChkPendientes")) +
            (request.getParameter ("ChkEjecutadas") == null ? "" : request.getParameter ("ChkEjecutadas")) +
            (request.getParameter ("ChkNoEjecutadas") == null ? "" : request.getParameter ("ChkNoEjecutadas"));
        if (estatus == null || estatus.length () == 0)
        {
            estatus = " ";
        }
        if (estatus.endsWith (","))
        {
            estatus = estatus.substring (0, estatus.length () - 1);
        }

        // Filtro por importe
        String importe = request.getParameter ("Importe");
        if (importe == null || importe.length () == 0)
        {
            importe = " ";
        }
        // Filtro por cuenta
        String cuenta = request.getParameter ("cuenta");
        if (cuenta != null && !cuenta.equals (""))
        {
            request.getSession ().setAttribute ("CuentaConsulta", cuenta);
        }
        if (cuenta.length () == 0)
        {
            cuenta = " ";
        }

        // Filtro por usuario
        String usuario = request.getParameter ("usuario");
        if (usuario == null || usuario.length () == 0)
        {
            usuario = " ";
        }

        // Folio de registro de mancomunidad
        String folioReg = request.getParameter ("Folio_Registro");
        if (folioReg == null || folioReg.length () == 0)
        {
            folioReg = " ";
        }

        ServicioTux servicio = new ServicioTux ();
        /*servicio.setContext (
            ((com.netscape.server.servlet.platformhttp.PlatformServletContext)
                getServletContext()).getContext());*/

        String trama = "2EWEB|" + session.getUserID () + "|COMA|" +
            session.getContractNumber () + '|' + session.getUserID () + '|' +
            session.getUserProfile () + '|';

        String tramaManc = '@' + fechaIni + '@' + fechaFin + '@';
        if (campoFecha.equals ("FCH_AUTORIZAC"))
        {
            tramaManc += "" + 'A';
        }
        else
        {
            tramaManc += "" + 'R';
        }
        tramaManc += "" + '@' + session.getContractNumber () + '@';
        tramaManc += tipoOperacion + '@' + estatus + '@' + importe + '@' + cuenta + '@' +
            usuario + '@' + folioReg + '@' + session.getUserID () + '@';

        trama+=tramaManc;

        request.getSession().setAttribute("tramaManc", tramaManc);


        Hashtable respuesta;
        try {
            respuesta = servicio.web_red (trama);
            String arch = (String) respuesta.get ("BUFFER");
            EIGlobal.mensajePorTrace("GetAMancomunidad:recibe -- BUFFER [" + respuesta.get ("BUFFER") + "] --" , EIGlobal.NivelLog.INFO);
            if (arch.equals ("")) {
                despliegaPaginaError ("Error al tratar de obtener la consulta", request, response);
            } else {

            	EIGlobal.mensajePorTrace("GetAMancomunidad:recibe - Inicia", EIGlobal.NivelLog.INFO);

            	//IF PROYECTO ATBIA1 (NAS) FASE II

            	boolean Respuestal = true;


                ArchivoRemoto recibeArch = new ArchivoRemoto();

               	if(!recibeArch.copiaCUENTAS(arch, Global.DIRECTORIO_LOCAL)){

    					EIGlobal.mensajePorTrace("*** GetAMancomunidad.consulta  No se realizo la copia remota:" + arch, EIGlobal.NivelLog.ERROR);
    					Respuestal = false;

    				}
    				else {
    				    EIGlobal.mensajePorTrace("*** GetAMancomunidad.consulta archivo remoto copiado exitosamente:" + arch, EIGlobal.NivelLog.DEBUG);

    				}
               	//*********************************************

                if (Respuestal) {
                    java.util.ArrayList resultados =
                        new java.util.ArrayList ();
                    File Archivo = new File(
                        arch);
                    java.io.BufferedReader reader = new java.io.BufferedReader (
                        new FileReader (Archivo));
                    String temp;
                    temp = reader.readLine ();
                    if (!temp.startsWith ("OK")) {
 //Getronics CP Mexico, Q25268 IKDA - INICIA
                        despliegaPaginaError ("Su transacci�n no puede ser atendida. Intente m�s tarde.","Consulta de Bit&aacute;cora de Mancomunidad",
                			      "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n", request, response);
                	//despliegaPaginaError ("No hay registros para su consulta", request, response);
//Getronics CP Mexico, Q25268 IKDA - FIN
                        return;
                    }
                    boolean vacio = true;
                    NomPreBusquedaAction npbAction = new NomPreBusquedaAction();
                    EIGlobal.mensajePorTrace ("GetAMancomunidad - va por los nombres LM1D", EIGlobal.NivelLog.INFO);
        			NomPreLM1D datosEmpleado = npbAction.obtenRelacionTarjetaEmpleado(session.getContractNumber (), null, null, null);
        			EIGlobal.mensajePorTrace ("DatosManc - regresa con los nombres LM1D: ["+datosEmpleado.getDetalle()+"]" , EIGlobal.NivelLog.INFO);
        			ArrayList resDatos = null;

        			if (datosEmpleado.isCodExito() && datosEmpleado.getDetalle() != null) {
        				resDatos = new ArrayList(datosEmpleado.getDetalle());
                        EIGlobal.mensajePorTrace("GetAMancomunidad.consulta -> PASA [" + resDatos.size() + "]", EIGlobal.NivelLog.INFO);
        			} else {
        				EIGlobal.mensajePorTrace ("No existen tarjetas para el contrato" , EIGlobal.NivelLog.INFO);
        			}
                    //System.out.println ("Antes de crear la lista de resultados");

					/*paral lacuenta cargo PRE*/
					llamado_servicioCtasInteg(IEnlace.MEnvio_pagos_nomina," "," "," ",session.getUserID8()+".ambci",request);
					DatosManc obj = null;
					int contadorLN=0;
					double importeLN=0;

                    while ((temp = reader.readLine ()) != null) {
                    	if(!temp.startsWith("|")) {
                    		String empresa = getEmpresasMS(temp, request);
                    		obj = new DatosManc (temp, session.getContractNumber (), resDatos, empresa);
                    		if(obj.getTipo_operacion().trim().equals("PNLI") && obj.getEstatus().trim().equals("P")){
                    			contadorLN++;
                    			importeLN+=Double.parseDouble(obj.getImporte());
	                    		if(verificaFacultad ( FACULTADNLN ,request) &&
                    				valCuenta(session.getContractNumber(),session.getUserID8(),session.getUserProfile(),obj.getCta_origen(),obj.getTipo_operacion()))
                    			bandPintaNL = true;
                    		else{
	                    		bandPintaNL = false;
                    			//countNL++;
                    			msgCountNL.add(obj.getFolio_registro());
                    		}
                    		}
                    		obj.setBandPintaNL(bandPintaNL);
                    		resultados.add(obj);
                    		vacio = false;
                    	}
                    	else {
                    		String salida[] = temp.substring(1).split("\\|");
        					EIGlobal.mensajePorTrace("GetAMancomunidad.consulta -> Registros [" + salida[0] + "]" +
        							"Importe [" + salida[1] + "]", EIGlobal.NivelLog.INFO);
        					EIGlobal.mensajePorTrace("GetAMancomunidad.consulta -> Salida [" + salida + "]", EIGlobal.NivelLog.INFO);
        					//request.getSession().setAttribute("countNL", countNL);
        					request.getSession().setAttribute("msgCountNL", msgCountNL);
                    		request.getSession().setAttribute("regManc", Integer.toString((Integer.parseInt(salida[0])-contadorLN)));
                    		request.getSession().setAttribute("impManc", Double.toString((Double.parseDouble(salida[1])-importeLN)));
//                    		EIGlobal.mensajePorTrace ("GetAMancomunidad.consulta -> registros sin facultad: "+countNL , EIGlobal.NivelLog.INFO);
                    		EIGlobal.mensajePorTrace("GetAMancomunidad.consulta -> Registros final [" + Integer.toString((Integer.parseInt(salida[0])-contadorLN)) + "]" +
        							"Importe final [" + Double.toString((Double.parseDouble(salida[1])-importeLN)) + "]", EIGlobal.NivelLog.INFO);
                    	}
                    }
                    EIGlobal.mensajePorTrace("GetAMancomunidad.consulta -> " + "contadorLN [" + contadorLN + "]" + "importeLN [" + importeLN + "]", EIGlobal.NivelLog.INFO);
                    if (vacio) {
//Getronics CP Mexico, Q25268 IKDA - INICIA
                    	despliegaPaginaError ("No hay registros para su consulta","Consulta de Bit&aacute;cora de Mancomunidad",
                			      "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n", request, response);
                        //despliegaPaginaError ("No hay registros para su consulta", request, response);
//Getronics CP Mexico, Q25268 IKDA - FIN
                        return;
                    }
                    else{
//                    	TODO BIT CU5011 (Consulta de Mancomunidad), BIT CU5031 (Consulta Macomunidad Internacional)
            			/*VSWF-HGG-I*/
                        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
            			BitaHelper bh = new BitaHelperImpl(request, session, request.getSession(false));

            			BitaTransacBean bt = new BitaTransacBean();
            			bt = (BitaTransacBean)bh.llenarBean(bt);

            			if(request.getSession()
            					.getAttribute(BitaConstants.SESS_ID_FLUJO)
            					.equals(BitaConstants.EA_MANCOM_CONS_AUTO))
            				bt.setNumBit(BitaConstants.EA_MANCOM_CONS_AUTO_CONS_CUENTAS_MANC);

            			if(request.getSession()
            					.getAttribute(BitaConstants.SESS_ID_FLUJO)
            					.equals(BitaConstants.EA_MANCOM_OPER_INTER))
            				bt.setNumBit(BitaConstants.EA_MANCOM_OPER_INTER_CONS_CUENTAS_MANCOM);

            			if (session.getContractNumber() != null) {
            				bt.setContrato(session.getContractNumber().trim());
            				bt.setNombreArchivo(session.getContractNumber() + ".doc");
            			}
            			if(arch != null){
            				if(arch.substring(0,2).equals("OK")){
            					bt.setIdErr("COMA0000");
            				}else if(arch.length() > 8){
            					bt.setIdErr(arch.substring(0,8));
            				}else{
            					bt.setIdErr(arch.trim());
            				}
            			}
            			bt.setServTransTux("COMA");

            			try {
            				BitaHandler.getInstance().insertBitaTransac(bt);
            			} catch (SQLException e) {
            				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			} catch (Exception e) {
            				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			}
                        }
            			/*VSWF-HGG-F*/
                    }
                    System.out.println ("Despues de crear la lista");
                    request.getSession ().setAttribute ("ResultadosMancomunidad", resultados);
                    request.getSession ().setAttribute ("IndexMancomunidad", new Integer (-REGS_PAG));
                    pagina (request, response);
                } else {
                    despliegaPaginaError ("Error al tratar de obtener la consulta", request, response);
                }
            }
        } catch (Exception ex) {
            despliegaPaginaError ("Error al tratar de obtener la consulta", request, response);
            EIGlobal.mensajePorTrace ("Error en consulta de Mancomunidad: " + ex.getMessage (), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
        }
    }

    /**
     * Obtiene ek nombre de la empresa micrositio
	 * @param temp temp
	 * @param request  request
	 * @return String empresa
	 */
	private String getEmpresasMS(String temp, HttpServletRequest request ) {
		//Carga empresas del Micrositio
		final String LOG_METODO = "GetAMancomunidad - getEmpresasMS - ";
		EIGlobal.mensajePorTrace(LOG_METODO + "inicio", EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession ();
		HashMap empresas = (HashMap) ses.getAttribute("empresasMicrositio");
		String descripcionEmpresa = "";
		String tipoOper = null;
		String concepto = null;
		String convenio = null;
		pdTokenizer tokenizador = new pdTokenizer (temp, ";", 2);
	    try {
			tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tipoOper = tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    tokenizador.nextToken ();
		    concepto = tokenizador.nextToken ();
		} catch (Exception e) {
            EIGlobal.mensajePorTrace (LOG_METODO + "Error al consultar empresas Micrositio: " + e.getMessage (), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
	    if (tipoOper != null && ("PMIC".equals(tipoOper) || "PMRF".equals(tipoOper))) { //Recuperar Empresas
	    	if (concepto.lastIndexOf('@') > concepto.indexOf('@')+1){
	    		convenio = concepto.substring(concepto.indexOf('@')+1, concepto.lastIndexOf('@')).trim();
	    		if (empresas == null) {
	    			empresas = new HashMap();
	    		}
		    	if (!empresas.containsKey(convenio) ) {
		    		EIGlobal.mensajePorTrace(LOG_METODO + "Busca descripci�n convenio: [" + convenio + "]", EIGlobal.NivelLog.INFO);
		    		try {
						descripcionEmpresa = consultaB750(convenio, request);
						if (descripcionEmpresa != null ) {
							empresas.put(convenio, descripcionEmpresa);
							ses.setAttribute("empresasMicrositio", empresas);
							EIGlobal.mensajePorTrace(LOG_METODO + "Carga descripci�n: ["
									+ convenio + "," + descripcionEmpresa + "]", EIGlobal.NivelLog.INFO);
						}
					} catch (ServletException se) {
						EIGlobal.mensajePorTrace (LOG_METODO + "ServletException: [" + se + "]", EIGlobal.NivelLog.INFO);
					} catch (IOException ioe) {
						EIGlobal.mensajePorTrace (LOG_METODO + "IOException: [" + ioe + "]", EIGlobal.NivelLog.INFO);					}
		    	}
		    	if ("".equals(descripcionEmpresa.trim())) {
		    		if (empresas != null && empresas.containsKey(convenio)) {
		    			descripcionEmpresa = (String) empresas.get(convenio);
		    			EIGlobal.mensajePorTrace(LOG_METODO + "Recupera de la lista descripci�n convenio: [" + convenio + "]", EIGlobal.NivelLog.INFO);
		    		}
		    	}
			}
	    	EIGlobal.mensajePorTrace(LOG_METODO + "descripcionEmpresa: [" + descripcionEmpresa + "]", EIGlobal.NivelLog.DEBUG);
	    }
		return descripcionEmpresa;
	}

	/** Metodo para realizar la paginaci&oacute;n de los resultados de la consulta
     * @param request Request del servlet
     * @param response Response del servlet
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    public void pagina (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        int index = 0;
        int opc = NEXT;
        try {
            index = ((Integer) request.getSession ().getAttribute ("IndexMancomunidad")).intValue ();
        } catch (Exception ex) {
        }
        try {
            opc = Integer.parseInt ((String) request.getParameter ("OpcPag"));
        } catch (Exception ex) {
        }
        switch (opc) {
            case NEXT: {
                if (!(index + REGS_PAG > ((ArrayList) request.getSession ().getAttribute (
                "ResultadosMancomunidad")).size ()))
                    index += REGS_PAG;
                break;
            }
            case PREV: {
                index -= REGS_PAG;
                if (index < 0) index = 0;
                break;
            }
        }
        boolean checaTodos = false;
        java.io.File archivo = new java.io.File (IEnlace.DOWNLOAD_PATH +
            ((BaseResource)
            request.getSession ().getAttribute (SESSION)).getContractNumber ()
            + ".doc");
        java.io.FileWriter writer = new java.io.FileWriter (archivo);
        java.util.ListIterator liResultados =
            ((java.util.ArrayList) request.getSession (
                ).getAttribute ("ResultadosMancomunidad")).listIterator (index);
        while (liResultados.hasNext ()) {
            DatosManc temp = (DatosManc) liResultados.next ();
            writer.write (temp.toString ());
        }
        writer.flush ();
        writer.close ();
        ArchivoRemoto archR = new ArchivoRemoto();
        if (!archR.copiaLocalARemoto(((BaseResource)
        request.getSession ().getAttribute (SESSION)).getContractNumber ()
        + ".doc", "WEB")) {
            EIGlobal.mensajePorTrace( "No se realizo la copia remota", EIGlobal.NivelLog.INFO);
        } else {
            request.getSession ().setAttribute ("ArchivoManc", "/Download/" +
                ((BaseResource)
                request.getSession ().getAttribute (SESSION)).getContractNumber ()
                + ".doc");
        }
        liResultados =
            ((java.util.ArrayList) request.getSession (
                ).getAttribute ("ResultadosMancomunidad")).listIterator (index);
        while (liResultados.hasNext ()) {
            DatosManc temp = (DatosManc) liResultados.next ();
            if (temp.pendiente ()){ checaTodos = true;}
        }
        request.getSession ().setAttribute ("SeleccionaTodos", new Boolean (checaTodos));
        request.getSession ().setAttribute ("IndexMancomunidad", new Integer (index));
        request.getRequestDispatcher ("/jsp/GetAMancomunidad.jsp").forward (request, response);
    }

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     * @return String description of the servlet
     */
    public String getServletInfo () {
        return "Consulta y Autorizacion Mancomunidad";
    }

	/**
	 * Metodo encargado de Validar la cuenta asociada al contrato
	 * @param contrato Numero de Contrato
	 * @param usuario Numero de usuario
	 * @param perfil Perfil de usuario
	 * @param cuenta Numero de Cuenta
	 * @param tipoOp Tipo operacion
	 * @return result Resultado esperado
	 */
	public boolean valCuenta(String contrato, String usuario, String perfil, String cuenta, String tipoOp)
	{
    	String datoscta[]	= null;
		boolean result = true;
		ValidaCuentaContrato valCtas = new ValidaCuentaContrato ();


		if("IN04".equals(tipoOp) || "PNLI".equals(tipoOp))
		{
			datoscta = valCtas.obtenDatosValidaCuenta(contrato,
													  usuario,
                                                      perfil,
                                                      cuenta,
                                                      IEnlace.MEnvio_pagos_nomina);
		}
		else if("INTE".equals(tipoOp)) {
			   datoscta = valCtas.obtenDatosValidaCuenta(contrato,
					                                  usuario,
                                                      perfil,
                                                      cuenta,
                                                      IEnlace.MCargo_transf_pesos);
		}
		else if("PNOS".equals(tipoOp)) {
			datoscta = BuscandoCtaInteg(cuenta,IEnlace.LOCAL_TMP_DIR + "/"+usuario+".ambci",IEnlace.MEnvio_pagos_nomina);
		}

		if(datoscta == null)
		{
		       result = false;
		}

		return result;
	}

	/**
	 * desentramaDatosEmpresa
	 * @param trama trama
	 * @param tokens tokens
	 * @param separador separador
	 * @return aEmpresa aEmpresa
	 */
	private String [] desentramaDatosEmpresa(String trama, int tokens, String separador) //Desentrama los datos que regresa por trama el TUBO al ejecutar la CDEM
	{
		String [] aEmpresa = new String [tokens];
		int indice = trama.indexOf(separador); // String el pipe, o ;
		EIGlobal.mensajePorTrace("GetAMancomunidad - desentramaDatosEmpresa() - Formateando arreglo de datos", EIGlobal.NivelLog.INFO);
		for(int i=0; i<tokens;i++){
			if(indice>0)
			aEmpresa [i] = trama.substring(0,indice);
			if(aEmpresa[i] == null)
				aEmpresa [i] = "	  ";
			trama = trama.substring(indice+1);
			indice = trama.indexOf(separador);
		}
		return aEmpresa;
	}

	/**
	 * consultaB750
	 * @param convenio convenio
	 * @param req req
	 * @return String convenio
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public String consultaB750(String convenio, HttpServletRequest req) throws ServletException, IOException{
		final String LOG_METODO = "GetAMancomunidad - consultaB750 - ";
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		String contrato = session.getContractNumber ();
		String usuario = req.getParameter ("usuario");
		String descripcionEmpresa = null;
		String camposEmpresa[]	= null;
		String trama_salida = "";
		String trama_entrada = "";
		String tipo_operacion = "CDEM";  // prefijo del servicio ( consulta datos empresa Pago en l�nea)
		String cabecera = "1EWEB"; // medio de entrega ( regresa trama)
		String perfil = "";
		String pipe = "|";
		int tokens = 13;
		perfil = session.getUserProfile();
		if (!convenio.trim().equals("")){
			try{
				trama_entrada	=	cabecera + "|"
								+	 usuario + "|"
								+ tipo_operacion + "|"
								+	contrato + "|"
								+	 usuario + "|"
								+	  perfil + "|"
								+		convenio;
				EIGlobal.mensajePorTrace(LOG_METODO + "trama de entrada [" + trama_entrada + "]", EIGlobal.NivelLog.DEBUG);
				ServicioTux tuxedoGlobal = new ServicioTux();
				try{
					Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
					trama_salida = (String) hs.get("BUFFER");
				}catch( java.rmi.RemoteException re ){
					EIGlobal.mensajePorTrace(LOG_METODO + "RemoteException: ["+ re + "]", EIGlobal.NivelLog.INFO);
				}catch(Exception e) {
					EIGlobal.mensajePorTrace(LOG_METODO + "Exception: ["+ e + "]", EIGlobal.NivelLog.INFO);
				}
				if(trama_salida==null || trama_salida.equals("null"))
					trama_salida="";
				EIGlobal.mensajePorTrace(LOG_METODO + "Trama salida: "+trama_salida, EIGlobal.NivelLog.DEBUG);
				camposEmpresa=desentramaDatosEmpresa(trama_salida, tokens, pipe);
				if(camposEmpresa[0].equals("TUBO0000")){
					if (camposEmpresa[4].equals("") || camposEmpresa[12].equals("")){
						EIGlobal.mensajePorTrace(LOG_METODO + "Los datos de la empresa vienen vacios: [" + camposEmpresa[0] + "]", EIGlobal.NivelLog.ERROR);
					}else{
						descripcionEmpresa =  camposEmpresa[12].trim();
					}
				}else{
					EIGlobal.mensajePorTrace(LOG_METODO + "Falla al consultar datos empresa: ["+ camposEmpresa[0] + "]", EIGlobal.NivelLog.INFO);
				}
			}catch(Exception e){
				EIGlobal.mensajePorTrace(LOG_METODO + "Excepci�n en consultaB750() [" + e + "]", EIGlobal.NivelLog.ERROR);
			}
		}else{
			EIGlobal.mensajePorTrace(LOG_METODO + "El contrato viene vacio", EIGlobal.NivelLog.INFO);
		}
		return descripcionEmpresa;
	}

}