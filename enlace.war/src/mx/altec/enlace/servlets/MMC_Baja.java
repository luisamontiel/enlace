/**
*   Isban M?xico
*   Clase: MMC_Baja.java
*   Descripci?n: Servlet que maneja todo el flujo de Baja de Cuentas.
*
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Se agrega el manejo de excepciones a los metodos.
*/

package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceLynxConstants;
import mx.altec.enlace.utilerias.EnlaceLynxUtils;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

//VSWF
/**
 * Servlet que maneja todo el flujo de Baja de Cuentas
 * */
public class MMC_Baja extends BaseServlet {
    /**
     * S?mbolo separador de tramas
     * */
    static final String PIPE = "|";

    /** Nombre del atributo de la sesion interna para Enlace. */
    private static final String SESATTR_SESSION = "session";

    /** Nombre del atributo de sesion para guardar las facultades de baja. */
    private static final String SESATTR_FACULTADES_BAJA_CTAS = "facultadesBajaCtas";

    /** Atributo de sesion para nombre de usuario. */
    private static final String SESATTR_NOM_USUARIO = "NomUsuario";

    /** Atributo de sesion para numero de usuario. */
    private static final String SESATTR_NUM_USUARIO = "NumUsuario";

    /** Atributo de sesion para nombre del contrato. */
    private static final String SESATTR_NOM_CONTRATO = "NomContrato";

    /** Atributo de sesion para el numero de contrato. */
    private static final String SESATTR_NUM_CONTRATO = "NumContrato";

    /** Cadena del titulo de baja de cuentas. */
    private static final String TXT_BAJA_DE_CUENTAS = "Baja de cuentas";

    /** Cadena de texto para el breadcrumb de la pagina. */
    private static final String TXT_ADMON_CTRL_CTAS_BAJA = "Administraci&oacute;n y control &gt; Cuentas &gt; Baja";

    /** Clave de la pagina de ayuda del modulo. */
    private static final String CVE_AYUDA_S55205H = "s55205h";

    /** Codigo de accion de entrada inicial al modulo. */
    private static final String COD_MODULO_INICIAL = "0";
    /** Codigo de accion para agregar cuentas a la lista. */
    private static final String COD_MODULO_AGREGAR = "1";
    /** Codigo de accion para indicar la validacion de cuentas. */
    private static final String COD_MODULO_VALIDAR = "2";
    /** Codigo de accion para presentar la tabla de cuentas. */
    private static final String COD_MODULO_PRESENTA = "3";

    /**
     * Metodo que responde a peticiones GET
     *
     * @param request
     *            instancia de HTTPServletRequest
     * @param response
     *            instancia de HTTPServletResponse
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos/dispositivos
     * */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        inicioServletBajaCuentas(request, response);
    }

    /**
     * M?todo que responde a peticiones POST
     *
     * @param request
     *            instancia de HTTPServletRequest
     * @param response
     *            instancia de HTTPServletResponse
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos/dispositivos
     * */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        inicioServletBajaCuentas(request, response);
    }

    /*************************************************************************************/
    /************************************************** inicioServletBajaCuentas */
    /*************************************************************************************/
    /**
     * Inicializador del servlet para Bajaa de Cuentas
     *
     * @param request
     *            instancia de HTTPServletRequest
     * @param response
     *            instancia de HTTPServletResponse
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos
     * */
    public void inicioServletBajaCuentas(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        HashMap facultadesBajaCtas = new HashMap();
        HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESATTR_SESSION);

        EIGlobal.mensajePorTrace(
                "MMC_Baja - defaultAction(): Entrando a Baja de Cuentas.",
                EIGlobal.NivelLog.INFO);

        String modulo = request.getParameter("Modulo");
        EIGlobal.mensajePorTrace("MMC_Baja - defaultAction(): modulo ="
                + modulo, EIGlobal.NivelLog.INFO);
        Boolean envioCompleto = Boolean.valueOf((String) request.getParameter("flag"));
        if (modulo == null) {
            modulo = COD_MODULO_INICIAL;
        }
        if (SesionValida(request, response)) {
            // Facultades ...
            facultadesBajaCtas.put("BajaCtasMB", session.getFacultad(BaseResource.FAC_BAJA_MB));
            facultadesBajaCtas.put("BajaCtasBN", session.getFacultad(BaseResource.FAC_BAJA_BN));
            facultadesBajaCtas.put("BajaCtasBI", session.getFacultad(BaseResource.FAC_BAJA_BI));
            facultadesBajaCtas.put("AutoCtasMB", session.getFacultad(BaseResource.FAC_AUTO_AB_MB));
            facultadesBajaCtas.put("AutoCtasOB", session.getFacultad(BaseResource.FAC_AUTO_AB_OB));

            EIGlobal.mensajePorTrace(
                    "MMC_Baja - execute(): Leyendo facultades.",
                    EIGlobal.NivelLog.INFO);

            if (request.getSession().getAttribute(SESATTR_FACULTADES_BAJA_CTAS) != null) {
                request.getSession().removeAttribute(SESATTR_FACULTADES_BAJA_CTAS);
            }
            request.getSession().setAttribute(SESATTR_FACULTADES_BAJA_CTAS,
                    facultadesBajaCtas);

            if (!((Boolean) facultadesBajaCtas.get("BajaCtasBN"))
                    .booleanValue()
                    && !((Boolean) facultadesBajaCtas.get("BajaCtasMB"))
                            .booleanValue()
                    && !((Boolean) facultadesBajaCtas.get("BajaCtasBI"))
                            .booleanValue()) {
                despliegaPaginaErrorURL("<font color=red>No</font> tiene <i>facultades</i> para dar cuentas de <font color=green>Baja</font>.",
                        TXT_BAJA_DE_CUENTAS, TXT_ADMON_CTRL_CTAS_BAJA,
                        CVE_AYUDA_S55205H, request, response);
            } else {
                if (COD_MODULO_INICIAL.equals(modulo)) {
                    iniciaBaja(request, response);
                }
                if (COD_MODULO_AGREGAR.equals(modulo)) {
                    agregaCuentas(request, response);
                }
                if (COD_MODULO_VALIDAR.equals(modulo) && envioCompleto.booleanValue()) {
                    enviaCuentas(request, response);
                }
                if (COD_MODULO_VALIDAR.equals(modulo) && !envioCompleto.booleanValue()) {
                    enviaCuentasCancel(request, response);
                }
                if (COD_MODULO_PRESENTA.equals(modulo)) {
                    presentaCuentas(request, response);
                }
            }
        }
        EIGlobal.mensajePorTrace(
                "MMC_Baja - execute(): Saliendo de Baja de Cuentas.",
                EIGlobal.NivelLog.INFO);
    }

    /*************************************************************************************/
    /*************************************************************** inicioBajas */
    /*************************************************************************************/
    /**
     * Inicializador del flujo para baja de cuentas
     *
     * @param request
     *            instancia de HTTPServletRequest
     * @param response
     *            instancia de HTTPServletResponse
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos
     * */
    public void iniciaBaja(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESATTR_SESSION);

        EIGlobal.mensajePorTrace(
                "MMC_Baja - iniciaBaja(): Pantalla Principal de baja de cuentas.",
                EIGlobal.NivelLog.INFO);

        request.setAttribute("Modulo", COD_MODULO_INICIAL);

        request.setAttribute(SESATTR_NUM_CONTRATO,
                (session.getContractNumber() == null) ? "" : session
                        .getContractNumber());
        request.setAttribute(SESATTR_NOM_CONTRATO,
                (session.getNombreContrato() == null) ? "" : session
                        .getNombreContrato());
        request.setAttribute(SESATTR_NUM_USUARIO, (session.getUserID8() == null) ? ""
                : session.getUserID8());
        request.setAttribute(SESATTR_NOM_USUARIO,
                (session.getNombreUsuario() == null) ? "" : session
                        .getNombreUsuario());
        sess.removeAttribute("foliosBaja");
        sess.removeAttribute("CuentasLynxBaja");
        sess.removeAttribute("CuentasListaBaja");
        // TODO BIT CU5081, inicia el flujo de Baja de Cuentas
        /* VSWF HGG I */
        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")) {
            try {
                BitaHelper bh = new BitaHelperImpl(request, session, sess);
                if (request.getParameter(BitaConstants.FLUJO) != null) {
                    bh.incrementaFolioFlujo(request
                            .getParameter(BitaConstants.FLUJO));
                } else {
                    bh.incrementaFolioFlujo((String) request.getSession()
                            .getAttribute(BitaConstants.SESS_ID_FLUJO));
                }
                BitaTransacBean bt = new BitaTransacBean();
                bt = (BitaTransacBean) bh.llenarBean(bt);
                bt.setNumBit(BitaConstants.EA_CUENTAS_BAJA_ENTRA);
                if (session.getContractNumber() != null) {
                    bt.setContrato(session.getContractNumber().trim());
                }

                BitaHandler.getInstance().insertBitaTransac(bt);
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);

            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);

            }
        }
        /* VSWF HGG F */

        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("Encabezado",
                CreaEncabezado(TXT_BAJA_DE_CUENTAS, TXT_ADMON_CTRL_CTAS_BAJA,
                        "s26090h", request));

        session.setModuloConsultar(IEnlace.MMant_gral_ctas);

        evalTemplate("/jsp/MMC_Baja.jsp", request, response);
    }

    /*************************************************************************************/
    /************************************************** Pantalla Principal - Modulo=0 */
    /*************************************************************************************/
    /**
     * Metodo que realiza el agregado de cuentas a pantalla
     *
     * @param req
     *            instancia de HTTPServletRequest
     * @param res
     *            instancia de HTTPServletResponse
     * @return identificador de exito en la operaci?n
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos
     */
    public int agregaCuentas(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        EIGlobal.mensajePorTrace("MMC_Baja - agregaCuentas(): IniciaBaja.",
                EIGlobal.NivelLog.INFO);

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESATTR_SESSION);

        EIGlobal.mensajePorTrace("MMC_Baja - agregaCuentas(): tipoBaja."
                + (String) req.getParameter("radioTipoCta"),
                EIGlobal.NivelLog.INFO);

        int radioTipoCta = Integer.parseInt((String) req
                .getParameter("radioTipoCta"));

        String cmbCtasPropias = req.getParameter("cmbCtasPropias");
        String cmbCtasTerceros = req.getParameter("cmbCtasTerceros");
        String cmbCtasExtranjeras = req.getParameter("cmbCtasExtranjeras");
        String cmbCtasMoviles = req.getParameter("cmbCtasMoviles");
        EIGlobal.mensajePorTrace("Trama recibida: " + cmbCtasMoviles,
                EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace(
                "cmbCtasTerceros recibida: " + cmbCtasTerceros,
                EIGlobal.NivelLog.INFO);
        /** GAE **/
        String folioBajaLote = req.getParameter("folioBajaLote");
        Vector foliosBaja = null;

        if (req.getSession().getAttribute("foliosBaja") == null) {
            foliosBaja = new Vector();
        } else {
            foliosBaja = (Vector) req.getSession().getAttribute("foliosBaja");
        }
        /** FIN GAE **/

        StringBuilder TRAMA_MOVIL_MB = new StringBuilder();
        StringBuilder TRAMA_MOVIL_BN = new StringBuilder();

        String strTramaMB = (String) req.getParameter("strTramaMB");
        String strTramaBN = (String) req.getParameter("strTramaBN");
        String strTramaBI = (String) req.getParameter("strTramaBI");

        String numeroCuenta = "";
        String nombreTitular = "";

        String cvePlaza = "";
        String strPlaza = "";
        String cveBanco = "";
        String strBanco = "";
        String strSuc = "";
        String strDivisa = "";
        String tipoCuenta = "";

        String paisBI = "";
        String strCiudadBI = "";
        String strBancoBI = "";
        String cveDivisaBI = "";
        String strDivisaBI = "";
        String numeroCuentaBI = "";
        String strClaveABABI = "";

        String strCtaTmp = "";

        String modulo = COD_MODULO_AGREGAR;
        Map<String, String> cuentasLynx = req.getSession().getAttribute(
                "CuentasLynxBaja") == null ? new HashMap<String, String>()
                : (Map<String, String>) req.getSession().getAttribute(
                        "CuentasLynxBaja");
        switch (radioTipoCta) {
        case 0: // Mismo Banco
            EIGlobal.mensajePorTrace(
                    "MMC_Baja - agregaCuentas(): Mismo Banco.",
                    EIGlobal.NivelLog.INFO);

            numeroCuenta = cmbCtasPropias.substring(0,
                    cmbCtasPropias.indexOf('|'));
            nombreTitular = cmbCtasPropias.substring(
                    cmbCtasPropias.indexOf('|') + 1,
                    cmbCtasPropias.length() - 1);

            cvePlaza = "01001";
            strPlaza = "MEXICO, D.F.";
            cveBanco = "BANME";
            strBanco = "BANCO MEXICANO";
            strSuc = "035";

            strCtaTmp = numeroCuenta + PIPE + nombreTitular + PIPE + cvePlaza
                    + PIPE + strPlaza + PIPE + cveBanco + PIPE + strBanco
                    + PIPE + strSuc;
            strTramaMB += strCtaTmp.concat("@");

            break;
        case 1: // Banco Nacional
            EIGlobal.mensajePorTrace(
                    "MMC_Baja - agregaCuentas(): Banco Nacional.",
                    EIGlobal.NivelLog.INFO);

            numeroCuenta = cmbCtasTerceros.substring(0,
                    cmbCtasTerceros.indexOf('|'));
            nombreTitular = cmbCtasTerceros.substring(
                    cmbCtasTerceros.indexOf('|') + 1,
                    cmbCtasTerceros.indexOf('@'));

            cvePlaza = cmbCtasTerceros.substring(
                    cmbCtasTerceros.indexOf('%') + 1,
                    cmbCtasTerceros.indexOf('#'));
            strPlaza = cmbCtasTerceros.substring(
                    cmbCtasTerceros.indexOf('!') + 1,
                    cmbCtasTerceros.indexOf('$'));
            cveBanco = cmbCtasTerceros.substring(
                    cmbCtasTerceros.indexOf('~') + 1,
                    cmbCtasTerceros.indexOf('%'));
            strBanco = cmbCtasTerceros.substring(
                    cmbCtasTerceros.indexOf('@') + 1,
                    cmbCtasTerceros.indexOf('!'));
            strSuc = cmbCtasTerceros.substring(
                    cmbCtasTerceros.indexOf('$') + 1,
                    cmbCtasTerceros.indexOf('~'));
            strDivisa = cmbCtasTerceros.substring(
                    cmbCtasTerceros.indexOf('#') + 1, cmbCtasTerceros.length());

            switch (numeroCuenta.length()) {
            case 11:
                tipoCuenta = "01";
                break;
            case 16:
                tipoCuenta = "02";
                break;
            case 18:
                tipoCuenta = "40";
                break;
            default:
                tipoCuenta = "01";
                break;
            }

            strCtaTmp = numeroCuenta + PIPE + nombreTitular + PIPE + cvePlaza
                    + PIPE + strPlaza + PIPE + cveBanco + PIPE + strBanco
                    + PIPE + strSuc + PIPE + tipoCuenta + PIPE + strDivisa;
            strTramaBN += strCtaTmp.concat("@");

            EIGlobal.mensajePorTrace("strTramaBN:" + strTramaBN,
                    EIGlobal.NivelLog.INFO);

            break;
        case 2: // Banco Internacional
                // //cuenta|descripcion@ciudad!banco$aba~divisa%pais
            EIGlobal.mensajePorTrace(
                    "MMC_Baja - agregaCuentas(): cmbCtasExtranjeras "
                            + cmbCtasExtranjeras, EIGlobal.NivelLog.INFO);

            numeroCuenta = cmbCtasExtranjeras.substring(0,
                    cmbCtasExtranjeras.indexOf('|'));
            nombreTitular = cmbCtasExtranjeras.substring(
                    cmbCtasExtranjeras.indexOf('|') + 1,
                    cmbCtasExtranjeras.indexOf('@'));

            paisBI = cmbCtasExtranjeras.substring(
                    cmbCtasExtranjeras.indexOf('%') + 1,
                    cmbCtasExtranjeras.length());
            strBancoBI = cmbCtasExtranjeras.substring(
                    cmbCtasExtranjeras.indexOf('!') + 1,
                    cmbCtasExtranjeras.indexOf('$'));
            strDivisaBI = cmbCtasExtranjeras.substring(
                    cmbCtasExtranjeras.indexOf('~') + 1,
                    cmbCtasExtranjeras.indexOf('%'));
            strCiudadBI = cmbCtasExtranjeras.substring(
                    cmbCtasExtranjeras.indexOf('@') + 1,
                    cmbCtasExtranjeras.indexOf('!'));
            // strClaveABABI = cmbCtasExtranjeras.substring(
            // cmbCtasExtranjeras.indexOf( '$' ) + 1,
            // cmbCtasExtranjeras.indexOf( '~' ) );
            strClaveABABI = req.getParameter("txtABA");

            paisBI = traeClave("PAIS", paisBI);
            EIGlobal.mensajePorTrace("MMC_Baja - agregaCuentas(): pais "
                    + paisBI, EIGlobal.NivelLog.INFO);
            cveDivisaBI = traeClave("DIVISA", strDivisaBI);
            EIGlobal.mensajePorTrace("MMC_Baja - agregaCuentas(): divisa "
                    + strDivisaBI + " cve " + cveDivisaBI,
                    EIGlobal.NivelLog.INFO);

            strCtaTmp = numeroCuenta + PIPE + nombreTitular + PIPE + paisBI
                    + PIPE + strCiudadBI + PIPE + strBancoBI + PIPE
                    + cveDivisaBI + PIPE + strDivisaBI + PIPE + numeroCuentaBI
                    + PIPE + strClaveABABI;
            strTramaBI += strCtaTmp + "@";
            break;
        /** GAE **/
        case 3: // Baja Masiva de Cuentas

            EIGlobal.mensajePorTrace(
                    "MMC_Baja - agregaCuentas(): baja masiva de cuentas, folio->"
                            + folioBajaLote, EIGlobal.NivelLog.INFO);
            if (folioBajaLote != null && !folioBajaLote.equals("")) {
                if (existeLote(folioBajaLote)) {
                    if (validaLote(folioBajaLote)
                            || !validaLote2(folioBajaLote)) {
                        req.setAttribute("errorLote",
                                "El lote seleccionado no tiene cuentas a Cancelar ? dar Baja ");
                    } else {
                        if (folioRepetido(foliosBaja, folioBajaLote)) {
                            req.setAttribute("errorLote",
                                    "El lote ya se encuentra seleccionado");
                        } else {
                            foliosBaja.add(folioBajaLote);
                            // Descargar detalle de cuentas
                            extraeLote(folioBajaLote, cuentasLynx);
                        }
                        req.getSession().setAttribute("foliosBaja", foliosBaja);
                    }
                } else {
                    req.setAttribute("errorLote",
                            "El lote seleccionado no existe");
                }
            } else {
                req.setAttribute("errorLote",
                        "El lote seleccionado no puede ser vacio");
            }
            break;
        /** FIN GAE **/
        case 4: // Cuentas M?viles
            EIGlobal.mensajePorTrace(
                    "MMC_Baja - agregaCuentas(): Numeros Moviles.",
                    EIGlobal.NivelLog.INFO);

            final String TIPO_CUENTA_NM_SANTANDER = "BANME";
            EIGlobal.mensajePorTrace("Trama recibida: " + cmbCtasMoviles,
                    EIGlobal.NivelLog.INFO);
            numeroCuenta = cmbCtasMoviles.substring(0,
                    cmbCtasMoviles.indexOf('|'));
            nombreTitular = cmbCtasMoviles.substring(
                    cmbCtasMoviles.indexOf('|') + 1,
                    cmbCtasMoviles.indexOf('@'));
            cveBanco = cmbCtasMoviles.substring(
                    cmbCtasMoviles.indexOf('@') + 1,
                    cmbCtasMoviles.indexOf('!'));
            strBanco = cmbCtasMoviles.substring(
                    cmbCtasMoviles.indexOf('!') + 1,
                    cmbCtasMoviles.indexOf('$'));

            /** El tipo de cuenta seleccionado es Mismo Banco */
            if (TIPO_CUENTA_NM_SANTANDER.equals(cveBanco)) {
                TRAMA_MOVIL_MB.append(strTramaMB);
                int len = TRAMA_MOVIL_MB.length();
                TRAMA_MOVIL_MB.append(numeroCuenta).append(PIPE)
                        .append(nombreTitular).append(PIPE).append(cvePlaza)
                        .append(PIPE).append(strPlaza).append(PIPE)
                        .append(cveBanco).append(PIPE).append(strBanco)
                        .append(PIPE).append(strSuc).append(PIPE).append('@');
                // strTramaMB +=
                // numeroCuenta + pipe +
                // nombreTitular + pipe +
                // cvePlaza + pipe +
                // strPlaza + pipe +
                // cveBanco + pipe +
                // strBanco + pipe +
                // strSuc + "@";
                strTramaMB = TRAMA_MOVIL_MB.toString();
                strCtaTmp = TRAMA_MOVIL_MB.substring(len,
                        TRAMA_MOVIL_MB.length() - 1);
            }
            /** El tipo de cuenta seleccionado es Otros Bancos */
            else {
                TRAMA_MOVIL_BN.append(strTramaBN);
                int len = TRAMA_MOVIL_BN.length();
                TRAMA_MOVIL_BN.append(numeroCuenta).append(PIPE)
                        .append(nombreTitular).append(PIPE).append(cvePlaza)
                        .append(PIPE).append(strPlaza).append(PIPE)
                        .append(cveBanco).append(PIPE).append(strBanco)
                        .append(PIPE).append(strSuc).append(PIPE).append(" ")
                        .append(PIPE).append('@');
                // strTramaBN +=
                // numeroCuenta + pipe +
                // nombreTitular + pipe +
                // cvePlaza + pipe +
                // strPlaza + pipe +
                // cveBanco + pipe +
                // strBanco + pipe +
                // strSuc + pipe +
                // " " + "@";
                strTramaBN = TRAMA_MOVIL_BN.toString();
                strCtaTmp = TRAMA_MOVIL_BN.substring(len,
                        TRAMA_MOVIL_BN.length() - 1);
            }
            break;
        }

        if (!numeroCuenta.equals("")) {
            List<String> cuentas = req.getSession().getAttribute(
                    "CuentasListaBaja") == null ? new ArrayList<String>()
                    : (List<String>) req.getSession().getAttribute(
                            "CuentasListaBaja");
            cuentas.add(numeroCuenta);
            req.getSession().setAttribute("CuentasListaBaja", cuentas);
            cuentasLynx
                    .put(numeroCuenta, String.valueOf(radioTipoCta)
                            .concat(PIPE).concat(strCtaTmp));
        }
        req.getSession().setAttribute("CuentasLynxBaja", cuentasLynx);

        EIGlobal.mensajePorTrace(
                "MMC_Baja - agregaCuentas(): Finalizando baja.",
                EIGlobal.NivelLog.INFO);

        req.setAttribute("Modulo", COD_MODULO_AGREGAR);

        req.setAttribute("tablaBN", generaTablaCuentas(strTramaBN, false));
        req.setAttribute("strTramaBN", strTramaBN);

        req.setAttribute("tablaMB", generaTablaCuentas(strTramaMB, true));
        req.setAttribute("strTramaMB", strTramaMB);

        req.setAttribute("tablaBI",
                generaTablaCuentasBI(strTramaBI, "Cuentas Internacionales"));
        req.setAttribute("strTramaBI", strTramaBI);

        /** GAE **/
        req.setAttribute("tablaLotes", generaTablaLotes(foliosBaja));
        /** FIN GAE **/

        req.setAttribute("newMenu", session.getFuncionesDeMenu());
        req.setAttribute("MenuPrincipal", session.getStrMenu());
        req.setAttribute("Encabezado",
                CreaEncabezado(TXT_BAJA_DE_CUENTAS,
                        "Administraci&oacute;n y control &gt; Cuentas &gt; Baja &gt; Agregar",
                        "s26090h", req));

        req.setAttribute(
                "NumContrato",
                (session.getContractNumber() == null) ? "" : session
                        .getContractNumber());
        req.setAttribute(
                "NomContrato",
                (session.getNombreContrato() == null) ? "" : session
                        .getNombreContrato());
        req.setAttribute("NumUsuario", (session.getUserID8() == null) ? ""
                : session.getUserID8());
        req.setAttribute(
                "NomUsuario",
                (session.getNombreUsuario() == null) ? "" : session
                        .getNombreUsuario());

        EIGlobal.mensajePorTrace(
                "MMC_Baja - agregaCuentas(): Finalizando Baja.",
                EIGlobal.NivelLog.INFO);
        evalTemplate("/jsp/MMC_Baja.jsp", req, res);
        return 1;

    }

    /*************************************************************************************/
    /************************************************** Pantalla Principal - Modulo=2 */
    /*************************************************************************************/
    /**
     * M?todo que realiza la presentaci?n de las cuentas en pantalla
     *
     * @param req
     *            instancia de HTTPServletRequest
     * @param res
     *            instancia de HTTPServletResponse
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos
     * @return indicador de ?xito en la operaci?n
     * */
    public int presentaCuentas(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        EIGlobal.mensajePorTrace("MMC_Baja - presentaCuentas(): IniciaBaja.",
                EIGlobal.NivelLog.INFO);

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESATTR_SESSION);

        String cuentasSel = (String) req.getParameter("cuentasSel");
        EIGlobal.mensajePorTrace("MMC_Baja - presentaCuentas(): Seleccionadas "
                + cuentasSel, EIGlobal.NivelLog.INFO);

        String strTramaMB = (String) req.getParameter("strTramaMB");
        String strTramaBN = (String) req.getParameter("strTramaBN");
        String strTramaBI = (String) req.getParameter("strTramaBI");

        int reg = 0;
        int MBr = 0;
        int BNr = 0;
        int BIr = 0;

        /** GAE **/
        int r = 0;
        String msg = null;
        String cuentasSelByLote = "";
        String cuentasSelTmp = "";
        Integer conCuentasSelByLote = new Integer(COD_MODULO_INICIAL);
        boolean flag = false;
        Integer cuentasCanceladas = null;
        Vector foliosBaja = (Vector) req.getSession()
                .getAttribute("foliosBaja");
        String cuentasNoAut = null;
        if (foliosBaja != null) {
            String[] tramas = getTramas(foliosBaja, cuentasSel);
            strTramaMB += tramas[0];
            strTramaBN += tramas[1];
            strTramaBI += tramas[2];
            cuentasNoAut = tramas[3];
            conCuentasSelByLote = new Integer(tramas[4]);
        }

        EIGlobal.mensajePorTrace(
                "Numero de cuentas seleccionadas antes masivo->" + cuentasSel,
                EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Numero de iteraciones antes masivo->"
                + conCuentasSelByLote.intValue(), EIGlobal.NivelLog.INFO);
        if (foliosBaja != null) {
            for (int l = 0; l < conCuentasSelByLote.intValue(); l++) {
                cuentasSelByLote += COD_MODULO_AGREGAR;
                r++;
            }
            for (int g = 0; g < (cuentasSel.length() - foliosBaja.size()); g++) {
                cuentasSelTmp += cuentasSel.charAt(g);
            }
            cuentasSel = cuentasSelTmp + cuentasSelByLote;
        }
        EIGlobal.mensajePorTrace(
                "Numero de cuentas seleccionadas despues masivo->" + cuentasSel,
                EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Numero de iteraciones despues masivo->" + r,
                EIGlobal.NivelLog.INFO);

        /** FIN GAE **/

        if (strTramaMB.length() >= 1 || strTramaBN.length() >= 1
                || strTramaBI.length() >= 1) {

            String nombreArchivo = IEnlace.LOCAL_TMP_DIR + "/"
                    + session.getUserID8() + ".txt";
            String arcLinea = "";
            // StringBuilder arcBN = new StringBuilder();

            EI_Tipo MB = new EI_Tipo();
            EI_Tipo BN = new EI_Tipo();
            EI_Tipo BI = new EI_Tipo();
            final int LONGITUD_CUENTA_MOVIL = 10;

            MB.iniciaObjeto(strTramaMB);
            BN.iniciaObjeto(strTramaBN);
            BI.iniciaObjeto(strTramaBI);

            if (MB.totalRegistros >= 1 || BN.totalRegistros >= 1
                    || BI.totalRegistros >= 1) {

                EI_Exportar ArcSal = new EI_Exportar(nombreArchivo);
                EIGlobal.mensajePorTrace(
                        "MMC_Baja - presentaCuentas(): Nombre Archivo. "
                                + nombreArchivo, EIGlobal.NivelLog.INFO);
                if (ArcSal.creaArchivo()) {
                    arcLinea = "";

                    /**
                     * Los valores del arreglo "c" especifican la obtenci?n de
                     * los siguientes campos a manera de posiciones dentro de
                     * MB.camposTabla[posicion] : [ numeroCuentaBN,
                     * nombreTitularBN, cveBanco, cvePlaza, strSuc,
                     * tipoCuentaBN,
                     *
                     * ]
                     */
                    int c[] = { 0, 1, 4, 2, 8, 7 }; // BN Indice del elemento
                                                    // deseado

                    /**
                     * Los valores del arreglo "b" indican el tama?o de
                     * longitud m?xima aceptada en el layout de importaci?n
                     * de registros por archivo.
                     * */
                    int b[] = { 20, 40, 5, 5, 5, 2 }; // BN Longitud maxima del
                                                      // campo para el elemento

                    int ci[] = { 0, 1, 2, 5, 3, 4, 8 }; // BI Indice del
                                                        // elemento deseado
                    int bi[] = { 20, 40, 4, 4, 40, 40, 12 }; // BI Longitud
                                                             // maxima del campo
                                                             // para el elemento

                    EIGlobal.mensajePorTrace(
                            "MMC_Baja - presentaCuentas(): Exportando Cuentas mismo banco.",
                            EIGlobal.NivelLog.INFO);

                    for (int i = 0; i < MB.totalRegistros; i++) {
                        if (cuentasSel.charAt(reg + i) == '1') {
                            StringBuilder arcMB = new StringBuilder();
                            String cuenta = MB.camposTabla[i][0].trim();
                            boolean isMovil = LONGITUD_CUENTA_MOVIL == cuenta
                                    .length();
                            String tipoCuenta = isMovil ? "MOVIL " : "SANTAN";

                            arcMB.append(tipoCuenta);
                            arcMB.append(ArcSal.formateaCampo(cuenta, 20));
                            arcMB.append(ArcSal.formateaCampo(
                                    MB.camposTabla[i][1], 40));

                            // arcLinea=tipoCuenta;
                            // arcLinea+=ArcSal.formateaCampo(cuenta,20);
                            // arcLinea+=ArcSal.formateaCampo(MB.camposTabla[i][1],40);

                            /**
                             * En caso que el n?mero de cuenta sea un n?mero
                             * movil, agrega el cuarto campo requerido en el
                             * layout de texto
                             * */
                            if (isMovil)
                                arcMB.append(ArcSal.formateaCampo(
                                        MB.camposTabla[i][4], 5));
                            arcMB.append("\n");

                            arcLinea = arcMB.toString();
                            EIGlobal.mensajePorTrace(
                                    "MMC_Baja - presentaCuentas(): Insertando cuenta numero "
                                            + arcLinea + (i + 1),
                                    EIGlobal.NivelLog.INFO);
                            if (!ArcSal.escribeLinea(arcLinea))
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja - presentaCuentas(): Algo salio mal escribiendo "
                                                + arcLinea,
                                        EIGlobal.NivelLog.INFO);
                            MBr++;
                        }
                    }
                    reg += MB.totalRegistros;

                    EIGlobal.mensajePorTrace(
                            "MMC_Baja - presentaCuentas(): Exportando Cuentas Bancos nacionales.",
                            EIGlobal.NivelLog.INFO);
                    for (int i = 0; i < BN.totalRegistros; i++) {
                        if (cuentasSel.charAt(reg + i) == '1') {
                            // si la longitud == 10 entonces arcLinea="MOVIL ";
                            // si la longitud != 10 entonces arcLinea="EXTRNA";
                            arcLinea = LONGITUD_CUENTA_MOVIL == BN.camposTabla[i][0]
                                    .trim().length() ? "MOVIL " : "EXTRNA";

                            for (int j = 0; j < c.length; j++)
                                arcLinea += ArcSal.formateaCampo(
                                        BN.camposTabla[i][c[j]], b[j]);
                            arcLinea += "\n";
                            if (!ArcSal.escribeLinea(arcLinea))
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja - presentaCuentas()s(): Algo salio mal escribiendo: "
                                                + arcLinea,
                                        EIGlobal.NivelLog.INFO);
                            BNr++;
                        }
                    }
                    reg += BN.totalRegistros;

                    EIGlobal.mensajePorTrace(
                            "MMC_Baja - presentaCuentas(): Exportando Cuentas Bancos internacionales.",
                            EIGlobal.NivelLog.INFO);
                    for (int i = 0; i < BI.totalRegistros; i++) {
                        if (cuentasSel.charAt(reg + i) == '1') {
                            arcLinea = "INTNAL";
                            for (int j = 0; j < ci.length; j++)
                                arcLinea += ArcSal.formateaCampo(
                                        BI.camposTabla[i][ci[j]], bi[j]);
                            arcLinea += "\n";
                            if (!ArcSal.escribeLinea(arcLinea))
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja - presentaCuentas()s(): Algo salio mal escribiendo: "
                                                + arcLinea,
                                        EIGlobal.NivelLog.INFO);
                            BIr++;
                        }
                    }
                    ArcSal.cierraArchivo();
                    flag = true;
                } else {

                    EIGlobal.mensajePorTrace(
                            "MMC_Baja - presentaCuentas(): No se pudo llevar a cabo la exportacion.",
                            EIGlobal.NivelLog.INFO);
                }
            }
            req.setAttribute("nombreArchivoImp", nombreArchivo);
        }

        /** GAE **/
        if (foliosBaja != null) {
            try {
                cuentasCanceladas = new Integer(cuentasNoAut);
                if (cuentasCanceladas.intValue() > 0) {
                    req.setAttribute("cuentasNoAut", cuentasNoAut);
                    req.setAttribute(
                            "msgCuentasCanceladas",
                            "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
                                    + "<tr>"
                                    + "<td colspan=5 class=\"textabdatcla\"> "
                                    + "<font color=red size=3>Nota:</font><font size=2>El total de cuentas a cancelar es de "
                                    + cuentasCanceladas.toString()
                                    + "</font></td></tr></table></td></tr>");
                }
            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);

            }
            req.setAttribute("flag", new Boolean(flag));
        } else {
            req.setAttribute("flag", new Boolean(flag));
        }
        /** FIN GAE **/

        req.setAttribute("totalRegistros", Integer.toString(MBr + BNr + BIr));
        req.setAttribute("numeroCtasInternas", Integer.toString(MBr));
        req.setAttribute("numeroCtasNacionales", Integer.toString(BNr));
        req.setAttribute("numeroCtasInternacionales", Integer.toString(BIr));

        req.setAttribute("newMenu", session.getFuncionesDeMenu());
        req.setAttribute("MenuPrincipal", session.getStrMenu());
        req.setAttribute("Encabezado",
                CreaEncabezado("Baja de Cuentas", TXT_ADMON_CTRL_CTAS_BAJA,
                        "s26092h", req));

        req.setAttribute(
                "NumContrato",
                (session.getContractNumber() == null) ? "" : session
                        .getContractNumber());
        req.setAttribute(
                "NomContrato",
                (session.getNombreContrato() == null) ? "" : session
                        .getNombreContrato());
        req.setAttribute("NumUsuario", (session.getUserID8() == null) ? ""
                : session.getUserID8());
        req.setAttribute(
                "NomUsuario",
                (session.getNombreUsuario() == null) ? "" : session
                        .getNombreUsuario());

        EIGlobal.mensajePorTrace(
                "MMC_Baja - agregaCuentas(): Finalizando Baja.",
                EIGlobal.NivelLog.INFO);
        evalTemplate("/jsp/MMC_BajaArchivo.jsp", req, res);
        return 1;
    }

    /*************************************************************************************/
    /************************************************** Pantalla Principal - Modulo=0 */
    /*************************************************************************************/
    /**
     * M?todo que realiza el env?o de las cuentas a de la operaci?n
     *
     * @param req
     *            instancia de HTTPServletRequest
     * @param res
     *            instancia de HTTPServletResponse
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos
     * @return indicador de ?xito en la operaci?n
     * */
    public int enviaCuentas(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        EIGlobal.mensajePorTrace("MMC_Baja - enviaCuentas(): IniciaBaja.",
                EIGlobal.NivelLog.INFO);

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESATTR_SESSION);
        String codErrorOper = "";

        String nombreArchivoImp = (String) req.getParameter("nombreArchivoImp");
        String totalRegistros = (String) req.getParameter("totalRegistros");
        /** GAE **/
        Integer cuentasCanceladas = null;
        String cuentasNoAut = (String) req.getParameter("cuentasNoAut");
        Vector foliosBaja = (Vector) req.getSession()
                .getAttribute("foliosBaja");
        /** FIN GAE **/
        String strMensaje = "";
        String nombreArchivo = "";

        EIGlobal.mensajePorTrace(
                "MMC_Baja - enviaCuentas(): nombreArchivoImp: "
                        + nombreArchivoImp, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("MMC_Baja - enviaCuentas(): totalRegistros: "
                + totalRegistros, EIGlobal.NivelLog.INFO);

        String cabecera = "";
        String regServicio = "";
        String Trama = "";
        String Result = "";

        String medioEntrega = "";
        String usuario = "";
        String contrato = "";
        String clavePerfil = "";
        String tipoOperacion = "";

        String line = "";

        String registroDeSolicitud = "";
        String registroDeBaja = "";
        String folioRegistro = "";

        StringBuffer lineDuplicados = new StringBuffer("");

        boolean consulta = false;

        /*
         * 0 Envio Correcto 1 Error en Envio 2 Envio con errores 3 Envio
         * correcto (Pero no se pudo identificar si hubo duplicidad)
         */

        HashMap facultadesBajaCtas = new HashMap();
        facultadesBajaCtas = (HashMap) req.getSession().getAttribute(SESATTR_FACULTADES_BAJA_CTAS);

        ServicioTux TuxGlobal = new ServicioTux();
        // TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext)
        // getServletContext()).getContext());

        nombreArchivo = nombreArchivoImp.substring(nombreArchivoImp
                .lastIndexOf('/') + 1);
        EIGlobal.mensajePorTrace("MMC_Baja-> Nombre para la copia "
                + nombreArchivo, EIGlobal.NivelLog.INFO);

        usuario = (session.getUserID8() == null) ? "" : session.getUserID8();
        clavePerfil = (session.getUserProfile() == null) ? "" : session
                .getUserProfile();
        contrato = (session.getContractNumber() == null) ? "" : session
                .getContractNumber();
        tipoOperacion = "CT02";
        medioEntrega = "2EWEB";

        ArchivoRemoto archR = new ArchivoRemoto();

        EIGlobal.mensajePorTrace(
                "MMC_Baja-> Se continua con el envio del archivo "
                        + nombreArchivo + "  ruta:" + Global.DIRECTORIO_LOCAL,
                EIGlobal.NivelLog.INFO);
        // Cargar campos por defecto
        HashMap<Integer, String> campos = new HashMap<Integer, String>();
        if (Global.LYNX_INICIALIZADO) {
            // Se cargan valores por default para el mensaje
            EnlaceLynxConstants.cargaValoresDefaultNM(campos, req);
        }
        if (!archR.copiaLocalARemotoCUENTAS(nombreArchivo,
                Global.DIRECTORIO_LOCAL)) {
            EIGlobal.mensajePorTrace(
                    "MMC_Baja-> No se realizo la copia remota a tuxedo.archivo "
                            + nombreArchivo, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace(
                    "MMC_Baja-> Se detiene el proceso pues no se envia el archivo."
                            + nombreArchivo, EIGlobal.NivelLog.INFO);
            despliegaPaginaErrorURL("<i><font color=red>Error</font></i> de comunicaciones.<br><br> Por favor realice la importaci&oacute;n <font color=green> nuevamente </font>.<br> El archivo <font color=blue><b> "
                            + nombreArchivo
                            + " </b></font> no ha sido enviado correctamente.<br><br>", TXT_BAJA_DE_CUENTAS, TXT_ADMON_CTRL_CTAS_BAJA, CVE_AYUDA_S55205H, req, res);
            return 1;
        } else {
            EIGlobal.mensajePorTrace("MMC_Baja-> Copia remota OK.",
                    EIGlobal.NivelLog.INFO);

            cabecera = medioEntrega + PIPE + usuario + PIPE + tipoOperacion
                    + PIPE + contrato + PIPE + usuario + PIPE + clavePerfil
                    + PIPE;

            regServicio = contrato + "@" + nombreArchivo + "@" + totalRegistros
                    + "@";

            Trama = cabecera + regServicio;

            /* #################################### */
            /**/
            EIGlobal.mensajePorTrace("MMC_Baja->  Trama entrada envio: "
                    + Trama, EIGlobal.NivelLog.DEBUG);
            Date fechaHoraOper = new Date();
            try {
                Hashtable hs = TuxGlobal.web_red(Trama);
                Result = (String) hs.get("BUFFER") + "";
                codErrorOper = hs.get("COD_ERROR") + "";
            } catch (java.rmi.RemoteException re) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(re),
                        EIGlobal.NivelLog.INFO);

            } catch (Exception e) {
            }

            EIGlobal.mensajePorTrace(
                    "MMC_Baja-> Trama salida envio: " + Result,
                    EIGlobal.NivelLog.DEBUG);

            // TODO BIT CU5081, Realiza la Baja de la cuenta nueva
            /* VSWF HGG I */
            List<String> cuentas = (List<String>) req.getSession()
                    .getAttribute("CuentasListaBaja");

            Map<String, String> cuentaPrueba = (Map<String, String>) req
                    .getSession().getAttribute("CuentasLynxBaja");

            if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals(
                    "ON")
                    && cuentas != null) {

                Iterator i = cuentas.iterator();
                Iterator cta = cuentaPrueba.keySet().iterator();
                while (i.hasNext()) {
                    String cuentaDest = "";
                    String divisaBit = "";

                    try {
                        cuentaDest = cta.next().toString();
                        divisaBit = cuentaPrueba.get(cuentaDest);

                        EIGlobal.mensajePorTrace("MMC_Baja-> cuentaDest: "
                                + cuentaDest + " divisaBit: " + divisaBit
                                + " tipoOper: " + divisaBit.substring(0, 1),
                                EIGlobal.NivelLog.DEBUG);
                        if (divisaBit != null && divisaBit.length() > 1){
                            if (COD_MODULO_AGREGAR.equals(divisaBit.substring(0, 1))
                                    && divisaBit.indexOf("|USD") > 0) {
                                tipoOperacion = "CT07";
                            } else {
                                tipoOperacion = "CT01";
                            }
                        }

                        BitaHelper bh = new BitaHelperImpl(req, session, sess);

                        BitaTransacBean bt = new BitaTransacBean();
                        bt = (BitaTransacBean) bh.llenarBean(bt);
                        bt.setNumBit(BitaConstants.EA_CUENTAS_BAJA_REALIZA_BAJA);
                        if (session.getContractNumber() != null) {
                            bt.setContrato(session.getContractNumber().trim());
                        }
                        if (Result != null) {
                            if (Result.substring(0, 2).equals("OK")) {
                                bt.setIdErr(tipoOperacion + "0000");
                                log("VSWF: IdErr " + bt.getIdErr());
                            } else if (Result.length() > 8) {
                                bt.setIdErr(Result.substring(0, 8));
                            } else {
                                bt.setIdErr(Result.trim());
                            }
                        }
                        if (tipoOperacion != null) {
                            bt.setServTransTux(tipoOperacion);
                        }
                        if (nombreArchivo != null) {
                            bt.setNombreArchivo(nombreArchivo);
                        }
                        bt.setCctaDest(i.next().toString());

                        BitaHandler.getInstance().insertBitaTransac(bt);
                    } catch (SQLException e) {
                        EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                                EIGlobal.NivelLog.INFO);

                    } catch (Exception e) {
                        EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                                EIGlobal.NivelLog.INFO);

                    }
                }
            }
            req.getSession().removeAttribute("CuentasListaBaja");
            /* VSWF HGG F */

            /* #################################### */
            /*
             * Result=
             * "OK      @     805@/t/tuxedo/tdesbcae/interfaces/alta_cuentas/Entrada1.txt.mct@"
             * ;
             */
            /* Result=null; */

            if (Result == null || Result.equals("")) {
                EIGlobal.mensajePorTrace(
                        "MMC_Baja-> El servidor regreso null se procesa el mensaje ",
                        EIGlobal.NivelLog.DEBUG);
                despliegaPaginaErrorURL("Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.", TXT_BAJA_DE_CUENTAS, TXT_ADMON_CTRL_CTAS_BAJA, CVE_AYUDA_S55205H, req, res);
                return 1;
            } else {
                EIGlobal.mensajePorTrace(
                        "MMC_Baja-> La informacion se recibio, se evalua el contenido",
                        EIGlobal.NivelLog.DEBUG);
                EI_Tipo trRes = new EI_Tipo();
                if (!(Result.trim().substring(0, 2).equals("OK"))) {
                    consulta = false;
                    nombreArchivo = Result.substring(Result.indexOf("/"));
                    EIGlobal.mensajePorTrace(
                            "MMC_Baja-> La consulta no fue exitosa.",
                            EIGlobal.NivelLog.INFO);
                } else {
                    consulta = true;
                    trRes.iniciaObjeto('@', '|', Result + "|");
                    nombreArchivo = trRes.camposTabla[0][2];
                    EIGlobal.mensajePorTrace(
                            "MMC_Baja-> La consulta fue exitosa.",
                            EIGlobal.NivelLog.INFO);
                }

                EIGlobal.mensajePorTrace(
                        "MMC_Baja-> Nombre de archivo obtenido:  "
                                + nombreArchivo, EIGlobal.NivelLog.INFO);

                if (!archR.copiaCUENTAS(nombreArchivo, Global.DIRECTORIO_LOCAL)) {
                    EIGlobal.mensajePorTrace(
                            "MMC_Baja-> No se realizo la copia remota desde tuxedo.",
                            EIGlobal.NivelLog.INFO);
                    nombreArchivo = "-1";
                    if (consulta) {
                        strMensaje = "<br>El archivo fue enviado pero <font color=red>no</font> se pudo obtener la <font color=green>informaci&oacute;n</font> de registro.<br><br>Verifique en la <font color=blue>Autorizaci&oacute;n y Cancelaci&oacute;n </font> de cuentas para obtener el <b>registro</b> de la informaci&oacute;n <i>procesada</i>.";
                    } else {
                        strMensaje = "<br><b><font color=red>Transacci&oacute;n</font></b> no exitosa.<br><br>El archivo <font color=blue>no</font> fue enviado correctamente.<br>Porfavor <font color=green>intente</font> en otro momento.";
                    }
                } else {
                    EIGlobal.mensajePorTrace(
                            "MMC_Baja-> Copia remota desde tuxedo se realizo correctamente.",
                            EIGlobal.NivelLog.INFO);
                    BufferedReader arc;
                    try {
                        /*
                         * Se le quita el path al archivo para operar con el, en
                         * el directorio temporal del app
                         */
                        EIGlobal.mensajePorTrace(
                                "MMC_Baja-> El archivo que se trajo (E): "
                                        + nombreArchivo, EIGlobal.NivelLog.INFO);
                        nombreArchivo = IEnlace.LOCAL_TMP_DIR
                                + nombreArchivo.substring(nombreArchivo
                                        .lastIndexOf("/"));
                        EIGlobal.mensajePorTrace(
                                "MMC_Baja-> El archivo que se abre (E): "
                                        + nombreArchivo, EIGlobal.NivelLog.INFO);

                        /* #################################### */
                        /* nombreArchivo="/tmp/1001851.txt.err.txt"; */
                        /* #################################### */

                        arc = new BufferedReader(new FileReader(nombreArchivo));
                        if ((line = arc.readLine()) != null) {
                            EIGlobal.mensajePorTrace(
                                    "MMC_Baja-> Se leyo del archivo: " + line,
                                    EIGlobal.NivelLog.INFO);
                            if (!consulta) {
                                if (line.trim().length() > 16) {
                                    strMensaje = line.trim().substring(16);
                                } else {
                                    strMensaje = "Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.";
                                }
                            } else {
                                EI_Tipo Sec = new EI_Tipo();
                                Sec.iniciaObjeto(';', '|', line + "|");
                                registroDeSolicitud = Sec.camposTabla[0][0]
                                        .trim();
                                registroDeBaja = Sec.camposTabla[0][1].trim();
                                folioRegistro = Sec.camposTabla[0][2].trim();

                                EmailDetails details = new EmailDetails();

                                int regDif = 0;
                                int regRec = 0;

                                if (registroDeBaja != null
                                        && registroDeSolicitud != null
                                        && totalRegistros != null) {
                                    regDif = Integer.parseInt(registroDeBaja)
                                            + Integer
                                                    .parseInt(registroDeSolicitud);
                                    regRec = Integer.parseInt(totalRegistros)
                                            - regDif;
                                }

                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> totalRegistros para solicitud de baja      : "
                                                + totalRegistros,
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> Cuentas pendientes para solicitud de baja  : "
                                                + registroDeSolicitud,
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> Cuentas autorizadas para  baja             : "
                                                + registroDeBaja,
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> Cuentas rechazadas para  baja              : "
                                                + regRec,
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> Numero de contrato para solicitud de baja  : "
                                                + session.getContractNumber(),
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> Nombre de contrato para solicitud de baja  : "
                                                + session.getNombreContrato(),
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> folioRegistro para solicitud de baja       : "
                                                + folioRegistro,
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja -> codErrorOper para solicitud de baja        : "
                                                + codErrorOper,
                                        EIGlobal.NivelLog.INFO);
                                try {
                                    EmailSender emailSender = new EmailSender();
                                    if (emailSender
                                            .enviaNotificacion(codErrorOper)) {
                                        // SMS y EMAIL
                                        details.setNumRegImportados(Integer
                                                .parseInt(totalRegistros));

                                        details.setNumeroContrato(session
                                                .getContractNumber());
                                        details.setRazonSocial(session
                                                .getNombreContrato());
                                        details.setNumRef(folioRegistro);
                                        details.setDetalle_Alta(registroDeBaja
                                                + "," + registroDeSolicitud
                                                + "," + regRec);
                                        details.setTipoOpCuenta("BAJA");
                                        details.setEstatusActual("ENVIADO");

                                        details.setEstatusActual(codErrorOper);
                                        emailSender.sendNotificacion(req,
                                                IEnlace.NOT_BAJA_CUENTAS,
                                                details);
                                    }

                                } catch (Exception e) {
                                    EIGlobal.mensajePorTrace(
                                            new Formateador().formatea(e),
                                            EIGlobal.NivelLog.INFO);

                                }

                                strMensaje = "<br>La informaci&oacute;n fue <font color=blue>enviada</font> y <b>procesada</b> por el sistema. <br>Para verificar el <font color=green>estatus</font> de las cuentas enviadas presione el bot&oacute;n Ver Detalle.<br><br>";

                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja-> Cuentas registradas para solicitud de baja: "
                                                + registroDeSolicitud,
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja-> Cuentas registradas para solicitud de baja: "
                                                + registroDeBaja,
                                        EIGlobal.NivelLog.INFO);
                                EIGlobal.mensajePorTrace(
                                        "MMC_Baja-> Folio de registro para el envio: "
                                                + folioRegistro,
                                        EIGlobal.NivelLog.INFO);
                            }
                        }
                        // Envio a Lynx
                        Map<String, String> cuentasLynx = (Map<String, String>) req
                                .getSession().getAttribute("CuentasLynxBaja");
                        EIGlobal.mensajePorTrace(
                                "MMC_Baja - CuentasLynxBaja: Lynx="
                                        + Global.LYNX_INICIALIZADO
                                        + ", CtasMap="
                                        + (cuentasLynx == null ? -1
                                                : cuentasLynx.size()),
                                EIGlobal.NivelLog.INFO);
                        EnlaceLynxUtils.llamadoConectorLynxBaja(fechaHoraOper,
                                cuentasLynx, campos, arc, contrato);
                        try {
                            arc.close();
                        } catch (IOException a) {
                        }
                    } catch (IOException e) {
                        EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                                EIGlobal.NivelLog.INFO);

                    }
                }
            }
        }

        /** GAE **/
        try {
            EIGlobal.mensajePorTrace("checa1->" + cuentasNoAut,
                    EIGlobal.NivelLog.INFO);
            if (cuentasNoAut != null && !cuentasNoAut.equals("")) {
                EIGlobal.mensajePorTrace("checa2->" + cuentasNoAut,
                        EIGlobal.NivelLog.INFO);
                List<String> cuentas = new ArrayList<String>();
                cuentasCanceladas = cancelaCuentas(cuentasNoAut, foliosBaja,
                        cuentas);
                if (cuentasCanceladas.intValue() > 0) {
                    Map<String, String> cuentasLynx = (Map<String, String>) req
                            .getSession().getAttribute("CuentasLynxBaja");
                    EIGlobal.mensajePorTrace(
                            "MMC_Baja - cuentasNoAut: Lynx="
                                    + Global.LYNX_INICIALIZADO
                                    + ", CtasCan="
                                    + cuentas.size()
                                    + ", CtasMap="
                                    + (cuentasLynx == null ? -1 : cuentasLynx
                                            .size()), EIGlobal.NivelLog.INFO);
                    if (Global.LYNX_INICIALIZADO && !cuentas.isEmpty()) {
                        EnlaceLynxUtils.llamadoConectorLynxBaja(cuentasLynx,
                                campos, contrato, cuentas);
                    }
                    req.setAttribute(
                            "msgCuentasCanceladasFinal",
                            "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
                                    + "<tr><td colspan=5 class=\"textabdatcla\"> "
                                    + "<br>Se cancelaron un total de <font color=red>"
                                    + cuentasCanceladas.toString()
                                    + "</font> cuentas.<br><br>nuevo estado <font color=blue>Cancelada</font>"
                                    + "</td></tr></table></td></tr>");
                } else {
                    req.setAttribute(
                            "msgCuentasCanceladasFinal",
                            "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
                                    + "<tr><td colspan=5 class=\"textabdatcla\"> "
                                    + "<font color=red><br>Error al tratar de Cancelar las cuentas.<br><br>Por favor vuelva a intentar</font>"
                                    + "</td></tr></table></td></tr>");
                }
            }

            req.getSession().removeAttribute("foliosBaja");
        } catch (Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

        }
        req.getSession().removeAttribute("CuentasLynxBaja");
        /** FIN GAE **/

        if (!consulta) {
            despliegaPaginaErrorURL(strMensaje, TXT_BAJA_DE_CUENTAS, TXT_ADMON_CTRL_CTAS_BAJA, CVE_AYUDA_S55205H, req, res);
        } else {
            /** GAE **/
            if (foliosBaja != null) {
                insertBajaLote(foliosBaja);
            }
            req.setAttribute("flagFin", new Boolean(true));
            /** FIN GAE **/

            req.setAttribute("strMensaje", strMensaje);
            req.setAttribute("nombreArchivoImp", nombreArchivoImp);
            req.setAttribute("nombreArchivoDetalle", nombreArchivo);
            req.setAttribute("registroDeSolicitud", registroDeSolicitud);
            req.setAttribute("registroDeBaja", registroDeBaja);
            req.setAttribute("totalRegistros", totalRegistros);

            req.setAttribute("newMenu", session.getFuncionesDeMenu());
            req.setAttribute("MenuPrincipal", session.getStrMenu());
            req.setAttribute("Encabezado",
                    CreaEncabezado(TXT_BAJA_DE_CUENTAS,
                            "Administraci&oacute;n y control &gt; Cuentas  &gt; Baja &gt; Env&iacute;o",
                            "s26094h", req));

            req.setAttribute(
                    "NumContrato",
                    (session.getContractNumber() == null) ? "" : session
                            .getContractNumber());
            req.setAttribute(
                    "NomContrato",
                    (session.getNombreContrato() == null) ? "" : session
                            .getNombreContrato());
            req.setAttribute("NumUsuario", (session.getUserID8() == null) ? ""
                    : session.getUserID8());
            req.setAttribute(
                    "NomUsuario",
                    (session.getNombreUsuario() == null) ? "" : session
                            .getNombreUsuario());

            EIGlobal.mensajePorTrace(
                    "MMC_Baja - agregaCuentas(): Finalizando Baja.",
                    EIGlobal.NivelLog.INFO);
            try {
                evalTemplate("/jsp/MMC_BajaEnvio.jsp", req, res);
            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }
        }
        return 1;
    }

    /**
     * Metodo de envio alternativo para enviar solo cuentas canceladas, dado que
     * no se crea archivo de tuxedo
     *
     * @param req
     *            Request de la petici?n
     * @param res
     *            Response de la petici?n
     * @return 1 en caso de que todo termine bien
     * @throws ServletException
     *             Error de tipo ServletException
     * @throws IOException
     *             Error de tipo IOException
     */
    public int enviaCuentasCancel(HttpServletRequest req,
            HttpServletResponse res) throws ServletException, IOException {
        Integer cuentasCanceladas = null;
        String cuentasNoAut = (String) req.getParameter("cuentasNoAut");
        Vector foliosBaja = (Vector) req.getSession()
                .getAttribute("foliosBaja");
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESATTR_SESSION);

        try {
            if (cuentasNoAut != null) {
                EIGlobal.mensajePorTrace("checa->" + cuentasNoAut,
                        EIGlobal.NivelLog.INFO);
                List<String> cuentas = new ArrayList<String>();
                cuentasCanceladas = cancelaCuentas(cuentasNoAut, foliosBaja,
                        cuentas);
                if (cuentasCanceladas.intValue() > 0) {
                    if (foliosBaja != null) {
                        insertBajaLote(foliosBaja);
                    }
                    Map<String, String> cuentasLynx = (Map<String, String>) req
                            .getSession().getAttribute("CuentasLynxBaja");
                    EIGlobal.mensajePorTrace(
                            "MMC_Baja - enviaCuentasCancel(): Lynx="
                                    + Global.LYNX_INICIALIZADO
                                    + ", CtasCan="
                                    + cuentas.size()
                                    + ", CtasMap="
                                    + (cuentasLynx == null ? -1 : cuentasLynx
                                            .size()), EIGlobal.NivelLog.INFO);
                    if (Global.LYNX_INICIALIZADO && !cuentas.isEmpty()) {
                        HashMap<Integer, String> campos = new HashMap<Integer, String>();
                        EnlaceLynxConstants.cargaValoresDefaultNM(campos, req);
                        EnlaceLynxUtils.llamadoConectorLynxBaja(cuentasLynx,
                                campos,
                                (session.getContractNumber() == null) ? ""
                                        : session.getContractNumber(), cuentas);

                    }
                    req.setAttribute(
                            "msgCuentasCanceladasFinal",
                            "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
                                    + "<tr><td colspan=5 class=\"textabdatcla\"> "
                                    + "<br>Se cancelaron un total de <font color=red>"
                                    + cuentasCanceladas.toString()
                                    + "</font> cuentas.<br><br>nuevo estado <font color=blue>Cancelada</font>"
                                    + "</td></tr></table></td></tr>");
                } else {
                    req.setAttribute(
                            "msgCuentasCanceladasFinal",
                            "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
                                    + "<tr><td colspan=5 class=\"textabdatcla\"> "
                                    + "<font color=red><br>Error al tratar de Cancelar las cuentas.<br><br>Por favor vuelva a intentar</font>"
                                    + "</td></tr></table></td></tr>");
                }
            }

        } catch (Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

        }

        req.setAttribute("strMensaje", "");
        req.setAttribute("nombreArchivoImp", "");
        req.setAttribute("nombreArchivoDetalle", "");
        req.setAttribute("registroDeSolicitud", COD_MODULO_INICIAL);
        req.setAttribute("registroDeBaja", COD_MODULO_INICIAL);
        req.setAttribute("totalRegistros", COD_MODULO_INICIAL);
        req.setAttribute(
                "NumContrato",
                (session.getContractNumber() == null) ? "" : session
                        .getContractNumber());
        req.setAttribute(
                "NomContrato",
                (session.getNombreContrato() == null) ? "" : session
                        .getNombreContrato());
        req.setAttribute("NumUsuario", (session.getUserID8() == null) ? ""
                : session.getUserID8());
        req.setAttribute(
                "NomUsuario",
                (session.getNombreUsuario() == null) ? "" : session
                        .getNombreUsuario());

        req.getSession().removeAttribute("foliosBaja");
        req.setAttribute("flagFin", new Boolean(false));
        req.setAttribute("newMenu", session.getFuncionesDeMenu());
        req.setAttribute("MenuPrincipal", session.getStrMenu());
        req.setAttribute("Encabezado",
                CreaEncabezado(TXT_BAJA_DE_CUENTAS,
                        "Administraci&oacute;n y control &gt; Cuentas  &gt; Baja &gt; Env&iacute;o",
                        "s26094h", req));

        EIGlobal.mensajePorTrace(
                "MMC_Baja - enviaCuentasCancel(): Finalizando Baja.",
                EIGlobal.NivelLog.INFO);
        evalTemplate("/jsp/MMC_BajaEnvio.jsp", req, res);
        return 1;
    }

    /*************************************************************************************/
    /************************************************************ generaTablaCuentasMB */
    /*************************************************************************************/
    /**
     * M?todo que realiza la construcci?n de las tablas que muestran los
     * detalles en pantalla
     *
     * @param strCtaTmp
     *            instancia de la cadena de cuenta que se mostrara
     * @param titulo
     *            cadena que expresa los encabezados de las tablas
     * @return cadena que regresa la cadena HTML con la construcci?n de la
     *         tabla
     * */
    String generaTablaCuentas(String strCtaTmp, boolean titulo) {
        EIGlobal.mensajePorTrace("generaTablaCuentas - strCtaTmp >" + strCtaTmp
                + "<", EIGlobal.NivelLog.INFO);

        if (strCtaTmp.trim().equals(""))
            return "";

        String[] titulos = { "", "Cuenta/M&oacute;vil", "Titular", "Plaza",
                "Banco", "Sucursal", "Divisa" };

        int[] datos = { 6, 2, 0, 1, 3, 5, 6, 8 };
        int[] values = { 0, 0, 1, 2, 3, 4, 5, 6 };
        int[] align = { 1, 0, 0, 0, 1, 0 };

        if (titulo) {
            datos[0] = 2;
            titulos[0] = "Cuentas Mismo Banco";
        } else {
            datos[0] = 6;
            titulos[0] = "Cuentas/M&oacute;viles Bancos Nacionales";
        }

        EI_Tipo ctaMB = new EI_Tipo();

        ctaMB.iniciaObjeto(strCtaTmp);
        return ctaMB.generaTabla(titulos, datos, values, align);
    }

    /*************************************************************************************/
    /************************************************************ generaTablaCuentasBI */
    /*************************************************************************************/
    /**
     * M?todo que realiza la construcci?n de las tablas que muestran los
     * detalles en pantalla para Cuentas Internacionales
     *
     * @param strCtaTmp
     *            instancia de la cadena de cuenta que se mostrara
     * @param titulo
     *            cadena que expresa los encabezados de las tablas
     * @return cadena que regresa la cadena HTML con la construcci?n de la
     *         tabla
     * */
    String generaTablaCuentasBI(String strCtaTmp, String titulo) {
        if (strCtaTmp.trim().equals(""))
            return "";

        String[] titulos = { "", "Cuenta", "Titular", "Clave Divisa", "Divisa",
                "Pa&iacute;s", "Banco", "Ciudad", "Clave ABA/SWIFT" };

        titulos[0] = titulo;

        /*
         * 0 numeroCuentaBI 1 nombreTitularBI 2 paisBI 3 strCiudadBI 4
         * strBancoBI 5 cveDivisaBI 6 strDivisaBI 7 numeroCuentaBI 8
         * strClaveABABI
         */

        int[] datos = { 8, 2, 0, 1, 5, 6, 2, 4, 3, 8 };
        int[] values = { 0, 0, 1, 2, 3, 4, 5, 6, 7 };
        int[] align = { 1, 0, 0, 0, 1, 0, 0, 0, 0, 0 };

        EI_Tipo ctaMB = new EI_Tipo();

        ctaMB.iniciaObjeto(strCtaTmp);
        return ctaMB.generaTabla(titulos, datos, values, align);
    }

    /*************************************************************************************/
    /********************************************************** despliegaPaginaErrorURL */
    /*************************************************************************************/
    /**
     * M?todo que despliega los errores que arroja alguna URL
     *
     * @param Error
     *            error que recibe el m?todo
     * @param param1
     *            par?metro requerido por el m?todo
     * @param param2
     *            par?metro requerido por el m?todo
     * @param param3
     *            par?metro requerido por el m?todo
     * @param request
     *            instancia de HTTPServletRequest
     * @param response
     *            instancia de HTTPServletResponse
     * @throws ServletException
     *             error en el servlet
     * @throws IOException
     *             fallo en operaciones de lectura de archivos/dispositivos
     * */
    public void despliegaPaginaErrorURL(String Error, String param1,
            String param2, String param3, HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        /************* Modificacion para la sesion ***************/
        HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESATTR_SESSION);
        EIGlobal EnlaceGlobal = new EIGlobal(session, getServletContext(), this);

        EIGlobal.mensajePorTrace(
                "MMC_Baja - despliegaPaginaErrorURL(): Generando error.",
                EIGlobal.NivelLog.INFO);

        /* Pagina de ayuda para mensajes de error y boton regresar */
        param3 = CVE_AYUDA_S55205H;

        request.setAttribute("FechaHoy",
                EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
        request.setAttribute("Error", Error);

        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("Encabezado",
                CreaEncabezado(param1, param2, param3, request));

        request.setAttribute(
                "NumContrato",
                (session.getContractNumber() == null) ? "" : session
                        .getContractNumber());
        request.setAttribute(
                "NomContrato",
                (session.getNombreContrato() == null) ? "" : session
                        .getNombreContrato());
        request.setAttribute("NumUsuario", (session.getUserID8() == null) ? ""
                : session.getUserID8());
        request.setAttribute(
                "NomUsuario",
                (session.getNombreUsuario() == null) ? "" : session
                        .getNombreUsuario());
        request.setAttribute("URL", "MMC_Baja?Modulo=0");

        RequestDispatcher rdSalida = getServletContext().getRequestDispatcher(
                "/jsp/EI_Mensaje.jsp");
        rdSalida.include(request, response);
    }

    /*************************************************************************************/
    /****************************************************************** trae Divisas */
    /*************************************************************************************/
    /**
     * M?todo que despliega los errores que arroja alguna URL
     *
     * @param tipo
     *            par?metro requerido por el m?todo
     * @param dato
     *            par?metro requerido por el m?todo
     * @return cadena con la clave de la divisa
     * */
    public String traeClave(String tipo, String dato) {
        String strOpcion = "";
        String sqlQuery = "";
        Vector result = null;

        if (tipo.equals("PAIS")) {
            sqlQuery = "SELECT CVE_PAIS from COMU_PAISES where upper(DESCRIPCION) like '%"
                    + dato.toUpperCase() + "%' order by CVE_PAIS";
        }
        if (tipo.equals("DIVISA")) {
            sqlQuery = "SELECT CVE_DIVISA FROM COMU_MON_DIVISA where upper(DESCRIPCION) like '%"
                    + dato.toUpperCase() + "%' order BY UPPER(CVE_DIVISA)";
        }
        /** GAE **/
        if (tipo.equals("STRDIVISA")) {
            sqlQuery = "SELECT upper(DESCRIPCION) FROM COMU_MON_DIVISA where CVE_DIVISA like '%"
                    + dato.toUpperCase() + "%' order BY UPPER(CVE_DIVISA)";
        }
        /** FIN GAE **/
        EI_Query BD = new EI_Query();
        result = BD.ejecutaQueryCampo(sqlQuery);
        if (result == null || result.size() < 1) {
            return dato;
        }
        strOpcion = (String) result.get(0);
        return strOpcion;

    }

    /**
     * GAE Metodo encargado de construir la tabla de folio de lotes para baja
     * masiva
     *
     * @param foliosBaja
     *            Vector con los Folios de lote por carga
     * @return table Tabla con la descripci?n detallada de los lotes a
     *         eliminar.
     */
    private String generaTablaLotes(Vector foliosBaja) {
        StringBuffer table = new StringBuffer();
        String colLin = null;

        if (foliosBaja == null) {
            return "";
        }

        if (foliosBaja.size() == 0) {
            return "";
        }
        table.append("<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");
        table.append("<tr><td class='textittab'colspan=3 align=center>Lotes de Carga de Cuentas</td></tr>");
        table.append("<tr>");
        table.append("<th class='tittabdat'><br></th>");
        table.append("<th class='tittabdat'> &nbsp; Lote &nbsp; </th>");
        table.append("<th class='tittabdat'> &nbsp; Fecha Carga &nbsp; </th>");
        table.append("<th class='tittabdat'> &nbsp; Cuentas Santander &nbsp; </th>");
        table.append("<th class='tittabdat'> &nbsp; Cuentas Otros Bancos &nbsp; </th>");
        table.append("<th class='tittabdat'> &nbsp; Cuentas Internacionales &nbsp; </th>");
        table.append("</tr>");

        for (int i = 0; i < foliosBaja.size(); i++) {
            table.append("<tr>");
            if ((i % 2) == 0) {
                colLin = "class='textabdatobs'";
            } else
                colLin = "class='textabdatcla'";
            table.append("<td "
                    + colLin
                    + "align=center><input type=checkbox name='idLote1' CHECKED></td>");
            table.append("<td " + colLin + "align=center>&nbsp;"
                    + (String) foliosBaja.get(i) + "&nbsp;<br></td>");
            table.append("<td " + colLin + "align=center>&nbsp;"
                    + getFechLote((String) foliosBaja.get(i))
                    + "&nbsp;<br></td>");
            table.append("<td "
                    + colLin
                    + "align=center>&nbsp;"
                    + getCuentasLote((String) foliosBaja.get(i), "in ('T','P')")
                    + "&nbsp;<br></td>");
            table.append("<td " + colLin + "align=center>&nbsp;"
                    + getCuentasLote((String) foliosBaja.get(i), "='E'")
                    + "&nbsp;<br></td>");
            table.append("<td " + colLin + "align=center>&nbsp;"
                    + getCuentasLote((String) foliosBaja.get(i), "='I'")
                    + "&nbsp;<br></td>");
            table.append("</tr>");
        }
        table.append("</table>");

        return table.toString();
    }

    /**
     * Metodo encargado de obtener la fecha de carga del lote
     *
     * @param folioLote
     *            Folio del lote de carga
     * @return fechLote Fecha del lote de carga
     */
    private String getFechLote(String folioLote) {
        String query = null;
        String fechLote = null;
        ResultSet rs = null;
        Connection conn = null;
        Statement sDup = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            query = "select to_char(trunc(fch_registro),'DD-MM-YYYY') as fechLote "
                    + "from eweb_aut_operctas "
                    + "where id_lote='"
                    + folioLote
                    + "'";
            EIGlobal.mensajePorTrace("MMC_Baja::getFechLote:: -> Query:"
                    + query, EIGlobal.NivelLog.INFO);
            rs = sDup.executeQuery(query);

            if (rs.next()) {
                fechLote = rs.getString("fechLote");
            }
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);
        } finally {
            try {
                rs.close();
                sDup.close();
                conn.close();
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }

        }
        return fechLote;
    }

    /**
     * Metodo funcional para obtener la fecha del lote de carga
     *
     * @param folioLote
     *            Folio de carga del lote
     * @param tipoCuenta
     *            Valor entero para identificar el tipo de cuenta
     * @return counter Contador segun el tipo de cuenta
     */
    private int getCuentasLote(String folioLote, String tipoCuenta) {
        String query = null;
        int counter = 0;
        ResultSet rs = null;
        Connection conn = null;
        Statement sDup = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            query = "Select count(clase_cuenta) as counter "
                    + "from eweb_aut_operctas " + "where id_lote='" + folioLote
                    + "' " + "and clase_cuenta " + tipoCuenta;
            EIGlobal.mensajePorTrace("MMC_Baja::getCuentasLote:: -> Query:"
                    + query, EIGlobal.NivelLog.INFO);
            rs = sDup.executeQuery(query);

            if (rs.next()) {
                counter = rs.getInt("counter");
            }
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);
        } finally {
            try {
                rs.close();
                sDup.close();
                conn.close();
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }

        }
        return counter;
    }

    /**
     * GAE Metodo encargado de regresar las tramas correspondientes a cuentas
     * Santader, otros Bancos e Internacionales que tengan estatus_oper en 'A'
     * (Autorizadas)
     *
     * @param foliosBaja
     *            Vector contenedor de los folios de los lotes
     * @param cuentasSel
     *            Cuentas Seleccionadas desde el front
     * @return tramasAut Arreglo contenedor de cuentas autorizadas Posici?n 0
     *         - Cuentas Santander Posici?n 1 - Cuentas Otros Bancos
     *         Posici?n 2 - Cuentas Internacionales
     */
    private String[] getTramas(Vector foliosBaja, String cuentasSel) {
        String[] tramasAut = new String[5];
        String idLote = null;
        String strTramaMB = "";
        String strTramaBN = "";
        String strTramaBI = "";
        Integer cuentasNoAut = new Integer(COD_MODULO_INICIAL);
        Integer numCuentasByLote = new Integer(COD_MODULO_INICIAL);
        String[] foliosActivos = null;

        foliosActivos = dameFoliosActivos(cuentasSel, foliosBaja.size());
        for (int i = 0; i < foliosBaja.size(); i++) {
            if (foliosActivos[i].equals(COD_MODULO_AGREGAR)) {
                idLote = (String) foliosBaja.get(i);
                strTramaMB += getTramasDetails(idLote, 0, "in ('P','T')");
                strTramaBN += getTramasDetails(idLote, 1, "= 'E'");
                strTramaBI += getTramasDetails(idLote, 2, "= 'I'");
                cuentasNoAut = new Integer(
                        cuentasNoAut.intValue()
                                + new Integer(getTramasDetails(idLote, 3, ""))
                                        .intValue());
                numCuentasByLote = new Integer(numCuentasByLote.intValue()
                        + dameNumCuentasByLote(idLote).intValue());
            }
        }
        tramasAut[0] = strTramaMB;
        tramasAut[1] = strTramaBN;
        tramasAut[2] = strTramaBI;
        tramasAut[3] = "" + cuentasNoAut;
        tramasAut[4] = "" + numCuentasByLote.intValue();

        return tramasAut;
    }

    /**
     * Metodo encargado de analizar que folios fueron seleccionados desde el
     * front
     *
     * @param cuentasSel
     *            Seleccion desde el front
     * @param lonVector
     *            Numero de lotes del front
     * @return foliosActivos Banderas de seleccion por lote
     */
    private String[] dameFoliosActivos(String cuentasSel, int lonVector) {
        String[] foliosActivos = null;
        int totalCuentasSel = 0;
        int totalFoliosSel = 0;

        try {
            totalCuentasSel = cuentasSel.length();
            totalFoliosSel = lonVector;
            foliosActivos = new String[totalFoliosSel];
            for (int i = 0; i < foliosActivos.length; i++) {
                foliosActivos[i] = ""
                        + cuentasSel.charAt(totalCuentasSel - totalFoliosSel);
                totalFoliosSel--;
            }

            // Solo para Debug
            for (int j = 0; j < foliosActivos.length; j++) {
                EIGlobal.mensajePorTrace("Folio " + (j + 1) + ":"
                        + foliosActivos[j], EIGlobal.NivelLog.INFO);
            }
        } catch (Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

        }
        return foliosActivos;
    }

    /**
     * Metodo encargado de ver cuantas cuentas se encuentran dentro del lote
     *
     * @param idLote
     *            Folio de Carga Masiva
     * @return El numero de cuentas dentro del lote
     */
    private Integer dameNumCuentasByLote(String idLote) {
        String query = null;
        Integer counter = null;
        int c = 0;
        ResultSet rs = null;
        Connection conn = null;
        Statement sDup = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            query = "Select count(1) as counter " + "from eweb_aut_operctas "
                    + "where id_lote='" + idLote + "'";
            EIGlobal.mensajePorTrace(
                    "MMC_Baja::dameNumCuentasByLote:: -> Query:" + query,
                    EIGlobal.NivelLog.INFO);
            rs = sDup.executeQuery(query);

            if (rs.next()) {
                c = rs.getInt("counter");
            }
            counter = new Integer(c);
            EIGlobal.mensajePorTrace(
                    "<<VALOR DE COUNTER>>" + counter.intValue(),
                    EIGlobal.NivelLog.INFO);
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);
        } finally {
            try {
                rs.close();
                sDup.close();
                conn.close();
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }

        }
        return counter;
    }

    /**
     * Metodo encargado de formar las distintas tramas que provienen de la baja
     * masiva
     *
     * @param idLote
     *            Folio de Carga Masiva
     * @param condicion
     *            Tipo de cuenta: Santander, Otros Bancos o Inter
     * @param where
     *            Condicion de consulta
     * @return Tramas formadas
     */
    private String getTramasDetails(String idLote, int condicion, String where) {
        String query = null;
        int counter = 0;
        ResultSet rs = null;

        StringBuffer strTramaMB = new StringBuffer("");
        StringBuffer strTramaBN = new StringBuffer("");
        StringBuffer strTramaBI = new StringBuffer("");

        String result = "";

        String numeroCuenta = "";
        String nombreTitular = "";
        String cvePlaza = "";
        String strPlaza = "";
        String cveBanco = "";
        String strBanco = "";
        String strSuc = "";
        String tipoCuenta = "";
        String paisBI = "";
        String strCiudadBI = "";
        String strBancoBI = "";
        String cveDivisaBI = "";
        String strDivisaBI = "";
        String numeroCuentaBI = "";
        String strClaveABABI = "";
        Connection conn = null;
        Statement sDup = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            switch (condicion) {
            case 0: // Mismo Banco
                EIGlobal.mensajePorTrace(
                        "MMC_Baja - getTramasDetails(): Mismo Banco.",
                        EIGlobal.NivelLog.INFO);

                query = "Select num_cuenta as numeroCuenta, "
                        + "titular as nombreTitular "
                        + "from eweb_aut_operctas " + "where id_lote='"
                        + idLote + "' " + "and clase_cuenta " + where
                        + " and estatus_oper='A'";
                EIGlobal.mensajePorTrace(
                        "MMC_Baja::getTramasDetails:: -> Query:" + query,
                        EIGlobal.NivelLog.INFO);
                rs = sDup.executeQuery(query);

                while (rs.next()) {
                    numeroCuenta = (rs.getString("numeroCuenta") != null) ? rs
                            .getString("numeroCuenta") : "";
                    nombreTitular = (rs.getString("nombreTitular") != null) ? rs
                            .getString("nombreTitular") : "";
                    cvePlaza = "01001";
                    strPlaza = "MEXICO, D.F.";
                    cveBanco = "BANME";
                    strBanco = "BANCO MEXICANO";
                    strSuc = "035";

                    strTramaMB.append(numeroCuenta.trim() + PIPE);
                    strTramaMB.append(nombreTitular.trim() + PIPE);
                    strTramaMB.append(cvePlaza.trim() + PIPE);
                    strTramaMB.append(strPlaza.trim() + PIPE);
                    strTramaMB.append(cveBanco.trim() + PIPE);
                    strTramaMB.append(strBanco.trim() + PIPE);
                    strTramaMB.append(strSuc.trim() + "@");
                }
                result = strTramaMB.toString();
                break;

            case 1: // Banco Nacional
                EIGlobal.mensajePorTrace(
                        "MMC_Baja - getTramasDetails(): Banco Nacional.",
                        EIGlobal.NivelLog.INFO);

                query = "Select num_cuenta as numeroCuenta, "
                        + "titular as nombreTitular, "
                        + "plaza_banxico as cvePlaza, "
                        + "cve_interme as cveBanco, " + "sucursal as strSuc, "
                        + "tipo_cuenta as tipoCuenta "
                        + ", cve_divisa as cveDivisa "
                        + "from eweb_aut_operctas " + "where id_lote='"
                        + idLote + "' " + "and clase_cuenta " + where
                        + " and estatus_oper='A'";
                EIGlobal.mensajePorTrace(
                        "MMC_Baja::getTramasDetails:: -> Query:" + query,
                        EIGlobal.NivelLog.INFO);
                rs = sDup.executeQuery(query);

                while (rs.next()) {
                    numeroCuenta = (rs.getString("numeroCuenta") != null) ? rs
                            .getString("numeroCuenta") : "";
                    nombreTitular = (rs.getString("nombreTitular") != null) ? rs
                            .getString("nombreTitular") : "";
                    cvePlaza = (rs.getString("cvePlaza") != null) ? rs
                            .getString("cvePlaza") : "";
                    strPlaza = traePlaza(cvePlaza.trim());
                    cveBanco = (rs.getString("cveBanco") != null) ? rs
                            .getString("cveBanco") : "";
                    strBanco = traeBanco(cveBanco.trim());
                    strSuc = (rs.getString("strSuc") != null) ? rs
                            .getString("strSuc") : "";
                    tipoCuenta = (rs.getString("tipoCuenta") != null) ? rs
                            .getString("tipoCuenta") : "";
                    cveDivisaBI = (rs.getString("cveDivisa") != null) ? rs
                            .getString("cveDivisa") : "MXP";

                    strTramaBN.append(numeroCuenta.trim() + PIPE);
                    strTramaBN.append(nombreTitular.trim() + PIPE);
                    strTramaBN.append(cvePlaza.trim() + PIPE);
                    strTramaBN.append(strPlaza.trim() + PIPE);
                    strTramaBN.append(cveBanco.trim() + PIPE);
                    strTramaBN.append(strBanco.trim() + PIPE);
                    strTramaBN.append(strSuc.trim() + PIPE);
                    strTramaBN.append(tipoCuenta.trim() + PIPE);
                    strTramaBN.append(cveDivisaBI.trim() + "@");

                }
                result = strTramaBN.toString();
                break;

            case 2: // Banco Internacional
                EIGlobal.mensajePorTrace(
                        "MMC_Baja - getTramasDetails(): internacionales ",
                        EIGlobal.NivelLog.INFO);

                query = "Select num_cuenta as numeroCuenta, "
                        + "titular as nombreTitular, " + "cve_pais as paisBI, "
                        + "desc_ciudad as strCiudadBI, "
                        + "desc_banco as strBancoBI, "
                        + "cve_divisa as cveDivisaBI, "
                        + "cve_aba as strClaveABABI, "
                        + "CVE_SWIFT as strClaveSWIFTBI "
                        + "from eweb_aut_operctas " + "where id_lote='"
                        + idLote + "' " + "and clase_cuenta " + where
                        + " and estatus_oper='A'";
                EIGlobal.mensajePorTrace(
                        "MMC_Baja::getTramasDetails:: -> Query:" + query,
                        EIGlobal.NivelLog.INFO);
                rs = sDup.executeQuery(query);

                while (rs.next()) {
                    numeroCuenta = (rs.getString("numeroCuenta") != null) ? rs
                            .getString("numeroCuenta") : "";
                    nombreTitular = (rs.getString("nombreTitular") != null) ? rs
                            .getString("nombreTitular") : "";
                    paisBI = (rs.getString("paisBI") != null) ? rs
                            .getString("paisBI") : "";
                    strCiudadBI = (rs.getString("strCiudadBI") != null) ? rs
                            .getString("strCiudadBI") : "";
                    strBancoBI = (rs.getString("strBancoBI") != null) ? rs
                            .getString("strBancoBI") : "";
                    cveDivisaBI = (rs.getString("cveDivisaBI") != null) ? rs
                            .getString("cveDivisaBI") : "";
                    strDivisaBI = traeClave("STRDIVISA", cveDivisaBI.trim());
                    if (paisBI.trim().equals("USA"))
                        strClaveABABI = (rs.getString("strClaveABABI") != null) ? rs
                                .getString("strClaveABABI") : "";
                    else
                        strClaveABABI = (rs.getString("strClaveSWIFTBI") != null) ? rs
                                .getString("strClaveSWIFTBI") : "";

                    strTramaBI.append(numeroCuenta.trim() + PIPE);
                    strTramaBI.append(nombreTitular.trim() + PIPE);
                    strTramaBI.append(paisBI.trim() + PIPE);
                    strTramaBI.append(strCiudadBI.trim() + PIPE);
                    strTramaBI.append(strBancoBI.trim() + PIPE);
                    strTramaBI.append(cveDivisaBI.trim() + PIPE);
                    strTramaBI.append(strDivisaBI.trim() + PIPE);
                    strTramaBI.append(numeroCuentaBI.trim() + PIPE);
                    strTramaBI.append(strClaveABABI.trim() + "@");

                }
                result = strTramaBI.toString();
                break;

            default: // Cuentas no autorizadas
                EIGlobal.mensajePorTrace(
                        "MMC_Baja - getTramasDetails(): Cuentas no Autorizadas.",
                        EIGlobal.NivelLog.INFO);

                query = "Select count(1) as cuentas "
                        + "from eweb_aut_operctas " + "where id_lote='"
                        + idLote + "' " + "and estatus_oper in ('P', 'I')";
                EIGlobal.mensajePorTrace(
                        "MMC_Baja::getTramasDetails:: -> Query:" + query,
                        EIGlobal.NivelLog.INFO);
                rs = sDup.executeQuery(query);

                numeroCuenta = COD_MODULO_INICIAL;
                if (rs.next()) {
                    numeroCuenta = "" + rs.getInt("cuentas");
                }
                result = numeroCuenta;
                break;

            }

        } catch (Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);
        } finally {
            try {
                rs.close();
                sDup.close();
                conn.close();
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }

        }

        return result;
    }

    /**
     * Metodo encargado de cancelar las cuentas que no han sido autorizadas.
     *
     * @param cuentasNoAut
     *            Cuentas a cancelar
     * @param foliosBaja
     *            Lotes
     * @param cuentasPorCancelar
     *            Regresa el listado de cuentas por cancelar
     * @return Integer con el resultado
     * @throws Exception
     *             Error de tipo Exception
     */
    private Integer cancelaCuentas(String cuentasNoAut, Vector foliosBaja,
            List<String> cuentasPorCancelar) throws Exception {
        String query;
        int counter = 0;
        int count = 0;
        Connection conn = null;
        PreparedStatement stmtConsultaCtas = null;
        PreparedStatement stmtActualizaCtas = null;
        try {
            if (!cuentasNoAut.equals("") && !cuentasNoAut.equals(COD_MODULO_INICIAL)) {
                conn = createiASConn(Global.DATASOURCE_ORACLE);
                stmtConsultaCtas = conn
                        .prepareStatement("SELECT num_cuenta, estatus_oper"
                                + " FROM eweb_aut_operctas"
                                + " WHERE id_lote = ?"
                                + " AND estatus_oper IN ('P','I')");
                stmtActualizaCtas = conn
                        .prepareStatement(query = "Update eweb_aut_operctas set "
                                + "estatus_oper='C' "
                                + "where id_lote = ? "
                                + "and estatus_oper in ('P','I')");
                for (int i = 0; i < foliosBaja.size(); i++) {
                    stmtConsultaCtas.setString(1, (String) foliosBaja.get(i));
                    ResultSet rs = stmtConsultaCtas.executeQuery();
                    while (cuentasPorCancelar != null && rs.next()) {
                        if ("P".equals(rs.getString(2))) {
                            cuentasPorCancelar.add(rs.getString(1).trim());
                        }
                    }
                    rs.close();
                    stmtActualizaCtas.setString(1, (String) foliosBaja.get(i));
                    EIGlobal.mensajePorTrace(
                            "MMC_Baja::getCuentasLote:: -> Query["
                                    + foliosBaja.get(i) + "]:" + query,
                            EIGlobal.NivelLog.INFO);
                    counter = stmtActualizaCtas.executeUpdate();

                    EIGlobal.mensajePorTrace(
                            "MMC_Baja::getCuentasLote:: -> Actualizados:"
                                    + counter + ", Acumulado Lynx: "
                                    + cuentasPorCancelar.size(),
                            EIGlobal.NivelLog.INFO);
                    count += counter;
                }
            }
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

            throw e;
        } finally {
            if (stmtConsultaCtas != null) {
                stmtConsultaCtas.close();
            }
            if (stmtActualizaCtas != null) {
                stmtActualizaCtas.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return count;
    }

    /**
     * GAE Metodo prestado para traer la descripcion del banco
     *
     * @param cveBanco
     *            Clave del banco
     * @return resBanco Descripcion del banco
     */
    private String traeBanco(String cveBanco) {
        String totales = "";
        String resBanco = cveBanco;
        PreparedStatement psCont = null;
        ResultSet rsCont = null;
        Connection conn = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            totales = "Select nombre_corto from comu_interme_fin where num_cecoban<>0 "
                    + "and cve_interme='" + cveBanco + "'";

            psCont = conn.prepareStatement(totales);
            rsCont = psCont.executeQuery();
            rsCont.next();
            resBanco = rsCont.getString(1).trim();
            // rsCont.close();
        } catch (Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

            resBanco = cveBanco;
        } finally {
            try {
                rsCont.close();
                psCont.close();
                conn.close();
            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);

            }
        }
        return resBanco;
    }

    /**
     * GAE Metodo prestado para traer la descripcion de la plaza
     *
     * @param cvePlaza
     *            Clave de la plaza
     * @return resPlaza Descripcion de la plaza
     */
    private String traePlaza(String cvePlaza) {
        String totales = "";
        String resPlaza = cvePlaza;
        PreparedStatement psCont = null;
        ResultSet rsCont = null;
        Connection conn = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            totales = "Select descripcion from comu_pla_banxico where plaza_banxico='"
                    + cvePlaza + "'";
            psCont = conn.prepareStatement(totales);
            rsCont = psCont.executeQuery();
            rsCont.next();
            resPlaza = rsCont.getString(1).trim();
            // rsCont.close();
        } catch (Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

            return resPlaza = cvePlaza;
        } finally {
            try {
                rsCont.close();
                psCont.close();
                conn.close();
            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);

            }
        }
        EIGlobal.mensajePorTrace("cuentasSerfinSanander - plaza " + resPlaza,
                EIGlobal.NivelLog.INFO);
        return resPlaza;
    }

    /**
     * GAE Metodo encargado de validar que el lote agregado contenga cuentas a
     * dar de Baja o a Cancelar
     *
     * @param folioBajaLote
     *            Identificador del lote de carga
     * @return true - si existen cuentas a dar de baja o cancelar en el lote
     *         false - si no existen cuentas a dar de baja o cancelar en el lote
     * @author CSA se actualiza para el manejo de cierre a base de Datos.
     * @since 17/11/2013
     */
    private boolean validaLote(String folioBajaLote) {
        String query = null;
        int counter = 0;
        ResultSet rs = null;
        Connection conn = null;
        Statement sDup = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            query = "Select count(*) as counter " + "from eweb_baja_lote "
                    + "where id_lote='" + folioBajaLote + "'";
            EIGlobal.mensajePorTrace(
                    "MMC_Baja::validaLote:: -> Query:" + query,
                    EIGlobal.NivelLog.INFO);
            rs = sDup.executeQuery(query);

            if (rs.next()) {
                counter = rs.getInt("counter");
            }
            EIGlobal.mensajePorTrace("<<VALOR DE COUNTER>>" + counter,
                    EIGlobal.NivelLog.INFO);
            if (counter > 0) {
                return true;
            }

        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

        } finally {
            try {
                rs.close();
                sDup.close();
                conn.close();
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }
        }
        return false;
    }

    /**
     * GAE Metodo encargado de validar que el lote agregado contenga cuentas a
     * dar de Baja o a Cancelar
     *
     * @param folioBajaLote
     *            Identificador del lote de carga
     * @return true - si existen cuentas a dar de baja o cancelar en el lote
     *         false - si no existen cuentas a dar de baja o cancelar en el lote
     * @author CSA se actualiza para el manejo de cierre a base de Datos.
     * @since 17/11/2013
     */
    private boolean validaLote2(String folioBajaLote) {
        String query = null;
        int counter = 0;
        ResultSet rs = null;
        Connection conn = null;
        Statement sDup = null;
        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            query = "Select count(*) as counter " + "from eweb_aut_operctas "
                    + "where id_lote='" + folioBajaLote + "' "
                    + "and estatus_oper in ('A','P','I')";
            EIGlobal.mensajePorTrace("MMC_Baja::validaLote2:: -> Query:"
                    + query, EIGlobal.NivelLog.INFO);
            rs = sDup.executeQuery(query);

            if (rs.next()) {
                counter = rs.getInt("counter");
            }
            EIGlobal.mensajePorTrace("<<VALOR DE COUNTER>>" + counter,
                    EIGlobal.NivelLog.INFO);
            if (counter > 0) {
                return true;
            }
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

        } finally {
            try {
                rs.close();
                sDup.close();
                conn.close();
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }

        }
        return false;
    }

    /**
     * GAE Metodo encargado de validar que el lote agregado exista
     *
     * @param folioBajaLote
     *            Identificador del lote de carga
     * @return true - si existe lote false - si no existen lote
     * @author CSA se actualiza para el manejo de cierre a base de Datos.
     * @since 17/11/2013
     */
    private boolean existeLote(String folioBajaLote) {
        String query = null;
        int counter = 0;
        ResultSet rs = null;
        Connection conn = null;
        Statement sDup = null;

        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            query = "Select count(1) as counter " + "from eweb_aut_operctas "
                    + "where id_lote='" + folioBajaLote + "'";
            EIGlobal.mensajePorTrace(
                    "MMC_Baja::existeLote:: -> Query:" + query,
                    EIGlobal.NivelLog.INFO);
            rs = sDup.executeQuery(query);

            if (rs.next()) {
                counter = rs.getInt("counter");
            }
            EIGlobal.mensajePorTrace("<<VALOR DE COUNTER de existeLote>>"
                    + counter, EIGlobal.NivelLog.INFO);
            if (counter > 0) {
                return true;
            }
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);
        } finally {
            try {
                rs.close();
                sDup.close();
                conn.close();
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }

        }
        return false;
    }

    /**
     * Metodo encargado de bitacorizar el lote que se dara de baja
     *
     * @param foliosBaja
     *            Vector con folios de cargas
     */
    private void insertBajaLote(Vector foliosBaja) {
        String query = null;
        String in = null;
        int[] i = null;

        try {
            Connection conn = createiASConn(Global.DATASOURCE_ORACLE);
            Statement sDup = conn.createStatement();
            for (int j = 0; j < foliosBaja.size(); j++) {
                in = (String) foliosBaja.get(j);
                query = "Insert into eweb_baja_lote(id_lote) " + "values('"
                        + in.trim() + "')";
                EIGlobal.mensajePorTrace("MMC_Baja::insertBajaLote:: -> Query:"
                        + query, EIGlobal.NivelLog.INFO);
                sDup.addBatch(query);
            }
            i = sDup.executeBatch();

            EIGlobal.mensajePorTrace("MMC_Baja::insertBajaLote:: -> "
                    + "Se bitacorizan lote(s) exitosamente",
                    EIGlobal.NivelLog.INFO);

            sDup.close();
            conn.close();
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);

        }
    }

    /**
     * GAE
     *
     * @param foliosBaja
     *            Vector con los folios activos
     * @param folioBajaLote
     *            Folio a registrar
     * @return true si existe folio false si no existe folio
     */
    private boolean folioRepetido(Vector foliosBaja, String folioBajaLote) {
        for (int h = 0; h < foliosBaja.size(); h++) {
            if (folioBajaLote.equals((String) foliosBaja.get(h))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Extrae el detalle del lote y lo carga en el mapa de cuentas
     *
     * @param folioBajaLote
     *            numero de lote a dar de baja
     * @param cuentas
     *            mapa con el detalle de cuentas
     * @return <code>true</code> en caso de exito en la obtencion de la
     *         informacion
     */
    private boolean extraeLote(String folioBajaLote, Map<String, String> cuentas) {
        int counter = 0;
        Connection conn = null;
        Statement sDup = null;
        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            sDup = conn.createStatement();
            StringBuilder trama = new StringBuilder();
            trama.append(
                    "SELECT NUM_CUENTA2, CLASE_CUENTA, NUM_CUENTA"
                            + ", NVL(CVE_PAIS, ' '), NVL(DESC_CIUDAD, ' '), CVE_INTERME"
                            + ", NVL( CVE_ABA, NVL(CVE_SWIFT,' ') )"
                            + " FROM EWEB_AUT_OPERCTAS" + " WHERE id_lote = '")
                    .append(folioBajaLote)
                    .append("' AND estatus_oper IN ('A','P','I')");
            String query = trama.toString();
            EIGlobal.mensajePorTrace(
                    "MMC_Baja::extraeLote:: -> Query:" + query,
                    EIGlobal.NivelLog.INFO);
            ResultSet rs = sDup.executeQuery(query);
            String tmp;
            while (rs.next()) {
                trama.setLength(0);
                tmp = rs.getString(2); // CLASE_CUENTA
                char cSep = '|';
                if (tmp != null && tmp.length() > 0) {
                    switch (tmp.charAt(0)) {
                    case 'P':
                    case 'T':
                        trama.append(trama.length() == 10 ? '4' : '0');
                        trama.append("|||||BANME|");
                        break;
                    case 'I':
                        trama.append("2|||").append(rs.getString(4))
                                .append(cSep).append(rs.getString(5))
                                .append("|||||").append(rs.getString(7));
                        break;
                    default: // 'E'
                        trama.append(trama.length() == 10 ? '4' : '1');
                        trama.append("|||||").append(rs.getString(6))
                                .append(cSep);
                    }
                }
                EIGlobal.mensajePorTrace(
                        "MMC_Baja -> cuenta baja lynx >" + rs.getString(3)
                                + "< >" + trama.toString() + "<",
                        EIGlobal.NivelLog.DEBUG);
                cuentas.put(rs.getString(3).trim(), trama.toString());
                counter++;
            }
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                    EIGlobal.NivelLog.INFO);
        } finally {
            try {
                if (conn != null) {
                    if (sDup != null) {
                        sDup.close();
                    }
                    conn.close();
                }
            } catch (SQLException e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e),
                        EIGlobal.NivelLog.INFO);
            }
        }
        return counter > 0;
    }

}