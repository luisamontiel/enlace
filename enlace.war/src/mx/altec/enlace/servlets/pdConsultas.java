package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.pdArchivoTemporal;
import mx.altec.enlace.bo.pdBeneficiario;
import mx.altec.enlace.bo.pdCFiltro;
import mx.altec.enlace.bo.pdCPago;
import mx.altec.enlace.bo.pdCalendario;
import mx.altec.enlace.bo.pdPago;
import mx.altec.enlace.bo.pdServicio;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.sql.*;


/**
 * pdConsultas is a blank servlet to which you add your own code.
 * @author Rafael Martinez Montiel
 * @version 1.0
 */

/*-----------------------------------------------------------------------------
	Incidencia:	IM323029
	Asignado a:	Norberto Vel�zquez Sep�lveda (NVS) - Mantenimiento Praxis
	Descripci�n:El archivo de exportacion de la consulta de pago directo no se
				generaba de forma completa debido a la eliminacion de registros
				que contienen un numero de pago ya existente.
	Correci�n:	En el m�todo actualizar, existe una llamada al m�todo de la
				clase pdArchivoTemporal.java de nombre exportar, a esta clase
				se le agreg� un metodo nuevo del mismo nombre que recibe otro
				par�metro de tipo ArrayList, de modo que ahora se le agrego a
				esa llamada el nuevo parametro.
	Modificado:	Noviembre 10, 2004.
-----------------------------------------------------------------------------*/

/*String fileName = arch.exportar(usuario, al);
Descripci�n de la incidencia
Descripci�n corta de lo que se le har� al archivo y por qu�
Fecha de �ltima modificaci�n que le hicieron */

public class pdConsultas extends BaseServlet {

    /** ..
     * TODO Checar, y cambiar... solo esta definida como de clase para pruebas
     */
    //short sucOpera = (short)787;

    /**
     *
     ** <code>doGet</code> is the entry-point of all HttpServlets.
     ** @param req the HttpServletRequest
     ** @param res the HttpServletResponse
     ** @exception javax.servlet.ServletException -- per spec
     ** @exception java.io.IOException -- per spec
     */

    public void doGet (HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException {
        defaultAction (req, res);
    }

    /**
     ** <code>doPost</code> is the entry-point of all HttpServlets.
     ** @param req the HttpServletRequest
     ** @param res the HttpServletResponse
     ** @exception javax.servlet.ServletException -- per spec
     ** @exception java.io.IOException -- per spec
     */

    public void doPost (HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException {
        defaultAction (req, res);
    }

    /**
     * <code>displayMessage</code> allows for a short message to be streamed
     * back to the user.  It is used by various NAB-specific wizards.
     * @param req the HttpServletRequest
     * @param res the HttpServletResponse
     * @param messageText the message-text to stream to the user.
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    public void displayMessage (HttpServletRequest req,
    HttpServletResponse res,
    String messageText)
    throws ServletException, IOException {
        res.setContentType ("text/html");
        PrintWriter out = res.getWriter ();
        out.println (messageText);
    }

    /**
     ** <code>defaultAction</code> is the default entry-point for iAS-extended
     ** @param req the HttpServletRequest for this servlet invocation.
     ** @param res the HttpServletResponse for this servlet invocation
     ** @exception javax.servlet.ServletException when a servlet exception occurs.
     ** @exception java.io.IOException when an io exception occurs during output.
     */
    public void defaultAction (HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException {
        boolean sesionvalida;
        if(!(sesionvalida=SesionValida ( req, res ))){
            return;
        }
        /* Modificacion Rafael Martinez Montiel para eliminar la variable de instancia session
         */
        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        if(null == session ){
            EIGlobal.mensajePorTrace ("pdConsultas:defaultAction() session="+ session, EIGlobal.NivelLog.DEBUG);
            req.setAttribute("Error",IEnlace.MSG_PAG_NO_DISP);
            evalTemplate ("/jsp/EI_Error.jsp",req,res);
            return;
        }

        String clavePerfil="";
        String usuario="";
        String contrato="";

        //Obtener opcion de consulta
        //0 desplegar ventana de seleccionde filtros
        //1 desplegar datos que cumplen el filtro
        //2 depurar
        //3 cancelar
        //4 expportar
        //5 modificar
        //6 imprimir
        String opc = req.getParameter ("pdcOpcion");




        if(sesionvalida) {



            if (session != null) {

                req.setAttribute ("newMenu", session.getFuncionesDeMenu ());
                req.setAttribute ("MenuPrincipal", session.getStrMenu ());
            }


            session.setModuloConsultar (IEnlace.MAlta_ordenes_pago);

            HashMap permisos = new HashMap ();

            permisos.put ("Depurar",new Boolean( session.getFacultad ( session.FAC_PADIRDEPURA_PD ) ) );//eliminar registros de la vista
            permisos.put ("Cancelar",new Boolean( session.getFacultad( session.FAC_PADIRCANCEL_PD ) ) );//cancelar pagos
            permisos.put ("Exportar", new Boolean( session.getFacultad (session.FAC_PADIREXPARC_PD ) ) );//exportar los pagos
            permisos.put ("Imprimir", new Boolean( session.getFacultad (session.FAC_PADIRIMPREP_PD ) ) );//imprimir pagos
            permisos.put ("Modificar", new Boolean( session.getFacultad (session.FAC_PADIRMODSER_PD ) ) );//Modificar pagos

            req.getSession ().setAttribute ("pdCPermisos",permisos);

            //variables de sesion
            contrato=session.getContractNumber ();
            usuario=session.getUserID8 ();
            short sucOpera = Short.parseShort (IEnlace.SUCURSAL_OPERANTE);
            clavePerfil=session.getUserProfile ();
            /*Debug Code
                     Copy Paste...*/String archivoAyuda = "s31030h";
                     req.setAttribute ("Encabezado", CreaEncabezado ("Consultas Pago Directo",
                     "Servicios &gt; Pago Directo &gt; Consultas de Pago Directo &gt; Selecci&oacute;n de filtro",
                     archivoAyuda, req));

                     if(opc == null || opc == "")opc ="0";

                     int Opcion = Integer.parseInt (opc);

                     switch(Opcion) {
                         case 0:{//Seleccionar filtros
//                           TODO: BIT CU 4251, El cliente entro al flujo
                             /*
                     		 * VSWF ARR -I
                     		 */
                             if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
                     		BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
                     		if (req.getParameter(BitaConstants.FLUJO)!=null){
                     		       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
                     		  }else{
                     		   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
                     		  }
                     		BitaTransacBean bt = new BitaTransacBean();
                     		bt = (BitaTransacBean)bh.llenarBean(bt);
                     		bt.setNumBit(BitaConstants.ES_PAGO_DIR_CONS_MODIF_ENTRA);
                     		bt.setContrato(session.getContractNumber());
                     		try{
                     		BitaHandler.getInstance().insertBitaTransac(bt);
                     		}catch (SQLException e){
                     			//e.printStackTrace();
                     		}catch(Exception e){
                     			//e.printStackTrace();
                     		}
                             }
                     		/*
                     		 * VSWF ARR -F
                     		 */
                             req.setAttribute ("Encabezado", CreaEncabezado ("Consultas Pago Directo",
                             "Servicios &gt; Pago Directo &gt; Consultas de Pago Directo &gt; Selecci&oacute;n de filtro",
                             archivoAyuda,req));
                             iniciar (contrato,clavePerfil , sucOpera , usuario ,req ,res );
                             break;
                         }
                         case 1:{//Ver Resultados de filtros
                             archivoAyuda = "s31031h";
                             req.setAttribute ("Encabezado", CreaEncabezado ("Consultas Pago Directo",
                             "Servicios &gt; Pago Directo &gt; Consultas de Pago Directo &gt; Selecci&oacute;n de filtro",
                             archivoAyuda,req));
							 EIGlobal.mensajePorTrace("La opcion 1", EIGlobal.NivelLog.DEBUG);
                             actualizar (contrato, clavePerfil, sucOpera, usuario, req, res);
                             break;
                         }
                         case 2:{//Depurar
                             archivoAyuda = "s31031h";
                             req.setAttribute ("Encabezado", CreaEncabezado ("Consultas Pago Directo",
                             "Servicios &gt; Pago Directo &gt; Consultas de Pago Directo &gt; Selecci&oacute;n de filtro",
                             archivoAyuda,req));
                             depurar (contrato,clavePerfil,sucOpera,usuario,req,res);
                             break;
                         }
                         case 3:{//Cancelar
                             archivoAyuda = "s31012h";
                             req.setAttribute ("Encabezado", CreaEncabezado ("Consultas Pago Directo",
                             "Servicios &gt; Pago Directo &gt; Consultas de Pago Directo &gt; Selecci&oacute;n de filtro",
                             archivoAyuda,req));
                             cancelar (contrato,clavePerfil,sucOpera,usuario,req,res);
                             break;
                         }
                         case 4:{//Exportar
                             break;
                         }
                         case 5:{//Modificar
                             archivoAyuda = "s31012h";
                             req.setAttribute ("Encabezado", CreaEncabezado ("Consultas Pago Directo",
                             "Servicios &gt; Pago Directo &gt; Consultas de Pago Directo &gt; Selecci&oacute;n de filtro",
                             archivoAyuda,req));
                             modificar (contrato,clavePerfil,sucOpera,usuario,req,res);
                         }/*
                case 6:{//imprimir
                    break;
            }*/
                     }
        }


    }
    /** <code>iniciar</code> obtiene los valores predeterminados del filtro,
     *                         las cuentas, y los beneficiarios
     * @param contrato es un string con el contrato actual
     * @param clavePerfil ?
     * @param sucOpera ?
     * @param usuario ?
     * @param req es el HttpServletRequest
     * @param res es el HttpServletResponse
     * @throws ServletException Per spec
     * @throws IOException Per Spec
     */
    private void iniciar (String contrato,
    String clavePerfil,
    short sucOpera,
    String usuario,
    HttpServletRequest req,
    HttpServletResponse res)
    throws ServletException, IOException {
        //Obtener Fechas

        pdCFiltro filtro = new pdCFiltro ();

        EIGlobal.mensajePorTrace("Antes de obtenr dias inhabiles", EIGlobal.NivelLog.DEBUG);
        ArrayList aInhabil = cargarDiasInh ("Consulta");
        GregorianCalendar fechaIni = new GregorianCalendar ();
		EIGlobal.mensajePorTrace(" El mes "+fechaIni.MONTH, EIGlobal.NivelLog.DEBUG);
		// MSD - 09 - Marzo - 2005 - Inicio
		/*if(fechaIni.get(GregorianCalendar.MONTH) <= 2) {
			fechaIni.roll (fechaIni.MONTH,-2);
			fechaIni.roll (fechaIni.YEAR,-1);
		}else{
			fechaIni.roll (fechaIni.MONTH,-2);
		} */
		fechaIni.set(GregorianCalendar.MONTH, fechaIni.get(GregorianCalendar.MONTH) - 2);
		// MSD - 09 - Marzo - 2005 - Inicio

        // Modificacion RMM 20021213
            fechaIni.set(java.util.Calendar.DAY_OF_MONTH, 4);
        //fin modificacion RMM 20021213

        fechaIni = pdCalendario.getAntDiaHabil (fechaIni, aInhabil);

        GregorianCalendar fechaFin =  pdCalendario.getAntDiaHabil (new GregorianCalendar (), aInhabil);
        /**
        * Modificacion para obtener una fecha 3 meses posterior al dia actual
        * solo soporta modificacion de 1 a 11 meses la variable de meses a futuro
        */
        EIGlobal.mensajePorTrace ("pdConsultas: **************calculo de Fecha Fin session******************", EIGlobal.NivelLog.DEBUG);
        int mesesFuturos = 3;

        int mesAnt = fechaFin.get(fechaFin.MONTH);

        fechaFin.roll(fechaFin.MONTH, +mesesFuturos);

        int mesAct = fechaFin.get(fechaFin.MONTH);

        if(mesAnt > mesAct){
            fechaFin.roll(fechaFin.YEAR, +1);
        }
        ///////////////////////////////////////////
        //fechaFin.roll(fechaFin.DAY_OF_MONTH, EIGlobal.NivelLog.DEBUG);
        fechaFin = pdCalendario.getAntDiaHabil (fechaFin, aInhabil);
        EIGlobal.mensajePorTrace("fechaIni=" + java.text.SimpleDateFormat.getInstance().format(fechaIni.getTime() ), EIGlobal.NivelLog.DEBUG );
        EIGlobal.mensajePorTrace("fechaFin=" + java.text.SimpleDateFormat.getInstance().format(fechaFin.getTime() ), EIGlobal.NivelLog.DEBUG );
        String tmp = "DiasInhabiles=[";
       for(java.util.ListIterator lit = aInhabil.listIterator();
       lit.hasNext();
       tmp +=java.text.SimpleDateFormat.getInstance()
           .format( ( (java.util.GregorianCalendar) lit.next() ).getTime() )  + ",");
       EIGlobal.mensajePorTrace(tmp + "]", EIGlobal.NivelLog.DEBUG);

        GregorianCalendar tmpCal = (GregorianCalendar)fechaFin.clone();
        tmpCal.roll(tmpCal.DAY_OF_MONTH, 4);
        ArrayList Calendario = pdCalendario.getCalendario (aInhabil,fechaIni, tmpCal);

        tmp = "";
/*        java.util.ListIterator lit = Calendario.listIterator();
 *       while( lit.hasNext()        ){
 *               pdMes mes = (pdMes)lit.next();
 *               tmp += mes.getMes() + " =[";
 *               java.util.ListIterator lit2 = mes.getDias().listIterator();
 *               pdDiaHabil dia = new pdDiaHabil();
 *               while(lit2.hasNext()){
 *                   dia = (pdDiaHabil)lit2.next();
 *                   if(dia.getHabil()){tmp+= "* ";}
 *                   tmp += dia.getDia() +", ";
 *               }
 *               tmp +="\n";
 *       }
 */

        GregorianCalendar fechaMax = new GregorianCalendar ();
        fechaMax = pdCalendario.getAntDiaHabil (fechaMax,aInhabil);

        tmpCal = new GregorianCalendar();
        tmpCal.set(GregorianCalendar.DAY_OF_MONTH, 4);
		/*if(tmpCal.MONTH == 2)
			tmpCal.roll(tmpCal.DAY_OF_MONTH, EIGlobal.NivelLog.DEBUG);
		else*/
			tmpCal = pdCalendario.getSigDiaHabil(tmpCal,aInhabil);
        filtro.setFechaIni ((GregorianCalendar)tmpCal.clone());
        filtro.setFechaFin (fechaFin);

        req.getSession ().setAttribute ("fechaLib", fechaIni);
        req.getSession ().setAttribute ("Calendario", Calendario);
        req.getSession ().setAttribute ("fechaLimite", fechaFin);

        EIGlobal.mensajePorTrace ("pdConsultas: **************fechaINI:*"+fechaIni, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace ("pdConsultas: **************fechaFin:*"+fechaFin, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace ("pdConsultas: **************Calendario:*"+Calendario, EIGlobal.NivelLog.DEBUG);

        //Obtener Cuentas
        pdServicio servicio = new pdServicio ();

        //servicio.getServicioTux ().setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());

        servicio.setContrato (contrato);
        servicio.setUsuario (usuario);
		servicio.setPerfil (clavePerfil);
		EIGlobal.mensajePorTrace("Se asigna la clave de perfil para servicio.", EIGlobal.NivelLog.DEBUG);

        //crear Atributos
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");

        ArrayList benef = servicio.getBeneficiarios ();
        if (null == benef || benef.size () == 0){
            EIGlobal.mensajePorTrace  ("pdConsultas::inicia - beneficiarios Nulos", EIGlobal.NivelLog.DEBUG);
        }

        EIGlobal.mensajePorTrace ("pdConsultas: **************pdCFechaMax:*"+fechaMax, EIGlobal.NivelLog.DEBUG);

        req.getSession ().setAttribute ("pdCFechaMax", fechaMax );
        req.getSession ().setAttribute ("pdcCuentas", servicio.getCuentas () );
        req.getSession ().setAttribute ("Beneficiarios" , benef);
        req.getSession ().setAttribute ("pdcCalendario",Calendario);
        req.getSession ().setAttribute ("pdCFiltro", filtro );

        evalTemplate ("/jsp/pdConsultaFiltro.jsp",req,res);


    }

    /** <CODE>actualizar</CODE>Realiza la consulta con los parametros indicados en el
     * 	request
     * @param contrato per spec
     * @param clavePerfil ?
     * @param sucOpera ?
     * @param usuario per spec
     * @param req per spec HttpServletRequest
     * @param res per spec HttpServletResponse
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    private void actualizar (String contrato,
    String clavePerfil,
    short sucOpera,
    String usuario,
    HttpServletRequest req,
    HttpServletResponse res)
    throws ServletException, IOException {
        /* Modificacion Rafael Martinez Montiel para eliminar la variable de instancia session
        */
        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        if(null == session ){
            EIGlobal.mensajePorTrace ("pdConsultas:actualizar() session=" + session, EIGlobal.NivelLog.DEBUG);
            req.setAttribute("Error",IEnlace.MSG_PAG_NO_DISP);
            evalTemplate ("/jsp/EI_Error.jsp",req,res);

            return;
        }
        EIGlobal.mensajePorTrace("La session true", EIGlobal.NivelLog.DEBUG);
        pdCFiltro filtro = new pdCFiltro ();
        if(req.getParameter ("pdcOpcion").equals ("1")){
			EIGlobal.mensajePorTrace("A asignar valores", EIGlobal.NivelLog.DEBUG);
            filtro.fill (req);
        } else if (null != req.getSession ().getAttribute ("pdCFiltro")){
            filtro = (pdCFiltro) req.getSession ().getAttribute ("pdCFiltro");
        }

        req.getSession ().setAttribute ("pdCFiltro",filtro);

        if(! filtro.validate ()){

            req.getSession ().setAttribute ("pdError",filtro.getMensaje ());
            evalTemplate ("/jsp/pdConsultaFiltro.jsp",req,res);
            return;
        }

        String query= filtro.getQuery ();

        pdServicio servicio = new pdServicio ();
        //servicio.getServicioTux ().setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());

        servicio.setContrato (contrato);
        servicio.setUsuario (usuario);
		servicio.setPerfil (clavePerfil);

		EIGlobal.mensajePorTrace("Se asigna la clave de perfil para servicio.", EIGlobal.NivelLog.DEBUG);

        ArrayList al = servicio.getConsulta (filtro);
        if ( null == al ){
            EIGlobal.mensajePorTrace  ("pdConsulta::actualiza - Resultados de consulta nulos", EIGlobal.NivelLog.DEBUG);

            req.setAttribute ("pdMensaje","No se pudo realizar la consulta o no hay resultados...");
            evalTemplate ("/jsp/pdConsultaFiltro.jsp",req,res);
            return;
        }
        req.getSession ().setAttribute ("pdConsultaRes",al);
        req.getSession ().setAttribute ("pdCBeneficiariosM",servicio.getBeneficiariosMapa ());

        req.getSession ().setAttribute ("start",new Integer (0));
        //req.getSession().setAttribute("view",new Integer(10));


          if(session.getFacultad(session.FAC_PADIREXPARC_PD ) ){
              pdArchivoTemporal arch = new pdArchivoTemporal ( "pdConsultas" + clavePerfil );

              if(null != arch){
                  EIGlobal.mensajePorTrace ("pdConsultas:actualizar - arch="+arch.getNombre(), EIGlobal.NivelLog.DEBUG);
                  ListIterator li = al.listIterator ();
                  while(li.hasNext ()){
					  arch.agrega ( new pdPago ( (pdCPago) li.next () ) );
                  }
                  // IM323029 - Inicio Cambio
                  //String fileName = arch.exportar ( usuario );
                  String fileName = arch.exportar(usuario, al);
                  // IM323029 - Fin Cambio
                  EIGlobal.mensajePorTrace ("pdConsultas:actualizar - fileName=" + fileName, EIGlobal.NivelLog.DEBUG);

                  if( ! fileName.startsWith ("Error") ){

                      req.getSession ().setAttribute ("pdCArchivo",fileName);

                  } else {
                      EIGlobal.mensajePorTrace ("pdConsultas:actualizar - ret="+ fileName, EIGlobal.NivelLog.ERROR);
                      EIGlobal.mensajePorTrace ("pdConsultas:actualizar - arch  no pudo ser exportado...", EIGlobal.NivelLog.ERROR);
                  }
                  //                    TODO: BIT CU 4251, B1
				                        /*
				                 		 * VSWF ARR -I
				                		 */
				                          if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				                          try{
				                		BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
				                		BitaTransacBean bt = new BitaTransacBean();
				                		bt = (BitaTransacBean)bh.llenarBean(bt);
				                		bt.setNumBit(BitaConstants.ES_PAGO_DIR_CONS_MODIF_REALIZA_CONS_PAGO_DIR);
				                		bt.setContrato(contrato);
				                		bt.setServTransTux("COOP");
				                		bt.setIdErr("COOP0000");
				                		bt.setNombreArchivo(fileName);
				                		BitaHandler.getInstance().insertBitaTransac(bt);
				                		}catch (SQLException e){
				                			//e.printStackTrace();
				                		}catch(Exception e){
				                			//e.printStackTrace();
				                		}
				                          }
				                		/*
				                		 * VSWF ARR -F
              		 */
              } else {
                  EIGlobal.mensajePorTrace  ("pdConsultas:actualizar - arch  no pudo ser creado...", EIGlobal.NivelLog.ERROR);
              }
          }

          evalTemplate ("/jsp/pdCReporte.jsp",req,res);

          //EIGlobal.mensajePorTrace ("pdConsultas:: actualiza - sale");
    }

    /** <code>depurar</code>Elimina de la vista los registros seleccionados
     * @param contrato es el contrato actual
     * @param req es el HttpServletRequest
     * @param res Es el HttpServletResponse
     * @throws ServletException per spec
     * @throws IOException per spec
     * @param clavePerfil es el usuario actual
     * @param sucOpera ...
     * @param usuario usuario actual
     */
    protected void depurar (String contrato,
    String clavePerfil,
    short sucOpera,
    String usuario,
    HttpServletRequest req,
    HttpServletResponse res)
    throws ServletException, IOException{
        int start =0;
        int view = 10;
        ArrayList result = new ArrayList ();
        try{
            if(null != req.getParameter ("start") ){
                start = Integer.parseInt (req.getParameter ("start"));

            }
        } catch (NumberFormatException e){

        }

        result = (ArrayList) req.getSession ().getAttribute ("pdConsultaRes");

        if(null != req.getParameter ("reg")){
            String [] registros = req.getParameterValues ("reg");

            if(null == result) return;

            if(null != registros || 1 > registros.length ){
                try{
                    //EIGlobal.mensajePorTrace ("pdConsultas:depura - antes eliminar result.size()=" + result.size());
                    //EIGlobal.mensajePorTrace ("pdConsultas:depura - reg.length = " + registros.length);
                    for( int i = 0; registros.length > i; ++i){
                        result.remove (Integer.parseInt (registros[i]));
                    }
                } catch (NumberFormatException e){
                    EIGlobal.mensajePorTrace ("pdConsultaS:depura - error en el formato del numero en \"reg\"", EIGlobal.NivelLog.ERROR);
                }
            } else {
                EIGlobal.mensajePorTrace ("pdConsultas:depura - no hay registros seleccionados...", EIGlobal.NivelLog.ERROR);
            }

            req.getSession ().setAttribute ("pdConsultaRes",result);
        }




          while(result.size () <= start ){

              start -= view;
          }
          if(start < 0) start = 0;

          req.getSession ().removeAttribute ("start");

          req.getSession ().setAttribute ("start",new Integer (start));

          EIGlobal.mensajePorTrace  ( getClass ().getName () + " pdCArchivo =" + req.getParameter ("pdCArchivo") , EIGlobal.NivelLog.DEBUG);
          if ( req.getSession ().getAttribute ("pdCArchivo") == null ||
          req.getSession ().getAttribute ("pdCArchivo").equals ("") &&
          (req.getParameter ("pdCArchivo") != null && ! req.getParameter ("pdCArchivo").equals ("") ) ){
              req.getSession ().setAttribute ("pdCArchivo",req.getParameter ("pdCArchivo") );
          }

          evalTemplate ("/jsp/pdCReporte.jsp",req,res);

    }

    /** <CODE>cancelar</CODE>
     * obtiene los registros seleccionadoa, y cancela estos
     * @param contrato contrato actual
     * @param clavePerfil perfil actual
     * @param sucOpera sucursal actual
     * @param usuario usuario actual
     * @param req HttpServletRequest per spec
     * @param res HttpServletResponse per spec
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    protected void cancelar (String contrato,
    String clavePerfil,
    short sucOpera,
    String usuario,
    HttpServletRequest req,
    HttpServletResponse res)
    throws ServletException,IOException{
        //EIGlobal.mensajePorTrace ("pdConsultas::cancelar() - inicia");
        String [] selec;
        ArrayList regs;
        ArrayList pagos = new ArrayList ();
        try{
            if(null == req.getParameter ("reg")){
                //EIGlobal.mensajePorTrace ("pdConsultas:cancelar - reg es NULL");
                throw new Exception ("No hay ning&uacute;n registro seleccionado, NULL");
            }
            //EIGlobal.mensajePorTrace ("pdConsultas:cancelar - obteniendo registros a eliminar");
            selec = req.getParameterValues ("reg");
            if(selec.length < 1){
                //EIGlobal.mensajePorTrace ("pdConsultas:cancelar - reg no contiene elementos");
                throw new Exception ("No hay ning&uacute;n registro seleccionado.");
            }
            regs = (ArrayList) req.getSession ().getAttribute ("pdConsultaRes");

            if(null == regs ){
                throw new Exception ("Se perdieron los Registros, vuelva a realizar la consulta...");
            }

            int i=0;
            for(i=0;i<selec.length;++i){
                try{
                    pdCPago pago =(pdCPago) regs.get (Integer.parseInt (selec[i]));
                    char c = pago.getStatus ();


                    if('C' == c || 'V' == c || 'L' == c || 'M' == c || 'N' == c){
                        /*EIGlobal.mensajePorTrace ("pdConsultas: ( 'I' != c && 'R' != c )=" +
                                    ( 'I' != c && 'R' != c ));
                        EIGlobal.mensajePorTrace ("pdConsultas: ('C' == c || 'V' == c || 'L' == c || 'M' == c || 'N' == c)=" +
                                    ('C' == c || 'V' == c || 'L' == c || 'M' == c || 'N' == c));*/

                        throw new Exception ("No se puede eliminar el pago " + pago.getNoOrden () );
                    }
                    pagos.add ( pago );

                } catch (NumberFormatException e){
                    EIGlobal.mensajePorTrace  ("pdConsultas:cancelar - Error en el formato del numero", EIGlobal.NivelLog.ERROR);
                }
            }
            if(pagos.size () < 1){
                EIGlobal.mensajePorTrace  ("pdConsultas::cancelar - No se pudo seleccionar ningun registro", EIGlobal.NivelLog.ERROR);
                throw new Exception ("No se pudo realizar la operaci&oacute;n");
            }
        } catch (Exception e){
                  req.setAttribute ("pdError",e.getMessage ());
                  evalTemplate ("/jsp/pdCReporte.jsp",req,res);
                  EIGlobal.mensajePorTrace ("pdConsultas::cancelar - Mensaje:" + e.getMessage(), EIGlobal.NivelLog.ERROR);
                  EIGlobal.mensajePorTrace ("pdConsultas::cancelar() - sale con error", EIGlobal.NivelLog.ERROR);
                  return;
        }

        pdServicio servicio = new pdServicio ();
        //servicio.getServicioTux ().setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());

        servicio.setContrato ( contrato );
        servicio.setUsuario ( usuario );
		servicio.setPerfil ( clavePerfil );
		EIGlobal.mensajePorTrace("Se asigna la clave de perfil para servicio.", EIGlobal.NivelLog.DEBUG);

        //EIGlobal.mensajePorTrace ("pdConsultas:cancelar - llamada a pdServicio");
        servicio.setRequest(req);
        ArrayList result = servicio.cancelaPagos ( pagos );
        String msg  = "";
        ListIterator li = result.listIterator ();
        while(li.hasNext ()){
            msg += (String) li.next ().toString ().trim () + "<br>\\n";
            EIGlobal.mensajePorTrace("Aqui entre en pdConsulta cuando sse cancela", EIGlobal.NivelLog.DEBUG);
        }

        //EIGlobal.mensajePorTrace ("pdConsultas::cancela - msg =" + msg);

        //EIGlobal.mensajePorTrace ("pdConsultas::cancelar() - sale OK");
        req.setAttribute ("pdMensaje", msg);

        actualizar (contrato,clavePerfil,sucOpera,usuario,req,res);
    }

    /** <CODE>modificar</CODE>
     * hace la modificacion de un pago
     * @param contrato contrato actual
     * @param clavePerfil perfil actual
     * @param sucOpera sucursal actual
     * @param usuario usuario actual
     * @param req HttpServletRequest per spec
     * @param res HttpServletResponse per spec
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    protected void modificar (String contrato,
    String clavePerfil,
    short sucOpera,
    String usuario,
    HttpServletRequest req,
    HttpServletResponse res)
    throws ServletException,IOException{
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        int i = Integer.parseInt (req.getParameter ("reg"));
		ArrayList aInhabil = cargarDiasInh ("Modificar");
        ArrayList al = (ArrayList) req.getSession ().getAttribute ("pdConsultaRes");
		GregorianCalendar Hoy = new GregorianCalendar();
        pdCPago pago = (pdCPago) al.get (i);
        ArrayList Calendario = pdCalendario.getCalendario (Hoy, aInhabil );
        pdPago tmp = new pdPago (pago);
		HashMap Permisos = new HashMap ();
		Permisos.put ("Envio", new Boolean (session.getFacultad ("PADIRREGLIN"))); //Facultad Envio de Regsitro en Linea
		Permisos.put ("bensinreg", new Boolean (session.getFacultad ("PADIRBENORE")));    //Facultad de envio de Beneficiarios no regsitrados	 "PADIRBENORE"

		req.getSession ().setAttribute("Permisos",Permisos);
        req.setAttribute ("Pago",tmp);
		req.getSession ().setAttribute("fechaLib",Hoy);
		req.getSession ().setAttribute("Calendario",Calendario);
                /* para que pdLinea pueda obtener los beneficiarios */
        req.getSession ().setAttribute ("mapaBen", req.getSession ().getAttribute ("pdCBeneficiariosM"));

        evalTemplate ("/jsp/pdLinea.jsp",req,res);
    }
    /** <CODE>cargarDiasInh</CODE>Obtiene los dias inhabiles
     * @return devuelve un ArrayList que contiene un String por cada dia inhabil
     */
    private ArrayList cargarDiasInh (String opcion){

        String qry = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha";
        if(opcion.equals("Consulta"))
			qry = qry +" <= sysdate ";
		else
			qry = qry +" >= sysdate ";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList aResult =  new ArrayList ();
        try{
            conn = createiASConn (Global.DATASOURCE_ORACLE);
            /*            Connection conn = createiASConn("jdbc/enlace");            */

            ps = conn.prepareStatement (qry);
            rs = ps.executeQuery ();

            while(rs.next ()){
                String dia = rs.getString (1);
                String mes = rs.getString (2);
                String anio = rs.getString (3);
                GregorianCalendar fecha = new GregorianCalendar (Integer.parseInt (anio),
                Integer.parseInt (mes) -1,
                Integer.parseInt (dia));
				//EIGlobal.mensajePorTrace("Fecha inhabil "+java.text.SimpleDateFormat.getInstance().format(fecha.getTime() ), EIGlobal.NivelLog.DEBUG);
                aResult.add (fecha);
            }//aResult.add(new GregorianCalendar());
           
        }catch(SQLException sqle){
            EIGlobal.mensajePorTrace  ( "pdFechas.java::cargarDiasInh()::" +
            "ERROR: Creando conexión en:" , EIGlobal.NivelLog.ERROR);
            //sqle.printStackTrace ();
        } finally {
		EI_Query.cierraConexion(rs, ps, conn);
        }

        return aResult;
    }

    /** <CODE>evalTemplate</CODE>hace un forward al template designado
     * @param template es el template al cual se hara el forward
     * @param req HttpServletRequest per spec
     * @param res HttpServletResponse per spec
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    public void evalTemplate (String template, HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException {
        getServletContext ().getRequestDispatcher (template).forward (req,res);
    }


//    /**
//     * Metodo para obtener un calendario con las fechas habiles en un periodo de tiempo definido RMM 20021213
//     * @param diasInhabiles Lista con los dias inhabiles
//     * @param fechaIni fecha inicial para el calendario
//     * @param fechaFin fecha final para el calendario
//     * @return Lista con los dia habiles
//     * @deprecated solo para pruebas, se debe remover
//     */
//     public static ArrayList getCalendario(java.util.List diasInhabiles, GregorianCalendar fechaIni, GregorianCalendar fechaFin){
//         ArrayList retValue = new ArrayList();
//         GregorianCalendar fecha = (GregorianCalendar) fechaIni.clone ();
//        pdDiaHabil DiaTemp;
//        pdMes MesTemp;
//        int MesesExtra = 3;
//        int MesAct = fecha.get (GregorianCalendar.MONTH);;
//        int DiasExtra = fechaIni.get (GregorianCalendar.DAY_OF_MONTH);
//        GregorianCalendar fechaLimite = (GregorianCalendar) fechaFin.clone();
//		EIGlobal.mensajePorTrace("fechaFin  "+java.text.SimpleDateFormat.getInstance().format(fechaLimite.getTime() ) , EIGlobal.NivelLog.DEBUG);
//		EIGlobal.mensajePorTrace("fechaIni  "+java.text.SimpleDateFormat.getInstance().format(fecha.getTime())+"\n" , EIGlobal.NivelLog.DEBUG);
//        if (diasInhabiles == null) return null;
//
//
//        MesTemp = new pdMes (fecha.get (GregorianCalendar.MONTH),
//                                fecha.get (GregorianCalendar.YEAR),
//                                new ArrayList ());
//
//        while (fechaLimite.get (GregorianCalendar.DAY_OF_YEAR)
//                != fecha.get (GregorianCalendar.DAY_OF_YEAR)) {
//            if (MesAct != fecha.get (GregorianCalendar.MONTH)) {
//                retValue.add (MesTemp);
//                MesAct = fecha.get (GregorianCalendar.MONTH);
//                MesTemp = new pdMes (fecha.get (GregorianCalendar.MONTH),
//                                        fecha.get (GregorianCalendar.YEAR),
//                                        new ArrayList ());
//            }
//            if (!contiene (diasInhabiles, fecha)) {
//                if ((fecha.get (GregorianCalendar.DAY_OF_WEEK) == 1) ||
//                    (fecha.get (GregorianCalendar.DAY_OF_WEEK) == 7)) {
//                    DiaTemp = new pdDiaHabil (false, (short) fecha.get(GregorianCalendar.DAY_OF_MONTH),
//                                fecha.get (GregorianCalendar.DAY_OF_WEEK));
//                } else {
//                    DiaTemp = new pdDiaHabil (true, (short) fecha.get (GregorianCalendar.DAY_OF_MONTH),
//                                fecha.get (GregorianCalendar.DAY_OF_WEEK));
//                }
//            } else {
//                DiaTemp = new pdDiaHabil (false, (short) fecha.get(GregorianCalendar.DAY_OF_MONTH),
//                            fecha.get (GregorianCalendar.DAY_OF_WEEK));
//            }
//            MesTemp.agregaDia (DiaTemp);
//            fecha.set (GregorianCalendar.DAY_OF_YEAR, fecha.get (GregorianCalendar.DAY_OF_YEAR) + 1);
//        }
//        if (!retValue.contains (MesTemp)) retValue.add (MesTemp);
//        return retValue;
//     }

//      /**
//     * Metodo para ver si un dia se encuentra en una lista de dias
//     * @param diasInhabiles Lista con los dias inhabiles
//     * @param fecha Fecha que se quiere buscar en la lista
//     * @return true si la fecha esta en la lista de lo contrario false
//     */
//    protected static boolean contiene (java.util.List diasInhabiles, GregorianCalendar fecha) {
//        ListIterator liDiasInhabiles = diasInhabiles.listIterator ();
//        GregorianCalendar fechaComp;
//        while (liDiasInhabiles.hasNext ()) {
//            fechaComp = (GregorianCalendar) liDiasInhabiles.next ();
//            if ((fechaComp.get(fechaComp.YEAR) == fecha.get(fecha.YEAR) )&&
//                (fechaComp.get (GregorianCalendar.DAY_OF_YEAR) ==
//                fecha.get (GregorianCalendar.DAY_OF_YEAR)) ){
//                //    + java.text.SimpleDateFormat.getInstance().format(fechaComp.getTime()));
//                return true;
//            }
//        }
//        return false;
//    }

}