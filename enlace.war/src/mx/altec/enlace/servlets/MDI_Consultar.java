package mx.altec.enlace.servlets;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.TranInterbancaria;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.TransferenciaInterbancariaBUS;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.TranInterConstantes;

/**
 * 
 * @author Z708764
 *
 */
public class MDI_Consultar extends BaseServlet
{
	/**
	 * serialVersionUID serial version de compilacion
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TIPO_MONEDA = "tipoMoneda";
	/**
	 * sucursalOpera 787;
	 */
	short  sucursalOpera = (short)787;
	/**
	 * Cuenta_seleccionada Cuenta Seleccionada
	 */
	String Cuenta_seleccionada;
	/**
	 * Implementa doGet del servlet
	 * @param req request
	 * @param res response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}
	/**
	 * Implementa doPost del servlet
	 * @param req request
	 * @param res response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}
	/**
	 * Metodo default de la clase
	 * @param req request
	 * @param res response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    String clavePerfil = "";
	    String usuario     = "";
	    String contrato    = "";
	    String Trama       = "";
	    String Result      = "";
	    String[][] arrayCuentasCargo = null;
	    /************* Modificacion para la sesion ***************/
	    HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(TranInterConstantes.SESSION);
	    EIGlobal.mensajePorTrace("MDI_Consultar - execute(): Entrando a Consultas de Dep. Inter. ", EIGlobal.NivelLog.INFO);
	    String modulo=(String) getFormParameter(req, TranInterConstantes.MODULO);
		EIGlobal.mensajePorTrace("MDI_Consultar - execute(): Modulo= ["+ modulo + "]", EIGlobal.NivelLog.INFO);
	    boolean sesionvalida = SesionValida( req, res );
	    if(sesionvalida) {
	        //Variables de sesion ...
	        contrato      = session.getContractNumber();
	        sucursalOpera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
	        usuario       = session.getUserID8();
	        clavePerfil   = session.getUserProfile();
			EIGlobal.mensajePorTrace("MDI_Consultar - execute(): Iniciando operaciones ", EIGlobal.NivelLog.INFO);
	        if(TranInterConstantes.CERO.equals(modulo)){
	            iniciaConsultar( req, res );
	        }else if("1".equals(modulo)) {
	            generaTablaConsultar(arrayCuentasCargo,Result,Trama,clavePerfil, usuario,contrato, req, res );
	        }else if("2".equals(modulo)) {
	            generaTablaComisiones(arrayCuentasCargo,Result,Trama,clavePerfil, usuario,contrato, req, res );
	        }else if("3".equals(modulo)){
	        	generaArchivoParaExportar(usuario,req,res);
	        }
	    }else if(sesionvalida) {
	    	req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
	    	evalTemplate(IEnlace.ERROR_TMPL, req, res);
	    }
	    EIGlobal.mensajePorTrace("MDI_Consultar - execute(): Saliendo de Consultas de Dep. Inter.", EIGlobal.NivelLog.INFO);
    }
	/**
	 * Inicio de Consultas Modulo = 0
	 * @param req request
	 * @param res response
	 * @return int valor exitoso
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public int iniciaConsultar(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		StringBuffer strTabla = new StringBuffer();
	    StringBuffer varFechaHoy = new StringBuffer();
	    String strInhabiles = "";
	    /************* Modificacion para la sesion ***************/
	    HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(TranInterConstantes.SESSION);
	    EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this);
	    CalendarNomina myCalendarNomina;
	    myCalendarNomina= new CalendarNomina();
	    String VarFechaHoy = "";
	    VarFechaHoy = myCalendarNomina.armaArregloFechas();
	    req.setAttribute( "VarFechaHoy", VarFechaHoy);
	    strTabla.append("\n<p><br>");
	    strTabla.append("\n  <table width=250 border=0 cellspacing=2 cellpadding=3 class='tabfonbla' align=center>");
	    strTabla.append(TranInterConstantes.PRIMERAPERTURA);
	    strTabla.append("\n     <td class='tittabdat' colspan=2> Periodo a consultar</td>");
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	    strTabla.append("\n    <tr align=center>");
	    strTabla.append("\n     <td class='textabdatcla' valign=top colspan=2>");
	    strTabla.append("\n      <table width=100% border=0 cellspacing=5 cellpadding=0>");
	    //FSW Vector ARS Inicio SPID
	    strTabla.append(TranInterConstantes.PRIMERAPERTURA);
	    strTabla.append("\n          <td class='tabmovtex' align='center'>");
	    strTabla.append("\n          Pesos:");
	    strTabla.append("\n          <input type=radio name='tipoMoneda' id='tipoMonedaMXN' value='MXN' checked onClick='tipoConsultaDivisa('MXN');'/>");
	    strTabla.append("\n          </td>");
	    strTabla.append("\n          <td class='tabmovtex' align='center'>");
	    strTabla.append("\n          Dolares:");
	    strTabla.append("\n          <input type=radio name='tipoMoneda' id='tipoMonedaUSD' value='USD' onClick='tipoConsultaDivisa('USD');'/>");
	    strTabla.append("\n          </td>");
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	  //FSW Vector ARS Fin SPID
	    strTabla.append(TranInterConstantes.PRIMERAPERTURA);
	    strTabla.append("\n          <td class='textabdatcla'>");
	    strTabla.append("\n          <input type=radio name='Busqueda' id='BusquedaHOY' value='HOY' onClick='cambiaFechas(this);' checked>");
	    strTabla.append("\n          Del d&iacute;a</td>");
	    strTabla.append("\n        <td>&nbsp;</td>");
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	    strTabla.append(TranInterConstantes.PRIMERAPERTURA);
	    strTabla.append("\n        <td class='textabdatcla'>");
	    strTabla.append("\n          <input type=radio name='Busqueda' id='BusquedaHIS' value='HIS' onClick='cambiaFechas(this);'>");
	    strTabla.append("\n          Hist&oacute;rico </td>");
	    strTabla.append("\n        <td>&nbsp;</td>");
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	    strTabla.append(TranInterConstantes.PRIMERAPERTURA);
	    strTabla.append("\n        <td class='textabdatcla' align=right> De la fecha:</td>");
	    strTabla.append(TranInterConstantes.ETIQUETATDABRE);
	    strTabla.append("\n          <input type=text name=FechaIni value='"+EnlaceGlobal.fechaHoy("dd/mm/aaaa")+"' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(0);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Inicial' align='absmiddle'></a>");
	    strTabla.append(TranInterConstantes.ETIQUETATDCIERRA);
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	    strTabla.append(TranInterConstantes.PRIMERAPERTURA);
	    strTabla.append("\n       <td class='textabdatcla' align=right>A la fecha:</td>");
	    strTabla.append(TranInterConstantes.ETIQUETATDABRE);
	    strTabla.append("\n          <input type=text name=FechaFin value='"+EnlaceGlobal.fechaHoy("dd/mm/aaaa")+"' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(1);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Final' align='absmiddle'></a>");
	    strTabla.append(TranInterConstantes.ETIQUETATDCIERRA);
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	    strTabla.append(TranInterConstantes.CIERRETABLA);
	    strTabla.append(TranInterConstantes.ETIQUETATDCIERRA);
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	    strTabla.append(TranInterConstantes.CIERRETABLA);
	    strTabla.append("\n  <input type=hidden name=Numero value="+Integer.toString(Global.MAX_REGISTROS)+">");
	    strTabla.append("\n  <table border=0 cellspacing=0 cellpading=0 align=center>");
	    strTabla.append(TranInterConstantes.PRIMERAPERTURA);
	    strTabla.append("\n      <td colspan=3 align=center><br><a href='javascript:Enviar();'><img src='/gifs/EnlaceMig/gbo25220.gif' border=0></a></td>");
	    strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
	    strTabla.append(TranInterConstantes.CIERRETABLA);

	    // Principio Christian Barrios Osornio (CBO) IM382433  27/01/03
	    Cuenta_seleccionada= getFormParameter(req, "textCargoReg");
	    // Fin Christian Barrios Osornio (CBO) IM382433  27/01/03

	    /********************************** Arreglos de fechas para 'Hoy' .... */
	    varFechaHoy.append("  //Fecha De Hoy");
	    varFechaHoy.append("\n  dia=new Array("+generaFechaAnt(TranInterConstantes.DIA)+","+generaFechaAnt(TranInterConstantes.DIA)+");");
	    varFechaHoy.append("\n  mes=new Array("+generaFechaAnt(TranInterConstantes.MES)+","+generaFechaAnt(TranInterConstantes.MES)+");");
	    varFechaHoy.append("\n  anio=new Array("+generaFechaAnt(TranInterConstantes.ANIO)+","+generaFechaAnt(TranInterConstantes.ANIO)+");");
	    varFechaHoy.append("\n\n  //Fechas Seleccionadas");
	    varFechaHoy.append("\n  dia1=new Array("+generaFechaAnt(TranInterConstantes.DIA)+","+generaFechaAnt(TranInterConstantes.DIA)+");");
	    varFechaHoy.append("\n  mes1=new Array("+generaFechaAnt(TranInterConstantes.MES)+","+generaFechaAnt(TranInterConstantes.MES)+");");
	    varFechaHoy.append("\n  anio1=new Array("+generaFechaAnt(TranInterConstantes.ANIO)+","+generaFechaAnt(TranInterConstantes.ANIO)+");");
	    varFechaHoy.append("\n  Fecha=new Array('"+generaFechaAnt(TranInterConstantes.FECHAX)+"','"+generaFechaAnt(TranInterConstantes.FECHAX)+"');");

	    req.setAttribute("DiasInhabiles",strInhabiles);
	    req.setAttribute("VarFechaHoy",varFechaHoy.toString());
	    req.setAttribute(TranInterConstantes.MODULO,"1");
	    req.setAttribute("Tabla",strTabla.toString());
	    req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
	    req.setAttribute("FechaAyer",generaFechaAnt(TranInterConstantes.FECHAX));
	    req.setAttribute("FechaDia",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
	    req.setAttribute("FechaPrimero","01/"+EnlaceGlobal.fechaHoy("mm/aaaa"));

	    req.setAttribute("MenuPrincipal", session.getStrMenu());
	    req.setAttribute("newMenu", session.getFuncionesDeMenu());
	    req.setAttribute("Encabezado", CreaEncabezado("Consultas de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Consultas","",req));

	    req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
	    req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
	    req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
	    req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
	    if ("ON".equals(Global.USAR_BITACORAS.trim())) {
	    	try{
	    		BitaConstants.archivo = getServletContext().getRealPath(TranInterConstantes.DIAGONALINVERTIDA) + "logBitacora.txt";
	    		BitaTransacBean bt = new BitaTransacBean();
			    BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
			    bt = (BitaTransacBean) bh.llenarBean(bt);
			    if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.EC_MOV_TARJETA)) {
			    	bt.setNumBit(BitaConstants.EC_MOV_TARJETA_CONSULTA_MOVS_TARJETA);
			    	if(session.getContractNumber() != null) {
			    		bt.setContrato(session.getContractNumber());
			    	}
			    }
			    if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_INTERBANCARIAS)) {
			    	bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_INICIA_CONSULTA_ANTERIORES);
			    	if(session.getContractNumber() != null) {
			    		bt.setContrato(session.getContractNumber());
			    	}
			    }
			    if(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION != req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) && req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO) {
			    	bt.setContrato(session.getContractNumber());
			    }
			    BitaHandler.getInstance().insertBitaTransac(bt);
	    	}catch(SQLException e){
	    		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
	    	}catch(Exception e){
	    		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
	    	}
	    }
	    evalTemplate("/jsp/MDI_Consultar.jsp", req, res);
	    return 1;
	}
	/**
	 * Metodo que realiza la generacion de la tabla
	 * @param arrayCuentasCargo arreglo de cuentas
	 * @param Result resultado de la consulta
	 * @param Trama trama de consulta
	 * @param clavePerfil clave de perfil
	 * @param usuario usuario
	 * @param contrato contrato
	 * @param req request
	 * @param res response 
	 * @return int valor exitoso
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public int generaTablaConsultar(String[][] arrayCuentasCargo,String Result, String Trama, String clavePerfil, String usuario,
			String contrato, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace("MDI_Consultar - execute(): Preparando la consulta", EIGlobal.NivelLog.INFO);

		StringBuffer botones = new StringBuffer();
		StringBuffer tablaRegistros = new StringBuffer();
		String fechaIniSer = "";
		String fechaFinSer = "";
		String fechaIni = (String) getFormParameter(req, "FechaIni");
		String fechaFin = (String) getFormParameter(req, "FechaFin");
		String tipoBusqueda = (String) getFormParameter(req, "Busqueda");
		String tipoMoneda = (String) getFormParameter(req, TIPO_MONEDA);//ARS VECTOR SPID
		int pagina = Integer.parseInt((String) getFormParameter(req, TranInterConstantes.PAGINAX));
		int regPagina = Integer.parseInt((String) getFormParameter(req,TranInterConstantes.NUMERO));
		int totalLineas = 0;
		int fin = 0;
		int paginaTMP = 0;

		if (pagina < 0) {
			pagina = 0;
		}

		paginaTMP = pagina - 2;
		paginaTMP = (paginaTMP > -1) ? paginaTMP : 0;
		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(TranInterConstantes.SESSION);
		EIGlobal EnlaceGlobal = new EIGlobal(session, getServletContext(), this);
		if ("HIS".equals(tipoBusqueda)) {
			fechaIniSer = fechaIni.substring(0, 2) + fechaIni.substring(3, 5) + fechaIni.substring(fechaIni.length() - 4, fechaIni.length());
			fechaFinSer = fechaFin.substring(0, 2) + fechaFin.substring(3, 5) + fechaFin.substring(fechaFin.length() - 4, fechaFin.length());
			EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaConsultar(): Fecha ini " + fechaIniSer + " Fecha fin " + fechaFinSer, EIGlobal.NivelLog.INFO);
		} else {
			fechaIniSer = EnlaceGlobal.fechaHoy("ddmmaaaa");
			fechaFinSer = fechaIniSer;
			EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaConsultar(): Fecha ini "+ fechaIniSer + " Fecha fin " + fechaFinSer,EIGlobal.NivelLog.INFO);
		}
		TransferenciaInterbancariaBUS transferenciaInterbancariaBUS= TransferenciaInterbancariaBUS.getInstance();
		HashMap<Integer,TranInterbancaria> listTranferencias=transferenciaInterbancariaBUS.consultaTransferencias(session.getContractNumber(),fechaIniSer,fechaFinSer, tipoMoneda);//ARS VECTOR SPID
		totalLineas=listTranferencias.size();
		if (totalLineas >= 1) {
			botones.append("\n <br>");
			botones.append("\n <table border=0 align=center>");
			botones.append(TranInterConstantes.PRIMERAPERTURA);
			botones.append("\n   <td align=center class='texfootpagneg'>");
			if (pagina > 0) { // Si NO es la primer pagina se mete la etiqueta de '< Anterior'
				botones.append("\n &lt; <a href='javascript:BotonPagina("+ (pagina - 1)+ ");' class='texfootpaggri'>Anterior</a>&nbsp;\n");
			}
			if (calculaPaginas(totalLineas, regPagina) >= 2) {
				for (int i = 1; i <= calculaPaginas(totalLineas, regPagina); i++)
					if (pagina + 1 == i) {
						botones.append("&nbsp;\n" + Integer.toString(i)+ "&nbsp;");
					}else {
						botones.append("&nbsp;\n<a href='javascript:BotonPagina("+ Integer.toString(i - 1) + ");' class='texfootpaggri'>"+ Integer.toString(i)+ "</a>&nbsp;\n");
					}
				}

			if (totalLineas> ((pagina + 1) * regPagina)) {
				paginaTMP = ((pagina - 1) > -1) ? (pagina - 1) : 0;
				botones.append("\n<a href='javascript:BotonPagina("+ (pagina + 1)+ ");' class='texfootpaggri'>Siguiente</a> &gt;\n");
				fin = regPagina;
			}else {
				fin = totalLineas - (pagina * regPagina);
			}
			botones.append(TranInterConstantes.ETIQUETATDCIERRA);
			botones.append(TranInterConstantes.SEGUNDOCIERRE);
			botones.append("\n<tr>\n");
			botones.append("<td align=center>\n");
			botones.append("<br><a href='javascript:exportarConsulta();'>");
			botones.append("<img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" width=85 height=22 alt=Exportar></a>");
			botones.append("<a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt=Imprimir></a>\n");
			botones.append("</td>\n");
			botones.append(TranInterConstantes.SEGUNDOCIERRE);
			botones.append(TranInterConstantes.CIERRETABLA);
			tablaRegistros.append("\n <table border=0 cellpadding=2 cellspacing=3 align=center>");
			tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
			tablaRegistros.append(TranInterConstantes.ETIQUETATDABRE);
			tablaRegistros.append("\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>");
			tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
			tablaRegistros.append("\n    <td class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red>" + Integer.toString(totalLineas) + "</font> para el contrato: <font color=red>" + contrato + "</font>&nbsp;</td>");
			tablaRegistros.append("\n    <td align=right class='textabref'><b> del <font color=red>"+ fechaIni + "</font> al <font color=red>" + fechaFin + "</font></td>");
			tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
			tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
			tablaRegistros.append("\n    <td align=left class='textabref'><b>&nbsp;Pagina <font color=blue>" + Integer.toString(pagina + 1) + "</font> de <font color=blue>" + Integer.toString(calculaPaginas(totalLineas, regPagina)) + "</font>&nbsp;</b></td>");
			tablaRegistros.append("\n    <td align=right class='textabref'>&nbsp;<b>Registros <font color=blue>" + Integer.toString((pagina * regPagina) + 1) + " - " + Integer.toString((pagina * regPagina) + fin) + "</font></b>&nbsp;</td>");
			tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
			tablaRegistros.append(TranInterConstantes.CIERRETABLA);
			EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaConsultar(): Desplegando tabla TCTArchivo parametros: " + Integer.toString(pagina * regPagina)+ "," + Integer.toString(fin),EIGlobal.NivelLog.INFO);
			tablaRegistros.append(TranInterConstantes.ETIQUETATDCIERRA);
			tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
			tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
			tablaRegistros.append(TranInterConstantes.ETIQUETATDABRE);
			tablaRegistros.append(seleccionaRegistros(listTranferencias,pagina * regPagina,fin,  session.getUserID8(), req));
			tablaRegistros.append(TranInterConstantes.ETIQUETATDCIERRA);
			tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
			tablaRegistros.append(TranInterConstantes.CIERRETABLA);
			req.setAttribute("Busqueda", tipoBusqueda);
			req.setAttribute("FechaIni", fechaIni);
			req.setAttribute("FechaFin", fechaFin);
			req.setAttribute("FechaHoy", EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
			req.setAttribute("Botones", botones.toString());
			req.setAttribute("Registros", tablaRegistros.toString());
			req.setAttribute(TIPO_MONEDA, tipoMoneda);//ARS Vector SPID
			req.setAttribute(TranInterConstantes.NUMERO, Integer.toString(regPagina));
			req.setAttribute(TranInterConstantes.PAGINAX, Integer.toString(pagina));
			req.setAttribute(TranInterConstantes.MODULO, "1");
			req.setAttribute("web_application", Global.WEB_APPLICATION);
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Consultas de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Consultas",TranInterConstantes.AYUDA, req));
			req.setAttribute("NumContrato",(session.getContractNumber() == null) ? "" : session.getContractNumber());
			req.setAttribute("NomContrato",(session.getNombreContrato() == null) ? "" : session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8() == null) ? "" : session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario() == null) ? "" : session.getNombreUsuario());
			req.setAttribute("ClaveBanco", (session.getClaveBanco() == null) ? "014" : session.getClaveBanco());
			if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())) {
				try {
					BitaConstants.archivo = getServletContext().getRealPath(TranInterConstantes.DIAGONALINVERTIDA) + "logBitacora.txt";
					BitaTransacBean bt = new BitaTransacBean();
					BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
					bt = (BitaTransacBean) bh.llenarBean(bt);
					if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.EC_MOV_TARJETA)) {
						bt.setNumBit(BitaConstants.EC_MOV_TARJETA_CONS_INTERBANC);
						}
						if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.EC_INFORME)) {
							bt.setNumBit(BitaConstants.EC_INFORME_CONS_RECUP_INTERBANC);
						}
						if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_INTERBANCARIAS)) {
							bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_RECUP_INTERBANCARIOS);
						}
						if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)) {
							bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_RPIB);
						} else {
							if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)) {
								bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_RPIB);
							}else {
								if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)) {
									bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_RPIB);
								}
							}
						}
						if (contrato != null) {
							bt.setContrato(contrato.trim());
						}
						bt.setNombreArchivo(usuario + "_TICon.doc");
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
					}
				}
			evalTemplate("/jsp/MDI_Consultar.jsp", req, res);
		}else {
			EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaConsultar(): No existen Registros.",EIGlobal.NivelLog.INFO);
			despliegaPaginaErrorURL("No se encontraron movimientos de Depositos Interbancarios realizados en este intervalo de tiempo.", "Consultas de Transferencias Interbancarias", "Transferencias &gt; Interbancarias &gt; Consultas", TranInterConstantes.AYUDA, "MDI_Consultar?Modulo=0", req, res);
		}
		return 1;
	}
	/**
	 * Regresa el total de paginas
	 * @param total total de registros
	 * @param regPagina registros por pagina
	 * @return total de paginas
	 */
	int calculaPaginas(int total,int regPagina) {
	    Double a1  = new Double(total);
	    Double a2  = new Double(regPagina);
	    double a3  = a1.doubleValue()/a2.doubleValue();
	    Double a4  = new Double(total/regPagina);
	    double a5  = a3/a4.doubleValue();
	    int totPag = 0;

	    if(a3 < 1.0) {
	        totPag = 1;
	    }else {
	    	if(a5 > 1.0){
	        	totPag = (total/regPagina) + 1;
	        }
	        if(a5 == 1.0) {
	        	totPag = (total/regPagina);
	        }
        }
	    return totPag;
    }
	/**
	 * Genera la fecha de la Consulta 
	 * @param tipo  tipo de campo a evaluar
	 * @return fecha para la consulta
	 */
	String generaFechaAnt(String tipo) {
		String strFecha = "";
	    java.util.Date Hoy = new java.util.Date();
	    GregorianCalendar Cal = new GregorianCalendar();
	    Cal.setTime(Hoy);
	    Cal.add(Calendar.DATE, -1);
	    if(tipo.equals(TranInterConstantes.FECHAX)) {
	        if(Cal.get(Calendar.DATE) <= 9) {
	            strFecha += TranInterConstantes.CERO + Cal.get(Calendar.DATE) + TranInterConstantes.DIAGONALINVERTIDA;
	        }else {
	            strFecha += Cal.get(Calendar.DATE) + TranInterConstantes.DIAGONALINVERTIDA;
	        }
	        if(Cal.get(Calendar.MONTH)+ 1 <= 9) {
	            strFecha += TranInterConstantes.CERO + (Cal.get(Calendar.MONTH) + 1) + TranInterConstantes.DIAGONALINVERTIDA;
	        }else {
	        	strFecha += (Cal.get(Calendar.MONTH) + 1) + TranInterConstantes.DIAGONALINVERTIDA;
	        }
	        strFecha+=Cal.get(Calendar.YEAR);
        }
	    if(tipo.equals(TranInterConstantes.DIA)) {
	        strFecha += Cal.get(Calendar.DATE);
	    }
	    if(tipo.equals(TranInterConstantes.MES)) {
	        strFecha += (Cal.get(Calendar.MONTH) + 1);
	    }
	    if(tipo.equals(TranInterConstantes.ANIO)) {
	        strFecha += Cal.get(Calendar.YEAR);
	    }	
	    return strFecha;
    }
	/**
	 * Metodo que realiza el filtrado de los registros seleccionados
	 * @param listDatos lista de datos
	 * @param ini registro inicial
	 * @param fin registro final
	 * @param usuario usuario
	 * @param req request
	 * @return String strTabla con la tabla armada con los registros
	 */
	String seleccionaRegistros(HashMap<Integer,TranInterbancaria> listDatos,int ini, int fin,String usuario,HttpServletRequest req) {
		StringBuffer strTabla = new StringBuffer();
		StringBuffer strComprobante = new StringBuffer();
		StringBuffer strEnvio = new StringBuffer();
		String tipoMoneda = (String) getFormParameter(req, TIPO_MONEDA);//ARS VECTOR SPID
		ValidaCuentaContrato valCtas = null;
		String colorbg= "";
		StringBuffer strExportacion = new StringBuffer();
		int totalCampos=listDatos.size();
		if(totalCampos >0) {
			strTabla.append("\n<table border=0 align=center>");
			strTabla.append(TranInterConstantes.PRIMERAPERTURA);
			strTabla.append("\n<th class='tittabdat'>&nbsp; N&uacute;mero de<br> &nbsp; Referencia &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Cuenta Origen &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Cuenta Destino/M&oacute;vil &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Beneficiario &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Importe &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Fecha &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Estatus &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Concepto del<br> &nbsp; Pago/Transferencia &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Referencia<br> &nbsp; Interbancaria &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Forma Aplicaci&oacute;n &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Fecha y Hora de Aplicaci&oacute;n &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Clave de Rastreo &nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Motivo de Devolucion&nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; IVA&nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; RFC&nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Comprobante Electr&oacute;nico&nbsp; </th>");
			strTabla.append("\n<th class='tittabdat'>&nbsp; Divisa </th>");
			strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
			if(fin > totalCampos) {
				fin =totalCampos;
			}
			EIGlobal.mensajePorTrace("MDI_Consultar - seleccionaRegistros(): Generando la Tabla.", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("MDI_Consultar - seleccionaRegistros(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);
			try {
				valCtas = new ValidaCuentaContrato();
				String tdIva = "";
				for(int i=ini;i<(ini+fin);i++) {
					TranInterbancaria tranInterbancaria=listDatos.get(i);
					EIGlobal.mensajePorTrace("MDI_Consultar - seleccionaRegistros(): VALOR  IVA <"+tranInterbancaria.getIva()+">", EIGlobal.NivelLog.INFO);
					if( (i % 2) == 0) {
						colorbg = "class='textabdatobs'";
					}else {
						colorbg = "class='textabdatcla'";
					}
					if(tranInterbancaria.getIva().contains("IVA")){
						tdIva = tranInterbancaria.getIva().substring(tranInterbancaria.getIva().indexOf("IVA")+3, tranInterbancaria.getIva().indexOf("@"));
					}
					strTabla.append(TranInterConstantes.PRIMERAPERTURA);
					strComprobante = new StringBuffer();
					strComprobante.append(tranInterbancaria.getReferencia());
					strComprobante.append(TranInterConstantes.PIPE);//0
					strComprobante.append(tranInterbancaria.getCuentaCargo());
					strComprobante.append(TranInterConstantes.PIPE);//1
					strComprobante.append(tranInterbancaria.getCuentaCLABE());/*si cuentaClaBE==18 si muestra la cuentaCLABE*/
					strComprobante.append(TranInterConstantes.PIPE);//2
					strComprobante.append(tranInterbancaria.getTitular()); 
					strComprobante.append(TranInterConstantes.PIPE);//3
					strComprobante.append(tranInterbancaria.getCuentaAbono()); 
					strComprobante.append(TranInterConstantes.PIPE);//4
					strComprobante.append(tranInterbancaria.getBeneficiario()); 
					strComprobante.append(TranInterConstantes.PIPE);//5
					strComprobante.append(tranInterbancaria.getImporte()); 
					strComprobante.append(TranInterConstantes.PIPE);//6
					strComprobante.append(tranInterbancaria.getConceptoPago()); 
					strComprobante.append(TranInterConstantes.PIPE);//7
					strComprobante.append(tranInterbancaria.getReferenciaInterb());
					strComprobante.append(TranInterConstantes.PIPE);//8
					strComprobante.append(tranInterbancaria.getBanco()); 
					strComprobante.append(TranInterConstantes.PIPE);//9
					strComprobante.append(tranInterbancaria.getPlaza()); 
					strComprobante.append(TranInterConstantes.PIPE);//10
					strComprobante.append(tranInterbancaria.getSucursal()); 
					strComprobante.append(TranInterConstantes.PIPE);//11
					strComprobante.append(tranInterbancaria.getDescEstatus()); 
					strComprobante.append(TranInterConstantes.PIPE);//12
					strComprobante.append(tranInterbancaria.getFchEnvio()); 
					strComprobante.append(TranInterConstantes.PIPE);//13
					strComprobante.append(tranInterbancaria.getCveRastreo()); 
					strComprobante.append(TranInterConstantes.PIPE);//14
					strComprobante.append(tranInterbancaria.getRfc()); 
					strComprobante.append(TranInterConstantes.PIPE);//15
					strComprobante.append(tranInterbancaria.getIva() == null ? tdIva : tranInterbancaria.getIva());  //strComprobante.append(tranInterbancaria.getIva()); 
					strComprobante.append(TranInterConstantes.PIPE);//16
					strComprobante.append(tranInterbancaria.getDescFormaAplic()); 
					strComprobante.append(TranInterConstantes.PIPE);//17
					strComprobante.append(tranInterbancaria.getFchAplicacion()); 
					strComprobante.append(TranInterConstantes.PIPE);//18
					strComprobante.append(tranInterbancaria.getDesMotivoDevol());//19
					strComprobante.append("@");								
					strTabla.append("<td " + colorbg + "><a href=\"javascript:despliegaDatos('" + strComprobante + "')\"> &nbsp;" + tranInterbancaria.getReferencia() + "</a></td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getCuentaCargo()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getCuentaAbono()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getBeneficiario()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+FormatoMoneda(tranInterbancaria.getImporte())+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.fchEnvio+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getDescEstatus()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getConceptoPago()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getReferenciaInterb()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getDescFormaAplic()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getFchAplicacion()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getCveRastreo()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getDesMotivoDevol()+"&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getIva()+"&nbsp; </td>"); 
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tranInterbancaria.getRfc()+"&nbsp; </td>");	
					
					strEnvio = new StringBuffer();
					/**Trama a enviar [09/06/2015|TR204 9020248|CMCA|0021804440027810|27.0]*
					 *                 09/06/2015|TR204 9020249|BANAM|0021804440027810|27.0
					 *                 09/06/2015|TR004 9127564|BANAM|002010471800514371|2.0
					 * fecha=09/06/2015
					 * cveRastreo=TR204 9020249
					 * banco=BANAM
					 * ctaAbono=002010471800514371
					 * importe=2.0
					 * */
					EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOG+"DescEstatus()"+TranInterConstantes.PUNTOS+tranInterbancaria.getDescEstatus()+TranInterConstantes.PUNTOS, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOG+"Estatus()"+TranInterConstantes.PUNTOS+tranInterbancaria.getEstatus()+TranInterConstantes.PUNTOS, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace(TranInterConstantes.ETIQUETALOG+"formaLiquidacion()"+TranInterConstantes.PUNTOS+tranInterbancaria.formaLiquidacion+TranInterConstantes.PUNTOS, EIGlobal.NivelLog.INFO);
					strTabla.append(TranInterConstantes.ETIQUETATD);
					strTabla.append(colorbg);
					strTabla.append("> &nbsp; ");
					EIGlobal.mensajePorTrace("marreguin----------------->MDI_Consultar - tranInterbancaria Estatus="+tranInterbancaria.getEstatus(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("marreguin----------------->MDI_Consultar - tranInterbancaria formaLiquidacion="+tranInterbancaria.formaLiquidacion, EIGlobal.NivelLog.DEBUG);
					
					if(TranInterConstantes.CO.equals(tranInterbancaria.getEstatus()) && (TranInterConstantes.H.equals(tranInterbancaria.formaLiquidacion) 
										|| TranInterConstantes.FORMA_LIQUIDACION_2.equals(tranInterbancaria.formaLiquidacion))){ // FWS TCS marreguin P/023894 SPID Liga CEP
						
						EIGlobal.mensajePorTrace("MDI_Consultar.java::entre al IF", EIGlobal.NivelLog.INFO);
//						if(tipoMoneda.equals("USD")) strEnvio.append("opcion=2");
//						else
						strEnvio.append(tipoMoneda.equals("USD")?"opcion=2":"opcion=1");
						EIGlobal.mensajePorTrace("marreguin-------------------------->tipoMoneda"+tipoMoneda+ "StrEnvioOpcion---->"+ strEnvio, EIGlobal.NivelLog.DEBUG);
						strEnvio.append(TranInterConstantes.AMPERSON);
						strEnvio.append(TranInterConstantes.FECHA);
						strEnvio.append(tranInterbancaria.fchEnvio);
						strEnvio.append(TranInterConstantes.AMPERSON);
						strEnvio.append(TranInterConstantes.CLAVERASTREO); //para DLS est� vacia
						strEnvio.append(tranInterbancaria.getCveRastreo());
						strEnvio.append(TranInterConstantes.AMPERSON);
						strEnvio.append(TranInterConstantes.BANCO);
						strEnvio.append(tranInterbancaria.getBanco());
						strEnvio.append(TranInterConstantes.AMPERSON);
						strEnvio.append(TranInterConstantes.CTABONO);
						strEnvio.append(tranInterbancaria.getCuentaCargo());
						strEnvio.append(TranInterConstantes.AMPERSON);
						strEnvio.append(TranInterConstantes.IMPORTE);
						strEnvio.append(tranInterbancaria.getImporte());
						strEnvio.append(TranInterConstantes.AMPERSON);
						strEnvio.append(TranInterConstantes.CTACARGO);
						strEnvio.append(tranInterbancaria.getCuentaAbono());
							
							if(tipoMoneda.equals("USD")){								//FSW marreguin Liga CEP SPID 09/12/2016
								strEnvio.append(TranInterConstantes.AMPERSON);	        //FSW marreguin Liga CEP SPID 09/12/2016
								strEnvio.append(TranInterConstantes.DEPOSITO_A_BANCO);  //FSW marreguin Liga CEP SPID 09/12/2016
								strEnvio.append("N");									//FSW marreguin Liga CEP SPID 09/12/2016 Hardcode Solicitado por ISBAN aprobado por Transfer
							}
						
						EIGlobal.mensajePorTrace("MDI_Consultar - envioURL"+TranInterConstantes.PUNTOS+"MDI_CepServlet?"+strEnvio+TranInterConstantes.PUNTOS, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("marreguin----------------->MDI_Consultar - envioUR MDI_CepServlet------------------------->|"+strEnvio+"|", EIGlobal.NivelLog.INFO);
						
						strTabla.append(tipoMoneda.equals("MXN") || tipoMoneda.equals("MXP") || tipoMoneda.equals("USD") ? "<a href=\"MDI_CepServlet?" : "");
						strTabla.append(tipoMoneda.equals("MXN") || tipoMoneda.equals("MXP") || tipoMoneda.equals("USD") ? strEnvio : ""); 		
						strTabla.append(tipoMoneda.equals("MXN") || tipoMoneda.equals("MXP") || tipoMoneda.equals("USD") ? "\" onclick=\"return validaNavegador(this);\">Descargar</a>" : "");			
					}
					strTabla.append("&nbsp; </td>");
					strTabla.append(TranInterConstantes.ETIQUETATD + colorbg + "> &nbsp;"+tipoMoneda+"&nbsp; </td>"); //ARS VECTOR SPID
					strTabla.append("\n </tr>");

					NumberFormat formateador = new DecimalFormat("#0.00");
					strExportacion.append(tranInterbancaria.getReferencia());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getReferenciaInterb());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getFchEnvio());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getCuentaCargo());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(formateador.format(tranInterbancaria.getImporte()));
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getCuentaAbono());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getBeneficiario());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getSucursal());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getBanco());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getExpEstatus());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getPlaza());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getConceptoPago());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getExpFormaAplica());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getFchAplicacion());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getCveRastreo());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getDesMotivoDevol());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getIva());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tranInterbancaria.getRfc());
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append(tipoMoneda);
					strExportacion.append(TranInterConstantes.PUNTOYCOMA);
					strExportacion.append("\015\n");
				}
			} catch(Exception e) {
				EIGlobal.mensajePorTrace("Error en metodo seleccionaRegistros de clase MDI_Consultar->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} finally {
				try {
					valCtas.closeTransaction();
				} catch(Exception e) {
					EIGlobal.mensajePorTrace("Error al cerrar la conexion a MQ", EIGlobal.NivelLog.ERROR);
				}
			}
			strTabla.append(TranInterConstantes.PRIMERAPERTURA);
			strTabla.append("\n   <td colspan=8 class='textabref'>Si desea obtener su comprobante, haga click en el n&uacute;mero de referencia subrayado</td>");
			strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
			strTabla.append(TranInterConstantes.CIERRETABLA);
		}
		req.getSession().setAttribute("datosArchivo",strExportacion.toString());
		EIGlobal.mensajePorTrace("marreguin----------------->MDI_Consultar - tranInterbancaria strTabla="+strTabla.toString(), EIGlobal.NivelLog.DEBUG);
		return strTabla.toString();
	}
	/**
	 * Metodo para obtener descripcion estatus
	 * @param estatus estatus
	 * @return descripcion de estatus
	 */
	public String getDescEstatus(String estatus) {
		String descripcion="";
		if("ENVI0000".equals(estatus.trim())) {
			descripcion="<font color=blue><b> ENVIADA </b></font>";
		}else if("PEND0000".equals(estatus.trim())) {
			descripcion="<font color=cyan><b> PENDIENTE </b></font>";
		}else if("CONF0000".equals(estatus.trim())) {
			descripcion="<font color=green><b> CONFIRMADA </b></font>";
		}else if("RECH0000".equals(estatus.trim())) {
			descripcion="<font color=red><b> RECHAZADA </b></font>";
		}else {
			descripcion="<font color=black><b> RECHAZADA </b></font>";
		}
		return descripcion;
	}
	/**
	 * Metodo que obtiene la fecha de Cotizacion 
	 * @param fec fecha
	 * @param tipo boleano
	 * @param diasNoHabiles vector de dias inhablies
	 * @return fecha la fecha
	 */
	String creaFechaAplica(String fec,boolean tipo, Vector diasNoHabiles) {
		StringBuffer strFecha = new StringBuffer();
		int dia=Integer.parseInt(fec.substring(0,2));
		int mes=Integer.parseInt(fec.substring(3,5));
		int anio=Integer.parseInt(fec.substring(6));
		EIGlobal.mensajePorTrace("MDI_Consultar - creaFechaAplica: " + tipo, EIGlobal.NivelLog.INFO);
		GregorianCalendar Cal = new GregorianCalendar(anio,mes-1,dia);
		if(!tipo) {  //Siguiente dia ...
			GregorianCalendar Cal24 = new GregorianCalendar(anio,mes-1,dia);
			Cal24=(GregorianCalendar)evaluaDiaSiguienteHabil(Cal24, diasNoHabiles);
			if(Cal24.get(Calendar.DATE) <= 9) {
				strFecha.append(TranInterConstantes.CERO);
				strFecha.append(Cal24.get(Calendar.DATE));
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}else {
				strFecha.append(Cal24.get(Calendar.DATE));
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}
			if(Cal24.get(Calendar.MONTH)+1 <= 9) {
				strFecha.append(TranInterConstantes.CERO);
				strFecha.append((Cal24.get(Calendar.MONTH)+1));
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}else {
				strFecha.append(Cal24.get(Calendar.MONTH)+1);
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}
			strFecha.append(Cal24.get(Calendar.YEAR));
		}else { //Mismo dia ...
			if(Cal.get(Calendar.DATE) <= 9) {
				strFecha.append(TranInterConstantes.CERO);
				strFecha.append(Cal.get(Calendar.DATE));
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}else {
				strFecha.append(Cal.get(Calendar.DATE));
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}
			if(Cal.get(Calendar.MONTH)+1 <= 9) {
				strFecha.append(TranInterConstantes.CERO);
				strFecha.append(Cal.get(Calendar.MONTH)+1);
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}else {
				strFecha.append(Cal.get(Calendar.MONTH)+1); 
				strFecha.append(TranInterConstantes.DIAGONALINVERTIDA);
			}
			strFecha.append(Cal.get(Calendar.YEAR));
		}
		return strFecha.toString();
	}
	/**
	 * metodo que evalua si el siguiente dia es Habil 
	 * @param fecha fecha a evaluar
	 * @param diasNoHabiles vector con dias inhabiles
	 * @return Calendar de la fecha
	 */
	@SuppressWarnings("static-access")
	public static Calendar evaluaDiaSiguienteHabil(Calendar fecha, Vector diasNoHabiles)
  {
	 int      indice;
	 int      diaDeLaSemana;
	 boolean  encontrado;
	 boolean  bandera=true;

	 EIGlobal.mensajePorTrace("MDI_Consultar - EvaluaDiaHabil", EIGlobal.NivelLog.INFO);

	 do
		{
		   fecha.add( fecha.DAY_OF_MONTH,  1);
		   diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
		   encontrado = false;
		   if(diasNoHabiles!=null)
			{
			  for(indice=0; indice<diasNoHabiles.size() || bandera; indice++)
			   {
				  String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
				  String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
				  String anio = Integer.toString( fecha.get( fecha.YEAR) );
				  dia=(dia.trim().length()==1)?TranInterConstantes.CERO+dia.trim():dia.trim();
				  mes=(mes.trim().length()==1)?TranInterConstantes.CERO+mes.trim():mes.trim();
				  String strFecha = dia.trim() + TranInterConstantes.DIAGONALINVERTIDA + mes.trim() + TranInterConstantes.DIAGONALINVERTIDA + anio.trim();
				  if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
				   {
					 EIGlobal.mensajePorTrace("MDI_Consultar - Dia es inhabil", EIGlobal.NivelLog.INFO);
					 encontrado = true;
					 bandera=false;
				   }
			   }
			}
	   }while( (diaDeLaSemana == Calendar.SUNDAY) || (diaDeLaSemana == Calendar.SATURDAY) || encontrado );
	 return fecha;
  }
	/**
	 * Metodo para Carga los dias de las tablas 
	 * @param formato formato
	 * @return devuelve los dias inhabiles
	 */
	public Vector CargarDias(int formato) {
		EIGlobal.mensajePorTrace("MDI_Consultar Entrando a CargaDias ", EIGlobal.NivelLog.INFO);
		Connection  Conexion = null;
		PreparedStatement qrDias;
		ResultSet rsDias;
		String     sqlDias;
		Vector diasInhabiles = new Vector();
		boolean    estado;
		sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2)";
		EIGlobal.mensajePorTrace("***tranferencia.class Query"+sqlDias, EIGlobal.NivelLog.ERROR);
		try {
			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			if(Conexion!=null) {
				qrDias = Conexion.prepareStatement(sqlDias);
				if(qrDias!=null) {
					rsDias = qrDias.executeQuery();
					if(rsDias!=null) {
						while( rsDias.next()) {
							String dia  = rsDias.getString(1);
							String mes  = rsDias.getString(2);
							String anio = rsDias.getString(3);
							if(formato == 0) {
								dia  =  Integer.toString( Integer.parseInt(dia) );
								mes  =  Integer.toString( Integer.parseInt(mes) );
								anio =  Integer.toString( Integer.parseInt(anio) );
							}
							String fecha = dia.trim() + TranInterConstantes.DIAGONALINVERTIDA  + mes.trim() + TranInterConstantes.DIAGONALINVERTIDA + anio.trim();
							diasInhabiles.addElement(fecha);
						}
						rsDias.close();
					}else {
						EIGlobal.mensajePorTrace("MDI_Consultar Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
					}
				}else {
					EIGlobal.mensajePorTrace("MDI_Consultar Cannot Create Query", EIGlobal.NivelLog.ERROR);
				}
			}else {
				EIGlobal.mensajePorTrace("MDI_Consultar Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
			}
		} catch(SQLException sqle ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				Conexion.close();
			} catch(Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return(diasInhabiles);
	}
    /**
     * Metodo que genera la tabla de comisiones
     * @param arrayCuentasCargo cuentas de cargo
     * @param Result resultado
     * @param Trama trama
     * @param clavePerfil clave del perfil
     * @param usuario usuario
     * @param contrato contrato 
     * @param req request
     * @param res response
     * @return validacion exitosa 
     * @throws ServletException Excepcion Generica
     * @throws IOException Excepcion Generica
     */
	public int generaTablaComisiones( String[][] arrayCuentasCargo, String Result, String Trama, String clavePerfil, String usuario, String contrato, HttpServletRequest req, HttpServletResponse res)
                                    throws ServletException, IOException {
		StringBuffer botones = new StringBuffer();
        StringBuffer tablaRegistros = new StringBuffer();
        String mensajeError     = "Error inesperado.";
        String tipoOperacion    = "COMI"; //servicio de consulta de comisiones cobradas

        boolean comuError=false;

        int fin         = 0;
        int pagina      = Integer.parseInt((String) getFormParameter(req, TranInterConstantes.PAGINAX));
        int regPagina   = Integer.parseInt((String) getFormParameter(req, TranInterConstantes.NUMERO));
        int paginaTMP   = 0;

        paginaTMP = pagina - 2;
        paginaTMP = ( paginaTMP >= 0) ? paginaTMP : 0;

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(TranInterConstantes.SESSION);
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

        ServicioTux TuxGlobal = new ServicioTux();

        /**trama de envio 2<MEDIO_ENTREGA>|<USUARIO>|COMI|<CONTRATO>|<USUARIO>|<PERFIL>|**/
        Trama = IEnlace.medioEntrega2 + "|" + usuario + "|" + tipoOperacion + "|" + contrato + "|" + usuario + "|" + clavePerfil + "|" ;
        EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaComisiones(): Trama entrada: " + Trama, EIGlobal.NivelLog.DEBUG);
        try {
        	Hashtable hs = TuxGlobal.web_red(Trama);
            Result = (String) hs.get("BUFFER");
            EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaComisiones(): Trama salida: " + Result, EIGlobal.NivelLog.DEBUG);
        }catch( java.rmi.RemoteException re ) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.ERROR);
        }
        catch(Exception e){
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);	
        }
        String arcLinea         = "";
        String cadenaTCT        = "";
        String cuentaCargo      = "";
        String nombreArchivo    = "";

        int     totalLineas     = 0;
        boolean status          = false;
        nombreArchivo           = IEnlace.LOCAL_TMP_DIR + "/comcob";// Simulando archivo del servicio

        ArchivoRemoto archR = new ArchivoRemoto();
        if(!archR.copia(usuario)) {
        	EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaComisiones(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
        }
        
        EI_Tipo     TCTArchivo  = new EI_Tipo(this);
        EI_Exportar ArcEnt      = new EI_Exportar(nombreArchivo);
        boolean bandera=true;
        if(ArcEnt.abreArchivoLectura()) {
        	boolean noError = true;
        	do {
        		arcLinea = ArcEnt.leeLinea();
        		if(totalLineas == 0) {
        			if(("OK".equals(arcLinea.substring(0,2)) || "ENCO0000".equals(arcLinea.substring(16,24)))) {
        				status = true;
        			}
        		}
        		if(!"ERROR".equals(arcLinea)&&bandera) {
        			if("ERROR".equals(arcLinea.substring(0,5).trim())){
        				bandera=false;
        			}
        			if(totalLineas == 1) {
        				cuentaCargo = arcLinea.substring(0,11).trim();
        			}
        			totalLineas++;
                    if(totalLineas >= 3) {
                        cadenaTCT = arcLinea.substring(0,arcLinea.length() - 1) + " @";
                    }
        		}else {
        			noError = false;
        		}
        	}while(noError);
        	ArcEnt.cierraArchivo();
        	if("ERROR".equals(arcLinea.substring(0,5).trim()) && totalLineas <= 1 && !status) {
        		comuError       = true;
        		mensajeError    = "Su transaccion no puede ser atendida. Por favor intente mas tarde.";
        	}
        }else {
        	comuError       = true;
        	mensajeError    = "Problemas de comunicacion. Por favor intente mas tarde.";
        }
        if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
        	try{
        		BitaConstants.archivo = getServletContext().getRealPath(TranInterConstantes.DIAGONALINVERTIDA) + "logBitacora.txt";
        		BitaTransacBean bt = new BitaTransacBean();;
        		if (((String)getFormParameter(req, BitaConstants.FLUJO)).equals(BitaConstants.ET_INTERBANCARIAS)) {
        			BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
        			bt = (BitaTransacBean) bh.llenarBean(bt);
        			bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_CONS_COMISIONES_COBRADAS);
        		}
        		if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS)) {
        			BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
        			bt = (BitaTransacBean) bh.llenarBean(bt);
        			bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_COMI);
        		}else{
        			if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)) {
        				BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
        				bt = (BitaTransacBean) bh.llenarBean(bt);
        				bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_COMI);
        			}else{
        				if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)) {
        					BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
        					bt = (BitaTransacBean) bh.llenarBean(bt);
        					bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_COMI);
        				}
        			}
        		}
        		if (session.getContractNumber() != null) {
        			bt.setContrato(session.getContractNumber().trim());
        		}
        		if (usuario != null) {
        			bt.setNombreArchivo(usuario + "_TICon.doc");
        		}
        		if(Result != null) {
        			if(Result.substring(0,2).equals("OK")) {
        				bt.setIdErr(tipoOperacion.trim()+"0000");
        			}else if(Result.length()>8) {
        				bt.setIdErr(Result.substring(0,8));
        			}else{
        				bt.setIdErr(Result.trim());
        			}
        		}else{
        			bt.setIdErr(" ");
        		}
        		BitaHandler.getInstance().insertBitaTransac(bt);
        	}catch(SQLException e){
        		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        	}catch(Exception e){
        		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        	}
        }
        if(!comuError) {
        	TCTArchivo.iniciaObjeto(';','@',cadenaTCT);
        	if(TCTArchivo.totalRegistros >= 1) {
        		botones.append("\n <br>");
        		botones.append("\n <table border=0 align=center>");
        		botones.append(TranInterConstantes.PRIMERAPERTURA);
        		botones.append("\n   <td align=center class='texfootpagneg'>");
                if(pagina > 0) {
                    botones.append("\n &lt; <a href='javascript:BotonPagina(" + (pagina - 1)+ ");' class='texfootpaggri'>Anterior</a>&nbsp;\n");
                }
                if(calculaPaginas(TCTArchivo.totalRegistros,regPagina) >= 2) {
                    for(int i = 1; i <= calculaPaginas(TCTArchivo.totalRegistros,regPagina); i++) {
                        if(pagina + 1 == i) {
                            botones.append("&nbsp;" + Integer.toString(i) + "&nbsp;");
                        }else {
                            botones.append("&nbsp;<a href='javascript:BotonPagina(" + Integer.toString(i - 1) + ");' class='texfootpaggri'>" + Integer.toString(i) + "</a>&nbsp;");
                        }
                    }
                }
                if(TCTArchivo.totalRegistros > ((pagina + 1) * regPagina)) {
                	paginaTMP = ( (pagina - 1) > -1) ? (pagina - 1) : 0;
                    botones.append("\n<a href='javascript:BotonPagina(" + (pagina + 1) + ");' class='texfootpaggri'>Siguiente</a> &gt;\n");
                    fin = regPagina;
                }else {
                	fin = TCTArchivo.totalRegistros - (pagina * regPagina);
                }
                botones.append(TranInterConstantes.ETIQUETATDCIERRA);
				botones.append(TranInterConstantes.SEGUNDOCIERRE);
				botones.append(TranInterConstantes.PRIMERAPERTURA);
				botones.append("\n    <td align=center><br><a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0></a></td>");
				botones.append(TranInterConstantes.SEGUNDOCIERRE);
				botones.append(TranInterConstantes.CIERRETABLA);
				/******************************** Encabezado de la pagina ... */
				tablaRegistros.append("\n <table border=0 cellpadding=2 cellspacing=3 align=center>");
				tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
				tablaRegistros.append(TranInterConstantes.ETIQUETATDABRE);
				tablaRegistros.append("\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>");
				tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
				tablaRegistros.append("\n    <td class='textabref'><b>Cuenta de Cargo: <font color=red>"+cuentaCargo+"</font>&nbsp;</td>");
				tablaRegistros.append("\n    <td align=right class='textabref'>&nbsp;Pagina <font color=blue>"+Integer.toString(pagina+1)+"</font> de <font color=blue>"+Integer.toString(calculaPaginas(TCTArchivo.totalRegistros,regPagina))+"</font>&nbsp;</b></td>");
				tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
				tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
				tablaRegistros.append("\n    <td align=left class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red>"+Integer.toString(TCTArchivo.totalRegistros)+"</font></td>");
				tablaRegistros.append("\n    <td align=right class='textabref'>&nbsp;<b>Registros <font color=blue>"+Integer.toString((pagina*regPagina)+1)+" - "+Integer.toString((pagina*regPagina)+fin)+"</font></b>&nbsp;</td>");
				tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
				tablaRegistros.append(TranInterConstantes.CIERRETABLA);
				EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaComisiones(): Desplegando tabla TCTArchivo parametros: "+Integer.toString(pagina*regPagina)+","+Integer.toString(fin), EIGlobal.NivelLog.INFO);
				tablaRegistros.append(TranInterConstantes.ETIQUETATDCIERRA);
				tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
				tablaRegistros.append(TranInterConstantes.PRIMERAPERTURA);
				tablaRegistros.append(TranInterConstantes.ETIQUETATDABRE);
				tablaRegistros.append(seleccionaRegistrosComisiones(pagina*regPagina,fin,TCTArchivo,req));
				tablaRegistros.append(TranInterConstantes.ETIQUETATDCIERRA);
				tablaRegistros.append(TranInterConstantes.SEGUNDOCIERRE);
				tablaRegistros.append(TranInterConstantes.CIERRETABLA);

				req.setAttribute("TotalTrans",TCTArchivo.strOriginal);
				req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
				req.setAttribute("Botones",botones.toString());
				req.setAttribute("Registros",tablaRegistros.toString());
				req.setAttribute(TranInterConstantes.NUMERO,Integer.toString(regPagina));
				req.setAttribute(TranInterConstantes.PAGINAX,Integer.toString(pagina));
				req.setAttribute(TranInterConstantes.MODULO,"2");
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Consulta de Comisiones Cobradas","Consultas &gt; Comisiones &gt; Comisiones Cobradas",TranInterConstantes.AYUDA,req));
				req.setAttribute("NumContrato", (session.getContractNumber() == null) ? "" : session.getContractNumber());
				req.setAttribute("NomContrato", (session.getNombreContrato() == null) ? "" : session.getNombreContrato());
				req.setAttribute("NumUsuario", (session.getUserID8()          == null) ? "" : session.getUserID8());
				req.setAttribute("NomUsuario", (session.getNombreUsuario()   == null) ? "" : session.getNombreUsuario());

				evalTemplate("/jsp/MDI_Consultar.jsp", req, res);
			}else {
				EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaComisiones(): No existen Registros.", EIGlobal.NivelLog.INFO);
                despliegaPaginaErrorURL("No se encontraron movimientos de Comisiones Cobradas realizadas.","Consulta de Comisiones Cobradas","Consultas &gt; Comisiones &gt; Comisiones Cobradas",TranInterConstantes.AYUDA, "MDI_Consultar?Modulo=0",req, res);
			}
        }else {
        	EIGlobal.mensajePorTrace("MDI_Consultar - generaTablaComisiones(): Error."+mensajeError, EIGlobal.NivelLog.INFO);
        	despliegaPaginaErrorURL(mensajeError,"Consulta de Comisiones Cobradas","Consultas &gt; Comisiones &gt; Comisiones Cobradas", TranInterConstantes.AYUDA,"MDI_Consultar?Modulo=0",req, res );
        }
        return 1;
	}
    /**
     * metodo que arma la tabla de movimientos seleccionados por intervalo
     * @param ini registro inicio
     * @param fin registro fin
     * @param Trans objeto de EI_Tipo
     * @param req request
     * @return tabla de movimientos
     */
	String seleccionaRegistrosComisiones(int ini, int fin, EI_Tipo Trans, HttpServletRequest req) {
		StringBuffer strTabla = new StringBuffer();
        String colorbg="";
        String centrado  ="";
        String derecha   ="";
        String param="";

        /************* Modificacion para la sesion ***************/
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(TranInterConstantes.SESSION);
        EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

        if(Trans.totalRegistros>=1) {
        	int c[]={1,0,2,3,4};
            /**
            0- Descripcion comision
            1- Fecha
            2- Numero de operaciones
            3- Monto operado
            4- Monto comision

            Fecha de aplicacion
            Tipo de comision
            No. de operaciones
            Tarifa unitaria
            Monto de comision
            **/
            strTabla.append("\n<table border=0 align=center>");
            strTabla.append(TranInterConstantes.PRIMERAPERTURA);
            strTabla.append("\n<th class='tittabdat'>&nbsp; Fecha de aplicaci&oacute;n &nbsp; </th>");
            strTabla.append("\n<th class='tittabdat'>&nbsp; Tipo de comisi&oacute;n &nbsp; </th>");
            strTabla.append("\n<th class='tittabdat'>&nbsp; No. Operaciones &nbsp; </th>");
            strTabla.append("\n<th class='tittabdat'>&nbsp; Tarifa unitaria &nbsp; </th>");
            strTabla.append("\n<th class='tittabdat'>&nbsp; Monto de comisi&oacute;n &nbsp; </th>");
            strTabla.append(TranInterConstantes.SEGUNDOCIERRE);

            EIGlobal.mensajePorTrace("MDI_Consultar - seleccionaRegistrosComisiones(): Generando la Tabla.", EIGlobal.NivelLog.INFO);

            if(fin>Trans.totalRegistros) {
                fin=Trans.totalRegistros;
            }

            EIGlobal.mensajePorTrace("MDI_Consultar - seleccionaRegistrosComisiones(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);
            EnlaceGlobal.formateaImporte(Trans, 3);
            EnlaceGlobal.formateaImporte(Trans, 4);

            centrado    =" align=center";
            derecha     =" align=right";

            for(int i=ini;i<(ini+fin);i++) {
                if((i%2)==0) {
                    colorbg="class='textabdatobs'";
                }else {
                    colorbg="class='textabdatcla'";
                }
                strTabla.append(TranInterConstantes.PRIMERAPERTURA);

                for(int j=0;j<5;j++) {
                    if (j==2) {            
                    	param = colorbg + centrado;
                    }else if (j==3||j==4) {
                    	param = colorbg + derecha;
                    }else {
                    	param = colorbg ;
                    }
                    EIGlobal.mensajePorTrace("MDI_Consultar - seleccionaRegistrosComisiones(): valor del registro " +Integer.toString(j)+" = "+Trans.camposTabla[i][c[j]], EIGlobal.NivelLog.DEBUG);
                    strTabla.append("<td "+param+"> &nbsp; "+Trans.camposTabla[i][c[j]]+" &nbsp; </td>");
                }
                strTabla.append(TranInterConstantes.SEGUNDOCIERRE);
            }
            strTabla.append(TranInterConstantes.PRIMERAPERTURA);
            strTabla.append(TranInterConstantes.CIERRETABLA);
        }
        return strTabla.toString();
    }
	/**
	 * Metodo que realiza el renombrado del archivo
	 * @param nombreArchivo nombre nuevo
	 * @param nombre nombre anterior
	 * @return boolean de si cambio o no el nombre del archivo
	 */
	public boolean cambiaNombreArchivo(String nombreArchivo, String nombre) {
		boolean cambio=true;
		EIGlobal.mensajePorTrace( "EI_Importar - cambiaNombreArchivo():Cambiar nombre al archivo por : >" +nombre+ "<", EIGlobal.NivelLog.INFO);
        try {
        	EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():Si el archivo existe, se renombra.", EIGlobal.NivelLog.INFO);
        	File archivo = new File( nombreArchivo );
            if (archivo.exists()) {
                EIGlobal.mensajePorTrace("YA EXISTE EL ARCHIVO : >" +archivo+ "<", EIGlobal.NivelLog.DEBUG);
            }else {
                EIGlobal.mensajePorTrace("NO EXISTE EL ARCHIVO : >" +archivo+ "<", EIGlobal.NivelLog.DEBUG);
            }
            if (archivo.exists()) {
            	EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():Se renombra de: "+nombreArchivo+ " a " +nombre, EIGlobal.NivelLog.INFO);
            	archivo.renameTo(new File(nombre));
            }else {
                EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():El archivo no existe.", EIGlobal.NivelLog.INFO);
                cambio=false;
            }
        }catch(Exception e) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
        	cambio=false;
        }
        EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():Se cambio el nombre" , EIGlobal.NivelLog.INFO);
        return cambio;
    }
    /**
     * Metodo para generar el archivo de exportacion
     * @param usuario usuario
     * @param request request
     * @param response response
     * @throws ServletException Excepcion Generica
     * @throws java.io.IOException Excepcion Generica
     */
    protected void generaArchivoParaExportar(String usuario,HttpServletRequest request,HttpServletResponse response)
	throws ServletException, java.io.IOException {
    	String contenidoDelArchivo=(String)request.getSession().getAttribute("datosArchivo");
    	String app="text/richtext";
    	descargaArchivo(usuario,contenidoDelArchivo,app,request,response);
    }
    /**
     * Metodo que realiza la descarga del archivo
     * @param usuario usuario
     * @param contenidoDelArchivo contenido del archivo
     * @param app app
     * @param request request
     * @param response response 
     * @throws ServletException Excepcion Generica
     * @throws java.io.IOException Excepcion Generica
     */
    protected void descargaArchivo(String usuario,String contenidoDelArchivo,String app,HttpServletRequest request,HttpServletResponse response)
    	throws ServletException, java.io.IOException {
    	EIGlobal.mensajePorTrace( " Peticion de descarga de archivo ..." , EIGlobal.NivelLog.DEBUG);
    	String prefijo=usuario + "_TICon.txt";
    	String tipoMoneda = (String) getFormParameter(request, TIPO_MONEDA);//ARS VECTOR SPID 
    	EIGlobal.mensajePorTrace(  " Nombre de archivo: " + prefijo, EIGlobal.NivelLog.DEBUG);
    	EIGlobal.mensajePorTrace(  " tipoMoneda: " + tipoMoneda, EIGlobal.NivelLog.INFO);//ARS VECTOR SPID 
    	if("".equals(app.trim())) {
    		app="application/x-msdownload";
    	}
    	EIGlobal.mensajePorTrace(" YArchivo: " + prefijo, EIGlobal.NivelLog.DEBUG);
    	EIGlobal.mensajePorTrace( " YTipo de archivo: " + app, EIGlobal.NivelLog.DEBUG);
    	response.setHeader("Content-disposition","attachment; filename=" + prefijo );
    	response.setContentType(app);
    	BufferedOutputStream salida = null;
    	ServletOutputStream out=response.getOutputStream();
    	salida = new BufferedOutputStream(out);
    	salida.write(contenidoDelArchivo.getBytes());
    	salida.write(tipoMoneda.getBytes());//ARS VECTOR SPID 	
    	salida.close();
    	EIGlobal.mensajePorTrace( " Regresando de la descarga, archivo Generado", EIGlobal.NivelLog.DEBUG);
    }
}