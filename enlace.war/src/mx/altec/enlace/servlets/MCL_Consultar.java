package mx.altec.enlace.servlets;

import java.util.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.Format;
import java.text.NumberFormat;


/**<B><I><code>MCL_Consultar</code></I></B>
* <P>Modificaciones:
* 01/10/2002 Se cambiaron vocales acentuadas por su c&oacute;digo de escape.<BR>
* </P>
*/
public class MCL_Consultar extends mx.altec.enlace.servlets.BaseServlet
{

    /** El objeto pipe. */
    String pipe         = "|";

	/** El objeto exportar. */
	String exportar = "";

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	/**
	 * Default action.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 * @throws ServletException La servlet exception
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		String estatusError = "";
		String errores      = "";
	    String strCuentas   = "";
        String strTmp       = "";
        String cadenaFac    = "";
        String contrato     = "";
        String usuario      = "";
        String clavePerfil  = "";
		String tipoMov      = "";

		String titulo1      = "";
		String titulo2      = "";
		String arcAyuda     = "";

		StringBuilder Trama		= new StringBuilder();
		String Result		= "";
		String tipoRelacion	= "";
		String tipoOper="";
		String saldoDisp="";



		Hashtable htResult = null;

		boolean existenCuentas=false;

		EI_Tipo Saldo         = new EI_Tipo(this);
		EI_Tipo TCTArchivo    = new EI_Tipo(this);
		EI_Tipo FacArchivo    = new EI_Tipo(this);

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );


		String tipoArchivo = request.getParameter("tExportacion");

		String fileName = session.getUserID8() + "salCre.doc";
		String fileOut = IEnlace.LOCAL_TMP_DIR+"/"+fileName;
		String valorarchivo = "";

		if(tipoArchivo != null &&!("".equals(tipoArchivo))){
		   String rutaArchivo = IEnlace.DOWNLOAD_PATH + "/"+fileName;
		   EIGlobal.mensajePorTrace("MCL_Consultar - execute()  EXPORTAR "+rutaArchivo, EIGlobal.NivelLog.INFO);
		   EIGlobal.mensajePorTrace("MCL_Consultar - execute()  ARCHIVO EXPORT <"+rutaArchivo+">", EIGlobal.NivelLog.INFO);
		   exportarArchivo(tipoArchivo, rutaArchivo, response);
		   return;
		}


		EIGlobal.mensajePorTrace("MCL_Consultar - execute(): Entrando a Consulta de Saldos.", EIGlobal.NivelLog.INFO);

		boolean sesionvalida=SesionValida(request,response);
        if(sesionvalida)
		 {
			//Variables de sesion ...
			contrato      = session.getContractNumber();
			usuario       = session.getUserID8();
			clavePerfil   = session.getUserProfile();

			String tipo = "";
			String nombreArchivo = IEnlace.LOCAL_TMP_DIR+"/"+usuario+".amb";
			String arcLinea = "";

			// Recuperar Cuentas desde archivo.amb
			EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

			if(ArcEnt.abreArchivoLectura())
            {
				boolean noError=true;
				try{
				do
					{
						arcLinea=ArcEnt.leeLinea();
						if(!"ERROR".equals(arcLinea))
						{
							if("ERROR".equals(arcLinea.substring(0,5).trim())){
								break;
							}
							// Obtiene cuentas asociadas ...
							/*
							if(arcLinea.substring(0,1).equals("2"))
								cadenaTCT+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
							*/
							// Obtiene facultades para cuentas ...
							if("7".equals(arcLinea.substring(0,1))){
								cadenaFac+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
							}
						}else{
							noError = false;
						}
					} while(noError);
				}finally{
					ArcEnt.cierraArchivo();
				}

				// Registros de lineas y tarjetas (5,0)
				TCTArchivo.iniciaObjeto(request.getParameter("Cuentas"));
				FacArchivo.iniciaObjeto(';','@',cadenaFac);

				EI_Exportar ArcSal;
				ArcSal = new EI_Exportar( fileOut );
				ArcSal.creaArchivo();

				// Si existen cuentas asociadas ...
				if(TCTArchivo.totalRegistros>=1)
				 {
					for(int i=0;i<TCTArchivo.totalRegistros;i++)
					 {
						//POST ---------------------------
						 String cuenta = TCTArchivo.camposTabla[i][1].trim();
		                    EI_Tipo cuentasContrato = (EI_Tipo) sess.getAttribute("listaCuentas");

						if(!evaluarCuenta(cuenta,cuentasContrato)){
							 despliegaPaginaError("Operaci&oacute;n no autorizada.","Consultas de Saldos de L&iacute;nea Cr&eacute;dito","Tesorer&iacute;a &gt Cr&eacute;dito &gt Cr&eacute;dito en L&iacute;nea &gt Consultas &gt Saldos","s26260h",request, response );
							 return;
						}
						// ---- POST

						saldoDisp="";
						if("5".equals(TCTArchivo.camposTabla[i][4]) || "0".equals(TCTArchivo.camposTabla[i][4]))
						 {


							//FW
							EIGlobal.mensajePorTrace("MCL_Consultar - CAMPOS TABLA  >> "+TCTArchivo.camposTabla[i][4]+" <<", EIGlobal.NivelLog.DEBUG);

//							String salidaArch = generarArchivos(usuario, session.getArchivoAmb(), TCTArchivo.totalRegistros, nombreArchivo, request, response);

							if("5".equals(TCTArchivo.camposTabla[i][4]))
							 {
								// Tarjetas de Cr?dito ...
								// Revisar facultades para cuenta[i] - Tarjeta.
								tipoMov="Tarjetas";
								tipoRelacion=TCTArchivo.camposTabla[i][3];
								if( ("P".equals(TCTArchivo.camposTabla[i][3].trim()) && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSSALDOS",request)) ||
									("T".equals(TCTArchivo.camposTabla[i][3].trim()) && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSSALDOTER",request)) )
								{
									titulo1="Consultas de Saldos de Tarjeta de Cr&eacute;dito";
									titulo2="Consultas &gt Saldos &gt Por Cuenta &gt Tarjeta de Cr&eacute;dito";
									arcAyuda="s25080h";

									existenCuentas=true;
									tipoOper="SDCR";
									tipo="Tarjeta";

									Trama.delete(0, Trama.length());

									Trama.append(IEnlace.medioEntrega1).append(pipe);
									Trama.append(usuario          ).append(pipe);
									Trama.append("SDCR"           ).append(pipe);
									Trama.append(contrato         ).append(pipe);
									Trama.append(usuario          ).append(pipe);
									Trama.append(clavePerfil      ).append(pipe);
									Trama.append(TCTArchivo.camposTabla[i][1].trim() ).append(pipe); // esta mal numero de cuenta
									Trama.append(TCTArchivo.camposTabla[i][3].trim() ).append(pipe); //
									Trama.append("9999");

									EIGlobal.mensajePorTrace("MCL_Consultar - execute(): Trama entrada = >" +Trama+ "<", EIGlobal.NivelLog.DEBUG);

									try {
						    			htResult = tuxGlobal.web_red( Trama.toString() );
									} catch ( java.rmi.RemoteException re ) {
						    			re.printStackTrace();
									}catch ( Exception e ){}
									if (htResult != null) {
										Result = ( String ) htResult.get( "BUFFER" );
									}

									if(Result==null || "null".equals(Result))
									  Result="ERROR  12345678 Su transacci&oacute;n no puede ser atendida.";

									EIGlobal.mensajePorTrace("MCL_Consultar- execute(): Trama 1 Salida : >" +Result+ "<", EIGlobal.NivelLog.DEBUG);

									strCuentas+=TCTArchivo.camposTabla[i][1]+pipe+TCTArchivo.camposTabla[i][3]+pipe+tipo+pipe+TCTArchivo.camposTabla[i][2]+"@";


									if(!"ERROR".equals(formateaResult(Result).trim()))
									 {

									 		double caut=0;
											double atot=0;
											double cdis=0;

										try{

											caut=new Double(Result.substring(69,83).trim()).doubleValue();
           									atot=new Double(Result.substring(16,30).trim()).doubleValue();
           									cdis=caut-atot;

           									saldoDisp = String.valueOf(cdis);

										}catch (Exception e){

											System.out.println("CAUT: " + caut);
											System.out.println("ATOT: " + atot);
											System.out.println("CDIS: " + cdis);
											saldoDisp="0";
										}

									   TCTArchivo.camposTabla[i][0]=" ";
									   StringBuilder registro = new StringBuilder();
									   for(int j=0;j<TCTArchivo.totalCampos;j++){
										   registro.append(TCTArchivo.camposTabla[i][j]).append(pipe);
									   }
									   registro.append(EnlaceGlobal.fechaHoy("dd/mm/aa|th:tm:ts")).append(pipe).append(tipo).append(pipe);
									   registro.append(formateaResult(Result));
									   strTmp+=	registro.toString();
									   valorarchivo = obtenerColumnas(registro.toString());
									   if (i<TCTArchivo.totalRegistros-1) {
										   valorarchivo+="\n";
									   }
									   ArcSal.escribeLinea(valorarchivo);
									 }

									else
									 {
									   if("OK".equals(Result.substring(0,2)))
										 estatusError+="Cuenta "+TCTArchivo.camposTabla[i][1]+" Su transacci&oacute;n no puede ser atendida en este momento.<br>";
									   else
										 estatusError+="Cuenta "+TCTArchivo.camposTabla[i][1]+" "+Result.substring(16,Result.length())+"<br>";
									   errores+="\ncuadroDialogo('Error en la cuenta: " + TCTArchivo.camposTabla[i][1]+".\\n"+Result.substring(16,Result.length())+"', 3);";
									 }
                                 }
                       }
                      else
                       {
                         // Lineas de Cr?dito ...
                         // Revisar facultades para cuenta[i] - Linea.
						 tipoMov="Lineas";
                         tipoRelacion=TCTArchivo.camposTabla[i][3];
                         if(tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1].trim(),"TECRESALDOSC",request))
                          {
							titulo1="Consultas de Saldos de L&iacute;nea Cr&eacute;dito";
							titulo2="Tesorer&iacute;a &gt Cr&eacute;dito &gt Cr&eacute;dito en L&iacute;nea &gt Consultas &gt Saldos";
							arcAyuda="s26260h";

                            existenCuentas=true;
                            tipoOper="RG01";
                            tipo="Linea";

                            Trama.delete(0, Trama.length());

                            Trama.append(IEnlace.medioEntrega2).append(pipe);
                            Trama.append(usuario                ).append(pipe);
                            Trama.append("RG01"                 ).append(pipe);
                            Trama.append(contrato               ).append(pipe);
                            Trama.append(usuario                ).append(pipe);
                            Trama.append(clavePerfil            ).append(pipe);
                            Trama.append(TCTArchivo.camposTabla[i][1].substring(0,2)   ).append( "@");
                            Trama.append(TCTArchivo.camposTabla[i][1].substring(2,10)  ).append( "@");
                            Trama.append(TCTArchivo.camposTabla[i][1].substring(10,11) ).append( "@");

                            EIGlobal.mensajePorTrace("MCL_Consultar- execute(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

                            //########## Cambiar ....
							try {
						    	htResult = tuxGlobal.web_red( Trama.toString() );
						    } catch ( java.rmi.RemoteException re ) {
						    	re.printStackTrace();
						    }catch ( Exception e ){}
						    if (htResult != null) {
	                            Result = ( String ) htResult.get( "BUFFER" );
						    }

							if(Result==null || "null".equals(Result))
							  Result="ERROR  12345678 Su transacci&oacute;n no puede ser atendida.";
							else
							  Result=formateaResultLinea(usuario).trim();

                            EIGlobal.mensajePorTrace("MCL_Consultar- execute(): Trama 2 Salida: "+Result, EIGlobal.NivelLog.DEBUG);

                            EI_Tipo ResultTmp=new EI_Tipo(this);

                            if(Result != null && Result.length() >= 8 && "RGOS0000".equals(Result.substring(0,8)))  //!Result.substring(0,5).equals("ERROR") && !Result.equals("OPENFAIL") && !
                             {
                               String adeudo=calculaAdeudoTotal(Result, strCuentas);
                               //String disponible=calculaCapitalDisponible(Result,adeudo, strCuentas);
                               String disponible=calculaCreditoDisponible(Result, adeudo,request);
                               /*VSWF-BMB*/
                               saldoDisp=disponible;

							   ResultTmp.iniciaObjeto(Result+"@");

                               //Chequera
                               strTmp+=ResultTmp.camposTabla[0][35]+"-";
                               strTmp+=ResultTmp.camposTabla[0][36]+"-";
                               strTmp+=ResultTmp.camposTabla[0][37]+ pipe;

                               for(int j=1;j<TCTArchivo.totalCampos;j++)
                                 strTmp+=TCTArchivo.camposTabla[i][j]+pipe;

                               strTmp+=EnlaceGlobal.fechaHoy("dd/mm/aa|th:tm:ts")+pipe;
                               strTmp+=ResultTmp.camposTabla[0][39]+pipe;

                               strTmp+=ResultTmp.camposTabla[0][0]+pipe;
                               strTmp+=adeudo+pipe;
                               strTmp+=ResultTmp.camposTabla[0][0]+pipe;
                               strTmp+=ResultTmp.camposTabla[0][0]+pipe;
                               strTmp+=ResultTmp.camposTabla[0][47]+pipe;
                               strTmp+=ResultTmp.camposTabla[0][40]+pipe;
                               strTmp+=disponible+" @";
                             }
                            else
                             {
                               if("OPENFAIL".equals(Result))
                                {
                                  estatusError+="Su transacci&#243;n no puede ser atendida en este momento. Cuenta "+TCTArchivo.camposTabla[i][1]+"<br>";
								  errores+="\ncuadroDialogo('Su transacci&#243;n no puede ser atendida en este momento.\\nCuenta "+TCTArchivo.camposTabla[i][1]+"', 3);";
                                }
                               else
                                {
                                  estatusError+="Cuenta "+TCTArchivo.camposTabla[i][1]+" "+Result.substring(16,Result.length())+"<br>";
								  errores+="\ncuadroDialogo('Error en la cuenta: " + TCTArchivo.camposTabla[i][1]+".\\n"+Result.substring(16,Result.length())+"', 3);";
                                }
                             }
                          }
                       }
                    }
						  //TODO BIT CU1071, CU3051 continua flujo SALDO
						  /*
							 * 03/ENE/07
							 * 15/Enero/2007	Modificaci?n para el moudulo de Tesorer?a
							 * VSWF-BMB-I
							 */
						  if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
						  try{
							  if(existenCuentas){
								  if(saldoDisp==null ||  "".equals(saldoDisp)){
									  saldoDisp="0.00";
								  }
						        BitaHelper bh = new BitaHelperImpl(request, session, sess);
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
								equals(BitaConstants.EC_POSICION_TARJETA)){
									bt.setNumBit(BitaConstants.EC_POSICION_TARJETA_CONS_SALDO_CREDITO);
								}
								else if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
										equals(BitaConstants.EC_SALDO_CUENTA_TARJETA)){
											bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_TARJETA_CONS_SALDO_CREDITO);
								}
								else{
									if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
										bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_CDCR);
									}
								}
								bt.setContrato((contrato == null)?" ":contrato);


								if (TCTArchivo.camposTabla[i][1]!=null){
									if (TCTArchivo.camposTabla[i][1].length()>20){
										bt.setCctaOrig(TCTArchivo.camposTabla[i][1].substring(0,20));
									}else{
										bt.setCctaOrig(TCTArchivo.camposTabla[i][1].trim());
									}
								}


								if(Result!=null){
					    			if("OK".equals(Result.substring(0,2))){
					    				bt.setIdErr(tipoOper.trim()+"0000");
					    			}else if(Result.length()>8){
										bt.setIdErr(Result.substring(0,8));
									}else{
										bt.setIdErr(Result.trim());
									}
								}


								try{
									bt.setImporte(Double.parseDouble(saldoDisp.trim()));
								}catch(NumberFormatException e){
									bt.setImporte(0);
								}


								bt.setServTransTux((tipoOper == null)?" ":tipoOper.trim());
								BitaHandler.getInstance().insertBitaTransac(bt);
							  }
						  	}catch(NumberFormatException e){
						  		EIGlobal.mensajePorTrace("MCL_Consultar - defaultAction()  Excepcion NumberFormat "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
						  	}
						  catch(SQLException e){
							    EIGlobal.mensajePorTrace("MCL_Consultar - defaultAction()  Excepcion SQL "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
							}catch(Exception e){
								EIGlobal.mensajePorTrace("MCL_Consultar - defaultAction()  Excepcion  "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
							}
						  }
                }
					try{
						if (ArcSal!=null) {
							ArchivoRemoto archR = new ArchivoRemoto();
							archR.copiaLocalARemoto(fileName,"WEB");
						}
					}finally{
						ArcSal.cierraArchivo();
					}
               }
			  Saldo.iniciaObjeto(strTmp);
			  //POST -----
			 // sess.setAttribute("listaCuentasSel",TCTArchivo);
			  //POST
              generaTablaConsultar(FacArchivo,Saldo,TCTArchivo,existenCuentas, request, response ,estatusError, errores,  strCuentas , tipoMov);
            }
		   else
            despliegaPaginaError("Su transacci&#243;n no puede ser atendida en este momento.",titulo1,titulo2,arcAyuda,request, response );
         }
		else
		 {
           request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
           evalTemplate( IEnlace.ERROR_TMPL, request, response );
		 }
	  EIGlobal.mensajePorTrace("MCL_Consultar - execute(): Saliendo de Consulta de Saldos.", EIGlobal.NivelLog.INFO);
    }

/**
 * Obtener columnas.
 *
 * @param valorArchivo El objeto: valor archivo
 * @return Objeto string
 */
private String obtenerColumnas(String valorArchivo) {
		StringBuilder sb = new StringBuilder();
		String[] cols = valorArchivo.split("\\|");
		// |1|2|11|14|15|16|17
		// |CUENTA|DESCRIPCION|ADEUDO TOTAL|FECHA DE VENCIMIENTO|CREDITO AUTORIZADO|CREDITO DISPONIBLE|TASA ANUALIZADA
		sb.append(cols[1]).append(";");
		sb.append(cols[2]).append(";");
		sb.append(cols[11]).append(";");
		sb.append(cols[14]).append(";");
		sb.append(cols[15]).append(";");
		sb.append(cols[16]).append(";");
		sb.append(cols[17].replace("@", ""));
		return sb.toString();
	}

/**
 * Evalua que la cuenta este dada de alta en la lista grobal de cuentas del contrato.
 * @param cuenta Cuenta a buscar.
 * @param cuentasContrato Cuentasdel contrato.
 * @return True si existe la cuenta en la relacion de cuentas del contrato.
 */
	private boolean evaluarCuenta(String cuenta, EI_Tipo cuentasContrato) {
		boolean resultado =false;
		String cuentaAux ="";
		String[][] cuentas = cuentasContrato.camposTabla;
		EIGlobal.mensajePorTrace("Evaluando la cuenta: "+cuenta +" dentro de un universo de: "+cuentas.length, EIGlobal.NivelLog.INFO);
		try {
			for(int i =0;i<cuentas.length;i++){
				cuentaAux = cuentas[i][1];
				EIGlobal.mensajePorTrace("Evaluando la cuenta: "+cuentaAux, EIGlobal.NivelLog.INFO);
				if(cuentaAux.equals(cuenta)){
					resultado = true;
					break;
				}
			}	
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("MCL_Consultar - evaluarCuenta - Ocurrio un "
					+ "problema evaluando la cuenta: " + cuenta, EIGlobal.NivelLog.ERROR);
		}
		return resultado;
}

/**
 * **********************************************************************************.
 *
 * @param FacArchivo El objeto: fac archivo
 * @param Saldo El objeto: saldo
 * @param TCTArchivo El objeto: TCT archivo
 * @param existenCuentas El objeto: existen cuentas
 * @param request El objeto: request
 * @param response El objeto: response
 * @param estatusError El objeto: estatus error
 * @param errores El objeto: errores
 * @param strCuentas El objeto: str cuentas
 * @param tipoMov El objeto: tipo mov
 * @throws ServletException La servlet exception
 * @throws IOException Una excepcion de I/O ha ocurrido.
 */
    public void generaTablaConsultar(EI_Tipo FacArchivo,EI_Tipo Saldo,EI_Tipo TCTArchivo,boolean existenCuentas, HttpServletRequest request, HttpServletResponse response, String estatusError, String errores, String strCuentas , String tipoMov)
			throws ServletException, IOException {

	   String titulo1      = "";
	   String titulo2      = "";
	   String tipo         = "";
   	   String arcAyuda     = "";
       String strTabla="";
	   String[] titulos = new String[8];

		if ("Tarjetas".equals(tipoMov))
		{
			titulos[0] = "";
			titulos[1] = "Cr&eacute;dito";
			titulos[2] = "Descripci&oacute;n";
                         //"Fecha",
                         //"Hora",
			titulos[3] = "Cr&eacute;dito autorizado";
			titulos[4] = "Adeudo Total";
			titulos[5] = "Cr&eacute;dito Disponible";
			titulos[6] = "Fecha de vencimiento";
			titulos[7] = "Tasa Anualizada**" ;
		}
		else
		{
			titulos[0] = "";
            titulos[1] = "Chequera";
            titulos[2] = "Cr&eacute;dito";
            titulos[3] = "Descripci&oacute;n";
                         //"Fecha",
			             //"Hora",
            titulos[4] = "Cr&eacute;dito autorizado";
            titulos[5] = "Adeudo Total";
            titulos[6] = "Cr&eacute;dito Disponible";
            titulos[7] = "Fecha de vencimiento";
		}


     //int[] datos={7,1,0,1,9,7,8,15,11,16,14,2}; con fecha y hora
        int[] value={4,1,2,3,4};

	/* Caracteristicas para Linea de Credito */
		int[] datos={7,1,0,1,2,15,11,16,14,2};
	    int[] align={1,1,0,2,2,2,1,1};

	/* Ajustes para Tarjeta de Credito
	    int[] datos={7,1,1,2,15,11,16,14,17,2};
	    int[] align={1,1,2,2,2,1,2,1};	*/
	   if ("Tarjetas".equals(tipoMov))
	   {
			datos[0] =  7;
			datos[1] =  1;
			datos[2] =  1;    align[0] = 1;
			datos[3] =  2;	  align[1] = 1;
			datos[4] = 15;	  align[2] = 2;
			datos[5] = 11;	  align[3] = 2;
			datos[6] = 16;	  align[4] = 2;
			datos[7] = 14;	  align[5] = 1;
			datos[8] = 17;	  align[6] = 2;
			datos[9] =  2;	  align[7] = 1;
	   }

	   /************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	   if("Tarjetas".equals(tipoMov)) {
		  tipo="1";
		  titulo1="Consultas de Saldos de Tarjeta de Cr&eacute;dito";
		  titulo2="Consultas &gt Saldos &gt Por Cuenta &gt Tarjeta de Cr&eacute;dito";
		  arcAyuda="s25080h";
		} else {
		  tipo="2";
		  titulo1="Consultas de Saldos de L&iacute;nea Cr&eacute;dito";
		  titulo2="Tesorer&iacute;a &gt Cr&eacute;dito &gt Cr&eacute;dito en L&iacute;nea &gt Consultas &gt Saldos";
		  arcAyuda="s26260h";
		}

       if(Saldo.totalRegistros>=1)
		{

		  formateaImporte(Saldo,15);
		  formateaImporte(Saldo,11);
		  formateaImporte(Saldo,16);

          strTabla+=Saldo.generaTabla(titulos,datos,value,align);

		  request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          request.setAttribute("Errores",errores);
          request.setAttribute("strCuentas",strCuentas);
          request.setAttribute("FacArchivo",FacArchivo.strOriginal);
		  request.setAttribute("Tipo",tipo);
          request.setAttribute("Tabla",strTabla);
          request.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));

          request.setAttribute("MenuPrincipal", session.getStrMenu());
		  request.setAttribute("newMenu", session.getFuncionesDeMenu());
		  request.setAttribute("Encabezado",CreaEncabezado(titulo1,titulo2,arcAyuda,request));
		  request.setAttribute("tipoConsulta",tipoMov); // Tasa Anualizada
		  // TODO TODO BIT CU1031

          evalTemplate("/jsp/MCL_Consultar.jsp", request, response );
        }
	   else
		{
		  if(estatusError.trim().equals(""))
			 estatusError="Error en la ejecuci&oacute;n de la consulta.";

          if(existenCuentas)
            despliegaPaginaError(estatusError,titulo1,titulo2,arcAyuda, request, response );
		  else
		   {
			 if(TCTArchivo.totalRegistros>=1)
			   despliegaPaginaError("No tiene facultad para revisar las cuentas seleccionadas.",titulo1,titulo2,arcAyuda, request, response );
			 else
			   despliegaPaginaError("Error en la ejecuci&oacute;n de la consulta.",titulo1,titulo2,arcAyuda, request, response );
		   }
        }
	}

/**
 * **********************************************************************************.
 *
 * @param Result El objeto: result
 * @return Objeto string
 */
/***************** formatea Resultado para Tarjetas de Credito ***********************/
/*************************************************************************************/
    String formateaResult(String Result)
     {
       StringBuffer formateado = new StringBuffer("");
	   String strTasa = "";
       double caut=0;
       double atot=0;
       double cdis=0;
       if(Result.length()>=70)
        if(Result.substring(0,7).trim().equals("OK"))
         {
           formateado.append(Result.substring( 8,16).trim());
           formateado.append(pipe);
           formateado.append(Result.substring(16,30).trim());
		   formateado.append(pipe);
           formateado.append(Result.substring(30,44).trim());
		   formateado.append(pipe);
           formateado.append(Result.substring(44,58).trim());
		   formateado.append(pipe);
           formateado.append(Result.substring(58,69).trim());
		   formateado.append(pipe);
           formateado.append(Result.substring(69,83).trim());
		   formateado.append(pipe);
           caut=new Double(Result.substring(69,83).trim()).doubleValue();
           atot=new Double(Result.substring(16,30).trim()).doubleValue();
           cdis=caut-atot;
           formateado.append(Double.toString(cdis));
           formateado.append(pipe);

		   try{
			   strTasa = InsertaPunto( Result.substring(83,87).trim() ); //Tasa Anualizada
		   }catch(Exception e){}

	       if( !strTasa.equals("") && !strTasa.equals("00.00") )
		   {
	           formateado.append(formatea(strTasa));
			   formateado.append("%@"); //Tasa Anualizada
		   }
		   else
		   {
	           formateado.append(" @");
		   }

           return formateado.toString();
         }
       return "ERROR";
     }

/**
 * **********************************************************************************.
 *
 * @param Result El objeto: result
 * @param strCuentas El objeto: str cuentas
 * @return Objeto string
 */
/*********************************************************** Calcula Adeudo Total    */
/*************************************************************************************/
    String calculaAdeudoTotal(String Result, String strCuentas)
     {
       double  atotal=0;

       EI_Tipo ResultTmp=new EI_Tipo(this);

	   ResultTmp.iniciaObjeto(Result+"@");

       double capitalExigible      = new Double(ResultTmp.camposTabla[0][13]).doubleValue();
       double interesExigible      = new Double(ResultTmp.camposTabla[0][14]).doubleValue();
       double ivaInteresExigible   = new Double(ResultTmp.camposTabla[0][15]).doubleValue();

       double capitalNoExigible    = new Double(ResultTmp.camposTabla[0][16]).doubleValue();
       double interesNoExigible    = new Double(ResultTmp.camposTabla[0][17]).doubleValue();
       double ivaInteresNoExigible = new Double(ResultTmp.camposTabla[0][18]).doubleValue();

       double interesMoratorio=new Double(ResultTmp.camposTabla[0][19]).doubleValue();
       double ivaInteresMoratorio=new Double(ResultTmp.camposTabla[0][20]).doubleValue();
       double seguro=new Double(ResultTmp.camposTabla[0][21]).doubleValue();

       double gastosJuridicos=new Double(ResultTmp.camposTabla[0][24]).doubleValue();
       double penaConvencional=new Double(ResultTmp.camposTabla[0][25]).doubleValue();
       double ivaPenaConvencional=new Double(ResultTmp.camposTabla[0][26]).doubleValue();

       double otros=new Double(ResultTmp.camposTabla[0][27]).doubleValue();
       double ivaOtros=new Double(ResultTmp.camposTabla[0][28]).doubleValue();

       atotal=capitalExigible + interesExigible + ivaInteresExigible;
       atotal+=capitalNoExigible + interesNoExigible + ivaInteresNoExigible;
       atotal+=interesMoratorio + ivaInteresMoratorio + seguro + gastosJuridicos;
       atotal+=penaConvencional + ivaPenaConvencional + otros + ivaOtros;

       return Double.toString(atotal);
     }

/**
 * **********************************************************************************.
 *
 * @param Result El objeto: result
 * @param atotal El objeto: atotal
 * @param strCuentas El objeto: str cuentas
 * @return Objeto string
 */
    String calculaCapitalDisponible(String Result, String atotal, String strCuentas)
     {
       double cdisponible=0;
       double adeudoTotal=new Double(atotal).doubleValue();

       EI_Tipo ResultTmp=new EI_Tipo(this);

       ResultTmp.iniciaObjeto(Result+"@");

       double capitalExigible   = new Double(ResultTmp.camposTabla[0][13]).doubleValue();
       double capitalNoExigible = new Double(ResultTmp.camposTabla[0][16]).doubleValue();
       double capitalAutorizado = new Double(ResultTmp.camposTabla[0][40]).doubleValue();

       if(adeudoTotal>capitalAutorizado || capitalExigible>0)
         cdisponible=0;
       else
         cdisponible=capitalAutorizado-capitalNoExigible;

       return Double.toString(cdisponible);
     }

/**
 * **********************************************************************************.
 *
 * @param tramaRespuesta El objeto: trama respuesta
 * @param cadenaAdeudoTotal El objeto: cadena adeudo total
 * @param request El objeto: request
 * @return Objeto string
 */
   String calculaCreditoDisponible(String tramaRespuesta, String cadenaAdeudoTotal,HttpServletRequest request)
   {
      Double disponible;
      double adeudoTotal=0;
      double creditoAutorizado=0;
      double creditoDisponible=0;

      EI_Tipo procesaTrama=new EI_Tipo(this);
      procesaTrama.iniciaObjeto(tramaRespuesta+"@");

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");
	  EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

      try
      {
         adeudoTotal=Double.parseDouble(cadenaAdeudoTotal);
         creditoAutorizado=Double.parseDouble(procesaTrama.camposTabla[0][40]);

         if(creditoAutorizado>adeudoTotal)
         {
            creditoDisponible=creditoAutorizado-adeudoTotal;
         } // Fin if

      }
      catch(NumberFormatException nfe)
      {
         EnlaceGlobal.mensajePorTrace("Error al convertir a flotante: "+nfe.getMessage(), EIGlobal.NivelLog.INFO);
      } // Fin try-catch

      disponible=new Double(creditoDisponible);

      return disponible.toString();
   } // Fin metodo calculaCreditoDisponible


/**
 * **********************************************************************************.
 *
 * @param usuario El objeto: usuario
 * @return Objeto string
 */
/*************************************************** formatea Reultado para Linea    */
/*************************************************************************************/
    String formateaResultLinea(String usuario)
     {
	   String arcLinea="";
       String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 EIGlobal.mensajePorTrace("MCL_Consultar - formateaResultLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
	   // **********************************************************

       EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

       if(ArcEnt.abreArchivoLectura())
        {
          boolean noError=true;
          arcLinea=ArcEnt.leeLinea();
          if(arcLinea.substring(0,2).equals("OK"))
           {
             arcLinea=ArcEnt.leeLinea();
             if(arcLinea.equals("ERROR"))
               return "OPENFAIL";
           }
		  ArcEnt.cierraArchivo();
        }
       else
        return "OPENFAIL";
       return arcLinea;

      //########## Cambiar ....
    }

/**
 * **********************************************************************************.
 *
 * @param FacArchivo El objeto: fac archivo
 * @param tipoRelacion El objeto: tipo relacion
 * @param credito El objeto: credito
 * @param facultad El objeto: facultad
 * @param request El objeto: request
 * @return true, si exitoso
 */
/*************************************************************************************/
/*************************************************************************************/
   boolean tieneFacultad(EI_Tipo FacArchivo,String tipoRelacion, String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

	  if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_Consultar - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_Consultar - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
          {
            if(tipoRelacion.trim().equals("P"))
              FacultadAsociadaACuenta=true;
            else
              FacultadAsociadaACuenta=false;
          }
       }
	  EIGlobal.mensajePorTrace("MCL_Consultar - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }

    /**
     * Facultad asociada.
     *
     * @param FacArchivo El objeto: fac archivo
     * @param credito El objeto: credito
     * @param facultad El objeto: facultad
     * @param signo El objeto: signo
     * @return true, si exitoso
     */
    boolean facultadAsociada(EI_Tipo FacArchivo,String credito,String facultad,String signo)
     {
      /*
       0 7
       1 Clave Fac
       2 Cuenta
       3 signo
      */

       if(FacArchivo.totalRegistros>=1)
        for(int i=0;i<FacArchivo.totalRegistros;i++)
         {
           if(credito.trim().equals(FacArchivo.camposTabla[i][2].trim()))
            if(FacArchivo.camposTabla[i][1].trim().equals(facultad))
             if(FacArchivo.camposTabla[i][3].trim().equals(signo))
			  {
			    EIGlobal.mensajePorTrace("MCL_Consultar - facultadAsociada(): Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
                return true;
			  }
         }
	   EIGlobal.mensajePorTrace("MCL_Consultar - facultadAsociada(): Facultad asociada regresa false: Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
       return false;
     }

	/**
	 * Formatea.
	 *
	 * @param format El objeto: format
	 * @return Objeto string
	 */
	private static String formatea(String format)
    {
        int decimal = 2;
        try
        {
            BigDecimal importe = (new BigDecimal(format)).setScale(decimal, 4);
            NumberFormat formateo = NumberFormat.getNumberInstance();
            formateo.setMaximumFractionDigits(decimal);
            formateo.setMinimumFractionDigits(decimal);
            format = formateo.format(importe);
        }
        catch(Exception _ex) {
        	EIGlobal.mensajePorTrace("MCL_Consultar - facultadAsociada() Excepcion: " +_ex.getMessage(), EIGlobal.NivelLog.INFO);
							 }
        return format;
    }

	/**
	 * Inserta punto.
	 *
	 * @param CANTIDAD El objeto: cantidad
	 * @return Objeto string
	 */
	private String InsertaPunto(String CANTIDAD)
    {
        String CantidadConPunto = "";
        try
        {
            CantidadConPunto = CANTIDAD.substring(0, CANTIDAD.length() - 2) + "." + CANTIDAD.substring(CANTIDAD.length() - 2);
        }
        catch(Exception e)
        {
            System.out.println("Modulo [MCL_Movimientos.InsertaPunto]:Convirtiendo[" + CANTIDAD + "]: Exception(" + e.toString() + ")");
        }
        return CantidadConPunto;
    }

	/**
	 * Formatea importe.
	 *
	 * @param Tipo El objeto: tipo
	 * @param ind El objeto: ind
	 */
	public  void formateaImporte( EI_Tipo Tipo, int ind )
	  {
		 for(int i=0;i<Tipo.totalRegistros;i++)
		 {
			if(! (Tipo.camposTabla[i][ind].trim().equals("")) )
			{
				// importe=new Float(Tipo.camposTabla[i][ind]).floatValue();
				// Tipo.camposTabla[i][ind]=FormatoMoneda(Float.toString(importe));
				 Tipo.camposTabla[i][ind]=FormatoMoneda(Tipo.camposTabla[i][ind]);
			}
		  }
	   }

	/* (non-Javadoc)
	 * @see mx.altec.enlace.servlets.BaseServlet#FormatoMoneda(java.lang.String)
	 */
	public String FormatoMoneda( String cantidad ){


		String language = "la"; // ar
		String country = "MX";  // AF
		Locale local = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato = "";
		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.0";

		double importeTemp = 0.0;
		importeTemp = new Double(cantidad).doubleValue();
		if (importeTemp < 0)
		{
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!(formato.substring(0,1).equals("$")))
		    		 if (formato.substring (0,3).equals ("(MX"))
		    			 formato ="$ -"+ formato.substring (4,formato.length () - 1);
		    		 else
		    			 formato ="$ -"+ formato.substring (4,formato.length ());
			} catch(NumberFormatException e) {formato="$0.00";}
		} else {
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!(formato.substring(0,1).equals("$")))
		    		 if (formato.substring (0,3).equals ("MXN"))
		    			 formato ="$ "+ formato.substring (3,formato.length ());
		    		 else
		    			 formato ="$ "+ formato.substring (1,formato.length ());
			} catch(NumberFormatException e) {
				formato="$0.00";}
		} // del else
		if(formato==null)
			formato = "";
		return formato;
	}

	/**
	 * Exportar archivo.
	 *
	 * @param tipoArchivo El tipo de archivo a exportar
	 * @param nombreArchivo La ruta del archivo de donde se obtienen los datos
	 * @param res El request de la pagina
	 */
	private void exportarArchivo(String tipoArchivo, String nombreArchivo, HttpServletResponse res){
		EIGlobal.mensajePorTrace("MCL_Consultar - exportarArchivo() ARCHIVO DE ENTRADA  -->"+nombreArchivo+"<--",EIGlobal.NivelLog.DEBUG);
		   ExportModel em = new ExportModel("cSaldoCredito", '|',";")
		   .addColumn(new Column(0, "CREDITO"))
		   .addColumn(new Column(1, "DESCRIPCION"))
		   .addColumn(new Column(2, "ADEUDO TOTAL","$#########################0.00"))
		   .addColumn(new Column(3, "FECHA DE VENCIMIENTO"))
		   .addColumn(new Column(4, "CREDITO AUTORIZADO","$#########################0.00"))
		   .addColumn(new Column(5, "CREDITO DISPONIBLE","$#########################0.00"))
		   .addColumn(new Column(6, "TASA ANUALIZADA"));

		   try{
			   EIGlobal.mensajePorTrace("MCL_Consultar - exportarArchivo() Generando Exportacion --> ",EIGlobal.NivelLog.DEBUG);

			   HtmlTableExporter.export(tipoArchivo, em, nombreArchivo, res);

		   }catch(IOException ex){
			   EIGlobal.mensajePorTrace("MCL_Consultar - exportarArchivo()  ERROR --> Exportar Archivo -> " + ex.getMessage(),EIGlobal.NivelLog.ERROR);
		   }
	}

}
/*2005.1*/