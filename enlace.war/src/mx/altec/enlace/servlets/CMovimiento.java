/*Banco Santander Mexicano
  Clase CMovimiento Contiene las funciones para la presentacion de pantalla de movimientos
  @Autor:
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 08/03/2001 - Solo presentar cuentas propias para la consulta
*/
package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.ibm.websphere.validation.base.config.level51.requestmetricsvalidation_51_NLS;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
/*Inicia - EVERIS VALIDACION CSS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION CSS */

/** 
*   Isban M�xico
*   Clase: CMovimiento.java
*   Descripci�n: Servlet utilizado en la funcionalidad de Movimientos.
*
*   Control de Cambios:
*   1.0 16/10/2017 Everis - Limpieza Cross Site Scripting
*/
public class CMovimiento extends BaseServlet
{
    private String nombreArchivo = "", pipe="|", tramaentrada="", Result="", folio = "";
    private String Path = "", datosparaarch="";
    Hashtable hs = null;
    StringBuffer Trama;
    String registrosMostrar="";
    boolean script = false;
    
    public void doGet(HttpServletRequest req,HttpServletResponse res)
        throws ServletException, IOException
    {
        defaultAction(req,res);
    }

    public void doPost(HttpServletRequest req,HttpServletResponse res)
        throws ServletException, IOException
    {
        defaultAction(req,res);
    }

    /**
     * Inserte aqu� la descripci�n del m�todo.
     * Entrega el nombre del Archivo
     * Fecha de creaci�n: (5/3/01 9:52:42)
     * @return java.lang.String
     * @param Archivo java.lang.String
     * @param Destino java.lang.String
     */
    public String getNombreArchivo()
    {
      return this.nombreArchivo;
    }

    /**
     * Inserte aqu� la descripci�n del m�todo.
     * Entrega el Path del Archivo
     * Fecha de creaci�n: (5/3/01 9:52:42)
     * @return java.lang.String
     * @param Archivo java.lang.String
     * @param Destino java.lang.String
     */
    public String getPath()
    {
        return this.Path;
    }

    /**
     * Inserte aqu� la descripci�n del m�todo.
     * Obtiene el nombre del Archivo
     * Fecha de creaci�n: (5/3/01 9:52:42)
     * @return java.lang.String
     * @param Archivo java.lang.String
     * @param Destino java.lang.String
     */
    public void setNombreArchivo(String nombarch)
    {
        this.nombreArchivo = nombarch;
    }

    /**
     * Inserte aqu� la descripci�n del m�todo.
     * Obtiene el Path
     * Fecha de creaci�n: (5/3/01 9:52:42)
     * @return java.lang.String
     * @param Archivo java.lang.String
     * @param Destino java.lang.String
     */
    public void setPath(String patharch)
    {
        this.Path = patharch;
    }

    //Este metodo obtiene un archivo remoto
    ////Version JSCH, Proyecto CNBV Limpiar.........
    /*
    private String ObtieneArchivoRemoto( String usr, String Remoto, String Rutarch, String elnomarch, String diraguardar)
    {
        String Comando = "rcp " + usr + "@" + Remoto + ":" + Rutarch + elnomarch + " " +  diraguardar;
        return Comando;
    }
    */
    //Implementa ObtieneArchivoRemoto
    public String ObtenHora()
    {
        String hora = "";
        int horas, minutos;
        java.util.Date Hoy = new java.util.Date();
        GregorianCalendar Cal = new GregorianCalendar();

        Cal.setTime(Hoy);
        horas = Cal.get(Calendar.HOUR_OF_DAY);
        minutos = Cal.get(Calendar.MINUTE);
        hora = (horas < 10 ? "0" : "") + horas + ":" + (minutos < 10 ? "0" : "") + minutos;
        return hora;
    }

    public String rellenaCaracter(String origen, char caracter, int cantidad, boolean izquierda)
    {
        int cfin = cantidad - origen.length();
        if(origen.length() < cantidad)
        {
            for(int contador = 0 ; contador< cfin;contador++)
            {
                if(izquierda)
                    origen = caracter + origen;
                else
                    origen = origen + caracter;
            }
        }
        if(origen.length() > cantidad) origen = origen.substring(0,cantidad);
        // ---
        return origen;
    }

    public String Periodicidad(String num)
    {
        String salida="";
        if( num.equals("1") )
            salida = "Diario";
        else if (num.substring(0,1).equals("2"))
            salida = "Semanal"+num.substring(1);
        else if( num.equals("3") )
            salida = "Mensual";
        else
            salida ="error";
        return salida;
    }

    public String Movimiento(String num)
    {
        String salida="";
        if( num.equals("C") )
            salida = "Cargo";
        else if( num.equals("A") )
            salida = "Abono";
        else if( num.equals("B") )
            salida = "Cargo/Abono";
        else
            salida ="error";
        return salida;
    }

    public String BuenCobro(String num)
    {
        String salida="";
        if( num.equals("0") )
            salida = "Si";
        else if( num.equals("1") )
            salida = "No";
        else
            salida ="error";
        return salida;
    }

    public String Vista(String num)
    {
        String salida="";
        if( num.equals("0") )
            salida = "Si";
        else if( num.equals("1") )
            salida = "No";
        else
            salida ="error";
        return salida;
    }

    public String Formato(String num)
    {
        String salida="";
        if( num.equals("N43"))
            salida = "Norma 43";
        else if( num.equals("TCT"))
            salida = "Enlace";
        else
            salida ="error";
       return salida;
    }

    public String Medio(String num)
    {
        String salida="";
        if( num.equals("1") )
            salida = "Email.";
        else if( num.equals("2") )
            salida = "Buzon.";
        else if( num.equals("3") )
            salida = "Enlace.";
        else
            salida ="error";
       return salida;
    }

    public String Estatus(String num)
    {
        String salida="";
        if( num.equals("0") )
            salida = "Activo";
        else if( num.equals("1") )
            salida = "Inactivo";
        else
            salida ="error";
       return salida;
    }

    public String TipoConsulta(String num)
    {
        String salida="";
        if( num.equals("1") )
            salida = "Detallado";
        else if( num.equals("2") )
            salida = "Consolidado";
        else
            salida ="error";
       return salida;
    }

    public String pintarcelda
        (
            String nl,
            String lc,
            String ls,
            String sim,
            String mails,
            int    contar,
            int    auxtotal,
            int    prev
        )
    {
    	if (mails.length () ==0)
    	    mails =", ";
        // pintarcelda(nuevalinea,lineacuenta,miselect,simbolo,datosparaarchmail);
        StringTokenizer sttres = new StringTokenizer(nl,"$");
        String datostabla="", dato="n", datoaux="", salida="", datosparaarchcuenta = "", lcauxiliar="";
        int contadordescripcion = 0;
        StringTokenizer stcuatro = new StringTokenizer(lc,"$");
        String miselect = "<SELECT NAME=\"Cuentas\" style=\"height: 22px; width: 300px\">";
        while( stcuatro.hasMoreTokens() )
        {
            lcauxiliar = stcuatro.nextToken();
            datosparaarchcuenta = datosparaarchcuenta + lcauxiliar + ", ";
            miselect = miselect + "<OPTION value = \"\">" + lcauxiliar + "</OPTION>";
        }
        miselect = miselect + "<Option value = \"\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Option></select>";
        if( sttres.hasMoreTokens() )
        {
            dato = sttres.nextToken();
            datosparaarch = datosparaarch + dato.trim() + ", ";
            datostabla = datostabla + "<td align=\"center\" width=\"50\" class=\"" + sim + "\"  ><INPUT TYPE =\"RADIO\"  NAME=\"CHRCF\" onclick=\"val_entrega()\" VALUE=\"" + folio + "\"></td>";
            contadordescripcion++;
        }
        contar = contar+1;
        log("-!? Inicio Desentramando ");
       
        String aux="";
        while( dato != "" )
        {
            datostabla = datostabla + "<td align=\"center\" width=\"100\" class=\"" + sim + "\" >" + dato + "</td>";
            if ( sttres.hasMoreTokens() == true)
            {
                dato="";
                aux="";
                aux = sttres.nextToken();
                if( contadordescripcion == 2 )
                {
                    dato = TipoConsulta(aux);
                    datosparaarch = datosparaarch + datosparaarchcuenta + dato + ", ";
                }
                else if( contadordescripcion == 3 )
                {
                    dato = Periodicidad(aux);
                    datosparaarch = datosparaarch + dato + ", ";
                }
                else if( contadordescripcion == 5 )
                {
                    dato = Movimiento(aux);
                    datosparaarch = datosparaarch + dato + ", ";
                }
                else if( contadordescripcion == 6 )
                {
                    dato = BuenCobro(aux);
                    datosparaarch = datosparaarch + dato + ", ";
                }
                else if( contadordescripcion == 9 )
                {
                    dato = Medio(aux);
                    datosparaarch = datosparaarch + dato + ", ";
                }
                else if( contadordescripcion == 7 )
                {
                    if(aux.equals("0")){
                    	dato = "&nbsp;";
                    	datosparaarch = datosparaarch +", ";
                    }
                    else{
                        dato=aux;
                        datosparaarch = datosparaarch + dato  + ", ";
                    }
                }
                else if( contadordescripcion == 11 )
                {
                    dato = Formato(aux);
                    datosparaarch = datosparaarch + mails + dato  + ", ";
                }
                else if( contadordescripcion == 12 )
                {
                    dato = Vista(aux);
                    datosparaarch = datosparaarch + dato  + ", ";
                }
                else if( contadordescripcion == 13 )
                {
                    dato = Estatus(aux);
                    datosparaarch = datosparaarch + dato  + ", ";
                }
                else if( contadordescripcion == 14 )
                {
                    datosparaarch = datosparaarch + aux;
                    dato = aux;
                   /* guardandoFolios=guardandoFolios+"-"+dato;
                    EIGlobal.mensajePorTrace("yhg--> Este debe ser el folio:**********"+dato, EIGlobal.NivelLog.DEBUG);
                    EIGlobal.mensajePorTrace("yhg--> GuardandoFolios:**********"+guardandoFolios, EIGlobal.NivelLog.DEBUG);
                    
                    */
                }
                else
                    dato = aux;
                if( (contadordescripcion != 1) && (contadordescripcion != 2) && (contadordescripcion != 3) && (contadordescripcion != 5)  && (contadordescripcion != 6)&& (contadordescripcion != 7) && (contadordescripcion != 9) && (contadordescripcion != 10) && (contadordescripcion != 11) && (contadordescripcion != 12) && (contadordescripcion != 13) && (contadordescripcion != 14) )
                    datosparaarch = datosparaarch + dato  + ", ";
                contadordescripcion++;
            }
            else
                dato = "";
        }
       
        //req.getSession().setAttribute("FoliosConAccesoVulnerab", guardandoFolios);
        datosparaarch = datosparaarch + "\n";
        String celdabuzonenlace = new String("&nbsp;");
        int indice=0, indicedos=0;
        indice = datostabla.indexOf("*");
        salida = datostabla.substring(0,indice) + miselect + datostabla.substring(indice+1,datostabla.length());
        EIGlobal.mensajePorTrace("Salida antes:" + salida, EIGlobal.NivelLog.INFO);
        indicedos = salida.indexOf("#");
        EIGlobal.mensajePorTrace("indicedos" + indicedos, EIGlobal.NivelLog.INFO);
        if( salida.indexOf("Buzon")!=-1 )
            salida = salida.substring(0,indicedos) + celdabuzonenlace + salida.substring(indicedos+1,salida.length());
        if( salida.indexOf("Enlace.")!=-1 )
            salida = salida.substring(0,indicedos) + celdabuzonenlace + salida.substring(indicedos+1,salida.length());
        if( salida.indexOf("Email")!=-1 )
            salida = salida.substring(0,indicedos) + ls + salida.substring(indicedos+6,salida.length());
        //EIGlobal.mensajePorTrace("-->datosparaarch "+datosparaarch, EIGlobal.NivelLog.INFO);
        //EIGlobal.mensajePorTrace("-->contador "+contar, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Salida despues:" + salida, EIGlobal.NivelLog.INFO);
        if( auxtotal==1 )
            registrosMostrar = datosparaarch;
        return salida;
    }

public void defaultAction ( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		boolean sesionvalida = SesionValida (req, res);
		Map<String, String[]> paramMap = req.getParameterMap();
		if(!sesionvalida) {
			return;
		}
        if (Utilerias.validarRequest(paramMap)){
        	req.setAttribute("Error",IEnlace.MSG_DATOS_INVALIDOS);
        	evalTemplate("/jsp/EI_Error.jsp", req, res);
        }
        else{
        HttpSession sess = req.getSession ();
        BaseResource session = (BaseResource) sess.getAttribute ("session");
        String Tipoenlace = "0";
        if(req.getParameter("prog")!=null){
        Tipoenlace = req.getParameter("prog");
        }
        String idfolio = req.getParameter("idfolio");
        /*Inicia - EVERIS VALIDACION CSS*/
        EIGlobal.mensajePorTrace("Validar CSS para el parametro [Banca]", EIGlobal.NivelLog.INFO);
        String tipoBanca = UtilidadesEnlaceOwasp.cleanString(req.getParameter("Banca"));
		/*Finaliza - EVERIS VALIDACION CSS*/
        //Nombre de Archivo
        String codigoA = req.getParameter ("codigoA");
        //Path del Archivo
        String codigoP = req.getParameter ("codigoP");

        String codigoE = req.getParameter ("codigoE");
        String codigoF = req.getParameter ("codigoF");
        String codigoX = req.getParameter ("codigoX");
        
        String codigoE1 = req.getParameter ("codigoE1");
        String codigoX1 = req.getParameter ("codigoX1");
        String canD = req.getParameter ("can");
        String mensajeD = req.getParameter ("mensaje");
        EIGlobal.mensajePorTrace("*******************************************parametros***********************", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("canD "+canD, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("mensajeD "+mensajeD, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("-->registrosMostrar "+registrosMostrar, EIGlobal.NivelLog.INFO);

        if (codigoA == null )
            codigoA = "";
        if (codigoP == null )
            codigoP = "";
        if (codigoE == null )
            codigoE = "";
        if (codigoF == null )
            codigoF = "";
        if (codigoX == null )
            codigoX = "";
        if (codigoE1 == null )
            codigoE1 = "";
        if (codigoX1 == null )
            codigoX1 = "";
        if (canD == null )
            canD = "";
        if (mensajeD == null )
            mensajeD = "";

        codigoE=codigoE.replace('_', ' ');
        codigoF=codigoF.replace('_', ' ');
        codigoX=codigoX.replace('_', ' ');
        codigoE1=codigoE1.replace('_', ' ');
        codigoX1=codigoX1.replace('_', ' ');     
        
        
        try{
         req.setAttribute ("MenuPrincipal", session.getStrMenu ());
         req.setAttribute ("newMenu", session.getFuncionesDeMenu ());
         req.setAttribute ("Encabezado", CreaEncabezado ( "Consulta de Programaciones Vigentes",
         "Consultas &gt; Movimientos &gt; Chequeras &gt; Programados &gt; Registro", "s36030h",req ) );
         } catch(Exception e) { e.printStackTrace();}

        EIGlobal Global = new EIGlobal (session , getServletContext () , this);
        //En este Path es donde se encuentran los archivos de los clientes

        if( Tipoenlace.equals("0") )
        {
        /* Modificacion validacion de facultades 19/09/2002 */
        if (! sesionvalida ) { return; }

        if( ! session.getFacultad (session.FAC_CONSULTA_MOVTOS) ) {

            //despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de Saldos de cuentas de cheques", "Consultas &gt; Saldos &gt; Por cuenta &gt; Cheques", "s25010h",req, res);
            // Modificado por incidencia SANT-324
            //req.getRequestDispatcher ("/EnlaceMig/SinFacultades").forward (req, res);

            //req.getRequestDispatcher ("/SinFacultades").forward (req, res);

            //req.getRequestDispatcher ("/jsp/SinFacRedirect.jsp").forward (req, res);
            //req.getRequestDispatcher ( mx.altec.enlace.utilerias.Global.APP_PATH + "/WEB-INF/classes/EnlaceMig/SinFacultades").forward (req, res);

            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute ( "plantillaElegida", laDireccion);
            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward (req,res);
            return;
        } {
            {
                EIGlobal.mensajePorTrace ("***CMovimiento.class Entrando a CMovimiento ", EIGlobal.NivelLog.DEBUG);

                /*Inicia - EVERIS VALIDACION CSS*/
                EIGlobal.mensajePorTrace("Validar CSS para el parametro [cta_posi]", EIGlobal.NivelLog.INFO);
                String tramacta  = UtilidadesEnlaceOwasp.cleanString(req.getParameter("cta_posi"));
				/*Finaliza - EVERIS VALIDACION CSS*/
                String tramactaConsolida  = req.getParameter ("radTabla");

                EIGlobal.mensajePorTrace ("<<<<< tramactaConsolida prueba2 = " + tramactaConsolida + ">>>>>>>", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace ("<<<<< TRAMA CUENTA = " + tramacta + ">>>>>>>", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace ("<<<<< TIPO BANCA = " + tipoBanca + ">>>>>>>", EIGlobal.NivelLog.INFO);

                if (tramacta != null  || tramactaConsolida != null ) {
                    if (tramacta.equals ("") || tramacta == null && tramactaConsolida != null){
                        tramacta = tramactaConsolida;
                    }
                }
                int      contador  = 0;
                boolean encontrado = false;

                String diasInhabiles = diasInhabilesAnt ();

                if (diasInhabiles.trim ().length ()==0){

                    if (tipoBanca.equals ("E")){
                        despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP, "Consulta de movimientos de banca especializada","Consultas &gt; Movimientos &gt; Banca especializada","s25200h", req, res);
                    }else{
                        despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras" ,"s25150h", req, res);
                    }
                    return;
                }

                EIGlobal.mensajePorTrace ("***CMovimiento.class tramacta:" + tramacta, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace ("***CMovimiento.class tipoBanca:" + tipoBanca, EIGlobal.NivelLog.INFO);

                String[] cta_posi2 = null;
                String cta = null;

                //				System.out.println("<<<<< TRAMA CUENTA 2 = " + tramacta + ">>>>>>>");
                //				System.out.println("<<<<< TIPO BANCA   2 = " + tipoBanca + ">>>>>>>");

                EIGlobal.mensajePorTrace ("<<<<< TRAMA CUENTA 2 = " + tramacta + ">>>>>>>", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace ("<<<<< TIPO BANCA   2 = " + tipoBanca + ">>>>>>>", EIGlobal.NivelLog.INFO);

                if (tipoBanca==null) {
                    tipoBanca = "";
                }

                if (tramacta!=null) {
                    if(tramacta.trim ().length ()==0) tramacta=null;
                }

                if (tramacta!=null) {
                    cta_posi2 = desentramaC (tramacta,'|');
                    cta=cta_posi2[1];
                }else{
                    cta=null;
                }

                req.setAttribute ("MenuPrincipal", session.getStrMenu ());
                req.setAttribute ("newMenu", session.getFuncionesDeMenu ());

                if (tipoBanca.equals ("E")){
                    req.setAttribute ("Encabezado", CreaEncabezado ("Consulta de movimientos de banca especializada","Consultas &gt; Movimientos &gt; Banca especializada","s25200h",req));
                }else{
                    req.setAttribute ("Encabezado", CreaEncabezado ("Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras","s25150h",req));
                }

                req.setAttribute ("diasInhabiles",diasInhabiles);
                req.setAttribute ("tipoBanca", tipoBanca);
                //        //+",tipoBanca="+tipoBanca+",e="+tipoBanca.equals("E"));
                //
                //
                //        //***********************************************
                //        //modificaci�n para integraci�n pva 07/03/2002
                //        //***********************************************
                //
                //        //String[][] arrayCuentas = null;
                //
                //        //if (tipoBanca.equals("E")) arrayCuentas=BEContCtas(session.getContractNumber());
                //        //else arrayCuentas=ContCtasRelacpara_mis_transferencias(session.getContractNumber());
                //        //String Cuentas ="";
                //        //String tipo_cuenta="";
                //        //String seleccion="";
                //        //***********************************************
                //                        /*Esto que?? lo quite porque esta de sobra
                //                        if(arrayCuentas!=null) {
                //                                System.out.println("No es nulo el dato");
                //                                if(arrayCuentas[0]!=null) {
                //                                        System.out.println("No es nulo el penultimo elemento");
                //                                        if(arrayCuentas[0][0]!=null)
                //                                                        System.out.println("No es nulo el ultimo elemento");
                //                        }
                //                        } else {
                //                                            if (tipoBanca.equals("E")){
                //                                                                System.out.println("Aqui 1");
                //                                                                arrayCuentas=BEContCtas(session.getContractNumber());
                //                                                        }
                //                                                        else {
                //                                                                System.out.println("Aqui 2"+session.getContractNumber());
                //                                                                arrayCuentas=ContCtasRelacpara_mis_transferencias(session.getContractNumber());
                //                                                        }
                //                        }
                //                         */
                //        //***********************************************
                //        //modificaci�n para integraci�n pva 07/03/2002
                //        //***********************************************
                //            /*
                //            boolean chkseg;
                //
                //                        for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++){
                //                             chkseg=true;
                //                 if (arrayCuentas[indice][2].equals("P"))
                //                              chkseg  = !existeFacultadCuenta(arrayCuentas[indice][1], "CONSMOVTOS", "-");
                //                             else if (arrayCuentas[indice][2].equals("T"))
                //                              chkseg  = existeFacultadCuenta(arrayCuentas[indice][1], "CONSMOVTOTER", "+");
                //                 if(chkseg==true)
                //                             {
                //                   if ((cta!=null)&&(cta.equals(arrayCuentas[indice][1])))
                //                   {
                //                                     Cuentas = Cuentas + "<OPTION value = \"" + arrayCuentas[indice][1] +"@"+arrayCuentas[indice][2]+"@"+arrayCuentas[indice][4]+"@" +"\"  SELECTED>" + arrayCuentas[indice][1] + " " + arrayCuentas[indice][4];
                //                         encontrado=true;
                //                                }
                //                    else
                //                                        {
                //                      //tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
                //                      if (tipoBanca.equals("E"))
                //                                   Cuentas = Cuentas + "<OPTION value = \"" + arrayCuentas[indice][1] +"@"+arrayCuentas[indice][2]+"@"+arrayCuentas[indice][4]+"@"+"\" >" + arrayCuentas[indice][1] + " " + arrayCuentas[indice][4];
                //                                 else if ((!arrayCuentas[indice][3].equals("5"))&&
                //                                       (!arrayCuentas[indice][3].equals("0"))&&
                //                                       (!arrayCuentas[indice][3].equals("4")))
                //                                 Cuentas = Cuentas + "<OPTION value = \"" + arrayCuentas[indice][1]+"@"+arrayCuentas[indice][2]+"@"+arrayCuentas[indice][4]+"@"+"\" >" + arrayCuentas[indice][1] + " " + arrayCuentas[indice][4];
                //                                }
                //                                        contador++;
                //                            }
                //
                //            }*/
                //        //modificacion para integracion if  ((cta!=null)&&(encontrado==false))
                //        //modificacion para integracion{
                //        //modificacion para integracion    despliegaPaginaError("No se tienen facultades para ver movimientos sobre esta cuenta","Consulta de movimientos","", req, res);
                //        //modificacion para integracion}
                //        //modificacion para integracionelse
                //        //modificacion para integracion{
                //        //modificacion para integracion   if (contador==0)
                //        //modificacion para integracion     despliegaPaginaError("No tiene facultades para ver movimientos en las cuentas","Consulta Movimientos","", req, res);
                //        //modificacion para integracion
                //        //modificacion para integracion   else
                //        //modificacion para integracion   {
                //        //modificacion para integracion      req.setAttribute("Cuentas",Cuentas);
                //        //req.setAttribute("MenuPrincipal", session.getstrMenu());
                req.setAttribute ("Fecha",ObtenFecha ());
                String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio () + "\">";
                datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes () + "\">";
                datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia () + "\">";
                req.setAttribute ("Movfechas",datesrvr);
                //***********************************************
                //modificaci�n para integraci�n pva 07/03/2002
                //***********************************************
                if(cta!=null) {
                    req.setAttribute ("textEnlaceCuenta",cta);
                    req.setAttribute ("EnlaceCuenta",tramacta);
                    EIGlobal.mensajePorTrace("FSWV ---> ENVIA CUENTA numeroCuenta0 <"+cta+">", EIGlobal.NivelLog.INFO);
                    req.setAttribute("numeroCuenta0", cta.trim());
                }else{   req.setAttribute ("textEnlaceCuenta","");
                req.setAttribute ("EnlaceCuenta","");
                }

                //08/01/07
                BitaHelper bh=null;
                BitaTransacBean bt=null;
                 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
	                 bh = new BitaHelperImpl(req, session, sess);
	                 bt = new BitaTransacBean();

                	bt = (BitaTransacBean)bh.llenarBean(bt);
                }
                if (tipoBanca.equals ("E")){

                    session.setModuloConsultar (IEnlace.MConsulta_Saldos_BE);
                    /*TODO BIT CU1111
        			 * VSWF-BMB-I
        			 */
        			 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
        			 try{
	        			if(req.getParameter(BitaConstants.FLUJO)!=null){
	        				bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
	        			}
	        			else{
	        				bh.incrementaFolioFlujo(BitaConstants.EC_MOV_BANCA);
	        			}
	        			bt.setNumBit(BitaConstants.EC_MOV_BANCA_ENTRA);
    				}catch(Exception e){
    					e.printStackTrace();
    				}
					}

        			/*
        			 * VSWF-BMB-F
        			 */
                }else{
                    session.setModuloConsultar (IEnlace.MConsulta_Saldos);
                    /*TODO BIT CU1081
        			 * 08/ENE/07
        			 * VSWF-BMB-I
        			 */
        			 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
	                    try{
	                    if(req.getParameter(BitaConstants.FLUJO)!=null){
	        				bh.incrementaFolioFlujo(req.getParameter(BitaConstants.FLUJO));
	        			}
	                    else{
	                    	bh.incrementaFolioFlujo(BitaConstants.EC_MOV_CHEQ_LINEA);
	                    }
	        			bt = (BitaTransacBean)bh.llenarBean(bt);
	        			bt.setNumBit(BitaConstants.EC_MOV_CHEQ_LINEA_ENTRA);
	    				}catch(Exception e){
	    					e.printStackTrace();
	    				}
    				}
        			/*
        			 * VSWF-BMB-F
        			 */
                }
                //***********************************************
                /*
    			 * VSWF-BMB-I
    			 */
                if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
                try{
                	if(session.getContractNumber()!=null){
	                 bt.setContrato(session.getContractNumber());
                	}
                	if(cta!=null){
                		if(cta.length()>20){
                			bt.setCctaOrig(cta.substring(0,20));
                		}else{
                			bt.setCctaOrig(cta.trim());
                		}
                	}
	    			BitaHandler.getInstance().insertBitaTransac(bt);
                }catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
                }
                /*
    			 * VSWF-BMB-F
    			 */
                evalTemplate (IEnlace.C_MOVIMIENTO_TMPL, req, res);
                return;
                //modificacion para integracion}
                //modificacion para integracion}

            }

            //12/10/2002
//            if (tipoBanca.equals ("E"))
//                despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP, "Consulta de movimientos de banca especializada","Consultas &gt; Movimientos &gt; Banca especializada","s25200h", req, res);
//            else
//                despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos de cuenta de cheques","Consultas &gt; Movimientos &gt; Chequeras" ,"s25150h", req, res);
//

            //				despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos","", req, res);
        }

        }

        if(Tipoenlace.equals("1"))
        {
            log("-!? >>>>> NORMA 43 <<<<<<< " );
            String todaslaslineas="";
            String pre="";
            String nex="";
            String tota="";
            registrosMostrar="";
            datosparaarch="";

            //validacion de facultades
            if(!session.getFacultad (session.FAC_CONSULTA_MOVTOS))
            {
              String laDireccion = "document.location='SinFacultades'";
              req.setAttribute( "plantillaElegida", laDireccion);
              req.getRequestDispatcher(IEnlace.CARGATEMPLATE_TMPL).forward(req,res);
              return;
            }

            //Recupera parametros de paginacion
            try
            {
                pre =(String)req.getParameter ("ant");
                nex =(String)req.getParameter ("sig");
                tota =(String)req.getParameter ("tot");
                EIGlobal.mensajePorTrace ( "*paginacion*****************************", EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace ( "***valor de paginacion prev "+pre, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace ( "***valor de paginacion next "+nex, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace ( "***valor de paginacion tota"+tota, EIGlobal.NivelLog.INFO);
                if(pre.equals("")){
                    pre=null;
                }
                if(nex.equals("")){
                    nex=null;
                }
                if(tota.equals("")){
                    tota=null;
                }
            }
            catch(Exception e) {}


            //inicializar parametros de paginacion
            int total=0,prev=0,next=0;
            String paginacion="";

         //validar paginacion
            if((pre!=null) &&  (nex!=null) && (tota!=null))
            {
                //vienen valores en parametros
                try
                {
                    prev =  Integer.parseInt (pre);
                    next =  Integer.parseInt (nex);
                    total = Integer.parseInt (tota);
                }
                catch(Exception e)
                {
                                   e.printStackTrace();
                }
            }
            else
            {
                //Es la primera entrada a paginacion
                EIGlobal.mensajePorTrace ( "***Cmovimientos parametrso de paginacion todos nullos", EIGlobal.NivelLog.DEBUG);
                try
                {
                    EIGlobal.mensajePorTrace(""+session.getUserID8(), EIGlobal.NivelLog.DEBUG);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

                //inicializamos valores de paginacion
                //total=getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/"+codigoA);  //total de registros del archivo
                prev=0;
                total=31;
                EIGlobal.mensajePorTrace ( "***valor de paginacion total"+total, EIGlobal.NivelLog.INFO);

                //validacion para paginacion
//--->          	if (total>EnlaceMig.Global.NUM_REGISTROS_PAGINA) {
                if (total>mx.altec.enlace.utilerias.Global.NUM_REGISTROS_PAGINA1)
                {
//--->                   next=EnlaceMig.Global.NUM_REGISTROS_PAGINA;
                    next=mx.altec.enlace.utilerias.Global.NUM_REGISTROS_PAGINA1;
                }
                else
                {
                    next=total;
                }
            }
            EIGlobal.mensajePorTrace ( "*paginacion2****************************", EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace ( "***valor de paginacion prev "+prev, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace ( "***valor de paginacion next "+next, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace ( "***valor de paginacion total"+total, EIGlobal.NivelLog.INFO);


            log("-!? Inicio de Movimientos " );
            Global.mensajePorTrace ("CMovimiento.java **************Archivo y ruta CODIGOA = >" + codigoA + "<", EIGlobal.NivelLog.INFO);
            Global.mensajePorTrace ("CMovimiento.java CODIGOP = >" + codigoP + "<", EIGlobal.NivelLog.INFO);

            setNombreArchivo(codigoA);
            setPath(codigoP);

            log("-!? Inicio de Leer Archivo " );
            ArchivoRemoto archR1 = new ArchivoRemoto();
                if(!archR1.copiaArchivo(codigoA))
                {
                    EIGlobal.mensajePorTrace("***Cmovimientos.class No se pudo copiar archivo remoto:"+IEnlace.REMOTO_TMP_DIR+"/"+session.getUserID8(), EIGlobal.NivelLog.ERROR);
                }
                else
                {
                      EIGlobal.mensajePorTrace("***Cmovimientos.class archivo remoto copiado:"+IEnlace.REMOTO_TMP_DIR+"/"+session.getUserID8(), EIGlobal.NivelLog.DEBUG);
                }//fin else remoto

            log("-!? Final de Leer Archivo " );
            log("-!? Inicio de la Obtencion de Datos " );


            int j = 0;
            String linea = null, sigdato = null;
            FileReader lee = null;

            //Pinta encabezado
            String tablaapintar = "<tr>" +
                   "<td  width=\"50\" align=\"center\" class=\"tittabdat\">&nbsp</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Fecha de Alta del movimiento</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Cuenta</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Tipo de Consulta</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Periodicidad</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Horario</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Tipo de Movimiento</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">SBC</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Importe</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Fecha de entrega</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Medio de Entrega</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Direcci&oacute;n EMail</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Formato</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Vista</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">Estatus</td>" +
                   "<td  width=\"100\" align=\"center\" class=\"tittabdat\">No De Folio</td>" +
                   "</tr>";


            boolean h = true, haytokens = false;
            String nuevalinea="", lineacuenta="", simbolo="", miselect="" , datomitoken ="", lineaaux="", colorcelda="", datosparaarchmail= "";
            int auxtotal=0;
            int cuentalineas=1;

            try
            {
                lee = new FileReader (mx.altec.enlace.utilerias.Global.DOWNLOAD_PATH + codigoA);
                BufferedReader entrada = new BufferedReader (lee);
                log("-!? Antes de Leer el Archivo " );

                linea = entrada.readLine ();

                if( linea != null )
                {
                    log("-!? EXISTEN DATOS " );
                    StringTokenizer st = new StringTokenizer(linea,"$");

                    folio  = st.nextToken();

                    int contadorcol=0;


                    //obtiene todas las columnas del renglon
                    do
                    {
                        contadorcol++;
                        if( contadorcol != 2 )
                            nuevalinea = nuevalinea + "$" + st.nextToken();

                        if( contadorcol == 2 )
                        {
                            nuevalinea = nuevalinea + "$" + "*";
                            lineacuenta = lineacuenta + "$" + st.nextToken() + " " + st.nextToken();
                         }//end if col=2

                         if( contadorcol == 10 )
                         {
                             miselect = "<SELECT NAME=\"Email\" style=\"height: 22px; width: 200px\">";
                             while( (st.hasMoreTokens()) && (contadorcol < 15) )
                             {
                                datomitoken = st.nextToken();
                                if ( !datomitoken.equals("_") )
                                {
                                    miselect = miselect + "<OPTION value = \"\">" + datomitoken + "</OPTION>";
                                    datosparaarchmail = datosparaarchmail + datomitoken + ", ";
                                }
                                contadorcol++;
                             }//end while
                             miselect = miselect + "<Option value = value = \"\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Option></select></td>";
                             nuevalinea = nuevalinea + "$" + "#";
                         }//end  if col=10
                    }while( st.hasMoreTokens() );

                    /************************************************************/
                    //lee el siguiente registro
                    /*****************************************************************/
                    while((linea = entrada.readLine ()) != null)
                    {
                           StringTokenizer stdos = new StringTokenizer(linea,"$");
                           String foliodos = "";
                           foliodos = stdos.nextToken();

                           if( folio.equals(foliodos) )
                           {
                               stdos.nextToken();
                               lineacuenta = lineacuenta + "$" + stdos.nextToken() + " " + stdos.nextToken();
                           }
                           else  //son diferentes los datos otro renglon
                           {
                               if( h == true )
                               {
                                   simbolo = "textabdatobs";
                                   colorcelda = "#CCCCCC";
                                   h = false;
                               }
                               else
                               {
                                   simbolo = "textabdatcla";
                                   colorcelda = "#EBEBEB";
                                   h = true;
                               }
                               nuevalinea = nuevalinea + "$" + folio;

                               lineaaux = pintarcelda(nuevalinea,lineacuenta,miselect,simbolo,datosparaarchmail, cuentalineas, auxtotal, prev);

                              /******************************************************************************
                              //   validacion para paginaci�n
                              /***************************************************************************/
                               if( cuentalineas > prev && cuentalineas <= next )
                               {
                                    todaslaslineas = todaslaslineas + "<tr bgcolor=" + colorcelda + ">" + lineaaux + "</tr>";
                                    //registrosMostrar = registrosMostrar + datosparaarch;
                               }
                               cuentalineas++;
                               datosparaarchmail="";

                               lineacuenta = ""; nuevalinea="";
                               folio = foliodos;

                               contadorcol=0;

                               while( stdos.hasMoreTokens() )
                               {
                                    contadorcol++;

                                    if( contadorcol != 2 )
                                        nuevalinea = nuevalinea + "$" + stdos.nextToken();

                                    if( contadorcol == 2 )
                                    {
                                        nuevalinea = nuevalinea + "$" + "*";
                                        lineacuenta = lineacuenta + "$" + stdos.nextToken() + " " + stdos.nextToken();
                                    }

                                    if( contadorcol == 10 )
                                    {
                                        miselect = "<SELECT NAME=\"Email\" style=\"height: 22px; width: 200px\">";
                                        datomitoken = "";
                                        while( (stdos.hasMoreTokens()) && (contadorcol < 15) )
                                        {
                                            datomitoken = stdos.nextToken();
                                            if ( !datomitoken.equals("_") )
                                            {
                                                miselect = miselect + "<OPTION value = \"\">" + datomitoken + "</OPTION>";
                                                datosparaarchmail = datosparaarchmail + datomitoken + ", ";
                                            }
                                            contadorcol++;
                                        }
                                      miselect = miselect + "<Option value = value = \"\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Option></select></td>";
                                      nuevalinea = nuevalinea + "$" + "#";
                                    }//end col=10
                               }//en While Columnas
                           }//end else nuevo renglon
                    }//End while
                    //Ultimos datos que fueron leidos
                    if( h == true )
                    {
                        simbolo = "textabdatobs";
                        colorcelda = "#CCCCCC";
                        h = false;
                    }
                    else
                    {
                        simbolo = "textabdatcla";
                        colorcelda = "#EBEBEB";
                        h = true;
                    }
                    //Ultimos datos que fueron leidos
                    nuevalinea = nuevalinea + "$" + folio;
                    auxtotal=1;
                    lineaaux = pintarcelda(nuevalinea,lineacuenta,miselect,simbolo,datosparaarchmail,cuentalineas,auxtotal,prev);
                    EIGlobal.mensajePorTrace ("NO LEES NADA", EIGlobal.NivelLog.DEBUG);
                    if( (cuentalineas >= prev) && (cuentalineas <= next) ){
                        EIGlobal.mensajePorTrace ("******************************* Ultima Ecribo linea", EIGlobal.NivelLog.DEBUG);
                        //registrosMostrar = registrosMostrar + datosparaarch;
                        todaslaslineas = todaslaslineas + "<tr bgcolor=" + colorcelda + ">" + lineaaux + "</tr>";
                    }
                    cuentalineas++;
                //Ultimos datos que fueron leidos*/


                }//end if
                else
                    EIGlobal.mensajePorTrace ("NO LEES NADA", EIGlobal.NivelLog.DEBUG);
                    log("-!? NO EXISTEN DATOS EN ARCHIVO " );

                tablaapintar = tablaapintar + todaslaslineas;
                entrada.close ();
            }
            catch(Exception e)
            {
              EIGlobal.mensajePorTrace (""+e,EIGlobal.NivelLog.ERROR);
              e.printStackTrace ();
            }
            finally
            {
              try
              {
                 lee.close();
              }
              catch(Exception e) {}
            }

            req.setAttribute ( "tabladato", tablaapintar );
             total=cuentalineas-1;
             if (total>mx.altec.enlace.utilerias.Global.NUM_REGISTROS_PAGINA1){
                paginacion="<table  width=450 align=center>";

                if(prev > 0) {
                    paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:atras();\">< Anteriores "+mx.altec.enlace.utilerias.Global.NUM_REGISTROS_PAGINA1+"</A></td></TR>";
                }
                if(next < total) {
                     if((next + mx.altec.enlace.utilerias.Global.NUM_REGISTROS_PAGINA1) <= total) {
                        paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer (next -prev).toString () + " ></A></td></TR>";
                    } else {
                        paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer (total-next).toString () + " ></A></td></TR>";
                    }
                }

                paginacion+="</table><br><br>";
                EIGlobal.mensajePorTrace ( "***paginacion :"+paginacion, EIGlobal.NivelLog.INFO);

            }

            req.setAttribute ("total", ""+total);
            req.setAttribute ("prev", ""+prev);
            req.setAttribute ("next", ""+next);
            req.setAttribute ("paginacion", paginacion);
            EIGlobal.mensajePorTrace("Paginacion "+paginacion, EIGlobal.NivelLog.INFO);
            req.setAttribute ("varpaginacion", ""+mx.altec.enlace.utilerias.Global.NUM_REGISTROS_PAGINA1);
            log("-!? Fin de la Obtencion de Datos " );

            //Parametrso de mensajes
            req.setAttribute ("codigoE1",codigoE1);
            req.setAttribute ("codigoX1",codigoX1);
            mensajeD=mensajeD.replace('_', ' ');
            req.setAttribute ("can",canD);
            req.setAttribute ("mensaje",mensajeD);

            req.setAttribute ( "FechaHoy", Global.fechaHoy ("dt, dd de mt de aaaa"));
            req.setAttribute ( "MenuPrincipal", session.getStrMenu () );
            req.setAttribute ( "newMenu", session.getFuncionesDeMenu ());

            String contrato_ = session.getContractNumber ();
            if( contrato_!=null )
               log("-!? Prueba Dav El contrato no es nulo ="+contrato_);
            else
            {
               log("-!? Prueba Dav El contrato es nulo le ponemos cadena vacia");
               contrato_ ="";
            }

            req.setAttribute ( "NumContrato", contrato_ );
            req.setAttribute ( "NomContrato", session.getNombreContrato () );
            req.setAttribute ( "NumUsuario", session.getUserID8 () );
            req.setAttribute ( "NomUsuario", session.getNombreUsuario () );
            req.setAttribute ( "URL", "x");

            String fileOut="", fechaexpt = "", valorarchivo = "";
            fileOut = session.getUserID8() + "exportar.doc";

            fechaexpt = ObtenFecha(true);
            fechaexpt = fechaexpt.substring(0,2)+fechaexpt.substring(3,5)+fechaexpt.substring(6,fechaexpt.length());

            //CREAR ARCHIVO DE EXPORTACION
            EI_Exportar ArcSal;
            ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
            EIGlobal.mensajePorTrace( "***Se verificar� que el archivo no exista en caso contrario se borrara y crear� uno nuevo", EIGlobal.NivelLog.DEBUG);
            ArcSal.creaArchivo();

            //DARLE FORMATO A LA LINEA DINAMICAMENTE
            EIGlobal.mensajePorTrace("valores a escribir: " + session.getUserID8() + " " + session.getNombreUsuario() + " " + fechaexpt, EIGlobal.NivelLog.INFO);
            valorarchivo = rellenaCaracter(session.getUserID8(),' ',16,false)+ rellenaCaracter(session.getNombreUsuario(),' ',40,false)+ fechaexpt +
            ObtenHora() + rellenaCaracter("000",'0',15,true) + rellenaCaracter("000",'0',15,true) +
            rellenaCaracter("000",'0',15,true);

            //GRAVAR LINEA DE EXPORTACION
            ArcSal.escribeLinea(registrosMostrar + "\r\n");
            EIGlobal.mensajePorTrace( "***Se escribio en el archivo  "+valorarchivo,EIGlobal.NivelLog.INFO );

            //CERRAR EL ARCHIVO Y COPIARLO
            ArcSal.cierraArchivo();
            ArchivoRemoto archR = new ArchivoRemoto();
            if(!archR.copiaLocalARemoto(fileOut,"WEB"))
                EIGlobal.mensajePorTrace("***cuentasaldo.class No se pudo crear archivo para exportar saldos", EIGlobal.NivelLog.ERROR);

            //LIGA A LA EXPORTACION
            String downloadurl = "/Download/" + fileOut;

            req.setAttribute("downloadurl",downloadurl);

            req.setAttribute( "codigoE", codigoE);
            req.setAttribute( "codigoF", codigoF);
            req.setAttribute( "codigoX", codigoX);
            
            //TODO CU1091
		    /*
			 * 05/ENE/07
			 * VSWF-BMB-I
			 */
            if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
            try{
			    BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_REGISTRO_ENTRA);
				if(session.getContractNumber()!=null){
					bt.setContrato(session.getContractNumber());
				}
				if(fileOut!=null){
					bt.setNombreArchivo(fileOut);
				}
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
            }
		    /*
			 * VSWF-BMB-F
			 */

            RequestDispatcher rdSalida = getServletContext ().getRequestDispatcher ( "/jsp/botones.jsp" );

            res.setContentType("text/html");

            rdSalida.include ( req, res );
            //EIGlobal.mensajePorTrace("-->registrosMostrar "+registrosMostrar, EIGlobal.NivelLog.INFO);
            log("-!? Termino de Movimientos " );
        }

        if( Tipoenlace.equals( "2" )  )
        {//Cancelar
              if (session.getFacultad ( session.FAC_CANPROGMOV_N43 ))
              {//FAC_CANPROGMOV_N43
                  log("-!? Inicio de Movimientos en 2 TUXEDO " );
                  Global.mensajePorTrace (" INICIO TUXEDO ", EIGlobal.NivelLog.DEBUG);

                  String folioobtenido = req.getParameter("Vigfolio");

                  Global.mensajePorTrace (" !!!!!!!!folioobtenido:!!!!! " +  folioobtenido , EIGlobal.NivelLog.INFO);

                  Trama = new StringBuffer("");

                  EIGlobal.mensajePorTrace("CMovimiento: Armando Trama.,", EIGlobal.NivelLog.DEBUG);

                  Trama=new StringBuffer("");
                  Trama.append("1EWEB");
                  Trama.append(pipe);
                  Trama.append(session.getUserID8());
                  Trama.append(pipe);
                  Trama.append("CPRM");
                  Trama.append(pipe);
                  Trama.append(session.getContractNumber());
                  Trama.append(pipe);
                  Trama.append(session.getUserID8());
                  Trama.append(pipe);
                  Trama.append(session.getUserProfile());
                  Trama.append(pipe);
                  Trama.append(folioobtenido);
                  Trama.append("@");
                  Trama.append(pipe);

                  EIGlobal.mensajePorTrace("CMovimiento: Trama Entrada: " + Trama.toString(), EIGlobal.NivelLog.INFO);
                  ServicioTux TuxGlobal = new ServicioTux();
                  //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

                  //########## Cambiar ....
                  try
                  {
                     hs = TuxGlobal.web_red(Trama.toString());
                  }
                  catch( java.rmi.RemoteException re )
                  {
                    re.printStackTrace();
                  }
                  catch (Exception e) {}

                  Result = (String) hs.get("BUFFER");

                  Global.mensajePorTrace ("CMovimiento: Result que llego --> " + Result, EIGlobal.NivelLog.INFO);

                  EIGlobal.mensajePorTrace("CMovimiento: Result que llego --> " + Result.toString(), EIGlobal.NivelLog.INFO);

                  String mensaje = "", codigoerror="", primeraparte="";

                  primeraparte = Result.substring(0, 4);

                  EIGlobal.mensajePorTrace("CMovimiento: primeraparte --> " + primeraparte, EIGlobal.NivelLog.INFO);

                  EIGlobal.mensajePorTrace("CMovimiento: primeraparte --> " + primeraparte, EIGlobal.NivelLog.INFO);

                  codigoerror = Result.substring(4, 8);

                  EIGlobal.mensajePorTrace("CMovimiento: codigoerror --> " + codigoerror, EIGlobal.NivelLog.INFO);

                  EIGlobal.mensajePorTrace("CMovimiento: codigoerror --> " + codigoerror, EIGlobal.NivelLog.INFO);

                  primeraparte = primeraparte.trim();

                  if ( primeraparte.equals("NORM") == false )
                  {
                       RequestDispatcher rdSalida = getServletContext().getRequestDispatcher ( "/jsp/MensajeCancelacion.jsp" );
                       res.setContentType("text/html");
                       rdSalida.include ( req, res );
                  }

                  mensaje = Result.substring(9, Result.length());

                  StringTokenizer stmensaje = new StringTokenizer(mensaje,"@");
                  mensaje = stmensaje.nextToken();
                  //mensaje = mensaje + " \n Folio: " + stmensaje.nextToken();
                  mensaje = mensaje + " Folio: " + stmensaje.nextToken();
                  Global.mensajePorTrace (" NUEVO Result= " + Result, EIGlobal.NivelLog.INFO);

                  String r = "";

                  if ( codigoerror.equals("0000"))
                  {
                	  //TODO BIT CU1091
      			    /*
      				 * 05/ENE/07
      				 * VSWF-BMB-I
      				 */
                	  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
                	  try{
	      			    BitaHelper bh = new BitaHelperImpl(req, session, sess);
	      				BitaTransacBean bt = new BitaTransacBean();
	      				bt = (BitaTransacBean)bh.llenarBean(bt);
	      				bt.setNumBit(BitaConstants.EC_MOV_CHEQ_PROG_REGISTRO_CANCEL_PROGRAMACIONES);
	      				if(session.getContractNumber()!=null){
	      					bt.setContrato(session.getContractNumber());
	      				}
	      				if(Result!=null){
			    			if(Result.substring(0,2).equals("OK")){
			    				bt.setIdErr("CPRM0000");
			    			}else if(Result.length()>8){
		      				bt.setIdErr(Result.substring(0,8));
		      				}else{
		      					bt.setIdErr(Result.trim());
		      				}
	      				}
	      				bt.setServTransTux("CPRM");
	      				BitaHandler.getInstance().insertBitaTransac(bt);
      				}catch(SQLException e){
      					e.printStackTrace();
      				}catch(Exception e){
      					e.printStackTrace();
      				}
                	  }
      			    /*
      				 * VSWF-BMB-F
      				 */
                       Global.mensajePorTrace (" Result con 0000 = " + Result, EIGlobal.NivelLog.INFO);
                       mensaje=mensaje.replace(' ', '_');
                       res.sendRedirect("csaldo1?prog=1&prog1=0&c=0&m="+mensaje);
                  } else if (codigoerror.equals("XXXX"))
                  {
                       mensaje=mensaje.replace(' ', '_');
                       res.sendRedirect("csaldo1?prog=1&prog1=0&c=1&m="+mensaje);
                  }
                  else
                  {
                        mensaje ="por el momento no sepuede realizar la operaci�n";
                        mensaje=mensaje.replace(' ', '_');
                        res.sendRedirect("csaldo1?prog=1&prog1=0&c=2&m="+mensaje);
                  }

              }//FAC_CANPROGMOV_N43
              else
              {
                  String laDireccion = "document.location='SinFacultades'";
                  req.setAttribute( "plantillaElegida", laDireccion);
                  res.setContentType("text/html");
                  req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
              }
        }//Cancelar
       }
  }

}
