/*
ltima modificacin :


13/05/2004
MX-2004-196-01 SPEI Cuenta Ordenante
Se agrega la cuenta clabe en la trama de regreso de las transferencias en linea.
para mostrarse en el comprobante de operacin.
*/

/**Praxis
*Q05-0001087
*ivn Ma. Isabel Valencia Navarrete
*27/01/2005
*CUANDO SE REALIZA UNA TRANSFERENCIA PINTAR EN EL LOG FECHA, HR,
*DIRECCIN IP(DE DONDE FUE REALIZADA LA TRANSFERENCIA)
*EJEMPLO:
*[20/Jan/2005 12:02:03] DIRECCIN IP  TRAMA
*/

/* EVERIS
 * 28 Mayo 2007
 * Art. 52 Solucion de raiz
 * Jose Antonio Rodriguez Alba
 * Modificacion en la forma en que se realizan las transacciones por archivo.
 * See realiza el llamado de servicios tuxedo: generador de folio,
 * controlador de registros y dispersor de transacciones.
 * Estos requieren archivos de tramas, bitacora y de salida.
 *
 **/

package mx.altec.enlace.servlets;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.AdminHorariosBean;
import mx.altec.enlace.beans.ExportTransferenciasBean;
import mx.altec.enlace.beans.OperacionesProgramadasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.AdminHorariosBO;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.Convertidor;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.OperacionesProgramadasBO;
import mx.altec.enlace.bo.TransferenciaInterbancariaBUS;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.FlujosConstants;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.TransferenciasMonitorPlusUtils;

public class MDI_Enviar extends BaseServlet{

  	/**
  	 * Constante con la literal "DIBT"
  	 */
  	public static final String DIBT = "DIBT";
  	
  	
	/** Constante para la cadena "titular". */
	private static final String TITULAR = "titular";


      // EVERIS 28 Mayo 2007
      // Variable boolean indica si hubo un error en el envio de folio
	  /**
	   * Variable boolean para manejar error
	   */
      transient boolean errorEnvio = false;
      /**
       * Variable para almacenar la sucursal
       */
      transient String sucuOpera="";
      /**
       * Variable para el medio de entrega
       */
      transient String medioEntrega="";
      /**
       * Variable para la referencia
       */
      transient String referenciaMed="";
      /**
       * Variable para el separador
       */
      transient String pipe="|";
      /**
       * Variable para la sucursal
       */
      transient short sucursalOpera=(short)787;
      /**
       * Variable para el tipo de operacion
       */
      transient private String tipoOperacion="";


  	
    /**
     * Metodo para recibir la llamadas get
     * @param req objeto HttpServletRequest
     * @param res objeto HttpServletResponse
	 * @exception ServletException excepcion a manejar
	 * @exception IOException excepcion a manejar
     */
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

    /**
     * Metodo para recibir la llamadas post
     * @param req objeto HttpServletRequest
     * @param res objeto HttpServletResponse
	 * @exception ServletException excepcion a manejar
	 * @exception IOException excepcion a manejar
     */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
     * @param req objeto HttpServletRequest
     * @param res objeto HttpServletResponse
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
     */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		String contrato="";
		String usuario="";
	    String clavePerfil="";
		String[][] arrayCuentasCargo=null;
//VSWF RRG 08-Dic-2008 I Se le agrego el segundo parametro al metodo decode, para migracion Enlace
		String enc="UTF-8";
//VSWF RRG 08-Dic-2008 F


		boolean arcExp=false;

		EI_Tipo Todas=new EI_Tipo(this);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		boolean banderaTransaccion = true;

		if(req.getSession().getAttribute("Recarga_MDI_Enviar")!=null && verificaArchivos(Global.DOWNLOAD_PATH +req.getSession().getId().toString()+".ses",false))
		 {
			EIGlobal.mensajePorTrace( "MDI_Enviar. El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion =false;
		 }
		else
		if(req.getSession().getAttribute("Recarga_MDI_Enviar")!=null)
		 {
		   EIGlobal.mensajePorTrace("MDI_Enviar Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
		   req.getSession().removeAttribute("Recarga_MDI_Enviar");
		   banderaTransaccion=false;
		 }
		else
		if(verificaArchivos(Global.DOWNLOAD_PATH +req.getSession().getId().toString()+".ses",false) )
		 {
			EIGlobal.mensajePorTrace( "MDI_Enviar El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion = false;
		 }
		else
		if(req.getSession().getAttribute("Recarga_MDI_Enviar")==null && !verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false))
		 {
			EIGlobal.mensajePorTrace( "MDI_Enviar Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion = true;
		 }

		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Entrando a Envio de Transferencias. Vers(1.1) 18 Enero 2005", EIGlobal.NivelLog.DEBUG);

		boolean sesionvalida = SesionValida( req, res );

                if ( !sesionvalida ) { return; }
                /******************************************Inicia validacion OTP**************************************/
                /**
    			 * Inicio PYME 2015 Unificacion Token Contrasena incorrecta
    			 */
                String valida = "";
    			if( (req.getAttribute("validaNull") != null && "si".equals(req.getAttribute("validaNull"))) &&
    				(req.getAttribute("forzar") != null && "si".equals(req.getAttribute("forzar"))) ){
    				valida = null;
    				EIGlobal.mensajePorTrace( "------------valida---->"+ valida, EIGlobal.NivelLog.DEBUG);
    			} else {
    				valida = req.getParameter("valida");
    				if( ( valida == null || "".equals( valida ) ) &&
    					req.getAttribute("valida2") != null ){
    					valida = req.getAttribute("valida2").toString();
    				}
    			}
    			/**
    			 * Fin PYME 2015 Unificacion Token
    			 */
		        String validaChall = req.getAttribute( "validaChallenge" ) != null
		        	? req.getAttribute( "validaChallenge" ).toString() : "";

	        	EIGlobal.mensajePorTrace("--------->banderaTransaccion" + banderaTransaccion, EIGlobal.NivelLog.DEBUG);
	        	EIGlobal.mensajePorTrace("--------->valida" + valida, EIGlobal.NivelLog.DEBUG);
	        	EIGlobal.mensajePorTrace("--------->validaChall" + validaChall, EIGlobal.NivelLog.DEBUG);

		        if(validaPeticion( req,  res,session,sess,valida))
				{
		            EIGlobal.mensajePorTrace(" ENTRA A VALIDAR LA PETICION ", EIGlobal.NivelLog.DEBUG);
		        }
				/******************************************Termina validacion OTP**************************************/
                else if(banderaTransaccion ||  (valida!=null && "1".equals(valida)) || (validaChall!=null && "1".equals(validaChall)))
			{
				EIGlobal.mensajePorTrace( "MDI_Enviar entra en banderaTransaccion Ok", EIGlobal.NivelLog.DEBUG);
				//Creacion de archivo de rastro de sesion
				EI_Exportar arcTmp=new EI_Exportar(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses");

				if(arcTmp.creaArchivo()){
				EIGlobal.mensajePorTrace( "MDI_Enviar. El archivo.ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);}

				req.getSession().setAttribute("Recarga_MDI_Enviar","TRUE");

				//Variables de sesion ...
				contrato=session.getContractNumber();
				sucursalOpera=Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
				usuario=session.getUserID8();
				clavePerfil=session.getUserProfile();


				//***** everis Cerrando RandomAccessFile (EI_Exportar) 06/05/2008  ..inicio
				arcTmp.cierraArchivo();
				//****  everis Cerrando RandomAccessFile (EI_Exportar) 06/05/2008  ..fin
				String encoded = getFormParameter(req,"encoded");
				if (encoded == null){
					encoded = "false";
				}

				/** INICIO CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token se cambia
				 * getParameter por getAttribute por que no contenia valor*/
				EIGlobal.mensajePorTrace("<--------OIVI validacion del correcto encodig-------> TotalTrans [" + req.getAttribute("TotalTrans") + "]", EIGlobal.NivelLog.DEBUG);

				if(req.getAttribute("TotalTrans") != null  && "Archivo de Exportacion".equals(String.valueOf(req.getAttribute("TotalTrans")).trim())) {
					arcExp=true;
			 	}
				else if(req.getAttribute("TotalTrans") != null) {
					if (encoded == null || "true".equals(encoded)) {
						EIGlobal.mensajePorTrace("<--------OIVI validacion del correcto encodig-------> TotalTrans [" + req.getAttribute("TotalTrans") + "]", EIGlobal.NivelLog.DEBUG);
						Todas.iniciaObjeto(new String(req.getAttribute("TotalTrans").toString().getBytes("ISO-8859-1"),"UTF-8")   );
					} else {
						Todas.iniciaObjeto(req.getAttribute("TotalTrans").toString());
					}

				}
				else {
					EIGlobal.mensajePorTrace("TotalTrans viene nulo", EIGlobal.NivelLog.INFO);
				}
				/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token se cambia
				 * getParameter por getAttribute */
				///////////////////--------------challenge--------------------------
				String validaChallenge = req.getAttribute("challngeExito") != null
					? req.getAttribute("challngeExito").toString() : "";

				EIGlobal.mensajePorTrace("---------------> MDI_Enviar > validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

				if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
					EIGlobal.mensajePorTrace("---------------> MDI_Enviar > Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
					req.setAttribute("validaChallenge", "1");
					ValidaOTP.guardaParametrosEnSession(req);
					/**
					 * PYME 2015 Unificacion Token para contraseρa incorrecta
					 */
					req.setAttribute("vieneDe", "/enlaceMig/MDI_Enviar");
					validacionesRSA(req, res);
					return;
				}

				//Fin validacion rsa para challenge
				///////////////////-------------------------------------------------

				//interrumpe la transaccion para invocar la validacin

				EIGlobal.mensajePorTrace("-----------> valor valida: ["+valida+"]", EIGlobal.NivelLog.DEBUG);

				boolean valBitacora = true;
				if(  valida==null){
					EIGlobal.mensajePorTrace("Interrumpe el flujo", EIGlobal.NivelLog.DEBUG);

					boolean solVal=ValidaOTP.solicitaValidacion(contrato,IEnlace.TRANS_INTERBANC);

					if(session.getValidarToken() &&
                                            session.getFacultad(session.FAC_VAL_OTP) &&
                                            session.getToken().getStatus() == 1 &&
                                            solVal)
					{
						//VSWF-HGG -- Bandera para guardar el token en la bitcora
						req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
						/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
						 * se redirecciona al Servlte MDI_Cotizar para que valide
						 * la operaciones y despues regrese para que continue
						 * con la validacion del token*/
						String mod =(String) getFormParameter(req,"Modulo");
						String radio = (String) getFormParameter(req,"Registradas");
						EIGlobal.mensajePorTrace("LINEAS [" + (String) req.getAttribute("Lineas") + "]", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("IMPORTE [" + (String) req.getAttribute("Importe") + "]", EIGlobal.NivelLog.DEBUG);

						// Transferencias archivo
					  	// INDRA PYME ABRIL 2015



						// Transferencias archivo
					  	// INDRA PYME ABRIL 2015
						req.setAttribute("MenuPrincipal", session.getStrMenu());
						req.setAttribute("newMenu", session.getFuncionesDeMenu());
						req.setAttribute("Encabezado", CreaEncabezado("Transferencias Interbancarias","Transferencias Interbancarias &gt  Validaci&oacute;n de L&iacute;mites y Montos",req));
						// Transferencias programadas
					  	// INDRA PYME ABRIL 2015
					    req.setAttribute("TransReg",req.getAttribute("TransReg"));
					    req.setAttribute("TransNoReg",req.getAttribute("TransNoReg"));
					 // PYME 2015 para no mostrar nuevamente mensaje de los duplicados cuando sea nip incorrecto en token
						if( (req.getAttribute("validaNull") != null && "si".equals(req.getAttribute("validaNull"))) &&
							(req.getAttribute("forzar") != null && "si".equals(req.getAttribute("forzar"))) ){
							req.setAttribute("InfoUser", null);
							EIGlobal.mensajePorTrace( "------------Viene de Token Incorrecto2---->", EIGlobal.NivelLog.DEBUG);
						}
						// Transferencias programadas
					  	// INDRA PYME ABRIL 2015
						ValidaOTP.guardaParametrosEnSession(req);
						req.getSession().removeAttribute("mensajeSession");
						EIGlobal.mensajePorTrace( "------------ DIVISA ----> divTrx " 
						                        +  getFormParameter(req, "divTrx")
						                        + " divisaOper " + getFormParameter(req, "divisaOper"), EIGlobal.NivelLog.DEBUG);
						req.setAttribute("DivisaMsjOper", getFormParameter(req, "divTrx"));
						ValidaOTP.mensajeOTP(req, "TransInterBanc");
						req.removeAttribute("DivisaMsjOper");
						/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
						 * Se modica jsp al que se envia */
						if( "2".equals( mod ) && "2".equals( radio ) ){
							// Cuando se importa un archivo
							//Duplicados archivo
							//INDRA 04/05/2015


							if ( req.getAttribute("Tabla") == null ) {
								req.setAttribute("Tabla", "");
							}
							ValidaOTP.validaOTP(req,res, "/jsp/MDI_Cotizar.jsp");
						} else {
							// Cuando son Registradas y No Registradas
							ValidaOTP.validaOTP(req,res, "/jsp/MDI_Cotizar.jsp");
						}
						/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
					} else{
						ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
						valida="1";
						valBitacora = false;
					}
				}
                //retoma el flujo
                if( valida!=null && "1".equals(valida))
				{
                	try{
                		/** INICIO PYME FSW - INDRA MARZO 2015 Unificacion Token se cambia
						 * tmpParametros por getAttribute por que no contenia valor*/
                	EIGlobal.mensajePorTrace("<--------OIVI validacion del correcto encodig-------> TotalTrans [" + req.getAttribute("TotalTrans") + "]", EIGlobal.NivelLog.DEBUG);
                	if("Archivo de Exportacion".equals(java.net.URLDecoder.decode((String) req.getAttribute("TotalTrans"),enc).trim())){
       				 arcExp = true;
                	} else if (encoded == null || "true".equals(encoded)) {
						Todas.iniciaObjeto(new String(req.getAttribute("TotalTrans").toString().getBytes("ISO-8859-1"),"UTF-8")   );
					} else {
						Todas.iniciaObjeto(req.getAttribute("TotalTrans").toString());
					}
                	/** FIN PYME FSW - INDRA MARZO 2015 Unificacion Token se cambia
					 * tmpParametros por getAttribute por que no contenia valor*/
                //	else
       			//	 Todas.iniciaObjeto(java.net.URLDecoder.decode((String) req.getParameter("TotalTrans"),enc));

                	} catch(Exception e ) {
                		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                	}
					int[] resultadoEnv = iniciaEnviar(Todas,arcExp,arrayCuentasCargo,clavePerfil,usuario,contrato, req, res );//<########Daniel########>

					//ARS Indra Inicio

					if (valBitacora)
					{
						try{
							HttpSession sessionBit = req.getSession(false);
							Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
							int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
							String claveOperacion = BitaConstants.CTK_TRANSFERENCIAS_INTERB;
							String concepto = BitaConstants.CTK_CONCEPTO_TRANSFERENCIAS_INTERB;
							String token = " ";
							if (req.getParameter("token") != null && !req.getParameter("token").equals(""));
							{token = req.getParameter("token");}
							EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.INFO);
							double importeDouble=0;

							if (tmpParametros!=null) {
								Enumeration enumer = req.getParameterNames();

								while(enumer.hasMoreElements()) {
									String name = (String)enumer.nextElement();
									EIGlobal.mensajePorTrace("Parametro " + name + "->" + tmpParametros.get(name), EIGlobal.NivelLog.INFO);
								}

								String importe = tmpParametros.get("Importe").toString();
								/** PYME FSW - INDRA MARZO 2015 Unificacion Token se cambia
								 * tmpParametros por getAttribute por que no contenia valor*/
								String totalTrans = req.getAttribute("TotalTrans").toString();

								if(importe.contains(":")){
									importe = importe.substring(importe.indexOf(":"));
								}
								if(importe.contains("$")){
									importe = importe.substring(importe.indexOf("$") + 1);
								}


								if("Archivo de Exportacion".equals(totalTrans)){
									importeDouble = Double.valueOf(importe).doubleValue();
								} else {
									String importeTotal = sess.getAttribute("importeTIB").toString();
									importeDouble = Double.valueOf(importeTotal).doubleValue();
								}
							}

							EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

							sessionBit.removeAttribute("parametrosBitacora");

							@SuppressWarnings("unused")
							String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,"0",importeDouble,claveOperacion,"0",token,concepto,0);
							BitaHelper bh = new BitaHelperImpl(req, session, req.getSession());
							int referenciaBit = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
							OperacionesProgramadasBO operacionesProgramadasBO = new OperacionesProgramadasBO();

							//if( resultadoEnv[1] == 1&&!operacionesProgramadasBO.validaContratoMancomunado(contrato) ){
								//bitacoraTCT(bh, session.getUserID8(),referenciaBit,req.getAttribute("concepto").toString(),req.getAttribute("cuentaCargo").toString(),req.getAttribute("cuentaAbono").toString(),importeDouble);
							//}
						} catch(Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					}
					//ARS Indra Final

					if(resultadoEnv[0] > 0) {
						req.setAttribute("MenuPrincipal", session.getStrMenu());
						req.setAttribute("newMenu", session.getFuncionesDeMenu());
						req.setAttribute("Encabezado", CreaEncabezado("Transferencias Interbancarias","Transferencias Interbancarias &gt  Validaci&oacute;n de L&iacute;mites y Montos",req));

						evalTemplate("/jsp/transferencia_val_lym.jsp", req, res);

						return;
					}
                        ValidaOTP.guardaBitacora((List)req.getSession(false).getAttribute("bitacora"),tipoOperacion);
                           //San, se comenta el evaltemplate y se prueba el send redirect
                        	EIGlobal.mensajePorTrace("**555************************cambio invoca pagina retomada.", EIGlobal.NivelLog.DEBUG);
                         if(arcExp){
                         	// EVERIS 28 Mayo 2007 Linea agregada para mostrar el error si lo hubo en lugar del archivo con folio
                         	if(!errorEnvio){
                         		evalTemplate("/jsp/MDI_EnviarArchivo.jsp", req, res);
                         	}
                         } else {
                             evalTemplate("/jsp/MDI_Enviar.jsp", req, res);
                         }                       
                        EIGlobal.mensajePorTrace("Regresa de enviar datos y de guardar en bitacorao", EIGlobal.NivelLog.DEBUG);
				}
			}
			else
            {

				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Env&iacute;o de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Cotizaci&oacute;n &gt; Env&iacute;o","s55240",req));
				req.setAttribute("totalRegistros",(String) req.getParameter("Lineas"));
				req.setAttribute("Importe",(String) req.getParameter("Importe"));

				evalTemplate("/jsp/MDI_Inter_TranenProceso.jsp", req, res);

				EIGlobal.mensajePorTrace( "MDI_Interbancario.  Accesa a servicio Ocupado", EIGlobal.NivelLog.DEBUG);
		    }

		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Saliendo de Envio de Transferencias.", EIGlobal.NivelLog.DEBUG);
   }


/*************************************************************************************/
/******************************************************************** Modulo Enviar  */
/*************************************************************************************/
	
	/**
	 * Modulo Enviar
	 * @param Todas EI_Tipo
	 * @param arcExp boolean
	 * @param arrayCuentasCargo String[][]
	 * @param clavePerfil String
	 * @param usuario String
	 * @param contrato String
	 * @param req objeto HttpServletRequest
	 * @param res objeto HttpServletResponse
	 * @return int[]
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
  public int[] iniciaEnviar(EI_Tipo Todas, boolean arcExp,String[][] arrayCuentasCargo,String clavePerfil,String usuario,String contrato, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
	 String nombreArchivo=usuario+".lti";
	 String mensajeError="";
	 String arcLinea="";
	 StringBuffer arcLineaSalida;
	 TransferenciaInterbancariaBUS transInterbBUS=TransferenciaInterbancariaBUS.getInstance();
	 String Referencia="";
	 String TipoError="";
	 String tipoErrorExportacion = "";
	 String codError="";
//	 String nuevaTrama="";
	 StringBuilder nuevaTrama = new StringBuilder();
	 StringBuffer strComprobante;
     String cta_clabe="-";
     int[] respuestaProgramadoResultado = new int[2];
	 String nombreSalida=usuario+".doc";
	 StringBuffer strTabla=new StringBuffer("");
	 String Trama="";
	 String Result="";
	 String cuentaCargo = "";
	 String tipoRelacion = "";
	 String titular="";
	 String cuentaAbono="";
	 String banco="";
	 String beneficiario="";
	 String importe="";
	 String plaza="";
	 String concepto="";
	 String referencia =""; //LT
	 String formaAplica="";
	 String sucursal ="";
	 /* VECTOR 06-2016: SPID */
	 String tipoDivisa = "";
	 String rfcTransac = "";
	 String spidTipoOperacion = "";

	 double importeTotal=0;

	 boolean comuError=false;
	 boolean salidaArc=false;
	 boolean noError=true;
	 int totalLineas=0;
	 int totalRegistros=0;

	 String referencias = "";
	 StringBuilder cod_error = new StringBuilder();
	 String cod_error_string = "";

	 // EVERIS 28 Mayo 2007 Declaracion de Variables
	 int numRegistrosOk = 0;
	 String folioArchivo = "";
	 @SuppressWarnings("unused")
	String mensajeErrorFolio = "";
	 String mensajeErrorEnviar = "";
	 String mensajeErrorDispersor = "";
	 ValidaCuentaContrato valCtas = null;
	 // EVERIS 28 Mayo 2007 Fin Declaracion de Variables

//VSWF RRG 08-Dic-2008 I Se le agrego el segundo parametro al metodo decode, para migracion Enlace
		String enc="UTF-8";
//VSWF RRG 08-Dic-2008 F
		
		nuevaTrama.delete(0, nuevaTrama.length());


	 /************* Modificacion para la sesion ***************/
	 HttpSession ses = req.getSession();
	 BaseResource session = (BaseResource) ses.getAttribute("session");
	 EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	 EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreSalida);
	 EI_Exportar ArcEnt=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);

	 // EVERIS 01 Junio 2007 Creacion de archivo con tramas sin linea en blanco al principio
	 EI_Exportar ArcTramas = null;
	 String archivoTramas = null;

	 // EVERIS 28 Mayo 2007 Creacion de Archivo con datos de la Bitacora
	 String archivoBitacora = null;
	 EI_Exportar ArcBit = null;
	 boolean archivoBitCreado = false;
	 String tramaBitacora = "";
	// EVERIS 28 Mayo 2007

	 ServicioTux TuxGlobal = new ServicioTux();
     medioEntrega=IEnlace.medioEntrega1;
     sucuOpera=Integer.toString(sucursalOpera);
     referenciaMed=EnlaceGlobal.fechaHoy("ddthtmts");

	 EI_Tipo objTrama= new EI_Tipo();
	 Convertidor conv=new Convertidor();

     strTabla.append("\n<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");
     strTabla.append("\n <tr>");
//GOR 20110113 (Req. Enlace - 3 - Inicio): Cambiar en la columna donde dice REFERENCIA DE OPERACI?N por N?MERO DE REFERENCIA
     strTabla.append("\n  <th class='tittabdat'>&nbsp; N&uacute;mero de Referencia&nbsp; </th>");
//GOR 20110113 (Req. Enlace - 3 - Fin)
     strTabla.append("\n  <th class='tittabdat'>&nbsp; Estatus &nbsp; </th>");
     strTabla.append("\n  <th class='tittabdat'>&nbsp; Cuenta Origen &nbsp; </th>");
     strTabla.append("\n  <th class='tittabdat'>&nbsp; Cuenta Destino/M&oacute;vil &nbsp; </th>");
	 strTabla.append("\n  <th class='tittabdat'>&nbsp; Beneficiario &nbsp; </th>");
     /* VECTOR 06-2016: SPID : Se agrega nueva columna para mostrar el tipo de divisa */	
     strTabla.append("\n  <th class='tittabdat'>&nbsp; Divisa &nbsp; </th>"); 
	 
     strTabla.append("\n  <th class='tittabdat'>&nbsp; Importe &nbsp; </th>");
          
//GOR 20110113 (Req. Enlace - 4 - Inicio): Cambiar en la columna donde dice CONCEPTO por CONCEPTO DEL PAGO / TRANSFERENCIA
     strTabla.append("\n  <th class='tittabdat'>&nbsp; Concepto del Pago / Transferencia &nbsp; </th>");
//GOR 20110113 (Req. Enlace - 4 - Fin)
     strTabla.append("\n  <th class='tittabdat'>&nbsp; Referencia Interbancaria&nbsp; </th>");
     strTabla.append("\n </tr>");

	String referenciaArchivo = "";
	LYMValidador valida = new LYMValidador();
	ArrayList <String> respLYM = new ArrayList<String>();
	int linea = 0;
	 if(arcExp)
	  {
	  // EVERIS 28 Mayo 2007 Obtencion del numero de lineas sin error
	 	if(ArcEnt.abreArchivo() )
	 		{
	 		if(!(inicializando()== 1))
				{
			  	arcLinea="ERROR    000000";
			  	EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): No pudo inicializar la conexion.",EIGlobal.NivelLog.INFO);

			  	//***** everis Cerrando RandomAccessFile (EI_Exportar) 06/05/2008  ..inicio
			  	ArcEnt.cierraArchivo();
				//****  everis Cerrando RandomAccessFile (EI_Exportar) 06/05/2008  ..fin

				}
			else
				{
	 			do{
	 				arcLinea=ArcEnt.leeLinea();
	 				EIGlobal.mensajePorTrace(" ******* ARCENT" + arcLinea, EIGlobal.NivelLog.INFO);
	 				if(!arcLinea.equals("ERROR"))
	 					{
	 					numRegistrosOk++;

	 					String resLYM = valida.validaLyM(arcLinea);
						if(!resLYM.equals("ALYM0000")) {
							respLYM.add((++linea) + "|" + resLYM);
						}

	 					}
					if ( ArcEnt.eof() )
						{
						noError = false;
						}
				  }while(noError);
				ArcEnt.cierraArchivo();
				valida.cerrar();
				noError=true;
				}
				desconectando();
	  		}

	 	if(!respLYM.isEmpty()) {
			req.getSession().setAttribute("erroresLYM", respLYM);
			respuestaProgramadoResultado[0] = respLYM.size();
			respuestaProgramadoResultado[1] = 0;
			return respuestaProgramadoResultado;
		}
	 	//YHG Aqui se insertan las opciones por archivo, esta funcionalidad se implementa en el transacexecrvr
	  	// EVERIS 28 Mayo 2007 Servicio Tuxedo para generar folio
		String tramaParaFolio = BaseServlet.convierteUsr8a7(usuario) + "|" + clavePerfil + "|" +contrato + "|DIBT|" + numRegistrosOk + "|";
		try{
			Hashtable hs = TuxGlobal.alta_folio(tramaParaFolio);
			Result= String.valueOf(hs.get("BUFFER"));
			}
		catch( java.rmi.RemoteException re )
			{
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
			mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde. Error al generar Folio. ";
			errorEnvio=true;
			despliegaPaginaError(mensajeError,"Env&iacute;o de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Cotizaci&oacute;n &gt; Env&iacute;o","s25470h", req, res);
			} catch(Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}


		if(Result==null || "null".equals(Result) ){
			Result="ERROR 	   Error en el servicio.";
		}
		EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() GENERADOR DE FOLIO : Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

		try {
			referenciaArchivo = Result.substring(Result.lastIndexOf("|")+1);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

		codError=Result.substring(0,Result.indexOf("|") );

		if(!"TRSV0000".equals(codError.trim()))
			{
			comuError=true;
			mensajeErrorFolio = Result;
			mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde. Error al generar Folio. ";
			errorEnvio=true;
			}
		else
			{ // EVERIS 28 Mayo 2007 Inicio ELSE condicional de recepcion de folio
			folioArchivo = Result.substring( Result.indexOf("|")+1, Result.length() );
			folioArchivo = folioArchivo.trim();
			if ( folioArchivo.length() < 12)
				{
				do{
					folioArchivo = "0" + folioArchivo;
				  }while(folioArchivo.length() < 12);
				}
			if ( ( folioArchivo.length() != 12 ) || "000000000000".equals(folioArchivo) )
				{
				comuError=true;
				mensajeErrorFolio = Result;
				mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde. Error al generar Folio: Folio Incorrecto. ";
				errorEnvio=true;
				}
			else
			{ // EVERIS 28 Mayo 2007 Inicio ELSE condicional de folio correcto
			req.setAttribute("folioArchivo", folioArchivo);

			// EVERIS 28 Mayo 2007 Generacion del archivo con tramas sin linea en blanco al principio
			archivoTramas = usuario + "_"+folioArchivo+"TIB.tram";
			ArcTramas = new EI_Exportar(IEnlace.DOWNLOAD_PATH+archivoTramas);
	       	if (ArcTramas.creaArchivo() ){
	       		if(ArcEnt.abreArchivo() )
	 		       {
	 		       if(!(inicializando()== 1))
				     {
			  	     arcLinea="ERROR    000000";
			  	     EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): No pudo inicializar la conexion.", EIGlobal.NivelLog.INFO);
			  	     //***** everis Cerrando RandomAccessFile (EI_Exportar) 06/05/2008  ..inicio
				  	 ArcEnt.cierraArchivo();
					 //****  everis Cerrando RandomAccessFile (EI_Exportar) 06/05/2008  ..fin

				     }
			       else
				      {
	 			      do{
	 				    arcLinea=ArcEnt.leeLinea();
	 				    if (!arcLinea.equals("ERROR") && !ArcTramas.escribeLinea(arcLinea+"\n"))
						 		EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Algo salio mal escribiendo en el archivo de tramas: "+arcLinea, EIGlobal.NivelLog.DEBUG);
					    if ( ArcEnt.eof() )
						   noError = false;
				        }while(noError);
				      ArcEnt.cierraArchivo();
				      noError=true;
				      }
				   desconectando();
	  		       }
	  		    ArcTramas.cierraArchivo();
	       	    }

			if( ! mx.altec.enlace.utilerias.GzipArchivo.zipFile( IEnlace.DOWNLOAD_PATH+archivoTramas, IEnlace.DOWNLOAD_PATH + usuario+ "_" + folioArchivo + ".tram" ) )
				EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Problema: No se reallizo el gzip del archivo " + IEnlace.DOWNLOAD_PATH+archivoTramas, EIGlobal.NivelLog.DEBUG);
			else EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Se realizo el gzip: " + IEnlace.DOWNLOAD_PATH + usuario+ "_"+folioArchivo + ".tram.gz", EIGlobal.NivelLog.DEBUG);
			ArcTramas.borraArchivo();

		// EVERIS 28 Mayo 2007 Modificacion del archivo de salida.
        if(ArcEnt.abreArchivo())
		  {
		   arcLinea=ArcEnt.leeLinea();
		   if(ArcSal.creaArchivo())
			 salidaArc=true;

		   // EVERIS 28 Mayo 2007 Creacion del archivo con tramas de bitacora
		   archivoBitacora = usuario + "_"+folioArchivo+"TIB.bita";
	       ArcBit = new EI_Exportar(IEnlace.DOWNLOAD_PATH+archivoBitacora);
	       if (ArcBit.creaArchivo() ) archivoBitCreado = true;

		   if(!(inicializando()== 1))
			{
			  arcLinea="ERROR    000000";
			  EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): No pudo inicializar la conexion.", EIGlobal.NivelLog.INFO);
			}
		   else
		    {
		      do
				{
				  arcLinea=ArcEnt.leeLinea();
				  if(!"ERROR".equals(arcLinea))
				   {
					 totalRegistros++;
					 totalLineas++;

					 EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): Trama entrada por arcLinea: "+arcLinea, EIGlobal.NivelLog.DEBUG);

					 if(totalLineas%25==0)
					   EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): Linea no."+Integer.toString(totalLineas), EIGlobal.NivelLog.DEBUG);

					  objTrama.iniciaObjeto(arcLinea+"@");
					  EIGlobal.mensajePorTrace("<------Error con archivo de carga------>", EIGlobal.NivelLog.INFO);
					  arcLineaSalida=new StringBuffer("");
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][6],16)); //cta cargo
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][9],20)); //cta abono
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][10],5)); //benef
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][11],40)); //benef
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][12],4)); //suc
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][13],15)); //importe
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][14],5)); //plaza
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][16],120)); //concepto
					  arcLineaSalida.append(formateaCampo(objTrama.camposTabla[0][17],7)); //referencia interbancaria MSD 11/2005 Proyecto Ley de Transparencia
					  arcLineaSalida.append(conv.cliente_7To8(objTrama.camposTabla[0][4]));
					  // EVERIS 28 Mayo 2007 Linea agregada para que genere un renglon vacio al final del archivo
					  if (numRegistrosOk != totalRegistros ){
					  	arcLineaSalida.append("\n");
					  }
					  // EVERIS 28 Mayo 2007 Se genera linea para archivo de bitacora
					  if (archivoBitCreado )
					  	{
					  //BIT CU2061,BIT CU2101 Envia transaccin en archivo de exportacin(en lnea)
					  /**
					   * VSWF-***-I
					   * 15/Enero/2007	Modificacin para el modulo de Tesorera
					   */
					  if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
						try {
							BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
							BitaTransacBean bt = new BitaTransacBean();
							/*HGG-I*/
							if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ET_INTERBANCARIAS)){
								BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_REALIZA_OPER_TRANSFERENCIA);
							}else if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
								BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_AGREGAR_REGISTRO_BIT);
							}
							  /*HGG-F*/


	//					  	BIT CU3131 Envia operacin

						    if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
								  BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
								  bt = (BitaTransacBean) bh.llenarBean(bt);
							}
							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
							}
							if(referencia != null  && !referencia.isEmpty()){
								try {
									bt.setReferencia(Long.parseLong(referencia));
								}catch(NumberFormatException e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									bt.setReferencia(0);
									EIGlobal.mensajePorTrace("<-------Valor Referencia--------->."+referencia, EIGlobal.NivelLog.DEBUG);
								}
							}
							if (nombreSalida != null) {
								bt.setNombreArchivo(nombreSalida);
							}
							if (objTrama.camposTabla[0][6] != null) {
								bt.setCctaOrig(objTrama.camposTabla[0][6]);
							}
							if (objTrama.camposTabla[0][9] != null) {
								bt.setCctaDest(objTrama.camposTabla[0][9]);
							}
							if(objTrama.camposTabla[0][13] != null){
								bt.setImporte(Double.parseDouble(objTrama.camposTabla[0][13]));
							}
							bt.setServTransTux(DIBT);
							/*VSWF I Autor=BMB fecha=07-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario*/
							if(objTrama.camposTabla[0][11]!=null){
								bt.setBeneficiario(objTrama.camposTabla[0][11]);
							}
							/*VSWF F*/
							// EVERIS 28 Mayo 2007 Se coloca cambo de error como vacio
							bt.setIdErr(" ");
							/*SPID 2016 Vector. Bitacora*/
							bt.setTipoMoneda(tipoDivisa);
							if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
								&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
										.equals(BitaConstants.VALIDA)) {
								bt.setIdToken(session.getToken().getSerialNumber());
								}

							// EVERIS 28 Mayo 2007 Inicio Creando la trama para bitacora
							String arregloDatos[] = {String.valueOf(bt.getFolioBit()),String.valueOf(bt.getFolioFlujo()),bt.getIdFlujo(),bt.getNumBit()
								, String.valueOf( (bt.getFecha()==null)? bt.getFecha() : new java.sql.Date( bt.getFecha().getTime() ) )
								, String.valueOf( (bt.getFechaProgramada()==null)? bt.getFechaProgramada() :new java.sql.Date( bt.getFechaProgramada().getTime() ) )
								, bt.getTipoMoneda() , String.valueOf(bt.getNumTit()) , String.valueOf(bt.getReferencia()) , String.valueOf(bt.getImporte() ), String.valueOf(bt.getTipoCambio())
								, bt.getIdErr(), bt.getHora() , bt.getDirIp() , bt.getCodCliente() , bt.getIdToken() , bt.getBancoDest() , bt.getServTransTux()
								, bt.getCanal() , bt.getUsr() , bt.getContrato() , bt.getIdWeb() , bt.getIdSesion()
								, ( bt.getCctaOrig() ).substring(0, 11) , ( bt.getCctaDest() ).substring(0, 11)
								, bt.getNombreHostWeb()
								, String.valueOf( (bt.getFechaAplicacion()==null)? bt.getFechaAplicacion() : new java.sql.Date( bt.getFechaAplicacion().getTime() ) )
								, bt.getEstatus() , bt.getNombreArchivo() };

							tramaBitacora = "";
							for ( int i = 0; i < arregloDatos.length ; i++ )
								{
								if ( arregloDatos[i] == null || arregloDatos[i] == "" || arregloDatos[i].equals("null") )
									arregloDatos[i] = " ";
								if ( i == arregloDatos.length-1 )
									tramaBitacora += arregloDatos[i];
								else tramaBitacora += arregloDatos[i] + "|";
								}
							// EVERIS 28 Mayo 2007 FIN Creando la trama para bitacora
							}
							catch(Exception e){
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
					  	}
                	 /**
                 	  * VSWF-***-F
                 	  */

					  if(salidaArc && !ArcSal.escribeLinea(arcLineaSalida.toString()))
						  EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): Algo salio mal escribiendo: "+arcLineaSalida.toString(), EIGlobal.NivelLog.INFO);

					  // EVERIS 28 Mayo 2007 Escribe en archivo con tramas para bitacora
					  if(!ArcBit.escribeLinea(tramaBitacora+"\n"))
						 		EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Algo salio mal escribiendo en el archivo de la bitacora: "+tramaBitacora, EIGlobal.NivelLog.INFO);

					  }
					  // EVERIS 28 Mayo 2007 FIN Se genera linea para archivo de bitacora
			   		}
			  		else
			   			noError=false;
			}while(noError);
			//VSWF-HGG  -  borra bandera
			req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
		   }

		   ArcEnt.cierraArchivo();
		   desconectando();
		   ArcSal.cierraArchivo();

		   // EVERIS 28 Mayo 2007 Cierre y compresion del archivo con tramas para bitacora y de salida
		   ArcBit.cierraArchivo();
		   if( ! mx.altec.enlace.utilerias.GzipArchivo.zipFile( IEnlace.DOWNLOAD_PATH+archivoBitacora,IEnlace.DOWNLOAD_PATH + usuario + "_"+folioArchivo + ".bita" ) )
		   	EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Problema: No se reallizo el gzip del archivo "+IEnlace.DOWNLOAD_PATH+archivoBitacora, EIGlobal.NivelLog.DEBUG);
		   else EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Se realizo el gzip: "+ IEnlace.DOWNLOAD_PATH + usuario + "_"+folioArchivo + ".bita.gz", EIGlobal.NivelLog.DEBUG);
			ArcBit.borraArchivo();

		   if( ! mx.altec.enlace.utilerias.GzipArchivo.zipFile( IEnlace.DOWNLOAD_PATH+nombreSalida,IEnlace.DOWNLOAD_PATH + nombreSalida + "_R" + folioArchivo ) )
		      EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Problema: No se reallizo el gzip del archivo "+IEnlace.DOWNLOAD_PATH+nombreSalida , EIGlobal.NivelLog.DEBUG);
		   else EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Se realizo el gzip: "+ IEnlace.DOWNLOAD_PATH + nombreSalida + "_R" + folioArchivo+".gz", EIGlobal.NivelLog.DEBUG);
		   // EVERIS 28 Mayo 2007 FIN Cierre y compresion del archivo con tramas para bitacora y de salida

	   if("ERROR".equals(arcLinea.substring(0,5).trim()) && totalLineas<1)
	     {
	       comuError=true;
	       mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
	     }
	 }
	else
	 {
	   comuError=true;
	   mensajeError="Problemas de comunicacion. Por favor intente mas tarde.";
	 }

	 // EVERIS 28 Mayo 2007 Realizando RCP de los archivos gzip
	 ArchivoRemoto archRemoto = new ArchivoRemoto();
	 String gzipTrama= usuario+"_"+ folioArchivo + ".tram.gz";
	 String gzipBita= usuario + "_" +folioArchivo + ".bita.gz";
	 String gzipResu = nombreSalida + "_R" + folioArchivo + ".gz";
	 boolean rcpRealizado = false;

	 if ( ( new File(IEnlace.DOWNLOAD_PATH + gzipTrama)).exists() &&
	 	  ( new File(IEnlace.DOWNLOAD_PATH + gzipBita) ).exists() &&
	 	  ( new File(IEnlace.DOWNLOAD_PATH + gzipResu) ).exists() 	)
	 	{

	 	if(archRemoto.copiaLocalARemoto(gzipTrama))
	 		{
			EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() El archivo "+gzipTrama+" se copio correctamente", EIGlobal.NivelLog.DEBUG);
			if(archRemoto.copiaLocalARemoto(gzipBita))
				{
				EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() El archivo "+gzipBita+" se copio correctamente", EIGlobal.NivelLog.DEBUG);
				if(archRemoto.copiaLocalARemoto( gzipResu )){
					rcpRealizado = true;
					EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() El archivo "+gzipResu+" se copio correctamente", EIGlobal.NivelLog.DEBUG);
					}
				else
					EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Problema al copiar el archivo " +gzipResu, EIGlobal.NivelLog.INFO);
				}
			else
				EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Problema al copiar el archivo " +gzipBita, EIGlobal.NivelLog.INFO);
	 		}
	 	else
			EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Problema al copiar el archivo " +gzipTrama, EIGlobal.NivelLog.INFO);
		}
	else
		{
		EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() No se encontraron archivos para copiar por rcp.", EIGlobal.NivelLog.INFO);
		}


	if (!rcpRealizado)
		{
	 	EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() No se realizo copia por rcp.", EIGlobal.NivelLog.INFO);
	 	comuError=true;
	 	mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde. Error en la copia de archivo. ";
	 	errorEnvio=true;
	 	}
	else{
		EIGlobal.mensajePorTrace("MDI_Enviar - Fecha: "+ getFormParameter(req,"Fecha"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MDI_Enviar - fecha_completaReg: "+ getFormParameter(req,"fecha_completaReg"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MDI_Enviar - strArc: "+ getFormParameter(req,"strArc"), EIGlobal.NivelLog.DEBUG);
		if (!getFormParameter(req,"Fecha").equals(getFormParameter(req,"fecha_completaReg")) ) {

			//INICIO PYME 2015 INDRA operaciones programadas Registradas / No Registradas
			String inTransReg="";


			String outTransRegSProg="";
			List<OperacionesProgramadasBean> listaOperacionesProgramadasRegistradas = new ArrayList<OperacionesProgramadasBean>();


				EIGlobal.mensajePorTrace("Valor getFormParameter(req, strArc) antes de encoding: " + getFormParameter(req,"strArc"), EIGlobal.NivelLog.DEBUG);
				inTransReg = new String(getFormParameter(req,"strArc").getBytes("ISO-8859-1"), "UTF-8");
				EIGlobal.mensajePorTrace("Valor inTransReg despues de encoding: " + inTransReg, EIGlobal.NivelLog.DEBUG);

				OperacionesProgramadasBO operacionesProgramadasBO = new OperacionesProgramadasBO();
				if (inTransReg.length()>0){

					outTransRegSProg = sepTrnsPrg("S", inTransReg, "@", 10, 15);
					if (outTransRegSProg.length()>0){
						respuestaProgramadoResultado[1] = 1;
						try {
							if(!operacionesProgramadasBO.validaContratoMancomunado(contrato)){
								listaOperacionesProgramadasRegistradas = obtieneValorasOpeProgramadas(outTransRegSProg, contrato, usuario, clavePerfil, req);
								int tamanioArreglo=listaOperacionesProgramadasRegistradas.size();
								EIGlobal.mensajePorTrace("tamanioArreglo :"+tamanioArreglo, EIGlobal.NivelLog.ERROR);
								int x=0;
								for (OperacionesProgramadasBean operacionesProgramadasBean : listaOperacionesProgramadasRegistradas) {
									operacionesProgramadasBO.altaOperacionesInterban(operacionesProgramadasBean,req,x);
									x++;
								}
							}
						} catch (BusinessException e) {
							EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR); // ARS Indra Validar el tipo de excepcion el eplicar Sonnar
						}
					}
				}



			//FIN PYME 2015 INDRA operaciones programadas

    	// Operaciones Programadas
    	 // Indra PYME 2015

		} else {
		// EVERIS 28 Mayo 2007 Llamado a Servicio Tuxedo controlador de registros
		String tramaParaEnviar = BaseServlet.convierteUsr8a7(usuario) + "|" + clavePerfil + "|" + contrato + "|" + folioArchivo + "|DIBT|" + IEnlace.REMOTO_TMP_DIR + "/"+ gzipTrama + "|" +
			IEnlace.REMOTO_TMP_DIR+"/"+ gzipBita +"|" + IEnlace.REMOTO_TMP_DIR + "/" + gzipResu;
		try{
			Hashtable hs = TuxGlobal.envia_datos(tramaParaEnviar);
			Result= String.valueOf(hs.get("BUFFER"));
			}
		catch( java.rmi.RemoteException re ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

		if(Result==null || "null".equals(Result) )
			Result="ERROR 	   Error en el servicio.";
		EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() CONTROLADOR DE REGISTROS Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
		codError=Result.substring(0,Result.indexOf("|"));

		if(!"TRSV0000".equals(codError.trim()))
			{
			mensajeErrorEnviar = Result.substring(Result.indexOf("|")+1,Result.length() );
			EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() No se enviaron los registros al controlador de registros Mensaje: " +mensajeErrorEnviar, EIGlobal.NivelLog.INFO);
			comuError=true;
			errorEnvio=true;
			mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde. Error en el envio de registros. ";
			}
		else
			{
			mensajeErrorEnviar = Result.substring(Result.indexOf("|")+1,Result.length() );
			if ( "OK".equals(mensajeErrorEnviar) )
				{
				// EVERIS 28 Mayo 2007 Llamado a Servicio Tuxedo dispersor de transacciones
				String tramaParaDispersor = BaseServlet.convierteUsr8a7(usuario) + "|" + clavePerfil + "|" + contrato + "|" + folioArchivo + "|DIBT|";
				try{
					//Envio de transferencia a Monitor Plus.
					EIGlobal.mensajePorTrace("-------->Se envian valores TIBArchivo para mapeo a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
					TransferenciasMonitorPlusUtils.enviarTIBArchivo(req, objTrama.camposTabla, folioArchivo, session, IEnlace.SUCURSAL_OPERANTE, contrato, codError.trim());
				
				}catch (Exception e) {
					EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
				
				try{
					Hashtable hs = TuxGlobal.inicia_proceso(tramaParaDispersor);
					Result= (String) hs.get("BUFFER") +"";
					}
				catch( java.rmi.RemoteException re ){
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				}
				catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

				if(Result==null || "null".equals(Result) ){
					Result="ERROR 	   Error en el servicio.";
				}
				EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() DISPERSOR DE REGISTROS  : Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
				codError=Result.substring(0,Result.indexOf("|"));

				cod_error_string = codError;

				if(!"TRSV0000".equals(codError.trim()))
					{
					mensajeErrorDispersor = Result.substring(Result.indexOf("|")+1,Result.length() );
					EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() No se realizo la dispersion de registros Mensaje: " +mensajeErrorDispersor, EIGlobal.NivelLog.DEBUG);
					comuError=true;
					errorEnvio=true;
					mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde. \n Error en la dispersion de registros. ";
					}
				else
					{
					mensajeErrorDispersor = Result.substring(Result.indexOf("|")+1,Result.length() );
					EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.DEBUG);
					}
				}// EVERIS 28 Mayo 2007 FIN Llamado a Servicio Tuxedo dispersor de transacciones
			}
		}
		}// EVERIS 28 Mayo 2007 FIN Llamado a Servicio Tuxedo controlador de registros
	 }// EVERIS 28 Mayo 2007 FIN ELSE condicional de folio correcto
	 }// EVERIS 28 Mayo 2007 FIN ELSE condicional de recepcion de folio

	 } // Operaciones en Linea
	else
     if(Todas.totalRegistros>=1)
      {
    	 // Operaciones Programadas
    	 // Indra PYME 2015

    	//INICIO PYME 2015 INDRA operaciones programadas Registradas / No Registradas
    	 	HttpSession sessionBit = req.getSession(false);
    	 	Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
			String inTransReg="";
			String inTransNoReg="";

			String outTransRegSProg="";
			List<OperacionesProgramadasBean> listaOperacionesProgramadasRegistradas = new ArrayList<OperacionesProgramadasBean>();
			List<OperacionesProgramadasBean> listaOperacionesProgramadasNoRegistradas = new ArrayList<OperacionesProgramadasBean>();

			String outTransNoRegSProg = "";
			EIGlobal.mensajePorTrace("MDI_Enviar RRR_ARS: Antes Programada", EIGlobal.NivelLog.ERROR);
				inTransReg = tmpParametros.get("TransReg") == null ? "" : String.valueOf( tmpParametros.get("TransReg") );
				inTransNoReg = tmpParametros.get("TransNoReg") == null ? "" : String.valueOf( tmpParametros.get("TransNoReg") ) ;
				EIGlobal.mensajePorTrace("MDI_Enviar RRR_ARS: Despues Programada", EIGlobal.NivelLog.ERROR);
				String strCheck = (String) req.getSession().getAttribute("strCheck");

				EIGlobal.mensajePorTrace("inTransReg["+inTransReg+"]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("inTransNoReg["+inTransNoReg+"]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("strCheck["+strCheck+"]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("strCheck["+strCheck+"]", EIGlobal.NivelLog.ERROR);

				String Reg_and_NoRegDos = "";
				Reg_and_NoRegDos +=((String)req.getSession().getAttribute("Registradas"));
				Reg_and_NoRegDos+=((String)req.getSession().getAttribute("NoRegistradas"));
				EIGlobal.mensajePorTrace("Reg_and_NoRegDos["+Reg_and_NoRegDos+"]", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Todas.strOriginal["+Todas.strOriginal+"]", EIGlobal.NivelLog.ERROR);
				boolean esMancomunada = false;
				try{
					outTransNoRegSProg = "";
					OperacionesProgramadasBO operacionesProgramadasBO = new OperacionesProgramadasBO();
					int x=0;
					String[] resCadenas = new String[2];
					if (inTransReg.length()>0){
						resCadenas = sepTrnsPrg( inTransReg, strCheck );
						outTransRegSProg = resCadenas[0];
						strCheck = resCadenas[1];
						if (outTransRegSProg.length()>0){
							respuestaProgramadoResultado[1] = 1;
							try {
								if(!operacionesProgramadasBO.validaContratoMancomunado(contrato)){
									listaOperacionesProgramadasRegistradas = obtieneValorasOpeProgramadas(outTransRegSProg, contrato, usuario, clavePerfil, req);
									int tamanioArreglo=listaOperacionesProgramadasRegistradas.size();
									EIGlobal.mensajePorTrace("tamanioArreglo :"+tamanioArreglo, EIGlobal.NivelLog.ERROR);
									for (OperacionesProgramadasBean operacionesProgramadasBean : listaOperacionesProgramadasRegistradas) {
										operacionesProgramadasBO.altaOperacionesInterban(operacionesProgramadasBean,req,x);
										EIGlobal.mensajePorTrace("RegProg Se genero referencia con x["+x+"]", EIGlobal.NivelLog.ERROR);
										x++;
									}
								}
							} catch (BusinessException e) {
								EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR); // ARS Indra Validar el tipo de excepcion el eplicar Sonnar
							}
						}
					}
					if (inTransNoReg.length()>0)
					{
						resCadenas = sepTrnsPrg( inTransNoReg, strCheck );
						outTransNoRegSProg = resCadenas[0];
						if (outTransNoRegSProg.length()>0){
							respuestaProgramadoResultado[1] = 1;
							try {
								if(!operacionesProgramadasBO.validaContratoMancomunado(contrato)){
									listaOperacionesProgramadasNoRegistradas = obtieneValorasOpeProgramadas(outTransNoRegSProg, contrato, usuario, clavePerfil, req);
									int tamanioArreglo=listaOperacionesProgramadasRegistradas.size();
									EIGlobal.mensajePorTrace("tamanioArreglo :"+tamanioArreglo, EIGlobal.NivelLog.ERROR);
									for (OperacionesProgramadasBean operacionesProgramadasBean : listaOperacionesProgramadasNoRegistradas) {
										operacionesProgramadasBO.altaOperacionesInterban(operacionesProgramadasBean,req,x);
										EIGlobal.mensajePorTrace("NoRegProg Se genero referencia con x["+x+"]", EIGlobal.NivelLog.ERROR);
										x++;
									}
								}
							} catch (BusinessException e) {
								EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR); // ARS Indra Validar el tipo de excepcion el eplicar Sonnar
							}
						}
					}
				}catch(BusinessException e){
					EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
					esMancomunada = true;
				}
			//FIN PYME 2015 INDRA operaciones programadas

    	// Operaciones Programadas
    	 // Indra PYME 2015

    	try{
	    //llamado_servicioCtasInteg(IEnlace.MDep_inter_cargo," "," "," ",session.getUserID()+".ambci",req);
		String datoscta[]=null;

		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Entrando a modificacion" , EIGlobal.NivelLog.DEBUG);

		String No_Registradas = "";
		String Registradas    = "";
		String Reg_and_NoReg	="";
		String Reg_Enviar="";
		String cuenta1="", cuenta_1="", cuenta2="", cuenta_2="";
		String idModulo = "";

		//boolean cuentasiguales = true;

		if(req.getSession().getAttribute("NoRegistradas")==null){
		  No_Registradas="";
		} else {
		  No_Registradas=(String)req.getSession().getAttribute("NoRegistradas"); }
		EIGlobal.mensajePorTrace("MTI_Enviar - execute(): No_Registradas "+ No_Registradas , EIGlobal.NivelLog.DEBUG);

		if(req.getSession().getAttribute("Registradas")==null){
		  Registradas="";
		} else {
		  Registradas=(String)req.getSession().getAttribute("Registradas"); }
		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Registradas "+ Registradas , EIGlobal.NivelLog.DEBUG);


//		Reg_and_NoReg+=((String)req.getSession().getAttribute("Registradas"));
//		Reg_and_NoReg+=((String)req.getSession().getAttribute("NoRegistradas"));
		//Incidente ?
		EIGlobal.mensajePorTrace("MDI_Enviar - RRR_ARS() Antes de Asignar", EIGlobal.NivelLog.INFO);
		Reg_and_NoReg+= tmpParametros.get("TransReg");
		EIGlobal.mensajePorTrace("MDI_Enviar - RRR_ARS(): ["+Reg_and_NoReg+"]", EIGlobal.NivelLog.INFO);
		Reg_and_NoReg+= tmpParametros.get("TransNoReg");
		EIGlobal.mensajePorTrace("MDI_Enviar - RRR_ARS(): ["+Reg_and_NoReg+"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MDI_Enviar - RRR_ARS() Despues de Asignar", EIGlobal.NivelLog.INFO);



		Reg_Enviar = Todas.strOriginal;

		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Registradas	 "+ Registradas , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): No_Registradas "+ No_Registradas , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_and_NoReg  "+ Reg_and_NoReg , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar     "+ Reg_Enviar , EIGlobal.NivelLog.INFO);

		EI_Tipo misCuentas=new EI_Tipo();  //Reg_and_NoReg
		EI_Tipo lasCuentas=new EI_Tipo();  //Reg_Enviar
		strCheck = (String) req.getSession().getAttribute("strCheck");
		misCuentas.iniciaObjeto(new String(Reg_and_NoReg.toString().getBytes("ISO-8859-1"), "UTF-8"));
		lasCuentas.iniciaObjeto(new String(Reg_Enviar.toString().getBytes("ISO-8859-1"), "UTF-8"));
//		if( !esMancomunada ){
//			EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Es mancomunado no ejecuta obtenerFechaProgramada", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Antes obtenerFechaProgramada", EIGlobal.NivelLog.INFO);
		Reg_Enviar = obtenerFechaProgramada(Reg_and_NoReg, strCheck);
		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Despues obtenerFechaProgramada", EIGlobal.NivelLog.INFO);
//		}
		EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar Prog"+ Reg_Enviar , EIGlobal.NivelLog.INFO);
		// Iniciacizar objetos
		//Para programadas
		Todas.iniciaObjeto(new String(Reg_Enviar.toString().getBytes("ISO-8859-1"), "UTF-8"));


		int ajuste = 0;
		valCtas = new ValidaCuentaContrato();
		// INDRA PYME EXPORTAR ARCHIVO
		List<ExportTransferenciasBean> listaArchivoExportar =
			new ArrayList<ExportTransferenciasBean>();

		// FIN INDRA PYME EXPORTAR ARCHIVO
		int contProg = 0; //Contador para programadas Indra 2015
		for(int i=0;i<Todas.totalRegistros;i++)
		 {
		  cuentaCargo	    = Todas.camposTabla[i][0];
 		  EIGlobal.mensajePorTrace("MDI_Enviar - cuentaCargo->"+cuentaCargo , EIGlobal.NivelLog.INFO);
		   if(req.getAttribute("cuentaCargo")!=null){
			 req.removeAttribute("cuentaCargo"); }
		   req.setAttribute("cuentaCargo",cuentaCargo);
		   if(ses.getAttribute("cuentaCargo")!=null){
			 ses.removeAttribute("cuentaCargo"); }
		   ses.setAttribute("cuentaCargo",cuentaCargo);
		   titular	    = Todas.camposTabla[i][1];
		   if(req.getAttribute(TITULAR)!=null){
			 req.removeAttribute(TITULAR); }
		   req.setAttribute(TITULAR,titular);
		   if(ses.getAttribute(TITULAR)!=null){
			 ses.removeAttribute(TITULAR); }
		   ses.setAttribute(TITULAR,titular);

		   cuentaAbono	    = Todas.camposTabla[i][2];
		   req.setAttribute("cuentaAbono",cuentaAbono);
		   beneficiario     = Todas.camposTabla[i][3];
		   importe	        = Todas.camposTabla[i][4];
		   concepto	        = Todas.camposTabla[i][5];
		   req.setAttribute("concepto",concepto);
		   referencia       = Todas.camposTabla[i][6];
		   banco	        = Todas.camposTabla[i][7];
		   plaza	        = Todas.camposTabla[i][8];
		   sucursal	        = Todas.camposTabla[i][9];
		   formaAplica		= Todas.camposTabla[i][13];
		   /* VECTOR 06-2016: SPID */	
		   tipoDivisa = Todas.camposTabla[i][16];
		   rfcTransac = Todas.camposTabla[i][17];
		   spidTipoOperacion = Todas.camposTabla[i][18];
		   
		   EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): SPID ["+Todas.camposTabla[i][16]+"]", EIGlobal.NivelLog.INFO);
		   EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): SPID CVE OPER ["+Todas.camposTabla[i][18]+"]", EIGlobal.NivelLog.INFO);
		   
		   if (Todas.camposTabla[i][15] == null || "".equals(Todas.camposTabla[i][15])) {
			   Todas.camposTabla[i][15] = Todas.camposTabla[i][10];
		   }
		   if (!cuentaCargo.equals("SIN CUENTA"))
			 {
			   //datoscta=BuscandoCtaInteg(cuentaCargo.trim(),IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambci",IEnlace.MDep_inter_cargo);
			   
			   if("USD".equals(tipoDivisa)){
				   idModulo = IEnlace.MCargo_transf_dolar;
			   }else{
				   idModulo = IEnlace.MDep_inter_cargo;
			   }

				datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
						  								  session.getUserID8(),
												          session.getUserProfile(),
												          cuentaCargo.trim(),
												          idModulo);
			   if (datoscta!=null){
				  tipoRelacion =datoscta[2];
			   } else {
				  tipoRelacion="T"; }
			 }
		    else {
			  tipoRelacion ="P"; }

		      /* Agregar RFC, e Importe IVA */
			concepto=formateaCampo(concepto,40);
			if(! Todas.camposTabla[i][11].trim().equals("") ) 	//MSD 11/2005 Proyecto Ley de Transparencia
			 {
			   concepto+="RFC "+Todas.camposTabla[i][11]; 		//MSD 11/2005 Proyecto Ley de Transparencia
			   String campoIVA = Todas.camposTabla[i][12].length() > 6 ? Todas.camposTabla[i][12].substring(0, 6) : Todas.camposTabla[i][12];
			   concepto+=" IVA "+campoIVA; 		//MSD 11/2005 Proyecto Ley de Transparencia
			  }

//			Inicio de validacion de cuentas cargo y abono dentro del entorno
//			Se genera trama para transferencia si la ctas. cargo y abono son validas. Si se tienen facultades para transferir a ctas. no registradas no se valida la cta abono.
			if(("SIN CUENTA".equals(cuentaCargo) || datoscta != null) &&
					(session.getFacultad(session.FAC_TRANSF_NOREG) || valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
																									 session.getUserID(),
																									 session.getUserProfile(),
																									 cuentaAbono.trim(),
																									 IEnlace.MDep_Inter_Abono) != null)) {
			/* VECTOR 06-2016: SPID Se agregan 2 nuevos parametros para mandar llamar la trama Divisa y RFC */			
			Trama=generaTrama(tipoRelacion,concepto, referencia,formaAplica, cuentaAbono,banco,beneficiario,importe, plaza, sucursal,req, clavePerfil,usuario,contrato, true, tipoDivisa, rfcTransac, spidTipoOperacion); //MSD 11/2005 Proyecto Ley de Transparencia
			} else {
				Trama = null;
			}
//			Inicio de validacion de cuentas cargo y abono dentro del entorno

			String IP = " ";
			String fechaHr = "";												//variables locales al mtodo
			IP = req.getRemoteAddr();											//ObtenerIP
			java.util.Date fechaHrAct = new java.util.Date();
			SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
			fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignacin de fecha y hora

			EIGlobal.mensajePorTrace(fechaHr+"MDI_Enviar - iniciaEnviar(): Trama entrada por generaTrama: "+Trama, EIGlobal.NivelLog.DEBUG);
			// INDRA PYME 2015 no enviar programadas
			EIGlobal.mensajePorTrace(fechaHr+"MDI_Enviar - iniciaEnviar(): Fecha: "+Todas.camposTabla[i][10], EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(fechaHr+"MDI_Enviar - iniciaEnviar(): Fecha Programada: "+Todas.camposTabla[i][15], EIGlobal.NivelLog.DEBUG);
			if (Todas.camposTabla[i][10].equals(Todas.camposTabla[i][15])) {
			try
			  {
//				 Fin de validacion de cuentas cargo y abono dentro del entorno
				 if(Trama != null) {
				 Hashtable hs = TuxGlobal.web_red(Trama);
				 Result= String.valueOf(hs.get("BUFFER"));
 //
				cod_error.append(hs.get("COD_ERROR")).append("-");
				 } else {
					 Result = null;
				 }

//				 Fin de validacion de cuentas cargo y abono dentro del entorno
			  }catch( java.rmi.RemoteException re )
				{
				  EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				}catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				try{
					 /* VECTOR 06-2016: SPID */	
				EIGlobal.mensajePorTrace("MDI_Enviar - TRANSFERENCIAS EN LINEA SPEI <"+Result+">", EIGlobal.NivelLog.DEBUG);
				if(Result!=null  && Result.length()>8 && "OK".equals(Result.substring(0,2)) && !Result.substring(Result.length()-4,Result.length()).equals("MANC")){
					EIGlobal.mensajePorTrace("\n TRANSFERENCIAS INTERBANCARIAS EN LINEA SPEI COMPLEMENTO", EIGlobal.NivelLog.DEBUG);
						transInterbBUS.registraTransferencia(Trama, Result);
				} 
				
//				else if (Result.startsWith("TRIB0000")){
//					EIGlobal.mensajePorTrace("\n TRANSFERENCIAS INTERBANCARIAS EN LINEA USD", EIGlobal.NivelLog.INFO);
//					transInterbBUS.registraTransferencia(Trama, Result);
//				}
				if(Result!=null && "OK".equals(Result.substring(0,2)) || "MANC".equals(Result.substring(Result.length()-4,Result.length()))){
					
					//Envio transferencias a monitor plus 
					String numRefe = Result.substring(9, 16);
					EIGlobal.mensajePorTrace("-------->Se envian valores TIBIndividual para mapeo a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
					TransferenciasMonitorPlusUtils.enviarTIBIndividual(req, Todas.camposTabla, i, session, IEnlace.SUCURSAL_OPERANTE, cod_error.toString(), numRefe);
				 }
				}catch (Exception e) {
					EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
			} else {
				referencia = "0";

			}
				//YHG AQUI PODRIA HACER EL INSERT DE LA TRANSFERENCIA
			//BIT CU2061,BIT CU2101 Envia transaccin de archivo
			/**
			 * VSWF
			 * 15/Enero/2007	Modificacin para el modulo de Tesorera
			 */
			if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			try{
				BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaTransacBean bt = new BitaTransacBean();
				if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ET_INTERBANCARIAS)){
					BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_REALIZA_OPER_TRANSFERENCIA);
				}else if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
					BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_AGREGAR_REGISTRO_BIT);
				}


				//BIT CU3131 Envia operacin (se genera archivo de exportacin)

					if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
						BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
						bt = (BitaTransacBean) bh.llenarBean(bt);

						bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DIBT);
						bt.setNombreArchivo(nombreSalida);
				}
				if (nombreSalida != null) {
					bt.setNombreArchivo(nombreSalida.trim());
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if(referencia != null){
					try {
						bt.setReferencia(Long.parseLong(referencia));
					}catch(NumberFormatException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						bt.setReferencia(0);
					}
				}
				if (cuentaCargo != null) {
					bt.setCctaOrig(cuentaCargo.trim());
				}
				if (cuentaAbono != null) {
					bt.setCctaDest(cuentaAbono.trim());
				}
				if (banco != null) {
					bt.setBancoDest(banco.trim());

				}
				if(importe != null){
					bt.setImporte(Double.parseDouble(importe));
				}
				if (usuario != null) {
					bt.setUsr(BaseServlet.convierteUsr8a7(usuario.trim()));
				}
				bt.setServTransTux(DIBT);
				/*BMB-I*/
				if(Result!=null){
	    			if(Result.length() > 2 && "OK".equals(Result.substring(0,2))){
	    				bt.setIdErr("DIBT0000");
	    			}else if(Result.length()>8){
		    			bt.setIdErr(Result.substring(0,8));
		    		}else{
		    			bt.setIdErr(Result.trim());
		    		}
	    		}else{
					bt.setIdErr("");
	    		}
				/*BMB-F*/
				bt.setTipoMoneda(tipoDivisa);
				if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
					&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
							.equals(BitaConstants.VALIDA)) {
					bt.setIdToken(session.getToken().getSerialNumber());
				}
				/*VSWF I Autor=BMB fecha=07-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario*/
				if(beneficiario!=null && beneficiario!=""){
					bt.setBeneficiario(beneficiario.trim());
				}
				/*VSWF F*/
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			}
       	 /**
       	  * VSWF
       	  */

			if(Result==null || "".equals(Result) || Result.length() < 8) {
			  EIGlobal.mensajePorTrace("MDI_Enviar - Valor invalido de Result: "+Result, EIGlobal.NivelLog.INFO);
			  Result="ERROR           Error en el servicio.";
		    }

				if (!Todas.camposTabla[i][10].equals(Todas.camposTabla[i][15])) {
					Result="ERROR           Programada";
				}


			EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(2): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

			importeTotal+=new Double(Todas.camposTabla[i][4]).doubleValue();
			String colorbg   ="";
			String str	    ="";
			String alignTxt[]={"left","center","right"};
			
			/* VECTOR 06-2016: SPID Modificados los arreglos aling y c */
			double impTmp    =0;
			int	  align[]   ={0,0,1,1,2,0,1};
			int	  c[]	    ={0,2,3,16,4,5,6};
			int	  Error     =0;
			codError=Result.substring(0,8);
			
		    if(strCheck.charAt(i + ajuste) == '0'){
			   ajuste++;
		    }
		    cuenta1  = misCuentas.camposTabla[i + ajuste][0];
		    cuenta_1 = lasCuentas.camposTabla[i][0];
		    cuenta2  = misCuentas.camposTabla[i + ajuste][2];
		    cuenta_2 = lasCuentas.camposTabla[i][2];

		    EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar "+ cuenta1 , EIGlobal.NivelLog.INFO);
		    EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar "+ cuenta_1 , EIGlobal.NivelLog.INFO);
		    EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar "+ cuenta2 , EIGlobal.NivelLog.INFO);
		    EIGlobal.mensajePorTrace("MDI_Enviar - execute(): Reg_Enviar "+ cuenta_2 , EIGlobal.NivelLog.INFO);

		    if(cuenta1.equals(cuenta_1) && cuenta2.equals(cuenta_2) ){
			   EIGlobal.mensajePorTrace("contratos o.k.", EIGlobal.NivelLog.DEBUG);
		    } else
			 {
			   EIGlobal.mensajePorTrace("Se modificaron los contratos", EIGlobal.NivelLog.DEBUG);
			   mensajeError="Transaccion no Vlida";
			   comuError=true;
			 }

			EIGlobal.mensajePorTrace("Empezando a evaluar...", EIGlobal.NivelLog.DEBUG);
			if("OK".equals(codError.trim()))
			 {				
				EIGlobal.mensajePorTrace("codError Ok.", EIGlobal.NivelLog.DEBUG);
//GOR 20110113 (Req. Enlace - 2,5 - Inicio): en vez de ENVIADA debe mostrar EN PROCESO DE VALIDACIΤN
				TipoError="<font color=blue><b>ACEPTADA</b></font>";
				tipoErrorExportacion = "ACEPTADA";
//GOR 20110113 (Req. Enlace - 2,5 - Fin)

				if(Result.substring(Result.length()-4,Result.length()).equals("MANC"))
				 {
					Error=1;
					TipoError="<font color=green><b>MANCOMUNADA</b></font>";
					tipoErrorExportacion = "MANCOMUNADA";
					Referencia=Result.substring(8,Result.length()-4);
				 }
				else
				   Referencia = Result.substring(8,Result.indexOf("|"));
				cta_clabe  = Result.substring(Result.indexOf("|")+1, Result.length()); //  13/05/2004 Cambio SPEI-cta ordenante v1 jbg
						
			 }
		    else
			 {
			   Referencia=Result.substring(8,16);
				 if(Result.length()>16) {
					 TipoError="<font color=red><b>"+Result.substring(16,Result.length())+"</b></font>";
					 tipoErrorExportacion = Result.substring(16,Result.length());
				 }
				 else {
					 TipoError="<font color=red><b>NO ACEPTADA</b></font>";
					 tipoErrorExportacion = "NO ACEPTADA";
				 }
			   Error=1;
			 }
			
			/* VECTOR 06-2016: SPID */
//			if(Result.startsWith("TRIB0000")){
//				EIGlobal.mensajePorTrace("MDI_Enviar - Oper Dolares - Cod Error < "+ Result.substring(0, 8) +">" , EIGlobal.NivelLog.INFO);
//				EIGlobal.mensajePorTrace("MDI_Enviar - Oper Dolares - Referencia < "+ Result.substring(79, 86) +">" , EIGlobal.NivelLog.INFO);
//				TipoError="<font color=green><b>"+Result.substring(117, 137)+"</b></font>";
//				tipoErrorExportacion = Result.substring(117, 137).trim();
//				cod_error.append(Result.substring(0, 8).trim());
//				Referencia=Result.substring(79, 86).toString().trim();
//			}				

			referencias = referencias+Referencia+", ";

			nuevaTrama.append(Referencia);
			nuevaTrama.append(pipe);
			
					for (int j = 0; j < 10; j++) {
						nuevaTrama.append(Todas.camposTabla[i][j]).append(pipe);
					}
					
		   nuevaTrama.append(TipoError);
		   nuevaTrama.append(pipe);
		   nuevaTrama.append(EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
		   nuevaTrama.append(pipe);		   
		   nuevaTrama.append(Todas.camposTabla[i][11]);// RFC
		   nuevaTrama.append(pipe);	
		   nuevaTrama.append(Todas.camposTabla[i][12]);// IVA
		   nuevaTrama.append(pipe);	
		   EIGlobal.mensajePorTrace("Forma Desc Todas.camposTabla[i][14] "+Todas.camposTabla[i][14], EIGlobal.NivelLog.DEBUG);
		   nuevaTrama.append(Todas.camposTabla[i][14]); //forma aplica desc
		   nuevaTrama.append(pipe);	
		   EIGlobal.mensajePorTrace("Feha Aplica Todas.camposTabla[i][15] "+Todas.camposTabla[i][15], EIGlobal.NivelLog.DEBUG);
		   nuevaTrama.append(Todas.camposTabla[i][15]); // fecha aplica
		   nuevaTrama.append(pipe);	
		   EIGlobal.mensajePorTrace("Forma Aplica Todas.camposTabla[i][13] "+Todas.camposTabla[i][13], EIGlobal.NivelLog.DEBUG);
		   nuevaTrama.append(Todas.camposTabla[i][13]); //forma aplica
		   nuevaTrama.append(pipe);	
		   /* VECTOR 06-2016: SPID : Se obtine el tipo de divisa para mostrarse en la tabla de resultado */
		   nuevaTrama.append(Todas.camposTabla[i][16]);
		   nuevaTrama.append(pipe);	
		   EIGlobal.mensajePorTrace("Forma Aplica Todas.camposTabla[i][17] "+Todas.camposTabla[i][17], EIGlobal.NivelLog.DEBUG);
		   nuevaTrama.append("@");

		   EIGlobal.mensajePorTrace("Armando tramas.", EIGlobal.NivelLog.DEBUG);

		   if((i%2)==0){
			 colorbg="class='textabdatobs'";
		   } else {
			 colorbg="class='textabdatcla'"; }
		   strTabla.append("\n<tr>");
		// INDRA PYME EXPORTAR ARCHIVO Abril - 2015
		   ExportTransferenciasBean lineaExportar = new ExportTransferenciasBean();
		   // Referencia
		   lineaExportar.setCheck(Referencia); 
		   // Estatus
		   lineaExportar.setTipoOperacion(tipoErrorExportacion);

		// INDRA PYME EXPORTAR ARCHIVO Abril - 2015
		   if(Error==1) 
			 {
			   strTabla.append("<td ");
			   strTabla.append(colorbg);
			   strTabla.append("> &nbsp;");
			   if (Todas.camposTabla[i][10].equals(Todas.camposTabla[i][15])) {
				   strTabla.append(Referencia);
			   } else {
				   EIGlobal.mensajePorTrace("Valor contProg Programadas : "+contProg, EIGlobal.NivelLog.DEBUG);
				   int referenciaBit =(Integer)ses.getAttribute("referenciaBitProgramada"+contProg);
				   contProg++;//Se modifica el contador para progradas
				   EIGlobal.mensajePorTrace("referenciaBit: "+referenciaBit, EIGlobal.NivelLog.DEBUG);
				   strTabla.append(referenciaBit);
			   }

			   strTabla.append("</td><td ");
			   strTabla.append(colorbg);
			   strTabla.append("> ");
			   strTabla.append(TipoError);
			   strTabla.append(" </td>");

			   EIGlobal.mensajePorTrace("Verificando problema.", EIGlobal.NivelLog.DEBUG);
			 } else {
			  // Se arma la trama para el link del comprobante ...
			  EIGlobal.mensajePorTrace("Creando ligas para comprobantes.", EIGlobal.NivelLog.DEBUG);

			  strComprobante=new StringBuffer("");
			  strComprobante.append(Referencia);
			  strComprobante.append(pipe);
			  for(int j=0;j<10;j++)									//MSD 11/2005 Proyecto Ley de Transparencia
			   {
				  if (j==1)
				   {
					 //========================================
					 // 13/05/2004 SPEI-Cta ordenante v1 jbg
					 strComprobante.append(cta_clabe.trim());
					 strComprobante.append(pipe);
					 //========================================
				   }
				  strComprobante.append(Todas.camposTabla[i][j]);
				  strComprobante.append(pipe);
			   }
			  strComprobante.append(TipoError);
			  strComprobante.append(pipe);
			  strComprobante.append(EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
			  strComprobante.append(pipe);
			  strComprobante.append(pipe);  //Clave de rastreo
			  strComprobante.append(Todas.camposTabla[i][11]);	  // RFC
			  strComprobante.append(pipe);
			  strComprobante.append(Todas.camposTabla[i][12]);    // IVA
			  strComprobante.append(pipe);
			  strComprobante.append(Todas.camposTabla[i][14]);    // Forma aplicacion desc
			  strComprobante.append(pipe);
			  strComprobante.append(Todas.camposTabla[i][15]);    // Fecha aplicacion
			  strComprobante.append(pipe);
			  /* VECTOR 06-2016: SPID */
//			  strComprobante.append(Todas.camposTabla[i][16]); 	  // Tipo Divisa [USD o MXN]
//			  strComprobante.append(pipe);
			  
			  strComprobante.append("");    // Motivo de devolucion
			  strComprobante.append('@');

			  str="despliegaDatos('"+strComprobante+"');";
			  strTabla.append("<td ");
			  strTabla.append(colorbg);
			  strTabla.append("> &nbsp;<a href=\"javascript:");
			  strTabla.append(str);
			  strTabla.append("\">");
			  strTabla.append(Referencia);
			  strTabla.append("</a></td><td ");
			  strTabla.append(colorbg);
			  strTabla.append("> ");
			  strTabla.append(TipoError);
			  strTabla.append(" </td>");

			}

		  impTmp=new Double(Todas.camposTabla[i][4]).doubleValue();
		  Todas.camposTabla[i][4]=FormatoMoneda(Double.toString(impTmp));
		  /* VECTOR 06-2016: SPID Se aumenta el valor del For para el nuevo elemento */
		  for (int j = 0; j < 7; j++) {
		      strTabla.append("<td ");
			  strTabla.append(colorbg);
			  strTabla.append(" align=");
			  strTabla.append(alignTxt[align[j]]);
		      strTabla.append("> &nbsp;");
		      EIGlobal.mensajePorTrace("Antes Encoding", EIGlobal.NivelLog.INFO);
		      EIGlobal.mensajePorTrace("["+Todas.camposTabla[i][c[j]]+"]", EIGlobal.NivelLog.INFO);
		      strTabla.append(Todas.camposTabla[i][c[j]]);
		      //EIGlobal.mensajePorTrace("Con Encoding["+new String(Todas.camposTabla[i][c[j]].getBytes("ISO-8859-1"), "UTF-8")+"]", EIGlobal.NivelLog.INFO);
		      strTabla.append(" &nbsp;</td>");
		   // INDRA PYME EXPORTAR ARCHIVO Abril - 2015
		      asignaValor(lineaExportar, j, new String(Todas.camposTabla[i][c[j]].getBytes("ISO-8859-1"), "UTF-8"));
		   // INDRA PYME EXPORTAR ARCHIVO Abril - 2015
		  }		  

		  listaArchivoExportar.add(lineaExportar);
		  
		   strTabla.append("\n</tr>");
		 } // FIN FOR
		/* VECTOR 06-2016: SPID */
		ExportModel em = new ExportModel("sepfileexp", '|')
		   .addColumn(new Column("check", "Nϊmero de Referencia"))
		   .addColumn(new Column("tipoOperacion", "Estatus"))
		   .addColumn(new Column("cuenta", "Cuenta Origen"))
		   .addColumn(new Column("titular", "Cuenta Destino/Mσvil"))
		   .addColumn(new Column("banco", "Beneficiario"))
		   .addColumn(new Column("tipoDivisa", "Divisa"))
		   .addColumn(new Column("folioRegistro", "Importe"))		   
		   .addColumn(new Column("estado", "Concepto del Pago / Transferencia"))
		   .addColumn(new Column("observaciones", "Referencia Interbancaria"))
		   ;
		ses.setAttribute("ExportModel", em);
		ses.setAttribute("nombreBeanExportArch", listaArchivoExportar);
  	  }catch(Exception e){
  		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	  }finally{
	      try{
	    	  valCtas.closeTransaction();
	      }catch (Exception e) {
	    	  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	      }
	  }
		//VSWF-HGG  -  borra bandera
		req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);

		EmailSender emailSender=new EmailSender();
		EmailDetails beanEmailDetails = new EmailDetails();
		try {
			if (cod_error!=null) {
				cod_error_string = cod_error.toString().trim();
			}
//YHG AQUI DEBE ISSERTA
			if(emailSender.enviaNotificacion(cod_error_string)) {

				if (referencias!=null && referencias.contains(",")) {
					referencias = referencias.substring(0, referencias.lastIndexOf(","));
				}

				EIGlobal.mensajePorTrace("ββββββββββββββββββββ Se debe enviar notificacion? " + emailSender.enviaNotificacion(cod_error_string), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ Notificacion en MDI_Enviar sin archivo", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ cod_error_string ->" + cod_error_string, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ Importe ->" + importeTotal, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ no de registros ->" + Todas.totalRegistros, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ referencias ->" + referencias, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("ββββββββββββββββββββ status ->" + cod_error_string, EIGlobal.NivelLog.INFO);

				beanEmailDetails.setNumRegImportados(Todas.totalRegistros);
				beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(importeTotal));
				beanEmailDetails.setNumeroContrato(session.getContractNumber());
				beanEmailDetails.setRazonSocial(session.getNombreContrato());
				beanEmailDetails.setNumRef(referencias);
				beanEmailDetails.setEstatusActual(cod_error_string);


					if(Todas.totalRegistros > 1){
						emailSender.sendNotificacion(req, IEnlace.MAS_TRANS_INTERBANCARIAS, beanEmailDetails);
					}
					else{
						EIGlobal.mensajePorTrace("ββββββββββββββββββββ CuentaCargo ->" + Todas.camposTabla[0][0], EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("ββββββββββββββββββββ CuentaDestino ->" + Todas.camposTabla[0][2], EIGlobal.NivelLog.INFO);

						beanEmailDetails.setNumCuentaCargo(Todas.camposTabla[0][0]);
						beanEmailDetails.setCuentaDestino(Todas.camposTabla[0][2]);
						beanEmailDetails.setTipoTransferenciaUni("Interbancaria");

						emailSender.sendNotificacion(req, IEnlace.UNI_TRANS_INTERBANCARIAS, beanEmailDetails);
					}

		}
		//Termina mail
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

     }

     if(comuError){
	   despliegaPaginaError(mensajeError,"Env&iacute;o de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Cotizaci&oacute;n &gt; Env&iacute;o","s25470h", req, res);
     } else
	  {
		EIGlobal.mensajePorTrace("Verificando canal de operaciones.", EIGlobal.NivelLog.DEBUG);
		if(arcExp)
		 {
		   strTabla=new StringBuffer("Archivo de exportacion");
		   Todas.strOriginal="Archivo de Exportacion";
		 }
		else
		 {
			Todas.strOriginal=nuevaTrama.toString();
			strTabla.append("\n</table>");
		 }

		if(salidaArc)
		 {
			try {
				//*************************************************************
				ArchivoRemoto archR = new ArchivoRemoto();
				  if(!archR.copiaLocalARemoto(nombreSalida,"WEB")){
					 EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO); }
			} catch (IllegalStateException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}

		 }


		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());

		req.setAttribute("TotalTrans",Todas.strOriginal);
		req.setAttribute("Tabla",strTabla.toString());
		req.setAttribute("CLABE","1"); // 13/05/2004 Cambio SPEI-Cta ordenante v1 jbg

		req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		req.setAttribute("Archivo",usuario+".doc");
		req.setAttribute("web_application",Global.WEB_APPLICATION);

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Env&iacute;o de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Cotizaci&oacute;n &gt; Env&iacute;o","s25470h",req));

		ses.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		ses.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		ses.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		ses.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		ses.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());

		ses.setAttribute("TotalTrans",Todas.strOriginal);
		ses.setAttribute("Tabla",strTabla.toString());
		ses.setAttribute("CLABE","1"); // 13/05/2004 Cambio SPEI-Cta ordenante v1 jbg

		ses.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		ses.setAttribute("Archivo",usuario+".doc");
		ses.setAttribute("web_application",Global.WEB_APPLICATION);

		ses.setAttribute("MenuPrincipal", session.getStrMenu());
		ses.setAttribute("newMenu", session.getFuncionesDeMenu());
		ses.setAttribute("Encabezado", CreaEncabezado("Env&iacute;o de Transferencias Interbancarias","Transferencias &gt; Interbancarias &gt; Cotizaci&oacute;n &gt; Env&iacute;o","s25470h",req));
		/* VECTOR 06-2016: SPID */
		req.setAttribute("infoTipoDivisa", tipoDivisa);
		
		if(arcExp)
		 {

			try{
			   req.setAttribute("totalRegistros",java.net.URLDecoder.decode((String) req.getParameter("Lineas"),enc));
			   req.setAttribute("importeTotal",java.net.URLDecoder.decode((String) req.getParameter("Importe"),enc));
			   String CadImporte = java.net.URLDecoder.decode((String) req.getParameter("Importe"),enc);
			   EIGlobal.mensajePorTrace("valor de Importe:" +CadImporte, EIGlobal.NivelLog.DEBUG);
			   // EVERIS 28 Mayo 2007 PARA EVITAR UN ERROR NULLPOINTER EXCEPTION String CadImporte2 = java.net.URLDecoder.decode((String) req.getParameter("importeTotal"));
			   String CadImporte2 = ( req.getParameter("importeTotal") != null )? java.net.URLDecoder.decode((String) req.getParameter("importeTotal"),enc) : "";
			   EIGlobal.mensajePorTrace("valor de Importe2:" +CadImporte2, EIGlobal.NivelLog.DEBUG);

			    EmailSender emailSender=new EmailSender();
				EmailDetails beanEmailDetails = new EmailDetails();

				if (cod_error_string ==  null) {
					cod_error_string = "";
				}

				if(emailSender.enviaNotificacionArchivo(cod_error_string)) {
					int noRegistros = 0;

					try {
						noRegistros = Integer.parseInt(req.getParameter("Lineas"));
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}

					EIGlobal.mensajePorTrace("ββββββββββββββββββββ Notificacion en MDI_Enviar con archivo", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ Se debe enviar notificacion? " + emailSender.enviaNotificacionArchivo(cod_error_string), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ cod_error ->" + cod_error_string, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ Importe ->" + CadImporte, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ no de registros ->" + noRegistros, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ referencia ->" + referenciaArchivo, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ββββββββββββββββββββ status ->" + cod_error_string, EIGlobal.NivelLog.INFO);

					beanEmailDetails.setNumRegImportados(noRegistros);
					beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(CadImporte));
					beanEmailDetails.setNumeroContrato(session.getContractNumber());
					beanEmailDetails.setRazonSocial(session.getNombreContrato());
					beanEmailDetails.setNumRef(referenciaArchivo);
					beanEmailDetails.setEstatusActual(cod_error_string);

					try {

						if(noRegistros > 1){
							emailSender.sendNotificacion(req, IEnlace.MAS_TRANS_INTERBANCARIAS, beanEmailDetails);
						}
						else{

							EIGlobal.mensajePorTrace("ββββββββββββββββββββ CuentaCargo ->" + objTrama.camposTabla[0][6], EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("ββββββββββββββββββββ CuentaDestino ->" + objTrama.camposTabla[0][9], EIGlobal.NivelLog.INFO);

							beanEmailDetails.setNumCuentaCargo(objTrama.camposTabla[0][6]);
							beanEmailDetails.setCuentaDestino(objTrama.camposTabla[0][9]);

							beanEmailDetails.setTipoTransferenciaUni("Interbancaria");

							emailSender.sendNotificacion(req, IEnlace.MAS_TRANS_INTERBANCARIAS, beanEmailDetails);
						}
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
			   }
			//Termina mail


			}catch(Exception e)
			{
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		 }
	    else
		 {
		   req.setAttribute("totalRegistros","Total de Registros: "+Integer.toString(Todas.totalRegistros));
		   req.setAttribute("importeTotal","Importe Total: "+FormatoMoneda(Double.toString(importeTotal)));
		   ses.setAttribute("totalRegistros","Total de Registros: "+Integer.toString(Todas.totalRegistros));
		   ses.setAttribute("importeTotal","Importe Total: "+FormatoMoneda(Double.toString(importeTotal)));
		   		  		   
		   //San, se comenta el evaltemplate y se prueba el send redirect
		//   res.sendRedirect(res.encodeRedirectURL("/NASApp/"+Global.WEB_APPLICATION+"/jsp/MDI_Enviar.jsp"));
                  //  EIGlobal.mensajePorTrace("yes termina MDI_Enviar -.", EIGlobal.NivelLog.INFO);

					//San, se comenta el evaltemplate y se prueba el send redirect
                    //evalTemplate("/jsp/MDI_Enviar.jsp", req, res);
		 }
                //  EIGlobal.mensajePorTrace("yes 2 termina MDI_Enviar -.", EIGlobal.NivelLog.INFO);
	  }
     respuestaProgramadoResultado[0] = 0;
     return respuestaProgramadoResultado;
  }
  /**
   * Metodo para obtener la fecha programada
   * @since 28/07/2015
   * @author FSW-Indra
   * @param Reg_and_NoReg todos los registros
   * @param strCheck registros
   * @return String registros seleccionados con fecha programada
   */
  private String obtenerFechaProgramada( String Reg_and_NoReg, String strCheck ){
		StringBuffer Reg_EnviarDos = new StringBuffer("");
		String[] regAndNoReg = Reg_and_NoReg.split("@");
		for( int i = 0 ; i < regAndNoReg.length ; i++ ){
			if( strCheck.charAt(i) == '1' ){
				Reg_EnviarDos.append( regAndNoReg[i] )
					.append('@');
			}
		}
		return Reg_EnviarDos.toString();
  }

  /**
   * Asigna valor a bean para exportacion
   * @since 22/04/2015
   * @author FSW-Indra
   * @param linea objeto a exportar
   * @param posicion posicion del arreglo TODAS
   * @param valor valor a asignar
   */
  private void asignaValor(ExportTransferenciasBean linea,
		  int posicion, String valor) {
	  switch(posicion) {
	  		case 0:  linea.setCuenta(valor); // Cuenta origen
	  			break;
	  		case 1:  linea.setTitular(valor); //  Cuenta Destino/Mσvil
  			break;
	  		case 2:  linea.setBanco(valor); //  Beneficiario
  			break;
	  		case 3:  linea.setTipoDivisa(valor); // DIVISA SPID
  			break;
	  		case 4:  linea.setFolioRegistro(valor); // IMPORTE
  			break;
	  		case 5:  linea.setEstado(valor); // CONCEPTO
  			break;
	  		case 6:  linea.setObservaciones(valor); // REFERENCIA BANCARIA
  			break;
	  }
  }
  /**
   * Metodo para separar operaciones programadas y no programadas
   * @since 29/07/2015
   * @author FSW-Indra
   * @param trama trama con el total de operaciones
   * @param strCheck registros seleccionados
   * @return String[] resultado de cadenas
   * @throws BusinessException excepcion a manejar
   */
  private String[] sepTrnsPrg(String trama, String strCheck) throws BusinessException
  {
	  EIGlobal.mensajePorTrace("MDI_Interbancario - sepTrnsPrg Nuevo :: Inicia", EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("MDI_Interbancario - sepTrnsPrg Nuevo :: Inicia", EIGlobal.NivelLog.INFO);
	  String[] resultadoCadenas = new String[2];
	  String[] operaciones = trama.split("@");
	  int totalOperaciones = operaciones.length;
	  StringBuffer programadas = new StringBuffer("");
	  for( int i = 0 ; i < totalOperaciones ; i++ ){
		  if( strCheck.charAt(i) != '0' ){
			  String[] camposOperacion = operaciones[i].split("\\|");
			  if( camposOperacion.length == 15 ){
				  EIGlobal.mensajePorTrace("MDI_Interbancario - sepTrnsPrg Nuevo :: Es un contrato Mancomunado", EIGlobal.NivelLog.INFO);
				  throw new BusinessException("Es un contrato Mancomunado");
			  }
			  if( !camposOperacion[10].equals( camposOperacion[15] ) ){
				  //Programadas
				  EIGlobal.mensajePorTrace("MDI_Interbancario - sepTrnsPrg Nuevo :: Es programada", EIGlobal.NivelLog.INFO);
				  programadas.append(operaciones[i])
				  	.append('@');
			  }
		  }
	  }
	  resultadoCadenas[0] = programadas.toString();
	  resultadoCadenas[1] = strCheck.substring(totalOperaciones);
	  EIGlobal.mensajePorTrace("MDI_Interbancario - sepTrnsPrg Nuevo :: Fin", EIGlobal.NivelLog.INFO);
	  return resultadoCadenas;
  }
  /**
   * Obtiene valor programadas
   * @since 30/04/2015
   * @author FSW-Indra
   * @param prog si es porgramada
   * @param inTrm trama
   * @param sSep seperarados
   * @param nCmp1 inicio
   * @param nCmp2 fin
   * @return cadena programadas
   */
    private String sepTrnsPrg(String prog, String inTrm, String sSep, int nCmp1, int nCmp2)
    {
      String outTrm="";
      String curMem="";
      String nxtTrm="";
      EIGlobal.mensajePorTrace("MDI_Interbancario - sepTrnsPrg():", EIGlobal.NivelLog.INFO);
      EIGlobal.mensajePorTrace("                    prog<" + prog + ">", EIGlobal.NivelLog.INFO);
      EIGlobal.mensajePorTrace("                    inTrm<" + inTrm + ">", EIGlobal.NivelLog.INFO);
      EIGlobal.mensajePorTrace("                    sSep<" + sSep + ">", EIGlobal.NivelLog.INFO);
      EIGlobal.mensajePorTrace("                    nCmp1<" + nCmp1 + ">", EIGlobal.NivelLog.INFO);
      EIGlobal.mensajePorTrace("                    nCmp1<" + nCmp2 + ">", EIGlobal.NivelLog.INFO);
      curMem=inTrm.substring(0,inTrm.indexOf(sSep));
      nxtTrm=inTrm.substring(inTrm.indexOf(sSep)+1);
      if(obtCmpNReg(curMem, "|", nCmp1).equals(obtCmpNReg(curMem, "|", nCmp2)))
      {
//        System.out.println("curMem - fechas iguales (NO PROGRAMADA)");
        if("S".equals(prog))
        {
//          System.out.println("excluye NO programada");
          outTrm = "";
        }
        else
        {
//          System.out.println("incluye SI programada");
          outTrm = curMem + sSep;
        }
      }
      else
      {
//        System.out.println("curMem - fechas diferentes (PROGRAMADA)");
        if("S".equals(prog))
        {
//          System.out.println("incluye SI programada");
          outTrm = curMem + sSep;
        }
        else
        {
//          System.out.println("excluye NO programada");
          outTrm = "";
        }
      }
      if(nxtTrm.length()>0)
      {
        return outTrm + sepTrnsPrg(prog, nxtTrm, sSep, nCmp1, nCmp2);
      }
      else
      {
        return outTrm;
      }
    }
    /**
     * Separa elemenot.
     * @since 30/04/2015
     * @author FSW-Indra
     * @param inReg Inicio.
     * @param sSep Separador.
     * @param nCmp Fin.
     * @return Cadena.
     */
    private String obtCmpNReg(String inReg, String sSep, int nCmp)
    {
      String sCmp = "";
      if (nCmp == 0)
      {
        if (inReg.indexOf(sSep)>(-1))
        {
          sCmp = inReg.substring(0,inReg.indexOf(sSep));
        }
        else
        {
          sCmp = inReg;
        }
        EIGlobal.mensajePorTrace("MDI_Interbancario - obtCmpNReg(): => sCmp<" + sCmp + ">", EIGlobal.NivelLog.ERROR);
      }
      else
      {
        if (inReg.indexOf(sSep)>(-1))
        {
          sCmp = obtCmpNReg(inReg.substring(inReg.indexOf(sSep)+1, inReg.length()), sSep, nCmp-1);
        }
        else
        {
          sCmp = inReg;
        }
      }
      return sCmp.trim();
    }
  /**
	 * Metodo para desentramas la cadena de las operaciones programadas.
	 * @since 16/03/2015
	 * @author FSW-Indra
	 * @param cadenaProgramadas de tipo String
	 * @param contrato de tipo String
	 * @param usuario de tipo String
	 * @param clavePerfil de tipo String
	 * @param req de tipo HttpServletRequest
	 * @return ArrayList<OperacionesProgramadasBean> de tipo OperacionesProgramadasBean
	 */
	private List<OperacionesProgramadasBean> obtieneValorasOpeProgramadas(
			String cadenaProgramadas, String contrato, String usuario,
			String clavePerfil, HttpServletRequest req) {
		/*REGISTRADAS
			CuentaCargo|DescripcnCuentaCargo|CuentaAbno|Beneficiario|Impr|Concepto|RefIBnc|Banco|Plaza|Scrl|FechDelDia|RFC|ImpIVA|A|AplicacionDes|FchDeProgr@
			SPEI
				11000360907|PRUEBA PYMES 7X|4423430802|BENEFICIARIO02 SI PROG|02.00|CONCEPTO02 SI PROG|0234567|BANAM|01001|0234|23/03/2015|||H|Mismo Dνa|24/03/2015@
			TEF
				11000402037|PRUEBA PYMES 5|4423430858|BENEFICIARIO|11.00|CONCEPTO|1234567|BAJIO|01001|1234|23/03/2015|||2|Dνa Siguiente|24/03/2015@*/
		/*NO REGISTRADAS
			CuentaCargo|DescripcnCuentaCargo|CuentaAbno|Beneficiario|Impr|Concepto|RefIBnc|Banco|Plaza|Scrl|FechDelDia|RFC|ImpIVA|A|AplicacionDes|FchDeProgr@
			SPEI
				11000360907|PRUEBA PYMES 7X|4423430802|BENEFICIARIO02 SI PROG|02.00|CONCEPTO02 SI PROG|0234567|BANAM|01001|0234|23/03/2015|||H|Mismo Dνa|24/03/2015@
			TEF
				11000402037|PRUEBA PYMES 5|4423430858|BENEFICIARIO|11.00|CONCEPTO|1234567|BAJIO|01001|1234|23/03/2015|||2|Dνa Siguiente|24/03/2015@*/
		EIGlobal.mensajePorTrace(" ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               -> ENTRO obtieneValorasOpeProgramadas()", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               cadenaProgramadas = " + cadenaProgramadas, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               contrato          = " + contrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               usuario           = " + usuario, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               -> clavePerfil       = " + clavePerfil, EIGlobal.NivelLog.INFO);
		ArrayList<OperacionesProgramadasBean> listaOperacionesProgramadas = new ArrayList<OperacionesProgramadasBean>();
//		StringTokenizer numeroOperacionesProgramadas = new StringTokenizer(cadenaProgramadas);
		String dirIP =  obtenerIPCliente(req);
		StringTokenizer numeroOperacionesProgramadas = new StringTokenizer(cadenaProgramadas,"@");
		while (numeroOperacionesProgramadas.hasMoreElements()) {
			String cadenaOperacionesProg = (String) numeroOperacionesProgramadas.nextElement();
			OperacionesProgramadasBean operacionesProgramadas = new OperacionesProgramadasBean();
			String[] valoresProgramdas = cadenaOperacionesProg.split("\\|");
			operacionesProgramadas.setNumCuenta(contrato);
			operacionesProgramadas.setUsuario(usuario);
			operacionesProgramadas.setTipoCtaOrigen("P");
			operacionesProgramadas.setTipoCtaDestino("T");
			operacionesProgramadas.setTitulos("0");
			operacionesProgramadas.setCvePerfil(clavePerfil);
			operacionesProgramadas.setTipoOperacion(DIBT);
			operacionesProgramadas.setReferenciaBit("0");
			operacionesProgramadas.setCuenta1(valoresProgramdas[0]); //CuentaCargo
			operacionesProgramadas.setDescripcionCtaOrigen(valoresProgramdas[1]); //DescripcnCuentaCargo
			operacionesProgramadas.setCuenta2(valoresProgramdas[2]); //CuentaAbno
			operacionesProgramadas.setDescripcionCtaDestino(valoresProgramdas[3]); //Beneficiario?
			operacionesProgramadas.setImporte(valoresProgramdas[4]); //Impr
			//operacionesProgramadas.setConcepto(armaConcepto(valoresProgramdas[5], valoresProgramdas[11], valoresProgramdas[12])); //Concepto
			//private String armaConcepto(String concepto, String rfc, String iva)
			operacionesProgramadas.setConcepto(formateaTamanioCampo(valoresProgramdas[5],40)); //Concepto
			operacionesProgramadas.setRFC(valoresProgramdas[11]);
			operacionesProgramadas.setIva(valoresProgramdas[12]);
			operacionesProgramadas.setRefInterb(valoresProgramdas[6]); //RefIBnc
			operacionesProgramadas.setCveBancoCtaDestino(valoresProgramdas[7]); //Banco
			operacionesProgramadas.setPlazaCtaDestino(valoresProgramdas[8]); //Plaza
			operacionesProgramadas.setSucursalCtaDestino(valoresProgramdas[9]); //Scrl
			operacionesProgramadas.setFchRegistro(valoresProgramdas[10]); //FechDelDia
			if ( "2".equals(valoresProgramdas[13]) ) { //A (Forma de Aplicacion)
				operacionesProgramadas.setFormaAplicacion("TEF");
			} else {
				operacionesProgramadas.setFormaAplicacion("SPEI");
			}
			operacionesProgramadas.setFechaAplicacion(valoresProgramdas[15]); //FchDeProgr
			EIGlobal.mensajePorTrace("                               DIVISA SPID  <" + valoresProgramdas[16] + ">", EIGlobal.NivelLog.INFO);			
			operacionesProgramadas.setDivisa(valoresProgramdas[16]); //Divisa SPID
			EIGlobal.mensajePorTrace("                               CLAVE SPID  <" + valoresProgramdas[18] + ">", EIGlobal.NivelLog.INFO);
			operacionesProgramadas.setCveOperacionSPID(valoresProgramdas[18]);
			operacionesProgramadas.setDireccionIP(dirIP);
			listaOperacionesProgramadas.add(operacionesProgramadas);
			
		}
		return listaOperacionesProgramadas;
	}
	/**
	 * Arma texto concepto.
	 * @since 30/04/2015
	 * @author FSW-Indra
	 * @param concepto concepto
	 * @param rfc RFC
	 * @param iva IVA
	 * @return texto concepto.s
	 */
	  private String armaConcepto(String concepto, String rfc, String iva)
	  {
	    concepto=formateaTamanioCampo(concepto,40);
	    if(!rfc.trim().equals("")){
	      return concepto + "RFC " + rfc + " IVA " + iva;
	    } else {
	      return concepto;
	    }
	  }
	  /**
	   * Formatea campo.
	   * @since 30/04/2015
	   * @author FSW-Indra
	   * @param campo campo
	   * @param tamanio longitud
	   * @return campo formateada
	   */
	  private String formateaTamanioCampo(String campo,int tamanio)
	  {
	    String spc="                                                                                                                                                ";
	    String campoFormateado="";

	    campo=campo.trim();
	    if(campo.length()<tamanio){
	      campoFormateado=campo + spc.substring(0, (tamanio-campo.length()) );}
	    if(campo.length()>tamanio){
	      campoFormateado=campo.substring(0,tamanio);}
	    if(campo.length()==tamanio){
	      campoFormateado=campo;}
	    return campoFormateado;
	  }
/*************************************************************************************/
/****************************************************** genera Trama Cotizar/Enviar  */
/*************************************************************************************/
	  /**
	   * Genera trama
	   * @param tipoRelacion String
	   * @param concepto String
	   * @param referenciaOper String
	   * @param formaAplica String
	   * @param cuentaAbono String
	   * @param banco String
	   * @param beneficiario String
	   * @param importe String
	   * @param plaza String
	   * @param sucursal String
	   * @param req HttpServletRequest
	   * @param clavePerfil String
	   * @param usuario String
	   * @param contrato String
	   * @param servicio boolean
	   * @param tDivisa String
	   * @param rfcTran String
	   * @param spidTipoOperacion String
	   * @return
	   */
  String generaTrama( String tipoRelacion,String concepto, String referenciaOper, String formaAplica,String cuentaAbono,String banco,String beneficiario,
						String importe,String plaza,String sucursal,HttpServletRequest req, String clavePerfil, String usuario, String contrato, boolean servicio, String tDivisa, String rfcTran, String spidTipoOperacion)
    {
	  String cabecera="";
	  String regInterbancario="";
	  String cuentaCargo = (String)req.getAttribute("cuentaCargo");
	  String titular = (String) req.getAttribute(TITULAR);
	  titular=(titular==null || titular.equals(""))?" ":titular;
	  beneficiario=(beneficiario==null || beneficiario.equals(""))?" ":beneficiario;
	  referenciaOper=(referenciaOper==null || referenciaOper.equals(""))?" ":referenciaOper;
	  plaza=(plaza==null || plaza.trim().equals(""))?" ":plaza;


	  //**** Cambio de sucursal nula a un 0   everis 18/07/2008    inicio

	  sucursal=(sucursal==null || sucursal.trim().equals(""))?"0":sucursal;

      //**** Cambio de sucursal nula a un 0   everis 18/07/2008    fin


      if(servicio)
       {
    	  String dirIP =  obtenerIPCliente(req);
			tipoOperacion=DIBT;
			regInterbancario=cuentaCargo.trim()	+ pipe +
						  tipoRelacion.trim()	+ pipe +
						  titular				+ pipe +
						  cuentaAbono.trim()	+ pipe +
						  banco.trim()			+ pipe +
						  beneficiario			+ pipe +
						  sucursal      		+ pipe +
						  importe.trim()		+ pipe +
						  plaza       			+ pipe +
						  referenciaMed.trim()	+ pipe +
						  concepto			    + pipe +
						  referenciaOper        + pipe +
						  " "					+ pipe +
						  formaAplica			+ pipe +						  
						  tDivisa				+ pipe +
						  rfcTran				+ pipe +
						  dirIP					+ pipe +
						  spidTipoOperacion.trim()	+ pipe;
			
			
       }
      else
       {
			tipoOperacion="DIBC";
			regInterbancario=cuentaCargo.trim()	+ pipe +
						  tipoRelacion.trim()	+ pipe +
						  titular				+ pipe +
						  cuentaAbono.trim()	+ pipe +
						  banco.trim()			+ pipe +
						  beneficiario			+ pipe +
						  sucursal.trim()		+ pipe +
						  importe.trim()		+ pipe +
						  plaza.trim()			+ pipe;
       }
      
	 cabecera=	  medioEntrega			+ pipe +
				  usuario				+ pipe +
				  tipoOperacion			+ pipe +
		          contrato				+ pipe +
		          usuario				+ pipe +
		          clavePerfil			+ pipe;

	 EIGlobal.mensajePorTrace("MDI_Enviar - generaTrama() - TRAMA TRX < "+cabecera + regInterbancario+" >", EIGlobal.NivelLog.INFO); 
     return cabecera + regInterbancario;
   }


/*************************************************************************************/
/******************************************************************** formateaCampo  */
/*************************************************************************************/
  /**
   * Metodo que formatea campo a un tamanio
   * @param campo nombre del campo
   * @param tamanio tamanio a definir
   * @return String campo formateado
   */
   String formateaCampo(String campo,int tamanio)
    {
      String spc="                                                                                                                                                ";
      String campoFormateado="";

      campo=campo.trim();
	  if(campo.length()<tamanio){
		  campoFormateado=campo + spc.substring(0, (tamanio-campo.length()) ); }
      if(campo.length()>tamanio){
		  campoFormateado=campo.substring(0,tamanio); }
      if(campo.length()==tamanio){
		  campoFormateado=campo; }
      return campoFormateado;
    }

// Metodo para verificar existencia del Archivo de Sesion para evitar transmision duplicada
/**
 * @param arc String
 * @param eliminar boolean
 * @return boolean
 */
public boolean verificaArchivos(String arc,boolean eliminar)
	throws ServletException, java.io.IOException
{
	EIGlobal.mensajePorTrace( "Consult.java Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
	File archivocomp = new File( arc );
	if (archivocomp.exists())
	 {
	   EIGlobal.mensajePorTrace( "Consult.java El archivo si existe.", EIGlobal.NivelLog.DEBUG);
	   if(eliminar)
		{
	      archivocomp.delete();
		  EIGlobal.mensajePorTrace( "Consult.java El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
		}
	   return true;
	 }
	EIGlobal.mensajePorTrace( "Consult.java El archivo no existe.", EIGlobal.NivelLog.DEBUG);
	return false;
}

/**
	 * Bitacora TCT
	 * @param bh : BitaHelper
	 * @param usuario : String
	 */
	private void bitacoraTCT(BitaHelper bh, String usuario, int referencia, String concepto,String cuentaOrigen, String cuentaDestinoFondo,double importe ) {
		EIGlobal.mensajePorTrace( "MDI_Enviar.java Entro Bitacora", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MDI_Enviar.java usuario: "+usuario, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MDI_Enviar.java referencia:"+referencia, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MDI_Enviar.java concepto:"+concepto, EIGlobal.NivelLog.DEBUG);
		BitaTCTBean beanTCT = new BitaTCTBean ();
		beanTCT = bh.llenarBeanTCT(beanTCT);
		beanTCT.setReferencia(referencia);
		beanTCT.setTipoOperacion("PROG");
		beanTCT.setCodError("PROG0000");
		beanTCT.setOperador(usuario);
		beanTCT.setCuentaOrigen(cuentaOrigen);
		beanTCT.setCuentaDestinoFondo(cuentaDestinoFondo);
		beanTCT.setImporte(importe);
		beanTCT.setConcepto(concepto);

		try {
		      BitaHandler.getInstance().insertBitaTCT(beanTCT);
		}catch(Exception e) {
		      EIGlobal.mensajePorTrace("Cambio de Imagen - Cambio de Imagen",EIGlobal.NivelLog.ERROR);
		}
	}
}