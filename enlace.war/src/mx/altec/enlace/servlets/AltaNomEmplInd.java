package mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.NomAltaIndUtil.RELLENA_IZQUIEDA;
import static mx.altec.enlace.utilerias.NomAltaIndUtil.depuraLista;
import static mx.altec.enlace.utilerias.NomAltaIndUtil.escribirRespuestaCodigoPostal;
import static mx.altec.enlace.utilerias.NomAltaIndUtil.generaRegistro;
import static mx.altec.enlace.utilerias.NomAltaIndUtil.obtenerEmpleado;
import static mx.altec.enlace.utilerias.NomAltaIndUtil.rellenaCadena;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.NomPreCodPost;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.NomPreEmpleadoDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Class AltaNomEmplInd.
 */
public class AltaNomEmplInd extends BaseServlet {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3322416680934685801L;

	/** The Constant SESSION_BR. */
	private static final String SESSION_BR = "session";

 	/** La constante POS_MENU_ALTA_PRINCIPAL. */
	 private static final String POS_MENU_ALTA_PRINCIPAL = "Servicios &gt; N&oacute;mina &gt;Alta de Empleados";

 	/** La constante POS_MENU_ALTA_IND. */
	 private static final String POS_MENU_ALTA_IND = "Servicios &gt; N&oacute;mina &gt; Alta de Empleados &gt; Alta de empleado individual";

	/**  Constante para archivo de ayuda *. */
	private static final String ARCHIVO_AYUDA = "aEDC_ConConfig";

	/** La constante LISTA_EMPL_PARAMETRO. */
	private static final String LISTA_EMPL_PARAMETRO = "listaEmpleados";

	/** La constante PANTALLA_ALTA_DE_EMPLEADOS. */
	private static final String PANTALLA_ALTA_DE_EMPLEADOS = "/jsp/AltaEmpleados.jsp";

	/** La constante PANTALLA_INICIAL_ALTA. */
	private static final String PANTALLA_INICIAL_ALTA = "/jsp/Nom_mtto_emp.jsp";

	/** La constante ID_EMPLEADO_PARAM. */
	private static final String ID_EMPLEADO_PARAM = "idEmpleado";

	/** La constante INICIALIZA_PANTALLA. */
	private static final int INICIALIZA_PANTALLA = 1;

	/** La constante AGREGA_EMPLEADO. */
	private static final int AGREGA_EMPLEADO = 2;

	/** La constante CONSULTA_COLONIAS. */
	private static final int CONSULTA_COLONIAS = 3;

	/** La constante ENVIA_ARCHIVO. */
	private static final int ENVIA_ARCHIVO = 4;

	/** La constante CANCELA_ARCHIVO. */
	private static final int CANCELA_ARCHIVO = 5;

	/** La constante GUARDAR_Y_SALIR. */
	private static final int GUARDAR_Y_SALIR = 6;

	/** La constante ACCION_REGRESAR. */
	private static final int ACCION_REGRESAR = 7;

	/** La constante ARROBA. */
	private static final char ARROBA = '@';

	/**
	 * Get.
	 *
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * Post.
	 *
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException{
		defaultAction( req, res );
	}


	/**
	 * Default action.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{
		final HttpSession sess = req.getSession();
		String opcion= (String)req.getParameter("opcion");
		if(opcion==null|| "".equals(opcion)){
			EIGlobal.mensajePorTrace( "AltaNomEmplInd - defaultAction: opcion es nula, se inicializara", EIGlobal.NivelLog.INFO);
			opcion="1";
		}
		EIGlobal.mensajePorTrace( "AltaNomEmplInd - defaultAction: opcion " + opcion, EIGlobal.NivelLog.INFO);
		if (!SesionValida( req, res)){
			EIGlobal.mensajePorTrace( "AltaNomEmplInd SESION NO VALIDA.", EIGlobal.NivelLog.INFO);
			return;
		}else{
			req.setAttribute("facultades", req.getSession().getAttribute("facultadesE"));
		}
		final int accion = Integer.parseInt(opcion);
		if(accion!=1&&(req.getParameter(ID_EMPLEADO_PARAM)!= null&&!"".equals(req.getParameter(ID_EMPLEADO_PARAM)))){
			actualizarEmpleado(accion,req,res);
			return;
		}
		switch (accion) {
		case INICIALIZA_PANTALLA:
			inicializarPantalla(req,res);
			break;
		case AGREGA_EMPLEADO:
			agregarEmpleado(req,res,accion);
			break;
		case GUARDAR_Y_SALIR:
			guardarSalir(req,res,accion);
			break;
		case CONSULTA_COLONIAS:
			consultarCodigoPostal(req,res);
			break;
		case ENVIA_ARCHIVO:
			enviarArchivo(req,res);
			break;
		case CANCELA_ARCHIVO:
			sess.removeAttribute(LISTA_EMPL_PARAMETRO);
		case ACCION_REGRESAR:
			generaMenu(req, POS_MENU_ALTA_PRINCIPAL);
			sess.removeAttribute(LISTA_EMPL_PARAMETRO);
			evalTemplate(PANTALLA_INICIAL_ALTA, req, res);
			break;
		}
	}

	/**
	 * Actualizar empleado.
	 *
	 * @param accion El objeto: accion
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	private void actualizarEmpleado(int accion, HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {
		final HttpSession sess = req.getSession();
		List<Map<String,String>> listaEmpleados = (List<Map<String, String>>) sess.getAttribute(LISTA_EMPL_PARAMETRO);
		Map<String,String> empleado = obtenerEmpleado(req);
		int idEmpleado = Integer.parseInt(req.getParameter(ID_EMPLEADO_PARAM));
		listaEmpleados.remove(idEmpleado);
		listaEmpleados.add(empleado);
		sess.setAttribute(LISTA_EMPL_PARAMETRO, listaEmpleados);
		EIGlobal.mensajePorTrace( "AltaNomEmplInd - agregarEmpleado: terminado ", EIGlobal.NivelLog.INFO);
		req.setAttribute("numEmpleados",listaEmpleados.size());
		if (accion==AGREGA_EMPLEADO) {
			generaMenu(req, POS_MENU_ALTA_IND);
			evalTemplate(PANTALLA_ALTA_DE_EMPLEADOS, req, res);
		}else{
			generaMenu(req, POS_MENU_ALTA_PRINCIPAL);
			evalTemplate(PANTALLA_INICIAL_ALTA, req, res);
		}
	}

	/**
	 * Inicializar pantalla.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	private void inicializarPantalla(HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {
		generaMenu(req, POS_MENU_ALTA_IND);
		String idEmpleado = req.getParameter(ID_EMPLEADO_PARAM);
		final HttpSession sess = req.getSession();
		List<Map<String,String>> listaEmpleados = (List<Map<String, String>>) sess.getAttribute(LISTA_EMPL_PARAMETRO);
		if (listaEmpleados==null) {
			listaEmpleados = new ArrayList<Map<String,String>>();
		}

		if (idEmpleado!=null&&!"".equals(idEmpleado)) {
			EIGlobal.mensajePorTrace( "AltaNomEmplInd - inicializarPantalla: se modificara el empleado: "+idEmpleado, EIGlobal.NivelLog.INFO);
			listaEmpleados = (List<Map<String, String>>) sess.getAttribute(LISTA_EMPL_PARAMETRO);
			req.setAttribute("empleado", listaEmpleados.get(Integer.parseInt(idEmpleado)));
			req.setAttribute(ID_EMPLEADO_PARAM,idEmpleado);
		}
		req.setAttribute("numEmpleados",listaEmpleados.size());
		EIGlobal.mensajePorTrace( "AltaNomEmplInd - inicializarPantalla: terminado ", EIGlobal.NivelLog.INFO);
		evalTemplate(PANTALLA_ALTA_DE_EMPLEADOS, req, res);
	}

	/**
	 * Agregar empleado.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @param accion El objeto: accion
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	private void agregarEmpleado(HttpServletRequest req, HttpServletResponse res, int accion) throws ServletException, IOException {
		final HttpSession sess = req.getSession();
		List<Map<String,String>> listaEmpleados = (List<Map<String, String>>) sess.getAttribute(LISTA_EMPL_PARAMETRO);
		if (listaEmpleados==null) {
			listaEmpleados = new ArrayList<Map<String,String>>();
		}
		Map<String,String> empleado = obtenerEmpleado(req);
		listaEmpleados.add(empleado);
		sess.setAttribute(LISTA_EMPL_PARAMETRO, listaEmpleados);
		EIGlobal.mensajePorTrace( "AltaNomEmplInd - agregarEmpleado: terminado "+listaEmpleados.size(), EIGlobal.NivelLog.INFO);
		req.setAttribute("numEmpleados",listaEmpleados.size());

		generaMenu(req, POS_MENU_ALTA_IND);
		evalTemplate(PANTALLA_ALTA_DE_EMPLEADOS, req, res);
	}

	/**
	 * Guardar empleado y salir.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @param accion El objeto: accion
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	private void guardarSalir(HttpServletRequest req, HttpServletResponse res, int accion) throws ServletException, IOException {
		final HttpSession sess = req.getSession();
		List<Map<String,String>> listaEmpleados = (List<Map<String, String>>) sess.getAttribute(LISTA_EMPL_PARAMETRO);
		if (listaEmpleados==null) {
			listaEmpleados = new ArrayList<Map<String,String>>();
		}

		sess.setAttribute(LISTA_EMPL_PARAMETRO, listaEmpleados);
		EIGlobal.mensajePorTrace( "AltaNomEmplInd - guardarSalir: terminado "+listaEmpleados.size(), EIGlobal.NivelLog.INFO);
		req.setAttribute("numEmpleados",listaEmpleados.size());
		generaMenu(req, POS_MENU_ALTA_PRINCIPAL);
		evalTemplate(PANTALLA_INICIAL_ALTA, req, res);
	}

	/**
	 * Consultar codigo postal.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	private void consultarCodigoPostal(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String codPostal = request.getParameter("codigoPostal");
		EIGlobal.mensajePorTrace( "AltaNomEmplInd --> consultarColonias, codigoPostal:" + codPostal, NivelLog.INFO);
		StringBuilder respuesta = new StringBuilder();
		StringBuilder colonias = new StringBuilder();
		NomPreCodPost elemento = new NomPreCodPost();
		String codigoResultado = "";

		NomPreEmpleadoDAO empleadoDAO = new NomPreEmpleadoDAO();
		NomPreCodPost cpBean = empleadoDAO.consultarCodigoPostal(codPostal);
		if (cpBean != null && cpBean.getDetalle() != null&& cpBean.isCodExito()) {
			EIGlobal.mensajePorTrace("Existe codigo postal:" + codPostal, NivelLog.INFO);
			List<NomPreCodPost> elementos = cpBean.getDetalle();
			for (NomPreCodPost item : elementos) {
				elemento=item;
				colonias.append(item.getColonia());
				if (elementos.indexOf(item)!=(elementos.size()-1)) {
					colonias.append('|');
				}
			}
			codigoResultado = "EXITOSO";
		} else {
			EIGlobal.mensajePorTrace("No Existe Codigo Postal:" + codPostal, NivelLog.INFO);
			codigoResultado = "ERROR";
		}
		respuesta.append(codigoResultado).append(ARROBA);
		respuesta.append(elemento.getDelegacion()).append(ARROBA);
		respuesta.append(elemento.getCiudad()).append(ARROBA);
		respuesta.append(elemento.getCodAplicacion()).append(ARROBA);
		respuesta.append(colonias).append(ARROBA);
		EIGlobal.mensajePorTrace( "AltaNomEmplInd - consultarCodigoPostal: terminado ", EIGlobal.NivelLog.INFO);
		escribirRespuestaCodigoPostal(response,respuesta);
	}

	/**
	 * Enviar archivo.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	private void enviarArchivo(HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {
		final HttpSession sess = req.getSession();
		final BaseResource session = (BaseResource) req.getSession().getAttribute(SESSION_BR);
		List<Map<String,String>> listaEmpleados = (List<Map<String, String>>) sess.getAttribute(LISTA_EMPL_PARAMETRO);
		listaEmpleados = depuraLista(listaEmpleados, req.getParameterValues("empleadoSeleccion"));
		SimpleDateFormat sdf= new SimpleDateFormat("MMddyyyy", Locale.getDefault());
		StringBuilder sb = new StringBuilder();
		Date d = new Date();
		sb.append("100001E").append(sdf.format(d)).append("\n");
		Integer i = 2;
		Integer noRegistros = listaEmpleados.size();
		for (Map<String, String> empleado : listaEmpleados) {
			sb.append(generaRegistro(empleado,i));
			i++;
		}
		sb.append("3").append(rellenaCadena(i.toString(),5,"0", RELLENA_IZQUIEDA)).append(rellenaCadena(noRegistros.toString(),5,"0", RELLENA_IZQUIEDA));
		EIGlobal.mensajePorTrace("AltaNomEmplInd -> Archivo de alta individual:\n" + sb.toString(), NivelLog.INFO);
		String fileName = "alta_ind" + sdf.format(d)+session.getUserID8() + ".txt";
		String rutaArchivo = IEnlace.DOWNLOAD_PATH + fileName;
		FileWriter fw = null;
		try {
			fw= new FileWriter(new File(rutaArchivo));
			fw.write(sb.toString());
			fw.flush();
			fw.close();
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("AltaNomEmplInd -> Error al generar el archivo de alta individual:" + e.getMessage(), NivelLog.INFO);
			if(fw!=null){
				try {
					fw.close();
				} catch (IOException e1) {
					EIGlobal.mensajePorTrace("AltaNomEmplInd -> No se puede cerrar el archivo:" + e1.getMessage(), NivelLog.INFO);
				}
			}
			return;
		}
		sess.removeAttribute(LISTA_EMPL_PARAMETRO);
		EIGlobal.mensajePorTrace( "AltaNomEmplInd - enviarArchivo: terminado ", EIGlobal.NivelLog.INFO);
		getServletContext().getRequestDispatcher("/enlaceMig/NomEmpImporta?operacion=importar&Sucursal=&fileName="+fileName).include(req, res);
	}

	/**
	 * Despliega pagina error url.
	 *
	 * @param Error the error
	 * @param param1 the param1
	 * @param param2 the param2
	 * @param param3 the param3
	 * @param request the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace( "AltaNomEmplInd: despliegaPaginaErrorURL-> entra ", EIGlobal.NivelLog.DEBUG);
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
		final EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
		request.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute("Error", Error );
		request.setAttribute("MenuPrincipal", session.getStrMenu() );
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("Encabezado", CreaEncabezado( param1, param2, param3,request ) );
		request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "AltaNomEmplInd");
		EIGlobal.mensajePorTrace( "despliegaPaginaErrorURL-> sale ", EIGlobal.NivelLog.DEBUG);
		final RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}

	/**
	 * Genera menu.
	 *
	 * @param req the req
	 * @param headerVal the header val
	 */
	private void generaMenu(HttpServletRequest req, String headerVal){
		final BaseResource session = (BaseResource) req.getSession().getAttribute(SESSION_BR);
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		if (POS_MENU_ALTA_IND.equals(headerVal)) {
			req.setAttribute("Encabezado", CreaEncabezado("Alta de Empleado Individual",headerVal,ARCHIVO_AYUDA,req));
		} else {
			req.setAttribute("Encabezado", CreaEncabezado("Alta de Empleados",headerVal,ARCHIVO_AYUDA,req));
		}
	}
}