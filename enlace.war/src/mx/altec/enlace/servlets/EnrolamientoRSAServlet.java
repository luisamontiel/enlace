package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ImagenDTO;
import mx.altec.enlace.beans.PreguntaDTO;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.UtilidadesRSA;
import mx.isban.rsa.aa.ws.BindingType;
import mx.isban.rsa.aa.ws.ChallengeQuestion;
import mx.isban.rsa.aa.ws.DeviceData;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.bean.ServiciosSTUResponse;
import mx.isban.rsa.conector.servicios.ServiciosAA;
import mx.isban.rsa.conector.servicios.ServiciosSTU;
import mx.isban.rsa.stu.ws.Image;

import com.passmarksecurity.PassMarkDeviceSupportLite;

/**
 * Servlet implementation class EnrolamientoRSAServlet
 */
public class EnrolamientoRSAServlet extends BaseServlet {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**respuesta**/
	private static final String RESPUESTA = "respuesta";
	/**session**/
	private static final String SESSIONS = "session";
	/**fail**/
	private static final String FAIL = "fail";
	

	/** {@inheritDoc} */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		cargaDatosPagina(request, response);
	}

	/** {@inheritDoc} */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace("--------->  EnrolamientoRSAServlet -- doPost", EIGlobal.NivelLog.DEBUG );
		
		request.getSession().removeAttribute("listaImages");

		HttpSession sess = request.getSession();
		String valida = request.getParameter("valida");

		boolean sesionvalida = SesionValida(request, response);

		if (!sesionvalida) {
			return;
		}

		if (valida == null) {
			valida = validaToken(request, response);
		}

		if (valida != null && "1".equals(valida)) {

			
			String devicePrint = request.getParameter("devicePrinter");
			request.getSession().setAttribute("valorDeviceP", devicePrint );
	
			Map<String, String> mapa = new HashMap<String, String>();
			try {
				mapa = ejecutaRSA(request, response);
			} catch (BusinessException e) {
				request.setAttribute(RESPUESTA, FAIL);
			}
			boolean validaRSA = Boolean.valueOf(mapa.get("ValidaRSA").toString());
			String respuesta = mapa.get(RESPUESTA);
			
			if(validaRSA) {
			    if("SUCCESS".equals(respuesta)) {
			    	
			    	mandaCorreo(request);
			    	bitacorizaProceso(request, sess);
			    	
			    	request.setAttribute(RESPUESTA, "exito");
			    	
			    }		
			    
			    UtilidadesRSA.crearCookie(response, mapa.get("deviceTokenCookie"));
				request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, mapa.get("deviceTokenFSO"));
				
			} else {
				request.setAttribute(RESPUESTA, FAIL);
			}
			
			cargaPaginaSaldos(request, response);
			
		}

	}

	/**
	 * pagina para cargar los datos
	 * @param request : request
	 * @param response : response
	 * @throws ServletException : exception
	 * @throws IOException : IOException
	 */
	private void cargaDatosPagina(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace("--------->  cargaDatosPagina: enrolamiento ",EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		request.setAttribute("opcion", request.getParameter("opcion"));
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		//request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute(
						"Encabezado",
						CreaEncabezado("Enrolamiento de Usuario",
								"",
								"RSAEnrol01", request));

		UtilidadesRSA utils = new UtilidadesRSA();
		RSABean rsaBean = new RSABean();
		rsaBean = utils.generaBean(request, "", "", request.getHeader("IV-USER"));

		try {
			List<ImagenDTO> lista = utils.obtenerImagenes(rsaBean, request,response);
			List<PreguntaDTO> listaPreguntas = utils.obtenerPreguntas(rsaBean,request, response);

			request.setAttribute("listaImages", lista);
			request.getSession().setAttribute("listaImages", lista);
			request.setAttribute("listaPreguntas", listaPreguntas);

		} catch (BusinessException e) {
			EIGlobal.mensajePorTrace("--------->  Error: " + e.getMessage(),EIGlobal.NivelLog.DEBUG);
			request.setAttribute("respuesta", FAIL);
		} catch (NullPointerException e) {
			EIGlobal.mensajePorTrace("--------->  Error: " + e.getMessage(),EIGlobal.NivelLog.DEBUG);
			request.setAttribute("respuesta", FAIL);
		}

		evalTemplate("/jsp/paginaEnrolamiento.jsp", request, response);
		return;
	}
	
	/**
	 * carga pagina saldos
	 * @param request : request
	 * @param response : response
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	private void cargaPaginaSaldos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace("--------->  entra cargaPaginaSaldos: ", EIGlobal.NivelLog.DEBUG);
		
		String laDireccion = "document.location=\'csaldo1?prog=0&"+BitaConstants.FLUJO+"="+BitaConstants.EC_SALDO_CUENTA_CHEQUE +"\'";
		request.setAttribute( "plantillaElegida", laDireccion);
		request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
        return;
	
	}
	
	/**
	 * Validar Token
	 * @param request : request
	 * @param response : response
	 * @return String
	 * @throws IOException : exception
	 */
	private String validaToken(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		EIGlobal.mensajePorTrace(
						"Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP",
						EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.ENROLAMIENT0_RSA);
		//CSA-Vulnerabilidad  Token Bloqueado-Inicio
        int estatusToken = obtenerEstatusToken(session.getToken());

		if (session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& estatusToken == 1 && solVal) {
			EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", 
					EIGlobal.NivelLog.DEBUG);

			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request, "EnrolamientoRSAServlet");
			ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_ENROLAMIENTO);

		}else if(session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& estatusToken == 1 && solVal){                        
        	cierraSesionTokenBloqueado(session,request,response);
		}
		//CSA-Vulnerabilidad  Token Bloqueado-Fin 
		else {
			return "1";
		}
		return "";
	}
	
	/**
	 * @param request : request
	 */
	private void mandaCorreo(HttpServletRequest request) {
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);
		
		EmailSender sender = new EmailSender();
		EmailDetails details = new EmailDetails();
		details.setNumeroContrato(session.getContractNumber());
		details.setRazonSocial(session.getNombreContrato());
		details.setUsuario(session.getUserID8());
		sender.sendNotificacion(request,IEnlace.ENROLAMIENT0_RSA, details);
	}
	
	/**
	 * @param request : request
	 * @param sess : sess
	 */
	private void bitacorizaProceso(HttpServletRequest request, HttpSession sess) {
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			BaseResource session = (BaseResource) sess.getAttribute(SESSIONS);
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			if (request.getParameter(BitaConstants.FLUJO) != null) {
				bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO).toString());
			}else{
				request.getSession().getAttribute((BitaConstants.SESS_ID_FLUJO).toString());
			}
			
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setIdFlujo(BitaConstants.EA_ENROLAMIENTO_RSA.substring(0,4));
			bt.setNumBit(BitaConstants.EA_ENROLAMIENTO_RSA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}	

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
				
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setIdFlujo(BitaConstants.EA_CAMBIO_IMAGEN.substring(0,4));
				bt.setNumBit(BitaConstants.EA_CAMBIO_IMAGEN);
				BitaHandler.getInstance().insertBitaTransac(bt);
				
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setIdFlujo(BitaConstants.EA_CAMBIO_PREGUNTA.substring(0,4));
				bt.setNumBit(BitaConstants.EA_CAMBIO_PREGUNTA);
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("EnrolamientoRSAServlet - " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("EnrolamientoRSAServlet - " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}
	
	/**
	 * @param request : request
	 * @param response : request
	 * @return Map<String, String> mapa de resultados
	 * @exception BusinessException : exception
	 */
	private Map<String, String> ejecutaRSA(HttpServletRequest request, HttpServletResponse response) throws BusinessException {
		
		ServiciosAA siteToUser = new ServiciosAA();
		RSABean rsaBean = new RSABean();
		UtilidadesRSA utils = new UtilidadesRSA();
		ServiciosSTU siteToUserData = new ServiciosSTU();
		Map<String, String> mapa = new HashMap<String, String>();
		boolean validaRSA = true;

		String deviceTokenFSO = request.getSession().getAttribute(
				"PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO") != null
				&& "".equals(request.getSession().getAttribute(
						"PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO")
						.toString()) ? request.getSession().getAttribute(
				"PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO").toString()
				: "";

		String deviceTokenCookie = UtilidadesRSA.getCookieValue(request);

		rsaBean = utils.generaBean(request, "", "", request.getHeader("IV-USER"));

		String imagen = request.getParameter("imagenID") != null ? request.getParameter("imagenID") : "";
		String pregunta = request.getParameter("preguntaID") != null ? request.getParameter("preguntaID") : "";
		String respuesta = request.getParameter("idRespuesta") != null ? request.getParameter("idRespuesta") : "";
		String tipoDisp = request.getParameter("dispocitivo") != null ? request.getParameter("dispocitivo") : "";

		ChallengeQuestion challengeQuestionList = new ChallengeQuestion();
		challengeQuestionList.setActualAnswer(respuesta);
		challengeQuestionList.setQuestionId(pregunta);

		ChallengeQuestion[] arrayChallengeQuesitons = { challengeQuestionList };

		Image userImage = new Image();
		userImage.setImageId(imagen);
		userImage.setPath(imagen);

		DeviceData deviceData = new DeviceData();
		if ("privado".equals(tipoDisp)) {
			deviceData.setBindingType(BindingType.HARD_BIND);
		} else {
			deviceData.setBindingType(BindingType.NONE);
		}

		ServiciosSTUResponse respSTU = new ServiciosSTUResponse();
		respSTU = siteToUserData.setImage(rsaBean, userImage, UtilidadesRSA.VERSION_RSA);
		
		deviceTokenFSO = respSTU.getDeviceResult().getDeviceData().getDeviceTokenFSO();
		deviceTokenCookie = respSTU.getDeviceResult().getDeviceData().getDeviceTokenCookie();

		UtilidadesRSA.crearCookie(response, deviceTokenCookie);
		request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
		
		ServiciosAAResponse resp = new ServiciosAAResponse();
		
		String rpta = "";
		resp = siteToUser.enrollUser(rsaBean, arrayChallengeQuesitons, deviceData, UtilidadesRSA.VERSION_RSA);
		deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
		deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();

		UtilidadesRSA.crearCookie(response, deviceTokenCookie);
		request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
		rpta = resp.getDeviceResult().getCallStatus().getStatusCode();

		EIGlobal.mensajePorTrace("-->Resultado de llamada:"
				+ resp.getDeviceResult().getCallStatus().getStatusCode(), EIGlobal.NivelLog.ERROR);
		mapa.put("ValidaRSA", String.valueOf(validaRSA));
		mapa.put(RESPUESTA, rpta);
		mapa.put("deviceTokenFSO", deviceTokenFSO);
		mapa.put("deviceTokenCookie", deviceTokenCookie);
		
		return mapa;
		
	}
}
