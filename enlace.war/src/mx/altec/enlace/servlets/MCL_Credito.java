package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;

import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.CuentasBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;

import java.io.*;

//02/01/07

/**
 * The Class MCL_Credito.
 */
public class MCL_Credito extends BaseServlet
{
	
	/** The pipe. */
	String pipe="|";


	/**
	 * doGet.
	 * 
	 * @param req El HttpServletRequest obtenido de la vista
	 * @param res El HttpServletResponse obtenido de la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}


	/**
	 * doPost.
	 * 
	 * @param req El HttpServletRequest enviado a la vista
	 * @param res El HttpServletResponse enviado a la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	/**
	 * Default action.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("FSWV --> MCL_Credito --> VERSION < v.1.0 - 01012015> ", EIGlobal.NivelLog.INFO);

		String strTmp      = "";
		StringBuffer cadenaTCT   = new StringBuffer("");//CSA
		String cadenaFac   = "";
		String contrato    = "";
		String usuario     = "";
		String usuario7    = "";
		String clavePerfil = "";

		String strTipoDes   = "";
		String titulo1      = "";
		String titulo2      = "";
		String tipoRelacion = "P";
//		String strCuentas   = "";
		String tipoCuenta   = "";
		String pipe		    = "|";
		String arcAyuda	    = "";

		boolean existenCuentas = false;

		EI_Tipo Saldo      = new EI_Tipo(this);
		EI_Tipo TCTArchivo = new EI_Tipo(this);
		EI_Tipo FacArchivo = new EI_Tipo(this);
		
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
//		EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

        EIGlobal.mensajePorTrace("MCL_Credito - execute(): Entrando a Consulta de Saldos (Menu).", EIGlobal.NivelLog.INFO);

        /* Modificacion Validaci�n de facultades 20/08/2002 */
        boolean validSession = SesionValida(req,res);
		if (! validSession ) { return; }

		tipoCuenta = (String) req.getParameter("Cuenta");

		existenCuentas = false;

		if("5".equals(tipoCuenta))
		{
			titulo1 = "Consulta de Saldos de Tarjeta de Cr&eacute;dito";
			titulo2 = "Consultas &gt Saldos &gt Por Cuenta &gt Tarjeta de Cr&eacute;dito";
			strTipoDes = "Tarjetas";
			arcAyuda = "s25070h";
		} else {
			titulo1 = "Consulta de Saldos de Cr&eacute;dito";
			titulo2 = "Tesorer&iacute;a &gt Cr&eacute;dito &gt Cr&eacute;dito en L&iacute;nea &gt Consultas &gt Saldos";
			strTipoDes = "L&iacute;neas";
			arcAyuda = "s26250h";

		   //NALM
            EIGlobal.mensajePorTrace("MCL_Credito - execute():  CONSSALDOS = >" +session.getFacultad("CONSSALDOS")+ "<", EIGlobal.NivelLog.INFO);
            //EIGlobal.mensajePorTrace("MCL_Credito - execute():  TECRESALDOSC = >" +session.getFacultad("TECRESALDOSC")+ "<", EIGlobal.NivelLog.INFO);
        	//System.out.println("Validando CONSSALDOS" +session.getFacultad("CONSSALDOS"));
			if( !session.getFacultad("CONSSALDOS") )
            //if( !session.getFacultad("TECRESALDOSC") )
			{
				EIGlobal.mensajePorTrace("MCL_Credito -> Estoy fuera de CONSSALDOS",  EIGlobal.NivelLog.INFO);
				String laDireccion = "document.location='SinFacultades'";
				req.setAttribute( "plantillaElegida", laDireccion);
				req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
				return;
			}
		}

		//Variables de sesion ...
		contrato = session.getContractNumber();
		usuario = session.getUserID8();
		usuario7 = session.getUserID();
		clavePerfil = session.getUserProfile();

//		String tipo="";
		String arcLinea="";

		String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario+".amb";
		//########## Cambiar solo para pruebas
		//if(tipoCuenta.equals("0"))
		//String nombreArchivo="/tmp/rada.amb.tmp";
		
		//CAMBIO PARA LA EXPORTACION
		 
		
		EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);

		EIGlobal.mensajePorTrace("MCL_Credito - execute(): Nombre Archivo = >" +nombreArchivo+ "<", EIGlobal.NivelLog.INFO);

		// Recuperar Cuentas desde archivo.amb
		if(ArcEnt.abreArchivoLectura())
		{
			boolean noError=true;
			do
			{
				arcLinea=ArcEnt.leeLinea();				
				if(!"ERROR".equals(arcLinea))
				{
					if("ERROR".equals(arcLinea.substring(0,5).trim()))
						break;
					// Obtiene cuentas asociadas ...
					//if(arcLinea.substring(0,1).equals("2"))
					//	cadenaTCT+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
					// Obtiene facultades para cuentas ...
					if("7".equals(arcLinea.substring(0,1)))
						cadenaFac+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
				} else
				  noError=false;
			} while(noError);
			ArcEnt.cierraArchivo();

			//CSA ------------------
			CuentasBO cuentasBo = new CuentasBO();
			EIGlobal.mensajePorTrace("CSA: Consulta las cuentas del contrato :"+ contrato, EIGlobal.NivelLog.INFO);
			
						
			List<String> cuentas=cuentasBo.consultaCuentasRel(contrato);
				
			EIGlobal.mensajePorTrace("CSA: Numero de cuentas obtenidas :"+cuentas.size(), EIGlobal.NivelLog.INFO);
			
			Iterator<String> cuentasIter = cuentas.iterator();
			
			while(cuentasIter.hasNext()){				
				arcLinea = cuentasIter.next();
				if(!"ERROR".equals(arcLinea))
				 {
						if("ERROR".equals(arcLinea.substring(0,5).trim()))
						{
							break;
						}
						if("2".equals(arcLinea.substring(0,1)))
						 {
							cadenaTCT.append (arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @");
						}
					 }
					else{
						break;
					}
			}
			EIGlobal.mensajePorTrace("CSA: Termino lectura de cuentas. ", EIGlobal.NivelLog.INFO);
			//CSA -----------------
			
			EIGlobal.mensajePorTrace("FSWV CADENA FORMADA --->"+cadenaTCT.toString()+"<---", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("FSWV DESENTRAMAR CADENA  >"+cadenaFac+"<", EIGlobal.NivelLog.INFO);
			
			// Registros de lineas y tarjetas (5,0)
			TCTArchivo.iniciaObjeto(';','@',cadenaTCT.toString());//CSA
			// Registros de facultades (7)
			FacArchivo.iniciaObjeto(';','@',cadenaFac);

			// Si existen cuentas asociadas ...
			if(TCTArchivo.totalRegistros>=1)
			{
				for(int i=0;i<TCTArchivo.totalRegistros;i++)
				{
					if(TCTArchivo.camposTabla[i][4].equals(tipoCuenta.trim()))
					{
						if("5".equals(TCTArchivo.camposTabla[i][4]))
						{
							// Tarjetas de Cr�dito ...
							// Revisar facultades para cuenta[i] - Tarjeta.
							tipoRelacion = TCTArchivo.camposTabla[i][3];

							EIGlobal.mensajePorTrace("MCL_Credito -> Estoy dentro de TECRESALDOS tarjeta", EIGlobal.NivelLog.INFO);
							 
							if( ("P".equals(TCTArchivo.camposTabla[i][3].trim()) && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSSALDOS",req)) ||
							   ("T".equals(TCTArchivo.camposTabla[i][3].trim()) && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSSALDOTER",req)) )
							{
							   existenCuentas = true;
							   TCTArchivo.camposTabla[i][5] = "TARJETA CREDITO";
							   for(int a=0;a<7;a++)
								{
								  strTmp+=TCTArchivo.camposTabla[i][a];
								  strTmp+=pipe;
								}
							   strTmp+=" @";
							 }
						 }
						else
						 {
						   // Lineas de Cr�dito ...
						   // Revisar facultades para cuenta[i] - Linea.
						   //########### Modificacion incidencia
						   tipoRelacion = TCTArchivo.camposTabla[i][3];
						   if("P".equals(tipoRelacion.trim()))
							   existenCuentas=true;
   						   //if(tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1].trim(),"TECRESALDOSC",req))
						   if(tieneFacultadNueva(FacArchivo,TCTArchivo.camposTabla[i][1].trim(),"TECRESALDOSC",req) && "P".equals(tipoRelacion.trim()))
							{
 			  			   EIGlobal.mensajePorTrace("MCL_Credito -> Estoy dentro de TECRESALDOS linea", EIGlobal.NivelLog.INFO);
							  TCTArchivo.camposTabla[i][5]="LINEA CREDITO";
							  for(int a=0;a<7;a++)
							   {
								 EIGlobal.mensajePorTrace("FSW V --> MCL_Credito -> FOR TABLA <"+TCTArchivo.camposTabla[i][a]+">", EIGlobal.NivelLog.INFO); 
								 strTmp+=TCTArchivo.camposTabla[i][a];
								 strTmp+=pipe;
							   }
							  strTmp+=" @";
							}
						 }
					 }
				 }
			 }
		   Saldo.iniciaObjeto(strTmp);
		 //POST -------------
			sess.setAttribute("listaCuentas", Saldo);
			//POST ------------------ 
					   
		   
		   EIGlobal.mensajePorTrace("FSWV --> TRAMA INICIA OBJETO   >"+strTmp+"<", EIGlobal.NivelLog.INFO);
		   generaTablaConsultar(FacArchivo,Saldo,existenCuentas,req,res,titulo1,titulo2,strTipoDes,arcAyuda);
		   		   
		 }

		else
		 {
			despliegaPaginaError("Su transacci&oacute;n no puede ser atendida en este momento.", titulo1, titulo2, arcAyuda, req, res);
		 }

	   EIGlobal.mensajePorTrace("MCL_Credito - execute(): Saliendo de Consulta de Saldos (Menu).", EIGlobal.NivelLog.INFO);
	}


    /**
     * Genera tabla consultar.
     *
     * @param FacArchivo the fac archivo
     * @param Saldo the saldo
     * @param existenCuentas the existen cuentas
     * @param req the req
     * @param res the res
     * @param titulo1 the titulo1
     * @param titulo2 the titulo2
     * @param strTipoDes the str tipo des
     * @param arcAyuda the arc ayuda
     * @return the int
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public int generaTablaConsultar(EI_Tipo FacArchivo,EI_Tipo Saldo,boolean existenCuentas,HttpServletRequest req, HttpServletResponse res, String titulo1, String titulo2, String strTipoDes, String arcAyuda)
		throws ServletException, IOException
	{
		HttpSession sesse = req.getSession();
		BaseResource sessione = (BaseResource) sesse.getAttribute("session");
    	
		String strTabla  = "";
		String[] titulos = {"", "Cr&eacute;dito", "Descripci&oacute;n", "Tipo"};


		int[] datos={2,4,1,2,5};
        int[] value={7,0,1,2,3,4,5,6};

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

		//############### Modificacion incidencia
		boolean facultadTECRE = false;
		boolean lineaCredito = false;
		if( session.getFacultad("TECRESALDOSC") )
			facultadTECRE=true;
		if(!"Tarjetas".equals(strTipoDes.trim()))
			lineaCredito=true;
		
		EIGlobal.mensajePorTrace("MCL_Credito -> Facultad TECRE "+facultadTECRE, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MCL_Credito -> Tipo Cuenta linea "+lineaCredito, EIGlobal.NivelLog.INFO);

		if(Saldo.totalRegistros>=1)
		 {
			EIGlobal.mensajePorTrace("MCL_Credito -> Hay almenos un registro encontrado", EIGlobal.NivelLog.INFO);

//			***
			strTabla+=Saldo.generaTabla(titulos,datos,value);
			
			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

			req.setAttribute("strCuentas",Saldo.strCuentas);
			req.setAttribute("FacArchivo",FacArchivo.strOriginal);
			req.setAttribute("Tabla",strTabla);
			req.setAttribute("FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));

			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado(titulo1,titulo2,arcAyuda,req));
//			 TODO  BIT CU1031, CU3051 inicia flujo
			/*
			 * 02/ENE/07
			 * VSWF-BMB-I
			 */
			if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		   try{
			   BitaHelper bh = new BitaHelperImpl(req, session, sess);
			   BitaTransacBean bt = new BitaTransacBean();
			   if(req.getParameter(BitaConstants.FLUJO)!=null){
				   bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			   }else{
				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			   }
			   bt = (BitaTransacBean)bh.llenarBean(bt);
				if (((String)req.getParameter(BitaConstants.FLUJO)).
						equals(BitaConstants.EC_SALDO_CUENTA_TARJETA)){
					bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_TARJETA_ENTRA);
				}
				else{
					if (((String)req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO_ENTRA);
					}
				}
				bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				EIGlobal.mensajePorTrace(">>MCL_Credito -> generaTablaConsultar -> " + e.getMessage(),EIGlobal.NivelLog.ERROR);
			}catch(Exception e){
				EIGlobal.mensajePorTrace(">>MCL_Credito -> generaTablaConsultar -> " + e.getMessage(),EIGlobal.NivelLog.ERROR);
			}
			}
			
	        /*
			 * VSWF-BMB-F
			 */
			evalTemplate("/jsp/MCL_Credito.jsp", req, res);
		 }
		else
		 {
			EIGlobal.mensajePorTrace("MCL_Credito -> No hay registros encontrados", EIGlobal.NivelLog.INFO);

			if(lineaCredito)
			 {
				EIGlobal.mensajePorTrace("MCL_Credito -> Es linea de credito", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("MCL_Credito -> Existen Cuentas "+ existenCuentas, EIGlobal.NivelLog.INFO);

				if( (facultadTECRE && existenCuentas) || !facultadTECRE)
				 {
				   EIGlobal.mensajePorTrace("MCL_Credito -> Tiene facultad y existen registros pero no permisos", EIGlobal.NivelLog.INFO);
				   EIGlobal.mensajePorTrace("MCL_Credito -> Tiene facultad y existen registros pero no permisos", EIGlobal.NivelLog.INFO);
	
				   despliegaPaginaError("No tiene facultad.",titulo1,titulo2,arcAyuda, req, res);
				 }
				else
				  despliegaPaginaError("No se encontraron " +strTipoDes+" de Cr&eacute;dito Asociadas.",titulo1,titulo2,arcAyuda, req, res);
			 }
			else
			 {
				if(existenCuentas)
				  despliegaPaginaError("No tiene facultad.",titulo1,titulo2,arcAyuda, req, res);
				else
				  despliegaPaginaError("No se encontraron " +strTipoDes+" de Cr&eacute;dito Asociadas.",titulo1,titulo2,arcAyuda, req, res);
			 }
         }
		return 1;
	}


   /**
    * Tiene facultad.
    *
    * @param FacArchivo the fac archivo
    * @param tipoRelacion the tipo relacion
    * @param credito the credito
    * @param facultad the facultad
    * @param request the request
    * @return true, if successful
    */
   boolean tieneFacultad(EI_Tipo FacArchivo,String tipoRelacion,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_Credito - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_Credito - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
          {
            if("P".equals(tipoRelacion.trim()))
              FacultadAsociadaACuenta=true;
            else
              FacultadAsociadaACuenta=false;
          }
       }
	  EIGlobal.mensajePorTrace("MCL_Credito - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }

    /**
     * Facultad asociada.
     *
     * @param FacArchivo the fac archivo
     * @param credito the credito
     * @param facultad the facultad
     * @param signo the signo
     * @return true, if successful
     */
    boolean facultadAsociada(EI_Tipo FacArchivo,String credito, String facultad, String signo)
    {
		/*
		0 7
		1 Clave Fac
		2 Cuenta
		3 signo
		*/

       if(FacArchivo.totalRegistros>=1)
        for(int i=0;i<FacArchivo.totalRegistros;i++)
         {
           if(credito.trim().equals(FacArchivo.camposTabla[i][2].trim()))
            if(FacArchivo.camposTabla[i][1].trim().equals(facultad))
             if(FacArchivo.camposTabla[i][3].trim().equals(signo))
			  {
			    EIGlobal.mensajePorTrace("MCL_Credito - facultadAsociada(): Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
                return true;
			  }
		 }
		EIGlobal.mensajePorTrace("MCL_Credito - facultadAsociada(): Facultad asociada regresa false: Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
		return false;
	}


   /**
    * Tiene facultad nueva.
    *
    * @param FacArchivo the fac archivo
    * @param credito the credito
    * @param facultad the facultad
    * @param request the request
    * @return true, if successful
    */
   boolean tieneFacultadNueva(EI_Tipo FacArchivo,String credito,String facultad, HttpServletRequest request)
     {

	  boolean FacAmb=false;
      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_Credito - tieneFacultad(): #################################\nNo reviso ninguna facultad N\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_Credito - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
		   FacultadAsociadaACuenta=false;
       }
	  EIGlobal.mensajePorTrace("MCL_Credito - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }
}