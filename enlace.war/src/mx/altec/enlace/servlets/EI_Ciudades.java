package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.util.*;
import java.lang.reflect.*;
import java.sql.*;
import java.io.*;

public class EI_Ciudades extends BaseServlet
{
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Entrando a Catalogo de Ciudadess.", EIGlobal.NivelLog.INFO);

		String strCiudades="";
		String totales="";
		String Error="SUCCESS";
		Connection conn = null;
		PreparedStatement ciudadQuery = null;
		ResultSet ciudadResult = null;
		int totalCiudades=0;

		try {
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null)
			{
				EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			String sqlCiudad="SELECT DISTINCT(RTRIM(CIUDAD)) AS FCIUDAD, ESTADO From camb_banco_eu ORDER BY FCIUDAD";

			ciudadQuery = conn.prepareStatement(sqlCiudad);
			if(ciudadQuery == null)
			{
				EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			ciudadResult = ciudadQuery.executeQuery();
			if(ciudadResult==null)
			{
				EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}

			if(Error.equals("SUCCESS"))
			{
				String arcLinea="";
				String para1="";
				String para2="";
				String nombreArchivo=IEnlace.DOWNLOAD_PATH+"EI_Ciudades.html";

				EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
				if(ArcSal.recuperaArchivo())
				{
					arcLinea="\n<html>";
					arcLinea+="\n<head><title>Ciudades</title>";
					arcLinea+="<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>";
					arcLinea+="\n</head>";
					arcLinea+="\n<body bgcolor='#FFFFFF'>";
					arcLinea+="\n <form name=Forma>";
					arcLinea+="\n   <table border=0>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n        <th class='tittabdat'> Seleccione una ciudad  </th>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n        <td class='tabmovtex'>";
					arcLinea+="\n         <select NAME=SelCiudad size=20 class='tabmovtex'>\n";

					if(!ArcSal.escribeLinea(arcLinea))
						EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);

					while(ciudadResult.next())
					{
						//for(int i=0; i<totalCiudades; i++)
						//{
						para1=(ciudadResult.getString(1)==null)?"":ciudadResult.getString(1);
						para2=(ciudadResult.getString(2)==null)?"":ciudadResult.getString(2);
						arcLinea="\n          <option value="+para2.trim()+">"+para1+", "+para2+"</option>";
						if(!ArcSal.escribeLinea(arcLinea))
							EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
						//ciudadResult.next();
					}
					EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Se escribieron los Ciudades...", EIGlobal.NivelLog.INFO);

					while(!ArcSal.eof())
					{
						if(!ArcSal.escribeLinea("                                                                                  "))
						EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Algo salio mal eof()", EIGlobal.NivelLog.INFO);
					}
					arcLinea+="\n         </select>";
					arcLinea+="\n        </td>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n          <td><br></td>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n      <tr>";
					arcLinea+="\n        <td align=center>";
					arcLinea+="\n         <table border=0 align=center cellspacing=0 cellpadding=0>";
					arcLinea+="\n          <tr>";
					arcLinea+="\n           <td>";
					arcLinea+="\n            <a href='javascript:opener.Selecciona(3); window.close();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>";
					arcLinea+="\n           </td>";
					arcLinea+="\n          </tr>";
					arcLinea+="\n         </table>";
					arcLinea+="\n        </td>";
					arcLinea+="\n      </tr>";
					arcLinea+="\n    </table>\n </form>\n</body>\n</html>";

					if(!ArcSal.escribeLinea(arcLinea))
						EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);

					ArcSal.cierraArchivo();

					//*************************************************************
					ArchivoRemoto archR = new ArchivoRemoto();
					if(!archR.copiaLocalARemoto("EI_Ciudades.html","WEB"))
						EIGlobal.mensajePorTrace("EI_Ciudades - execute(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);
					//**************************************************************
				} else {
					strCiudades+="\n<p><br><table border=0 width=90% align=center>";
					strCiudades+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
					strCiudades+="\n<tr><th bgcolor=red><font color=white>En este momento no se pudo generar cat&aacute;logo. Por favor intente m&aacute;s tarde.<font></td></tr>";
					strCiudades+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
					strCiudades+="\n</table>";
				}
				ciudadResult.close();
			} else {
				strCiudades+="\n<p><br><table border=0 width=90% align=center>";
				strCiudades+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
				strCiudades+="\n<tr><th bgcolor=red><font color=white>Su transacci&oacute;n no puede ser atendida. Por favor intente m&aacute;s tarde.<font></td></tr>";
				strCiudades+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
				strCiudades+="\n</table>";
			}
		} catch( SQLException sqle ) {
			sqle.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
				EI_Query.cierraConexion(ciudadResult, ciudadQuery, conn);
		}
		req.setAttribute("Sciudades",strCiudades);
		EIGlobal.mensajePorTrace("EI_Ciudades - execute(): Saliendo de Catalogo de Ciudades.", EIGlobal.NivelLog.INFO);
		evalTemplate("/jsp/EI_Ciudades.jsp", req, res);
	}
}