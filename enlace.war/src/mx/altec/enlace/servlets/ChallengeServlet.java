/**
 * Isban Mexico
 *   Clase: ChallengeServlet.java
 *   Descripci�n:
 *
 *   Control de Cambios:
 *   1.0 25/06/2013 Stefanini - Creaci�n
 */
package mx.altec.enlace.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.UtilidadesChallengeRSA;
import mx.altec.enlace.utilerias.UtilidadesRSA;
import mx.isban.rsa.aa.ws.BindingType;
import mx.isban.rsa.aa.ws.ChallengeQuestion;
import mx.isban.rsa.aa.ws.DeviceData;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.conector.servicios.ServiciosAA;

import com.passmarksecurity.PassMarkDeviceSupportLite;

/**
 * Servlet implementation class CambioImagenServlet
 */
public class ChallengeServlet extends BaseServlet {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**RESPUESTA**/
	private static final String RESPUESTA = "respuesta";

	 /** {@inheritDoc} */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		validarCliente(request, response);
	}

	 /** {@inheritDoc} */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		validarCliente(request, response);
	}

	/**
	 * @param request : request
	 * @param response : response
	 * @throws ServletException : exception
	 * @throws IOException : IOException
	 */
	private void validarCliente(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		EIGlobal.mensajePorTrace(">>>>>>>> : Entra :ChallengeServlet- validarCliente" ,EIGlobal.NivelLog.INFO);

		if(Global.VALIDA_RSA) {
	    	String deviceTokenFSO = "";
		    String deviceTokenCookie = "";

	    	final UtilidadesRSA utils = new UtilidadesRSA();
	    	final ServiciosAA utilsChallenge = new ServiciosAA();
	    	ServiciosAAResponse resp = new ServiciosAAResponse();
			RSABean rsaBean = new RSABean();

			final String idPregunta = request.getParameter("idPregunta") != null ? request.getParameter("idPregunta") : "";
			final String respuesta = request.getParameter(RESPUESTA) != null ? request.getParameter(RESPUESTA) : "";
			String idSesion = request.getParameter("Sesion") != null ? request.getParameter("Sesion") : "";
			String idTransaccion = request.getParameter("Transaccion") != null ? request.getParameter("Transaccion") : "";
			//String idTransaccion = "";
			final String servletRegreso = request.getParameter("servletRegreso");

			rsaBean =  utils.generaBean(request, idSesion, idTransaccion, request.getHeader("IV-USER"));
			rsaBean.setAuthSessionId(idSesion);
			rsaBean.setAuthTransactionId(idTransaccion);
			final ChallengeQuestion question = new ChallengeQuestion();
			question.setQuestionId(idPregunta);
			question.setUserAnswer(respuesta);
			question.setActualAnswer(respuesta);
			final String txtPregunta = request.getParameter("txtPregunta") != null ? request.getParameter("txtPregunta") : "";
			question.setQuestionText(txtPregunta);
	    	final ChallengeQuestion[] payload = {question};

	    	try {
	    	resp = utilsChallenge.authenticateChallQuestion(rsaBean, payload, UtilidadesRSA.VERSION_RSA);
	    	} catch (NullPointerException e) {
	    		errorValirRSA(request, response, servletRegreso);
			}

			if(resp != null && resp.getDeviceResult() != null){
		    	deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
			    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();

			    UtilidadesRSA.crearCookie(response, deviceTokenCookie);
				request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

				if(resp.getStatusHeader().getReasonCode() != 1304 && resp.getStatusHeader().getReasonCode() != 1203) {

			    	final String estatus = resp.getCredentialAuthResultList().getChallengeQuestionAuthResult().getPayload().
			    		getAuthenticationResult().getAuthStatusCode();
			    	final String estatusUsr = resp.getIdentificationData().getUserStatus().toString();

			    	idSesion = resp.getIdentificationData().getSessionId();
			    	idTransaccion = resp.getIdentificationData().getTransactionId();
			    	final String tipoDisp = request.getParameter("tipoDisp") != null ? request.getParameter("tipoDisp") : "";

			    	evaluarRespuesta (request, response,  estatus, estatusUsr,
			                idPregunta, txtPregunta, idSesion, idTransaccion, tipoDisp, rsaBean, servletRegreso);
				} else {
					validarClienteAUX(request, response, servletRegreso);
					return;
				}
			} else {
				errorValirRSA(request, response, servletRegreso);
			}
		}
	}

	/**
	 * @param request : request
	 * @param response : response
	 * @param servletRegreso : servletRegreso
	 * @throws ServletException : ServletException
	 * @throws IOException : IOException
	 */
	private void validarClienteAUX(HttpServletRequest request, HttpServletResponse response, String servletRegreso)
	throws ServletException, IOException {
		final HttpSession ses = request.getSession();
		final BaseResource session = (BaseResource) ses.getAttribute("session");

		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pregunta Reto","", "", request));
		request.setAttribute("servletRegreso", servletRegreso);

		EIGlobal.mensajePorTrace("-------------->Intenta direccionar pagina challenge" ,EIGlobal.NivelLog.INFO);
		request.setAttribute(RESPUESTA, "TIMEOUT01");
		evalTemplate("/jsp/paginaChallenge.jsp", request, response);
		//return;
	}

	/**
	 * @param request : request
	 * @param response : response
	 * @param servletRegreso : servletRegreso
	 * @throws ServletException : ServletException
	 * @throws IOException : IOException
	 */
	private void errorValirRSA(HttpServletRequest request, HttpServletResponse response, String servletRegreso)
	throws ServletException, IOException {
		EIGlobal.mensajePorTrace(">>>>>>>> RSA no respondio ............" ,EIGlobal.NivelLog.INFO);
		request.setAttribute("challngeExito", "exito");
		
		final String params = ValidaOTP.reestableceParametrosEnSessionEncoded(request);
		final RequestDispatcher rd = request.getRequestDispatcher(servletRegreso + "?" + params+"&challngeExito=SUCCESS" );

		rd.forward(request, response);
		//return;
	}

	 /**
     * @param req : request
     * @param res : response
     * @param estatus : estatus
     * @param estatusUsr : estatus usuario
     * @param idPregunta : pregunta
     * @param txtPregunta : texto pregunta
     * @param idSesion : sesion
     * @param idTransaccion : transaccion
     * @param tipoDisp : tipo de dispositivo
     * @param rsaBean : rsaBean
     * @param servletRegreso : servletRegreso
	 * @throws IOException
	 * @throws ServletException
     */
    public void evaluarRespuesta (HttpServletRequest req,
            HttpServletResponse res,  String estatus, String estatusUsr,
            String idPregunta, String txtPregunta, String idSesion,
            String idTransaccion, String tipoDisp, RSABean rsaBean,
            String servletRegreso)
    throws ServletException, IOException{

    	EIGlobal.mensajePorTrace(">>>>>>>> : Entra :ChallengeServlet- valor > evaluarRespuesta" ,EIGlobal.NivelLog.INFO);

    	final HttpSession ses = req.getSession();
		final BaseResource session = (BaseResource) ses.getAttribute("session");

    	if("FAIL".equals(estatus)) {
			if("LOCKOUT".equals(estatusUsr)) {
				siLockOut(req, res, ses);
			}
			else {
				EIGlobal.mensajePorTrace(">>>> intentar pregunta de nuevo" ,EIGlobal.NivelLog.INFO);
				final ChallengeQuestion questionUsr = new ChallengeQuestion();
				questionUsr.setQuestionId(idPregunta);
				questionUsr.setQuestionText(txtPregunta);

				EIGlobal.mensajePorTrace("Pregunta   " + questionUsr.getQuestionText() ,EIGlobal.NivelLog.INFO);
				req.setAttribute("sessionID", idSesion);
				req.setAttribute("transactionID", idTransaccion);
				req.setAttribute("preguntaUser", questionUsr.getQuestionText());
				req.setAttribute("preguntaUserID", questionUsr.getQuestionId());
				req.setAttribute("servletRegreso", servletRegreso);
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Pregunta Reto","", "", req));
				req.setAttribute("tipoDisp", tipoDisp);
				req.setAttribute(RESPUESTA, "ERR000");

				int referenciaBit = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
				UtilidadesRSA.bitacoraTCT(req, UtilidadesRSA.CVE_CHALLENGE, referenciaBit, "CORR0001", UtilidadesRSA.DES_CHALLENGE);
				UtilidadesRSA.bitacorizaTransac(req, UtilidadesRSA.CVE_CHALLENGE, UtilidadesRSA.RECHAZADO,referenciaBit);
				evalTemplate("/jsp/paginaChallenge.jsp", req, res);
			}
			return;
		}

		if("SUCCESS".equals(estatus)) {
			EIGlobal.mensajePorTrace(">>>> estado del usuario SUCCESS.....Template a direccionar" + servletRegreso ,EIGlobal.NivelLog.INFO);

			final ServiciosAA siteToUser = new ServiciosAA();
			rsaBean.setAuthSessionId(idSesion);
	    	rsaBean.setAuthTransactionId(idTransaccion);

			ServiciosAAResponse resp = new ServiciosAAResponse();

			final DeviceData deviceData = new DeviceData();
			if("privado".equals(tipoDisp)){
				deviceData.setBindingType(BindingType.HARD_BIND);
				resp = siteToUser.updateDevice(rsaBean, deviceData, UtilidadesRSA.VERSION_RSA);

				final String deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
				final String deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();

				UtilidadesRSA.crearCookie(res, deviceTokenCookie);
				req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
			}
			req.setAttribute("challngeExito", "SUCCESS");
			final String params = ValidaOTP.reestableceParametrosEnSessionEncoded(req);
			
			EIGlobal.mensajePorTrace("-->ChallegeServlet template hacia challenge " + servletRegreso + "?" + params ,EIGlobal.NivelLog.DEBUG);

			req.setAttribute("challngeExito", "SUCCESS");
			req.getSession().setAttribute("valTokenRSA", "1");
			EIGlobal.mensajePorTrace(">>>> valor forma transacciones" + getFormParameter(req,"ventana") ,EIGlobal.NivelLog.INFO);

			int referenciaBit = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
			UtilidadesRSA.bitacoraTCT(req, UtilidadesRSA.CVE_CHALLENGE, referenciaBit, "RSA_0000", UtilidadesRSA.DES_CHALLENGE);
			UtilidadesRSA.bitacorizaTransac(req, UtilidadesRSA.CVE_CHALLENGE, UtilidadesRSA.ACEPTADO,referenciaBit);
			//evalTemplate("/jsp/paginaChallenge.jsp", req, res);
			final RequestDispatcher rd = req.getRequestDispatcher(servletRegreso + "?" + params );
			rd.forward(req, res);
		}
    }

    /**
     * Efectua direccion si existe LOCKOUT
     * @param req : req
     * @param res : res
     * @param ses : ses
     * @throws ServletException : exception
     * @throws IOException : exception
     */
    public void siLockOut(HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException, IOException {
    	EIGlobal.mensajePorTrace(">>>> estado del usuario LOCKOUT" ,EIGlobal.NivelLog.INFO);

		final UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
		utilidadesRSA.mandarCorreo("LOCKOUT", req);

		int referenciaBit = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		UtilidadesRSA.bitacoraTCT(req, UtilidadesRSA.CVE_REVIEW, referenciaBit, "RSA_0000", UtilidadesRSA.DES_REVIEW);
		UtilidadesRSA.bitacorizaTransac(req, UtilidadesRSA.CVE_REVIEW, "",referenciaBit);
		
		req.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"La situaci&oacute;n de su usuario est&aacute; siendo analizada por el banco, cualquier duda favor de comunicarse a S&uacute;per L&iacute;nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000.\", 13);" );
		evalTemplate ( IEnlace.LOGIN, req, res );
		ses.invalidate();
		//return;
    }
}