package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ListIterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.beans.NomPreCodPost;
import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1E;
import mx.altec.enlace.beans.NomPreLM1F;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.NomPreEmpleadoDAO;
import mx.altec.enlace.dao.NomPreEstadosDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * @author bmesquitillo
 * Fecha: 26/junio/2009
 * proyecto: 200813700 Nomina Prepago
 * Descripcion: Servlet para modificacion de datos demograficos.
 */
 public class NomPreModificaEmpleado extends BaseServlet {
	private final static String MOD_DEMOGRAFICO_INICIA="inicia";
	private final static String MOD_DEMOGRAFICO_CONS="consulta";
	private final static String MOD_DEMOGRAFICO_EJECUTA="ejecuta";
	private final static String MOD_DEMOGRAFICO_CARGACP="cargaCP"; //VSWF ESC nuevos capos NP

	public NomPreModificaEmpleado() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}
	protected void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("Entra NomPreModificaEmpleado", NivelLog.INFO);
		String accion = request.getParameter("opcion");

		accion = accion == null || accion.length() == 0 ? MOD_DEMOGRAFICO_INICIA : accion;
		boolean sesionvalida = SesionValida( request, response );
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");

		if(sesionvalida)
		{
			String strMenu =  (session.getstrMenu());
			strMenu =  (strMenu != null ? strMenu : "");
			request.setAttribute("MenuPrincipal", strMenu);
			request.setAttribute("newMenu", session.getFuncionesDeMenu());
			request.setAttribute("Encabezado", CreaEncabezado( "Edici&oacute;n de datos demogr&aacute;ficos","Servicios &gt; Tarjeta de Pagos Santander &gt; Asignaci&oacute;n tarjeta-empleado &gt; Consulta &gt; Datos demogr&aacute;ficos","s25320h",request));//YHG NPRE

			//CGH Valida servicio de nomina
			if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
				return;
			}

			if(accion.trim().equals(MOD_DEMOGRAFICO_INICIA)){
				inicia(request, response,session);
			}else if(accion.trim().equals(MOD_DEMOGRAFICO_EJECUTA)){
				ejecuta(request, response,session);
			//Inicio VSWF ESC nuevos campos NP
			} else if (accion.trim().equals(MOD_DEMOGRAFICO_CARGACP)){
				cargaCodPost(request, response,session);
			}
			//Fin VSWF ESC nuevos campos NP

		}else
			{
				request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response);
			}
	}


	/**
	 * @author Berenice Mesquitillo
	 *  Metodo inicial, carja el jsp inicial ya sea para la modificacion demografica
	 * @param request
	 * @param response
	 * Fecha: 26-06-2009
	 */
	private void inicia(HttpServletRequest request, HttpServletResponse response,BaseResource session){
		EIGlobal.mensajePorTrace("NomPreModificaEmpleado.class>>inicia()>Entra", NivelLog.INFO);
		String cadenaEmp=request.getParameter("selempleado");
		EIGlobal.mensajePorTrace("NomPreModificaEmpleado.class>>inicia()>cadenaEmp:"+cadenaEmp, NivelLog.INFO);
		String operConfirma=null;
		String numEmpleado="";
		String numTarjeta="";
		String codOperBit=null;
		String [] arrEmpl=null;
		if(cadenaEmp!=null){
			arrEmpl=cadenaEmp.split("[|]");
			numEmpleado=arrEmpl[0];
			numTarjeta=arrEmpl[1];
		}

		NomPreEmpleadoDAO empleadoDAO=new NomPreEmpleadoDAO();
		NomPreLM1F demografico;
		try {
			demografico=empleadoDAO.consultarDatosDemograficos(session.getContractNumber(), numEmpleado, numTarjeta);
			EIGlobal.mensajePorTrace("demografico>>"+demografico+"<<", NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("demografico.empleado>>"+demografico.getEmpleado()+"<<", NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("demografico.tarjeta>>"+demografico.getTarjeta()+"<<", NivelLog.DEBUG);
			if(demografico!=null && demografico.isCodExito()){
				if(demografico.getEmpleado()!=null && demografico.getTarjeta()!=null){
					request.setAttribute("numempleado",numEmpleado);
					request.setAttribute("numtarjeta",numTarjeta);
					request.setAttribute("numId",demografico.getEmpleado().getNumID());//VSWF ESC nuevos campos NP
					request.setAttribute("pais",demografico.getEmpleado().getPais());//VSWF ESC nuevos campos NP
					request.setAttribute("dirCalleEmp",  demografico.getEmpleado().getCalle());
					request.setAttribute("dirNumEmp",demografico.getEmpleado().getNumExt());//VSWF ESC nuevos campos NP
					request.setAttribute("dirNumIntEmp",demografico.getEmpleado().getNumInt());//VSWF ESC nuevos campos NP
					request.setAttribute("dirColoniaEmp","<option CLASS='tabmovtex'>" +
							demografico.getEmpleado().getColonia() + "</option> \n");//VSWF ESC nuevos campos NP
					request.setAttribute("dirCodPosEmp",demografico.getEmpleado().getCodigoPostal());
					request.setAttribute("dirDelegacionEmp",demografico.getEmpleado().getDelegacion());
					request.setAttribute("dirCiudadEmp",demografico.getEmpleado().getCiudad());
					request.setAttribute("dirEstadoEmp",demografico.getEmpleado().getEstado());
					request.setAttribute("dirLadaEmp",demografico.getEmpleado().getLada());
					request.setAttribute("dirTelefonoEmp",demografico.getEmpleado().getTelefono());
					request.setAttribute("dirEstados",NomPreEstadosDAO.consultaEstados());
					codOperBit=demografico.getCodigoOperacion();
					operConfirma="consultaOK";
				}
			}
			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_DEMOGRAFICOS);
			if(numTarjeta!=null)
				bt.setCctaOrig(numTarjeta);
			if(numEmpleado!=null)
				bt.setBeneficiario(numEmpleado);

			NomPreUtil.bitacoriza(request,"LM1F",codOperBit, bt);

			request.setAttribute("operconfirma",operConfirma);
			evalTemplate(IEnlace.NOMPRE_MODIF_EMP , request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "consultaBloqueo" + " -Termina",EIGlobal.NivelLog.INFO);

	}

	/**
	 * @author Berenice Mesquitillo
	 * Metodo que realiza la modificacion de los datos demograficos
	 * @param request
	 * @param response
	 * Fecha:26-06-2009
	 */
	private void ejecuta(HttpServletRequest request, HttpServletResponse response,BaseResource session){
		try {

			EIGlobal.mensajePorTrace("Modificacion de codigo postal ejecuta", NivelLog.INFO);
			System.out.println("Modificacion de codigo postal ejecuta"  );

			//Inicio VSWF ESC nuevos campos NP
			String cuadroDialogo=null;
			NomPreEmpleadoDAO empleadoDAO=new NomPreEmpleadoDAO();
			NomPreEmpleado empleado=new NomPreEmpleado();
			EIGlobal.mensajePorTrace("Antes de recuperar parametros", NivelLog.DEBUG);
			empleado.setNoEmpleado((String)request.getParameter("numempleado"));
			String codPostal = (String)request.getParameter("numId");
			if(codPostal == null || codPostal.equals("")){
				empleado.setNumID("0000000000000000000000000");
			} else {
				empleado.setNumID((String)request.getParameter("numId"));
			}
			String selPais = (String)request.getParameter("pais");
			if(selPais == null || selPais.equals("")){
				EIGlobal.mensajePorTrace("pais " + codPostal, NivelLog.DEBUG);
				empleado.setPais("MEXICO");
			} else {
				EIGlobal.mensajePorTrace("pais else " + codPostal, NivelLog.DEBUG);
				empleado.setPais((String)request.getParameter("pais"));
			}

			empleado.setCalle((String)request.getParameter("dirCalleEmp"));
			empleado.setNumExt((String)request.getParameter("dirNumEmp"));
			empleado.setNumInt((String)request.getParameter("dirNumIntEmp"));
			empleado.setColonia((String)request.getParameter("dirColoniaEmp"));
			empleado.setCodigoPostal((String)request.getParameter("dirCodPosEmp"));
			empleado.setDelegacion((String)request.getParameter("dirDelegacionEmp"));
			empleado.setCiudad((String)request.getParameter("dirCiudadEmp"));
			empleado.setEstado((String)request.getParameter("dirEstadoEmp"));
			empleado.setLada((String)request.getParameter("dirLadaEmp"));
			empleado.setTelefono((String)request.getParameter("dirTelefonoEmp"));
			//Fin VSWF ESC nuevos campos NP

			String numTarjeta=(String)request.getParameter("numtarjeta");



			String postal = request.getParameter("dirCodPosEmp");
			NomPreCodPost npcodpostal = empleadoDAO.consultarCodigoPostal(postal);

			String operConfirma=null;
			String codErrBit=" ";
			if(npcodpostal!=null && npcodpostal.isCodExito() && npcodpostal.getDetalle() != null){
				EIGlobal.mensajePorTrace("Existe Codigo Postal codigo postal " + postal , NivelLog.DEBUG);
				System.out.println("Existe Codigo Posta codigo postal " + postal );
				EIGlobal.mensajePorTrace("Antes de actualizaDatosDemograficos", NivelLog.DEBUG);
				NomPreLM1E npLM1E=empleadoDAO.actualizaDatosDemograficos(session.getContractNumber(), numTarjeta, empleado);
				EIGlobal.mensajePorTrace("Despues de actualizaDatosDemograficos", NivelLog.DEBUG);
				if(npLM1E!=null && npLM1E.isCodExito()){
					operConfirma="ejecutaOK";
					cuadroDialogo="cuadroDialogo(\"Se han realizado los cambios demogr&aacute;ficos al empleado: "+empleado.getNoEmpleado()+"\", 1)";
					try
					{
						int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));

						if(npLM1E!=null && npLM1E.getCodigoOperacion()!=null && !npLM1E.getCodigoOperacion().trim().equals("")){
							codErrBit=npLM1E.getCodigoOperacion();
						}
						String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID8()," ",0.0,NomPreUtil.ES_NP_TARJETA_CONSULTA_MODIFICA_DEMOGRAFICOS_OPER," ",0," ");
					}catch(Exception e){
						EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
					}
				}else{
					cuadroDialogo="cuadroDialogo(\"No se han podido realizar los cambios demogr&aacute;ficos al empleado: "+empleado.getNoEmpleado()+"\", 1)";
				}

			}
			else{
				operConfirma="ejecutaOK";
				EIGlobal.mensajePorTrace("No Existe Codigo Posta codigo postal " + postal , NivelLog.DEBUG);
				System.out.println("No Existe Codigo Posta codigo postal " + postal );
				cuadroDialogo="cuadroDialogo(\"No existe c&oacute;digo postal: " + postal+ "\", 1)";
			}

			String numEmp=empleado.getNoEmpleado();

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_MODIFICA_DEMOGRAFICOS);
			if(numTarjeta!=null)
				bt.setCctaOrig(numTarjeta);
		    if(numEmp!=null)
				bt.setBeneficiario(numEmp);

			NomPreUtil.bitacoriza(request,"LM1E",codErrBit, bt);


			request.setAttribute("numempleado",empleado.getNoEmpleado());
			request.setAttribute("numtarjeta",numTarjeta);
			request.setAttribute("dirCalleEmp",empleado.getCalle());
			request.setAttribute("dirNumEmp",empleado.getNumExt());//VSWF ESC nuevos campos NP
			request.setAttribute("dirNumIntEmp",empleado.getNumInt());//VSWF ESC nuevos campos NP
			request.setAttribute("dirColoniaEmp","<option CLASS='tabmovtex'>" +
						empleado.getColonia() + "</option> \n");//VSWF ESC nuevos campos NP
			request.setAttribute("dirCodPosEmp",empleado.getCodigoPostal());
			request.setAttribute("dirDelegacionEmp",empleado.getDelegacion());
			request.setAttribute("dirCiudadEmp",empleado.getCiudad());
			request.setAttribute("dirEstadoEmp",empleado.getEstado());
			request.setAttribute("dirLadaEmp",empleado.getLada());
			request.setAttribute("dirTelefonoEmp",empleado.getTelefono());
			request.setAttribute("dirEstados",NomPreEstadosDAO.consultaEstados());
			request.setAttribute("operconfirma",operConfirma);
			request.setAttribute("mensajeDem",cuadroDialogo);

			EIGlobal.mensajePorTrace("NomPreModificaEmpleado Despues de asignar parametros", NivelLog.DEBUG);

			evalTemplate(IEnlace.NOMPRE_MODIF_EMP, request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "inicia" + " -Termina",EIGlobal.NivelLog.INFO);

	}

	/**
	 * @author VSWF ESC
	 *  Metodo cargaCodPost, busca de acuerdo al codigo postal capturado las colonias, delegacion, ciudad
	 *  y el estado para cargarlos en el jsp.
	 * @param request
	 * @param response
	 * Fecha: 25-03-2010
	 */
	private void cargaCodPost(HttpServletRequest request, HttpServletResponse response,BaseResource session){
		EIGlobal.mensajePorTrace("NomPreModificaEmpleado.class>>cargaCodPost()>Entra", NivelLog.INFO);

		EIGlobal.mensajePorTrace("Modificacion de codigo postal cargaCodPost", NivelLog.INFO);
		System.out.println("Modificacion de codigo postal cargaCodPost"  );


		String operConfirma=null;
		String numEmpleado="";
		String numTarjeta="";
		String codOperBit=null;
		String postal = (String)request.getParameter("dirCodPosEmp");
		String colonias = "";
		String delegacion = "";
		String ciudad = "";
		String estado = "";
		try {
				NomPreEmpleadoDAO empleadoDAO=new NomPreEmpleadoDAO();
				NomPreCodPost npcodpostal = empleadoDAO.consultarCodigoPostal(postal);
				if(npcodpostal!=null && npcodpostal.isCodExito() && npcodpostal.getDetalle() != null){

					EIGlobal.mensajePorTrace("Existe Codigo Posta codigo postal " + postal , NivelLog.INFO);
					System.out.println("Existe Codigo Posta codigo postal " + postal );
					ListIterator li = npcodpostal.getDetalle().listIterator();

					while (li.hasNext()){
						NomPreCodPost npCodPost = (NomPreCodPost) li.next();
						colonias += "<option CLASS='tabmovtex'>" + npCodPost.getColonia() + "</option> \n";
						delegacion = npCodPost.getDelegacion();
						ciudad = npCodPost.getCiudad();
						estado = npCodPost.getEstado();
					}
				}	else {

					request.setAttribute("mensajeDem","cuadroDialogo(\"No existe c&oacute;digo postal: " + postal+ "\", 1)");
					EIGlobal.mensajePorTrace("No Existe Codigo Posta codigo postal " + postal , NivelLog.INFO);
					System.out.println("No Existe Codigo Posta codigo postal " + postal );
					colonias += "<option CLASS='tabmovtex'>" + (String)request.getParameter("dirColoniaEmp") + "</option> \n";
					delegacion = (String)request.getParameter("dirDelegacionEmp");
					ciudad = (String)request.getParameter("dirCiudadEmp");
					estado = (String)request.getParameter("dirEstadoEmp");
				}
				request.setAttribute("numempleado",(String)request.getParameter("numempleado"));
				request.setAttribute("numtarjeta",(String)request.getParameter("numtarjeta"));
				request.setAttribute("numId",(String)request.getParameter("numId"));
				request.setAttribute("dirCalleEmp",(String)request.getParameter("dirCalleEmp"));
				request.setAttribute("dirNumEmp",(String)request.getParameter("dirNumEmp"));
				request.setAttribute("dirNumIntEmp",(String)request.getParameter("dirNumIntEmp"));
				request.setAttribute("dirColoniaEmp",colonias);
				request.setAttribute("dirCodPosEmp",postal);
				request.setAttribute("dirDelegacionEmp",delegacion);
				request.setAttribute("dirCiudadEmp",ciudad);
				request.setAttribute("dirEstadoEmp",estado);
				request.setAttribute("dirLadaEmp",(String)request.getParameter("dirLadaEmp"));
				request.setAttribute("dirTelefonoEmp",(String)request.getParameter("dirTelefonoEmp"));
				request.setAttribute("dirEstados",NomPreEstadosDAO.consultaEstados());
				codOperBit=npcodpostal.getCodigoOperacion();
				operConfirma="consultaOK";

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_DEMOGRAFICOS);
			if(numTarjeta!=null)
				bt.setCctaOrig(numTarjeta);
			if(numEmpleado!=null)
				bt.setBeneficiario(numEmpleado);

			NomPreUtil.bitacoriza(request,"LM1F",codOperBit, bt);

			request.setAttribute("operconfirma",operConfirma);
			evalTemplate(IEnlace.NOMPRE_MODIF_EMP , request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "consultaBloqueo" + " -Termina",EIGlobal.NivelLog.INFO);

	}
}