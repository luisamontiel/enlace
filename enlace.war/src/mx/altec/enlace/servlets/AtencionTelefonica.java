package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class AtencionTelefonica extends BaseServlet{

 public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String   strVentana;
	    EIGlobal.mensajePorTrace( "***AtencionTelefonica.class & Entre a AtencionTelefonica &", EIGlobal.NivelLog.INFO);

         if(SesionValida(req, res))
           {
			  presentaPagina(req, res);
		   }
    }

	public int presentaPagina(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");


	    req.setAttribute("newMenu",	       session.getFuncionesDeMenu());
        req.setAttribute("Encabezado",	   CreaEncabezado("Atenci&oacute;n telef&oacute;nica de Enlace Internet", "Atenci&oacute;n telef&oacute;nica", "s26360h", req));
	    req.setAttribute("MenuPrincipal",  session.getStrMenu());
		req.setAttribute("nombreUsuario",  session.getNombreUsuario());

		evalTemplate("/jsp/AtencionTelefonica.jsp", req, res);

		return 1;

	}


	public String CreaEncabezado( String tituloPantalla, String posicionMenu, String ClaveAyuda, HttpServletRequest req) {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String strEncabezado="";
		String v_contrato="";
		String tit_contrato="";

		if ( session.getContractNumber() != null ) {
			tit_contrato = "Contrato:";
			v_contrato = ObtenContUser(req);
			EIGlobal.mensajePorTrace("#############-----------------@@@@@@@@@---->"+v_contrato, EIGlobal.NivelLog.INFO);

		}
		StringBuffer strEnc = new StringBuffer();

		strEnc.append("<table width=");
		strEnc.append("\"760\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		strEnc.append("\n<tr>\n");
		strEnc.append("<td width=\"676\" valign=\"top\" align=\"left\">\n");
		strEnc.append("<table width=\"666\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td width=\"528\" valign=\"top\" class=\"titpag\">\n");
		strEnc.append(tituloPantalla);
		strEnc.append("</td>\n");
		strEnc.append("<td width=\"120\" align=\"right\" valign=\"bottom\" class=\"texencfec\">\n");
		strEnc.append(ObtenFecha());
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td valign=\"top\" class=\"texencrut\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gic25030.gif\" width=\"7\" height=\"13\"> \n");
		strEnc.append(posicionMenu);
		strEnc.append("</td>\n");
		strEnc.append("<td class=\"texenchor\" align=\"right\" valign=\"top\">\n");
		strEnc.append(ObtenHora());
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");
		strEnc.append("</td>\n");
		strEnc.append( "<td width=\"40\" valign=\"top\"><a href=\"javascript:FrameAyuda('" );
		strEnc.append( ClaveAyuda );
		strEnc.append("');\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('ayuda','','/gifs/EnlaceMig/gbo25171.gif',1)\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gbo25170.gif\" width=\"33\" height=\"49\" name=\"ayuda\" border=\"0\"></a></td>\n");
		strEnc.append("<td width=\"44\" valign=\"top\"><a href=\"logout\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gbo25180.gif\" width=\"44\" height=\"49\" name=\"finsesion\" border=\"0\"></a></td>\n\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");
//Contrato
		/*strEnc.append("<table width=\"760\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td align=\"center\" valign=\"top\" class=\"texenccon\"><span class=\"texencconbol\">");
		strEnc.append(tit_contrato);
		strEnc.append("</span>\n");
		strEnc.append(v_contrato);
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");*/

		strEnc.append("<Script language = \"JavaScript\">\n");
		strEnc.append("<!--\n");
		strEnc.append("function FrameAyuda(ventana){\n");
		strEnc.append("hlp=window.open(\"/EnlaceMig/\"+ventana+\".html\" ,\"hlp\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450\");\n");
		strEnc.append("hlp.focus();\n");
		strEnc.append("}\n");
		strEnc.append("//-->\n");
		strEnc.append("</Script>\n");
		strEncabezado= strEnc.toString();

		return strEncabezado;

	}

}

