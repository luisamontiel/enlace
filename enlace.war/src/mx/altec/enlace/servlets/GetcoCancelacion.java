package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



/**
** coEstadisticas is a blank servlet to which you add your own code.
*/
public class GetcoCancelacion extends BaseServlet
{

    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }
    public void displayMessage(HttpServletRequest req,
                      HttpServletResponse res,
                      String messageText)
                    throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println(messageText);
    }
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException
    {

		boolean sesionvalida = SesionValida( req, res );
		// servidor para facultar a Consultas
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
		if ( sesionvalida ) {

			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu",       session.getFuncionesDeMenu());
			req.setAttribute( "ContUser",      ObtenContUser(req));
			req.setAttribute( "Fecha",         ObtenFecha() );

			String Arch = req.getParameter("Arch");
			req.setAttribute("Arch", Arch);
			String Reg = req.getParameter("Reg");
			req.setAttribute("Reg", Reg);
			String fechaIni = req.getParameter("fecha1");
			req.setAttribute("fecha1", fechaIni);
			String fechaFin = req.getParameter("fecha2");
			req.setAttribute("fecha2", fechaFin);
			String Estatus = req.getParameter("Estatus");
			String TDoc = req.getParameter("TDoc");
			boolean Facu = session.getFacultad(session.FAC_CCCANCELPAG_CONF);
			req.setAttribute("facu",""+Facu);
			boolean facuEx = session.getFacultad(session.FAC_CCCANCELPAG_CONF);
			req.setAttribute("facuEx", ""+facuEx);
			String ArchivoEx = session.getUserID();
			req.setAttribute("nombre2", ArchivoEx);
			String Prov = req.getParameter("Proveedor");
			if(Prov!=null){req.setAttribute("Proveedor", Prov);}
			String importe = req.getParameter("Importe");

			if(importe==null || importe.equals("")) {req.setAttribute("Importe","-1");}
			else {req.setAttribute("Importe", importe);}

			String Eleccion = "";
			String trama1_salida= "";
			String trama2_salida="";
			String resultado = "";
			String mensaje = "";
			String nombreArchivo ="";
			String registroLeido = "";
			String EstatusDoc ="";

			Vector sta = new Vector();
			Vector temporal = new Vector();
			Vector temporal1 = new Vector();
			Vector lista= new Vector();
			Vector listaVar = new Vector();
			Vector l1 = new Vector();
			Vector l2 = new Vector();
			Vector l3 = new Vector();

			File ArchivoResp1;

			int primero = 0;
			int ultimo = 0;
			int salida = 0;

			//Consulta por arcihvo:
			EIGlobal.mensajePorTrace("##########----> Opcion de Consulta >----------- "+Reg, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("##########----> Facultad de Cancelacion >----------- "+Facu, EIGlobal.NivelLog.INFO);
			if (Reg.equals("1"))
			{
				String secuencia="";
                String nombre="";
				String trama1="";
				//TCT|1001851|CFAE|80000648376|1001851|1001851A|1@161@|
				secuencia = Arch;

				try{
					if (Estatus==null){

						EIGlobal.mensajePorTrace("*********Lista Estatus Vacia********--> ", EIGlobal.NivelLog.INFO);
						int tamTemp2 = 0;
						String tam2 = Integer.toString(tamTemp2);
						req.setAttribute("tamListaEstatus", tam2);
						EIGlobal.mensajePorTrace("*********Mandando valor de 0 ********-->"+tam2, EIGlobal.NivelLog.INFO);
					}
					else{
						EIGlobal.mensajePorTrace("*********Lista Estatus NO Vacia********--> ", EIGlobal.NivelLog.INFO);
						String[] listEstatusDoc = req.getParameterValues("Estatus");
						int tamLista2 = listEstatusDoc.length;
						String tam2 = Integer.toString(tamLista2);
						EIGlobal.mensajePorTrace("*********Mandando Tama�o********--> ", EIGlobal.NivelLog.INFO);
						req.setAttribute("tamListaEstatus", tam2);

						for (int i=0;i<listEstatusDoc.length; i++) {
	   						EIGlobal.mensajePorTrace("Checkbox marcado con " + listEstatusDoc[i]  + " en su posicion" +i, EIGlobal.NivelLog.INFO);
						}
						EIGlobal.mensajePorTrace("*********Mandando Valores de Estatus********--> ", EIGlobal.NivelLog.INFO);
							if(tamLista2==1){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
							}
							else if(tamLista2==2){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);}
							else if(tamLista2==3){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);}
							else if(tamLista2==4){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);}
							else if(tamLista2==5){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);
								req.setAttribute("Estatus5", listEstatusDoc[4]);}
							else if(tamLista2==6){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);
								req.setAttribute("Estatus5", listEstatusDoc[4]);
								req.setAttribute("Estatus6", listEstatusDoc[5]);}
							else if(tamLista2==7){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);
								req.setAttribute("Estatus5", listEstatusDoc[4]);
								req.setAttribute("Estatus6", listEstatusDoc[5]);
								req.setAttribute("Estatus7", listEstatusDoc[6]);}

							EstatusDoc = listEstatusDoc[0];
					}


						//TCT|1001851|CFAE|80000648376|1001851|1001851A|1@161@|
						trama1 = "2EWEB|"+session.getUserID()+"|CFAE|"+session.getContractNumber()+"|"+session.getUserID()+"|"+
						session.getUserProfile()+"|1@"+secuencia+"@|";
						EIGlobal.mensajePorTrace( "***GetcoCancelacion.class  trama de salida 1.0 >>"+trama1+"<<", EIGlobal.NivelLog.DEBUG);

						ServicioTux tuxGlobal = new ServicioTux();
						//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
						String ArchivoResp = "";
						Hashtable ht = tuxGlobal.web_red(trama1);
						String Buffer = (String) ht.get("BUFFER");

						nombreArchivo=Buffer.substring(Buffer.lastIndexOf('/')+1,Buffer.length());

						ArchivoRemoto archivinR = new ArchivoRemoto();

						if(!archivinR.copia(nombreArchivo))
								EIGlobal.mensajePorTrace( "COCancelacion - consulta: No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
						else{
								EIGlobal.mensajePorTrace( "COCancelacion - consulta: Copia remota OK.", EIGlobal.NivelLog.INFO);
								//EIGlobal.mensajePorTrace("paso", EIGlobal.NivelLog.INFO);
							}

						nombreArchivo=Global.DIRECTORIO_LOCAL + "/" +nombreArchivo;

                        File nombreArch = new File(nombreArchivo);
                        File temporalito = new File(Global.DIRECTORIO_LOCAL + "/" +session.getUserID()+".tramas");
                        if(nombreArch.renameTo(temporalito))	{
							EIGlobal.mensajePorTrace("***********Archivo respuesta con exito", EIGlobal.NivelLog.INFO);
							nombre = Global.DIRECTORIO_LOCAL + "/" +session.getUserID()+".tramas";
                        }else {
							EIGlobal.mensajePorTrace( "****Error al renombrar archivo de tramas ", EIGlobal.NivelLog.INFO);
							nombre = nombreArchivo;
                        }
						req.setAttribute("nombre", nombre);
                        EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraConcentracion(): Path NombreArchivo."+nombre, EIGlobal.NivelLog.INFO);


						if (TDoc==null){

							  EIGlobal.mensajePorTrace("*********Lista Vacia ********--> ", EIGlobal.NivelLog.INFO);
							  int tamTemp = 0;
							  String tam = Integer.toString(tamTemp);
							  req.setAttribute("tamLista", tam);
							  EIGlobal.mensajePorTrace("*********Mandando valor de 0 ********-->"+tam, EIGlobal.NivelLog.INFO);
						}
						else {

							String[] listEstatus = req.getParameterValues("TDoc");
							int tamLista = listEstatus.length;
							String tam = Integer.toString(tamLista);
							req.setAttribute("tamLista", tam);

								for (int i=0;i<listEstatus.length; i++) {
	   									EIGlobal.mensajePorTrace("Checkbox marcado con " + listEstatus[i] + " en su posicion" +i, EIGlobal.NivelLog.INFO);

									}
									if(listEstatus[0]!=null)req.setAttribute("TDoc1", listEstatus[0]);
									if(listEstatus[1]!=null)req.setAttribute("TDoc2", listEstatus[1]);
									if(listEstatus[2]!=null)req.setAttribute("TDoc3", listEstatus[2]);
									if(listEstatus[3]!=null)req.setAttribute("TDoc4", listEstatus[3]);
						}


					}catch( java.rmi.RemoteException re ){
								re.printStackTrace();
							}
					catch(Exception e) {}

					if ( trama1_salida.equals( IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID() ) ){
					// Copia Remota
					EIGlobal.mensajePorTrace( "***coConsulta.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
					}

					if(trama1_salida.startsWith("OK"))
					{
					primero = trama1_salida.indexOf( (int) '@');
					ultimo  = trama1_salida.lastIndexOf( (int) '@');
					resultado = trama1_salida.substring(primero+1, ultimo);
					mensaje = resultado;
					}
					else mensaje = trama1_salida;

					if ( mensaje.trim().length() == 0 ) mensaje = "La transacci&oacute;n ha sido rechazada.";
			}



//---------------------Consulta por documento: --------------------------
            //TCT|1001851|CFAE|80000648376|1001851|1001851A|2@27/06/2002@27/06/2002@|

			else if( Reg.equals("2")){
				String nombreArch = "";
                String nombre= "";
				String trama2="";


				try{
					if (Estatus==null){

						EIGlobal.mensajePorTrace("*********Lista Estatus Vacia********--> ", EIGlobal.NivelLog.INFO);
						int tamTemp2 = 0;
						String tam2 = Integer.toString(tamTemp2);
						req.setAttribute("tamListaEstatus", tam2);
						EIGlobal.mensajePorTrace("*********Mandando valor de 0 ********-->"+tam2, EIGlobal.NivelLog.INFO);
					}
					else{
						EIGlobal.mensajePorTrace("*********Lista Estatus NO Vacia********--> ", EIGlobal.NivelLog.INFO);
						String[] listEstatusDoc = req.getParameterValues("Estatus");
						int tamLista2 = listEstatusDoc.length;
						String tam2 = Integer.toString(tamLista2);
						EIGlobal.mensajePorTrace("*********Mandando Tama�o********--> ", EIGlobal.NivelLog.INFO);
						req.setAttribute("tamListaEstatus", tam2);

						for (int i=0;i<listEstatusDoc.length; i++) {
	   						EIGlobal.mensajePorTrace("Checkbox marcado con " + listEstatusDoc[i]  + " en su posicion" +i, EIGlobal.NivelLog.INFO);
						}
						EIGlobal.mensajePorTrace("*********Mandando Valores de Estatus********--> ", EIGlobal.NivelLog.INFO);
							if(tamLista2==1){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
							}
							if(tamLista2==2){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);}
							else if(tamLista2==3){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);}
							else if(tamLista2==4){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);}
							else if(tamLista2==5){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);
								req.setAttribute("Estatus5", listEstatusDoc[4]);}
							else if(tamLista2==6){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);
								req.setAttribute("Estatus5", listEstatusDoc[4]);
								req.setAttribute("Estatus6", listEstatusDoc[5]);}
							else if(tamLista2==7){
								req.setAttribute("Estatus1", listEstatusDoc[0]);
								req.setAttribute("Estatus2", listEstatusDoc[1]);
								req.setAttribute("Estatus3", listEstatusDoc[2]);
								req.setAttribute("Estatus4", listEstatusDoc[3]);
								req.setAttribute("Estatus5", listEstatusDoc[4]);
								req.setAttribute("Estatus6", listEstatusDoc[5]);
								req.setAttribute("Estatus7", listEstatusDoc[6]);}
							EstatusDoc = listEstatusDoc[0];

					}


					//TCT|1001851|CFAE|80000648376|1001851|1001851A|2@27/06/2002@27/06/2002@|
					trama2 = "2EWEB|"+session.getUserID()+"|CFAE|"+session.getContractNumber()+"|"+session.getUserID()+"|"+
					session.getUserProfile()+"|2@"+fechaIni+"@"+fechaFin+"@|";

					ServicioTux tuxGlobal = new ServicioTux();
					//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
					Hashtable hs = tuxGlobal.web_red(trama2);
					trama2_salida = (String) hs.get("BUFFER");
					EIGlobal.mensajePorTrace( "***GetcoCancelacion.class  trama de salida >>"+trama2+"<<", EIGlobal.NivelLog.DEBUG);

					nombreArch = trama2_salida.substring(trama2_salida.lastIndexOf('/')+1,trama2_salida.length());

					ArchivoRemoto archivinR = new ArchivoRemoto();
					//req.setAttribute("op_cons", Reg);

					if(!archivinR.copia(nombreArch))
							EIGlobal.mensajePorTrace( "COCancelacion - consulta: No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
					else{
							EIGlobal.mensajePorTrace( "COCancelacion - consulta: Copia remota OK.", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("paso archivo "+nombreArch, EIGlobal.NivelLog.INFO);
						}

					nombreArchivo=Global.DIRECTORIO_LOCAL + "/" +nombreArch;
					File nombreArchi = new File(nombreArchivo);
                    File temporalito = new File(Global.DIRECTORIO_LOCAL + "/"+session.getUserID()+".tramas");
					if(nombreArchi.renameTo(temporalito))	{
							EIGlobal.mensajePorTrace("***********Archivo respuesta con exito", EIGlobal.NivelLog.INFO);
                            nombre = Global.DIRECTORIO_LOCAL +"/"+session.getUserID()+".tramas";
                    }else {
                            EIGlobal.mensajePorTrace( "****Error al renombrar archivo de tramas ", EIGlobal.NivelLog.INFO);
                            nombre = nombreArchivo;
					}
					req.setAttribute("nombre", nombre);
					EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraConcentracion(): Path NombreArchivo."+nombre, EIGlobal.NivelLog.INFO);


						if (TDoc==null){

							  EIGlobal.mensajePorTrace("*********Lista Vacia ********--> ", EIGlobal.NivelLog.INFO);
							  int tamTemp = 0;
							  String tam = Integer.toString(tamTemp);
							  req.setAttribute("tamLista", tam);
							  EIGlobal.mensajePorTrace("*********Mandando valor de 0 ********-->"+tam, EIGlobal.NivelLog.INFO);
						}
						else {

							String[] listEstatus = req.getParameterValues("TDoc");
							int tamLista = listEstatus.length;
							String tam = Integer.toString(tamLista);
							req.setAttribute("tamLista", tam);

								for (int i=0;i<listEstatus.length; i++) {
	   									EIGlobal.mensajePorTrace("Checkbox marcado con " + listEstatus[i] + " en su posicion" +i, EIGlobal.NivelLog.INFO);

									}
									if(listEstatus[0]!=null)req.setAttribute("TDoc1", listEstatus[0]);
									if(listEstatus[1]!=null)req.setAttribute("TDoc2", listEstatus[1]);
									if(listEstatus[2]!=null)req.setAttribute("TDoc3", listEstatus[2]);
									if(listEstatus[3]!=null)req.setAttribute("TDoc4", listEstatus[3]);
						}


					}catch( java.rmi.RemoteException re ){
								re.printStackTrace();
							}
					catch(Exception e) {}

					if ( trama2_salida.equals( IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID() ) ){
					// Copia Remota
					EIGlobal.mensajePorTrace( "***coConsulta.class  &remote shellito ...&", EIGlobal.NivelLog.INFO);
					}

					if(trama2_salida.startsWith("OK"))
					{
					primero = trama2_salida.indexOf( (int) '@');
					ultimo  = trama2_salida.lastIndexOf( (int) '@');
					resultado = trama2_salida.substring(primero+1, ultimo);
					mensaje = resultado;
					}
					else mensaje = trama2_salida;

					if ( mensaje.trim().length() == 0 ) mensaje = "La transacci&oacute;n ha sido rechazada.";
			}

			//inicio( req, res ); // m�todo principal

			req.setAttribute("diasInhabiles", diasInhabilesAnt() );
			req.setAttribute("mensaje", mensaje);
			req.setAttribute("Encabezado",    CreaEncabezado("Consulta y cancelaci&oacute;n de pagos",
			"Confirming &gt; Consulta y cancelaci&oacute;n de pagos", req ));

			//***********************************************


			String DownPath = IEnlace.DOWNLOAD_PATH;
			req.setAttribute("Path", DownPath);
			evalTemplate(IEnlace.RESULTADO_CONSULTA,req,res);



		} else{
			req.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}
    }

	public void evalTemplate(String x, HttpServletRequest y, HttpServletResponse z)throws IOException, ServletException{
			y.getRequestDispatcher(x).forward(y, z);
	}

	public String replace(String fuente,char oldChar,String newString){
		String toReturn="";
		int index=fuente.indexOf(oldChar);

		if (index!=-1){
			if (index!=0)
				toReturn+=fuente.substring(0,index);
			toReturn+=newString;
			toReturn+=fuente.substring(index+1,fuente.length());
		}
		return toReturn;
	}

}