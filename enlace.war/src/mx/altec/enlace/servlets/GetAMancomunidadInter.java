/*****************************************************************************
 Banco Santander Mexicano
 Clase: GetAMancomunidad
 Fecha: 28 de enero de 2004
*****************************************************************************/

package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.util.*;
import java.io.*;
import java.sql.*;
import javax.sql.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosMancInter;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


//VSWF


public class GetAMancomunidadInter extends BaseServlet
{
    public static final int NEXT = 1;
    public static final int PREV = 2;

    public void init (ServletConfig config) throws ServletException
    {
        super.init (config);
    }

    public void destroy () {}

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException
	{
		if (SesionValida (request, response))
		{
            BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");

            request.getSession ().setAttribute ("MenuPrincipal", session.getStrMenu ());
            request.getSession ().setAttribute ("newMenu", session.getFuncionesDeMenu ());
            request.getSession ().setAttribute ("Encabezado", CreaEncabezado ("Mancomunidad Operaciones Internacionales",
                "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Operaciones Internacionales",  "s39020h", request));
			obtenerTiposCambio(request, response);
            int opc = 0;
            try
			{
                opc = Integer.parseInt ((String) request.getParameter ("Opcion"));
			}
			catch (NumberFormatException ex) { }
            System.out.println ("Opcion: " + opc);
            switch (opc)
			{
                case 0: consulta (request, response);  break;
                case 1: pagina (request, response);    break;
            }
        }
    }

//------------------------------------------------------------------------------------------------------------------
    /** Consulta de Mancomunidad
     * @param request request del servlet
     * @param response response del servlet
     */
    protected void consulta (HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException
	{
		  System.out.println ("Entrando a consulta de mancomunidad internacional");
		  HttpSession sess = req.getSession();
		  BaseResource session = (BaseResource) sess.getAttribute("session");

		  req.setAttribute ("Fecha", ObtenFecha ());

		  // Obtencion de datos del filtro
		  String fechaIni = req.getParameter ("fecha1");
		  String fechaFin = req.getParameter ("fecha2");
		  String opcion   = req.getParameter ("Registro");	// Busqueda por autorizacion o por registro
		  String contrato = session.getContractNumber();

		 // Filtros por tipo de operación
		 String tipoOperacion = "" + (req.getParameter ("ChkCambios") == null ? "" : req.getParameter ("ChkCambios")) +
									 (req.getParameter ("ChkTransInter") == null ? "" : req.getParameter ("ChkTransInter"));
		 if (tipoOperacion == null || tipoOperacion.length() == 0)
			 tipoOperacion = " ";
		 if (tipoOperacion.endsWith (","))
			 tipoOperacion = tipoOperacion.substring (0, tipoOperacion.length () - 1);


		 // Filtros por estatus de la operación
		 String estatus = "" +  (req.getParameter("ChkAceptadas") == null ? "" : req.getParameter ("ChkAceptadas")) +
								(req.getParameter ("ChkRechazadas") == null ? "" : req.getParameter ("ChkRechazadas")) +
								(req.getParameter ("ChkCanceladas") == null ? "" : req.getParameter ("ChkCanceladas")) +
								(req.getParameter ("ChkPendientes") == null ? "" : req.getParameter ("ChkPendientes")) +
								(req.getParameter ("ChkEjecutadas") == null ? "" : req.getParameter ("ChkEjecutadas")) +
								(req.getParameter ("ChkNoEjecutadas") == null ? "" : req.getParameter ("ChkNoEjecutadas"));
		 if (estatus == null || estatus.length() == 0)
			 estatus = " ";
		 if (estatus.endsWith (","))
			 estatus = estatus.substring (0, estatus.length() - 1);


		// Filtro por importe
		String importe = req.getParameter ("Importe");
		if (importe == null || importe.length() == 0)
			importe = " ";

		// Filtro por cuenta
		String cuenta = req.getParameter ("cuenta");
		if (cuenta.length() == 0)
			cuenta = " ";

		// Filtro por usuario
		String usuario = req.getParameter ("usuario");
		 if (usuario == null || usuario.length() == 0)
			 usuario = " ";

		// Folio de registro de mancomunidad
		String folioReg = req.getParameter ("Folio_Registro");
		if (folioReg == null || folioReg.length () == 0)
			folioReg = " ";

		if(sess.getAttribute("fechaIni")!=null)
           sess.removeAttribute("fechaIni");
        sess.setAttribute("fechaIni", fechaIni);

		if(sess.getAttribute("fechaFin")!=null)
           sess.removeAttribute("fechaFin");
        sess.setAttribute("fechaFin", fechaFin);

		req.setAttribute ("fechaIni",fechaIni);
		req.setAttribute ("fechaFin", fechaFin);
		req.setAttribute ("opcion", opcion);
		req.setAttribute ("tipoOperacion", tipoOperacion);
		req.setAttribute ("estatus", estatus);
		req.setAttribute ("importe", importe);
		req.setAttribute ("cuenta", cuenta);
		req.setAttribute ("usuario", usuario);
		req.setAttribute ("folio", folioReg);

		EIGlobal.mensajePorTrace("********************************", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("*  FechaIni: "+req.getAttribute("fechaIni"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  FechaFin: "+req.getAttribute("fechaFin"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  opcion  : "+req.getAttribute("opcion"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  contrato: "+contrato, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  tipoOperacion: "+req.getAttribute("tipoOperacion"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  estatus : "+req.getAttribute("estatus"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  importe : "+req.getAttribute("importe"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  cuenta  : "+req.getAttribute("cuenta"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  usuario : "+req.getAttribute("usuario"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*  folio   : "+req.getAttribute("folio"), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("*********************************", EIGlobal.NivelLog.INFO);

		ServicioTux servicio = new ServicioTux ();
		//servicio.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext)getServletContext()).getContext());

		String  trama_entrada = "2EWEB|" + session.getUserID() + "|CIMA|" + session.getContractNumber() + '|';
				trama_entrada += session.getUserID() + '|' + session.getUserProfile() + '|';
				trama_entrada += '@' + fechaIni + '@' + fechaFin + '@' + opcion + '@' + contrato;
				trama_entrada += '@' + tipoOperacion + '@' + estatus + '@' + importe + '@' + cuenta;
				trama_entrada += '@' + usuario + '@' + folioReg + '@' + session.getUserID() + '@';

		EIGlobal.mensajePorTrace("  Trama_entrada: "+trama_entrada, EIGlobal.NivelLog.DEBUG);

		Hashtable respuesta;

        try
		{
			respuesta = servicio.web_red (trama_entrada);
			String arch = (String) respuesta.get ("BUFFER");

			EIGlobal.mensajePorTrace("Archivo ha leer : "+arch, EIGlobal.NivelLog.DEBUG);

			if ( arch.equals(""))
				despliegaPaginaError ("Error al tratar de obtener la consulta", req, res);
			else
			{

				EIGlobal.mensajePorTrace("GetAMancomunidadInter:recibe - Inicia", EIGlobal.NivelLog.INFO);
				//IF PROYECTO ATBIA1 (NAS) FASE II
				boolean Respuestal = true;

	            ArchivoRemoto recibeArch = new ArchivoRemoto();
	           
	           	if(!recibeArch.copiaCUENTAS(arch, Global.DIRECTORIO_LOCAL)){
					
						EIGlobal.mensajePorTrace("*** GetAMancomunidadInter.consulta  No se realizo la copia remota:" + arch, EIGlobal.NivelLog.ERROR);
						Respuestal = false;
						
					}
					else {
					    EIGlobal.mensajePorTrace("*** GetAMancomunidadInter.consulta archivo remoto copiado exitosamente:" + arch, EIGlobal.NivelLog.DEBUG);
					    
					}
	            
	           	//*********************************************
				
				if (Respuestal)
				{
					java.util.ArrayList resultados = new java.util.ArrayList ();
                    File Archivo = new File( arch );
                    java.io.BufferedReader reader = new java.io.BufferedReader ( new FileReader (Archivo));
                    String temp;
                    temp = reader.readLine ();
                    if (temp == null || !temp.startsWith ("OK"))
					{
                        despliegaPaginaError ("No hay registros para su consulta", req, res);
                        return;
                    }
                    boolean vacio = true;
                    System.out.println ("Antes de entrar al while y crear lista de resultados");
                    while ((temp = reader.readLine ()) != null)
					{
                        resultados.add (new DatosMancInter (temp));
						System.out.println ("Estoy dentro del while");
                        vacio = false;
                    }
                    if (vacio)
					{
                        despliegaPaginaError ("No hay registros para su consulta", req, res);
                        return;
                    }else
                    {
//                    	TODO BIT CU5011 (Consulta de Mancomunidad), BIT CU5031 (Consulta Macomunidad Internacional)
            			/*VSWF-HGG-I*/
                        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
            				try {

            				BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));

            				BitaTransacBean bt = new BitaTransacBean();
            				bt = (BitaTransacBean)bh.llenarBean(bt);

            				if(req.getSession()
            						.getAttribute(BitaConstants.SESS_ID_FLUJO)
            						.equals(BitaConstants.EA_MANCOM_CONS_AUTO))
            					bt.setNumBit(BitaConstants.EA_MANCOM_CONS_AUTO_CONS_CUENTAS_MANC);

            				if(req.getSession()
            						.getAttribute(BitaConstants.SESS_ID_FLUJO)
            						.equals(BitaConstants.EA_MANCOM_OPER_INTER))
            					bt.setNumBit(BitaConstants.EA_MANCOM_OPER_INTER_CONS_CUENTAS_MANCOM);

				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					bt.setNombreArchivo(session.getContractNumber() + ".doc");
				}
            				if(arch != null){
                				if(arch.substring(0,2).equals("OK")){
                					bt.setIdErr("CIMA0000");
                				}else if(arch.length() > 8){
                					bt.setIdErr(arch.substring(0,8));
                				}else{
                					bt.setIdErr(arch.trim());
                				}
                			}

            			BitaHandler.getInstance().insertBitaTransac(bt);
            			} catch (SQLException e) {
            				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			} catch (Exception e) {
            				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			}

                        }
            			/*VSWF-HGG-F*/
                    }
                    System.out.println ("Despues de crear la lista");
                    req.getSession ().setAttribute ("ResultadosMancomunidadInter", resultados);
                    req.getSession ().setAttribute ("IndexMancomunidadInter", new Integer (-30));

                    pagina (req, res);
                }
				else
                    despliegaPaginaError ("Error al tratar de obtener la consulta", req, res);
            }
        }
		catch (Exception ex)
		{
            despliegaPaginaError ("Error al tratar de obtener la consulta", req, res);
            EIGlobal.mensajePorTrace ("Error en consulta de Mancomunidad Internacional: " + ex.getMessage (), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
        }
    }

	//----------------------------------------------------------------------------------------------------------
    /** Metodo para realizar la paginaci&oacute;n de los resultados de la consulta
     * @param request Request del servlet
     * @param response Response del servlet
     */
    public void pagina (HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException
	{
		int index = 0;
        int opc = NEXT;
        int paginacion = 30;
        try
		{
            index = ((Integer) request.getSession ().getAttribute ("IndexMancomunidadInter")).intValue ();
        } catch (Exception ex) { }
        try
		{
            opc = Integer.parseInt ((String) request.getParameter ("OpcPag"));
        }
		catch (Exception ex) { }
        switch (opc)
		{
            case NEXT: if (!(index + 30 > ((ArrayList) request.getSession ().getAttribute ("ResultadosMancomunidadInter")).size ()))
							index += paginacion;
					   break;
            case PREV: index -= paginacion;
					   if (index < 0)
						   index = 0;
	                   break;
        }

		boolean checaTodos = false;
        java.io.File archivo = new java.io.File (IEnlace.DOWNLOAD_PATH + ((BaseResource) request.getSession ().getAttribute ("session")).getContractNumber () + ".doc");
        java.io.FileWriter writer = new java.io.FileWriter (archivo);
        java.util.ListIterator liResultados =  ((java.util.ArrayList) request.getSession ().getAttribute ("ResultadosMancomunidadInter")).listIterator (index);

		while (liResultados.hasNext ())
		{
            DatosMancInter temp = (DatosMancInter) liResultados.next ();
            writer.write (temp.toString ());
        }
        writer.flush ();
        writer.close ();

        ArchivoRemoto archR = new ArchivoRemoto();
        if (!archR.copiaLocalARemoto(((BaseResource) request.getSession ().getAttribute ("session")).getContractNumber () + ".doc", "WEB"))
		{
            EIGlobal.mensajePorTrace( "No se realizo la copia remota", EIGlobal.NivelLog.INFO);
        }
		else
		{
            request.getSession ().setAttribute ("ArchivoManc", "/Download/" + ((BaseResource) request.getSession ().getAttribute ("session")).getContractNumber () + ".doc");
        }
        liResultados = ((java.util.ArrayList) request.getSession ().getAttribute ("ResultadosMancomunidadInter")).listIterator (index);

		while (liResultados.hasNext ())
		{
            DatosMancInter temp = (DatosMancInter) liResultados.next ();
            if (temp.pendiente ())
				checaTodos = true;
        }
        request.getSession ().setAttribute ("SeleccionaTodos", new Boolean (checaTodos));
        request.getSession ().setAttribute ("IndexMancomunidadInter", new Integer (index));
        request.getRequestDispatcher ("/jsp/GetAMancomunidadInter.jsp").forward (request, response);
    }

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo ()
	{
        return "Operaciones Internacionales";
    }

	/**  Metodo para obtener los tipos de cambio en dolar y euro a la compra ya la venta    **/
	public void obtenerTiposCambio(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException
	{
		System.out.println ("Entrando a obtener tipos de cambio en usd y euros");

		HttpSession ses = request.getSession();
        BaseResource session = (BaseResource) ses.getAttribute("session");

		String  cve_divisaEURO = "";
		String  cve_divisaUSD  = "";
		String  tc_ventanilla_compra_usd  = "";
		String 	tc_ventanilla_venta_usd   = "";
		String  tc_ventanilla_compra_euro = "";
		String 	tc_ventanilla_venta_euro  = "";

		String query;
		ResultSet rs = null;
		Connection Conexion = null;
		Statement qrtipos = null;

	   	java.util.GregorianCalendar fechaHoy = new java.util.GregorianCalendar();

		String lafechahoy = EIGlobal.formatoFecha(fechaHoy, "dt, dd de mt de aaaa");

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		query =" SELECT TO_CHAR(fecha,'YYYYMMDD hh24:mi:ss'),to_char(tc_cpa_vent,'999,999,999.000000'),to_char(tc_vta_vent,'999,999,999.000000')";
		query+=" from comu_tc_base ";
		query+=" WHERE cve_divisa = 'USD' ";
		query+=" AND fecha = (select MAX(fecha) from comu_tc_base WHERE cve_divisa = 'USD')";

        try
	    {
          Conexion = createiASConn(Global.DATASOURCE_ORACLE);
		  if(Conexion!=null)
		  {
              qrtipos = Conexion.createStatement();
		      if(qrtipos!=null)
		      {
				  rs = qrtipos.executeQuery(query);
				  if(rs!=null)
				  {
					 try
					 {
							rs.next();
							tc_ventanilla_compra_usd = rs.getString(2);
							tc_ventanilla_venta_usd  = rs.getString(3);
			         }
					 catch(SQLException e)  { EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);  }
				  }


					query =" SELECT TO_CHAR(fecha,'YYYYMMDD hh24:mi:ss'),to_char(tc_cpa_vent,'999,999,999.000000'),to_char(tc_vta_vent,'999,999,999.000000')";
					query+=" from comu_tc_base ";
					query+=" WHERE cve_divisa = 'EUR' ";
					query+=" AND fecha = (select MAX(fecha) from comu_tc_base WHERE cve_divisa = 'EUR')";

					rs = qrtipos.executeQuery(query);
				  if(rs!=null)
				  {
					 try
					 {
						 rs.next();
						 tc_ventanilla_compra_euro = rs.getString(2);
						 tc_ventanilla_venta_euro  = rs.getString(3);
			          }
					  catch(SQLException e)  { EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);  }
					}

			  }
          }
        }
	    catch(SQLException sqle ){ EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);	}
	    catch(Exception e) {  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO); }
	    finally
	    {
            try
			{
            	qrtipos.close();
            	rs.close();
            	Conexion.close();
            }catch(Exception e)	{  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);	}
        }

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	   if (tc_ventanilla_compra_euro.equals("") || tc_ventanilla_compra_euro==null)
			tc_ventanilla_compra_euro = "No disponible";
	   if (tc_ventanilla_venta_euro.equals("") || tc_ventanilla_venta_euro==null)
			tc_ventanilla_venta_euro = "No disponible";
	   if (tc_ventanilla_compra_usd.equals("") || tc_ventanilla_compra_usd==null)
			tc_ventanilla_compra_usd = "No disponible";
	   if (tc_ventanilla_venta_euro.equals("") || tc_ventanilla_venta_euro==null)
			tc_ventanilla_venta_euro = "No disponible";

	   if( ses.getAttribute("tcc_usd")!=null )
		   ses.removeAttribute("tcc_usd");
	   ses.setAttribute("tcc_usd", tc_ventanilla_compra_usd);

	   if( ses.getAttribute("tcc_euro")!=null )
		   ses.removeAttribute("tcc_euro");
	   ses.setAttribute("tcc_euro", tc_ventanilla_compra_euro);

	   if( ses.getAttribute("tcv_usd")!=null )
		   ses.removeAttribute("tcv_usd");
	   ses.setAttribute("tcv_usd", tc_ventanilla_venta_usd);

	   if( ses.getAttribute("tcv_euro")!=null )
		   ses.removeAttribute("tcv_euro");
	   ses.setAttribute("tcv_euro", tc_ventanilla_venta_euro);

	   request.setAttribute("tcc_usd", tc_ventanilla_compra_usd);
	   request.setAttribute("tcc_euro", tc_ventanilla_compra_euro);
       request.setAttribute("tcv_usd",  tc_ventanilla_venta_usd);
	   request.setAttribute("tcv_euro", tc_ventanilla_venta_euro);

   	   System.out.println ("Saliendo de obtener tipos de cambio en usd y euros con los siguientes datos: ");
	   System.out.println ("tc_ventanilla_compra_euro"+tc_ventanilla_compra_euro);
	   System.out.println ("tc_ventanilla_venta_euro"+tc_ventanilla_compra_euro);
	   System.out.println ("tc_ventanilla_compra_usd"+tc_ventanilla_compra_usd);
	   System.out.println ("tc_ventanilla_venta_usd"+tc_ventanilla_venta_usd);
	}
}