package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.Archivos;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



/**
** proCancel is a blank servlet to which you add your own code.
*/
public class proCancel extends BaseServlet
{
    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }



    /**
    ** <code>displayMessage</code> allows for a short message to be streamed
    ** back to the user.  It is used by various NAB-specific wizards.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @param messageText the message-text to stream to the user.
    */
    public void displayMessage(HttpServletRequest req,
                      HttpServletResponse res,
                      String messageText)
                    throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println(messageText);
    }

    /**
    ** <code>defaultAction</code> is the default entry-point for iAS-extended
    ** @param req the HttpServletRequest for this servlet invocation.
    ** @param res the HttpServletResponse for this servlet invocation.
    ** @exception javax.servlet.ServletException when a servlet exception occurs.
    ** @exception java.io.IOException when an io exception occurs during output.
    */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException
    {

		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        boolean sesionvalida = SesionValida( req, res );
        Vector Vec_Cancel = new Vector();
		Vector Vec_Cancel2 = new Vector();
		Vector Vec_Cancel3 = new Vector();

        String archivo = (String)req.getSession().getAttribute("archivo");
		System.out.println("nombre IncheArchivo---"+archivo);
		String mensaje = "Checando Cancelacion";

	// servidor para facultar a Consultas

        if ( sesionvalida ) {

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu",       session.getFuncionesDeMenu());
		//req.setAttribute("diasInhabiles", diasInhabilesAnt() );
		req.setAttribute("Encabezado",    CreaEncabezado("Consulta y cancelaci&oacute;n de pagos",
									"Confirming &gt; Consulta y cancelaci&oacute;n de pagos", req ));

		String Cancel1 = (String)req.getParameter("Cancel1");
        String[] listaCancel1 = req.getParameterValues("Cancel1");
		String Cancel2 = (String)req.getParameter("Cancel2");
		String[] listaCancel2 = req.getParameterValues("Cancel2");
		String Cancel3 = (String)req.getParameter("Cancel3");
		String[] listaCancel3 = req.getParameterValues("Cancel3");



        int tamLista1 = listaCancel1.length -2;
		int tamLista2 = listaCancel2.length -2;
		int tamLista3 = listaCancel3.length -2;

		//Debugeo Temporal
		for (int i =0;i<tamLista1;i++)System.out.println("Valor del Checkbox---->"+listaCancel1[i]);
		for (int i =0;i<tamLista2;i++)System.out.println("Valor del Checkbox---->"+listaCancel2[i]);
		for (int i =0;i<tamLista3;i++)System.out.println("Valor del Checkbox---->"+listaCancel3[i]);

        Vector ls_1 = (Vector)req.getSession().getAttribute("lista1");
		Vector ls_2 = (Vector)req.getSession().getAttribute("lista2");
		Vector ls_3 = (Vector)req.getSession().getAttribute("lista3");

		//Debugeo Temporal
		System.out.println("Valor de Lista1---->"+ls_1.size());
		System.out.println("Valor de Lista2---->"+ls_2.size());
		System.out.println("Valor de Lista3---->"+ls_3.size());
        int l = 0;

		if(tamLista1!=0){
			System.out.println("Dentro de Lista1----#");
			for (int i=0 ;i<tamLista1 ;i++ )
			{
				int x = Integer.parseInt(listaCancel1[i]);
				int y = x*20;
				int m = y+20;
				for(int k= x*20; k<m; k++){
					Vec_Cancel.add((String)ls_1.elementAt(k));}

			}
			mensaje = ChecaCancelacion(Vec_Cancel, archivo);
		}

		if(tamLista2!=0){
			System.out.println("Dentro de Lista2----#");
			for (int i=0 ;i<tamLista2 ;i++ )
			{
				int x = Integer.parseInt(listaCancel2[i]);
				int y = x*13;
				int m = y+13;
				for(int k= x*13; k<m; k++){
					Vec_Cancel.add((String)ls_2.elementAt(k));}

			}
			mensaje = ChecaNotas(Vec_Cancel, archivo);
		}

		if(tamLista3!=0){
			System.out.println("Dentro de Lista3----#");
			for (int i=0 ;i<tamLista3 ;i++ )
			{
				int x = Integer.parseInt(listaCancel3[i]);
				int y = x*8;
				int m = y+8;
				for(int k= x*8; k<m; k++){
					Vec_Cancel.add((String)ls_3.elementAt(k));}

			}

			mensaje = ChecaFacturas(Vec_Cancel, archivo);
		}

		if(mensaje.equals("")){
			//Armando Trama de Cancelacion

			System.out.println("Listo para ser Cancelado");
			for(int i =0;i<Vec_Cancel.size(); i++){System.out.println("Registro a Cancelar--<>"+Vec_Cancel.elementAt(i));}

			String secuencia = ((String)Vec_Cancel.elementAt(1)).trim();
			String persona =  ((String)Vec_Cancel.elementAt(2)).trim();
			String tipo = ((String)Vec_Cancel.elementAt(0)).trim();
			String docu = ((String)Vec_Cancel.elementAt(3)).trim();
			String trama_salida = "" ;
			String resultado = "" ;
			String mensaje2 = "";

			String trama = "1EWEB|"+session.getUserID()+"|CFCP|"+session.getContractNumber()+"|"+session.getUserID()+"|"+
						session.getUserProfile()+"|"+secuencia+"@"+persona+"@"+tipo+"@"+docu+"@|";

			int  primero = 0;
			int  ultimo = 0;

			System.out.println("TRAMA-----"+trama);

				//Mandando Servicio de Cancelacion

			try{

				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				Hashtable ht = tuxGlobal.web_red(trama);
				trama_salida = ((String) ht.get("BUFFER")).trim();
				EIGlobal.mensajePorTrace( "<-----Checando Regreso de Cancelacion------>"+trama_salida, EIGlobal.NivelLog.INFO);

			}catch ( java.rmi.RemoteException re ){re.printStackTrace();
			}catch(Exception e) {System.out.println("Erro del siguiente tipo--"+e);}

			EIGlobal.mensajePorTrace( "<-----Checando Regreso de Cancelacion despues del try------>", EIGlobal.NivelLog.INFO);
			if(trama_salida.startsWith("OK")){
				//primero = trama_salida.indexOf( (int) '@');
				//ultimo  = trama_salida.lastIndexOf( (int) '@');
				resultado = trama_salida.substring(0, 3);
				mensaje = resultado + "   Documento Cancelado";
				EIGlobal.mensajePorTrace( "<-----Checando Regreso de Cancelacion despues de validaciones------>", EIGlobal.NivelLog.INFO);
			}
			else {
				resultado = trama_salida.substring(16,(int)trama_salida.length());
				mensaje = resultado;}

			if ( (mensaje.trim()).length() == 0 )  {mensaje = "La transacci&oacute;n ha sido rechazada.";}

		}
		else{System.out.println("Resultado de Checar la Cancelacion--->"+mensaje);}

		req.setAttribute("mensaje", mensaje);

        }//Fin del sesionvalida


		evalTemplate(IEnlace.RESULTADO_CANCELACION,req,res);

    }

    /**
     * Metodo para checar la cancelacion de una Factura
     * @param ls_1 lista donde llegan todas las notas de las consultas
     * @param archivo es el path del archivo donde se tiene todas las tramas del archivo consultado
     * @return "" si el archivo se puede cancelar de lo contrario regresa el mensaje asociado
     */
	public String ChecaCancelacion(Vector ls_1,String Archivo){

        String mensaje ="";
		String nota = "";
		boolean neteo = false;
        int size = ls_1.size();

        if(size==0){
            mensaje = "El documento buscado no existe"; return(mensaje);}
        else{
            String es_cobro =((String)ls_1.elementAt(17)).trim();
            String es_pago = ((String)ls_1.elementAt(18)).trim();
            String cve_documento = ((String)ls_1.elementAt(0)).trim();

            //Version para la opcion 3
            if(cve_documento.equals("1")){
                if(es_cobro.equals("C")){
                    mensaje= "No se puede cancelar este documento por que ya fue cobrado";
                    return(mensaje);
                }
                else if(es_cobro.equals("R")){
					mensaje ="";
                    return(mensaje);
				}
                else{
                 mensaje = "No se puede Cancelar este documento porque esta vencido";
                 return(mensaje);
                }
            }

            if( cve_documento.equals("1") || cve_documento.equals("2")){
            String fechaVencLs =((String)ls_1.elementAt(9)).trim();
            GregorianCalendar fechaVenc = getFechaString(fechaVencLs);
            GregorianCalendar fechaActual = new GregorianCalendar();
                if(comparaFechasMayor(fechaVenc, fechaActual)){
                     mensaje = "El documento seleccionado esta fuera de plazo para ser cancelado";
                     return(mensaje);
                }
                if( es_cobro!="P" || es_pago!="R"){
                    if(es_cobro!="P"){
                    mensaje = "El documento seleccionado no esta pendiente de cobro -Por parte del Proveedor";
                    return (mensaje);
                    }
                    if(es_pago!="R"){
                    mensaje = "El documento seleccionado no esta pendiente de pago - Para que se pueda cancelar";
                    return (mensaje);
                    }
                }
                if(cve_documento.equals("1")){

                    //Validar lo referente al neteo.
					Archivos archi = new Archivos();
					Vector lista3 = new Vector();

						lista3 = archi.docLee(Archivo,"3");


					if("vacio".equals(((String)lista3.elementAt(0)).trim())){
						neteo = false;
						//Preparado para  armar la trama de Cancelacion
					}
					else{
							String numDoc = ((String)ls_1.elementAt(3)).trim();

							if(lista3.size()!=1){
								int m = 0;
								while(m<lista3.size()){
									if(numDoc.equals(((String)lista3.elementAt(3)).trim())) {
										neteo = true;
										nota = (String)lista3.elementAt(4);
									}
									m=m+8;
								}//While
							}
					}

					//Si el neteo es positivo regresar
					if(neteo){
						mensaje ="El documento se encuentra asociado a la nota numero   " +nota+
							" para poder cancelar este documento debe de cancelar las notas asociadas";
						return (mensaje);
					}
                   else{mensaje =""; return (mensaje);}

                }
            }
            if(cve_documento.equals("3")){
                System.out.println("Prueba de Debugeo--->1.15");
                Archivos archi = new Archivos();
					Vector lista3 = new Vector();

						lista3 = archi.docLee(Archivo,"3");


					if("vacio".equals(((String)lista3.elementAt(0)).trim())){
						neteo = false;
						//Preparado para  armar la trama de Cancelacion
					}
					else{
							String numDoc = ((String)ls_1.elementAt(3)).trim();

							if(lista3.size()!=1){
								int m = 0;
								while(m<lista3.size()){
									if(numDoc.equals(((String)lista3.elementAt(3)).trim())) {
										neteo = true;
										nota = (String)lista3.elementAt(4);
									}
									m=m+8;
								}//While
							}
					}
					//Si el neteo es positivo regresar
					if(neteo){
						mensaje ="";
						return (mensaje);
					}
                   else{mensaje ="Archivo sin notas"; return (mensaje);}
            }
        }
     return(mensaje);
    }

	/**
     * Metodo para checar si las notas estan asociadas a alguna factura en especifico
     * @param ls_3 lista donde llegan todas las notas neteadas de las consultas
     * @param archivo es el path del archivo donde se tiene todas las tramas del archivo consultado
     * @return "" si el archivo se puede cancelar de lo contrario regresa el mensaje asociado
     */
    public String ChecaFacturas(Vector ls_3, String archivo){


		String mensaje ="";
		String nota = "";
		boolean neteo = false;

		int size = ls_3.size();
		String cve_documento = "";

        if(size==0){
            mensaje = "No se encontro el documento buscado"; return(mensaje);}
        else{
            cve_documento = ((String)ls_3.elementAt(0)).trim();
		}

		if(cve_documento.equals("3")){

                Archivos archi = new Archivos();
				Vector lista1 = new Vector();


						lista1 = archi.sickLee(archivo,"1");


					if("vacio".equals(((String)lista1.elementAt(0)).trim())){
						mensaje = "Facturas no encontradas para esta nota";
						neteo = false;
						return (mensaje);
						//Preparado para  armar la trama de Cancelacion
					}
					else{

							String numDoc = ((String)ls_3.elementAt(3)).trim();

							if(lista1.size()!=1){
								int m = 0;
								while(m<lista1.size()){
									if(numDoc.equals(((String)lista1.elementAt(m+3)).trim())) {
										neteo = true;
									}
									m=m+20;
								}//While
							}
					}
					//Si el neteo es positivo regresar
					if(neteo){
						mensaje ="";
						return (mensaje);
					}else{mensaje = "Nota que no tiene asociada facturas pendientes a Cancelar";}

            }
			return (mensaje);
	}

	/**
     * Metodo para checar si las notas estan asociadas a algun tipo de documento
     * @param ls_2 lista donde llegan todas las notas de las consultas
     * @param archivo es el path del archivo donde se tiene todas las tramas del archivo consultado
     * @return "" si el archivo se puede cancelar de lo contrario regresa el mensaje asociado
     */
	public String ChecaNotas(Vector ls_2, String archivo){

		String nota = "";
		boolean neteo = false;
		String mensaje = "";

		int size = ls_2.size();
		String cve_documento = "";
		String  l_estatus = "";

        if(size==0){
            mensaje = "No se encontro el documento buscado"; return(mensaje);}
        else{
			cve_documento = ((String)ls_2.elementAt(0)).trim();
			l_estatus = ((String)ls_2.elementAt(10)).trim();
		}

		if(cve_documento.equals("1")){
                if(l_estatus.equals("C")){
                    mensaje= "No se puede cancelar este documento por que ya fue cobrado";
                    return(mensaje);
                }
                else if(l_estatus.equals("R")){
					mensaje ="";
                    return(mensaje);
				}
                else{

                 mensaje = "No se puede Cancelar este documento porque esta vencido";
                 return(mensaje);
                }
         }
		 else if(cve_documento.equals("2")){

                    //Validar lo referente al neteo.
					Archivos archi = new Archivos();
					Vector lista2 = new Vector();

						lista2 = archi.docLee(archivo,"3");


					if("vacio".equals(((String)lista2.elementAt(0)).trim())){
						neteo = false;
						//Preparado para  armar la trama de Cancelacion
					}
					else{
							String numDoc = ((String)ls_2.elementAt(3)).trim();

							if(lista2.size()!=1){
								int m = 0;
								while(m<lista2.size()){
									if(numDoc.equals(((String)lista2.elementAt(3)).trim())) {
										neteo = true;
										nota = (String)lista2.elementAt(4);
									}
									m=m+8;
								}//While
							}
					}
			//Si el neteo es positivo regresar
			if(neteo){
				mensaje ="El documento se encuentra asociado a la nota numero   " +nota+
								" para poder cancelar este documento debe de cancelar las notas asociadas";
				return (mensaje);
			}
            else{mensaje =""; return (mensaje);}

          }
		  return (mensaje);
	}



    public static GregorianCalendar getFechaString (String fecha) {
        try {
                StringTokenizer stFecha = new StringTokenizer (fecha, "/");
                int Dia = Integer.parseInt (stFecha.nextToken ());
                int Mes = Integer.parseInt (stFecha.nextToken ()) - 1;
                int Anio = Integer.parseInt (stFecha.nextToken ());
                return new GregorianCalendar (Anio, Mes, Dia);
            } catch (Exception ex) {
                return null;
            }
        }

     /**
     * Metodo para comparar dos fechas y ver si la fecha 1 es mayor o igual a la fecha 2
     * @param fecha1 Fecha a comparar
     * @param fecha2 Fecha del dia actual
     * @return true si la fecha1 es igual o mayor a la fecha2
     */

    public static boolean comparaFechasMayor (GregorianCalendar fecha1,
        GregorianCalendar fecha2) {
        if (fecha1.get (Calendar.DAY_OF_YEAR) <= fecha2.get (Calendar.DAY_OF_YEAR))
            return false;
        if (fecha1.get (Calendar.YEAR) <= fecha2.get (Calendar.YEAR))
            return false;
        return true;
    }

	/**
     * Metodo para mandar a llamar al jsp que despliega la informacion sobre el procesod e cancelacion
     * @param String x es la llamada a la clase de IEnlace
     * @param y es el Request
     * @return z es el Response
     */

	public void evalTemplate(String x, HttpServletRequest y,
		HttpServletResponse z)throws IOException, ServletException{

			y.getRequestDispatcher(x).forward(y, z);
	}

}
