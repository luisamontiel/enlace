package mx.altec.enlace.servlets;



import java.util.*;
import java.io.IOException;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import javax.servlet.http.*;
import java.text.*;
import javax.naming.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



public class Consult extends BaseServlet {



	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}



	public void defaultAction( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {



		boolean sesionvalida = SesionValida( request, response );
					BaseResource session = (BaseResource) request.getSession().getAttribute("session");

		// servidor para faciultad a Mancomunidad
		if ( sesionvalida )

		{

			String operacion=(String)request.getParameter("operacion");

			EIGlobal.mensajePorTrace( "Consult.java Entrando y detectando si viene operacion: "+ operacion, EIGlobal.NivelLog.INFO);

			if( operacion!=null )
			 {
				EIGlobal.mensajePorTrace( "Consult.java Evaluando para descarga >>"+ operacion, EIGlobal.NivelLog.INFO);
				if(operacion.trim().equals("descarga") )
				 descargaArchivos(request, response);
			 }
			else
			 {

				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("newMenu",       session.getFuncionesDeMenu());
				request.setAttribute("diasInhabiles", diasInhabilesAnt() );
				request.setAttribute("Encabezado",    CreaEncabezado("Consulta de Bit&aacute;cora de Mancomunidad",
					"Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n",request ) );


				//***********************************************
				//modificación para integración pva 07/03/2002
				//***********************************************
				// Obtiene las cuentas asociadas al contrato
				/*String[][] arrayCuentas = ContCtasRelacpara_mis_transferencias( session.getContractNumber() );
				String Cuentas          = "";
				String tipo_cuenta      = "";



				for ( int indice = 1; indice <= Integer.parseInt( arrayCuentas[0][0] ); indice++ ) {
					tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
					if ( arrayCuentas[indice][2].equals( "P" ) ) {
						if ( tipo_cuenta.equals( "49" ) ) {
							 //Cuentas dolares
							if ( arrayCuentas[indice][3].equals( "6" ) )
								Cuentas += "<OPTION value = " + arrayCuentas[indice][1] + ">" +
										arrayCuentas[indice][1] + " " + arrayCuentas[indice][4] + "\n";
						} else
							Cuentas += "<OPTION value = " + arrayCuentas[indice][1] + ">" +
									arrayCuentas[indice][1] + " " + arrayCuentas[indice][4] + "\n";
					}
				}

			   */
				//***********************************************
				// Obtiene los usuarios de las cuentas asociadas al contrato.
				String[][] arrayUsuarios = TraeUsuarios(request);
				String Usuarios          = "";
				String NoUsuario         = "";

				long UserAux = 0;
				long numero1 = 0;
				long numero2 = 0;



				for( int i = 1; i <= Integer.parseInt( arrayUsuarios[0][0].trim() ); i++ ) {
					for( int j = i; j <= Integer.parseInt( arrayUsuarios[0][0].trim() ); j++ ) {
						numero1 = new Long(arrayUsuarios[i][1]).longValue();
						numero2 = new Long(arrayUsuarios[j][1]).longValue();
						if(numero1 > numero2) {
							Usuarios = arrayUsuarios[i][2];
							NoUsuario = arrayUsuarios[i][1];
							arrayUsuarios[i][2] = arrayUsuarios[j][2];
							arrayUsuarios[i][1] = arrayUsuarios[j][1];
							arrayUsuarios[j][2] = Usuarios;
							arrayUsuarios[j][1] = NoUsuario;
						}
					}
				}

				Usuarios = "";
				EIGlobal.mensajePorTrace( "numero de usuarios " + arrayUsuarios[0][0], EIGlobal.NivelLog.INFO);
				for ( int indice = 1; indice <= Integer.parseInt( arrayUsuarios[0][0].trim() ); indice++ ) {
					Usuarios += "<OPTION value = " + arrayUsuarios[indice][1] + ">" +
							arrayUsuarios[indice][1] + " " + arrayUsuarios[indice][2] + "\n";
				}

				String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" +
						ObtenAnio() + "\">";
				datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
				datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
				request.setAttribute("Bitfechas",datesrvr);
				//modificacion para integracion request.setAttribute("Cuentas",Cuentas);
				request.setAttribute("Usuarios",Usuarios);
				//***********************************************
				//modificación para integración pva 07/03/2002
				//***********************************************
				session.setModuloConsultar(IEnlace.MOperaciones_mancom);
				//***********************************************
				request.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA); 			evalTemplate( IEnlace.CAMANCOMUNIDAD_TMPL, request, response );
			}
		} else if ( sesionvalida ) {
			request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}
	}

	protected void descargaArchivos(HttpServletRequest request,HttpServletResponse response)
		throws ServletException, java.io.IOException
	{
		//String nombreDelArchivo = "rada.zip";
		String nombreDelArchivo= request.getParameter("nombreArchivo");

		EIGlobal.mensajePorTrace( "Consult.java Entrando al metodo para descarga de: "+ nombreDelArchivo, EIGlobal.NivelLog.INFO);

		response.setHeader("Content-disposition","attachment; filename=" + nombreDelArchivo );
		response.setContentType("application/pdf");

		BufferedInputStream  entrada = null;
		BufferedOutputStream salida = null;
		InputStream arc= new FileInputStream (IEnlace.LOCAL_TMP_DIR+"/"+nombreDelArchivo);

		ServletOutputStream out=response.getOutputStream();
		entrada = new BufferedInputStream(arc);
		salida = new BufferedOutputStream(out);
		byte[] buff = new byte[1024];
		int cantidad=0;

		while( (cantidad = entrada.read(buff, 0, buff.length )) !=-1)
			salida.write(buff,0,cantidad);

		salida.close();
		entrada.close();
	}
}
/*
   text/richtext
   text/html
   audio/x-aiff
   audio/basic
   audio/wav
   image/gif
   image/jpeg
   image/pjpeg
   image/tiff
   image/x-png
   image/x-xbitmap
   image/bmp
   image/x-jg
   image/x-emf
   image/x-wmf
   video/avi
   video/mpeg
   application/postscript
   application/base64
   application/macbinhex40
   application/pdf
   application/x-compressed
   application/x-zip-compressed
   application/x-gzip-compressed
   application/java
   application/x-msdownload
   Consult?nombreArchivo=uno.pdf&operacion=descarga
*/

