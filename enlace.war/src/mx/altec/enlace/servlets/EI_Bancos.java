package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



import java.util.*;
import java.lang.reflect.*;
import java.sql.*;
import java.io.*;

public class EI_Bancos extends BaseServlet
{
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		String clave = (String) req.getParameter("Clave");

		if(clave==null)
		  clave = "0";

		if(clave.equals("0")){
			 try{			
				iniciaCatalogoBancos(req, res);
			 }catch(Exception e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al ejecutar metodo iniciaCatalogoBancos del servlet EI_Bancos", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("EI_Bancos::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ e.getMessage()
				               				+ "<DATOS GENERALES>"
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);				
			}		  
		}else{
			try{
				iniciaCatalogoClaves(req, res);
			 }catch(Exception e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al ejecutar metodo iniciaCatalogoClaves del servlet EI_Bancos", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("EI_Bancos::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ e.getMessage()
				               				+ "<DATOS GENERALES>"
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);				
			}		  
		}
	}

	public void iniciaCatalogoClaves(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		StringBuffer strDatos=new StringBuffer("");
		StringBuffer strFuncion=new StringBuffer("");
        String totales="";
        String strBancoTxt="";
        String sqlDato="";
        String Clave="";
        String Error="SUCCESS";

		String BancoABA="";
        String CiudadABA="";
        String EstadoABA="";
        String Cancelar="";

		Connection conn = null;
		ResultSet contResult = null;
		ResultSet datoResult = null;

        boolean cveABA=false;

        int totalDatos=0;
		int totalPlazas=0;

		strBancoTxt=(String) req.getParameter("BancoTxt");

		if(strBancoTxt==null)
			strBancoTxt = "ALDEN";

		PreparedStatement contQuery ;
		PreparedStatement plazaQuery;

		try
		{

			conn = createiASConn(Global.DATASOURCE_ORACLE);

			if(conn == null){/*No se pudo obtener una conexion valida*/

				EIGlobal.mensajePorTrace("EI_Bancos - iniciaCatalogoClaves(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			else{ /*Si se pudo obtener una conexion valida*/

				if(strBancoTxt.trim().equals("ABA")){/*Consulta por Claves*/

					EIGlobal.mensajePorTrace("EI_Bancos - iniciaCatalogoClaves(): Entrando a buscar la clave", EIGlobal.NivelLog.ERROR);
					Clave=req.getParameter("Clave");
					if(Clave==null)
						Clave="";
					totales="Select count(nombre) from camb_banco_eu where CVE_ABA = ? ";
					sqlDato="SELECT NOMBRE,CIUDAD,ESTADO FROM CAMB_BANCO_EU WHERE CVE_ABA = ? ";
					cveABA=true;
                    
					EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Clave - totales: "+ totales, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Clave - sqlDato: "+ sqlDato, EIGlobal.NivelLog.DEBUG);
					
					contQuery = conn.prepareStatement(totales);
					contQuery.setString(1, Clave);
					
                    EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Clave - contQuery: "+ contQuery, EIGlobal.NivelLog.DEBUG);

					plazaQuery = conn.prepareStatement(sqlDato);
					plazaQuery.setString(1, Clave);
					
					EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Clave - plazaQuery: "+ plazaQuery, EIGlobal.NivelLog.DEBUG);
					
				}
				else /*Consulta por Nombre*/
				{
					EIGlobal.mensajePorTrace("EI_Bancos - iniciaCatalogoClaves(): Entrando a buscar el banco", EIGlobal.NivelLog.ERROR);
					totales="Select count(distinct(nombre)) from camb_banco_eu where nombre like ? ";
					sqlDato="SELECT CVE_ABA,CIUDAD,ESTADO FROM CAMB_BANCO_EU WHERE NOMBRE LIKE ? ";
					cveABA=false;

                    EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Nombre - totales: "+ totales, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Nombre - sqlDato: "+ sqlDato, EIGlobal.NivelLog.DEBUG);
					
					contQuery = conn.prepareStatement(totales);
					contQuery.setString(1, strBancoTxt);

					EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Nombre - contQuery: "+ contQuery, EIGlobal.NivelLog.DEBUG);
					
					plazaQuery = conn.prepareStatement(sqlDato);
					plazaQuery.setString(1, strBancoTxt);

					EIGlobal.mensajePorTrace("EI_Bancos - Consulta por Nombre - plazaQuery: "+ plazaQuery, EIGlobal.NivelLog.DEBUG);
				}

				/*Ejecuta la consulta para obtener el numero de bancos disponibles */
				contResult = contQuery.executeQuery();

				if(contResult.next()){
					totalDatos=Integer.parseInt(contResult.getString(1));
				}
				else{
					totalDatos=0;
				}

				EIGlobal.mensajePorTrace("EI_Bancos - iniciaCatalogoClaves(): Total de Datos: "+Integer.toString(totalDatos), EIGlobal.NivelLog.INFO);

				/************************************************** Contar registros de bancos .... */

				/*Ejecuta la consulta para obtener los bancos/plazas disponibles */
				datoResult = plazaQuery.executeQuery();

				if(datoResult==null){
					EIGlobal.mensajePorTrace("EI_Bancos - iniciaCatalogoClaves(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
					Error="NOSUCCESS";
				}

			} //Fin if - else


			if(Error.equals("SUCCESS"))
			{
				EIGlobal.mensajePorTrace("EI_Bancos - iniciaCatalogoClaves(): Buscando las claves", EIGlobal.NivelLog.ERROR);
				if(!cveABA)
				{
					strFuncion.append("<script>");
					strFuncion.append("function seleccionar() {");
					strFuncion.append("        var indice = document.Forma.SelClave.selectedIndex;");
					strFuncion.append("        var cve = document.Forma.SelClave.options[indice].value;");
					strFuncion.append("		  var desentramaCve = cve.split(\"|\");");
					strFuncion.append("        opener.document.formulario.vCveABA.value = desentramaCve[1];");
					strFuncion.append("        opener.document.formulario.vDescripcionCiudad.value = desentramaCve[2];");
					strFuncion.append("}");
					strFuncion.append("</script>");

					for(int i=0; i<totalDatos; i++)
					{
						datoResult.next();
						strDatos.append("\n          <option value=");
						strDatos.append("'");
						strDatos.append(i);
						strDatos.append("|");
						strDatos.append(datoResult.getString(1).trim());
						strDatos.append("|");
						strDatos.append(datoResult.getString(2).trim());
						strDatos.append("'");
						strDatos.append(">");
						strDatos.append(datoResult.getString(1).trim());
						strDatos.append(" - ");
						strDatos.append(datoResult.getString(2).trim());
						strDatos.append(", ");
						strDatos.append(datoResult.getString(3).trim());
						strDatos.append("</option>");
					}
				}
				else
				{
					if(totalDatos>0)
					{
						datoResult.next();
						BancoABA=datoResult.getString(1);
						CiudadABA=datoResult.getString(2);
						EstadoABA=datoResult.getString(3);

						strDatos.append("\n<table border=0 width=90% align=center class='textabdatcla' cellpadding=0 cellspacing=0>");
						strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
						strDatos.append("\n<tr><th class='tabmovtex' align=left> &nbsp;&nbsp;&nbsp; Clave ABA: ");
						strDatos.append(Clave);
						strDatos.append("</td></tr>");
						strDatos.append("\n<tr><th class='tabmovtex' align=left> &nbsp;&nbsp;&nbsp; Banco: ");
						strDatos.append(BancoABA);
						strDatos.append("</td></tr>");
						strDatos.append("\n<tr><th class='tabmovtex' align=left> &nbsp;&nbsp;&nbsp; Ciudad: ");
						strDatos.append(CiudadABA);
						strDatos.append(", ");
						strDatos.append(EstadoABA);
						strDatos.append("</td></tr>");
						strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
						strDatos.append("\n<tr><td class='tabmovtex' align=center> Desea actualizar la pantalla con &eacute;stos datos ?</td></tr>");
						strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
						strDatos.append("\n<tr><td bgcolor=white><br></td></tr>");
						strDatos.append("\n</table>");

						Cancelar="<a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>";
					}
					else
					{
						strDatos.append("\n<table border=0 width=90% align=center class='textabdatcla' cellpadding=0 cellspacing=0>");
						strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
						strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
						strDatos.append("\n<tr><th class='tabmovtex'>La clave ABA especificada no existe, por favor verifiquela e intente nuevamente.</td></tr>");
						strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
						strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
						strDatos.append("\n<tr><td bgcolor=white><br></td></tr>");
						strDatos.append("\n</table>");
					}
				}

			}
			else{

				strDatos.append("\n<table border=0 width=90% align=center class='textabdatcla' cellpadding=0 cellspacing=0>");
				strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
				strDatos.append("\n<tr><th class='tabmovtex'>Su transaccitransaccionoacute;n no puede ser atendida. Por favor intente mas tarde.</td></tr>");
				strDatos.append("\n<tr><td class='tabmovtex'><br></td></tr>");
				strDatos.append("\n<tr><td bgcolor=white><br></td></tr>");
				strDatos.append("\n</table>");
			}
		} catch( SQLException sqle ) {
			//sqle.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = sqle.getStackTrace();
			EIGlobal.mensajePorTrace("Error al iniciar catalogo de claves", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("EI_Bancos::iniciaCatalogoClaves:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ sqle.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
		} catch(Exception e) {
			//e.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al iniciar catalogo de claves", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("EI_Bancos::iniciaCatalogoClaves:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
		}finally
		  {
			try
			 {
			   conn.close();
			   contResult.close();
			   datoResult.close();
			 }catch(Exception e) {}
		  }

		req.setAttribute("SFuncion", strFuncion.toString());
		req.setAttribute("SClaves",strDatos.toString());
		req.setAttribute("BancoABA",BancoABA);
		req.setAttribute("CiudadABA",CiudadABA);
		req.setAttribute("EstadoABA",EstadoABA);
		req.setAttribute("Cancelar",Cancelar);

		if(cveABA)
		 evalTemplate("/jsp/EI_CveABA.jsp", req, res);
		else
		 evalTemplate("/jsp/EI_Claves.jsp", req, res);
	}

	public void iniciaCatalogoBancos(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		StringBuffer strPlazas=new StringBuffer("");
		String totales="";
		String Error="SUCCESS";

		ResultSet contResult = null;
		ResultSet plazaResult = null;
		Connection conn = null;

		int totalPlazas = 0;
		int salida=0;

		EIGlobal.mensajePorTrace("EI_Bancos - execute(): Entrando a Catalogo de Bacos.", EIGlobal.NivelLog.INFO);
		try
		{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null)
			{
				EIGlobal.mensajePorTrace("EI_Bancos - execute(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			totales="Select count(distinct(nombre)) from camb_banco_eu";
			PreparedStatement contQuery = conn.prepareStatement(totales);
			if(contQuery == null)
			{
				EIGlobal.mensajePorTrace("EI_Bancos - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			contResult = contQuery.executeQuery();
			if(contResult==null)
			{
				EIGlobal.mensajePorTrace("EI_Bancos - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			if(contResult.next())
				totalPlazas=Integer.parseInt(contResult.getString(1));
			else
				totalPlazas =0;

			EIGlobal.mensajePorTrace("EI_Bancos - execute(): Total de Bancos: "+Integer.toString(totalPlazas), EIGlobal.NivelLog.INFO);
			/************************************************** Contar registros de bancos .... */
			String sqlplaza="SELECT DISTINCT(RTRIM(NOMBRE)) AS SNOMBRE FROM CAMB_BANCO_EU ORDER BY SNOMBRE";
			PreparedStatement plazaQuery = conn.prepareStatement(sqlplaza);
			if(plazaQuery == null)
			{
				EIGlobal.mensajePorTrace("EI_Bancos - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			plazaResult = plazaQuery.executeQuery();
			if(plazaResult==null)
			{
				EIGlobal.mensajePorTrace("EI_Bancos - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			}
			if(Error.equals("SUCCESS"))
			{
				//****EI_BancosII
				String nombreArchivo=IEnlace.DOWNLOAD_PATH+"EI_Bancos.html";

				EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);

//*********** 200820500


				if(ArcSal.recuperaArchivo())
				{
					StringBuffer arcLineaH = new StringBuffer("");

					arcLineaH.append("\n<html>");
					arcLineaH.append("\n<head><title>Plazas</title>");
					arcLineaH.append("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
					arcLineaH.append("<script> ");
					arcLineaH.append("function seleccionar() {	");
					arcLineaH.append("var indice = document.Forma.SelBanco.selectedIndex;	");
					arcLineaH.append("var cve = document.Forma.SelBanco.options[indice].value; ");
					arcLineaH.append("var desentramaCve = cve.split(\"|\"); ");
					arcLineaH.append("opener.document.formulario.vDescripcionBanco.value = desentramaCve[1];" );
					arcLineaH.append("}");
					arcLineaH.append("</script>");
					arcLineaH.append("\n</head>");
					arcLineaH.append("\n<body bgcolor='#FFFFFF'>");
					arcLineaH.append("\n <form name=Forma>");
					arcLineaH.append("\n   <table border=0>");
					arcLineaH.append("\n      <tr>");
					arcLineaH.append("\n        <th class='tittabdat'> Seleccione un banco  </th>");
					arcLineaH.append("\n      </tr>");
					arcLineaH.append("\n      <tr>");
					arcLineaH.append("\n        <td class='tabmovtex'>");
					arcLineaH.append("\n         <select NAME=SelBanco size=20 class='tabmovtex'>\n");

					if(!ArcSal.escribeLinea(arcLineaH.toString()))
						EIGlobal.mensajePorTrace("EI_Bancos - execute(): Algo salio mal escribiendo: "+arcLineaH.toString(), EIGlobal.NivelLog.INFO);

					plazaResult.next();

					for(int i=0; i<totalPlazas; i++)
					{
						StringBuffer arcLineaB=new StringBuffer("");

						arcLineaB.append("\n          <option value=");
						//****200820500 jsegovia agregar descripcion del banco en el value del select
						arcLineaB.append("'");
						arcLineaB.append(i);
						arcLineaB.append("|");
						arcLineaB.append(plazaResult.getString(1).trim());
						arcLineaB.append("'");

						arcLineaB.append(">");
						arcLineaB.append(plazaResult.getString(1).trim());
						arcLineaB.append("</option>");
						if(!ArcSal.escribeLinea(arcLineaB.toString()))
							EIGlobal.mensajePorTrace("EI_Bancos - execute(): Algo salio mal escribiendo: "+arcLineaB.toString(), EIGlobal.NivelLog.INFO);

						plazaResult.next();
					}

//					EIGlobal.mensajePorTrace("EI_Bancos - execute(): Se escribieron los bancos...", EIGlobal.NivelLog.INFO);
//
//					while(!ArcSal.eof())
//					{
//						if(!ArcSal.escribeLinea("                                                                                  "))
//						EIGlobal.mensajePorTrace("EI_Bancos - execute(): Algo salio mal eof()", EIGlobal.NivelLog.INFO);
//					}

					StringBuffer arcLineaF=new StringBuffer("");

					arcLineaF.append("\n         </select>");
					arcLineaF.append("\n        </td>");
					arcLineaF.append("\n      </tr>");
					arcLineaF.append("\n      <tr>");
					arcLineaF.append("\n          <td><br></td>");
					arcLineaF.append("\n      </tr>");
					arcLineaF.append("\n      <tr>");
					arcLineaF.append("\n        <td align=center>");
					arcLineaF.append("\n         <table border=0 align=center cellspacing=0 cellpadding=0>");
					arcLineaF.append("\n          <tr>");
					arcLineaF.append("\n           <td>");
					//*****200820500
					EIGlobal.mensajePorTrace("EI_Bancos - valor cerrar "+ req.getParameter("cerrar"), EIGlobal.NivelLog.INFO);

					if(req.getParameter("cerrar") == null) { //MSD Q05-24447
						//************* 200820500
						arcLineaF.append("\n            <a href='javascript:opener.Selecciona(2); window.close();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>");
						//arcLineaF.append("\n <a href='javascript:seleccionar(); window.close();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>");
					}
					else {
						arcLineaF.append("\n            <a href='javascript:opener.Selecciona(2);'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>");
						//arcLineaF.append("\n            <a href='javascript:seleccionar();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>");
					}

					arcLineaF.append("\n           </td>");
					arcLineaF.append("\n          </tr>");
					arcLineaF.append("\n         </table>");
					arcLineaF.append("\n        </td>");
					arcLineaF.append("\n      </tr>");
					arcLineaF.append("\n    </table>\n </form>\n</body>\n</html>");

					//*****200820500
					EIGlobal.mensajePorTrace("EI_Bancos - valor cerrar "+ arcLineaF.toString(), EIGlobal.NivelLog.INFO);

					if(!ArcSal.escribeLinea(arcLineaF.toString()))
						EIGlobal.mensajePorTrace("EI_Bancos - execute(): Algo salio mal escribiendo: "+arcLineaF.toString(), EIGlobal.NivelLog.INFO);


					//*****200820500
					EIGlobal.mensajePorTrace("EI_Bancos - execute(): Se escribieron los bancos...", EIGlobal.NivelLog.INFO);

					while(!ArcSal.eof())
					{
						if(!ArcSal.escribeLinea("                                                                                  "))
						EIGlobal.mensajePorTrace("EI_Bancos - execute(): Algo salio mal eof()", EIGlobal.NivelLog.INFO);
					}
					//**----------

					ArcSal.cierraArchivo();

					//###### Cambiar....
					//*************************************************************
					ArchivoRemoto archR = new ArchivoRemoto();
					if(!archR.copiaLocalARemoto("EI_Bancos.html","WEB"))
						EIGlobal.mensajePorTrace("EI_Bancos - execute(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);
					//**************************************************************
				} else {
					strPlazas.append("\n<p><br><table border=0 width=90% align=center>");
					strPlazas.append("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");
					strPlazas.append("\n<tr><th bgcolor=red><font color=white>En este momento no se pudo generar cat&aacute;logo. Por favor intente m&aacute;s tarde.<font></td></tr>");
					strPlazas.append("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");
					strPlazas.append("\n</table>");
				}
			} else {
				strPlazas.append("\n<p><br><table border=0 width=90% align=center>");
				strPlazas.append("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");
				strPlazas.append("\n<tr><th bgcolor=red><font color=white>Su transacci&oacute;n no puede ser atendida. Por favor intente m&aacute;s tarde.<font></td></tr>");
				strPlazas.append("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");
				strPlazas.append("\n</table>");
			}
		} catch( SQLException sqle ) {
			//sqle.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = sqle.getStackTrace();
			EIGlobal.mensajePorTrace("Error al iniciar catalogo de bancos", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("EI_Bancos::iniciaCatalogoBancos:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ sqle.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);			
		} catch(Exception e) {
			//e.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al iniciar catalogo de bancos", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("EI_Bancos::iniciaCatalogoBancos:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
		}finally
		  {
			try
			 {
			   conn.close();
			   contResult.close();
			   plazaResult.close();
			 }catch(Exception e) {}
		  }

		EIGlobal.mensajePorTrace("EI_Bancos - execute(): Saliendo de Catalogo de Bancos.", EIGlobal.NivelLog.INFO);
		//----200820500
		req.setAttribute("SBancos",strPlazas.toString());
		//****
		evalTemplate("/jsp/EI_Bancos.jsp", req, res);
	}

	/**
     * clavesABA()
     * Metodo para obtener las claves ABA de una cuenta
     * respecto a un banco en USA.
     * @author abanda
     * FSW: Stefanini Mexico SA. de CV.
     * Fecha de creacion 2009/03/24
     * @return
     */

    private String creaArrayClavesABA(HttpServletRequest req, HttpServletResponse res) {

    	String strArray = "";
        Connection conn = null;
        PreparedStatement TipoQuery = null;
        ResultSet TipoResult = null;
        String bancoCorresponsal = req.getParameter("vDescripcionBanco");

        try {
            // Se realiza la consulta a la BD
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if(conn == null) {
            	debug("Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
            	return null;
            }

            if(bancoCorresponsal == null || bancoCorresponsal.equals(""))
            	TipoQuery = conn.prepareStatement("SELECT CVE_ABA,NOMBRE, CIUDAD FROM CAMB_BANCO_EU");
            else
            	TipoQuery = conn.prepareStatement("SELECT CVE_ABA,NOMBRE, CIUDAD FROM CAMB_BANCO_EU WHERE NOMBRE LIKE '" + bancoCorresponsal + "'");

            if(TipoQuery == null) {
            	debug("No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
            	return null;
            }

            TipoResult = TipoQuery.executeQuery();
            if(TipoResult == null) {
            	debug("No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
            	return null;
            }

            while(TipoResult.next())
            	strArray = TipoResult.getString(1).trim() + "|" + TipoResult.getString(2).trim() + "|" + TipoResult.getString(3).trim() + "@";
        } catch( SQLException sqle ) {
            debug("EXCEPCION en creaArrayClavesABA(): " + sqle.getMessage(), EIGlobal.NivelLog.INFO);
            strArray = null;
        } finally {
            try {
            	TipoResult.close();
            } catch(Exception e) {
            }
            try {
            	conn.close();
            } catch(Exception e) {
            }
        }

        req.setAttribute("arrayCveABA", strArray);
        return strArray;
    }

      private void debug(String mensaje, EIGlobal.NivelLog nivel) {
        EIGlobal.mensajePorTrace("<debug coMtoProveedor.java> " + mensaje,nivel);
    }

}