/*Banco Santander Mexicano
  Clase cambio_instruccion -- cambio de instrumento para inversiones
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 01/03/2001 -se Agrega encabezado
 */

package mx.altec.enlace.servlets;
import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.IOException;

public class cambio_instruccion extends mx.altec.enlace.servlets.BaseServlet {

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {

      EIGlobal.mensajePorTrace("***cambio_instruccion.class Entrando" + " al Action de cambio de instruccion ", EIGlobal.NivelLog.INFO);

	  boolean sesionvalida      = SesionValida( request, response );
      String contrato_inversion = "";
      String tipo_producto      = "";
      String folio              = "";
      String coderror			= "";
      String trama_datos        = "";
      String instr				= "";
      String indicador1			= "";
      String indicador2			= "";
      StringBuffer mensaje_salida		= new StringBuffer("");
	  String numeroReferencia	= "";
      int salida;
      short suc_opera;
      String ctaposicion        = "";

	  HttpSession sess = request.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

      suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
      request.setAttribute("MenuPrincipal", session.getStrMenu());
      request.setAttribute("Fecha", ObtenFecha());
      if (session.getFacultad(session.FAC_DISPONIBLE_INVERSION))
	  {

        trama_datos=request.getParameter("CI");
        instr=request.getParameter("inst1");
	    String[] Datos=desentramaC("4|"+trama_datos,'|');
        contrato_inversion=Datos[1];
        tipo_producto=Datos[2];
        folio=Datos[3];

		ctaposicion=Datos[4];

        if (instr.equals("1"))
        {
          indicador1="S";
          indicador2="N";
        }
        else if (instr.equals("2"))
        {
          indicador1="S";
          indicador2="S";
        }
        else if (instr.equals("3"))
        {
          indicador1="N";
          indicador2="N";
        }


		String trama_entrada="1EWEB|"+session.getUserID()+"|CPCI|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+ctaposicion+"|P|"+
                               contrato_inversion+"|"+folio+"|"+tipo_producto+"|"+indicador1+"|"+indicador2+"| |";

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	    Hashtable htResult = null;

	    try {
	    	htResult = tuxGlobal.web_red( trama_entrada );
	    } catch ( java.rmi.RemoteException re ) {
	    	re.printStackTrace();
		} catch (Exception e) {}


	    coderror = ( String ) htResult.get( "BUFFER" );
	    EIGlobal.mensajePorTrace("***transferencia Cambio_instruccion Aqui<<"+coderror, EIGlobal.NivelLog.DEBUG);


       mensaje_salida.append("<table width=760 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
       mensaje_salida.append("<TR>");
       mensaje_salida.append("<TD WIDTH=163 class=tittabdat align=left>Referencia</TD>");
       mensaje_salida.append("<TD WIDTH=163 class=tittabdat align=left>Contrato</TD>");
       mensaje_salida.append("<TD WIDTH=163 class=tittabdat align=left>folio</TD>");
       mensaje_salida.append("<TD WIDTH=163 class=tittabdat align=left>Instrucci&oacute;n</TD>");
       mensaje_salida.append("<TD WIDTH=163 class=tittabdat align=left>Estatus</TD>");
       mensaje_salida.append("</TR>");

		if(coderror!=null)
	  	{
            if (coderror.length()>=16 )
                numeroReferencia=coderror.substring(8,16);
            mensaje_salida.append("<TR>");
            mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>");
			mensaje_salida.append(numeroReferencia);
			mensaje_salida.append("</TD>");
            mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>");
			mensaje_salida.append(contrato_inversion);
			mensaje_salida.append("</TD>");
            mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>");
			mensaje_salida.append(folio);
			mensaje_salida.append("</TD>");
            if (instr.equals("1"))
               mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>Reinvertir Capital </TD>");
            else if (instr.equals("2"))
               mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>Reinvertir Capital e interes</TD>");
            else if (instr.equals("3"))
               mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>Transferir Capital e intereses</TD>");
		    if (coderror.substring(0,2).equals("OK") )
               mensaje_salida.append("<TD  class=textabdatobs align=left nowrap><P ALIGN=RIGHT>Operaci&oacute;n aceptada</TD>");
   	        else if (coderror.length()>16)
			 {
               mensaje_salida.append("<TD  class=textabdatobs align=left nowrap><P ALIGN=RIGHT>");
			   mensaje_salida.append(coderror.substring(16,coderror.length()));
			   mensaje_salida.append("</TD>");
			 }
            else
               mensaje_salida.append("<TD  class=textabdatobs align=left nowrap><P ALIGN=RIGHT>Operaci&oacute;n rechazada</TD>");
        }
		else
		{

             mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>--------</TD>");
             mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>");
			 mensaje_salida.append(contrato_inversion);
			 mensaje_salida.append("</TD>");
             mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>");
			 mensaje_salida.append(folio);
			 mensaje_salida.append("</TD>");
             if (instr.equals("1"))
                mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>Reinvertir Capital </TD>");
             else if (instr.equals("2"))
                mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>Reinvertir Capital e interes</TD>");
             else if (instr.equals("3"))
                mensaje_salida.append("<TD  class=textabdatobs align=left nowrap>Transferir Capital e intereses</TD>");
             mensaje_salida.append("<TD  class=textabdatobs align=left nowrap><P ALIGN=RIGHT>Operaci&oacute;n rechazada</TD>");
        }
        mensaje_salida.append("</TR>");
        mensaje_salida.append("</TABLE>");

        request.setAttribute( "mensaje_salida",mensaje_salida.toString());
	    request.setAttribute( "MenuPrincipal", session.getStrMenu());
        request.setAttribute( "newMenu", session.getFuncionesDeMenu());
        request.setAttribute( "Encabezado",	CreaEncabezado( "Cambio de Instrucci&oacute;n",	"Inversioness &gt; Cambio de Instrucci&oacute;n", "s26230h",request) );
		request.setAttribute("banco",session.getClaveBanco());
		sess.setAttribute( "mensaje_salida",mensaje_salida);
	    sess.setAttribute( "MenuPrincipal", session.getStrMenu());
        sess.setAttribute( "newMenu", session.getFuncionesDeMenu());
        sess.setAttribute( "Encabezado",	CreaEncabezado( "Cambio de Instrucci&oacute;n",	"Inversioness &gt; Cambio de Instrucci&oacute;n", "s26230h",request) );
        request.setAttribute("web_application",Global.WEB_APPLICATION);
	    sess.setAttribute("web_application",Global.WEB_APPLICATION);
	    //<vswf:meg cambio de NASApp por Enlace 08122008>
		response.sendRedirect(response.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+IEnlace.MI_OPER_TMPL));
        EIGlobal.mensajePorTrace("***transferencia Cambio_instruccion entro al if<<", EIGlobal.NivelLog.DEBUG);

     }
      else
      {
	    despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Cambio de Instrucci&oacute;n","Inversioness &gt; Cambio de Instrucci&oacute;n", "s26230h", request, response);
      }


    }

}