/*
    Elaborado: Francisco Serrato Jimenez (fsj)
    Proyecto:  MX-2002-257
               Getronics CP Mexico
**/

package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;




public class InverSoc
    extends BaseServlet
{
    public void doGet(HttpServletRequest request,HttpServletResponse response)
        throws ServletException, IOException
    {
        defaultAction(request,response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response)
        throws ServletException, IOException
    {
        defaultAction( request, response );
    }

    public void defaultAction(HttpServletRequest request,HttpServletResponse response)
        throws IOException, ServletException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Comenzar[defaultAction();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess    = request.getSession();
        BaseResource session = (BaseResource)sess.getAttribute("session");
        String ventana       = ( request.getParameter("ventana")    !=null ) ? request.getParameter("ventana")     : "";
        EIGlobal.mensajePorTrace("-!?ventana["+ ventana +"]", EIGlobal.NivelLog.INFO);
        if( SesionValida(request,response) )
        {
            EIGlobal.mensajePorTrace("-!?***InverSoc.class Sesion Valida", EIGlobal.NivelLog.INFO);
            session.setModuloConsultar(IEnlace.MInversiones);
                 if( ventana.equals("0") )//Seleccion
                seleccionarContrato(request,response);
            else if( ventana.equals("1") )//Emisoras
                presentarEmisoras(request,response);
            else if( ventana.equals("2") )//Confirmacion
                confirmarOperacion(request,response);
            else if( ventana.equals("3") )//Comprobante
                compraVentaFondos(request,response);
        }//fin if
		/*
        else
        {
            EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Sesion NO Valida", EIGlobal.NivelLog.INFO);
            request.setAttribute("MenuPrincipal",session.getStrMenu()   );
            request.setAttribute("Fecha"        ,ObtenFecha()           );
            request.setAttribute("ContUser"     ,ObtenContUser(request) );
            request.setAttribute("MsgError"     ,IEnlace.MSG_PAG_NO_DISP);
            evalTemplate(IEnlace.ERROR_TMPL,request,response);
        }//fin else
		*/

        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Terminar[defaultAction();]", EIGlobal.NivelLog.INFO);
    }

    public void seleccionarContrato
        (
            HttpServletRequest request,
            HttpServletResponse response
        )
        throws IOException, ServletException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Comenzar[seleccionarContrato();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess    = request.getSession();
        BaseResource session = (BaseResource)sess.getAttribute("session");
        sess.removeAttribute("_Posicion");
        sess.removeAttribute("_listEmisoras");
        request.setAttribute("cuenta"       ,""                           );
        request.setAttribute("Mensaje"      ,""                           );
        request.setAttribute("FechaHoy"     ,ObtenFecha(false)            );
        request.setAttribute("Fecha"        ,ObtenFecha(true)             );
        request.setAttribute("ContUser"     ,ObtenContUser(request)       );
        request.setAttribute("newMenu"      ,session.getFuncionesDeMenu() );
        request.setAttribute("MenuPrincipal",session.getStrMenu()         );
        request.setAttribute("Encabezado"   ,CreaEncabezado("Selecci&oacute;n de Contrato Sociedades de Inversi&oacute;n", "Tesorer&iacute;a &gt; Inversiones &gt; Sociedades de Inversi&oacute;n","s26110h",request));

        //TODO CU3011 Inicio de flujo
        /**
 		 * VSWF - FVC - I
 		 * 17/Enero/2007
 		 * 02-Febreo-2007 Modificaci�n FVC
 		 */
        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
        try{
	        BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
	 		BitaHelper bh = new BitaHelperImpl(request, session, sess);

	 		BitaTransacBean bt = new BitaTransacBean();

	 		if(request.getParameter(BitaConstants.FLUJO) != null)
	 			bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
	 		else
	 			bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
	 		bt = (BitaTransacBean)bh.llenarBean(bt);
	 		bt.setNumBit(BitaConstants.ER_INVERSIONES_FONDOS_INVERSION_ENTRA);
	 		bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());


	 		BitaHandler.getInstance().insertBitaTransac(bt);
 		}catch(Exception e){
 			e.printStackTrace();
 		}
        }
 		/**
 		 * VSWF - FVC - F
 		 */
        evalTemplate("/jsp/InverSoc.jsp"   ,request,response);
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Terminar[seleccionarContrato();]", EIGlobal.NivelLog.INFO);
    }

    public void presentarEmisoras
        (
            HttpServletRequest request,
            HttpServletResponse response
        )
        throws IOException, ServletException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Comenzar[presentarEmisoras();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess       = request.getSession();
        BaseResource session    = (BaseResource)sess.getAttribute("session");
        String HTMLEmisoras = listarEmisoras(request,response);
        request.setAttribute("cuenta"       ,""                           );
        request.setAttribute("Mensaje"      ,""                           );
        request.setAttribute("Fecha"        ,ObtenFecha(true)             );
        request.setAttribute("FechaHoy"     ,ObtenFecha(false)            );
        request.setAttribute("ContUser"     ,ObtenContUser(request)       );
        request.setAttribute("emisoras"     ,HTMLEmisoras                 );
        request.setAttribute("newMenu"      ,session.getFuncionesDeMenu() );
        request.setAttribute("MenuPrincipal",session.getStrMenu()         );
        request.setAttribute("Encabezado"   ,CreaEncabezado("Lista de Emisoras Sociedades de Inversi&oacute;n","Tesorer&iacute;a &gt; Inversiones &gt; Sociedades de Inversi&oacute;n","s26120h", request) );
        evalTemplate("/jsp/InverSoc1.jsp",request,response);
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Terminar[presentarEmisoras();]", EIGlobal.NivelLog.INFO);
    }

    public String listarEmisoras
        (
            HttpServletRequest request,
            HttpServletResponse response
        )
        throws  ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Comenzar[listarEmisoras();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess    = request.getSession();
        BaseResource session = (BaseResource)sess.getAttribute("session");
        String cboCuenta     = ( request.getParameter("cboCuenta")  !=null ) ? request.getParameter("cboCuenta")   : "";
        String opcCompVent   = ( request.getParameter("opcCompVent")!=null ) ? request.getParameter("opcCompVent") : "C";
        String estilo        = "";
        String registro      = "";
        String HTMLEmisoras  = "";
        EIGlobal.mensajePorTrace("-!?cboCuenta["+ cboCuenta +"] opcCompVent["+ opcCompVent +"]", EIGlobal.NivelLog.DEBUG);
        Vector listEmisoras  = ObtenerEmisoras(request,response);
        String datCboCuenta[] = desentrama(cboCuenta,'|');
        HTMLEmisoras += "  <table width=\"600\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">\n";
        HTMLEmisoras += "   <tr>\n";
        HTMLEmisoras += "    <td class=\"texencconbol\" align=\"left\">Contrato Fondos de Inversi&oacute;n: "+ datCboCuenta[0] +"</td>\n";
        HTMLEmisoras += "   </tr>\n";
        HTMLEmisoras += "   <tr>\n";
        HTMLEmisoras += "    <td class=\"texencconbol\" align=\"left\"> &nbsp; </td>\n";
        HTMLEmisoras += "   </tr>\n";
        HTMLEmisoras += "   <tr>\n";
        HTMLEmisoras += "    <td class=\"texencconbol\" align=\"left\">Emisoras</td>\n";
        HTMLEmisoras += "   </tr>\n";
        HTMLEmisoras += "  </table>\n";
        HTMLEmisoras += "  <table width=\"600\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">\n";
        HTMLEmisoras += "   <tr>\n";
        HTMLEmisoras += "    <td class=\"tittabdat\" align=\"center\" width=\"40\" >&nbsp;</td>\n";
        HTMLEmisoras += "    <td class=\"tittabdat\" align=\"center\" width=\"90\" >EMISORA</td>\n";
		HTMLEmisoras += "    <td class=\"tittabdat\" align=\"center\" > SERIE </td>\n";
        HTMLEmisoras += "    <td class=\"tittabdat\" align=\"center\" width=\"90\" >PRECIO</td>\n";

        HTMLEmisoras += "    <td class=\"tittabdat\" align=\"center\" width=\"170\" colspan=\"2\">POSICI&Oacute;N<br>T&Iacute;TULOS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; SALDOS</td>\n";
        HTMLEmisoras += "    <td class=\"tittabdat\" align=\"center\" width=\"90\" >FECHA LIQUIDACI&Oacute;N</td>\n";
        HTMLEmisoras += "    <td class=\"tittabdat\" align=\"center\" width=\"120\">HORARIO OPERACI&Oacute;N<br>INICIO &nbsp; - &nbsp; FIN </td>\n";
        HTMLEmisoras += "   </tr>\n";

        //TODO CU3011 Selecci�n de contrato
				        /**
				 		 * VSWF - FVC - I
				 		 * 17/Enero/2007
				 		 */
				        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				        try{
					        BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
					 		BitaHelper bh = new BitaHelperImpl(request, session, sess);

					 		BitaTransacBean bt = new BitaTransacBean();
					 		bt = (BitaTransacBean)bh.llenarBean(bt);
					 		bt.setContrato((session.getContractNumber() == null)?" ": session.getContractNumber());
					 		bt.setNumBit(BitaConstants.ER_INVERSIONES_FONDOS_INVERSION_SEL_CONTRATO);
					 		bt.setCctaDest((datCboCuenta[0] == null)?" ":datCboCuenta[0]);
					 		BitaHandler.getInstance().insertBitaTransac(bt);
				 		}catch(Exception e){
				 			e.printStackTrace();
				 		}
				        }
				 		/**
				 		 * VSWF - FVC - F
 		 */

        //Obtener Posicion del Contrato Fondos
        Hashtable datPosicion = ConsultaPosicion(request,response);
        Hashtable datPosi = null;
        String[] arrayDatPosi = null;
        for(int i=0,idx=0; i<listEmisoras.size() ;i++)
        {
            String titulos = "";
            String importe = "";
			String serie = "";
            String[] obtTrama = (String[])listEmisoras.elementAt(i);
            if( obtTrama[6].equals(opcCompVent) )
            {
            	serie   = ((obtTrama[3])!=null ) ? obtTrama[3] : " -- ";
            	if(datPosicion!=null){
            		datPosi = (Hashtable) datPosicion.get(obtTrama[2]);
            		if(datPosi!=null){
            			arrayDatPosi = (String[]) datPosi.get(serie);
            			if(arrayDatPosi!=null){
            				titulos = arrayDatPosi[0];
            				importe = arrayDatPosi[1];
            			}else{
            				titulos = "0";
            				importe = "0.0";
            			}
            		}else{
            			titulos = "0";
        				importe = "0.0";
            		}
            	}else{
            		titulos = "0";
    				importe = "0.0";
            	}
            	//titulos = ( datPosicion.get(obtTrama[2])!=null ) ? ((String[])datPosicion.get(obtTrama[2]))[0] : "0";
                //importe = ( datPosicion.get(obtTrama[2])!=null ) ? ((String[])datPosicion.get(obtTrama[2]))[1] : "0.00";
                if( (idx%2)==0 )estilo = "textabdatobs";
                else            estilo = "textabdatcla";
                HTMLEmisoras +="   <tr>\n";
                if ( idx==0 )
				    HTMLEmisoras += "    <td class=\""+ estilo +"\" align=\"center\"><input type=\"radio\" Checked name=\"emisoras\" value=\""+ obtTrama[2] +"|"+ obtTrama[3] +"|"+ obtTrama[5] +"|"+ obtTrama[9] +"|\"></td>\n";
                else
					HTMLEmisoras += "    <td class=\""+ estilo +"\" align=\"center\"><input type=\"radio\"         name=\"emisoras\" value=\""+ obtTrama[2] +"|"+ obtTrama[3] +"|"+ obtTrama[5] +"|"+ obtTrama[9] +"|\"></td>\n";
                HTMLEmisoras += "    <td class=\""+ estilo +"\" nowrap align=\"center\">&nbsp; "+ obtTrama[2] +"</td>\n";
                HTMLEmisoras += "    <td class=\""+ estilo +"\" nowrap align=\"center\" width=\"85\">&nbsp;"+ serie +" &nbsp;</td>\n";
				HTMLEmisoras += "    <td class=\""+ estilo +"\" nowrap align=\"right\" >&nbsp;$"+ formatear(obtTrama[5],8) +" &nbsp;</td>\n";
				HTMLEmisoras += "    <td class=\""+ estilo +"\" nowrap align=\"right\" width=\"85\">&nbsp;"+ titulos +" &nbsp;</td>\n";
                HTMLEmisoras += "    <td class=\""+ estilo +"\" nowrap align=\"right\" width=\"85\">&nbsp;$"+ importe +" &nbsp;</td>\n";
                HTMLEmisoras += "    <td class=\""+ estilo +"\" nowrap align=\"center\">&nbsp; "+ fchFormat(obtTrama[9]) +" &nbsp;</td>\n";
                HTMLEmisoras += "    <td class=\""+ estilo +"\" nowrap align=\"center\">&nbsp; "+ obtTrama[12] +" - "+ obtTrama[13] +" &nbsp;</td>\n";
                HTMLEmisoras +="   </tr>\n";
                idx ++;
            }//fin if
        }//fin for i
        if( listEmisoras.isEmpty() )  // No existen Emisoras
        {
            HTMLEmisoras += "   <tr>\n";
            HTMLEmisoras += "    <td align=\"center\" class=\"textabdatobs\" colspan=\"8\">No Existen Emisoras o el contrato no puede operar Fondos de Inversi&oacute;n</td>\n";
            HTMLEmisoras += "   </tr>\n";
        }//fin if
        HTMLEmisoras += "  </table><br>\n";
        HTMLEmisoras += "  <table width=\"600\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">\n";
        HTMLEmisoras += "   <tr>\n";
        if( opcCompVent.equals("C") )HTMLEmisoras += "    <td class=\"texencconbol\" align=\"right\" ><input type=\"radio\" name=\"opcCompVent\" value=\"C\" Checked onClick=\"cargarEmisoras('C');\"> Compra </td>\n";
        else                         HTMLEmisoras += "    <td class=\"texencconbol\" align=\"right\" ><input type=\"radio\" name=\"opcCompVent\" value=\"C\"         onClick=\"cargarEmisoras('C');\"> Compra </td>\n";
        HTMLEmisoras += "    <td class=\"texencconbol\" align=\"center\"> &nbsp; </td>\n";
        if( opcCompVent.equals("C") )HTMLEmisoras += "    <td class=\"texencconbol\" align=\"left\"  ><input type=\"radio\" name=\"opcCompVent\" value=\"V\"         onClick=\"cargarEmisoras('V');\"> Venta  </td>\n";
        else                         HTMLEmisoras += "    <td class=\"texencconbol\" align=\"left\"  ><input type=\"radio\" name=\"opcCompVent\" value=\"V\" Checked onClick=\"cargarEmisoras('V');\"> Venta  </td>\n";
        HTMLEmisoras += "   </tr>\n";
        HTMLEmisoras += "   <tr>\n";
        HTMLEmisoras += "    <td colspan=\"3\" align=\"center\"> &nbsp; </td>\n";
        HTMLEmisoras += "   </tr>\n";
        HTMLEmisoras += "   <tr>\n";
        HTMLEmisoras += "    <td class=\"tabmovtex\" align=\"right\" >Importe:        <input type=\"text\" name=\"importe\" size=\"15\" maxlength=\"12\" class=\"tabmovtex\" onFocus=\"ValidaCampo(1);\"></td>\n";
        HTMLEmisoras += "    <td class=\"tabmovtex\" align=\"center\"> &nbsp; </td>\n";
        HTMLEmisoras += "    <td class=\"tabmovtex\" align=\"left\"  >T&iacute;tulos: <input type=\"text\" name=\"titulos\" size=\"15\" maxlength=\"12\" class=\"tabmovtex\" onFocus=\"ValidaCampo(2);\"></td>\n";
        HTMLEmisoras += "   </tr>\n";
        HTMLEmisoras += "  </table><br>\n";
        HTMLEmisoras += "  <input type=\"hidden\" name=\"cboCuenta\" value=\""+ cboCuenta +"\">\n";
        HTMLEmisoras += "  <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
        HTMLEmisoras += "   <tr>\n";
        if( listEmisoras.isEmpty() )HTMLEmisoras += "    <td align=\"center\"><a href=\"JavaScript:Regresar();\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25320.gif\" alt=\"Regresar\"></a></td>\n";
        else                        HTMLEmisoras += "    <td align=\"center\"><a href=\"JavaScript:ValidaForma(document.frmInverSoc2);\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25610.gif\" alt=\"Siguiente\"></a> &nbsp; &nbsp; &nbsp;<a href=\"JavaScript:Regresar();\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25320.gif\" alt=\"Regresar\"></a></td>\n";
        HTMLEmisoras += "   </tr>\n";
        HTMLEmisoras += "  </table><br>\n";
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Terminar[listarEmisoras();]", EIGlobal.NivelLog.INFO);
        return HTMLEmisoras;
    }

    private Vector ObtenerEmisoras
        (
            HttpServletRequest request,
            HttpServletResponse response
        )
        throws  ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Comenzar[ObtenerEmisoras();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess    = request.getSession();
        BaseResource session = (BaseResource)sess.getAttribute("session");
        String cboCuenta     = ( request.getParameter("cboCuenta")  !=null ) ? request.getParameter("cboCuenta")   : "";
        String trama_entrada       = "";
        String trama_salida        = "";
        String path_archivo        = "";
        String nombre_archivo      = "";
        String trama_salidaRedSrvr = "";
        String registro            = "";
        Vector listEmisoras        = new Vector();
        if( sess.getAttribute("_listEmisoras")!=null && 5 == 3) //PARA EVITAR QUE OCURRA EL BUG
        {
            EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Obtener Lista de Memoria...", EIGlobal.NivelLog.INFO);
            listEmisoras = (Vector)sess.getAttribute("_listEmisoras");
        }//fin if
        else
        {
            String datCboCuenta[] = desentrama(cboCuenta,'|');
            EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Obtener Lista de Servicio...", EIGlobal.NivelLog.INFO);
            ServicioTux tuxGlobal = new ServicioTux();
            //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
            trama_entrada = "2EWEB|"+ session.getUserID8() +"|CATA|"+ session.getContractNumber() +"|"+ session.getUserID8() +"|"+ session.getUserProfile() +"|"+ datCboCuenta[0];

		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = request.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada



            EIGlobal.mensajePorTrace(fechaHr+"-!?***InverSoc.class ###Trama Entrada["+ trama_entrada +"]", EIGlobal.NivelLog.DEBUG);
            try
            {
                Hashtable ht = tuxGlobal.web_red(trama_entrada);
                trama_salida = (String)ht.get("BUFFER");
                trama_salida = (trama_salida==null) ? "" : trama_salida;
            }catch(Exception e){  }
            EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Trama Salida["+ trama_salida +"]", EIGlobal.NivelLog.DEBUG);
            path_archivo = trama_salida;
            nombre_archivo = EIGlobal.BuscarToken(path_archivo,'/', 5);
            BufferedReader fileSua = null;
            try
            {
                // Copia Remota
                ArchivoRemoto archivo_remoto = new ArchivoRemoto();
                if( !archivo_remoto.copia(nombre_archivo) )
                    EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Error remote shell...", EIGlobal.NivelLog.INFO);
                else
                    EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Ok remote shell...   ", EIGlobal.NivelLog.INFO);
                File drvSua = new File(path_archivo);
                fileSua = new BufferedReader(new FileReader(drvSua));
                trama_salidaRedSrvr = fileSua.readLine();
                EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Path["+ path_archivo +"]", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Trama["+ trama_salidaRedSrvr +"]", EIGlobal.NivelLog.INFO);
                if( trama_salidaRedSrvr.trim().equals( "" ) )
                    trama_salidaRedSrvr = fileSua.readLine();
                if (trama_salidaRedSrvr.startsWith("OK ")) // Archivo OK
                {
                    while( (registro = fileSua.readLine())!=null )
                    {
                        if( registro.trim().equals("") )
                            registro = fileSua.readLine();
                        if( registro.startsWith("13;") )
                            listEmisoras.addElement(desentrama(registro,';'));
                    }//fin while
                }//fin if
                fileSua.close();
                sess.setAttribute("_listEmisoras",listEmisoras);
            }catch(Exception e)
                {
                    EIGlobal.mensajePorTrace("-!?***InverSoc.class  Exception ObtenerEmisoras["+ e +"]", EIGlobal.NivelLog.INFO);
                    sess.removeAttribute("_listEmisoras");
                }
                finally
                {
                    if( fileSua!=null )
                        try{ fileSua.close(); }catch(Exception e){  }
                    fileSua = null;
                }
        }//fin else
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Terminar[ObtenerEmisoras();] {"+ listEmisoras.size() +"}", EIGlobal.NivelLog.INFO);
        return listEmisoras;
    }

    public void confirmarOperacion
        (
            HttpServletRequest request,
            HttpServletResponse response
        )
        throws  ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Comenzar[confirmarOperacion();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess    = request.getSession();
        BaseResource session = (BaseResource)sess.getAttribute("session");
        String HTMLConfirmar = "";
        String importe       = ( request.getParameter("importe")      !=null ) ? request.getParameter("importe")       : "";
        String titulos       = ( request.getParameter("titulos")      !=null ) ? request.getParameter("titulos")       : "";
        String emisoras      = ( request.getParameter("emisoras")     !=null ) ? request.getParameter("emisoras")      : "";
        String cboCuenta     = ( request.getParameter("cboCuenta")    !=null ) ? request.getParameter("cboCuenta")     : "";
        String opcCompVent   = ( request.getParameter("opcCompVent")  !=null ) ? request.getParameter("opcCompVent")   : "";
        EIGlobal.mensajePorTrace("-!?importe["+ importe +"] titulos["+ titulos +"] emisoras["+ emisoras +"] cboCuenta["+ cboCuenta +"] opcCompVent["+ opcCompVent +"]", EIGlobal.NivelLog.DEBUG);
        String[] datEmisoras   = desentrama(emisoras,'|');      //emisora|serie|precio|fecha_operacion|
        String[] datCboCuenta  = desentrama(cboCuenta,'|');     //cuenta|descripcion|tipo_relacion|tipo_cuenta|tipo_persona|
        String[] datCalculados = calcularDatos(titulos,importe,datEmisoras[2]);
        importe = ( importe.trim().equals("") ) ? "0" : importe;
        titulos = ( titulos.trim().equals("") ) ? "0" : titulos;

        //TODO CU3011 Operacion solicitada
        /**
 		 * VSWF - FVC - I
 		 * 17/Enero/2007
 		 */
        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
        try{
	        BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
	 		BitaHelper bh = new BitaHelperImpl(request, session, sess);

	 		BitaTransacBean bt = new BitaTransacBean();
	 		bt = (BitaTransacBean)bh.llenarBean(bt);
	 		bt.setContrato((session.getContractNumber() == null)?" ": session.getContractNumber());
	 		bt.setNumBit(BitaConstants.ER_INVERSIONES_FONDOS_INVERSION_OPER_SOLIC);
	 		bt.setImporte(Double.parseDouble((importe == null)?"0":importe));
	 		bt.setNumTit(Long.parseLong((titulos == null)?"0":titulos));
	 		bt.setCctaOrig((datCboCuenta[0] == null)?" ":datCboCuenta[0]);
	 		bt.setCctaDest((datEmisoras[0] == null)?" ":datEmisoras[0]);
	 		/*VSWF I Autor=BMB fecha=08-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario*/
	 		bt.setBeneficiario((datEmisoras[0] == null)?" ":datEmisoras[0].trim());
	 		/*VSWF F*/

	 		BitaHandler.getInstance().insertBitaTransac(bt);
 		}catch(Exception e){
 			e.printStackTrace();
 		}
        }
 		/**
 		 * VSWF - FVC - F
 		 */




        HTMLConfirmar += "  <table border=\"0\" cellspacing=\"2\" cellpadding=\"3\" width=\"580\">\n";
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "    <td class=\"tittabdat\"    width=\"85\" align=\"left\" nowrap>Contrato Fondos de Inversi&oacute;n</td>\n";
        HTMLConfirmar += "    <td class=\"textabdatcla\" width=\"85\" align=\"left\" nowrap>&nbsp; "+ datCboCuenta[0] +"</td>\n";
        HTMLConfirmar += "    <td colspan=\"5\"> &nbsp; </td>\n";
        HTMLConfirmar += "   </tr>\n";
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "    <td class=\"tittabdat\"    width=\"85\" align=\"left\" nowrap>Operaci&oacute;n </td>\n";
        HTMLConfirmar += "    <td class=\"textabdatcla\" width=\"85\" align=\"left\" nowrap>&nbsp; "+ (opcCompVent.equals("C") ? "COMPRA" : "VENTA") +"</td>\n";
        HTMLConfirmar += "    <td colspan=\"5\"> &nbsp; </td>\n";
        HTMLConfirmar += "   </tr>\n";
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "    <td colspan=\"6\"> &nbsp; </td>\n";
        HTMLConfirmar += "   </tr>\n";
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "    <td class=\"tittabdat\" width=\"85\" align=\"center\" nowrap>Emisora                 </td>\n";
        HTMLConfirmar += "    <td class=\"tittabdat\" width=\"85\" align=\"center\" nowrap>Precio                  </td>\n";
        HTMLConfirmar += "    <td class=\"tittabdat\" width=\"85\" align=\"center\" nowrap>T&iacute;tulos          </td>\n";
        HTMLConfirmar += "    <td class=\"tittabdat\" width=\"85\" align=\"center\" nowrap>Importe                 </td>\n";
        HTMLConfirmar += "    <td class=\"tittabdat\" width=\"85\" align=\"center\" nowrap>Fecha Liquidaci&oacute;n</td>\n";
        HTMLConfirmar += "   </tr>\n";
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "    <td class=\"textabdatcla\" align=\"center\">&nbsp; "+ datEmisoras[0]                +"</td>\n";
        HTMLConfirmar += "    <td class=\"textabdatcla\" align=\"center\">&nbsp;$"+ formatear(datEmisoras[2]  ,8) +"</td>\n";
        HTMLConfirmar += "    <td class=\"textabdatcla\" align=\"center\">&nbsp; "+ formatear(datCalculados[1],0) +"</td>\n";
        HTMLConfirmar += "    <td class=\"textabdatcla\" align=\"center\">&nbsp;$"+ formatear(datCalculados[0],2) +"</td>\n";
        HTMLConfirmar += "    <td class=\"textabdatcla\" align=\"center\">&nbsp; "+ fchFormat(datEmisoras[3])     +"</td>\n";
        HTMLConfirmar += "   </tr>\n";
        if( opcCompVent.equals("V") && titulos.equals("0") )
        {
            HTMLConfirmar += "   <tr>\n";
            HTMLConfirmar += "    <td colspan=\"6\"align=\"right\" class=\"texencconbol\">*El Importe de la operaci&oacute;n puede variar +/- el Precio de un T&iacute;tulo. &nbsp; &nbsp; &nbsp;</td>\n";
            HTMLConfirmar += "   </tr>\n";
        }//fin if
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "    <td colspan=\"6\"> &nbsp; </td>\n";
        HTMLConfirmar += "   </tr>\n";
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "   <tr>\n";
        HTMLConfirmar += "    <td colspan=\"6\" align=\"center\"><a href=\"JavaScript:ValidaForma(document.frmInverSoc2);\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25280.gif\" alt=\"Aceptar\" ></a>&nbsp; &nbsp; &nbsp;<a href=\"JavaScript:scrImpresion();\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25240.gif\" alt=\"Imprimir\"></a>&nbsp; &nbsp; &nbsp;<a href=\"JavaScript:Regresar();\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25320.gif\" alt=\"Regresar\"></a></td>\n";
        HTMLConfirmar += "   </tr>\n";
        HTMLConfirmar += "  </table><br>\n";
        HTMLConfirmar += "  <input type=\"hidden\" name=\"importe\"     value=\""+ importe     +"\">\n";
        HTMLConfirmar += "  <input type=\"hidden\" name=\"titulos\"     value=\""+ titulos     +"\">\n";
        HTMLConfirmar += "  <input type=\"hidden\" name=\"emisoras\"    value=\""+ emisoras    +"\">\n";
        HTMLConfirmar += "  <input type=\"hidden\" name=\"cboCuenta\"   value=\""+ cboCuenta   +"\">\n";
        HTMLConfirmar += "  <input type=\"hidden\" name=\"opcCompVent\" value=\""+ opcCompVent +"\">\n";
        request.setAttribute("cuenta"       ,""                           );
        request.setAttribute("Mensaje"      ,""                           );
        request.setAttribute("Fecha"        ,ObtenFecha(true)             );
        request.setAttribute("FechaHoy"     ,ObtenFecha(false)            );
        request.setAttribute("ContUser"     ,ObtenContUser(request)       );
        request.setAttribute("confirmar"    ,HTMLConfirmar                );
        request.setAttribute("newMenu"      ,session.getFuncionesDeMenu() );
        request.setAttribute("MenuPrincipal",session.getStrMenu()         );
        request.setAttribute("Encabezado"   ,CreaEncabezado("Confirmaci&oacute;n Compra/Venta Sociedades de Inversi&oacute;n","Tesorer&iacute;a &gt; Inversiones &gt; Sociedades de Inversi&oacute;n","s26120h", request) );
        evalTemplate("/jsp/InverSoc2.jsp"   ,request,response);
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Terminar[confirmarOperacion();]", EIGlobal.NivelLog.INFO);
    }

    public void compraVentaFondos
        (
            HttpServletRequest request,
            HttpServletResponse response
        )
        throws  ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Comenzar[compraVentaFondos();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess    = request.getSession();
        BaseResource session = (BaseResource)sess.getAttribute("session");
        String importe       = ( request.getParameter("importe")      !=null ) ? request.getParameter("importe")       : "";
        String titulos       = ( request.getParameter("titulos")      !=null ) ? request.getParameter("titulos")       : "";
        String emisoras      = ( request.getParameter("emisoras")     !=null ) ? request.getParameter("emisoras")      : "";
        String cboCuenta     = ( request.getParameter("cboCuenta")    !=null ) ? request.getParameter("cboCuenta")     : "";
        String opcCompVent   = ( request.getParameter("opcCompVent")  !=null ) ? request.getParameter("opcCompVent")   : "";
        EIGlobal.mensajePorTrace("-!?emisoras["+ emisoras +"] cboCuenta["+ cboCuenta +"] opcCompVent["+ opcCompVent +"]", EIGlobal.NivelLog.DEBUG);
        String[] datEmisoras    = desentrama(emisoras,'|');      //emisora|serie|precio|fecha_operacion|
        String[] datCboCuenta   = desentrama(cboCuenta,'|');     //cuenta|descripcion|tipo_relacion|tipo_cuenta|tipo_persona|
        String trama_entrada    = "";
        String trama_salida     = "";
        String operacion        = "";
        String referencia       = "";
        String salidaReferencia = "";
        String HTMLComprobante  = "";




        /*
         *********************INICIO Cambios. everis  30/11/2007 ***********************************************************
         */


        //******************** Declaraci�n de variable auxiliar para series: "ser" **************************
        //String ser = "00";

        if( opcCompVent.equals("C") )operacion = "CPSI";
        else                         operacion = "VTSI";




		//*************************Cambios en la trama para que la serie este dentro de la misma  ***************
		//********************everis  30/11/2007 *******************************

		EIGlobal.mensajePorTrace("**************************************CAMBIOS:", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("**************************************Serie Inicio:" + datEmisoras[1], EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("**************************************Entra al ciclo IF", EIGlobal.NivelLog.DEBUG);



			if(datEmisoras[1].equals("B") || datEmisoras[1].equals("*"))
			{
				EIGlobal.mensajePorTrace("**************************************Ciclo.....-> IF", EIGlobal.NivelLog.DEBUG);
				datEmisoras[1] = "B";
			}
			else
			{
				EIGlobal.mensajePorTrace("**************************************Ciclo.....-> ELSE", EIGlobal.NivelLog.DEBUG);
				datEmisoras[1] = datEmisoras[1];
			}

		//********************everis  30/11/2007 *******************************
		//********************valor final que tiene la variable correspondiente al tipo de sesi�n *******************************
		EIGlobal.mensajePorTrace("**************************************Serie Fin:" + datEmisoras[1], EIGlobal.NivelLog.DEBUG);

		// Se agrega try para Validaci�n de Cuentas - NVS
		ValidaCuentaContrato valCtas = null;
		String datoscta[] = null;

		try{
			valCtas = new ValidaCuentaContrato();
	    	datoscta = valCtas.obtenDatosValidaCuenta(
	    			session.getContractNumber(),
	    			session.getUserID8(),
	    			session.getUserProfile(),
	    			datCboCuenta[0],
	    			IEnlace.MInversiones
	    	);

	    	// Si la cuenta es invalida manda error, en caso contrario continua
	    	// flujo normal
			if(datoscta == null) {
				valCtas.closeTransaction();
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("Encabezado", CreaEncabezado("Sociedades de Inversi&oacute;n","Tesorer&iacute;a &gt; Inversiones &gt; Sociedades de Inversi&oacute;n", "s26120h", request));
				request.setAttribute("Mensaje", "Error H701: Cuenta inv&aacute;lida.");
				evalTemplate( "/jsp/EI_Mensaje2.jsp", request, response);
			}
			// Inicia flujo normal despues de validar cuentas
			else {

				//********************everis  30/11/2007 *******************************
				//********************el campo correspondiente a la sesi�n es  agregado a la trama de datos *******************************
		        trama_entrada = "1EWEB|"+ session.getUserID8() +"|"+ operacion +"|"+ session.getContractNumber() +"|"+ session.getUserID8() +"|"+ session.getUserProfile() +"|"+ datCboCuenta[0] +"|"+ datCboCuenta[2] +"|"+ datEmisoras[0]+"@"+datEmisoras[1] +"|F|"+ importe +"|"+ titulos +"|"+ datEmisoras[0] +"| |";


				/*
				 *********************FIN Cambios. everis  30/11/2007 ***********************************************************
				 */


		        //trama_entrada = "1EWEB|"+ session.getUserID8() +"|"+ operacion +"|"+ session.getContractNumber() +"|"+ session.getUserID8() +"|"+ session.getUserProfile() +"|"+ datCboCuenta[0] +"|"+ datCboCuenta[2] +"|"+ datEmisoras[0] +"|"+ datCboCuenta[4] +"|"+ importe +"|"+ titulos +"|"+ datEmisoras[0] +"| |";

				 //MSD Q05-0029909 inicio lineas agregadas
				 String IP = " ";
				 String fechaHr = "";												//variables locales al m�todo
				 IP = request.getRemoteAddr();											//ObtenerIP
				 java.util.Date fechaHrAct = new java.util.Date();
				 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
				 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora

				 //MSD Q05-0029909 fin lineas agregada
		        EIGlobal.mensajePorTrace(fechaHr+"-!?***InverSoc.class ###Trama Entrada["+ trama_entrada +"]", EIGlobal.NivelLog.DEBUG);
		        ServicioTux tuxGlobal = new ServicioTux();
		        //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		        try
		        {
		            Hashtable htResult = tuxGlobal.web_red(trama_entrada);
		            trama_salida = (String) htResult.get("BUFFER");
		            trama_salida = (trama_salida==null) ? "" : trama_salida;
		        }catch(Exception e){  }
		        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Trama Salida["+ trama_salida +"]", EIGlobal.NivelLog.DEBUG);
		        boolean errorLF57 = true;
		        if( trama_salida.startsWith("OK ") )
		        {
		            if (trama_salida.substring(16,20).trim().startsWith("MANC") )
		            {
		                salidaReferencia += "Referencia: " + trama_salida.substring(8,16).trim();
		                salidaReferencia += "  Operaci&oacute;n Mancomunada.";
		            }//fin if
		            else
		            {
		                String datCompVent[] = desentrama(trama_salida,'|');
		                errorLF57 = false;
		                HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\" align=\"center\">\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td width=\"247\" valign=\"top\" class=\"titpag\">Comprobante de operaci&oacute;n</td>\n";
		                HTMLComprobante += "     <td valign=\"top\" class=\"titpag\" height=\"40\"><img src=\"/gifs/EnlaceMig/glo25030.gif\" width=\"152\" height=\"30\" alt=\"Enlace\"></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "   </table>\n";
		                HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\"></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\"></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"8\" alt=\"..\"></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "   </table>\n";
		                HTMLComprobante += "   <table width=\"400\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td align=\"center\">\n";
		                HTMLComprobante += "      <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		                HTMLComprobante += "       <tr>\n";
		                HTMLComprobante += "        <td colspan=\"3\"> </td>\n";
		                HTMLComprobante += "       </tr>\n";
		                HTMLComprobante += "       <tr>\n";
		                HTMLComprobante += "        <td width=\"21\" background=\"/gifs/EnlaceMig/gfo25030.gif\"><img src=\"/gifs/EnlaceMig/gau00010.gif\" width=\"21\" height=\"2\" alt=\"..\" name=\"..\"></td>\n";
		                HTMLComprobante += "        <td width=\"428\" height=\"150\" valign=\"top\" align=\"center\">\n";
		                HTMLComprobante += "         <table width=\"380\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" background=\"/gifs/EnlaceMig/gau25010.gif\">\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Contrato:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ session.getContractNumber() +" "+ session.getNombreContrato() +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Contrato Fondos de Inversi&oacute;n:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ datCboCuenta[0] +" "+ datCboCuenta[1] +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Operaci&oacute;n:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ (opcCompVent.equals("C") ? "COMPRA" : "VENTA") +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Status Orden:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ datCompVent[6] +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Emisora:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ datEmisoras[0] +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Precio:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>$"+ formatear(datCompVent[2],8) +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">T&iacute;tulos:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ formatear(datCompVent[3],0) +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Importe:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>$"+ formatear(datCompVent[4],2) +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Fecha Operaci&oacute;n:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ fchFormat(datCompVent[7]) +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">N&uacute;mero de Orden:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ datCompVent[5] +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "          <tr>\n";
		                HTMLComprobante += "           <td class=\"tittabcom\" align=\"right\" width=\"0\">Referencia:</td>\n";
		                HTMLComprobante += "           <td class=\"textabcom\" align=\"left\" nowrap>"+ datCompVent[1] +"</td>\n";
		                HTMLComprobante += "          </tr>\n";
		                HTMLComprobante += "         </table>\n";
		                HTMLComprobante += "        </td>\n";
		                HTMLComprobante += "        <td width=\"21\" background=\"/gifs/EnlaceMig/gfo25040.gif\"><img src=\"/gifs/EnlaceMig/gau00010.gif\" width=\"21\" height=\"2\" alt=\"..\" name=\"..\"></td>\n";
		                HTMLComprobante += "       </tr>\n";
		                HTMLComprobante += "      </table>\n";
		                HTMLComprobante += "     </td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "   </table>\n";
		                HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"8\" alt=\"..\" name=\".\"></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "   </table><br><br>\n";
		                HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		                HTMLComprobante += "    <tr>\n";
		                HTMLComprobante += "     <td align=\"center\"><a href=\"JavaScript:scrImpresion();\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25240.gif\" alt=\"Imprimir\"></a> &nbsp; &nbsp; &nbsp;<a href=\"JavaScript:Regresar();\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25320.gif\" alt=\"Regresar\"></a></td>\n";
		                HTMLComprobante += "    </tr>\n";
		                HTMLComprobante += "   </table><br>\n";
		                sess.removeAttribute("_Posicion");
		            }//fin else
		        }//fin if
		        else
		            salidaReferencia += trama_salida.substring(16,trama_salida.length());
		        if( errorLF57 )
		        {
		            HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\"></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\"></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"8\" alt=\"..\"></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "   </table>\n";
		            HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td width=\"21\" background=\"/gifs/EnlaceMig/gfo25030.gif\"><img src=\"/gifs/EnlaceMig/gau00010.gif\" width=\"21\" height=\"2\" alt=\"..\"></td>\n";
		            HTMLComprobante += "     <td width=\"428\" height=\"100\" valign=\"top\" align=\"center\">\n";
		            HTMLComprobante += "      <table width=\"380\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\">\n";
		            HTMLComprobante += "       <tr>\n";
		            HTMLComprobante += "        <td>&nbsp;</td>\n";
		            HTMLComprobante += "       </tr>\n";
		            HTMLComprobante += "       <tr>\n";
		            HTMLComprobante += "        <td class=\"tittabcom\" align=\"center\">"+ salidaReferencia +"</td>\n";
		            HTMLComprobante += "       </tr>\n";
		            HTMLComprobante += "      </table>\n";
		            HTMLComprobante += "     </td>\n";
		            HTMLComprobante += "     <td width=\"21\" background=\"/gifs/EnlaceMig/gfo25040.gif\"><img src=\"/gifs/EnlaceMig/gau00010.gif\" width=\"21\" height=\"2\" alt=\"..\"        name=\"..\"></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "   </table>\n";
		            HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"8\" alt=\"..\" name=\".\"></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "   </table><br><br>\n";
		            HTMLComprobante += "   <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n";
		            HTMLComprobante += "    <tr>\n";
		            HTMLComprobante += "     <td align=\"center\"><a href=\"JavaScript:Regresar();\"><img border=\"0\" src=\"/gifs/EnlaceMig/gbo25320.gif\" alt=\"Regresar\"></a></td>\n";
		            HTMLComprobante += "    </tr>\n";
		            HTMLComprobante += "   </table><br>\n";
		        }//fin if
		        HTMLComprobante += "   <input type=\"hidden\" name=\"emisoras\"    value=\""+ emisoras    +"\">\n";
		        HTMLComprobante += "   <input type=\"hidden\" name=\"cboCuenta\"   value=\""+ cboCuenta   +"\">\n";
		        HTMLComprobante += "   <input type=\"hidden\" name=\"opcCompVent\" value=\""+ opcCompVent +"\">\n";
		        request.setAttribute("Fecha"        ,ObtenFecha(true)             );
		        request.setAttribute("FechaHoy"     ,ObtenFecha(false)            );
		        request.setAttribute("ContUser"     ,ObtenContUser(request)       );
		        request.setAttribute("referencia"   ,salidaReferencia             );
		        request.setAttribute("newMenu"      ,session.getFuncionesDeMenu() );
		        request.setAttribute("MenuPrincipal",session.getStrMenu()         );
		        request.setAttribute("datFondos390" ,HTMLComprobante              );
		        request.setAttribute("Encabezado"   ,CreaEncabezado("Sociedades de Inversi&oacute;n","Tesorer&iacute;a &gt; Inversiones &gt; Sociedades de Inversi&oacute;n", "s26120h", request));


		        //TODO CU3011 Realiza operaciones
		        /**
		 		 * VSWF - FVC - I
		 		 * 17/Enero/2007
		 		 */
		        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		        try{
		        BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
		 		BitaHelper bh = new BitaHelperImpl(request, session, sess);

		 		BitaTransacBean bt = new BitaTransacBean();
		 		bt = (BitaTransacBean)bh.llenarBean(bt);
		 		if(opcCompVent.equals("C")){
		 			bt.setNumBit(BitaConstants.ER_INVERSIONES_FONDOS_INVERSION_OPER_REDSRV_CPSI);
		 		}
		 		else{
		 			bt.setNumBit(BitaConstants.ER_INVERSIONES_FONDOS_INVERSION_OPER_REDSRV_VTSI);
		 		}
		 		bt.setCctaOrig((datCboCuenta[0] == null)?" ":datCboCuenta[0]);
		 		bt.setCctaDest((datEmisoras[0] == null)?" ":datEmisoras[0]);
		 		bt.setImporte(Double.parseDouble((importe == null)?" ":importe));
		 		bt.setNumTit(Long.parseLong((titulos == null)?" ":titulos));
		 		bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
		 		bt.setServTransTux((operacion == null)?" ":operacion.trim());
		 		/*BMB-I*/
		 		if(trama_salida!=null){
					if(trama_salida.substring(0,2).equals("OK")){
						bt.setIdErr(operacion.trim()+"0000");
					}else if(trama_salida.length()>8){
		    			bt.setIdErr(trama_salida.substring(0,8));
		    		}else{
		    			bt.setIdErr(trama_salida.trim());
		    		}
				}
		 		/*BMB-F*/
		 		/*VSWF I Autor=BMB fecha=08-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario*/
		 		bt.setBeneficiario((datEmisoras[0] == null)?" ":datEmisoras[0].trim());
		 		/*VSWF F*/
		 		BitaHandler.getInstance().insertBitaTransac(bt);
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		        }
		 		/**
		 		 * VSWF - FVC - F
		 		 */

		        evalTemplate("/jsp/InverSoc3.jsp"   ,request,response);
		        EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Terminar[compraVentaFondos();]", EIGlobal.NivelLog.INFO);

			} // Termina Flujo normal despues de validar cuentas
		}
		// Catch y Finally de Validaci�n de Cuentas
		catch (Exception e){
			request.setAttribute("Mensaje", "Hubo un problema con el servicio, favor de intentar m&aacute;s tarde.");
			evalTemplate( "/jsp/EI_Mensaje2.jsp", request, response);
		}
		finally{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace("Error al cerrar la conexion a MQ", EIGlobal.NivelLog.ERROR);
			}
		}

    }

    private Hashtable ConsultaPosicion
        (
            HttpServletRequest request,
            HttpServletResponse response
        )
            throws ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***InverSoc.class Comenzar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
        HttpSession  sess    = request.getSession();
        BaseResource session = (BaseResource)sess.getAttribute("session");
        Hashtable datPosicion     = new Hashtable();
        if( sess.getAttribute("_Posicion")!=null )
        {
            EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Obtener Posicion de Memoria...", EIGlobal.NivelLog.INFO);
            datPosicion = (Hashtable)sess.getAttribute("_Posicion");
        }
        else
        {
            EIGlobal.mensajePorTrace("-!?***InverSoc.class ###Obtener Posicion de Servicio...", EIGlobal.NivelLog.INFO);
            String cboCuenta = ( request.getParameter("cboCuenta")!=null ) ? request.getParameter("cboCuenta") : "";
            EIGlobal.mensajePorTrace("-!?cboCuenta["+ cboCuenta +"]", EIGlobal.NivelLog.DEBUG);
            String[] datCboCuenta   = desentrama(cboCuenta,'|');     //cuenta|descripcion|tipo_relacion|tipo_cuenta|tipo_persona|
            ServicioTux tuxGlobal = new ServicioTux();
            //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
            RandomAccessFile archivoRespuesta;
            String tramaEnvio = "2EWEB|"+ session.getUserID8() +"|POSI|"+ session.getContractNumber() +"|"+ session.getUserID8() +"|"+ session.getUserProfile() +"|"+ datCboCuenta[0] +"|"+ datCboCuenta[2];
		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = request.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada

            EIGlobal.mensajePorTrace(fechaHr+"-!?***Trama Enviar["+ tramaEnvio +"]", EIGlobal.NivelLog.INFO);
            String tramaRegresoServicio = "";
            try
            {
                Hashtable hs = tuxGlobal.web_red(tramaEnvio);
                tramaRegresoServicio = (String) hs.get("BUFFER");
                EIGlobal.mensajePorTrace("-!?***Trama Recibe["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.DEBUG);
            }catch(Exception e1){ e1.printStackTrace(); }
            String nombreArchivo = Global.DIRECTORIO_LOCAL + tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/'));
            ArchivoRemoto archRem = new ArchivoRemoto();
            archRem.copia(tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/')+1));
            try
            {
                archivoRespuesta = new RandomAccessFile(nombreArchivo,"rw");
                String datSaldosArch = obtenerSaldosArchivo(archivoRespuesta);
                EIGlobal.mensajePorTrace("-!?***Servicio Regreso["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("-!?***    Trama Saldos["+ datSaldosArch +"]", EIGlobal.NivelLog.INFO);
                if( datSaldosArch.startsWith("OK ") )
                {
                    datPosicion = obtenerPosicionArchivo(archivoRespuesta);
                    sess.setAttribute("_Posicion",datPosicion);
                }//fin if
            }catch(Exception e){ EIGlobal.mensajePorTrace("-!?Excepcion ConsultaPosicion["+ e +"]", EIGlobal.NivelLog.INFO); }
        }//fin else
        EIGlobal.mensajePorTrace("-!?***InverSoc.class Terminar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
        return datPosicion;
    }

    public String obtenerSaldosArchivo(RandomAccessFile archivoSalida)
    {
        long   posAct    = 0;
        long   lngArch   = 0;
        String tramaArch = "";
        try
        {
            int numLineas = 0;
            archivoSalida.seek(0);
            lngArch = archivoSalida.length();
            do
            {
                tramaArch += archivoSalida.readLine();
                posAct = archivoSalida.getFilePointer();
                numLineas ++;
            }while( posAct<lngArch && numLineas<2 );
        }catch(Exception e){  }
        return tramaArch;
    }

    public Hashtable obtenerPosicionArchivo(RandomAccessFile archivoSalida)
    {
        long   posAct     = 0;
        long   lngArch    = 0;
        String tramaArch  = "";
        Hashtable regPosi = new Hashtable();
        Hashtable regPosicion = new Hashtable();
        try
        {
            int numLineas = 0;
            archivoSalida.seek(0);
            lngArch = archivoSalida.length();
            do
            {
                tramaArch = archivoSalida.readLine();
                if( numLineas>1 )
                {
                	System.out.println("trama incidente->"+tramaArch);
                    String[] tmpDatos = desentrama(tramaArch,';');
                    String importe = "";
                    String emisora = tmpDatos[0];
                    String titulos = tmpDatos[1];
                    String precio  = tmpDatos[3];
					String serie   = tmpDatos[0];

                    emisora = emisora.substring(0,emisora.indexOf(" "));
					serie = serie.substring(serie.lastIndexOf(" ")+1,serie.length());

                    try
                    {   //Obtener Importe
                        BigDecimal tmp1 = new BigDecimal(titulos);
                        BigDecimal tmp2 = new BigDecimal(precio );
                        BigDecimal tmp3 = new BigDecimal("0");
                        tmp3 = tmp1.multiply(tmp2);
                        importe = tmp3.setScale(2,BigDecimal.ROUND_DOWN).toString();
                    }catch(Exception e1){ importe = "0"; }
                    String[] datos = new String[3];
                    datos[0] = formatear(titulos,0);
                    datos[1] = formatear(importe,2);
					datos[2] = serie;
					if(regPosicion.containsKey(emisora)){
						regPosi = (Hashtable)regPosicion.get(emisora);
						regPosi.put(serie,datos);
					}else{
						regPosi = new Hashtable();
						regPosi.put(serie,datos);
						regPosicion.put(emisora, regPosi);
					}
                }//fin if
                posAct = archivoSalida.getFilePointer();
                numLineas ++;
            }while( posAct<lngArch );
        }catch(Exception e){EIGlobal.mensajePorTrace("error "+e.getMessage(), EIGlobal.NivelLog.INFO);  }
        return regPosicion;
    }

    private String[] calcularDatos(String titulos,String importe,String precio)
    {
        String[] datos = new String[2];
        try
        {
            precio  = ( precio.trim().equals("")  ) ? "0" : precio.trim();
            titulos = ( titulos.trim().equals("") ) ? "0" : titulos.trim();
            importe = ( importe.trim().equals("") ) ? "0" : importe.trim();
            EIGlobal.mensajePorTrace("-!?titulos["+ titulos +"] importe["+ importe +"] precio["+ precio +"]", EIGlobal.NivelLog.DEBUG);
            BigDecimal vPrec = new BigDecimal(precio);
            BigDecimal vTitu = new BigDecimal(titulos);
            BigDecimal vMont = new BigDecimal(importe);
            if( importe.trim().equals("0") )
            {   //Obtener Importe
                vMont = vPrec.multiply(vTitu);
            }//fin if
            else
            {   //Obtener Titulos
                vTitu = vMont.divide(vPrec,0,BigDecimal.ROUND_DOWN);
                vMont = vPrec.multiply( new BigDecimal(vTitu.toBigInteger()) );
            }//fin else
            datos[0] = vMont.setScale(2,BigDecimal.ROUND_DOWN).toString();  //Importe Calculado
            datos[1] = vTitu.setScale(0,BigDecimal.ROUND_DOWN).toString();  //Titulos Calculado
            EIGlobal.mensajePorTrace("-!?Importe Calculado["+ datos[0] +"] Titulos Calculado["+ datos[1] +"]", EIGlobal.NivelLog.DEBUG);
        }catch(Exception e){  }
        return datos;
    }

    private String[] desentrama(String auxCad,char Separador)
    {
        int      NumSep = 0;
        String[] Salida = null;
        if( auxCad.trim().toCharArray()[(auxCad.length()-1)]!=Separador )
            auxCad += ""+ Separador;
        for(int i=0; i<auxCad.length() ;i++)
            if( auxCad.toCharArray()[i]==Separador )
                NumSep ++;
        try
        {
            Salida = new String[NumSep];
            for(int i=0; i<=NumSep ;i++)
            {
                Salida[i] = auxCad.substring(0, auxCad.indexOf(Separador)).trim();
                auxCad    = auxCad.substring((auxCad.indexOf(Separador)+1), auxCad.length());
            }
        }catch(Exception e){  }
        return Salida;
    }

    private String formatear(String format,int decimal)
    {
        try
        {
            BigDecimal importe = new BigDecimal(format).setScale(decimal,BigDecimal.ROUND_DOWN);
            NumberFormat formateo = NumberFormat.getNumberInstance();
            formateo.setMaximumFractionDigits(decimal);
            formateo.setMinimumFractionDigits(decimal);
            format = formateo.format(importe);
        }catch(Exception e){  }
        return format;
    }

    private String fchFormat(String fecha)
    {   //formato Entrada aaaa-mm-dd
        //formato Regreso dd/mm/aaaa
        return fecha.substring(8) +"/"+ fecha.substring(5,7) +"/"+ fecha.substring(0, 4);
    }
}

/*fsj*/