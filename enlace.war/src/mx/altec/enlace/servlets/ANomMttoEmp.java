/** Banco Santander Mexicano

*		Clase ANomMttoEmp

*		@author Gerardo Salazar Vega

*		@version 1.0

*		fecha de creacion: 08 de Noviembre del 2000

*		responsable: Roberto Guadalupe Resendiz Altamirano

*		@return SUCCESS resultado de la ejecucion del applogic

* 	descripcion: Applogic para llamar al template de mantenimiento de empleados

					session.setNameFileNomina( nomArchNomina );

					session.setNumerosEmpleado( "" );

					session.setfacultad( "" );




					sess.getAttribute("nombreArchivo")

					sess.getAttribute("numeroEmp");  checar el set cuando le subo a la session

					sess.getAttribute("facultadesE")

*/



package mx.altec.enlace.servlets;



import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;



public class ANomMttoEmp extends BaseServlet {

	/**
	 * Constante para la literal "\">"
	 */
	private static final String CIERRA_ETIQUETA = "\">";

	/**
	 * Metodo para la peticion Get
	 * @param request informacion de la instancia del cliente
	 * @param response respuesta del tipo http
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )

			throws ServletException, IOException {

		defaultAction( request, response );

	}


	/**
	 * Metodo para la peticion Post
	 * @param request informacion de la instancia del cliente
	 * @param response respuesta del tipo http
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )

    			throws ServletException, IOException {

		defaultAction( request, response );

	}


	/**
	 * Metodo para ejecutar despues de la peticion post o get
	 * @param request informacion de la instancia del cliente
	 * @param response respuesta del tipo http
	 * @throws IOException excepcion a manejar
	 * @throws ServletException excepcion a manejar
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )

			throws IOException, ServletException {



		/*

		*	Obtiene el menu, la fecha y los datos del usuario

		*/

		boolean sesionvalida = SesionValida( request, response );


		if ( sesionvalida ) {

			HttpSession sess = request.getSession();

			BaseResource session = (BaseResource) sess.getAttribute("session");

			request.setAttribute("MenuPrincipal", session.getStrMenu());

			request.setAttribute("newMenu",       session.getFuncionesDeMenu());

			request.setAttribute("Encabezado",    CreaEncabezado( "Alta de Empleados","Servicios &gt; N&oacute;mina &gt; Alta de Empleados","s25720h",request ) );

			facultades(request);

			request.setAttribute( "ContenidoArchivo", "" );

			//request.setAttribute( "facultades", session.getFacultadesEmp() );

			request.setAttribute( "facultades", sess.getAttribute("facultadesE") );
		   /*
			*	Modificado por Miguel Cortes Arellano
			*  Fecha: 04/12/03
			*  Empieza codigo Sinapsis
			*  Proposito: Configurar la pagina a ser deplegada por el template de Mantenimiento de Empleados
			*					1 = Mantenimiento
			*					2 = Consultas
			*/
			if( "1".equals(request.getParameter("page")) ){
				//TODO: BIT CU 4121, El cliente entra al flujo
				/*
	    		 * VSWF ARR -I
	    		 */
				if ( "ON".equals(Global.USAR_BITACORAS.trim()) ){
					try{
					BitaHelper bh = new BitaHelperImpl(request, session,sess);
					bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_NOMINA_EMP_MANTEN_ENTRA);
					bt.setContrato(session.getContractNumber());

					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
					}catch(Exception e){
						EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
					}
				}
	    		/*
	    		 * VSWF ARR -F
	    		 */
				sess.removeAttribute("listaEmpleados");
				evalTemplate( IEnlace.NOM_MTTO_EMP_TMPL, request, response );
			}
			else
			{
				//TODO: BIT CU 4131, El cliente entra al flujo
				/*
	    		 * VSWF	ARR -I
	    		 */
				if ( "ON".equals(Global.USAR_BITACORAS.trim()) ){
					try{
					BitaHelper bh = new BitaHelperImpl(request, session,sess);
					bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_NOMINA_EMP_CONS_ALTA_ENTRA);
					bt.setContrato(session.getContractNumber());

					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
					}catch(Exception e){
						EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
					}
				}
	    		/*
	    		 * VSWF	-F
	    		 */
				 inicioConsulta (request, response);
				evalTemplate( IEnlace.NOM_MTTO_CSL_TMPL, request, response );
			}
			//Finaliza codigo Sinapsis
		} else {

			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);

			evalTemplate( IEnlace.ERROR_TMPL, request, response );

		}

	}

	/**
	*		Modificado por Miguel Cortes Arellano
	*		Fecha: 17/12/2003
	*		Proposito: Inicializar las variables de session en caso de seleccionar la opcion de consulta de Empleados
	*		Comienza codigo Sinapsis
	* @param request informacion de la instancia del cliente
	* @param response respuesta del tipo http
	* @throws ServletException excepcion a manejar
	* @throws IOException excepcion a manejar
	*/

	protected void inicioConsulta (HttpServletRequest request, HttpServletResponse response)

		throws ServletException, IOException

	{

		HttpSession sess = request.getSession();

		BaseResource session = (BaseResource) sess.getAttribute("session");



		//TODO: posible refactor, buscar en resto de codigo

		StringBuffer datesrvr = new StringBuffer("");

		datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");

		datesrvr.append( ObtenAnio() );

		datesrvr.append(CIERRA_ETIQUETA);

		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");

		datesrvr.append( ObtenMes() );

		datesrvr.append( CIERRA_ETIQUETA);

		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");

		datesrvr.append( ObtenDia() );

		datesrvr.append(CIERRA_ETIQUETA);

		//TODO:posible refactor

		request.setAttribute("newMenu", session.getFuncionesDeMenu());

		//fin TODO

		request.setAttribute("Bitfechas",datesrvr.toString());

		request.setAttribute("Encabezado", CreaEncabezado("Consulta de Empleados","Servicios &gt; N&oacute;mina &gt;Consultas &gt;Alta Empleados ", "s25800conh",request));



		if(sess.getAttribute("facultadesN") != null){
			request.setAttribute( "facultades", sess.getAttribute("facultadesN") );
		}


		request.setAttribute("textcuenta","");

		request.setAttribute( "ContenidoArchivo", "" );

	}

	// Finaliza codigo Sinapsis
	/**
	 * Metodo para las facultades
	 * @param request informacion de la instancia del cliente
	 * @return String valor
	 */
	public String facultades(HttpServletRequest request) {

		HttpSession sess = request.getSession();

		StringBuffer cadFacultades = new StringBuffer("");

		if ( verificaFacultad( "INOMACTUEMP" ,request) ){ cadFacultades.append( "INOMACTUEMP, " ); }

		if ( verificaFacultad( "INOMCREAEMP" ,request) ){ cadFacultades.append( "INOMCREAEMP, " ); }

		if ( verificaFacultad( "INOMIMPAEMP" ,request) ){ cadFacultades.append( "INOMIMPAEMP, " ); }

		if ( verificaFacultad( "INOMIMPREMP" ,request) ){ cadFacultades.append( "INOMIMPREMP, " ); }

		if ( verificaFacultad( "INOMALTAEMP" ,request) ){ cadFacultades.append( "INOMALTAEMP, " ); }

		if ( verificaFacultad( "INOMMODIEMP" ,request) ){ cadFacultades.append( "INOMMODIEMP, " ); }

		if ( verificaFacultad( "INOMBAJAEMP" ,request) ){ cadFacultades.append( "INOMBAJAEMP, " ); }

		if ( verificaFacultad( "INOMEXPOEMP" ,request) ){ cadFacultades.append( "INOMEXPOEMP, " ); }

		if ( verificaFacultad( "INOMENVIEMP" ,request) ){ cadFacultades.append( "INOMENVIEMP, " ); }

		if ( verificaFacultad( "INOMBORREMP" ,request) ){ cadFacultades.append( "INOMBORREMP, " ); }
		//session.setFacultadesEmp( cadFacultades );

		sess.setAttribute("facultadesE",cadFacultades);

		EIGlobal.mensajePorTrace( "setFacultadesEmp :" + cadFacultades, EIGlobal.NivelLog.INFO);

		return cadFacultades.toString();

	}

}