/*Banco Santander Mexicano
  Clase c_posicion creacion del template ionicial para la consulta de posicion
  @Autor:
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 01/03/2001 -Poner encabezado
 */

package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.Method;
import java.lang.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
//03/01/07
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class c_posicion extends BaseServlet{
   public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
      HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

      String bgcolor    = "";
      String opcion     = "";
      String titulo     = "";
      String Contrato   = "";
      String strSalida2 = "";
      String tipo       = "";
      int salida;
      int indice		= 1;
      boolean ChkSeg	= false;
      opcion= (req.getParameter("opcion")).trim();
      if (opcion==null)
         opcion="0";

      if(SesionValida( req, res))
      {
//        if(session.getFacCPosicion().trim().length()!=0){
        if(session.getFacultad(session.FAC_CONSULTA_POSICION))
        {
   		   //***********************************************
           //modificación para integración pva 06/03/2002
           //***********************************************
  	       EIGlobal.mensajePorTrace( "***c_posicion orev"+req.getParameter("prev"), EIGlobal.NivelLog.INFO);
  	       EIGlobal.mensajePorTrace( "***c_posicion next"+req.getParameter("next"), EIGlobal.NivelLog.INFO);
  	       EIGlobal.mensajePorTrace( "***c_posicion total"+req.getParameter("total"), EIGlobal.NivelLog.INFO);
	       int total=0,prev=0,next=0;
		   String paginacion="";
           if  ((req.getParameter("prev")!=null) &&
		      (req.getParameter("next")!=null) &&
			  (req.getParameter("total")!=null))
	       {
		      prev =  Integer.parseInt(req.getParameter("prev"));
              next =  Integer.parseInt(req.getParameter("next"));
  	          total = Integer.parseInt(req.getParameter("total"));
		   }
           else
	       {
              EIGlobal.mensajePorTrace( "***csaldo1 todos nullos", EIGlobal.NivelLog.INFO);
   	          llamado_servicioCtasInteg(IEnlace.MConsulta_posicion," "," "," ",session.getUserID8()+".ambci",req);
              total=getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
		      prev=0;
		      if (total>Global.NUM_REGISTROS_PAGINA)
		         next=Global.NUM_REGISTROS_PAGINA;
		      else
		         next=total;
		   }
  	       EIGlobal.mensajePorTrace( "***c_posicion orev"+prev, EIGlobal.NivelLog.INFO);
    	   EIGlobal.mensajePorTrace( "***c_posicion next"+next, EIGlobal.NivelLog.INFO);
  	       EIGlobal.mensajePorTrace( "***c_posicion total"+total, EIGlobal.NivelLog.INFO);
           Contrato=session.getContractNumber();
           //***********************************************
           //modificación para integración pva 06/03/2002
           //***********************************************
		   //String[][] arrayCuentas = ContCtasRelac(Contrato);
		   String[][] arrayCuentas = ObteniendoCtasInteg(prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
		   //***********************************************
           if(Integer.parseInt(arrayCuentas[0][0])>=1)
           {
              if(opcion.equals("0"))
                 titulo="<img src=/gifs/EnlaceMig/t_conpos.gif width=212 height=21 alt=Posici&oacute;n>";
              else
                 titulo="Cambio de Instrumento ";

              strSalida2+="\n<table width=500 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
              //strSalida2+="\n<tr><th colspan=2 bgcolor=red><font color=white>Seleccione una cuenta</font><th></tr>";
		      strSalida2+="\n<tr>";
		      strSalida2+="<td align=center class=tittabdat width=74>Seleccione </td>";
		      strSalida2+="<td width=80     class=tittabdat align=center>Cuenta</td>";
		      strSalida2+="<td class=tittabdat align=center width=320>Descripci&oacute;n</td>";
		      strSalida2+="\n</tr>";

		      int contador=0;
		      boolean chkseg;
              for(int indice2=1;indice2<=Integer.parseInt(arrayCuentas[0][0]);indice2++)
              {
        	      //***********************************************
                  //modificación para integración pva 06/03/2002
                  //***********************************************
                  /*
		          chkseg=true;
                  if (arrayCuentas[indice][2].equals("P"))
                  chkseg  = !existeFacultadCuenta(arrayCuentas[indice][1], "CONSPOSIPROP", "-");
		          else if (arrayCuentas[indice][2].equals("T"))
	              chkseg  = existeFacultadCuenta(arrayCuentas[indice][1], "CONSPOSITER", "+");
                  */
			      //if(chkseg==true)
			      //{

                  //tipo=arrayCuentas[indice2][1].substring(0, EIGlobal.NivelLog.ERROR);
                  //
                  //if((tipo.equals("51") || tipo.equals("60") || tipo.equals("65") || tipo.equals("57") ))
                  //{

                  if(contador%2==0)
                     bgcolor="textabdatobs";
                  else
                     bgcolor="textabdatcla";
	              if(indice2==1)
			      {
		 		     strSalida2+="\n<tr>";
				     strSalida2+="<td class='"+bgcolor+"' align=center width=74><input type=radio name=cta_posi value=\"3|" + arrayCuentas[indice2][1] + "|" + arrayCuentas[indice2][4] +"|"+arrayCuentas[indice2][2]+"|\" CHECKED>&nbsp;</td>";
				     strSalida2+="<td class='"+bgcolor+"' nowrap align=center width=80>" + arrayCuentas[indice2][1] + "&nbsp;&nbsp;</td>";
  				     if(arrayCuentas[indice2][4].length()>0)
		 		        strSalida2+="<td class='"+bgcolor+"' width=320>" + arrayCuentas[indice2][4]+"</td></tr>";
			         else
                        strSalida2+="<td class='"+bgcolor+"' width=320>&nbsp;&nbsp;</td></tr>";
			      }
                  else
			      {
                     strSalida2+="\n<tr>";
			         strSalida2+="<td class='"+bgcolor+"' align=center width=74><input type=radio name=cta_posi value=\"3|" + arrayCuentas[indice2][1] + "|" + arrayCuentas[indice2][4] +"|"+arrayCuentas[indice2][2]+"|\"></td>";
			         strSalida2+="<td class='"+bgcolor+"' nowrap align=center width=80>" + arrayCuentas[indice2][1] + "&nbsp;&nbsp;</td>";
			         if(arrayCuentas[indice2][4].length()>0)
				        strSalida2+="<td class='"+bgcolor+"' width=320>" + arrayCuentas[indice2][4]+"</td></tr>";
				     else
          		        strSalida2+="<td class='"+bgcolor+"' width=320>&nbsp;&nbsp;</td></tr>";
			      }
			      contador++;

		// }
//		 }
			  }
              strSalida2+="\n</table>";
              log(strSalida2);
              //req.setAttribute("MenuPrincipal", session.getstrMenu());
              //req.setAttribute("FechaHoy",ObtenFecha());
	          req.setAttribute("Enviar","<input type=submit value=' Posicion' name=Enviar>");
              //req.setAttribute("DatosUsuario",ObtenContUser());
              //req.setAttribute("titulo",titulo);
              //req.setAttribute("Hora",ObtenHora());
              req.setAttribute("opcion",opcion);
              req.setAttribute("Tabla",strSalida2);
              req.setAttribute("MenuPrincipal", session.getStrMenu());
              req.setAttribute("newMenu", session.getFuncionesDeMenu());
		      ////////////////////////////////////////////////////////////////
              HttpSession ses = req.getSession();
		      ses.setAttribute("Enviar","<input type=submit value=' Posicion' name=Enviar>");
		      ses.setAttribute("opcion",opcion);
		      ses.setAttribute("Tabla",strSalida2);
		      ses.setAttribute("MenuPrincipal", session.getStrMenu());
		      ses.setAttribute("newMenu", session.getFuncionesDeMenu());

		      if(opcion.equals("0"))
//			        ses.setAttribute("Encabezado", CreaEncabezado("Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n &gt; Chequeras","s25090h"));
                 req.setAttribute("Encabezado", CreaEncabezado("Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n &gt; Chequeras","s25090h",req));
		      else
//		          ses.setAttribute("Encabezado", CreaEncabezado("Cambio de Instrucci&oacute;n","Tesorer&iacute;a &gt; Inversiones  &gt; Plazo &gt; Cambio de Instrucci&oacute;n","s26210h"));
                 req.setAttribute("Encabezado", CreaEncabezado("Cambio de Instrucci&oacute;n","Tesorer&iacute;a &gt; Inversiones  &gt; Plazo &gt; Cambio de Instrucci&oacute;n","s26210h",req));
//				 res.sendRedirect(res.encodeRedirectURL("/NASApp/enlaceMig"+IEnlace.POSICION));
                 //modificación para integración pva 06/03/2002

		      if (total>Global.NUM_REGISTROS_PAGINA)
		      {
                 paginacion="<table  width=450 align=center>";
		         if(prev > 0)
                    paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:atras();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";
                 if(next < total)
	             {
                    if((next + Global.NUM_REGISTROS_PAGINA) <= total)
 	                   paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(next -prev).toString() + " ></A></td></TR>";
                    else
                       paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(total-next).toString() + " ></A></td></TR>";
			     }
                 paginacion+="</table><br><br>";
    	         EIGlobal.mensajePorTrace( "***paginacion :"+paginacion, EIGlobal.NivelLog.INFO);
			  }
			  req.setAttribute("total", ""+total);
		      req.setAttribute("prev", ""+prev);
		      req.setAttribute("next", ""+next);
			  req.setAttribute("paginacion", paginacion);
			  req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
              //***********************************************
			  //TODO TODO BIT CU1051
		        /*
				 * 03/ENE/07
				 * VSWF-BMB-I
				 */
			  if (Global.USAR_BITACORAS.trim().equals("ON")){
				  try{
			        BitaHelper bh = new BitaHelperImpl(req, session, sess);
			        if(req.getParameter(BitaConstants.FLUJO)!=null){
			        	bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			        }else{
			        	bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			        }
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_POSICION_CHEQUERAS_ENTRA);
					if(Contrato!=null){
						bt.setContrato(Contrato);
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			  }
		        /*
				 * VSWF-BMB-F
				 */
              evalTemplate(IEnlace.POSICION,req,res);
		   }
           else
		   {
		      if(opcion.equals("0"))
		         despliegaPaginaError("No se encontraron cuentas Asociadas.","Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n &gt; Chequeras","s25090h",req,res);
		      else
                 despliegaPaginaError("No se encontraron cuentas Asociadas.", "Cambio de Instrucci&oacute;n","Tesorer&iacute;a &gt; Inversiones  &gt; Plazo &gt; Cambio de Instrucci&oacute;n","s26210h", req, res);
		   }
		}
        else
        {
			//LGN Q19262 Líneas anteriores al cambio efectuado
        	//req.setAttribute("Error","No tiene facultad para dar de alta beneficiarios");
			//despliegaPaginaError("No tiene facultad para dar de alta beneficiarios","Alta de Beneficiarios Pago Directo","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Alta",req,res);
			//LGN Q19262 Líneas anteriores al cambio efectuado


			//LGN Q19262
			//17-11-2004 Inicio del cambio
			req.setAttribute("Error","No tiene facultad para realizar Consultas de Posición");
			despliegaPaginaError("No tiene facultad para Realizar Consultas de Posición","Consulta de Posición","Consultas &gt; Posición &gt; Chequeras",req,res);
			//17-11-2004 Fin del cambio

		}
	  }
	}
}
/*		else
		{
           req.setAttribute("Error","No tiene facultad para dar de alta beneficiarios");
           despliegaPaginaError("No tiene facultad para dar de alta beneficiarios","Alta de Beneficiarios Pago Directo","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Alta",req,res);
		}
	  else
	  {
	     req.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
		 evalTemplate( IEnlace.ERROR_TMPL, req, res );
	  }*/