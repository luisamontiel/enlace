package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.sql.*;

import javax.servlet.http.*;
import javax.servlet.*;

import java.net.*;
import javax.sql.*;

//####################################################################
//
//      Modificacion: validar Nueva version sua y fechalimite
//                    para aceptar version anterior.
//      Proyecto    : SUA 2005
//      Cod         : MX2005-21100
//      Programador : Carlos Aleman Rosas.
//      clave en cod: W3xx
//####################################################################

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Importar;
import mx.altec.enlace.bo.SuaErrorDisco;
import mx.altec.enlace.bo.SuaImpuesto;
import mx.altec.enlace.bo.SuaRegistro;
import mx.altec.enlace.bo.SuaVerificaDisco;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.SuaUtil;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

//####################################################################
//
//	Historial de Correcciones
//	8 Dic 2003
//	El llamado a extraer registro cambia el tipo de dato que recibe
//	antes era String ahora es String Buffer
//	Proyecto: Mantenimiento Praxis
//	Por Lorenzo Muci�o Mendoza
//
//
//####################################################################

//####################################################################
//
//	Modificaciones Ana Mu�oz AMR 09/08/2007
//	Se agrega validacion de acceso en horario permitido
//####################################################################

public class SuaDisco extends BaseServlet
{


	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		// VSWF - JGAL
		String ventana = getFormParameter(request, "ventana");

		if ( SesionValida( request, response ) )
		{
			session.setModuloConsultar(IEnlace.MSua_envio);
			//Inicia Mod. AMR 11/07/2007
			EIGlobal.mensajePorTrace("==========================================Notas AMR Inicia validaci�n de hora de acceso =b   =====================================", EIGlobal.NivelLog.INFO);
			String  continuar;
			Calendar hoy;
			HttpSession lSesion = request.getSession();
			continuar = "SI";
			hoy = new GregorianCalendar();
			EIGlobal.mensajePorTrace("=========================================Son las ========="+hoy.get(hoy.HOUR_OF_DAY), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("=========================================del dia ========="+hoy.get(hoy.DAY_OF_WEEK), EIGlobal.NivelLog.INFO);
			if( hoy.get(hoy.DAY_OF_WEEK) == hoy.SATURDAY||
				hoy.get(hoy.DAY_OF_WEEK) ==hoy.SUNDAY){
				continuar = "NO";
			} else {
				if (hoy.get(hoy.HOUR_OF_DAY) < 8 || hoy.get(hoy.HOUR_OF_DAY) >=20){
				    continuar = "NO";
			           }
			}
			if (continuar == "NO")
			{
				EIGlobal.mensajePorTrace("Fuera de horario", EIGlobal.NivelLog.INFO);
				request.setAttribute("newMenu",              session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal",        session.getStrMenu());
				request.setAttribute("Encabezado",           CreaEncabezado("Validaci&oacute;n de Disco SUA", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos", "s25700h", request));
				request.setAttribute("Fecha",                ObtenFecha());
				request.setAttribute("Hora",                 ObtenHora());
				lSesion.setAttribute("mensaje_salida", "<B>Transacci�n fuera de horario<B><br>Horario del servicio:<br>De Lunes a Viernes de 8:00 a 20:00 horas");
				evalTemplate("/jsp/SuaMensajes.jsp", request, response);
			}
			EIGlobal.mensajePorTrace("=========================================continuar========="+continuar, EIGlobal.NivelLog.INFO);
			//System.out.println("=======Ventana=========" + ventana);
			//System.out.println("======continuar========" + continuar);
			//if ( ventana.equals( "0" ) )
			if ( ventana.equals( "0" )  && continuar.equals("SI"))
			//Fin mod. AMR 11/07/2007
			{
				pedirArchivo( request, response );
			} else if ( ventana.equals( "1" ) ) {
				importaArchivo( request, response );
			}
		}
	}


	// Devuelve el numero de tokens de una trama.
	public int CuantosTokens( String Cadena, char Caracter )
	{
		int PosicionInicial = 0;
		int PosicionFinal   = 0;
		int Cuantos         = 0;

		PosicionFinal = Cadena.indexOf( Caracter, PosicionInicial);
		while( PosicionFinal != -1 )
		{
			PosicionFinal   = Cadena.indexOf( Caracter, PosicionInicial);
			if(PosicionFinal != -1)
			{
				Cuantos = Cuantos + 1;
				PosicionInicial = PosicionFinal + 1;
			}
		}
		if( PosicionInicial < Cadena.length())
			Cuantos = Cuantos + 1;
		return(Cuantos);
	}
	// Devuelve un token de una trama.
	public String BuscarToken(String Cadena, char Caracter, int Indice)
	{
		String Token           = "";
		int    PosicionInicial = 0;
		int    PosicionFinal   = 0;
		int    Cuantos         = 0;

		PosicionFinal = Cadena.indexOf( Caracter, PosicionInicial);
		while ( PosicionFinal != -1 )
		{
			PosicionFinal   = Cadena.indexOf( Caracter, PosicionInicial);
			if(PosicionFinal != -1)
			{
				Cuantos = Cuantos + 1;
				if ( Indice == Cuantos )
				{
					Token = Cadena.substring( PosicionInicial, PosicionFinal);
					break;
				}
				PosicionInicial = PosicionFinal + 1;
			}
		}
		if( PosicionInicial < Cadena.length())
		{
			Cuantos = Cuantos + 1;
			if( Indice == Cuantos)
				Token = Cadena.substring(PosicionInicial);
		}
		return(Token);
	}

	/**
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 * @return
	 */
	public Vector CargarDias()
	{
		Connection  conn = null;
		PreparedStatement     psDias;
		ResultSet rsDias = null;
		boolean    estado;
		String     sqlDias;
		Vector diasInhabiles = null;

		EIGlobal.mensajePorTrace( "***SuaDisco.class & cargar dias habiles &", EIGlobal.NivelLog.INFO);

		diasInhabiles = new Vector();
		sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm')," +
				  " to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI'" +
				  " and fecha >= sysdate";
		try {
			conn = createiASConn( Global.DATASOURCE_ORACLE );
			EIGlobal.mensajePorTrace( "***SuaDisco.class conexion establecida", EIGlobal.NivelLog.ERROR);
			psDias = conn.prepareStatement( sqlDias );
			EIGlobal.mensajePorTrace( "***SuaDisco.class objeto query listo", EIGlobal.NivelLog.ERROR);
			rsDias = psDias.executeQuery();
			EIGlobal.mensajePorTrace( "***SuaDisco.class objeto resultSet listo", EIGlobal.NivelLog.ERROR);

			while ( rsDias.next() )
			{
				//EIGlobal.mensajePorTrace( "***SuaDisco.class & Leyendo fecha &", EIGlobal.NivelLog.INFO);

				String dia = rsDias.getString(1);
				String mes = rsDias.getString(2);
				String anio = rsDias.getString(3);

				/*
				EIGlobal.mensajePorTrace( "***SuaDisco.class & dia: " + dia + " &", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***SuaDisco.class & mes: " + mes + " &", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***SuaDisco.class & anio:" + anio + " &", EIGlobal.NivelLog.INFO);
				*/

				dia  =  Integer.toString( Integer.parseInt(dia) );
				mes  =  Integer.toString( Integer.parseInt(mes) );
				anio =  Integer.toString( Integer.parseInt(anio) );

				/*
				EIGlobal.mensajePorTrace( "***SuaDisco.class & dia: " + dia + " &", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***SuaDisco.class & mes: " + mes + " &", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***SuaDisco.class & anio:" + anio + " &", EIGlobal.NivelLog.INFO);
				*/

				String fecha = anio.trim() + "/"  + mes.trim() + "/" + dia.trim();
				//EIGlobal.mensajePorTrace( "***SuaDisco.class & fecha no habil " + fecha + " &", EIGlobal.NivelLog.INFO);
				diasInhabiles.addElement( fecha );
			}

		} catch ( SQLException sqle ) {
			EIGlobal.mensajePorTrace( "***SuaDisco.class & " + sqle.toString(), EIGlobal.NivelLog.INFO);
		} finally {
			EI_Query.cierraResultSet(rsDias);
			EI_Query.cierraConnection(conn);
		}
		EIGlobal.mensajePorTrace( "***SuaDisco.class & termina cargar dias habiles &", EIGlobal.NivelLog.INFO);
		return ( diasInhabiles );
	}
//	Nuevo Metodo para la obtencion de los datos mas importantes....
//
	public SuaRegistro extraeImssPatronalFolioSua( String ArchivoDatos)
	{
		int         suaSizeRegister = 295;
		int         registros = 0;
		int         indice;
		int         segmento;
		int			tipoRegistro;
		StringBuffer      strRegistro;
		String      bloque = "";
		SuaRegistro rsDatosGrales;

		EIGlobal.mensajePorTrace( "***SuaDisco.class & extraeImssPatronalFolioSua &", EIGlobal.NivelLog.INFO);

		rsDatosGrales  = new SuaRegistro();

		//EIGlobal.mensajePorTrace( "***SuaDisco.class & Registros: " + Long.toString(registros) + " &", EIGlobal.NivelLog.INFO);

		BufferedReader arc=null;
		try
		{
		arc = new BufferedReader( new FileReader( ArchivoDatos ) );
		bloque=arc.readLine();
		registros = bloque.length() / suaSizeRegister;

		strRegistro = new StringBuffer("");
		for ( indice = 0; indice < registros; indice++ )
		{
			//EIGlobal.mensajePorTrace( "***SuaDisco.class # Leer datos del bloque #", 5);
			//EIGlobal.mensajePorTrace( "***SuaDisco.class # No " +indice+ " #", 5);

			segmento = indice * suaSizeRegister;
			if ( segmento < ( bloque.length() - 1 ) )
				strRegistro = new StringBuffer( bloque.substring( segmento, segmento + suaSizeRegister) );
			else
				strRegistro = new StringBuffer( bloque.substring( segmento) );

			tipoRegistro= (int) Integer.parseInt(strRegistro.substring(1,2));
			switch(tipoRegistro)
			{
				case 2:
						rsDatosGrales.regDosImssPatronal	= strRegistro.substring( 2 , 13); //W3xx Carta de Rechazo

						rsDatosGrales.regDosFolioSua		= strRegistro.substring( 32 , 38); // antes 6

						rsDatosGrales.regDosRazonSocial     = strRegistro.substring( 38 , 88); //W3xx Carta de Rechazo

						rsDatosGrales.regDosPeriodoDePago     = strRegistro.substring( 26 , 32); //W3xx Carta de Rechazo

						rsDatosGrales.regDosRfcEmpresa     = strRegistro.substring( 13 , 26); //W3xx Carta de Rechazo

						break;
				case 6:
						rsDatosGrales.regSeisNumeroDeDiscos = strRegistro.substring( 66 , 68);
                        rsDatosGrales.regSeisVersionSua= strRegistro.substring( 68, 72);

                        if ( SuaUtil.ValidaFormato("88", rsDatosGrales.regSeisNumeroDeDiscos) == 4) {
                        	rsDatosGrales.regSeisNumeroDeDiscos="01";
                        }

						EIGlobal.mensajePorTrace( "***SuaDisco.class # regSeisNumeroDeDiscos <" + rsDatosGrales.regSeisNumeroDeDiscos + ">", EIGlobal.NivelLog.DEBUG);
						break;
			}

		}	// ----------------------- >>> FIN DEL FOR
		}catch(IOException e)
		{
		}
		finally
		 {
			try
			 {
			   arc.close();
			 }catch(Exception e) {}
		 }


		EIGlobal.mensajePorTrace( "***SuaDisco.class & Termina: extraeImssPatronalFolioSua &", EIGlobal.NivelLog.INFO);
		return(rsDatosGrales);
	}

	public void pedirArchivo( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		int              salida;
		boolean          fac_programadas;
		String           campos_fecha         = "";
		String           contrato             = "";
		String           usuario              = "";
		String           clave_perfil         = "";
		//String           cadena_getFacCOpProg = "";
		String           fecha_hoy            = "";
		String           enCliente            = "";
		String           enServidor           = "";
		String           enArchivo            = "";
		String           lstArchivos          = "";
		short            suc_opera            = (short) 787;

		EIGlobal.mensajePorTrace( "***SuaDisco.class & Entrando a peticion de archivo &", EIGlobal.NivelLog.INFO);

		contrato             = session.getContractNumber();
		suc_opera            = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		usuario              = session.getUserID8();
		clave_perfil         = session.getUserProfile();
		/*cadena_getFacCOpProg = (session.getFacultad(session.FAC_PROGRAMA_OP));
		fac_programadas      = ( cadena_getFacCOpProg != "" );*/
		fecha_hoy            = ObtenFecha();

		/*
		suc_opera            = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);


		//cadena_getFacCOpProg = session.getFacCOpProg();
		fac_programadas      = true;
		fecha_hoy            = ObtenFecha();*/

		enCliente   = "<input type=hidden name=archivosCliente  value=\"\">";
		enServidor  = "<input type=hidden name=archivosServidor value=\"\">";
		enArchivo   = "<input type=hidden name=archivoFinal     value=\"\">";

		lstArchivos = "\nV5.Archivos importados: 0 (no se han importado archivos)";

		request.setAttribute("newMenu",              session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal",        session.getStrMenu());
		request.setAttribute("Encabezado",           CreaEncabezado("Validaci&oacute;n de Disco SUA", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos", "s25700h", request));
		request.setAttribute("lbArchivoFinal",       enArchivo   );
		request.setAttribute("lbArchivosCliente",    enCliente   );
		request.setAttribute("lbArchivosServidor",   enServidor  );
		request.setAttribute("lbArchivosImportados", lstArchivos );
//		TODO: BIT CU 4101, EL cliente entra al flujo
		/*
		 * VSWF ARR -I
		 */
		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
		BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
		//VSWF - JGAL
		bh.incrementaFolioFlujo(getFormParameter(request, BitaConstants.FLUJO));
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ES_AP_OP_PAGOS_ENTRA);
		bt.setContrato(contrato);

		BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			e.printStackTrace();
		}catch (Exception e) {

			e.printStackTrace();
		}
		 }
		/*
		 * VSWF ARR -F
		 */

		evalTemplate("/jsp/SuaDisco.jsp", request, response );
		//RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/SuaDisco.jsp" );
		//rdSalida.include( request, response );

		//EIGlobal.mensajePorTrace( "***SuaDisco.class & Saliendo de peticion de archivo &" + salida + " &", EIGlobal.NivelLog.INFO);
	}

	public String listarCuentasDeContrato( String contrato, String clavePerfil, short sucOpera )
	{
		String           tipoCuenta     = "";
		String           codigoError    = "";
		String           listaCuentas   = "";
		String           camposFecha    = "";
		String[][]       Cuentas;
		String           tramaEntrada   = "";
		String           cveProducto    = "";

		boolean          tieneFacultad  = false;
		int              indice;
		int              numeraCuentas;

		EIGlobal.mensajePorTrace( "***SuaDisco.class & listarCuentasDeContrato &", EIGlobal.NivelLog.INFO);

		listaCuentas  = "";
		numeraCuentas = 0;

		tramaEntrada  = contrato + "@1@";

		//modificacion para integracion
		/*Cuentas       = ContCtasRelacpara_mis_transferencias( contrato );
		EIGlobal.mensajePorTrace( "***SuaDisco.class & Cuentas:" + Cuentas[0][0] + " &", EIGlobal.NivelLog.INFO);*/

		EIGlobal.mensajePorTrace( "***SuaDisco.class & Termina: listarCuentasDeContrato &", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaDisco.class & listaCuentas : " + listaCuentas + " &", EIGlobal.NivelLog.INFO);

		return(listaCuentas);
	}

	public int importaArchivo( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
                GregorianCalendar  fecha_limite_W3xx = new GregorianCalendar(2006,02,01); //Despues de esta fecha solo se aceptaran vensiones W3xx
                GregorianCalendar  fecha_sistema = new GregorianCalendar();
                String clave_con = new String("*ABCDEFGHIJKLNMOPQRSTUVWXYZ3"); //primer carecter para la clave de nueva version
		boolean  fac_programadas;
		boolean  errorEnArchivo;

		String  contrato             = "";
		//String  cadena_getFacCOpProg = "";
		String  usuario              = "";
		String  clave_perfil         = "";
		String  archivosEnCliente    = "";
		String  archivosEnServidor   = "";
		String  nombreOriginal       = "";
		String  nombreArchivo        = "";
		String  fecha_hoy            = "";
		String  enCliente            = "";
		String  enServidor           = "";
		String  lstArchivos          = "";
		String  fieldArchivo         = "";
		String  archivo              = "";
		String  archivoFinal         = "";
		String  nombreArchivoFinal   = "";
		String  mensaje              = "";
		//String  primeraNA            = "";
		//String  primeraVS            = "";

		boolean  ultimoDisco;
		short    suc_opera   = (short)787;
		int      salida;
		int      indice;
		int      Cuantos;
		int      totalDiscos = 0;
        int tmp;
        String prueba = new String("valor de 0,1 y 1,2= ");
        SuaRegistro rsDatos;
        rsDatos  = new SuaRegistro();

		EIGlobal.mensajePorTrace( "***SuaDisco.class & Entrando al modulo de importacion importaArchivo()...&", EIGlobal.NivelLog.INFO);

		contrato             = session.getContractNumber();
		suc_opera            = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		usuario              = session.getUserID8();
		clave_perfil         = session.getUserProfile();
		/*cadena_getFacCOpProg = session.getFacCOpProg();
		fac_programadas      = ( cadena_getFacCOpProg != "");*/
		fecha_hoy            = ObtenFecha();

		/*
		suc_opera            = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);


		//cadena_getFacCOpProg = session.getFacCOpProg();
		fac_programadas      = true;
		fecha_hoy            = ObtenFecha();*/

		EIGlobal.mensajePorTrace( "***SuaDisco.class & Leyendo y Escribiendo archivo &", EIGlobal.NivelLog.INFO);

		salida = 0;
		errorEnArchivo = false;

		EI_Importar arcImp= new EI_Importar();
		String byteArchivo="";

		try {
			archivosEnCliente  = getFormParameter(request, "archivosCliente");
			archivosEnServidor = getFormParameter(request, "archivosServidor");
			nombreArchivoFinal = getFormParameter(request, "archivoFinal");
			nombreOriginal	   = getFormParameter(request, "nombreArchivoSua");

			EIGlobal.mensajePorTrace( "***SuaDisco.class & archivosEnCliente  ="+archivosEnCliente  , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***SuaDisco.class & archivosEnServidor ="+archivosEnServidor , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***SuaDisco.class & nombreArchivoFinal ="+nombreArchivoFinal , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***SuaDisco.class & nombreOriginal = " +nombreOriginal, EIGlobal.NivelLog.INFO);



		    //byteArchivo=arcImp.importaArchivo(request,response);
			byteArchivo = getFormParameter(request, "fileNameUpload");
			arcImp.nombreArchivo = byteArchivo;

		    EIGlobal.mensajePorTrace( "***SuaDisco Archivo:[" + byteArchivo + "]", EIGlobal.NivelLog.INFO);

			//int sizeArchivo = byteArchivo.length;

			if(byteArchivo.indexOf("ZIPERROR")>=0)
			 {
				despliegaPaginaError( " Imposible Importar el archivo, no es un archivo zip o esta da�ado","Validaci&oacute;n de Disco SUA", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos","s25700h", request, response );
				EIGlobal.mensajePorTrace("***ImportNomina.class &primeralinea & Imposible Importar el archivo, no es un archivo zip o esta da�ado." , EIGlobal.NivelLog.INFO);
				return 1;
			 }

			try {
//LMM			rsDatos     = extraerDatosPreliminares(byteArchivo); // LMM 29 Dic 2003. Correccion, no es necesario la llamada a esta funcion, se propone ir solo por los datos necesarios.
				rsDatos		= extraeImssPatronalFolioSua(byteArchivo);	// LMM 29 Dic 2003. Registro Patronal y Folio del Sua



///////////////////////////////////////////////////////////////////////////
//        Nuevo Boque:
//        validacion fecha limite, nueva version - W3xx -
//        proyecto : SUA 2005
///////////////////////////////////////////////////////////////////////////
    /*

                                if (  (clave_con.indexOf(rsDatos.regSeisVersionSua.substring(0,1))<0) || (clave_con.indexOf(rsDatos.regSeisVersionSua.substring(1,2))<0)   )
                                {
                                        if(fecha_sistema.after(fecha_limite_W3xx))
                                        {

                                                despliegaPaginaError( "Version Sua Invalida desde 01-oct-2005","Validaci&oacute;n de Disco SUA", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos","s25700h", request, response );

                                                return 0;

                                        }else{

                                               EIGlobal.mensajePorTrace( "<<SuaDisco--Version sua aun valida>> ", EIGlobal.NivelLog.INFO);
                                        }
                                 }
                                 else {

                                         es_version_W3xx=true;


                                         }*/
///////////////////////////////////////////////////////////////////////////
//               fin Nuevo Blque - valida fecha y version -
///////////////////////////////////////////////////////////////////////////



				archivo     = rsDatos.regDosImssPatronal.trim() + rsDatos.regDosFolioSua.trim();
				totalDiscos = Integer.parseInt( rsDatos.regSeisNumeroDeDiscos);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}

//			EIGlobal.mensajePorTrace( "***SuaDisco.class / Fase 1 ... LMM"  , EIGlobal.NivelLog.INFO);

			if ( (!archivo.equals("")) && (archivo!=null) )
				nombreArchivoFinal = archivo;

			if( nombreArchivoFinal.equals( "" ) || ( nombreArchivoFinal == null ) )
			{
				Cuantos = (int)Math.random() * 10000;
				nombreArchivoFinal = "tmpSua" + Integer.toString(Cuantos) + ".sua";
			}
//			EIGlobal.mensajePorTrace( "***SuaDisco.class / Fase 2 ... LMM"  , EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("***SuaDisco.class NOMBRE ARCHIVO FINAL = >" +nombreArchivoFinal+ "<", EIGlobal.NivelLog.INFO);
			nombreOriginal.trim();

			if ( nombreOriginal.length() >= 12 )
				nombreArchivo = nombreOriginal.substring( nombreOriginal.length() - 12, nombreOriginal.length());
			else
				nombreArchivo = nombreOriginal;

			EIGlobal.mensajePorTrace("***SuaDisco.class NOMBRE ARCHIVO = >" + nombreArchivo + "<", EIGlobal.NivelLog.INFO);

			//Cambios para la VERSION SUA
			/*primeraNA = nombreArchivo.substring(0, EIGlobal.NivelLog.INFO);
			primeraVS = rsDatos.regSeisVersionSua.substring(0, EIGlobal.NivelLog.INFO);

			System.out.println("PRIMERA NARCHIVO = >" +primeraNA+ "<");
			System.out.println("PRIMERA VERSIONS = >" +primeraVS+ "<");*/

			nombreArchivo =  IEnlace.LOCAL_TMP_DIR + "/" + nombreArchivoFinal + "_" + nombreArchivo;
			archivoFinal  =  IEnlace.LOCAL_TMP_DIR + "/" + nombreArchivoFinal + ".sua";
			mensaje = "nombreArchivo: " + nombreArchivo;

			EIGlobal.mensajePorTrace( "***SuaDisco.class & nombreArchivo : " + nombreArchivo + " &", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***SuaDisco.class & archivoFinal  : " + archivoFinal + " &", EIGlobal.NivelLog.INFO);

			boolean errorEnElArchivo=false;
			if(!arcImp.cambiaNombreArchivo(nombreArchivo))
			  errorEnElArchivo=true;

			/*
			File drvSua = new File(nombreArchivo);
			RandomAccessFile fileSua = new RandomAccessFile(drvSua, "rw");
			fileSua.write(byteArchivo, 0,  sizeArchivo);
			fileSua.close();
			*/

		} catch ( Exception e ) {
			errorEnArchivo = true;
			EIGlobal.mensajePorTrace( "***SuaConsulta.class Excepcion %importaArchivo() Error en escritura >> " + e.getMessage() + " <<", EIGlobal.NivelLog.INFO);
			e.printStackTrace();
			if ( mensaje.equals( "" ) || ( mensaje == null ) )
				mensaje = "Error en el proceso inicial de revisi&oacute;n del disco SUA";
			else
				mensaje = mensaje + ", Error en escritura, " + e.getMessage();
		}
		//cambio realizado validar que la primera letra del archivo sea igual a la de la Version SUA (HGCV)
		/*if (!(primeraNA.equals(primeraVS)))
		{
			despliegaPaginaError( "El nombre del Archivo es Inv&aacute;lido", "Validaci&oacute;n de Disco SUA", "Servicios &gt;" +
											" Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos", request, response);
			System.out.println("LA PRIMERA LETRA DEL ARCHIVO ES DIFERENTE A LA DE VERSION SUA");
		} else {
			System.out.println("SIGO CON LO DEMAS");*/
			if( !errorEnArchivo )
			{
				archivosEnServidor.trim();
				if( archivosEnServidor.length()==0 )
					archivosEnServidor = nombreArchivo;
				else
					archivosEnServidor = archivosEnServidor + "|" + nombreArchivo;
				archivosEnCliente.trim();
				if( archivosEnCliente.length() == 0 )
					archivosEnCliente = nombreOriginal;
				else
					archivosEnCliente = archivosEnCliente + "|" + nombreOriginal;

				EIGlobal.mensajePorTrace("***SuaDisco.class ARCHIVOS EN SERVIDOR = >" +archivosEnServidor+ "<", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("***Suadosco.class ARCHIVOS EN CLIENTE = >" +archivosEnCliente+ "<", EIGlobal.NivelLog.INFO);

				Cuantos = CuantosTokens(archivosEnCliente,  '|');

				if(Cuantos!=totalDiscos){
					//TODO: BIT CU 4101, A3
					/*
		    		 * VSWF ARR -I
		    		 */
					 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
						int num =nombreOriginal.lastIndexOf("/");
					BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_AP_OP_PAGOS_IMPORTA_ARCHIVO);
					bt.setContrato(contrato);
					bt.setNombreArchivo(nombreOriginal.substring(nombreOriginal.lastIndexOf("\\")+1,nombreOriginal.length()));
					bt.setIdErr((new Boolean(errorEnArchivo)).toString());
    				BitaHandler.getInstance().insertBitaTransac(bt);
    				}catch(SQLException e){
    					e.printStackTrace();
    				}catch (Exception e) {
    					e.printStackTrace();
    				}
					 }
		    		/*
		    		 * VSWF ARR - F
		    		 */
					publicarImportaArchivo( archivosEnCliente, archivosEnServidor, nombreArchivoFinal, request, response );
				}
				else{
//					TODO: BIT CU 4101, A3
					/*
		    		 * VSWF ARR -I
		    		 */
					 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
						int num =nombreOriginal.lastIndexOf("/");
					BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_AP_OP_PAGOS_IMPORTA_ARCHIVO);
					bt.setContrato(contrato);
					bt.setNombreArchivo(nombreOriginal.substring(nombreOriginal.lastIndexOf("\\")+1,nombreOriginal.length()));
					bt.setIdErr((new Boolean(errorEnArchivo)).toString());
    				BitaHandler.getInstance().insertBitaTransac(bt);
    				}catch(SQLException e){
    					e.printStackTrace();
    				}catch (Exception e) {
    					e.printStackTrace();
    				}
					 }
		    		/*
		    		 * VSWF ARR - F
		    		 */

					procesaArchivo( rsDatos, archivosEnServidor, archivoFinal, request, response ); //W3xx Carta Rechazo
				}
			} else
				despliegaPaginaError( mensaje , "Validaci&oacute;n de Disco SUA","Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos", request, response );
		//}
		//EIGlobal.mensajePorTrace( "***SuaDisco.class & Saliendo del modulo de importacion " +salida+ " &", EIGlobal.NivelLog.INFO);
		return 1;
	}

	public boolean procesaArchivo( SuaRegistro rsDatos, String archivosPorProcesar, String archivoFinal, HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException //W3xx Carta Rechazo
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String   archivosLeidos[];
		String   listaErrores = "";
		boolean  errorProceso;
		int      cuantos;
		int      indice;
		Vector   dias;
		boolean  facultades;
		String   contrato        = "";
		String   clavePerfil     = "";
		String   listaFacultades = "";
		String   listaCuentas    = "";
		short    sucOpera;

		EIGlobal.mensajePorTrace( "***SuaDisco.class & procesarArchivo &", EIGlobal.NivelLog.INFO);

		cuantos =  CuantosTokens(archivosPorProcesar,  '|');
		archivosLeidos = new String[cuantos];
		for ( indice=1; indice <= cuantos; indice++ )
			archivosLeidos[indice-1] = BuscarToken(archivosPorProcesar, '|', indice);

		dias = CargarDias();

		EIGlobal.mensajePorTrace( "***SuaDisco.class & Inicio: Verificando disco &", EIGlobal.NivelLog.INFO);

		SuaVerificaDisco svdVerifica = new SuaVerificaDisco( archivosLeidos, archivoFinal, dias);

		errorProceso = svdVerifica.Procesar();

		if( errorProceso ) {
			EIGlobal.mensajePorTrace( "***SuaDisco.class & preparando lista de errores &", EIGlobal.NivelLog.INFO);
			publicarErrores(rsDatos, svdVerifica.scsAnalist.suaErrores, request, response );
		} else {
			EIGlobal.mensajePorTrace( "***SuaDisco.class & preparando totales &", EIGlobal.NivelLog.INFO);
			contrato        = session.getContractNumber();
			sucOpera        = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			clavePerfil     = session.getUserProfile();
			listaCuentas = listarCuentasDeContrato(contrato, clavePerfil, sucOpera);

			publicarTotalesSua(svdVerifica.scsRegistro, svdVerifica.scsImpuesto, listaCuentas,
					archivosPorProcesar, request, response );
		}
		EIGlobal.mensajePorTrace( "***SuaDisco.class & Termina procesarArchivo &", EIGlobal.NivelLog.INFO);
		return errorProceso;
	}

	public void publicarImportaArchivo( String archivosCliente, String archivosServidor, String archivoFinal, HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String  enCliente     = "";
		String  enServidor    = "";
		String  lstArchivos   = "";
		String  fieldArchivo  = "";
		String  archivo       = "";
		String  enArchivo     = "";
		int     salida;
		int     indice;
		int     cuantos;

		EIGlobal.mensajePorTrace( "***SuaDisco.class & Iniciar: publicarImportaArchivo &", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaDisco.class & lbArchivosCliente = >" +archivosCliente+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaDisco.class & lbArchivosServidor = > " +archivosServidor+ "<", EIGlobal.NivelLog.INFO);

		enCliente   = "<input type=hidden name=archivosCliente  value=\"" + archivosCliente  + "\">";
		enServidor  = "<input type=hidden name=archivosServidor value=\"" + archivosServidor + "\">";
		enArchivo   = "<input type=hidden name=archivoFinal     value=\"" + archivoFinal     + "\">";

		cuantos =  CuantosTokens(archivosCliente,  '|');
		lstArchivos = "\nArchivos importados: " + Integer.toString( cuantos );

		for( indice = 1; indice <= cuantos; indice++ )
		{
			archivo = BuscarToken(archivosCliente, '|', indice );
			fieldArchivo  = "<tr>\n<td width=10><img src=/gifs/EnlaceMig/spacer.gif width=10 height=1></td>\n";
			fieldArchivo += "<td bgcolor=#F0F0F0>" +archivo+ " </td>\n</tr> \n";
			lstArchivos  = lstArchivos + "|" + fieldArchivo;
		}

		request.setAttribute("newMenu",              session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal",        session.getStrMenu());
		request.setAttribute("Encabezado",           CreaEncabezado("Validaci&oacute;n de Disco SUA", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos", "s25700h", request));
		request.setAttribute("lbArchivoFinal",       enArchivo    );
		request.setAttribute("lbArchivosCliente",    enCliente    );
		request.setAttribute("lbArchivosServidor",   enServidor   );
		request.setAttribute("lbArchivosImportados", lstArchivos  );
		request.setAttribute("Fecha",                ObtenFecha());
		request.setAttribute("Hora",                 ObtenHora());

		//System.out.println("lbArchivoFinal       >" +enArchivo+ "<" );
		//System.out.println("lbArchivosCliente    >" +enCliente+ "<" );
		//System.out.println("lbArchivosServidor   >" +enServidor+ "<");
		//System.out.println("lbArchivosImportados >" +lstArchivos+ "<");

		session.setModuloConsultar(IEnlace.MSua_envio);

		evalTemplate("/jsp/SuaDisco.jsp", request, response );
		//RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/SuaDisco.jsp" );
		//rdSalida.include( request, response );

		EIGlobal.mensajePorTrace( "***SuaDisco.class & Terminar: publicarImportaArchivo &", EIGlobal.NivelLog.INFO);
	}

	public int publicarErrores(SuaRegistro rsDatos, Vector listaErrores, HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException //W3xx Carta Rechazo
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String lista;
		String lista_msg;
		String colorbg;
		int    indice;
		int    cuantos;
		int    salida = 0;
		int    meses=0;


		GregorianCalendar hoy = new GregorianCalendar();

		meses = (int)hoy.get(hoy.MONTH) + 1;
		try {
			lista = "";
			lista_msg = "";
			//Inicia W3xx Carta Rechazo
			lista += "REPORTE DE LA CAUSA QUE IMPIDE LA LECTURA DEL ARCHIVO SUA" + "<BR>";
			lista += "CON EL QUE SE PRETENDE EFECTUAR EL PAGO" + "<BR><BR>";
			lista += "NOMBRE	: " + rsDatos.regDosRazonSocial.trim() + "<BR>";
			lista += "REG.PAT	: " + rsDatos.regDosImssPatronal + "<BR>";
			lista += "R.F.C.	: " + rsDatos.regDosRfcEmpresa + "<BR>";
			if ( rsDatos.regDosPeriodoDePago.length() == 6 )
				lista += "MES   	: " + rsDatos.regDosPeriodoDePago.substring(4, 6) + "-" + rsDatos.regDosPeriodoDePago.substring(0, 4) + "<BR>";
			else
				lista += "MES   	: " + rsDatos.regDosPeriodoDePago + "<BR>";
			lista += "FECHA   	: " + hoy.get(hoy.DAY_OF_MONTH) + "-" + meses + "-" + hoy.get(hoy.YEAR) + "<BR>";
			lista += "FOLIO SUA	: " + rsDatos.regDosFolioSua + "<BR><BR>";
			lista += "Estimado empresario:" + "<BR>";
			lista += "Al validar la informacion presentada por usted para efectura las aportaciones al Sistema de Ahorro" + "<BR>";
			lista += "para el Retiro, se ha presentado el error descrito a continuacion." + "<BR><BR>";

			EIGlobal.mensajePorTrace("***SuaDisco.class LISTA ENCABEZADO = >" +lista+ "<", EIGlobal.NivelLog.INFO);

			cuantos = listaErrores.size();
			for ( indice = 0; indice < cuantos; indice++ ) {
				SuaErrorDisco porLeer = ( SuaErrorDisco ) listaErrores.elementAt( indice );
				String mensaje = SuaErrorDisco.msgError( porLeer.error );
				lista_msg += mensaje + "<BR>";
			}
			lista += lista_msg;
			//termina W3xx Carta Rechazo

			/*request.setAttribute("newMenu",              session.getFuncionesDeMenu());
			request.setAttribute("MenuPrincipal",        session.getStrMenu());
			request.setAttribute("Encabezado",           CreaEncabezado("Pagos del S.U.A.","Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos"));
			request.setAttribute("lbErroresEnDisco",     lista );
			salida = evalTemplate("EnlaceMig/Templates/SuaErrores.html",(ITemplateData) null, mapDisco);
			*/
			despliegaPaginaError( lista ,
					"Pagos del S.U.A.",
					"Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos",
					request, response );
		} catch ( Exception E ) {
			EIGlobal.mensajePorTrace( "***SuaDisco.class Excepcion %publicarErrores() >> " +
				E.getMessage() + " <<", EIGlobal.NivelLog.INFO);
			String mensaje = E.getMessage();
			despliegaPaginaError( mensaje ,
					"Pagos del S.U.A.",
					"Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos",
					request, response );
			salida = 1;
		}
		return ( salida );
	}

	public String ToString( double dato ) {
		String strValor;
		long   valor;


		valor = Math.round( dato * 100 );
		strValor = Long.toString( valor );
		strValor.trim();
		if ( strValor.length() > 2 )
			strValor = strValor.substring( 0, strValor.length()-2) + "." +
					strValor.substring( strValor.length() - 2 );
		else if ( strValor.length() == 2 )
			strValor = "0." + strValor;
		else if ( strValor.length() == 1 )
			strValor = "0.0" + strValor;
		else
			strValor = "0.00";
		strValor.trim();
		return( strValor );
	}

	public String alinearCadena( String cadena, int limite )
	{
		String espacios = "";
		int    indice;
		for( indice = 0; indice<limite; indice++)
			espacios = espacios + " ";

		cadena.trim();
		if( cadena.length() < limite )
			espacios = espacios.substring(0, limite - cadena.length()) + cadena;
		else
		espacios = cadena;

		return(espacios);
	}

	public String ToPublish( double dato, int limite )
	{
		String strPublica;

		strPublica = FormatoMoneda(dato);
		//strPublica = alinearCadena(strPublica.trim(), limite);

		return(strPublica);
	}

	public String construirTrama( String glbUsuario, String glbPtovta, SuaRegistro rsDatos, SuaImpuesto rsImpuesto)
	{
		String[] meses = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
		String fechaLimite     = "";
		String fechaActual     = "";
		String fechaTrama      = "";
		String trama           = "";
		String strTrabajadores = "";
		int    trabajadores;

		try {
			trabajadores = Integer.parseInt(rsDatos.regDosTrabajadores);
		} catch ( Exception E ) {
			trabajadores = 0;
			EIGlobal.mensajePorTrace( "***SuaDisco.class Excepcion %construirTrama() >> "+ E.getMessage() + " <<", EIGlobal.NivelLog.INFO);
		}

		strTrabajadores = Integer.toString(trabajadores);

		fechaActual = ObtenFecha(true);
		fechaTrama  = fechaActual.substring(0, 2);

		fechaTrama += "-";

		fechaTrama += meses[Integer.parseInt(fechaActual.substring(3, 5))-1];

		fechaTrama += "-";

		fechaTrama += fechaActual.substring(6, 10);




		fechaLimite  = rsDatos.regSeisFechaLimite.substring(6, 8);

		fechaLimite += "-";

		fechaLimite += meses[Integer.parseInt(rsDatos.regSeisFechaLimite.substring(4, 6))-1];

		fechaLimite += "-";

		fechaLimite += rsDatos.regSeisFechaLimite.substring(0, 4);

		if(meses[Integer.parseInt(rsDatos.regSeisFechaLimite.substring(4, 6))-1].equals("APR") && rsDatos.regSeisFechaLimite.substring(0, 4).equals("2003"))

			fechaLimite="21-APR-2003";



		trama  =        rsDatos.regDosFolioSua.trim();

		trama += "@"  + rsDatos.regDosImssPatronal.trim();

		trama += "@"  + rsDatos.regDosFolioSua.trim() + ".TMP";

		trama += "@"  + "ESU";

		trama += "@"  + fechaLimite.trim();

		trama += "@";

		trama += "@"  + fechaTrama.trim();

		trama += "@";

		trama += "@";

		trama += "@";

		trama += "@"  + rsDatos.regDosAnioDePago.trim() + rsDatos.regDosPeriodoDePago.trim();

		trama += "@"  + "VALID";

		trama += "@";

		trama += "@"  + strTrabajadores.trim();

		trama += "@";

		trama += "@"  + glbPtovta.trim();

		trama += "@"  + glbUsuario.trim();



		trama += "@"  + ToString( rsImpuesto.impCincoEnfermedadCuota );

		trama += "@"  + ToString( rsImpuesto.impCincoEnfermedadExcedente );

		trama += "@"  + ToString( rsImpuesto.impCincoEnfermedadPrestaciones );

		trama += "@"  + ToString( rsImpuesto.impCincoGastosMedicos );

		trama += "@"  + ToString( rsImpuesto.impCincoRiesgosDeTrabajo );

		trama += "@"  + ToString( rsImpuesto.impCincoInvalidezVida );

		trama += "@"  + ToString( rsImpuesto.impCincoGuarderiaPrestaciones );

		trama += "@"  + ToString( rsImpuesto.impCincoSegurosImss );

		trama += "@"  + ToString( rsImpuesto.impCincoSegurosImssActualizacion );

		trama += "@"  + ToString( rsImpuesto.impCincoSegurosImssRecargos );

		trama += "@"  + ToString( rsImpuesto.impCincoRetiro );

		trama += "@"  + ToString( rsImpuesto.impCincoCesantiaVejez );

		trama += "@"  + ToString( rsImpuesto.impCincoRetiro + rsImpuesto.impCincoCesantiaVejez );

		trama += "@"  + ToString( rsImpuesto.impCincoRetiroCesantiaActualizacion );

		trama += "@"  + ToString( rsImpuesto.impCincoRetiroCesantiaVejezRecargos );

		trama += "@"  + ToString( rsImpuesto.impCincoAportacionVoluntaria );

		trama += "@"  + ToString( rsImpuesto.impCincoAportacionPatronal );

		trama += "@"  + ToString( rsImpuesto.impCincoAmortizacionCredito );

		trama += "@"  + ToString( rsImpuesto.impCincoInfonavitSubtotal );

		trama += "@"  + ToString( rsImpuesto.impCincoInfonavitActualizacion );

		trama += "@"  + ToString( rsImpuesto.impCincoInfonavitRecargos );

		trama += "@";

		trama += "@"  + "TX" + glbPtovta.trim();

		trama += "@"  + rsDatos.regDosDireccion.trim();

		trama += "@"  + rsDatos.regDosRazonSocial.trim();

		trama += "@"  + rsDatos.regDosRfcEmpresa.trim();

		trama += "@"  + rsDatos.regDosPoblacion.trim();

		trama += "@"  + rsDatos.regDosEstado.trim();

		trama += "@"  + "014";

	//*************************************************

        //* INICIO NUEVO BLOQUE

        //* agrega nuevos campos a la trama

        //* ************************************************



                trama += "@"  + ToString(rsImpuesto.impCincoInfonavitMultas);

                trama += "@"  + ToString(rsImpuesto.impCincoFundemexDonativo);

                trama += "@"  + ToString(rsImpuesto.impCincoAportacionComplementaria);

                trama += "@"  + rsDatos.regSeisVersionSua.trim();



         //************************************************

         //* fin nuevo bloque

         //************************************************



                trama += "@";



		return(trama);

	}



	public void publicarTotalesSua( SuaRegistro rsDatos, SuaImpuesto rsImpuesto, String listaCuentas, String archivosServidor, HttpServletRequest request, HttpServletResponse response )

		throws IOException, ServletException

	{

		HttpSession sess = request.getSession();

		BaseResource session = (BaseResource) sess.getAttribute("session");



		String           fechaHoy;

		String           trama;

		int              trabajadores;

		int              indice;

		int              salida;

		int              bimestre;



		fechaHoy = ObtenFecha();

		trama = construirTrama( session.getUserID8(), "0981", rsDatos, rsImpuesto);



		request.setAttribute("trama_referencia",  trama );

		request.setAttribute("cboCuentaCargo",    listaCuentas );

		request.setAttribute("txtFolioAutoriza",  "" );



		try {

			trabajadores = Integer.parseInt(rsDatos.regDosTrabajadores);

		} catch ( Exception E ) {

			EIGlobal.mensajePorTrace( "***SuaDisco.class Excepcion %publicarTotalesSua() >> " +

					E.getMessage() + " <<", EIGlobal.NivelLog.INFO);

			trabajadores = 0;

		}



		request.setAttribute("txtNumTrabaja",     Integer.toString(trabajadores)   );

		request.setAttribute("txtNumTrabajaCred", Integer.toString(rsDatos.trabajadoresConCredito) );

		request.setAttribute("txtRegPatronal",    rsDatos.regDosImssPatronal );

		request.setAttribute("txtFolioSua",       rsDatos.regDosFolioSua );

		request.setAttribute("txtRFC",            rsDatos.regDosRfcEmpresa );

		request.setAttribute("txtImssPeriodo",    rsDatos.regDosPeriodoDePago );

		request.setAttribute("txtImssYear",       rsDatos.regDosAnioDePago );

		request.setAttribute("txtReferencia",     "" );

		request.setAttribute("txtNomRazSoc",     rsDatos.regDosRazonSocial );

		bimestre = Integer.parseInt(rsDatos.regDosPeriodoDePago);

		if( (bimestre % 2) == 0) {

			bimestre = bimestre / 2;

			request.setAttribute("txtSarBimestre", Integer.toString(bimestre) );

			request.setAttribute("txtSarYear",     rsDatos.regDosAnioDePago );

		}



		request.setAttribute("txtImssImpSeg",      ToPublish( rsImpuesto.impCincoSegurosImss  ,14 ));

		request.setAttribute("txtImssImpAct",      ToPublish( rsImpuesto.impCincoSegurosImssActualizacion, 14 ));

		request.setAttribute("txtImssImpRec",      ToPublish( rsImpuesto.impCincoSegurosImssRecargos, 14) );

		request.setAttribute("txtImssTotal",       ToPublish( rsImpuesto.impCincoTotalSegurosImss, 14) );



		request.setAttribute("txtAfoImpRCV",       ToPublish( rsImpuesto.impCincoImporteAfore, 14) );

		request.setAttribute("txtAfoImpAct",       ToPublish( rsImpuesto.impCincoRetiroCesantiaActualizacion, 14) );

		request.setAttribute("txtAfoImpRec",       ToPublish( rsImpuesto.impCincoRetiroCesantiaVejezRecargos, 14) );



		request.setAttribute("txtAfoSubTotal",     ToPublish( rsImpuesto.impCincoSubtotalAfore, 14 ));

		request.setAttribute("txtAfoImpAV",        ToPublish( rsImpuesto.impCincoAportacionVoluntaria , 14) );

		request.setAttribute("txtAfoTotal",        ToPublish( rsImpuesto.impCincoTotalAfore , 14) );



		request.setAttribute("txtInfImpApo",       ToPublish( rsImpuesto.impCincoAportacionPatronal , 14 ));

		request.setAttribute("txtInfImpAct",       ToPublish( rsImpuesto.impCincoAmortizacionCredito, 14 ));

		request.setAttribute("txtInfSubTotal",     ToPublish( rsImpuesto.impCincoSubTotalInfonavit, 14  ));



		request.setAttribute("txtInfAmortCred",    ToPublish( rsImpuesto.impCincoInfonavitSubtotal, 14 ));

		request.setAttribute("txtInfActAmortCred", ToPublish( rsImpuesto.impCincoInfonavitActualizacion, 14 ));

		request.setAttribute("txtInfRecAmortCred", ToPublish( rsImpuesto.impCincoInfonavitRecargos, 14 ));

		request.setAttribute("txtInfTotal",        ToPublish( rsImpuesto.impCincoTotalInfonavit, 14 ));

		request.setAttribute("txtGranTotal",       ToPublish( rsImpuesto.impCincoGranTotal,  14 ));



		request.setAttribute("total_a_pagar",      ToString( rsImpuesto.impCincoGranTotal) );

		request.setAttribute("nombre_archivo",     rsDatos.archivoFinal  );



		request.setAttribute("strRegPatronal",     rsDatos.regDosImssPatronal );

		request.setAttribute("strFolio",           rsDatos.regDosFolioSua     );



		request.setAttribute("newMenu",            session.getFuncionesDeMenu());

		request.setAttribute("MenuPrincipal",      session.getStrMenu());

		request.setAttribute("Encabezado",         CreaEncabezado("Pago de SUA","Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos", "s25700h", request));



		request.setAttribute("archivosServidor",    archivosServidor);

                request.setAttribute("txtMultasInfonavit",  ToPublish( rsImpuesto.impCincoInfonavitMultas, 14 ));

                request.setAttribute("txtDonativofundemex", ToPublish( rsImpuesto.impCincoFundemexDonativo, 14 ));

                request.setAttribute("txtVersionSua",       rsDatos.regSeisVersionSua );

                request.setAttribute("txtAportacionesComp", ToPublish(rsImpuesto.impCincoAportacionComplementaria, 14));



		evalTemplate("/jsp/SuaImporta.jsp", request, response );

		//RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/SuaImporta.jsp" );

		//rdSalida.include( request, response );

	}

}