package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.beans.NomPreLM1H;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.beans.NomPreLM1D.Relacion;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EnlcTarjetaBO;
import mx.altec.enlace.bo.EnlcTarjetaBOImpl;
import mx.altec.enlace.bo.NomPreBusquedaAction;
import mx.altec.enlace.dao.DaoException;
import mx.altec.enlace.dao.NomPreTarjetaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * @author Berenice Mesquitillo
 * Fecha: 24/junio/2009
 * proyecto: 200813700 Nomina Prepago
 * Descripcion: Servlet para la reasignacion de tarjeta de prepago.
 */
 public class NomPreReposicionTarjeta extends BaseServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final static String REASIGNA_TAR_INICIA="inicia";
	private final static String REASIGNA_TAR_CONS="consulta";
	private final static String REASIGNA_TAR_EJECUTA="ejecuta";

	public NomPreReposicionTarjeta() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	private void defaultAction(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {

		String accion = request.getParameter("opcion");
		accion = accion == null ||  accion.length() == 0? REASIGNA_TAR_INICIA : accion;

		if(SesionValida(request, response))
		{
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			String strMenu =  (session.getstrMenu());
			strMenu =  (strMenu != null ? strMenu : "");
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request.setAttribute("newMenu", session.getFuncionesDeMenu());

			//CGH Valida servicio de nomina
			if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
				request.setAttribute("Encabezado", CreaEncabezado( "Reposici&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Reposici&oacute;n","s25320h",request));//YHG NPRE
				return;
			}

			if(accion.trim().equals(REASIGNA_TAR_INICIA)){
				request.setAttribute("Encabezado", CreaEncabezado( "Reposici&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Reposici&oacute;n","s25320h",request));//YHG NPRE
				inicia(request, response);
			}else if(accion.trim().equals(REASIGNA_TAR_CONS)){
				request.setAttribute("Encabezado", CreaEncabezado( "Reposici&oacute;n - confirmaci&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Reposici&oacute;n","s25320h",request));//YHG NPRE
				consulta(request, response,session);
			}else if(accion.trim().equals(REASIGNA_TAR_EJECUTA)){
				request.setAttribute("Encabezado", CreaEncabezado( "Reposici&oacute;n - confirmaci&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Reposici&oacute;n","s25320h",request));//YHG NPRE
				ejecuta(request, response,session);


			}

		}else{
				request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response);
			}



	}
	/**
	 * @author Berenice Mesquitillo
	 *  Metodo inicial, carja el jsp inicial ya sea para la reasignacion de tarjeta
	 * @param request
	 * @param response
	 * Fecha: 24-06-2009
	 */
	private void inicia(final HttpServletRequest request, HttpServletResponse response){
		try {

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_REASIGNACION_ENTRA);
			NomPreUtil.bitacoriza(request,null,null,true, bt);
			evalTemplate(IEnlace.NOMPRE_REASIGNA_TARJETA, request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "inicia" + " -Termina",EIGlobal.NivelLog.INFO);

	}
	/**
	 * @author Berenice Mesquitillo
	 * Metodo donde se consultan los datos de la tarjeta que se pretende reasignar.
	 * @param request
	 * @param response
	 * Fecha: 24-06-2009
	 */
	private void consulta(final HttpServletRequest request, HttpServletResponse response, BaseResource session){
		String numTarAnt=getFormParameter(request, "tarjetaAnt");//request.getParameter("tarjetaAnt");
		String numTarNueva=getFormParameter(request, "tarjetaNueva");//request.getParameter("tarjetaNueva");
		String edoBitAux = "";
		String nomEmpAux = "";
		String codOperacion = "";
		EIGlobal.mensajePorTrace("NomPreReposicionTarjeta: consulta", NivelLog.INFO);
		try {

			NomPreLM1D lm1d = new NomPreBusquedaAction().obtenRelacionTarjetaEmpleado(session.getContractNumber(), numTarAnt, null, null);

			if(lm1d != null && lm1d.getDetalle() != null && lm1d.getDetalle().size() > 0){

				Relacion relacion = lm1d.getDetalle().get(0);

				nomEmpAux = relacion.getEmpleado().getNombre() + " " + relacion.getEmpleado().getPaterno() + " " + relacion.getEmpleado().getMaterno();
				request.setAttribute("numempleado", relacion.getEmpleado().getNoEmpleado());
				request.setAttribute("nomempleado", nomEmpAux);
				request.setAttribute("numtarjetaAnt",numTarAnt);
				request.setAttribute("edotarjeta",relacion.getTarjeta().getDescEstadoTarjeta());
				request.setAttribute("numtarjetaNueva",numTarNueva);
				edoBitAux = relacion.getTarjeta().getDescEstadoTarjeta();
				codOperacion = lm1d.getCodigoOperacion();
				EIGlobal.mensajePorTrace("NomPreReposicionTarjeta: Operacion :" + codOperacion + "Nuevo Estado :" + edoBitAux, NivelLog.INFO);

			}else{

				String cuadroDialogo="cuadroDialogo(\"No se encontr&oacute; informaci&oacute;n para la tarjeta: "+numTarAnt+"\", 1)";
				request.setAttribute("opreasigna","consulta");
				request.setAttribute("mensajereasigna", cuadroDialogo);

			}

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_REASIGNACION_CONSULTA);
			if(edoBitAux!=null)
				bt.setEstatus(edoBitAux);
			if(numTarAnt!=null)
				bt.setCctaOrig(numTarAnt);
			if(nomEmpAux!=null)
				bt.setBeneficiario(nomEmpAux);
			NomPreUtil.bitacoriza(request,"LM1F", codOperacion, bt);
			evalTemplate(IEnlace.NOMPRE_REASIGNA_TARJETA_TRAS, request, response);
		} catch (ServletException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace("Error :" + e.getMessage(), NivelLog.ERROR);
		} catch (IOException e) {
			e.printStackTrace();
			EIGlobal.mensajePorTrace("Error :" + e.getMessage(), NivelLog.ERROR);
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "consultaBloqueo" + " -Termina",EIGlobal.NivelLog.INFO);

	}

	/**
	 * @author Berenice Mesquitillo
	 * Metodo que realiza la reasignacion de tarjeta prepago
	 * @param request
	 * @param response
	 * Fecha:24-06-2009
	 */
	private void ejecuta(HttpServletRequest request, HttpServletResponse response,BaseResource session){
		try {

			String numTarjetaAnt=request.getParameter("tarjetaAnt");
			String numTarjetaNueva=request.getParameter("tarjetaNueva");
			String numEmpleado=request.getParameter("numEmp");
			String nomEmpleado=request.getParameter("nomEmp");
			String estatus=request.getParameter("estatus");
			String cuadroDialogo="";
			String operConfirma="";
			String codErrBit=" ";
			EIGlobal.mensajePorTrace(numTarjetaAnt+" - "+numTarjetaNueva+" - "+numEmpleado+"-"+nomEmpleado+" - "+estatus, NivelLog.INFO);
			NomPreTarjetaDAO prepagoDAO=new NomPreTarjetaDAO();
			NomPreLM1H npLM1H=prepagoDAO.reasignarTarjeta(session.getContractNumber(),numEmpleado, numTarjetaAnt, numTarjetaNueva);
			EIGlobal.mensajePorTrace("NomPreReposicionTarjeta codExito: " + npLM1H.isCodExito(), NivelLog.INFO);
			if(npLM1H!=null && npLM1H.isCodExito()){
				NomPreLM1D lm1d = new NomPreBusquedaAction().obtenRelacionTarjetaEmpleado(session.getContractNumber(), numTarjetaAnt, null, null);
				if(lm1d != null && lm1d.getDetalle() != null && lm1d.getDetalle().size() > 0){
					Relacion relacion = lm1d.getDetalle().get(0);
					estatus=relacion.getTarjeta().getDescEstadoTarjeta();
				}
				try
				{
					int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
					if(npLM1H!=null && npLM1H.getCodigoOperacion()!=null && !npLM1H.getCodigoOperacion().trim().equals("")){
						codErrBit=npLM1H.getCodigoOperacion();
					}
					String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID8()," ",0.0,NomPreUtil.ES_NP_TARJETA_REASIGNACION_OPER," ",0," ");
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
				}
				operConfirma="ejecutaOK";
				cuadroDialogo="cuadroDialogo(\"La tarjeta: "+numTarjetaNueva+" ha sido repuesta\", 1)";
				
				//Se Vincula la tarjeta repuesta.
				EnlcTarjetaBO boAlta;
				EIGlobal.mensajePorTrace("NomPreReposicionTarjeta::ejecuta:: iniciando con la vinculación de la tarjeta.", NivelLog.INFO);
				boAlta = new EnlcTarjetaBOImpl();
				TarjetaContrato tarjetaVinc;
				tarjetaVinc = new TarjetaContrato();
				tarjetaVinc.setEstatus("A");
				tarjetaVinc.setContrato(session.getContractNumber());
				tarjetaVinc.setTarjeta(numTarjetaNueva);
				tarjetaVinc.setUsuarioReg(numEmpleado);

				try {
					ArrayList<TarjetaContrato> listTarj;
					listTarj = new ArrayList<TarjetaContrato>();
					listTarj.add(tarjetaVinc);
					EIGlobal.mensajePorTrace("NomPreReposicionTarjeta::ejecuta:: insertando la vinculación de la tarjeta.", NivelLog.INFO);
					boAlta.agregarTarjeta(listTarj, request);
					EIGlobal.mensajePorTrace("NomPreReposicionTarjeta::ejecuta:: finalizando la vinculación de la tarjeta.", NivelLog.INFO);
				} catch (DaoException e) {
					EIGlobal.mensajePorTrace("NomPreReposicionTarjeta::ejecuta:: Error al vincular la tarjeta->"+e.getMessage(), NivelLog.INFO);
				}
				
			}else{
				cuadroDialogo="cuadroDialogo(\"No se ha podido realizar la reposici&oacute;n de la tarjeta: "+numTarjetaAnt+"\", 1)";
			}

			request.setAttribute("numempleado",numEmpleado);
			request.setAttribute("nomempleado",nomEmpleado);
			request.setAttribute("numtarjetaAnt",numTarjetaAnt);
			request.setAttribute("numtarjetaNueva",numTarjetaNueva);
			request.setAttribute("edotarjeta",estatus);
			request.setAttribute("opreasigna",operConfirma);
			request.setAttribute("mensajereasigna",cuadroDialogo);

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_REASIGNACION_CONFIRMA);
			if(estatus!=null)
				bt.setEstatus(estatus.substring(0, 1));
			if(numTarjetaNueva!=null)
				bt.setCctaOrig(numTarjetaNueva);
			if(nomEmpleado!=null)
				bt.setBeneficiario(nomEmpleado);
			if(npLM1H!=null && npLM1H.getCodigoOperacion()!=null){
				codErrBit=npLM1H.getCodigoOperacion();
			}

			NomPreUtil.bitacoriza(request,"LM1H",codErrBit, bt);

			evalTemplate(IEnlace.NOMPRE_REASIGNA_TARJETA_TRAS, request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "inicia" + " -Termina",EIGlobal.NivelLog.INFO);

	}
}