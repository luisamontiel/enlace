/*
 * ChesCancelar.java
 *
 * Created on 14 de enero de 2003, 09:53 AM
 */

package mx.altec.enlace.servlets;

import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.ches.ChesResultadoCancelacion;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



/** Cancela una serie de cheques de seguridad
 * @author Rafael Martinez Montiel
 * @version 2 se rehizo la mayor parte del codigo como parte del proyecto de chequera de seguridad simplificada
 */
public class ChesCancelar extends BaseServlet {
    /** nombre de la clase para rapida referencia
     */
    public static final String CLASS_NAME = ChesCancelar.class.getName ();

    /** Cadena con el nombre del modulo para los encabezados
     */
    public static final String S_MODULO = "Cancelaci&oacute;n de cheques de seguridad";
    /** Cadena con la cabecera del modulo
     */
    public static final String S_CABECERA = "Servicios &gt; Chequera Seguridad &gt; Cancelaci&oacute;n de cheques";
    /** Despachador por defecto
     */
    public static final String DEFAULT_DISPATCHER = "/jsp/ches/ChesCancelarResult.jsp";
    /** Initializes the servlet.
     * @param config per spec
     * @throws ServletException per spec
     */
    public void init (ServletConfig config) throws ServletException {
        super.init (config);

    }

    /** Destroys the servlet.
     */
    public void destroy () {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        EIGlobal.mensajePorTrace (CLASS_NAME + ": entrando processRequest()", EIGlobal.NivelLog.INFO);
        if(! SesionValida ( request,response) ){
            EIGlobal.mensajePorTrace (CLASS_NAME + ": saliendo processRequest()", EIGlobal.NivelLog.INFO);
            return;
        }
        BaseResource baseResource = (BaseResource)request.getSession ().getAttribute ("session");
        if(null == baseResource ){
            EIGlobal.mensajePorTrace (CLASS_NAME + ": baseResource="+ baseResource, EIGlobal.NivelLog.INFO);
            request.setAttribute ("Error",IEnlace.MSG_PAG_NO_DISP);
            request.getRequestDispatcher ("/jsp/EI_Error.jsp").forward (request,response) ;
            EIGlobal.mensajePorTrace (CLASS_NAME + ": saliendo processRequest()", EIGlobal.NivelLog.INFO);
            return;
        }
        String encabezado = CreaEncabezado (S_MODULO,S_CABECERA, request);
        request.setAttribute ("encabezado",encabezado);

        String requestDispatcher = DEFAULT_DISPATCHER;
        baseResource.setModuloConsultar ( IEnlace.MAlta_ctas_cheq_seg   );
        requestDispatcher = cancelarCheques (request,response);


        if(null != requestDispatcher){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": requestDispatcher=" + requestDispatcher, EIGlobal.NivelLog.INFO);
            request.getRequestDispatcher ( requestDispatcher ).forward ( request,response);
        }
        EIGlobal.mensajePorTrace ( CLASS_NAME + ": saliendo del processRequest()", EIGlobal.NivelLog.INFO);
    }

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException per spec
     * @throws IOException per spec */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException per spec
     * @throws IOException per spec */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     * @return per spec
     */
    public String getServletInfo () {
        return "Short description";
    }

    /** Cancela una serie de cheques
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws IOException per spec
     * @throws ServletException per spec
     * @return String con el requestDispatcher a utilizar, null si ya se hizo un forward o un include
     */
    protected String cancelarCheques ( HttpServletRequest request, HttpServletResponse response)
    throws java.io.IOException ,javax.servlet.ServletException{
        String requestDispatcher = null;
        int countReg = 0;
        String cuentaBit ="";
        java.util.List resOp = new java.util.LinkedList ();
        try{

            java.util.List listReg = new java.util.ArrayList ();

            if(null == request.getParameter ("reg")){
                EIGlobal.mensajePorTrace (CLASS_NAME + " - reg es NULL", EIGlobal.NivelLog.DEBUG);
                throw new Exception ("No hay ning&uacute;n registro seleccionado, NULL");
            }
            EIGlobal.mensajePorTrace (CLASS_NAME + " - obteniendo registros a eliminar" , EIGlobal.NivelLog.INFO);
            String [] regs = request.getParameterValues ("reg");
            if(null == regs){
                EIGlobal.mensajePorTrace (CLASS_NAME + " - regs no contiene elementos, NULL" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("No hay ning&uacute;n registro seleccionado.");
            }else if(regs.length < 1){
                EIGlobal.mensajePorTrace (CLASS_NAME + " - regs no contiene elementos" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("No hay ning&uacute;n registro seleccionado.");
            }

            EIGlobal.mensajePorTrace (CLASS_NAME + " - registros a eliminar: " + regs.length , EIGlobal.NivelLog.INFO);
            for(int c=0;c<regs.length;++c){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": dato=" + regs[c], EIGlobal.NivelLog.INFO);
                listReg.add ( regs[c] );

            }
            EIGlobal.mensajePorTrace (CLASS_NAME + " - obteniendo registros a eliminar: " + listReg , EIGlobal.NivelLog.INFO);
            ServicioTux servicio = new ServicioTux ();
            //servicio.setContext ( ((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext () );
            BaseResource baseResource = (BaseResource)request.getSession ().getAttribute ("session");
            String tramaBase = "1EWEB|"
            + baseResource.getUserID8 () + "|"
            + "CACH" + "|"
            + baseResource.getContractNumber () + "|"
            + baseResource.getUserID8() + "|"
            + baseResource.getUserProfile () + "|";
            EIGlobal.mensajePorTrace (CLASS_NAME + " - trama base:" + tramaBase, EIGlobal.NivelLog.INFO);
            java.util.ListIterator li = listReg.listIterator ();
            while(li.hasNext ()){
                String reg = (String) li.next ();
                EIGlobal.mensajePorTrace (CLASS_NAME + " - eliminando: "  + reg, EIGlobal.NivelLog.INFO);
                String trama = tramaBase + reg + "@";
                mx.altec.enlace.ches.ChesResultadoCancelacion resCan = new mx.altec.enlace.ches.ChesResultadoCancelacion ();
                java.util.StringTokenizer tok = new java.util.StringTokenizer ( reg , "@");
                cuentaBit=tok.nextToken ();
                resCan.setNumCuenta ( cuentaBit );
                resCan.setNumCheque ( tok.nextToken () );
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": tramaSalida=" + trama, EIGlobal.NivelLog.INFO);
                java.util.Hashtable ret = servicio.web_red (trama);

                if(null == ret){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                    throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
                }

                String tramaRetorno = (String) ret.get ("BUFFER");
                if(null == tramaRetorno){
                    EIGlobal.mensajePorTrace (CLASS_NAME + " - buffer null" , EIGlobal.NivelLog.INFO);
                    throw new Exception ("Buffer es null");
                }
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama retorno=\n" + tramaRetorno, EIGlobal.NivelLog.DEBUG);
                try{
                    if(null == tramaRetorno){
                        EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                        throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
                    }else if( (tramaRetorno.startsWith ("OK")) && (-1 != tramaRetorno.indexOf ('@') ) ){

                        java.util.StringTokenizer tokTrama =
                        new java.util.StringTokenizer ( tramaRetorno.substring ( tramaRetorno.indexOf ('@') ),"@");
                        resCan.setDescripcion ( tokTrama.nextToken () );
                        resOp.add ( resCan );
//                      TODO: BIT CU 4191, A7
                        /*
                		 * VSWF ARR -I
                		 */
                        if (Global.USAR_BITACORAS.trim().equals("ON")){
            	            try{
            				BitaHelper bh = new BitaHelperImpl(request, baseResource,request.getSession());
            				BitaTransacBean bt = new BitaTransacBean();
            				bt = (BitaTransacBean)bh.llenarBean(bt);
            				bt.setNumBit(BitaConstants.ES_CH_SEG_CONS_CHEQUES_CANC_CANCELA_CHEQUES);
            				bt.setContrato(baseResource.getContractNumber());
            				bt.setServTransTux("CACH");
            				if(tramaRetorno!=null){
            					 if(tramaRetorno.substring(0,2).equals("OK")){
            						   bt.setIdErr("CACH0000");
            					 }else if(tramaRetorno.length()>8){
            						  bt.setIdErr(tramaRetorno.substring(0,8));
            					 }else{
            						  bt.setIdErr(tramaRetorno.trim());
            					 }
            					}
            				if(cuentaBit!=null && !cuentaBit.equals(""))
            					bt.setCctaOrig(cuentaBit);
            				BitaHandler.getInstance().insertBitaTransac(bt);
            				}catch (SQLException e){
            					e.printStackTrace();
            				}catch(Exception e){
            					e.printStackTrace();
            				}
                        }
                		/*
                		 * VSWF ARR -F
                		 */
                    }else if(tramaRetorno.startsWith ("OK") ){
                        EIGlobal.mensajePorTrace ( CLASS_NAME + ": el servicio respondio de manera inadecuada:" + tramaRetorno, EIGlobal.NivelLog.DEBUG);
                        throw new Exception ( "la trama de retorno deberia contener @");
                    }else{
                        EIGlobal.mensajePorTrace ( CLASS_NAME + ": el servicio respondio de manera inadecuada:" + tramaRetorno, EIGlobal.NivelLog.DEBUG);
                        throw new Exception ( "el servicio respondio de manera inadecuada");
                    }
                }catch(java.util.NoSuchElementException ex){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": la trama de respuesta no tiene el formato adecuado", EIGlobal.NivelLog.DEBUG);
                    throw new Exception ( "la trama de respuesta no tiene el formato adecuado");
                }
            }
            request.setAttribute ( "resOp", resOp );
            requestDispatcher = DEFAULT_DISPATCHER;

        }catch(java.util.NoSuchElementException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": Los parametros de entrada no tienen el formato adecuado", EIGlobal.NivelLog.DEBUG);
            despliegaPaginaErrorURL ( "No se pudo realizar la operaci&oacute;n",S_MODULO,S_CABECERA,ChesConsulta.MOD_CHEQUE_S_HLP_INICIAL,
            "ChesConsulta?" +ChesConsulta.PAR_MODULO + "=" + ChesConsulta.CONSULTA_CHEQUES,request,response);
        }catch(NumberFormatException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": no se pudo obtener la cuenta de registros seleccionados", EIGlobal.NivelLog.DEBUG);
            despliegaPaginaErrorURL ( "No se pudo realizar la operaci&oacute;n",S_MODULO,S_CABECERA,ChesConsulta.MOD_CHEQUE_S_HLP_INICIAL,
            "ChesConsulta?" +ChesConsulta.PAR_MODULO + "=" + ChesConsulta.CONSULTA_CHEQUES,request,response);
        }catch(Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": no se pudieron cancelar los cheques," + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            ex.printStackTrace ();
            if(resOp.isEmpty ()){
                despliegaPaginaErrorURL ( "No se pudo realizar la operaci&oacute;n",S_MODULO,S_CABECERA,ChesConsulta.MOD_CHEQUE_S_HLP_INICIAL,
                "ChesConsulta?" +ChesConsulta.PAR_MODULO + "=" + ChesConsulta.CONSULTA_CHEQUES,request,response);
            }else{
                request.setAttribute ( "resOp",resOp);
                request.setAttribute ( "msg", "No se pudo cancelar el resto de la selecci&oacute;n");
                requestDispatcher = DEFAULT_DISPATCHER;
            }
        }
        return requestDispatcher;
    }

}