package mx.altec.enlace.servlets;

import java.io.*;
import java.net.*;

import javax.servlet.http.*;
import javax.servlet.*;

import admin.stats.e;

import com.passmarksecurity.PassMarkDeviceSupportLite;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Hashtable;
import java.sql.*;
import java.util.concurrent.ExecutionException;

import mx.altec.enlace.beans.RSABeanAUX;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.WSBloqueoToken;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.RSADAO;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.HttpTimeoutHandler;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.UtilidadesRSA;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
/*Inicia - EVERIS VALIDACION CSRF */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Inicia - EVERIS VALIDACION CSRF */
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;

import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bo.BloqueoUsuarioBO;

/************************************************
 *
 * @author MDR
 * incia import para RSA
 *
 */
import mx.altec.enlace.utilerias.RSAFI;
import mx.altec.enlace.utilerias.RSAFII;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;

/************************************************
 *
 * MDR termina Import para RSA
 *
 */

// VC ESC optimizacion URL Connection
public class LoginServlet extends BaseServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	/** The Constant TEXT_HTML. */
	private static final String TEXT_HTML = "text/html";
	
	/** The Constant COOKIE_KEY_DEVICE_TOKEN. */
	private static final String COOKIE_KEY_DEVICE_TOKEN = "deviceTokenCookie";
	
	/** The Constant COOKIE_KEY_DEVICE_TOKEN_FSO. */
	private static final String COOKIE_KEY_DEVICE_TOKEN_FSO = "deviceTokenFSO";
	
	/** The Constant PARAMETER_KEY_USUARIO8. */
	private static final String PARAMETER_KEY_USUARIO8 = "usuario8";
	
	/** The Constant ATTRIBUTE_KEY_MENSAJE_LOGIN01. */
	private static final String ATTRIBUTE_KEY_MENSAJE_LOGIN01 = "MensajeLogin01";
	
	/** The Constant ATTRIBUTE_KEY_FECHA. */
	private static final String ATTRIBUTE_KEY_FECHA = "Fecha";

	/************************************************************
	 * get
	 *
	 * @param request
	 *            variable Request
	 * @param response
	 *            variable Response
	 * @throws IOException
	 *             Excepcion Generica
	 * @throws ServletException
	 *             Excepcion Generica
	 ***********************************************************/
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(TEXT_HTML);
		defaultAction(request, response);
	}

	/************************************************************
	 * post
	 *
	 * @param request
	 *            variable Request
	 * @param response
	 *            variable Response
	 * @throws IOException
	 *             Excepcion Generica
	 * @throws ServletException
	 *             Excepcion Generica
	 ***********************************************************/
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(TEXT_HTML);
		defaultAction(request, response);
	}

	/************************************************************
	 * defaultAction Realiza acciones generales para get y post
	 *
	 * @param request
	 *            variable Request
	 * @param response
	 *            variable Response
	 * @throws IOException
	 *             Excepcion Generica
	 * @throws ServletException
	 *             Excepcion Generica
	 ***********************************************************/
	public void defaultAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		boolean banderaFaseII=Global.BANDERA_RSAFII;
		String  accionRSA = "";
		/*INICIA VALIDACION CSRF TOKEN*/
		EIGlobal.mensajePorTrace("Entramos a LoginServlet || defaultAction: ",EIGlobal.NivelLog.DEBUG);		
		
		if(!validaTokenCsrf(request)){
			request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"M�todo de Autenticaci�n no aceptado.\",3);" );
			request.setAttribute ( ATTRIBUTE_KEY_FECHA, ObtenFecha () );
			evalTemplate ( IEnlace.LOGIN, request, response );
		}
		/*FINALIZA VALIDACION CSRF TOKEN*/		
		/**
		 * Se agrega bandera para verificar
		 */
		if(!banderaFaseII){
			/***************************************************
			 * Codigo insertado para el control de RSA Fase I ** MDR 21/12/2012
			 ***************************************************/
			final String origenLogin = request.getParameter("origenLogin");
			final boolean validarRSA=Global.VALIDA_RSA;

			request.getSession();
			if ("1".equals(origenLogin)&&validarRSA) {
				String deviceTokenFSOBrowser = "";
				String deviceTokenCookieBrowser = "";

				final String cookieNameDeviceToken = COOKIE_KEY_DEVICE_TOKEN;
				final String cookieNameDeviceTokenFSO = COOKIE_KEY_DEVICE_TOKEN_FSO;
				

				final Cookie[] cookies = request.getCookies();
				if ((cookies == null) || (cookies.length == 0)) {
					EIGlobal.mensajePorTrace("No Cookies found",
							EIGlobal.NivelLog.DEBUG);
				} else {
					EIGlobal.mensajePorTrace("TAMA�O COOKIES: " + cookies.length,EIGlobal.NivelLog.DEBUG);
					for (int i = 0; i < cookies.length; i++) {
						if (cookieNameDeviceToken.equals(cookies[i].getName())) {
							deviceTokenCookieBrowser = cookies[i].getValue();
						}
						if (cookieNameDeviceTokenFSO.equals(cookies[i].getName())) {
							deviceTokenFSOBrowser = cookies[i].getValue();
						}
					}
				}

				final String devicePrinter = request.getParameter("devicePrinter");
				final String httpAcept = request.getHeader("Accept");
				final String httpAceptChars = request.getHeader("Accept-Charset");
				final String httpAceptEncoding = request.getHeader("Accept-Encoding");
				final String httpAceptLanguage = request.getHeader("Accept-Language");
				final String httpReferer = request.getHeader("Referer");
				String ipAddr = request.getHeader("iv-remote-address");
				if (ipAddr == null) {
					ipAddr = request.getRemoteAddr();
				}
				final String httpUserAgent = request.getHeader("User-Agent");
				final String eDescription = "Acceso de Enlace";
				response.setContentType(TEXT_HTML);

				final RSAFI rsa = new RSAFI();

				EIGlobal.mensajePorTrace("devicePrinter: " + devicePrinter,EIGlobal.NivelLog.DEBUG);

				String deviceToken[] = new String[2];

				final String usuario = request.getParameter(PARAMETER_KEY_USUARIO8);

				try {
					deviceToken = rsa.ejecutaAnalyzeRequest(devicePrinter,
							httpAcept, httpAceptChars, httpAceptEncoding,
							httpAceptLanguage, httpReferer, ipAddr, httpUserAgent,
							usuario, eDescription, usuario, "Enlace",
							deviceTokenCookieBrowser, deviceTokenFSOBrowser);
				} catch (ExecutionException e) {
					EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
				} catch (InterruptedException e) {
					EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
				}

				EIGlobal.mensajePorTrace(COOKIE_KEY_DEVICE_TOKEN+deviceToken[0],EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(COOKIE_KEY_DEVICE_TOKEN_FSO+deviceToken[1],EIGlobal.NivelLog.DEBUG);

				final Cookie deviceTokenCookie = new Cookie(COOKIE_KEY_DEVICE_TOKEN,deviceToken[0]);
				final Cookie deviceTokenFSO = new Cookie(COOKIE_KEY_DEVICE_TOKEN_FSO,deviceToken[1]);

				final int SECONDS_PER_YEAR = 60 * 60 * 24 * 90;

				deviceTokenCookie.setMaxAge(SECONDS_PER_YEAR);
				deviceTokenFSO.setMaxAge(SECONDS_PER_YEAR);

				response.addCookie(deviceTokenCookie);
				response.addCookie(deviceTokenFSO);
			}
			/***************************************************
			 * Termina Codigo insertado para el control de RSA Fase I ** MDR
			 * 21/12/2012
			 *
			 *
			 * **************************************************/
		}else{
			/***************************************************
			 * Codigo insertado para el control de RSA Fase I ** MDR 21/12/2012
			 ***************************************************/
			final String origenLogin = request.getParameter("origenLogin");
			String devicePrint = request.getParameter("devicePrinter");
			if (devicePrint != null) {
				request.getSession().setAttribute("valorDeviceP", devicePrint );
			}

			boolean validaRSA = true;
			 String estatusUsuario = "";
			    String deviceTokenFSO = "";
			    String deviceTokenCookie = "";
			if (Global.VALIDA_RSA && "1".equals(origenLogin)) {

				UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
				utilidadesRSA.iniciarConexionRSA();
				
				try {
					if(RSADAO.consultaVersionRSA() == 1) {
						UtilidadesRSA.VERSION_RSA = "RSA7";
					} else {
						UtilidadesRSA.VERSION_RSA = "RSA6";
					}
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace("-->BaseServlet error consulta gfi rsa " + e1.getMessage() ,EIGlobal.NivelLog.ERROR);
				}

				ServiciosAAResponse resp = new ServiciosAAResponse();
				//
				RSABeanAUX rsaBeanAUX = new RSABeanAUX();
				RSABean rsaBean = new RSABean();
				rsaBean = utilidadesRSA.generaBean(request, "", "", request.getParameter(PARAMETER_KEY_USUARIO8));
				rsaBeanAUX.setRsaBean(rsaBean);
				rsaBeanAUX.setValorMetodo(RSAFII.valorMetodo.QUERY);

				// resp = query.getSiteToUserData(rsaBean);

				RSAFII rsa = new RSAFII();

				try {


					resp = (ServiciosAAResponse) rsa.ejecutaMetodosRSA7(rsaBeanAUX);

					if (resp.getIdentificationData() != null && resp.getDeviceResult() != null) {

						EIGlobal.mensajePorTrace("Valor estatusUsuario -> " + resp.getIdentificationData().getUserStatus(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Valor getDeviceData -> " + resp.getDeviceResult().getDeviceData(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Valor deviceTokenFSO -> " + resp.getDeviceResult().getDeviceData().getDeviceTokenFSO(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Valor deviceTokenCookie -> " + resp.getDeviceResult().getDeviceData().getDeviceTokenCookie(), EIGlobal.NivelLog.DEBUG);
						
						estatusUsuario = resp.getIdentificationData().getUserStatus().toString();
						deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
						deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();
					} else {
						EIGlobal.mensajePorTrace("False query", EIGlobal.NivelLog.DEBUG);
						validaRSA = false;
					}
				} catch (ExecutionException e) {
					EIGlobal.mensajePorTrace(
							"No hay conexion con RSAExecutionException",EIGlobal.NivelLog.INFO);
					validaRSA = false;
				} catch (InterruptedException e) {
					EIGlobal.mensajePorTrace(
							"No hay conexion con RSAInterruptedException",EIGlobal.NivelLog.INFO);
					validaRSA = false;
				} catch (ClassCastException e) {
					validaRSA = false;
					EIGlobal.mensajePorTrace(
							"No hay conexion con RSAClassCastException",EIGlobal.NivelLog.INFO);
				}

				if (validaRSA) {

					UtilidadesRSA.crearCookie(response, deviceTokenCookie);
					request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
					EIGlobal.mensajePorTrace("ATTR_SET_FLASH_SO en LoginServlet: " + request.getSession().getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO),
							EIGlobal.NivelLog.DEBUG);

					EIGlobal.mensajePorTrace("Estatus del usuario: " + estatusUsuario, EIGlobal.NivelLog.INFO);

					if ("NOTENROLLED".equals(estatusUsuario) || "UNVERIFIED".equals(estatusUsuario)) {
						accionRSA = validarRSA(request, response, estatusUsuario);
					} else if ("VERIFIED".equals(estatusUsuario)) {

						accionRSA = "NONE";

					}
				}

			}
			/***************************************************
			 * Termina Codigo insertado para el control de RSA Fase I ** MDR
			 * 21/12/2012
			 *
			 *
			 * **************************************************/
		}
		String opcion = request.getParameter("opcion");
		EIGlobal.mensajePorTrace("Opcion: " + opcion, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CONFIRMA Opcion: " + opcion,EIGlobal.NivelLog.DEBUG);
		if (opcion != null && "0".equals(opcion)) {
			/*
			 * <VC proyecto="200710001" autor="JGR" fecha="15/05/2007"
			 * descripci�n="VERIFICAR USUARIO SAM">
			 */
			String usr8 = "";
			String contra = "";
			String templateElegido = "";
		//CAMBIOS MEG TEMPORAL PARA NO ENTRAR POR EL SAM
			/*if (request.getHeader("IV-USER") == null){
				EIGlobal.mensajePorTrace( "Header IV-USER->" + request.getHeader("IV-USER") , EIGlobal.NivelLog.INFO);
				request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Error en comunicaci�n SAM-Enlace\", 3);" );
				evalTemplate ( IEnlace.LOGIN, request, response );
				return;
			}*/
			String origenSAM = request.getParameter("origenSAM");

			if (origenSAM == null){ 	//El origen no es SAM
					request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"M�todo de Autenticaci�n no aceptado.\",3);" );
					request.setAttribute ( ATTRIBUTE_KEY_FECHA, ObtenFecha () );
					evalTemplate ( IEnlace.LOGIN, request, response );
			}else{		//EL origen en SAM, ya se valido la contrasena
				usr8 = request.getParameter(PARAMETER_KEY_USUARIO8);
				templateElegido = request.getParameter("plantilla");
				templateElegido =(templateElegido!=null?templateElegido:"");
				//////Stefanini rsa
				if("ENROLAR".equals(accionRSA)) {
					request.getSession().setAttribute("enrolar","true");
				}
				//////fi Stefanini rsa
				request.getSession().setAttribute("plantillaSAM", templateElegido);
				EIGlobal.mensajePorTrace( "PLANTILLA SAM->" + request.getSession().getAttribute("plantillaSAM"), EIGlobal.NivelLog.INFO);
				//coderror = "SEG_0000@Operaci[s]n Exitosa@";
				//<VC proyecto="200710001" autor="JGR" fecha="01/08/2007" descripci�n="SALTO SUPERNET ENLACE">
				contra = request.getParameter("contrato");
				//</VC>

				request.getSession().setAttribute("acceso", "acceso");
				request.getSession().setAttribute("usuario", usr8);
				request.getSession().setAttribute(PARAMETER_KEY_USUARIO8, usr8);
				request.getSession().setAttribute("contrato", contra);
				request.getSession().setAttribute("plantilla", templateElegido);
				request.getSession().setAttribute("coderror", "SEG_0000@Operaci[s]n Exitosa@");
				
		

			}
			/*</VC F>*/
		}
		else if(opcion != null && "1".equals(opcion)) {
			EIGlobal.mensajePorTrace( "Entra a opcion 1" , EIGlobal.NivelLog.DEBUG);
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			BloqueoUsuarioBO bloq = new BloqueoUsuarioBO();

			if (session == null) {
				boolLogin = false;
				request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Sesion expirada, intente nuevamente.\", 3);" );
				request.setAttribute ( ATTRIBUTE_KEY_FECHA, ObtenFecha () );
				evalTemplate ( IEnlace.LOGIN, request, response );
				sess.invalidate();
				return;
			}

			if (sess.getAttribute("bloqCreaAmbiente") == null&&bloq.isBlockedUser(session.getContractNumber(),session.getUserID8())){
				EIGlobal.mensajePorTrace( "<<<<---USUARIO BLOQUEADO POR INACTIVIDAD--->>>> ", EIGlobal.NivelLog.DEBUG);
				cierraDuplicidad(session.getUserID8());
				request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Su Usuario ha sido bloqueado por inactividad.\",3);" );
				request.setAttribute ( ATTRIBUTE_KEY_FECHA, ObtenFecha () );
				evalTemplate ( IEnlace.LOGIN, request, response );
				sess.invalidate();
				return;
			}

			String usr = session.getUserID8();
			Token tok = session.getToken();
			boolean tokenValido = false;
			Connection conn= null;
			try {
				conn= createiASConn ( Global.DATASOURCE_ORACLE );
				tokenValido = tok.validaToken(conn, request.getParameter("token"));
				conn.close();
			} catch(SQLException e) {
				EIGlobal.mensajePorTrace("LoginServlet - SQLException Al validar token",EIGlobal.NivelLog.ERROR);
			}finally {
				try {
					if(conn!=null){						
						conn.close ();
					}
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace("LoginServlet - SQLException al cerrar conexion en la validacion de token",EIGlobal.NivelLog.ERROR);
				}
			 }

			session.setTokenValidado(tokenValido);
			if(tokenValido) {
				//evalTemplate (templateElegido,request, response);
				limpiaIntentoFallido(tok);
				evalTemplate (IEnlace.HOSTAUTH,request, response);
			}
			else {
				if(tok.getIntentos() >= 4){
					//llamar al WS de bloqueo
					WSBloqueoToken bloqueo = new WSBloqueoToken();
					String[] resultado = bloqueo.bloqueoToken(usr);
					//Realizar envio de notificaci�n
					EmailSender emailSender=new EmailSender();
					EmailDetails emailDetails = new EmailDetails();

					String numContrato = session.getContractNumber();
					String nombreContrato = session.getNombreContrato();

					emailDetails.setNumeroContrato(numContrato);
					emailDetails.setRazonSocial(nombreContrato);

					emailSender.sendNotificacion(request,IEnlace.BLOQUEO_TOKEN_INTENTOS,emailDetails);
					//Guardar pista de auditoria
					ValidaOTP.guardaRegistroBitacora(request,"BTIF Bloqueo de token por intentos fallidos.");
					//Realizar cierre de sesi�n y enviar alerta con mensaje de bloqueo por intentos

					BitaTCTBean beanTCT = new BitaTCTBean ();
					BitaHelper bh =	new BitaHelperImpl(request,session,request.getSession(false));

					beanTCT = bh.llenarBeanTCT(beanTCT);
					beanTCT.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
					beanTCT.setTipoOperacion("BTIF");
					beanTCT.setCodError("BTIF0000");
					beanTCT.setOperador(session.getUserID8());
					beanTCT.setConcepto("Bloqueo de token por intentos fallidos");

					try {
						BitaHandler.getInstance().insertBitaTCT(beanTCT);
					}catch(Exception e) {
						EIGlobal.mensajePorTrace("LoginServlet - Error al insertar en bitacora intentos fallidos",EIGlobal.NivelLog.ERROR);

					}

					bh.incrementaFolioFlujo((String)getFormParameter(request,BitaConstants.FLUJO));
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setIdFlujo("BTIF");
					bt.setNumBit("000000");
					bt.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
					bt.setIdToken(tok.getSerialNumber());

					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}

					try {
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace ("LoginServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace ("LoginServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					}

					intentoFallido(tok);
					request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Token Bloqueado por intentos fallidos, favor de llamar a nuestra S�per l�nea Empresarial para desbloquearlo.\", 3);" );
					request.setAttribute ( ATTRIBUTE_KEY_FECHA, ObtenFecha () );
					int resDup = cierraDuplicidad(session.getUserID8());
					sess.setAttribute("session", session);
					 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServlet.java::token() -> resDup: " + resDup,
							 EIGlobal.NivelLog.DEBUG);
					sess.invalidate();
					evalTemplate(IEnlace.LOGIN, request, response);
					return;
				}
				intentoFallido(tok);
				boolLogin = false;
				//intentoFallido(usr);
				request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Contrase&ntilde;a incorrecta\", 3);" );
				request.setAttribute ( ATTRIBUTE_KEY_FECHA, ObtenFecha () );
				sess.setAttribute("session", session);
				evalTemplate ( IEnlace.TOKEN, request, response );
				return;
			}
			return;

		}
		//String clv5 = "";
		//clv5 = request.getParameter ( "clave" );
		//EIGlobal.mensajePorTrace( "BSPRLOPMGR-023: Confirming.default: Reserved=0 Total=:"+clv5 , EIGlobal.NivelLog.INFO);
		try{
			login( false, request, response );
		} catch(Exception e) {
			response.setContentType(TEXT_HTML);
			evalTemplate( IEnlace.LOGIN, request, response );
		}
	}

	/**
	 * @param req :req
	 * @param resp : resp
	 * @param estatusUsuario : estatusUsuario
	 * @return String
	 */
	@SuppressWarnings("unchecked")
	private String validarRSA(HttpServletRequest req, HttpServletResponse resp, String estatusUsuario) {
		if("NOTENROLLED".equals(estatusUsuario)) {
			Map <String, String> respuestaCreate = new HashMap<String, String>();
			UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
		    utilidadesRSA.iniciarConexionRSA();
			RSABeanAUX  rsaBeanAUX = new RSABeanAUX();
		    RSABean  rsaBean = new RSABean();
		    rsaBean = utilidadesRSA.generaBean(req, "", "", req.getParameter(PARAMETER_KEY_USUARIO8));
		    rsaBeanAUX.setRsaBean(rsaBean);
		    rsaBeanAUX.setValorMetodo(RSAFII.valorMetodo.QUERY);

		    String deviceTokenFSO = "";
		    String deviceTokenCookie = "";

		    RSAFII rsa = new RSAFII();
			rsaBeanAUX = new RSABeanAUX();
			rsaBeanAUX.setRsaBean(rsaBean);
			rsaBeanAUX.setValorMetodo(RSAFII.valorMetodo.CREAR_USUARIO);

			//respuestaCreate = servicioRSA.ejecutaCreateUser(rsaBean);
			try {
				respuestaCreate = (Map<String, String>) rsa.ejecutaMetodosRSA7(rsaBeanAUX);
			} catch (ExecutionException e) {
				EIGlobal.mensajePorTrace("No hay conexion con RSAExecutionException.",EIGlobal.NivelLog.INFO);
			} catch (InterruptedException e) {
				EIGlobal.mensajePorTrace("No hay conexion con RSAInterruptedException",EIGlobal.NivelLog.INFO);
			}

			deviceTokenFSO = respuestaCreate.get(COOKIE_KEY_DEVICE_TOKEN_FSO);
	    	deviceTokenCookie = respuestaCreate.get(COOKIE_KEY_DEVICE_TOKEN);

	    	UtilidadesRSA.crearCookie(resp, deviceTokenCookie);
			req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

		}

		return "ENROLAR";
	}
	
	/**
	 * Validar el token CSRF a nivel de cookie
	 * @param request Peticion
	 * @return Bandera indicando el resultado de un token valido
	 */
	public boolean validaTokenCsrf(HttpServletRequest request) {
		final Cookie[] cookiesCsrf = request.getCookies();
		String enlaceToken = "";
		boolean flag = false;
		if ((cookiesCsrf == null) || (cookiesCsrf.length == 0)) {
			EIGlobal.mensajePorTrace("No CookiesCsrf found",
					EIGlobal.NivelLog.DEBUG);
		} else {
			EIGlobal.mensajePorTrace("size COOKIES: " + cookiesCsrf.length,EIGlobal.NivelLog.DEBUG);
			for (int i = 0; i < cookiesCsrf.length; i++) {
				EIGlobal.mensajePorTrace("CookiesCsrf found ||"+cookiesCsrf[i].getName()+"||"+cookiesCsrf[i].getValue(),
						EIGlobal.NivelLog.DEBUG);
				if ("enlaceToken".equals(cookiesCsrf[i].getName())) {
					enlaceToken = cookiesCsrf[i].getValue();
				}
			}
		}
		EIGlobal.mensajePorTrace("enlaceToken||" + enlaceToken+"||",EIGlobal.NivelLog.DEBUG);
		int logitudToken = enlaceToken != null ? enlaceToken.length() : 0;
		if(logitudToken == 36) {
			EIGlobal.mensajePorTrace("Acceso valido: ",EIGlobal.NivelLog.DEBUG);
			flag = true;
		}else {
			EIGlobal.mensajePorTrace("Acceso de un intruso: ",EIGlobal.NivelLog.DEBUG);
		}
		
		return flag;
	}


}
/*2007.01.1*/