/*Banco Santander Mexicano
  Clase ActCtaCLABE actualización manual de cuentas CLABE
  @Autor:Rafael Rosiles Soto
  @version: 1.0
  fecha de creacion: 14 de Enero de 2004
*/

package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class ActCtaCLABE extends BaseServlet {
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
	//public void doPost( request, response )
		throws ServletException, IOException
	{
		//defaultAction( HttpServletRequest request, HttpServletResponse response );
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		EIGlobal.mensajePorTrace ("*** ActCtaCLABE.defaultAction entrando ***", EIGlobal.NivelLog.INFO);

     	request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta y Actualizaci&oacute;n de cuentas CLABE",
				"Administraci&oacute;n y control &gt; Cuentas &gt; Actualizaci&oacute;n de cuentas CLABE","s37020h", request));
		request.setAttribute("Fecha", ObtenFecha());
		request.setAttribute("Hora", ObtenHora());
		request.setAttribute("ContUser",ObtenContUser(request));

		boolean sesionvalida = SesionValida( request, response );

		// Parametro que se debe agregar a IENLACE para mostrar el numero de cuentas
		// que el usuario desee
		String NumeroPag = IEnlace.REGISTROS_CLABE;
		String ctasClabe = "";
		String Contrato = session.getContractNumber();

		//if(sesionvalida && session.getFacultad(session.FAC_ALTA_CTAS_OTROSBANC) && session.getFacultad(session.FAC_TRANSF_NOREG)) {

			int i;

			for (i=1; i<=Integer.parseInt(NumeroPag); i++) {
				if ((String) request.getParameter( "ctasCLABE"+String.valueOf(i) )!=null) {
					EIGlobal.mensajePorTrace ("ctasCLABE"+String.valueOf(i)+"="+(String) request.getParameter( "ctasCLABE"+String.valueOf(i) ), EIGlobal.NivelLog.INFO);
					ctasClabe += (String) request.getParameter( "ctasCLABE"+String.valueOf(i) ) + "@";
				}
			}

			boolean Servicio = false;

			String m = (String) request.getParameter("j");

			System.out.println("ctasClabe  ===========> " + ctasClabe);
			System.out.println("EL VALOR DEL JJJJJJJJJJJJJJJJJJ " + m);

			Servicio = localFileCtasCLABE(ctasClabe,Contrato);

			// llamar actualizacion de cuentas CLABE
			if (Servicio) {
				String[] vectorCuentas = actualizaCLABE("A", Contrato +".iclabe", request, response);

				if (!vectorCuentas[1].equals("ACCL0000")) {
					request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
					evalTemplate( IEnlace.ERROR_TMPL, request, response );
				}
				else {

					StringBuffer strFecha = new StringBuffer("");

	      			Date Hoy = new Date();

	      			GregorianCalendar Cal = new GregorianCalendar();

	      			Cal.setTime(Hoy);

	              	strFecha.append(Cal.get(Calendar.DATE));

	        		strFecha.append("/");

	      	        strFecha.append((Cal.get(Calendar.MONTH)+1));

	      			strFecha.append("/");

	        		strFecha.append(Cal.get(Calendar.YEAR));

					String tablaResultados="<table border=0 cellspacing=2 cellpadding=3 bgcolor=#FFFFFF>"+
						    "<tr><td align='center' class=tittabdat nowrap>Operaci&oacute;n</td>"+
						    "<td align='center' class=tittabdat nowrap>Fecha</td>"+
						    "<td align='center' class=tittabdat nowrap>Total de cuentas</td>"+
					   	    "<td align='center' class=tittabdat nowrap>Cuentas actualizadas</td></tr>"+
				            "<tr><td align='center' class='textabdatcla' nowrap>&nbsp;Actualizaci&oacute;n de cuentas CLABE</td>"+
				            "<td align='center' class='textabdatcla' nowrap>"+strFecha.toString()+
				            "&nbsp;</td><td align='center' class='textabdatcla' nowrap>"+vectorCuentas[2]+
							"&nbsp;</td><td align='center' class='textabdatcla' nowrap>"+vectorCuentas[3]+
							"&nbsp;</td></tr></table><br><br>"+
							"<table align='center'><tr><td>"+
							//"<td><a href='javascript:Enviar();'><input type='image' border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25280.gif' width='83' height='22' alt='Aceptar'></a></td></tr>"+
							//"<td><a href='javascript:return false;' onClick='Enviar();'><input type='image' border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25280.gif' width='83' height='22' alt='Aceptar'></a></td></tr>"+
							"<a href='ConActCLABE'><input type='image' border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25320.gif' width='83' height='22' alt='Regresar'></a>"+
							"<a href = 'javascript:scrImpresion();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a>"+
							"</td></tr></table>";
					//System.out.println("LA TABLA ================================================================>");
					//System.out.println(tablaResultados);

					request.setAttribute("newMenu", session.getFuncionesDeMenu());
					request.setAttribute("Resultados", tablaResultados );
					evalTemplate( "/jsp/ActCtaCLABE.jsp", request, response );
				}
			}
			else {
				request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
				evalTemplate( IEnlace.ERROR_TMPL, request, response );
			}
		/*}
		else {
			request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}*/

    }


    public boolean localFileCtasCLABE (String buffer, String contrato ) {

	     int i = 1;

	     boolean result = false;

	     String datos[] =null;

	     String filename = contrato + ".iclabe";

	     try {

	     	EIGlobal.mensajePorTrace ("*** ActCtaCLABE.localFileCtasCLABE entrando ***", EIGlobal.NivelLog.INFO);

	     	EIGlobal.mensajePorTrace ("*** ActCtaCLABE.localFileCtasCLABE Trama cuentas CLABE > " + buffer, EIGlobal.NivelLog.INFO);
	     	EIGlobal.mensajePorTrace ("*** ActCtaCLABE.localFileCtasCLABE Contrato > " + contrato, EIGlobal.NivelLog.INFO);

	     	EIGlobal.mensajePorTrace ("*** ActCtaCLABE.localFileCtasCLABE Archivo de salida de cuentas CLABE >" + filename, EIGlobal.NivelLog.INFO);

	     	datos = desentramaString ( buffer, '@' );

		    FileWriter writer = new FileWriter(IEnlace.REMOTO_TMP_DIR + "/" + filename );

		    BufferedWriter buf_writer = new BufferedWriter(writer);

		    while ( i <= Integer.parseInt(datos[0]) ) {

	     		buf_writer.write(datos[i]);

		        buf_writer.newLine();

	     		i++;
	     	    }

		    buf_writer.close();

		    EIGlobal.mensajePorTrace ("*** ActCtaCLABE.localFileCtasCLABE Archivo de cuentas CLABE creado exitosamente >" + filename, EIGlobal.NivelLog.INFO);

		    //copiar archivo local a host remoto

		    EIGlobal.mensajePorTrace("*** ActCtaCLABE.localFileCtasCLABE  inicio de copia de archivo local  ...", EIGlobal.NivelLog.DEBUG);

			ArchivoRemoto archivoCLABE = new ArchivoRemoto();

			// Copia de archivo local a ruta remota
			if(!archivoCLABE.copiaLocalARemoto(filename)) {
				EIGlobal.mensajePorTrace("*** ActCtaCLABE.localFileCtasCLABE  no se pudo copiar archivo local a remoto :" + filename, EIGlobal.NivelLog.INFO);
				result = false;
			}
			else {
			    EIGlobal.mensajePorTrace("*** ActCtaCLABE.localFileCtasCLABE  archivo local copiado exitosamente:" + filename, EIGlobal.NivelLog.INFO);
			    result = true;
			}

			EIGlobal.mensajePorTrace("*** ActCtaCLABE.localFileCtasCLABE  fin de copia de archivo local ...", EIGlobal.NivelLog.DEBUG);



	     } catch ( Exception e ) {

		     	EIGlobal.mensajePorTrace ("*** ActCtaCLABE.localFileCtasCLABE Error: > " + e.toString (), EIGlobal.NivelLog.INFO);

		     	e.printStackTrace ();

				return result;

		 }
	     return result;
     }


     public int cuentaseparadorCLABE (String linea, String Separador) {

		 int sep=0;
		 int pos=0;
		 int longi=linea.length ();

		 String linea_aux="";

		 while ((pos>-1)&&(pos<longi)) {

		     pos=linea.indexOf (Separador);

		     if (pos>=0) {

				 sep++;
				 linea=linea.substring (linea.indexOf (Separador)+1,linea.length ());

		     }
		 }
		 return sep;
     }

}