/**
 * Clase OpcionesNomina : exhibe la plantilla necesaria para dar mantenimiento
 * al archivo, asimismo, envio y recuperacion de archivo
 * responsable: Ruben Fragoso V.
 * descripcion: en el caso de ejecutar la opcion altas, se exhibira
 * una plantilla para capturar un nuevo registro, en el caso de bajas y
 * modificaciones, muestra al usuario los campos que integran al registro, estas
 * operaciones (altas, bajas y cambios) usan la misma plantilla. Cuando el
 * usuario ha seleccionado la opcion envio, prepara los datos que han de ser
 * enviados al servicio IN04,IN05 e IN06 de WEB_RED. <br> Cuanto el usuario
 * selecciona recuperacion, el servicio es IN10, tambien a traves de WEB_RED
 *
 * @version 1.2 Refactor y modificaciones para Optiizaciones Nomina RMM.
 */


package mx.altec.enlace.servlets;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.text.*;

import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.beans.NominaArchBean;
import mx.altec.enlace.beans.SolPDFBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.ArchivosNomina;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.DescargaEstadoCuentaBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.NominaArchBO;
import mx.altec.enlace.bo.NominaBO;
import mx.altec.enlace.bo.NominaPagosBO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.bo.FormatoMoneda;
import static mx.altec.enlace.utilerias.NominaConstantes.*;

public class OpcionesNomina extends BaseServlet {

	private static final long serialVersionUID = 1L;
	/**
	 * Constante para la literal "cancelar- arch_salida_consulta:>>"
	 */
	private static final String CANCELAR_ARCH = "cancelar- arch_salida_consulta:>>";
	/**
	 * Constante para la literal "<table align=center border=0 cellspacing=0 cellpadding=0>"
	 */
	private static final String ABRE_TABLE = "<table align=center border=0 cellspacing=0 cellpadding=0>";
	/**
	 * Constante para la literal "<tr><td align=center colspan=7> <a style=\"cursor: pointer;\" >Exporta en TXT <input id=\"tipoExportacion\" type=\"radio\" value ='txt' checked  name=\"tipoExportacion\"></a>\n</td></tr>"
	 */
	private static final String EXPORTAR_TXT = "<tr><td align=center colspan=7 class=\"tabmovtex\"> <a style=\"cursor: pointer;\" >Exporta en TXT <input id=\"tipoExportacion\" type=\"radio\" value ='txt' checked  name=\"tipoExportacion\"></a>\n</td></tr>";
	/**
	 * Constante para la literal "<tr><td align=center colspan=7> <a style=\"cursor: pointer;\" >Exportar en XLS <input id=\"tipoExportacion\" type=\"radio\" value ='csv' name=\"tipoExportacion\"></a>\n<br></td></tr>"
	 */
	private static final String EXPORTAR_XLS = "<tr><td align=center colspan=7 class=\"tabmovtex\"> <a style=\"cursor: pointer;\" >Exportar en XLS <input id=\"tipoExportacion\" type=\"radio\" value ='csv' name=\"tipoExportacion\"></a>\n<br></td></tr>";
	/**
	 * Constante para la literal "<tr>"
	 */
	private static final String ABRE_TR = "<tr>";
	/** Constate para HR_P */
	private static final String HR_P = "HR_P";
	/** Constante para cadena null */
	private static final String CAD_NULL = "null";
	/** Constaten para solicitar informe */
	private static final String SOL_INFORME = "<td><a href=\"javascript:js_solicitarPDF();\"><img src=\"/gifs/EnlaceMig/solInforme.gif\" alt=\"Solicitar Informe PDF\" border=\"0\" height=\"22\" width=\"125\"></a></td>";
	/** Cosntate para encabezado de consulta de nomina */
	private static final String CON_NOM_PROG = "Servicios &gt; N&oacute;mina  &gt; Consultas &gt; Pagos Programados";
	/** Cosntate para encabezado de consulta de nomina */
	private static final String CONS_NOMINA = "Consulta de N&oacute;mina";
	/** Constante para estilo de encabezado de nomina*/
	private static final String CONST_CANH = "s25800canh";
	/**
	 * Horario premier
	 */
	final String HRPREMIER = "HorarioPremier";



	String textLog ="";
	private String textoLog(String funcion){
		return textLog = OpcionesNomina.class.getName() + "." + funcion + "::";
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void defaultAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		textoLog("defaultAction");
		boolean sesionvalida = SesionValida(request, response);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		byte strDescFav[] = null;
		String dscFav = null;
		boolean solicitarOTP = true;

		if (sesionvalida) {
			// se verifica el llamado del servicio para saber si se trata de
			// validaci�n de duplicados o ejecuci�n de la transacci�n
			if (request.getSession().getAttribute("Ejec_Proc_Val_Pago") != null) {
				EIGlobal.mensajePorTrace(textLog + "Se va a efectuar la transaccion", EIGlobal.NivelLog.DEBUG);
				solicitarOTP = true;
			}
			// Inicia validacion OTP
			String valida = "";
			EIGlobal.mensajePorTrace(textLog + "request.getAttribute(validaNull)" +request.getAttribute("validaNull"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "request.getAttribute(validaNull)" +request.getAttribute("validaNull"), EIGlobal.NivelLog.DEBUG);
			/**
			 * Inicio PYME 2015 Unificacion Token
			 */
			if( (request.getAttribute("validaNull") != null && "si".equals(request.getAttribute("validaNull"))) &&
				(request.getAttribute("forzar") != null && "si".equals(request.getAttribute("forzar"))) ){
				valida = null;
				EIGlobal.mensajePorTrace( "------------getParameter folio---->"+ request.getAttribute("folio"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "------------getParameter txtFav---->"+ request.getAttribute("txtFav"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "------------getParameter strCheck---->"+ request.getAttribute("strCheck"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "------------getParameter chkBoxFav---->"+ request.getAttribute("chkBoxFav"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "------------getParameter horario_seleccionado---->"+ request.getAttribute("horario_seleccionado"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "------------getParameter HorarioPremier---->"+ request.getAttribute("HorarioPremier"), EIGlobal.NivelLog.DEBUG);
				if( request.getAttribute("horario_seleccionado") != null && !"".equals(request.getAttribute("horario_seleccionado")) ){
					request.setAttribute("cargarValores", "si");
					request.setAttribute("infoImportados", null);
				}
			} else {
				valida = request.getParameter("valida");
			}
			/**
			 * Fin PYME 2015 Unificacion Token
			 */
			EIGlobal.mensajePorTrace(textLog + "request.getParameter(valida)["+request.getParameter("valida")+"]", EIGlobal.NivelLog.DEBUG);
			if (validaPeticion(request, response, session, sess, valida)) {
				EIGlobal.mensajePorTrace(textLog + "Entro a validar Peticion", EIGlobal.NivelLog.DEBUG);
			}
			// Termina validacion OTP
			else {
				session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
				EIGlobal.mensajePorTrace(textLog, EIGlobal.NivelLog.DEBUG);
				request.setAttribute("Fecha", ObtenFecha());
				request.setAttribute("ContUser", ObtenContUser(request));
				String arch_exportar = "";
				String nombre_arch_salida = "";
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				StringBuffer datesrvr = new StringBuffer("");
				datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
				datesrvr.append(ObtenAnio());
				datesrvr.append("\">");
				datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
				datesrvr.append(ObtenMes());
				datesrvr.append("\">");
				datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
				datesrvr.append(ObtenDia());
				datesrvr.append("\">");
				request.setAttribute("Fechas", ObtenFecha(true));
				request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");
				request.setAttribute("botLimpiar", "");
				/**
				 * Se comenta linea para que obtenga valores como atributos
				 * ya que asi fueron asignados
				 * PYME 2015 UNIFICACION DE TOKEN
				 * String strOperacion = (String) getFormParameter(request, OPERACIONX);
				 */
				String strOperacion = "";
				if ( request.getAttribute("operacion") == null ) {
					strOperacion = (String) getFormParameter(request, "operacion");
				} else {
					strOperacion = request.getAttribute("operacion").toString();
				}
				if (strOperacion == null || ("").equals(strOperacion)) {
					strOperacion = "";
				}

				if (strOperacion == null || "".equals(strOperacion))
					strOperacion = "";
	            String strRegistro = (String) getFormParameter(request, "registro");
				if (strRegistro == null || "".equals(strRegistro))
					strRegistro = "";
				request.setAttribute("operacion", strOperacion);
				/**
				 * Se comenta linEa para que obtenga valores como atributos
				 * ya que asi fueron asignados
				 * PYME 2015 UNIFICACION DE TOKEN
				 * String tipoArchivotmp = getFormParameter(request, TIPOARCHIVOX);
				 */
				String tipoArchivotmp = "";
				if ( request.getAttribute("tipoArchivo") == null ) {
					tipoArchivotmp = (String) getFormParameter(request, "tipoArchivo");
				} else {
					tipoArchivotmp = request.getAttribute("tipoArchivo").toString();
				}
				EIGlobal.mensajePorTrace(textLog + "tipoArchivo [" + tipoArchivotmp + "]", EIGlobal.NivelLog.DEBUG);
				if (tipoArchivotmp == null || "".equals(tipoArchivotmp))
					tipoArchivotmp = "";
				if ("regresarPagos".equals(strOperacion)) {// regresar� a la pagina inicial
					inicioPagos(request, response);
				} else if ("descarga".equals(strOperacion)) {// Para descargar el archivo. Se envia desde el application
					descargaArchivos(request, response);
				} else if ("regresar".equals(strOperacion)) {// regresar� a la pagina inicial
					inicioConsulta(request, response);
				} else if ("envio".equals(strOperacion)) {
					EIGlobal.mensajePorTrace(textLog + "EnvioDeArchivo: Verificando datos en sesion", EIGlobal.NivelLog.DEBUG);
					String Concepto = "";
					String archivoConcepto = "";
					if (sess.getAttribute("conceptoEnArchivo") != null) {
						EIGlobal.mensajePorTrace(textLog + "EnvioDeArchivo: Si existe en sesion! Se lee el parametro concepto", EIGlobal.NivelLog.DEBUG);
						Concepto = (String) getFormParameter(request, "Concepto");
						archivoConcepto = (String) sess.getAttribute("nombreArchivo");
						EIGlobal.mensajePorTrace(textLog + "EnvioDeArchivo: Concepto: " + Concepto, EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textLog + "EnvioDeArchivo: archivoConcepto: " + archivoConcepto, EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textLog + "EnvioDeArchivo: concepto_seleccionado OVI: " + Concepto, EIGlobal.NivelLog.DEBUG);
						if(null != Concepto && !"null".equals(Concepto))
						{
							if (actualizaConceptoEnArchivo(archivoConcepto, Concepto)) {
								EIGlobal.mensajePorTrace(textLog + "EnvioDeArchivo: Se actualizo el archivo", EIGlobal.NivelLog.DEBUG);
								sess.removeAttribute("conceptoEnArchivo");
							} else {
								despliegaPaginaError(
									IEnlace.MSG_PAG_NO_DISP,
									CONS_NOMINA,
									"Servicios &gt; Env&iacute;o de Archivos de N&oacute;mina",
									request, response);
								return;
							}
						}
					} else
						EIGlobal.mensajePorTrace(textLog + "EnvioDeArchivo: Los registros en archivo contienen el concepto", EIGlobal.NivelLog.DEBUG);

					String validaChallenge = request.getAttribute("challngeExito") != null
						? request.getAttribute("challngeExito").toString() : "";
					EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);
					if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
						EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
						guardaParametrosEnSession(request);
						/**
						 * PYME 2015 Unificacion Token
						 */
						request.setAttribute("vieneDe", "/enlaceMig/OpcionesNomina");
						validacionesRSA(request, response);
						return;
					}
					EIGlobal.mensajePorTrace(textLog + "Antes de Verificacion de Token, solicitarOTP = " + solicitarOTP, EIGlobal.NivelLog.DEBUG);
					// interrumpe la transaccion para invocar la validaci�n
					boolean valBitacora = true;
					EIGlobal.mensajePorTrace(textLog + "valida" + valida, EIGlobal.NivelLog.DEBUG);
					if (valida == null) {
						EIGlobal.mensajePorTrace(textLog + "Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP", EIGlobal.NivelLog.DEBUG);

						boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);

						if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal && solicitarOTP) {
							EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
							guardaParametrosEnSession(request);

							EIGlobal.mensajePorTrace("No de registros importados ->" + session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
							if (session.getNominaNumberOfRecords() != null) {
								sess.setAttribute("noRegistrosNomina", session.getNominaNumberOfRecords().trim());
							}

							request.getSession().removeAttribute("mensajeSession");
							ValidaOTP.mensajeOTP(request, "PagoNomina");
							/**
							 * Se comenta linea para la unifiacion del token
							 * ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
							 * PYME 2015 Unificacion Token
							 * Se agrega Atributo (mostrarToken)para mostrar u ocultar el token
							 */
//							ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
							EIGlobal.mensajePorTrace("2 === PYME", EIGlobal.NivelLog.INFO);
							if (request.getParameter("TIPO_TOKEN") != null
									&& "tokenOriginal".equals(request
											.getParameter("TIPO_TOKEN"))) {
								//Aqui entra cuando viene de duplicados
								ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
							} else {
								request.setAttribute("mostrarToken", "true");
								request.setAttribute("vieneDe", "OpcionesNomina");
								ValidaOTP.validaOTP(request,response, "/jsp/MantenimientoNomina.jsp");
							}
						} else {
							valida = "1";
							valBitacora = false;
						}
					}
					// retoma el flujo
					if (valida != null && "1".equals(valida)) {
						boolean banderaTransaccion = true;

						// PARA VALIDAR Y DESPU�S CARGAR EL ARCHIVO
						if (request.getSession().getAttribute( "Ejec_Proc_Val_Pago") != null) {
							EIGlobal.mensajePorTrace(textLog + "Es recarga de procedimiento por VALIDACION.", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = true;


							if (valBitacora)
							{
								try{
									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									HttpSession sessionBit = request.getSession(false);
									String claveOperacion = BitaConstants.CTK_PAGO_NOMINA;
									String concepto = BitaConstants.CTK_CONCEPTO_PAGO_NOMINA;
									String token = "0";
									if (request.getParameter("token") != null && !"".equals(request.getParameter("token")));
									{token = request.getParameter("token");}
									double importeDouble = 0;
									String cuenta = "0";

									if (session.getCuentaCargo() != null && !"".equals(session.getCuentaCargo()))
									{cuenta = session.getCuentaCargo();}

									if (sessionBit.getAttribute("sumTmpSession") != null && ! "".equals(sessionBit.getAttribute("sumTmpSession")))
									{String importestr = sessionBit.getAttribute("sumTmpSession").toString();
									 importeDouble = Double.valueOf(importestr).doubleValue();}

									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

									@SuppressWarnings("unused")
									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
								} catch(Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							}
						} else if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null
								&& verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses", false)) {
							EIGlobal.mensajePorTrace(textLog + "El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = false;
						} else if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null) {
							EIGlobal.mensajePorTrace(textLog + "Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
							request.getSession().removeAttribute("Recarga_Ses_Multiples");
							banderaTransaccion = false;
						} else if (verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses", false)) {
							EIGlobal.mensajePorTrace(textLog + " " +  Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses",EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog + "El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = false;
						} else if (request.getSession().getAttribute("Recarga_Ses_Multiples") == null
								&& !verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses", false)) {
							EIGlobal.mensajePorTrace(textLog + "Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = true;
						}

						if (request.getSession().getAttribute("Ejec_Proc_Val_Pago") != null) {
							request.getSession().removeAttribute("Ejec_Proc_Val_Pago");
							EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago REMOVIDO.", EIGlobal.NivelLog.DEBUG);
						}

						EIGlobal.mensajePorTrace(textLog + "banderaTransaccion:" + banderaTransaccion, EIGlobal.NivelLog.DEBUG);
						/* Termina construccion variables control recarga sesion*/

						if (banderaTransaccion) {
							// Se crea el archivo para control de recarga sesion, y puesta de var. Recarga
							EI_Exportar arcTmp = new EI_Exportar(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses");
							if (arcTmp.creaArchivo())
								EIGlobal.mensajePorTrace(textLog + "Archivo .ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);

							request.getSession().setAttribute("Recarga_Ses_Multiples", "TRUE");
							EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago Creado.", EIGlobal.NivelLog.DEBUG);

							// Se modificar� para enviar en un solo servicio el
							// archivo, como lo envia la nomina interbancaria
							String nombreArchivo = (String) sess.getAttribute("nombreArchivo");
							String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
							/**
							 * PYME 2015 Unificacion Token
							 */
							EIGlobal.mensajePorTrace(textLog + "getFormParameter(request,statusDuplicado) ["+ getFormParameter(request,"statusDuplicado") + "]", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog + "request.getAttribute(statusDuplicado) ["+ request.getAttribute("statusDuplicado") + "]", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog + "request.getAttribute(statusDuplicado) ["+ sess.getAttribute("statusDuplicado")  + "]", EIGlobal.NivelLog.DEBUG);
							String digit = getFormParameter(request,
									"statusDuplicado") == null ? request
									.getAttribute("statusDuplicado").toString()
									: getFormParameter(request,
											"statusDuplicado");
							String digit_d = getFormParameter(request,
									"statushrc") == null ? request
									.getAttribute("statushrc").toString()
									: getFormParameter(request, "statushrc");

							EIGlobal.mensajePorTrace(textLog + "nueva_ruta ["+ nueva_ruta + "]", EIGlobal.NivelLog.DEBUG);
							nombreArchivo = nombreArchivo + "/";

							EIGlobal.mensajePorTrace( "***OpcionesNomina: Nombre del archivo1: " + nombreArchivo , EIGlobal.NivelLog.DEBUG);

							nombreArchivo = nombreArchivo.substring(posCar(nombreArchivo, '/', 4) + 1, posCar(nombreArchivo, '/', 5));

							EIGlobal.mensajePorTrace( "***OpcionesNomina: Nombre del archivo2: " + nombreArchivo , EIGlobal.NivelLog.DEBUG);

							String rutaArch = nueva_ruta + "/" + nombreArchivo;
							EIGlobal.mensajePorTrace(textLog + "nombreArchivo dentro de envio" + nombreArchivo, EIGlobal.NivelLog.DEBUG);
							try {
								String tipoCopia = "3";
								ArchivoRemoto archR = new ArchivoRemoto();
								if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
									EIGlobal.mensajePorTrace(textLog + "No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
							} catch (Exception ioeSua) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
							}
							/**
							 * Se comenta linea para que obtenga valores como atributos
							 * ya que asi fueron asignados
							 * PYME 2015 UNIFICACION DE TOKEN
							 * String fechaAplicacion = (String) getFormParameter(request, ENCFECHAPLIX);//encFechApli
							 */
							String fechaAplicacion = "";//encFechApli
							if ( request.getAttribute("encFechApli") == null ) {
								fechaAplicacion = (String) getFormParameter(request, "encFechApli");
							} else {
								fechaAplicacion = request.getAttribute("encFechApli").toString();
							}
							request.setAttribute("encFechApli", fechaAplicacion);
							EIGlobal.mensajePorTrace(textLog + "fechaAplicacion " + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
							if (fechaAplicacion != null)
								sess.setAttribute("NOMIencFechApli", fechaAplicacion);
							if (fechaAplicacion != null) {
								EIGlobal.mensajePorTrace(textLog + "fechaAplicacion dentro de envio LENGTH" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
								fechaAplicacion = fechaAplicacion.trim();
								fechaAplicacion = fechaAplicacion.substring(2, 4) + fechaAplicacion.substring(0, 2) + fechaAplicacion.substring(4, 8);
							} else {
								EIGlobal.mensajePorTrace(textLog + "fechaAplicacion dentro de envio" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
								fechaAplicacion = (String) sess.getAttribute("NOMIencFechApli");
								fechaAplicacion = fechaAplicacion.trim();
								fechaAplicacion = fechaAplicacion.substring(2,4) + fechaAplicacion.substring(0, 2) + fechaAplicacion.substring(4, 8);
							}
							java.util.Date dt = new java.util.Date();
							String HorarioDisp = "";
							String HorarioSele = "";
							/*
							 * Se cambia horario ya que despues de las 16:30 no
							 * se puede enviar nada de informacion.
							 */
							EIGlobal.mensajePorTrace(textLog + "Validacion de fecha", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog + "Fecha del dia de hoy (true) " + ObtenFecha(true), EIGlobal.NivelLog.DEBUG);
							String fecha_hoy = ObtenFecha(true);
							String dd_hoy = fecha_hoy.substring(0, 2);
							String mm_hoy = fecha_hoy.substring(3, 5);
							String aa_hoy = fecha_hoy.substring(6, 10);
							EIGlobal.mensajePorTrace(textLog + "Parametros " + "dd_hoy=" + dd_hoy + ", mm_hoy=" + mm_hoy + ", aa_hoy=" + aa_hoy, EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog + "fecha de hoy (false)" + ObtenFecha(false), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog + "Fecha Aplicacion fechaAplicacion.substring(0,2)=" + fechaAplicacion.substring(0, 2) + ", fechaAplicacion.substring(2,4)=" + fechaAplicacion.substring(2, 4) + ", fechaAplicacion.substring(4,8)=" + fechaAplicacion.substring(4, 8), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog +  "digit >>" + digit + "<<", EIGlobal.NivelLog.INFO);
							/**
							 * PYME UNIFICACION DE TOKEN
							 * para que no muestre los el numero de registros importados
							 */
							request.setAttribute("infoImportados", null);
							if ((dd_hoy.equals(fechaAplicacion.substring(0, 2))
									&& mm_hoy.equals(fechaAplicacion.substring(2, 4))
									&& aa_hoy.equals(fechaAplicacion.substring(4, 8)))
									&& ((dt.getHours() == 16
									&& dt.getMinutes() >= 30)
									|| dt.getHours() >= 17)) {
								EIGlobal.mensajePorTrace(textLog + "Horario Premier >>" + HorarioSele + "<<", EIGlobal.NivelLog.INFO);
								request.setAttribute("folio", (String) getFormParameter(request, "folio"));
								request.setAttribute("newMenu", session.getFuncionesDeMenu());
								request.setAttribute("MenuPrincipal", session.getStrMenu());
								request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
								String infoUser = "HORA LIMITE PARA APLICAR PAGOS HOY ES 16:30 HRS.";
								String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
								infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
								request.setAttribute("infoUser", infoUser);
								if (sess.getAttribute("facultadesN") != null)
									request.setAttribute("facultades", sess.getAttribute("facultadesN"));
								request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
								sess.setAttribute("archDuplicado", "si");
								request.setAttribute("digit", "0");
								evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
								return;
							}
							EIGlobal.mensajePorTrace(textLog +  "digit 2 >> " + digit + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textLog + "fechaAplicacion FINAL" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
							String contrato = session.getContractNumber();
							String empleado = session.getUserID8();
							String perfil = session.getUserProfile();
							String cuenta = session.getCuentaCargo();
							String importe = session.getImpTotalNom().trim();
							String importePunto = agregarPunto(importe);
							String totalRegNom = session.getTotalRegsNom().trim();
							String folio = "";
							ServicioTux tuxGlobal = new ServicioTux();
							String tramaEntrada = "";
							String horarioPremier = "";
							String FacultadPremier = "";
							FacultadPremier = (String) sess.getAttribute("facultadesN");
							EIGlobal.mensajePorTrace(textLog + "Facultad Premier CCBFP " + FacultadPremier, EIGlobal.NivelLog.INFO);
							if (FacultadPremier.indexOf("PremierVerdadero") != -1) {
		                        horarioPremier = getFormParameter(request, "horario_seleccionado");
								EIGlobal.mensajePorTrace(textLog + "CCBHorarioPremierMod " + horarioPremier, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + "horarioPremier  Modificado >" + horarioPremier.trim() + "<", EIGlobal.NivelLog.DEBUG);
		                        request.setAttribute("horario_seleccionado", horarioPremier);
		                        request.setAttribute("HorarioPremier", horarioPremier);
								if (horarioPremier == null)
									horarioPremier = "17:00";
							} else {
								horarioPremier = "17:00";
							}
		                    sess.setAttribute(HR_P, horarioPremier);
							EIGlobal.mensajePorTrace(textLog + "BOX 1", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textLog + "sess.getAttribute(HR_P) >>" + sess.getAttribute(HR_P) + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textLog + "digit 3 >>" + digit_d + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textLog + "sess.getAttribute(archDuplicado)>>" + sess.getAttribute("archDuplicado") + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textLog + "BOX 1", EIGlobal.NivelLog.INFO);
							if (digit.compareTo("0") == 0) {
								horarioPremier = (String) sess.getAttribute(HR_P);
								EIGlobal.mensajePorTrace(textLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.INFO);
								digit = "0";
							}

							// La primera vez invoca servicio NOMI_VAL_PAGO para validar archivo.
							String servicio = "NOVP";
							// Consulta variable global para saber si debe seguir validando el archivo.
							String validar = (String) sess.getAttribute("VALIDAR");
							// Se asigna el folio capturado.
							folio = (String) sess.getAttribute("FOLIO");
							// Se almacena el folio para no perderlo.
							if (folio == null || "".equals(folio)) {
								/**
								 * PYME 2015 Unificacion Token
								 */
		                        sess.setAttribute("FOLIO", request.getAttribute("folio") == null ? getFormParameter(request, "folio") : request.getAttribute("folio").toString() );
								folio = (String) sess.getAttribute("FOLIO");
								EIGlobal.mensajePorTrace(textLog + "El folio esta vacio", EIGlobal.NivelLog.DEBUG);
							}
							EIGlobal.mensajePorTrace(textLog + "Folio [" + folio + "]", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textLog + "Validar [" + validar + "]", EIGlobal.NivelLog.DEBUG);

							// Archivo enviado solo se puede procesar en el horario 17:00
							String HorarioNormal = (String) sess .getAttribute("HorarioNormal");
							if (HorarioNormal == null)
								HorarioNormal = "";
							if ("1".equals(HorarioNormal)) {
								if (digit_d.compareTo("0") == 0) {
									horarioPremier = "17:00";
									EIGlobal.mensajePorTrace(textLog + "horarioPremier 17:00 >>" + horarioPremier + "<<", EIGlobal.NivelLog.INFO);
									digit = "0";
								}
							}

							/*
							 * Si ya se pasaron todas las validaciones del
							 * archivo, invocar al servicio NOMI_PAGO para
							 * procesar el archivo.
							 */
							if (validar == null)
								validar = "";
							if (validar.compareTo("N") == 0) {
								servicio = "IN04";
							}


							/**
							 * eliminas la posibilidad de que el valor del contrato viaje nulo al back-end UNIX
							 * si es null o esta vacia o tiene la palabra null, nuevamente solicita el contrato de sesi�n
							 */
							if(contrato == null || "".equals(contrato) || CAD_NULL.equals(contrato)){
								contrato = session.getContractNumber();
							}
							//CSA ---------------- CANDADO PARA PREVENIR ERROR DE CUENTA NULL
							//cuenta = null;
							if (cuenta == null || "".equals(cuenta) || "NULL".equals(cuenta) || CAD_NULL.equals(cuenta) || "".equals(cuenta)){
							EIGlobal.mensajePorTrace(textLog + "----- CSA ------ null en la cuenta cargo", EIGlobal.NivelLog.INFO);
							//despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,
							//		"Consulta de N&oacute;mina",
							//		"Servicios &gt; Consulta de N&oacute;mina", request,
							//		response);
							//return;

							request.setAttribute("folio", (String) getFormParameter(request, "folio"));
							request.setAttribute("newMenu", session.getFuncionesDeMenu());
							request.setAttribute("MenuPrincipal", session.getStrMenu());
							request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
							String infoUser = "Error al enviar el archivo, intente mas tarde.";

							String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
							infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
							request.setAttribute("infoUser", infoUser);
							if (sess.getAttribute("facultadesN") != null)
								request.setAttribute("facultades", sess.getAttribute("facultadesN"));
							request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
							sess.setAttribute("archDuplicado", "si");
							request.setAttribute("digit", "0");
							evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
							return;

							}
														//----------- CSA ---------------------------
							tramaEntrada = "1EWEB|" + empleado + "|" + servicio
								+ "|" + contrato + "|" + empleado + "|" + perfil
								+ "|" + contrato + "@" + fechaAplicacion + "@"
								+ totalRegNom + "@" + importePunto + "@"
								+ cuenta + "@" + rutaArch + "@" + digit + "@"
								+ horarioPremier + "@|";

							if ("".equals(folio))
								tramaEntrada += " | | |";
							else
								tramaEntrada += cuenta + "|" + importePunto
									+ "|" + folio + "|";

							EIGlobal.mensajePorTrace(textLog + "Trama que se enviara >>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);
							String tramaSalida = "";


							String folioArchivo = "";
							if ("IN04".equals(servicio)) {

								LYMValidador val = new LYMValidador();
			 					String resLYM = val.validaLyM(tramaEntrada);
			 					val.cerrar();//CSA
								if(!"ALYM0000".equals(resLYM)) {

									request.setAttribute("folio", (String) getFormParameter(request, "folio"));
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
									String infoUser = resLYM;
									String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
									infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
									request.setAttribute("infoUser", infoUser);
									if (sess.getAttribute("facultadesN") != null)
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));
									request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
									sess.setAttribute("archDuplicado", "si");
									request.setAttribute("digit", "0");
									evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
									return;

								}


								// Servicio para folio -------------------------
								folioArchivo = generaFolio(session.getUserID8(), session.getUserProfile(), contrato, servicio, totalRegNom);
								session.setFolioArchivo(folioArchivo);
								request.setAttribute("folioArchivo", folioArchivo);
								EIGlobal.mensajePorTrace(textLog + "folioArchivo >>" + folioArchivo + "<<", EIGlobal.NivelLog.INFO);
								// Env�o de Datos ------------------------------
								String mensajeErrorEnviar = envioDatos(session.getUserID8(), session.getUserProfile(), contrato, folioArchivo, servicio, rutaArch, tramaEntrada);
								EIGlobal.mensajePorTrace(textLog + "mensajeErrorEnviar>>" + mensajeErrorEnviar + "<<", EIGlobal.NivelLog.ERROR);
								// Servicio dispersor de transacciones ---------
								if ("OK".equals(mensajeErrorEnviar)) {
									tramaSalida = realizaDispersion(session.getUserID8(), session.getUserProfile(), contrato, folioArchivo, servicio);
								}
							} else {
								tramaSalida = ejecutaServicio(tramaEntrada);

							}



							EIGlobal.mensajePorTrace(textLog + "Trama salida>>" + tramaSalida + "<<", EIGlobal.NivelLog.INFO);
							String estatus_envio = "";
							String fechaAplicacionFormato = "";
							fechaAplicacionFormato = fechaAplicacion.substring( 0, 2) + "/" + fechaAplicacion.substring(2, 4) + "/" + fechaAplicacion.substring(4, 8);
							if (!"MANC".startsWith(tramaSalida)) {
								try {
									estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									estatus_envio = "";
								}
							}

							/* Todas las operaciones correctas exceptuando las
							 * IN04, la cuales en caso de que sean realizadas
							 * tanto de forma correcta como incorrecta se van
							 * por el flujo del else.
							*/
							if (estatus_envio.equals("NOMI0000")) {

								//FAVORITOS -----------

								  agregarFavoritoNomina(request,NominaPagosBO.generarInfoArchivo(rutaArch+"_preprocesar"));
								//FAVORITOS ----


								/*
								 * Si el servicio invocado fue NOMI_VAL_PAGO,
								 * entonces las validaciones se pasaron
								 * correctamente, por lo tanto hay que invocar
								 * al servicio NOMI_PAGO para procesar el
								 * archivo.
								 */
								if ("NOVP".equals(servicio)) {
									sess.setAttribute("VALIDAR", "N"); // Setear la variable global para que no se contin�e con la validaci�n.
									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago CREADO.", EIGlobal.NivelLog.DEBUG);
									defaultAction(request, response); // Ejecutar nuevamente la operaci�n de env�o para procesar el archivo.
									return;
								}

								String transmision = tramaSalida.substring(posCar(tramaSalida, '@', 2) + 1, posCar(tramaSalida, '@', 3));
								String nombreArchivo_aceptado = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
								String fechaCargo = null;
								int start = posCar(tramaSalida, '@', 3) + 1;
								int end = tramaSalida.indexOf('@', start);
								if (start < end) {
									fechaCargo = tramaSalida.substring(start, end);
									fechaCargo = fechaCargo.substring(0, 2) + "/" + fechaCargo.substring(2, 4) + "/" + fechaCargo.substring(4, 8);
								}
								String bgcolor = "textabdatobs";
								StringBuffer tablaTotales = new StringBuffer("");
								tablaTotales.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
								tablaTotales.append("<tr> ");
								tablaTotales.append("<td class=\"textabref\">Pago de n&oacute;mina</td>");
								tablaTotales.append("</tr>");
								tablaTotales.append("</table>");
								tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
								tablaTotales.append("<tr> ");
								tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
								tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descripci&oacute;n</td>");
								if (null != fechaCargo) {
									tablaTotales.append("<td class='tittabdat' width='90' align='center'>Fecha de Cargo</td>");
								}
								tablaTotales.append("<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
								tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
								tablaTotales.append("<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
								tablaTotales.append("<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de transmisi&oacute;n</td>");
								tablaTotales.append("</tr>");
								tablaTotales.append("<td class=\"");
								tablaTotales.append(bgcolor);
								tablaTotales.append("\" nowrap align=\"center\">");
								tablaTotales.append(session.getCuentaCargo());
								tablaTotales.append("&nbsp;</td>");
								tablaTotales.append("<td class=\"");
								tablaTotales.append(bgcolor);
								tablaTotales.append("\" nowrap align=\"center\">");
								tablaTotales.append(sess.getAttribute("descpCuentaCargo"));
								tablaTotales.append("&nbsp;</td>");
								if (null != fechaCargo) {
									tablaTotales.append("<td class='");
									tablaTotales.append(bgcolor);
									tablaTotales.append("' align='center'>");
									tablaTotales.append(fechaCargo);
									tablaTotales.append("</td>");
								}
								tablaTotales.append("<td class=\"");
								tablaTotales.append(bgcolor);
								tablaTotales.append("\" nowrap align=\"center\">");
								tablaTotales.append(fechaAplicacionFormato);
								tablaTotales.append("&nbsp;</td>");
								tablaTotales.append("<td class=\"");
								tablaTotales.append(bgcolor);
								tablaTotales.append("\" nowrap align=\"center\">");
								tablaTotales.append(FormatoMoneda.formateaMoneda(new Double(importe).doubleValue()));
								tablaTotales.append("&nbsp;</td>");
								tablaTotales.append("<td class=\"");
								tablaTotales.append(bgcolor);
								tablaTotales.append("\" nowrap align=\"center\">");
								tablaTotales.append(session.getNominaNumberOfRecords());
								tablaTotales.append("&nbsp;</td>");
								tablaTotales.append("<td class=\"");
								tablaTotales.append(bgcolor);
								tablaTotales.append("\" nowrap align=\"center\">");
								tablaTotales.append(transmision);
								tablaTotales.append("&nbsp;</td>");
								tablaTotales.append("</table><br>");
								tablaTotales.append("<input type=\"hidden\" name=\"hdnPagBack\" value=1>");
								nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());
								request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
								request.setAttribute("newMenu", session.getFuncionesDeMenu());
								request.setAttribute("MenuPrincipal", session.getStrMenu());
								request.setAttribute("Encabezado", CreaEncabezado( "Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
								request.setAttribute("TablaTotales", tablaTotales.toString());
								String infoUser = "La nomina fue aceptada correctamente: " + nombreArchivo_aceptado + " n�mero de transmisi�n = " + transmision;
								infoUser = "cuadroDialogo (\"" + infoUser + "\",1)";
								request.setAttribute("infoUser", infoUser);
								if (sess.getAttribute("facultadesN") != null)
									request.setAttribute("facultades", sess.getAttribute("facultadesN"));
								request.setAttribute("tipoArchivo", "enviado");
								String botonestmp = "<br>"
										+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
										+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
										+ " <td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
										+ "<td></td></tr> </table>";
								request.setAttribute("botones", botonestmp);
								request.setAttribute("ContenidoArchivo", " ");
								EIGlobal.mensajePorTrace(textLog + "botones >>" + request .getAttribute("botones") + "<<", EIGlobal.NivelLog.INFO);
								evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
							} else {// de trasaccion exitosa
								/*
								 * Proposito: Reenvio de Errores en caso de que
								 * falle la importacion del Archivo En el
								 * horario especificado.
								 *
								 * NOTA: tambien se utiliza para las operaci�nes
								 * IN04 que sean enviadas correctamente o con
								 * error ya que se adapto el flujo para poder
								 * realizar una dispersi�n sin esperar respuesta
								 * del servidor.
								 */
								HorarioDisp = "";
								HorarioSele = "";
								if (FacultadPremier.indexOf("PremierVerdadero") != -1) {
									HorarioDisp = (String) sess.getAttribute("SelectHorarioPremier");
									/**
									 * PYME 2015 se agrega condicion para cuando el valor no lo
									 * encuentre en los parametros lo obtenga de los atributos.
									 */
									if ( getFormParameter(request, "horario_seleccionado") == null ) {
										HorarioSele = (String) getFormParameter(request, "horario_seleccionado");
									} else {
										HorarioSele = request.getAttribute("horario_seleccionado").toString();
									}
									EIGlobal.mensajePorTrace(textLog + "El horario seleccionado es:   " + HorarioSele, EIGlobal.NivelLog.INFO);
								}
								EIGlobal.mensajePorTrace(textLog + "****************BOX 2********************", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + "****************************************", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + ">>" + sess.getAttribute("statusHorario") + "<<", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + ">>" + request.getAttribute("hr_lost") + "<<", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + ">>" + HorarioDisp + "<<", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + "****************************************", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + "*******************BOX 2*****************", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + "HORARIO PREMIER>>" + HorarioSele + "<<", EIGlobal.NivelLog.INFO);
		                        request.setAttribute("folio", (String) getFormParameter(request, "folio"));
								EIGlobal.mensajePorTrace(textLog + "CCBEst_Env" + estatus_envio, EIGlobal.NivelLog.DEBUG);

								//Favoritos
								request.setAttribute("chkBoxFav", request.getParameter("strCheck"));
								request.setAttribute("strCheck", request.getParameter("strCheck"));
								
								strDescFav = request.getParameter("txtFav").getBytes();
								try {
									dscFav = new String(strDescFav, "UTF-8");
									EIGlobal.mensajePorTrace(
									"OpcionesNomina: Favorito sin Encode: "
											+ request.getParameter("txtFav").toString() , EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace(
									"OpcionesNomina: Favorito con Encode: "
											+ dscFav , EIGlobal.NivelLog.INFO);
								} catch (UnsupportedEncodingException e)
								{
									EIGlobal.mensajePorTrace(
									"OpcionesNomina: Excepcion con Encode: "
											+ e.getMessage() , EIGlobal.NivelLog.INFO);
								}
								
								request.setAttribute("txtFav", dscFav);
								request.setAttribute("cargarValores", "si");

								/* Si es OK significa que la operaci�n IN04 se
								 * ha realizado de manera correcta y por tanto
								 * envia mensaje de que todo es correcto.
								 */
								EIGlobal.mensajePorTrace(textLog + "tramasalida [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textLog + "folioArchivo[" + folioArchivo + "]", EIGlobal.NivelLog.INFO);
								if ("OK".equals(tramaSalida)){
									EIGlobal.mensajePorTrace(textLog + "IN04 Correcto, procediendo a generar tabla y mensaje", EIGlobal.NivelLog.DEBUG);

									//Inicio Nomina Arch
									NominaArchBO archBO = new NominaArchBO();
									NominaArchBean archBean = new NominaArchBean();
									final SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:MM");
									final Date fecha=new Date();
									final String fchCarga=f.format(fecha);

									archBean.setFechaCarga(fchCarga);
									archBean.setContrato(contrato);
									archBean.setNombreArchivo(nombreArchivo);
									archBean.setNumRegistros(totalRegNom);
									archBean.setImporte(importePunto);
									archBean.setEstatusCarga("N");
									archBean.setTipoArchivo("T");

									archBean.setBiatuxOrigen(validaTerciaBiatux(archBean).getBiatuxOrigen());


									archBO.registraNominaArch(archBean);

									//Fin Nomina Arch

									sess.setAttribute("HorarioNormal", "0");

									String tablaTotales = "";

									if (sess.getAttribute("descpCuentaCargo") == null) {
										sess.setAttribute("descpCuentaCargo", "");
									}

									try {
										tablaTotales = generaTablaTotales(
												session.getCuentaCargo(),
												sess.getAttribute("descpCuentaCargo").toString(),
												fechaAplicacionFormato,
												importe,
												session.getNominaNumberOfRecords()
										);
									} catch (Exception e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									}

									StringBuffer mensajeUser = new StringBuffer();

									mensajeUser.append("La n&oacute;mina est&aacute; siendo procesada.<br>");
									mensajeUser.append("El n&uacute;mero de folio correspondiente a la operaci&oacute;n es:<br>");
									mensajeUser.append(folioArchivo);
									mensajeUser.append("<br>Utillice este n&uacute;mero para consultar las<br>");
									mensajeUser.append("referencias y estado de las referencias,<br>");
									mensajeUser.append("en Seguimiento de Transferencias.");

									String infoUser = "cuadroDialogo (\"" + mensajeUser.toString() + "\",10)";
									request.setAttribute("infoUser", infoUser);

									if (sess.getAttribute("facultadesN") != null) {
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));
									}

									String botonestmp = "<br>"
											+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
											+ " <td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table></td></tr>";
									request.setAttribute("botones", botonestmp);

									nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());
									request.setAttribute("archivo_actual", nombreArchivo);
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
									request.setAttribute("TablaTotales", tablaTotales);
									request.setAttribute("ContenidoArchivo", " ");
									/**
									 * PYME UNIFICACION DE TOKEN, Se agrega Atributo
									 * (mostrarToken)para mostrar u ocultar el token
									 */
									request.setAttribute("mostrarToken", "false");
									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);

									try {
									EmailSender emailSender = new EmailSender();
					    			EmailDetails beanEmailDetails = new EmailDetails();

						    			int noRegistros = 0;
						    			int referenciaInt = 0;
						    			String referencia = "";

						    			try {
						    				referenciaInt = Integer.parseInt(folioArchivo);
						    				referencia = String.valueOf(referenciaInt);
						    			} catch (Exception e) {
						    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						    				referencia = folioArchivo;
										}

						    			try {
						    				noRegistros = Integer.parseInt(session.getNominaNumberOfRecords().trim());
						    			} catch (Exception e) {
						    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}

						    			if (importe.length() >= 2) {
						    				importe = (importe.substring(0, importe.length()-2).concat(".").concat(importe.substring(importe.length()-2)));
						    			}

						    			EIGlobal.mensajePorTrace("�������������������� Notificacion en OpcionesNomina", EIGlobal.NivelLog.INFO);
						    			//EIGlobal.mensajePorTrace("�������������������� Se debe enviar notificacion? " + emailSender.enviaNotificacionArchivo(cod_error), EIGlobal.NivelLog.INFO);
						    			EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
						    			EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
						    			EIGlobal.mensajePorTrace("�������������������� CuentaCargo ->" + session.getCuentaCargo(), EIGlobal.NivelLog.INFO);
						    			EIGlobal.mensajePorTrace("�������������������� Importe ->" + importe, EIGlobal.NivelLog.INFO);
						    			EIGlobal.mensajePorTrace("�������������������� No de Transmision ->" + referencia, EIGlobal.NivelLog.INFO);
						    			EIGlobal.mensajePorTrace("�������������������� Registros Importados ->" + noRegistros, EIGlobal.NivelLog.INFO);
						    			EIGlobal.mensajePorTrace("�������������������� Estatus -> NOMI0000(Hardcode)" , EIGlobal.NivelLog.INFO);

						    			beanEmailDetails.setNumeroContrato(session.getContractNumber());
						    			beanEmailDetails.setRazonSocial(session.getNombreContrato());
						    			beanEmailDetails.setNumCuentaCargo(session.getCuentaCargo());
						    			beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(importe));
						    			beanEmailDetails.setNumRef(referencia);
						    			beanEmailDetails.setNumTransmision(referencia);
						    			beanEmailDetails.setNumRegImportados(noRegistros);
						    			beanEmailDetails.setEstatusActual("NOMI0000");
						    			beanEmailDetails.setTipoPagoNomina("Tradicional");


						    				emailSender.sendNotificacion(request, IEnlace.PAGO_NOMINA, beanEmailDetails);



									} catch (Exception e) {
					    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									}
									////////////////

									evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
								}

								// Archivo Duplicado NOMI0050 ------------------
								else if (estatus_envio.equals("NOMI0050"))
								{
									sess.setAttribute("HorarioNormal", "0");
									sess.setAttribute(HR_P, HorarioSele);
									EIGlobal.mensajePorTrace(textLog + "HR_P 2>>" + sess.getAttribute(HR_P) + "<<", EIGlobal.NivelLog.INFO);
									request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
									String infoUser = " Archivo Duplicado. �Desea enviar este archivo de cualquier manera? ";
									String infoEncabezadoUser = "<B>ALERTA</B>";
									infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
									request.setAttribute("infoUser", infoUser);
									if (sess.getAttribute("facultadesN") != null)
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));
									sess.setAttribute("archDuplicado", "si");
									request.setAttribute("digit", "0");
									request.setAttribute("tipoArchivo", "duplicado");
									request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
									EIGlobal.mensajePorTrace(textLog + "El horario seleccionado antes de volver a llamar el MantenimientoNomina despuesa de archivo duplicado es: ========>" + HorarioSele + "<======", EIGlobal.NivelLog.DEBUG);
									request.setAttribute("horario_seleccionado", HorarioSele);
									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago CREADO por Duplicado.", EIGlobal.NivelLog.DEBUG);
									/**
									 * PYME 2015 UNIFICACION DE TOKEN
									 */
									request.setAttribute("operacion","envio");
									evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
								}

								// EL archivo enviado solo se puede procesar en el horario 17:00
								else if (estatus_envio.equals("NOMI0051")) {
									sess.setAttribute("HorarioNormal", "1");
									sess.setAttribute(HR_P, "17:00");
									EIGlobal.mensajePorTrace(textLog + "HR_P 2 >>" + sess.getAttribute(HR_P) + "<<", EIGlobal.NivelLog.INFO);
									request.setAttribute("archivo_actual ",nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
									String infoUser = " El vol&uacute;men que desea dispersar solamente puede ser procesado en el horario de 17:00";
									String infoEncabezadoUser = "<B>Aviso</B>";
									infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
									request.setAttribute("infoUser", infoUser);
									if (sess.getAttribute("facultadesN") != null)
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));
									request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
									request.setAttribute("horarioPremier", "17:00");
									sess.setAttribute("archDuplicado", "si");
									request.setAttribute("digit", "2");
									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago CREADO por horario de 17:00 hrs.", EIGlobal.NivelLog.DEBUG);
									evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
								}

								//Horario Saturado Elija el siguiente horario del elejido
								else if (estatus_envio.equals("NOMI0052")) {
									/*
									 * Horarios saturados verificacion de
									 * horarios para establecer mensaje de
									 * cambio de fecha de aplicacion
									 */
									EIGlobal.mensajePorTrace(textLog + "Verificando los horarios saturados", EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace(textLog + "Horario seleccionado: " + HorarioSele, EIGlobal.NivelLog.INFO);
									String cad = (String) sess.getAttribute("HOR_SAT");
									if (cad == null) {
										sess.setAttribute("HOR_SAT", HorarioSele);
										cad = HorarioSele + "|";
									} else
										cad = cad + HorarioSele + "|";
									sess.setAttribute("HOR_SAT", cad);
									EIGlobal.mensajePorTrace(textLog + "Horarios Saturados: " + cad, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace(textLog + ">> Antes de validacion de horarios: " + HorarioDisp + "<<", EIGlobal.NivelLog.INFO);
									EI_Tipo hrD = new EI_Tipo();
									hrD.iniciaObjeto(HorarioDisp + "@");

									String nuevosHorariosDisp = "";
									EIGlobal.mensajePorTrace(textLog + ">> Se buscaran horarios saturados en sesion<<", EIGlobal.NivelLog.INFO);
									for (int i = 0; i < hrD.totalCampos; i++) {
										EIGlobal.mensajePorTrace(textLog + ">> Horario disponible: " + hrD.camposTabla[0][i] + "<<", EIGlobal.NivelLog.DEBUG);
										if (!"".equals(hrD.camposTabla[0][i].trim()))
											if (!hrD.camposTabla[0][i].equals(HorarioSele))
												if (!(cad.indexOf(hrD.camposTabla[0][i].trim()) >= 0)) {
													EIGlobal.mensajePorTrace(textLog + ">> El horario no se encontro : " + hrD.camposTabla[0][i] + "<<", EIGlobal.NivelLog.DEBUG);
													EIGlobal.mensajePorTrace(textLog + ">> en saturados: " + cad + "<<", EIGlobal.NivelLog.DEBUG);
													nuevosHorariosDisp += hrD.camposTabla[0][i] + "|";
												}
									}
									HorarioDisp = nuevosHorariosDisp;

									EIGlobal.mensajePorTrace(textLog + ">> Despues de validacion de horarios: " + HorarioDisp + "<<", EIGlobal.NivelLog.INFO);

									sess.setAttribute("HorarioNormal", "0");
									sess.setAttribute(HR_P, HorarioSele);
									request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
									String infoUser = "El horario de las " + HorarioSele + " por el momento no se encuentra disponible.";
									String infoEncabezadoUser = "<B>Horario No Disponible</B>";

									if (HorarioDisp.trim().length() < 4) {
										infoUser = "Por el momento no se encuentra disponible ning&uacute;n horario. Por favor cambie la fecha de aplicaci&oacute;n del archivo para realizar su env&iacute;o.";
										infoUser = "cuadroDialogoPagosNomina(\" <b>Horarios No Disponibles</b>\",\"" + infoUser + "\",10)";
									} else {
										if (FacultadPremier.indexOf("PremierVerdadero") != -1) {
											infoUser = "El horario de las " + HorarioSele + " por el momento no se encuentra disponible. Por favor elija otro horario entre los siguientes: ";
											infoUser += obtenSeleccion(HorarioDisp, HorarioSele, 4);
											infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",2)";// ENVIAR
										} else {
											infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",15)";// ENVIAR
										}
									}

									request.setAttribute("infoUser", infoUser);
									if (sess.getAttribute("facultadesN") != null)
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));

									request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));

									if (FacultadPremier.indexOf("PremierVerdadero") != -1) {
										request.setAttribute("horario_seleccionado", HorarioSele);
									}
									sess.setAttribute("archDuplicado", "si");
									request.setAttribute("digit", "0");
									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago CREADO por horario saturado.", EIGlobal.NivelLog.DEBUG);

									evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
								}

								// Envio 1/2 hora antes
								else if ("NOMI0020".equals(estatus_envio)) {
									sess.setAttribute("HorarioNormal", "0");
									request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
									String infoUser = "Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n.";
									String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
									if (FacultadPremier.indexOf("PremierVerdadero") != -1) {
										infoUser = "Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n. Por favor elija uno de los siguientes horarios:";
										infoUser += obtenSeleccion(HorarioDisp, HorarioSele, 2);
										infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",2)";// ENVIAR
									} else {
										infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",15)";// ENVIAR
									}
									request.setAttribute("infoUser", infoUser);
									if (sess.getAttribute("facultadesN") != null)
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));
									request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
									if (FacultadPremier.indexOf("PremierVerdadero") != -1) {
										request.setAttribute("horario_seleccionado", HorarioSele);
									}
									sess.setAttribute("archDuplicado", "si");
									request.setAttribute("digit", "0");
									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago CREADO por media hora de anticipacion.", EIGlobal.NivelLog.DEBUG);

									evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
								}

								// Cualquier otro problema que se pueda presentar
								else {
									// "NOMI0008@NO SE PUDO INSERTAR EL ARCHIVO EN TABLA@";
									// ERROR MANC0014 2893Usuarios iguales en mancomunidad - Genere un nuevo folio<
									sess.setAttribute("HorarioNormal", "0");
									String mensaje_error = "";
									try {
										if (tramaSalida.startsWith("MANC"))
											mensaje_error = tramaSalida.substring(16, tramaSalida.length());
										else
											mensaje_error = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
									} catch (Exception e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										mensaje_error = "";
									}
									EIGlobal.mensajePorTrace(textLog + "mensaje_error: >>" + mensaje_error + "<<", EIGlobal.NivelLog.INFO);
									if ("".equals(mensaje_error))
										mensaje_error = "Servcio no disponible en este momento, intente m&aacute;s tarde !.";
									String bgcolor = "textabdatobs";

									StringBuffer tablaTotales = new StringBuffer();
									tablaTotales.append("<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
									tablaTotales.append("<tr> ");
									tablaTotales.append("<td class=\"textabref\">Pago de n&oacute;mina</td>");
									tablaTotales.append("</tr>");
									tablaTotales.append("</table>");
									tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
									tablaTotales.append("<tr> ");
									tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
									tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descipci&oacute;n</td>");
									tablaTotales.append("<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci�n</td>");
									tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
									tablaTotales.append("<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
									tablaTotales.append("</tr>");
									tablaTotales.append("<td class=\"");
									tablaTotales.append(bgcolor);
									tablaTotales.append("\" nowrap align=\"center\">");
									tablaTotales.append(session.getCuentaCargo());
									tablaTotales.append("&nbsp;</td>");
									tablaTotales.append("<td class=\"");
									tablaTotales.append(bgcolor);
									tablaTotales.append("\" nowrap align=\"center\">");
									tablaTotales.append(sess.getAttribute("descpCuentaCargo"));
									tablaTotales.append("&nbsp;</td>");
									tablaTotales.append("<td class=\"");
									tablaTotales.append(bgcolor);
									tablaTotales.append("\" nowrap align=\"center\">");
									tablaTotales.append(fechaAplicacionFormato);
									tablaTotales.append("&nbsp;</td>");
									tablaTotales.append("<td class=\"");
									tablaTotales.append(bgcolor);
									tablaTotales.append("\" nowrap align=\"center\">");
									tablaTotales.append(formatoMoneda(importe));
									tablaTotales.append("&nbsp;</td>");
									tablaTotales.append("<td class=\"");
									tablaTotales.append(bgcolor);
									tablaTotales.append("\" nowrap align=\"center\">");
									tablaTotales.append(session.getNominaNumberOfRecords());
									tablaTotales.append("&nbsp;</td>" + "</table><br>");
									nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());
									request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800IIIh", request));
									request.setAttribute("TablaTotales", tablaTotales.toString());
									String infoUser = mensaje_error;
									infoUser = "cuadroDialogo (\"" + infoUser + "\",1)";
									request.setAttribute("infoUser", infoUser);
									if (sess.getAttribute("facultadesN") != null) {
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));
									}
									String botonestmp = "<br>"
											+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
											+ " <td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table></td></tr>";
									request.setAttribute("botones", botonestmp);
									request.setAttribute("ContenidoArchivo", " ");

									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									EIGlobal.mensajePorTrace(textLog + "Atributo Ejec_Proc_Val_Pago CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);
									evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
								}
							}
						} else {
							EIGlobal.mensajePorTrace(textLog + "***Entra NominaOcupada", EIGlobal.NivelLog.DEBUG);
							request.setAttribute("MenuPrincipal", session.getStrMenu());
							request.setAttribute("newMenu", session.getFuncionesDeMenu());
							request.setAttribute("Encabezado", CreaEncabezado("Transacci&oacute;n en Proceso", "Servicios &gt; N&oacute;mina &gt; Pagos", "s55290", request));
							request.setAttribute("operacion", "tranopera");
							evalTemplate("/jsp/NominaTranenProceso.jsp", request, response);
						}


					}// Termina else para interrumpe el flujo
				} // Termina operacion igual a envio
				if ("enviarCorreo".equals(strOperacion)) { // enviar el correo
					EIGlobal.mensajePorTrace(textLog + "cancelar- ", EIGlobal.NivelLog.DEBUG);
					String contrato = session.getContractNumber();
					String empleado = session.getUserID8();
					String perfil = session.getUserProfile();
	                String valores_cancelar = (String) getFormParameter(request, "valor_radio"); //valor del radio boton
					valores_cancelar = valores_cancelar.trim();
					EIGlobal.mensajePorTrace(textLog + "cancelar- Valor del radio >>" + valores_cancelar, EIGlobal.NivelLog.DEBUG);
					String archivo = valores_cancelar.substring(0, posCar(valores_cancelar, '@', 1));
					String secuencia = valores_cancelar.substring(posCar(valores_cancelar, '@', 1) + 1, posCar(valores_cancelar, '@', 2));
					String fechaRecepcion = valores_cancelar.substring(posCar(valores_cancelar, '@', 2) + 1, valores_cancelar.length());
					EIGlobal.mensajePorTrace(textLog + "cancelar- fechaRecepcion:>>" + fechaRecepcion, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textLog + "cancelar- fechaRecepcion formateada:>>" + fechaRecepcion, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textLog + "cancelar- secuencia:>>" + secuencia, EIGlobal.NivelLog.DEBUG);
	                String lista_nombres_archivos = (String) getFormParameter(request, "lista_nombres_archivos"); //del radio boton
	                String arch_salida_consulta = (String) getFormParameter(request, "nombre_arch_salida"); // /tmp/10001851.nomCon
	                String correo = (String) getFormParameter(request, "CorreoE");
					request.setAttribute("MenuPrincipal", session.getStrMenu());
					request.setAttribute("newMenu", session.getFuncionesDeMenu());
					request.setAttribute("Encabezado", CreaEncabezado("Cancelaci&oacute;n de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Consulta de N&oacute;mina &gt; Cancelaci&oacute;n de N&oacute;mina", CONST_CANH, request));
					request.setAttribute("Fechas", ObtenFecha(true));
					request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");
					if ("".equals(contrato) || contrato == null){
						contrato = " ";}
					if ("".equals(archivo)){
						archivo = " ";}
					EIGlobal.mensajePorTrace(textLog + CANCELAR_ARCH + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
					arch_salida_consulta = arch_salida_consulta.trim();
					EIGlobal.mensajePorTrace(textLog + CANCELAR_ARCH + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textLog + "cancelar- archivo:>>" + archivo, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textLog + "cancelar- contrato:>>" + contrato, EIGlobal.NivelLog.DEBUG);
					ServicioTux tuxGlobal = new ServicioTux();
					Hashtable htResult = null;
					String tramaEntrada = "";
					String direccionCorreo = "";
					if (correo.indexOf("@") > 0){
						direccionCorreo = correo.substring(0, correo.indexOf("@")) + "[]" + correo.substring(correo.indexOf("@") + 1);}
					EIGlobal.mensajePorTrace(textLog + "Correo>>" + direccionCorreo.trim() + "<<", EIGlobal.NivelLog.INFO);
					tramaEntrada = "1EWEB|" + empleado + "|NOEM|" + contrato + "|" + empleado + "|" + perfil + "|" + contrato + "@" + secuencia + "@" + direccionCorreo + "@";
					EIGlobal.mensajePorTrace(textLog + "Trama enviada a NOCA:>>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);
					try {
						htResult = tuxGlobal.web_red(tramaEntrada);
					} catch (Exception e) {
						//re.printStackTrace();
						try{
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("OpcionesNomina::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage(), EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("<DATOS GENERALES>", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
						}catch(Exception e2){
							EIGlobal.mensajePorTrace("OpcionesNomina::defaultAction::"
									                + "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									                + e2.getMessage(), EIGlobal.NivelLog.ERROR);
						}
					}
					String tramaSalida = (String) htResult.get("BUFFER");
					EIGlobal.mensajePorTrace(textLog + "El servicio nomi_enviardetalle regreso: >>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
					if (tramaSalida == null || "".equals(tramaSalida.trim())){
						tramaSalida = "NOMI9999        @Error en el servicio, intente mas tarde@";}
					String estatus_transacion = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
					String mensaje_transacion = tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@', 2));

					ArchivosNomina archSalida = new ArchivosNomina(arch_salida_consulta, request);

					/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
					archSalida.setTipoNomina(VAL_TIPO_SOLPDF_TR);

					if ( "NOMI0000".equals(estatus_transacion) ) {
						String contenidoArch = archSalida.lecturaArchivoSalidaConsul(lista_nombres_archivos);
						if ("".equals(contenidoArch) || contenidoArch.indexOf("Error ") >= 0 || contenidoArch.startsWith("Error")) {
							String elMensaje = "Error en la lectura del archivo, Intente m�s tarde por favor.";
							String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
							request.setAttribute("infoUser", infoUser);
							if (sess.getAttribute("facultadesN") != null){
								request.setAttribute("facultades", sess.getAttribute("facultadesN"));}
							request.setAttribute("nombre_arch_salida", nombre_arch_salida);
							request.setAttribute("lista_nombres_archivos", lista_nombres_archivos);
							String botonestmp = "<br>"
									+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
									+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
									+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
									+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
									+ "<td></td></tr> </table>";
							request.setAttribute("botones", botonestmp);
							request.setAttribute("ContenidoArchivo", contenidoArch);
							evalTemplate(IEnlace.NOMINADATOS_TMPL, request, response);
						} else {
							ArchivosNomina archSalida1 = new ArchivosNomina(arch_salida_consulta, request);

							/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
							archSalida1.setTipoNomina(VAL_TIPO_SOLPDF_TR);


							lista_nombres_archivos += ";" + "";
							EIGlobal.mensajePorTrace(textLog + "cancelar- lista_nombres_archivos:>>" + lista_nombres_archivos, EIGlobal.NivelLog.DEBUG);
							String contenidoArchivo = archSalida1.lecturaArchivoSalidaConsul(lista_nombres_archivos);
							String infoUser = "cuadroDialogo (\"" + mensaje_transacion + "\",1)";
							request.setAttribute("infoUser", infoUser);
							request.setAttribute("ContenidoArchivo", contenidoArchivo);
							request.setAttribute("nombre_arch_salida", arch_salida_consulta);
							if (sess.getAttribute("facultadesN") != null)
								request.setAttribute("facultades", sess.getAttribute("facultadesN"));
							request.setAttribute("lista_nombres_archivos", lista_nombres_archivos);
							arch_exportar = session.getUserID8() + "Nom.doc";
							String botonestmp = "<br>"
									+ ABRE_TABLE
									+ EXPORTAR_TXT
									+ EXPORTAR_XLS
									+ ABRE_TR
									+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
									+ // Exportacion de archivo.
									"<td><A href = \"/Download/"
									+ arch_exportar
									+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>"
									+ // Ya no se enviara el detalle por
										// correo ...
									// pues siempre si ...
									"<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
									+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
									+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"

									/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
									+SOL_INFORME

									+ "<td></td></tr> </table>";
							request.setAttribute("botones", botonestmp);
							evalTemplate(IEnlace.NOMINADATOS_TMPL, request,
									response);
						}
					} else { // Transaccion Exitosa
						String contenidoArchivo = archSalida
								.lecturaArchivoSalidaConsul();
						request.setAttribute("ContenidoArchivo",
								contenidoArchivo);
						request.setAttribute("nombre_arch_salida",
								arch_salida_consulta);
						String infoUser = "cuadroDialogo (\""
								+ mensaje_transacion + "\",1)";
						request.setAttribute("infoUser", infoUser);
						if (sess.getAttribute("facultadesN") != null)
							request.setAttribute("facultades", sess
									.getAttribute("facultadesN"));
						arch_exportar = session.getUserID8() + "Nom.doc";
						String botonestmp = "<br>"
								+ ABRE_TABLE
								+ EXPORTAR_TXT
								+ EXPORTAR_XLS
								+ ABRE_TR
								+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
								+ // Exportacion de archivo.
								"<td><A href = \"/Download/"
								+ arch_exportar
								+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>"
								+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
								+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
								+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"

								/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
								+SOL_INFORME

								+ "<td></td></tr> </table>";
						request.setAttribute("botones", botonestmp);
						evalTemplate(IEnlace.NOMINADATOS_TMPL, request,
								response);
						// se presenta los registros con todos los registros
					}
				}
				if ("cancelar".equals(strOperacion)) { // cancelar una nomina
					EIGlobal.mensajePorTrace(textLog + "cancelar- ", EIGlobal.NivelLog.DEBUG);
					String diaRecpcion = "";
					String mesRecpcion = "";
					String anoRecpcion = "";
					String contrato = session.getContractNumber();
					String empleado = session.getUserID8();
					String perfil = session.getUserProfile();
					String valores_cancelar = (String) request
							.getParameter("valor_radio"); // valor del radio
															// boton
					valores_cancelar = valores_cancelar.trim();
					EIGlobal.mensajePorTrace(textLog + "cancelar- Valor del radio :::::::::>>" + valores_cancelar, EIGlobal.NivelLog.DEBUG);
					String archivo = valores_cancelar.substring(0, posCar(
							valores_cancelar, '@', 1));
					String secuencia = valores_cancelar.substring(posCar(
							valores_cancelar, '@', 1) + 1, posCar(
							valores_cancelar, '@', 2));
					String fechaRecepcion = valores_cancelar.substring(posCar(
							valores_cancelar, '@', 2) + 1, valores_cancelar
							.length());
					EIGlobal.mensajePorTrace(textLog + "cancelar- fechaRecepcion:>>" + fechaRecepcion, EIGlobal.NivelLog.DEBUG);
					if (fechaRecepcion.length() >= 10) {
						diaRecpcion = fechaRecepcion.substring(0, posCar(
								fechaRecepcion, '/', 1));
						mesRecpcion = fechaRecepcion.substring(posCar(
								fechaRecepcion, '/', 1) + 1, posCar(
								fechaRecepcion, '/', 2));
						anoRecpcion = fechaRecepcion.substring(posCar(
								fechaRecepcion, '/', 2) + 1, fechaRecepcion
								.length());
						fechaRecepcion = diaRecpcion + mesRecpcion
								+ anoRecpcion;
						fechaRecepcion = fechaRecepcion.trim();
					}
					EIGlobal.mensajePorTrace(textLog + "cancelar- fechaRecepcion formateada:>>" + fechaRecepcion + "<<", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textLog + "cancelar- secuencia:>>" + secuencia + "<<", EIGlobal.NivelLog.DEBUG);
					String lista_nombres_archivos = (String) request
							.getParameter("lista_nombres_archivos"); // del
																		// radio
																		// boton
					String arch_salida_consulta = (String) request
							.getParameter("nombre_arch_salida");
					request.setAttribute("MenuPrincipal", session.getStrMenu());
					request.setAttribute("newMenu", session
							.getFuncionesDeMenu());
					request
							.setAttribute(
									"Encabezado",
									CreaEncabezado(
											"Cancelaci&oacute;n de N&oacute;mina",
											"Servicios &gt; N&oacute;mina &gt; Consulta de N&oacute;mina &gt; Cancelaci&oacute;n de N&oacute;mina",
											CONST_CANH, request));
					request.setAttribute("Fechas", ObtenFecha(true));
					request.setAttribute("FechaHoy", ObtenFecha(false)
							+ " - 06");
					if (contrato.equals("") || contrato == null)
						contrato = " ";
					if (archivo.equals(""))
						archivo = " ";
					EIGlobal.mensajePorTrace(textLog + CANCELAR_ARCH + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
					arch_salida_consulta = arch_salida_consulta.trim();
					EIGlobal.mensajePorTrace(textLog + CANCELAR_ARCH + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textLog + "cancelar- archivo:>>" + archivo, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textLog + "cancelar- contrato:>>" + contrato, EIGlobal.NivelLog.DEBUG);
					ServicioTux tuxGlobal = new ServicioTux();
					Hashtable htResult = null;
					String tramaEntrada = "";// trama que se enviara a
												// WEB_RED
					tramaEntrada = "1EWEB|" + empleado + "|NOCA|" + contrato
							+ "|" + empleado + "|" + perfil + "|" + contrato
							+ "@" + secuencia + "@" + fechaRecepcion + "@";
					EIGlobal.mensajePorTrace(textLog + "Trama enviada a NOCA:>>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);
					try {
						htResult = tuxGlobal.web_red(tramaEntrada);
					} catch (Exception e) {
						//re.printStackTrace();
						try{
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("OpcionesNomina::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage(), EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("<DATOS GENERALES>", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
						}catch(Exception e2){
							EIGlobal.mensajePorTrace("OpcionesNomina::defaultAction::"
									                + "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									                + e2.getMessage(), EIGlobal.NivelLog.ERROR);
						}
					}
					String tramaSalida = (String) htResult.get("BUFFER");
					// String tramaSalida = "NOMI0000@Transaccion
					// Exitosa@NOMIXXXX@Mensaje de error@";
					EIGlobal.mensajePorTrace(textLog + "El servicio nomi_cancela regreso: >>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
					if (tramaSalida == null || tramaSalida.trim().equals("") || tramaSalida.indexOf("@") == -1)
						tramaSalida = "NOMI9999        @Error en el servicio, intente mas tarde@";
					String estatus_transacion = tramaSalida.substring(0,
							posCar(tramaSalida, '@', 1));
					String mensaje_transacion = tramaSalida.substring((posCar(
							tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@',
							2));
					ArchivosNomina archSalida = new ArchivosNomina(
							arch_salida_consulta, request);

					/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
					archSalida.setTipoNomina(VAL_TIPO_SOLPDF_TR);

					if (estatus_transacion.equals("NOMI0000")) {
						String contenidoArch = archSalida
								.lecturaArchivoSalidaConsul(lista_nombres_archivos);
						// se presenta los registros con el archvio estatus
						// cancelado y sin radio boton
						if (contenidoArch.equals("")
								|| contenidoArch.indexOf("Error ") >= 0
								|| contenidoArch.startsWith("Error")) {
							String elMensaje = "Error en la lectura del archivo, Intente m�s tarde por favor.";
							String infoUser = "cuadroDialogo (\"" + elMensaje
									+ "\",1)";
							request.setAttribute("infoUser", infoUser);
							if (sess.getAttribute("facultadesN") != null)
								request.setAttribute("facultades", sess
										.getAttribute("facultadesN"));
							request.setAttribute("nombre_arch_salida",
									nombre_arch_salida);
							request.setAttribute("lista_nombres_archivos",
									lista_nombres_archivos);
							String botonestmp = "<br>"
									+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
									+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
									+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
									+ " <td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
									+ "<td></td></tr> </table>";
							request.setAttribute("botones", botonestmp);
							request.setAttribute("ContenidoArchivo",
									contenidoArch);
							evalTemplate(IEnlace.NOMINADATOS_TMPL, request,
									response);
						} else {
							ArchivosNomina archSalida1 = new ArchivosNomina(
									arch_salida_consulta, request);

							/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
							archSalida1.setTipoNomina(VAL_TIPO_SOLPDF_TR);

							lista_nombres_archivos += ";" + archivo;
							EIGlobal.mensajePorTrace(textLog + "cancelar- lista_nombres_archivos:>>" + lista_nombres_archivos + "<<", EIGlobal.NivelLog.DEBUG);
							String contenidoArchivo = archSalida1
									.lecturaArchivoSalidaConsul(lista_nombres_archivos);
							String infoUser = "cuadroDialogo (\""
									+ mensaje_transacion + "\",1)";
							request.setAttribute("infoUser", infoUser);
							request.setAttribute("ContenidoArchivo",
									contenidoArchivo);
							request.setAttribute("nombre_arch_salida",
									arch_salida_consulta);
							if (sess.getAttribute("facultadesN") != null)
								request.setAttribute("facultades", sess
										.getAttribute("facultadesN"));
							request.setAttribute("lista_nombres_archivos",
									lista_nombres_archivos);
							arch_exportar = session.getUserID8() + "Nom.doc";
							String botonestmp = "<br>"
									+ ABRE_TABLE
									+ EXPORTAR_TXT
									+ EXPORTAR_XLS
									+ ABRE_TR
									+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
									+
									// Exportacion de archivo.
									"<td><A href = \"/Download/"
									+ arch_exportar
									+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>"
									+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
									+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
									+ " <td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"

									/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
									+SOL_INFORME

									+ "<td></td></tr> </table>";
							request.setAttribute("botones", botonestmp);
							evalTemplate(IEnlace.NOMINADATOS_TMPL, request,
									response);
						}
					} else { // Transaccion Exitosa
						String contenidoArchivo = archSalida
								.lecturaArchivoSalidaConsul();
						request.setAttribute("ContenidoArchivo",
								contenidoArchivo);
						request.setAttribute("nombre_arch_salida",
								arch_salida_consulta);
						String infoUser = "cuadroDialogo (\""
								+ mensaje_transacion + "\",1)";
						request.setAttribute("infoUser", infoUser);
						if (sess.getAttribute("facultadesN") != null)
							request.setAttribute("facultades", sess
									.getAttribute("facultadesN"));
						arch_exportar = session.getUserID8() + "Nom.doc";
						String botonestmp = "<br>"
								+ ABRE_TABLE
								+ EXPORTAR_TXT
								+ EXPORTAR_XLS
								+ ABRE_TR
								+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
								+
								// Exportacion de archivo.
								"<td><A href = \"/Download/"
								+ arch_exportar
								+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>"
								+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
								+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
								+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"

								/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
								+SOL_INFORME

								+ "<td></td></tr> </table>";
						request.setAttribute("botones", botonestmp);
						evalTemplate(IEnlace.NOMINADATOS_TMPL, request,
								response);
						// se presenta los registros con todos los registros
					}
				}
				if (strOperacion.equals("consulta")) {
					consultaArchivos(request, response);
				}
				String arcRecuperado = "";
				String tmpTipoArchivo = "";
				if (strOperacion.equals("reporte")) {
					request.setAttribute("archivo", "Pagos");
					arcRecuperado = getFormParameter(request, "tipoArchivo");
					String registrosEnArchivo = request
							.getParameter("cantidadDeRegistrosEnArchivo");
					EIGlobal.mensajePorTrace(textLog + "EL VALOR DE registrosEnArchivo dentro de reporte>>" + registrosEnArchivo + "<<", EIGlobal.NivelLog.DEBUG);
					arcRecuperado.trim();
					EIGlobal.mensajePorTrace(textLog + "EL VALOR DE arcRecuperado> antes>>" + arcRecuperado + "<<", EIGlobal.NivelLog.DEBUG);
					if (arcRecuperado.equals(""))
						arcRecuperado = "nuevo";
					EIGlobal.mensajePorTrace(textLog + "EL VALOR DE arcRecuperado>>" + arcRecuperado + "<<", EIGlobal.NivelLog.DEBUG);
					tmpTipoArchivo = arcRecuperado.substring(0, 1);
					EIGlobal.mensajePorTrace(textLog + "EL VALOR DE tmpTipoArchivo>>" + tmpTipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
					if (tmpTipoArchivo.equals("r")) {
						nombre_arch_salida = Global.DOWNLOAD_PATH
								+ getFormParameter(request, "archivo_actual");
						String secuencia_recu = getFormParameter(request,
								"transmision");
						ArchivosNomina archRecuperado = new ArchivosNomina(
								nombre_arch_salida, request);

						/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
						archRecuperado.setTipoNomina(VAL_TIPO_SOLPDF_TR);

						request.setAttribute("newMenu", session
								.getFuncionesDeMenu());
						request.setAttribute("MenuPrincipal", session
								.getStrMenu());
						request.setAttribute("Encabezado", CreaEncabezado(
								"Pago de N&oacute;mina",
								"Servicios &gt; N&oacute;mina &gt; Altas",
								"s25810h", request));
						request.setAttribute("etiquetaNameFile",
								"Nombre del archivo: Archivo Recuperado");
						request.setAttribute("etiquetaNumTransmision",
								"N&uacute;mero de Transmisi&oacute;n: "
										+ secuencia_recu);
						request.setAttribute("tipoArchivo", arcRecuperado);
						request.setAttribute("cantidadDeRegistrosEnArchivo",
								registrosEnArchivo);
						request.setAttribute("ContenidoArchivo", archRecuperado
								.reporteArchivoSalida());
					} else {
						String archivoName = (String) sess
								.getAttribute("nombreArchivo");
						// String archivoName=session.getNameFileNomina();
						String nombreArchivoOriginal = archivoName
								.substring(archivoName.lastIndexOf("/") + 1);
						// ArchivosNomina archNvoImportado=new
						// ArchivosNomina(this,
						// session.getNameFileNomina());
						ArchivosNomina archNvoImportado = new ArchivosNomina(
								(String) sess.getAttribute("nombreArchivo"),
								request);

						/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
						archNvoImportado.setTipoNomina(VAL_TIPO_SOLPDF_TR);

						request.setAttribute("newMenu", session
								.getFuncionesDeMenu());
						request.setAttribute("MenuPrincipal", session
								.getStrMenu());
						request.setAttribute("Encabezado", CreaEncabezado(
								"Pago de N&oacute;mina",
								"Servicios &gt; N&oacute;mina &gt; Altas",
								"s25810h", request));
						request.setAttribute("etiquetaNameFile",
								"Nombre del archivo: " + nombreArchivoOriginal);
						request
								.setAttribute("etiquetaNumTransmision",
										"N&uacute;mero de Transmisi&oacute;n: Archivo no transmitido");
						request.setAttribute("tipoArchivo", arcRecuperado);
						request.setAttribute("cantidadDeRegistrosEnArchivo",
								registrosEnArchivo);
						request.setAttribute("ContenidoArchivo",
								archNvoImportado.reporteArchivo());
					}

					evalTemplate(IEnlace.REPORTE_NOMINA_TMPL, request, response);
				}
				/*VECTOR - Solicitud de Informe PDF*/
				if(COD_OPER_PROG_SOl_PDF.equals(strOperacion)){
					solicitaInformePDF(request, response);
					redirigeRequest(request, response);

				}
				if(COD_OPER_PROG_DES_PDF.equals(strOperacion)){
					descargaInformePDF(request, response);
					redirigeRequest(request, response);

				}

				//Fin solicitud de informe PDF
			}// fin de else de validacion OTP.
		} // fin se sessionValida
		else {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	// metodo posCar que devuelve la posicion de un caracter en un String
	public int posCar(String cad, char car, int cant) {
		int result = 0, pos = 0;
		for (int i = 0; i < cant; i++) {
			result = cad.indexOf(car, pos);
			pos = result + 1;
		}
		return result;
	}

	/**
	 * Modificado por Miguel Cortes Arellano Fecha: 23/12/03 Proposito: Regresa
	 * el valor del HTML FORM SLECT campo para la opcion requerida 1 Significa
	 * que se obtengan todas las horas excepto la elegida por el usuario 2
	 * Significa que se selecciona la hora despues de la seleccionada por el
	 * usuario. Empieza codigo SINAPSIS
	 */
	private String obtenSeleccion(String HorarioDisp, String HorarioSele,
			int Opcion) {
		textoLog("obtenSeleccion");
		String tempSelect = "<OPTION VALUE=" + HorarioSele + ">" + HorarioSele
				+ "</OPTION><BR>";
		StringTokenizer splitSelect = new StringTokenizer(HorarioDisp, "|");
		String tempFinalSelect = "";
		String tempActualToken = "";
		boolean found = false;
		StringTokenizer indSelect;
		String tempToken = "";
		tempFinalSelect = "<SELECT NAME=HorarioPremierDialogo>";
		while (splitSelect.hasMoreTokens()) {
			tempActualToken = splitSelect.nextToken();
			indSelect = new StringTokenizer(tempActualToken, "BR");
			while (indSelect.hasMoreTokens()) {
				tempToken = indSelect.nextToken();
				tempToken = "<OPTION VALUE=" + tempToken + ">" + tempToken
						+ "</OPTION><BR>";
				EIGlobal.mensajePorTrace(textLog + "EL VALOR DEL tempFinalSelectACTUAL ES>>" + tempFinalSelect + "<<", EIGlobal.NivelLog.DEBUG);
				if (Opcion == 1) {
					if (tempToken.equals(tempSelect))
						continue;
					else
						tempFinalSelect += tempToken;
				} else if (Opcion == 2) {
					if (found)
						tempFinalSelect += tempToken;
					if (tempToken.equals(tempSelect))
						found = true;
				}
			}
		}
		tempFinalSelect += "</SELECT>";
		return tempFinalSelect;
	}

	public void leerArchivo(HttpServletRequest request,
			HttpServletResponse response, String tipoArchivo)
			throws IOException, ServletException {
		textoLog("leerArchivo");
		boolean sesionvalida = SesionValida(request, response);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String tmpTipoArchivo = "";
		String contenidoArch = "";
		String cuentaCargo = session.getCuentaCargo();
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		if (sess.getAttribute("facultadesN") != null)
			request
					.setAttribute("facultades", sess
							.getAttribute("facultadesN"));
		request.setAttribute("Encabezado", CreaEncabezado(
				"Pago de N&oacute;mina",
				"Servicios &gt; N&oacute;mina &gt; Pagos", "s25800h", request));
		String systemHour = ObtenHora();
		request.setAttribute("horaSistema", systemHour);
		String archivoName = (String) sess.getAttribute("nombreArchivo");
		ArchivoRemoto archR = new ArchivoRemoto();
		String nombreArchivoOriginal = archivoName.substring(archivoName
				.lastIndexOf("/") + 1);
		archR.copiaLocalARemoto(nombreArchivoOriginal, "WEB");
		if (session.getRutaDescarga() != null)
			request.setAttribute("DescargaArchivo", session.getRutaDescarga());
		request.setAttribute("archivo_actual", archivoName
				.substring(archivoName.lastIndexOf("/") + 1));
		if (sesionvalida) {// servidor para facultad a Mancomunidad
			String nombreArchivoEmp = "", contenidoArchivoStr = "";
			String registrosEnArchivo = request
					.getParameter("cantidadDeRegistrosEnArchivo");
			nombreArchivoEmp = (String) sess.getAttribute("nombreArchivo");
			ArchivosNomina archivo1 = new ArchivosNomina(nombreArchivoEmp,
					request);

			/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
			archivo1.setTipoNomina(VAL_TIPO_SOLPDF_TR);

			EIGlobal.mensajePorTrace(textLog + "valor de tipoArchivo >>" + tipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
			tipoArchivo.trim();
			if (tipoArchivo.equals(""))
				tipoArchivo = "nuevo";
			EIGlobal.mensajePorTrace(textLog + "EL VALOR DE tipoArchivo >>" + tipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
			tmpTipoArchivo = tipoArchivo.substring(0, 4);
			EIGlobal.mensajePorTrace(textLog + "EL VALOR DE tmpTipoArchivo >>" + tmpTipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
			if (tmpTipoArchivo.equals("r")) {
				contenidoArchivoStr += archivo1.lecturaArchivoSalida(request);
				if (contenidoArchivoStr != null
						&& contenidoArchivoStr.length() > 1) {
					contenidoArch = contenidoArchivoStr.substring(0,
							contenidoArchivoStr.indexOf("|"));
				}
			} else {
				contenidoArch += archivo1.lecturaArchivo();
			}
			request.setAttribute("cantidadDeRegistrosEnArchivo",
					registrosEnArchivo);
			request.setAttribute("ContenidoArchivo", contenidoArch);
			if (sess.getAttribute("facultadesN") != null)
				request.setAttribute("facultades", sess
						.getAttribute("facultadesN"));
			request.setAttribute("tipoArchivo", tipoArchivo);
			request.setAttribute("textcuenta", cuentaCargo);
			session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
			evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
		} else {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	public String comboPlazas() {
		String comboPlazas = "";
		comboPlazas += "\n <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas += "\n <input type=text size=25 name=PlazaBanxico value='MEXICO, DF' onFocus='blur();'>";
		comboPlazas += "\n <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>";
		return comboPlazas;
	}

	public String colocaPlaza(String plazaBank) {
		String comboPlazas = "";
		comboPlazas += "\n <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas += "\n <input type=text size=25 name=PlazaBanxico value='"
				+ plazaBank + "' onFocus='blur();'>";
		comboPlazas += "\n <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>";
		return comboPlazas;
	}

	public String formatoMoneda(String cantidad) {
		textoLog("formatoMoneda");
		EIGlobal.mensajePorTrace(textLog + "***Entrando a formatoMoneda ", EIGlobal.NivelLog.DEBUG);
		cantidad = cantidad.trim();
		String language = "la"; // ar
		String country = "MX"; // AF
		Locale local = new Locale(language, country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato = "";
		String cantidadDecimal = "";
		String formatoFinal = "";
		String cantidadtmp = "";
		if (cantidad == null || cantidad.equals(""))
			cantidad = "0.0";
		if (cantidad.length() > 2) {
			try {
				cantidadtmp = cantidad.substring(0, cantidad.length() - 2);
				cantidadDecimal = cantidad.substring(cantidad.length() - 2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				if (formato.substring(0,3).equals("MXN")){
					formato = formato.substring(3, formato.length() - 2);
				} else {
					formato = formato.substring(1, formato.length() - 2);
				}
				formatoFinal = "$ " + formato + cantidadDecimal;
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				formatoFinal = "$ " + "0.00";
			}
		} else {
			try {
				if (cantidad.length() > 1) {
					cantidadDecimal = cantidad.substring(cantidad.length() - 2);
					formatoFinal = "$ " + "0." + cantidadDecimal;
				} else {
					cantidadDecimal = cantidad.substring(cantidad.length() - 1);
					formatoFinal = "$ " + "0.0" + cantidadDecimal;
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				formatoFinal = "$ " + "0.00";
			}
		}
		if (formatoFinal == null)
			formato = "";
		return formatoFinal;
	}

	public String agregarPunto(String importe) {
		textoLog("agregarPunto");
		String importeConPunto = "";
		importe = importe.trim();
		if (importe == null || importe.equals(""))
			importe = "0.0";
		if (importe.length() > 2) {
			try {
				importe = importe.trim();
				String enteros = importe.substring(0, importe.length() - 2);
				enteros = enteros.trim();
				String decimales = importe.substring(importe.length() - 2,
						importe.length());
				importe = enteros + " " + decimales;
				importeConPunto = importe.replace(' ', '.');
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				importeConPunto = "0.00";
			}
		} else {
			try {
				if (importe.length() > 1) {
					importe = importe.trim();
					String decimales = importe.substring(importe.length() - 2,
							importe.length());
					importe = "0" + " " + decimales;
					importeConPunto = importe.replace(' ', '.');
				} else {
					importe = importe.trim();
					String decimales = importe.substring(importe.length() - 1,
							importe.length());
					importe = "0" + " " + "0" + decimales;
					importeConPunto = importe.replace(' ', '.');
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				importeConPunto = "0.00";
			}
		}
		if (importeConPunto == null)
			importeConPunto = "";
		return importeConPunto;
	}

	/** Descarga de archivos desde el AppServer */
	protected void descargaArchivos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		String nombreDelArchivo = "rada.zip";
		response.setHeader("Content-disposition", "attachment; filename="
				+ nombreDelArchivo);
		response.setContentType("application/x-gzip");
		BufferedInputStream entrada = null;
		BufferedOutputStream salida = null;
		InputStream arc = new FileInputStream(IEnlace.LOCAL_TMP_DIR + "/"
				+ nombreDelArchivo);
		ServletOutputStream out = response.getOutputStream();
		entrada = new BufferedInputStream(arc);
		salida = new BufferedOutputStream(out);
		byte[] buff = new byte[1024];
		int cantidad = 0;
		while ((cantidad = entrada.read(buff, 0, buff.length)) != -1)
			salida.write(buff, 0, cantidad);
		salida.close();
		entrada.close();
	}

	/**
	 * Despliega la pagina principal de Pagos
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected void inicioPagos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		textoLog("inicioPagos");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		if (sess.getAttribute("facultadesN") != null)
			request
					.setAttribute("facultades", sess
							.getAttribute("facultadesN"));
		request.setAttribute("ContenidoArchivo", "");
		request.setAttribute("Encabezado", CreaEncabezado(
				"Pago de N&oacute;mina",
				"Servicios &gt; N&oacute;mina &gt; Pagos", "s25800h", request));
		sess.setAttribute("FOLIO", "");
		sess.setAttribute("VALIDAR", "");
		EIGlobal.mensajePorTrace(textLog + "**********LIMPIANDO VARIABLES FOLIO Y VALIDAR****", EIGlobal.NivelLog.DEBUG);

		if (session.getFacultad("INOMENVIPAG")) {
			EIGlobal.mensajePorTrace(textLog + "DAG Entro modulo=0 y con Facultad INOMENVIPAG  DESDE OPCIONES NOMINA HELG****", EIGlobal.NivelLog.DEBUG);
			// Se controla la Recarga para evitar duplicidad de registros
			// transferencia
			verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString()
					+ ".ses", true);
			if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null) {
				request.getSession().removeAttribute("Recarga_Ses_Multiples");
				EIGlobal.mensajePorTrace(textLog + "Inicio Pagos - Borrando variable de sesion desde OpcionesNomina", EIGlobal.NivelLog.DEBUG);
			}
		}
		evalTemplate("/jsp/MantenimientoNomina.jsp", request, response);
	}

	/**
	 * muestra la pantalla de inicio de consultas de nomina
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected void inicioConsulta(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer datesrvr = new StringBuffer("");
		datesrvr
				.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvr.append(ObtenAnio());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvr.append(ObtenMes());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvr.append(ObtenDia());
		datesrvr.append("\">");
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Bitfechas", datesrvr.toString());
		request.setAttribute("Encabezado", CreaEncabezado(
				CONS_NOMINA,
				CON_NOM_PROG, "s25800conh",
				request));
		if (sess.getAttribute("facultadesN") != null)
			request
					.setAttribute("facultades", sess
							.getAttribute("facultadesN"));
		request.setAttribute("textcuenta", "");
		request.setAttribute("ContenidoArchivo", "");

		evalTemplate(IEnlace.NOMINADATOS_TMPL, request, response);
	}

	/**
	 * Realiza la consulta de archivos de nomina
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected void consultaArchivos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		textoLog("consultaArchivos");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer datesrvr = new StringBuffer("");
		datesrvr
				.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvr.append(ObtenAnio());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvr.append(ObtenMes());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvr.append(ObtenDia());
		datesrvr.append("\">");
		String nombre_arch_salida = "";
		EIGlobal.mensajePorTrace(textLog + "Entro a consula de nomina****", EIGlobal.NivelLog.DEBUG);
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable htResult = null;
		String tramaEntrada = generarTramaConsultaArchivo(request, response);
		try {
			EIGlobal.mensajePorTrace(textLog + "tramaEntrada >>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);
			htResult = tuxGlobal.web_red(tramaEntrada);
		} catch (Exception e) {
			//re.printStackTrace();
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNomina::consultaArchivos:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("<DATOS GENERALES>", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("OpcionesNomina::consultaArchivos::"
						                + "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
						                + e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		if (null == htResult) {
			EIGlobal.mensajePorTrace(textLog + "null ret despues de webred", EIGlobal.NivelLog.INFO);
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,
					CONS_NOMINA,
					CON_NOM_PROG, request,
					response);
			return;
		}
		String tramaSalida = (String) htResult.get("BUFFER");
		// Simulaci�n de la trama de regreso :
		// NOMI0001@Mensaje de error@
		// String tramaSalida = "NOMI0000@/tmp/1001851.conNom@";
		// "TCT_0010 1397Servicio de bitacora no se encuentra disponible";
		EIGlobal.mensajePorTrace(textLog + "El servicio consulta_nomi regreso:1 " + tramaSalida + "***", EIGlobal.NivelLog.DEBUG);
		String elMensaje = "";
		String estatus_envio = "";
		if (tramaSalida == null)
			tramaSalida = "";
		if (tramaSalida.indexOf("NOMI") >= 0) {
			elMensaje = tramaSalida.substring(
					(posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@',
							2));
			EIGlobal.mensajePorTrace(textLog + "elMensaje : >" + elMensaje + "<", EIGlobal.NivelLog.DEBUG);
			estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
			EIGlobal.mensajePorTrace(textLog + "estatus_envio : >" + estatus_envio + "<", EIGlobal.NivelLog.DEBUG);
		} else {
			elMensaje = "";
			estatus_envio = "";
		}
		if (estatus_envio.equals("NOMI0000")) {
			String nombreArchivo = tramaSalida.substring(tramaSalida
					.lastIndexOf("/") + 1, tramaSalida.lastIndexOf("@"));
			try {
				String tipoCopia = "2"; // de /t/tuxedo... a /tmp
				ArchivoRemoto archR = new ArchivoRemoto();
				if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
					EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
			} catch (Exception ioeSua) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
			}
			nombre_arch_salida = IEnlace.DOWNLOAD_PATH + nombreArchivo;
			EIGlobal.mensajePorTrace(textLog + "nombre_arch_salida: " + nombre_arch_salida + "***", EIGlobal.NivelLog.DEBUG);
			// copia el archivo en la ruta tmp
			// nombre_arch_salida = "/tmp/" + nombre_arch_salida;

			/*FSW Vector - Tactico Epyme*/
			try{
			sess.removeAttribute(VAR_SES_SOl_PDF);

			HashMap< Integer,SolPDFBean> comprobantesPDF = null;
			NominaBO nominaBO = null;
			SolPDFBean solPDFBean = null;

			solPDFBean = new SolPDFBean();
			solPDFBean.setContrato(session.getContractNumber());
			solPDFBean.setTipoNomina(VAL_TIPO_SOLPDF_TR);
			nominaBO = new NominaBO();
			//SE realiza la consulta de los comprobantes de Nomina En PDF y se sube a sesion.
			comprobantesPDF = nominaBO.consultaComprobantesPDF(solPDFBean);

			if(comprobantesPDF != null){
				EIGlobal.mensajePorTrace("OpcionesNomina.java :: Consulta Archivos :: Numero de soliciutdes " + comprobantesPDF.size(), EIGlobal.NivelLog.DEBUG);
				sess.setAttribute(VAR_SES_SOl_PDF, comprobantesPDF);
			}


			}catch(Exception e){
				EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			ArchivosNomina archSalida = new ArchivosNomina(nombre_arch_salida,
					request);


			/*FSW Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
			archSalida.setTipoNomina(VAL_TIPO_SOLPDF_TR);

			String contenidoArch = archSalida.lecturaArchivoSalidaConsul();
			if (contenidoArch.indexOf("NO EXISTEN DATOS") >= 0
					|| contenidoArch.equals("")
					|| contenidoArch.indexOf("Error ") >= 0
					|| contenidoArch.startsWith("Error")) {
				request.setAttribute("Bitfechas", datesrvr.toString());
				request
						.setAttribute(
								"Encabezado",
								CreaEncabezado(
										CONS_NOMINA,
										CON_NOM_PROG,
										CONST_CANH, request));
				if (contenidoArch.indexOf("NO EXISTEN DATOS") >= 0)
					elMensaje = "NO EXISTEN DATOS PARA LA CONSULTA.";
				else
					elMensaje = "Error en la lectura del archivo, Intente m�s tarde por favor.";
				String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
				request.setAttribute("infoUser", infoUser);
				if (sess.getAttribute("facultadesN") != null)
					request.setAttribute("facultades", sess
							.getAttribute("facultadesN"));
				request.setAttribute("textcuenta", "");
				request.setAttribute("ContenidoArchivo", "");
				evalTemplate(IEnlace.NOMINADATOS_TMPL, request, response);
			} else {
				String arch_exportar = "";
				arch_exportar = session.getUserID8() + "Nom.doc";
				try {
					//String tipoCopia = "2"; // de /t/tuxedo... a /tmp
					ArchivoRemoto archR = new ArchivoRemoto();
					//copiaOtroPathRemoto(String Archivo, String Destino, String pathDestino)
					//JGAL - Copia a ruta de download
					if (!archR.copiaOtroPathRemoto(nombreArchivo, Global.DIRECTORIO_REMOTO_WEB+ "/" + arch_exportar, nueva_ruta))
						EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
				} catch (Exception ioeSua) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
				}

				// Exportar Archivo TXT CSV
				// Enlace Marzo 2015 PYMES 2015

				ExportModel em = new ExportModel("sepfileexp", '|',";", true)
				.addColumn(new Column(1, "Fecha recepci�n"))
				.addColumn(new Column(2, "Fecha de cargo"))
				.addColumn(new Column(3, "Fecha de aplicaci�n"))
				.addColumn(new Column(4, "Hora de aplicaci�n"))
				.addColumn(new Column(5, "Cuenta de cargo"))
				.addColumn(new Column(7, "Nombre del Archivo"))
				.addColumn(new Column(0, "Secuencia"))
				.addColumn(new Column(9, "Num. registros"))
				.addColumn(new Column(8, "Importe aplicado"))
				.addColumn(new Column(6, "Estatus"))
				;

				sess.setAttribute("af", Global.DIRECTORIO_REMOTO_WEB+ "/" + arch_exportar);
				sess.setAttribute("ExportModel", em);
				// Enlace Marzo 2015  PYMES 2015
				// Fin archivo TXT CSV


				request
						.setAttribute(
								"Encabezado",
								CreaEncabezado(
										CONS_NOMINA,
										CON_NOM_PROG,
										CONST_CANH, request));
				request.setAttribute("ContenidoArchivo", contenidoArch);
				request.setAttribute("textcuenta", "");
				request.setAttribute("nombre_arch_salida", nombre_arch_salida);
				String botonestmp = "<br>"
						+ ABRE_TABLE
						+ "<tr><td align=center colspan=7 class=\"tabmovtex\"> <a style=\"cursor: pointer;\" >Exportar en TXT <input id=\"tipoExportacion\" type=\"radio\" value ='txt'   name=\"tipoExportacion\"></a>\n</td></tr>"
						+ "<tr><td align=center colspan=7 class=\"tabmovtex\"> <a style=\"cursor: pointer;\" >Exportar en XLS <input id=\"tipoExportacion\" type=\"radio\" value ='csv' checked name=\"tipoExportacion\"></a>\n<br></td></tr>"
						+ "<tr><td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
						+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
						+ "<td><A href = javascript:exportarArchivoPlano(); ><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>"
						+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
						+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"

						/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
						+SOL_INFORME

						+ "<td></td></tr> </table>";
				request.setAttribute("botones", botonestmp);
				evalTemplate(IEnlace.NOMINADATOS_TMPL, request, response);
			}
		} else {// del Transacci�n Exitosa
			if (elMensaje.equals(""))
				elMensaje = "Servicio no disponible por el momento.";
			request.setAttribute("Bitfechas", datesrvr.toString());
			request.setAttribute("Encabezado", CreaEncabezado(
					CONS_NOMINA,
					CON_NOM_PROG, CONST_CANH,
					request));
			String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
			request.setAttribute("infoUser", infoUser);
			if (sess.getAttribute("facultadesN") != null)
				request.setAttribute("facultades", sess
						.getAttribute("facultadesN"));
			request.setAttribute("textcuenta", "");
			request.setAttribute("ContenidoArchivo", "");
			evalTemplate(IEnlace.NOMINADATOS_TMPL, request, response);
		}
	}

	/**
	 * Genera la trama para la consulta de archivos de nomina a partir de los
	 * parametros seleccionados. Trama:
	 *
	 * <pre>
	 * 1EWEB|&lt;b&gt;empleado&lt;/b&gt;|IN08|&lt;b&gt;no_contrato&lt;/b&gt;|&lt;b&gt;no_empleado&lt;/b&gt;|&lt;b&gt;perfil&lt;/b&gt;|&lt;b&gt;cadena_busqueda&lt;/b&gt;@&lt;b&gt;ruta_archivo_respuesta&lt;/b&gt;@
	 * </pre>
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected String generarTramaConsultaArchivo(HttpServletRequest request,
			HttpServletResponse response) {
		textoLog("generarTramaConsultaArchivo");
		BaseResource baseResource = (BaseResource) request.getSession().getAttribute("session");
		String sessionId = (String) request.getSession().getId();
		EIGlobal.mensajePorTrace(textLog + "sessionId [" + sessionId + "]", EIGlobal.NivelLog.DEBUG);
		String contrato = baseResource.getContractNumber();
		StringBuffer retValue = new StringBuffer("1EWEB|");
		retValue.append(
			baseResource.getUserID8()).append("|IN08|").append(
			baseResource.getContractNumber()).append('|').append(
			baseResource.getUserID8()).append('|').append(
			baseResource.getUserProfile()).append('|');
		String CuentaCargo = (String) getFormParameter(request, "textcuenta");
		String fchaRecepcion = (String) getFormParameter(request, "fecha1"); // formatear la fecha enviar sin diagonales ddmmaaaa
		String fchaAplicacion = (String) getFormParameter(request, "fecha2");
		String importe = (String) getFormParameter(request, "Importe");
		String num_secuencia = (String) getFormParameter(request, "secuencia");
		String ChkRecibido = (String) getFormParameter(request, "ChkRecibido");
		String ChkCancelado = (String) getFormParameter(request, "ChkCancelado");
		String ChkEnviado = (String) getFormParameter(request, "ChkEnviado");
		String ChkPagado = (String) getFormParameter(request, "ChkPagado");
		String ChkCargado = (String) getFormParameter(request, "ChkCargado");
		retValue.append("num_cuenta2='").append(contrato).append("'");
		if (null != CuentaCargo && !CuentaCargo.trim().equals("")) {
			retValue.append(" and cta_cargo='");
			if (10 < CuentaCargo.length()) {
				retValue.append(CuentaCargo.substring(0, 11));
			} else {
				retValue.append(CuentaCargo.trim());
			}
			retValue.append("'");
		}
		if (null != fchaRecepcion && !fchaRecepcion.trim().equals("")) {
			fchaRecepcion = fchaRecepcion.trim();
			retValue.append(" and fch_recep=to_date('");
			if (10 >= fchaRecepcion.length()) {
				fchaRecepcion = fchaRecepcion.substring(0, 2)
						+ fchaRecepcion.substring(3, 5)
						+ fchaRecepcion.substring(6);
			}
			retValue.append(fchaRecepcion.trim()).append("','ddmmyyyy')");
		}
		if (null != fchaAplicacion && !fchaAplicacion.trim().equals("")) {
			fchaAplicacion = fchaAplicacion.trim();
			retValue.append(" and fch_aplic=to_date('");
			if (10 >= fchaAplicacion.length()) {
				fchaAplicacion = fchaAplicacion.substring(0, 2)
						+ fchaAplicacion.substring(3, 5)
						+ fchaAplicacion.substring(6);
			}
			retValue.append(fchaAplicacion.trim()).append("','ddmmyyyy')");
		}
		boolean filterByStatus = false;
		if (null != ChkRecibido && ChkRecibido.startsWith("R")) {
			filterByStatus = true;
			retValue.append("and estatus in ('").append(ChkRecibido.trim())
					.append("'");
		}
		if (null != ChkCancelado && ChkCancelado.startsWith("C")) {
			if (!filterByStatus) {
				retValue.append("and estatus in (");
				filterByStatus = true;
			} else {
				retValue.append(",");
			}
			retValue.append("'").append(ChkCancelado.trim()).append("'");
		}
		if (null != ChkEnviado && ChkEnviado.startsWith("E")) {
			if (!filterByStatus) {
				retValue.append("and estatus in (");
				filterByStatus = true;
			} else {
				retValue.append(",");
			}
			retValue.append("'").append(ChkEnviado.trim()).append("'");
		}
		if (null != ChkPagado && ChkPagado.startsWith("P")) {
			if (!filterByStatus) {
				retValue.append("and estatus in (");
				filterByStatus = true;
			} else {
				retValue.append(",");
			}
			retValue.append("'").append(ChkPagado.trim()).append("'");
		}
		if (null != ChkCargado && ChkCargado.startsWith("A")) {
			if (!filterByStatus) {
				retValue.append("and estatus in (");
				filterByStatus = true;
			} else {
				retValue.append(",");
			}
			retValue.append("'").append(ChkCargado.trim()).append("'");
		}
		if (filterByStatus) {
			retValue.append(")");
		}
		if (null != importe && !importe.trim().equals("")) {
			retValue.append(" and importe_aplic='").append(importe.trim())
					.append("'");
		}
		if (null != num_secuencia && !num_secuencia.trim().equals("")) {
			retValue.append(" and sec_pago='").append(num_secuencia.trim())
					.append("'");
		}
		retValue.append('@').append(
				new java.io.File(Global.DIRECTORIO_REMOTO_INTERNET, request
						.getSession().getId()).getAbsolutePath()).append('@');
		return retValue.toString();
	}

	// Metodo para verificar existencia del Archivo de Sesion
	public boolean verificaArchivos(String arc, boolean eliminar)
			throws ServletException, java.io.IOException {
		textoLog("verificaArchivos");
		EIGlobal.mensajePorTrace(textLog + "Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
		File archivocomp = new File(arc);
		if (archivocomp.exists()) {
			EIGlobal.mensajePorTrace(textLog + "El archivo si existe.", EIGlobal.NivelLog.DEBUG);
			if (eliminar) {
				archivocomp.delete();
				EIGlobal.mensajePorTrace(textLog + "El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
			}
			return true;
		}
		EIGlobal
				.mensajePorTrace("El archivo no existe.", EIGlobal.NivelLog.DEBUG);
		return false;
	}

	public void guardaParametrosEnSession(HttpServletRequest request) {
		textoLog("guardaParametrosEnSession");
		EIGlobal.mensajePorTrace(textLog + "Dentro de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
		request.getSession().setAttribute("plantilla", request.getServletPath());

		HttpSession session = request.getSession(false);
		session.removeAttribute("bitacora");

		if (session != null) {
			Map tmp = new HashMap();

			EIGlobal.mensajePorTrace(textLog + "valida-> " + (getFormParameter(request, "valida") == null ? request.getAttribute("valida") : getFormParameter(request, "valida") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "operacion-> " + (getFormParameter(request, "operacion") == null ? request.getAttribute("operacion") : getFormParameter(request, "operacion") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "registro-> " + (getFormParameter(request, "registro") == null ? request.getAttribute("registro") : getFormParameter(request, "registro") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "tipoArchivo-> " + (getFormParameter(request, "tipoArchivo") == null ? request.getAttribute("tipoArchivo") : getFormParameter(request, "tipoArchivo") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "statusDuplicado-> " + (getFormParameter(request, "statusDuplicado") == null ? request.getAttribute("statusDuplicado") : getFormParameter(request, "statusDuplicado") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "statushrc-> " + (getFormParameter(request, "statushrc") == null ? request.getAttribute("statushrc") : getFormParameter(request, "statushrc") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "encFechApli-> " + (getFormParameter(request, "encFechApli") == null ? request.getAttribute("encFechApli") : getFormParameter(request, "encFechApli") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "horario_seleccionado-> " + (getFormParameter(request, "horario_seleccionado") == null ? request.getAttribute("horario_seleccionado") : getFormParameter(request, "horario_seleccionado") ), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "Concepto-> " + (getFormParameter(request, "Concepto") == null ? request.getAttribute("Concepto") : getFormParameter(request, "Concepto") ), EIGlobal.NivelLog.DEBUG);
			String value;
			value = (String) getFormParameter(request, "valida");
			if (value != null && !value.equals(null)) {
				tmp.put("valida", getFormParameter(request, "valida"));
			}

			tmp.put("operacion", getFormParameter(request, "operacion"));
			tmp.put("registro", getFormParameter(request, "registro"));
			tmp.put("tipoArchivo", getFormParameter(request, "tipoArchivo"));
			tmp.put("statusDuplicado", getFormParameter(request, "statusDuplicado"));
			tmp.put("statushrc", getFormParameter(request, "statushrc"));
			tmp.put("encFechApli", getFormParameter(request, "encFechApli"));
			tmp.put("horario_seleccionado", getFormParameter(request, "horario_seleccionado"));
			tmp.put("Concepto", getFormParameter(request, "Concepto"));
			tmp.put("folio", getFormParameter(request, "folio"));

			//Favoritos
			EIGlobal.mensajePorTrace(textLog + "favorito desc-> " + getFormParameter(request, "txtFav"), EIGlobal.NivelLog.DEBUG);
			tmp.put("strCheck", getFormParameter(request, "strCheck"));
			tmp.put("txtFav", getFormParameter(request, "txtFav"));


			session.setAttribute("parametros", tmp);

			tmp = new HashMap();
			Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
		EIGlobal.mensajePorTrace(textLog + "Saliendo de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
	}

	/** ************* Nomina Concepto de Archvo */
	boolean actualizaConceptoEnArchivo(String nombreArchivo, String Concepto) {
		textoLog("actualizaConceptoEnArchivo");
		String line = "";
		BufferedReader arc;
		BufferedWriter arcWrite;
		String nombreArchivoEscritura = nombreArchivo + ".nom";

		try {
			// Se le quita el path al archivo para operar con el, en el
			// directorio temporal del app
			EIGlobal.mensajePorTrace(textLog + "El archivo origen: " + nombreArchivo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textLog + "El archivo destino: " + nombreArchivoEscritura, EIGlobal.NivelLog.DEBUG);

			arc = new BufferedReader(new FileReader(nombreArchivo));
			arcWrite = new BufferedWriter(
					new FileWriter(nombreArchivoEscritura));

			while ((line = arc.readLine()) != null) {
				if (line.length() > 126)
					line += Concepto;
				arcWrite.write(line + "\n");
			}

			File a = new File(nombreArchivo);
			File b = new File(nombreArchivoEscritura);
			a.delete();
			b.renameTo(new File(nombreArchivo));

			try {
				arc.close();
				arcWrite.close();
			} catch (IOException d) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(d), EIGlobal.NivelLog.INFO);
			}
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			return false;
		}
		return true;
	}

	/** ************* Nomina Concepto de Archvo */

	// Servicio Tuxedo para generar folio
	public String generaFolio(String usuario, String perfil, String contrato,
			String servicio, String registros) {
		String result = "";
		String codError = "";
		String folioArchivo = "";
		String tramaParaFolio = usuario + "|" + perfil + "|" + contrato + "|"
				+ servicio + "|" + registros + "|";

		ServicioTux tuxGlobal = new ServicioTux();
		try {
			Hashtable hs = tuxGlobal.alta_folio(tramaParaFolio);
			result = (String) hs.get("BUFFER") + "";
		} catch (Exception e) {
			//re.printStackTrace();
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNomina::generaFolio:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("<DATOS GENERALES>", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("OpcionesNomina::generaFolio::"
						                + "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
						                + e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}

		if (result == null || result.equals(CAD_NULL))
			result = "ERROR            Error en el servicio.";

		EIGlobal.mensajePorTrace("Respuesta de BUFFER en generaFolio ->" + result, EIGlobal.NivelLog.INFO);
		if (result.contains("|")) {
			codError = result.substring(0, result.indexOf("|"));
		}

		if (codError.trim().equals("TRSV0000")) {
			folioArchivo = result.substring(result.indexOf("|") + 1, result
					.length());
			folioArchivo = folioArchivo.trim();
			if (folioArchivo.length() < 12) {
				do {
					folioArchivo = "0" + folioArchivo;
				} while (folioArchivo.length() < 12);
			}
		}
		return folioArchivo;
	}

	public String envioDatos(String usuario, String perfil, String contrato,
			String folio, String servicio, String ruta, String trama) {
		textoLog("envioDatos");
		String result = "";
		String codError = "";
		String mensajeErrorEnviar = "";
		String tramaParaEnviar = usuario + "|" + perfil + "|" + contrato + "|"
				+ folio + "|" + servicio + "|" + ruta + "_preprocesar" + "|"
				+ ruta + "|" + trama;

		ServicioTux tuxGlobal = new ServicioTux();

		try {
			Hashtable hs = tuxGlobal.envia_datos(tramaParaEnviar);
			result = (String) hs.get("BUFFER") + "";
		} catch (Exception e) {
			//re.printStackTrace();
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNomina::envioDatos:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("<DATOS GENERALES>", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("OpcionesNomina::envioDatos::"
						                + "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
						                + e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}

		if (result == null || result.equals(CAD_NULL))
			result = "ERROR            Error en el servicio.";

		EIGlobal.mensajePorTrace(textLog + "CONTROLADOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("Respuesta de BUFFER en envioDatos ->" + result, EIGlobal.NivelLog.INFO);
		if (result.contains("|")) {
			codError = result.substring(0, result.indexOf("|"));
		}

		if (!codError.trim().equals("TRSV0000"))
			EIGlobal.mensajePorTrace(textLog + "No se enviaron los registros al controlador de registros Mensaje: " + mensajeErrorEnviar, EIGlobal.NivelLog.DEBUG);
		else
			EIGlobal.mensajePorTrace(textLog + "registros enviados al controlador de registros Mensaje: " + mensajeErrorEnviar, EIGlobal.NivelLog.DEBUG);

		mensajeErrorEnviar = result.substring(result.indexOf("|") + 1, result.length());

		return mensajeErrorEnviar;
	}

	public String realizaDispersion(String usuario, String perfil,
			String contrato, String folio, String servicio) {
		textoLog("realizaDispersion");
		String result = "";
		String codError = "";
		String mensajeErrorDispersor = "";
		String tramaParaDispersor = usuario + "|" + perfil + "|" + contrato
				+ "|" + folio + "|" + servicio + "|";

		ServicioTux tuxGlobal = new ServicioTux();

		try {
			Hashtable hs = tuxGlobal.inicia_proceso(tramaParaDispersor);
			result = (String) hs.get("BUFFER");
		} catch (Exception e) {
			//re.printStackTrace();
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNomina::realizaDispersion:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("<DATOS GENERALES>", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("OpcionesNomina::realizaDispersion::"
						                + "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
						                + e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}

		if (result == null || result.equals(CAD_NULL))
			result = "ERROR            Error en el servicio.";

		EIGlobal.mensajePorTrace(textLog + "Dispersor de registros: Trama salida: " + result, EIGlobal.NivelLog.INFO);

		try{
			codError = result.substring(0, result.indexOf("|"));
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			codError = "";
		}

		if (!codError.trim().equals("TRSV0000")) {
			EIGlobal.mensajePorTrace(textLog + "No se realizo la dispersion de registros", EIGlobal.NivelLog.INFO);
		} else {

			EIGlobal.mensajePorTrace(textLog + "Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.INFO);
		}

		mensajeErrorDispersor = result.substring(result.indexOf("|") + 1,
				result.length());
		return mensajeErrorDispersor;
	}


	private String ejecutaServicio(String trama) {
		textoLog("ejecutaServicio");
		Hashtable htResult = null;

		ServicioTux tuxGlobal = new ServicioTux();
		String tramaSalida= "";

		try {
			htResult = tuxGlobal.web_red(trama);
			tramaSalida = (String) htResult.get("BUFFER");
			} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			tramaSalida = "";
			}

		EIGlobal.mensajePorTrace(textLog + "tramaSalida: [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);
		return tramaSalida;
	}

	public String generaTablaTotales(String cuenta, String descripcion, String fchAplicacion, String importe, String registros){
		String bgcolor = "textabdatobs";

		StringBuffer tablaTotales = new StringBuffer();
		tablaTotales.append("<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		tablaTotales.append("<tr> ");
		tablaTotales.append("<td class=\"textabref\">Pago de n&oacute;mina</td>");
		tablaTotales.append("</tr>");
		tablaTotales.append("</table>");
		tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
		tablaTotales.append("<tr> ");
		tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
		tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descipci&oacute;n</td>");
		tablaTotales.append("<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci�n</td>");
		tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
		tablaTotales.append("<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
		tablaTotales.append("</tr>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(cuenta);
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(descripcion);
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(fchAplicacion);
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(formatoMoneda(importe));
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(registros);
		tablaTotales.append("&nbsp;</td>" + "</table><br>");

		return tablaTotales.toString();
	}

	/**
	 * Funcion de registro de solicitudes de informe PDF
	 * @author FSW Vector
	 * @param request peticion
	 * @param response respuesta
	 * @return String cadena
	 * @throws ServletException excepcion a manejar
	 * @throws java.io.IOException excepcion a manejar
	 *
	 */
	public String solicitaInformePDF(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	java.io.IOException{

		StringBuffer mensaje = new StringBuffer("");
		final HttpSession sess = request.getSession(false);
		final BaseResource session = (BaseResource) sess.getAttribute("session");
		NominaBO bo = null;
		SolPDFBean pdfBean = null;
		Date fechaRecep;
		SimpleDateFormat sdf;
        boolean resultado = false;
        String secuencia = "";
        String strFch = "";
        String codError = "";
        int tipoMensaje = 0;

        HashMap <Integer,SolPDFBean> solicitudesPDF =  null;


		EIGlobal.mensajePorTrace("OpcionesNomina.java :: solicitaInformePDF :: Entrando", EIGlobal.NivelLog.INFO);

		try{

			String []arrDatos = null; //Se desentraman datos para secuencia
			String strDatos = (String) getFormParameter(request, PARAM_DATOS_TR);
			EIGlobal.mensajePorTrace("OpcionesNomina.java :: solicitaInformePDF :: Procesando Datos [" + strDatos + "]" , EIGlobal.NivelLog.INFO);
			if(strDatos != null && !"".equals(strDatos)){
				arrDatos = strDatos.split("[@]");
				if(arrDatos != null){
					secuencia = arrDatos[1].trim();
					strFch = arrDatos[2].trim();
					EIGlobal.mensajePorTrace("OpcionesNomina.java :: solicitaInformePDF ::Secuencia [" + secuencia + "]" , EIGlobal.NivelLog.DEBUG);

					if(session != null){
						pdfBean = new SolPDFBean();
						bo = new NominaBO();

						sdf = new SimpleDateFormat(FOR_FECHA_DMA);
						fechaRecep = sdf.parse(strFch);
						pdfBean.setContrato(session.getContractNumber());
						pdfBean.setUsuarioAlta(session.getUserID8());
						pdfBean.setSecuencia(Integer.parseInt(secuencia));
						pdfBean.setEstatus(VAL_ESTAT_SOLPDF_SOL);
						pdfBean.setTipoNomina(VAL_TIPO_SOLPDF_TR);
						pdfBean.setFchPago(fechaRecep);
						pdfBean.setTipoOper(BIT_OPER_SOLPDF_TRAD);

						resultado = bo.registraSolcitudPDF(request,pdfBean);
						String auxMensaje = "";
						if(resultado) {
							tipoMensaje = 1;
							solicitudesPDF = (HashMap <Integer,SolPDFBean>) sess.getAttribute(VAR_SES_SOl_PDF);

							//Se registra la solicitud de informe en PDF del archivo seleccionado

							if(solicitudesPDF != null){
								solicitudesPDF.put( pdfBean.getSecuencia(),pdfBean);
								EIGlobal.mensajePorTrace("OpcionesNomina.java :: SolicitaInformePDF :: Numero de soliciutdes " + solicitudesPDF.size(), EIGlobal.NivelLog.DEBUG);
								sess.removeAttribute(VAR_SES_SOl_PDF);
								sess.setAttribute(VAR_SES_SOl_PDF, solicitudesPDF);
							}
							auxMensaje = pdfBean.getMensaje();


						}else {
							tipoMensaje = 10;
							auxMensaje = pdfBean.getMensaje();

						}
						mensaje = new StringBuffer(VAL_FUNCION_MENSAJE).append( "\"").append( MSG_ENCAB_SOLPDF ).append( "\",\"" ).append( auxMensaje ).append( "\",").append( tipoMensaje ).append(");");

						request.setAttribute(VAL_VAR_MENSAJE, mensaje.toString());

						codError = BIT_OPER_SOLPDF_TRAD + pdfBean.getCodError();
						sdf = null;
					}
				}

			}else{
				mensaje = new StringBuffer(VAL_FUNCION_MENSAJE).append( "\"" ).append( MSG_ENCAB_SOLPDF ).append( "\",\"" ).append( MSG_SOLPDF_ERROR_PARAM ).append( "\",10);");
				request.setAttribute(VAL_VAR_MENSAJE, mensaje.toString());
			}


		}catch(Exception e){
			mensaje = new StringBuffer(VAL_FUNCION_MENSAJE ).append( "\"" ).append( MSG_ENCAB_SOLPDF ).append( "\",\"" ).append( MSG_SOLPDF_ERROR_EXCEP ).append( "\",10);");
			request.setAttribute(VAL_VAR_MENSAJE, mensaje.toString());
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}

		EIGlobal.mensajePorTrace("OpcionesNomina.java :: Saliendo solicitaInformePDF", EIGlobal.NivelLog.INFO);
		return codError;
	}
	/**
	 * Metodo para descargar informe pdf
	 * @since 14/05/2015
	 * @param request peticion
	 * @param response respuesta
	 * @return String valor
	 * @throws ServletException excepcion a manejar
	 * @throws java.io.IOException excepcion a manejar
	 */
	public String descargaInformePDF(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	java.io.IOException{
		String codError ="";
		StringBuffer mensaje = new StringBuffer("");
		final HttpSession sess = request.getSession(false);
		final BaseResource session = (BaseResource) sess.getAttribute("session");
		String secuencia = "";
		String strFch = "";
		String urlDescarga = "";
		CuentasDescargaBean descargaBean = null;
		DescargaEstadoCuentaBO descargaEstadoCuentaBO = null;
		NominaBO nominaBO = null;
		boolean registroDescarga = false;
		SimpleDateFormat sdf = null;
		Date  fechaRecep = null;
		String fchDescarga = "";


			String []arrDatos = null; //Se desentraman datos para secuencia
			String strDatos = (String) getFormParameter(request, "secDescarga");


			EIGlobal.mensajePorTrace("OpcionesNomina.java :: solicitaInformePDF :: Procesando Datos [" + secuencia + "]" ,
					                  EIGlobal.NivelLog.INFO);

			if(strDatos != null && !"".equals(strDatos)){

				arrDatos = strDatos.split("[@]");

				if(arrDatos != null ){
					secuencia = arrDatos[0].trim();
					strFch = arrDatos[2];
					EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF ::Secuencia [" + secuencia + "]" ,
							EIGlobal.NivelLog.DEBUG);

					EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF ::Secuencia [" + secuencia + "]" ,
							EIGlobal.NivelLog.DEBUG);

					if(session != null){

						sdf = new SimpleDateFormat(FOR_FECHA_DMA);
						try {
							fechaRecep = sdf.parse(strFch);
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							EIGlobal.mensajePorTrace(e1.getMessage(), EIGlobal.NivelLog.ERROR);
						}
						sdf = null;

						sdf = new SimpleDateFormat("MMddyy");//formato para descarga de Ondemand
						fchDescarga = sdf.format(fechaRecep);

						descargaEstadoCuentaBO=new DescargaEstadoCuentaBO();
						nominaBO = new NominaBO();
						descargaBean = new CuentasDescargaBean();
						descargaBean.setLlaveDescarga(descargaEstadoCuentaBO.genKey());
						descargaBean.setCodigoCliente(session.getUserID8());
						descargaBean.setCuenta(session.getContractNumber());
						descargaBean.setTipo("N");
						descargaBean.setTipoEdoCta(VAL_TIPO_SOLPDF_TR);
						descargaBean.setPais("MX");
						descargaBean.setPeriodoSeleccionado(fchDescarga);
						descargaBean.setFolioSeleccionado(secuencia);

						try{
							registroDescarga = nominaBO.insertaRegistroDescarga(descargaBean);
						}catch(BusinessException e){
							EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
							codError = "ERROR";
						}

						if(registroDescarga){
							urlDescarga = "../../../EdoEnlace/edoCtaEnlace/descargaInformeNom.do?cadena="+descargaBean.getLlaveDescarga();
							request.setAttribute("URLDescargaPDF", urlDescarga);
						}else {
							mensaje = new StringBuffer( VAL_FUNCION_MENSAJE ).append( "\"" ).append(  MSG_ENCAB_SOLPDF ).append( "\",\"" ).append( "Error al Crear Registro en Tabla para Descarga " ).append( "\",10);");// ACEPTAR, CANCELAR
							request.setAttribute(VAL_VAR_MENSAJE, mensaje.toString());
						}
					}
				}
			}
			return codError;
	}

	public void redirigeRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException,
	java.io.IOException{

		final HttpSession sess = request.getSession(false);
		BaseResource session = (BaseResource) sess.getAttribute("session");
		ArchivosNomina archSalida1 = null;
		String contenidoArchivo = "";
		String arch_exportar = "";

		String arch_salida_consulta = (String) getFormParameter(request, "nombre_arch_salida");
		String lista_nombres_archivos = (String) getFormParameter(request, "lista_nombres_archivos");

		arch_salida_consulta = arch_salida_consulta.trim();

		archSalida1 = new ArchivosNomina(arch_salida_consulta, request);
		archSalida1.setTipoNomina(VAL_TIPO_SOLPDF_TR);



		contenidoArchivo = archSalida1.lecturaArchivoSalidaConsul(lista_nombres_archivos);

		request
		.setAttribute(
				"Encabezado",
				CreaEncabezado(
						CONS_NOMINA,
						CON_NOM_PROG,
						CONST_CANH, request));

		request.setAttribute("ContenidoArchivo", contenidoArchivo);
		request.setAttribute("nombre_arch_salida", arch_salida_consulta);
		if (sess.getAttribute("facultadesN") != null)
			request.setAttribute("facultades", sess.getAttribute("facultadesN"));
		request.setAttribute("lista_nombres_archivos", lista_nombres_archivos);
		arch_exportar = session.getUserID8() + "Nom.doc";

		String botonestmp = "<br>"
				+ ABRE_TABLE
				+ EXPORTAR_TXT
				+ EXPORTAR_XLS
				+ ABRE_TR
				+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
				+ // Exportacion de archivo.
				"<td><A href = \"/Download/"
				+ arch_exportar
				+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>"
				+ // Ya no se enviara el detalle por
					// correo ...
				// pues siempre si ...
				"<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
				+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
				+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"

				/*Vector - Tactico Epyme - Generacion de comprobantes de Nomina*/
				+SOL_INFORME

				+ "<td></td></tr> </table>";
		request.setAttribute("botones", botonestmp);
		evalTemplate(IEnlace.NOMINADATOS_TMPL, request,
				response);
	}

	/**
	 * validaTerciaBiatux lee archivo  para validar la tercia.
	 * @param bean datos de entrada
	 */
	public NominaArchBean validaTerciaBiatux(NominaArchBean bean) {
		BufferedReader arc;
		String archivo=Global.directorioConfignln+"/config.cfg";
		try {
			EIGlobal.mensajePorTrace(textLog + "Archivo::::  " + archivo, EIGlobal.NivelLog.DEBUG);
			arc = new BufferedReader(new FileReader(archivo));
			String linea=arc.readLine();
			linea=linea != null ? linea : "";
			EIGlobal.mensajePorTrace("OpcionesNomina.java :: linea : "+linea, EIGlobal.NivelLog.INFO);
			bean.setBiatuxOrigen(linea.trim());
			EIGlobal.mensajePorTrace("OpcionesNomina.java :: servidorBiatux = "+bean.getBiatuxOrigen(), EIGlobal.NivelLog.INFO);
			arc.close();
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("OpcionesNomina.java :: el archivo " + archivo + " no existe .:.:.", EIGlobal.NivelLog.INFO);
			bean.setBiatuxOrigen(" ");
			EIGlobal.mensajePorTrace("OpcionesNomina.java :: se pone por default en el equipo biatux el valor de '" + bean.getBiatuxOrigen() + "' .:.:. ", EIGlobal.NivelLog.INFO);
		}
		return bean;
	}
}