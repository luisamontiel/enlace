//trama csaldos 1EWEB|0006980|SDCT|80000000161|0006980|0006980|9990674111|P|
package mx.altec.enlace.servlets;

import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.*;

import java.rmi.*;
import javax.sql.*;
import java.sql.*;
import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.TrxGP93VO;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.CatDivisaPais;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

//VSWF
public class MTI_Inicio extends mx.altec.enlace.servlets.BaseServlet
{

	String pipe="|";

//STFQRO 31012011	String tcvHora="";
	String tcvCompra="";
	String tcvVenta="";
	boolean tranExito = false; //STFQRO 31012011 Si el GPA0 trae info. se vuelve true

//=================================================================================================
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
//=================================================================================================
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
//=================================================================================================
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{

	String cadenaFac = "";
	String cadenaTCT = "";
	String arcLinea = "";
	String tipoCambioVenta = "";
	boolean  facAccesoPant;

	/*STFQRO 31012011 INI*/
	ServicioTux tuxGlobal = new ServicioTux();
	Integer referencia = 0;
	String codError = "";
	/*STFQRO 31012011 FIN*/
	//tcvHora="";
	tcvCompra="";
	tcvVenta="";

	/************* Modificacion para la sesion ***************/
	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");
	EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
	
	req.getSession().removeAttribute("montoMNreal");
	req.getSession().removeAttribute("montoUSDreal");
	req.getSession().removeAttribute("montoDivisareal");
	req.getSession().removeAttribute("CveEspecial");
	req.getSession().removeAttribute("tipoEspecial");
	req.getSession().removeAttribute("inicialCveDivisa");
	req.getSession().removeAttribute("catCvePais");
    EIGlobal.mensajePorTrace("MTI_Inicio - execute(): Entrando a Trasnferencias Internacionales.", EIGlobal.NivelLog.INFO);

	boolean sesionvalida = SesionValida ( req, res );

	if(session.getFacultad ("ABOCTAINTERN") )
	{

	  if(sesionvalida)
 	   {
           facAccesoPant =	session.getFacultad(session.FAC_TRANSF_INTERNAC);
		   EIGlobal.mensajePorTrace("Facultad de Acceso a Pantalla: "+facAccesoPant, EIGlobal.NivelLog.INFO);

		   session.setModuloConsultar(IEnlace.MCargo_TI_pes_dolar);
		   
			 /*STFQRO 31012011*/
			 try {
				// Se llama al servicio para sacar REFERENCIA
				Hashtable hs = tuxGlobal.sreferencia("901");
				EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",	EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hs.toString(), EIGlobal.NivelLog.INFO);							
				codError = hs.get("COD_ERROR").toString();
				
				// VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
				if (codError.endsWith("0000")) {
					referencia = (Integer) hs.get("REFERENCIA");
					// Obtienes el tipo de cambio por medio de la TRX GPAO
					this.tipoCambioVentanilla();
					if(tranExito)
						codError = "DIIN0000";
					else
						codError = "DIIN9999";
				}

				
			} catch (Exception e1) {
				EIGlobal.mensajePorTrace("ERROR : "
						+ e1.getMessage(), EIGlobal.NivelLog.ERROR);
			}
			/*STFQRO 31012011 FIN*/
		   //tipoCambioVentanilla();


		   //TODO BIT CU3131 Inicio de flujo transferencias internacionales modulo de Tesorer�a
		   //Para detectar si el tipo de cambio es del d�a.
		   //TODO BIT CU2101, Inicio de flujo Transferencias Internacionales
			/**
			 * VSWF}
			 * 15/Enero/2007	Modificaci�n para el modulo de Tesorer�a
			 */
		   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try {
				BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				bh.incrementaFolioFlujo(req.getParameter(BitaConstants.FLUJO));
				BitaTransacBean bt = new BitaTransacBean();
				BitaTCTBean beanTCT = new BitaTCTBean ();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
					bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_ENTRA);
				}
				if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
					bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_ENTRA);
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if (tcvVenta != null && !tcvVenta.equals("")) {
					bt.setTipoCambio(Double.parseDouble(tcvVenta.trim()));
				}

				BitaHandler.getInstance().insertBitaTransac(bt);
				//INI BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
				beanTCT = bh.llenarBeanTCT(beanTCT);
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion("DICO");
				beanTCT.setCodError(codError);
				beanTCT.setUsuario(session.getUserID8());
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);
				//FIN BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
		   }
			/*
			 * VSWF
			 */
		   //+ EnlaceGlobal.fechaHoy("aaaammdd")+"<br>"+tcvHora
		   /**
		   if(tcvHora!=null)
			{
		      if(tcvHora.indexOf(EnlaceGlobal.fechaHoy("aaaammdd"))<0)
			    req.setAttribute("TipoCambioError","El tipo de cambio no est� actualizado.");
		      else
			    req.setAttribute("TipoCambioError","");
			}
		   else
			 req.setAttribute("TipoCambioError","No se pudo obtener la informaci�n del tipo de cambio.");**/
		   
		   /*STFQRO 31012011 INI */			   
		   if(tranExito==false)
			 req.setAttribute("TipoCambioError","No se pudo obtener la informaci�n del tipo de cambio.");
		   else
			 req.setAttribute("TipoCambioError","");
		   /*STFQRO 31012011 FIN */

		   EIGlobal.mensajePorTrace("MTI_Inicio - execute(): Enviando informacion al jsp.", EIGlobal.NivelLog.INFO);

		   req.setAttribute("TiempoTerminado","");
		   req.setAttribute("TCV_Venta",tcvVenta.trim());
		   req.setAttribute("TCV_Compra",tcvCompra.trim());
		   req.setAttribute("Divisas",traeDivisas());

		   req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		   req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		   req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		   req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

		   req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
		   req.setAttribute("MenuPrincipal", session.getStrMenu());
		   req.setAttribute("newMenu", session.getFuncionesDeMenu());
		   req.setAttribute("Encabezado",CreaEncabezado("Transferencias Internacionales","Transferencias &gt; Internacionales","s27540h",req));

		   evalTemplate( "/jsp/MTI_Inicio.jsp", req, res );
       }
	      else
        if(sesionvalida)
	    {
           req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
		   evalTemplate( IEnlace.ERROR_TMPL, req, res );
		    }
    }
   else
	{
  	   String laDireccion = "document.location='SinFacultades'";
	   req.setAttribute( "plantillaElegida", laDireccion);
	   req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
       facAccesoPant =	session.getFacultad(session.FAC_TRANSF_INTERNAC);
	   EIGlobal.mensajePorTrace("Facultad de Acceso a Pantalla: " +facAccesoPant, EIGlobal.NivelLog.INFO);
    }


  EIGlobal.mensajePorTrace("MTI_Inicio - execute(): Saliendo de Transferencias Internacionales.", EIGlobal.NivelLog.INFO);
}	

/******************************************************************************/
/** trae Divisas **************************************************************/
/******************************************************************************/
public String traeDivisas() {
	String strOpcion="";
	Vector divisas = null;
	EIGlobal.mensajePorTrace("MTI_Inicio::traeDivisas:: Iniciando extracci�n de divisas...", EIGlobal.NivelLog.DEBUG);
	
	try{
		divisas = CatDivisaPais.getInstance().getListaDivisas();	    
    	for(int i=0; i<divisas.size(); i++){		   
    		strOpcion += "<option CLASS='tabmovtex' value='" + ((TrxGP93VO)divisas.get(i)).getOVarCod().trim() + "'>" 
    		          +  ((TrxGP93VO)divisas.get(i)).getOVarDes().trim() + "\n";
    	}
    	EIGlobal.mensajePorTrace("MTI_Inicio::traeDivisas:: saliendo de la extracci�n de divisas...", EIGlobal.NivelLog.DEBUG);
	}catch(Exception e){
		StackTraceElement[] lineaError;
		lineaError = e.getStackTrace();
		EIGlobal.mensajePorTrace("Error al realizar la extracci�n de divisas por la GP93.", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MTI_Inicio::traeDivisas:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
								+ e.getMessage()
	               				+ "<DATOS GENERALES>"
					 			+ "Linea encontrada->" + lineaError[0]
					 			, EIGlobal.NivelLog.DEBUG);
	}
	
	
	/**
	String sqlDivisa="";
	PreparedStatement DivisaQuery = null;
	ResultSet DivisaResult = null;
	Connection conn = null;

	try {
		conn = createiASConn(Global.DATASOURCE_ORACLE);
		if(conn==null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
			return "";
		}

		sqlDivisa="SELECT CVE_DIVISA, UPPER(DESCRIPCION) FROM COMU_MON_DIVISA ORDER BY UPPER(DESCRIPCION)";
		DivisaQuery  = conn.prepareStatement(sqlDivisa);

		if(DivisaQuery==null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
			conn.close();
			return "";
		}

		DivisaResult = DivisaQuery.executeQuery();
		if(DivisaResult==null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
			conn.close();
			try {
				DivisaQuery.close();
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("DivisaResult nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}
			return "";
		}
	 
		while(DivisaResult.next())
			strOpcion += "<option CLASS='tabmovtex' value='" + DivisaResult.getString(1) + "'>" + DivisaResult.getString(2) + "\n";
	}
	catch( SQLException sqle ) {
		EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas(): Excepcion, Error... "+sqle.getMessage(), EIGlobal.NivelLog.INFO);
		return "";
	}
	finally {
		try {
			DivisaQuery.close();
			DivisaResult.close();
			conn.close();
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace("MTI_Inicio - traeDivisas() en finally", EIGlobal.NivelLog.INFO);
		}
	}**/
	return strOpcion;
}

/******************************************************************************/
/** trae Divisas **************************************************************/
/******************************************************************************/
private void tipoCambioVentanilla() {

	TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();
//STFQRO	String  query_realizar="";
//STFQRO	Connection connbd = null;
//STFQRO	PreparedStatement TestQuery = null;
//STFQRO	ResultSet TestResult = null;

	EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): Entrando a tipoCambioVentanilla", EIGlobal.NivelLog.INFO);

	/*STFQRO 31012011 INI GPA0*/
	try {
		tcvCompra = tipoCambio.getCambioSugerido("CPA","TRAN");
		tcvVenta  = tipoCambio.getCambioSugerido("VTA","TRAN");
/*		
	try {
		connbd = createiASConn(Global.DATASOURCE_ORACLE);
		if(connbd == null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
			return;
		}*/
		//Verificando si los datos son los esperados, sino ocurre la excepcion  
		Float.parseFloat(tcvCompra);
		Float.parseFloat(tcvVenta);
		tranExito = true;
/*STFQRO 31012011		query_realizar="SELECT TO_CHAR(fecha,'YYYYMMDD hh24:mi:ss'),to_char(tc_cpa_vent,'999,999,999.000000'),to_char(tc_vta_vent,'999,999,999.000000')";
		query_realizar=query_realizar + " from comu_tc_base ";
		query_realizar=query_realizar + " WHERE cve_divisa = 'USD' ";
		query_realizar=query_realizar + " AND fecha = (select MAX(fecha) from comu_tc_base WHERE cve_divisa = 'USD')";

		EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): Query: "+query_realizar, EIGlobal.NivelLog.ERROR);
		TestQuery = connbd.prepareStatement(query_realizar);

		if(TestQuery == null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): Cannot Create Query", EIGlobal.NivelLog.ERROR);
			connbd.close();
			return;
		}

		TestResult = TestQuery.executeQuery();
		if(TestResult == null) {
			EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
			connbd.close();
			try {
				TestQuery.close();
				connbd.close();STFQRO 31012011*/
			}catch(Exception e){
//		EIGlobal.mensajePorTrace("TestResult nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);	
		EIGlobal.mensajePorTrace("Error al tratar de obtener el tipo de cambio"+e.getMessage(), EIGlobal.NivelLog.ERROR);
		tcvCompra="";
		tcvVenta="";
		tranExito = false;
	}
	/*STFQRO 31012011 FINGPA0*/			

/*STFQRO 31012011 INI			}
			return;
		}

		while(TestResult.next()) {
			tcvHora=(TestResult.getString(1)==null)?"":TestResult.getString(1);
			tcvCompra=(TestResult.getString(2)==null)?"":TestResult.getString(2);
			tcvVenta=(TestResult.getString(3)==null)?"":TestResult.getString(3);
		}

		EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): HORA   = >" +tcvHora+ "<", EIGlobal.NivelLog.INFO);
STFQRO 31012011 */		
		EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): COMPRA = >" +tcvCompra+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla(): VENTA  = >" +tcvVenta+ "<", EIGlobal.NivelLog.INFO);
/*	}
	catch( SQLException sqle ) {
		EIGlobal.mensajePorTrace("MTI_Inicio tipoCambioVentanilla()- : Excepcion, Error... "+sqle.getMessage(), EIGlobal.NivelLog.INFO);
	}
	finally {
		try {
			TestQuery.close();
			TestResult.close();
			connbd.close();
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace("MTI_Inicio - tipoCambioVentanilla() en finally", EIGlobal.NivelLog.INFO);
		}
	}STFQRO 31012011 FIN*/
	return;
}

}