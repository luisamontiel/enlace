package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EmailCelularBean;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailCelularDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class ConfigMedioNot extends BaseServlet {

	/**
	 * Numero de versi�n serial
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Opci�n para cargar
	 */
	public static final int ACCION_CARGAR = 0;
	/**
	 * Opci�n para la modificar
	 */
	public static final int ACCION_MODIFICA = 1;
	/**
	 * Opci�n para cargar, si la llamada viene
	 * despues de seleccionar contrato
	 */
	public static final int ACCION_CARGAR_INICIAL = 2;

	/**
	 * Metodo encargado de iniciar la actividad de captura cuando el form action
	 * es el metodo Get
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 * @throws ServletException		Error de tipo ServletException
	 * @throws IOException			Error de tipo IOException
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		inicioServletConfigMedioNot(req, res);
	}

	/**
	 * Metodo encargado de iniciar la actividad de captura cuando el form action
	 * es el metodo Post
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 * @throws ServletException		Error de tipo ServletException
	 * @throws IOException			Error de tipo IOException
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		inicioServletConfigMedioNot(req, res);
	}

	/**
	 * Metodo general donde iniciara la acci�n del servlet
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 */


	private void inicioServletConfigMedioNot(
			HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {

		HashMap beanMap = new HashMap();
		Integer opcion = 0;
        EmailCelularBean emailCelularBean = new EmailCelularBean();
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String[] statusActualizacion = {"",""};
		String valida = req.getParameter("valida");

		boolean sesionvalida = SesionValida(req, res);
		//Validando la session
        if(!sesionvalida){
        	return;
        }

        if (validaPeticion(req, res, session, sess, valida)) {
			EIGlobal.mensajePorTrace("Entro a validar Peticion", EIGlobal.NivelLog.DEBUG);
		} else {

		    if (/*verificaFacultad("CAMMEDNOTSUP", req) && */session.getMensajeUSR().isSuperUsuario()) {
		    	beanMap = cargaParametros(emailCelularBean,req);

		    	if (req.getParameter("valida") != null && req.getParameter("valida").equals("1")) {
		    		opcion = 1;
		    	} else {
		    		opcion = new Integer((String)beanMap.get("opcion"));
			        EIGlobal.mensajePorTrace("ConfiguraMedioNotificacion:opcion:" + opcion, EIGlobal.NivelLog.INFO);
		    	}

				/**
				 * Opci�n cargar: si la llamada viene despues de seleccionar contrato, esta obligando
				 * al usuario a actualizar los datos
				 */
				if(opcion.intValue() == ACCION_CARGAR_INICIAL) {

					req.setAttribute("exitoMsg", "Antes de realizar cualquier operaci�n es necesario que actualice los datos para notificaci�n de sus operaciones");
					if (!obtieneDatosSession(req)) {
						despliegaPaginaError("Ocurri� un error al tratar de consultar sus datos.", req,res);
					}
				}

				/**
				 * Opci�n cargar si la llamada viene al seleccionar desde el menu, el usuario
				 * entra a actualizar sus datos
				 */
				if(opcion.intValue() == ACCION_CARGAR) {

					if (!obtieneDatosSession(req)) {
						despliegaPaginaError("Ocurri� un error al tratar de consultar sus datos.", req,res);
					}
				}

				/**
				 * Opci�n modificar
				 */
				if(opcion.intValue() == ACCION_MODIFICA) {
					if (!obtieneDatosSession(req)) {
						despliegaPaginaError("Ocurri� un error al tratar de consultar sus datos.", req,res);
					}

					if (valida == null) {
						EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
						boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.MTO_MEDIOS_NOT);

						if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal) {
							EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
							guardaParametrosEnSession(req);

							req.getSession().removeAttribute("mensajeSession");

							ValidaOTP.mensajeOTP(req, "ConfigMedioNot");
							ValidaOTP.validaOTP(req, res, IEnlace.VALIDA_OTP_CONFIRM);
						} else {
							valida = "1";
						}
					}

					if (valida != null && valida.equals("1")) {

						try {
							//Actualiza
							String medioNot = "";

							if (req.getParameter("valida") != null && req.getParameter("valida").equals("1")) {
								emailCelularBean.setCorreo(sess.getAttribute("email") != null ? sess.getAttribute("email").toString() : "");
								emailCelularBean.setLada(sess.getAttribute("lada") != null ? sess.getAttribute("lada").toString() : "");
								emailCelularBean.setNoCelular(sess.getAttribute("celular") != null ? sess.getAttribute("celular").toString() : "");
								emailCelularBean.setCarrier(sess.getAttribute("carrier") != null ? sess.getAttribute("carrier").toString() : "");
								medioNot = sess.getAttribute("tipoNotificacion") != null ? sess.getAttribute("tipoNotificacion").toString() : "";
							} else {
								emailCelularBean.setCorreo(req.getParameter("email") != null ? req.getParameter("email") : "");
								emailCelularBean.setLada(req.getParameter("lada") != null ? req.getParameter("lada") : "");
								emailCelularBean.setNoCelular(req.getParameter("celular") != null ? req.getParameter("celular") : "");
								emailCelularBean.setCarrier(req.getParameter("carrier") != null ? req.getParameter("carrier") : "");
								medioNot = req.getParameter("tipoNotificacion") != null ? req.getParameter("tipoNotificacion").toString() : "";
							}

							if(emailCelularBean.getCarrier().trim().length()==0) {
								emailCelularBean.setCarrier("TE");
								EIGlobal.mensajePorTrace("<><><><><>[CONFIGURACION DE MEDIOS] -> Se asigno carrier default TE", EIGlobal.NivelLog.DEBUG);
							}

							EIGlobal.mensajePorTrace("<><><><><>[CONFIGURACION DE MEDIOS] -> Datos Obtenidos: ", EIGlobal.NivelLog.DEBUG);

							EIGlobal.mensajePorTrace("<><><><><>[CONFIGURACION DE MEDIOS] -> Email: " + emailCelularBean.getCorreo(), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("<><><><><>[CONFIGURACION DE MEDIOS] -> Lada: " + emailCelularBean.getLada(), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("<><><><><>[CONFIGURACION DE MEDIOS] -> Celular: " + emailCelularBean.getNoCelular(), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("<><><><><>[CONFIGURACION DE MEDIOS] -> Carrier: " + emailCelularBean.getCarrier(), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("<><><><><><><><><>[CONFIGURACION DE MEDIOS]Tipo de Notificacion seleccionada: " + medioNot, EIGlobal.NivelLog.DEBUG);

							statusActualizacion = modificaEmailCelular(emailCelularBean, session.getMensajeUSR().getClaveSuperUsuario(), req, medioNot);

							EIGlobal.mensajePorTrace("<><><><>Estatus devuelto al actualizar datos -> " + statusActualizacion[0] +
									"," + statusActualizacion[1], EIGlobal.NivelLog.INFO);

							if(statusActualizacion[0] != null && statusActualizacion[0] != "") {
								if(statusActualizacion[0].contains("exitosa")) {
									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Correo Actual en session es: " + session.getMensajeUSR().getEmailUsuario(),EIGlobal.NivelLog.DEBUG);

									session.getMensajeUSR().setEmailUsuario(emailCelularBean.getCorreo());

									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Correo Nuevo en session es: " + session.getMensajeUSR().getEmailUsuario(),EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Usuario es SuperUsuario?: " + session.getMensajeUSR().isSuperUsuario(),EIGlobal.NivelLog.DEBUG);

									if(session.getMensajeUSR().isSuperUsuario()) {
										EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Usuario es SuperUsuario, Actualizando datos...",EIGlobal.NivelLog.DEBUG);
										EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->Correo de superUsuario actual... " + session.getMensajeUSR().getEmailSuperUsuario(),EIGlobal.NivelLog.DEBUG);

										session.getMensajeUSR().setEmailSuperUsuario(emailCelularBean.getCorreo());

										EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->Correo de superUsuario modificado... " + session.getMensajeUSR().getEmailSuperUsuario(),EIGlobal.NivelLog.DEBUG);
									}
								}
							}

							if(statusActualizacion[1] != null && statusActualizacion[1] != "") {
								if(statusActualizacion[1].contains("exitosa")) {
									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Celular Actual en session es: " + session.getMensajeUSR().getNoCelularUsuario(),EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Lada Actual en session es: " + session.getMensajeUSR().getLadaUsuario(),EIGlobal.NivelLog.DEBUG);
									//session.getMensajeUSR().setCelularUsuario(emailCelularBean.getLada()+emailCelularBean.getNoCelular());
									session.getMensajeUSR().setNoCelularUsuario(emailCelularBean.getNoCelular());
									session.getMensajeUSR().setLadaUsuario(emailCelularBean.getLada());
									session.getMensajeUSR().setCompCelularUsuario((String)session.getCarrierCompleto().get(emailCelularBean.getCarrier()));
									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Lada Nuevo en session es: " + session.getMensajeUSR().getLadaUsuario(),EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Celular Nuevo en session es: " + session.getMensajeUSR().getNoCelularUsuario(),EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Usuario es SuperUsuario?: " + session.getMensajeUSR().isSuperUsuario(),EIGlobal.NivelLog.DEBUG);

									if(session.getMensajeUSR().isSuperUsuario()) {
										EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->El Usuario es SuperUsuario, Actualizando datos...",EIGlobal.NivelLog.DEBUG);
										EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->Celular de superUsuario actual... " + session.getMensajeUSR().getNoCelularSuperUsuario(),EIGlobal.NivelLog.DEBUG);
										EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->Lada de superUsuario actual... " + session.getMensajeUSR().getLadaSuperUsuario(),EIGlobal.NivelLog.DEBUG);
										//session.getMensajeUSR().setCelularSuperUsuario(emailCelularBean.getLada()+emailCelularBean.getNoCelular());
										session.getMensajeUSR().setNoCelularSuperUsuario(emailCelularBean.getNoCelular());
										session.getMensajeUSR().setLadaSuperUsuario(emailCelularBean.getLada());
										session.getMensajeUSR().setCompCelularSuperUsuario((String)session.getCarrierCompleto().get(emailCelularBean.getCarrier()));

										EIGlobal.mensajePorTrace("<><><><><><><>[CONFIGURACION DE MEDIOS]->Celular de superUsuario modificado... " + session.getMensajeUSR().getLadaSuperUsuario() + "-" + session.getMensajeUSR().getNoCelularSuperUsuario(),EIGlobal.NivelLog.DEBUG);
									}
								}
							}

							if (actualizaMedioNotUsuario(medioNot, req)) {
								session.getMensajeUSR().setMedioNotSuperUsuario(medioNot);

							    if (!obtieneDatosSession(req)) {
									despliegaPaginaError("Ocurri&oacute; un error al tratar de actualizar sus datos.", req,res);
								}

							    req.setAttribute("exitoMsg", creaMensaje(statusActualizacion));

							    //session.setCampaniaActiva(false);

						    } else {
						    	req.setAttribute("exitoMsg", "Ocurrio un error al tratar de actualizar sus datos");
						    	return;
						    }


						} catch(Exception e) {
							despliegaPaginaError("Ocurri&oacute; un error al tratar de actualizar sus datos.", req,res);
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						} finally {
							sess.removeAttribute("email");
							sess.removeAttribute("lada");
							sess.removeAttribute("celular");
							sess.removeAttribute("carrier");
							sess.removeAttribute("emtipoNotificacionail");
						}
					}
				}

				req.setAttribute("newMenu", (String) beanMap.get("newMenu"));
				req.setAttribute("MenuPrincipal", (String) beanMap.get("MenuPrincipal"));
				req.setAttribute("Encabezado", (String) beanMap.get("Encabezado"));

				EIGlobal.mensajePorTrace("****************ConfiguraMedioNotificacion:cargaParametros: termino flujo", EIGlobal.NivelLog.INFO);
				try {
					evalTemplate("/jsp/ConfigMedioNot.jsp", req, res);
				} catch (ServletException e) {
					despliegaPaginaError("Ocurri&oacute; un error al tratar de actualizar sus datos.", req,res);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (IOException e) {
					despliegaPaginaError("Ocurri&oacute; un error al tratar de actualizar sus datos.", req,res);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
		    } else {
		    	despliegaPaginaError("No cuenta con Facultades para accesar a esta pantalla o usted no es el SuperUsuario de este contrato", req,res);
		    }
		}
	}



	/**
	 * Metodo encargado de cargar en un mapa los parametros provenientes de la
	 * pantalla
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @return parametros			Mapa con Parametros llenos
	 */
	private HashMap cargaParametros(EmailCelularBean emailCelularBean, HttpServletRequest req) {
		HashMap parametros = new HashMap();
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		parametros.put("opcion", req.getParameter("opcion"));
		parametros.put("MenuPrincipal", session.getStrMenu());
		parametros.put("Encabezado", CreaEncabezado("Configuraci&oacute;n de Medios de Notificaci&oacute;n", "Administraci&oacute;n y Control &gt; Mant. de Datos Personales", "s55250h", req));
		parametros.put("newMenu", session.getFuncionesDeMenu());

		EIGlobal.mensajePorTrace("ConfiguraMedioNotificacion:cargaParametros: se cargaron los datos", EIGlobal.NivelLog.INFO);

		return parametros;
	}

	/**
	 * Metodo encargado de obtener de session los datos del super usuario para
	 * regresarlos a pantalla
	 *
	 * @param req		Petici�n del servidor de aplicaciones
	 */
	private boolean obtieneDatosSession(HttpServletRequest req) {
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");
		boolean exito = false;

		//Obtiene de session los datos del super usuario
		try {
			if(session.getMensajeUSR().getEmailSuperUsuario() != null) {
				req.setAttribute("email", session.getMensajeUSR().getEmailSuperUsuario().toString());
			} else {
				req.setAttribute("email", "");
			}

			if ( session.getMensajeUSR().getNoCelularSuperUsuario() != null
				 && (session.getMensajeUSR().getNoCelularSuperUsuario().length() + session.getMensajeUSR().getLadaSuperUsuario().length()) >= 10
				 && session.getMensajeUSR().getLadaSuperUsuario() != null) {
				String lada = session.getMensajeUSR().getLadaSuperUsuario();
				String celular = session.getMensajeUSR().getNoCelularSuperUsuario();
				req.setAttribute("lada", lada);
				req.setAttribute("celular", celular);
			} else {
				req.setAttribute("lada", "");
				req.setAttribute("celular", "");
			}
			if (session.getMensajeUSR().getCompCelularSuperUsuario() != null) {
				req.setAttribute("carrier", session.getMensajeUSR().getCompCelularSuperUsuario());
			}else {
				req.setAttribute("carrier", "");
			}
			if(session.getMensajeUSR().getMedioNotSuperUsuario() != null) {
				req.setAttribute("tipoNotificacion", session.getMensajeUSR().getMedioNotSuperUsuario());
			} else {
				req.setAttribute("tipoNotificacion", "");
			}

			EIGlobal.mensajePorTrace("ConfiguraMedioNotificacion:obtieneDatosSession: se cargaron los datos", EIGlobal.NivelLog.INFO);

			HashMap hm = new HashMap();
			hm = session.getCarrierCompleto();

			Object estatus = hm.get("RES");

			if (estatus != null) {
				String estatusConsulta = estatus.toString();
				if (estatusConsulta.equals("CORRECTO")){
					req.setAttribute("carriers", hm);
				}
			}
			exito = true;
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace("ConfiguraMedioNotificacion:obtieneDatosSession: se obtuvieron los datos se session", EIGlobal.NivelLog.INFO);
		return exito;
	}

	/**
	 * Metodo encargado de indicar que el medio de notificaci�n ha sido actualizado.
	 * @param beanMap			Mapa con las propiedades del request
	 * @throws Exception		Error de tipo Exception
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 *
	 */
	private boolean actualizaMedioNotUsuario(String medioNot, HttpServletRequest req) throws Exception {
		String query = null;
		boolean exito = false;
		int i = 0;

		BaseResource session = (BaseResource) req.getSession().getAttribute("session");
		Connection conn = null;
		Statement sDup = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();

			query = "update tct_cuentas_tele set medio_notificacion = '".concat(medioNot)
					.concat("' where cve_superusuario ='").concat(session.getMensajeUSR().getClaveSuperUsuario())
					.concat("' and num_cuenta='").concat(session.getContractNumber()).concat("'");

			if (session.getMensajeUSR().isSuperUsuario()) {
				EIGlobal.mensajePorTrace ("CapturaDatosUsuario::actualizaMedioNotUsuario:: -> Query:" + query, EIGlobal.NivelLog.INFO);
				i = sDup.executeUpdate(query);
			}

			if(i > 0) {
				EIGlobal.mensajePorTrace ("CapturaDatosUsuario::indicaCtrUpd:: -> contrato actualizado", EIGlobal.NivelLog.INFO);
				exito = true;
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} finally {
			EI_Query.cierraStatement(sDup);
			EI_Query.cierraConnection(conn);
		}
		return exito;
	}


	/**
	 * Nuevo Metodo encargado de la modificaci�n de email y celular de usuario
	 *
	 * @param emailCelularBean	Bean con el contenido de datos provenientes de
	 * 							la pantalla
	 * @param persona			Usuario que actualiza datos
	 * @param req				Request para recuperar datos de session
	 * @throws Exception		Error de tipo Exception
	 * @return	estatus			Arreglo con el estatus de cada transaccion realizada:
	 * 								estatus[0] -> Estatus Transaccion ODB6(Correo)
	 * 								estatus[1] -> Estatus Transaccion ODB5(Celular)
	 */
	private String[] modificaEmailCelular(EmailCelularBean emailCelularBean, String persona, HttpServletRequest req, String medNot) {

		EmailCelularDAO emailCelularDAO = new EmailCelularDAO();

		String[] estatus = {"NA","NA"};

		String actualiza = "ninguno";

		BaseResource session = (BaseResource) req.getSession().getAttribute("session");

		if(medNot != null && medNot != ""){

			if(medNot.contains("1")){
				actualiza = "correo";
			}
			if(medNot.contains("2")){
				actualiza = "celular";
			}
			if(medNot.contains("3")){
				actualiza = "ambos";
			}

		}

		try {

			EIGlobal.mensajePorTrace("<><><><><><><><><><>[CONFIGURACION DE MEDIOS]: Medio de notificacion seleccionado -> " + medNot, EIGlobal.NivelLog.DEBUG);

			if(actualiza.equals("ambos") || actualiza.equals("correo")) {

				if(session.getStatusCorreo().equals("M")) {

					EIGlobal.mensajePorTrace("<><><><>CONFIGURACION DE MEDIOS DE NOTIFICACION [Se realiza Modificacion de correo] session.getStatusCorreo() --> " + session.getStatusCorreo(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Actualizando Correo <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);

					estatus[0] = emailCelularDAO.actualizaCorreo(emailCelularBean, persona, session.getStatusCorreo());

					EIGlobal.mensajePorTrace("<><><><><><>Estatus Actualizacion Correo ----> " + estatus[0], EIGlobal.NivelLog.INFO);

				}

				if(session.getStatusCorreo().equals("A")) {

					EIGlobal.mensajePorTrace("<><><><>CONFIGURACION DE MEDIOS DE NOTIFICACION [Se realiza Alta de Registro] session.getStatusCorreo() --> " + session.getStatusCorreo(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Alta para Correo <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);

					estatus[0] = emailCelularDAO.actualizaCorreo(emailCelularBean, persona, session.getStatusCorreo());

					EIGlobal.mensajePorTrace("<><><><><><>Estatus Alta Correo ----> " + estatus[0], EIGlobal.NivelLog.INFO);

					if( estatus[0] != null && estatus[0] != "" && estatus[0].length() >= 3 )
						if(estatus[0].contains("exitosa")){
							session.setStatusCorreo("M");
						}
				}
			}

			if(actualiza.equals("ambos") || actualiza.equals("celular")) {

				if(session.getStatusCelular().equals("M")) {

					EIGlobal.mensajePorTrace("<><><><>MANTENIMIENTO DE DATOS PERSONALES [Se realiza Modificacion de celular] session.getStatusCelular() --> " + session.getStatusCelular(), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Actualizando Celular <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);

					estatus[1] = emailCelularDAO.actualizaCelular(emailCelularBean, persona,session.getStatusCelular());

					EIGlobal.mensajePorTrace("<><><><><><>Estatus Actualizacion Celular ----> " + estatus[1], EIGlobal.NivelLog.INFO);

				}

				if(session.getStatusCelular().equals("A")) {

					EIGlobal.mensajePorTrace("<><><><>MANTENIMIENTO DE DATOS PERSONALES [Se realiza Alta de Registro de celular] session.getStatusCelular() --> " + session.getStatusCelular(), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("<><><><><><><><><><><><> Alta para Celular <><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);

					estatus[1] = emailCelularDAO.actualizaCelular(emailCelularBean, persona, session.getStatusCelular());

					EIGlobal.mensajePorTrace("<><><><><><>Estatus Alta Celular ----> " + estatus[1], EIGlobal.NivelLog.INFO);

					//Se cambia la bandera de A a M.

					if( estatus[1] != null && estatus[1] != "" && estatus[1].length() >= 3 )
						if(estatus[1].contains("exitosa")){
							session.setStatusCelular("M");
						}
				}
			}

		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

		session.setCampaniaActiva(false);
		return estatus;
	}

	/**
	 * Nuevo Metodo encargado de la creacion del mensaje a mostrar al usuario
	 * @param estatus	Estatus de las transacciones
	 * @return	msg		Mensaje de resultado de transacciones
	 *
	 */
	public String creaMensaje( String[] estatus) {

		String msg = "Error al Crear El mensaje";

		String celActualizado = "";
		String emailActualizado = "";

		EIGlobal.mensajePorTrace("<><><><><><><><> CONFIGURACION DE MEDIOS DE NOTIFICACION [Estructurando mensaje] -> Codigo de ODB5: " + estatus[0], EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("<><><><><><><><> CONFIGURACION DE MEDIOS DE NOTIFICACION [Estructurando mensaje] -> Codigo de ODB6: " + estatus[1], EIGlobal.NivelLog.DEBUG);


		if( estatus[0] != null && estatus[0] != "" && estatus[0].length() >= 3 )
			if(estatus[0].contains("exitosa")){
				emailActualizado = "A";
			}

		if( estatus[1] != null && estatus[1] != "" && estatus[1].length() >= 3 )
			if(estatus[1].contains("exitosa")){
				celActualizado = "A";
			}

		if( estatus[0] != null && estatus[0] != "" )
			if(estatus[0].contains("NA")){
				emailActualizado = "NA";
			}

		if( estatus[1] != null && estatus[1] != "" )
			if(estatus[1].contains("NA")){
				celActualizado = "NA";
			}

		if( estatus[0] != null && estatus[0] != "" && estatus[0].length() >= 3 )
			if ( estatus[0].equals("Persona Inexistente") || estatus[0].equals("Error") ){
				emailActualizado = "E";
			}

		if( estatus[1] != null && estatus[1] != "" && estatus[1].length() >= 3 )
			if(estatus[1].equals("Persona Inexistente") || estatus[1].equals("Error")){
				celActualizado = "E";
			}

		if(emailActualizado.equals("NA")){
			if(celActualizado.equals("NA")){
				msg = "No se Actualizaron sus Datos";
			}else{
				if(celActualizado.equals("E")){
					msg = "Ocurri� un Error al Tratar de actualizar su N&uacute;mero de Celular.";
				}else{
					msg = ("Su N&uacute;mero Celular fue actualizado correctamente.");
				}
			}
		}else{
			if(emailActualizado.equals("A")){
				if(celActualizado.equals("NA")){
					msg = ("Su correo fue actualizado correctamente.");
				}else{
					if(celActualizado.equals("E")){
						msg = "Su Correo fue actualizado correctamente, pero ocurri&oacute; un Error al tratar de actualizar su N&uacute;mero de Celular.";
					}else{
						msg = ("Sus datos de notificaci&oacute;n fueron actualizados correctamente.");
					}
				}
			}else{
				if(celActualizado.equals("NA")){
					msg = ("Ocurri&oacute un Error al tratar de Actualizar su correo.");
				}else{
					if(celActualizado.equals("E")){
						msg= "Ocurri&oacute un Error al Tratar de actualizar sus datos.";
					}else{
						msg = ("Se actualiz&oacute; su N&uacute;mero Celular correctamente, pero ocurri&oacute; un Error al tratar de actualizar su Correo.");
					}
				}
			}
		}

		return msg;
	}

	public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().setAttribute("email", request.getParameter("email"));
		request.getSession().setAttribute("lada", request.getParameter("lada"));
		request.getSession().setAttribute("celular", request.getParameter("celular"));
		request.getSession().setAttribute("carrier", request.getParameter("carrier"));
		request.getSession().setAttribute("tipoNotificacion", request.getParameter("tipoNotificacion"));

		EIGlobal.mensajePorTrace("\n\n\n Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
  }

}