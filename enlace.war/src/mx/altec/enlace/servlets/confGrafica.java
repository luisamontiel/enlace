package mx.altec.enlace.servlets;


import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.servicioConfirming;
import mx.altec.enlace.dao.CombosConf;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class confGrafica extends BaseServlet
	{
	/** Petici�n GET */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{defaultAction(request, response);}

	/** Petici�n POST */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{defaultAction(request, response);}

	/** Punto de atenci�n para todas las peticiones */
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{

		// si la sesi�n no es v�lida, no se permite entrar al m�dulo
		if (!SesionValida(req, res))
			{
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, req, res);
			return;
			}

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");

		// Se asignan atributos para pasar al jsp
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu",       session.getFuncionesDeMenu());
		req.setAttribute("diasInhabiles", diasInhabilesAnt());
		req.setAttribute("Encabezado",    CreaEncabezado("Filtro de Gr&aacute;ficas", "Servicios &gt; Confirming &gt; Estad&iacute;sticas &gt; Gr&aacute;ficas","s28320h",req));

		// se redirige el control seg�n el tipo de operaci�n
		String opcion = req.getParameter("tipoReq");
		if(opcion == null){
//			TODO: BIT CU 4371. El cliente entra al flujo
			/*
    		 * VSWF ARR -I
    		 */
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				try{

				BitaHelper bh = new BitaHelperImpl(req, session, req.getSession());
				if (req.getParameter(BitaConstants.FLUJO)!=null){
				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				  }else{
				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				  }
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_EST_GRA_ENTRA);
				bt.setContrato(session.getContractNumber());
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException ex){
					//ex.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = ex.getStackTrace();
					EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet confGrafica", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("confGrafica::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ ex.getMessage()
				               				+ "<DATOS GENERALES>"
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}catch(Exception e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet confGrafica", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("confGrafica::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ e.getMessage()
				               				+ "<DATOS GENERALES>"
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}
			}
    		/*
    		 * VSWF ARR -F
    		 */
			try{
				filtro(req,res);
			}catch(Exception e){
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar el filtro del servlet confGrafica", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("confGrafica::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		else if(opcion.equals("1")){
			try{
				consulta("",req,res);
			}catch(Exception e){
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar la consulta del servlet confGrafica", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("confGrafica::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
	}

	/** C�digo encargado de mostrar el filtro para las consultas */
	private void filtro(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource)req.getSession ().getAttribute ("session");

		//session.setModuloConsultar(IEnlace.MOperaciones_mancom);  <--- PENDIENTE: averiguar qu� fregaos es esto
		// Combo de proveedores detallado confirming fase II
        servicioConfirming arch_combos = new servicioConfirming();
        arch_combos.setUsuario(session.getUserID8());
        arch_combos.setContrato(session.getContractNumber());
        arch_combos.setPerfil(session.getUserProfile());
        req.setAttribute("arch_combos", arch_combos);

		// Se preparan atributos para enviar al jsp
		req.setAttribute("Lista",listaProv(req));
		req.setAttribute("Anio", ObtenAnio());
		req.setAttribute("Mes", ObtenMes());
		req.setAttribute("Dia", ObtenDia());

		// se pasa el control al jsp
		evalTemplate("/jsp/coGraficas.jsp",req,res);
		}

	/** C�digo encargado de atender las consultas */
	private void consulta(String mensaje, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{

		String trama = "";
		StringBuffer info = new StringBuffer("");
		String ReDivisa="0";
		Vector notasCanceladas = new Vector();

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");

		// Se obtienen y configuran par�metros
		String parCant = req.getParameter("Cant");
		String parTodos = req.getParameter("Todos");
		String parFecha1 = req.getParameter("fecha1");
		String parFecha2 = req.getParameter("fecha2");
		String parProveedor = req.getParameter("Proveedor");
		String divisa = req.getParameter("Divisa");
		String cuentaCargo = req.getParameter("textCuentas");

        //JIRM STEFANINI 23/MAR/2009 VAR PAR AEL FILTRO DIVISA

		if(divisa.equals("1"))
			divisa="MXN";
		if(divisa.equals("2"))
			divisa="USD";
		if(divisa.equals("0"))
			divisa="";


		// PENDIENTE: la facultad
		//boolean Facu =   session.getFacultad(session.FAC_CCCANCELPAG_CONF);  req.setAttribute("facu",""+Facu);

		// Se crea la trama para Tuxedo
		trama = "2EWEB|" + session.getUserID8() + "|CFAE|" + session.getContractNumber() + "|" + session.getUserID8() + "|" +
			session.getUserProfile() + "|2@" + parFecha1 + "@" + parFecha2 + "@|";
		debug("Trama de env�o: " + trama, EIGlobal.NivelLog.DEBUG);
		System.out.println("Trama de env�o: " + trama);

		// Se env�a la consulta a Tuxedo
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		Hashtable ht = null;
		try
			{ht = tuxGlobal.web_red(trama);}
		catch(Exception e)
			{
			debug("Excepci�n al enviar trama a tuxedo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}
		trama = (String) ht.get("BUFFER");
		debug("tuxedo regresa:" + trama, EIGlobal.NivelLog.DEBUG);

		// Se recupera el archivo de resultados de la consulta mediante copia remota
		trama = trama.substring(trama.lastIndexOf('/')+1);
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		if(archRemoto.copia(trama))
			{
			debug("Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +trama;
			}
		else
			{
			debug("Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		// Se renombra el archivo
		File nombreViejo = new File(trama);
		File nombreNuevo = new File(Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas");
		if(nombreViejo.renameTo(nombreNuevo))
			{
			debug("Archivo renombrado", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas";
			}
		else
			{
			debug("Error al renombrar archivo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		// se construye la l�nea que indica en el jsp la selecci�n de consulta
		String sel = "Movimientos del " + parFecha1 + " al " + parFecha2;

		// Se obtiene lista de proveedores
		String lista[][] = listaProv(req);

		// Se obtiene nombre del proveedor DAG 210909
		String nomProve = "";
		if(parProveedor!=null && !parProveedor.equals("0")){
			nomProve = parProveedor + "-" + getProv(parProveedor, lista);
		}

		// --------------------- Se procesa el archivo ----------------------
		FileWriter exportacion = new FileWriter(Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + ".doc");
		BufferedReader entrada = new BufferedReader(new FileReader(trama));
		String aux;
		String linea;
		String campos[] = new String[31];
		StringTokenizer parser;
		int pos;
		boolean tieneDatos = false;

		entrada.readLine();
		while((linea=entrada.readLine()) != null)
			{
			while((pos=linea.indexOf(";;")) != -1) linea = linea.substring(0,pos+1) + " " + linea.substring(pos+1);
			parser = new StringTokenizer(linea,";");
			try
				{for(pos=0;parser.hasMoreTokens();pos++) campos[pos] = parser.nextToken(); while(++pos<30) campos[pos]="";}
			catch(Exception e)
				{
				debug("Excepci�n al parsear l�nea", EIGlobal.NivelLog.INFO);
				error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
				return;
				}

System.out.print("-----> "); for(int xx=0; xx<31; xx++) System.out.print(xx+"["+campos[xx]+"] "); EIGlobal.mensajePorTrace("",EIGlobal.NivelLog.DEBUG);
System.out.print("<----- "); System.out.println("");
				// se calcula el status
				aux="";
				if(campos[0].equals("1"))
					{
					if(campos[18].equals("P") && campos[19].equals("R"))
						{aux="R";}
					else if(campos[18].equals("C") && campos[19].equals("R"))
						{aux="R";}
					else if(campos[18].equals("V") && campos[19].equals("R"))
						{aux="V";}
					else if(campos[18].equals("P") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("C") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("V") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("C") && campos[19].equals("L"))
						{aux="L";}
					else if(campos[18].equals("C") && campos[19].equals("P"))
						{aux="P";}
					else if(campos[18].equals("C") && campos[19].equals("A"))
						{aux="A";}
					else if(campos[18].equals("C") && campos[19].equals("Z"))
						{aux="Z";}
					else if(campos[18].equals("D") && campos[19].equals("Z"))
						{aux="Z";}
					else if(campos[18].equals("D") && campos[19].equals("C"))
						{aux="Z";}
					else if(campos[18].equals("P") && campos[19].equals("A"))
						{aux="A";}
					else if(campos[18].equals("P") && campos[19].equals("S"))
						{aux="R";}
					else if(campos[18].equals("V") && campos[19].equals("A"))
						{aux="V";}
					else if(campos[18].equals("V") && campos[19].equals("C"))
						{aux="V";}
					}
				else
					{aux = campos[11];}
				campos[19] = aux;

			// FILTRO
			if(!campos[0].equals("3"))
				{

				// parche para ya no pasar notas
				if(campos[0].equals("2")) continue;

				int car;
				if(parTodos.equals("0") && !parProveedor.equals(campos[3])) {EIGlobal.mensajePorTrace("Se rechaza en 1: " + campos[4] + "-->" + ((campos[0].equals("1"))?campos[9]:campos[7]),EIGlobal.NivelLog.DEBUG); System.out.println("C2"); continue;}
				aux=(campos[0].equals("1"))?campos[9]:campos[7];
				if(!(fechaMayor(aux,parFecha1) && fechaMenor(aux,parFecha2))) {EIGlobal.mensajePorTrace("Se rechaza en 2: " + campos[4] + "-->" + ((campos[0].equals("1"))?campos[9]:campos[7]),EIGlobal.NivelLog.DEBUG); System.out.println("C3"); continue;}
				}
			else
				continue;

//			JIRM STEFANINI 23/MAR/2009 FILTRO DIVISA
			//	 filtro de divisa

		//	else {
			if(!divisa.equals("")){
				String aux2=campos[24];
				ReDivisa=campos[24];
				if(divisa.trim().equals("USD")){
					aux="USD";
					ReDivisa="2";}
				if(divisa.trim().equals("MXN")){
					aux="MXN";
					ReDivisa="1";
				}

				if(parCant.equals("1"))
					ReDivisa="3";

				if(!aux.trim().equals(aux2.trim())) continue;
			}
			//}

//			DAG STEFANINI 21/SEP/2009 FILTRO CUENTA DE CARGO
//			 filtro cuenta de cargo
			if(!cuentaCargo.equals("")){
				cuentaCargo = cuentaCargo.substring(0,11);
				EIGlobal.mensajePorTrace("----- cuentacargo -*-----------------> " + cuentaCargo, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("----- cuentacargo -*-----------------> " + campos[23], EIGlobal.NivelLog.INFO);
				System.out.println("----- cuentacargo VAR-*-----------------> " + cuentaCargo);
				System.out.println("----- cuentacargo CAMPO-*-----------------> " + campos[23]);
				aux=campos[23];
				if(!aux.trim().equals(cuentaCargo.trim())) continue;
			}

			// Con una sola vez que pase el filtro, se tentr�n datos para mostrar
			tieneDatos = true;

			// Se arman las tramas de informaci�n para pasar al jsp
			if((campos[1].equals("3") || campos[1].equals("4")) && campos[6].equals("D")) campos[5] = "-" + campos[5];


			//linea = ((campos[0].equals("1"))?campos[9]:campos[7]).substring(3,5) +"|"+ (campos[0].equals("1")?(campos[19]+"|"+campos[7]):(campos[11]+"|"+campos[5]));
			// parche para mostrar importes neteados (para quitar este parche: borrar la sig l�nea y descomentar la anterior)
			linea = ((campos[0].equals("1"))?campos[9]:campos[7]).substring(3,5) +"|"+
				(campos[0].equals("1")?(campos[19]+"|"+campos[8]):(campos[11]+"|"+campos[5]))+ "|"+ campos[24] + "|";

			System.out.println("LINEA:["+linea+"] Cuenta cargo:["+campos[23]+"] Proveedor:["+campos[3]+"]");
			info.append(linea+"\n");

			}

		entrada.close();
		debug("informaci�n a mandar:\n" + info.toString(), EIGlobal.NivelLog.INFO);
		debug("cantidad: " + parCant, EIGlobal.NivelLog.INFO);

		// Si al final no se tienen l�neas para mostrar, se muestra un aviso
		if(!tieneDatos) {error("La consulta realizada no contiene datos",req,res); return;}

		// Se asignan atributos para el jsp
		req.setAttribute("Info", info.toString());
		req.setAttribute("Cant", parCant);
		req.setAttribute("ReDivisa", ReDivisa);
		req.setAttribute("cuentaCargo", cuentaCargo);
		req.setAttribute("nomProve", nomProve);
		req.setAttribute("fecha1", parFecha1);
		req.setAttribute("fecha2", parFecha2);

		//inicia Modif PVA 08/03/2003
		if(parCant.equals("1")) //Graficar por N&uacute;mero de documento
   		  req.setAttribute("Encabezado",    CreaEncabezado("Filtro de Gr&aacute;ficas", "Servicos &gt; Confirming &gt; Estadisticas &gt; Gr&aacute;ficas","s28320Bh",req));
        else                    //por importe
   		  req.setAttribute("Encabezado",    CreaEncabezado("Filtro de Gr&aacute;ficas", "Servicos &gt; Confirming &gt; Estadisticas &gt; Gr&aacute;ficas","s28320Ch",req));
		//termina
		//	TODO: BIT CU 4371 A8. Se actualiza grafica
		/*
		 * VSWF ARR -I
		 */
		if (Global.USAR_BITACORAS.trim().equals("ON")){
			try{
			BitaHelper bh = new BitaHelperImpl(req, session, req.getSession());
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_EST_GRA_ACTUALIZA_GRAFICA);
			bt.setContrato(session.getContractNumber());
			bt.setServTransTux("CFAE");
			if(trama!=null){
				 if(trama.substring(0,2).equals("OK")){
					   bt.setIdErr("CFAE0000");
				 }else if(trama.length()>8){
					  bt.setIdErr(trama.substring(0,8));
				 }else{
					  bt.setIdErr(trama.trim());
				 }
				}
			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch (SQLException ex){
			//ex.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("Error al consultar los graficos de los pagos", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("confGrafica::consulta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ ex.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
			}catch(Exception e){
				//e.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al consultar los graficos de los pagos", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("confGrafica::consulta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
			}
		}
		/*
		 * VSWF ARR -F
		 */
		System.out.print("-----.................................................................555555555> ");
		evalTemplate("/jsp/coGraficasCon.jsp",req,res);
		}

	/** Devuelve una lista con los nombres de proveedores ([ren][1]) y sus respectivos n�meros ([ren][0]) */
	private String[][] listaProv(HttpServletRequest req)
		{

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");

		// Se prepara el objeto CombosConf usado para consultas a Tuxedo
		CombosConf arch_combos = new CombosConf();
		//arch_combos.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		arch_combos.setUsuario(session.getUserID8());
		arch_combos.setContrato(session.getContractNumber());
		arch_combos.setPerfil(session.getUserProfile());
		String  divisa="";
		String archivo = arch_combos.envia_tux(divisa);
		Vector lineas = new Vector();
		String [][] lista = null;
		BufferedReader entrada = null;
		String linea = null;
		String campos[] = new String[6];
		StringTokenizer parser = null;
		String aux;
		int a,b;

		try
			{
			entrada = new BufferedReader(new FileReader(archivo));
			int car;

			while((linea=entrada.readLine())!=null)
				{
				if(!linea.startsWith("9")) continue;
				while((car=linea.indexOf(";;"))!=-1) linea = linea.substring(0,car+1) + " " + linea.substring(car+1);
				lineas.add(linea);
				}
			lista = new String[lineas.size()][2];
			for(car=0;car<lineas.size();car++)
				{
				linea=(String)lineas.get(car);
				parser = new StringTokenizer(linea,";");
				parser.nextToken();
				for(int x=0;x<6;x++) campos[x]=parser.nextToken();
				if(campos[0].equals("F")) campos[3] += " " + campos[4] + " " + campos[5];
				lista[car][0]=campos[1];
				lista[car][1]=campos[3];
				}
			for(a=0;a<lista.length-1;a++)
				for(b=a+1;b<lista.length;b++)
					if(lista[a][1].compareTo(lista[b][1]) > 0)
						{
						aux = lista[a][1]; lista[a][1] = lista[b][1]; lista[b][1] = aux;
						aux = lista[a][0]; lista[a][0] = lista[b][0]; lista[b][0] = aux;
						}
			}
		catch(Exception e)
			{
			debug("Excepci�n en listaProv", EIGlobal.NivelLog.INFO);
			}

		return lista;
		}

	/** Imprime un mensaje al log */
	private void debug(String mensaje, EIGlobal.NivelLog nivel)
		{
		EIGlobal.mensajePorTrace("<DEBUG confGrafica.java> " + mensaje, nivel);
		}

	/** Muestra una p�gina de error con la descripci�n indicada */
	private void error(String mensaje,HttpServletRequest request, HttpServletResponse response)
		{
		try
			{
			despliegaMensaje(mensaje,
				"Filtro de Gr&aacute;ficas",
				"Servicios &gt; Confirming &gt; Estad&iacute;sticas &gt; Gr&aacute;ficas",
				request,
				response);
			}
		catch(Exception e)
			{
			debug("Excepci�n al intentar mostar la pantalla de error", EIGlobal.NivelLog.INFO);
			}
		}

	/** Devuelve el nombre de un proveedor seg�n su n�mero */
	private String getProv(String clave, String[][] lista)
		{
		String prov = "no disponible";
		int pos;
		for(pos=0;pos<lista.length;pos++)
			{if(clave.equals(lista[pos][0])) {prov=lista[pos][1]; break;}}
		return prov;
		}

	/** Devuelve el estatus del documento seg�n el tipo indicado */
	private String getStatus(String tipoDoc)
		{
		if(tipoDoc.trim().equals("R"))
			tipoDoc = "Por Pagar";
		else if(tipoDoc.trim().equals("L"))
			tipoDoc = "Liberados";
		else if(tipoDoc.trim().equals("P"))
			tipoDoc = "Pagados";
		else if(tipoDoc.trim().equals("A"))
			tipoDoc = "Anticipados";
		else if(tipoDoc.trim().equals("C"))
			tipoDoc = "Cancelados";
		else if(tipoDoc.trim().equals("Z"))
			tipoDoc = "Rechazados";
		else if(tipoDoc.trim().equals("V"))
			tipoDoc = "Vencidos";
		else
			tipoDoc = "No especificado";
		return tipoDoc;
		}

	/** Indica si 'mayor' es mayor a 'menor' */
	private boolean fechaMayor(String mayor, String menor)
		{
		int max = Integer.parseInt(mayor.substring(6,10) + mayor.substring(3,5) + mayor.substring(0,2));
		int min = Integer.parseInt(menor.substring(6,10) + menor.substring(3,5) + menor.substring(0,2));
		return max>=min;
		}

	/** Indica si 'menor' es mayor a 'mayor' */
	private boolean fechaMenor(String menor, String mayor)
		{
		int max = Integer.parseInt(mayor.substring(6,10) + mayor.substring(3,5) + mayor.substring(0,2));
		int min = Integer.parseInt(menor.substring(6,10) + menor.substring(3,5) + menor.substring(0,2));
		return max>=min;
		}

	/** Devuelve un texto que representa un valor monetario */


	private String aMoneda(String num)
	{
	int pos = num.indexOf(".");
	if(pos == -1) {pos = num.length(); num += ".";}
	while(pos+3<num.length()) num = num.substring(0,num.length()-1);
	while(pos+3>num.length()) num += "0";
	while(num.length()<4) num = "0" + num;
	for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
	return "$" + num;
	}


	/** Prepara una importe para colocarlo en un archivo de exportaci�n */

	private String aplana(String num)
	{
	String aux = "";
	num=aMoneda(num);
	for(int x=0;x<num.length();x++) if(",$.".indexOf(num.substring(x,x+1)) == -1) aux += num.substring(x,x+1);
	return aux;
	}

	/** Prepara una fecha para colocarla en un archivo de exportaci�n */
	private String voltea(String fecha)
		{
		return fecha.substring(3,5) + fecha.substring(0,2) + fecha.substring(6,10);
		}

	/** rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 */
	public static String rellenar (String cad, int lon, char rel, char tip)
		{
		String aux = "";
		if(cad.length()>lon) return cad.substring(0,lon);
		if(tip!='I' && tip!='D') tip = 'I';
		if( tip=='D' ) aux = cad;
			for (int i=0; i<(lon-cad.length()); i++) aux += ""+ rel;
		if( tip=='I' ) aux += ""+ cad;
		return aux;
		}

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{

		// parche de Ram�n Tejada
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal( session , getServletContext() , this );

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		request.setAttribute( "URL", "confGrafica");

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s28320h", request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}

	}