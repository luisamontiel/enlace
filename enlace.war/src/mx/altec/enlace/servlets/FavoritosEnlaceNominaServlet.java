package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ArchivoNominaBean;
import mx.altec.enlace.beans.ArchivoNominaDetBean;
import mx.altec.enlace.beans.FavoritosEnlaceBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.dao.FavoritosEnlaceDAO;
import mx.altec.enlace.dao.FavoritosEnlaceNominaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FavoritosConstantes;


/**
 * Servlet que atiende las peticiones de favoritos de la nomina
 * 
 * @author ISBAN
 * 
 */
public class FavoritosEnlaceNominaServlet extends FavoritosEnlaceServlet {

	/**
	 * ID version.
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.altec.enlace.servlets.FavoritosEnlaceServlet#doGet(javax.servlet.http
	 * .HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.altec.enlace.servlets.FavoritosEnlaceServlet#doPost(javax.servlet.
	 * http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		FavoritosEnlaceDAO favoritosDAO = new FavoritosEnlaceNominaDAO();
		ejecutarAccion(favoritosDAO, req, resp);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.altec.enlace.servlets.FavoritosEnlaceServlet#redireccionarFlujoOriginal
	 * (java.lang.String, javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void redireccionarFlujoOriginal(String idModulo,
			HttpServletRequest req, HttpServletResponse resp)
			throws IOException, SQLException {
		EIGlobal.mensajePorTrace("Inicia el redir de los favoritos",
				EIGlobal.NivelLog.INFO);

		FavoritosEnlaceBean favorito = null;

		ArchivoNominaBean nominaBean = null;
		String servletOrigen = "";
		HttpSession sess = req.getSession();

		favorito = obtenerFavoritoSeleccionado(req);

		if ((favorito != null)) {

			nominaBean = obtenerBeanArchivoNom(favorito);
			sess.setAttribute(FavoritosConstantes.ID_FAVORITO, nominaBean);

			EIGlobal.mensajePorTrace(
					"Se selecciono el de la lista el cual es: "
							+ favorito.getContrato() + "de "
							+ favorito.getCuentaAbono() + " para  "
							+ favorito.getCuentaCargo() + " con la desc "
							+ favorito.getConceptoOperacion(),
					EIGlobal.NivelLog.INFO);

			servletOrigen = sess.getAttribute(
					FavoritosConstantes.ID_SERVLET_ORIGEN).toString();

			EIGlobal.mensajePorTrace("se redirecciona a: " + servletOrigen,
					EIGlobal.NivelLog.INFO);

			bitacorizarOperacionesFavoritos(BitaConstants.SELECCIONA_FAVORITO,
					favorito, req);
			resp.sendRedirect(servletOrigen + "?"
					+ FavoritosConstantes.ID_PARAM_FAV + "="
					+ FavoritosConstantes.ES_PETICION_DE_FAVORITO+"&Modulo=3&submodulo=2&accion=agregar");

		} else {
			EIGlobal.mensajePorTrace("Se perdieron datos en sesion ERRROR!!!!",
					EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Regresa el objeto Bean necesario para la nomina.
	 *
	 * @param favorito
	 *            Favorito con datos a buscar.
	 * @return Bean con la informacion de la nomina que esta como favorita.
	 * @throws SQLException
	 *             Excepcion SQL
	 */
	private ArchivoNominaBean obtenerBeanArchivoNom(FavoritosEnlaceBean favorito)
			throws SQLException {
		EIGlobal.mensajePorTrace(
				"Iniciando la busqueda de la informacion de favorito de NOMINA",
				EIGlobal.NivelLog.INFO);
		ArchivoNominaBean beanNomina = new ArchivoNominaBean();
		FavoritosEnlaceNominaDAO favDAO = new FavoritosEnlaceNominaDAO();
		LinkedHashMap<String, ArchivoNominaDetBean> detalle = new LinkedHashMap<String, ArchivoNominaDetBean>();

		detalle = favDAO.obtenerDetalleNomina(favorito.getIdFav());

		beanNomina.setContrato(favorito.getContrato());
		beanNomina.setCtaCargo(favorito.getCuentaCargo());
		beanNomina.setImpTotal(favorito.getImporte());
		beanNomina.setNumRegistros(favorito.getNumeroRegistros());
		beanNomina.setDetalleArchivo(detalle);

		return beanNomina;
	}

}