package mx.altec.enlace.servlets;

import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.IEnlace;


import java.io.IOException;

public class EI_EnConstruccion extends BaseServlet {
	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {


		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

	    boolean sesionvalida = SesionValida( request, response );
		if ( sesionvalida ) {
				request.setAttribute( "NumContrato",session.getContractNumber());
				request.setAttribute( "NomContrato",session.getNombreContrato());
				request.setAttribute( "NumUsuario",session.getUserID8());
				request.setAttribute( "NomUsuario",session.getNombreUsuario());

				request.setAttribute( "MenuPrincipal", session.getStrMenu());
				request.setAttribute( "newMenu", session.getFuncionesDeMenu());
				request.setAttribute( "Encabezado",CreaEncabezado( "Página en Construcción", "Información no disponible." ,request) );
				evalTemplate( "/jsp/EI_EnConstruccion.jsp", request, response );
		} else {
           request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
           evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}
    }
}