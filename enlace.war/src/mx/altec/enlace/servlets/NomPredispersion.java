//Checar las constantes de bitaConstants para agregar las nuevas o usar las mismas
package	mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.pdCalendario;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
//import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.NomPreUtil;
import static mx.altec.enlace.utilerias.NomPreUtil.*;


public class NomPredispersion extends BaseServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 3668703668525265150L;


	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace("***TarjetaPrepago.class &Entrando al metodo execute: &", EIGlobal.NivelLog.INFO);


		Vector diasNoHabiles ;
		CalendarNomina myCalendarNomina = null;
		String nuevoMenu;


		String modulo = "";
 		boolean sesionvalida = SesionValida( request, response );
 		HttpSession sess = request.getSession();
 	    BaseResource session = (BaseResource) sess.getAttribute("session");


 	    if (! sesionvalida )
 	    {
 	    	return;
 	    }
 		try {
 			myCalendarNomina= new CalendarNomina( );
 		}
 		catch(Exception e)
          {
 			despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP ,"Pago de N&oacute;mina", "Importando Archivo", "s25800h", request, response );
    			EIGlobal.mensajePorTrace("***TarjetaPrepago.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
          }
 		request.setAttribute("Fecha", ObtenFecha());
 		request.setAttribute("ContUser",ObtenContUser(request));
 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());

		/*************** Nomina Concepto de Archvo */
		if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
			request.getSession().removeAttribute("conceptoEnArchivo");
		/*************** Nomina Concepto de Archvo */

		if(sesionvalida)
		{
			String VarFechaHoy = "";
			diasNoHabiles = myCalendarNomina.CargarDias();
			String strInhabiles = myCalendarNomina.armaDiasInhabilesJS();
			Calendar fechaFin = myCalendarNomina.calculaFechaFin();
			VarFechaHoy = myCalendarNomina.armaArregloFechas();
			String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();


			myCalendarNomina = null;
		 	diasNoHabiles.clear();
		 	diasNoHabiles = null;


 			StringBuffer datesrvrt = new StringBuffer ("");
			datesrvrt = new StringBuffer ("<Input type = \"hidden\" name =\"strAnio\" value = \"");
			datesrvrt.append (ObtenAnio());
			datesrvrt.append ("\">");
            datesrvrt.append ("<Input type = \"hidden\" name =\"strMes\" value = \"");
			datesrvrt.append (ObtenMes());
			datesrvrt.append ("\">");
            datesrvrt.append ("<Input type = \"hidden\" name =\"strDia\" value = \"");
			datesrvrt.append (ObtenDia());
			datesrvrt.append ("\">");

 			request.setAttribute( "VarFechaHoy", VarFechaHoy);
 			request.setAttribute( "fechaFin", strFechaFin);
 			request.setAttribute( "diasInhabiles", strInhabiles);
 			//sess.setAttribute( "diasInhabiles", strInhabiles);
 			request.setAttribute( "fecha2", ObtenFecha( true ) );
 			request.setAttribute( "fechaHoy", ObtenFecha(false) + " - 06" );
 		    request.setAttribute( "ContenidoArchivo", "" );

 			String NumberOfRecords = "";
 			if(session.getNominaNumberOfRecords() != null)
 			 {
 				NumberOfRecords = session.getNominaNumberOfRecords();
 			 }
 			request.setAttribute( "totRegs", NumberOfRecords);
 			estableceFacultades( request, response );
 			session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);

			modulo = request.getParameter("Modulo");
			logInfo("valor de modulo****"+ modulo);
			if ( (modulo == null) || ( modulo.equals("")))
                            modulo = "0";


			if (modulo.equals("7"))
			{
				if(sesionvalida)
				{

					request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE

					//CGH Valida servicio de nomina
					if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
						return;
					}

					BitaTransacBean bt = new BitaTransacBean();
					bt.setNumBit(BitaConstants.ES_NP_PAGOS_IMPORTACION_ENTRA);
					NomPreUtil.bitacoriza(request,null,null,true, bt);

					logInfo("DAG Entro modulo=7 y con Facultad INOMENVIPAG****");

                    verificaArchivos(Global.DOWNLOAD_PATH +request.getSession().getId().toString()+".ses",true);
                    if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
                    {
                    	request.getSession().removeAttribute("Recarga_Ses_Multiples");
                    	EIGlobal.mensajePorTrace( "Inicio Nomina - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
                    	EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples1"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
                    }
                    	EIGlobal.mensajePorTrace( "Inicio Nomina - La variable ya fue borrada", EIGlobal.NivelLog.INFO);


                        request.getSession().removeAttribute("folio");
                        sess.setAttribute("FOLIO", "");
                        System.out.println("LIMPIANDO FOLIO DESDE LA CLASE NOMPREDISPERSION");


                    EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples2"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
                    // Se settea Variable para validación de duplicados
                    sess.removeAttribute("VALIDAR");
                    /*ESC HD1000000085131 - RFC:49313 - Se setea la cuenta de cargo*/
                    session.setCuentaCargo(null);

                    evalTemplate( "/jsp/prepago/NomPredispersion.jsp", request, response );
				}
			   else
				{
				   String laDireccion = "document.location='SinFacultades'";
				   request.setAttribute( "plantillaElegida", laDireccion);
				   request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
				}
			}
		  else
		  {
			  if(modulo.equals("8"))
			  {
				  if( sesionvalida )
				  {

					 request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE

					 //CGH Valida servicio de nomina
					 if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
					 	return;
					 }

					  BitaTransacBean bt = new BitaTransacBean();
					  bt.setNumBit(BitaConstants.ES_NP_PAGOS_CONSULTA_ENTRA);
					  NomPreUtil.bitacoriza(request,null,null,true, bt);

					  request.setAttribute("Bitfechas",datesrvrt);
					  //sess.setAttribute("Bitfechas",datesrvrt);
					  evalTemplate( "/jsp/prepago/NomPreConsultadispersion.jsp", request, response );
				  }
				  else
				  {
					  String laDireccion = "document.location='SinFacultades'";
					  request.setAttribute( "plantillaElegida", laDireccion);
					  request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
				  }
			  }
			  else
			  {//Modulo = 9
				  //Codigo para el pago de la nomina individual
				  if(sesionvalida )
				  {
					  request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));//YHG NPRE

					  if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
						 	return;
					  }

					  //CGH inicializacion de variables de sesion

						if (sess.getAttribute("facultadesN") != null)
							request.setAttribute("facultades", sess.getAttribute("facultadesN"));

						sess.setAttribute("FOLIO", "");
						sess.setAttribute("VALIDARIND", "");

						if (session.getFacultad("INOMENVIPAG")) {
							if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null) {
								request.getSession().removeAttribute("Recarga_Ses_Multiples");
							}
						}

						if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
							request.getSession().removeAttribute("conceptoEnArchivo");

					 //CGH fin de modificacion

					  BitaTransacBean bt = new BitaTransacBean();
					  bt.setNumBit(BitaConstants.ES_NP_PAGOS_INDIVIDUAL_ENTRA);
					  NomPreUtil.bitacoriza(request,null,null,true, bt);
					  logInfo("DAG Entro modulo=9 y con Facultad INOMENVIPAG****");
					/***********************.  Req. Q14709. limpieza de variables de Sesion. *****************************/
					//Se controla la Recarga para evitar duplicidad de registros transferencia
					  verificaArchivos(Global.DOWNLOAD_PATH +request.getSession().getId().toString()+".ses",true);
					  if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
					  {
						  request.getSession().removeAttribute("Recarga_Ses_Multiples");
						  EIGlobal.mensajePorTrace( "Inicio Nomina - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
						  EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples1"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
					  } else
						  EIGlobal.mensajePorTrace( "Inicio Nomina - La variable ya fue borrada", EIGlobal.NivelLog.INFO);

					  EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples2"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
					/**********************Termina limpieza de variables de Sesion**********************/
					  request.setAttribute("Bitfechas",datesrvrt);
					  sess.setAttribute("Bitfechas",datesrvrt);

//					 Fecha de hoy
			            GregorianCalendar hoy = new GregorianCalendar ();
			            GregorianCalendar fechaLib = pdCalendario.getSigDiaHabil (hoy, getDiasInhabiles ());
	                    ArrayList Calendario = pdCalendario.getCalendario (fechaLib, getDiasInhabiles ());
	                    GregorianCalendar fechaLimite = (GregorianCalendar) fechaLib.clone ();
	                    fechaLimite.set (GregorianCalendar.MONTH, fechaLimite.get (GregorianCalendar.MONTH) + 3);
	                    fechaLimite = pdCalendario.getAntDiaHabil (fechaLimite, getDiasInhabiles ());

	                    request.getSession ().setAttribute ("fechaLib", fechaLib);
	                    request.getSession ().setAttribute ("Calendario", Calendario);
	                    request.getSession ().setAttribute ("fechaLimite", fechaLimite);
					  evalTemplate( "/jsp/prepago/NomPredispersionIndividual.jsp", request, response );
				  }
				  else
				  {
					  String laDireccion = "document.location='SinFacultades'";
					  request.setAttribute( "plantillaElegida", laDireccion);
					  request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
				  }
			  }
		  }

		}
		else
		{
			String laDireccion = "document.location='SinFacultades'";
			request.setAttribute( "plantillaElegida", laDireccion);
			request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
		}
	}

 	// Metodo que establecera las facultades para el modulo de pago de nomina
	public void estableceFacultades( HttpServletRequest request, HttpServletResponse response )throws ServletException, IOException
	{
 		HttpSession sess = request.getSession();
 		String eImportar="", eAltas="", eModificar="", eBaja="", eEnvio="", eRecuperar="", eExportar="", eBorrar="";
 		String cHTML="<INPUT TYPE=\"hidden\" NAME=\"fImportar\" VALUE=\"";
 		String cHTML1="\">";
/*
		//facultad para Importar
 		if(verificaFacultad("INOMIMPAPAG",request))
 		{
 			eImportar="ImportarVerdadero";
 		}
 		else
 		{
 			eImportar="SinFacultades";
 		}
 		//facultad para Alta de Registros
 		if(verificaFacultad("INOMALTAPAG",request))
 		{
 			eAltas="AltaVerdadero";
 		}
 		else
 		{
 			eAltas="SinFacultades";
 		}
 		//facultad para Edicion de Registros
 		if(verificaFacultad("INOMMODIPAG",request))
 		{
 			eModificar="CambiosVerdadero";
 		}
 		else
 		{
 			eModificar="SinFacultades";
 		}
 		//facultad para Baja de Registros
 		if(verificaFacultad("INOMBAJAPAG",request))
 		{
 			eBaja="BajasVerdadero";
 		}
 		else
 		{
 			eBaja="SinFacultades";
 		}
 		//facultad para el Envio
 		if(verificaFacultad("INOMENVIPAG",request))
 		{
 			eEnvio="EnvioVerdadero";
 		}
 		else
 		{
 			eEnvio="SinFacultades";
 			request.setAttribute("facultadEnvio",cHTML+eEnvio+cHTML1);
 		}
 		//facultad para la recuperacion
 		if(verificaFacultad("INOMACTUPAG",request))
 		{
 			eRecuperar="RecuperarVerdadero";
 		}
 		else
 		{
 			eRecuperar="SinFacultades";
 		}
 		//facultad para exportar
 		if(verificaFacultad("INOMEXPOPAG",request))
 		{
 			eExportar="ExportarVerdadero";
 		}
 		else
 		{
 			eExportar="SinFacultades";
 		}
 		//facultad para borrar un archivo
 		if(verificaFacultad("INOMBORRPAG",request))
 		{
 			eBorrar="BorraVerdadero";
 		}
 		else
 		{
 			eBorrar="SinFacultades";
 		}
		//facultad para obtener HORARIOS PREMIER
		String eHorarioPremier="";
		if(verificaFacultad("INOMHORARIOS",request))
		{
			eHorarioPremier="PremierVerdadero";
		}
		else
		{
			eHorarioPremier="SinFacultades";
		}
		*/
		StringBuffer valor1 = new StringBuffer ("");  /*JEV 12/03/04*/
		valor1 = new StringBuffer ("ImportarVerdadero");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("EnvioVerdadero");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("PremierVerdadero");

		sess.setAttribute("facultadesN",valor1.toString());
		request.setAttribute("facultades",valor1.toString());
		logInfo("Facultades de nomina en la variable de ambiente: "+sess.getAttribute("facultadesN"));
	}

// Metodo para verificar existencia del Archivo de Sesion
	public boolean verificaArchivos(String arc,boolean eliminar) throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "TarjetaInicioNomina.java Verificando si el archivo existe", EIGlobal.NivelLog.INFO);
		File archivocomp = new File( arc );
		if (archivocomp.exists())
		{
			EIGlobal.mensajePorTrace( "TarjetaInicioNomina.java El archivo si existe.", EIGlobal.NivelLog.INFO);
			if(eliminar)
			{
               archivocomp.delete();
               EIGlobal.mensajePorTrace( "TarjetaInicioNomina.java El archivo se elimina.", EIGlobal.NivelLog.INFO);
			}
			return true;
        }
		EIGlobal.mensajePorTrace( "TarjetaInicioNomina.java El archivo no existe.", EIGlobal.NivelLog.INFO);
		return false;
	}

	 /**
     * Metodo para obtener los dias inhabiles a partir de la fecha actual
     * @return Lista con los dias inhabiles
     * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
     */
    protected ArrayList getDiasInhabiles () {
        java.sql.ResultSet rs = null;
        java.sql.Connection conn = null;
        java.sql.Statement stmt = null;
        ArrayList diasInhabiles = new ArrayList ();
        GregorianCalendar fecha;
        int Dia, Mes, Anio;
        String sql = "select to_char(FECHA, 'dd'), to_char(FECHA, 'mm'), to_char(FECHA, 'yyyy') from COMU_DIA_INHABIL where CVE_PAIS = 'MEXI' and FECHA >= sysdate";
        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if (conn == null) return null;
            stmt = conn.createStatement ();
            rs = stmt.executeQuery (sql);
            while (rs.next ()) {
                Dia = Integer.parseInt (rs.getString (1));
                Mes = Integer.parseInt (rs.getString (2));
                Anio = Integer.parseInt (rs.getString (3));
		Mes++;
                fecha = new GregorianCalendar (Anio, Mes - 2, Dia);
                diasInhabiles.add (fecha);
            }
            rs.close ();
            stmt.close ();
            conn.close ();
            return diasInhabiles;
        } catch (java.sql.SQLException ex) {
            System.err.println (ex.getMessage ());
            ex.printStackTrace (System.err);
        } finally {
        	EI_Query.cierraConexion(rs, stmt, conn);
        }
        return null;
    }
}