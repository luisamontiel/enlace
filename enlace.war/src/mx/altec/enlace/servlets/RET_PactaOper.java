package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.RET_PactaOperBO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;


/**
 *  Clase servlet para realizar el pactado de operaciones con la herramienta Reuters
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 02, 2012
 * 		Control de cambios:
 * @author TCS
 * @version 1.1 Ago 10, 2016 Se agregan tratamiento FLAME 
 */
public class RET_PactaOper extends BaseServlet{

    /**
     * Constante accion iniciar
     */
    private static final String ACCION_INICIAR = "iniciar";

    /**
     * Constante accion regresar
     */
    private static final String ACCION_REGRESAR = "regresar";

    /**
     * Constante accion validaSession
     */
    private static final String ACCION_VALIDA_SESSION = "validaSession";

    /**
     * Constante accion accionFlame
     */
    private static final String ACCION_FLAME = "accionFlame";

    /**
     * Constante Mensaje Error
     */
    private static final String ERROR_DETECTADO = "RET_PactaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->";

    /**
     * Constante Datos Generales
     */
    private static final String DATOS_GENERALES = "<DATOS GENERALES>";

   /**
     * Constante Linea Encontrada
     */
    private static final String LINEA_ENCONTRADA = "Linea encontrada->";
	
   /**
     * Constante transaccion no puede ser atentidad
     */
    private static final String TRANSACCION_NO_ATENDIDA = "Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.";
	/**
	 * Objeto manejador RET_PactaOperBO
	 */
	private final RET_PactaOperBO manejador = new RET_PactaOperBO();


	/**
	 * DoGet
	 * @param req Request
	 * @param res Response
	 * @throws ServletException Excepcion
	 * @throws IOException Excepcion
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		processRequest( req, res );
	 }

	/**
	 * DoPost
	 * @param req Request
	 * @param res Response
	 * @throws ServletException Excepcion
	 * @throws IOException Excepcion
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		processRequest( req, res );
	 }

	/**
	 * Metodo principal para el manejo de la apertura del applet de pactado de operaciones Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws ServletException		Exception de tipo ServletException
	 * @throws IOException			Exception de tipo IOException
	 */
	public void processRequest( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException{

		EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Entrando a compra y venta de divisas.", EIGlobal.NivelLog.DEBUG);
		String opcion = null;
		opcion = (String) getFormParameter(req,"opcion");
		if (opcion == null) {
			opcion = ACCION_INICIAR;
			EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Opcion null, se asigna inicio" , EIGlobal.NivelLog.ERROR);
		}
		EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Opcion: " + opcion, EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if(SesionValida(req,res)){
			//*****************************************Inicia validacion OTP****************
			String valida = getFormParameter(req,"valida" );
			if(validaPeticion( req, res,session,sess,valida)){
				EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Entro a validar peticion", EIGlobal.NivelLog.INFO);
			}else{//*****************************************Termina validacion OTP**************************************
				EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Leyendo facultades.", EIGlobal.NivelLog.INFO);

				if(!verificaFacultad("COMPFXREUTER",req) &&
		           !verificaFacultad("LIBPFXREUTER",req)){
					manejador.paginaError("<p align=\"left\">Por el momento no cuenta con Facultades para operar <font color=green>FX Online</font>,<br>"
							+ ",le pedimos se ponga en contacto con su Ejecutivo de cuenta,<br>" 
							+ "su Ejecutivo Cash &oacute; con su Asesor de Inversi&oacute;n," 
							+ "<br>quienes lo podr�n apoyar para contratar <font color=green>FX Online</font>.</p>", req, res);
				}else{

					/**
					 * Opcion Iniciar
					 */
					if(opcion.equals(ACCION_INICIAR)){
						EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Antes de verificacion de token", EIGlobal.NivelLog.INFO);
						try{
							//interrumpe la transaccion para invocar la validaci�n
							if(valida==null ){
								EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Se verifica si el usuario tiene token y si la "
										+ "transacci�n est� parametrizada para solicitar la validaci�n con el OTP.", EIGlobal.NivelLog.INFO);
								boolean solVal=ValidaOTP.solicitaValidacion(session.getContractNumber(),IEnlace.TRANSFERENCIAS_FX_ONLINE);

								if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP)
								   && session.getToken().getStatus() == 1 && solVal){
									//Bandera para guardar el token en la bit�cora
									req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
									EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: El usuario tiene Token. Solicit� la validaci�n "
											+ "Se guardan los parametros en sesion.", EIGlobal.NivelLog.INFO);
									guardaParametrosEnSession(req);
									ValidaOTP.mensajeOTP(req, "InitFxOnline");
									ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
								}else{
									ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
	                                valida="1";
	                            }
							}
							//retoma el flujo
							if( valida!=null && "1".equals(valida)){
								EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Se retoma el flujo ", EIGlobal.NivelLog.INFO);
								//Valida si es cliente es usuario tipo FLAME o FX Online
								if(manejador.isFlameClient(session.getContractNumber())){
									manejador.iniciarFlame(req,res);
									EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Fin de retoma el flujo FLAME", EIGlobal.NivelLog.INFO);
									evalTemplate("/jsp/RET_pactaOperFlame.jsp", req, res);
								}else{
									manejador.iniciar(req,res);
									EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Fin de retoma el flujo ", EIGlobal.NivelLog.INFO);
									evalTemplate("/jsp/RET_pactaOper.jsp", req, res);
								}
							}
						} catch (ServletException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.ERROR);
						} catch (IOException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.ERROR);
						}
					}

					/**
					 * Opcion Regresar
					 */
					if(opcion.equals(ACCION_REGRESAR)){
					   	try {
							manejador.regresar(req, res);
					        EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Finalizando regresar.", EIGlobal.NivelLog.INFO);
					        evalTemplate("/jsp/RET_pactaOper.jsp", req, res);
						} catch (ServletException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.INFO);
						} catch (IOException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.INFO);
						}
					}

					/**
					 * Opcion validaSession
					 */
					if(opcion.equals(ACCION_VALIDA_SESSION)){
					   	try {
					   		EIGlobal.mensajePorTrace("RET_PactaOperTimer::processRequest:: Se actualiza session Enlace", EIGlobal.NivelLog.INFO);
					        EIGlobal.mensajePorTrace("RET_PactaOperTimer::processRequest:: Finalizando validaSession.", EIGlobal.NivelLog.INFO);
					        evalTemplate("/jsp/RET_PactaOperTimer.jsp", req, res);
						} catch (ServletException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.INFO);
						} catch (IOException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.INFO);
						}
					}
					/**
					 * Opcion accionFlame
					 */
					if(opcion.equals(ACCION_FLAME)){
					   	try {
					   		manejador.doFlameTasks(req, res);
						} catch (ServletException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.INFO);
						} catch (IOException e) {
							logBitacora(req, res, e, EIGlobal.NivelLog.INFO);
						}
					}
				}
			}
		EIGlobal.mensajePorTrace("RET_PactaOper::processRequest:: Saliendo de compra y venta de divisas.", EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Bitacoriza mensajes de error
	 * @param req Peticion
	 * @param res Respuesta
	 * @param e Excepcion
	 * @param nivel nivel Log
	 */
	private void logBitacora(HttpServletRequest req, HttpServletResponse res, Exception e, EIGlobal.NivelLog nivel){
		StackTraceElement[] lineaError;
		lineaError = e.getStackTrace();
		EIGlobal.mensajePorTrace(ERROR_DETECTADO
								+ e.getMessage()
	               				+ DATOS_GENERALES
					 			+ LINEA_ENCONTRADA + lineaError[0]
					 			, nivel);
		manejador.paginaError(TRANSACCION_NO_ATENDIDA, req, res);

	}
	/**
	 * Metodo encargado de guardar los parametros en la session
	 *
	 * @param request			Petici�n de entrada
	 */
	public void guardaParametrosEnSession(HttpServletRequest request){
		EIGlobal.mensajePorTrace("RET_PactaOper::guardaParametrosEnSession::Dentro de guardaParametrosEnSession ", EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().removeAttribute("bitacora");

        HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();

			EIGlobal.mensajePorTrace("RET_PactaOper::guardaParametrosEnSession:: opcion-> " + getFormParameter(request,"opcion"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_PactaOper::guardaParametrosEnSession:: Modulo-> " + getFormParameter(request,"Modulo"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_PactaOper::guardaParametrosEnSession:: nombreArchivoImp-> " + getFormParameter(request,"nombreArchivoImp"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_PactaOper::guardaParametrosEnSession:: totalRegistros-> " + getFormParameter(request,"totalRegistros"), EIGlobal.NivelLog.INFO);

			String value;

			value = (String) getFormParameter(request,"opcion");
			if(value != null && !value.equals(null)) {
				tmp.put("opcion",getFormParameter(request,"opcion"));
			}

			value = (String) getFormParameter(request,"Modulo");
			if(value != null && !value.equals(null)) {
            	tmp.put("Modulo",getFormParameter(request,"Modulo"));
			}

			value = (String) getFormParameter(request,"nombreArchivoImp");
			if(value != null && !value.equals(null)) {
            	tmp.put("nombreArchivoImp",getFormParameter(request,"nombreArchivoImp"));
			}

			value = (String) getFormParameter(request,"totalRegistros");
			if(value != null && !value.equals(null)) {
            	tmp.put("totalRegistros",getFormParameter(request,"totalRegistros"));
			}

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
        EIGlobal.mensajePorTrace("RET_PactaOper::guardaParametrosEnSession:: Saliendo de guardaParametrosEnSession ", EIGlobal.NivelLog.INFO);
    }

 }
