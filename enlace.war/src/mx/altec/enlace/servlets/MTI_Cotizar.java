
package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.beans.NomPrePE68;
import mx.altec.enlace.dao.NomPrePersonasDAO;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.sql.*;

//VSWF

public class MTI_Cotizar extends BaseServlet{

      String pipe="|";


    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		String contrato = "";
		String usuario = "";
		String clavePerfil = "";
		String[][] arrayCuentasCargo=null;

		boolean arcExp = false;

        EI_Tipo Reg = new EI_Tipo(this);
        EI_Tipo NoReg = new EI_Tipo(this);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

        EIGlobal.mensajePorTrace("MTI_Cotizar - execute(): Entrando a Cotizacion Internacional.", EIGlobal.NivelLog.INFO);

        boolean sesionvalida=SesionValida( req, res );
        if(sesionvalida)
         {
			//Variables de sesion ...
			contrato = session.getContractNumber();
			//sucursalOpera, = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			usuario=session.getUserID8();
			clavePerfil = session.getUserProfile();

			Reg.iniciaObjeto(req.getParameter("TransReg"));
			EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Reg.totalRegistros="+Reg.totalRegistros, EIGlobal.NivelLog.INFO);

            NoReg.iniciaObjeto(req.getParameter("TransNoReg"));
			EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() NoReg.totalRegistros="+NoReg.totalRegistros, EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("MTI_Cotizar - execute(): Transferencias revisadsas.", EIGlobal.NivelLog.INFO);

			if(((String) req.getParameter("TransArch")).trim().equals("Archivo de Exportacion"))
			 arcExp=true;
            iniciaCotizar(NoReg,Reg,arcExp,arrayCuentasCargo,clavePerfil,usuario, contrato, req, res );
         }
		else
		if(sesionvalida){
           req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
           evalTemplate(IEnlace.ERROR_TMPL, req, res);
		 }
		EIGlobal.mensajePorTrace("MTI_Cotizar - execute(): Saliendo de Cotizacion Internacionales.", EIGlobal.NivelLog.INFO);

    }

/*************************************************************************************/
/**************************************************************** Inicio Cotizar     */
/*************************************************************************************/
  public void iniciaCotizar(EI_Tipo NoReg,EI_Tipo Reg,boolean arcExp,String[][] arrayCuentasCargo,String clavePerfil, String usuario, String contrato,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
  {
	 /*STFQRO 31012011 INI*/
	  TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();
	  Integer referencia = 0;
	  String codError = "";
  	  String claveEspecial = "";
	  String tipoEspecial = "";
	 /*STFQRO 31012011 FIN*/
	 String nombreArchivo=usuario+".lti";
	 String mensajeError="";
	 String Linea="";
	 String strLinea="";

	 String strTabla="";
     String strCheck="";

     String[] cmpCtz=new String[4];
	 String fechaMenor="";
	 String fechaMayor="";
	 String Monto="";

	 String[][] arregloSegmento=null;
	 String segCuenta="";
	 String cadenaSegmento="";
     String nuevaTramaReg="";
     String nuevaTramaNoReg="";
	 String nuevaTramaTodas="";

	 String TCV_Venta="";
	 String TCE_Venta="";
	 String inicialImporte="";
	 String inicialDesDivisa="";
	 String inicialCveDivisa="";
	 String inicialCuentaCargo="";

     String Trama="";
	 String Result="";
	 String medioEntrega ="";
	 String cuentaCargo = "";
	 String tipoRelacion="";
	 String titular = "";
	 String cuentaAbono = "";
	 String banco = "";
	 String beneficiario = "";
	 String importe="";
	 String plaza="";
	 String sucursal="";
	 String concepto="";
	 String Directo_Inverso="";
	 Directo_Inverso = (String) req.getSession().getAttribute("Directo_Inverso");
	 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Directo_Inverso->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
	 float impSegmento=0;
	 float impCuenta=0;

	 boolean noError=true;
	 boolean servicioErroneo=false;

	 int checkTotal=0;
     int n1=Reg.totalRegistros;
     int n2=n1+NoReg.totalRegistros;

	 double importeTotal=0;

	 EI_Exportar ArcEnt=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
	 EI_Tipo TCTArchivo=new EI_Tipo(this);
	 EI_Tipo Segmento=new EI_Tipo(this);
	 EI_Tipo Todas=new EI_Tipo(this);
	 EI_Tipo CuentaIni = new EI_Tipo(this);

	 ServicioTux tuxGlobal = new ServicioTux();
	 ValidaCuentaContrato valCtas = null;
	 //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	 /************* Modificacion para la sesion ***************/
	 HttpSession sess = req.getSession();
	 BaseResource session = (BaseResource) sess.getAttribute("session");
	 EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Iniciando la cotizacion.", EIGlobal.NivelLog.INFO);

	 TCV_Venta=req.getParameter("TCV_Venta");
	 TCE_Venta=req.getParameter("TCE_Venta");
	 inicialImporte=req.getParameter("inicialImporte");
	 inicialCveDivisa=req.getParameter("inicialCveDivisa");
	 inicialDesDivisa=req.getParameter("inicialDesDivisa");
	 inicialCuentaCargo=req.getParameter("inicialCuentaCargo");

	 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Se recuperaron los parametros.", EIGlobal.NivelLog.INFO);

		try{
			if((String) req.getSession().getAttribute("CveEspecial")!=null
			   && !((String) req.getSession().getAttribute("CveEspecial")).trim().equals("")
			   && !((String) req.getSession().getAttribute("CveEspecial")).trim().equals("null")){
				claveEspecial = (String) req.getSession().getAttribute("CveEspecial");
				tipoEspecial = (String) req.getSession().getAttribute("tipoEspecial");
			}
		}catch(Exception e){}


     /************************************************************
	 Recuperar las transferencias de Archivo de importacion...
	 ************************************************************/
     if(arcExp)
	  {
		EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Transferencias desde archivo.", EIGlobal.NivelLog.INFO);
		if(ArcEnt.abreArchivo())
		 {
		   do
			{
              Linea=ArcEnt.leeLinea();
			  if(Linea.equals("ERROR"))
				noError=false;
			  else
				strLinea+=Linea;
			}while(noError);
		   ArcEnt.cierraArchivo();
		   Todas.iniciaObjeto(strLinea.trim());
		 }
		else
		  mensajeError="Problemas de comunicacion. Por favor intente mas tarde.";
	  }
	 else
	  {
		 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Transferencias en linea.", EIGlobal.NivelLog.INFO);
		 /************************************************************
		 Procesar las transferencias en linea...
		 ************************************************************/
		 strCheck=req.getParameter("strCheck");
		 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Trams checadas." + strCheck, EIGlobal.NivelLog.INFO);
		 if(strCheck.length()<1)
			strCheck="111";
		 nuevaTramaReg="";
		 nuevaTramaNoReg="";
		 for(int i=0;i<Reg.totalRegistros;i++)
		  if(strCheck.charAt(i)!='0')
		   {
			 for(int c=0;c<16;c++)
			  nuevaTramaReg+=Reg.camposTabla[i][c]+pipe;
			 nuevaTramaReg+="@";
		   }

		 for(int i=0;i<NoReg.totalRegistros;i++)
		  if(strCheck.charAt(i+n1)!='0')
		   {
			 for(int c=0;c<16;c++)
			   nuevaTramaNoReg+=NoReg.camposTabla[i][c]+pipe;
			 nuevaTramaNoReg+="@";
		   }
		 Todas.iniciaObjeto(nuevaTramaReg+nuevaTramaNoReg);
	  }

	  EIGlobal.mensajePorTrace("MTI_Cotizar - IniciaCotizar() Trama: "+Todas.strOriginal, EIGlobal.NivelLog.INFO);

  	  //Separar las cuentas que tienen clave especial para no cotizar
	  String strNoCotiza="";
	  String strCotiza="";

	  EI_Tipo ObjCotiza=new EI_Tipo(this);
	  EI_Tipo ObjNoCotiza=new EI_Tipo(this);

	  for(int i=0;i<Todas.totalRegistros;i++)
	   {
		 if(Todas.camposTabla[i][15].trim().equals(""))
		   {
			 for(int j=0;j<Todas.totalCampos;j++)
			  strCotiza+=Todas.camposTabla[i][j]+pipe;
			 strCotiza+="@";
		   }
		  else
		   {
			 for(int j=0;j<Todas.totalCampos;j++)
			  strNoCotiza+=Todas.camposTabla[i][j]+pipe;
			 strNoCotiza+="@";
		   }
		 /*vswf bmb 05-05-08 quitar*/
		 for (int j=0;j<Todas.totalCampos;j++){
			 EIGlobal.mensajePorTrace(">>> Todas.camposTabla ["+i+"]["+j+"]=>"+Todas.camposTabla[i][j]+"<<<<", EIGlobal.NivelLog.DEBUG);
		 }
		 /*fin quitar*/
	   }

	  ObjCotiza.iniciaObjeto(strCotiza);
	  ObjNoCotiza.iniciaObjeto(strNoCotiza);

	  cambiaImporteNormal(ObjCotiza, 4);
	  cambiaImporteNormal(ObjNoCotiza, 4);

	  EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar() Agrupando cuentas por segmento.", EIGlobal.NivelLog.INFO);
      /************************************************************
	  Agrupar las cuentas por segmento y sumar los importes para
	  cada grupo....
	  ************************************************************/
	  cadenaSegmento="";
	  arregloSegmento=new String[ObjCotiza.totalRegistros+1][3];
	  for(int i=0;i<ObjCotiza.totalRegistros;i++)
	   {
		 // Transferencias para cotizar
		 segCuenta=buscaSegmento(ObjCotiza.camposTabla[i][0].trim(),contrato);
		 //###### Cambiar ...
		 /*
		 if(i%2==0)
		  segCuenta="A";
		 else
		  segCuenta="B";
		 strTabla+="<br> arregloSeg ["+i+"][0]="+segCuenta;
		 */
		 arregloSegmento[i][0]=segCuenta;
		 if(cadenaSegmento.indexOf(segCuenta)<0)
		   cadenaSegmento+=segCuenta+"|";
		 importeTotal+=new Float(ObjCotiza.camposTabla[i][4]).floatValue();
	   }
	  cadenaSegmento+="@";
	  cadenaSegmento+=cadenaSegmento+cadenaSegmento+cadenaSegmento+cadenaSegmento+cadenaSegmento+cadenaSegmento+cadenaSegmento;
	  Segmento.iniciaObjeto(cadenaSegmento);

	  //limpiar importes y cuentas de segmentos
	  for(int j=0;j<Segmento.totalCampos;j++)
		Segmento.camposTabla[1][j]="";

	  for(int i=0;i<Segmento.totalCampos;i++)
	   {
		 for(int j=0;j<ObjCotiza.totalRegistros;j++)
		  {
			if(arregloSegmento[j][0].equals(Segmento.camposTabla[0][i]))
			 {
			   if(Segmento.camposTabla[1][i].trim().equals(""))
			    {
			      Segmento.camposTabla[1][i]=ObjCotiza.camposTabla[j][4];
				  Segmento.camposTabla[2][i]=new Integer(j).toString();
				}
			   else
				{
				  impSegmento=new Float(Segmento.camposTabla[1][i]).floatValue();
				  impCuenta=new Float(ObjCotiza.camposTabla[j][4]).floatValue();
				  Segmento.camposTabla[1][i]=new Float(impSegmento+impCuenta).toString();
				  Segmento.camposTabla[2][i]=new Integer(j).toString();
				}
			 }
		  }
	   }

	  /* Desplegar datos del proceso... */
	  /*
	  strTabla+="<br><br> totalCampos: "+new Integer(Segmento.totalCampos).toString();
	  for(int i=0;i<Segmento.totalCampos;i++)
	   {
	     strTabla+="<br><br> Segmento: "+Segmento.camposTabla[0][i];
		 strTabla+="<br> Importe: "+Segmento.camposTabla[1][i];
		 strTabla+="<br> Cuenta: "+Segmento.camposTabla[2][i];
	   }
	  strTabla+="<br><br> cadenaSegmento: "+cadenaSegmento;
	  strTabla+="<br> arregloSegmento: ";
	  for(int i=0;i<ObjCotiza.totalRegistros;i++)
	    if(ObjCotiza.camposTabla[i][15].trim().equals(""))
		  strTabla+=arregloSegmento[i][0]+" ";
	  */

	  /************************************************************
	  Realizar la cotizacion para los segmentos diferentes y
	  asignar el tipo de cambio segun el segmento....
	  ************************************************************/
	  String tipoOperacion="";
	  String monedaDestino="";
	  String monedaOrigen="";
	  String importeTrans="";
	  String cuenta="";

	  String tipoCambioMN="";
	  String tipoCambioDA="";
	  String dirOinver="";
	  String importeMN="";
	  String importeDA="";
	  String importeDV="";

	  String tramasNoCotiza="";

	  boolean cotizacion=true;

	  try{
	  valCtas = new ValidaCuentaContrato();
	  //llamado_servicioCtasInteg(IEnlace.MCargo_TI_pes_dolar," "," "," ",session.getUserID8()+".ambci",req);
      String datoscta[]=null;

      CuentaIni.iniciaObjeto(inicialCuentaCargo);

      	//VSWF
      	BitaTransacBean bt = new BitaTransacBean();
		BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
		//VSWF
	  for(int i=0;i<Segmento.totalCampos;i++)
	   {
		 cuenta=ObjCotiza.camposTabla[Integer.parseInt(Segmento.camposTabla[2][i])][0].trim();
 		 //datoscta=BuscandoCtaInteg(cuenta,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci",IEnlace.MCargo_TI_pes_dolar);
		 datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
				 								   session.getUserID8(),
										           session.getUserProfile(),
										           cuenta.trim(),
										           IEnlace.MCargo_TI_pes_dolar);
         if(datoscta!=null)
		  {
			if(datoscta[3].trim().equals("6"))
              monedaOrigen="DA";
		    else
			  monedaOrigen="MN";
		  }
         else
           monedaOrigen="MN";

		 monedaDestino=ObjCotiza.camposTabla[Integer.parseInt(Segmento.camposTabla[2][i])][6].trim();
		 importeTrans=Segmento.camposTabla[1][i];
		 tipoOperacion="VTA";

//STFQRO	 Trama=IEnlace.medioEntrega1+"|"+session.getUserID8()+"|DICO|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+cuenta+"@TRAN@"+tipoOperacion+"@"+monedaDestino+"@"+monedaOrigen+"@"+importeTrans+"@@";
//STFQRO	 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

         /* Verificar que la transferencia no sea DA,DA,en caso contrario cotizar... */
		 cotizacion=true;
		 if(
			 (monedaOrigen.equals("DA") || monedaOrigen.equals("USD")) &&
			 (monedaDestino.equals("DA") || monedaDestino.equals("USD"))
			)
		   {
			 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): Transferencia dolar - dolar.", EIGlobal.NivelLog.INFO);
		     Result="OK         96818013:20:07|1.0000|1.0000|D|1.0000|1.000000|";
			 cotizacion=false;
	       }
		 else
		  {
		   try
			 {
/*STFQRO 31012100 INI*/
			      // Se llama al servicio para sacar REFERENCIA
				  Hashtable hash = tuxGlobal.sreferencia("901");
		    	  EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hash.toString(), EIGlobal.NivelLog.INFO);
		    	  codError = hash.get("COD_ERROR").toString();

				  // VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
				  if (codError.endsWith("0000")) {
					  String cambVentanillaMXP = "";
					  String cambVentanillaUSD = "";
					  String tipoCambContrario = "";
					  String impMN = "";
					  String impDLL = "";
					  String impDiv = "";
					  referencia = (Integer) hash.get("REFERENCIA");
					  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: claveEspecial->"+claveEspecial, EIGlobal.NivelLog.DEBUG);
					  if(!claveEspecial.trim().equals("")){
						  if(getDivisaTrx(CuentaIni.camposTabla[0][0]).trim().equals("USD")){
							  cambVentanillaUSD = tipoEspecial;
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio especial DIVISA vs USD->"+tipoEspecial, EIGlobal.NivelLog.DEBUG);
							  impDiv = (String)req.getSession().getAttribute("montoDivisareal");
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION->"+impDiv, EIGlobal.NivelLog.DEBUG);
							  if(monedaDestino.equals("DA") || monedaDestino.equals("USD")) {
								  impDLL = (String)req.getSession().getAttribute("montoUSDreal");
							  }
							  else {
								  //LLAMAR GPA0 para sacar tipo de cambio especial pesos
								  tipoCambContrario = tipoCambio.getCambioBase(CuentaIni.camposTabla[0][0], "CPA", "USD", "MXP", "TRAN");
								  if(Directo_Inverso.equals("I")) {
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Inverso 1->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
									  impDLL = String.valueOf(Double.parseDouble(impDiv) / Double.parseDouble(tipoEspecial));
									  impMN = String.valueOf((Double.parseDouble(tipoCambContrario) / Double.parseDouble(tipoEspecial)) * Double.parseDouble(impDiv));
								  }
								  else {
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Directo 1->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
									  impDLL = String.valueOf(Double.parseDouble(impDiv) * Double.parseDouble(tipoEspecial));
									  impMN = String.valueOf((Double.parseDouble(tipoCambContrario) * Double.parseDouble(tipoEspecial)) * Double.parseDouble(impDiv));
								  }
							  }
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe DLL para COTIZACION 1->"+impDLL, EIGlobal.NivelLog.DEBUG);
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe MN para COTIZACION 1->"+impMN, EIGlobal.NivelLog.DEBUG);
							  cambVentanillaMXP = tipoCambContrario;
						  }else{
							  cambVentanillaMXP = tipoEspecial;
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio especial DIVISA vs MXP->"+tipoEspecial, EIGlobal.NivelLog.DEBUG);
							  impDiv = (String)req.getSession().getAttribute("montoDivisareal");
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION->"+impDiv, EIGlobal.NivelLog.DEBUG);
							  impMN = String.valueOf(Double.parseDouble(impDiv) * Double.parseDouble(tipoEspecial));
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe MN para COTIZACION 2->"+impMN, EIGlobal.NivelLog.DEBUG);
							  //LLAMAR GPA0 para sacar tipo de cambio especial dolares
							  if(monedaDestino.equals("DA") || monedaDestino.equals("USD")) {
								  impDLL = (String)req.getSession().getAttribute("montoUSDreal");
							  }
							  else{
								  tipoCambContrario = tipoCambio.getCambioBase(CuentaIni.camposTabla[0][0], "CPA", "USD", "MXP", "TRAN");
								  if(Directo_Inverso.equals("I")) {
								  impDLL = String.valueOf((Double.parseDouble(tipoEspecial) / Double.parseDouble(tipoCambContrario)) * Double.parseDouble(impDiv));
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Inverso 2->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
								  }
								  else {
									  impDLL = String.valueOf((Double.parseDouble(tipoEspecial) * Double.parseDouble(tipoCambContrario)) * Double.parseDouble(impDiv));
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Directo 2->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
								  }
							  }
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe DLL para COTIZACION 2->"+impDLL, EIGlobal.NivelLog.DEBUG);
							  cambVentanillaUSD = tipoCambContrario;
						  }

					  }else{
						  EIGlobal.mensajePorTrace("CUENTA CARGO->" + CuentaIni.camposTabla[0][0], EIGlobal.NivelLog.DEBUG);
						  cambVentanillaUSD = (String) req.getParameter("TCE_Dolar");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio USD->"+cambVentanillaUSD, EIGlobal.NivelLog.DEBUG);
						  cambVentanillaMXP = (String) req.getParameter("TCE_Venta");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio MXP->"+cambVentanillaMXP, EIGlobal.NivelLog.DEBUG);
						  impDiv = (String)req.getSession().getAttribute("montoDivisareal");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION->"+impDiv, EIGlobal.NivelLog.DEBUG);
						  impMN = (String)req.getSession().getAttribute("montoMNreal");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe MN para COTIZACION 3->"+impMN, EIGlobal.NivelLog.DEBUG);
						  impDLL = (String)req.getSession().getAttribute("montoUSDreal");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe DLL para COTIZACION 3->"+impDLL, EIGlobal.NivelLog.DEBUG);
					  }

					  Result="OK         96818013:20:07|"+cambVentanillaMXP+"|"+cambVentanillaUSD+"|D|"+impMN+"|"+impDLL+"|"+impDiv+"|";
					  codError = "DIIN0000";
					  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: tipoCambioOperacion->"+impDLL, EIGlobal.NivelLog.DEBUG);
				  }

/*STFQRO 31012011 FIN*/
//				   Hashtable hs = tuxGlobal.web_red(Trama); // cambia del programa de desarrollo
//				   Result= (String) hs.get("BUFFER") +"";
//				 }catch( java.rmi.RemoteException re )
//				   {re.printStackTrace();}
//				  catch ( Exception e ){}
				 EIGlobal.mensajePorTrace("MTI_Cotizar - SE HA HECHO UNA COTIZACION:"+Result, EIGlobal.NivelLog.INFO);

			 }catch ( Exception e ){
				 codError = "DIIN9999";
				 StackTraceElement[] lineaError;
				   lineaError = e.getStackTrace();
				   EIGlobal.mensajePorTrace("MTI_Cotizar - ERROR AL TRATAR DE COTIZAR: "
						   +e.getMessage()+"\n"+lineaError[0], EIGlobal.NivelLog.ERROR);
				   despliegaPaginaError("Error al tratar de cotizar", req, res);
				   return;
			 }
			 }

		 //#########
		 //Result="OK         96818013:20:07|5.0000|2.0000|D|3.0000|4.000000|12.00|";
		 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

		 /* Despliega informacion en pantalla */
		 /*
		 strTabla+="<br> Segmento: "+Segmento.camposTabla[0][i]+" Se envia: "+Trama;
		 strTabla+="<br>"+Result;
		 */
			//TODO BIT CU2101, Cotizaci�n Internacional
		 	//TODO BIT CU3131 realiza cotizaci�n, operaci�n DICO
			/**
			 * VSWF-***-I
			 * 15/Enero/2007	Modificaci�n para modulo de Tesorer�a
			 */
		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaTCTBean beanTCT = new BitaTCTBean ();

				bt = (BitaTransacBean)bh.llenarBean(bt);
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_TRANSFERENCIAS_INTER))
				   bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_VALIDA_TRANSF_DUPLICADAS);
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER))
				   bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DICO);
				if(Result != null){
					if(!Result.equals("") && Result.substring(0,2).equals("OK")){
						bt.setIdErr("DICO0000");
					}else if(Result.length() > 8){
						bt.setIdErr(Result.substring(0,8));
					}else{
						bt.setIdErr(Result.trim());
					}
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if (importeTrans != null) {
					bt.setImporte(Double.parseDouble(importeTrans.trim()));
				}
				if (monedaDestino != null) {
					bt.setTipoMoneda(monedaDestino.trim());
				}
				if (nombreArchivo != null) {
					bt.setNombreArchivo(nombreArchivo.trim());
				}
				bt.setServTransTux("DICO");
				if (TCV_Venta != null) {
					bt.setTipoCambio(Double.parseDouble(TCV_Venta.trim()));
				}
				/*VSWF I Autor=BMB fecha=05-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario, cuenta origen y destino*/
				cuentaAbono =ObjCotiza.camposTabla[Integer.parseInt(Segmento.camposTabla[2][i])][2].trim();
				beneficiario=ObjCotiza.camposTabla[Integer.parseInt(Segmento.camposTabla[2][i])][3].trim();
				bt.setCctaOrig((cuenta==null)?" ":cuenta);
				bt.setCctaDest((cuentaAbono==null)?"":cuentaAbono);
				bt.setBeneficiario((beneficiario==null)?" ":beneficiario);
				/*VSWF F*/
				BitaHandler.getInstance().insertBitaTransac(bt);
				//INI BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
				beanTCT = bh.llenarBeanTCT(beanTCT);
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion("DICO");
				beanTCT.setCodError(codError);
				beanTCT.setUsuario(session.getUserID8());
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);
				//FIN BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
			}catch(SQLException e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		 }
			/**
			 * VSWF-***-F
			 */

		 if(Result!=null || Result.equals("null"))
		  if(!Result.substring(0,2).equals("OK")) {
			/*MSD Q05-24447*/
			//servicioErroneo=true;
			despliegaPaginaError("ERROR: " + Result.substring(16),"Transferencias Internacionales","Transferencias &gt; Internacionales",req,res);
			return;
			/*MSD Q05-24447*/
		  }

		 if(!cotizacion)
		   {
			 dirOinver="D";
			 importeMN = (String)req.getSession().getAttribute("montoMNreal");
			 EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe MN para COTIZACION (USD vs USD)->"+importeMN, EIGlobal.NivelLog.DEBUG);
			 importeDA = (String)req.getSession().getAttribute("montoUSDreal");
			 EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DLL para COTIZACION (USD vs USD)->"+importeDA, EIGlobal.NivelLog.DEBUG);
			 importeDV = (String)req.getSession().getAttribute("montoDivisareal");
			 EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION (USD vs USD)->"+importeDV, EIGlobal.NivelLog.DEBUG);
			 tipoCambioMN="1.000";
			 tipoCambioDA="1.000";
		   }
		  else
		   {
			 dirOinver=extraeDatos(Result,"DI",monedaOrigen,monedaDestino);
			 importeMN=extraeDatos(Result,"IMN",monedaOrigen,monedaDestino);
			 importeDA=extraeDatos(Result,"IDA",monedaOrigen,monedaDestino);
			 tipoCambioMN=extraeDatos(Result,"MN",monedaOrigen,monedaDestino);
			 tipoCambioDA=extraeDatos(Result,"DA",monedaOrigen,monedaDestino);
			 importeDV=extraeDatos(Result,"IDV",monedaOrigen,monedaDestino);

			 if(tipoCambioMN.equals("NO DISPONIBLE"))
			  {
				if(TCE_Venta.equals("NO DISPONIBLE"))
				 {
				   if(TCE_Venta.equals("NO DISPONIBLE"))
				     tipoCambioMN="1.000";
				   else
					 tipoCambioMN=TCV_Venta;
				 }
				else
				 tipoCambioMN=TCE_Venta;
			  }
		   }

	 	 /* Despliega informacion en pantalla*/
		 /*
		 strTabla+="<br> Tipo de cambio MN: "+tipoCambioMN;
		 strTabla+="<br> Tipo de cambio DA: "+tipoCambioDA;
		 */

		 Segmento.camposTabla[2][i]=tipoCambioMN;
		 Segmento.camposTabla[3][i]=tipoCambioDA;
		 Segmento.camposTabla[4][i]=dirOinver;
		 Segmento.camposTabla[5][i]=importeMN;
		 Segmento.camposTabla[6][i]=importeDA;
		 Segmento.camposTabla[7][i]=importeDV;
	   }

	  //cotizar las de clave especial para obtener los datos necesarios
	  for(int k=0;k<ObjNoCotiza.totalRegistros;k++)
	   {
		 cuenta=ObjNoCotiza.camposTabla[k][0].trim();
		 //datoscta=BuscandoCtaInteg(cuenta,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci",IEnlace.MCargo_TI_pes_dolar);
		   datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
												     session.getUserID8(),
										             session.getUserProfile(),
										             cuenta.trim(),
										             IEnlace.MCargo_TI_pes_dolar);
         if(datoscta!=null)
		  {
			if(datoscta[3].trim().equals("6"))
              monedaOrigen="DA";
		    else
			  monedaOrigen="MN";
		  }
         else
           monedaOrigen="MN";
		 monedaDestino=ObjNoCotiza.camposTabla[k][6].trim();
		 importeTrans=ObjNoCotiza.camposTabla[k][4].trim();
		 tipoOperacion="VTA";

//STFQRO 31012011 Trama=IEnlace.medioEntrega1+"|"+usuario+"|DICO|"+contrato+"|"+usuario+"|"+clavePerfil+"|"+cuenta+"@TRAN@"+tipoOperacion+"@"+monedaDestino+"@"+monedaOrigen+"@"+importeTrans+"@"+ObjNoCotiza.camposTabla[k][7].trim()+"@";
//STFQRO 31012011 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

         /* Verificar que la transferencia no sea DA,DA,en caso contrario cotizar... */
		 cotizacion=true;
		 if(
			 (monedaOrigen.equals("DA") || monedaOrigen.equals("USD")) &&
			 (monedaDestino.equals("DA") || monedaDestino.equals("USD"))
			)
		   {
			 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): Transferencia dolar - dolar.", EIGlobal.NivelLog.INFO);
		     Result="OK         96818013:20:07|1.0000|1.0000|D|1.0000|1.000000|";
			 cotizacion=false;
	       }
		 else//STFQRO 31012011 INI SI SELECCIONAMOS EUROS U OTRA DIVISA QUE NO SEA DOLARES PASARA POR AQU�
//		 else
		   {
			 try
			   {
				 //SE OBTIENE LA REFERENCIA DE ESTA COTIZACION
				  referencia = 0;
				  Hashtable hash = tuxGlobal.sreferencia("901");
		    	  EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hash.toString(), EIGlobal.NivelLog.INFO);
		    	  codError = hash.get("COD_ERROR").toString();

		    	// VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
				  if (codError.endsWith("0000")) {
					  String cambVentanillaMXP = "";
					  String cambVentanillaUSD = "";
					  String tipoCambContrario = "";
					  String impMN = "";
					  String impDLL = "";
					  String impDiv = "";
					  referencia = (Integer) hash.get("REFERENCIA");
					  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: claveEspecial->"+claveEspecial, EIGlobal.NivelLog.DEBUG);
					  if(!claveEspecial.trim().equals("")){
						  if(getDivisaTrx(CuentaIni.camposTabla[0][0]).trim().equals("USD")){
							  cambVentanillaUSD = tipoEspecial;
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio especial DIVISA vs USD->"+tipoEspecial, EIGlobal.NivelLog.DEBUG);
							  impDiv = (String)req.getSession().getAttribute("montoDivisareal");
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION->"+impDiv, EIGlobal.NivelLog.DEBUG);
							  if(monedaDestino.equals("DA") || monedaDestino.equals("USD")) {
								  impDLL = (String)req.getSession().getAttribute("montoUSDreal");
							  }
							  else {
								  //LLAMAR GPA0 para sacar tipo de cambio especial pesos
								  tipoCambContrario = tipoCambio.getCambioBase(CuentaIni.camposTabla[0][0], "CPA", "USD", "MXP", "TRAN");
								  if(Directo_Inverso.equals("I")) {
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Inverso 4->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
									  impDLL = String.valueOf(Double.parseDouble(impDiv) / Double.parseDouble(tipoEspecial));
									  impMN = String.valueOf((Double.parseDouble(tipoCambContrario) / Double.parseDouble(tipoEspecial)) * Double.parseDouble(impDiv));
								  }
								  else {
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Directo 4->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
									  impDLL = String.valueOf(Double.parseDouble(impDiv) * Double.parseDouble(tipoEspecial));
									  impMN = String.valueOf((Double.parseDouble(tipoCambContrario) * Double.parseDouble(tipoEspecial)) * Double.parseDouble(impDiv));
								  }
							  }
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe DLL para COTIZACION 4->"+impDLL, EIGlobal.NivelLog.DEBUG);
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe MN para COTIZACION 4->"+impMN, EIGlobal.NivelLog.DEBUG);
							  cambVentanillaMXP = tipoCambContrario;
						  }else{
							  cambVentanillaMXP = tipoEspecial;
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio especial DIVISA vs MXP->"+tipoEspecial, EIGlobal.NivelLog.DEBUG);
							  impDiv = (String)req.getSession().getAttribute("montoDivisareal");
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION->"+impDiv, EIGlobal.NivelLog.DEBUG);
							  impMN = String.valueOf(Double.parseDouble(impDiv) * Double.parseDouble(tipoEspecial));
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe MN para COTIZACION 5->"+impMN, EIGlobal.NivelLog.DEBUG);
							  //LLAMAR GPA0 para sacar tipo de cambio especial dolares
							  if(monedaDestino.equals("DA") || monedaDestino.equals("USD")) {
								  impDLL = (String)req.getSession().getAttribute("montoUSDreal");
							  }
							  else{
								  tipoCambContrario = tipoCambio.getCambioBase(CuentaIni.camposTabla[0][0], "CPA", "USD", "MXP", "TRAN");
								  if(Directo_Inverso.equals("I")) {
								  impDLL = String.valueOf((Double.parseDouble(tipoEspecial) / Double.parseDouble(tipoCambContrario)) * Double.parseDouble(impDiv));
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Inverso 5->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
								  }
								  else {
									  impDLL = String.valueOf((Double.parseDouble(tipoEspecial) * Double.parseDouble(tipoCambContrario)) * Double.parseDouble(impDiv));
									  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Directo 5->"+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
								  }
							  }
							  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe DLL para COTIZACION 5->"+impDLL, EIGlobal.NivelLog.DEBUG);
							  cambVentanillaUSD = tipoCambContrario;
						  }

					  }else{
						  EIGlobal.mensajePorTrace("CUENTA CARGO->" + CuentaIni.camposTabla[0][0], EIGlobal.NivelLog.DEBUG);
						  cambVentanillaUSD = (String) req.getParameter("TCE_Dolar");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio USD->"+cambVentanillaUSD, EIGlobal.NivelLog.DEBUG);
						  cambVentanillaMXP = (String) req.getParameter("TCE_Venta");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Tipo de cambio MXP->"+cambVentanillaMXP, EIGlobal.NivelLog.DEBUG);
						  impDiv = (String)req.getSession().getAttribute("montoDivisareal");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION->"+impDiv, EIGlobal.NivelLog.DEBUG);
						  impMN = (String)req.getSession().getAttribute("montoMNreal");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe MN para COTIZACION 6->"+impMN, EIGlobal.NivelLog.DEBUG);
						  impDLL = (String)req.getSession().getAttribute("montoUSDreal");
						  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Resultado Importe DLL para COTIZACION 6->"+impDLL, EIGlobal.NivelLog.DEBUG);
					  }

					  Result="OK         96818013:20:07|"+cambVentanillaMXP+"|"+cambVentanillaUSD+"|D|"+impMN+"|"+impDLL+"|"+impDiv+"|";
					  codError = "DIIN0000";
					  EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: tipoCambioOperacion->"+impDLL, EIGlobal.NivelLog.DEBUG);
				  }
				 EIGlobal.mensajePorTrace("MTI_Cotizar - SE HA HECHO UNA COTIZACION:"+Result, EIGlobal.NivelLog.INFO);

				 /*				 Hashtable hs = tuxGlobal.web_red(Trama); // cambia del programa de desarrollo
				 Result= (String) hs.get("BUFFER") +"";
			   }catch( java.rmi.RemoteException re )
				{re.printStackTrace();}
			    catch ( Exception e ){}
			   }STFQRO 31012011 FIN*/

			   }catch ( Exception e ){
				   StackTraceElement[] lineaError;
				   lineaError = e.getStackTrace();
				   EIGlobal.mensajePorTrace("MTI_Cotizar - ERROR AL TRATAR DE COTIZAR: "
						   +e.getMessage()+"\n"+lineaError[0], EIGlobal.NivelLog.ERROR);
				   despliegaPaginaError("Error al tratar de cotizar", req, res);
				   return;
			   }

		 //#########
		 //Result="OK         96818013:20:07|5.0000|2.0000|D|3.0000|4.000000|12.00|";
		 if(Result!=null || Result.equals("null"))
		  if(!Result.substring(0,2).equals("OK")) {
			/*MSD Q05-24447*/
			//servicioErroneo=true;
			despliegaPaginaError("ERROR: " + Result.substring(16),"Transferencias Internacionales","Transferencias &gt; Internacionales",req,res);
			return;
			/*MSD Q05-24447*/
		  }

		 EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

		 if(!cotizacion)
		   {
			 dirOinver="D";
			 importeMN = (String)req.getSession().getAttribute("montoMNreal");
			 EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe MN para COTIZACION (USD vs USD)->"+importeMN, EIGlobal.NivelLog.DEBUG);
			 importeDA = (String)req.getSession().getAttribute("montoUSDreal");
			 EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DLL para COTIZACION (USD vs USD)->"+importeDA, EIGlobal.NivelLog.DEBUG);
			 importeDV = (String)req.getSession().getAttribute("montoDivisareal");
			 EIGlobal.mensajePorTrace("MTI_Cotizar:: iniciaCotizar:: Importe DIVISA para COTIZACION (USD vs USD)->"+importeDV, EIGlobal.NivelLog.DEBUG);
			 tipoCambioMN="1.000";
			 tipoCambioDA="1.000";
		   }
		  else
		   {
			 dirOinver=extraeDatos(Result,"DI",monedaOrigen,monedaDestino);
			 importeMN=extraeDatos(Result,"IMN",monedaOrigen,monedaDestino);
			 importeDA=extraeDatos(Result,"IDA",monedaOrigen,monedaDestino);
			 tipoCambioMN=extraeDatos(Result,"MN",monedaOrigen,monedaDestino);
			 tipoCambioDA=extraeDatos(Result,"DA",monedaOrigen,monedaDestino);
			 importeDV=extraeDatos(Result,"IDV",monedaOrigen,monedaDestino);

			 if(tipoCambioMN.equals("NO DISPONIBLE"))
			  {
				if(TCE_Venta.equals("NO DISPONIBLE"))
				 {
				   if(TCE_Venta.equals("NO DISPONIBLE"))
				     tipoCambioMN="1.000";
				   else
					 tipoCambioMN=TCV_Venta;
				 }
				else
				 tipoCambioMN=TCE_Venta;
			  }
		   }

		  //Arama trama para enviar la transferencia con los nuevos datos....
		  for(int l=0;l<15;l++)
		   {
			if(l==7)
		     {
			   if(! (monedaOrigen.trim().equals("DA") || monedaOrigen.trim().equals("USD")) )
				 tramasNoCotiza+=tipoCambioMN+pipe;
			   else
				 tramasNoCotiza+=tipoCambioDA+pipe;
			 }
			else
			  tramasNoCotiza+=ObjNoCotiza.camposTabla[k][l]+pipe;
		   }

		  EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): ASIGNADO 1 " + tramasNoCotiza, EIGlobal.NivelLog.INFO);

		  tramasNoCotiza+=tipoCambioDA+pipe;
		  tramasNoCotiza+=ObjNoCotiza.camposTabla[k][15]+pipe;
		  tramasNoCotiza+=extraeDatos(Result,"HC",monedaOrigen,monedaDestino)+pipe;
		  tramasNoCotiza+=dirOinver+pipe;
		  tramasNoCotiza+=importeMN+pipe;
		  tramasNoCotiza+=importeDA+pipe;
		  tramasNoCotiza+=importeDV+pipe;
		  tramasNoCotiza+=EnlaceGlobal.fechaHoy("dd/mm/aaaa")+pipe;
		  tramasNoCotiza+=tipoCambioMN+pipe;
		  tramasNoCotiza+="@";
	   }

		/**
		 * VSWF-***-I
		 * 15/Enero/2007	Modificaci�n para modulo de Tesorer�a
		 */
		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaTCTBean beanTCT = new BitaTCTBean ();

				bt = (BitaTransacBean)bh.llenarBean(bt);
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_TRANSFERENCIAS_INTER))
				   bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_VALIDA_TRANSF_DUPLICADAS);
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER))
				   bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DICO);
				if(Result != null){
					if(!Result.equals("") && Result.substring(0,2).equals("OK")){
						bt.setIdErr("DICO0000");
					}else if(Result.length() > 8){
						bt.setIdErr(Result.substring(0,8));
					}else{
						bt.setIdErr(Result.trim());
					}
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if (importeTrans != null) {
					bt.setImporte(Double.parseDouble(importeTrans.trim()));
				}
				if (monedaDestino != null) {
					bt.setTipoMoneda(monedaDestino.trim());
				}
				if (nombreArchivo != null) {
					bt.setNombreArchivo(nombreArchivo.trim());
				}
				bt.setServTransTux("DICO");
				if (TCV_Venta != null) {
					bt.setTipoCambio(Double.parseDouble(TCV_Venta.trim()));
				}
				/*VSWF I Autor=BMB fecha=05-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario, cuenta origen y destino*/
				cuentaAbono =ObjCotiza.camposTabla[Integer.parseInt(Segmento.camposTabla[2][k])][2].trim();
				beneficiario=ObjCotiza.camposTabla[Integer.parseInt(Segmento.camposTabla[2][k])][3].trim();
				bt.setCctaOrig((cuenta==null)?" ":cuenta);
				bt.setCctaDest((cuentaAbono==null)?"":cuentaAbono);
				bt.setBeneficiario((beneficiario==null)?" ":beneficiario);
				/*VSWF F*/
				BitaHandler.getInstance().insertBitaTransac(bt);
				//INI BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
				beanTCT = bh.llenarBeanTCT(beanTCT);
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion("DICO");
				beanTCT.setCodError(codError);
				beanTCT.setUsuario(session.getUserID8());
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);
				//FIN BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
			}catch(SQLException e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		 }
	   }

	  }catch(Exception e){
		  EIGlobal.mensajePorTrace("Error en metodo iniciaCotizar de clase MTI_Cotizar->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
	  }finally{
	      try{
	    	  valCtas.closeTransaction();
	      }catch (Exception e) {
	    	  EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
	      }
	  }

	  /******************************************/
	  //Armar trama para las que cotizan sin clave especial...
	  for(int i=0;i<ObjCotiza.totalRegistros;i++)
	   {
		 tipoCambioDA="";
		 dirOinver="";

			for(int j=0;j<Segmento.totalCampos;j++)
			 if(arregloSegmento[i][0].equals(Segmento.camposTabla[0][j]))
			  {
				ObjCotiza.camposTabla[i][7]=Segmento.camposTabla[2][j];
				tipoCambioDA=Segmento.camposTabla[3][j];
				dirOinver=Segmento.camposTabla[4][j];
				importeMN=Segmento.camposTabla[5][j];
				importeDA=Segmento.camposTabla[6][j];
				importeDV=Segmento.camposTabla[7][j];
			  }

			for(int k=0;k<15;k++)
		     {
			  if(k==7)
		       {
			     if(! (monedaOrigen.trim().equals("DA") || monedaOrigen.trim().equals("USD")) )
			       nuevaTramaTodas+=tipoCambioMN+pipe;
			     else
				   nuevaTramaTodas+=tipoCambioDA+pipe;
			   }
			  else
			    nuevaTramaTodas+=ObjCotiza.camposTabla[i][k]+pipe;
		     }

			EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): ASIGNADO " + nuevaTramaTodas, EIGlobal.NivelLog.INFO);

			nuevaTramaTodas+=tipoCambioDA+pipe;
			nuevaTramaTodas+=""+pipe;
			nuevaTramaTodas+=extraeDatos(Result,"HC",monedaOrigen,monedaDestino)+pipe;
			nuevaTramaTodas+=dirOinver+pipe;
			nuevaTramaTodas+=importeMN+pipe;
			nuevaTramaTodas+=importeDA+pipe;
			nuevaTramaTodas+=importeDV+pipe;
			nuevaTramaTodas+=EnlaceGlobal.fechaHoy("dd/mm/aaaa")+pipe;
			nuevaTramaTodas+=tipoCambioMN+pipe;
			nuevaTramaTodas+="@";
	   }

  	  EI_Tipo NuevasNoCotiza=new EI_Tipo(this);
	  NuevasNoCotiza.iniciaObjeto(tramasNoCotiza);

	  EI_Tipo Agrupadas=new EI_Tipo(this);
	  Agrupadas.iniciaObjeto(nuevaTramaTodas+tramasNoCotiza);

	  boolean aplica=true;
	  String botonEnviar="";

       for(int i=0;i<Agrupadas.totalRegistros;i++)
	    {
          //Para calcular la fecha en la que aplica ...
		  if( Agrupadas.camposTabla[i][8].trim().equals("USA") ||
			  Agrupadas.camposTabla[i][8].trim().equals("CANA") )
			{
			  if(esHorarioValido(0,Agrupadas.camposTabla[i][5]))
			   {
			     if(esHorarioValido(1,Agrupadas.camposTabla[i][5]))
			      Agrupadas.camposTabla[i][22]=EnlaceGlobal.fechaHoy("dd/mm/aaaa");
				 else
				  Agrupadas.camposTabla[i][22]="Pr&oacute;ximas 24 horas h&aacute;biles";
			   }
			  else
			   {
				 Agrupadas.camposTabla[i][22]="No Aplica. Fuera de Horario";
				 aplica=false;
			   }
			}
		  else
			{
			  if(esHorarioValido(0,Agrupadas.camposTabla[i][5]))
				Agrupadas.camposTabla[i][22]="Pr&oacute;ximas 48 horas h&aacute;biles";
			  else
			   {
				 Agrupadas.camposTabla[i][22]="No Aplica. Fuera de Horario";
				 aplica=false;
			   }
			}
		}

	  if(Agrupadas.totalRegistros>=1)
		strTabla+=generaTabla("Trasferencias Seleccionadas",2,Agrupadas,true,req);

	  if (aplica) {


			if( session.getToken().getStatus() == 1 ){
				botonEnviar="<td><a href='javascript:respuesta=1;continua();'><img src='/gifs/EnlaceMig/gbo25360.gif' border=0 alt='Transferir'></a></td>";
			} else {
				botonEnviar="<td><a href='javascript:Enviar();'><img src='/gifs/EnlaceMig/gbo25360.gif' border=0 alt='Transferir'></a></td>";
			}
	  }


	  /* ################Despliega informacion en pantalla */
	  /*
	  if(ObjCotiza.totalRegistros>=1)
	   strTabla+=generaTabla("Cotizar",2,ObjCotiza,true,req);
	  if(NuevasNoCotiza.totalRegistros>=1)
	   strTabla+="<br>"+generaTabla("NuevoNoCotizar",2,NuevasNoCotiza,true,req);
	  strTabla+="<br><br>NuevaTrama: "+nuevaTramaTodas;
	  strTabla+="<br><br>NuevaNoCotiza"+tramasNoCotiza;
	  */

      /*MSD Q05-24447*/
	  /*System.out.println("MSD ------> Listo...");
	  System.out.println("MSD ------> TCV_Venta... " + TCV_Venta);
	  System.out.println("MSD ------> TCE_Venta... " + TCE_Venta);
	  System.out.println("MSD ------> inicialCveDivisa... " + inicialCveDivisa);*/

	  req.getSession().setAttribute("TCV_Venta",TCV_Venta);
	  req.getSession().setAttribute("TCE_Venta",TCE_Venta);
	  req.getSession().setAttribute("inicialCveDivisa",inicialCveDivisa);
	  /*MSD Q05-24447*/

	  req.setAttribute("TCV_Venta",TCV_Venta);
	  req.setAttribute("TCE_Venta",TCE_Venta);
	  req.setAttribute("inicialImporte",inicialImporte);
	  req.setAttribute("inicialDesDivisa",inicialDesDivisa);
	  req.setAttribute("inicialCveDivisa",inicialCveDivisa);
	  req.setAttribute("inicialCuentaCargo",inicialCuentaCargo);
	  req.setAttribute("BotonEnviar",botonEnviar);

	  req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
	  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
	  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
	  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

	  req.setAttribute("Tabla",strTabla);
	  req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));
	  req.setAttribute("MenuPrincipal", session.getStrMenu());
	  req.setAttribute("newMenu", session.getFuncionesDeMenu());
	  req.setAttribute("Encabezado", CreaEncabezado("Cotizaci&oacute;n Internacional","Transferencias &gt; Transferencias Internacionales &gt; Cotizaci&oacute;n","s25460h",req));

      /* Tiempo ....*/
	  req.setAttribute("Tiempo",EnlaceGlobal.fechaHoy("tm|ts|"));
	  req.setAttribute("Lineas",req.getParameter("Lineas"));
	  req.setAttribute("Importe",req.getParameter("Importe"));
	  req.setAttribute("totalRegistros","Total de Registros: "+Integer.toString(Todas.totalRegistros));
	  req.setAttribute("importeTotal","Importe Total: "+FormatoMoneda(Double.toString(importeTotal)));

	  if(servicioErroneo)
		 despliegaPaginaError("Su transacci&oacute;n no puede ser atendida en este momento.","Transferencias Internacionales","Transferencias &gt; Internacionales",req,res);
	  else
	   {
		  if(!arcExp)
		   {
			 req.setAttribute("TotalTrans",nuevaTramaTodas+tramasNoCotiza);
			 evalTemplate("/jsp/MTI_Cotizar.jsp", req, res);
		   }
		  else
		   {
			 if(ObjCotiza.totalRegistros>30)
			  {
				req.setAttribute("TotalTrans","Archivo de Exportacion");
				evalTemplate("/jsp/MTI_CotizarArchivo.jsp",req, res);
			  }
			 else
			  {
				req.setAttribute("TotalTrans",nuevaTramaTodas);
				evalTemplate("/jsp/MTI_Cotizar.jsp",req, res);
			  }
		   }
	   }
  }

/****************************************************************************************/
/***********************************   Cambia importe normal   **************************/
/****************************************************************************************/
 void cambiaImporteNormal(EI_Tipo Obj,int indice)
	{
	  String tmp="";

	  for(int i=0;i<Obj.totalRegistros;i++)
	   {
		 tmp="";
		 EIGlobal.mensajePorTrace("MTI_Cotizar - execute(): Antes: "+Obj.camposTabla[i][indice], EIGlobal.NivelLog.INFO);
		 Obj.camposTabla[i][indice]=Obj.camposTabla[i][indice].replace(',',' ').trim();
		 Obj.camposTabla[i][indice]=Obj.camposTabla[i][indice].replace('$',' ').trim();
		 EIGlobal.mensajePorTrace("MTI_Cotizar - execute(): Antes: "+Obj.camposTabla[i][indice], EIGlobal.NivelLog.INFO);
		 for(int j=0;j<Obj.camposTabla[i][indice].length();j++)
		  if(Obj.camposTabla[i][indice].charAt(j)!=' ')
		    tmp+=Obj.camposTabla[i][indice].charAt(j);
		 Obj.camposTabla[i][indice]=tmp;
		 EIGlobal.mensajePorTrace("MTI_Cotizar - execute(): Despues: "+Obj.camposTabla[i][indice], EIGlobal.NivelLog.INFO);
	   }
	}

/****************************************************************************************/
/***********************************   Extrae tipo de cambio   **************************/
/****************************************************************************************/
  String extraeDatos(String Result, String tipo,String divisa,String divisaAbono)
   {

	 String strCambio="";
	 EI_Tipo Cambio=new EI_Tipo(this);

	 Cambio.iniciaObjeto(Result.trim()+"@");
	 if(Result.substring(0,2).equals("OK"))
	  {
		if(tipo.equals("IDV"))
		  strCambio=Cambio.camposTabla[0][6];
		if(tipo.equals("IMN"))
		  strCambio=Cambio.camposTabla[0][4];
		if(tipo.equals("IDA"))
		  strCambio=Cambio.camposTabla[0][5];
		if(tipo.equals("MN"))
		  strCambio=Cambio.camposTabla[0][1];
		if(tipo.equals("DA"))
		  {
			if(divisa.trim().equals("MN"))
			  {
				EIGlobal.mensajePorTrace("MTI_Cotizar - extraeDatos(): La origen es cuenta en pesos", EIGlobal.NivelLog.DEBUG);
				if( (divisaAbono.trim().equals("USD") || divisaAbono.trim().equals("DA") ))
				  {
					EIGlobal.mensajePorTrace("MTI_Cotizar - extraeDatos(): La destino es dolares", EIGlobal.NivelLog.DEBUG);
					strCambio="1.00";
				  }
				 else
				  strCambio=Cambio.camposTabla[0][2];
		      }
			else
			  strCambio=Cambio.camposTabla[0][2];

			EIGlobal.mensajePorTrace("MTI_Cotizar - extraeDatos(): Tipo de cambio respecto al dolar: "+strCambio+" Divisa "+divisa + " Destino "+divisaAbono, EIGlobal.NivelLog.DEBUG);
		  }
		if(tipo.equals("DI"))
		  strCambio=Cambio.camposTabla[0][3];
		if(tipo.equals("HC"))
	      strCambio=Cambio.camposTabla[0][0].substring(16,Cambio.camposTabla[0][0].length());
	  }
	 else
	   strCambio="";

	 return strCambio.trim();
   }

/*************************************************************************************/
/********************** genera Tabla con icono de informacion sobre transferencia    */
/*************************************************************************************/
  String generaTabla(String titulo, int ind, EI_Tipo Trans,boolean RFC, HttpServletRequest req)
	{
/* Orden de datos en Objeto Trans
   0 Cargo
   1 Titular
   2 Abono
   3 Beneficiario
   4 Importe
   5 Concepto
   6 Divisa
   7 TCE_Venta
   8 Pais
   9 ClaveABA
  10 Ciudad
  11 Banco
  12 Fecha
  13 RFC
  14 IVA
  15 tipo Cambio
  16 Clave Especial

  17 HoraCotiza
  18 DirOinver

  19 Importe MN
  20 Importe USD
  21 Importe Divisa

  22 Fecha aplica
*/
	   String[] titulos={ "",
	                      "Cuenta Ordenante",
		                  "Nombre Ordenante",
						  "Cuenta Beneficiario",
						  "Nombre Beneficiario",
						  "Tipo de Cambio",
		                  "Importe de Operacion",
		                  "Concepto",

		                  "Importe�(MN)",
		                  "Importe (USD)",
						  "Importe (Divisa)",

		                  "Divisa",
	                      "Banco",
		                  "Ciudad",
		                  "Pais",
		                  "Clave ABA",
		                  "RFC Beneficiario",
		                  "Importe IVA",
		                  "Fecha Aplica",
		                  "Clave"};

	   int datos[]={19,0,0,1,2,3,7,4,5,19,20,21,6,11,10,8,9,13,14,22,16};
	   int values[]={2,0,1,2};
	   int align[]={0,0,0,0,2,2,0,2,2,2,1,0,0,0,1,0,2,1,1,0};

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	   EIGlobal.mensajePorTrace("MTI_Cotizar - generaTabla(): Genrando Tabla de Transferencias.", EIGlobal.NivelLog.INFO);

	   titulos[0]=titulo;
	   if(ind==0 || ind==1)
 	     datos[1]=2;

	   EIGlobal.mensajePorTrace("MTI_Cotizar - generaTabla(): Trama: "+Trans.strOriginal, EIGlobal.NivelLog.INFO);

	   EnlaceGlobal.formateaImporte(Trans,19);  //Importe  MN
	   EnlaceGlobal.formateaImporte(Trans,20);  //Importe  USD
	   EnlaceGlobal.formateaImporte(Trans,21);  //Importe  Divisa

	   EIGlobal.mensajePorTrace("MTI_Cotizar - generaTabla(): Se formatearon importes.", EIGlobal.NivelLog.INFO);

	   for(int i=0;i<Trans.totalRegistros;i++)
		{
		  //Para que despliegue o no la clave especial ...
		  if(!Trans.camposTabla[i][15].trim().equals(""))
			datos[0]=18;
		  Trans.camposTabla[i][20]=Trans.camposTabla[i][20].replace('$',' ').trim();
		  Trans.camposTabla[i][21]=Trans.camposTabla[i][21].replace('$',' ').trim();
		}
	   return Trans.generaTabla(titulos,datos,values,align);
	}

/*************************************************************************************/
/****************************************************************  Horario Valido  ***/
/*************************************************************************************/
  boolean esHorarioValido(int hora,String cheat)
	{
	   /*
		   hora 0: Todo el horario de 9 a 16
	       hora 1: Horario de 9 a 13:30
		   hora 2: Horario de 13:31 a 16:00
	   /*/
	   java.util.Date dateHoy = new java.util.Date();

       if(hora==0)
		{
		  if(cheat.indexOf("DIBTC1")>=0)
		    return true;
		  else
		   {
		     if(dateHoy.getHours()>=9 && (dateHoy.getHours()<16 && dateHoy.getMinutes()<=59) )
			   return true;
		   }
		}

	   if(hora==1)
		{
		  if(dateHoy.getHours()>=9)
		   {
		     if(dateHoy.getHours()<13)
			   return true;
		     else
			   {
			     if(dateHoy.getHours()==13 && dateHoy.getMinutes()<=30)
				  return true;
			   }
		   }
		}

	   return false;
	}

/*************************************************************************************/
/****************************************************************  Busca Segmento  ***/
/*************************************************************************************/
   String buscaSegmento(String strCuenta, String contrato)
    {
      String totales="";
	  String segmento="";
	  String cliente = "";

      int Existe=0;
	  int totalPlazas=0;

	  Connection conn = null;
	  ResultSet contResult = null;

	  try
	  {
      conn = createiASConn(Global.DATASOURCE_ORACLE);

      if(conn == null)
        EIGlobal.mensajePorTrace("MTI_Cotizar - llenaListaPlazas(): No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);

	  //cambio***
      //totales ="SELECT cve_act_inver ";
	  //totales+="FROM  nucl_cuentas a, nucl_persona b ";
	  //totales+="WHERE a.num_cuenta='"+contrato.trim()+"' and a.num_persona=b.num_persona ";

	  totales = "SELECT num_persona ";
	  totales+="FROM tct_cuentas_tele ";
	  totales+="WHERE num_cuenta='"+contrato.trim()+"'";

      PreparedStatement contQuery = conn.prepareStatement(totales);
      if(contQuery == null)
	     EIGlobal.mensajePorTrace("MTI_Cotizar - buscaSegmento()1: No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
	  contResult = contQuery.executeQuery();

      if(contResult.next())
	     EIGlobal.mensajePorTrace("MTI_Cotizar - buscaSegmento1(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);

      if(contResult.getString(1)!=null){
		 cliente=contResult.getString(1).trim();
		 NomPrePersonasDAO persDAO = new NomPrePersonasDAO(); //--
         NomPrePE68 pe68 = persDAO.consultarPersona(cliente);  //--
		 if(pe68 != null){
			 segmento = pe68.getSubsegmento();
		 }else{
			segmento="ERROR";
		 }

	  }else{
		segmento="ERROR";
	  }
		}catch( SQLException sqle){
			segmento="ERROR";
			EIGlobal.mensajePorTrace("MTI_Cotizar - buscaSegmento1(): Excepcion, Error... "+sqle.getMessage(), EIGlobal.NivelLog.INFO);
		}finally
		  {
			try
			 {
			   conn.close();
			   contResult.close();
			 }catch(Exception e) {}
		  }

	  return segmento;
    }

	/**
	 * Metodo para obtener la divisa de la cuenta
	 *
	 * @param cuenta
	 * @return Divisa
	 */
	private String getDivisaTrx(String cuenta){
		String prefijo = "";
		try{
			prefijo = cuenta.substring(0,2);
			if(prefijo.trim().equals("09") || prefijo.trim().equals("42") ||
			   prefijo.trim().equals("49") || prefijo.trim().equals("82") ||
		       prefijo.trim().equals("83") || prefijo.trim().equals("97") ||
		       prefijo.trim().equals("98")){
				return "USD";
		    }else if(prefijo.trim().equals("08") || prefijo.trim().equals("10") ||
					   prefijo.trim().equals("11") || prefijo.trim().equals("13") ||
					   prefijo.trim().equals("15") || prefijo.trim().equals("16") ||
					   prefijo.trim().equals("17") || prefijo.trim().equals("18") ||
					   prefijo.trim().equals("19") || prefijo.trim().equals("20") ||
					   prefijo.trim().equals("21") || prefijo.trim().equals("22") ||
					   prefijo.trim().equals("23") || prefijo.trim().equals("24") ||
					   prefijo.trim().equals("25") || prefijo.trim().equals("26") ||
					   prefijo.trim().equals("27") || prefijo.trim().equals("28") ||
					   prefijo.trim().equals("40") || prefijo.trim().equals("43") ||
					   prefijo.trim().equals("44") || prefijo.trim().equals("45") ||
					   prefijo.trim().equals("46") || prefijo.trim().equals("47") ||
					   prefijo.trim().equals("48") || prefijo.trim().equals("50") ||
					   prefijo.trim().equals("51") || prefijo.trim().equals("52") ||
					   prefijo.trim().equals("53") || prefijo.trim().equals("54") ||
					   prefijo.trim().equals("55") || prefijo.trim().equals("56") ||
					   prefijo.trim().equals("57") || prefijo.trim().equals("58") ||
					   prefijo.trim().equals("59") || prefijo.trim().equals("60") ||
					   prefijo.trim().equals("61") || prefijo.trim().equals("62") ||
					   prefijo.trim().equals("63") || prefijo.trim().equals("64") ||
					   prefijo.trim().equals("65") || prefijo.trim().equals("66") ||
					   prefijo.trim().equals("67") || prefijo.trim().equals("68") ||
					   prefijo.trim().equals("69") || prefijo.trim().equals("70") ||
					   prefijo.trim().equals("71") || prefijo.trim().equals("72") ||
					   prefijo.trim().equals("73") || prefijo.trim().equals("74") ||
					   prefijo.trim().equals("75") || prefijo.trim().equals("76") ||
					   prefijo.trim().equals("77") || prefijo.trim().equals("78") ||
					   prefijo.trim().equals("79") || prefijo.trim().equals("80") ||
					   prefijo.trim().equals("81") || prefijo.trim().equals("85") ||
					   prefijo.trim().equals("86") || prefijo.trim().equals("87") ||
					   prefijo.trim().equals("88") || prefijo.trim().equals("89") ||
					   prefijo.trim().equals("90") || prefijo.trim().equals("91") ||
					   prefijo.trim().equals("92") || prefijo.trim().equals("93") ||
					   prefijo.trim().equals("94") || prefijo.trim().equals("95") ||
					   prefijo.trim().equals("96") || prefijo.trim().equals("97") ||
					   prefijo.trim().equals("99")){
		    	return "MXP";
		    }
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("MIPD_cambios::getDivisaTrx:: Problemas..."+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return "USD";
	}
}