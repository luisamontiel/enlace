/**
 * Clase OpcionesNominaLn : exhibe la plantilla necesaria para dar mantenimiento
 * al archivo, asimismo, envio y recuperacion de archivo
 * responsable: Santander.
 * descripcion: en el caso de ejecutar la opcion altas, se exhibira
 * una plantilla para capturar un nuevo registro, en el caso de bajas y
 * modificaciones, muestra al usuario los campos que integran al registro, estas
 * operaciones (altas, bajas y cambios) usan la misma plantilla. Cuando el
 * usuario ha seleccionado la opcion envio, prepara los datos que han de ser
 * enviados al servicio PNLI,PVNL de WEB_RED. <br> Cuanto el usuario
 * selecciona recuperacion, el servicio es IN10, tambien a traves de WEB_RED
 *
 * @version 1.2 Refactor y modificaciones para Optimizaciones Nomina RMM
 */
package mx.altec.enlace.servlets;


import static mx.altec.enlace.utilerias.NominaConstantes.BIT_OPER_SOLPDF_NL;
import static mx.altec.enlace.utilerias.NominaConstantes.COD_OPER_PROG_DES_PDF;
import static mx.altec.enlace.utilerias.NominaConstantes.COD_OPER_PROG_SOl_PDF;
import static mx.altec.enlace.utilerias.NominaConstantes.FOR_FECHA_AMD;
import static mx.altec.enlace.utilerias.NominaConstantes.MSG_ENCAB_SOLPDF;
import static mx.altec.enlace.utilerias.NominaConstantes.MSG_SOLPDF_ERROR_PARAM;
import static mx.altec.enlace.utilerias.NominaConstantes.PARAM_DATOS_NL;
import static mx.altec.enlace.utilerias.NominaConstantes.VAL_ESTAT_SOLPDF_SOL;
import static mx.altec.enlace.utilerias.NominaConstantes.VAL_TIPO_SOLPDF_NL;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.beans.NominaLineaBean;
import mx.altec.enlace.beans.SolPDFBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.ArchivosNomina;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.DescargaEstadoCuentaBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.FormatoMoneda;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.NominaBO;
import mx.altec.enlace.bo.NominaLineaBO;
import mx.altec.enlace.bo.NominaPagosBO;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 *
 * @author z708764 consulta y pago de nomina en linea
 *
 * Modificaciones :
 * @author FSW Vector
 * @since Marzo 2015
 * Se agrega funcionalidad para manejo de solicitudes de informes en PDF
 *
 */
public class OpcionesNominaLn extends BaseServlet {

	/**
	 * Constante para la literal "archivoExportar"
	 */
	public static final String ARCHIVOEXPORTAR = "archivoExportar";
	/**
	 * Constante para la literal "secArchivo"
	 */
	public static final String SECARCHIVO = "secArchivo";
	/**
	 * Constante para la literal "[|]"
	 */
	public static final String SEPARADOR = "[|]";
	/**
	 * SESSIONX: session de aplicacion
	 */
	static final String SESSIONX="session";
	/**
	 * EJEC_PROC_VAL_PAGO: procedimiento de pago
	 */
	static final String EJEC_PROC_VAL_PAGO="Ejec_Proc_Val_Pago";
	/**
	 * valida: variable de validacion
	 */
	static final String VALIDAX="valida";
	/**
	 * newMenu: menu de aplicacion
	 */
	static final String NEWMENUX="newMenu";
	/**
	 * MenuPrincipal: menu principal de aplicacion
	 */
	static final String MENUPRINCIPALX="MenuPrincipal";
	/**
	 * operacion: operacion que se va a ejecutar
	 */
	static final String OPERACIONX="operacion";
	/**
	 * registro: valor del regostro
	 */
	static final String REGISTROX="registro";
	/**
	 * TIPOARCHIVOX: tipo de archivo a procesar
	 */
	static final String TIPOARCHIVOX="tipoArchivo";
	/**
	 * nombreArchivo: nombre del archivo de carga
	 */
	static final String NOMBREARCHIVOX="nombreArchivo";
	/**
	 * Consulta de N&oacute;mina en L&iacute;nea
	 */
	static final String CONSULTADENOMINAENLINEAX="Consulta de N&oacute;mina en L&iacute;nea";
	/**
	 * Recarga_Ses_Multiples: carga de sesiones multiple
	 */
	static final String RECARGA_SES_MULTIPLESX="Recarga_Ses_Multiples";
	/**
	 * ses: valoir de session
	 */
	static final String SESX=".ses";
	/**
	 * /tmp/ ruta local
	 */
	static final String TEMPX="/tmp/";
	/**
	 * TRUE: bandera de control
	 */
	static final String TRUEX="TRUE";
	/**
	 * statusDuplicado: estatus duplicado
	 */
	static final String STATUSDUPLICADOX="statusDuplicado";
	/**
	 * statushrc: estatus de archivo
	 */
	static final String STATUSHRCX="statushrc";
	/**
	 * encFechApli: fecha aplicacion de encabezado
	 */
	static final String ENCFECHAPLIX="encFechApli";
	/**
	 * folio: folio de la operacion
	 */
	static final String FOLIOX="folio";
	/**
	 * Encabezado: encabezado de aplicacion
	 */
	static final String ENCABEZADOX="Encabezado";
	/**
	 * Pago de nomina en Linea
	 */
	static final String PAGODENOMINAENLINEAX="Pago de N&oacute;mina en L&iacute;nea";
	/**
	 * Servicios / Nomina / Pagos en Linea
	 */
	static final String SERVICIOSNOMINAPAGOSENLINEAX="Servicios &gt; N&oacute;mina &gt; Pagos en L&iacute;nea";
	/**
	 * S25800IIIH: valor de aplicacion
	 */
	static final String S25800IIIH= "s25800IIIh";
	/**
	 * OPERACIONRECHAZADAX: operacion rechazada
	 */
	static final String OPERACIONRECHAZADAX="<B>Operaci&oacute;n Rechazada</B>";
	/**
	 * CUADRODIALOGOPAGOSNOMINAX: cuadro dialogo de mensajes
	 */
	static final String CUADRODIALOGOPAGOSNOMINAX="cuadroDialogoPagosNomina(\"";
	/**
	 * INFOUSERX: informacion de usuario
	 */
	static final String INFOUSERX="infoUser";
	/**
	 * facultadesN: facultades nuevas
	 */
	static final String FACULTADESNX="facultadesN";
	/**
	 * facultades: facultades de aplicacion
	 */
	static final String FACULTADESX="facultades";
	/**
	 * ContenidoArchivo: contenido del archivo de carga
	 */
	static final String CONTENIDOARCHIVOX="ContenidoArchivo";
	/**
	 * archDuplicado: archivo duplicado
	 */
	static final String ARCHDUPLICADOX="archDuplicado";
	/**
	 * digit: digito
	 */
	static final String DIGITX="digit";
	/**
	 * HR_P: valor de hora
	 */
	static final String HR_PX="HR_P";
	/**
	 * horario_seleccionado: horario seleccionado que NO aplica
	 */
	static final String HORARIO_SELECCIONADOX="horario_seleccionado";
	/**
	 * HorarioNormal: horario normal
	 */
	static final String HORARIONORMALX="HorarioNormal";
	/**
	 * tag tr html
	 */
	static final String TR= "<tr> ";
	/**
	 * tag tr html
	 */
	static final String TRC="</tr>";
	/**
	 * tag class html
	 */
	static final String TDCLASS="<td class=\"";
	/**
	 * tag de alineacion
	 */
	static final String NOWRAPALINGCENTER="\" nowrap align=\"center\">";
	/**
	 * tag de espacio
	 */
	static final String ESPACIOTD="&nbsp;</td>";
	/**
	 * DESCPCUENTACARGOX: descripcion de cuenta cargo
	 */
	static final String DESCPCUENTACARGOX="descpCuentaCargo";
	/**
	 * archivo_actualx: archivo actual
	 */
	static final String ARCHIVO_ACTUALX="archivo_actual";
	/**
	 * tag br
	 */
	static final String BR="<br>";
	/**
	 * tag table
	 */
	static final String TABLE="<table align=center border=0 cellspacing=0 cellpadding=0><tr>";
	/**
	 * imprimir: boton imprimir
	 */
	static final String IMPRIMIR="<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>";
	/**
	 * tag termina table
	 */
	static final String TERMINATABLE="<td></td></tr> </table>";
	/**
	 * BOTONESX: botones aplicacion
	 */
	static final String BOTONESX="botones";
	/**
	 * NOMBRE_ARCH_SALIDAX: nombre archivo de salida
	 */
	static final String NOMBRE_ARCH_SALIDAX="nombre_arch_salida";
	/**
	 * Errores filesrvr: error de vfilesrvr
	 */
	static final String ERROREFILESRVR="Error al ejecutar validacion de cuenta por medio de vfilesrvr";
	/**
	 * datos generales de aplicaicon
	 */
	static final String DATOSGENERALESX="<DATOS GENERALES>";
	/**
	 * USUARIOX: usuario de cliente
	 */
	static final String USUARIOX="Usuario->";
	/**
	 * CONTRATOX: contrato de cliente
	 */
	static final String CONTRATOX="Contrato->";
	/**
	 * MODULOACONSULTARX: modulo consulta de archivos
	 */
	static final String MODULOACONSULTARX="Modulo a consultar->";
	/**
	 * LINEADETRUENEX: linea de control
	 */
	static final String LINEADETRUENEX="Linea de truene->";
	/**
	 * mensaje error general
	 */
	static final String SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX="SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->";
	/**
	 * BUFFERX: control buffer
	 */
	static final String BUFFERX="BUFFER";
	/**
	 * CANTIDADDEREGISTROSENARCHIVOX: cantidad de registros en archivo
	 */
	static final String CANTIDADDEREGISTROSENARCHIVOX="cantidadDeRegistrosEnArchivo";
	/**
	 * TEXTCUENTAX: texto de cuenta
	 */
	static final String TEXTCUENTAX="textcuenta";
	/**
	 * ceros: valor monetario
	 */
	static final String CEROS="0.00";
	/**
	 * serialVersionUID Version de clase
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constante para la cadena "Concepto"
	 */
	private static final String STR_CONCEPTO = "Concepto";

	/**
	 * Constante para la cadena "null"
	 */
	private static final String STR_NULL = "null";

	/**
	 * Constante para la cadena "sumTmpSession"
	 */
	private static final String STR_SUM_TMP_SESSION = "sumTmpSession";

	/**
	 * Constante para la cadena "NOMIencFechApli"
	 */
	private static final String STR_NOM_IENC_FECH_APLI = "NOMIencFechApli";

	/**
	 * Constante para la cadena "VALIDAR"
	 */
	private static final String STR_VALIDAR = "VALIDAR";

	/**
	 * Constante para la cadena "strCheck"
	 */
	private static final String STR_CHECK = "strCheck";

	/**
	 * myCalendarNomina calendario nomina
	 */
	CalendarNomina myCalendarNomina;
	/**
	 * textoLog: trace de clase
	 */
	static String textoLog ="";
	/**
	 * diasNoHabiles dias no habiles
	 */
	static Vector diasNoHabiles;
	/**
	 * Metodo que obtiene el texto que se pintara en el log
	 * @param funcion parametro de funcion
	 * @return textoLog cadena de log
	 */
	private String textodeLog(String funcion){
		return textoLog = OpcionesNominaLn.class.getName() + "." + funcion + "::";
	}
	/**
	 * inicio defaultaccion
	 * @param request recibir parametros
	 * @param response respuesta del servlet
	 * @throws IOException control de excepciones
	 * @throws ServletException control de excepciones en servlet
	 * @throws ServletException control de excepciones del servlet
	 * @throws IOException control de excepciones de archivo
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}
	/**
	 * Respuesta del servlet
	 * @param request recepcion de parametros
	 * @param response respuesta de servlet
	 * @throws IOException control de excepciones
	 * @throws ServletException control de excepciones en servlet
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}
	/**
	 * Metodo DefaultAction de la aplicacion
	 * @param request recepcion de atributos
	 * @param response respuesta de servlet
	 * @throws IOException control de excepciones
	 * @throws ServletException control de excepciones en servlet
	 */
	public void defaultAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		textodeLog("defaultAction");
		final boolean sesionvalida = SesionValida(request, response);
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
		boolean solicitarOTP = true;
		byte strDescFav[] = null;
		String dscFav = null;
		if (sesionvalida) {
						
			//M022532 NOMINA EN LINEA	 		
			   String tipoCargo = "1";
			   Object tipoCargoObj =request.getSession().getAttribute("TipoCargoIGNomLin");
			   if(tipoCargoObj!=null)
				 {
				 tipoCargo=(String)tipoCargoObj;
				 }
			   
			   EIGlobal.mensajePorTrace( "OpcionesNominaLn ::: INICIO ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
			//M022532 -FIN
			
			try {
				diasNoHabiles = new Vector();
				myCalendarNomina= new CalendarNomina( );
			}
			catch(Exception e)
			{
				despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP ,PAGODENOMINAENLINEAX, "Importando Archivo", "s25800h", request, response );
				EIGlobal.mensajePorTrace("***InicioNominaLn.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
			}
			// se verifica el llamado del servicio para saber si se trata de
			// validaci�n de duplicados o ejecuci�n de la transacci�n
			if (request.getSession().getAttribute(EJEC_PROC_VAL_PAGO) != null) {
				EIGlobal.mensajePorTrace(textoLog + "Se va a efectuar la transaccion", EIGlobal.NivelLog.DEBUG);
				solicitarOTP = true;
			}
			// Inicia validacion OTP
			String valida = "";
			/**
			 * Inicio PYME 2015 Unificacion Token
			 */
			if( request.getAttribute("forzar") != null && "si".equals(request.getAttribute("forzar")) ){
				valida = null;
				request.setAttribute("cargarValores", "si");
				request.setAttribute("infoImportados", null);
			} else {
				valida = request.getParameter(VALIDAX);
			}
			/**
			 * Fin PYME 2015 Unificacion Token
			 */
			EIGlobal.mensajePorTrace("request.getParameter(valida)["+request.getParameter(VALIDAX)+"]", EIGlobal.NivelLog.DEBUG);
			if (validaPeticion(request, response, session, sess, valida)) {
				EIGlobal.mensajePorTrace(textoLog + "Entro a validar Peticion", EIGlobal.NivelLog.DEBUG);
			}
			// Termina validacion OTP
			else {
				session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
				EIGlobal.mensajePorTrace(textoLog, EIGlobal.NivelLog.DEBUG);
				request.setAttribute("Fecha", ObtenFecha());
				request.setAttribute("ContUser", ObtenContUser(request));
				String nombre_arch_salida = "";
				request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
				request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
				request.setAttribute("Fechas", ObtenFecha(true));
				request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");
				request.setAttribute("botLimpiar", "");
				/**
				 * Se comenta linea para que obtenga valores como atributos
				 * ya que asi fueron asignados
				 * PYME 2015 UNIFICACION DE TOKEN
				 * String strOperacion = (String) getFormParameter(request, OPERACIONX);
				 */
				String strOperacion = "";
				if ( request.getAttribute(OPERACIONX) == null ) {
					strOperacion = (String) getFormParameter(request, OPERACIONX);
				} else {
					strOperacion = request.getAttribute(OPERACIONX).toString();
				}
				if (strOperacion == null || ("").equals(strOperacion)) {
					strOperacion = "";
				}
				String strRegistro = (String) getFormParameter(request, REGISTROX);
				if (strRegistro == null || ("").equals(strRegistro)) {
					strRegistro = "";
				}
				request.setAttribute(OPERACIONX, strOperacion);
				/**
				 * Se comenta linEa para que obtenga valores como atributos
				 * ya que asi fueron asignados
				 * PYME 2015 UNIFICACION DE TOKEN
				 * String tipoArchivotmp = getFormParameter(request, TIPOARCHIVOX);
				 */
				String tipoArchivotmp = "";
				if ( request.getAttribute(TIPOARCHIVOX) == null ) {
					tipoArchivotmp = (String) getFormParameter(request, TIPOARCHIVOX);
				} else {
					tipoArchivotmp = request.getAttribute(TIPOARCHIVOX).toString();
				}

				EIGlobal.mensajePorTrace(textoLog + "tipoArchivo [" + tipoArchivotmp + "]", EIGlobal.NivelLog.DEBUG);
				if (tipoArchivotmp == null || ("").equals(tipoArchivotmp)) {
					tipoArchivotmp = "";
				}
				if (("regresarPagos").equals(strOperacion)) {// regresara a la pagina inicial
					inicioPagos(request, response);
				} else if (("descarga").equals(strOperacion)) {// Para descargar el archivo. Se envia desde el application
					descargaArchivos(request, response);
				} else if (("regresar").equals(strOperacion)) {// regresara a la pagina inicial
					inicioConsulta(request, response);
				} else if (("envio").equals(strOperacion)) {
					EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Verificando datos en sesion", EIGlobal.NivelLog.DEBUG);
					String Concepto = "";
					String archivoConcepto = "";
					if (sess.getAttribute("conceptoEnArchivo") != null) {
						EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Si existe en sesion! Se lee el parametro concepto", EIGlobal.NivelLog.DEBUG);
						Concepto = (String) getFormParameter(request, STR_CONCEPTO);
						archivoConcepto = (String) sess.getAttribute(NOMBREARCHIVOX);
						EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Concepto: " + Concepto, EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: archivoConcepto: " + archivoConcepto, EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: concepto_seleccionado OVI: " + Concepto, EIGlobal.NivelLog.DEBUG);
						if(null != Concepto && !STR_NULL.equals(Concepto))
						{
							if (actualizaConceptoEnArchivo(archivoConcepto, Concepto)) {
								EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Se actualizo el archivo", EIGlobal.NivelLog.DEBUG);
								sess.removeAttribute("conceptoEnArchivo");
							} else {
								despliegaPaginaError(
									IEnlace.MSG_PAG_NO_DISP,
									CONSULTADENOMINAENLINEAX,
									"Servicios &gt; Env&iacute;o de Archivos de N&oacute;mina en L&iacute;nea",
									request, response);
								return;
							}
						}
					} else {
						EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Los registros en archivo contienen el concepto", EIGlobal.NivelLog.DEBUG);
					}
					EIGlobal.mensajePorTrace(textoLog + "Antes de Verificacion de Token, solicitarOTP = " + solicitarOTP, EIGlobal.NivelLog.DEBUG);
					// interrumpe la transaccion para invocar la validaci�n
					boolean valBitacora = true;
					if (valida == null) {
						EIGlobal.mensajePorTrace(textoLog + "Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP", EIGlobal.NivelLog.DEBUG);
						final boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);
						if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal && solicitarOTP) {
							EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
							guardaParametrosEnSession(request);
							EIGlobal.mensajePorTrace("No de registros importados ->" + session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
							if (session.getNominaNumberOfRecords() != null) {
								sess.setAttribute("noRegistrosNomina", session.getNominaNumberOfRecords().trim());
							}
							request.getSession().removeAttribute("mensajeSession");
							ValidaOTP.mensajeOTP(request, "PagoNomina");
							/**
							 * Se comenta linea para la unifiacion del token
							 * ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
							 * PYME 2015 Unificacion Token
							 * Se agrega Atributo (mostrarToken)para mostrar u ocultar el token
							 */
//							ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
							EIGlobal.mensajePorTrace("2 Ln === PYME", EIGlobal.NivelLog.INFO);
							if (request.getParameter("TIPO_TOKEN") != null
									&& "tokenOriginal".equals(request
											.getParameter("TIPO_TOKEN"))) {
								//Aqui entra cuando viene de duplicados
								ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
							} else {
								request.setAttribute("mostrarToken", "true");
								request.setAttribute("vieneDe", "OpcionesNominaLn");
								ValidaOTP.validaOTP(request,response, "/jsp/MantenimientoNominaLn.jsp");
							}
						} else {
							valida = "1";
							valBitacora = false;
						}
					}
					// retoma el flujo
					if (("1").equals(valida)) {
						boolean banderaTransaccion = true;
						// PARA VALIDAR Y DESPU�S CARGAR EL ARCHIVO
						if (request.getSession().getAttribute( EJEC_PROC_VAL_PAGO) != null) {
							EIGlobal.mensajePorTrace(textoLog + "Es recarga de procedimiento por VALIDACION.", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = true;
							if (valBitacora)
							{
								try{
									final int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									final HttpSession sessionBit = request.getSession(false);
									final String claveOperacion = BitaConstants.CTK_PAGO_NOMINALN;
									final String concepto = BitaConstants.CTK_CONCEPTO_PAGO_NOMINALN;
									String token = "0";
									if (request.getParameter("token") != null && !("").equals(request.getParameter("token"))) {
										token = request.getParameter("token");
									}
									double importeDouble = 0;
									String cuenta = "0";
									if (session.getCuentaCargo() != null && !("").equals(session.getCuentaCargo())) {
										cuenta = session.getCuentaCargo();
									}
									if (sessionBit.getAttribute(STR_SUM_TMP_SESSION) != null && ! ("").equals(sessionBit.getAttribute(STR_SUM_TMP_SESSION))) {
										final String importestr = sessionBit.getAttribute(STR_SUM_TMP_SESSION).toString();
										importeDouble = Double.valueOf(importestr);
									}
									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

									@SuppressWarnings("unused")
									final String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
								} catch(Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							}
						} else if (request.getSession().getAttribute(RECARGA_SES_MULTIPLESX) != null
								&& verificaArchivos(TEMPX + request.getSession().getId().toString() + SESX, false)) {
							EIGlobal.mensajePorTrace(textoLog + "El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = false;
						} else if (request.getSession().getAttribute(RECARGA_SES_MULTIPLESX) != null) {
							EIGlobal.mensajePorTrace(textoLog + "Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
							request.getSession().removeAttribute(RECARGA_SES_MULTIPLESX);
							banderaTransaccion = false;
						} else if (verificaArchivos(TEMPX + request.getSession().getId().toString() + SESX, false)) {
							EIGlobal.mensajePorTrace(textoLog + "El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = false;
						} else if (request.getSession().getAttribute(RECARGA_SES_MULTIPLESX) == null
								&& !verificaArchivos(TEMPX + request.getSession().getId().toString() + SESX, false)) {
							EIGlobal.mensajePorTrace(textoLog + "Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion = true;
						}
						if (request.getSession().getAttribute(EJEC_PROC_VAL_PAGO) != null) {
							request.getSession().removeAttribute(EJEC_PROC_VAL_PAGO);
							EIGlobal.mensajePorTrace(textoLog + "Atributo EJEC_PROC_VAL_PAGO REMOVIDO.", EIGlobal.NivelLog.DEBUG);
						}
						EIGlobal.mensajePorTrace(textoLog + "banderaTransaccion:" + banderaTransaccion, EIGlobal.NivelLog.DEBUG);
						/* Termina construccion variables control recarga sesion*/
						if (banderaTransaccion) {
							// Se crea el archivo para control de recarga sesion, y puesta de var. Recarga
							final EI_Exportar arcTmp = new EI_Exportar(TEMPX + request.getSession().getId().toString() + SESX);
							if (arcTmp.creaArchivo()) {
								EIGlobal.mensajePorTrace(textoLog + "Archivo .ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);
							}
							request.getSession().setAttribute(RECARGA_SES_MULTIPLESX, TRUEX);
							EIGlobal.mensajePorTrace(textoLog + "Atributo EJEC_PROC_VAL_PAGO Creado.", EIGlobal.NivelLog.DEBUG);
							//Se modificar� para enviar en un solo servicio el
							//archivo, como lo envia la nomina interbancaria
							String nombreArchivo = (String) sess.getAttribute(NOMBREARCHIVOX)+"/";
							final String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
							/**
							 * PYME 2015 Unificacion Token
							 */
							String digit = getFormParameter(request,
									STATUSDUPLICADOX) == null ? request
									.getAttribute(STATUSDUPLICADOX).toString()
									: getFormParameter(request,
											STATUSDUPLICADOX);
							final String digit_d = getFormParameter(request,
									STATUSHRCX) == null ? request
									.getAttribute(STATUSHRCX).toString()
									: getFormParameter(request, STATUSHRCX);
							EIGlobal.mensajePorTrace(textoLog + "nueva_ruta ["+ nueva_ruta + "]", EIGlobal.NivelLog.DEBUG);
							nombreArchivo = nombreArchivo.substring(posCar(nombreArchivo, '/', 4) + 1, posCar(nombreArchivo, '/', 5));
							final String rutaArch = nueva_ruta + "/" + nombreArchivo;
							EIGlobal.mensajePorTrace(textoLog + "nombreArchivo dentro de envio" + nombreArchivo, EIGlobal.NivelLog.DEBUG);
							try {
								String tipoCopia = "3";
								final ArchivoRemoto archR = new ArchivoRemoto();
								if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia)) {
									EIGlobal.mensajePorTrace(textoLog + "No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
								}

								//FIXME Se implementa funcion para copiar archivo a la ruta de contingencia
								final String ruta_contingente = Global.directorioContingentenln;//Directorio de contingencia
								EIGlobal.mensajePorTrace(textoLog + "Inicio Copiando en contingencia.... "+ruta_contingente, EIGlobal.NivelLog.DEBUG);
								tipoCopia = "1";
								if (!archR.copia(nombreArchivo, ruta_contingente, tipoCopia)) {
									EIGlobal.mensajePorTrace(textoLog + "No se pudo copiar el archivo de contingencia", EIGlobal.NivelLog.DEBUG);
								}
								EIGlobal.mensajePorTrace(textoLog + "Fin copiando en contingencia....", EIGlobal.NivelLog.DEBUG);
							} catch (Exception ioeSua) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
							}
							/**
							 * Se comenta linea para que obtenga valores como atributos
							 * ya que asi fueron asignados
							 * PYME 2015 UNIFICACION DE TOKEN
							 * String fechaAplicacion = (String) getFormParameter(request, ENCFECHAPLIX);//encFechApli
							 */
							String fechaAplicacion = "";//encFechApli
							if ( request.getAttribute(ENCFECHAPLIX) == null ) {
								fechaAplicacion = (String) getFormParameter(request, ENCFECHAPLIX);
							} else {
								fechaAplicacion = request.getAttribute(ENCFECHAPLIX).toString();
							}

							request.setAttribute(ENCFECHAPLIX, fechaAplicacion);
							EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion PARAMETER" + getFormParameter(request, ENCFECHAPLIX), EIGlobal.NivelLog.DEBUG);
							if (fechaAplicacion != null) {
								sess.setAttribute(STR_NOM_IENC_FECH_APLI, fechaAplicacion);
							}
							if (fechaAplicacion != null) {
								EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion dentro de envio LENGTH" + getFormParameter(request, ENCFECHAPLIX), EIGlobal.NivelLog.DEBUG);
								fechaAplicacion = fechaAplicacion.trim();
								fechaAplicacion = fechaAplicacion.substring(2, 4) + fechaAplicacion.substring(0, 2) + fechaAplicacion.substring(4, 8);
							} else {
								EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion dentro de envio" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
								fechaAplicacion = (String) sess.getAttribute(STR_NOM_IENC_FECH_APLI);
								fechaAplicacion = fechaAplicacion.trim();
								fechaAplicacion = fechaAplicacion.substring(2,4) + fechaAplicacion.substring(0, 2) + fechaAplicacion.substring(4, 8);
							}
							final java.util.Date dt = new java.util.Date();
							final String HorarioSele = "";
							/*
							 * Se cambia horario ya que despues de las 16:30 no
							 * se puede enviar nada de informacion.
							 */
							EIGlobal.mensajePorTrace(textoLog + "Validacion de fecha", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textoLog + "Fecha del dia de hoy (true) " + ObtenFecha(true), EIGlobal.NivelLog.DEBUG);
							final String fecha_hoy = ObtenFecha(true);
							final String dd_hoy = fecha_hoy.substring(0, 2);
							final String mm_hoy = fecha_hoy.substring(3, 5);
							final String aa_hoy = fecha_hoy.substring(6, 10);
							EIGlobal.mensajePorTrace(textoLog + "Parametros " + "dd_hoy=" + dd_hoy + ", mm_hoy=" + mm_hoy + ", aa_hoy=" + aa_hoy, EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textoLog + "fecha de hoy (false)" + ObtenFecha(false), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textoLog + "Fecha Aplicacion fechaAplicacion.substring(0,2)=" + fechaAplicacion.substring(0, 2) + ", fechaAplicacion.substring(2,4)=" + fechaAplicacion.substring(2, 4) + ", fechaAplicacion.substring(4,8)=" + fechaAplicacion.substring(4, 8), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textoLog +  "digit >>" + digit + "<<", EIGlobal.NivelLog.INFO);


							EIGlobal.mensajePorTrace(textoLog + "dd_hoy >> " + dd_hoy +"fechaAplicacion.substring(0, 2) >>"+fechaAplicacion.substring(0, 2)+ "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "mm_hoy >> " + mm_hoy +"fechaAplicacion.substring(2, 4) >>"+fechaAplicacion.substring(2, 4)+ "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "aa_hoy >> " + aa_hoy +"fechaAplicacion.substring(4, 8) >>"+fechaAplicacion.substring(4, 8)+ "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "dt.getHours() >> " + dt.getHours() + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "dt.getMinutes() >> " + dt.getMinutes() + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "Global.nlnHora >> " + Global.nlnHora + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "Global.nlnMinutos >> " + Global.nlnMinutos + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "Global.nlnHora)+1 >> " + (Integer.parseInt(Global.nlnHora)+1) + "<<", EIGlobal.NivelLog.INFO);
							/**
							 * PYME 2015 UNIFICACION DE TOKEN
							 * No mostrar mensaje de reg importados
							 */
							request.setAttribute("infoImportados", null);
							if ((dd_hoy.equals(fechaAplicacion.substring(0, 2))
									&& mm_hoy.equals(fechaAplicacion.substring(2, 4))
									&& aa_hoy.equals(fechaAplicacion.substring(4, 8)))
									&& ((dt.getHours() == Integer.parseInt(Global.nlnHora)
									&& dt.getMinutes() >= Integer.parseInt(Global.nlnMinutos))
									|| dt.getHours() >= (Integer.parseInt(Global.nlnHora)+1) )) {//Hora limieta 08 pm
								EIGlobal.mensajePorTrace(textoLog + "Horario Premier >>" + HorarioSele + "<<", EIGlobal.NivelLog.INFO);
								request.setAttribute(FOLIOX, (String) getFormParameter(request, FOLIOX));
								request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
								request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
								request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAENLINEAX, SERVICIOSNOMINAPAGOSENLINEAX, S25800IIIH, request));
								StringBuffer infoUser=new StringBuffer();
								infoUser.append(CUADRODIALOGOPAGOSNOMINAX);
								infoUser.append(OPERACIONRECHAZADAX);
								infoUser.append("\",\"");
								infoUser.append("HORA LIMITE PARA APLICAR PAGOS HOY ES 19:40 HRS.");
								infoUser.append("\",10)");
								request.setAttribute(INFOUSERX, infoUser.toString());
								if (sess.getAttribute(FACULTADESNX) != null) {
									request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
								}
								request.setAttribute(CONTENIDOARCHIVOX, sess.getAttribute(CONTENIDOARCHIVOX));
								sess.setAttribute(ARCHDUPLICADOX, "si");
								request.setAttribute(DIGITX, "0");
								evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
								return;
							}
							EIGlobal.mensajePorTrace(textoLog +  "digit 2 >> " + digit + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion FINAL" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
							String contrato = session.getContractNumber();
							final String empleado = session.getUserID8();
							final String perfil = session.getUserProfile();
							final String cuenta = session.getCuentaCargo();
							String importe = session.getImpTotalNom().trim();
							final String importePunto = agregarPunto(importe);
							final String totalRegNom = session.getTotalRegsNom().trim();
							String tramaEntrada = "";
							String horarioPremier = "";
							String FacultadPremier = "";
							FacultadPremier = (String) sess.getAttribute(FACULTADESNX);
							EIGlobal.mensajePorTrace(textoLog + "Facultad Premier CCBFP " + FacultadPremier, EIGlobal.NivelLog.INFO);
							sess.setAttribute(HR_PX, getFormParameter(request, HORARIO_SELECCIONADOX));
							EIGlobal.mensajePorTrace(textoLog + "BOX 1", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "sess.getAttribute(HR_P) >>" + sess.getAttribute(HR_PX) + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "digit 3 >>" + digit_d + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "sess.getAttribute(archDuplicado)>>" + sess.getAttribute(ARCHDUPLICADOX) + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "BOX 1", EIGlobal.NivelLog.INFO);
							if (digit.compareTo("0") == 0) {
								horarioPremier = (String) sess.getAttribute(HR_PX);
								EIGlobal.mensajePorTrace(textoLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.INFO);
								digit = "0";
							}
							// La primera vez invoca servicio NOMI_VAL_PAGO para validar archivo.
							String servicio = "NLVP"; //Se cambia NOVP por NLVP
							// Consulta variable global para saber si debe seguir validando el archivo.
							String validar = (String) sess.getAttribute(STR_VALIDAR);
							EIGlobal.mensajePorTrace(textoLog + "Validar [" + validar + "]", EIGlobal.NivelLog.DEBUG);
							// Archivo enviado solo se puede procesar en el horario 17:00
							String HorarioNormal = (String) sess .getAttribute(HORARIONORMALX);
							if (HorarioNormal == null) {
								HorarioNormal = "";
							}
							if (("1").equals(HorarioNormal)) {
								if (digit_d.compareTo("0") == 0) {
									horarioPremier = "17:00";
									EIGlobal.mensajePorTrace(textoLog + "horarioPremier 17:00 >>" + horarioPremier + "<<", EIGlobal.NivelLog.INFO);
									digit = "0";
								}
							}
							/*
							 * Si ya se pasaron todas las validaciones del
							 * archivo, invocar al servicio NOMI_PAGO para
							 * procesar el archivo.
							 */
							if (validar == null) {
								validar = "";
							}
							if (validar.compareTo("N") == 0) {
								servicio = "PNLI";//Se cambia IN04 por PNLI
							}//Se agrega validacion "si".equals(sess.getAttribute(ARCHDUPLICADOX) para que envie el archivo aunque este duplicado
							if("si".equals(sess.getAttribute(ARCHDUPLICADOX))){
								servicio = "PNLI";//Se cambia IN04 por PNLI
								//Limpia variables de sesi�n usadas en dispersiones anteriores
								sess.removeAttribute(STR_VALIDAR);
								sess.removeAttribute("HOR_SAT");
								sess.removeAttribute("HorarioNormal");
								sess.removeAttribute(STR_VALIDAR);
								sess.removeAttribute("FOLIO");
								sess.removeAttribute("HR_P");
								sess.removeAttribute("archDuplicado");
								sess.removeAttribute(STR_NOM_IENC_FECH_APLI);

								//TODO EJECUTAR INSERT EWEB_NOMLIN_ARCH
								/**** Inicio de inserccion a la tabla eweb_nomlin_arch ****/
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inicia inserrcion de nueva tabla ::: ", EIGlobal.NivelLog.INFO);
								final HttpSession sessionBit = request.getSession(false);
								final SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:MM", new java.util.Locale("es","mx"));
								final Date fecha=new Date();
								final String fchCarga=f.format(fecha);

								NominaLineaBO bo=new NominaLineaBO();
								NominaLineaBean bean=new NominaLineaBean();
								bean.setContrato(session.getContractNumber());
								bean.setNomArchivo(nombreArchivo);
								bean.setNumRegistros(session.getNominaNumberOfRecords());
								bean.setImporteRecibido(sessionBit.getAttribute(STR_SUM_TMP_SESSION).toString());
								bean.setFchCarga(fchCarga);
								/**** Inicio de actualizacion para la tabla eweb_nomlin_cata ****/
								bean=validaTerciaBiatux(bean);
								/**** Fin de actualizacion para la tabla eweb_nomlin_cata ****/
								//M022532 NOMINA EN LINEA - INI
								bean.setTipoCargoArch(tipoCargo);
								//M022532 NOMINA EN LINEA - FIN
								
								
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta duplicado :: contrato 		:: " + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta duplicado :: nombreArchivo	:: " + nombreArchivo, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta duplicado :: noRegistros 	:: " + session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta duplicado :: importeRecibido :: " + sessionBit.getAttribute(STR_SUM_TMP_SESSION).toString(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta duplicado :: fchCarga 		:: " + fchCarga, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta duplicado :: servidorBiatux	:: " + bean.getServidorBiatux(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta duplicado :: tipoCargoArch   :: " + bean.getTipoCargoArch(), EIGlobal.NivelLog.INFO);

								boolean resultado=bo.registraNomlinArch(bean);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: se termina de registrar duplicado :: " + resultado, EIGlobal.NivelLog.INFO);
								/**** Fin de inserccion a la tabla eweb_nomlin_arch ****/
							}
							/**
							 * eliminas la posibilidad de que el valor del contrato viaje nulo al back-end UNIX
							 * si es null o esta vacia o tiene la palabra null, nuevamente solicita el contrato de sesi�n
							 */
							if(contrato == null || ("").equals(contrato) || (STR_NULL).equals(contrato)) {
								contrato = session.getContractNumber();
							}
							//CSA ---------------- CANDADO PARA PREVENIR ERROR DE CUENTA NULL
							//cuenta = null;
							if (cuenta == null || ("").equals(cuenta) || ("NULL").equals(cuenta) || (STR_NULL).equals(cuenta) || ("").equals(cuenta)) {
								EIGlobal.mensajePorTrace(textoLog + "----- CSA ------ null en la cuenta cargo", EIGlobal.NivelLog.INFO);
								request.setAttribute(FOLIOX, (String) getFormParameter(request, FOLIOX));
								request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
								request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
								request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAENLINEAX, SERVICIOSNOMINAPAGOSENLINEAX, S25800IIIH, request));
								String infoUser = "Error al enviar el archivo, intente mas tarde.";
								final String infoEncabezadoUser = OPERACIONRECHAZADAX;
								infoUser = CUADRODIALOGOPAGOSNOMINAX + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
								request.setAttribute(INFOUSERX, infoUser);
								if (sess.getAttribute(FACULTADESNX) != null) {
									request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
								}
								request.setAttribute(CONTENIDOARCHIVOX, sess.getAttribute(CONTENIDOARCHIVOX));
								sess.setAttribute(ARCHDUPLICADOX, "si");
								request.setAttribute(DIGITX, "0");
								evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
								return;
							}
							//----------- CSA ---------------------------
							tramaEntrada = "1EWEB|" + empleado + "|" + servicio
									+ "|" + contrato + "|" + empleado + "|" + perfil+ "|" + empleado
									+ "@" + contrato + "@" + fechaAplicacion + "@"
									+ totalRegNom + "@" + importePunto + "@"
									//M022532 NOMINA EN LINEA - INI
									//+ cuenta + "@" + rutaArch + "@|"+" | | |";
									+ cuenta + "@" + rutaArch +"@"+tipoCargo+"@|"+" | | |";
									//M022532 NOMINA EN LINEA - FIN
                            EIGlobal.mensajePorTrace(textoLog + "Trama que se enviara >>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);
							String tramaSalida = "";
							String folioArchivo = "";
							if (("PNLI").equals(servicio)) {//Se cmabia IN04 por PNLI
								final LYMValidador val = new LYMValidador();
								final String resLYM = val.validaLyM(tramaEntrada);
								val.cerrar();
								if(!("ALYM0000").equals(resLYM)) {
									request.setAttribute(FOLIOX, (String) getFormParameter(request, FOLIOX));
									request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
									request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
									request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAENLINEAX, SERVICIOSNOMINAPAGOSENLINEAX, S25800IIIH, request));
									String infoUser = resLYM;
									final String infoEncabezadoUser = OPERACIONRECHAZADAX;
									infoUser = CUADRODIALOGOPAGOSNOMINAX + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
									request.setAttribute(INFOUSERX, infoUser);
									if (sess.getAttribute(FACULTADESNX) != null) {
										request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
									}
									request.setAttribute(CONTENIDOARCHIVOX, sess.getAttribute(CONTENIDOARCHIVOX));
									sess.setAttribute(ARCHDUPLICADOX, "si");
									request.setAttribute(DIGITX, "0");
									evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
									return;
								}

								// Servicio para folio -------------------------
								folioArchivo = generaFolio(session.getUserID8(), session.getUserProfile(), contrato, servicio, totalRegNom);
								session.setFolioArchivo(folioArchivo);
								request.setAttribute("folioArchivo", folioArchivo);
								EIGlobal.mensajePorTrace(textoLog + "folioArchivo >>" + folioArchivo + "<<", EIGlobal.NivelLog.INFO);
								// Env�o de Datos ------------------------------
								final String mensajeErrorEnviar = envioDatos(session.getUserID8(), session.getUserProfile(), contrato, folioArchivo, servicio, rutaArch, tramaEntrada);
								EIGlobal.mensajePorTrace(textoLog + "mensajeErrorEnviar>>" + mensajeErrorEnviar + "<<", EIGlobal.NivelLog.ERROR);
								// Servicio dispersor de transacciones ---------
								if (("OK").equals(mensajeErrorEnviar)) {
									tramaSalida = realizaDispersion(session.getUserID8(), session.getUserProfile(), contrato, folioArchivo, servicio);
								}
							} else {
								tramaSalida = ejecutaServicio(tramaEntrada);

							}



							EIGlobal.mensajePorTrace(textoLog + "Trama salida>>" + tramaSalida + "<<", EIGlobal.NivelLog.INFO);
							String estatus_envio = "";
							String fechaAplicacionFormato = "";
							fechaAplicacionFormato = fechaAplicacion.substring( 0, 2) + "/" + fechaAplicacion.substring(2, 4) + "/" + fechaAplicacion.substring(4, 8);
							if (!tramaSalida.startsWith("MANC")) {
								try {
									EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: antes de estatus_envio :: ", EIGlobal.NivelLog.INFO);
									estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
									EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: despues de estatus_envio :: ", EIGlobal.NivelLog.INFO);
								} catch (Exception e) {
									EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: estatus_envio ::::: "+estatus_envio, EIGlobal.NivelLog.INFO);
									estatus_envio = "";
								}
							}

							/* Todas las operaciones correctas exceptuando las
							 * PNLI, la cuales en caso de que sean realizadas
							 * tanto de forma correcta como incorrecta se van
							 * por el flujo del else.
							 */
							boolean duplicadoJdbc = false;
							final HttpSession sessionBit = request.getSession(false);
							NominaLineaBO bo=new NominaLineaBO();
							NominaLineaBean bean=new NominaLineaBean();
							bean.setContrato(session.getContractNumber());
							bean.setNumRegistros(session.getNominaNumberOfRecords());
							bean.setImporteRecibido(sessionBit.getAttribute(STR_SUM_TMP_SESSION).toString());
							//M022532 NOMINA EN LINEA - INI
							bean.setTipoCargoArch(tipoCargo);
							//M022532 NOMINA EN LINEA - FIN
							if (validar.compareTo("N") != 0 && ("NLVP").equals(servicio)){
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: validaDuplicadosjdbc :: contrato 		:: " + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: validaDuplicadosjdbc :: noRegistros 		:: " + session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: validaDuplicadosjdbc :: importeRecibido 	:: " + sessionBit.getAttribute(STR_SUM_TMP_SESSION).toString(), EIGlobal.NivelLog.INFO);

								duplicadoJdbc = bo.validaDuplicadoNomlinArch(bean);
								EIGlobal.mensajePorTrace("NominaLineaBO.java - validaDuplicadosjdbc :: duplicadoJdbc: "+duplicadoJdbc, EIGlobal.NivelLog.ERROR);

							}
							if (("NOMI0000").equals(estatus_envio) && !duplicadoJdbc) {

								//FAVORITOS -----------
								  agregarFavoritoNomina(request,NominaPagosBO.generarInfoArchivo(rutaArch+"_preprocesar"));
								//FAVORITOS ----

								//TODO EJECUTAR INSERT EWEB_NOMLIN_ARCH
								/**** Inicio de inserccion a la tabla eweb_nomlin_arch ****/
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inicia inserrcion de nueva tabla ::: ", EIGlobal.NivelLog.INFO);
								final SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:MM", new java.util.Locale("es","mx"));
								final Date fecha=new Date();
								final String fchCarga=f.format(fecha);

								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta :: contrato 			:: " + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta :: nombreArchivo		:: " + nombreArchivo, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta :: noRegistros 		:: " + session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta :: importeRecibido 	:: " + sessionBit.getAttribute(STR_SUM_TMP_SESSION).toString(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta :: fchCarga 			:: " + fchCarga, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: inserta :: tipoCargoArch     :: " + bean.getTipoCargoArch(), EIGlobal.NivelLog.INFO);

								bean.setContrato(session.getContractNumber());
								bean.setNomArchivo(nombreArchivo);
								bean.setNumRegistros(session.getNominaNumberOfRecords());
								bean.setImporteRecibido(sessionBit.getAttribute(STR_SUM_TMP_SESSION).toString());
								bean.setFchCarga(fchCarga);
								/**** Inicio de actualizacion para la tabla eweb_nomlin_cata ****/
								bean=validaTerciaBiatux(bean);
								/**** Fin de actualizacion para la tabla eweb_nomlin_cata ****/

								boolean resultado=bo.registraNomlinArch(bean);
								EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: se termina de registrar :: " + resultado, EIGlobal.NivelLog.INFO);
								/**** Fin de inserccion a la tabla eweb_nomlin_arch ****/
								/*
								 * Si el servicio invocado fue NOMI_VAL_PAGO,
								 * entonces las validaciones se pasaron
								 * correctamente, por lo tanto hay que invocar
								 * al servicio NOMI_PAGO para procesar el
								 * archivo.
								 */
								if (("NLVP").equals(servicio)) {// Se cambia NOVP por NLVP
									sess.setAttribute(STR_VALIDAR, "N"); // Setear la variable global para que no se contin�e con la validaci�n.
									request.getSession().setAttribute(EJEC_PROC_VAL_PAGO, TRUEX);
									EIGlobal.mensajePorTrace(textoLog + "Atributo EJEC_PROC_VAL_PAGO CREADO.", EIGlobal.NivelLog.DEBUG);
									defaultAction(request, response); // Ejecutar nuevamente la operaci�n de env�o para procesar el archivo.
									return;
								}
								final String transmision = tramaSalida.substring(posCar(tramaSalida, '@', 2) + 1, posCar(tramaSalida, '@', 3));
								final String nombreArchivo_aceptado = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
								String fechaCargo = null;
								final int start = posCar(tramaSalida, '@', 3) + 1;
								final int end = tramaSalida.indexOf('@', start);
								if (start < end) {
									fechaCargo = tramaSalida.substring(start, end);
									fechaCargo = fechaCargo.substring(0, 2) + "/" + fechaCargo.substring(2, 4) + "/" + fechaCargo.substring(4, 8);
								}
								final String bgcolor = "textabdatobs";
								StringBuffer tablaTotales = new
										StringBuffer("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
												TR+
												"<td class=\"textabref\">Pago de n&oacute;mina en l&iacute;nea</td>"+
												TRC+
												"</table>"+
												"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
												TR+
												"<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>"+
												"<td width=\"80\" class=\"tittabdat\" align=\"center\"><center>Descripci&oacute;n</center></td>");
								if (null != fechaCargo) {
									tablaTotales.append("<td class='tittabdat' width='90' align='center'>Fecha de Cargo</td>");
								}
								tablaTotales.append("<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>"+
										"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>"+
										"<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>"+
										"<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de transmisi&oacute;n</td>"+
										TRC+
										TDCLASS+
										bgcolor+
										NOWRAPALINGCENTER+
										session.getCuentaCargo()+
										ESPACIOTD+
										TDCLASS+
										bgcolor+
										NOWRAPALINGCENTER+
										sess.getAttribute(DESCPCUENTACARGOX)+
										ESPACIOTD);
								if (null != fechaCargo) {
									tablaTotales.append("<td class='"+
											bgcolor+
											"' align='center'>"+
											fechaCargo+
											"</td>");
								}
								tablaTotales.append(TDCLASS+
										bgcolor+
										NOWRAPALINGCENTER+
										fechaAplicacionFormato+
										ESPACIOTD+
										TDCLASS+
										bgcolor+
										NOWRAPALINGCENTER+
										FormatoMoneda.formateaMoneda(new Double(importe).doubleValue())+
										ESPACIOTD+
										TDCLASS+
										bgcolor+
										NOWRAPALINGCENTER+
										session.getNominaNumberOfRecords()+
										ESPACIOTD+
										TDCLASS+
										bgcolor+
										NOWRAPALINGCENTER+
										transmision+
										ESPACIOTD+
										"</table><br>"+
										"<input type=\"hidden\" name=\"hdnPagBack\" value=1>");
								nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());
								request.setAttribute(ARCHIVO_ACTUALX, nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
								request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
								request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
								request.setAttribute(ENCABEZADOX, CreaEncabezado( PAGODENOMINAENLINEAX, SERVICIOSNOMINAPAGOSENLINEAX, S25800IIIH, request));
								request.setAttribute("TablaTotales", tablaTotales.toString());
								String infoUser = "La nomina fue aceptada correctamente: " + nombreArchivo_aceptado + " n�mero de transmisi�n = " + transmision;
								infoUser = "cuadroDialogo (\"" + infoUser + "\",1)";
								request.setAttribute(INFOUSERX, infoUser);
								if (sess.getAttribute(FACULTADESNX) != null) {
									request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
								}
								request.setAttribute(TIPOARCHIVOX, "enviado");
								final String botonestmp = BR
										+ TABLE
										+ IMPRIMIR
										+ " <td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
										+ TERMINATABLE;
								request.setAttribute(BOTONESX, botonestmp);
								request.setAttribute(CONTENIDOARCHIVOX, " ");
								EIGlobal.mensajePorTrace(textoLog + "botones >>" + request .getAttribute(BOTONESX) + "<<", EIGlobal.NivelLog.INFO);
								evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
							} else {// de trasaccion exitosa
								/*
								 * Proposito: Reenvio de Errores en caso de que
								 * falle la importacion del Archivo En el
								 * horario especificado.
								 *
								 * NOTA: tambien se utiliza para las operaci�nes
								 * PNLI que sean enviadas correctamente o con
								 * error ya que se adapto el flujo para poder
								 * realizar una dispersi�n sin esperar respuesta
								 * del servidor.
								 */
								EIGlobal.mensajePorTrace(textoLog + "****************BOX 2********************", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textoLog + "****************************************", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textoLog + ">>" + sess.getAttribute("statusHorario") + "<<", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textoLog + ">>" + request.getAttribute("hr_lost") + "<<", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textoLog + "****************************************", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textoLog + "*******************BOX 2*****************", EIGlobal.NivelLog.INFO);
								request.setAttribute(FOLIOX, (String) getFormParameter(request, FOLIOX));
								EIGlobal.mensajePorTrace(textoLog + "CCBEst_Env" + estatus_envio, EIGlobal.NivelLog.DEBUG);

								//Favoritos
								request.setAttribute("chkBoxFav", request.getParameter(STR_CHECK));
								request.setAttribute(STR_CHECK, request.getParameter(STR_CHECK));

								strDescFav = request.getParameter("txtFav").getBytes();
								try {
									dscFav = new String(strDescFav, "UTF-8");
									EIGlobal.mensajePorTrace(
									"OpcionesNominaLN: Favorito sin Encode: "
											+ request.getParameter("txtFav").toString() , EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace(
									"OpcionesNominaLN: Favorito con Encode: "
											+ dscFav , EIGlobal.NivelLog.INFO);
								} catch (UnsupportedEncodingException e)
								{
									EIGlobal.mensajePorTrace(
									"OpcionesNominaLN: Excepcion con Encode: "
											+ e.getMessage() , EIGlobal.NivelLog.INFO);
								}

								request.setAttribute("txtFav", dscFav);
								request.setAttribute("cargarValores", "si");

								/* Si es OK significa que la operaci�n PNLI se
								 * ha realizado de manera correcta y por tanto
								 * envia mensaje de que todo es correcto.
								 */
								EIGlobal.mensajePorTrace(textoLog + "tramasalida [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(textoLog + "folioArchivo[" + folioArchivo + "]", EIGlobal.NivelLog.INFO);
								if (("OK").equals(tramaSalida) && !duplicadoJdbc){
									EIGlobal.mensajePorTrace(textoLog + "PNLI Correcto, procediendo a generar tabla y mensaje", EIGlobal.NivelLog.DEBUG);
									sess.setAttribute(HORARIONORMALX, "0");

									String tablaTotales = "";

									if (sess.getAttribute(DESCPCUENTACARGOX) == null) {
										sess.setAttribute(DESCPCUENTACARGOX, "");
									}

									try {
										tablaTotales = generaTablaTotales(
												session.getCuentaCargo(),
												sess.getAttribute(DESCPCUENTACARGOX).toString(),
												fechaAplicacionFormato,
												importe,
												session.getNominaNumberOfRecords()
												);
									} catch (Exception e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									}

									StringBuffer mensajeUser = new
											StringBuffer("La n&oacute;mina est&aacute; siendo procesada.<br>"+
													"El n&uacute;mero de folio correspondiente a la operaci&oacute;n es:<br>"+
													folioArchivo+
													"<br>Utilice este n&uacute;mero para consultar las<br>"+
													"referencias y estado de las referencias,<br>"+
													"en Seguimiento de Transferencias.");

									final String infoUser = "cuadroDialogo (\"" + mensajeUser.toString() + "\",11)";
									request.setAttribute(INFOUSERX, infoUser);

									if (sess.getAttribute(FACULTADESNX) != null) {
										request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
									}

									final String botonestmp = BR
											+ TABLE
											+ " <td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table></td></tr>";
									request.setAttribute(BOTONESX, botonestmp);

									nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());
									request.setAttribute(ARCHIVO_ACTUALX, nombreArchivo);
									request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
									request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
									request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAENLINEAX, SERVICIOSNOMINAPAGOSENLINEAX, S25800IIIH, request));
									request.setAttribute("TablaTotales", tablaTotales);
									request.setAttribute(CONTENIDOARCHIVOX, " ");
									request.getSession().setAttribute(EJEC_PROC_VAL_PAGO, TRUEX);
									/**
									 * PYME UNIFICACION DE TOKEN, Se agrega Atributo
									 * (mostrarToken)para mostrar u ocultar el token
									 */
									request.setAttribute("mostrarToken", "false");
									EIGlobal.mensajePorTrace(textoLog + "Atributo EJEC_PROC_VAL_PAGO CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);

									try {
										int noRegistros = 0;
										int referenciaInt = 0;
										String referencia = "";

										try {
											referenciaInt = Integer.parseInt(folioArchivo);
											referencia = String.valueOf(referenciaInt);
										} catch (Exception e) {
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											referencia = folioArchivo;
										}

										try {
											noRegistros = Integer.parseInt(session.getNominaNumberOfRecords().trim());
										} catch (Exception e) {
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}
										if (importe.length() >= 2) {
											importe = (importe.substring(0, importe.length()-2).concat(".").concat(importe.substring(importe.length()-2)));
										}

										/************************** Envio de notificacion *******************************/
										EIGlobal.mensajePorTrace("�������������������� Notificacion en OpcionesNomina", EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� CuentaCargo ->" + session.getCuentaCargo(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� Importe ->" + importe, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� Estatus -> NOMI0000(Hardcode)" , EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� No de Transmision ->" + referencia, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� Registros Importados ->" + noRegistros, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� Estatus -> NOMI0000(Hardcode)" , EIGlobal.NivelLog.INFO);

										bean.setContrato(session.getContractNumber());
										bean.setRazonSocial(session.getNombreContrato());
										bean.setCtaCargo(session.getCuentaCargo());
										bean.setImporteAplicado(ValidaOTP.formatoNumero(importe));
										bean.setNumRef(referencia);
										bean.setNumTransmision(referencia);
										bean.setNumRegistros(Integer.toString(noRegistros));
										bean.setEstatus("NOMI0000");

										//Envio de notificacion Pago de Nomina en Linea
										bo.sendNotificacion(request, bean);

									} catch (Exception e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									}
									////////////////
									evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
									EIGlobal.mensajePorTrace(textoLog + "Redireccionando a ::::::::::: ["+IEnlace.MTO_NOMINA_TMPLN+"]", EIGlobal.NivelLog.INFO);
								}

								// Archivo Duplicado NOMI0050 ------------------
								else if (("NOMI0050").equals(estatus_envio) || duplicadoJdbc)
								{
									sess.setAttribute(HORARIONORMALX, "0");
									sess.setAttribute(HR_PX, HorarioSele);
									EIGlobal.mensajePorTrace(textoLog + "HR_P 2>>" + sess.getAttribute(HR_PX) + "<<", EIGlobal.NivelLog.INFO);
									request.setAttribute(ARCHIVO_ACTUALX, nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
									request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
									request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
									request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAENLINEAX, SERVICIOSNOMINAPAGOSENLINEAX, S25800IIIH, request));
									String infoUser = " Archivo Duplicado. �Desea enviar este archivo de cualquier manera? ";
									final String infoEncabezadoUser = "<B>ALERTA</B>";
									infoUser = CUADRODIALOGOPAGOSNOMINAX + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
									request.setAttribute(INFOUSERX, infoUser);
									if (sess.getAttribute(FACULTADESNX) != null) {
										request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
									}
									sess.setAttribute(ARCHDUPLICADOX, "si");
									request.setAttribute(DIGITX, "0");
									request.setAttribute(TIPOARCHIVOX, "duplicado");
									request.setAttribute(CONTENIDOARCHIVOX, sess.getAttribute(CONTENIDOARCHIVOX));
									EIGlobal.mensajePorTrace(textoLog + "El horario seleccionado antes de volver a llamar el MantenimientoNomina despuesa de archivo duplicado es: ========>" + HorarioSele + "<======", EIGlobal.NivelLog.DEBUG);
									request.setAttribute(HORARIO_SELECCIONADOX, HorarioSele);
									request.getSession().setAttribute(EJEC_PROC_VAL_PAGO, TRUEX);
									EIGlobal.mensajePorTrace(textoLog + "Atributo EJEC_PROC_VAL_PAGO CREADO por Duplicado.", EIGlobal.NivelLog.DEBUG);
									/**
									 * PYME 2015 UNIFICACION DE TOKEN
									 */
									request.setAttribute(OPERACIONX,"envio");
									evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
								}

								// Cualquier otro problema que se pueda presentar
								else {
									// "NOMI0008@NO SE PUDO INSERTAR EL ARCHIVO EN TABLA@";
									// ERROR MANC0014 2893Usuarios iguales en mancomunidad - Genere un nuevo folio<
									sess.setAttribute(HORARIONORMALX, "0");
									String mensaje_error = "";
									try {
										if (tramaSalida.startsWith("MANC")) {
											mensaje_error = tramaSalida.substring(16, tramaSalida.length());
										}
										else {
											mensaje_error = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
										}
									} catch (Exception e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										mensaje_error = "";
									}
									EIGlobal.mensajePorTrace(textoLog + "mensaje_error: >>" + mensaje_error + "<<", EIGlobal.NivelLog.INFO);
									if (("").equals(mensaje_error)) {
										mensaje_error = "Servcio no disponible en este momento, intente m&aacute;s tarde !.";
									}
									final String bgcolor = "textabdatobs";
									StringBuffer tablaTotales = new
											StringBuffer("<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
													TR+
													"<td class=\"textabref\">Pago de n&oacute;mina en l&iacute;nea</td>"+
													TRC+
													"</table>"+
													"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
													TR+
													"<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>"+
													"<td width=\"80\" class=\"tittabdat\" align=\"center\"><center>Descipci&oacute;n</center></td>"+
													"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci�n</td>"+
													"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>"+
													"<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>"+
													TRC+
													TDCLASS+
													bgcolor+
													NOWRAPALINGCENTER+
													session.getCuentaCargo()+
													ESPACIOTD+
													TDCLASS+
													bgcolor+
													NOWRAPALINGCENTER+
													sess.getAttribute(DESCPCUENTACARGOX)+
													ESPACIOTD+
													TDCLASS+
													bgcolor+
													NOWRAPALINGCENTER+
													fechaAplicacionFormato+
													ESPACIOTD+
													TDCLASS+
													bgcolor+
													NOWRAPALINGCENTER+
													formatoMoneda(importe)+
													ESPACIOTD+
													TDCLASS+
													bgcolor+
													NOWRAPALINGCENTER+
													session.getNominaNumberOfRecords()+
													ESPACIOTD + "</table><br>");
									nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());
									request.setAttribute(ARCHIVO_ACTUALX, nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
									request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
									request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
									request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAENLINEAX, SERVICIOSNOMINAPAGOSENLINEAX, S25800IIIH, request));
									request.setAttribute("TablaTotales", tablaTotales.toString());
									String infoUser = mensaje_error;
									infoUser = "cuadroDialogo (\"" + infoUser + "\",1)";
									request.setAttribute(INFOUSERX, infoUser);
									if (sess.getAttribute(FACULTADESNX) != null) {
										request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
									}
									final String botonestmp = BR
											+ TABLE
											+ " <td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table></td></tr>";
									request.setAttribute(BOTONESX, botonestmp);
									request.setAttribute(CONTENIDOARCHIVOX, " ");

									request.getSession().setAttribute(EJEC_PROC_VAL_PAGO, TRUEX);
									EIGlobal.mensajePorTrace(textoLog + "Atributo EJEC_PROC_VAL_PAGO CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);
									evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
								}
							}
						} else {
							EIGlobal.mensajePorTrace(textoLog + "***Entra NominaOcupada", EIGlobal.NivelLog.DEBUG);
							request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
							request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
							request.setAttribute(ENCABEZADOX, CreaEncabezado("Transacci&oacute;n en Proceso", SERVICIOSNOMINAPAGOSENLINEAX, "s55290", request));
							request.setAttribute(OPERACIONX, "tranopera");
							evalTemplate(IEnlace.MTO_TRANPROC_TMPLN, request, response);
							EIGlobal.mensajePorTrace(textoLog + "Redireccionando a ::::::::::: ["+IEnlace.MTO_TRANPROC_TMPLN+"]", EIGlobal.NivelLog.INFO);
						}


					}// Termina else para interrumpe el flujo
				} // Termina operacion igual a envio

				if (("consulta").equals(strOperacion)) {
					consultarArchivos(request, response);
				}
				if (("exportar").equals(strOperacion)) {
					exportarArchivo(request, response);
				}
				String arcRecuperado = "";
				String tmpTipoArchivo = "";
				if (("reporte").equals(strOperacion)) {
					request.setAttribute("archivo", "Pagos");
					arcRecuperado = getFormParameter(request, TIPOARCHIVOX);
					final String registrosEnArchivo = request.getParameter(CANTIDADDEREGISTROSENARCHIVOX);
					EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE registrosEnArchivo dentro de reporte>>" + registrosEnArchivo + "<<", EIGlobal.NivelLog.DEBUG);
					arcRecuperado.trim();
					EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE arcRecuperado> antes>>" + arcRecuperado + "<<", EIGlobal.NivelLog.DEBUG);
					if (("").equals(arcRecuperado)) {
						arcRecuperado = "nuevo";
					}
					EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE arcRecuperado>>" + arcRecuperado + "<<", EIGlobal.NivelLog.DEBUG);
					tmpTipoArchivo = arcRecuperado.substring(0, 1);
					EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE tmpTipoArchivo>>" + tmpTipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
					if (("r").equals(tmpTipoArchivo)) {
						nombre_arch_salida = TEMPX
								+ getFormParameter(request, ARCHIVO_ACTUALX);
						final String secuencia_recu = getFormParameter(request,"transmision");
						ArchivosNomina archRecuperado = new ArchivosNomina(nombre_arch_salida, request);
						request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
						request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
						request.setAttribute(ENCABEZADOX, CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Altas","s25810h", request));
						request.setAttribute("etiquetaNameFile","Nombre del archivo: Archivo Recuperado");
						request.setAttribute("etiquetaNumTransmision","N&uacute;mero de Transmisi&oacute;n: "+ secuencia_recu);
						request.setAttribute(TIPOARCHIVOX, arcRecuperado);
						request.setAttribute(CANTIDADDEREGISTROSENARCHIVOX,registrosEnArchivo);
						request.setAttribute(CONTENIDOARCHIVOX, archRecuperado.reporteArchivoSalida());
					} else {
						final String archivoName = (String) sess.getAttribute(NOMBREARCHIVOX);
						// String archivoName=session.getNameFileNomina();
						final String nombreArchivoOriginal = archivoName.substring(archivoName.indexOf('/') + 1);
						// ArchivosNomina archNvoImportado=new
						// ArchivosNomina(this,
						// session.getNameFileNomina());
						final ArchivosNomina archNvoImportado = new ArchivosNomina((String) sess.getAttribute(NOMBREARCHIVOX),request);
						request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
						request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
						request.setAttribute(ENCABEZADOX, CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Altas","s25810h", request));
						request.setAttribute("etiquetaNameFile","Nombre del archivo: " + nombreArchivoOriginal);
						request.setAttribute("etiquetaNumTransmision","N&uacute;mero de Transmisi&oacute;n: Archivo no transmitido");
						request.setAttribute(TIPOARCHIVOX, arcRecuperado);
						request.setAttribute(CANTIDADDEREGISTROSENARCHIVOX,registrosEnArchivo);
						request.setAttribute(CONTENIDOARCHIVOX,archNvoImportado.reporteArchivo());
					}

					evalTemplate(IEnlace.REPORTE_NOMINALN_TMPL, request, response);
				}

				/*VECTOR - Solicitud de Informe PDF*/
				if(COD_OPER_PROG_SOl_PDF.equals(strOperacion)){
					solicitaInformePDF(request, response);
				}

				if(COD_OPER_PROG_DES_PDF.equals(strOperacion)){
					descargaInformePDF(request, response);
				}

			}// fin de else de validacion OTP.
		} // fin se sessionValida
		else {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	/**
	 * Metodo posCar que devuelve la posicion de un caracter en un String
	 * @param cad cadena
	 * @param car caracter
	 * @param cant cantidad
	 * @return result valor entero de posicion
	 */
	public int posCar(String cad, char car, int cant) {
		int result = 0, pos = 0;
		for (int i = 0; i < cant; i++) {
			result = cad.indexOf(car, pos);
			pos = result + 1;
		}
		return result;
	}

	/**
	 * Carga valores de combo plazas
	 * @return comboPlazas cadena que arma combo
	 */
	public String comboPlazas() {
		StringBuffer comboPlazas = new StringBuffer("");
		comboPlazas.append( "\n <input type=hidden name=valorPlaza value='01001'>" )
			.append( "\n <input type=text size=25 name=PlazaBanxico value='MEXICO, DF' onFocus='blur();'>" )
			.append( "\n <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>");
		return comboPlazas.toString();
	}

	/**
	 * Contruye combo coloca plazas
	 * @param plazaBank valor de plaza
	 * @return comboPlazas cadena que arma combo
	 */
	public String colocaPlaza(String plazaBank) {
		StringBuffer comboPlazas = new StringBuffer("");
		comboPlazas.append( "\n <input type=hidden name=valorPlaza value='01001'>" )
			.append( "\n <input type=text size=25 name=PlazaBanxico value='" )
			.append( plazaBank ).append( "' onFocus='blur();'>" )
			.append( "\n <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>" );
		return comboPlazas.toString();
	}

	/**
	 * Metodo que le da formato al importe del archivo
	 * @param cantidad 		cantidad a la que se dara formato
	 * @return formatoFinal devuelve el formato de la cantidad
	 */
	public String formatoMoneda(String cantidad) {
		textodeLog("formatoMoneda");
		EIGlobal.mensajePorTrace(textoLog + "***Entrando a formatoMoneda ", EIGlobal.NivelLog.DEBUG);
		cantidad = cantidad.trim();
		final String language = "la"; // ar
		final String country = "MX"; // AF
		final Locale local = new Locale(language, country);
		final NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato = "";
		String cantidadDecimal = "";
		String formatoFinal = "";
		String cantidadtmp = "";
		if (cantidad == null || ("").equals(cantidad)) {
			cantidad = "0.0";
		}
		if (cantidad.length() > 2) {
			try {
				cantidadtmp = cantidad.substring(0, cantidad.length() - 2);
				cantidadDecimal = cantidad.substring(cantidad.length() - 2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				if (("MXN").equals(formato.substring(0,3))){
					formato = formato.substring(3, formato.length() - 2);
				} else {
					formato = formato.substring(1, formato.length() - 2);
				}
				formatoFinal = "$ " + formato + cantidadDecimal;
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				formatoFinal = "$ " + CEROS;
			}
		} else {
			try {
				if (cantidad.length() > 1) {
					cantidadDecimal = cantidad.substring(cantidad.length() - 2);
					formatoFinal = "$ " + "0." + cantidadDecimal;
				} else {
					cantidadDecimal = cantidad.substring(cantidad.length() - 1);
					formatoFinal = "$ " + "0.0" + cantidadDecimal;
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				formatoFinal = "$ " + CEROS;
			}
		}
		return formatoFinal;
	}

	/**
	 * Metodo que agrega el punto al importe del archivo
	 * @param importe valor del importe
	 * @return importeConPunto devuelve cantidad con punto
	 */
	public String agregarPunto(String importe) {
		textodeLog("agregarPunto");
		String importeConPunto = "";
		importe = importe.trim();
		if (importe == null || ("").equals(importe)){
			importe = "0.0";}
		if (importe.length() > 2) {
			try {
				importe = importe.trim();
				String enteros = importe.substring(0, importe.length() - 2);
				enteros = enteros.trim();
				final String decimales = importe.substring(importe.length() - 2,importe.length());
				importe = enteros + " " + decimales;
				importeConPunto = importe.replace(' ', '.');
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				importeConPunto = CEROS;
			}
		} else {
			try {
				if (importe.length() > 1) {
					importe = importe.trim();
					final String decimales = importe.substring(importe.length() - 2,
							importe.length());
					importe = "0" + " " + decimales;
					importeConPunto = importe.replace(' ', '.');
				} else {
					importe = importe.trim();
					final String decimales = importe.substring(importe.length() - 1,
							importe.length());
					importe = "0" + " " + "0" + decimales;
					importeConPunto = importe.replace(' ', '.');
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				importeConPunto = CEROS;
			}
		}
		if (importeConPunto == null){
			importeConPunto = "";}
		return importeConPunto;
	}

	/**
	 * Descarga de archivos desde el AppServer
	 * @param request peticion del cliente
	 * @param response respuesta al cliente
	 * @throws ServletException control de excepciones del servlet
	 * @throws java.io.IOException control de exceciones del archivo
	 */
	protected void descargaArchivos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		String nombreDelArchivo = "rada.zip";
		response.setHeader("Content-disposition", "attachment; filename=" + nombreDelArchivo);
		response.setContentType("application/x-gzip");
		BufferedInputStream entrada = null;
		BufferedOutputStream salida = null;
		InputStream arc = null;
		try{
			arc = new FileInputStream(IEnlace.LOCAL_TMP_DIR + "/" + nombreDelArchivo);
			final ServletOutputStream out = response.getOutputStream();
			entrada = new BufferedInputStream(arc);
			salida = new BufferedOutputStream(out);
			final byte[] buff = new byte[1024];
			int cantidad = 0;
			while ((cantidad = entrada.read(buff, 0, buff.length)) != -1) {
				salida.write(buff, 0, cantidad);
			}
		}catch (IOException e) {
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java ::  descargaArchivos :: Se controlan problemas en el archivo: "+new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
		}catch (Exception e1) {
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java ::  descargaArchivos :: Se controlan problemas en el archivo: "+new Formateador().formatea(e1), EIGlobal.NivelLog.DEBUG);
		}finally{
			try {
				if (salida != null){
					salida.close();
				}if (entrada != null){
					entrada.close();
				}if (arc != null){
					arc.close();
				}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace("OpcionesNominaLn.java ::  descargaArchivos :: Se controlo problema al cerrar archivo: "+new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
			}
		}


	}

	/**
	 * Despliega la pagina principal de Pagos
	 * @param request peticion del cliente
	 * @param response respuesta al cliente
	 * @throws ServletException control de excepciones del servlet
	 * @throws java.io.IOException control de exceciones del archivo
	 */
	protected void inicioPagos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		textodeLog("inicioPagos");
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
		request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
		request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
		if (sess.getAttribute(FACULTADESNX) != null){
			request.setAttribute(FACULTADESX, sess.getAttribute(FACULTADESNX));
			request.setAttribute(CONTENIDOARCHIVOX, "");
			request.setAttribute(ENCABEZADOX, CreaEncabezado(PAGODENOMINAENLINEAX,SERVICIOSNOMINAPAGOSENLINEAX, "s25800h", request));
			sess.setAttribute("FOLIO", "");
			sess.setAttribute(STR_VALIDAR, "");
			EIGlobal.mensajePorTrace(textoLog + "**********LIMPIANDO VARIABLES FOLIO Y VALIDAR****", EIGlobal.NivelLog.DEBUG);

			if (session.getFacultad("INOMENVIPAG")) {
				EIGlobal.mensajePorTrace(textoLog + "DAG Entro modulo=0 y con Facultad INOMENVIPAG  DESDE OPCIONES NOMINA HELG****", EIGlobal.NivelLog.DEBUG);
				// Se controla la Recarga para evitar duplicidad de registros
				// transferencia
				verificaArchivos(TEMPX + request.getSession().getId().toString()
						+ SESX, true);
				if (request.getSession().getAttribute(RECARGA_SES_MULTIPLESX) != null) {
					request.getSession().removeAttribute(RECARGA_SES_MULTIPLESX);
					EIGlobal.mensajePorTrace(textoLog + "Inicio Pagos - Borrando variable de sesion desde OpcionesNominaLn", EIGlobal.NivelLog.DEBUG);
				}
			}

			//MSD Limpia variables de sesi�n usadas en dispersiones anteriores
			sess.removeAttribute(STR_VALIDAR);
			sess.removeAttribute("HOR_SAT");
			sess.removeAttribute("HorarioNormal");
			sess.removeAttribute(STR_VALIDAR);
			sess.removeAttribute("FOLIO");
			sess.removeAttribute("HR_P");
			sess.removeAttribute("archDuplicado");
			sess.removeAttribute(STR_NOM_IENC_FECH_APLI);
			//MSD Limpia variables de sesi�n usadas en dispersiones anteriores

			EIGlobal.mensajePorTrace(textoLog + "**********Regresando a MantenimientoNominaLn.jsp****", EIGlobal.NivelLog.DEBUG);
			evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response);
		}
	}

	/**
	 * Muestra la pantalla de inicio de consultas de nomina
	 * @param request peticion del cliente
	 * @param response respuesta del cliente
	 * @throws ServletException control de excepciones del servlet
	 * @throws java.io.IOException control de exceciones del archivo
	 */
	protected void inicioConsulta(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {

		EIGlobal.mensajePorTrace(textoLog + ":: inicioConsulta() :: Regresando a Busqueda", EIGlobal.NivelLog.DEBUG);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);

		EIGlobal.mensajePorTrace(textoLog + ":: inicioConsulta() :: Armando Calendar", EIGlobal.NivelLog.DEBUG);
		String VarFechaHoy = "";
		VarFechaHoy = myCalendarNomina.armaArregloFechas();
		EIGlobal.mensajePorTrace(textoLog + ":: inicioConsulta() :: Calendar creado", EIGlobal.NivelLog.DEBUG);

		String fechaHoy = "";
		GregorianCalendar cal = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("es","mx"));
		fechaHoy = sdf.format(cal.getTime());

		EIGlobal.mensajePorTrace(textoLog + ":: inicioConsulta() :: VarFechaHoy: "+VarFechaHoy, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(textoLog + ":: inicioConsulta() :: DiaHoy: "+fechaHoy, EIGlobal.NivelLog.DEBUG);
		request.setAttribute( "VarFechaHoy", VarFechaHoy);
		request.setAttribute("DiaHoy",fechaHoy);

		request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
		request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
		request.setAttribute(ENCABEZADOX, CreaEncabezado(
				CONSULTADENOMINAENLINEAX,
				"Servicios &gt; N&oacute;mina  &gt; Consultas &gt; Pagos en L&iacute;nea", "s25800conh",
				request));
		if (sess.getAttribute(FACULTADESNX) != null){
			request
			.setAttribute(FACULTADESX, sess
					.getAttribute(FACULTADESNX));}
		request.setAttribute(TEXTCUENTAX, "");
		request.setAttribute(CONTENIDOARCHIVOX, "");

		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("es","mx"));
		Date fechaActual = new Date();
		String hoy = formato.format(fechaActual);
		request.setAttribute("fechaActual",hoy);

		evalTemplate(IEnlace.NOMINADATOSLN_TMPL, request, response);
		EIGlobal.mensajePorTrace(textoLog + ":: inicioConsulta() :: Redireccionando a: "+IEnlace.NOMINADATOSLN_TMPL, EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Realiza la consulta de archivos de nomina en linea con paginacion
	 * @param request peticion del cliente
	 * @param response respuesta al cliente
	 * @throws ServletException control de excepciones del servlet
	 * @throws java.io.IOException control de exceciones del archivo
	 */
	protected void consultarArchivos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {

		EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: Iniciando ConsultaPaginacion", EIGlobal.NivelLog.INFO);
		NominaLineaBean resultado=new NominaLineaBean();

		try {
			BaseResource baseResource = (BaseResource) request.getSession().getAttribute(SESSIONX);
			String sessionId = (String) request.getSession().getId();
			EIGlobal.mensajePorTrace(textoLog + "sessionId [" + sessionId + "]", EIGlobal.NivelLog.DEBUG);

			final String contrato = baseResource.getContractNumber();
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: contrato: "+contrato, EIGlobal.NivelLog.INFO);
			String cuentaCargo = (String) getFormParameter(request, TEXTCUENTAX);
			if(null != cuentaCargo){
				String cuenta[]=cuentaCargo.split(" ");
				cuentaCargo=cuenta[0];
			}
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: cuentaCargo: "+cuentaCargo, EIGlobal.NivelLog.INFO);

			final String tipoFecha = request.getParameter("rbtnFecha") != null ? request.getParameter("rbtnFecha") : request.getParameter("hddTipoFecha");
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: rbtnFecha: "+tipoFecha, EIGlobal.NivelLog.INFO);

			final String fchIni = (String) getFormParameter(request, "fecha1"); // formatear la fecha enviar sin diagonales ddmmaaaa
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: fchIni: "+fchIni, EIGlobal.NivelLog.INFO);
			final String fchFin = (String) getFormParameter(request, "fecha2");
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: fchFin: "+fchFin, EIGlobal.NivelLog.INFO);

			final NominaLineaBean consulta=new NominaLineaBean();
			consulta.setContrato(contrato);
			consulta.setCtaCargo(cuentaCargo != null ? cuentaCargo : "");
			consulta.setTipoFecha(tipoFecha != null ? tipoFecha : "");
			consulta.setFchIni(fchIni != null ? fchIni : "");
			consulta.setFchFin(fchFin != null ? fchFin : "");
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: Bean creado", EIGlobal.NivelLog.INFO);

			final String strPagina = request.getParameter("NoPagina").length()==0?null:request.getParameter("NoPagina");
			final Integer pagina = Integer.parseInt(strPagina);
			consulta.getPaginacion().setPagina(pagina);
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: pagina: "+pagina, EIGlobal.NivelLog.INFO);


			final NominaLineaBO lineaBO=new NominaLineaBO();
			resultado=lineaBO.consultarArchivos(consulta, request, response);

			 ExportModel em = new ExportModel("sepfileexp", '|',"\t")
			   .addColumn(new Column("fchAplicacion", "Fecha de aplicaci�n"))
			   .addColumn(new Column("ctaCargo", "Cuenta de Cargo"))
			   .addColumn(new Column("numRegistros", "N�m. registros"))
			   .addColumn(new Column("importeAplicado", "Importe aplicado", "$####################0.00"))
			   .addColumn(new Column("estatus", "Estatus"));
			 em.setOcultarTitulos(false);
			 request.getSession().setAttribute("ExportModel", em);
			 request.getSession().setAttribute("nombreBeanExportArch", resultado.getLstConsulta());


			request.setAttribute("pContrato", contrato);
			request.setAttribute("pCtaCargo", cuentaCargo);
			request.setAttribute("pFchIni", fchIni);
			request.setAttribute("pFchFin", fchFin);
			request.setAttribute("pTipoFecha", tipoFecha);

			request.setAttribute("resultadoConsulta", resultado);
			request.setAttribute("totalRegs", resultado.getPaginacion().getTotalRegs());
			request.setAttribute("pPaginas", resultado.getPaginacion().getPaginasTotales());
			request.setAttribute("pPagina", resultado.getPaginacion().getPagina());
			request.setAttribute("pDel", request.getSession().getAttribute("sesDel"));
			request.setAttribute("pAl", request.getSession().getAttribute("sesAl"));
			request.setAttribute(ARCHIVOEXPORTAR, "");

			request.getSession().setAttribute("sesContrato", contrato);
			request.getSession().setAttribute("sesCtaCargo", cuentaCargo);
			request.getSession().setAttribute("sesFchIni", fchIni);
			request.getSession().setAttribute("sesFchFin", fchFin);
			request.getSession().setAttribute("sesresultadoConsulta", resultado);
			request.getSession().setAttribute("sestotalRegs", resultado.getPaginacion().getTotalRegs());
			request.getSession().setAttribute("sesPaginas", resultado.getPaginacion().getPaginasTotales());
			request.getSession().setAttribute("sesPagina", pagina);

			request.setAttribute(ENCABEZADOX,CreaEncabezado(CONSULTADENOMINAENLINEAX,
					"Servicios &gt; N&oacute;mina  &gt; Consultas &gt; Pagos en L&iacute;nea", "s25800canh", request));

			evalTemplate(IEnlace.NOMINALNRESULT_TMPL, request, response);
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: consultarArchivos :: redireccionando a ResultadoNominaLn.jsp ", EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
	}

	/***
	 * Realiza la consulta de de detalle para ser exportados a un archivo .doc
	 * @param request peticion del cliente al servidor
	 * @param response repsuesta al cliente
	 * @throws ServletException control de excepciones del servlet
	 * @throws java.io.IOException control de exceciones del archivo
	 */
	protected void exportarArchivo(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo ", EIGlobal.NivelLog.INFO);
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
		String nombre_arch_salida = "";


		final String tipoFecha = request.getParameter("rbtnFecha") != null ? request.getParameter("rbtnFecha") : request.getParameter("hddTipoFecha");
		EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo :: rbtnFecha: "+tipoFecha, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace(textoLog + "Entro a exportar archivo****", EIGlobal.NivelLog.DEBUG);
		request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
		request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
		final String nueva_ruta = Global.directorioExportarnln;//Directorio exportar
		final ServicioTux tuxGlobal = new ServicioTux();//Servicio Tux
		Hashtable htResult = null;//Resultado exportacion

		final String tramaEntrada = generarTramaExportarArchivo(request, response);



		try {
			EIGlobal.mensajePorTrace(textoLog + "tramaEntrada >>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);
			htResult = tuxGlobal.webRedLN(tramaEntrada);
		} catch (Exception e) {
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar servicio WEB_RED", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNomina::consultaArchivos:: "+SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(DATOSGENERALESX, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(USUARIOX + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(CONTRATOX + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(MODULOACONSULTARX + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(LINEADETRUENEX + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception ex){
				EIGlobal.mensajePorTrace("OpcionesNomina::consultaArchivos::"
						+ SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX
						+ ex.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		if (null == htResult) {
			EIGlobal.mensajePorTrace(textoLog + "null ret despues de webred", EIGlobal.NivelLog.INFO);
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,
					CONSULTADENOMINAENLINEAX,
					"Servicios &gt; Consulta de N&oacute;mina en l&iacute;nea", request,
					response);
			return;
		}
		final String tramaSalida = (String) htResult.get(BUFFERX);
		// Simulaci�n de la trama de regreso :
		// NOMI0001@Mensaje de error@
		// String tramaSalida = "NOMI0000@/tmp/1001851.conNom@";
		// "TCT_0010 1397Servicio de bitacora no se encuentra disponible";
		EIGlobal.mensajePorTrace(textoLog + "Resultado de web_WEB_RED:: " + tramaSalida + "***", EIGlobal.NivelLog.DEBUG);

		/******** Exportando archivo generado en *********/
		final String nombreArch=(String) request.getSession().getAttribute("sessNombreArch")+".nomlin";
		String arch_exportar = "";

		String []arrDatos = null; //VECTOR - Para desentramar los datos de secuencia
		String strDatos = (String) getFormParameter(request, SECARCHIVO);
		String tipoArchivoExportar = (String) getFormParameter(request, "tipoExportacion");
		String secuencia = "";
		if(strDatos != null && !"".equals(strDatos)){
			arrDatos = strDatos.split(SEPARADOR);
			if(arrDatos != null){
				secuencia = arrDatos[0];
			}
		}else{
			secuencia = (String) getFormParameter(request, SECARCHIVO);
		}

		//final String numero_secuencia = (String) getFormParameter(request, SECARCHIVO);




		if (null != secuencia && !("").equals(secuencia.trim())) {

			/*Modificacion Nomina Epynme para recuperar nuevos datos*/

			arch_exportar = session.getUserID8() + "_" + secuencia.trim() + "_NomLn.doc";
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo :: arch_exportar con secuencia .:. " + arch_exportar, EIGlobal.NivelLog.INFO);


		} else {
			arch_exportar = session.getUserID8() + "NomLn.doc";
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo :: arch_exportar sin secuencia .:. " + arch_exportar, EIGlobal.NivelLog.INFO);
		}
		try {



			//String tipoCopia = "2"; // de /t/tuxedo... a /tmp
			final ArchivoRemoto archR = new ArchivoRemoto();
			//copiaOtroPathRemoto(String Archivo, String Destino, String pathDestino)
			//JGAL - Copia a ruta de download
			nombre_arch_salida = IEnlace.DOWNLOAD_PATH + nombreArch;


			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo :: Global.DIRECTORIO_REMOTO_WEB .:. "+Global.DIRECTORIO_REMOTO_WEB, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo :: arch_exportar .:. "+arch_exportar, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo :: nombre_arch_salida .:. "+nombre_arch_salida, EIGlobal.NivelLog.INFO);

			if (!archR.copiaOtroPathRemoto(nombreArch, Global.DIRECTORIO_REMOTO_WEB+ "/" + arch_exportar, nueva_ruta+ "/")) {
				EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
			}

			 ExportModel em = new ExportModel("sepfileexp", '|',"\\|", true)
			   .addColumn(new Column(0, ""))
			   .addColumn(new Column(1, ""))
			   .addColumn(new Column(2, ""))
			   .addColumn(new Column(3, ""))
			   .addColumn(new Column(4, ""))
			   .addColumn(new Column(5, ""))
			   .addColumn(new Column(6, ""))
			   .addColumn(new Column(7, ""))
			   .addColumn(new Column(8, ""))
			   .addColumn(new Column(9, ""))
			   .addColumn(new Column(10, ""))
			   .addColumn(new Column(11, ""))

			   ;
			 em.setOcultarTitulos(false);
			request.getSession().setAttribute("ExportModel", em);
			sess.setAttribute("af", nueva_ruta+ "/" + nombreArch);


		} catch (Exception ioeSua) {
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: Error archivo remoto"+new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
		}

		try{
			request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
			request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
			request.setAttribute(ENCABEZADOX,CreaEncabezado(CONSULTADENOMINAENLINEAX,"Servicios &gt; N&oacute;mina  &gt; Consulta &gt; Pagos en L&iacute;nea","s25800canh", request));
			request.setAttribute(TEXTCUENTAX, "");
			request.setAttribute(NOMBRE_ARCH_SALIDAX, nombre_arch_salida);
			request.setAttribute("pContrato", request.getSession().getAttribute("sesContrato"));
			request.setAttribute("pCtaCargo", request.getSession().getAttribute("sesCtaCargo"));
			request.setAttribute("pFchIni", request.getSession().getAttribute("sesFchIni"));
			request.setAttribute("pFchFin", request.getSession().getAttribute("sesFchFin"));
			request.setAttribute("totalRegs", request.getSession().getAttribute("sestotalRegs"));
			request.setAttribute("pPaginas", request.getSession().getAttribute("sesPaginas"));
			request.setAttribute("pPagina", request.getSession().getAttribute("sesPagina"));
			request.setAttribute("resultadoConsulta", request.getSession().getAttribute("sesresultadoConsulta"));
			//Se setea nuevamente el tipo de fecha
			request.setAttribute("pTipoFecha", tipoFecha);

			request.setAttribute(ARCHIVOEXPORTAR, arch_exportar);
			request.setAttribute("tipoArchivoExportar", tipoArchivoExportar);
			if (!tramaSalida.startsWith("NOMI0000")) {
				request.setAttribute(ARCHIVOEXPORTAR, "");
				String infoUser = "No se pudo exportar el archivo";
				infoUser = "cuadroDialogo (\"" + infoUser + "\",1)";
				request.setAttribute(INFOUSERX, infoUser);
			}
			request.setAttribute("pDel", request.getSession().getAttribute("sesDel"));
			request.setAttribute("pAl", request.getSession().getAttribute("sesAl"));
			evalTemplate(IEnlace.NOMINALNRESULT_TMPL, request, response);
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: Ocurrio error al exportar archivo"+new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		/******** Exportando archivo generado en *********/
		EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: exportarArchivo :: fin exportarArchivo ", EIGlobal.NivelLog.INFO);
	}

	/**
	 * Metodo que arma la trama del servicio tuxedo para exportar detalle de archivo
	 * @param request peticion del cliente al servidor
	 * @param response respuesta al cliente
	 * @return retValue devuelve tama para exportar archivo
	 */
	protected String generarTramaExportarArchivo(HttpServletRequest request,
			HttpServletResponse response) {
		EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: generarTramaExportarArchivo() :: generando trama... ", EIGlobal.NivelLog.INFO);
		final BaseResource baseResource = (BaseResource) request.getSession().getAttribute(SESSIONX);
		final String sessionId = (String) request.getSession().getId();
		EIGlobal.mensajePorTrace(textoLog + "sessionId [" + sessionId + "]", EIGlobal.NivelLog.DEBUG);
		final StringBuffer retValue = new StringBuffer("1EWEB|");
		retValue.append(
				baseResource.getUserID8()).append("|NLCO|").append(
						baseResource.getContractNumber()).append('|').append(
								baseResource.getUserID8()).append('|').append(
										baseResource.getUserProfile()).append('|');

		/*VECTOR - Desentramar datos de secuencia*/
		String []arrDatos = null; //VECTOR - Para desentramar los datos de secuencia
		String strDatos = (String) getFormParameter(request, SECARCHIVO);
		String secuencia = "";
		if(strDatos != null && !"".equals(strDatos)){
			arrDatos = strDatos.split(SEPARADOR);
			if(arrDatos != null){
				secuencia = arrDatos[0];
			}
		}else{
			secuencia = (String) getFormParameter(request, SECARCHIVO);
		}

		//final String num_secuencia = (String) getFormParameter(request, SECARCHIVO);

		if (null != secuencia && !("").equals(secuencia.trim())) {
			retValue.append(secuencia.trim());
		}
		final String nombreArch=request.getSession().getId();
		request.getSession().setAttribute("sessNombreArch", nombreArch);
		retValue.append('@').append(
				new java.io.File(Global.directorioExportarnln, nombreArch).getAbsolutePath()).append(".nomlin").append('@');
		return retValue.toString();
	}

	/**
	 * Metodo para verificar existencia del Archivo de Sesion
	 * @param arc 					archivo a verificar
	 * @param eliminar 				bandera que controla la eliminacion del archivo
	 * @return bandera 				valor de exito o fracaso de la eliminacion
	 * @throws ServletException 	control de excepciones del servlet
	 * @throws java.io.IOException 	control de excepciones del archivo
	 */
	public boolean verificaArchivos(String arc, boolean eliminar)
			throws ServletException, java.io.IOException {
		textodeLog("verificaArchivos");
		EIGlobal.mensajePorTrace(textoLog + "Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
		final File archivocomp = new File(arc);
		if (archivocomp.exists()) {
			EIGlobal.mensajePorTrace(textoLog + "El archivo si existe.", EIGlobal.NivelLog.DEBUG);
			if (eliminar) {
				archivocomp.delete();
				EIGlobal.mensajePorTrace(textoLog + "El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
			}
			return true;
		}
		EIGlobal
		.mensajePorTrace("El archivo no existe.", EIGlobal.NivelLog.DEBUG);
		return false;
	}

	/**
	 * Metodo que guarda los parametros en session
	 * @param request peticion del cliente al servidor
	 */
	public void guardaParametrosEnSession(HttpServletRequest request) {
		textodeLog("guardaParametrosEnSession");
		EIGlobal.mensajePorTrace(textoLog + "Dentro de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
		request.getSession().setAttribute("plantilla", request.getServletPath());
		final HttpSession session = request.getSession(false);
		session.removeAttribute("bitacora");
		if (session != null) {
			Map tmp = new HashMap();
			EIGlobal.mensajePorTrace(textoLog + "valida-> " + getFormParameter(request, VALIDAX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "operacion-> " + getFormParameter(request, OPERACIONX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "registro-> " + getFormParameter(request, REGISTROX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "tipoArchivo-> " + getFormParameter(request, TIPOARCHIVOX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "statusDuplicado-> " + getFormParameter(request, STATUSDUPLICADOX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "statushrc-> " + getFormParameter(request, STATUSHRCX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "encFechApli-> " + getFormParameter(request, ENCFECHAPLIX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "horario_seleccionado-> " + getFormParameter(request, HORARIO_SELECCIONADOX), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "Concepto-> " + getFormParameter(request, STR_CONCEPTO), EIGlobal.NivelLog.DEBUG);
			String value;
			value = (String) getFormParameter(request, VALIDAX);
			if (value != null && !(STR_NULL).equals(value)) {
				tmp.put(VALIDAX, getFormParameter(request, VALIDAX));
			}
			tmp.put(OPERACIONX, getFormParameter(request, OPERACIONX));
			tmp.put(REGISTROX, getFormParameter(request, REGISTROX));
			tmp.put(TIPOARCHIVOX, getFormParameter(request, TIPOARCHIVOX));
			tmp.put(STATUSDUPLICADOX, getFormParameter(request, STATUSDUPLICADOX));
			tmp.put(STATUSHRCX, getFormParameter(request, STATUSHRCX));
			tmp.put(ENCFECHAPLIX, getFormParameter(request, ENCFECHAPLIX));
			tmp.put(HORARIO_SELECCIONADOX, getFormParameter(request, HORARIO_SELECCIONADOX));
			tmp.put(STR_CONCEPTO, getFormParameter(request, STR_CONCEPTO));
			tmp.put(FOLIOX, getFormParameter(request, FOLIOX));

			//Favoritos
			EIGlobal.mensajePorTrace(textoLog + "favorito desc-> " + getFormParameter(request, "txtFav"), EIGlobal.NivelLog.DEBUG);
			tmp.put(STR_CHECK, getFormParameter(request, STR_CHECK));
			tmp.put("txtFav", getFormParameter(request, "txtFav"));

			session.setAttribute("parametros", tmp);
			tmp = new HashMap();
			final Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				final String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
		EIGlobal.mensajePorTrace(textoLog + "Saliendo de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Metodo que pone concepto de archivo a los registros
	 * @param nombreArchivo nombre del archivo al que se actualizara concepto
	 * @param Concepto valor del concepto que se actualizara en el archivo
	 * @return bandera devuelve falso o verdadero de la operacion
	 */
	boolean actualizaConceptoEnArchivo(String nombreArchivo, String Concepto) {
		textodeLog("actualizaConceptoEnArchivo");
		String line = "";
		BufferedReader arc = null;
		BufferedWriter arcWrite = null;
		final String nombreArchivoEscritura = nombreArchivo + ".nom";
		try {
			// Se le quita el path al archivo para operar con el, en el
			// directorio temporal del app
			EIGlobal.mensajePorTrace(textoLog + "El archivo origen: " + nombreArchivo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "El archivo destino: " + nombreArchivoEscritura, EIGlobal.NivelLog.DEBUG);
			arc = new BufferedReader(new FileReader(nombreArchivo));
			arcWrite = new BufferedWriter(
					new FileWriter(nombreArchivoEscritura));
			while ((line = arc.readLine()) != null) {
				if (line.length() > 126) {
					line += Concepto;
				}
				arcWrite.write(line + "\n");
			}
			final File a = new File(nombreArchivo);
			final File b = new File(nombreArchivoEscritura);
			a.delete();
			b.renameTo(new File(nombreArchivo));

		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			return false;
		}finally{
			try {
				if(arc!=null){
					arc.close();
				}
				if(arcWrite!=null){
					arcWrite.close();
				}
			} catch (IOException d) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(d), EIGlobal.NivelLog.INFO);
			}
		}
		return true;
	}

	/**
	 * Metodo que ejecuta servicio Tuxedo para generar folio
	 * @param usuario usuario del cliente
	 * @param perfil perfil del cliente
	 * @param contrato contrato del cliente
	 * @param servicio servicio que se ejecutara para generar folio
	 * @param registros registros de operacion
	 * @return folioArchivo devuelve el folio de operacion del archivo
	 */
	public String generaFolio(String usuario, String perfil, String contrato,
			String servicio, String registros) {
		String result = "";
		String codError = "";
		String folioArchivo = "";
		final String tramaParaFolio = usuario + "|" + perfil + "|" + contrato + "|"	+ servicio + "|" + registros + "|";
		final ServicioTux tuxGlobal = new ServicioTux();
		try {
			final Hashtable hs = tuxGlobal.alta_folio(tramaParaFolio);
			result = (String) hs.get(BUFFERX);
		} catch (Exception e) {
			//re.printStackTrace();
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace(ERROREFILESRVR, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNominaLn::generaFolio:: "+SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(DATOSGENERALESX, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(USUARIOX + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(CONTRATOX + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(MODULOACONSULTARX + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(LINEADETRUENEX + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("OpcionesNominaLn::generaFolio::"
						+ SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX
						+ e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}

		if (result == null || (STR_NULL).equals(result)){
			result = "ERROR            Error en el servicio.";}

		EIGlobal.mensajePorTrace("Respuesta de BUFFER en generaFolio ->" + result, EIGlobal.NivelLog.INFO);
		if (result.contains("|")) {
			codError = result.substring(0, result.indexOf('|'));
		}

		if (("TRSV0000").equals(codError.trim())) {
			folioArchivo = result.substring(result.indexOf('|') + 1, result
					.length());
			folioArchivo = folioArchivo.trim();
			if (folioArchivo.length() < 12) {
				do {
					folioArchivo = "0" + folioArchivo;
				} while (folioArchivo.length() < 12);
			}
		}
		return folioArchivo;
	}

	/**
	 * Metodo que ejecuta servicio tuxedo para el envio de datos importados
	 * @param usuario usuario del cliente
	 * @param perfil perfil del cliente
	 * @param contrato contrato del cliente
	 * @param folio folio del cliente
	 * @param servicio servicio que se ejecutara
	 * @param ruta ruta del archivo
	 * @param trama trama del servicio de envio de datos
	 * @return mensajeErrorEnviar devuelve el mensaje de error de la operacion
	 */
	public String envioDatos(String usuario, String perfil, String contrato,
			String folio, String servicio, String ruta, String trama) {
		textodeLog("envioDatos");
		String result = "";
		String codError = "";
		String mensajeErrorEnviar = "";
		final String tramaParaEnviar = usuario + "|" + perfil + "|" + contrato + "|"+ folio + "|" + servicio + "|" + ruta + "_preprocesar" + "|" + ruta + "|" + trama;
		final ServicioTux tuxGlobal = new ServicioTux();
		try {
			final Hashtable hs = tuxGlobal.envia_datos(tramaParaEnviar);
			result = (String) hs.get(BUFFERX);
		} catch (Exception e) {
			//re.printStackTrace();
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace(ERROREFILESRVR, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNominaLn::envioDatos:: "+SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX + e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(DATOSGENERALESX, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(USUARIOX + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(CONTRATOX + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(MODULOACONSULTARX + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(LINEADETRUENEX + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("OpcionesNominaLn::envioDatos::" + SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX + e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		if (result == null || (STR_NULL).equals(result)) {
			result = "ERROR            Error en el servicio.";
		}
		EIGlobal.mensajePorTrace(textoLog + "CONTROLADOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Respuesta de BUFFER en envioDatos ->" + result, EIGlobal.NivelLog.INFO);
		if (result.contains("|")) {
			codError = result.substring(0, result.indexOf('|'));
		}
		if (!("TRSV0000").equals(codError.trim())) {
			EIGlobal.mensajePorTrace(textoLog + "No se enviaron los registros al controlador de registros Mensaje: " + mensajeErrorEnviar, EIGlobal.NivelLog.DEBUG);
		}
		else {
			EIGlobal.mensajePorTrace(textoLog + "registros enviados al controlador de registros Mensaje: " + mensajeErrorEnviar, EIGlobal.NivelLog.DEBUG);
		}
		mensajeErrorEnviar = result.substring(result.indexOf('|') + 1, result.length());
		return mensajeErrorEnviar;
	}

	/**
	 * Metodo que ejecuta servicio tuxedo para la dispersion del archivo improtado
	 * @param usuario usuario del cliente
	 * @param perfil perfil del cliente
	 * @param contrato contrato del cliente
	 * @param folio folio del cliente
	 * @param servicio servicio de dispersion
	 * @return mensajeErrorDispersor devuelve respuesta de dispersion
	 */
	public String realizaDispersion(String usuario, String perfil, String contrato, String folio, String servicio) {
		textodeLog("realizaDispersion");
		String result = "";
		String codError = "";
		String mensajeErrorDispersor = "";
		final String tramaParaDispersor = usuario + "|" + perfil + "|" + contrato + "|" + folio + "|" + servicio + "|";
		final ServicioTux tuxGlobal = new ServicioTux();
		try {
			final Hashtable hs = tuxGlobal.inicia_proceso(tramaParaDispersor);
			result = (String) hs.get(BUFFERX);
		} catch (Exception e) {
			//re.printStackTrace();
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace(ERROREFILESRVR, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OpcionesNominaLn::realizaDispersion:: "+SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(DATOSGENERALESX, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(USUARIOX + session.getUserID8(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(CONTRATOX + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(MODULOACONSULTARX + modulo, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(LINEADETRUENEX + lineaError[0], EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("OpcionesNominaLn::realizaDispersion::"
						+ SEDETECTOYCONTROLOELPROBLEMASIGUIENTEX
						+ e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		if (result == null || (STR_NULL).equals(result)){
			result = "ERROR            Error en el servicio.";}
		EIGlobal.mensajePorTrace(textoLog + "Dispersor de registros: Trama salida: " + result, EIGlobal.NivelLog.INFO);
		try{
			codError = result.substring(0, result.indexOf('|'));
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			codError = "";
		}
		if (!("TRSV0000").equals(codError.trim())) {
			EIGlobal.mensajePorTrace(textoLog + "No se realizo la dispersion de registros", EIGlobal.NivelLog.INFO);
		} else {
			EIGlobal.mensajePorTrace(textoLog + "Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.INFO);
		}
		mensajeErrorDispersor = result.substring(result.indexOf('|') + 1,
				result.length());
		return mensajeErrorDispersor;
	}

	/**
	 * Metodo que ejecuta los servicios tuxedo
	 * @param trama trama del servicio a ejecutar
	 * @return tramaSalida devuelve respuesta del servicio ejecutado
	 */
	private String ejecutaServicio(String trama) {
		textodeLog("ejecutaServicio");
		Hashtable htResult = null;
		final ServicioTux tuxGlobal = new ServicioTux();
		String tramaSalida= "";
		try {
			htResult = tuxGlobal.web_red(trama);
			tramaSalida = (String) htResult.get(BUFFERX);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			tramaSalida = "";
		}
		EIGlobal.mensajePorTrace(textoLog + "tramaSalida: [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);
		return tramaSalida;
	}

	/**
	 * Funcion que contruye la tabla de totales
	 * @param cuenta cuenta del cliente
	 * @param descripcion descripcion
	 * @param fchAplicacion fecha de aplicacion
	 * @param importe importe del pago
	 * @param registros registros procesados
	 * @return tablaTotales devuelve tabla que muestra el total de pago
	 */
	public String generaTablaTotales(String cuenta, String descripcion, String fchAplicacion, String importe, String registros){
		final String bgcolor = "textabdatobs";
		final StringBuffer tablaTotales = new StringBuffer("<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" + TR +
				"<td class=\"textabref\">Pago de n&oacute;mina</td>"+ TRC + "</table>" + "<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">" + TR +
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>" + "<td width=\"80\" class=\"tittabdat\" align=\"center\"><center>Descipci&oacute;n</center></td>" +	"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci�n</td>" +
				"<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>"+ "<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>"+
				TRC + TDCLASS +	bgcolor + NOWRAPALINGCENTER + cuenta + ESPACIOTD + TDCLASS + bgcolor + NOWRAPALINGCENTER + descripcion + ESPACIOTD + TDCLASS + bgcolor + NOWRAPALINGCENTER + fchAplicacion + ESPACIOTD + TDCLASS + bgcolor+
				NOWRAPALINGCENTER +	formatoMoneda(importe) + ESPACIOTD + TDCLASS + bgcolor + NOWRAPALINGCENTER + registros + ESPACIOTD + "</table><br>");
		return tablaTotales.toString();
	}

	/**
	 * validaTerciaBiatux lee archivo  para validar la tercia.
	 * @param bean datos de entrada
	 */
	public NominaLineaBean validaTerciaBiatux(NominaLineaBean bean) {
		BufferedReader arc = null;
		NominaLineaBO bo=new NominaLineaBO();
		String archivo=Global.directorioConfignln+"/config.cfg";
		try {
			EIGlobal.mensajePorTrace(textoLog + "Archivo::::  " + archivo, EIGlobal.NivelLog.DEBUG);
			arc = new BufferedReader(new FileReader(archivo));
			String linea=arc.readLine();
			linea=linea != null ? linea : "";
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: linea : "+linea, EIGlobal.NivelLog.INFO);
			bean.setServidorBiatux(linea.trim());
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: servidorBiatux = "+bean.getServidorBiatux(), EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: el archivo " + archivo + " no existe .:.:.", EIGlobal.NivelLog.INFO);
			bean.setServidorBiatux(" ");
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: se pone por default en el equipo biatux el valor de '" + bean.getServidorBiatux() + "' .:.:. ", EIGlobal.NivelLog.INFO);
		}finally{
			try {
				if(arc!=null){
					arc.close();
				}
			} catch (IOException d) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(d), EIGlobal.NivelLog.INFO);
			}
		}
		return bean;
	}


	/**
	 * @author FSW Vector
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion a manejar
	 * @throws java.io.IOException excepcion a manejar
	 * Funcion de registro de solicitudes de informe PDF
	 */
	public void solicitaInformePDF(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	java.io.IOException{

		StringBuffer mensaje = new StringBuffer("");
		final HttpSession sess = request.getSession(false);
		final BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
		NominaBO bo = null;
		SolPDFBean pdfBean = null;
		Date fechaRecep;
		SimpleDateFormat sdf;
        boolean resultado = false;
    	String secuencia = "";
    	String strFch = "";
    	int tipoMensaje;

        final String tipoFecha = request.getParameter("rbtnFecha") != null ? request.getParameter("rbtnFecha") : request.getParameter("hddTipoFecha");
		EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF :: rbtnFecha: "+tipoFecha, EIGlobal.NivelLog.INFO);
		try{

			String []arrDatos = null; //Se desentraman datos para secuencia
			String strDatos = (String) getFormParameter(request, PARAM_DATOS_NL);
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF :: Procesando Datos [" + strDatos + "]" ,
					                  EIGlobal.NivelLog.INFO);

			if(strDatos != null && !"".equals(strDatos)){
				arrDatos = strDatos.split(SEPARADOR);
				if(arrDatos != null){
					secuencia = arrDatos[0].trim();
					strFch = arrDatos[2];
					EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF ::Secuencia [" + secuencia + "]" ,
							                  EIGlobal.NivelLog.DEBUG);

					if(session != null){
						pdfBean = new SolPDFBean();
						bo = new NominaBO();

						sdf = new SimpleDateFormat(FOR_FECHA_AMD);
						fechaRecep = sdf.parse(strFch);
						pdfBean.setContrato(session.getContractNumber());
						pdfBean.setUsuarioAlta(session.getUserID8());
						pdfBean.setSecuencia(Integer.parseInt(secuencia));
						pdfBean.setEstatus(VAL_ESTAT_SOLPDF_SOL);
						pdfBean.setTipoNomina(VAL_TIPO_SOLPDF_NL);
						pdfBean.setFchPago(fechaRecep);
						pdfBean.setTipoOper(BIT_OPER_SOLPDF_NL);

						resultado = bo.registraSolcitudPDF(request,pdfBean);
						String auxMensaje = "";
						if(resultado) {
							auxMensaje = pdfBean.getMensaje();
							tipoMensaje = 1;

						}else {
							auxMensaje = pdfBean.getMensaje() ;
							tipoMensaje = 10;

						}
						mensaje = new StringBuffer(CUADRODIALOGOPAGOSNOMINAX ).append( MSG_ENCAB_SOLPDF ).append( "\",\"" ).append( auxMensaje ).append( "\"," ).append( tipoMensaje ).append( ");");// ACEPTAR, CANCELAR
						request.setAttribute(INFOUSERX, mensaje.toString());
						sdf = null;
					}
				}
			}else{
				mensaje = new StringBuffer( CUADRODIALOGOPAGOSNOMINAX ).append( MSG_ENCAB_SOLPDF ).append( "\",\"" ).append( MSG_SOLPDF_ERROR_PARAM ).append( "\",9);");// ACEPTAR, CANCELAR
				request.setAttribute(INFOUSERX, mensaje.toString());
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		//Se setea nuevamente el tipo de fecha
		request.setAttribute("pTipoFecha", tipoFecha);
		consultarArchivos(request,response);

	}

	/**
	 * Funcion de descarga de  informes PDF de NOmina
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion a manejar
	 * @throws java.io.IOException excepcion a manejar
	 *
	 */
	public void descargaInformePDF(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	java.io.IOException{
		StringBuffer mensaje = new StringBuffer("");
		final HttpSession sess = request.getSession(false);
		final BaseResource session = (BaseResource) sess.getAttribute(SESSIONX);
		String secuencia = "";
		String strFch = "";
		String urlDescarga = "";
		CuentasDescargaBean descargaBean = null;
		DescargaEstadoCuentaBO descargaEstadoCuentaBO = null;
		NominaBO nominaBO = null;
		boolean registroDescarga = false;
		SimpleDateFormat sdf = null;
		Date  fechaRecep = null;
		String fchDescarga = "";



			String []arrDatos = null; //Se desentraman datos para secuencia
			String strDatos = (String) getFormParameter(request, "secDescarga");

            final String tipoFecha = request.getParameter("rbtnFecha") != null ? request.getParameter("rbtnFecha") : request.getParameter("hddTipoFecha");
			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF :: rbtnFecha: "+tipoFecha, EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF :: Procesando Datos [" + secuencia + "]" ,
					                  EIGlobal.NivelLog.INFO);

			if(strDatos != null && !"".equals(strDatos)){

				arrDatos = strDatos.split(SEPARADOR);

				if(arrDatos != null ){
					secuencia = arrDatos[0].trim();
					strFch = arrDatos[2];
					EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF ::Secuencia [" + secuencia + "]" ,
							EIGlobal.NivelLog.DEBUG);



					EIGlobal.mensajePorTrace("OpcionesNominaLn.java :: solicitaInformePDF ::Secuencia [" + secuencia + "]" ,
							EIGlobal.NivelLog.DEBUG);

					if(session != null){

						sdf = new SimpleDateFormat(FOR_FECHA_AMD);

						try {
							fechaRecep = sdf.parse(strFch);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							EIGlobal.mensajePorTraceExcepcion(e);
						}

						sdf = null;

						sdf = new SimpleDateFormat("MMddyy");//formato para descarga de Ondemand
						fchDescarga = sdf.format(fechaRecep);

						descargaEstadoCuentaBO=new DescargaEstadoCuentaBO();
						nominaBO = new NominaBO();
						descargaBean = new CuentasDescargaBean();
						descargaBean.setLlaveDescarga(descargaEstadoCuentaBO.genKey());
						descargaBean.setCodigoCliente(session.getUserID8());
						descargaBean.setCuenta(session.getContractNumber());
						descargaBean.setTipo("N");
						descargaBean.setTipoEdoCta(VAL_TIPO_SOLPDF_NL);
						descargaBean.setPais("MX");
						descargaBean.setPeriodoSeleccionado(fchDescarga);
						descargaBean.setFolioSeleccionado(secuencia);


						try{

							registroDescarga = nominaBO.insertaRegistroDescarga(descargaBean);

						}catch(BusinessException e){
							EIGlobal.mensajePorTraceExcepcion(e);

						}

						if(registroDescarga){
							urlDescarga = "../../../EdoEnlace/edoCtaEnlace/descargaInformeNom.do?cadena="+descargaBean.getLlaveDescarga();
							request.setAttribute("URLDescargaPDF", urlDescarga);
						}else {
							mensaje = new StringBuffer( CUADRODIALOGOPAGOSNOMINAX ).append( MSG_ENCAB_SOLPDF ).append( "\",\"" ).append( "Error al Crear Registro en Tabla para Descarga " ).append( "\",10);");// ACEPTAR, CANCELAR
							request.setAttribute(INFOUSERX, mensaje.toString());
						}
					}
				}
			}

	        //Se setea nuevamente el tipo de fecha
		    request.setAttribute("pTipoFecha", tipoFecha);
			request.setAttribute(NEWMENUX, session.getFuncionesDeMenu());
			request.setAttribute(MENUPRINCIPALX, session.getStrMenu());
			request.setAttribute(ENCABEZADOX,CreaEncabezado(CONSULTADENOMINAENLINEAX,"Servicios &gt; N&oacute;mina  &gt; Consulta &gt; Pagos en L&iacute;nea","s25800canh", request));
			request.setAttribute(TEXTCUENTAX, "");
			request.setAttribute("pContrato", request.getSession().getAttribute("sesContrato"));
			request.setAttribute("pCtaCargo", request.getSession().getAttribute("sesCtaCargo"));
			request.setAttribute("pFchIni", request.getSession().getAttribute("sesFchIni"));
			request.setAttribute("pFchFin", request.getSession().getAttribute("sesFchFin"));
			request.setAttribute("totalRegs", request.getSession().getAttribute("sestotalRegs"));
			request.setAttribute("pPaginas", request.getSession().getAttribute("sesPaginas"));
			request.setAttribute("pPagina", request.getSession().getAttribute("sesPagina"));
			request.setAttribute("resultadoConsulta", request.getSession().getAttribute("sesresultadoConsulta"));
			request.setAttribute(ARCHIVOEXPORTAR, "");
			request.setAttribute("pDel", request.getSession().getAttribute("sesDel"));
			request.setAttribute("pAl", request.getSession().getAttribute("sesAl"));
			evalTemplate(IEnlace.NOMINALNRESULT_TMPL, request, response);

	}
}