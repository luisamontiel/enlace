/********************* CONTROL DE CAMBIOS ********************************
 **	No	 		Descripci�n					Responsable		Fecha
 **	1		Se realiza notificaci�n por		  EVERIS		29/11/2007
 **			cambio de Correo electr�nico
************************************************************************/

package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;


/**
 *  Clase encargada de manejar el proceso de movimientos de estados de cuenta
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Nov 22, 2010
 */
public class MTE_MovEdoCuenta extends BaseServlet {

	/**
	 * Numero de versi�n serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constante para identificar servicio
	 */
	public static final String SERVICIO_TUXEDO = "CONS_MOV_EDOCTA";

	/**
	 * Opci�n para la iniciar
	 */
	public static final String ACCION_INICIAR = "iniciar";

	/**
	 * Opci�n para la exportar
	 */
	public static final String ACCION_EXPORTAR = "exportar";

	/**
	 * Metodo encargado de iniciar la actividad metodo Get
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 * @throws ServletException		Error de tipo ServletException
	 * @throws IOException			Error de tipo IOException
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		inicioServletMovEdoCuenta(req, res);
	}

	/**
	 * Metodo encargado de iniciar la actividad metodo Post
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 * @throws ServletException		Error de tipo ServletException
	 * @throws IOException			Error de tipo IOException
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		inicioServletMovEdoCuenta(req, res);
	}

	/**
	 * Metodo general donde iniciara la acci�n del servlet
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @param res					Respuesta del servidor de aplicaciones
	 */
	private void inicioServletMovEdoCuenta(
			HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
		HashMap beanMap = new HashMap();
		String opcion = null;
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		sess.removeAttribute("fileExport");
		boolean sesionvalida = SesionValida(req, res);

		//Validando la session
        if(!sesionvalida){
        	return;
        }

        /******************************************Inicia validacion OTP**************************************/
        String valida = req.getParameter ("valida");
        if(validaPeticion( req,  res,session,sess,valida)){
        	EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::inicioServletMovEdoCuenta:: Entra a validar la petici�n.", EIGlobal.NivelLog.INFO);
        }else{/************************************Termina validacion OTP*************************************/
    		EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::inicioServletMovEdoCuenta:: Entrando al servlet.", EIGlobal.NivelLog.INFO);
    		beanMap = cargaParametros(req);
    		opcion = (String)beanMap.get("opcion");

    		/**
			 * Opci�n iniciar
			 */
			if(opcion.equals(ACCION_INICIAR)){
				try{
					EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::inicioServletMovEdoCuenta:: Opci�n Iniciar", EIGlobal.NivelLog.INFO);
					if(manejaArchivo(req, res, beanMap)){
						EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::inicioServletMovEdoCuenta:: Finalizando iniciar exitoso.", EIGlobal.NivelLog.INFO);
						sess.setAttribute("fileExport","/Download/" + "TSFE_MOVEDOCTA_" + getFileName(session.getUserID8(),2) + ".doc");
						evalTemplate("/jsp/MTE_MovEdoCuenta.jsp", req, res);
					}else{
						EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::inicioServletMovEdoCuenta:: Finalizando iniciar con problemas.", EIGlobal.NivelLog.INFO);
						paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req, res);
					}
				}catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req, res);
				}
			}
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::inicioServletCapturaDatosUsuario:: Saliendo de Movimientos de Estados de Cuenta.", EIGlobal.NivelLog.INFO);
       }

	}

	/**
	 * Metodo encargado de manejar todo el negocio para la extracci�n del
	 * archivo.
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @param beanMap				Mapa con parametros provenientes de la vista.
	 * @return flag					true  - si el proveso de extracci�n fue correcto.
	 * 								false - si hubo algun problema inesperado.
	 */
	private boolean manejaArchivo(HttpServletRequest req, HttpServletResponse res, HashMap beanMap){
		EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: Entrando...", EIGlobal.NivelLog.DEBUG);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		ServicioTux tuxGlobal = new ServicioTux ();
		ArchivoRemoto archR = new ArchivoRemoto();
		Hashtable hs = null;
		boolean flag = true;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = null;
		String rutaOrigen = null;
		String rutaDestino = null;
		String nombreArchivo = null;
		String downloadFile = null;
		File verifyFile = null;

		try{
			rutaOrigen = Global.RUTA_TESOFE_MOV_EDO_CTA;
			rutaDestino = Global.DIRECTORIO_REMOTO + "/";
			nombreArchivo = getFileName("",1);
			downloadFile = Global.DIRECTORIO_REMOTO_WEB + "/TSFE_MOVEDOCTA_" + getFileName(session.getUserID8(),2) + ".doc";

			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: rutaOrigen->" + rutaOrigen, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: rutaDestino->" + rutaDestino, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: nombreArchivo->" + nombreArchivo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: downloadFile->" + downloadFile, EIGlobal.NivelLog.DEBUG);

			verifyFile = new File(downloadFile);
			if(!verifyFile.exists()){
				tramaEntrada = rutaOrigen + "@"
					 		 + rutaDestino + "@"
					 		 + nombreArchivo + "@";

				EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: tramaEntrada->" + tramaEntrada, EIGlobal.NivelLog.DEBUG);
				hs = tuxGlobal.validaTransferencias(tramaEntrada, SERVICIO_TUXEDO);
				if(hs != null) {
					CodError = (String) hs.get("COD_ERROR");
					tramaSalida = (String) hs.get("BUFFER");
				}
				EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: CodError->" + CodError, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: tramaSalida->" + tramaSalida, EIGlobal.NivelLog.DEBUG);
				if(CodError.equals("TCMO0000") && tramaSalida!=null){
					if(!archR.copia(nombreArchivo)) {
		  				EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: Problemas para traer el archivo.", EIGlobal.NivelLog.INFO);
		  				flag = false;
		  			}else{
		  				if(!archR.copiaLocalARemoto(nombreArchivo,"WEB")) {
			  				EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: Problemas para copiar al download.", EIGlobal.NivelLog.INFO);
			  				flag = false;
		  				}else{
		  					EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: Empieza proceso exportar.", EIGlobal.NivelLog.DEBUG);
		  					exportar(req,res,nombreArchivo, downloadFile);
		  				}
		  			}
					if(hs != null) hs.clear();

				}else if(CodError.equals("TCMO9999")){
					EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: No se pudo obtener el archivo de la interface.", EIGlobal.NivelLog.INFO);
					flag = false;
				}
			}else{
				EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: El archivo ya se obtuvo el d�a de hoy.", EIGlobal.NivelLog.INFO);
			}


		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::manejaArchivo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
		               				+ "Usuario->" + session.getUserID8()
		               				+ "Contrato->" + session.getContractNumber()
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
			flag = false;
		}


		req.setAttribute("cveContrato", (String)beanMap.get("cveContrato"));
		req.setAttribute("descContrato", (String)beanMap.get("descContrato"));
		req.setAttribute("newMenu", (String) beanMap.get("newMenu"));
		req.setAttribute("MenuPrincipal", (String) beanMap.get("MenuPrincipal"));
		req.setAttribute("Encabezado", (String) beanMap.get("Encabezado"));
		return flag;
	}

	/**
	 * Metodo para manejar el negocio para exportar los beneficiarios
	 * que se autorizaran y cancelaran.
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @param nombreArchivo			nombre del archivo con la consulta
	 * @param downloadFile			nombre del archivo exportado
	 * @throws Exception			Dispara Exception
	 */
	private void exportar(HttpServletRequest req, HttpServletResponse res, String nombreArchivo, String downloadFile)
	throws Exception{
		EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::exportar:: Entrando...", EIGlobal.NivelLog.DEBUG);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String nombreArchivoFinal = null;
		String linea = null;
		String tituloLinea = null;

		nombreArchivoFinal = Global.DIRECTORIO_REMOTO_WEB + "/" + nombreArchivo;
		BufferedReader entrada = new BufferedReader(new FileReader(nombreArchivoFinal));
		FileWriter fileExport = new FileWriter(downloadFile);
		EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::exportar:: Se creo el archivo.", EIGlobal.NivelLog.DEBUG);

		try{
			tituloLinea = "Num. de Cuenta,"
				        + "Hora,"
				        + "Fecha Op.,"
				        + "Centro Operante(Suc),"
				        + "Descrip. Codigo,"
				        + "Codigo,"
				        + "Importe,"
				        + "Saldo Nuevo,"
				        + "Observaciones,"
				        + "Referencia,"
				        + "Codigo de Oper. TESOFE,"
				        + "Folio";
			fileExport.write(tituloLinea + "\n");
			linea = entrada.readLine();
			while(linea != null){
				fileExport.write(linea + "\n");
				linea = entrada.readLine();
			}

		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::exportar:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
								   + "<DATOS GENERALES>"
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.INFO);
		}finally{
			fileExport.close();
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::exportar:: Se cerro el archivo.", EIGlobal.NivelLog.DEBUG);
		}
	}

	/**
	 * rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 */
	public static String rellenar (String cad, int lon, char rel, char tip){
		String aux = "";
		if(cad.length()>lon) return cad.substring(0,lon);
		if(tip!='I' && tip!='D') tip = 'I';
		if( tip=='D' ) aux = cad;
		for (int i=0; i<(lon-cad.length()); i++) aux += ""+ rel;
		if( tip=='I' ) aux += ""+ cad;
		return aux;
	}

	/**
	 * Metodo encargado de cargar en un mapa los parametros provenientes de la
	 * pantalla
	 *
	 * @param req					Petici�n del servidor de aplicaciones
	 * @return parametros			Mapa con Parametros llenos
	 */
	private HashMap cargaParametros(HttpServletRequest req) {
		HashMap parametros = new HashMap();
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		parametros.put("opcion", getFormParameter(req, "opcion"));
		parametros.put("tipoMod", getFormParameter(req, "tipoMod"));
		parametros.put("cveContrato", session.getContractNumber());
		parametros.put("descContrato", session.getNombreContrato());
		parametros.put("MenuPrincipal", session.getStrMenu());
		parametros.put("Encabezado", CreaEncabezado("Movimientos de Estados de Cuenta", "Servicios &gt; TESOFE &gt; Movimientos de Estados de Cuenta", "s55170h", req));
		parametros.put("newMenu", session.getFuncionesDeMenu());
		return parametros;
	}

	/**
	 * Muestra una pagina de error con la descripcion indicada
	 * @param mensaje			Mensaje de error presentado en pantalla
	 * @param request			request de la operaci�n
	 * @param response			response de la operaci�n
	 *
	 */
	public void paginaError(String mensaje,HttpServletRequest request, HttpServletResponse response){
		try{
			despliegaPaginaErrorURL(mensaje,
				"TESOFE",
				"Servicios &gt; TESOFE &gt; ",
				"Movimientos de Estados de Cuenta",
				request,
				response);
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::paginaError:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
								   + "<DATOS GENERALES>"
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo encargado de desplegar la pagina de error
	 * por medio de una URL
	 *
	 * @param Error			Error a desplegar
	 * @param param1		Parametro del encabezado
	 * @param param2		Parametro del encabezado
	 * @param param3 		Parametro del encabezado
	 * @param request		Petici�n de la aplicaci�n
	 *
	 */
	private void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response )
	throws IOException, ServletException{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RequestDispatcher view = null;

		EIGlobal.mensajePorTrace("MTE_MovEdoCuenta::despliegaPaginaErrorURL:: Entrando.", EIGlobal.NivelLog.DEBUG);

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		String param4 = "s55205h";

		request.setAttribute( "FechaHoy", fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2 + param3, param4, request ) );

		//evalTemplate("/jsp/EI_Mensaje.jsp", request, response);
		view = request.getRequestDispatcher("/jsp/EI_Mensaje.jsp");
		view.forward(request,response);
	}

	/**
	 * Metodo para definir la fecha para el mensaje de error.
	 *
	 * @param strFecha			formato de la fecha
	 * @return Fecha Formato
	 */
	private String fechaHoy(String strFecha) {
		Date Hoy = new Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);

		String dd = "";
		String mm = "";
		String aa = "";
		String aaaa = "";
		String dt = "";
		String mt = "";
		String th = "";
		String tm = "";
		String ts = "";

		int indth = 0;
		int indtm = 0;
		int indts = 0;
		int inddd = 0;
		int indmm = 0;
		int indaaaa = 0;
		int indaa = 0;
		int indmt = 0;
		int inddt = 0;

		String[] dias = {
			"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes",
			"Sabado"
		};
		String[] meses = {
			"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
			"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
		};

		if (Cal.get(Calendar.DATE) <= 9)
			dd += "0" + Cal.get(Calendar.DATE);
		else
			dd += Cal.get(Calendar.DATE);

		if (Cal.get(Calendar.MONTH) + 1 <= 9)
			mm += "0" + (Cal.get(Calendar.MONTH) + 1);
		else
			mm += (Cal.get(Calendar.MONTH) + 1);

		if (Cal.get(Calendar.HOUR_OF_DAY) < 10)
			th += "0" + Cal.get(Calendar.HOUR_OF_DAY);
		else
			th += Cal.get(Calendar.HOUR_OF_DAY);

		if (Cal.get(Calendar.MINUTE) < 10)
			tm += "0" + Cal.get(Calendar.MINUTE);
		else
			tm += Cal.get(Calendar.MINUTE);

		if (Cal.get(Calendar.SECOND) < 10)
			ts += "0" + Cal.get(Calendar.SECOND);
		else
			ts += Cal.get(Calendar.SECOND);

		aaaa += Cal.get(Calendar.YEAR);
		aa += aaaa.substring(aaaa.length() - 2, aaaa.length());
		dt += dias[Cal.get(Calendar.DAY_OF_WEEK) - 1];
		mt += meses[Cal.get(Calendar.MONTH)];

		while (
			indth >= 0 || indtm >= 0 || indts >= 0 || inddd >= 0
			|| indmm >= 0 || indaaaa >= 0 || indaa >= 0 || indmt >= 0
			|| inddt >= 0
			) {
			indth = strFecha.indexOf("th");
			if (indth >= 0)
				strFecha = strFecha.substring(0, indth) + th + strFecha.substring(indth + 2, strFecha.length());
			indtm = strFecha.indexOf("tm");
			if (indtm >= 0)
				strFecha = strFecha.substring(0, indtm) + tm + strFecha.substring(indtm + 2, strFecha.length());
			indts = strFecha.indexOf("ts");
			if (indts >= 0)
				strFecha = strFecha.substring(0, indts) + ts + strFecha.substring(indts + 2, strFecha.length());
			inddd = strFecha.indexOf("dd");
			if (inddd >= 0)
				strFecha = strFecha.substring(0, inddd) + dd + strFecha.substring(inddd + 2, strFecha.length());
			indmm = strFecha.indexOf("mm");
			if (indmm >= 0)
				strFecha = strFecha.substring(0, indmm) + mm + strFecha.substring(indmm + 2, strFecha.length());
			indaaaa = strFecha.indexOf("aaaa");
			if (indaaaa >= 0)
				strFecha = strFecha.substring(0, indaaaa) + aaaa + strFecha.substring(indaaaa + 4, strFecha.length());
			indaa = strFecha.indexOf("aa");
			if (indaa >= 0)
				strFecha = strFecha.substring(0, indaa) + aa + strFecha.substring(indaa + 2, strFecha.length());
			indmt = strFecha.indexOf("mt");
			if (indmt >= 0)
				strFecha = strFecha.substring(0, indmt) + mt + strFecha.substring(indmt + 2, strFecha.length());
			inddt = strFecha.indexOf("dt");
			if (inddt >= 0)
				strFecha = strFecha.substring(0, inddt) + dt + strFecha.substring(inddt + 2, strFecha.length());
		}
		return strFecha;
	}

	/**
	 * Metodo encargado de crear el nombre del archivo que se exportara
	 *
	 * @param userNumber		Numero del usuario
	 * @return fileUserName		Nombre final del archivo
	 */
	private String getFileName(String userNumber, int opc){
		GregorianCalendar cal = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", new java.util.Locale("es","mx"));
		String fileName = null;
		if(opc==1)
			fileName = "MOVEDOCTA.txt";
		else if(opc==2)
			fileName = userNumber + "_" + sdf.format(cal.getTime());
		return fileName;
	}

}