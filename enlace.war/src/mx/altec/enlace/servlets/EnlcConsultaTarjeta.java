package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.FiltroConsultaBean;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EnlcTarjetaBO;
import mx.altec.enlace.bo.EnlcTarjetaBOImpl;
import mx.altec.enlace.dao.DaoException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * Servlet implementation class consulta tarjetas
 */
public class EnlcConsultaTarjeta extends BaseServlet {
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Log
	 */
	private static final String TEXTOLOG = EnlcConsultaTarjeta.class.getName();
	/**
	 * start
	 */
	private static final String START = "start";

	/**
	 * Metodo que recibe las peticiones get
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(verificaFacultad ("CONVINCTARJ",request)){		
			HttpSession sess;
			sess = request.getSession();
			BaseResource session;
			session = (BaseResource) sess.getAttribute("session");
			request.getSession().removeAttribute("MensajeLogin01");
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request.setAttribute("newMenu", session.getFuncionesDeMenu());

			request.setAttribute("Encabezado",    CreaEncabezado("Consulta de Tarjetas",
					"Servicios &gt; Tarjeta de Pagos Santander &gt; Vinculaci&oacute;n tarjeta &ndash; contrato &gt Consulta y Baja", "", request ) );

			int operacion;
			operacion = Integer.parseInt(request.getParameter("operacion"));
			EIGlobal.mensajePorTrace(TEXTOLOG + "Operación: " + operacion, EIGlobal.NivelLog.INFO);
			switch (operacion) {
		    case 1:
		    	iniciarConsulta(request, response);
		    	break;
		    case 2:
		    	consultarTarjetas(request, response);
		    	break;
		    case 3:
		    	desplazamientoCons(request, response);
		    	break;
			default:
				exportarDatosTarjetas(request, response);
				break;
		    }
		} else {
			request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(request, response);
		}
	}

	/**
	 * Metodo que recibe las peticiones post
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Inicia la pantalla de consulta de tarjetas
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void iniciarConsulta(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		request.getSession().removeAttribute("tarjetasRes");
		request.getSession().removeAttribute("valida");
		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESECTV", EIGlobal.NivelLog.INFO);
		request.setAttribute("anio", ObtenAnio());
		request.setAttribute("mes", ObtenMes());
		request.setAttribute("dia", ObtenDia());
		request.setAttribute ("Fecha",ObtenFecha ());
		evalTemplate("/jsp/filtroConsultaTarjeta.jsp", request, response );
	}

	/**
	 * Paginacion de la pantalla
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void desplazamientoCons(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String start1;
		start1 = (String) request.getParameter(START);
		if(start1 != null && !"".equals(start1)){
			Integer startInt;
			startInt = Integer.valueOf(request.getParameter (START));
			request.setAttribute (START, startInt);
			request.setAttribute("anio", ObtenAnio());
			request.setAttribute("mes", ObtenMes());
			request.setAttribute("dia", ObtenDia());
			request.setAttribute ("Fecha",ObtenFecha ());
			request.getRequestDispatcher("/jsp/filtroConsultaTarjeta.jsp").forward(request, response);
			return;
		}
	}

	/**
	 * Consulta las tarjetas de acuerdo a los filtros capturados.
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void consultarTarjetas(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESECTV", EIGlobal.NivelLog.INFO);
		HttpSession sess;
		sess = request.getSession();
		final String tarjeta = request.getParameter("tarjeta");
		final String fechaInicio = request.getParameter("fecha1");
		final String fechaFin = request.getParameter("fecha2");
		final String estatus = request.getParameter("estatus");
		final String idLote = request.getParameter("loteCarga");

		FiltroConsultaBean filtros;
		filtros = new FiltroConsultaBean();
		filtros.setTarjeta(tarjeta);
		filtros.setFechaInicio(fechaInicio);
		filtros.setFechaFin(fechaFin);
		filtros.setEstatus(estatus);
		filtros.setIdLote(idLote);

		BaseResource session1;
		session1 = (BaseResource) sess.getAttribute("session");
		String noContrato;
		noContrato = session1.getContractNumber();
		
		filtros.setContrato(noContrato);
		
		EnlcTarjetaBO tarjetasBO;
		tarjetasBO = new EnlcTarjetaBOImpl();
		List<TarjetaContrato> tarjetasRes = null;
		try {
			tarjetasRes = tarjetasBO.consultarDatosTarjetas(filtros, request);
		} catch (DaoException e) {
			sess.setAttribute("MensajeLogin01", " cuadroDialogo(\"No hay registros asociados a su consulta.\", 1);");
		}
		if(tarjetasRes == null || tarjetasRes.isEmpty()){
			tarjetasRes = null;
			sess.setAttribute("MensajeLogin01", " cuadroDialogo(\"No hay registros asociados a su consulta.\", 1);");
		}
		request.setAttribute("anio", ObtenAnio());
		request.setAttribute("mes", ObtenMes());
		request.setAttribute("dia", ObtenDia());
		request.setAttribute ("Fecha",ObtenFecha ());
		request.setAttribute (START, Integer.valueOf("0") );
		request.getSession ().setAttribute ("despl", Global.NUM_REGISTROS_PAGINA);
		request.getSession ().setAttribute("tarjetasRes", tarjetasRes);
		evalTemplate("/jsp/filtroConsultaTarjeta.jsp", request, response );
	}

	/**
	 * Exporta la consulta a word
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void exportarDatosTarjetas(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESECTV", EIGlobal.NivelLog.INFO);
		response.setContentType("application/vnd.ms-word");
		HttpSession sess;
		sess = request.getSession();
		BaseResource session;
		session = (BaseResource) sess.getAttribute("session");
		String numCliente;
		numCliente = session.getUserID8();
		response.setHeader("Content-disposition", "attachment; filename=" + numCliente + "VincTar.doc");
		request.getRequestDispatcher("/jsp/VincTarjetasExp.jsp").include(request,response);
	}
}
