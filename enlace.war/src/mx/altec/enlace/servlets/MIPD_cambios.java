package mx.altec.enlace.servlets;

import java.io.*;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

//VSWF
/**
 * The Class MIPD_cambios.
 */
public class MIPD_cambios extends BaseServlet

{
	/** The Enlace global. */
    EIGlobal EnlaceGlobal;

	/** The strdest. */
    String strdest			     = "";

	/** The strorig. */
    String strorig			     = "";

	/** The tc_ventanilla_compra. */
    String tc_ventanilla_compra  = ""; //tipo de cambio en ventanilla a la compra,general para todos

	/** The tc_ventanilla_venta. */
    String tc_ventanilla_venta	 = ""; //tipo de cambio en ventanilla a la venta, general para todos


	/**
	 * Instantiates a new MIP d_cambios.
	 */
	public MIPD_cambios()

	{

		super();

	}


	/**
	 * Inits the.
	 *
	 * @param config the config
	 * @throws ServletException the servlet exception
	 */
	public void init (ServletConfig config) throws ServletException

	{

		super.init(config);

	}


	/**
	 * Service.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)

		throws ServletException, IOException

	{

		/* HMM - Verifica la Facultad de Acceso (FAC_OPERA_CAMREG) 18/08/2004 */
		boolean  facAccesoPant;

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a execute", EIGlobal.NivelLog.INFO);
		HttpSession ses = req.getSession();

		boolean sesionvalida = SesionValida(req,res);

		try{

			if (sesionvalida)
			{

				HttpSession sess = req.getSession();

		      	BaseResource session = (BaseResource) sess.getAttribute("session");

				/* HMM - Verifica la Facultad de Acceso (FAC_OPERA_CAMREG) 18/08/2004 */
		        facAccesoPant =	session.getFacultad(session.FAC_OPERA_CAMREG);
		        /* HMM - Verifica la Facultad de Acceso (FAC_OPERA_CAMREG) 18/08/2004 */
				EIGlobal.mensajePorTrace("*** HMM-El valor de la variable facAccesoPant es =" +facAccesoPant, EIGlobal.NivelLog.INFO);

				EnlaceGlobal = new EIGlobal(session,getServletContext(),this);
				session.setModuloConsultar(IEnlace.MCargo_cambio_pesos);

				if (facAccesoPant)
				{

					if( ses.getAttribute("MenuPrincipal")!=null ){

						ses.removeAttribute("MenuPrincipal");}

						ses.setAttribute("MenuPrincipal", session.getStrMenu());

					if( ses.getAttribute("newMenu")!=null ){

						ses.removeAttribute("newMenu");}

						ses.setAttribute("newMenu", session.getFuncionesDeMenu());

					if( ses.getAttribute("Encabezado")!=null ){

						ses.removeAttribute("Encabezado");}

						ses.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Cambios de divisas(Transferencias)",null,req));


					req.setAttribute("MenuPrincipal", session.getStrMenu());

					req.setAttribute("newMenu", session.getFuncionesDeMenu());

					req.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Cambios de divisas(Transferencias)",null,req));



					String funcion_llamar = req.getParameter("ventana");

					EIGlobal.mensajePorTrace("***MIPD_cambios funcion_llamar >" +funcion_llamar+ "<", EIGlobal.NivelLog.INFO);

					if (funcion_llamar.equals("0")){

						pantalla_inicial_cambios("", req, res);}

					else if (funcion_llamar.equals("1")){

						pantalla_datos_cambios(req, res);}

					else if (funcion_llamar.equals("2")){

						pantalla_tabla_cambios(req, res);}

					else if (funcion_llamar.equals("3")){

						pantalla_tabla_cotizacion_cambios(req, res);}

					else if (funcion_llamar.equals("4")){

						ejecucion_operaciones_cambios(req, res);}

				}
				else
				{
					String laDireccion = "document.location='SinFacultades'";
					req.setAttribute( "plantillaElegida", laDireccion);
					req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
				}
			}

			else if (sesionvalida)
			{

				despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Cambios de divisas(Transferencias)","Cambios de divisas(Transferencias)","s26170h",req, res );

			}

		} catch (Exception e) {

			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

		}

		return;
	}


/**
 * Obtener_tipo_cambio_ventanilla.
 *
 * @param req the req
 * @param res the res
 * @return true, if successful
 * @throws IOException Signals that an I/O exception has occurred.
 * @throws ServletException the servlet exception
 */
private boolean obtener_tipo_cambio_ventanilla(HttpServletRequest req, HttpServletResponse res)
throws IOException,ServletException {

	EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a Obtener_tipo_cambio_ventanilla()", EIGlobal.NivelLog.INFO);
	tc_ventanilla_compra = "";
	tc_ventanilla_venta =	"";
	boolean actual=true;
	TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();

	/* STFQRO INICIO TRANSACCION GPA0  */
	try {
		tc_ventanilla_compra = tipoCambio.getCambioSugerido("CPA","CHDO");
		tc_ventanilla_venta  = tipoCambio.getCambioSugerido("VTA","CHDO");
	} catch (Exception e){
		EIGlobal.mensajePorTrace ( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
		actual = false;
		e.printStackTrace();
	}

	EIGlobal.mensajePorTrace("***MIPD_cambios.class tipo_cambio_ventanilla() Compra= >>>"+tc_ventanilla_compra +"<<<", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("***MIPD_cambios.class tipo_cambio_ventanilla() Venta = >>>"+tc_ventanilla_venta +"<<<", EIGlobal.NivelLog.INFO);

	try {
		//Verificando si los datos son los esperados
		Float.parseFloat(tc_ventanilla_compra);
		Float.parseFloat(tc_ventanilla_venta);
	} catch (Exception e) {



		actual = false;
		EIGlobal.mensajePorTrace ( "Los datos obtenidos no son validos" , EIGlobal.NivelLog.ERROR);
	}

	return actual;
}


/**
 * Llamada_a_servicio_cotizacion.
 *
 * @param cta the cta
 * @param tipo the tipo
 * @param importetrans the importetrans
 * @param claveesp the claveesp
 * @param tcesp the tcesp
 * @param req the req
 * @return the string[]
 * @throws IOException Signals that an I/O exception has occurred.
 * @throws ServletException the servlet exception
 */
private String[] llamada_a_servicio_cotizacion(String cta, String tipo, String importetrans,
		String claveesp,String tcesp,HttpServletRequest req)throws IOException,ServletException{

	TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();
	ServicioTux tuxGlobal = new ServicioTux();
	Integer referencia = 0;
	String codError = "";
	String resCodError = "";
	String desResCodErr = "";
	String cambioVentanilla = "";
	String tipoCambioAplic = "";
	Hashtable hs = null;

		EIGlobal.mensajePorTrace("***llamada_a_servicio_cotizacion", EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***llamada_a_servicio_cotizacion cta		" + cta, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***llamada_a_servicio_cotizacion tipo 	" + tipo, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***llamada_a_servicio_cotizacion importetrans " + importetrans, EIGlobal.NivelLog.INFO);

	    EIGlobal.mensajePorTrace("***llamada_a_servicio_cotizacion claveesp "+claveesp+">", EIGlobal.NivelLog.INFO);

	    EIGlobal.mensajePorTrace("***llamada_a_servicio_cotizacion tcesp"+tcesp+">", EIGlobal.NivelLog.INFO);



	String tc_cot_compra  = "0";

	String tc_cot_venta   = "0";

	String tc_cot_aplicar = "0";

	String tc_ventanilla_aplicar = "0";

	String importemn   = "0";

	String importeusd  = "0";

	String capUsdEsp = "0";

		String arr[]	   ;



	arr=new String[6];



		HttpSession sess = req.getSession();

	BaseResource session = (BaseResource) sess.getAttribute("session");



		String coderror = "";

		String tipo_operacion = "";

		String buffer = "";

		String[] Cotizacion;



	   if (claveesp.length()==0){

		   tcesp="";}



		if (tipo.equals("6")){

			tipo_operacion = "CPA";}

		else{

			tipo_operacion = "VTA";}

		try{
			//Llamado al SREFERENCIA
			try {
				hs = tuxGlobal.sreferencia("901");;
				codError = hs.get("COD_ERROR").toString();
				EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",	EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hs.toString(), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				codError = null;
				referencia = 0;
			}
			// VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
			if (codError.endsWith("0000")) {
				referencia = (Integer) hs.get("REFERENCIA");
				// SE LLAMA GPA0 para obtener contizaci�n
				EIGlobal.mensajePorTrace("CUENTA CARGO->" + cta, EIGlobal.NivelLog.DEBUG);
				cambioVentanilla = tipoCambio.getCambioSugerido(cta, tipo_operacion,"USD","","CHDO");
				codError = "DICO0000";
				resCodError = "OK";
			}else{
				//CASO CONTRARIO REFERENCIA INCORRECTA
				referencia = 0;
				codError = "DICO9999";
				resCodError = "ERROR";
				desResCodErr = "PROBLEMAS AL OBTENER LA REFERENCIA";
			}
			EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: cambioVentanilla = " + cambioVentanilla, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: codError = " + codError, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: resCodError = " + resCodError, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: cambioVentanilla = " + desResCodErr, EIGlobal.NivelLog.DEBUG);

			if (tipo.equals("6"))

			{

			   tc_cot_compra = cambioVentanilla;

			   tc_cot_aplicar = tc_cot_compra;

			   tc_ventanilla_aplicar = tipoCambio.getCambioVentanilla(cta, tipo_operacion,"USD","","CHDO");

			}

			else

			{

				tc_cot_venta = cambioVentanilla;

				tc_cot_aplicar = tc_cot_venta;

				tc_ventanilla_aplicar = tipoCambio.getCambioVentanilla(cta, tipo_operacion,"USD","","CHDO");

			}

			//ELECCION DEL TIPO DE CAMBIO ADECUADO
			if(claveesp.length()==0){
				tipoCambioAplic = cambioVentanilla;//SUGERIDO
			}else{
				tipoCambioAplic = tcesp;//TIPO ESPECIAL
			}
			EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: tipoCambioAplic->"+tipoCambioAplic, EIGlobal.NivelLog.DEBUG);

			try{
				//GENERANDO LA COTIZACI�N
				capUsdEsp = (String)sess.getAttribute("capUsdEsp");
				EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: CAPTURA capUsdEsp>> Dolares(1) o Pesos(0) ->"+capUsdEsp, EIGlobal.NivelLog.DEBUG);
				if(capUsdEsp.trim().equals("1")){
					importeusd = (String)sess.getAttribute("importeUSD");
					EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: divisa USD->"+importeusd, EIGlobal.NivelLog.DEBUG);
					importemn  = String.valueOf(redondear((Double.parseDouble(importeusd) * Double.parseDouble(tipoCambioAplic)), 2));
					EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: calculo divisa MXP->"+importemn, EIGlobal.NivelLog.DEBUG);
				}else if(capUsdEsp.trim().equals("0")){
					importemn = (String)sess.getAttribute("importeMXP");
					EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: divisa MXP->"+importemn, EIGlobal.NivelLog.DEBUG);
					importeusd  = String.valueOf(redondear((Double.parseDouble(importemn) / Double.parseDouble(tipoCambioAplic)), 2));
					EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: calculo divisa USD->"+importeusd, EIGlobal.NivelLog.DEBUG);
				}

				EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: importeusd->"+importeusd, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: importemn->"+importemn, EIGlobal.NivelLog.DEBUG);
			}catch(Exception e){
				EIGlobal.mensajePorTrace("MIPD_cambios::llamada_a_servicio_cotizacion:: No se hacen Calculos... ", EIGlobal.NivelLog.DEBUG);
				importemn  = "0";
				importeusd = "0";
			}

	if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
			BitaTransacBean bt = new BitaTransacBean();
			BitaTCTBean beanTCT = new BitaTCTBean ();

			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_CAMBIOS)){
				BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);

				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ET_CAMBIOS_COTIZACION_INTER);
				if(resCodError != null){
					if(resCodError.equals("OK")){
						bt.setIdErr("DICO0000");
					}else{
						bt.setIdErr("DICO9999");
					}
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if (tc_ventanilla_aplicar != null) {
					bt.setTipoCambio(Double.parseDouble(tc_ventanilla_aplicar));
				}
				if (importeusd != null) {
					bt.setImporte(Double.parseDouble(importeusd));
				}
				bt.setServTransTux("DICO");

				BitaHandler.getInstance().insertBitaTransac(bt);

			}

			if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
				BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);

				bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				if(tipo_operacion.equals("CPA"))
					bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_OPER_REDSRV_CPA);
				else
					bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_OPER_REDSRV_VTA);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());

				}
				if (tc_ventanilla_aplicar != null) {
					bt.setTipoCambio(Double.parseDouble(tc_ventanilla_aplicar));
				}
				if (importeusd != null) {
					bt.setImporte(Double.parseDouble(importeusd));
				}
				bt.setServTransTux("DICO");

				if(resCodError!=null){
	    			if(resCodError.equals("OK")){
	    				bt.setIdErr("DICO0000");
	    			}else {
		    			bt.setIdErr("DICO9999");
		    		}
	    		}

				if (cta != null) {
					if(cta.length() > 20){
						bt.setCctaOrig(cta.substring(0,20));
					}else{
						bt.setCctaOrig(cta.trim());
					}
				}

				BitaHandler.getInstance().insertBitaTransac(bt);
				//INI BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
				beanTCT = bh.llenarBeanTCT(beanTCT);
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion("DICO");
				beanTCT.setCodError(codError);
				beanTCT.setUsuario(session.getUserID8());
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);
				//FIN BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390

			}
		}catch(SQLException e){
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		 }
		/**
		 * VSWF-***-F
		 */

	    arr[0]=tc_cot_compra;

	    arr[1]=tc_cot_venta;

	    //Valida si existe un tipo de cambio especial
	    if(claveesp.length()==0)
	    	arr[2]=tc_cot_aplicar;
	    else
	    	arr[2]= tcesp;

	    arr[3]=tc_ventanilla_aplicar;

	    arr[4]=importemn;
	    arr[5]=importeusd;

			EIGlobal.mensajePorTrace(
					"MIPD_cambios - llamada_a_servicio_cotizacion:: RESULTADO : "
							+ arr.toString(), EIGlobal.NivelLog.DEBUG);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(
					"MIPD_cambios - llamada_a_servicio_cotizacion:: "
							+ e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}


	return arr;

	}


	/**
	 * Pantalla_inicial_cambios.
	 *
	 * @param Mensaje the mensaje
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	private int pantalla_inicial_cambios(String Mensaje, HttpServletRequest req, HttpServletResponse res)

		throws IOException,ServletException

	{

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a pantalla_inicial_cambios()", EIGlobal.NivelLog.INFO);



		String strSalida = "";

		String claveProducto = "";

		String mensaje_actualizacion="";



		HttpSession sess = req.getSession();

	BaseResource session = (BaseResource) sess.getAttribute("session");

	/**
	 * VSWF
	 * 15/Enero/2007	Modificaci�n para el modulo de Tesorer�a
	 */
	 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
	try{
		BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
		BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);
		
		String flujo = req.getParameter(BitaConstants.FLUJO);
		if((flujo==null)||(flujo.equals(""))){
			EIGlobal.mensajePorTrace("El valor NO EXISTE se agregara el por DEFAULT que es ETCA"+flujo, EIGlobal.NivelLog.INFO); 
			flujo = BitaConstants.ET_CAMBIOS;
		}
		
		EIGlobal.mensajePorTrace("El valor del flujo es: "+flujo+" y el id de objeto de bh es:"+bh,EIGlobal.NivelLog.INFO );
		bh.incrementaFolioFlujo(flujo);
		EIGlobal.mensajePorTrace("El valor SESS_ID_FLUJO en sesion es : "+req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)+" y el id de objeto de bh es:"+bh,EIGlobal.NivelLog.INFO );
		BitaTransacBean bt = new BitaTransacBean();
		EIGlobal.mensajePorTrace("Antes de invocar el llenar bean:  "+bt+" ---bh: "+bh, EIGlobal.NivelLog.INFO);
		bt = (BitaTransacBean)bh.llenarBean(bt);
		
		EIGlobal.mensajePorTrace("Antes de invocar el llenar bean:  "+bt+" ---bh: "+bh, EIGlobal.NivelLog.INFO);
		
		if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_CAMBIOS)){
			bt.setNumBit(BitaConstants.ET_CAMBIOS_ENTRA);
		}else{
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
				bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_ENTRA);
			}
		}

		if (session.getContractNumber() != null) {
			bt.setContrato(session.getContractNumber());
		}

		BitaHandler.getInstance().insertBitaTransac(bt);
	}catch(SQLException e){
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	}catch(Exception e){
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	}
	 }
	/**
	 * VSWF
	 */

		if  (obtener_tipo_cambio_ventanilla(req, res)==false)

	  despliegaPaginaError("No existe cotizaci&oacute;n para el d&oacute;lar en estos momentos ","Cambios de divisas(Transferencias)","", req, res);

		else

		{

			if(sess.getAttribute("MenuPrincipal")!=null){

				sess.removeAttribute("MenuPrincipal");}

			sess.setAttribute("MenuPrincipal", session.getStrMenu());

			if(sess.getAttribute("newMenu")!=null){

				sess.removeAttribute("newMenu");}

			sess.setAttribute("newMenu", session.getFuncionesDeMenu());

			if(sess.getAttribute("Encabezado")!=null){

				sess.removeAttribute("Encabezado");}

			sess.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s27500h",req));

			if(sess.getAttribute("tcv_compra")!=null){

				sess.removeAttribute("tcv_compra");}

			sess.setAttribute("tcv_compra", ""+tc_ventanilla_compra.trim());

			if(sess.getAttribute("tcv_venta")!=null){

				sess.removeAttribute("tcv_venta");}

			sess.setAttribute("tcv_venta", ""+tc_ventanilla_venta.trim());

			if(sess.getAttribute("TiempoTerminado")!=null){

				sess.removeAttribute("TiempoTerminado");}

			if(sess.getAttribute("importeUSD")!=null){
				sess.removeAttribute("importeUSD");}

			if(sess.getAttribute("importeMXP")!=null){
				sess.removeAttribute("importeMXP");}

			if(sess.getAttribute("capUsdEsp")!=null){
				sess.removeAttribute("capUsdEsp");}



		sess.setAttribute("TiempoTerminado",Mensaje);

			req.setAttribute("MenuPrincipal", session.getStrMenu());

			req.setAttribute("newMenu", session.getFuncionesDeMenu());

			req.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s27500h",req));

			req.setAttribute("tcv_compra", ""+tc_ventanilla_compra.trim());

			req.setAttribute("tcv_venta", ""+tc_ventanilla_venta.trim());

		req.setAttribute("TiempoTerminado",Mensaje);

			req.setAttribute("Mensaje_actualizacion",mensaje_actualizacion);



			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(IEnlace.CAMBIOS_INICIAL_TMPL);

			dispatcher.forward(req, res);

		}

		return 1;

	}


	/**
     * Pantalla_datos_cambios.
     *
     * @param req the req
     * @param res the res
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    private int pantalla_datos_cambios(HttpServletRequest req, HttpServletResponse res)

		throws IOException,ServletException

	{

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a pantalla_datos_cambios()", EIGlobal.NivelLog.INFO);



		String cuentasabono = "";

		String cuentascargo = "";

		String strSalida2	 = "";

		String NoRegistradas = "";

		String tramacta      = "";

		String importetrans  = "";

		String cve_especial  = "";

		String claveProducto = "";

		boolean  facCtasNoReg;

		String[] Cta=null;

		String ctaselec="";

	String desctaselec="";

	    String tc_cot_compra  = "";

	    String tc_cot_venta   = "";

	    String tc_cot_aplicar = "";

	    String tc_ventanilla_aplicar = "";

	    String importemn   = "";

	    String importeusd  = "";

		String arr[]	   ;

		boolean cuentaOperable = false;
		String trama = "";
		String resultado = "";
		TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();



		HttpSession sess = req.getSession();

 	    BaseResource session = (BaseResource) sess.getAttribute("session");


		facCtasNoReg =	session.getFacultad(session.FAC_OPERA_CAMNOREG);

		NoRegistradas = cuentasNoRegistradas("Pesos", facCtasNoReg);

	    tramacta     = req.getParameter("origen");

		importetrans = req.getParameter("monto");



		 EIGlobal.mensajePorTrace("***MIPD_cambios.class tcv_venta  >" +tc_ventanilla_venta+ "<", EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("***MIPD_cambios.class tcv_compra >" +tc_ventanilla_compra+ "<", EIGlobal.NivelLog.INFO);

		    EIGlobal.mensajePorTrace("***MIPD_cambios.class origen     >" +tramacta+ "<", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class monto	   >" +importetrans+ "<", EIGlobal.NivelLog.INFO);


	    if ((tramacta!=null)&&(importetrans!=null))

	    {

				Cta = desentramaC("4|"+tramacta,'|');

			    ctaselec=Cta[1];

				desctaselec=Cta[3];

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Antes de  llamada_a_servicio_cotizacion()", EIGlobal.NivelLog.INFO);

			//SE VERIFICA QUE LA CUENTA SEA OPERABLE
			EIGlobal.mensajePorTrace("VERIFICANDO CUENTA CARGO->"+Cta[1],EIGlobal.NivelLog.DEBUG);
			trama  = tipoCambio.verificaCuenta(Cta[1], (Cta[4].equals("6")?"CPA":"VTA"),"USD","","CHDO");
			if(trama.substring(0, 2).trim().equals("@2")){
				resultado = formatoMensaje(trama.substring(18));//SACANDO EL ERROR
				EIGlobal.mensajePorTrace("MIPD_cambios::verificaCuenta:: "+resultado,EIGlobal.NivelLog.DEBUG);
			}else{
				cuentaOperable = true;
				EIGlobal.mensajePorTrace("MIPD_cambios::verificaCuenta:: La cuenta es operable",EIGlobal.NivelLog.DEBUG);
			}

				arr=llamada_a_servicio_cotizacion(Cta[1], Cta[4], importetrans,"","",req);



		    tc_cot_compra=arr[0];

			tc_cot_venta=arr[1];

		    tc_cot_aplicar=arr[2];

		    tc_ventanilla_aplicar=arr[3];

		    importemn=arr[4];

		    importeusd=arr[5];

	    }



		 if(sess.getAttribute("NoRegistradas")!=null){

			sess.removeAttribute("NoRegistradas");}

			sess.setAttribute("NoRegistradas", NoRegistradas);

		 if(sess.getAttribute("arregloctas_origen1")!=null){

			sess.removeAttribute("arregloctas_origen1");}

			sess.setAttribute("arregloctas_origen1","");

		 if(sess.getAttribute("arregloctas_destino1")!=null){

			sess.removeAttribute("arregloctas_destino1");}

			sess.setAttribute("arregloctas_destino1","");

		 if(sess.getAttribute("arregloimportes1")!=null){

			sess.removeAttribute("arregloimportes1");}

			sess.setAttribute("arregloimportes1","");

		 if(sess.getAttribute("arregloconceptos1")!=null){

			sess.removeAttribute("arregloconceptos1");}

			sess.setAttribute("arregloconceptos1","");

		 if(sess.getAttribute("arregloestatus1")!=null){

			sess.removeAttribute("arregloestatus1");}

			sess.setAttribute("arregloestatus1","");

		 if(sess.getAttribute("arreglorfcs1")!=null){

			sess.removeAttribute("arreglorfcs1");}

			sess.setAttribute("arreglorfcs1","");

		 if(sess.getAttribute("arreglorivas1")!=null){

			sess.removeAttribute("arreglorivas1");}

			sess.setAttribute("arreglorivas1","");

		 if(sess.getAttribute("arregloctas_origen2")!=null){

			sess.removeAttribute("arregloctas_origen2");}

			sess.setAttribute("arregloctas_origen2","");

		 if(sess.getAttribute("arregloctas_destino2")!=null){

			sess.removeAttribute("arregloctas_destino2");}

			sess.setAttribute("arregloctas_destino2","");

		 if(sess.getAttribute("arregloimportes2")!=null){

			sess.removeAttribute("arregloimportes2");}

			sess.setAttribute("arregloimportes2","");

		 if(sess.getAttribute("arregloconceptos2")!=null){

			sess.removeAttribute("arregloconceptos2");}

		    sess.setAttribute("arregloconceptos2","");

		 if(sess.getAttribute("arregloestatus2")!=null){

			sess.removeAttribute("arregloestatus2");}

			sess.setAttribute("arregloestatus2","");

		 if(sess.getAttribute("arreglorfcs2")!=null){

			sess.removeAttribute("arreglorfcs2");}

			sess.setAttribute("arreglorfcs2","");

		 if(sess.getAttribute("arreglorivas2")!=null){

			sess.removeAttribute("arreglorivas2");}

			sess.setAttribute("arreglorivas2","");

		 if(sess.getAttribute("contador1")!=null){

			sess.removeAttribute("contador1");}

			sess.setAttribute("contador1","0");

		 if(sess.getAttribute("contador2")!=null){

			sess.removeAttribute("contador2");}

			sess.setAttribute("contador2","0");

		 if(sess.getAttribute("tabla")!=null){

			sess.removeAttribute("tabla");}

			sess.setAttribute("tabla","");

		 if(sess.getAttribute("TIPOTRANS")!=null){

			sess.removeAttribute("TIPOTRANS");}

			sess.setAttribute("TIPOTRANS","LINEA");

		 if(sess.getAttribute("INICIAL")!=null){

			sess.removeAttribute("INICIAL");}

			sess.setAttribute("INICIAL","SI");

		 if(sess.getAttribute("IMPORTACIONES")!=null){

			sess.removeAttribute("IMPORTACIONES");}

			sess.setAttribute("IMPORTACIONES","0");

		 if(sess.getAttribute("NMAXOPER")!=null){

			sess.removeAttribute("NMAXOPER");}

			sess.setAttribute("NMAXOPER"," "+Global.MAX_REGISTROS);

		 if(sess.getAttribute("trans")!=null){

			sess.removeAttribute("trans");}

			sess.setAttribute("trans","0");

		 if(sess.getAttribute("MenuPrincipal")!=null){

			sess.removeAttribute("MenuPrincipal");}

			sess.setAttribute("MenuPrincipal", session.getStrMenu());

		 if(sess.getAttribute("newMenu")!=null){

			sess.removeAttribute("newMenu");}

	    sess.setAttribute("newMenu", session.getFuncionesDeMenu());

		 if(sess.getAttribute("Encabezado")!=null){

			sess.removeAttribute("Encabezado");}

	    sess.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s27510h",req));



			req.setAttribute("NoRegistradas", NoRegistradas);

			req.setAttribute("arregloctas_origen1","");

			req.setAttribute("arregloctas_destino1","");

			req.setAttribute("arregloimportes1","");

			req.setAttribute("arregloconceptos1","");

			req.setAttribute("arregloestatus1","");

			req.setAttribute("arreglorfcs1","");

			req.setAttribute("arregloivas1","");

			req.setAttribute("arregloctas_origen2","");

			req.setAttribute("arregloctas_destino2","");

			req.setAttribute("arregloimportes2","");

		    req.setAttribute("arregloconceptos2","");

			req.setAttribute("arregloestatus2","");

			req.setAttribute("arreglorfcs2","");

			req.setAttribute("arreglorivas2","");

			req.setAttribute("arregloclesp1","");

		    req.setAttribute("arregloclesp2","");

		    req.setAttribute("arreglotipcambesp1","");

		    req.setAttribute("arreglotipcambesp2","");

			req.setAttribute("contador1","0");

			req.setAttribute("contador2","0");

			req.setAttribute("tabla","");

			req.setAttribute("TIPOTRANS","LINEA");

			req.setAttribute("INICIAL","SI");

			req.setAttribute("IMPORTACIONES","0");

			req.setAttribute("NMAXOPER"," "+Global.MAX_REGISTROS);

			req.setAttribute("trans","0");

			req.setAttribute("MenuPrincipal", session.getStrMenu());

	    req.setAttribute("newMenu", session.getFuncionesDeMenu());

	    req.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s27510h",req));



		    EIGlobal.mensajePorTrace("***MIPD_cambios.class tcc_compra	 >" +tc_cot_compra+ "<", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class tcc_venta    >" +tc_cot_venta+ "<", EIGlobal.NivelLog.INFO);

		    EIGlobal.mensajePorTrace("***MIPD_cambios.class tcv_compra	 >" +tc_ventanilla_compra+ "<", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class tcv_venta    >" +tc_ventanilla_venta+ "<", EIGlobal.NivelLog.INFO);

		    EIGlobal.mensajePorTrace("***MIPD_cambios.class tcc_aplicar  >" +tc_cot_aplicar+ "<", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class tcv_aplicar  >" +tc_ventanilla_aplicar+ "<", EIGlobal.NivelLog.INFO);

		    EIGlobal.mensajePorTrace("***MIPD_cambios.class tramacta	 >" +tramacta+ "<", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class importetrans >" +importetrans+ "<", EIGlobal.NivelLog.INFO);



			if(sess.getAttribute("tcc_compra1")!=null){

				sess.removeAttribute("tcc_compra1");}

				sess.setAttribute("tcc_compra1", ""+tc_cot_compra);

			if(sess.getAttribute("tcc_venta1")!=null){

				sess.removeAttribute("tcc_venta1");}

				sess.setAttribute("tcc_venta1", ""+tc_cot_venta);

			if(sess.getAttribute("tcv_compra1")!=null){

				sess.removeAttribute("tcv_compra1");}

				sess.setAttribute("tcv_compra1", ""+tc_ventanilla_compra);

			if(sess.getAttribute("tcv_venta1")!=null){

				sess.removeAttribute("tcv_venta1");}

				sess.setAttribute("tcv_venta1", ""+tc_ventanilla_venta);

			if(sess.getAttribute("tcc_aplicar1")!=null){

				sess.removeAttribute("tcc_aplicar1");}

				sess.setAttribute("tcc_aplicar1", ""+tc_cot_aplicar);

			if(sess.getAttribute("tcv_aplicar1")!=null){

				sess.removeAttribute("tcv_aplicar1");}

				sess.setAttribute("tcv_aplicar1", ""+tc_ventanilla_aplicar);

			if(sess.getAttribute("ctasel")!=null){

				sess.removeAttribute("ctasel");}

				sess.setAttribute("ctasel","\""+tramacta+"\"");

			if(sess.getAttribute("ctasel2")!=null){

				sess.removeAttribute("ctasel2");}

				sess.setAttribute("ctasel2","\""+tramacta+"\"");

			if(sess.getAttribute("impsel")!=null){

				sess.removeAttribute("impsel");}

			sess.setAttribute("impsel",importetrans);

			if(sess.getAttribute("impsel2")!=null){

				sess.removeAttribute("impsel2");}

			sess.setAttribute("impsel2",importetrans);

			if(sess.getAttribute("cve_esp2")!=null){

				sess.removeAttribute("cve_esp2");}

				sess.setAttribute("cve_esp2",cve_especial);

			if(sess.getAttribute("tcv_venta2")!=null){

				sess.removeAttribute("tcv_venta2");}

				sess.setAttribute("tcv_venta2",tc_ventanilla_venta);

			if(sess.getAttribute("tcv_compra2")!=null){

				sess.removeAttribute("tcv_compra2");}

				sess.setAttribute("tcv_compra2",tc_ventanilla_compra);



			req.setAttribute("tcc_compra1", ""+tc_cot_compra);

	    req.setAttribute("tcc_venta1", ""+tc_cot_venta);

			req.setAttribute("tcv_compra1", ""+tc_ventanilla_compra);

	    req.setAttribute("tcv_venta1", ""+tc_ventanilla_venta);

			req.setAttribute("tcc_aplicar1", ""+tc_cot_aplicar);

			req.setAttribute("tcv_aplicar1", ""+tc_ventanilla_aplicar);

			req.setAttribute("ctasel","\""+tramacta+"\"");

			req.setAttribute("ctasel2","\""+tramacta+"\"");

			req.setAttribute("textctasel","\""+ctaselec+" "+desctaselec+"\"");

		req.setAttribute("impsel",importetrans);

		req.setAttribute("impsel2",importetrans);

	    req.setAttribute("tcv_venta2", ""+tc_ventanilla_venta);

			req.setAttribute("tcv_compra2", ""+tc_ventanilla_compra);

			req.setAttribute("arregloclesp1","");

	    req.setAttribute("arreglotipcambesp1","");

			req.setAttribute("arregloclesp2","");

	    req.setAttribute("arreglotipcambesp2","");

	    if(cuentaOperable)
	    	evalTemplate(IEnlace.CAMBIOS_DATOS_TMPL, req, res);
	    else
	    	despliegaPaginaError(resultado.replace('?', ' '),"Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s25360h",req,res);

		return 1;

	}


	/**
	 * Pantalla_tabla_cambios.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	private int pantalla_tabla_cambios(HttpServletRequest req, HttpServletResponse res)

		throws IOException,ServletException

    {

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a pantalla_tabla_cambios ", EIGlobal.NivelLog.INFO);



		String tramactacargo = "";

		String tramactaabono = "";

		String ctacargo = "";

		String ctaabono = "";

		String importedivisa = "";

		String concepto = "";

		String iva = "";

		String rfc = "";

		String clesp="";

		String tcesp="";

		String checkboxrfc = "";

		String arrctascargo;

		String arrctasabono;

		String arrimportes;

		String arrconceptos;

		String arrrfcs;

		String arrivas;

		String arrclesp;

		String arrtcesp;

		String arrestatus;

		String scontador = "";

		String strArchivo = "";

		String tabla = "";

		String tipo_cambio_aplicar = "";

		boolean ctanoregvalida = true;

		String Cta[]=null;

		String inicial="NO";

		String simporteMN="";

		int contador = 0;

		boolean archivo_sin_error = true;

		double importetabla = 0.0;

		int cont = 0;

		String claveProducto = "";

		String cuentascargo = "";

		String cuentasabono = "";

		String desctanoreg = "";

	    String botontransferir = "";

		boolean facCtasNoReg=false;

		boolean chkseg = true;

		int numero_cuentas_trans_car = 0;

		int numero_cuentas_trans_abo = 0;



		HttpSession sess = req.getSession();

	BaseResource session = (BaseResource) sess.getAttribute("session");



	String chkvalor = req.getParameter ("ChkOnline");

	String capUsdEsp = req.getParameter("capUsdEsp");



	arrctascargo = req.getParameter ("arregloctas_origen1");

	    arrctasabono = req.getParameter ("arregloctas_destino1");

	arrimportes  = req.getParameter ("arregloimportes1");

		arrconceptos = req.getParameter ("arregloconceptos1");

	    arrestatus = req.getParameter ("arregloestatus1");

		arrrfcs    = req.getParameter ("arreglorfcs1");

		arrivas    = req.getParameter ("arregloivas1");

		arrclesp	= req.getParameter ("arregloclesp1");

	arrtcesp	= req.getParameter ("arreglotipcambesp1");

		scontador  = req.getParameter ("contador1");

		contador = Integer.parseInt(scontador);



		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloctas_origen1: " + arrctascargo, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloctas_destino1: " + arrctasabono, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloimportes1: " + arrimportes, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloconceptos1: " + arrconceptos, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloestatus1: " + arrestatus, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglorfcs1: " + arrrfcs, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloivas1: " + arrivas, EIGlobal.NivelLog.DEBUG);	//en null

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  contador1: " + scontador, EIGlobal.NivelLog.DEBUG);

facCtasNoReg =	session.getFacultad(session.FAC_OPERA_CAMNOREG);


		if (!chkvalor.equals("OnFile"))

	{

			tramactacargo = req.getParameter ("origen");



			if(chkvalor.trim().equals("OnNoReg"))

			{

				tramactaabono  = req.getParameter ("destinoNoReg");

				ctanoregvalida = ValidaCuentaNoReg(tramactaabono, desctanoreg,req);

				if (ctanoregvalida==true)

				{

			   	ctanoregvalida=validaBancocuenta(tramactaabono,req);



				   if (ctanoregvalida==true)

				   {

					   if ((tramactaabono.substring(0,2).equals("82"))||

						   (tramactaabono.substring(0,2).equals("83"))||

						  ((tramactaabono.substring(0,2).equals("49"))&&(tramactaabono.length()==11)))

						    tramactaabono+="|NR|"+desctanoreg+"|6|";

					    else

						    tramactaabono+="|NR|"+desctanoreg+"|1|";

				   }

				}

			}

			else

				tramactaabono = req.getParameter ("destino");

				concepto = req.getParameter ("concepto");

				concepto=concepto.toUpperCase();

				Cta = desentramaC("4|"+tramactacargo,'|');

				importedivisa=req.getParameter("montoUSD");

				simporteMN=req.getParameter("montoMN");

				sess.setAttribute("importeUSD", importedivisa);
				sess.setAttribute("importeMXP", simporteMN);
				sess.setAttribute("capUsdEsp", capUsdEsp);
				EIGlobal.mensajePorTrace("***importeUSD->" + sess.getAttribute("importeUSD"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("***importeMXP->" + sess.getAttribute("importeMXP"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("***MIPD_cambios.class  CAPTURA>> Dolares(1) o Pesos(0) " +capUsdEsp, EIGlobal.NivelLog.INFO);



				if (req.getParameter("CHRCF").equals("1"))

				{

					iva=req.getParameter("IVA");

					rfc=req.getParameter("RFC");

				}

				else

				{

					iva="";

					rfc="";

				}



			   clesp=req.getParameter("CLAVEESP");



		       clesp=clesp.trim();

	       EIGlobal.mensajePorTrace("***MIPD_cambios.class	CLAVEESP: " + clesp, EIGlobal.NivelLog.INFO);

		       if(clesp.length()>0)

			   {



				   tcesp= req.getParameter("TIPOCAMBESP");

		   EIGlobal.mensajePorTrace("***MIPD_cambios.class  TIPOCAMBESP: " +tcesp, EIGlobal.NivelLog.INFO);

		   double dimportedivisa;

			   double dtcesp=new Double(tcesp).doubleValue();

				   double importeMN;


		   if(capUsdEsp.trim().equals("1")){
			   dimportedivisa=new Double(importedivisa).doubleValue();
			   importeMN=dimportedivisa*dtcesp;
			   simporteMN=new Double(redondear(importeMN, 2)).toString();
		   }else if(capUsdEsp.trim().equals("0")){
			   importeMN=new Double(simporteMN).doubleValue();
			   dimportedivisa=importeMN/dtcesp;
			   importedivisa=new Double(redondear(dimportedivisa,2)).toString();
		   }
		   EIGlobal.mensajePorTrace("***MIPD_cambios.class  IMPORTE DOLLAR ESP: " +importedivisa, EIGlobal.NivelLog.INFO);
		   EIGlobal.mensajePorTrace("***MIPD_cambios.class  IMPORTE PESOS ESP: " +simporteMN, EIGlobal.NivelLog.INFO);

	       }



			if (ctanoregvalida==true)

			{

				arrctascargo +=tramactacargo+"@";

				arrctasabono +=tramactaabono+"@";

				arrimportes  +=importedivisa+"@";

				arrconceptos +=concepto+"@";

				arrestatus   +="1@";

				arrivas      +=iva+"@";

				arrrfcs      +=rfc+"@";

				arrclesp	 +=clesp+"@";

				arrtcesp	 +=tcesp+"@";

				contador++;

				EIGlobal.mensajePorTrace("***MIPD_cambios.class  arrimportes1 = >" +arrimportes+ "<", EIGlobal.NivelLog.INFO);

			}

			else

			{

				req.setAttribute("ArchivoErr","cuadroDialogo('Cuenta no registrada inv&aacute;lida', 3);");

			inicial="SI";

			}

		}





	req.setAttribute("NoRegistradas",cuentasNoRegistradas("Pesos", facCtasNoReg));



		req.setAttribute("ctasel","\""+tramactacargo+"\"");

		req.setAttribute("ctasel2","\""+tramactacargo+"\"");

		req.setAttribute("textctasel","\""+Cta[1]+" "+Cta[3]+"\"");

		req.setAttribute("arregloctas_origen1","\""+arrctascargo+"\"");

		req.setAttribute("arregloctas_destino1","\""+arrctasabono+"\"");

		req.setAttribute("arregloctas_origen2","\""+arrctascargo+"\"");

		req.setAttribute("arregloctas_destino2","\""+arrctasabono+"\"");

		req.setAttribute("arregloclesp1","\""+arrclesp+"\"");

		req.setAttribute("arregloclesp2","\""+arrclesp+"\"");

		req.setAttribute("arreglotipcambesp1","\""+arrtcesp+"\"");

		req.setAttribute("arreglotipcambesp2","\""+arrtcesp+"\"");



	req.setAttribute("NoRegistradas",cuentasNoRegistradas("Pesos", facCtasNoReg));

	EIGlobal.mensajePorTrace("*** TRAMA CUENTA CARGO =	" +tramactacargo, EIGlobal.NivelLog.INFO);

    strorig=arrctascargo; //cuentas origen

    EIGlobal.mensajePorTrace("IMPRESI�N DE LA CUENTA DE CARGO QUE SE RESPALDARA->" +arrctascargo, EIGlobal.NivelLog.DEBUG);

	strdest=arrctasabono; //cuentas destino

	EIGlobal.mensajePorTrace("IMPRESI�N DE LA CUENTA DE ABONO QUE SE RESPALDARA->" +arrctasabono, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  NoRegistradas =	" +cuentasNoRegistradas("Pesos", facCtasNoReg)+ "<", EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloctas_origen1 =	" +arrctascargo+ "<", EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloctas_destino1 = " +arrctasabono+ "<", EIGlobal.NivelLog.DEBUG);



		req.setAttribute("arregloimportes1",arrimportes);

		req.setAttribute("arregloimportes2",arrimportes);

    	req.setAttribute("arregloconceptos1","\""+arrconceptos+"\"");

		req.setAttribute("arregloconceptos2","\""+arrconceptos+"\"");

		req.setAttribute("arregloivas1","\""+arrivas+"\"");

		req.setAttribute("arregloivas2","\""+arrivas+"\"");

		req.setAttribute("arreglorfcs1","\""+arrrfcs+"\"");

		req.setAttribute("arreglorfcs2","\""+arrrfcs+"\"");

		req.setAttribute("arregloestatus1",arrestatus);

		req.setAttribute("arregloestatus2",arrestatus);



	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloimportes1:  "+arrimportes, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloconceptos1: "+arrconceptos, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloivas1:	    "+arrivas, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglorfcs1:	    "+arrrfcs, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arregloestatus1:   "+arrestatus, EIGlobal.NivelLog.DEBUG);



		req.setAttribute("contador1",Integer.toString(contador));

		req.setAttribute("contador2",Integer.toString(contador));

		req.setAttribute("INICIAL",inicial);

		req.setAttribute("TIPOTRANS", req.getParameter("TIPOTRANS"));

		req.setAttribute("IMPORTACIONES","1");

		req.setAttribute("NMAXOPER"," "+Global.MAX_REGISTROS);

		req.setAttribute("trans",req.getParameter("trans"));



	EIGlobal.mensajePorTrace("***MIPD_cambios.class  contador1 = >" +Integer.toString(contador)+ "<", EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  TIPOTRANS = >" +(String)req.getParameter("TIPOTRANS")+ "<", EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  NMAXOPER  = >" +Global.MAX_REGISTROS+ "<", EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  trans	   = >" +(String)req.getParameter("trans")+ "<", EIGlobal.NivelLog.INFO);



		req.setAttribute("MenuPrincipal", session.getStrMenu());

	req.setAttribute("newMenu", session.getFuncionesDeMenu());

	req.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s27510h",req));



	String tcc_compra="";
	String tcc_venta="";


	tcc_compra=req.getParameter("tcc_compra1");

		if (tcc_compra==null){

			tcc_compra="";}

		tcc_venta=req.getParameter("tcc_venta1");

		if (tcc_venta==null){

	    tcc_venta="";}

	req.setAttribute("tcc_compra1", ""+tcc_compra);

	req.setAttribute("tcc_venta1", ""+tcc_venta);

		req.setAttribute("tcv_compra1", ""+tc_ventanilla_compra);

	req.setAttribute("tcv_venta1", ""+tc_ventanilla_venta);



	EIGlobal.mensajePorTrace("***MIPD_cambios.class  tcc_compra1 = >" +tcc_compra+ "<", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  tcc_venta1  = >" +tcc_venta+ "<", EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  tcv_compra1 = >" +tc_ventanilla_compra+ "<", EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  tcv_venta1  = >" +tc_ventanilla_venta+ "<", EIGlobal.NivelLog.INFO);



		req.setAttribute("tcc_aplicar1", ""+req.getParameter("tcenl"));

		req.setAttribute("tcv_aplicar1", ""+req.getParameter("tcvent"));



	req.setAttribute("impsel", "");

		req.setAttribute("impsel2","");



	req.setAttribute("cve_esp1", req.getParameter("cve_esp1"));

		req.setAttribute("cve_esp2", req.getParameter("cve_esp1"));

		req.setAttribute("tcv_compra2", ""+tc_ventanilla_compra);

	req.setAttribute("tcv_venta2", ""+tc_ventanilla_venta);



	EIGlobal.mensajePorTrace("***MIPD_cambios.class  tcc_aplicar1: "+(String)req.getParameter("tcenl"), EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  tcv_aplicar1: "+(String)req.getParameter("tcvent"), EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  ctasel:       "+(String)req.getParameter("ctasel"), EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  impsel:       "+(String)req.getParameter("impsel"), EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  cve_esp1:     "+(String)req.getParameter("cve_esp1"), EIGlobal.NivelLog.DEBUG);



	if (archivo_sin_error==true)

	{

			importetabla=0.0;

			String[] Ctas_origen = desentramaC(Integer.toString(contador)+"@"+arrctascargo,'@');

			String[] Ctas_abono  = desentramaC(Integer.toString(contador)+"@"+arrctasabono,'@');

			String[] Importes    = desentramaC(Integer.toString(contador)+"@"+arrimportes,'@');

			String[] Conceptos   = desentramaC(Integer.toString(contador)+"@"+arrconceptos,'@');

			String[] Estatus     = desentramaC(Integer.toString(contador)+"@"+arrestatus,'@');

	   String[] RFCs	 = desentramaC(Integer.toString(contador)+"@"+arrrfcs,'@');

	   String[] IVAs	 = desentramaC(Integer.toString(contador)+"@"+arrivas,'@');



		   tabla="<TR><Th class=tittabdat >Cuenta cargo</Th><Th class=tittabdat>Cuenta abono</Th><Th class=tittabdat>Importe cargo</Th><Th class=tittabdat>Importe abono</Th><Th class=tittabdat >Concepto</Th><th class=tittabdat >RFC</th><th class=tittabdat >IVA</th></TR>";



			String clase = "";



			for (int indice=1;indice<=contador;indice++)

			{

				if ((indice%2)==0)

					clase="textabdatobs";

				else

					clase="textabdatcla";

					tabla=tabla+"<TR>";



				String[] Cta1 = desentramaC("4|"+Ctas_origen[indice],'|');

				String[] Cta2 = desentramaC("4|"+Ctas_abono[indice],'|');



				tabla=tabla+"<TD class="+clase+" align=left  >"+Cta1[1]+"  "+Cta1[3]+"</TD>";

				tabla=tabla+"<TD class="+clase+" align=left  >"+Cta2[1]+"  "+Cta2[3]+"</TD>";

				if(Cta[4].equals("6"))

				{

					tabla=tabla+"<TD class="+clase+" align=right nowrap >"+FormatoMoneda(Importes[indice])+"</TD>";

				tabla=tabla+"<TD class="+clase+" align=right nowrap >"+FormatoMoneda(simporteMN)+"</TD>";

				}

				else

		{

				tabla=tabla+"<TD class="+clase+" align=right nowrap >"+FormatoMoneda(simporteMN)+"</TD>";

					tabla=tabla+"<TD class="+clase+" align=right nowrap >"+FormatoMoneda(Importes[indice])+"</TD>";

		}

				tabla=tabla+"<TD class="+clase+" align=left >"+Conceptos[indice]+"</TD>";



			  if(RFCs[indice].equals(""))

			    tabla=tabla+"<TD class="+clase+" align=left nowrap>&nbsp;</TD>";

			  else

		tabla=tabla+"<TD class="+clase+" align=left nowrap>"+RFCs[indice]+"</TD>";



			  if(IVAs[indice].equals(""))

			    tabla=tabla+"<TD class="+clase+" align=left nowrap>&nbsp;</TD>";

			  else

		tabla=tabla+"<TD class="+clase+" align=left nowrap>"+IVAs[indice]+"</TD>";



				tabla=tabla+"</TR>";

				importetabla+=new Double(Importes[indice]).doubleValue();

			}



			req.setAttribute("imptabla","SI");

			if (contador>0)

			{

			  if ((esHorarioValido(1))||

			    (esHorarioValido(2)))
			  {

			      botontransferir = "<td align=right width=89><a href=\"javascript:confirmacion();\" border=0>";

			      botontransferir = botontransferir + "<img src=\"/gifs/EnlaceMig/gbo25390c.gif\" border=0 >";

			      botontransferir = botontransferir + "</a></td><td align=center><a href=\"javascript:scrImpresion();\" border=0>";

			      botontransferir = botontransferir + "<img border=0  src=\"/gifs/EnlaceMig/gbo25240.gif\"></a></td>";

	      }

			  else

			  {

			     botontransferir = botontransferir + "<td align=center><a href=\"javascript:scrImpresion();\" border=0>";

			     botontransferir = botontransferir + "<img border=0  src=\"/gifs/EnlaceMig/gbo25240.gif\"></a></td>";

	       }



	       req.setAttribute("tabla",tabla);

		       req.setAttribute("botontransferir", botontransferir);

			}

			if (req.getParameter("TIPOTRANS").equals("LINEA"))

			{

				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(IEnlace.CAMBIOS_DATOS_TMPL);

				dispatcher.forward(req, res);

			}

			else

			{

				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(IEnlace.CAMBIOS_ARCHIVO_TMPL);

				dispatcher.forward(req, res);

			}

		}

		else

	{

	   req.setAttribute("tabla","");

	   req.setAttribute("botontransferir","");

		   req.setAttribute("contadoroperaciones","");

		   RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(IEnlace.CAMBIOS_DATOS_TMPL);

		   dispatcher.forward(req, res);

		}

		return 1;

	}






	/**
     * Pantalla_tabla_cotizacion_cambios.
     *
     * @param req the req
     * @param res the res
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    private int pantalla_tabla_cotizacion_cambios(HttpServletRequest req,HttpServletResponse res)

		throws IOException,ServletException

    {

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a pantalla_tabla_cotizacion_cambios ", EIGlobal.NivelLog.INFO);



		String tramactacargo = "";

		String tramactaabono = "";

		String ctacargo = "";

		String ctaabono = "";

		String importedivisa = "";

		String concepto = "";

		String tipocambioesp="";

		String arrctascargo = "";

		String arrctasabono = "";

		String arrimportesmn = "";

		String arrimportesusd = "";

		String arrimportes = "";

		String arrconceptos = "";

		String arrivas = "";

		String arrrfcs = "";

		String arrtcs = "";

		String arrclesp="";

		String arrtcesp="";

		String arrestatus = "";

		String scontador = "";

		String strArchivo = "";

		String tabla = "";

	    String tcc_compra  = "";

	    String tcc_venta   = "";

	    String tcc_aplicar = "";

	    String tcv_aplicar = "";

	    String importemn   = "";

	    String importeusd  = "";

		String arr[]	   ;



		int contador = 0;

		boolean archivo_sin_error = true;



		HttpSession sess = req.getSession();

	BaseResource session = (BaseResource) sess.getAttribute("session");



	String chkvalor = req.getParameter("ChkOnline");



	arrctascargo = req.getParameter("arregloctas_origen2");

	    arrctasabono = req.getParameter("arregloctas_destino2");

	arrimportes  = req.getParameter("arregloimportes2");

		arrconceptos = req.getParameter("arregloconceptos2");

	    arrestatus	 = req.getParameter("arregloestatus2");

		arrrfcs      = req.getParameter("arreglorfcs2");

	arrivas      = req.getParameter("arregloivas2");

	arrclesp     = req.getParameter("arregloclesp2");

		arrtcesp     = req.getParameter("arreglotipcambesp2");

		scontador    = req.getParameter("contador2");

		contador     = Integer.parseInt(scontador);



	EIGlobal.mensajePorTrace("***COTIZAR arrctascargo: "+arrctascargo, EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***COTIZAR arrctasabono: "+arrctasabono, EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class arrctascargo: "+arrctascargo, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class arrctasabono: "+arrctasabono, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class arrimportes:  "+arrimportes, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class arrconceptos: "+arrconceptos, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class arrestatus:   "+arrestatus, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class arregloclesp2:	   "+arrclesp, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class arreglotipcambesp2:	"+arrtcesp, EIGlobal.NivelLog.DEBUG);



	String[] Ctas_origen  = desentramaC(Integer.toString(contador)+"@"+arrctascargo,'@');

		String[] Ctas_abono   = desentramaC(Integer.toString(contador)+"@"+arrctasabono,'@');

		String[] Importes     = desentramaC(Integer.toString(contador)+"@"+arrimportes,'@');

		String[] Conceptos    = desentramaC(Integer.toString(contador)+"@"+arrconceptos,'@');

		String[] Estatus      = desentramaC(Integer.toString(contador)+"@"+arrestatus,'@');

		String[] Claveesp     = desentramaC(Integer.toString(contador)+"@"+arrclesp,'@');

		String[] TCesp	      = desentramaC(Integer.toString(contador)+"@"+arrtcesp,'@');



	boolean imptabla = true;



	if((req.getParameter("TIPOTRANS").equals("ARCHIVO"))&&(contador>=Global.MAX_REGISTROS)){

	   imptabla = false;}



		boolean horario=true;

	tabla = "";

	tabla ="<table width=760 border=0 cellspacing=2 cellpadding=3>";

	    tabla+="<TR><Th class=tittabdat >Cuenta cargo</Th><Th class=tittabdat >Cuenta abono</Th><Th class=tittabdat >Importe MN</Th><Th class=tittabdat >Importe USD</Th><Th class=tittabdat >Tipo de Cambio</Th><Th class=tittabdat >Fecha de Aplicaci&oacute;n</Th><Th class=tittabdat >Concepto</Th></TR>";



	String clase = "";



		for (int indice=1;indice<=contador;indice++)

		{

			if (Estatus[indice].equals("1"))

			{

				if ((indice%2)==0)

					clase="textabdatobs";

				else

					clase="textabdatcla";

					tabla=tabla+"<TR>";



					String[] Cta1 = desentramaC("4|"+Ctas_origen[indice],'|');

					String[] Cta2 = desentramaC("4|"+Ctas_abono[indice],'|');



					arr=llamada_a_servicio_cotizacion(Cta1[1], Cta1[4], Importes[indice],Claveesp[indice],TCesp[indice],req);

					tcc_compra=arr[0];

				    tcc_venta=arr[1];

			tcc_aplicar=arr[2];

			tcv_aplicar=arr[3];

			importemn=arr[4];

			importeusd=arr[5];



				EIGlobal.mensajePorTrace("***MIPD_cambios.class importemn: "+importemn, EIGlobal.NivelLog.INFO);

				EIGlobal.mensajePorTrace("***MIPD_cambios.class importeusd: "+importeusd, EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("***MIPD_cambios.class tcc_aplicar: "+tcc_aplicar, EIGlobal.NivelLog.INFO);



					arrimportesmn+=importemn+"@";

					arrimportesusd+=importeusd+"@";

					arrtcs+=tcc_aplicar+"@";

				tabla=tabla+"<TD class="+clase+" align=left  >"+Cta1[1]+"  "+Cta1[3]+"</TD>";

					tabla=tabla+"<TD class="+clase+" align=left  >"+Cta2[1]+"  "+Cta2[3]+"</TD>";



					tabla=tabla+"<TD class="+clase+" align=right nowrap >"+FormatoMoneda(importemn)+"</TD>";

					tabla=tabla+"<TD class="+clase+" align=right nowrap >"+FormatoMoneda(importeusd)+"</TD>";

					tabla=tabla+"<TD class="+clase+" align=right nowrap >"+tcc_aplicar+"</TD>";



				if ((esHorarioValido(1))||

					(esHorarioValido(2)))

				    {

					    tabla=tabla+"<TD class="+clase+" align=center nowrap>"+ObtenFecha(true)+"</TD>";

			    }

				    else

					tabla=tabla+"<TD class="+clase+" align=center >No Aplica. Fuera de Horario</TD>";



					tabla=tabla+"<TD class="+clase+" align=left >"+Conceptos[indice]+"</TD>";

					tabla=tabla+"</TR>";

	    }

		}

		   tabla+="</table>";

		req.setAttribute("arregloctas_origen2","\""+arrctascargo+"\"");

		req.setAttribute("arregloctas_destino2","\""+arrctasabono+"\"");

		req.setAttribute("arregloimportesmn2",arrimportesmn);

		req.setAttribute("arregloimportesusd2",arrimportesusd);

	    req.setAttribute("arreglotc2",arrtcs);

		req.setAttribute("arregloconceptos2","\""+arrconceptos+"\"");

		req.setAttribute("arregloivas2","\""+arrivas+"\"");

		req.setAttribute("arreglorfcs2","\""+arrrfcs+"\"");

		req.setAttribute("arregloclesp2","\""+arrclesp+"\"");

		req.setAttribute("arreglotipcambesp2","\""+arrtcesp+"\"");



		EIGlobal.mensajePorTrace("***MIPD_cambios.class 13", EIGlobal.NivelLog.INFO);

		req.setAttribute("arregloestatus2",arrestatus);

		req.setAttribute("contador2",Integer.toString(contador));

		EIGlobal.mensajePorTrace("***MIPD_cambios.class 14", EIGlobal.NivelLog.INFO);

		req.setAttribute("INICIAL","NO");

		req.setAttribute("TIPOTRANS", req.getParameter("TIPOTRANS"));

		req.setAttribute("IMPORTACIONES","1");

		req.setAttribute("NMAXOPER"," "+Global.MAX_REGISTROS);

		req.setAttribute("trans", req.getParameter("trans"));

		EIGlobal.mensajePorTrace("***MIPD_cambios.class 15", EIGlobal.NivelLog.INFO);

		req.setAttribute("MenuPrincipal", session.getStrMenu());

	req.setAttribute("newMenu", session.getFuncionesDeMenu());

	req.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s27520h",req));

		EIGlobal.mensajePorTrace("***MIPD_cambios.class 16", EIGlobal.NivelLog.INFO);



		req.setAttribute("ctasel2", "\""+req.getParameter("ctasel2")+"\"");

	req.setAttribute("impsel2", req.getParameter("impsel2"));

		EIGlobal.mensajePorTrace("***MIPD_cambios.class 17", EIGlobal.NivelLog.INFO);

		if (req.getParameter("cve_esp1")!=null)

		   req.setAttribute("cve_esp1", req.getParameter("cve_esp1"));

		else

		   req.setAttribute("cve_esp1", "");

		if (req.getParameter("cve_esp2")!=null)

		   req.setAttribute("cve_esp2", req.getParameter("cve_esp2"));

		else

	   req.setAttribute("cve_esp2", "");

		EIGlobal.mensajePorTrace("***MIPD_cambios.class 18", EIGlobal.NivelLog.INFO);

		if (req.getParameter ("tcv_venta2")!=null)

		  req.setAttribute("tcv_venta",req.getParameter ("tcv_venta2"));

	else

	  req.setAttribute("tcv_venta","");

		if (req.getParameter("tcv_compra2")!=null)

		  req.setAttribute("tcv_compra",req.getParameter("tcv_compra2"));

		else

	  req.setAttribute("tcv_compra","");

		EIGlobal.mensajePorTrace("***MIPD_cambios.class 19", EIGlobal.NivelLog.INFO);

		req.setAttribute("Tiempo",EnlaceGlobal.fechaHoy("tm|ts|"));



	req.setAttribute("imptabla","SI");



	    EIGlobal.mensajePorTrace("***MIPD_cambios.class tabla "+tabla, EIGlobal.NivelLog.INFO);



	req.setAttribute("tabla",tabla);

	   if ((esHorarioValido(1))||

				    (esHorarioValido(2)))

		  req.setAttribute("botontransferir","<td align=right width=89><a  href=\"javascript:validar();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=center><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td>");

	else

		  req.setAttribute("botontransferir","<td align=center><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td>");



		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(IEnlace.CAMBIOS_COTIZ_TMPL);

		dispatcher.forward(req, res);



		return 1;

	}








	/**
	 * Ejecucion_operaciones_cambios.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public int ejecucion_operaciones_cambios(HttpServletRequest req, HttpServletResponse res)

		throws IOException,ServletException

    {

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a ejecucion_operaciones_transferencias ", EIGlobal.NivelLog.INFO);


		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable hs = null;
		String buffer = "";
		Double conSaldo = 0.0;
		Double importeDiv = 0.0;
		boolean esMancomunado = false;
		String desResCodErr = "";
		String folioOperacion = "";
		String respuesta02 = "";
		String divisaOperacion = "";

	String strorig1 = "";
	String strdest1 = "";
	String mensaje_salida = "";

	String codError = "";

	String resCodError = null;

	String cta_origen = "";

	String cta_destino = "";

		String des_cta_origen = "";

		String des_cta_destino = "";

		String simportemn = "";

		String simporteusd = "";

		String tipo_cambio_aplicar = "";

	double importe = 0.0;

	String concepto = "";

	Double importedbl;

	String tipo_cta_origen = "";

	String tipo_cta_destino = "";

	String cadena_cta_origen = "";

	String cadena_cta_destino = "";

	String arreglo_ctas_origen = "";

	String arreglo_ctas_destino = "";

	String arreglo_importesmn = "";

	String arreglo_importesusd = "";

		String arreglo_tcs = "";

		String arreglo_beneficiarios = "";

		String arreglo_conceptos = "";

		String arreglo_rfcs = "";

		String arreglo_ivas = "";

		String arreglo_clesp="";

		String arreglo_tcesp="";

	String arreglo_estatus = "";

		int contador = 0;

	String scontador = "";

	int indice = 0;

	String bandera_estatus = "";

		String imptabla = "";

		String tramaok = "";

		String error = "";

	String tipo_operacion = "";

		String cve_divisa_cargo = "";

		String cve_divisa_abono = "";

		String importe_cargo = "";

  	    String importe_abono = "";

		String clave_especial = "";

        String strdest2 			  = "";

	    String strorig2 			  = "";

		String	mensajeError		= "";

	boolean comuError=false;

	    float tiempoTotal = 0;

	    Integer referencia = 0;

	    String respuesta   = "";

	    String tiempoEspera = req.getParameter("Tiempo");

		HttpSession sess = req.getSession();

	BaseResource session = (BaseResource) sess.getAttribute("session");



		int contMin1 = Integer.parseInt(tiempoEspera.substring(0,2));

		int contSeg1 = Integer.parseInt(tiempoEspera.substring(3,5));

		int contMin2 = Integer.parseInt(EnlaceGlobal.fechaHoy("tm").trim());

		int contSeg2 = Integer.parseInt(EnlaceGlobal.fechaHoy("ts").trim());



	    if(contMin1>50 && contMin2<=50){

		  contMin2+=60;}



		contMin1=(contMin1*60)+contSeg1;

	    EIGlobal.mensajePorTrace("MIPD_cambios- generaTablaDepositos(): Tiempo Total minutos 1: "+ new Integer(contMin1).toString(), EIGlobal.NivelLog.INFO);

	    contMin2=(contMin2*60)+contSeg2;

	    EIGlobal.mensajePorTrace("MIPD_cambios - generaTablaDepositos(): Tiempo Total minutos 2: "+ new Integer(contMin2).toString(), EIGlobal.NivelLog.INFO);

	    tiempoTotal=(float)((contMin2-contMin1)/60);

	    EIGlobal.mensajePorTrace("MIPD_cambios - generaTablaDepositos(): Tiempo Total: "+ new Float(tiempoTotal).toString(), EIGlobal.NivelLog.INFO);



		if(tiempoTotal>=3.0)

	    {

			pantalla_inicial_cambios("cuadroDialogo('Ya han pasado mas de 3 minutos, por lo que el tipo de cambio pudo haber variado.\\nEs necesario realizar una nueva cotizaci�n.');", req, res);

			return 0;

	    }



	scontador = req.getParameter("contador2");

	contador = Integer.parseInt(scontador);


	arreglo_ctas_origen = req.getParameter("arregloctas_origen2");
	strorig1 = arreglo_ctas_origen;

	arreglo_ctas_destino = req.getParameter("arregloctas_destino2");
	strdest1 = arreglo_ctas_destino;

	arreglo_importesmn = req.getParameter("arregloimportesmn2");

		arreglo_importesusd = req.getParameter("arregloimportesusd2");

		arreglo_tcs = req.getParameter("arreglotc2");

	arreglo_conceptos = req.getParameter("arregloconceptos2");

	arreglo_estatus = req.getParameter("arregloestatus2");

		arreglo_ivas = req.getParameter("arregloivas2");

		arreglo_rfcs = req.getParameter("arreglorfcs2");

		arreglo_clesp= req.getParameter("arregloclesp2");

	    imptabla = req.getParameter("imptabla");


    strorig2=arreglo_ctas_origen;

    EIGlobal.mensajePorTrace("CUENTA CARGO RESPALDADA<" + strorig1 + "> CUENTA CARGO A COMPARAR<" + strorig2 + ">", EIGlobal.NivelLog.DEBUG);
		 if (!strorig1.equals(strorig2))

		     {

		     EIGlobal.mensajePorTrace("PROBLEMA: Se Modifico la(s) Cuenta(s) Cargo", EIGlobal.NivelLog.DEBUG);

			  comuError=true;

	      mensajeError="Transaccion No V�lida";

			 }


    strdest2=arreglo_ctas_destino;

	EIGlobal.mensajePorTrace("CUENTA ABONO RESPALDADA<" + strdest1 + "> CUENTA ABONO A COMPARAR<" + strdest2 + ">", EIGlobal.NivelLog.DEBUG);
	if (!strdest1.equals(strdest2))

		    {
		      EIGlobal.mensajePorTrace("PROBLEMA: -Se Modifico la(s) Cuenta(s) Abono", EIGlobal.NivelLog.DEBUG);

	      comuError=true;

	      mensajeError="Transaccion No V�lida";

		    }

    if(comuError==true)

     {

	despliegaPaginaError(mensajeError,"Cambios de divisas(Transferencias)","", req, res);

	   return 1;

      }


	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_ctas_origen  "+arreglo_ctas_origen, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_ctas_destino "+arreglo_ctas_destino, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_importesmn   "+arreglo_importesmn, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_importesusd  "+arreglo_importesusd, EIGlobal.NivelLog.DEBUG);

	EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_tc	      "+arreglo_tcs, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_conceptos    "+arreglo_conceptos, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_ivas	      "+arreglo_ivas, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  arreglo_rfcs	      "+arreglo_rfcs, EIGlobal.NivelLog.DEBUG);

		EIGlobal.mensajePorTrace("***MIPD_cambios.class  clave_especial       "+arreglo_clesp, EIGlobal.NivelLog.DEBUG);

	    EIGlobal.mensajePorTrace("***MIPD_cambios.class  imptabla "+imptabla, EIGlobal.NivelLog.DEBUG);



	String[] Ctas_origen  = desentramaC(scontador+"@"+arreglo_ctas_origen,'@');

	String[] Ctas_destino = desentramaC(scontador+"@"+arreglo_ctas_destino,'@');

	String[] ImportesMN   = desentramaC(scontador+"@"+arreglo_importesmn,'@');

		String[] ImportesUSD  = desentramaC(scontador+"@"+arreglo_importesusd,'@');

		String[] TiposCambio  = desentramaC(Integer.toString(contador)+"@"+arreglo_tcs,'@');

		String[] Conceptos    = desentramaC(scontador+"@"+arreglo_conceptos,'@');

	String[] Estatus      = desentramaC(scontador+"@"+arreglo_estatus,'@');

		String[] Ivas	      = desentramaC(scontador+"@"+arreglo_ivas,'@');

		String[] RFCs	      = desentramaC(scontador+"@"+arreglo_rfcs,'@');

		String[] Clavespecial = desentramaC(scontador+"@"+arreglo_clesp,'@');



	    mensaje_salida="<table width=760 border=0 cellspacing=2 cellpadding=3>";

		mensaje_salida+="<tr><td class=textabref colspan=8>Este es el resultado de sus operaciones</td></tr>";

	mensaje_salida+="<TR>";

	mensaje_salida+="<Th align=left class=tittabdat   >Cuenta cargo</Th>";

	mensaje_salida+="<Th align=left class=tittabdat   >Cuenta abono</Th>";

	mensaje_salida+="<Th class=tittabdat align=center >Importe MN</Th>";

		mensaje_salida+="<Th class=tittabdat align=center >Importe USD</Th>";

		mensaje_salida+="<Th class=tittabdat align=center >Tipo de Cambio</Th>";

		mensaje_salida+="<Th class=tittabdat align=center >Fecha de Aplicaci&oacute;n</Th>";

	mensaje_salida+="<Th class=tittabdat align=center >Concepto</Th>";

		mensaje_salida+="<Th class=tittabdat align=center >Estatus</Th>";

	mensaje_salida+="<Th class=tittabdat align=center >No. Orden</Th>";

		mensaje_salida+="<Th class=tittabdat align=center >Referencia</Th>";

	mensaje_salida+="<Th class=tittabdat align=center >Saldo actualizado</Th>";

	mensaje_salida+="</TR>";



	int contadorok = 0;

	String trama_entrada = "";

		String sreferencia = "";

	String clase = "";

		String orden = "";

		String saldo_disponible = "";

		String estatus = "";

		short tipo_fecha = 0;

		String conceptocompleto = "";

		int longi=0;

		int j=0;

		LYMValidador valida = null;

	try {

	for (int indice1=1;indice1<=contador;indice1++)

	{

	    bandera_estatus = Estatus[indice1];

	    if (bandera_estatus.equals("1"))

	    {

				cadena_cta_origen  = Ctas_origen[indice1];

				cadena_cta_destino = Ctas_destino[indice1];

		        simportemn	       = ImportesMN[indice1];
				simporteusd	   	   = ImportesUSD[indice1];

			    concepto	       = Conceptos[indice1];



				String Cta1 [] = desentramaC("4|"+cadena_cta_origen,'|');

				String Cta2 [] = desentramaC("4|"+cadena_cta_destino,'|');



		    cta_origen	= Cta1[1];

				cta_destino = Cta2[1];

				des_cta_origen = Cta1[3];

				des_cta_destino = Cta2[3];



				mensaje_salida += "<TR>";

				if ((indice1%2)==0)

			clase="textabdatobs";

				else

					clase="textabdatcla";



				if (Cta1[4].equals("6")) //si la cta de cargo es de dolar

				{

					tipo_operacion = "CPA";

					cve_divisa_cargo = "USD";

					cve_divisa_abono = "MN";

					importe_cargo = simporteusd;

					importe_abono = simportemn;

				}

				else

				{

					tipo_operacion = "VTA";

					cve_divisa_cargo = "MN";

					cve_divisa_abono = "USD";

					importe_cargo = simportemn;

					importe_abono = simporteusd;

				}



			    conceptocompleto="";

			    if (concepto.length()>60){

				    conceptocompleto=concepto.substring(0,60);}

				else

				{

					conceptocompleto=concepto;

					longi=concepto.length();

					for(j=0;j<(60-longi);j++)

						conceptocompleto+=" ";

				}

				if  ((RFCs[indice1].length()!=0)&&(Ivas[indice1].length()!=0)){

					conceptocompleto+="+"+RFCs[indice1]+" IVA "+Ivas[indice1]; }





				clave_especial=Clavespecial[indice1];

				clave_especial=clave_especial.trim();



				EIGlobal.mensajePorTrace("************************************************", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace("***MIPD_cambios - tipo_operacion: /"+tipo_operacion+"/", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace("***MIPD_cambios - divisaCargo   : /"+cve_divisa_cargo+"/", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace("***MIPD_cambios - divisaAbono   : /"+cve_divisa_abono+"/", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace("***MIPD_cambios - beneficiario  : /"+des_cta_destino+"/", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace("***MIPD_cambios - concepto	  : /"+conceptocompleto+"/", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace("***MIPD_cambios - claveEspecial : /"+clave_especial+"/", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace("************************************************", EIGlobal.NivelLog.DEBUG);

				String refmancomunidad = "| |";

				trama_entrada = "1EWEB|"+session.getUserID8()+"|DIPD|"+session.getContractNumber()+"|";

				trama_entrada += session.getUserID8()+"|"+session.getUserProfile()+"|"+"CHDO@"+tipo_operacion+"@";

				trama_entrada += cve_divisa_cargo+"@"+cve_divisa_abono+"@"+cta_origen+"@"+cta_destino+"@MN@";

				trama_entrada += simportemn+"@"+simporteusd+"@"+simporteusd+"@"+TiposCambio[indice1]+"@1@d@@@@@@";

				trama_entrada += des_cta_destino+"@"+conceptocompleto+"@10:00@"+clave_especial+"@@"+ refmancomunidad;

				//VALIDACI�N DEL HORARIO PARA TRANSFERENCIA INTERNACIONAL
				if ((esHorarioValido(1)) || (esHorarioValido(2))){
					//Llamado al SREFERENCIA
					try {
						hs = tuxGlobal.sreferencia("901");;
						codError = hs.get("COD_ERROR").toString();
						EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",	EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hs.toString(), EIGlobal.NivelLog.INFO);
						referencia = (Integer) hs.get("REFERENCIA");
					} catch (Exception e) {
						codError = null;
						referencia = 0;
					}
					// VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
					if (codError.endsWith("0000")) {

						EIGlobal.mensajePorTrace("LYM_VALIDA [DIPD]", EIGlobal.NivelLog.INFO);

						if (valida == null) {
							valida = new LYMValidador();
						}

						codError = valida.validaLyM(trama_entrada);

						EIGlobal.mensajePorTrace("LYM_VALIDA [DIPD]: " + codError, EIGlobal.NivelLog.INFO);

						if(codError.indexOf("ALYM0000") != -1) {

						//FORMANDO TRAMA PARA SERVICIO MANC_VALIDA
						String trama_manc = "";
		  			    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
		  				trama_manc += sdf.format(new Date()) + "|";
		  				trama_manc += referencia.toString() +  "|";
		  				trama_manc += session.getContractNumber()+"|";
		  				trama_manc += "DIPD|||";
		  				trama_manc += convierteUsr8a7(session.getUserID()) + "|";
		  				trama_manc += cta_origen +"|";
		  				trama_manc += cta_destino +"|";
		  				trama_manc += cve_divisa_abono +"|";
		  				trama_manc += tipo_operacion +"|";
		  				trama_manc += cve_divisa_cargo +"||||";
		  				trama_manc += "d||";
		  				trama_manc += "CHDO||||";
		  				trama_manc += des_cta_destino + "||||";
		  				trama_manc += conceptocompleto+ "||";
		  				trama_manc += clave_especial +"|";
		  				trama_manc += formatoImporte(simportemn) + "|";
		  				trama_manc += formatoImporte(simporteusd) + "|";
		  				trama_manc += formatoImporte(simporteusd) + "|";
		  				trama_manc += TiposCambio[indice1] + "|";
		  				trama_manc += "1.000000" + "||";

		  				//LLAMADO AL MANC_VALIDA
						try {
							hs = tuxGlobal.manc_valida(trama_manc);
							codError = (String) hs.get("COD_ERROR");
							buffer = (String) hs.get("BUFFER");
						} catch (Exception e) {
							codError = null;
						}

						//VERIFICANDO RESPUESTA DEL MANC_VALIDA
						if (codError.endsWith("0000")) {
							try {
								if(buffer.substring(0,1).equals("0")){
									//FLUJO MANCOMUNADO, NO SE EJECUTA GP07
									resCodError = "OK";
									esMancomunado = true;
								}else if(buffer.substring(0,1).equals("1")){

											TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();

											if(tipo_operacion.trim().equals("VTA"))
												divisaOperacion = cve_divisa_abono;
											else if(tipo_operacion.trim().equals("CPA"))
												divisaOperacion = cve_divisa_cargo;
											EIGlobal.mensajePorTrace("MIPD_cambios:: ejecucion_operaciones_cambios:: divisaOperacion->"+divisaOperacion, EIGlobal.NivelLog.INFO);

											respuesta = tipoCambio.getTranspasoCuentas(
													cta_origen, //cuentaCargo
													divisaOperacion, //divisa
													formatoImporte(simporteusd), //montoDivisa
													TiposCambio[indice1],   //tipo cambio
													formatoImporte(simportemn),  //equivalenteMN
													formatoImporte(simporteusd), //equivalenteDLLS
													cta_destino, //cuentaAbono
													clave_especial, //referenciaClave
													"", //folioOpCancelacion
													"0981", //centroOperante
													"0981", //centroOrigen
													"0981", //centroDestino
													concepto, //ObsCargo Concepto
													concepto); //ObsAbono

											EIGlobal.mensajePorTrace("MIPD_cambios . RESPUESTA GP02<<" + respuesta + ">>", EIGlobal.NivelLog.INFO);

											if(respuesta.substring(0, 2).trim().equals("@1")){

												//SE LLENA VARIABLE DE FOLIO DE LIQUIDACI�N
												respuesta02 = respuesta.substring(respuesta.indexOf("DCGPM0021 P"), respuesta.length());
												folioOperacion = respuesta02.substring(264, 274);
												EIGlobal.mensajePorTrace("MIPD_cambios::ejecucion_operaciones_cambios:: folioOperacion<<" + folioOperacion + ">>", EIGlobal.NivelLog.INFO);
												//SE LLENA VARIABLE DE ORDEN
												orden = folioOperacion;
												EIGlobal.mensajePorTrace("MIPD_cambios::ejecucion_operaciones_cambios:: orden<<" + orden + ">>", EIGlobal.NivelLog.INFO);

												//SE BITACORIZA EN LA EWEB_BITA_INTER
												tipoCambio.insertEWEBBITA(session.getContractNumber(), "D", orden, referencia, "RT",
																	      "RT", tipo_operacion,"", cve_divisa_abono, cve_divisa_cargo,
																	      "CHDO", session.getUserID8(),"", clave_especial,
														                  cta_origen, cta_destino, folioOperacion, folioOperacion, 0,
														                  0, simportemn, simporteusd, simporteusd, TiposCambio[indice1], "1.000000",
														                  sdf.format(new Date()), sdf.format(new Date()), "", "", "", conceptocompleto,
														                  des_cta_destino, "");

												resCodError = "OK";
												codError = "DIIN0000";
											}else{
												//ERROR AL EJECUTAR GP02
												resCodError = "ERROR";
												desResCodErr = respuesta.substring(18, respuesta.indexOf('�'));
												codError = respuesta.substring(10, 17);
												EIGlobal.mensajePorTrace("MIPD_cambios:: ejecucion_operaciones_cambios:: RECHAZADA->"+desResCodErr, EIGlobal.NivelLog.INFO);
												EIGlobal.mensajePorTrace("MIPD_cambios:: ejecucion_operaciones_cambios:: COD ERROR->"+codError, EIGlobal.NivelLog.INFO);
											}

								}
							} catch (Exception e) {
								EIGlobal.mensajePorTrace("*** MIPD_cambios - ERROR EN LA TRANSFERENCIA  ***"+e.getMessage(), EIGlobal.NivelLog.INFO);
								resCodError = "ERROR";
								codError = "DIIN9999";
								desResCodErr = "PROBLEMAS AL REALIZAR LA OPERACI�N";
							}
						}else{
							//LA RESPUESTA DEL MANC_VALIDA NO FUE EXITOSA
							resCodError = "ERROR";
							desResCodErr = getDesErrManc(codError);
							EIGlobal.mensajePorTrace("MIPD_cambios:: ejecucion_operaciones_cambios::"+desResCodErr, EIGlobal.NivelLog.INFO);
						}
						} else {

							desResCodErr = codError.substring(16);
							codError = codError.substring(0, 8);
							resCodError = "ERROR";

							EIGlobal.mensajePorTrace("LYM_VALIDA [DIPD] [ERROR] :" + codError, EIGlobal.NivelLog.INFO);
						}
					}else{
						//CASO CONTRARIO REFERENCIA INCORRECTA
						resCodError = "ERROR";
						desResCodErr = "PROBLEMAS AL OBTENER LA REFERENCIA";
						EIGlobal.mensajePorTrace("MIPD_cambios:: ejecucion_operaciones_cambios::"+desResCodErr, EIGlobal.NivelLog.INFO);
					}
				}else{
					//HORARIO NO VALIDO PARA TRANSFERENCIA INTERNACIONAL
					resCodError = "ERROR";
					desResCodErr = "FUERA DE HORARIO PARA OPERAR";
					EIGlobal.mensajePorTrace("MIPD_cambios:: ejecucion_operaciones_cambios::"+desResCodErr, EIGlobal.NivelLog.INFO);
				}

				 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
					BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
					BitaTransacBean bt = new BitaTransacBean();
					BitaTCTBean beanTCT = new BitaTCTBean ();

					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_CAMBIOS)){
						BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);
						bt = new BitaTransacBean();

						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ET_CAMBIOS_CAMBIO_DIVISA);
						bt.setTipoMoneda("MN");
						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}
						if (cta_origen != null) {
							if(cta_origen.length() > 20){
								bt.setCctaOrig(cta_origen.substring(0,20));
							}else{
								bt.setCctaOrig(cta_origen.trim());
							}
						}
						if (cta_destino != null) {
							if(cta_destino.length() > 20){
								bt.setCctaDest(cta_destino.substring(0,20));
							}else{
								bt.setCctaDest(cta_destino.trim());
							}
						}
						if (TiposCambio[indice1] != null && !TiposCambio[indice1].trim().equals("")) {
							bt.setTipoCambio(Double.parseDouble(TiposCambio[indice1].trim()));
						}
						if (simportemn != null && !simportemn.trim().equals("")) {
							bt.setImporte(Double.parseDouble(simportemn.trim()));
						}
					}

					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
						BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);

					bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_OPER_REDSRV_DIDP);


					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					if (cta_origen != null) {
						if(cta_origen.length() > 20){
							bt.setCctaOrig(cta_origen.substring(0,20));
						}else{
							bt.setCctaOrig(cta_origen.trim());
						}
					}
					if (cta_destino != null) {
						if(cta_destino.length() > 20){
							bt.setCctaDest(cta_destino.substring(0,20));
						}else{
							bt.setCctaDest(cta_destino.trim());
						}
					}
					if (TiposCambio[indice1] != null && !TiposCambio[indice1].trim().equals("")) {
						bt.setTipoCambio(Double.parseDouble(TiposCambio[indice1].trim()));
					}
					if (simportemn != null && !simportemn.trim().equals("")) {
						bt.setImporte(Double.parseDouble(simportemn.trim()));
					}
					bt.setTipoMoneda("MN");

					}
					if(resCodError != null){
						if(resCodError.equals("OK")){
							bt.setIdErr("DIDP0000");
						}else{
							bt.setIdErr("DIDP9999");
						}
					}

						sreferencia = referencia.toString().trim();

					if(sreferencia.equals("")){
						bt.setReferencia(0);
					}else{
						bt.setReferencia(Long.parseLong(sreferencia));
					}
					bt.setServTransTux("DIDP");
					/*VSWF I Autor=BMB fecha=29-04-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario*/
					EIGlobal.mensajePorTrace("***MIPD_cambios - Antes de bitacorizar el Beneficiario: ", EIGlobal.NivelLog.DEBUG);
					if(des_cta_destino!=null && des_cta_destino!=""){
						bt.setBeneficiario(des_cta_destino);
						EIGlobal.mensajePorTrace("***MIPD_cambios - bitacoriza el Beneficiario: "+des_cta_destino, EIGlobal.NivelLog.DEBUG);
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
					//INI BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
					BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);
					beanTCT = bh.llenarBeanTCT(beanTCT);
					beanTCT.setCuentaOrigen(cta_origen);
					beanTCT.setCuentaDestinoFondo(cta_destino);
					beanTCT.setReferencia(referencia);
					beanTCT.setTipoOperacion("DIPD");
					beanTCT.setCodError(codError);
					beanTCT.setUsuario(session.getUserID8());
					if(getDivisaTrx(cta_origen).equals("USD"))
						beanTCT.setImporte(Double.parseDouble(simporteusd.trim()));
					else
						beanTCT.setImporte(Double.parseDouble(simportemn.trim()));
				    BitaHandler.getInstance().insertBitaTCT(beanTCT);
					//FIN BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
				}catch(SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				 }

			mensaje_salida += "<TD class="+clase+" align=left   nowrap >"+cta_origen+"  "+des_cta_origen+"</TD>";

				mensaje_salida += "<TD class="+clase+" align=left   nowrap>"+cta_destino+"  "+des_cta_destino+"</TD>";

				mensaje_salida += "<TD class="+clase+" align=right nowrap>"+FormatoMoneda(simportemn)+"</TD>";

				mensaje_salida += "<TD class="+clase+" align=right nowrap>"+FormatoMoneda(simporteusd)+"</TD>";

				mensaje_salida += "<TD class="+clase+" align=right nowrap>"+TiposCambio[indice1]+"</TD>";



				if ((esHorarioValido(1)) || (esHorarioValido(2)))

					mensaje_salida += "<TD class="+clase+" align=center nowrap>"+ObtenFecha(true)+"</TD>";

			    else

					mensaje_salida += "<TD class="+clase+" align=center >No Aplica. Fuera de Horario</TD>";



				mensaje_salida += "<TD class="+clase+" align=left nowrap>"+concepto.trim()+"</TD>";



				if(resCodError!=null)

				{

					if (resCodError.equals("OK"))

					{

						contadorok++;

						if(esMancomunado)
						{

							estatus     = "MANCOMUNADA";

							orden	    = "-------";

							sreferencia = referencia.toString();

							saldo_disponible = "-------";



							mensaje_salida += "<TD class="+clase+" align=center nowrap><font color=GREEN>"+estatus+"</font></TD>";

							mensaje_salida += "<TD class="+clase+" align=right nowrap>" +orden+"</TD>";

							mensaje_salida += "<TD class="+clase+" align=center nowrap>"+sreferencia.trim()+"</TD>";

							mensaje_salida += "<TD class="+clase+" align=right nowrap>" +saldo_disponible+"</TD>";

						}

						else

						{

							EIGlobal.mensajePorTrace("**************************", EIGlobal.NivelLog.INFO);

							EIGlobal.mensajePorTrace("***ok sin mancomunidad ***", EIGlobal.NivelLog.DEBUG);

							EIGlobal.mensajePorTrace("**************************", EIGlobal.NivelLog.INFO);

							estatus = "ENVIADA";

							contadorok++;

                            sreferencia = referencia.toString().trim();

							EIGlobal.mensajePorTrace("***MIPD_cambios.class  IVA  : "+Ivas[indice1], EIGlobal.NivelLog.INFO);

							EIGlobal.mensajePorTrace("***MIPD_cambios.class  RFC  : "+RFCs[indice1], EIGlobal.NivelLog.INFO);

							EIGlobal.mensajePorTrace("***MIPD_cambios.class  Orden: "+orden, EIGlobal.NivelLog.INFO);

							EIGlobal.mensajePorTrace("***MIPD_cambios.class  Saldo: "+saldo_disponible, EIGlobal.NivelLog.INFO);



							cta_origen  += "  "+des_cta_origen;

							cta_destino += "  "+des_cta_destino;


							tramaok += contadorok+"|"+cta_origen+"|"+cta_destino+"|"+simportemn+"|"+simporteusd+"|";

							tramaok += TiposCambio[indice1]+"|"+ObtenFecha(true)+"|"+concepto+"|"+orden.trim()+"|";

							tramaok += sreferencia.trim()+"|"+RFCs[indice1]+"|"+Ivas[indice1]+"|"+estatus+"@";



							sess.setAttribute ("tramaok", tramaok);

							req.setAttribute ("tramaok", tramaok);



							mensaje_salida += "<TD class="+clase+" align=center nowrap><font color=GREEN>"+estatus+"</font></TD>";

							mensaje_salida += "<TD class="+clase+" align=center nowrap>"+orden+"</TD>";

							mensaje_salida += "<TD class="+clase+" align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value,"+contadorok+");\">"+sreferencia+"</A></TD>";


							EIGlobal.mensajePorTrace("***MIPD_cambios.class  Saldo: "+saldo_disponible, EIGlobal.NivelLog.INFO);

							mensaje_salida += "<TD class="+clase+" align=right nowrap>"+ FormatoMoneda(saldo_disponible)+"</TD>";

						}


					}

					else

					{

						EIGlobal.mensajePorTrace("************************************************", EIGlobal.NivelLog.INFO);

						EIGlobal.mensajePorTrace("*** envio de error en la trama de salida**** ", EIGlobal.NivelLog.DEBUG);

						EIGlobal.mensajePorTrace("************************************************", EIGlobal.NivelLog.INFO);



						sreferencia	 = "-------";

						orden		 = "-------";

						saldo_disponible = "-------";



						mensaje_salida += "<TD class="+clase+" align=center nowrap><font color=red>"+desResCodErr+"</font></TD>";

						mensaje_salida += "<TD class="+clase+" align=center nowrap>"+orden+"</TD>";

						mensaje_salida += "<TD class="+clase+" align=center nowrap>"+sreferencia+"</TD>";

						mensaje_salida += "<TD class="+clase+" align=right nowrap>"+saldo_disponible+"</TD>";



						cta_origen  += "  "+des_cta_origen;

						cta_destino += "  "+des_cta_destino;

					}

				}

				else

				{

					error		 = "Error de operaci&oacute;n";

					sreferencia	 = "-------";

					orden		 = "-------";

				    saldo_disponible = "-------";



					mensaje_salida += "<TD class="+clase+" align=center nowrap><font color=red>"+error+"</font></TD>";

					mensaje_salida += "<TD class="+clase+" align=center nowrap>"+orden+"</TD>";

					mensaje_salida += "<TD class="+clase+" align=right nowrap>"+sreferencia+"</TD>";

					mensaje_salida += "<TD class="+clase+" align=right nowrap>"+saldo_disponible+"</TD>";

				}

				mensaje_salida += "</TR>";



			}

	}
	} finally {
		if (valida != null) {
			valida.cerrar();
		}
	}
	mensaje_salida += "<tr><td class=textabref colspan=8>Si desea obtener su comprobante, haga click en el n&uacute;mero de referencia subrayado.</td></tr>";

	    mensaje_salida += "</TABLE>";

		req.setAttribute("banco",session.getClaveBanco().trim());

		EIGlobal.mensajePorTrace("----------------------> CONFIGURACION NOTIFICACION", EIGlobal.NivelLog.INFO);
		EmailDetails details = new EmailDetails();
		EmailSender sender = new EmailSender();
		EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MPID_cambios -> totalRegistros para Cambio de divisa      : " + contador, EIGlobal.NivelLog.DEBUG);
		try{
			EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MPID_cambios -> codErrorOper para Cambio de divisa        : " + codError, EIGlobal.NivelLog.DEBUG);
			if(sender.enviaNotificacion(codError)){
				if(contador == 1){
							EIGlobal.mensajePorTrace( "<><><><><><><><><>MPID_cambios -> Ctas_origen.length para Cambio de divisa  : " + Ctas_origen.length, EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "<><><><><><><><><>MPID_cambios -> Ctas_destino.length para Cambio de divisa : " + Ctas_destino.length, EIGlobal.NivelLog.DEBUG);

								EIGlobal.mensajePorTrace( "<><><><><><><><><>MPID_cambios -> Cuenta Cargo para Cambio de divisa    	   : " + Ctas_origen[1], EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace( "<><><><><><><><><>MPID_cambios -> Cuenta Abono para Cambio de divisa    	   : " + Ctas_destino[1], EIGlobal.NivelLog.DEBUG);
								if(Ctas_origen != null && Ctas_origen.length > 1){
									String cadena = Ctas_origen[1].substring(0,Ctas_origen[1].indexOf("|")).trim();
									details.setNumCuentaCargo(cadena);
									EIGlobal.mensajePorTrace("<><><><><><><><><>Cuenta Origen obtenida: " + details.getNumCuentaCargo(), EIGlobal.NivelLog.INFO);
								}
								if(Ctas_destino != null && Ctas_destino.length > 1){
									String cadena = Ctas_destino[1].substring(0,Ctas_destino[1].indexOf("|")).trim();
									details.setCuentaDestino(cadena);
									EIGlobal.mensajePorTrace("<><><><><><><><><>Cuenta Destino obtenida: " + details.getCuentaDestino(), EIGlobal.NivelLog.INFO);
								}

							}

						double impTotalUSD = 0;
						double impTotalMN = 0;
						for(int imp = 1; imp <= contador; imp++){
							if(ImportesUSD[imp] != null)
								impTotalUSD = impTotalUSD + Double.parseDouble(ImportesUSD[imp]);
							if(ImportesMN[imp] != null)
								impTotalMN = impTotalMN + Double.parseDouble(ImportesMN[imp]);
						}
						EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MPID_cambios -> ImporteUSD para Cambio de divisa 		   : " + impTotalUSD, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MPID_cambios -> ImporteMN para Cambio de divisa           : " + impTotalMN, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MPID_cambios -> Numero de contrato para Cambio de divisa  : " + session.getContractNumber(), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MPID_cambios -> Nombre de contrato para Cambio de divisa  : " + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MPID_cambios -> refNot para Cambio de divisa              : " + referencia.toString(), EIGlobal.NivelLog.INFO);

						details.setNumeroContrato(session.getContractNumber());
						details.setRazonSocial(session.getNombreContrato());
						details.setImpTotal("" + ValidaOTP.formatoNumero(impTotalUSD) + "-" + ValidaOTP.formatoNumero(impTotalMN));
						details.setNumRegImportados(contador);
						details.setNumRef(referencia.toString());

						EIGlobal.mensajePorTrace("setEstatusActual(codError): <" + codError + ">", EIGlobal.NivelLog.INFO);
						details.setEstatusActual(codError);
						sender.sendNotificacion(req, IEnlace.NOT_CAMBIO_DIVISAS, details);

			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

		if(sess.getAttribute("botonexp")!=null)

			sess.removeAttribute("botonexp");

			sess.setAttribute("botonexp","");

		if(sess.getAttribute("mensaje_salida")!=null)

			sess.removeAttribute("mensaje_salida");

			sess.setAttribute("mensaje_salida",mensaje_salida);

		if(sess.getAttribute("MenuPrincipal")!=null)

			sess.removeAttribute("MenuPrincipal");

			sess.setAttribute("MenuPrincipal",session.getStrMenu());

		if(sess.getAttribute("newMenu")!=null)

			sess.removeAttribute("newMenu");

			sess.setAttribute("newMenu",session.getFuncionesDeMenu());

		if(sess.getAttribute("tramaok")!=null)

			sess.removeAttribute("tramaok");

			sess.setAttribute("tramaok","\""+tramaok+"\"");

		if(sess.getAttribute("contadorok")!=null)

			sess.removeAttribute("contadorok");

			sess.setAttribute("contadorok",""+contadorok);

		if(sess.getAttribute("datoscontrato")!=null)

			sess.removeAttribute("datoscontrato");

			sess.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");

		if(sess.getAttribute("datosusuario")!=null)

			sess.removeAttribute("datosusuario");

			sess.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");

		if(sess.getAttribute("Encabezado")!=null)

			sess.removeAttribute("Encabezado");

			sess.setAttribute("Encabezado",CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s27530h",req));

	    req.setAttribute("botonexp","");

	req.setAttribute("mensaje_salida",mensaje_salida);

		req.setAttribute("MenuPrincipal", session.getStrMenu());

	req.setAttribute("newMenu", session.getFuncionesDeMenu());

	req.setAttribute("tramaok","\""+tramaok+"\"");

	req.setAttribute("contadorok",""+contadorok);



	EIGlobal.mensajePorTrace("***MIPD_cambios  tramaok:"+"\""+tramaok+"\"", EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("***MIPD_cambios  contadorok:"+contadorok, EIGlobal.NivelLog.INFO);



	req.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");

	req.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");



	req.setAttribute("Encabezado", CreaEncabezado("Cambios de divisas(Transferencias)","Tesorer&iacute;a &gt; Tesorer&iacute;a Internacional &gt; Cambios ","s25360h",req));

	req.setAttribute("web_application",Global.WEB_APPLICATION);

	evalTemplate(IEnlace.CAMBIOS_SALIDA_TMPL, req, res);

	return 1;

 }


	/**
	 * Cuentas no registradas.
	 *
	 * @param tipoTrans the tipo trans
	 * @param facCtasNoReg the fac ctas no reg
	 * @return the string
	 */
	String cuentasNoRegistradas(String tipoTrans, boolean facCtasNoReg)

	{

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a cuentasNoRegistradas()", EIGlobal.NivelLog.INFO);



		String strNoRegistradas = "";

		String totales = "";

		String tipoCuenta = "";

		boolean banderaNoReg;



		int totalBancos;

		int numeroCuentasCargo = 0;



		if  (facCtasNoReg)

		{

			strNoRegistradas+="<tr valign=middle><td class=tabmovtexbol nowrap><INPUT TYPE=radio value=OnNoReg NAME=ChkOnline>No Registrada</TD></TR>";

			strNoRegistradas+="<TR><TD class=tabmovtexbol nowrap>";

			strNoRegistradas+="<INPUT TYPE=text NAME=destinoNoReg SIZE=25 class=tabmovtexbol maxlength=20> ";

			strNoRegistradas+="</TD></TR>";

			strNoRegistradas+="<INPUT TYPE=HIDDEN NAME=banderaNoReg VALUE=1>";

		}

		else

		{

			strNoRegistradas+= "";

			strNoRegistradas+="<INPUT TYPE=HIDDEN NAME=banderaNoReg VALUE=0>";

		}

		return strNoRegistradas;

	}


	/**
	 * Valida cuenta no reg.
	 *
	 * @param cta_checar the cta_checar
	 * @param desctanoreg the desctanoreg
	 * @param req the req
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	boolean ValidaCuentaNoReg(String cta_checar, String desctanoreg,HttpServletRequest req) throws IOException,ServletException

	{

		EIGlobal.mensajePorTrace("***MIPD_cambios.class Entrando a ValidaCuentaNoReg", EIGlobal.NivelLog.INFO);





		HttpSession sess = req.getSession();

	BaseResource session = (BaseResource) sess.getAttribute("session");



		String trama_entrada = "1EWEB|"+session.getUserID8()+"|NAME|"+ session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+cta_checar+"|NR";

		String coderror = "";

		boolean respuesta = true;



		try {

			ServicioTux tuxGlobal = new ServicioTux();


		 String IP = " ";
		 String fechaHr = "";
		 IP = req.getRemoteAddr();
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";

			EIGlobal.mensajePorTrace(fechaHr+"***MIPD_cambios  WEB_RED-NAME>>" + trama_entrada, EIGlobal.NivelLog.DEBUG);

			Hashtable hs = tuxGlobal.web_red(trama_entrada);

		coderror = (String) hs.get("BUFFER");

		} catch (Exception e) {

			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

		}



		EIGlobal.mensajePorTrace("***MIPD_cambios<<" + coderror, EIGlobal.NivelLog.DEBUG);

		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
			BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_CAMBIOS)){
				bt.setNumBit(BitaConstants.ET_CAMBIOS_VALIDA_CUENTA_NO_REG);
			}else{
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
					bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_OPER_REDSRV_NAME);
				}
			}
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			if (cta_checar != null) {
				if(cta_checar.length() > 20){
					bt.setCctaDest(cta_checar.substring(0,20));
				}else{
					bt.setCctaDest(cta_checar.trim());
				}
			}
			bt.setServTransTux("NAME");

			if(coderror!=null){
    			if(coderror.substring(0,2).equals("OK")){
    				bt.setIdErr("NAME0000");
    			}else if(coderror.length()>8){
	    			bt.setIdErr(coderror.substring(0,8));
	    		}else{
	    			bt.setIdErr(coderror.trim());
	    		}
    		}

			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		 }

		if (coderror.substring(0,2).equals("OK"))

			desctanoreg = coderror.substring(16,coderror.length());

		else

		{

			respuesta = false;

			desctanoreg = "N&uacute;mero de cuenta err&oacute;nea";

		}

		return respuesta;

	}


  /**
   * Es horario valido.
   *
   * @param hora the hora
   * @return true, if successful
   */
  boolean esHorarioValido(int hora)

	{

	  java.util.Date dateHoy = new java.util.Date();



	   if(hora==1)

		{

		  if(dateHoy.getHours()>=8)

		   {

		     if(dateHoy.getHours()<13)

			   return true;

		     else

			   {

			     if(dateHoy.getHours()==13 && dateHoy.getMinutes()<=30)

				  return true;

			   }

		   }

		}

	else if(hora==2)

	{



		   if (dateHoy.getHours()>13&& dateHoy.getHours()<19)

			   return true;

		     else

			   {

			     if(dateHoy.getHours()==13 && dateHoy.getMinutes()>30)

				  return true;

			   }



	}



	   return false;

	}




	/**
	 * Valida bancocuenta.
	 *
	 * @param cuenta the cuenta
	 * @param req the req
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean validaBancocuenta(String cuenta,HttpServletRequest req)throws ServletException, IOException

	{

		    EIGlobal.mensajePorTrace("***MIPD_cambios.class consultaClaves", EIGlobal.NivelLog.INFO);



			HttpSession sess = req.getSession();

	    BaseResource session = (BaseResource) sess.getAttribute("session");



			ServicioTux tuxGlobal = new ServicioTux();

			String tipo_cuenta			= "";

			String fileOut				= "";

			String tramaEntrada			= "";

			String tramaSalida			= "";

		    String path_archivo 		= "";

			String nombre_archivo		= "";

			String registro 			= "";

			String claveCuen			= "";

			boolean respuesta	    = false;

		BufferedReader fileSua =null;







			fileOut = session.getUserID8() + ".tmp";

			EI_Exportar ArcSal;

			try

			{

			  ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );

			  ArcSal.creaArchivo();

			  ArcSal.escribeLinea(cuenta+ "\n" );

			  ArcSal.cierraArchivo();

			  ArchivoRemoto archR = new ArchivoRemoto();

			  if(!archR.copiaLocalARemoto(fileOut))

		      {

			    EIGlobal.mensajePorTrace("***MIPD_cambios.class No se pudo crear archivo para exportar claves", EIGlobal.NivelLog.INFO);

			    return false;

			  }

			}

			catch(Exception ioeSua )

			{

		    EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);

			   return false;

	    }



			tramaEntrada = "2EWEB|" + session.getUserID8() + "|ECBE|" + session.getContractNumber() + "|" + session.getUserID8() +

						   "|" + session.getUserProfile() + "|";


		 String IP = " ";
		 String fechaHr = "";
		 IP = req.getRemoteAddr();
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";

		    EIGlobal.mensajePorTrace(fechaHr + "***MIPD_cambios.class >> tecb :" + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);

			try

			{

				Hashtable hs = tuxGlobal.web_red(tramaEntrada);

			tramaSalida = (String) hs.get("BUFFER");

				EIGlobal.mensajePorTrace( "***MIPD_cambios.class trama_salida >>" + tramaSalida + "<<", EIGlobal.NivelLog.DEBUG);

			}

			catch(java.rmi.RemoteException re )

			{
				EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);

				return false;

			}

			catch (Exception e) {return false;}


			 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
			BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_CAMBIOS)){
				bt.setNumBit(BitaConstants.ET_CAMBIOS_CONS_CLABE_BANC_EST);
			}else{
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
					bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_OPER_REDSRV_ECBE);
				}
			}
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			if (fileOut != null) {
				bt.setNombreArchivo(fileOut.trim());
			}
			bt.setServTransTux("ECBE");

			if(tramaSalida!=null){
    			if(tramaSalida.substring(0,2).equals("OK")){
    				bt.setIdErr("ECBE0000");
    			}else if(tramaSalida.length()>8){
	    			bt.setIdErr(tramaSalida.substring(0,8));
	    		}else{
	    			bt.setIdErr(tramaSalida.trim());
	    		}
    		}


			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
			 }

		boolean errorFile  = false;

	    try

	    {

		      path_archivo = tramaSalida;

	      nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());



			  EIGlobal.mensajePorTrace( "***MIPD_cambios.class & nombre_archivo_cb:" + nombre_archivo + " &", EIGlobal.NivelLog.INFO);



			  // Copia Remota

			  ArchivoRemoto archR = new ArchivoRemoto();

			  if(!archR.copia(nombre_archivo))

			  {

				EIGlobal.mensajePorTrace( "***MIPD_cambios.class & consultaClaves: No se realizo la copia remota. &", EIGlobal.NivelLog.INFO);

			  }

			  else

			  {

				EIGlobal.mensajePorTrace( "***MIPD_cambios.class & consultaClaves: Copia remota OK. &", EIGlobal.NivelLog.INFO);

			  }



		      path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo; // +"0";



			  EIGlobal.mensajePorTrace( "***MIPD_cambios.class & path_archivo_cb:" + path_archivo + " &", EIGlobal.NivelLog.INFO);







		      fileSua =new BufferedReader( new FileReader(path_archivo) );



			  int indice = 1;

	      while((registro = fileSua.readLine() ) != null)

			  {

			    String cadAux = registro.substring(0, 6).trim();

			    String elError= registro.substring(16, registro.trim().length());

			EIGlobal.mensajePorTrace( "***MIPD_cambios.class & consultaClaves: cadAux. & "+ cadAux, EIGlobal.NivelLog.INFO);

				if (! ( cadAux.equals("CORR00") || cadAux.equals("ERROR3") )  )

			    {

			      claveCuen   = EIGlobal.BuscarToken(registro, '|', 3);

				}

				else

		  claveCuen   = "";

			  } // del while



	 }

		 catch(Exception ioeSua )

	 {

     EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);

	      if(fileSua != null){

		try{

		    fileSua.close();

		}catch(Exception e){}

		fileSua = null;
			}
			  return false;

	 }

		 finally

		 {

	    if(null != fileSua){

		try{

		    fileSua.close();

		}catch(Exception e){}

		fileSua = null;

	    }

		 }

	 EIGlobal.mensajePorTrace( "***MIPD_cambios.class claveCuen >> "+claveCuen +  " <<", EIGlobal.NivelLog.INFO);



	 if(claveCuen==null)

		   claveCuen="";



		  EIGlobal.mensajePorTrace( "***MIPD_cambios.class claveCuen:" + claveCuen + " &", EIGlobal.NivelLog.INFO);





		 if(claveCuen.length()>4)

		 {



			 if(claveCuen.substring(0,4).indexOf(session.getClaveBanco())>=0)

	      respuesta=true;

	 }

		 else

			respuesta=false;

	 return respuesta;

   }

	/**
	 * Metodo para formatear el importe que necesita el servicio MANC_VALIDA
	 *
	 * @param imp		Cadena de importe a formatear
	 * @return			Cadena a dos digitos
	 */
	private String formatoImporte(String imp){
		EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: cantidad recibida->" + imp, EIGlobal.NivelLog.INFO);
		String language = "la"; // ar
		String country = "MX";  // AF
		Locale local = new Locale (language,  country);
		NumberFormat nf = NumberFormat.getInstance(local);
		EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: cantidad formateada->" + nf.format(new Double (imp).doubleValue ()).replace(",",""), EIGlobal.NivelLog.INFO);
		imp = nf.format(new Double (imp).doubleValue ()).replace(",","");
		int dec = 0;
		if(imp.indexOf(".")>0){
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: Longitud cantidad->" + imp.length(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: Caracter punto->" + imp.indexOf("."), EIGlobal.NivelLog.DEBUG);
			dec = imp.length() - (imp.indexOf(".")+1);
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte:: Numero de decimales->" + dec, EIGlobal.NivelLog.DEBUG);
			if(dec>=2)
				return imp.substring(0, imp.indexOf(".")) + imp.substring(imp.indexOf("."), imp.indexOf(".")+3);
			else{
				for(int i=0; i<dec; i++)
					imp += "0";
			}
		}else{
			imp += ".00";
		}
		return imp;
	}

	/**
	 * Metodo para obtener el error de salida del servicio MANC_VALIDA
	 * cuando este no es exitoso
	 *
	 * @param codError		Codigo de Error de salida
	 * @return				Descripci�n del Error
	 */
	private String getDesErrManc(String codError){
		if(codError!=null){
			if(codError.endsWith("0001")){
				return "No existe cuenta en mancomunidad";
			}else if(codError.endsWith("0002")){
				return "No existe tipo operacion en mancomunidad";
			}else if(codError.endsWith("0003")){
				return "No existe usuario en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0004")){
				return "No pudo abrir archivo en mancomunidad";
			}else if(codError.endsWith("0005")){
				return "Usuario tipo A en mancomunidad";
			}else if(codError.endsWith("0006")){
				return "Tipo de operacion no mancomunada";
			}else if(codError.endsWith("0007")){
				return "Tipo firma no autorizada en mancomunidad";
			}else if(codError.endsWith("0008")){
				return "Diferente importe en mancomunidad";
			}else if(codError.endsWith("0009")){
				return "Diferente tipo de cuenta de cargo en mancomunidad - Genere nuevo folio";
			}else if(codError.endsWith("0010")){
				return "No existe folio en mancomunidad-Verifique que sus datos sean correctos";
			}else if(codError.endsWith("0011")){
				return "Contrato-tipo de operacion no mancomunado";
			}else if(codError.endsWith("0012")){
				return "Rechazada";
			}else if(codError.endsWith("0013")){
				return "No hay registros";
			}else if(codError.endsWith("0014")){
				return "Usuarios iguales en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0015")){
				return "Diferente tipo de operacion en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0016")){
				return "Contrato no mancomunado";
			}else if(codError.endsWith("0017")){
				return "Folio no permitido en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0018")){
				return "Suma de montos autorizados menor a importe - Genere un nuevo folio";
			}else if(codError.endsWith("0019")){
				return "No se permite la operacion con cuenta en dolares";
			}else if(codError.endsWith("9999")){
				return "Servicio de mancomunidad no disponible (No se tiene conexion con la BD)";
			}
		}
	return "";
	}

	/**
	 * Metodo para obtener la divisa de la cuenta
	 *
	 * @param cuenta
	 * @return Divisa
	 */
	private String getDivisaTrx(String cuenta){
		String prefijo = "";
		try{
			prefijo = cuenta.substring(0,2);
			if(prefijo.trim().equals("09") || prefijo.trim().equals("42") ||
			   prefijo.trim().equals("49") || prefijo.trim().equals("82") ||
		       prefijo.trim().equals("83") || prefijo.trim().equals("97") ||
		       prefijo.trim().equals("98")){
				return "USD";
		    }else if(prefijo.trim().equals("08") || prefijo.trim().equals("10") ||
					   prefijo.trim().equals("11") || prefijo.trim().equals("13") ||
					   prefijo.trim().equals("15") || prefijo.trim().equals("16") ||
					   prefijo.trim().equals("17") || prefijo.trim().equals("18") ||
					   prefijo.trim().equals("19") || prefijo.trim().equals("20") ||
					   prefijo.trim().equals("21") || prefijo.trim().equals("22") ||
					   prefijo.trim().equals("23") || prefijo.trim().equals("24") ||
					   prefijo.trim().equals("25") || prefijo.trim().equals("26") ||
					   prefijo.trim().equals("27") || prefijo.trim().equals("28") ||
					   prefijo.trim().equals("40") || prefijo.trim().equals("43") ||
					   prefijo.trim().equals("44") || prefijo.trim().equals("45") ||
					   prefijo.trim().equals("46") || prefijo.trim().equals("47") ||
					   prefijo.trim().equals("48") || prefijo.trim().equals("50") ||
					   prefijo.trim().equals("51") || prefijo.trim().equals("52") ||
					   prefijo.trim().equals("53") || prefijo.trim().equals("54") ||
					   prefijo.trim().equals("55") || prefijo.trim().equals("56") ||
					   prefijo.trim().equals("57") || prefijo.trim().equals("58") ||
					   prefijo.trim().equals("59") || prefijo.trim().equals("60") ||
					   prefijo.trim().equals("61") || prefijo.trim().equals("62") ||
					   prefijo.trim().equals("63") || prefijo.trim().equals("64") ||
					   prefijo.trim().equals("65") || prefijo.trim().equals("66") ||
					   prefijo.trim().equals("67") || prefijo.trim().equals("68") ||
					   prefijo.trim().equals("69") || prefijo.trim().equals("70") ||
					   prefijo.trim().equals("71") || prefijo.trim().equals("72") ||
					   prefijo.trim().equals("73") || prefijo.trim().equals("74") ||
					   prefijo.trim().equals("75") || prefijo.trim().equals("76") ||
					   prefijo.trim().equals("77") || prefijo.trim().equals("78") ||
					   prefijo.trim().equals("79") || prefijo.trim().equals("80") ||
					   prefijo.trim().equals("81") || prefijo.trim().equals("85") ||
					   prefijo.trim().equals("86") || prefijo.trim().equals("87") ||
					   prefijo.trim().equals("88") || prefijo.trim().equals("89") ||
					   prefijo.trim().equals("90") || prefijo.trim().equals("91") ||
					   prefijo.trim().equals("92") || prefijo.trim().equals("93") ||
					   prefijo.trim().equals("94") || prefijo.trim().equals("95") ||
					   prefijo.trim().equals("96") || prefijo.trim().equals("97") ||
					   prefijo.trim().equals("99")){
		    	return "MXP";
		    }
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("MIPD_cambios::getDivisaTrx:: Problemas..."+e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		return "USD";
	}

	/**
	 * Metodo para darle formato al mensaje de respuesta
	 *
	 * @param cadena		Cadena a formatear
	 * @return				Cadena formateada
	 */
	private String formatoMensaje(String cadena){
		String patron = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz0123456789 ";
		String formato = "";
		for(int i=0; i<=cadena.length()-1; i++){
			if(patron.indexOf(cadena.charAt(i))>=0){
				formato += cadena.charAt(i);
			}
		}
		return formato;
	}

	/**
	 * Metodo para redondear a un numero determinado de decimales
	 *
	 * @param numero		Numero a rendondear
	 * @param decimales		Precisi�n en decimales
	 * @return				Numero redondeado
	 */
	public double redondear( double numero, int decimales ) {
		return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
	}

}