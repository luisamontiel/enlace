//10611 Antonio Perez




/**Banco Santander Mexicano

* Clase	ImportNomina, Modificacion al archivo de pago de nomina

* @autor Rodrigo Godo

* @version 1.1

* fecha	de creacion: Diciembre 2000	- Enero	20001

* responsable: Roberto Guadalupe Resendiz Altamirano

* Esta clase evalua	si el archivo es nuevo o importado,	en el primer caso, se permitira	la edici�n total del archivo

* (altas, bajas	y cambios en los registros), asi como su eliminaci�n; cuando el	archivo	sea	importado, se validar�n	todos los campos

* de todos los registros de	acuerdo	a las validaciones efectuadas en  enlace internet 2.1 (version de visual basic),

* a	continuaci�n se	determinar�	la cantidad	de registros de	detalle	existentes en el archivo, si el	resultado de esta cuenta

* es de	30 o menor,	se exhibir�n los registros en una tabla	pero no	se podr�n realizar modificaciones en ellos,	en caso	contrario,

* es decir,	cuando sean	m�s	de 30 los registros	existentes,	no se presentar�n.

* La cantidad de registros puede cambiar de	acuerdo	a los caprichos	del	usuario, por lo	cual el	n�mero de registros

* que se presentar�n "ser�n	parametizables"	como dice el Roberto

* Cuando se	permita	la edici�n de registros, se	llamar�	a la plantilla NominaDatos.html

*/



package	mx.altec.enlace.servlets;



import java.util.*;

import java.util.zip.*;

import java.io.*;

import javax.servlet.*;

import javax.servlet.http.*;

import java.text.*;

import javax.naming.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ValidaArchivoPagos;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.MultipartRequest;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.bo.FormatoMoneda;






public class ImportNomina extends BaseServlet {

	/** Cosntante para session */
	public static final String SESSION = "session";
	/** Cosntante para Encabezado */
	public static final String ENCABEZADO = "Encabezado";
	/** Cosntante para Pago de Nomina */
	public static final String PAGO_NOMINA = "Pago de N&oacute;mina";
	/** Cosntante para s25800h */
	public static final String S25800H = "s25800h";
	/** Cosntante para ContenidoArchivo */
	public static final String CONTENIDO_ARCHIVO = "ContenidoArchivo";
	/** Cosntante para Importando Archivo */
	public static final String IMPORTANDO_ARCHIVO = "Importando Archivo";
	/** Cosntante para NOMI0000 */
	public static final String NOMI0000 = "NOMI0000";
	/** Contate encabezado pago nomina*/
	public static final String ENCABEZADO_NOMINA = "Servicios &gt; N&oacute;mina &gt; Pagos";
	/** Constante peracion  */
	public static final String OPERACION = "operacion";
	/** Constate de mensaje de error */
	public static final String MSG_ERROR = "MsgError";
	/** Constate para tipo de archivo */
	public static final String TIPO_ARCHIVO = "tipoArchivo";
	/** Constate para fecha de aplicacion */
	public static final String EN_FECHA_APLIC = "encFechApli";
	/** CONSTANTE SERLVET NOMINA EN LINEA**/
	public static final String PAG_TMPL_SRV_LIN = "InicioNominaLn";


	/**
	 * Metodo doGet
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws IOException Excepcion
	 * @throws ServletException Excepcion
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )

			throws ServletException, IOException {

		defaultAction( request, response );

	}

	/**
	 * Metodo doPost
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws IOException Excepcion
	 * @throws ServletException Excepcion
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )

				throws ServletException, IOException {

		defaultAction( request, response );

	}

	/**
	 * Metodo default del tipo de metodo
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws IOException Excepcion
	 * @throws ServletException Excepcion
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )

			throws IOException, ServletException {

		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo execute()&", EIGlobal.NivelLog.INFO);

		boolean sesionvalida = SesionValida( request, response );

		HttpSession sess = request.getSession();

	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		if ( sesionvalida ) {

			request.setAttribute( "newMenu", session.getFuncionesDeMenu());

			request.setAttribute( "MenuPrincipal", session.getStrMenu());

			request.setAttribute( ENCABEZADO, CreaEncabezado(PAGO_NOMINA,

					ENCABEZADO_NOMINA,S25800H,request));

			if ((String)sess.getAttribute("facultadesN") != null){
				request.setAttribute( "facultades", (String)sess.getAttribute("facultadesN") );
			}



			//se obtiene valor de la fecha de hoy para ser usado como valor del campo fecha de aplicacion del registro de

			//encabezado para archivos nuevos

			GregorianCalendar fechaHoy= new GregorianCalendar();

			String laFechaHoy = EIGlobal.formatoFecha(fechaHoy,"aaaammdd");

			request.setAttribute("fechaHoy",laFechaHoy);

			//variables usadas para formar el calendario que se exhibira dentro del template, los metodos cargarDias(),

			//armaDiasInhablies(), calclafechaFin() y armaArregloFechas(), son autoria de Alejandro Rada y

			//han sido modificados por Ruben Fragoso, Rodrigo Godo tan solo ha unido estos metodos en una sola clase

			//llamada CalendarNomina.class

			/*
			//No se utiliza

			CalendarNomina nominaCalendar = new CalendarNomina( );

			String VarFechaHoy  = "";

			String strInhabiles = nominaCalendar.armaDiasInhabilesJS();

			Calendar fechaFin	= nominaCalendar.calculaFechaFin();

			VarFechaHoy			= nominaCalendar.armaArregloFechas();

			String strFechaFin	= new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();

			*/


			//variables que se crean para almacenar el numero de cuenta seleccionado por el usuario el crear un archivo nuevo,

			//cuando se trate de un archivo importado, se debera leer el valor del campo numero de cuenta del registro de

			//encabezado y tomarlo como valor del combo exhibido



			//se colocan los putString que se usaran el el calendario

			/* //No se utiliza

			request.setAttribute("VarFechaHoy",	 VarFechaHoy);

			request.setAttribute("fechaFin",	 strFechaFin);

			request.setAttribute("diasInhabiles",         strInhabiles);
			*/

			request.setAttribute("Fechas",		  ObtenFecha(true));

			request.setAttribute("FechaHoy",	 ObtenFecha(false) + " - 06" );

			request.setAttribute(CONTENIDO_ARCHIVO,"");



			//se determina la hora del servidor

			String systemHour=ObtenHora();



			request.setAttribute("horaSistema", systemHour);

			//el arreglo de bytes contenidoArchivo almacenara la informacion que el archivo contenga

			//esto es asi debido a que cuando se crea el archivo en el servidor WebServer, este se encuentra

			//vacio, asi que se debe escribir en el la informacion original


			/*******   	 GET - HELG - INICIO - 21 FEB 2005      *******/
           		if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
			{
                 		request.getSession().removeAttribute("Recarga_Ses_Multiples");
                 		EIGlobal.mensajePorTrace( "Import Nomina - Removiendo atributo Recarga_Ses_Multiples.", EIGlobal.NivelLog.INFO);
                 	}

           		if(request.getSession().getAttribute("Ejec_Proc_Val_Pago")!=null)
			{
          	 		request.getSession().removeAttribute("Ejec_Proc_Val_Pago");
           			EIGlobal.mensajePorTrace("Import Nomina - Removiendo atributo Ejec_Proc_Val_Pago.", EIGlobal.NivelLog.INFO);
			}
			/*******   	 GET - HELG - FIN - 21 FEB 2005      *******/


			String operacion   = getFormParameter(request,OPERACION);

			EIGlobal.mensajePorTrace("***ImportNomina.class operacion&  "+operacion, EIGlobal.NivelLog.INFO);
			/*INICIA VALIDACION FICHERO MALICIOSO*/
			if(operacion.equals("errorTipeFile")){
				
				request.setAttribute("ArchivoErr","cuadroDialogo('Archivo de Importaci&oacute;n inv&aacute;lido', 3);");
				despliegaPaginaError("Archivo de Importaci&oacute;n inv&aacute;lido",request, response );
				
			}
			/*FINALIZA VALIDACION FICHERO MALICIOSO*/

			if ( (operacion == null) || ( "".equals(operacion) )){
	             operacion = "cargaArchivo";
			}


			//instancia de ArchivoRemoto que servira para hacer la copia al servidor de web
			try{
				if ( "cargaArchivo".equals(operacion) ) {

					ejecutaCargaArchivo ( request, response);

				}
			}catch (IOException e) {
				EIGlobal.mensajePorTrace("ImportNomina::defaultAction::Hubo Problemas en operacion->"+operacion, EIGlobal.NivelLog.INFO);
				request.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}catch (ServletException e) {
				EIGlobal.mensajePorTrace("ImportNomina::defaultAction :: Hubo Problemas en operacion->"+operacion, EIGlobal.NivelLog.INFO);
				request.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}

			try{
				if ( "validaArchivoImport".equals( operacion ) ) {
					ejecutaValidaArchivo( request, response);
				}
			}catch (IOException e) {
				EIGlobal.mensajePorTrace("ImportNomina : defaultAction : Hubo Problemas en operacion->"+operacion, EIGlobal.NivelLog.INFO);
				request.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}catch (ServletException e) {
				EIGlobal.mensajePorTrace("ImportNomina::defaultAction:: Hubo Problemas en operacion->"+operacion, EIGlobal.NivelLog.INFO);
				request.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response );
			}

		} else {

		   request.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);

		   evalTemplate(IEnlace.ERROR_TMPL, request, response );

		}

	}// default


	 /**
	  * Metodo para cargar archivo
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws IOException Excepcion lanzada
	 * @throws ServletException Excepcion lanzada
	 */
	 public void ejecutaCargaArchivo( HttpServletRequest request, HttpServletResponse response)

			throws IOException, ServletException {

		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo ejecutaCargaArchivo: &", EIGlobal.NivelLog.INFO);

		HttpSession sess = request.getSession();

	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		String nombreOriginal		= "";

		String originalName         = "";

		String nomArchNomina		= "";

		long tamanioArchivo	 	    = 0;

		String fechaAplicacionArchivo = "";

		String cuentaCargo			= "";

		boolean cuentaAsociada		= true;

		boolean errorEnZip=false;

		/*FSW Vector */
		String horario = null;


		String sFile = "";

		BufferedOutputStream dest = null;

		FileInputStream fis = null;

		BufferedInputStream buis = null;

		ZipInputStream zis = null;

		FileOutputStream fos = null;

		try{

		    //se obtiene la ruta original del archivo

		    nombreOriginal  = getFormParameter(request, "fileName");

            nombreOriginal = nombreOriginal.trim();

 			/*FSW Vector */
    		//horario = getFormParameter(request, "horario");


		    EIGlobal.mensajePorTrace("***ImportNomina.class & nombreOriginal>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);

		    EIGlobal.mensajePorTrace("***ImportNomina.class & nombreOriginal Tam�o del archivo antes: "+request.getContentLength(), EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("***ImportNomina.class & Parseando objeto multipart ", EIGlobal.NivelLog.INFO);

			/*FSW Vector */
			EIGlobal.mensajePorTrace("***ImportNomina.class & Horario " + horario , EIGlobal.NivelLog.INFO);





			//

			// Modificacion para manejar el archivo directo de disco

			//

			// Asignacion dinamica de espacio

/*			MultipartRequest mprFile = new MultipartRequest ( request, IEnlace.LOCAL_TMP_DIR, 20971520 );

			//MultipartRequest mprFile = new MultipartRequest ( request, IEnlace.LOCAL_TMP_DIR,request.getContentLength() );

			//java.io.FileInputStream entrada;						//MSD 20071214 se comentar�a porque no se usa este objeto.

			java.util.Enumeration files = mprFile.getFileNames ();

			// Se obtiene el nombre del archivo enviado

			if( files.hasMoreElements () )

			 {

			    String name = ( String ) files.nextElement ();

				sFile = mprFile.getFilesystemName ( name );

			 }
*/
			sFile = getFormParameter(request, "fileName");
			EIGlobal.mensajePorTrace("***ImportNomina.class & Fin Parser - Archivo:  "+ sFile, EIGlobal.NivelLog.INFO);



			// Si el archivo es .zip se descomprime

		    if (sFile.toLowerCase().endsWith(".zip"))

		      {

			    EIGlobal.mensajePorTrace("***ImportNomina.class: El archivo viene en zip", EIGlobal.NivelLog.INFO);



           		final int BUFFER = 2048;

		        fis = new FileInputStream(IEnlace.LOCAL_TMP_DIR+"/" + sFile);

		        buis = new BufferedInputStream(fis);						//MSD 20071214

		        zis = new ZipInputStream(buis);									//MSD 20071214

		        ZipEntry entry;

		        entry = zis.getNextEntry();



		        EIGlobal.mensajePorTrace("***ChesRegistro.class: entry: "+entry, EIGlobal.NivelLog.INFO);

		        if(entry==null)

		         {

				   EIGlobal.mensajePorTrace("***ChesRegistro.class: Entry fue null, archivo no es zip o esta da�ando ", EIGlobal.NivelLog.INFO);

				   errorEnZip=true;

				 }

			   else

			     {

					log( "ImportNomina.java::Extrayendo"  + entry);

					int count;

					byte data[] = new byte[BUFFER];



					// Se escriben los archivos a disco

					// Nombre del archivo zip

					File archivocomp = new File( IEnlace.LOCAL_TMP_DIR +"/" + sFile );

					// Nombre del archivo que incluye el zip

					sFile = entry.getName();



					fos = new FileOutputStream(IEnlace.LOCAL_TMP_DIR + "/"+sFile);

					dest = new BufferedOutputStream(fos, BUFFER);

					while ((count = zis.read(data, 0, BUFFER)) != -1)

					  {

						dest.write(data, 0, count);

					  }

					dest.flush();

					/*dest.close();
					fos.close();						//MSD 20071214*/



					// Borra el archivo comprimido

					if (archivocomp.exists()) {
					  archivocomp.delete();
				  	}
					  archivocomp = null;

					log( "ImportNomina.java::Fin extraccion de: "  + entry);

				 }

			   /*zis.close();
			   buis.close();							//MSD 20071214
			   fis.close();								//MSD 20071214*/

   		     }



		} catch(Exception e) {
			EIGlobal.mensajePorTrace("ImportNomina.java::defaultAction()::try:Recibe Archivo", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} finally {

			try {

				if (dest != null){
					dest.close();							//MSD 20071214
				}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			try {

				if (fos != null){
					fos.close();							//MSD 20071214
				}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			try {

				if (zis != null){
					zis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			try {

				if (buis != null){
					buis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			try {

				if (fis != null){
					fis.close();							//MSD 20071214
				}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}

		EIGlobal.mensajePorTrace("***ImportNomina.class After SchumiCode 01", EIGlobal.NivelLog.INFO);

		//de la ruta del archivo, se obtiene solo el nombre

		nombreOriginal = nombreOriginal.substring( nombreOriginal.lastIndexOf( '\\' ) + 1 );
		EIGlobal.mensajePorTrace("***ImportNomina.class After SchumiCode 02", EIGlobal.NivelLog.INFO);
		originalName   = nombreOriginal;

		EIGlobal.mensajePorTrace("***ImportNomina.class & nombreOriginalantes>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);

		//cuando el archivo sea importado, se eliminara su extension

		nombreOriginal= nombreOriginal.substring(0,nombreOriginal.length()-4);



		// Para reconocer los archivos pertenecientes a este modulo se

		// les asignara la extension .nom

		//nombreOriginal = nombreOriginal + ".nom";

		//32 caracteres.


		Calendar cal = Calendar.getInstance();

		nombreOriginal = session.getContractNumber() + cal.getTime().getTime();
		//nombreOriginal = sessionId;

		cal = null;

	    EIGlobal.mensajePorTrace("***ImportNomina.class & nombreOriginal_ultimo ...>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);



		//String nueva_ruta= "/t/tuxedo/procesos/enlace/internet/archivos_internet/";

		//nomArchNomina  = "" + nueva_ruta + nombreOriginal;



		nomArchNomina  = IEnlace.DOWNLOAD_PATH + nombreOriginal;

		EIGlobal.mensajePorTrace("***ImportNomina.class & nombre asigando >"+nomArchNomina ,EIGlobal.NivelLog.INFO);




		//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

		RandomAccessFile archivoNomina = null;
		//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin




		//Si ya existe el archivo lo borra

		try{

		        EIGlobal.mensajePorTrace("***ImportNomina.class & Si el archivo existe, se renombra.", EIGlobal.NivelLog.INFO);

			// Renombra el archivo descargado

			File archivo = new File( IEnlace.LOCAL_TMP_DIR +"/"+ sFile );

			if (archivo.exists())

			 {

			   EIGlobal.mensajePorTrace("***ImportNomina.class & Se renombra a: "+nomArchNomina, EIGlobal.NivelLog.INFO);

			   archivo.renameTo(new File(nomArchNomina));



			  }

			else

			 {

			   EIGlobal.mensajePorTrace("***ImportNomina.class & El archivo no existe.", EIGlobal.NivelLog.INFO);

			   // throw file not found exception

			 }

		} catch(Exception e) {

			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %ejecutaCargaArchivo()"+e, EIGlobal.NivelLog.INFO);

		}

		ValidaCuentaContrato valCtas = null;
		try{





			EIGlobal.mensajePorTrace("***ImportNomina.class & Se abre nuevo archivo: "+nomArchNomina, EIGlobal.NivelLog.INFO);

			File pathArchivoNom	= new File( nomArchNomina );


			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio


			//RandomAccessFile archivoNomina = new RandomAccessFile( pathArchivoNom, "r" );
			archivoNomina = new RandomAccessFile( pathArchivoNom, "r" );

			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin


			tamanioArchivo = archivoNomina.length();

			sess.setAttribute("nombreArchivo",nomArchNomina);

			request.setAttribute( TIPO_ARCHIVO,"importado" );

			archivoNomina.seek(0);

			String primeralinea	= archivoNomina.readLine();

			EIGlobal.mensajePorTrace("***ImportNomina.class &primeralinea &"+primeralinea.length(), EIGlobal.NivelLog.INFO);



			String ctaAux="";



			if(errorEnZip)

			 {

			  despliegaPaginaError( " Imposible Importar el archivo, no es un archivo zip o esta da�ado",

			  PAGO_NOMINA, IMPORTANDO_ARCHIVO,S25800H, request, response );

			   EIGlobal.mensajePorTrace("***ImportNomina.class &primeralinea & Imposible Importar el archivo, no es un archivo zip o esta da�ado." , EIGlobal.NivelLog.INFO);


			   	//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

				archivoNomina.close();
				//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin


			  }

			else

			 if (primeralinea.length() !=  39)

			{

			despliegaPaginaError( " Imposible Importar el archivo seleccionado, la longitud del encabezado es incorrecto.",

			PAGO_NOMINA, IMPORTANDO_ARCHIVO,S25800H, request, response );


			//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

			archivoNomina.close();
			//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin


			}else {

					//cuenta de cargo del archivo

					if(primeralinea.length()>31)

					 {

					   cuentaCargo = primeralinea.substring( 15, 31 );

					   ctaAux=cuentaCargo.substring(0,11);

					   EIGlobal.mensajePorTrace("***ImportNomina.class & Cta Aux = "+ctaAux, EIGlobal.NivelLog.INFO);

					 }

					cuentaCargo = cuentaCargo.trim();

					EIGlobal.mensajePorTrace("***ImportNomina.class &cuentaCargo &"+cuentaCargo, EIGlobal.NivelLog.INFO);

					session.setCuentaCargo( cuentaCargo );

					fechaAplicacionArchivo = new String(primeralinea.substring(primeralinea.length() - 8,primeralinea.length()));

					EIGlobal.mensajePorTrace("***ImportNomina.class &fechaAplicacionArchivo &"+fechaAplicacionArchivo, EIGlobal.NivelLog.INFO);

					//***********************************************

					//modificaci�n para integraci�n pva 07/03/2002

			        //***********************************************

				    //llamado_servicioCtasInteg(IEnlace.MEnvio_pagos_nomina," "," "," ",session.getUserID()+".ambci",request);

					valCtas = new ValidaCuentaContrato();

					String datoscta[]	= null;

				    //datoscta			= BuscandoCtaInteg(cuentaCargo,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambci",IEnlace.MEnvio_pagos_nomina);

					datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
					  		  								  session.getUserID8(),
					  		  								  session.getUserProfile(),
					  		  								  cuentaCargo.trim(),
					  		  								  IEnlace.MEnvio_pagos_nomina);

					if(datoscta == null){

				       cuentaAsociada	= false;

					} else {

					cuentaAsociada = true;

					sess.setAttribute("descpCuentaCargo",datoscta[4]);

					EIGlobal.mensajePorTrace("***ImportNomina.class &datoscta[4] &"+datoscta[4], EIGlobal.NivelLog.INFO);

					}

					session.setCuentasArchivo("");

					sess.setAttribute("numeroEmp","");

					if (archivoNomina!=null){
						archivoNomina.close();
					}



					if ( !cuentaAsociada ) // aqui cuando la cuenta y el archivo estan mal

						{

						despliegaPaginaError( " Imposible Importar el archivo seleccionado, la cuenta  (" + cuentaCargo + ")  no existe en el contrato o no est&aacute; disponible para el servicio de n&oacute;mina.",

						PAGO_NOMINA, IMPORTANDO_ARCHIVO,S25800H, request, response );

						session.setCuentaCargo(" ");

					}else

					 {

						if(ctaAux.trim().length()!=11)

						 {

						   EIGlobal.mensajePorTrace("***ImportNomina.class & La cuenta aux empieza con espacios, mal justificada.", EIGlobal.NivelLog.INFO);

						   despliegaPaginaError( " Imposible Importar el archivo seleccionado, la cuenta de cargo no est&aacute; justificada correctamente.",PAGO_NOMINA, IMPORTANDO_ARCHIVO,S25800H, request, response );

						   session.setCuentaCargo(" ");

						 }

						else

						 {

							/*FSW Vector
							 * Modificacion para enviar horarios seleccionado en la pagina de alta
							 * */

						   //request.setAttribute("horario", horario);
						   request.setAttribute("tamanioArchivo",String.valueOf(tamanioArchivo));

						   request.setAttribute("nomArchNomina",nomArchNomina);

						   request.setAttribute("originalName",originalName);

						   evalTemplate( "/jsp/NominaReporte.jsp", request, response );

						 }

					}

				} //else != 39

		} catch(Exception e) {

			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %ejecutaCargaArchivo()"+e, EIGlobal.NivelLog.INFO);

		}



		//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
		finally{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
			}

            if(null != archivoNomina){
                try{
                	archivoNomina.close();
                }catch(IOException e){}
                archivoNomina = null;
            }
        }

		//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin



	}//fin del metodo ejecutaCargaArchivo

		/**
		 * Modificado por: Miguel Cortes Arellano
		 * Fecha: 19/12/2003
		 * Proposito: Ejecutar un servicio TUXEDO y obtener los horarios premier para la presente session
		 * Empieza codigo SINAPSIS
		 * @param request HttpServletRequest
		 * @param response HttpServletResponse
		 * @return String Resultado
		 */
		private String obtenHorarioPremier( HttpServletRequest request, HttpServletResponse response )
		{
				HttpSession sess = request.getSession();

				BaseResource session = (BaseResource) sess.getAttribute(SESSION);

				String usuario=session.getUserID8(), contrato=session.getContractNumber(),clavePerfil=session.getUserProfile();

				String trama_entrada="1EWEB|"+usuario+"|NOCH|"+contrato+"|"+usuario+"|"+clavePerfil+"|"+contrato+"@";

				String CodError = "";
				Hashtable htResult = null;
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				//*********************
				//Se llama al servicio de tuxedo
				try {
					htResult = tuxGlobal.web_red( trama_entrada );
				}
				catch ( java.rmi.RemoteException re ) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				}
				catch (Exception e) {}
				CodError = ( String ) htResult.get( "BUFFER" );

				EIGlobal.mensajePorTrace("Despues de llamar a WEB_RED, cod error:"+CodError, EIGlobal.NivelLog.DEBUG);

				//CODIGOERROR@DESCERROR@HORARIOS SEPARADOS POR PIPES@
				String estatus_transacion = CodError.substring(0,posCar(CodError,'@',1));
				String Horario = CodError.substring((posCar(CodError,'@',2))+1,posCar(CodError,'@',3));
				if(NOMI0000.equals(estatus_transacion)) {
					return NOMI0000+" "+Horario;
				} else {
					return "Existe un error en la obtencion de horarios por favor trate mas tarde!!!";
				}

		}

			/**
			 * Metodo posCar que devuelve la posicion de un caracter en un String
			 * @param cad Cadena de entrada
			 * @param car Caracter buscado
			 * @param cant Cantidad de caracter
			 * @return int Resultado de posicion
			 */
			public int posCar(String cad,char car,int cant) {

				int result=0,pos=0;

				for (int i=0;i<cant;i++) {

					result=cad.indexOf(car,pos);

					pos=result+1;

				}

				return result;

			}
		//Finaliza codigo SINAPSIS

			/**
			 * Metodo para validar archivo
			 * @param request HttpServletRequest
			 * @param response HttpServletResponse
			 * @throws IOException Excepcion lanzada
			 * @throws ServletException Excepcion lanzada
			 */
	 public void ejecutaValidaArchivo( HttpServletRequest request, HttpServletResponse response )

			throws IOException, ServletException {

			EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo ejecutaValidaArchivo: &", EIGlobal.NivelLog.INFO);

			HttpSession sess = request.getSession();

		    BaseResource session = (BaseResource) sess.getAttribute(SESSION);


		    //***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

			//ValidaArchivoPagos validaArchivo;
			ValidaArchivoPagos validaArchivo = null;
			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin



			String nomArchNomina		= getFormParameter(request,"nomArchNomina");

			/*FSW Vector*/
			String horario = null; //getFormParameter(request, "horario");

			nomArchNomina = nomArchNomina.trim();

			//validaArchivo				= new ValidaArchivoPagos( this, nomArchNomina );

			Vector erroresEnElArchivo	= new Vector();

			erroresEnElArchivo.addElement("<table>");

			boolean archivoValido		= true;

			Vector cantidadRegistros    = new Vector();

			String[][] arrayDetails;

			StringBuffer contenidoArchivoStr	= new StringBuffer("");

			String originalName		= getFormParameter(request, "fileName");//request.getParameter("originalName");

			EIGlobal.mensajePorTrace("***ImportNomina.class &originalName: prueba 3 &"+originalName, EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("***ImportNomina.class &nomArchNomina: &"+nomArchNomina, EIGlobal.NivelLog.INFO);

			ArchivoRemoto archR;

			archR				   = new ArchivoRemoto();

			/**
			*		Modificado por Miguel Cortes Arellano
			*		Fecha: 23/12/03
			*		Proposito: Variables usadas para la obtencion del horario
			*		Empieza codigo SINAPSIS
			*/
			String HorarioPremier = "";
			String tempHorarioPremier = "";
			StringBuffer SelectHorarioPremier = new StringBuffer("");
			StringTokenizer getHorario;
			String Horario = "";
			String FacultadPremier = "";
			FacultadPremier =(String)request.getAttribute("facultades");
			//Finaliza codigo SINAPSIS

			/*

			* Hasta esta linea, se ha creado el archivo( nuevo o importado ),

			* se procedera a conocer el numero de registros

			* de detalle que contiene.

			*/

			// Se invoca al metodo registrosEnArchivo() para DETERMINAR cual es la

			// CANTIDAD DE REGISTROS EXISTENTES EN EL ARCHIVO

			//File pathArchivoNom	= new File( nomArchNomina );

			//Cambiar es metodo por buffer

			//RandomAccessFile archivo = new RandomAccessFile( pathArchivoNom, "rw");

			int registros		  = 0;

			registros= registrosEnArchivo( nomArchNomina ,request, response);

			EIGlobal.mensajePorTrace("***ImportNomina.class &registros&"+registros, EIGlobal.NivelLog.INFO);

			int cuentaDeRegistros = registros;

			String numerosEmpleados[] = null;

			/*try{

				numerosEmpleados = arregloNumeroEmpleado( pathArchivoNom, registros - 2 );

			} catch(Exception e) {

			e.printStackTrace();

			}*/

			String cuentasAbono[]	  = null;

			/*try{

				cuentasAbono= arregloCuentas( pathArchivoNom, registros - 2 );

			}catch(Exception e) {

			e.printStackTrace();

			}*/

			int registrosLeidos	   = 0;

			registrosLeidos		   = cuentaDeRegistros;

			//FIN DETERMINA NUMERO DE REGISTROS

			BufferedReader arch = null;

			FileReader reader = null;


			reader  = new FileReader( nomArchNomina );
			arch	= new BufferedReader( reader );

			String registroEnc	= arch.readLine();

			String encTipoReg	= "";

			String encNumSec	= "";

			String encSentido	= "";

			String encFechGen	= "";

			String encCuenta	= "";

			String encFechApli	= "";

			validaArchivo				= new ValidaArchivoPagos( this, nomArchNomina );

			try

				{

				encTipoReg	= new String( registroEnc.substring( 0, 1 ) );

			    if ( !validaArchivo.validaDigito( encTipoReg ) ||
					!validaArchivo.validaEncTipoReg( encTipoReg ) ) {

					erroresEnElArchivo.addElement( "<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\">Error en el primer Caracter del encabezado, se esperaba 1</td>");

					archivoValido	= false;

					EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el Tipo registro encabezado&", EIGlobal.NivelLog.INFO);

				   }

				encNumSec = new String( registroEnc.substring( 1, 6 ) );

				EIGlobal.mensajePorTrace("***ImportNomina.class en encNumSec a: ("+encNumSec+")", EIGlobal.NivelLog.INFO);

				encNumSec=encNumSec.trim();

				EIGlobal.mensajePorTrace("***ImportNomina.class en encNumSec b: ("+encNumSec+")", EIGlobal.NivelLog.INFO);



				if ( !validaArchivo.validaDigito( encNumSec ) ||
					!validaArchivo.validaPrimerNumSec( encNumSec )) {

					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del encabezado, se esperaba 1</td>");

					archivoValido=false;

					EIGlobal.mensajePorTrace("***ImportNomina.class &Error en n&uacute;mero secuencial del encabezado&", EIGlobal.NivelLog.INFO);

				  }

				encSentido = new String(registroEnc.substring(6,7));

				EIGlobal.mensajePorTrace("***ImportNomina.class  encSentido:"+encSentido, EIGlobal.NivelLog.INFO);

				 if (!validaArchivo.validaLetras(encSentido) || !validaArchivo.validaSentido(encSentido)){

					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el caracter de sentido del encabezado, se esperaba E</td>");

					archivoValido	= false;

					EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el sentido del encabezado, se esperaba 'E'&", EIGlobal.NivelLog.INFO);

				  }

				 encFechGen = new String(registroEnc.substring(7,15));

  				 EIGlobal.mensajePorTrace("***ImportNomina.class &encFechGen:"+encFechGen, EIGlobal.NivelLog.INFO);

				 if (!validaArchivo.validaDigito(encFechGen) || !validaArchivo.fechaValida(encFechGen)){

					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de generacion del archivo</td>");

					archivoValido	= false;

					EIGlobal.mensajePorTrace("***ImportNomina.class &Error en la fecha de generacion del archivo&", EIGlobal.NivelLog.INFO);

				  }

				   // La cuenta ya no se verifica pues al momento de formar el arreglo de

				   // cuentas, se ha verificado que exista y que sea propia y/o de terceros

				  encCuenta = new String( registroEnc.substring( 15,31 ) );

  				  EIGlobal.mensajePorTrace("***ImportNomina.class &encCuenta:"+ encCuenta, EIGlobal.NivelLog.INFO);

				  encFechApli = new String(registroEnc.substring(registroEnc.length() - 8,registroEnc.length()));

  				  EIGlobal.mensajePorTrace("***ImportNomina.class &encFechApli:", EIGlobal.NivelLog.INFO);

				   if (!validaArchivo.validaDigito(encFechApli) || !validaArchivo.fechaValida(encFechApli)){

					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la fecha de aplicacion del archivo</td>");

					archivoValido=false;

					EIGlobal.mensajePorTrace("***ImportNomina.class &Error en la fecha de aplicacion&", EIGlobal.NivelLog.INFO);

				   }

            }

			catch(Exception e)

            {

   			 erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");

			 archivoValido=false;

			 EIGlobal.mensajePorTrace("***ImportNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);

             }

 			//EIGlobal.total_lineas_leidas(1,registros);

			session.setFechaAplicacion( encFechApli );

			String registroEncabezadoCompleto;

			registroEncabezadoCompleto = encTipoReg + encNumSec+encSentido +

			encFechGen + encCuenta + encFechApli;

			/* Detalle del archivo */

			if ( encNumSec != null ) {

				cantidadRegistros.addElement( encNumSec );

			}

			int longitudArreglo = cuentaDeRegistros - 2;

			// La longitud del arreglo esta determinada por la cantidad de

			// registros de ddetalle existentes en el archivo de pago de nomina

			if ( longitudArreglo < 0 ){
				longitudArreglo = 0;
			}

			String longitudArray = Integer.toString( longitudArreglo );

			//el metodo setNominaNumberOfRecords() devuelve el numero de registros de detalle en el archivo

			session.setNominaNumberOfRecords( longitudArray );

			arrayDetails = new String[longitudArreglo][9];

			//int longTotalDetalle;

			//longTotalDetalle = 0;

			double sumImporte       = 0;





					String registroDet		= "";  //jtg saco la declaracion de variables del for

					String tipoRegistro		= "";

					String NumeroSecuencia	= "";

					String NumeroEmpleado	= "";

					String ApellidoPaterno	= "";

					String ApellidoMaterno	= "";

					String Nombre			= "";

					String NumeroCuenta		= "";

					String Importe			= "";

					double num				= 0.0;

					int contadorLinea		= 1;

					/*************** Nomina Concepto de Archvo */
					String Concepto         = "";
					boolean registroContieneConcepto=false;
					int registrosConConcepto=0;
					Vector listaIDConceptos=getIDConceptos();
					/*************** Nomina Concepto de Archvo */

		//if ( arrayDetails.length > Global.REG_MAX_IMPORTAR_NOMINA ){



			for (int i= 0; i < arrayDetails.length; i++)

				{

			  try

                 {

				try{

					registroDet		= arch.readLine();

					if(registroDet==null){ //jtg   si es nulo no puedo obtener los datos

						continue;
					}
					if(! (registroDet.length() == 127  || registroDet.length()==129) )
					 {

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el tama�o del Registro </td>");

					    archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);

						continue;

					}// else{



					//}

					tipoRegistro	= new String ( registroDet.substring(0,1)		);

					NumeroSecuencia	= new String ( registroDet.substring(1,6)		);

					NumeroEmpleado	= new String ( registroDet.substring(6,13)		);

					ApellidoPaterno	= new String ( registroDet.substring(13,43)		);

					ApellidoMaterno	= new String ( registroDet.substring(43,63)		);

					Nombre			= new String ( registroDet.substring(63,93)		);

					NumeroCuenta	= new String ( registroDet.substring(93,109)	);

					Importe			= new String ( registroDet.substring(109,127)	);

					/*************** Nomina Concepto de Archvo */
					if(registroDet.length()>127)
					 {
					   Concepto        = new String ( registroDet.substring(127)    );
					   registrosConConcepto++;
					   registroContieneConcepto=true;
					 }
					/*************** Nomina Concepto de Archvo */


					num = Double.parseDouble(Importe);

					sumImporte = sumImporte + num;

				} catch(Exception e) {

					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				}

					arrayDetails[i][0] = tipoRegistro;

					if ( !validaArchivo.validaDigito( arrayDetails[i][0] ) ||
							!validaArchivo.validaDetalleTipoReg( arrayDetails[i][0] )) {

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el tipo de Registro del detalle</td>");

					    archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el tipo de Registro del detalle&", EIGlobal.NivelLog.INFO);

					}

					arrayDetails[i][1]=NumeroSecuencia;

					if (!validaArchivo.validaDigito(arrayDetails[i][1]) || !validaArchivo.validaNumSec(i, arrayDetails[i][1])){

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del detalle</td>");

					    archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el n&uacute;mero secuencial del detalle&", EIGlobal.NivelLog.INFO);

					}

					arrayDetails[i][2]=NumeroEmpleado;

					/*if (NumeroEmpleado.equals("") || NumeroEmpleado == null || NumeroEmpleado.equals("       ") ){



						NumeroEmpleado = " ";

						arrayDetails[i][2]=NumeroEmpleado;

					}else {

						arrayDetails[i][2]=NumeroEmpleado;

						if (validaArchivo.verificaDuplicidadNumeroEmpleado(numerosEmpleados, arrayDetails[i][2])){

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">El empleado esta duplicado</td>");

					    archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el numero de empleado&", EIGlobal.NivelLog.INFO);

						}

					}*/

					arrayDetails[i][3]=ApellidoPaterno;

					/*if (arrayDetails[i][3]!="")

						if (validaArchivo.validaNombres(arrayDetails[i][3])==false){

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Apellido Paterno</td>");

							archivoValido=false;

							EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el apellido paterno&", EIGlobal.NivelLog.INFO);

						}*/

					arrayDetails[i][4]=ApellidoMaterno;

					/*if(arrayDetails[i][4]!="")

						if (validaArchivo.validaNombres(arrayDetails[i][4])==false){

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Apellido Materno</td>");

							archivoValido=false;

							EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el apellido materno&", EIGlobal.NivelLog.INFO);

						}*/

					arrayDetails[i][5]=Nombre;

					/*if(arrayDetails[i][5]!="")

						if (validaArchivo.validaNombres(arrayDetails[i][5])==false){

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Nombre</td>");

						    archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el nombre&", EIGlobal.NivelLog.INFO);

						}*/

					arrayDetails[i][6]=NumeroCuenta;

					/* modificacion para integracion

					if (validaArchivo.validaDigito(arrayDetails[i][6])==false||validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false

							||validaArchivo.validaCuenta(arrayDetails[i][6])==false||validaArchivo.verificaDuplicidadCuentas(cuentasAbono, arrayDetails[i][6])){

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

						archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el nombre&", EIGlobal.NivelLog.INFO);

					}*/

				    //EIGlobal.mensajePorTrace("***Comienza modificacion para integracion: ", EIGlobal.NivelLog.INFO);

					/*if (validaArchivo.validaDigito(arrayDetails[i][6])==false||

						validaArchivo.validaCuenta(arrayDetails[i][6])==false||validaArchivo.verificaDuplicidadCuentas(cuentasAbono, arrayDetails[i][6]))

					{*/

					/*if (validaArchivo.validaDigito(arrayDetails[i][6])==false||	validaArchivo.validaCuenta(arrayDetails[i][6])==false)

					{

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

						archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el Numero de Cuenta&", EIGlobal.NivelLog.INFO);

					}*/
				
					if (!validaArchivo.validaCuenta(arrayDetails[i][6]))

					{
							
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero de Cuenta</td>");

						archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el n&uacute;mero de Cuenta&", EIGlobal.NivelLog.INFO);

					}
					//26-Mayo-2016 JFOL valida que el numero de cuenta sea num�rico, se crea el m�todo validaCampo, plan de calidad CSA
					else{						
						String cuentabonosinblancos= arrayDetails[i][6].trim();
						if (!validaCuentaNumerica(cuentabonosinblancos)){							
							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero de Cuenta, Debe ser num&eacute;rico</td>");	
							
							archivoValido	= false;
							EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el n&uacute;mero de Cuenta, Debe ser num&eacute;rico&", EIGlobal.NivelLog.INFO);
						}						
					}
					
					


					/*if (validaArchivo.validaCuentaDolares(arrayDetails[i][6])==false)

					{

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error el Numero de Cuenta es en dolares</td>");

						archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error el Numero de Cuenta es de dolares&", EIGlobal.NivelLog.INFO);

					}*/

					/*if ( (session.getClaveBanco().equals(Global.CLAVE_SANTANDER))&&

						 (validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false))

                    {

						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

						archivoValido=false;

						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el Numero de Cuenta&", EIGlobal.NivelLog.INFO);

                    }

                    if  (session.getClaveBanco().equals(Global.CLAVE_SERFIN))

                    {

                        if (validaArchivo.validaDigitoCuenta(arrayDetails[i][6])==false)

                        {

							if (validaDigitoCuentaHogan(arrayDetails[i][6])==false)

							{

                               erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Numero de Cuenta</td>");

					           archivoValido=false;

						       EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el numero de cuenta&", EIGlobal.NivelLog.INFO);

                            }

                        }

                    }*/

			        //EIGlobal.mensajePorTrace("***termina modificacion para integracion: ", EIGlobal.NivelLog.INFO);



                    //******fin de modificacion para integracion


					/*************** Nomina Concepto de Archvo */
					if (!validaArchivo.validaDigito(Concepto) ||
						  (registroContieneConcepto && "".equals(Concepto.trim()))
					    )
					{
						erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Identificador no v&aacute;lido en el campo Concepto.</td>");
						archivoValido=false;
						EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el n&uacute;mero de Concepto&", EIGlobal.NivelLog.INFO);
					}
				   else/* Validacion en archivo  */
				    if(listaIDConceptos!=null && Concepto.trim().length()>0 && listaIDConceptos.indexOf(Concepto)<0)
				      {
					    EIGlobal.mensajePorTrace("***ImportNomina.class & El Concepto no esta registrado.", EIGlobal.NivelLog.INFO);
					    erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">La clave del Concepto no est&aacute; Registrada.</td>");
					    archivoValido=false;
					   }


				    /*************** Nomina Concepto de Archvo */


					arrayDetails[i][7]=Importe;

					//if (validaArchivo.validaDigito(arrayDetails[i][7])==false){

					if (!validaArchivo.validaDigito(arrayDetails[i][7]) || arrayDetails[i][7].endsWith(" "))

					 {

					   erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe</td>");

					   archivoValido=false;

					   //EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el importe&", EIGlobal.NivelLog.INFO);

					 }

					else

					 {

						String ctaAux1="";

						ctaAux1=arrayDetails[i][6].substring(0,11);

						//EIGlobal.mensajePorTrace("***ImportNomina.class &Cuenta auxiliar abono = "+ctaAux1, EIGlobal.NivelLog.DEBUG);

						if(ctaAux1.trim().length()!=11 && ctaAux1.trim().length()!=10)

						 {

							erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la Cuenta, no est&aacute; justificada correctamente.</td>");

							archivoValido=false;

							EIGlobal.mensajePorTrace("***ImportNomina.class &Error en la cuenta no esta justificada correctamente&", EIGlobal.NivelLog.DEBUG);

						 }

					 }



					if (Importe.trim().length() == 0){

					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Debe especificar importe</td>");

					archivoValido=false;

					}

					cantidadRegistros.addElement(arrayDetails[i][1]);

					String registroDetalleCompleto;

					registroDetalleCompleto=arrayDetails[i][0]+arrayDetails[i][1]+arrayDetails[i][2]+arrayDetails[i][3]+arrayDetails[i][4]+arrayDetails[i][5]+arrayDetails[i][6]+arrayDetails[i][7];

					//longTotalDetalle= registroDetalleCompleto.length();

		  } catch(Exception e) {
    		  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

   			//erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea "+(i+2)+"</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato en el archivo � un campo esta vacio</td>");

			archivoValido=false;

			EIGlobal.mensajePorTrace("***ImportNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.DEBUG);

		      }

			contadorLinea++;

			if((contadorLinea%1000)==0) {
			  EIGlobal.mensajePorTrace("***ImportNomina.class Importando el registro ### "+contadorLinea, EIGlobal.NivelLog.INFO);
			}

			//EIGlobal.mensajePorTrace("***ImportNomina.class &# de l�nea><<>><<"+contadorLinea + "><<>><<", EIGlobal.NivelLog.INFO);

		}//del for

				double sumTotal = 0 ;

				//Registro Sumario

				String registroSum = arch.readLine();

				if(registroSum != null) {
					EIGlobal.mensajePorTrace("***ImportNomina.class &registroSum&"+registroSum.length(), EIGlobal.NivelLog.DEBUG);
				}
				else {
					EIGlobal.mensajePorTrace("***ImportNomina.class &registroSum& viene nulo.", EIGlobal.NivelLog.ERROR);
				}

				String sumTipoReg  = "";

				String sumNumSec   = "";

				String sumTotRegs  = "";

				String sumImpTotal = "";



                try

				{

					if (registroSum == null || registroSum.length() != 29)

					{

					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en la longitud del sumario</td>");

				    archivoValido=false;

					EIGlobal.mensajePorTrace("***ImportNomina.class &Error en la longitud del sumario&", EIGlobal.NivelLog.INFO);



					}else {

						sumTipoReg=new String(registroSum.substring(0,1));

					    if (!validaArchivo.validaDigito(sumTipoReg) || !validaArchivo.validaSumTipoReg(sumTipoReg)){

						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero de registro</td>");

						  archivoValido=false;

						  EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el n&uacute;mero de registro del sumario&", EIGlobal.NivelLog.INFO);

					    }

						sumNumSec= new String(registroSum.substring(1,6));

					    if(validaArchivo.validaDigito(sumNumSec) && validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)){

						 session.setLastSecNom(sumNumSec);

					    }

					    if (!validaArchivo.validaDigito(sumNumSec) || !validaArchivo.validaUltimoNumeroSecuencial(sumNumSec,registrosLeidos)){

						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el n&uacute;mero secuencial del sumario</td>");

						  archivoValido=false;

						  EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el n&uacute;mero secuencial del sumario&", EIGlobal.NivelLog.INFO);

					    }

					    sumTotRegs = new String(registroSum.substring(6,11));

					    if(validaArchivo.validaDigito(sumTotRegs) && validaArchivo.validaTotalDeRegistros(sumTotRegs, cuentaDeRegistros)){

						  session.setTotalRegsNom(sumTotRegs);

						}

					    if (!validaArchivo.validaDigito(sumTotRegs) || !validaArchivo.validaTotalDeRegistros(sumTotRegs, cuentaDeRegistros) ){

						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el total de registros del sumario</td>");

						  archivoValido=false;


						  EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el total de registros&", EIGlobal.NivelLog.INFO);

						}

					    sumImpTotal = new String(registroSum.substring(11,29));

					    if (validaArchivo.validaDigito(sumImpTotal)  ){

						  session.setImpTotalNom(sumImpTotal);

					    }

					    validaArchivo.cerrar();


						//***** everis se Comenta el valor que se asigna a validaArchivo  RandomAccessFile 08/05/2008  ..inicio
					    //validaArchivo = null;
						//***** everis se Comenta el valor que se asigna a validaArchivo  RandomAccessFile 08/05/2008  ..fin




						/*************** Nomina Concepto de Archvo */
						EIGlobal.mensajePorTrace("***ImportNomina.class &Total de Registros en Archivo: "+session.getNominaNumberOfRecords(), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("***ImportNomina.class &Total de Registros con Concepto: "+registrosConConcepto, EIGlobal.NivelLog.INFO);
						if(registrosConConcepto==0)
						 {
							if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
								request.getSession().removeAttribute("conceptoEnArchivo");
							request.getSession().setAttribute("conceptoEnArchivo","false");
						 }
						if(registrosConConcepto!=0 && registrosConConcepto!= (Integer.parseInt(session.getNominaNumberOfRecords())) )
						 {
							 erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Error: </td><td class=\"textabdatobs\" nowrap align=\"center\"> Si los registros incluyen Concepto debe especificarlo en todos.</td>");
							 archivoValido=false;
							 EIGlobal.mensajePorTrace("***ImportNomina.class &Si los registros incluyen concepto debe especificarlo en todos.&", EIGlobal.NivelLog.INFO);
						 }
						/*************** Nomina Concepto de Archvo */


						sumTotal = Double.parseDouble(sumImpTotal);

						/* Suma de los importes */

						if (sumTotal != sumImporte){

						  erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea Final</td><td class=\"textabdatobs\" nowrap align=\"center\">Error en el Importe total </td>");

						  archivoValido=false;

						  EIGlobal.mensajePorTrace("***ImportNomina.class &Error en el Importe total&", EIGlobal.NivelLog.INFO);

						}

						} //else != 29

		            }

   					catch(Exception e)

				    {

   					erroresEnElArchivo.addElement("<tr><td class=\"textabdatobs\" nowrap align=\"center\">Linea 1</td><td class=\"textabdatobs\" nowrap align=\"center\">Error de formato  en el archivo</td>");

					archivoValido=false;

					EIGlobal.mensajePorTrace("***ImportNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);

					}


					//***** everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..inicio
					finally{
			            if(null != validaArchivo){
			                try{
			                	validaArchivo.cerrar();
			                }catch(Exception e){}
			                validaArchivo = null;
			            }
					}

					//****  everis Cerrando RandomAccessFile -- ValidaArchivoPagos 08/05/2008  ..fin


				erroresEnElArchivo.addElement("</table>");

				erroresEnElArchivo.trimToSize();//a partir de esta linea el vector erroresEnElArchivo contiene la tabla de errores

				String registroSumarioCompleto;

				registroSumarioCompleto=sumTipoReg+sumNumSec+sumTotRegs+sumImpTotal;

				int longTotalSumario;

				longTotalSumario = registroSumarioCompleto.length();

				if (arch!=null){

					arch.close();
				}



				cantidadRegistros.addElement(sumNumSec);

				int total;

				total = cantidadRegistros.size();

				// Aqui finaliza la lectura y validacion de los campos del archivo

				// es necesario determinar si se exhibiran o no los registros de detalle, solo se exhibiran cuando sean menos de

				// 30 registros y estos sean validos

				//if(Integer.parseInt(session.getNominaNumberOfRecords())<=30){

			/*}//del if

			   else {

			        despliegaPaginaError( "Imposible Importar el archivo seleccionado, tiene mas de"+  Global.REG_MAX_IMPORTAR_NOMINA + " registros.",

					"Pago de N&oacute;mina", "Importando Archivo","s25800h", request, response );

			        archivoValido=false;

					}*/



				if (archivoValido)
				 {
					contenidoArchivoStr=new StringBuffer("");
					contenidoArchivoStr.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
					contenidoArchivoStr.append("<tr> ");
					contenidoArchivoStr.append("<td class=\"textabref\">Pago de n&oacute;mina</td>");
					contenidoArchivoStr.append("</tr>");
					contenidoArchivoStr.append("</table>");
					contenidoArchivoStr.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
					contenidoArchivoStr.append("<tr> ");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descripci&oacute;n</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
					contenidoArchivoStr.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
					contenidoArchivoStr.append("</tr>");

						int contador=1;

						int residuo=0;

						residuo=contador % 2 ;

						String bgcolor="textabdatobs";

						if (residuo == 0){

							bgcolor="textabdatcla";

						}

						//10272002

						/**
						*		Modificado por Miguel Cortes Arellano
						*		Fecha: 19/12/03
						*		Proposito: Crear dinamicamente los horarios premier dependiendo del usuario de la presente session.
						*		Empieza codigo SINAPSIS
						*/

						String encFechApliformato = "";
						if (encFechApli.length() >= 8 )
							encFechApliformato = encFechApli.substring(2,4) + "/" + encFechApli.substring(0,2) + "/" + encFechApli.substring(4,8);
						else
							encFechApliformato = encFechApli;

						/*FSW Vector*/
						if(horario != null ){

							SelectHorarioPremier.append(horario);
							SelectHorarioPremier.append("<input type =\"hidden\" name =\"HorarioPremier\" value \""+ horario+"\" />");


						}else if(FacultadPremier.indexOf("PremierVerdadero") != -1)
						{
							HorarioPremier = obtenHorarioPremier(request, response);//Llama servicio tuxedo para obtener horarios premier. Un string separados por PIPES (|)
							if(HorarioPremier.indexOf(NOMI0000)!= -1){
									tempHorarioPremier = HorarioPremier.substring(9);
									getHorario = new StringTokenizer(tempHorarioPremier,"|");
									SelectHorarioPremier.append("<SELECT NAME=HorarioPremier><BR>");
									SelectHorarioPremier.append("<OPTION VALUE=\"\" SELECTED></OPTION><BR>");

									/*************************************************************************
									  Validacion de la fecha de aplicacion para determinar
									  horarios de fin de semana
									*************************************************************************/
									EIGlobal.mensajePorTrace("***ImportNomina.class Validando el dia de aplicacion", EIGlobal.NivelLog.INFO);

									GregorianCalendar fechaAplicacion=new GregorianCalendar();
									fechaAplicacion=getFechaString(encFechApliformato);

									EIGlobal.mensajePorTrace("***ImportNomina.class Dia N�mero de aplicacion: " + fechaAplicacion.get(Calendar.DAY_OF_WEEK), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***ImportNomina.class Mes de aplicacion: " + fechaAplicacion.get(Calendar.MONTH), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***ImportNomina.class Dia de aplicacion: " + fechaAplicacion.get(Calendar.DATE), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***ImportNomina.class A�o de aplicacion: " + fechaAplicacion.get(Calendar.YEAR), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("***ImportNomina.class tempHorarioPremier: " + tempHorarioPremier, EIGlobal.NivelLog.INFO);


									if(sess.getAttribute("HOR_SAT")!=null)
										sess.removeAttribute("HOR_SAT");

									if( fechaAplicacion.get(Calendar.DAY_OF_WEEK)==7)
									 {
										SelectHorarioPremier.append("<option VALUE=06:00>06:00</option><br>");
										SelectHorarioPremier.append("<option VALUE=11:00>11:00</option><br>");
										tempHorarioPremier="06:00|11:00|";
									 }
									else
								     {
										while (getHorario.hasMoreTokens())
										{//Split el ID y el horario que se obtiene en el primer elemento separado por Pipes(|). El segundo elemento viene separado por ;
											Horario = getHorario.nextToken();
											if(!Horario.equals("18:30")){
												SelectHorarioPremier.append("<OPTION VALUE=");
												SelectHorarioPremier.append(Horario);
												SelectHorarioPremier.append(">");
												SelectHorarioPremier.append(Horario);
												SelectHorarioPremier.append("</OPTION><BR>");
											}
										}
									 }
									/*************************************************************************/


									SelectHorarioPremier.append("</SELECT><BR>");
									EIGlobal.mensajePorTrace("***ImportNomina.class Nuevos tempHorarioPremier: " + tempHorarioPremier, EIGlobal.NivelLog.INFO);
									//Se inicializa una variable de session la cual mantiene los horarios pertinentes
									sess.setAttribute("SelectHorarioPremier",tempHorarioPremier);
							}
						}
						// Finaliza Codigo Sinapsis
						String sumTmp = sumImpTotal.substring(0, sumImpTotal.length() - 2) + "." + sumImpTotal.substring(sumImpTotal.length() - 2);
						//String importe1 = FormatoMoneda.formateaMoneda(new Double(sumImpTotal).doubleValue());
					    sess.setAttribute("sumTmpSession", Double.parseDouble(sumTmp.trim()));
						String importe1 = FormatoMoneda.formateaMoneda(new Double(sumTmp).doubleValue());
			    		contenidoArchivoStr.append("<tr><td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(session.getCuentaCargo() );
						contenidoArchivoStr.append("&nbsp;</td>");

			    		contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(sess.getAttribute("descpCuentaCargo") );
						contenidoArchivoStr.append("&nbsp;</td>");

					    contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(encFechApliformato );
						contenidoArchivoStr.append("&nbsp;</td>");

						contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(importe1);
						contenidoArchivoStr.append("&nbsp;</td>");

						contenidoArchivoStr.append("<td class=\"" );
						contenidoArchivoStr.append(bgcolor );
						contenidoArchivoStr.append("\" nowrap align=\"center\">");
						contenidoArchivoStr.append(session.getNominaNumberOfRecords());
						contenidoArchivoStr.append("&nbsp;</td>");

						contenidoArchivoStr.append("</tr>");

						contenidoArchivoStr.append("<tr><td width=\"40\" valign=top >&nbsp;</td><td valign=middle width=\"120\"  class=textabdatcla>Folio:&nbsp;<INPUT TYPE=text SIZE=15 NAME=folio></td>");

						contenidoArchivoStr.append("<td width=\"80\"  class=textabdatcla>");
						/**
						*		Modificado por Miguel Cortes Arellano
						*		Fecha: 19/12/03
						*		Proposito: Crear dinamicamente los horarios premier dependiendo del usuario de la presente session.
						*		Empieza codigo SINAPSIS
						*/

						/*FSW Vector Inicio*/
						if(horario !=null){
							contenidoArchivoStr.append("<center>Hora de aplicaci&oacute;n");

							contenidoArchivoStr.append(SelectHorarioPremier.toString());

							contenidoArchivoStr.append("</center></td><td colspan=2 valign=top width=\"160\"  class=textabdatcla>Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n");

						}/*FSW Vector*/
						if(HorarioPremier.indexOf(NOMI0000)!= -1&&FacultadPremier.indexOf("PremierVerdadero") != -1)
					    {
							contenidoArchivoStr.append("<center>Hora de aplicaci&oacute;n");

							contenidoArchivoStr.append(SelectHorarioPremier.toString());

							contenidoArchivoStr.append("</center></td><td colspan=2 valign=top width=\"160\"  class=textabdatcla>Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n");
				        }
						//Finaliza Codigo SINAPSIS
						contenidoArchivoStr.append("</td></tr>");

						/*************** Nomina Concepto de Archvo */

						if(!registroContieneConcepto)
					     {
							contenidoArchivoStr.append("		<tr>");
							contenidoArchivoStr.append("		 <td ></td>");
							contenidoArchivoStr.append("		 <td class='textabdatobs' align=center> Concepto de Pago de Archivo</td>");
							contenidoArchivoStr.append("		 <td class='textabdatobs' colspan=3>");
							contenidoArchivoStr.append("		   <select name=Concepto class='textabdatobs'>");
							contenidoArchivoStr.append(getConceptos());
							contenidoArchivoStr.append("		   </select>");
							contenidoArchivoStr.append("		 </td>");
							contenidoArchivoStr.append("		</tr>");

						 }

						/*FSW Vector Inicio*/

						/*************** Nomina Favoritos */

						contenidoArchivoStr.append("		<tr>");
						contenidoArchivoStr.append("		 <td ></td>");
						contenidoArchivoStr.append("		 <td class='textabdatobs' align=center>Si desea agregar a favoritos marque esta casilla</td>");
						contenidoArchivoStr.append("		 <td class='textabdatobs' colspan=3>");
						contenidoArchivoStr.append("		   <input type=\"checkbox\" name=\"chkBoxFav\" id=\"chkBoxFav\" onclick=\"javascript:seleccionarFavoritoArch(this);\"/>");
						contenidoArchivoStr.append("		   <input type=\"text\" disabled=\"true\" value=\"\" name=\"txtFav\" id=\"txtFav\" size=\"20\" onblur=\"validaEspecialesFavorito(this,'Favoritos');\" />");
						contenidoArchivoStr.append("<input type=\"hidden\" name=\"strCheck\" id=\"strCheck\" />");
						contenidoArchivoStr.append("		 </td>");
						contenidoArchivoStr.append("		</tr>");

						/*FSW Vector Fin*/

						/**
						 * INICIO PYME 2015 Se comenta codigo por la unificacion de token
						 * para que no muestre boton Enviar Regresar, ahora se
						 * utilizarian los del token
						 */
						contenidoArchivoStr.append("</table></td></tr>");

//						contenidoArchivoStr.append("<br><table align=center border=0 cellspacing=0 cellpadding=0>");
//
//						if( session.getToken().getStatus() == 1 ){
//							contenidoArchivoStr.append("<tr><td></td><td id=\"enviar\"><a href =javascript:js_envioConOTP(); border = 0><img src = /gifs/EnlaceMig/gbo25520.gif border=0 alt=Envio></a></td>");
//						} else {
//							contenidoArchivoStr.append("<tr><td></td><td id=\"enviar\"><a href = javascript:js_envio(); border = 0><img src = /gifs/EnlaceMig/gbo25520.gif border=0 alt=Envio></a></td>");
//						}
//
//						contenidoArchivoStr.append("<td><A href = javascript:js_regresarPagosNv(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td></tr>" +
//								"<tr id=\"enviar2\" style=\"visibility:hidden\" class=\"tabmovtex\">" +
//								"<td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td><tr/>" +
//								"</table></td></tr>");

						/**
						 * INICIO PYME 2015 Se comenta codigo por la unificacion de token
						 */

				} // ArchvioValido



					EIGlobal.mensajePorTrace("***ImportNomina.class 1&El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);



			String descargaArchivo = "";



/*			try {

				EIGlobal.mensajePorTrace("***ImportNomina.class &El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);

				if ( originalName.substring( originalName.length() - 4,

						originalName.length() - 3 ).equals( "." ) )

			        originalName = originalName.substring( 0, originalName.length() - 4 ) + ".nom";

				else

					originalName = originalName + ".nom";



			} catch( Exception e ) {

				EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion al agregar extension al archivo: %execute()"+e, EIGlobal.NivelLog.INFO);

				originalName = originalName+".nom";

			}

			EIGlobal.mensajePorTrace("***ImportNomina.class &El archivo es importado, imposible ejecutar operaciones en el&", EIGlobal.NivelLog.INFO);

			descargaArchivo="document.location='"+"/Download/" + originalName + "'";*/

			String archivoName = "";

				archivoName = (String) sess.getAttribute("nombreArchivo");

			//***********************************************

			//modificaci�n para integraci�n pva 07/03/2002

            //***********************************************

	         session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);

            //***********************************************



	//	if (bandera)

	//	{



			if ( archivoValido ) {

				/*		if ( archR.copiaLocalARemoto( archivoName.substring(archivoName.lastIndexOf("/") + 1 ), "WEB" ) ) {

							session.setRutaDescarga( "/Download/" + ( archivoName.substring(archivoName.lastIndexOf("/") + 1 ) ) );

						} else {



						}*/

						request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
						sess.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));

						request.setAttribute(CONTENIDO_ARCHIVO,""+contenidoArchivoStr.toString());
						sess.setAttribute(CONTENIDO_ARCHIVO,""+contenidoArchivoStr.toString());
						//request.setAttribute("DescargaArchivo",session.getRutaDescarga());

						request.setAttribute(EN_FECHA_APLIC,encFechApli);
						sess.setAttribute(EN_FECHA_APLIC,encFechApli);
						String msg="";
						/**
						*		Modificado por Miguel Cortes Arellano
						*		Fecha: 23/12/03
						*		Proposito: Enviar un mensaje de error al usuario en caso de que tenga facultades de cliente Premier y no se obtenga los horarios.
						*		Empieza codigo SINAPSIS
						*/
						if(FacultadPremier.indexOf("PremierVerdadero") != -1)
					    {
							if(HorarioPremier.indexOf(NOMI0000)!= -1)
								msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros\", 1)";
							else
								msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros. Pero, no se obtuvieron los Horarios por favor trate mas tarde.\", 1)";
				        }else{
								msg = "cuadroDialogo(\"Se importaron exitosamente: "+session.getNominaNumberOfRecords()+" registros\", 1)";
						}
						//Finaliza Codigo SINAPSIS


						request.setAttribute("infoImportados",msg);

						request.setAttribute("cantidadDeRegistrosEnArchivo",session.getNominaNumberOfRecords());

						request.setAttribute( ENCABEZADO, CreaEncabezado(PAGO_NOMINA,ENCABEZADO_NOMINA,"s25800IIh",request));
						sess.setAttribute( ENCABEZADO, CreaEncabezado(PAGO_NOMINA,ENCABEZADO_NOMINA,"s25800IIh",request));


				//		EIGlobal.total_lineas_leidas(registros,registros);
						/**
						 * SE COMENTA ESTA LINEA PARA QUE CONTINUE Y SE VAYA A  VALIDAR EL TOKEN
						 * INCIO PYME 2015 UNIFICACION DE TOKEN, AHORA SE REDIRECCIONA A
						 * OpcionesNomina
						 */
						EIGlobal.mensajePorTrace("Redireccionando a OpcionesNomina - execute()", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("1 === PYME", EIGlobal.NivelLog.INFO);
						request.setAttribute("operacion", "envio");
						request.setAttribute(TIPO_ARCHIVO, "enviado");
						RequestDispatcher rdSalida = getServletContext()
						.getRequestDispatcher("/enlaceMig/OpcionesNomina");
						EIGlobal.mensajePorTrace("valida-> " + (getFormParameter(request, "valida") == null ? request.getAttribute("valida") : getFormParameter(request, "valida") ), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("operacion-> " + (getFormParameter(request, OPERACION) == null ? request.getAttribute(OPERACION) : getFormParameter(request, OPERACION) ), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("registro-> " + (getFormParameter(request, "registro") == null ? request.getAttribute("registro") : getFormParameter(request, "registro") ), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("tipoArchivo-> " + (getFormParameter(request, TIPO_ARCHIVO) == null ? request.getAttribute(TIPO_ARCHIVO) : getFormParameter(request, TIPO_ARCHIVO) ), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("statusDuplicado-> " + (getFormParameter(request, "statusDuplicado") == null ? request.getAttribute("statusDuplicado") : getFormParameter(request, "statusDuplicado") ), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("statushrc-> " + (getFormParameter(request, "statushrc") == null ? request.getAttribute("statushrc") : getFormParameter(request, "statushrc") ), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("encFechApli-> " + (getFormParameter(request, EN_FECHA_APLIC) == null ? request.getAttribute(EN_FECHA_APLIC) : getFormParameter(request, EN_FECHA_APLIC) ), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("horario_seleccionado-> " + (getFormParameter(request, "horario_seleccionado") == null ? request.getAttribute("horario_seleccionado") : getFormParameter(request, "horario_seleccionado") ), EIGlobal.NivelLog.DEBUG);
						rdSalida.forward( request , response );
//						evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response );
						/**
						 * FIN PYME 2015 UNIFICACION DE TOKEN
						 */
			} else {

				String msg="";

				String tablaErrores="";

				for(int z=0; z<erroresEnElArchivo.size(); z++){

					tablaErrores=tablaErrores+erroresEnElArchivo.elementAt(z);

				}

				request.setAttribute("archivoEstatus",tablaErrores);

				request.setAttribute("ArchivoErr","archivoErrores();");

				request.setAttribute("archivo_actual","Error al Importar: "+archivoName.substring(archivoName.lastIndexOf("/")+1));

				request.setAttribute("Cuentas","<Option value = \"\">Error en el archivo");

				request.setAttribute(CONTENIDO_ARCHIVO,"");

				request.setAttribute(TIPO_ARCHIVO,"importadoErroneo");

				request.setAttribute("cantidadDeRegistrosEnArchivo","");

				//EIGlobal.total_lineas_leidas(registros,registros);
				/*FSW - Vector Redirigir a pagina inicial*/
				sess.setAttribute("archivoEstatus",tablaErrores);
				sess.setAttribute("ArchivoErr","archivoErrores();");
				String url =PAG_TMPL_SRV_LIN + "?modulo=3&submodulo=2&accion=iniciar";

				response.sendRedirect(url);

				//evalTemplate(PAG_TMPL_SRV_LIN, request, response );
				/*FSW Vector - Fin*/
			}

	//	  } // bandera

	}//ejecutaimportar



	// M�todo para conocer la cantidad de registros existentes en un archivo.

	//public int registrosEnArchivo(RandomAccessFile file){



 	/**
 	 * Metodo para contar registros en el archivo
 	 * @param pathArchivoNom Path del archivo
 	 * @param request HttpServletRequest
 	 * @param response HttpServletResponse
 	 * @return int Numero de registros
 	 * @throws IOException Excepcion lanzada
 	 * @throws ServletException Excepcion lanzada
 	 */
 	public int registrosEnArchivo(String pathArchivoNom, HttpServletRequest request, HttpServletResponse response )

			throws IOException, ServletException {

		EIGlobal.mensajePorTrace( "***ImportNomina.class &Entrando al metodo registrosEnArchivo 35&", EIGlobal.NivelLog.INFO);

		pathArchivoNom = pathArchivoNom.trim();

		BufferedReader arch = null;
		FileReader reader = null;

		reader  = new FileReader( pathArchivoNom );
		arch	= new BufferedReader( reader );

		String registroLeido = "";

		long posicion		 = 0;

		int cantRegistros    = 0;

		try {

				do {

					registroLeido = arch.readLine();

					if (registroLeido != null)

						cantRegistros++;

					} while( registroLeido != null );

				//} while( posicion < file.length() );

		} catch(IllegalArgumentException e) {

			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(1)"+e, EIGlobal.NivelLog.INFO);

		} catch(FileNotFoundException e) {

			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(2)"+e, EIGlobal.NivelLog.INFO);

		} catch(SecurityException e){

			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(3)"+e, EIGlobal.NivelLog.INFO);

		} catch(Exception e) {

			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %registrosEnArchivo(4)"+e, EIGlobal.NivelLog.INFO);

		}


		//***** everis Cerrando BufferedReader 08/05/2008  ..inicio
		finally {

			try {

		if (arch!=null){
			arch.close();
		}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			try {

		if(reader != null) {
			reader.close();
		}
			}catch(IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}

		}

		//****  everis Cerrando BufferedReader 08/05/2008  ..fin

		EIGlobal.mensajePorTrace("***registrosEnArchivo() cantRegistros: "+cantRegistros, EIGlobal.NivelLog.INFO);

		return cantRegistros;

	}


	/**
	 * METODO PARA AGRGAR PUNTO	DECIMAL	AL IMPORTE
	 * @param importe Importe de entrada
	 * @return String Cadena resultado
	 */
	public String  agregarPunto(String importe){

		 EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo agregarPunto()&", EIGlobal.NivelLog.INFO);

         String importeConPunto	= "";

         String cadena1			= importe.substring(0,importe.length()-2);

         cadena1				= cadena1.trim();

         String cadena2			= importe.substring(importe.length()-2, importe.length());

         importe				= cadena1+" "+cadena2;

         importeConPunto		= importe.replace(' ','.');

		 if(importeConPunto.startsWith(".."))

			 importeConPunto	= ".0"+cadena2.trim();

		 EIGlobal.mensajePorTrace("***ImportNomina.class &Saliendo del metodo agregarPunto()&", EIGlobal.NivelLog.INFO);

         return importeConPunto;

     }



	/**
	 * METODO PARA FORMATEAR EL IMPORTE CUANDO CONTENGA DECIMALES
	 * @param importe Importe de entrada
	 * @return String Importe resultado
	 */
	public String formateaImporte(String importe){

		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo formateaImporte()&", EIGlobal.NivelLog.INFO);

		String importeFormateado	= "";

		boolean contienePunto		= false;

		int i=0;

		for (i=0; i<importe.length(); i++){

			if(importe.charAt(i)=='.'){

				contienePunto=true;

				break;

			}

		}

		if(contienePunto)

			importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());

		else

			importeFormateado=importe;

		EIGlobal.mensajePorTrace("***ImportNomina.class &Saliendo del metodo formateaImporte()&", EIGlobal.NivelLog.INFO);

		return importeFormateado;

	}


	/**
	 * Metodo para visualizar los errores en una pagina html
	 * archivoValido=false; este metodo no se utiliza en este programa...
	 * @param archivoValido Archivo
	 * @param s Cadena
	 * @param variable Variable
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException Excepcion lanzada
	 * @throws IOException Excepcion lanzada
	 */
	public void salirDelProceso( boolean [] archivoValido, String s, String variable,

			HttpServletRequest request, HttpServletResponse response )

			throws ServletException, IOException {

		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);

		HttpSession sess = request.getSession();

	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		StringBuffer contArchivoStr=new StringBuffer("");

	    contArchivoStr.append("<table border=0 align=center>");

		contArchivoStr.append("<tr><td colspan=9 align=center><b>Pago de Nomina</b></td></tr>");

		contArchivoStr.append("<tr bgcolor=red+><td colspan=9 align=center><b>Se han detectado errores al leer el archivo</b></td></tr>");

		contArchivoStr.append("<tr><td colspan=9 align=center><b>");
		contArchivoStr.append(s);
		contArchivoStr.append("</b></td></tr>");

	    contArchivoStr.append("<tr><td colspan=9 align=center><b>");
		contArchivoStr.append(variable);
		contArchivoStr.append("</b></td></tr>");

		contArchivoStr.append("</tr></table>");

		request.setAttribute("newMenu", session.getFuncionesDeMenu());

		request.setAttribute("MenuPrincipal", session.getStrMenu());

		request.setAttribute(ENCABEZADO, CreaEncabezado("Pago de N&oacute;mina",ENCABEZADO_NOMINA,S25800H,request));

	    request.setAttribute("erroresEnArchivo",""+contArchivoStr.toString());

//		popup de jorge

//		despliegaPaginaError(contArchivoStr,"Pago de Nomina","Errores al momento de importar el archivo");

	    archivoValido[0]=false;

		evalTemplate(IEnlace.ERR_NOMINA_TMPL, request, response );

	}



	/**
	 * Metodo para obtener cuentas
	 * @param nameArchivo Nombre de archivo
	 * @param numeroRegs Numero de registros
	 * @return String[] Array de cuentas
	 */
	public String[] arregloCuentas(File nameArchivo, int numeroRegs){

	//public String[] arregloCuentas(String nameArchivo, int numeroRegs){

		EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo arregloCuentas &", EIGlobal.NivelLog.INFO);

		//nameArchivo = nameArchivo.trim();

		//BufferedReader archivoLectura = null;

		String lineaQueSeLeyo	 = "";



		String[] arreglo		 = new String[numeroRegs];



		//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

		RandomAccessFile archivoLectura = null;
		//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin



		try{

			//archivoLectura = new BufferedReader( new FileReader( nameArchivo ) );



			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio


			//RandomAccessFile archivoLectura=new RandomAccessFile(nameArchivo,"r");
			archivoLectura=new RandomAccessFile(nameArchivo,"r");

			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin




			/*try{

			EIGlobal.mensajePorTrace("***ImportNomina.class & 4&", EIGlobal.NivelLog.INFO);

			archivoLectura.seek(41);

			lineaQueSeLeyo		= archivoLectura.readLine();

			EIGlobal.mensajePorTrace("***ImportNomina.class & lineaQueSeLeyo5&"+ lineaQueSeLeyo, EIGlobal.NivelLog.INFO);

			if(lineaQueSeLeyo==null) {

				lineaQueSeLeyo		= archivoLectura.readLine();

			}

			} catch(Exception e){

				e.printStackTrace();

			}*/

			archivoLectura.seek(41);

			lineaQueSeLeyo = archivoLectura.readLine();



			if (lineaQueSeLeyo.length()  == 126  ){

				archivoLectura.seek(40);

				lineaQueSeLeyo = archivoLectura.readLine();

				}else {

				    archivoLectura.seek(41);

					lineaQueSeLeyo = archivoLectura.readLine();

					}



				for(int i=0; i<arreglo.length; i++){

					if ( lineaQueSeLeyo.equals( "" ) )

						lineaQueSeLeyo = archivoLectura.readLine();

					if ( lineaQueSeLeyo == null )

						break;

					if(lineaQueSeLeyo.length()>108) { //jtg  para evitar el error

						arreglo[i]		= lineaQueSeLeyo.substring(93,109);

					}else {

						i--;

						}



					lineaQueSeLeyo = archivoLectura.readLine();

				}


		   	//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

			archivoLectura.close();
			//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin


		} catch(IOException exception) {



			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %arregloCuentas()"+exception, EIGlobal.NivelLog.INFO);

		} catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}



		//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
		finally{
            if(null != archivoLectura){
                try{
                	archivoLectura.close();
                }catch(IOException e){}
                archivoLectura = null;
            }
        }

		//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin


		return arreglo;

	}



	/**
	 * Metodo para obtener numeros de empelados
	 * @param nameArchivo Nombre de archivo
	 * @param numeroRegs Numero de registros
	 * @return String[] Array de num de empleados
	 */
	public String[] arregloNumeroEmpleado(File nameArchivo, int numeroRegs){

	//public String[] arregloNumeroEmpleado(String nameArchivo, int numeroRegs){

	EIGlobal.mensajePorTrace("***ImportNomina.class &Entrando al metodo arregloNumeroEmpleado&", EIGlobal.NivelLog.INFO);

	//nameArchivo =nameArchivo.trim();

	//BufferedReader archivoLectura = null;

	String lineaQueSeLeyo	= "";

	String[] arregloNumeroEmpleado = new String[numeroRegs];


	//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio

	RandomAccessFile archivoLectura = null;
	//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin



		try{


			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..inicio


			//RandomAccessFile archivoLectura = new RandomAccessFile(nameArchivo,"r");
			archivoLectura = new RandomAccessFile(nameArchivo,"r");

			//***** everis Declaraci�n RandomAccessFile 08/05/2008  ..fin




			//archivoLectura = new BufferedReader( new FileReader( nameArchivo ) );

			archivoLectura.seek(41);

			lineaQueSeLeyo = archivoLectura.readLine();



			if (lineaQueSeLeyo.length()  == 126  ){

				archivoLectura.seek(40);

				lineaQueSeLeyo = archivoLectura.readLine();

				}else {

				    archivoLectura.seek(41);

					lineaQueSeLeyo = archivoLectura.readLine();

					}



			for ( int i = 0; i < arregloNumeroEmpleado.length; i++ ) {

				if ( lineaQueSeLeyo.equals( "" ) )

					lineaQueSeLeyo = archivoLectura.readLine();

				if ( lineaQueSeLeyo == null )

					break;

				if(lineaQueSeLeyo.length()> 12){

					arregloNumeroEmpleado[i] = lineaQueSeLeyo.substring( 6, 13 );

				}else {

					i--;

					}

					lineaQueSeLeyo = archivoLectura.readLine();



			}

			//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio

			archivoLectura.close();
			//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin


		} catch(IOException exception) {



			EIGlobal.mensajePorTrace("***ImportNomina.class Ha ocurrido una excepcion en: %arregloNumeroEmpleado()"+exception, EIGlobal.NivelLog.INFO);

		} catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}


		//***** everis Cerrando RandomAccessFile 08/05/2008  ..inicio
		finally{
            if(null != archivoLectura){
                try{
                	archivoLectura.close();
                }catch(IOException e){}
                archivoLectura = null;
            }
        }

		//****  everis Cerrando RandomAccessFile 08/05/2008  ..fin


		return arregloNumeroEmpleado;

	}



	/**
	 * Metodo para verificar cuenta cargo
	 * @param arrayCuentas Array de cuentas
	 * @param cuentaCargo Cuenta cargo de entrada
	 * @return boolean Resultado de cuenta cargo
	 */
	boolean verificaCuentaCargo(String[][] arrayCuentas, String cuentaCargo){

		for(int indice=1; indice<=Integer.parseInt(arrayCuentas[0][0]); indice++){

			if (arrayCuentas[indice][1].trim().equals(cuentaCargo)&&arrayCuentas[indice][2].trim().equals("P"));

			return true;

		}

		return false;

	}



	public String formatea(String strNum)

		{

		long num = 0;

		String forma = null;



		// Se verifica que la cadena sea un n�mero y se quitan los posible ceros a la izq.

		try

			{num = Long.parseLong(strNum);}

		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("formatea: La cadena no es un n�mero", EIGlobal.NivelLog.ERROR); return null;}

		forma = "" + num;



		// Se a�ade el punto

		forma=forma.substring(0,forma.length()-2) + "." + forma.substring(forma.length()-2);



		return forma;

		}



	public String formatoMoneda( String cantidad )

	{

	EIGlobal.mensajePorTrace("***Entrando a formatoMoneda ", EIGlobal.NivelLog.INFO);

	    cantidad = cantidad.trim();

		String language = "la"; // ar

		String country  = "MX";  // AF

		Locale local    = new Locale(language,  country);

		NumberFormat nf = NumberFormat.getCurrencyInstance(local);

		String formato  = "";

		String cantidadDecimal = "";

		String formatoFinal    = "";

		String cantidadtmp     = "";



		if(cantidad ==null ||cantidad.equals(""))

			cantidad="0.0";

		if(cantidad.length() > 2){

			try {

				cantidadtmp =cantidad.substring(0,cantidad.length()-2);

				cantidadDecimal = cantidad.substring(cantidad.length()-2);

				formato = nf.format(new Double(cantidadtmp).doubleValue());

				formato=formato.substring(1,formato.length()-2);

				formatoFinal = "$ " + formato + cantidadDecimal;

			} catch(NumberFormatException e) {

				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);

				formatoFinal= "$ " + "0.00";}

		}else {

		try {

		       if (cantidad.length() > 1){

				cantidadDecimal = cantidad.substring(cantidad.length()-2);

				formatoFinal = "$ " + "0."+ cantidadDecimal;

			   } else {

				cantidadDecimal = cantidad.substring(cantidad.length()-1);

				formatoFinal = "$ " + "0.0"+ cantidadDecimal;

				}

			} catch(NumberFormatException e) {

				EIGlobal.mensajePorTrace("***Error de formatoMoneda "+e.toString(), EIGlobal.NivelLog.DEBUG);

				formatoFinal="$ " + "0.00";}

		}

		if(formatoFinal==null)

			formato = "";

		return formatoFinal;

	}

public GregorianCalendar getFechaString(String fecha)
	{
	  EIGlobal.mensajePorTrace("ImportNomina - getFechaString(): Fecha cambio: " + fecha, EIGlobal.NivelLog.INFO);
      try
		{
		  if(fecha.length()==10)
			{
			  int Anio = Integer.parseInt(fecha.substring(6));
			  int Mes = Integer.parseInt(fecha.substring(3,5))-1;
			  int Dia = Integer.parseInt(fecha.substring(0,2));
			  return new GregorianCalendar(Anio, Mes, Dia);
			}
		  else
			{
			  EIGlobal.mensajePorTrace("ImportNomina - getFechaString(): Error en la longitud de la fecha.", EIGlobal.NivelLog.INFO);
			}

		} catch (Exception e)
		 {
			EIGlobal.mensajePorTrace("ImportNomina - getFechaString(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
	     }
	   return null;
     }

/*************** Nomina Concepto de Archvo */
public StringBuffer getConceptos()
	{
	   EI_Query BD= new EI_Query();
	   Hashtable listaConceptos=new Hashtable();
	   StringBuffer cadenaRegreso=new StringBuffer("");

	   String sqlConcepto  = "SELECT id_concepto,Concepto from INOM_CONCEPTOS order by id_concepto";
	   listaConceptos =BD.ejecutaQuery(sqlConcepto);
	   if( listaConceptos==null )
		 {
			EIGlobal.mensajePorTrace( "ImportNomina - getConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ImportNomina - getConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);

			cadenaRegreso.append("<option value='01'>Pago de N�mina. </option>\n");
			cadenaRegreso.append("<option value='02'>Pago Vacaciones. </option>\n");
			cadenaRegreso.append("<option value='03'>Pago Gratificaciones. </option>\n");
			cadenaRegreso.append("<option value='04'>Pago por Comisiones. </option>\n");
			cadenaRegreso.append("<option value='05'>Pago por Beca. </option>\n");
			cadenaRegreso.append("<option value='06'>Pago por Pensi�n. </option>\n");
			cadenaRegreso.append("<option value='07'>Pago por Subsidios. </option>\n");
			cadenaRegreso.append("<option value='08'>Otros pagos por transferencia. </option>\n");
			cadenaRegreso.append("<option value='09'>Pago por Honorarios. </option>\n");
			cadenaRegreso.append("<option value='10'>Pr�stamo. </option>\n");
			cadenaRegreso.append("<option value='11'>Pago de vi�ticos. </option>\n");
			cadenaRegreso.append("<option value='12'>Anticipo de vi�ticos. </option>\n");
			cadenaRegreso.append("<option value='13'>Fondo de ahorro. </option>\n");
		  }
		else
		  {
			EIGlobal.mensajePorTrace( "ImportNomina - getConceptos()-> Tama�o : "+listaConceptos.size() , EIGlobal.NivelLog.INFO);
			String []datos=new String[2];
			for (int i=0;i<listaConceptos.size();i++ )
			  {
				 datos=(String[])listaConceptos.get(""+i);
				 cadenaRegreso.append("<option value='"+datos[0]+"'>");
				 cadenaRegreso.append(datos[1]);
				 cadenaRegreso.append("</option>\n");
			  }
		  }
	   return cadenaRegreso;
	}

public Vector getIDConceptos()
	{
	   EI_Query BD= new EI_Query();
	   Vector listaIDConceptos=new Vector();;

	   String sqlIDConcepto  = "SELECT id_concepto from INOM_CONCEPTOS order by id_concepto";
	   listaIDConceptos =BD.ejecutaQueryCampo(sqlIDConcepto);
	   if( listaIDConceptos==null )
		 {
			listaIDConceptos=new Vector();
			EIGlobal.mensajePorTrace( "ImportNomina - getIDConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ImportNomina - getIDConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);
			for(int a=1;a<14 ;a++ )
			 {
			   String dato=(a<10)?"0"+a:""+a;
			   listaIDConceptos.add(dato);
			 }
		 }

	   return listaIDConceptos;
	   
	   
	   
	}

/*************** Nomina Concepto de Archvo */
//26-05-2016 JFOL se crea este metodo para validar que la cadena sea num�rica
public static boolean validaCuentaNumerica(String campo) {

	EIGlobal.mensajePorTrace("ImportNomina ::  ValidaCuentaSeaNum�rica ::  ", EIGlobal.NivelLog.DEBUG);
	String caracteresPermitidos="";
		caracteresPermitidos = "0123456789";
	boolean campoValido = false;
	int carValido = 0;
	char letra;

	EIGlobal.mensajePorTrace("ImportNomina ::  ValidaCuentaSeaNum�rica :: recorriendo cadena: "+campo+" ::: ", EIGlobal.NivelLog.DEBUG);
	for (int i = 0; i < campo.length(); i++) {
		letra = campo.charAt(i);
		for (int j = 0; j < caracteresPermitidos.length(); j++) {
			if (letra == caracteresPermitidos.charAt(j)){
				carValido++;
			}
		}
	}

	if (carValido == campo.length()) {
		campoValido = true;
	}else{
		
	}

	EIGlobal.mensajePorTrace("ImportNomina ::  ValidaCuentaSeaNum�rica :: campoValido: "+campoValido, EIGlobal.NivelLog.DEBUG);

	return campoValido;

}

}