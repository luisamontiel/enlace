//####################################################################
//
//      Modificacion: validar Nueva version sua y fechalimite
//                    para aceptar version anterior.
//      Proyecto    : SUA 2005
//      Cod         : MX2005-21100
//      Programador : Carlos Aleman Rosas.
//      clave en cod: W3xx
//#################################################################

/** Banco Santander Mexicano
*   Clase	       SuaConsulta Consultar los movimientos del SUA
*   @autor	       Ruben Fragoso
*   @version	       1.0
*   fecha de creacion: 2 de marzo del 2001
*   responsable      : Mariela Betancourt
*   descripcion      : Ejecuta servicios del web_red para consultar el estatus de los
*		       movimientos del SUA.
*
*   Modificacion: MPP-Maria Elena de la Pe�a, 25/11/2003 Incidencia IMV519881
*		  Se agrega clave del banco para logo en reportes.
*/
package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import sun.security.provider.PolicyParser.GrantEntry;



public class SuaConsulta extends BaseServlet
{
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	//modif PVA 12/08/2002
	public boolean SesionValida( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, java.io.IOException
	{
		HttpSession sess;
		sess = request.getSession( false );
		BaseResource session = (BaseResource) sess.getAttribute("session");

		boolean resultado = false;
		String remotehost=request.getRemoteHost();
		String servername = request.getServerName();
		String port=String.valueOf(request.getServerPort());
		String serversofw=getServletContext().getServerInfo();
		String ip=request.getRemoteAddr();
		String protocolo=Global.PROTOCOLO;//request.getProtocol();

		//protocolo=protocolo.substring(0,protocolo.indexOf("/"));

		/*System.out.println("remotehost = >" +remotehost+ "<");
		System.out.println("port = >" +port+ "<");
		System.out.println("servername = >" +servername+ "<");
		System.out.println("serversofw = >" +serversofw+ "<");
		System.out.println("ip = >" +ip+ "<");
		System.out.println("protocolo = >" +protocolo+ "<");*/

		//sess=null;
		if ( sess == null || session == null)
		{
			/*request.setAttribute("web_application",Global.WEB_APPLICATION);
			request.setAttribute("host",protocolo+"://"+remotehost);
			evalTemplate("/jsp/sessionfuera.jsp", request, response );*/

		} else {
			//session = new BaseResource( sess );
			if( session.getUserID8() == null || session.getUserID8().equals("") )
			{
				/*request.setAttribute("web_application",Global.WEB_APPLICATION);
				request.setAttribute("host",protocolo+"://"+remotehost);
				evalTemplate("/jsp/sessionfuera.jsp", request, response );*/
			} else {
				resultado = true;
			}
		}
		return resultado;
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{

		/*
		 * VSWF ARR -I
		 */
		HttpSession sess = null;
		BaseResource session = null;


		String origen = request.getParameter( "origen" );
		String ventana = request.getParameter( "ventana" );

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & origen : >" +origen + "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & ventana : >" +ventana+ "<", EIGlobal.NivelLog.INFO);

		//modif PVA 08/08/2002
		boolean validar_session=true;
		boolean session_ok=true;

		session_ok=SesionValida( request, response );

		if ((session_ok==false)&&
			((origen.equals("Consulta")&&ventana.equals("3")) ||
			(origen.equals("Disco"))))
			session_ok=true;

		if ( session_ok )
		{
			sess = request.getSession( false );
			session = (BaseResource) sess.getAttribute("session");
			if (origen.equals("Consulta"))
			{
				if (ventana.equals("0"))
				{
					capturaRegPatronal( request, response );
				} else if (ventana.equals("2")) {
					publicaPagoSua("Consulta", request, response );
				} else if (ventana.equals("3")) {
					publicaPagoSua("Comprobante", request, response );
				}
			} else if (origen.equals("Disco")) {
				publicaPagoSua("Comprobante", request, response );
			}
		}
	}
	/** El metodo BuscarToken() devuelve un token
	*   de una trama.
	*/
	public String BuscarToken(String Cadena, char Caracter, int Indice)
	{
		String Token	       = "";
		int    PosicionInicial = 0;
		int    PosicionFinal   = 0;
		int    Cuantos	       = 0;

		PosicionFinal = Cadena.indexOf( Caracter, PosicionInicial);
		while(PosicionFinal != -1)
		{
			PosicionFinal	= Cadena.indexOf( Caracter, PosicionInicial);
			if(PosicionFinal != -1)
			{
				Cuantos = Cuantos + 1;
				if(Indice == Cuantos)
				{
					Token = Cadena.substring( PosicionInicial, PosicionFinal);
					break;
				}
				PosicionInicial = PosicionFinal + 1;
			}
		}
		if( PosicionInicial < Cadena.length())
		{
			Cuantos = Cuantos + 1;
			if( Indice == Cuantos)
				Token = Cadena.substring(PosicionInicial);
		}
		return(Token);
	}
	/** El metodo capturaRegPatronal() muestra la pagina donde el usuario
	*   captura el Registro Patronal y el Folio a consultar.
	*/
	public void capturaRegPatronal ( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & Entrando a pantalla de Captura RegPatronal &", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String contrato = "";
		short  suc_opera = (short)787;
		String usuario = "";
		String clave_perfil = "";
		Date curDate = new Date();
		String strBoton = "";

		//Variables de sesion

		contrato = session.getContractNumber();
		suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		usuario = session.getUserID8();
		clave_perfil = session.getUserProfile();

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & contrato >" +contrato+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & suc_opera >" +suc_opera+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & usuario >" +usuario+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & clave_perfil >" +clave_perfil+ "<", EIGlobal.NivelLog.INFO);

		String fecha_hoy = "";
		String tipo_cuenta = "";

		fecha_hoy = ObtenFecha();
		String strFecha = ObtenFecha(true);

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & fecha_hoy >" +fecha_hoy+ "<", EIGlobal.NivelLog.INFO);

		strBoton  = "<A href = \"javascript:if(validaForma(document.frmConsulta,evalua,errores)) document.frmConsulta.submit();\" ";
		strBoton += "border=0><img src=\"/gifs/EnlaceMig/gbo25220.gif\" border=0 alt=\"Consultar\" width=\"90\" height=\"22\"></a> ";

		request.setAttribute("newMenu",       session.getFuncionesDeMenu());
		request.setAttribute("Encabezado",    CreaEncabezado("Consulta de pagos SUA","Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", "s25680h", request));
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("ContUser",      ObtenContUser(request));
		request.setAttribute("strBoton",      strBoton);

//		TODO: BIT CU 4111, El cliente entra al flujo
		/*
		 * VSWF ARR -I
		 */
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
		BitaHelper bh = new BitaHelperImpl(request, session,sess);
		if (request.getParameter(BitaConstants.FLUJO)!=null){
		       bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
		  }else{
		   bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
		  }

		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ES_AP_OP_CONSULTAS_ENTRA);
		bt.setContrato(session.getContractNumber());

		BitaHandler.getInstance().insertBitaTransac(bt);
		}catch (SQLException e){
			e.printStackTrace();
		}catch(Exception e){

			e.printStackTrace();
		}
		}
		/*
		 * VSWF ARR -F
		 */

		evalTemplate("/jsp/SuaConsulta.jsp", request, response);

		//RequestDispatcher rdSalida = getServletContext().getRequestDispatcher("/jsp/SuaConsulta.jsp");
		//rdSalida.include( request, response );

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & Saliendo de la pantalla de Captura ", EIGlobal.NivelLog.DEBUG);
	}
	/** El metodo publicaPagoSua() ejecuta el servicio de Consulta
	*   de pagos del SUA y muestra los resultados en un jsp.
	*/
	public void publicaPagoSua( String strDestino, HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & Entrando a pantalla de muestra consulta &", EIGlobal.NivelLog.DEBUG);

		int salida = 0;
		String contrato = "";
		String coderror = "";
		short  suc_opera = (short)787;
		String usuario = "";
		String clave_perfil = "";
		String cadena_getFacCOpProg="";
		String cve_banco = "";
                String clave_con = new String("ABCDEFGHIJKLNMOPQRSTUVWXYZ3"); //carecter clave nueva version W3xx

		//Variables de sesion
		//modif PVA 08/08/2002
		//MPP cambio 251103 ClaveBanco
		try
		{
			/*System.out.println("request contrato "+ request.getParameter("comp_contrato"));
			System.out.println("request usuario "+request.getParameter("comp_usuario"));
			System.out.println("request perfil  "+request.getParameter("comp_perfil"));*/

			contrato = session.getContractNumber();
			if(contrato==null)
				contrato=request.getParameter("comp_contrato");

			usuario = session.getUserID8();
			if(usuario==null)
				usuario=request.getParameter("comp_usuario");

			clave_perfil = session.getUserProfile();
			if(clave_perfil == null)
				clave_perfil=request.getParameter("comp_perfil");

			cve_banco = session.getClaveBanco();
			if(cve_banco == null)
				cve_banco = request.getParameter("ClaveBanco");

			//cadena_getFacCOpProg = (session.getFacultad(session.FacCOpProg));
			suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace( "***error "+e, EIGlobal.NivelLog.ERROR);
			contrato=request.getParameter("comp_contrato");
			usuario=request.getParameter("comp_usuario");
			clave_perfil=request.getParameter("comp_perfil");
		}

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & contrato >" +contrato+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & suc_opera >" +suc_opera+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & usuario >" +usuario+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & clave_perfil >" +clave_perfil+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & cve_banco >" +cve_banco+ "<", EIGlobal.NivelLog.INFO);

		//Modif PVA 12/08/2002
		if ((contrato==null) || (usuario==null)|| (clave_perfil==null))
		{
			//System.out.println("Datos insuficientes");
			if(strDestino.equals("Comprobante"))
			{
				request.setAttribute("mensajerror","Datos insuficentes para ejecutar la operaci&oacute;n");
				RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/suaconsultaerror.jsp" );
				rdSalida.include( request, response );
			} else
				despliegaPaginaError("Datos insuficientes para ejecutar la operaci&oacute;n","Consulta de Pagos S.U.A.", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", request, response );
			return;
		}

		String strRegPatronal = request.getParameter("strRegPatronal");
		String strFolio = request.getParameter("strFolio");
		String fecha_hoy = ObtenFecha(true);

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & fecha hoy >" +fecha_hoy+ "<", EIGlobal.NivelLog.INFO);

		String trama_entrada_con = "";// trama de entrada de la consulta.
		String encabezado = "";
		String retCode = "";
		String strSalida = "";
		String[] arrSalida;
		String trama_salidaRedSrvr = "";// trama de salida del web_red.
		String trama_salidaRecaSrvr = "";// trama de salida del recasrvr.
		String trama_salida = "";
		String strMensaje = "";
		String strDebug = "";
		String strBoton = "";
		Hashtable htResult = null;

		boolean errFlag = true;

		String medio_entrega = "1EWEB";
		String tipo_operacion = "RE04";

		encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato;
		encabezado += "|" + usuario + "|" + clave_perfil + "|";

		trama_entrada_con = strFolio + "@" + strRegPatronal + "@";
		trama_entrada_con = encabezado + trama_entrada_con;

		strDebug += "trama_entrada_con:" + trama_entrada_con;
		EIGlobal.mensajePorTrace( "***SuaConsulta.class >> trama_entrada_con : >" +trama_entrada_con+ "<", EIGlobal.NivelLog.DEBUG);

		try {
			htResult = tuxGlobal.web_red( trama_entrada_con );
		} catch ( java.rmi.RemoteException re ) {
			re.printStackTrace();
		} catch (Exception e) {}
		trama_salida = ( String ) htResult.get( "BUFFER" );

		EIGlobal.mensajePorTrace( "***SuaConsulta.class >> trama_salida_con : >" +trama_salida+ "<", EIGlobal.NivelLog.DEBUG);
		strDebug += "trama_salida_con:" + trama_salida;

		//OK	     14378SUAS0009	   REGISTRO PATRONAL O FOLIO INCORRECTO
		if (trama_salida.substring(0,8).trim().equals("OK"))
		{
			strMensaje = trama_salida.substring(24, trama_salida.length());
			//System.out.println(" strMensaje "+strMensaje);
			if(strDestino.equals("Comprobante"))
			{
				request.setAttribute("mensajerror",strMensaje);
				RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/suaconsultaerror.jsp" );
				rdSalida.include( request, response );
			} else
				despliegaPaginaError(strMensaje ,"Consulta de Pagos S.U.A.", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", request, response );
			return;
		}

		String path_archivo = BuscarToken(trama_salida, '|', 1);
		String nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & path_archivo : >" +path_archivo+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & nombre_archivo : >" +nombre_archivo+ "<", EIGlobal.NivelLog.INFO);

		// Copia Remota
		ArchivoRemoto archR = new ArchivoRemoto();
		if(!archR.copia(nombre_archivo))
		{
			EIGlobal.mensajePorTrace( "***SuaConsulta.class & publicaPagoSua: No se realizo la copia remota. &", EIGlobal.NivelLog.ERROR);
		} else {
			EIGlobal.mensajePorTrace( "***SuaConsulta.class & publicaPagoSua: Copia remota OK. &", EIGlobal.NivelLog.DEBUG);
		}

		path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & path_archivo : >" +path_archivo+ "<", EIGlobal.NivelLog.INFO);
		strDebug += "path:" + path_archivo;

		boolean errorFile=false;
		try {
			File drvSua = new File(path_archivo);
			BufferedReader fileSua = new BufferedReader( new FileReader(drvSua));
			trama_salidaRedSrvr = fileSua.readLine();
			trama_salidaRecaSrvr = fileSua.readLine();
			fileSua.close();
		} catch ( Exception ioeSua ) {
			EIGlobal.mensajePorTrace( "***SuaConsulta.class Excepcion %publicaPagoSua() >> "+ ioeSua.getMessage() + " <<", EIGlobal.NivelLog.INFO);
			errorFile = true;
		}

		if (errorFile)
		{
			strMensaje = "Error de Archivo: No es posible la comunicaci&oacute;n con el servicio RE_CONREF_ENL. Intente m&aacute;s tarde.";

			if(strDestino.equals("Comprobante"))
			{
				//System.out.println(" strMensaje "+strMensaje);
				request.setAttribute("mensajerror",strMensaje);
				RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/suaconsultaerror.jsp" );
				rdSalida.include( request, response );
			} else
				despliegaPaginaError(strMensaje, "Consulta de Pagos S.U.A.", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", request, response );

			return;
		}

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & trama_salidaRedSrvr : >" +trama_salidaRedSrvr+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaConsulta.class & trama_salidaRecaSrvr : >" +trama_salidaRecaSrvr+ "<", EIGlobal.NivelLog.INFO);

		retCode = BuscarToken(trama_salidaRecaSrvr, '|', 1) ;

		String espacios = "	       ";

		if (trama_salidaRedSrvr.substring(0,8).trim().equals("OK"))
		{
			if (retCode.equals("RECA0000"))// transaccion realizada correctamente.
			{// antes 47, mas 3 campos para la version W3xx
				arrSalida = desentramaC("51|" + trama_salidaRecaSrvr ,'|');

				request.setAttribute("newMenu",       session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("Encabezado",    CreaEncabezado("Consulta de Pagos S.U.A.", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", "s25690h", request));
				request.setAttribute("txtRegPatronal", strRegPatronal);
				request.setAttribute("txtFolio",       strFolio);
				//System.out.println("Numero de Trabajadores = >" +arrSalida[17]+ "<");
				request.setAttribute("txtNumTrabaja",  arrSalida[17]);
				request.setAttribute("txtRFC",	       arrSalida[47]);
				request.setAttribute("txtReferencia",  arrSalida[2]);
				request.setAttribute("txtFechaPago",   fecha_corta(arrSalida[10]));
				request.setAttribute("txtImssPeriodo", arrSalida[14].substring(4, 6));
				request.setAttribute("txtImssYear",    arrSalida[14].substring(0, 4));
				request.setAttribute("txtNomRazSoc",   arrSalida[46]);
				request.setAttribute("txtImssImpSeg",  FormatoMoneda(arrSalida[29]));
				request.setAttribute("txtImssImpAct",  FormatoMoneda(arrSalida[30]));
				request.setAttribute("txtImssImpRec",  FormatoMoneda(arrSalida[31]));
				request.setAttribute("txtAfoImpRCV",   FormatoMoneda(arrSalida[34]));
				request.setAttribute("txtAfoImpAct",   FormatoMoneda(arrSalida[35]));
				request.setAttribute("txtInfImpApo",   FormatoMoneda(arrSalida[38]));
				request.setAttribute("txtAfoImpAct",   FormatoMoneda(arrSalida[35]));
				request.setAttribute("txtInfImpAct",   FormatoMoneda(arrSalida[39]));
				request.setAttribute("txtAfoImpRec",	   FormatoMoneda(arrSalida[36]));
				request.setAttribute("txtInfAmortCred",    FormatoMoneda(arrSalida[40]));
				request.setAttribute("txtAfoImpAV",	   FormatoMoneda(arrSalida[37]));
				request.setAttribute("txtInfActAmortCred", FormatoMoneda(arrSalida[41]));
				request.setAttribute("txtInfRecAmortCred", FormatoMoneda(arrSalida[42]));
                                //#####################################################
                                // Nuevo bloque para obtener nuevos campos version 3Wxx
                                //#####################################################
                                request.setAttribute("txtMultasInfonavit", FormatoMoneda(arrSalida[48]));
                                request.setAttribute("txtDonativofundemex", FormatoMoneda(arrSalida[49]));
                                request.setAttribute("txtAportacionesComp", FormatoMoneda(arrSalida[50]));
                                request.setAttribute("txtVersionSua", arrSalida[51]);
                                //#############################################
                                //fin bloque nuevos campos
                                //#############################################

				if( (Integer.parseInt(arrSalida[14].substring(4, 6)) % 2) == 0)
				{
					request.setAttribute("txtSarBimestre", Integer.toString(Integer.parseInt(arrSalida[14].substring(4, 6))/2));
					request.setAttribute("txtSarYear", arrSalida[14].substring(0, 4));
				}

				String strEstatus="";

				if (arrSalida[15].equals("CONCI"))
					strEstatus = "CONCILIADO";
				else if (arrSalida[15].equals("PENDI"))
					strEstatus = "PENDIENTE";
				else if (arrSalida[15].equals("IMPRE"))
					strEstatus = "IMPRESO";
				else if (arrSalida[15].equals("PENDI"))
					strEstatus = "PENDIENTE";
				else if (arrSalida[15].equals("PAGAD"))
					strEstatus = "PAGADO";
				else if (arrSalida[15].equals("VALID"))
					strEstatus = "VALIDADO";
				else if (arrSalida[15].equals("GRABA"))
					strEstatus = "GRABADO";

				double dblImssTotal = new Double(arrSalida[29]).doubleValue() + new Double(arrSalida[30]).doubleValue() + new Double(arrSalida[31]).doubleValue();
				double dblAfoSubTotal = new Double(arrSalida[34]).doubleValue() + new Double(arrSalida[35]).doubleValue() + new Double(arrSalida[36]).doubleValue();
				double dblAfoTotal = dblAfoSubTotal + new Double(arrSalida[37]).doubleValue() + new Double(arrSalida[50]).doubleValue();
				double dblInfSubTotal = new Double(arrSalida[38]).doubleValue() + new Double(arrSalida[39]).doubleValue();
				double dblInfTotal = dblInfSubTotal + new Double(arrSalida[40]).doubleValue() + new Double(arrSalida[41]).doubleValue()+ new Double(arrSalida[42]).doubleValue()+ new Double(arrSalida[48]).doubleValue()+ new Double(arrSalida[49]).doubleValue();
				double dblGranTotal = dblImssTotal + dblAfoTotal + dblInfTotal;

				request.setAttribute("txtInfSubTotal", FormatoMoneda(dblInfSubTotal));
				request.setAttribute("txtAfoSubTotal", FormatoMoneda(dblAfoSubTotal));
				request.setAttribute("txtImssTotal",   FormatoMoneda(dblImssTotal));
				request.setAttribute("txtAfoTotal",    FormatoMoneda(dblAfoTotal));
				request.setAttribute("txtInfTotal",    FormatoMoneda(dblInfTotal));
				request.setAttribute("txtGranTotal",   FormatoMoneda(dblGranTotal));
				request.setAttribute("txtEstatus",     strEstatus);

				if (strEstatus.equals("PAGADO"))
				{
					strBoton  = "<A href = \"javascript:generaComprobante(document.frmConsulta);\" ";
					strBoton += "border=0><img src=\"/gifs/EnlaceMig/gbocomprobante.gif\" border=0 alt=\"Comprobante\"></a> ";
				}

				strBoton += "<A href = \"javascript:scrImpresion();\" ";
				strBoton += "border=0><img src=\"/gifs/EnlaceMig/gbo25240.gif\" border=0 alt=\"Imprimir\" width=\"83\" height=\"22\"></a> ";

				request.setAttribute("strBoton", strBoton);
				request.setAttribute("strDebug", strDebug);
				request.setAttribute("ContUser", ObtenContUser(request));

				if (strDestino.equals("Consulta"))
				{

					//modif PVA 06/08/2002
					request.setAttribute("web_application",Global.WEB_APPLICATION);
					request.setAttribute("comp_usuario", session.getUserID8());
					request.setAttribute("comp_contrato",session.getContractNumber());
					request.setAttribute("comp_perfil",session.getUserProfile());

					RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/SuaConsulta.jsp" );
					rdSalida.include( request, response );
				} else if (strDestino.equals("Comprobante"))
                                        {
					request.setAttribute("ClaveBanco", session.getClaveBanco());//MPP
                                        //###################################
                                        //Nuevo Bloque para nuevo comprobante
                                        //###################################

                                        //if (  (!(clave_con.indexOf(arrSalida[50].substring(0,1))<0)) && (clave_con.indexOf(arrSalida[51].substring(1,2))<0)   )
					EIGlobal.mensajePorTrace( "***SuaConsulta.class & VERSION >>>>>>>>>>" +arrSalida[51]+ "<", EIGlobal.NivelLog.INFO);
				if (clave_con.indexOf(arrSalida[51].substring(1,2))<0)
                                        {
                                               RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/SuaComprobante.jsp" );
					       rdSalida.include( request, response );
                                        }else{
                                               RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/SuaComprobanteW3.jsp" );
					       rdSalida.include( request, response );
                                        }
                                        //###################################
                                        //Fin Nuevo Bloque W3xx
                                        //###################################
				}
//				TODO: BIT CU 4111, A5, 4101 A11
				/*
				 * VSWF ARR - I
				 */
				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
					String refBit = strFolio;
				BitaHelper bh = new BitaHelperImpl(request, session,sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_AP_OP_PAGOS))
					bt.setNumBit(BitaConstants.ES_AP_OP_PAGOS_REALIZA_CONSULTA_SUA);
				else
					if ((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_AP_OP_CONSULTAS))
						bt.setNumBit(BitaConstants.ES_AP_OP_CONSULTAS_CONSULTA_PAGO);
				bt.setContrato(contrato);
				if(retCode!=null){
					 if(retCode.substring(0,2).equals("OK")){
						   bt.setIdErr("RECA0000");
					 }else if(retCode.length()>8){
						  bt.setIdErr(retCode.substring(0,8));
					 }else{
						  bt.setIdErr(retCode.trim());
					 }
					}
				bt.setImporte(dblGranTotal);
				bt.setServTransTux(tipo_operacion);
				if(refBit!=null && !refBit.equals(""))
					bt.setReferencia(Long.parseLong(refBit));
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
				}
				/*
				 * VSWF ARR -F
				 */
			} else if (retCode.substring(0,4).equals("RECA")) {// El recasrvr devolvio un error.
				strMensaje += BuscarToken(trama_salidaRecaSrvr, '|', 2);
				if(strDestino.equals("Comprobante"))
				{
					//System.out.println(" strMensaje "+strMensaje);
					request.setAttribute("mensajerror",strMensaje);
					RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/suaconsultaerror.jsp" );
					rdSalida.include( request, response );
				} else
					despliegaPaginaError(strMensaje ,"Consulta de Pagos S.U.A.", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", request, response );
			} else {
				strMensaje += trama_salidaRecaSrvr.substring(16, trama_salidaRecaSrvr.length());
				if(strDestino.equals("Comprobante"))
				{
					//System.out.println(" strMensaje "+strMensaje);
					request.setAttribute("mensajerror",strMensaje);
					RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/suaconsultaerror.jsp" );
					rdSalida.include( request, response );
				} else
					despliegaPaginaError(strMensaje ,"Consulta de Pagos S.U.A.", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", request, response );
			}

		} else {
			strMensaje += "Problemas con la comunicaci&oacute;n. Intente m&aacute;s tarde.";
			if(strDestino.equals("Comprobante"))
			{
				//System.out.println(" strMensaje "+strMensaje);
				request.setAttribute("mensajerror",strMensaje);
				RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/suaconsultaerror.jsp" );
				rdSalida.include( request, response );
			} else
				despliegaPaginaError(strMensaje ,"Consulta de Pagos S.U.A.", "Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Consultas", request, response );
		}

		EIGlobal.mensajePorTrace( "***SuaConsulta.class & Saliendo de la pantalla de muestra consulta : " + salida + " &", EIGlobal.NivelLog.INFO);
	}
	/** El metodo justifica() llena con espacios a la izquierda una cadena
	*   para ponerla en el value de un input text box de longitud 14.
	*/
	String justifica(String cadena)
	{
		String espacios = "	       ";
		return espacios.substring(0, 14 - cadena.length()) + cadena;
	}

	/** El metodo fecha_corta() recibe un String en formato dd-mmm-aaaa
	*   y devuelve otro en formato dd/mm/aaaa.
	*/
	String fecha_corta(String fecha_ent)
	{
		Hashtable Mes = new Hashtable();
		Mes.put("jan","01");
		Mes.put("feb","02");
		Mes.put("mar","03");
		Mes.put("apr","04");
		Mes.put("may","05");
		Mes.put("jun","06");
		Mes.put("jul","07");
		Mes.put("aug","08");
		Mes.put("sep","09");
		Mes.put("oct","10");
		Mes.put("nov","11");
		Mes.put("dec","12");

		String fecha_sal = fecha_ent.substring(0, 2) + "/" + Mes.get(fecha_ent.substring(3, 6)) + "/" + fecha_ent.substring(7, 11);
		return fecha_sal;
	}
}