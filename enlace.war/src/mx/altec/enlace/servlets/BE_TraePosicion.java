/*
    Desarrollado por: Francisco Serrato Jimenez (fsj)
    Consultoria:      Getronics CP Mexico
    Proyecto:         Migracion de Fondos a 390(MX-2002-257)
**/

package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.lang.reflect.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class BE_TraePosicion
    extends BaseServlet
{
    public void doGet(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException
    {
        defaultAction(request,response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException
    {
        defaultAction(request,response);
    }

    public void defaultAction(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Comenzar[defaultAction();]", EIGlobal.NivelLog.INFO);
        HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
        boolean sesionvalida = SesionValida(request,response);
        String cta_saldo    = ( request.getParameter("cta_saldo")!=null )   ? request.getParameter("cta_saldo")    : "";
        String tipoConsulta = ( request.getParameter("tipoConsulta")!=null )? request.getParameter("tipoConsulta") : "";
        String m            = ( request.getParameter("j")!=null )           ? request.getParameter("j")            : "";
        EIGlobal.mensajePorTrace("-!?cta_Saldo["+ cta_saldo +"] tipoCta["+ tipoConsulta +"] m["+ m +"]", EIGlobal.NivelLog.INFO);
        String[][] arrayCuentas = null;
        String[]   Carreglo     = desentramaC(m +","+ cta_saldo,',');
        if( sesionvalida && session.getFacultad(session.FAC_CONSULTA_SALDO) )
        {
            EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Sesion Valida", EIGlobal.NivelLog.INFO);
            llamado_servicioCtasInteg(IEnlace.MConsulta_Saldos_BE," "," "," ",session.getUserID8()+".ambci",request);
            int contctas = getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR +"/"+ session.getUserID8() +".ambci");
            arrayCuentas = ObteniendoCtasInteg(0,contctas,IEnlace.LOCAL_TMP_DIR +"/"+ session.getUserID8() +".ambci");
            String[][] cuentasEnvio = new String[Carreglo.length][5];
            for(int i=1; i<Carreglo.length; i++ )
            {
                for(int j=1; j<arrayCuentas.length; j++)
                {
                    if( Carreglo[i].equals(arrayCuentas[j][1]) )
                        for(int k=0; k<5 ;k++)
                            cuentasEnvio[i-1][k] = arrayCuentas[j][k];
                }//fin for j
            }//fin for i
            if( tipoConsulta.equals("soloPosicion") )
                ConsultaPosicion(request,response,session,cuentasEnvio,Carreglo,cta_saldo);
        }//fin if
        else
        {
            EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Sesion NO Valida", EIGlobal.NivelLog.INFO);
            request.setAttribute("MenuPrincipal",session.getstrMenu());
            request.setAttribute("Fecha"        ,ObtenFecha());
            request.setAttribute("ContUser"     ,ObtenContUser(request));
            request.setAttribute("MsgError"     ,IEnlace.MSG_PAG_NO_DISP);
            evalTemplate(IEnlace.ERROR_TMPL,request,response);
        }//fin else
        EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Terminar[defaultAction();]", EIGlobal.NivelLog.INFO);
    }

    private void ConsultaPosicion
        (
            HttpServletRequest request,
            HttpServletResponse response,
            BaseResource session,
            String[][] cuentasEnvio,
            String[] Carreglo,
            String cta_saldo
        )
            throws ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Comenzar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
        int        recordsOnFile = 0;
        String     color         = "";
        String     numCta        = "";
        String     tipOper       = "";
        String     tipRela       = "";
        String     tablaHTML     = "";
        String[]   respSaldo     = null;
        BigDecimal sldCartera    = new BigDecimal("0");
        BigDecimal saldoCart     = new BigDecimal("0");
        BigDecimal saldoDisp     = new BigDecimal("0");
        BigDecimal saldoActu     = new BigDecimal("0");
        BigDecimal saldo24Hr     = new BigDecimal("0");
        BigDecimal saldoMesa     = new BigDecimal("0");
        cta_saldo = cta_saldo.substring(0,cta_saldo.length()-1);
        ServicioTux tuxGlobal = new ServicioTux();
        //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
        RandomAccessFile archivoRespuesta;
        if( cuentasEnvio[0][3].equals("4") )     tipOper="POSI";
        else if( cuentasEnvio[0][3].equals("5") )tipOper="SDCR";
        else                                     tipOper="SDCT";
        EIGlobal.mensajePorTrace("-!?***tipOper["+ tipOper +"]", EIGlobal.NivelLog.INFO);
        tipRela = cuentasEnvio[0][2];
        String tramaEnvio = "2EWEB|"+ session.getUserID8() +"|"+ tipOper +"|"+ session.getContractNumber() +"|"+ session.getUserID8() +"|"+ session.getUserProfile() +"|"+ cta_saldo +"|"+ tipRela;
        EIGlobal.mensajePorTrace("-!?***Trama Enviar["+ tramaEnvio +"]", EIGlobal.NivelLog.INFO);
        String tramaRegresoServicio = "";
        try
        {
            Hashtable hs = tuxGlobal.web_red(tramaEnvio);
            tramaRegresoServicio = (String) hs.get("BUFFER");
            EIGlobal.mensajePorTrace("-!?***Trama Recibe["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.DEBUG);
        }catch(Exception e1){ e1.printStackTrace(); }
        String nombreArchivo = Global.DIRECTORIO_LOCAL + tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/'));
        ArchivoRemoto archRem = new ArchivoRemoto();
        archRem.copia(tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/')+1));
        try
        {
            archivoRespuesta = new RandomAccessFile(nombreArchivo,"rw");
            String datSaldosArch = obtenerSaldosArchivo(archivoRespuesta);
            EIGlobal.mensajePorTrace("-!?***Servicio Regreso["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("-!?***    Trama Saldos["+ datSaldosArch +"]", EIGlobal.NivelLog.INFO);
            if( datSaldosArch.startsWith("OK ") )
            {
                tablaHTML += "  <tr align=\"center\">\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Emisora       </td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">T&iacute;tulos</td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Costo Promedio</td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Precio        </td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Fondos        </td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">% Cartera     </td>\n";
                tablaHTML += "  </tr>\n";
                Vector datPosicionArch = obtenerPosicionArchivo(archivoRespuesta);
                EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Registros["+ datPosicionArch.size() +"]", EIGlobal.NivelLog.INFO);
                if( datPosicionArch.isEmpty() )
                {
                    tablaHTML += "  <tr align=\"center\">\n";
                    tablaHTML += "   <td class=\"textabdatobs\" colspan=\"6\">&nbsp;  El contrato no tiene Posici&oacute;n &nbsp;</td>\n";
                    tablaHTML += "  </tr>\n";
                }//fin if
                else
                {
                    respSaldo = desentrama(datSaldosArch,';');
                    if( respSaldo.length>5 )
                    {
                        EIGlobal.mensajePorTrace("-!?Cartera["+ respSaldo[1] +"] Disponible["+ respSaldo[2] +"] Actual["+ respSaldo[3] +"] 24Hrs.["+ respSaldo[4] +"] Titular["+ respSaldo[5] +"] Mesa Dinero["+ respSaldo[6] +"]", EIGlobal.NivelLog.INFO);
                        saldoCart = new BigDecimal( quitarComas(respSaldo[1]) );
                        saldoDisp = new BigDecimal( quitarComas(respSaldo[2]) );
                        saldoActu = new BigDecimal( quitarComas(respSaldo[3]) );
                        saldo24Hr = new BigDecimal( quitarComas(respSaldo[4]) );
                        saldoMesa = new BigDecimal( quitarComas(respSaldo[6]) );
                    }//fin if
                    EIGlobal.mensajePorTrace("-!?Cartera["+ saldoCart.toString() +"] Disponible["+ saldoDisp.toString() +"] Actual["+ saldoActu.toString() +"] 24Hrs.["+ saldo24Hr.toString() +"] Mesa Dinero["+ saldoMesa.toString() +"]", EIGlobal.NivelLog.INFO);
                    Vector regPosi = new Vector();
                    for(int i=0,indx=0; i<datPosicionArch.size(); i++,indx+=4)
                    {
                        String[] colPosi = new String[5];
                        colPosi[0] = ((String[])datPosicionArch.elementAt(i))[0];   //Emisora
                        colPosi[1] = ((String[])datPosicionArch.elementAt(i))[1];   //Titulos
                        colPosi[2] = ((String[])datPosicionArch.elementAt(i))[2];   //Promedio
                        colPosi[3] = ((String[])datPosicionArch.elementAt(i))[3];   //Precio
                        try
                        {   //Obtener Importe
                            BigDecimal tmp1 = new BigDecimal(colPosi[1]);
                            BigDecimal tmp2 = new BigDecimal(colPosi[3]);
                            BigDecimal tmp3 = new BigDecimal("0");
                            tmp3 = tmp1.multiply(tmp2);
                            colPosi[4] = tmp3.setScale(2,BigDecimal.ROUND_DOWN).toString();     //Importe
                        }catch(Exception e1){ colPosi[4] = "0"; }
                        EIGlobal.mensajePorTrace("-!?Emisora["+ colPosi[0] +"]\tTitulos["+ colPosi[1] +"]\tPromedio["+ colPosi[2] +"]\tPrecio["+ colPosi[3] +"]\tImporte["+ colPosi[4] +"]", EIGlobal.NivelLog.INFO);
                        sldCartera = sldCartera.add( new BigDecimal(colPosi[4]) );
                        regPosi.addElement(colPosi);
                    }//fin for i
                    for(int i=0; i<regPosi.size() ;i++)
                    {
                        String porCartera = "0";
                        try
                        {   //Obtener Porcentaje
                            BigDecimal tmp1 = new BigDecimal( ((String[])regPosi.elementAt(i))[4] );    //Importe
                            BigDecimal tmp2 = tmp1.multiply(new BigDecimal("100"));
                            tmp2 = tmp2.divide(sldCartera, 3);
                            porCartera = tmp2.setScale(2,BigDecimal.ROUND_DOWN).toString();     //Porcentaje
                        }catch(Exception e1){  }
                        if( (i%2)==0 ) color="textabdatobs";
                        else           color="textabdatcla";
                        tablaHTML += "  <tr>";
                        tablaHTML += "   <td align=\"left\"  class=\""+ color +"\">&nbsp; "+            ((String[])regPosi.elementAt(i))[0]    +"&nbsp;</td>\n";
                        tablaHTML += "   <td align=\"right\" class=\""+ color +"\">&nbsp; "+ formatear( ((String[])regPosi.elementAt(i))[1],0) +"&nbsp;</td>\n";
                        tablaHTML += "   <td align=\"right\" class=\""+ color +"\">&nbsp;$"+ formatear( ((String[])regPosi.elementAt(i))[2],8) +"&nbsp;</td>\n";
                        tablaHTML += "   <td align=\"right\" class=\""+ color +"\">&nbsp;$"+ formatear( ((String[])regPosi.elementAt(i))[3],8) +"&nbsp;</td>\n";
                        tablaHTML += "   <td align=\"right\" class=\""+ color +"\">&nbsp;$"+ formatear( ((String[])regPosi.elementAt(i))[4],2) +"&nbsp;</td>\n";
                        tablaHTML += "   <td align=\"right\" class=\""+ color +"\">&nbsp; "+ formatear( porCartera                         ,2) +"&nbsp;</td>\n";
                        tablaHTML += "  </tr>\n";
                    }//fin for
                }//fin else
                request.setAttribute("MenuPrincipal",session.getStrMenu());
                request.setAttribute("newMenu"      ,session.getFuncionesDeMenu());
                request.setAttribute("Encabezado"   ,CreaEncabezado("Consulta de Posici&oacute;n por cuenta de Banca especializada","Consultas &gt; Posici&oacute;n &gt; Por cuenta &gt; Banca Especializada", "s25040h",request));
                request.setAttribute("count"        ,cta_saldo);
                request.setAttribute("totalMesa"    ,formatear(saldoMesa.toString(),2));
                request.setAttribute("totalCart"    ,formatear(saldoCart.toString(),2));
                request.setAttribute("tabla"        ,tablaHTML);
                //TODO BIT CU1061
                /*
    			 * VSWF-BMB-I
    			 */
                if (Global.USAR_BITACORAS.trim().equals("ON")){
                try{
	                HttpSession sess = request.getSession();
	    	        BitaHelper bh = new BitaHelperImpl(request, session, sess);
	    			BitaTransacBean bt = new BitaTransacBean();
	    			bt = (BitaTransacBean)bh.llenarBean(bt);
	    			if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals
	    					(BitaConstants.EC_SALDO_CUENTA_BANCA)){
	    				//BIT CU1021
	    				bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_BANCA_CONS_POS_CUENTA);
	    			} else if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals
	    					(BitaConstants.EC_SALDO_CONS_CHEQUE)){
	    				bt.setNumBit(BitaConstants.EC_SALDO_CONS_CHEQUE_CONS_POS_CUENTA);
	    			}
	    			else{
	    			bt.setNumBit(BitaConstants.EC_POSICION_BANCA_CONS_TRAE_POS_SALDOS);
	    			}
	    			if(session.getContractNumber()!=null){
	    				bt.setContrato(session.getContractNumber());
	    			}
	    			if(cta_saldo!=null){
	    				if(cta_saldo.length()>20){
	    				 bt.setCctaOrig(cta_saldo.substring(0,20));
	    				}else{
	    					bt.setCctaOrig(cta_saldo.trim());
	    				}
	    			}
	    			if(tipOper!=null){
	    				bt.setServTransTux(tipOper.trim());
	    			}
	    			if(tramaRegresoServicio!=null){
		    			if(tramaRegresoServicio.substring(0,2).equals("OK")){
		    				bt.setIdErr(tipOper.trim()+"0000");
		    			}else if(tramaRegresoServicio.length()>8){
		    				bt.setIdErr(tramaRegresoServicio.substring(0,8));
		    			}else{
		    				bt.setIdErr(tramaRegresoServicio.trim());
		    			}
	    			}
	    			bt.setImporte(saldoCart.doubleValue());
    			BitaHandler.getInstance().insertBitaTransac(bt);
    			}catch(NumberFormatException e){}
                catch (SQLException e){
    				e.printStackTrace();
    			}catch(Exception e){
    				e.printStackTrace();
    			}
                }
    	        /*
    			 * VSWF-BMB-F
    			 */
                evalTemplate(IEnlace.BE_POSICION_TMPL,request,response);
                EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Terminar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
                return;
            }//fin if
        }catch(Exception e){ EIGlobal.mensajePorTrace("-!?Excepcion ConsultaPosicion["+ e +"]", EIGlobal.NivelLog.INFO); }
        despliegaPaginaError("Servicio de Consulta de Posici&oacute;n no disponible","Consulta de Posici&oacute;n por cuenta de Banca especializada", "Consultas &gt; Posici&oacute;n &gt; Por cuenta &gt; Banca Especializada", "s25040h", request,response);
        EIGlobal.mensajePorTrace("-!?***BE_TraePosicion.class Terminar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
    }

    public String obtenerSaldosArchivo(RandomAccessFile archivoSalida)
    {
        long   posAct    = 0;
        long   lngArch   = 0;
        String tramaArch = "";
        try
        {
            int numLineas = 0;
            archivoSalida.seek(0);
            lngArch = archivoSalida.length();
            do
            {
                tramaArch += archivoSalida.readLine();
                posAct = archivoSalida.getFilePointer();
                numLineas ++;
            }while( posAct<lngArch && numLineas<2 );
        }catch(Exception e){  }
        return tramaArch;
    }

    public Vector obtenerPosicionArchivo(RandomAccessFile archivoSalida)
    {
        long   posAct    = 0;
        long   lngArch   = 0;
        String tramaArch = "";
        Vector regPosi   = new Vector();
        try
        {
            int numLineas = 0;
            archivoSalida.seek(0);
            lngArch = archivoSalida.length();
            do
            {
                tramaArch = archivoSalida.readLine();
                if( numLineas>1 )
                    regPosi.addElement(desentrama(tramaArch,';'));
                posAct = archivoSalida.getFilePointer();
                numLineas ++;
            }while( posAct<lngArch );
        }catch(Exception e){  }
        return regPosi;
    }

    private String[] desentrama(String trama,char delimitador)
    {
        int i = 0;
        String as[] = null;
        for(int j=0; j<trama.length(); j++)
            if( trama.toCharArray()[j]==delimitador )
                i ++;
        try
        {
            as = new String[i];
            for(int k=0; k<=i ;k++)
            {
                as[k] = trama.substring(0,trama.indexOf(delimitador)).trim();
                trama = trama.substring(trama.indexOf(delimitador)+1,trama.length());
            }//fin for k
        }
        catch(Exception e){  }
        return as;
    }

    private String formatear(String format,int decimal)
    {
        try
        {
            BigDecimal importe = new BigDecimal(format).setScale(decimal,BigDecimal.ROUND_DOWN);
            NumberFormat formateo = NumberFormat.getNumberInstance();
            formateo.setMaximumFractionDigits(decimal);
            formateo.setMinimumFractionDigits(decimal);
            format = formateo.format(importe);
        }catch(Exception e){  }
        return format;
    }

    private String quitarComas(String cad)
    {
        String aux = "";
        try
        {
            for(int i=0; i<cad.length() ;i++)
                if( cad.toCharArray()[i]!=',' )
                    aux += ""+ cad.toCharArray()[i];
        }catch(Exception e){  }
        return aux;
    }
}

/*fsj*/