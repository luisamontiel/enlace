package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import java.sql.*;

//VSWF

public class CtasInternacionales extends BaseServlet
{
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}
	public static void main(String[] args) {
		String cadena = "AAGOCHZ1XXX  USA";

		System.out.println(cadena.substring(cadena.length() - 3));
	}
	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace ("CtasInternacionales.java::defaultAction()", EIGlobal.NivelLog.INFO);

		String numeroCuenta	= "";
		String titular		= "";
		String banco		= "";
		String pais			= "";
		String ciudad		= "";
		String divisa		= "";
		String cveABASWIFT	= "";

		/************* Modificacion para la sesion ***************/
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		Hashtable htResult = null;
		String m_menup     = "";
		boolean sesionvalida = SesionValida( req, res );/*Valida la sesion del usuario*/

		if (sesionvalida)
		{
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Consulta de Cuentas de Bancos Internacionales","Administraci&oacute;n y control &gt; Cuentas &gt; Internacionales","s26070h",req));

			String tablaCuentas  = "";
			String salidaTabla   = "";
			String color         = "";

           //modificación para integración pva 06/03/2002
           //************************************************
  			EIGlobal.mensajePorTrace( "***CtasInternacionales orev"+req.getParameter("prev"), EIGlobal.NivelLog.INFO);
  			EIGlobal.mensajePorTrace( "***CtasInternacionales next"+req.getParameter("next"), EIGlobal.NivelLog.INFO);
  			EIGlobal.mensajePorTrace( "***CtasInternacionales total"+req.getParameter("total"), EIGlobal.NivelLog.INFO);
  			for (int i = 0; i < 10; i++)EIGlobal.mensajePorTrace( "***************************************************", EIGlobal.NivelLog.INFO);
  				EIGlobal.mensajePorTrace( "*****************> " + req.getRequestURL().toString().split("/")[2], EIGlobal.NivelLog.INFO);
  			for (int i = 0; i < 10; i++)EIGlobal.mensajePorTrace( "***************************************************", EIGlobal.NivelLog.INFO);

			int total=0,prev=0,next=0;

			String paginacion="";
			if  ((req.getParameter("prev")!=null) &&

			      (req.getParameter("next")!=null) &&

			      (req.getParameter("total")!=null))

	         {

		         prev =  Integer.parseInt(req.getParameter("prev"));

                next =  Integer.parseInt(req.getParameter("next"));

		         total = Integer.parseInt(req.getParameter("total"));

		     }
             else

	         {
                 EIGlobal.mensajePorTrace( "***CtasInternacionales todos nullos", EIGlobal.NivelLog.INFO);
		         llamado_servicioCtasInteg(IEnlace.MAbono_TI," "," "," ",session.getUserID8()+".ambci",req);
                 total=getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
				 //TODO BIT CU5061, Consulta de Cuentas Internacionales
				/*VSWF HGG I*/
                 if (Global.USAR_BITACORAS.trim().equals("ON")){
	                try {
						BitaHelper bh = new BitaHelperImpl(req, session, ses);

						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_CUENTAS_INTER);
						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}

						BitaHandler.getInstance().insertBitaTransac(bt);
	                 } catch (SQLException e) {
	         			e.printStackTrace();
	         		} catch (Exception e) {
	         			e.printStackTrace();
	         		}
                 }
         		/*VSWF HGG F*/

		         prev=0;

		         if (total>Global.NUM_REGISTROS_PAGINA)

		           next=Global.NUM_REGISTROS_PAGINA;
		         else

			       next=total;
		     }

  	         EIGlobal.mensajePorTrace( "***CtasInternacionales orev"+prev, EIGlobal.NivelLog.INFO);

  	         EIGlobal.mensajePorTrace( "***CtasInternacionales  next"+next, EIGlobal.NivelLog.INFO);
  	         EIGlobal.mensajePorTrace( "***CtasInternacionales total"+total, EIGlobal.NivelLog.INFO);
             //Modificacion para integracion
			 String[][] arrCuentas = ObteniendoCtasInteg(prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
           	 if(arrCuentas!=null)
			 {
           		salidaTabla="<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>"+
			   "<tr><td class=tittabdat align=center>Numero de Cuenta</td>" +
			   "<td class=tittabdat align=center>Titular</td>" +
			   "<td class=tittabdat align=center>Banco</td>" +
			   "<td class=tittabdat align=center>Pa&iacute;s</td>" +
			   "<td class=tittabdat align=center>Ciudad</td>" +
			   "<td class=tittabdat align=center>Divisa</td>" +
			   "<td class=tittabdat align=center>Cve.ABA/SWIFT</td></tr>" +
			   "<td class=tittabdat align=center>Domicilio</td></tr>" + 
			   "<td class=tittabdat align=center>Ciudad Beneficiario</td></tr>" + 
			   "<td class=tittabdat align=center>N&uacute;mero de Identificaci&oacute;n</td></tr>" +
			   "<td class=tittabdat align=center>Pa&iacute;s Beneficiario</td></tr>";

       			for ( int i = 1; i <= Integer.parseInt( arrCuentas[0][0] ); i++ )
				{
				  if ( ( i % 2 ) == 0 )
					color = "textabdatobs";
				  else
					color = "textabdatcla";

					numeroCuenta	= arrCuentas[i][1];
					titular			= arrCuentas[i][2];
					banco			= arrCuentas[i][3];

					if(arrCuentas[i][4].trim().length() > 0){
						if(arrCuentas[i][4].substring(arrCuentas[i][4].length() - 3).equals("USA")){
							pais	= "USA";
							cveABASWIFT		= arrCuentas[i][8];
						}else{
							cveABASWIFT	= arrCuentas[i][5];
							pais		= arrCuentas[i][4].substring(arrCuentas[i][4].length() - 4);
						}
					}
					ciudad			= arrCuentas[i][6];
					divisa			= arrCuentas[i][7];

					salidaTabla+="<tr><td class='"+ color +"'>"+numeroCuenta+"</td>"
								+"<td class='"+ color +"'>"+titular+"&nbsp;</td>"
								+"<td class='"+ color +"'>"+banco+"&nbsp;</td>"
								+"<td class='"+ color +"'>"+pais+"&nbsp;</td>"
								+"<td class='"+ color +"'>"+ciudad+"&nbsp;</td>"
								+"<td class='"+ color +"'>"+divisa+"&nbsp;</td>"
								+"<td class='"+ color +"'>"+cveABASWIFT+"&nbsp;</td></tr>"
								+"<td class='"+ color +"'>Calle, Numero Ext, Numero Int, Colonia, Provincia, Código Postal&nbsp;</td></tr>"
								+"<td class='"+ color +"'>Nombre Ciudad&nbsp;</td></tr>"
								+"<td class='"+ color +"'>12345678&nbsp;</td></tr>";
				}
				salidaTabla+="</table>";

			    if (total>Global.NUM_REGISTROS_PAGINA)

		        {


                 paginacion="<table  width=450 align=center>";

		          if(prev > 0)

                   paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:atras();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";

                 if(next < total)

                 {

                   if((next + Global.NUM_REGISTROS_PAGINA) <= total)

 	                  paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(next -prev).toString() + " ></A></td></TR>";

                   else

                     paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(total-next).toString() + " ></A></td></TR>";

                 }

                 paginacion+="</table><br><br>";

    	          EIGlobal.mensajePorTrace( "***paginacion :"+paginacion, EIGlobal.NivelLog.INFO);

		        }
                if ( Integer.parseInt(arrCuentas[0][0])>0)
				{
					req.setAttribute("total", ""+total);

					req.setAttribute("prev", ""+prev);

					req.setAttribute("next", ""+next);

					req.setAttribute("paginacion", paginacion);

					req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);

 					req.setAttribute("Cuentas",salidaTabla);
 					req.setAttribute("tipoCta", "INTERNACIONAL");
					evalTemplate(IEnlace.CTAS_OTROS_BANCOS_TMPL, req, res);
                }
                else
   				  despliegaPaginaError("No se encontraron Cuentas Internacionales registradas.","Consultas de Cuentas de Bancos Internacionales","Administraci&oacute;n y Control &gt; Cuentas &gt; Consultas", req, res);


			 }
			 else
   				despliegaPaginaError("No se encontraron Cuentas Internacionales registradas.","Consultas de Cuentas de Bancos Internacionales","Administraci&oacute;n y Control &gt; Cuentas &gt; Consultas", req, res);
        }
    else if(sesionvalida)
    	{
      		req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
        	evalTemplate(IEnlace.ERROR_TMPL, req, res);
    	}

       return;
    }

	public int posCar(String cad,char car,int cant)
	{
	int result = 0, pos = 0;
	for (int i=0;i<cant;i++)
		{
			result = cad.indexOf(car,pos);
			pos    = result+1;
		}
	return result;
	}
	static protected String _defaultTemplate ="EnlaceMig/CtasInternacionales";
}