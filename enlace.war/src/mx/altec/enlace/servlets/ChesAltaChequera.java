package mx.altec.enlace.servlets;import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import java.lang.Number.*;
import java.io.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class ChesAltaChequera extends BaseServlet{
    String fecha_hoy		= "";
    String strFecha			= "";
    int salida				= 0;
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {


      HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");


			String usuario			= "";
			String contrato			= "";
			String clave_perfil		= "";
			String strSalida		= "";
			String cuenta			= "";
            String mensaje			= "";
			String AmbCod			= "";
			int  EXISTE_ERROR		= 0;


		if ( SesionValida( req, res ) ){
			usuario			= session.getUserID8();
	    	contrato		= session.getContractNumber();
			clave_perfil	= session.getUserProfile();
			AmbCod			= TraeAmbiente(usuario,contrato,req);
			EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &Inicia clase&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &usuario:"+usuario+"&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &contrato:"+contrato+"&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &perfil:"+clave_perfil+"&", EIGlobal.NivelLog.INFO);


			String ventana = (String) req.getParameter("ventana");
			EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &ventana:"+ventana+"&", EIGlobal.NivelLog.INFO);
			if( ventana.equals("0") ){
				//TODO:BIT CU 4211 El cliente entra al flujo
				/*
	    		 * VSWF ARR -I
	    		 */
				if (Global.USAR_BITACORAS.trim().equals("ON")){
					try{
					BitaHelper bh = new BitaHelperImpl(req, session,sess);
					bh.incrementaFolioFlujo(req.getParameter(BitaConstants.FLUJO));
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_CHE_SEG_MAN_CUENTAS_ALTA_ENTRA);
					if(contrato!=null && !contrato.equals(""))
						bt.setContrato(contrato);
					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
	    		/*
	    		 * VSWF ARR -F
	    		 */
				InicioChequera( req, res , usuario, strSalida, AmbCod, EXISTE_ERROR , contrato, clave_perfil);
			}
			else if( ventana.equals("1") )
				AltaChequera( req, res,  usuario , contrato, clave_perfil, strSalida, mensaje, EXISTE_ERROR );


		}

		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &Termina clase&", EIGlobal.NivelLog.INFO);
	} // fin execute

	public int InicioChequera(HttpServletRequest req, HttpServletResponse res, String usuario, String strSalida, String AmbCod, int  EXISTE_ERROR, String contrato, String clave_perfil)
			throws ServletException, IOException {

      HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");
	  EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &inicia m�todo InicioChequera()&", EIGlobal.NivelLog.INFO);
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
        int numeroCuentas		= 0;
		int totalCuentas		= 0;
		numeroCuentas	= 0;
		EXISTE_ERROR	= 0;
		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);

		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		EIGlobal.mensajePorTrace( "***Cuentas registradas : " + totalCuentas, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***uentas NO registradas : " + numeroCuentas, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &cuentas registradas:"+totalCuentas+"&", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &cuentas NO registradas:"+numeroCuentas+"&", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);

		req.setAttribute("titulo","Alta de Chequera Seguridad");
		req.setAttribute("FechaHoy", ObtenFecha(false) );
		req.setAttribute("ContUser",ObtenContUser(req));
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Alta de Chequera Seguridad ","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Cuentas &gt; Alta","s25930h",req) );

		session.setModuloConsultar(IEnlace.MAlta_ctas_cheq_seg);

		if ( EXISTE_ERROR ==1 )
			despliegaPaginaError(strSalida,"Alta de Chequera de Seguridad ","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Cuentas &gt; Alta","s25930h", req, res);
		else
			evalTemplate("/jsp/ches/ChesAltaChequera.jsp", req, res );

		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &termina m�todo InicioChequera()&", EIGlobal.NivelLog.INFO);
		return salida;
	}

	public int AltaChequera(HttpServletRequest req, HttpServletResponse res, String usuario, String contrato, String clave_perfil, String strSalida, String mensaje, int  EXISTE_ERROR)
			throws ServletException, IOException {

      HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");


		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &inicia m�todo AltaChequera()&", EIGlobal.NivelLog.INFO);

		String cboCuentas=(String) req.getParameter("cboCuentas");
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");

		EIGlobal.mensajePorTrace( "***Cuenta : " + cboCuentas, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &cuenta:"+cboCuentas+"&", EIGlobal.NivelLog.INFO);

		EXISTE_ERROR = 0;
		StringBuffer loc_trama	= new StringBuffer ("");   /* <--- */
		String glb_trama	= "";
		StringBuffer trama_entrada = new StringBuffer ("");   /* <--- */
		String trama_salida	= "";
		String cuenta       = "";
		String cabecera		= "1EWEB";
		String operacion	= "CHSM"; //Modif pva 09-01-2003-Nuevo prefijo //"ABRE";  // prefijo del servicio ( alta-baja de chequera)
		String resultado	= "";
		int primero			= 0;
		int ultimo			= 0;


		cuenta			= cboCuentas;
		fecha_hoy		= ObtenFecha();


		if(session.getFacultad(session.FAC_ALTA_CTA_CONTRATO))
		{
			EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &facultades OK&", EIGlobal.NivelLog.INFO);

			if ( cboCuentas.equals("") ) mensaje = "Debe seleccionar una cuenta.";

			else // cuenta seleccionada
			{

				cuenta =  EIGlobal.BuscarToken(cboCuentas.trim(), '|', 1);


				loc_trama	= new StringBuffer (FormateaContrato(contrato));
				loc_trama.append ("@");
				loc_trama.append  (FormateaContrato( cuenta ));
				loc_trama.append  ("@");
				loc_trama.append  ("A");
				loc_trama.append  ("@"); // (A) alta de chequera

				trama_entrada	= new StringBuffer (cabecera);
				trama_entrada.append ("|");
				trama_entrada.append (usuario);
				trama_entrada.append ("|");
				trama_entrada.append (operacion);
				trama_entrada.append ("|");
				trama_entrada.append (contrato);
				trama_entrada.append ("|");
				trama_entrada.append (usuario);
				trama_entrada.append ("|");
				trama_entrada.append (clave_perfil);
				trama_entrada.append ("|");
				trama_entrada.append (loc_trama );

				EIGlobal.mensajePorTrace( "***trama de entrada : " + trama_entrada, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &ejecutando servicio ...&", EIGlobal.NivelLog.DEBUG);
				try{
					ServicioTux tuxGlobal = new ServicioTux();
					//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

					Hashtable hs = tuxGlobal.web_red(trama_entrada.toString());
					trama_salida = (String) hs.get("BUFFER");
					EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);
					}catch( java.rmi.RemoteException re ){
						re.printStackTrace();
				    }catch(Exception e) {}

				if(trama_salida.startsWith("OK"))
				{
					primero = trama_salida.indexOf( (int) '@');
					ultimo  = trama_salida.lastIndexOf( (int) '@');
					resultado = trama_salida.substring(primero+1, ultimo);
					mensaje = resultado;

//					TODO: BIT CU 4211, A6
						  /*
						   * VSWF ARR -I
						   */
					if (Global.USAR_BITACORAS.trim().equals("ON")){
						  try{
						  BitaHelper bh = new BitaHelperImpl(req, session,sess);
						  BitaTransacBean bt = new BitaTransacBean();
						  bt = (BitaTransacBean)bh.llenarBean(bt);
						  bt.setNumBit(BitaConstants.ES_CHE_SEG_MAN_CUENTAS_ALTA_ALTA_CHEQUERA);
						  bt.setContrato(contrato);
						  if(operacion!=null && !operacion.equals(""))
							  bt.setServTransTux(operacion);
						  if(trama_salida!=null){
								 if(trama_salida.substring(0,2).equals("OK")){
									   bt.setIdErr(operacion+"0000");
								 }else if(trama_salida.length()>8){
									  bt.setIdErr(trama_salida.substring(0,8));
								 }else{
									  bt.setIdErr(trama_salida.trim());
								 }
								}
						  if(cuenta!=null && !cuenta.equals(""))
							  bt.setCctaOrig(cuenta);
						  BitaHandler.getInstance().insertBitaTransac(bt);
						  }catch (SQLException e){
							  e.printStackTrace();
						  }catch(Exception e){
							  e.printStackTrace();
						  }
					}
						  /*
						   * VSWF ARR -F
						   */
				}
				else mensaje = trama_salida;

			}

			if ( mensaje.trim().length() == 0 ) mensaje = "La transacci&oacute;n ha sido rechazada.";
		}

		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);

		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &se deslpliegan datos en pantalla&", EIGlobal.NivelLog.INFO);

		req.setAttribute("titulo","Alta de Chequera Seguridad");
		req.setAttribute("FechaHoy", ObtenFecha(false) );
		req.setAttribute("ContUser",ObtenContUser(req));
		req.setAttribute("cboCuentas", strSalida);
		req.setAttribute("mensaje", mensaje);
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Alta de Chequera Seguridad ","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Cuentas &gt; Alta","s25930h",req) );

		despliegaPaginaError( mensaje ,"Alta de Chequera Seguridad ","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Cuentas &gt; Alta","s25930h", req, res);



		EIGlobal.mensajePorTrace( "***ChesAltaChequera.class  &termina m�todo AltaChequera()&", EIGlobal.NivelLog.INFO);

		return salida;
	}

/*----------------------------------------------------------------------------------------------------*/

	String FormateaContrato (String ctr_tmp)
	{
		int i			= 0;
		StringBuffer temporal = new StringBuffer ("");   /* <--- */
		String caracter	= "";

		for (i = 0 ; i < ctr_tmp.length() ; i++)
		{
			caracter = ctr_tmp.substring(i, i+1);
			if (caracter.equals("-"))
			{
				// no se toma en cuenta el guion
			}
			else
			{
				temporal.append (caracter );
			}
		}

		return temporal.toString();
	}

} // fin clase