package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.io.FileWriter;
import java.io.IOException;

//VSWF


public class CtasOtrosBancos extends BaseServlet
{
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
		defaultAction( request, response );
	}
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		log( "CtasOtrosBancos.java::defaultAction()" );
		boolean sesionvalida = SesionValida( request, response );
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta de Cuentas de otros bancos",
				"Administraci&oacute;n y control &gt; Cuentas &gt; Consulta","s26070h", request));
		request.setAttribute("Fecha", ObtenFecha());
		request.setAttribute("Hora", ObtenHora());
		request.setAttribute("ContUser",ObtenContUser(request));

		/*Valida la sesion del usuario*/
		if ( sesionvalida ) {


			//Modificacion para integracion
			//String strCuentas2 = session.getCuentasOtrosBancos();
			//String[] Cuentas = desentramaC(strCuentas2,'@');
			//***********************************************

           //modificación para integración pva 06/03/2002

           //***********************************************

  	         EIGlobal.mensajePorTrace( "***CtasOtrosBancos orev"+request.getParameter("prev"), EIGlobal.NivelLog.INFO);
  	         EIGlobal.mensajePorTrace( "***CtasOtrosBancos next"+request.getParameter("next"), EIGlobal.NivelLog.INFO);
  	         EIGlobal.mensajePorTrace( "***CtasOtrosBancos total"+request.getParameter("total"), EIGlobal.NivelLog.INFO);

		     int total=0,prev=0,next=0;

		     String paginacion="";
             if  ((request.getParameter("prev")!=null) &&
			      (request.getParameter("next")!=null) &&
			      (request.getParameter("total")!=null))

	         {

		         prev =  Integer.parseInt(request.getParameter("prev"));
                 next =  Integer.parseInt(request.getParameter("next"));
		         total = Integer.parseInt(request.getParameter("total"));

		     }
             else

	         {
                 EIGlobal.mensajePorTrace( "***CtasOtrosBancos todos nullos", EIGlobal.NivelLog.INFO);
		         llamado_servicioCtasInteg(IEnlace.MDep_Inter_Abono," "," "," ",session.getUserID8()+".ambci", request);
                 total=getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");

		         prev=0;

		         if (total>Global.NUM_REGISTROS_PAGINA)

		           next=Global.NUM_REGISTROS_PAGINA;
		         else

			       next=total;
		     }
  	         EIGlobal.mensajePorTrace( "***CtasOtrosBancos orev"+prev, EIGlobal.NivelLog.INFO);
  	         EIGlobal.mensajePorTrace( "***CtasOtrosBancos next"+next, EIGlobal.NivelLog.INFO);
  	         EIGlobal.mensajePorTrace( "***CtasOtrosBancos total"+total, EIGlobal.NivelLog.INFO);

            //***********************************************

			String tablaCuentas="<table border=0 cellspacing=2 cellpadding=3 bgcolor=#FFFFFF>"+
					"<tr><td align='center' class=tittabdat>Numero de Cuenta</td>"+
					"<td align='center' class=tittabdat>Descripci&oacute;n</td>"+
					"<td align='center' class=tittabdat>Banco</td>"+
					"<td align='center' class=tittabdat>Plaza</td>"+
					"<td align='center' class=tittabdat>Sucursal</td></tr>";
            //Modificacion para integracion
			String[][] arrCuentas = ObteniendoCtasInteg(prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");

			// modificacion para agregar la opcion de exportacion del archivo
			String pathFileDownload = "";
			/*VSWF*/
			boolean archivoExport = false;
			/*VSWF*/
			try {
				archivoExport = GeneraArchivoExport(session.getUserID8()+".ambci",request, response);
				if(archivoExport){
					pathFileDownload = "/Download/"+session.getUserID8()+".ambcie";
				}
			}catch( Exception e ) {
				EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes Error obteniendo parametros>>" + e.toString(), EIGlobal.NivelLog.INFO);
				e.printStackTrace();
			}
			//TODO BIT CU5061, Consulta de otros Bancos Nacionales
			/*VSWF-HGG-I*/
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				try {
				BitaHelper bh = new BitaHelperImpl(request, session, sess);

				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_OTROS_BANCOS);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				bt.setNombreArchivo(session.getUserID8()+".ambcie");
				if (archivoExport == false){
					bt.setIdErr("ACCL9999");//false
				}else{
					if(archivoExport == true)
						bt.setIdErr("ACCL0000");//true
				}


					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			/*VSWF-HGG-F*/

			//String[][] arrCuentas=new String[Integer.parseInt(Cuentas[0]+1)][7];
			String color = "";
			for ( int i = 1; i <= Integer.parseInt( arrCuentas[0][0] ); i++ ) {
				if ( ( i % 2 ) == 0 )
					color = "textabdatobs";
				else
					color = "textabdatcla";

				/*Modificacion para integracion
			    arrCuentas[i] = desentramaC( Cuentas[i]+"|",'|');
				tablaCuentas += "<tr><td align='center' class="+ color +">"+arrCuentas[i][2]+
						"&nbsp;</td><td class="+ color +">"+arrCuentas[i][3]+
						"&nbsp;</td><td class="+ color +">"+descBanco(arrCuentas[i][6])+
						"&nbsp;</td><td class="+ color +">"+descPlaza(arrCuentas[i][4])+
						"&nbsp;</td><td align='center' class="+ color +">"+arrCuentas[i][5]+"&nbsp;</td></tr>";*/
				System.out.println("La clave de la plaza es: " + arrCuentas[i][4]);
                tablaCuentas += "<tr><td align='center' class="+ color +">"+arrCuentas[i][1]+
						"&nbsp;</td><td class="+ color +">"+arrCuentas[i][3]+
						"&nbsp;</td><td class="+ color +">"+descBanco(arrCuentas[i][2])+
						"&nbsp;</td><td class="+ color +">"+descPlaza(arrCuentas[i][4])+
						"&nbsp;</td><td align='center' class="+ color +">"+arrCuentas[i][5]+"&nbsp;</td></tr>";

			}
			tablaCuentas += "</table>";

            //modificación para integración pva 06/03/2002

            //***********************************************
            EIGlobal.mensajePorTrace("*****total "+total, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("NUM_REGISTROS_PAGINA "+Global.NUM_REGISTROS_PAGINA, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("prev "+prev, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("next "+next, EIGlobal.NivelLog.INFO);
			if (total>Global.NUM_REGISTROS_PAGINA)
		    {
              paginacion="<table  width=450 align=center>";
		      if(prev > 0)
                paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:history.back();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";
              if(next < total)
              {
                if((next + Global.NUM_REGISTROS_PAGINA) <= total)
 	              paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(next -prev).toString() + " ></A></td></TR>";
                else
                  paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(total-next).toString() + " ></A></td></TR>";
              }
              paginacion+="</table><br><br>";
    	      EIGlobal.mensajePorTrace( "***paginacion :"+paginacion, EIGlobal.NivelLog.INFO);
		    }
		    request.setAttribute("pathFileDownload", pathFileDownload);
			request.setAttribute("total", ""+total);
		    request.setAttribute("prev", ""+prev);
		    request.setAttribute("next", ""+next);
			request.setAttribute("paginacion", paginacion);
			request.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
            //***********************************************
			request.setAttribute("newMenu", session.getFuncionesDeMenu());
			request.setAttribute( "Cuentas", tablaCuentas );
			evalTemplate( IEnlace.CTAS_OTROS_BANCOS_TMPL, request, response );
		} else {
			request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}
    }

	private boolean GeneraArchivoExport(String nombreArchivoExport, HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException{
		System.out.println("Ya estoy en GeneraArchivoExport el archivo es:" + nombreArchivoExport);
		String[] vectorCuentas = actualizaCLABE("D", nombreArchivoExport, request, response);
		System.out.println("Regrese de actualizaCLABE ........" + nombreArchivoExport);
		System.out.println("Vector de cuentas ---------->" + vectorCuentas[1] + "<");
		boolean resp = false;
        /////  para simular una respuesta con error vectorCuentas[1]="ACCL9999";
		try{
			if (vectorCuentas[1].equals("ACCL9999")){
				//request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP );
				//evalTemplate( IEnlace.ERROR_TMPL, request, response );
				resp=false;
			}else{
				if(vectorCuentas[1].equals("ACCL0000"))
					resp=true;
			}
		}catch( Exception e ) {
			EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes Error obteniendo parametros>>" + e.toString(), EIGlobal.NivelLog.INFO);
			e.printStackTrace();
			resp=false;
		}
		return resp;
	}
}