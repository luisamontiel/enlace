
package mx.altec.enlace.servlets;
import java.io.*;
import java.rmi.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class ConsultaCancelacion extends BaseServlet{


	//=============================================================================================
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &Inicia clase&", EIGlobal.NivelLog.INFO);

		boolean sesion_valida = SesionValida( req, res);

		String m_menup = "";

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if ( sesion_valida && session.getFacultad(session.FAC_CONSULTA_BITACORA) ) {

			m_menup = session.getstrMenu();
			if(m_menup==null)
				m_menup = "";

			req.setAttribute( "MenuPrincipal", m_menup );
			req.setAttribute( "ContUser",      ObtenContUser(req) );
			req.setAttribute( "Fecha",         ObtenFecha() );
			inicio( req, res ); // m�todo principal

		}else if( sesion_valida ) {
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora","s25320h",req) );
			req.setAttribute("Mensaje", "P&aacute;gina no disponible en este momento, intente m&aacute;s tarde !.");
			evalTemplate( "/jsp/coConsultPrueb.jsp", req, res);
		}
		EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &Termina clase&", EIGlobal.NivelLog.INFO);
	}

	//=============================================================================================

	public int inicio( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &inicia m�todo inicio()&", EIGlobal.NivelLog.INFO);

		String m_menu = "";

		req.setAttribute( "Fecha", ObtenFecha() );

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		m_menu = session.getstrMenu();

		if (m_menu==null)
			m_menu = "";

		req.setAttribute( "MenuPrincipal",	m_menu );

		String archivo_servicio	= "";
		String archivo_filtro	= IEnlace.LOCAL_TMP_DIR + "/"+ session.getUserID() + ".bit";
		String salida			= "";
		String NOMBRE_ARCHIVO	= "";
		String ARCHIVO_EXPORTAR	= "";
		String SEMAFORO[]		= new String[11];
		String FECHAFIN			= "";
		String FECHAINI			= "";
		String CONTRATO			= "";
		String MISPARAMETROS	= "";
		String [] REFERENCIA	= new String [1];
		String IMPORTE			= "";
		String USUARIO1			= "";
		String USUARIO			= "";
		String CUENTA			= "";
		String [] ERROR			= new String [1];
		String PREV				= "";
		String NEXT				= "";
		String TOTAL			= "";

		int llamada_servicio = 0;
		int total_registros	 = 0;
		int parametros		 = 0;
		int exportar		 = 0;
		int filtro = 0;


	boolean TRANSFERENCIAS	= false;
	boolean INVERSIONES		= false;
	boolean INTERBANCARIAS	= false;
	boolean ACEPTADAS		= false;
	boolean RECHAZADAS		= false;
	boolean PROGRAMADAS		= false;
	boolean PENDIENTES		= false;
	boolean [] regreso = new boolean [7];

		PREV  = ( String) req.getParameter( "prev" );
		NEXT  = ( String) req.getParameter( "next" );
		TOTAL = ( String) req.getParameter( "total" );
		//System.out.println(PREV+" "+NEXT+" "+TOTAL);
		for ( int i = 0 ; i < 11  ; i++ ) SEMAFORO[i] = "0"; // 0 0 0 0 0000000

		// log( "Inicializando m�scara de filtros : "+SEMAFORO[0]+SEMAFORO[1]+SEMAFORO[2]+SEMAFORO[3]+SEMAFORO[4]+SEMAFORO[5]+SEMAFORO[6]+SEMAFORO[7]+SEMAFORO[8]+SEMAFORO[9]+SEMAFORO[10] );

		ERROR[0] = "No hay registros asociados a su consulta";

		// para la primer ventana
        if( PREV == null && NEXT == null ){
			//System.out.println("**********************************  ENTRAMOS A LA PRIMERA VENTANA **********************************");
			String hoy		= ( String) req.getParameter( "deldia" );

			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &primera ventana ...&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &obteniendo par�metros del ambiente ...&", EIGlobal.NivelLog.INFO);

			CUENTA		= ( String) req.getParameter( "cuenta" );
			USUARIO		= ( String) req.getParameter( "usuario" );
			USUARIO1	= ( String) req.getParameter( "usuario" );
			CONTRATO	= ( String) req.getParameter( "contrato" );
			IMPORTE		= ( String) req.getParameter( "Importe" );
			REFERENCIA [0]	= ( String) req.getParameter( "Refe" );
			FECHAINI	= ( String) req.getParameter( "fecha1" );
			FECHAFIN	= ( String) req.getParameter( "fecha2" );
			try{
			System.out.println("\n\n\nContrato = "+CONTRATO);
			String el_ = (String) req.getAttribute("contrato");

			System.out.println("Contrato ="+el_);
			} catch (Exception e) {
				e.printStackTrace();
			}


			//System.otu.println();
			//req.setAttribute("contrato", CONTRATO);

			if ( USUARIO1.trim().length() > 1)
				SEMAFORO[0]	= "1";
			else
				USUARIO1="0";

			if ( CUENTA.length() > 1 )		  SEMAFORO[1]	= "1";
			if ( IMPORTE.length() > 0 )		  SEMAFORO[2]	= "1";
			if ( REFERENCIA[0].length() > 1 ) SEMAFORO[3]	= "1";

			if ( USUARIO.length() < 2 ) USUARIO	= session.getUserID();
			if ( CONTRATO==null )  CONTRATO	    = session.getContractNumber();

			NOMBRE_ARCHIVO   = CONTRATO + USUARIO + "bit.doc";
			ARCHIVO_EXPORTAR = IEnlace.DOWNLOAD_PATH + NOMBRE_ARCHIVO;

			// filtros de la consulta
			if(( String) req.getParameter("ChkTranf")!= null)         {	TRANSFERENCIAS	=true; SEMAFORO[4] ="1"; }
			if(( String) req.getParameter("ChkInversiones") != null)  {	INVERSIONES		=true; SEMAFORO[5] ="1"; }
			if(( String) req.getParameter("ChkInterbancario") != null){ INTERBANCARIAS	=true; SEMAFORO[6] ="1"; }
			if(( String) req.getParameter("ChkAceptadas") != null)	  {	ACEPTADAS		=true; SEMAFORO[7] ="1"; }
			if(( String) req.getParameter("ChkRechazadas") != null)   {	RECHAZADAS		=true; SEMAFORO[8] ="1"; }
			if(( String) req.getParameter("ChkProgramadas") != null)  {	PROGRAMADAS		=true; SEMAFORO[9] ="1"; }
			if(( String) req.getParameter("ChkPendientes") != null)   {	PENDIENTES		=true; SEMAFORO[10]="1"; }

			session.setfechaini( FECHAINI );
			session.setfechafin( FECHAFIN );

			llamada_servicio = actualizaBitacora(USUARIO,CONTRATO, req, res);  // llamada al servicio e inicializaci�n de archivo

			// Error en el servicio
			if ( llamada_servicio != 0 ){
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &error en el servicio&", EIGlobal.NivelLog.INFO);
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute( "Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora",req) );
				req.setAttribute("Mensaje", "Por el momento no es posible atender su consulta, intente m&aacute;s tarde!");
				evalTemplate( "/jsp/coConsultPrueb.jsp",req,res);
				return 1;
			}else{
				// servicio OK

				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &servicio OK ...&", EIGlobal.NivelLog.INFO);

				archivo_servicio = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID();

				// n�mero de lineas del archivo
				total_registros = totalRegistros(ERROR, archivo_servicio );
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &total de registros: "+total_registros+"&", EIGlobal.NivelLog.INFO);

				session.settotal( new Integer( total_registros ).toString() ); // se guardan en el ambiente

				// existen registros
				if( total_registros >0 ){
					EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &filtrando archivo ...&", EIGlobal.NivelLog.INFO);
					filtro = filtrarArchivo(INTERBANCARIAS,CUENTA,USUARIO1,IMPORTE,REFERENCIA,SEMAFORO,archivo_servicio, archivo_filtro, total_registros );


					if(filtro == 0)// filtro del archivo OK
					{
						EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &archivo filtro OK ...&", EIGlobal.NivelLog.INFO);

						session.setArchivoBit( archivo_filtro ); // se guarda en el ambiente

						total_registros = totalRegistros(ERROR, session.getArchivoBit() );
						EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &registros filtrados: "+total_registros+"&", EIGlobal.NivelLog.INFO);

						session.settotal( new Integer( total_registros ).toString() ); // Se guarda total de registros
						EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &creando archivo de exportaci�n ...&", EIGlobal.NivelLog.INFO);
						exportar = exportarArchivo(NOMBRE_ARCHIVO, session.getArchivoBit() , ARCHIVO_EXPORTAR, total_registros, req, res );
						req.setAttribute( "CONTRATO", CONTRATO);

						if ( exportar != 0 )
							return exportar;
						if ( total_registros>1000 )// demasiados registros
						{
							String boton_exportar = "";

							salida += "		<Form name =\"frmbit\" method = \"POST\">\n";
							salida += "			<input type=\"Hidden\" name=\"total\" value= \"" + session.gettotal() + "\">\n";
							salida += "			<input type=\"Hidden\" name=\"prev\" value= \"" + 0 + "\">\n";
							salida += "			<input type=\"Hidden\" name=\"next\" value= \"" + 0 + "\">\n";
							salida += "			Existen m&aacute;s de 1000 movimientos\n";
							salida += "		</form>\n";

							boton_exportar  = "<a href=\"/Download/" + CONTRATO + USUARIO + "bit.doc"  + "\">\n";
							boton_exportar += "<img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" width=85 height=22 alt=Exportar></a>";

							req.setAttribute( "Output1",salida);
							req.setAttribute( "newMenu", session.getFuncionesDeMenu());
							req.setAttribute( "MenuPrincipal", session.getStrMenu());
							req.setAttribute( "Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora",req) );
							req.setAttribute( "Mensaje", salida );
							req.setAttribute( "Boton", boton_exportar );
							//req.setAttribute( "CONTRATO", CONTRATO);

							evalTemplate( "/jsp/EI_Mensaje1000.jsp", req, res);
						}else if( total_registros >= 30 ) {
							regreso[6] = TRANSFERENCIAS;
							regreso[5] = INVERSIONES;
							regreso[4] = INTERBANCARIAS;
							regreso[3] = ACEPTADAS;
							regreso[2] = RECHAZADAS;
							regreso[1] = PROGRAMADAS;
							regreso[0] = PENDIENTES;

							muestraBitacora(IMPORTE,USUARIO,CUENTA,regreso,REFERENCIA,CONTRATO,FECHAINI,FECHAFIN, session.getArchivoBit(), 0, 30 ,req, res);
							}

						else if( total_registros < 30 && total_registros >=1 ) {
							regreso[6] = TRANSFERENCIAS;
							regreso[5] = INVERSIONES;
							regreso[4] = INTERBANCARIAS;
							regreso[3] = ACEPTADAS;
							regreso[2] = RECHAZADAS;
							regreso[1] = PROGRAMADAS;
							regreso[0] = PENDIENTES;

							muestraBitacora(IMPORTE,USUARIO,CUENTA,regreso,REFERENCIA,CONTRATO,FECHAINI,FECHAFIN, session.getArchivoBit(), 0, total_registros, req, res );
							}

						else{
							req.setAttribute( "Output1","<h3>No hay registros asociados a su consulta !");
							req.setAttribute( "newMenu", session.getFuncionesDeMenu());
							req.setAttribute( "MenuPrincipal", session.getStrMenu());
							req.setAttribute( "Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora",req) );
							req.setAttribute( "Mensaje", "No hay registros asociados a su consulta !"  );
							evalTemplate( "/jsp/coConsultPrueb.jsp", req, res);
						}
					}else{
						// error en el archivo filtro
						EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &error en archivo filtro ...&", EIGlobal.NivelLog.INFO);
						req.setAttribute( "Output1","<h3>Error inesperado !");
						req.setAttribute( "newMenu", session.getFuncionesDeMenu());
						req.setAttribute( "MenuPrincipal", session.getStrMenu());
						req.setAttribute( "Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora",req) );
						req.setAttribute( "Mensaje", "Error inseperado !." );
						evalTemplate( "/jsp/coConsultPrueb.jsp", req, res);
					}
				}else
				{
					// no hay registros
					req.setAttribute("Output1","<h3>" + ERROR[0] + "!" );
					req.setAttribute("newMenu", session.getFuncionesDeMenu());
					req.setAttribute("MenuPrincipal", session.getStrMenu());
					req.setAttribute( "Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora",req) );
					req.setAttribute("Mensaje",ERROR[0]);
					evalTemplate( "/jsp/coConsultPrueb.jsp", req, res);
				} // if( total_registros >0 )

			} // if ( llamada_servicio )
		}else{
			//System.out.println("**********************************  ENTRAMOS A SIGUIENTE **********************************");

			// no es la primer ventana
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &ventana siguiente ...&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &obteniendo par�metros ...&", EIGlobal.NivelLog.INFO);

			MISPARAMETROS	= ( String) req.getParameter( "parametros" );

			String [] elimportetemp = new String [1];
			elimportetemp[0] = IMPORTE;
			String USUARIO_ [] = new String [1];
			USUARIO_[0] = USUARIO;

			String CONTRATO_ [] = new String [1];
			CONTRATO_[0] = CONTRATO;

			String CUENTA_ [] = new String [1];
			CUENTA_[0] = CUENTA;
			regreso[6] = TRANSFERENCIAS;
			regreso[5] = INVERSIONES;
			regreso[4] = INTERBANCARIAS;
			regreso[3] = ACEPTADAS;
			regreso[2] = RECHAZADAS;
			regreso[1] = PROGRAMADAS;
			regreso[0] = PENDIENTES;

			//System.out.println("MISPARAMETROS =  "+MISPARAMETROS);
			String fechas[] = new String[2];
			fechas[0] ="";
			fechas[1] ="";

			parametros = obtenParametros(fechas, regreso,CUENTA_,USUARIO_,elimportetemp,REFERENCIA,MISPARAMETROS,CONTRATO_,SEMAFORO,req);
			FECHAINI	= fechas[0];
			FECHAFIN	= fechas[1];

			//System.out.println("******"+parametros);

			IMPORTE = elimportetemp [0];
			USUARIO = USUARIO_[0];
			CONTRATO = CONTRATO_[0];
			CUENTA = CUENTA_[0];


			TRANSFERENCIAS =regreso[6];
			INVERSIONES=regreso[5];
			INTERBANCARIAS=regreso[4];
			ACEPTADAS=regreso[3];
			RECHAZADAS=regreso[2];
			PROGRAMADAS=regreso[1];
			PENDIENTES=regreso[0];





//FECHAINI FECHAFIN REFERENCIA
			if ( parametros == 0 ){
				// par�metros OK

				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &par�metros OK ...&", EIGlobal.NivelLog.INFO);

				if ( CONTRATO.length() < 2 )
					CONTRATO	= session.getContractNumber();
				if ( USUARIO.length() < 2 )
					USUARIO	= session.getUserID();

				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &registros del "+PREV+" al "+NEXT+"&", EIGlobal.NivelLog.INFO);

			regreso[6] = TRANSFERENCIAS;
			regreso[5] = INVERSIONES;
			regreso[4] = INTERBANCARIAS;
			regreso[3] = ACEPTADAS;
			regreso[2] = RECHAZADAS;
			regreso[1] = PROGRAMADAS;
			regreso[0] = PENDIENTES;

				muestraBitacora(IMPORTE,USUARIO,CUENTA,regreso,REFERENCIA,CONTRATO, FECHAINI, FECHAFIN, session.getArchivoBit(), Integer.parseInt(PREV), Integer.parseInt(NEXT) , req, res);

			}else{
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &error en par�metros ...&", EIGlobal.NivelLog.INFO);
				req.setAttribute("Output1","<h3>Error inesperado !");
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute( "Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora",req) );
				req.setAttribute( "Mensaje", "Error inesperado !." );
				evalTemplate( "/jsp/coConsultPrueb.jsp", req, res);
			} // if ( par�metros )

		} // if ( PREV )

		return 1;
	} // m�todo

	//=============================================================================================

	public int actualizaBitacora(String USUARIO, String CONTRATO,HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

		int salida			= 0; // por default OK

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String trama_salida		= "";
		String trama_entrada	= "";
		String nombre_archivo	= "";
		String ruta_archivo		= "";
		String salida_archivo	= "";
		String tipo_operacion	= "ABIT";  // prefijo del servicio ( actualizaci�n de la bit�cora )
		String cabecera			= "2EWEB"; // medio de entrega ( regresa un archivo )

		String facultad_consulta = (session.getFacultad(session.FAC_CONSULTA_BITACORA))?"true":"false"; // facultad para actualizar
		String fecha_inicial	 = ( String) req.getParameter("fecha1");
		String fecha_final		 = ( String) req.getParameter("fecha2");
		String PERFIL			 = "";
		//System.out.println("facultad_consulta                   ------->  "+facultad_consulta);

		PERFIL = session.getUserProfile();

		fecha_inicial	= fecha_inicial.substring( 3,5 )
						+ fecha_inicial.substring( 0,2 )
						+ fecha_inicial.substring( 8,10 );  // mmddaa

		fecha_final		= fecha_final.substring( 3,5 )
						+ fecha_final.substring( 0,2 )
						+ fecha_final.substring( 8,10 );  // mmddaa

		boolean errorFile = false;

		//TuxedoGlobal tuxedoGlobal;

		if ( facultad_consulta.trim().equals("true") || facultad_consulta.trim().length()!=0 ){
			try{
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &facultad de consulta OK ...&", EIGlobal.NivelLog.INFO);
				trama_entrada	=       cabecera + "|"
								+        USUARIO + "|"
								+ tipo_operacion + "|"
								+       CONTRATO + "|"
								+        PERFIL  + "|"
								+        USUARIO + "|"
								+  fecha_inicial + "|"
								+  fecha_final;

				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

				try{
					//tuxedoGlobal = (TuxedoGlobal) session.getEjbTuxedo() ;
					ServicioTux tuxedoGlobal = new ServicioTux();
					//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
					Hashtable hs = tuxedoGlobal.web_red( trama_entrada );
					trama_salida = (String) hs.get("BUFFER");

				}catch(java.rmi.RemoteException re){
					re.printStackTrace();
				}catch (Exception e) {}
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);

				if ( trama_salida.equals( IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID() ) ){
					// Copia Remota
					EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
					ArchivoRemoto archivo_remoto = new ArchivoRemoto();

					if(!archivo_remoto.copia( session.getUserID() ) ){
						salida = 1;	// error al copiar
						EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &error de  remote shell ...&", EIGlobal.NivelLog.INFO);
					}else{
						salida=0; // OK
						EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
					}
				}else
					salida=1; // error
			}catch(Exception ioe ){
				salida = 1; // error
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  Excepci�n en actualizaBitacora() >>"+ioe.getMessage()+"<<", EIGlobal.NivelLog.ERROR );
			} // try

		}else{
			// no se tiene facultad
			salida = 1;
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &facultad de consulta NO v�lida ...&", EIGlobal.NivelLog.INFO);
		}
		return salida;
	}

	//=============================================================================================

	public int filtrarArchivo(boolean INTERBANCARIAS,String CUENTA,String USUARIO1,String IMPORTE,String [] REFERENCIA,String SEMAFORO[], String archivo_entrada, String archivo_salida, int numero_lineas ){
		BufferedReader entrada;
		BufferedWriter salida;
		StringTokenizer valor ;

		int i = 0;
		int k = 0;
		int j = 0;
		int renglon_valido = 0;

		String[] renglon = null;
		String linea	 = "";
		String estatus	= "";

		try{
			entrada	= new BufferedReader( new FileReader( archivo_entrada ) );
			salida	= new BufferedWriter( new FileWriter( archivo_salida ) );

			linea  = entrada.readLine();
			linea += "\r\n";
			salida.write( linea ,0 ,linea.length() );

			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &creando archivo filtro ...&", EIGlobal.NivelLog.INFO);
			// EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &n�mero delineas :"+numero_lineas+"&", EIGlobal.NivelLog.INFO);

			for( i = 0; i < numero_lineas; i++ ){
				linea = entrada.readLine();
				/////
				if( linea.trim().equals(""))
					linea = entrada.readLine();
				if( linea == null )
					break;
				 //EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &registro : "+linea+"&", EIGlobal.NivelLog.INFO);
				renglon = desentrama( linea , ';' );
				estatus = obtenEstatus( renglon[0], renglon[10] ); // [0]tipo de operaci�n y [10]c�digo de error
				renglon_valido = aplicaFiltro(INTERBANCARIAS,CUENTA,USUARIO1,IMPORTE,REFERENCIA,SEMAFORO, renglon, estatus ); // filtro por cada registro

				if ( renglon_valido == 0 )// pas� el filtro
				{
					linea = estatus + ";" + linea + "\r\n";
					salida.write( linea, 0, linea.length() );
					salida.flush();
				} // if ( renglon_valido )
			} // for ( i )
			salida.close();
			entrada.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  Excepci�n en filtrarArchivo() >>"+e.getMessage()+"<<2", EIGlobal.NivelLog.ERROR );

		} // try
		return 0;
	} // m�todo

	//=============================================================================================

	String obtenEstatus ( String tipo_operacion, String codigo_error ){
		String	tipo_error	= "";
		String	terminacion	= "";
		String  estatus		= "";

		tipo_error	= codigo_error.substring( 0, 4);
		terminacion = codigo_error.substring( 4,8 );
		estatus		= "";

		if ( !tipo_error.equals( "MANC" ) ){
			if ( !tipo_error.equals( "PROG" ) ){
				if ( codigo_error.equals( "TRANSITO" ) )
					estatus = "T"; // en tr�nsito
				else if ( !terminacion.equals( "0000" ) )
					estatus = "R"; // rechazado
				else
					estatus = "A"; // aceptado
			}else{
				if ( tipo_operacion.equals( "ABIT" ) )// actualizaci�n a la bitacora
				{
					if ( !terminacion.equals( "0000" ) )
						estatus = "R";
					else
						estatus = "A";
				}else{
					if ( !terminacion.equals( "0000" ) )
						estatus = "R";
					else
						estatus = "P"; // programado
				} // if ( ABIT )
			} // if ( PROG )
		}else{
			estatus = "M"; // mancomunada
		} // if ( MANC )
		return estatus;
	} // m�todo

	//=============================================================================================

	public int aplicaFiltro(boolean INTERBANCARIAS, String CUENTA, String USUARIO1,String IMPORTE, String [] REFERENCIA, String SEMAFORO[],String[] registro, String estatus_temp ){
		int filtro = 0;

		String tipo_operacion	= "";
		String usuario_registro = "";
		String usuario_filtro	= "";
		String cuenta_retiro	= "";
		String cuenta_deposito	= "";
		String cuenta_filtro	= "";
		String importe_registro	= "";
		String ref_registro		= "";
		String semaforo_filtro[]= new String[11];

		int ninguno				=0;
		int tmp_usuario			=0;

		tmp_usuario =Integer.parseInt( USUARIO1 );

		tipo_operacion	= registro[0];
		usuario_registro= registro[9];
		usuario_filtro	= new Integer(tmp_usuario).toString();
		cuenta_retiro	= registro[1];
		cuenta_deposito	= registro[2];
		importe_registro= registro[3];
		ref_registro	= registro[8];
		cuenta_filtro	= FormateaContrato( CUENTA );

		for ( int i = 0 ; i < 11  ; i++ ) semaforo_filtro[i] = "0"; // 0 0 0 0 000 0000

		filtro = 1; // no aplica el filtro

		ninguno = Integer.parseInt( SEMAFORO[0] )
				+ Integer.parseInt( SEMAFORO[1] )
				+ Integer.parseInt( SEMAFORO[2] )
				+ Integer.parseInt( SEMAFORO[3] )
				+ Integer.parseInt( SEMAFORO[4] )
				+ Integer.parseInt( SEMAFORO[5] )
				+ Integer.parseInt( SEMAFORO[6] )
				+ Integer.parseInt( SEMAFORO[7] )
				+ Integer.parseInt( SEMAFORO[8] )
				+ Integer.parseInt( SEMAFORO[9] )
				+ Integer.parseInt( SEMAFORO[10] );

		// existe un filtro por lo menos
		if ( ninguno > 0 ){
			// filtro de usuario
			if ( ( usuario_filtro.trim().length() > 1) && ( usuario_registro.equals( usuario_filtro ) ) ) semaforo_filtro[0] = "1";

			// filtro de cuenta
			if ( ( cuenta_filtro.length() > 1 ) && ( cuenta_retiro.equals( cuenta_filtro ) ||
												   cuenta_deposito.equals( cuenta_filtro ) ) ) semaforo_filtro[1] = "1";

			// filtro de importe
			if ( IMPORTE.length() > 0 ){
				importe_registro = FormatoMoneda( importe_registro );
				if ( importe_registro.equals( FormatoMoneda( IMPORTE ) ) ) semaforo_filtro[2] = "1";
			}

			// filtro de referencia
			if ( ( REFERENCIA[0].length() > 1 ) && ( ref_registro.equals( REFERENCIA[0] ) ) ) semaforo_filtro[3] = "1";

			// filtro de transferencias
			if ( tipo_operacion.equals("TRAN") ||
				 tipo_operacion.equals("RETJ") ||
				 tipo_operacion.equals("PAGT") ||
				 tipo_operacion.equals("TRTJ") ||
				 tipo_operacion.equals("PIMP") ||
				 tipo_operacion.equals("RG03")  ) semaforo_filtro[4] = "1";

			// filtro de inversiones
			if ( tipo_operacion.equals("CPSI") ||
				 tipo_operacion.equals("VTSI") ||
				 tipo_operacion.equals("CPDI") ||
				 tipo_operacion.equals("CPDV") ||
				 tipo_operacion.equals("CPID") ||
				 tipo_operacion.equals("CPVD") ||
				 tipo_operacion.equals("CPCI")  ) semaforo_filtro[5] = "1";

			// filtro de interbancario
			if ( ( INTERBANCARIAS ) && ( tipo_operacion.equals("CAIB") ||
										 tipo_operacion.equals("DIBT") ) ) semaforo_filtro[6] = "1";

			// filtro de estatus de operaciones
			if ( estatus_temp.equals( "A" ) )	semaforo_filtro[7] = "1"; // aceptado
			if ( estatus_temp.equals( "R" ) )	semaforo_filtro[8] = "1"; // rechazado
			if ( estatus_temp.equals( "P" ) )	semaforo_filtro[9] = "1"; // programada
			if ( estatus_temp.equals( "N" ) )	semaforo_filtro[10] = "1"; // pendientes

			// log("m�scara del registro :"+semaforo_filtro[0]+semaforo_filtro[1]+semaforo_filtro[2]+semaforo_filtro[3]+semaforo_filtro[4]+semaforo_filtro[5]+semaforo_filtro[6]+semaforo_filtro[7]+semaforo_filtro[8]+semaforo_filtro[9]+semaforo_filtro[10]);

			// ( usuario ) y ( cuenta ) y ( transf o inver o inter ) y ( acep o rech o prog o pend )
			if ( ( ( semaforo_filtro[0].equals( SEMAFORO[0] ) && semaforo_filtro[0].equals("1") ) || SEMAFORO[0].equals("0") ) &&
				 ( ( semaforo_filtro[1].equals( SEMAFORO[1] ) && semaforo_filtro[1].equals("1") ) || SEMAFORO[1].equals("0") ) &&
				 ( ( semaforo_filtro[2].equals( SEMAFORO[2] ) && semaforo_filtro[2].equals("1") ) || SEMAFORO[2].equals("0") ) &&
				 ( ( semaforo_filtro[3].equals( SEMAFORO[3] ) && semaforo_filtro[3].equals("1") ) || SEMAFORO[3].equals("0") ) &&
				 ( ( semaforo_filtro[4].equals( SEMAFORO[4] ) && semaforo_filtro[4].equals("1") ) ||
				   ( semaforo_filtro[5].equals( SEMAFORO[5] ) && semaforo_filtro[5].equals("1") ) ||
				   ( semaforo_filtro[6].equals( SEMAFORO[6] ) && semaforo_filtro[6].equals("1") ) ||
				   ( SEMAFORO[4].equals("0") && SEMAFORO[5].equals("0") && SEMAFORO[6].equals("0") ) ) &&
				 ( ( semaforo_filtro[7].equals( SEMAFORO[7] ) && semaforo_filtro[7].equals("1") ) ||
				   ( semaforo_filtro[8].equals( SEMAFORO[8] ) && semaforo_filtro[8].equals("1") ) ||
				   ( semaforo_filtro[9].equals( SEMAFORO[9] ) && semaforo_filtro[9].equals("1") ) ||
				   ( semaforo_filtro[10].equals( SEMAFORO[10] ) && semaforo_filtro[10].equals("1") ) ||
				   ( SEMAFORO[7].equals("0") && SEMAFORO[8].equals("0") && SEMAFORO[9].equals("0") && SEMAFORO[10].equals("0") ) ) ) filtro = 0; // pas� el filtro
		}else{
			// no se seleccion� ningun filtro
			filtro =0;
		}

		return filtro;
	} // m�todo

	//=============================================================================================

	public int muestraBitacora(String IMPORTE,String USUARIO,String CUENTA, boolean [] regreso, String [] REFERENCIA,String CONTRATO, String FECHAINI,
									String FECHAFIN, String archivo_fuente, int inicio, int fin,HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &desplegando datos en pantalla ...&", EIGlobal.NivelLog.INFO);

		String tabla		= "";
		String encabezado	= "";

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");


	boolean TRANSFERENCIAS	= false;
	boolean INVERSIONES		= false;
	boolean INTERBANCARIAS	= false;
	boolean ACEPTADAS		= false;
	boolean RECHAZADAS		= false;
	boolean PROGRAMADAS		= false;
	boolean PENDIENTES		= false;



		/*Date hoy	= new Date();
		Date day1	= new Date();
		Date day2	= new Date();*/

		int delimitador = 0;

		String menu_p = "";
		menu_p        = session.getstrMenu();

		if(menu_p == null)
			menu_p = "";

		req.setAttribute( "MenuPrincipal",	menu_p );
		req.setAttribute( "Fecha",			ObtenFecha() );
		req.setAttribute( "ContUser",		ObtenContUser(req) );

		delimitador	= 14;
		encabezado	= "Consulta de bit&aacute;cora general del d&iacute;a";

		String arreglo_registros[][] = obtenRegistrosArchivo( archivo_fuente, ";", delimitador, inicio, fin );
		String fecha_inicio			 = "";
		String fecha_fin			 = "";

		// se inicializa la tabla de datos
		tabla += "<Table border=0 cellspacing=2 cellpadding=0 width=750 >\n";
		tabla += "	<tr align=left>\n";
		if (CUENTA.trim().length()>1 )tabla += "		<td class=texenccon align=left colspan=10>Cuenta : " +  CUENTA + "</td>\n";
		else tabla += "		<td align=left colspan=10>&nbsp;</td>\n";
		tabla += "	</tr>\n";

		/*comentado por pablo***************************************************
		**ya que no se usan las sig lineas*************************************
		************************************************************************
		day1.setDate(  Integer.parseInt( session.getfechaini().substring( 0,2 ) ) );
		day1.setMonth( Integer.parseInt( session.getfechaini().substring( 3,5 ) )-1 );
		day1.setYear(  Integer.parseInt( session.getfechaini().substring( 6,10 ) ) );
		day2.setDate(  Integer.parseInt( session.getfechafin().substring( 0,2 ) ) );
		day2.setMonth( Integer.parseInt( session.getfechafin().substring( 3,5 ) )-1 );
		day2.setYear(  Integer.parseInt( session.getfechafin().substring( 6,10 ) ) );



		//   rango de fechas
		if(	( hoy.getDate()  == day1.getDate() && hoy.getDate()	 == day2.getDate() ) &&
			( hoy.getMonth() == day1.getMonth()&& hoy.getMonth() == day2.getMonth() )&&
			( ( hoy.getYear() + 1900 ) == day1.getYear() ) &&
			( ( hoy.getYear() + 1900 ) == day2.getYear() ) )
		{
			//tabla += "	<tr>\n";
			//tabla += "		<td align=right colspan=10><b>Bit&aacute;cora del d&iacute;a&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;:&nbsp;" + session.gettotal() + "</b></td>\n";
			//tabla += "	</tr>\n";
		}
		else
		{
			//tabla += "	<tr>\n";
			//tabla += "		<td align=right colspan=10><b>Bit&aacute;cora del&nbsp;" + FECHAINI;
			//tabla +=		"&nbsp;al&nbsp;" + FECHAFIN + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;:&nbsp;" + session.gettotal() + "</b></td>";
			//tabla += "	</tr>\n";
		}
		*comentado por pablo***************************************************
		***********************************************************************
		***********************************************************************/
		// total de operaciones
		tabla += "	<tr>\n";
		tabla += "		<td align=left class=texenccon colspan=10>Total de operaciones:&nbsp;" + session.gettotal() + "</td>\n";
		tabla += "	</tr>\n";
		tabla += "</table>\n";

        tabla += "<table width=750 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>\n";

		// encabezados de columna
		tabla += "	<tr>\n";
		tabla += "		<td width=50  class=tittabdat align=center>Fecha</td>\n";
		tabla += "		<td width=30  class=tittabdat align=center>Hora</td>\n";
		tabla += "		<td width=155 class=tittabdat align=center>Descripci&oacute;n</td>\n";
		tabla += "		<td width=100 class=tittabdat align=center>Cuenta de cargo</td>\n";
		tabla += "		<td width=100 class=tittabdat align=center>Cuenta de abono</td>\n";
		tabla += "		<td width=66  class=tittabdat align=center>Importe</td>\n";
		tabla += "		<td width=65  class=tittabdat align=center>Referencia</td>\n";
		tabla += "		<td width=55  class=tittabdat align=center>Usuario</td>\n";
		tabla += "		<td width=55  class=tittabdat align=center>Estatus</td>\n";
		tabla += "      <td width=55  class=tittabdat align=center>Origen Oper.</td>\n";
		tabla += "	</tr>\n";

		// contenido de la tabla
		tabla += armaRenglones(USUARIO, arreglo_registros, fin - inicio, req );

		tabla += "</table>\n";

		// env�a par�metros
		tabla += "<table width=750 border=0 cellspacing=2 cellpadding=0>\n";
		tabla += "	<tr align=left>\n";
		tabla += "		<td class=texenccon>Si desea obtener el comprobante de cualquier ";
        tabla +=		"operaci&oacute;n, d&eacute; click en el n&uacute;mero de referencia ";
		tabla +=		"subrayado.</td>\n";
		tabla += "	</tr>\n";
		tabla += "</table>";
		tabla += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>\n";
		tabla += "	<tr><td>&nbsp;</td></tr>\n";
		tabla += "	<tr>\n";
		tabla += "		<td align=center class=texfootpagneg>\n";
		tabla += "			<Form name =\"frmbit\" method = \"POST\" ACTION=\"ConsultaCancelacion\">\n";
		tabla += "				<input type=\"Hidden\" name=\"total\" value= \"" + session.gettotal() + "\">\n";
		tabla += "				<input type=\"Hidden\" name=\"prev\" value= \"" + new Integer(inicio).toString() + "\">\n";
		tabla += "				<input type=\"Hidden\" name=\"next\" value= \"" + new Integer(fin).toString() + "\">\n";
		tabla += "				<input type=\"Hidden\" name=\"parametros\" value=\"" + CONTRATO + ";" ;

		if( USUARIO.trim().length() == 0 ) tabla += "0;";
		else							   tabla += USUARIO + ";";

		if( CUENTA.trim().length() == 0 )  tabla += "0;";
		else					           tabla += CUENTA + ";";

		if( IMPORTE.trim().length() == 0 ) tabla += "0;";
		else					           tabla += IMPORTE + ";";

		if(REFERENCIA[0].trim().length()==0)  tabla += "0;";
		else						       tabla += REFERENCIA[0] + ";";

		tabla += new Boolean( TRANSFERENCIAS ).toString() + ";"
			   + new Boolean( INVERSIONES ).toString() + ";"
			   + new Boolean( INTERBANCARIAS ).toString() + ";"
			   + new Boolean( ACEPTADAS ).toString() + ";"
			   + new Boolean( RECHAZADAS ).toString() + ";"
			   + new Boolean( PROGRAMADAS ).toString() + ";"
			   + new Boolean( PENDIENTES ).toString() + ";"
			   + FECHAINI + ";"
			   + FECHAFIN + ";\">\n";

		tabla += "Movimientos : " + new Integer( inicio+1 ).toString() + " - "
			   + new Integer( fin ).toString() + " de " + session.gettotal() + "<br><br>\n";

		if( inicio > 0 ) tabla += "<a href=\"javascript:history.back();\" class=texfootpaggri>< Anteriores 30</a>&nbsp;\n";

		if( fin < Integer.parseInt( session.gettotal() ) )
		{
			if(	( fin + 30 ) <= Integer.parseInt( session.gettotal() ) )
				tabla += "<nobr><a href=\"javascript:adelante();\" class=texfootpaggri>Siguientes " + new Integer( fin - inicio ).toString() + " ></a>\n";
			else
				tabla += "<nobr><a href=\"javascript:adelante();\" class=texfootpaggri>Siguientes " + new Integer(Integer.parseInt( session.gettotal() ) - fin ).toString() + " ></a>\n";
		}
		tabla += "<br><br><a href=\"/Download/" + CONTRATO + USUARIO + "bit.doc"  + "\"><img border=0 name=imageField4 src=\"/gifs/EnlaceMig/gbo25230.gif\" width=85 height=22 alt=Exportar></a><nobr>\n";
		tabla += "<a href = \"javascript:scrImpresion();\"><img border=0 name=imageField42 src=\"/gifs/EnlaceMig/gbo25240.gif\" width=83 height=22 alt=\"Imprimir\"></a>\n";
		tabla += "			</Form>\n ";
		tabla += "		</td>\n";
		tabla += "	</tr>\n";
		tabla += "</table>\n";

		if ( !FECHAINI.trim().equals( FECHAFIN.trim() ) ) encabezado = "Consulta de bit&aacute;cora hist&oacute;rica";

		req.setAttribute( "Encabezado", CreaEncabezado(encabezado,"Consultas &gt; Bit&aacute;cora",req) );
		req.setAttribute("Output1",tabla);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);

		evalTemplate(IEnlace.BITACORA_TMPL, req, res);

							regreso[6] = TRANSFERENCIAS;
							regreso[5] = INVERSIONES;
							regreso[4] = INTERBANCARIAS;
							regreso[3] = ACEPTADAS;
							regreso[2] = RECHAZADAS;
							regreso[1] = PROGRAMADAS;
							regreso[0] = PENDIENTES;
		return 0;
	}

	//=============================================================================================

	public String[][] obtenRegistrosArchivo(String filename,String delim,int Ndelim,int b_ini,int b_end){

        EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  Inicia obtenRegistrosArchivo()...&", EIGlobal.NivelLog.INFO);

		BufferedReader entrada;
		StringTokenizer t ;
		String linea;

		linea     = null;
		int i     = 0;
		int i_ini = b_ini;
		int i_end = b_end;
		int j     = 0;
		int k     = 0;

		String[][] salida = new String[i_end - i_ini][Ndelim];
		try
		{
			entrada = new BufferedReader( new FileReader( filename ) );
			linea   = entrada.readLine();

			if( i_ini > 0 )
			{
				for( j=0 ; j <i_ini ; j++ ) linea = entrada.readLine();
				/*{
					linea = entrada.readLine();
					/////
					if(linea==null||linea.trim().equals(""))
						linea = entrada.readLine();
					if( linea == null )
						continue;
						//break;
				}*/
			}

			for( j=0 ; j<( i_end - i_ini ) ; j++ ){
				linea = entrada.readLine();
				//////
				/*if( linea.trim().equals(""))
					linea = entrada.readLine();
				if( linea == null )
					break;*/
				for( k=0 ; k < Ndelim ; k++ )
				{
					try
					{
						salida[j][k] = EIGlobal.BuscarToken(linea , ';' , k+1  );
					}
					catch(Exception e)
					{
						salida[j][k]=" ";
           			}
				}
			}//Fin for
			entrada.close();
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  Excepci�n en obtenRegistrosArchivo() >>"+e.getMessage()+"<<", EIGlobal.NivelLog.ERROR );
			e.printStackTrace();
		}
		String[][] straux = new String[i_end - i_ini][Ndelim];

		for( j = 0; j < (i_end - i_ini); j++ )
		{
			for( k = 0; k < Ndelim ; k++ )
			{
				straux[j][k] = salida[j][k];
//     			System.out.println("MATRIZ straux[j][k]  = " + straux[j][k]);
			}
  //      System.out.println(        "<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>");
		}
		return straux;
	}

	//=============================================================================================

	public String armaRenglones (String USUARIO, String[][] registros ,int numero_registros,HttpServletRequest req  )
	{
		String hora				   = "";
		String renglon			   = "";
		String estilo			   = "";
		String estatus			   = "";
		String origen              = "";
		String descripcion		   = "";
		String comprobante		   = "";
		String[] datos_comprobante = new String[12];

		StringBuffer renglon_ = new StringBuffer();

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		double Cargo_Abono = 0.0;

		for( int j = 0; j < numero_registros; j++)
		{
			if( ( j % 2 ) != 0 )	estilo = "		<td class=textabdatcla ";
			else					estilo = "		<td class=textabdatobs ";

			//renglon += "		<tr>\n";
			renglon_.append("		<tr>\n");

			// fecha
			if( registros[j][5].trim().length() > 0 )
			{
				//renglon += estilo + "nowrap align=center>" + registros[j][5] + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("nowrap align=center>");
				renglon_.append(registros[j][5]);
				renglon_.append("</td>\n");
			}
			else
			{
				//renglon += estilo + "nowrap align=center>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("nowrap align=center>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[0] = "6";
			datos_comprobante[5] = registros[j][5];

			// Hora
			if( registros[j][7].trim().length()>0 )
			{
				if ( registros[j][7].trim().length() == 3 )
					hora = "0" + registros[j][7].substring( 0,1 ) + ":" + registros[j][7].substring ( 1, 3);
				else
					hora = registros[j][7].substring( 0,2 ) + ":" + registros[j][7].substring ( 2, 4);

					//renglon += estilo + "align=center>" + hora + "</td>\n";
					renglon_.append(estilo);
					renglon_.append("align=center>");
					renglon_.append(hora);
					renglon_.append("</td>\n");
			}else
			{
				//renglon += estilo + "align=center>&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=center>&nbsp;&nbsp;</td>\n");

			}

			datos_comprobante[6] = hora ;

			// Descripci�n
			if (registros[j][12].trim().length() > 0 )
			{
				descripcion = registros[j][12].trim();
				//renglon += estilo + "algin=left>" + descripcion + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("algin=left>");
				renglon_.append(descripcion);
				renglon_.append("</td>\n");
			}else
			{
				descripcion = Description( registros[j][1].trim() );
				if(descripcion != null)	{
				//renglon += estilo + "align=left>" + descripcion + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left>");
				renglon_.append(descripcion);
				renglon_.append("</td>\n");
				}
				else
				{
					descripcion = registros[j][1].trim();
					//renglon += estilo + "align=left>" + descripcion + "</td>\n";
					renglon_.append(estilo);
					renglon_.append("align=left>");
					renglon_.append(descripcion);
					renglon_.append("</td>\n");
				}
			}

			descripcion = descripcion.replace( ' ','+' );
			datos_comprobante[9] = descripcion;
			datos_comprobante[10]= registros[j][1].trim();

			// Cuenta Retiro
			if( registros[j][2].trim().length() >0 )
			{
				//renglon += estilo + "align=left nowrap>" + registros[j][2] + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>");
				renglon_.append(registros[j][2]);
				renglon_.append("</td>\n");
			}
			else
			{
				 //renglon += estilo + "align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[3] = registros[j][2];

			// Cuenta Dep�sito
			if(registros[j][3].trim().length()>0)
			{
				//renglon += estilo + "align=left nowrap>" + registros[j][3] + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>");
				renglon_.append(registros[j][3]);
				renglon_.append("</td>\n");
			}
			else
			{
				//renglon += estilo + "align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[4] = registros[j][3];

			// Importe
			if(registros[j][4].trim().length()>0)
			{
				//renglon += estilo + "align=right nowrap>" + FormatoMoneda( registros[j][4].trim() ) + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=right nowrap>");
				try{
				renglon_.append(FormatoMoneda( registros[j][4].trim() ));
				} catch(Exception e){}
				renglon_.append("</td>\n");
			}
			else
			{
				//renglon += estilo + "align=right nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=right nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[2] = registros[j][4].trim();

			// Referencia
			if(registros[j][9].trim().length()>0)
			{
				datos_comprobante[1] = registros[j][9];
				datos_comprobante[7] = USUARIO;
				datos_comprobante[8] = ObtenNombreUsuario(USUARIO,true,req);
				datos_comprobante[11]= registros[j][0];

				comprobante = armaComprobante( datos_comprobante );  // se arma el link para enviar el comprobante

				if ( comprobante.length() > 1 ) {
					//renglon += estilo + "align=center nowrap>" + comprobante + registros[j][9].trim() + "</a></td>\n";
					renglon_.append(estilo);
					renglon_.append("align=center nowrap>");
					renglon_.append(comprobante);
					renglon_.append(registros[j][9].trim());
					renglon_.append("</a></td>\n");
				}
				else
				{
					//renglon += estilo + "align=center nowrap>" + registros[j][9].trim() + "</td>\n";
					renglon_.append(estilo);
					renglon_.append("align=center nowrap>");
					renglon_.append(registros[j][9].trim());
					renglon_.append("</td>\n");

				}
			}else
			{
				//renglon += estilo + "align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			// Usuario
			if(registros[j][10].trim().length()>0) {
				//renglon += estilo + "align=center nowrap>" + registros[j][10].trim() + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				renglon_.append(registros[j][10].trim());
				renglon_.append("</td>\n");

			}
			else
			{
				//renglon += estilo + "align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			// Estatus
			if      ( registros[j][0].equals( "A" ) ) estatus = "Aceptado";
			else if ( registros[j][0].equals( "R" ) ) estatus = "Rechazado";
			else if ( registros[j][0].equals( "M" ) ) estatus = "Mancomunado";
			else if ( registros[j][0].equals( "P" ) ) estatus = "Programado";
			else if ( registros[j][0].equals( "N" ) ) estatus = "Pendiente";
			else estatus = "No catalogado";
			//renglon += estilo + "align=center nowrap>" + estatus + "</td>\n";

				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				renglon_.append(estatus);
				renglon_.append("</td>\n");

			/*System.out.println("RENGLONES REGISTRO = " + "0-" + registros[j][0] + "1-" + registros[j][1] + "2-" + registros[j][2] + "3-" + registros[j][3] + "4-" + registros[j][4] + "5-" + registros[j][5] + "6-" + registros[j][6] + "7-" + registros[j][7] + "8-" + registros[j][8] +
				 "9-" + registros[j][9] + "10-" + registros[j][10] + "11-" + registros[j][11] + "12-" + registros[j][12] + "13-" + registros[j][13]);*/

            // Origen de operaci�n
			try{
				if(registros[j][13] ==null)
					registros[j][13] = "";
			} catch(Exception e) {

				origen = "No catalogado"; //cambio
				//renglon += estilo + "align=center nowrap>" + origen + "</td>\n"; //cambio
				//renglon += "	</tr>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				renglon_.append(origen);
				renglon_.append("</td>\n");
				renglon_.append("	</tr>\n");
				continue;
			}
            if      ( registros[j][13].trim().equals( "EWEB" ) ) origen = "Web";//cambio
            else if ( registros[j][13].trim().equals( "TCT" ) )  origen = "Tradicional"; //cambio
            else origen = "No catalogado"; //cambio
            //renglon += estilo + "align=center nowrap>" + origen + "</td>\n"; //cambio
			//renglon += "	</tr>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				renglon_.append(origen);
				renglon_.append("</td>\n");
				renglon_.append("	</tr>\n");
		} // for ( j )
		//System.out.println(renglon_.toString());
		return renglon_.toString();
		//return renglon;
	}

	//=============================================================================================


	public String armaComprobante( String[] registro )
	{
		String link	= " ";
		System.out.println(">>>>>>>>>>************Entrando a armaComprobante () ***************>>>>");
		// ### Modificacion para el estado fiscal ...
		String concepto = "";
		String rfc      = "";
		String iva      = "";

		int rfcIn = -1;
		int ivaIn = -1;

		// tipo de operaci�n y estatus
		if ( ( registro[10].equals("DIBT") || registro[10].equals("TRAN") || registro[10].equals("PAGT") ||
			   registro[10].equals("CPDI") || registro[10].equals("CPID") ||
			   registro[10].equals("CPDV") ) && registro[11].equals( "A" ) )
		{
			concepto = registro[9];
			rfcIn=concepto.lastIndexOf("RFC");
			ivaIn=concepto.lastIndexOf("IVA");

			if(rfcIn>=30)
			 {
			  if(ivaIn>=30)
				{
			   rfc=concepto.substring(concepto.lastIndexOf("RFC")+4,concepto.lastIndexOf("IVA")).trim();
			   iva=concepto.substring(concepto.lastIndexOf("IVA")+4,concepto.length()).trim();
//			   concepto = concepto.substring(0,concepto.indexOf(","));
			 }
			 else
				{
				   rfc=concepto.substring(concepto.lastIndexOf("RFC")+4,concepto.length()).trim();
				   iva="";
				}
				 concepto=concepto.substring(0,concepto.lastIndexOf("RFC"));
			}
			link ="<a href=\"javascript:VentanaComprobante(";
			link += "'"+ registro[0] + "',"; // tipo
			link += "'"+ registro[1] + "',"; // referencia
			link += "'"+ registro[2] + "',"; // importe
			link += "'"+ registro[3] + "',"; // cuenta origen
			link += "'"+ registro[4] + "',"; // cuenta destino
			link += "'"+ registro[5] + "',"; // fecha
			link += "'"+ registro[6] + "',"; // hora
			link += "'"+ registro[7] + "',"; // usuario
			link += "'"+ registro[8] + "',"; // descripci�n de usuario
			link += "'"+ concepto + "',";    // descripci�n de operaci�n

			link += "'"+ rfc + "',";         // descripci�n de usuario
			link += "'"+ iva + "');";        // descripci�n de usuario

			//link += "'"+ registro[9] + "');";// descripci�n de operaci�n
			link += "\">";
		}
		return link ;
	}

	//=============================================================================================


	public int obtenParametros(String [] fechas,boolean [] regreso,String [] CUENTA,String [] USUARIO,String [] impo,
										String [] REFERENCIA,String MISPARAMETROS	,String [] CONTRATO, String SEMAFORO[], HttpServletRequest req){
		int salida					= 0;
		String parametros[]			= new String[14];
        StringTokenizer parametro	= new StringTokenizer(MISPARAMETROS,";");
		String FECHAFIN ="";
		String FECHAINI ="";

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

	boolean TRANSFERENCIAS_	= false;
	boolean INVERSIONES_	= false;
	boolean INTERBANCARIAS_	= false;
	boolean ACEPTADAS_		= false;
	boolean RECHAZADAS_		= false;
	boolean PROGRAMADAS_	= false;
	boolean PENDIENTES_		= false;

		try{
			for( int k=0 ; k<14 ; k++ ) parametros[k] = parametro.nextToken();

			CONTRATO [0]		= parametros[0];
			USUARIO [0]		= parametros[1];
			CUENTA[0]			= parametros[2];
			//IMPORTE			= parametros[3];
			impo [0]			= parametros[3];
			REFERENCIA [0]	= parametros[4];

			if( CUENTA[0].equals("0") ) CUENTA[0] = " ";

			if( !parametros[5].equals("false" ) ) { TRANSFERENCIAS_ =true; SEMAFORO[1] = "1"; }
			if( !parametros[6].equals("false" ) ) { INVERSIONES_ =true; SEMAFORO[2] = "1"; }
			if( !parametros[7].equals("false" ) ) { INTERBANCARIAS_ =true; SEMAFORO[3] = "1"; }
			if( !parametros[8].equals("false" ) ) { ACEPTADAS_ =true; SEMAFORO[4] = "1"; }
			if( !parametros[9].equals("false" ) ) { RECHAZADAS_ =true; SEMAFORO[5] = "1"; }
			if( !parametros[10].equals("false" )) { PROGRAMADAS_ =true; SEMAFORO[6] = "1"; }
			if( !parametros[11].equals("false" )) { PENDIENTES_ =true; SEMAFORO[7] = "1"; }


			regreso[6] = TRANSFERENCIAS_;
			regreso[5] = INVERSIONES_;
			regreso[4] = INTERBANCARIAS_;
			regreso[3] = ACEPTADAS_;
			regreso[2] = RECHAZADAS_;
			regreso[1] = PROGRAMADAS_;
			regreso[0] = PENDIENTES_;



			FECHAINI		= parametros[12];
			FECHAFIN		= parametros[13];

			fechas[0] =FECHAINI;
			fechas[1] =FECHAFIN;
//System.out.println("FechaINI   FECHAFIN "+FECHAINI+"   "+FECHAFIN);

			session.setfechaini( FECHAINI );
			session.setfechafin( FECHAFIN );

		}catch ( Exception ioe ){
			salida = 1; // error en los datos
		}
		return salida;
	}

	//=============================================================================================

	public int totalRegistros(String [] ERROR, String filename )
	{
		int registros = 0;
		String linea = null;

		try{
			BufferedReader entrada= new BufferedReader(new FileReader(filename));
			linea = entrada.readLine();

			if ( linea.startsWith( "OK" ) ){
				while( ( linea = entrada.readLine() )!=null ){
					if( linea.trim().equals("")||linea==null)
						linea = entrada.readLine();
					if( linea == null )
						//break;
						continue;
					registros++;
				}
			}else{
				// archivo sin registros
				ERROR[0] = linea.substring(16);
				EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &no hay registros en el archivo ...&", EIGlobal.NivelLog.INFO);
			}
			entrada.close();
		}catch(Exception e){
			return 0; // error
		}
		return registros;
	}
	//=============================================================================================

	String FormateaContrato (String contrato_entrada)
	{
		String salida	= "";
		String caracter	= "";

		for ( int i = 0 ; i < contrato_entrada.length() ; i++ ){
			caracter = contrato_entrada.substring( i, i+1 );
			if ( !caracter.equals("-") ) salida += caracter;
		}
		return salida;
	}

	//=============================================================================================

	public int exportarArchivo(String NOMBRE_ARCHIVO, String archivo_entrada, String archivo_salida, int numero_lineas,
			HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		BufferedReader entrada;
		StringTokenizer valor ;

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		int i = 0;
		int k = 0;
		int renglon_valido = 0;

		String[] renglon		= null;
		String linea			= "";
		String linea_exportar	= "";

		EI_Exportar archivo_exportar = new EI_Exportar( archivo_salida );
		ArchivoRemoto archR=new ArchivoRemoto();
		archivo_exportar.creaArchivo();

		try{
			entrada	= new BufferedReader( new FileReader( archivo_entrada ) );
			linea	= entrada.readLine();

			for( i = 0; i < numero_lineas; i++ ){
				linea = entrada.readLine();
				/////
				if( linea.trim().equals(""))
					linea = entrada.readLine();
				if( linea == null )
					break;
				renglon = desentrama( linea, ';' );

				linea_exportar = armaRenglonExportar( renglon );
				archivo_exportar.escribeLinea( linea_exportar );

			} // for ( i )
			archivo_exportar.cierraArchivo();
			entrada.close();

			// Doble remote shell
			archR.copiaLocalARemoto( NOMBRE_ARCHIVO ,"WEB");
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  &Ejecutando copia local a remoto...&", EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			e.printStackTrace();
			EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  Excepci�n en exportarArchivo() >>"+e.getMessage()+"<<", EIGlobal.NivelLog.ERROR );
			request.setAttribute( "Output1","<h3>No hay registros asociados a su consulta !");
			request.setAttribute( "newMenu", session.getFuncionesDeMenu());
			request.setAttribute( "MenuPrincipal", session.getStrMenu());
			request.setAttribute( "Encabezado", CreaEncabezado( "Consulta de bit&aacute;cora","Consultas &gt; Bit&aacute;cora",request) );
			request.setAttribute( "Mensaje", "No hay registros asociados a su consulta !"  );
			evalTemplate( "/jsp/coConsultPrueb.jsp", request, response );
			return 1;
		} // try
		return 0;
	} // m�todo

	//=============================================================================================

	public String armaRenglonExportar ( String[] registro ){
		String hora			= "";
		String renglon		= "";
        String origen      = "";//cambio
		String estatus		= "";
		String descripcion	= "";
/*
try{
//System.out.println("\n\n\n\n\n\n*");

for(int r=0;r<registro.length;r++)
		//System.out.println(registro[r]);
//System.out.println("*\n\n\n\n\n\n");
}catch(Exception e) {e.printStackTrace();}
*/

			// fecha
			if( registro[5].trim().length() > 0 )
				renglon += registro[5] + ";";
			else
				renglon += " ;";

			// Hora
			if( registro[7].trim().length()>0 ){
				if ( registro[7].trim().length() == 3 )
					hora = "0" + registro[7].substring( 0,1 ) + ":" + registro[7].substring ( 1, 3);
				else
					hora = registro[7].substring( 0,2 ) + ":" + registro[7].substring ( 2, 4);

				renglon += hora + ";";
			}
			else renglon += " ;";

			// Descripci�n
			if(registro.length>12)
				if (registro[12].trim().length() > 0 ) renglon += registro[12] + ";";
				else{
					descripcion = Description( registro[1].trim() );
					if(descripcion != null)	renglon += descripcion + ";";
					else					renglon += registro[1].trim() + ";";
				}

			// Cuenta Retiro
			if( registro[2].trim().length() >0 )
				renglon += registro[2] + ";";
			else
				renglon += " ;";

			// Cuenta Dep�sito
			if(registro[3].trim().length()>0)
				renglon += registro[3] + ";";
			else
				renglon += " ;";

			// Importe
			if(registro[4].trim().length()>0)
				renglon += FormatoMoneda( registro[4].trim() ) + ";";
			else
				renglon += " ;";

			// Referencia
			if(registro[9].trim().length()>0)
				renglon += registro[9].trim() + ";";
			else
				renglon += " ;";

			// Usuario
			if(registro[10].trim().length()>0)
				renglon += registro[10].trim() + ";";
			else
				renglon += " ;";

			// Estatus
			if ( registro[0].equals( "A" ) ) estatus = "Aceptado;";
			else if ( registro[0].equals( "R" ) ) estatus = "Rechazado;";
			else if ( registro[0].equals( "M" ) ) estatus = "Mancomunado;";
			else if ( registro[0].equals( "P" ) ) estatus = "Programado;";
			else if ( registro[0].equals( "N" ) ) estatus = "Pendiente;";
			else estatus = "No catalogado;";
			renglon += estatus;

            // Origen de operaci�n
			if(registro.length>13) {
				if      ( registro[13].trim().equals( "EWEB" ) ) origen = "Web;"; //cambio
				else if ( registro[13].trim().equals( "TCT" ) )  origen = "Tradicional;"; //cambio
				else origen = "No catalogado;"; //cambio
			} else
				origen = "No catalogado";
            renglon += origen ; //cambio


			renglon += "\r\n";

		return renglon;
	} // m�todo




	//=============================================================================================

	public String ObtenNombreUsuario(String NumUsuario, boolean EspacioMas, HttpServletRequest req)
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String[][] ArregloUsuarios = TraeUsuarios(req);
        String Nombre = "";
        for(int i=1;i<=Integer.parseInt(ArregloUsuarios[0][0]);i++)
			if(ArregloUsuarios[i][1].equals(NumUsuario))
            {
                Nombre = ArregloUsuarios[i][2].trim();
                break;
            }
        if(EspacioMas) Nombre = Nombre.replace(' ','+');
        return Nombre;
    }

	//=============================================================================================

	public String[] desentrama(String auxCad, char Separador)
	{
		int NumSep = 0; // Variable que contiene el numero de separadores
		String[] Salida = null; // Arreglo de String donde se guardan los valores
								// Obtener n�mero de separadores

		if (auxCad.charAt((auxCad.length() - 1)) != Separador)
			auxCad += "" + Separador;

			for ( int i = 0; i < auxCad.length(); i++ )
				if (auxCad.toCharArray()[i] == Separador)  NumSep++;

		// Almacenar valores en el arreglo de Strings
		try{
			Salida = new String[NumSep];
			for (int i = 0; i <= NumSep; i++){
				Salida[i] = auxCad.substring(0, auxCad.indexOf(Separador));
				auxCad = auxCad.substring((auxCad.indexOf(Separador) + 1), auxCad.length());
			}
		}catch (Exception e){
			//EIGlobal.mensajePorTrace( "***ConsultaCancelacion.class  Excepci�n en desentrama() >>"+e.getMessage()+"<<",1 );
		}
		return Salida;
	}


Calendar toCalendar(String strFecha) //cambio
  {
    Calendar fecha;
    String separador = "/";

     try
      {
        if (strFecha.substring(2, 3).equals(separador) && strFecha.substring(5, 6).equals(separador))
          {
            int dia  = Integer.parseInt(strFecha.substring(0, 2));
            int mes  = Integer.parseInt(strFecha.substring(3, 5)) - 1;
            int anio = Integer.parseInt(strFecha.substring(6, 10));
            fecha = new GregorianCalendar(anio, mes, dia);
         }
        else
          fecha = null;

      } catch(Exception e)
          {
           EIGlobal.mensajePorTrace( "***ChesConsulta.class Excepcion %toCalendar() >> "+ e.toString() + " <<", EIGlobal.NivelLog.INFO);
           fecha = null;
          }

      return fecha;

  }





    static protected String _defaultTemplate ="EnlaceMig/movimientos";

} // clase
