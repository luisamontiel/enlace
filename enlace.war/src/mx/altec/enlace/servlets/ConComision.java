package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.lang.reflect.*;


public class ConComision extends BaseServlet
{
    int     salida          = 0;
    short   sucursalOpera   = (short)787;

    //EIGlobal EnlaceGlobal   = new EIGlobal(this);

//=================================================================================================

    public void doGet( HttpServletRequest req, HttpServletResponse res )
            throws ServletException, IOException
    {
        defaultAction( req, res );
    }

//=================================================================================================

    public void doPost( HttpServletRequest req, HttpServletResponse res )
            throws ServletException, IOException
    {
        defaultAction( req, res );
    }

//=================================================================================================

    public void defaultAction( HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        String      Trama               = "";
        String      Result              = "";
        String      usuario             = "";
        String      contrato            = "";
        String      clavePerfil         = "";
        String[][]  arrayCuentasCargo   = null;

		BaseResource session = (BaseResource) req.getSession().getAttribute("session");
        EIGlobal.mensajePorTrace("ConComision - execute(): Entrando a Consultas de Dep. Inter.", EIGlobal.NivelLog.INFO);

        String modulo=(String)req.getParameter("Modulo");
		if(modulo == null) modulo = "2";

        boolean sesionvalida=SesionValida( req, res );
        if(sesionvalida)
        {
            // Variables de sesion
            usuario         = session.getUserID();
            contrato        = session.getContractNumber();
            clavePerfil     = session.getUserProfile();
            sucursalOpera   = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);

            // modificacion para integracion arrayCuentasCargo=ContCtasRelacpara_mis_transferencias(contrato);

           if(modulo.equals("0"))
             generaTablaComisiones(arrayCuentasCargo,Result,Trama,clavePerfil, usuario,contrato, req, res );


        } // if (sesionvalida)
        else if(sesionvalida)
        {
            req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
            evalTemplate(IEnlace.ERROR_TMPL, req, res);
        }

        EIGlobal.mensajePorTrace("ConComision - execute(): Saliendo de Consultas de Dep. Inter.", EIGlobal.NivelLog.INFO);
    }


  public void formateaImporte( EI_Tipo Tipo, int ind )
  {
	 double importe = 0;
     for(int i=0;i<Tipo.totalRegistros;i++)
	 {
		if(! (Tipo.camposTabla[i][ind].trim().equals("")) )
		{
			// importe=new Float(Tipo.camposTabla[i][ind]).floatValue();
			// Tipo.camposTabla[i][ind]=FormatoMoneda(Float.toString(importe));

			 Tipo.camposTabla[i][ind]=FormatoMoneda(Tipo.camposTabla[i][ind]);
		}
	  }
   }


/*************************************************************************************/
/**************************************************** Regresa el total de paginas    */
/*************************************************************************************/
   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }

//=================================================================================================
//#################################################################################################
//=================================================================================================

    public int generaTablaComisiones( String[][] arrayCuentasCargo,String Result,String Trama, String clavePerfil, String usuario, String contrato, HttpServletRequest req, HttpServletResponse res)
                                    throws ServletException, IOException
    {

        String totales          = "";
        String botones          = "";
        String mensajeError     = "Error inesperado.";
        String tipoOperacion    = "COMI"; //servicio de consulta de comisiones cobradas
        String tablaRegistros   = "";

        boolean comuError=false;

        String strPagina      = (String)req.getParameter("Pagina");
        String strRegPagina   = (String)req.getParameter("Numero");
		if(strPagina == null) strPagina = "0";
		if(strRegPagina == null) strRegPagina = "30";

		int fin         = 0;
        int pagina      = Integer.parseInt(strPagina);
        int regPagina   = Integer.parseInt(strRegPagina);

		BaseResource session = (BaseResource) req.getSession().getAttribute("session");

//================================= llamada al servicio ===========================================

        ServicioTux TuxGlobal = new ServicioTux();
        //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

        // trama de env�o 2<MEDIO_ENTREGA>|<USUARIO>|COMI|<CONTRATO>|<USUARIO>|<PERFIL>|

        Trama = IEnlace.medioEntrega2   + "|" +
                usuario                 + "|" +
                tipoOperacion           + "|" +
                contrato                + "|" +
                usuario                 + "|" +
                clavePerfil             + "|" ;

        EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

        try
        {
            Hashtable hs = TuxGlobal.web_red(Trama);
            Result= ""+ (String) hs.get("BUFFER") +"" ;
            EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
        }
        catch( java.rmi.RemoteException re )
        {
            re.printStackTrace();
        }
        catch(Exception e){}

        //========================== archivo del servicio =========================================

        String arcLinea         = "";
        String cadenaTCT        = "";
        String cuentaCargo      = "";
        String cadenaArchivo    = "";
        String nombreArchivo    = "";

        int     totalLineas     = 0;
        boolean status          = false;

        nombreArchivo           = IEnlace.LOCAL_TMP_DIR+"/"+usuario;
        //nombreArchivo           = IEnlace.LOCAL_TMP_DIR+"/comcob";// Simulando archivo del servicio

        //================================ Copia Remota ===========================================

        ArchivoRemoto archR = new ArchivoRemoto();

        if(!archR.copia(usuario))
            EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);

        //=========================================================================================

        EI_Tipo     TCTArchivo  = new EI_Tipo(this);
        EI_Exportar ArcEnt      = new EI_Exportar(nombreArchivo);

        if(ArcEnt.abreArchivoLectura())
        {
            boolean noError=true;
            do
            {
                arcLinea=ArcEnt.leeLinea();
                if(totalLineas==0)
                    if((arcLinea.substring(0,2).equals("OK") || arcLinea.substring(16,24).equals("ENCO0000")))
                        status=true;
                if(!arcLinea.equals("ERROR"))
                {
                    if(arcLinea.substring(0,5).trim().equals("ERROR")) break;

                    if (totalLineas == 1) cuentaCargo = arcLinea.substring(0,11).trim();

                    totalLineas++;

                    if(totalLineas>=3) cadenaTCT += arcLinea.substring(0,arcLinea.length()-1)+" @";
                }
                else noError=false;
            }while(noError);

            ArcEnt.cierraArchivo();

            if(arcLinea.substring(0,5).trim().equals("ERROR") && totalLineas<=1 && !status)
            {
                comuError       = true;
                mensajeError    = "Su transacci�n no puede ser atendida. Por favor intente mas tarde.";
            }
        }
        else
        {
            comuError       = true;
            mensajeError    = "Problemas de comunicacion. Por favor intente mas tarde.";
        }

        /********************************************************************************/

        if(!comuError)
        {
            TCTArchivo.iniciaObjeto(';','@',cadenaTCT);

            if(TCTArchivo.totalRegistros>=1)
            {
                botones+="\n <br>";
                botones+="\n <table border=0 align=center>";
                botones+="\n  <tr>";
                botones+="\n   <td align=center class='texfootpagneg'>";
                if(pagina>0) botones+="&lt; <a href='javascript:BotonPagina(0);' class='texfootpaggri'>Anterior</a>&nbsp;";

                if(calculaPaginas(TCTArchivo.totalRegistros,regPagina)>=2)
                {
                    for(int i=1;i<=calculaPaginas(TCTArchivo.totalRegistros,regPagina);i++)
                        if(pagina+1==i) botones+="&nbsp;"+Integer.toString(i)+"&nbsp;";
                        else            botones+="&nbsp;<a href='javascript:BotonPagina("+Integer.toString(i+3)+");' class='texfootpaggri'>"+Integer.toString(i)+"</a>&nbsp;";
                }

                if(TCTArchivo.totalRegistros>((pagina+1)*regPagina))
                {
                    botones+="<a href='javascript:BotonPagina(1);' class='texfootpaggri'>Siguiente</a> &gt;";
                    fin=regPagina;
                }
                else fin=TCTArchivo.totalRegistros-(pagina*regPagina);

                  botones+="\n   </td>";
                  botones+="\n  </tr>";
                  botones+="\n  <tr>";
                  botones+="\n    <td align=center><br><a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0></a></td>";
                  botones+="\n  </tr>";
                  botones+="\n </table>";

/************************************************* Encabezado de la pagina ... */
                  tablaRegistros+="\n <table border=0 cellpadding=2 cellspacing=3 align=center>";
                  tablaRegistros+="\n <tr>";
                  tablaRegistros+="\n <td>";

          tablaRegistros+="\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>";
          tablaRegistros+="\n  <tr>";
          tablaRegistros+="\n    <td class='textabref'><b>Cuenta de Cargo: <font color=red>"+cuentaCargo+"</font>&nbsp;</td>";
          tablaRegistros+="\n    <td align=right class='textabref'>&nbsp;Pagina <font color=blue>"+Integer.toString(pagina+1)+"</font> de <font color=blue>"+Integer.toString(calculaPaginas(TCTArchivo.totalRegistros,regPagina))+"</font>&nbsp;</b></td>";
          tablaRegistros+="\n  </tr>";
          tablaRegistros+="\n  <tr>";
          tablaRegistros+="\n    <td align=left class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red>"+Integer.toString(TCTArchivo.totalRegistros)+"</font></td>";
          tablaRegistros+="\n    <td align=right class='textabref'>&nbsp;<b>Registros <font color=blue>"+Integer.toString((pagina*regPagina)+1)+" - "+Integer.toString((pagina*regPagina)+fin)+"</font></b>&nbsp;</td>";
          tablaRegistros+="\n  </tr>";
          tablaRegistros+="\n </table>";

                  EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Desplegando tabla TCTArchivo parametros: "+Integer.toString(pagina*regPagina)+","+Integer.toString(fin), EIGlobal.NivelLog.INFO);

                  tablaRegistros+="\n </td>";
                  tablaRegistros+="\n </tr>";
                  tablaRegistros+="\n <tr>";
                  tablaRegistros+="\n <td>";

                  tablaRegistros+=seleccionaRegistrosComisiones(pagina*regPagina,fin,TCTArchivo);

                  tablaRegistros+="\n </td>";
                  tablaRegistros+="\n </tr>";
                  tablaRegistros+="\n </table>";


			EIGlobal gsl = new EIGlobal(session,getServletContext(),this);

          req.setAttribute("TotalTrans",TCTArchivo.strOriginal);
          req.setAttribute("FechaHoy",gsl.fechaHoy("dt, dd de mt de aaaa"));
          req.setAttribute("Botones",botones);
          req.setAttribute("Registros",tablaRegistros);
          req.setAttribute("Numero",Integer.toString(regPagina));
          req.setAttribute("Pagina",Integer.toString(pagina));
          req.setAttribute("Modulo","1");

                  req.setAttribute("MenuPrincipal", session.getStrMenu());
                  req.setAttribute("newMenu", session.getFuncionesDeMenu());
                  req.setAttribute("Encabezado", CreaEncabezado("Consulta de Comisiones Cobradas","Consultas &gt; Comisiones &gt; Comisiones Cobradas","s25490h",req));

          req.setAttribute("NumContrato",session.getContractNumber());
                  req.setAttribute("NomContrato",session.getNombreContrato());
                  req.setAttribute("NumUsuario",session.getUserID());
                  req.setAttribute("NomUsuario",session.getNombreUsuario());

          evalTemplate("/jsp/MDI_Consultar.jsp", req, res);
        }
       else
                {
                    EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): No existen Registros.", EIGlobal.NivelLog.INFO);
                    despliegaPaginaError("No se encontraron movimientos de Comisiones Cobradas realizadas.","Consulta de Comisiones Cobradas","Consultas &gt; Comisiones &gt; Comisiones Cobradas", req, res);
                }
     }
    else
         {
            EIGlobal.mensajePorTrace("ConComision - generaTablaComisiones(): Error."+mensajeError, EIGlobal.NivelLog.INFO);
            despliegaPaginaError(mensajeError,"Consulta de Comisiones Cobradas","Consultas &gt; Comisiones &gt; Comisiones Cobradas", req, res );
         }
    return 1;
  }


/*************************************************************************************/
/********************************* tabla de movimientos seleccionados por intervalo  */
/*************************************************************************************/

   String seleccionaRegistrosComisiones(int ini, int fin, EI_Tipo Trans)
    {
      String strTabla="";
      String colorbg="";
        String centrado  ="";
        String derecha   ="";
        String izquierda ="";
        String param="";

//      String str="";
//      String strComprobante="";

      if(Trans.totalRegistros>=1)
       {
         int c[]={1,0,2,3,4};

/*
    0- Descripci�n comisi�n
    1- Fecha
    2- N�mero de operaciones
    3- Monto operado
    4- Monto comision

    Fecha de aplicacion
    Tipo de comision
    No. de operaciones
    Tarifa unitaria
    Monto de comision
*/


         strTabla+="\n<table border=0 align=center>";

         strTabla+="\n<tr>";
         strTabla+="\n<th class='tittabdat'>&nbsp; Fecha de aplicaci&oacute;n &nbsp; </th>";
         strTabla+="\n<th class='tittabdat'>&nbsp; Tipo de comisi&oacute;n &nbsp; </th>";
         strTabla+="\n<th class='tittabdat'>&nbsp; No. Operaciones &nbsp; </th>";
         strTabla+="\n<th class='tittabdat'>&nbsp; Tarifa unitaria &nbsp; </th>";
         strTabla+="\n<th class='tittabdat'>&nbsp; Monto de comisi&oacute;n &nbsp; </th>";
         strTabla+="\n</tr>";

         EIGlobal.mensajePorTrace("ConComision - seleccionaRegistrosComisiones(): Generando la Tabla.", EIGlobal.NivelLog.INFO);

         if(fin>Trans.totalRegistros)
          fin=Trans.totalRegistros;

           EIGlobal.mensajePorTrace("ConComision - seleccionaRegistrosComisiones(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);

            formateaImporte(Trans, 3);
            formateaImporte(Trans, 4);

            centrado    =" align=center";
            izquierda   =" align=left";
            derecha     =" align=right";

         for(int i=ini;i<(ini+fin);i++)
          {
                        if((i%2)==0)
             colorbg="class='textabdatobs'";
           else
             colorbg="class='textabdatcla'";
            strTabla+="\n<tr>";

            for(int j=0;j<5;j++)
              {
                if (j==2)            param = colorbg + centrado;
                else if (j==3||j==4) param = colorbg + derecha;
                else                 param = colorbg ;

                EIGlobal.mensajePorTrace("ConComision - seleccionaRegistrosComisiones(): valor del registro " +Integer.toString(j)+" = "+Trans.camposTabla[i][c[j]], EIGlobal.NivelLog.DEBUG);

                strTabla+="<td "+param+"> &nbsp; "+Trans.camposTabla[i][c[j]]+" &nbsp; </td>";

              }
            strTabla+="\n</tr>";
          }
                 strTabla+="\n <tr>";
         strTabla+="\n</table>";
       }
      return strTabla;
    }

//=================================================================================================

}