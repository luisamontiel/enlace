package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.TI_CuentaDispersion;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.IOException;
import java.rmi.*;
//import javax.sql.*;
import java.sql.*;


public class TIConSaldos extends BaseServlet
	{

	// --- Entrada por m�todo GET ------------------------------------------------------
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// --- Entrada por m�todo POST -----------------------------------------------------
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// --- MAIN (Este m�todo se ejecuta en cuanto se invoca al servlet) ----------------
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		debug("Entrando a defaultAction()");

		//No continuar si la sesi�n no es v�lida
		if(!SesionValida(req,res)) return;

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		// Se declaran en inicializan variables y par�metros
		String parTotal = (String)req.getParameter("Total");
		String parOpcAnt = (String)req.getParameter("OpcAnt");
		String parAccion = (String)req.getParameter("Accion");
		String parStrOpcion = (String)req.getParameter("opcion");
		String parParciales = (String)req.getParameter("Parciales");
		String parMontoTotal = (String)req.getParameter("MontoTotal");

		if(parTotal == null) parTotal = "";
		if(parOpcAnt == null) parOpcAnt = "";
		if(parAccion == null) parAccion = "0";
		if(parStrOpcion == null) parStrOpcion = "0";
		if(parParciales == null) parParciales = "";
		if(parMontoTotal == null) parMontoTotal = "";

		int opcion = (parStrOpcion.equals("1"))?1:0;
		int accion = Integer.parseInt(parAccion);
		double[] sumas;
		String archAyuda;

		final int numCtasXpag = Global.NUM_REGISTROS_PAGINA;

		parStrOpcion = (opcion == 1)?"concentraci&oacute;n":"dispersi&oacute;n";
		archAyuda = (opcion == 1)?"s29030h":"s29040h";

		setMensError("", req);
		setNombreArchivo(IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tei", req); 
                String monedaDefault = "";
                String Agrega = req.getParameter("hdnAgrega")!=null?(String)req.getParameter("hdnAgrega"):"";;

				String Divisa="";
				Divisa=(String)sess.getAttribute("DivisaDefault");
				System.out.println("DivisaDefault en Inicio es  " + Divisa);
				if(Divisa==null)Divisa="";
				if(Agrega==null){Agrega="";Divisa="";}
				if((Divisa != null || !Divisa.trim().equals("")) && parStrOpcion.equals("concentraci&oacute;n") && parStrOpcion.equals("dispersi&oacute;n") && Agrega.equals(""))
					Agrega=Divisa;
				else
					sess.removeAttribute("DivisaDefault");
				sess.setAttribute("DivisaDefault", Agrega);

		        System.out.println("Agrega en Inicio es  " + Agrega);
				System.out.println("Divisa en Inicio es  " + Divisa);

		// Se calculan datos
		if(parMontoTotal.equals("") || !parStrOpcion.substring(0,3).equals(parOpcAnt.substring(0,3)))
			{
			monedaDefault = creaArchivo(opcion, req);
                System.out.println("0  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF monedaDefault "+monedaDefault);
			sumas = montosYtamano(opcion, req);
			for(int a=1;a<sumas.length-1;a++) parParciales += "@" + sumas[a];
			parMontoTotal = "" + sumas[0];
			parTotal = "" + (int)sumas[sumas.length-1];
			}

		//se asignan los par�metros
                System.out.println("1  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF monedaDefault "+monedaDefault);
                //sess.setAttribute("DivisaDefault",monedaDefault);
                //sess.setAttribute("filtrarPor",filtrarPor);
		req.setAttribute("StrOpcion",parStrOpcion);


		//TODO BIT CU3171,CU3181 Inicia flujo, Consulta de saldos Concentraci�n y Dispersi�n.

		/**
		 * VSWF - FVC - I
		 * 17/Enero/2007
		 */
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
		BitaHelper bh;
		BitaTransacBean bt;
		try {
			bh = new BitaHelperImpl(req, session, sess);
			bt = new BitaTransacBean();
			bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			bt = (BitaTransacBean)bh.llenarBean(bt);

			if(req.getParameter(BitaConstants.FLUJO) != null && ((String)req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_SALDO_CONC)){
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_SALDO_CONC_ENTRA);
			}
			if(req.getParameter(BitaConstants.FLUJO) != null && ((String)req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_SALDO_DISP)){
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_SALDO_DISP_ENTRA);
			}
			bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
			bt.setNombreArchivo((getNombreArchivo(req) == null)? " ":getNombreArchivo(req));

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		/**
		 * VSWF - FVC - F
		 */

		if(getMensError(req).equals(""))
			{
			req.setAttribute("FuncionesMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado",CreaEncabezado("Consulta de saldos de " + parStrOpcion,"Tesorer�a &gt; Tesorer&iacute;a inteligente &gt; Consulta de saldos &gt; " + parStrOpcion, archAyuda, req));

			req.setAttribute("OpcAnt",parStrOpcion);
			req.setAttribute("Cuentas",tramaCuentas(opcion,accion,numCtasXpag,req));
			req.setAttribute("Parciales",parParciales);
			req.setAttribute("MontoTotal",parMontoTotal);

			req.setAttribute("Accion",parAccion);
			req.setAttribute("Total","" + parTotal);
			req.setAttribute("Ancho","" + numCtasXpag);

			}

		// Se muestran los resultados
		if(getMensError(req).equals("")){

			//TODO BIT CU3171,CU3181 Consulta de saldos Concentraci�n y Dispersi�n.

			/**
			 * VSWF - FVC - F
			 * 17/Enero/2007
			 */
			if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
			try {
				BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();

				bt = (BitaTransacBean)bh.llenarBean(bt);
				if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_SALDO_CONC)){
					bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_SALDO_CONC_OPER_REDSRV_TI02);
				}
				if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_SALDO_DISP)){
					bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_SALDO_DISP_OPER_REDSRV_TI02);
				}
				bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
				bt.setNombreArchivo((getNombreArchivo(req) == null)?" ":getNombreArchivo(req));
				bt.setServTransTux("TI02");
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			}
			/**
			 * VSWF - FVC - I
			 */
			evalTemplate("/jsp/TIConSaldos.jsp",req,res);
		}
		else
			despliegaMensaje(getMensError(req),"Consulta de saldos de " + parStrOpcion,"Tesorer�a &gt; Tesorer&iacute;a inteligente &gt; Consulta de saldos &gt; " + parStrOpcion, req, res, archAyuda,parStrOpcion);

		debug("Saliendo de defaultAction()");
		}

	// --- Devuelve una trama la info de las cuentas -----------------------------------
	private String tramaCuentas(int opcion, int offset, int numCtasXpag, HttpServletRequest req)
		{
		debug("Entrando a tramaCuentas()");
		int a;
		String datos = "";
		String linea;
		BufferedReader leeArch;
		File archivo = new File(getNombreArchivo(req));

		try
			{
			if(!archivo.exists()) creaArchivo(opcion, req);
			leeArch = new BufferedReader(new FileReader(archivo));
			for(a=0;(linea = leeArch.readLine()) != null && a<offset;a++);
			if(linea == null)
				{
				if(archivo.length() == 0)
					setMensError("La estructura est&aacute; vac&iacute;a", req);
				else
					throw new Exception();
				}
			else
				{
				datos += "@" + linea;
				for(a=1;(linea = leeArch.readLine()) != null && a<numCtasXpag;a++) datos += "@" + linea;
				}
			}
		catch(Exception e)
			{
			datos = "";
			debug("Error en tramaCuentas(): " + e.toString());
			setMensError("Su transacci&oacute;n no puede ser atendida en este momento.<BR>Intente m&aacute;s tarde", req);
			}
		finally
			{
			debug("Saliendo de tramaCuentas()");
			return datos;
			}
		}

	// --- Regresa una arreglo con valores total (0), num. cuentas (el �ltimo) y parciales (los dem�s) -----------
	private double[] montosYtamano(int opcion, HttpServletRequest req)
		{
		debug("Entrando a montosYtamano()");
		int nivel;
		int numCtas;
		double saldo;
		double arreglo[] = null;
		Vector sumas = new Vector();
		File archivo = new File(getNombreArchivo(req));
		BufferedReader leeArch;
		String linea;
		StringTokenizer divideTokens;
		Double sumaParcial;

		try
			{
			sumas.add(new Double(0.0));
			if(!archivo.exists()) creaArchivo(opcion, req);
			leeArch = new BufferedReader(new FileReader(archivo));
			numCtas = 0;
			while((linea = leeArch.readLine()) != null)
				{
				numCtas++;
				divideTokens = new StringTokenizer(linea,"|");
				divideTokens.nextToken();
				nivel = Integer.parseInt(divideTokens.nextToken());
				saldo = Double.parseDouble(divideTokens.nextToken());
				while(nivel >= sumas.size()) sumas.add(new Double(0.0));
				sumaParcial = (Double)sumas.get(nivel);
				saldo += sumaParcial.doubleValue();
				sumas.set(nivel,new Double(saldo));
				}
			saldo = 0;
			for(int a=1;a<sumas.size();a++)
				{
				sumaParcial = (Double)sumas.get(a);
				saldo += sumaParcial.doubleValue();
				}
			sumas.set(0,new Double(saldo));
			arreglo = new double[sumas.size()+1];
			for(int a=0;a<sumas.size();a++) arreglo[a] = ((Double)sumas.get(a)).doubleValue();
			arreglo[sumas.size()] = (double)numCtas;
			}
		catch(Exception e)
			{
			arreglo = null;
			debug("Error en montosYtamano(): " + e.toString());
			setMensError("Su transacci&oacute;n no puede ser atendida en este momento.<BR>Intente m&aacute;s tarde", req);
			}
		finally
			{
			debug("Saliendo de montosYtamano()");
			return arreglo;
			}
		}

	// --- Crea un archivo con los datos por mostrar -----------------------------------
	private String creaArchivo(int opcion, HttpServletRequest req)
		{
		HttpSession sess = req.getSession();
        String filtrarPor = req.getParameter("hdnAgrega")!=null?(String)req.getParameter("hdnAgrega"):"";;
		debug("Entrando a creaArchivo()");
		File archivo = new File(getNombreArchivo(req));
		FileWriter escrArch;
		String trama, aviso, linea;
		Vector cuentas;
                Vector cuentasTemp;
		TI_CuentaDispersion cuenta;
                String monedaDefault = "";
		try
			{
			//Se obtiene el arbol
			trama = enviaTuxedo((opcion == 1)?"CONCENTRACION":"DISPERSION", req);
			trama = leeArchivo(trama,req);
			aviso = trama.substring(0,trama.indexOf("@"));
			trama = trama.substring(trama.indexOf("@"));
			if(!aviso.substring(0,6).equals("TRAMA_")) {throw new Exception(trama.substring(17));}
                        System.out.println("--------------------- TRAMA "+trama);

                        //monedaDefault = devuelveDivisaDefault(trama);
						monedaDefault = (String)sess.getAttribute("DivisaDefault");
                        System.out.println("------------- monedaDefault "+monedaDefault);

						cuentasTemp = creaCuentas(ordenaArbol(trama, req), req);
                        String filtroTemp = monedaDefault;
                        if(filtrarPor != null && filtrarPor.trim().length() > 0){
                          filtroTemp = filtrarPor;
                        }
						System.out.println("------------- filtroTemp "+filtroTemp);
                        cuentas = depuraCuentas(cuentasTemp,filtroTemp);

			//Para cada cuenta se crea una trama y se a�ade al archivo
			if(archivo.exists()) archivo.delete();
			escrArch = new FileWriter(archivo);
			while(cuentas.size()>0)
				{
				cuenta = (TI_CuentaDispersion)cuentas.remove(0);
				linea = cuenta.getNumCta() + " - " + cuenta.getDescripcion() + "|" + cuenta.nivel() + "|";
				trama = enviaTuxedo(cuenta.getNumCta(), req);
				if(!trama.substring(0,2).equals("OK"))
					{
					throw new Exception(trama.substring(16));
					}
				trama = trama.substring(16,31).trim();
				linea += trama + "\n";
				escrArch.write(linea);
				}
			escrArch.close();

			}
		catch(Exception e)
			{
			debug("Error en creaArchivo(): " + e.toString());
			setMensError(e.getMessage(), req);
			}
		finally
			{debug("Saliendo de creaArchivo()");}
                  return monedaDefault;
		}
	// --- Ejecuta una consulta a Tuxedo y regresa el aviso de Tuxedo ------------------
	private String enviaTuxedo(String tipo, HttpServletRequest req)
		{
		debug("Entrando a enviaTuxedo()");
		String trama = "";
		Hashtable ht;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		//Observacion Juan Ramon Tejada para Rafa Villar (En todas las tramas)
		//Verificar que ni el userID ni el contract number ni el UserProfile sean nulos o vacios, de lo contrario no armar la trama pues va a tronar
		//  generalmente el profile es igual al useid  si viene el perfil vacio convendria enviarle el userid

        String Divisa = "";
		Divisa = (String)sess.getAttribute("DivisaDefault");
		System.out.println("DivisaDefault en enviaTuxedo es  " + Divisa);
		System.out.println("Tipo es  >" + tipo+"<");
		if(Divisa==null){Divisa="";}
		if(Divisa.equals("") && (tipo.equals("DISPERSION") || tipo.equals("CONCENTRACION"))){
			Divisa="ALL";
		}
		System.out.println("Divisa Final es  " + Divisa);

	    /*if (sess.getAttribute("ctasUSD").equals("Existe"))
	    {
	  	  if(sess.getAttribute("ctasMN").equals("Existe"))
	  	  {
	 		Divisa = "MN";
	  	  }
	  	  else
	  	  {
	   		Divisa = "USD";
	  	  }
	    }
	    else
	    {
	  	  Divisa="MN";
	    }
		*/
	    System.out.println("*****Divisa*****" + Divisa);

		/*
		if(tipo.equals("DISPERSION"))
			{trama = "2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@";}
		else if(tipo.equals("CONCENTRACION"))
			{trama = "2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@C@";}
		else
			{trama = "1EWEB|"+session.getUserID()+"|SDCT|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+tipo+"|P";}
		*/

		if(tipo.equals("DISPERSION"))
			{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@|"+Divisa+"|";}
//			{trama = "2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@|"+Divisa+"|";}
		else if(tipo.equals("CONCENTRACION"))
			{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@C@|"+Divisa+"|";}
//			{trama = "2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@C@|"+Divisa+"|";}
		else {
                  if(tipo != null && (tipo.startsWith("82") || tipo.startsWith("83") || tipo.startsWith("49"))){
                    Divisa = "USD";
                  }
                  else{
                    Divisa = "MN";
                  }
                  trama = "1EWEB|"+session.getUserID8()+"|SDCT|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+tipo+"|P|"+Divisa+"|";
                }


		try
			{
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			ht = tuxedo.web_red(trama);
			trama = (String)ht.get("BUFFER");
			debug("Tuxedo regresa: " + trama);
			if(tipo.equals("DISPERSION") || tipo.equals("CONCENTRACION"))
				{
// ----------------------------------------------------- Secci�n de archivo remoto -----
				trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
				if(archRemoto.copia(trama))
					{
					debug("Copia remota realizada (Tuxedo a local)");
					trama = Global.DIRECTORIO_LOCAL + "/" +trama;
					}
				else
					{
					debug("Copia remota NO realizada (Tuxedo a local)");
					setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
					}
// -------------------------------------------------------------------------------------
				}
			}
		catch(Exception e)
			{
			debug("Error intentando conectar a Tuxedo: " + e.toString() + " <---> " + e.getMessage() +  " <---> " + e.getLocalizedMessage());
			setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
			}
		finally
			{
			debug("trama de regreso: " + trama);
			debug("Saliendo de enviaTuxedo()");
			return trama;
			}
		}

	// --- Lee el archivo de tuxedo y regresa una trama de cuentas ---------------------
	private String leeArchivo(String nombreArchivo, HttpServletRequest req)
		{
		debug("Entrando a leeArchivo()");
		StringBuffer tramaT = new StringBuffer(""), tramaR = new StringBuffer("");
		StringTokenizer tokens;
		FileReader archivo;
		String tipoArbol, strAux;
		Vector cuentas;
		TI_CuentaDispersion cuenta;
		int car, a, b;
		String DivisaConsulta="";

		try
			{
			HttpSession sess = req.getSession();
			// Se lee del archivo al StringBuffer
			archivo = new FileReader(nombreArchivo);
			car = 0; while(car != -1) {car = archivo.read(); tramaT.append("" + (char)car);}
			archivo.close();

			// Se "corrige" la trama
			while((a = tramaT.toString().indexOf("@@")) != -1) tramaT.insert(a+1," ");

			// Se arma la trama mientras se verifica su integridad
			tipoArbol = "";
			if(!tramaT.toString().substring(17,25).equals("TEIN0000"))
				{tramaR = tramaT; tramaR.delete(0,26); tramaR.insert(0,"<<ERROR>>@trama de tuxedo: ");}
			else
				{
				tokens = new StringTokenizer(tramaT.toString(),"@");
				cuentas = new Vector();
				for(a=0;a<3;a++) tokens.nextToken();
				DivisaConsulta = tokens.nextToken();
				car = Integer.parseInt(tokens.nextToken());
				System.out.println("La Divisa en Consulta Saldos es : "+ DivisaConsulta);
				System.out.println("EL car    en Consulta Saldos es : "+ car);
				sess.setAttribute("DivisaDefault",DivisaConsulta);
				for(a=0;a<car;a++)
					{
					tramaR.append("@" + tokens.nextToken() + "|");
					tokens.nextToken();
					tipoArbol = tokens.nextToken();
					strAux = tokens.nextToken();
					tramaR.append((strAux.equals("0"))?" ":strAux);
					tramaR.append("|" + tokens.nextToken() + "|0|0.0|N|N|0");
					}
				tramaR.insert(0,"TRAMA_" + tipoArbol + "@");
				}
			}
		catch(Exception e)
			{
			debug("Error en leeArchivo(): " + e.toString());
			tramaR = new StringBuffer("<<ERROR>>@trama en el archivo: " + tramaT.toString());
			}
		finally
			{
			debug("Saliendo de leeArchivo()");
			return tramaR.toString();
			}
		}

        private Vector depuraCuentas(Vector cuentasTemp, String divisa){
          Vector cuentasFinal = new Vector();
          for(int v = 0; v < cuentasTemp.size(); v++){
            TI_CuentaDispersion cuentaDisp = (TI_CuentaDispersion)cuentasTemp.elementAt(v);
            if(divisa.equals("MN") || divisa.equals("ALL")){
              if(!(cuentaDisp.getNumCta().startsWith("82") || cuentaDisp.getNumCta().startsWith("83") || cuentaDisp.getNumCta().startsWith("49")) ){
                cuentasFinal.add(cuentaDisp);
              }
            }
            else{
              if(cuentaDisp.getNumCta().startsWith("82") || cuentaDisp.getNumCta().startsWith("83") || cuentaDisp.getNumCta().startsWith("49") ){
                cuentasFinal.add(cuentaDisp);
              }
            }
          }
          return cuentasFinal;
        }

        private String devuelveDivisaDefault(String tramaCuentas){
          String divisaDefault = "ALL";
          StringTokenizer st = new StringTokenizer(tramaCuentas,"@");
          int countUsd = 0;
          int countMn = 0;
          while(st.hasMoreTokens()){
            String cuentaTemp = st.nextToken();
            if(cuentaTemp != null && (cuentaTemp.startsWith("82") || cuentaTemp.startsWith("83") || cuentaTemp.startsWith("49"))){
              countUsd++;
            }
            else{
              countMn++;
            }
          }
          if(countUsd > 0 && countMn <= 0){
            divisaDefault = "USD";
          }
          else if(countUsd <= 0 && countMn > 0){
            divisaDefault = "MN";
          }
          return divisaDefault;
        }

	// --- Regresa un vector de cuentas a partir de una trama --------------------------
	public Vector creaCuentas(String tramaCuentas, HttpServletRequest req)
		{
		debug("Entrando a creaCuentas()");
		Vector cuentas = null;
		StringTokenizer tokens;
		TI_CuentaDispersion ctaAux1, ctaAux2;
		int a, b;

		try
			{
			cuentas = new Vector();
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				cuentas.add(TI_CuentaDispersion.creaCuentaDispersion("@" + tokens.nextToken()));
				for(a=0;a<cuentas.size();a++)
					{
					ctaAux1 = (TI_CuentaDispersion)cuentas.get(a);
					if(!ctaAux1.posiblePadre.equals(""))
						for(b=0;b<cuentas.size();b++)
							{
							ctaAux2 = (TI_CuentaDispersion)cuentas.get(b);
							if(ctaAux2.getNumCta().equals(ctaAux1.posiblePadre))
								{
								ctaAux1.setPadre(ctaAux2);
								b=cuentas.size();
								}
							}
					}
				}
			}
		catch(Exception e)
			{
			debug("Error en creaCuentas(): " + e.toString());
			cuentas = new Vector();
			setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
			}
		finally
			{
			debug("Saliendo de creaCuentas()");
			return cuentas;
			}
		}

	// --- Ordena las cuentas seg�n el arbol -------------------------------------------
	private String ordenaArbol(String tramaEntrada, HttpServletRequest req)
		{
		debug("Entrando a ordenaArbol()");
		String tramaSalida;
		Vector cuentas, ctasOrden;
		TI_CuentaDispersion ctaAux;
		int a, b;

		b = 1;
		cuentas = creaCuentas(tramaEntrada, req);
		ctasOrden = new Vector();
		while(cuentas.size() > 0)
			{
			for(a=0;a<cuentas.size();a++)
				{
				ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
				if(ctaAux.nivel() == b) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
				}
			b++;
			}
		cuentas = ctasOrden;
		ctasOrden = new Vector();
		for(a=0;a<cuentas.size();a++)
			{
			ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
			if(ctaAux.nivel() == 1) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
			}
		while(cuentas.size() > 0)
			{
			ctaAux = ((TI_CuentaDispersion)cuentas.get(0));
			ctasOrden.add(ctasOrden.indexOf(ctaAux.getPadre()) + 1,ctaAux);
			cuentas.remove(ctaAux);
			}
		cuentas = ctasOrden;
		tramaSalida = "";
		for(a=0;a<cuentas.size();a++) tramaSalida += ((TI_CuentaDispersion)cuentas.get(a)).trama();

		debug("Saliendo de ordenaArbol()");
		return tramaSalida;
		}

	//--- Toma el mensaje de error de la sesi�n ----------------------------------------
	private String getMensError(HttpServletRequest req)
		{

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String mensError = (String)sess.getAttribute("TI_MensError"); if(mensError == null) mensError = "";
		return mensError;
		}

	//--- Indica el mensaje de error para guardar en la sesi�n -------------------------
	private void setMensError(String mensError, HttpServletRequest req)
		{

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		sess.setAttribute("TI_MensError",mensError);
		}

	//---
	private String getNombreArchivo(HttpServletRequest req)
		{

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		return (String)sess.getAttribute("TI_NombreArchivo");
		}

	//---
	private void setNombreArchivo(String info, HttpServletRequest req)
		{

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		sess.setAttribute("TI_NombreArchivo",info);
		}

	//--- Mensaje de debugeo -----------------------------------------------------------
	private void debug(String mensaje) {EIGlobal.mensajePorTrace("<DEBUG TIConSaldos> " + mensaje, EIGlobal.NivelLog.INFO);}

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	public void despliegaMensaje(String mensaje, String param1, String param2, HttpServletRequest request, HttpServletResponse response, String pagAyuda, String parStrOpcion) throws ServletException, IOException
		{

		// parche de Ram�n Tejada
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal(session , getServletContext() , this );

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		//request.setAttribute( "URL", "javascript:history.go(-1)" );
		System.out.println("++++++++++++++++++++++++++++++++  parStrOpcion  ...................." + parStrOpcion);
		if(parStrOpcion.equals("concentraci&oacute;n")){
			request.setAttribute("URL", "TIConSaldos?opcion=1&Accion=0" );
		}
		if(parStrOpcion.equals("dispersi&oacute;n")){
			request.setAttribute("URL", "TIConSaldos?opcion=0&Accion=0" );
		}

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, pagAyuda, request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );  
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		sess.setAttribute( "Encabezado", null);
		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}

	}
//ERROR3     50561Su transaccion no puede ser atendida en este momento
// ERROR3     50571Su transaccion no puede ser atendida en este momento