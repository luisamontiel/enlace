package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.beans.CtaMovilBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConsultaCuentaMovilBO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Servlet implementation class ConCuentasMovil
 */
public class ConCuentasMovil extends  BaseServlet {

	/** Constante para Lista Cuentas*/
    private static final String LISTA_CUENTA = "listaCuentas";

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    public void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    	EIGlobal.mensajePorTrace("ConCuentasMovil --> ************ INICIANDO CONSULTA ************" , EIGlobal.NivelLog.INFO);
    	lupaCtas(request, response);
    }

	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    public void lupaCtas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ConsultaCuentaMovilBO consultaBO =  new ConsultaCuentaMovilBO();
		List<CtaMovilBean> cuentas = new ArrayList<CtaMovilBean>();

		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		String contratoAnterior = (String) request.getSession().getAttribute("contratoAnterior");
		String contratoActual = session.getContractNumber();

		boolean cambioContrato = cambioContrato(request);

		String opcion = request.getParameter("opcion") != null && !"".equals(request.getParameter("opcion"))
			? request.getParameter("opcion") : "";

		EIGlobal.mensajePorTrace("ConCuentasMovil lupaCtas() ---> opcion: " + opcion, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConCuentasMovil lupaCtas() ---> Segundo Mensaje " , EIGlobal.NivelLog.INFO);

		if (("".equals(opcion) && request.getSession().getAttribute(LISTA_CUENTA) == null)
				|| cambioContrato) {
			cuentas = consultaBO.consultaCuentas(contratoActual);


			EIGlobal.mensajePorTrace("ConCuentasMovil lupaCtas() ---> Longitud de la lista: "+ cuentas.size(),EIGlobal.NivelLog.DEBUG);

			request.setAttribute(LISTA_CUENTA, cuentas);
//			request.getSession().setAttribute(LISTA_CUENTA, cuentas);
		} else {
			filtrarDatos(request);

		}

		evalTemplate("/jsp/consultaCtaMovil.jsp", request, response);
	}

	/**
	 * metodo para filtrar las cuentas
	 * @param request : HttpServletRequest
	 */
	public void filtrarDatos(HttpServletRequest request)  {

		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		String contratoAnterior = (String) request.getSession().getAttribute("contratoAnterior");
		String contratoActual = session.getContractNumber();

		ConsultaCuentaMovilBO consultaBO =  new ConsultaCuentaMovilBO();
		List<CtaMovilBean> cuentas = new ArrayList<CtaMovilBean>();

		String cuenta = request.getParameter("filtroCuenta") != null ? request.getParameter("filtroCuenta") : "";
		String descripcion = request.getParameter("filtroDesc") != null ? request.getParameter("filtroDesc") : "";

		cuentas = consultaBO.consultaCuentas(contratoActual);

		List<CtaMovilBean> cuentasTMP = new ArrayList<CtaMovilBean>();
		List<CtaMovilBean> cuentasNueva = new ArrayList<CtaMovilBean>();
		request.setAttribute(LISTA_CUENTA, cuentas);
		cuentasTMP = (List<CtaMovilBean>)request.getAttribute(LISTA_CUENTA);

//		cuentasTMP = (List<CtaMovilBean>)request.getSession().getAttribute(LISTA_CUENTA);



		EIGlobal.mensajePorTrace("ConCuentasMovil filtrarDatos() ---> BEAN MOVIL " +cuentasTMP, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("ConCuentasMovil filtrarDatos() ---> cuenta: " + cuenta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConCuentasMovil filtrarDatos() ---> descripcion: " + descripcion, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConCuentasMovil filtrarDatos() ---> cuenta length: " + cuenta.length(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConCuentasMovil filtrarDatos() ---> descripcion: length" + descripcion.length(), EIGlobal.NivelLog.INFO);

		if (cuenta.length() > 0 || descripcion.length() > 0) {
			EIGlobal.mensajePorTrace(this.getClass() + "-----> con filtro..." , EIGlobal.NivelLog.INFO);
			for(int i = 0; i < cuentasTMP.size(); i++) {

				// Busqueda unicamente por cuenta.
				if((("".equals(descripcion.trim())) && (cuenta.length() > 0)) && (cuentasTMP.get(i).getCtaMovil().contains(cuenta))) {
						EIGlobal.mensajePorTrace(this.getClass() + "-----> con filtro cuenta" , EIGlobal.NivelLog.INFO);
						cuentasNueva.add(cuentasTMP.get(i));
						/** continue;*/
				}
				// Busqueda unicamente por descripcion.
				if((("".equals(cuenta.trim())) && (descripcion.length() > 0)) && (cuentasTMP.get(i).getTitular().contains(descripcion))) {
						EIGlobal.mensajePorTrace(this.getClass() + "-----> con filtro descripcion" , EIGlobal.NivelLog.INFO);
						cuentasNueva.add(cuentasTMP.get(i));
						/** continue;*/
				}
				// Busqueda por cuenta y por descripcion.
				if((cuenta.length() > 0 && descripcion.length() > 0) && (cuentasTMP.get(i).getCtaMovil().contains(cuenta) &&
						cuentasTMP.get(i).getTitular().contains(descripcion))) {
							EIGlobal.mensajePorTrace(this.getClass() + "-----> con filtro cuenta y descripcion" , EIGlobal.NivelLog.INFO);
							cuentasNueva.add(cuentasTMP.get(i));
							/** continue;*/
				}
			}

			if ((cuentasNueva.size())==0) {
				request.setAttribute("MensajeCtas01", "  ");
			}
			request.setAttribute(LISTA_CUENTA, cuentasNueva);
		} else {
			request.setAttribute(LISTA_CUENTA, cuentasTMP);
		}
	}



	/**
	 * Metodo para verificar si el cliente ha cambiado de contrato. Esto con la
	 * finalidad de determinar si es necesario volver a realizar la consulta de
	 * cuentas, ya que el catalogo vive en sesion y si el usuario cambia de
	 * contrato es necesario volver a consultarla en base de datos y guardarla
	 * nuevamente en sesion.
	 *
	 * @param request objeto HttpServletRequest.
	 * @return boolean que indica si el cliente cambio de contrato.
	 */
	public boolean cambioContrato(HttpServletRequest request) {
		EIGlobal.mensajePorTrace("ConCuentasMovil :: Inicio de metodo cambioContrato" , EIGlobal.NivelLog.INFO);
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		String contratoAnterior = (String) request.getSession().getAttribute("contratoAnterior");
		String contratoActual = session.getContractNumber();
		boolean cambioContrato = false;

		EIGlobal.mensajePorTrace("ConCuentasMovil :: contrato anterior: ["
				+ contratoAnterior + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConCuentasMovil :: contrato actual: ["
				+ contratoActual + "]", EIGlobal.NivelLog.INFO);

		// La primer vez que se ingresa al metodo se setea contratoAnterior.
		if (contratoAnterior == null) {
			contratoAnterior = contratoActual;
			request.getSession().setAttribute("contratoAnterior", contratoAnterior);
			cambioContrato = true;
		} else if (!contratoAnterior.equals(contratoActual)) {
			// Cuando el contrato cambia, la variable contratoAnterior toma el
			// valor del contrato actual del cliente para posteriormente validar
			// con dicho valor.
			contratoAnterior = contratoActual;
			request.getSession().setAttribute("contratoAnterior", contratoAnterior);
			cambioContrato = true;
		}
		EIGlobal.mensajePorTrace("ConCuentasMovil :: contratos distintos: ["
				+ cambioContrato + "]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("ConCuentasMovil :: Fin de metodo cambioContrato" , EIGlobal.NivelLog.INFO);
		// En caso de que el contrato no haya cambiado se retorna el valor default 'false'.
		return cambioContrato;
	}

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException {
		processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException {
		processRequest (request, response);
    }

}