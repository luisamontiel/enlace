/*Banco Santander Mexicano
  Clase comprobante_trans - Genera el comprobante para transf,vista,inversion 7/28,plazo
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 28/02/2001 - agregar encabezado
 */
package mx.altec.enlace.servlets;

import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.*;
import java.util.Enumeration;
import javax.naming.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import netscape.bsp.*;
import netscape.bsp.runtime.*;
import netscape.bsp.dataobject.*;

public class comprobante_cambios extends BaseServlet
{
    //EIGlobal EnlaceGlobal;

 	public comprobante_cambios()
	{
		super();
		//EnlaceGlobal = new EIGlobal(this);
	}

	public void init (ServletConfig config)
		throws ServletException
	{
		super.init(config);
		System.out.println("Metodo Init del Servlet comprobante_cambios");
	}

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		try {
			generar_comprobante_transferencias(req,res);
		} catch(Exception e) {
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, req, res );
		}
	}

    public int generar_comprobante_transferencias(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
   		EIGlobal.mensajePorTrace("***comprobante_cambios.class Entrando a generar_comprobante_cambios ", EIGlobal.NivelLog.INFO);

		int salida;


		String cta_origen  = req.getParameter("cta_origen");
		String cta_destino = req.getParameter("cta_destino");
		String importemn   = req.getParameter("importemn");
		String importeusd  = req.getParameter("importeusd");
		String tipocambio  = req.getParameter("tipocambio");
		String fecha       = req.getParameter("fecha");
		String concepto    = req.getParameter("concepto");
		String orden       = req.getParameter("orden");
		String referencia  = req.getParameter("referencia");
		String usuario     = req.getParameter("usuario");
		String contrato    = req.getParameter("contrato");
		String rfc         = req.getParameter("rfc");
		String iva         = req.getParameter("iva");
		String estatus     = req.getParameter("estatus");

		EIGlobal.mensajePorTrace("***comprobante_trans.class cta_origen : >" +cta_origen+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***comprobante_trans.class cta_destino : >"+cta_destino+ "<", EIGlobal.NivelLog.INFO);
   		EIGlobal.mensajePorTrace("***comprobante_trans.class importemn : " +importemn+ "<", EIGlobal.NivelLog.INFO);
   		EIGlobal.mensajePorTrace("***comprobante_trans.class importeusd : " +importeusd+ "<", EIGlobal.NivelLog.INFO);
   		EIGlobal.mensajePorTrace("***comprobante_trans.class tipocambio : " +tipocambio+ "<", EIGlobal.NivelLog.INFO);
   		EIGlobal.mensajePorTrace("***comprobante_trans.class fecha : " +fecha+ "<", EIGlobal.NivelLog.INFO);
   		EIGlobal.mensajePorTrace("***comprobante_trans.class concepto : " +concepto+ "<", EIGlobal.NivelLog.INFO);
  		EIGlobal.mensajePorTrace("***comprobante_trans.class orden : " +orden+ "<", EIGlobal.NivelLog.INFO);
  		EIGlobal.mensajePorTrace("***comprobante_trans.class referencia : " +referencia+ "<", EIGlobal.NivelLog.INFO);
   		EIGlobal.mensajePorTrace("***comprobante_trans.class usuario : " +usuario+ "<", EIGlobal.NivelLog.INFO);
   		EIGlobal.mensajePorTrace("***comprobante_trans.class contrato : " +contrato+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***comprobante_trans.class rfc : " +rfc+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***comprobante_trans.class iva : " +iva+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***comprobante_trans.class estatus : " +estatus+ "<", EIGlobal.NivelLog.INFO);
        //map.putString("hora",hora);
		req.setAttribute("contrato",contrato);
		req.setAttribute("usuario",usuario);
		req.setAttribute("cta_origen",cta_origen);
		req.setAttribute("cta_destino",cta_destino);
		String datimportemn="";
	    /*if ( (cta_origen.substring(0,2).equals("82"))||
           (cta_origen.substring(0,2).equals("83"))||
		   (cta_origen.substring(0,2).equals("49")))
      		req.setAttribute("importemn","");
	    else
	    {  */

	  	  datimportemn="<tr><td class=tittabcom align=right width=0>Importe MN:</td>";
		  datimportemn+="<td class=textabcom align=left nowrap>";
		  //datimportemn+=""+importemn;
 		  datimportemn+=""+FormatoMoneda(importemn);
          datimportemn+="</td></tr>";
  		  req.setAttribute("importemn",datimportemn);
        //}

		req.setAttribute("importeusd",FormatoMoneda(importeusd));
		/*req.setAttribute("tipocambio",FormatoMoneda(tipocambio));*/
		//req.setAttribute("importeusd",importeusd);
		req.setAttribute("tipocambio",tipocambio);
		req.setAttribute("fecha",fecha);
		req.setAttribute("hora",ObtenHora());
		EIGlobal.mensajePorTrace("***comprobante_trans.class concepto : >" +concepto+ "<", EIGlobal.NivelLog.INFO);

		req.setAttribute("concepto",concepto);
		req.setAttribute("orden",orden);
		req.setAttribute("referencia",referencia);
		req.setAttribute("rfc",rfc);
		req.setAttribute("iva",FormatoMoneda(iva.trim()));
  		//Q6973 Getronics
		req.setAttribute("estatus",estatus);
		EIGlobal.mensajePorTrace("***antes de SesionValida: ><", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***ClaveBanco: "+req.getParameter("banco"), EIGlobal.NivelLog.INFO);
        req.setAttribute("ClaveBanco:",req.getParameter("banco"));
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(IEnlace.CAMBIOS_COMPROB_TMPL);

		//TODO BIT CU3131 Genera comprobante de Transferencias Internacionales
		//TODO BIT CU3121 Genera comprobante Cambios

		/**
		 * VSWF - FVC - I
		 * 17/Enero/2007
		 */
		if (Global.USAR_BITACORAS.trim().equals("ON")){
			try {
				BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaHelper bh = new BitaHelperImpl(req, (BaseResource) req.getSession().getAttribute("session"), req.getSession());
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);

				if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
					bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_GENERA_COMPROBANTE);
				}else {
					if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
						bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_GENERA_COMPROBANTE);
					}
				}
				bt.setContrato((contrato == null)?" ":contrato);
				bt.setImporte(Double.parseDouble((importemn == null)?"0":importemn));
				bt.setTipoCambio(Double.parseDouble((tipocambio == null)?" ":tipocambio));
				bt.setCctaOrig((cta_origen == null)?" ":cta_origen);
				bt.setCctaDest((cta_destino == null)?" ":cta_destino);
				bt.setReferencia(Long.parseLong((referencia == null)?"0":referencia));
				bt.setNombreArchivo((usuario == null)? " ":usuario + ".doc");

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/**
		 * VSWF - FVC - F
		 */
		dispatcher.forward(req, res);

      //salida=evalTemplate(IEnlace.CAMBIOS_COMPROB_TMPL,(ITemplateData) null, map);
      return 1;
    }
}