package mx.altec.enlace.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Servlet implementation class FueraHorarioManejador
 */
public class FueraHorarioManejador extends BaseServlet {
    /** Numero serial de la clase */
	private static final long serialVersionUID = 1L;
	/** El objeto FLUJO TITULO. */
	private static final String FLUJO_TITULO = "Fuera de Horario";

	/** El objeto RUTA FLUJO. */
	private static final String RUTA_FLUJO = " Fuera de Horario";

	/** El objeto ID FLUJO. */
	private static final String ID_FLUJO = "Dispersion";

	/**
	 * Redirecciona hacia el metodo doPost.
	 * @param request Request
	 * @param response Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Envia al usuario hacia la pagina de error de la aplicacion.
	 * @param request Request
	 * @param response Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace("***FueraHorarioManejador :: doPost() :: " + "Intento de acceso a una operacion fuera de horario.", EIGlobal.NivelLog.INFO);
		despliegaPaginaError("Operaci\u00f3n fuera de horario de servicio.",
				FLUJO_TITULO, RUTA_FLUJO, ID_FLUJO, request, response);
	}

}
