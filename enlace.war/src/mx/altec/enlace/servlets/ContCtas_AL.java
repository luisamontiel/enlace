/** 
*   Isban Mexico
*   Clase: ContCtas_AL.java
*   Descripcion:Servlet que se encargada de la gestion de la consulta de cuentas.
*   
*   Control de Cambios:
*   
*   1.1 22/06/2016  FSW. Everis - Implementacion permite la gestion de transferencia en dolares(Spid). 
*/

/*
Christian Barrios Osornio CBO Q21952

SE SOLICITA INTEGRAR UN BOT�N DE "EXPORTAR" INFORMACI�N EN LA PANTALLA DE CONSULTA DE CUENTAS CLABE,
SE INTEGRA A ESTE DOCUMENTO LA SECUENCIA PARA ACCEDER A LA CONSULTA, PANTALLA ACTUAL,
PANTALLA REQUERIDA "NUEVA" Y LAYOUT DE INFORMACI�N A ARROJAR POR EL SISTEMA.

Se agrega un m�todo que guarda los datos de la cuenta clave en un archivo para ser mostrados
despues con el botonde exportar.


Christian Barrios Osornio CBO Q21952
*/

/*
Getronics Mexico, S.A. de C.V.
Modifico:Nancy Rodriguez Nevarez  NRN
Requerimiento:  Q05-12535  Fecha fin:24/06/05
Objetivo:Implementar Consulta Autorizacion y Cancelacion de cuentas
*/

/* everis - Alta de Cuentas - Enero 16, 2008
Modificador: Jose Antonio Rodriguez Alba
Objetivo: Manejar identificador de lote en la autorizacion de alta de cuentas
*/

/**
 * Cambios para PYMES, se quitan los botones al final de la tabla y se agrega un cuadro de Filtros.
 * @author FSW Indra
 * Marzo 2015
*/

package mx.altec.enlace.servlets;
// Modificacion RMM 20021218 Cierre de archivos,
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConsultaCtasMovilesBean;
import mx.altec.enlace.beans.CuentaInterbancariaBean;
import mx.altec.enlace.beans.CuentaInternacionalBean;
import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.beans.CuentasSantanderBean;
import mx.altec.enlace.beans.ExportAdmCtrlCtaACBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConsultaCtasBO;
import mx.altec.enlace.bo.Convertidor;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.ExportConsultaBO;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailContainer;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceLynxUtils;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.EnlaceLynxConstants;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.LynxAutCanCuentasDTO;
import mx.altec.enlace.utilerias.VinculacionMonitorPlusUtils;
import mx.isban.conector.lynx.dto.RespuestaLynxDTO;
/**
 * clase modificada por jorge olalde para cambios del proyecto
 * M022223 - P022514 Optimizaciones Pyme sobre enlace actual
 * @author Z708764
 *
 */
public class ContCtas_AL extends BaseServlet {

	/** Serial UID **/
	private static final long serialVersionUID = -8314932034396874737L;
	/**  Constante para archivo de ayuda *. */
	private static final String ARCHIVO_AYUDA = "aEDC_ConConfig";
	/** Nombre del parametro contiene el nombre del atributo se sesion donde se almacena el modelo de exportacion. **/
	private static final String PARAM_ATRIBUTO_EXPORT_MODEL = "em";
	/** Tipo de Exportacion utilizada Bean, SubString, Separados. **/
	private static final String PARAM_TIPO_EXPORT = "metodoExportacion";
	/** Nombre del Bean para identificar el objeto con la informacion a exportar. **/
	private static final String NOMBRE_BEAN_EXPORT = "nombreBeanExport";
	/** The Constant NUM_PAGINA. "numPag" */
	private static final String NUM_PAGINA = "numPag";
	/** The Constant RANGO. 100 */
	private static final int RANGO = 100;
	/** The Constant SESSION_BR. "session" */
	private static final String SESSION_BR = "session";
	/** Constante para MENU_PRINCIPAL_STR. "MenuPrincipal" */
	private static final String MENU_PRINCIPAL_STR = "MenuPrincipal";
	/** Constante para ENCABEZADO_STR. "Encabezado" */
	private static final String ENCABEZADO_STR = "Encabezado";
	/** Constante para FECHA_STR. "Fecha"*/
	private static final String FECHA_STR = "Fecha";
	/** Constante para HORA_STR. "Hora" */
	private static final String HORA_STR = "Hora";
	/** Constante para CONT_USER_SRT. "ContUser" */
	private static final String CONT_USER_SRT = "ContUser";
	/** Constante para MSG_ERROR_SRT. "MsgError" */
	private static final String MSG_ERROR_SRT = "MsgError";
	/** Constante para NEW_MENU_STR. "newMenu" */
	private static final String NEW_MENU_STR = "newMenu";
	/** Constante para la cadena "Banco". */
	private static final String BANCO = "Banco";
	/** Constante para la cadena "textabdatobs". */
	private static final String TEXTABDATOBS = "textabdatobs";
	/** Constante para la cadena "textabdatcla". */
	private static final String TEXTABDATCLA = "textabdatcla";
	/** Constante para la cadena "<tr>". */
	private static final String TAG_TR = "<tr>";
	/** Constante para la cadena "&nbsp;</td>". */
	private static final String NBSP_TAG_TD = "&nbsp;</td>";
	/** Constante para la cadena "<td class=". */
	private static final String TAG_TD_CLASS = "<td class=";
	/** Constante para la cadena "</tr>". */
	private static final String TAG_CLOSE_TR = "</tr>";
	/** Constante para la cadena "encabezadosTabla". */
	private static final String ENCBZDS_TBL = "encabezadosTabla";
	/** Constante para la cadena "encabezadosTablaU". */
	private static final String ENCBZDS_TBLU = "encabezadosTablaU";
	/** Constante para la cadena "encabezadosTabla1". */
	private static final String ENCBZDS_TBL1 = "encabezadosTabla1";
	/** Constante para la cadena "Mensaje1". */
	private static final String MSG1 = "Mensaje1";
	/** Constante para la cadena "MensajeU". */
	private static final String MSGU = "MensajeU";
	/** Constante para la cadena "Mensaje2". */
	private static final String MSG2 = "Mensaje2";
	/** Constante para la cadena "boton". */
	private static final String BOTON = "boton";
	/** Constante para la cadena " ></A></td></TR>". */
	private static final String FIN_PAGNCN = " ></A></td></TR>";
	/** Constante para la cadena "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante1();\">Siguientes ". */
	private static final String INI_PAGNCN = "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante1();\">Siguientes ";
	/** Constante para la cadena "***paginacion :". */
	private static final String A_PAGNCN = "***paginacion :";
	/** Constante para la cadena "Nombre_Archivo". */
	private static final String NOM_ARCH = "Nombre_Archivo";
	/** Constante para la cadena "paginacion". */
	private static final String ATR_PAGNCN = "paginacion";
	/** Constante para la cadena "varpaginacion". */
	private static final String VAR_PAGNCN = "varpaginacion";
	/** Constante para la cadena "null". */
	private static final String NULL = "null";
	/** Constante para la cadena "</td>". */
	private static final String TAG_CLOSE_TD = "</td>";
	/** Constante para la cadena "<td class='". */
	private static final String TAG_TD_CLASS2 = "<td class='";

	/** Constante para la cadena "tipoCuenta". */
	private static final String TIPO_CTA = "tipoCuenta";
	/** Constante para la cadena "Autorizaci&oacute;n y Cancelaci&oacute;n ". */
	private static final String AUT_CANC = "Autorizaci&oacute;n y Cancelaci&oacute;n ";
	/** Constante para la cadena "ContCtas_AL?Modulo=2". */
	private static final String CONTCTAS_M2 = "ContCtas_AL?Modulo=2";
	/** Constante para la cadena "Autorizaci&oacute;n y Cancelaci&oacute;n". */
	private static final String AUT_CANC2 = "Autorizaci&oacute;n y Cancelaci&oacute;n";
	/** Constante para la cadena "Administraci&oacute;n y control &gt; Cuentas &gt; Autorizaci&oacute;n y Cancelaci&oacute;n". */
	private static final String UBC_AUT_CANC = "Administraci&oacute;n y control &gt; Cuentas &gt; Autorizaci&oacute;n y Cancelaci&oacute;n";
	/** Constante para la cadena "Administracion y control &gt; Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n". */
	private static final String UBC_AUT_CANC2 = "Administraci&oacute;n y control &gt; Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n";
	/** Constante para la cadena "Administraci&oacute;n y control &gt; Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n". */
	private static final String UBC_AUT_CANC3 = "Administraci&oacute;n y control &gt; Cuentas &gt Autorizaci&oacute;n y Cancelaci&oacute;n";
	/** Lista que contiene los datos de la consulta para realizar la exportaci�n **/
	private static List<ExportAdmCtrlCtaACBean> listaExport;
	/** Constante para titulo de pantalla */
	private static final String TITULO_PANTALLA = "Consulta de Cuentas";
	/** Constante para posicion de menu */
	private static final String POS_MENU = "Administraci&oacute;n y control > Cuentas Relacionadas > Consulta";

	/** Constante para la etiqueta de "s55310h". */
	private static final String S55310H="s55310h";
	/** Constante para la etiqueta "s55320h". */
	private static final String S55320H="s55320h";
	/** Constante para la etiqueta "No existen datos disponibles para su consulta.". */
	private static final String SIN_DATOS="No existen datos disponibles para su consulta.";
	/** Constante para CADENA_VACIA*/
	private static final String CADENA_VACIA = "";
	/** Atributo de sesion para el RB criterio de busqueda. */
    private static final String SESATTR_RB_CRIT_BUSQ = "RBCritBusq";
    /** Atributo de sesion para el texto de criterio de busqueda. */
    private static final String SESATTR_TXT_CRIT_BUSQ = "TxtCritBusq";
    /** Atributo de sesion para el RB tipo de cuenta. */
    private static final String SESATTR_RB_TIPO_CTA = "RBTipoCta";
    /** Leyenda en trace de RBTipoCta. */
    private static final String MSG_TRACE_RB_TIPO_CTA = "] RBTipoCta [";
    /** Leyenda para el mensaje por trace de Registros totales. */
    private static final String MSG_TRACE_REGISTROS = "Registros totales ->";
    /** Path de retorno al modulo 0. */
    private static final String CONT_CTAS_AL_MODULO0 = "ContCtas_AL?Modulo=0";
    /** Parametro de request de registros totales. */
    private static final String REQPARAM_REG_TOTAL = "regTotal";
    /** Parametro de request de next1. */
    private static final String REQPARAM_NEXT1 = "next1";
    /** Parametro de request de prev1. */
    private static final String REQPARAM_PREV1 = "prev1";
    /** Constante para CADENA_NINGUNA*/
	private static final String CADENA_NINGUNA = "NINGUNA";
	/** Constante para CADENA_TOTAL*/
	private static final String CADENA_TOTAL = "total";
	/** Constante para CADENA_TOTAL_PAGINAS*/
	private static final String CADENA_TOTAL_PAGINAS = "totalPaginas";
	/** Constante para CADENA_S26060H*/
	private static final String CADENA_S26060H = "s26060h";
	/** Constante para CADENA_DIA*/
	private static final String CADENA_DIA = "DIA";
	/** Constante para CADENA_MES*/
	private static final String CADENA_MES = "MES";
	/** Constante para CADENA_ANIO*/
	private static final String CADENA_ANIO = "ANIO";
	/** Constante para CADENA_ALTA*/
	private static final String CADENA_ALTA = "Alta";

	/**
	 * Metodo doGet del Servlet
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @throws IOException para el manejo de Excepcion
	 * @throws ServletException para el manejo de Excepcion
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
	throws ServletException, IOException {
		doPost( request, response );
	}

	/**
	 * Metodo DoPost del Servlet
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @throws IOException para el manejo de Excepcion
	 * @throws ServletException para el manejo de Excepcion
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
	throws ServletException, IOException {
		/** para exportacion * **/
        String opcionexportar = request.getParameter("opcionexportar");
        if( "exportar".equals(opcionexportar)){
        	try{
	        ExportConsultaBO instancia=new ExportConsultaBO();
	        instancia.defaultAction(request, response);
	        }catch(IOException e){
	        	despliegaPaginaErrorURL("Ocurrio un error al generar su exportacion.",TITULO_PANTALLA,
	        			POS_MENU,ARCHIVO_AYUDA,null,request,response);
	        }
	     }
        /** Fin de exportacion */
		BaseResource session = (BaseResource) request.getSession().getAttribute (SESSION_BR);
		EIGlobal.mensajePorTrace("ContCtas_AL.java::doPost()",NivelLog.INFO );
		boolean sesionvalida = SesionValida( request, response );
		request.setAttribute(MENU_PRINCIPAL_STR, session.getStrMenu());
		request.setAttribute(NEW_MENU_STR, session.getFuncionesDeMenu());
	
		request.setAttribute(ENCABEZADO_STR, CreaEncabezado("Consulta de Cuentas de otros bancos","Administraci&oacute;n y control &gt; Cuentas &gt; Consulta","s26070h", request));
		
		request.setAttribute(FECHA_STR, ObtenFecha());
		
		request.setAttribute(HORA_STR, ObtenHora());
		EIGlobal.mensajePorTrace("ContCtas_AL.java::5",NivelLog.INFO );
		request.setAttribute(CONT_USER_SRT,ObtenContUser(request));
		EIGlobal.mensajePorTrace("ContCtas_AL.java::5",NivelLog.INFO );

		/*Valida la sesion del usuario*/
		if ( sesionvalida) {
			EIGlobal.mensajePorTrace("ContCtas_AL.java::6",NivelLog.INFO );
			if (request.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER) == null) {
				if (request.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
					request.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
							request.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
				} else if (getFormParameter(request,EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
					request.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
							getFormParameter(request,EnlaceMonitorPlusConstants.DATOS_BROWSER));
				}
			}
			String modulo = (String) request.getParameter("Modulo");
			if ( "".equals(modulo)) {
				modulo = "0";
			}
			EIGlobal.mensajePorTrace("ContCtas_AL.java:::modulo::"+modulo+"::",NivelLog.INFO );
			if ("0".equals(modulo)) {
					procesaConsulta(request,response,modulo);
			}else if ("20".equals(modulo)) {
				cargaInicial(request,response,modulo);
		    }else if("21".equals(modulo)){
		    	request.setAttribute( ENCABEZADO_STR, CreaEncabezado( TITULO_PANTALLA,POS_MENU,"s55310h",request ) );
				String RBTipoCta	 = "RBSantander";
				String Criterio		 = "";
				String CadBuscar	 = "";
				boolean filtrar = false;
				EIGlobal.mensajePorTrace("RBTipoCta ["+RBTipoCta+"] RBCritBusq ["+Criterio+"] TxtCritBusq ["+CadBuscar+"] filtrar ["+filtrar+"] ", NivelLog.DEBUG);
				request.setAttribute(SESATTR_RB_TIPO_CTA,RBTipoCta);
				request.setAttribute(SESATTR_RB_CRIT_BUSQ,"");
				request.setAttribute(SESATTR_TXT_CRIT_BUSQ,"");
				evalTemplate( IEnlace.PRINCIPAL, request, response );
		    } else {
		    	EIGlobal.mensajePorTrace("ContCtas_AL.java:::antes del defaultAction",NivelLog.INFO );
				defaultAction(request,response,modulo);
				EIGlobal.mensajePorTrace("ContCtas_AL.java:::despues del defaultAction",NivelLog.INFO );
			}
		}else {
			request.setAttribute( MSG_ERROR_SRT, IEnlace.MSG_PAG_NO_DISP );
			evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}
	}

	/**
	 * Metodo default para procesar las peticiones de los modulos > 0
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @param modulo de tipo String
	 * @throws IOException para el manejo de Excepcion
	 * @throws ServletException  para el manejo de Excepcion
	 */
	@SuppressWarnings("unchecked")
	public void defaultAction( HttpServletRequest request, HttpServletResponse response,String modulo )
	throws IOException, ServletException {
		EIGlobal.mensajePorTrace( "ContCtas_AL.java::defaultAction() Modulo="+modulo , NivelLog.INFO);

			request.setAttribute( ENCABEZADO_STR, CreaEncabezado( TITULO_PANTALLA,POS_MENU,"s26070h", request ) );
			
			EIGlobal.mensajePorTrace("Entrando a ContCtas_AL >> antes de las validaciones de if", NivelLog.INFO);
			if (("2").equals(modulo)) {  //NRN Nuevo Inicio Q12535
				EIGlobal.mensajePorTrace("Entrando a ContCtas_AL >> InicioConsCtas()", NivelLog.INFO);
				try {
					InicioConsCtas(request, response);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				}
			}else if (("3").equals(modulo))
			{
				EIGlobal.mensajePorTrace("Entrando a ContCtas_AL >> generaTablaConsultar()", NivelLog.INFO);
				try {
					generaTablaConsultar(request, response);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				}
			}else if (("4").equals(modulo)) {
				EIGlobal.mensajePorTrace("Entrando a ContCtas_AL >> generaTablaConsultarRegistrosPorPaginacion()", NivelLog.INFO);
				generaTablaConsultarRegistrosPorPaginacion(request, response);
			}else if (("5").equals(modulo)) {
				EIGlobal.mensajePorTrace("Entrando a ContCtas_AL >> Realizando Alta o Bajas()", NivelLog.INFO);
				EjecutaAltaBajaRechazo(request, response);
			}

		EIGlobal.mensajePorTrace("Entrando a ContCtas_AL >> despues de las validaciones de if", NivelLog.INFO);
	}// fin del del default

	/**
	 * M�todo que Procesa Consulta de Cuentas a traves de filtros por tipo de cuenta, manejando Cuentas
	 * Mismo banco, Interbancarias, Internacionales y m�viles.
	 * @param request request con la informaci�n de los filtros a aplicar
	 * @param response response
	 * @param modulo modulo Indica el tipo de consulta a efectual, se refiere al tipo de cuenta.
	 * @throws IOException exception IO
	 * @throws ServletException exception Servlet
	 */
	public void procesaConsulta( HttpServletRequest request, HttpServletResponse response, String modulo)
			throws IOException, ServletException {

		EIGlobal.mensajePorTrace( "ContCtas_AL.java::procesaConsulta() Modulo="+modulo , NivelLog.INFO);

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

		//boolean sesionvalida = SesionValida( request, response );
		boolean filtrar = false;

		String modTux		 = "";
		String RBTipoCta	 = "";
		String BitaString	 = " ";
		String Criterio		 = "";
		String CadBuscar	 = "";


			RBTipoCta=(String)request.getParameter(SESATTR_RB_TIPO_CTA);
			Criterio=(request.getParameter(SESATTR_RB_CRIT_BUSQ)!=null) ? (String)request.getParameter(SESATTR_RB_CRIT_BUSQ) : "";
			CadBuscar=(request.getParameter(SESATTR_TXT_CRIT_BUSQ)!=null) ? (String)request.getParameter(SESATTR_TXT_CRIT_BUSQ) : "";
			filtrar=( "".equals(Criterio) || "".equals(CadBuscar) ) ? false : true;
			EIGlobal.mensajePorTrace("RBTipoCta ["+RBTipoCta+"] RBCritBusq ["+Criterio+"] TxtCritBusq ["+CadBuscar+"] filtrar ["+filtrar+"] ", NivelLog.DEBUG);
			request.setAttribute(SESATTR_RB_TIPO_CTA,RBTipoCta);
			request.setAttribute(SESATTR_RB_CRIT_BUSQ,Criterio);
			request.setAttribute(SESATTR_TXT_CRIT_BUSQ,CadBuscar);

			/* Se obtiene tipoCta del Filtro, moduloTux, BitaString dependiento del Filtro*/
			if ("RBSantander".equals(RBTipoCta)) {
				modTux=IEnlace.MMant_gral_ctas;
				BitaString=BitaConstants.EA_CUENTAS_CONSULTA_ENTRA;
				EIGlobal.mensajePorTrace("modTux ["+modTux+"] BitaString ["+BitaString+MSG_TRACE_RB_TIPO_CTA+RBTipoCta+"]", NivelLog.DEBUG);
			}else if ("RBOtros".equals(RBTipoCta)) {
				modTux=IEnlace.MDep_Inter_Abono;
				BitaString=BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_OTROS_BANCOS;
				EIGlobal.mensajePorTrace("modTux ["+modTux+"] BitaString ["+BitaString+MSG_TRACE_RB_TIPO_CTA+RBTipoCta+"]", NivelLog.DEBUG);
			}else if ("RBExtranjeros".equals(RBTipoCta)) {
				modTux=IEnlace.MAbono_TI;
				BitaString=BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_CUENTAS_INTER;
				EIGlobal.mensajePorTrace("modTux ["+modTux+"] BitaString ["+BitaString+MSG_TRACE_RB_TIPO_CTA+RBTipoCta+"]", NivelLog.DEBUG);
			}else if ("RBMovil".equals(RBTipoCta)) {
				BitaString=BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_NUMERO_MOVIL;
				EIGlobal.mensajePorTrace("modTux [Moviles] BitaString ["+BitaString+MSG_TRACE_RB_TIPO_CTA+RBTipoCta+"]", NivelLog.DEBUG);
				/** Cuentas Moviles va por un flujo alterno **/
			}else {
				EIGlobal.mensajePorTrace("RBTipoCta vacio en el request.", NivelLog.DEBUG);
				modTux=IEnlace.MMant_gral_ctas;
				BitaString=BitaConstants.EA_CUENTAS_CONSULTA_ENTRA;
			}
			//Dependiendo del tipo RBTipoCta seleccionado en los filtros se carga al array y se va al metodo para pintar valores.

			//Se bitacoriza el acceso
			if (("ON").equals(Global.USAR_BITACORAS.trim())){
				try {
					BitaHelper bh = new BitaHelperImpl(request, session, sess);
					if (request.getParameter(BitaConstants.FLUJO)!=null){
						bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
					}else{
						bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					}
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaString);
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.ERROR);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.ERROR);
				}
			}
			int totalRegistros=0;
			if ( "RBSantander".equals(RBTipoCta) ) {
				totalRegistros = TotalSantander(request);
				if(totalRegistros>0){
					consultaSantander(request, response, totalRegistros);
				}else{
					despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,null,request,response);
				}
			}else if ( "RBOtros".equals(RBTipoCta) ){
				totalRegistros= TotalInterbancarias(request);
				if(totalRegistros>0){
					consultaOtrosBancos(request,response, totalRegistros);
				}else{
					despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,null,request,response);
				}
			}else if ( "RBExtranjeros".equals(RBTipoCta) ) {
				totalRegistros = TotalInternacionales(request);
				if(totalRegistros >0){
					consultaInternacional(request,response,totalRegistros);
				}else{
					despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,null,request,response);
				}
			}else if ("RBMovil".equals(RBTipoCta)){
				totalRegistros = TotalMoviles(request);
				if(totalRegistros>0){
					procesaMovil(request,response,totalRegistros);
				}else{
					despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,null,request,response);
				}
			}else {
				evalTemplate( IEnlace.PRINCIPAL, request, response );
			}

	}
	/**
	 * M�todo que Procesa Consulta de Cuentas a traves de filtros por tipo de cuenta, manejando Cuentas
	 * Mismo banco, Interbancarias, Internacionales y m�viles.
	 * @param request request con la informaci�n de los filtros a aplicar
	 * @param response response
	 * @param modulo modulo Indica el tipo de consulta a efectual, se refiere al tipo de cuenta.
	 * @throws IOException exception IO
	 * @throws ServletException exception Servlet
	 */
	public void cargaInicial( HttpServletRequest request, HttpServletResponse response, String modulo)
			throws IOException, ServletException {

		EIGlobal.mensajePorTrace( "ContCtas_AL.java::procesaConsulta() Modulo="+modulo , NivelLog.INFO);

		boolean filtrar = false;
		String RBTipoCta	 = "";
		String Criterio		 = "";
		String CadBuscar	 = "";
		request.setAttribute( ENCABEZADO_STR, CreaEncabezado( TITULO_PANTALLA,POS_MENU,"s55310h",request ) );
		RBTipoCta=(String)request.getParameter(SESATTR_RB_TIPO_CTA);
		EIGlobal.mensajePorTrace("RBTipoCta ["+RBTipoCta+"] RBCritBusq ["+Criterio+"] TxtCritBusq ["+CadBuscar+"] filtrar ["+filtrar+"] ", NivelLog.DEBUG);

		request.setAttribute(SESATTR_RB_TIPO_CTA,RBTipoCta);
		request.setAttribute(SESATTR_RB_CRIT_BUSQ,"");
		request.setAttribute(SESATTR_TXT_CRIT_BUSQ,"");
		evalTemplate( IEnlace.PRINCIPAL, request, response );

	}

 	/**
      * Metodo para consultar las cuentas de Otros Bancos Nacionales, esto de acuerdo a los
      * filtros indicados por el cliente
      * @param request de tipo HttpServletRequest
      * @param response de tipo HttpServletResponse
      * @param totalRegistros numero de registros
      * @throws ServletException para manejo de excepciones
      * @throws IOException para manejo de excepciones
      * @return int
      **/
     public int consultaOtrosBancos(HttpServletRequest request, HttpServletResponse response, int totalRegistros) throws ServletException, IOException {

    	 EIGlobal.mensajePorTrace("ContCtas_AL.java::consultaOtrosBancos()",NivelLog.INFO);
    	 List<CuentaInterbancariaBean> ctasInterbancariasList = new ArrayList<CuentaInterbancariaBean>();

    	 String reqNumPag = request.getParameter(NUM_PAGINA);
    	 final int numPag = (reqNumPag!=null && !(NULL.equalsIgnoreCase(reqNumPag) || "".equals(reqNumPag)))
						 			? Integer.parseInt(request.getParameter(NUM_PAGINA))
						 					: 1;
		int totalPaginas = 1;

		boolean banderapaginacion=true;
	
			ctasInterbancariasList = llenaBeanYConsultaCtaInterbancaria(request,numPag);

   			if(ctasInterbancariasList != null && !ctasInterbancariasList.isEmpty()){
   				request.setAttribute("ctasInterbancariasList", ctasInterbancariasList);
//   				regTotal = ctasInterbancariasList.get(0).getTotalRegistro();
   				EIGlobal.mensajePorTrace(MSG_TRACE_REGISTROS + totalRegistros, EIGlobal.NivelLog.INFO);
   			}else{
   				despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA, CONT_CTAS_AL_MODULO0,request,response);
   				return -1;
   			}
   			if(totalRegistros>RANGO){
   				totalPaginas = totalRegistros/RANGO;
   				if(totalRegistros%RANGO > 0){
   					totalPaginas++;
   				}
   			}
   			int prev1=0,next1=0;
            boolean estaPrev1 = (request.getParameter(REQPARAM_PREV1)!=null && !"".equals(request.getParameter(REQPARAM_PREV1)) && !NULL.equals(request.getParameter(REQPARAM_PREV1)));
            boolean estaNext1 = (request.getParameter(REQPARAM_NEXT1)!=null && !"".equals(request.getParameter(REQPARAM_NEXT1)) && !NULL.equals(request.getParameter(REQPARAM_NEXT1)));
   			if  ( estaPrev1 && estaNext1  &&
   					(request.getParameter(REQPARAM_REG_TOTAL)!=null && !"".equals(request.getParameter(CADENA_TOTAL))  && !NULL.equals(request.getParameter(REQPARAM_REG_TOTAL)))) {
   				prev1 =  Integer.parseInt(request.getParameter(REQPARAM_PREV1));
   				next1 =  Integer.parseInt(request.getParameter(REQPARAM_NEXT1));
   				totalRegistros = Integer.parseInt(request.getParameter(REQPARAM_REG_TOTAL));
   				EIGlobal.mensajePorTrace("consultaOtrosBancos::no son nulos", EIGlobal.NivelLog.INFO);
   				EIGlobal.mensajePorTrace("consultaOtrosBancos::IF::totalPaginas::" + totalPaginas+"::prev1::"+prev1+"::next1:"+next1+"::regTotal::"+totalRegistros, EIGlobal.NivelLog.INFO);
   				banderapaginacion=false;
   			}
   			if(banderapaginacion){
   				prev1=0;
   				if (totalRegistros>Global.NUM_REGISTROS_PAGINA){
   					next1=Global.NUM_REGISTROS_PAGINA;
   				} else {
   					next1=totalRegistros;
   				}
   			}
   			request.setAttribute(CADENA_TOTAL_PAGINAS,totalPaginas);
   			request.setAttribute(NUM_PAGINA,numPag);
   			EIGlobal.mensajePorTrace("consultaOtrosBancos::totalPaginas::" + totalPaginas+"::numPag::"+numPag+"::prev1::"+prev1+"::next1:"+next1+"::regTotal::"+totalRegistros, EIGlobal.NivelLog.INFO);
   			consultaInterbancaria(request, response, ctasInterbancariasList, totalPaginas, prev1, next1, totalRegistros, numPag, totalPaginas);

    	 return 1;
	}

     /**
      * Metodo para consultar las cuentas de Otros Bancos Nacionales, esto de acuerdo a los
      * filtros indicados por el cliente
      * @param request de tipo HttpServletRequest
      * @param response de tipo HttpServletResponse
      * @param arrayCuentas de tipo String
      * @param total de tipo int
      * @param prev de tipo int
      * @param next de tipo int
      * @return int
      * @throws ServletException para manejo de excepciones
      * @throws IOException para manejo de excepciones
      **/
     public int consultaSantander(HttpServletRequest request, HttpServletResponse response, int totalRegistros) throws ServletException, IOException {
    	 HttpSession sess = request.getSession ();
    	 BaseResource session = (BaseResource) sess.getAttribute (SESSION_BR);
    	 EIGlobal.mensajePorTrace("ContCtas_AL.java::consultaSantander()",NivelLog.INFO);
    	 List<CuentasSantanderBean> ctasSantanderList = new ArrayList<CuentasSantanderBean>();

    	 String reqNumPag = request.getParameter(NUM_PAGINA);
    	 final int numPag = (reqNumPag!=null && !(NULL.equalsIgnoreCase(reqNumPag) || "".equals(reqNumPag))) ? Integer.parseInt(request.getParameter(NUM_PAGINA)): 1;
    	 int totalPaginas = 1;
    	 boolean banderapaginacion=true;
    	// if ( SesionValida( request, response ) ) {
    		 ctasSantanderList = llenaBeanYConsultaCtaSantander(request,numPag);
    		 if(ctasSantanderList != null && !ctasSantanderList.isEmpty()){
    			 request.setAttribute("ctasSantanderList", ctasSantanderList);
    			 EIGlobal.mensajePorTrace(MSG_TRACE_REGISTROS + totalRegistros, EIGlobal.NivelLog.INFO);
    		 }else{
				 despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA, CONT_CTAS_AL_MODULO0,request,response);
   				return -1;
   			}
   			if(totalRegistros>RANGO){
   				totalPaginas = totalRegistros/RANGO;
   				if(totalRegistros%RANGO > 0){
   					totalPaginas++;
   				}
   			}
   			int prev1=0,next1=0;
   			if  ((request.getParameter(REQPARAM_PREV1)!=null && !"".equals(request.getParameter(REQPARAM_PREV1)) && !NULL.equals(request.getParameter(REQPARAM_PREV1))) &&
   					(request.getParameter(REQPARAM_NEXT1)!=null && !"".equals(request.getParameter(REQPARAM_NEXT1)) && !NULL.equals(request.getParameter(REQPARAM_NEXT1)) ) &&
   					(request.getParameter(REQPARAM_REG_TOTAL)!=null && !"".equals(request.getParameter(CADENA_TOTAL))  && !NULL.equals(request.getParameter(REQPARAM_REG_TOTAL)))) {
   				prev1 =  Integer.parseInt(request.getParameter(REQPARAM_PREV1));
   				next1 =  Integer.parseInt(request.getParameter(REQPARAM_NEXT1));
   				totalRegistros = Integer.parseInt(request.getParameter(REQPARAM_REG_TOTAL));
   				EIGlobal.mensajePorTrace("consultaOtrosBancos::no son nulos", EIGlobal.NivelLog.INFO);
   				EIGlobal.mensajePorTrace("consultaOtrosBancos::IF::totalPaginas::" + totalPaginas+"::prev1::"+prev1+"::next1:"+next1+"::regTotal::"+totalRegistros, EIGlobal.NivelLog.INFO);
   				banderapaginacion=false;
   			}
   			if(banderapaginacion){
   				prev1=0;
   				if (totalRegistros>Global.NUM_REGISTROS_PAGINA){
   					next1=Global.NUM_REGISTROS_PAGINA;
   				} else {
   					next1=totalRegistros;
   				}
   			}
   			request.setAttribute(CADENA_TOTAL_PAGINAS,totalPaginas);
   			request.setAttribute(NUM_PAGINA,numPag);
   			EIGlobal.mensajePorTrace("consultaOtrosBancos::totalPaginas::" + totalPaginas+"::numPag::"+numPag+"::prev1::"+prev1+"::next1:"+next1+"::regTotal::"+totalRegistros, EIGlobal.NivelLog.INFO);
   			/**inicio bitacora**/
   			if (("ON").equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
   				try{
   					BitaHelper bh = new BitaHelperImpl(request, session, sess);
   					/**comienza implementacion bitacora tct_bitacora**/
   					if(getFormParameter(request,BitaConstants.FLUJO) != null){
   						bh.incrementaFolioFlujo((String)getFormParameter(request,BitaConstants.FLUJO));
   					}else{
   						bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
   					}
   					BitaTCTBean beanTCT = new BitaTCTBean ();
   					beanTCT = bh.llenarBeanTCT(beanTCT);
   					beanTCT.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
   					beanTCT.setTipoOperacion("ECBE");
   					if (session.getContractNumber() != null) {
   						beanTCT.setNumCuenta(session.getContractNumber().trim());
   					}
   					if (session.getUserID8() != null) {
   						beanTCT.setUsuario(session.getUserID8().trim());
   						beanTCT.setOperador(session.getUserID8().trim());
   					}
   					if(ctasSantanderList.get(0).getTramaSalida() != null){
   						if(("OK").equals(ctasSantanderList.get(0).getTramaSalida().substring(0,2))){
   							beanTCT.setCodError("ECBE0000");
   						}else if(ctasSantanderList.get(0).getTramaSalida().length() > 8){
   							beanTCT.setCodError(ctasSantanderList.get(0).getTramaSalida().substring(0,8));
   						}else{
   							beanTCT.setCodError(ctasSantanderList.get(0).getTramaSalida().trim());
   						}
   					}
   					BitaHandler.getInstance().insertBitaTCT(beanTCT);
   				
   				} catch (SQLException e) {
   					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
   				} catch (Exception e) {
   					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
   				}
   			}
   			/**fin bitacora**/
   			consultaSantanderArma(request, response, ctasSantanderList, totalPaginas, prev1, next1, totalRegistros, numPag, totalPaginas);

    	 return 1;
	}

 	/**
     * Metodo para consultar las cuentas M�viles, esto de acuerdo a los
     * filtros indicados por el cliente
 	 * @param request de tipo HttpServletRequest
 	 * @param response de tipo HttpServletResponse
 	 * @param totalRegistros de int
 	 * @return int
 	 * @throws IOException para menejo de excepciones
 	 * @throws ServletException para manejo de excepciones
 	 */
 	public int procesaMovil(HttpServletRequest request, HttpServletResponse response, int totalRegistros) throws IOException, ServletException {
 		final HttpSession sess = request.getSession();
 		final String descErr = request.getParameter("descErr");

 		String reqNumPag = request.getParameter(NUM_PAGINA);
 		final int numPag = (reqNumPag!=null && !(NULL.equalsIgnoreCase(reqNumPag) || "".equals(reqNumPag)))
						 			? Integer.parseInt(request.getParameter(NUM_PAGINA))
						 					: 1;
		EIGlobal.mensajePorTrace("numPag ->" + numPag, EIGlobal.NivelLog.INFO);
 		List<CuentaMovilBean> ctasList = new ArrayList<CuentaMovilBean>();

 		int totalPaginas = 1;


 			ctasList = llenaBeanYConsultaCtaMovil(request, descErr, numPag);
   			if(ctasList != null && !ctasList.isEmpty()){
   				request.setAttribute("ctasList", ctasList);
   				EIGlobal.mensajePorTrace(MSG_TRACE_REGISTROS + totalRegistros, EIGlobal.NivelLog.INFO);
   			}else{
   				despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA, CONT_CTAS_AL_MODULO0,request,response);
   				return -1;
   			}
   			if(totalRegistros>RANGO){
   				totalPaginas = totalRegistros/RANGO;
   				if(totalRegistros%RANGO > 0){
   					totalPaginas++;
   				}
   			}
   			EIGlobal.mensajePorTrace("Paginas totalPaginas ->" + totalPaginas, EIGlobal.NivelLog.INFO);
   			int total1=0,prev1=0,next1=0;
   			boolean banderapaginacion=true;
   			if(!"".equals(request.getParameter(REQPARAM_PREV1))
   						&&!"".equals(request.getParameter(REQPARAM_NEXT1))
   							&&!"".equals(request.getParameter(REQPARAM_REG_TOTAL))
   								&&!NULL.equals(request.getParameter(REQPARAM_PREV1))
   									&& !NULL.equals(request.getParameter(REQPARAM_NEXT1))
   										&& !NULL.equals(request.getParameter(REQPARAM_REG_TOTAL))){
   				EIGlobal.mensajePorTrace("Paginas prev1 ->" + request.getParameter(REQPARAM_PREV1), EIGlobal.NivelLog.INFO);
   				EIGlobal.mensajePorTrace("Paginas next1 ->" + request.getParameter(REQPARAM_NEXT1), EIGlobal.NivelLog.INFO);
   				EIGlobal.mensajePorTrace("Paginas regTotal ->" + request.getParameter(REQPARAM_REG_TOTAL), EIGlobal.NivelLog.INFO);
   				EIGlobal.mensajePorTrace("***Paginas -> entro validacion", EIGlobal.NivelLog.INFO);
   				prev1 =  Integer.parseInt(request.getParameter(REQPARAM_PREV1));
   				next1 =  Integer.parseInt(request.getParameter(REQPARAM_NEXT1));
   				banderapaginacion=false;
   			}

   			if(banderapaginacion){
   				prev1=0;
   				if (totalRegistros>Global.NUM_REGISTROS_PAGINA){
   					next1=Global.NUM_REGISTROS_PAGINA;
   				} else {
   					next1=totalRegistros;
   				}
   			}
   			request.setAttribute(CADENA_TOTAL_PAGINAS,totalPaginas);
   			request.setAttribute(NUM_PAGINA,numPag);
   			request.setAttribute("descErrC", descErr);
   			insertaBit(request, sess);
   			/**  Instancia para la bitacora admin *. */
   			EIGlobal.mensajePorTrace ("procesaMovil - Valores Paginaci�n: total1["+total1+"] prev1["+prev1+"] next1["+next1+"]", EIGlobal.NivelLog.DEBUG);
   			EIGlobal.mensajePorTrace ("procesaMovil - Valores Paginaci�n: ctasList.get(0).getTotalRegistros()["+ctasList.get(0).getTotalRegistros()+"]", EIGlobal.NivelLog.DEBUG);
   			EIGlobal.mensajePorTrace ("procesaMovil - Valores Paginaci�n: ctasList.size()["+ctasList.size()+"]", EIGlobal.NivelLog.DEBUG);
   			consultaMovil(request, response, ctasList, totalPaginas, numPag, 0, totalRegistros, prev1, next1, numPag, totalPaginas);

 		return 1;
 	}
 	/**
 	 * Metodo para insertar en bitacora
 	 * en el metodo de procesaMovil, se crea por Sonar
 	 * @since 23/04/2015
 	 * @author FSW-Indra
 	 * @param request HttpServletRequest informacion
 	 * @param sess componente de sesion
 	 */
    public void insertaBit(HttpServletRequest request, HttpSession sess){
    	if ("ON".equals(Global.USAR_BITACORAS.trim())){
			try {
			BitaHelper bh = new BitaHelperImpl(request, session, sess);

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_NUMERO_MOVIL);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			bt.setNombreArchivo(session.getUserID8()+".ambcie");
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("CtaMoviles - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CtaMoviles - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
    }
    /**
     * Metodo para llenar el bean y despues consultar la cta movil
     * @since 04/06/2015
     * @author z708764-Jorge Olalde
     * @param request informacion de pantalla
     * @param descErr descripcion del error
     * @param numPag numero de pagina
     * @return List<CuentaMovilBean> lista con las cuentas moviles
     */
    public List<CuentaMovilBean> llenaBeanYConsultaCtaMovil(HttpServletRequest request, String descErr, int numPag){
    	final BaseResource session = (BaseResource) request.getSession().getAttribute(SESSION_BR);
    	ConsultaCtasMovilesBean consCtaMovilBean = new ConsultaCtasMovilesBean();
    	final ConsultaCtasBO consCtaBO = new ConsultaCtasBO();
    	consCtaMovilBean.setDescErr(descErr);
    	consCtaMovilBean.setContrato(session.getContractNumber());
		EIGlobal.mensajePorTrace("Descripcion del Error ->" + consCtaMovilBean.getDescErr(),
				EIGlobal.NivelLog.INFO);
		consCtaMovilBean.setRegIni((numPag-1)*RANGO);
		consCtaMovilBean.setRegFin(numPag*RANGO);
			request.setAttribute(ENCABEZADO_STR, CreaEncabezado("Cuentas M&oacute;viles",
				"Cuentas &gt; Consulta &gt; Cuentas M&oacute;viles",ARCHIVO_AYUDA,request));
		consCtaMovilBean.setCriterio((request.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
		consCtaMovilBean.setTextoBuscar((request.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
		//Obten Cuentas de BD
		return consCtaBO.consultaCtasMovil(consCtaMovilBean);
    }

    /**
     * Metodo para llenar el bean y despues consultar la cta movil
     * @since 04/06/2015
     * @author z708764-Jorge Olalde
     * @param request informacion de pantalla
     * @param descErr descripcion del error
     * @param numPag numero de pagina
     * @return List<CuentaInterbancariaBean> lista con las cuentas Interbancarias
     */
    public List<CuentaInterbancariaBean> llenaBeanYConsultaCtaInterbancaria(HttpServletRequest request, int numPag){
    	final BaseResource session = (BaseResource) request.getSession().getAttribute(SESSION_BR);
    	CuentaInterbancariaBean consCtaBean = new CuentaInterbancariaBean();
    	final ConsultaCtasBO consCtaBO = new ConsultaCtasBO();
		consCtaBean.setContrato(session.getContractNumber());
        consCtaBean.setRegIni((numPag-1)*RANGO);
		consCtaBean.setRegFin(numPag*RANGO);

		request.setAttribute(ENCABEZADO_STR, CreaEncabezado("Cuentas Interbancarias",
				"Cuentas &gt; Consulta &gt; Cuentas Interbancarias",ARCHIVO_AYUDA,request));
		consCtaBean.setCriterio((request.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
		consCtaBean.setTextoBuscar((request.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
		//Obten Cuentas de BD
		return consCtaBO.consultaCtasInterbancarias(consCtaBean);
    }

    /**
     * Metodo para llenar el bean y despues consultar la cta movil
     * @since 04/06/2015
     * @author z708764-Jorge Olalde
     * @param request informacion de pantalla
     * @param descErr descripcion del error
     * @param numPag numero de pagina
     * @return List<CuentasSantanderBean> lista con las cuentas Mismo Banco (Santander)
     */
    public List<CuentasSantanderBean> llenaBeanYConsultaCtaSantander(HttpServletRequest request, int numPag){
    	HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute (SESSION_BR);
    	CuentasSantanderBean consCtaBean = new CuentasSantanderBean();
    	final ConsultaCtasBO consCtaBO = new ConsultaCtasBO();
		consCtaBean.setContrato(session.getContractNumber());
        consCtaBean.setRegIni((numPag-1)*RANGO);
		consCtaBean.setRegFin(numPag*RANGO);
		consCtaBean.setUSUARIO(session.getUserID8());

		request.setAttribute(ENCABEZADO_STR, CreaEncabezado("Cuentas Santander",
				"Cuentas &gt; Consulta &gt; Cuentas Santander",ARCHIVO_AYUDA,request));
		consCtaBean.setCriterio((request.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
		consCtaBean.setTextoBuscar((request.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
		//Obten Cuentas de BD
		return consCtaBO.consultaCtasSantander(consCtaBean);
    }
    /**
     * Metodo para llenar el bean y despues consultar la cta movil
     * @since 04/06/2015
     * @author z708764-Jorge Olalde
     * @param request informacion de pantalla
     * @param descErr descripcion del error
     * @param numPag numero de pagina
     * @return List<CuentaInternacionalBean> lista con las cuentas Internacionales
     */
    public List<CuentaInternacionalBean> llenaBeanYConsultaCtaInternacionales(HttpServletRequest request, int numPag){
    	final BaseResource session = (BaseResource) request.getSession().getAttribute(SESSION_BR);
    	CuentaInternacionalBean consCtaBean = new CuentaInternacionalBean();
    	final ConsultaCtasBO consCtaBO = new ConsultaCtasBO();
		consCtaBean.setContrato(session.getContractNumber());
        consCtaBean.setRegIni((numPag-1)*RANGO);
		consCtaBean.setRegFin(numPag*RANGO);

		request.setAttribute(ENCABEZADO_STR, CreaEncabezado("Cuentas Internacionales",
				"Cuentas &gt; Consulta &gt; Cuentas Internacionales",ARCHIVO_AYUDA,request));
		consCtaBean.setCriterio((request.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
		consCtaBean.setTextoBuscar((request.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? request.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
		//Obten Cuentas de BD
		return consCtaBO.consultaCtasInternacionales(consCtaBean);
    }
    /**
     * Metodo para prepar las cuentas M�viles a presentar en pantalla, esto de acuerdo a los
     * filtros indicados por el cliente
     * @param request de tipo HttpServletRequest
     * @param response de tipo HttpServletResponse
     * @param ctasList de tipo List
     * @param total de tipo int
     * @param prev de tipo int
     * @param next de tipo int
     * @param regTotal de tipo int
     * @param prev1 de tipo int
     * @param next1 de tipo int
     * @param numPag de tipo int
     * @param totalReg de tipo int
     * @return int
     * @throws ServletException para menejo de excepciones
     * @throws IOException para menejo de excepciones
     */
    public int consultaMovil(HttpServletRequest request,
			HttpServletResponse response, List<CuentaMovilBean> ctasList,
			int total, int prev, int next, int regTotal, int prev1, int next1, int numPag, int totalReg)
			throws ServletException, IOException {
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::consultaMovil()", EIGlobal.NivelLog.INFO);

 		String pathFileDownload = "";
 		String tablaDetalle	 = "";
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::ctasList.size())" + ctasList.size(), EIGlobal.NivelLog.INFO);
 		tablaDetalle = armaTablaDetalle(ctasList);
 		String encabezaMenuU = "<td class=tittabdat align=center>N&uacute;mero M&oacute;vil</td>";
 		String encabezaMenu1 = "<td class=tittabdat align=center>Nombre del Titular</td>"+
					 		"<td class=tittabdat align=center>Tipo de Cuenta</td>" +
					 		"<td class=tittabdat align=center>Banco</td>";
 		request = setAtributosRequest(request, tablaDetalle, encabezaMenuU, encabezaMenu1);
 		request.setAttribute( ENCABEZADO_STR, CreaEncabezado( TITULO_PANTALLA,POS_MENU, CADENA_S26060H, request ) );

 		request.setAttribute("pathFileDownload", pathFileDownload);
 		request.setAttribute(NOM_ARCH, pathFileDownload);
 		request.setAttribute(CADENA_TOTAL, String.valueOf(total));
 		request.setAttribute("prev", String.valueOf(prev));
 		request.setAttribute("next", String.valueOf(next));
 		request.setAttribute(REQPARAM_REG_TOTAL, regTotal);
 		request.setAttribute(REQPARAM_PREV1, prev1);
 		request.setAttribute(REQPARAM_NEXT1, next1);
 		request.setAttribute(ATR_PAGNCN, metodoArmaPaginacion(next1, prev1, regTotal, numPag, totalReg));
 		request.setAttribute(VAR_PAGNCN, Global.NUM_REGISTROS_PAGINA);
 		//***********************************************
 		evalTemplate( IEnlace.PRINCIPAL, request, response );
 		return 1;
 	}

    /**
     * Metodo para prepar las cuentas Interbancarias a presentar en pantalla, esto de acuerdo a los
     * filtros indicados por el cliente
     * @param request de tipo HttpServletRequest
     * @param response de tipo HttpServletResponse
     * @param ctasList de tipo List
     * @param total total de tipo int
     * @param prev1 de tipo int
     * @param next1 de tipo int
     * @param regTotal de tipo int
     * @param numPag de tipo int
     * @param totalPag de tipo int
     * @return int
     * @throws ServletException para menejo de excepciones
     * @throws IOException para menejo de excepciones
     */
    public int consultaInterbancaria(HttpServletRequest request,
			HttpServletResponse response, List<CuentaInterbancariaBean> ctasList,
			int total, int prev1, int next1, int regTotal, int numPag, int totalPag)
			throws ServletException, IOException {
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::consultaInterbancaria()", EIGlobal.NivelLog.INFO);
 		String pathFileDownload = "";
 		String tablaDetalle	 = "";
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::ctasList.size())" + ctasList.size(), EIGlobal.NivelLog.INFO);
 		tablaDetalle = armaTablaDetalleInterbancarias(ctasList);
 		String encabezaMenuU = "<td align='center' class=tittabdat>Numero de Cuenta</td>";
 		String encabezaMenu1 =	"<td align='center' class=tittabdat>Descripci&oacute;n</td>"+
    	 "<td align='center' class=tittabdat>Banco</td>"+
    	 "<td align='center' class=tittabdat>Plaza</td>"+
    	 "<td align='center' class=tittabdat>Sucursal</td>"+
    	 "<td align='center' class=tittabdat>Divisa</td>";
 		request = setAtributosRequest(request, tablaDetalle, encabezaMenuU, encabezaMenu1);
 		request.setAttribute( ENCABEZADO_STR, CreaEncabezado( TITULO_PANTALLA,POS_MENU, CADENA_S26060H, request ) );
 		request.setAttribute("pathFileDownload", pathFileDownload);
 		request.setAttribute(NOM_ARCH, pathFileDownload);
 		request.setAttribute(CADENA_TOTAL, String.valueOf(total));
 		request.setAttribute(REQPARAM_PREV1, String.valueOf(prev1));
 		request.setAttribute(REQPARAM_NEXT1, String.valueOf(next1));
 		request.setAttribute(REQPARAM_REG_TOTAL, regTotal);
 		request.setAttribute(ATR_PAGNCN, metodoArmaPaginacion(next1, prev1, regTotal, numPag, totalPag));
 		request.setAttribute(VAR_PAGNCN, Global.NUM_REGISTROS_PAGINA);
 		//***********************************************
 		evalTemplate( IEnlace.PRINCIPAL, request, response );
 		return 1;
 	}

    /**
     * Metodo para prepar las cuentas mismo banco (Santander) a presentar en pantalla, esto de acuerdo a los
     * filtros indicados por el cliente
     * @param request de tipo HttpServletRequest
     * @param response de tipo HttpServletResponse
     * @param ctasList de tipo List
     * @param total total de tipo int
     * @param prev1 de tipo int
     * @param next1 de tipo int
     * @param regTotal de tipo int
     * @param numPag de tipo int
     * @param totalPag de tipo int
     * @return int
     * @throws ServletException para menejo de excepciones
     * @throws IOException para menejo de excepciones
     */
    public int consultaSantanderArma(HttpServletRequest request,
			HttpServletResponse response, List<CuentasSantanderBean> ctasList,
			int total, int prev1, int next1, int regTotal, int numPag, int totalPag)
			throws ServletException, IOException {
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::consultaSantanderArma()", EIGlobal.NivelLog.INFO);
 		String pathFileDownload = "";
 		String tablaDetalle	 = "";
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::ctasList.size())" + ctasList.size(), EIGlobal.NivelLog.INFO);
 		tablaDetalle = armaTablaDetalleSantander(ctasList);
		String encabezaMenuU= "<TD class=tittabdat align=\"center\">Cuenta</TD>";
		String encabezaMenu1= "<TD class=tittabdat align=\"center\">Propietario</TD>"+
						"<TD class=tittabdat align=\"center\">Tipo</TD>"+
						"<TD class=tittabdat align=\"center\">Clave&nbsp;Bancaria&nbsp;Estandarizada</TD>";
 		request = setAtributosRequest(request, tablaDetalle, encabezaMenuU, encabezaMenu1);
 		request.setAttribute( ENCABEZADO_STR, CreaEncabezado( TITULO_PANTALLA,POS_MENU, CADENA_S26060H, request ) );
 		request.setAttribute("pathFileDownload", pathFileDownload);
 		request.setAttribute(NOM_ARCH, pathFileDownload);
 		request.setAttribute(CADENA_TOTAL, String.valueOf(total));
 		request.setAttribute(REQPARAM_PREV1, String.valueOf(prev1));
 		request.setAttribute(REQPARAM_NEXT1, String.valueOf(next1));
 		request.setAttribute(REQPARAM_REG_TOTAL, regTotal);
 		request.setAttribute(ATR_PAGNCN, metodoArmaPaginacion(next1, prev1, regTotal, numPag, totalPag));
 		request.setAttribute(VAR_PAGNCN, Global.NUM_REGISTROS_PAGINA);
 		evalTemplate( IEnlace.PRINCIPAL, request, response );
 		return 1;
 	}

    /**
     * Metodo para prepar las cuentas Internacionales a presentar en pantalla, esto de acuerdo a los
     * filtros indicados por el cliente
     * @param request de tipo HttpServletRequest
     * @param response de tipo HttpServletResponse
     * @param ctasList de tipo List
     * @param total total de tipo int
     * @param prev1 de tipo int
     * @param next1 de tipo int
     * @param regTotal de tipo int
     * @param numPag de tipo int
     * @param totalPag de tipo int
     * @return int
     * @throws ServletException para menejo de excepciones
     * @throws IOException para menejo de excepciones
     */
    public int consultaInternacionales(HttpServletRequest request,
			HttpServletResponse response, List<CuentaInternacionalBean> ctasList,
			int total, int prev1, int next1, int regTotal, int numPag, int totalPag)
			throws ServletException, IOException {
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::consultaInternacionales()", EIGlobal.NivelLog.INFO);

 		String pathFileDownload = "";
 		String tablaDetalle	 = "";
 		EIGlobal.mensajePorTrace ("ConCtas_AL.java::ctasList.size())" + ctasList.size(), EIGlobal.NivelLog.INFO);
 		tablaDetalle = armaTablaDetalleInternacionales(ctasList);
 		String encabezaMenuU =  "<td class=tittabdat align=center>Numero de Cuenta</td>";
 		String encabezaMenu1 =  "<td class=tittabdat align=center>Titular</td>"+
					    	 "<td class=tittabdat align=center>Banco</td>" +
					    	 "<td class=tittabdat align=center>Pa&iacute;s</td>" +
					    	 "<td class=tittabdat align=center>Ciudad</td>" +
					    	 "<td class=tittabdat align=center>Divisa</td>" +
					    	 "<td class=tittabdat align=center>Cve.ABA/SWIFT</td>" + 
					    	 "<td class=tittabdat align=center width = '266'>Domicilio</td>" +
					    	 "<td class=tittabdat align=center width = '266'>Ciudad Beneficiario</td>" +
					    	 "<td class=tittabdat align=center>N&uacute;mero de Identificaci&oacute;n</td>" +
					    	 "<td class=tittabdat align=center>Pa&iacute;s Beneficiario</td>";
 		request = setAtributosRequest(request, tablaDetalle, encabezaMenuU, encabezaMenu1);
 		request.setAttribute( ENCABEZADO_STR, CreaEncabezado( TITULO_PANTALLA,POS_MENU, CADENA_S26060H, request ) );
 		request.setAttribute("pathFileDownload", pathFileDownload);
 		request.setAttribute(NOM_ARCH, pathFileDownload);
 		request.setAttribute(CADENA_TOTAL, String.valueOf(total));
 		request.setAttribute(REQPARAM_PREV1, String.valueOf(prev1));
 		request.setAttribute(REQPARAM_NEXT1, String.valueOf(next1));
 		request.setAttribute(REQPARAM_REG_TOTAL, regTotal);
 		request.setAttribute(ATR_PAGNCN, metodoArmaPaginacion(next1, prev1, regTotal, numPag, totalPag));
 		request.setAttribute(VAR_PAGNCN, Global.NUM_REGISTROS_PAGINA);
 		//***********************************************
 		evalTemplate( IEnlace.PRINCIPAL, request, response );
 		return 1;
 	}

 	/**
 	 * Metodo para armar tabla de Detalle
 	 * creado por sonar
 	 * @since 23/04/2015
 	 * @author FSW-Indra
 	 * @param ctasList lista con la info para la tabla
 	 * @return String html de la tabla detalle
 	 */
 	public String armaTablaDetalle(List<CuentaMovilBean> ctasList){
 		StringBuffer tablaDetalle = new StringBuffer();
 		String color = "";
 		int i = 0;
 		for ( CuentaMovilBean b : ctasList) {
 			EIGlobal.mensajePorTrace("Cuentas Moviles = "+b.getNumeroMovil(), NivelLog.INFO);
 			if ( ( i%2 ) == 0 ) {
 				color = TEXTABDATOBS;
 			}else {
 				color = TEXTABDATCLA;
 			}
 			tablaDetalle.append( TAG_TR )
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getNumeroMovil()).append(TAG_CLOSE_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getTitular()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getTipoCuenta()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getDescBanco()).append(NBSP_TAG_TD)
			 			.append(TAG_CLOSE_TR);
 			i++;
 		}
 		EIGlobal.mensajePorTrace("Total Cuentas Moviles = "+(i), NivelLog.INFO);
 		return tablaDetalle.toString();
 	}
 	/**
 	 * Metodo para armar tabla de Detalle de cuentas Internancarias
 	 * creado por sonar
 	 * @since 05/06/2015
 	 * @author z708764 - Jorge Olalde
 	 * @param ctasList lista con la info para la tabla
 	 * @return String html de la tabla detalle
 	 */
 	public String armaTablaDetalleInterbancarias(List<CuentaInterbancariaBean> ctasList){
 		StringBuffer tablaDetalle = new StringBuffer();
 		String color = "";
 		int i = 0;
 		for ( CuentaInterbancariaBean b : ctasList) {
 			EIGlobal.mensajePorTrace("Cuentas Interbancarias = "+b.getnumeroInterbancaria(), NivelLog.INFO);
 			if ( ( i%2 ) == 0 ) {
 				color = TEXTABDATOBS;
 			}else {
 				color = TEXTABDATCLA;
 			}
 			tablaDetalle.append( TAG_TR )
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getnumeroInterbancaria()).append(TAG_CLOSE_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getdescripcion()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getbanco()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getplaza()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getsucursal()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getDivisa()).append(NBSP_TAG_TD)
			 			.append(TAG_CLOSE_TR);
 			i++;
 		}
 		EIGlobal.mensajePorTrace("Total Cuentas Interbancarias = "+(i), NivelLog.INFO);
 		return tablaDetalle.toString();
 	}
 	/**
 	 * Metodo para armar tabla de Detalle de cuentas mismo banco (Santander)
 	 * creado por sonar
 	 * @since 05/06/2015
 	 * @author z708764 - Jorge Olalde
 	 * @param ctasList lista con la info para la tabla
 	 * @return String html de la tabla detalle
 	 */
 	public String armaTablaDetalleSantander(List<CuentasSantanderBean> ctasList){
 		StringBuffer tablaDetalle = new StringBuffer();
 		String color = "";
 		int i = 0;
 		for ( CuentasSantanderBean b : ctasList) {
 			EIGlobal.mensajePorTrace("Cuentas Santander = "+b.getNumCuenta(), NivelLog.INFO);
 			if ( ( i%2 ) == 0 ) {
 				color = TEXTABDATOBS;
 			}else {
 				color = TEXTABDATCLA;
 			}
 			tablaDetalle.append( TAG_TR )
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getNumCuenta()).append(TAG_CLOSE_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getDescripcion()).append(NBSP_TAG_TD)
 				        .append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getTipoRelacCtas()).append(NBSP_TAG_TD)
 						.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getCLABE()).append(NBSP_TAG_TD)
			 			.append(TAG_CLOSE_TR);
 			i++;
 		}
 		EIGlobal.mensajePorTrace("Total Cuentas Interbancarias = "+(i), NivelLog.INFO);
 		return tablaDetalle.toString();
 	}
 	/**
 	 * Metodo para armar tabla de Detalle de cuentas Internacionales
 	 * creado por sonar
 	 * @since 05/06/2015
 	 * @author z708764 - Jorge Olalde
 	 * @param ctasList lista con la info para la tabla
 	 * @return String html de la tabla detalle
 	 */
 	public String armaTablaDetalleInternacionales(List<CuentaInternacionalBean> ctasList){
 		StringBuffer tablaDetalle = new StringBuffer();
 		String color = "";
 		int i = 0;
 		for ( CuentaInternacionalBean b : ctasList) {
 			EIGlobal.mensajePorTrace("Cuentas Internacionales = "+b.getnumeroInternacional(), NivelLog.INFO);
 			if ( ( i%2 ) == 0 ) {
 				color = TEXTABDATOBS;
 			}else {
 				color = TEXTABDATCLA;
 			}
 			tablaDetalle.append( TAG_TR )
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getnumeroInternacional()).append(TAG_CLOSE_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.gettitular()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getbanco()).append(NBSP_TAG_TD);
 			if(b.getpais().trim().length() > 0){
 				 if(("USA").equals(b.getpais().substring(b.getpais().length() - 3))){
 					tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append("USA").append(NBSP_TAG_TD);
 				 }else{
 					tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getpais()).append(NBSP_TAG_TD);
 				 }
 			}
			tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getciudad()).append(NBSP_TAG_TD)
			 			.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getdivisa()).append(NBSP_TAG_TD);
 			if(b.getpais().trim().length() > 0){
				 if(("USA").equals(b.getpais().substring(b.getpais().length() - 3))){
					 tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getaba()).append(NBSP_TAG_TD);
				 }else{
					 tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getswift()).append(NBSP_TAG_TD);
				 }
			}
 			tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getDireccionBenef()).append(NBSP_TAG_TD);
 			tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getCiudadBenef()).append(NBSP_TAG_TD);
 			tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getIdClienteBenef()).append(NBSP_TAG_TD);
 			tablaDetalle.append(TAG_TD_CLASS2).append( color ).append("'>").append(b.getPaisBenef()).append(NBSP_TAG_TD);
 			tablaDetalle.append(TAG_CLOSE_TR);
 			i++;
		 /***
		  * ("numeroInternacional", "Numero de Cuenta")1
			("titular", "Titular")2
			("banco", BANCO)3
			("pais", "Pa�s")4
			("ciudad", "Ciudad")5
			("divisa", "Divisa")6
			("aba", "Cve.ABA")7
			("swift", "SWIFT")8
		  * */

 		}
 		EIGlobal.mensajePorTrace("Total Cuentas Internacionales = "+(i), NivelLog.INFO);
 		return tablaDetalle.toString();
 	}
 	/**
 	 * Metodo para set atributos al request
 	 * @since 23/04/2015
 	 * @author FSW-Indra
 	 * @param request informacion
 	 * @param tablaDetalle html con la tabla de detalle
 	 * @param encabezaMenuU html con el menu
 	 * @param encabezaMenu1 html con el menu
 	 * @return HttpServletRequest valores
 	 */
 	public HttpServletRequest setAtributosRequest(HttpServletRequest request, String tablaDetalle, String encabezaMenuU,String encabezaMenu1){
		String vacio   = "";
		request.setAttribute( ENCBZDS_TBL, vacio );
 		request.setAttribute( ENCBZDS_TBLU, encabezaMenuU );
 		request.setAttribute( ENCBZDS_TBL1, encabezaMenu1 );
 		request.setAttribute( MSG1, vacio);
 		request.setAttribute( MSGU, vacio);
 		request.setAttribute( MSG2, tablaDetalle );
 		request.setAttribute( BOTON,vacio);
 		request.setAttribute( CONT_USER_SRT, ObtenContUser(request));
 		return request;
 	}
 	/**
 	 * Metodo para generar html de paginacion
 	 * creado por sonar
 	 * @since 05/06/2015
 	 * @author z708764 - Jorge Olalde
 	 * @param next1 valor de tipo int
 	 * @param prev1 valor de tipo int
 	 * @param regTotal valor de tipo int
 	 * @param numPag valor de tipo int
 	 * @param totalPag valor de tipo int
 	 * @return String html de paginacion
 	 */
 		public String metodoArmaPaginacion(int next1, int prev1, int regTotal, int numPag, int totalPag){
		StringBuffer paginacion = new StringBuffer();
 		paginacion.append("<table border='0' cellspacing='0' cellpadding='0' width=450 align=center>");
		EIGlobal.mensajePorTrace ("consultaMovil - Valores antes de Paginaci�n: regTotal["+regTotal+"] prev1["+prev1+"] next1["+next1+"]", EIGlobal.NivelLog.DEBUG);
 		int nexprev = next1 - prev1;
 		EIGlobal.mensajePorTrace ("consultaMovil - Valores antes de Paginaci�n: nexprev["+nexprev+"]", EIGlobal.NivelLog.DEBUG);
 		int totnex = regTotal - next1;
 		EIGlobal.mensajePorTrace ("consultaMovil - Valores antes de Paginaci�n: totnex["+totnex+"]", EIGlobal.NivelLog.DEBUG);
 		if (regTotal > Global.NUM_REGISTROS_PAGINA) {
 			EIGlobal.mensajePorTrace ("consultaMovil - Valores de Paginaci�n: entre en el primer if", EIGlobal.NivelLog.DEBUG);
 			paginacion.append(TAG_TR);
			if(prev1 > 0) {
				EIGlobal.mensajePorTrace ("consultaMovil - Valores de Paginaci�n: entre en el segundo if", EIGlobal.NivelLog.DEBUG);
				paginacion.append("<td align=left class=\"textabref\"><a href='javascript:anteriores();'>Anteriores ");
				paginacion.append(Global.NUM_REGISTROS_PAGINA);
				paginacion.append("</a>&nbsp;</td>");
			}
			if(next1 < regTotal) {
				EIGlobal.mensajePorTrace ("consultaMovil - Valores de Paginaci�n: entre en el tercer if", EIGlobal.NivelLog.DEBUG);
				if((next1 + Global.NUM_REGISTROS_PAGINA) <= regTotal) {
					EIGlobal.mensajePorTrace ("consultaMovil - Valores de Paginaci�n: entre en el cuarto if", EIGlobal.NivelLog.DEBUG);
					paginacion.append("<td align=right class=\"textabref\">&nbsp;<a href='javascript:siguientes();'>Siguientes ");
					paginacion.append(nexprev);
					paginacion.append("</a></td>");
				} else {
					EIGlobal.mensajePorTrace ("consultaMovil - Valores de Paginaci�n: entre en el else del cuarto if", EIGlobal.NivelLog.DEBUG);
					paginacion.append("<td align=right class=\"textabref\">&nbsp;<a href='javascript:siguientes();'>Siguientes ");
					paginacion.append(totnex);
					paginacion.append("</a></td>");
				}
			}
			paginacion.append(TAG_TR);
		}
 		/**se implementa el numero de pagina actual y el total de paginas**/
 		paginacion.append(TAG_TR);
 		paginacion.append("<table  width=450 align=center> <td align=center class='textabref'> <a>P&aacute;gina ");
 		paginacion.append(numPag);
 		paginacion.append(" de ");
 		paginacion.append(totalPag);
 		paginacion.append("</a>&nbsp </td></table>");
 		paginacion.append("</tr>");
 		EIGlobal.mensajePorTrace ("consultaMovil - Valores Paginaci�n: regTotal["+regTotal+"] prev1["+prev1+"] next1["+next1+"]"+"numPag["+numPag+"]totalPag["+totalPag+"]", EIGlobal.NivelLog.DEBUG);
 		paginacion.append("</tr></table>");
 		EIGlobal.mensajePorTrace( A_PAGNCN+paginacion, NivelLog.INFO);
 		return paginacion.toString();
 	}

     /**
      * Metodo para preparar las cuentas Internacionales a presentar en pantalla, esto de acuerdo a los
      * filtros indicados por el cliente
 	  * @since 05/06/2015
 	  * @author z708764 - Jorge Olalde
      * @param request de tipo HttpServletRequest
      * @param response de tipo HttpServletResponse
      * @param arrayCuentas de tipo String
      * @param total de tipo int
      * @param prev de tipo int
      * @param next de tipo int
      * @return numero 1 o 0
      * @throws ServletException para menejo de excepciones
      * @throws IOException para menejo de excepciones
      */
     public int consultaInternacional(HttpServletRequest request, HttpServletResponse response,int totalRegistros) throws ServletException, IOException {
    	 EIGlobal.mensajePorTrace("ContCtas_AL.java::consultaInternacional()",NivelLog.INFO);
    	 List<CuentaInternacionalBean> ctasInternacionalList = new ArrayList<CuentaInternacionalBean>();

    	 boolean esInternacional = true;
    	 request.setAttribute("esInternacional", esInternacional);
    	 
    	 String reqNumPag = request.getParameter(NUM_PAGINA);
    	 final int numPag = (reqNumPag!=null && !(NULL.equalsIgnoreCase(reqNumPag) || "".equals(reqNumPag)))
						 			? Integer.parseInt(request.getParameter(NUM_PAGINA))
						 					: 1;
		int totalPaginas = 1;
		boolean banderapaginacion=true;
	//	if ( SesionValida( request, response ) ) {
			ctasInternacionalList = llenaBeanYConsultaCtaInternacionales(request,numPag);

   			if(ctasInternacionalList != null && !ctasInternacionalList.isEmpty()){
   				request.setAttribute("ctasInternacionalList", ctasInternacionalList);
   				EIGlobal.mensajePorTrace(MSG_TRACE_REGISTROS + totalRegistros, EIGlobal.NivelLog.INFO);
   			}else{
   				despliegaPaginaErrorURL(SIN_DATOS,TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA, CONT_CTAS_AL_MODULO0,request,response);
   				return -1;
   			}
   			if(totalRegistros>RANGO){
   				totalPaginas = totalRegistros/RANGO;
   				if(totalRegistros%RANGO > 0){
   					totalPaginas++;
   				}
   			}
   			int prev1=0,next1=0;
   			if  ((request.getParameter(REQPARAM_PREV1)!=null && !"".equals(request.getParameter(REQPARAM_PREV1)) && !NULL.equals(request.getParameter(REQPARAM_PREV1))) &&
   					(request.getParameter(REQPARAM_NEXT1)!=null && !"".equals(request.getParameter(REQPARAM_NEXT1)) && !NULL.equals(request.getParameter(REQPARAM_NEXT1)) ) &&
   					(request.getParameter(REQPARAM_REG_TOTAL)!=null && !"".equals(request.getParameter(CADENA_TOTAL))  && !NULL.equals(request.getParameter(REQPARAM_REG_TOTAL)))) {
   				prev1 =  Integer.parseInt(request.getParameter(REQPARAM_PREV1));
   				next1 =  Integer.parseInt(request.getParameter(REQPARAM_NEXT1));
   				totalRegistros = Integer.parseInt(request.getParameter(REQPARAM_REG_TOTAL));
   				EIGlobal.mensajePorTrace("consultaInternacional::no son nulos", EIGlobal.NivelLog.INFO);
   				EIGlobal.mensajePorTrace("consultaInternacional::IF::totalPaginas::" + totalPaginas+"::prev1::"+prev1+"::next1:"+next1+"::regTotal::"+totalRegistros, EIGlobal.NivelLog.INFO);
   				banderapaginacion=false;
   			}
   			if(banderapaginacion){
   				prev1=0;
   				if (totalRegistros>Global.NUM_REGISTROS_PAGINA){
   					next1=Global.NUM_REGISTROS_PAGINA;
   				} else {
   					next1=totalRegistros;
   				}
   			}
   			request.setAttribute(CADENA_TOTAL_PAGINAS,totalPaginas);
   			request.setAttribute(NUM_PAGINA,numPag);
   			EIGlobal.mensajePorTrace("consultaInternacional::totalPaginas::" + totalPaginas+"::numPag::"+numPag+"::prev1::"+prev1+"::next1:"+next1+"::regTotal::"+totalRegistros, EIGlobal.NivelLog.INFO);
   			consultaInternacionales(request, response, ctasInternacionalList, totalPaginas, prev1, next1, totalRegistros, numPag, totalPaginas);
	//	}
    	 return 1;
     }
	/**
	 * Obtiene el numero de lineas de las cuentas Integrales
	 * @param filename de tipo String
	 * @return int j
	 */
     public int getnumberlinesCtasInteg (String filename) {
		 EIGlobal.mensajePorTrace ("***getnumberlinesCtasInteg ", EIGlobal.NivelLog.DEBUG);
		 int j = 0;
		 String linea = null;
			 FileReader lee = null;
		 try{
				 lee= new FileReader (filename);
		     BufferedReader entrada= new BufferedReader (lee);
		     while((linea = entrada.readLine ())!=null) {
				 if("".equals (linea.trim ())){
				     linea = entrada.readLine ();
				 }
				 if(linea != null){
					 j++;
				 }
		     }
		     entrada.close ();
		 } catch(IOException e) {
		     EIGlobal.mensajePorTrace (" "+e, EIGlobal.NivelLog.ERROR);
		     EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		     EIGlobal.mensajePorTrace ("E->getnumberlinesCtasInteg->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		     return 0;
		 } finally {
				 try{
					 lee.close();
				 }catch(IOException e) {
				 }
			 }
		 EIGlobal.mensajePorTrace ("***leer numero de lineas "+j, EIGlobal.NivelLog.DEBUG);
		 return j;
	}

	/**
	 * Obtiene el numero de lineas de las cuentas Integrales con Clabe santander
	 * @param filename de tipo String
	 * @return int j
	 */
	public int getnumberlinesCtasIntegClabe(String filename) {
		EIGlobal.mensajePorTrace("***getnumberlinesCtasInteg ", NivelLog.INFO);
		int j        = 0;
		String linea = "";
		//String dato  = "";
		String datos[] =null;
		int separadores=0;
		BufferedReader entrada = null;// Modificacion RMM 20021218
		try{
			entrada= new BufferedReader(new FileReader(filename)); // Modificacion RMM 20021218
			while((linea = entrada.readLine())!=null){
				if("".equals(linea.trim())){
					linea = entrada.readLine();
				}
				if(linea == null){
					break;
				}
				separadores=cuentaseparadores(linea);
				datos = desentramaC(""+separadores+"@"+linea,'@');
				String claveProducto= datos[7];
				if("1".equals(claveProducto) || "2".equals(claveProducto) || "3".equals(claveProducto) || "6".equals(claveProducto) || "8".equals(claveProducto)  )
					//MHG (IM224718)     if(claveProducto.equals("1"))
				{
					j++;
				}
			}
			entrada.close();
		}catch(IOException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
			//return 0;    // Modificacion RMM 20021218 commented out
			j = 0;// Modificacion RMM 20021218 mas abajo return j;
		} finally{// Modificacion RMM 20021218 inicio
			if(null != entrada){
				try{
					entrada.close();
				}catch(IOException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				}
				entrada = null;
			}
		}// Modificacion RMM 20021218 fin
		EIGlobal.mensajePorTrace("***leer numero de lineas "+j, NivelLog.INFO);
		return j;
	}







/**
 * Christian Barrios Osornio CBO 21/12/2004 Q21952
 * Nuevo m�todo  para escribir las cuentas clave en un archivo de exportar en formato .txt Q21952
 * @param datos de tipo String
 * @param nombre_archivo de tipo String
 * @return boolean
 */
boolean CreaCtasClabeImpor(String datos[][], String nombre_archivo){

   String linea= new String();
   PrintWriter out;
   boolean f=true;

EIGlobal.mensajePorTrace("Chris ruta= "+nombre_archivo, NivelLog.INFO);

try{

     out=new PrintWriter(new FileWriter(nombre_archivo));


      for(int i=0; i<datos.length;i++){
   	   StringBuffer buf= new StringBuffer(datos[i][0].trim());

   	   if(buf.length()<16){
   		   int r=16-buf.length();
   		   for(int j=0;j<r;j++){
   			   buf.append(" ");
   			   }
   	   }

   	   linea=buf.toString();

          buf=new StringBuffer(datos[i][1].trim());

          if(buf.length()<20){
   		   int r=20-buf.length();
   		   for(int j=0;j<r;j++){
   			   buf.append(" ");
   			   }
   		   }

   	   linea=linea+buf.toString();

          buf=new StringBuffer(datos[i][2].trim());

   	          if(buf.length()<40){
   	   		   int r=40-buf.length();
   	   		   for(int j=0;j<r;j++){
   	   			   buf.append(" ");
   	   			   }
   		       }

   	   linea=linea+buf.toString();

          if ("P".equals( datos[i][3].trim() ) ) {
              linea=linea + "01";
          } else if ("T".equals( datos[i][3].trim() ) ) {
              linea=linea + "02";
          } else { 
        	  linea=linea + "03";
          }
          
   		out.println(linea);

   }//Fin del For

   out.flush();
out.close();

f=envia(new File(nombre_archivo));

EIGlobal.mensajePorTrace("Chris  CreaCtasClabeImpo   f="+f, NivelLog.INFO);
 }//try

catch(IOException e){
	EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
	EIGlobal.mensajePorTrace("No se pudo crear el archivo......CreaCtasClabeImpor" , NivelLog.INFO);
	f=false;
	EIGlobal.mensajePorTrace("Chris  CreaCtasClabeImpo   f="+f, NivelLog.INFO);

	}


return f;




  }//Fin CreaCtasClabeImpor
/*Christian Barrios Osornio CBO 21/12/2004 Q21952
Nuevo m�todo  para escribir las cuentas clave en un archivo de exportar en formato .txt Q21952*/

/**
 * Christian Barrios Osornio CBO 21/12/2004 Q21952
 * Nuevo m�todo  para copiar el archivo CuentasClabe+nombre_archivo+.txt en ambos servidores Q21952
 * @param Archivo de tipo File
 * @return boolean
 */
boolean envia (File Archivo) {
//       try {

		    EIGlobal.mensajePorTrace("dentro del metodo envia de ContCtas_Al.java", NivelLog.DEBUG);

		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_LOCAL = " + Global.DIRECTORIO_LOCAL, NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_REMOTO_WEB = " + Global.DIRECTORIO_REMOTO_WEB, NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_LOCAL = " + Global.HOST_LOCAL, NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_REMOTO_WEB = " + Global.HOST_REMOTO_WEB, NivelLog.DEBUG);
			//Req. Q15591  Se corrige validacion para no copiar, se incluye comparacion de rutas.

		    EIGlobal.mensajePorTrace("Envia. Pasa validacion para generar copia remota", NivelLog.DEBUG);

		  //IF PROYECTO ATBIA1 (NAS) FASE II
           	boolean Respuestal = true;
            ArchivoRemoto envArch = new ArchivoRemoto();

           	if(!envArch.copiaLocalARemoto(Archivo.getName(),"WEB")){

					EIGlobal.mensajePorTrace("*** ContCtas_AL.envia  no se pudo copiar archivo remoto_web:" + Archivo.getName(), NivelLog.ERROR);
					Respuestal = false;

				}
				else {
				    EIGlobal.mensajePorTrace("*** ContCtas_AL.envia archivo remoto copiado exitosamente:" + Archivo.getName(), NivelLog.DEBUG);

				}

           	if (Respuestal){
                return true;
			}

//        } catch (Exception ex) {
//        	EIGlobal.mensajePorTrace(new Formateador().formatea(ex), NivelLog.INFO);
//        }
        return false;
    }

/*Christian Barrios Osornio CBO 21/12/2004 Q21952
Nuevo m�todo  para copiar el archivo CuentasClabe+nombre_archivo+.txt en ambos servidores Q21952*/

/***********************************NRN -- Getronics Q12535 inicia nuevo******************************/
/**********************************************InicioConsCtas************************************/
/*********************************************************************************************/
    /**
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws SQLException Excepcion
     * @throws Exception Excepcion
     */
    public void InicioConsCtas(  HttpServletRequest request, HttpServletResponse response)throws SQLException, Exception
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
		String strInhabiles = armaDiasInhabilesJS();
		StringBuffer varFechaHoy=new StringBuffer();

		//Obtiene los usuarios de las cuentas asociadas al contrato.
		String[][] arrayUsuarios = TraeUsuarios(request);
		String Usuarios          = "";



        //modificacion para cliente 7to8 cgc 11/03/2004
           Convertidor con = new Convertidor();
           for (int z = 1; z <= Integer.parseInt( arrayUsuarios[0][0].trim() ); z++ ) {
        	   arrayUsuarios[z][1]=con.cliente_7To8(arrayUsuarios[z][1]);
           }
 	   //fin de la modificacion 11/03/2004 cgc.

			Usuarios = "";

			for ( int indice = 1; indice <= Integer.parseInt( arrayUsuarios[0][0].trim() ); indice++ )
			{
				Usuarios += "<OPTION value = " + arrayUsuarios[indice][1] + ">" + arrayUsuarios[indice][1] + " " + arrayUsuarios[indice][2] + "\n";
			}

			String  datesrvr  = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">\n";
					datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">\n";
					datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">\n";

			request.setAttribute("Bitfechas", datesrvr);

			request.setAttribute("Usuarios",Usuarios);

		EIGlobal.mensajePorTrace( "ContCtas_AL:  Entrando InicioConsCtas() -->> Cuentas Nuevo", NivelLog.INFO);

  		/************************************************** Arreglos de fechas para 'Hoy' ... */
		varFechaHoy.append("  /*Fecha De Hoy*/");
		varFechaHoy.append("\n  dia=new Array(");
		varFechaHoy.append(generaFechaAnt(CADENA_DIA));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt(CADENA_DIA));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes=new Array(");
		varFechaHoy.append(generaFechaAnt(CADENA_MES));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt(CADENA_MES));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio=new Array(");
		varFechaHoy.append(generaFechaAnt(CADENA_ANIO));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt(CADENA_ANIO));
		varFechaHoy.append(");");
		varFechaHoy.append("\n\n  /*Fechas Seleccionadas*/");
		varFechaHoy.append("\n  dia1=new Array(");
		varFechaHoy.append(generaFechaAnt(CADENA_DIA));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt(CADENA_DIA));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes1=new Array(");
		varFechaHoy.append(generaFechaAnt(CADENA_MES));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt(CADENA_MES));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio1=new Array(");
		varFechaHoy.append(generaFechaAnt(CADENA_ANIO));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt(CADENA_ANIO));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  Fecha=new Array('");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("','");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("');");

		EIGlobal.mensajePorTrace( "ContCtas_AL Enviando strInhabiles + varFechaHoy...."+ strInhabiles  +varFechaHoy , NivelLog.INFO);

        EIGlobal EIG=new EIGlobal(session , getServletContext() , this  );

		request.setAttribute("DiasInhabiles",strInhabiles);
		request.setAttribute("VarFechaHoy",varFechaHoy.toString());
		request.setAttribute("DiaHoy",EIG.fechaHoy("dd/mm/aaaa"));
		request.setAttribute(ENCABEZADO_STR, CreaEncabezado("Filtro de consulta de cuentas por autorizar",UBC_AUT_CANC3,S55310H,request));
		EIGlobal.mensajePorTrace( "ContCtas_AL Enviando las nuevas variables a Cont_Cons.jsp....", NivelLog.INFO);
		//TODO BIT CU5091, inicio de flujo Autorizaci�n
		 /*VSWF IHG I*/
		  if (("ON").equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		try{
		BitaHelper bh = new BitaHelperImpl(request, session, sess);

		if (request.getParameter(BitaConstants.FLUJO)!=null){
			bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
		}else{

			bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
		}
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.EA_CUENTAS_AUTO_CANC_ENTRA);
		if (session.getContractNumber() != null) {
			bt.setContrato(session.getContractNumber().trim());
		}

		BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
		}
		  }
		 /*VSWF HGG F*/

		evalTemplate ("/jsp/Cont_Cons.jsp", request, response);
	}

/*********************************************************************************************/
/*****************************************generaTablaConsultar************************************/
/*********************************************************************************************/
    /**
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws SQLException Excepcion
     * @throws Exception Excepcion
     */
    public void generaTablaConsultar(  HttpServletRequest request, HttpServletResponse response)throws SQLException, Exception
	{

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
        request.setAttribute (FECHA_STR, ObtenFecha ());

		String tramaServicio="";
		String Result="";
		String pipe="|";
		String nombreArchivo="";
		String nombreArchivoExportacion="";
		String exportar="";
		String mensajeError="";
		String arcLinea="";

			/* Variables de ambiente */
		String usuario=(session.getUserID8()==null)?"":session.getUserID8();
		String clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		String contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		String contratoConsulta=contrato;

		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos contrato:"+contrato, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos contratoConsulta:"+contratoConsulta, NivelLog.INFO);

        // Filtro por usuario Opcion
        String usuarioOpcion =(String) request.getParameter ("usuarioOpcion");
        if (usuarioOpcion == null || usuarioOpcion.length () == 0){
            usuarioOpcion = " ";
        }
		// Campo en el cual se hara la consulta
       String tipoConsulta =(String) request.getParameter ("Registro");

        // Filtro por fecha
		String Fecha1_01=(String)request.getParameter("Fecha1_01");
		String Fecha1_02=(String)request.getParameter("Fecha1_02");

        // Folio de registro de mancomunidad
        String folioReg =(String) request.getParameter ("Folio_Registro");
        if (folioReg == null || folioReg.length () == 0){
            folioReg = " ";
        }
        // Folio de registro de mancomunidad
        String fcuenta =(String) request.getParameter ("FCuenta");
        if (fcuenta == null || fcuenta.length () == 0){
            fcuenta = " ";
        }
        // Filtro por Estatus
		String tipoEstatus="";
		if (request.getParameter ("TipoEstatus")!= null)
		 {
		      tipoEstatus= (String)request.getParameter ("TipoEstatus");
		 }
		else
	     {
            tipoEstatus= "" + (request.getParameter ("ChkAuto") == null ? "" : request.getParameter ("ChkAuto")) +
              (request.getParameter ("ChkRech") == null ? "" : request.getParameter ("ChkRech")) +
              (request.getParameter ("ChkCanc") == null ? "" : request.getParameter ("ChkCanc")) +
              (request.getParameter ("ChkPend") == null ? "" : request.getParameter ("ChkPend")) ;
           if (tipoEstatus == null || tipoEstatus.length () == 0){
              tipoEstatus = " ";
           }
           if (tipoEstatus.endsWith (",")){
               tipoEstatus = tipoEstatus.substring (0, tipoEstatus.length () - 1);
           }
          }

        // Filtro por Tipo Cuenta
		String tipoCuenta="";
		if (request.getParameter (TIPO_CTA)!= null)
		 {
		      tipoCuenta= (String)request.getParameter (TIPO_CTA);
		 }
		else
		{
		   tipoCuenta = "" + (request.getParameter ("ChkPropiasMismo") == null ? "" : request.getParameter ("ChkPropiasMismo")) +
            (request.getParameter ("ChkTercerosMismo") == null ? "" : request.getParameter ("ChkTercerosMismo")) +
            (request.getParameter ("ChkOtrosNales") == null ? "" : request.getParameter ("ChkOtrosNales")) +
            (request.getParameter ("ChkOtrosInter") == null ? "" : request.getParameter ("ChkOtrosInter")) ;
           if (tipoCuenta == null || tipoCuenta.length () == 0) {
              tipoCuenta = " ";
           }
		    if (tipoCuenta.endsWith (",")) {
               tipoCuenta = tipoCuenta.substring (0, tipoCuenta.length () - 1);
		    }
         }

        // Filtro por Tipo Operacion

		String tipoOperAB = "";
		if (request.getParameter ("tipoOperAB")!= null)
		 {
		      tipoOperAB= (String)request.getParameter ("tipoOperAB");
		 }
		else
		{
  		      tipoOperAB = "" + (request.getParameter (CADENA_ALTA) == null ? "" : request.getParameter (CADENA_ALTA)) +
              (request.getParameter ("Baja") == null ? "" : request.getParameter ("Baja"));
              if (tipoOperAB == null || tipoOperAB.length () == 0){
              tipoOperAB = " ";
              }
  		     if (tipoOperAB.endsWith (",")){
             tipoOperAB = tipoOperAB.substring (0, tipoOperAB.length () - 1);
  		     }
       }

/***********Session*****************************/

		request.getSession().setAttribute("usuarioOpcion",usuarioOpcion);

		request.getSession().setAttribute("Registro",tipoConsulta);

		request.getSession().setAttribute("Fecha1_01",Fecha1_01);
		request.getSession().setAttribute("Fecha1_02",Fecha1_02);

		request.getSession().setAttribute("Folio_Registro",folioReg);

		request.getSession().setAttribute("FCuenta",fcuenta);

		request.getSession().setAttribute("TipoEstatus",tipoEstatus);

		request.getSession().setAttribute(TIPO_CTA,tipoCuenta);

		request.getSession().setAttribute("tipoOperAB",tipoOperAB);

/*****************************************/

		Fecha1_01=formateaCamposfecha(Fecha1_01);
		Fecha1_02=formateaCamposfecha(Fecha1_02);

		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos usuarioOpcion:"+usuarioOpcion, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos tipoConsulta:"+tipoConsulta, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos Fecha1_01:"+Fecha1_01, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos Fecha1_02:"+Fecha1_02, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos folioReg:"+folioReg, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos fcuenta:"+fcuenta, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos tipoEstatus:"+tipoEstatus, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos tipoCuenta:"+tipoCuenta, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>Campos Recibidos tipo operacion Alta o Baja:"+tipoOperAB, NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ContCtas_AL >>>>El directorio al que van los archivos es: " + Global.DIRECTORIO_REMOTO_CUENTAS , NivelLog.INFO);

		int totalRegistros=0;
		int registrosPorPagina=0;
		int totalPaginas=0;
		int pagina=0;

		ArchivoRemoto archR = new ArchivoRemoto();

			tramaServicio=
								contrato				+ "@" +
								usuarioOpcion		+ "@" +
								tipoConsulta		+ "@" +
								Fecha1_01			+ "@" +
								Fecha1_02			+ "@" +
								folioReg				+ "@" +
								fcuenta				+ "@" +
								tipoEstatus			+ "@" +
								tipoCuenta			+ "@" +
								tipoOperAB			+ "@" + pipe;

		EIGlobal.mensajePorTrace( "ContCtas_AL Ejecutando llamado TUXEDO ....:"+Result, NivelLog.INFO);
        Result=llamaServicio(IEnlace.medioEntrega2,"CT03",contrato,usuario,clavePerfil,tramaServicio);

		//TODO BIT CU5091, Consulta de cuentas
		/*VSWF HGG I*/
        if (("ON").equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
        try{

		if(null!=Result || !(NULL).equals(Result)){
			EI_Tipo temp=new EI_Tipo();
			if(("OK").equals(Result.trim().substring(0,2))){
				temp.iniciaObjeto('@','|',Result);
				String archivos=temp.camposTabla[0][2];
				if(archivos.indexOf(";")>0){
					nombreArchivoExportacion=archivos.substring(archivos.indexOf(';')+1);
				 }
			}
		}

		BitaHelper bh = new BitaHelperImpl(request, session, sess);

		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.EA_CUENTAS_AUTO_CANC_CONSULTA_CUENTA);
		if (session.getContractNumber() != null) {
			bt.setContrato(session.getContractNumber().trim());
		}
		if(Result != null){
			if("OK".equals(Result.substring(0,2))){
				bt.setIdErr("CT030000");
			}else if(Result.length() > 8){
				bt.setIdErr(Result.substring(0,8));
			}else{
				bt.setIdErr(Result.trim());
			}
		}
		if (nombreArchivoExportacion != null) {
			bt.setNombreArchivo(nombreArchivoExportacion);
		}
		bt.setServTransTux("CT03");

		BitaHandler.getInstance().insertBitaTransac(bt);
        } catch (SQLException e) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
		}
        }
		/*VSWF HGG T*/

		EIGlobal.mensajePorTrace( "ContCtas_AL TRAMA TUXEDO Regreso:"+Result, NivelLog.INFO);

	if(null==Result || (NULL).equals(Result)) {
		despliegaPaginaErrorURL("<br>Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento.<br>Por favor intente mas tarde.",
							AUT_CANC,UBC_AUT_CANC3,S55310H,CONTCTAS_M2,request,response);
					return;
	}
	else {
		EI_Tipo trRes=new EI_Tipo();
		if("OK".equals(Result.trim().substring(0,2))) {
			trRes.iniciaObjeto('@','|',Result);
			String archivos=trRes.camposTabla[0][2];
			if(archivos.indexOf(";")>0) {
				nombreArchivo=archivos.substring(0,archivos.indexOf(';'));
				nombreArchivoExportacion=archivos.substring(archivos.indexOf(';')+1);
			}
			else {
				nombreArchivo=archivos;
			}
		}
		else {
//validado
					nombreArchivo=usuario;
			      	  	      /***********************************************************  Copia Remota*/
					if(!archR.copia(nombreArchivo)) {
						EIGlobal.mensajePorTrace("generaTablaConsultar(): No se realizo la copia remota.", NivelLog.INFO);
						mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
						despliegaPaginaErrorURL(mensajeError,AUT_CANC2,
								UBC_AUT_CANC,
								S55310H,CONTCTAS_M2,request, response);
						return;
					}
/************************************************************/
					else {
						EI_Exportar ArcEnt=new EI_Exportar(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivo);
						EIGlobal.mensajePorTrace("generaTablaConsultar nombreArchivo enviado a EI_Exportar: "+nombreArchivo, NivelLog.DEBUG);
						if(ArcEnt.abreArchivoLectura()) {
							arcLinea=ArcEnt.leeLinea();
							mensajeError=arcLinea.substring(16,arcLinea.length());
					    	EIGlobal.mensajePorTrace("mensaje: "  +  mensajeError, NivelLog.INFO);
							ArcEnt.cierraArchivo();
							despliegaPaginaErrorURL(mensajeError,AUT_CANC,
									UBC_AUT_CANC,
									S55310H,CONTCTAS_M2,request, response);
						}
					}
					return;
		}
		EIGlobal.mensajePorTrace( "ContCtas_AL-> Ejecutando la copio de TUXEDO : "+ nombreArchivo, NivelLog.INFO);
		if(archR.copiaCUENTAS(nombreArchivo,Global.DIRECTORIO_LOCAL)) {
			if(archR.copiaCUENTAS(nombreArchivoExportacion,Global.DIRECTORIO_LOCAL)) {
				nombreArchivoExportacion=nombreArchivoExportacion.substring(nombreArchivoExportacion.lastIndexOf("/")+1);
				if(archR.copiaLocalARemoto(nombreArchivoExportacion,"WEB")) {
					EIGlobal.mensajePorTrace( "ContCtas_AL-> Se copio a WEB-A: "+ nombreArchivoExportacion, NivelLog.INFO);
					//FSWI
					exportar= "<a href='javascript:exportacion()' border=0><img src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar' border=0></a>";
				}
				else{
					EIGlobal.mensajePorTrace( "ContCtas_AL-> No se pudo realizar la copia remota al WEB del archivo exportacion:"+nombreArchivoExportacion, NivelLog.INFO);
				}
			}
			else{
				EIGlobal.mensajePorTrace( "ContCtas_AL-> No se pudo realizar la copia remota del archivo exportacion de tuxedo:"+nombreArchivoExportacion, NivelLog.INFO);
			}
			nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
			totalRegistros=obtenTotalLineas(nombreArchivo,request,response);
			if(totalRegistros>0) {
				EIGlobal.mensajePorTrace( "inicio pagina "+pagina, NivelLog.INFO);
				EIGlobal.mensajePorTrace( "inicio totalRegistros "+totalRegistros, NivelLog.INFO);
				EIGlobal.mensajePorTrace( "inicio registrosPorPagina "+registrosPorPagina, NivelLog.INFO);
				EIGlobal.mensajePorTrace( "inicio totalPaginas "+totalPaginas, NivelLog.INFO);
				registrosPorPagina=Global.MAX_REGISTROS;
				totalPaginas=calculaPaginas(totalRegistros,registrosPorPagina);
				EIGlobal.mensajePorTrace( "ContCtas_AL-> Generando la pantalla (Registros)", NivelLog.INFO);
				EIGlobal.mensajePorTrace( "calculo pagina "+pagina, NivelLog.INFO);
				EIGlobal.mensajePorTrace( "calculo totalRegistros "+totalRegistros, NivelLog.INFO);
				EIGlobal.mensajePorTrace( "calculo registrosPorPagina "+registrosPorPagina, NivelLog.INFO);
				EIGlobal.mensajePorTrace( "calculo totalPaginas "+totalPaginas, NivelLog.INFO);
				generaTablaConsultarRegistros(nombreArchivo,Fecha1_01,Fecha1_02,contratoConsulta,pagina,totalRegistros,registrosPorPagina,totalPaginas,exportar,request,response);
			}
			else if(totalRegistros<0) {
				EIGlobal.mensajePorTrace( "ContCtas_AL-> No se pudo abrir el archivo ... (Registros)", NivelLog.INFO);
				despliegaPaginaErrorURL("La <i><font color=blue>transacci&oacute;n</font></i> solicitada no se llevo a cabo correctamente.<br>Por favor intente mas tarde.",TITULO_PANTALLA,"Administraci&oacute;n y control &gt; Cuentas Relacionadas &gt Consulta y Autorizaci&oacute;n",CONTCTAS_M2,S55310H,request,response);
				return;
			}
			else if(totalRegistros==0) {
				EIGlobal.mensajePorTrace( "ContCtas_AL-> Archivo encontrado pero sin registros ... (Registros)", NivelLog.INFO);
				despliegaPaginaErrorURL("No se encontraron <font color=green>registros</font> para el tipo de b&uacute;squeda seleccionado.",AUT_CANC2,UBC_AUT_CANC3,S55310H,CONTCTAS_M2,request,response);
				return;
			}
		}
		else {
			EIGlobal.mensajePorTrace( "ContCtas_AL-> No se pudo realizar la copia remota del archivo de tuxedo (Registros):"+nombreArchivo, NivelLog.INFO);
			despliegaPaginaErrorURL("La <i><font color=green>transacci&oacute;n</font></i> solicitada no se llevo a cabo.<br>Por favor intente mas tarde.",AUT_CANC2,UBC_AUT_CANC3,S55310H,CONTCTAS_M2,request,response);
			return;
		}
	}

}

/*********************************************************************************************/
/***************************************** generaTablaConsultarRegistros*****************************/
/*********************************************************************************************/
 /**
 * @param nombreArchivo String
 * @param Fecha1_01 String
 * @param Fecha1_02 String
 * @param contratoConsulta String
 * @param pagina int
 * @param totalRegistros int
 * @param registrosPorPagina int
 * @param totalPaginas int
 * @param exportar String
 * @param request HttpServletRequest
 * @param response HttpServletResponse
 * @throws ServletException
 * @throws java.io.IOException
 */
public void generaTablaConsultarRegistros(String nombreArchivo,String Fecha1_01,String Fecha1_02,String contratoConsulta,int pagina,int totalRegistros,int registrosPorPagina,int totalPaginas,String exportar,HttpServletRequest request,HttpServletResponse response) throws ServletException, java.io.IOException
   {
      /************* Modificacion para la sesion ***************/
      HttpSession sess = request.getSession();
	 // BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Generando la tabla de consulta de archivos", NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Objetos creados para la consulta de archivos nombreArchivo "+nombreArchivo, NivelLog.INFO);
  	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Objetos creados para la consulta de archivos pagina "+pagina, NivelLog.INFO);
  	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Objetos creados para la consulta de archivos registrosPorPagina "+registrosPorPagina, NivelLog.INFO);
  	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Objetos creados para la consulta de archivos totalRegistros "+totalRegistros, NivelLog.INFO);

	  String numRegpage="";
	  String [] strRegistros ;
	  String consulta="";

	  StringBuffer tablaRegistros=new StringBuffer("");
	  StringBuffer botones=new StringBuffer("");
	  // String chkBox="";
	   String[] titulos={"",
			   "Tipo Operaci&oacute;n",
			   "Tipo Cuenta",
		       "Cuenta/M&oacute;vil",
		       "Titular",
			   BANCO,
		       "Fecha Registro",
			   "Folio Registro",
		       "Fecha Autorizaci&oacute;n",
			   "Folio Autorizaci&oacute;n",
			   "Usuario Registr&oacute;",
			   "Usuario Autoriz&oacute;",
			   "Estado",
			   "Divisa",
			   "Observaciones",};

      int[] datos={14,4,9,1,2,3,10,4,6,5,11,7,8,0,14,12,15,16,17,18}; //{14,4,9,1,2,3,10,4,6,5,11,7,8,0,14,12};
      /* INICIO everis JARA Alta de Cuentas se agrego el 13*/
	  /* int[] values={13,0,9,1,2,3,10,4,6,5,11,7,8,0,12};*/
	  int[] values={19,0,9,1,2,3,10,4,6,5,11,7,8,0,12,13,15,16,17,18};  //{15,0,9,1,2,3,10,4,6,5,11,7,8,0,12,13};
	  /* FIN everis JARA Alta de Cuentas */
	  int[] align={0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	  EI_Tipo regSel=new EI_Tipo();
	 // EI_Tipo headSel=new EI_Tipo();

	  EIGlobal.mensajePorTrace( "ContCtas_AL >> generaTablaConsultarRegistros()-> Se leyo el archivo.", NivelLog.INFO);

	  consulta=obtenDatos(nombreArchivo);

	  if("ERR".equals(consulta.trim()))
	   {
		  despliegaPaginaErrorURL("La <i><font color=blue>transacci&oacute;n</font></i> solicitada no se llevo a cabo correctamente.<br>Por favor intente mas tarde.",AUT_CANC,UBC_AUT_CANC3,S55310H,CONTCTAS_M2,request,response);
		  return;
	   }
	  else	  if("".equals(consulta.trim()))
	   {
		  EIGlobal.mensajePorTrace( "ContCtas_AL-> No se encontraron registros en el archivo.", NivelLog.INFO);
		  despliegaPaginaErrorURL("No se encontraron <font color=green>registros</font> para el rango de fechas y/o estatus seleccionados",AUT_CANC,UBC_AUT_CANC3,S55310H,CONTCTAS_M2,request,response);
		  return;
	   }
	  else
	   {
		  EIGlobal.mensajePorTrace( "ContCtas_AL-> Objetos creados para la consulta de archivos", NivelLog.INFO);

	  strRegistros=seleccionaRegistrosDeArchivo(nombreArchivo,pagina,registrosPorPagina,totalRegistros);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> nombreArchivo"+nombreArchivo , NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> pagina"+pagina , NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> registrosPorPagina"+registrosPorPagina, NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> totalRegistros"+totalRegistros , NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Registros 0>>"+strRegistros[0] +"<<", NivelLog.INFO);
	  regSel.iniciaObjeto(';','@',strRegistros[0]);
	  formateaCamposT(regSel,0,1,9, datos);
	  ejecutaExportacion(request, response);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Objetos creados para la consulta de archivos..."+regSel.chkBox, NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Registros 1>>"+strRegistros[1] +"<<", NivelLog.INFO);
	  regSel.chkBox=strRegistros[1];
	  tablaRegistros.append(regSel.generaTablaCHK(titulos,datos,values,align));
	  numRegpage=strRegistros[2];//para saber cuantos registros tengo por cada pagina
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Registros 2>>"+strRegistros[2] +"<<", NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> Objetos creados", NivelLog.INFO);
	  EIGlobal.mensajePorTrace( "ContCtas_AL-> registros Pendientes en Pagina actual numRegpage: "+numRegpage, NivelLog.INFO);

	  if(totalRegistros>registrosPorPagina)
	   {
		  EIGlobal.mensajePorTrace("ContCtas_AL >> generaTablaConsultarRegistros()-> Estableciendo links para las paginas", NivelLog.INFO);
		  botones.append("\n <br>");
		  botones.append("\n <table width=620 border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
		  EIGlobal.mensajePorTrace("ContCtas_AL >> generaTablaConsultarRegistros()-> Pagina actual = "+pagina, NivelLog.INFO);
		  if(pagina>0) {
			 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
		  }
		  if(totalPaginas>=2)
		   {
			 for(int i=1;i<=totalPaginas;i++) {
			  if(pagina+1==i)
			   {
				 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
				 botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a> ");
			   }
			 }
		   }
		  if(totalRegistros>((pagina+1)*registrosPorPagina)) {
			 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");
		  }

		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");
	   }

		request.setAttribute("Fecha1_01",Fecha1_01);
		request.setAttribute("Fecha1_02",Fecha1_02);
		request.setAttribute("nombreArchivo",nombreArchivo);
		request.setAttribute("Exportar",exportar);
		request.setAttribute("contratoConsulta",contratoConsulta);
		request.setAttribute(CADENA_TOTAL_PAGINAS,Integer.toString(totalPaginas));
		request.setAttribute("totalRegistros",Integer.toString(totalRegistros));
		request.setAttribute("numRegpage",numRegpage);
		request.setAttribute("registrosPorPagina",Integer.toString(registrosPorPagina));
		request.setAttribute("pagina",Integer.toString(pagina+1));
		request.setAttribute("tablaRegistros",tablaRegistros.toString());
		request.setAttribute("Botones",botones.toString());
		request.setAttribute("Pagina","1");

		//request.setAttribute(MENU_PRINCIPAL_STR, session.getStrMenu());
		//request.setAttribute(NEW_MENU_STR, session.getFuncionesDeMenu());
		request.setAttribute(ENCABEZADO_STR, CreaEncabezado(AUT_CANC2,UBC_AUT_CANC3,S55320H,request));
		EIGlobal.mensajePorTrace( "ContCtas_AL Enviando las nuevas variables a  Cont_ConsTabla.jsp....", NivelLog.INFO);
		evalTemplate ("/jsp/Cont_ConsTabla.jsp", request, response);
   }
}

/************************************ Arma dias inhabiles   *********************************/
/*************************************************************************************/
  /**
 * @return String
 */
String armaDiasInhabilesJS()
   {
      String resultado="";
      Vector diasInhabiles;

	  int indice=0;

      diasInhabiles=CargarDias(1);
      if(diasInhabiles!=null)
	   {
		 resultado=diasInhabiles.elementAt(indice).toString();
		 for(indice=1; indice<diasInhabiles.size(); indice++) {
  		   resultado += ", " + diasInhabiles.elementAt(indice).toString();
		 }
       }
      return resultado;
	}

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 * @param formato int
 * @return Vector
 */
/*************************************************************************************/
  public Vector CargarDias(int formato)
	{

	  EIGlobal.mensajePorTrace(" ContCtas_A>> Entrando a CargaDias ", NivelLog.INFO);

	  Connection  Conexion = null;
	  PreparedStatement qrDias = null;
	  ResultSet rsDias = null;
	  String     sqlDias;
	  Vector diasInhabiles = new Vector();



	  sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2)";
	  EIGlobal.mensajePorTrace("ContCtas_A>> Query"+sqlDias,NivelLog.DEBUG);
	  try
	   {
		 Conexion = createiASConn(Global.DATASOURCE_ORACLE);
		 if(Conexion!=null)
		  {
		    qrDias = Conexion.prepareStatement(sqlDias);
		    if(qrDias!=null)
		     {
		  	  rsDias = qrDias.executeQuery();
		       if(rsDias!=null)
		        {
		          while( rsDias.next())
					{
		              String dia  = rsDias.getString(1);
		              String mes  = rsDias.getString(2);
		              String anio = rsDias.getString(3);
		              if(formato == 0)
		               {
		                  dia  =  Integer.toString( Integer.parseInt(dia) );
		                  mes  =  Integer.toString( Integer.parseInt(mes) );
		                  anio =  Integer.toString( Integer.parseInt(anio) );
		               }

		              String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
		              diasInhabiles.addElement(fecha);
					  /********************/
		              }

		       }else {
		         EIGlobal.mensajePorTrace("ContCtas_A>>  Cannot Create ResultSet",NivelLog.WARN);
		       }
		    }else {
		      EIGlobal.mensajePorTrace("ContCtas_A>>  Cannot Create Query",NivelLog.WARN);
		    }
		 }else {
			 EIGlobal.mensajePorTrace("ContCtas_A>> No se pudo crear la conexion",NivelLog.WARN);
		 }
	   }catch(SQLException sqle ){
		   EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), NivelLog.INFO);
	   }finally {
		   EI_Query.cierraConexion(rsDias, qrDias, Conexion);
	   }
	 return(diasInhabiles);
	}

   /**
    * fecha Consulta
 * @param tipo String
 * @return String
 */
String generaFechaAnt(String tipo)
    {
      String strFecha = "";
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);

	   while(Cal.get(Calendar.DAY_OF_WEEK) == 1 || Cal.get(Calendar.DAY_OF_WEEK) == 7) {
           Cal.add(Calendar.DATE, -1);
       }

      if("FECHA".equals(tipo)) {
         if(Cal.get(Calendar.DATE) <= 9) {
           strFecha += "0" + Cal.get(Calendar.DATE) + "/";
         }
         else {
           strFecha += Cal.get(Calendar.DATE) + "/";
         }

         if(Cal.get(Calendar.MONTH)+ 1 <= 9) {
           strFecha += "0" + (Cal.get(Calendar.MONTH) + 1) + "/";
         }
         else {
           strFecha += (Cal.get(Calendar.MONTH) + 1) + "/";
         }

         strFecha+=Cal.get(Calendar.YEAR);
        }

      if((CADENA_DIA).equals(tipo)){
        strFecha += Cal.get(Calendar.DATE);
      }
      if((CADENA_MES).equals(tipo)){
        strFecha += (Cal.get(Calendar.MONTH) + 1);
      }
      if((CADENA_ANIO).equals(tipo)){
        strFecha += Cal.get(Calendar.YEAR);
      }
      return strFecha;
    }

	/**
	 * formatea Campos Fecha
	 * @param fec String
	 * @return String
	 */
	public String formateaCamposfecha(String fec)
	 {
		String fecha=fec;
		if(fec.trim().length()==10){
			fecha=fec.substring(6) + fec.substring(3,5) + fec.substring(0, 2);
		}
		return fecha;
	 }
	
  /**
   * llamaServicio
 * @param medioEntrega String
 * @param tipoOperacion String
 * @param contrato String
 * @param usuario String
 * @param clavePerfil String
 * @param tramaServicio String
 * @return String
 * @throws IOException Excepcion
 */
public String llamaServicio(String medioEntrega,String tipoOperacion,String contrato, String usuario,String clavePerfil,String tramaServicio) throws IOException
    {

	   String Result="";
	   String Trama="";

	   EIGlobal.mensajePorTrace("ContCtas_AL -> - Servicio de Consulta Trama entrada: "+Trama, NivelLog.DEBUG);
		//Cabecera
				Trama = medioEntrega 		+ "|" +
						usuario 					+ "|" +
						tipoOperacion			+ "|" +
						contrato					+ "|" +
						usuario 					+ "|" +
						clavePerfil				+ "|" +
					    tramaServicio			;

	    EIGlobal.mensajePorTrace("ContCtas_AL.java-> - Servicio de Consulta Trama entrada: "+Trama, NivelLog.DEBUG);

		ServicioTux tuxGlobal = new ServicioTux();

		try
		 {
			Hashtable htResult = tuxGlobal.web_red( Trama );
			Result = ( String ) htResult.get( "BUFFER" );
	     } catch(Exception e){
	    	 EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
	     }

		EIGlobal.mensajePorTrace("ContCtas_AL.java-> - Trama salida: "+Result, NivelLog.DEBUG);

		return Result;
	}

  /**
 * @param Archivo String
 * @return String
 * @throws IOException Excepcion
 */
public String obtenDatos(String Archivo) throws IOException
	{
	  String linea="";
	  StringBuffer result=new StringBuffer("");

	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));

		  while((linea=arc.readLine())!=null) {
			 result.append(linea+"@");
		  }
		  arc.close();
		}catch(IOException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
			result = new StringBuffer("ERR");
		 }
	  return result.toString();
	}

  /**
 * @param Archivo String
 * @param req HttpServletRequest
 * @param res HttpServletResponse
 * @return int
 * @throws IOException Excepcion
 */
public int obtenTotalLineas(String Archivo,HttpServletRequest req, HttpServletResponse res) throws IOException
    {
      int num=0;
	

	  String linea="";
	 

	

	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));
		  while((linea=arc.readLine())!=null)
		   {
				 java.util.StringTokenizer tokenizer = new java.util.StringTokenizer (linea,";");
			 	 num++;
		   }
		  arc.close();
		}catch(IOException e)
		{
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
		  num=-1;
		}
      return num;
    }

   /**
    * Regresa el total de paginas
 * @param total int
 * @param regPagina int
 * @return int
 */
int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0) {
        totPag=1;
      }else {
         if(a5>1.0) {
          totPag=(total/regPagina)+1;
         }
         if(a5==1.0) {
          totPag=(total/regPagina);
         }
       }
      return totPag;
    }

  /**
 * @param Archivo String
 * @return String
 * @throws IOException Excepcion
 */
public String obtenTitulos(String Archivo) throws IOException
    {
      String line="";
	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));
		  line=arc.readLine()+"@";
		  if(line==null) {
			 return "";
		  }
		}catch(IOException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
			line="";
		 }
      return line;
    }

  /**
 * @param Archivo String
 * @param pagina int
 * @param regPorPagina int
 * @param totalReg int
 * @return String[]
 * @throws IOException Excepcion
 */
public String[] seleccionaRegistrosDeArchivo(String Archivo,int pagina,int regPorPagina,int totalReg) throws IOException
	{
	  String linea="";
	  String box="";
	  StringBuffer result=new StringBuffer("");
	  String [] resultx=new String[3];

	  int numregpage=0;
	  int inicio=0;
	  int fin=0;

	  inicio=(pagina*regPorPagina);
	  fin=inicio+regPorPagina;
	  if(fin>totalReg){
		fin=totalReg;
	  }
	  BufferedReader arc=new BufferedReader(new FileReader(Archivo));

	  for(int i=0;i<fin;i++)
	   {
		 linea=arc.readLine();
		 if(i>=inicio)
		 {
			result.append(linea+"@");

			if(linea.charAt(0)!='I' && linea.charAt(0)!='P')
			{
			box=box +"0";
			}
			else
			{
			box=box +"1";
			numregpage++;
			}
	  	  }
		 }

	  String numreg= numregpage +CADENA_VACIA;
	  arc.close();
	  EIGlobal.mensajePorTrace("ContCtas_AL >> seleccionaRegistrosDeArchivo() result.toString(): "+result.toString(), NivelLog.INFO);
	  EIGlobal.mensajePorTrace("ContCtas_AL >> seleccionaRegistrosDeArchivo() box: "+box, NivelLog.INFO);
	  EIGlobal.mensajePorTrace("ContCtas_AL >> seleccionaRegistrosDeArchivo() numreg: "+numreg, NivelLog.INFO);
	  resultx[0]=result.toString();
	  resultx[1]=box;
	  resultx[2]=numreg;
	  return resultx;
	}

 /**
 * @param req HttpServletRequest
 * @param res HttpServletResponse
 * @throws ServletException Excepcion
 * @throws java.io.IOException Excepcion
 */
public void generaTablaConsultarRegistrosPorPaginacion( HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	  EIGlobal.mensajePorTrace( "ContCtas_AL >> generaTablaConsultarRegistrosPorPaginacion()-> Entrando a paginacion de archivo", NivelLog.INFO);

	  String Fecha1_01=(String)req.getParameter("Fecha1_01");
	  String Fecha1_02=(String)req.getParameter("Fecha1_02");
	  String nombreArchivo=(String)req.getParameter("nombreArchivo");
	  String contratoConsulta=(String)req.getParameter("contratoConsulta");
	  String exportar=(String)req.getParameter("Exportar");
	  String chkNombre=(String)req.getParameter("chkNombre");

      int totalRegistros=Integer.parseInt((String)req.getParameter("totalRegistros"));
	  int registrosPorPagina=Integer.parseInt((String)req.getParameter("registrosPorPagina"));
	  int totalPaginas=Integer.parseInt((String)req.getParameter(CADENA_TOTAL_PAGINAS));
	  int pagina=Integer.parseInt((String)req.getParameter("pagina"));

	  EIGlobal.mensajePorTrace( "ContCtas_AL >> generaTablaConsultarRegistrosPorPaginacion()-> Busqueda por registros ", NivelLog.INFO);

      generaTablaConsultarRegistros(nombreArchivo,Fecha1_01,Fecha1_02,contratoConsulta,pagina,totalRegistros,registrosPorPagina,totalPaginas,exportar,req,res);
  }
	/**
	 * Metodo para formatear campos.
	 * @param Tipo de tipo EI_Tipo
	 * @param Est_Oper de tipo int
	 * @param tipo_cta de tipo int
	 * @param Tipo_Sol de tipo int
	 * @param datos de tipo int
	 */
   public void formateaCamposT(EI_Tipo Tipo,int Est_Oper,int tipo_cta, int Tipo_Sol, int[] datos) {
	   listaExport = new ArrayList<ExportAdmCtrlCtaACBean>();
	   EIGlobal.mensajePorTrace( "ContCtas_AL >> formateaCamposT()-> Formateando estatus operaci�n del objeto tabla: "+Est_Oper, NivelLog.INFO);

	   for(int i=0;i<Tipo.totalRegistros;i++) {
		   /*EIGlobal.mensajePorTrace( "ContCtas_AL >> formateaCamposT()-> i,Est_Oper:"+Tipo.camposTabla[i][Est_Oper], NivelLog.INFO);
				EIGlobal.mensajePorTrace( "ContCtas_AL >> formateaCamposT()-> i,tipo_cta:"+Tipo.camposTabla[i][tipo_cta], NivelLog.INFO);
					EIGlobal.mensajePorTrace( "ContCtas_AL >> formateaCamposT()-> i,Tipo_Sol:"+Tipo.camposTabla[i][Tipo_Sol], NivelLog.INFO);*/

		  if(!(null==Tipo.camposTabla[i][Est_Oper] || ("").equals(Tipo.camposTabla[i][Est_Oper].trim()))) {
					if("A".equals(Tipo.camposTabla[i][Est_Oper].trim())) {
						Tipo.camposTabla[i][Est_Oper]="Ejecutada";
					}
					else if("P".equals(Tipo.camposTabla[i][Est_Oper].trim())) {
					  Tipo.camposTabla[i][Est_Oper]="Pendiente por activar";
					}
					else
					if("I".equals(Tipo.camposTabla[i][Est_Oper].trim())) {
					  Tipo.camposTabla[i][Est_Oper]="Pendiente por autorizar";
					}
					else
					if("R".equals(Tipo.camposTabla[i][Est_Oper].trim())) {
					  Tipo.camposTabla[i][Est_Oper]="Rechazado";
					}
					else
					if("C".equals(Tipo.camposTabla[i][Est_Oper].trim())){
					  Tipo.camposTabla[i][Est_Oper]="Cancelado";
					}
			  }

		  if(! (null==Tipo.camposTabla[i][tipo_cta] || ("").equals(Tipo.camposTabla[i][tipo_cta].trim()))) {
			  if(("I").equals(Tipo.camposTabla[i][tipo_cta].trim())) {
				  Tipo.camposTabla[i][tipo_cta]="Internacional";
			  }
			  else
				  if(("E").equals(Tipo.camposTabla[i][tipo_cta].trim())) {
					  Tipo.camposTabla[i][tipo_cta]="Externa";
				  }
				  else
					  if(("P").equals(Tipo.camposTabla[i][tipo_cta].trim())) {
						  Tipo.camposTabla[i][tipo_cta]="Propia";
					  }
					  else
						  if(("T").equals(Tipo.camposTabla[i][tipo_cta].trim())) {
							  Tipo.camposTabla[i][tipo_cta]="Tercera";
						  }
		  }

		 if(! (null==Tipo.camposTabla[i][Tipo_Sol] || ("").equals(Tipo.camposTabla[i][Tipo_Sol].trim()))) {
	  				if(("A").equals(Tipo.camposTabla[i][Tipo_Sol].trim())) {
	  					Tipo.camposTabla[i][Tipo_Sol]=CADENA_ALTA;
	  				}
	  				else
	  					if(("B").equals(Tipo.camposTabla[i][Tipo_Sol].trim())) {
	  						Tipo.camposTabla[i][Tipo_Sol]="Baja";
	  					}
		 }

		/*for(int m=0;m<14;m++)
			  EIGlobal.mensajePorTrace( "ContCtas_AL >> formateaCamposT()->"+i+","+m+">>"+Tipo.camposTabla[i][m]+"<<", NivelLog.INFO);*/

	  EIGlobal.mensajePorTrace( "ContCtas_AL >> formateaCamposT()-> Campos formateados ...." + Tipo.camposTabla[i][Est_Oper], NivelLog.INFO);

	  listaExport.add(llenarBeanExportar(Tipo.camposTabla[i]));

		}
	}

/**
 * @param req HttpServletRequest
 * @param res HttpServletResponse
 * @return int
 * @throws ServletException Excepcion
 * @throws java.io.IOException Excepcion
 */
public int EjecutaAltaBajaRechazo( HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {

	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

   	 String tramaServicio="";
	 String Result="";
	 String usuario="";
	 String contrato="";
	 String clavePerfil="";
  	 String tipo="";
	 String arcLinea="";
	 String medioEntrega	="";
	 String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/" + session.getUserID8()+".cctts";
	 String pipe="|";
	 String strMensaje="";
	 String MsgErrorR1="";
	 String MsgErrorR2="";
	 String registroRechazados="";
 	 String registroAceptados="";
	 String line="";
	// String Fecha1_01=(String)req.getParameter("Fecha1_01");
//	 String Fecha1_02=(String)req.getParameter("Fecha1_02");
	 String totalRegistros=(String)req.getParameter("totalRegistros");
	 String numRegpage=(String)req.getParameter("numRegpage");
	 String chkNombre=(String)req.getParameter("chkNombre");
	 String tipoOper = req.getParameter( "tipoOper" );
	 String top="";
	 String srvrnombre="";

	 /* INICIO everis JARA Alta de Cuentas */
	 String idLote = "0";
	 /* FIN everis JARA Alta de Cuentas */

	 /**GAE**/
	 Vector detalleCuentas = new Vector();
	 String nomEstatus = "";
	 String nomAut = "Pendiente por activar";
	 String tipoSolic = "";
	 /**FIN GAE**/

	 EIGlobal.mensajePorTrace( "ContCtas_AL >> EjecutaAltaBajaRechazo()-> Tipo Operacion: "+ tipoOper, NivelLog.INFO);
     EIGlobal.mensajePorTrace( "ContCtas_AL >> EjecutaAltaBajaRechazo()-> Recibiendo trama Registros: "+ chkNombre, NivelLog.INFO);

	int envioDeArchivo=0;
	boolean consulta=false;



		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		medioEntrega="2EWEB";

        EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
		ArchivoRemoto archR = new ArchivoRemoto();

		EI_Tipo TCTArchivo= new EI_Tipo();
		TCTArchivo.iniciaObjeto(chkNombre);

		int	regenviados=0;
		Map<String, LynxAutCanCuentasDTO> mapaRegistrosLynx = new HashMap<String, LynxAutCanCuentasDTO>();

		//Se crea mapa para envio de informacion a Monitor Plus
		Map<String, Map<Integer, String>> mapaMonPlus = new HashMap<String, Map<Integer, String>>();
		
	   if(ArcSal.creaArchivo())
           {
             arcLinea="";
				 LynxAutCanCuentasDTO lynxDTO; 
				 Map<Integer, String> mapaMP;

				for(int i=0; i < TCTArchivo.totalRegistros; i++)
				{
					lynxDTO = new LynxAutCanCuentasDTO();
					mapaMP = new HashMap<Integer, String>();
				if("A".equals(tipoOper.trim()))
					{
					tipo="A";
					nomEstatus = TCTArchivo.camposTabla[i][12];
					top="Autorizaci&oacute;n";
					srvrnombre="CT04";
					lynxDTO.setTipoOperacion(LynxAutCanCuentasDTO.TIPO_OPERACION_AUTORIZACION);
					//Tipo de operacion
					mapaMP.put(1, "A");
					}
				 else if ("R".equals(tipoOper.trim()))
					{
					tipo="C";
					top="Cancelaci&oacute;n";
					srvrnombre="CT05";
					lynxDTO.setTipoOperacion(LynxAutCanCuentasDTO.TIPO_OPERACION_CANCELACION);
					//Tipo de operacion
					mapaMP.put(1, "C");
					}


				if(nomEstatus.equals(nomAut)){
					EIGlobal.mensajePorTrace( "ContCtas_AL >> Tratando de autorizar cuentas ya autorizadas.", NivelLog.INFO);

					strMensaje="<i><font color=red>Error</font></i> No es posible autorizar cuentas ya autorizadas.<br><br> Por favor realice la operaci&oacute;n <font color=green> nuevamente </font>.";
					despliegaPaginaErrorURL(strMensaje,AUT_CANC2,UBC_AUT_CANC,S55320H, CONTCTAS_M2, req, res);
					return 1;
				}
					 arcLinea="";
					arcLinea+=tipo;
					arcLinea+="@";
					arcLinea+=TCTArchivo.camposTabla[i][3];
					/* INICIO everis JARA Alta de Cuentas - Autorizacion y Cancelacion de cuentas*/
					arcLinea+="@";
					arcLinea+=TCTArchivo.camposTabla[i][14];
					/* FIN everis JARA Alta de Cuentas - Autorizacion y Cancelacion de cuentas*/
				    arcLinea+="@\n";

				    /**GAE**/
				    EmailContainer ec = new EmailContainer();
					tipoSolic = TCTArchivo.camposTabla[i][1].trim();
				    ec.setClaseCuenta(TCTArchivo.camposTabla[i][2]);
				    ec.setNumCuenta(TCTArchivo.camposTabla[i][3]);
				    ec.setTitular(TCTArchivo.camposTabla[i][4]);
				    ec.setFechRegistro(TCTArchivo.camposTabla[i][6]);
				    ec.setOrigenAlta("ENLACE");
					detalleCuentas.add(ec);
					 /**FIN GAE**/

					//Se llena la informacion para informacion en lynx
					lynxDTO.setClaseCuenta(TCTArchivo.camposTabla[i][2]);
					lynxDTO.setNumCuenta(TCTArchivo.camposTabla[i][3]);
					lynxDTO.setTitular(TCTArchivo.camposTabla[i][4]);
					lynxDTO.setFechaRegistro(TCTArchivo.camposTabla[i][6]);
					lynxDTO.setDescripcionStatus(TCTArchivo.camposTabla[i][12]);
					lynxDTO.setOrigenAlta("ENLACE");
					lynxDTO.setNumContrato(contrato);
					lynxDTO.setTipoCuenta(String.valueOf(TCTArchivo.camposTabla[i][2].charAt(0)));
					lynxDTO.setTipoAccion(String.valueOf(TCTArchivo.camposTabla[i][1].charAt(0)));

					//Se llena mapa para envio de informacion a monitor plus
					//Numero de Cuenta Origen
					mapaMP.put(3540, TCTArchivo.camposTabla[i][3]);
					//Nombre Cliente Titular Cta Origen
					mapaMP.put(3523, TCTArchivo.camposTabla[i][4]);
					//Descripcion
					mapaMP.put(2, TCTArchivo.camposTabla[i][12]);
					//Tipo de accion
					mapaMP.put(3, String.valueOf(TCTArchivo.camposTabla[i][1].charAt(0)));
					
					// Se coloca en mapa objeto con el numero de cuenta como llave
                    if (mapaRegistrosLynx.containsKey(lynxDTO.getNumCuenta())) {
                        EIGlobal.mensajePorTrace( ">>>>> Numero de Cuenta ya se encuentra procesado en el mapa de Lynx: " + lynxDTO.getNumCuenta(), EIGlobal.NivelLog.DEBUG);
                    } else {
                        mapaRegistrosLynx.put(lynxDTO.getNumCuenta(), lynxDTO);
                    }
                    
                    // Se coloca en mapaMonPlus objeto con el numero de cuenta como llave
                    if(mapaMonPlus.containsKey(TCTArchivo.camposTabla[i][3])){
                    	EIGlobal.mensajePorTrace( ">>>>> Numero de Cuenta ya se encuentra procesado en el mapa de Monitor Plus: " + TCTArchivo.camposTabla[i][3], EIGlobal.NivelLog.DEBUG);
                    }else{
                    	mapaMonPlus.put(TCTArchivo.camposTabla[i][3], mapaMP);
                    }

					EIGlobal.mensajePorTrace( "Aqu�-> ContCtas_AL >>  Tipo de solicitud " + tipoSolic, NivelLog.INFO);
					EIGlobal.mensajePorTrace( "ContCtas_AL >>  Realizando " + top + " de cuentas", NivelLog.INFO);

	                 EIGlobal.mensajePorTrace("ContCtas_AL regenviados: "+arcLinea, NivelLog.INFO);
					regenviados++;
	                 EIGlobal.mensajePorTrace("ContCtas_AL regenviados: "+regenviados, NivelLog.INFO);

                      if(!ArcSal.escribeLinea(arcLinea)){
                      EIGlobal.mensajePorTrace("ContCtas_AL >> EjecutaAltaBajaRechazo(): Algo salio mal escribiendo "+arcLinea, NivelLog.INFO);
                      }
			     }
				        ArcSal.cierraArchivo();
		   }
      else
           {
             EIGlobal.mensajePorTrace("ContCtas_AL >> EjecutaAltaBajaRechazo(): No se pudo llevar a cabo la carga de datos en el archivo.", NivelLog.INFO);
           }
//  Fin Formatea el campo dependiendo el tipo de accion a realizar Alta o Rechazo para el envio a Tuxedo del archivo de cuentas
/****************************************************************************************************/
	 String numreg= regenviados +CADENA_VACIA;
//validado
	  nombreArchivo=nombreArchivo.substring(nombreArchivo.lastIndexOf("/")+1);
	if( !archR.copiaLocalARemotoCUENTAS(nombreArchivo,Global.DIRECTORIO_LOCAL) )
	 {
			EIGlobal.mensajePorTrace( "ContCtas_AL >> No se realizo la copia remota a tuxedo.", NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ContCtas_AL >> Se detiene el proceso pues no se envia el archivo.", NivelLog.INFO);

		strMensaje="<i><font color=red>Error</font></i> de comunicaciones.<br><br> Por favor realice la operaci&oacute;n <font color=green> nuevamente </font>.";
		despliegaPaginaErrorURL(strMensaje,AUT_CANC2,UBC_AUT_CANC,S55320H,CONTCTAS_M2, req, res);
		return 1;
	 }
	else
	 {
		EIGlobal.mensajePorTrace( "ContCtas_AL >> Copia remota a TUXEDO se realizo OK ....", NivelLog.INFO);

		EIGlobal.mensajePorTrace( "ContCtas_AL >> Armando trama para llamado a servicio de " + top + " Cuentas.....", NivelLog.INFO);

					tramaServicio=
					nombreArchivo	;

				Result=llamaServicio(IEnlace.medioEntrega2,"CT04",contrato,usuario,clavePerfil,tramaServicio);

		//TODO BIT CU5091, Autorizaci�n de Alta y Baja de cuentas
		/*VSWF HGG I*/
	 if (("ON").equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		try{
		BitaHelper bh = new BitaHelperImpl(req, session, sess);

		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.EA_CUENTAS_AUTO_CANC_AUTORIZA_ALTA_BAJA);
		if (session.getContractNumber() != null) {
			bt.setContrato(session.getContractNumber().trim());
		}
		if(Result != null){
			if(("OK").equals(Result.substring(0,2))) {
				bt.setIdErr("CT040000");
			}else if(Result.length() > 8) {
				bt.setIdErr(Result.substring(0,8));
			}else {
				bt.setIdErr(Result.trim());
			}
		}
		bt.setServTransTux("CT04");

		BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
		}
	 }
		/*VSWF HGG F*/

	 if(null==Result || (NULL).equals(Result)) {
		 despliegaPaginaErrorURL("<br>Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento.<br>Por favor intente mas tarde.",AUT_CANC2,UBC_AUT_CANC2,S55320H,CONTCTAS_M2,req,res);
		 return 1;
	 }
	 else {
		 EI_Tipo trRes=new EI_Tipo();
		 if( !(("OK").equals(Result.trim().substring(0,2)))) {
			 consulta=false;
			 nombreArchivo=Result.substring(Result.indexOf("/"));
			 EIGlobal.mensajePorTrace("ContCtas_AL >> La consulta no fue exitosa.", NivelLog.INFO);
		 }
		 else {
			 consulta=true;
			 trRes.iniciaObjeto('@','|',Result);
			 nombreArchivo=trRes.camposTabla[0][2];
			 EIGlobal.mensajePorTrace("ContCtas_AL >> La consulta fue exitosa.", NivelLog.INFO);
		 }
		 EIGlobal.mensajePorTrace( "ContCtas_AL >> Nombre de archivo obtenido:  "+nombreArchivo, NivelLog.INFO);
		 if( !archR.copiaCUENTAS(nombreArchivo,Global.DIRECTORIO_LOCAL) ) {
			 EIGlobal.mensajePorTrace( "ContCtas_AL >> No se realizo la copia remota desde tuxedo.", NivelLog.INFO);
			 if(consulta)  {//trama ok pero no trae
				 strMensaje="<br>Fue enviada la petici&oacute;n pero no <font color=red>no</font> se pudo obtener la <font color=green>informaci&oacute;n</font> del registro.<br><br>Verifique en la <font color=blue>consulta de cuentas</font> para obtener el detalle del <b>n&uacute;mero</b> de registros que fueron enviados para ser <i>procesados para la " + top + " de cuentas.</i>.";
				 despliegaPaginaErrorURL(strMensaje,AUT_CANC2,UBC_AUT_CANC2,S55320H,CONTCTAS_M2,req,res);
				 return 1;
			 }
			 else {//trama mal  no ok  no se puede abrir el archivo
				 strMensaje="<br><b><font color=red>Transacci&oacute;n</font></b> no exitosa.<br><br>El env&iacute;o <font color=blue>no</font> fue realizado correctamente.<br>Por favor <font color=green>intente</font> en otro momento.";
				 despliegaPaginaErrorURL(strMensaje,AUT_CANC2,UBC_AUT_CANC2,S55320H,CONTCTAS_M2,req,res);
				 return 1;
			 }
		 }
		 else {
			 EIGlobal.mensajePorTrace( "ContCtas_AL >> Copia remota desde tuxedo se realizo correctamente.", NivelLog.INFO);
			 BufferedReader arc;
			 try {
				 EIGlobal.mensajePorTrace( "ContCtas_AL >> El archivo que se trajo (E): "+ nombreArchivo, NivelLog.INFO);
				 nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
				 EIGlobal.mensajePorTrace( "ContCtas_AL >> El archivo que se abre (E): "+nombreArchivo, NivelLog.INFO);
				 arc=new BufferedReader(new FileReader(nombreArchivo));
				 if(null!=(line=arc.readLine())) {
					 if(!consulta) {
						 if(line.trim().length()>16) {
							 strMensaje=line.trim().substring(16);
							 despliegaPaginaErrorURL(strMensaje,AUT_CANC2,UBC_AUT_CANC2,S55320H,CONTCTAS_M2,req,res);
							 return 1;
						 }
						 else {
							 strMensaje="<br><br><br>Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Por favor intente mas tarde.";
							 despliegaPaginaErrorURL(strMensaje,AUT_CANC2,UBC_AUT_CANC2,S55320H,CONTCTAS_M2,req,res);
							 return 1;
						 }
					 }
					 else {
						 EI_Tipo Sec=new EI_Tipo();
						 Sec.iniciaObjeto(';','|',line+"|");
						 MsgErrorR1=Sec.camposTabla[0][0];
						 registroAceptados=Sec.camposTabla[0][1];
						 MsgErrorR2=Sec.camposTabla[0][2];
						 registroRechazados=Sec.camposTabla[0][3];
						 /* INICIO everis JARA Alta de Cuentas*/
						 idLote = Sec.camposTabla[0][4];
						 EIGlobal.mensajePorTrace( "id_lote: "+idLote, NivelLog.INFO);
						 /* FIN everis JARA Alta de Cuentas*/
						 EIGlobal.mensajePorTrace( "totalRegistros: "+totalRegistros, NivelLog.INFO);
						 EIGlobal.mensajePorTrace( "numRegpage: "+numRegpage, NivelLog.INFO);
						 EIGlobal.mensajePorTrace( "Mensaje de er Aceptados: "+MsgErrorR1, NivelLog.INFO);
						 EIGlobal.mensajePorTrace( "Registros Aceptados : "+registroAceptados, NivelLog.INFO);
						 EIGlobal.mensajePorTrace( "Mensaje de er Rechazados: "+MsgErrorR2, NivelLog.INFO);
						 EIGlobal.mensajePorTrace( "Registros rechazados : "+registroRechazados, NivelLog.INFO);
						 if(!registroRechazados.equals("0")) {
							 envioDeArchivo=2;
							 EIGlobal.mensajePorTrace("ContCtas_AL >> No se actualizaron todos los registros....", NivelLog.INFO);
						 }
					 }
				 }
                 EIGlobal.mensajePorTrace(">>>> Procesando archivo respuesta para Lynx", EIGlobal.NivelLog.INFO);     
                 if (Global.LYNX_INICIALIZADO) {
                     EIGlobal.mensajePorTrace(">>>> Lynx Inicializado correctamente", EIGlobal.NivelLog.INFO);
    				 	 RespuestaLynxDTO respuesta = EnlaceLynxUtils.llamadoConectorLynxAutorizacionCancelacion(
    				 	 		mapaRegistrosLynx, req, tipoSolic, regenviados, registroAceptados, arc);
    	                 EIGlobal.mensajePorTrace(">>>> Lynx respuesta, codError: " + respuesta.getCodError() + ", mensaje: " + respuesta.getMsgError(), EIGlobal.NivelLog.INFO);
                 }
                 
                 //Envio de datos a Monitor Plus                 
                 EIGlobal.mensajePorTrace("-------->Se envian valores AutCanCuentas para mapeo a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
                 VinculacionMonitorPlusUtils.enviarValoresAutoCuentas(mapaMonPlus, req, regenviados, registroAceptados, session, arc, IEnlace.SUCURSAL_OPERANTE);
                 
				 try {
					 arc.close();
				 }catch(IOException a){
					 EIGlobal.mensajePorTrace(new Formateador().formatea(a), NivelLog.INFO);
				 }
			 }
			 catch(IOException e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
			 }
		 }
	 }

		//GAE
		//lanzaEmail(detalleCuentas, req);
		//FIN GAE
		if( ("0").equals(registroAceptados)  && ("0").equals(registroRechazados)){
			envioDeArchivo=2;
		}


	    if(envioDeArchivo==1) {
	    	despliegaPaginaErrorURL(strMensaje,AUT_CANC2,UBC_AUT_CANC,"s55330h", CONTCTAS_M2, req, res);
	    	return 1;
	    }
	    else {
	    	if(envioDeArchivo==0){
	    		strMensaje="<br><i>El archivo ha sido enviado satisfactoriamente.</i><br>Se enviaron <font color=green>"+numreg+"</font> registros correctamente para la "+top+".";
				/**GAE**/
	    		if("A".equals(tipoOper.trim()) && CADENA_ALTA.equals(tipoSolic)){
	    			lanzaEmail(detalleCuentas, req, idLote, numreg);
	    		}
				/**FIN GAE**/
			}
	    	if(envioDeArchivo==2) {
	    		int regSolCorr=Integer.parseInt(numreg.trim())-Integer.parseInt(registroAceptados.trim());
	    		strMensaje="<br>Durante el env&iacute;o de la informaci&oacute;n se encontraron algunos <font color=red>errores</font> al procesar las cuentas para la " + top +".";
	    		if(!numreg.trim().equals("1")) {
	    			strMensaje+="<br>De las <font color=green>"+numreg+"</font> cuentas enviadas, no se pudieron registrar <font color=red>"+ regSolCorr +"</font> para la solicitud de "+ top +".<br><font color blue>Revise</font> el detalle de la informaci&oacute;n y <font color=blue>verifique</font> su informaci&oacute;n.<br><br>";
	    		}
	    		else {
	    			strMensaje+="<br>No se pudo registrar correctamente la cuenta que se env&iacute;o por favor <br><font color blue>Revise</font> el detalle de la informaci&oacute;n y <font color=blue>verifique</font> su informaci&oacute;n.<br><br>";
	    		}
	    	}

			req.setAttribute("strMensaje",strMensaje);
			req.setAttribute("nombreArchivoImp",nombreArchivo);
			req.setAttribute("nombreArchivoDetalle",nombreArchivo);
			req.setAttribute("registroAceptados",registroAceptados);
			req.setAttribute("registroRechazados",registroRechazados);
			req.setAttribute("totalRegistros",totalRegistros);
			req.setAttribute("numRegpage",numreg);
			req.setAttribute("tipoOper",tipoOper);


	
			req.setAttribute(ENCABEZADO_STR, CreaEncabezado(AUT_CANC2,"Administraci&oacute;n y control &gt; Cuentas &gt Consulta y Autorizaci&oacute;n","s55330h", req));

			EIGlobal.mensajePorTrace("ContCtas_AL >> agregaCuentas(): Finalizando ......", NivelLog.WARN);
			evalTemplate("/jsp/Cont_ConsEnvio.jsp", req, res);
	}

 }
 return 1;
}

	/**
	 * GAE
	 * Metodo encargado de disparar la rutina de confirmaci�n via Email
	 *
	 * @param detalleCuentas		detalle de cuentas a Autorizar
	 * @param req					Request del sistema
	 * @param id_lote				id del lote
	 * @param numReg				Numero de registro
	 */
	private void lanzaEmail(Vector detalleCuentas, HttpServletRequest req, String id_lote, String numReg){
		EmailSender es = new EmailSender();
		HttpSession ses = req.getSession();

		BaseResource session = (BaseResource) ses.getAttribute(SESSION_BR);
		String numContrato = session.getContractNumber();
		String descContrato = session.getNombreContrato();

		String idLote = "0";
		/* INICIO everis JARA Alta de Cuentas */
		if( !id_lote.equals("") && id_lote!="" ){
			idLote = id_lote;
			}
		/* FIN everis JARA Alta de Cuentas */
		es.confirmaEmail(detalleCuentas, numContrato, descContrato, req, idLote, session, numReg);
	}

	/**
	 * Metodo para llenar el bean para Exportar.
	 * @param tipo de ripo String
	 * @return ExportAdmCtrlCtaACBean cuenta
	 */
	public ExportAdmCtrlCtaACBean llenarBeanExportar (String[] tipo){

		ExportAdmCtrlCtaACBean cuenta = new ExportAdmCtrlCtaACBean();
		int i = 0;

		cuenta.setCheck(tipo[i].trim());
		cuenta.setEstado(tipo[i++].trim());
		cuenta.setTipoCuenta(tipo[i++].trim());
		cuenta.setCuenta(tipo[i++].trim());
		cuenta.setTitular(tipo[i++].trim());
		cuenta.setFechaRegistro(tipo[i++].trim());
		cuenta.setFechaAtorizacion(tipo[i++].trim());
		cuenta.setFolioRegistro(tipo[i++].trim());
		cuenta.setUsrRegistrado(tipo[i++].trim());
		cuenta.setUsrAutorizado(tipo[i++].trim());
		cuenta.setTipoOperacion(tipo[i++].trim());
		cuenta.setBanco(tipo[i++].trim());
		cuenta.setFolioAutorizacion(tipo[i++].trim());
		cuenta.setObservaciones(tipo[i++].trim());
		i++;
		cuenta.setDivisa(tipo[i++].trim());

		return cuenta;

	}

	/**
	 * Metodo para ejecutar la Exportacion.
	 * @param req de tipo HttpServletRequest
	 * @param res de tipo HttpServletResponse
	 * @throws ServletException para manejo de excepciones
	 * @throws IOException para manejo de excepciones
	 */
	public void ejecutaExportacion (HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{

		ExportModel em = new ExportModel("ctasAutorizacionCancelacion", '|');
		em.addColumn(new Column("tipoOperacion", "Tipo Operaci�n"));
		em.addColumn(new Column(TIPO_CTA, "Tipo Cuenta"));
		em.addColumn(new Column("cuenta", "Cuenta/M�vil"));
		em.addColumn(new Column("titular", "Titular"));
		em.addColumn(new Column("banco", BANCO));
		em.addColumn(new Column("fechaRegistro", "Fecha Registro"));
		em.addColumn(new Column("folioRegistro", "Folio Registro"));
		em.addColumn(new Column("fechaAtorizacion", "Fecha Autorizaci�n"));
		em.addColumn(new Column("folioAutorizacion", "Folio Autorizaci�n"));
		em.addColumn(new Column("usrRegistrado", "Usuario Registr�"));
		em.addColumn(new Column("usrAutorizado", "Usuario Autoriz�"));
		em.addColumn(new Column("estado", "Estado"));
		em.addColumn(new Column("divisa", "Divisa"));
		em.addColumn(new Column("observaciones", "Observaciones"));


	    req.getSession().setAttribute(PARAM_ATRIBUTO_EXPORT_MODEL, em);
	    req.setAttribute(PARAM_TIPO_EXPORT, "BEAN");
	    req.setAttribute(NOMBRE_BEAN_EXPORT, "listaBeanExportar");
	    req.getSession().setAttribute("listaBeanExportar", listaExport);

	}
	/**
	 * Metodo TotalInterbancarias que obtiene el total de cuentas interbancarias
	 * @param req request con los parametros para la consulta
	 * @return resultado total de Cuentas Interbancarias
	 */
	private int TotalInterbancarias(HttpServletRequest req)
	{
			int resultado = 0;
			final HttpSession sess = req.getSession();
			final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
			final ConsultaCtasBO InterbancariasBO =  new ConsultaCtasBO();
			final CuentaInterbancariaBean conCtasInterbancariasBean = new CuentaInterbancariaBean();
			conCtasInterbancariasBean.setContrato(session.getContractNumber());
			conCtasInterbancariasBean.setRegIni(0);
			conCtasInterbancariasBean.setRegFin(Integer.MAX_VALUE);
			conCtasInterbancariasBean.setCriterio((req.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
			conCtasInterbancariasBean.setTextoBuscar((req.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
			resultado = InterbancariasBO.consultaCtasInterbancariasTotal(conCtasInterbancariasBean);
			return resultado;

     }
	/**
	 * Metodo TotalInternacionales que obtiene el total de cuentas internacionales
	 * @param req request con los parametros para la consulta
	 * @return resultado total de Cuentas Internacionales
	 */
	private int TotalInternacionales(HttpServletRequest req)
	{
			int resultado = 0;
			final HttpSession sess = req.getSession();
			final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

			final ConsultaCtasBO InterbancariasBO =  new ConsultaCtasBO();
			final CuentaInternacionalBean conCtasInternacionalBean = new CuentaInternacionalBean();
			conCtasInternacionalBean.setContrato(session.getContractNumber());
			conCtasInternacionalBean.setRegIni(0);
			conCtasInternacionalBean.setRegFin(Integer.MAX_VALUE);
			conCtasInternacionalBean.setCriterio((req.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
			conCtasInternacionalBean.setTextoBuscar((req.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
			resultado = InterbancariasBO.consultaCtasInternacionalesTotal(conCtasInternacionalBean);
			return resultado;

     }
	/**
	 * Metodo TotalSantander que obtiene el total de cuentas mismo banco (Santander)
	 * @param req request con los parametros para la consulta
	 * @return resultado total de Cuentas mismo banco (Santander)
	 */
	private int TotalSantander(HttpServletRequest req)
	{
			int resultado = 0;
			final HttpSession sess = req.getSession();
			final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

			final ConsultaCtasBO InterbancariasBO =  new ConsultaCtasBO();
			final CuentasSantanderBean conCtasSantanderBean = new CuentasSantanderBean();
			conCtasSantanderBean.setContrato(session.getContractNumber());
			conCtasSantanderBean.setRegIni(0);
			conCtasSantanderBean.setRegFin(Integer.MAX_VALUE);
			conCtasSantanderBean.setCriterio((req.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
			conCtasSantanderBean.setTextoBuscar((req.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
			resultado = InterbancariasBO.consultaCtasSantanderTotal(conCtasSantanderBean);
			return resultado;

     }
	/**
	 * Metodo TotalMoviles que obtiene el total de cuentas moviles
	 * @param req request con los parametros para la consulta
	 * @return resultado total de Cuentas moviles
	 */
	private int TotalMoviles(HttpServletRequest req)
	{
			int resultado = 0;
			final HttpSession sess = req.getSession();
			final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

			final ConsultaCtasBO InterbancariasBO =  new ConsultaCtasBO();
			final ConsultaCtasMovilesBean conCtasMovilBean = new ConsultaCtasMovilesBean();
			conCtasMovilBean.setContrato(session.getContractNumber());
			conCtasMovilBean.setRegIni(0);
			conCtasMovilBean.setRegFin(Integer.MAX_VALUE);
			conCtasMovilBean.setCriterio((req.getParameter(SESATTR_RB_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_RB_CRIT_BUSQ) : "" );
			conCtasMovilBean.setTextoBuscar((req.getParameter(SESATTR_TXT_CRIT_BUSQ) != null) ? req.getParameter(SESATTR_TXT_CRIT_BUSQ).toUpperCase() : "" );
			resultado = InterbancariasBO.consultaCtasMovilTotal(conCtasMovilBean);
			return resultado;

     }

	/**
 	 * Metodo que extrae las cuentas a presentar en pantalla, mismas que pueden ser cuentas Santander, Interbancarias y Otros Bancos
 	 * @param inicio, inicio de los registros contemplados por pagina
 	 * @param fin , fin de  los registros contemplados por pagina
 	 * @param listaCuentas lista de cuentas a presentar de acuerdo a la p�gina
 	 * @param tipoCta tipo de cuenta a presenta
 	 * @return String arrcuentas Regresa relaci�n de cuentas  a presentar en pantalla
 	 */
 	public String[][] MapeaCuentasExportarcion(List<String> listaCuentas,String tipoCta) {
 		EIGlobal.mensajePorTrace("***Entrando a  MapeaCuentasExportarcion - tipo ["+tipoCta+"]", NivelLog.INFO);
 		String[][] arrcuentas =null;
 		String datos[] =null;
 		int separadores=0;
 		int indice=1;
 		int contador=0;
 		EIGlobal.mensajePorTrace("***Entrando a  MapeaCuentasExportarcion - listaCuentas.size() ["+listaCuentas.size()+"]", NivelLog.INFO);
 		arrcuentas = new String[listaCuentas.size()+1][9];
 		arrcuentas[0][1] = "NUM_CUENTA";
 		arrcuentas[0][2] = "TIPO_RELAC_CTAS";
 		arrcuentas[0][3] = "TIPO_CUENTA";
 		arrcuentas[0][4] = "NOMBRE_TITULAR";
 		arrcuentas[0][5] = "CUENTA_HOGAN";
 		arrcuentas[0][6] = "FM";
 		arrcuentas[0][7] = "CVE_PRODUCTO_S";
 		arrcuentas[0][8] = "CVE_SUBPRODUCTO_S";

 		try {
 			for (String linea : listaCuentas ) {
 				separadores=cuentaseparadores(linea);
 				datos = desentramaC(""+separadores+"@"+linea,'@');
 				EIGlobal.mensajePorTrace("linea -> "+linea, NivelLog.DEBUG);
 				String xx="";
 				for (int x=0;x<datos.length;x++){
 					xx=xx.concat("datos["+x+"]=["+datos[x]+"] ");
 				}
 				EIGlobal.mensajePorTrace("datos["+xx+"] ", NivelLog.DEBUG);

					EIGlobal.mensajePorTrace("Separadores ["+separadores+"]", NivelLog.DEBUG);
					if(separadores!=5) {
						arrcuentas[indice][0]="8";
						arrcuentas[indice][1]=datos[1]; //numero cta
						EIGlobal.mensajePorTrace("**1 numero_cta -> "+datos[1] , EIGlobal.NivelLog.INFO);
						arrcuentas[indice][2]=datos[8]; //propia o de terceros
						EIGlobal.mensajePorTrace("**8 propia-terc-> "+datos[8] , NivelLog.INFO);
						arrcuentas[indice][3]=datos[7]; //tipo cta
						EIGlobal.mensajePorTrace("**7 tipo de cta-> "+datos[7] , NivelLog.INFO);
						if (CADENA_NINGUNA.equals(datos[2])) {
							EIGlobal.mensajePorTrace("**3 descricpion-> "+datos[3] , EIGlobal.NivelLog.INFO);
							arrcuentas[indice][4]=datos[3]; //descripcion
						} else {
							EIGlobal.mensajePorTrace("**2 descricpion-> "+datos[2] , EIGlobal.NivelLog.INFO);
							arrcuentas[indice][4]=datos[2]+"  "+datos[3];
						}
						EIGlobal.mensajePorTrace("**3  FM"+datos[3] , NivelLog.INFO);
						if (CADENA_NINGUNA.equals(datos[2])) {
							arrcuentas[indice][5]="";
						} else {
							arrcuentas[indice][5]=datos[2]; //cuenta hogan
						}
						EIGlobal.mensajePorTrace("**2 CTA HOGAN ->"+datos[2] , NivelLog.INFO);
						arrcuentas[indice][6]=datos[6]; //FM
						EIGlobal.mensajePorTrace("**6  FM ->"+datos[6] , NivelLog.INFO);
						arrcuentas[indice][7]=datos[4]; //FM
						EIGlobal.mensajePorTrace("**4  PRODUCTO -> "+datos[4] , NivelLog.INFO);
						arrcuentas[indice][8]=datos[5];
						EIGlobal.mensajePorTrace("**5 SUBPRODUCTO -> "+datos[5] , NivelLog.INFO);
					}
					else {		//interbancarios
						EIGlobal.mensajePorTrace("INTERBANCARIAS :::::::: Asigna 1 a 5 respectivamente ", EIGlobal.NivelLog.DEBUG);
						arrcuentas[indice][1]=datos[1];
						arrcuentas[indice][2]=datos[2];
						arrcuentas[indice][3]=datos[3];
						arrcuentas[indice][4]=datos[4];
						arrcuentas[indice][5]=datos[5];
					}
					indice++;

				contador++;
 			}  //while
 		} catch ( Exception e ) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
 		}
 		indice--;
 		arrcuentas[0][0] = CADENA_VACIA+indice;
 		EIGlobal.mensajePorTrace("**Sali del arrcuentas "+arrcuentas[0][0], NivelLog.INFO);
 		return arrcuentas;
 	}
}// de la clase