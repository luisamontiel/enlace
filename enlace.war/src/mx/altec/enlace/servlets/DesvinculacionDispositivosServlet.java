package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.RSABeanAUX;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.RSAFII;
import mx.altec.enlace.utilerias.UtilidadesRSA;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;
import com.passmarksecurity.PassMarkDeviceSupportLite;

/**
 * Servlet implementation class CambioImagenServlet
 */
/**
 * @author asanjuan
 *
 */
public class DesvinculacionDispositivosServlet extends BaseServlet {

	
	/** serialVersionUID**/
	private static final long serialVersionUID = 1L;

	/**respuesta**/
	private static final String RESPUESTA = "respuesta"; 
	/**session**/
	private static final String SESSIONS = "session";
	/**fail**/
	private static final String FAIL = "fail";

	 /** {@inheritDoc} */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cargaDatosPagina(request, response);
	}

	 /** {@inheritDoc} */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace("--------->  DesvinculacionDispositivosServlet -- doPost", EIGlobal.NivelLog.DEBUG );
		
		HttpSession sess = request.getSession();
		String valida = request.getParameter("valida");
		
		boolean sesionvalida = SesionValida( request, response );
		
		if(Global.VALIDA_RSA) {
			if ( !sesionvalida ) {
				return;
			}
			
			if (valida == null) {
				valida = validaToken(request, response);
			}
	
			if (valida != null && "1".equals(valida)) {
			
				String devicePrint = request.getParameter("devicePrinter");
				request.getSession().setAttribute("valorDeviceP", devicePrint ); 
		
				Map<String, String> mapa = new HashMap<String, String>();
				mapa = ejecutaRSA(request, response);
				boolean validaRSA = Boolean.valueOf(mapa.get("ValidaRSA").toString());
				String respuesta = mapa.get(RESPUESTA);
				
				if(validaRSA) {
				    if("SUCCESS".equals(respuesta)) {
				    	
				    	mandaCorreo(request);
				    	bitacorizaProceso(request, sess);
				    	
				    	request.setAttribute(RESPUESTA, "exito");
				    	request.setAttribute("cerrarSesion", "true");
				    }		
				    
				    UtilidadesRSA.crearCookie(response, mapa.get("deviceTokenCookie"));
					request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, mapa.get("deviceTokenFSO"));
					
				} else {
					request.setAttribute(RESPUESTA, FAIL);
				}
			}
		} else {
			request.setAttribute(RESPUESTA, FAIL);
		}
		cargaDatosPagina (request, response);
	}
	
	/**
	 * Carga los datos de la pagina
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	private void cargaDatosPagina (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);

		request.setAttribute("opcion", request.getParameter("opcion"));
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Desvinculaci&oacute;n de Dispositivos","Administraci&oacute;n y Control &gt; Mant. de Datos Personales", "RSADesv01", request));
		
		if(!Global.VALIDA_RSA) {
			request.setAttribute(RESPUESTA, FAIL);
		}
		
		evalTemplate("/jsp/paginaDesvinculacion.jsp", request, response);
	}
	
	/**
	 * Validar Token
	 * @param request : request
	 * @param response : response
	 * @return String
	 * @throws IOException : exception
	 */
	@SuppressWarnings("static-access")
	private String validaToken(HttpServletRequest request, HttpServletResponse response) 
		throws IOException{
		EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.DESVINCULACION_DISP);
		//CSA-Vulnerabilidad  Token Bloqueado-Inicio
        int estatusToken = obtenerEstatusToken(session.getToken());
		
		if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && 
				estatusToken == 1 && solVal) {
			EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
			
			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request, "DesvinculacionDispositivosServlet");
			ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
		}else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && 
				estatusToken == 2 && solVal){                        
        	cierraSesionTokenBloqueado(session,request,response);
		}
		//CSA-Vulnerabilidad  Token Bloqueado-Fin 
		else {
			return "1";
		}
		return "";
		
	}
	
	/**
	 * @param request : request
	 */
	private void mandaCorreo(HttpServletRequest request) {
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);
		EmailSender sender = new EmailSender();
		EmailDetails details = new EmailDetails();	
		details.setNumeroContrato(session.getContractNumber());
		details.setRazonSocial(session.getNombreContrato());
		details.setUsuario(session.getUserID8());
		
		sender.sendNotificacion(request,IEnlace.DESVINCULACION_DISP, details);
	}
	
	/**
	 * @param request : request
	 * @param sess : sess
	 */
	private void bitacorizaProceso(HttpServletRequest request, HttpSession sess) {
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			BaseResource session = (BaseResource) sess.getAttribute(SESSIONS);
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			if (request.getParameter(BitaConstants.FLUJO) != null) {
				bh.incrementaFolioFlujo(
					request.getParameter(BitaConstants.FLUJO).toString());
			}else{
				request.getSession().getAttribute((BitaConstants.SESS_ID_FLUJO).toString());
			}
			
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setIdFlujo(BitaConstants.EA_DESV_DISP.substring(0,4));
			bt.setNumBit(BitaConstants.EA_DESV_DISP);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}	

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("LoginServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("LoginServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}
	
	/**
	 * @param request : request
	 * @param response : request
	 * @return Map<String, String> mapa de resultados
	 */
	private Map<String, String> ejecutaRSA(HttpServletRequest request, HttpServletResponse response) {
		ServiciosAAResponse res = new ServiciosAAResponse();
		RSABeanAUX rsaBeanAUX = new RSABeanAUX();
		RSABean rsaBean = new RSABean();
		UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
		Map<String, String> mapa = new HashMap<String, String>();
		boolean validaRSA = true;
		
		rsaBean = utilidadesRSA.generaBean(request, "", "", request.getHeader("IV-USER"));
		rsaBeanAUX.setRsaBean(rsaBean);
		rsaBeanAUX.setValorMetodo(RSAFII.valorMetodo.DESVINCULACION);
		RSAFII rsa = new RSAFII();
		String deviceTokenFSO = "";
		String deviceTokenCookie = "";
		String respuesta = "";
		
		try {
			
			res = (ServiciosAAResponse) rsa.ejecutaMetodosRSA7(rsaBeanAUX);
			if(res.getDeviceResult().getDeviceData() != null && res.getDeviceManagementResponse().getCallStatus() != null) {
				deviceTokenFSO = res.getDeviceResult().getDeviceData().getDeviceTokenFSO();
		    	deviceTokenCookie = res.getDeviceResult().getDeviceData().getDeviceTokenCookie();
		    	respuesta = res.getDeviceManagementResponse().getCallStatus().getStatusCode();
			} else {
				validaRSA = false;
			}
		} catch (ExecutionException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
			validaRSA = false;
		} catch (InterruptedException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);	
			validaRSA = false;
		}
		
		mapa.put("ValidaRSA", String.valueOf(validaRSA));
		mapa.put(RESPUESTA, respuesta);
		mapa.put("deviceTokenFSO", deviceTokenFSO);
		mapa.put("deviceTokenCookie", deviceTokenCookie);
		
		return mapa;
		
	}

}
