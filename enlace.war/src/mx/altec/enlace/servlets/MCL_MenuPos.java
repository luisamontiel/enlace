package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;

import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.CuentasBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.io.*;
//04/01/07



/**<B><I><code>MCL_MenuPos</code></I></B>
* <P>Modificaciones:
* 01/10/2002 Se cambiaron vocales acentuadas por su c&oacute;digo de escape.<BR>
* </P>
*/
public class MCL_MenuPos extends BaseServlet
	{

		String pipe="|";

	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	/**
	 * @param req
	 * @param res
	 * @throws ServletException
	 * @throws IOException
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		String strTmp      = "";
		StringBuffer cadenaTCT   = new StringBuffer("");
		String cadenaFac   = "";
		String contrato    = "";
		String usuario     = "";
		String usuario7    = "";
		String clavePerfil = "";

		String tipoRelacion="P";

		String tipoModulo	="";
		String tipoCuenta	="";
		String strTipo		="";
		String strMov		="";
		String titulo		="";
		String arcAyuda		="";

		
		
		boolean existenCuentas=false;
      
		EI_Tipo Saldo=new EI_Tipo(this);
		EI_Tipo TCTArchivo=new EI_Tipo(this);
		EI_Tipo FacArchivo=new EI_Tipo(this);

		EIGlobal.mensajePorTrace("MCL_MenuPos - execute(): Entrando a Posicion - Movimientos (menu).", EIGlobal.NivelLog.INFO);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

		tipoModulo = req.getParameter("Tipo");
		tipoCuenta = req.getParameter("Cuenta");

		if(tipoModulo.equals("Movimientos"))
			strTipo="Movimientos";
		else
			strTipo="Posici&oacute;n";

		if(tipoCuenta.equals("5"))
			{
			strMov="Tarjeta";
			titulo="Consultas &gt; "+strTipo+" &gt; Tarjeta de Cr&eacute;dito";
			if(tipoModulo.equals("Movimientos"))
				arcAyuda="s25230h"; //Mov Tarjeta
			else
				arcAyuda="s25130h"; //Pos Tarjeta
			}
		else
			{
			strMov="L&iacute;nea";
			titulo="Consultas &gt; "+strTipo+" &gt; L&iacute;nea de Cr&eacute;dito";
			if(tipoModulo.equals("Movimientos"))
				arcAyuda="s26310h"; //Mov Linea
			else
				arcAyuda="s26270h"; //Pos Linea
			}

		boolean sesionvalida=SesionValida( req, res);
		if(sesionvalida)
			{
			//Variables de sesion ...
			contrato=session.getContractNumber();
			usuario=session.getUserID8();
			usuario7=session.getUserID();
			clavePerfil=session.getUserProfile();

			String tipo="";
			String arcLinea="";

			String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario+".amb";
			//########## Cambiar solo para pruebas
			//if(tipoCuenta.equals("0"))
			//String nombreArchivo="/tmp/rada.amb.tmp";

			EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);
			EIGlobal.mensajePorTrace("MCL_MenuPos - execute(): Nombre Archivo."+nombreArchivo, EIGlobal.NivelLog.INFO);

			if(ArcEnt.abreArchivoLectura())
				{
				boolean noError=true;
				do
					{
					arcLinea=ArcEnt.leeLinea();
					if(!arcLinea.equals("ERROR"))
						{
						if(arcLinea.substring(0,5).trim().equals("ERROR")) break;
						// Obtiene cuentas asociadas
												
	
						//if(arcLinea.substring(0,1).equals("2"))
						//cadenaTCT+=arcLinea.substring(2,arcLinea.lastIndexOf(';'))+" @";
	                    
						// Obtiene facultades para cuentas ...
						if(arcLinea.substring(0,1).equals("7"))
							cadenaFac+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
						}
					else
						noError=false;
					}
				while(noError);
				ArcEnt.cierraArchivo();
				
				
				CuentasBO cuentasBo = new CuentasBO();
                EIGlobal.mensajePorTrace("MCL_MenuPos.java -> Consulta las cuentas del contrato :"+ contrato, EIGlobal.NivelLog.INFO);
                List<String> cuentas=cuentasBo.consultaCuentasRel(contrato);
                EIGlobal.mensajePorTrace("MCL_MenuPos.java -> Numero de cuentas obtenidas :"+cuentas.size(), EIGlobal.NivelLog.INFO);
                Iterator<String> cuentasIter = cuentas.iterator();
                while(cuentasIter.hasNext()){
                       arcLinea = cuentasIter.next();
                       EIGlobal.mensajePorTrace("MCL_MenuPos.java ->" + arcLinea , EIGlobal.NivelLog.DEBUG);
                       if(!arcLinea.equals("ERROR"))
                        {
                                    if(arcLinea.substring(0,5).trim().equals("ERROR"))
                                    {
                                           break;
                                    }
                                    if(arcLinea.substring(0,1).equals("2"))
                                    {
                                           cadenaTCT.append(arcLinea.substring(2,arcLinea.lastIndexOf(';'))+" @");
                                         
                                    }
                         }
                         else{
                               break;
                             }
                }
                EIGlobal.mensajePorTrace("MCL_MenuPos.java -> Termino lectura de cuentas. ", EIGlobal.NivelLog.INFO);
				

				// Registros de lineas y tarjetas (5,0)
				TCTArchivo.iniciaObjeto(';','@',cadenaTCT.toString());
				EIGlobal.mensajePorTrace("MCL_MenuPos.java TCTArchivo->" + TCTArchivo, EIGlobal.NivelLog.INFO);
				// Registros de facultades (7)
				FacArchivo.iniciaObjeto(';','@',cadenaFac);
				EIGlobal.mensajePorTrace("MCL_MenuPos.java FacArchivo->" + FacArchivo, EIGlobal.NivelLog.INFO);
				
				EIGlobal.mensajePorTrace("MCL_MenuPos.java -> Termino lectura de cuentas. ", EIGlobal.NivelLog.INFO);
				// Si existen cuentas asociadas
				if(TCTArchivo.totalRegistros>=1)
					{
					for(int i=0;i<TCTArchivo.totalRegistros;i++)
						{
						if(TCTArchivo.camposTabla[i][3].equals(tipoCuenta.trim()))
							{
							if(TCTArchivo.camposTabla[i][3].equals("5"))
								{
								// Tarjetas de Cr�dito
								// Revisar facultades para cuenta[i] - Tarjeta.
								tipoRelacion=TCTArchivo.camposTabla[i][2];
								if( (TCTArchivo.camposTabla[i][2].trim().equals("P") && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSPOSIPROP",req)) ||
									(TCTArchivo.camposTabla[i][2].trim().equals("T") && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSPOSITER",req)) )
									{
									existenCuentas=true;
									TCTArchivo.camposTabla[i][4]="TARJETA CREDITO";
									for(int a=0;a<6;a++)
										{
										strTmp+=TCTArchivo.camposTabla[i][a];
										strTmp+=pipe;
										}
									strTmp+=" @";
									}
								}
							else
								{
								// Lineas de Cr�dito
								// Revisar facultades para cuenta[i] - Linea.
								tipoRelacion=TCTArchivo.camposTabla[i][2];

								//Modificacion incidencia
								if(tipoRelacion.trim().equals("P"))
									existenCuentas=true;
								if(tieneFacultadNueva(FacArchivo,TCTArchivo.camposTabla[i][0].trim(),"TECREPOSICIOC",req) && tipoRelacion.trim().equals("P") )
									{
									TCTArchivo.camposTabla[i][4]="LINEA CREDITO";
									for(int a=0;a<6;a++)
 										{
										strTmp+=TCTArchivo.camposTabla[i][a];
										strTmp+=pipe;
										}
									strTmp+=" @";
									}
								}
							}
						}
					}
				Saldo.iniciaObjeto(strTmp);

				generaTablaConsultar(titulo,strMov,strTipo,tipoCuenta,tipoModulo,arcAyuda,TCTArchivo,FacArchivo,Saldo,existenCuentas, req, res );
				}
			else
				despliegaPaginaError("Su transacci&oacute;n no puede ser atendida en este momento.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res);
			}
		else if(sesionvalida)
			{
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, req, res );
			}
		EIGlobal.mensajePorTrace("MCL_MenuPos - execute(): Saliendo de Posicion - Movimientos (menu).", EIGlobal.NivelLog.INFO);
		}

/*************************************************************************************/
/*********************************************************** tabla Consultar         */
/*************************************************************************************/
    public int generaTablaConsultar(String titulo,String strMov,String strTipo,String tipoCuenta,String tipoModulo,String arcAyuda,EI_Tipo TCTArchivo,EI_Tipo FacArchivo,EI_Tipo Saldo,boolean existenCuentas,HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
       String strTabla="";
	   String[] titulos={"",
                         "Cr&eacute;dito",
                         "Descripci&oacute;n",
		                 "Tipo"};

       int[] datos={2,1,0,1,4};
       int[] value={5,0,1,2,3,4,5};

	   boolean lineaCredito=false;
	   boolean facultadPOS=false;
	   boolean facultadMOV=false;

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

       if(Saldo.totalRegistros>=1)
        {
		  //Si es posicion para tarjeta se pueden seleccionar varias
		  //opciones para consultar saldos.
		  if(tipoModulo.equals("Posicion") && tipoCuenta.equals("5"))
		   {
			 datos[1]=4;
			 req.setAttribute("radTabla","<input type=hidden name=radTabla>");
		   }

		  if(tipoCuenta.equals("0"))
			{
			  lineaCredito=true;
			  if(tipoModulo.equals("Posicion"))
			   {
				 if( session.getFacultad("TECREPOSICIOC") )
					facultadPOS=true;
			   }
			  else
			   {
				 if( session.getFacultad("TECREMOVTOSC") )
					facultadMOV=true;
			   }
			}

          strTabla+=Saldo.generaTabla(titulos,datos,value);

		  req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          req.setAttribute("strCuentas",TCTArchivo.strOriginal);
          req.setAttribute("FacArchivo",FacArchivo.strOriginal);
          req.setAttribute("Tabla",strTabla);
          req.setAttribute("FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		  req.setAttribute("Tipo",tipoModulo);

		  req.setAttribute("MenuPrincipal", session.getStrMenu());
		  req.setAttribute("newMenu", session.getFuncionesDeMenu());
		  req.setAttribute("Encabezado",CreaEncabezado("Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda,req));
//		TODO BIT CU1071, CU3061 inicio de flujo
	        /*
			 * 04/ENE/07
			 * 15/Enero/2007	Modificaci�n para el modulo de Tesorer�a
			 * VSWF
			 */
		  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);

				if(req.getParameter(BitaConstants.FLUJO) != null || req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != null){
					String temp = "";
							if(req.getParameter(BitaConstants.FLUJO) != null){

								bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));

								temp=(String)req.getParameter(BitaConstants.FLUJO);
							}else{
								bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
								temp=(String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO);
							}
							if((req.getParameter(BitaConstants.FLUJO)).
									equals(BitaConstants.EC_POSICION_TARJETA)){
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.EC_POSICION_TARJETA_ENTRA);
								bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
								BitaHandler.getInstance().insertBitaTransac(bt);
							}
							else{
								if(req.getParameter(BitaConstants.FLUJO) != null || req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != null){
									if(temp.equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)){
										BitaTransacBean bt = new BitaTransacBean();
										bt = (BitaTransacBean)bh.llenarBean(bt);
										bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_ENTRA);
										bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
										BitaHandler.getInstance().insertBitaTransac(bt);
									}
								}//fin compara si hay null
							}//fin else
				}//fin if exterior
				else{
					log("No hay id de flujo");
					log("Parameter: " + (String)req.getParameter(BitaConstants.FLUJO));
					log("Atribute: " + (String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				}
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
		  }
	        /*
			 * VSWF
			 */
          evalTemplate("/jsp/MCL_MenuPos.jsp", req, res );
        }
       else
        {

		  if(lineaCredito)
			{
			  if(tipoModulo.equals("Posicion"))
				{
				  System.out.println("Posicion de lineas de credito");
			      if( (existenCuentas && facultadPOS) || !facultadPOS)
					 despliegaPaginaError("No tiene facultad.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res );
				  else
					despliegaPaginaError("No se encontraron "+strMov+"s de Cr&eacute;dito Asociadas.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res );

				}
			  else
				{
				  System.out.println("Movimientos de lineas de credito");
				  if( (existenCuentas && facultadMOV) || !facultadMOV)
					 despliegaPaginaError("No tiene facultad.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res );
				  else
					despliegaPaginaError("No se encontraron "+strMov+"s de Cr&eacute;dito Asociadas.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res );
				}
			}
		  else
			{
			  if(existenCuentas)
				despliegaPaginaError("No tiene facultad.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res );
			  else
				despliegaPaginaError("No se encontraron "+strMov+"s de Cr&eacute;dito Asociadas.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res );
			}
        }
       return 1;
     }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
   boolean tieneFacultad(EI_Tipo FacArchivo,String tipoRelacion,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_MenuPos - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_MenuPos - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
          {
            if(tipoRelacion.trim().equals("P"))
              FacultadAsociadaACuenta=true;
            else
              FacultadAsociadaACuenta=false;
          }
       }
	  EIGlobal.mensajePorTrace("MCL_MenuPos - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }

    boolean facultadAsociada(EI_Tipo FacArchivo,String credito, String facultad, String signo)
     {
      /*
       0 7
       1 Clave Fac
       2 Cuenta
       3 signo
      */

       if(FacArchivo.totalRegistros>=1)
        for(int i=0;i<FacArchivo.totalRegistros;i++)
         {
           if(credito.trim().equals(FacArchivo.camposTabla[i][2].trim()))
            if(FacArchivo.camposTabla[i][1].trim().equals(facultad))
             if(FacArchivo.camposTabla[i][3].trim().equals(signo))
               return true;
         }
       EIGlobal.mensajePorTrace("MCL_MenuPos - facultadAsociada(): Facultad asociada regresa false", EIGlobal.NivelLog.INFO);
       return false;
     }


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/

   boolean tieneFacultadNueva(EI_Tipo FacArchivo,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_MenuPos - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_MenuPos - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
		   FacultadAsociadaACuenta=false;
       }
	  EIGlobal.mensajePorTrace("MCL_MenuPos - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }
}