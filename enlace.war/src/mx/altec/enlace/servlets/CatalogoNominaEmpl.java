/*************************************************************************************************/
/** Cat�logoN�minaEmpl - Clase Principal para la administraci�n del nuevo Cat�logo de N�mina  	**/
 /** 	15/oct/2007  																			**/
 /** Nuevo Alcance																				**/
 /** 	17 - Enero - 2008																		**/
 /** EVERIS MEXICO																				**/
 /************************************************************************************************/

package	mx.altec.enlace.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.CatNominaDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.bo.CatNominaAction;
import mx.altec.enlace.bo.CatNominaBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.CatNominaConstantes;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
/*Inicia - EVERIS VALIDACION CSS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION CSS */


/** 
*   Isban M�xico
*   Clase: CatalogoNominaEmpl.java
*   Descripci�n: Servlet utilizado en la funcionalidad de Catalogo Nomina.
*
*   Control de Cambios:
*   1.0 16/10/2017 Everis - Limpieza Cross Site Scripting
*/
public class CatalogoNominaEmpl extends BaseServlet {
	/** Constante para encabezado de servicio de nomina */
	public static final String ENC_SERV_NOM = "Servicios > N&oacute;mina > Empleados > ";
	boolean existReg = true;
	ArrayList listaModifFinal = new ArrayList();
	ArrayList listaBajaFinal = new ArrayList();
	/* everis JARA - Ambientacion - Cambio de ruta de subida a /tmp/ */
	String rutadeSubida=Global.DIRECTORIO_LOCAL;

	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		defaultAction(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		defaultAction(request, response);
	}

	/***************************************************************
	 *** M�todo encargado de realizar los llamados a las 		****
	 *** funciones de Busqueda, Baja y Eliminaci�n de Empleados ****
	 *** del nuevo Cat�logo de N�mina							****
	 **************************************************************/
	public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try{
			/* everis JARA - Ambientacion - */
			boolean bandermalicioso = true;
			String numContrato="";
			String usuario="";
			String clavePerfil="";
			String modulo="";
			boolean bander=true;

			HttpSession sessionHttp = request.getSession();
			/* everis JARA - Ambientacion - Obtencion de session del BaseResource */
			BaseResource session = (BaseResource) sessionHttp.getAttribute("session");
			boolean sesionvalida=SesionValida( request, response );

			if(sesionvalida)
			{
				/* everis JARA - Ambientacion - Obtencion de variables de session */
				numContrato=(session.getContractNumber()==null)?"":session.getContractNumber();
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): contrato: "+numContrato,EIGlobal.NivelLog.INFO);
				usuario=(session.getUserID8()==null)?"":session.getUserID8();
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): usuario: "+usuario,EIGlobal.NivelLog.INFO);
				clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): clavePerfil: "+clavePerfil,EIGlobal.NivelLog.INFO);
				modulo=(String) request.getParameter("Modulo");
				EIGlobal.mensajePorTrace("CatalogoNominaEmpl - defaultAction(): el modulo es: "+modulo ,EIGlobal.NivelLog.INFO);

				/* everis JARA - Ambientacion - Se carga los atributos de men princiapal ******/
				try {
					request.setAttribute ("MenuPrincipal", session.getStrMenu ());
					request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
				}
				catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				/* everis JARA - Ambientacion -  Se carga los atributos de men princiapal ******/

				String opcion = request.getParameter("opcion");

				// Si es la primera vez se carga la pagina por default
				if(opcion == null)
					opcion = "1";




				/***********************************************************
				 *** opcion 1 = Cargar p�gina de busqueda de Empleados.  ***
				 **********************************************************/
				if(opcion.equals("1"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Cat&aacute;logo de N&oacute;mina > B&uacute;squeda", "s37100h", request ) );


					/* everis JARA - Ambientacion -  Redireccionando a jsp */
					evalTemplate("/jsp/CatNominaBusqueda.jsp", request, response );

				}



				/**************************************************
				 *** Opcion 2 = Realizar Busqueda de Empleados  ***
				 **************************************************/
				else if(opcion.equals("2"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Cat&aacute;logo de N&oacute;mina > B&uacute;squeda", "s37100h", request ) );


					CatNominaAction action = new CatNominaAction();
					CatNominaDAO dao = new CatNominaDAO();
					/*Inicia - EVERIS VALIDACION CSS */
					EIGlobal.mensajePorTrace("Validar CSS para el parametro [valorBusqueda]", EIGlobal.NivelLog.INFO);
					String valorBusqueda = UtilidadesEnlaceOwasp.cleanString(request.getParameter("valorBusqueda"));
					/*Finaliza - EVERIS VALIDACION CSS */
					String radioValue = request.getParameter("radioValue");

					String tablaEmpleados = "";
					/* jp@everis */
					int numeroPagina = 0;
					int numeroPaginas = 0;
					int inicioBloque = 0;
					int finBloque = 0;
					int totalReg = 0;
					int tamanoBloque = 15; //Tamano del bloque de presentaci�n es 15
					String pagReg[] = null;
					String[][] arrDatosQuery = null;
					boolean errorExcepcion = false; 		// Verifica Errores por Manejo de Excepciones
					String archivoCatNomina = ""; //VC ESC crear Archivo
					String archivoCatNominaTabs = "";

					if( sessionHttp.getAttribute("arrDatosQuery") == null || request.getParameter("numeroPagina") == null)
					{
						/* Invoca metodo para obtener numero de paginas */

						pagReg =  action.consultaPaginas(numContrato, radioValue, valorBusqueda, tamanoBloque);

						if(!pagReg[0].equals("ERROR0000"))
						{
							numeroPaginas = Integer.parseInt(pagReg[0]);
							totalReg = Integer.parseInt(pagReg[1]);
							if( numeroPaginas != 0 ){
								numeroPagina = 1;
								finBloque = inicioBloque + tamanoBloque;
							}else{
								finBloque = totalReg;
							}
							request.setAttribute("numeroPagina", String.valueOf(numeroPagina));
							request.setAttribute("numeroPaginas", String.valueOf(numeroPaginas));
							request.setAttribute("inicioBloque", String.valueOf(inicioBloque));
							request.setAttribute("finBloque", String.valueOf(finBloque));
							request.setAttribute("tamanoBloque", String.valueOf(tamanoBloque));
							request.setAttribute("totalReg", String.valueOf(totalReg));

							/* Invoca metodo para obtener el arreglo general de los empleados */
							arrDatosQuery = dao.buscaEmpleado(numContrato, radioValue, valorBusqueda, totalReg);

							if(!arrDatosQuery[0][0].equals("ERROR0000"))
							{
								//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
 								try{
								       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT BUSQUEDA EMPLEADO",EIGlobal.NivelLog.INFO);
									  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									  	if(referencia>0){
										EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
									  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
									  	BitaTCTBean bean = new BitaTCTBean ();
									  	bean.setReferencia(referencia);
									  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_BUSQUEDA_EMPL);
									  	bean.setConcepto(BitaConstants.ES_NOMINA_BUSQUEDA_EMPL_MSG);
									  	bean = bh.llenarBeanTCT(bean);
									    BitaHandler.getInstance().insertBitaTCT(bean);
									  	}
						    		}catch(SQLException e){
						    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						    		}catch(Exception e){
						    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						    		}
								EIGlobal.mensajePorTrace("TERMINA BITACORIZACION TCT BUSQUEDA EMPLEADO",EIGlobal.NivelLog.INFO);
								//</VC>
//								//VC ESC crear Archivo
								archivoCatNomina = session.getUserID8()+ CatNominaConstantes._ARCHIVO_NOMINA;
								archivoCatNominaTabs = session.getUserID8()+ CatNominaConstantes._ARCHIVO_NOMINA_EXPORTAR;

								if(!arrDatosQuery[0][0].equals(CatNominaConstantes._ERROR))
								{
									crearArchivoSeparadoCaracter(arrDatosQuery, archivoCatNominaTabs);
									if(crearArchivo(arrDatosQuery,archivoCatNomina) != null)
										sessionHttp.setAttribute("archivoCatNomina",archivoCatNomina);
								}
								// /VC




								sessionHttp.setAttribute("arrDatosQuery", arrDatosQuery);

								tablaEmpleados = action.consultaEmpleados(
										arrDatosQuery,
										inicioBloque,
										finBloque,
										tamanoBloque,
										totalReg,
										numeroPagina,
										numeroPaginas);
								/*jp@everis*/

								// Exportar archivo a TXT separado por | o CVS
								// ENLACE PYME - Marzo 2015

								 ExportModel em = new ExportModel("sepfileexp", '|', true)
								 .addColumn(new Column(0, 6, "N�mero de Empleado"))
								 .addColumn(new Column(7,12, "N�mero de Departamento"))
								 .addColumn(new Column(13, 42, "Apellido Paterno"))
								 .addColumn(new Column(43, 72,  "Apellido Materno"))
								 .addColumn(new Column(73, 102, "Nombre"))
								 .addColumn(new Column(103, 120, "N�mero de Cuenta"))
								 .addColumn(new Column(121, 136, "Banco"))
								 .addColumn(new Column(137, 149, "RFC"))
								 .addColumn(new Column(150, 170,"Estado"))
								 .addColumn(new Column(171, 186,"Ingreso Mensual", "$####################0.00"));



								 sessionHttp.setAttribute("ExportModel", em);

								sessionHttp.setAttribute("af", Global.DIRECTORIO_REMOTO_WEB.concat("/").concat(archivoCatNomina));
								// Fin Exportar archivo a TXT separado por | o CVS
								// ENLACE PYME - Marzo 2015

								if(tablaEmpleados.equals("")){

									request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La b&uacute;squeda configurada, no gener&oacute resultados<br> Intente con otros valores\", 1);");
									tablaEmpleados = null;
								}
								request.setAttribute("codigoTabla", tablaEmpleados);
								request.setAttribute("valorBusqueda", valorBusqueda);
								request.setAttribute("radioValue", radioValue);
								try{
									EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
				    				String claveOperacion = BitaConstants.CONSULTA_CAT_NOMINA_MISMO_BANCO;
				    				String concepto = BitaConstants.CONCEPTO_CONSULTA_CAT_NOMINA_MISMO_BANCO;

				    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
				    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

				    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

								} catch(Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							} // If Manejo 2
							else {
								generaPantallaError(request, response, "Error inesperado. Fallo en la operaci&oacute;n con la Base de Datos. <br> Intente m&aacute;s tarde", "B&uacute;squeda");
								errorExcepcion = true;
							}
						} //if Manejo 1
						else{
							generaPantallaError(request, response, "Error inesperado. Fallo en la operaci&oacute;n con la Base de Datos. <br> Intente m&aacute;s tarde", "B&uacute;squeda");
							errorExcepcion = true;
						}
					}
					else
					{
						numeroPagina = Integer.parseInt(request.getParameter("numeroPagina").toString());
						numeroPaginas = Integer.parseInt(request.getParameter("numeroPaginas").toString());
						inicioBloque =  Integer.parseInt(request.getParameter("inicioBloque").toString());
						finBloque = Integer.parseInt(request.getParameter("finBloque").toString());
						totalReg = Integer.parseInt(request.getParameter("totalReg").toString());
						arrDatosQuery = (String[][])sessionHttp.getAttribute("arrDatosQuery");
						/* Invoca metodo para obtener el arreglo general de los empleados */
						tablaEmpleados = action.consultaEmpleados(
								arrDatosQuery,
								inicioBloque,
								finBloque,
								tamanoBloque,
								totalReg,
								numeroPagina,
								numeroPaginas);
						if(tablaEmpleados.equals("")){

							request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La b&uacute;squeda configurada, no gener&oacute resultados<br> Intente con otros valores\", 1);");
							tablaEmpleados = null;
						}
						request.setAttribute("codigoTabla", tablaEmpleados);
						request.setAttribute("valorBusqueda", valorBusqueda);
						request.setAttribute("radioValue", radioValue);
						request.setAttribute("numeroPagina", String.valueOf(numeroPagina));
						request.setAttribute("numeroPaginas", String.valueOf(numeroPaginas));
						request.setAttribute("inicioBloque", String.valueOf(inicioBloque));
						request.setAttribute("finBloque", String.valueOf(finBloque));
						request.setAttribute("tamanoBloque", String.valueOf(tamanoBloque));
						request.setAttribute("totalReg", String.valueOf(totalReg));
					}
					if(!errorExcepcion)
					{
						existReg = true;
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaBusqueda.jsp", request, response );
					}


				}



				/**************************************************
				 *** Opcion 3 = Abrir Modificaci�n de Empleados  ***
				 **************************************************/
				else if(opcion.equals("3"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Cat&aacute;logo de N&oacute;mina > B&uacute;squeda > Modificaci&oacute;n en L&iacute;nea", "s37110h", request ) );


					CatNominaAction action = new CatNominaAction();
					boolean varReturn = true;

					String tipoCuenta = (request.getParameter("cuentaSelec")).substring(0,6);
					String numCuenta = (request.getParameter("cuentaSelec")).substring(6);

					varReturn = action.consultaDatosEmpleado(numContrato, numCuenta, request, sessionHttp, tipoCuenta);

					if(varReturn){
						request.setAttribute("numeroCta", numCuenta);
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaModificaOnline.jsp", request, response );
					}
					else
						generaPantallaError(request, response, "Fallo al momento de obtener informaci&oacute;n desde la Base de Datos. Por favor intente m&aacute;s tarde", "Modificaci&oacuten en L&iacute;nea");


				}



				/******************************************************
				 *** Opcion 4 = Realizar Modificaci�n de Empleados  ***
				 ******************************************************/
				else if(opcion.equals("4"))
				{
					EIGlobal.mensajePorTrace("�������������� Entra a CatalogoNominaEmpleado con opcion 4 Modificacion de Empleados", EIGlobal.NivelLog.INFO);

					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Cat&aacute;logo de N&oacute;mina > B&uacute;squeda > Modificaci&oacute;n en L&iacute;nea", "s37110h", request ) );


					CatNominaAction actionModif = new CatNominaAction();
					boolean varModifica = true;

					String numCuenta = request.getParameter("CuentaA");


					/*****************************************
					 * Inicio Funcionalidad de TOKEN
					 * **************************************/

			        //interrumpe la transaccion para invocar la validaci�n para Alta de proveedor

			       /* if(  valida==null) {
			            EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

			            boolean solVal=ValidaOTP.solicitaValidacion(
			                    session.getContractNumber(),IEnlace.NOMINA);

			            if( session.getValidarToken() &&
			                    session.getFacultad(session.FAC_VAL_OTP) &&
			                    session.getToken().getStatus() == 1 &&
			                    solVal) {
			                EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
			                //ValidaOTP.guardaParametrosEnSession(req);
			                guardaParametrosEnSession(req);
			                ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
			            } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
			                valida="1";
						}
			        }
			        // retoma el flujo
			        if( valida!=null && valida.equals("1")) {*/

					varModifica = actionModif.modificaEmpleado(numCuenta, usuario, request);
					EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 4 varModifica ->" + varModifica, EIGlobal.NivelLog.INFO);

					if(varModifica) {
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
						try{
							String tipoCuenta = request.getParameter("rtipoCuenta");
					       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT MODIFICA EMPLEADO ONLINE",EIGlobal.NivelLog.INFO);
						  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
							EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
						  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
						  	BitaTCTBean bean = new BitaTCTBean ();
						  	bean.setReferencia(referencia);

						  	if(CatNominaConstantes._CTA_INTERNA.equals(tipoCuenta))
						  	{
						  		bean.setTipoOperacion(BitaConstants.ES_NOMINA_MOD_EMPL_EMPL_LINEA);
							  	bean.setConcepto(BitaConstants.ES_NOMINA_MOD_EMPL_LINEA_MSG);
						  	}else{
						  		bean.setTipoOperacion(BitaConstants.ES_NOMINA_MOD_EMPL_INTERBAN_LINEA);
							  	bean.setConcepto(BitaConstants.ES_NOMINA_MOD_EMPL_INTERBAN_LINEA_MSG);
						  	}
						  	bean = bh.llenarBeanTCT(bean);
						    BitaHandler.getInstance().insertBitaTCT(bean);
						    EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 4 tipoCuenta ->" + tipoCuenta, EIGlobal.NivelLog.INFO);
						    EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 4 referencia ->" + referencia, EIGlobal.NivelLog.INFO);
						    try {
							    EmailSender emailSender = new EmailSender();
								EmailDetails beanEmailDetails = new EmailDetails();

								EIGlobal.mensajePorTrace("�������������������� Notificacion en CatalogoNominaEmpl modificacion en linea", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("�������������������� Cuenta ->" + numCuenta, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("�������������������� referencia ->" + referencia, EIGlobal.NivelLog.INFO);

								beanEmailDetails.setNumeroContrato(session.getContractNumber());
								beanEmailDetails.setRazonSocial(session.getNombreContrato());
								beanEmailDetails.setNumCuentaCargo(numCuenta);
								beanEmailDetails.setNumRef(""+referencia);
								beanEmailDetails.setEstatusFinal("REALIZADO");
								emailSender.sendNotificacion(request, IEnlace.NOT_MODIF_EMPL_UNI, beanEmailDetails);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
			    		}catch(SQLException e){
			    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			    		}catch(Exception e){
			    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			    		}
					EIGlobal.mensajePorTrace("TERMINA BITACORIZACION TCT MODIFICA EMPLEADO ONLINE",EIGlobal.NivelLog.INFO);
					//</VC>
						evalTemplate("/jsp/CatNominaBusqueda.jsp", request, response );
						log ( "BITACORIZACION" );
						modificaPropiaCN(usuario, numCuenta,  request);
						try{
							EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

							int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		    				String claveOperacion = BitaConstants.MODIFICACION_CAT_NOMINA_MISMO_BANCO;
		    				String concepto = BitaConstants.CONCEPTO_MODIFICACION_CAT_NOMINA_MISMO_BANCO;

		    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
		    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

		    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

						} catch(Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}

					}
					else
						generaPantallaError(request, response, "Fallo en la Modificaci&oacute;n de registros en la base de Datos", "Modificaci&oacute;n en L&iacute;nea");



				//}
					/*****************************************
					* Finaliza Funcionalidad de TOKEN
					/* **************************************/

				}



				/**************************************************
				 *** Opcion 5 = Realizar Baja de Empleados  ***
				 **************************************************/
				else if(opcion.equals("5"))
				{
					EIGlobal.mensajePorTrace("�������������� Entra a CatalogoNominaEmpleado con opcion 5 Baja de Empleados", EIGlobal.NivelLog.INFO);

					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
											"Cat&aacute;logo de N&oacute;mina > B&uacute;squeda", "s37100h", request ) );


					boolean fueBaja = false;
					CatNominaAction action = new CatNominaAction();

					String empleadoBaja = (request.getParameter("cuentaSelec")).substring(6);


					/*****************************************
					 * Inicio Funcionalidad de TOKEN
					 * **************************************/

			        //interrumpe la transaccion para invocar la validaci�n para Alta de proveedor

			       /* if(  valida==null) {
			            EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

			            boolean solVal=ValidaOTP.solicitaValidacion(
			                    session.getContractNumber(),IEnlace.NOMINA);

			            if( session.getValidarToken() &&
			                    session.getFacultad(session.FAC_VAL_OTP) &&
			                    session.getToken().getStatus() == 1 &&
			                    solVal) {
			                EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
			                //ValidaOTP.guardaParametrosEnSession(req);
			                guardaParametrosEnSession(req);
			                ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
			            } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
			                valida="1";
						}
			        }
			        // retoma el flujo
			        if( valida!=null && valida.equals("1")) {*/

						fueBaja = action.bajaCtaEmplOnline(empleadoBaja, numContrato, usuario);

						EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 5 fueBaja ->" + fueBaja, EIGlobal.NivelLog.INFO);

						if(fueBaja)	{

							//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
							try{

								String tipoCuenta = request.getParameter("rtipoCuenta");
						       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT BAJA EMPLEADO ONLINE",EIGlobal.NivelLog.INFO);
							  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean.setReferencia(referencia);
							 	if(CatNominaConstantes._CTA_INTERNA.equals(tipoCuenta)){
							 		bean.setTipoOperacion(BitaConstants.ES_NOMINA_BAJA_EMPL_LINEA);
							 		bean.setConcepto(BitaConstants.ES_NOMINA_BAJA_EMPL_LINEA_MSG);
							  	}
							 	else{
							 		bean.setTipoOperacion(BitaConstants.ES_NOMINA_BAJA_INTERBAN_LINEA);
							 		bean.setConcepto(BitaConstants.ES_NOMINA_BAJA_INTERBAN_LINEA_MSG);
							 	}

							  	bean = bh.llenarBeanTCT(bean);
							    BitaHandler.getInstance().insertBitaTCT(bean);
								EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 5 tipoCuenta ->" + tipoCuenta, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 5 referencia ->" + referencia, EIGlobal.NivelLog.INFO);
								try {
									EmailSender emailSender = new EmailSender();
									EmailDetails beanEmailDetails = new EmailDetails();

									EIGlobal.mensajePorTrace("�������������������� Notificacion en CatalogoNominaEmpl baja en linea", EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("�������������������� Cuenta ->" + empleadoBaja, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("�������������������� referencia ->" + referencia, EIGlobal.NivelLog.INFO);

									beanEmailDetails.setNumeroContrato(session.getContractNumber());
									beanEmailDetails.setRazonSocial(session.getNombreContrato());
									beanEmailDetails.setNumCuentaCargo(empleadoBaja);
									beanEmailDetails.setNumRef(""+referencia);
									beanEmailDetails.setEstatusFinal("REALIZADO");
									emailSender.sendNotificacion(request, IEnlace.NOT_BAJA_EMPL_UNI, beanEmailDetails);
								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
				    		}catch(SQLException e){
				    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				    		}catch(Exception e){
				    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				    		}
				    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION BAJA EMPLEADO ONLINE",EIGlobal.NivelLog.INFO);
				    		//</VC>
							request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La baja del empleado se realiz&oacute <br> exitosamente.\", 1);");
							log ( "BITACORIZACION" );
							modificaPropiaCN(usuario, numContrato,  request);
							/* everis JARA - Ambientacion -  Redireccionando a jsp */
							evalTemplate("/jsp/CatNominaBusqueda.jsp", request, response );
							try{
								EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

								int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
			    				String claveOperacion = claveOperacion = BitaConstants.BAJA_CAT_NOMINA_MISMO_BANCO;
			    				String concepto = BitaConstants.CONCEPTO_BAJA_CAT_NOMINA_MISMO_BANCO;

			    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
			    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

			    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

							} catch(Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}

						}
						else{

							generaPantallaError(request, response, "Fallo en la realizaci&oacute;n de la baja de registro en la Base de Datos. <br> Favor de Intentar m&aacute;s tarde.", "Baja en l&iacute;nea");
						}
					//}
						/*****************************************
						 * Finaliza Funcionalidad de TOKEN
						 * **************************************/



				}



				/********************************************************
				 *  Opcion = 6 - Baja L�gica de Empleados por archivo 	*
				 ********************************************************/
				else if(opcion.equals("6"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Baja de Empleados Cat&aacute;logo de N&oacute;mina", ENC_SERV_NOM +
											"Cat&aacute;logo de N&oacute;mina > Baja por Archivo", "s37130h", request ) );


					/* everis JARA - Ambientacion -  Redireccionando a jsp */
					evalTemplate("/jsp/CatNominaBajaArchivo.jsp", request, response );

				}



				/******************************************************************
				 *  Opcion = 7 - Realiza la lectura del archivo con las cuentas
				 *  			que ser�n dadas de baja del cat�logo de n�mina.
				 *  			Se realizar�n las respectivas validaciones
				 *******************************************************************/
				else if(opcion.equals("7"))
				{
					EIGlobal.mensajePorTrace("�������������� Entra a CatalogoNominaEmpleado con opcion 7 Lectura archivo para baja", EIGlobal.NivelLog.INFO);

					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Baja de Empleados Cat&aacute;logo de N&oacute;mina", ENC_SERV_NOM +
											"Cat&aacute;logo de N&oacute;mina > Baja por Archivo", "s37130h", request ) );


					String tabla = "";
					CatNominaBean bean = new CatNominaBean();
					try {
						CatNominaAction action = new CatNominaAction();
						CatNominaConstantes cons = new CatNominaConstantes();
						getFileInBytes(request);
						String  nombreArchivo= (String)request.getAttribute("Archivo");//varible con el nombre del archivo que se esta mandando a llamar


						if(nombreArchivo!=null)
						{
							ArrayList erroresArchivoModif;
							bean = action.validaArchivoBaja( rutadeSubida+"/" + nombreArchivo, numContrato, request);

							if(!bean.getFalloInesperado())
							{
								erroresArchivoModif = bean.getListaErrores();

								if(erroresArchivoModif.size() == 0)
								{
									listaBajaFinal = bean.getListaRegistros();

									if (bean.getTotalRegistros() <= 10) {
										tabla = action.creaDetalleImport(numContrato, bean.getListaRegistros(), cons._BAJA );
										request.setAttribute("TablaRegistros", tabla);
									}
									else {
										String lineaCant = "";
													lineaCant += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
													lineaCant += "<tr><td>&nbsp;</td></tr>";
													lineaCant += "<tr>";
													lineaCant += "<td class=texfootpagneg align=center bottom=\"middle\">";
													lineaCant += "El total de registros a dar de baja son: " + bean.getTotalRegistros() + " <br><br>";
													lineaCant += "</td>";
													lineaCant += "</tr>";
													lineaCant += "</table>";

										request.setAttribute("LineaCantidad", lineaCant);
									}
								}
								else
								{
									String Linea = "";
									Iterator it = erroresArchivoModif.iterator();
									int  i = 0;
									while (it.hasNext()) {
										if(i==0){
											it.next();
										}
										i++;

										if(erroresArchivoModif.get(0).equals("Long")){

											Linea += "Longitud de Linea Incorrecta: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
										else if(erroresArchivoModif.get(0).equals("Formato")){

											Linea += "Tipo de dato incorrecto: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
										else if(erroresArchivoModif.get(0).equals("Cuenta")){

											Linea += "Cuenta Inexistente: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
									}

									request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>"
											+ Linea + ".\", 1);");
								}
								/* everis JARA - Ambientacion -  Redireccionando a jsp */
								//evalTemplate("/jsp/CatNominaBajaArchivo.jsp", request, response );
								EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 7 TotalRegistros ->" + bean.getTotalRegistros(), EIGlobal.NivelLog.INFO);

							}// Error Inesperado
							else
							{
								generaPantallaError(request, response, "Fallo en la Baja de registros en la Base de Datos. <br>Favor de Intentar mas tarde.", "Baja por Archivo");
							}
						}
					}catch(FileNotFoundException e) {

						EIGlobal.mensajePorTrace("Exception file not found", EIGlobal.NivelLog.INFO, e);
						
						
						bandermalicioso = false;
			        }
					catch(Exception e)
					{
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
					
					if(bandermalicioso){
					if(!bean.getFalloInesperado()){
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaBajaArchivo.jsp", request, response );
					}
					}else{
						despliegaPaginaError("Archivo de Importaci&oacute;n inv&aacute;lido",request, response );
					}

				}

				/******************************************************************
				 *  Opcion 8=  - Da de Baja a un empleado del nuevo cat�logo de
				 *  			n�mina
				 *******************************************************************/
				else if(opcion.equals("8"))
				{
					EIGlobal.mensajePorTrace("�������������� Entra a CatalogoNominaEmpleado con opcion 8 Baja del nuevo catalogo", EIGlobal.NivelLog.INFO);

					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Baja de Empleados Cat&aacute;logo de N&oacute;mina", ENC_SERV_NOM +
											"Cat&aacute;logo de N&oacute;mina > B&uacute;squeda", "s37130h", request ) );


					CatNominaAction action = new CatNominaAction();
					boolean actualizaciones = false;

				/*****************************************
				 * Inicio Funcionalidad de TOKEN
				 * **************************************/

		            //interrumpe la transaccion para invocar la validaci�n para Alta de proveedor

		           /* if(  valida==null) {
		                EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

		                boolean solVal=ValidaOTP.solicitaValidacion(
		                        session.getContractNumber(),IEnlace.NOMINA);

		                if( session.getValidarToken() &&
		                        session.getFacultad(session.FAC_VAL_OTP) &&
		                        session.getToken().getStatus() == 1 &&
		                        solVal) {
		                    EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
		                    //ValidaOTP.guardaParametrosEnSession(req);
		                    guardaParametrosEnSession(req);
		                    ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
		                } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
		                    valida="1";
						}
		            }
		            // retoma el flujo
		            if( valida!=null && valida.equals("1")) {*/
						actualizaciones = action.bajaCtaEmplArchivo(numContrato, usuario, listaBajaFinal);
						EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado opcion 8 actualizaciones ->" + actualizaciones, EIGlobal.NivelLog.INFO);
						if(!actualizaciones) {

							generaPantallaError(request, response, "Fallo en la baja de empleados en la Base de Datos. Favor de Intentar m&aacute;s tarde", "Baja por Archivo");
						}

						else {
							request.setAttribute("MensajeLogin01", " cuadroDialogo(\"Se dieron de baja satisfactoriamente las cuentas.\", 1);");

							log ( "BITACORIZACION" );
							modificaPropiaCN(usuario, numContrato,  request);
							//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
							try{
						       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT BAJA EMPLEADO ARCHIVO",EIGlobal.NivelLog.INFO);
							  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean.setReferencia(referencia);
							  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_BAJA_EMPL_ARCH);
							  	bean.setConcepto(BitaConstants.ES_NOMINA_BAJA_EMPL_ARCH_MSG);
							  	bean = bh.llenarBeanTCT(bean);
							    BitaHandler.getInstance().insertBitaTCT(bean);
								EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 8 referencia ->" + referencia, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado no de cuentas a dar de baja ->" + listaBajaFinal.size(), EIGlobal.NivelLog.INFO);
								try {
									if (listaBajaFinal.size() > 0) {
										EmailSender emailSender = new EmailSender();
										EmailDetails beanEmailDetails = new EmailDetails();

										EIGlobal.mensajePorTrace("�������������������� Notificacion en CatalogoNominaEmpl baja por archivo", EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� no de registros ->" + listaBajaFinal.size(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� referencia ->" + referencia, EIGlobal.NivelLog.INFO);

										beanEmailDetails.setNumeroContrato(session.getContractNumber());
										beanEmailDetails.setRazonSocial(session.getNombreContrato());
										beanEmailDetails.setNumRegImportados(listaBajaFinal.size());
										beanEmailDetails.setNumRef(""+referencia);
										beanEmailDetails.setEstatusFinal("ENVIADO");


											emailSender.sendNotificacion(request, IEnlace.NOT_BAJA_EMPL_MAS, beanEmailDetails);

									}
								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
				    		}catch(SQLException e){
				    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				    		}catch(Exception e){
				    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				    		}
				    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION SALTO BAJA EMPLEADO ARCHIVO",EIGlobal.NivelLog.INFO);
				    		//</VC>
							/* everis JARA - Ambientacion -  Redireccionando a jsp */
							evalTemplate("/jsp/CatNominaBajaArchivo.jsp", request, response );
							try{
								EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

								int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
			    				String claveOperacion = BitaConstants.BAJA_CAT_NOMINA_MISMO_BANCO;
			    				String concepto = BitaConstants.CONCEPTO_BAJA_CAT_NOMINA_MISMO_BANCO;

			    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
			    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

			    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);
							} catch(Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						}

           			//}
						/*****************************************
						 * Finaliza Funcionalidad de TOKEN
						 * **************************************/

				}



				/******************************************************************
				 *  Opcion = 9 - Muestra pantalla de Modificaci�n por Archivo de
				 *  			datos de Empleados en el nuevo cat�logo de n�mina
				 *******************************************************************/
				else if (opcion.equals("9"))
				{
					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n de Empleados Cat&aacute;logo de N&oacute;mina", ENC_SERV_NOM +
											"Cat&aacute;logo de N&oacute;mina > Modificaci&oacute;n por Archivo", "s37120h", request ) );


					/* everis JARA - Ambientacion -  Redireccionando a jsp */
					evalTemplate("/jsp/CatNominaModificaArchivo.jsp", request, response );

				}



				/******************************************************************
				 *  Opcion = 10 - Importaci�n y validaciones de archivo para
				 *
				 *******************************************************************/
				else if (opcion.equals("10"))
				{
					EIGlobal.mensajePorTrace("�������������� Entra a CatalogoNominaEmpleado con opcion 10 Importacion y validaciones", EIGlobal.NivelLog.INFO);

					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n de Empleados Cat&aacute;logo de N&oacute;mina", ENC_SERV_NOM +
											"Cat&aacute;logo de N&oacute;mina > Modificaci&oacute;n por Archivo", "s37120h", request ) );


					String tabla = "";
					CatNominaBean bean = new CatNominaBean();
					CatNominaConstantes cons = new CatNominaConstantes();
					try
					{
						CatNominaAction action = new CatNominaAction();
						getFileInBytes(request);
						String  nombreArchivo= (String)request.getAttribute("Archivo");//varible con el nombre del archivo que se esta mandando a llamar

						/*INICIA - EVERIS VALIDACION FICHEROS */
						File validaArchivo = new File(rutadeSubida+"/" + nombreArchivo);
						EIGlobal.mensajePorTrace("CatalogoNominaEmpleado || Inicia valida archivo malicioso", EIGlobal.NivelLog.INFO);	
						if(!(nombreArchivo!=null && UtilidadesEnlaceOwasp.esArchivoValido(validaArchivo, "A")))
						{
							bander=false;
							EIGlobal.mensajePorTrace("No es tipo de archivo TXT o ZIP", EIGlobal.NivelLog.INFO);	
							throw new Exception("Tipo de Archivo invalido");
							
						}
												
						else{
							/*FINALIZA - EVERIS VALIDACION FICHEROS */	
							ArrayList erroresArchivo;
							bean = action.validaArchivoModif(rutadeSubida+"/" + nombreArchivo, numContrato, request);

							if(!bean.getFalloInesperado())
							{
								erroresArchivo = bean.getListaErrores();

								if(erroresArchivo.size() == 0)
								{
									listaModifFinal = bean.getListaRegistros();

									if (bean.getTotalRegistros() <= 10) {
										tabla = action.creaDetalleImport(numContrato, bean.getListaRegistros(), cons._MODIF);
										request.setAttribute("TablaRegistros", tabla);
									}
									else {
										String lineaCant = "";

													lineaCant += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
													lineaCant += "<tr><td>&nbsp;</td></tr>";
													lineaCant += "<tr>";
													lineaCant += "<td class=texfootpagneg align=center bottom=\"middle\">";
													lineaCant += "El total de registros a modificar son: " + bean.getTotalRegistros() + " <br><br>";
													lineaCant += "</td>";
													lineaCant += "</tr>";
													lineaCant += "</table>";


										//"<br><br> <td class='tittabdat'align=center bottom=\"middle\"> El total de registros a modificar son: "
										//						+ bean.getTotalRegistros() + " </td><br><br>";
										request.setAttribute("LineaCantidad", lineaCant);
									}
								}
								else
								{
									String Linea = "";
									Iterator it = erroresArchivo.iterator();
									int  i = 0;
									while (it.hasNext()) {
										if(i==0){
											it.next();
										}
										i++;
										if(erroresArchivo.get(0).equals("Long")){

											Linea += "Longitud de Linea Incorrecta: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
										else if(erroresArchivo.get(0).equals("Datos")){

											Linea += "Tipo de dato incorrecto: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
										else if(erroresArchivo.get(0).equals("Cuentas")){

											Linea += "Cuenta Inexistente: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}

										else if(erroresArchivo.get(0).equals("Campo")){
										

											String str= null;
											str = (String)(it.next());


											int pos=str.indexOf('@');

											String campo = "", registro = "";

											campo = str.substring(0, pos);
											registro = str.substring(pos+1);




											Linea += "Campo Obligatorio: <b><FONT COLOR = RED> " + campo + "</font></b>" + " en l&iacute;nea: " + registro + "  <br>";
											request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Hace falta campo obligatorio: <br><br>"
											+ Linea + ".\", 1);");
										}

									}
									request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>"
											+ Linea + ".\", 1);");
								}
								EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 10 TotalRegistros ->" + bean.getTotalRegistros(), EIGlobal.NivelLog.INFO);
							}
							else
							{
								generaPantallaError(request, response, "Fallo en la Modificaci&oacute;n de registros en la Base de Datos. <br>Favor de Intentar mas tarde.", "Modificaci&oacute;n por Archivo");
							}
						}
					}
					catch(Exception e) {
						if(bander){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}else{
							
							request.setAttribute("ArchivoErr","cuadroDialogo('Archivo de Importaci&oacute;n inv&aacute;lido', 3);");
						}
					}
					if(!bean.getFalloInesperado()){
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaModificaArchivo.jsp", request, response );
					}

				}



				/********************************************************************
				 *  Opcion = 11 - Realizar Modificaciones despu�s de haber sido		*
				 *  			validado y mostrado el detalle de la importaci�n	*
				 *  			de registros con modificaciones					  	*
				 *******************************************************************/
				else if (opcion.equals("11"))
				{
					EIGlobal.mensajePorTrace("�������������� Entra a CatalogoNominaEmpleado con opcion 11 Modificaciones despues de validar", EIGlobal.NivelLog.INFO);

					/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
					request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n de Empleados Cat&aacute;logo de N&oacute;mina", ENC_SERV_NOM +
											"Cat&aacute;logo de N&oacute;mina > Modificaci&oacute;n por Archivo", "s37120h", request ) );


					CatNominaAction action = new CatNominaAction();
					boolean re;

					/*****************************************
					 * Inicio Funcionalidad de TOKEN
					 * **************************************/

			        //interrumpe la transaccion para invocar la validaci�n para Alta de proveedor

			       /* if(  valida==null) {
			            EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

			            boolean solVal=ValidaOTP.solicitaValidacion(
			                    session.getContractNumber(),IEnlace.NOMINA);

			            if( session.getValidarToken() &&
			                    session.getFacultad(session.FAC_VAL_OTP) &&
			                    session.getToken().getStatus() == 1 &&
			                    solVal) {
			                EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
			                //ValidaOTP.guardaParametrosEnSession(req);
			                guardaParametrosEnSession(req);
			                ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
			            } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
			                valida="1";
						}
			        }
			        // retoma el flujo
			        if( valida!=null && valida.equals("1")) {*/


						re = action.modificaDatosArchivo(numContrato, usuario, listaModifFinal);
						EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado opcion 11 re ->" + re, EIGlobal.NivelLog.INFO);
						if (re) {
							request.setAttribute("MensajeLogin01"," cuadroDialogo(\"La modificaci&oacute;n se realiz&oacute <br> exitosamente.\", 1);");
							log ( "BITACORIZACION" );
							modificaPropiaCN(usuario, numContrato,  request);
							//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
							try{
						       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT MODIFICA EMPLEADO ARCHIVO",EIGlobal.NivelLog.INFO);
							  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean.setReferencia(referencia);
							  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_MOD_EMPL_ARCH);
							  	bean.setConcepto(BitaConstants.ES_NOMINA_MOD_EMPL_ARCH_MSG);
							  	bean = bh.llenarBeanTCT(bean);
							    BitaHandler.getInstance().insertBitaTCT(bean);
							    EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado con opcion 11 referencia ->" + referencia, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("�������������� CatalogoNominaEmpleado no de cuentas a modificar ->" + listaModifFinal.size(), EIGlobal.NivelLog.INFO);
								try {
									if (listaModifFinal.size() > 0) {
										EmailSender emailSender = new EmailSender();
										EmailDetails beanEmailDetails = new EmailDetails();

										EIGlobal.mensajePorTrace("�������������������� Notificacion en CatalogoNominaEmpl modificacion por archivo", EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� no de registros ->" + listaModifFinal.size(), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("�������������������� referencia ->" + referencia, EIGlobal.NivelLog.INFO);

										beanEmailDetails.setNumeroContrato(session.getContractNumber());
										beanEmailDetails.setRazonSocial(session.getNombreContrato());
										beanEmailDetails.setNumRegImportados(listaModifFinal.size());
										beanEmailDetails.setNumRef(""+referencia);
										beanEmailDetails.setEstatusFinal("ENVIADO");


											emailSender.sendNotificacion(request, IEnlace.NOT_MODIF_EMPL_MAS, beanEmailDetails);

								       }
								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
				    		}catch(SQLException e){
				    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				    		}catch(Exception e){
				    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				    		}
				    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION MODIFICA EMPLEADO ARCHIVO",EIGlobal.NivelLog.INFO);
				    		//</VC>
							evalTemplate("/jsp/CatNominaModificaArchivo.jsp", request, response );
							try{
								EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

								int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
			    				String claveOperacion = BitaConstants.MODIFICACION_CAT_NOMINA_MISMO_BANCO;
			    				String concepto = BitaConstants.CONCEPTO_MODIFICACION_CAT_NOMINA_MISMO_BANCO;

			    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
			    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

			    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

							} catch(Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						}else{
							generaPantallaError(request, response, "Fallo en la modificaci&oacute;n de registros con la Base de Datos.<br> Intente m&aacute;s tarde", "Modificac&oacute;n por Archivo");
						}
						/* everis JARA - Ambientacion -  Redireccionando a jsp */


						/*****************************************
						 * Finaliza Funcionalidad de TOKEN
						 * **************************************/
				}
				/* everis JARA - Ambientacion - En caso de error (Por Definir)*/
				else{
					despliegaPaginaError("Error inesperado.",
					"B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina",
					"Servicios &gt; N&oacute;mina &gt; Cat&aacute;logo de N&oacute;mina &gt; B&uacute;squeda",
					request, response );
				}

			}/* everis JARA - Ambientacion - CERRANDO EL IF de sesion valida*/

		}/* everis JARA - Ambientacion - En caso de error GENERAL*/
	  	catch(Exception e){

	  		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

	  		despliegaPaginaError("Error inesperado en el Servidor.",
					"Cat&aacute;logo de N&oacute;mina",
					"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Cat&aacute;logo de N&oacute;mina",
					request, response );
	  	}
	}


	/*******************************************************************/
	/** Metodo: getFileInBytes para realizar importaciones de archivos.
	 * @return byte[]
	 * @param HttpServletRequest -> request
	/********************************************************************/
	public byte[] getFileInBytes ( HttpServletRequest request) throws IOException, SecurityException
	{
		String sFile = "";

		//MultipartRequest mprFile = new MultipartRequest ( request, rutadeSubida, 2097152 );

		FileInputStream entrada;

		sFile = getFormParameter(request, "fileName");

	    request.setAttribute("Archivo", sFile);


		 File fArchivo = new File ( ( rutadeSubida + "/" + sFile ).intern () );
		 byte[] aBArchivo = new byte[ ( int ) fArchivo.length () ];
		 entrada = new FileInputStream ( fArchivo );
		 byte bByte = 0;
		 try{
		 for ( int i = 0; ( ( bByte = ( byte ) entrada.read () ) != -1 ); i++ ) {
		     aBArchivo[ i ] = bByte;
		 }
			 } catch(Exception e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 } finally {
			 entrada.close ();
			     //fArchivo.delete ();
		 }

		 //Enumeration e = mprFile.getParameterNames();
		 HashMap parametros = getFormParameters(request);
		 Iterator e = parametros.entrySet().iterator();

		 while(e.hasNext()) {
			 String parametro = (String) ((Map.Entry) e.next()).getKey();

			 request.setAttribute(parametro, parametros.get(parametro));
		 }

		 return aBArchivo;
	}


		/***********************************************/
		/** Metodo: altaPropiaCN
		 ** Bitacorizaci�n para Alta de registro
		/***********************************************/
	    public static void altaPropiaCN (String usuario, String contrato, HttpServletRequest request) throws ServletException, IOException
	    {
	    	HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			if(Global.USAR_BITACORAS.trim().equals("ON")){

				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNP");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);

				bt.setContrato(contrato);


				try{

					bt.setNumBit(BitaConstants.EC_NOMINA_PROPIA_ALTA);

					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}catch (Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			}
		}


		/**********************************************/
		/** Metodo: consultaPropiaCN
		 ** Bitacorizaci�n para Consulta de registro
		/**********************************************/
		public void consultaPropiaCN(String usuario, String contrato, HttpServletRequest request)throws ServletException, IOException
		{
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");


			if(Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNP");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);

				bt.setContrato(contrato);


				try{

					bt.setNumBit(BitaConstants.EC_NOMINA_PROPIA_CONSULTA);

					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}catch (Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			}
		}


		/*************************************************/
		/** Metodo: modificaPropiaCN
		 ** Bitacorizaci�n para Modificacion de registro
		/*************************************************/
		public void modificaPropiaCN(String usuario, String contrato, HttpServletRequest request)throws ServletException, IOException
		{
			EIGlobal.mensajePorTrace("�������������� Entra a CatalogoNominaEmpleado::modificaPropiaCN", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("�������������� modificaPropiaCN usuario ->" + usuario, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("�������������� modificaPropiaCN contrato ->" + contrato, EIGlobal.NivelLog.INFO);
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");


			if(Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNP");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);

				bt.setContrato(contrato);


				try{

						bt.setNumBit(BitaConstants.EC_NOMINA_PROPIA_MODIFICA);

					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}catch (Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			}
		}


		/*****************************************************
		 ** Metodo que genera pantalla de Error, para
		 ** el manejo de excepciones por errores inesperados.
		*****************************************************/
		public void generaPantallaError(HttpServletRequest request, HttpServletResponse response, String mensaje, String funcion) throws Exception
		{

			despliegaPaginaError(mensaje, "Cat&aacute;logo de N&oacute;mina",
			"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Cat&aacute;logo de Empleados  &gt; " + funcion,
			request, response );

		}

		/***************************************************************
		 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
		 *** Metodo para exportar el catlogo de nomina a un archivo ****
		 **************************************************************/
		public String crearArchivo(String arrDatosQuery [][],String nombre)
		{

			String nomArchivo = null;
			String tmp="";
			int longitudes [] = {18,7,6,30,30,30,13,20,6,21,16};
			if(nombre !=null || nombre != "")
			{
			 nomArchivo = Global.DIRECTORIO_LOCAL + "/" + nombre;

			 try {
		            File Exportado = new File(nomArchivo);

		            boolean Creado = Exportado.createNewFile();
		            if (!Creado) {
		                Exportado.delete();
		                Exportado.createNewFile();
		            }
		            FileWriter writer = new FileWriter(Exportado);
		            StringBuffer Linea = new StringBuffer ("");
					int j = 0;
					int i = 0;
					for (i = 0 ; i < arrDatosQuery.length ; i++) {
						Linea = new StringBuffer ("");
						Linea.append (rellenar(arrDatosQuery[i][1],"D",longitudes[1],' '));
						Linea.append (rellenar(arrDatosQuery[i][2],"D",longitudes[2],' '));
						Linea.append (rellenar(arrDatosQuery[i][3],"D",longitudes[3],' '));
						Linea.append (rellenar(arrDatosQuery[i][4],"D",longitudes[4],' '));
						Linea.append (rellenar(arrDatosQuery[i][5],"D",longitudes[5],' '));
						Linea.append (rellenar(arrDatosQuery[i][0],"D",longitudes[0],' '));
						Linea.append (rellenar(arrDatosQuery[i][10],"D",longitudes[10],' '));
						Linea.append (rellenar(arrDatosQuery[i][6],"D",longitudes[6],' '));
						Linea.append (rellenar((CatNominaConstantes._ACT.equals(arrDatosQuery[i][9]) ? CatNominaConstantes._DESC_ACT: CatNominaConstantes._PEND.equals(arrDatosQuery[i][9]) ? CatNominaConstantes._DESC_PEND : "") ,"D",longitudes[9],' '));
						Linea.append (rellenar(arrDatosQuery[i][7],"D",longitudes[7],' '));

						Linea.append ("\n");
						writer.write(Linea.toString());
					}
					writer.flush ();
		            writer.close ();

		            //ESC 17/jun/2008 Correccion de envio de archivo remoto para poder ser descargado

		            ArchivoRemoto archRem = new ArchivoRemoto();
		            if(archRem.copiaLocalARemoto(nombre,"WEB"))
		            {
		            	nomArchivo = nombre;
		            	EIGlobal.mensajePorTrace("***CatalogoNominaEmpl.class Se creo el archivo remoto " + nomArchivo,EIGlobal.NivelLog.INFO);
		            }
		            else{
		            	nomArchivo = null;
		            	EIGlobal.mensajePorTrace("***CatalogoNominaEmpl.class Error al crear el archivo remoto remoto " + nomArchivo ,EIGlobal.NivelLog.INFO);
		            }

		        } catch (FileNotFoundException ex1) {
		        	nomArchivo = null;
		            EIGlobal.mensajePorTrace(new Formateador().formatea(ex1), EIGlobal.NivelLog.INFO);
		        } catch (IOException ex2) {
		        	nomArchivo = null;
					EIGlobal.mensajePorTrace(new Formateador().formatea(ex2), EIGlobal.NivelLog.INFO);
		        }
			}

		    return nomArchivo;
		}

		/**
		 * Crea Archivo separado por caracter.
		 * @since 26/03/2015
		 * @param arrDatosQuery arrDatosQuery
		 * @param nombre nombre
		 * @return String archivo
		 */
		public String crearArchivoSeparadoCaracter(String arrDatosQuery [][],String nombre)
		{

			String nomArchivo = null;
			String tmp="";
			int longitudes [] = {18,7,6,30,30,30,13,20,6,21,16};
			if(nombre !=null || nombre != "")
			{
			 nomArchivo = Global.DIRECTORIO_LOCAL + "/" + nombre;

			 try {
		            File Exportado = new File(nomArchivo);

		            boolean Creado = Exportado.createNewFile();
		            if (!Creado) {
		                Exportado.delete();
		                Exportado.createNewFile();
		            }
		            FileWriter writer = new FileWriter(Exportado);
		            StringBuffer Linea = new StringBuffer ("");
					int j = 0;
					int i = 0;
					for (i = 0 ; i < arrDatosQuery.length ; i++) {
						Linea = new StringBuffer ("");
						Linea.append (rellenar(arrDatosQuery[i][1],"D",longitudes[1],' ') + "\t");
						Linea.append (rellenar(arrDatosQuery[i][2],"D",longitudes[2],' ')+ "\t");
						Linea.append (rellenar(arrDatosQuery[i][3],"D",longitudes[3],' ')+ "\t");
						Linea.append (rellenar(arrDatosQuery[i][4],"D",longitudes[4],' ')+ "\t");
						Linea.append (rellenar(arrDatosQuery[i][5],"D",longitudes[5],' ')+ "\t");
						Linea.append (rellenar(arrDatosQuery[i][0],"D",longitudes[0],' ')+ "\t");
						Linea.append (rellenar(arrDatosQuery[i][10],"D",longitudes[10],' ')+ "\t");
						Linea.append (rellenar(arrDatosQuery[i][6],"D",longitudes[6],' ')+ "\t");
						Linea.append (rellenar((CatNominaConstantes._ACT.equals(arrDatosQuery[i][9]) ? CatNominaConstantes._DESC_ACT: CatNominaConstantes._PEND.equals(arrDatosQuery[i][9]) ? CatNominaConstantes._DESC_PEND : "") ,"D",longitudes[9],' ')+ "\t");
						Linea.append (rellenar(arrDatosQuery[i][7],"D",longitudes[7],' '));

						Linea.append ("\n");
						writer.write(Linea.toString());
					}
					writer.flush ();
		            writer.close ();

		            //ESC 17/jun/2008 Correccion de envio de archivo remoto para poder ser descargado

		            ArchivoRemoto archRem = new ArchivoRemoto();
		            if(archRem.copiaLocalARemoto(nombre,"WEB"))
		            {
		            	nomArchivo = nombre;
		            	EIGlobal.mensajePorTrace("***CatalogoNominaEmpl.class Se creo el archivo remoto " + nomArchivo,EIGlobal.NivelLog.INFO);
		            }
		            else{
		            	nomArchivo = null;
		            	EIGlobal.mensajePorTrace("***CatalogoNominaEmpl.class Error al crear el archivo remoto remoto " + nomArchivo ,EIGlobal.NivelLog.INFO);
		            }

		        } catch (FileNotFoundException ex1) {
		        	nomArchivo = null;
		            EIGlobal.mensajePorTrace(new Formateador().formatea(ex1), EIGlobal.NivelLog.INFO);
		        } catch (IOException ex2) {
		        	nomArchivo = null;
					EIGlobal.mensajePorTrace(new Formateador().formatea(ex2), EIGlobal.NivelLog.INFO);
		        }
			}

		    return nomArchivo;
		}

		/***************************************************************
		 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
		 *** Metodo para exportar el catlogo de nomina a un archivo ****
		 **************************************************************/

		public String rellenar(String cadena,String pos,int tamanio ,char carac )
		{
			int lon;
			String aux1="";
			if(cadena == null)
				cadena ="";

			lon = cadena.length();
			if(lon > tamanio)//Validamos la longitud de la cadena, si es mayor al tama�o deseado se corta
				cadena = cadena.substring(0,tamanio);

			lon = (tamanio - lon < 0 ? 0 : tamanio - lon ); //obtenemos la longitud de la cadena de relleno

			for(int i = 1 ; i <= lon ; i++)
			{
				aux1 += carac;
			}

			//validamos si se rellena a la izquierda o a la derecha
			if("I".equals(pos))
				aux1 = aux1 + cadena;
			else
				aux1 = cadena + aux1;

			return aux1;
		}

}