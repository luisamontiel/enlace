/*Banco Santander Mexicano
  Clase AltaCtaCLABE muestra la pantalla de alta de cuentas CLABE
  @Autor:Rafael Rosiles Soto
  @version: 1.0
  fecha de creacion: 14 de Enero de 2004
*/

package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class AltaCtaCLABE extends BaseServlet {
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
			HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.defaultAction entrando ***", EIGlobal.NivelLog.INFO);

		boolean sesionvalida = SesionValida( request, response );
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Alta de cuentas CLABE",
				"Administraci&oacute;n y control &gt; Cuentas &gt; Actualizaci&oacute;n de cuentas CLABE","s37030h", request));
		request.setAttribute("Fecha", ObtenFecha());
		request.setAttribute("Hora", ObtenHora());
		request.setAttribute("ContUser",ObtenContUser(request));


		/*Valida la sesion del usuario*/
		//if(sesionvalida && (session.getFacultad(session.FAC_ALTA_CTAS_OTROSBANC)  session.getFacultad(session.FAC_TRANSF_NOREG)) {
		if(sesionvalida && (session.getFacultad(session.FAC_ALTA_CTAS_OTROSBANC) || session.getFacultad(session.FAC_TRANSF_NOREG) || session.getFacultad(session.FAC_CCENVARCHPR_CONF))){

			String Operacion = (String) request.getParameter( "Operacion" );

			EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.defaultAction  Operacion >>" + Operacion, EIGlobal.NivelLog.INFO);

			String tablaImportacion="<table align=center>"+
    				            "<tr><td class='tittabdat' colspan='2'>&nbsp;Seleccione el archivo a importar&nbsp;</td></tr>"+
    				            "<tr align='center'><td class='textabdatcla' valign='top' colspan='2'>"+
    				            "<table width='180' border='0' cellspacing='4' cellpadding='0'>"+
            				    "<tr valign='middle'><td class='tabmovtex' nowrap>Importar archivo</td></tr>"+
              				    "<tr><td nowrap class='tabmovtex'><input type='file' name='archivocuentas'></td></tr>"+
          				        "</table></td></tr></table><br>"+
          				        "<table align=center>"+
          				        "<tr align='center'><td class='tabmovtex' nowrap>"+
         				        "<A HREF='javascript:importar();'><img border='0' name='imageField' src='/gifs/EnlaceMig/gbo25300.gif' width='83' height='22' alt='Importar'></a>"+
         				        "</td></tr></table>";

         	tablaImportacion += "<input type='hidden' name='Operacion'><input type='hidden' name='indica'>";

			// Muestra el numero de registros que contiene el archivo
            if (Operacion.equals("2")) {

            	EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.defaultAction  Operacion >>" + Operacion, EIGlobal.NivelLog.INFO);


            	EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.defaultAction  importando archivo ...", EIGlobal.NivelLog.INFO);

            	// Importacion del archivo de cuentas CLABE
				String nombreArchivoClabe = importaArchivoClabes( request, response);

				EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.defaultAction archivo en el servidor >> " + nombreArchivoClabe, EIGlobal.NivelLog.INFO);

				//*********************************
				// validacion del archivo

				nombreArchivoClabe = nombreArchivoClabe.substring(nombreArchivoClabe.indexOf("/",2)+1,nombreArchivoClabe.length());

				EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.defaultAction archivo >>"+nombreArchivoClabe, EIGlobal.NivelLog.INFO);

				//copiar archivo local a host remoto

		    	EIGlobal.mensajePorTrace("*** AltaCtaCLABE.defaultAction inicio de copia de archivo local  ...", EIGlobal.NivelLog.DEBUG);

				ArchivoRemoto archivoCLABE = new ArchivoRemoto();

				boolean result = false;

				// Copia de archivo local a ruta remota
				if(!archivoCLABE.copiaLocalARemoto(nombreArchivoClabe)) {
					EIGlobal.mensajePorTrace("*** AltaCtaCLABE.defaultAction  no se pudo copiar archivo local a remoto :" + nombreArchivoClabe, EIGlobal.NivelLog.INFO);
					result = false;
				}
				else {
			    	EIGlobal.mensajePorTrace("*** AltaCtaCLABE.defaultAction  archivo local copiado exitosamente:" + nombreArchivoClabe, EIGlobal.NivelLog.INFO);
			    	result = true;
				}

				EIGlobal.mensajePorTrace("*** AltaCtaCLABE.defaultAction  fin de copia de archivo local ...", EIGlobal.NivelLog.DEBUG);

				//*********************************

				if (result) {
					// validacion del archivo de cuentas CLABE
					String[] vectorCuentas = actualizaCLABE( "C" , nombreArchivoClabe , request, response);

					if (vectorCuentas[1].equals("ACCL9999")) {
						request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
						evalTemplate( IEnlace.ERROR_TMPL, request, response );
					}
					else {

						String sRegistros;

						if (vectorCuentas[1].equals("ACCL0000")){

							sRegistros = vectorCuentas[3];

							StringBuffer strFecha = new StringBuffer("");
			      			Date Hoy = new Date();
			      			GregorianCalendar Cal = new GregorianCalendar();
			      			Cal.setTime(Hoy);
			              	strFecha.append(Cal.get(Calendar.DATE));
			           		strFecha.append("/");
			     	        strFecha.append((Cal.get(Calendar.MONTH)+1));
			      			strFecha.append("/");
			        		strFecha.append(Cal.get(Calendar.YEAR));

			        		tablaImportacion += "<input type='hidden' name='Archivo' value='" + nombreArchivoClabe + "'>";

							tablaImportacion += "<br><br><table border=0 cellspacing=2 cellpadding=3 bgcolor=#FFFFFF>"+
								    "<tr><td align='center' class=tittabdat nowrap>Operaci&oacute;n</td>"+
								    "<td align='center' class=tittabdat nowrap>Fecha</td>"+
								    "<td align='center' class=tittabdat nowrap>No. de registros</td></tr>"+
						            "<tr><td align='center' class='textabdatcla' nowrap>&nbsp;Actualizaci&oacute;n de cuentas CLABE</td>"+
						            "<td align='center' class='textabdatcla' nowrap>"+strFecha.toString()+"</td>"+
						            "<td align='center' class='textabdatcla' nowrap>"+sRegistros+
									"&nbsp;</td></tr></table>";

							if (Integer.parseInt(sRegistros)!=0) {
								request.setAttribute("Registros", sRegistros);
								tablaImportacion += "<br><table align=center><tr>"+
										//"<td class='tabmovtex' nowrap><a href='javascript:dialogoConfirma("+sRegistros+");'><img border='0' name='imageField' src='/gifs/EnlaceMig/AltaCtaCLABE.jpg' height='22' alt='Alta cuenta CLABE'></a>"+
										"<td class='tabmovtex' nowrap><a href='javascript:cuentas();'><img border='0' name='imageField' src='/gifs/EnlaceMig/AltaCtaCLABE.jpg' height='22' alt='Alta cuenta CLABE'></a>"+
										"<a href = 'javascript:scrImpresion();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a>"+
										"</td></tr></table>";
							}
							else {

								String archivoEstatus = "/tmp/"+session.getContractNumber()+".aclabe";

								String codigoHTML = armaArchivoError(archivoEstatus);
								EIGlobal.mensajePorTrace("*** codigoHTML>> "+codigoHTML, EIGlobal.NivelLog.INFO);
								request.setAttribute("armaArchivoError",codigoHTML);
								request.setAttribute("ArchivoErr","true;");
							}
						}
						else {

							tablaImportacion += "<br><br><table border=0><tr><td align='center' class='texencfec'>"+vectorCuentas[2]+"</td></tr></table>";

						}


					}
				}
				else {
					request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
					evalTemplate( IEnlace.ERROR_TMPL, request, response );
				}
            }

			request.setAttribute("newMenu", session.getFuncionesDeMenu());

			request.setAttribute( "Importacion", tablaImportacion );


			evalTemplate( "/jsp/AltaCtaCLABE.jsp", request, response );
		} else {
			//System.out.println("Este contrato NO tiene facultades para esta operaci�n");
			//request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			//evalTemplate( IEnlace.ERROR_TMPL, request, response );
			response.sendRedirect("SinFacultades");
		}
    }


    private String importaArchivoClabes(HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String archivo_clabes = "";
		int linea = 0;
		byte contenidoArchivo[] = new byte[1];
		String nombreArchivoClabe = "";
		String nombreOriginal = "";

		nombreOriginal  = request.getParameter("archivocuentas_file");

		EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes nombre archivo>> " + nombreOriginal, EIGlobal.NivelLog.INFO);

		try {
			EIGlobal.mensajePorTrace( "*** AltaCtaCLABE.importaArchivoClabes  obteniendo parametros del archivo", EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes  nombre original"+nombreOriginal, EIGlobal.NivelLog.INFO);
			contenidoArchivo  = getFileInBytes( request, nombreOriginal );
		} catch( Exception e ) {
			EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes Error obteniendo parametros>>" + e.toString(), EIGlobal.NivelLog.INFO);
			e.printStackTrace();
		}
		int tamanioArchivo = 0;
		tamanioArchivo = contenidoArchivo.length;
		EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes  creando nombre archivo", EIGlobal.NivelLog.INFO);
		//Obtiene el nombre del archivo y le asigna extension .iclabe
		nombreOriginal = nombreOriginal.substring(nombreOriginal.lastIndexOf('\\')+1);
		nombreOriginal = extensionCLABE(nombreOriginal,request);
		nombreArchivoClabe = ""+ Global.DIRECTORIO_LOCAL + "/" + nombreOriginal;
		EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes  nombre archivo local>>" + nombreArchivoClabe, EIGlobal.NivelLog.INFO);
		//Si ya existe el archivo lo borra
		try{
				File archivo=new File(nombreArchivoClabe);
				if (archivo.exists())
					{
						archivo.delete();
					}
			 }
		catch(Exception e)
			{
				EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes error creando objeto File(delete): " + e.toString(), EIGlobal.NivelLog.INFO);
			}
		//Guardando el archivo (upload)
		try
			{
				EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes  Creando objeto File", EIGlobal.NivelLog.INFO);
			File pathArchivoClabe = new File(nombreArchivoClabe);
				EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes  Creando objeto RandomAccessFile", EIGlobal.NivelLog.INFO);
			RandomAccessFile archivoClabesImportado = new RandomAccessFile(pathArchivoClabe, "rw");
			archivoClabesImportado.setLength(0); //para truncar el archivo en caso que exista
				EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.importaArchivoClabes  Escribiendo en : " + pathArchivoClabe, EIGlobal.NivelLog.INFO);
			  		archivoClabesImportado.write(contenidoArchivo, 0,  tamanioArchivo);
				EIGlobal.mensajePorTrace ("*** AltaCtaCLABE.importaArchivoClabes  Cerrando archivo", EIGlobal.NivelLog.INFO);
		  		archivoClabesImportado.close();
			}
		catch(IOException e)
			{
				EIGlobal.mensajePorTrace("*** AltaCtaCLABE.importaArchivoClabes  Error creando archivo de cuentas CLABE" + e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			return nombreArchivoClabe;
	}


	private String extensionCLABE(String nombre,HttpServletRequest request){
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		if (nombre.length()>0) {
			if (nombre.indexOf('.')==-1)
				{
					return session.getContractNumber() + ".iclabe";
				}
			else {
				return session.getContractNumber()+".iclabe";
			}
		}
		else {
			return "temp"+ session.getContractNumber() +".iclabe";
		}
	}


	public String armaArchivoError (String Archivo)
    	throws ServletException, IOException
    {

    	String codigoHTML="<html>";
			   codigoHTML+="<head><title>Estatus</title>";
			   codigoHTML+="<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>";
			   codigoHTML+="</head>";
			   codigoHTML+="<body bgcolor='white'>";
			   codigoHTML+="<form>";
			   codigoHTML+="<table border=0 width=420 class='textabdatcla' align=center>";
			   codigoHTML+="<tr><th class='tittabdat'>informaci&oacute;n del Archivo Importado</th></tr>";
			   codigoHTML+="<tr><td class='tabmovtex1' align=center>";

			   codigoHTML+="<table border=0 align=center><tr><td class='tabmovtex1' align=center width=>";
			   codigoHTML+="<img src='/gifs/EnlaceMig/gic25060.gif'></td>";
			   codigoHTML+="<td class='tabmovtex1' align='center'><br>No se llevo a cabo la importaci�n ya que el archivo seleccionado tiene los siguientes errores.<br><br>";
			   codigoHTML+="</td></tr></table>";
			   codigoHTML+="</td></tr>";

			   // Lectura del detalle del archivo aclabe
			   int NumeroRegistros = getnumberlinesCtasCLABE(Archivo)-1;

			   String[][] arrCuentas = ObteniendoCtasCLABE(Archivo,NumeroRegistros, "aclabe");

			   int cont=0;

			   for ( int i = 1; i <= NumeroRegistros; i++ ) {
			   		if (arrCuentas[i][1].trim().equals("R")) {
			   			codigoHTML+="<tr><td class='tabmovtex1'>";
			   			codigoHTML+="<b>Linea "+String.valueOf(i)+"</b>";
			   			codigoHTML+="<br><DD><LI>"+arrCuentas[i][2];
			   			codigoHTML+="</td></tr>";
			   			cont++;
			   		}
			   }

			   codigoHTML+="<tr><td class='tabmovtex1'>";
			   codigoHTML+="<br><b>TOTAL: "+String.valueOf(cont)+" errores</b>";
			   codigoHTML+="</tr></td>";
			   codigoHTML+="<br>";
			   codigoHTML+="</table>";
			   codigoHTML+="<table border=0 align=center >";
			   codigoHTML+="<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>";
			   codigoHTML+="</table>";
			   codigoHTML+="</form>";
			   codigoHTML+="</body></html>";
		return codigoHTML;
    }

}