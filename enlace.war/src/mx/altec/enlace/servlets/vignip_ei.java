package mx.altec.enlace.servlets;
import java.io.IOException;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.text.*;
import javax.naming.*;

import mx.altec.enlace.utilerias.IEnlace;


import java.lang.reflect.*;
import java.lang.*;
import java.sql.*;
import java.util.*;

public class vignip_ei extends HttpServlet
{
  public int vignip(Connection p_conn,String p_cve_usr,HttpServletRequest request, HttpServletResponse response)
  throws  IOException,SQLException
  {
      String v_sql="";
      int    v_exi_usr=0;
      String v_valor="";
      int v_ok=0,v_num_dias_vig=0;
      //BaseServlet bs = new BaseServlet();
      v_sql= " select cve_usr,fch_exp_nip "+
                    " from eweb_vignip " +
                    " where cve_usr = "+p_cve_usr;
      try
      {
         v_exi_usr=existe_registro(p_conn,v_sql);
         System.out.println("v_exi_usr "+v_exi_usr);
         if (v_exi_usr==1)
         {
            v_sql = " select trunc(FCH_EXP_Nip - sysdate) + 1 "+
                      " from eweb_vignip "+
                      " where cve_usr = "+p_cve_usr;
            v_valor = retorna_valor(p_conn,v_sql);
            System.out.println("v_valor: "+v_valor);
            v_num_dias_vig = Integer.parseInt(v_valor);
            System.out.println("v_num_dias_vig: "+v_num_dias_vig);
            if (v_num_dias_vig <= 0)
            {
               try
               {
                  Despliega_pagina(IEnlace.CAMBIO_PASSWD,request,response);
               }
               catch(ServletException err_Servlet)
               {
                  System.out.println("Error en Despliega_pagina(): "+err_Servlet.toString());
               }
            }
         }
         else
         {
             v_sql = " insert into eweb_vignip(cve_usr,dias_vig_nip,fch_ult_mod_nip,fch_exp_nip) "+
                                      " values("+p_cve_usr+",30,sysdate,sysdate+30)";
             v_ok=ejecuta_sql(p_conn,v_sql);
             System.out.println("v_ok ejecuta: "+v_ok );
         }

         return 1;
      }
      catch(SQLException err_sql)
      {
         System.out.println("Error en de SQL en vignip(): "+err_sql.toString());
         return 0;
      }

  }

  public int  ejecuta_sql(Connection conn,String v_dml)
    throws  IOException,SQLException
  {
     String  com_sql= "";
     int     v_todo_uno_int=0;
     int     v_todos_int=0;
     String v_clave_q="";
     try
     {
       // Load the Oracle JDBC driver
       //DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
       // Create a Statement
       Statement stmt = conn.createStatement ();
       stmt.executeUpdate(v_dml);
       conn.commit();
       stmt.close();
       return 1;
     }
     catch(SQLException err_sql)
     {
        System.out.println("Error en de SQL en ejecuta_sql(): "+err_sql.toString());
        return 0;
     }
  }

     public int existe_registro(Connection p_conn,String p_dml)
       throws SQLException
     {
       ResultSet rset=null;
       Statement stmt = null;
       try
       {
          stmt = p_conn.createStatement ();
          rset = stmt.executeQuery (p_dml);
          System.out.println("p_dml"+p_dml);
          if (rset.next ())
          {
             stmt.close();
             rset.close();
             return 1;
          }
          else
          {
             stmt.close();
             rset.close();
             return 0;
          }
       }
       catch(SQLException err_sql)
       {
         System.out.println("Error de SQL en existe_registro(): "+err_sql.toString());
         stmt.close();
         rset.close();
         return 0;
       }
     }

     public String retorna_valor(Connection p_conn,String p_dml)
       throws SQLException
     {
       ResultSet rset=null;
       Statement stmt = null;
       String v_valor="";
       try
       {
          stmt = p_conn.createStatement ();
          rset = stmt.executeQuery (p_dml);
          if (rset.next ())
          {
             v_valor = rset.getString (1);
             stmt.close();
             rset.close();
             return v_valor;
          }
          else
          {
             stmt.close();
             rset.close();
             return null;
          }
       }
       catch(SQLException err_sql)
       {
         System.out.println("Error en de SQL en retorna_valor(): "+err_sql.toString());
         stmt.close();
         rset.close();
         return null;
       }
     }
  public void Despliega_pagina(String template, HttpServletRequest req, HttpServletResponse res)
     throws ServletException, IOException
  {
     System.out.println("template: "+template);
	   RequestDispatcher disp = getServletContext().getRequestDispatcher(template);
	   res.setContentType("text/html");
	   disp.forward(req,res);
  }
}