/**Banco Santander Mexicano
* Clase SeleccionaBajas, lee el registro seleccionada para modificarlo o darlo de baja
* @autor Rodrigo Godo
* @version 1.1
* fecha de creacion: Diciembre 2000 - Enero 20001
* responsable: Roberto Guadalupe Resendiz Altamirano
*/

package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class SeleccionaBajas extends BaseServlet
{
	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
    			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		Vector diasNoHabiles;
		//Vector diasNoHabilesJS = new Vector();
		GregorianCalendar fechaHoy= new GregorianCalendar();
		CalendarNomina nominaCalendario= new CalendarNomina(this);


		HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		boolean sesionvalida = SesionValida( request, response );
		EIGlobal.mensajePorTrace("***SeleccionaBajas.class &entrando al metodo selecciona bajas&", EIGlobal.NivelLog.INFO);
        request.setAttribute("MenuPrincipal", session.getstrMenu());
        request.setAttribute("Fecha", ObtenFecha());
        request.setAttribute("ContUser",ObtenContUser(request));
		String laFechaHoy=EIGlobal.formatoFecha(fechaHoy,"aaaammdd");
        String VarFechaHoy = "";
        diasNoHabiles = nominaCalendario.CargarDias();
        String strInhabiles = nominaCalendario.armaDiasInhabilesJS();
        Calendar fechaFin = nominaCalendario.calculaFechaFin();
        VarFechaHoy = nominaCalendario.armaArregloFechas();
		nominaCalendario = null;
		diasNoHabiles.clear();
		diasNoHabiles = null;
        String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();
        String Cuentas ="";
        String tipo_cuenta="";
        /*Modificacion para integracion
		/*String[][] arrayCuentas = ContCtasRelac(session.getContractNumber());

		for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++){
	        tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
             if (arrayCuentas[indice][2].equals("P")&& (!tipo_cuenta.equals("BM"))&&(!tipo_cuenta.equals("SI")) && (!tipo_cuenta.equals("49")))
             Cuentas += "<OPTION value = " + arrayCuentas[indice][1] + ">" + arrayCuentas[indice][1] + "\n";
        }
		*/
        session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);

        String  datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
				datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
                datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";

		request.setAttribute("Cuentas","<Option value = " + session.getCuentaCargo() + ">" + session.getCuentaCargo());
        request.setAttribute("VarFechaHoy",           VarFechaHoy);
        request.setAttribute("fechaFin",              strFechaFin);
        request.setAttribute("diasInhabiles",         strInhabiles);
        request.setAttribute("Fechas",                 ObtenFecha(true));
        request.setAttribute("FechaHoy",              ObtenFecha(false) + " - 06" );

        String arrayDetails[][];
		if (sesionvalida){
    		String fileNomina="";
			int linea=0;
			String contenidoArchivoStr="";

		try{
           	InputStream arch = new FileInputStream(session.getNameFileNomina());
            int result=0;
            String longitud=session.getNominaNumberOfRecords();
            int longitudArreglo=Integer.parseInt(longitud);
	        byte []campo = new byte[1]; result= arch.read(campo); String encTipoReg= new String(campo);
	    	campo=new byte[5];	result=arch.read(campo); String encNumSec = new String(campo);
		    campo=new byte[1];	result=arch.read(campo); String encSentido = new String(campo);
			campo=new byte[8];	result=arch.read(campo); String encFechGen = new String(campo);
			campo=new byte[16];	result=arch.read(campo); String encCuenta = new String(campo);
			campo=new byte[8];	result=arch.read(campo); String encFechApli = new String(campo);
			campo=new byte[2];	result=arch.read(campo); String finEncabezado = new String(campo);

			/* Detalle del archivo */
            //  InputStream arch = new FileInputStream(pathArchivoNom);
            arrayDetails=  new String[longitudArreglo][9];
            int longTotalDetalle;
			longTotalDetalle= 0;
            for (int i= 0; i<arrayDetails.length; i++){
				campo=new byte[1];  result=arch.read(campo); String tipoRegistro =new String (campo);
		  		campo=new byte[5];  result=arch.read(campo); String NumeroSecuencia   =new String (campo);
                campo=new byte[7];  result=arch.read(campo); String NumeroEmpleado    =new String (campo);
	        	campo=new byte[30]; result=arch.read(campo); String ApellidoPaterno      =new String (campo);
		        campo=new byte[20]; result=arch.read(campo); String ApellidoMaterno      =new String (campo);
			    campo=new byte[30]; result=arch.read(campo); String Nombre                =new String (campo);
        		campo=new byte[16]; result=arch.read(campo); String NumeroCuenta      =new String (campo);
	    		campo=new byte[18]; result=arch.read(campo); String Importe     =new String (campo);
                campo=new byte[2];  result=arch.read(campo); String finDetalle = new String(campo);
    	    	arrayDetails[i][0]=tipoRegistro;
	    		arrayDetails[i][1]=NumeroSecuencia;
        		arrayDetails[i][2]=NumeroEmpleado;
	    		arrayDetails[i][3]=ApellidoPaterno;
        		arrayDetails[i][4]=ApellidoMaterno;
        		arrayDetails[i][5]=Nombre;
	    		arrayDetails[i][6]=NumeroCuenta;
	    		arrayDetails[i][7]=Importe;
	    		arrayDetails[i][8]=finDetalle;
                String registroDetalleCompleto;
                registroDetalleCompleto=arrayDetails[i][0]+arrayDetails[i][1]+arrayDetails[i][2]+arrayDetails[i][3]+arrayDetails[i][4]+arrayDetails[i][5]+arrayDetails[i][6]+arrayDetails[i][7]+arrayDetails[i][8];
        		longTotalDetalle= registroDetalleCompleto.length();
    	    }

			//Registro Sumario
            campo= new byte[1]; result= arch.read(campo); String sumTipoReg = new String(campo);
            campo= new byte[5]; result= arch.read(campo); String sumNumSec= new String(campo);
		    campo=new byte[5];  result=arch.read(campo); String sumTotRegs = new String(campo);
			campo= new byte[18]; result= arch.read(campo); String sumImpTotal = new String(campo);
            String registroSumarioCompleto;
            registroSumarioCompleto=sumTipoReg+sumNumSec+sumTotRegs+sumImpTotal;
            int longTotalSumario;
            longTotalSumario=registroSumarioCompleto.length();
			arch.close();

        /////////// Aqui finaliza la lectura de campos del archivo///////

            contenidoArchivoStr="<table border=1 align=center>";
			contenidoArchivoStr=contenidoArchivoStr+"<tr><td colspan=9 align=center><b>Pago de Nomina</b></td></tr>";
			contenidoArchivoStr=contenidoArchivoStr+"<tr><td bgcolor='red'>Edit/Supr</td><td bgcolor=red>No.Empleado</td><td bgcolor=red>Paterno</td><td bgcolor=red>Materno</td><td bgcolor=red>Nombre</td>";
			contenidoArchivoStr=contenidoArchivoStr+"<td bgcolor=red>Cuenta</td><td bgcolor=red>Importe</td><td bgcolor=red>Ref</td>";
			contenidoArchivoStr=contenidoArchivoStr+"<td bgcolor=red>Edo</td>";

            for( int i=0; i<arrayDetails.length; i++){
				long posicionReg=i+1;
                contenidoArchivoStr=contenidoArchivoStr+"<tr><td><input type=radio name=seleccionaRegs value="+posicionReg+"></td>";
        		contenidoArchivoStr=contenidoArchivoStr+"<td>"+arrayDetails[i][2]+"</td>";
		        contenidoArchivoStr=contenidoArchivoStr+"<td>"+arrayDetails[i][3]+"</td>";
			    contenidoArchivoStr=contenidoArchivoStr+"<td>"+arrayDetails[i][4]+"</td>";
			    contenidoArchivoStr=contenidoArchivoStr+"<td>"+arrayDetails[i][5]+"</td>";
				contenidoArchivoStr=contenidoArchivoStr+"<td>"+arrayDetails[i][6]+"</td>";
				contenidoArchivoStr=contenidoArchivoStr+"<td>"+arrayDetails[i][7]+"</td>";
            }
            contenidoArchivoStr=contenidoArchivoStr+"</tr></table>";
		}
		catch(IOException e){
			EIGlobal.mensajePorTrace("***SeleccionaBajas.class Ha ocurrido una excepcion en: %execute()"+e, EIGlobal.NivelLog.INFO);
        }
        request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr);
		String descargaArchivo=session.getRutaDescarga();
		request.setAttribute("DescargaArchivo",descargaArchivo);
		String archivoName=session.getNameFileNomina();
		request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));

        evalTemplate(IEnlace.MTO_NOMINA_TMPL,request, response );
	} else {
         request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
         evalTemplate(IEnlace.ERROR_TMPL, request, response );
    }
  }
}