package mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.IEnlace.NOM_PRE_CONSULTA_ESTADO_ASIGNACION;
import static mx.altec.enlace.utilerias.IEnlace.NOM_PRE_CONSULTA_ESTADO_ASIGNACION_DETALLE;
import static mx.altec.enlace.utilerias.IEnlace.NOM_PRE_EXP_ESTADO_ASIGNACION;
import static mx.altec.enlace.utilerias.NomPreUtil.bitacoriza;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.NomPreAltaMasiva;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.NomPreAltaMasivaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;

public class NomPreConsultaEstadoAsignacion extends BaseServlet {

	private static final String DETALLE_ESTADO_ASIGNACION = "detalle";
	private static final String ENVIO_DETALLE_ESTADO_ASIGNACION = "enviar";
	private static final String CANCELAR_ARCHIVO_CARGA = "cancelar";
	private static final String SER_ARCH_CONSULTA_ARC = "archconarchalta";
	private static final String COD_EXITO = "NOMI0000";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (!SesionValida(request, response)) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
			return;
		}

		BaseResource session = obtenerBaseResource(request);

		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta estado asignaci&oacute;n", "Servicios > Tarjeta de Pagos Santander  > Asignaci&oacute;n tarjeta-empleado > Consulta estado asignaci&oacute;n", "s37140h", request));//YHG NPRE

		//CGH Valida servicio de nomina
		if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
			return;
		}

		String operacion = request.getParameter("operacion");

		if (((DETALLE_ESTADO_ASIGNACION.equals(operacion) && !"true".equals(request.getParameter("inicial"))) ||
			ENVIO_DETALLE_ESTADO_ASIGNACION.equals(operacion)) &&
			request.getSession().getAttribute(SER_ARCH_CONSULTA_ARC) == null) {

			operacion = null;
		}

		if (DETALLE_ESTADO_ASIGNACION.equals(operacion)) {

			detallar(request, response);

		} else if (ENVIO_DETALLE_ESTADO_ASIGNACION.equals(operacion)) {

			enviar(request, response);

		} else if (CANCELAR_ARCHIVO_CARGA.equals(operacion)) {

			cancelar(request, response);

		} else {

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_ALTA_ENTRA);
			bitacoriza(request, null, null, true, bt);

			request.getRequestDispatcher(NOM_PRE_CONSULTA_ESTADO_ASIGNACION)
				.include(request, response);
		}
	}

	private BaseResource obtenerBaseResource(HttpServletRequest request) {

		HttpSession session = request.getSession();

		return (BaseResource) session.getAttribute("session");
	}

	private void cancelar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String secuencia = request.getParameter("archivo");
		String fechaRecepcion = null;

		if (secuencia != null && secuencia.indexOf("|") != -1) {

			int index = secuencia.indexOf("|");
			fechaRecepcion = secuencia.substring(index + 1);
			secuencia = secuencia.substring(0, index);

			final BaseResource br = obtenerBaseResource(request);

			NomPreAltaMasivaDAO dao = new NomPreAltaMasivaDAO();

			String salida = dao.cancelarAltaMasiva(br.getUserID8(), br.getContractNumber(), br.getUserProfile(), secuencia, fechaRecepcion);
			String codigo = null;
			String mensaje = null;

			if (salida != null && salida.length() > 0) {

				codigo = salida.substring(0, 8);
			}

			BitaTransacBean bt = new BitaTransacBean();
			bt.setUsr(br.getUserID8());
			bt.setContrato(br.getContractNumber());
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_ALTA_CANCELA);
			bitacoriza(request, "NPCE", codigo, bt);


			if (COD_EXITO.equals(codigo)) {

				mensaje = "Archivo cancelado exitosamente";
				try
				{
					int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
					String bitacora = llamada_bitacora(referencia,codigo,br.getContractNumber(),br.getUserID8()," ",0.0,NomPreUtil.ES_NP_TARJETA_CONSULTA_ALTA_CANCELA_OPER," ",0," ");
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
				}

			} else if (codigo != null){

				mensaje = "Error al cancelar el archivo";

			} else {

				mensaje = "Servicio no disponible";
			}

			generaMensaje(request, mensaje);
			request.getSession().removeAttribute(SER_ARCH_CONSULTA_ARC);
			request.setAttribute("inicial", "true");
			detallar(request, response);
		}
	}

	private void enviar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String correoElectronico = request.getParameter("correoElec");
		String secuencia = request.getParameter("archivo");

		if (secuencia != null && secuencia.indexOf("|") != -1
				&& correoElectronico != null && correoElectronico.indexOf("@") != -1) {

			int index = secuencia.indexOf("|");

			secuencia = secuencia.substring(0, index);
			correoElectronico = correoElectronico.replaceAll("@", "[]");

			final BaseResource br = obtenerBaseResource(request);

			NomPreAltaMasivaDAO dao = new NomPreAltaMasivaDAO();

			String salida = dao.enviarDetalleCorreo(br.getUserID8(), br.getContractNumber(), br.getUserProfile(), secuencia, correoElectronico);
			String codigo = null;
			String mensaje = null;

			if (salida != null && salida.length() > 0) {

				codigo = salida.substring(0, 8);
			}

			BitaTransacBean bt = new BitaTransacBean();
			bt.setUsr(br.getUserID8());
			bt.setContrato(br.getContractNumber());
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_ALTA_CORREO);
			bitacoriza(request, "NPEA", codigo, bt);

			if (COD_EXITO.equals(codigo)) {

				mensaje = "Detalle de archivo de alta masiva enviado exitosamente";

			} else if (codigo != null){

				mensaje = "Error al enviar el detalle del archivo de alta masiva";

			} else {

				mensaje = "Servicio no disponible";
			}

			generaMensaje(request, mensaje);
			detallar(request, response);
		}
	}

	private void generaMensaje(HttpServletRequest request, String mensaje) {
		request.setAttribute("mensajeconestado", mensaje);
	}

	private void detallar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		String fechaRecepcion = request.getParameter("fecha1");
		String fechaAplicacion = request.getParameter("fecha2");
		String secuencia = request.getParameter("secuencia");
		String[] estados = request.getParameterValues("chkEstado");

		NomPreAltaMasivaDAO dao = new NomPreAltaMasivaDAO();
		final BaseResource br = obtenerBaseResource(request);

		List<NomPreAltaMasiva> detalle = null;
		String include = null;

		if ("true".equals(request.getParameter("inicial")) || "true".equals(request.getAttribute("inicial"))) {

			EIGlobal.mensajePorTrace("Consulta inicial " + fechaRecepcion + " " + fechaAplicacion + " " + secuencia + " " + (estados == null || estados.length == 0 ? "" : Arrays.toString(estados)), EIGlobal.NivelLog.INFO);

			String salida = dao.obtenerDetalleArchivos(br.getContractNumber(), br.getUserID8(), br.getUserProfile(), request.getSession().getId(), fechaRecepcion, fechaAplicacion, secuencia, estados);
			String archivo = null;
			String codigo = null;

			if (salida != null && salida.length() > 0) {

				codigo = salida.substring(0, 8);
			}

			BitaTransacBean bt = new BitaTransacBean();
			bt.setUsr(br.getUserID8());
			bt.setContrato(br.getContractNumber());
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_ALTA_CONSULTA);
			bitacoriza(request, "NPCT", codigo, bt);

			EIGlobal.mensajePorTrace("Codigo operacion: " + codigo, EIGlobal.NivelLog.INFO);

			if (COD_EXITO.equals(codigo)) {

				archivo = salida.substring(9, salida.length() - 1);

				EIGlobal.mensajePorTrace("Archivo salida AMD: [" + salida + "]", EIGlobal.NivelLog.INFO);

			} else if (codigo == null){

				generaMensaje(request, "Servicio no disponible");

				request.getRequestDispatcher(NOM_PRE_CONSULTA_ESTADO_ASIGNACION_DETALLE).include(request, response);

				return;
			}

			if (archivo != null && archivo.length() > 0) {

				archivo = archivo.substring(archivo.lastIndexOf("/") + 1);

				ArchivoRemoto remoto = new ArchivoRemoto();

				EIGlobal.mensajePorTrace("Obteniendo archivo : " + archivo, EIGlobal.NivelLog.INFO);

				boolean result = remoto.copia(archivo, Global.DIRECTORIO_REMOTO_INTERNET, "2");

				if (!result) {
					return;
				}

				File file = new File(IEnlace.DOWNLOAD_PATH + archivo);

				if (!file.exists() || !file.canRead()) {
					return;
				}

				EIGlobal.mensajePorTrace("Leyendo archivo : " + archivo, EIGlobal.NivelLog.INFO);

				BufferedReader reader = null;

				try {

					reader = new BufferedReader(new FileReader(file));
					String line = null;

					while ((line = reader.readLine()) != null) {

						if (detalle == null) {
							detalle = new ArrayList<NomPreAltaMasiva>();
						}

						String[] contenido = null;

						if (line.length() > 0 && (contenido = line.split(";")).length == 7) {

							detalle.add(new NomPreAltaMasiva(contenido[0], contenido[1], contenido[2], contenido[3], contenido[4], contenido[5], contenido[6]));
						}
					}

				} finally {

					if (reader != null) {

						reader.close();
					}
				}
			}

			String archivoSer = NomPreUtil.saveObjectToFile((ArrayList<NomPreAltaMasiva>) detalle, session);

			session.setAttribute(SER_ARCH_CONSULTA_ARC, archivoSer);

		} else {

			EIGlobal.mensajePorTrace("Obtiene existente " + fechaRecepcion + " " + fechaAplicacion + " " + secuencia + " " + (estados == null || estados.length == 0 ? "" : Arrays.toString(estados)), EIGlobal.NivelLog.INFO);

			String archivoSer = session.getAttribute(SER_ARCH_CONSULTA_ARC).toString();

			detalle = (ArrayList<NomPreAltaMasiva>) NomPreUtil.objectFromFile(archivoSer);
		}

		EIGlobal.mensajePorTrace("Tamano lista " + (detalle == null ? "ninguno:null" : detalle.size()), EIGlobal.NivelLog.INFO);

		request.setAttribute("detalle", detalle);

		if ("excel".equals(request.getParameter("salida"))) {

			configurarSalidaReporte(response, "detalleAltaMasiva");

			include = NOM_PRE_EXP_ESTADO_ASIGNACION;

			BitaTransacBean bt = new BitaTransacBean();
			bt.setUsr(br.getUserID8());
			bt.setContrato(br.getContractNumber());
			bt.setNombreArchivo("detalleAltaMasiva.xls");
			bitacoriza(request, "NPCT", null, bt);

		} else {

	        request.setAttribute("diasInhabiles", diasInhabilesAnt());

	        include = NOM_PRE_CONSULTA_ESTADO_ASIGNACION_DETALLE;
		}

		request.getRequestDispatcher(include).include(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void configurarSalidaReporte(HttpServletResponse response, String nombreArchivo) throws IOException {

		response.setContentType("application/vnd.ms-excel");

		response.setHeader("Content-disposition", "attachment; filename="
				+ nombreArchivo + ".xls");
	}
}