
/** Banco Santander Mexicano
*   Clase              SuaEnviar Envio del Archivo SUA
*   @autor             Ruben Fragoso
*   @version           1.0
*   fecha de creacion: 2 de marzo del 2001
*   responsable      : Mariela Betancourt
*   descripcion      : Ejecuta servicios del web_red para enviar el archivo SUA
*                     y realizar el cargo-abono a la cuenta seleccionada por el usuario.
*/

package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class SuaEnviar extends BaseServlet{

    private String tipo_operacion        = "";

	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
            //Se agregaron estas dos lineas antes de SesionValida
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if ( SesionValida( request, response ) )
		{
            /******************************************Inicia validacion OTP**************************************/
			String valida = request.getParameter ( "valida" );
			if(validaPeticion( request,  response,session,sess,valida))
			{
				EIGlobal.mensajePorTrace("SI---------------***ENTRA A VALIDAR LA PETICION *********", EIGlobal.NivelLog.DEBUG);
			}
			/******************************************Termina validacion OTP**************************************/
			//interrumpe la transaccion para invocar la validación
            else{

				///////////////////--------------challenge--------------------------
				String validaChallenge = request.getAttribute("challngeExito") != null
					? request.getAttribute("challngeExito").toString() : "";

				EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

				if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
					EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
					ValidaOTP.guardaParametrosEnSession(request);

					validacionesRSA(request, response);
					return;
				}

				//Fin validacion rsa para challenge
				///////////////////-------------------------------------------------


            	boolean valBitacora = true;
            	if(  valida==null)
			{
				EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacción está parametrizada para solicitar la validación con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

				boolean solVal=ValidaOTP.solicitaValidacion(
							session.getContractNumber(),IEnlace.SUA);

				if( session.getValidarToken() &&
							    session.getFacultad(session.FAC_VAL_OTP) &&
							    session.getToken().getStatus() == 1 &&
							    solVal)
				{
					//VSWF-HGG -- Bandera para guardar el token en la bitácora
					request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
					EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicitó la validación. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);
					//guardaParametrosEnSession(request);
					ValidaOTP.guardaParametrosEnSession(request);

					EIGlobal.mensajePorTrace("TOKEN SUA DISCO", EIGlobal.NivelLog.INFO);
					request.getSession().removeAttribute("mensajeSession");
					ValidaOTP.mensajeOTP(request, "SUA_Disco");

					ValidaOTP.validaOTP(request,response, IEnlace.VALIDA_OTP_CONFIRM);
				} else {
					valida="1";
					valBitacora = false;
					ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
				}
			}
			//retoma el flujo
			if( valida!=null && valida.equals("1"))
			{


				if (valBitacora)
				{
					try{
						EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

						int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
						String claveOperacion = BitaConstants.CTK_PAGO_SUA;
						String concepto = BitaConstants.CTK_CONCEPTO_PAGO_SUA;
						String token = "0";
						if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
						{token = request.getParameter("token");}
						String cuenta = "0";
						double importeDouble = 0;

						if(request.getParameter("cboCuentaCargo") !=null && !request.getParameter("cboCuentaCargo").equals(""))
						{String[] cboCuentaCargo = desentramaC("3|" + request.getParameter("cboCuentaCargo"), '|');
						 cuenta = cboCuentaCargo[1];}

						if(request.getParameter("total_a_pagar") !=null && !request.getParameter("total_a_pagar").equals(""))
						{String importe = request.getParameter("total_a_pagar");
						 importeDouble = Double.valueOf(importe).doubleValue();}

						EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

						String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
					} catch(Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						//EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

					}
				}
			enviarDisco( request, response );
                        ValidaOTP.guardaBitacora((List)sess.getAttribute("bitacora"),tipo_operacion);
                        }
		}

              }
	}

	/** El metodo enviarDisco() envia el archivo SUA
	*   y realiza el cargo-abono.
	*/
	public void enviarDisco( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace( "***SuaEnviar.class & Entrando a pantalla enviarDisco &", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		// Clase que hace llamadas a Tuxedo.
		//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		HttpSession ses = request.getSession();

		Hashtable htResult = null;
		String codErrorOper = "";

		int salida = 0;
		String contrato = "";
		String coderror = "";
		short  suc_opera = ( short ) 787;
		String usuario = "";
		String clave_perfil = "";

		//Variables de sesion
		contrato = session.getContractNumber();
		suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		usuario = session.getUserID8();
		clave_perfil = session.getUserProfile();

		String trama_entrada_ref = "";// trama para obtener la referencia
		String trama_entrada_bor = "";// trama para borrar una referencia
		String trama_entrada_car = "";// trama para realizar el cargo-abono

		trama_entrada_ref = request.getParameter( "trama_referencia" );

		String[] cboCuentaCargo = desentramaC("3|" + request.getParameter("cboCuentaCargo"), '|');
		String txtFolioAutoriza = request.getParameter("txtFolioAutoriza"); //por verificar
		String total_a_pagar = request.getParameter("total_a_pagar");
		String nombre_archivo = request.getParameter("nombre_archivo");
		String strRegPatronal = request.getParameter("strRegPatronal");
		String strFolio = request.getParameter("strFolio");

		EIGlobal.mensajePorTrace( "***SuaEnviar.class & strRegPatronal : >" +strRegPatronal+ "<", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***SuaEnviar.class & strFolio : >" +strFolio+ "<", EIGlobal.NivelLog.INFO);

		strRegPatronal = strRegPatronal.trim();
		strFolio = strFolio.trim();

		String archivosServidor = request.getParameter("archivosServidor");

		EIGlobal.mensajePorTrace( "***SuaEnviar.class & trama_entrada_ref : " +trama_entrada_ref+ " &", EIGlobal.NivelLog.INFO);

		String retCodeRedSrvr        = "";
		String retCodeRecaSrvr       = "";
		String[] arrSalida;
		String referencia            = "";
		String trama_salida          = "";
		String strMensaje            = "";
		String strDebug              = "";
		String strBoton              = "";
		String encabezado            = "";
		String trama_mancomunidad    = "";

		boolean errFlag = true;
		boolean enviaNotificacion=false;

		String medio_entrega         = "1EWEB";

		String nombre_archivo_remoto = "";
		String nombreArchivoSua      = "";
		//<vswf:meg cambio de NASApp por Enlace 08122008>
		strBoton =  "<a href =\"/Enlace/" +Global.WEB_APPLICATION+ "/SuaDisco?ventana=0\">";
		strBoton += "<img border=\"0\" src=\"/gifs/EnlaceMig/gbo25320.gif\"";
		strBoton += "width=\"83\" height=\"22\" alt=\"Regresar\"></a>";

		tipo_operacion = "RE01";
		encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato;
		encabezado += "|" + usuario + "|" + clave_perfil + "|";

		trama_entrada_ref = encabezado.trim() + trama_entrada_ref.trim();

		strDebug += "\nter :" + trama_entrada_ref;
		EIGlobal.mensajePorTrace( "***SuaEnviar.class >> trama_entrada_ref : " +trama_entrada_ref+ " <<", EIGlobal.NivelLog.DEBUG);


		try {
			htResult = tuxGlobal.web_red( trama_entrada_ref );
			codErrorOper = htResult.get("COD_ERROR") + "";
		} catch ( Exception re ) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
			//re.printStackTrace();
			//CSA -----------------------
			htResult = new Hashtable(2);
			//---------------------------
		}

		trama_salida = ( String ) htResult.get( "BUFFER" );

		strDebug += "\ntsr :" + trama_salida;
		EIGlobal.mensajePorTrace( "***SuaEnviar.class >> trama_salida : " +trama_salida+ " <<", EIGlobal.NivelLog.DEBUG);

		//CSA -------------------------------------
		if((trama_salida!=null)){
			retCodeRedSrvr = trama_salida.substring(0, 8).trim();
		}else{
			retCodeRedSrvr = "";
		}
		EIGlobal.mensajePorTrace( "***SuaEnviar.class >> codigo RED:" +retCodeRedSrvr+ "<<", EIGlobal.NivelLog.INFO);

		//-----------------------------------------

		if ( retCodeRedSrvr.equals( "OK" ) )
		{
			retCodeRecaSrvr = trama_salida.substring(16, 24);
			if (retCodeRecaSrvr.equals("RECA0000") )
			{
				// Transaccion realizada correctamente
				arrSalida = desentramaC("3|" + trama_salida,'|');
				referencia = arrSalida[2];
				errFlag = false;
			} else if( retCodeRecaSrvr.equals("RECA0125")) {
				// El Folio esta incompleto
				arrSalida = desentramaC("2|" + trama_salida,'|');
				referencia = arrSalida[2];
				tipo_operacion = "RE02";
				encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|";
				encabezado += usuario + "|" + clave_perfil + "|";
				trama_entrada_bor = encabezado + referencia + "@";
				strDebug += "\nteb :" + trama_entrada_bor;

				// Borra la referencia
				try {
					htResult = tuxGlobal.web_red( trama_entrada_bor );
				} catch ( Exception re ) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
					//re.printStackTrace();
					//CSA -----------------------
					htResult = new Hashtable(2);
					//---------------------------
				}// catch (Exception e) {}

				trama_salida = ( String ) htResult.get( "BUFFER" );
				strDebug += "\ntsb :" + trama_salida;

				//CSA ----------------------------------
				if((trama_salida != null)){
					retCodeRedSrvr = trama_salida.substring(0, 8).trim();
				}else{
					retCodeRedSrvr = "";
				}
				EIGlobal.mensajePorTrace( "***SuaEnviar.class >> codigo RED:" +retCodeRedSrvr+ "<<", EIGlobal.NivelLog.INFO);
				//--------------------------------------

				if ( retCodeRedSrvr.equals( "OK" ) )
				{
					// El web_red genero una respuesta
					retCodeRecaSrvr = trama_salida.substring(16, 24);
					if ( retCodeRecaSrvr.equals( "RECA0000" ) )
					{
						// Transaccion realizada correctamente
						strDebug += "\nter :" + trama_entrada_ref;

						try {
							htResult = tuxGlobal.web_red( trama_entrada_ref );
						} catch ( Exception re ) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
							//re.printStackTrace();
							//CSA -----------------------
							htResult = new Hashtable(2);
							//-----------------------------
						} //catch(Exception e) {}

						trama_salida = ( String ) htResult.get( "BUFFER" );	// Genera una referencia
						strDebug += "\ntsr :" + trama_salida;

						//CSA ---------------------
						if(trama_salida != null){
							retCodeRedSrvr = trama_salida.substring(0, 8).trim();
						}else{
							retCodeRedSrvr = "";
						}
						EIGlobal.mensajePorTrace( "***SuaEnviar.class >> codigo RED:" +retCodeRedSrvr+ "<<", EIGlobal.NivelLog.INFO);
						//-------------------------


						if ( retCodeRedSrvr.equals( "OK" ) )
						{
							// El web_red genero una respuesta
							retCodeRecaSrvr = trama_salida.substring(16, 24);
							if ( retCodeRecaSrvr.equals( "RECA0000" ) )
							{
								// Transaccion realizada correctamente
								arrSalida = desentramaC("3|" + trama_salida,'|');
								referencia = arrSalida[2];
								errFlag = false;
//								TODO: BIT CU 4101, A9
								/*
					    		 * VSWF ARR -I
					    		 */
								if (Global.USAR_BITACORAS.trim().equals("ON")){
								try{
								BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ES_AP_OP_PAGOS_REALIZA_PAGO_IMSS);
								bt.setContrato(contrato);
								if(referencia!=null && !referencia.equals(""))
									bt.setReferencia(Long.parseLong(referencia));
								bt.setNombreArchivo(nombre_archivo);
								if(retCodeRecaSrvr!=null){
									 if(retCodeRecaSrvr.substring(0,2).equals("OK")){
										   bt.setIdErr("RECA0000");
									 }else if(retCodeRecaSrvr.length()>8){
										  bt.setIdErr(retCodeRecaSrvr.substring(0,8));
									 }else{
										  bt.setIdErr(retCodeRecaSrvr.trim());
									 }
									}
								bt.setServTransTux(tipo_operacion);
								if(cboCuentaCargo[1]!=null && !cboCuentaCargo[1].equals(""))
									bt.setCctaOrig(cboCuentaCargo[1]);
								if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
											.equals(BitaConstants.VALIDA))
									bt.setIdToken(session.getToken().getSerialNumber());
								BitaHandler.getInstance().insertBitaTransac(bt);
								}catch(SQLException e){
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									//e.printStackTrace();
								}catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									//e.printStackTrace();
								}
								}
					    		/*
					    		 * VSWF ARR -F
					    		 */

							} else if ( retCodeRecaSrvr.substring( 0, 4 ) == "RECA" )
								// El recasrvr genero una respuesta de error
								strMensaje += "Error: " + trama_salida.substring( 16, trama_salida.length() );
							else
								strMensaje += "No es posible la comunicaci&oacute;n con el servicio RE_REFERENCIA. Intente m&aacute;s tarde.";

						} else if ( retCodeRedSrvr.equals( "MANC" ) )
							//El web_red genero un error de mancomunidad
							strMensaje += "Mancomunidad: " + trama_salida.substring( 8, trama_salida.length() );
						else
							strMensaje += "Problemas con la comunicaci&oacute;n";
					} else if ( retCodeRecaSrvr.substring( 0, 4 ) == "RECA" )
						// El recasrvr genero una respuesta de error
						strMensaje += "El Folio ya existe y no pudo ser actualizado.";
					else
						strMensaje += "No es posible la comunicaci&oacute;n con el servicio RE_BORREF. Intente m&aacute;s tarde";
				} else if ( retCodeRedSrvr.equals( "MANC" ) )
					// El web_red genero un error de mancomunidad
					strMensaje += "Mancomunidad: " + trama_salida.substring(8, trama_salida.length());
				else{
					strMensaje += "Problemas con la comunicaci&oacute;n";
					}
			} else if ( retCodeRecaSrvr.equals( "RECA9018" ) )
				// El Folio ya fue pagado
				strMensaje = "El Folio est&aacute; Duplicado.";
			else if ( retCodeRecaSrvr.substring( 0, 4 ) == "RECA" )
				// El recasrvr genero una respuesta de error
				strMensaje += "Error: " + trama_salida.substring( 16, trama_salida.length() );
			else
				strMensaje += "No es posible la comunicaci&oacute;n con el servicio RE_REFERENCIA.intente m&aacute;s tarde.";

			if( errFlag == false )
			{
				if ( txtFolioAutoriza.equals( null ) || txtFolioAutoriza.equals( "" ) )
					txtFolioAutoriza = " ";

				nombreArchivoSua = nombre_archivo.substring( nombre_archivo.lastIndexOf( '/' ) + 1 , nombre_archivo.length() );
				nombre_archivo_remoto = Global.DIRECTORIO_REMOTO + "/" + nombreArchivoSua;

				// Copia Remota
				ArchivoRemoto archE = new ArchivoRemoto();

				if ( !archE.copiaLocalARemoto( nombreArchivoSua ) )
				{
					EIGlobal.mensajePorTrace( "***SuaEnviar.class & enviarArhivo: No se realizo el envio del archivo. &", EIGlobal.NivelLog.INFO);
				} else {
					/*if ( !archE.cambiaAtributoRemoto( nombreArchivoSua, "777" ) )
						EIGlobal.mensajePorTrace( "***SuaEnviar.class & cambiaAtributo: No se realizo el cambio de atgributos del archivo. &", EIGlobal.NivelLog.INFO);
					else
						EIGlobal.mensajePorTrace( "***SuaEnviar.class & cambiaAtributo: Cambio OK. &", EIGlobal.NivelLog.INFO);*/
					EIGlobal.mensajePorTrace( "***SuaEnviar.class & enviarArhivo: enviarArhivo: Envio OK. &", EIGlobal.NivelLog.DEBUG);
				}
				tipo_operacion = "RE03";

				encabezado = medio_entrega+ "|" +usuario+ "|" +tipo_operacion+ "|" +contrato+ "|" +usuario+ "|" +clave_perfil+ "|";

				EIGlobal.mensajePorTrace( "***SuaEnviar.class & NOMBRE = >" +nombre_archivo_remoto+ "<", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***SuaEnviar.class & REFERENCIA = >" +referencia+ "<", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***SuaEnviar.class & TOTAL A PAGAR = >" +total_a_pagar+ "<", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***SuaEnviar.class & FOLIO = >" +txtFolioAutoriza+ "<", EIGlobal.NivelLog.INFO);

				trama_entrada_car = nombre_archivo_remoto.trim()+ "@" +referencia+ "@" +cboCuentaCargo[1]+ "@" +total_a_pagar.trim()+ "@";
				trama_mancomunidad = "|" +txtFolioAutoriza+ "|";
				trama_entrada_car = encabezado.trim() + trama_entrada_car.trim() + trama_mancomunidad.trim();

				strDebug += "\ntec :" + trama_entrada_car;

				EIGlobal.mensajePorTrace( "***SuaEnviar.class >> trama_entrada_car : " +trama_entrada_car+ " <<", EIGlobal.NivelLog.DEBUG);

				// Realiza el Cargo-Abono
				try {
					htResult = tuxGlobal.web_red( trama_entrada_car );
				} catch ( Exception re ) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
					//re.printStackTrace();
					//CSA --------------------------
					htResult = new Hashtable(2);
					//---------------------------
				} //catch (Exception e) {}

				trama_salida = ( String ) htResult.get( "BUFFER" );

				strDebug += "\ntsc :" + trama_salida;
				EIGlobal.mensajePorTrace( "***SuaEnviar.class >> trama_salida : " + trama_salida + " <<", EIGlobal.NivelLog.DEBUG);
				//CSA ----------------------------------
				if(trama_salida!=null){
					retCodeRedSrvr = trama_salida.substring( 0, 8 ).trim();
				}else{
					retCodeRedSrvr = "";
				}
				EIGlobal.mensajePorTrace( "***SuaEnviar.class >> codigo RED:" +retCodeRedSrvr+ "<<", EIGlobal.NivelLog.INFO);
				// --------------------------------------


				if ( retCodeRedSrvr.equals( "OK" ) )
				{
					// El web_red genero una respuesta
					retCodeRecaSrvr = trama_salida.substring(16, 24);
					if ( retCodeRecaSrvr.equals( "RECA0000" ) )
					{enviaNotificacion=true;
						// Transaccion realizada correctamente
						strMensaje += "Cargo realizado correctamente.  Referencia: " + referencia;
						strBoton    = "<a href =\"javascript:generaComprobante(document.frmDisco);\">";
						strBoton   += "<img border=\"0\" src=\"/gifs/EnlaceMig/gbocomprobante.gif\"";
						strBoton   += "alt=\"Comprobante\"></a>";
						errFlag = false;

					try {
						int cuantos = EIGlobal.CuantosTokens(archivosServidor, '|');
						File drvCon;
						String strArchivo = "";
						for( int indice = 1; indice <= cuantos; indice++ )
						{
							strArchivo = EIGlobal.BuscarToken( archivosServidor, '|', indice );
							drvCon = new File( strArchivo );
							drvCon.delete();
						}
//						TODO: BIT CU 4101, A9
						/*
			    		 * VSWF ARR -I
			    		 */
						if (Global.USAR_BITACORAS.trim().equals("ON")){
						try{
						BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ES_AP_OP_PAGOS_REALIZA_PAGO_IMSS);
						bt.setContrato(contrato);

						if(retCodeRecaSrvr!=null){
							 if(retCodeRecaSrvr.substring(0,2).equals("OK")){
								   bt.setIdErr("RECA0000");
							 }else if(retCodeRecaSrvr.length()>8){
								  bt.setIdErr(retCodeRecaSrvr.substring(0,8));
							 }else{
								  bt.setIdErr(retCodeRecaSrvr.trim());
							 }
							}
						if(referencia!=null && !referencia.equals(""))
							try {
								bt.setReferencia(Long.parseLong(referencia));
							} catch (NumberFormatException e) {
								bt.setReferencia(0);
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								//EIGlobal.mensajePorTrace( "Error al parsear referencia a long para bitacorizacion: " + referencia, EIGlobal.NivelLog.ERROR);
							}
						bt.setNombreArchivo(nombre_archivo);
						if(tipo_operacion!=null && !tipo_operacion.equals(""))
							bt.setServTransTux(tipo_operacion);
						if(cboCuentaCargo[1]!=null && !cboCuentaCargo[1].equals(""))
							bt.setCctaOrig(cboCuentaCargo[1]);
						if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
							&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
									.equals(BitaConstants.VALIDA))
							bt.setIdToken(session.getToken().getSerialNumber());
						BitaHandler.getInstance().insertBitaTransac(bt);
						}catch(SQLException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							//e.printStackTrace();
						}catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							//e.printStackTrace();
						}
						}
			    		/*
			    		 * VSWF ARR -F
			    		 */
					} catch ( Exception ioeSua ) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
						//EIGlobal.mensajePorTrace( "***SuaEnviar.class Excepcion %enviarDisco() Error al borrar los archivos: >> " +ioeSua.getMessage()+ "<<", EIGlobal.NivelLog.INFO);
					}
				} else if ( retCodeRecaSrvr.substring( 0, 4 ).equals( "RECA" ) )
					// El recasrvr genero una respuesta de error
					strMensaje += "No se pudo realizar el Cargo. " + trama_salida.substring(9, trama_salida.length() );
				else
					strMensaje += "No se pudo realizar el Cargo. " + trama_salida.substring(16, trama_salida.length());
				} else if ( retCodeRedSrvr.substring( 0, 4 ).equals( "MANC" ) )
					// El web_red genero un error de mancomunidad
					strMensaje += "Mancomunidad: " + trama_salida.substring(8, trama_salida.length());
				else if (retCodeRedSrvr.startsWith("ALYM" )) {
					strMensaje += "Error: " + retCodeRedSrvr + " " +
						trama_salida.substring(16, trama_salida.length());
				} else {
					strMensaje += "Problemas con la comunicación.";
				}
			}
			// RedSrvr = OK
//			TODO: BIT CU 4101, A8
			/*
    		 * VSWF ARR -I
    		 */
			if (Global.USAR_BITACORAS.trim().equals("ON")){
			try{
			BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_AP_OP_PAGOS_OBTIENE_REF_SUA);
			bt.setContrato(contrato);
			if(retCodeRecaSrvr!=null){
				 if(retCodeRecaSrvr.substring(0,2).equals("OK")){
					   bt.setIdErr("RECA0000");
				 }else if(retCodeRecaSrvr.length()>8){
					  bt.setIdErr(retCodeRecaSrvr.substring(0,8));
				 }else{
					  bt.setIdErr(retCodeRecaSrvr.trim());
				 }
				}
			if(referencia!=null && !referencia.equals("")) {
				try {
					bt.setReferencia(Long.parseLong(referencia));
				} catch(NumberFormatException e) {
					bt.setReferencia(0);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					//EIGlobal.mensajePorTrace("Error en parseo de referencia: <" + referencia + ">", EIGlobal.NivelLog.ERROR);
				}
			}
			bt.setNombreArchivo(nombre_archivo);
			if(tipo_operacion!=null && !tipo_operacion.equals(""))
				bt.setServTransTux(tipo_operacion);
			if(cboCuentaCargo[1]!=null && !cboCuentaCargo[1].equals(""))
				bt.setCctaOrig(cboCuentaCargo[1]);
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				//e.printStackTrace();
			}catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				//e.printStackTrace();
			}
			}
    		/*
    		 * VSWF ARR -F
    		 */
		} else if ( retCodeRedSrvr.substring( 0, 4 ).equals( "MANC" ) )
			// El web_red genero un error de mancomunidad
			strMensaje += "Mancomunidad: " + trama_salida.substring(8, trama_salida.length());
		else
			strMensaje += "Problemas con la comunicación.";


		EIGlobal.mensajePorTrace("Inicio de Notificacion por Correo", EIGlobal.NivelLog.INFO);
		try {
		if (enviaNotificacion) {

			EmailSender emailSender = new EmailSender();
			EmailDetails beanEmailDetails = new EmailDetails();
			if(emailSender.enviaNotificacion(codErrorOper)){
					EIGlobal.mensajePorTrace("NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("CuentaCargo ->" + cboCuentaCargo[1], EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("Importe ->" + total_a_pagar, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("Referencia ->" + referencia, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("codErrorOper ->" + codErrorOper, EIGlobal.NivelLog.INFO);

					beanEmailDetails.setNumeroContrato(session.getContractNumber());
					beanEmailDetails.setRazonSocial(session.getNombreContrato());
					beanEmailDetails.setNumCuentaCargo(cboCuentaCargo[1]);
					beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(total_a_pagar));
					beanEmailDetails.setNumRef(referencia);
					beanEmailDetails.setTipoPagoImp("SUA");
					beanEmailDetails.setEstatusActual("ENVIADO");
					EIGlobal.mensajePorTrace("Estatus->" + beanEmailDetails.getEstatusActual(), EIGlobal.NivelLog.INFO);

							beanEmailDetails.setEstatusActual(codErrorOper);
							emailSender.sendNotificacion(request,IEnlace.PAGO_SUA,beanEmailDetails);
					 }
		} else {
			EIGlobal.mensajePorTrace("°°°°°°°°°°°°°°°°°°°° No manda mensaje. Resultado de la operacion ->" + retCodeRedSrvr, EIGlobal.NivelLog.INFO);
		}
		} catch (Exception e) {
				EIGlobal.mensajePorTrace("Excepcion al enviar el correo ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}

		/////////////////////////////////////////////////////////

		String fecha_hoy=ObtenFecha();

		//EIGlobal.mensajePorTrace( "***SuaEnviar.class & strSalida : " + strSalida + " &", EIGlobal.NivelLog.INFO);
		//EIGlobal.mensajePorTrace( "***SuaEnviar.class & fecha_hoy : " + fecha_hoy + " &", EIGlobal.NivelLog.INFO);

		if(ses.getAttribute("newMenu")!=null)
			ses.removeAttribute("newMenu");
		if(ses.getAttribute("MenuPrincipal")!=null)
			ses.removeAttribute("MenuPrincipal");
		if(ses.getAttribute("Encabezado")!=null)
			ses.removeAttribute("Encabezado");
		if(ses.getAttribute("mensaje_debug")!=null)
			ses.removeAttribute("mensaje_debug");
		if(ses.getAttribute("mensaje_salida")!=null)
			ses.removeAttribute("mensaje_salida");
		if(ses.getAttribute("strRegPatronal")!=null)
			ses.removeAttribute("strRegPatronal");
		if(ses.getAttribute("strFolio")!=null)
			ses.removeAttribute("strFolio");
		if(ses.getAttribute("btnGeneral")!=null)
			ses.removeAttribute("btnGeneral");
		if(ses.getAttribute("web_application")!=null)
			ses.removeAttribute("web_application");
		if(ses.getAttribute("comp_usuario")!=null)
			ses.removeAttribute("comp_usuario");
		if(ses.getAttribute("comp_contrato")!=null)
			ses.removeAttribute("comp_contrato");
		if(ses.getAttribute("comp_perfil")!=null)
			ses.removeAttribute("comp_perfil");

		ses.setAttribute("newMenu",        session.getFuncionesDeMenu());
		ses.setAttribute("MenuPrincipal",  session.getStrMenu());
		ses.setAttribute("Encabezado",     CreaEncabezado("Pagos del S.U.A.","Servicios &gt; Aportaciones Obrero Patronales (S.U.A.) &gt; Pagos", "s25700h", request));
		ses.setAttribute("mensaje_debug",  strDebug);
		ses.setAttribute("mensaje_salida", "<B>" + strMensaje + "<B>");

		ses.setAttribute("strRegPatronal", strRegPatronal);
		ses.setAttribute("strFolio",       strFolio);
		ses.setAttribute("btnGeneral",     strBoton);

		//modif PVA 06/08/2002
		ses.setAttribute("web_application",Global.WEB_APPLICATION);
		ses.setAttribute("comp_usuario", session.getUserID8());
		ses.setAttribute("comp_contrato",session.getContractNumber());
		ses.setAttribute("comp_perfil",session.getUserProfile());

		//sesDispatcher rdSalida = getServletContext().
		//getRequestDispatcher( "/jsp/SuaMensajes.jsp" );
		//rdSalida.include( request, response );
		//response.sendRedirect(response.encodeRedirectURL("/NASApp/"+Global.WEB_APPLICATION+"/jsp/SuaMensajes.jsp"));
		  evalTemplate("/jsp/SuaMensajes.jsp", request, response);

	}


        public static void guardaParametrosEnSession (HttpServletRequest request){

          EIGlobal.mensajePorTrace("-aaaaa111111---------Interrumple el flujo--", EIGlobal.NivelLog.DEBUG);
          String template= request.getServletPath();
          EIGlobal.mensajePorTrace("5-****--------------->template**********--->"+template, EIGlobal.NivelLog.DEBUG);
          request.getSession().setAttribute("plantilla",template);

          HttpSession session = request.getSession(false);
          session.removeAttribute("bitacora");

        if(session != null){

            Map tmp = new HashMap();
         //   Enumeration enumer = request.getParameterNames();

                EIGlobal.mensajePorTrace("6665555-****--------------->enumer**********--->"+request.getParameter( "valida" ), EIGlobal.NivelLog.DEBUG);
          /*  while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
               EIGlobal.mensajePorTrace(name+"----valor-------"+request.getParameter(name), EIGlobal.NivelLog.INFO);

                tmp.put(name,request.getParameter(name));
            }
           */
                 tmp.put("trama_referencia",request.getParameter("trama_referencia"));
                  tmp.put("cboCuentaCargo",request.getParameter("cboCuentaCargo"));
                   tmp.put("txtFolioAutoriza",request.getParameter("txtFolioAutoriza"));
                    tmp.put("total_a_pagar",request.getParameter("total_a_pagar"));
                     tmp.put("nombre_archivo",request.getParameter("nombre_archivo"));
                      tmp.put("strRegPatronal",request.getParameter("strRegPatronal"));
                       tmp.put("strFolio",request.getParameter("strFolio"));
                        tmp.put("archivosServidor",request.getParameter("archivosServidor"));




            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
           Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }

    }
}