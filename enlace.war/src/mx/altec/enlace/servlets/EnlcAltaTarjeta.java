package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.EnlcTarjetaBO;
import mx.altec.enlace.bo.EnlcTarjetaBOImpl;
import mx.altec.enlace.dao.DaoException;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
/*Inicia - EVERIS VALIDACION CSS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION CSS */

/**
 * Servlet implementation class alta de tarjetas
 */
public class EnlcAltaTarjeta extends BaseServlet {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Log
	 */
	private static final String TEXTOLOG = EnlcAltaTarjeta.class.getName();
	/**
	 * despl
	 */
	private static final String DESPL = "despl";
	/**
	 * tarjetasRes
	 */
	private static final String TARJETASRES = "tarjetasRes";
	/**
	 * tarjetasResLinea
	 */
	private static final String TARJETASRESLINEA = "tarjetasResLinea";
	/**
	 * start
	 */
	private static final String START = "start";
	/**
	 * Metodo que recibe las peticiones get
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(verificaFacultad ("ALTVINCTARJ",request)){
			if (!SesionValida(request, response)) {
				request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response);
				return;
			}
			request.getSession().removeAttribute("MensajeLogin01");
			HttpSession sess;
			sess = request.getSession();
			BaseResource session;
			session = (BaseResource) sess.getAttribute("session");
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request.setAttribute("newMenu", session.getFuncionesDeMenu());

			request.setAttribute("Encabezado",    CreaEncabezado("Alta en linea",
					"Servicios &gt; Tarjeta de Pagos Santander &gt; Vinculaci&oacute;n tarjeta &ndash; contrato &gt Alta", "", request ) );

			int operacion;
			operacion = Integer.parseInt(request.getParameter("operacion"));
			EIGlobal.mensajePorTrace(TEXTOLOG + "Operación: " + operacion, EIGlobal.NivelLog.INFO);
			switch (operacion) {
			case 1:
		    	limpiarRegresar(request, response);
		    	break;
		    case 2:
		    	validarTarjeta(request, response);
		    	break;
		    case 3:
		    	validarRegistroArchivo(request, response);
		    	break;
		    case 4:
		    	pagTarjetaArch(request, response);
		    	break;

		    case 5:
		    	enviarTarjeta(request, response);
		    	break;
		    case 6:
		    	pagTarjetaArchFinal(request, response);
		    	break;

		    case 7:
		    	registrarAltaTarjeta(request, response);
		    	break;
			default:
				limpiarRegresar(request, response);
				break;
		    }

		} else {
			request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(request, response);
		}
	}

	/**
	 * Metodo que recibe las peticiones post
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Inicia la pantalla de altas en linea o por archivo
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void limpiarRegresar(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEATV limpiarRegresar", EIGlobal.NivelLog.INFO);
		request.getSession().removeAttribute(DESPL);
		request.getSession().removeAttribute(TARJETASRES);
		request.getSession().removeAttribute(TARJETASRESLINEA);
		request.getSession().removeAttribute("valida");
		request.getRequestDispatcher("/jsp/altaTarjeta.jsp").forward(request, response);
	}

	/**
	 * Valida los datos de la tarjeta en linea
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void validarTarjeta(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEATV validarTarjeta", EIGlobal.NivelLog.INFO);
		ArrayList<TarjetaContrato> tarjetasResLin  = null;
		tarjetasResLin = new ArrayList<TarjetaContrato>();
		/*Inicia - EVERIS VALIDACION SQL INJECTION*/
		EIGlobal.mensajePorTrace("Validar SQLINJECTION para el parametro [tarjeta]", EIGlobal.NivelLog.INFO);
		String tarjeta = UtilidadesEnlaceOwasp.cleanQuery(getFormParameterVinc(request,"tarjeta"));
		/*Finaliza - EVERIS VALIDACION SQL INJECTION*/
		String usuario;
		usuario = getFormParameterVinc(request,"usuario" );
		String origen;
		origen = usuario;
		if(usuario.length() < 7) {
            for(int contador = 0 ; contador< (7 - usuario.length());contador++) {
                origen = "0" + origen;
            }
            usuario = origen;
        }
		TarjetaContrato tarjetaObjReq;
		tarjetaObjReq = new TarjetaContrato();
		HttpSession sess;
		sess = request.getSession();
		BaseResource session;
		session = (BaseResource) sess.getAttribute("session");
		String noContrato;
		noContrato = session.getContractNumber();
		if(tarjeta != null){
			tarjetaObjReq.setTarjeta(tarjeta);
			tarjetaObjReq.setUsuarioReg(usuario);
			tarjetaObjReq.setContrato(noContrato);
			tarjetasResLin.add(tarjetaObjReq);
		}


		EnlcTarjetaBO tarjetasBO;
		tarjetasBO = new EnlcTarjetaBOImpl();
		for(TarjetaContrato tarjetaObj: tarjetasResLin){
			TarjetaContrato tarjetaRes = null;
			try {
				tarjetaRes = tarjetasBO.validarDatosTarjetas(request, tarjetaObj.getTarjeta(), tarjetaObj.getContrato());
			} catch (DaoException e) {
				tarjetaRes = new TarjetaContrato();
				tarjetaRes.setCodExito(false);
				tarjetaRes.setMensError("No se pudo validar datos");
			}
			tarjetaObj.setCodExito(tarjetaRes.isCodExito());
			tarjetaObj.setMensError(tarjetaRes.getMensError());
		}
		request.setAttribute (START, Integer.valueOf("0") );
		request.getSession ().setAttribute (DESPL, Integer.valueOf("2") );
		request.getSession ().setAttribute(TARJETASRES, tarjetasResLin);
		request.getSession().removeAttribute(TARJETASRESLINEA);
		evalTemplate("/jsp/detalleErroresArchivo.jsp", request, response );
	}

	/**
	 * Valida los datos de la tarjeta por archivo
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void validarRegistroArchivo(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEATV validarRegistroArchivo", EIGlobal.NivelLog.INFO);
		EnlcTarjetaBO tarjetasBO;
		tarjetasBO = new EnlcTarjetaBOImpl();
		try {
			tarjetasBO.validarArchivo(request);
		} catch (BusinessException e) {
			limpiarRegresar(request, response);
		}
		request.setAttribute (START, Integer.valueOf("0"));
		request.getSession ().setAttribute (DESPL, Global.NUM_REGISTROS_PAGINA );
		evalTemplate("/jsp/detalleErroresArchivo.jsp", request, response );
	}


	/**
	 * Da de alta la lista de tarjetas seleccionadas
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void registrarAltaTarjeta(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEATV registrarAltaTarjeta", EIGlobal.NivelLog.INFO);

		HttpSession sess;
		sess = request.getSession();
		BaseResource session;
		session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace(TEXTOLOG + "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
		String valida = getFormParameter(request,"valida" );
		if (valida == null)
		{
			boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);
			//CSA-Vulnerabilidad  Token Bloqueado-Inicio
            int estatusToken = obtenerEstatusToken(session.getToken());
            
			if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && estatusToken == 1 && solVal)
			{
				EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
				request.getSession().setAttribute("valida", "validado");
				ValidaOTP.guardaParametrosEnSession(request);
				ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP);
			}else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && estatusToken == 2 && solVal){                        
            	cierraSesionTokenBloqueado(session,request,response);
			}
			//CSA-Vulnerabilidad  Token Bloqueado-Fin
			else
			{
				valida = "1";
			}
		}
		if (valida != null && valida.equals("1"))
		{
			ArrayList<TarjetaContrato> tarjetas;
			tarjetas = (ArrayList<TarjetaContrato>)request.getSession().getAttribute(TARJETASRES);
			EnlcTarjetaBO bo;
			bo = new EnlcTarjetaBOImpl();
			try {
				bo.agregarTarjeta(tarjetas, request);
			} catch (DaoException e) {
				EIGlobal.mensajePorTrace(TEXTOLOG + e.getMessage(), EIGlobal.NivelLog.INFO);
			}
			request.getSession().removeAttribute(TARJETASRES);
			request.getSession().setAttribute(TARJETASRES, tarjetas);

			evalTemplate("/jsp/resultadoOperacionAltaTarjeta.jsp", request, response );
		} else {
			evalTemplate("/jsp/detalleFinalTarjeta.jsp", request, response );
		}
	}

	/**
	 * Pagina la pantalla de alta por archivo
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void pagTarjetaArch(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEATV pagTarjetaArch", EIGlobal.NivelLog.INFO);
		String start1;
		start1 = (String) request.getParameter(START);
		if(start1 != null && !"".equals(start1)){
			Integer startInt;
			startInt = Integer.valueOf(request.getParameter (START));
			request.setAttribute (START, startInt);
			evalTemplate("/jsp/detalleErroresArchivo.jsp", request, response );
		}
	}

	/**
	 * Acumula alta de tarjetas en linea
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void enviarTarjeta(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEATV enviarTarjeta", EIGlobal.NivelLog.INFO);
		ArrayList<TarjetaContrato> tarjetas;
		tarjetas = (ArrayList<TarjetaContrato>)request.getSession().getAttribute(TARJETASRES);
		ArrayList<TarjetaContrato> tarjRes;
		tarjRes = new ArrayList<TarjetaContrato>();
		for(TarjetaContrato tarjeta: tarjetas){
			if(tarjeta.isCodExito()){
				tarjRes.add(tarjeta);
			}
		}
		request.setAttribute(START, Integer.valueOf("0") );
		request.getSession().removeAttribute(TARJETASRES);
		request.getSession().setAttribute(TARJETASRES, tarjRes);
		evalTemplate("/jsp/detalleFinalTarjeta.jsp", request, response );
	}

	/**
	 * Pagina la pantalla de alta por archivo final
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void pagTarjetaArchFinal(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEATV pagTarjetaArchFinal", EIGlobal.NivelLog.INFO);
		String start1;
		start1 = (String) request.getParameter(START);
		if(start1 != null && !"".equals(start1)){
			Integer startInt;
			startInt = Integer.valueOf(request.getParameter (START));
			request.setAttribute (START, startInt);
			evalTemplate("/jsp/detalleFinalTarjeta.jsp", request, response );
		}
	}

    /**
     * Obtiene los parametros de request
     * @param request peticion
     * @param parametro a obtener
     * @return String cadena obtenida
     */
    public String getFormParameterVinc(HttpServletRequest request, String parametro)
    {
    	HashMap<String,String> formParameters  = null;
    	String value = "";

    	if(!ServletFileUpload.isMultipartContent(request) || request.getParameter(parametro) != null)
    	{
    		return request.getParameter(parametro);
    	}

    	if( request.getAttribute("formParameters")== null)
    	{
    		List parametros = null;
    		Iterator it;
    		FileItem fileItem;


    		ServletFileUpload sfp;
    		sfp = new ServletFileUpload(new DiskFileItemFactory());
    		try {
    			parametros = sfp.parseRequest(request);

				it = parametros.iterator();
				formParameters = new HashMap<String,String>();
				while(it.hasNext())
				{
					fileItem = (FileItem)it.next();

					if(fileItem.isFormField())
					{
						EIGlobal.mensajePorTrace("campo<" + fileItem.getFieldName() + "> valor <" + fileItem.getString() + ">", EIGlobal.NivelLog.INFO);
						Object obj = null;
						obj = formParameters.get(fileItem.getFieldName());
						if (obj == null) {
							formParameters.put(fileItem.getFieldName(),fileItem.getString());
						}
					}
				}
				request.setAttribute("formParameters",formParameters);
			} catch (FileUploadException e) {
				EIGlobal.mensajePorTrace("E->getFormParameter->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
    	}else
    	{
    		formParameters = (HashMap<String,String>) request.getAttribute("formParameters");
    	}
    	value = formParameters != null ? formParameters.get(parametro) : null;
    	EIGlobal.mensajePorTrace("getFormParameters <" + parametro+ "><" + value +  ">", EIGlobal.NivelLog.INFO);
    	return ( value);
    }
}
