package mx.altec.enlace.servlets;

import java.util.*;
import javax.servlet.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.sql.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.bc_CuentaProgFechas;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.sql.*;



/**
** bcConDevoluciones is a blank servlet to which you add your own code.
 *@author Adrian Mayen
*/
public class bcConFechasProg extends BaseServlet{
	public BaseResource session;
	public ServletContext contexto;
	EIGlobal EnlaceGlobal=new EIGlobal(session,contexto,this); //amt
    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException{
         defaultAction(req, res);
    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException{
         defaultAction(req, res);
    }



    /**
     * <code>displayMessage</code> allows for a short message to be streamed
     * back to the user.  It is used by various NAB-specific wizards.
     * @param req the HttpServletRequest
     * @param res the HttpServletResponse
     * @param messageText the message-text to stream to the user.
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    public void displayMessage(HttpServletRequest req,
                      HttpServletResponse res,
                      String messageText)
                    throws ServletException, IOException{
        res.setContentType("text/html");
        java.io.PrintWriter out = res.getWriter();
        out.println(messageText);
    }


    /**
    ** <code>defaultAction</code> is the default entry-point for iAS-extended
    ** @param req the HttpServletRequest for this servlet invocation.
    ** @param res the HttpServletResponse for this servlet invocation.
    ** @exception javax.servlet.ServletException when a servlet exception occurs.
    ** @exception java.io.IOException when an io exception occurs during output.
    */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException{
		EIGlobal.mensajePorTrace("bcConFechasProg:defaultAction - inicio ", EIGlobal.NivelLog.INFO);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String mensError [] = new String[1];
		mensError[0]="";

		String parAccion = (String)req.getParameter("Accion"); if(parAccion == null) parAccion = "";
		String parTrama = (String)req.getParameter("Trama"); if(parTrama == null) parTrama = "";
		String parCuentas = (String)req.getParameter("Cuentas"); if(parCuentas == null) parCuentas = "";
		String parTramaOld = (String)req.getParameter("tramaOld"); if(parTramaOld == null) parTramaOld = "";

		String [] pant = new String [2];
		StringBuffer varFechaHoy = new StringBuffer ("");
		pant[0] = "0";
		int pantN = 0;
		if(!SesionValida(req,res)) return;
		varFechaHoy = new StringBuffer ("  //Fecha De Hoy");
		varFechaHoy.append ("\n  dia=new Array(");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  mes=new Array(");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  anio=new Array(");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n\n  //Fechas Seleccionadas");
		varFechaHoy.append ("\n  dia1=new Array(");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  mes1=new Array(");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  anio1=new Array(");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  Fecha=new Array('");
		varFechaHoy.append (formateaFecha("FECHA"));
		varFechaHoy.append ("','");
		varFechaHoy.append (formateaFecha("FECHA"));
		varFechaHoy.append ("');");
//		String strInhabiles = armaDiasInhabilesJS();
		String diasInhabiles = diasInhabilesAnt();
        String datesrvr = ObtenDia() + "-" + ObtenMes() + "-" + ObtenAnio();

		// --- Modificacion para integraciòn. de las cuentas
		session.setModuloConsultar(IEnlace.MMant_estruc_B0);

		if(parAccion.equals("")) { if (!session.getFacultad(session.FAC_BASECONEST_BC)) {parCuentas = consultaArbol(req, res, mensError, pant); req.setAttribute("Trama", parTrama);
		//TODO. bitacora entrada del clienete
		 /*
		  * VSWF ARR -I
		  */
		 				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		 				try{
		 				BitaHelper bh = new BitaHelperImpl(req, session, sess);
		 				if (req.getParameter(BitaConstants.FLUJO)!=null){
		 				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
		 				  }else{
		 				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
		 				  }
		 				BitaTransacBean bt = new BitaTransacBean();
		 				bt = (BitaTransacBean)bh.llenarBean(bt);
		 				bt.setNumBit(BitaConstants.ER_ESQ_BASE0_PROG_OP_CONS_ENTRA);
		 				bt.setContrato(session.getContractNumber());

		 				BitaHandler.getInstance().insertBitaTransac(bt);
		 				}catch (SQLException ex){
		 					ex.printStackTrace();
		 				}catch(Exception e){

		 					e.printStackTrace();
		 				}
		 				}
		 	    		/*
		 	    		 * VSWF ARR -F
	    		 */
		}  else mensError[0] = "Usted no tiene facultad para consultar estructuras.";}
		if(parAccion.equals("ELIMINA")) {
			if(session.getFacultad(session.FAC_BASBORPRO_BC)) {
				//TODO. bitacora entrada del clienete
				parCuentas = eliminaCuenta(parTrama, parCuentas, req, mensError);
				req.setAttribute("Trama", parTrama);
			}		else  mensError[0] = "Usted no tiene facultad para eliminar registros.";}

//		req.setAttribute("diasInhabiles", strInhabiles);
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
        req.setAttribute("Encabezado", CreaEncabezado("Consultas de Operaci&oacute;n programada","Tesorer&iacute;a &gt; Esquema Base Cero &gt; Consulta de operaci&oacute;n programada","s33060h", req));
		req.setAttribute("Cuentas",parCuentas);
		req.setAttribute("Mensaje",mensError[0]);
		req.setAttribute("tramaOld",parTramaOld);
//		req.setAttribute("DiasInhabiles",strInhabiles);
		req.setAttribute("Fecha",ObtenFecha());
		req.setAttribute("Movfechas",datesrvr);
		req.setAttribute("FechaAyer",formateaFecha("FECHA"));
		req.setAttribute("FechaPrimero","01/"+formateaFecha("MES2")+"/"+formateaFecha("ANIO"));
		req.setAttribute("VarFechaHoy",varFechaHoy);
		req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		req.setAttribute("FechaDia",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));

		if (diasInhabiles.trim().length()==0) {pant[0] = "3"; mensError[0] = "X-PServicio no disponible por el momento";}
		pantN = Integer.parseInt(pant[0]);
		EIGlobal.mensajePorTrace("bcConFechasProg:defaultAction - pant = " + pantN, EIGlobal.NivelLog.INFO);
		switch(pantN)
			{
			case 0: evalTemplate("/jsp/bcConOperacionesProg.jsp",req,res); break;
			case 2: evalTemplate("/jsp/EI_Error.jsp", req, res); break;
			case 3: despliegaMensaje(mensError[0].substring(3),"Programaci&oacute;n de Fechas","Tesorer�a Santander &gt; Esquema base cero &gt; Programaci&oacute;n de Fechas", req, res); break;
			}
		EIGlobal.mensajePorTrace("bcConFechasProg:defaultAction - fin ", EIGlobal.NivelLog.INFO);
    }

	// Metodo que da formato a la fecha
	String formateaFecha(String tipo){
		StringBuffer strFecha = new StringBuffer ("");
		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);
		do{
			Cal.add(Calendar.DATE,-1);
		}while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);
		if(tipo.equals("FECHA")){
			if(Cal.get(Calendar.DATE) <= 9)
			{strFecha.append ("0");
			strFecha.append (Cal.get(Calendar.DATE));
			strFecha.append ("/");}
			else
			{strFecha.append (Cal.get(Calendar.DATE));
			strFecha.append ("/");}
			if(Cal.get(Calendar.MONTH)+1 <= 9)
			{strFecha.append ("0");
			strFecha.append (Cal.get(Calendar.MONTH)+1);
			strFecha.append ("/");}
			else
			{strFecha.append (Cal.get(Calendar.MONTH)+1);
			strFecha.append ("/");
			strFecha.append (Cal.get(Calendar.YEAR));}
		}
		if(tipo.equals("MES2")){
			if(Cal.get(Calendar.MONTH)+1 <= 9)
			{strFecha.append ("0");
			strFecha.append (Cal.get(Calendar.MONTH)+1);}
         	else
			{strFecha.append(Cal.get(Calendar.MONTH)+1);}
	    }
		if(tipo.equals("DIA"))
		{strFecha.append (Cal.get(Calendar.DATE));}
		if(tipo.equals("MES"))
		{strFecha.append (Cal.get(Calendar.MONTH)+1);}
		if(tipo.equals("ANIO"))
		{strFecha.append (Cal.get(Calendar.YEAR));}
		return strFecha.toString();
    }

	// --- determina dias inhabiles -----------------------------------------------------
	String armaDiasInhabilesJS()
		{
		StringBuffer  resultado = new StringBuffer ("");
		Vector diasInhabiles;

		int indice = 0;

		diasInhabiles = CargarDias(1);
		if(diasInhabiles!=null)
			{
			resultado = new StringBuffer (diasInhabiles.elementAt(indice).toString());
			for(indice = 1; indice<diasInhabiles.size(); indice++)
  				resultado.append (", " + diasInhabiles.elementAt(indice).toString());
			}
		return resultado.toString();
		}

	// --- Carga Dias -------------------------------------------------------------------
	public Vector CargarDias(int formato)
		{
		String sqlDias;
		boolean    estado;
		Vector diasInhabiles = new Vector();
		Connection Conexion;
		PreparedStatement qrDias;
		ResultSet rsDias;

		sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
		try
			{
			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			if(Conexion!=null)
				{
				qrDias = Conexion.prepareStatement( sqlDias );
				if(qrDias!=null)
					{
        			rsDias = qrDias.executeQuery();
					if(rsDias!=null)
						{
						rsDias.next();
						do
							{
							String fecha;
							String dia  = rsDias.getString(1);
							String mes  = rsDias.getString(2);
							String anio = rsDias.getString(3);
							if(formato == 0)
								{
								dia=Integer.toString( Integer.parseInt(dia) );
								mes=Integer.toString( Integer.parseInt(mes) );
								anio=Integer.toString( Integer.parseInt(anio) );
								}
							fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
							diasInhabiles.addElement(fecha);
							}
						while(rsDias.next());
						rsDias.close();
						}
					else
			  			EIGlobal.mensajePorTrace("puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					}
				else
       	  			EIGlobal.mensajePorTrace("puedo crear Query.", EIGlobal.NivelLog.ERROR);
				}
			else
       			EIGlobal.mensajePorTrace("No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
			Conexion.close();
			}
		catch(SQLException sqle)
			{
      		EIGlobal.mensajePorTrace("efectuo la sig. excepcion: ", EIGlobal.NivelLog.ERROR);
      		EIGlobal.mensajePorTrace( sqle.getMessage() , EIGlobal.NivelLog.ERROR);
			}
		return(diasInhabiles);
		}
	// --- Regresa una trama con el arbol ----------------------------------------------
	private String consultaArbol(HttpServletRequest req, HttpServletResponse res, String [] mensError, String [] pant){
		String retorno, trama = "", aviso, msgError, cuentaHija = "", desc, token;
		String token1;
		StringBuffer tramaFin = new StringBuffer ("");
		StringBuffer trama1 = new StringBuffer ("");
		String comboCuentas1 = comboCuentas1(req, mensError);
		int car;

		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Entrando a consultaArbol, comboCuentas1=\n" + comboCuentas1, EIGlobal.NivelLog.INFO);

		/**/try{
			StringTokenizer tokens = new StringTokenizer(comboCuentas1,"@");
			token = tokens.nextToken();
			while(tokens.hasMoreTokens()){
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... consultaArbol, token:\n" + token, EIGlobal.NivelLog.INFO);
				car = token.indexOf("|");
				cuentaHija = token.substring(0,car);
				desc = token.substring(car+1);
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... consultaArbol, desc:\n" + desc, EIGlobal.NivelLog.INFO);
				retorno = enviaTuxedo("CONSULTA", cuentaHija,req, "", "", mensError); // se realiza una copia del archivo procesado por Tuxedo
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... consultaArbol, retorno:\n" + retorno, EIGlobal.NivelLog.INFO);
				trama = leeArchivo(retorno);// + "\n";
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... consultaArbol, trama 1:\n**" + trama + "**", EIGlobal.NivelLog.INFO);
				aviso = trama.substring(0,trama.indexOf("@"));
				trama = trama.substring(trama.indexOf("@") + 1);
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... consultaArbol, trama 2:\n**" + trama + "**", EIGlobal.NivelLog.INFO);
				if(!aviso.equals("TRAMA_OK")){
					//mensError = ""; // amt // Para que despliegue el mensaje de error cuando no existen Contratos
					//mensError = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>Intente m&aacute;s tarde.<br>Gracias.";
					//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Error en consultaArbol() -- tramas devueltas:\n" + aviso, EIGlobal.NivelLog.INFO);
					//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Error en consultaArbol() -- ultimo valor de la trama= " + trama, EIGlobal.NivelLog.INFO);
					//req.setAttribute("Error","La p&acute;gina que solicito no esta disponible en este momento, por favor intente m&acute;s tarde.");
					//evalTemplate("/jsp/EI_Error.jsp", req, res);
					req.setAttribute("Error","No existen programaciones para este contrato.");
					pant[0] = "2";
					return "";
				} else {
					// Agrega la descripcion a las cuentas
					StringTokenizer tokens1 = new StringTokenizer(trama,"@");
					while (tokens1.hasMoreTokens()){
						token1  = tokens1.nextToken(); // cuenta|descripcion
						trama1.append (token1);
						trama1.append (desc);
						trama1.append ("@");
						EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... consultaArbol, trama1=\n " + trama1, EIGlobal.NivelLog.INFO);
					}
					tramaFin.append (trama1);// + "\n";
				}
				token = tokens.nextToken();
		    }
		}catch (Exception e){
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Error en consultaArbol(): " + e + "\n", EIGlobal.NivelLog.INFO);
            //req.setAttribute("Error","La p&acute;gina que solicito no esta disponible en este momento, por favor intente m&aacute;s tarde.");
			req.setAttribute("Error","No existen programaciones para este contrato o fall&oacute; la consulta");
			pant[0] = "2";
            //evalTemplate("/jsp/EI_Error.jsp", req, res);
		}finally{
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... consultaArbol, tramaFin=\n**" + tramaFin + "**", EIGlobal.NivelLog.INFO);
			return tramaFin.toString();
		}
	}
	// --- Elimina una cuenta de la trama de cuentas -----------------------------------
	private String eliminaCuenta(String cuentaFecha, String tramaCuentas, HttpServletRequest req, String [] mensError){
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta, \ncuentaFecha= " + cuentaFecha + "\ntramaCuentas=\n" + tramaCuentas, EIGlobal.NivelLog.INFO); //Salida
		Vector cuentas;
		bc_CuentaProgFechas cta;
		boolean consistente;
		String fechaIni, fechaFin, fechaIni1, fechaFin1;
		String cuenta, retorno, creaTrama="";
		String tramaArchivo;
		int a, index;

		cuenta = cuentaFecha.substring(0, cuentaFecha.indexOf("|"));
		cuentas = creaCuentas(tramaCuentas, req, mensError);
		fechaIni = cuentaFecha.substring(cuentaFecha.indexOf("|")+6,cuentaFecha.indexOf("|")+16);
		fechaFin = cuentaFecha.substring(cuentaFecha.indexOf("|")+21);
		fechaIni1 = quitaFormatoFecha(fechaIni);
		fechaFin1 = quitaFormatoFecha(fechaFin);
		//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta, fechaIni1=\n*" + fechaIni1 + "*", EIGlobal.NivelLog.INFO);
		//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta, fechaFin1=\n*" + fechaFin1 + "*", EIGlobal.NivelLog.INFO);
		for (a=0;a<cuentas.size();a++){
			cta = (bc_CuentaProgFechas)cuentas.get(a);
			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta, cta.getVigencia1()=\n*" + cta.getVigencia1() + "*", EIGlobal.NivelLog.INFO);
			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta, cta.getVigencia2()=\n*" + cta.getVigencia2() + "*", EIGlobal.NivelLog.INFO);
			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta, \ncta.getNumCta()=\n" + cta.getNumCta() + ", a= " + a, EIGlobal.NivelLog.INFO);
			if(cta.getNumCta().equals(cuenta) && cta.getVigencia1().equals(fechaIni) && cta.getVigencia2().equals(fechaFin)){
				retorno = enviaTuxedo("BORRAR", cuenta, req, fechaIni1, fechaFin1, mensError);
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta, \nretorno.substring(0,2)=*" + retorno.substring(0,2) + "*", EIGlobal.NivelLog.INFO);
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta\nretorno=" + retorno, EIGlobal.NivelLog.INFO);
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> ..........  eliminaCuenta\retorno.substring(1,4)=" + retorno.substring(1,4), EIGlobal.NivelLog.INFO);
				if (retorno.substring(1,4) != "tmp"){
					if (retorno.substring(16,24).equals("BASE0000")){
						//cuentas.remove(cta);
						cuentas.remove(a);
						creaTrama = creaTrama(cuentas);
						mensError[0] = "La cuenta ha sido eliminada.";
					}else{
						creaTrama = creaTrama(cuentas);
						mensError[0] = "La cuenta no pudo ser eliminada.";
					}
				}else{
					creaTrama = creaTrama(cuentas);
					mensError[0] = "Favor de repetir la operaci&oacute;n";
				}
			}else {
					creaTrama = creaTrama(cuentas);
			}
		}
		return creaTrama;
	}
	//Despliegue de mensaje
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal Global=new EIGlobal(session,contexto,this); //amt

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		//<vswf:meg cambio de NASApp por Enlace 08122008>
		request.setAttribute( "URL", "/Enlace/enlaceMig/bcProgFechas" );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "", request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() ); 
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}
	// informacion de las cuentas hijas existentes
	public String comboCuentas1(HttpServletRequest req, String [] mensError){
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		Hashtable ht;
		StringBuffer trama = new StringBuffer ("");
		Vector cuentasMto = new Vector();
		StringBuffer valorFinal = new StringBuffer ("");
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		Hashtable infoCuentas = new Hashtable();
		String infoArchivo = "infoCtas.mto";
		String infoLinea = "";
		String infoClave, infoContenido;
		BufferedReader infoEntrada = null;
		int n=0, i;
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1", EIGlobal.NivelLog.INFO);

		try
		{
			// Hacer consulta a tuxedo
			trama = new StringBuffer ("2EWEB|");
			trama.append (session.getUserID8()); 
			trama.append ("|B0AC|");
			trama.append (session.getContractNumber());
			trama.append ("|");
			trama.append (session.getUserID8()); 
			trama.append ("|");
			trama.append (session.getUserProfile()); // <--- aquì va la trama de consulta, de la estructura de Marco (Cuentas padre)
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			ht = tuxedo.web_red(trama.toString());
			trama = new StringBuffer ((String)ht.get("BUFFER"));
			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1, trama regresada por Tuxedo= " + trama, EIGlobal.NivelLog.INFO);

			// realizar copia remota
			trama = new StringBuffer (trama.substring(trama.toString().lastIndexOf('/')+1,trama.length()));
			if(archRemoto.copia(trama.toString()))
				{
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1, copia remota realizada (Tuxedo a local), trama= " + trama, EIGlobal.NivelLog.INFO);
				trama = new StringBuffer (Global.DIRECTORIO_LOCAL + "/" + trama);
				}
			else
				{
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
				mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
				return "";
				//return null;
				}

			// Leer datos
			BufferedReader entrada = new BufferedReader(new FileReader(trama.toString()));
			String linea = entrada.readLine();

			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1, linea 1: " + linea, EIGlobal.NivelLog.INFO);

			if(linea != null) linea = entrada.readLine(); else return "";
			if(linea == null) return "";

			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1, linea 2: " + linea, EIGlobal.NivelLog.INFO);
			n = 2;
			while((linea = entrada.readLine()) != null)
				{
				n++;
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1, linea " + n + ": " + linea, EIGlobal.NivelLog.INFO);
				cuentasMto.add(linea.substring(0,linea.indexOf("|"))); //+descripcion);
				}

			//  Realizar consulta para obtener archivo de cuentas
			//  Obtener descripciones de las cuentas
			if(llamado_servicioCtasInteg("7@", " ", " ", " ",infoArchivo, req))
				{
				//- Hacer un parser del archivo y guardar la info en un Hastable: clave: cuenta, valor: descripción
				infoEntrada = new BufferedReader(new FileReader(Global.DOWNLOAD_PATH + infoArchivo));
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1: infoEntrada "+infoEntrada, EIGlobal.NivelLog.INFO);
				while((infoLinea = infoEntrada.readLine())!= null)
					{
					//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1: infoLinea "+infoLinea, EIGlobal.NivelLog.INFO);
					infoClave = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoContenido = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoCuentas.put(infoClave,infoContenido);
					}
				}

			for (i=0; i<cuentasMto.size(); i++){
				infoContenido = (String)infoCuentas.get((String)cuentasMto.get(i));
				if(infoContenido == null) infoContenido = "";
				valorFinal.append ((String)cuentasMto.get(i) + "|" + infoContenido + "@\n");
			}
			infoEntrada.close();
			entrada.close();
		}
		catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG bcProgFechas.java> Error: " + e, EIGlobal.NivelLog.INFO);
			}
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... comboCuentas1, valorFinal:\n" + valorFinal, EIGlobal.NivelLog.INFO);
		return valorFinal.toString();
	} // Fin comboCuentas1

	// --- Ejecuta una consulta a Tuxedo y regresa el aviso de Tuxedo ------------------
	private String enviaTuxedo(String tipo, String cuentaHija, HttpServletRequest req, String fechaIni, String fechaFin, String [] mensError)		{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String tipoOper="";
		String Cuenta="";
		String fecha="";
		String Arch="";
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Llego a enviaTuxedo", EIGlobal.NivelLog.INFO);
		StringBuffer trama = new StringBuffer ("");
		Hashtable ht;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		boolean criterio = false;

		// --- Sección de archivo remoto -----
		if(tipo.equals("ALTA") || tipo.equals("MODIFICACION")){
			if(archRemoto.copiaLocalARemoto(session.getUserID8()+".tmp"))// <-- Aqui realiza la copia para tuxedo
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Copia remota realizada (local a Tuxedo)", EIGlobal.NivelLog.INFO);
			else
				{
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Copia remota NO realizada (local a Tuxedo)", EIGlobal.NivelLog.INFO);
				mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
				}
		}

		if(tipo.equals("ALTA"))
			{trama = new StringBuffer ("2EWEB|");
			trama.append (session.getUserID8()); 
			trama.append ("|B0PA|");
			trama.append (session.getContractNumber());
			trama.append ("|");
			trama.append (session.getUserID8());
			trama.append ("|");
			trama.append (session.getUserProfile());
			trama.append ("|");
			trama.append ("1");
			trama.append ("| |");
			tipoOper="B0PA";
			} // No debe llevar el nombre del archivo en la trama
		else if(tipo.equals("CONSULTA"))
			{trama = new StringBuffer ("2EWEB|");
			trama.append (session.getUserID8());
			trama.append ("|B0PC|");
			trama.append (session.getContractNumber());
			trama.append ("|");
			trama.append (session.getUserID8()); 
			trama.append ("|");
			trama.append (session.getUserProfile());
			trama.append ("|N@");
			trama.append (cuentaHija);
			trama.append ("| |");
			tipoOper="B0PC";
			Cuenta=cuentaHija;
			}
		else if(tipo.equals("BORRAR"))
			{trama = new StringBuffer ("2EWEB|");
			trama.append (session.getUserID8()); 
			trama.append ("|B0PB|");
			trama.append (session.getContractNumber());
			trama.append ("|");
			trama.append (session.getUserID8());
			trama.append ("|");
			trama.append (session.getUserProfile());
			trama.append ("|");
			trama.append (cuentaHija);
			trama.append ("@N@");
			trama.append (fechaIni);
			trama.append ("@");
			trama.append (fechaFin);
			trama.append ("@| |");
			tipoOper="B0PB";
			Cuenta=cuentaHija;
			fecha=fechaFin;
			}
		else
			{trama = new StringBuffer (session.getContractNumber());
			trama.append ("|");
			trama.append (session.getUserID8());
			trama.append ("|");
			trama.append (session.getUserProfile());
			trama.append ("|");
			trama.append (IEnlace.DOWNLOAD_PATH+session.getUserID8());
			trama.append (".tmp@");
			trama.append ("|6@|");
			trama.append (tipo);
			trama.append ("| | |");
			Arch=session.getUserID8()+".doc"; 
			criterio = true;}
		try
			{
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... enviaTuxedo, trama :"+trama, EIGlobal.NivelLog.INFO);
			// --- Crea el ambiente para Tuxedo
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			if(!criterio) {ht = tuxedo.web_red(trama.toString());} else {ht = tuxedo.cuentas_modulo(trama.toString());}
			// --- Guarda el resultado de Tuxedo en trama
			trama = new StringBuffer ((String)ht.get("BUFFER"));
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... enviaTuxedo, trama 1 : " + trama, EIGlobal.NivelLog.INFO);
			if(criterio && !((String)ht.get("COD_ERROR")).equals("BASE0000"))
				{
				criterio = false;
				mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
				EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Error en consulta a Tuxedo", EIGlobal.NivelLog.INFO);
				}
			if(criterio) trama = new StringBuffer (session.getUserID8() + ".ctasA"); 
			// --- Sección de archivo remoto -----
			if(tipo.equals("CONSULTA") || criterio)
				{
				trama = new StringBuffer (trama.substring(trama.toString().lastIndexOf('/')+1,trama.length()));
				if(archRemoto.copia(trama.toString()))
					{
					//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
					trama = new StringBuffer (Global.DIRECTORIO_LOCAL + "/" +trama);
					EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Copia remota realizada (Tuxedo a local), trama= " + trama, EIGlobal.NivelLog.INFO);
					}
				else
					{
					EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
					mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
					}
				}
//			todo Bitacorizacion
			 /*
	         * VSWF ARR -I
	         */
	        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
	        try{

				BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit("ERBP02");
				bt.setContrato(session.getContractNumber());
				String CodeError = (String)ht.get("COD_ERROR");
				if(CodeError!=null){
				 if(CodeError.substring(0,2).equals("OK")){
					   bt.setIdErr(tipoOper + "0000");
				 }else if(CodeError.length()>8){
					  bt.setIdErr(CodeError);
				 }else{
					  bt.setIdErr(CodeError);
				 }
				}
				if(Cuenta!=null && !Cuenta.equals(""))
					bt.setCctaOrig(Cuenta);
				if(Arch!=null && !Arch.equals(""))
					bt.setNombreArchivo(Arch);
				bt.setServTransTux(tipoOper);
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
	        }
			/*
			 * VSWF ARR -F
			 */
			}
		catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Error intentando conectar a Tuxedo: " + e.toString() + " <---> " + e.getMessage() +  " <---> " + e.getLocalizedMessage(), EIGlobal.NivelLog.INFO);
			mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			}
		finally
			{
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Saliendo de enviaTuxedo()", EIGlobal.NivelLog.INFO);
			return trama.toString();
			}
		}

	// --- Lee el archivo de tuxedo y regresa una trama de cuentas ---------------------
	private String leeArchivo(String nombreArchivo)
		{
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Llego a leeArchivo", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... nombreArchivo de la trama: " + nombreArchivo, EIGlobal.NivelLog.INFO);
		StringBuffer tramaT = new StringBuffer(""), tramaR = new StringBuffer("");
		StringBuffer nomArchivo1 = new StringBuffer("");
		StringBuffer nomArchivo2 = new StringBuffer("");
		StringTokenizer tokens;
		FileReader archivo;
		String strAux, linea;//, tipoArbol;
		Vector cuentas;
		bc_CuentaProgFechas cuenta;
		int car, a, b;

		BufferedReader nomArchivo;

		try{
			nomArchivo = new BufferedReader(new FileReader (nombreArchivo));
			linea = nomArchivo.readLine();
			nomArchivo1.append(linea);
			while ((linea = nomArchivo.readLine()) != null){
				nomArchivo1.append("@" + linea);
			}
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... leeArchivo, nomArchivo1=\n**" + nomArchivo1+"**", EIGlobal.NivelLog.INFO);
			if(!nomArchivo1.toString().substring(16,24).equals("BASE0000"))
				{nomArchivo1.delete(0,24); nomArchivo1.insert(0,"<<ERROR>>@trama de tuxedo: ");}
			else {
				nomArchivo1.delete(0,nomArchivo1.toString().indexOf("@") + 1);
				nomArchivo1.insert(0,"TRAMA_OK@");
				}
			nomArchivo.close();
		}catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Error en leeArchivo(): " + e.toString(), EIGlobal.NivelLog.INFO);
			nomArchivo1 = new StringBuffer("<<ERROR>>@trama en el archivo: " + nomArchivo1.toString());
			}
		finally
			{
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Saliendo de leeArchivo", EIGlobal.NivelLog.INFO);
			return nomArchivo1.toString();
			}
		}

	// --- Regresa un vector de cuentas a partir de una trama --------------------------
	public Vector creaCuentas(String tramaCuentas, HttpServletRequest req, String [] mensError)
		{
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Llego a creaCuentas, tramaCuentas=*" + tramaCuentas + "*", EIGlobal.NivelLog.INFO);
		Vector cuentas = null;
		StringTokenizer token;
		bc_CuentaProgFechas ctaAux1, ctaAux2;
		int a, b;
		String valorToken;

		//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... creaCuentas1", EIGlobal.NivelLog.INFO);
		try
			{
			cuentas = new Vector();
			token = new StringTokenizer(tramaCuentas,"@");
			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... creaCuentas2", EIGlobal.NivelLog.INFO);
			while(token.hasMoreTokens())
				{
				valorToken = token.nextToken().trim();
				cuentas.add(bc_CuentaProgFechas.creaCuentaFechas("@" + valorToken));
				//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... creaCuentas3, valorToken= \n*" + valorToken + "*", EIGlobal.NivelLog.INFO);
				}
			}
		catch(Exception e)
			{
			//EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Error en creaCuentas(): " + e.toString(), EIGlobal.NivelLog.INFO);
			cuentas = new Vector();
			mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			}
		finally{
			EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... Saliendo de creaCuentas", EIGlobal.NivelLog.INFO);
			return cuentas;}
		}

	//Elimina el formato de fecha dd/mm/aaaa a ddmmaaaa
	 private String quitaFormatoFecha(String fecha){
		String dia, mes, ano, fechaFinal;
		int car;
		car = fecha.indexOf("/");
		dia = fecha.substring(0, car);
		fecha = fecha.substring(car+1);
		car = fecha.indexOf("/");
		mes = fecha.substring(0, car);
		fecha = fecha.substring(car+1);
		ano = fecha;
		fechaFinal = dia + mes + ano;
		return fechaFinal;
	 }

	// --- Regresa una trama a partir de un vector de cuentas --------------------------
	public String creaTrama(Vector vectorCuentas)
		{
		StringBuffer regreso = new StringBuffer("");
		for(int a=0;a<vectorCuentas.size();a++)
			{regreso.append(((bc_CuentaProgFechas)vectorCuentas.get(a)).trama());}
		EIGlobal.mensajePorTrace("<DEBUG bcConFechasProg> .......... creaTrama, regreso.toString()=\n" + regreso.toString(), EIGlobal.NivelLog.INFO);
		return regreso.toString();
		}

}