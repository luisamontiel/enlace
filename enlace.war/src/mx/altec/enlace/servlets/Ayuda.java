package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/*Inicia - EVERIS VALIDACION CSS */
import mx.altec.enlace.utilerias.UtilidadesEnlaceOwasp;
/*Finaliza - EVERIS VALIDACION CSS */

/** 
*   Isban M�xico
*   Clase: Ayuda.java
*   Descripci�n: Servlet utilizado en la funcionalidad de ayuda.
*
*   Control de Cambios:
*   1.0 16/10/2017 Everis - Limpieza Cross Site Scripting
*/
public class Ayuda extends BaseServlet
{
    public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
         defaultAction(req, res);
    }

	public void defaultAction(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
      /*Inicia - EVERIS VALIDACION CSS */
		EIGlobal.mensajePorTrace("Validar CSS para el parametro [ClaveAyuda]", EIGlobal.NivelLog.INFO);
      String ClaveAyuda = UtilidadesEnlaceOwasp.cleanString(req.getParameter("ClaveAyuda"));

		EIGlobal.mensajePorTrace("Entrando a ayuda", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("ClaveAyuda = >" + ClaveAyuda + "<", EIGlobal.NivelLog.DEBUG);

		req.setAttribute("ClaveAyuda", ClaveAyuda);
		/**Finaliza - EVERIS VALIDACION CSS */
        evalTemplate("/jsp/Ayuda.jsp", req, res);
        return;
    }    
}
