package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.sql.*;

public class MTI_Comprobante extends BaseServlet
  {
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	    int indice=0;
		EI_Tipo Env = new EI_Tipo(this);

	    EIGlobal.mensajePorTrace("MTI_Comprobante - execute(): Entrando a Comprobante de Dep. Inter.", EIGlobal.NivelLog.DEBUG);
		Env.iniciaObjeto ((String) req.getParameter("TransEnv"));
		indice=Integer.parseInt((String) req.getParameter("Indice"));
		EIGlobal.mensajePorTrace("MTI_Comprobante - execute(): Elemento no. " + indice, EIGlobal.NivelLog.DEBUG);
		iniciaComprobante(Env,indice, req, res );
		EIGlobal.mensajePorTrace("MTI_Comprobante - execute(): Saliendo de Comprobante de Dep. Inter.", EIGlobal.NivelLog.DEBUG);
	}

/******************************************* Modulo Comprobante     */
	public void iniciaComprobante(EI_Tipo Env,	int indice, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	 String strTabla="";
	 String ncontrato_ = "";
     String contrato_   = "";
	 String nusuario_ = "";
     String usuario_   = "";
	 String cvebanco_ = "";

	 String[] titulo={
	                      "Ref. de Cargo",
						  "No. de Orden",
		                  "Ref. de Rastreo",
						  "Concepto",
		                  "Ordenante",
		                  "Beneficiario",
		                  "RFC Beneficiario",
		                  "Banco Receptor",
	                      "Pais",
						  "Ciudad",
		                  "Divisa",
		                  "Tipo de cambio",
		                  "Importe MN",
		                  "Importe Dlls",
		                  "Importe Divisa",
		                  "Importe IVA",
		                  "Estatus",
		                  "Ref. de Confirmaci�n (FED)",
						  ""};

	 String totales="";
	 String plaza="";
	 String strMen="";
	 String refMec=req.getParameter("RefMec");

	 int totalC=17;

	 /************* Modificacion para la sesion ***************/
	 HttpSession sess = req.getSession();
	 BaseResource session = (BaseResource) sess.getAttribute("session");
	 EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	 EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): Recibiendo parametros.", EIGlobal.NivelLog.DEBUG);

	 if(req.getParameter("NumContrato")!= null)
		contrato_ = (String)req.getParameter("NumContrato");
	 if(req.getParameter("NomContrato")!= null)
	    ncontrato_ = (String) req.getParameter("NomContrato");
	 if(req.getParameter("NumUsuario")!= null)
		nusuario_ = (String)req.getParameter("NumUsuario");
	 if(req.getParameter("NomUsuario")!= null)
		usuario_ = (String) req.getParameter("NomUsuario");
	 if(req.getParameter("ClaveBanco")!= null)
		cvebanco_ = (String) req.getParameter("ClaveBanco");

	 if(refMec!=null)
	   {
		 if(refMec.trim().equals("CON"))
		   totalC=18;
	   }

	 if(Env.camposTabla[0][12].indexOf("$")!=-1)
	   EnlaceGlobal.formateaImporte(Env,12);

	 EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): Parametros leidos.", EIGlobal.NivelLog.DEBUG);

	 EnlaceGlobal.formateaImporte(Env,13);
	 Env.camposTabla[0][13]=Env.camposTabla[0][13].replace('$',' ').trim();
	 EnlaceGlobal.formateaImporte(Env,14);
	 Env.camposTabla[0][14]=Env.camposTabla[0][14].replace('$',' ').trim();

	 EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): Importes formateados.", EIGlobal.NivelLog.DEBUG);
String imp = "";

ValidaCuentaContrato valCtas = null;
try{
	valCtas = new ValidaCuentaContrato();
     for(int i=0;i<totalC;i++)
      {
		imp = Env.camposTabla[indice][i];

		if(i==12)
		 {
            //modif PVA 06/08/2002
			EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): 3.", EIGlobal.NivelLog.DEBUG);
	        if (
				 (Env.camposTabla[indice][10].trim().equals("USD")==true)
				   &&
				 (esCuentaDolares(Env.camposTabla[indice][4],nusuario_, valCtas, session)==true)
			   )
			 {
			   EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): Transferencia a la par.", EIGlobal.NivelLog.DEBUG);
             }
			else
			{

			  EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): Desplegando dstos.", EIGlobal.NivelLog.DEBUG);
              strTabla+="\n <tr><td class='tittabcom' align='right'>";
			  strTabla+="<b>"+titulo[i]+": </b></td>";
			  strTabla+="\n<td class='textabcom' nowrap>";
			  Env.camposTabla[indice][i]=formatea_importe(Env.camposTabla[indice][i]);
			  strTabla+=Env.camposTabla[indice][i]+"</td>";
			  strTabla+="</tr>";
			}
		 }
		else
		 {
		   strTabla+="\n <tr><td class='tittabcom' align='right'>";
		   strTabla+="<b>"+titulo[i]+": </b></td>";
		   strTabla+="\n<td class='textabcom' nowrap>";

		   if(i==13 && i==14)
			 {
			   Env.camposTabla[indice][i]=formatea_importe(Env.camposTabla[indice][i]);
			   Env.camposTabla[indice][i]=Env.camposTabla[indice][i].replace('$',' ').trim();
			 }
			if(i==16)
			 {
			   strMen="";
			   if(Env.camposTabla[indice][i].indexOf("ENVIADA")>=0)
				 strMen="Esta operaci�n a�n no ha sido confirmada por el banco del beneficiario";
			   strTabla+=Env.camposTabla[indice][i]+"</td>";
			 }
			else
			 strTabla+=Env.camposTabla[indice][i]+"</td>";

			strTabla+="</tr>";
		 }
		//TODO BIT CU3121, CU3131
	 	/**
		 * VSWF
		 * 17/Enero/2007
		 */
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try {
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();

			bt = (BitaTransacBean)bh.llenarBean(bt);
			if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
				bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_GENERA_COMPROBANTE);
			}else{
				if(req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
					bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_GENERA_COMPROBANTE);
				}
			}
			bt.setContrato((contrato_ == null)?" ":contrato_);
			bt.setBancoDest((cvebanco_ == null)?" ":cvebanco_);
			if(imp != null){
				imp = imp.replace('$',' ').trim();
				try {
					bt.setImporte(Double.parseDouble(imp));
				} catch (NumberFormatException e) {
					//e.printStackTrace();
					EIGlobal.mensajePorTrace("Error al parsear importe para bitacorizar: " + imp, EIGlobal.NivelLog.ERROR);
				}
			}

			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(NumberFormatException e){
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		/**
		 * VSWF
		 */


      }
}catch(Exception e){
	EIGlobal.mensajePorTrace("Error en metodo iniciaComprobante de clase MTI_Comprobante->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
	}  //fin de try-catch
finally
{
	try{
		valCtas.closeTransaction();
	}catch (Exception e) {
		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
	}
}
	 req.setAttribute("NomContrato",ncontrato_);
	 req.setAttribute("NumContrato",contrato_);
	 req.setAttribute("NumUsuario",nusuario_);
	 req.setAttribute("NomUsuario",usuario_);
	 req.setAttribute("ClaveBanco",cvebanco_);

	 req.setAttribute("Tabla",strTabla);
     req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dd/mm/aaaa th:tm"));
	 req.setAttribute("Mensaje",strMen);


	 evalTemplate("/jsp/MTI_Comprobante.jsp", req, res);
	}


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
	public boolean esCuentaDolares(String cuenta,String usuario, ValidaCuentaContrato valCtas, BaseResource session)
	  {
		String datoscta[]=null;

		EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): metodo de busqueda 1.", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): metodo de busqueda cuenta: "+cuenta+"]" , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): metodo de busqueda usuario: "+usuario, EIGlobal.NivelLog.INFO);

		cuenta=cuenta.substring(0,cuenta.indexOf(" "));
		EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): metodo de busqueda cuenta: "+cuenta+"]" , EIGlobal.NivelLog.INFO);

		//datoscta=BuscandoCtaInteg(cuenta,IEnlace.LOCAL_TMP_DIR + "/"+usuario.trim()+".ambci",IEnlace.MCargo_TI_pes_dolar);
		datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
												  session.getUserID8(),
										          session.getUserProfile(),
										          cuenta.trim(),
										          IEnlace.MCargo_TI_pes_dolar);
		if(datoscta!=null)
		 {
		   EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): metodo de busqueda. 2", EIGlobal.NivelLog.DEBUG);
		   EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): divisa: "+datoscta[3], EIGlobal.NivelLog.INFO);
		   if(!datoscta[3].trim().equals("6"))
			return true;
		 }
		else
		 EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): cuenta fue nula.", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("MTI_Comprobante - iniciaComprobante(): metodo de busqueda. false", EIGlobal.NivelLog.ERROR);
		return false;
	  }
}