/**Banco Santander Mexicano
* Clase ConsultaUsuarios, realiza una consulta de los usuarios, asi como la gest�on del m�dulo de "Pago de SAR"
* @version 1.0
* fecha de creacion:
* responsable: Juan Pablo Pe�a Morales
* descripcion: Se modific� la clase de ConsultaUsuarios, agregandol� la funcionalidad de gesti�n de "Pago de SAR".
	Inicialmente muestra la pantalla de validaci�n de la l�nea de captura que contiene los datos del pago, si la
	validaci�n es exitosa se presenta una pantalla con los datos de la dependencia a quien le corresponde el pago,
	posteriormrnte se podr� realizar el pago, para finalizar se presentar� el comprobante correspondiente.
	Se agregar�n los siguientes m�todos:
		.	iniciaPagoSar
		.	consultaDependencia
		.	realizaTransaccion
		.	enviaDatosComprobante
		�	AbreFrame:
		�	FormateaCampo:
		�	|LineaCaptura:
		�	DespliegaPaginaErrorURL:
*/

package mx.altec.enlace.servlets;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.Convertidor;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;

public class ConsultaUsuarios extends BaseServlet{

    public void doGet( HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException{
        defaultAction( req, res );
    }

    /**
	 * Metodo principal cuando la peticion es de tipo POST
	 * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws IOException Excepciones del metodo
	 * @throws ServletException Excepciones del metodo
	 */
    public void doPost( HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException{
        defaultAction( req, res );
    }

    /**
     * Metodo principal
     * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws IOException Excepciones del metodo
	 * @throws ServletException Excepciones del metodo
     */
    public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException{

		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("**************+** ENTRA AL METODO defaultAction *************", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
        boolean sesionvalida = SesionValida( req, res );
		if ( sesionvalida )
		{
            String valida = req.getParameter ( "valida" );
            if(validaPeticion( req,  res,session,sess,valida)){
                EIGlobal.mensajePorTrace("\n\nENTRA A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.INFO);
            }
            else
			{
				if (req.getParameter("Modulo")!=null)//condici�n para Pago de SAR
				{
					session.setModuloConsultar (IEnlace.MSua_envio);
					String modulo="";
					modulo=(String) req.getParameter("Modulo");

					EIGlobal.mensajePorTrace("ConsultaUsuarios - El valor de Modulo es:" + modulo, EIGlobal.NivelLog.DEBUG);
					if (modulo.equals("0")){
						iniciaPagoSar(req,res); // ok
					}
					else if	(modulo.equals("1"))
						consultaDependencia(req,res); // ok
					else if (modulo.equals("2"))
					{

						//interrumpe la transaccion para invocar la validaci�n
						///////////////////--------------challenge--------------------------
						String validaChallenge = req.getAttribute("challngeExito") != null
							? req.getAttribute("challngeExito").toString() : "";

						EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

						if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
							EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
							ValidaOTP.guardaParametrosEnSession(req);

							validacionesRSA(req, res);
							return;
						}

						//Fin validacion rsa para challenge
						///////////////////-------------------------------------------------
						boolean valBitacora = true;
						if(  valida==null)
						{
							EIGlobal.mensajePorTrace("\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP\n", EIGlobal.NivelLog.INFO);

							boolean solVal=ValidaOTP.solicitaValidacion (
										session.getContractNumber(),IEnlace.SAR);

							if( session.getValidarToken() &&
								    session.getFacultad(session.FAC_VAL_OTP) &&
								    session.getToken().getStatus() == 1 &&
								    solVal )
							{
								req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
								EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);
								ValidaOTP.guardaParametrosEnSession(req);

								EIGlobal.mensajePorTrace("TOKEN PAGO SAR", EIGlobal.NivelLog.INFO);
								req.getSession().removeAttribute("mensajeSession");
								ValidaOTP.mensajeOTP(req, "PagoSAR");

								ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
							}
							else{
								ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
								valida="1";
								valBitacora = false;
								}
						}
						//retoma el flujo
						if( valida!=null && valida.equals("1")) {

							if (valBitacora)
							{
								try{
									EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion = BitaConstants.CTK_PAGO_SAR;
									String concepto = BitaConstants.CTK_CONCEPTO_PAGO_SAR;
									double importeDouble = 0;
									String cuenta = "0";
									String token = "0";
				        			if (req.getParameter("token") != null && !req.getParameter("token").equals(""));
									{token = req.getParameter("token");}

									if (req.getParameter("txtTotal") != null && !req.getParameter("txtTotal").equals(""))
									{
										String importeStr = req.getParameter("txtTotal");
										importeStr = importeStr.substring(importeStr.indexOf("$")+1).trim();
										importeStr = importeStr.replace(",", "");
										importeDouble = Double.valueOf(importeStr).doubleValue();
									}

									String[] datosCuenta = null;
									datosCuenta = desentrama(req.getParameter("cuenta"),'|');
									if (datosCuenta[0] != null && !datosCuenta[0].equals(""))
									{cuenta = datosCuenta[0];}


									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
								} catch(Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

								}
							}
                            if(validaCuentas(req))
							     realizaTransaccion(req,res);
                            else
    							despliegaPaginaErrorURL("<font color=red>Error H701:Cuenta Inv&aacute;lida </font><br> ","Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0", req, res);

						}

					}
					else if (modulo.equals("3"))
						enviaDatosComprobante(req,res);

					//inicia Fase II, Pago de SAR
					else if (modulo.equals("6")){
						iniciaConsulta(req,res);
					}
					else if (modulo.equals("7"))
						realizaConsulta(req,res);
					else if (modulo.equals("8"))
						generaTablaConsultarRegistrosPorPaginacion(req,res);
					else if(modulo.trim().equals("9")) // Comprobante PDF
						generaComprobante("PDF",req,res);
					else if(modulo.trim().equals("10")) // Comprobante HTML
						generaComprobante("HTML",req,res);
					else if (modulo.equals("11"))
						generaArchivoParaExportar(req,res);
					else if (modulo.equals("12"))
						cancelaPago(req,res);
				}
				else
				{
					EIGlobal.mensajePorTrace( "ConsultaUsuarios.java::defaultAction()", EIGlobal.NivelLog.INFO);

					String[][] arrayUsuarios = TraeUsuarios(req);
					req.setAttribute( "ContUser", ObtenContUser(req) );
					req.setAttribute( "newMenu", session.getFuncionesDeMenu());
					req.setAttribute( "MenuPrincipal", session.getStrMenu());
					req.setAttribute( "Encabezado", CreaEncabezado( "Usuarios Asociados al Contrato", "Administraci&oacute;n y Control &gt; Usuarios", "s26100h" ,req) );


						StringBuffer Usuarios = new StringBuffer("");
						String NoUsuario = "";

						long UserAux = 0;
						long numero1 = 0;
						long numero2 = 0;

						Usuarios.append("<table border=\"0\" cellspacing=\"2\" cellpadding=\"3\"" );
						Usuarios.append(" class=\"tabfonbla\" align=\"center\">\n<tr><td align=\"center\"");
						Usuarios.append(" class=\"tittabdat\" width=\"120\">C&oacute;digo de Cliente</td>");
						Usuarios.append("<td align=\"center\" class=\"tittabdat\" width=\"220\">Nombre</td></tr>\n");

						EIGlobal.mensajePorTrace("numero de usuarios " + arrayUsuarios[0][0], EIGlobal.NivelLog.INFO);

						Convertidor con=new Convertidor();

						for (int indice=1;indice<=Integer.parseInt(arrayUsuarios[0][0].trim());
							 indice++){

							int residuo=indice % 2;
							String nameClass = "textabdatobs";

							if (residuo == 0){
								nameClass="textabdatcla";
							}

							Usuarios.append("<tr><td class=\"" );
							Usuarios.append(nameClass );
							Usuarios.append("\">&nbsp;");
							Usuarios.append(rellenaCon(con.cliente_7To8(arrayUsuarios[indice][1].trim()),'0',7,true) );
							Usuarios.append("&nbsp;</td><td class=\"" );
							Usuarios.append(nameClass );
							Usuarios.append("\">&nbsp;" );
							Usuarios.append(arrayUsuarios[indice][2] );
							Usuarios.append("&nbsp;</td></tr>\n");
						}

						Usuarios.append("</table>");
						req.setAttribute("Usuarios",Usuarios.toString());
						req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
						//TODO BIT CU5101, inicia flujo consulta usuarios : 4381 Consulta de dependencias
						if (Global.USAR_BITACORAS.trim().equals("ON")){

							BitaHelper bh = new BitaHelperImpl(req, session, sess);

							if(req.getParameter(BitaConstants.FLUJO) != null){
								bh.incrementaFolioFlujo(req.getParameter(BitaConstants.FLUJO));
							}
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);

							if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
								     equals(BitaConstants.EA_USUARIOS)){
									bt.setNumBit(BitaConstants.EA_USUARIOS_ENTRA);
							}else
								if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
								     equals(BitaConstants.ES_SAR_PAGOS)){
									bt.setNumBit(BitaConstants.ES_SAR_PAGOS_CONSULTA_DEPENDENCIA);
								}
							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
							}

							try{

								BitaHandler.getInstance().insertBitaTransac(bt);

							} catch (SQLException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						}
						evalTemplate( IEnlace.CONSULTA_USUARIOS_TMPL, req, res );
				}

			}
			//Antes de fin de sesion Valida
		}else{
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}
    }

    /**
     * Metodo que rellena una cadena con un caracter especificado
     * @param origen Cadena de origen
     * @param caracter Caracter con el que se va a llener
     * @param cantidad Longitud de la cadena
     * @param izquierda Indica si se va a rellenar a la izquierda o derecha
     * @return Regresa una cadena del tama�o que se indico
     */
    public String rellenaCon( String origen, char caracter, int cantidad,
                              boolean izquierda){
        if(origen.length() < cantidad)
            for(int contador = 0 ; contador<(cantidad+ 1 -origen.length());
                    contador++)

                if(izquierda)
                    origen = caracter + origen;
                else
                    origen += caracter;
        return origen;
    }

	 /**
	  * Metodo para extraer la fecha del sistema 12-12-06
	  * @return Regresa una cadena con la fecha del sistema
	  */
 public static String getFechaSistema()
{
	GregorianCalendar hoy = new GregorianCalendar();
	String fchDD = "" ;
	String fchMM = "" ;
	String fchYYYY = "" ;
	String FechaSistema ="";
	fchDD = lpad( Integer.toString(hoy.get(GregorianCalendar.DAY_OF_MONTH)), "0", 2 ).toString() ;
	fchMM = lpad( Integer.toString(hoy.get(GregorianCalendar.MONTH)+ 1), "0", 2 ).toString() ;
	fchYYYY = Integer.toString(hoy.get(GregorianCalendar.YEAR));
	FechaSistema = fchYYYY+fchMM+fchDD;
	return  FechaSistema;
}

/**
 * Metodo que rellena la cadena por la izquierda hasta la longitud que
 * se indique y con el caracter definido
 * @param s Cadena original
 * @param c Caracter de tipo String para rellenar
 * @param n Longitud de la cadena
 * @return Regresa la cadena con la longitud especificada
 */
public static String lpad(String s,String c, int n)
{
	while (s.length() < n)
	{
		s = c+s;
    }
    return s;
}

/**
 * M�todo:      IniciaPagoSar
 * Descripci�n: Este m�todo se encarga de enviar par�metros de inicio al archivo LineaCaptura.jsp, que contiene el
 * applet de validaci�n de la l�nea de captura.
 * @param req Request de la pagina
 * @param res Response de la pagina
 * @throws ServletException Excepcion en caso de que ocurra algun error
 * @throws IOException Excepcion en caso de que ocurra algun error
 */
	public void iniciaPagoSar(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
       HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   req.setAttribute("MenuPrincipal", session.getStrMenu());
	   req.setAttribute("newMenu", session.getFuncionesDeMenu());
	   req.setAttribute("Encabezado", CreaEncabezado("Pago SAR","Servicios &gt; SAR &gt; Pagos","s35004h",req));
	   req.setAttribute("ArchivoErr","");
	   //	 TODO BIT CU 4391, Pago SAR: 4381 PAGO SAR
	   if (Global.USAR_BITACORAS.trim().equals("ON")){
		   try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				EIGlobal.mensajePorTrace("ID_Flujo: ", EIGlobal.NivelLog.INFO);
				 if(req.getParameter(BitaConstants.FLUJO)!=null){
					   bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				   }else if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
						   equals(BitaConstants.ES_SAR_PAGOS)){
					   bh.incrementaFolioFlujo(BitaConstants.ES_SAR_PAGOS);
				   }else{
					   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				   }

				if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					     equals(BitaConstants.ES_SAR_PAGOS)){
						bt.setNumBit(BitaConstants.ES_SAR_PAGOS_ENTRA);
				}else
					if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					     equals(BitaConstants.ES_SAR_CONS_CANCEL)){
						bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_REALIZA_PAGO_SAR);
					}
					bt.setContrato(session.getContractNumber());
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
	   }
	   evalTemplate("/jsp/LineaCaptura.jsp", req, res );

    }

	/**
	 * M�todo:       ConsultaDependencia:
	 * Descripci�n:  Este m�todo se encarga de enviar  los datos necesarios para realizar la consulta
	 * de dependencia, as� como presentar los resultados de dicha consulta en el archivo Dependencia.jsp.
	 * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws ServletException Excepcion en caso de que ocurra algun error
	 * @throws IOException Excepcion en caso de que ocurra algun error
	 */
	public void consultaDependencia(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		// modificar arreglo a 11 ahora el applet regresa 11 campos
		String[] arrLineaCap=new String[11];
		String lineaCaptura = (String)req.getParameter("LineCaptureValidate");
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		arrLineaCap=desentramaLineaCaptura(lineaCaptura);

		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("****** Aqui imprime la Fecha de la LC ******", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Fecha LC = " + arrLineaCap[9], EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("****** Aqui imprime la Fecha del Sistema ******", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Fecha LC = " + getFechaSistema(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);

		// Funcion que valida la fecha del sistema con la fecha de la linea de captura
		if (!validaFechaSistema (arrLineaCap[9],getFechaSistema())){
			String FechaFormato ="";
			FechaFormato = getFechaSistema();
			FechaFormato = FechaFormato.substring(6,8)+"/"+FechaFormato.substring(4,6)+"/" + FechaFormato.substring(0, 4);
			despliegaPaginaErrorURL("<font color=red>Error </font><br> "+ "La Fecha  de Caducidad es Menor que la Fecha del Sistema. <BR> Fecha Sistema: " + FechaFormato + "<BR> Fecha de Caducidad: " + arrLineaCap[9].substring(6,8)+"/"+arrLineaCap[9].substring(4,6)+"/" +arrLineaCap[9].substring(0,4) ,"Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0", req, res);
		}
		else
		{
			String tramaEnviada ="";
			tramaEnviada="1EWEB|"+session.getUserID8()+"|SARD|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|A@" + arrLineaCap[1] + "@";
			EIGlobal.mensajePorTrace("Consulta Usuarios - ConsultaDependencia(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
			ServicioTux tuxGlobal = new ServicioTux();
			Hashtable htResult=null;

			String mifecha =null;

			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("****** Aqui imprime la LC******", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Recibe  LC " + lineaCaptura, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Debe de Imprimir la fecha del sistema", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(""+ getFechaSistema(), EIGlobal.NivelLog.DEBUG);

			try{
				htResult=tuxGlobal.web_red(tramaEnviada);
			}
			catch(java.rmi.RemoteException re){
				EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
			} catch(Exception e) {}

			String CodError = ( String ) htResult.get( "BUFFER" );
			EIGlobal.mensajePorTrace("ConsultaUsuarios consultaDepoendencia- CodError contiene"+CodError, EIGlobal.NivelLog.DEBUG);

			if (CodError.substring(0,2).trim().equals("OK"))
			{
				String[] arrCodError=null;
				arrCodError=desentrama(CodError,'@');

				int NumeroError = Integer.parseInt(arrCodError[2].substring(4,8));

				if (NumeroError==0)
				{
					//en este momento se quitan los ceros a la l�nea de captura
					String lineaCapFormat="";
					String cadVivienda="";
					String cadRetiro="";
					String cadTotal="";
					StringBuffer varFechaHoy=new StringBuffer();
					String strInhabiles = "";

					varFechaHoy.append("  //Fecha De Hoy");
					varFechaHoy.append("\n  dia=new Array(");
					varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
					varFechaHoy.append(",");
					varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
					varFechaHoy.append(");");

					varFechaHoy.append("\n  mes=new Array(");
					varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
					varFechaHoy.append(",");
					varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
					varFechaHoy.append(");");

					varFechaHoy.append("\n  anio=new Array(");
					varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
					varFechaHoy.append(",");
					varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
					varFechaHoy.append(");");

					varFechaHoy.append("\n\n  //Fechas Seleccionadas");
					varFechaHoy.append("\n  dia1=new Array(");
					varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
					varFechaHoy.append(",");
					varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
					varFechaHoy.append(");");

					varFechaHoy.append("\n  mes1=new Array(");
					varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
					varFechaHoy.append(",");
					varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
					varFechaHoy.append(");");

					varFechaHoy.append("\n  anio1=new Array(");
					varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
					varFechaHoy.append(",");
					varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
					varFechaHoy.append(");");

					varFechaHoy.append("\n  Fecha=new Array('");
					varFechaHoy.append(EIGlobal.generaFechaAnt("FECHA",0));
					varFechaHoy.append("','");
					varFechaHoy.append(EIGlobal.generaFechaAnt("FECHA",0));
					varFechaHoy.append("');");

					lineaCapFormat=arrLineaCap[0]+"    "+arrLineaCap[1]+"    "+arrLineaCap[2]+"    " + arrLineaCap[3] + "    "+arrLineaCap[4]+"    "
						+arrLineaCap[5]+"    "+arrLineaCap[6]+"    "+arrLineaCap[7]+"    "+arrLineaCap[8]+ "    "+arrLineaCap[9]+"    "+arrLineaCap[10];

					//calculo de las cantidades
					String ahorro = formateaImporte(arrLineaCap[3]);
					String vivienda = formateaImporte(arrLineaCap[4]);
					String total = formateaImporte(arrLineaCap[5]);

					String bimestre= arrLineaCap[7].substring(2,6).trim()+arrLineaCap[7].substring(0,2).trim();

					/*Atributos para calendario*/
					req.setAttribute("DiasInhabiles",strInhabiles);
					req.setAttribute("VarFechaHoy",varFechaHoy.toString());
					req.setAttribute("FechaHoy",EIGlobal.generaFechaAnt("FECHA",0));

				// agregar 2 cajas de texto para incluir fecha de caducidad y Tipo de Linea de Captura
					//L�neas agregadas para saber que tipo de l�nea de captura es
					req.setAttribute("TipoLineaCaptura",arrLineaCap[10]);
					switch(Integer.parseInt(arrLineaCap[10]))
					{
						case 85:	req.setAttribute("NombreTipoLineaCaptura","RETIRO Y VIVIENDA");
									break;

						case 86:	req.setAttribute("NombreTipoLineaCaptura","PAGOS EXTEMPOR&Aacute;NEOS");
							break;

						case 87:	req.setAttribute("NombreTipoLineaCaptura","AHORRO VOLUNTARIO");
							break;

						case 88:	req.setAttribute("NombreTipoLineaCaptura","CR�DITO DE VIVIENDA");
							break;

						case 89:	req.setAttribute("NombreTipoLineaCaptura","AHORRO VOLUNTARIO CETES");
							break;
					}

					req.setAttribute("TipoPago",arrLineaCap[6]);
					req.setAttribute("CentroPago",arrLineaCap[1]);
					req.setAttribute("Bimestre",bimestre);
					req.setAttribute("Ahorro",ahorro);
					req.setAttribute("Vivienda",vivienda);
					req.setAttribute("Total", total);
					req.setAttribute("Dependencia",arrCodError[3]);

					req.setAttribute("RFC", arrCodError[4]);

					req.setAttribute("Domicilio", arrCodError[5]);

					req.setAttribute("LineCaptureValidate", lineaCapFormat);
					req.setAttribute("LineaCaptura", lineaCapFormat);
					req.setAttribute("MenuPrincipal", session.getStrMenu());
					req.setAttribute("newMenu", session.getFuncionesDeMenu());
					req.setAttribute("Encabezado", CreaEncabezado("Pago SAR","Servicios &gt; SAR &gt; Pagos","s35005h",req));

//					 TODO BIT CU 4391, Dependencias C2: 4381 Dependencias A6
					if (Global.USAR_BITACORAS.trim().equals("ON")){
						try{
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
								     equals(BitaConstants.ES_SAR_PAGOS)){
									bt.setNumBit(BitaConstants.ES_SAR_PAGOS_CONSULTA_DEPENDENCIA);
							}else
								if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
								     equals(BitaConstants.ES_SAR_CONS_CANCEL_ENTRA)){
									bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_CONSULTA_DEPENDENCIA);
								}
							bt.setContrato(session.getContractNumber());

							bt.setServTransTux("SARD");
							if(CodError!=null){
				    			if(CodError.substring(0,2).equals("OK")){
				    				bt.setIdErr("SARD0000");
				    			}else if(CodError.length()>8){
					    			bt.setIdErr(CodError.substring(0,8));
					    		}else{
					    			bt.setIdErr(CodError.trim());
					    		}
				    		}
							if((arrLineaCap[5]!=null) && (!arrLineaCap[5].equals(""))){
								bt.setImporte(Double.parseDouble(arrLineaCap[5]));
							}
							BitaHandler.getInstance().insertBitaTransac(bt);
						}catch(SQLException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}catch(Exception e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					}
					evalTemplate("/jsp/Dependencia.jsp", req, res );
				}
				else
				{
//					 TODO BIT CU 4391, Dependencias C2: 4381 Dependencias A6
					if (Global.USAR_BITACORAS.trim().equals("ON")){
						try{
							BitaHelper bh = new BitaHelperImpl(req, session, sess);

							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							EIGlobal.mensajePorTrace("VSWF Id_flujo -->"+ (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)) +"\n\n", EIGlobal.NivelLog.INFO);
							if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
								     equals(BitaConstants.ES_SAR_PAGOS)){
									bt.setNumBit(BitaConstants.ES_SAR_PAGOS_CONSULTA_DEPENDENCIA);
							}else
								if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
								     equals(BitaConstants.ES_SAR_CONS_CANCEL_ENTRA)){
									bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_CONSULTA_DEPENDENCIA);
								}
							EIGlobal.mensajePorTrace("VSWF Id_flujo 2 -->"+ (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO))+"\n\n", EIGlobal.NivelLog.INFO);
							bt.setContrato(session.getContractNumber());
							bt.setServTransTux("SARD");
							if(CodError!=null){
				    			if(CodError.substring(0,2).equals("OK")){
				    				bt.setIdErr("SARD0000");
				    			}else if(CodError.length()>8){
					    			bt.setIdErr(CodError.substring(0,8));
					    		}else{
					    			bt.setIdErr(CodError.trim());
					    		}
				    		}
							if((arrLineaCap[5]!=null) && (!arrLineaCap[5].equals(""))){
								bt.setImporte(Double.parseDouble(arrLineaCap[5]));
							}
							BitaHandler.getInstance().insertBitaTransac(bt);
						}catch(SQLException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}catch(Exception e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}
					}
					EIGlobal.mensajePorTrace("\n\nNumeroError -->"+ NumeroError +"\n\n", EIGlobal.NivelLog.INFO);

					despliegaPaginaErrorURL("<font color=red>Error </font><br> "+arrCodError[3].replace('<',' ').replace('>',' '),"Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0", req, res);
				}
			}
			else{
//				 TODO BIT CU 4391, Dependencias C2: 4381 Dependencias A6
				if (Global.USAR_BITACORAS.trim().equals("ON")){
					try{
						BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
							     equals(BitaConstants.ES_SAR_PAGOS)){
								bt.setNumBit(BitaConstants.ES_SAR_PAGOS_CONSULTA_DEPENDENCIA);
						}else
							if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
							     equals(BitaConstants.ES_SAR_CONS_CANCEL_ENTRA)){
								bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_CONSULTA_DEPENDENCIA);
							}
						bt.setContrato(session.getContractNumber());
						bt.setServTransTux("SARD");
						if(CodError!=null){
			    			if(CodError.substring(0,2).equals("OK")){
			    				bt.setIdErr("SARD0000");
			    			}else if(CodError.length()>8){
				    			bt.setIdErr(CodError.substring(0,8));
				    		}else{
				    			bt.setIdErr(CodError.trim());
				    		}
			    		}
						if((arrLineaCap[5]!=null) && (!arrLineaCap[5].equals(""))){
							bt.setImporte(Double.parseDouble(arrLineaCap[5]));
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					}catch(SQLException e){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

					}catch(Exception e){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

					}
				}
				despliegaPaginaErrorURL("<font color=red>Error </font><br> "+CodError.substring(16,CodError.length()).trim(),"Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0", req, res);
			}
		}
	}

	/**
	 * M�todo que valida fechas
	 * DVN 12-12-06
	 * @param fechalinea Fecha de la linea de captura
	 * @param fechaprog Fecha prog
	 * @return Regresa verdadero si la linea de captura es menor o igual
	 * 			a la fecha prog
	 */
	private static  boolean validaFecha( String fechalinea , String fechaprog ) {
			EIGlobal.mensajePorTrace("Entra al metodo validaFecha ", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Recibe Fecha LC " + fechalinea, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Recibe Fecha Prog " + fechaprog , EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
		String[] fecprog = new String [3];
		fecprog[0]= fechaprog.substring(0, 2);
		fecprog[1]= fechaprog.substring(3, 3);
		fecprog[2]= fechaprog.substring(6,10);
		int fechlinea = Integer.parseInt(fechalinea);
		int fechprog = Integer.parseInt(fecprog[2] + fecprog[1] + fecprog[0]);

		if (fechprog  <= fechlinea)
			return true;
		else
			return false;

	}

	/**
	 * Funcion que valida la fecha del sistema, comparandola con la fecha de la LC
	 * @param fechalinea Fecha de la linea de captura
	 * @param fechaSis Fecha del sistema
	 * @return Regresa verdadero si la fecha de la linea de captura es mayor o igual
	 * 			a la fecha del sistema
	 */
	private static  boolean validaFechaSistema( String fechalinea , String fechaSis) {
			EIGlobal.mensajePorTrace("Entra al metodo validaFechaSistema ", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Recibe Fecha LC " + fechalinea, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Recibe Fecha Sistema " + fechaSis, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
		int fechlinea = Integer.parseInt(fechalinea);
		int fechsistema = Integer.parseInt(fechaSis);

		if ( fechlinea >= fechsistema)
			return true;
		else
			return false;

	}

	/**
	 * M�todo:       realizaTransaccion
	 * Descripci�n:  Este m�todo es el encargado de enviar los datos necesarios para realizar el pago de SAR. En caso de
	 * que la transacci�n fuera exitosa presentar� la pantalla del archivo ConfirmaTransacSAR.jsp, en caso contrario mostrara el error ocurrido.
	 * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws ServletException Excepcion en caso de que ocurra algun error
	 * @throws IOException Excepcion en caso de que ocurra algun error
	 */
	public void realizaTransaccion(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		String codErrorOper = "";
		String[] arrLineaCap=new String[11];
		String lineaCapturaFormato="";
		String bimestre="";
		String[] cuenta = null;
		String mancomunidad="";
		String fecha="";
		String fechaProgramada="";
		String fechaFormato="";
		String lineaCaptura = (String)req.getParameter("LineCaptureValidate");
		boolean esProgramada=false;

		// SE AGREGO ESTA VARIABLE PARA VALIDAR EL PAGO
		boolean fechaok=false;
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");

		cuenta = desentrama(req.getParameter("cuenta"),'|');
		arrLineaCap=desentrama(lineaCaptura, "    ");
		//formateo de la linea de captura para que contenga 62 caracteres
		//SAR-NUEVO -APPLET se agregan los nuevos campos para la linea de captura
		lineaCaptura=arrLineaCap[0]+arrLineaCap[1]+arrLineaCap[2]+arrLineaCap[3]+arrLineaCap[4]
			+arrLineaCap[5]+arrLineaCap[6]+arrLineaCap[7]+arrLineaCap[8]+arrLineaCap[9]+arrLineaCap[10];
		// SAR-NUEVO.APPLET Se ajusto al tama�o a 65 caracteres pero no separa el ultimo campo de TLC
		lineaCapturaFormato = formateaCampo(lineaCaptura,65," ",false);
		EIGlobal.mensajePorTrace("Consulta Usuarios - realizaTransaccion <"+lineaCapturaFormato+">", EIGlobal.NivelLog.INFO);

		//calculo del bimestre con el formato BAAAA (Bimestre-A�o)
		bimestre= arrLineaCap[7].substring(2,6).trim()+arrLineaCap[7].substring(1,2).trim();
		mancomunidad=req.getParameter("refmancomunidad");
		if ((mancomunidad==null) || (mancomunidad.equals("")))
			mancomunidad="  ";

		fecha=req.getParameter("fechaHoy");//fecha de hoy con formato dd/MM/yyyy
		fechaProgramada=(String)req.getParameter("FechaProg");//formato dd/mm/yyyy
		//fecha con formato yyyy-mm-dd
		fechaFormato=formateaFecha(fechaProgramada);

		//formato de la trama 1EWEB|contrato|tipo_operacion|usuario|cve_perfil|..@..@|folio_manc|importe|cuenta|

		EIGlobal.mensajePorTrace("Debe de llegar aqui", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Imprime Fecha Programada " +fechaProgramada, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Imprime Fecha " + fecha, EIGlobal.NivelLog.DEBUG);
		String tramaEnviada ="";
		if (fechaProgramada.equals(fecha))
		{

			tramaEnviada="1EWEB|"+session.getUserID8()+"|SARA|"+session.getContractNumber()+"|"+
				session.getUserID8()+"|"+session.getUserProfile()+"|"+fechaFormato+"@"+lineaCapturaFormato+"@"+
				formateaCampo(arrLineaCap[3],15,"0",true)+"@"+formateaCampo(arrLineaCap[4],15,"0",true)+"@"+
				formateaCampo(arrLineaCap[5],15,"0",true)+"@"+bimestre+"@"+arrLineaCap[6]+"@"+cuenta[0]+"@"+
				session.getContractNumber()+"@"+session.getUserID8()+"@|"+mancomunidad+"|"+
				formateaCampo(arrLineaCap[5],15,"0",true)+"|"+cuenta[0]+"|L|";
			esProgramada=false;
			EIGlobal.mensajePorTrace("***Imprime Trama*** " +tramaEnviada, EIGlobal.NivelLog.DEBUG);
			fechaok=true;
			EIGlobal.mensajePorTrace("***Imprime Fechaok *** " +fechaok, EIGlobal.NivelLog.DEBUG);

		}
		else
		{
			//			|mmddaa|cta        |cuerpo  |folio_mac|importe|ban_manc
			EIGlobal.mensajePorTrace("***** Entra a la funcion Valida Fecha *****", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Imprime Fecha LC " + arrLineaCap[9], EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("Imprime Fecha Sistema " + fecha, EIGlobal.NivelLog.DEBUG);
			if (!validaFecha (arrLineaCap[9],fechaProgramada)) {
				despliegaPaginaErrorURL("<font color=red>Error </font><br> "+ "La Fecha Programada es Mayor a la Fecha de Caducidad de la L�nea de Captura. <BR> Fecha Programada: " +fechaProgramada + "<BR> Fecha de Caducidad: " + arrLineaCap[9].substring(6,8)+"/"+arrLineaCap[9].substring(4,6)+"/" +arrLineaCap[9].substring(0,4) ,"Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0", req, res);
				fechaok=false;
			}
			else
			{
				fechaok = true;
				//											|mmddaa|cta        |cuerpo  |folio_mac|importe|ban_manc
				String fechaProgFormato="";
				fechaProgFormato=fechaProgramada.substring(3,5)+fechaProgramada.substring(0,2)+fechaProgramada.substring(8,10);
				tramaEnviada="1EWEB|"+session.getUserID8()+"|PROG|"+session.getContractNumber()+"|"+
					session.getUserID8()+"|"+session.getUserProfile()+"|SARA|"+fechaProgFormato+"|"+
					cuenta[0]+"|"+fechaFormato+"@"+lineaCapturaFormato+"@"+formateaCampo(arrLineaCap[3],15,"0",true)+"@"+
					formateaCampo(arrLineaCap[4],15,"0",true)+"@"+formateaCampo(arrLineaCap[5],15,"0",true)+"@"+
					bimestre+"@"+arrLineaCap[6]+"@"+cuenta[0]+"@"+session.getContractNumber()+"@"+
					session.getUserID8()+"@|"+mancomunidad+"|"+formateaCampo(arrLineaCap[5],15,"0",true)+"|L|";
				esProgramada=true;
			}
		}

			EIGlobal.mensajePorTrace("***Imprime Fechaok despues de validar la fecha  *** " +fechaok, EIGlobal.NivelLog.DEBUG);
			if (fechaok)
			{
				EIGlobal.mensajePorTrace("ConsultaUsuarios - realizaTransaccion(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
				ServicioTux tuxGlobal = new ServicioTux();
				Hashtable htResult=null;

				try{
					htResult=tuxGlobal.web_red(tramaEnviada);
					codErrorOper = htResult.get("COD_ERROR") + "";
				}
				catch(java.rmi.RemoteException re){
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				} catch(Exception e) {}

				String CodError = ( String ) htResult.get( "BUFFER" );
				EIGlobal.mensajePorTrace("ConsultaUsuarios- RealizaTransaccion -El contenido de CodError es JPPM 5:05"+CodError, EIGlobal.NivelLog.DEBUG);

				if (CodError.substring(0,2).trim().equals("OK"))
				{
					if (esProgramada==false)
					{
						EIGlobal.mensajePorTrace("Es programada = false", EIGlobal.NivelLog.DEBUG);
						String[] arrCodError=null;
						arrCodError=desentrama(CodError,'@');
						//Trama de regreso exitosa ok  @referencia@SAR_0000@Transeferencia existosa@Cta Clabe@referencia de Cargo@
						//Trama de regerso erronea ok  @referencia@SAR_0004@Mensaje   de      error@

						int NumeroError = Integer.parseInt(arrCodError[2].substring(4,8));
						 EIGlobal.mensajePorTrace("*********************prueba 1: " + NumeroError, EIGlobal.NivelLog.INFO);
						if (NumeroError==0)
						{
							String TramaImprimir=arrCodError[4]+"|"+cuenta[2]+"|"+fechaFormato+"|"+req.getParameter("txtLineaCaptura")+"|"+req.getParameter("txtBimestre")+"|";
								TramaImprimir+=req.getParameter("txtTipoPago")+"|"+req.getParameter("txtRetiro")+"|"+req.getParameter("txtVivienda")+"|";
								TramaImprimir+=req.getParameter("txtTotal")+"|"+arrCodError[5]+"|"+req.getParameter("txtDependencia")+"|"+req.getParameter("txtRFC")+"|";//lineas Nuevas
								TramaImprimir+=req.getParameter("txtCentroPago")+"|"+req.getParameter("txtDomicilio")+"|"+req.getParameter("txtTipoLineaCap")+"|"+req.getParameter("txtTipoLinea")+"|";

							//Formato anterior de Trama Imprimir
							//Cuenta Clabe|Descripcion Cuenta|Fecha|Linea Captura|Bimestre|Tipo Pago|$Retiro|$vivienda|$total|referencia|Dependencia|
							//RFC Dependencia|CentroPago|Domicilio
							//Formato Nuevo de Trama Imprimir
							//Cuenta Clabe|Fecha|Linea Captura|Bimestre|Tipo Pago|$Retiro|$vivienda|$total|referencia|Dependencia|
							//RFC Dependencia|CentroPago|Domicilio|

							EIGlobal.mensajePorTrace("ConsultaUsuarios - realizaTransaccion: TramaImprimir contiene: "+TramaImprimir, EIGlobal.NivelLog.DEBUG);
							req.setAttribute("MenuPrincipal", session.getStrMenu());
							req.setAttribute("newMenu", session.getFuncionesDeMenu());
							req.setAttribute("Encabezado", CreaEncabezado("Pago SAR","Servicios &gt; SAR &gt; Pagos","",req));
							req.setAttribute("TramaImprimir", TramaImprimir);
							req.setAttribute("ReferenciaEnlace", arrCodError[1]);//referencia de enlace
							req.setAttribute("ReferenciaCargo", arrCodError[5]);//referencia de cargo
							//TODO: BIT CU 4391. Realiza transac
							if (Global.USAR_BITACORAS.trim().equals("ON")){
								try{
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
									     equals(BitaConstants.ES_SAR_PAGOS)){
										bt.setNumBit(BitaConstants.ES_SAR_PAGOS_REALIZA_PAGO_SAR);
								}else
									if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
									     equals(BitaConstants.ES_SAR_CONS_CANCEL_ENTRA)){
										bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_REALIZA_PAGO_SAR);
									}

								bt.setContrato(session.getContractNumber());
								if(cuenta[0]!=null){
									bt.setCctaOrig(cuenta[0]);
								}
								bt.setServTransTux("SARA");
								if(CodError!=null){
					    			if(CodError.substring(0,2).equals("OK")){
					    				bt.setIdErr("SARA0000");
					    			}else if(CodError.length()>8){
						    			bt.setIdErr(CodError.substring(0,8));
						    		}else{
						    			bt.setIdErr(CodError.trim());
						    		}
					    		}
								bt.setFechaProgramada(Utilerias.MdyToDate(fechaProgramada));
								log("VSWF: bandera token " + req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN));
								if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
											.equals(BitaConstants.VALIDA)) {
									bt.setIdToken(session.getToken().getSerialNumber());
								}else{
									log("VSWF: no hubo bandera de token");
								}
								req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);

								BitaHandler.getInstance().insertBitaTransac(bt);
								}catch (SQLException e){
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

								}catch(Exception e){
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

								}
							}
							evalTemplate("/jsp/ConfirmaTransacSAR.jsp", req, res );
						}
						else
						{
							EIGlobal.mensajePorTrace("Es programada = true", EIGlobal.NivelLog.DEBUG);
							despliegaPaginaErrorURL("<font color=red>Error </font><br> "+arrCodError[3].replace('<',' ').replace('>',' '),"Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0", req, res);
						}
					}
					else//esProgramada=true
					{
						//Trama de regreso exitosa ok  @referencia@
						 EIGlobal.mensajePorTrace("*********************prueba 2: " + fechaProgramada + CodError.substring(12), EIGlobal.NivelLog.INFO);
						//se utiliza el m�todo despliegaPaginaErrorURL, no porque exista un error
						//sino unicamente para mostrar el mensaje de confirmaci�n del pago programado
						despliegaPaginaErrorURL("Su pago fu&eacute; programado para el d&iacute;a "+fechaProgramada+",con el n&uacute;mero de referencia "+CodError.substring(12)+".","Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0",req,res);
					}
						 try{
							 String[] arrCodErrorMsg=null;
					         arrCodErrorMsg=desentrama(CodError,'@');
					         EIGlobal.mensajePorTrace("CodError: " + CodError, EIGlobal.NivelLog.INFO);
							 int NumeroErrorMsg = Integer.parseInt(arrCodErrorMsg[2].substring(4,8));
							 EIGlobal.mensajePorTrace("NumeroErrorMsg: " + NumeroErrorMsg, EIGlobal.NivelLog.INFO);

							   EmailSender emailSender = new EmailSender();
				     		   EmailDetails EmailDetails = new EmailDetails();

					           EIGlobal.mensajePorTrace("CodError: " + CodError, EIGlobal.NivelLog.INFO);
					           EIGlobal.mensajePorTrace("arrCodErrorMsg: " + arrCodErrorMsg, EIGlobal.NivelLog.INFO);
					           EIGlobal.mensajePorTrace("CONTRATO: " + session.getContractNumber().toString(), EIGlobal.NivelLog.INFO);
					           EIGlobal.mensajePorTrace("NOMBRE CONTRATO: " + session.getNombreContrato().toString(), EIGlobal.NivelLog.INFO);
					           EIGlobal.mensajePorTrace("REFERENCIA DE ENLACE: " + arrCodErrorMsg[1], EIGlobal.NivelLog.INFO);
					           String importeStr = req.getParameter("txtTotal");
					    	   EIGlobal.mensajePorTrace("IMPORTE: " + importeStr, EIGlobal.NivelLog.INFO);
					    	   EIGlobal.mensajePorTrace("CUENTA: " + cuenta[0] + "-" + cuenta[1], EIGlobal.NivelLog.INFO);
					    	   EIGlobal.mensajePorTrace("MANCOMUNIDAD: " +  mancomunidad, EIGlobal.NivelLog.INFO);
					    	   EIGlobal.mensajePorTrace("codErrorOper: " +  codErrorOper, EIGlobal.NivelLog.INFO);

					    	   EmailDetails.setNumeroContrato(session.getContractNumber().toString());
					           EmailDetails.setRazonSocial(session.getNombreContrato().toString());
					           EmailDetails.setNumRef(arrCodErrorMsg[1]);
					           EmailDetails.setNumCuentaCargo(cuenta[0]);
					           EmailDetails.setImpTotal(importeStr);
					           EmailDetails.setTipoPagoImp("SAR");

					           if(emailSender.enviaNotificacion(codErrorOper)){

									EmailDetails.setEstatusActual(codErrorOper);
									emailSender.sendNotificacion(req, IEnlace.PAGO_SAR, EmailDetails);
					           }
				 		} catch(Exception e) {
				 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				 		}
				}
				else
					despliegaPaginaErrorURL("<font color=red>Error </font><br> "+CodError.substring(16,CodError.length()).trim(),"Pago SAR","Servicios &gt; SAR &gt; Pagos" ,"","0", req, res);
			}
	}

	/**
	 * M�todo:       enviaDatosComprobante
	 * Descripci�n:  Es el encargado de enviar los datos al frame del comprobante final (ComprobanteSAR.jsp) y
	 * presentarlo en pantalla para su impresi�n.
	 * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws ServletException Excepcion en caso de que ocurra algun error
	 * @throws IOException Excepcion en caso de que ocurra algun error
	 */
	public void enviaDatosComprobante(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		//Cuenta Clabe|Fecha|Linea Captura|Bimestre|Tipo Pago|$Retiro|$vivienda|$total|referencia|Dependencia|
		//RFC Dependencia|CentroPago|Domicilio
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String[] datosComprobante=desentrama(req.getParameter("TramaImprimir"),'|');

		//convierte la clave de usuario de 7 a 8 caracteres
		Convertidor objConv = new Convertidor();
		String IDUsuario = objConv.cliente_7To8(session.getUserID8());

		req.setAttribute("CuentaCargo", datosComprobante[0]);
		req.setAttribute("DescripCuenta", datosComprobante[1]);
		req.setAttribute("Fecha", datosComprobante[2]);
		req.setAttribute("LineaCaptura", datosComprobante[3]);
		req.setAttribute("Bimestre", datosComprobante[4]);
		req.setAttribute("TipoPago", datosComprobante[5]);
		req.setAttribute("Retiro", datosComprobante[6]);
		req.setAttribute("Vivienda", datosComprobante[7]);
		req.setAttribute("Total", datosComprobante[8]);
		req.setAttribute("Referencia", datosComprobante[9]);
		//SAR-APPLET-NUEVO L�neas Nuevas
		req.setAttribute("TipoLineaCaptura", datosComprobante[14]);
		req.setAttribute("NombreTipoLineaCaptura", datosComprobante[15]);
		// L�neas Nuevas
		req.setAttribute("Dependencia", datosComprobante[10]);
		req.setAttribute("RFC", datosComprobante[11]);
		req.setAttribute("CentroPago", datosComprobante[12]);
		req.setAttribute("Domicilio", datosComprobante[13]);
		//datos de enlace
		req.setAttribute("NumContrato", session.getContractNumber());
		req.setAttribute("NombreUsuario", session.getNombreUsuario());
		req.setAttribute("NombreContrato", session.getNombreContrato());
		req.setAttribute("IDUsuario", IDUsuario);
		req.setAttribute("Operacion", "A");//Indica si es alta de linea(A) o consulta(C)
		evalTemplate("/jsp/ComprobanteSAR.jsp", req, res );

	}

	/**
	 * Pone ceros a la izquierda de acuerdo al tama�o proporcionado en longCampo
	 * @param campo Indica el campo que se quiere formatear
	 * @param longCampo Indica la longitud del campo
	 * @param caracter Indica el caracter con el que se va a rellenar
	 * @param izquierda Indica si es a la izquierda o derecha
	 * @return Regresa una cadena formateada
	 */
	public String formateaCampo(String campo, int longCampo, String caracter, boolean izquierda)
	{
		String cadena="";
		if (campo!=null)
		{
			cadena=campo;
			int total= longCampo-campo.length();
			if (izquierda)
			{
				for (int i=1;i<=total;i++ )
					cadena= caracter + cadena;
			}
			else
				for (int i=1;i<=total;i++ )
					cadena+=caracter;
		}
		else
			cadena=" ";
		return cadena;
	}

	/**
	 * M�todo:       desentrama
	 * Descripci�n:  Se encarga de desentramar una cadena que contenga tokens, tiene 2 m�todos sobrecargados.
	 * @param cadena Cadena principal
	 * @param caracter String Delimitador de los tokens
	 * @return Regresa un arreglo de cadenas
	 */
	public String[] desentrama(String cadena, String caracter)
	{

		StringTokenizer separador = new StringTokenizer(cadena, caracter);
		int nDatos=separador.countTokens();
		String[] result=new String[nDatos];
		int i =0;
		while (separador.hasMoreTokens())
		{
			result[i]=separador.nextToken().trim();
			EIGlobal.mensajePorTrace("ConsultaUsuarios- desentrama() elemento: " + i + " es : "+ result[i], EIGlobal.NivelLog.DEBUG);
			i++;
		}
		return result;
	}

	/**
	 * M�todo:       desentrama
	 * Descripci�n:  Se encarga de desentramar una cadena que contenga tokens, tiene 2 m�todos sobrecargados.
	 * @param cadena Cadena principal
	 * @param caracter Caracter Delimitador de los tokens
	 * @return Regresa un arreglo de cadenas
	 */
	public String[] desentrama(String cadena, char caracter)
	{

		String vartmp="";
	    String regtmp="";
	    int totalCampos=0;
		int tamanioArreglo=0;
	    String[] camposTabla;

	    regtmp = cadena;
	    for (int i = 0; i < regtmp.length(); i++ )
		{
		  if ( regtmp.charAt( i ) == caracter )
			totalCampos++;
		}
		EIGlobal.mensajePorTrace( "ConsultaUsuarios desentrama() -  Total de campos : " + Integer.toString( totalCampos ), EIGlobal.NivelLog.INFO);

		camposTabla = new String[totalCampos];

		for (int i = 0; i < totalCampos; i++ )
		{
			if ( regtmp.charAt( 0 ) == caracter )
			{
				vartmp = " ";
				regtmp = regtmp.substring( 1, regtmp.length() );
			}
			else
			{
				vartmp = regtmp.substring( 0, regtmp.indexOf( caracter )).trim();
				if ( regtmp.indexOf( caracter ) > 0 )
					regtmp=regtmp.substring( regtmp.indexOf( caracter ) + 1,regtmp.length() );
			}
			camposTabla[i]=vartmp;
			EIGlobal.mensajePorTrace( "ConsultaUsuarios desentrama() -  Elemento " + i + " es: " + camposTabla[i] , EIGlobal.NivelLog.INFO);
		}
		return camposTabla;
	}

	/**
	 * M�todo:       desentramaLineaCaptura
	 * Descripci�n:  Se encarga de separar los campos que conforman la l�nea de captura. Ejemplo: dependencia, ahorro,
	 * 	vivienda, tipo de pago, etc.
	 * @param lineaCaptura Cadena que contiene la linea de captura
	 * @return Regresa un arreglo de cadenas que contine los campos de la linea de captura
	 */
	public String[] desentramaLineaCaptura(String lineaCaptura)
	{
		String[] result=new String[11];
		result[0]=lineaCaptura.substring(0,1).trim();   //N�mero consecutivo de la dependencia
		result[1]=lineaCaptura.substring(1,8).trim();   //Clave del centro de pago
		result[2]=lineaCaptura.substring(8,11).trim();  //Clave del banco
		result[3]=lineaCaptura.substring(11,22).trim(); //Monto del pago de ahorro para el retiro
		result[4]=lineaCaptura.substring(22,33).trim(); //Monto del pago para el fondo de la vivienda
		result[5]=lineaCaptura.substring(33,45).trim(); //Monto total del pago
		result[6]=lineaCaptura.substring(45,46).trim(); //Tipo de pago, debe ser 1-->pago normal
		result[7]=lineaCaptura.substring(46,52).trim(); //A�o y Bimestre AAAABB
		result[8]=lineaCaptura.substring(52,55).trim(); //Digitos Verificadores
		result[9]=lineaCaptura.substring(55,63).trim(); //Fecha de Caducidad
		result[10]=lineaCaptura.substring(63,65).trim(); //Tipo de L�nea de Captura

		return result;
	}

	/**
	 * M�todo:       despliegaPaginaErrorURL
	 * Descripci�n:  se encarga de mostrar el error ocurrido durante alguna de las fases del proceso de pago del SAR
	 * (consulta de dependencias y transacci�n del pago).
	 * @param Error Cadena que contiene la descripcion del error
	 * @param param1 Parametro 1
	 * @param param2 Parametro 2
	 * @param param3 Parametro 3
	 * @param modulo Indica el modulo
	 * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws ServletException Excepcion en caso de que ocurra algun error
	 * @throws IOException Excepcion en caso de que ocurra algun error
	 */
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3,String modulo, HttpServletRequest req, HttpServletResponse res ) throws IOException, ServletException
	{
		//si el parametro modulo = "ESP", se presentara el error sin menus
		req.setAttribute( "Error", Error );

		if (modulo.equals("ESP"))//Error especial
			req.setAttribute( "URL", "javascript:top.window.close();");
		else
		{
			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

			req.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
			req.setAttribute( "MenuPrincipal", session.getStrMenu() );
			req.setAttribute( "newMenu", session.getFuncionesDeMenu());
			req.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,req ) );
			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
			req.setAttribute( "URL", "ConsultaUsuarios?Modulo="+modulo);
		}

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( req, res );
	}

	/**
	 * M�todo:      iniciaConsulta
	 * Descripci�n: Este m�todo se encarga de enviar par�metros de inicio al archivo ConsultaSAR.jsp, que contiene los
	 * los filtros para realizar consultas de pago de SAR en Internet.
	 * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws ServletException Excepcion en caso de que ocurra algun error
	 * @throws IOException Excepcion en caso de que ocurra algun error
	 */
	public void iniciaConsulta(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
        HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");

		StringBuffer varFechaHoy=new StringBuffer();

		EIGlobal.mensajePorTrace( "ConsultaUsuarios Entrando inicioConsulta", EIGlobal.NivelLog.INFO);

		/************************************************** Arreglos de fechas para 'Hoy' ... */
		varFechaHoy.append("  //Fecha De Hoy");
		varFechaHoy.append("\n  dia=new Array(");
		varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n  mes=new Array(");
		varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n  anio=new Array(");
		varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n\n  //Fechas Seleccionadas");
		varFechaHoy.append("\n  dia1=new Array(");
		varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(EIGlobal.generaFechaAnt("DIA",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n  mes1=new Array(");
		varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(EIGlobal.generaFechaAnt("MES",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n  anio1=new Array(");
		varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(EIGlobal.generaFechaAnt("ANIO",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n  Fecha=new Array('");
		varFechaHoy.append(EIGlobal.generaFechaAnt("FECHA",0));
		varFechaHoy.append("','");
		varFechaHoy.append(EIGlobal.generaFechaAnt("FECHA",0));
		varFechaHoy.append("');");

		req.setAttribute("VarFechaHoy",varFechaHoy.toString());
		req.setAttribute("FechaHoy",EIGlobal.generaFechaAnt("FECHA",0));
		req.setAttribute("MenuPrincipal", session.getStrMenu());
	    req.setAttribute("newMenu", session.getFuncionesDeMenu());
	    req.setAttribute("Encabezado", CreaEncabezado("Consulta y Cancelaci&oacute;n de Pagos SAR ","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","s35006h",req));
	    req.setAttribute("ArchivoErr","");
//		TODO: BIT CU 4391. El cliente entra al flujo
		if (Global.USAR_BITACORAS.trim().equals("ON")){
			try{

			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();
			if(req.getParameter(BitaConstants.FLUJO)!=null){
				   bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			   }else{
				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			   }

			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_ENTRA);
			bt.setContrato(session.getContractNumber());
			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch (SQLException ex){
				EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
	    evalTemplate("/jsp/ConsultaSAR.jsp", req, res );

    }

	/**
	 * M�todo:      realizaConsulta
	 * Descripci�n: Este m�todo se encarga de enviar el filtro de la consulta, recibir, formatear y presentar los datos
	 * 				 en caso de que la consulta haya sido exitosa.
	 * @param req Request de la pagina
	 * @param res Response de la pagina
	 * @throws ServletException Excepcion en caso de que ocurra algun error
	 * @throws IOException Excepcion en caso de que ocurra algun error
	 */
	public void realizaConsulta(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{

		String tramaFiltro="";
		String estatusArc=req.getParameter("chkEstatusArc");
		String Fecha1_01=(String)req.getParameter("Fecha1_01");
		String Fecha1_02=(String)req.getParameter("Fecha1_02");
		String importeInicial=(String)req.getParameter("impInicial");
		String importeFinal=(String)req.getParameter("impFinal");
		String referencia=(String)req.getParameter("referencia");
		String RFC=(String)req.getParameter("RFC");
		String cuenta= (String)req.getParameter("cuenta");
		//SAR-APPLET-NUEVO L�neas Nuevas para verificar el Tipo de l�nea de captura
		String TipoLineaCapturaChk = req.getParameter("chkTipoLC");

		//validaci�n por si existe espacio vacio en los parametros
		importeInicial= (importeInicial.equals(""))?" ":importeInicial;
		importeFinal= (importeFinal.equals(""))?" ":importeFinal;
		cuenta= (cuenta.equals(""))?" ":cuenta;
		referencia= (referencia.equals(""))?" ":referencia;
		RFC= (RFC.equals(""))?" ":RFC;
		//SAR-APPLET-NUEVO L�nea agregada
		TipoLineaCapturaChk =(TipoLineaCapturaChk.equals(""))?"00":TipoLineaCapturaChk;

		tramaFiltro+=Fecha1_01+"|"+Fecha1_02+"|"+importeInicial+"|";
		tramaFiltro+=importeFinal+"|"+cuenta+"|";
		//SAR-APPLET-NUEVO se le agrego al final de la trama el TLC
		tramaFiltro+=referencia+"|"+estatusArc+"|"+RFC+"|"+TipoLineaCapturaChk+"|@"; //Variable agregada TipoLineaCapturaChk
		consultaPagos(estatusArc,0,tramaFiltro,false,req,res);
    }

/**
 * Metodo que genera la tabla de consulta por paginacion
 * @param req Request de la pagina
 * @param res Response de la pagina
 * @throws ServletException Excepcion en caso de que ocurra algun error
 * @throws IOException Excepcion en caso de que ocurra algun error
 */
 public void generaTablaConsultarRegistrosPorPaginacion( HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	  EIGlobal.mensajePorTrace( "ConsultaUsuarios Entrando a paginacion de archivo", EIGlobal.NivelLog.INFO);

	  String tramaFiltro=(String)req.getParameter("tramaFiltro");
	  String nombreArchivo=(String)req.getParameter("nombreArchivo");

      int totalRegistros=Integer.parseInt((String)req.getParameter("totalRegistros"));
	  int registrosPorPagina=Integer.parseInt((String)req.getParameter("registrosPorPagina"));
	  int totalPaginas=Integer.parseInt((String)req.getParameter("totalPaginas"));
	  int pagina=Integer.parseInt((String)req.getParameter("pagina"));

	  EIGlobal.mensajePorTrace( "ConsultaUsuarios Busqueda por registros ", EIGlobal.NivelLog.INFO);

	  String[] Filtro=null;
	  Filtro=desentrama(tramaFiltro,'|');
	  if(Filtro[6].trim().equals("A"))
	    generaTablaConsultarRegistrosPagados(tramaFiltro,nombreArchivo,pagina,totalRegistros,registrosPorPagina,totalPaginas,req,res);
	  else
	    generaTablaConsultarRegistrosCanceladosRrechazados(tramaFiltro,nombreArchivo,pagina,totalRegistros,registrosPorPagina,totalPaginas,req,res);
   }


 /**
  * Metodo que genera la tabla de los registros pagados
  * @param tramaFiltro Cadena que contiene los filtros
  * @param nombreArchivo Cadena con el nombre del archivo
  * @param pagina Indica el numero de la pagina
  * @param totalRegistros Indica el total de registros
  * @param registrosPorPagina Indica cuantos registros se muestran por pagina
  * @param totalPaginas Indica el total de paginas
  * @param req Request de la pagina
  * @param res Response de la pagina
  * @throws ServletException Excepcion en caso de que ocurra algun error
  * @throws IOException Excepcion en caso de que ocurra algun error
  */
 public void generaTablaConsultarRegistrosPagados(String tramaFiltro,String nombreArchivo,int pagina,int totalRegistros,int registrosPorPagina,int totalPaginas,HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	  EIGlobal.mensajePorTrace( "ConsultaUsuarios generando la tabla de consulta", EIGlobal.NivelLog.INFO);

	  String strRegistros="";

	  HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");
	  StringBuffer tablaRegistros=new StringBuffer("");
	  StringBuffer botones=new StringBuffer("");

	  // Layout de Archivo
	  //0 Fecha Aplicacion
	  //1 LineaCaptura
	  //2 ImporteTotal
	  //3 RFC
	  //4 Referencia
	  //5 Contrato Enlace
	  //6 Usuario
	  //7 SECAPO
	  //8 Cuenta Cargo;

	  //layout de Archivo
	  //Fecha Aplicacion;LineaCaptura;ImporteTotal;RFC;Referencia;Contrato Enlace;Usuario;SECAPO;Cuenta Cargo;Tipo LC;
	  String[] titulos={"",
	                    "Fecha de Recepci&oacute;n",
						"L&iacute;nea de Captura",
						"Importe Total",
		                "RFC",
						"Cuenta Cargo",
						"Referencia",
						"Usuario Enlace",
						"Tipo LC"};

      int[] datos={8,1,0,1,2,3,8,4,6,9};// SAR-APPLET-NUEVO Si llega modificado agregar un nuevo campo y el numero 9
	  int[] align={1,0,2,1,1,1,1,1,1,1};
	  int[] values={8,7,5,4,1,2,8,6,0,9};

	  //el value de radiobutton contiene:
	  //SECAPO|Contrato Enlace|Referencia|Linea Captura|Importe Total|Cuenta Cargo|Usuario|Fecha Aplicaci�n
	  EI_Tipo regSel=new EI_Tipo();

	  EIGlobal.mensajePorTrace( "ConsultaUsuarios Preparando los datos.", EIGlobal.NivelLog.INFO);

	  strRegistros=seleccionaRegistrosDeArchivo(nombreArchivo,pagina,registrosPorPagina,totalRegistros);
	  if(strRegistros.trim().equals("ERROR"))
		  despliegaPaginaErrorURL("El archivo de <font color=blue>datos</font> no se pudo generar <b><i>correctamente</i></b>, intente en otro momento.","Consulta y Cancelaci&oacute;n de Pagos SAR","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","","6",req,res);
	  else
	   {

		  EIGlobal.mensajePorTrace( "ConsultaUsuarios Se leyo el archivo.", EIGlobal.NivelLog.INFO);
		  regSel.iniciaObjeto(';','@',strRegistros,true, 3);
		  EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
		  EnlaceGlobal.formateaImporte(regSel, 2);
		  tablaRegistros.append(regSel.generaTabla(titulos,datos,values,align));
		  EIGlobal.mensajePorTrace( "ConsultaUsuarios Objetos creados", EIGlobal.NivelLog.INFO);

		  if(totalRegistros>registrosPorPagina)
		   {
			  EIGlobal.mensajePorTrace("ConsultaUsuarios Estableciendo links para las paginas", EIGlobal.NivelLog.INFO);
			  botones.append("\n <br>");
			  botones.append("\n <table border=0 align=center>");
			  botones.append("\n  <tr>");
			  botones.append("\n   <td align=center class='texfootpagneg'>");
			  EIGlobal.mensajePorTrace("ConsultaUsuarios Pagina actual = "+pagina, EIGlobal.NivelLog.INFO);
			  if(pagina>0)
				 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
			  if(totalPaginas>=2)
			   {
				 for(int i=1;i<=totalPaginas;i++)
				  if(pagina+1==i)
				   {
					 botones.append("&nbsp;");
					 botones.append(Integer.toString(i));
					 botones.append("&nbsp;");
				   }
				  else
				   {
					 botones.append("&nbsp;<a href='javascript:BotonPagina(");
					 botones.append(Integer.toString(i));
					 botones.append(");' class='texfootpaggri'>");
					 botones.append(Integer.toString(i));
					 botones.append("</a>&nbsp;");
				   }
			   }
			  if(totalRegistros>((pagina+1)*registrosPorPagina))
				 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");

			  botones.append("\n   </td>");
			  botones.append("\n  </tr>");
			  botones.append("\n </table>");
		   }

		  req.setAttribute("fechaHoy",formateaFecha(EIGlobal.generaFechaAnt("FECHA",0)));
		  req.setAttribute("tramaFiltro",tramaFiltro);
		  req.setAttribute("nombreArchivo",nombreArchivo);
		  req.setAttribute("totalRegistros",Integer.toString(totalRegistros));
		  req.setAttribute("registrosPorPagina",Integer.toString(registrosPorPagina));
		  req.setAttribute("totalPaginas",Integer.toString(totalPaginas));
		  req.setAttribute("tablaRegistros",tablaRegistros.toString());
		  req.setAttribute("pagina",Integer.toString(pagina+1));
		  EIGlobal.mensajePorTrace( "ConsultaUsuarios generaTablaConsultarRegistrosPagados - pagina =" + Integer.toString(pagina+1), EIGlobal.NivelLog.INFO);
		  req.setAttribute("Botones",botones.toString());
		  req.setAttribute( "newMenu", session.getFuncionesDeMenu());
          req.setAttribute( "MenuPrincipal", session.getStrMenu());
		  req.setAttribute( "Encabezado", CreaEncabezado("Consulta y Cancelaci&oacute;n de Pagos SAR ","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","s35007h",req) );

		  req.setAttribute("Pagina","1");
		  req.setAttribute("Modulo","8");

		  EIGlobal.mensajePorTrace( "ConsultaUsuarios Se presenta pantalla de importacion", EIGlobal.NivelLog.INFO);

		  evalTemplate ("/jsp/ResultadoSARPag.jsp", req, res );
	   }
   }
 /**
  * Genera la tabla de consulta de registros cancelados y rechazados
  * @param tramaFiltro Cadena que contiene los filtros
  * @param nombreArchivo Cadena con el nombre del archivo
  * @param pagina Indica el numero de la pagina
  * @param totalRegistros Indica el total de registros
  * @param registrosPorPagina Indica cuantos registros se muestran por pagina
  * @param totalPaginas Indica el total de paginas
  * @param req Request de la pagina
  * @param res Response de la pagina
  * @throws ServletException Excepcion en caso de que ocurra algun error
  * @throws IOException Excepcion en caso de que ocurra algun error
  */
 public void generaTablaConsultarRegistrosCanceladosRrechazados(String tramaFiltro,String nombreArchivo,int pagina,int totalRegistros,int registrosPorPagina,int totalPaginas,HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	  EIGlobal.mensajePorTrace( "ConsultaUsuarios generando la tabla de consultar Cancelados y Rechazados", EIGlobal.NivelLog.INFO);

	  String strRegistros="";

	  HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");
	  StringBuffer tablaRegistros=new StringBuffer("");
	  StringBuffer botones=new StringBuffer("");

	  //Fecha Aplicacion;Linea Captura;ImporteTotal;Referencia;Contrato;Usuario;Cuenta Cargo;TipoLC
  	  String[] titulos={"",
	                    "Fecha de Aplicaci&oacute;n",
						"L&iacute;nea de Captura",
						"Importe Total",
						"Cuenta Cargo",
						"Referencia",
						"Usuario Enlace",
						"Tipo LC"};

      int[] datos={7,0,0,1,2,6,3,5,7};
	  int[] align={1,0,1,1,1,1,1,1};
	  int[] values={1,1,1};

	  EI_Tipo regSel=new EI_Tipo();

	  EIGlobal.mensajePorTrace( "ConsultaUsuarios Preparando los datos.", EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("registrosPorPagina"+registrosPorPagina, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("totalPaginas"+totalPaginas, EIGlobal.NivelLog.INFO);

	  strRegistros=seleccionaRegistrosDeArchivo(nombreArchivo,pagina,registrosPorPagina,totalRegistros);
	  if(strRegistros.trim().equals("ERROR"))
		  despliegaPaginaErrorURL("El archivo de <font color=blue>datos</font> no se pudo generar <b><i>correctamente</i></b>, intente en otro momento.","Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","","6",req,res);
	  else
	   {

		  EIGlobal.mensajePorTrace( "ConsultaUsuarios Se leyo el archivo.", EIGlobal.NivelLog.INFO);
		  regSel.iniciaObjeto(';','@',strRegistros);
	      EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
		  EnlaceGlobal.formateaImporte(regSel, 2);
		  tablaRegistros.append(regSel.generaTabla(titulos,datos,values,align));
		  EIGlobal.mensajePorTrace( "ConsultaUsuarios Objetos creados", EIGlobal.NivelLog.INFO);

		  if(totalRegistros>registrosPorPagina)
		   {
			  EIGlobal.mensajePorTrace("ConsultaUsuarios Estableciendo links para las paginas", EIGlobal.NivelLog.INFO);
			  botones.append("\n <br>");
			  botones.append("\n <table border=0 align=center>");
			  botones.append("\n  <tr>");
			  botones.append("\n   <td align=center class='texfootpagneg'>");
			  EIGlobal.mensajePorTrace("ConsultaUsuarios Pagina actual = "+pagina, EIGlobal.NivelLog.INFO);
			  if(pagina>0)
				 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
			  if(totalPaginas>=2)
			   {
				 for(int i=1;i<=totalPaginas;i++)
				  if(pagina+1==i) //si se encuentra en la p�gina actual
				   {
					 botones.append("&nbsp;");
					 botones.append(Integer.toString(i));
					 botones.append("&nbsp;");
				   }
				  else
				   { //creaci�n de las demas p�ginas
					 botones.append("&nbsp;<a href='javascript:BotonPagina(");
					 botones.append(Integer.toString(i));
					 botones.append(");' class='texfootpaggri'>");
					 botones.append(Integer.toString(i));
					 botones.append("</a>&nbsp;");
				   }
			   }
			  if(totalRegistros>((pagina+1)*registrosPorPagina))
				 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");

			  botones.append("\n   </td>");
			  botones.append("\n  </tr>");
			  botones.append("\n </table>");
		   }

		  req.setAttribute("tramaFiltro",tramaFiltro);
		  req.setAttribute("nombreArchivo",nombreArchivo);
		  req.setAttribute("totalRegistros",Integer.toString(totalRegistros));
		  req.setAttribute("registrosPorPagina",Integer.toString(registrosPorPagina));
		  req.setAttribute("totalPaginas",Integer.toString(totalPaginas));
		  req.setAttribute("tablaRegistros",tablaRegistros.toString());
		  req.setAttribute("pagina",Integer.toString(pagina+1));
		  req.setAttribute("Botones",botones.toString());
		  req.setAttribute("newMenu", session.getFuncionesDeMenu());
          req.setAttribute("MenuPrincipal", session.getStrMenu());
		  req.setAttribute("Encabezado", CreaEncabezado("Consulta y Cancelaci&oacute;n de Pagos SAR ","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","s35007h",req) );

		  req.setAttribute("Pagina","1");
		  req.setAttribute("Modulo","8");

		  EIGlobal.mensajePorTrace( "ConsultaUsuarios Se presenta pantalla de importacion", EIGlobal.NivelLog.INFO);

		  evalTemplate ("/jsp/ResultadoSARCan.jsp", req, res );
	   }
   }

 /**
  * Regresa el total de paginas
  * @param total Indica el total de registros
  * @param regPagina Indica el total de registros por pagina
  * @return Regresa el total de paginas
  */
 int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }

	  EIGlobal.mensajePorTrace("ConsultaUsuarios Total de paginas: "+ totPag, EIGlobal.NivelLog.INFO);
      return totPag;
    }

  /**
   * Metodo que selecciona los registros del archivo
   * @param Archivo Nombre del archivo
   * @param pagina Indica el numero de pagina
   * @param regPorPagina Registros por pagina
   * @param totalReg Total de registros
   * @throws IOException En caso de que haya algun error
   * @return Regresa una cadena
   */
  public String seleccionaRegistrosDeArchivo(String Archivo,int pagina,int regPorPagina,int totalReg) throws IOException
	{
	  String linea="";
	  StringBuffer result=new StringBuffer("");

	  int inicio=0;
	  int fin=0;

	  inicio=(pagina*regPorPagina);
	  fin=inicio+regPorPagina;
	  if(fin>totalReg)
		fin=totalReg;

  	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));

		  for(int i=0;i<fin;i++)
		   {
			 linea=arc.readLine();
			 if(i>=inicio)
				result.append(linea+"@");
		   }
		  arc.close();
 		}catch(IOException e)
		 {
			EIGlobal.mensajePorTrace("ConsultaUsuarios Error al tratar de leer: "+ Archivo, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ConsultaUsuarios Err: "+e.toString(), EIGlobal.NivelLog.INFO);
			result = new StringBuffer("ERROR");
		 }

	  return result.toString();
	}

  /**
   * Metodo que genera un archivo para exportar
   * @param request Request de la pagina
   * @param response Response de la pagina
   * @throws ServletException Excepcion en caso de que ocurra algun error
   * @throws IOException Excepcion en caso de que ocurra algun error
   */
  protected void generaArchivoParaExportar(HttpServletRequest request,HttpServletResponse response)
		throws ServletException, java.io.IOException
	{
	    String nombreDelArchivo=(String)request.getParameter("nombreArchivo");
		String app="text/richtext";

		descargaArchivo(nombreDelArchivo,app,request,response);
	}

  /**
   * Metodo que descarga el archivo
   * @param nombreDelArchivo Nombre del archivo
   * @param app Nombre de la aplicacion
   * @param request Request de la pagina
   * @param response Response de la pagina
   * @throws ServletException Excepcion en caso de que ocurra algun error
   * @throws IOException Excepcion en caso de que ocurra algun error
   */
  protected void descargaArchivo(String nombreDelArchivo,String app,HttpServletRequest request,HttpServletResponse response)
		throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaUsuarios Peticion de descarga de archivo ...", EIGlobal.NivelLog.INFO);

		if(app.trim().equals(""))
			app="application/x-msdownload";

		String nombreSinPath=nombreDelArchivo;
		if(nombreDelArchivo.lastIndexOf("/")>=0)
		  nombreSinPath=nombreDelArchivo.substring(nombreDelArchivo.lastIndexOf("/")+1);

		EIGlobal.mensajePorTrace( "ConsultaUsuarios Archivo: " + nombreDelArchivo, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ConsultaUsuarios Archivo sin Path: " + nombreSinPath, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ConsultaUsuarios Tipo de archivo: " + app, EIGlobal.NivelLog.INFO);

		response.setHeader("Content-disposition","attachment; filename=" + nombreSinPath );
		response.setContentType(app);

		BufferedInputStream  entrada = null;
		BufferedOutputStream salida = null;
		InputStream arc= new FileInputStream (nombreDelArchivo);

		ServletOutputStream out=response.getOutputStream();
		entrada = new BufferedInputStream(arc);
		salida = new BufferedOutputStream(out);
		byte[] buff = new byte[1024];
		int cantidad=0;

		while( (cantidad = entrada.read(buff, 0, buff.length )) !=-1)
			salida.write(buff,0,cantidad);

		salida.close();
		entrada.close();

		EIGlobal.mensajePorTrace( "ConsultaUsuarios Regresando de la descarga", EIGlobal.NivelLog.INFO);
	}

  /**
   * Metodo que cancela el pago
   * @param req Request de la pagina
   * @param res Response de la pagina
   * @throws ServletException Excepcion en caso de que ocurra algun error
   * @throws IOException Excepcion en caso de que ocurra algun error
   */
  public void cancelaPago(HttpServletRequest req,HttpServletResponse res)
		throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaUsuarios Entrando a la cancelacion de pagos", EIGlobal.NivelLog.INFO);

		String tramaFiltro=(String)req.getParameter("tramaFiltro");
		String nombreArchivo=(String)req.getParameter("nombreArchivo");
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		boolean ultimoRegistro=false;
        int totalRegistros=Integer.parseInt((String)req.getParameter("totalRegistros"));
	    int registrosPorPagina=Integer.parseInt((String)req.getParameter("registrosPorPagina"));
	    int totalPaginas=Integer.parseInt((String)req.getParameter("totalPaginas"));
	    int pagina=Integer.parseInt((String)req.getParameter("pagina"));
	    String valores=(String) req.getParameter("radTabla");
		String tramaEnviada ="";
		//radTabla= SECAPO|Contrato Enlace|Referencia|Linea Captura|Importe Total|Cuenta Cargo|Usuario

		EI_Tipo arrValores=new EI_Tipo();
		arrValores.iniciaObjeto(valores);
		arrValores.camposTabla[0][4]= reFormateaImporte(arrValores.camposTabla[0][4]);

		//Layout de Trama:
		//medioEntrega|usuario|tipoOperacion|contrato|usuario|clavePerfil|..@..@
		//AplicacionOrigen@ContratoEnlace@Referencia@LineaCaptura@Importe@CuentaCargo@Usuario@
		String CuentaCargo="";

		//transforma la cuenta clabe en cuenta normal
		if (arrValores.camposTabla[0][5].trim().length()>11)
			CuentaCargo=  arrValores.camposTabla[0][5].trim().substring(6,17);
		else
			CuentaCargo= arrValores.camposTabla[0][5].trim();

		tramaEnviada="1EWEB|"+session.getUserID8()+"|SARC|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+
			session.getUserProfile()+"|E@"+arrValores.camposTabla[0][1].trim()+"@"+
			arrValores.camposTabla[0][2].trim()+"@"+arrValores.camposTabla[0][3].trim()+"@"+
			arrValores.camposTabla[0][4].trim()+"@"+CuentaCargo+"@"+
			arrValores.camposTabla[0][6].trim()+"@";

		EIGlobal.mensajePorTrace("ConsultaUusuarios - realizaConsulta(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable htResult=null;

		try{
			htResult=tuxGlobal.web_red(tramaEnviada);
		}
		catch(java.rmi.RemoteException re){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		} catch(Exception e) {}

		//buffer
		//OK  @referencia@code error@descripcion@
	    String CodError = ( String ) htResult.get( "BUFFER" );
		EIGlobal.mensajePorTrace("ConsultaUusuarios - realizaConsulta(): CodError contiene"+CodError, EIGlobal.NivelLog.DEBUG);

		if (CodError.substring(0,2).trim().equals("OK"))
		{
			String[] arrCodError=null;
			arrCodError=desentrama(CodError,'@');
			int NumeroError = Integer.parseInt(arrCodError[2].substring(4,8));
			if (NumeroError==0)
			{
		        EIGlobal.mensajePorTrace("ConsultaUsuarios La cancelacion fue exitosa (Registros).", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Total de registros = " + totalRegistros, EIGlobal.NivelLog.INFO);
				req.setAttribute("mensajeCancelacion","cuadroDialogo('Se cancelo el registro con la referencia: "+arrValores.camposTabla[0][2]+"',4);");
				if (totalRegistros==1)
					ultimoRegistro=true;
			}
			else
			{
				EIGlobal.mensajePorTrace("ConsultaUsuarios error en el servicio de cancelacion", EIGlobal.NivelLog.INFO);
				req.setAttribute("mensajeCancelacion","cuadroDialogo('&lt;<font color=red>Error</font>&gt;: "+arrCodError[3].replace('<',' ').replace('>',' ')+"',4);");
			}
			consultaPagos("A",pagina-1,tramaFiltro,ultimoRegistro,req,res);
		}
		else
			despliegaPaginaErrorURL("<font color=red>Error </font><br> "+CodError.substring(16,CodError.length()).trim(),"Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos" ,"","6", req, res);
	}

  /**
   * Metodo que consulta los pagos
   * @param estatusArc Estatus del archivo
   * @param pagina Indica el numero de pagina actual
   * @param tramaFiltro Contiene los filtros
   * @param ultimoRegistro Indica si es el ultimo registro
   * @param req Request de la pagina
   * @param res Response de la pagina
   * @throws ServletException Excepcion en caso de que ocurra algun error
   * @throws IOException Excepcion en caso de que ocurra algun error
   */
  protected void consultaPagos(String estatusArc,int pagina,String tramaFiltro,boolean ultimoRegistro,HttpServletRequest req,HttpServletResponse res)
		throws ServletException, java.io.IOException
	 {
		//TramaFiltro= FechaInicial@FechaFinal@ImporteInicial@ImporteFinal@CuentaCargo@Referencia@Status@RFC@
		String tramaEnviada="";
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EI_Tipo Filtro=new EI_Tipo();
        Filtro.iniciaObjeto(tramaFiltro);

		//Layout de Trama
		//medioEntrega|usuario|tipoOperacion|contrato|usuario|clavePerfil|..@..@
		//AplicacionOrigen@NombreArchivo@ContratoEnlace@FechaInicial@FechaFinal@ImporteInicial@ImporteFinal@CuentaCargo@Referencia@Status@RFC@

		EIGlobal.mensajePorTrace("ConsultaUusuarios - consultaPagos(): TramaFiltro contiene: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
		tramaEnviada="2EWEB|"+session.getUserID8()+"|SARB|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+
		    session.getUserProfile()+"|E@"+session.getUserID8()+"@"+session.getContractNumber()+"@"+
		    formateaFecha(Filtro.camposTabla[0][0])+"@"+formateaFecha(Filtro.camposTabla[0][1])+"@"+Filtro.camposTabla[0][2]+"@"+
		    Filtro.camposTabla[0][3]+"@"+Filtro.camposTabla[0][4]+"@"+Filtro.camposTabla[0][5]+"@"+
			Filtro.camposTabla[0][6]+"@"+Filtro.camposTabla[0][7]+"@"+Filtro.camposTabla[0][8]+"@";

		EIGlobal.mensajePorTrace("ConsultaUusuarios - consultaPagos(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable htResult=null;

		try{
			htResult=tuxGlobal.web_red(tramaEnviada);
		}
		catch(java.rmi.RemoteException re){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		} catch(Exception e) {}

		//OK  @referencia@code error@descripcion@numero registros@importe total@
	    //String CodError = "OK  @SAR_0000@Consulta Exitosa@1@500";
		String CodError = ( String ) htResult.get( "BUFFER" );

		EIGlobal.mensajePorTrace("ConsultaUusuarios - realizaConsulta(): CodError contiene"+CodError, EIGlobal.NivelLog.DEBUG);

		if (CodError.substring(0,2).trim().equals("OK"))
		{
			String[] arrCodError=null;
			arrCodError=desentrama(CodError,'@');
			int NumeroError = Integer.parseInt(arrCodError[2].substring(4,8));
			if (NumeroError==0)
			{
				int totalRegistros=0;
				int registrosPorPagina=0;
				int totalPaginas=0;
				String nombreArchivo="";

			    //Validaci�n para verificar si existe correctamente la variable MAX_REGISTROS
				if(Global.MAX_REGISTROS>0)
				{
					registrosPorPagina=Global.MAX_REGISTROS;
					EIGlobal.mensajePorTrace( "ConsultaUsuarios Registros por pagina " + registrosPorPagina, EIGlobal.NivelLog.INFO );
				}
				else
			    {
					EIGlobal.mensajePorTrace( "ConsultaUsuarios Error al convertir MAX_REGISTROS", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "ConsultaUsuarios Se pone por default = 10", EIGlobal.NivelLog.INFO);
				    registrosPorPagina=10;
				}

				EIGlobal.mensajePorTrace("ConsultaUsuarios La consulta fue exitosa (Registros).", EIGlobal.NivelLog.INFO);
				nombreArchivo = session.getUserID8()+".SAR";
				EIGlobal.mensajePorTrace( "ConsultaUsuarios Archivo de registros: " + nombreArchivo, EIGlobal.NivelLog.INFO);

				ArchivoRemoto archR=new ArchivoRemoto();
				if(archR.copia(nombreArchivo))
				{
					nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivo;
	        		EIGlobal.mensajePorTrace( "ConsultaUsuarios Copia remota desde tuxedo se realizo correctamente. (Registros)", EIGlobal.NivelLog.INFO);

					totalRegistros= Integer.parseInt(arrCodError[4]);
					EIGlobal.mensajePorTrace( "totalRegistros" + totalRegistros, EIGlobal.NivelLog.INFO);

					if(totalRegistros>0)
					 {
					   totalPaginas=calculaPaginas(totalRegistros,registrosPorPagina);
					   EIGlobal.mensajePorTrace( "ConsultaUsuarios Generando la pantalla (Registros)", EIGlobal.NivelLog.INFO);

					   if(estatusArc.trim().equals("A"))
					     generaTablaConsultarRegistrosPagados(tramaFiltro,nombreArchivo,pagina,totalRegistros,registrosPorPagina,totalPaginas,req,res);
					   else
						 generaTablaConsultarRegistrosCanceladosRrechazados(tramaFiltro,nombreArchivo,pagina,totalRegistros,registrosPorPagina,totalPaginas,req,res);

					 }
					else if(totalRegistros<0)
					 {
						EIGlobal.mensajePorTrace( "ConsultaUsuarios No se pudo abrir el archivo ... (Registros)", EIGlobal.NivelLog.INFO);
						despliegaPaginaErrorURL("La <i><font color=blue>transacci&oacute;n</font></i> solicitada no se llevo a cabo correctamente.<br>Por favor intente mas tarde.","Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","","6",req,res);
						return;
					 }
					else if(totalRegistros==0)
					 {
						EIGlobal.mensajePorTrace( "ConsultaUsuarios Archivo encontrado pero sin registros ... (Registros)", EIGlobal.NivelLog.INFO);
						despliegaPaginaErrorURL("No se encontraron <font color=green>registros</font> para el tipo de b&uacute;squeda seleccionado.","Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","","6",req,res);
						return;
					 }

//					TODO: BIT CU 4391. A5
					if (Global.USAR_BITACORAS.trim().equals("ON")){
						try{
						BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_CONSULTA_PAGO_SAR);
						bt.setContrato(session.getContractNumber());
						bt.setServTransTux("SARB");
						if(CodError!=null){
			    			if(CodError.substring(0,2).equals("OK")){
			    				bt.setIdErr("SARB0000");
			    			}else if(CodError.length()>8){
				    			bt.setIdErr(CodError.substring(0,8));
				    		}else{
				    			bt.setIdErr(CodError.trim());
				    		}
			    		}
						BitaHandler.getInstance().insertBitaTransac(bt);
						}catch (SQLException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}catch(Exception e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}
					}
				}
				else
				{
					EIGlobal.mensajePorTrace( "ConsultaUsuarios No se pudo realizar la copia remota del archivo de tuxedo (Registros)", EIGlobal.NivelLog.INFO);
					despliegaPaginaErrorURL("La <i><font color=green>transacci&oacute;n</font></i> solicitada no se llevo a cabo.<br>Por favor intente mas tarde.","Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","","6",req,res);
					return;
				}
			}
			else
			{
				if (ultimoRegistro==true)
					despliegaPaginaErrorURL("Se <b>cancel&oacute;</b> el registro exitosamente.","Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos" ,"","6", req, res);
				else
				despliegaPaginaErrorURL("<font color=red>Error </font><br> "+arrCodError[3].replace('<',' ').replace('>',' '),"Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos" ,"","6", req, res);
			}
		}
		else
			despliegaPaginaErrorURL("<font color=red>Error </font><br> "+CodError.substring(16,CodError.length()).trim(),"Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","","6",req,res);

	}

  /**
   * Metodo que genera el comprobante
   * @param tipo Indica el tipo de comprobante
   * @param req Request de la pagina
   * @param res Response de la pagina
   * @throws ServletException Excepcion en caso de que ocurra algun error
   * @throws IOException Excepcion en caso de que ocurra algun error
   */
  protected void generaComprobante(String tipo,HttpServletRequest req,HttpServletResponse res)
		throws ServletException, java.io.IOException
	{

		boolean error=false;
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String valores=(String) req.getParameter("radTabla");
		String IMAGE_PATH="/proarchivapp/WebSphere8/enlace/estatico/gifs/EnlaceMig/";
		String nombreDelArchivo=IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8()+".pdf";
		String app="application/pdf";
		String TipoLineaCaptura="";
		String DescTLC="";

		EIGlobal.mensajePorTrace( "ConsultaUsuarios Empezando el metodo generaComprobante() ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ConsultaUsuarios Valores para el documento " + valores, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ConsultaUsuarios Tipo de documento " + tipo, EIGlobal.NivelLog.INFO);

		//el siguiente if determina el formato que mostrar� DespliegaP�ginaError
		if (tipo.equals("HTML"))
			modulo="ESP";//error especial
		else
			modulo="6";

		//Layout de Trama:
		//medio_entrega|empleado|tipo_operacion|contrato|empleado|clave_perfil|contrato
		String tramaEnviada ="";
		tramaEnviada="1EWEB|"+session.getUserID8()+"|SARE|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+ valores.substring(0,valores.indexOf("|"));

		EIGlobal.mensajePorTrace("ConsultaUusuarios - realizaConsulta(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable htResult=null;

		try{
			htResult=tuxGlobal.web_red(tramaEnviada);
		}
		catch(java.rmi.RemoteException re){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		} catch(Exception e) {}

		//ejemplo de buffer correcto
		//OK        @     379@SAR_0000@INT NAL DE LAS PERSONAS ADULTAS MAYORES @INP790822PM7@PETEN NO. 419 BENITO JUAREZ                      @
	    String CodError = ( String ) htResult.get( "BUFFER" );
		//String CodError = "OK        @     379@SAR_0000@  50@";
		EIGlobal.mensajePorTrace("ConsultaUusuarios - realizaConsulta(): CodError contiene"+CodError, EIGlobal.NivelLog.DEBUG);

		if (CodError.substring(0,2).trim().equals("OK"))
		{
			String[] arrCodError=null;
			arrCodError=desentrama(CodError,'@');

			//int NumeroError = Integer.parseInt(arrCodError.camposTabla[0][2].substring(4,8));
			int NumeroError = Integer.parseInt(arrCodError[2].substring(4,8));
			if (NumeroError==0)
			{
				//4-FechaPago 5-Hora 6-LineaCaptura 7-Bimestre 8-TipoPago 9-ImporteAhorro 10-ImporteVivienda
				//11-ImporteTotal 12-Referencia 13-Dependencia 14-RFC 15-CentroPago 16-Domicilio 17-CuentaCargo
				//18-Contrato 19-Usuario 20-nombre usuario 21-descripcion cuenta 22-Descripci�n Contrato

				//Ejemplo de salida
				EIGlobal.mensajePorTrace( "ConsultaUsuarios Servicio de Detalle Exitoso generando los datos ", EIGlobal.NivelLog.INFO);

				String fechaPago=arrCodError[4].trim();
				String Hora=arrCodError[5].replace('.',':').trim();
				String lineaCaptura=arrCodError[6].trim();
				String bimestreDePago=arrCodError[7].trim();
				String tipoDePago=arrCodError[8].trim();
				String importeRetiro=formateaImporte(arrCodError[9].trim());
				String importeVivienda=formateaImporte(arrCodError[10].trim());
				String totalPagado=formateaImporte(arrCodError[11].trim());
				String numOperacion=arrCodError[12].trim();
				if(numOperacion.length()>8)
					numOperacion=numOperacion.substring(numOperacion.length()-8,numOperacion.length());
				if (lineaCaptura.trim().length() == 65)
					TipoLineaCaptura = lineaCaptura.substring(63,65);
				else
					TipoLineaCaptura = "00";

				String dependencia=arrCodError[13].trim();
				String RFCDependencia=arrCodError[14].trim();
				String centroDePago=arrCodError[15].trim();
				String domicilio=arrCodError[16].trim();

				String cuentaDeCargo=arrCodError[17].trim()+" "+arrCodError[21].trim();
				String contratoEnlace=arrCodError[18].trim()+" "+arrCodError[22].trim();
				String usuarioEnlace=arrCodError[19].trim()+" "+arrCodError[20].trim();

				if(tipo.trim().equals("HTML"))
				 {

					req.setAttribute("Fecha", fechaPago);
					// Lineas agregadas para mandar a imprimir en el  el encabezado del tipo de linea de captura
					req.setAttribute("TipoLineaCaptura",TipoLineaCaptura);
					switch(Integer.parseInt(TipoLineaCaptura))
					{
						case 85:	req.setAttribute("NombreTipoLineaCaptura","RETIRO Y VIVIENDA");
									break;

						case 86:	req.setAttribute("NombreTipoLineaCaptura","PAGOS EXTEMPOR&Aacute;NEOS");
									break;

						case 87:	req.setAttribute("NombreTipoLineaCaptura","AHORRO VOLUNTARIO");
									break;

						case 88:	req.setAttribute("NombreTipoLineaCaptura","CR�DITO DE VIVIENDA");
									break;

						case 89:	req.setAttribute("NombreTipoLineaCaptura","AHORRO VOLUNTARIO CETES");
									break;

						default:
									req.setAttribute("NombreTipoLineaCaptura","");

					}

					req.setAttribute("LineaCaptura", lineaCaptura);
					req.setAttribute("Bimestre", bimestreDePago);
					req.setAttribute("TipoPago", tipoDePago);
					req.setAttribute("Hora", Hora);
					req.setAttribute("Retiro", importeRetiro);
					req.setAttribute("Vivienda", importeVivienda);
					req.setAttribute("Total", totalPagado);
					req.setAttribute("Referencia", numOperacion);
					req.setAttribute("Dependencia", dependencia);
					req.setAttribute("RFC", RFCDependencia);
					req.setAttribute("CentroPago", centroDePago);
					req.setAttribute("Domicilio", domicilio);
					//datos de enlace
					req.setAttribute("NumContrato", arrCodError[18].trim());
					req.setAttribute("NombreUsuario", arrCodError[20].trim());
					req.setAttribute("NombreContrato", arrCodError[22].trim());
					req.setAttribute("IDUsuario", arrCodError[19].trim());
					req.setAttribute("CuentaCargo", arrCodError[17].trim());
					req.setAttribute("DescripCuenta", arrCodError[21].trim());
					req.setAttribute("Operacion", "C");//Describe si es consulta(C) o fu� alta de l�nea(A)
//					TODO: BIT CU 4391. D2
					if (Global.USAR_BITACORAS.trim().equals("ON")){
						try{
						BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_CONS_REIMP_PAGO_SAR);
						bt.setContrato(session.getContractNumber());
						bt.setNombreArchivo(nombreDelArchivo);
						if(arrCodError[11]!=null && arrCodError[11].equals(""))
							bt.setImporte(Double.parseDouble(arrCodError[11].trim()));
						bt.setCctaOrig(arrCodError[17].trim());
						bt.setServTransTux("SARE");
						if(CodError!=null){
			    			if(CodError.substring(0,2).equals("OK")){
			    				bt.setIdErr("SARE0000");
			    			}else if(CodError.length()>8){
				    			bt.setIdErr(CodError.substring(0,8));
				    		}else{
				    			bt.setIdErr(CodError.trim());
				    		}
			    		}
						BitaHandler.getInstance().insertBitaTransac(bt);
						}catch (SQLException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}catch(Exception e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}
					}
					evalTemplate("/jsp/ComprobanteSAR.jsp", req, res );

				 }
				else
					// SAR-APPLET-NUEVO Modificacion de la estructura del PDF para incluir la linea de captura
				 if(tipo.trim().equals("PDF"))
				  {
					EIGlobal.mensajePorTrace( "ConsultaUsuarios Generando el comprobante PDF y enviando parametros ", EIGlobal.NivelLog.INFO);

					Document document = new Document(PageSize.LETTER);
					try
					  {
						Font fontTit=new Font(Font.HELVETICA, 11,Font.BOLD,new Color(0,0,0));
						Font fontDat=new Font(Font.COURIER, 10, Font.NORMAL, new Color(0, 0, 0));
						PdfWriter writer=PdfWriter.getInstance(document, new FileOutputStream(nombreDelArchivo));
						document.open();
						Image png1 = Image.getInstance(IMAGE_PATH + "logo_sant.gif");
						Image jpg2 = Image.getInstance(IMAGE_PATH + "call_center.gif");
						jpg2.setAlignment(Image.MIDDLE);

						document.addTitle("Comprobante de Pago");
						document.addSubject("Linea de Captura via Internet");
						document.addKeywords("Banco Santander Serfin.\nDesarrollador: Alejandro Rada Vazquez.");
						document.addCreator("Enlace Internet");
						document.addAuthor("Banco Santander Serfin");
						document.addHeader("Expires", "0");

						int headerwidths[] = {5,26,69};
						Table aTable = new Table (3);
						aTable.setPadding(4);
						aTable.setSpacing(0);
						aTable.setBorder(Rectangle.NO_BORDER);
						aTable.setWidths(headerwidths);
						aTable.setWidth(100);
						aTable.setDefaultCellBorderWidth(0);

						Phrase t1 = new Phrase(18, new Chunk("\nS.A.R. Comprobante del pago por l�nea de Captura v�a Internet\n",fontTit));
						Phrase t2 = new Phrase(18, new Chunk("Datos del Pago",fontTit));
						Phrase t3 = new Phrase(18, new Chunk("Datos de la Dependencia",fontTit));
						// Bloque para poner la descripcion de la linea de captura
						switch(Integer.parseInt(TipoLineaCaptura))
						{
							case 85:	DescTLC="RETIRO Y VIVIENDA";
										break;

							case 86:	DescTLC="PAGOS EXTEMPOR�NEOS";
										break;

							case 87:	DescTLC="AHORRO VOLUNTARIO";
										break;

							case 88:	DescTLC="CR�DITO DE VIVIENDA";
										break;

							case 89:	DescTLC="AHORRO VOLUNTARIO CETES";
										break;

						}

						Paragraph p1   = new Paragraph("\n",fontDat);
								  p1.add(new Paragraph("Fecha de Pago:"));
								  p1.add(new Paragraph("Linea de Captura:"));
								  p1.add(new Paragraph(""));
								  p1.add(new Paragraph("Bimestre de Pago:"));
								  p1.add(new Paragraph("Tipo de Pago:"));
								  p1.add(new Paragraph("Importe Retiro:"));
								  p1.add(new Paragraph("Importe Vivienda:"));
								  p1.add(new Paragraph("Total Pagado:"));
								  p1.add(new Paragraph("Num. Operaci�n:"));
								  p1.add(new Paragraph("Tipo de L. C.:"));

						  Paragraph r1   = new Paragraph("\n",fontDat);
								  r1.add (new Paragraph(fechaPago+ "          Hora:"+Hora));
								  r1.add(new Paragraph(lineaCaptura));
								  r1.add(new Paragraph(bimestreDePago));
								  r1.add(new Paragraph(tipoDePago));
								  r1.add(new Paragraph(importeRetiro));
								  r1.add(new Paragraph(importeVivienda));
								  r1.add(new Paragraph(totalPagado));
								  r1.add(new Paragraph(numOperacion));
								  r1.add(new Paragraph(TipoLineaCaptura+ "          "+DescTLC));

						Paragraph p2   = new Paragraph("Dependencia:\n",fontDat);
								  p2.add(new Paragraph("RFC Dependencia:"));
								  p2.add(new Paragraph("Centro de Pago:"));
								  p2.add(new Paragraph("Domicilio:"));
								  p2.add(new Paragraph("Cuenta de Cargo:"));
								  p2.add(new Paragraph("Contrato Enlace:"));
								  p2.add(new Paragraph("Usuario Enlace:\n\n"));

						Paragraph r2   = new Paragraph(dependencia+"\n",fontDat);
								  r2.add(new Paragraph(RFCDependencia));
								  r2.add(new Paragraph(centroDePago));
								  r2.add(new Paragraph(domicilio));
								  r2.add(new Paragraph(cuentaDeCargo));
								  r2.add(new Paragraph(contratoEnlace));
								  r2.add(new Paragraph(usuarioEnlace+"\n\n"));

						aTable.setDefaultColspan(3);
						aTable.addCell(t2);
						aTable.setDefaultColspan(1);

						aTable.addCell(" ");
						aTable.addCell(p1);
						aTable.addCell(r1);

						aTable.setDefaultColspan(3);
						aTable.addCell(t3);
						aTable.setDefaultColspan(1);

						aTable.addCell(" ");
						aTable.addCell(p2);
						aTable.addCell(r2);

						document.add(png1);
						document.add(t1);
						document.add(aTable);
						document.add(jpg2);

					}catch(DocumentException de)
					 {
						EIGlobal.mensajePorTrace( "ConsultaUsuarios Errores al generarlo ...", EIGlobal.NivelLog.INFO);
						System.err.println(de.getMessage());
						error=true;
					 }
					catch(IOException ioe)
					 {
						EIGlobal.mensajePorTrace( "ConsultaUsuarios Errores de entrada/salida ...", EIGlobal.NivelLog.INFO);
						System.err.println(ioe.getMessage());
						error=true;
					 }
					document.close();
					if(!error)
					  descargaArchivo(nombreDelArchivo,app,req,res);
					else
					  despliegaPaginaErrorURL("Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento.<br>Por favor intente mas tarde.","Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","","6",req,res);
				  }
			}
			else
			{
//				TODO: BIT CU 4391. D2
				if (Global.USAR_BITACORAS.trim().equals("ON")){
					try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_CONS_REIMP_PAGO_SAR);
					bt.setContrato(session.getContractNumber());
					bt.setNombreArchivo(nombreDelArchivo);
					bt.setServTransTux("SARE");
					if(CodError!=null){
		    			if(CodError.substring(0,2).equals("OK")){
		    				bt.setIdErr("SARE0000");
		    			}else if(CodError.length()>8){
			    			bt.setIdErr(CodError.substring(0,8));
			    		}else{
			    			bt.setIdErr(CodError.trim());
			    		}
		    		}
					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

					}catch(Exception e){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

					}
				}

				despliegaPaginaErrorURL("<font color=red>Error </font><br> "+arrCodError[3].replace('<',' ').replace('>',' '),"Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos" ,"",modulo, req, res);
			}
		}
		else{
//			TODO: BIT CU 4391. D2
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_SAR_CONS_CANCEL_CONS_REIMP_PAGO_SAR);
				bt.setContrato(session.getContractNumber());
				bt.setNombreArchivo(nombreDelArchivo);
				bt.setServTransTux("SARE");
				if(CodError!=null){
	    			if(CodError.substring(0,2).equals("OK")){
	    				bt.setIdErr("SARE0000");
	    			}else if(CodError.length()>8){
		    			bt.setIdErr(CodError.substring(0,8));
		    		}else{
		    			bt.setIdErr(CodError.trim());
		    		}
	    		}
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				}catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

				}
			}
			despliegaPaginaErrorURL("<font color=red>Error </font><br> "+CodError.substring(16,CodError.length()).trim(),"Consulta y Cancelaci&oacute;n de Pagos","Servicios &gt; SAR &gt; Consulta y Cancelaci&oacute;n de Pagos","",modulo,req,res);
		}
	}

  /**
   * Metodo que formatea la fecha
   * @param fec Fecha a formatear
   * @return Regresa la fecha formateada
   */
  public String formateaFecha(String fec)
  {
	    if(fec==null || fec.trim().equals(""))
			return "";
		String fecha=fec;
		EIGlobal.mensajePorTrace("EIGlobal Longitud de fecha:" +fec.trim().length(), EIGlobal.NivelLog.INFO);
		if(fec.trim().length()==10)
			fecha=fec.substring(6) + "-" + fec.substring(3,5) + "-" + fec.substring(0, 2);
		EIGlobal.mensajePorTrace("EIGlobal La nueva fecha es: " + fecha +" antes " + fec, EIGlobal.NivelLog.INFO);
		return fecha;
   }

/**
 * Metodo que formatea el importe
 * @param importe Cadena con el importe
 * @return Regresa el importe formateado
 */
public String formateaImporte(String importe)
  {
	if (importe==null || importe.trim().equals("") )
		   importe="00";
	//calculo de las cantidades
	double calculaCent = (double) Double.parseDouble(importe)/100;//calcula centavos

	//formato a las cantidades $000,000.00
	NumberFormat formato = NumberFormat.getCurrencyInstance(Locale.US);
	String formatoImp= formato.format(calculaCent);
	if ((formatoImp.length() >= 3) && (formatoImp.substring(0, 3).equals("MXN"))) {
		formatoImp = "$ " + formatoImp.substring(3, formatoImp.length());
	}
	return formatoImp;
  }

  /**
   * Metodo que reformatea el importe
   * @param cantidad Cadena que contiene la cantidad
   * @return Regresa una cadena con el importe reformateado
   */
  public String reFormateaImporte( String cantidad )
	 {
	   String imp="";
	   if (cantidad==null || cantidad.trim().equals("") )
		   imp="0";
	   for(int i=0;i<cantidad.length();i++)
		 {
		   if(cantidad.charAt(i)!='$' && cantidad.charAt(i)!='.' && cantidad.charAt(i)!=',' && cantidad.charAt(i)!=' ')
			 imp+=cantidad.charAt(i);
		 }
	   return imp;
	 }

  	/**
  	 * Metodo que guarda los parametros en sesion
  	 * @param request Request de la pagina
  	 */
	public void guardaParametrosEnSession(HttpServletRequest request)
	{
            EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("\n\n\n jgal - Dentro de guardaParametrosEnSession  - plantilla\n\n\n" + request.getServletPath(), EIGlobal.NivelLog.INFO);

            request.getSession().setAttribute("plantilla",request.getServletPath());

            HttpSession session = request.getSession(false);
            session.removeAttribute("bitacora");

            if(session != null) {
			Map tmp = new HashMap();

			String value;

			value = (String) request.getParameter ("valida");
			if(value != null && !value.equals(null)) {
				tmp.put("valida",request.getParameter ("valida"));
			}

			tmp.put("Modulo",request.getParameter ("Modulo") );
			tmp.put("LineCaptureValidate",request.getParameter ("LineCaptureValidate") );
			tmp.put("cuenta",request.getParameter ("cuenta") );
			tmp.put("refmancomunidad",request.getParameter ("refmancomunidad") );
			tmp.put("fechaHoy",request.getParameter ("fechaHoy") );
			tmp.put("FechaProg",request.getParameter ("FechaProg") );
			tmp.put("txtLineaCaptura",request.getParameter ("txtLineaCaptura") );
			tmp.put("txtBimestre",request.getParameter ("txtBimestre") );
			tmp.put("txtTipoPago",request.getParameter ("txtTipoPago") );
			tmp.put("txtRetiro",request.getParameter ("txtRetiro") );
			tmp.put("txtVivienda",request.getParameter ("txtVivienda") );
			tmp.put("txtTotal",request.getParameter ("txtTotal") );
			tmp.put("txtDependencia",request.getParameter ("txtDependencia") );
			tmp.put("txtRFC",request.getParameter ("txtRFC") );
			tmp.put("txtCentroPago",request.getParameter ("txtCentroPago") );
			tmp.put("txtDomicilio",request.getParameter ("txtDomicilio") );
			tmp.put("txtTipoLineaCap",request.getParameter ("txtTipoLineaCap") );
			tmp.put("txtTipoLinea",request.getParameter ("txtTipoLinea") );

			tmp.put("TramaImprimir",request.getParameter ("TramaImprimir") );

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
    }

	/**
	 * Metodo que valida las cuentas
	 * @param request Request de la pagina
	 * @return Regresa un boolean indicando si las cuentas son validas o no
	 */
	private boolean validaCuentas(HttpServletRequest request)
	{   BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		ValidaCuentaContrato valCtas = new ValidaCuentaContrato();
		boolean retorno=true;
		String[]cuenta = desentrama(request.getParameter("cuenta"),'|');

	try{
		EIGlobal.mensajePorTrace("SAR CUENTA["+0+"]->:"+cuenta[0], EIGlobal.NivelLog.DEBUG);
		String datoscta[] =null;

		datoscta= valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
				session.getUserID8(),
				session.getUserProfile(),
				cuenta[0],
				IEnlace.MSua_envio);

		if(datoscta==null){
			retorno=false;
			EIGlobal.mensajePorTrace("SAR - Entra a Break y rompe ciclo", EIGlobal.NivelLog.INFO);
		}
	}
    catch(Exception e) {
		EIGlobal.mensajePorTrace("TIFondeo -  ENTRO A EXCEPCION->:"+e.getMessage(), EIGlobal.NivelLog.INFO);
		e.printStackTrace();
    }

   	return retorno;
    }
}