/*Banco Santander Mexicano
* Clase	MtoNomina, Carga la	plantilla para el mantenimineto	del	archivo	de pagos
* @author Rodrigo Godo
* @version 1.1
* fecha	de creacion: Diciembre 2000	- Enero	20001
* responsable: Roberto Guadalupe Resendiz Altamirano
* descripcion: Crea	un objeto del tipo TemplateMapBasic, carga un calendario para exhibirse	dentro del TemplateMap,
				asi como un combo con la lista de cuentas con las cuales puede operar el usuario,
				valida facultades para saber  que tipo de operaciones pueden efectuarse sobre los archivos de pago de nomina
				(creacion, importacion, altas, bajas, cambios, borrado y recuperacion de archivos,
				asi como la transmision de los pagos que habran de efectuarse).
*/


package	mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.NominaConstantes.ES_NOMINA_PAGOS_IMP_ENVIO;
import static mx.altec.enlace.utilerias.NominaConstantes.ES_NOMINA_PAGOS_IMP_ENVIO_ENTRA;
import static mx.altec.enlace.utilerias.NominaConstantes.FOR_FECHA_DMA;
import static mx.altec.enlace.utilerias.NominaConstantes.PAG_TMPL_INICIO_PAGOS;
import static mx.altec.enlace.utilerias.NominaConstantes.PAG_TMPL_SRV_LIN;
import static mx.altec.enlace.utilerias.NominaConstantes.PAG_TMPL_SRV_TRAD;
import static mx.altec.enlace.utilerias.NominaConstantes.PARAM_FECHA_PAG;
import static mx.altec.enlace.utilerias.NominaConstantes.PARAM_HOR_PAG;
import static mx.altec.enlace.utilerias.NominaConstantes.PARAM_ORIGEN_PAG;
import static mx.altec.enlace.utilerias.NominaConstantes.PARAM_TIP_APLI_PAG;
import static mx.altec.enlace.utilerias.NominaConstantes.VAL_TIPO_SOLPDF_TR;
import static mx.altec.enlace.utilerias.NominaConstantes.VAR_REQ_BEAN_PAG;
import static mx.altec.enlace.utilerias.NominaConstantes.VAR_SESS_BEAN_PAG;
import static mx.altec.enlace.utilerias.NominaConstantes.VAR_SESS_CTA_CAT_PAG;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ArchivoNominaBean;
import mx.altec.enlace.beans.ArchivoNominaDetBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.NominaPagosBO;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FavoritosConstantes;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

@SuppressWarnings("serial")
public class InicioNominaLn extends BaseServlet {

	/**
     * RECARGA_SES_MULTIPLESX para recarga de sesione
     */
	static final String RECARGA_SES_MULTIPLESX = "Recarga_Ses_Multiples";
	/**
	 * INOMPAGLINE para facultades
	 */
	static final String INOMPAGLINE="INOMPAGLINE";
	/**
	 * INOMPAGONOM para facultades
	 */
	static final String INOMPAGONOM="INOMPAGONOM";
	/**
	 * INOMENVIPAG para facultades
	 */
	static final String INOMENVIPAG="INOMENVIPAG";
	/**
    * Constante para la literal "session"
    */
    private static final String SESSION = "session";
    /**
    * Constante para la literal "VALIDAR"
    */
    private static final String VALIDAR = "VALIDAR";
    /**
    * Constante para la literal "document.location='SinFacultades'"
    */
    private static final String DOCUMENT_LOCATION = "document.location='SinFacultades'";
    /**
    * Constante para la literal "plantillaElegida"
    */
    private static final String PLATILLAELEGIDA = "plantillaElegida";
    
	 /**
	 * String para tipo de cargo Individual / Global para nomina en linea
	 */
	private static final String TIPO_CARGO_IG_NOM_LIN = "TipoCargoIGNomLin";
    
	/**
	 * Mapa de tipoPago
	 */
	HashMap <String,String> tipoPago = null;
	/**
	 * nuevoMenu para el menu
	 */
	String nuevoMenu;
	/**
	 * Variable para dias inhabiles
	 */
	String strInhabiles	    = "";

	/**
    * myCalendarNomina para el calendario
	 */
    CalendarNomina myCalendarNomina;
	/** Instancias para pagosBO */
	private  NominaPagosBO pagosBO =  new NominaPagosBO();

	/**
	 * doGet
	 * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}
	/**
	 * doPost
	 * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}
	/**
	 * Metodo DefaultAction
	 * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	@SuppressWarnings({ "rawtypes", "static-access", "unused" })
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***InicioNominaLn.class &Entrando al metodo execute: &", EIGlobal.NivelLog.INFO);
		String modulo = "";
 		final boolean sesionvalida = SesionValida( request, response );
 		final HttpSession sess = request.getSession();

 		
 		ServicioTux tuxGlobal = new ServicioTux();
 		Hashtable hs = null;
 		Integer referencia = 0 ;
 		String codError = null;

 		/*FSW Vector Marzo 2015*/
 		String accion = "";
 		String subModulo = "";


 	    final BaseResource session = (BaseResource) sess.getAttribute(SESSION);


 	    if (! sesionvalida ) {
        	return;
        }
        myCalendarNomina= new CalendarNomina();
 		request.setAttribute("Fecha", ObtenFecha());
 		request.setAttribute("ContUser",ObtenContUser(request));
 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());
		/*************** Nomina Concepto de Archvo */
 		if(request.getSession().getAttribute("conceptoEnArchivo")!=null) {
			request.getSession().removeAttribute("conceptoEnArchivo");
		}
		/*************** Nomina Concepto de Archvo */
		EIGlobal.mensajePorTrace( "session.getFacultad INOMPAGLINE= " + session.getFacultad (INOMPAGLINE ), EIGlobal.NivelLog.INFO );
		EIGlobal.mensajePorTrace( "session.getFacultad INOMPAGONOM= " + session.getFacultad (INOMPAGONOM ), EIGlobal.NivelLog.INFO );
		EIGlobal.mensajePorTrace( "session.getFacultad INOMENVIPAG= " + session.getFacultad (INOMENVIPAG ), EIGlobal.NivelLog.INFO );



		if( true ) {
			String VarFechaHoy = "";
			final Calendar fechaFin = myCalendarNomina.calculaFechaFin();
			VarFechaHoy = myCalendarNomina.armaArregloFechas();
			final String strFechaFin =  Integer.valueOf(fechaFin.get(fechaFin.YEAR)) + "/" +  Integer.valueOf(fechaFin.get(fechaFin.MONTH)) + "/" +  Integer.valueOf(fechaFin.get(fechaFin.DAY_OF_MONTH));
			String fechaHoy = "";
			final GregorianCalendar cal = new GregorianCalendar();
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("es","mx"));
			fechaHoy = sdf.format(cal.getTime());
			request.setAttribute( "VarFechaHoy", VarFechaHoy);
			request.setAttribute("DiaHoy",fechaHoy);

 		    request.setAttribute( "ContenidoArchivo", "" );
 			String NumberOfRecords = "";
 			if(session.getNominaNumberOfRecords() != null) {
 				NumberOfRecords = session.getNominaNumberOfRecords();
 			}
 			request.setAttribute( "totRegs", NumberOfRecords);
 			estableceFacultades( request, response );
 			session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
			modulo = request.getParameter("Modulo");
			subModulo = request.getParameter("submodulo");
			accion = request.getParameter("accion");

			EIGlobal.mensajePorTrace( "valor de modulo****" + modulo  + " " + subModulo +  " " + accion, EIGlobal.NivelLog.INFO );



			if ( (modulo == null) || ( ("").equals(modulo))) {
				modulo = "0";
			}
		   if ( (subModulo == null) || ( ("").equals(subModulo))) {
			   	subModulo = "0";
		   }
			if("2".equals(subModulo)){
				modulo = "3";
			}
			if ( (accion == null) || ( ("").equals(accion))) {
				accion = "iniciar";
			}

			EIGlobal.mensajePorTrace( "valor de Modulo   ****" + modulo, EIGlobal.NivelLog.INFO );
			EIGlobal.mensajePorTrace( "valor de subModulo****" + modulo, EIGlobal.NivelLog.INFO );
			EIGlobal.mensajePorTrace( "valor de accion   ****" + accion, EIGlobal.NivelLog.INFO );



			if (("0").equals(modulo)) {
				//MSD Limpia variables de sesi�n usadas en dispersiones anteriores
				sess.removeAttribute(VALIDAR);
				sess.removeAttribute("HOR_SAT");
				sess.removeAttribute("HorarioNormal");
				sess.removeAttribute(VALIDAR);
				sess.removeAttribute("FOLIO");
				sess.removeAttribute("HR_P");
				sess.removeAttribute("archDuplicado");
				sess.removeAttribute("NOMIencFechApli");
				//MSD Limpia variables de sesi�n usadas en dispersiones anteriores
				/*	GETRONICS CP MEXICO, NAA.*/
				if(session.getFacultad (INOMPAGLINE) ) {
					//BIT CU 4141, El cliente entra al flujo)
					/*
		    		 * VSWF ARR -I
		    		 */
					if (("ON").equals(Global.USAR_BITACORAS.trim())){
						try {
							final BitaHelper bh = new BitaHelperImpl(request, session,sess);

							hs = tuxGlobal.sreferencia("901");
							referencia = (Integer) hs.get("REFERENCIA");

							bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.ES_NOMINALN_PAGOS_IMP_ENVIO_ENTRA);
							bt.setContrato(session.getContractNumber());
							bt.setReferencia(referencia);
							bt.setIdFlujo(BitaConstants.ES_NOMINALN_PAGOS_IMP_ENVIO);
							Date fch=new Date();
							bt.setFechaProgramada(fch);
							bt.setFechaAplicacion(fch);
							codError = hs.get("COD_ERROR").toString();

							if(null != session.getToken().getSerialNumber()) {
								bt.setIdToken(session.getToken().getSerialNumber());
							}
							bt.setIdErr(codError);
							BitaHandler.getInstance().insertBitaTransac(bt);
						}catch (SQLException e){
							EIGlobal.mensajePorTrace("***InicioNominaLn.class &Error de excepcion SQL"+e, EIGlobal.NivelLog.INFO);
						}catch(Exception e){
							EIGlobal.mensajePorTrace("***InicioNominaLn.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
						}
					}
		    		/*
		    		 * VSWF ARR -F
		    		 */
					EIGlobal.mensajePorTrace("DAG Entro modulo=0 y con Facultad INOMPAGLINE****", EIGlobal.NivelLog.INFO);
				/***********************.  Req. Q14709. limpieza de variables de Sesion. *****************************/
				//Se controla la Recarga para evitar duplicidad de registros transferencia
					verificaArchivos("/tmp/"+request.getSession().getId().toString()+".ses",true);
					if(request.getSession().getAttribute(RECARGA_SES_MULTIPLESX)!=null){
						request.getSession().removeAttribute(RECARGA_SES_MULTIPLESX);
						EIGlobal.mensajePorTrace( "Inicio Nomina - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples1"+ request.getSession().getAttribute(RECARGA_SES_MULTIPLESX), EIGlobal.NivelLog.INFO);
					} else {
						EIGlobal.mensajePorTrace( "Inicio Nomina - La variable ya fue borrada", EIGlobal.NivelLog.INFO);
					}
					EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples2"+ request.getSession().getAttribute(RECARGA_SES_MULTIPLESX), EIGlobal.NivelLog.INFO);
				/**********************Termina limpieza de variables de Sesion**********************/
					request.setAttribute("Encabezado",CreaEncabezado("Pago de N&oacute;mina en L&iacute;nea","Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request));

					evalTemplate(IEnlace.MTO_NOMINA_TMPLN, request, response );

				} else {
					final String laDireccion = DOCUMENT_LOCATION;
					request.setAttribute( PLATILLAELEGIDA, laDireccion);
					request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
				}
			}else if(("3").equals(modulo)){

				if((session.getFacultad (INOMPAGONOM) &&  session.getFacultad (INOMENVIPAG))) {
					pagosNomina(request, response, accion);
				} else {
					final String laDireccion = DOCUMENT_LOCATION;
					request.setAttribute( PLATILLAELEGIDA, laDireccion);
					request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
				}

			} else {
			  /*	GETRONICS CP MEXICO, NAA.*/
				if( session.getFacultad ("CONSMOVTOS") && session.getFacultad (INOMPAGLINE)) {
  				  // BIT CU 4151, El cliente entra al flujo
  				/*
  	    		 * VSWF
  	    		 */
					if (("ON").equals(Global.USAR_BITACORAS)){
						try {
							final BitaHelper bh = new BitaHelperImpl(request, session,sess);
							bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
							BitaTransacBean bt = new BitaTransacBean();
							hs = tuxGlobal.sreferencia("901");
							referencia = (Integer) hs.get("REFERENCIA");
							bt = (BitaTransacBean)bh.llenarBean(bt);
							if(null != session.getToken().getSerialNumber()) {
								bt.setIdToken(session.getToken().getSerialNumber());
							}
							bt.setIdFlujo(BitaConstants.ES_NOMINALN_PAGOS_CONSULTA);
							bt.setReferencia(referencia);
							codError = hs.get("COD_ERROR").toString();
							bt.setIdErr(codError);
							bt.setNumBit(BitaConstants.ES_NOMINALN_PAGOS_CONSULTA_ENTRA);
							bt.setContrato(session.getContractNumber());
							Date fch=new Date();
							bt.setFechaProgramada(fch);
							bt.setFechaAplicacion(fch);
							BitaHandler.getInstance().insertBitaTransac(bt);
						}catch (SQLException e){
							EIGlobal.mensajePorTrace("***InicioNominaLn.class &Error de excepcion SQL"+e, EIGlobal.NivelLog.INFO);
						}catch(Exception e){
							EIGlobal.mensajePorTrace("***InicioNominaLn.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
	  				}
  				}
  	    		/*
  	    		 * VSWF
  	    		 */

					/**FSW Vector Cambiar titulo**/
			        request.setAttribute("Encabezado", CreaEncabezado("Consulta de N&oacute;mina en L&iacute;nea",
			               "Servicios &gt; N&oacute;mina  &gt; Consultas &gt; Pagos en L&iacute;nea", "s25800conh",request));

			        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy",new java.util.Locale("es","mx"));
			        Date fechaActual = new Date();
				    String hoy = formato.format(fechaActual);
				    request.setAttribute("fechaActual",hoy);

                    evalTemplate( IEnlace.NOMINADATOSLN_TMPL, request, response );
			   } else {
				   final String laDireccion = DOCUMENT_LOCATION;
				   request.setAttribute( PLATILLAELEGIDA, laDireccion);
				   request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
			  }
			}
		} else {
			final String laDireccion = DOCUMENT_LOCATION;
			request.setAttribute( PLATILLAELEGIDA, laDireccion);
			request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
		}
	}

	/** Metodo para realizar el pago de Nomina
	 * @param request contiene los paramatros para realizar el pago
	 * @param response respuesta del pago
	 * @param accion define la accion a llevar a cabo
	 * @throws ServletException expcecion en el Servlet
	 * @throws IOException Excepcion en la entrada salida de archivo
	 */

	public void pagosNomina( HttpServletRequest request, HttpServletResponse response ,String accion)
 			throws ServletException, IOException {
		EIGlobal.mensajePorTrace( "InicioNomina ::  - Entrando a Nuevo pago de nomina", EIGlobal.NivelLog.INFO);
		String campos_fecha;
		String strInhabilesJS;
		BitaTransacBean bt = null;
		ArchivoNominaBean nominaBeanFav = null;//Bean de favoritos


	 	final HttpSession sess = request.getSession(false);
	 	final BaseResource session = (BaseResource) sess.getAttribute(SESSION);
	 	HashMap <String,Boolean>facultadesPagosNomina  = null;
	 	HashMap <String, ArchivoNominaDetBean> ctaCatalogo = null;

	 	String fechaCompleta = "";
		String horario = "";
		String origen = "";
		String tipoNomina = "";
		String tipoAlta = "";

		//M022532 NOMINA EN LINEA - INI
		String  tipoCargo= (String) getFormParameter(request, "TipoCargoIG");		
		EIGlobal.mensajePorTrace( "InicioNominaLn ::: pagosNomina ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );		
		//M022532 NOMINA EN LINEA - FIN
		origen = request.getParameter(PARAM_ORIGEN_PAG);
		origen = (origen == null ? "" : origen);

		fechaCompleta = request.getParameter(PARAM_FECHA_PAG);
		fechaCompleta = (fechaCompleta == null ? "" : fechaCompleta);

		horario =  request.getParameter(PARAM_HOR_PAG);
		horario = (horario == null ? "" : horario);

		tipoNomina = request.getParameter(PARAM_TIP_APLI_PAG);
		tipoNomina = (tipoNomina == null ? "" : tipoNomina);

		tipoAlta = request.getParameter("TipoAlta");


		if("iniciar".equals(accion)){

			//M022532 NOMINA EN LINEA - INI
			tipoCargo = null;
	 		EIGlobal.mensajePorTrace( "InicioNominaLn ::: pagosNomina ::: iniciar ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
	 		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
	 		//M022532 NOMINA EN LINEA - FIN
			
			EIGlobal.mensajePorTrace("DAG Entro modulo=0 y con Facultad INOMPAGLINE****", EIGlobal.NivelLog.INFO);
			/***********************.  Req. Q14709. limpieza de variables de Sesion. *****************************/
			//Se controla la Recarga para evitar duplicidad de registros transferencia
				/*FSW - Vector - Eliminar archivo temporal de Nomina Tradicional*/
				verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString()+".ses",true);
				/*FSW - Vector - Eliminar archivo temporal de Nomina en Linea*/
				verificaArchivos("/tmp/"+request.getSession().getId().toString()+".ses",true);
				if(request.getSession().getAttribute(RECARGA_SES_MULTIPLESX)!=null){
					request.getSession().removeAttribute(RECARGA_SES_MULTIPLESX);
					EIGlobal.mensajePorTrace( "Inicio Nomina - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples1"+ request.getSession().getAttribute(RECARGA_SES_MULTIPLESX), EIGlobal.NivelLog.INFO);
				} else {
					EIGlobal.mensajePorTrace( "Inicio Nomina - La variable ya fue borrada", EIGlobal.NivelLog.INFO);
				}
				EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples2"+ request.getSession().getAttribute(RECARGA_SES_MULTIPLESX), EIGlobal.NivelLog.INFO);
			/**********************Termina limpieza de variables de Sesion**********************/


			sess.removeAttribute(VALIDAR);
			sess.removeAttribute("HOR_SAT");
			sess.removeAttribute("HorarioNormal");
			sess.removeAttribute(VALIDAR);
			sess.removeAttribute("FOLIO");
			sess.removeAttribute("HR_P");
			sess.removeAttribute("archDuplicado");
			sess.removeAttribute("NOMIencFechApli");
			sess.removeAttribute("archivoNominaBeanSess");
			sess.removeAttribute("infoUser");
			sess.removeAttribute(VAR_SESS_CTA_CAT_PAG);

			/*try {

				ctaCatalogo = pagosBO.consultaCtasCatalogo(session.getContractNumber());

				if(ctaCatalogo != null){
					sess.setAttribute(VAR_SESS_CTA_CAT_PAG, ctaCatalogo);
				}
			} catch (BusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/

			facultadesPagosNomina = new HashMap<String,Boolean>();

			facultadesPagosNomina.put("PagNomTrad", session.getFacultad (INOMPAGONOM));
			facultadesPagosNomina.put("PagNomLin", session.getFacultad (INOMPAGLINE ));
			facultadesPagosNomina.put("PagNomEnvPag", session.getFacultad (INOMENVIPAG));

			sess.setAttribute("facultadesPagosNomina", facultadesPagosNomina);
			session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);

			sess.setAttribute("regresarPagEP", "true");

			bt = pagosBO.creaPista(request, ES_NOMINA_PAGOS_IMP_ENVIO, ES_NOMINA_PAGOS_IMP_ENVIO_ENTRA);
			bt.setIdErr("0000");
			pagosBO.registraPista(bt);

		}else if("agregar".equals(accion)){
			//M022532 NOMINA EN LINEA - INI
	 		EIGlobal.mensajePorTrace( "InicioNominaLN ::: pagosNomina ::: agregar ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
	 		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
	 		//M022532 NOMINA EN LINEA - FIN
			if(identificarEsFavorito(request)){

				nominaBeanFav = (ArchivoNominaBean) sess.getAttribute(FavoritosConstantes.ID_FAVORITO);

				if(nominaBeanFav != null){
					request.setAttribute("ctaCargoFav",nominaBeanFav.getCtaCargo());
					request.setAttribute("esFavorito", "1");
					sess.setAttribute(VAR_SESS_BEAN_PAG, nominaBeanFav);
					request.setAttribute(VAR_REQ_BEAN_PAG, nominaBeanFav);
				}
			}else{
				String cuentaSel = request.getParameter("origen");
				EIGlobal.mensajePorTrace("Cuenta seleccionada:"+cuentaSel, EIGlobal.NivelLog.INFO);
				if((cuentaSel!=null)&&(!"".equals(cuentaSel.trim()))){
					request.setAttribute("ctaCargoFav",cuentaSel);
					request.setAttribute("ctaSel", "1");
				}
				agregarRegistroNomina(request,response);
			}

		}else if("enviar".equals(accion)){
			//M022532 NOMINA EN LINEA - INI
			request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
			//M022532 NOMINA EN LINEA - FIN
			enviarArchivo(request,response);

			return;
		}

		//ctaCatalogo = (HashMap <String,ArchivoNominaDetBean>)sess.getAttribute(VAR_SESS_CTA_CAT_PAG);

		request.setAttribute("Encabezado",CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request));

	    campos_fecha = poner_campos_fecha(true);
	    strInhabiles =diasInhabilesDesp();

	    if( "".equals(strInhabiles.trim()) ){
			strInhabiles="01/01/2002,21/03/2002";
	    }
		strInhabilesJS = armaDiasInhabilesJS();

		request.setAttribute("Modulo", "3");
		request.setAttribute("submodulo", "2");
		request.setAttribute("campos_fecha",campos_fecha);
	    request.setAttribute("calendario_normal", poner_calendario_programadas(true,"fecha_completa","WindowCalendar()",fechaCompleta));
	    request.setAttribute("diasInhabiles", strInhabilesJS);
	    request.setAttribute("tiposPago", getConceptos().toString());
	    request.setAttribute("tiposPago2", getConceptos().toString());
	    request.setAttribute(PARAM_FECHA_PAG, fechaCompleta);
		request.setAttribute(PARAM_HOR_PAG, horario);
		request.setAttribute(PARAM_ORIGEN_PAG, origen);
		request.setAttribute(PARAM_TIP_APLI_PAG, tipoNomina);
		//request.setAttribute(VAR_SESS_CTA_CAT_PAG, ctaCatalogo);
		request.setAttribute("TipoAlta", tipoAlta);
		//M022532 NOMINA EN LINEA - INI
		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
 		EIGlobal.mensajePorTrace( "InicioNominaLn ::: pagosNomina2 ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
 		//M022532 NOMINA EN LINEA - FIN
 		
		evalTemplate(PAG_TMPL_INICIO_PAGOS, request, response );

		EIGlobal.mensajePorTrace( "InicioNomina :: pagosNomina ::  - Saliendo a Nuevo pago de nomina", EIGlobal.NivelLog.INFO);


	}


	/**
	 * FSW - Vector
	 * @param request Request de la peticion
	 * @param response Response de la peticion
	 * @throws ServletException ServletException
	 * @throws IOException	IOException
	 */
	public void agregarRegistroNomina( HttpServletRequest request, HttpServletResponse response )
 			throws ServletException, IOException {

		final HttpSession sess = request.getSession(false);
	 	final BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		ArchivoNominaBean nominaBean = null;
		BitaTransacBean bt = null;
		//M022532 NOMINA EN LINEA - INI
 		String tipoCargo = (String) getFormParameter(request, "TipoCargoIG");
 		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN,tipoCargo);
 		EIGlobal.mensajePorTrace( "InicioNominaLn ::: agregarRegistroNomina  ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO );
		//M022532 NOMINA EN LINEA - FIN

		EIGlobal.mensajePorTrace( "InicioNomina :: agregarRegistroNomina ::  - Inicio a Nuevo pago de nomina", EIGlobal.NivelLog.INFO);

		nominaBean = (ArchivoNominaBean)sess.getAttribute(VAR_SESS_BEAN_PAG);
		if(nominaBean == null){
			nominaBean = new ArchivoNominaBean();
			nominaBean.setDetalleArchivo(new LinkedHashMap<String, ArchivoNominaDetBean>());
		}

		obtenValoresRequestAgregar(request, nominaBean);

		sess.setAttribute(VAR_SESS_BEAN_PAG, nominaBean);
		request.setAttribute(VAR_REQ_BEAN_PAG, nominaBean);


		EIGlobal.mensajePorTrace( "InicioNomina :: agregarRegistroNomina ::  - Saliendo a Nuevo pago de nomina", EIGlobal.NivelLog.INFO);
	}

	/**
	 * FSW - Vector
	 * @param request Request de la peticion
	 * @param response Response de la peticion
	 * @throws ServletException ServletException
	 * @throws IOException	IOException
	 */

	public void enviarArchivo(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException{

		final HttpSession sess = request.getSession(false);
		final BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		BitaTransacBean bt = null;

		ArchivoNominaBean nominaBean = null;
		String fecha = null;
		String tipoNomina = null;
		String horario = null;
		String cuentaCargo = null;


		SimpleDateFormat sdf;
		Calendar cal = null;
		String srvlDestino = "";

		String nombreArchivo = null;
		String url = "";
		EIGlobal.mensajePorTrace( "InicioNomina :: enviarArchivo ::  - Inicio", EIGlobal.NivelLog.INFO);

		//M022532 NOMINA EN LINEA - INI
 		String tipoCargo = (String) getFormParameter(request, "TipoCargoIG");
 		EIGlobal.mensajePorTrace( "InicioNominaLN ::: enviarArchivo ::: TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO ); 		
 		request.getSession().setAttribute(TIPO_CARGO_IG_NOM_LIN, tipoCargo);
		//M022532 NOMINA EN LINEA - FIN

		tipoNomina = request.getParameter(PARAM_TIP_APLI_PAG);
		horario = request.getParameter(PARAM_HOR_PAG);
		cuentaCargo = request.getParameter(PARAM_ORIGEN_PAG);

		if(VAL_TIPO_SOLPDF_TR.equals(tipoNomina)){
			fecha = request.getParameter(PARAM_FECHA_PAG);
		}else{
			cal = GregorianCalendar.getInstance();
			sdf = new SimpleDateFormat(FOR_FECHA_DMA, new java.util.Locale("es","mx"));
			fecha = sdf.format(cal.getTime());

		}

		EIGlobal.mensajePorTrace( "InicioNomina :: enviarArchivo ::  - tipoNomina  [" + tipoNomina +"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "InicioNomina :: enviarArchivo ::  - horario     [" + horario +"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "InicioNomina :: enviarArchivo ::  - cuentaCargo [" + cuentaCargo +"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "InicioNomina :: enviarArchivo ::  - fecha       [" + fecha +"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "InicioNomina :: enviarArchivo ::  - Tipo Cargo  [" + tipoCargo +"]", EIGlobal.NivelLog.INFO);


		nominaBean = (ArchivoNominaBean)sess.getAttribute(VAR_SESS_BEAN_PAG);

		obtenValoresRequestEnviar(request,nominaBean);
		nominaBean.setCtaCargo(cuentaCargo);
		nominaBean.setFechaEnvio(fecha);
		//nominaBean.setFechaRecepcion(fecha);
		pagosBO = new NominaPagosBO();

		nombreArchivo = sess.getId() + "_ind.nom";

		pagosBO.generaArchivoPagos(IEnlace.LOCAL_TMP_DIR + "/" + nombreArchivo, nominaBean);

		sess.setAttribute(VAR_SESS_BEAN_PAG, nominaBean);


		if(VAL_TIPO_SOLPDF_TR.equals(tipoNomina)){
			srvlDestino = PAG_TMPL_SRV_TRAD;

		}else{
			srvlDestino = PAG_TMPL_SRV_LIN;
		}
		//M022532 NOMINA EN LINEA - INI
		//url =srvlDestino + "?operacion=cargaArchivo&tipoArchivo=individual&fileName="+nombreArchivo ;
		url =srvlDestino + "?operacion=cargaArchivo&tipoArchivo=individual&fileName="+nombreArchivo+"&TipoCargoIG="+tipoCargo ;
		//M022532 NOMINA EN LINEA - FIN

		EIGlobal.mensajePorTrace( "InicioNominaLn :: enviarArchivo ::  - redireccionando [" + url +"]", EIGlobal.NivelLog.INFO);
		response.sendRedirect(url);

		EIGlobal.mensajePorTrace( "InicioNominaLn :: enviarArchivo ::  - Saliendo", EIGlobal.NivelLog.INFO);
}

	/**
	 * FSW - Vector
	 * @param request request de la peticion
	 * @param nominaBean Bean de archivo de nomina
	 */
	public void obtenValoresRequestAgregar(HttpServletRequest request, ArchivoNominaBean nominaBean){
		String []cuentasSel = null;
		ArchivoNominaDetBean detalleBean = null;
		String monto;
		double montoDouble = 0;
		String tipoAbono;
		LinkedHashMap< String, ArchivoNominaDetBean> detalleArchivo;
		String [] arrDatos = null;
		String cuentaAbono = null;
		int numReg = 0;

		EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - Inicio ", EIGlobal.NivelLog.INFO);

		cuentaAbono = request.getParameter("destino");

		//Obtiene los valores de importe y tipo de abono de la cuentas seleccionadas en el checkbox
		cuentasSel= request.getParameterValues("ctaAbonoSel");

		if(cuentasSel != null && cuentasSel.length >0){
			EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - Seteando los valores del request ", EIGlobal.NivelLog.DEBUG);

			if(nominaBean != null && !nominaBean.getDetalleArchivo().isEmpty()){

				for (String cuenta : cuentasSel) {
					montoDouble = 0;
					monto     = request.getParameter("monto_"+cuenta);
					tipoAbono = request.getParameter("tipoAbono_"+cuenta);

					monto = (monto != null ? monto : "0");
					tipoAbono = (tipoAbono != null ? tipoAbono : "01");

					try{
						montoDouble = Double.parseDouble(monto);
					}catch(NumberFormatException e){
						montoDouble = 0;
						EIGlobal.mensajePorTraceExcepcion(e);
					}

					EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - CuentaSel   [" + cuenta + "]" , EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - Monto       [" + monto + "]" , EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - montoDouble [" + montoDouble + "]" , EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - tipoAbono   [" + tipoAbono + "]" , EIGlobal.NivelLog.DEBUG);

					detalleArchivo = nominaBean.getDetalleArchivo();

					if(detalleArchivo.containsKey(cuenta)){
						EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - Actualizando   [" + cuenta + "]" , EIGlobal.NivelLog.DEBUG);
						detalleBean = detalleArchivo.get(cuenta);
						detalleBean.setImporte(montoDouble);
						detalleBean.setTipoPago(tipoAbono);
						detalleArchivo.put(cuenta, detalleBean);
					}
				}
			}
		}

		if(cuentaAbono != null &&  !"".equals(cuentaAbono.trim())){

			detalleBean = new ArchivoNominaDetBean();
			try{
			arrDatos = cuentaAbono.split("[|]");
			if(arrDatos != null){

				detalleBean.setCuentaAbono(arrDatos[0]);
				detalleBean.setAppPaterno(arrDatos[4]);
				detalleBean.setAppMaterno(arrDatos[5]);
				detalleBean.setNombre(arrDatos[6]);
				detalleBean.setIdEmpleado(arrDatos[3]);

				if(nominaBean.getDetalleArchivo() != null){
					numReg = nominaBean.getDetalleArchivo().size() + 2;
					detalleBean.setSecuencia(numReg);
					nominaBean.getDetalleArchivo().put(Integer.toString(detalleBean.getSecuencia()), detalleBean);
				}
			}
			}catch(NullPointerException e){
				EIGlobal.mensajePorTrace( e.getMessage() , EIGlobal.NivelLog.ERROR);
			}
			catch(ArrayIndexOutOfBoundsException e){
				EIGlobal.mensajePorTrace( e.getMessage() , EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequest ::  - Fin ", EIGlobal.NivelLog.INFO);
	}

	/**
	 * FSW - Vector
	 * @param request request de la peticion
	 * @param nominaBean Bean de archivo de nomina
	 */
	public void obtenValoresRequestEnviar(HttpServletRequest request, ArchivoNominaBean nominaBean){
		String []cuentasSel = null;
		ArchivoNominaDetBean detalleBean = null;
		String monto;
		double montoDouble = 0;
		String tipoAbono;
		LinkedHashMap< String, ArchivoNominaDetBean> detalleArchivo;
		LinkedHashMap< String, ArchivoNominaDetBean> detalleArchivoSel;
		String [] arrDatos = null;
		String cuentaAbono = null;

		EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequestEnviar ::  - Inicio ", EIGlobal.NivelLog.INFO);

		cuentaAbono = request.getParameter("destino");

		//Obtiene los valores de importe y tipo de abono de la cuentas seleccionadas en el checkbox
		cuentasSel= request.getParameterValues("ctaAbonoSel");

		if(cuentasSel != null && cuentasSel.length >0){
			EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequestEnviar ::  - Seteando los valores del request ", EIGlobal.NivelLog.DEBUG);

			if(nominaBean != null && !nominaBean.getDetalleArchivo().isEmpty()){
				detalleArchivoSel = new LinkedHashMap <String, ArchivoNominaDetBean>();
				detalleArchivo = nominaBean.getDetalleArchivo();
				EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequestEnviar ::  - Registros Totales " + detalleArchivo.size(), EIGlobal.NivelLog.INFO);
				for (String cuenta : cuentasSel) {
					montoDouble = 0;
					EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequestEnviar ::  - Registro seleccionado [" + cuenta + "]", EIGlobal.NivelLog.INFO);


					monto     = request.getParameter("monto_"+cuenta);
					tipoAbono = request.getParameter("tipoAbono_"+cuenta);

					monto = (monto != null ? monto : "0");
					tipoAbono = (tipoAbono != null ? tipoAbono : "01");

					try{
						montoDouble = Double.parseDouble(monto);
					}catch(NumberFormatException e){
						montoDouble = 0;
						EIGlobal.mensajePorTrace( e.getMessage() , EIGlobal.NivelLog.ERROR);
					}


					if(detalleArchivo.containsKey(cuenta)){
						EIGlobal.mensajePorTrace( "InicioNomina :: obtenValoresRequestEnviar ::  - Cuenta Encontrada", EIGlobal.NivelLog.DEBUG);
						detalleBean = detalleArchivo.get(cuenta);
						detalleBean.setImporte(montoDouble);
						detalleBean.setTipoPago(tipoAbono);
						detalleArchivoSel.put(cuenta, detalleBean);
					}
				}
				detalleArchivo.clear();
				detalleArchivo = null;
				nominaBean.setDetalleArchivo(detalleArchivoSel);

			}
		}
	}

	/**
	 * M�todo que establecer� las facultades para el m�dulo de pago de n�mina
	 * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
 	public void estableceFacultades( HttpServletRequest request, HttpServletResponse response )
 			throws ServletException, IOException {
 		final HttpSession sess = request.getSession();
 		String eImportar="", eAltas="", eModificar="", eBaja="", eEnvio="", eRecuperar="",
 				eExportar="", eBorrar="";
 		final String cHTML="<INPUT TYPE=\"hidden\" NAME=\"fImportar\" VALUE=\"";
 		final String cHTML1="\">";
 		final String SinFacultades = "SinFacultades";
		//facultad para Importar
 		if(verificaFacultad("INOMIMPAPAG",request)) {
 			eImportar="ImportarVerdadero";
 		} else {
 			eImportar=SinFacultades;
 		}
 		//facultad para Alta de Registros
 		if(verificaFacultad("INOMALTAPAG",request)) {
 			eAltas="AltaVerdadero";
 		} else {
 			eAltas=SinFacultades;
 		}
 		//facultad para Edicion de Registros
 		if(verificaFacultad("INOMMODIPAG",request)) {
 			eModificar="CambiosVerdadero";
 		} else {
 			eModificar=SinFacultades;
 		}
 		//facultad para Baja de Registros
 		if(verificaFacultad("INOMBAJAPAG",request)) {
 			eBaja="BajasVerdadero";
 		} else {
 			eBaja=SinFacultades;
 		}
 		//facultad para el Envio
 		if(verificaFacultad("INOMENVIPAG",request)) {
 			eEnvio="EnvioVerdadero";
 		} else {
 			eEnvio=SinFacultades;
 			request.setAttribute("facultadEnvio",cHTML+eEnvio+cHTML1);
 		}
 		//facultad para la recuperacion
 		if(verificaFacultad("INOMACTUPAG",request)) {
 			eRecuperar="RecuperarVerdadero";
 		} else {
 			eRecuperar=SinFacultades;
 		}
 		//facultad para exportar
 		if(verificaFacultad("INOMEXPOPAG",request)) {
 			eExportar="ExportarVerdadero";
 		} else{
 			eExportar=SinFacultades;
 		}
 		//facultad para borrar un archivo
 		if(verificaFacultad("INOMBORRPAG",request)) {
 			eBorrar="BorraVerdadero";
 		} else{
 			eBorrar=SinFacultades;
 		}
		//facultad para obtener HORARIOS PREMIER
		/**
		*		Modificado por Miguel Cortes Arellano
		*		Fecha 19/12/03
		*		Proposito: Verificar si el usuario tiene facultades para cliente premier. Y dar el correspondiente atributo a la cadena.
		*/
		String eHorarioPremier="";
		if(verificaFacultad("INOMHORARIOS",request)) {
/*		int paso = 1;
		if (paso == 1){ */
			eHorarioPremier="PremierVerdadero";
		}else{
			eHorarioPremier=SinFacultades;
		}
		//String eHorarioPremier="PremierVerdadero";
		final StringBuffer valor1 = new StringBuffer (eImportar+","+eAltas+","+eModificar+","+eBaja+","+eEnvio+","+eRecuperar+","+eExportar+","+eBorrar+","+eHorarioPremier);
		sess.setAttribute("facultadesN",valor1.toString()); /*JEV 12/03/04*/
		request.setAttribute("facultades",valor1.toString());    /*JEV 12/03/04*/
		EIGlobal.mensajePorTrace("Facultades de nomina en la variable de ambiente: "+sess.getAttribute("facultadesN"), EIGlobal.NivelLog.INFO);
	}

	/**
	 * Metodo para verificar existencia del Archivo de Sesion
	 * @param arc archivo
	 * @param eliminar bandera
	 * @return true ? false
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	public boolean verificaArchivos(String arc,boolean eliminar) throws ServletException, java.io.IOException{
		EIGlobal.mensajePorTrace( "InicioNominaLn.java Verificando si el archivo existe " +  arc, EIGlobal.NivelLog.INFO);
		final File archivocomp = new File( arc );
		if (archivocomp.exists()){
		   EIGlobal.mensajePorTrace( "InicioNominaLn.java El archivo si existe.", EIGlobal.NivelLog.INFO);
		   if(eliminar){
	               archivocomp.delete();
	               EIGlobal.mensajePorTrace( "InicioNomina.java El archivo se elimina.", EIGlobal.NivelLog.INFO);
	           }
		   return true;
	        }
		EIGlobal.mensajePorTrace( "InicioNomina.java El archivo no existe.", EIGlobal.NivelLog.INFO);
		return false;
	}

	/**
	* Poner_campos_fecha.
	*
	* @param fac_programadas the fac_programadas
	* @return the string
	*/
	String poner_campos_fecha(boolean fac_programadas)
	{
	String dia_forma=ObtenDia();
	String mes_forma=ObtenMes();
	String anio_forma=ObtenAnio();
	StringBuffer campos_fecha= new StringBuffer("");

	campos_fecha.append("<INPUT ID=dia  TYPE=HIDDEN NAME=dia VALUE=").append(dia_forma).append(" SIZE=2 MAXLENGTH=2> ")
	.append("<INPUT ID=mes  TYPE=HIDDEN NAME=mes VALUE=").append(mes_forma).append(" SIZE=2 MAXLENGTH=2> ")
	.append("<INPUT ID=anio TYPE=HIDDEN NAME=anio VALUE=").append(anio_forma).append(" SIZE=4 MAXLENGTH=4> ");

	return campos_fecha.toString();
	}

	/**
	* Poner_calendario_programadas.
	*
	* @param fac_programadas the fac_programadas
	* @param nombre_campo the nombre_campo
	* @param funcion the funcion
	* @param fecha recibe una fecha
	* @return the string
	*/
	String poner_calendario_programadas(boolean fac_programadas,String nombre_campo,String funcion,String fecha)
	{


	StringBuffer cal = new StringBuffer("");
	if(fecha == null || fecha == "")
	{
		fecha = ObtenDia()+"/"+ObtenMes()+"/"+ObtenAnio();
	}

	cal.append("<INPUT ID=").append(nombre_campo).append(" TYPE=TEXT SIZE=15 class=tabmovtexbol NAME=\"").append(nombre_campo).append("\" VALUE=\"").append( fecha ).append("\" OnFocus=\"blur();\">");
		if (fac_programadas){
			cal.append("<A HREF=\"javascript:").append(funcion).append(";\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" width=12 height=14 border=0 align=absmiddle></A>");
		}
	return cal.toString();
	}

	/**
	* Cargar dias.
	*
	* @param formato the formato
	* @return the vector
	*/
	public Vector<String> CargarDias(int formato)
	{
			EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Entrando", EIGlobal.NivelLog.DEBUG);

	Connection  Conexion=null;
	Statement qrDias=null;
	ResultSet rsDias=null;

	String	   sqlDias;
	Vector<String> diasInhabiles = new Vector<String>();


	 sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
	 EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Query"+sqlDias, EIGlobal.NivelLog.ERROR);
	 try{

		Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			 if(Conexion!=null)
		 {
		    qrDias = Conexion.createStatement();

		    if(qrDias!=null)
		    {
			  rsDias = qrDias.executeQuery(sqlDias);
		       if(rsDias!=null)
		       {
			      String dia  = "";
			      String mes  = "";
			      String anio = "";

			  while( rsDias.next()){
					  try{
				dia  = rsDias.getString(1);
				mes  = rsDias.getString(2);
				anio = rsDias.getString(3);
					  } catch(SQLException e) {
						  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					  }
			      if(formato == 0)
			       {
						  try{
				  dia  =  Integer.toString( Integer.parseInt(dia) );
				  mes  =  Integer.toString( Integer.parseInt(mes) );
				  anio =  Integer.toString( Integer.parseInt(anio) );
						  } catch(Exception e) {

						  }
			       }

			      String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
					  if( "".equals(fecha.trim()) ){
						  fecha="1/1/2002";
					  }
			      diasInhabiles.addElement(fecha);
			  }

		       }
				   else{
			 EIGlobal.mensajePorTrace("***transferencia.CargarDias()->  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);}
		    }
				else{
		      EIGlobal.mensajePorTrace("***transferencia.CargarDias()->  Cannot Create Query", EIGlobal.NivelLog.ERROR);}
		 }
			 else{
				EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);}
	   }catch(SQLException sqle ){
		   EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	   } catch(Exception e) {
		   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	   }
	   finally
	   {
	    try	{
	       rsDias.close();
	    }catch(SQLException e){}
	    try	{
		   qrDias.close();
	    }catch(SQLException e){}
	    try	{
	       Conexion.close();
	    }catch(SQLException e){}
       }

	   return(diasInhabiles);
	}

	/**
	* Arma dias inhabiles js.
	*
	* @return the string
	*/
	String armaDiasInhabilesJS()
	{
	int indice	 = 0;
	StringBuffer resultado = new StringBuffer("");
	Vector<String> diasInhabiles;

	diasInhabiles = CargarDias(1);

	if(diasInhabiles!=null)
	{
		try{
			resultado.append(diasInhabiles.elementAt(indice).toString());
		}catch(Exception e) {};

		for(indice=1; indice<diasInhabiles.size(); indice++){
			resultado.append( ", " ).append( diasInhabiles.elementAt(indice).toString());}

		}
		return resultado.toString();
	}

	/**
	 * Metodo para obtener los tipos pagos
	 * @since 27/05/2015
	 * @return Map<String,String> Mapa con los tipos de pagos
	 */
	public HashMap<String,String> obtenTiposPagos(){

		if(tipoPago == null){
			HashMap<String,String> tipoPago = new HashMap<String,String>();

			tipoPago.put("01","Pago de N&oacute;mina");
	   		tipoPago.put("02","Pago de Vacaciones");
	   		tipoPago.put("03","Pago de Gratificaciones");
	   		tipoPago.put("04","Pago por Comisiones");
	   		tipoPago.put("05","Pago por Beca");
	   		tipoPago.put("06","Pago por Pensi&oacute;n");
	   		tipoPago.put("07","Pago por Subsidios");
	   		tipoPago.put("08","Otros pagos por transferencia");
	   		tipoPago.put("09","Pago por Honorarios");
	   		tipoPago.put("10","Prestamo");
	   		tipoPago.put("11","Pago de viaticos");
	   		tipoPago.put("12","Anticipo de viaticos");
	   		tipoPago.put("13","Fondo de Ahorro");
		}
   		return tipoPago;
	}

	/**
	 * Nomina Concepto de Archvo
	 * @return Codigo HTML con las opciones de los conceptos
	 */
	public StringBuffer getConceptos()
		{
		   EI_Query BD= new EI_Query();
		   StringBuffer cadenaRegreso=new StringBuffer("");
		   Hashtable<String,String[]> listaConceptosBD=null;

		   String sqlConcepto  = "SELECT id_concepto,Concepto from INOM_CONCEPTOS order by id_concepto";
		   listaConceptosBD =BD.ejecutaQuery(sqlConcepto);
		   if( listaConceptosBD==null )
			 {
				EIGlobal.mensajePorTrace( "InicioNominaLn - getConceptos()-> Alguno no funciono ...", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "InicioNominaLn - getConceptos()-> Se tomaran Valores por default.", EIGlobal.NivelLog.INFO);
				cadenaRegreso.append("<option value='01'>Pago de N�mina. </option>\n");
				cadenaRegreso.append("<option value='02'>Pago Vacaciones. </option>\n");
				cadenaRegreso.append("<option value='03'>Pago Gratificaciones. </option>\n");
				cadenaRegreso.append("<option value='04'>Pago por Comisiones. </option>\n");
				cadenaRegreso.append("<option value='05'>Pago por Beca. </option>\n");
				cadenaRegreso.append("<option value='06'>Pago por Pensi�n. </option>\n");
				cadenaRegreso.append("<option value='07'>Pago por Subsidios. </option>\n");
				cadenaRegreso.append("<option value='08'>Otros pagos por transferencia. </option>\n");
				cadenaRegreso.append("<option value='09'>Pago por Honorarios. </option>\n");
				cadenaRegreso.append("<option value='10'>Pr�stamo. </option>\n");
				cadenaRegreso.append("<option value='11'>Pago de vi�ticos. </option>\n");
				cadenaRegreso.append("<option value='12'>Anticipo de vi�ticos. </option>\n");
				cadenaRegreso.append("<option value='13'>Fondo de ahorro. </option>\n");
			  }
			else
			  {
				EIGlobal.mensajePorTrace( "InicioNominaLn - getConceptos()-> Tama�o : "+listaConceptosBD.size() , EIGlobal.NivelLog.INFO);
				String []datos=new String[2];
				for (int i=0;i<listaConceptosBD.size();i++ )
				  {
					 datos=(String[])listaConceptosBD.get(""+i);
					 cadenaRegreso.append("<option value='"+datos[0]+"'>");
					 cadenaRegreso.append(datos[1]);
					 cadenaRegreso.append("</option>\n");
				  }
			  }
		   return cadenaRegreso;
		}
}