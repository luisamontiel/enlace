package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.FavoritosEnlaceBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.FavoritosEnlaceDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FavoritosConstantes;

/**
 * Servlet que gestiona las acciones de favoritos.
 *
 * @author
 *
 */
public class FavoritosEnlaceServlet extends BaseServlet {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Constante para a literal "newMenu"
	 */
	private static final String NEWMENU = "newMenu";

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		FavoritosEnlaceDAO favDAO = new FavoritosEnlaceDAO();
		ejecutarAccion(favDAO, req, resp);

	}

	/**
	 * Ejecuta la accion seleccionada por el cliente.
	 *
	 * @param favoritosDAO
	 *            DAO a utilizar.
	 * @param req
	 *            Objeto peticion.
	 * @param resp
	 *            Objeto respuesta
	 * @throws IOException
	 *             Error de IO
	 * @throws ServletException
	 *             Error de Servlet.
	 */
	protected void ejecutarAccion(FavoritosEnlaceDAO favoritosDAO,
			HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		boolean existeExcepcion = false;

		String idModulo = "";
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceServlet:Inicio del servlet de favoritos.",
				EIGlobal.NivelLog.INFO);

		String tipoOperacion = "";

		if (SesionValida(req, resp)) {

			tipoOperacion = req.getParameter(FavoritosConstantes.ID_OPERACION);

			if (tipoOperacion.equals(FavoritosConstantes.ID_CONSULTA)) {
				idModulo = req.getParameter(FavoritosConstantes.ID_MODULO);
			} else {
				idModulo = sess.getAttribute(
						FavoritosConstantes.ID_MODULO_PARAM).toString();
			}
			EIGlobal.mensajePorTrace(
					"FavoritosEnlaceServlet: La operacion sera: "
							+ tipoOperacion, EIGlobal.NivelLog.INFO);

			sess.removeAttribute(FavoritosConstantes.ID_PARAM_ES_ARCH);

			if(!"IN04".equals(idModulo)){
			sess.setAttribute(FavoritosConstantes.ID_PARAM_ES_ARCH,
					FavoritosConstantes.ID_NO_ES_ARCHIVO);
			}else{
				sess.setAttribute(FavoritosConstantes.ID_PARAM_ES_ARCH,
						FavoritosConstantes.ID_ES_ARCHIVO);
			}
			EIGlobal.mensajePorTrace(
					"FavoritosEnlaceServlet: ES favortio normal!!",
					EIGlobal.NivelLog.INFO);

			if (tipoOperacion.equals(FavoritosConstantes.ID_CONSULTA)) {

				bitacorizarOperacionesFavoritos(
						BitaConstants.CONSULTA_FAVORITOS, null, req);

				try {
					consultarFavoritos(req, resp, session, favoritosDAO,
							FavoritosConstantes.ID_CONSULTA);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(
							"ERROR de sql reportado:" + e.getMessage(),
							EIGlobal.NivelLog.INFO);
					existeExcepcion = true;
				}

			} else if (tipoOperacion.equals(FavoritosConstantes.ID_REDIR)) {
				try {
					redireccionarFlujoOriginal(idModulo, req, resp);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(
							"ERROR SQL al redireccionar al flujo:"
									+ e.getMessage(), EIGlobal.NivelLog.INFO);
					existeExcepcion = true;
				}
			} else if (tipoOperacion.equals(FavoritosConstantes.ID_MODIFICAR)) {
				modificarFavorito(req, resp, favoritosDAO);
			} else if (tipoOperacion.equals(FavoritosConstantes.ID_BORRAR)) {
				borrarFavorito(req, resp, favoritosDAO);
			} else {

				try {
					recorrerPaginas(req, resp, session, favoritosDAO);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace("ERROR SQL al recorrer la pagina:"
							+ e.getMessage(), EIGlobal.NivelLog.INFO);
					existeExcepcion = true;
				}

			}

		}

		if (existeExcepcion) {

			despliegaPaginaErrorFavoritos(
					"Error en la consulta de favoritos, regresar e intentar nuevamente.",
					req, resp);
		}

	}

	/**
	 * Se encarga de eliminar un favorito.
	 *
	 * @param req
	 *            Objeto HttpRequest
	 * @param resp
	 *            Objeto HttpResponse
	 * @param favDAO
	 *            DAo a utilizar.
	 */
	protected void borrarFavorito(HttpServletRequest req,
			HttpServletResponse resp, FavoritosEnlaceDAO favDAO) {

		FavoritosEnlaceBean favorito = obtenerFavoritoSeleccionado(req);
		HttpSession sess = req.getSession();

		if (favorito != null) {

			EIGlobal.mensajePorTrace("Se va a modificar un favorito: "
					+ favorito.getDescripcion(), EIGlobal.NivelLog.INFO);

			favDAO.eliminarFavorito(favorito);

			bitacorizarOperacionesFavoritos(BitaConstants.BAJA_FAVORITO,
					favorito, req);

			try {
				consultarFavoritos(req, resp,
						(BaseResource) sess.getAttribute("session"), favDAO,
						FavoritosConstantes.ID_BORRAR);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"ERROR de SQL al consultar los favoritos :"
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
			} catch (IOException e) {
				EIGlobal.mensajePorTrace("ERROR de IO:" + e.getMessage(),
						EIGlobal.NivelLog.INFO);
			} catch (ServletException e) {
				EIGlobal.mensajePorTrace("ERROR de Servlet:" + e.getMessage(),
						EIGlobal.NivelLog.INFO);
			}

		}
	}

	/**
	 * Se encarga de modificar un favorito.
	 *
	 * @param req
	 *            Objeto HttpRequest
	 * @param resp
	 *            Objeto HttpResponse
	 * @param favDAO
	 *            DAo a utilizar.
	 */
	protected void modificarFavorito(HttpServletRequest req,
			HttpServletResponse resp, FavoritosEnlaceDAO favDAO) {

		String nvaDesc = "";
		FavoritosEnlaceBean favorito = obtenerFavoritoSeleccionado(req);
		HttpSession sess = req.getSession();

		if (favorito != null) {

			nvaDesc = req.getParameter(FavoritosConstantes.ID_NVO_DESC_FAV);

			EIGlobal.mensajePorTrace(
					"Se va a modificar un favorito viaja descripcion ->"
							+ favorito.getDescripcion() + "<-  nva ->"
							+ nvaDesc + "<-", EIGlobal.NivelLog.INFO);

			favorito.setDescripcion(nvaDesc);

			favDAO.modificarFavorito(favorito);
			calculaDatosPaginado(
					Integer.parseInt(sess.getAttribute(
							FavoritosConstantes.ID_NUM_FAVORITOS).toString()),
					req, sess);
			bitacorizarOperacionesFavoritos(BitaConstants.MODIFICAR_FAVORITO,
					favorito, req);

			try {
				irPaginaFavoritos(req, resp);
			} catch (ServletException e) {
				EIGlobal.mensajePorTrace(
						"ERROR sql al redireccionar a la pagina:"
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
			} catch (IOException e) {
				EIGlobal.mensajePorTrace(
						"ERROR de io al redireccionar a la pagina:"
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
			}

		}
	}

	/**
	 * Obtiene un favorito seleccionado.
	 *
	 * @param req
	 *            Objeto HttpRequest
	 * @return El objeto favorito seleccionado.
	 */
	protected FavoritosEnlaceBean obtenerFavoritoSeleccionado(
			HttpServletRequest req) {
		int numFav = -1;
		FavoritosEnlaceBean favorito = null;
		List<FavoritosEnlaceBean> lisFavoritos = null;

		String numFavorito = req
				.getParameter(FavoritosConstantes.ID_NUM_FAVORITO_SEL);
		HttpSession sess = req.getSession();

		EIGlobal.mensajePorTrace("---->>>el numero de favorito seleccionado:"
				+ numFavorito, EIGlobal.NivelLog.INFO);

		lisFavoritos = (List<FavoritosEnlaceBean>) sess
				.getAttribute(FavoritosConstantes.ID_FAVORITOS_PAGINA);

		if ((numFavorito != null) && (lisFavoritos != null)) {

			numFav = Integer.parseInt(numFavorito);
			favorito = lisFavoritos.get(numFav);
			EIGlobal.mensajePorTrace(
					"El numero de favorrito seleccionad es :" + numFav
							+ " este es el favortio "
							+ favorito.getDescripcion(), EIGlobal.NivelLog.INFO);
		} else {
			EIGlobal.mensajePorTrace("ERROR!! no hay favoritos",
					EIGlobal.NivelLog.INFO);
		}
		return favorito;

	}

	/**
	 * Realiza el paginado.
	 *
	 * @param req
	 *            Peticion
	 * @param resp
	 *            Respuesta
	 * @param session
	 *            Objeto con los datos principales.
	 * @param favoritosDAO
	 *            DAO a utilizar.
	 * @throws SQLException
	 *             Posible error en la consulta de los favoritos.
	 * @throws IOException
	 *             Excepcion de IO.
	 * @throws ServletException
	 *             Excepcion de Servlet.
	 */
	protected void recorrerPaginas(HttpServletRequest req,
			HttpServletResponse resp, BaseResource session,
			FavoritosEnlaceDAO favoritosDAO) throws SQLException,
			ServletException, IOException {

		HttpSession sesion = req.getSession();

		int numPagina = Integer.parseInt(req
				.getParameter(FavoritosConstantes.ID_NUM_PAGINA));

		int limiteInferior = FavoritosConstantes.MAX_FAVORITOS
				* (numPagina - 1);
		int limiteSuperior = FavoritosConstantes.MAX_FAVORITOS * numPagina;

		int numPaginasTotales = Integer.parseInt(sesion.getAttribute(
				FavoritosConstantes.ID_NUMERO_PAGINAS).toString());

		int numFavoritos = Integer.parseInt(sesion.getAttribute(
				FavoritosConstantes.ID_NUM_FAVORITOS).toString());

		String contrato = session.getContractNumber();
		String codCliente = session.getUserID8();
		String idModulo = sesion.getAttribute(
				FavoritosConstantes.ID_MODULO_PARAM).toString();

		List<FavoritosEnlaceBean> favoritosPagina = null;

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceServlet: Se va recorrer a la pagina:"
						+ numPagina + " con los limites " + limiteInferior
						+ " a " + limiteSuperior, EIGlobal.NivelLog.INFO);

		if (numFavoritos <= limiteSuperior) {
			limiteSuperior = numFavoritos;
		}

		favoritosPagina = favoritosDAO.obtenerFavoritosPaginados(contrato,
				codCliente, idModulo, limiteInferior+1, limiteSuperior);

		// GUARDAR TODAS LAS VARIABLES.

		sesion.setAttribute(FavoritosConstantes.ID_FAVORITOS_PAGINA,
				favoritosPagina);
		req.setAttribute(FavoritosConstantes.ID_FIN_PAGINA, limiteSuperior);
		req.setAttribute(
				FavoritosConstantes.ID_ES_PAGINA_FINAL,
				numPagina == numPaginasTotales ? FavoritosConstantes.SI_ES_PAG_FINAL
						: FavoritosConstantes.NO_ES_PAG_FINAL);
		req.setAttribute(FavoritosConstantes.ID_INICIO_PAGINA, limiteInferior);
		req.setAttribute(FavoritosConstantes.ID_NUM_PAGINA, numPagina);

		irPaginaFavoritos(req, resp);

	}

	/**
	 * Realiza la consulta de favoritos.
	 *
	 * @param req
	 *            Peticion Http.
	 * @param resp
	 *            Respuesta Http.
	 * @param session
	 *            Sesion.
	 * @param favoritosDAO
	 *            DAO a utilizar para la consulta.
	 * @param origen
	 *            Origen de donde se realiza la consulta.
	 * @throws SQLException
	 *             Excepcion de sql.
	 * @throws IOException
	 *             Excepcion de IO.
	 * @throws ServletException
	 *             Excepcion de servlet.
	 */
	protected void consultarFavoritos(HttpServletRequest req,
			HttpServletResponse resp, BaseResource session,
			FavoritosEnlaceDAO favoritosDAO, String origen)
			throws SQLException, IOException, ServletException {

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceServlet: Inicia la consulta de los favoritos",
				EIGlobal.NivelLog.INFO);

		int numFavoritos = 0;

		HttpSession sess = req.getSession();

		String contrato = session.getContractNumber();
		String codCliente = session.getUserID8();

		String idModulo = "";
		String servletOrigen = "";

		List<FavoritosEnlaceBean> favoritos = null;

		if (FavoritosConstantes.ID_CONSULTA.equals(origen)) {
			inicializarAtributos(sess);

			EIGlobal.mensajePorTrace("....>>Es desde la consulta",
					EIGlobal.NivelLog.INFO);
			idModulo = req.getParameter(FavoritosConstantes.ID_MODULO);
			servletOrigen = req
					.getParameter(FavoritosConstantes.ID_SERVLET_ORIGEN);

		} else {
			EIGlobal.mensajePorTrace("....>>Es desde la opcion de borrar",
					EIGlobal.NivelLog.INFO);
			idModulo = sess.getAttribute(FavoritosConstantes.ID_MODULO_PARAM)
					.toString();
			servletOrigen = sess.getAttribute(
					FavoritosConstantes.ID_SERVLET_ORIGEN).toString();

		}
		numFavoritos = favoritosDAO.obtenerNumeroFavoritos(contrato,
				codCliente, idModulo);

		if (numFavoritos == 0) {
			despliegaPaginaErrorFavoritos("No hay favoritos dados de alta.",
					req, resp);
		} else {

			favoritos = favoritosDAO.obtenerFavoritosPaginados(contrato,
					codCliente, idModulo, 1, FavoritosConstantes.MAX_FAVORITOS);

			calculaDatosPaginado(numFavoritos, req, sess);

			sess.setAttribute(FavoritosConstantes.ID_FAVORITOS_PAGINA,
					favoritos);

			sess.setAttribute(FavoritosConstantes.ID_SERVLET_ORIGEN,
					servletOrigen);
			sess.setAttribute(FavoritosConstantes.ID_NUM_FAVORITOS,
					numFavoritos);
			sess.setAttribute(FavoritosConstantes.ID_MODULO_PARAM, idModulo);

			EIGlobal.mensajePorTrace("Numero de favoritos encontrados: "
					+ favoritos.size(), EIGlobal.NivelLog.INFO);
			irPaginaFavoritos(req, resp);
		}

	}

	/**
	 * Calcula los datos del paginado, esos datos son el numero de paginas y los
	 * rangos a tomar.
	 *
	 * @param numFavoritos
	 *            El total de favoritos.
	 * @param req
	 *            Objeto peticion.
	 * @param sess
	 *            Objeto de sesion.
	 */
	protected void calculaDatosPaginado(int numFavoritos,
			HttpServletRequest req, HttpSession sess) {

		int aux = 0;
		int numPaginas = 1;

		// Inicializa los elementos de la paginacion.
		req.setAttribute(FavoritosConstantes.ID_INICIO_PAGINA, 0);
		req.setAttribute(FavoritosConstantes.ID_NUM_PAGINA, 1);

		if (numFavoritos > FavoritosConstantes.MAX_FAVORITOS) {

			aux = numFavoritos % FavoritosConstantes.MAX_FAVORITOS;
			numPaginas = numFavoritos / FavoritosConstantes.MAX_FAVORITOS;

			if (aux > 0) {
				numPaginas++;
			}

			req.setAttribute(FavoritosConstantes.ID_FIN_PAGINA,
					FavoritosConstantes.MAX_FAVORITOS);
			req.setAttribute(FavoritosConstantes.ID_ES_PAGINA_FINAL,
					FavoritosConstantes.NO_ES_PAG_FINAL);

			sess.setAttribute(FavoritosConstantes.ID_ES_CONSULTA_PAGINADA,
					FavoritosConstantes.SI_ES_PAGINADA);
			sess.setAttribute(FavoritosConstantes.ID_NUMERO_PAGINAS, numPaginas);

		} else {

			req.setAttribute(FavoritosConstantes.ID_FIN_PAGINA, "1");
			req.setAttribute(FavoritosConstantes.ID_ES_PAGINA_FINAL,
					FavoritosConstantes.SI_ES_PAG_FINAL);

			sess.setAttribute(FavoritosConstantes.ID_ES_CONSULTA_PAGINADA,
					FavoritosConstantes.NO_ES_PAGINADA);
			sess.setAttribute(FavoritosConstantes.ID_NUMERO_PAGINAS, 1);
		}

		EIGlobal.mensajePorTrace(
				"En calculaDatosPaginado, el numero de paginas sera de :"
						+ numPaginas, EIGlobal.NivelLog.INFO);

	}

	/**
	 * Metodo encargado de inicializar los atributos de sesion.
	 *
	 * @param sess
	 *            Objeto sesion de la peticion actual.
	 */
	protected void inicializarAtributos(HttpSession sess) {
		sess.removeAttribute(FavoritosConstantes.ID_LISTA_FAVORITOS);
		sess.removeAttribute(FavoritosConstantes.ID_FAVORITO);
		sess.removeAttribute(FavoritosConstantes.ID_SERVLET_ORIGEN);
		sess.removeAttribute(FavoritosConstantes.ID_MODULO_PARAM);
	}

	/**
	 * Redirecciona a la pagina de FAvoritos.
	 *
	 * @param req
	 *            Peticion.
	 * @param resp
	 *            Respuesta.
	 * @throws ServletException
	 *             Excepcion de Servlet.
	 * @throws IOException
	 *             Excepcion IO.
	 */
	protected void irPaginaFavoritos(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("SE va a la pagina de Favoritos",
				EIGlobal.NivelLog.INFO);
		req.setAttribute(NEWMENU, session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado",
				CreaEncabezado("Favoritos Enlace", "Favoritos", "", req));

		evalTemplate("/jsp/FavoritosEnlace.jsp", req, resp);

	}

	/**
	 * Redirecciona al flujo original.
	 *
	 * @param idMoulo
	 *            modulo del que es la peticion.
	 * @param req
	 *            Objeto de la peticion HTTP.
	 * @param resp
	 *            Objeto de la respuesta HTTP.
	 * @throws IOException
	 *             Excepcion IO.
	 * @throws SQLException
	 *             Excepcion de sql.
	 */
	protected void redireccionarFlujoOriginal(String idModulo,
			HttpServletRequest req, HttpServletResponse resp)
			throws IOException, SQLException {

		EIGlobal.mensajePorTrace("Inicia el redir de los favoritos",
				EIGlobal.NivelLog.INFO);

		FavoritosEnlaceBean favorito = null;

		String servletOrigen = "";
		HttpSession sess = req.getSession();

		favorito = obtenerFavoritoSeleccionado(req);

		if ((favorito != null)) {

			sess.setAttribute(FavoritosConstantes.ID_FAVORITO, favorito);

			EIGlobal.mensajePorTrace(
					"Se selecciono el de la lista el cual es: "
							+ favorito.getContrato() + "de "
							+ favorito.getCuentaAbono() + " para  "
							+ favorito.getCuentaCargo() + " con la desc "
							+ favorito.getConceptoOperacion(),
					EIGlobal.NivelLog.INFO);

			servletOrigen = sess.getAttribute(
					FavoritosConstantes.ID_SERVLET_ORIGEN).toString();

			EIGlobal.mensajePorTrace("se redirecciona a: " + servletOrigen,
					EIGlobal.NivelLog.INFO);

			bitacorizarOperacionesFavoritos(BitaConstants.SELECCIONA_FAVORITO,
					favorito, req);
			resp.sendRedirect(servletOrigen + "?"
					+ FavoritosConstantes.ID_PARAM_FAV + "="
					+ FavoritosConstantes.ES_PETICION_DE_FAVORITO);

		} else {
			EIGlobal.mensajePorTrace("Se perdieron datos en sesion ERRROR!!!!",
					EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Despliega la pagina de error para favoritos.
	 *
	 * @param error
	 *            Mensaje de error.
	 * @param request
	 *            Objeto request
	 * @param response
	 *            Objeto response.
	 * @throws IOException
	 *             Error de IO.
	 * @throws ServletException
	 *             Error de Servlet.
	 */
	protected void despliegaPaginaErrorFavoritosMismaPagina(String error,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String pagina = "javascript:window.close();";

		HttpSession session = request.getSession();
		session.setAttribute(NEWMENU, "");
		session.setAttribute("Error", null);
		session.setAttribute("MenuPrincipal", "");
		session.setAttribute(NEWMENU, "");
		session.setAttribute("Encabezado", "");
		session.setAttribute("EstatusError", "");

		request.setAttribute("Error", error);
		request.setAttribute("MenuPrincipal", "");
		request.setAttribute(NEWMENU, "");
		request.setAttribute("Encabezado", "");

		request.setAttribute("NumContrato", "");
		request.setAttribute("NomContrato", "");
		request.setAttribute("NumUsuario", "");
		request.setAttribute("NomUsuario", "");
		request.setAttribute("URL", pagina);

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher(
				"/jsp/EI_Mensaje.jsp");
		response.setContentType("text/html");

		rdSalida.include(request, response);
	}

	/**
	 *
	 * @param error a presentar
	 * @param request peticion del cliente
	 * @param response respuesta
	 * @throws IOException excepcion a manejar
	 * @throws ServletException excepcion a manejar
	 */
	protected void despliegaPaginaErrorFavoritos(String error,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.setAttribute("Error", error);
		request.setAttribute("hayMensaje", "1");
		irPaginaFavoritos(request, response);

	}
}