/*Banco Santander Mexicano
  Clase CuentasImpuestos  Clase para el manejo de las cuentas de impuestos, alta, baja y consulta
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 03/03/2001 - Quitar codigo muerto
 */

package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.*;

public class CuentasImpuestos extends mx.altec.enlace.servlets.MPagoImpuestos{
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

		defaultAction( req, res );

	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

		defaultAction( req, res );

	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	  EIGlobal.mensajePorTrace("***CuentasImpuestos.class Entrando a execute", EIGlobal.NivelLog.INFO);

      String opcion=(String) req.getParameter("OPCION");

      boolean sesionvalida = SesionValida( req, res );
      if (sesionvalida)
      {
        if (opcion.equals("0")){
           despliegaPaginaError("Le informamos que por disposición oficial a partir de esta  fecha únicamente podrá realizar su pago de Impuestos Federales (Esquema Anterior) a través de nuestras ventanillas de la red de sucursales Santander Serfin.<BR><BR><BR>", "Alta de cuentas para pago de impuestos","Servicios &gt; Impuestos federales &gt; Mantenimiento de cuentas &gt; Altas", "s25600h",req, res);
           // Modificación (ARV) req. Q05-34192 Cierre de Alta Cuentas
		   //pantaltacuentaspi( req, res );
        }else if (opcion.equals("1")){
           altacuentaspi( req, res );
        }else if (opcion.equals("10")){
          despliegaPaginaError("Le informamos que por disposición oficial a partir de esta  fecha únicamente podrá realizar su pago de Impuestos Federales (Esquema Anterior) a través de nuestras ventanillas de la red de sucursales Santander Serfin.<BR><BR><BR>", "Baja de cuentas para pago de impuestos","Servicios &gt; Impuestos federales &gt; Mantenimiento de cuentas &gt; Bajas", "s25600h",req, res);
          // Modificación (ARV) req. Q05-34192 Cierre de Baja Cuentas
          //consultacuentaspi( req, res );
        }else if (opcion.equals("11")){
           bajacuentaspi( req, res );
        }
      }
    }

    //Crea la pantalla inicial para el alta de ctas ..
	private void pantaltacuentaspi( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("***CuentasImpuestos.class Entrando a pantaltacuentaspi", EIGlobal.NivelLog.INFO);

		String contrato=session.getContractNumber();

	    //***********************************************
	    //modificación para integración pva 07/03/2002
        //***********************************************
		/*String[][] arrayCuentas=ContCtasRelacpara_mis_transferencias(contrato);
		String tipo_cuenta="";
		String strcuentas="";
		int numcuentas=0;
		int salida;
		boolean estatus=false;
		boolean chkseg=true;
		for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++){
			tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);

			if (arrayCuentas[indice][2].equals("P")&&
					(!arrayCuentas[indice][3].equals("4"))&&
					(!arrayCuentas[indice][3].equals("5"))&&
					(!arrayCuentas[indice][3].equals("6"))){

				chkseg  = !existeFacultadCuenta(arrayCuentas[indice][1], "CARCHPROP", "-");
				//chkseg  = !verificaFacultadCuenta(arrayCuentas[indice][1], "CARCHPROP");
				if(chkseg==true){
					strcuentas = strcuentas + "<OPTION VALUE=\""+arrayCuentas[indice][1]+"|"+arrayCuentas[indice][2]+"|"+ arrayCuentas[indice][4]+"|"+"\"> "  + arrayCuentas[indice][1] +" </OPTION>\n" ;
					numcuentas++;
				}
			}

		}
*/
		session.setModuloConsultar(IEnlace.MAlta_ctas_pago_imp);
		req.setAttribute("fecha_hoy",ObtenFecha());
		//modificacion para integracion req.setAttribute("cuentasgx", strcuentas);
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Alta de cuentas para pago de impuestos","Servicios &gt; Impuestos federales  &gt; Mantenimiento de cuentas &gt; Altas","s25550h",req));
		evalTemplate( IEnlace.PIALTA_CTAS_TMPL, req, res);
	}

	 //realiza el alta de ctas llamando al servicio WEB_RED
     private void altacuentaspi( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

   	   EIGlobal.mensajePorTrace("***CuentasImpuestos.class Entrando a altacuentaspi", EIGlobal.NivelLog.INFO);

       boolean estatus = false;
       String coderror = "";
       String mensaje = "";
       String contrato = session.getContractNumber();
       String claveperfil = session.getUserProfile();
       String usuario = session.getUserID();

       String cuenta = (String) req.getParameter("cuenta");
       String mensajeservicio = "";
       if( cuenta.length() > 11)
         cuenta = cuenta.substring(0,11);
       else
         EIGlobal.mensajePorTrace("***CuentasImpuestos.class cuenta Erronea:"+cuenta, EIGlobal.NivelLog.INFO);

       String rfc = (String) req.getParameter("rfc").toUpperCase();
       String homoclave = (String) req.getParameter("homoclave").toUpperCase();
       String razonsocial = (String) req.getParameter("nombrerazonsocial").toUpperCase();
       String calle = (String) req.getParameter ("calle").toUpperCase();
       String colonia = (String) req.getParameter ("colonia").toUpperCase();
       String delegacion = (String) req.getParameter ("delegacion").toUpperCase();
       String estado =(String) req.getParameter ("estado").toUpperCase();
       String ciudad = (String) req.getParameter ("ciudad").toUpperCase();
       String cp = (String) req.getParameter ("cp");
       String telefono = (String) req.getParameter ("telefono");
	   if (telefono.length()==0)
	      telefono="00000000";


       String tramaentrada = "1TCT|"+usuario+"|AIMP|"+contrato+"|"+usuario+"|"+claveperfil+"|"+contrato+"@"+cuenta+"@"+razonsocial+"@"+calle+"@"+colonia+
                                     "@"+delegacion+"@"+estado+"@"+ciudad+"@"+cp+
                                     "@"+telefono+"@"+rfc+"@"+homoclave+"@";

	   try{
	   	//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   	Hashtable hs = tuxGlobal.web_red(tramaentrada);;
	   	EIGlobal.mensajePorTrace("***CuentasImpuestos WEB_RED-AIMP>>"+tramaentrada, EIGlobal.NivelLog.DEBUG);
 	    coderror = (String) hs.get("BUFFER");
	    EIGlobal.mensajePorTrace("***CuentasImpuestos<<"+coderror, EIGlobal.NivelLog.DEBUG);

	   }catch( java.rmi.RemoteException re){
	   	re.printStackTrace();
	   } catch (Exception e) {}



       mensaje = "<TABLE BORDER=1 ALIGN=CENTER>";
	   mensaje += "<TR><TD align=center class=tittabdat width=200>Cuenta </TD><TD class=tittabdat align=center width=60>Estatus</TD></TR>";
       mensaje += "<TR>";
       mensaje += "<TD class=textabdatobs>";
       mensaje += cuenta;
       mensaje += "</TD>";
       mensaje += "<TD class=textabdatobs>";
       if (coderror.substring(0,2).equals("OK"))
          mensaje += "Alta realizada ";
       else
		  {
		  // Modificación para 'cortar' un mensaje (no debe mostrar 'oprima actualizar') RVV 13-jul-2002
		  String mensError = coderror.substring(16,coderror.length());
		  if(mensError.equals("La cuenta ya esta registrada,oprima actualizar")) mensError = "La cuenta ya esta registrada";
          mensaje += mensError;
		  }
       mensaje += "</TD>";
       mensaje += "</TR>";
       mensaje += "</TABLE>";
       operacionrealizada("Alta de cuentas para pago de impuestos",mensaje,"Servicios &gt; Impuestos federales  &gt; Mantenimiento de cuentas &gt; Altas","s25550h", req, res);

     }

	 //Realiza la consulta de ctas para el pago de impuestos
	 //llamando al servicio WEB_RED y leyendo el archivo con las ctas..
     private void consultacuentaspi( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

       EIGlobal.mensajePorTrace("***CuentasImpuestos.class Entrando a consultacuentaspi", EIGlobal.NivelLog.INFO);

       String mensaje = "";
       //modificacion para integración mensaje=llamada_servicio_consulta_cuentas_pi(0,req, res);
       if(mensaje!=null){
           //modificacion para integracion req.setAttribute("mensaje_salida",mensaje);
           req.setAttribute("fecha_hoy",ObtenFecha());
           req.setAttribute("MenuPrincipal", session.getStrMenu());
           req.setAttribute("newMenu", session.getFuncionesDeMenu());
           req.setAttribute("Encabezado", CreaEncabezado("Selecci&oacute;n de Cuentas a dar de Baja","Servicios &gt; Impuestos federales  &gt; Mantenimiento de cuentas &gt; Bajas","s25560h", req));

		   //***********************************************
		   //modificación para integración pva 07/03/2002
           //***********************************************
		   session.setModuloConsultar(IEnlace.MEnvio_pago_imp);
           //***********************************************
		   evalTemplate(IEnlace.PICONS_CTAS_TMPL, req, res );
        }
     }

     //Ejecuta la baja de la cta para el pago de impuestos
	 //llamando al servicio WEB_RED
     private void bajacuentaspi( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException{
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

       EIGlobal.mensajePorTrace("***CuentasImpuestos.class Entrando a bajacuentaspi", EIGlobal.NivelLog.INFO);

	   String contrato = session.getContractNumber();
       String claveperfil = session.getUserProfile();
       String usuario = session.getUserID();
       String coderror = "";
       String cuenta = (String) req.getParameter("cuenta");
       String mensajeservicio = "";
       String tramaentrada;
       String mensaje = "";

       mensaje = "<TABLE BORDER=1 ALIGN=CENTER>";
       tramaentrada = "1TCT|"+usuario+"|BIMP|"+contrato+"|"+usuario+"|"+claveperfil+"|"+contrato+"@"+cuenta+"@";
	   try{
	   	//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   	Hashtable hs = tuxGlobal.web_red(tramaentrada);
	   	EIGlobal.mensajePorTrace("***CuentasImpuestos WEB_RED-BIMP>>"+tramaentrada, EIGlobal.NivelLog.DEBUG);
	   	coderror = (String) hs.get("BUFFER") ;
        EIGlobal.mensajePorTrace("***CuentasImpuestos<<"+coderror, EIGlobal.NivelLog.DEBUG);
	   }catch( java.rmi.RemoteException re){
	   	re.printStackTrace();
	   } catch (Exception e) {}





       if(coderror.length()>16)
         mensajeservicio = coderror.substring(16,coderror.length());
       if (coderror!=null);
         coderror=coderror.substring(0,8);

	   mensaje += "<TR><TD align=center class=tittabdat width=200>Cuenta </TD><TD class=tittabdat align=center width=60>Estatus</TD></TR>";
       mensaje += "<TR>";
       mensaje += "<TD class=textabdatobs>"+cuenta +"</TD>";
       mensaje += "<TD class=textabdatobs>";
       if (coderror.substring(0,2).equals("OK"))
          mensaje += "Baja realizada ";
       else
          mensaje += mensajeservicio;//serverror(coderror);
       mensaje += "</TD>";
       mensaje += "</TR>";
       mensaje += "</TABLE>";
       operacionrealizada("Baja de cuentas para pago de impuestos",mensaje,"Servicios &gt; Impuestos federales  &gt; Mantenimiento de cuentas &gt; Bajas","s25560h", req, res);

     }


}