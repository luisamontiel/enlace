package mx.altec.enlace.servlets;
//Modificacion RMM 20021217 cerrado de archivos, cambio RandomAccessFile por BufferedReader
import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class CentroMensajes extends BaseServlet{


	short sucOpera       = (short)787;

    public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

      //Variables de sesion ...
	  String ventana       = "";
      String contrato      = "";
      String usuario       = "";
      String clavePerfil   = "";

	  ventana = (String) req.getParameter("ventana");

        if(SesionValida(req, res))
          {
			 contrato          = session.getContractNumber();
		     sucOpera          = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			 usuario           = session.getUserID8();
			 clavePerfil       = session.getUserProfile();

  	         EIGlobal.mensajePorTrace("contrato: "+ contrato, EIGlobal.NivelLog.INFO);
			 EIGlobal.mensajePorTrace("sucopera: "+sucOpera, EIGlobal.NivelLog.INFO);
			 EIGlobal.mensajePorTrace("usuario: "+usuario, EIGlobal.NivelLog.INFO);
			 EIGlobal.mensajePorTrace("clave perfil: "+clavePerfil, EIGlobal.NivelLog.INFO);

             if(ventana.equals("0"))
              {
                paginaPrincipal(usuario, contrato, clavePerfil, req, res);
              }
             else if(ventana.equals("1"))
              {
				leerMensaje(usuario, contrato, clavePerfil, req, res);
              }

             else if(ventana.equals("2"))
              {
				escribirMensaje(usuario, contrato, clavePerfil, req, res);
              }
            else if(ventana.equals("3"))
              {
				enviarMensaje(usuario, contrato, clavePerfil, req, res);
              }
          }

	}



public int paginaPrincipal(String usuario, String contrato, String clavePerfil, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");


	   EIGlobal.mensajePorTrace( "***CentroMensajes.class & Entrando a paginaPrincipal &", EIGlobal.NivelLog.INFO);

       //TuxedoGlobal tuxGlobal;
       //tuxGlobal = new TuxedoGlobal(this);

	   ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

       int salida=0;

       String tramaEntrada        = "";
       String tramaSalida         = "";
       String path_archivo        = "";
       String nombre_archivo      = "";
       String nombre_archivo2     = "";
       String trama_salidaRedSrvr = "";
       String retCodeRedSrvr      = "";
       String registro            = "";
	   String strMensaje          = "";
	   String strBotones          = "";
	   String strClase            = "";

	   StringBuffer encabezado          = new StringBuffer("");
	   StringBuffer strTabla            = new StringBuffer("");
	   StringBuffer tramaMensajes       = new StringBuffer("");

	   //2TCT|6980|MENS|09000357620|6980|6980|0

       String medio_entrega  = "2EWEB";
       String tipo_operacion = "MENS";

	   // ojo: revisar de donde se va a obtener.
	   String ultimoMensaje = "0";

       encabezado.append( medio_entrega );
	   encabezado.append("|" );
	   encabezado.append(usuario );
	   encabezado.append("|" );
	   encabezado.append(tipo_operacion );
	   encabezado.append("|" );
	   encabezado.append(contrato );
	   encabezado.append("|");
       encabezado.append(usuario );
	   encabezado.append("|" );
	   encabezado.append(clavePerfil );
	   encabezado.append("|");

       tramaEntrada  = encabezado.toString() + ultimoMensaje;

	   EIGlobal.mensajePorTrace( "***CentroMensajes.class >> tramaEntrada MENS: " + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);

       //tramaSalida = tuxGlobal.ejecutaWEB_RED(tramaEntrada);

	   try{
			Hashtable hs = tuxGlobal.web_red(tramaEntrada);
		   	tramaSalida = (String) hs.get("BUFFER");
		  	EIGlobal.mensajePorTrace( "***CentroMensajes.class trama_salidaRedSrvr >>" + tramaSalida + "<<", EIGlobal.NivelLog.DEBUG);
	   }catch( java.rmi.RemoteException re ){
			re.printStackTrace();
	   } catch(Exception e) {}

	   EIGlobal.mensajePorTrace( "***CentroMensajes.class >> tramaSalida MENS:" + tramaSalida + " <<", EIGlobal.NivelLog.DEBUG);

	   path_archivo = tramaSalida;
       nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());
       nombre_archivo2 = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length()) + "mens";

	   EIGlobal.mensajePorTrace( "***CentroMensajes.class & nombre_archivo MENS:" + nombre_archivo + " &", EIGlobal.NivelLog.DEBUG);
	   EIGlobal.mensajePorTrace( "***CentroMensajes.class & nombre_archivo MENS 2:" + nombre_archivo2 + " &", EIGlobal.NivelLog.DEBUG);

	    // Copia Remota
		ArchivoRemoto archR = new ArchivoRemoto();

		// 2011-06-06, se envian los dos archivos, estos son generados por el corsrvr
		if(!archR.copia(nombre_archivo) || !archR.copia(nombre_archivo2))
		 {
			EIGlobal.mensajePorTrace( "***CentroMensajes.class & paginaPrincipal: No se realizo la copia remota. &", EIGlobal.NivelLog.INFO);
			strMensaje = "No se pudo obtener el archivo de mensajes. Intente m&aacute;s tarde.";
		    despliegaPaginaError(strMensaje, "Centro de Mensajes","Centro de Mensajes", req, res);
		 }
		else
		 {
	        boolean errorFile=false;
            //Modificacion RMM 20021217
            BufferedReader tmpFile = null;
	        try
		     {
				EIGlobal.mensajePorTrace( "***CentroMensajes.class & paginaPrincipal: Copia remota OK. &", EIGlobal.NivelLog.INFO);

				path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo2;
				EIGlobal.mensajePorTrace( "***CentroMensajes.class & path_archivo MENS:" + path_archivo + " &", EIGlobal.NivelLog.INFO);

		        File drvFile = new File(path_archivo);
                //Modificacion RMM 20021217
			    tmpFile = new java.io.BufferedReader(new java.io.FileReader(drvFile));

				trama_salidaRedSrvr = tmpFile.readLine();

				EIGlobal.mensajePorTrace( "***CentroMensajes.class trama_salidaRedSrvr MENS: &" + trama_salidaRedSrvr + "&", EIGlobal.NivelLog.INFO);

				// 2011-06-06, se lee primero el archivo /tmp/usuariomens
				if (trama_salidaRedSrvr != null){

						int mensajes = 0;
						mensajes = EIGlobal.CuantosTokens(trama_salidaRedSrvr, '|');
						EIGlobal.mensajePorTrace( "***CentroMensajes.class No. mensajes: " + mensajes, EIGlobal.NivelLog.INFO);

						for(int i=1; i<=mensajes;i++){
							strMensaje = EIGlobal.BuscarToken(trama_salidaRedSrvr, '|', i);
							strTabla.append("  <tr>");
							strTabla.append("    <td class=\"textabcen\" colspan=\"2\" align=\"center\" >" );
							strTabla.append(strMensaje );
							strTabla.append("</td>");
							strTabla.append("  </tr>");
						}
				// Si el archivo /tmp/usuariomens no tiene info, se lee el archivo /tmp/usuario
				} else {

					path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;
					drvFile = new File(path_archivo);
				    tmpFile = new java.io.BufferedReader(new java.io.FileReader(drvFile));

					trama_salidaRedSrvr = tmpFile.readLine();
					EIGlobal.mensajePorTrace( "***CentroMensajes.class trama_salidaRedSrvr MENS 2: &" + trama_salidaRedSrvr + "&", EIGlobal.NivelLog.INFO);

					strMensaje = trama_salidaRedSrvr.substring(16, trama_salidaRedSrvr.length());
					strTabla.append("  <tr>");
					strTabla.append("    <td class=\"textabcen\" colspan=\"2\" align=\"center\" >" );
					strTabla.append(strMensaje );
					strTabla.append("</td>");
					strTabla.append("  </tr>");
				} // 2011-06-06. FIN

				tmpFile.close();
				req.setAttribute("Tabla",		   strTabla.toString());
				req.setAttribute("tramaMensajes", tramaMensajes.toString());
				req.setAttribute("newMenu",       session.getFuncionesDeMenu());
				req.setAttribute("Encabezado",    CreaEncabezado("Centro de mensajes de Enlace Internet", "Centro de mensajes", "s26370h", req));
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				evalTemplate("/jsp/CemePrincipal.jsp", req, res);

			 }catch(Exception ioeSua)
               {
					EIGlobal.mensajePorTrace( "***CentroMensajes.class Excepcion %paginaPrincipal() >> "+ ioeSua.getMessage() + " <<", EIGlobal.NivelLog.INFO);
					strMensaje += "Error al leer el archivo de mensajes, por favor intente mas tarde.";
					despliegaPaginaError(strMensaje ,"Centro de Mensajes","Centro de Mensajes", req, res);
               }finally{
                   //Modificacion RMM 20021217
                   if(null != tmpFile){
                       try{
                           tmpFile.close();
                       }catch(java.io.IOException ex){}
                       tmpFile = null;
                   }
               }

		 }

  return 1;

}



public int leerMensaje(String usuario, String contrato, String clavePerfil, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");


	EIGlobal.mensajePorTrace( "***CentroMensajes.class & Entrando a leerMensaje &", EIGlobal.NivelLog.INFO);

    int salida        = 0;

	String tramaMensajes    = (String) req.getParameter("tramaMensajes");
	String strNumeroMensaje = (String) req.getParameter("numeroMensaje");
	int numeroMensaje = new Integer(strNumeroMensaje).intValue();

    String registro            = "";
	String strMensaje          = "";
	String strBotones          = "";
	String strClase            = "";

    registro = EIGlobal.BuscarToken(tramaMensajes, '|', numeroMensaje);
	strMensaje = EIGlobal.BuscarToken(registro, '@', 2);

	req.setAttribute("Mensaje",       strMensaje);
	req.setAttribute("tramaMensajes", tramaMensajes);
	req.setAttribute("newMenu",       session.getFuncionesDeMenu());
	req.setAttribute("Encabezado",    CreaEncabezado("Centro de mensajes de Enlace Internet", "Centro de mensajes", "s26380h", req));
	req.setAttribute("MenuPrincipal", session.getStrMenu());
	evalTemplate("/jsp/CemeLeer.jsp", req, res);

  return 1;

}



public int escribirMensaje(String usuario, String contrato, String clavePerfil, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

	EIGlobal.mensajePorTrace( "***CentroMensajes.class & Entrando a escribirMensaje &", EIGlobal.NivelLog.INFO);

    int    salida      = 0;

	StringBuffer strTextArea = new StringBuffer("");
	StringBuffer strBotones  = new StringBuffer("");

	strTextArea.append( "\n<table width=\"320\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
	strTextArea.append("\n   <tr>");
	strTextArea.append("\n    <td class=\"tittabdat\" align=\"center\">Escribir Mensaje</td>");
	strTextArea.append("\n   </tr>");
    strTextArea.append("\n   <tr>");
    strTextArea.append("\n     <td class=\"tabmovtex\" align=\"center\" nowrap>");
	strTextArea.append("\n       <TEXTAREA class=\"tabmovtex\" NAME=\"taMensaje\" ROWS=\"7\" COLS=\"50\"></TEXTAREA>");
	strTextArea.append("\n     </td>");
    strTextArea.append("\n   </tr>");
    strTextArea.append("\n </table><br>");

    strBotones.append( "<td align=\"right\" valign=\"middle\" width=\"66\">");
    strBotones.append("<A href = \"javascript:enviarMensaje(document.frmCentroMensajes);\" border=0>");
    strBotones.append("<img src=\"/gifs/EnlaceInternet/gbo25520.gif\" border=0 alt=\"Enviar\"></a>");
    strBotones.append("</td>");

	req.setAttribute("Cuerpo",        strTextArea.toString());
	req.setAttribute("Botones",       strBotones.toString());
	req.setAttribute("newMenu",       session.getFuncionesDeMenu());
	req.setAttribute("Encabezado",    CreaEncabezado("Centro de mensajes de Enlace Internet", "Centro de mensajes", "s26390h", req));
	req.setAttribute("MenuPrincipal", session.getStrMenu());

	evalTemplate("/jsp/CemeEscribir.jsp", req, res);

    return 1;
}


public int enviarMensaje(String usuario, String contrato, String clavePerfil, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

	   EIGlobal.mensajePorTrace( "***CentroMensajes.class & Entrando a enviarMensaje &", EIGlobal.NivelLog.INFO);

	   String taMensaje = (String) req.getParameter("taMensaje");

       //TuxedoGlobal tuxGlobal;
       //tuxGlobal = new TuxedoGlobal(this);

   	   ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

       int salida=0;

       StringBuffer encabezado          = new StringBuffer("");
       String tramaEntrada        = "";
       String tramaSalida         = "";
       String retCodeRedSrvr      = "";
	   StringBuffer strMensaje          = new StringBuffer("");
	   String strTitulo           = "";
	   String referencia          = "";

	   //1TCT|817|ENME|80000648376|817|0000817|ESTE ES UN MENSAJE DE PRUEBA 01

       String medio_entrega  = "1EWEB";
       String tipo_operacion = "ENME";

	   // ojo: revisar de donde se va a obtener.
	   //String ultimoMensaje = "0";

       encabezado.append(medio_entrega );
	   encabezado.append("|" );
	   encabezado.append(usuario );
	   encabezado.append("|" );
	   encabezado.append(tipo_operacion );
	   encabezado.append("|" );
	   encabezado.append(contrato );
	   encabezado.append("|");
       encabezado.append(usuario );
	   encabezado.append("|" );
	   encabezado.append(clavePerfil );
	   encabezado.append("|");

       tramaEntrada  = encabezado.toString() + taMensaje;

	   EIGlobal.mensajePorTrace( "***CentroMensajes.class >> tramaEntrada ENME: " + tramaEntrada + "<<", EIGlobal.NivelLog.DEBUG);

       //tramaSalida = tuxGlobal.ejecutaWEB_RED(tramaEntrada);

	   try{
			Hashtable hs = tuxGlobal.web_red(tramaEntrada);
	      	tramaSalida = (String) hs.get("BUFFER");
		  	EIGlobal.mensajePorTrace( "***CentroMensajes.class trama_salidaRedSrvr >>" + tramaSalida + "<<", EIGlobal.NivelLog.DEBUG);
	   }catch( java.rmi.RemoteException re ){
	  		re.printStackTrace();
	   } catch(Exception e) {}


	   EIGlobal.mensajePorTrace( "***CentroMensajes.class >> tramaSalida ENME:" + tramaSalida + "<<", EIGlobal.NivelLog.DEBUG);

	   retCodeRedSrvr = tramaSalida.substring(0, 8).trim();

	   EIGlobal.mensajePorTrace( "***CentroMensajes.class >> retCodeRedSrvr: " + retCodeRedSrvr + "<<", EIGlobal.NivelLog.DEBUG);

	   if (retCodeRedSrvr.equals("OK"))
		{
			referencia  = tramaSalida.substring(8, tramaSalida.length());
			strTitulo   = "Mensaje Recibido";
			strMensaje.append( "<p class=\"titpag\">Gracias</p>");
			strMensaje.append("<p>Estimado(a) " );
			strMensaje.append(session.getNombreUsuario());
			strMensaje.append(", hemos recibido su mensaje, y tan pronto nos sea posible");
			strMensaje.append(", nos pondremos en contacto con usted.</p>");
		}
	   else
		{
			strTitulo   = "El mensaje no fu&eacute; recibido";
		    strMensaje.append(tramaSalida.substring(8, tramaSalida.length()));
		}

		req.setAttribute("Titulo",		   strTitulo);
		req.setAttribute("Mensaje",	   strMensaje.toString());
		req.setAttribute("newMenu",       session.getFuncionesDeMenu());
		req.setAttribute("Encabezado",    CreaEncabezado("Centro de mensajes de Enlace Internet", "Centro de mensajes", "s26400h", req));
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		evalTemplate("/jsp/CemeEnviar.jsp", req, res);

  return 1;

}


	public String CreaEncabezado( String tituloPantalla, String posicionMenu, String ClaveAyuda, HttpServletRequest req) {
		String strEncabezado="";
		String v_contrato="";
		String tit_contrato="";

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if ( session.getContractNumber() != null ) {
			tit_contrato = "Contrato:";
			v_contrato = ObtenContUser(req);
			EIGlobal.mensajePorTrace("#############-----------------@@@@@@@@@---->"+v_contrato, EIGlobal.NivelLog.INFO);

		}
		StringBuffer strEnc = new StringBuffer();

		strEnc.append("<table width=");
		strEnc.append("\"760\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		strEnc.append("\n<tr>\n");
		strEnc.append("<td width=\"676\" valign=\"top\" align=\"left\">\n");
		strEnc.append("<table width=\"666\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td width=\"528\" valign=\"top\" class=\"titpag\">\n");
		strEnc.append(tituloPantalla);
		strEnc.append("</td>\n");
		strEnc.append("<td width=\"120\" align=\"right\" valign=\"bottom\" class=\"texencfec\">\n");
		strEnc.append(ObtenFecha());
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td valign=\"top\" class=\"texencrut\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gic25030.gif\" width=\"7\" height=\"13\"> \n");
		strEnc.append(posicionMenu);
		strEnc.append("</td>\n");
		strEnc.append("<td class=\"texenchor\" align=\"right\" valign=\"top\">\n");
		strEnc.append(ObtenHora());
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");
		strEnc.append("</td>\n");
		strEnc.append( "<td width=\"40\" valign=\"top\"><a href=\"javascript:FrameAyuda('" );
		strEnc.append( ClaveAyuda );
		strEnc.append("');\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('ayuda','','/gifs/EnlaceMig/gbo25171.gif',1)\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gbo25170.gif\" width=\"33\" height=\"49\" name=\"ayuda\" border=\"0\"></a></td>\n");
		strEnc.append("<td width=\"44\" valign=\"top\"><a href=\"logout\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)\">\n");
		strEnc.append("<img src=\"/gifs/EnlaceMig/gbo25180.gif\" width=\"44\" height=\"49\" name=\"finsesion\" border=\"0\"></a></td>\n\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");
//Contrato
		/*strEnc.append("<table width=\"760\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">\n");
		strEnc.append("<tr>\n");
		strEnc.append("<td align=\"center\" valign=\"top\" class=\"texenccon\"><span class=\"texencconbol\">");
		strEnc.append(tit_contrato);
		strEnc.append("</span>\n");
		strEnc.append(v_contrato);
		strEnc.append("</td>\n");
		strEnc.append("</tr>\n");
		strEnc.append("</table>\n");*/

		strEnc.append("<Script language = \"JavaScript\">\n");
		strEnc.append("<!--\n");
		strEnc.append("function FrameAyuda(ventana){\n");
		strEnc.append("hlp=window.open(\"/EnlaceMig/\"+ventana+\".html\" ,\"hlp\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450\");\n");
		strEnc.append("hlp.focus();\n");
		strEnc.append("}\n");
		strEnc.append("//-->\n");
		strEnc.append("</Script>\n");
		strEncabezado= strEnc.toString();

		return strEncabezado;

	}

}