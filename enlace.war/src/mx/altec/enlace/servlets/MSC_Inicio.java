
package mx.altec.enlace.servlets;

import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.*;
import java.math.BigDecimal;

import java.rmi.*;
import javax.sql.*;
import java.sql.*;
import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


//=================================================================================================
public class MSC_Inicio extends mx.altec.enlace.servlets.BaseServlet
	{

	  String pipe             = "|";

//=================================================================================================
	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

//=================================================================================================
	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

//=================================================================================================
	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	  {

        /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

        String Trama="";
		String Result="";
		String coderror="";

		String strCuentasDolares  = "";
        String strCuentasNacional = "";

        String contrato           = "";
        String usuario            = "";
        String clavePerfil        = "";

		String cadenaFac          = "";
		String cadenaTCT          = "";
		String exportar			  = "";
		String[] importes		  = {"0.0","0.0","0.0"};
		String[] titulos		  = {"", "Cuenta", "Descripci&oacute;n", "Disponible", "SBC", "Total"};

		Hashtable htResult = null;

		int[] datos = {5,1,1,2,4,0,3,5};
		int[] value = {4,7,1,2,8,4};  //Todos los datos
		int[] align = {0,0,2,2,2,2};

		int erroresNacional = 0;
		int erroresDolares  = 0;

		BigDecimal impTotNac = new BigDecimal("0");
		BigDecimal impDisNac = new BigDecimal("0");
		BigDecimal impSBCNac = new BigDecimal("0");

		BigDecimal impTotDol = new BigDecimal("0");
		BigDecimal impDisDol = new BigDecimal("0");
		BigDecimal impSBCDol = new BigDecimal("0");
		

		BigDecimal imp2 = new BigDecimal("0");
		BigDecimal imp1 = new BigDecimal("0");
		BigDecimal imp0 = new BigDecimal("0");


		int numeroreferencia=0;

		boolean sesionvalida  = SesionValida ( req, res );

		EI_Tipo TCTArchivo      = new EI_Tipo(this);
		//EI_Tipo FacArchivo      = new EI_Tipo(this);
		EI_Tipo CuentasNacional = new EI_Tipo(this);
		EI_Tipo CuentasDolares  = new EI_Tipo(this);

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Entrando a Consulta de Saldos Consolidados.", EIGlobal.NivelLog.INFO);

		if(sesionvalida)
			{
			//Variables de sesion ...
			contrato      = session.getContractNumber();
			//sucursalOpera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			usuario       = session.getUserID8();
			clavePerfil   = session.getUserProfile();

			String nombreArchivo       = IEnlace.LOCAL_TMP_DIR+"/"+ usuario+".amb";
			String nombreArchivoSalida = usuario+"MSC.doc";
			String arcLinea            = "";

			int total=0;

			//TODO  BIT CU1041
			/*
			 * 09/ENE/07
			 * VSWF-BMB-I
			 */
			 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
	        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
	        	if(req.getParameter(BitaConstants.FLUJO)!=null){
	        		bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
	        	}else{
	        		bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
	        	}
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EC_SALDO_CONS_CHEQUE_ENTRA);
				if(contrato!=null){
					bt.setContrato(contrato);
				}
				if(nombreArchivoSalida!=null){
					bt.setNombreArchivo(nombreArchivoSalida);
				}
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
			 }
			/*
			 * 02/ENE/07
			 * VSWF BMB-F
			 */
			//String cadenaTCt="";
			// LLamar al servicio para traer las cuentas.
            llamado_servicioCtasInteg (IEnlace.MConsulta_Saldos," "," "," ",session.getUserID8 ()+".ambci",req);
            total=getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+".ambci");

			//2;51500282474;GRUPO BANCA ELECTRONICA;P;1;;GRUPO BANCA ELECTRONICA                 ;M;
			//2;contrato;descripcion;tipoRelacion;cveProducto; ; @
			//EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);
			EI_Exportar ArcSal = new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivoSalida);

			String[][] arrayCuentas = ObteniendoCtasInteg (0,total,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+".ambci");
			for(int indice2 = 1;indice2<=Integer.parseInt (arrayCuentas[0][0]);indice2++){
				/*EIGlobal.mensajePorTrace("\n\n\narray campo1 "+arrayCuentas[indice2][1]+"campo2 "+arrayCuentas[indice2][2]+"campo3 "+arrayCuentas[indice2][3]+"campo4 "+arrayCuentas[indice2][4]+"campo5 "+arrayCuentas[indice2][5]+"campo6 "+arrayCuentas[indice2][6]+"campo7 "+arrayCuentas[indice2][7], EIGlobal.NivelLog.INFO);

				if( !arrayCuentas[indice2][3].equals("0") &&
					!arrayCuentas[indice2][3].equals("4") &&
					!arrayCuentas[indice2][3].equals("5") &&
					!arrayCuentas[indice2][3].equals("6") &&
					!arrayCuentas[indice2][1].substring(0,2).equals("BM") )
					EIGlobal.mensajePorTrace("Cuentas nacional\narray campo1 "+arrayCuentas[indice2][1]+"campo2 "+arrayCuentas[indice2][2]+"campo3 "+arrayCuentas[indice2][3]+"campo4 "+arrayCuentas[indice2][4]+"campo5 "+arrayCuentas[indice2][5]+"campo6 "+arrayCuentas[indice2][6]+"campo7 "+arrayCuentas[indice2][7], EIGlobal.NivelLog.INFO);*/
				cadenaTCT+="2;"+arrayCuentas[indice2][1]+";"+arrayCuentas[indice2][4]+";"+arrayCuentas[indice2][2]+";"+arrayCuentas[indice2][3]+";; ;"+arrayCuentas[indice2][6]+" @";
				//EIGlobal.mensajePorTrace("cadenaTCt "+cadenaTCT, EIGlobal.NivelLog.INFO);
			}
			//82500005708@NINGUNA@GRUPO BANCA ELECTRONICA@82@82@M@6@P@
			//2;82500005708;GRUPO BANCA ELECTRONICA;P;6;;GRUPO BANCA ELECTRONICA                 ;M @
			//2;82500005708;GRUPO BANCA ELECTRONICA;P;6;;GRUPO BANCA ELECTRONICA                 ;M @
			//2;82500005708;GRUPO BANCA ELECTRONICA;P;6;;M @
			//if(ArcEnt.abreArchivoLectura())
			EIGlobal.mensajePorTrace("cadenaTCT "+cadenaTCT, EIGlobal.NivelLog.INFO);
			if(cadenaTCT != null)
				{
				/*boolean noError=true;
				EIGlobal.mensajePorTrace("\n\nantes de do-while", EIGlobal.NivelLog.INFO);
				do
					{
					arcLinea=ArcEnt.leeLinea();
					if(!arcLinea.equals("ERROR"))
						{
						//if(arcLinea.substring(0,5).trim().equals("ERROR")) break;
						// Obtiene cuentas asociadas ...
						if(arcLinea.substring(0,1).equals("2")){
							cadenaTCT+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
							cadenaFac+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
							EIGlobal.mensajePorTrace("La cadena TCT "+cadenaTCT, EIGlobal.NivelLog.INFO);
						}
						// Obtiene facultades para cuentas ...
						if(arcLinea.substring(0,1).equals("7")){

							//EIGlobal.mensajePorTrace("La cadena cadenaFac "+cadenaFac, EIGlobal.NivelLog.INFO);
						}
						}
					else
						noError = false;
					}
				while(noError);
				ArcEnt.cierraArchivo();	*/

				// Registros de Cuentas
				TCTArchivo.iniciaObjeto(';','@',cadenaTCT);
				// Registros de facultades (7)
				//FacArchivo.iniciaObjeto(';','@',cadenaFac);

				String strTmpN = "";
				String strTmpD = "";

                EIGlobal.mensajePorTrace("obtener Numero de referencia de la operacion ", EIGlobal.NivelLog.INFO);

			    /* Inicia Modificación de Paulina
		        try
				{
					htResult = tuxGlobal.sreferencia(IEnlace.SUCURSAL_OPERANTE);
					numeroreferencia=((Integer)htResult.get( "REFERENCIA" )).intValue();
					EIGlobal.mensajePorTrace("Numero de referencia "+numeroreferencia, EIGlobal.NivelLog.INFO);

			    }
				catch(java.rmi.RemoteException re)
						{re.printStackTrace();}
				catch ( Exception e ){;}
                Termina  Modificación de Paulina
				*/



				for(int i=0;i<TCTArchivo.totalRegistros;i++)
					{
					//2;51500282474;GRUPO BANCA ELECTRONICA;P;1;;GRUPO BANCA ELECTRONICA                 ;M;
					if(!TCTArchivo.camposTabla[i][4].equals("0") &&
						!TCTArchivo.camposTabla[i][4].equals("4") &&
						!TCTArchivo.camposTabla[i][4].equals("5") &&
						!TCTArchivo.camposTabla[i][4].equals("6") &&
						!TCTArchivo.camposTabla[i][1].substring(0,2).equals("BM") &&
						!TCTArchivo.camposTabla[i][1].substring(0,2).equals("SI"))

						{


							for(int a=0;a<7;a++)
								{
								strTmpN += TCTArchivo.camposTabla[i][a];
								strTmpN += pipe;
								}
							strTmpN+="3"+pipe+TCTArchivo.camposTabla[i][3]+pipe+" @";

						}

					if(TCTArchivo.camposTabla[i][4].equals("6"))
						{

							for(int a=0;a<7;a++)
								{
								strTmpD += TCTArchivo.camposTabla[i][a];
								strTmpD += pipe;
								}
							strTmpD+="3"+pipe+TCTArchivo.camposTabla[i][3]+pipe+" @";

						}
					}
				CuentasNacional.iniciaObjeto(strTmpN);
				CuentasDolares.iniciaObjeto(strTmpD);

				if(CuentasNacional.totalRegistros>=1 || CuentasDolares.totalRegistros>=1)
					{
					if(CuentasNacional.totalRegistros>=1)
						{
						for(int i=0;i<CuentasNacional.totalRegistros;i++)
							{
							Trama=IEnlace.medioEntrega1		   + pipe;
							Trama+=session.getUserID8()		   + pipe;
							Trama+="SDCT"                      + pipe;
							Trama+=session.getContractNumber() + pipe;
							Trama+=session.getUserID8()         + pipe;
							Trama+=session.getUserProfile()    + pipe;
							Trama+=CuentasNacional.camposTabla[i][1] + pipe;
							Trama+=CuentasNacional.camposTabla[i][3] + pipe;

							EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Trama entrada (Nacional):"+Trama, EIGlobal.NivelLog.DEBUG);

							try
								{htResult = tuxGlobal.web_red( Trama );}
							catch(java.rmi.RemoteException re)
								{re.printStackTrace();}
							catch ( Exception e ){;}

                            /*  Inicio Modificacion Paulina
								try
								{htResult = tuxGlobal.tubo(numeroreferencia,CuentasNacional.camposTabla[i][1]);

			                  }
							catch(java.rmi.RemoteException re)
								{re.printStackTrace();}
							catch ( Exception e ){;}
                            Termina Modificación Paulina */

							Result = ( String ) htResult.get( "BUFFER" );

							EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Trama salida (Buffer):"+Result, EIGlobal.NivelLog.DEBUG);
					        EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Trama salida (Cod_error):"+( String ) htResult.get( "COD_ERROR" ), EIGlobal.NivelLog.DEBUG);

							if(Result.substring(0,2).equals("OK"))
								{
								try
									{
									importes = obtenImpDis(Result);
									CuentasNacional.camposTabla[i][0] = importes[0]; //sbc
									CuentasNacional.camposTabla[i][3] = importes[1]; //total
									CuentasNacional.camposTabla[i][4] = importes[2]; //disponible

									imp0 = new BigDecimal(CuentasNacional.camposTabla[i][0]);
									imp1 = new BigDecimal(CuentasNacional.camposTabla[i][3]);
									imp2 = new BigDecimal(CuentasNacional.camposTabla[i][4]);
//									imp0 = new Double(CuentasNacional.camposTabla[i][0]).doubleValue();
//									imp1 = new Double(CuentasNacional.camposTabla[i][3]).doubleValue();
//									imp2 = new Double(CuentasNacional.camposTabla[i][4]).doubleValue();
									impDisNac = impDisNac.add(imp2);
									impTotNac = impTotNac.add(imp1);
									impSBCNac = impSBCNac.add(imp0);
//									impDisNac += imp2;
//									impTotNac += imp1;
//									impSBCNac += imp0;
									}
								catch(Exception e)
									{
									CuentasNacional.camposTabla[i][0] = "";
									CuentasNacional.camposTabla[i][3] = "";
									CuentasNacional.camposTabla[i][4] = "";
									CuentasNacional.camposTabla[i][2] = "No se pudo obtener la informaci&oacute;n de esta cuenta";
									erroresNacional++;
									}
								}
							else
								{
								CuentasNacional.camposTabla[i][0] = "";
								CuentasNacional.camposTabla[i][3] = "";
								CuentasNacional.camposTabla[i][4] = "";
								CuentasNacional.camposTabla[i][2] = "No se pudo obtener la informaci&oacute;n de esta cuenta";
								erroresNacional++;
								}
							}
						EnlaceGlobal.formateaImporte(CuentasNacional,0);
						EnlaceGlobal.formateaImporte(CuentasNacional, 3);
						EnlaceGlobal.formateaImporte(CuentasNacional, 4);

						strCuentasNacional=" &nbsp; Cheques moneda Nacional<br>";
						strCuentasNacional+=CuentasNacional.generaTabla(titulos,datos,value,align,1);
						strCuentasNacional+="<tr>";
						strCuentasNacional+="<td ><br></td>";
						strCuentasNacional+="<td ><br></td>";
						strCuentasNacional+="<td class='tittabdat' align=right>Total &nbsp;&nbsp; </td>";
						strCuentasNacional+="<td class='tittabdat' align=right>"+FormatoMoneda(impDisNac.toString())+"</td>";
						strCuentasNacional+="<td class='tittabdat' align=right>"+FormatoMoneda(impSBCNac.toString())+"</td>";
						strCuentasNacional+="<td class='tittabdat' align=right>"+FormatoMoneda(impTotNac.toString())+"</td>";
						strCuentasNacional+="<tr>";
						strCuentasNacional+="</table>";
						}
					if(CuentasDolares.totalRegistros>=1)
						{
						for(int i=0;i<CuentasDolares.totalRegistros;i++)
							{
							Trama=IEnlace.medioEntrega1		        + pipe;
							Trama+=session.getUserID8()		        + pipe;
							Trama+="SDCT"                           + pipe;
							Trama+=session.getContractNumber()      + pipe;
							Trama+=session.getUserID8()              + pipe;
							Trama+=session.getUserProfile()         + pipe;
							Trama+=CuentasDolares.camposTabla[i][1] + pipe;
							Trama+=CuentasDolares.camposTabla[i][3] + pipe;

							EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Trama entrada (Dolares): "+Trama, EIGlobal.NivelLog.DEBUG);

							try
								{
								htResult = tuxGlobal.web_red( Trama );
								}
							catch (java.rmi.RemoteException re)
								{
						    	re.printStackTrace();
								}
							catch (Exception e){;}
							Result = (String)htResult.get("BUFFER");


							EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Trama salida (Dolares): "+Result, EIGlobal.NivelLog.DEBUG);

							if(Result.substring(0,2).equals("OK"))
								{
								try
									{
									importes = obtenImpDis(Result);
									CuentasDolares.camposTabla[i][0] = importes[0];
									CuentasDolares.camposTabla[i][3] = importes[1];
									CuentasDolares.camposTabla[i][4] = importes[2];



									
//									imp0=new Double(CuentasDolares.camposTabla[i][0]).doubleValue();
//									imp1=new Double(CuentasDolares.camposTabla[i][3]).doubleValue();
//									imp2=new Double(CuentasDolares.camposTabla[i][4]).doubleValue();
									imp0 = new BigDecimal(CuentasDolares.camposTabla[i][0]);
									imp1 = new BigDecimal(CuentasDolares.camposTabla[i][3]);
									imp2 = new BigDecimal(CuentasDolares.camposTabla[i][4]);

									impDisDol = impDisDol.add(imp2);
									impTotDol = impTotDol.add(imp1);
									impSBCDol = impSBCDol.add(imp0);
//									impDisDol += imp2;
//									impTotDol += imp1;
//									impSBCDol += imp0;
									}
								catch(Exception e)
									{
									CuentasDolares.camposTabla[i][0] = "";
									CuentasDolares.camposTabla[i][3] = "";
									CuentasDolares.camposTabla[i][4] = "";
									CuentasDolares.camposTabla[i][2] = "No se pudo obtener la informaci&oacute;n de esta cuenta";
									erroresDolares++;
									}
								}
							else
								{
								CuentasDolares.camposTabla[i][0] = "";
								CuentasDolares.camposTabla[i][3] = "";
								CuentasDolares.camposTabla[i][4] = "";
								CuentasDolares.camposTabla[i][2] = "No se pudo obtener la informaci&oacute;n de esta cuenta";
								erroresDolares++;
								}
							}

							EnlaceGlobal.formateaImporte(CuentasDolares,0);
							EnlaceGlobal.formateaImporte(CuentasDolares, 3);
							EnlaceGlobal.formateaImporte(CuentasDolares, 4);

							strCuentasDolares=" &nbsp; Cheques D&oacute;lares<br>";
							strCuentasDolares+=CuentasDolares.generaTabla(titulos,datos,value,align,1);
							strCuentasDolares+="<tr>";
							strCuentasDolares+="<td ><br></td>";
							strCuentasDolares+="<td ><br></td>";
							strCuentasDolares+="<td class='tittabdat' align=right>Total &nbsp;&nbsp; </td>";
							strCuentasDolares+="<td class='tittabdat' align=right>"+FormatoMoneda(impDisDol.toString())+"</td>";
							strCuentasDolares+="<td class='tittabdat' align=right>"+FormatoMoneda(impSBCDol.toString())+"</td>";
							strCuentasDolares+="<td class='tittabdat' align=right>"+FormatoMoneda(impTotDol.toString())+"</td>";
							strCuentasDolares+="<tr>";
							strCuentasDolares+="</table>";
							}
						EIGlobal.mensajePorTrace("\n\nErrores en nacional "+erroresNacional+" De "+CuentasNacional.totalRegistros+" registros\nErorres D&oacute;lares "+erroresDolares+" De "+CuentasDolares.totalRegistros+" registros", EIGlobal.NivelLog.INFO);
						/*
						if(erroresNacional<CuentasNacional.totalRegistros || erroresDolares<CuentasDolares.totalRegistros)
							{
							//exportar="Escribiendo en la bitacora";
							String bitacora = "";
							if(inicializando()==1){
								try
								{
								bitacora = llamada_bitacora(Integer.parseInt(Result.substring(8,16).trim()),Result.substring(0,8).trim(),contrato,usuario,"0",0,"SDCR","0",0," ");
								}catch(Exception e){
									e.printStackTrace();
								}
							}
							}*/

						/*else
							{
							//exportar="No se escribio en la bitacora";
							}

						coderror_bit=llamada_bitacora(
						numeroReferencia,
						"PROG0000",
						contrato,
						usuario,
						"0",
						0,
						"COPR",
						"",
						0,
						"");

						int  numeroReferencia,
						String coderror1,
						String contrato,
						String usuario,
						String cuenta,
						double importe,
						String tipo_operacion,
						String cuenta2,
						int cant_titulos,
						String dispositivo2)
						*/

						//Creando archivo de exportacion ...
						if(ArcSal.creaArchivo())
							{
							for(int i=0;i<CuentasNacional.totalRegistros;i++)
								{
								arcLinea=ArcSal.formateaCampo(CuentasNacional.camposTabla[i][1],20);
								arcLinea+=ArcSal.formateaCampo(CuentasNacional.camposTabla[i][2],20);
								arcLinea+=EnlaceGlobal.fechaHoy("ddmmaaaa");
								arcLinea+=EnlaceGlobal.fechaHoy("th:tm");		   //hm:hs
								arcLinea+=ArcSal.formateaCampoImporte(CuentasNacional.camposTabla[i][4],15);
								arcLinea+=ArcSal.formateaCampoImporte(CuentasNacional.camposTabla[i][0],15);
								arcLinea+=ArcSal.formateaCampoImporte(CuentasNacional.camposTabla[i][3],15);

								 if(!ArcSal.escribeLinea(arcLinea + "\r\n"))
									EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
								}

						for(int i=0;i<CuentasDolares.totalRegistros;i++)
							{
							arcLinea="1";
							arcLinea+=ArcSal.formateaCampo(CuentasDolares.camposTabla[i][1],20);
							arcLinea+=ArcSal.formateaCampo(CuentasDolares.camposTabla[i][2],40);
							arcLinea+=EnlaceGlobal.fechaHoy("ddmmaaaa");
							arcLinea+=EnlaceGlobal.fechaHoy("th:tm");	  //hm:hs
							arcLinea+=ArcSal.formateaCampoImporte(CuentasDolares.camposTabla[i][4],15);
							arcLinea+=ArcSal.formateaCampoImporte(CuentasDolares.camposTabla[i][0],15);
							arcLinea+=ArcSal.formateaCampoImporte(CuentasDolares.camposTabla[i][3],15);

							  if(!ArcSal.escribeLinea(arcLinea + "\r\n"))
								EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
							}

						/*************************************** Archivo remotro */
						ArchivoRemoto archR = new ArchivoRemoto();
						if(! archR.copiaLocalARemoto(nombreArchivoSalida,"WEB"))
							EIGlobal.mensajePorTrace("MSC_Inicio - execute(): No se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
						else
							{
							EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Si se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
							exportar+="\n<a href='/Download/"+nombreArchivoSalida+"'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
							}
						//exportar+="\n<a href='/Download/"+nombreArchivoSalida+"'><img border=0 src='/gifs/EnlaceInternet/gbo25230.gif' alt='Exportar'></a>";
						/*************************************** Archivo remotro */
						}
					ArcSal.cierraArchivo();

					/*
					exportar+="<br>"+ArcSal.formateaCampoImporte("$ 50,000.0 ",15);
					exportar+="<br>"+ArcSal.formateaCampoImporte("$ 50,000.20 ",15);
					exportar+="<br>"+ArcSal.formateaCampoImporte("$ 50,000 ",15);
					*/


/**************************************************************************************************/
/**/																							/**/
/**/	//OpcionRv   1 = NM   2 = Dolar															/**/
/**/	String parOpcionRv = (String)req.getParameter("OpcionRv");								/**/
/**/	if(parOpcionRv != null)																	/**/
/**/		{																					/**/
/**/		String parNumCuentas = (String)req.getParameter("NumCuentas");						/**/
/**/		String parCtasXpag = (String)req.getParameter("CtasXpag");							/**/
/**/		String parOffset = (String)req.getParameter("Offset");								/**/
/**/																							/**/
/**/		if(parNumCuentas == null) parNumCuentas = "";										/**/
/**/		if(parCtasXpag == null) parCtasXpag = "";											/**/
/**/		if(parOffset == null) parOffset = "";												/**/
/**/																							/**/
/**/		int a, b, c;																		/**/
/**/		String codBruto, cabecera, pie;														/**/
/**/		Vector lineasHtml;																	/**/
/**/																							/**/
/**/		codBruto = (parOpcionRv.equals("1"))?strCuentasNacional:strCuentasDolares;			/**/
/**/		strCuentasNacional = ""; strCuentasDolares = "";									/**/
/**/		a = codBruto.indexOf("<tr><td class='textabda");									/**/
/**/		b = codBruto.indexOf("<tr><td ><br></td><td ><br></td>");							/**/
/**/		cabecera = codBruto.substring(0,a);													/**/
/**/		pie = codBruto.substring(b);														/**/
/**/		codBruto = codBruto.substring(a,b);													/**/
/**/		lineasHtml = new Vector();															/**/
/**/		while(codBruto.indexOf("</tr>") != -1)												/**/
/**/			{																				/**/
/**/			a = codBruto.indexOf("<tr><td class='textabda");								/**/
/**/			b = codBruto.indexOf("</tr>") + 5;												/**/
/**/			lineasHtml.add(codBruto.substring(a,b));										/**/
/**/			codBruto = codBruto.substring(b);												/**/
/**/			}																				/**/
/**/		a = Integer.parseInt(parOffset);													/**/
/**/		b = Global.NUM_REGISTROS_PAGINA;													/**/
/**/		codBruto = cabecera;																/**/
/**/		for(c=a;c<a+b && c<lineasHtml.size();c++) codBruto += lineasHtml.get(c);			/**/
/**/		codBruto += ((a+b >=lineasHtml.size())?pie:"</table>");								/**/
/**/																							/**/
/**/		req.setAttribute("OpcionRv",parOpcionRv);											/**/
/**/		req.setAttribute("NumCuentas","" + lineasHtml.size());								/**/
/**/		req.setAttribute("CtasXpag","" + b);												/**/
/**/		req.setAttribute("Offset",parOffset);												/**/
/**/		req.setAttribute("strCuentas",codBruto);											/**/
/**/		}																					/**/
/**/																							/**/
/**************************************************************************************************/

               /* Inicia  Modificacion Paulina
			    try
				{
					htResult = tuxGlobal.sbitacora(numeroreferencia,"SALDO0000",session.getContractNumber(),session.getUserID(),"X2",0.0,
								"SDCT","X",0,"X");
					coderror = ( String ) htResult.get ( "COD_ERROR" );

			    }
				catch(java.rmi.RemoteException re)
						{re.printStackTrace();}
				catch ( Exception e ){;}

				Termina Modificación Paulina
				*/



					req.setAttribute("NumContrato",session.getContractNumber());
					req.setAttribute("NomContrato",session.getNombreContrato());
					req.setAttribute("NumUsuario",session.getUserID8());
					req.setAttribute("NomUsuario",session.getNombreUsuario());
					req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));

					req.setAttribute("Exportar",exportar);
					req.setAttribute("SaldoNacional",FormatoMoneda(impDisNac.toString()));
					req.setAttribute("SaldoDolares",FormatoMoneda(impDisDol.toString()));

					req.setAttribute("strCuentasNacional",strCuentasNacional);
					req.setAttribute("strCuentasDolares",strCuentasDolares);
					//req.setAttribute("FacArchivo",FacArchivo.strOriginal);

					req.setAttribute("MenuPrincipal", session.getStrMenu());
					req.setAttribute("newMenu", session.getFuncionesDeMenu());
					req.setAttribute("Encabezado",CreaEncabezado("Saldos Consolidados","Consultas &gt; Saldos &gt Consolidados &gt Cheques","s25080sch",req));
					//TODO  BIT CU1041
					/*
					 * VSWF-BMB-I
					 */
					 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
						BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EC_SALDO_CONS_CHEQUE_CONS_SALDO_EFECTIVO);
						if(session.getContractNumber()!=null){
							bt.setContrato(session.getContractNumber());
						}
						if(Result!=null){
			    			if(Result.substring(0,2).equals("OK")){
			    				bt.setIdErr("SDCT0000");
			    			}else if(Result.length()>8){
								bt.setIdErr(Result.substring(0,8));
							}else{
								bt.setIdErr(Result.trim());
							}
						}
						bt.setServTransTux("SDCT");
						if(nombreArchivoSalida!=null){
							bt.setNombreArchivo(nombreArchivoSalida);
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					}catch(SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
					 }
					/*
					 * VSWF-BMB-F
					 */

					evalTemplate( "/jsp/MSC_Inicio.jsp", req, res );
					}
				else
					despliegaPaginaError("No existen cuentas asociadas.","Saldos Consolidados","Consultas &gt; Saldos &gt Consolidados &gt Cheques","s25080sch", req, res);

				}
			else
				despliegaPaginaError("Saldos Consolidados","Consultas &gt; Saldos &gt Consolidados &gt Cheques","Su transacci&oacute;n no puede ser atendida en este momento.","s25080sch", req, res);
			}
		else
			{
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
			}
		EIGlobal.mensajePorTrace("MSC_Inicio - execute(): Saliendo de Consulta de Saldos.", EIGlobal.NivelLog.INFO);
		}

	/*************************************************************************************/
	/****************************** formatea Resultado      ******************************/
	/*************************************************************************************/
	String formateaResult(String Result)
		{
		String formateado="";

		float caut=0;
		float atot=0;
		float cdis=0;

		if(Result.length()>=70) if(Result.substring(0,7).trim().equals("OK"))
			{
			formateado+=Result.substring(8,16).trim()+pipe;
			formateado+=Result.substring(16,30).trim()+pipe;
			formateado+=Result.substring(30,44).trim()+pipe;
			formateado+=Result.substring(44,58).trim()+pipe;
			formateado+=Result.substring(58,69).trim()+pipe;
			formateado+=Result.substring(69,Result.length()).trim()+pipe;
			caut=new Float(Result.substring(69,Result.length()).trim()).floatValue();
			atot=new Float(Result.substring(16,30).trim()).floatValue();
			cdis=caut-atot;
			formateado+=Float.toString(cdis)+"@";

			return formateado;
			}
		return "ERROR";
		}

	/*************************************************************************************/
	/*************************************************** formatea Reultado para Linea    */
	/*************************************************************************************/
	String formateaResultLinea(String usuario)
		{

		String arcLinea="";
		String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

		// **********************************************************  Copia Remota
		ArchivoRemoto archR = new ArchivoRemoto();
		if(!archR.copia(usuario))
			EIGlobal.mensajePorTrace("MSC_Inicio - formateaResultLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
		// **********************************************************

		EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);

		if(ArcEnt.abreArchivoLectura())
			{
			boolean noError=true;
			arcLinea=ArcEnt.leeLinea();
			if(arcLinea.substring(0,2).equals("OK"))
				{
				arcLinea=ArcEnt.leeLinea();
				if(arcLinea.equals("ERROR")) return "OPENFAIL";
				}
			 ArcEnt.cierraArchivo();
			}
		else
			return "OPENFAIL";
		return arcLinea;

		//########## Cambiar ....
		}

	String[] obtenImpDis(String Result)
	 {
		/*
			0 -> importe SBC
			1 -> total
			2 -> disponible
		*/

		String[] impTemp={"0.00","0.00","0.00"};

		if(Result.length()>=58)
		 {
			impTemp[2]=Result.substring(16,30).trim();  //disponible
			impTemp[1]=Result.substring(44,58).trim();  //total
			impTemp[0]=Result.substring(30,44).trim();  //sbc
		 }
		return impTemp;
	}
}