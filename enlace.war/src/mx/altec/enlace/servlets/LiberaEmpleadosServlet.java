package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.DetalleCuentasBean;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.DetalleCuentasServicio;
import mx.altec.enlace.utilerias.DetalleCuentasException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Servlet implementation class LiberaEmpleadosServlet
 */
public class LiberaEmpleadosServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final String PARAM_MENSAJE = "msg";
	private static final String CODIGO_ERROR_ALTA_ARCHIVO = "NOAL9999";
	private static final String CODIGO_EXITO_ALTA_ARCHIVO = "NOAL0000";
	private static final String LISTA_DTE_ARCHIVOS = "listaArchivos";
	private static final String LISTA_DTE_CUENTAS = "listaCuentas";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LiberaEmpleadosServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	protected void defaultAction(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		DetalleCuentasServicio servicio = new DetalleCuentasServicio();

		EIGlobal.mensajePorTrace("Entrando a validar Session LiberaEmpleadosServlet.class",EIGlobal.NivelLog.INFO);
		HttpSession sessionHttp = request.getSession();
		/**/
		BaseResource session = (BaseResource) sessionHttp.getAttribute("session");
		boolean sesionvalida=SesionValida( request, response );
		//request.removeAttribute(PARAM_MENSAJE);
		if(sesionvalida)
		{

				try {
					request.setAttribute ("MenuPrincipal", session.getStrMenu ());
					request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
				}
				catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

				request.setAttribute ("Encabezado", CreaEncabezado ( "Liberaci&oacute;n de empleados", "Servicios > N&oacute;mina > " +
						"Cat&aacute;logo de N&oacute;mina > Liberaci&oacute;n de empleados no registrados", "s37120h", request ) );


				String opcion = request.getParameter("opcion").toString();
				String contrato = session.getContractNumber();
				List<DetalleCuentasBean> listaCtas = null;
				List<DetalleCuentasBean> listaArchivos = null;
				int folio = 0;
				//Valores para el registro de Cuentas.
				if(opcion.equals("1")){
					EIGlobal.mensajePorTrace("ENTRA OPCION 1 LIBERAR EMPLEADOS", EIGlobal.NivelLog.INFO);
					try {
						request.getSession().setAttribute("cuentasCatNom", null);
						listaArchivos = servicio.consultarArchivosCuentasNoRegistradas(contrato);
						if(listaArchivos != null && listaArchivos.size() > 0){
							EIGlobal.mensajePorTrace("LISTA ARCHIVOS CON VARIOS REGISTROS", EIGlobal.NivelLog.INFO);
							sessionHttp.setAttribute(LISTA_DTE_ARCHIVOS, listaArchivos);
							evalTemplate("/jsp/lenrListadoArchivos.jsp", request, response );
						}/*else if(listaArchivos != null && listaArchivos.size() == 1){
							EIGlobal.mensajePorTrace("LISTA ARCHIVOS CON UN SOLO ARCHIVO", EIGlobal.NivelLog.INFO);
							sessionHttp.setAttribute("folio", listaArchivos.get(0).getFolio());
							listaCtas = servicio.consultarCuentasDeFolio(contrato, listaArchivos.get(0).getFolio());
							sessionHttp.setAttribute(LISTA_DTE_CUENTAS, listaCtas);
							request.setAttribute("contrato", contrato);
							evalTemplate("/jsp/lenrListadoCuentas.jsp", request, response );
						}*/else{
							sessionHttp.setAttribute(LISTA_DTE_ARCHIVOS, listaArchivos);
							evalTemplate("/jsp/lenrListadoArchivos.jsp", request, response );
						}
					}catch (DetalleCuentasException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}

				}else if(opcion.equals("2")){
					folio = Integer.parseInt(request.getParameter("radioValue").toString());
					sessionHttp.setAttribute("folio", folio);
					EIGlobal.mensajePorTrace("Entra DESPUES DE LISTA DE ARCHIVOS", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("Folio del Archivo: " + folio, EIGlobal.NivelLog.INFO);
					try {
						listaCtas = servicio.consultarCuentasDeFolio(contrato, folio);
						sessionHttp.setAttribute(LISTA_DTE_CUENTAS, listaCtas);
						request.setAttribute("contrato", contrato);
						evalTemplate("/jsp/lenrListadoCuentas.jsp", request, response );
					} catch (DetalleCuentasException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
				}else if(opcion.equals("3")){
					if(verificaFacultad ( "INOMENVIEMP" ,request)){
						try{
							folio = Integer.parseInt(sessionHttp.getAttribute("folio").toString());
							validandoToken(request, response,contrato,folio,session);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							enviarMensaje(request,response,"Se produjo un error al procesar la petici&oacute;n");
						}
					}else{
						request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(request, response);
					}
				}

		}
	}

	private List<String> obtenerCuentas(String [] cuentas){
		List<String> cuenta = new ArrayList<String>();
		for(String cta : cuentas){
			cuenta.add(cta);
		}
		return cuenta;
	}

	private void imprimirCuentas(String [] cuentas){
		for(String cta : cuentas){
			EIGlobal.mensajePorTrace("Cuenta: " + cta, EIGlobal.NivelLog.INFO);
		}
	}

	private void validandoToken(HttpServletRequest request, HttpServletResponse response,String contrato,int folio,BaseResource session) throws Exception, IOException{
			String valida = request.getParameter("valida");
			EIGlobal.mensajePorTrace("VALIDA LIBERAR EMPLEADOS: " + valida,  EIGlobal.NivelLog.INFO);
			/*
			 * valida es null cuando apenas se va a pedir el token
			 * */
			String [] cuentas = null;

			if (valida == null){
				EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
				boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);
				//CSA-Vulnerabilidad  Token Bloqueado-Inicio
	            int estatusToken = obtenerEstatusToken(session.getToken());
	            
				if ((session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP)
					&& estatusToken == 1 && solVal)

					|| true

						){ //Si queremos que valide el token
					EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
					ValidaOTP.guardaParametrosEnSession(request);
					ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP);
					cuentas = request.getParameterValues("chkCuentas");
					request.getSession().setAttribute("cuentasCatNom", cuentas);
				}else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP)
						&& estatusToken == 2 && solVal){                        
	            	cierraSesionTokenBloqueado(session,request,response);
				}
				//CSA-Vulnerabilidad  Token Bloqueado-Fin
				else{//No queremos que valide el token
					valida = "1";
				}
			}

			if (valida != null && valida.equals("1")){

				cuentas = (String [])request.getSession().getAttribute("cuentasCatNom");
				if(cuentas == null)
					cuentas = request.getParameterValues("chkCuentas");

				imprimirCuentas(cuentas);
				EIGlobal.mensajePorTrace("Entra a Liberacion Cuentas no Registradas", EIGlobal.NivelLog.INFO);
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				BitaTCTBean bean = new BitaTCTBean();
				int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
				DetalleCuentasServicio servicio =  new DetalleCuentasServicio();
				servicio.registraCuentasLiberacion(contrato, folio, obtenerCuentas(cuentas), bean,bh,referencia);
				enviarMensaje(request, response, "Se registraron exitosamente "+cuentas.length+" registros");
				EIGlobal.mensajePorTrace("SALIENDO DE INSERTAR CUENTAS NO REGISTRADAS", EIGlobal.NivelLog.INFO);
			}
	}

	private void enviarMensaje(HttpServletRequest request,
			HttpServletResponse response,String mensaje){
		try{
			request.setAttribute(PARAM_MENSAJE, mensaje);
			//evalTemplate("/Enlace/enlaceMig/LiberaEmpleadosServlet?opcion=1", request, response );
			request.getRequestDispatcher("/enlaceMig/LiberaEmpleadosServlet?opcion=1").forward(request, response);
		} catch (Exception e) {
			try {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (Exception ex) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
			}
		}
	}
}