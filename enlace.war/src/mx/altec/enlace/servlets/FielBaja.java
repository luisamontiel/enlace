/** 
*   Isban Mexico
*   Clase: FielBaja.java
*   Descripcion: Servlet que gestiona el flujo de Baja de fiel.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.BeanAdminFiel;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;
import mx.altec.enlace.utilerias.FielUtils;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.UtilAdminFIEL;

/**
 * Servlet que gestiona el flujo de Baja de fiel.
 *
 * @author FSW Everis.
 */
public class FielBaja extends BaseServlet {

    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = -4368235704320108670L;
    /** Titulo del encabezado de la pagina. */
    private static final String PAG_ENCAB_TITULO = "Baja de FIEL";
    /** Ruta base del encabezado de la pagina. */
    private static final String PAG_ENCAB_RUTA = "Administraci&oacute;n y control &gt; FIEL &gt; Baja";
    /** Cadena base de registro en Log. */
    private static final String LOG_MODULO = "BajaFiel - ";
        

    /**
     * Metodo que responde a peticiones GET
     *
     * @param request objeto inherente a la peticion
     * @param response objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        defaultAction(request, response);
    }

    /**
     * Metodo que responde a peticiones POST
     *
     * @param request objeto inherente a la peticion
     * @param response objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        defaultAction(request, response);
    }

    /**
     * Metodo que atiende peticiones del flujo
     *
     * @param req objeto inherente a la peticion
     * @param res objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        EIGlobal.mensajePorTrace("defaultAction - Inicia FielBaja", EIGlobal.NivelLog.INFO);
        BaseResource attrSession = (BaseResource) req.getSession().getAttribute("session");
        String titulo = FielConstants.CADENA_VACIA;
        String pagResultado = FielConstants.JSP_FIEL_BAJA_INIT;
        
        if (SesionValida(req, res)) {
			attrSession.setModuloConsultar(IEnlace.MDep_inter_cargo); // Limpiar modulo de busueda de cuentas
            String valida = FielUtils.validaRecToken(req, this);

            if (validaPeticion(req, res, attrSession, req.getSession(), valida)) {
                EIGlobal.mensajePorTrace("\n\nENTRO A VALIDAR LA PETICION\n\n", EIGlobal.NivelLog.INFO);
            } else {
                String mod = getFormParameter(req, FielConstants.PARAM_MODULO);
                EIGlobal.mensajePorTrace(LOG_MODULO + "Modulo - " + mod, EIGlobal.NivelLog.INFO);
                if (mod == null || mod.isEmpty()) {
                    mod = FielConstants.INICIA_REGISTRO;
                }

                switch (mod.charAt(FielConstants.CERO_INT)) {
                    case FielConstants.UNO_CHAR:
                        EIGlobal.mensajePorTrace(LOG_MODULO.concat(FielConstants.ACCION_CONFIRMAR), EIGlobal.NivelLog.INFO);
                        pagResultado = validacionBaja( req, res, attrSession, valida);
                        titulo = null == pagResultado || pagResultado.isEmpty() 
                                ? FielConstants.CADENA_VACIA
                                : null != pagResultado && !pagResultado.isEmpty() && FielConstants.JSP_FIEL_RESULTADO.equals(pagResultado) 
                                    ? FielConstants.ACCION_RESULTADO 
                                    : FielConstants.ACCION_CONFIRMAR;
                        break;
                    case FielConstants.DOS_CHAR:
                        EIGlobal.mensajePorTrace(LOG_MODULO.concat(FielConstants.ACCION_RESULTADO), EIGlobal.NivelLog.INFO);
                        titulo = FielConstants.ACCION_RESULTADO;
                        pagResultado = FielConstants.JSP_FIEL_RESULTADO;
                        break;
                    case FielConstants.CUATRO_CHAR:
                        EIGlobal.mensajePorTrace(LOG_MODULO + "Imprimir comprobante", EIGlobal.NivelLog.INFO);
                        req.setAttribute(FielConstants.PARAM_FIEL_RESULT,
                                req.getSession().getAttribute(FielConstants.PARAM_FIEL_RESULT));
                        pagResultado = FielConstants.JSP_FIEL_COMPROBANTE;
                        break;
                    default:
                        EIGlobal.mensajePorTrace(LOG_MODULO, EIGlobal.NivelLog.INFO);
                        req.getSession().removeAttribute(FielConstants.PARAM_FIEL_RESULT);
                        FielUtils.grabaPistas(req, attrSession, FielConstants.BITA_FIEL_BAJA_ENTRA,
                                null, null, null, Boolean.FALSE, null);
                        break;
                }
                EIGlobal.mensajePorTrace(LOG_MODULO.concat(", pagResultado|").concat(pagResultado)
                        .concat(", Modulo|" + mod.charAt(0)), EIGlobal.NivelLog.INFO);
                
                if (null != pagResultado && !pagResultado.isEmpty()) {
					if(FielConstants.JSP_FIEL_RESULTADO.equals(pagResultado)){
                        titulo = FielConstants.ACCION_RESULTADO;
                    }else if(FielConstants.JSP_FIEL_BAJA_INIT.equals(pagResultado)){
                        titulo = FielConstants.CADENA_VACIA;
                    }
                    FielUtils.cargaEncabezadoPagina(req, this, attrSession, 
                                    PAG_ENCAB_TITULO, PAG_ENCAB_RUTA.concat(titulo), 
                                    FielConstants.AYUDA_FIEL_BAJA);
                	evalTemplate(pagResultado, req, res);	
				}
            }
        }
        
        
        EIGlobal.mensajePorTrace("defaultAction - Termina FielBaja", EIGlobal.NivelLog.INFO);
    }


    /**
     * Iniicializa la pagina de baja de FIEL.
     * @param req HTTP request
     * @param res HTTP response
     * @param attrSession sesion interna de enlace
     * @param valida bandera del resultado de la validacion
     * @return la pagina donde se redireccionara
     * @throws IOException en caso de error
     */
    private String validacionBaja(HttpServletRequest req, HttpServletResponse res,
            BaseResource attrSession, String valida) throws IOException {
        
        HttpSession ses = req.getSession();
        String paginaRetorno = FielConstants.JSP_FIEL_BAJA_INIT;
        String cuenta = getFormParameter(req, FielConstants.PARAM_CBO_CTA);
        if (cuenta != null && cuenta.indexOf(FielConstants.PIPE_CHAR) >= FielConstants.CERO_INT) {
        	cuenta = cuenta.substring(FielConstants.CERO_INT, cuenta.indexOf(FielConstants.PIPE_CHAR));
		}
        
        BeanAdminFiel detalleFiel = new BeanAdminFiel();
        detalleFiel.setCuenta(cuenta);
        detalleFiel.setIndicadorOperacion(FielConstants.BAJA_DOCUMENTO);
        detalleFiel  = UtilAdminFIEL.consultaFielTrx(detalleFiel);
        
        EIGlobal.mensajePorTrace(LOG_MODULO + "IN> Cuenta = " + cuenta, EIGlobal.NivelLog.INFO);
        if (FielConstants.COD_ERRROR_OK.equals(detalleFiel.getCodError())) {
            consultaNumeroDoc(detalleFiel, req, ses);
            
            if (FielUtils.validaRSA(req, res, req.getSession(), this, "/enlaceMig/FielBaja")) {
                paginaRetorno = null;
            } else {
                EIGlobal.mensajePorTrace("\n\n\n ANTES DE VERIFICACION DE TOKEN \n\n\n", EIGlobal.NivelLog.INFO);
                if (valida == null) {
                    EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: valida es null", EIGlobal.NivelLog.DEBUG);
                    boolean solVal = ValidaOTP.solicitaValidacion(attrSession.getContractNumber(),
                            IEnlace.FIEL_OTP_BAJA);
                    if (solVal && attrSession.getValidarToken()
                            && attrSession.getFacultad(BaseResource.FAC_VAL_OTP)
                            && attrSession.getToken().getStatus() == FielConstants.UNO_INT) {
                        paginaRetorno = FielUtils.validaToken(req, res, attrSession,
						        this, paginaRetorno,PAG_ENCAB_TITULO,
                                PAG_ENCAB_RUTA.concat(FielConstants.ACCION_CONFIRMAR),
                                FielConstants.AYUDA_FIEL_BAJA,
                                FielConstants.CONTEXTO_ENLACE + Global.WEB_APPLICATION
                                        + "/FielBaja?modulo=0&" + BitaConstants.FLUJO
                                        + FielConstants.IGUAL + FielConstants.BITA_FIEL_ALTA,
                                FielConstants.PREGUNTA_ABRE 
                                        + FielConstants.MSG_BAJA_001 + cuenta
                                        + FielConstants.PREGUNTA_CIERRA);
                    } else {
                        EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: valida NO es null", EIGlobal.NivelLog.DEBUG);
                        ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado.");
                        valida = FielConstants.UNO_STR;
                    }
                }
                
                EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: continua Valida >" + valida, EIGlobal.NivelLog.INFO);
                if (valida != null && FielConstants.UNO_STR.equals(valida)) {
                	detalleFiel = UtilAdminFIEL.administraFiel(detalleFiel);
                    int referencia = obten_referencia_operacion(
                            Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
                    detalleFiel.setReferencia(String.valueOf(referencia));
                    FielUtils.realizarOperaciones(req, attrSession, 
                            referencia, cuenta,
                            FielConstants.BITA_FIEL_BAJA_EJEC, 
                            FielConstants.BITA_FIEL_BAJA,
                            detalleFiel.getCodError(),
                            "Baja", Description(FielConstants.BITA_FIEL_BAJA));
                	if (!FielConstants.COD_ERRROR_OK.equals(detalleFiel.getCodError())) {
                		detalleFiel.setMsgError(FielConstants.MSG_ERROR_001);
                		EIGlobal.mensajePorTrace(LOG_MODULO + "modificaDocumentosElectronicos error", EIGlobal.NivelLog.INFO);		
                	}
                    paginaRetorno = FielConstants.JSP_FIEL_RESULTADO;
                    
                    if(null == req.getSession().getAttribute(FielConstants.PARAM_FIEL_RESULT)){
                        
                        req.getSession().setAttribute(FielConstants.PARAM_FIEL_RESULT, detalleFiel);
                    }
                    
                    
                } else {
                    EIGlobal.mensajePorTrace(LOG_MODULO + "NO RETOMA EL FLUJO", EIGlobal.NivelLog.INFO);
                }
            }
        } else {
        	req.setAttribute(FielConstants.ETIQUETA_MSG_ERROR, FielConstants.MSG_BAJA_002);
        	paginaRetorno = FielConstants.JSP_FIEL_BAJA_INIT;
        }
        req.setAttribute(FielConstants.ETIQUETA_NOMBRE_MODULO_FIEL, FielConstants.VALOR_NOMBRE_MODULO_BAJA_FIEL);
        
        return paginaRetorno;
    }
    
    /**
     * Metodo para obtener la Fiel en Transaccion 390
     * @param req objeto inherente a la peticion
     * @param ses objeto de sesion
     */
    private void consultaNumeroDoc(BeanAdminFiel detalleFiel, HttpServletRequest req, HttpSession ses){
        EIGlobal.mensajePorTrace("Entra a consultaNumeroDoc" + detalleFiel.getFiel() , EIGlobal.NivelLog.INFO);
                detalleFiel = UtilAdminFIEL.buscaFiel(detalleFiel);
                
                if(!FielConstants.COD_ERRROR_OK.equals(detalleFiel.getCodError())){
                    EIGlobal.mensajePorTrace(LOG_MODULO + "error al consultar la FIEL en el WS", EIGlobal.NivelLog.INFO);
                    }
                }
    

}