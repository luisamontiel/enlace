package mx.altec.enlace.nuevopagoimpuestos;

import java.io.*;
import java.util.*;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;


public class PagoImpTop extends BaseServlet
{

	public void doGet( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		defaultAction( req, res );
	}



	public void doPost( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		defaultAction( req, res );
	}



	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		presentaApplet(req,res);
	}


	public void presentaApplet( HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException,java.io.IOException
	{
		evalTemplate("/jsp/nuevopagoimpuestos/pagoimptop.jsp", req, res );
		EIGlobal.mensajePorTrace( "***InterApplet.class & Saliendo de presentaApplet ", EIGlobal.NivelLog.INFO);
	}

}