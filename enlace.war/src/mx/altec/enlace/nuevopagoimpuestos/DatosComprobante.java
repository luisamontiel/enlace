package mx.altec.enlace.nuevopagoimpuestos;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;

import java.io.*;
import java.util.*;
public class DatosComprobante extends BaseServlet
{

	public void doGet( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		boolean sesionvalida = SesionValida( req, res );

		if (sesionvalida)
		{

		} else {
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.MSG_PAG_NO_DISP,req,res);
		}
	}

}