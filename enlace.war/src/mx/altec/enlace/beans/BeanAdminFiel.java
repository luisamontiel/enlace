/** 
*   Isban Mexico
*   Clase: BeanAdminFiel.java
*   Descripcion: Objeto de tranferencia de informacion de la FIEL
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.beans;

import java.io.Serializable;
import mx.altec.enlace.cliente.ws.etransfernal.BeanResConsultaCertificado;
import mx.altec.enlace.utilerias.FielConstants;

/**
 * DTO BeanAdminFiel.
 *
 * @author FSW Everis
 */
public class BeanAdminFiel implements Serializable {

    /** Serial Version Universal ID. */
    private static final long serialVersionUID = 5408761788068826387L;
	
	/**  DTO de respuesta. */
	private BeanResConsultaCertificado detalleConsulta;

	/**  RFC de la cuenta 390. */
	private String rfc = FielConstants.CADENA_VACIA;
	
	/**  Cuenta asociada. */
	private String cuenta = FielConstants.CADENA_VACIA;
	
	/**  firma electronica. */
	private String fiel = FielConstants.CADENA_VACIA;
	
    /** La variable num cliente. */
	private String numCliente = FielConstants.CADENA_VACIA;
	
    /** La variable indicador operacion. */
	private String indicadorOperacion = FielConstants.CADENA_VACIA;
    
    /** La variable secuencia documento. */
	private String secuenciaDocumento = FielConstants.CADENA_VACIA;
	
	/**  Numero de referencia. */
	private String referencia = FielConstants.CADENA_VACIA;
	
	/**  Codigo de  error. */
	private String codError;
	
	/**  Mensaje de error. */
	private String msgError;


	/**
	 * Contructor de bean admin fiel.
	 */
	public BeanAdminFiel(){
		super();
		this.detalleConsulta = new BeanResConsultaCertificado();
		this.codError = FielConstants.EMPTY_STRING;
		this.msgError = FielConstants.EMPTY_STRING;
	}

	/**
	 * Obtiene el valor de la variable rfc.
	 *
	 * @return el rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * Coloca el valor de rfc.
	 *
	 * @param rfc es el nuevo valor de rfc
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * Obtiene el valor de la variable cuenta.
	 *
	 * @return el cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * Coloca el valor de cuenta.
	 *
	 * @param cuenta es el nuevo valor de cuenta
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * Obtiene el valor de la variable cod error.
	 *
	 * @return el cod error
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * Coloca el valor de cod error.
	 *
	 * @param codError es el nuevo valor de cod error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * Obtiene el valor de la variable msg error.
	 *
	 * @return el msg error
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * Coloca el valor de msg error.
	 *
	 * @param msgError es el nuevo valor de msg error
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * Obtiene el valor de la variable fiel.
	 *
	 * @return el fiel
	 */
	public String getFiel() {
		return fiel;
	}

	/**
	 * Coloca el valor de fiel.
	 *
	 * @param fiel es el nuevo valor de fiel
	 */
	public void setFiel(String fiel) {
		this.fiel = fiel;
	}

	/**
	 * Obtiene el valor de la variable detalle consulta.
	 *
	 * @return el detalle consulta
	 */
	public BeanResConsultaCertificado getDetalleConsulta() {
		return detalleConsulta;
	}

	/**
	 * Coloca el valor de detalle consulta.
	 *
	 * @param detalleConsulta es el nuevo valor de detalle consulta
	 */
	public void setDetalleConsulta(BeanResConsultaCertificado detalleConsulta) {
		this.detalleConsulta = detalleConsulta;
	}

	/**
	 * Obtiene el valor de la variable indicador operacion.
	 *
	 * @return el indicador operacion
	 */
	public String getIndicadorOperacion() {
		return indicadorOperacion;
	}

	/**
	 * Coloca el valor de indicador operacion.
	 *
	 * @param indicadorOperacion es el nuevo valor de indicador operacion
	 */
	public void setIndicadorOperacion(String indicadorOperacion) {
		this.indicadorOperacion = indicadorOperacion;
	}

	/**
	 * Obtiene el valor de la variable secuencia documento.
	 *
	 * @return el secuencia documento
	 */
	public String getSecuenciaDocumento() {
		return secuenciaDocumento;
	}

	/**
	 * Coloca el valor de secuencia documento.
	 *
	 * @param secuenciaDocumento es el nuevo valor de secuencia documento
	 */
	public void setSecuenciaDocumento(String secuenciaDocumento) {
		this.secuenciaDocumento = secuenciaDocumento;
	}

	/**
	 * Obtiene el valor de la variable num cliente.
	 *
	 * @return el num cliente
	 */
	public String getNumCliente() {
		return numCliente;
	}

	/**
	 * Coloca el valor de num cliente.
	 *
	 * @param numCliente es el nuevo valor de num cliente
	 */
	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}
	
	/**
	 * Obtiene el valor de la variable referencia.
	 *
	 * @return el referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	
	/**
	 * Coloca el valor de referencia.
	 *
	 * @param referencia es el nuevo valor de referencia
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
}
