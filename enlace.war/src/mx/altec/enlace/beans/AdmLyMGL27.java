package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;


import java.io.Serializable;
import java.util.ArrayList;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Util;

public class AdmLyMGL27 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL2700531123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCGLM271  P";
	private static final String FORMATO_HEADER_DET = "@DCGLM272  P";
	
	private static final AdmLyMGL27Factory FACTORY = new AdmLyMGL27Factory();
	
	private ArrayList<LimitesOperacion> limites;
	
	public static AdmLyMGL27Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmLyMGL27Factory implements AdmUsrBuilder<AdmLyMGL27> {

		public AdmLyMGL27 build(String arg) {
			
			AdmLyMGL27 bean = new AdmLyMGL27();
			ArrayList<LimitesOperacion> limList = new ArrayList<LimitesOperacion>();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL270000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);							

				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						int numRegs = Integer.parseInt(getValor(arg, index , 3));
						EIGlobal.mensajePorTrace("==> numRegs: [" + numRegs + "]", EIGlobal.NivelLog.INFO);
						if (numRegs > 0) {
							index = arg.indexOf(FORMATO_HEADER_DET);
							if (index != -1) {
								EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
								for (int i = 0; i < numRegs; i ++) { // lee el detalle
									index += FORMATO_HEADER_DET.length();
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
									LimitesOperacion loBean = new LimitesOperacion();
									loBean.setContrato(getValor(arg, index, 11));
									EIGlobal.mensajePorTrace("==> contrato: [" + loBean.getContrato() + "]", EIGlobal.NivelLog.INFO);
									loBean.setUsuario(getValor(arg, index += 11, 8));
									EIGlobal.mensajePorTrace("==> usuario: [" + loBean.getUsuario() + "]", EIGlobal.NivelLog.INFO);
									loBean.setClaveOperacion(getValor(arg, index += 8, 2));
									EIGlobal.mensajePorTrace("==> clave operacion: [" + loBean.getClaveOperacion() + "]", EIGlobal.NivelLog.INFO);
									loBean.setDescripcionOperacion(getValor(arg, index += 2, 40));
									EIGlobal.mensajePorTrace("==> desc operacion: [" + loBean.getDescripcionOperacion() + "]", EIGlobal.NivelLog.INFO);
									loBean.setLimiteOperacion(Util.formateaImporte(getValor(arg, index += 40, 17)));
									EIGlobal.mensajePorTrace("==> limite operacion: [" + loBean.getLimiteOperacion() + "]", EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
									index += 18;
									limList.add(loBean);
								}
							}
						}
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmLyMGL27 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setLimites(limList);
			return bean;
		}
		
	}

	public ArrayList<LimitesOperacion> getLimites() {
		return limites;
	}

	public void setLimites(ArrayList<LimitesOperacion> limites) {
		this.limites = limites;
	}

}
