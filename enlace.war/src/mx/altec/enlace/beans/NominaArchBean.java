package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * The Class NominaArchBean.
 */
public class NominaArchBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The fecha carga. */
	private String fechaCarga;
	
	/** The contrato. */
	private String contrato;
	
	/** The nombre archivo. */
	private String nombreArchivo;
	
	/** The num registros. */
	private String numRegistros;
	
	/** The importe. */
	private String importe;
	
	/** The estatus carga. */
	private String estatusCarga;
	
	/** The biatux origen. */
	private String biatuxOrigen;
	
	/** The tipo archivo. */
	private String tipoArchivo;
	

	/**
	 * Gets the fecha carga.
	 *
	 * @return the fecha carga
	 */
	public String getFechaCarga() {
		return fechaCarga;
	}

	/**
	 * Sets the fecha carga.
	 *
	 * @param fechaCarga the new fecha carga
	 */
	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	/**
	 * Gets the contrato.
	 *
	 * @return the contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * Sets the contrato.
	 *
	 * @param contrato the new contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * Gets the nombre archivo.
	 *
	 * @return the nombre archivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Sets the nombre archivo.
	 *
	 * @param nombreArchivo the new nombre archivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Gets the num registros.
	 *
	 * @return the num registros
	 */
	public String getNumRegistros() {
		return numRegistros;
	}

	/**
	 * Sets the num registros.
	 *
	 * @param numRegistros the new num registros
	 */
	public void setNumRegistros(String numRegistros) {
		this.numRegistros = numRegistros;
	}

	/**
	 * Gets the importe.
	 *
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * Sets the importe.
	 *
	 * @param importe the new importe
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * Gets the estatus carga.
	 *
	 * @return the estatus carga
	 */
	public String getEstatusCarga() {
		return estatusCarga;
	}

	/**
	 * Sets the estatus carga.
	 *
	 * @param estatusCarga the new estatus carga
	 */
	public void setEstatusCarga(String estatusCarga) {
		this.estatusCarga = estatusCarga;
	}

	/**
	 * Gets the biatux origen.
	 *
	 * @return the biatux origen
	 */
	public String getBiatuxOrigen() {
		return biatuxOrigen;
	}

	/**
	 * Sets the biatux origen.
	 *
	 * @param biatuxOrigen the new biatux origen
	 */
	public void setBiatuxOrigen(String biatuxOrigen) {
		this.biatuxOrigen = biatuxOrigen;
	}

	/**
	 * Gets the tipo archivo.
	 *
	 * @return the tipo archivo
	 */
	public String getTipoArchivo() {
		return tipoArchivo;
	}

	/**
	 * Sets the tipo archivo.
	 *
	 * @param tipoArchivo the new tipo archivo
	 */
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

}