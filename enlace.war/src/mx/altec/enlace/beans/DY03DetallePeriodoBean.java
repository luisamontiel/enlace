/**
 * Isban Mexico
 *   Clase: DY03DetallePeriodoBean.java
 *   Descripcion: 
 *
 *   Control de Cambios:
 *   1.0 29/04/2013 Stefanini - Creacion
 */
package mx.altec.enlace.beans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

/**
 * The Class DY03DetallePeriodoBean.
 */
public class DY03DetallePeriodoBean implements Serializable {

	/** The Constant ESTATUS_INEXISTENTE. */
	private static final String ESTATUS_INEXISTENTE = "INEXISTENTE";

	/** The Constant ESTATUS_PERIODO_NO_GENERADO. */
	private static final String ESTATUS_PERIODO_NO_GENERADO = "PERIODO NO GENERADO";

	/** The Constant ESTATUS_ERROR_DE_EXTRACCION. */
	private static final String ESTATUS_ERROR_DE_EXTRACCION = "ERROR DE EXTRACCION";

	/** The Constant ESTATUS_PERIODO_NO_DISPONIBLE. */
	private static final String ESTATUS_PERIODO_NO_DISPONIBLE = "PERIODO NO DISPONIBLE";

	/** The Constant ESTATUS_DISPONIBLE. */
	private static final String ESTATUS_DISPONIBLE = "DISPONIBLE";
	
	/** The Constant ESTATUS_DISPONIBLE_INT. */
	private static final String ESTATUS_DISPONIBLE_INT = "02";

	/** The Constant ESTATUS_EN_PROCESO. */
	private static final String ESTATUS_EN_PROCESO = "EN PROCESO";

	/** The Constant ESTATUS_PENDIENTE. */
	private static final String ESTATUS_PENDIENTE = "PENDIENTE";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5441497163773446369L;
	
	/** The SDF. */
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMM", new Locale("ES", "mx"));
	
	/** The SDF2. */
	private static final SimpleDateFormat SDF2 = new SimpleDateFormat("MMMM yyyy", new Locale("ES", "mx"));
	
	/** The cuenta respuesta. */
	private String cuentaRespuesta;
	
	/** The formato respuesta. */
	private String formatoRespuesta;
	
	/** The periodo respuesta. */
	private String periodoRespuesta;
	
	/** The periodo respuesta txt. */
	private String periodoRespuestaTxt;
	
	/** The tipo cuenta respuesta. */
	private String tipoCuentaRespuesta;
	
	/** The folio respuesta. */
	private String folioRespuesta;
	
	/** The estatus respuesta. */
	private String estatusRespuesta;
	
	/** The folio archivo respuesta. */
	private String folioArchivoRespuesta;
	
	/** The nombre archivo respuesta. */
	private String nombreArchivoRespuesta;
	
	/** The tamanio archivo respuesta. */
	private String tamanioArchivoRespuesta;
	
	/** The codigo extraccion respuesta. */
	private String codigoExtraccionRespuesta;
	
	/** The estatus respuesta txt. */
	private String estatusRespuestaTxt;
	
	/**
	 * Gets the cuenta respuesta.
	 *
	 * @return the cuenta respuesta
	 */
	public String getCuentaRespuesta() {
		return cuentaRespuesta;
	}

	/**
	 * Sets the cuenta respuesta.
	 *
	 * @param cuentaRespuesta the new cuenta respuesta
	 */
	public void setCuentaRespuesta(String cuentaRespuesta) {
		this.cuentaRespuesta = cuentaRespuesta;
	}

	/**
	 * Gets the formato respuesta.
	 *
	 * @return the formato respuesta
	 */
	public String getFormatoRespuesta() {
		return formatoRespuesta;
	}

	/**
	 * Sets the formato respuesta.
	 *
	 * @param formatoRespuesta the new formato respuesta
	 */
	public void setFormatoRespuesta(String formatoRespuesta) {
		this.formatoRespuesta = formatoRespuesta;
	}

	/**
	 * Gets the periodo respuesta.
	 *
	 * @return the periodo respuesta
	 */
	public String getPeriodoRespuesta() {
		return periodoRespuesta;
	}

	/**
	 * Sets the periodo respuesta.
	 *
	 * @param periodoRespuesta the new periodo respuesta
	 */
	public void setPeriodoRespuesta(String periodoRespuesta) {
		this.periodoRespuesta = periodoRespuesta;
		this.periodoRespuestaTxt = getPeriodoTxt(periodoRespuesta);
	}

	/**
	 * Gets the tipo cuenta respuesta.
	 *
	 * @return the tipo cuenta respuesta
	 */
	public String getTipoCuentaRespuesta() {
		return tipoCuentaRespuesta;
	}

	/**
	 * Sets the tipo cuenta respuesta.
	 *
	 * @param tipoCuentaRespuesta the new tipo cuenta respuesta
	 */
	public void setTipoCuentaRespuesta(String tipoCuentaRespuesta) {
		this.tipoCuentaRespuesta = tipoCuentaRespuesta;
	}

	/**
	 * Gets the folio respuesta.
	 *
	 * @return the folio respuesta
	 */
	public String getFolioRespuesta() {
		return folioRespuesta;
	}

	/**
	 * Sets the folio respuesta.
	 *
	 * @param folioRespuesta the new folio respuesta
	 */
	public void setFolioRespuesta(String folioRespuesta) {
		this.folioRespuesta = folioRespuesta;
	}

	/**
	 * Gets the estatus respuesta.
	 *
	 * @return the estatus respuesta
	 */
	public String getEstatusRespuesta() {
		return estatusRespuesta;
	}

	/**
	 * Sets the estatus respuesta.
	 *
	 * @param estatusRespuesta the new estatus respuesta
	 */
	public void setEstatusRespuesta(String estatusRespuesta) {
		this.estatusRespuesta = estatusRespuesta;
		this.setEstatusRespuestaTxt(estatusRespuesta);
	}

	/**
	 * Gets the folio archivo respuesta.
	 *
	 * @return the folio archivo respuesta
	 */
	public String getFolioArchivoRespuesta() {
		return folioArchivoRespuesta;
	}

	/**
	 * Sets the folio archivo respuesta.
	 *
	 * @param folioArchivoRespuesta the new folio archivo respuesta
	 */
	public void setFolioArchivoRespuesta(String folioArchivoRespuesta) {
		this.folioArchivoRespuesta = folioArchivoRespuesta;
	}

	/**
	 * Gets the nombre archivo respuesta.
	 *
	 * @return the nombre archivo respuesta
	 */
	public String getNombreArchivoRespuesta() {
		return nombreArchivoRespuesta;
	}

	/**
	 * Sets the nombre archivo respuesta.
	 *
	 * @param nombreArchivoRespuesta the new nombre archivo respuesta
	 */
	public void setNombreArchivoRespuesta(String nombreArchivoRespuesta) {
		this.nombreArchivoRespuesta = nombreArchivoRespuesta;
	}

	/**
	 * Gets the tamanio archivo respuesta.
	 *
	 * @return the tamanio archivo respuesta
	 */
	public String getTamanioArchivoRespuesta() {
		return tamanioArchivoRespuesta;
	}

	/**
	 * Sets the tamanio archivo respuesta.
	 *
	 * @param tamanioArchivoRespuesta the new tamanio archivo respuesta
	 */
	public void setTamanioArchivoRespuesta(String tamanioArchivoRespuesta) {
		this.tamanioArchivoRespuesta = tamanioArchivoRespuesta;
	}

	/**
	 * Gets the codigo extraccion respuesta.
	 *
	 * @return the codigo extraccion respuesta
	 */
	public String getCodigoExtraccionRespuesta() {
		return codigoExtraccionRespuesta;
	}

	/**
	 * Sets the codigo extraccion respuesta.
	 *
	 * @param codigoExtraccionRespuesta the new codigo extraccion respuesta
	 */
	public void setCodigoExtraccionRespuesta(String codigoExtraccionRespuesta) {
		this.codigoExtraccionRespuesta = codigoExtraccionRespuesta;
	}
	
	/**
	 * Gets the estatus respuesta txt.
	 *
	 * @return the estatus respuesta txt
	 */
	public String getEstatusRespuestaTxt() {
		return estatusRespuestaTxt;
	}

	/**
	 * Sets the estatus respuesta txt.
	 *
	 * @param estatusRespuestaTxt the new estatus respuesta txt
	 */
	public void setEstatusRespuestaTxt(String estatusRespuestaTxt) {
		this.estatusRespuestaTxt = getEstatusTxt(estatusRespuestaTxt);
	}

	/**
	 * Gets the periodo respuesta txt.
	 *
	 * @return the periodo respuesta txt
	 */
	public String getPeriodoRespuestaTxt() {
		return periodoRespuestaTxt;
	}

	/**
	 * Sets the periodo respuesta txt.
	 *
	 * @param periodoRespuestaTxt the new periodo respuesta txt
	 */
	public void setPeriodoRespuestaTxt(String periodoRespuestaTxt) {
		this.periodoRespuestaTxt = periodoRespuestaTxt;
	}

	/**
	 * Procesar mensaje respuesta.
	 *
	 * @param mensajeRespuesta the mensaje respuesta
	 */
	public void procesarMensajeRespuesta(String mensajeRespuesta){
		this.setCuentaRespuesta(mensajeRespuesta.substring(0, 20).trim());
		this.setFormatoRespuesta(mensajeRespuesta.substring(20, 21).trim());
		
		this.setPeriodoRespuesta(mensajeRespuesta.substring(21, 27).trim());
		this.setTipoCuentaRespuesta(mensajeRespuesta.substring(27, 30).trim());
		this.setFolioRespuesta(mensajeRespuesta.substring(30, 40).trim());
		this.setEstatusRespuesta(mensajeRespuesta.substring(40, 42).trim());

		if(ESTATUS_DISPONIBLE_INT.equals(estatusRespuesta)){
			this.setFolioArchivoRespuesta(mensajeRespuesta.substring(42, 62).trim());
			this.setNombreArchivoRespuesta(mensajeRespuesta.substring(62, 122).trim());
			this.setTamanioArchivoRespuesta(mensajeRespuesta.substring(122, 132).trim());
//			this.setCodigoExtraccionRespuesta(mensajeRespuesta.substring(132, 134).trim());
		}
	}
	
	/**
	 * Gets the periodo txt.
	 *
	 * @param txt the txt
	 * @return the periodo txt
	 */
	private synchronized static String getPeriodoTxt(String txt){
		try {
			return StringUtils.capitalize(SDF2.format(SDF.parse(txt)));
		} catch (ParseException e) {
			return StringUtils.EMPTY;
		}
	}
	
	/**
	 * Gets the periodo txt.
	 *
	 * @param txt the txt
	 * @return the periodo txt
	 */
	private static String getEstatusTxt(String txt){
		String res = StringUtils.EMPTY;
		if(StringUtils.isNumeric(txt)){
			int estatus = Integer.parseInt(txt);
			switch (estatus) {
				case 0:
					res = ESTATUS_PENDIENTE;
					break;
				case 1:
					res = ESTATUS_EN_PROCESO;
					break;
				case 2:
					res = ESTATUS_DISPONIBLE;
					break;
				case 3:
					res = ESTATUS_PERIODO_NO_DISPONIBLE;
					break;
				case 4:
					res = ESTATUS_ERROR_DE_EXTRACCION;
					break;
				case 5:
					res = ESTATUS_PERIODO_NO_GENERADO;
					break;
				case 6:
					res = ESTATUS_INEXISTENTE;
					break;
				default:
					res = StringUtils.EMPTY;
					break;
			}
		}
		return res;
	}
}
