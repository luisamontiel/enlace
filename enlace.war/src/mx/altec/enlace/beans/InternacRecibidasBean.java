package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class InternacRecibidasBean.
 * Bean de la Operacion enviada por el Ordenante
 */
public class InternacRecibidasBean implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** El objeto codigo respuesta enviado por el Ordenante. */
	private String codRespuesta;

	/** El objeto estatus enviado por el Ordenante. */
	private String estatus;

	/** El objeto referencia enviado por el Ordenante. */
	private String referencia;
	
	/** El objeto cuenta destino enviado por el Ordenante. */
	private String cuentaDestino;

	/** El objeto nom cta destino enviado por el Ordenante. */
	private String nomCtaDestino;

	/** El objeto cuenta del Ordenante. */
	private String cuentaOrdenante;

	/** El objeto nombre del Ordenante. */
	private String nombreOrdenante;

	/** El objeto banco enviado por el Ordenante. */
	private String bancoOrdenante;

	/** El objeto pais enviado por el Ordenante. */
	private String paisOrdenante;

	/** El objeto descripcion pais enviado por el Ordenante. */
	private String descripcionPais;

	/** El objeto ciudad enviado por el Ordenante. */
	private String ciudadOrdenante;

	/** El objeto importe enviado por el Ordenante. */
	private String importe;

	/** El objeto divisa enviado por el Ordenante. */
	private String divisa;

	/** El objeto tipo cambio enviado por el Ordenante. */
	private String tipoCambio;

	/** El objeto descripcion estatus enviado por el Ordenante. */
	private String descripcionEstatus;

	/** El objeto fecha hora enviado por el Ordenante. */
	private String fechaHora;

	/** El objeto concepto pago enviado por el Ordenante. */
	private String conceptoPago;

	/** El objeto lstTransferRecibidas. */
	private List<InternacRecibidasBean> lstTransferRecibidas = new ArrayList<InternacRecibidasBean>();


	/**
	 *
	 * @return the codRespuesta enviado por el Ordenante
	 */
	public String getCodRespuesta() {
		return codRespuesta;
	}

	/**
	 * @param codRespuesta the codRespuesta to set
	 */
	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	/**
	 * @return the referencia enviado por el Ordenante
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	/**
	 * @return the cuentaDestino enviado por el Ordenante
	 */
	public String getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino the cuentaDestino to set
	 */
	public void setCuentaDestino(String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	/**
	 * @return the nomCtaDestino enviado por el Ordenante
	 */
	public String getNomCtaDestino() {
		return nomCtaDestino;
	}

	/**
	 * @param nomCtaDestino the nomCtaDestino to set
	 */
	public void setNomCtaDestino(String nomCtaDestino) {
		this.nomCtaDestino = nomCtaDestino;
	}

	/**
	 * @return the cuentaOrdenante 
	 */
	public String getCuentaOrdenante() {
		return cuentaOrdenante;
	}

	/**
	 * @param cuentaOrdenante the cuentaOrdenante to set
	 */
	public void setCuentaOrdenante(String cuentaOrdenante) {
		this.cuentaOrdenante = cuentaOrdenante;
	}

	/**
	 * @return the nombreOrdenante 
	 */
	public String getNombreOrdenante() {
		return nombreOrdenante;
	}

	/**
	 * @param nombreOrdenante the nombreOrdenante to set
	 */
	public void setNombreOrdenante(String nombreOrdenante) {
		this.nombreOrdenante = nombreOrdenante;
	}

	/**
	 * @return the bancoOrdenante 
	 */
	public String getBancoOrdenante() {
		return bancoOrdenante;
	}

	/**
	 * @param bancoOrdenante the bancoOrdenante to set
	 */
	public void setBancoOrdenante(String bancoOrdenante) {
		this.bancoOrdenante = bancoOrdenante;
	}

	/**
	 * @return the paisOrdenante
	 */
	public String getPaisOrdenante() {
		return paisOrdenante;
	}

	/**
	 * @param paisOrdenante the paisOrdenante to set
	 */
	public void setPaisOrdenante(String paisOrdenante) {
		this.paisOrdenante = paisOrdenante;
	}

	/**
	 * @return the descripcionPais enviado por el Ordenante
	 */
	public String getDescripcionPais() {
		return descripcionPais;
	}

	/**
	 * @param descripcionPais the descripcionPais to set
	 */
	public void setDescripcionPais(String descripcionPais) {
		this.descripcionPais = descripcionPais;
	}

	/**
	 * @return the ciudadOrdenante 
	 */
	public String getCiudadOrdenante() {
		return ciudadOrdenante;
	}

	/**
	 * @param ciudadOrdenante the ciudadOrdenante to set
	 */
	public void setCiudadOrdenante(String ciudadOrdenante) {
		this.ciudadOrdenante = ciudadOrdenante;
	}

	/**
	 * @return the importe enviado por el Ordenante
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * @return the divisa enviado por el Ordenante
	 */
	public String getDivisa() {
		return divisa;
	}

	/**
	 * @param divisa the divisa to set
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 * @return the tipoCambio enviado por el Ordenante
	 */
	public String getTipoCambio() {
		return tipoCambio;
	}

	/**
	 * @param tipoCambio the tipoCambio to set
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	/**
	 * @return the estatus enviado por el Ordenante
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the descripcionEstatus enviado por el Ordenante
	 */
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

	/**
	 * @param descripcionEstatus the descripcionEstatus to set
	 */
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}

	/**
	 * @return the fechaHora enviado por el Ordenante
	 */
	public String getFechaHora() {
		return fechaHora;
	}

	/**
	 * @param fechaHora the fechaHora to set
	 */
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	/**
	 * @return the conceptoPago enviado por el Ordenante
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}

	/**
	 * @param conceptoPago the conceptoPago to set
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	/**
	 * @return the lstTransferRecibidas
	 */
	public List<InternacRecibidasBean> getLstTransferRecibidas() {
		return lstTransferRecibidas;
	}

	/**
	 * @param lstTransferRecibidas the lstTransferRecibidas to set
	 */
	public void setLstTransferRecibidas(
			List<InternacRecibidasBean> lstTransferRecibidas) {
		this.lstTransferRecibidas = lstTransferRecibidas;
	}

}