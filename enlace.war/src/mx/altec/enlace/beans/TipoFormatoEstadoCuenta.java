package mx.altec.enlace.beans;

public enum TipoFormatoEstadoCuenta {
	/**
	 * Formato PDF
	 */
	PDF(1, "PDF"),
	/**
	 * Formato XML
	 */
	XML(2, "XML");
	
	/**
	 * Identificador del formato
	 */
	int idFormato;
	/**
	 * Descripcion del formato
	 */
	String descripcion;
	
	/**
	 * Constructor para un nuevo tipo de formato para el estado de cuenta
	 * @param idFormato El identificador del formato
	 * @param descripcion La descripcon del formato
	 */
	private TipoFormatoEstadoCuenta(int idFormato, String descripcion){
		this.idFormato = idFormato;
		this.descripcion = descripcion;
	}
	
	/**
	 * Obtiene el identificador del formato
	 * @return El identificador del formato
	 */
	public int obtenerIdFormato(){
		return idFormato;
	}
	
	/**
	 * Obtiene la descripcion del formato
	 * @return La descripcion del formato
	 */
	public String obtenerDescripcion(){
		return descripcion;
	}
}
