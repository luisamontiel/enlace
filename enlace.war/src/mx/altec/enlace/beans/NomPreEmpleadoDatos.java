package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.io.Serializable;

public class NomPreEmpleadoDatos extends NomPreEmpleado {


	private static final long serialVersionUID = 6337412145813137608L;

	public static int LNG_FECHA_INGRESO = 10;

	public static int LNG_SUCURSAL_ADMIN = 4;

	public static int LNG_TIPO_PERSONA = 1;

	public static int LNG_FECHA_INICIO_ACT = 10;

	public static int LNG_CANAL_VENTA = 3;

	public static int LNG_CANAL_CAPTACION = 3;

	public static int LNG_PAIS_ORIGEN = 3;

	public static int LNG_CODIGO_CAMPANIA = 2;

	public static int LNG_AGRUPACION_OFICIALES = 20;

	public static int LNG_NACIONALIDAD3 = 3;

	public static int LNG_PAIS_RESIDENCIA = 3;

	public static int LNG_SECTOR = 3;

	public static int LNG_TIPO_OCUPACION = 2;

	public static int LNG_CODIGO_ACTIVIDAD = 8;

	public static int LNG_TIEMPO_RESIDENCIA = 3;

	public static int LNG_FORMA_JURIDICA = 2;

	public static int LNG_NATURALEZA_JURIDICA = 3;

	public static int LNG_UNIDAD_NEGOCIO = 3;

	public static int LNG_CODIGO_SUJETO = 3;

	public static int LNG_MARCA_BANCA_PRIVADA = 1;

	public static int LNG_NIVEL_ACCESO = 1;

	public static int LNG_ESTADO_PERSONA = 3;

	public static int LNG_CONDICION_PERSONA = 3;

	public static int LNG_INDICADORES = 14;

	public static int LNG_COD_ACT_RIU = 5;

	public static int LNG_SUBSEGMENTO = 3;

	public static int LNG_IDIOMA = 3;

	public static int LNG_TIMESTAMP1 = 26;

	public static int LNG_EXPEDIDO_POR = 15;

	public static int LNG_FECHA_VENCIMIENTO = 10;

	public static int LNG_MARCA_VERIFICACION = 1;

	public static int LNG_CONDICION_DOCUMENTO = 1;

	public static int LNG_CODIGO_PAIS = 3;

	public static int LNG_FECHA_EXPEDICION = 10;

	public static int LNG_LUGAR_EXPOSICION = 7;

	public static int LNG_NUMERO_OCR = 12;

	public static int LNG_NUMERO_FOLIO_RFC = 8;

	public static int LNG_TIMESTAMP2 = 26;

	public static int LNG_CAMPO_AGREGADO_1 = 84;

	public static int LNG_CAMPO_AGREGADO_2 = 73;

	public static int LNG_RUTA_CARTERO = 9;

	public static int LNG_CAMPO_AGREGADO = 7;

	public static int LNG_FECHA_VERIFICACION = 10;

	public static int LNG_CAMPO_AGREGADO_4 = 5;

	public static int LNG_CAMPO_VARIABLE_1 = 100;

	public static int LNG_CAMPO_VARIABLE_2 = 100;

	public static int LNG_TIMESTAMP_3 = 26;

	public static int LNG_SECUENCIA_DOMICILIO = 3;

	public static int LNG_AGREGADO_TELEFONO = 75;

	public static int LNG_TIMESTAMP_4 = 26;

	private String fechaIngreso;

	private String sucursalAdmin;

	private String tipoPersona;

	private String fechaInicioActiv;

	private String canalVenta;

	private String canalCaptacion;

	private String paisOrigen;

	private String codigoCampania;

	private String agrupacionOficiales;

	private String paisResidencia;

	private String sector;

	private String tipoOcupacion;

	private String codigoActividad;

	private String tiempoResidencia;

	private String formaJuridica;

	private String naturalezaJuridica;

	private String unidadNegocio;

	private String codigoSujeto;

	private String marcaBancaPrivada;

	private String nivelAcceso;

	private String estadoPersona;

	private String condicionPersona;

	private String indicadores;

	private String codActRiu;

	private String subsegmento;

	private String idioma;

	private String timeStamp1;

	private String expedidoPor;

	private String fechaVencimiento;

	private String marcaVerificacion;

	private String condicionDocumento;

	private String codigoPais;

	private String fechaExpedicion;

	private String lugarExposicion;

	private String numeroOCR;

	private String numeroFolioRFC;

	private String timeStamp2;

	private String campoAgregado1;

	private String campoAgregado2;

	private String rutaCartero;

	private String campoAgregado;

	private String fechaVerificacion;

	private String campoAgregado4;

	private String campoVariable1;

	private String campoVariable2;

	private String timeStamp3;

	private String secuenciaDomicilio;

	private String agregadoTelefono;

	private String timeStamp4;

	public String getAgregadoTelefono() {
		return agregadoTelefono;
	}

	public void setAgregadoTelefono(String agregadoTelefono) {
		this.agregadoTelefono = agregadoTelefono;
	}

	public String getAgrupacionOficiales() {
		return agrupacionOficiales;
	}

	public void setAgrupacionOficiales(String agrupacionOficiales) {
		this.agrupacionOficiales = agrupacionOficiales;
	}

	public String getCampoAgregado() {
		return campoAgregado;
	}

	public void setCampoAgregado(String campoAgregado) {
		this.campoAgregado = campoAgregado;
	}

	public String getCampoAgregado1() {
		return campoAgregado1;
	}

	public void setCampoAgregado1(String campoAgregado1) {
		this.campoAgregado1 = campoAgregado1;
	}

	public String getCampoAgregado2() {
		return campoAgregado2;
	}

	public void setCampoAgregado2(String campoAgregado2) {
		this.campoAgregado2 = campoAgregado2;
	}

	public String getCampoAgregado4() {
		return campoAgregado4;
	}

	public void setCampoAgregado4(String campoAgregado4) {
		this.campoAgregado4 = campoAgregado4;
	}

	public String getCampoVariable1() {
		return campoVariable1;
	}

	public void setCampoVariable1(String campoVariable1) {
		this.campoVariable1 = campoVariable1;
	}

	public String getCampoVariable2() {
		return campoVariable2;
	}

	public void setCampoVariable2(String campoVariable2) {
		this.campoVariable2 = campoVariable2;
	}

	public String getCanalCaptacion() {
		return canalCaptacion;
	}

	public void setCanalCaptacion(String canalCaptacion) {
		this.canalCaptacion = canalCaptacion;
	}

	public String getCanalVenta() {
		return canalVenta;
	}

	public void setCanalVenta(String canalVenta) {
		this.canalVenta = canalVenta;
	}

	public String getCodActRiu() {
		return codActRiu;
	}

	public void setCodActRiu(String codActRiu) {
		this.codActRiu = codActRiu;
	}

	public String getCodigoActividad() {
		return codigoActividad;
	}

	public void setCodigoActividad(String codigoActividad) {
		this.codigoActividad = codigoActividad;
	}

	public String getCodigoCampania() {
		return codigoCampania;
	}

	public void setCodigoCampania(String codigoCampania) {
		this.codigoCampania = codigoCampania;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	public String getCodigoSujeto() {
		return codigoSujeto;
	}

	public void setCodigoSujeto(String codigoSujeto) {
		this.codigoSujeto = codigoSujeto;
	}

	public String getCondicionDocumento() {
		return condicionDocumento;
	}

	public void setCondicionDocumento(String condicionDocumento) {
		this.condicionDocumento = condicionDocumento;
	}

	public String getCondicionPersona() {
		return condicionPersona;
	}

	public void setCondicionPersona(String condicionPersona) {
		this.condicionPersona = condicionPersona;
	}

	public String getEstadoPersona() {
		return estadoPersona;
	}

	public void setEstadoPersona(String estadoPersona) {
		this.estadoPersona = estadoPersona;
	}

	public String getExpedidoPor() {
		return expedidoPor;
	}

	public void setExpedidoPor(String expedidoPor) {
		this.expedidoPor = expedidoPor;
	}

	public String getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(String fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getFechaInicioActiv() {
		return fechaInicioActiv;
	}

	public void setFechaInicioActiv(String fechaInicioActiv) {
		this.fechaInicioActiv = fechaInicioActiv;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getFechaVerificacion() {
		return fechaVerificacion;
	}

	public void setFechaVerificacion(String fechaVerificacion) {
		this.fechaVerificacion = fechaVerificacion;
	}

	public String getFormaJuridica() {
		return formaJuridica;
	}

	public void setFormaJuridica(String formaJuridica) {
		this.formaJuridica = formaJuridica;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getIndicadores() {
		return indicadores;
	}

	public void setIndicadores(String indicadores) {
		this.indicadores = indicadores;
	}

	public String getLugarExposicion() {
		return lugarExposicion;
	}

	public void setLugarExposicion(String lugarExposicion) {
		this.lugarExposicion = lugarExposicion;
	}

	public String getMarcaBancaPrivada() {
		return marcaBancaPrivada;
	}

	public void setMarcaBancaPrivada(String marcaBancaPrivada) {
		this.marcaBancaPrivada = marcaBancaPrivada;
	}

	public String getMarcaVerificacion() {
		return marcaVerificacion;
	}

	public void setMarcaVerificacion(String marcaVerificacion) {
		this.marcaVerificacion = marcaVerificacion;
	}

	public String getNaturalezaJuridica() {
		return naturalezaJuridica;
	}

	public void setNaturalezaJuridica(String naturalezaJuridica) {
		this.naturalezaJuridica = naturalezaJuridica;
	}

	public String getNivelAcceso() {
		return nivelAcceso;
	}

	public void setNivelAcceso(String nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}

	public String getNumeroFolioRFC() {
		return numeroFolioRFC;
	}

	public void setNumeroFolioRFC(String numeroFolioRFC) {
		this.numeroFolioRFC = numeroFolioRFC;
	}

	public String getNumeroOCR() {
		return numeroOCR;
	}

	public void setNumeroOCR(String numeroOCR) {
		this.numeroOCR = numeroOCR;
	}

	public String getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	public String getPaisResidencia() {
		return paisResidencia;
	}

	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	public String getRutaCartero() {
		return rutaCartero;
	}

	public void setRutaCartero(String rutaCartero) {
		this.rutaCartero = rutaCartero;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}

	public void setSecuenciaDomicilio(String secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}

	public String getSubsegmento() {
		return subsegmento;
	}

	public void setSubsegmento(String subsegmento) {
		this.subsegmento = subsegmento;
	}

	public String getSucursalAdmin() {
		return sucursalAdmin;
	}

	public void setSucursalAdmin(String sucursalAdmin) {
		this.sucursalAdmin = sucursalAdmin;
	}

	public String getTiempoResidencia() {
		return tiempoResidencia;
	}

	public void setTiempoResidencia(String tiempoResidencia) {
		this.tiempoResidencia = tiempoResidencia;
	}

	public String getTimeStamp1() {
		return timeStamp1;
	}

	public void setTimeStamp1(String timeStamp1) {
		this.timeStamp1 = timeStamp1;
	}

	public String getTimeStamp2() {
		return timeStamp2;
	}

	public void setTimeStamp2(String timeStamp2) {
		this.timeStamp2 = timeStamp2;
	}

	public String getTimeStamp3() {
		return timeStamp3;
	}

	public void setTimeStamp3(String timeStamp3) {
		this.timeStamp3 = timeStamp3;
	}

	public String getTimeStamp4() {
		return timeStamp4;
	}

	public void setTimeStamp4(String timeStamp4) {
		this.timeStamp4 = timeStamp4;
	}

	public String getTipoOcupacion() {
		return tipoOcupacion;
	}

	public void setTipoOcupacion(String tipoOcupacion) {
		this.tipoOcupacion = tipoOcupacion;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getUnidadNegocio() {
		return unidadNegocio;
	}

	public void setUnidadNegocio(String unidadNegocio) {
		this.unidadNegocio = unidadNegocio;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("=== Contenido del Bean NomPreEmpleado ===\n")
		.append("Paterno: ").append(this.getPaterno()).append("\n")
		.append("Materno: ").append(this.getMaterno()).append("\n")
		.append("Nombre: ").append(this.getNombre()).append("\n")
		.append("Fecha Ingreso: ").append(fechaIngreso).append("\n")
		.append("Sucursal Admin: ").append(sucursalAdmin).append("\n")
		.append("Estado Civil: ").append(this.getEstadoCivil()).append("\n")
		.append("Sexo: ").append(this.getSexo()).append("\n")
		.append("Tipo Persona: ").append(tipoPersona).append("\n")
		.append("Fecha Nacimiento: ").append(this.getFechaNacimiento()).append("\n")
		.append("Fecha Inicio Actividades: ").append(fechaInicioActiv).append("\n")
		.append("Canal Venta: ").append(canalVenta).append("\n")
		.append("Canal Captacion: ").append(canalCaptacion).append("\n")
		.append("Pais Origen: ").append(paisOrigen).append("\n")
		.append("Codigo campania: ").append(codigoCampania).append("\n")
		.append("Agrupacion Oficiales: ").append(agrupacionOficiales).append("\n")
		.append("Nacionalidad: ").append(this.getNacionalidad()).append("\n")
		.append("Pais Residencia: ").append(paisResidencia).append("\n")
		.append("Sector: ").append(sector).append("\n")
		.append("Tipo de Ocupacion: ").append(tipoOcupacion).append("\n")
		.append("Codigo Actividad: ").append(codigoActividad).append("\n")
		.append("Tiempo Residencia: ").append(tiempoResidencia).append("\n")
		.append("Forma Juridica: ").append(formaJuridica).append("\n")
		.append("Naturaleza Juridica: ").append(naturalezaJuridica).append("\n")
		.append("Unidad Negocio: ").append(unidadNegocio).append("\n")
		.append("Codigo Sujeto: ").append(codigoSujeto).append("\n")
		.append("Marca Banca Privada: ").append(marcaBancaPrivada).append("\n")
		.append("Nivel Acceso: ").append(nivelAcceso).append("\n")
		.append("Estado Persona: ").append(estadoPersona).append("\n")
		.append("Condicion Persona: ").append(condicionPersona).append("\n")
		.append("Indicadores: ").append(indicadores).append("\n")
		.append("Cod Act Riu: ").append(codActRiu).append("\n")
		.append("Subsegmento: ").append(subsegmento).append("\n")
		.append("Idioma: ").append(idioma).append("\n")
		.append("TimeStamp 1: ").append(timeStamp1).append("\n")
		.append("Expedido Por: ").append(expedidoPor).append("\n")
		.append("Fecha Vencimiento: ").append(fechaVencimiento).append("\n")
		.append("Marca Verificacion: ").append(marcaVerificacion).append("\n")
		.append("Condicion Documento: ").append(condicionDocumento).append("\n")
		.append("Codigo Pais: ").append(codigoPais).append("\n")
		.append("Fecha Expedicion: ").append(fechaExpedicion).append("\n")
		.append("Lugar Exposicion: ").append(lugarExposicion).append("\n")
		.append("Numero OCR: ").append(numeroOCR).append("\n")
		.append("Numero Folio: ").append(numeroFolioRFC).append("\n");

		return sb.toString();
	}

}