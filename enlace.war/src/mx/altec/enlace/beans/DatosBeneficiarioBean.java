/**
 * 
 */
package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * Propiedades para los datos complementarios de la cuentas beneficiarias internacionales
 * @author FSW Indra
 *
 */
public class DatosBeneficiarioBean implements Serializable{

	/**	numero de serie */
	private static final long serialVersionUID = -6208747987183593196L;
	/** Direccion */
	private String direccion;
	/** Ciudad */
	private String ciudad;
	/** Identificador del cliente */
	private String idCliente;
	/** Clave del pais */
	private String cvePais;
	/** Nombre del banco */
	private String descBanco;
	/** Nombre del titular */
	private String descTitular;
	/** Clave de divisa */
	private String cveDivisa;
	/** Clave del pais del beneficiario */
	private String cvePaisBenef;
	/** Clave ABA */
	private String cveAba;
	/** Clave Swift */
	private String cveSwift;
	/** Ciudad */
	private String descCiudad;
	
	/**
	 * @return el direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion el direccion a establecer
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return el ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * @param ciudad el ciudad a establecer
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * @return el idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente el idCliente a establecer
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return el cvePais
	 */
	public String getCvePais() {
		return cvePais;
	}
	/**
	 * @param cvePais el cvePais a establecer
	 */
	public void setCvePais(String cvePais) {
		this.cvePais = cvePais;
	}
	/**
	 * @return el descBanco
	 */
	public String getDescBanco() {
		return descBanco;
	}
	/**
	 * @param descBanco el descBanco a establecer
	 */
	public void setDescBanco(String descBanco) {
		this.descBanco = descBanco;
	}
	/**
	 * @return el descTitular
	 */
	public String getDescTitular() {
		return descTitular;
	}
	/**
	 * @param descTitular el descTitular a establecer
	 */
	public void setDescTitular(String descTitular) {
		this.descTitular = descTitular;
	}
	/**
	 * @return el cveDivisa
	 */
	public String getCveDivisa() {
		return cveDivisa;
	}
	/**
	 * @param cveDivisa el cveDivisa a establecer
	 */
	public void setCveDivisa(String cveDivisa) {
		this.cveDivisa = cveDivisa;
	}
	/**
	 * @return el cvePaisBenef
	 */
	public String getCvePaisBenef() {
		return cvePaisBenef;
	}
	/**
	 * @param cvePaisBenef el cvePaisBenef a establecer
	 */
	public void setCvePaisBenef(String cvePaisBenef) {
		this.cvePaisBenef = cvePaisBenef;
	}
	/**
	 * @return el cveAba
	 */
	public String getCveAba() {
		return cveAba;
	}
	/**
	 * @param cveAba el cveAba a establecer
	 */
	public void setCveAba(String cveAba) {
		this.cveAba = cveAba;
	}
	/**
	 * @return el cveSwift
	 */
	public String getCveSwift() {
		return cveSwift;
	}
	/**
	 * @param cveSwift el cveSwift a establecer
	 */
	public void setCveSwift(String cveSwift) {
		this.cveSwift = cveSwift;
	}
	/**
	 * @return el descCiudad
	 */
	public String getDescCiudad() {
		return descCiudad;
	}
	/**
	 * @param descCiudad el descCiudad a establecer
	 */
	public void setDescCiudad(String descCiudad) {
		this.descCiudad = descCiudad;
	}
	
}
