/**
 * Isban Mexico
 *   Clase: TablaEwebNominaBean.java
 *   Descripcion: Implementacion de TablaEwebNominaBean, bean de datos para la tabla eweb_nomina
 *
 *   Control de Cambios:
 *   1.0 Mar 01, 2015 asanjuan - Creacion
 *   
 **/
package mx.altec.enlace.beans;

/**
 * Creacion TablaEwebNominaBean
 * @author Arturo
 * @version 1.0
 * 
 */
public class TablaEwebNominaBean  {

	/**
	 * Serial generado
	 */
	private static final long serialVersionUID = 6550873117220354018L;
	/**estatus**/
	private String estatus;
	/**fecha de recepcion**/
	private String fechaRecepcion;
	/**fecha de cargo**/
	private String fechaCargo;
	/**fecha de la aplicacion**/
	private String fechaAplicacion;
	/**hora de la aplicacion**/
	private String horaAplicacion;
	/**nombre del archivo**/
	private String nombreArchivo;
	/**numoer de registros**/
	private String numeroRegistros;
	/**secuencia**/
	private String secuencia;
	/**numero cuenta2**/
	private String numeroCuenta2;
	/**cuenta cargo**/
	private String ctaCargo;
	/**importe aplicado**/
	private String importeAplic;
	/**id hora disp**/
	private String idHorDips;
	
	
	/**
	 * @return el estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus el estatus a establecer
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return el fechaRecepcion
	 */
	public String getFechaRecepcion() {
		return fechaRecepcion;
	}
	/**
	 * @param fechaRecepcion el fechaRecepcion a establecer
	 */
	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	/**
	 * @return el fechaCargo
	 */
	public String getFechaCargo() {
		return fechaCargo;
	}
	/**
	 * @param fechaCargo el fechaCargo a establecer
	 */
	public void setFechaCargo(String fechaCargo) {
		this.fechaCargo = fechaCargo;
	}
	/**
	 * @return el fechaAplicacion
	 */
	public String getFechaAplicacion() {
		return fechaAplicacion;
	}
	/**
	 * @param fechaAplicacion el fechaAplicacion a establecer
	 */
	public void setFechaAplicacion(String fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}
	/**
	 * @return el horaAplicacion
	 */
	public String getHoraAplicacion() {
		return horaAplicacion;
	}
	/**
	 * @param horaAplicacion el horaAplicacion a establecer
	 */
	public void setHoraAplicacion(String horaAplicacion) {
		this.horaAplicacion = horaAplicacion;
	}
	/**
	 * @return el nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo el nombreArchivo a establecer
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * @return el numeroRegistros
	 */
	public String getNumeroRegistros() {
		return numeroRegistros;
	}
	/**
	 * @param numeroRegistros el numeroRegistros a establecer
	 */
	public void setNumeroRegistros(String numeroRegistros) {
		this.numeroRegistros = numeroRegistros;
	}
	/**
	 * @return el secuencia
	 */
	public String getSecuencia() {
		return secuencia;
	}
	/**
	 * @param secuencia el secuencia a establecer
	 */
	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
	/**
	 * @return el numeroCuenta2
	 */
	public String getNumeroCuenta2() {
		return numeroCuenta2;
	}
	/**
	 * @param numeroCuenta2 el numeroCuenta2 a establecer
	 */
	public void setNumeroCuenta2(String numeroCuenta2) {
		this.numeroCuenta2 = numeroCuenta2;
	}
	/**
	 * @return el ctaCargo
	 */
	public String getCtaCargo() {
		return ctaCargo;
	}
	/**
	 * @param ctaCargo el ctaCargo a establecer
	 */
	public void setCtaCargo(String ctaCargo) {
		this.ctaCargo = ctaCargo;
	}
	/**
	 * @return el importeAplic
	 */
	public String getImporteAplic() {
		return importeAplic;
	}
	/**
	 * @param importeAplic el importeAplic a establecer
	 */
	public void setImporteAplic(String importeAplic) {
		this.importeAplic = importeAplic;
	}
	/**
	 * @return el idHorDips
	 */
	public String getIdHorDips() {
		return idHorDips;
	}
	/**
	 * @param idHorDips el idHorDips a establecer
	 */
	public void setIdHorDips(String idHorDips) {
		this.idHorDips = idHorDips;
	}	
}