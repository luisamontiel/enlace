package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * @author ESC
 * Bean de Tarjetas Contrato BD EWEB_VINC_TARJETACON 
 */
public class TarjetaContrato implements Serializable {
	/**
	 * Cadena LNG_FECHA
	 */
	public static int lngfecha = 10;
	/**
	 * Cadena LNG_NO_TARJETA
	 */
	public static int lngNoTarjeta = 16;
	/**
	 * Cadena LNG_CONTRATO
	 */
	public static int lngContrato = 7;
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * N&uacute;mero de contrato
	 */
	private String contrato;	
	/**
	 * N&uacute;mero de tarjeta
	 */
	private String tarjeta;	
	/**
	 * Estatus de la tarjeta
	 */
	private String estatus;	
	/**
	 * Lote de carga
	 */
	private String idLote;	
	/**
	 * Nombre de Archivo
	 */
	private String archivo;	
	
	
	/**
	 * @return the archivo
	 */
	public String getArchivo() {
		return archivo;
	}

	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	/**
	 * Fecha ultimo acceso
	 */
	private String fechaUltAcceso;
	/**
	 * Usuario de registro
	 */
	private String usuarioReg;	
	/**
	 * Indicador si la transaccion es exitosa
	 */
	private boolean codExito;
	/**
	 * Cadena con el codigo de la operacion
	 */
	private String codigoOperacion;
	/**
	 * Cadena con el mensaje de error de la operacion
	 */
	private String mensError;
	
	/**
	 * obtiene contrato
	 * @return contrato
	 */
	public String getContrato() {
		return contrato;
	}
	
	/**
	 * Setea contrato
	 * @param contrato contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * obtiene tarjeta
	 * @return tarjeta
	 */
	public String getTarjeta() {
		return tarjeta;
	}
	/**
	 * Setea tarjeta
	 * @param tarjeta tarjeta
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	/**
	 * obtiene estatus
	 * @return estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Setea estatus
	 * @param estatus estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * obtiene idLote
	 * @return idLote
	 */
	public String getIdLote() {
		return idLote;
	}
	/**
	 * Setea idLote
	 * @param idLote idLote
	 */
	public void setIdLote(String idLote) {
		this.idLote = idLote;
	}
	/**
	 * obtiene fechaUltAcceso
	 * @return fechaUltAcceso
	 */
	public String getFechaUltAcceso() {
		return fechaUltAcceso;
	}
	/**
	 * Setea fechaUltAcceso
	 * @param fechaUltAcceso fechaUltAcceso
	 */
	public void setFechaUltAcceso(String fechaUltAcceso) {
		this.fechaUltAcceso = fechaUltAcceso;
	}
	/**
	 * obtiene usuarioReg
	 * @return usuarioReg
	 */
	public String getUsuarioReg() {
		return usuarioReg;
	}
	/**
	 * Setea usuarioReg
	 * @param usuarioReg usuarioReg
	 */
	public void setUsuarioReg(String usuarioReg) {
		this.usuarioReg = usuarioReg;
	}
	/**
	 * Setea codExito
	 * @param codExito codExito
	 */
	public void setCodExito(boolean codExito) {
		this.codExito = codExito;
	}
	/**
	 * Boolean exito
	 * @return exito
	 */
	public boolean isCodExito() {
		return codExito;
	}
	/**
	 * Setea codigoOperacion
	 * @param codigoOperacion codigoOperacion
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	/**
	 * obtiene codigoOperacion
	 * @return codigoOperacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	/**
	 * Setea mensError
	 * @param mensError mensError
	 */
	public void setMensError(String mensError) {
		this.mensError = mensError;
	}
	/**
	 * obtiene mensError
	 * @return mensError
	 */
	public String getMensError() {
		return mensError;
	}
}
