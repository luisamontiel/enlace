package mx.altec.enlace.beans;

import java.io.Serializable;

public class EstadoCuentaHistoricoBean implements Serializable {
	/**
	 * Id de version generado automaticamente para la serializacion del objeto
	 */
	private static final long serialVersionUID = -7480264269656758419L;
	/** Cliente **/
	private String cliente;
	/** Cuenta del estado de cuenta solicitado **/
	private String cuentaEstadoCuenta;
	/** Periodo del estado de cuenta **/
	private String periodo;
	/** Folio de peticion **/
	private int folioPeticion;
	/** Folio Ondemand **/
	private String folioOndemand;
	/** Folio UUID **/
	private String folioUUID;
	/** Formato en que se generara el estado de cuenta **/
	private TipoFormatoEstadoCuenta formato;
	/** Numero de secuencia del domicilio **/
	private String numeroSecuenciaDomicilio;
	/** Cuenta de enlace **/
	private String cuentaEnlace;
	/** Usuario de enlace **/
	private String usuarioEnlace;
	/** Numero de secuencia historica **/
	private int numeroSecuenciaHistorica;
	/** Tipo de Estado de Cuenta **/
	private String tipoEDC;
	/** Codigo Retorno BD**/
	private String codigoRespuestaDY02;
	/** Descripcion Codigo Retorno BD**/
	private String descripcionRespuestaDY02;
	/** Codigo Retorno BD**/
	private String codigoRespuestaBD;
	/** Descripcion Codigo Retorno BD**/
	private String descripcionRespuestaBD;
	
	
	/**
	 * Obtiene el cliente
	 * @return El cliente
	 */
	public String getCliente() {
		return cliente;
	}
	/**
	 * Asigna un valor al cliente
	 * @param cliente El cliente que se va a asignar
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	/**
	 * Obtiene la cuenta del estado de cuenta
	 * @return La cuenta del estado de cuenta
	 */
	public String getCuentaEstadoCuenta() {
		return cuentaEstadoCuenta;
	}
	/**
	 * Asigna un valor para el cuenta del estado de cuenta
	 * @param cuentaEstadoCuenta El cuenta del estado de cuenta que se desea asignar
	 */
	public void setCuentaEstadoCuenta(String cuentaEstadoCuenta) {
		this.cuentaEstadoCuenta = cuentaEstadoCuenta;
	}
	/**
	 * Obtiene el periodo
	 * @return El periodo
	 */
	public String getPeriodo() {
		return periodo;
	}
	/**
	 * Asigna un valor para el periodo
	 * @param periodo El periodo que se desea asignar
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	/**
	 * Obtiene el folio de peticion
	 * @return El folio de peticion
	 */
	public int getFolioPeticion() {
		return folioPeticion;
	}
	/**
	 * Asigna un valor para el folio de peticion
	 * @param folioPeticion El folio de peticion que se desea asignar
	 */
	public void setFolioPeticion(int folioPeticion) {
		this.folioPeticion = folioPeticion;
	}
	/**
	 * Obtiene el folio Ondemand
	 * @return El folio Ondemand
	 */
	public String getFolioOndemand() {
		return folioOndemand;
	}
	/**
	 * Asigna un valor al folio Ondemand
	 * @param folioOndemand El folio Ondemand que se desea asignar
	 */
	public void setFolioOndemand(String folioOndemand) {
		this.folioOndemand = folioOndemand;
	}
	/**
	 * Obtiene el folio UUID
	 * @return El folio UUID
	 */
	public String getFolioUUID() {
		return folioUUID;
	}
	/**
	 * Asigna un valor al folio UUID
	 * @param folioUUID El folio UUID que se desea asignar
	 */
	public void setFolioUUID(String folioUUID) {
		this.folioUUID = folioUUID;
	}
	/**
	 * Obtiene el formato
	 * @return El formato
	 */
	public TipoFormatoEstadoCuenta getFormato() {
		return formato;
	}
	/**
	 * Asigna un valor al formato
	 * @param formato El formato que se desea asignar
	 */
	public void setFormato(TipoFormatoEstadoCuenta formato) {
		this.formato = formato;
	}
	/**
	 * Obtiene el numero de secuencia del domicilio
	 * @return El numero de secuencia del domicilio
	 */
	public String getNumeroSecuenciaDomicilio() {
		return numeroSecuenciaDomicilio;
	}
	/**
	 * Asigna un valor para el numero de secuencia del domicilio
	 * @param numeroSecuenciaDomicilio El nuevo numero de secuencia del domicilio
	 */
	public void setNumeroSecuenciaDomicilio(String numeroSecuenciaDomicilio) {
		this.numeroSecuenciaDomicilio = numeroSecuenciaDomicilio;
	}
	/**
	 * Obtiene la cuenta de enlace
	 * @return La cuenta de enlace
	 */
	public String getCuentaEnlace() {
		return cuentaEnlace;
	}
	/**
	 * Asiga un valor a la cuenta de enlace
	 * @param cuentaEnlace El nuevo valor de la cuenta de enlace
	 */
	public void setCuentaEnlace(String cuentaEnlace) {
		this.cuentaEnlace = cuentaEnlace;
	}
	/**
	 * Obtiene el usuario de enlace
	 * @return El usuario de enlace
	 */
	public String getUsuarioEnlace() {
		return usuarioEnlace;
	}
	/**
	 * Asigna un valor al usuario de enlace
	 * @param usuarioEnlace El nuevo usuario de enlace
	 */
	public void setUsuarioEnlace(String usuarioEnlace) {
		this.usuarioEnlace = usuarioEnlace;
	}
	/**
	 * Obtiene el numero de secuencia historica
	 * @return El numero de secuencia historica
	 */
	public int getNumeroSecuenciaHistorica() {
		return numeroSecuenciaHistorica;
	}
	/**
	 * Asigna un valor para el numero de secuencia historica
	 * @param numeroSecuenciaHistorica El nuevo numero de secuencia historica
	 */
	public void setNumeroSecuenciaHistorica(int numeroSecuenciaHistorica) {
		this.numeroSecuenciaHistorica = numeroSecuenciaHistorica;
	}
	/**
	 * Asigna el tipo de Estado de Cuenta devuelto en el llamado a la Tx OW42
	 * @param tipoEDC el tipoEDC a establecer
	 */
	public void setTipoEDC(String tipoEDC) {
		this.tipoEDC = tipoEDC;
	}
	/**
	 * Obtiene el tipo de Estado de Cuenta.x
	 * @return el tipoEDC
	 */
	public String getTipoEDC() {
		return tipoEDC;
	}
	/**
	 * @param codigoRespuestaDY02 el codigoRespuestaDY02 a establecer
	 */
	public void setCodigoRespuestaDY02(String codigoRespuestaDY02) {
		this.codigoRespuestaDY02 = codigoRespuestaDY02;
	}
	/**
	 * @return el codigoRespuestaDY02
	 */
	public String getCodigoRespuestaDY02() {
		return codigoRespuestaDY02;
	}
	/**
	 * @param descripcionRespuestaDY02 el descripcionRespuestaDY02 a establecer
	 */
	public void setDescripcionRespuestaDY02(String descripcionRespuestaDY02) {
		this.descripcionRespuestaDY02 = descripcionRespuestaDY02;
	}
	/**
	 * @return el descripcionRespuestaDY02
	 */
	public String getDescripcionRespuestaDY02() {
		return descripcionRespuestaDY02;
	}
	/**
	 * @param codigoRespuestaBD el codigoRespuestaBD a establecer
	 */
	public void setCodigoRespuestaBD(String codigoRespuestaBD) {
		this.codigoRespuestaBD = codigoRespuestaBD;
	}
	/**
	 * @return el codigoRespuestaBD
	 */
	public String getCodigoRespuestaBD() {
		return codigoRespuestaBD;
	}
	/**
	 * @param descripcionRespuestaBD el descripcionRespuestaBD a establecer
	 */
	public void setDescripcionRespuestaBD(String descripcionRespuestaBD) {
		this.descripcionRespuestaBD = descripcionRespuestaBD;
	}
	/**
	 * @return el descripcionRespuestaBD
	 */
	public String getDescripcionRespuestaBD() {
		return descripcionRespuestaBD;
	}
}