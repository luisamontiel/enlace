package mx.altec.enlace.beans;

/**
 * @author cgodinez
 *
 */
public abstract class AdmUsrGL35Base {
	
	/**
	 * Indicador si la transaccion es exitosa
	 */
	private boolean codExito;
	
	/**
	 * Cadena con el codigo de la operacion
	 */
	private String codigoOperacion;
	/**
	 * Cadena con el mensaje de error de la operacion
	 */
	private String mensError;
	/**
	 * Obtiene el indicador de exito de la operacion
	 * @return Boolean
	 */
	public boolean isCodExito() {
		return codExito;
	}

	/**
	 * Asigna el indicador de exito de la operacion
	 * @param codExito Boolean
	 */
	public void setCodExito(boolean codExito) {
		this.codExito = codExito;
	}

	/**
	 * Obtiene el codigo de salida de la operacion
	 * @return Cadena con codigo de la operacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	/**
	 * Establece el codigo de salida de la operacion
	 * @param codigoOperacion Cadena con codigo de la operacion
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	public String getMensError() {
		return mensError;
	}

	public void setMensError(String mensajeError) {
		this.mensError = mensajeError;
	}
	
}
