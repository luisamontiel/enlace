package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

public class ODD4Bean implements Serializable {

	/** Id generado automaticamente para la serializacion de la clase */
	private static final long serialVersionUID = -4147961626426623118L;
	/** Id de la trama */
	private static final String ID_TRAMA = "ODD4";
	/** Identificador de que se va a realizar rellamado */
	private static final String CON_RELLAMADO = "S";
	/** Constante para las cadenas vacias */
	private static final String CADENA_VACIA = "";	

	/** Codigo de operacion de la transaccion */
	private String codigoOperacion;
	/** Mensaje de operacion de la transaccion */
	private String mensajeOperacion;
	/** Numero de cuenta en la transaccion */
	private String numeroCuenta;
	/** Indicador de rellamada en la transaccion */
	private String indicadorRellamada;
	/** Codigo de entidad en la transaccion */
	private String codigoEntidad;
	/** Ultimo codigo de oficina devuelto en la transaccion */
	private String ultimoCodigoOficina;
	/** Ultimo codigo de producto devuelto en la transaccion */
	private String ultimoCodigoProducto;
	/** Ultimo codigo de subproducto devuelto en la transaccion */
	private String ultimoCodigoSubproducto;
	/** Ultimo tipo de uso devuelto en la transaccion */
	private String ultimoTipoUso;
	/** Numero de cliente en la transaccion */
	private String numeroCliente;
	/** Secuencia de domicilio en la transaccion */
	private String secuenciaDomicilio;
	/** Calle en la transaccion en la transaccion */
	private String calle;
	/** Numero exterior en la transaccion */
	private String numeroExterior;
	/** Numero interior en la transaccion */
	private String numeroInterior;
	/** Colonia en la transaccion */
	private String colonia;
	/** Ciudad en la transaccion */
	private String ciudad;
	/** Municipio en la transaccion */
	private String municipio;
	/** Codigo postal en la transaccion */
	private String codigoPostal;
	/** Codigo de estado en la transaccion */
	private String codigoEstado;
	/** Indicador Paperless en la transaccion */
	private String indicadorPaperless;
	/** Nombre del cliente */
	private String nombre;
	/** Apellido paterno */
	private String apellidoPaterno;
	/** Apellido materno */
	private String apellidoMaterno;
	/** Bandera que indica si la transaccion respondio con error de ambientacion de datos. */
	private boolean tieneProblemaDatos;
	/** Atributo privado para almacenar el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.*/
	private String mensajeProblemaDatos = "";
	
	
	/**
	 * Metodo para obtener el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @return String indicando el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 */
	public String getMensajeProblemaDatos() {
		return mensajeProblemaDatos;
	}
	
	/**
	 * Metodo para definir el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @param mensajeProblemaDatos : String el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 */
	public void setMensajeProblemaDatos(String mensajeProblemaDatos) {
		this.mensajeProblemaDatos = mensajeProblemaDatos;
	}
	
	/**
	 * Metodo para determinar si la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @return boolean : true indicando si la transaccion respondio con error de ambientacion de datos. 
	 */
	public boolean isTieneProblemaDatos() {
		return tieneProblemaDatos;
	}
	
	/**
	 * Metodo para definir si la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @param tieneProblemaDatos : true si la transaccion respondio con error de ambientacion de datos.
	 */
	public void setTieneProblemaDatos(boolean tieneProblemaDatos) {
		this.tieneProblemaDatos = tieneProblemaDatos;
	}

	/** Cuentas en la transaccion */
	private List<CuentaODD4Bean> cuentas;

	/** Trama de respuesta de la transaccion */
	private transient List<String> tramasRespuesta;

	
	/**
	 * Obtiene el codigo de operacion
	 * @return Codigo de operacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	/**
	 * Asigna el codigo de operacion
	 * @param codigoOperacion El codigo de operacion que se va a asignar
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	/**
	 * Obtiene el mensaje de operacion
	 * @return Mensaje de operacion
	 */
	public String getMensajeOperacion() {
		return mensajeOperacion;
	}
	/**
	 * Asigna el mensaje de operacion
	 * @param mensajeOperacion El mensaje de operacion que se va asignar
	 */
	public void setMensajeOperacion(String mensajeOperacion) {
		this.mensajeOperacion = mensajeOperacion;
	}
	/**
	 * Obtiene el numero de cuenta
	 * @return El numero de cuenta
	 * @throws ValidationException En caso de que no exista un estado de cuenta
	 */
	public String getNumeroCuenta() throws ValidationException {
		if(numeroCuenta == null){
			throw new ValidationException("No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el N&uacute;mero de Cuenta.");
		}
		
		return numeroCuenta;
	}
	/**
	 * Asigna un valor al estado de cuenta
	 * @param numeroCuenta El numero de cuenta que se va a asignar
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	/**
	 * Obtiene el indicador de rellamada
	 * @return El indicador de rellamada
	 */
	public String getIndicadorRellamada() {
		if(indicadorRellamada == null){
			indicadorRellamada = CADENA_VACIA;
		}
		
		return indicadorRellamada;
	}
	/**
	 * Asigna valor al indicador de rellamada
	 * @param indicadorRellamada El indicador de rellamada que se va a asignar
	 */
	public void setIndicadorRellamada(String indicadorRellamada) {
		this.indicadorRellamada = indicadorRellamada;
	}
	/**
	 * Obtiene el codigo de entidad
	 * @return El codigo de entidad
	 */
	public String getCodigoEntidad() {
		if(codigoEntidad == null){
			codigoEntidad = CADENA_VACIA;
		}
		
		return codigoEntidad;
	}
	/**
	 * Asigna el codigo de entidad
	 * @param codigoEntidad El codigo de entidad que se va a asignar
	 */
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	/**
	 * Obtiene el ultimo codigo de oficina
	 * @return El codigo de oficina
	 */
	public String getUltimoCodigoOficina() {
		if(ultimoCodigoOficina == null){
			ultimoCodigoOficina = CADENA_VACIA;
		}
		
		return ultimoCodigoOficina;
	}
	/**
	 * Asigna un valor al ultimo codigo de oficina
	 * @param ultimoCodigoOficina El ultimo codigo de oficina que se va a asignar
	 */
	public void setUltimoCodigoOficina(String ultimoCodigoOficina) {
		this.ultimoCodigoOficina = ultimoCodigoOficina;
	}
	/**
	 * Obtiene el ultimo codigo de producto
	 * @return El ultimo codigo de producto
	 */
	public String getUltimoCodigoProducto() {
		if(ultimoCodigoProducto == null){
			ultimoCodigoProducto = CADENA_VACIA;
		}
		
		return ultimoCodigoProducto;
	}
	/**
	 * Asigna el ultimo codigo de producto
	 * @param ultimoCodigoProducto El ultimo codigo de producto que se va a asignar
	 */
	public void setUltimoCodigoProducto(String ultimoCodigoProducto) {
		this.ultimoCodigoProducto = ultimoCodigoProducto;
	}
	/**
	 * Obtiene el ultimo codigo de subproducto
	 * @return El ultimo codigo de subproducto
	 */
	public String getUltimoCodigoSubproducto() {
		if(ultimoCodigoSubproducto == null){
			ultimoCodigoSubproducto = CADENA_VACIA;
		}
		
		return ultimoCodigoSubproducto;
	}
	/**
	 * Asigna un valor a el ultimo codigo de subproducto
	 * @param ultimoCodigoSubproducto El ultimo codigo de subproducto que se va a asignar
	 */
	public void setUltimoCodigoSubproducto(String ultimoCodigoSubproducto) {
		this.ultimoCodigoSubproducto = ultimoCodigoSubproducto;
	}
	/**
	 * Obtiene el ultimo tipo de uso
	 * @return El ultimo tipo de uso
	 */
	public String getUltimoTipoUso() {
		if(ultimoTipoUso == null){
			ultimoTipoUso = CADENA_VACIA;
		}
		
		return ultimoTipoUso;
	}
	/**
	 * Asigna el ultimo tipo de uso
	 * @param ultimoTipoUso El ultimo tipo de uso que se va a asignar
	 */
	public void setUltimoTipoUso(String ultimoTipoUso) {
		this.ultimoTipoUso = ultimoTipoUso;
	}
	/**
	 * Obtiene el numero de cliente
	 * @return El numero de cliente
	 */
	public String getNumeroCliente() {
		if(numeroCliente == null){
			numeroCliente = CADENA_VACIA;
		}
		
		return numeroCliente;
	}
	/**
	 * Asigna el numero de cliente
	 * @param numeroCliente El numero de cliente que se va a asignar
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}
	/**
	 * Obtiene la secuencia de domicilio
	 * @return La secuencia de domicilio
	 */
	public String getSecuenciaDomicilio() {
		if(secuenciaDomicilio == null){
			secuenciaDomicilio = CADENA_VACIA;
		}
		
		return secuenciaDomicilio;
	}
	/**
	 * Asigna la secuencia de domicilio
	 * @param secuenciaDomicilio La secuencia de domicilio que se va a asignar
	 */
	public void setSecuenciaDomicilio(String secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}
	/**
	 * Obtiene el domicilio completo
	 * @return Los valores de la calle, numero interior, numero exterior, colonia, ciudad y municipio
	 */
	public String obtenerDomicilioCompleto(){
		return String.format("%s %s %s, %s %s, %s", 
				getCalle(), getNumeroInterior(), getNumeroExterior(), 
				getColonia(), getCiudad(), getMunicipio());
	}
	/**
	 * Obtiene la calle.
	 * @return La calle La calle del domicilio
	 */
	public String getCalle() {
		if(calle == null){
			calle = CADENA_VACIA;
		}
		return calle;
	}
	/**
	 * Asigna un valor a la calle.
	 * @param calle : calle
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * Obtiene el numero exterior
	 * @return El numero exterior
	 */
	public String getNumeroExterior() {
		if(numeroExterior == null){
			numeroExterior = CADENA_VACIA;
		}
		
		return numeroExterior;
	}
	/**
	 * Asigna un valor al numero exterior
	 * @param numeroExterior El numero exterior que se va a asignar
	 */
	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}
	/**
	 * Obtiene el numero interior
	 * @return El numero interior
	 */
	public String getNumeroInterior() {
		if(numeroInterior == null){
			numeroInterior = CADENA_VACIA;
		}
		
		return numeroInterior;
	}
	/**
	 * Asigna el numero interior
	 * @param numeroInterior El numero interior que se va a asignar
	 */
	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}
	/**
	 * Obtiene la colonia
	 * @return La colonia
	 */
	public String getColonia() {
		if(colonia == null){
			colonia = CADENA_VACIA;
		}
		
		return colonia;
	}
	/**
	 * Asigna la colonia
	 * @param colonia La colonia que se va a asignar
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	/**
	 * Obtiene la ciudad
	 * @return La ciudad
	 */
	public String getCiudad() {
		if(ciudad == null){
			ciudad = CADENA_VACIA;
		}
		
		return ciudad;
	}
	/**
	 * Asigna la ciudad
	 * @param ciudad La ciudad que se va a asignar
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * Obtiene el municipio
	 * @return El municipio
	 */
	public String getMunicipio() {
		if(municipio == null){
			municipio = CADENA_VACIA;
		}
		
		return municipio;
	}
	/**
	 * Asigna el municipio
	 * @param municipio El municipio que se va a asignar
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	/**
	 * Obtiene el codigo postal
	 * @return El codigo postal
	 */
	public String getCodigoPostal() {
		if(codigoPostal == null){
			codigoPostal = CADENA_VACIA;
		}
		
		return codigoPostal;
	}
	/**
	 * Asigna el codigo postal
	 * @param codigoPostal El codigo postal que se va a asignar
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	/**
	 * Obtiene el codigo de estado
	 * @return El codigo de estado
	 */
	public String getCodigoEstado() {
		if(codigoEstado == null){
			codigoEstado = CADENA_VACIA;
		}
		
		return codigoEstado;
	}
	/**
	 * Asigna el codigo de estado
	 * @param codigoEstado El codigo de estado que se va a asginar
	 */
	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	/**
	 * Obtiene el indicador de Paperless
	 * @return El indicador de Paperless
	 */
	public String getIndicadorPaperless() {
		if(indicadorPaperless == null){
			indicadorPaperless = CADENA_VACIA;
		}
		
		return indicadorPaperless;
	}
	/**
	 * Asigna el indicador de Paperless
	 * @param indicadorPaperless El indicador de Paperless que se va a asignar
	 */
	public void setIndicadorPaperless(String indicadorPaperless) {
		this.indicadorPaperless = indicadorPaperless;
	}
	/**
	 * Obtiene el nombre completo
	 * @return Obtiene los valores de nombre, apellido paterno y apellido materno
	 */
	public String obtenerNombreCompleto(){
		return String.format("%s %s %s", 
				getNombre(), getApellidoPaterno(), getApellidoMaterno());
	}
	/**
	 * Obtiene el nombre
	 * @return El nombre
	 */
	public String getNombre() {
		if(nombre == null){
			nombre = CADENA_VACIA;
		}
		
		return nombre;
	}
	/**
	 * Asigna el nombre
	 * @param nombre El nombre que se va a asignar
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * Obtiene el apellido paterno
	 * @return El apellido paterno
	 */
	public String getApellidoPaterno() {
		if(apellidoPaterno == null){
			apellidoPaterno = CADENA_VACIA;
		}
		
		return apellidoPaterno;
	}
	/**
	 * Asigna el apellido paterno
	 * @param apellidoPaterno El apellido paterno que se va a asignar
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * Obtiene el apellido materno
	 * @return El apellido materno
	 */
	public String getApellidoMaterno() {
		if(apellidoMaterno == null){
			apellidoMaterno = CADENA_VACIA;
		}
		
		return apellidoMaterno;
	}
	/**
	 * Asigna el apellido materno
	 * @param apellidoMaterno El apellido materno que se va a asignar
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	

	
	/**
	 * Obtiene las cuentas
	 * @return Las cuentas
	 */
	public List<CuentaODD4Bean> getCuentas() {
		if(cuentas == null){
			cuentas = new ArrayList<CuentaODD4Bean>();
		}
		
		return cuentas;
	}
	/**
	 * Obtiene los numeros de cuenta
	 * @return Los numeros de cuenta
	 */
	public List<String> obtenerNumerosDeCuenta(){
		final List<String> numeroCuentas = new ArrayList<String>();
		
		if(cuentas != null){
			for(CuentaODD4Bean cuentaODD4 : cuentas){
				numeroCuentas.add(cuentaODD4.getNumeroCuenta());
			}
		}
		
		return numeroCuentas;
	}
	/**
	 * Asigna las cuentas
	 * @param cuentas Las cuentas que se van a asignar
	 */
	public void setCuentas(List<CuentaODD4Bean> cuentas) {
		this.cuentas = cuentas;
	}
	
	/**
	 * Agrega una cuenta
	 * @param cuenta La cuenta que se desea agregar
	 */
	public void agregarCuenta(CuentaODD4Bean cuenta){
		if(cuentas == null){
			cuentas = new ArrayList<CuentaODD4Bean>();
		}
		cuentas.add(cuenta);
	}

	
	/**
	 * Con la informacion del objeto crea el mensaje de entrada para la trama ODD4
	 * @return El mensaje de la trama ODD4
	 * @throws ValidationException En caso de que hagan falta datos
	 */
	public String generarMensajeEntrada() throws ValidationException {
		final StringBuilder trama = new StringBuilder();		
		trama.append(rellenar(getNumeroCuenta(), 12));
		EIGlobal.mensajePorTrace("trama YY 1-->"+trama.toString()+"<--- ",EIGlobal.NivelLog.DEBUG);
		trama.append(rellenar(getIndicadorPaperless(), 1));
		EIGlobal.mensajePorTrace("trama YY 2-->"+trama.toString()+"<--- ",EIGlobal.NivelLog.DEBUG);
		
		return agregarEncabezadoPS7(ID_TRAMA, trama);
	}
	/**
	 * Con la informacion del objeto crea el mensaje de entrada para el rellamado de la trama ODD4
	 * @return El mensaje de rellamado de la trama ODD4
	 * @throws ValidationException En caso de hagan falta datos
	 */
	public String generarMensajeEntradaRellamado() throws ValidationException {
		final StringBuilder trama = new StringBuilder();
		//056722702270 S 0014 5612 01 0056 C
		trama
			.append(rellenar(obtenerUltimoNumeroCuenta(), 12))
			.append(CON_RELLAMADO)
			.append(rellenar(obtenerUltimoCodigoEntidad(), 4))
			.append(rellenar(obtenerUltimoCodigoOficina(), 4))
			.append(rellenar(obtenerUltimoCodigoProducto(), 2))
			.append(rellenar(obtenerUltimoCodigoSubproducto(), 4))
			.append(rellenar(getUltimoTipoUso(), 3))
			.append(rellenar(getNumeroCliente(), 8))
			.append(rellenar(getSecuenciaDomicilio(), 3));
		return agregarEncabezadoPS7(ID_TRAMA, trama);
	}
	/**
	 * Obtiene el numero numero de cuenta obtenido
	 * @return EL ultimo numero de cuenta
	 * @throws ValidationException En caso de que no sea posible obtener el ultimo numero de cuenta
	 */
	private String obtenerUltimoNumeroCuenta() throws ValidationException {
		if(cuentas == null || cuentas.isEmpty()){
			throw new ValidationException("No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el N&uacute;mero de Cuenta.");
		}
		
		return cuentas.get(cuentas.size() - 1).getNumeroCuenta();
	}
	/**
	 * Obtiene el ultimo codigo de entidad
	 * @return El ultimo codigo de entidad
	 * @throws ValidationException En caso de que no sea posible obtener el ultimo codigo de entidad
	 */
	private String obtenerUltimoCodigoEntidad() throws ValidationException {
		if(cuentas == null || cuentas.isEmpty()){
			throw new ValidationException("No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el C&oacute;digo de Entidad.");
		}
		
		return cuentas.get(cuentas.size() - 1).getCodigoEntidad();
	}
	/**
	 * Obtiene el ultimo codigo de oficina
	 * @return El ultimo codigo de oficina
	 * @throws ValidationException En caso de que no sea posible obtener el ultimo codigo de oficina
	 */
	private String obtenerUltimoCodigoOficina() throws ValidationException {
		if(cuentas == null || cuentas.isEmpty()){
			throw new ValidationException("No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el C&oacute;digo de Oficina.");
		}
		
		return cuentas.get(cuentas.size() - 1).getCodigoOficina();
	}
	/**
	 * Obtien el ultimo codigo de producto
	 * @return El ultimo codigo de producto
	 * @throws ValidationException En caso de que no sea posible obtener el ultimo codigo de producto
	 */
	private String obtenerUltimoCodigoProducto() throws ValidationException {
		if(cuentas == null || cuentas.isEmpty()){
			throw new ValidationException("No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el C&oacute;digo de Producto.");
		}
		
		return cuentas.get(cuentas.size() - 1).getCodigoProducto();
	}
	/**
	 * Obtiene el ultimo codigo de subproducto
	 * @return El ultimo codigo de subproducto
	 * @throws ValidationException En caso de que no sea posible obtener el ultimo codigo de subproducto
	 */
	private String obtenerUltimoCodigoSubproducto() throws ValidationException {
		if(cuentas == null || cuentas.isEmpty()){
			throw new ValidationException("No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el C&oacute;digo de Subproducto.");
		}
		
		return cuentas.get(cuentas.size() - 1).getCodigoSubproducto();
	}
	
	/**
	 * Obtiene las tramas de respuesta recibidas de la transaccion ODD4
	 * @return Lista con las tramas de respuesta
	 */
	public List<String> getTramasRespuesta(){
		if(tramasRespuesta == null){
			tramasRespuesta = new ArrayList<String>();
		}
		
		return tramasRespuesta;
	}
	
	/**
	 * Agrega una trama de respuesta a la lista
	 * @param tramaRespuesta La trama de respuesta que se desea almacenar
	 */
	private void agregarTrama(String tramaRespuesta){
		if(tramasRespuesta == null){
			tramasRespuesta = new ArrayList<String>();
		}
		
		tramasRespuesta.add(tramaRespuesta);
	}
	
	/**
	 * Procesa el mensaje de respuesta obtenido de la ejecucion de la transaccion ODD4
	 * @param mensajeRespuesta El mensaje de respuesta que se desea procesar
	 */
	public void procesarMensajeRespuesta(String mensajeRespuesta){
		
		final String[] cuentas = mensajeRespuesta.split("@");
		final String mensajeResp = cuentas[3];
		
		if(mensajeResp.length() >= 313) {
			final CuentaODD4Bean cuenta1 = new CuentaODD4Bean();
			cuenta1.setNumeroCuenta(mensajeResp.substring(11, 13));
			cuenta1.setCodigoEntidad(mensajeResp.substring(23, 27));
			cuenta1.setCodigoOficina(mensajeResp.substring(27, 31));
			cuenta1.setCodigoProducto(mensajeResp.substring(31, 33));
			cuenta1.setCodigoSubproducto(mensajeResp.substring(33, 37));
			cuenta1.setIndicadorEstatusCuenta(mensajeResp.substring(37, 38));
			
			setUltimoTipoUso(mensajeResp.substring(38,41).trim());
			setNumeroCliente(mensajeResp.substring(41,49).trim());
			setSecuenciaDomicilio(mensajeResp.substring(49,52).trim());
			setCalle(mensajeResp.substring(52,102).trim());
			setNumeroExterior(mensajeResp.substring(102,117).trim());
			setNumeroInterior(mensajeResp.substring(117,132).trim());
			setColonia(mensajeResp.substring(132,162).trim());
			setCiudad(mensajeResp.substring(162,192).trim());
			setMunicipio(mensajeResp.substring(192,222).trim());
			setCodigoPostal(mensajeResp.substring(222,230).trim());
			setCodigoEstado(mensajeResp.substring(230,232).trim());
			setIndicadorPaperless(mensajeResp.substring(232,233).trim());
			setNombre(mensajeResp.substring(233,273).trim());
			setApellidoPaterno(mensajeResp.substring(273,293).trim());
			setApellidoMaterno(mensajeResp.substring(293,313).trim());
			
			for(int contadorCuentas = 4; contadorCuentas < cuentas.length ; contadorCuentas++){
				EIGlobal.mensajePorTrace("Cuenta de la trama a procesar: " + cuentas[contadorCuentas],EIGlobal.NivelLog.DEBUG);
				
				final CuentaODD4Bean cuenta = new CuentaODD4Bean();
				EIGlobal.mensajePorTrace("setNumeroCuenta ["+cuentas[contadorCuentas].substring(11, 23)+"]",EIGlobal.NivelLog.DEBUG);
				cuenta.setNumeroCuenta(cuentas[contadorCuentas].substring(11, 23));
				cuenta.setCodigoEntidad(cuentas[contadorCuentas].substring(23, 27));
				cuenta.setCodigoOficina(cuentas[contadorCuentas].substring(27, 31));
				cuenta.setCodigoProducto(cuentas[contadorCuentas].substring(31, 33));
				cuenta.setCodigoSubproducto(cuentas[contadorCuentas].substring(33, 37));
				cuenta.setIndicadorEstatusCuenta(cuentas[contadorCuentas].substring(37, 38));
				
				agregarCuenta(cuenta);
			}
		}
		
	}
	
	/**
	 * Procesa el mensaje de respuesta obtenido de la ejecucion de un rellamado de la transaccion ODD4
	 * @param mensajeRespuesta El mensaje de respuesta que se desea procesar
	 */
	public void procesarMensajeRespuestaRellamado(String mensajeRespuesta){
		agregarTrama(mensajeRespuesta);
		
		final String[] cuentas = mensajeRespuesta.split("@");
		
		for(int contadorCuentas = 4; contadorCuentas <= cuentas.length-1; contadorCuentas++){
			
			EIGlobal.mensajePorTrace("---->Cuenta:" + contadorCuentas  + " -- " + cuentas[contadorCuentas],
					NivelLog.DEBUG);
			
			final CuentaODD4Bean cuenta = new CuentaODD4Bean();
			cuenta.setNumeroCuenta(cuentas[contadorCuentas].substring(11, 23));
			cuenta.setCodigoEntidad(cuentas[contadorCuentas].substring(23, 27));
			cuenta.setCodigoOficina(cuentas[contadorCuentas].substring(27, 31));
			cuenta.setCodigoProducto(cuentas[contadorCuentas].substring(31, 33));
			cuenta.setCodigoSubproducto(cuentas[contadorCuentas].substring(33, 37));
			cuenta.setIndicadorEstatusCuenta(cuentas[contadorCuentas].substring(37, 38));
			
			
			agregarCuenta(cuenta);
			
			
		}
	}
	
	/**
	 * Agrega el encabezado PS7 a la trama especificada
	 * @param transaccion Nombre de la transaccion
	 * @param trama Mensaje de entrada de la transaccion
	 * @return Mensaje de entrada con el encabezado PS7
	 */
	private String agregarEncabezadoPS7(String transaccion, StringBuilder trama) {
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Inicio", EIGlobal.NivelLog.INFO);
		final StringBuffer tramaBuffer = new StringBuffer();
		tramaBuffer.append(rellenar(NP_MQ_TERMINAL, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(ADMUSR_MQ_USUARIO, 8, ' ', 'D'));
		tramaBuffer.append(rellenar(transaccion, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(Integer.toString(334), 4, '0', 'I'));
		tramaBuffer.append("1123451O00N2");
		tramaBuffer.append(trama);

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Fin", EIGlobal.NivelLog.INFO);
		return tramaBuffer.toString();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
