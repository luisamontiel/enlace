package mx.altec.enlace.beans;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class Pkcs12Bean {

	
	/**
	 * key PrivateKey llave privada del certificado
	 */
	private PrivateKey key;
    
	/**
	 * cert X509Certificate certificado en formato X509
	 */
	private X509Certificate cert;
    
    /**
     * @return key llave privada del certificado
     */
    public PrivateKey getKey() {
          return key;
    }
    
    /**
     * @param key : key llave privada del certificado
     */
    public void setKey(PrivateKey key) {
          this.key = key;
    }
    
    /**
     * @return cert en formato X509
     */
    public X509Certificate getCert() {
          return cert;
    }
    
    /**
     * @param cert : cert en formato X509
     */
    public void setCert(X509Certificate cert) {
          this.cert = cert;
    }

}
