/**
 * Isban Mexico
 *   Clase: DY03Bean.java
 *   Descripci�n: 
 *
 *   Control de Cambios:
 *   1.0 27/06/2013 Stefanini - Creaci�n
 */
package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * The Class DY03Bean.
 */
public class DY03Bean implements Serializable {

	/** Id de version generado automaticamente para la serializacion del objeto. */
	private static final long serialVersionUID = -4270818680990241547L;

	/** The Constant ID_TRAMA. */
	private static final String ID_TRAMA = "DY03";
	
	/** The Constant OPCION. */
	private static final String OPCION = "C";
	
	/** The Constant TIPO_CUENTA. */
	private static final String TIPO_CUENTA = "001";

	/** The codigo cliente. */
	private String codigoCliente;
	
	/** The cuenta. */
	private String cuenta;
	
	/** The formato. */
	private TipoFormatoEstadoCuenta formato;
	
	/** The folio. */
	private String folio;
	
	/** The periodo. */
	private String periodo;

	/** The codigo respuesta. */
	private String codigoRespuesta;
	
	/** The descripcion respuesta. */
	private String descripcionRespuesta;
	
	/** The detalle periodo. */
	private List<DY03DetallePeriodoBean> detallePeriodo;

	/**
	 * Gets the codigo cliente.
	 *
	 * @return the codigo cliente
	 * @throws ValidationException the validation exception
	 */
	public String getCodigoCliente() throws ValidationException {
		if (codigoCliente == null || StringUtils.isBlank(codigoCliente)) {
			throw new ValidationException("DY03E01 El c�digo es requerido");
		}
		return codigoCliente;
	}

	/**
	 * Sets the codigo cliente.
	 *
	 * @param codigoCliente the new codigo cliente
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	/**
	 * Gets the cuenta.
	 *
	 * @return the cuenta
	 * @throws ValidationException the validation exception
	 */
	public String getCuenta() throws ValidationException {
		if (cuenta == null || StringUtils.isBlank(cuenta)) {
			throw new ValidationException("DY03E02 La cuenta es requerida");
		}
		return cuenta;
	}

	/**
	 * Sets the cuenta.
	 *
	 * @param cuenta the new cuenta
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * Gets the formato.
	 *
	 * @return the formato
	 * @throws ValidationException the validation exception
	 */
	public TipoFormatoEstadoCuenta getFormato() throws ValidationException {
		if (formato == null) {
			throw new ValidationException("DY03E03 El formato es requerido");
		}
		return formato;
	}

	/**
	 * Gets the id formato.
	 *
	 * @return the id formato
	 * @throws ValidationException the validation exception
	 */
	public int getIdFormato() throws ValidationException {
		if (formato == null) {
			throw new ValidationException("DY03E03 El formato es requerido");
		}
		return formato.obtenerIdFormato();
	}

	/**
	 * Sets the formato.
	 *
	 * @param formato the new formato
	 */
	public void setFormato(TipoFormatoEstadoCuenta formato) {
		this.formato = formato;
	}

	/**
	 * Gets the periodo.
	 *
	 * @return the periodo
	 * @throws ValidationException the validation exception
	 */
	public String getPeriodo() throws ValidationException {
		if (periodo == null) {
			throw new ValidationException("DY03E04 El periodo es requerido");
		}
		return periodo;
	}

	/**
	 * Sets the periodo.
	 *
	 * @param periodo the new periodo
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/**
	 * Gets the folio.
	 *
	 * @return the folio
	 * @throws ValidationException the validation exception
	 */
	public String getFolio() throws ValidationException {
		if (folio == null) {
			throw new ValidationException("DY03E05 El folio es requerido");
		}
		return folio;
	}

	/**
	 * Sets the folio.
	 *
	 * @param folio the new folio
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * Gets the codigo respuesta.
	 *
	 * @return the codigo respuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * Sets the codigo respuesta.
	 *
	 * @param codigoRespuesta the new codigo respuesta
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	/**
	 * Gets the descripcion respuesta.
	 *
	 * @return the descripcion respuesta
	 */
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}

	/**
	 * Sets the descripcion respuesta.
	 *
	 * @param descripcionRespuesta the new descripcion respuesta
	 */
	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}
	
	/**
	 * Gets the detalle periodo.
	 *
	 * @return the detalle periodo
	 */
	public List<DY03DetallePeriodoBean> getDetallePeriodo() {
		return detallePeriodo;
	}

	/**
	 * Sets the detalle periodo.
	 *
	 * @param detallePeriodo the new detalle periodo
	 */
	public void setDetallePeriodo(List<DY03DetallePeriodoBean> detallePeriodo) {
		this.detallePeriodo = detallePeriodo;
	}

	/**
	 * Generar mensaje entrada.
	 *
	 * @return the string
	 * @throws ValidationException the validation exception
	 */
	public String generarMensajeEntrada() throws ValidationException {
		StringBuilder trama = new StringBuilder();
		
		trama
			.append(rellenar(OPCION, 1))
			.append(rellenar(getCodigoCliente(), 8, '0', 'I'))
			.append(rellenar(getCuenta(), 20, ' ','I'))
			.append(getIdFormato())
			.append(rellenar(TIPO_CUENTA, 3))
			.append(rellenar(getFolio(), 10, ' ', 'I'))
			.append(rellenar(getPeriodo(), 6, ' ', 'I'));
		return agregarEncabezadoPS7(ID_TRAMA, trama);
	}
	
	/**
	 * Agregar encabezado p s7.
	 *
	 * @param transaccion the transaccion
	 * @param trama the trama
	 * @return the string
	 */
	private String agregarEncabezadoPS7(String transaccion, StringBuilder trama) {
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Inicio", EIGlobal.NivelLog.INFO);
		StringBuffer tramaBuffer = new StringBuffer();
		tramaBuffer.append(rellenar(NP_MQ_TERMINAL, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(ADMUSR_MQ_USUARIO, 8, ' ', 'D'));
		tramaBuffer.append(rellenar(transaccion, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(Integer.toString(trama.length() + 32), 4, '0', 'I'));
		tramaBuffer.append("1123451O00N2");
		tramaBuffer.append(trama);

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Fin", EIGlobal.NivelLog.INFO);
		return tramaBuffer.toString();
	}
	
	/**
	 * Procesar mensaje respuesta.
	 *
	 * @param detalleStr the detalle str
	 */
	public void procesarDetalleRespuesta(String detalleStr){
		EIGlobal.mensajePorTrace("Procesando respuesta DY03", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Mensaje: [" + detalleStr + "]", EIGlobal.NivelLog.DEBUG);
		
		detallePeriodo = new ArrayList<DY03DetallePeriodoBean>();
		if(detalleStr != null){
			DY03DetallePeriodoBean detallePeriodoBean = new DY03DetallePeriodoBean();
			detallePeriodoBean.procesarMensajeRespuesta(detalleStr);
			detallePeriodo.add(detallePeriodoBean);
		} else {
			EIGlobal.mensajePorTrace("Trama DY03 vacia", EIGlobal.NivelLog.DEBUG);
		}
	}
}

