package mx.altec.enlace.beans;

import java.util.Date;
import java.util.List;

public class ConfEdosCtaIndBean {

	/**
	 * Cuenta
	 */
	private String cuenta;
	/**
	 * Lista de cuentas
	 */
	private List<String> cuentas;
	/**
	 * Codigo de cliente
	 */
	private String codCliente;
	/**
	 * Codigo de cliente de personas
	 */
	private String codClientePersonas;
	/**
	 * Indicador de paperless
	 */
	private String idPaperless;
	/**
	 * Nombre
	 */
	private String nombre;
	/**
	 * Apellido paterno
	 */
	private String apPaterno;
	/**
	 * Apellido materno
	 */
	private String apMaterno;
	/**
	 * Estado de cta disponible
	 */
	private boolean edoCtaDisponible;
	/**
	 * Suscripcion a paperless
	 */
	private boolean suscripPaperless;
	/**
	 * Oficina
	 */
	private String oficina;
	/**
	 * Producto
	 */
	private String producto;
	/**
	 * Subproducto
	 */
	private String subproducto;
	/**
	 * Uso
	 */
	private String uso;
	/**
	 * Secuencia de domicilio
	 */
	private String secDomicilio;
	/**
	 * Calle
	 */
	private String calle;
	/**
	 * Numero exterior
	 */
	private String noExterior;
	/**
	 * Numero interior
	 */
	private String noInterior;
	/**
	 * Colonia
	 */
	private String colonia;
	/**
	 * Ciudad
	 */
	private String ciudad;
	/**
	 * Delegacion / Municipio
	 */
	private String delegMunic;
	/**
	 * Codigo postal
	 */
	private String codPostal;
	/**
	 * Estado
	 */
	private String estado;
	/**
	 * Domicilio completo
	 */
	private String domCompleto;
	/**
	 * Codigo de retorno
	 */
	private String codRetorno;
	/**
	 * Descripcion de retorno
	 */
	private String descRetorno;
	/**
	 * Folio de operacion
	 */
	private String folioOp;
	/**
	 * Contrato
	 */
	private String contrato;
	/**
	 * Fecha de configuracion
	 */
	private Date fechaConfig;
	/**
	 * Periodo disponible
	 */
	private String periodoDisp;
	/**
	 * Folio ondemand
	 */
	private String folioOnD;
	/**
	 * Folio UUID
	 */
	private String folioUUID;
	/**
	 * Descripcion de la cuenta
	 */
	private String descCuenta;
	
	/** Bandera que indica si la transaccion respondio con error de ambientacion de datos. */
	private boolean tieneProblemaDatos;
	
	/** Atributo privado para almacenar el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.*/
	private String mensajeProblemaDatos = "";
	
	/**
	 * @return el cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * @param cuenta el cuenta a establecer
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * @return el cuentas
	 */
	public List<String> getCuentas() {
		return cuentas;
	}
	/**
	 * @param cuentas el cuentas a establecer
	 */
	public void setCuentas(List<String> cuentas) {
		this.cuentas = cuentas;
	}
	/**
	 * @return el codCliente
	 */
	public String getCodCliente() {
		return codCliente;
	}
	/**
	 * @param codCliente el codCliente a establecer
	 */
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}
	/**
	 * @return codigo de cliente de personas.
	 */
	public String getCodClientePersonas() {
		return codClientePersonas;
	}
	/**
	 * @param codClientePersonas con el codigo de cliente de personas. 
	 */
	public void setCodClientePersonas(String codClientePersonas) {
		this.codClientePersonas = codClientePersonas;
	}
	/**
	 * @return el idPaperless
	 */
	public String getIdPaperless() {
		return idPaperless;
	}
	/**
	 * @param idPaperless el idPaperless a establecer
	 */
	public void setIdPaperless(String idPaperless) {
		this.idPaperless = idPaperless;
	}
	/**
	 * @return el nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre el nombre a establecer
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return el apPaterno
	 */
	public String getApPaterno() {
		return apPaterno;
	}
	/**
	 * @param apPaterno el apPaterno a establecer
	 */
	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}
	/**
	 * @return el apMaterno
	 */
	public String getApMaterno() {
		return apMaterno;
	}
	/**
	 * @param apMaterno el apMaterno a establecer
	 */
	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}
	/**
	 * @return el edoCtaDisponible
	 */
	public boolean isEdoCtaDisponible() {
		return edoCtaDisponible;
	}
	/**
	 * @param edoCtaDisponible el edoCtaDisponible a establecer
	 */
	public void setEdoCtaDisponible(boolean edoCtaDisponible) {
		this.edoCtaDisponible = edoCtaDisponible;
	}
	/**
	 * @return el suscripPaperless
	 */
	public boolean isSuscripPaperless() {
		return suscripPaperless;
	}
	/**
	 * @param suscripPaperless el suscripPaperless a establecer
	 */
	public void setSuscripPaperless(boolean suscripPaperless) {
		this.suscripPaperless = suscripPaperless;
	}
	/**
	 * @return el oficina
	 */
	public String getOficina() {
		return oficina;
	}
	/**
	 * @param oficina el oficina a establecer
	 */
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	/**
	 * @return el producto
	 */
	public String getProducto() {
		return producto;
	}
	/**
	 * @param producto el producto a establecer
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/**
	 * @return el subproducto
	 */
	public String getSubproducto() {
		return subproducto;
	}
	/**
	 * @param subproducto el subproducto a establecer
	 */
	public void setSubproducto(String subproducto) {
		this.subproducto = subproducto;
	}
	/**
	 * @return el uso
	 */
	public String getUso() {
		return uso;
	}
	/**
	 * @param uso el uso a establecer
	 */
	public void setUso(String uso) {
		this.uso = uso;
	}
	/**
	 * @return el secDomicilio
	 */
	public String getSecDomicilio() {
		return secDomicilio;
	}
	/**
	 * @param secDomicilio el secDomicilio a establecer
	 */
	public void setSecDomicilio(String secDomicilio) {
		this.secDomicilio = secDomicilio;
	}
	/**
	 * @return el calle
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * @param calle el calle a establecer
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * @return el noExterior
	 */
	public String getNoExterior() {
		return noExterior;
	}
	/**
	 * @param noExterior el noExterior a establecer
	 */
	public void setNoExterior(String noExterior) {
		this.noExterior = noExterior;
	}
	/**
	 * @return el noInterior
	 */
	public String getNoInterior() {
		return noInterior;
	}
	/**
	 * @param noInterior el noInterior a establecer
	 */
	public void setNoInterior(String noInterior) {
		this.noInterior = noInterior;
	}
	/**
	 * @return el colonia
	 */
	public String getColonia() {
		return colonia;
	}
	/**
	 * @param colonia el colonia a establecer
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	/**
	 * @return el ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * @param ciudad el ciudad a establecer
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * @return el delegMunic
	 */
	public String getDelegMunic() {
		return delegMunic;
	}
	/**
	 * @param delegMunic el delegMunic a establecer
	 */
	public void setDelegMunic(String delegMunic) {
		this.delegMunic = delegMunic;
	}
	/**
	 * @return el codPostal
	 */
	public String getCodPostal() {
		return codPostal;
	}
	/**
	 * @param codPostal el codPostal a establecer
	 */
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}
	/**
	 * @return el estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado el estado a establecer
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return el codRetorno
	 */
	public String getCodRetorno() {
		return codRetorno;
	}
	/**
	 * @param codRetorno el codRetorno a establecer
	 */
	public void setCodRetorno(String codRetorno) {
		this.codRetorno = codRetorno;
	}
	/**
	 * @return el descRetorno
	 */
	public String getDescRetorno() {
		return descRetorno;
	}
	/**
	 * @param descRetorno el descRetorno a establecer
	 */
	public void setDescRetorno(String descRetorno) {
		this.descRetorno = descRetorno;
	}
	/**
	 * @return el domCompleto
	 */
	public String getDomCompleto() {
		return domCompleto;
	}
	/**
	 * @param domCompleto el domCompleto a establecer
	 */
	public void setDomCompleto(String domCompleto) {
		this.domCompleto = domCompleto;
	}
	/**
	 * @return el contrato
	 */
	public String getContrato() {
		return contrato;
	}
	/**
	 * @param contrato el contrato a establecer
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * @return el fechaConfig
	 */
	public Date getFechaConfig() {
		return fechaConfig;
	}
	/**
	 * @param fechaConfig el fechaConfig a establecer
	 */
	public void setFechaConfig(Date fechaConfig) {
		this.fechaConfig = fechaConfig;
	}
	/**
	 * @return el periodoDisp
	 */
	public String getPeriodoDisp() {
		return periodoDisp;
	}
	/**
	 * @param periodoDisp el periodoDisp a establecer
	 */
	public void setPeriodoDisp(String periodoDisp) {
		this.periodoDisp = periodoDisp;
	}
	/**
	 * @return el folioOp
	 */
	public String getFolioOp() {
		return folioOp;
	}
	/**
	 * @param folioOp el folioOp a establecer
	 */
	public void setFolioOp(String folioOp) {
		this.folioOp = folioOp;
	}
	/**
	 * @return el folioOnD
	 */
	public String getFolioOnD() {
		return folioOnD;
	}
	/**
	 * @param folioOnD el folioOnD a establecer
	 */
	public void setFolioOnD(String folioOnD) {
		this.folioOnD = folioOnD;
	}
	/**
	 * @return el folioUUID
	 */
	public String getFolioUUID() {
		return folioUUID;
	}
	/**
	 * @param folioUUID el folioUUID a establecer
	 */
	public void setFolioUUID(String folioUUID) {
		this.folioUUID = folioUUID;
	}
	/**
	 * @return el descCuenta
	 */
	public String getDescCuenta() {
		return descCuenta;
	}
	/**
	 * @param descCuenta el descCuenta a establecer
	 */
	public void setDescCuenta(String descCuenta) {
		this.descCuenta = descCuenta;
	}
	
	/**
	 * Metodo para determinar si la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @return boolean : true indicando si la transaccion respondio con error de ambientacion de datos.
	 */
	public boolean isTieneProblemaDatos() {
		return tieneProblemaDatos;
	}
	
	/**
	 * Metodo para definir si la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @param tieneProblemaDatos : true si la transaccion respondio con error de ambientacion de datos.
	 */
	public void setTieneProblemaDatos(boolean tieneProblemaDatos) {
		this.tieneProblemaDatos = tieneProblemaDatos;
	}
		
	/**
	 * Metodo para obtener el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @return String indicando el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 */
	public String getMensajeProblemaDatos() {
		return mensajeProblemaDatos;
	}
	
	/**
	 * Metodo para definir el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 * 
	 * @param mensajeProblemaDatos : String el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.
	 */
	public void setMensajeProblemaDatos(String mensajeProblemaDatos) {
		this.mensajeProblemaDatos = mensajeProblemaDatos;
	}
}
