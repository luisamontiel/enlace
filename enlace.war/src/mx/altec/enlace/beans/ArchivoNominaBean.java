package mx.altec.enlace.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ArchivoNominaBean {
	
	String tipoAplicacion; //Linea - Programada
	String fecha;
	String hora;
	String ctaCargo;	
	String contrato;
	String fechaEnvio;
	String fechaRecepcion;
	
	int    numRegistros;	
	double impTotal;
	LinkedHashMap< String, ArchivoNominaDetBean> detalleArchivo;
	public String getTipoAplicacion() {
		return tipoAplicacion;
	}


	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getCtaCargo() {
		return ctaCargo;
	}

	public void setCtaCargo(String ctaCargo) {
		this.ctaCargo = ctaCargo;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public int getNumRegistros() {
		return numRegistros;
	}

	public void setNumRegistros(int numRegistros) {
		this.numRegistros = numRegistros;
	}

	public double getImpTotal() {
		return impTotal;
	}

	public void setImpTotal(double impTotal) {
		this.impTotal = impTotal;
	}


	public LinkedHashMap<String, ArchivoNominaDetBean> getDetalleArchivo() {
		return detalleArchivo;
	}

	public void setDetalleArchivo(
			LinkedHashMap<String, ArchivoNominaDetBean> detalleArchivo) {
		this.detalleArchivo = detalleArchivo;
	}




}