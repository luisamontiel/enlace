/**
 * ISBAN M�xico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * mx.altec.enlace.beans.MMC_SpidBean.java
 *
 * Control de versiones:
 *
 * Version 	Date	 	By 		        Company 	Description
 * ------- 	------  	-------------   -----------  ------------------------------
 * 1.0		12-16		M Fuentes	    IDS			   Creaci�n
 */

package mx.altec.enlace.beans;

/**
 * Bean para el viaje de informacion bancos SPID.
 * 
 * @author FSW IDS :::KS-JMFR:::
 */
public class MMC_SpidBean {

	/** Variable de tipo string para cve interbancaria */
	private String cveInter = "";
	/** Variable de tipo string para nombre largo del banco */
	private String nombre = "";
	/** Variable de tipo string para cecoban */
	private String cecoban = "";

	/**
	 * FSW IDS :::KS-JMFR:::
	 * @return cveInter de tipo String.
	 */
	public String getCveInter() {
		return cveInter;
	}

	/**
	 * FSW IDS :::KS-JMFR:::
	 * @param cveInter de tipo String.
	 */
	public void setCveInter(String cveInter) {
		this.cveInter = cveInter;
	}

	/**
	 * FSW IDS :::KS-JMFR:::
	 * @return nombre de tipo String.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * FSW IDS :::KS-JMFR:::
	 * @param nombre de tipo String.
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * FSW IDS :::KS-JMFR:::
	 * @return cecoban de tipo String.
	 */
	public String getCecoban() {
		return cecoban;
	}

	/**
	 * FSW IDS :::KS-JMFR:::
	 * @param cecoban de tipo String.
	 */
	public void setCecoban(String cecoban) {
		this.cecoban = cecoban;
	}

}
