package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;

import java.io.Serializable;
import java.util.StringTokenizer;

import mx.altec.enlace.bo.TrxGP72VO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase administradora para manejar el procedimiento para la transaccion GP72
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Abr 27, 2012
 */
public class AdmTrxGP72 extends AdmUsrGL35Base implements Serializable {

	/**
	 * Serial Id de la clase
	 */
	private static final long serialVersionUID = -8010833219323594156L;

	/**
	 * Constantes para codigo exito
	 */
	private static final String CODIGO_EXITO = "@1";

	/**
	 * Constantes para codigo error
	 */
	private static final String CODIGO_ERROR = "@2";

	/**
	 * Constante para la cabecera de la trama
	 */
	public static String HEADER = "GP7204581000011O00N2";

	/**
	 * Constante para reconocer el formato de salida de la transaccion
	 */
	private static final String FORMATO_HEADER = "@DCGPM0721 P";

	/**
	 * Constante para reconocer la instancia de la clase
	 */
	private static final TrxGP72VOFactory FACTORY = new TrxGP72VOFactory();

	/**
	 * Obtener el valor de FACTORY.
     *
     * @return FACTORY valor asignado.
	 */
	public static TrxGP72VOFactory getFactoryInstance() {
		return FACTORY;
	}

	/**
	 * Objeto contenedor de tipo TrxGP72VO
	 */
	private TrxGP72VO GP72Bean;

	/**
	 * Obtener el valor de GP72Bean.
     *
     * @return GP72Bean valor asignado.
	 */
	public TrxGP72VO getGP72bean() {
		return GP72Bean;
	}

	/**
     * Asignar un valor a GP72Bean.
     *
     * @param GP72Bean Valor a asignar.
     */
	public void setGP72bean(TrxGP72VO beanGP72) {
		this.GP72Bean = beanGP72;
	}

	/**
	 * Inner Class que implementa la interfaz AdmUsrBuilder
	 * Metodos a implementar: 	build
	 *
	 */
	private static class TrxGP72VOFactory implements AdmUsrBuilder<AdmTrxGP72> {

		/**
		 * Metodo implementado build para el trato de la trama de salida
		 */
		public AdmTrxGP72 build(String arg) {
	    	EIGlobal.mensajePorTrace("AdmTrxGP72::TrxGP72VOFactory::build:: Entrando..."
	    			, EIGlobal.NivelLog.DEBUG);
			AdmTrxGP72 bean = new AdmTrxGP72();
			TrxGP72VO beanGP72 = new TrxGP72VO();
			StringTokenizer tramaSalida = null;
			String token = "";
			EIGlobal.mensajePorTrace("AdmTrxGP72::TrxGP72VOFactory::build:: Trama Salida->" + arg
	    			, EIGlobal.NivelLog.DEBUG);
			if(verificaCodigo(arg)){
		    	EIGlobal.mensajePorTrace("AdmTrxGP72::TrxGP72VOFactory::build:: Codigo exitoso"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion("GP720000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_HEADER);
				if(index != -1){
					tramaSalida = new StringTokenizer(arg.substring(arg.indexOf(FORMATO_HEADER), arg.length()),"@");
					EIGlobal.mensajePorTrace("AdmTrxGP72::TrxGP72VOFactory::build:: Cuantos tokens son:"+tramaSalida.countTokens(), EIGlobal.NivelLog.DEBUG);
					while(tramaSalida.hasMoreTokens()){
						try{
							index = 0;
							token = tramaSalida.nextToken();
							index += FORMATO_HEADER.length()-1;
							beanGP72.setFolioOperacionPadre(getValor(token, index, 10));
							beanGP72.setFolioOperacion(getValor(token, index += 10, 10));
							beanGP72.setEstatusOperacion(getValor(token, index += 10, 3));
							beanGP72.setFolioTransfer(getValor(token, index += 3, 7));

					    	EIGlobal.mensajePorTrace("AdmTrxGP72::TrxGP72VOFactory::build:: Resultados..." +
					    			beanGP72.toString(), EIGlobal.NivelLog.DEBUG);
						}catch(Exception e){
							EIGlobal.mensajePorTrace("AdmLyMGL24 - Error en la cantidad de registros: ["
									+ e + "]", EIGlobal.NivelLog.ERROR);
						}
						break;
					}
				}
			}else{
		    	EIGlobal.mensajePorTrace("AdmTrxGP72::TrxGP72VOFactory::build:: Codigo erroneo"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setGP72bean(beanGP72);
			return bean;
		}
	}

	/**
	 * Metodo para verificar el codigo de error
	 * @param resultado
	 * @return
	 */
	public static boolean verificaCodigo(String resultado){
		if (resultado != null && resultado.indexOf(CODIGO_EXITO) != -1)
			return true;
		else if (resultado != null && resultado.indexOf(CODIGO_ERROR) != -1)
			return false;
		return false;
	}
}