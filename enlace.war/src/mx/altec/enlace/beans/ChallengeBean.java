package mx.altec.enlace.beans;

public class ChallengeBean {
	
	private String respuesta;
	private String seesionID;
	private String transactionID;
	private String preguntaUser;
	private String preguntaUserID;
	private String servletRegreso;
	
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getSeesionID() {
		return seesionID;
	}
	public void setSeesionID(String seesionID) {
		this.seesionID = seesionID;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getPreguntaUser() {
		return preguntaUser;
	}
	public void setPreguntaUser(String preguntaUser) {
		this.preguntaUser = preguntaUser;
	}
	public String getPreguntaUserID() {
		return preguntaUserID;
	}
	public void setPreguntaUserID(String preguntaUserID) {
		this.preguntaUserID = preguntaUserID;
	}
	public String getServletRegreso() {
		return servletRegreso;
	}
	public void setServletRegreso(String servletRegreso) {
		this.servletRegreso = servletRegreso;
	}
	
	

}