package mx.altec.enlace.beans;

import java.util.ArrayList;
import java.util.List;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

public class AdmUsrGL61 extends GLBase {
	
	public static final String HEADER = "GL6101021123451O00N2";
	
	public static final AdmUsrBuilder<AdmUsrGL61> BUILDER = new GL61Builder();
	
	/////////////////////////////////////
	
	private static final String INDICADOR_EXITO = "@112345";
	
	private static final String FMT_PAGINACION = "@DCGLM6130 P";  
	
	private static final String FMT_CABECERA = "@DCGLM6110 P";
	
	private static final String FTM_DETALLE = "@DCGLM6120 P";												
	
	/////////////////////////////////////
	
	private int numeroRegistros;
	
	private String contratoEnlace;
	
	private String usuarioEnlace;	
					
	private List<GLFacultadCta> facultades;
	
	private String facultadPaginacion;
	
	private AdmUsrGL61() {		
	}
			
	public String getContratoEnlace() {
		return contratoEnlace;
	}

	public int getNumeroRegistros() {
		return numeroRegistros;
	}

	public String getFacultadPaginacion() {
		return facultadPaginacion;
	}

	public String getUsuarioEnlace() {
		return usuarioEnlace;
	}

	public List<GLFacultadCta> getFacultades() {
		return facultades;
	}
	
	/////////////////////////////////////
	
	public static String tramaConsulta(String contratoEnlace, String usuarioEnlace, String claveFacultad, String usuarioOperacion, String facultadPaginacion) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(rellenar(contratoEnlace, 20));
		sb.append(rellenar(usuarioEnlace, 8));
		sb.append(rellenar(claveFacultad, 16));
		sb.append(facultadPaginacion == null || facultadPaginacion.length() == 0 ? "  " : "SI");
		sb.append(rellenar(facultadPaginacion, 16));
		sb.append(rellenar(usuarioOperacion, 8));
		
		return sb.toString();
	}	

	/////////////////////////////////////

	public static class GL61Builder implements AdmUsrBuilder<AdmUsrGL61> {

		public AdmUsrGL61 build(String string) {
			
			if (string == null || string.length() == 0) {
	            throw new IllegalArgumentException();
	        }

			AdmUsrGL61 gl61 = new AdmUsrGL61();

			if (isCodigoError(string)) {
				
				gl61.setCodMsg(getCodigoError(string));	        	
				gl61.setMensaje(getMensajeError(string));
	        	
				return gl61;
			}

	        if (string.startsWith(INDICADOR_EXITO)) {	        	        	
	        	
	        	//SE PROCESA EL DETALLE
	        	
	        	int ind = 0;        
	        	
	        	List<GLFacultadCta> facultades = new ArrayList<GLFacultadCta>();
	        	
	        	while ((ind = string.indexOf(FTM_DETALLE, ind)) != -1) {
	        		
	        		ind += FTM_DETALLE.length();
	        		
	        		GLFacultadCta facultad = new GLFacultadCta();
	        		facultad.setClaveFacultad(string.substring(ind, (ind += 16)));
	        		facultad.setSigno(string.substring(ind, (ind += 1)));
	        		facultad.setDescripcion(string.substring(ind, (ind += 80)).trim());
	        		facultad.setNombre(string.substring(ind, (ind += 80)).trim());
	        		facultad.setVisible(string.substring(ind, (ind += 2)));
	        		facultad.setUso(string.substring(ind, (ind += 2)));
	        		facultad.setNivel(string.substring(ind, (ind += 1)));
	        		facultad.setGrupo(string.substring(ind, (ind += 3)));
	        		
	        		facultades.add(facultad);
	        	}      
	        	
	        	gl61.facultades = facultades;
	        	
	        	//SE PROCESA LA INFORMACION DE PAGINACION
	        	
	        	if ((ind = string.indexOf(FMT_PAGINACION)) != -1) {
	        		
	        		ind += FMT_PAGINACION.length();
	        		
	        		gl61.facultadPaginacion = string.substring(ind, (ind += 16));
	        	}
	        	
	        	//SE PROCESA LA INFORMACION DE CABECERA
	        	
	        	if ((ind = string.indexOf(FMT_CABECERA)) != -1) {
	        		
	        		ind += FMT_CABECERA.length();
	        			        			        		
	        		//Se salta el numero de registros
	        		gl61.numeroRegistros = Integer.parseInt(string.substring(ind, (ind += 3)));	        			
	        		gl61.contratoEnlace = string.substring(ind, (ind += 11));
	        		gl61.usuarioEnlace = string.substring(ind, (ind += 8));	        	
	        	}	        
	        }

	        return gl61;        	
		}	
	}
}
