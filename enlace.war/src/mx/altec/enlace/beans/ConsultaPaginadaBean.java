/**
 * Isban Mexico
 *   Clase: ConsultaPaginadaBean.java
 *   Descripcion: Implementacion de ConsultaPaginadaBean, bean de datos para paginar las consultas por query
 *
 *   Control de Cambios:
 *   1.0 Mar 01, 2015 asanjuan - Creacion
 */
package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.Map;

/**
 * Creacion ConsultaPaginadaBean para paginacion
 * @author Arturo
 * @version 1.0
 * 
 */
public class ConsultaPaginadaBean implements Serializable{

	/**
	 * Serial generado
	 */
	private static final long serialVersionUID = 6642648099329735724L;
	/**Numero del registro inicial para hacer la consulta**/
	int numeroRegInicio;
	/**Numero del registro final para hacer la consulta**/
	int numeroRegFin;
	/**total de los registros encontrados**/
	int totalRegistros;
	/**<Mapa de objetos de la consulta**/
	Map<String, Object> resultadoConsulta;
	
	/**
	 * @return el numeroRegInicio
	 */
	public int getNumeroRegInicio() {
		return numeroRegInicio;
	}
	/**
	 * @param numeroRegInicio el numeroRegInicio a establecer
	 */
	public void setNumeroRegInicio(int numeroRegInicio) {
		this.numeroRegInicio = numeroRegInicio;
	}
	/**
	 * @return el numeroRegFin
	 */
	public int getNumeroRegFin() {
		return numeroRegFin;
	}
	/**
	 * @param numeroRegFin el numeroRegFin a establecer
	 */
	public void setNumeroRegFin(int numeroRegFin) {
		this.numeroRegFin = numeroRegFin;
	}
	/**
	 * @return el totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}
	/**
	 * @param totalRegistros el totalRegistros a establecer
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	/**
	 * @return el resultadoConsulta
	 */
	public Map<String, Object> getResultadoConsulta() {
		return resultadoConsulta;
	}
	/**
	 * @param resultadoConsulta el resultadoConsulta a establecer
	 */
	public void setResultadoConsulta(Map<String, Object> resultadoConsulta) {
		this.resultadoConsulta = resultadoConsulta;
	}
}