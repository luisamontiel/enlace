package mx.altec.enlace.beans;

import java.util.Date;

/**
 * Bean que alamacena informacion de los favoritos de ENLACE.
 * 
 * @author
 * 
 */
public class FavoritosEnlaceBean {
	/**
	 * Contrato del cliente al que pertenece el favorito.
	 */
	private String contrato;
	/**
	 * Cod Cliente.
	 */
	private String codCliente;
	/**
	 * Id del modulo al que pertenece el favorito.
	 */
	private String idModulo;
	/**
	 * Cuenta cargo.
	 */
	private String cuentaCargo;
	/**
	 * Cuenta abono.
	 */
	private String cuentaAbono;
	/**
	 * Descripcion de la cuenta cargo.
	 */
	private String descCuentaCargo;
	/**
	 * Descripcion de la cuenta de abono.
	 */
	private String descCuentaAbono;
	/**
	 * Tipo de registro, si es individual o por archivo.
	 */
	private String tipo;
	/**
	 * Descripcion del favorito.
	 */
	private String descripcion;
	/**
	 * Importe de la operacion favorito.
	 */
	private double importe;
	/**
	 * Concepto de la operacion.
	 */
	private String conceptoOperacion;
	/**
	 * Fecha de alta de la operacion.
	 */
	private Date fechaAlta;
	/**
	 * Tip de transaccion SPEI o TEF.
	 */
	private String tipoTransaccion;
	// NOMINA
	/**
	 * Id. del favorito.
	 */
	private String idFav;
	/**
	 * Numero de registros importados en el archivo.
	 */
	private int numeroRegistros;
	/**
	 * Identifica si es una carga por archivo.
	 */
	private boolean esArchivo;

	/**
	 * @return El valor de contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * @param contrato
	 *            valor a establecer para contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * @return El valor de codCliente
	 */
	public String getCodCliente() {
		return codCliente;
	}

	/**
	 * @param codCliente
	 *            valor a establecer para codCliente
	 */
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	/**
	 * @return El valor de idModulo
	 */
	public String getIdModulo() {
		return idModulo;
	}

	/**
	 * @param idModulo
	 *            valor a establecer para idModulo
	 */
	public void setIdModulo(String idModulo) {
		this.idModulo = idModulo;
	}

	/**
	 * @return El valor de cuentaCargo
	 */
	public String getCuentaCargo() {
		return cuentaCargo;
	}

	/**
	 * @param cuentaCargo
	 *            valor a establecer para cuentaCargo
	 */
	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}

	/**
	 * @return El valor de cuentaAbono
	 */
	public String getCuentaAbono() {
		return cuentaAbono;
	}

	/**
	 * @param cuentaAbono
	 *            valor a establecer para cuentaAbono
	 */
	public void setCuentaAbono(String cuentaAbono) {
		this.cuentaAbono = cuentaAbono;
	}

	/**
	 * @return El valor de tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            valor a establecer para tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return El valor de descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            valor a establecer para descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return El valor de importe
	 */
	public double getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            valor a establecer para importe
	 */
	public void setImporte(double importe) {
		this.importe = importe;
	}

	/**
	 * @return El valor de conceptoOperacion
	 */
	public String getConceptoOperacion() {
		return conceptoOperacion;
	}

	/**
	 * @param conceptoOperacion
	 *            valor a establecer para conceptoOperacion
	 */
	public void setConceptoOperacion(String conceptoOperacion) {
		this.conceptoOperacion = conceptoOperacion;
	}

	/**
	 * @return El valor de fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            valor a establecer para fechaAlta
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return el descCuentaCargo
	 */
	public String getDescCuentaCargo() {
		return descCuentaCargo;
	}

	/**
	 * @param descCuentaCargo
	 *            el descCuentaCargo a establecer.
	 */
	public void setDescCuentaCargo(String descCuentaCargo) {
		this.descCuentaCargo = descCuentaCargo;
	}

	/**
	 * @return el descCuentaAbono
	 */
	public String getDescCuentaAbono() {
		return descCuentaAbono;
	}

	/**
	 * @param descCuentaAbono
	 *            el descCuentaAbono a establecer
	 */
	public void setDescCuentaAbono(String descCuentaAbono) {
		this.descCuentaAbono = descCuentaAbono;
	}

	/**
	 * @return el tipoTransaccion
	 */
	public String getTipoTransaccion() {
		return tipoTransaccion;
	}

	/**
	 * @param tipoTransaccion
	 *            tipoTransaccion a establecer
	 */
	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	/**
	 * @return el numeroRegistros
	 */
	public int getNumeroRegistros() {
		return numeroRegistros;
	}

	/**
	 * @param numeroRegistros
	 *            el numeroRegistros a establecer.
	 */
	public void setNumeroRegistros(int numeroRegistros) {
		this.numeroRegistros = numeroRegistros;
	}

	/**
	 * @return el esArchivo
	 */
	public boolean getEsArchivo() {
		return esArchivo;
	}

	/**
	 * @param esArchivo
	 *            el esArchivo a establecer.
	 */
	public void setEsArchivo(boolean esArchivo) {
		this.esArchivo = esArchivo;
	}

	/**
	 * @return el idFAv
	 */
	public String getIdFav() {
		return idFav;
	}

	/**
	 * @param idFAv
	 *            a establecer.
	 */
	public void setIdFav(String idFAv) {
		this.idFav = idFAv;
	}

}