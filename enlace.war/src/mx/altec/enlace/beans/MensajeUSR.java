/**
 * MensajeUSR
 * Value Object que contiene informacion de los mensajes de notificacion para la implementacion
 * de capitulo 10
 * @author marreguin
 * @since 8/Jul/2010
 */

package mx.altec.enlace.beans;

import mx.altec.enlace.utilerias.EIGlobal;


public class MensajeUSR {
	private boolean sms;
	private boolean email;
	private boolean enviarUsuario;
	private boolean enviarSuperUsuario;
	private String claveSuperUsuario= ""; //Clave de super usuario
	private String emailSuperUsuario="";//Email de super usuario
	private String noCelularSuperUsuario="";//Celular de super usuario
	private String ladaSuperUsuario="";//Lada de super usuario
	private String compCelularSuperUsuario="";// Compa�ia de celular de super usuario
	private String medioNotSuperUsuario="";//Medio por el cual se notificara al super usuario y a quien se notificara.
	private String emailUsuario="";// Email de usuario
	private String noCelularUsuario="";//Celular de usuario
	private String ladaUsuario="";//Lada de usuario
	private String compCelularUsuario="";// Compa�ia de celular de usuario
	private String emailContrato="";// Email del contrato
	private int tipoEnvio=0;
	private char envioA=0;
	private boolean superUsuario; //El usuario logueado es  super Usuario

	public String toString() {
		return "[MensajeUSR toString] CSU: " + claveSuperUsuario + ", ESU: " + emailSuperUsuario + ", CSU: " + ladaSuperUsuario + noCelularSuperUsuario + ", MNSU: " + medioNotSuperUsuario + ", CU: " + ladaUsuario + noCelularUsuario + ", EU: " + emailUsuario;
	}
	
	public void setIsSms(boolean sms){
            this.sms = sms;
      }


	public boolean isEnviarUsuario() {
		return enviarUsuario;
	}

	public boolean isEnviarSuperUsuario() {
		return enviarSuperUsuario;
	}

	/**
	 * getCompCelularSuperUsuario
	 * Compa�ia de celular de super usuario
	 * @return String
	 */
	public String getCompCelularSuperUsuario() {
		return compCelularSuperUsuario;
	}
	/**
	 * setCompCelularSuperUsuario
	 * Compa�ia de celular de super usuario
	 */
	public void setCompCelularSuperUsuario(String compCelularSuperUsuario) {
		this.compCelularSuperUsuario = compCelularSuperUsuario;
	}
	/**
	 * isEmail
	 * contiene verdadero si se envio la notificacion via mail
	 * @return boolean
	 */
	public boolean isEmail() {
		return email;
	}
	/**
	 * getEmailSuperUsuario
	 * Email de super usuario
	 * @return String
	 */
	public String getEmailSuperUsuario() {
		return emailSuperUsuario;
	}
	/**
	 * setEmailSuperUsuario
	 * Email de super usuario
	 */
	public void setEmailSuperUsuario(String emailSuperUsuario) {
		this.emailSuperUsuario = emailSuperUsuario;
	}
	/**
	 * getEmailUsuario
	 * Clave de super usuario
	 * @return String
	 */
	public String getEmailUsuario() {
		return emailUsuario;
	}
	/**
	 * setEmailUsuario
	 * Clave de super usuario
	 */
	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}
	/**
	 * getMedioNotSuperUsuario
	 * Medio por el cual se notificara al super usuario y a quien se notificara.
	 * @return String
	 */
	public String getMedioNotSuperUsuario() {
		return medioNotSuperUsuario;
	}
	/**
	 * setMedioNotSuperUsuario
	 * Medio por el cual se notificara al super usuario y a quien se notificara.
	 */




	public void setMedioNotSuperUsuario(String medioNotSuperUsuario) {


		this.medioNotSuperUsuario = medioNotSuperUsuario;
		if (medioNotSuperUsuario!=null && !medioNotSuperUsuario.equals("")) {
			this.envioA=medioNotSuperUsuario.charAt(0);
			this.tipoEnvio=Integer.parseInt((medioNotSuperUsuario.charAt(1)+""));
		} else {
			this.envioA = 'B';
			this.tipoEnvio = 1;
		}


		/**
		 * getNumero
		 * Las siguientes opciones aplican unicamente para el super usuario y es para se selecciona la opcion A o C
		 * 1 - Indica que se selecciono la opcion de envio por Email
		 * 2 - Indica que se selecciono la opcion de envio por Celular
		 * 3 - Indica que se selecciono la opcion de envio por Email y Celular
		 * @return int
		 */
		switch(tipoEnvio)
		{
		case 1: this.email=true;
				this.sms=false;
				break;
		case 2: this.email=false;
				this.sms=true;
				break;
		case 3: this.email=true;
				this.sms=true;
				break;
		default :this.email=false;
				this.sms=false;
				 break;
		}
		/**
		 * getCaracter
		 * indica los siguinetes valores:
		 *
		 * A - Se enviara notificacion via celular y/o email al super usuario unicamente cuando sus usuarios realicen alguna operacion
		 * B - Se enviara notificacion via email al usuario firmado o logueado cuando realice alguna operacion
		 * C - Se enviara notificacion al super usuario via celular y/o emal cuando el mismo realice alguna operacion
		 * @return String
		 */

		switch(envioA)
		{
		case 'A':
				this.enviarUsuario=true;
				this.enviarSuperUsuario=true;
				break;
		case 'B':
				this.enviarUsuario=true;
				this.enviarSuperUsuario=false;
				break;
		case 'C':
				this.enviarUsuario=false;
				this.enviarSuperUsuario=true;
				break;
		default:
				this.enviarUsuario=false;
				this.enviarSuperUsuario=false;
				break;
		}

		EIGlobal.mensajePorTrace("MensajeUSR.setMedioNotSuperUsuario ->enviarUsuario-->" + enviarUsuario, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MensajeUSR.setMedioNotSuperUsuario ->enviarSuperUsuario---->" + enviarSuperUsuario, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MensajeUSR.setMedioNotSuperUsuario ->email--->" +email, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MensajeUSR.setMedioNotSuperUsuario ->sms--->" +sms, EIGlobal.NivelLog.INFO);
	}

	/**
	 * isSms
	 * contiene true si la notificacion se mando via sms
	 * @return boolean
	 */
	public boolean isSms() {
		return sms;
	}



	public String getClaveSuperUsuario() {
		return claveSuperUsuario;
	}

	public void setClaveSuperUsuario(String claveSuperUsuario) {
		this.claveSuperUsuario = claveSuperUsuario;
	}

	public boolean isSuperUsuario() {
		return superUsuario;
	}

	public void setSuperUsuario(boolean superUsuario) {
		this.superUsuario = superUsuario;
	}

	public String getCompCelularUsuario() {
		return compCelularUsuario;
	}

	public void setCompCelularUsuario(String compCelularUsuario) {
		this.compCelularUsuario = compCelularUsuario;
	}

	public String getEmailContrato() {
		return emailContrato;
	}

	public void setEmailContrato(String emailContrato) {
		this.emailContrato = emailContrato;
	}

	public String getLadaSuperUsuario() {
		return ladaSuperUsuario;
	}

	public void setLadaSuperUsuario(String ladaSuperUsuario) {
		this.ladaSuperUsuario = ladaSuperUsuario;
	}

	public String getLadaUsuario() {
		return ladaUsuario;
	}

	public void setLadaUsuario(String ladaUsuario) {
		this.ladaUsuario = ladaUsuario;
	}

	public String getNoCelularSuperUsuario() {
		return noCelularSuperUsuario;
	}

	public void setNoCelularSuperUsuario(String noCelularSuperUsuario) {
		this.noCelularSuperUsuario = noCelularSuperUsuario;
	}

	public String getNoCelularUsuario() {
		return noCelularUsuario;
	}

	public void setNoCelularUsuario(String noCelularUsuario) {
		this.noCelularUsuario = noCelularUsuario;
	}





}