package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;


import java.io.Serializable;

/**
 * @author ESC
 *	Clase que realiza la consulta de la transacci&oacuten LM1J
 */
public class TrxLM1JBean extends NomPreLM1Base implements Serializable {

	/**
	 * Header de la transac&oacuten LM1J
	 */
	public static final String HEADER = "LM1J01891123451O00N2";
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 3988187929011449902L;
	/**
	 * Formato detalle de la transac&oacuten LM1J
	 */
	private static final String FORMATO_DETALLE = "@DCLMCENT1 P";

	/**
	 * Formato detalle de la transac&oacuten paginado LM1J
	 */
	private static final String FORMATO_DETALLE_PAG = "@DCLMCENT2 P";

	/**
	 * Factory de la clase TrxLM1JBean
	 */
	private static final TrxLM1JFactory FACTORY = new TrxLM1JFactory();

	/**
	 *Detalle del resultado de la consulta
	 */
	private String detalle;
	/**
	 *Numero de tarjeta a consultar
	 */
	private String tarjeta;
	/**
	 *NOMBRE EMPLEADO
	 */
	private String nombreEmpleado;
	/**
	 *APELLIDO PATERNO
	 */
	private String apellidoPat;
	/**
	 *APELLIDO MATERNO
	 */
	private String apellidoMat;
	/**
	 *RFC
	 */
	private String rfc;
	/**
	 *HOMOCLAVE
	 */
	private String homoclave;
	/**
	 *FECHA NACIMIENTO
	 */
	private String fechaNac;
	/**
	 *NUMERO TARJETA PAG
	 */
	private String tarjetaPag;

	/**
	 * Metodo que regresa el factory
	 * @return Factory seteado
	 */
	public static TrxLM1JFactory getFactoryInstance() {
		return FACTORY;
	}

	/**
	 * Metodo que setea la tarjeta
	 * @param tarjeta a consultar
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * Metodo que regresa la tarjeta
	 * @return tarjeta a consultar
	 */
	public String getTarjeta() {
		return tarjeta;
	}

	/**
	 * Metodo que setea el detalle de la transac&oacuten LM1J
	 * @param detalle respuesta
	 */
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	/**
	 * Metodo que regresa el detalle de la transac&oacuten LM1J
	 * @return detalle respuesta
	 */
	public String getDetalle() {
		return detalle;
	}

	/**
	 * Setea pagina targeta
	 * @param tarjetaPag tarjeta
	 */
	public void setTarjetaPag(String tarjetaPag) {
		this.tarjetaPag = tarjetaPag;
	}
	/**
	 * Obtiene pagina targeta
	 * @return tarjetaPag tarjeta
	 */
	public String getTarjetaPag() {
		return tarjetaPag;
	}
	/**
	 * Setea pagina targeta
	 * @param fechaNac fechaNac
	 */
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	/**
	 * Obtiene pagina targeta
	 * @return tarjetaPag tarjeta
	 */
	public String getFechaNac() {
		return fechaNac;
	}
	/**
	 * Setea homoclave
	 * @param homoclave homoclave
	 */
	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}
	/**
	 * Obtiene pagina targeta
	 * @return tarjetaPag tarjeta
	 */
	public String getHomoclave() {
		return homoclave;
	}
	/**
	 * Setea rfc
	 * @param rfc rfc
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	/**
	 * Obtiene pagina targeta
	 * @return tarjetaPag tarjeta
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 * Setea apellidoMat
	 * @param apellidoMat apellidoMat
	 */
	public void setApellidoMat(String apellidoMat) {
		this.apellidoMat = apellidoMat;
	}
	/**
	 * Obtiene pagina targeta
	 * @return tarjetaPag tarjeta
	 */
	public String getApellidoMat() {
		return apellidoMat;
	}
	/**
	 * Setea apellidoPat
	 * @param apellidoPat apellidoPat
	 */
	public void setApellidoPat(String apellidoPat) {
		this.apellidoPat = apellidoPat;
	}
	/**
	 * Obtiene pagina targeta
	 * @return tarjetaPag tarjeta
	 */
	public String getApellidoPat() {
		return apellidoPat;
	}
	/**
	 * Setea nombreEmpleado
	 * @param nombreEmpleado nombreEmpleado
	 */
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}
	/**
	 * Obtiene pagina targeta
	 * @return tarjetaPag tarjeta
	 */
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}

	/**
	 * @author ESC
	 *	Clase usada para generar un elemento LM1J a partir de la cadena de salida
	 * 	de la transacci&oacuten
	 */
	private static class TrxLM1JFactory implements NomPreBuilder<TrxLM1JBean> {

		/**
		 * Componente encargado de construir el objeto TrxLM1JBean a partir de una cadena de entrada
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg Cadena de entrada
		 * @return TrxLM1JBean tarjeta
		 */
		public TrxLM1JBean build(String arg) {

			TrxLM1JBean bean;
			bean = new TrxLM1JBean();

			if (isCodigoExito(arg)) {
				bean.setCodigoOperacion("LM1J0000");
				bean.setCodExito(true);

				String[] detalles;
				detalles = getDetalles(arg, FORMATO_DETALLE, 24);
				if (detalles != null && detalles.length > 0) {

					for (String detalle : detalles) {

						if (detalle.length() == 0) {
							continue;
						}
						if(!"3".equals(detalle.substring(22, 23))  && !"4".equals(detalle.substring(22, 23))){
							bean.setCodExito(false);
							bean.setCodigoOperacion("LM1J0001");
						}
					}
				} else {
					String[] detallesPag;
					detallesPag = getDetalles(arg, FORMATO_DETALLE_PAG, 24);
					if (detallesPag != null && detallesPag.length > 0) {

						for (String detalle : detallesPag) {

							if (detalle.length() != 0) {
								bean.setDetalle(getValor(detalle, 0, 18));
							}
						}
					} else {
						bean.setCodExito(false);
						bean.setCodigoOperacion("LM1J0001");
					}
				}

			} else if (isCodigoError(arg))  {

				bean.setCodigoOperacion(getCodigoError(arg));
			}

			return bean;
		}
	}
}
