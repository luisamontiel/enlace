package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * The Class CuentaBean.
 */
public class CuentaBean extends CuentasBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7762954218971243676L;

	/** The banco. */
	private String banco;
	
	/** The plaza. */
	private String pais;
	
	/** The sucursal. */
	private String divisa;
	
	/** The ciudad. */
	private String ciudad;
	
	/** The plaza. */
	private String plaza;
	
	/** The sucursal. */
	private String sucursal;
	
	/**
	 * Constructor de la clase
	 */
	public CuentaBean() {
		banco = "";
		pais = "";
		divisa = "";
		ciudad = "";
		plaza = "";
		sucursal = "";
	}

	/**
	 * Gets the banco.
	 *
	 * @return the banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Sets the banco.
	 *
	 * @param banco the new banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Gets the pais.
	 *
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * Sets the pais.
	 *
	 * @param pais the new pais
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * Gets the divisa.
	 *
	 * @return the divisa
	 */
	public String getDivisa() {
		return divisa;
	}

	/**
	 * Sets the divisa.
	 *
	 * @param divisa the new divisa
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 * Gets the ciudad.
	 *
	 * @return the ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}

	/**
	 * Sets the ciudad.
	 *
	 * @param ciudad the new ciudad
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * Gets the plaza.
	 *
	 * @return the plaza
	 */
	public String getPlaza() {
		return plaza;
	}

	/**
	 * Sets the plaza.
	 *
	 * @param plaza the new plaza
	 */
	public void setPlaza(String plaza) {
		this.plaza = plaza;
	}

	/**
	 * Gets the sucursal.
	 *
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * Sets the sucursal.
	 *
	 * @param sucursal the new sucursal
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
}