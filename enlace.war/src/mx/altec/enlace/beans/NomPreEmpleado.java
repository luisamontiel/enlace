package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.io.Serializable;

public class NomPreEmpleado implements Serializable {

	private static final long serialVersionUID = -6124505369262440185L;

	public static int LNG_NO_EMPLEADO = 7;

	public static int LNG_NOMBRE = 30;

	public static int LNG_PATERNO = 30;

	public static int LNG_MATERNO = 20;

	public static int LNG_RFC = 10;
	
	public static int LNG_NUM_ID = 25;//VSWF ESC nuevos campos NP
	
	//public static int LNG_TIPO_ID = 10; VSWF ESC se comenta codigo tipo id
	
	public static int LNG_HOMOCLAVE = 3;	
	
	public static int LNG_FECHA_NACIMIENTO = 10;

	public static int LNG_SEXO = 1;

	public static int LNG_NACIONALIDAD = 4;
	
	public static int LNG_PAIS = 25;//VSWF ESC nuevos campos NP
	
	public static int LNG_ESTADO_CIVIL = 1;

	public static int LNG_CORREO_ELECTRONICO = 30;

	public static int LNG_CALLE = 50;
	
	public static int LNG_NUM_EXT = 10;//VSWF ESC nuevos campos NP
	
	public static int LNG_NUM_INT = 10;//VSWF ESC nuevos campos NP
	
	public static int LNG_COLONIA = 30;

	public static int LNG_DELEGACION = 20;

	public static int LNG_CIUDAD = 30;

	public static int LNG_ESTADO = 30;

	public static int LNG_CODIGO_POSTAL = 5;

	public static int LNG_LADA = 5;

	public static int LNG_TELEFONO = 8;

	private String noEmpleado;

	private String nombre;

	private String paterno;

	private String materno;

	private String rfc;
	
	private String numID;//VSWF ESC nuevos campos NP
	
	//private String tipoID; VSWF ESC se comenta codigo tipo id

	private String homoclave;

	private String fechaNacimiento;

	private String sexo;

	private String nacionalidad;
	
	private String pais;//VSWF ESC nuevos campos NP

	private String estadoCivil;

	private String correoElectronico;

	private String calle;

	private String numExt;//VSWF ESC nuevos campos NP
	
	private String numInt;//VSWF ESC nuevos campos NP

	private String colonia;

	private String delegacion;

	private String ciudad;

	private String estado;

	private String codigoPostal;

	private String lada;

	private String telefono;

	public NomPreEmpleado(){
	}

	public NomPreEmpleado(String noEmpleado, String nombre, String paterno, String materno) {
		this.noEmpleado = noEmpleado;
		this.nombre = nombre;
		this.paterno = paterno;
		this.materno = materno;
	}
	
	public NomPreEmpleado(String noEmpleado, String nombre, String paterno, String materno, String rfc, String homoclave, String numID,/* String tipoID,*/ String fechaNacimiento, String sexo, String nacionalidad, String pais, String estadoCivil, String correoElectronico, String calle, String numExt, String numInt, String colonia, String delegacion, String ciudad, String estado, String codigoPostal, String lada, String telefono) {
		this(noEmpleado, nombre, paterno, materno);
		this.rfc = rfc;
		this.homoclave = homoclave;
		this.numID = numID;//VSWF ESC nuevos campos NP
		//this.tipoID = tipoID;//VSWF ESC nuevos campos NP
		this.fechaNacimiento = fechaNacimiento;
		this.sexo = sexo;
		this.nacionalidad = nacionalidad;
		this.pais = pais;//VSWF ESC nuevos campos NP
		this.estadoCivil = estadoCivil;
		this.correoElectronico = correoElectronico;
		this.calle = calle;
		this.numExt = numExt;//VSWF ESC nuevos campos NP
		this.numInt = numInt;//VSWF ESC nuevos campos NP
		this.colonia = colonia;
		this.delegacion = delegacion;
		this.ciudad = ciudad;
		this.estado = estado;
		this.codigoPostal = codigoPostal;
		this.lada = lada;
		this.telefono = telefono;
	}

	public String getCalleFrm() {
		return rellenar(calle, LNG_CALLE);
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCiudadFrm() {
		return rellenar(ciudad, LNG_CIUDAD);
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCodigoPostalFrm() {
		return rellenar(codigoPostal, LNG_CODIGO_POSTAL);
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getColoniaFrm() {
		return rellenar(colonia, LNG_COLONIA);
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCorreoElectronicoFrm() {
		return rellenar(correoElectronico, LNG_CORREO_ELECTRONICO);
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getDelegacionFrm() {
		return rellenar(delegacion, LNG_DELEGACION);
	}

	public String getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}

	public String getEstadoFrm() {
		return rellenar(estado, LNG_ESTADO);
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstadoCivilFrm() {
		return rellenar(estadoCivil, LNG_ESTADO_CIVIL);
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getFechaNacimientoFrm() {
		return rellenar(fechaNacimiento, LNG_FECHA_NACIMIENTO);
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getHomoclaveFrm() {
		return rellenar(homoclave, LNG_HOMOCLAVE);
	}

	public String getHomoclave() {
		return homoclave;
	}

	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}

	public String getLadaFrm() {
		return rellenar(lada, LNG_LADA);
	}

	public String getLada() {
		return lada;
	}

	public void setLada(String lada) {
		this.lada = lada;
	}

	public String getMaternoFrm() {
		return rellenar(materno, LNG_MATERNO);
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getNacionalidadFrm() {
		return rellenar(nacionalidad, LNG_NACIONALIDAD);
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	public String getPaisFrm() {
		return rellenar(pais, LNG_PAIS);
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getNoEmpleadoFrm() {
		return rellenar(noEmpleado, LNG_NO_EMPLEADO);
	}

	public String getNoEmpleado() {
		return noEmpleado;
	}

	public void setNoEmpleado(String noEmpleado) {
		this.noEmpleado = noEmpleado;
	}

	public String getNombreFrm() {
		return rellenar(nombre, LNG_NOMBRE);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroFrm() {
		return rellenar(numExt, LNG_NUM_EXT);
	}
	
	public String getNumExt() {
		return numExt;
	}

	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}
	
	public String getNumIntFrm() {
		return rellenar(numInt, LNG_NUM_INT);
	}
	
	public String getNumInt() {
		return numInt;
	}

	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}

	public String getPaternoFrm() {
		return rellenar(paterno, LNG_PATERNO);
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getRfcFrm() {
		return rellenar(rfc, LNG_RFC);
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
	public String getNumIDFrm() {
		return rellenar(numID, LNG_NUM_ID);
	}
	
	public String getNumID() {
		return numID;
	}

	public void setNumID(String numID) {
		this.numID = numID;
	}
	
	/*
	 * VSWF ESC se comenta codigo tipo id
	 */
	/*
	public String getTipoIDFrm() {
		return rellenar(tipoID, LNG_TIPO_ID);
	}
	
	public String getTipoID() {
		return tipoID;
	}

	public void setTipoID(String tipoID) {
		this.tipoID = tipoID;
	}*/

	public String getSexoFrm() {
		return rellenar(sexo, LNG_SEXO);
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTelefonoFrm() {
		return rellenar(telefono, LNG_TELEFONO);
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}