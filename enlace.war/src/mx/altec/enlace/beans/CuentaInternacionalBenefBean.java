package mx.altec.enlace.beans;


public class CuentaInternacionalBenefBean {

	/** variable direccionBenef */
	private String direccionBenef;
	/** variable ciudadBenef */
	private String ciudadBenef;
	/** variable paisBenef */
	private String paisBenef;
	/** variable idClienteBenef */
	private String idClienteBenef;
	
	/**
	 * @return el direccionBenef
	 */
	public String getDireccionBenef() {
		return direccionBenef;
	}
	/**
	 * @param direccionBenef el direccionBenef a establecer
	 */
	public void setDireccionBenef(String direccionBenef) {
		this.direccionBenef = direccionBenef;
	}
	/**
	 * @return el ciudadBenef
	 */
	public String getCiudadBenef() {
		return ciudadBenef;
	}
	/**
	 * @param ciudadBenef el ciudadBenef a establecer
	 */
	public void setCiudadBenef(String ciudadBenef) {
		this.ciudadBenef = ciudadBenef;
	}
	/**
	 * @return el paisBenef
	 */
	public String getPaisBenef() {
		return paisBenef;
	}
	/**
	 * @param paisBenef el paisBenef a establecer
	 */
	public void setPaisBenef(String paisBenef) {
		this.paisBenef = paisBenef;
	}
	/**
	 * @return el idClienteBenef
	 */
	public String getIdClienteBenef() {
		return idClienteBenef;
	}
	/**
	 * @param idClienteBenef el idClienteBenef a establecer
	 */
	public void setIdClienteBenef(String idClienteBenef) {
		this.idClienteBenef = idClienteBenef;
	}
	
}
