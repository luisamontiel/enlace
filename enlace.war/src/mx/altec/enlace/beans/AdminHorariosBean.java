package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * Bean de datos para Administracion de horarios
 * @author Z712236 ferruzca
 *
 */
public class AdminHorariosBean implements Serializable {

	/**
	 * Serial de version
	 */
	private static final long serialVersionUID = 1L;

	//********************************************************
	//* Definicion de variables 							 */
	//********************************************************

	/**
	 * activo estatus del horario
	 */
	private String activo;
	/**
	 * idFunc clave de funcionalidad
	 */
	private String idFunc;
	/**
	 * descFunc descripcion de funcionalidad
	 */
	private String descFunc;
	/**
	 * horaIniSem hora inicio semana
	 */
	private String horaIniSem;
	/**
	 * horaFinSem hora fin semana
	 */
	private String horaFinSem;
	/**
	 * horaIniSab hora inicio sabado
	 */
	private String horaIniSab;
	/**
	 * horaFinSab hora fin sabado
	 */
	private String horaFinSab;
	/**
	 * horaIniDom hora inicio domingo
	 */
	private String horaIniDom;
	/**
	 * horaFinDom hora fin domingo
	 */
	private String horaFinDom;
	/**
	 * lunes valor lunes
	 */
	private String lunes;
	/**
	 * martes valor martes
	 */
	private String martes;
	/**
	 * miercoles valor miercoles
	 */
	private String miercoles;
	/**
	 * jueves valor jueves
	 */
	private String jueves;
	/**
	 * viernes valor viernes
	 */
	private String viernes;
	/**
	 * sabado valor sabado
	 */
	private String sabado;
	/**
	 * domingo valor domingo
	 */
	private String domingo;
	/**
	 * diaInhabil dia inhabil
	 */
	private String diaInhabil;


	/**
	 * horaActual hora actual
	 */
	private String horaActual;
	/**
	 * diaSistema dia semana
	 */
	private String diaSistema;


	/**
	 * horaInicio hora inicio
	 */
	private String horaInicio;
	/**
	 * horaFin hora fin
	 */
	private String horaFin;


	/**
	 * codError codigo error
	 */
	private String codError;
	/**
	 * descError descripcion de error
	 */
	private String descError;
	/**
	 * horarioValido bandera horario valido
	 */
	private boolean horarioValido;
	/**
	 * tieneHorarioActivo tiene horario activo
	 */
	private boolean tieneHorarioActivo;
	/**
	 * Contiene el nombre del servlet de la peticion entrante en el filtro.
	 */
	private String nombreServlet; 

	//********************************************************
	//* Geters and Seters								 */
	//********************************************************

	/**
	 * Obtener valor activo
	 * @return activo valor activo
	 */
	public String getActivo() {
		return activo;
	}
	/**
	 * Asignar valor activo
	 * @param activo valor activo
	 */
	public void setActivo(String activo) {
		this.activo = activo;
	}
	/**
	 * Obtener id funcion
	 * @return idFunc clave de funcionalidad
	 */
	public String getIdFunc() {
		return idFunc;
	}
	/**
	 * Asignar id funcionalidad
	 * @param idFunc id funcionalidad
	 */
	public void setIdFunc(String idFunc) {
		this.idFunc = idFunc;
	}
	/**
	 * Obtener descripcion funcionalidad
	 * @return descFunc descripcion de funcioanlidad
	 */
	public String getDescFunc() {
		return descFunc;
	}
	/**
	 * Asignar descripcion de funcionalidad
	 * @param descFunc descripcion de funcionalidad
	 */
	public void setDescFunc(String descFunc) {
		this.descFunc = descFunc;
	}
	/**
	 * Obtener hora inicial semana
	 * @return horaIniSem hora inicial semana
	 */
	public String getHoraIniSem() {
		return horaIniSem;
	}
	/**
	 * Asignar hora inicial semana
	 * @param horaIniSem hora incial semana
	 */
	public void setHoraIniSem(String horaIniSem) {
		this.horaIniSem = horaIniSem;
	}
	/**
	 * Obtener hora fin semana
	 * @return horaFinSem hora fin semana
	 */
	public String getHoraFinSem() {
		return horaFinSem;
	}
	/**
	 * Asignar hora fin semana
	 * @param horaFinSem hora fin semana
	 */
	public void setHoraFinSem(String horaFinSem) {
		this.horaFinSem = horaFinSem;
	}
	/**
	 * Obtener hora inicial sabado
	 * @return horaIniSab hora inicial sabado
	 */
	public String getHoraIniSab() {
		return horaIniSab;
	}
	/**
	 * Asignar hora inicial sabado
	 * @param horaIniSab hora inicial sabado
	 */
	public void setHoraIniSab(String horaIniSab) {
		this.horaIniSab = horaIniSab;
	}
	/**
	 * Obtener hora fin sabado
	 * @return horaFinSab hora fin sabado
	 */
	public String getHoraFinSab() {
		return horaFinSab;
	}
	/**
	 * Asignar hora fin sabado
	 * @param horaFinSab hora fin sabado
	 */
	public void setHoraFinSab(String horaFinSab) {
		this.horaFinSab = horaFinSab;
	}
	/**
	 * Obtener hora inicial domingo
	 * @return horaIniDom hora fin domingo
	 */
	public String getHoraIniDom() {
		return horaIniDom;
	}
	/**
	 * Asignar hora fin domingo
	 * @param horaIniDom hora fin domingo
	 */
	public void setHoraIniDom(String horaIniDom) {
		this.horaIniDom = horaIniDom;
	}
	/**
	 * Obtener hora fin domingo
	 * @return horaFinDom hora fin domingo
	 */
	public String getHoraFinDom() {
		return horaFinDom;
	}
	/**
	 * Asignar hora fin domingo
	 * @param horaFinDom hora fin domingo
	 */
	public void setHoraFinDom(String horaFinDom) {
		this.horaFinDom = horaFinDom;
	}
	/**
	 * Obtener lunes
	 * @return lunes dia lunes
	 */
	public String getLunes() {
		return lunes;
	}
	/**
	 * Asignar valor lunes
	 * @param lunes valor lunes
	 */
	public void setLunes(String lunes) {
		this.lunes = lunes;
	}
	/**
	 * Obtener valor martes
	 * @return martes valor martes
	 */
	public String getMartes() {
		return martes;
	}
	/**
	 * Asignar valor martes
	 * @param martes valor martes
	 */
	public void setMartes(String martes) {
		this.martes = martes;
	}
	/**
	 * Obtener valor miercoles
	 * @return miercoles valor miercoles
	 */
	public String getMiercoles() {
		return miercoles;
	}
	/**
	 * Asignar valor miercoles
	 * @param miercoles valor miercoles
	 */
	public void setMiercoles(String miercoles) {
		this.miercoles = miercoles;
	}
	/**
	 * Obtener valor jueves
	 * @return jueves valor jueves
	 */
	public String getJueves() {
		return jueves;
	}
	/**
	 * Asignar valor jueves
	 * @param jueves valor jueves
	 */
	public void setJueves(String jueves) {
		this.jueves = jueves;
	}
	/**
	 * Ontener valor viernes
	 * @return viernes valor viernes
	 */
	public String getViernes() {
		return viernes;
	}
	/**
	 * Asignar valor viernes
	 * @param viernes valor viernes
	 */
	public void setViernes(String viernes) {
		this.viernes = viernes;
	}
	/**
	 * Obtener valor sabado
	 * @return sabado valor sabado
	 */
	public String getSabado() {
		return sabado;
	}
	/**
	 * Asignar valor sabado
	 * @param sabado valor sabado
	 */
	public void setSabado(String sabado) {
		this.sabado = sabado;
	}
	/**
	 * Obtener valor domingo
	 * @return valor domingo
	 */
	public String getDomingo() {
		return domingo;
	}
	/**
	 * Asignar valor domingo
	 * @param domingo valor domingo
	 */
	public void setDomingo(String domingo) {
		this.domingo = domingo;
	}
	/**
	 * Obtener valor inhabil
	 * @return valor inhabil
	 */
	public String getDiaInhabil() {
		return diaInhabil;
	}
	/**
	 * Asignar valor inhabil
	 * @param diaInhabil valor inhabil
	 */
	public void setDiaInhabil(String diaInhabil) {
		this.diaInhabil = diaInhabil;
	}
	/**
	 * Obtener hora actual
	 * @return horaActual valor hora actual
	 */
	public String getHoraActual() {
		return horaActual;
	}
	/**
	 * Asignar valor hora actual
	 * @param horaActual valor hora actual
	 */
	public void setHoraActual(String horaActual) {
		this.horaActual = horaActual;
	}
	/**
	 * Obtener dia sistema
	 * @return diaSistema valor dia sistema
	 */
	public String getDiaSistema() {
		return diaSistema;
	}
	/**
	 * Asignar valor dia sistema
	 * @param diaSistema valor dia sistema
	 */
	public void setDiaSistema(String diaSistema) {
		this.diaSistema = diaSistema;
	}
	/**
	 * Obtener dogigo de error
	 * @return codError codigo de error
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Asignar codigo de error
	 * @param codError codigo de error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Obtener hora inicio
	 * @return horaInicio valor hora inicio
	 */
	public String getHoraInicio() {
		return horaInicio;
	}
	/**
	 * Asignar hora inicio
	 * @param horaInicio valor hora inicio
	 */
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	/**
	 * Obtener valor hora fin
	 * @return horaFin valor hora fin
	 */
	public String getHoraFin() {
		return horaFin;
	}
	/**
	 * Asignar valor hora fin
	 * @param horaFin valor hora fin
	 */
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	/**
	 * Obtener descripcion de error
	 * @return descError descripcion error
	 */
	public String getDescError() {
		return descError;
	}
	/**
	 * Asignar descripcion de error
	 * @param descError descripcion de error
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}
	/**
	 * Obtener valor horario valido
	 * @return horarioValido valor horario valido
	 */
	public boolean isHorarioValido() {
		return horarioValido;
	}
	/**
	 * Asignar valor horario valido
	 * @param horarioValido valor horario valido
	 */
	public void setHorarioValido(boolean horarioValido) {
		this.horarioValido = horarioValido;
	}
	/**
	 * Obtener valor true si tiene horario activo
	 * @return tieneHorarioActivo valor horario activo
	 */
	public boolean getTieneHorarioActivo() {
		return tieneHorarioActivo;
	}
	/**
	 * Asignar false si no tiene activo horario
	 * @param tieneHorarioActivo valor tiene horario activo
	 */
	public void setTieneHorarioActivo(boolean tieneHorarioActivo) {
		this.tieneHorarioActivo = tieneHorarioActivo;
	}
	
	/**
	 * Obtiene el valor del campo nombreServlet
	 * @return Valor del campo nombreServlet
	 */
	public String getNombreServlet(){
		return nombreServlet;
	}
	/**
	 * Asigna el valor del campo nombreServlet
	 * @param nombreServlet Valor que sera asignado al campo nombreServlet
	 */
	public void setNombreServlet(String nombreServlet){
		this.nombreServlet = nombreServlet;
	}
}