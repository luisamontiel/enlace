package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * The Class ParametroConsultaCuentaBean.
 */
public class ParametroConsultaCuentaBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3353369345836428886L;
	
	/** The ventana. */
	private String ventana;
	
	/** The opcion. */
	private String opcion;
	
	/** The numero cuenta. */
	private String numeroCuenta;
	
	/** The descripcion. */
	private String descripcion;
	
	/** The nombre combo. */
	private String nombreCombo;
	
	/**
	 * Gets the ventana.
	 * @return the ventana
	 */
	public String getVentana() {
		return ventana;
	}
	
	/**
	 * Sets the ventana.
	 * @param ventana the new ventana
	 */
	public void setVentana(String ventana) {
		this.ventana = ventana;
	}
	
	/**
	 * Gets the opcion.
	 * @return the opcion
	 */
	public String getOpcion() {
		return opcion;
	}
	
	/**
	 * Sets the opcion.
	 * @param opcion the new opcion
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	
	/**
	 * Gets the numero cuenta.
	 * @return the numero cuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	
	/**
	 * Sets the numero cuenta.
	 * @param numeroCuenta the new numero cuenta
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	/**
	 * Gets the descripcion.
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * Sets the descripcion.
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * Gets the nombre combo.
	 * @return the nombre combo
	 */
	public String getNombreCombo() {
		return nombreCombo;
	}
	
	/**
	 * Sets the nombre combo.
	 * @param nombreCombo the new nombre combo
	 */
	public void setNombreCombo(String nombreCombo) {
		this.nombreCombo = nombreCombo;
	}
}