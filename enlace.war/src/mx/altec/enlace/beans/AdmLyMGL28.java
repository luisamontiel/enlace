package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminlym.shared.ValidacionLyM;


import java.io.Serializable;

import mx.altec.enlace.utilerias.EIGlobal;

public class AdmLyMGL28 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL2800721123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCGLM281 P";
	
	private static final AdmLyMGL28Factory FACTORY = new AdmLyMGL28Factory();
	
	private ValidacionLyM validacion;
	
	public static AdmLyMGL28Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmLyMGL28Factory implements AdmUsrBuilder<AdmLyMGL28> {

		public AdmLyMGL28 build(String arg) {
			
			AdmLyMGL28 bean = new AdmLyMGL28();
			ValidacionLyM loBean = new ValidacionLyM();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL280000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_HEADER);							
				if (index != -1) {
					index += FORMATO_HEADER.length();
					EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
					loBean.setContrato(getValor(arg, index, 11));
					EIGlobal.mensajePorTrace("==> contrato: [" + loBean.getContrato() + "]", EIGlobal.NivelLog.INFO);
					loBean.setUsuario(getValor(arg, index += 11, 8));
					EIGlobal.mensajePorTrace("==> usuario: [" + loBean.getUsuario() + "]", EIGlobal.NivelLog.INFO);
					loBean.setClaveOperacion(getValor(arg, index += 8, 4));
					EIGlobal.mensajePorTrace("==> grupo: [" + loBean.getClaveOperacion() + "]", EIGlobal.NivelLog.INFO);
					loBean.setImporte(getValor(arg, index += 4, 17));
					EIGlobal.mensajePorTrace("==> importe: [" + loBean.getImporte() + "]", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
				}
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setValidacion(loBean);
			return bean;
		}
		
	}

	public ValidacionLyM getValidacion() {
		return validacion;
	}

	public void setValidacion(ValidacionLyM validacion) {
		this.validacion = validacion;
	}


}
