/**
 * Isban Mexico
 *   Clase: TablaTCTProgramacionBean.java
 *   Descripcion: Implementacion de TablaTCTProgramacionBean, bean de datos para la tabla tct_programacion
 *
 *   Control de Cambios:
 *   1.0 Mar 01, 2015 asanjuan - Creacion
 *   
 **/
package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * Creacion TablaTCTProgramacionBean
 * @author Arturo
 * @version 1.0
 * 
 */
public class TablaTCTProgramacionBean extends ProgramacionBean implements Serializable {
	
	/**
	 * Serial generado
	 */
	private static final long serialVersionUID = -3036586855426942151L;
	

	/**referencia bit**/
	private String referenciaBit;
	/**Numero de cuenta**/
	private String numCuenta;
	/**numero de cuenta 1 cargo**/
	private String cuenta1;
	/**numero de cuenta 2 destino**/
	private String cuenta2;
	/**tipo de cuenta origen**/
	private String tipoCuentaOrigen;
	/**tipo de cuenta destino**/
	private String tipoCuentaDestino;
	/**titulos**/
	private String titulos;
	/**clave de perfil**/
	private String cvePerfil;
	/**decha de registro**/
	private String fchRegistro;
	/**forma de aplicacion**/
	private String formaAplicaion;
	/**beneficiario**/
	private String beneficiario;
	/**Referecia interbancaria**/
	private String referenciaInterbancaria;
	/**Estatus**/
	private String estatus;
	/**Para otro tipo de dato**/
	private String datoAux;
	
	/**
	 * @return el estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus a establecer
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return el datoAux
	 */
	public String getDatoAux() {
		return datoAux;
	}
	/**
	 * @param datoAux el datoAux a establecer
	 */
	public void setDatoAux(String datoAux) {
		this.datoAux = datoAux;
	}
	/**
	 * @return el referenciaInterbancaria
	 */
	public String getReferenciaInterbancaria() {
		return referenciaInterbancaria;
	}
	/**
	 * @param referenciaInterbancaria el referenciaInterbancaria a establecer
	 */
	public void setReferenciaInterbancaria(String referenciaInterbancaria) {
		this.referenciaInterbancaria = referenciaInterbancaria;
	}
	/**
	 * @return el beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}
	/**
	 * @param beneficiario el beneficiario a establecer
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	/**
	 * @return el referenciaBit
	 */
	public String getReferenciaBit() {
		return referenciaBit;
	}
	/**
	 * @param referenciaBit el referenciaBit a establecer
	 */
	public void setReferenciaBit(String referenciaBit) {
		this.referenciaBit = referenciaBit;
	}
	/**
	 * @return el numCuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}
	/**
	 * @param numCuenta el numCuenta a establecer
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	/**
	 * @return el cuenta1
	 */
	public String getCuenta1() {
		return cuenta1;
	}
	/**
	 * @param cuenta1 el cuenta1 a establecer
	 */
	public void setCuenta1(String cuenta1) {
		this.cuenta1 = cuenta1;
	}
	/**
	 * @return el cuenta2
	 */
	public String getCuenta2() {
		return cuenta2;
	}
	/**
	 * @param cuenta2 el cuenta2 a establecer
	 */
	public void setCuenta2(String cuenta2) {
		this.cuenta2 = cuenta2;
	}
	/**
	 * @return el tipoCuentaOrigen
	 */
	public String getTipoCuentaOrigen() {
		return tipoCuentaOrigen;
	}
	/**
	 * @param tipoCuentaOrigen el tipoCuentaOrigen a establecer
	 */
	public void setTipoCuentaOrigen(String tipoCuentaOrigen) {
		this.tipoCuentaOrigen = tipoCuentaOrigen;
	}
	/**
	 * @return el tipoCuentaDestino
	 */
	public String getTipoCuentaDestino() {
		return tipoCuentaDestino;
	}
	/**
	 * @param tipoCuentaDestino el tipoCuentaDestino a establecer
	 */
	public void setTipoCuentaDestino(String tipoCuentaDestino) {
		this.tipoCuentaDestino = tipoCuentaDestino;
	}
	/**
	 * @return el titulos
	 */
	public String getTitulos() {
		return titulos;
	}
	/**
	 * @param titulos el titulos a establecer
	 */
	public void setTitulos(String titulos) {
		this.titulos = titulos;
	}
	/**
	 * @return el cvePerfil
	 */
	public String getCvePerfil() {
		return cvePerfil;
	}
	/**
	 * @param cvePerfil el cvePerfil a establecer
	 */
	public void setCvePerfil(String cvePerfil) {
		this.cvePerfil = cvePerfil;
	}
	/**
	 * @return el fchRegistro
	 */
	public String getFchRegistro() {
		return fchRegistro;
	}
	/**
	 * @param fchRegistro el fchRegistro a establecer
	 */
	public void setFchRegistro(String fchRegistro) {
		this.fchRegistro = fchRegistro;
	}
	/**
	 * @return el formaAplicaion
	 */
	public String getFormaAplicaion() {
		return formaAplicaion;
	}
	/**
	 * @param formaAplicaion el formaAplicaion a establecer
	 */
	public void setFormaAplicaion(String formaAplicaion) {
		this.formaAplicaion = formaAplicaion;
	}	
}