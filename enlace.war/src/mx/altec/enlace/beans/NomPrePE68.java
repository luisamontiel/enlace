package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 * @author gmiranda
 *
 */
public class NomPrePE68 extends NomPreEmpleadoDatos implements Serializable {

	private static final long serialVersionUID = -136722112706803039L;

	/**
	 * Header de entrada para la transaccion PE68
	 */
	public static final String HEADER = "PE6809451123451O00N2";

	/**
	 * Constructor de salida de transaccion
	 */
	private static final NomPrePE68Factory FACTORY = new NomPrePE68Factory();

	/** Obtiene la instancia del constructor de la transaccion
	 * @return NomPreBuilder
	 */
	public static NomPrePE68Factory getFactoryInstance() {
		return FACTORY;
	}

	/**
	 * Clase usada para generar un elemento NomPrePE68 a partir de la cadena de salida
	 * de la transaccion
	 * @author gmiranda
	 *
	 */
	private static class NomPrePE68Factory implements AdmUsrBuilder<NomPrePE68> {

		/**
		 * Componente encargado de construir el objeto NomPrePE68 a partir de una cadena de entrada
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg Cadena de entrada
		 */
		public NomPrePE68 build(String arg) {

			NomPrePE68 bean = new NomPrePE68();

			EIGlobal.mensajePorTrace("Validando: " + arg, EIGlobal.NivelLog.DEBUG);

			try {
				if (arg.indexOf("@ER") <= 0) {
					EIGlobal.mensajePorTrace("Trama exitosa....", EIGlobal.NivelLog.DEBUG);

					int i = 81;

					bean.setMaterno(arg.substring(i, i+= NomPreEmpleado.LNG_NOMBRE).trim());
					bean.setNombre(arg.substring(i, i+= NomPreEmpleado.LNG_PATERNO).trim());
					bean.setPaterno(arg.substring(i, i+= NomPreEmpleado.LNG_MATERNO).trim());
					bean.setFechaIngreso(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_FECHA_INGRESO).trim());
					bean.setSucursalAdmin(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_SUCURSAL_ADMIN).trim());
					bean.setEstadoCivil(arg.substring(i, i+= NomPreEmpleado.LNG_ESTADO_CIVIL).trim());
					bean.setSexo(arg.substring(i, i+= NomPreEmpleado.LNG_SEXO).trim());
					bean.setTipoPersona(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_TIPO_PERSONA).trim());
					bean.setFechaNacimiento(arg.substring(i, i+= NomPreEmpleado.LNG_FECHA_NACIMIENTO).trim());
					bean.setFechaInicioActiv(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_FECHA_INICIO_ACT).trim());
					bean.setCanalVenta(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CANAL_VENTA).trim());
					bean.setCanalCaptacion(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CANAL_CAPTACION).trim());
					bean.setPaisOrigen(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_PAIS_ORIGEN).trim());
					bean.setCodigoCampania(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CODIGO_CAMPANIA).trim());
					bean.setAgrupacionOficiales(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_AGRUPACION_OFICIALES).trim());
					bean.setNacionalidad(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_NACIONALIDAD3).trim());
					bean.setPaisResidencia(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_PAIS_RESIDENCIA).trim());
					bean.setSector(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_SECTOR).trim());
					bean.setTipoOcupacion(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_TIPO_OCUPACION).trim());
					bean.setCodigoActividad(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CODIGO_ACTIVIDAD).trim());
					bean.setTiempoResidencia(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_TIEMPO_RESIDENCIA).trim());
					bean.setFormaJuridica(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_FORMA_JURIDICA).trim());
					bean.setNaturalezaJuridica(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_NATURALEZA_JURIDICA).trim());
					bean.setUnidadNegocio(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_UNIDAD_NEGOCIO).trim());
					bean.setCodigoSujeto(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CODIGO_SUJETO).trim());
					bean.setMarcaBancaPrivada(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_MARCA_BANCA_PRIVADA).trim());
					bean.setNivelAcceso(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_NIVEL_ACCESO).trim());
					bean.setEstadoPersona(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_ESTADO_PERSONA).trim());
					bean.setCondicionPersona(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CONDICION_PERSONA).trim());
					bean.setIndicadores(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_INDICADORES).trim());
					bean.setCodActRiu(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_COD_ACT_RIU).trim());
					bean.setSubsegmento(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_SUBSEGMENTO).trim());
					bean.setIdioma(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_IDIOMA).trim());
					bean.setTimeStamp1(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_TIMESTAMP1).trim());
					bean.setExpedidoPor(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_EXPEDIDO_POR).trim());
					bean.setFechaVencimiento(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_FECHA_VENCIMIENTO).trim());
					bean.setMarcaVerificacion(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_MARCA_VERIFICACION).trim());
					bean.setCondicionDocumento(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CONDICION_DOCUMENTO).trim());
					bean.setCodigoPais(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_CODIGO_PAIS).trim());
					bean.setFechaExpedicion(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_FECHA_EXPEDICION).trim());
					bean.setLugarExposicion(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_LUGAR_EXPOSICION).trim());
					bean.setNumeroOCR(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_NUMERO_OCR).trim());
					bean.setNumeroFolioRFC(arg.substring(i, i+= NomPreEmpleadoDatos.LNG_NUMERO_FOLIO_RFC).trim());

				} else {
					EIGlobal.mensajePorTrace("Se obtuvo un problema en la trama... " + isCodigoError(arg), EIGlobal.NivelLog.WARN);

					bean.setNombre("No disponible");
					bean.setPaterno("");
					bean.setMaterno("");
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("Se genero un problema al desentramar" + e.getMessage(), EIGlobal.NivelLog.WARN);

				if (bean.getNombre() == null || bean.getNombre().equals("")) {
					bean.setNombre("No disponible");
					bean.setPaterno("");
					bean.setMaterno("");
				}
			}

			return bean;
		}

	}
}