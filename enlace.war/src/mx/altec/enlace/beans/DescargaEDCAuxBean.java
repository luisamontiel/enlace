/**
 * 
 */
package mx.altec.enlace.beans;

/**
 * @author FSW-Indra
 * @sice 24/04/2015
 *
 */
public class DescargaEDCAuxBean {
	
	/** Numero de Cliente **/
	private String numCliente;
	/** Numero de Cuenta **/
	private String numCuenta;
	/** Numero de tarjeta (PAN) **/
	private String numTarjeta;
	/** Numero de Contrato **/
	private String numContrato;
	/** Numero de Persona **/
	private String numPersona;
	/** Codigo de Participante **/
	private String codParticipante;
	/** Codigo de Entidad **/
	private String codEntidad;
	/** Centro de Alta **/
	private String centAlta;
	
	/**
	 * @return el numCliente
	 */
	public String getNumCliente() {
		return numCliente;
	}
	/**
	 * @param numCliente el numCliente a establecer
	 */
	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}
	/**
	 * @return el numCuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}
	/**
	 * @param numCuenta el numCuenta a establecer
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	/**
	 * @return el numTarjeta
	 */
	public String getNumTarjeta() {
		return numTarjeta;
	}
	/**
	 * @param numTarjeta el numTarjeta a establecer
	 */
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	/**
	 * @return el numContrato
	 */
	public String getNumContrato() {
		return numContrato;
	}
	/**
	 * @param numContrato el numContrato a establecer
	 */
	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}
	/**
	 * @return el numPersona
	 */
	public String getNumPersona() {
		return numPersona;
	}
	/**
	 * @param numPersona el numPersona a establecer
	 */
	public void setNumPersona(String numPersona) {
		this.numPersona = numPersona;
	}
	/**
	 * @return el codParticipante
	 */
	public String getCodParticipante() {
		return codParticipante;
	}
	/**
	 * @param codParticipante el codParticipante a establecer
	 */
	public void setCodParticipante(String codParticipante) {
		this.codParticipante = codParticipante;
	}
	/**
	 * @return el codEntidad
	 */
	public String getCodEntidad() {
		return codEntidad;
	}
	/**
	 * @param codEntidad el codEntidad a establecer
	 */
	public void setCodEntidad(String codEntidad) {
		this.codEntidad = codEntidad;
	}
	/**
	 * @return el centAlta
	 */
	public String getCentAlta() {
		return centAlta;
	}
	/**
	 * @param centAlta el centAlta a establecer
	 */
	public void setCentAlta(String centAlta) {
		this.centAlta = centAlta;
	}

}
