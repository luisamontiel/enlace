package mx.altec.enlace.beans;

public class ArchivoNominaDetBean {
	
	int    secuencia;
	String idEmpleado;
	String AppPaterno;
	String AppMaterno;
	String nombre;
	String cuentaAbono;
	double importe;
	String tipoPago;
	
	
	public int getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(int secuencia) {
		this.secuencia = secuencia;
	}
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getAppPaterno() {
		return AppPaterno;
	}
	public void setAppPaterno(String appPaterno) {
		AppPaterno = appPaterno;
	}
	public String getAppMaterno() {
		return AppMaterno;
	}
	public void setAppMaterno(String appMaterno) {
		AppMaterno = appMaterno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCuentaAbono() {
		return cuentaAbono;
	}
	public void setCuentaAbono(String cuentaAbono) {
		this.cuentaAbono = cuentaAbono;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	

}