package mx.altec.enlace.beans;

public class CepBean {
	/**
	 * bancoSantander bancoSantander
	 */
	private int bancoSantander;
	/**
	 * banco banco
	 */
	private int banco;
	/**
	 * nomBancoSantander nomBancoSantander
	 */
	private String nomBancoSantander;
	/**
	 * nomBanco nomBanco
	 */
	private String nomBanco;
	/***
	 * fecha fecha
	 */
	private String fecha;
	/**
	 * cveRastreo cveRastreo
	 */
	private String cveRastreo;
	/**
	 * ctaAbono ctaAbono
	 */
	private String ctaAbono;
	/**
	 * importe importe
	 */
	private String importe;
	/**
	 * ctaCargo ctaCargo
	 */
	private String ctaCargo;
	/**
	 * bandera bandera
	 */
	private boolean bandera;
	/**
	 * errorBandera errorBandera
	 */
	private String errorBandera;
	
	/**
	 * @author FWS TCS marreguin
	 * deposito a banco valores [S,N]
	 * @since09/12/2016
	 */
	private String depositoABanco;
	/**
	 * getBancoSantander getBancoSantander
	 * @return bancoSantander
	 */
	public int getBancoSantander() {
		return bancoSantander;
	}
	/**
	 * setBancoSantander setBancoSantander
	 * @param bancoSantander bancoSantander
	 */
	public void setBancoSantander(int bancoSantander) {
		this.bancoSantander = bancoSantander;
	}
	/**
	 * getBanco getBanco
	 * @return banco
	 */
	public int getBanco() {
		return banco;
	}
	/**
	 * setBanco setBanco
	 * @param banco banco
	 */
	public void setBanco(int banco) {
		this.banco = banco;
	}
	/**
	 * getNomBancoSantander getNomBancoSantander
	 * @return nomBancoSantander
	 */
	public String getNomBancoSantander() {
		return nomBancoSantander;
	}
	/**
	 * setNomBancoSantander setNomBancoSantander
	 * @param nomBancoSantander nomBancoSantander
	 */
	public void setNomBancoSantander(String nomBancoSantander) {
		this.nomBancoSantander = nomBancoSantander;
	}
	/**
	 * getNomBanco getNomBanco
	 * @return nomBanco nomBanco
	 */
	public String getNomBanco() {
		return nomBanco;
	}
	/**
	 * setNomBanco setNomBanco
	 * @param nomBanco nomBanco
	 */
	public void setNomBanco(String nomBanco) {
		this.nomBanco = nomBanco;
	}
	/**
	 * getFecha getFecha
	 * @return Fecha Fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * setFecha setFecha
	 * @param fecha fecha
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * getCveRastreo getCveRastreo
	 * @return cveRastreo
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * setCveRastreo setCveRastreo
	 * @param cveRastreo cveRastreo
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	/**
	 * getCtaAbono getCtaAbono
	 * @return ctaAbono ctaAbono
	 */
	public String getCtaAbono() {
		return ctaAbono;
	}
	/**
	 * setCtaAbono setCtaAbono
	 * @param ctaAbono ctaAbono
	 */
	public void setCtaAbono(String ctaAbono) {
		this.ctaAbono = ctaAbono;
	}
	/**
	 * getImporte getImporte
	 * @return importe importe
	 */
	public String getImporte() {
		return importe;
	}
	/**
	 * setImporte setImporte
	 * @param importe importe
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}
	/**
	 * getCtaCargo getCtaCargo
	 * @return ctaCargo
	 */
	public String getCtaCargo() {
		return ctaCargo;
	}
	/**
	 * setCtaCargo setCtaCargo
	 * @param ctaCargo ctaCargo
	 */
	public void setCtaCargo(String ctaCargo) {
		this.ctaCargo = ctaCargo;
	}
	/**
	 * isBandera isBandera
	 * @return bandera bandera
	 */
	public boolean isBandera() {
		return bandera;
	}
	/**
	 * setBandera setBandera
	 * @param bandera bandera
	 */
	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}
	/**
	 * getErrorBandera getErrorBandera
	 * @return errorBandera errorBandera
	 */
	public String getErrorBandera() {
		return errorBandera;
	}
	/**
	 * setErrorBandera setErrorBandera
	 * @param errorBandera errorBandera
	 */
	public void setErrorBandera(String errorBandera) {
		this.errorBandera = errorBandera;
	}
	/**
	 * @author FWS TCS marreguin
	 * deposito a banco valores [S,N]
	 * @since09/12/2016
	 * @return String
	 */
	public String getDepositoABanco() {
		return depositoABanco;
	}
	/**
	 * @author FWS TCS marreguin
	 * deposito a banco valores [S,N]
	 * @since09/12/2016
	 * @param String
	 */
	public void setDepositoABanco(String depositoABanco) {
		this.depositoABanco = depositoABanco;
	}

	
}
