package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static mx.altec.enlace.beans.NomPreTarjeta.*;

public class NomPreLM1C extends NomPreLM1Base implements Serializable {
	
	private static final long serialVersionUID = -6880234374400979326L;

	public static  String HEADER = "LM1C12151123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCLMRREM1 P";
	
	private static final String FORMATO_DETALLE = "@DCLMRREM2 P";
	
	private static final NomPreLM1CFactory FACTORY = new NomPreLM1CFactory();
	
	private int totalTarjetas;
	
	private List<NomPreTarjeta> detalle;

	public List<NomPreTarjeta> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<NomPreTarjeta> detalle) {
		this.detalle = detalle;
	}

	public int getTotalTarjetas() {
		return totalTarjetas;
	}

	public void setTotalTarjetas(int totalTarjetas) {
		this.totalTarjetas = totalTarjetas;
	}
	
	public static NomPreLM1CFactory getFactoryInstance() {
		return FACTORY;
	}
	
	private static class NomPreLM1CFactory implements NomPreBuilder<NomPreLM1C> {

		public NomPreLM1C build(String arg) {
					
			NomPreLM1C bean = new NomPreLM1C();
	
			if (isCodigoExito(arg)) {			
				bean.setCodigoOperacion("LM1C0000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);									
				
				if (index != -1) {
					
					index += FORMATO_HEADER.length();
					bean.setTotalTarjetas(Integer.parseInt(getValor(arg, index, 6))); //TOT-REMESAS
				}
								
				String[] detalles = getDetalles(arg, FORMATO_DETALLE, 62);
	
				if (detalles != null && detalles.length > 0) {					
					
					if (bean.getDetalle() == null) {
						
						bean.setDetalle(new ArrayList<NomPreTarjeta>());
					}
					
					for (String detalle : detalles) {
						
						if (detalle.length() == 0) {
							continue;
						}				
						
						NomPreTarjeta remesa = new NomPreTarjeta(
							getValor(detalle, 0, LNG_NO_TARJETA), 		//NUM-TARJETA-S
							null, 
							null,
							getValor(detalle, 22, LNG_DESCRIPCION), 	//DES-ESTADO-TAR-S
							getValor(detalle, 42, LNG_OBSERVACIONES));	//DES-OBSERVACIONES																	
	
						bean.getDetalle().add(remesa);
					}
				}
				
			} else if (isCodigoError(arg))  {
				
				bean.setCodigoOperacion(getCodigoError(arg));
			}	
	
			return bean;
		}
	}		
}

