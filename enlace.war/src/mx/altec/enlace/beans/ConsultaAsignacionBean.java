package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class ConsultaAsignacionBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<ConsultaAsignacionTEResBean> listaAsignacion;

	public ArrayList<ConsultaAsignacionTEResBean> getListaAsignacion() {
		return listaAsignacion;
	}

	public void setListaAsignacion(
			ArrayList<ConsultaAsignacionTEResBean> listaAsignacion) {
		this.listaAsignacion = listaAsignacion;
	}

}