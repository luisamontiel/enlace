package mx.altec.enlace.beans;

import java.io.Serializable;

import mx.altec.enlace.utilerias.RSAFII;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.stu.ws.Image;
import mx.isban.rsa.aa.ws.ChallengeQuestion;

/**
 * @author Stefanini
 *
 */
public class RSABeanAUX implements Serializable {

	/**
	 * seria generado
	 */
	private static final long serialVersionUID = 2611789844798065166L;

	/**
	 *valor del metodo a ejecutar 
	 */
	private RSAFII.valorMetodo valorMetodo;
	
	/**
	 *valor de rsaBean 
	 */
	private RSABean rsaBean;
	
	/**
	 *imagen seleccionada por el usuario 
	 */
	private Image imageRSA;
	
	/**
	 * arreglo de preguntas 
	 */
	private ChallengeQuestion[] arrayChallengeQuesitons;
	
	/**
	 * @return enemu : valor del metodo a ejecutar
	 */
	public RSAFII.valorMetodo getValorMetodo() {
		return valorMetodo;
	}
	
	/**
	 * @param valorMetodo : metodo a ejecutar
	 */
	public void setValorMetodo(RSAFII.valorMetodo valorMetodo) {
		this.valorMetodo = valorMetodo;
	}
	
	/**
	 * @return RSABean : bean de rsa
	 */
	public RSABean getRsaBean() {
		return rsaBean;
	}
	
	/**
	 * @param rsaBean : bean
	 */
	public void setRsaBean(RSABean rsaBean) {
		this.rsaBean = rsaBean;
	}
	/**
	 * @return Image : imagen del usuario rsa
	 */
	public Image getImageRSA() {
		return imageRSA;
	}
	
	/**
	 * @param imageRSA : imagen rsa
	 */
	public void setImageRSA(Image imageRSA) {
		this.imageRSA = imageRSA;
	}

	/**
	 * @return ChallengeQuestion : preguntas
	 */
	public ChallengeQuestion[] getArrayChallengeQuesitons() {
		return arrayChallengeQuesitons;
	}

	/**
	 * @param arrayChallengeQuesitons : arrayChallengeQuesitons
	 */
	public void setArrayChallengeQuesitons(
			ChallengeQuestion[] arrayChallengeQuesitons) {
		this.arrayChallengeQuesitons = arrayChallengeQuesitons;
	}
	
	

	}