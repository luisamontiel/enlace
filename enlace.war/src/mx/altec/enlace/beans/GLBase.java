package mx.altec.enlace.beans;
/**
 * 
 * @author Z096114 Emmanuel Sanchez Castillo
 * @version 1.0
 * @Fecha 12 Noviembre 2010
 * @proyecto 201001500 - EBEE (Estrategia de banca electronica Enlace
 *
 */
public class GLBase {
	
	private String codTran;
	private String codMsg;
	private String mensaje;
	
	
	public String getCodMsg() {
		return codMsg;
	}
	public void setCodMsg(String codMsg) {
		this.codMsg = codMsg;
	}
	public String getCodTran() {
		return codTran;
	}
	public void setCodTran(String codTran) {
		this.codTran = codTran;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	

}
