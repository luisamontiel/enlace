package mx.altec.enlace.beans;

import java.util.List;

/**
 * @author asanjuan
 *
 */

public class ConfEdosCtaArchivoDatosBean {
	
	/**lista de datos tipo  ConfEdosCtaArchivoBean**/
	private List<ConfEdosCtaArchivoBean> listaDatos;
	/**nombre del archivo exportado**/
	private String nombreArchivo;
	/**idUsuario**/
	private String idUsuario;
	/**numeroCuenta**/
	private String numeroCuenta;
	
	/**
	 * get lista de datos
	 * @return lista de datos
	 */
	public List<ConfEdosCtaArchivoBean> getListaDatos() {
		return listaDatos;
	}
	/**
	 * set lista de datos
	 * @param listaDatos : listaDatos
	 */
	public void setListaDatos(List<ConfEdosCtaArchivoBean> listaDatos) {
		this.listaDatos = listaDatos;
	}
	/**
	 * get nombre del archivo
	 * @return String : nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * set nombre del archivo
	 * @param nombreArchivo : nombreArchivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * get idUsuario
	 * @return String : idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}
	/**
	 * set idUsuario
	 * @param idUsuario : idUsuario
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	/**
	 * get numeroCuenta
	 * @return String : numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	/**set numeroCuenta
	 * @param numeroCuenta : numeroCuenta
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	
	
	
}
