package mx.altec.enlace.beans;

public class CuentaInternacionalBean extends CuentaInternacionalBenefBean{

	/**
	 * NumeroInternacional NumeroInternacional
	 */
	private String numeroInternacional;
	/**
	 * Titular Titular
	 */
	private String titular;
	/**
	 * Pais Pais
	 */
	private String pais;
	/**
	 * Banco Banco
	 */
	private String banco;
	/**
	 * Ciudad Ciudad
	 */
	private String ciudad;
	/**
	 * Divisa Divisa
	 */
	private String divisa;
	/**
	 * Aba Aba
	 */
	private String aba;
	/**
	 * Swift Swift
	 */
	private String swift;
	
	/**
	 * variable para el contrato
	 */
	private String contrato;
	/**
	 * variable regIni
	 */
	private int regIni;
	
	/**
	 * variable regFin
	 */
	private int regFin;
	
	/**
	 * variable criterio
	 */
	private String criterio;
	
	/**
	 * variable textoBuscar
	 */
	private String textoBuscar;
	
	/**
	 * Constructor
	 * @param numeroInternacional : numeroInternacional
	 * @param titular : titular
	 * @param pais : pais
	 * @param banco : banco
	 * @param ciudad : ciudad
	 * @param divisa : divisa
	 * @param aba : aba
	 * @param swift : swift
	 */
    public CuentaInternacionalBean(String numeroInternacional, String titular, String pais, String banco, String ciudad, String divisa, String aba, String swift) {
        super();
        this.numeroInternacional = numeroInternacional;
        this.titular = titular;
        this.pais = pais;
        this.banco = banco;
        this.ciudad = ciudad;
        this.divisa = divisa;
        this.aba = aba;
        this.swift = swift;
        
    }
    /**
     * Constructor
     */
    public CuentaInternacionalBean(){
    	
    }
	
	/**
	 * getNumeroInternacional NumeroInternacional
	 * @return NumeroInternacional
	 */
	public String getnumeroInternacional() {
		return numeroInternacional;
	}
	/**
	 * setNumeroInternacional setNumeroInternacional
	 * @param numeroInternacional : numeroInternacional
	 */
	public void setnumeroInternacional(String numeroInternacional) {
		this.numeroInternacional = numeroInternacional;
	}
	/**
	 * getTitular Titular
	 * @return Titular
	 */
	public String gettitular() {
		return titular;
	}
	/**
	 * setTitular setTitular
	 * @param titular : titular
	 */
	public void settitular(String titular) {
		this.titular = titular;
	}
	/**
	 * getPais Pais
	 * @return Pais
	 */
	public String getpais() {
		return pais;
	}
	/**
	 * setPais setPais
	 * @param pais : pais
	 */
	public void setpais(String pais) {
		this.pais = pais;
	}
	/**
	 * getBanco Banco
	 * @return Banco
	 */
	public String getbanco() {
		return banco;
	}
	/**
	 * setBanco setBanco
	 * @param banco : banco
	 */
	public void setbanco(String banco) {
		this.banco = banco;
	}
	/**
	 * getCiudad getCiudad
	 * @return Ciudad
	 */
	public String getciudad() {
		return ciudad;
	}
	/**
	 * setCiudad setCiudad
	 * @param ciudad : ciudad
	 */
	public void setciudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * getDivisa getDivisa
	 * @return Divisa
	 */
	public String getdivisa() {
		return divisa;
	}
	/**
	 * setDivisa setDivisa
	 * @param divisa : divisa
	 */
	public void setdivisa(String divisa) {
		this.divisa = divisa;
	}
	/**
	 * getAba getAba
	 * @return Aba
	 */
	public String getaba() {
		return aba;
	}
	/**
	 * setAba setAba
	 * @param aba : aba
	 */
	public void setaba(String aba) {
		this.aba = aba;
	}
	/**
	 * getSwift getSwift
	 * @return Swift
	 */
	public String getswift() {
		return swift;
	}
	/**
	 * setSwift setSwift
	 * @param swift : swift
	 */
	public void setswift(String swift) {
		this.swift = swift;
	}
	/**
	 * getContrato getContrato
	 * @return contrato
	 */
	public String getContrato() {
		return contrato;
	}
	/**
	 * setContrato setContrato
	 * @param contrato : contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * getRegIni getRegIni
	 * @return regIni
	 */
	public int getRegIni() {
		return regIni;
	}
	/**
	 * setRegIni setRegIni
	 * @param regIni : regIni
	 */
	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}
	/**
	 * getRegFin getRegFin
	 * @return regFin
	 */
	public int getRegFin() {
		return regFin;
	}
	/**
	 * setRJegFin setRJegFin
	 * @param regFin : regFin
	 */
	public void setRegFin(int regFin) {
		this.regFin = regFin;
	}
	/**
	 * getCriterio getCriterio
	 * @return criterio
	 */
	public String getCriterio() {
		return criterio;
	}
	/**
	 * setCriterio setCriterio
	 * @param criterio : criterio
	 */
	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}
	/**
	 * getTextoBuscar getTextoBuscar
	 * @return textoBuscar
	 */
	public String getTextoBuscar() {
		return textoBuscar;
	}
	/**
	 * setTextoBuscar setTextoBuscar
	 * @param textoBuscar : textoBuscar
	 */
	public void setTextoBuscar(String textoBuscar) {
		this.textoBuscar = textoBuscar;
	}
}