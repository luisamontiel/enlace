package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;

import mx.altec.enlace.utilerias.EIGlobal;

public class LMXF extends AdmUsrGL35Base implements Serializable {

	/**
	 * Header de entrada para la transaccion LMXE
	 */
	public static String HEADER = "LMXF00891123451O00N2";
	/**
	 * Identificador de header
	 */
	private static final String FORMATO_HEADER = "@DCLMRREM1";

	private static final String MODIFICACION_EFECTUADA = "MODIFICACION EFECTUADA";
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Entidad de las remesas
	 */
	private String entidad = "0014";
	/**
	 * Contrato enlace de las remesas
	 */
	private String contratoEnlace;

	private String folioRemesa2;

	private String fechaIni;

	private String fechaFin;

	private String tipoFecha;



	public String getTipoFecha() {
		return tipoFecha;
	}

	public void setTipoFecha(String tipoFecha) {
		this.tipoFecha = tipoFecha;
	}

	public String getFolioRemesa2() {
		return folioRemesa2;
	}

	public void setFolioRemesa2(String folioRemesa2) {
		this.folioRemesa2 = folioRemesa2;
	}

	public String getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	private String numeroRemesa;
	public String getNumeroRemesa() {
		return numeroRemesa;
	}

	public void setNumeroRemesa(String numeroRemesa) {
		this.numeroRemesa = numeroRemesa;
	}

	/**
	 * Contrato distribuci&oacute;n de las remesas
	 */
	private String ctroDistribucion;
	/**
	 * Estado de las tarjetas
	 */
	private String estadoTarjeta;
	/**
	 * N&uacute;mero de tarjeta
	 */
	private String tarjeta;
	/**
	 * Constructor de salida de transaccion
	 */
	private static final NomPreLMXFFactory FACTORY = new NomPreLMXFFactory();

	public LMXF() {
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getContratoEnlace() {
		return contratoEnlace;
	}

	public void setContratoEnlace(String contratoEnlace) {
		this.contratoEnlace = contratoEnlace;
	}

	public String getCtroDistribucion() {
		return ctroDistribucion;
	}

	public void setCtroDistribucion(String ctroDistribucion) {
		this.ctroDistribucion = ctroDistribucion;
	}

	public void setEstadoTarjeta(String estadoTarjeta) {
		this.estadoTarjeta = estadoTarjeta;
	}

	public String getEstadoTarjeta() {
		return estadoTarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public static NomPreLMXFFactory getFactoryInstance() {
		return FACTORY;
	}

	/**
	 * Clase usada para generar un elemento LMXC a partir de la cadena de salida
	 * de la transaccion
	 *
	 * @author ESC
	 */
	private static class NomPreLMXFFactory implements AdmUsrBuilder<LMXF> {

		/**
		 * Componente encargado de construir el objeto LMXC a partir de una
		 * cadena de entrada
		 *
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg
		 *            Cadena de entrada
		 */
		public LMXF build(String arg) {

			LMXF bean = new LMXF();
			// if (isCodigoExito(arg)) {
			// if (arg.contains(FORMATO_HEADER)) {
			// bean.setCodExito(true);
			// bean.setCodigoOperacion("LMX80000");
			// } else {
			// bean.setCodExito(false);
			// bean.setCodigoOperacion("LMX80001");
			// }
			// } else if (isCodigoError(arg)) {
			// bean.setCodExito(false);
			// bean.setCodigoOperacion(getCodigoError(arg));
			// }
			EIGlobal
			.mensajePorTrace("trama Regreso#############"+ arg,
					EIGlobal.NivelLog.DEBUG);
			if (arg.indexOf(MODIFICACION_EFECTUADA) != -1) {
				bean.setCodExito(true);
			} else {
				bean.setCodExito(false);
			}

			return bean;
		}
	}
}