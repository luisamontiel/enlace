package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.Date;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;

/**
 * 
 * @author Christian Israel Castro Ram&iacute;rez
 *
 */
public class DetalleCuentasBean implements Serializable {
	private static final long serialVersionUID = -8131749702518208663L;
	private String cuenta;
	private String nombre;
	private String aPaterno;
	private String aMaterno;
	private boolean guardar;
	//Propiedades de los Archivos en pantalla
	private int folio;
    private Date fecha;
    private int registros;
    private int noRegistrados;
    private String estado;
    //Propiedades de las cuentas en pantalla
    private String cuentaLiberar;
    private String descripcion;
	private String estatus;
	private String observaciones;
	//Informacion adicional tabla ControlDetalleEmp
    private String contrato;
    private String estadoCta;
    private String observacionesCta;
    private String estadoPlan;
    private String observacionesPlan;
    //Informacion adicional tabla ControlEmp
    private String origen;
    private int secuencia;
    //Informacion adicional tabla TransacProc 
    private int numeroRegistros; 
    
	public DetalleCuentasBean() {
	}

	public DetalleCuentasBean(String cuenta, String nombre, String aPaterno,
			String aMaterno, boolean guardar) {
		this.cuenta = cuenta;
		this.nombre = nombre;
		this.aPaterno = aPaterno;
		this.aMaterno = aMaterno;
		this.guardar = guardar;
	}
	
	public DetalleCuentasBean(int folio, Date fecha, int registros,
			int noRegistrados,String estado) {
		this.folio = folio;
		this.fecha = fecha;
		this.registros = registros;
		this.noRegistrados = noRegistrados;
		this.estado = estado;
	}

	public DetalleCuentasBean(String cuentaLiberar, String descripcion,String estatus,String observaciones) {
		this.cuentaLiberar = cuentaLiberar;
		this.descripcion = descripcion;
		this.estatus = estatus;
		this.observaciones = observaciones;
	}

	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getaPaterno() {
		return aPaterno;
	}
	public void setaPaterno(String aPaterno) {
		this.aPaterno = aPaterno;
	}
	public String getaMaterno() {
		return aMaterno;
	}
	public void setaMaterno(String aMaterno) {
		this.aMaterno = aMaterno;
	}
	public boolean isGuardar() {
		return guardar;
	}
	public void setGuardar(boolean guardar) {
		this.guardar = guardar;
	}
	
	//M�todos Get y Set de propiedades de Archivos
	public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getNoRegistrados() {
        return noRegistrados;
    }

    public void setNoRegistrados(int noRegistrados) {
        this.noRegistrados = noRegistrados;
    }

    public int getRegistros() {
        return registros;
    }

    public void setRegistros(int registros) {
        this.registros = registros;
    }
    public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	//M�todos Get y Set de propiedades de cuentas
	public String getCuentaLiberar() {
		return cuentaLiberar;
	}

	public void setCuentaLiberar(String cuentaLiberar) {
		this.cuentaLiberar = cuentaLiberar;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getEstadoCta() {
		return estadoCta;
	}

	public void setEstadoCta(String estadoCta) {
		this.estadoCta = estadoCta;
	}

	public String getObservacionesCta() {
		return observacionesCta;
	}

	public void setObservacionesCta(String observacionesCta) {
		this.observacionesCta = observacionesCta;
	}

	public String getEstadoPlan() {
		return estadoPlan;
	}

	public void setEstadoPlan(String estadoPlan) {
		this.estadoPlan = estadoPlan;
	}

	public String getObservacionesPlan() {
		return observacionesPlan;
	}

	public void setObservacionesPlan(String observacionesPlan) {
		this.observacionesPlan = observacionesPlan;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public int getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(int secuencia) {
		this.secuencia = secuencia;
	}

	public int getNumeroRegistros() {
		return numeroRegistros;
	}

	public void setNumeroRegistros(int numeroRegistros) {
		this.numeroRegistros = numeroRegistros;
	}	
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 2;
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if (o == null || ! (o instanceof DetalleCuentasBean) )
			return false;
		
		DetalleCuentasBean beanCtas = (DetalleCuentasBean) o;
	
		return this.cuenta.equals(beanCtas.getCuenta());
	}
}
