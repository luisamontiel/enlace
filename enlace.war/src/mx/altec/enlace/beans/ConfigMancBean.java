package mx.altec.enlace.beans;

/**
 * Bean para almacenar los datos del mantenimiento a l�mites y Montos  
 * @author 
 *
 */
public class ConfigMancBean {

	
	private String contrato = "";
	private String usuario = "";
	private String limite = "";
	private String firma = "";
	
	
	public ConfigMancBean() {
		super();
		this.contrato = "";
		this.usuario = "";
		this.limite = "";
		this.firma = "";
	}
	
	
	public ConfigMancBean(String contrato, String usuario, String limite,
			String firma) {
		super();
		this.contrato = contrato;
		this.usuario = usuario;
		this.limite = limite;
		this.firma = firma;
	}
	
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getLimite() {
		return limite;
	}
	public void setLimite(String limite) {
		this.limite = limite;
	}
	public String getFirma() {
		return firma;
	}
	public void setFirma(String firma) {
		this.firma = firma;
	}
	
}
