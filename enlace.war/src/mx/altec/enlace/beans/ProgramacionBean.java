/**
 * Isban Mexico
 *   Clase: ProgramacionBean.java
 *   Descripcion: Implementacion de ProgramacionBean, bean de datos patre de datos
 *   de consultas programadas
 *
 *   Control de Cambios:
 *   1.0 Mar 01, 2015 asanjuan - Creacion
 *   
 **/
package mx.altec.enlace.beans;

import java.io.Serializable;

public class ProgramacionBean  implements Serializable {

	/**
	 * Serial generado
	 */
	private static final long serialVersionUID = -1258773448706227788L;

	/**id usuario**/
	private String usuario;
	/**tipo de concepto**/
	private String concepto;
	/**regerencia*/
	private String referencia;
	/**fecha de alta*/
	private String fecha;
	/**descripcion de la cuenta**/
	private String descripcionCuenta;
	/**tipo de consulta**/
	private String tipoOperacion;
	/**el importe**/
	private String importe;
	/**el Estatus**/
	private String estatus;
	
	
	/**
	 * @return el estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus el estatus a establecer
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return el usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario el usuario a establecer
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * @return el concepto
	 */
	public String getConcepto() {
		return concepto;
	}
	/**
	 * @param concepto el concepto a establecer
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	/**
	 * @return el referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia el referencia a establecer
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return el fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha el fecha a establecer
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return el descripcionCuenta
	 */
	public String getDescripcionCuenta() {
		return descripcionCuenta;
	}
	/**
	 * @param descripcionCuenta el descripcionCuenta a establecer
	 */
	public void setDescripcionCuenta(String descripcionCuenta) {
		this.descripcionCuenta = descripcionCuenta;
	}
	/**
	 * @return el tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion el tipoOperacion a establecer
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * @return el importe
	 */
	public String getImporte() {
		return importe;
	}
	/**
	 * @param importe el importe a establecer
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}
}