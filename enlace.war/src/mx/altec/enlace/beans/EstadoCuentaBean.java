/**
 * Isban Mexico
 *   Clase: EstadoCuentaBean.java
 *   Descripcion: 
 *
 *   Control de Cambios:
 *   1.0 7/04/2013 Stefanini - Creacion
 */
package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * The Class EstadoCuentaBean.
 */
public class EstadoCuentaBean implements Serializable {

	/**
	 * Serial UID Generado
	 */
	private static final long serialVersionUID = -818357368425899043L;

	/** The codigo cliente. */
	private String codigoCliente;
	
	/** The cuenta. */
	private String cuenta;
	
	/** The formato. */
	private TipoFormatoEstadoCuenta formato;
	
	/** The folio. */
	private String folio;
	
	/** The periodo. */
	private String periodo;

	/**
	 * Gets the codigo cliente.
	 *
	 * @return the codigo cliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}

	/**
	 * Sets the codigo cliente.
	 *
	 * @param codigoCliente the new codigo cliente
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	/**
	 * Gets the cuenta.
	 *
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * Sets the cuenta.
	 *
	 * @param cuenta the new cuenta
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * Gets the formato.
	 *
	 * @return the formato
	 */
	public TipoFormatoEstadoCuenta getFormato() {
		return formato;
	}

	/**
	 * Sets the formato.
	 *
	 * @param formato the new formato
	 */
	public void setFormato(TipoFormatoEstadoCuenta formato) {
		this.formato = formato;
	}

	/**
	 * Gets the folio.
	 *
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * Sets the folio.
	 *
	 * @param folio the new folio
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * Gets the periodo.
	 *
	 * @return the periodo
	 */
	public String getPeriodo() {
		return periodo;
	}

	/**
	 * Sets the periodo.
	 *
	 * @param periodo the new periodo
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
}
