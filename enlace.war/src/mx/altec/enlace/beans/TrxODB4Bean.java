package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * Clase para almacenar los datos de respuesta de la trx ODB4
 * @author FSW-Indra
 * @sice 01/03/2015
 */
public class TrxODB4Bean implements Serializable{
	/**
	 * Version de la clase
	 */
	private static final long serialVersionUID = 165676527652000L;
	/**
	 * SEC DOMICILIO
	 */
	private String secDomicilio;
	/**
	 * NUMERO DE CUENTA
	 */
	private String numCuenta;
	/**
	 * NUMERO DE TDE
	 */
	private String numeroTdc;
	/**
	 * DESCRPICION COMERCIAL TDC
	 */
	private String descComercialTdc;
	/**
	 * CALLE Y NUMERO
	 */
	private String calleYNumero;
	/**
	 * DESCRIPCION DE LA LOCALIDAD
	 */
	private String descLocalidad;
	/**
	 * DESCRIPCION DE LA COMUNIDAD
	 */
	private String descComunidad;
	/**
	 * CODIGO POSTAL
	 */
	private String codigoPostal;
	/**
	 * INDICADOR DE LA CONSULTA
	 */
	private String indConsulta;
	/**
	 * CODIGO DE BLOQUEO DE TDC
	 */
	private String codigoBloqueoTdc;
	
	/**
	 * Metodo que obtiene el sec domicilio
	 * @author FSW-Indra
	 * @return String secDomicilio
	 */
	public String getSecDomicilio() {
		return secDomicilio;
	}

	/**
	 * Metodo que asigna el sec Domicilio
	 * @author FSW-Indra
	 * @param secDomicilio a asignar
	 */
	public void setSecDomicilio(String secDomicilio) {
		this.secDomicilio = secDomicilio;
	}

	/**
	 * Metodo que obtiene el numero de cuenta
	 * @author FSW-Indra
	 * @return String numCuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}

	/**
	 * Metodo que asigna el numero de cuenta
	 * @author FSW-Indra
	 * @param numCuenta a asignar
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	/**
	 * Metodo que obtiene el numero de tdc
	 * @author FSW-Indra
	 * @return String numeroTdc
	 */
	public String getNumeroTdc() {
		return numeroTdc;
	}

	/**
	 * Metodo que asigna el numero de tdc
	 * @author FSW-Indra
	 * @param numeroTdc a asignar
	 */
	public void setNumeroTdc(String numeroTdc) {
		this.numeroTdc = numeroTdc;
	}

	/**
	 * Metodo que obtiene la descripcion comercial tdc
	 * @author FSW-Indra
	 * @return String descComercialTdc
	 */
	public String getDescComercialTdc() {
		return descComercialTdc;
	}

	/**
	 * Metodo que obtiene la descripcion comercial tdc
	 * @author FSW-Indra
	 * @param descComercialTdc a asignar
	 */
	public void setDescComercialTdc(String descComercialTdc) {
		this.descComercialTdc = descComercialTdc;
	}

	/**
	 * Metodo que obtiene la calle y el numero
	 * @author FSW-Indra
	 * @return String calleYNumero
	 */
	public String getCalleYNumero() {
		return calleYNumero;
	}

	/**
	 * Metodo que asigna la calle y el numero
	 * @author FSW-Indra
	 * @param calleYNumero a asignar
	 */
	public void setCalleYNumero(String calleYNumero) {
		this.calleYNumero = calleYNumero;
	}

	/**
	 * Metodo que obtiene la descripcion de la localidad
	 * @author FSW-Indra
	 * @return String descLocalidad
	 */
	public String getDescLocalidad() {
		return descLocalidad;
	}

	/**
	 * Metodo que asigna la descripcion de la licalidad
	 * @author FSW-Indra
	 * @param descLocalidad a asignar
	 */
	public void setDescLocalidad(String descLocalidad) {
		this.descLocalidad = descLocalidad;
	}

	/**
	 * Metodo que obtiene la descripcion de la comunidad
	 * @author FSW-Indra
	 * @return String descComunidad
	 */
	public String getDescComunidad() {
		return descComunidad;
	}

	/**
	 * Metodo que asigna la descripcion de la comunidad
	 * @author FSW-Indra
	 * @param descComunidad a asignar
	 */
	public void setDescComunidad(String descComunidad) {
		this.descComunidad = descComunidad;
	}

	/**
	 * Metodo que obtiene el codigo postal
	 * @author FSW-Indra
	 * @return String codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}

	/**
	 * Metodo que asigna el codigo postal
	 * @author FSW-Indra
	 * @param codigoPostal a asignar
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 * Metodo que obtiene el ind consulta
	 * @author FSW-Indra
	 * @return String indConsulta
	 */
	public String getIndConsulta() {
		return indConsulta;
	}

	/**
	 * Metodo que obtiene el ind consulta
	 * @author FSW-Indra
	 * @param indConsulta a asignar
	 */
	public void setIndConsulta(String indConsulta) {
		this.indConsulta = indConsulta;
	}

	/**
	 * Metodo que obtiene el codigo bloqueo tdc
	 * @author FSW-Indra
	 * @return String codigoBloqueoTdc
	 */
	public String getCodigoBloqueoTdc() {
		return codigoBloqueoTdc;
	}

	/**
	 * Metodo que asigna el codigo de bloqueo tdc
	 * @author FSW-Indra
	 * @param codigoBloqueoTdc a asignar
	 */
	public void setCodigoBloqueoTdc(String codigoBloqueoTdc) {
		this.codigoBloqueoTdc = codigoBloqueoTdc;
	}
}
