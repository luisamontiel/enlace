package mx.altec.enlace.beans;

import java.io.Serializable;

public class NomPreRemesa implements Serializable {

	private static final long serialVersionUID = 3993087136628975398L;

	public static int LNG_NO_REMESA = 20; 
		
	public static int LNG_COD_ESTADO = 1;
	
	public static int LNG_DESCRIPCION = 20;
	
	private String noRemesa;

	private String estadoRemesa;

	private String descEstadoRemesa;
	
	private int totalTarjetas;
			
	public NomPreRemesa() {
	}

	public NomPreRemesa(String noRemesa, String estadoRemesa, String descEstadoRemesa, int totalTarjetas) {
		this.noRemesa = noRemesa;
		this.estadoRemesa = estadoRemesa;
		this.descEstadoRemesa = descEstadoRemesa;
		this.totalTarjetas = totalTarjetas;
	}

	public String getDescEstadoRemesa() {
		return descEstadoRemesa;
	}

	public void setDescEstadoRemesa(String descEstadoRemesa) {
		this.descEstadoRemesa = descEstadoRemesa;
	}

	public String getEstadoRemesa() {
		return estadoRemesa;
	}

	public void setEstadoRemesa(String estadoRemesa) {
		this.estadoRemesa = estadoRemesa;
	}

	public String getNoRemesa() {
		return noRemesa;
	}

	public void setNoRemesa(String noRemesa) {
		this.noRemesa = noRemesa;
	}

	public int getTotalTarjetas() {
		return totalTarjetas;
	}

	public void setTotalTarjetas(int totalTarjetas) {
		this.totalTarjetas = totalTarjetas;
	}
}
