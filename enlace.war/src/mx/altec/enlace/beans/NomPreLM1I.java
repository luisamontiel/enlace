package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class NomPreLM1I extends NomPreLM1Base implements Serializable {	

	private static final long serialVersionUID = 3988187929011449902L;

	public static String HEADER = "LM1I00391123451O00N2";
	
	private static final String FORMATO_DETALLE = "@DCLMLBIN1 P";
	
	private static final NomPreLM1IFactory FACTORY = new NomPreLM1IFactory();

	private Map<String, String> detalle;
	
	private String beanPaginacion;

	public Map<String, String> getDetalle() {
		return detalle;
	}

	public void setDetalle(Map<String, String> detalle) {
		this.detalle = detalle;
	}		
	
	public String getBeanPaginacion() {
		return beanPaginacion;
	}

	public void setBeanPaginacion(String beanPaginacion) {
		this.beanPaginacion = beanPaginacion;
	}

	public static NomPreLM1IFactory getFactoryInstance() {
		return FACTORY;
	}
	
	private static class NomPreLM1IFactory implements NomPreBuilder<NomPreLM1I> {

		public NomPreLM1I build(String arg) {						
				
			NomPreLM1I bean = new NomPreLM1I();
			
			if (isCodigoExito(arg)) {								
				bean.setCodigoOperacion("LM1I0000");
				bean.setCodExito(true); 							
	
				String[] detalles = getDetalles(arg, FORMATO_DETALLE, 24);
	
				if (detalles != null && detalles.length > 0) {
										
					if (bean.getDetalle() == null) {
						
						bean.setDetalle(new LinkedHashMap<String, String>());
					}
					
					for (String detalle : detalles) {
						
						if (detalle.length() == 0) {
							continue;
						}									
	
						bean.getDetalle().put(
								getValor(detalle, 0, 18), 
								getValor(detalle, 18, 6));
					}
				}
				
			} else if (isCodigoError(arg))  {
				
				bean.setCodigoOperacion(getCodigoError(arg));
			}	

			return bean;			
		}		
	}
}
