package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;


import java.io.Serializable;

import mx.altec.enlace.utilerias.EIGlobal;

public class AdmLyMGL26 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL2600721123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCGLM261 P";
	
	private static final AdmLyMGL26Factory FACTORY = new AdmLyMGL26Factory();
	
	private LimitesOperacion limitesOperacion;
	
	public static AdmLyMGL26Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmLyMGL26Factory implements AdmUsrBuilder<AdmLyMGL26> {

		public AdmLyMGL26 build(String arg) {
			
			AdmLyMGL26 bean = new AdmLyMGL26();
			LimitesOperacion loBean = new LimitesOperacion();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL260000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_HEADER);							
				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
						loBean.setOpcion(getValor(arg, index, 2));
						EIGlobal.mensajePorTrace("==> opcion: [" + loBean.getOpcion() + "]", EIGlobal.NivelLog.INFO);
						loBean.setContrato(getValor(arg, index += 2, 11));
						EIGlobal.mensajePorTrace("==> contrato: [" + loBean.getContrato() + "]", EIGlobal.NivelLog.INFO);
						loBean.setUsuario(getValor(arg, index += 11, 8));
						EIGlobal.mensajePorTrace("==> usuario: [" + loBean.getUsuario() + "]", EIGlobal.NivelLog.INFO);
						loBean.setClaveOperacion(getValor(arg, index += 8, 2));
						EIGlobal.mensajePorTrace("==> grupo: [" + loBean.getClaveOperacion() + "]", EIGlobal.NivelLog.INFO);
						loBean.setLimiteOperacion(getValor(arg, index += 2, 17));
						EIGlobal.mensajePorTrace("==> monto: [" + loBean.getLimiteOperacion() + "]", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmLyMGL26 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setLimitesOperacion(loBean);
			return bean;
		}
		
	}

	public LimitesOperacion getLimitesOperacion() {
		return limitesOperacion;
	}

	public void setLimitesOperacion(LimitesOperacion limitesOperacion) {
		this.limitesOperacion = limitesOperacion;
	}



}
