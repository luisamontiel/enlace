package mx.altec.enlace.beans;

public class CuentasSantanderBean {
	/**
	 * numCuenta numCuenta
	 */
	private String numCuenta="";
	/**
	 * descripcion descripcion
	 */
	private String descripcion;
	/**
	 * nProduct nProduct
	 */
	private String nProduct;
	/**
	 * nSubprod nSubprod
	 */
	private String nSubprod;
	/**
	 * nPerJur nPerJur
	 */
	private String nPerJur;
	/**
	 * tipoRelacCtas
	 */
	private String tipoRelacCtas;
	/**
	 * variable para el contrato
	 */
	private String contrato;
	/**
	 * variable regIni
	 */
	private int regIni;
	/**
	 * variable regFin
	 */
	private int regFin;
	/**
	 * variable criterio
	 */
	private String criterio;
	/**
	 * variable textoBuscar
	 */
	private String textoBuscar;
	private String CVE_PROD_ENL;
	private int totalRegistro;
	private String modulo;
	private String TIPO_RELAC_CUENTAS;
	private String CLABE;
	private String USUARIO;
	private String respuestaTux;
	private String archivo;
	private String tramaSalida;
	public String getTramaSalida() {
		return tramaSalida;
	}
	public void setTramaSalida(String tramaSalida) {
		this.tramaSalida = tramaSalida;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public String getRespuestaTux() {
		return respuestaTux;
	}
	public void setRespuestaTux(String respuestaTux) {
		this.respuestaTux = respuestaTux;
	}
	public String getUSUARIO() {
		return USUARIO;
	}
	public void setUSUARIO(String usuario) {
		USUARIO = usuario;
	}
	public String getCVE_PROD_ENL() {
		return CVE_PROD_ENL;
	}
	public void setCVE_PROD_ENL(String cve_prod_enl) {
		CVE_PROD_ENL = cve_prod_enl;
	}

	/**
	 * totalRegistro totalRegistro
	 */
	public String getCLABE() {
		return CLABE;
	}
	public void setCLABE(String clabe) {
		CLABE = clabe;
	}
	public String getTIPO_RELAC_CUENTAS() {
		return TIPO_RELAC_CUENTAS;
	}
	public void setTIPO_RELAC_CUENTAS(String tipo_relac_cuentas) {
		TIPO_RELAC_CUENTAS = tipo_relac_cuentas;
	}
	public String getNUM_CUENTA_SERFIN() {
		return NUM_CUENTA_SERFIN;
	}
	public void setNUM_CUENTA_SERFIN(String num_cuenta_serfin) {
		NUM_CUENTA_SERFIN = num_cuenta_serfin;
	}

	private String NUM_CUENTA_SERFIN;
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public int getRegIni() {
		return regIni;
	}
	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}
	public int getRegFin() {
		return regFin;
	}
	public void setRegFin(int regFin) {
		this.regFin = regFin;
	}
	public String getCriterio() {
		return criterio;
	}
	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}
	public String getTextoBuscar() {
		return textoBuscar;
	}
	public void setTextoBuscar(String textoBuscar) {
		this.textoBuscar = textoBuscar;
	}
	public int getTotalRegistro() {
		return totalRegistro;
	}
	public void setTotalRegistro(int totalRegistro) {
		this.totalRegistro = totalRegistro;
	}
	/**
	 * @return valor del n�mero de cuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}

	/**
	 * @param numCuenta : valor del n�mero de cuenta
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	/**
	 * @return valor del producto
	 */
	public String getNProduct() {
		return nProduct;
	}

	/**
	 * @param product : valor del producto
	 */
	public void setNProduct(String product) {
		nProduct = product;
	}

	/**
	 * @return valor del subproducto
	 */
	public String getNSubprod() {
		return nSubprod;
	}

	/**
	 * @param subprod : valor del subproducto
	 */
	public void setNSubprod(String subprod) {
		nSubprod = subprod;
	}

	/**
	 * @return valor del perJur
	 */
	public String getNPerJur() {
		return nPerJur;
	}

	/**
	 * @param perJur : valor del perJur
	 */
	public void setNPerJur(String perJur) {
		nPerJur = perJur;
	}

	/**
	 * @return valor del tipo relaci�n de Cuentas
	 */
	public String getTipoRelacCtas() {
		return tipoRelacCtas;
	}

	/**
	 * @param tipoRelacCtas : valor del tipo relaci�n de Cuentas
	 */
	public void setTipoRelacCtas(String tipoRelacCtas) {
		this.tipoRelacCtas = tipoRelacCtas;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcionParam) {
		descripcion = descripcionParam;
	}
}
