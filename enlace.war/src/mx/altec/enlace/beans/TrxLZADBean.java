/** 
*   Isban Mexico
*   Clase: TrxLZADBean.java
*   Descripcion: clase de respuesta para almacenamiento y transporte de registros.
*
*   Control de Cambios:
*   1.0 Marzo 2016 FSW Everis 
*/
package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.List;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Bean para efectuar transacciones a 390
 * @author FSW Everis
 *
 */
public class TrxLZADBean implements Serializable {
    
    /**
     * Id generado automaticamente para la serializacion de la clase
     */
    private static final long serialVersionUID = -185222517489856956L;
    
    /** Codigo de error.*/
    private String codError;
    /** Mensaje error.*/
    private String mensajeError;
    /**Indica si hubo errores*/
    private boolean error;
    
    /** Mapa de valores de la trama. */
    private transient List<String> campos;
    

    /**
     * Coloca el nuevo valor a la lista
     * @param campos valor de lista campos
     */
    public void setCampos(List<String> campos) {
        this.campos = campos;
    }

    /**
     * Obtiene el campo 
     * @param idx indice del campo dentro de la lista
     * @return campo en formato string
     */
    public  String getCampo(int idx){
        if(campos.size() > idx && !campos.isEmpty() ){
            return campos.get(idx);
        }
        return null;
    }
    /**
     * Obtiene el campo
     * @param idx indice del campo dentro de la lista
     * @return campo con formato string sin espacios
     */
    public String getCampoTrim(int idx){
        if(campos.size()> idx && !campos.isEmpty()){
            return campos.get(idx).trim();
        }
        return null;
    }
    /**
     * Obtiene el campo y se agrega punto decimal antes de las dos ultimas posiciones
     * @param idx indice del campo dentro de la lista
     * @return campo con formato doble
     */
    public String getCampoDouble(int idx){
        if(campos.size()>idx && !campos.isEmpty()){
        return    campos.get(idx).substring(0, campos.get(idx).length()-2)+"."+campos.get(idx).substring(campos.get(idx).length()-2, campos.get(idx).length());
        }
        return null;
    }
    
    
    /** Obtiene el codigo de error.
     * @return codigo de error
     */
    public String getCodError() {
        return codError;
    }
    /** Coloca el Nuevo valor del codigo de error.
     * @param codError codigo de error
     */
    public void setCodError(String codError) {
        this.codError = codError;
    }
    /** Obtiene el mensaje de error.
     * @return mensaje de error
     */
    public String getMensajeError() {
        return mensajeError;
    }
    /** Coloca el Nuevo valor de mensaje de error.
     * @param mensajeError mensaje de error
     */
    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }
    
    /**
     * @return the error
     */
    public boolean isError() {
        return error;
    }
    /**
     * @param error the error to set
     */
    public void setError(boolean error) {
        this.error = error;
    }

}
