package mx.altec.enlace.beans;

import mx.altec.enlace.utilerias.NomPreUtil;



public class CuentasBean {
	/*
	 * "TRIM(A.NUM_CUENTA)"	"TIPO_RELAC_CTAS"	"NVL(A.N_TIPO_CUENTA,'1')"	"NVL(A.N_PER_JUR,'F')"	"NVL(A.N_PRODUCT,'')"	"N_DESCRIPCION"	"'0000000'"	"'SINTITULAR'"	"NUM_CUENTA_REL"	"TIPO_PRODUCTO"
		"20005690302"	"P"	"1"	"F"	"20      "	"NARCISO WILLIAM AGUILAR SANTOYO"	"0000000"	"SIN TITULAR"	" "	1
*/
	/**
	 * numCuenta numero de cuenta
	 */
	String numCuenta ;
	/**
	 * tipoRel Tipo de Relacion
	 */
	String tipoRel;
	/**
	 * tipoCuenta Tipo de Cuenta
	 */
	String tipoCuenta;
	/**
	 * tipoPersona Tipo de Persona 
	 */
	String tipoPersona;
	/**
	 * cveProducto Clave del Producto
	 */
	String cveProducto;
	/**
	 * descripcion Descripcion
	 */
	String descripcion;
	/**
	 * clave clave
	 */
	String clave;
	/**
	 * titular Titular
	 */
	String titular;
	/**
	 * numCuentaRel numero de cuenta relacionada
	 */
	String numCuentaRel;
	/**
	 * tipoProd Tipo de Producto
	 */
	String tipoProd;

	/**
	 * Metodo getTipoProd 
	 * @return tipoProd tipo producto
	 */
	public String getTipoProd() {
		return tipoProd;
	}
	/**
	 * Metodo setTipoProd
	 * @param tipoProd Tipo producto
	 */
	public void setTipoProd(String tipoProd) {
		this.tipoProd = tipoProd;
	}
	/**
	 * Metodo getNumCuenta
	 * @return numCuenta Numero de Cuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}
	/**
	 * Metodo setNumCuenta
	 * @param numCuenta numero de cuenta
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	/**
	 * Metodo getTipoRel
	 * @return tipoRel Tipo de Relacion
	 */
	public String getTipoRel() {
		return tipoRel;
	}
	/**
	 * Metodo setTipoRel 
	 * @param tipoRel Tipo de Relacion
	 */
	public void setTipoRel(String tipoRel) {
		this.tipoRel = tipoRel;
	}
	/**
	 * Metodo getTipoCuenta
	 * @return tipoCuenta Tipo de Cuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 * Metodo setTipoCuenta
	 * @param tipoCuenta Tipo de Cuenta
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	/**
	 * Metodo getTipoPersona
	 * @return tipoPersona Tipo de Persona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * Metodo setTipoPersona
	 * @param tipoPersona Tipo de Persona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * Metodo getCveProducto
	 * @return cveProducto clave de producto
	 */
	public String getCveProducto() {
		return cveProducto;
	}
	/**
	 * metodo setCveProducto
	 * @param cveProducto clave de producto
	 */
	public void setCveProducto(String cveProducto) {
		this.cveProducto = cveProducto;
	}
	/**
	 * metodo getDescripcion
	 * @return descripcion descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * metodo setDescripcion
	 * @param descripcion descripcion
	 */
	public void setDescripcion(String descripcion) {
		if(descripcion!=null) {
			descripcion=descripcion.trim();
		}
		this.descripcion = descripcion;
	}
	/**
	 * metodo getClave
	 * @return clave clave
	 */
	public String getClave() {
		return clave;
	}
	/**
	 * metodo setClave
	 * @param clave clave
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}
	/**
	 * metodo getTitular
	 * @return titular titular
	 */
	public String getTitular() {
		return titular;
	}
	/**
	 * metodo setTitular
	 * @param titular titular
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}
	/**
	 * Metodo getNumCuentaRel
	 * @return numCuentaRel numero de cuenta relacionada
	 */
	public String getNumCuentaRel() {
		return numCuentaRel;
	}
	/**
	 * metodo setNumCuentaRel
	 * @param numCuentaRel numero de cuenta relacionada
	 */
	public void setNumCuentaRel(String numCuentaRel) {
		this.numCuentaRel = numCuentaRel;
	}
	/**
	 * Metodo generado para crear cadena
	 * @return cuenta cuenta
	 */
	public String toString() {
		StringBuffer cuenta = null;
		cuenta = new StringBuffer();
		cuenta.append("2;");
		cuenta.append(numCuenta);
		cuenta.append(";");
		cuenta.append(descripcion);
		cuenta.append(";");
		cuenta.append(tipoRel);
		cuenta.append(";");
		cuenta.append(tipoProd);
		cuenta.append(";");
		cuenta.append("                   ");
		cuenta.append(";");
		cuenta.append(NomPreUtil.rellenar(titular,40));
		cuenta.append(";");
		cuenta.append(tipoPersona);
		cuenta.append(";");
		return cuenta.toString();
	}
}
