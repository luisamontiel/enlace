package mx.altec.enlace.beans;

import java.io.Serializable;
/**
 *
 * @author jaferruzca
 * Bean por el que viajara el inicio y fin de la paginacion
 */
public class PaginacionBean implements  Serializable{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * codError
	 */
	String codError="";
	/**
	 * msgError
	 */
	String msgError="";
	/**
	 * inicio
	 */
	private Integer inicio ;
	/**
	 * fin
	 */
	private Integer fin ;
	/**
	 * pagina
	 */
	private Integer pagina;
	/**
	 * paginasTotales
	 */
	private Integer paginasTotales;
	/**
	 * totalRegs
	 */
	private Integer totalRegs;
	/**
	 *
	 * @return totalRegs obtener total de registros
	 */
	public Integer getTotalRegs() {
		return totalRegs;
	}
	/**
	 *
	 * @param totalRegs asignar total de registros
	 */
	public void setTotalRegs(Integer totalRegs) {
		this.totalRegs = totalRegs;
	}
	/**
	 *
	 * @return paginasTotales obtener total de paginas
	 */
	public Integer getPaginasTotales() {
		return paginasTotales;
	}
	/**
	 *
	 * @param paginasTotales asignar total de paginas
	 */
	public void setPaginasTotales(Integer paginasTotales) {
		this.paginasTotales = paginasTotales;
	}
	/**
	 *
	 * @return inicio obtener filtro inicio
	 */
	public Integer getInicio() {
		return inicio;
	}
	/**
	 *
	 * @param inicio asignar filtro inicio
	 */
	public void setInicio(Integer inicio) {
		this.inicio = inicio;
	}
	/**
	 *
	 * @return fin obtener el fin
	 */
	public Integer getFin() {
		return fin;
	}
	/**
	 *
	 * @param fin asignar el fin
	 */
	public void setFin(Integer fin) {
		this.fin = fin;
	}
	/**
	 *
	 * @return pagina obtener la pagina
	 */
	public Integer getPagina() {
		return pagina;
	}
	/**
	 *
	 * @param pagina asignar la pagina
	 */
	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}
	/**
	 *
	 * @return codError obtener el codigo de error
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *
	 * @param codError asignar el codigo de error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 *
	 * @return msgError obtener el mensaje de error
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 *
	 * @param msgError asignar el mensaje de error
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}


}
