package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminusr.shared.GrupoPerfiles;



import java.io.Serializable;
import java.util.ArrayList;

import mx.altec.enlace.utilerias.EIGlobal;

public class AdmUsrGL35 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL3500341123451O00N2";
								// ORM401431123451O00N2
					
	private static final String FORMATO_HEADER = "@DCGLM351  P";
	private static final String FORMATO_HEADER_DET = "@DCGLM352  P";
	
	private static final AdmUsrGL35Factory FACTORY = new AdmUsrGL35Factory();
		
	private ArrayList<GrupoPerfiles> grupos;
	
	public static AdmUsrGL35Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmUsrGL35Factory implements AdmUsrBuilder<AdmUsrGL35> {

		public AdmUsrGL35 build(String arg) {
			
			AdmUsrGL35 bean = new AdmUsrGL35();
			ArrayList<GrupoPerfiles> grpList = new ArrayList<GrupoPerfiles>();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL350000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);							
				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						int numRegs = Integer.parseInt(getValor(arg, index , 3));
						if (numRegs > 0) {
							index = arg.indexOf(FORMATO_HEADER_DET);
							if (index != -1) {
								EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.ERROR);
								for (int i = 0; i < numRegs; i ++) { // lee el detalle
									index += FORMATO_HEADER_DET.length();
									EIGlobal.mensajePorTrace("==> index3: [" + index + "]", EIGlobal.NivelLog.ERROR);
									GrupoPerfiles gpBean = new GrupoPerfiles();
									gpBean.setCveGrupo(getValor(arg, index, 2));
									EIGlobal.mensajePorTrace("==> Grupo: [" + gpBean.getCveGrupo() + "]", EIGlobal.NivelLog.ERROR);
									gpBean.setDescripcion(getValor(arg, index += 2, 40));
									EIGlobal.mensajePorTrace("==> Desc: [" + gpBean.getDescripcion() + "]", EIGlobal.NivelLog.ERROR);
									gpBean.setEstatus(getValor(arg, index+= 40 , 2));
									EIGlobal.mensajePorTrace("==> Estatus: [" + gpBean.getEstatus() + "]", EIGlobal.NivelLog.ERROR);
									gpBean.setDescEstatus(getValor(arg, index += 2, 10));
									EIGlobal.mensajePorTrace("==> DescEstatus: [" + gpBean.getDescEstatus() + "]", EIGlobal.NivelLog.ERROR);
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.ERROR);
									index += 11;
									grpList.add(gpBean);
								}
							}
							
						}
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmUsrGL35 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setGrupos(grpList);
			return bean;
		}
		
	}


	public ArrayList<GrupoPerfiles> getGrupos() {
		return grupos;
	}

	public void setGrupos(ArrayList<GrupoPerfiles> grupos) {
		this.grupos = grupos;
	}
}
