package mx.altec.enlace.beans;

import static mx.altec.enlace.beans.NomPreEmpleado.LNG_NO_EMPLEADO;
import static mx.altec.enlace.beans.NomPreTarjeta.LNG_NO_TARJETA;
import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;

public class NomPreLM1H extends NomPreLM1Base implements Serializable {

	private static final long serialVersionUID = -7749463636596861741L;

	public static String HEADER = "LM1H00941123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCLMECREAS P";
	
	private static final NomPreLM1HFactory FACTORY = new NomPreLM1HFactory();
	
	private String codigoEntrada;	//CODENT
	private String cuentaAlta;		//CENTALTA
	private String cuenta;			//CUENTA
	private String noEmpleado;		//COD-EMPLE
	private String tarjetaAnterior;	//TARJETA-ANT
	private String tarjetaActual;	//TARJETA-ACT
	
	public String getCodigoEntrada() {
		return codigoEntrada;
	}

	public void setCodigoEntrada(String codigoEntrada) {
		this.codigoEntrada = codigoEntrada;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getNoEmpleado() {
		return noEmpleado;
	}

	public void setNoEmpleado(String noEmpleado) {
		this.noEmpleado = noEmpleado;
	}

	public String getTarjetaActual() {
		return tarjetaActual;
	}

	public void setTarjetaActual(String tarjetaActual) {
		this.tarjetaActual = tarjetaActual;
	}

	public String getTarjetaAnterior() {
		return tarjetaAnterior;
	}

	public void setTarjetaAnterior(String tarjetaAnterior) {
		this.tarjetaAnterior = tarjetaAnterior;
	}

	public String getCuentaAlta() {
		return cuentaAlta;
	}

	public void setCuentaAlta(String cuentaAlta) {
		this.cuentaAlta = cuentaAlta;
	}

	public static NomPreLM1HFactory getFactoryInstance() {		
		return FACTORY;
	}	
	
	private static class NomPreLM1HFactory implements NomPreBuilder<NomPreLM1H> {

		public NomPreLM1H build(String arg) {
						
			NomPreLM1H bean = new NomPreLM1H();;
			
			if (isCodigoExito(arg)) {
				bean.setCodigoOperacion("LM1H0000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);
				
				if (index != -1) {
					index += FORMATO_HEADER.length();
					bean.setCodigoEntrada(getValor(arg, index, 4));
					bean.setCuentaAlta(getValor(arg, 4, 4));
					bean.setCuenta(getValor(arg, 8, 12));
					bean.setNoEmpleado(getValor(arg, 20, LNG_NO_EMPLEADO));
					bean.setTarjetaAnterior(getValor(arg, 27, LNG_NO_TARJETA));
					bean.setTarjetaActual(getValor(arg, 49, LNG_NO_TARJETA));
				}
			} else if (isCodigoError(arg))  {
				
				bean.setCodigoOperacion(getCodigoError(arg));
			}	
	
			return bean;										
		}	
	}
}
