package mx.altec.enlace.beans;

import java.util.Date;
import java.util.List;

/**
 * @author admartinezz
 *
 */
public class ConfEdosCtaTcIndBean {
	/**
	 * Tarjeta
	 */
	private String tarjeta;
	/**
	 * Lista Tarjetas
	 */
	private List<String> tarjetas;
	/**
	 * Codigo de cliente
	 */
	private String codCliente;
	/**
	 * Codigo de cliente de personas
	 */
	private String codClientePersonas;
	/**
	 * Indicador de paperless
	 */
	private String idPaperless;
	/**
	 * Nombre Completo
	 */
	private String nombreComp;
	/**
	 * Nombre
	 */
	private String nombre;
	/**
	 * Apellido paterno
	 */
	private String apPaterno;
	/**
	 * Apellido materno
	 */
	private String apMaterno;
	/**
	 * Tipo persona
	 */
	private String tipoPersona;
	/**
	 * Estado de cta disponible
	 */
	private boolean edoCtaDisponible;
	/**
	 * Suscripcion a paperless
	 */
	private boolean suscripPaperless;
	/**
	 * Oficina
	 */
	private String oficina;
	/**
	 * Secuencia de domicilio
	 */
	private String secDomicilio;
	/**
	 * Calle
	 */
	private String calle;
	/**
	 * Numero exterior
	 */
	private String noExterior;
	/**
	 * Numero interior
	 */
	private String noInterior;
	/**
	 * Colonia
	 */
	private String colonia;
	/**
	 * Ciudad
	 */
	private String ciudad;
	/**
	 * Delegacion / Municipio
	 */
	private String delegMunic;
	/**
	 * Codigo postal
	 */
	private String codPostal;
	/**
	 * Estado
	 */
	private String estado;
	/**
	 * Domicilio completo
	 */
	private String domCompleto;
	/**
	 * Codigo de retorno
	 */
	private String codRetorno;
	/**
	 * Descripcion de retorno
	 */
	private String descRetorno;
	/**
	 * Folio de operacion
	 */
	private String folioOp;
	/**
	 * Contrato
	 */
	private String contrato;
	/**
	 * Fecha de configuracion
	 */
	private Date fechaConfig;
	/**
	 * Periodo disponible
	 */
	private String periodoDisp;
	/**
	 * Folio ondemand
	 */
	private String folioOnD;
	/**
	 * Folio UUID
	 */
	private String folioUUID;
	/**
	 * Descripcion de la cuenta
	 */
	private String descCuenta;
	/**
	 * Codigo de entidad
	 */
	private String codent;
	/**
	 * Centro alta cta. tar.
	 */
	private String mensajeError;
	
	private String centalt;
	/** Bandera que indica si la transaccion respondio con error de ambientacion de datos. */
	private boolean tieneProblemaDatos;

	/** Atributo privado para almacenar el mensaje asociado cuando la transaccion respondio con error de ambientacion de datos.*/
	private String mensajeProblemaDatos = "";
	
	/**
	 * getTarjeta Tarjeta
	 * @return
	 */
	public String getMensajeError() {
		return mensajeError;
	}
	/**
	 * setTarjeta Tarjeta
	 * @param tarjeta
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	/**
	 * getTarjeta Tarjeta
	 * @return
	 */
	public String getTarjeta() {
		return tarjeta;
	}
	/**
	 * setTarjeta Tarjeta
	 * @param tarjeta
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	/**
	 * getTarjetas Lista Tarjetas
	 * @return
	 */
	public List<String> getTarjetas() {
		return tarjetas;
	}
	/**
	 * setTarjetas Lista Tarjetas
	 * @return
	 */
	public void setTarjetas(List<String> tarjetas) {
		this.tarjetas = tarjetas;
	}
	/**
	 * getCodCliente codigo cliente
	 * @return
	 */
	public String getCodCliente() {
		return codCliente;
	}
	/**
	 * setCodCliente codigo cliente
	 * @return
	 */
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}
	/**
	 * getCodClientePersonas codigo cliente personas
	 * @return
	 */
	public String getCodClientePersonas() {
		return codClientePersonas;
	}
	/**
	 * setCodClientePersonas codigo cliente personas
	 * @param codClientePersonas
	 */
	public void setCodClientePersonas(String codClientePersonas) {
		this.codClientePersonas = codClientePersonas;
	}
	/**
	 * getIdPaperless id paperless
	 * @return
	 */
	public String getIdPaperless() {
		return idPaperless;
	}
	/**
	 * setIdPaperless id paperless
	 * @param idPaperless
	 */
	public void setIdPaperless(String idPaperless) {
		this.idPaperless = idPaperless;
	}
	/**
	 * getNombreComp Nombre comp
	 * @return
	 */
	public String getNombreComp() {
		return nombreComp;
	}
	/**
	 * setNombreComp Nombre comp
	 * @param nombreComp
	 */
	public void setNombreComp(String nombreComp) {
		this.nombreComp = nombreComp;
	}
	/**
	 * getNombre Nombre
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * setNombre Nombre
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * getApPaterno Apellido Paterno
	 * @return
	 */
	public String getApPaterno() {
		return apPaterno;
	}
	/**
	 * setApPaterno Apellido Paterno
	 * @param apPaterno
	 */
	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}
	/**
	 * getApMaterno Apellido Materno
	 * @return
	 */
	public String getApMaterno() {
		return apMaterno;
	}
	/**
	 * setApMaterno Apellido Materno
	 * @param apMaterno
	 */
	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}
	/**
	 * getTipoPersona Tipo Persona
	 * @return
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * setTipoPersona Tipo Persona
	 * @param tipoPersona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * isEdoCtaDisponible Estado de Cuenta Disponible
	 * @return
	 */
	public boolean isEdoCtaDisponible() {
		return edoCtaDisponible;
	}
	/**
	 * setEdoCtaDisponible Estado de Cuenta Disponible
	 * @param edoCtaDisponible
	 */
	public void setEdoCtaDisponible(boolean edoCtaDisponible) {
		this.edoCtaDisponible = edoCtaDisponible;
	}
	/**
	 * isSuscripPaperless Suscripcion a Paperless
	 * @return
	 */
	public boolean isSuscripPaperless() {
		return suscripPaperless;
	}
	/**
	 * setSuscripPaperless Suscripcion a Paperless
	 * @param suscripPaperless
	 */
	public void setSuscripPaperless(boolean suscripPaperless) {
		this.suscripPaperless = suscripPaperless;
	}
	/**
	 * getOficina Oficina
	 * @return
	 */
	public String getOficina() {
		return oficina;
	}
	/**
	 * setOficina Oficina
	 * @param oficina
	 */
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	/**
	 * getSecDomicilio Domicilio
	 * @return
	 */
	public String getSecDomicilio() {
		return secDomicilio;
	}
	/**
	 * setSecDomicilio Domicilio
	 * @param secDomicilio
	 */
	public void setSecDomicilio(String secDomicilio) {
		this.secDomicilio = secDomicilio;
	}
	/**
	 * getCalle Calle
	 * @return
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * setCalle Calle
	 * @param calle
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * getNoExterior Numero Exterior
	 * @return
	 */
	public String getNoExterior() {
		return noExterior;
	}
	/**
	 * setNoExterior Numero Exterior
	 * @param noExterior
	 */
	public void setNoExterior(String noExterior) {
		this.noExterior = noExterior;
	}
	/**
	 * getNoInterior Numero Interior
	 * @return
	 */
	public String getNoInterior() {
		return noInterior;
	}
	/**
	 * setNoInterior Numero Interior
	 * @param noInterior
	 */
	public void setNoInterior(String noInterior) {
		this.noInterior = noInterior;
	}
	/**
	 * getColonia Colonia
	 * @return
	 */
	public String getColonia() {
		return colonia;
	}
	/**
	 * setColonia Colonia
	 * @param colonia
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	/**
	 * getCiudad Ciudad
	 * @return
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * setCiudad Ciudad
	 * @param ciudad
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * getDelegMunic Delegacion Municipal
	 * @return
	 */
	public String getDelegMunic() {
		return delegMunic;
	}
	/**
	 * setDelegMunic Delegacion Municipal
	 * @param delegMunic
	 */
	public void setDelegMunic(String delegMunic) {
		this.delegMunic = delegMunic;
	}
	/**
	 * getCodPostal Codigo Postal
	 * @return
	 */
	public String getCodPostal() {
		return codPostal;
	}
	/**
	 * setCodPostal Codigo Postal
	 * @param codPostal
	 */
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}
	/**
	 * getEstado Estado
	 * @return
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * setEstado Estado
	 * @param estado
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * getDomCompleto Domicilio Completo
	 * @return
	 */
	public String getDomCompleto() {
		return domCompleto;
	}
	/**
	 * setDomCompleto Domicilio Completo
	 * @param domCompleto
	 */
	public void setDomCompleto(String domCompleto) {
		this.domCompleto = domCompleto;
	}
	/**
	 * getCodRetorno Codigo Retorno
	 * @return
	 */
	public String getCodRetorno() {
		return codRetorno;
	}
	/**
	 * setCodRetorno Codigo Retorno
	 * @param codRetorno
	 */
	public void setCodRetorno(String codRetorno) {
		this.codRetorno = codRetorno;
	}
	/**
	 * getDescRetorno Descripcion Retorno
	 * @return
	 */
	public String getDescRetorno() {
		return descRetorno;
	}
	/**
	 * setDescRetorno Descripcion Retorno
	 * @param descRetorno
	 */
	public void setDescRetorno(String descRetorno) {
		this.descRetorno = descRetorno;
	}
	/**
	 * getFolioOp Folio Op
	 * @return
	 */
	public String getFolioOp() {
		return folioOp;
	}
	/**
	 * setFolioOp Folio Op
	 * @param folioOp
	 */
	public void setFolioOp(String folioOp) {
		this.folioOp = folioOp;
	}
	/**
	 * getContrato Contrato
	 * @return
	 */
	public String getContrato() {
		return contrato;
	}
	/**
	 * setContrato Contrato
	 * @param contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * getFechaConfig Fecha Config
	 * @return
	 */
	public Date getFechaConfig() {
		return fechaConfig;
	}
	/**
	 * setFechaConfig Fecha Config
	 * @param fechaConfig
	 */
	public void setFechaConfig(Date fechaConfig) {
		this.fechaConfig = fechaConfig;
	}
	/**
	 * getPeriodoDisp Periodo Disponible
	 * @return
	 */
	public String getPeriodoDisp() {
		return periodoDisp;
	}
	/**
	 * setPeriodoDisp Periodo Disponible
	 * @param periodoDisp
	 */
	public void setPeriodoDisp(String periodoDisp) {
		this.periodoDisp = periodoDisp;
	}
	/**
	 * getFolioOnD Folio On Demand
	 * @return
	 */
	public String getFolioOnD() {
		return folioOnD;
	}
	/**
	 * setFolioOnD Folio On Demand
	 * @param folioOnD
	 */
	public void setFolioOnD(String folioOnD) {
		this.folioOnD = folioOnD;
	}
	/**
	 * getFolioUUID Folio UUID
	 * @return
	 */
	public String getFolioUUID() {
		return folioUUID;
	}
	/**
	 * setFolioUUID Folio UUID
	 * @param folioUUID
	 */
	public void setFolioUUID(String folioUUID) {
		this.folioUUID = folioUUID;
	}
	/**
	 * getDescCuenta Descripcion Cuenta
	 * @return
	 */
	public String getDescCuenta() {
		return descCuenta;
	}
	/**
	 * setDescCuenta Descripcion Cuenta
	 * @param descCuenta
	 */
	public void setDescCuenta(String descCuenta) {
		this.descCuenta = descCuenta;
	}
	/**
	 * getCodent Codigo Entidad
	 * @return
	 */
	public String getCodent() {
		return codent;
	}
	/**
	 * setCodent Codigo Entidad
	 * @param codent
	 */
	public void setCodent(String codent) {
		this.codent = codent;
	}
	/**
	 * getCentalt Centro Alta
	 * @return
	 */
	public String getCentalt() {
		return centalt;
	}
	/**
	 * setCentalt Centro Alta
	 * @param centalt
	 */
	public void setCentalt(String centalt) {
		this.centalt = centalt;
	}
	/**
	 * isTieneProblemaDatos Tiene Problema de Datos
	 * @return
	 */
	public boolean isTieneProblemaDatos() {
		return tieneProblemaDatos;
	}
	/**
	 * setTieneProblemaDatos Tiene Problema de Datos
	 * @param tieneProblemaDatos
	 */
	public void setTieneProblemaDatos(boolean tieneProblemaDatos) {
		this.tieneProblemaDatos = tieneProblemaDatos;
	}
	/**
	 * getMensajeProblemaDatos Mensaje Problema de Datos
	 * @return
	 */
	public String getMensajeProblemaDatos() {
		return mensajeProblemaDatos;
	}
	/**
	 * setMensajeProblemaDatos Mensaje Problema de Datos
	 * @param mensajeProblemaDatos
	 */
	public void setMensajeProblemaDatos(String mensajeProblemaDatos) {
		this.mensajeProblemaDatos = mensajeProblemaDatos;
	}

}
