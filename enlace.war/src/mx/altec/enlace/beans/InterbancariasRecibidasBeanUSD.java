package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean de datos para Consulta de Interbancarias Recibidas en USD
 * @author FSW TCS 
 *
 */
public class InterbancariasRecibidasBeanUSD implements Serializable {

	/**
	 * Serial de version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * codError codigo de error
	 */
	private String codError;
	
	/**
	 * fchTrasnfer fecha transferencia
	 */
	private String fchTrasnfer;
	
	/**
	 * referencia de operacion
	 */
	private String referencia;
	
	/**
	 * cuenta del cliente
	 */
	private String cuenta;
	
	/**
	 * desc error 
	 */
	private String descError;
	
	/**
	 * Cve mecanismo
	 */
	private String cveMecanismo;

	/**
	 * Institucion ordenante
	 */
	private String instOrdenante;
	
	/**
	 * numero banxico ordenante
	 */
	private String numBanxOrdenante;
	
	/**
	 * nombre banxico ordenante
	 */
	private String nomBanxLargOrdenante;
	
	/**
	 * fechaHrConfirmacion fecha y hora de confirmacion
	 */
	private String fechaHrConfirmacion;

	/**
	 * URL que contiene los parametros para enviar a banxico
	 */
	private String url;
	/**
	 * lstInterbancarias lista de interbancarias recibidas
	 */
	private List<InterbancariasRecibidasBeanUSD> lstInterbancarias=new ArrayList<InterbancariasRecibidasBeanUSD>();

	/**
	 * Institucion del beneficiario
	*/
	private String instBenef;

	/**
	 * NÃºmero Banxico de la instituciÃ³n beneficiaria 
	*/
	private String numBanxBenef;

	/**
	 * Nombre largo de la instituciÃ³n beneficiaria
	*/
	private String nombLargInstBenef;

	/**
	 *  estatus
	*/
	private String estatus;
	
	/**
	 * numReferencia numero de referencia
	 */
	private String numReferencia;
	
	/**
	 * DescripciÃ³n de estatus
	*/
	private String descEstatus;

	/**
	 * Clave del motivo de devoluciÃ³n
	*/
	private String cveMotivDev;

	/**
	 * DescripciÃ³n del motivo de devoluciÃ³n
	*/
	private String descMotivDev;

	/**
	 * Fecha de ConstituciÃ³n Ordenante 
	*/
	private String fchConstOrdenante;

	/**
	 * RFC del Ordenante
	*/
	private String rfcOrdenante;

	/**
	 * RFC del Receptor
	*/
	private String rfcReceptor;

	/**
	 * Nombre del Ordenante
	*/
	private String nomOrdenante;

	/**
	 * Nombre del Receptor
	*/
	private String nomReceptor;

	/**
	 * Referencia del pago
	*/
	private String refPago;

	/**
	 * NÃºmero de cuenta Ordenante
	*/
	private String numCtaOrdenante;

	/**
	 * Tipo de cuenta Ordenante
	*/
	private String tipoCtaOrdenante;

	/**
	 * NÃºmero de cuenta Receptora
	*/
	private String numCtaReceptor;

	/**
	 * Tipo de cuenta Receptora
	*/
	private String tipoCtaReceptor;

	/**
	 * Clave de Rastreo
	*/
	private String cveRastreo;

	/**
	 * Referencia Transfer
	*/
	private String refTransfer;

	/**
	 * Concepto Pago
	*/
	private String conceptoPagoTransfer;

	/**
	 * DirecciÃ³n Ordenante
	*/
	private String direccOrdenante;

	/**
	 * DirecciÃ³n Beneficiaria
	*/
	private String direccionBenef;

	/**
	 * Importe Cargo 
	*/
	private String impCargo;

	/**
	 * Importe Abono
	*/
	private String impAbono;

	/**
	 * iva
	*/
	private String ivaMonto;

	/**
	 * Tipo de cambio
	*/
	private String tipoCambio;

	/**
	 * Importe Divisa 
	*/
	private String impDivisa;

	/**
	 * Divisa Ordenante
	*/
	private String divisaOrdenante;

	/**
	 * Divisa benef
	*/
	private String divisaBenef;

	/**
	 * Leyenda Ordenante.
	*/
	private String leyendaOrde;

	/**
	 * Leyenda benef.
	*/
	private String leyendaBenef;
	
	/**
	 * Leyenda bancoBN.
	*/
	private String bancoBN;
	
	
	
	/**
	 ********* Getters y Setters
	 */
	
	/**
	 * Obtener fecha hora de confirmacion
	 * @return fechaHrConfirmacion fecha y hora de confirmacion
	 */
	public String getFechaHrConfirmacion() {
		return fechaHrConfirmacion;
	}
	/**
	 * @return the fchTrasnfer
	 */
	public String getFchTrasnfer() {
		return fchTrasnfer;
	}
	/**
	 * @param fchTrasnfer the fchTrasnfer to set
	 */
	public void setFchTrasnfer(String fchTrasnfer) {
		this.fchTrasnfer = fchTrasnfer;
	}
	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * @return the descError
	 */
	public String getDescError() {
		return descError;
	}
	/**
	 * @param descError the descError to set
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}
	/**
	 * @return the cveMecanismo
	 */
	public String getCveMecanismo() {
		return cveMecanismo;
	}
	/**
	 * @param cveMecanismo the cveMecanismo to set
	 */
	public void setCveMecanismo(String cveMecanismo) {
		this.cveMecanismo = cveMecanismo;
	}
	/**
	 * @return the instOrdenante
	 */
	public String getInstOrdenante() {
		return instOrdenante;
	}
	/**
	 * @param instOrdenante the instOrdenante to set
	 */
	public void setInstOrdenante(String instOrdenante) {
		this.instOrdenante = instOrdenante;
	}
	/**
	 * @return the numBanxOrdenante
	 */
	public String getNumBanxOrdenante() {
		return numBanxOrdenante;
	}
	/**
	 * @param numBanxOrdenante the numBanxOrdenante to set
	 */
	public void setNumBanxOrdenante(String numBanxOrdenante) {
		this.numBanxOrdenante = numBanxOrdenante;
	}
	/**
	 * @return the nomBanxLargOrdenante
	 */
	public String getNomBanxLargOrdenante() {
		return nomBanxLargOrdenante;
	}
	/**
	 * @param nomBanxLargOrdenante the nomBanxLargOrdenante to set
	 */
	public void setNomBanxLargOrdenante(String nomBanxLargOrdenante) {
		this.nomBanxLargOrdenante = nomBanxLargOrdenante;
	}
	/**
	 * @return the instBenef
	 */
	public String getInstBenef() {
		return instBenef;
	}
	/**
	 * @param instBenef the instBenef to set
	 */
	public void setInstBenef(String instBenef) {
		this.instBenef = instBenef;
	}
	/**
	 * @return the numBanxBenef
	 */
	public String getNumBanxBenef() {
		return numBanxBenef;
	}
	/**
	 * @param numBanxBenef the numBanxBenef to set
	 */
	public void setNumBanxBenef(String numBanxBenef) {
		this.numBanxBenef = numBanxBenef;
	}
	/**
	 * @return the nombLargInstBenef
	 */
	public String getNombLargInstBenef() {
		return nombLargInstBenef;
	}
	/**
	 * @param nombLargInstBenef the nombLargInstBenef to set
	 */
	public void setNombLargInstBenef(String nombLargInstBenef) {
		this.nombLargInstBenef = nombLargInstBenef;
	}
	/**
	 * @return the descEstatus
	 */
	public String getDescEstatus() {
		return descEstatus;
	}
	/**
	 * @param descEstatus the descEstatus to set
	 */
	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}
	/**
	 * @return the cveMotivDev
	 */
	public String getCveMotivDev() {
		return cveMotivDev;
	}
	/**
	 * @param cveMotivDev the cveMotivDev to set
	 */
	public void setCveMotivDev(String cveMotivDev) {
		this.cveMotivDev = cveMotivDev;
	}
	/**
	 * @return the descMotivDev
	 */
	public String getDescMotivDev() {
		return descMotivDev;
	}
	/**
	 * @param descMotivDev the descMotivDev to set
	 */
	public void setDescMotivDev(String descMotivDev) {
		this.descMotivDev = descMotivDev;
	}
	/**
	 * @return the fchConstOrdenante
	 */
	public String getFchConstOrdenante() {
		return fchConstOrdenante;
	}
	/**
	 * @param fchConstOrdenante the fchConstOrdenante to set
	 */
	public void setFchConstOrdenante(String fchConstOrdenante) {
		this.fchConstOrdenante = fchConstOrdenante;
	}
	/**
	 * @return the rfcOrdenante
	 */
	public String getRfcOrdenante() {
		return rfcOrdenante;
	}
	/**
	 * @param rfcOrdenante the rfcOrdenante to set
	 */
	public void setRfcOrdenante(String rfcOrdenante) {
		this.rfcOrdenante = rfcOrdenante;
	}
	/**
	 * @return the rfcReceptor
	 */
	public String getRfcReceptor() {
		return rfcReceptor;
	}
	/**
	 * @param rfcReceptor the rfcReceptor to set
	 */
	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}
	/**
	 * @return the nomOrdenante
	 */
	public String getNomOrdenante() {
		return nomOrdenante;
	}
	/**
	 * @param nomOrdenante the nomOrdenante to set
	 */
	public void setNomOrdenante(String nomOrdenante) {
		this.nomOrdenante = nomOrdenante;
	}
	/**
	 * @return the nomReceptor
	 */
	public String getNomReceptor() {
		return nomReceptor;
	}
	/**
	 * @param nomReceptor the nomReceptor to set
	 */
	public void setNomReceptor(String nomReceptor) {
		this.nomReceptor = nomReceptor;
	}
	/**
	 * @return the refPago
	 */
	public String getRefPago() {
		return refPago;
	}
	/**
	 * @param refPago the refPago to set
	 */
	public void setRefPago(String refPago) {
		this.refPago = refPago;
	}
	/**
	 * @return the numCtaOrdenante
	 */
	public String getNumCtaOrdenante() {
		return numCtaOrdenante;
	}
	/**
	 * @param numCtaOrdenante the numCtaOrdenante to set
	 */
	public void setNumCtaOrdenante(String numCtaOrdenante) {
		this.numCtaOrdenante = numCtaOrdenante;
	}
	/**
	 * @return the tipoCtaOrdenante
	 */
	public String getTipoCtaOrdenante() {
		return tipoCtaOrdenante;
	}
	/**
	 * @param tipoCtaOrdenante the tipoCtaOrdenante to set
	 */
	public void setTipoCtaOrdenante(String tipoCtaOrdenante) {
		this.tipoCtaOrdenante = tipoCtaOrdenante;
	}
	/**
	 * @return the numCtaReceptor
	 */
	public String getNumCtaReceptor() {
		return numCtaReceptor;
	}
	/**
	 * @param numCtaReceptor the numCtaReceptor to set
	 */
	public void setNumCtaReceptor(String numCtaReceptor) {
		this.numCtaReceptor = numCtaReceptor;
	}
	/**
	 * @return the tipoCtaReceptor
	 */
	public String getTipoCtaReceptor() {
		return tipoCtaReceptor;
	}
	/**
	 * @param tipoCtaReceptor the tipoCtaReceptor to set
	 */
	public void setTipoCtaReceptor(String tipoCtaReceptor) {
		this.tipoCtaReceptor = tipoCtaReceptor;
	}
	/**
	 * @return the refTransfer
	 */
	public String getRefTransfer() {
		return refTransfer;
	}
	/**
	 * @param refTransfer the refTransfer to set
	 */
	public void setRefTransfer(String refTransfer) {
		this.refTransfer = refTransfer;
	}
	/**
	 * @return the conceptoPagoTransfer
	 */
	public String getConceptoPagoTransfer() {
		return conceptoPagoTransfer;
	}
	/**
	 * @param conceptoPagoTransfer the conceptoPagoTransfer to set
	 */
	public void setConceptoPagoTransfer(String conceptoPagoTransfer) {
		this.conceptoPagoTransfer = conceptoPagoTransfer;
	}
	/**
	 * @return the direccOrdenante
	 */
	public String getDireccOrdenante() {
		return direccOrdenante;
	}
	/**
	 * @param direccOrdenante the direccOrdenante to set
	 */
	public void setDireccOrdenante(String direccOrdenante) {
		this.direccOrdenante = direccOrdenante;
	}
	/**
	 * @return the direccionBenef
	 */
	public String getDireccionBenef() {
		return direccionBenef;
	}
	/**
	 * @param direccionBenef the direccionBenef to set
	 */
	public void setDireccionBenef(String direccionBenef) {
		this.direccionBenef = direccionBenef;
	}
	/**
	 * @return the impCargo
	 */
	public String getImpCargo() {
		return impCargo;
	}
	/**
	 * @param impCargo the impCargo to set
	 */
	public void setImpCargo(String impCargo) {
		this.impCargo = impCargo;
	}
	/**
	 * @return the impAbono
	 */
	public String getImpAbono() {
		return impAbono;
	}
	/**
	 * @param impAbono the impAbono to set
	 */
	public void setImpAbono(String impAbono) {
		this.impAbono = impAbono;
	}
	/**
	 * @return the ivaMonto
	 */
	public String getIvaMonto() {
		return ivaMonto;
	}
	/**
	 * @param ivaMonto the ivaMonto to set
	 */
	public void setIvaMonto(String ivaMonto) {
		this.ivaMonto = ivaMonto;
	}
	/**
	 * @return the tipoCambio
	 */
	public String getTipoCambio() {
		return tipoCambio;
	}
	/**
	 * @param tipoCambio the tipoCambio to set
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	/**
	 * @return the impDivisa
	 */
	public String getImpDivisa() {
		return impDivisa;
	}
	/**
	 * @param impDivisa the impDivisa to set
	 */
	public void setImpDivisa(String impDivisa) {
		this.impDivisa = impDivisa;
	}
	/**
	 * @return the divisaOrdenante
	 */
	public String getDivisaOrdenante() {
		return divisaOrdenante;
	}
	/**
	 * @param divisaOrdenante the divisaOrdenante to set
	 */
	public void setDivisaOrdenante(String divisaOrdenante) {
		this.divisaOrdenante = divisaOrdenante;
	}
	/**
	 * @return the divisaBenef
	 */
	public String getDivisaBenef() {
		return divisaBenef;
	}
	/**
	 * @param divisaBenef the divisaBenef to set
	 */
	public void setDivisaBenef(String divisaBenef) {
		this.divisaBenef = divisaBenef;
	}
	/**
	 * @return the leyendaOrde
	 */
	public String getLeyendaOrde() {
		return leyendaOrde;
	}
	/**
	 * @param leyendaOrde the leyendaOrde to set
	 */
	public void setLeyendaOrde(String leyendaOrde) {
		this.leyendaOrde = leyendaOrde;
	}
	/**
	 * @return the leyendaBenef
	 */
	public String getLeyendaBenef() {
		return leyendaBenef;
	}
	/**
	 * @param leyendaBenef the leyendaBenef to set
	 */
	public void setLeyendaBenef(String leyendaBenef) {
		this.leyendaBenef = leyendaBenef;
	}
	/**
	 * Asignar fecha y hora de confirmacion
	 * @param fechaHrConfirmacion fecha y hora de confirmacion
	 */
	public void setFechaHrConfirmacion(String fechaHrConfirmacion) {
		this.fechaHrConfirmacion = fechaHrConfirmacion;
	}

	/**
	 * Obtener lista de interbancarias recibidas
	 * @return lstInterbancarias lista de interbancarias recibidas
	 */
	public List<InterbancariasRecibidasBeanUSD> getLstInterbancarias() {
		return lstInterbancarias;
	}
	/**
	 * Asignar lista de interbancarias recibidas
	 * @param lstInterbancarias lista de interbancarias recibidas
	 */
	public void setLstInterbancarias(
			List<InterbancariasRecibidasBeanUSD> lstInterbancarias) {
		this.lstInterbancarias = lstInterbancarias;
	}
	
	/**
	 * Obtener codigo de error
	 * @return codError codigo de error
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Asignar valor codigo error
	 * @param codError codigo error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * Obtener clave de rastreo
	 * @return cveRastreo clave de rastreo
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * Asignar clave de rastreo
	 * @param cveRastreo clave de rastreo
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the numReferencia
	 */
	public String getNumReferencia() {
		return numReferencia;
	}
	/**
	 * @param numReferencia the numReferencia to set
	 */
	public void setNumReferencia(String numReferencia) {
		this.numReferencia = numReferencia;
	}
	
	/**
	 * @return the bancoBN obtiene clave de banco
	 */
	public String getBancoBN() {
		return bancoBN;
	}
	
	/**
	 * @param numReferencia the bancoBN to fija clave de banco
	 */
	public void setBancoBN(String bancoBN) {
		this.bancoBN = bancoBN;
	}
	
	
	
}