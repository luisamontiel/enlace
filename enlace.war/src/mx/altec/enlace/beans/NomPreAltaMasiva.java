package mx.altec.enlace.beans;

import java.io.Serializable;

public class NomPreAltaMasiva implements Serializable {

	private static final long serialVersionUID = -7501889545436191532L;

	private String nombreArchivo;
	private String fecha;
	private String secuencia;
	private String noRegEnviados;
	private String noRegAceptados;
	private String noRegRechazados;
	private String estatus;

	public NomPreAltaMasiva() {
	}

	public NomPreAltaMasiva(String nombreArchivo, String fecha,
			String secuencia, String noRegEnviados, String noRegAceptados,
			String noRegRechazados, String estatus) {
		this.nombreArchivo = nombreArchivo;
		this.fecha = fecha;
		this.secuencia = secuencia;
		this.noRegEnviados = noRegEnviados;
		this.noRegAceptados = noRegAceptados;
		this.noRegRechazados = noRegRechazados;
		this.estatus = estatus;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

	public String getNoRegEnviados() {
		return noRegEnviados;
	}

	public void setNoRegEnviados(String noRegEnviados) {
		this.noRegEnviados = noRegEnviados;
	}

	public String getNoRegAceptados() {
		return noRegAceptados;
	}

	public void setNoRegAceptados(String noRegAceptados) {
		this.noRegAceptados = noRegAceptados;
	}

	public String getNoRegRechazados() {
		return noRegRechazados;
	}

	public void setNoRegRechazados(String noRegRechazados) {
		this.noRegRechazados = noRegRechazados;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
}
