package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class DescargaEDCBean extends DescargaEDCAuxBean implements Serializable{

	/** Serial UID **/
	private static final long serialVersionUID = 8359759881547688089L;
	/** Primer Apellido **/
	private String primerApellido;
	/** Segundo Apellido**/
	private String segundoApellido;
	/** Nombre Persona **/
	private String nombrePersona;
	/** Tipo Persona **/
	private String tipoPersona;
	/** Calle y Numero **/
	private String calle;
	/** Localidad **/
	private String localidad;
	/** Comunidad **/
	private String comunidad;
	/** Codigo Postal **/
	private String codPostal;
	/** Tarjetas Relacionadas **/
	private List<String> tarjetasRel;
	/** Constante para las cadenas vacias */
	private static final String CADENA_VACIA = "";
	/**
	 * @return el primerApellido
	 */
	public String getPrimerApellido() {
		if(primerApellido == null){
			primerApellido = CADENA_VACIA;
		}
		return primerApellido;
	}
	/**
	 * @param primerApellido el primerApellido a establecer
	 */
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	/**
	 * @return el segundoApellido
	 */
	public String getSegundoApellido() {
		if(segundoApellido == null){
			segundoApellido = CADENA_VACIA;
		}
		return segundoApellido;
	}
	/**
	 * @param segundoApellido el segundoApellido a establecer
	 */
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	/**
	 * @return el nombrePersona
	 */
	public String getNombrePersona() {
		if(nombrePersona == null){
			nombrePersona = CADENA_VACIA;
		}
		return nombrePersona;
	}
	/**
	 * @param nombrePersona el nombrePersona a establecer
	 */
	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}
	/**
	 * @return el tipoPersona
	 */
	public String getTipoPersona() {
		if(tipoPersona == null){
			tipoPersona = CADENA_VACIA;
		}
		return tipoPersona;
	}
	/**
	 * @param tipoPersona el tipoPersona a establecer
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return el calle
	 */
	public String getCalle() {
		if(calle == null){
			calle = CADENA_VACIA;
		}
		return calle;
	}
	/**
	 * @param calle el calle a establecer
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * @return el localidad
	 */
	public String getLocalidad() {
		if(localidad == null){
			localidad = CADENA_VACIA;
		}

		return localidad;
	}
	/**
	 * @param localidad el localidad a establecer
	 */
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	/**
	 * @return el comunidad
	 */
	public String getComunidad() {
		if(comunidad == null){
			comunidad = CADENA_VACIA;
		}
		return comunidad;
	}
	/**
	 * @param comunidad el comunidad a establecer
	 */
	public void setComunidad(String comunidad) {
		this.comunidad = comunidad;
	}
	/**
	 * @return el codPostal
	 */
	public String getCodPostal() {
	
		if(codPostal != null && codPostal.trim().length()>0){
			return  codPostal;			
		}
		else
			{
			codPostal=CADENA_VACIA;
			return  codPostal;

			}
		
		
		
	}
	/**
	 * @param codPostal el codPostal a establecer
	 */
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	/**
	 * Obtiene el nombre completo
	 * @return Obtiene los valores de nombre, apellido paterno y apellido materno
	 */
	public String obtenerNombreCompleto(){
		String fullName="";
		if ("F".equals(tipoPersona)){
			fullName=String.format(" %s %s, %s ",getPrimerApellido(),getSegundoApellido(),getNombrePersona());
		}else {
			fullName=String.format(" %s ",getNombrePersona());
		}
		return fullName;
	}
	/**
	 * Obtiene el domicilio completo
	 * @return Los valores de la calle, numero interior, numero exterior, colonia, ciudad y municipio
	 */
	public String obtenerDomicilioCompleto(){
		String domicilioRegresar="";

		if ("F".equals(tipoPersona) || getCalle().length()>0)
		{
			domicilioRegresar= getCalle();
			
			if(getComunidad().trim().length()>0)				
			  domicilioRegresar+=", "+getComunidad();
			
			if(getLocalidad().trim().length()>0)				
				  domicilioRegresar+=", "+getLocalidad();
			
			
			if(getCodPostal().trim().length()>0)				
				  domicilioRegresar+=", C.P:"+getCodPostal();
			
			return domicilioRegresar;
			
			
		}
		else{ return CADENA_VACIA;
			
		}
	}

	/**
	 * @param tarjetasRel el tarjetasRel a establecer
	 */
	public void setTarjetasRel(List<String> tarjetasRel) {
		this.tarjetasRel = tarjetasRel;
	}
	/**
	 * @return el tarjetasRel
	 */
	public List<String> getTarjetasRel() {
		if (tarjetasRel == null) {
			tarjetasRel= new ArrayList<String>();
		}
		return tarjetasRel;
	}
}