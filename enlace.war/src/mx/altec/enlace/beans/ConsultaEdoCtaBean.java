package mx.altec.enlace.beans;

public class ConsultaEdoCtaBean {
	
	/**
	 * variable folio
	 */
	private String folio;
	
	/**
	 * variable usuarioSolicitante
	 */
	private String usuarioSolicitnate;
	
	/**
	 * variable fechaInicio
	 */
	private String fechaInicio;
	
	/**
	 * variable fechaFin
	 */
	private String fechaFin;
	
	/**
	 * variable estatus
	 */
	private String estatus;
        
      	/**
	 * variable estatus
	 */
	private String descErr;
	
	/**
	 * variable contrato
	 */
	private String contrato;
	
	/**
	 * variable regIni
	 */
	private int regIni;
	
	/**
	 * variable regFin
	 */
	private int regFin;

	/**
	 * obtener folio
	 * @return folio : folio
	 */
	public String getFolio() {
		return folio;
	}

	
	/**
	 * asignar folio
	 * @param folio : folio
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * obtener usuarioSolicitnate
	 * @return usuarioSolicitnate : usuarioSolicitnate
	 */
	public String getUsuarioSolicitnate() {
		return usuarioSolicitnate;
	}

	/**
	 * asignar usuarioSolicitnate
	 * @param usuarioSolicitnate : usuarioSolicitnate
	 */
	public void setUsuarioSolicitnate(String usuarioSolicitnate) {
		this.usuarioSolicitnate = usuarioSolicitnate;
	}

	/**
	 * obtener fechaInicio
	 * @return fechaInicio : fechaInicio
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * asignar fechaInicio
	 * @param fechaInicio : fechaInicio
	 */
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * obtener fechaFin
	 * @return fechaFin : fechaFin
	 */
	public String getFechaFin() {
		return fechaFin;
	}

	/**
	 * asignar fechaFin
	 * @param fechaFin : fechaFin
	 */
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	/**
	 * obtener estatus
	 * @return estatus : estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * asignar estatus
	 * @param estatus : estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * obtener contrato
	 * @return contrato : contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * asignar contrato
	 * @param contrato : contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * obtener regIni
	 * @return regIni : regIni
	 */
	public int getRegIni() {
		return regIni;
	}

	/**
	 * asignar regIni
	 * @param regIni : regIni
	 */
	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}

	/**
	 * obtener regFin
	 * @return regFin : regFin
	 */
	public int getRegFin() {
		return regFin;
	}

	/**
	 * asignar regFin
	 * @param regFin : regFin
	 */
	public void setRegFin(int regFin) {
		this.regFin = regFin;
	} 
	
        /**
	 * obtener descripcion del Error
	 * @return descErr : descErr
	 */
	public String getDescErr() {
		return descErr;
	}

	/**
	 * asignar descripcion del Error
	 * @param descErr : descErr
	 */
	public void setDescErr(String descErr) {
		this.descErr = descErr;
	} 
	

}
