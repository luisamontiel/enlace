package mx.altec.enlace.beans;

public class ExportTransferenciasBean {

	/**
	 * Variable para almacenar el valor del check.
	 */
	String check;
	/**
	 * Variable para almacenar el tipoOperacion.
	 */
	String tipoOperacion;
	/**
	 * Variable para almacenar el tipoCuenta.
	 */
	String tipoCuenta;
	/**
	 * Variable para almacenar la cuenta.
	 */
	String cuenta;
	/**
	 * Variable para almacenar el titular.
	 */
	String titular;
	/**
	 * Variable para almacenar el banco.
	 */
	String banco;
	/**
	 * Variable para almacenar la fechaRegistro.
	 */
	String fechaRegistro;
	/**
	 * Variable para almacenar el folioRegistro.
	 */
	String folioRegistro;
	/**
	 * Variable para almacenar la fechaAtorizacion.
	 */
	String fechaAtorizacion;
	/**
	 * Variable para almacenar el folioAutorizacion.
	 */
	String folioAutorizacion;
	/**
	 * Variable para almacenar el usrRegistrado.
	 */
	String usrRegistrado;
	/**
	 * Variable para almacenar el usrAutorizado.
	 */
	String usrAutorizado;
	/**
	 * Variable para almacenar el estado.
	 */
	String estado;
	/**
	 * Variable para almacenar las observaciones.
	 */
	String observaciones;
	/**
	 * Variable para almacenar el tipo de divisa.
	 */
	String tipoDivisa;	

	/**
	 * getCheck de tipo String.
	 * @author FSW-Indra
	 * @return check de tipo String
	 */
	public String getCheck() {
		return check;
	}
	/**
	 * setCheck para asignar valor a check.
	 * @author FSW-Indra
	 * @param check de tipo String
	 */
	public void setCheck(String check) {
		this.check = check;
	}
	/**
	 * getTipoOperacion de tipo String.
	 * @author FSW-Indra
	 * @return tipoOperacion de tipo String
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * setTipoOperacion para asignar valor a tipoOperacion.
	 * @author FSW-Indra
	 * @param tipoOperacion de tipo String
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * getTipoCuenta de tipo String.
	 * @author FSW-Indra
	 * @return tipoCuenta de tipo String
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 * setTipoCuenta para asignar valor a tipoCuenta.
	 * @author FSW-Indra
	 * @param tipoCuenta de tipo String
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	/**
	 * getCuenta de tipo String.
	 * @author FSW-Indra
	 * @return cuenta de tipo String
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * setCuenta para asignar valor a cuentacuenta.
	 * @author FSW-Indra
	 * @param cuenta de tipo String
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * getTitular de tipo String.
	 * @author FSW-Indra
	 * @return titular de tipo String
	 */
	public String getTitular() {
		return titular;
	}
	/**
	 * setTitular para asignar valor a titular.
	 * @author FSW-Indra
	 * @param titular de tipo String
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}
	/**
	 * getBanco de tipo String.
	 * @author FSW-Indra
	 * @return banco de tipo String
	 */
	public String getBanco() {
		return banco;
	}
	/**
	 * setBanco para asignar valor a banco.
	 * @author FSW-Indra
	 * @param banco de tipo String
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}
	/**
	 * getFechaRegistro de tipo String.
	 * @author FSW-Indra
	 * @return fechaRegistro de tipo String
	 */
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	/**
	 * setFechaRegistro para asignar valor a fechaRegistro.
	 * @author FSW-Indra
	 * @param fechaRegistro de tipo String
	 */
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	/**
	 * getFolioRegistro de tipo String.
	 * @author FSW-Indra
	 * @return folioRegistro de tipo String
	 */
	public String getFolioRegistro() {
		return folioRegistro;
	}
	/**
	 * setFolioRegistro para asignar valor a folioRegistro.
	 * @author FSW-Indra
	 * @param folioRegistro de tipo String
	 */
	public void setFolioRegistro(String folioRegistro) {
		this.folioRegistro = folioRegistro;
	}
	/**
	 * getFechaAtorizacion de tipo String.
	 * @author FSW-Indra
	 * @return fechaAtorizacion de tipo String
	 */
	public String getFechaAtorizacion() {
		return fechaAtorizacion;
	}
	/**
	 * setFechaAtorizacion para asignar valor a fechaAtorizacion.
	 * @author FSW-Indra
	 * @param fechaAtorizacion de tipo String
	 */
	public void setFechaAtorizacion(String fechaAtorizacion) {
		this.fechaAtorizacion = fechaAtorizacion;
	}
	/**
	 * getFolioAutorizacion de tipo String.
	 * @author FSW-Indra
	 * @return folioAutorizacion de tipo String
	 */
	public String getFolioAutorizacion() {
		return folioAutorizacion;
	}
	/**
	 * setFolioAutorizacion para asignar valor a folioAutorizacion.
	 * @author FSW-Indra
	 * @param folioAutorizacion de tipo String
	 */
	public void setFolioAutorizacion(String folioAutorizacion) {
		this.folioAutorizacion = folioAutorizacion;
	}
	/**
	 * getUsrRegistrado de tipo String.
	 * @author FSW-Indra
	 * @return usrRegistrado de tipo String
	 */
	public String getUsrRegistrado() {
		return usrRegistrado;
	}
	/**
	 * setUsrRegistrado para asignar valor a usrRegistrado.
	 * @author FSW-Indra
	 * @param usrRegistrado de tipo String
	 */
	public void setUsrRegistrado(String usrRegistrado) {
		this.usrRegistrado = usrRegistrado;
	}
	/**
	 * getUsrAutorizado de tipo String.
	 * @author FSW-Indra
	 * @return usrAutorizado de tipo String
	 */
	public String getUsrAutorizado() {
		return usrAutorizado;
	}
	/**
	 * setUsrAutorizado para asignar valor a usrAutorizado.
	 * @author FSW-Indra
	 * @param usrAutorizado de tipo String
	 */
	public void setUsrAutorizado(String usrAutorizado) {
		this.usrAutorizado = usrAutorizado;
	}
	/**
	 * getEstado de tipo String.
	 * @author FSW-Indra
	 * @return estado de tipo String
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * setEstado para asignar valor a estado.
	 * @author FSW-Indra
	 * @param estado de tipo String
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * getObservaciones de tipo String.
	 * @author FSW-Indra
	 * @return observaciones de tipo String
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/**
	 * setObservaciones para asignar valor a observaciones.
	 * @author FSW-Indra
	 * @param observaciones de tipo String
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/**
	 * getTipoDivisa de tipo String.
	 * @author FSW-Vector
	 * @return tipoDivisa de tipo String
	 */
	public String getTipoDivisa() {
		return tipoDivisa;
	}
	/**
	 * setTipoDivisa para asignar valor a tipoDivisa.
	 * @author FSW-Vector
	 * @param tipoDivisa de tipo String
	 */
	public void setTipoDivisa(String tipoDivisa) {
		this.tipoDivisa = tipoDivisa;
	}

}
