package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * @author asanjuan
 *
 */
/**
 * @author asanjuan
 *
 */
public class PreguntaDTO implements Serializable {

	/** serialVersionUID*/
	private static final long serialVersionUID = 5552146589398138893L;
	/** preguntaTexto*/
	private String preguntaTexto;
	/** preguntaID*/
	private String preguntaID;
	
	/**
	 * @return String : preguntaTexto
	 */
	public String getPreguntaTexto() {
		return preguntaTexto;
	}
	
	/**
	 * @param preguntaTexto : preguntaTexto
	 */
	public void setPreguntaTexto(String preguntaTexto) {
		this.preguntaTexto = preguntaTexto;
	}
	
	/**
	 * @return String : preguntaID
	 */
	public String getPreguntaID() {
		return preguntaID;
	}
	
	/**
	 * @param preguntaID : preguntaID
	 */
	public void setPreguntaID(String preguntaID) {
		this.preguntaID = preguntaID;
	}
	
	
}
