package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;


import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import com.netscape.server.servlet.i18n.io.Serializable;

public class AdmUsrGL60 extends AdmUsrGL35Base implements Serializable {
	
	public static String HEADER = "GL6001031123451O00N2";
	
	public static AdmUsrGL60Factory getFactoryInstance() {
		return FACTORY;
	}	

	
	private static final AdmUsrGL60Factory FACTORY = new AdmUsrGL60Factory();
	
	private static class AdmUsrGL60Factory implements AdmUsrBuilder<AdmUsrGL60> {

		public AdmUsrGL60 build(String arg) {
			
			AdmUsrGL60 bean = new AdmUsrGL60();
			
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GLA0000");
				bean.setCodExito(true);
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}			
			return bean;
		}
	}

}
