package mx.altec.enlace.beans;

public interface NomPreBuilder<T> {
	
	T build(String args);
}
