package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminlym.shared.LimitesContrato;


import java.io.Serializable;
import java.util.ArrayList;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Util;

public class AdmLyMGL25 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL2500461123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCGLM251  P";
	private static final String FORMATO_HEADER_DET = "@DCGLM252  P";
	
	private static final AdmLyMGL25Factory FACTORY = new AdmLyMGL25Factory();
	
	private ArrayList<LimitesContrato> limites;
	
	public static AdmLyMGL25Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmLyMGL25Factory implements AdmUsrBuilder<AdmLyMGL25> {

		public AdmLyMGL25 build(String arg) {
			
			AdmLyMGL25 bean = new AdmLyMGL25();
			ArrayList<LimitesContrato> limList = new ArrayList<LimitesContrato>();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL250000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);							

				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						int numRegs = Integer.parseInt(getValor(arg, index , 3));
						EIGlobal.mensajePorTrace("==> numRegs: [" + numRegs + "]", EIGlobal.NivelLog.INFO);
						if (numRegs > 0) {
							index = arg.indexOf(FORMATO_HEADER_DET);
							if (index != -1) {
								EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
								for (int i = 0; i < numRegs; i ++) { // lee el detalle
									index += FORMATO_HEADER_DET.length();
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
									LimitesContrato lcBean = new LimitesContrato();
									lcBean.setTipoOperacion(getValor(arg, index, 1));
									EIGlobal.mensajePorTrace("==> tipo operacion: [" + lcBean.getTipoOperacion() + "]", EIGlobal.NivelLog.INFO);
									lcBean.setGrupo(getValor(arg, index += 1, 2));
									EIGlobal.mensajePorTrace("==> grupo: [" + lcBean.getGrupo() + "]", EIGlobal.NivelLog.INFO);
									lcBean.setDescripcionGrupo(getValor(arg, index += 2, 40));
									EIGlobal.mensajePorTrace("==> desc grupo: [" + lcBean.getDescripcionGrupo() + "]", EIGlobal.NivelLog.INFO);
									lcBean.setContrato(getValor(arg, index += 40, 12).substring(1));
									EIGlobal.mensajePorTrace("==> contrato: [" + lcBean.getContrato() + "]", EIGlobal.NivelLog.INFO);
									lcBean.setMonto(Util.formateaImporte(getValor(arg, index += 12, 17)));
									EIGlobal.mensajePorTrace("==> monto: [" + lcBean.getMonto() + "]", EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
									index += 18;
									limList.add(lcBean);
								}
							}
						}
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmLyMGL25 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setLimites(limList);
			return bean;
		}
		
	}

	public ArrayList<LimitesContrato> getLimites() {
		return limites;
	}

	public void setLimites(ArrayList<LimitesContrato> limites) {
		this.limites = limites;
	}




}
