package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author ESC Bean referente a la transacci&oacute;n LMXE CONSULTA DETALLE
 *         REMESA
 */
public class LMXE extends AdmUsrGL35Base implements Serializable {

	/**
	 * Header de entrada para la transaccion LMXE
	 */
	public static String HEADER = "LMXE00891123451O00N2";
	/**
	 * Identificador de header
	 */
	private static final String FORMATO_HEADER = "@DCLMMCXE2 P";

	/**
	 * Identificador de detalle
	 */
	private static final String FORMATO_DETALLE = "@DCLMMCXE2 P";
	/**
	 * Identificador de fin de datos
	 */
	private static final String FIN_DATOS = "FIN DATOS";

	private static final String EXISTEN_MAS_DATOS = "EXISTEN MAS REGISTROS A CONSULTAR";
	
	/**
	 * Identificador de Consulta Efectuada Exitosamente por remesa.
	 */
	private static final String CONSULTA_EFECTUADA = "CONSULTA EFECTUADA";
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Entidad de las remesas
	 */
	private String entidad = "0014";
	/**
	 * Contrato enlace de las remesas
	 */
	private String contratoEnlace;

	private String totalTarjetas;
	public String getTotalTarjetas() {
		return totalTarjetas;
	}

	public void setTotalTarjetas(String totalTarjetas) {
		this.totalTarjetas = totalTarjetas;
	}

	/**
	 * Contrato distribuci&oacute;n de las remesas
	 */
	private String ctroDistribucion;
	/**
	 * N&uacute;mero de las remesas
	 */
	private String numeroRemesa;
	/**
	 * Estado de las tarjetas
	 */
	private String estadoTarjeta;
	/**
	 * N&uacute;mero de tarjeta
	 */
	private String tarjeta;

	private String numRegistros;

	private String numRegistrosAnt;

	private String numRemesa2;


	public String getNumRemesa2() {
		return numRemesa2;
	}

	public void setNumRemesa2(String numRemesa2) {
		this.numRemesa2 = numRemesa2;
	}

	private String tipoFecha;

	private String fechaIniicio;

	private String fechaFin;
	public String getTipoFecha() {
		return tipoFecha;
	}

	public void setTipoFecha(String tipoFecha) {
		this.tipoFecha = tipoFecha;
	}

	public String getFechaIniicio() {
		return fechaIniicio;
	}

	public void setFechaIniicio(String fechaIniicio) {
		this.fechaIniicio = fechaIniicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	/**
	 * Lista de tarjetas
	 */
	private ArrayList<TarjetasBean> listaTarjetas;
	/**
	 * Indicador si la transaccion es exitosa
	 */
	private boolean finDatos;

	private boolean existenMasDatos;
	/**
	 * Constructor de salida de transaccion
	 */
	private static final NomPreLMXEFactory FACTORY = new NomPreLMXEFactory();


	public String getNumRegistros() {
		return numRegistros;
	}

	public void setNumRegistros(String numRegistros) {
		this.numRegistros = numRegistros;
	}

	public String getNumRegistrosAnt() {
		return numRegistrosAnt;
	}

	public void setNumRegistrosAnt(String numRegistrosAnt) {
		this.numRegistrosAnt = numRegistrosAnt;
	}

	public boolean isExistenMasDatos() {
		return existenMasDatos;
	}

	public void setExistenMasDatos(boolean existenMasDatos) {
		this.existenMasDatos = existenMasDatos;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getContratoEnlace() {
		return contratoEnlace;
	}

	public void setContratoEnlace(String contratoEnlace) {
		this.contratoEnlace = contratoEnlace;
	}

	public String getCtroDistribucion() {
		return ctroDistribucion;
	}

	public void setCtroDistribucion(String ctroDistribucion) {
		this.ctroDistribucion = ctroDistribucion;
	}

	public String getNumeroRemesa() {
		return numeroRemesa;
	}

	public void setNumeroRemesa(String numeroRemesa) {
		this.numeroRemesa = numeroRemesa;
	}

	public void setEstadoTarjeta(String estadoTarjeta) {
		this.estadoTarjeta = estadoTarjeta;
	}

	public String getEstadoTarjeta() {
		return estadoTarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public static NomPreLMXEFactory getFactoryInstance() {
		return FACTORY;
	}

	public void setListaTarjetas(ArrayList<TarjetasBean> listaTarjetas) {
		this.listaTarjetas = listaTarjetas;
	}

	public ArrayList<TarjetasBean> getListaTarjetas() {
		return listaTarjetas;
	}

	public void setFinDatos(boolean finDatos) {
		this.finDatos = finDatos;
	}

	public boolean isFinDatos() {
		return finDatos;
	}

	/**
	 * Clase usada para generar un elemento LMXC a partir de la cadena de salida
	 * de la transaccion
	 *
	 * @author ESC
	 */
	private static class NomPreLMXEFactory implements AdmUsrBuilder<LMXE> {

		/**
		 * Componente encargado de construir el objeto LMXC a partir de una
		 * cadena de entrada
		 *
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg
		 *            Cadena de entrada
		 */
		public LMXE build(String arg) {

			LMXE bean = new LMXE();
			ArrayList<TarjetasBean> listaTarjetas = new ArrayList<TarjetasBean>();
			if (isCodigoExito(arg)) {

				bean.setCodExito(true);
				bean.setCodigoOperacion("LMXE0000");
				int index = arg.indexOf(FORMATO_HEADER);

				if (index != -1) {

					index += FORMATO_HEADER.length();
				}

				if (arg.indexOf(FIN_DATOS) != -1) {
					bean.setFinDatos(true);
				}

				if (arg.indexOf(EXISTEN_MAS_DATOS) != -1) {
					bean.setExistenMasDatos(true);
				} else {
					bean.setExistenMasDatos(false);
				}
				
				if (arg.indexOf(CONSULTA_EFECTUADA) != -1) {
					bean.setFinDatos(true);
				}

				String[] detalles = getDetalles(arg, FORMATO_DETALLE, 66);

				if (detalles != null && detalles.length > 0) {

					for (String detalle : detalles) {

						if (detalle.length() == 0) {
							continue;
						}

						TarjetasBean remesa = new TarjetasBean();
						remesa.setTarjeta(getValor(detalle, 0, 22)); // TARJETA
						remesa.setCondEstampa(getValor(detalle, 22, 3)); // COND-ESTAMPA
						remesa.setEstadoTarjeta(getValor(detalle, 25, 1)); // ESTADO-TARJETA
						remesa.setFechaCaducidad(getValor(detalle, 26, 10)); // FECHA-CADUCIDAD
						remesa.setFechaAsignacion(getValor(detalle, 36, 10)); // FECHA-ASIGNACION
						remesa.setFechaBaja(getValor(detalle, 46, 10)); // FECHA-BAJA
						remesa.setFechaUltimaMod(getValor(detalle, 56, 10)); // FECHA-ULTIMA-MOD
						listaTarjetas.add(remesa);
					}
					bean.setListaTarjetas(listaTarjetas);
				}

			} else if (isCodigoError(arg)) {
				bean.setCodExito(false);
				bean.setCodigoOperacion(getCodigoError(arg));
			}

			return bean;
		}
	}
}