package mx.altec.enlace.beans;

public class CAMancomunidadSABean {

	private String usuario;
	private String opConsulta;
	private String fecha1;
	private String fecha2;
	private String importe;
	private String folioRegistro;
	private String cuenta;
	private String producto;
	private String chkArchivo;
	private String folioArchivo;
	private String chkServicios;
	private String chkAutorizadas;
	private String chkPendientes;
	private String chkRechazadas;
	private String chkEjecutadas;
	private String chkCanceladas;
	private String chkNoEjecutadas;
	private String chkMancomunada;
	private String chkPreAutorizada;
	private String chkVerificada;


	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getOpConsulta() {
		return opConsulta;
	}
	public void setOpConsulta(String opConsulta) {
		this.opConsulta = opConsulta;
	}
	public String getFecha1() {
		return fecha1;
	}
	public void setFecha1(String fecha1) {
		this.fecha1 = fecha1;
	}
	public String getFecha2() {
		return fecha2;
	}
	public void setFecha2(String fecha2) {
		this.fecha2 = fecha2;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getFolioRegistro() {
		return folioRegistro;
	}
	public void setFolioRegistro(String folioRegistro) {
		this.folioRegistro = folioRegistro;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getChkArchivo() {
		return chkArchivo;
	}
	public void setChkArchivo(String chkArchivo) {
		this.chkArchivo = chkArchivo;
	}
	public String getFolioArchivo() {
		return folioArchivo;
	}
	public void setFolioArchivo(String folioArchivo) {
		this.folioArchivo = folioArchivo;
	}
	public String getChkServicios() {
		return chkServicios;
	}
	public void setChkServicios(String chkServicios) {
		this.chkServicios = chkServicios;
	}
	public String getChkAutorizadas() {
		return chkAutorizadas;
	}
	public void setChkAutorizadas(String chkAutorizadas) {
		this.chkAutorizadas = chkAutorizadas;
	}
	public String getChkPendientes() {
		return chkPendientes;
	}
	public void setChkPendientes(String chkPendientes) {
		this.chkPendientes = chkPendientes;
	}
	public String getChkRechazadas() {
		return chkRechazadas;
	}
	public void setChkRechazadas(String chkRechazadas) {
		this.chkRechazadas = chkRechazadas;
	}
	public String getChkEjecutadas() {
		return chkEjecutadas;
	}
	public void setChkEjecutadas(String chkEjecutadas) {
		this.chkEjecutadas = chkEjecutadas;
	}
	public String getChkCanceladas() {
		return chkCanceladas;
	}
	public void setChkCanceladas(String chkCanceladas) {
		this.chkCanceladas = chkCanceladas;
	}
	public String getChkNoEjecutadas() {
		return chkNoEjecutadas;
	}
	public void setChkNoEjecutadas(String chkNoEjecutadas) {
		this.chkNoEjecutadas = chkNoEjecutadas;
	}
	public String getChkMancomunada() {
		return chkMancomunada;
	}
	public void setChkMancomunada(String chkMancomunada) {
		this.chkMancomunada = chkMancomunada;
	}
	public String getChkPreAutorizada() {
		return chkPreAutorizada;
	}
	public void setChkPreAutorizada(String chkPreAutorizada) {
		this.chkPreAutorizada = chkPreAutorizada;
	}
	public String getChkVerificada() {
		return chkVerificada;
	}
	public void setChkVerificada(String chkVerificada) {
		this.chkVerificada = chkVerificada;
	}

}