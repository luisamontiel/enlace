package mx.altec.enlace.beans;

public class ConsEdoCtaDetalleHistBean {
	
	/**
	 * variable idEdoCtaHist
	 */
	private String idEdoCtaHist;
	
	/**
	 * variable numCuenta
	 */
	private String numCuenta;
	
	/**
	 * variable numSec
	 */
	private String numSec;
	
	/**
	 * variable estatus
	 */
	private String estatus;
	
	/**
	 * variable descError
	 */
	private String descError;

	/**
	 * variable periodo
	 */
	private String periodo;
	
	/**
	 * variable fchSol
	 */
	private String fchSol;
	
	/**
	 * variable tipoEdoCta
	 */
	private String tipoEdoCta;
	
	/**
	 * variable descCta
	 */
	private String descCta;
	
	/**
	 * variable totalRegistros
	 */
	private int totalRegistros;
	
	/**
	 * variable idUsuario
	 */
	private String idUsuario;
	
	/**
	 * obtiene estatus
	 * @return estatus : estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * asigna estatus
	 * @param estatus : estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Asigna periodo
	 * @param periodo : periodo
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	/**
	 * obtiene periodo
	 * @return periodo : periodo
	 */
	public String getPeriodo() {
		return periodo;
	}
	
	/**
	 * obtiene numSec
	 * @return numSec : numSec
	 */
	public String getNumSec() {
		return numSec;
	}

	/**
	 * asigna numSec
	 * @param numSec : numSec
	 */
	public void setNumSec(String numSec) {
		this.numSec = numSec;
	}


	/**
	 * obtiene fchSol
	 * @return fchSol : fchSol
	 */
	public String getFchSol() {
		return fchSol;
	}

	/**
	 * asigna fchSol
	 * @param fchSol : fchSol
	 */
	public void setFchSol(String fchSol) {
		this.fchSol = fchSol;
	}		
	
	/**
	 * Obtiene idEdoCtaHist
	 * @return idEdoCtaHist : idEdoCtaHist
	 */
	public String getIdEdoCtaHist() {
		return idEdoCtaHist;
	}

	/**
	 * asigna idEdoCtaHist
	 * @param idEdoCtaHist : idEdoCtaHist
	 */
	public void setIdEdoCtaHist(String idEdoCtaHist) {
		this.idEdoCtaHist = idEdoCtaHist;
	}

	/**
	 * obtiene numCuenta
	 * @return numCuenta : numCuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}

	/**
	 * asigna numCuenta
	 * @param numCuenta : numCuenta
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}


	/**
	 * obtiene descError
	 * @return descError : descError
	 */
	public String getDescError() {
		return descError;
	}

	/**
	 * asigna descError
	 * @param descError : descError
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}


	/**
	 * obtiene idUsuario
	 * @return idUsuario : idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}

	/**
	 * asigna idUsuario
	 * @param idUsuario : idUsuario
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * obtiene tipoEdoCta
	 * @return tipoEdoCta : tipoEdoCta
	 */
	public String getTipoEdoCta() {
		return tipoEdoCta;
	}

	/**
	 * asigna tipoEdoCta
	 * @param tipoEdoCta : tipoEdoCta
	 */
	public void setTipoEdoCta(String tipoEdoCta) {
		this.tipoEdoCta = tipoEdoCta;
	}

	/**
	 * obtiene descCta
	 * @return descCta : descCta
	 */
	public String getDescCta() {
		return descCta;
	}

	/**
	 * asigna descCta
	 * @param descCta : descCta
	 */
	public void setDescCta(String descCta) {
		this.descCta = descCta;
	}

	/**
	 * Obtitne totalRegistros
	 * @return totalRegistros : totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}

	/**
	 * Asigna totalRegistros
	 * @param totalRegistros : totalRegistros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

}
