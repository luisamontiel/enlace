package mx.altec.enlace.beans;
import java.io.Serializable;

/**
 * Bean de datos para la administracion de pago referenciado por micrositio
 * @author c109841 Ortiz Vargas
 *
 */
public class PagoMicrositioRefBean implements Serializable{

	/**
	 * Serial de version
	 */
	private static final long serialVersionUID = 1L;
	
	//********************************************************
	//* Definicion de variables 							 */
	//********************************************************
	
	/**
	 * Linea de captura 
	 */
	private String lineaCaptura;
	
	/**
	 * Tipo Sevicio
	 */
	private String idSat;
	
	/**
	 * Tipo Persona juridica
	 */
	private String tipoPersona;
	
	/**
	 * Importe
	 */
	private String importeSat;
	
	/**
	 * rfc
	 */
	private String rfc;
	
	/**
	 * Datos hash
	 */
	private String datosHash;
	
	/**
	 * Convenio
	 */
	private String convenio;
	/**
	 * Mensajes de error
	 */
	private StringBuffer msgError;
	
	//********************************************************
	//* Geters and Seters								 */
	//********************************************************
	
	/**
	 * Obtinen la linea de captura
	 * @return lineaCaptura linea de captura
	 */
	public String getLineaCaptura() {
		return lineaCaptura.trim();
	}

	/**
	 * Setea la linea de captura
	 * @param lineaCaptura linea de captura
	 */
	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}

	/**
	 * Obtiene el id de servicio a realizar
	 * @return idSat id de servicio
	 */
	public String getIdSat() {
		return idSat.trim();
	}

	/**
	 * Establece el Id de servicio
	 * @param idSat id de servicio
	 */
	public void setIdSat(String idSat) {
		this.idSat = idSat;
	}

	/**
	 * obtiene el tipo de persona juridica
	 * @return tipoPersona persona juridica
	 */
	public String getTipoPersona() {
		return tipoPersona.trim();
	}
	
	/**
	 * obtiene el mensaje de error a mostrar
	 * @return msgError mensaje de error
	 */
	public StringBuffer getMsgError(){
		return msgError;
	}

	/**
	 * Establece el tipo de persona juridica
	 * @param tipoPersona persona juridica
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
	/**
	 * Obtiene el importe
	 * @return importeSat importe
	 */
	public String getImporteSat() {
		return importeSat;
	}

	/**
	 * establece el importe
	 * @param importeSat importe
	 */
	public void setImporteSat(String importeSat) {
		this.importeSat = importeSat;
	}

	/**
	 * obtiene el rfce del contribuyente
	 * @return rfc rfc
	 */
	public String getRfc() {
		return rfc.trim();
	}

	/**
	 * establece el RFC del contribuyente
	 * @param rfc rfc
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * obtiene los datos del hash
	 * @return datos hash hash
	 */
	public String getDatosHash() {
		return datosHash;
	}

	/**
	 * Establece el hash recibido para comparar llaves
	 * @param datosHash hash
	 */
	public void setDatosHash(String datosHash) {
		this.datosHash = datosHash;
	}

	/**
	 * obtiene el convenio de la operacion 
	 * @return convenio convenio
	 */
	public String getConvenio() {
		return convenio.trim();
	}

	/**
	 * establece el convenio al cual se realiza el pago
	 * @param convenio convenio
	 */
	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	
	/**
	 * Establece el mensaje de error a mostrar
	 * @param msgError Mensaje de error
	 */
	public void setMsgError (StringBuffer msgError)
	{
		this.msgError  = msgError;
	}
}
