package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * @author asanjuan
 *
 */
public class ImagenDTO implements Serializable{

	/** serialVersionUID*/
	private static final long serialVersionUID = 6379702278566743148L;
	/** imagen*/
	private String imagen;
	/** idImagen*/
	private String idImagen;

	/**
	 * @return String : imagen
	 */
	public String getImagen() {
		return imagen;
	}

	/**
	 * @param imagen : imagen
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	/**
	 * @return String : imagen
	 */
	public String getIdImagen() {
		return idImagen;
	}

	/**
	 * @param idImagen : imagen
	 */
	public void setIdImagen(String idImagen) {
		this.idImagen = idImagen;
	}



}
