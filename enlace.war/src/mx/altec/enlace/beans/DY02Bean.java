package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.io.Serializable;

import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.utilerias.EIGlobal;

public class DY02Bean implements Serializable {

	/**
	 * Id de version generado automaticamente para la serializacion del objeto
	 */
	private static final long serialVersionUID = 1900035358757339711L;
	/**
	 * Id de la trma
	 */
	private static final String ID_TRAMA = "DY02";
	/**
	 * Campo "Opcion" en la trama DY02
	 */
	private static final String OPCION = "A";
	/**
	 * Campo "Canal" en la trama DY02
	 */
	private static final String CANAL = "05";
	
	/**
	 * Codigo del cliente
	 */
	private String codigoCliente;
	/**
	 * Cuenta de la que se solicitan los estados de cuenta
	 */
	private String cuenta;
	/**
	 * Tipo de formato para el estado de cuenta
	 */
	private TipoFormatoEstadoCuenta formato;
	/**
	 * Periodo del estado de cuenta
	 */
	private String periodo;
	/**
	 * Campo "Tipo de Cuenta" en la trama DY02
	 */
	private String tipoEDC; //TIPO_CUENTA;	
	/**
	 * Folio de peticion
	 */
	private String folioPeticion;
	/**
	 * Folio Ondemand
	 */
	private String folioOndemand;
	/**
	 * Folio UUID
	 */
	private String folioUUID;
	/**
	 * Codigo de respuesta de la trama DY02
	 */
	private String codigoRespuesta;
	/**
	 * Descripcion de la respuesta de la trama DY02
	 */
	private String descripcionRespuesta;
	
	/**
	 * Obtiene el Codigo del Cliente
	 * @return El Codigo del Cliente
	 * @throws ValidationException En caso de que no exista informacion
	 */
	public String getCodigoCliente() throws ValidationException {
		if(codigoCliente == null || codigoCliente.length() == 0){
			throw new ValidationException("DY02E01");
		}
		
		return codigoCliente;
	}
	/**
	 * Asigna un valor al Codigo del Cliente
	 * @param codigoCliente El Codigo del Cliente que se desea asignar
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	/**
	 * Obtiene la Cuenta
	 * @return La Cuenta
	 * @throws ValidationException En caso de no exista informacion
	 */
	public String getCuenta() throws ValidationException {
		if(cuenta == null || cuenta.length() == 0){
			throw new ValidationException("DY02E02");
		}
		
		return cuenta;
	}
	/**
	 * Asigna un valor para la Cuenta
	 * @param cuenta La cuenta que se desea asignar
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * Obtiene el Formato del estado de cuenta
	 * @return El Formato del estado de cuenta
	 * @throws ValidationException En caso de que no exista informacion
	 */
	public TipoFormatoEstadoCuenta getFormato() throws ValidationException {
		if(formato == null){
			throw new ValidationException("DY02E03");
		}
		
		return formato;
	}
	/**
	 * Obtiene el identificador del formato
	 * @return El identificador del formato
	 * @throws ValidationException En caso de que no exista informacion
	 */
	public int getIdFormato() throws ValidationException {
		if(formato == null){
			throw new ValidationException("DY02E04");
		}
		
		return formato.obtenerIdFormato();
	}
	/**
	 * Asigna un formato
	 * @param formato El formato que se desea asignar
	 */
	public void setFormato(TipoFormatoEstadoCuenta formato) {
		this.formato = formato;
	}
	/**
	 * Obtiene el periodo
	 * @return El periodo
	 * @throws ValidationException En caso de que no exista informacion
	 */
	public String getPeriodo() throws ValidationException {
		if(periodo == null || periodo.length() == 0){
			throw new ValidationException("DY02E05");
		}
		
		return periodo;
	}
	/**
	 * Asigna un valor para el periodo
	 * @param periodo El periodo que se desea asignar
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	/**
	 * @param tipoEDC el tipoEDC a establecer
	 */
	public void setTipoEDC(String tipoEDC) {
		this.tipoEDC = tipoEDC;
	}
	/**
	 * @return el tipoEDC
	 */
	public String getTipoEDC() {
		return tipoEDC;
	}
	/**
	 * Obtiene el folio de peticion
	 * @return El folio de peticion
	 * @throws ValidationException En caso de que no exista informacion
	 */
	public String getFolioPeticion() throws ValidationException {
		if(folioPeticion == null || folioPeticion.length() == 0){
			throw new ValidationException("DY02E06");
		}
		
		return folioPeticion;
	}
	/**
	 * Asigna un folio de peticion
	 * @param folioPeticion El folio de peticion que se desea asignar
	 */
	public void setFolioPeticion(String folioPeticion) {
		this.folioPeticion = folioPeticion;
	}
	/**
	 * Obtiene el folio ondemand
	 * @return El folio ondemand
	 * @throws ValidationException En caso de que no exista informacion
	 */
	public String getFolioOndemand() throws ValidationException {
		if((folioOndemand == null || folioOndemand.length() == 0) &&
		formato == TipoFormatoEstadoCuenta.PDF){
			throw new ValidationException("DY02E07");
		}
		
		return folioOndemand;
	}
	/**
	 * Asigna un valor para el folio ondemand
	 * @param folioOndemand El folio ondemand que se desea asignar
	 */
	public void setFolioOndemand(String folioOndemand) {
		this.folioOndemand = folioOndemand;
	}
	/**
	 * Obtiene el folio UUID
	 * @return El folio UUID
	 * @throws ValidationException En caso de que no exista informacion
	 */
	public String getFolioUUID() throws ValidationException {
		if((folioUUID == null || folioUUID.length() == 0) &&
				formato == TipoFormatoEstadoCuenta.XML){
			throw new ValidationException("DY02E08");
		}
		
		return folioUUID;
	}
	/**
	 * Asigna un valor para el folio UUID
	 * @param folioUUID El folio UUID que se desea asignar
	 */
	public void setFolioUUID(String folioUUID) {
		this.folioUUID = folioUUID;
	}
	/**
	 * Obtiene el codigo de respuesta
	 * @return El codigo de respuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	/**
	 * Asigna un valor para el codigo de respuesta
	 * @param codigoRespuesta El codigo de respuesta que se desa asignar
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	/**
	 * Obtiene la descripcion de la respuesta
	 * @return La descripcion de la respuesta
	 */
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}
	/**
	 * Asigna un valor para la descripcion de la respuesta
	 * @param descripcionRespuesta La descripcion de la respuesta que se desea asignar
	 */
	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}
	
	/**
	 * Con la informacion capturada genera el mensaje de entrada para la trama DY02
	 * @return El mensaje de entrada de la trama DY02
	 * @throws ValidationException En caso de que no exista toda la informacion
	 */
	public String generarMensajeEntrada() throws ValidationException {
		final StringBuilder trama = new StringBuilder();
		
		trama
			.append(rellenar(OPCION, 1))
			.append(rellenar(getCodigoCliente(), 8, '0', 'I'))
			.append(rellenar(getCuenta(), 20, ' ', 'I'))
			.append(getIdFormato())
			.append(rellenar(getPeriodo(), 6))
			//.append(rellenar(TIPO_CUENTA, 3))
			.append(rellenar(getTipoEDC(),3, '0', 'I'))
			.append(rellenar(CANAL, 2))
			.append(rellenar(getFolioPeticion(), 10))
			.append(rellenar(getFolioOndemand(), 8, '0', 'I'))
			.append(rellenar(getFolioUUID(), 36));
		
		return agregarEncabezadoPS7(ID_TRAMA, trama);
	}
	
	/**
	 * Agrega el encabezado PS7 al mensaje de entrada
	 * @param transaccion El nombre de la transaccion
	 * @param trama El mensaje de entrada de la transaccion
	 * @return El mensaje de entrada con el encabezado PS7
	 */
	private String agregarEncabezadoPS7(String transaccion, StringBuilder trama) {
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Inicio", EIGlobal.NivelLog.INFO);
		
		final StringBuffer tramaBuffer = new StringBuffer();
		tramaBuffer.append(rellenar(NP_MQ_TERMINAL, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(ADMUSR_MQ_USUARIO, 8, ' ', 'D'));
		tramaBuffer.append(rellenar(transaccion, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(Integer.toString(trama.length() + 32), 4, '0', 'I'));
		tramaBuffer.append("1123451O00N2");
		tramaBuffer.append(trama);

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Fin", EIGlobal.NivelLog.INFO);
		return tramaBuffer.toString();
	}
	
	/**
	 * Toma el mensaje de respuesta de la trama DY02 y asigna los valores a las variables de instancia
	 * @param mensajeRespuesta El mensaje de respuesta que se desea procesar
	 */
	public void procesarMensajeRespuesta(String mensajeRespuesta){
		this.setCodigoRespuesta(mensajeRespuesta.substring(0, 7).trim());
		this.setDescripcionRespuesta(mensajeRespuesta.substring(7, 57).trim());
	}
}
