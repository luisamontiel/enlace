package mx.altec.enlace.beans;

import java.io.Serializable;



public class TranInterbancaria implements Serializable {



	public  long referencia;
	public  String usuario;
	public  String contrato;
	public  long referenciaDevol;
	public  String fchEnvio;
	public  String cuentaCargo;
	public  String cuentaAbono;
	public  double importe;
	public  String beneficiario;
	public  String estatus;
	public  String formaLiquidacion;
	public  String fchAplicacion;
	public  String conceptoPago;
	public  long referenciaInterb;
	public  long folioMancomunado;
	public  String cveRastreo;
	public  String fchDevol;
	public  String cveMotivoDevol;
	public  String desMotivoDevol;
	public  String descEstatus;
	public String  descFormaAplic;
	public String  cuentaCLABE;
	public String  rfc;
	public String iva;
	public String  titular;
	public String  banco;
	public String  plaza;
	public String  sucursal;
	public String  expEstatus;
	public String  expFormaAplica;
	private String tipoDivisa;



	public String getExpEstatus() {
		return expEstatus;
	}
	public void setExpEstatus(String expEstatus) {
		this.expEstatus = expEstatus;
	}
	public String getExpFormaAplica() {
		return expFormaAplica;
	}
	public void setExpFormaAplica(String expFormaAplica) {
		this.expFormaAplica = expFormaAplica;
	}
	public String getCuentaCLABE() {
		return validaDato(cuentaCLABE);

	}
	public void setCuentaCLABE(String cuentaCLABE) {
		this.cuentaCLABE = validaLongitud(cuentaCLABE,18);
	
	}
	public String getRfc() {
		return validaDato(rfc);
	}
	public void setRfc(String rfc) {
		this.rfc = validaLongitud(rfc,20);;
	}
	public String getIva() {
		return validaDato(iva);

	}

	public String getIvaNull() {
		if(iva!=null && iva.trim().length()==0)
			return null;
		else return iva;

	}

	public void setIva(String iva) {
	 this.iva = iva;

	}

	public String getTitular() {
		return validaDato(titular);
	}
	public void setTitular(String titular) {		
		this.titular = validaLongitud(titular,20);
		
	}
	public String getBanco() {
		return validaDato(banco);
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getPlaza() {
		return validaDato(plaza);
	}
	public void setPlaza(String plaza) {
		this.plaza = plaza;
	}
	public String getSucursal() {
		return validaDato(sucursal);
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getDescFormaAplic() {
		return validaDato(descFormaAplic);
	}
	public void setDescFormaAplic(String descFormaAplic) {
		this.descFormaAplic = descFormaAplic;
	}
	public String getDescEstatus() {
		return validaDato(descEstatus);
	}
	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}
	public String getCveRastreo() {
		return validaDato(cveRastreo);
	}
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	public String getFchDevol() {
		return fchDevol;
	}
	public void setFchDevol(String fchDevol) {
		this.fchDevol = fchDevol;
	}
	public String getCveMotivoDevol() {
		return validaDato(cveMotivoDevol);
	}
	public void setCveMotivoDevol(String cveMotivoDevol) {
		this.cveMotivoDevol = cveMotivoDevol;
	}
	public String getDesMotivoDevol() {
		return validaDato(desMotivoDevol);
	}
	public void setDesMotivoDevol(String desMotivoDevol) {
		this.desMotivoDevol = desMotivoDevol;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public String getConceptoPago() {
		return validaDato(conceptoPago);
	}
	public void setConceptoPago(String conceptoPago) {

		this.conceptoPago =validaLongitud(conceptoPago,120);
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario =validaLongitud(usuario,8);
	}
	public long getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia =  Long.valueOf(referencia.trim());
	}


	public void setReferencia(long referencia) {
	this.referencia =  referencia;

	}
	public long getReferenciaDevol() {
		return referenciaDevol;
	}
	public void setReferenciaDevol(long referenciaDevol) {
		this.referenciaDevol = referenciaDevol;
	}

	public String getFchEnvio() {
		return fchEnvio;
	}
	public void setFchEnvio(String fchEnvio) {
		this.fchEnvio = fchEnvio;
	}
	public String getCuentaCargo() {
		return validaDato(cuentaCargo);

	}
	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = validaLongitud(cuentaCargo,20);
	}
	public String getCuentaAbono() {
		return validaDato(cuentaAbono);

	}
	public void setCuentaAbono(String cuentaAbono) {		
		this.cuentaAbono = validaLongitud(cuentaAbono,20);
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		importe= validaDato(importe);
		if(importe.length()==0)
			this.importe =0;
		else
			this.importe =  Double.valueOf(importe);
	}
	public String getBeneficiario() {
		return validaDato(beneficiario);
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = validaLongitud(beneficiario,50);
	}

	public String getEstatus() {
		return estatus;
	}

	public String getDesEstatus() {
		return estatus;///
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFormaLiquidacion() {
		return validaDato(formaLiquidacion);
	}
	public void setFormaLiquidacion(String formaLiquidacion) {
		this.formaLiquidacion = formaLiquidacion;
	}
	public String getFchAplicacion() {
		return  validaDato(fchAplicacion);
	}
	public void setFchAplicacion(String fchAplicacion) {
		this.fchAplicacion = fchAplicacion;
	}

	public long getReferenciaInterb() {
		return referenciaInterb;
	}
	public void setReferenciaInterb(String referenciaInterbancaria) {
		referenciaInterbancaria=referenciaInterbancaria.trim();
		if(referenciaInterbancaria.length()>0)
		this.referenciaInterb = Long.valueOf(referenciaInterbancaria);

	}
	public void setReferenciaInterb(long referenciaInterbancaria) {

		this.referenciaInterb = Long.valueOf(referenciaInterbancaria);

	}
	public long getFolioMancomunado() {
		return folioMancomunado;
	}
	public void setFolioMancomunado(String folioRegManc) {
		folioRegManc=folioRegManc.trim();
		if(folioRegManc.length()>0)
		this.folioMancomunado = Long.valueOf(folioRegManc);
	}
	public void setFolioMancomunado(long folioRegManc) {
		this.folioMancomunado = Long.valueOf(folioRegManc);
	}


	public String validaDato(String dato)
	{
		if(dato!=null) return dato.trim();
		else return "";
	}
	
	public String validaLongitud(String dato,int longitudMaxima)	
	{
		if (dato!= null){
			dato=dato.trim();
			if(dato.length()>longitudMaxima)
				dato=dato.substring(0,longitudMaxima);
		}
		return dato;
			
	}
	/**
	 * getTipoDivisa de tipo String.
	 * @author FSW-Vector
	 * @return tipoDivisa de tipo String
	 */
	public String getTipoDivisa() {
		return tipoDivisa;
	}
	/**
	 * setTipoDivisa para asignar valor a tipoDivisa.
	 * @author FSW-Vector
	 * @param tipoDivisa de tipo String
	 */
	public void setTipoDivisa(String tipoDivisa) {
		this.tipoDivisa = tipoDivisa;
	}
}