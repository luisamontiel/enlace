/**
 * VECTOR - Bean que almacena la informacion de solicitudes de nomina
 */
package mx.altec.enlace.beans;

import java.util.Date;

public class SolPDFBean {

	public SolPDFBean() {
		codError = "";
		mensaje = "";
		tipoOper = "";
        referencia = 0;
	}	
	
	private int secuencia;
	private String tipoNomina;
	private Date fchPago;
	private Date fchAlta;
	private Date fchProceso;
	private String usuarioAlta;
	private String estatus;
	private String Observaciones;
	private String contrato;
	
	private String codError;
	private String mensaje;
	private String tipoOper;
	private int referencia;
	
	public int getReferencia() {
		return referencia;
	}
	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}
	public String getCodError() {
		return codError;
	}
	public void setCodError(String codError) {
		this.codError = codError;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getTipoOper() {
		return tipoOper;
	}
	public void setTipoOper(String tipoOper) {
		this.tipoOper = tipoOper;
	}

	
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public int getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(int secuencia) {
		this.secuencia = secuencia;
	}
	public String getTipoNomina() {
		return tipoNomina;
	}
	public void setTipoNomina(String tipoNomina) {
		this.tipoNomina = tipoNomina;
	}
	public Date getFchPago() {
		return fchPago;
	}
	public void setFchPago(Date fchPago) {
		this.fchPago = fchPago;
	}
	public Date getFchAlta() {
		return fchAlta;
	}
	public void setFchAlta(Date fchAlta) {
		this.fchAlta = fchAlta;
	}
	public Date getFchProceso() {
		return fchProceso;
	}
	public void setFchProceso(Date fchProceso) {
		this.fchProceso = fchProceso;
	}
	public String getUsuarioAlta() {
		return usuarioAlta;
	}
	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getObservaciones() {
		return Observaciones;
	}
	public void setObservaciones(String observaciones) {
		Observaciones = observaciones;
	}

	

      
}