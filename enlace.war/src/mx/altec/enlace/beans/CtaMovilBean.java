package mx.altec.enlace.beans;

public class CtaMovilBean {
	/**
	 * variable titular
	 */
	private String titular;
	
	/**
	 * variable numeroMovil
	 */
	private String ctaMovil;
	
	/**
	 * variable tipoCuenta
	 */
	private String tipoCuenta;

	/**
	 * variable cveBanco
	 */
	private String cveBanco;
	
	/**
	 * variable descBanco
	 */
	private String nombreBanco;
	
	/**
	 * variable claveBanco
	 */
	private String claveBanco;

	/**
	 * @param titular the titular to set
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}

	/**
	 * @return the titular
	 */
	public String getTitular() {
		return titular;
	}

	/**
	 * @param ctaMovil the ctaMovil to set
	 */
	public void setCtaMovil(String ctaMovil) {
		this.ctaMovil = ctaMovil;
	}

	/**
	 * @return the ctaMovil
	 */
	public String getCtaMovil() {
		return ctaMovil;
	}

	/**
	 * @param tipoCuenta the tipoCuenta to set
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * @return the tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	
	/**
	 * @param nombreBanco the nombreBanco to set
	 */
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	/**
	 * @return the nombreBanco
	 */
	public String getNombreBanco() {
		return nombreBanco;
	}


	/**

	 * @param cveBanco the cveBanco to set
	 */
	public void setCveBanco(String cveBanco) {
		this.cveBanco = cveBanco;
	}

	/**
	 * @return the cveBanco
	 */
	public String getCveBanco() {
		return cveBanco;
	}

	/**
	 * @param claveBanco the claveBanco to set
	 */
	public void setClaveBanco(String claveBanco) {
		this.claveBanco = claveBanco;


	}

	/**
	 * @return the claveBanco

	 */
	public String getClaveBanco() {
		return claveBanco;


	}
	
}
