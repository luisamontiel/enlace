/** 
*   Isban Mexico
*   Clase: CuentaInterbancariaBean.java
*   Descripcion: Objeto de trasporte de datos para administrar cuentas interbancarias.
*   
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Se agrega campo divisa. 
*/

package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * Objeto de transporte de datos de la cuenta.
 */
public class CuentaInterbancariaBean implements Serializable{

	/**
	 * Numero de version que posee cada clase Serializable 
	 * el cual es usado en la deserializacion para verificar que el emisor y el 
	 * receptor de un objeto  mantienen una compatibilidad. 
	 */
	private static final long serialVersionUID = 8137547243559458446L;
	
	
	/**
	 * variable Banco
	 */
	private String banco;

	/**
	 * variable Plaza
	 */
	private String plaza;
	
	/**
	 * variable Sucursal
	 */
	private String sucursal;
	/**
	 * variable para el contrato
	 */
	private String contrato;
	/**
	 * variable regIni
	 */
	private int regIni;
	
	/**
	 * variable regFin
	 */
	private int regFin;
	
	/**
	 * variable criterio
	 */
	private String criterio;
	

	
	/**
	 * 
	 * variable divisa
	 */
	private String divisa;
	/**
	 * totalRegistro totalRegistro
	 */
	private int totalRegistro;
	
	/**
	 * variable textoBuscar
	 */
	private String textoBuscar;
	
	/**
	 * variable Descripcion
	 */
	private String descripcion;
	
	/**
	 * variable NumeroInterbancaria
	 */
	private String numeroInterbancaria;
	
	/**
	 * Constructor cuenta interbancaria 
	 * 
	 * @param descripcion descripcion de la cuenta
	 * @param numeroInterbancaria numero de cuenta interbancaria
	 * @param banco  banco 
	 * @param plaza  plaza
	 * @param sucursal sucursal bancaria
	 * @param divisa  divisa
	 */
	public CuentaInterbancariaBean(String descripcion, String numeroInterbancaria, String banco, String plaza, String sucursal, String divisa) {
		super();
		this.descripcion = descripcion;
		this.numeroInterbancaria = numeroInterbancaria;
		this.banco = banco;
		this.plaza = plaza;
		this.sucursal = sucursal;
		this.divisa=divisa;
		
	}
	
	/**
     * Constructor por defecto.
     */
    public CuentaInterbancariaBean(){
    }
	
	/**
	 * obtiene el valor totalRegistro
	 * @return totalRegistro
	 */
	public int getTotalRegistro() {
		return totalRegistro;
	}
	
	/**
	 * Asigna el total de registros
	 * @param totalRegistro : total de registros
	 */
	public void setTotalRegistro(int totalRegistro) {
		this.totalRegistro = totalRegistro;
	}
	/**
	 * getContrato Contrato
	 * @return contrato
	 */
	public String getContrato() {
		return contrato;
	}
	
	/**
	 * setContrato Contrato
	 * @param contrato : contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * obtiene Descripcion
	 * @return Descripcion : Descripcion
	 */	
	public String getdescripcion() {
		return descripcion;
	}

	/**
	 * Asigna el valor de la descripcion
	 * @param descripcion : descripcion
	 */
	public void setdescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * obtiene NumeroInterbancaria
	 * @return NumeroInterbancaria : NumeroInterbancaria
	 */
	/**
	 * obtiene numero de cuenta interbancaria
	 * @return numeroInterbancaria
	 */
	public String getnumeroInterbancaria() {
		return numeroInterbancaria;
	}

	/**
	 * asigna numeroInterbancaria
	 * @param numeroInterbancaria : numeroInterbancaria
	 */
	public void setnumeroInterbancaria(String numeroInterbancaria) {
		this.numeroInterbancaria = numeroInterbancaria;
	}

	/**
	 * obtiene banco
	 * @return banco : banco
	 */
	public String getbanco() {
		return banco;
	}

	/**
	 * asigna banco
	 * @param banco : banco
	 */
	public void setbanco(String banco) {
		this.banco = banco;
	}

	/**
	 * obtiene plaza
	 * @return plaza : plaza
	 */
	public String getplaza() {
		return plaza;
	}

	/**
	 * asigna plaza
	 * @param plaza : plaza
	 */
	public void setplaza(String plaza) {
		this.plaza = plaza;
	}

	/**
	 * obtiene sucursal
	 * @return sucursal : sucursal
	 */
	public String getsucursal() {
		return sucursal;
	}

	/**
	 * asigna sucursal
	 * @param sucursal : sucursal
	 */
	public void setsucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * Obtiene Registro inicial
	 * 
	 * @return regIni : registro inicial
	 */
	public int getRegIni() {
		return regIni;
	}

	/**
	 * Asigna el Registro inicial
	 * 
	 * @param regIni : regIni
	 */
	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}

	/**
	 * Obtienen el registro final
	 * 
	 * @return regFin : registro final
	 */
	public int getRegFin() {
		return regFin;
	}

	/**
	 * Asigna registro final
	 * 
	 * @param regFin : registro final
	 */
	public void setRegFin(int regFin) {
		this.regFin = regFin;
	}

	/**
	 * Obtiene el criterio
	 * 
	 * @return criterio : criterio
	 */
	public String getCriterio() {
		return criterio;
	}

	/**
	 * asigna el criterio
	 * 
	 * @param criterio : criterio
	 */
	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}

	/**
	 * Obtiene el texo de busqueda
	 * 
	 * @return textoBuscar
	 */
	public String getTextoBuscar() {
		return textoBuscar;
	}

	/**
	 * Asigna el texto de busqueda
	 * 
	 * @param textoBuscar : texto de busqueda
	 */
	public void setTextoBuscar(String textoBuscar) {
		this.textoBuscar = textoBuscar;
	}
	
	/**
	 * obtiene el valor de la divisa.
	 * 
	 * @return divisa
	 */
	public String getDivisa() {
		return divisa;
	}
	
	/**
	 * Asigna el valor de la divisa.
	 * 
	 * @param divisa detalle divisa
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}
	
	
	
}
