package mx.altec.enlace.beans;

public class CuentaMovilBean {
	
	/**
	 * variable titular
	 */
	private String titular;
	
	/**
	 * variable numeroMovil
	 */
	private String numeroMovil;
	
	/**
	 * variable tipoCuenta
	 */
	private String tipoCuenta;

	/**
	 * variable cveBanco
	 */
	private String cveBanco;
	
	/**
	 * variable descBanco
	 */
	private String descBanco;
	
	/**
	 * variable totalRegistros
	 */
	private int totalRegistros;

	/**
	 * obtiene titular
	 * @return titular : titular
	 */	
	public String getTitular() {
		return titular;
	}

	/**
	 * asigna titular
	 * @param titular : titular
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}

	/**
	 * obtiene numeroMovil
	 * @return numeroMovil : numeroMovil
	 */
	public String getNumeroMovil() {
		return numeroMovil;
	}

	/**
	 * asigna numeroMovil
	 * @param numeroMovil : numeroMovil
	 */
	public void setNumeroMovil(String numeroMovil) {
		this.numeroMovil = numeroMovil;
	}

	/**
	 * obtiene tipoCuenta
	 * @return tipoCuenta : tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * asigna tipoCuenta
	 * @param tipoCuenta : tipoCuenta
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * obtiene cveBanco
	 * @return cveBanco : cveBanco
	 */
	public String getCveBanco() {
		return cveBanco;
	}

	/**
	 * asigna cveBanco
	 * @param cveBanco : cveBanco
	 */
	public void setCveBanco(String cveBanco) {
		this.cveBanco = cveBanco;
	}

	/**
	 * obtiene descBanco
	 * @return descBanco : descBanco
	 */
	public String getDescBanco() {
		return descBanco;
	}

	/**
	 * asigna descBanco
	 * @param descBanco : descBanco
	 */
	public void setDescBanco(String descBanco) {
		this.descBanco = descBanco;
	}

	/**
	 * obtiene totalRegistros
	 * @return totalRegistros : totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}

	/**
	 * asigna totalRegistros
	 * @param totalRegistros : totalRegistros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
}
