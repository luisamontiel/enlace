/**
 * 
 */
package mx.altec.enlace.gwt.adminlym.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 */
public class LimitesUsuario implements IsSerializable {

	private String CodCliente = "";
	private String Nombre     = "";
	private ArrayList<LimitesOperacion> ListaLimitesOperacion = new ArrayList<LimitesOperacion>();
	/**
	 * @return el codCliente
	 */
	public String getCodCliente() {
		return CodCliente;
	}
	/**
	 * @param codCliente el codCliente a establecer
	 */
	public void setCodCliente(String codCliente) {
		CodCliente = codCliente;
	}
	/**
	 * @return el nombre
	 */
	public String getNombre() {
		return Nombre;
	}
	/**
	 * @param nombre el nombre a establecer
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	/**
	 * @return el listaLimitesOperacion
	 */
	public ArrayList<LimitesOperacion> getListaLimitesOperacion() {
		return ListaLimitesOperacion;
	}
	/**
	 * @param listaLimitesOperacion el listaLimitesOperacion a establecer
	 */
	public void setListaLimitesOperacion(
			ArrayList<LimitesOperacion> listaLimitesOperacion) {
		ListaLimitesOperacion = listaLimitesOperacion;
	}
	
	
}
