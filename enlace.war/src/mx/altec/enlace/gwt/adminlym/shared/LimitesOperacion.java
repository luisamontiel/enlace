/**
 * 
 */
package mx.altec.enlace.gwt.adminlym.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 */
public class LimitesOperacion  implements IsSerializable{
	
	private String opcion       		= "";
	private String contrato       		= "";
	private String usuario       		= "";
	private String nombreUsuario   		= "";
	private String estado               = "";
	private String ClaveOperacion       = "";
	private String DescripcionOperacion = "";
	private String LimiteOperacion      = "";
	
	/**
	 * @return el claveOperacion
	 */
	public String getClaveOperacion() {
		return ClaveOperacion;
	}
	/**
	 * @param claveOperacion el claveOperacion a establecer
	 */
	public void setClaveOperacion(String claveOperacion) {
		ClaveOperacion = claveOperacion;
	}
	/**
	 * @return el descripcionOperacion
	 */
	public String getDescripcionOperacion() {
		return DescripcionOperacion;
	}
	/**
	 * @param descripcionOperacion el descripcionOperacion a establecer
	 */
	public void setDescripcionOperacion(String descripcionOperacion) {
		DescripcionOperacion = descripcionOperacion;
	}
	/**
	 * @return el limiteOperacion
	 */
	public String getLimiteOperacion() {
		return LimiteOperacion;
	}
	/**
	 * @param limiteOperacion el limiteOperacion a establecer
	 */
	public void setLimiteOperacion(String limiteOperacion) {
		LimiteOperacion = limiteOperacion;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
