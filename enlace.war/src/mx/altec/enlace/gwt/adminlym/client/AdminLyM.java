package mx.altec.enlace.gwt.adminlym.client;

import java.util.ArrayList;
import java.util.HashMap;

import mx.altec.enlace.gwt.adminlym.shared.BeanAdminLyM;
import mx.altec.enlace.gwt.adminlym.shared.GrupoOperacion;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;
import mx.altec.enlace.gwt.adminlym.shared.LimitesUsuario;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Progressbar;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.grid.events.CellSavedEvent;
import com.smartgwt.client.widgets.grid.events.CellSavedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;


public class AdminLyM implements EntryPoint {

	private  VLayout                  VStackGeneralLyM        = new VLayout();
	private  SectionStack             SectionGeneralLyM       = new SectionStack();	
	private  SectionStackSection      SectionLimiteContrato   = new SectionStackSection("Limite contrato"); 		
	private  SectionStackSection      SectionLimitesUsuarios  = new SectionStackSection("Limites por usuario"); 		
	private  Image                    BotonRegresar                = new Image("/gifs/EnlaceMig/gbo25320.gif");
	private  Image                    BotonContinuar               = new Image("/gifs/EnlaceMig/gbo25310.gif");
	
	private final AdminLyMServiceAsync     AdminLyMService         = GWT.create(AdminLyMService.class);
	private       BeanAdminLyM             BeanAdmin               = new BeanAdminLyM(); 
	private       String                   URLServlet              = "/enlaceMig/AdmonLyMServlet";
	private       String                   URLOrigen               = "";
	private       String                   URLDestino              = "";
	private       String                   URLError                = "";
	private       String                   OperacionSolicitada     = "";
	final public static String             VISTA_DETALLE            = "detalle";
	final public static String             VISTA_DESGLOSE           = "desglose";
	public static String                   VistaMostrar             = VISTA_DETALLE;
	
	final private  TextItem           LimiteContrato          = new TextItem("limiteContrato", "Limite");
	private  CheckboxItem             LimiteContratoActivado  = new CheckboxItem("limiteContratoActivado", "Activo");
	private  Image                    BotonAgregarMonto       = new Image("/gifs/EnlaceMig/gbo25290.gif");
	private  Image                    BotonEliminarMonto      = new Image("/gifs/EnlaceMig/gbo25540.gif");
	
	final private  ListGrid           GridLimitesUsurios      = new ListGrid();	
	private HashMap<String, String>   CatalogoUsuarios        = new HashMap<String, String>();  	
	private HashMap<String, String>   CatalogoUsuarios2       = new HashMap<String, String>();  	
	private HashMap<String, String>   CatalogoOperaciones     = new HashMap<String, String>();  	

	private HLayout                   lobjLayoutBotonesMontos = new HLayout();
	private HLayout                   lobjLayoutBotonesNavegacion = new HLayout();
	
    private  VLayout                  LayoutCarga             = new VLayout();
    private  Label                    LabelCarga              = new Label("Cargando...");
    private  Progressbar              ProgressBarCarga        = new Progressbar();
    private  int                      PorcentajeCarga         = 0;
	
	public void onModuleLoad() {

		

		try{
			System.out.println("Bienvenido a AdminLyM...");
			
			URLServlet          = DOM.getElementById("URLServlet")         !=null?DOM.getElementById("URLServlet").getAttribute("value"):URLServlet;
			URLOrigen           = DOM.getElementById("URLOrigen")          !=null?DOM.getElementById("URLOrigen").getAttribute("value"):URLOrigen;
			URLDestino          = DOM.getElementById("URLDestino")         !=null?DOM.getElementById("URLDestino").getAttribute("value"):URLDestino;
			URLError            = DOM.getElementById("URLError")           !=null?DOM.getElementById("URLError").getAttribute("value"):URLError;
			OperacionSolicitada = DOM.getElementById("OperacionSolicitada")!=null?DOM.getElementById("OperacionSolicitada").getAttribute("value"):OperacionSolicitada;
			VistaMostrar        = DOM.getElementById("VistaMostrar")       !=null?DOM.getElementById("VistaMostrar").getAttribute("value"):VistaMostrar;
			
			System.out.println("URLServlet          :" + URLServlet);
			System.out.println("URLOrigen           :" + URLOrigen);
			System.out.println("URLDestino          :" + URLDestino);
			System.out.println("URLError            :" + URLError);
			System.out.println("OperacionSolicitada :" + OperacionSolicitada);
			System.out.println("OperacionSolicitada :" + OperacionSolicitada);
		}
		catch(Exception e){
			e.printStackTrace();
		}

		pintaModulo(VistaMostrar);
		
	}

	private void pintaModulo(String pstrVista){
		
		System.out.println("Pintando vista       :" + pstrVista);

		VStackGeneralLyM.setAlign(Alignment.CENTER);
		VStackGeneralLyM.setWidth(750);
		VStackGeneralLyM.setHeight(670);
		
		
		pintaTabLimiteContrato(pstrVista);
		pintaTabLimitesUsuarios(pstrVista);
		
		SectionGeneralLyM.setVisibilityMode(VisibilityMode.MULTIPLE);
		SectionGeneralLyM.addSection(SectionLimiteContrato);
		SectionGeneralLyM.addSection(SectionLimitesUsuarios);
		
		//VLayout lobjLayoutSecciones = new VLayout();
		//lobjLayoutSecciones.addMember(SectionGeneralLyM);

		
		lobjLayoutBotonesMontos.setHeight(40);
		//lobjLayoutBotonesMontos.setWidth(750);
		lobjLayoutBotonesMontos.setMembersMargin(20);
		lobjLayoutBotonesMontos.setAlign(Alignment.RIGHT);
		lobjLayoutBotonesMontos.addMember(BotonAgregarMonto);
		lobjLayoutBotonesMontos.addMember(BotonEliminarMonto);
		
		lobjLayoutBotonesNavegacion.setHeight(40);
		//lobjLayoutBotonesNavegacion.setWidth(750);
		lobjLayoutBotonesNavegacion.setMembersMargin(20);
		lobjLayoutBotonesNavegacion.setAlign(Alignment.CENTER);
		if(VistaMostrar.equals(VISTA_DESGLOSE)){
			lobjLayoutBotonesNavegacion.addMember(BotonRegresar);
		}
		lobjLayoutBotonesNavegacion.addMember(BotonContinuar);
		
		//VStackGeneralLyM.addMember(lobjLayoutSecciones);
		/*
		VStackGeneralLyM.addMember(SectionGeneralLyM);
		
		
		if(VistaMostrar.equals(VISTA_DETALLE)){
			VStackGeneralLyM.setMembersMargin(20);
			VStackGeneralLyM.addMember(lobjLayoutBotonesMontos);
			VStackGeneralLyM.addMember(lobjLayoutBotonesNavegacion);
		}
		*/
		
		BotonAgregarMonto.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				System.out.println("Agregar...");
				
				AltaOperacion.setListaOperaciones(CatalogoOperaciones);
				AltaOperacion.setListaUsuarios(CatalogoUsuarios2);
				AltaOperacion.setGridLimitesUsurios(GridLimitesUsurios);
				AltaOperacion.setBeanAdmin(BeanAdmin);
				AltaOperacion.showPanelAlta();
				
			}
			
		});

		BotonEliminarMonto.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				System.out.println("Eliminar...");
				
				if(GridLimitesUsurios.getSelectedRecord()!=null){
					
					final ListGridRecord record = GridLimitesUsurios.getSelectedRecord();
					
					if(record.getAttribute("estado").equals("Eliminado")){
						SC.say("La acci&oacute;n no se puede realizar debido a que el registro ya fue marcado previamente como eliminado...");
					}
					else if(record.getAttribute("estado").equals("")){
						SC.say("La acci&oacute;n no se puede realizar debido a que el registro no es valido...");
					}
					else{
						SC.ask("Eliminar Monto", "Seguro que desea eliminar el monto", new BooleanCallback(){
							public void execute(Boolean value) {
								if(value.booleanValue()){

									
									System.out.println("Registro eliminado...");

									System.out.println("Buscando Usuario ["+ record.getAttribute("usuario") + "] en BeanAdmin para ser eliminado...");
									ArrayList<LimitesUsuario> lista = BeanAdmin.getListaLimitesContratos();
									for(int i=0;i<lista.size();i++){
										LimitesUsuario LimiteUsuarioTmp = lista.get(i);
										if(LimiteUsuarioTmp.getCodCliente().equals(record.getAttribute("usuario"))){
											System.out.println("Usuario encontrado[" + LimiteUsuarioTmp.getCodCliente() + "]...");
											System.out.println("Nombre Usuario encontrado[" + LimiteUsuarioTmp.getNombre() + "]...");
											System.out.println("Numero de Operaciones:" + LimiteUsuarioTmp.getListaLimitesOperacion().size());


											LimitesOperacion limiteTemp = new LimitesOperacion();
											limiteTemp.setClaveOperacion(record.getAttribute("claveOper"));
											limiteTemp.setContrato(BeanAdmin.getLimitesContrato().getContrato());
											limiteTemp.setDescripcionOperacion(record.getAttribute("descOper").trim());
											limiteTemp.setLimiteOperacion(record.getAttribute("monto"));
											limiteTemp.setOpcion(BeanAdmin.getLimitesContrato().getOpcion());
											limiteTemp.setUsuario(record.getAttribute("usuario"));
											limiteTemp.setNombreUsuario(LimiteUsuarioTmp.getNombre());
											limiteTemp.setEstado("Eliminado");
											
											

											BeanAdmin.getListaLimitesEliminados().add(limiteTemp);
											
											/**
											 * Si el registro fue agregado se elimina de la lista
											 * de operaciones
											 */
											if(record.getAttribute("estado").equals("Agregado")){
												for(int iR=0;iR<LimiteUsuarioTmp.getListaLimitesOperacion().size();iR++){
													if(LimiteUsuarioTmp.getListaLimitesOperacion().get(iR).getClaveOperacion().equals(record.getAttribute("claveOper"))){
														LimiteUsuarioTmp.getListaLimitesOperacion().remove(iR);
														break;
													}
												}
												
												
												/**
												 *Comprobando que no sea el unico registro
												 *En caso de serlo se debe de crear un registro pivote
												 */
												int iRecord=0;
												
												ListGridRecord listaRegistros[] = GridLimitesUsurios.getRecords();
												for(int iReg=0;iReg<listaRegistros.length;iReg++){
													ListGridRecord recorAux = listaRegistros[iReg];
													if(recorAux.getAttribute("usuario").equals(record.getAttribute("usuario"))){
														iRecord++;
													}
												}
												
												if(iRecord==0){
													ListGridRecord record = new ListGridRecord();
													record.setAttribute("pk"       , record.getAttribute("pk"));
													record.setAttribute("usuario"  , record.getAttribute("usuario"));
													record.setAttribute("nombre"   , record.getAttribute("nombre"));
													record.setAttribute("descOper" , "");
													record.setAttribute("claveOper", "");
													record.setAttribute("monto"    , "");
													record.setAttribute("estado"   , "");
													GridLimitesUsurios.addData(record);
													System.out.println("Registro agregado[" + record.toString()+ "]...");													
												}
												GridLimitesUsurios.removeData(record);
												
											}
											else{
												record.setAttribute("estado", "Eliminado");
												
												/**
												 * Actualizando estado del registro
												 */
												ArrayList<LimitesOperacion> listaLimitesOper = LimiteUsuarioTmp.getListaLimitesOperacion();											
												for(int iLimites=0;iLimites<listaLimitesOper.size();iLimites++){
													LimitesOperacion oper = listaLimitesOper.get(iLimites);
													if(oper.getClaveOperacion().equals(record.getAttribute("claveOper"))){
														oper.setEstado("Eliminado");
														break;
													}
												}
												
												GridLimitesUsurios.refreshFields();
											}

											/**
											 * Actualizando estado del registro
											 */
											ArrayList<LimitesOperacion> listaLimitesOper = LimiteUsuarioTmp.getListaLimitesOperacion();											

											
											break;
										}
									}

								}
							}

						});
					}
				}
				else{
					SC.say("Primero es necesario seleccionar un monto");
				}
				
			}
			
		});
		
		
		BotonRegresar.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				System.out.println("Regresar->onMouseDown[Enviar Peticion de regreso]...");
				VStackGeneralLyM.setDisabled(true);
				previus();
				
			}
			
		});

		final String vista = VistaMostrar;
	    BotonContinuar.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				System.out.println("Continuar->onMouseOut[Enviar Peticion de continuar]...");
				
				VStackGeneralLyM.setDisabled(true);
				
				if(vista.equals(VISTA_DETALLE)){
					LimiteContrato.updateState();
					GridLimitesUsurios.updateHover();
					GridLimitesUsurios.saveAllEdits();
					GridLimitesUsurios.focus();

					new Timer(){
						public void run(){
							if(actualizaBean())
								next();

						}

					}.schedule(1000);

				}
				else{
					next();
				}

			}
			
		});
	    
	    
		
	    //VStackGeneralLyM.draw();
	    
	    //init(pstrVista);	
	    
	    LabelCarga.setHeight(16);
	    
	    ProgressBarCarga.setHeight(10);
	    ProgressBarCarga.setWidth(300);
	    ProgressBarCarga.setVertical(false);
	    ProgressBarCarga.setPercentDone(PorcentajeCarga);
	    
	    //LayoutCarga.setAlign(Alignment.CENTER);
	    LayoutCarga.setMargin(20);
	    LayoutCarga.addMember(LabelCarga);
	    LayoutCarga.addMember(ProgressBarCarga);
	    
	    VStackGeneralLyM.addMember(LayoutCarga);
	    
	    VStackGeneralLyM.draw();
	    
	    PorcentajeCarga = 0;
	    ProgressBarCarga.setPercentDone(PorcentajeCarga);
		LabelCarga.setContents("Cargando " + PorcentajeCarga + "%...");
		init(pstrVista);	
	}
	
	private boolean actualizaBean(){
		
		//LimiteContrato.updateState();
		//GridLimitesUsurios.updateHover();
		//GridLimitesUsurios.saveAllEdits();
		//GridLimitesUsurios.focus();
		
		/**
		 * Validando si algun valor esta vacio
		 */
		
		if((LimiteContrato.getValue()==null || LimiteContrato.getValue().equals("")) && ((Boolean)LimiteContratoActivado.getValue()).booleanValue()==true){
			LimiteContrato.focusInItem();
			SC.say("El monto del contrato tiene un valor no definido, favor de intoducir un valor igual o mayor a 0...");
			VStackGeneralLyM.setDisabled(false);
			return false;
		}
		
		
		ArrayList<LimitesUsuario> limites = BeanAdmin.getListaLimitesContratos();
		ArrayList<LimitesUsuario> limitesAux = new ArrayList<LimitesUsuario>();
		
		/**
		 * Onteniendo vista final de operaciones 
		 * [solo registros en grid como agregados, modificados o consultados] 
		 */
		for(int iLimites = 0; iLimites<limites.size();iLimites++){
			LimitesUsuario limiteUsrAux = limites.get(iLimites);
			if(limiteUsrAux.getListaLimitesOperacion().size()>1){
				limitesAux.add(limiteUsrAux);
			}
			else if(limiteUsrAux.getListaLimitesOperacion().size()==1){
				if(!limiteUsrAux.getListaLimitesOperacion().get(0).getClaveOperacion().equals(""))
					limitesAux.add(limiteUsrAux);
			}
		}
		
		BeanAdmin.getListaLimitesFinal().clear();
		for(int iFinales=0;iFinales<limitesAux.size();iFinales++){
			LimitesUsuario limiteUsrAux = limitesAux.get(iFinales);
			for(int iLimites=0;iLimites<limiteUsrAux.getListaLimitesOperacion().size();iLimites++){
				
				if(limiteUsrAux.getListaLimitesOperacion().get(iLimites).getLimiteOperacion()==null ||
				   limiteUsrAux.getListaLimitesOperacion().get(iLimites).getLimiteOperacion().equals("")){
					SC.say("El monto de la operacion tiene un valor no definido, favor de intoducir un valor igual o mayor a 0...");
					VStackGeneralLyM.setDisabled(false);
					return false;
				}
				
				
				BeanAdmin.getListaLimitesFinal().add(limiteUsrAux.getListaLimitesOperacion().get(iLimites));
			}
		}
		
		
		/**
		 * Validando si alguna operaci�n agregada 
		 * no fue eliminada posteriormente 
		 */
		
		ArrayList<LimitesOperacion> inconsistentesArg = new ArrayList<LimitesOperacion>(); 

		for(int iAgregados=0;iAgregados<BeanAdmin.getListaLimitesAgregados().size();iAgregados++){
			LimitesOperacion limiteAgregado = BeanAdmin.getListaLimitesAgregados().get(iAgregados);
			boolean limiteEliminado = false;
			
			for(int iElim=0;iElim< BeanAdmin.getListaLimitesEliminados().size();iElim++){
				LimitesOperacion limiteElim = BeanAdmin.getListaLimitesEliminados().get(iElim);
				try{
					if(limiteElim.getContrato().equals(limiteAgregado.getContrato()) &&
							limiteElim.getUsuario().equals(limiteAgregado.getUsuario())   &&
							limiteElim.getClaveOperacion().equals(limiteAgregado.getClaveOperacion())){
						limiteEliminado = true;
						break;
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			if(limiteEliminado)
				inconsistentesArg.add(limiteAgregado);
		}
		
		for(int iIncon=0;iIncon<inconsistentesArg.size();iIncon++){
			BeanAdmin.getListaLimitesAgregados().remove(inconsistentesArg.get(iIncon));
		}

		/**
		 * Validando si alguna operaci�n modificada 
		 * no fue eliminada posteriormente 
		 */
		ArrayList<LimitesOperacion> inconsistentesMod = new ArrayList<LimitesOperacion>(); 
		
		for(int iMod=0;iMod<BeanAdmin.getListaLimitesModificados().size();iMod++){
			LimitesOperacion limiteMod = BeanAdmin.getListaLimitesModificados().get(iMod);
			boolean limiteEliminado = false;
			
			for(int iCon=0;iCon< BeanAdmin.getListaLimitesEliminados().size();iCon++){
				LimitesOperacion limiteEli = BeanAdmin.getListaLimitesEliminados().get(iCon);
				try{
					if(limiteEli.getContrato().equals(limiteMod.getContrato()) &&
							limiteEli.getUsuario().equals(limiteMod.getUsuario())   &&
							limiteEli.getClaveOperacion().equals(limiteMod.getClaveOperacion())){
						limiteEliminado = true;
						break;
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
			if(limiteEliminado)
				inconsistentesMod.add(limiteMod);
			
		}
		
		for(int iIncon=0;iIncon<inconsistentesMod.size();iIncon++){
			BeanAdmin.getListaLimitesModificados().remove(inconsistentesMod.get(iIncon));
		}
		
		
		/**
		 * Solo se pueden eliminar operaciones consultadas por tal raz�n
		 * se valida que las operaci�n eliminada hayan sido consultadas y
		 * no provengan de operaciones agregadas
		 */
		
		ArrayList<LimitesOperacion> inconsistentesElim = new ArrayList<LimitesOperacion>(); 
		for(int iElim=0;iElim<BeanAdmin.getListaLimitesEliminados().size();iElim++){
			LimitesOperacion limiteDel = BeanAdmin.getListaLimitesEliminados().get(iElim);
			boolean limiteConsultado = false;
			
			for(int iCon=0;iCon< BeanAdmin.getListaLimitesConsultados().size();iCon++){
				LimitesOperacion limiteCon = BeanAdmin.getListaLimitesConsultados().get(iCon);
				try{
					if(limiteCon.getContrato().equals(limiteDel.getContrato()) &&
							limiteCon.getUsuario().equals(limiteDel.getUsuario())   &&
							limiteCon.getClaveOperacion().equals(limiteDel.getClaveOperacion())){
						limiteConsultado = true;
						break;
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
}
			
			if(!limiteConsultado)
				inconsistentesElim.add(limiteDel);
		}
		
		for(int iIncon=0;iIncon<inconsistentesElim.size();iIncon++){
			BeanAdmin.getListaLimitesEliminados().remove(inconsistentesElim.get(iIncon));
		}
		

		/**
		 * Solo se pueden modificar operaciones consultadas por tal raz�n
		 * se valida que las operaci�n modificada hayan sido consultadas ya
		 * que las operaciones agregadas que son modificadas se quedan como
		 * agregadas
		 */
		
		ArrayList<LimitesOperacion> inconsistentesMod2 = new ArrayList<LimitesOperacion>();
		
		for(int iMod=0;iMod<BeanAdmin.getListaLimitesModificados().size();iMod++){
			LimitesOperacion limiteMod = BeanAdmin.getListaLimitesModificados().get(iMod);
			boolean limiteConsultado = false;
			
			for(int iCon=0;iCon< BeanAdmin.getListaLimitesConsultados().size();iCon++){
				LimitesOperacion limiteCon = BeanAdmin.getListaLimitesConsultados().get(iCon);
				try{
					if(limiteCon.getContrato().equals(limiteMod.getContrato()) &&
							limiteCon.getUsuario().equals(limiteMod.getUsuario())   &&
							limiteCon.getClaveOperacion().equals(limiteMod.getClaveOperacion())){
						limiteConsultado = true;
						break;

					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(!limiteConsultado)
				inconsistentesMod2.add(limiteMod);
			
			
		}
		
		for(int iIncon=0;iIncon<inconsistentesMod2.size();iIncon++){
			BeanAdmin.getListaLimitesModificados().remove(inconsistentesMod2.get(iIncon));
		}
		
		return true;
	}
	
	private void next(){
		
		
		AdminLyMService.setBean(BeanAdmin, new AsyncCallback() {

			public void onFailure(Throwable caught) {
				System.out.println("Error al solicitar informacion al servidor");
				VStackGeneralLyM.setDisabled(false);
			 	String lstrURLRedirec = URLServlet + "?" +
                                        "Origen="  + URLOrigen  + "&" + 
                                        "Destino=" + URLError + "&"  +  
										"Operacion="  + OperacionSolicitada;
			 	//Window.alert("Next[Error]->Reenviando a:" + GWT.getModuleBaseURL() + lstrURLRedirec);
 				Window.open(lstrURLRedirec, "_parent", ""); 
			}
			
			public void onSuccess(Object result){
				System.out.println("Regreso del guardar...");
				VStackGeneralLyM.setDisabled(false);
			 	String lstrURLRedirec = URLServlet   + "?" +
			 							"Origen="    + URLOrigen  + "&" + 
			 							"Destino="   + URLDestino + "&"  +  
										"Operacion=" + OperacionSolicitada;
			 	//Window.alert("Next[Exitoso]->Reenviando a:" + GWT.getModuleBaseURL() + lstrURLRedirec);
                Window.open(lstrURLRedirec, "_parent", ""); 
			}
		});		
	}
	
	private void previus(){
		
		if (URLOrigen.equals("/jsp/adminlym/desgloseLyM.jsp")) {
			URLDestino = "/jsp/adminlym/adminlym.jsp";
		}
		
	 	String lstrURLRedirec = URLServlet + "?" +
								"Origen="  + URLOrigen  + "&" + 
								"Destino=" + URLDestino + "&" +
								"Operacion=" + OperacionSolicitada;
								
	 	Window.open(lstrURLRedirec, "_parent", "");
	 	
	 	
	}
	

	private void pintaTabLimiteContrato(String pstrVista){

		LimiteContrato.setKeyPressFilter("[0-9.]");
		LimiteContrato.setDisabled(new Boolean(true));		
		LimiteContratoActivado.addChangedHandler(new ChangedHandler(){

			public void onChanged(ChangedEvent event) {
				System.out.println("Valor cambio:" + event.getValue().toString());
				LimiteContrato.setDisabled(!((Boolean) event.getValue()));
				BeanAdmin.getLimitesContrato().setLimiteActivo(((Boolean) event.getValue()).booleanValue());
			}
			
		});
		
		LimiteContrato.addChangedHandler(new ChangedHandler(){

			public void onChanged(ChangedEvent event) {
				if(event.getValue()!=null){
					System.out.println("Valor Limite cambio:" + event.getValue().toString());
					BeanAdmin.getLimitesContrato().setMonto(event.getValue().toString());
				}
				else
					BeanAdmin.getLimitesContrato().setMonto("");
			}
			
		});
		
		DynamicForm form  = new DynamicForm();
		DynamicForm form2 = new DynamicForm();
		
		form.setItems(LimiteContrato);
		form2.setItems(LimiteContratoActivado);
		
		HLayout lobjLayout = new HLayout();
		lobjLayout.setHeight(60);
		lobjLayout.addMember(form);
		lobjLayout.addMember(form2);
		lobjLayout.setAlign(Alignment.CENTER);
		
		SectionLimiteContrato.addItem(lobjLayout);
		SectionLimiteContrato.setExpanded(true);  
		
	}

	private void pintaTabLimitesUsuarios(String pstrVista){

		GridLimitesUsurios.setCanEdit(true);  
		GridLimitesUsurios.setShowAllRecords(true);  
		GridLimitesUsurios.setCellHeight(22);
		GridLimitesUsurios.setWidth(750);
		//GridLimitesUsurios.setHeight(550);
		GridLimitesUsurios.setCanEdit(true);
		GridLimitesUsurios.setEditEvent(ListGridEditEvent.DOUBLECLICK);   
		GridLimitesUsurios.setGroupStartOpen(GroupStartOpen.ALL);   
		GridLimitesUsurios.setGroupByField("usuario");
		GridLimitesUsurios.setDataSource(MontosUsuariosDS.getInstance());
		//GridLimitesUsurios.set
		GridLimitesUsurios.setSortFieldAscendingText("Ordenar Asendentemente");
		GridLimitesUsurios.setSortFieldDescendingText("Ordenar Desendentemente");
		GridLimitesUsurios.setGroupByText("Agrupar por Usuario");
		GridLimitesUsurios.setUngroupText("Desagrupar");
		GridLimitesUsurios.setFreezeFieldText("Congelar Usuario");
		GridLimitesUsurios.setEmptyMessage("No hay elementos para mostrar");

		
		ListGridField FieldUsuario   = new ListGridField("usuario"   , "Usuario");   
		ListGridField FieldNombre    = new ListGridField("nombre"    , "Nombre");   
		ListGridField FieldDescOper  = new ListGridField("descOper"  , "Operacion");   
		ListGridField FieldMonto     = new ListGridField("monto"     , "Monto Autorizado");
		ListGridField FieldEstado    = new ListGridField("estado"     , "Estado");
		
		FieldUsuario.setCanEdit(false);
		FieldNombre.setCanEdit(false);
		FieldDescOper.setCanEdit(false);
		FieldEstado.setCanEdit(false);
		
		if(pstrVista.equals(VISTA_DETALLE))
			FieldMonto.setCanEdit(true);
		else
			FieldMonto.setCanEdit(false);
		
		FieldMonto.setType(ListGridFieldType.FLOAT);
		FieldMonto.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            NumberFormat nf = NumberFormat.getFormat("0,000,000.00");
	            if(value == null) return null;
	            String val = null;
	            try {
	                val = nf.format(((Number) value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	            return val + "km&sup2";
	        }
	    });		
		
		GridLimitesUsurios.setFields(FieldUsuario,FieldNombre,FieldDescOper, FieldEstado,FieldMonto);
		GridLimitesUsurios.setAutoFetchData(true);
		
		GridLimitesUsurios.addCellSavedHandler(new CellSavedHandler(){

			
			public void onCellSaved(CellSavedEvent event) {
				// TODO Ap�ndice de m�todo generado autom�ticamente
				System.out.println("onSelectionChanged...");
				
				ListGridRecord record = (ListGridRecord) event.getRecord();
				
				
				System.out.println("Buscando Usuario ["+ record.getAttribute("usuario") + "] en BeanAdmin para ser modificado...");
				ArrayList<LimitesUsuario> lista = BeanAdmin.getListaLimitesContratos();
				for(int i=0;i<lista.size();i++){
					LimitesUsuario LimiteUsuarioTmp = lista.get(i);
					if(LimiteUsuarioTmp.getCodCliente().equals(record.getAttribute("usuario"))){
						
						if(record.getAttribute("estado").equals("Eliminado")){
							GridLimitesUsurios.cancelEditing();
							GridLimitesUsurios.refreshFields();
							break;
						}
						
						System.out.println("Usuario encontrado[" + LimiteUsuarioTmp.getCodCliente() + "]...");
						System.out.println("Nombre Usuario encontrado[" + LimiteUsuarioTmp.getNombre() + "]...");
						System.out.println("Numero de Operaciones:" + LimiteUsuarioTmp.getListaLimitesOperacion().size());
						System.out.println("Monto:" + record.getAttribute("monto"));
						LimitesOperacion limiteTemp = new LimitesOperacion();
						limiteTemp.setClaveOperacion(record.getAttribute("claveOper"));
						limiteTemp.setContrato(BeanAdmin.getLimitesContrato().getContrato());
						limiteTemp.setDescripcionOperacion(record.getAttribute("descOper"));
						limiteTemp.setLimiteOperacion(record.getAttribute("monto"));
						limiteTemp.setOpcion(BeanAdmin.getLimitesContrato().getOpcion());
						limiteTemp.setUsuario(record.getAttribute("usuario"));
						limiteTemp.setNombreUsuario(record.getAttribute("nombre"));
						
						if(record.getAttribute("estado").equals("Agregado")){
							System.out.println("El registro tiene estado Agregado");
							for(int iAgr=0;iAgr<BeanAdmin.getListaLimitesAgregados().size();iAgr++){
								LimitesOperacion limiteAgr = BeanAdmin.getListaLimitesAgregados().get(iAgr);
								if(limiteAgr.getContrato().equals(limiteTemp.getContrato()) && 
										limiteAgr.getClaveOperacion().equals(limiteTemp.getClaveOperacion()) &&
										limiteAgr.getUsuario().equals(limiteTemp.getUsuario())){
									
									limiteAgr.setLimiteOperacion(record.getAttribute("monto"));
									break;
								}
							}
							limiteTemp.setEstado("Agregado");
						}
						else if(record.getAttribute("estado").equals("Modificado")){
							System.out.println("El registro tiene estado Modificado");
							for(int iAgr=0;iAgr<BeanAdmin.getListaLimitesModificados().size();iAgr++){
								LimitesOperacion limiteAgr = BeanAdmin.getListaLimitesModificados().get(iAgr);
								if(limiteAgr.getContrato().equals(limiteTemp.getContrato()) && 
										limiteAgr.getClaveOperacion().equals(limiteTemp.getClaveOperacion()) &&
										limiteAgr.getUsuario().equals(limiteTemp.getUsuario())){
									
									limiteAgr.setLimiteOperacion(record.getAttribute("monto"));
									break;
								}
							}
							limiteTemp.setEstado("Modificado");
						}
						else if(record.getAttribute("estado").equals("Consultado")){
							System.out.println("El registro tiene estado Consultado");
							limiteTemp.setEstado("Modificado");
							record.setAttribute("estado", "Modificado");
							GridLimitesUsurios.refreshFields();
							BeanAdmin.getListaLimitesModificados().add(limiteTemp);	
							
						}
							


						for(int iR=0;iR<LimiteUsuarioTmp.getListaLimitesOperacion().size();iR++){
							if(LimiteUsuarioTmp.getListaLimitesOperacion().get(iR).getClaveOperacion().equals(record.getAttribute("claveOper"))){
								LimiteUsuarioTmp.getListaLimitesOperacion().remove(iR);
								break;
							}
						}
						
						LimiteUsuarioTmp.getListaLimitesOperacion().add(limiteTemp);
					}
				}
				
				GridLimitesUsurios.sort();
			}

			
		});
		
		GridLimitesUsurios.addCellClickHandler(new CellClickHandler(){

			public void onCellClick(CellClickEvent event) {
				System.out.println("onCellClick...");
				if(VistaMostrar.equals(VISTA_DETALLE)){
					
					ListGridRecord record = event.getRecord();
					final String lstrUsuario = record.getAttribute("usuario");
					final String lstrNombre;

					System.out.println("Buscando Usuario ["+ lstrUsuario + "]...");
					ArrayList<LimitesUsuario> lista = BeanAdmin.getListaLimitesContratos();
					for(int i=0;i<lista.size();i++){
						final LimitesUsuario Usuario = lista.get(i);
						if(Usuario.getCodCliente().equals(lstrUsuario)){
							System.out.println("Usuario encontrado[" + Usuario.getCodCliente() + "]...");
							System.out.println("Nombre Usuario encontrado[" + Usuario.getNombre() + "]...");
							System.out.println("Numero de Operadciones:" + Usuario.getListaLimitesOperacion().size());
							lstrNombre  = Usuario.getNombre();
							if(Usuario.getListaLimitesOperacion().size()==0){
								VStackGeneralLyM.setDisabled(true);
								AdminLyMService.getListaLimitesOperacion(lstrUsuario, new AsyncCallback<ArrayList<LimitesOperacion>>() {
									public void onFailure(Throwable caught) {
										System.out.println("Error al solicitar informacion al servidor");
										VStackGeneralLyM.setDisabled(false);
									}

									public void onSuccess(ArrayList<LimitesOperacion> result) {
										System.out.println("Respuesta exitosa del servidor...");
										System.out.println("Registros devueltos:" + result.size());
										
										for(int i=0;i<result.size();i++){
											LimitesOperacion limite = result.get(i);
											ListGridRecord record = new ListGridRecord();
											record.setAttribute("pk"       , lstrUsuario + limite.getClaveOperacion());
											record.setAttribute("usuario"  , lstrUsuario);
											record.setAttribute("nombre"   , lstrNombre);
											record.setAttribute("descOper" , limite.getDescripcionOperacion());
											record.setAttribute("claveOper", limite.getClaveOperacion());
											record.setAttribute("monto"    , limite.getLimiteOperacion());
											record.setAttribute("estado"   , "Consultado");
											
											LimitesOperacion limiteTemp = new LimitesOperacion();
											limiteTemp.setClaveOperacion(record.getAttribute("claveOper"));
											limiteTemp.setContrato(BeanAdmin.getLimitesContrato().getContrato());
											limiteTemp.setDescripcionOperacion(record.getAttribute("descOper"));
											limiteTemp.setLimiteOperacion(record.getAttribute("monto"));
											limiteTemp.setOpcion(BeanAdmin.getLimitesContrato().getOpcion());
											limiteTemp.setUsuario(record.getAttribute("usuario"));
											limiteTemp.setEstado("Consultado");
											
											limite.setEstado("Consultado");
											
											boolean registroEliminadoPreviamente = false;
											
											for(int iRegDel =0; iRegDel<BeanAdmin.getListaLimitesEliminados().size();iRegDel++){
												LimitesOperacion limiteDel = BeanAdmin.getListaLimitesEliminados().get(iRegDel);
												if(limiteDel.getUsuario().equals(limiteTemp.getUsuario()) && 
												   limiteDel.getClaveOperacion().equals(limiteTemp.getClaveOperacion())){
													registroEliminadoPreviamente = true;
													break;
												}
													
											}
											
											if(!registroEliminadoPreviamente){
												GridLimitesUsurios.addData(record);
												BeanAdmin.getListaLimitesConsultados().add(limiteTemp);
												System.out.println("Registro agregado[" + lstrUsuario + limite.getClaveOperacion() + "," + lstrUsuario + "," + lstrNombre + "," + limite.getDescripcionOperacion() + "," + limite.getClaveOperacion() + "," + limite.getLimiteOperacion() + "]...");
											}
											
										}
										
										Usuario.setListaLimitesOperacion(result);
										
										if(result.size()>0){
											System.out.println("Buscando registro base para ser eliminado...");
											ListGridRecord listaRegistros[] = GridLimitesUsurios.getRecords();
											for(int iReg=0;iReg<listaRegistros.length;iReg++){
												ListGridRecord recorAux = listaRegistros[iReg];
												if(recorAux.getAttribute("usuario").equals(lstrUsuario) && recorAux.getAttribute("claveOper").equals("")){
													System.out.println("Registro base encontrado...");
													System.out.println("Eliminado registro base ...");
													GridLimitesUsurios.removeData(recorAux);
													System.out.println("Registro base eliminado...");
													break;
												}
											}
										}
										
										GridLimitesUsurios.refreshFields();
										GridLimitesUsurios.sort();
										
										System.out.println("Seleccionando primer registro...");
										ListGridRecord listaRegistros[] = GridLimitesUsurios.getRecords();
										for(int iReg=0;iReg<listaRegistros.length;iReg++){
											ListGridRecord recorAux = listaRegistros[iReg];
											if(recorAux.getAttribute("usuario").equals(lstrUsuario)){
												GridLimitesUsurios.selectSingleRecord(recorAux);
												System.out.println("Registro seleccionado...");
												break;
											}
										}
										
										VStackGeneralLyM.setDisabled(false);
									}
								});
								
								
								
							}
							break;
						}
					}
					
				}
				
			}
			
		});
		
		SectionLimitesUsuarios.addItem(GridLimitesUsurios);

		SectionLimitesUsuarios.setExpanded(true); 
		 
	}
	
	
	
	private void init(String pstrModule){
		
		VStackGeneralLyM.setDisabled(true);
		
		System.out.println("Inicializando modulo:" + pstrModule);
		final String lstrVista = pstrModule;
		AdminLyMService.getBean(pstrModule, new AsyncCallback<BeanAdminLyM>() {
			public void onFailure(Throwable caught) {
				System.out.println("Error al solicitar informacion al servidor");
				
				VStackGeneralLyM.setDisabled(false);
				
				VStackGeneralLyM.removeMember(LayoutCarga);
				VStackGeneralLyM.addMember(SectionGeneralLyM);
				if(VistaMostrar.equals(VISTA_DETALLE)){
					VStackGeneralLyM.setMembersMargin(20);
					VStackGeneralLyM.addMember(lobjLayoutBotonesMontos);
					VStackGeneralLyM.addMember(lobjLayoutBotonesNavegacion);
				}
				
			}

			public void onSuccess(BeanAdminLyM result) {
				System.out.println("Respuesta exitosa del servidor...");
				System.out.println("Result:" + result);
				if(result!=null){
					BeanAdmin = result;
					System.out.println("Limite Contrato   :" + result.getLimitesContrato().getMonto());
					System.out.println("Numero de Usuarios:" + result.getListaLimitesContratos().size());
					
					//pintaInformacionLimiteContrato(lstrVista);
					//pintaInformacionLimitesUsuarios(lstrVista);
					
					PorcentajeCarga=20;
					
				    new Timer(){
					   public void run(){
						   System.out.println("Porcentaje completado:" + PorcentajeCarga);
						   ProgressBarCarga.setPercentDone(PorcentajeCarga);
						   LabelCarga.setContents("Cargando " + PorcentajeCarga + "%...");
						   
						   if(PorcentajeCarga==100){
								VStackGeneralLyM.setDisabled(false);
								
								VStackGeneralLyM.removeMember(LayoutCarga);
								VStackGeneralLyM.addMember(SectionGeneralLyM);
								if(VistaMostrar.equals(VISTA_DETALLE)){
									VStackGeneralLyM.setMembersMargin(20);
									VStackGeneralLyM.addMember(lobjLayoutBotonesMontos);
									VStackGeneralLyM.addMember(lobjLayoutBotonesNavegacion);
								}
							   
						   }
						   else if(PorcentajeCarga==20){
							   PorcentajeCarga=40;
							   pintaInformacionLimiteContrato(lstrVista);
							   schedule(50);
						   }else if(PorcentajeCarga==40){
							   PorcentajeCarga=100;
							   pintaInformacionLimitesUsuarios(lstrVista);
							   schedule(50);
						   }
						   
					   }
				   }.schedule(50);
					
					
					
				}
				else {
					System.out.println("Pintando pesta�as por default...");
					pintaInformacionLimiteContrato(lstrVista);
					pintaInformacionLimitesUsuarios(lstrVista);
					VStackGeneralLyM.setDisabled(false);
					VStackGeneralLyM.removeMember(LayoutCarga);
					VStackGeneralLyM.addMember(SectionGeneralLyM);
					if(VistaMostrar.equals(VISTA_DETALLE)){
						VStackGeneralLyM.setMembersMargin(20);
						VStackGeneralLyM.addMember(lobjLayoutBotonesMontos);
						VStackGeneralLyM.addMember(lobjLayoutBotonesNavegacion);
					}
					
					
				}
				
			}
		});
		
	}
	
	
	private void pintaInformacionLimiteContrato(String pstrVista){
		LimiteContrato.setValue(BeanAdmin.getLimitesContrato().getMonto());
		if(pstrVista.equals(VISTA_DESGLOSE)){
			LimiteContratoActivado.setValue(Boolean.valueOf(BeanAdmin.getLimitesContrato().isLimiteActivo()));			
			LimiteContratoActivado.setDisabled(true);
			LimiteContrato.setDisabled(true);
		}
		else{
			if(BeanAdmin.getFacultadContrato()){
				LimiteContratoActivado.setValue(Boolean.valueOf(BeanAdmin.getLimitesContrato().isLimiteActivo()));
				LimiteContrato.setDisabled(!Boolean.valueOf(BeanAdmin.getLimitesContrato().isLimiteActivo()));
			}
			else{
				LimiteContratoActivado.setValue(Boolean.valueOf(BeanAdmin.getLimitesContrato().isLimiteActivo()));			
				LimiteContratoActivado.setDisabled(true);
				LimiteContrato.setDisabled(true);
				
			}
		}
	}

	
	private void pintaInformacionLimitesUsuarios(String pstrVista){
		
		ArrayList<LimitesUsuario> lobjMontos = new ArrayList<LimitesUsuario>();  
			
		if(pstrVista.equals(VISTA_DESGLOSE)){

			/**
			 * Cargando Registros Agregados 
			 */
			for(int iAgregados=0;iAgregados<BeanAdmin.getListaLimitesAgregados().size();iAgregados++){

				LimitesOperacion limiteOper   = BeanAdmin.getListaLimitesAgregados().get(iAgregados);
				LimitesUsuario   limiteUsrAux = new LimitesUsuario();

				limiteUsrAux.setCodCliente(limiteOper.getUsuario());
				limiteUsrAux.setNombre(limiteOper.getNombreUsuario());
				limiteUsrAux.getListaLimitesOperacion().add(limiteOper);
				
				lobjMontos.add(limiteUsrAux);
			}
			
			/**
			 * Cargando Registros Modificados 
			 */
			for(int iModificados=0;iModificados<BeanAdmin.getListaLimitesModificados().size();iModificados++){

				LimitesOperacion limiteOper   = BeanAdmin.getListaLimitesModificados().get(iModificados);
				LimitesUsuario   limiteUsrAux = new LimitesUsuario();

				limiteUsrAux.setCodCliente(limiteOper.getUsuario());
				limiteUsrAux.setNombre(limiteOper.getNombreUsuario());
				limiteUsrAux.getListaLimitesOperacion().add(limiteOper);
				lobjMontos.add(limiteUsrAux);
			}
			
			/**
			 * Cargando Registros Eliminados 
			 */
			for(int iEliminados=0;iEliminados<BeanAdmin.getListaLimitesEliminados().size();iEliminados++){

				LimitesOperacion limiteOper   = BeanAdmin.getListaLimitesEliminados().get(iEliminados);
				LimitesUsuario   limiteUsrAux = new LimitesUsuario();

				limiteUsrAux.setCodCliente(limiteOper.getUsuario());
				limiteUsrAux.setNombre(limiteOper.getNombreUsuario());
				limiteUsrAux.getListaLimitesOperacion().add(limiteOper);
				lobjMontos.add(limiteUsrAux);
			}
			

			
		}
		else{
			lobjMontos = BeanAdmin.getListaLimitesContratos();
			if(!BeanAdmin.getFacultadOperacion()){
				GridLimitesUsurios.getField("monto").setCanEdit(false);
			}
		}
		
		
		
		System.out.println("Cargando catalogo de usuarios..." );
		//HashMap<String, String> CatalogoUsuarios = new HashMap<String, String>();
		System.out.println("No de Usuarios a cargar:" + lobjMontos.size());
		//CatalogoUsuarios2.put("" ,"");
		for(int iMonto = 0; iMonto<lobjMontos.size();iMonto++){
			LimitesUsuario usuario = lobjMontos.get(iMonto);
			String lstrUsuario     = usuario.getCodCliente();
			String lstrNombre      = usuario.getNombre();
			CatalogoUsuarios.put(lstrUsuario, lstrUsuario);
			CatalogoUsuarios2.put(lstrNombre ,lstrUsuario);
			System.out.println("Usuario cargado:" + lstrUsuario);
		}
		MontosUsuariosDS.getInstance().setMapGrup(CatalogoUsuarios);
		System.out.println("Catalogo cargado..." );
		
		System.out.println("Cargando catalogo de operaciones..." );
		//CatalogoOperaciones = BeanAdmin.getListaOperaciones();
		ArrayList<GrupoOperacion> lobjOperaciones = BeanAdmin.getListaOperaciones();
		//CatalogoOperaciones.put("" ,"");
		for(int iOper=0;iOper<lobjOperaciones.size();iOper++){
			GrupoOperacion grupoAux = lobjOperaciones.get(iOper);
			CatalogoOperaciones.put(grupoAux.getClave(),grupoAux.getDescripcion());
		}
		System.out.println("Catalogo cargado..." );
		
		System.out.println("Cargando grid...");
		for(int iMonto = 0; iMonto<lobjMontos.size();iMonto++){
			LimitesUsuario usuario = lobjMontos.get(iMonto);
			String lstrUsuario     = usuario.getCodCliente();
			String lstrNombre      = usuario.getNombre();
			String lstrOperacion   = "";
			String lstrMonto       = "";
			String lstrClave       = "";
			String lstrEstado      = "";
			ArrayList<LimitesOperacion> listaLimitesOp = usuario.getListaLimitesOperacion();
			if(listaLimitesOp.size()>0){
				System.out.println("Agregando " + listaLimitesOp.size() + " limites por operacion..." );
				for(int iOper=0;iOper<listaLimitesOp.size();iOper++){
					LimitesOperacion limOper = listaLimitesOp.get(iOper);
					lstrOperacion   = limOper.getDescripcionOperacion();
					lstrMonto       = limOper.getLimiteOperacion();
					lstrClave       = limOper.getClaveOperacion();
					lstrEstado      = limOper.getEstado();
					ListGridRecord record = new ListGridRecord();
					record.setAttribute("pk"       , lstrUsuario + lstrClave);
					record.setAttribute("usuario"  , lstrUsuario);
					record.setAttribute("nombre"   , lstrNombre);
					record.setAttribute("descOper" , lstrOperacion);
					record.setAttribute("claveOper", lstrClave);
					record.setAttribute("monto"    , lstrMonto);
					record.setAttribute("estado"   , lstrEstado);
					GridLimitesUsurios.addData(record);
					System.out.println("Registro agregado[" + record.toString()+ "]...");
				}
			}
			else{
				System.out.println("Agregando limites base por usuario..." );
				ListGridRecord record = new ListGridRecord();
				record.setAttribute("pk"       , lstrUsuario + lstrClave);
				record.setAttribute("usuario"  , lstrUsuario);
				record.setAttribute("nombre"   , lstrNombre);
				record.setAttribute("descOper" , lstrOperacion);
				record.setAttribute("claveOper", lstrClave);
				record.setAttribute("monto"    , lstrMonto);
				record.setAttribute("estado"   , lstrEstado);
				GridLimitesUsurios.addData(record);
				System.out.println("Registro agregado[" + record.toString()+ "]...");
			}
		}
		
		//GridLimitesUsurios.sort();
		GridLimitesUsurios.setGroupStartOpen(GroupStartOpen.NONE);   
		GridLimitesUsurios.setShowAllRecords(true);
		
	}
	

	
	
}

