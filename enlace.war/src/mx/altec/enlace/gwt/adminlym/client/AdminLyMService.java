package mx.altec.enlace.gwt.adminlym.client;

import java.util.ArrayList;

import mx.altec.enlace.gwt.adminlym.shared.BeanAdminLyM;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("adminLyMService")
public interface AdminLyMService extends RemoteService {
	void setBean(BeanAdminLyM pstrBean) throws IllegalArgumentException;
	BeanAdminLyM getBean(String pstrVista) throws IllegalArgumentException;
	ArrayList<LimitesOperacion> getListaLimitesOperacion(String pstrUsuario) throws IllegalArgumentException;
}
