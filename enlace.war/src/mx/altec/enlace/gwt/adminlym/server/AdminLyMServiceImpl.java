package mx.altec.enlace.gwt.adminlym.server;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.AdmLyMGL27;
import mx.altec.enlace.dao.AdmLyMLimitesOperacionDAO;
import mx.altec.enlace.gwt.adminlym.client.AdminLyMService;
import mx.altec.enlace.gwt.adminlym.shared.BeanAdminLyM;
import mx.altec.enlace.gwt.adminlym.shared.GrupoOperacion;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;
import mx.altec.enlace.gwt.adminlym.shared.LimitesUsuario;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

/**
 * The server side implementation of the RPC service.
 */
public class AdminLyMServiceImpl extends RemoteServiceServlet implements
AdminLyMService {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2161966197237034653L;

	public BeanAdminLyM getBean(String pstrVista) throws IllegalArgumentException {
		
		//BeanAdminLyM.simulacionActivada = true;
		
		System.out.println("getBean()...");
		
		HttpSession sesion = getThreadLocalRequest().getSession();
		
		BeanAdminLyM bean = null;
		
		/*
		bean = getBeanTemporal();
		sesion.setAttribute("BeanAdminUsr", bean);
		System.out.println("BeanAdminUsr fue guardado en session...");
		*/
				
		try{
			if(pstrVista.equals("detalle")){
				bean = (BeanAdminLyM)sesion.getAttribute("BeanAdminLyMFin");
				if(bean==null)
					bean = (BeanAdminLyM)sesion.getAttribute("BeanAdminLyM");
			}			
			else
				bean = (BeanAdminLyM)sesion.getAttribute("BeanAdminLyMFin");
			
			
			System.out.println("BeanAdminUsr fue leido de session...");
			System.out.println("Bean:" + bean);
			System.out.println("Simulacion:" + BeanAdminLyM.simulacionActivada);
			
			/**
			 * Para pruebas
			 */
			if(bean==null && BeanAdminLyM.simulacionActivada){
				
				int iNumUsuario     = 5;
				int iNumOperaciones = 5;
				
				bean = new BeanAdminLyM();
				bean.getLimitesContrato().setLimiteActivo(true);
				bean.getLimitesContrato().setMonto("80000");
				ArrayList<LimitesUsuario> arrLimites = new ArrayList<LimitesUsuario>(); 
				
				for(int iUsuario=0;iUsuario<iNumUsuario;iUsuario++){
					LimitesUsuario limiteTemp = new LimitesUsuario();
					limiteTemp.setCodCliente("CodigoUsuario" + (iUsuario+1));
					limiteTemp.setNombre("NombreUsuario" + (iUsuario+1));
					arrLimites.add(limiteTemp);
				}

				bean.setListaLimitesContratos(arrLimites); 
				
				ArrayList<GrupoOperacion> CatalogoOperaciones = new ArrayList<GrupoOperacion>();
				for(int iCatOper=0;iCatOper<iNumOperaciones;iCatOper++){
					GrupoOperacion grupoTemp = new GrupoOperacion();
					grupoTemp.setClave("Operacion" + (iCatOper+1));
					grupoTemp.setDescripcion("DescOperacion" + (iCatOper+1));
					CatalogoOperaciones.add(grupoTemp);
				}
				
				bean.setListaOperaciones(CatalogoOperaciones);
				
			}
			if(bean != null) {
				System.out.println("No. Contratos:" + bean.getListaLimitesContratos().size());
				System.out.println("Limite       :" + bean.getLimitesContrato().getMonto());
				System.out.println("Activo       :" + bean.getLimitesContrato().isLimiteActivo());
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		
		return bean;

	}

	public void setBean(BeanAdminLyM pstrBean) throws IllegalArgumentException {
		System.out.println("setBean...");
		
		try{
			System.out.println("Limite:" + pstrBean.getLimitesContrato().getMonto());
			System.out.println();
			System.out.println("Numero de limites consultados:" + pstrBean.getListaLimitesConsultados().size());
			for(int iLimite=0;iLimite<pstrBean.getListaLimitesConsultados().size();iLimite++){
				LimitesOperacion limit = pstrBean.getListaLimitesConsultados().get(iLimite);
				System.out.println("[" + limit.getUsuario() + "," + limit.getContrato() + "," + limit.getClaveOperacion() + "," + limit.getLimiteOperacion() + "]");
			}
			System.out.println();
			
			System.out.println("Numero de limites finales:" + pstrBean.getListaLimitesFinal().size());
			for(int iLimite=0;iLimite<pstrBean.getListaLimitesFinal().size();iLimite++){
				LimitesOperacion limit = pstrBean.getListaLimitesFinal().get(iLimite);
				System.out.println("[" + limit.getUsuario() + "," + limit.getContrato() + "," + limit.getClaveOperacion() + "," + limit.getLimiteOperacion() + "]");
			}
			System.out.println();
			
			System.out.println("Numero de acciones asociadas a limites agregados:" + pstrBean.getListaLimitesAgregados().size());
			for(int iLimite=0;iLimite<pstrBean.getListaLimitesAgregados().size();iLimite++){
				LimitesOperacion limit = pstrBean.getListaLimitesAgregados().get(iLimite);
				System.out.println("[" + limit.getUsuario() + "," + limit.getContrato() + "," + limit.getClaveOperacion() + "," + limit.getLimiteOperacion() + "]");
			}
			System.out.println();

			System.out.println("Numero de acciones asociadas a limites eliminados:" + pstrBean.getListaLimitesEliminados().size());
			for(int iLimite=0;iLimite<pstrBean.getListaLimitesEliminados().size();iLimite++){
				LimitesOperacion limit = pstrBean.getListaLimitesEliminados().get(iLimite);
				System.out.println("[" + limit.getUsuario() + "," + limit.getContrato() + "," + limit.getClaveOperacion() + "," + limit.getLimiteOperacion() + "]");
			}
			System.out.println();

			System.out.println("Numero de acciones asociadas a limites modificados:" + pstrBean.getListaLimitesModificados().size());
			for(int iLimite=0;iLimite<pstrBean.getListaLimitesModificados().size();iLimite++){
				LimitesOperacion limit = pstrBean.getListaLimitesModificados().get(iLimite);
				System.out.println("[" + limit.getUsuario() + "," + limit.getContrato() + "," + limit.getClaveOperacion() + "," + limit.getLimiteOperacion() + "]");
			}
			System.out.println();

			getThreadLocalRequest().getSession().setAttribute("BeanAdminLyMFin", pstrBean);
			System.out.println("Bean guardado...");
			
		}
		catch(Exception e){
			e.printStackTrace();
		}		
	}

	public ArrayList<LimitesOperacion> getListaLimitesOperacion(String pstrUsuario)throws IllegalArgumentException {
		
		//BeanAdminLyM.simulacionActivada = true;
		
		ArrayList<LimitesOperacion>  lobjLimites = new ArrayList<LimitesOperacion>();
		HttpSession session = getThreadLocalRequest().getSession();
		
		/**
		 * Para pruebas
		 */
		
		if(BeanAdminLyM.simulacionActivada){
			
			int iOpers  = 2;
			
			for(int i=0;i<iOpers;i++){
				LimitesOperacion limite = new LimitesOperacion();
				limite.setClaveOperacion("Operacion" + (i+1));
				limite.setContrato(pstrUsuario.substring(8));
				limite.setNombreUsuario(pstrUsuario);
				limite.setDescripcionOperacion("DescOperacion" + (i+1));
				limite.setLimiteOperacion("10.0");
				lobjLimites.add(limite);
			}
			return lobjLimites;
		}
		
		/*
		String lstrServletLyM = "/enlaceMig/AdmonLimitesMontosServlet";
		try {
			URL urlServlet = new URL(lstrServletLyM);
			URLConnection urlConn = urlServlet.openConnection();
			urlConn.getOutputStream();
			
		} catch (Exception e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		}
		*/
		// 1. Crear instancia de AdmLyMLimitesOperacionDAO
		// 2 . Invocar al método 
			/*AdmLyMGL27 consultaLimitesOperacion(String tipoOperacion, 
				String cveGrupo, String contrato) */
		// 3. Dentro del bean AdmLyMGL27 que devuelve viene el atributo "límites" 
		// que es un ArrayList<LimitesOperacion>
		// 4. retornar el dato
		
		
		BeanAdminLyM bean = (BeanAdminLyM)session.getAttribute("BeanAdminLyM");
		AdmLyMLimitesOperacionDAO limOper = new AdmLyMLimitesOperacionDAO();
    	MQQueueSession mqsess = null;
    	AdmLyMGL27 res = null;
    	try {
    		mqsess = new MQQueueSession(
    				NP_JNDI_CONECTION_FACTORY, 
    				NP_JNDI_QUEUE_RESPONSE, 
    				NP_JNDI_QUEUE_REQUEST);
    		limOper.setMqsess(mqsess);
    		if (bean != null) {
        		res = limOper.consultaLimitesOperacion(bean.getLimitesContrato().getContrato(),
        				pstrUsuario, "");
        		lobjLimites = res.getLimites();
        		//res.getLimites();
    		}
    		
    	} catch (Exception x) {
		    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
	    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
    	} finally {
    		try {
    			mqsess.close();
    		}catch(Exception x) {
			    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
			    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
    		}
    	}
		
		
		
		return lobjLimites;
	}
	
	
	
	
}
