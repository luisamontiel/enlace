package mx.altec.enlace.gwt.adminusr.client;

import mx.altec.enlace.gwt.adminusr.shared.BeanAdminUsr;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AdminUsrServiceAsync {
	
	void getBean(String pstrVista, AsyncCallback<BeanAdminUsr> callback);
	void setBean(BeanAdminUsr bean, AsyncCallback callback);

}
