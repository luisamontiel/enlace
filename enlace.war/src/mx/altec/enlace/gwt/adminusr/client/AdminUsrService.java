package mx.altec.enlace.gwt.adminusr.client;

import mx.altec.enlace.gwt.adminusr.shared.BeanAdminUsr;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */

@RemoteServiceRelativePath("adminUsrService")
public interface AdminUsrService extends RemoteService {
	
	BeanAdminUsr getBean(String pstrVista);
	void setBean(BeanAdminUsr bean);
}
