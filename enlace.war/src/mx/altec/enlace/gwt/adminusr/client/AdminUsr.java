package mx.altec.enlace.gwt.adminusr.client;

import java.util.ArrayList;

import mx.altec.enlace.gwt.adminusr.shared.BeanAdminUsr;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Progressbar;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.DataArrivedEvent;
import com.smartgwt.client.widgets.tree.events.DataArrivedHandler;

public class AdminUsr implements EntryPoint {

	private  SectionStack             SectionGDetalle         = new SectionStack();	
	private  SectionStackSection      SectionMancomunidad     = new SectionStackSection("Mancomunidad"); 		
	private  SectionStackSection      SectionPerfiles         = new SectionStackSection("Perfiles"); 		
	private  SectionStackSection      SectionFacultades       = new SectionStackSection("Facultades Extraordinarias"); 		
	private  SelectItem               ComboFirma              = new SelectItem();  
	private  TextItem                 TextMonto               = new TextItem();
	private  Tree                     TreePerfilesIni         = new Tree();
	private  Tree                     TreePerfilesFin         = new Tree();
	private  Tree                     TreeFacultadesIni       = new Tree();
	private  Tree                     TreeFacultadesFin       = new Tree();
	private  Image                    Regresar                = new Image("/gifs/EnlaceMig/gbo25320.gif");
	private  Image                    Continuar               = new Image("/gifs/EnlaceMig/gbo25310.gif");
    private  VLayout                  GLayout                 = new VLayout();
    private  HLayout                  LayoutBotones           = new HLayout();
    private  VLayout                  LayoutCarga             = new VLayout();
    private  Label                    LabelCarga              = new Label("Cargando...");
    private  Progressbar              ProgressBarCarga        = new Progressbar();
    private  int                      PorcentajeCarga         = 0;
    
	private  AdminUsrServiceAsync     AdminUsrService         = GWT.create(AdminUsrService.class);
	private  TreeGrid                 gridIniPerfiles         = new TreeGrid();
	private  TreeGrid                 gridFinPerfiles         = new TreeGrid();
	private  TreeGrid                 gridIniFacultades       = new TreeGrid();
	private  TreeGrid                 gridFinFacultades       = new TreeGrid();
	private  TreeNode                 rootNodePerfilesIni     = null; 
	private  TreeNode                 rootNodePerfilesFin     = null;
	private  TreeNode                 rootNodeFacultadesIni   = null; 
	private  TreeNode                 rootNodeFacultadesFin   = null;
	private  String                   Firma                   = "";
	private  String                   Monto                   = "0.0";
	private  BeanAdminUsr             BeanAdmin               = new BeanAdminUsr(); 
	private  String                   URLServlet              = "/enlaceMig/AdmonUsuarioServlet";
	private  String                   URLOrigen               = "";
	private  String                   URLDestino              = "";
	private  String                   URLError                = "";
	private  String                   OperacionSolicitada     = "";
	private  String                   VistaMostrar            = VISTA_DETALLE;
	private static String             VISTA_DETALLE           = "detalle";
	private static String             VISTA_DESGLOSE          = "desglose";
	
	
	public void onModuleLoad() {
		try{
			BeanAdmin           = new BeanAdminUsr();
			URLServlet          = DOM.getElementById("URLServlet")         !=null?DOM.getElementById("URLServlet").getAttribute("value"):URLServlet;
			URLOrigen           = DOM.getElementById("URLOrigen")          !=null?DOM.getElementById("URLOrigen").getAttribute("value"):URLOrigen;
			URLDestino          = DOM.getElementById("URLDestino")         !=null?DOM.getElementById("URLDestino").getAttribute("value"):URLDestino;
			URLError            = DOM.getElementById("URLError")           !=null?DOM.getElementById("URLError").getAttribute("value"):URLError;
			OperacionSolicitada = DOM.getElementById("OperacionSolicitada")!=null?DOM.getElementById("OperacionSolicitada").getAttribute("value"):OperacionSolicitada;
			VistaMostrar        = DOM.getElementById("VistaMostrar")       !=null?DOM.getElementById("VistaMostrar").getAttribute("value"):VistaMostrar;
			
			System.out.println("URLServlet          :" + URLServlet);
			System.out.println("URLOrigen           :" + URLOrigen);
			System.out.println("URLDestino          :" + URLDestino);
			System.out.println("URLError            :" + URLError);
			System.out.println("OperacionSolicitada :" + OperacionSolicitada);
		}
		catch(Exception e){
			e.printStackTrace();
		}

		pintaModulo(VistaMostrar);
		
	}

	private void pintaModulo(String pstrVista){
		
		System.out.println("Pintando vista:" + pstrVista);
		
		final String lstrVista =  pstrVista;
		
		GLayout.setWidth(750);
		GLayout.setHeight(650);

		 
		SectionGDetalle.setVisibilityMode(VisibilityMode.MULTIPLE);
		SectionGDetalle.setWidth(750);
		SectionGDetalle.setHeight(600);
		pintaTabMancomunidad(pstrVista);
		pintaTabPerfiles(pstrVista);
		pintaTabFacultades(pstrVista);
		SectionGDetalle.addSection(SectionMancomunidad);
		SectionGDetalle.addSection(SectionPerfiles);
		SectionGDetalle.addSection(SectionFacultades);
		
		Regresar.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				System.out.println("Regresar->onMouseDown[Enviar Peticion de regreso]...");
				GLayout.setDisabled(true);
				previus();
				
			}
			
		});

	    Continuar.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				
				if(BeanAdmin.getMostrarMancomunidad() && ComboFirma.getValue().toString().equals("")){
					SC.say("Por favor seleccione el tipo de firma en la secci&oacute;n de Mancomunidad.");
					return;
				}
				else if(BeanAdmin.getMostrarMancomunidad() && TextMonto.getValue().toString().equals("")){
					SC.say("Por favor capture el monto en la secci&oacute;n de Mancomunidad.");
					return;
				} 
				
				System.out.println("Continuar->onMouseOut[Enviar Peticion de continuar]...");
				GLayout.setDisabled(true);
				if(VistaMostrar.equals(VISTA_DETALLE)){
					actualizaBeanAdminUsr();
					if(BeanAdmin.getMostrarMancomunidad()){
						BeanAdmin.setFirma(ComboFirma.getValue().toString());
						BeanAdmin.setMonto(TextMonto.getValue().toString());
					}
				}
				next();
			}
			
		});
	    
	    LabelCarga.setHeight(16);
	    
	    ProgressBarCarga.setHeight(10);
	    ProgressBarCarga.setWidth(300);
	    ProgressBarCarga.setVertical(false);
	    ProgressBarCarga.setPercentDone(PorcentajeCarga);
	    
	    //LayoutCarga.setAlign(Alignment.CENTER);
	    LayoutCarga.setMargin(20);
	    LayoutCarga.addMember(LabelCarga);
	    LayoutCarga.addMember(ProgressBarCarga);
	    
	    GLayout.addMember(LayoutCarga);
	    
	    GLayout.draw();
	    
	    PorcentajeCarga = 0;
	    ProgressBarCarga.setPercentDone(PorcentajeCarga);
		LabelCarga.setContents("Cargando " + PorcentajeCarga + "%...");
		   

	   
	   init(lstrVista);

	}
	
	private void actualizaBeanAdminUsr(){
		
		ArrayList<String> lalstPerfiles   = new ArrayList<String>();
		ArrayList<String> lalstFacultades = new ArrayList<String>();
		
		BeanAdmin.setFacultadesUsuario(lalstPerfiles);
		BeanAdmin.setFacultadesExtUsuario(lalstFacultades);
		
		try{
			TreeNode Grupos[] = TreePerfilesFin.getChildren(rootNodePerfilesFin);
			System.out.println("Numero de grupos:" + Grupos.length);
			for(int iGrupos=0;iGrupos<Grupos.length;iGrupos++){
				System.out.println("Grupo:" + Grupos[iGrupos].getAttribute("Nombre"));
				TreeNode Perfiles[] = TreePerfilesFin.getChildren(Grupos[iGrupos]);
				System.out.println("Numero de perfiles:" + Perfiles.length);
				for(int iPerfiles=0;iPerfiles<Perfiles.length;iPerfiles++){
					System.out.println("Perfil:" + Perfiles[iPerfiles].getAttribute("Nombre"));
					TreeNode Facultades[] = TreePerfilesFin.getChildren(Perfiles[iPerfiles]);
					System.out.println("Numero de facultades:" + Facultades.length);
					for(int iFacultades=0;iFacultades<Facultades.length;iFacultades++){
						System.out.println("Facultad:" + Facultades[iFacultades].getAttribute("Nombre"));
						String lstrFacultadesUsuarios = Grupos[iGrupos].getAttribute("Id")         + "&" + Grupos[iGrupos].getAttribute("Nombre")        + "@" +
						                                Perfiles[iPerfiles].getAttribute("Id")     + "&" + Perfiles[iPerfiles].getAttribute("Nombre")    + "@" +
						                                Facultades[iFacultades].getAttribute("Id") + "&" + Facultades[iFacultades].getAttribute("Nombre");
						System.out.println("Facultad Agregada:" + lstrFacultadesUsuarios);
						lalstPerfiles.add(lstrFacultadesUsuarios);
					}
				}
			}
			BeanAdmin.setFacultadesUsuario(lalstPerfiles);
			System.out.println("No. de Facultades Agregadas:" + lalstPerfiles.size());
			
			TreeNode FacultadesAux[] = TreeFacultadesFin.getChildren(rootNodeFacultadesFin);
			System.out.println("Numero de facultades ext:" + FacultadesAux.length);
			for(int iFacultades=0;iFacultades<FacultadesAux.length;iFacultades++){
				System.out.println("Facultad Extraordinaria Agregada:" + FacultadesAux[iFacultades].getAttribute("Id") +  "&" + FacultadesAux[iFacultades].getAttribute("Nombre"));
				lalstFacultades.add(FacultadesAux[iFacultades].getAttribute("Id") + "&" + FacultadesAux[iFacultades].getAttribute("Nombre"));			
			}
			
			BeanAdmin.setFacultadesExtUsuario(lalstFacultades);
			System.out.println("No. de Facultades Extraordinarias Agregadas:" + lalstPerfiles.size());

		}
		catch(Exception e){
			e.printStackTrace();
		}

	}
	
	private void next(){
		
		if(VistaMostrar.equals(VISTA_DETALLE)){
			AdminUsrService.setBean(BeanAdmin, new AsyncCallback() {

				public void onFailure(Throwable caught) {
					System.out.println("Error al solicitar informacion al servidor");
					GLayout.setDisabled(false);
					String lstrURLRedirec = URLServlet + "?" +
					"Origen="  + URLOrigen  + "&" + 
					"Destino=" + URLError + "&"  +  
					"Operacion="  + OperacionSolicitada;
					//Window.alert("Next[Error]->Reenviando a:" + GWT.getModuleBaseURL() + lstrURLRedirec);
					Window.open(lstrURLRedirec, "_parent", ""); 
				}

				public void onSuccess(Object result){
					System.out.println("Regreso del guardar...");
					GLayout.setDisabled(false);
					String lstrURLRedirec = URLServlet   + "?" +
					"Origen="    + URLOrigen  + "&" + 
					"Destino="   + URLDestino + "&"  +  
					"Operacion=" + OperacionSolicitada;
					//Window.alert("Next[Exitoso]->Reenviando a:" + GWT.getModuleBaseURL() + lstrURLRedirec);
					Window.open(lstrURLRedirec, "_parent", ""); 
				}
			});	
		}
		else{
			GLayout.setDisabled(false);
			String lstrURLRedirec = URLServlet   + "?" +
			"Origen="    + URLOrigen  + "&" + 
			"Destino="   + URLDestino + "&"  +  
			"Operacion=" + OperacionSolicitada;
			//Window.alert("Next[Exitoso]->Reenviando a:" + GWT.getModuleBaseURL() + lstrURLRedirec);
			Window.open(lstrURLRedirec, "_parent", ""); 
		}
	}
	
	private void previus(){
		if (URLOrigen.equals("/jsp/adminusr/detalleAltaUsr.jsp")) {
			URLDestino = "/jsp/adminusr/adminAltaUsr.jsp";
		} else if (URLOrigen.equals("/jsp/adminusr/desgloseAltaUsr.jsp")) {
			URLDestino = "/jsp/adminusr/detalleAltaUsr.jsp";
		} else if (URLOrigen.equals("/jsp/adminusr/detalleModiUsr.jsp")) {
			URLDestino = "/jsp/adminusr/adminModiUsr.jsp";
		} else if (URLOrigen.equals("/jsp/adminusr/desgloseModiUsr.jsp")) {
			URLDestino = "/jsp/adminusr/detalleModiUsr.jsp";
		} else if (URLOrigen.equals("/jsp/adminusr/desgloseBajaUsr.jsp")) {
			URLDestino = "/jsp/adminusr/adminBajaUsr.jsp";
		} 

	 	String lstrURLRedirec = URLServlet + "?" +
								"Origen="  + URLOrigen  + "&" + 
								"Destino=" + URLDestino + "&"  +  
								"Operacion=" + OperacionSolicitada; 
	 	//Window.alert("previus[Exitoso]->Reenviando a:" + GWT.getModuleBaseURL() + lstrURLRedirec);
	 	Window.open(lstrURLRedirec, "_parent", "");
	}
	
	private void pintaTabMancomunidad(String pstrVista){
		
		ComboFirma.setTitle("Firma");  
		ComboFirma.setHint("<nobr>Seleccione una firma</nobr>");  
		//ComboFirma.setType("comboBox");  
		ComboFirma.setName("firma");
		ComboFirma.setValueMap("A", "B", "C");
		
		TextMonto.setTitle("Monto");
		TextMonto.setName("monto");
		TextMonto.setKeyPressFilter("[0-9.]");
		TextMonto.setLength(new Integer(16));
		
		DynamicForm form  = new DynamicForm();
		DynamicForm form2 = new DynamicForm();
		form.setMargin(10);
		form2.setMargin(10);
		
		form.setItems(ComboFirma);
		form2.setItems(TextMonto);
		
		HLayout LayoutMancomunidad = new HLayout();
		LayoutMancomunidad.setMembersMargin(20);
		LayoutMancomunidad.addMember(form);
		LayoutMancomunidad.addMember(form2);
		LayoutMancomunidad.setHeight(40);
		
		SectionMancomunidad.setExpanded(true);  
		SectionMancomunidad.addItem(LayoutMancomunidad);
		
		/*
		if(pstrVista.equals(VISTA_DESGLOSE)){
			LayoutMancomunidad.setDisabled(true);
		}
		*/
			
	}

	private void pintaTabPerfiles(String pstrVista){
	
		TreePerfilesIni.setModelType(TreeModelType.CHILDREN);
		TreePerfilesIni.setNameProperty("Nombre");
		gridIniPerfiles.setDragDataAction(DragDataAction.MOVE);
		gridIniPerfiles.setCanReparentNodes(false);
		gridIniPerfiles.setCanDropOnLeaves(true);
		//gridIniPerfiles.setEdgeMarginSize(10);
		gridIniPerfiles.setSelectionType(SelectionStyle.SINGLE);
		//gridIniPerfiles.setTreeFieldTitle("Nombre");
		gridIniPerfiles.setTreeFieldTitle("Cat&aacute;logo de perfiles y facultades");
		gridIniPerfiles.setSortFieldAscendingText("Ordenar Ascendentemente");
		gridIniPerfiles.setSortFieldDescendingText("Ordenar Descendentemente");
		gridIniPerfiles.setEmptyMessage("No hay elementos para mostrar");
		
		TreePerfilesFin.setModelType(TreeModelType.CHILDREN);  
		TreePerfilesFin.setNameProperty("Nombre");
		gridFinPerfiles.setDragDataAction(DragDataAction.MOVE);
		gridFinPerfiles.setCanReparentNodes(false);
		gridFinPerfiles.setCanDropOnLeaves(true);
		//gridIniPerfiles.setEdgeMarginSize(15);
		gridFinPerfiles.setSelectionType(SelectionStyle.SINGLE);
		gridFinPerfiles.setTreeFieldTitle("Perfiles y facultades a asignar");
		gridFinPerfiles.setSortFieldAscendingText("Ordenar Ascendentemente");
		gridFinPerfiles.setSortFieldDescendingText("Ordenar Descendentemente");
		gridFinPerfiles.setEmptyMessage("No hay elementos para mostrar");
		
		//gridIniPerfiles.setHeaderSpanTitle("Facultades", "Facultades");
		
		
		VStack moveControls = new VStack(10);  
		moveControls.setWidth(32);  
		moveControls.setHeight(74);  
		moveControls.setLayoutAlign(Alignment.CENTER);  

		TransferImgButton rightArrow = new TransferImgButton(TransferImgButton.RIGHT, new ClickHandler() {  
			public void onClick(ClickEvent event) {
				System.out.println("Pasando elementos de Izquierda a derecha...");
				
				String         PathsSel  = gridIniPerfiles.getSelectedPaths();
				//ListGridRecord elemSel[] = gridIniPerfiles.getSelection();
				String lstrPath[] = PathsSel.split(",");
				System.out.println("Elementos Seleccionados:" + lstrPath.length);
				for(int iPath=0;iPath<lstrPath.length;iPath++){
					try{
						lstrPath[iPath] = lstrPath[iPath].substring(lstrPath[iPath].indexOf("\"")+1);
						lstrPath[iPath] = lstrPath[iPath].substring(0,lstrPath[iPath].indexOf("\""));
						System.out.println("Path:" + lstrPath[iPath]);
						String lstrDescompPath[] = lstrPath[iPath].split("/");
						System.out.println("Elemento compuesto por " + lstrDescompPath.length + " rutas");

						if(lstrDescompPath.length==4){

							String lstrGrupo    = lstrDescompPath[1];
							String lstrPerfil   = lstrDescompPath[2];						
							String lstrFacultad = lstrDescompPath[3];

							System.out.println("Grupo   :" + lstrGrupo);
							System.out.println("Perfil  :" + lstrPerfil);
							System.out.println("Facultad:" + lstrFacultad);

							transferFacultad(lstrGrupo, lstrPerfil, lstrFacultad, 
									TreePerfilesIni, TreePerfilesFin, 
									rootNodePerfilesIni, rootNodePerfilesFin,true);

						}
						else if(lstrDescompPath.length==3){
							System.out.println("Transfiriendo perfil..");
							String lstrGrupo    = lstrDescompPath[1];
							String lstrPerfil   = lstrDescompPath[2];
							System.out.println("Grupo   :" + lstrGrupo);
							System.out.println("Perfil  :" + lstrPerfil);

							transferPerfil(lstrGrupo, lstrPerfil, 
									TreePerfilesIni, TreePerfilesFin, 
									rootNodePerfilesIni, rootNodePerfilesFin,true);

						}
						else if(lstrDescompPath.length==2){
							System.out.println("Transfiriendo prupo..");
							String lstrGrupo    = lstrDescompPath[1];
							System.out.println("Grupo   :" + lstrGrupo);

							transferGrupo(lstrGrupo, 
									TreePerfilesIni, TreePerfilesFin, 
									rootNodePerfilesIni, rootNodePerfilesFin,true);

						}
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				
			}  
		});  
		moveControls.addMember(rightArrow);  

		TransferImgButton leftArrow = new TransferImgButton(TransferImgButton.LEFT, new ClickHandler() {  
			public void onClick(ClickEvent event) {
				System.out.println("Pasando elementos de derecha a izquierda...");
				String         PathsSel  = gridFinPerfiles.getSelectedPaths();
				//ListGridRecord elemSel[] = gridIniPerfiles.getSelection();
				String lstrPath[] = PathsSel.split(",");
				System.out.println("Elementos Seleccionados:" + lstrPath.length);
				for(int iPath=0;iPath<lstrPath.length;iPath++){

					try{
						System.out.println("Path elemento:" + lstrPath[iPath]);
						lstrPath[iPath] = lstrPath[iPath].substring(lstrPath[iPath].indexOf("\"")+1);
						lstrPath[iPath] = lstrPath[iPath].substring(0,lstrPath[iPath].indexOf("\""));
						System.out.println("Path:" + lstrPath[iPath]);
						String lstrDescompPath[] = lstrPath[iPath].split("/");
						System.out.println("Elemento compuesto por " + lstrDescompPath.length + " rutas");

						if(lstrDescompPath.length==4){

							String lstrGrupo    = lstrDescompPath[1];
							String lstrPerfil   = lstrDescompPath[2];						
							String lstrFacultad = lstrDescompPath[3];

							System.out.println("Grupo   :" + lstrGrupo);
							System.out.println("Perfil  :" + lstrPerfil);
							System.out.println("Facultad:" + lstrFacultad);

							transferFacultad(lstrGrupo, lstrPerfil, lstrFacultad, 
									TreePerfilesFin, TreePerfilesIni, 
									rootNodePerfilesFin, rootNodePerfilesIni,false);

						}
						else if(lstrDescompPath.length==3){
							System.out.println("Transfiriendo perfil..");
							String lstrGrupo    = lstrDescompPath[1];
							String lstrPerfil   = lstrDescompPath[2];
							System.out.println("Grupo   :" + lstrGrupo);
							System.out.println("Perfil  :" + lstrPerfil);

							transferPerfil(lstrGrupo, lstrPerfil, 
									TreePerfilesFin, TreePerfilesIni, 
									rootNodePerfilesFin, rootNodePerfilesIni,false);

						}
						else if(lstrDescompPath.length==2){
							System.out.println("Transfiriendo prupo..");
							String lstrGrupo    = lstrDescompPath[1];
							System.out.println("Grupo   :" + lstrGrupo);

							transferGrupo(lstrGrupo, 
									TreePerfilesFin, TreePerfilesIni, 
									rootNodePerfilesFin, rootNodePerfilesIni,false);

						}
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				
			}  
		});  
		
		
		moveControls.addMember(leftArrow);		

		
		gridIniPerfiles.addDataArrivedHandler(new DataArrivedHandler(){
			public void onDataArrived(DataArrivedEvent event) {
				System.out.println("onDataArrived[gridIniPerfiles -> gridFinPerfiles]...");
				System.out.println("Parent Mode:" + event.getParentNode());
				System.out.println("Source     :" + event.getSource());
			}
			
		});

		
		gridFinPerfiles.addDataArrivedHandler(new DataArrivedHandler(){
			public void onDataArrived(DataArrivedEvent event) {
				System.out.println("onDataArrived[gridFinPerfiles -> gridIniPerfiles]...");
				System.out.println("Parent Mode:" + event.getParentNode());
				System.out.println("Source     :" + event.getSource());
			}
			
		});
		
		HLayout LayoutPerfiles = new HLayout();
		
		if(pstrVista.equals(VISTA_DETALLE)){
			//LayoutPerfiles.setMembersMargin(10);
			LayoutPerfiles.addMember(gridIniPerfiles);
			LayoutPerfiles.addMember(moveControls);		
			LayoutPerfiles.addMember(gridFinPerfiles);
		}
		else{
			//LayoutPerfiles.setMembersMargin(10);
			//LayoutPerfiles.addMember(gridIniPerfiles);
			//LayoutPerfiles.addMember(moveControls);		
			LayoutPerfiles.addMember(gridFinPerfiles);
			//gridFinPerfiles.setDisabled(true);
			gridFinPerfiles.setDragDataAction(DragDataAction.NONE); 
			gridFinPerfiles.setCanReorderRecords(false);
		}
		
		SectionPerfiles.setExpanded(true);  
		SectionPerfiles.addItem(LayoutPerfiles);
		
	}

	private void pintaTabFacultades(String pstrVista){
		
		TreeFacultadesIni.setModelType(TreeModelType.CHILDREN);  
		TreeFacultadesIni.setNameProperty("Nombre");  
		gridIniFacultades.setDragDataAction(DragDataAction.MOVE);
		gridIniFacultades.setCanReparentNodes(false);
		gridIniFacultades.setCanDropOnLeaves(true);
		//gridIniFacultades.setEdgeMarginSize(10);
		gridIniFacultades.setTitleField("Nombre");
		gridIniFacultades.setTreeFieldTitle("Cat&aacute;logo de facultades extraordinarias");
		gridIniFacultades.setSortFieldAscendingText("Ordenar Asendentemente");
		gridIniFacultades.setSortFieldDescendingText("Ordenar Desendentemente");
		gridIniFacultades.setEmptyMessage("No hay elementos para mostrar");
		
		TreeFacultadesFin.setModelType(TreeModelType.CHILDREN);  
		TreeFacultadesFin.setNameProperty("Nombre");
		gridFinFacultades.setDragDataAction(DragDataAction.MOVE);
		gridFinFacultades.setCanReparentNodes(false);
		gridFinFacultades.setCanDropOnLeaves(true);
		//gridFinFacultades.setEdgeMarginSize(10);
		gridFinFacultades.setTitleField("Nombre");
		gridFinFacultades.setTreeFieldTitle("Facultades extraordinarias a asignar");
		gridFinFacultades.setSortFieldAscendingText("Ordenar Asendentemente");
		gridFinFacultades.setSortFieldDescendingText("Ordenar Desendentemente");
		gridFinFacultades.setEmptyMessage("No hay elementos para mostrar");
		
		
		VStack moveControls = new VStack(10);  
		moveControls.setWidth(32);  
		moveControls.setHeight(74);  
		moveControls.setLayoutAlign(Alignment.CENTER);  

		TransferImgButton rightArrow = new TransferImgButton(TransferImgButton.RIGHT, new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				gridFinFacultades.transferSelectedData(gridIniFacultades);
				//gridIniFacultades.sort();
				//gridFinFacultades.sort();
			}  
		});  
		moveControls.addMember(rightArrow);  

		TransferImgButton leftArrow = new TransferImgButton(TransferImgButton.LEFT, new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				gridIniFacultades.transferSelectedData(gridFinFacultades);
				//gridIniFacultades.sort();
				//gridFinFacultades.sort();
			}  
		});  
		moveControls.addMember(leftArrow);		

		HLayout LayoutFacultades = new HLayout();
		if(pstrVista.equals(VISTA_DETALLE)){		
			//LayoutFacultades.setMembersMargin(10);
			LayoutFacultades.addMember(gridIniFacultades);
			LayoutFacultades.addMember(moveControls);		
			LayoutFacultades.addMember(gridFinFacultades);
		}
		else{
			//LayoutFacultades.setMembersMargin(10);
			//LayoutFacultades.addMember(gridIniFacultades);
			//LayoutFacultades.addMember(moveControls);		
			LayoutFacultades.addMember(gridFinFacultades);
			gridFinFacultades.setDisabled(true);
		}
		
		SectionFacultades.setExpanded(true);  
		SectionFacultades.addItem(LayoutFacultades);
		 
	}
	
	
	
	private void init(String pstrModule){
		
		System.out.println("Inicializando modulo:" + pstrModule);
		
		final String lstrVista = pstrModule;
		
		System.out.println("show Loading...");
		AdminUsrService.getBean(pstrModule, new AsyncCallback<BeanAdminUsr>() {
			public void onFailure(Throwable caught) {
				System.out.println("Error al solicitar informacion al servidor");
				
				GLayout.removeMember(LayoutCarga);
				
			    GLayout.addMember(SectionGDetalle);
			    if(lstrVista.equals(VISTA_DETALLE)){
			    	LayoutBotones.addMember(Regresar);
			    	LayoutBotones.addMember(Continuar);
			    	LayoutBotones.setHeight(50);
			    	LayoutBotones.setAlign(Alignment.CENTER);
			    	GLayout.addMember(LayoutBotones);
			    }				
			}

			public void onSuccess(BeanAdminUsr result) {
				System.out.println("Respuesta exitosa del servidor...");
				System.out.println("Result:" + result);
				
				
				if(result==null){
					
					GLayout.removeMember(LayoutCarga);
					
				    GLayout.addMember(SectionGDetalle);
				    if(lstrVista.equals(VISTA_DETALLE)){
				    	LayoutBotones.addMember(Regresar);
				    	LayoutBotones.addMember(Continuar);
				    	LayoutBotones.setHeight(50);
				    	LayoutBotones.setAlign(Alignment.CENTER);
				    	GLayout.addMember(LayoutBotones);
				    }	
				    			
					return;
				}
				
				BeanAdmin = result;
				Firma     = result.getFirma()!=null?result.getFirma(): "";
				Monto     = result.getMonto()!=null?result.getMonto(): "0.0";
				
				PorcentajeCarga=20;
				
			    new Timer(){
				   public void run(){
					   System.out.println("Porcentaje completado:" + PorcentajeCarga);
					   ProgressBarCarga.setPercentDone(PorcentajeCarga);
					   LabelCarga.setContents("Cargando " + PorcentajeCarga + "%...");
					   
					   if(PorcentajeCarga==100){
						   
							GLayout.removeMember(LayoutCarga);
							
						    GLayout.addMember(SectionGDetalle);
						    if(lstrVista.equals(VISTA_DETALLE)){
						    	LayoutBotones.addMember(Regresar);
						    	LayoutBotones.addMember(Continuar);
						    	LayoutBotones.setHeight(50);
						    	LayoutBotones.setAlign(Alignment.CENTER);
						    	GLayout.addMember(LayoutBotones);
						    }
						    
							if(!BeanAdmin.getMostrarMancomunidad()){
								System.out.println("Ocultar Mancomunidad");
								SectionGDetalle.removeSection(0);
								if(lstrVista.equals(VISTA_DETALLE) && BeanAdmin.getTodasFacultadesExt().size()==0){
									SectionGDetalle.removeSection(1);
								}
								else if(lstrVista.equals(VISTA_DESGLOSE) && BeanAdmin.getFacultadesExtUsuario().size()==0){
									SectionGDetalle.removeSection(1);
								} 
							}
							else if(lstrVista.equals(VISTA_DETALLE) && BeanAdmin.getTodasFacultadesExt().size()==0){
								SectionGDetalle.removeSection(2);
							}
							else if(lstrVista.equals(VISTA_DESGLOSE) && BeanAdmin.getFacultadesExtUsuario().size()==0){
								SectionGDetalle.removeSection(2);
							} 
						    
						   
					   }else if(PorcentajeCarga==20){
						   PorcentajeCarga=30;
						   pintaInformacionMancomunidad(lstrVista);
						   schedule(50);
					   }else if(PorcentajeCarga==30){
						   PorcentajeCarga=80;
						   pintaInformacionPerfilesFix();
						   schedule(50);
					   }else if(PorcentajeCarga==80){
						   PorcentajeCarga=100;
						   pintaInformacionFacultades();
						   schedule(50);
					   }
					   
				   }
			   }.schedule(50);
			}
		});
		
	}
	
	
	private void pintaInformacionMancomunidad(String pstrVista){
		
		if(pstrVista.equals(VISTA_DESGLOSE)){
			//ComboFirma.setDisabled(Boolean.TRUE);
			TextMonto.setDisabled(Boolean.TRUE);
			ComboFirma.setValueMap(Firma);
		}
		ComboFirma.setValue(Firma);
		TextMonto.setValue(Monto);
		

	}

	private void pintaInformacionPerfilesFix(){
		
		/**
		 * Pintamos Arbol izquierdo
		 */
		rootNodePerfilesIni = new TreeNode("Grupos");
		TreePerfilesIni.setRoot(rootNodePerfilesIni);	
		gridIniPerfiles.setData(TreePerfilesIni);   
		for(int iFacultades=0;iFacultades<BeanAdmin.getFacultadesUsuarioNoSeleccionadas().size();iFacultades++){
			try{
				String        stFacultadAux     = BeanAdmin.getFacultadesUsuarioNoSeleccionadas().get(iFacultades);
				String        lstrNodes[]       = stFacultadAux.split("@");
				String        lstrGrupo         = lstrNodes[0];
				String        lstrPerfil        = lstrNodes[1];
				String        lstrFacultad      = lstrNodes[2];
				TreeNode      nodoGrupo         = null;
				TreeNode      nodoPerfil        = null;      
				nodoGrupo = agregarNodo(lstrGrupo    , rootNodePerfilesIni, TreePerfilesIni);
				if(nodoGrupo!=null){
					nodoPerfil = agregarNodo(lstrPerfil   , nodoGrupo          , TreePerfilesIni);
					if(nodoPerfil!=null){
						String   detalleFacultad[] = null;
						try{
							detalleFacultad = lstrFacultad.split("&");
						}
						catch(Exception e){
							e.printStackTrace();
						}

						if(detalleFacultad!=null  && detalleFacultad.length==2){
							PartsTreeNode nodoAux = new PartsTreeNode(detalleFacultad[1],detalleFacultad[0]);
							TreePerfilesIni.add(nodoAux, nodoPerfil);
							System.out.println("Facultad no seleccionada[" + stFacultadAux + "]agregada....");					}
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		//gridIniPerfiles.getData().openAll();
		
		
		/**
		 * Pintamos Arbol derecho
		 */
		rootNodePerfilesFin = new TreeNode("Grupos");
		TreePerfilesFin.setRoot(rootNodePerfilesFin);
		gridFinPerfiles.setData(TreePerfilesFin);   
		for(int iFacultades=0;iFacultades<BeanAdmin.getFacultadesUsuario().size();iFacultades++){
			try{
				String        stFacultadAux     = BeanAdmin.getFacultadesUsuario().get(iFacultades);
				String        lstrNodes[]       = stFacultadAux.split("@");
				String        lstrGrupo         = lstrNodes[0];
				String        lstrPerfil        = lstrNodes[1];
				String        lstrFacultad      = lstrNodes[2];
				TreeNode      nodoGrupo         = null;
				TreeNode      nodoPerfil        = null;      
				nodoGrupo = agregarNodo(lstrGrupo  , rootNodePerfilesFin , TreePerfilesFin);
				if(nodoGrupo!=null){
					nodoPerfil = agregarNodo(lstrPerfil , nodoGrupo, TreePerfilesFin);
					if(nodoPerfil!=null){
						String   detalleFacultad[] = null;
						try{
							detalleFacultad = lstrFacultad.split("&");
						}
						catch(Exception e){
							e.printStackTrace();
						}

						if(detalleFacultad!=null  && detalleFacultad.length==2){
							PartsTreeNode nodoAux = new PartsTreeNode(detalleFacultad[1],detalleFacultad[0]);
							TreePerfilesFin.add(nodoAux, nodoPerfil);
							System.out.println("Facultad seleccionada[" + stFacultadAux + "]agregada....");
						}
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		//gridFinPerfiles.getData().openAll();
		
	}
	
	private TreeNode agregarNodo(String pstrNodo, TreeNode parent, Tree tree){
		TreeNode nodoAux = null;
		String detalleNodo[] = null;
		try{
			detalleNodo = pstrNodo.split("&");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		if(detalleNodo==null || detalleNodo.length!=2)
			return nodoAux;
		
		nodoAux	= new PartsTreeNode(detalleNodo[1],detalleNodo[0]);
		TreeNode nodes[] = tree.getChildren(parent);
		boolean nodoExiste = false;
		for (TreeNode treeNode : nodes) {
			if(treeNode.getAttribute("Id").equals(nodoAux.getAttribute("Id"))){
				nodoAux = treeNode;
				nodoExiste = true;
				//System.out.println("El nodo existe...");
				break;
			}
		}
		
		if(!nodoExiste){
			//System.out.println("Agregando nodo...");
			tree.add(nodoAux, parent);
		}
		
		return nodoAux;
	}
	
	
	private void pintaInformacionFacultades(){
		
		/**
		 * Cargando facultades no seleccionadas
		 */
		rootNodeFacultadesIni = new PartsTreeNode("Facultades");
		TreeFacultadesIni.setRoot(rootNodeFacultadesIni);		
		gridIniFacultades.setData(TreeFacultadesIni);   
		
		for(int iFacultadNoSel=0;iFacultadNoSel<BeanAdmin.getFacultadesUsuarioExtNoSeleccionadas().size();iFacultadNoSel++){
			String pstrFacultad = BeanAdmin.getFacultadesUsuarioExtNoSeleccionadas().get(iFacultadNoSel);
			String facultadAux[] = pstrFacultad.split("&");
			String lstrClave = facultadAux[0];
			String lstrDesc  = facultadAux[1];
			PartsTreeNode nodoAux = new PartsTreeNode(lstrDesc,lstrClave);
			TreeFacultadesIni.add(nodoAux, rootNodeFacultadesIni);
			System.out.println("Facultad ext no seleccionada[" + pstrFacultad + "] agregada...");
			
		}
		gridIniFacultades.getData().openAll();
		
		/**
		 * Cargando facultades seleccionadas
		 */
		rootNodeFacultadesFin = new PartsTreeNode("Facultades");
		TreeFacultadesFin.setRoot(rootNodeFacultadesFin);		
		gridFinFacultades.setData(TreeFacultadesFin);   
		
		for(int iFacultadSel=0;iFacultadSel<BeanAdmin.getFacultadesExtUsuario().size();iFacultadSel++){
			String pstrFacultad = BeanAdmin.getFacultadesExtUsuario().get(iFacultadSel);
			String facultadAux[] = pstrFacultad.split("&");
			String lstrClave = facultadAux[0];
			String lstrDesc  = facultadAux[1];
			PartsTreeNode nodoAux = new PartsTreeNode(lstrDesc,lstrClave);
			TreeFacultadesFin.add(nodoAux, rootNodeFacultadesFin);
			System.out.println("Facultad ext seleccionada[" + pstrFacultad + "] agregada...");
		}
		gridFinFacultades.getData().openAll();
		
	}
	
	
	private void transferFacultad(String   pstrGrupo , String   pstrPerfil , String pstrFacultad, 
			                        Tree     TreeOrigen, Tree     TreeDestino, 
			                        TreeNode NodoOrigen, TreeNode NodoDestino,
			                        boolean unSoloPerfil){
		
		System.out.println("Verificando si existe el Grupo...");
		TreeNode grupos[] = TreeDestino.getChildren(NodoDestino);
		boolean grupoExist = false;
		TreeNode grupoAux = null;
		for(int iGrupos=0;iGrupos<grupos.length;iGrupos++){
			if(grupos[iGrupos].getAttribute("Nombre").equals(pstrGrupo)){
				System.out.println("El grupo existe...");
				grupoExist = true;
				grupoAux = grupos[iGrupos];
				break;
			}
		}
		
		if(!grupoExist){
			System.out.println("Buscando informacion del grupo...");
			TreeNode gruposIni[] = TreeOrigen.getChildren(NodoOrigen);
			for(int iGrupos=0;iGrupos<gruposIni.length;iGrupos++){
				if(gruposIni[iGrupos].getAttribute("Nombre").equals(pstrGrupo)){
					System.out.println("El grupo fue encontrado...");
					grupoAux = new PartsTreeNode(gruposIni[iGrupos].getAttribute("Nombre"),gruposIni[iGrupos].getAttribute("Id"));
					break;
				}			
			}
			System.out.println("Creando grupo...");
			TreeDestino.add(grupoAux, NodoDestino);
		}
		
		System.out.println("Verificando si el perfil existe...");
		TreeNode perfiles[] = TreeDestino.getChildren(grupoAux);
		boolean perfilExist = false;
		TreeNode perfilAux = null;
		
		
		for(int iPerfil=0;iPerfil<perfiles.length;iPerfil++){
			if(perfiles[iPerfil].getAttribute("Nombre").equals(pstrPerfil)){
				System.out.println("El perfil existe...");
				perfilExist = true;
				perfilAux = perfiles[iPerfil];
				break;
			}
		}
		
		//if(unSoloPerfil && perfiles.length==1 && perfilExist)
		if(unSoloPerfil && perfiles.length>=1 && !perfilExist){
			if(!grupoExist)
				TreeDestino.remove(grupoAux);
			SC.say("Solo se puede vincular un solo perfil");
			return;
		}		
		
		if(!perfilExist){
			System.out.println("Buscando informacion del perfil[" + pstrPerfil + "]...");
			TreeNode GrupoIni[] = TreeOrigen.getChildren(NodoOrigen);
			boolean perfilOk = false;
			for(int iGrupo=0;iGrupo<GrupoIni.length;iGrupo++){
				System.out.println("Grupo:" + GrupoIni[iGrupo].getAttribute("Nombre"));
				TreeNode PerfilIni[] = TreeOrigen.getChildren(GrupoIni[iGrupo]);
				for(int iPerfil=0;iPerfil<PerfilIni.length;iPerfil++){
					System.out.println("Perfil:" + PerfilIni[iPerfil].getAttribute("Nombre"));
					if(PerfilIni[iPerfil].getAttribute("Nombre").equals(pstrPerfil)){
						System.out.println("Agregando Perfil...");
						perfilAux = new PartsTreeNode(PerfilIni[iPerfil].getAttribute("Nombre"),PerfilIni[iPerfil].getAttribute("Id"));
						TreeDestino.add(perfilAux, grupoAux);
						System.out.println("Perfil Agregando...");
						perfilOk = true;
						break;
					}
				}
				if(perfilOk)
					break;
			}
			
		}
		
		System.out.println("Buscando informacion de la facultad[" + pstrFacultad + "]...");
		TreeNode GrupoIni[] = TreeOrigen.getChildren(NodoOrigen);
		TreeNode facultadAux = null;
		TreeNode facultadEli = null;
		TreeNode perfilEli   = null;
		TreeNode grupoEli    = null;

		for(int iGrupo=0;iGrupo<GrupoIni.length;iGrupo++){
			System.out.println("Grupo:" + GrupoIni[iGrupo].getAttribute("Nombre"));
			grupoEli = GrupoIni[iGrupo];
			TreeNode PerfilIni[] = TreeOrigen.getChildren(GrupoIni[iGrupo]);
			if(grupoAux.getAttribute("Nombre").equals(GrupoIni[iGrupo].getAttribute("Nombre"))){
				for(int iPerfil=0;iPerfil<PerfilIni.length;iPerfil++){
					System.out.println("Perfil:" + PerfilIni[iPerfil].getAttribute("Nombre"));
					TreeNode FacultadIni[] = TreeOrigen.getChildren(PerfilIni[iPerfil]);
					if(perfilAux.getAttribute("Nombre").equals(PerfilIni[iPerfil].getAttribute("Nombre"))){
						for(int iFacul=0;iFacul<FacultadIni.length;iFacul++){
							System.out.println("Facultad:" + FacultadIni[iFacul].getAttribute("Nombre"));
							if(FacultadIni[iFacul].getAttribute("Nombre").replaceAll(",", " ").equals(pstrFacultad.replaceAll(",", " "))){
								System.out.println("La facultad fue encontrada...");
								perfilEli = PerfilIni[iPerfil];
								facultadEli = FacultadIni[iFacul];
								facultadAux = new PartsTreeNode(FacultadIni[iFacul].getAttribute("Nombre"),FacultadIni[iFacul].getAttribute("Id"));

								System.out.println("Eliminando Facultad [" + facultadAux.getAttribute("Id") + "," + facultadAux.getAttribute("Nombre") + "]");
								if(TreeOrigen.remove(facultadEli)){
									System.out.println("Facultad Eliminada...");
									System.out.println("Creando facultad...");
									TreeDestino.add(facultadAux, perfilAux);
									System.out.println("facultad agregada...");
								}
								else{
									System.out.println("La Facultad no pudo ser eliminada");
									SC.say("La facultad no pudo ser eliminada");
								}

								break;
							}
						}
						break;
					}

				}
				break;
			}
			
		}
		
		

		
		if(TreeOrigen.getChildren(perfilEli).length==0){
			TreeOrigen.remove(perfilEli);
			System.out.println("Perfil eliminado...");
		}
		
		if(TreeOrigen.getChildren(grupoEli).length==0){
			TreeOrigen.remove(grupoEli);
			System.out.println("Grupo Eliminado...");
		}
		
		//NodoOrigen  = TreeOrigen.getRoot();
		//NodoDestino = TreeDestino.getRoot();
		
	}
	
	private void transferPerfil(String   pstrGrupo , String   pstrPerfil, 
            					Tree     TreeOrigen, Tree     TreeDestino, 
            					TreeNode NodoOrigen, TreeNode NodoDestino,
            					boolean unSoloPerfil){
	
		System.out.println("Verificando si existe el Grupo...");
		TreeNode grupos[] = TreeDestino.getChildren(NodoDestino);
		boolean grupoExist = false;
		TreeNode grupoAux = null;
		for(int iGrupos=0;iGrupos<grupos.length;iGrupos++){
			if(grupos[iGrupos].getAttribute("Nombre").equals(pstrGrupo)){
				System.out.println("El grupo existe...");
				grupoExist = true;
				grupoAux = grupos[iGrupos];
				break;
			}
		}
		
		if(!grupoExist){
			System.out.println("Buscando informacion del grupo...");
			TreeNode gruposIni[] = TreeOrigen.getChildren(NodoOrigen);
			for(int iGrupos=0;iGrupos<gruposIni.length;iGrupos++){
				if(gruposIni[iGrupos].getAttribute("Nombre").equals(pstrGrupo)){
					System.out.println("El grupo fue encontrado...");
					grupoAux = new PartsTreeNode(gruposIni[iGrupos].getAttribute("Nombre"),gruposIni[iGrupos].getAttribute("Id"));
					break;
				}			
			}
			System.out.println("Creando grupo...");
			TreeDestino.add(grupoAux, NodoDestino);
		}
		
		
		System.out.println("Verificando si el perfil existe...");
		TreeNode perfiles[] = TreeDestino.getChildren(grupoAux);
		boolean perfilExist = false;
		TreeNode perfilAux = null;
		
		

		for(int iPerfil=0;iPerfil<perfiles.length;iPerfil++){
			if(perfiles[iPerfil].getAttribute("Nombre").equals(pstrPerfil)){
				System.out.println("El perfil existe...");
				perfilExist = true;
				perfilAux = perfiles[iPerfil];
				break;
			}
		}
		
		if(unSoloPerfil && perfiles.length>=1 && !perfilExist){
			if(!grupoExist)
				TreeDestino.remove(grupoAux);
			SC.say("Solo se puede vincular un solo perfil");
			return;
		}		
		
		if(!perfilExist){
			System.out.println("Buscando informacion del perfil[" + pstrPerfil + "]...");
			TreeNode GrupoIniP[] = TreeOrigen.getChildren(NodoOrigen);
			boolean perfilOk = false;
			for(int iGrupo=0;iGrupo<GrupoIniP.length;iGrupo++){
				System.out.println("Grupo:" + GrupoIniP[iGrupo].getAttribute("Nombre"));
				TreeNode PerfilIni[] = TreeOrigen.getChildren(GrupoIniP[iGrupo]);
				for(int iPerfil=0;iPerfil<PerfilIni.length;iPerfil++){
					System.out.println("Perfil:" + PerfilIni[iPerfil].getAttribute("Nombre"));
					if(PerfilIni[iPerfil].getAttribute("Nombre").equals(pstrPerfil)){
						System.out.println("Agregando Perfil...");
						perfilAux = new PartsTreeNode(PerfilIni[iPerfil].getAttribute("Nombre"),PerfilIni[iPerfil].getAttribute("Id"));
						TreeDestino.add(perfilAux, grupoAux);
						System.out.println("Perfil Agregando...");
						perfilOk = true;
						break;
					}
				}
				if(perfilOk)
					break;
			}
		}
		
		/*
		if(!perfilExist){
			System.out.println("Buscando informacion del perfil...Grupo[" + pstrGrupo + "]");
			TreeNode grupoIni[] = TreeOrigen.getChildren(NodoOrigen);
			TreeNode grupo = null;
			System.out.println("No. de grupos:" + grupoIni.length);
			for(int igrupo=0;igrupo<grupoIni.length;igrupo++){
				System.out.println("Grupo:" + grupoIni[igrupo].getAttribute("Nombre"));
				if(grupoIni[igrupo].getAttribute("Nombre").equals("pstrGrupo")){
					System.out.println("Grupo encontrado...");
					grupo = grupoIni[igrupo];
					TreeNode perfilIni[] = TreeOrigen.getChildren(grupoIni[igrupo]);
					System.out.println("No. de Perfiles:" + perfilIni.length);
					for(int iPerfiles=0;iPerfiles<perfilIni.length;iPerfiles++){
						System.out.println("Perfil:" + perfilIni[iPerfiles].getAttribute("Nombre"));
						if(perfilIni[iPerfiles].getAttribute("Nombre").equals(pstrPerfil)){
							System.out.println("El perfil fue encontrado...");
							perfilAux = new PartsTreeNode(perfilIni[iPerfiles].getAttribute("Nombre"),perfilIni[iPerfiles].getAttribute("Id"));
							break;
						}			
						
					}
					break;
				}
			}
			System.out.println("Creando perfil...");
			TreeDestino.add(perfilAux, grupo);
		}
		*/
		
		
		System.out.println("Buscando informacion del perfil[" + pstrPerfil + "]...");
		TreeNode GrupoIni[] = TreeOrigen.getChildren(NodoOrigen);
		TreeNode perfilEli = null;
		TreeNode grupoEli = null;
		boolean perfilCopiado = false;
		for(int iGrupo=0;iGrupo<GrupoIni.length;iGrupo++){
			System.out.println("Grupo:" + GrupoIni[iGrupo].getAttribute("Nombre"));
			grupoEli = GrupoIni[iGrupo];
			TreeNode PerfilIni[] = TreeOrigen.getChildren(GrupoIni[iGrupo]);
			for(int iPerfil=0;iPerfil<PerfilIni.length;iPerfil++){
				System.out.println("Perfil:" + PerfilIni[iPerfil].getAttribute("Nombre"));
				if(PerfilIni[iPerfil].getAttribute("Nombre").equals(pstrPerfil)){
					System.out.println("Perfil encontrado...");
					perfilEli = PerfilIni[iPerfil];
					System.out.println("Guardando Facultades...");
					TreeNode FacultadIni[] = TreeOrigen.getChildren(PerfilIni[iPerfil]);
					for(int iFacul=0;iFacul<FacultadIni.length;iFacul++){
						System.out.println("Facultad:" + FacultadIni[iFacul].getAttribute("Nombre"));
						TreeNode facultadEliP = FacultadIni[iFacul];
						TreeNode facultadAuxP = new PartsTreeNode(FacultadIni[iFacul].getAttribute("Nombre"),FacultadIni[iFacul].getAttribute("Id"));
						System.out.println("Creando facultad...");
						TreeDestino.add(facultadAuxP, perfilAux);
						System.out.println("facultad agregada...");
						System.out.println("Eliminando Facultad...");
						TreeOrigen.remove(facultadEliP);
						System.out.println("Facultad Eliminada...");
					}
					
					
					System.out.println("Eliminando Perfil...");
					TreeOrigen.remove(perfilEli);
					System.out.println("Perfil Eliminada...");
					
					
					if(TreeOrigen.getChildren(grupoEli).length==0){
						TreeOrigen.remove(grupoEli);
						System.out.println("Grupo Eliminado...");
					}
					
					
					perfilCopiado = true;
					
					break;
				}
			}
			if(perfilCopiado)
				break;
		}
		
		//NodoOrigen  = TreeOrigen.getRoot();
		//NodoDestino = TreeDestino.getRoot();
		
	}
	
	private void transferGrupo(String   pstrGrupo, 
							   Tree     TreeOrigen, Tree     TreeDestino, 
							   TreeNode NodoOrigen, TreeNode NodoDestino,
							   boolean unSoloPerfil){
		
		
		
		System.out.println("Verificando si existe el Grupo...");
		TreeNode grupos[] = TreeDestino.getChildren(NodoDestino);
		boolean grupoExist = false;
		TreeNode grupoAux = null;
		for(int iGrupos=0;iGrupos<grupos.length;iGrupos++){
			if(grupos[iGrupos].getAttribute("Nombre").equals(pstrGrupo)){
				System.out.println("El grupo existe...");
				grupoExist = true;
				grupoAux = grupos[iGrupos];
				break;
			}
		}
		
		if(!grupoExist){
			System.out.println("Buscando informacion del grupo...");
			TreeNode gruposIni[] = TreeOrigen.getChildren(NodoOrigen);
			for(int iGrupos=0;iGrupos<gruposIni.length;iGrupos++){
				if(gruposIni[iGrupos].getAttribute("Nombre").equals(pstrGrupo)){
					System.out.println("El grupo fue encontrado...");
					grupoAux = new PartsTreeNode(gruposIni[iGrupos].getAttribute("Nombre"),gruposIni[iGrupos].getAttribute("Id"));
					break;
				}			
			}
			System.out.println("Creando grupo...");
			TreeDestino.add(grupoAux, NodoDestino);
		}
		
		System.out.println("Buscando informacion de grupo [" + pstrGrupo + "]...");
		TreeNode GrupoIni[] = TreeOrigen.getChildren(NodoOrigen);
		TreeNode grupoEli = null;
		for(int iGrupo=0;iGrupo<GrupoIni.length;iGrupo++){
			System.out.println("Grupo:" + GrupoIni[iGrupo].getAttribute("Nombre"));
			if(GrupoIni[iGrupo].getAttribute("Nombre").equals(pstrGrupo)){
				System.out.println("Grupo encontrado...");
				grupoEli = GrupoIni[iGrupo];
				TreeNode PerfilIni[] = TreeOrigen.getChildren(GrupoIni[iGrupo]);
				
				if(unSoloPerfil && PerfilIni.length>1){
					SC.say("No se puede vincular todo un producto, por favor seleccione un perfil");
					return;
				}

				for(int iPerfil=0;iPerfil<PerfilIni.length;iPerfil++){
					System.out.println("Transfiriendo perfil[" + PerfilIni[iPerfil].getAttribute("Nombre") + "]");
					transferPerfil(pstrGrupo , PerfilIni[iPerfil].getAttribute("Nombre"), 
							TreeOrigen, TreeDestino, 
							NodoOrigen, NodoDestino,unSoloPerfil);					

				}
				break;
			}
		}
		
		
		System.out.println("Eliminando grupo...");
		TreeOrigen.remove(grupoEli);
		System.out.println("Grupo Eliminado...");
		
		
		//NodoOrigen  = TreeOrigen.getRoot();
		//NodoDestino = TreeDestino.getRoot();
		
	}
	
}
