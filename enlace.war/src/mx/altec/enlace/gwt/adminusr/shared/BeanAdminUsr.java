package mx.altec.enlace.gwt.adminusr.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;


public class BeanAdminUsr implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9077998317179052122L;
	
	private ArrayList<GrupoPerfiles> listTodosGruposPerfiles     = new ArrayList<GrupoPerfiles>();
	private ArrayList<Facultad>      listTodssFacultadesExt      = new ArrayList<Facultad>();
	private String                   gstrFirma                   = ""; 
	private String                   gstrMonto                   = ""; 
	private ArrayList<String>        galtFacultadesUsuarioExtIni = new ArrayList<String>();
	private ArrayList<String>        galtFacultadesUsuarioIni    = new ArrayList<String>();
	private ArrayList<String>        galtFacultadesUsuarioFin    = new ArrayList<String>();
	private ArrayList<String>        galtFacultadesUsuarioExtFin = new ArrayList<String>();
	private boolean                  gbolMostrarMancomunidad     = false;
	
	
	public void setMostrarMancomunidad(boolean pbolVal){
		gbolMostrarMancomunidad = pbolVal;
	}
	
	public boolean getMostrarMancomunidad(){
		return gbolMostrarMancomunidad;
	}
	
	public void setTodosGrupoPerfiles(ArrayList<GrupoPerfiles> pobjValue){
		listTodosGruposPerfiles    = pobjValue;
	}

	public ArrayList<GrupoPerfiles> getTodosGrupoPerfiles(){
		return listTodosGruposPerfiles;
	}
	
	public void setTodasFacultadesExt(ArrayList<Facultad> pobjValue){
		listTodssFacultadesExt    = pobjValue;
	}

	public ArrayList<Facultad> getTodasFacultadesExt(){
		return listTodssFacultadesExt;
	}
	
	public void setFirma(String pstrValue){
		gstrFirma = pstrValue;
	}
	
	public void setMonto(String pstrValue){
		gstrMonto    = pstrValue;
	}
	

	public void setFacultadesUsuario(ArrayList<String> pstrValue){
		galtFacultadesUsuarioFin    = pstrValue;
	}
	
	public void setFacultadesUsuarioNoSeleccionadas(ArrayList<String> pstrValue){
		galtFacultadesUsuarioIni    = pstrValue;
	}
	
	public void setFacultadesUsuarioExtNoSeleccionadas(ArrayList<String> pstrValue){
		galtFacultadesUsuarioExtIni    = pstrValue;
	}
	
	public void setFacultadesExtUsuario(ArrayList<String> pstrValue){
		galtFacultadesUsuarioExtFin    = pstrValue;
	}
	
	public String getFirma(){
		return gstrFirma;
	}
	
	
	public String getMonto(){
		return gstrMonto;
	}
	
	
	public ArrayList<String> getFacultadesUsuario(){
		return galtFacultadesUsuarioFin;
	}
	
	
	public ArrayList<String> getFacultadesExtUsuario(){
		return galtFacultadesUsuarioExtFin;
	}
	
	
	public ArrayList<String> getFacultadesUsuarioNoSeleccionadas(){
		return galtFacultadesUsuarioIni;
	}
	
	public ArrayList<String> getFacultadesUsuarioExtNoSeleccionadas(){
		return galtFacultadesUsuarioExtIni;
	}
	
	public static BeanAdminUsr getBeanFalso(int numGrupos, int numPerfiles, int numFacultades, 
			                int numFacultadesExt, int numFacultadesUsr, int numFacultadesUsrExt){
		
		BeanAdminUsr bean = new BeanAdminUsr();
		
		ArrayList<GrupoPerfiles> ListaTodosGrupoPerfiles = new ArrayList<GrupoPerfiles>();
		ArrayList<Facultad>      ListaTodosFacultadesExt = new ArrayList<Facultad>();
		ArrayList<String>        FacultadesUsuario       = new ArrayList<String>();
		ArrayList<String>        FacultadesUsuarioExt    = new ArrayList<String>();
		
		for(int iGrupos=0;iGrupos<numGrupos;iGrupos++){
			GrupoPerfiles grupoAux = new GrupoPerfiles();
			grupoAux.setCveGrupo("cveGrupo" + (iGrupos+1));
			grupoAux.setDescripcion("desGrupo" + (iGrupos+1));
			ArrayList<PerfilPrototipo> listaPerfilesAux = new ArrayList<PerfilPrototipo>();
			for(int iPerfiles=0;iPerfiles<numPerfiles;iPerfiles++){
				PerfilPrototipo perfilAux = new PerfilPrototipo();
				perfilAux.setCvePerfil("cvePerfil" + (iGrupos+1) + (iPerfiles+1));
				perfilAux.setDescripcion("desPerfil" + (iGrupos+1) + (iPerfiles+1));
				ArrayList<Facultad> listaFacultadesAux = new ArrayList<Facultad>(); 
				for(int iFacultad=0;iFacultad<numFacultades;iFacultad++){
					Facultad facultadAux = new Facultad();
					facultadAux.setCveFacultad("cveFacultad" + (iGrupos+1) + (iFacultad+1));
					facultadAux.setDescripcion("desFacultad" + (iGrupos+1) + (iFacultad+1));
					listaFacultadesAux.add(facultadAux);
				}
				perfilAux.setFacultades(listaFacultadesAux);
				listaPerfilesAux.add(perfilAux);
			}
			grupoAux.setPerfiles(listaPerfilesAux);
			ListaTodosGrupoPerfiles.add(grupoAux);
		}
		
		for(int iFacultadesExt=0;iFacultadesExt<numFacultadesExt;iFacultadesExt++){
			Facultad facultadExtAux = new Facultad();
			facultadExtAux.setCveFacultad("cveFacultad" + (iFacultadesExt+1));
			facultadExtAux.setDescripcion("desFacultad" + (iFacultadesExt+1));
			ListaTodosFacultadesExt.add(facultadExtAux);
		}
		
		for(int iFacultadesUsr=0;iFacultadesUsr<numFacultadesUsr;iFacultadesUsr++){
			String lstrFacultadUsr = "cveGrupo1&desGrupo1"   + "@" + 
									 "cvePerfil1&desPerfil1" + "@" +
			                         "cveFacultad" + (iFacultadesUsr+1) + "&" + "desFacultad" + (iFacultadesUsr+1); 
			FacultadesUsuario.add(lstrFacultadUsr);
		}
		
		for(int iFacultadesUsrExt=0;iFacultadesUsrExt<numFacultadesUsrExt;iFacultadesUsrExt++){
			String lstrFacultadUsr = "cveFacultad" + (iFacultadesUsrExt+1) + "&"  + "desFacultad" + (iFacultadesUsrExt+1); 
			FacultadesUsuarioExt.add(lstrFacultadUsr);
		}
		
		
		bean.setTodosGrupoPerfiles(ListaTodosGrupoPerfiles);
		bean.setTodasFacultadesExt(ListaTodosFacultadesExt);
		bean.setFacultadesUsuario(FacultadesUsuario);
		bean.setFacultadesExtUsuario(FacultadesUsuarioExt);
		bean.setFirma("C");
		bean.setMonto("250000");
		bean.setMostrarMancomunidad(true);
		
		return bean;
		
	}	
}
