<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.servlets.*,mx.altec.enlace.ches.*,java.util.*,java.text.*"%><%--
Nombre: ChesConsultaCheqRes
@author Rafael Martinez Montiel
Muestra el resultado de la consulta y permite cancelar cheques
--%><html>
<head><title>Chequera de Seguridad</title><!-- ChesConsultaCheqRes.jsp -->
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />

<%-- jsp:useBean id="chesConsCheques" scope="session" class="java.util.List" /--%>
<%-- jsp:useBean id="start" scope="request"  class="java.lang.Integer" /--%>
<%-- jsp:useBean id="despl" scope="session"  class="java.lang.Integer" /--%>


<jsp:useBean id="chesArchivoExp" scope="session" class="java.lang.String" />
<%-- <jsp:useBean id="chesTipoCheque" scope="session" class="java.lang.String" /> --%>
<jsp:useBean id="ctaCargo" scope="session" class="java.lang.String" />
<jsp:useBean id="chesMsgRet" scope="request" class="java.lang.String" />
<%!
  SimpleDateFormat sdf = new SimpleDateFormat ( "dd/MM/yyyy" );
  	java.text.DecimalFormat nf = new java.text.DecimalFormat();	%>

<%
	Integer start = (Integer) request.getAttribute("start");
	Integer despl = (Integer) session.getAttribute("despl");
	List chesConsCheques = (List) session.getAttribute("chesConsCheques");
	
        nf .applyPattern("#,###,###,###0.0#");
        nf .setDecimalSeparatorAlwaysShown(true);
        nf .setGroupingSize(3);
      nf .setGroupingUsed(true);
        nf .setMinimumFractionDigits(2);
        nf .setMaximumFractionDigits(2);
%>
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language='javascript'>

function js_desplazamiento(index){

    document.frmCancelar.action = "ChesConsulta";
    document.frmCancelar.modulo.value = "cheques";
    document.frmCancelar.opcion.value = 2;
    document.frmCancelar.start.value = index;
    document.frmCancelar.submit();
}

function checkAll() {
	 for (var i=0; i < document.frmCancelar.elements.length;i++) {
		var e = document.frmCancelar.elements[i];
		if (e.name != 'allbox')
			e.checked = document.frmCancelar.allbox.checked;
 	}
}

function countSeleccionados(){
	var c = 0;

	for(var i = 0;i < document.frmCancelar.elements.length; i++){

		var e = document.frmCancelar.elements[i];
		if(e.name == 'reg' ){
			if(e.checked == true){
				c++;
			}
		}
	}
	return c;
}
var respuesta = -1;

function js_Cancelar(){
	document.frmCancelar.action = "ChesCancelar";

	var c = countSeleccionados();
   document.frmCancelar.countReg.value = c;
	if(0 == c){
		cuadroDialogo( "Seleccione al menos un registro..." ,3);
	} else if ( 1 == c){
		cuadroDialogo("Est&aacute; seguro que desea cancelar el registro?",2);
	} else {
		cuadroDialogo("Est&aacute; seguro que desea cancelar los registros?",2);
	}
}
function continua(){
	if (respuesta == 1){
		document.frmCancelar.action = "ChesCancelar";
		document.frmCancelar.submit();
	}
}

    </SCRIPT>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************/
// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%= baseResource.getStrMenu() %>
    <%= encabezado %>

    <% if ( ! chesMsgRet.equals ("") ){%>
        <script language="javascript">
            		cuadroDialogoEspecial( "<%= chesMsgRet %>" ,1);
        </script>
    <%}%>
<form name="frmCancelar" action="ChesCancelar" method="POST">
<input type="hidden" name='modulo' id="modulo" value="" >
<input type="hidden" name="opcion" id="opcion" value="">
<input type="hidden" name="countReg" id="countReg" value="">
<input type="hidden" name="start" id="start" value="<%= start %>" >
<table align="center"><tr><td align="left" class="tabmovtex">N&uacute;mero de Cuenta:<%= ctaCargo %></td></tr><%--// Modificacion RMM 20030207 --%>
<tr><td><%--// Modificacion RMM 20030207 --%>
<table align="center" >
    <tr class="tabdatfonazu">
        <th class='tittabdat'><input type='checkbox' name='allbox' value="checkbox" onclick='checkAll();'> Todos</th>
        <TH class='tittabdat'> No. de Cuenta        </th>
        <TH class='tittabdat'> No. de Cheque        </th>
        <th class='tittabdat'> Importe</th>
        <th class='tittabdat'> Estatus</th>
        <th class='tittabdat'> Fecha de Libramiento</th>
        <th class='tittabdat'> Fecha l&iacute;mite de pago</th>
        <th class='tittabdat'> Fecha de Pago / Cancelaci&oacute;n</th>
    </tr>
    <%
        ListIterator it = chesConsCheques.listIterator( start.intValue() );
        int reg = 0;
        ChesCheque cheque = null;
        boolean claro = false;
        while( it.hasNext() && despl.intValue() > reg){
            reg++;
            cheque = (ChesCheque )it.next();
            if(null == cheque){
                continue;
            }
    %>
        <tr class='<%= ((claro)?"textabdatcla":"textabdatobs") %>'>
            <TD class="tabmovtex" align="center">
                <%if(cheque.getStatus().startsWith("P") ){%>
                    <INPUT type="checkbox" name='reg' value="<%= cheque.getNumCuenta()%>@<%= cheque.getNumCheque()%>">
                <%} %>
                &nbsp;
            </td>
            <td class="tabmovtex" align="center"><%= cheque.getNumCuenta() %> &nbsp;</td>
            <td class="tabmovtex" align="center"><%= cheque.getNumCheque() %>&nbsp;</td>
            <td class="tabmovtex" align="right">$<%= nf.format( ( (double)( ( (double)cheque.getImporte() )/ 100) ) ) %>&nbsp;</td>
            <td class="tabmovtex" align="center"><%= cheque.getStatus() %>&nbsp;</td>
            <td class="tabmovtex" align="center"><%if(null != cheque.getFechaLibramiento()) out.print( sdf.format (cheque.getFechaLibramiento().getTime()) ); %>&nbsp;</td>
            <td class="tabmovtex" align="center">
				<%if(null != cheque.getFechaLimPago())
						{
							String sfechaLimPago="";
							sfechaLimPago = ""+sdf.format (cheque.getFechaLimPago().getTime()) ;
							if (sfechaLimPago.equals("31/12/9999")) sfechaLimPago = "Sin&nbsp;L&iacute;mite";
							out.print(sfechaLimPago);
						}
				%>
			&nbsp;</td>
            <td class="tabmovtex" align="center"><% if (null != cheque.getStatus() && cheque.getStatus().startsWith("L")){
                                        if(null != cheque.getFechaPago()){
                                            out.print( sdf.format (cheque.getFechaPago().getTime()));
                                        }
                                    } else if (null != cheque.getStatus() && cheque.getStatus().startsWith("C")){
                                        if(null != cheque.getFechaCancelacion().getTime()){
                                            out.print( sdf.format (cheque.getFechaCancelacion().getTime()));
                                        }
                                    }%>&nbsp;</td>
        </tr>
<%
        claro = !claro;
    }
%>
</TABLE>
<table  align="center">
<tr><td align="center" class='texfootpaggri'>
        <%
        if(start.intValue() - despl.intValue() >= 0){%><a href='javascript:js_desplazamiento(<%= start.intValue() - despl.intValue() %>)' class='texfootpaggri'>anterior</a>&nbsp;<%}
        if(chesConsCheques.size() > 1){
            for(int i = 0;i < chesConsCheques.size();i+= despl.intValue()){
                if(start.intValue() != i){
                    %><a href='javascript:js_desplazamiento(<%= i %>)' class='texfootpaggri'><%=(i /despl.intValue()) +1 %></a><%
                    }else{
                    %><%= ( i / despl.intValue() ) + 1%><%
                    }%>&nbsp;<%
            }
        }
        if(start.intValue() + despl.intValue() < chesConsCheques.size()){%>&nbsp;<a href='javascript:js_desplazamiento(<%= start.intValue() + despl.intValue() %>)' class='texfootpaggri'>siguiente</a><%}%>
        </td></tr>
</table>
<table align="center">
    <tr><td>
        <a href='javascript: js_Cancelar()' border='0'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0 alt='Cancelar' height='22' width='85'></a>
        <a href=' javascript: self.print()' border='0'><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir' height='22' width='83'></a>
        <% if ( (null != chesArchivoExp )&&(!chesArchivoExp.trim().equals("") ) ){%>
            <a href="/Download/<%= chesArchivoExp %>" ><img src='/gifs/EnlaceMig/gbo25230.gif' border=0 alt='Exportar' height='22' width='85'></a>
        <%}%>
        <A href='ChesConsulta?modulo=cheques'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
    </td><tr>
</table>
</td></tr></table><%--// Modificacion RMM 20030207 --%>
</form>
<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>

</body>
</html>
