<%@ page import="mx.altec.enlace.servlets.*" %>
<%-- 
Nombre: ChesRegistro
@author Rafael Martinez Montiel
Permite Seleccionar un archivo para importar cheques de seguridad--%>
<html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script type="text/javascript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
    <script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script type="text/javascript">
function js_enviar(){
    if (document.frmRegistro.Archivo.value != "") {
        document.frmRegistro.opcion.value = '<%= ChesRegistro.RECUPERAR %>';
        document.frmRegistro.submit();
    }

}
    </script>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function msgCifrado(){
	try {
		var mensajeCifrado = document.frmRegistro.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
			var arreglo = mensajeCifrado.split("|");
               var msj = arreglo[1];
               var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

/*************************************************************************/

// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" 	 
    onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">		
    <%=baseResource.getStrMenu()%>
    <%=encabezado%>
<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
<form  name="frmRegistro" ENCTYPE="multipart/form-data"  action="ChesRegistro" method="POST" >
	<input TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>">
    <input type="hidden" name='opcion' id='opcion' value=''>
    <table align="center">
        <tr class="tabfonazu">
            <td class="tabfonazu">
                <table class="tabfonazu"  >
                    <tr>
                        <td class="tittabdat" > Seleccione el archivo a enviar</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="textabdatcla">
            <td>
                <table align="center" class="textabdatcla" >
                    <tr class="textabdatcla">
                        <td class="tabmovtex">Buscar archivo:</td>
                    </tr>
                    <tr class="textabdatcla">
                        <td class="tabmovtex">
                            <input type="file" id="<%= ChesRegistro.FILE_NAME %>" name="<%= ChesRegistro.FILE_NAME %>" class="tabmovtex" size="25" >
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <A href = "javascript:js_enviar();"><img src="/gifs/EnlaceMig/gbo25300.gif" border=0 alt="Enviar" ></a>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
