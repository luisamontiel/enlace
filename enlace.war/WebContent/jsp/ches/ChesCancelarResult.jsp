<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.servlets.*,mx.altec.enlace.ches.*, java.util.*,java.text.*"%><%--
Nombre: ChesConsultaCheqRes
@author Rafael Martinez Montiel
Muestra el resultado de la consulta y permite cancelar cheques
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="resOp" scope="request" class="java.util.LinkedList" />
<jsp:useBean id="msg" scope="request" class="java.lang.String" />
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<%
mx.altec.enlace.bo.BaseResource baseResource = (mx.altec.enlace.bo.BaseResource) request.getSession().getAttribute ("session");
%>
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language='javascript'>
    </SCRIPT>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************/
// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%= baseResource.getStrMenu() %>
    <%= encabezado %>
<table align="center">
<tr>
    <td>
        <table>
            <tr class="tabdatfonazu">
                <TH class="tittabdat">Cuenta</th>
                <TH class="tittabdat">N&uacute;mero de cheque</th>
                <th class="tittabdat">Descripci&oacute;n</th>
            </tr>
            <%
                            ListIterator li = resOp.listIterator();
                            ChesResultadoCancelacion resCa = null;
                            boolean claro = false;
                            while (li.hasNext()){
                                resCa = (ChesResultadoCancelacion) li.next();
            %><tr class="<%= ((claro)?"textabdatcla":"textabdatobs")%>">
                <td align="center" class="tabmovtex"><%= resCa.getNumCuenta() %></td>
                <TD align="center" class="tabmovtex"><%= resCa.getNumCheque() %></td>
                <td align="center" class="tabmovtex"><%= resCa.getDescripcion() %></td>
            </tr>
<%                  claro = ! claro;
                } %>
        </table>
    </TD>
</TR>
<tr>
    <td align="center" class="tittabcom" ><%= msg %></TD>
</TR>
<tr>
    <td align="center"><A href='ChesConsulta?modulo=cheques'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
        <a href=' javascript: self.print()' border='0'><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir' height='22' width='83'></a>

    </TD>
</TR>
</TABLE>
<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>

</body>
</html>
