<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.servlets.*,mx.altec.enlace.ches.*,java.util.*,java.text.*,mx.altec.enlace.servlets.ChesConsulta"%><%--
Nombre: ChesConsultaArchRes.jsp
@author Rafael Martinez Montiel
Muestra el resultado de la consulta de archivos y permite obtener una consulta detallada por correo
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />

<<%--jsp:useBean id="chesArchivos" scope="session" class="java.util.List" /--%>
<<%--jsp:useBean id="start" scope="request"  class="java.lang.Integer" /--%>
<<%--jsp:useBean id="despl" scope="session"  class="java.lang.Integer" /--%>

<jsp:useBean id="chesMSG" scope="session" class="java.lang.String" />
<jsp:useBean id="chesArchivoExp" scope="session" class="java.lang.String" />
<jsp:useBean id="chesTipoArchivo" scope="session" class="java.lang.String" />

<%!java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");%>
<%
	List chesArchivos = (List) session.getAttribute("chesArchivos");
	Integer start = (Integer) request.getAttribute("start");
	Integer despl = (Integer) session.getAttribute("despl");
%>

<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language='javascript'>

function js_desplazamiento(index){

    document.frmConsulta.action = "ChesConsulta";
    document.frmConsulta.modulo.value = "<%= ChesConsulta.CONSULTA_ARCHIVOS %>";
    document.frmConsulta.opcion.value = 2;
    document.frmConsulta.start.value = index;
    document.frmConsulta.submit();
}

function countSeleccionados(){
	var c = 0;

	for(var i = 0;i < document.frmConsulta.elements.length; i++){

		var e = document.frmConsulta.elements[i];
		if(e.name == 'reg' ){
			if(e.checked == true){
				c++;
			}
		}
	}
	return c;
}

function getSelecccionado(){
    for(var i = 0;i < document.frmConsulta.elements.length; i++){

		var e = document.frmConsulta.elements[i];
		if(e.name == 'reg' ){
			if(e.checked == true){
				return e;
			}
		}
	}

}

var respuesta = -1;

function js_ConsultaDetalle(){
	document.frmConsulta.action = "ChesConsulta";
   document.frmConsulta.<%= ChesConsulta.PAR_MODULO %>.value = "<%= ChesConsulta.CONSULTA_DETALLE_ARCHIVO%>";

	var c = countSeleccionados();

	if(0 == c){
		cuadroDialogo( "Seleccione al menos un registro..." ,3);
	} else {
        if(validaEmail()){
            cuadroDialogo("Est&aacute; seguro que desea un informe detallado del archivo?",2);
        }
	}
}

function continua(){
	if (respuesta == 1){
		document.frmConsulta.action = "ChesConsulta";
      document.frmConsulta.<%= ChesConsulta.PAR_MODULO %>.value = "<%= ChesConsulta.CONSULTA_DETALLE_ARCHIVO%>";
		document.frmConsulta.submit();
	}
}

    </SCRIPT>
<script language = "Javascript">


function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){

		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){

		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){

		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){

		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){

		    return false
		 }

		 if (str.indexOf(" ")!=-1){

		    return false
		 }

 		 return true
	}

function validaEmail(){
	var emailID=document.frmConsulta.<%=ChesConsulta.EMAIL%>

	if ((emailID.value==null)||(emailID.value=="")){
        emailID.value="";
        emailID.focus();
        cuadroDialogo( "Introduzca un email valido..." ,3);
		return false
	}
	else if (echeck(emailID.value)==false){
		emailID.value="";
		emailID.focus();
      cuadroDialogo( "Introduzca un email valido..." ,3);
		return false
	}
	return true
 }
</script>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************/
// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%=baseResource.getStrMenu()%>
    <%=encabezado%>

<form  name="frmConsulta" action="ChesConsulta" method="POST">
<input type="hidden" name='<%= ChesConsulta.PAR_MODULO %>' id='<%= ChesConsulta.PAR_MODULO %>'
value='<%= ChesConsulta.CONSULTA_DETALLE_ARCHIVO %>' >
<input type="hidden" name="<%= ChesConsulta.PAR_OPCION %>" id="<%= ChesConsulta.PAR_OPCION %>"
value='' >
<input type="hidden" name="start" id="start" value="<%= start %>">



    <table align="center">
        <tr class="tabdatfonazu">
            <th class="tittabdat"> Seleccionar</th>
            <th class="tittabdat"> Contrato</th>
            <th class="tittabdat">Fecha de Recepci&oacute;n</th>
            <th class="tittabdat">Nombre de Archivo</th>
            <th class="tittabdat">N&uacute;mero de secuencia</th>
            <th class="tittabdat">N&uacute;mero de registros</th>
            <th class="tittabdat">Importe total</th>
            <th class="tittabdat">Estatus</th>

        </TR>
<%
    boolean hasConsultable = false;
    ListIterator li = chesArchivos.listIterator();
    boolean claro = true;
    while (li.hasNext()){
        ChesArchivoImportado archivo = (ChesArchivoImportado) li.next();
%>
        <tr class="<%=( (claro)?"textabdatcla":"textabdatobs" )%>">
            <td class="tabmovtex" align="center" ><%
            if(archivo.getStatus().equals("P")){
            %><input type="radio" id="reg" name="reg" value="<%= archivo.getNumSecuencia () %>"><%
                        hasConsultable = true;
                        }else{
            %>&nbsp;<%
            }
            %></td>
            <td class="tabmovtex" align="center" ><%=baseResource.getContractNumber()%></td>
            <td class="tabmovtex" align="center" >&nbsp;<%=sdf.format (archivo.getFechaEnvio().getTime() )%></td>
            <td class="tabmovtex" align="center" >&nbsp;<%=archivo.getFileName()%></td>
            <td class="tabmovtex" align="center" > &nbsp;<%=archivo.getNumSecuencia()%></td>
            <td class="tabmovtex" align="center" >&nbsp;<%=archivo.getNumRegistros()%></td>
            <td class="tabmovtex" align="right" >$&nbsp;<%=archivo.importeTotalAsString()%></td>
            <td class="tabmovtex" align="center" >&nbsp;<%=archivo.getStatus()%></td>

<%
    claro = ! claro;
    }
%>
    </TABLE>
<%
if( hasConsultable ){
%>
<table align="center">
    <tr >
        <th class="tittabdat">Correo destino
    </tr>
    <tr>
        <td><input type="text" name="<%= ChesConsulta.EMAIL %>" id="<%= ChesConsulta.EMAIL %>" value=""></td>
    </tr>

</TABLE>
<%
}
%>
<%
if(chesArchivos.size() > despl.intValue()){
%>
<table align="center" cellpadding='5'>
<tr><td align="center" class='texfootpaggri'>
		<%
		if(start.intValue() - despl.intValue() >= 0){
		%><a href='javascript:js_desplazamiento(<%= start.intValue() - despl.intValue() %>)' class='texfootpaggri'>anterior</a>&nbsp;<%
		        }
		        if(chesArchivos.size() > 1){
		    		for(int i = 0;i < chesArchivos.size();i+= despl.intValue()){
		    	if(start.intValue() != i){
		%><a href='javascript:js_desplazamiento(<%= i %>)' class='texfootpaggri'><%=(i /despl.intValue()) +1%></a><%
		}else{
		%><%=( i / despl.intValue() ) + 1%><%
		}
		%>&nbsp;<%
		        }
		        }
				if(start.intValue() + despl.intValue() < chesArchivos.size()){
		%>&nbsp;<a href='javascript:js_desplazamiento(<%= start.intValue() + despl.intValue() %>)' class='texfootpaggri'>siguiente</a><%
		}
		%>
		</td></tr>
</table>
<%
}
%>

<table align="center" cellpadding='5px'>
    <tr><td>
        <%
        if(hasConsultable){
        %><a href='javascript: js_ConsultaDetalle()' border='0'><img src="/gifs/EnlaceMig/gbo25245.gif" border=0 alt="Enviar Detalle" width="113" height='22'></a><%
        }
        %>
        <a href=' javascript: self.print()' border='0'><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir' height='22' width='83'></a>
        <%
        if ( (null != chesArchivoExp )&&(!chesArchivoExp.trim().equals("")) ){
        %>
            <a href="/Download/<%= chesArchivoExp %>" ><img src='/gifs/EnlaceMig/gbo25230.gif' border=0 alt='Exportar' height='22' width='85'></a>
        <%
        }
        %>
        <A href='ChesConsulta?<%= ChesConsulta.PAR_MODULO %>=<%= ChesConsulta.CONSULTA_ARCHIVOS%>'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
    </td><tr>
</table>
</form>
</body>
</html>
