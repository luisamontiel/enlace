<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.ches.*,mx.altec.enlace.servlets.*, java.util.GregorianCalendar"%><%--
Nombre: ChesConsultaArchFiltro
@author Rafael Martinez Montiel
Permite Seleccionar un filtro para consultar archivos enviados
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />
<%! java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
    java.text.SimpleDateFormat sdfNoSep = new java.text.SimpleDateFormat("ddMMyyyy");
%>
<%
    GregorianCalendar today = new GregorianCalendar();
    GregorianCalendar fechaMin= new GregorianCalendar();
    fechaMin.add( fechaMin.MONTH,-3);
    GregorianCalendar fechaMax = new GregorianCalendar();
    fechaMax.add(fechaMax.DAY_OF_MONTH,1);

%>
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language="javascript">
var chesFecha ="";
var opt;

function calendarioPre( field, fechaInicial, campoFinal){
	var el = document.frmConsulta.elements;
	milisegundos=parseInt(24*60*60*1000); //Suma 1 d�a para mostrar correctamente el rango m�ximo
	c =0;
	while( el[c].name != campoFinal ){++c;}
	fechaFinal = el[c].value;
	fechaTmp = new Date();
	fechaTmp.setFullYear(fechaFinal.slice(6, 10), fechaFinal.slice(3,5), fechaFinal.slice(0,2));
	tiempo=fechaTmp.getTime();
	total=fechaTmp.setTime(parseInt(tiempo+milisegundos));
	anio = "" + fechaTmp.getFullYear();
	mes = "" + fechaTmp.getMonth();
	dia = "" + fechaTmp.getDate() ;
	if (mes.length == 1) {mes = "0" + mes;}
	if (dia.length == 1) {dia = "0" + dia;}
	fechaFinal = dia + "/" + mes + "/" + anio;
    calendario( field, fechaInicial, fechaFinal,false);
}

function calendarioPost( field, campoInicial, fechaFinal){
	var el = document.frmConsulta.elements;
	c =0;
	while( el[c].name != campoInicial ){++c;}
	fechaInicial = el[c].value;
	calendario( field, fechaInicial, fechaFinal,false );
}

function calendario( field, fechaInicial, fechaFinal, inclusiveEnd){
    opt = field;
    fechaMin = fechaInicial.slice(0,2) + fechaInicial.slice( 3,5) + fechaInicial.slice(6,10);
    fechaMax = fechaFinal.slice(0,2) + fechaFinal.slice( 3,5) + fechaFinal.slice(6,10);
    msg = window.open("ChesConsulta?modulo=calendario"
    + "&" + "fechaMin=" + fechaMin
    + "&" + "fechaMax=" + fechaMax
    + "&" + "inclusiveEnd=" + inclusiveEnd, "Calendario",
    "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=390");
    msg.focus();
}


function continuaFecha(){
    var el = document.frmConsulta.elements;
    c = 0;
    while ( el[c].name != opt ){
        ++c;
    }
    el[c].value = chesFecha;
}

function js_validaForma(){
    if ("" == document.frmConsulta.secuencia.value){
        return true;
    }
    var i = parseInt( document.frmConsulta.secuencia.value );
    if( isNaN ( i )){
        cuadroDialogo("Para filtrar por n&uacute;mero de secuencia, introduzca un valor v&aacute;lido", 3);
        return false;
    }

    return true;
}
    </script>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************/

// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%= baseResource.getStrMenu() %>
    <%= encabezado %>
<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
<form  name="frmConsulta" action="ChesConsulta" method="POST" >
<input type="hidden" name="opcion" id="opcion" value="1">
<input type="hidden" name="modulo" id="modulo" value="archivos">
<table border="0" align="center">
    <tr class="tabfonazu"><th class="tittabdat" align="left">Consulta de Archivos
    </tr>
    <tr class="textabdatcla">
        <td align="center">
            <table border="0" cellpadding="20">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="tabmovtex">Fecha Inicial</td>
                                <td class="tabmovtex"><input type="text" name="fechaInicial" id="fechaInicial" onFocus="blur()" size="10" value="<%= sdf.format( fechaMin.getTime() ) %>">
                                    <a href="javascript:calendarioPre('fechaInicial','<%= sdf.format( fechaMin.getTime() ) %>','fechaFinal');"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario"></a></td>
                            </tr>
                            <tr>
                                <td class="tabmovtex">Fecha Final</td>
                                <td class="tabmovtex"><input type="text" name="fechaFinal" id="fechaFinal" onFocus="blur()" size="10" value="<%= sdf.format( today.getTime() ) %>">
                                    <a href="javascript:calendarioPost('fechaFinal','fechaInicial','<%= sdf.format( fechaMax.getTime() )%>');"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario"></a></td>
                            </tr>
                            <tr>
                                <td class="tabmovtex">Secuencia</td>
                                <td class="tabmovtex"><input type="text" name="secuencia" id="secuencia" ></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td class="tabmovtex">Recibidos</td>
                                <td class="tabmovtex"><input type="checkbox" name="status" id="status" checked="true" value="R"></td>
                            </tr>
                            <tr>
                                <td class="tabmovtex">Enviados</td>
                                <td class="tabmovtex"><input type="checkbox" name="status" id="status" value="E"></td>
                            </tr>
                            <tr>
                                <td class="tabmovtex">Procesados</td>
                                <td class="tabmovtex"><input type="checkbox" name="status" id="status" value="P"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
       <td>
            <table align="center">
                <tr>
                    <td>
                        <a href = "javascript:if(js_validaForma())document.frmConsulta.submit();" border=0><img src="/gifs/EnlaceMig/gbo25220.gif" border=0 alt="Consulta" width="90" height='22'></a>
                     </td>
                    <td >
                        <A href = "javascript:document.frmConsulta.reset();" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="76" height="22"></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>

</body>
</html>
