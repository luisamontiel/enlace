<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.servlets.*,mx.altec.enlace.ches.*,java.util.*,java.text.*"%><%--
Nombre: ChesConsultaCheqFiltro
@author Rafael Martinez Montiel
Permite seleccionar un filtro para consulta de cheques
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />
<jsp:useBean id="fechaMax" scope="request" class="java.util.GregorianCalendar" />
<jsp:useBean id="fechaMin" scope="request" class="java.util.GregorianCalendar" />
<jsp:useBean id="fechaMaxDisp" scope="request" class="java.util.GregorianCalendar" />
<%-- <jsp:useBean id="chesBenef" scope="session" class="java.util.Map" /> --%>
<%!
  SimpleDateFormat sdf = new SimpleDateFormat ( "dd/MM/yyyy" );
%>
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language="javascript">
<%
GregorianCalendar today = new GregorianCalendar();
GregorianCalendar tomorrow = new GregorianCalendar();
tomorrow.add( tomorrow.DAY_OF_MONTH,1);

%>
function PresentarCuentas(){
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

var ctaselec = "";

function actualizacuenta(){
  document.frmConsulta.ctaCargo.value=ctaselec;
}

var chesFecha ="";
var opt;

function calendarioPre( field, fechaInicial, campoFinal){
	var el = document.frmConsulta.elements;
	c =0;
	while( el[c].name != campoFinal ){++c;}
	fechaFinal = el[c].value;
    calendario( field, fechaInicial, fechaFinal,true);
}

function calendarioPost( field, campoInicial, fechaFinal){
	var el = document.frmConsulta.elements;
	c =0;
	while( el[c].name != campoInicial ){++c;}
	fechaInicial = el[c].value;
	calendario( field, fechaInicial, fechaFinal,false );
}

function calendario( field, fechaInicial, fechaFinal, inclusiveEnd){
    opt = field;
    fechaMin = fechaInicial.slice(0,2) + fechaInicial.slice( 3,5) + fechaInicial.slice(6,10);
    fechaMax = fechaFinal.slice(0,2) + fechaFinal.slice( 3,5) + fechaFinal.slice(6,10);
    msg = window.open("ChesConsulta?modulo=calendario"
    + "&" + "fechaMin=" + fechaMin
    + "&" + "fechaMax=" + fechaMax
    + "&" + "inclusiveEnd=" + inclusiveEnd, "Calendario",
    "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=390");
    msg.focus();
}


function continuaFecha(){
    var el = document.frmConsulta.elements;
    c = 0;
    while ( el[c].name != opt ){
        ++c;
    }
    el[c].value = chesFecha;
}

function js_validaForma(){
    frm = document.frmConsulta;


    if ("" != frm.importe.value && isNaN (frm.importe.value) ){
        cuadroDialogo("El IMPORTE no es un valor v&aacute;lido. <br>Introduzca un valor correcto o deje el campo en blanco.", 3);
        return false;
    }else {
        var i = parseFloat( frm.importe.value );
        if( 0 >= i ){
            cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
            return false;
        }
    }
    if("" == frm.ctaCargo.value ){
        cuadroDialogo("Seleccione una cuenta",3);
        return false;
    }
    if( validaEmail() == false){
            return false;
    }
    inicio = document.frmConsulta.cheqDesde.value;
    fin = document.frmConsulta.cheqHasta.value;
    if ( "" != inicio || "" != fin ){
        inicio = parseInt ( inicio );
        fin = parseInt ( fin );
        if( isNaN( inicio) ) {
            cuadroDialogo("Para filtrar por rango de n&uacute;mero de cheque, introduzca un valor v&aacute;lido de inicio", 3);
            return false;
        }
		if( isNaN( fin ) ) {
            cuadroDialogo("Para filtrar por rango de n&uacute;mero de cheque, introduzca un valor v&aacute;lido de fin", 3);
            return false;
        }
        if (( 1 > inicio) ||( 1 > fin)){
            cuadroDialogo("Para filtrar por rango de n&uacute;mero de cheque, introduzca un valor v&aacute;lido de inicio como de fin", 3);
            return false;
        }
        if(inicio > fin ){
            cuadroDialogo("El n&uacute;mero de cheque inicial no puede ser mayor al n&uacute;mero de cheque final", 3);
            return false;
        }
    }
    return true;
}
    </script>
    <script language = "Javascript">


function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   //alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){

		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){

		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){

		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){

		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){

		    return false
		 }

		 if (str.indexOf(" ")!=-1){

		    return false
		 }

 		 return true
	}

function validaEmail(){
	var emailID=document.frmConsulta.<%=ChesConsulta.MOD_CHEQUE_EMAIL%>

	if ((emailID.value==null)||(emailID.value=="")){
		emailID.value="";
		emailID.focus();
      cuadroDialogo( "Introduzca un email valido..." ,3);
		return false;
	}
	else if ( echeck(emailID.value) == false ){
		emailID.value=""
		emailID.focus()
      cuadroDialogo( "Introduzca un email valido..." ,3);
		return false
	}
	return true
 }
</script>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************/
// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%=baseResource.getStrMenu()%>
    <%=encabezado%>

<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
<form  name="frmConsulta" action="ChesConsulta" method="POST">
<input type="hidden" name="<%= ChesConsulta.PAR_MODULO %>" value="<%= ChesConsulta.CONSULTA_CHEQUES %>">
<input type="hidden" name="<%= ChesConsulta.PAR_OPCION %>" value="1">
<table align="center" border ="0">
    <tr align="left" class="tabfonazu">
        <th class="tittabdat">Seleccion de Filtro:
    </TR>
    <TR>
        <TD class="textabdatcla">
            <TABLE border="0" cellpadding="5" cellspacing="5" >
                <TR>
                    <TD>
                        <TABLE border="0" >
                            <tr>
                                <td class="tabmovtex">Cuenta de cargo</td>
                                <td colspan="2" class="tabmovtex">
                                    <%--<input type="text" name="ctaCargo" id="ctaCargo" onFocus="blur()">--%>
                                    <input type="text" name="ctaCargo"  class="tabmovtexbol" maxlength=16 size=16 onfocus="blur();" value="">
                                    <A HREF="javascript:PresentarCuentas();">
                                        <IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle>
                                    </A>
                                </td>
                            </tr>
                            <tr><td class="tabmovtex">Fecha Libramiento</td><td colspan="2" align="left"><input type="checkbox" name="useLib" id="useLib" value="<%=Boolean.TRUE%>"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" class="tabmovtex">De la fecha</td>
                                <td class="tabmovtex"><input type="text" name="libDesde" id="libDesde" onFocus="blur()" value="<%= sdf.format( fechaMin.getTime() )%>" size="10">
                                    <a href="javascript:calendarioPre('libDesde','<%= sdf.format( fechaMin.getTime() ) %>','libHasta');">
                                        <IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" class="tabmovtex">A la fecha</td>
                                <td class="tabmovtex"><input type="text" name="libHasta" id="libHasta" onFocus="blur()" value="<%= sdf.format( today.getTime() ) %>" size="10">
                                    <a href="javascript:calendarioPost('libHasta','libDesde','<%= sdf.format( tomorrow.getTime() )%>');">
                                        <IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="tabmovtex">Vigencia</td><td colspan="2" align="left"><input type="checkbox" name="useVig" id="useVig" value="<%=Boolean.TRUE%>"></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="right" class="tabmovtex">De la fecha</td>
                                <td class="tabmovtex"><input type="text" name="vigDesde" id="vigDesde" onFocus="blur()" value="<%= sdf.format( fechaMin.getTime() ) %>"  size="10">
                                    <a href="javascript:calendarioPre('vigDesde','<%= sdf.format( fechaMin.getTime() )%>','vigHasta');">
                                        <IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" class="tabmovtex">A la fecha</td>
                                <td class="tabmovtex"><input type="text" name="vigHasta" id="vigHasta" onFocus="blur()" value="<%= sdf.format( fechaMaxDisp.getTime() ) %>"  size="10">
                                    <a href="javascript:calendarioPost('vigHasta','vigDesde','<%= sdf.format( fechaMax.getTime() )%>');">
                                        <IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="tabmovtex">Liquidados</td><td colspan="2" align="left"><input type="checkbox" name="useLiq" id="useLiq" value="<%=Boolean.TRUE%>"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" class="tabmovtex">De la fecha</td>
                                <td class="tabmovtex"><input type="text" name="liqDesde" id="liqDesde" onFocus="blur()" value="<%= sdf.format( fechaMin.getTime() ) %>" size="10">
                                    <a href="javascript:calendarioPre('liqDesde','<%= sdf.format( fechaMin.getTime())%>','liqHasta');">
                                        <IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2" class="tabmovtex">A la fecha</td>
                                <td class="tabmovtex"><input type="text" name="liqHasta" id="liqHasta" onFocus="blur()" value="<%= sdf.format( today.getTime() ) %>" size="10">
                                    <a href="javascript:calendarioPost('liqHasta','liqDesde','<%= sdf.format( tomorrow.getTime() )%>');">
                                        <IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario">
                                    </a>
                                </td>

                            </tr>
                        </TABLE>
                    </TD>
                    <TD>
                        <TABLE border="0">
                            <tr><td colspan="3" class="tabmovtex">Cheques</Td></tr>
                            <tr>
                                <td colspan="2" align="right" class="tabmovtex">Del cheque</Td>
                                <td class="tabmovtex"><input type="text" name="cheqDesde" id="cheqDesde" ></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" class="tabmovtex">Al cheque</Td>
                                <td class="tabmovtex"><input type="text" name="cheqHasta" id="cheqHasta" ></td>
                            </tr>
                            <tr>
                                <td  class="tabmovtex">Importe</Td>
                                <td colspan="2" class="tabmovtex"><input type="text" name="importe" id="importe" ></td>
                            </tr>
                        </table>
                        <table>
                            <tr><td colspan="3" class="tabmovtex">Estatus</Td></tr>
                            <tr >
                                <td colspan="2" align="right" class="tabmovtex">En transito</td>
                                <td class="tabmovtex"><input type="checkbox" name="status" id="status" value="PE" checked="true"></td>
                            </tr>
                            <tr >
                                <td colspan="2" align="right" class="tabmovtex">Liquidados</td>
                                <td class="tabmovtex"><input type="checkbox" name="status" id="status" value="LI"></td>
                            </tr>
                            <tr >
                                <td colspan="2" align="right" class="tabmovtex">Cancelados</td>
                                <td class="tabmovtex"><input type="checkbox" name="status" id="status" value="CA"></td>
                            </tr>
                            <tr >
                                <td colspan="2" align="right" class="tabmovtex">Vencidos</td>
                                <td class="tabmovtex"><input type="checkbox" name="status" id="status" value="VE" ></td>
                            </tr>

                        </TABLE>
                    </TD>
                </TR>
                <tr>
                    <td align="center" colspan="2">
                        <table align= "center">
                            <tr>
                                <td class="tabmovtex">E-mail para env&iacute;o de consulta.</td>
                                <td class="tabmovtex"><input type="text" name="<%=ChesConsulta.MOD_CHEQUE_EMAIL%>" id="<%=ChesConsulta.MOD_CHEQUE_EMAIL%>" value=""></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </TABLE>
        </TD>
    </TR>
    <TR>
        <TD align="center">
            <a href = "javascript:if(js_validaForma())document.frmConsulta.submit();" border=0><img src="/gifs/EnlaceMig/gbo25220.gif" border=0 alt="Consulta" width="90" height='22'></a>
            <A href = "javascript:document.frmConsulta.reset();" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="76" height="22"></a>
        </TD>
    </TR>
</TABLE>
</form>
</body>
</html>
