<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>

<head>

<title>Alta de empleados</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

<script type="text/javascript" src= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript">
function selectTodos(ch){
	var isSelected = ch.checked;
	var listaEmpleados = document.forms["manttoempleados"]["empleadoSeleccion"];
	var noRegistros = listaEmpleados.length;
	var seleccionados = 0;
	if (noRegistros === undefined) {
		if (isSelected) {
			seleccionados = 1;
		}
		listaEmpleados.checked=isSelected;
	}else{
		if (isSelected) {
			seleccionados = listaEmpleados.length;
		}
		for ( var i = 0; i < listaEmpleados.length; i++) {
			listaEmpleados[i].checked = isSelected;
		}
	}
	document.getElementById("totalRegistros").value=seleccionados;
}

function verificaCheck(ch){
	var listaEmpleados = document.forms["manttoempleados"]["empleadoSeleccion"];
	var todos = listaEmpleados.length;
	var seleccionados = 0;
	var isSelected=false;
	if (todos === undefined) {
		if (ch.checked) {
			seleccionados=1;
		} else {
			seleccionados=0;
		}
		isSelected = ch.checked;
	}else{
		for ( var i = 0; i < listaEmpleados.length; i++) {
			if (listaEmpleados[i].checked) {
				seleccionados++;
			}
		}
		isSelected = (listaEmpleados.length===seleccionados);
	}
	document.getElementById("totalRegistros").value=seleccionados;
	document.getElementById("checkTodos").checked=isSelected;
}

function ingresar(){
	var form = document.createElement("form");
    form.method = "POST";
    form.action = "AltaNomEmplInd";
    document.body.appendChild(form);
    form.submit();
}

function enviaAltaIndividual(){
	var listaEmpleados = document.forms["manttoempleados"]["empleadoSeleccion"];
	var seleccionados = document.getElementById("totalRegistros").value;
	if (seleccionados === ""||seleccionados==="0") {
		cuadroDialogo("Debe seleccionar al menos un empleado de la lista antes de enviarlo.");
		return;
	}
	for ( var i = 0; i < listaEmpleados.length; i++) {
		if (listaEmpleados[i].checked) {
			seleccionados++;
		}
	}
	if(seleccionados>100){
		cuadroDialogo("No es posible enviar m�s de 100 registros.");
		return;
	}
    var form = document.createElement("form");
    form.method = "POST";
    form.action = "AltaNomEmplInd?opcion=4";
    completaFormulario(form);
    document.getElementById("divNoVisible").appendChild(form);
    form.submit();
}

<!--

var facultades="<%= request.getAttribute( "facultades" ) %>";

/**
*			Modificado por Miguel Cortes Arellano
*			Fecha: 02/01/2003
*			Empieza codigo SINAPSIS
*			Proposito: Verificar que el campo Sucursal en caso de no estar vacio, sea solo digitos. Este campo no es mandatorio.
**/
var infoCounter = 1;
var infoSucursal = 1;

/*Agreagado por Isaac Galvan La�ado */
var enviando = 0;
var importando = 0;
/*  */
var speed = 1000;
var control = 1;
var frase = 'Espere, procesando....';

var respuesta = -1;
var esCancelacionAlta= false;

function confirmaCancelacion(){
	esCancelacionAlta = true;
	cuadroDialogo('Se perderan todos los empleados capturados, �Desea cancelar?',2);
}

function completaFormulario(form){
	var seleccion = document.getElementsByName("empleadoSeleccion");
	var checks = [];
	for ( var i = 0; i < seleccion.length; i++) {
		checks[i] = seleccion[i];
	}
	for ( var i = 0; i < checks.length; i++) {
		form.appendChild(checks[i]);
	}
}

function cancelaAltaIndividual(){
    var form = document.createElement("form");
    form.method = "POST";
    form.action = "AltaNomEmplInd?opcion=5";
    document.body.appendChild(form);
    form.submit();
}

function actualizaEmpleado(idEmpleado){
    var form = document.createElement("form");
    form.method = "POST";
    form.action = "AltaNomEmplInd?opcion=1&idEmpleado="+idEmpleado;
    document.body.appendChild(form);
    form.submit();
}

function istatus()
{
  if (control == 1)
    {
      window.status=frase;
      control=0;
    }
  else
    {
      window.status="";
      control=1;
    }
  setTimeout("istatus()",speed);
}

function checkSucursal()
{
	tmp = document.manttoempleados.Sucursal.value;
	PosNum = "0123456789";
	c="";
	allValid = true;
	wrongLength = "";
  if(tmp != ""){

	/**
	  * Agrega una validacion para que la sucursal no vaya en 0 en caso de ser asi a la sucursal
	  * no se le agrega nada, viajara como vacia
	  * por Eric Gomez (Sinapsis) 24 / FEB / 2004
 	  */
	if (trimString(tmp) == "0000"){
		document.manttoempleados.Sucursal.value = "";
	}

	if(tmp.length > 4 || tmp.length != 4)
	{
		wrongLength = ". Ademas, el campo debe de ser de 4 caracteres.";
		allValid = false;
	}
	for( i=0;i<tmp.length;i++)
	  {
			c = tmp.charAt(i);
			for(j=0;j<PosNum.length;j++)
		   {
				if(PosNum.charAt(j) == c)
					break;
		   }
			if (j == PosNum.length)
	       {
              allValid = false;
              break;
	       }
	  }
  }
   if(allValid == false)
	{
			cuadroDialogo("El n&uacute;mero de Sucursal debe ser num&eacute;rico "+wrongLength,3);
			document.manttoempleados.Sucursal.value = "";
			infoSucursal = 2;
			infoCounter = 1;
	}else{
		infoSucursal = 1;
	}
}
//FINALIZA CODIGO SINAPSIS
function archivoErrores()

 {

   ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');

   ventanaInfo1.document.open();

   ventanaInfo1.document.write("<html>");

   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");

   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");

   ventanaInfo1.document.writeln("</head>");

   ventanaInfo1.document.writeln("<body bgcolor='white'>");

   ventanaInfo1.document.writeln("<form>");

   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");

   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>Informaci&oacute;n del Archivo Importado</th></tr>");

   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");

   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center width=0>");

   ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");

   ventanaInfo1.document.writeln("<td class='tabmovtex1' align='center'><br>No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los siguientes errores.<br><br>");

   ventanaInfo1.document.writeln("</td></tr></table>");

   ventanaInfo1.document.writeln("</td></tr>");

   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1'>");

   ventanaInfo1.document.writeln(document.manttoempleados.archivoEstatus.value+"<br><br>");

   ventanaInfo1.document.writeln("</td></tr>");

   ventanaInfo1.document.writeln("</table>");

   ventanaInfo1.document.writeln("<table border=0 align=center >");

   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0 alt='Cerrar'></a>");

   ventanaInfo1.document.writeln("<a href = 'javascript:window.print();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a></td></tr>");

   ventanaInfo1.document.writeln("</table>");

   ventanaInfo1.document.writeln("</form>");

   ventanaInfo1.document.writeln("</body>\n</html>");

   ventanaInfo1.document.close();

   ventanaInfo1.focus();

}



function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}



function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v3.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}


/*Modificado por Isaac Galvan La�ado para deshabilitar el boton de importar cuando
se oprime una vez
*/

function js_importar(){
	if (facultades.indexOf("INOMIMPAEMP") ==-1)
		{
			cuadroDialogo ("No tiene facultades para importar archivos de Empleados",1);
			return ;
		}

	if (document.manttoempleados.archivoempleados.value=="" || document.manttoempleados.archivoempleados.value==null) cuadroDialogo("Seleccione primero el archivo a importar con el boton Browse ...",1);
	else {
                if(importando == 0){
                    importando = 1;
                    istatus();
                    document.manttoempleados.operacion.value="importar";
                    document.manttoempleados.action="NomEmpImporta";
                    document.manttoempleados.submit();

                }
                else{
                    cuadroDialogo ("El archivo se est&aacute; importando",1);
                }
	}
}



function js_baja(){
	if (facultades.indexOf("INOMBAJAEMP") ==-1)
		{
			cuadroDialogo ("No tiene facultades para dar de baja Empleados",1)
			return ;
		}

		if (document.manttoempleados.tipo_archivo.value=="enviado"){
			cuadroDialogo("El archivo ya ha sido enviado",1);
			return;
			}

	var tipo = new String( document.manttoempleados.tipo_archivo.value );
	if ( tipo.substring( tipo.indexOf("importado"), tipo.indexOf("importado") + 9 ) =="importado"){
		cuadroDialogo("No puede ejecutar esta operaci&oacute;n en un archivo importado",1);
		return;
	}

	if (trimString (document.manttoempleados.archivo_actual.value)==""){
		cuadroDialogo("Antes de seleccionar esta operaci&#243;n, debe crear o importar un archivo", 1);
	 } else {
		var seleccion=false;

		   //for (var i in document.manttoempleados) {
		   for (i=0;i<document.manttoempleados.elements.length;i++) {
		      if (document.manttoempleados[i].type=="radio")
			  	if (document.manttoempleados[i].checked) {
				document.manttoempleados.registro.value=document.manttoempleados[i].value;
				seleccion=true;
				break;
				}
		   }

			if (seleccion){
				document.manttoempleados.operacion.value="baja";
				document.manttoempleados.action="NomEmpOpciones";
				document.manttoempleados.submit();
			}

			else {cuadroDialogo('Seleccione un registro por favor'),1}
	  }
}


function js_alta(){
	cuadroDialogo("Funci&oacute;n temporalmente fuera de servicio.",1)
}


<%/*function js_alta(){
	if (facultades.indexOf("INOMALTAEMP") ==-1)
		{
			cuadroDialogo ("No tiene facultades para dar de alta Empleados",1)
			return ;
		}

	if (document.manttoempleados.tipo_archivo.value=="enviado"){
		cuadroDialogo("El archivo ya ha sido enviado",1)
		return;
	}

	if (trimString (document.manttoempleados.archivo_actual.value)=="")
		{
			cuadroDialogo("Antes de seleccionar esta operaci&#243;n, debe crear o importar un archivo", 1);
			return;
		}
	var tipo = new String( document.manttoempleados.tipo_archivo.value );
	if ( tipo.substring( tipo.indexOf("importado"), tipo.indexOf("importado") + 9 ) =="importado"){
		cuadroDialogo("No puede ejecutar esta operaci&oacute;n en un archivo importado",1)
		return;
	}



	var js_total=parseInt(document.manttoempleados.total_registros.value);
	var js_max=parseInt(document.manttoempleados.max_registros.value);

	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No es posible realizar el alta",3)
			return ;
		}


	if (js_total >= js_max){
		cuadroDialogo ("Solo puede dar de alta "+ document.manttoempleados.max_registros.value +" registros",1)
		return ;
	}

		document.manttoempleados.operacion.value="alta";
		document.manttoempleados.action="NomEmpOpciones";
		document.manttoempleados.submit();
}COMENTARIADO MSD 20060922*/%>



function ValidaNumero(campo,nombreCampo,punto){

	tmp = campo.value;

	var numeros="0123456789";

	if (punto) numeros+=".";



    for (i = 0; i<tmp.length; i++) {

		car=tmp.charAt(i);

		if (numeros.indexOf(car)==-1) {

			campo.value="";

			campo.focus();

			//cuadroDialogo("El campo "+campo.name+" contiene caracteres no validos", 3);

			cuadroDialogo("El campo "+nombreCampo+" contiene caracteres no validos", 3);

			return false;

		}

	}

	return true;

}




/*Modificado por Isaac Galvan La�ado para deshabilitar el boton de enviar cuando
se oprime una vez
*/

function js_envio(){

if(infoSucursal == 1)
{

		if (facultades.indexOf("INOMENVIEMP") ==-1)

			{

				cuadroDialogo ("No tiene facultades para enviar archivos de Empleados",1)

				return ;

			}



		if (document.manttoempleados.tipo_archivo.value=="enviado"){

			cuadroDialogo("El archivo ya ha sido enviado",1)

			return;

		}



		if (trimString (document.manttoempleados.archivo_actual.value)==""){

			cuadroDialogo("Antes de seleccionar esta operaci&#243;n, debe crear o importar un archivo", 1);

			}
		else {
                     if(enviando==0){
                        enviando = 1;
                        istatus();
			document.manttoempleados.operacion.value="envio";
			document.manttoempleados.action="NomEmpOpciones";
			document.manttoempleados.submit();

                     }
                     else{
                        cuadroDialogo("El archivo se est&aacute; enviando", 1);
                     }
		}

  }
}





function js_modificacion(){
	if (facultades.indexOf("INOMMODIEMP") ==-1)
		{
			cuadroDialogo ("No tiene facultades para modificar Empleados",1)
			return ;
		}

	if (document.manttoempleados.tipo_archivo.value=="enviado"){
		cuadroDialogo("El archivo ya ha sido enviado",1)
		return;
	}

	var tipo = new String( document.manttoempleados.tipo_archivo.value );
	if ( tipo.substring( tipo.indexOf("importado"), tipo.indexOf("importado") + 9 ) =="importado"){
		cuadroDialogo("No puede ejecutar esta operaci&oacute;n en un archivo importado",1)
		return;
	}

	if (trimString (document.manttoempleados.archivo_actual.value)==""){
	 cuadroDialogo("Antes de seleccionar esta operaci&#243;n, debe crear o importar un archivo", 1);
	  } else {
		var seleccion=false;

		   //for (var i in document.manttoempleados) {
		   for (i=0;i<document.manttoempleados.elements.length;i++) {
		      if (document.manttoempleados[i].type=="radio")
			  	if (document.manttoempleados[i].checked) {
						document.manttoempleados.registro.value=document.manttoempleados[i].value;
						seleccion=true;
						break;
				}
		   }

			if (seleccion){
				document.manttoempleados.operacion.value="modificacion";
				document.manttoempleados.action="NomEmpOpciones";
				document.manttoempleados.submit();
			}
			else {cuadroDialogo('Seleccione un registro por favor',1)}
		}
}



function js_carga(){
	document.manttoempleados.operacion.value="carga";	document.manttoempleados.carga_archivo.value=document.manttoempleados.archivos.options[document.manttoempleados.archivos.options.selectedIndex].text;
	document.manttoempleados.action="NomEmpImporta"
	document.manttoempleados.submit();
}

/*

function js_recupera(){

	secuencia=prompt("Proporcione el numero de secuencia: ","");

	if (secuencia!=null){

		secuencia=parseInt(secuencia);

		if (isNaN(secuencia)) cuadroDialogo ("Proporcione un valor numerico para la secuencia :",1)

		else {//(alert ("secuencia="+secuencia));

			document.manttoempleados.operacion.value="recuperar";

			document.manttoempleados.secuencia.value=secuencia;

			document.manttoempleados.action="NomEmpOpciones"

			document.manttoempleados.submit();

			}

		}

}



function js_nuevo(){

	nombre_nuevo=prompt("Proporcione el nombre del archivo a crear: ","");

	if (nombre_nuevo!=null){

		if (nombre_nuevo=="") cuadroDialogo ("Proporcione un nombre para el archivo :",1)

		else {//(alert ("nombre_nuevo="+nombre_nuevo));



			document.manttoempleados.operacion.value="nuevo";

			document.manttoempleados.nombre_nuevo.value=nombre_nuevo;

			document.manttoempleados.action="NomEmpImporta"

			document.manttoempleados.submit();

			}

		}

}

*/



function js_recupera(){
	if (facultades.indexOf("INOMACTUEMP") ==-1)
		{
			cuadroDialogo ("No tiene facultades para recuperar archivos de Empleados",1)
			return ;
		}
/*      	if ( document.manttoempleados.r_numtran.value=="")
	cuadroDialogo("Antes de seleccionar esta operaci&#243;n, debe enviar un archivo", 3);
	else{
		*/
	document.manttoempleados.operacion.value="recuperar";
	document.manttoempleados.action="NomEmpOpciones";
	cuadroCapturaFile("Proporcione el numero de secuencia: ","Captura");
/*	}*/
}


function cuadroCapturaFile(mensaje,titulo)
{
	campoTexto ="";

	captura=window.open('','trainerWindow','width=380,height=170,toolbar=no,scrollbars=no,left=210,top=225');
	captura.document.open();
	captura.focus();
	captura.document.write("<html>");
	captura.document.writeln("<head><title>MSG</title>");
	captura.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	captura.document.writeln("<script language='javaScript'>");
	captura.document.writeln("var estate = false");
	captura.document.writeln("function ValidaForma(forma,boton)");
	captura.document.writeln(" {");
  captura.document.writeln("   if(boton==1)");
	captura.document.writeln("   { ");
 	captura.document.writeln("opener.campoTexto=forma.texto.value;");
 	captura.document.writeln("opener.respuesta=boton;");
 	captura.document.writeln("opener.continuaCaptura();");
 	captura.document.writeln("window.close();");
	captura.document.writeln("   }else{opener.respuesta=boton;window.close();}");
	captura.document.writeln(" }");
	captura.document.writeln("</script>");
	captura.document.writeln("</head>");
	captura.document.writeln("<body bgcolor='white'>");
	captura.document.writeln("<form enctype='multipart/form-data' name='captura' method=post onSubmit='return estate'> ");
	captura.document.writeln("<table style='border: 0px;' width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
	captura.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
	captura.document.writeln("</table>");
	captura.document.writeln("<table style='border: 0px;' width=365 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
	captura.document.writeln("<tr><td class='tabmovtex1' align=right width=50 rowspan=2><img src='/gifs/EnlaceMig/gic25020.gif' style='border: 0px;'/></a></td>");
	captura.document.writeln("<td class='tabmovtex1' align=center width=300><br/>"+mensaje+"<br/>");
	captura.document.writeln("</td></tr>");
	captura.document.writeln("<tr><td class='tabmovtex1' align=center><input type=hidden name=valor_radio><input type='hidden' name='operacion'/>");
	captura.document.writeln("<input type=text name=texto size=30 maxlength=120 class=tabmovtex/><br/><br/></td></tr>")
	captura.document.writeln("</table>");
	captura.document.writeln("<table style='border: 0px;' align=center cellspacing=0 cellpadding=0>");
	captura.document.writeln("<tr><td align=right><a href='javascript:ValidaForma(document.captura,1);'><img src='/gifs/EnlaceMig/gbo25280.gif' style='border: 0px;' onClick='estate=true'/></a></td>");
	captura.document.writeln("<td align=left><a href='javascript:ValidaForma(document.captura,2);'><img src='/gifs/EnlaceMig/gbo25190.gif' style='border: 0px;'/></a></td></tr>");
	captura.document.writeln("</table>");
	captura.document.writeln("</form>");
	captura.document.writeln("</body>\n</html>");
	captura.focus();
	captura.document.captura.texto.focus();
}


<%/*function js_nuevo(){
		if (facultades.indexOf("INOMCREAEMP") ==-1)
			{
				cuadroDialogo ("No tiene facultades para crear archivos de Empleados",1)
			return ;
			}

		cuadroCapturaFile("Proporcione el nombre del archivo a crear: ","Captura");
		document.manttoempleados.action="";
		document.manttoempleados.operacion.value="nuevo";
		document.manttoempleados.action="NomEmpImporta";
}COMENTARIADO MSD 20060922*/%>

function js_nuevo(){
	cuadroDialogo("Esta funci&oacute;n s&oacute;lo est&aacute; disponible mediante importaci&oacute;n de archivo.",1)
}


 function trimString (str)

  {

    str = this != window? this : str;

	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');

  }





function continuaCaptura(){

	if (document.manttoempleados.operacion.value=="nuevo"){

		if (respuesta==1){

			nombre_nuevo=campoTexto;

			//if (campoTexto.length >= 1 ){

			nombre_nuevo = trimString(nombre_nuevo);

			if (nombre_nuevo=="")

				alert("Proporcione un nombre para el archivo.");

			else if (nombre_nuevo.length >= 1 ){

				if (nombre_nuevo.indexOf(" ") >= 0)

					alert("El nombre del archivo no debe contener espacios.");

				else {

					document.manttoempleados.nombre_nuevo.value=nombre_nuevo;
					document.manttoempleados.submit();

				 }

			}

		}

	}




	if (document.manttoempleados.operacion.value=="recuperar"){

		if (respuesta==1){

			if (campoTexto.length==0) cuadroDialogo ("Proporcione un valor numerico para la secuencia",1)

				else {

						secuencia=parseInt(campoTexto);

						document.manttoempleados.secuencia.value=secuencia;

						document.manttoempleados.submit();

					}

			}

	}

}



//function js_exportar(archivo){

function js_exportar()
{
	if (facultades.indexOf("INOMEXPOEMP") ==-1)
	{
		cuadroDialogo ("No tiene facultades para exportar archivos de Empleados",1)
		return ;
	}

/***/
	if ( trimString(document.manttoempleados.archivo_actual.value) == "" && document.manttoempleados.tipo_archivo.value!="recuperado")
	{
		cuadroDialogo ("Antes de seleccionar esta operaci&#243;n, debe crear &#243; importar un archivo",1);
	}
	else
	{
		document.manttoempleados.operacion.value="exportar";
		document.manttoempleados.action="NomEmpOpciones";
		document.manttoempleados.submit();
	}
}



/*function js_borrar(){
	if (facultades.indexOf("INOMBORREMP") ==-1)
	   {
	   cuadroDialogo ("No tiene facultades para borrar archivos de Empleados",1)
	   return ;
	}else{
		if (trimString (document.manttoempleados.archivo_actual.value)=="")
		   {
		    cuadroDialogo ("Antes de seleccionar esta operaci&#243;n, debe crear &#243; importar un archivo",1);
		   //cuadroDialogo ("No hay ningun archivo por eliminar.",1);
		   return;
		}
	}
}
*/

// Modificado por Javier D�az - 03/2004. Se a�ade la funcionalidad del bot�n "Borrar".
function js_borrar(){

	if ( trimString( document.manttoempleados.archivo_actual.value ) == "" &&
	     document.manttoempleados.tipo_archivo.value != "recuperado") {
			cuadroDialogo( "No hay archivo a borrar.", 1 );
			return;
	}

	if ( document.manttoempleados.tipo_archivo.value == "importado"  ||
		 document.manttoempleados.tipo_archivo.value == "enviado"    ||
	     document.manttoempleados.tipo_archivo.value == "nuevo"      ||
		 document.manttoempleados.tipo_archivo.value == "recuperado"    ) {

	    var js_total = parseInt( document.manttoempleados.total_registros.value );
        if ( document.manttoempleados.tipo_archivo.value == "nuevo" && js_total > 0 ) {
			cuadroDialogo( "El archivo creado no debe contener registros.", 1 );
			return;
		}

		cuadroDialogo( "El archivo va a ser borrado, &#191;es correcto?", 2 );
	}
}



function continua(){

if (respuesta==1)
	{
	if (esCancelacionAlta) {
		cancelaAltaIndividual();
	}else{
	    document.manttoempleados.action="ANomMttoEmp?page=1";
	    document.manttoempleados.submit();
	}

/*		document.manttoempleados.operacion.value="borrar";

		document.manttoempleados.action="NomEmpOpciones"

		document.manttoempleados.submit(); */

        // Modificado por Javier D�az - 03/2004. Se elimina la informaci�n presentada en pantalla.


	}
else if(respuesta == 7) //La sucursal fue cancelada No hay movimiento de submit
	{
		document.manttoempleados.Sucursal.value = "";
	}
else if(respuesta == 8) //La sucursal fue aceptada
	{
		return;
	}
/*
 * Se a�aden las respuestas para la aceptaci�n y cancelaci�n de env�o
 * de archivos duplicados de Altas Masivas.
 * Agregado por Javier D�az (Sinapsis) - 01/2004.
 */

/**
 * Agragado por Eric Gomez la variable statusDup para validar la duplicidad
 * de archivos, y para que tenga la funcionalidad de cancelar y aceptar
 * 6 / FEB /2004
 */
else if(respuesta == 11) // Aceptar
    {
    		enviando = 1;
		istatus();
		document.manttoempleados.statusDup.value="0";
		document.manttoempleados.operacion.value="envio";
		document.manttoempleados.action="NomEmpOpciones";
		document.manttoempleados.submit();
	}
else if(respuesta == 12) // Cancelar
    {
    		document.manttoempleados.archivo_actual.value="";
		document.manttoempleados.statusDup.value="1";
		return;
	}
//Agregada la respuesta 13 y 14 por Isaac Galv�n La�ado para evitar el envio de archivos accidentales por el usuario
else if(respuesta == 13)// Aceptar
    {
    		document.manttoempleados.enviando.value="0";
		js_envio();
        }
else if(respuesta == 14) // Cancelar
    {
    		document.manttoempleados.enviando.value="1";
        }
		esCancelacionAlta= false;
}



function js_reporte()
{
	if (facultades.indexOf("INOMIMPREMP") ==-1)
	{
			cuadroDialogo ("No tiene facultades para imprimir reporte de archivos de Empleados",1)
			return ;
	}

/*
	if (document.manttoempleados.tipo_archivo.value=="enviado")
	{
		cuadroDialogo("El archivo ya ha sido enviado, primero seleccione la opci&oacute;n Recuperar",1)
		return;
	}

*/

/***/

/*
	if (trimString (document.manttoempleados.archivo_actual.value)=="")
	{
	    cuadroDialogo ("Antes de seleccionar esta operaci&#243;n, debe recuperar una secuencia enviada",1);
		return;
	}

*/
	var js_total=parseInt(document.manttoempleados.registros_aceptados.value);
	var js_max=parseInt(document.manttoempleados.max_registros.value);

	if (document.manttoempleados.archivo_actual.value!="" && document.manttoempleados.tipo_archivo.value=="recuperado")
	{
		if (js_total >= js_max)
		{
			cuadroDialogo ("Solo puede ver hasta "+ js_max +" registros en el reporte, utilice la opcion Exportar",1)
			return ;
		}

		if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No es posible mostrar el reporte",1)
			return ;
		}

		document.manttoempleados.operacion.value="reporte";
		document.manttoempleados.action="NomEmpOpciones?f=1";
		document.manttoempleados.submit();
	}
	else
	{
		//cuadroDialogo ("No hay datos para crear reporteeeeee.",1);
		 cuadroDialogo ("Antes de seleccionar esta operaci&#243;n, debe recuperar una secuencia enviada previamente",1);
		return;
	}

/***/
}

<%

     if (request.getAttribute("newMenu")!= null) {

     out.println(request.getAttribute("newMenu"));

	 }

%>
function displayWarning()
{
	if(document.manttoempleados.Sucursal.value!="" || infoCounter == 1)
	{
		checkSucursal();
	}else{
		return;
	}
}

//-->

function msgCifrado(){
	try {
		var mensajeCifrado = document.manttoempleados.mensajeCifrado.value;
		if(mensajeCifrado != null && ""!= mensajeCifrado && "null" != mensajeCifrado ) {
			var arreglo = mensajeCifrado.split("|");
            var msj = arreglo[1];
            var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}
</SCRIPT>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</HEAD>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
 style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
 onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');${requestScope.ArchivoErr}" >
<TABLE style="border: 0px;" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	${requestScope.MenuPrincipal}
	</TD>
  </TR>
</TABLE>
${requestScope.Encabezado}
<br/>
<FORM  NAME="manttoempleados" ENCTYPE="multipart/form-data" METHOD="POST" ACTION="">
<INPUT TYPE="hidden" VALUE="${requestScope.facultades}" NAME="facultades"/>
<INPUT TYPE="hidden" name="mensajeCifrado" value="${mensajeCifrado}"/>
<table width="760" style="border: 0px;" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="680" style="border: 0px;" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" valign="bottom" width="190">
              <table width="190" style="border: 0px;" cellspacing="2" cellpadding="3">
				<tr>
					<td class="tittabdat" colspan="4"> Alta de empleados</td>
				</tr>
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="180" style="border: 0px;" cellspacing="4" cellpadding="0">
                      <tr valign="middle"><td class="tabmovtex" nowrap>Alta individual</td></tr>
					  <tr><td nowrap class="tabmovtex">&nbsp;</td></tr>
                      <tr align="center">
                        <td class="tabmovtex" nowrap>
                          <A HREF="javascript:ingresar();"><img style="border: 0px;" name="imageField" src="/gifs/EnlaceMig/ingresar.png" alt="Ingresar"/></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="180" style="border: 0px;" cellspacing="4" cellpadding="0">
                      <tr valign="middle"><td class="tabmovtex" nowrap>Importar archivo</td></tr>
                      <tr><td nowrap class="tabmovtex"><input type="file" name="archivoempleados" value=""/></td></tr>
                      <tr align="center">
                        <td class="tabmovtex" nowrap>
                          <A HREF="javascript:js_importar();"><img style="border: 0px;" name="imageField" src="/gifs/EnlaceMig/gbo25280.gif"alt="Aceptar"/></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
        </table><br/>
        <c:if test="${!empty listaEmpleados}">
		<table style="width:800px; border-spacing: 0px; border: 0px;">
			<tr>
				<td class="tabmovtex">Cat�logo de empleados</td>
			</tr>
  		</table>
        <table class="textabdatcla" style="width:800px; border: 0px;"  cellspacing="2" cellpadding="3">
                <tr>
                  <td class="tittabdat"> <input id="checkTodos" type="checkbox" type="checkbox" checked="checked" onclick="selectTodos(this);" /> </td>
                  <td class="tittabdat">No. Empleado</td>
                  <td class="tittabdat">No. Depto</td>
                  <td class="tittabdat">Paterno</td>
                  <td class="tittabdat">Materno</td>
                  <td class="tittabdat">Nombre</td>
                  <td class="tittabdat">R.F.C.</td>
                  <td class="tittabdat">Homo</td>
                  <td class="tittabdat">Sexo</td>
                  <td class="tittabdat">Cuenta</td>
                </tr>
                <c:forEach var="empleado" items="${listaEmpleados}" varStatus="contador">
                <c:choose>
					<c:when test="${contador.count % 2 eq 0}">
						<c:set var="odd" value="textabdatcla" />
					</c:when>
					<c:otherwise>
						<c:set var="odd" value="textabdatobs" />
					</c:otherwise>
				</c:choose>
                <tr class="${odd}">
                	<td class="tabmovtex"><input value="${contador.index}" name="empleadoSeleccion" type="checkbox" checked="checked" onclick="verificaCheck(this);"/> </td>
                  	<td class="tabmovtex"><a href="javascript:actualizaEmpleado('${contador.index}')">${empleado.numeroEmpleado}</a></td>
                  	<td class="tabmovtex">${empleado.numeroDepto}</td>
                  	<td class="tabmovtex">${empleado.apellidoPaterno}</td>
                  	<td class="tabmovtex">${empleado.apellidoMaterno}</td>
                  	<td class="tabmovtex">${empleado.nombreEmpleado}</td>
                  	<td class="tabmovtex">${empleado.rfc}</td>
                  	<td class="tabmovtex">${empleado.homoclave}</td>
                  	<td class="tabmovtex">${empleado.genero}</td>
                  	<td class="tabmovtex">${empleado.numeroCuenta}</td>
                </tr>
                </c:forEach>
                <tr>
                	<td class="tabmovtex" style="text-align: right; font-weight: bold;" colspan="9">Total de registros a enviar</td>
                  	<td class="tabmovtex"><input id="totalRegistros" type="text" readonly="readonly" value="${fn:length(listaEmpleados)}"/></td>
                </tr>
        </table><br/>
        <div style="text-align: center;">
        <table  style="border:0; margin: auto;" cellspacing="0" cellpadding="0">
          <tr>
            <td width="78">
              <A HREF="javascript:enviaAltaIndividual();"><img style="border: 0px;" src="/gifs/EnlaceMig/gbo25520.gif" width="78" height="22" alt="Enviar"/></a>
            </td>
            <td width="77">
              <A HREF="javascript:confirmaCancelacion();"><img style="border: 0px;" name="imageField32" src="/gifs/EnlaceMig/gbo25190.gif" width="85" height="22" alt="Cancelar"/></a>
            </td>
          </tr>
        </table>
        </div>
		<input id="opcion" name="opcion" type="hidden" value="" />
        <br/>
        </c:if>
        <table width="800" class="textabdatcla" style="border-spacing: 0px; border: 0px;">
                <tr>
                  <td class="tittabdat" colspan="8"> Datos del archivo</td>
                </tr>
                <tr>
                	<td>
                		<div style="height:26px;">
	                		<label class="tabmovtex">Archivo:</label><br/>
                		</div>
                		<input type="text" name="archivo_actual" value="${requestScope.archivo_actual}" onfocus="blur()"/>
                	</td>
                	<td>
                		<div style="height:26px;">
                			<label class="tabmovtex">N&uacute;mero de transmisi&oacute;n:</label><br/>
                		</div>
                		<input type="text" size="10" name="r_numtran" value="${requestScope.r_numtran}" onfocus="blur()"/>
                	</td>
                	<td>
                		<div style="height:26px;">
	                		<label class="tabmovtex">Fecha de transmisi&oacute;n:</label><br/>
                		</div>
                		<input type="text" size="12" name="r_fchtran" value="${requestScope.r_fchtran}" onfocus="blur()"/>
                	</td>
                	<td>
                		<div style="height:26px;">
	                		<label class="tabmovtex">Total de registros:</label><br/>
                		</div>
                		<input type="text" size="10" name="text29" value="${requestScope.r_totregs}" onfocus="blur()"/>
                	</td>
                	<td>
                		<div style="height:26px;">
	                		<label class="tabmovtex">Registros aceptados:</label><br/>
                		</div>
                		<input type="text" size="10" name="text31" value="${requestScope.r_acept}" onfocus="blur()"/>
                	</td>
                	<td>
                		<div style="height:26px;">
	                		<label class="tabmovtex">Registros rechazados:</label><br/>
                		</div>
                		<input type="text" size="10" name="text32" value="${requestScope.r_rech}" onfocus="blur()"/>
                	</td>
                </tr>
              </table>
               <INPUT TYPE="hidden" SIZE="4" NAME="Sucursal" value="${requestScope.Sucursal}"  maxlength="4" onChange="displayWarning();">
		${requestScope.ContenidoArchivo}
		${requestScope.ligaDownload}
		<br/>
        <table style="border: 0px;" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="78">
              <A HREF="javascript:js_envio();"><img style="border: 0px;" name="boton1" src="/gifs/EnlaceMig/gbo25520.gif" width="80" height="22" alt="Enviar"/></a>
            </td>
            <td align="left" valign="top" width="77">
              <A HREF="javascript:cancelaAltaIndividual();"><img style="border: 0px;" name="imageField32" src="/gifs/EnlaceMig/gbo25190.gif" width="80" height="22" alt="Cancelar"/></a>
            </td>
			<td>
			  <A HREF="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" style=" border: 0px;" alt="Imprimir" width="80" height="22" /></a>
			</td>
          </tr>
        </table>
        <br/>
      </td>
    </tr>
</table>
<INPUT TYPE="hidden" VALUE="1" NAME="statusDup"/>
<INPUT TYPE="hidden" VALUE="0" NAME="operacion"/>
<INPUT TYPE="hidden" VALUE="-1" NAME="registro"/>
<INPUT TYPE="hidden" VALUE="" NAME="secuencia"/>
<INPUT TYPE="hidden" VALUE="" NAME="nombre_nuevo"/>
<INPUT TYPE="hidden" VALUE="${requestScope.tipo_archivo}" NAME="tipo_archivo"/>
<INPUT TYPE="hidden" VALUE="${requestScope.total_registros}" NAME="total_registros"/>
<INPUT TYPE="hidden" VALUE="${requestScope.max_registros}" NAME="max_registros"/>
<INPUT TYPE="hidden" VALUE="${requestScope.lista_numeros_empleado}" NAME="lista_numeros_empleado"/>
<INPUT TYPE="hidden" VALUE="${requestScope.archivoEstatus}" NAME="archivoEstatus"/>
<INPUT TYPE="hidden" VALUE="${requestScope.registros_aceptados}" NAME="registros_aceptados"/>
<INPUT TYPE="hidden" VALUE="${requestScope.basefilename}" NAME="basefilename"/>
<INPUT TYPE="hidden" VALUE="${requestScope.facultades}" NAME="facultades"/>
</FORM>
<script type="text/javascript">
${requestScope.mensaje_js}
</script>
<div id="divNoVisible" style="display: none;"></div>
</body>
<head><META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
<META HTTP-EQUIV="Expires" CONTENT="-1"/>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
<META HTTP-EQUIV="Expires" CONTENT="-1"/>
</head>
</html>