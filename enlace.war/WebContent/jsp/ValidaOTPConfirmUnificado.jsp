<%@page import="mx.altec.enlace.bo.BaseResource;"%>

<head>
	<meta http-equiv="Content-Type" content="text/html;" />
	<meta name="Codigo de Pantalla" content="s26050" />

	<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

	<script type="text/javascript" SRC= "/EnlaceMig/Convertidor.js"></script>

	<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
	<script type="text/javascript"    SRC="/EnlaceMig/cuadroDialogo.js"></script>
	<script type="text/javascript"    SRC="/EnlaceMig/scrImpresion.js"></script>
	<script type="text/javascript" SRC="/EnlaceMig/list.js"></script>
	<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
	<script type="text/javascript">fwLoadMenus();</script>

	<script type="text/javascript">
		function fcnBtnCancelar(){
        	document.form1.action = document.form1.urlCancelar.value;
        	document.form1.submit();
        }
		function oculta() {
			document.getElementById("enviar").style.visibility="hidden";
			document.getElementById("enviar2").style.visibility="visible";
		}
		function visualiza() {
			document.getElementById("enviar").style.visibility="visible";
			document.getElementById("enviar2").style.visibility="hidden";
		}

		function MM_preloadImages() { //v3.0
		 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}

		function MM_swapImgRestore() { //v3.0
		  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

		function MM_findObj(n, d) { //v3.0
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
		}

		function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}

		function unSoloContrato(){
		cuadroDialogo ('Solo existe un contrato asociado.',1);
		 }

		function MM_nbGroup(event, grpName) {
			var i,img,nbArr,args=MM_nbGroup.arguments;
		  	if (event == "init" && args.length > 2) {
		    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
			img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
			if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
			nbArr[nbArr.length] = img;
			for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
			  if (!img.MM_up) img.MM_up = img.src;
			  img.src = img.MM_dn = args[i+1];
			  nbArr[nbArr.length] = img;
		    } }
		  } else if (event == "over") {
		    document.MM_nbOver = nbArr = new Array();
			 for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
			if (!img.MM_up) img.MM_up = img.src;
			img.src = (img.MM_dn && args[i+2]) ? args[i+2] : args[i+1];
			nbArr[nbArr.length] = img;
		    }
		  } else if (event == "out" ) {
			 for (i=0; i < document.MM_nbOver.length; i++) {
		 img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
		  } else if (event == "down") {
			if ((nbArr = document[grpName]) != null)
		 for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
			 document[grpName] = nbArr = new Array();
		    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
			if (!img.MM_up) img.MM_up = img.src;
			img.src = img.MM_dn = args[i+1];
		 nbArr[nbArr.length] = img;
		  } }
		}

		function validaForma() {
			token = document.form1.token.value;
			if(token.length != 8) {
				cuadroDialogo ("La longitud de la contrase&ntilde;a debe ser de 8 posiciones.", 1);
				visualiza();
				return false;
			}
			if(!validaFormato(token)) {
				cuadroDialogo ("La contrase&ntilde;a debe ser num&eacute;rica.", 1);
				visualiza();
				return false;
			}
			//Esta validacion es para Unificacion de Token en Nomina Epyme Pago
			if( document.form1.vieneDe.value === "OpcionesNomina" || document.form1.vieneDe.value === "OpcionesNominaLn" ){
				js_envioConOTP();
			} else {
				oculta();
				document.form1.submit();
			}
		}
		function onkeyPress(e) {
			tecla=(document.all) ? e.keyCode : e.which;
			if(tecla==13) 
			{
				if(!validaForma())
				{
					return false;
				}
			}
		}	
		function validaFormato (pwd) {
			var TemplateF=/^[\d]{8,8}$/i;  //dominio de 0-9
			return TemplateF.test(pwd);  //Compara "pwd" con el formato "Template"
		}
		<% if( request.getSession().getAttribute("session")!=null)
			out.println(((BaseResource)request.getSession().getAttribute("session")).getFuncionesDeMenu() );
		%>
	</script>
	<style type="text/css">

	.tdOtro {

		font-family: Verdana, Arial, Helvetica, sans-serif;

		font-size: 10px;

		font-weight: normal;

		color: #666666;

		text-decoration: none;

	}

	.titulo {

		font-family: Verdana, Arial, Helvetica, sans-serif;

		font-size: 12px;

		font-weight: bold;

		color: #FF0000;

		text-decoration: none;

	}

	.tituloCopy {

		font-family: Verdana, Arial, Helvetica, sans-serif;

		font-size: 12px;

		font-weight: bold;

		color: #666666;

		text-decoration: none;

	}

	.preguntaColor {
		color: #666666;
	}

	</style>
</head>

<form name="form1" method="post" action="/Enlace/enlaceMig/transferencia?opctrans=1&valida=0">
		<input type="hidden" name="vieneDe" value="<%= request.getAttribute("vieneDe")%>" />
		<input type="hidden" name="urlCancelar" value="<%= request.getAttribute("urlCancelar")%>" />
		<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="641">
  			<TR>
  				 <td ALIGN="CENTER">
    				<table width="641" border="0">
    				<tr>
			        	<td class="tdOtro" align="center">
				        	<%if (request.getSession().getAttribute("mensajeSession") != null) { %>
							<table  class="textabdatcla"  cellspacing="0" cellpadding="1" width="641">
								<tr>
									<th style=" height: 25px;" align="center" class="tittabdat" colspan=2>Confirmaci&oacute;n</th>
								</tr>
								<tr>
									<td class="tdOtro" class="tabmovtex1" align="right" width="50"><img src="/gifs/EnlaceMig/gic25050.gif" style="border:0;"/></td>
									<td class="tabmovtex1 preguntaColor" align="center"><%= request.getSession().getAttribute("mensajeSession")%></td>
								</tr>
							</table>
							<%}%>
						</td>
		        	</tr>

    					<tr>
    						<td align="center">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0">
    								<tr>
    									<td style=" height: 20px;" align="center" >&nbsp;</td>
    								</tr>
    								<tr>
    									<td style=" height: 20px; bgcolor: #94B2DF;" align="center" class="tituloCopy">
    									<p class="tituloCopy" style="color:#000000;" >Capture
							              su contrase&ntilde;a din&aacute;mica</p>
							            </td>
							        </tr>
							        <tr>
							        	<td class="tdOtro">&nbsp;</td>
							        </tr>
							        <tr>
							        	<td class="tdOtro" align="center">
							        		<table width="56%" border="0" cellspacing="0" cellpadding="0">
							        			<tr valign="middle">
							        				<td class="tdOtro" width="150" class="tituloCopy">Contrase&ntilde;a din&aacute;mica:</td>
													<td class="tdOtro">
												<input name="token" type="password" size="8" maxlength="8" onkeypress="return onkeyPress(event);"/>

										            </td>
										        </tr>
										    </table>
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr align="center"  id="enviar2" style="visibility:hidden" class="tabmovtex">
										<td class="tdOtro">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
									</tr>
									<tr id="enviar" >
										<td class="tdOtro" align="center">
											<a href="javascript:validaForma();" ><img src="/gifs/EnlaceMig/gbo25280.gif" width="84" height="26" style="border-style: none;"/></a>
											<%
											if( request.getSession().getAttribute("valTokenRSA")!=null){
											%>
												<a href="javascript:history.go(-2);" ><img src="/gifs/EnlaceMig/gbo25190.gif" width="84" height="26" style="border-style: none;"/></a>
											<%}else{%>
												<a href="javascript:fcnBtnCancelar();" ><img src="/gifs/EnlaceMig/gbo25190.gif" width="84" height="26" style="border-style: none;"/></a>
											<%}%>
										</td>
									</tr>
									<tr>
										<td class="tdOtro">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr valign="top">
													<td class="tdOtro" width="36%"><strong>Paso 1.</strong> <br/>Presione el bot&oacute;n de su token</td>
													<td class="tdOtro">&nbsp;</td>
													<td class="tdOtro" width="36%"><strong>Paso 2. </strong><br/>Capture la contrase&ntilde;a din&aacute;mica que aparece en
									                  el Token.</td>
									            </tr>
									        </table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="tdOtro" align="center">
								<table width="500" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="tdOtro" width="2"><img src="/gifs/EnlaceMig/img001.gif" width="173" height="49"/></td>
										<td class="tdOtro" width="2"><img src="/gifs/EnlaceMig/img4.gif" width="42" height="49"/></td>
										<td class="tdOtro" width="2"><img src="/gifs/EnlaceMig/img7.gif" width="97" height="49"/></td>
								        <td class="tdOtro"><img src="/gifs/EnlaceMig/img12.gif" width="205" height="49"/></td>
							        </tr>
							        <tr>
							          <td class="tdOtro"><img src="/gifs/EnlaceMig/img2.gif" width="173" height="56"></td>
								      <td class="tdOtro"><a href="#"><img src="/gifs/EnlaceMig/img5_an.gif" width="42" height="56" style="border-style: none;"/></a></td>
							          <td class="tdOtro">
							          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							              <tr>
							              	<td class="tdOtro"><img src="/gifs/EnlaceMig/img8.gif" width="97" /></td>
							              </tr>
							              <tr>
							              	<td class="tdOtro">
							              		<table width="100%" border="0" cellspacing="0" cellpadding="0">
								                    <tr>
								                      <td class="tdOtro" width="2"><img src="/gifs/EnlaceMig/img9.gif" width="18"/></td>
								                      <td class="tdOtro" align="center" style="background-color: #A3B28E; color=#000000;" >12345678</td>
								                    </tr>
								                </table>
								            </td>
								          </tr>
								          <tr>
								          	<td class="tdOtro"><img src="/gifs/EnlaceMig/img10.gif" width="97"/></td>
								          </tr>
								        </table>
								      </td>
								      <td class="tdOtro"><img src="/gifs/EnlaceMig/img13.gif" width="205" height="56"/></td>
								    </tr>
								    <tr>
										<td class="tdOtro"><img src="/gifs/EnlaceMig/img3.gif" width="173" height="30"/></td>
										<td class="tdOtro"><img src="/gifs/EnlaceMig/img6.gif" width="42" height="30"/></td>
										<td class="tdOtro"><img src="/gifs/EnlaceMig/img11.gif" width="97" height="30"/></td>
										<td class="tdOtro"><img src="/gifs/EnlaceMig/img14.gif" width="205" height="30"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</TD>
			</TR>
		</TABLE>

<INPUT TYPE="HIDDEN" NAME="arregloctas_origen2"   VALUE="<%= request.getAttribute("arregloctas_origen2") %>"/>
<INPUT TYPE="HIDDEN" NAME="arregloctas_destino2"  VALUE="<%= request.getAttribute("arregloctas_destino2") %>"/>
<INPUT TYPE="HIDDEN" NAME="arregloimportes2"      VALUE="<%= request.getAttribute("arregloimportes2") %>"/>
<INPUT TYPE="HIDDEN" NAME="arreglofechas2"        VALUE="<%= request.getAttribute("arreglofechas2") %>"/>
<INPUT TYPE="HIDDEN" NAME="arregloconceptos2"     VALUE="<%= request.getAttribute("arregloconceptos2") %>"/>
<INPUT TYPE="HIDDEN" NAME="contador2"             VALUE="<%= request.getAttribute("contador2") %>"/>
<INPUT TYPE="HIDDEN" NAME="arregloestatus2"       VALUE="<%= request.getAttribute("arregloestatus2") %>"/>
<INPUT TYPE="HIDDEN" NAME="miSalida" VALUE="<%= request.getAttribute("miSalida") %>"/>
<INPUT TYPE="HIDDEN" NAME="imptabla"       VALUE="<%= request.getAttribute("imptabla") %>"/>
<INPUT TYPE="HIDDEN" NAME="trans" VALUE="<%= request.getAttribute("trans") %>"/>
<INPUT TYPE="HIDDEN" NAME="TIPOTRANS" VALUE="<%= request.getAttribute("TIPOTRANS") %>"/>
<INPUT TYPE="HIDDEN" NAME="TDC2CHQ" VALUE="<%= request.getAttribute("TDC2CHQ") %>"/>
<INPUT TYPE="HIDDEN" NAME="datos2" VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="datoscta" VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="datosimp" VALUE=""/>

<INPUT TYPE="HIDDEN" NAME="Descargar" VALUE="<%=request.getAttribute("DescargaArchivo") %>"/>

<%
String ivas2_ = "";
String rfcs2_ = "";
if(request.getAttribute("arregloivas2")!=null)
	ivas2_= (String)request.getAttribute("arregloivas2");
if(request.getAttribute("arreglorfcs2")!=null)
	rfcs2_=(String)request.getAttribute("arreglorfcs2");
%>

<INPUT TYPE="HIDDEN" NAME="arreglorfcs2" VALUE="<%= rfcs2_%>"/>
<INPUT TYPE="HIDDEN" NAME="arregloivas2" VALUE="<%= ivas2_ %>"/>

  </form>

<script type="text/javascript">

<%= request.getAttribute("MensajeLogin01") %>
</script>
