<jsp:useBean id="facultadesPagosNomina" class="java.util.HashMap" scope="session"/>
<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@ page import="mx.altec.enlace.bita.BitaConstants"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="mx.altec.enlace.utilerias.Global"%>
<%@page import="java.text.DecimalFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Pago de Nomina</title>
<meta http-equiv="Expires" content="1" />
<meta http-equiv="pragma" content="no-cache" />

<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script type="text/javascript" src="/EnlaceMig/FavoritosEnlaceJS.js"></script>
<%-- Estilos //--%>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
<style type="text/css">
.componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>

<script type="text/javascript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
<%
String tipoCargoIGNomLin= "1";

Object tipoCargoObj =request.getSession().getAttribute("TipoCargoIGNomLin");
			System.out.println("TIPO cARGO A JSP-->"+tipoCargoObj);
						
			 if(tipoCargoObj!=null)
			 {
			 tipoCargoIGNomLin=(String)tipoCargoObj;
			 }
System.out.println("TIPO cARGO A JSP 2-->"+tipoCargoIGNomLin);

if(request.getAttribute("ErroresEnArchivo")!=null)
 {
   System.out.println("Errores en archivo != null " + request.getAttribute("ErroresEnArchivo"));
   out.print(request.getAttribute("ErroresEnArchivo"));
 }
%>

function inicioCta()
{
	var bandera=<%=request.getAttribute("esFavorito")%>
	var bandera2 =<%=request.getAttribute("ctaSel")%>
	if((bandera == "1")||(bandera2 == "1")){
		document.AltaCuentas.numeroCuenta0.value = "<%=request.getAttribute("ctaCargoFav")%>";

	}
	
	document.getElementById("rdbTipoCargoG").disabled = false;
	document.getElementById("rdbTipoCargoI").disabled = false;
	var tipoCargo = <%=tipoCargoIGNomLin%>;	
	
	var global=1;
	var individual=2;
	sincronizaValues(tipoCargo);
	
	if(tipoCargo != null)
	{
		if(tipoCargo == global)
		{
			document.getElementById("rdbTipoCargoG").checked = true;
			document.getElementById("rdbTipoCargoI").checked = false;
		}
		
		if(tipoCargo == individual)
		{
			document.getElementById("rdbTipoCargoG").checked = false;			
			document.getElementById("rdbTipoCargoI").checked = true;
		}
	
	}	
	
	
}

/** INICIA CAMBIO PYME FSW -  MARZO 2015 */
js_diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';

var dia;
var mes;
var anio;
var fecha_completa;

function WindowCalendar()
{

	if(document.AltaCuentas.rdbAplicacion[0].checked) {
	    var m=new Date();
	    n=m.getMonth();
	    n=document.AltaCuentas.mes.value-1;

	    dia=document.AltaCuentas.dia.value;
		mes=document.AltaCuentas.mes.value;
		anio=document.AltaCuentas.anio.value;

	    msg=window.open("/EnlaceMig/calfutNominaPagos.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	    msg.focus();
	}
}

function Actualiza()
{
   document.AltaCuentas.fecha_completa.value=fecha_completa;
}


/** FIN CAMBIO PYME FSW -  MARZO 2015 */

function EnviarArchivo(){
	if(document.AltaCuentas.TipoAlta[0].checked){
		cuadroDialogo("Es necesario seleccionar un archivo de pago.",1);
		return;
	}

	if(document.MantNomina.MantNomina.value==""){

		cuadroDialogo("Es necesario seleccionar un archivo de pago.", 3);

	}else{

		if(document.AltaCuentas.rdbAplicacion[1].checked) {
			document.MantNomina.operacion.value="cargaArchivo";
		    document.MantNomina.action="ImportNominaLn";
		    document.MantNomina.submit();

		} else {
			document.MantNomina.operacion.value="cargaArchivo";
		    document.MantNomina.action="ImportNomina";
		    document.MantNomina.submit();
		 }
	 }
}

function EnviarInd()
 {
	var forma=document.AltaCuentas;
	var contador=0;
	var enviar = false;
	var importeNulo = false;
	if(document.AltaCuentas.flagDecimales.value=='false')
	{
		return;
	}

	if(forma.TipoAlta[1].checked){
		cuadroDialogo("Es necesario seleccionar la opcion Individual.",1);
		return;
	}

	for(j=0;j<forma.length;j++){
		if(forma.elements[j].type=='checkbox'){
			if(forma.elements[j].checked){
				contador++;
				if(forma.elements[j + 1].name.indexOf('monto_') > -1){
					if(forma.elements[j + 1 ].value == null || forma.elements[j + 1].value == ""){
					importeNulo = true;

					}else{
						if(isNaN(forma.elements[j + 1].value)){
							cuadroDialogo("El valor del importe debe ser num&eacute;rico.",1);
							return;
						}else if(forma.elements[j + 1].value < 0){
							cuadroDialogo("El valor de campo Importe es incorrecto.",1);
							return;
						}
					}
				}
			}
		}
	}
	 if(importeNulo) {
	   cuadroDialogo("Es necesario capturar el campo Importe.",1);
	   return;
	 }

	if(contador==0)
	 {
	   cuadroDialogo("Debe seleccionar al menos una opci&oacute;n",1);
	 }
	else
	 {
		var cadena="";
		for(x=1;x<forma.length;x++)
		 {
			if(forma.elements[x].type=="checkbox")
			 if(forma.elements[x].checked)
			   cadena+="1";
			 else
			   cadena+="0";
		 }

		if(obtenerCuentaSeleccionada("comboCuenta0", "origen")){

			if(document.AltaCuentas.rdbAplicacion[1].checked) {
					 		//Validaciones en Linea
		 		enviar = true;
			} else {
				//Validaciones programada
				enviar = true;
			}

			if(enviar){
				forma.cuentasSel.value=cadena;
				forma.action = "InicioNominaLn";
		    	forma.accion.value = "enviar";
				forma.submit();
			}
			}else{
			   var select = document.getElementById("comboCuenta0");
			   if ( select.selectedIndex > 0) {
			   	  cuadroDialogo("La cuenta de cargo es invalida",11);
			   } else {
			   	   cuadroDialogo("Debe seleccionar una cuenta de cargo",11);
			   }

			}
	 }

 }

function seleccionaHorario(horario){

	 var horarios = document.getElementById("horario");
	if (!(horarios === undefined)) {

		for (i = 0; i < horarios.length ; i++) {
			if (horarios[i].value == horario) {
				horarios[i].selected = true;
		      }
		}
	}

 }

function seleccionaTipoPago(tipoPago,elemento){

	 var tiposPago = document.getElementById(elemento);
	if (!(tiposPago === undefined)) {

		for (i = 0; i < tiposPago.length ; i++) {
			if (tiposPago[i].value == tipoPago) {
				tiposPago[i].selected = true;
		      }
		}
	}

}

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;
		document.getElementById(elemento).value = "";
		if(valor !== null) {
				var res = valor.split("\|");
     			if (res[0]=="") {
      				document.getElementById(elemento).value = "";
      				return false;
    			}
    	}
		if(valor !== null && "" !==valor ){
			document.getElementById(elemento).value = obtenerNumCuenta(valor);
			return true;
		}
	}
	return false;
}


function obtenerCuentaSeleccionada2(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;
		if(valor !== null && "" !==valor   ){
	   		var res = valor.split("\|");
     		if (res[0]=="") {
      			document.getElementById(elemento).value = "";
      			return false;
    		}
			document.getElementById(elemento).value = valor;
			return true;
		}
	}
	return false;
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0] + "|" + res[1] + "|"  + res[2] + "|";
    }
}

function obtenerNumCuenta(valor){
    var res = valor.split("\|");
    if(res.length>1){
	    return res[0] ;
    }
}



/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/

function esCtaDuplicada(elemento){

	cuenta = document.getElementById(elemento).value;

	var forma = document.getElementById("AltaCuentas");
	for (var i = 0; i < forma.elements.length; i++) {
		var e = forma.elements[i];
		if (e.type == "checkbox" && e.name == 'ctaAbonoSel')
			if(cuenta.indexOf(e.value) > -1){
				return true;
			}
	}
	return false;
}

function Agregar()
 {
   var forma=document.AltaCuentas;
	if(document.AltaCuentas.flagDecimales.value=='false')
	{
		return;
	}
   obtenerCuentaSeleccionada("comboCuenta0", "origen");

	if(obtenerCuentaSeleccionada2("comboCuenta1", "destino")){

	   for(j=0;j<forma.length;j++)
		 if(forma.elements[j].type=='radio' && forma.elements[j].name=='TipoAlta' )
			if(forma.elements[j].checked)
			   valRadio=forma.elements[j].value;

	     forma.action = "InicioNominaLn";
	     forma.accion.value = "agregar";
	     forma.submit();


	}else{
		var select = document.getElementById("comboCuenta1");
		if ( select.selectedIndex > 0) {
			cuadroDialogo("La cuenta de empleado es invalida",1);
		} else {
			cuadroDialogo("Debe seleccionar una cuenta de empleado",1);
		}


	}

 }




function validaForma(forma)
 {
   /* Mismo Banco ********/
   var i=0;
   var j=0;
   var valRadio=-1;

   for(j=0;j<forma.length;j++)
     if(forma.elements[j].type=='radio' && forma.elements[j].name=='TipoAlta' )
	    if(forma.elements[j].checked)
	       valRadio=forma.elements[j].value;

   if(valRadio==-1)
	 {
	   cuadroDialogo("Debe seleccionar una opci&oacute;n",1);
	   return false;
	 }
   else
   /* Nacionales ********/
   if(valRadio==0)
	{

    }
   else
   /* Importacion ********/
   if(valRadio==1)
	{
	   if(trimString(forma.Archivo.value)=="")
		{
		  forma.Archivo.focus();
		  cuadroDialogo("Debe especificar el nombre del archivo",3);
		  return false;
		}

	   if(trimString(forma.strTramaBN.value)!="")
		{
		  forma.Archivo.focus();
		  cuadroDialogo("Se han registrado operaciones en l&iacute;nea, no puede importar archivos en este momento.",3);
		  return false;
		}
    }
   return true;
 }

function validaCuenta(cuenta,tipo)
 {
   if(trimString(cuenta.value)=="")
	 {
	   cuenta.focus();
	   cuadroDialogo("La cuenta es obligatoria",1);
	   return false;
	 }


	return true;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function especiales(Txt1)
{
	//VSWF BMB se modific� la cadena strEspecial, agregando & como car�cter v�lido
   var strEspecial=" &ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("En el titular o descripcion de la cuenta, no se permiten caracteres especiales",3);
	   return false;
	 }
   return true;
}


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function especialesBI(Txt1,campo)
{
   var strEspecial=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("No se permiten caracteres especiales para " + campo,3);
	   return false;
	 }
   return true;
}


function Limpiar()
 {
		document.AltaCuentas.action = "InicioNominaLn";
	    document.AltaCuentas.accion.value = "iniciar";
	    document.AltaCuentas.submit();
 }

function isInteger(num)
{
  var TemplateI = /^\d*$/;	 /*** Formato de numero entero sin signo */
  return TemplateI.test(num); /**** Compara "num" con el formato "Template" */
}

<%= request.getAttribute("newMenu")%>

function msgCifrado2(){
	habilitarProgramado();
	consultaCuentasTarjetaPago();
	consultaCuentas(1,10,11);

	try {
		var mensajeCifrado = document.AltaCuentas.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
			var arreglo = mensajeCifrado.split("|");
            var msj = arreglo[1];
            var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

function msgCifrado(){
  habilitarProgramado();
  	if ((document.AltaCuentas.esFav.value != null
			&& document.AltaCuentas.esFav.value != ""
			&& document.AltaCuentas.esFav.value == "1")||(
					document.AltaCuentas.ctaSel.value != null
					&& document.AltaCuentas.ctaSel.value != ""
					&& document.AltaCuentas.ctaSel.value == "1")) {
		var cadenaCargo = '0,2,1';
		cadenaCargo = cadenaCargo + ',' + document.AltaCuentas.numeroCuenta0.value + ',';
		consultaMultiplesCuentas(cadenaCargo);
	} else {
		consultaCuentasTarjetaPago();

	}
	consultaCuentas(1,10,11);
	document.AltaCuentas.esFav.value = "";
	document.AltaCuentas.ctaSel.value = "";
	document.AltaCuentas.numeroCuenta0.value = "";
	try {
		var mensajeCifrado = document.AltaCuentas.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
			var arreglo = mensajeCifrado.split("|");
            var msj = arreglo[1];
            var tipo = arreglo[0];
            cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

function habilitarProgramado() {
	if(document.AltaCuentas.rdbAplicacion[1].checked) {
		document.getElementById("fecha_completa").disabled = true;
		document.getElementById("horario").disabled = true;
		var global=1;
		var individual=2;
		document.getElementById("rdbTipoCargoG").disabled = false;
		document.getElementById("rdbTipoCargoI").disabled = false;		
		document.getElementById("rdbTipoCargoG").checked = true;
		var tipoCargo = <%=tipoCargoIGNomLin%>;
				
		if(tipoCargo != null)
		{
			if(tipoCargo == global )
			{
				document.getElementById("rdbTipoCargoG").checked = true;
				document.getElementById("rdbTipoCargoI").checked = false;
				sincronizaValues(global);
			}
			else if(tipoCargo == individual)
			{
				document.getElementById("rdbTipoCargoG").checked = false;			
				document.getElementById("rdbTipoCargoI").checked = true;	
				sincronizaValues(individual);
			}
		}
		else
		{
			document.getElementById("rdbTipoCargoG").checked = true;
			document.getElementById("rdbTipoCargoI").checked = false;
			sincronizaValues(global);
		}
	} else {
			document.getElementById("fecha_completa").disabled = false;
			document.getElementById("horario").disabled = false;
			document.getElementById("rdbTipoCargoG").disabled = true;
			document.getElementById("rdbTipoCargoI").disabled = true;
			}

}

function validaDecimales(campo, mensajeNomina) {
		var cadena = campo.value;
	if(cadena != "")
	{
		var validaPunto = cadena.indexOf(".");
		var cadenaTMP = "";
		if(validaPunto == 0) {
			cadenaTMP = "0" + cadena;
		} else {
			cadenaTMP = cadena;
		}
		if( /^\d+(\.\d{2})?$/.test(cadenaTMP)){
			campo.value = cadenaTMP;
			document.AltaCuentas.flagDecimales.value="true";
			return true;
		} else {
			cuadroDialogo(mensajeNomina,4);
			campo.select();
			document.AltaCuentas.flagDecimales.value="false";
			return false;
		}
	}
}

function archivoErrores()

 {

   ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');

   ventanaInfo1.document.open();

   ventanaInfo1.document.write("<html>");

   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");

   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");

   ventanaInfo1.document.writeln("</head>");

   ventanaInfo1.document.writeln("<body bgcolor='white'>");

   ventanaInfo1.document.writeln("<form>");

   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");

   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>informacion del Archivo Importado</th></tr>");

   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");



   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center width=>");

   ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");

   ventanaInfo1.document.writeln("<td class='tabmovtex1' align='center'><br>No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los siguientes errores.<br><br>");

   ventanaInfo1.document.writeln("</td></tr></table>");



   ventanaInfo1.document.writeln("</td></tr>");

   ventanaInfo1.document.writeln("<tr><td class='textabdatobs'>");
   ventanaInfo1.document.writeln(document.AltaCuentas.archivoEstatus.value +  " <br><br>");
   ventanaInfo1.document.writeln("</td></tr>");

   ventanaInfo1.document.writeln("</table>");

   ventanaInfo1.document.writeln("<table border=0 align=center >");

   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0 alt='Cerrar'></a>");

   ventanaInfo1.document.writeln("<a href = 'javascript:window.print();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a></td></tr>");

   ventanaInfo1.document.writeln("</table>");

   ventanaInfo1.document.writeln("</form>");

   ventanaInfo1.document.writeln("</body>\n</html>");

   ventanaInfo1.document.close();

   ventanaInfo1.focus();

}

function sincronizaValues(valor)
{
	document.AltaCuentas.TipoCargoIG.value = valor;
	document.MantNomina.TipoCargoIG.value = valor;	
}
 
 function getCargaIndGlobal()
 {
    var tipoGlobal="1";
	var tipoIndividual="2";
	var tipoCargoGlobal = document.getElementById("rdbTipoCargoG").checked;
	var tipoCargoInd = document.getElementById("rdbTipoCargoI").checked;
	var valActual = document.getElementById("TipoCargoIG").value;
	
    if(tipoCargoGlobal == true )
    {
    	sincronizaValues(tipoGlobal);
    }
    
    if (tipoCargoInd == true )
    {
    	sincronizaValues(tipoIndividual);
    }
 }
 
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
</script>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
 onLoad="inicioCta();msgCifrado(); <%= session.getAttribute("ArchivoErr")%>; MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" >

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="760">
  <tr valign="top">
    <td width="*">
      <%=request.getAttribute("MenuPrincipal")%>
    </td>
  </tr>
</table>
<%=request.getAttribute("Encabezado")%>
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" alt="" /></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" alt="" /></td>
  </tr>
</table>

<form name="transferencia">
 <input type="hidden" name="Descripcion"/>
</form>



 <table width="800" border="0" cellpadding="0" cellspacing="0" >
 <tr>
 <td width = "30" >
 </td>
  <td>
<FORM  NAME="AltaCuentas" id ="AltaCuentas"  method="post" action="InicioNominaLn">
  <input TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>" />
    <input type="hidden" name="esFav" id="esFav" value="<%=request.getAttribute("esFavorito")%>"/>
    <input type="hidden" name="ctaSel" id="ctaSel" value="<%=request.getAttribute("ctaSel")%>"/>
    <input type="hidden" name="archivoEstatus" id = "archivoEstatus" value='<%

								if(	session.getAttribute("archivoEstatus") !=null)

									out.println( session.getAttribute("archivoEstatus"));

								else

									if (request.getAttribute("archivoEstatus")!= null)

										out.println(request.getAttribute("archivoEstatus"));

							%>'/>
    <input type="hidden" name="TipoCargoIG" id="TipoCargoIG" value="<%=tipoCargoIGNomLin%>"/>

    <table align="center" border="0" cellpadding="2" cellspacing="3" width="100%">
    

   <%
   if (((Boolean) facultadesPagosNomina.get ("PagNomTrad")).booleanValue() || ((Boolean) facultadesPagosNomina.get ("PagNomLin")).booleanValue())
    {
   %>
   <%-- ****************************************************************************************************** //--%>
   <%--  Pagos Individuales //--%>
   <%-- ****************************************************************************************************** //--%>
   <tr>
     <td colspan="4" class="tittabdat"><b> B&uacute;squeda de cuentas</b></td>
   </tr>
   <tr>
    <td colspan="4" >
    	<%@ include file="/jsp/filtroConsultaCuentasNomina.jspf" %>
    </td>
   </tr>




   <tr >
	<td class ="tittabdat" colspan="4">
		<%
		String tipoAlta = (String) request.getAttribute("TipoAlta");
		String chkTipoInd  = "" ;

		if(tipoAlta == null || "I".equals(tipoAlta )){
			chkTipoInd = "checked";
		}



		%>
			<INPUT type="hidden" name="flagDecimales" value="true"/>
		<INPUT TYPE="radio" value="I" NAME="TipoAlta" <%=chkTipoInd %>  /><b>Individual</b>
	<td>
	<tr>
	<tr>
		<td class="textabdatcla" colspan="4" width="100%">
			<table width="100%"  border="0" cellspacing="5" cellpadding="0">
			   <tr>
				<td class="tabmovtexbol" colspan = "2" nowrap><b>
				   * Cuenta de cargo</b><br />
			   		<%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
					<input type="hidden" name="origen" id="origen" value="" />
				   </td>

			   </tr>
			   <tr>
				 <td class="tabmovtexbol" colspan = "2" nowrap>

		         <b>* Nombre y cuenta del empleado:</b><br></br>
		         <%--  <%@ include file="/jsp/listaCuentas.jspf" %> --%>
		         <select id="comboCuenta1" name = "comboCuenta1"  class="comboCuenta" >

				</select>
		 		<input type="hidden" name="destino" id="destino" value="" />

		         </td>
			   </tr>

			   <tr>
					<td class="textabdatcla">
						<br/>
						<b>* Campos requeridos.</b>
					</td>
					<td class="textabdatcla" colspan="2">
						<br/>
					</td>
				</tr>

			</table>
		</td>
	</tr>

	<tr>
     <td class="tabmovtex" style="background-color: white;" colspan="4"></td>
   </tr>





    <tr>
      <td colspan="4" class="tittabdat"><b> Forma de Aplicaci&oacute;n</b></td>
    </tr>
    <tr>
		 <td class="textabdatcla" colspan="4" width="100%">
       <table border="0" class="textabdatcla" cellspacing="3" cellpadding="2" width="100%" >
		<%
			String chkLinea = "";
			String chkProgramada ="checked";
			String tipoApl = "";
			tipoApl = (String) request.getAttribute("rdbAplicacion");
			if(tipoApl != null){
				if("T".equals(tipoApl)){
					chkProgramada = "checked";
				}else if("L".equals(tipoApl)){
					chkLinea = "checked";
				}else {
					chkProgramada = "checked";
				}
			}
			System.out.println(tipoApl);
			System.out.println(chkLinea);
			System.out.println(chkProgramada);
			String chkGlobal = "checked";
			String chkIndividual = "";
		%>
	    <tr>
		 <td class="tabmovtex" width="10"><br></br></td>
         <td class="tabmovtexbol" colspan="3" align="center">
		 <INPUT TYPE="radio" value="T" NAME="rdbAplicacion" <%=chkProgramada%> onclick="habilitarProgramado();"  />&nbsp; Programada


		 </td>
         <td class="tabmovtexbol" colspan="3" align="center">
		 
		  <%
		 	if (((Boolean) facultadesPagosNomina.get ("PagNomLin")).booleanValue())
    	 	{
    	 	%>
    	 	<INPUT TYPE="radio" value="L" NAME="rdbAplicacion"  <%=chkLinea%>  onclick="habilitarProgramado();" />&nbsp; En l&iacute;nea

		 	<%} else{

		 	%>
			 <INPUT TYPE="radio" value="L" NAME="rdbAplicacion"  onclick="habilitarProgramado();" style="display:none;" />
		 	<%}%>

		  </td>
		</tr>
		<tr>
				<td class="tabmovtex" width="10"><br /></td>
                <td class="tabmovtexbol" nowrap valign="middle" colspan="3">
                <fieldset>
        	    * Fecha de aplicaci&oacute;n:						
                    <%= request.getAttribute("calendario_normal") %>
					<%= request.getAttribute("campos_fecha") %>
                 </fieldset>

				 </td>
        
                 <td class="tabmovtexbol" nowrap valign="middle">
				 <input type="hidden" id = "horario" name = "horario" value="" />
                   <%-- select id="horario" name="horario" style="width: 100%; visibility:hidden " class="tabmovtexbol" >
                   		<option value="06:00">06:00</option>
                   		<option value="09:00">09:00</option>
                   		<option value="11:00">11:00</option>
                   		<option value="13:00">13:00</option>
                   		<option value="15:00">15:00</option>
                   		<option value="17:00">17:00</option>
                   </select>--%>
                   <%if("T".equals(tipoApl)){
                   %>
	                   <script type="text/javascript">
	                   habilitarProgramado();
	                   </script>
                   <%}%>
				    
					<c:if test="${facultadesPagosNomina.get ('PagNomLin') == true}">
					   <fieldset>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <INPUT TYPE="radio" value="TG" NAME="rdbTipoCargo" id="rdbTipoCargoG" 
                            onclick="getCargaIndGlobal();"/>Cargo Global 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <INPUT TYPE="radio" value="TI" NAME="rdbTipoCargo" id="rdbTipoCargoI" 
                            onclick="getCargaIndGlobal();"/>Cargo Individual
                    </fieldset>
					</c:if>
					<c:if test="${facultadesPagosNomina.get ('PagNomLin') == false}">					                    
                    <div id='elementosOcultos' style='display:none;'>
                        <INPUT TYPE="radio" value="TG" NAME="rdbTipoCargo" id="rdbTipoCargoG" 
                        onclick="getCargaIndGlobal();"/>
                        <INPUT TYPE="radio" value="TI" NAME="rdbTipoCargo" id="rdbTipoCargoI" 
                            onclick="getCargaIndGlobal();"/>
                    </div>
					</c:if>
                 </td>
          </tr>
		</table>
		</td>
	</tr>





   <%
   ArchivoNominaBean nominaBean = null;
   LinkedHashMap<String ,ArchivoNominaDetBean> hmDetalleBean = null;
   ArchivoNominaDetBean detalleBean = null;
   nominaBean = (ArchivoNominaBean)request.getAttribute("archivoNominaBeanReq");
   String listaConceptos=(String)request.getAttribute("tiposPago");
   String bgColor = "";
   int contador = 2;
   Iterator <ArchivoNominaDetBean> it = null;
   String stClass ="";

   if(nominaBean != null ){
	   hmDetalleBean = nominaBean.getDetalleArchivo();

	   if(hmDetalleBean !=null){

   %>

   <tr>
   <td colspan="4" width="100%">
   	<table width="100%" border="0"  cellspacing="2" cellpadding="3" >
		<tr>
			<td colspan="1" class="tittabdat"></td>
   			<td colspan="2" class="tittabdat">* Cuenta Abono</td>
   			<td colspan="1" class="tittabdat">* Importe</td>
   			<td colspan="1" class="tittabdat">* Concepto de Pago</td>
		</tr>

		<%
		it = hmDetalleBean.values().iterator();

		double impTotal = 0;
		int totalRegistros = 0;
		String tipoAbono = "";
		  while(it.hasNext()){

			  if(contador % 2 == 0){
			    bgColor = "#CCCCCC";
			    stClass = "textabdatobs";

			  }else{
				bgColor = "#EBEBEB";
				stClass = "textabdatcla";
			  }

			  detalleBean = it.next();
			  DecimalFormat formatoDecimal = new DecimalFormat("################.00");
			  String importeDecimal = formatoDecimal.format(detalleBean.getImporte());
			  String importe = (detalleBean.getImporte() == 0 ? "" : importeDecimal);
			  String idTipoAbono = "tipoAbono_" + detalleBean.getSecuencia();
			  String idMonto = "monto_" + detalleBean.getSecuencia();
			  impTotal += detalleBean.getImporte();
			  tipoAbono = (detalleBean.getTipoPago() == null ? "01" : detalleBean.getTipoPago()) ;
		%>


		<tr bgcolor="<%=bgColor%>">
			<td class="<%=stClass%>" align="right">
			<input type="checkbox" value="<%=detalleBean.getSecuencia()%>" name = "ctaAbonoSel" checked="checked" />
			</td>
			<td class="<%=stClass%>">
				<%=detalleBean.getCuentaAbono()%>
			</td>
			<td class="<%=stClass%>">
				<%=detalleBean.getNombre()%>&nbsp;
				<%=detalleBean.getAppPaterno()%>&nbsp;
			    <%=detalleBean.getAppMaterno()%>&nbsp;
			</td>
			<td class="<%=stClass%>">
					<INPUT type="TEXT" id = "<%=idMonto%>" name="<%=idMonto%>" size="22" maxlength=15
					class="tabmovtexbol" value = "<%=importe%>" onblur="validaDecimales(this, 'El campo Importe debe ser num�rico y solo puede contener dos decimales.')" />
			</td>
			<td class="<%=stClass%>">
			<select id="<%=idTipoAbono%>" name="<%=idTipoAbono%>" style="width: 100%" class="tabmovtexbol" >
			            <%=listaConceptos%>
            </select>

                   <script type="text/javascript">
                   seleccionaTipoPago("<%=tipoAbono%>","<%=idTipoAbono%>");
                   </script>
            </td>
		</tr>


		<%
		contador++;
		totalRegistros++;
		  }
		%>
		<%--
		<tr>
			<td>
			</td>
			<td colspan = "2"  align="center">
				Total Registros:<input type = "text" readonly="readonly" name = "totReg" value = "<%=totalRegistros%>"/>
			</td>
			<td colspan = "2" align="center">
			    Total Abono:<input type = "text" readonly="readonly" name = "totAbono" value = "<%=impTotal%>"/>
			</td>
		</tr>
		 --%>
	</table>
   	</td>
   </tr>

   <%
	   }
    }
  %>

    <tr>
     <td class="tabmovtex" bgcolor="white" colspan="4">
     	<br />
	  <table align="center" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	    <td>
	    <%
	    if(nominaBean != null ){
	    hmDetalleBean = nominaBean.getDetalleArchivo();

	    if(hmDetalleBean !=null && hmDetalleBean.size() > 0){
		%>
	    	<A href = "javascript:EnviarInd();" border = "0"><img src = "/gifs/EnlaceMig/gbo25520.gif" style=" border: 0px;" alt="Enviar" /></a>
	    <%}} %>
	    </td>
	     <td>
             <A href = "javascript:Agregar();" border = "0">
                <img src = "/gifs/EnlaceMig/gbo25290.gif" style=" border: 0px;"  alt="Agregar" />
            </a>
         </td>
         <td>
             <A href = "javascript:Limpiar();" border = "0">
                <img src = "/gifs/EnlaceMig/gbo25250.gif" style=" border: 0px;"  alt="Limpiar" />
            </a>
         </td>
        <td valign="top" width="78">
                <a href="javascript:consultarFavoritos('IN04','InicioNominaLn');" border="0"><img style=" border:0;" src="/gifs/EnlaceMig/favoritosNom.png" alt="Consultar Favoritos"></a>
            </td>
	    </tr>
	  </table>

     </td>
    </tr>

  <%
  }%>

   <%
    if (((Boolean) facultadesPagosNomina.get ("PagNomTrad")).booleanValue())
     {
    	String tipoAlta = (String) request.getAttribute("TipoAlta");
    	String chkTipoArch = "";
    	if("A".equals(tipoAlta)){
    		 chkTipoArch = "checked";
    	}
   %>

  <tr>
	<td class="tittabdat" colspan="4"><INPUT TYPE="radio" value="A" NAME="TipoAlta" <%=chkTipoArch%>   />&nbsp;<b>Seleccione el archivo a importar.</b></td>
  </tr>

  <%}%>
<%
  String modulo="3";
  String submodulo = "2";


  if(request.getAttribute("Modulo")!=null)
	  modulo=(String)request.getAttribute("Modulo");

  if(request.getAttribute("submodulo")!=null)
	  submodulo=(String)request.getAttribute("submodulo");

 %>
 <input type="hidden" name="Modulo" value="<%=modulo%>"/>
 <input type="hidden" name="submodulo" value="<%=submodulo%>"/>
 <input type="hidden" name ="accion" value ="" />
 <input type="hidden" name="cuentasSel" />
 </form>
  <%
    if (((Boolean) facultadesPagosNomina.get ("PagNomTrad")).booleanValue())
     {
   %>

   <%-- ****************************************************************************************************** //--%>
   <%-- Importar Archivo //--%>
   <%-- ****************************************************************************************************** //--%>

  <tr>
   <td class="textabdatcla" colspan="4">
   <FORM  NAME="MantNomina"  ENCTYPE="multipart/form-data" method="post" action="ImportNomina">
        <input type="hidden" name="TipoCargoIG" id="TipoCargoIG" value="<%=tipoCargoIGNomLin%>"/> 
 		<INPUT TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>" />
		<table border="0" class="textabdatcla" cellspacing="3" cellpadding="2" >
		 <tr>
		  <td class="tabmovtex" width="2"><br /></td>
		  <td class="tabmovtex11">
		  		Buscar archivo: &nbsp; <INPUT TYPE="file" NAME="MantNomina" class="componentesHtml" value="Archivo" />
		  		<input type="hidden" name ="operacion" value ="" />
		  </td>
		 </tr>
		 <tr>
		  <td colspan="2" class="tabmovtex"><br /></td>
		 </tr>
		</table>
	</FORM>
	</td>
  </tr>
  <%
    }
   %>
 </table>



 </td>
 </tr>

 </table>

 <%if (((Boolean) facultadesPagosNomina.get ("PagNomTrad")).booleanValue() || ((Boolean) facultadesPagosNomina.get ("PagNomLin")).booleanValue())
 {
 %>
 <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td align="center"><A href = "javascript:EnviarArchivo();" border = "0"><img src = "/gifs/EnlaceMig/gbo25520.gif" style=" border: 0px;" alt="Enviar" /></a></td>
    </tr>
  </table>
  <%
   }
  %>


</body>
</html>

<script type="text/javascript">



<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
	if (request.getAttribute("infoUser")!= null) {
		out.println(request.getAttribute("infoUser"));
	}

	if (session.getAttribute("archivoEstatus")!= null) {
		session.removeAttribute("archivoEstatus");
	}


%>
<%
if (session.getAttribute("ArchivoErr")!= null) {
	out.println(session.getAttribute("ArchivoErr"));
	session.removeAttribute("ArchivoErr");
}%>
</script>