<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<html>
<head>
	<title>Posici&oacute;n - Movimientos</TITLE>
<%-- 01/10/2002 Correci�n ortogr�fica. --%>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" type="text/javascript">

<% if(request.getAttribute("Errores") != null ) out.print(request.getAttribute("Errores")); %>

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/**********************************************************************************/
// Funciones para deplegar la ventana con la informacion de la cuenta

var registrosTabla=new Array();
var camposTabla=new Array();
var totalCampos=0;
var totalRegistros=0;

function numeroRegistros(campo2)
    {
      totalRegistros=0;
      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
      //alert("Registros encontrados: "+ totalRegistros);
    }

function llenaTabla(campo2)
 {
    var strCuentas="";
    strCuentas=campo2;
    var vartmp="";

    if(totalRegistros>=1)
      {
        for(i=0;i<totalRegistros;i++)
         {
           vartmp=strCuentas.substring(0,strCuentas.indexOf('@'));
           registrosTabla[i]=vartmp;
           if(strCuentas.indexOf('@')>0)
             strCuentas=strCuentas.substring(strCuentas.indexOf('@')+1,strCuentas.length);
         }
      }
	separaCampos(registrosTabla);
 }

function separaCampos(arrCuentas)
 {
     var i=0;
     var j=0;
     var vartmp="";
     var regtmp="";

     for(i=0;i<=totalRegistros;i++)
       camposTabla[i]=new Array();

     totalCampos=0;
     if(totalRegistros>=1)
      {
        regtmp+=arrCuentas[0];
        for(i=0;i<regtmp.length;i++)
         {
           if(regtmp.charAt(i)=='|')
            totalCampos++;
         }
        for(i=0;i<totalRegistros;i++)
         {
           regtmp=arrCuentas[i];
		   for(j=0;j<totalCampos;j++)
            {
              if(regtmp.charAt(0)=="|")
               {
                 vartmp="";
                 regtmp=regtmp.substring(1,regtmp.length);
               }
              else
               {
                 vartmp=regtmp.substring(0,regtmp.indexOf("|"));
                 if(regtmp.indexOf("|")>0)
                   regtmp=regtmp.substring(regtmp.indexOf("|")+1,regtmp.length);
               }
              camposTabla[i][j]=vartmp;
			}
         }
      }
 }

/********************************************************************************/

function SeleccionaTodos()
 {
   forma=document.SaldoCred;

   if(forma.Todas.checked==true)
    {
 	  forma.Todas.checked=true;
      for(i2=0;i2<forma.length;i2++)
       if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
         forma.elements[i2].checked=true;
    }
   else
    {
      forma.Todas.checked=false;
      for(i2=0;i2<forma.length;i2++)
       if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
         forma.elements[i2].checked=false;
    }
 }


function Enviar()
 {
   var i=0;
   var contador=0;
   var nuevaTrama="";
   var Env='Posicion';

   for(i=0;i<document.SaldoCred.length;i++)
    {
	  if(document.SaldoCred.elements[i].type=='radio')
	    if(document.SaldoCred.elements[i].checked==true)
	    {
		  totalRegistros=1;
		  contador++;

          llenaTabla(document.SaldoCred.elements[i].value);

		  nuevaTrama+=camposTabla[0][0]+"|";
		  nuevaTrama+=camposTabla[0][2]+"|";

		  if(camposTabla[0][3]==5)
		    nuevaTrama+="Tarjeta"+"|";
		  else
		    nuevaTrama+="Linea"+"|";
		  nuevaTrama+=camposTabla[0][1];
		  nuevaTrama+="@";

	      document.SaldoCred.strCuentas.value=nuevaTrama;
		  break;
		}

	  if(document.SaldoCred.elements[i].type=='checkbox')
	    if(document.SaldoCred.elements[i].name!='Todas')
		  if(document.SaldoCred.elements[i].checked==true)
	    {
		  totalRegistros=1;
		  contador++;

		  llenaTabla(document.SaldoCred.elements[i].name);
		  document.SaldoCred.radTabla.value=document.SaldoCred.elements[i].name;

		  nuevaTrama=camposTabla[0][0]+"|";
		  nuevaTrama+=camposTabla[0][2]+"|";
		  if(camposTabla[0][3]==5)
		    nuevaTrama+="Tarjeta"+"|";
		  else
		    nuevaTrama+="Linea"+"|";
		  nuevaTrama+=camposTabla[0][1];
		  nuevaTrama+="@";
		  document.SaldoCred.strCuentas.value+=nuevaTrama;
		}
	}

   if(contador==0)
    {
	  //alert("Debe seleccionar m�nimo una cuenta.");
	  cuadroDialogo("Debe seleccionar m&iacute;nimo una cuenta.",3);
      return;
	}

   if(Env=='Posicion')
     document.SaldoCred.action="MCL_Posicion?Modulo=0";
   else
     document.SaldoCred.action="MCL_Movimientos?Modulo=0";
   document.SaldoCred.submit();
 }
<% if(request.getAttribute("strScript") != null ) out.print(request.getAttribute("strScript")); %>

<% if(request.getAttribute("newMenu") != null ) out.print(request.getAttribute("newMenu")); %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<% if(request.getAttribute("MenuPrincipal") != null ) out.print(request.getAttribute("MenuPrincipal")); %>
   </td>
  </tr>
</table>

<% if(request.getAttribute("Encabezado") != null ) out.print( request.getAttribute( "Encabezado" ) ); %>

<FORM  NAME="SaldoCred" method=post action="MCL_Consultar?Modulo=0">
  <p>
   <!-- Datos del Usuario -->
   <!--
   <table border=0 align=center>
   <tr>
     <th bgcolor='#DDDDDD'><font color=black>Datos del Usuario</font></th>
   </tr>
   <tr>
     <td align="center">
       <font size="2" face="Arial, Helvetica, sans-serif">
	     <b>Contrato: </b><% if(request.getAttribute("NumContrato") != null ) out.print(request.getAttribute("NumContrato")); %> <% if(request.getAttribute("NomContrato") != null ) out.print(request.getAttribute("NomContrato")); %>
	   </font>
	 </td>
   </tr>
   <tr>
     <td bgcolor='#FFFFFF'>
	   <b>Usuario: </b><% if(request.getAttribute("NumUsuario") != null ) out.print(request.getAttribute("NumUsuario")); %> <% if(request.getAttribute("NomUsuario") != null ) out.print(request.getAttribute("NomUsuario")); %>
	 </td>
   </tr>
   <tr>
     <td><br></td>
   </tr>
   </table>
   //-->

   <table border=0 align=center>
   <tr>
     <td><% if(request.getAttribute("Tabla") != null ) out.print(request.getAttribute("Tabla")); %></td>
   </tr>
  </table>
  <!--
  <input type=text name=Tipo value='<% if(request.getAttribute("Tipo") != null ) out.print(request.getAttribute("Tipo")); %>'>
  //-->
  <input type=hidden name=Cuentas>
  <input type=hidden name=Modulo value=0>
  <input type=hidden name=strCuentas>
  <input type=hidden name=FacArchivo value='<% if(request.getAttribute("FacArchivo") != null ) out.print(request.getAttribute("FacArchivo")); %>'>

  <% if(request.getAttribute("radTabla") != null ) out.print(request.getAttribute("radTabla")); %>
  <p>
  <table align=center border=0 cellpadding=0 cellspacing=0>
   <tr>
     <td><A href="javascript:Enviar();" border=0><img src = "/gifs/EnlaceMig/gbo25220.gif" border="0" alt="Border"></a></td>
   </tr>
  </table>
</FORM>

</BODY>
</HTML>