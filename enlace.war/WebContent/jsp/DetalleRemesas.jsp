<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@ page import="mx.altec.enlace.bita.BitaConstants"%>
<jsp:include page="/jsp/prepago/headerPrepago.jsp" />
<script language="javascript">

var mensaje="<%= request.getAttribute("mensaje") %>";

 if(mensaje.length>0 && mensaje!="null")
 {
 cuadroDialogo(mensaje, 1);
 }

function validar()
{
	var f=document.detalleTarjetas;
	var contador=0;
	for (i=0;i<f.elements.length;i++){
		if (f.elements[i].type=="radio"){
			if (f.elements[i].checked==true){
				contador=1;
			}
		}
	}
	if (contador==1){
		return true;
	}else{
		cuadroDialogo("Usted no ha seleccionado una tarjeta",1);
		return false;
	}
	return true;
}
function exportar() {
	var f=document.detalleTarjetas;
	f.operacion.value="6";
	f.submit();
}
function cancelar(){
	if(validar()){
		cuadroDialogo("&#191;Desea cancelar la tarjeta?",2);
	}
}
function continua()
{
	if(respuesta==1) {
		document.detalleTarjetas.reiniciaConta.value="S";
		document.detalleTarjetas.operacion.value="7";
		document.detalleTarjetas.submit();
	}
}
function siguiente()
{

		document.detalleTarjetas.operacion.value="9";
		document.detalleTarjetas.submit();

}
function regresar(){

		document.detalleTarjetas.operacion.value="8";
		document.detalleTarjetas.submit();

}

function imprimir() {
	scrImpresion();
}
</script>
<form action="${ctx}AdmonRemesasServlet" method="get"
	name="detalleTarjetas"><input type="hidden" name="operacion"
	value="1"> <%
		LMXE lmxe = (LMXE) request.getAttribute("TarjetasBean");
		if (lmxe != null && lmxe.getListaTarjetas() != null && lmxe.getListaTarjetas().size() > 0) {
			int numTarjetas = lmxe.getListaTarjetas().size();
	%>
<table cellspacing="0" cellpadding="0" border="0" width="760">
	<input type="hidden" name="numCtroDist"
		value="<%= lmxe.getCtroDistribucion()%>">
	<input type="hidden" name="numRemesa"
		value="<%= lmxe.getNumeroRemesa()%>">
	<input type="hidden" name="numRegistros"
		value="<%= lmxe.getNumRegistros()%>">
	<input type="hidden" name="numRegistrosAnt"
		value="<%= lmxe.getNumRegistrosAnt()%>">
	<input type="hidden" name="folioRemesa"
		value="<%= lmxe.getNumeroRemesa()%>|<%= lmxe.getCtroDistribucion()%>">
	<input type="hidden" name="folioRemesa2"
		value="<%= lmxe.getNumRemesa2()%>">
	<input type="hidden" name="fecha2" value="<%= lmxe.getFechaIniicio()%>">
	<input type="hidden" name="fecha1" value="<%= lmxe.getFechaFin()%>">
	<input type="hidden" name="reiniciaConta">

	<input type="hidden" name="totalTarjetas"
		value="<%= lmxe.getTotalTarjetas()%>">


	<input type="hidden" name="tipoFecha" value="<%= lmxe.getTipoFecha()%>">
	<% if(lmxe.isExistenMasDatos())
		{ %>

	<input type="hidden" name="numTarjetaPag"
		value="<%= lmxe.getTarjeta()%>">
	<%} %>
	<tr>
		<td align="center" class="tabmovtex">Total de tarjetas: <%=lmxe.getTotalTarjetas()%>
		</td>
	</tr>
	<tr>
		<td align="center">
		<DIV
			STYLE="overflow: auto; height: 250; widht: 400; border-left: 1px gray solid; border-bottom: 1px gray solid; padding: 0px; margin: 0px">
		<table width="500" border="0" cellspacing="1" cellpadding="2"
			class="tabfonbla">
			<tr>
				<td align="center" width="50" class="tittabdat"></td>
				<td align="center" width="275" class="tittabdat">N&uacute;mero
				Tarjeta</td>
				<td align="center" width="275" class="tittabdat">Estatus</td>
			</tr>
			<%
					int tamano = lmxe.getListaTarjetas().size();

					Iterator iterator = lmxe.getListaTarjetas().iterator();

					boolean claro = false;
					int i=0;
					while (iterator.hasNext()) {

						TarjetasBean tarjeta = (TarjetasBean) iterator.next();
						String sClass = claro ? "textabdatcla" : "textabdatobs";
					%>
			<tr>
				<td align="center" width="60" class="<%=sClass%>">
				<% if(tarjeta.getEstadoTarjeta().equals("RECIBIDA")){	%> <input
					type="radio" name="numTarjeta" value="<%= tarjeta.getTarjeta()%>">

				<%}%>
				</td>
				<td class="<%=sClass%>"><%= tarjeta.getTarjeta()%></td>
				<td class="<%=sClass%>"><%= tarjeta.getEstadoTarjeta() %></td>
			</tr>
			<%
						claro = !claro;
						i++;
					}
					%>

		</table>
		</DIV>
		</td>
	</tr>
	<tr>
		<td align="center" class="tabmovtex"><%=lmxe.getNumRegistrosAnt() %>
		- <%=lmxe.getNumRegistros() %> <% if(lmxe.isExistenMasDatos())
					{ %> <a href="javascript:siguiente();">Siguientes registros ></a>&nbsp;
		<%} %>
		</td>
	</tr>
	<tr>
		<td>
		<table height="40" cellspacing="0" cellpadding="0" border="0"
			width="760">
			<tr>
				<td></td>
			</tr>
			<tr>
				<td align="center" class="texfootpagneg">
				<table height="40" cellspacing="0" cellpadding="0" border="0"
					width="168">
					<tr>
						<td><a href="javascript:cancelar();"><img
							src="/gifs/EnlaceMig/gbo25190.gif" border="0" alt="Cancelar"
							width="80" height="22" align="top"> </a></td>
						<td><a href="javascript:exportar();"><img
							src="/gifs/EnlaceMig/gbo25230.gif" border=0 alt="Exportar"
							width="80" height="22" /></a></td>
						<td><A href="javascript:regresar();" border=0><img
							src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar"
							width="77" height="22"></a></td>
					</tr>
				</table>
				</td>
			</tr>

		</table>
		</td>
	</tr>
</table>
<%
			}else{
		%>
<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
		<table cellspacing="2" cellpadding="3" border="0" width="369"
			class="tabfonbla">
			<tr>
				<td align="left" class="tittabdat">Consulta de tarjetas</td>
			</tr>
			<tr>
				<td align="center" class="textabdatobs"><br />
				No se encontraron registros en la b&uacute;squeda<br />
				<br />
				</td>
			</tr>
		</table>
		<br />
		<A href="javascript:history.back();" border=0><img
			src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77"
			height="22"></a></td>
	</tr>
</table>
<%	} %>
</form>
<jsp:include page="/jsp/prepago/footerPrepago.jsp" />