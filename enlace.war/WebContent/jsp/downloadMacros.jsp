<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.bo.BaseResource"%><%--
Nombre: ChesError
@author Rafael Martinez Montiel
Muestra una pagina de error sin centrar el contenido
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<%
BaseResource baseResource = (BaseResource)session.getAttribute( "session");
%>
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************/

// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%= baseResource.getStrMenu() %>
    <%= encabezado %>

<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
<table align="center" cellspacing="5" cellpadding="5">
    <tr>
        <td>
            <table align="center" cellpadding="5" cellspacing="3">
                <tr>
                    <td class="tabfonazu" align="center"><span class="tittabdat">Pago de N&oacute;mina</span></td>
                    <td rowspan="2" valign="center">
                        <a href="javascript:FrameAyuda('s35002h');">
                            <img src="/gifs/EnlaceMig/gbo25710.gif" width="175" height="22" border="0">
                        </a></td>
                </tr>
                <tr>
                    <td class="textabdatcla" align="center"><span class="tabmovtex"><a href="/EnlaceMig/GFSSNOM.xls">N&oacute;mina Santander.xls</a></span></td>
                </tr>
                <tr>
                    <td class="tabfonazu" align="center">

                            <span class="tittabdat">Registro de Cheques de Seguridad</span>
                        
                    </td>
                    <td rowspan="2" valign="center">
                        <a href="javascript:FrameAyuda('s35003h');">
                            <img src="/gifs/EnlaceMig/gbo25700.gif" width="175" height="22" border="0">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="textabdatcla"  align="center"><span class="tabmovtex"><a href="/EnlaceMig/GFSSCHSEG.xls">Chequera Seguridad Santander.xls</a></span></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <a href="javascript:FrameAyuda('s35001h');">
                <img src="/gifs/EnlaceMig/gbo25720.gif" width="175" height="22" border="0">
            </a>
        </td>
    </tr>
</table>


</body>
</html>
