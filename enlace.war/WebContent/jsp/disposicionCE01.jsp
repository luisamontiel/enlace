<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>

<%@page import="mx.altec.enlace.utilerias.Global"%>
<%-- 23/07/2002 Se sustituyeron alerts por cuadroDialogo --%>
<%-- 17/09/2002 Se cambiaron de posici�n etiquetas html respecto a los scriplets --%>
<%-- 18/09/2002 Se cambi� orientaci�n de una celda. --%>
<%-- 19/09/2002 Cambios dise�o gr�fico.  Se sustituyeron par�metros por atributos. --%>
<%-- 26/09/2002 Se quitaron renglones vac�os --%>
<%-- 27/09/2002 Se eliminaron saltos de carro entre tablas --%>
<%-- 29/09/2002 Se eliminaron etiquetas sobrantes.  Correcciones dise�o. --%>
<%-- 30/09/2002 Cambios dise�o gr�fico. --%>
<%-- 04/10/2002 Mensajes JavaScript una por una y reducci�n tama�o tabla. --%>
<%-- 08/10/2002 Se agreg� una columna vac�a. --%>
<%-- 23/10/2002 Se cambiaron par�metros por atributos. --%>
<%-- 24/10/2002 Se centr� la p�gina -la impresi�n completa se hace ajustando los m�rgenes a cero en los controles de la impresora. --%>
<%-- 28/10/2002 Se cambi� el bot�n limpiar del extremo izquierdo al derecho. --%>
<%-- 01/11/2002 Regresa falso si hay m�s de dos decimales. --%>
<%-- 05/11/2002 Se inhiben caracteres no num�ricos y s�lo se permiten hasta dos decimales en el campo de importe.--%>
<%-- 06/11/2002 Se quitaron los eventos de JavaScript del teclado porque Explorer no los entiende. --%>
<%-- 07/11/2002 Se verifica que el importe s�lo contenta hasta dos posiciones decimales.--%>
<%-- 18/11/2002 Se indican los restantes en la liga. --%>
<%-- 19/11/2002 Se asigna el valor correcto de n�mero de registros (cuentas) por p�gina. --%>
<%-- 22/11/2002 Se inhabilita la escritura si incluir concepto no est� seleccionado. --%>
<%-- 02/12/2002 Ortograf�a y alineaci�n leyenda.--%>
<%-- 03/12/2002 Alineaci�n leyenda --%>
<%! String renglonesTabla, cadTotal, cadAcumulados, cadRestan, ancla, archivo, liga, menuPrincipal, newMenu; %>
<%! int total, acumulados, restan, registrosPagina; %>
<%! Global global; %>
<%! String mensajeErrorDatos; %>

<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO8859_1"><META NAME="GENERATOR" CONTENT="Oracle JDeveloper">
<META name="versionServlet" content="2.3.0">
<!-- Autor: Hugo Ruiz Zepeda -->
<TITLE>Disposici&oacute;n de cr&eacute;dito electr&oacute;nico</TITLE>
<%
   /*
   renglonesTabla=request.getParameter("tabla");
   cadTotal=request.getParameter("total");
   cadAcumulados=request.getParameter("acumulado");
   archivo=request.getParameter("archivo");
   */

   renglonesTabla=(String)request.getAttribute("tabla");
   cadTotal=(String)request.getAttribute("total");
   cadAcumulados=(String)request.getAttribute("acumulado");
   cadRestan=(String)request.getAttribute("restan");
   archivo=(String)request.getAttribute("archivo");
   liga="jsp/tasasCE01.jsp";
   menuPrincipal=(String)request.getAttribute("MenuPrincipal");
   mensajeErrorDatos=(String)request.getAttribute("mensajeErrorDatos");
   registrosPagina=Global.NUM_REGISTROS_PAGINA;

   if(menuPrincipal==null)
   {
      menuPrincipal=(String)session.getAttribute("MenuPrincipal");

      if(menuPrincipal==null)
         menuPrincipal="";
   }

   newMenu=(String)request.getAttribute("newMenu");

   if(newMenu==null)
   {
      newMenu=(String)session.getAttribute("newMenu");

      if(newMenu==null)
         newMenu="";
   }

   try
   {
    total=Integer.parseInt(cadTotal);
   }
   catch(Exception e1)
   {
    total=0;
   }

   try
   {
    acumulados=Integer.parseInt(cadAcumulados);
   }
   catch(Exception e2)
   {
    acumulados=0;
   }

   try
   {
      restan=Integer.parseInt(cadRestan);
   }
   catch(Exception e3)
   {
      restan=0;
   }

      response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
      response.setDateHeader("Expires", 0);
%>
<script language="JavaScript" type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript" type="text/javascript" src="/EnlaceMig/ConMov.js"></script>
<script language="JavaScript" type="text/javascript" src="/EnlaceMig/validate.js"></script>
<script language="JavaScript" type="text/javascript">
var envia=false;
var seleccionada="";
var campoMoneda=false;
var escribioPunto=false;

function confirmacion()
{
  var datosCuenta;
  var posicion1, posisicion2;
  var linea;
  envia=verifica();

  datosCuenta= new String(document.disposicion.datosCuenta.value);
  posicion1=datosCuenta.indexOf("|");
  posicion2=datosCuenta.lastIndexOf("|");

  if(posicion1>1 && posicion2>1)
  {
    linea=datosCuenta.substring(posicion1+1, posicion2-1);
    //alert(linea);
  }

  if(envia)
  {
    cuadroDialogo('\xBF'+"Desea efectuar la disposici"+'\xF3'+"n por $"+document.disposicion.importe.value+" de la l"+'\xED'+"nea de cr"+'\xE9'+"dito "+linea+"?", 2);
  }
} // Fin confirmacion


function continua()
{
  if(envia)
  {
    if(respuesta==1)
    {
      document.disposicion.submit();
    }
  }

  return false;
} // Fin continuaEnvio


function enviar()
{
  if(verifica())
  {
    document.disposicion.submit();
  }

}

function enviaTasas()
{
var destino;
var posicion1, posicion2;
var lineaCredito;


   if(document.disposicion.elements.length>4)
   {
      if(revisaSeleccionado())
      {
         posicion1=seleccionada.indexOf("|");
         posicion2=seleccionada.lastIndexOf("|");

         if(posicion1>0 && posicion2>0 && posicion2>posicion1)
         {
            lineaCredito=seleccionada.substring(posicion1+1, posicion2);
            //alert("Linea: "+lineaCredito);
            destino="ceVista?pantalla=1"+'\x26'+"lineaCredito="+lineaCredito;
            //alert(destino);
            window.open(destino, "Plazos", "dependent=true,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=525,height=425");
         }
         else
         {
            //alert("No se tiene l"+'\xED'+"nea de cr"+'\xE9'+"dito.");
            cuadroDialogo("No se tiene l"+'\xED'+"nea de cr"+'\xE9'+"dito.", 1);
         }
      }
      else
      {
         //alert("No ha seleccionado l"+'\xED'+"nea de cr"+'\xE9'+"dito.");
         cuadroDialogo("No ha seleccionado l"+'\xED'+"nea de cr"+'\xE9'+"dito.", 1);
      }
   }
   else
   {
      //alert("No hay l"+'\xED'+"neas de cr"+'\xE9'+"dito para efectuar consultas.");
      cuadroDialogo("No hay l"+'\xED'+"neas de cr"+'\xE9'+"dito para efectuar consultas.", 1);
   }
}

function verificaConcepto()
{
  if(!document.disposicion.incluirConcepto.checked)
  {
    document.disposicion.concepto.value="";
  }
}

function inhabilitaEscritura()
{
  if(!document.disposicion.incluirConcepto.checked)
  {
    document.disposicion.concepto.value="";
    document.disposicion.concepto.blur();
  }
}


function verifica()
{
  var cadPlazo, cadImporte, concepto;
  var plazo, importe;
  var resultado=true;
  var elementosForma;

  cadPlazo=document.disposicion.plazo.value;
  //cadImporte=Formatea_Importe(document.disposicion.importe.value);
  cadImporte=document.disposicion.importe.value;
  concepto=document.disposicion.concepto.value;
  document.disposicion.importe.value=cadImporte;
  plazo=parseInt(cadPlazo, 10);
  importe=parseFloat(cadImporte);

  if(isNaN(plazo))
  {
    //alert("El plazo debe ser un n"+'\xFA'+"mero entero y mayor que cero");
    cuadroDialogo("El plazo debe ser un n"+'\xFA'+"mero entero y mayor que cero", 1);
    document.disposicion.plazo.value="";
    //document.disposicion.plazo.focus();
    resultado=false;
    return false;
  }

  if(isNaN(importe))
  {
    //alert("El importe debe ser num"+'\xE9'+"rico y mayor que cero.");
    cuadroDialogo("El importe debe ser num"+'\xE9'+"rico y mayor que cero.", 1);
    document.disposicion.importe.value="";
    //document.disposicion.importe.focus();
    resultado=false;
    return false;
  }
  else
  {

      if(revisaPunto2()==false)
      {
         cuadroDialogo("El importe solamente puede tener hasta dos posiciones decimales.", 1);
         resultado=false;
         return false;
      }

    document.disposicion.importe.value=colocaFormatoMoneda(document.disposicion.importe.value);

    /*
    if(envia==false)
    {
      resultado=false;
      return false;
    }
    else
    {
      envia=false;
    }
    */
  }

  /*
  if(!isFloat(cadImporte))
  {
    alert("El importe debe ser num"+'\xE9'+"rico y mayor que cero.");
    document.disposicion.importe.value="";
    document.disposicion.importe.focus();
    resultado=false;
    return false;
  }
  */

  if(!isInteger(cadPlazo))
  {
    //alert("El plazo debe ser un n"+'\xFA'+"mero entero mayor que cero.");
    cuadroDialogo("El plazo debe ser un n"+'\xFA'+"mero entero mayor que cero.", 1);
    document.disposicion.plazo.value="";
    //document.disposicion.plazo.focus();
    resultado=false;
    return false;
  }


  if(plazo<1)
  {
    //alert("El plazo debe ser mayor que cero");
    cuadroDialogo("El plazo debe ser mayor que cero", 1);
    document.disposicion.plazo.value="";
    //document.disposicion.plazo.focus();
    resultado=false;
    return false;
  }

  if(importe<.01)
  {
    //alert("El importe debe ser una cantidad mayor que cero.");
    cuadroDialogo("El importe debe ser una cantidad mayor que cero.", 1);
    document.disposicion.importe.value="";
    //document.disposicion.importe.focus();
    resultado=false;
    return false;
  }



  if(document.disposicion.incluirConcepto.checked)
  {
    if(concepto.length<1)
    {
      //alert("Debe incluir el concepto.");
      cuadroDialogo("Debe incluir el concepto.", 1);
      //document.disposicion.concepto.focus();
      resultado=false;
      return false;
    }
  }
  else
  {
    document.disposicion.concepto.value="";
  } // Fin if disposicion

  elementosForma=document.disposicion.elements.length;

  //alert(elementosForma);

  if(elementosForma>4)
  {
    if(!revisaSeleccionado())
    {
      //alert("No ha seleccionado cuenta ni l"+'\xED'+"nea de cr"+'\xE9'+"dito.");
      cuadroDialogo("No ha seleccionado l"+'\xED'+"nea de cr"+'\xE9'+"dito.", 1);
      resultado=false;
      return false;
    }
  }
  else
  {
    //alert("No hay l"+'\xED'+"neas de cr"+'\xE9'+"dito para efectuar consultas.");
    cuadroDialogo("No ha seleccionado l"+'\xED'+"nea de cr"+'\xE9'+"dito.", 1);
    return false;
  }

  return resultado;
} // Fin funcion verifica


function colocaFormatoMoneda(dato)
{
	var posicionPunto;
	var cadena=dato;
	var tamano;


	if(dato.length<1)
	{
		return false;
	}

	tamano=cadena.length;
	posicionPunto=cadena.lastIndexOf(".");

	if(posicionPunto==-1)
	{
		cadena=cadena+".00";
	}
	else
	{
		if(posicionPunto==tamano-1)
		{
			cadena=cadena+"00";
		}

		if(posicionPunto==tamano-2)
		{
			cadena=cadena+"0";
		}

		if(tamano-posicionPunto>3)
		{
			cadena=dato.substring(0, posicionPunto+3);
         //alert("Debe redondear los centavos a dos posiciones.");
         cuadroDialogo("Debe redondear los centavos a dos posiciones.", 1);
         //envia=false;
		}


	} // Fin if-else posicionPunto -1


	if(cadena.charAt(0)=='.')
	{
			cadena="0"+cadena;
	}

   envia=true;
	return cadena;
} // Fin verificaFormatoMoneda


function revisaSeleccionado()
{
  var cuantos;
  var i=0;

  cuantos=document.disposicion.datosCuenta.length;

  if(document.disposicion.elements.length-4==1)
  {
      cuantos=1;

      if(document.disposicion.datosCuenta.checked==true)
      {
         seleccionada=document.disposicion.datosCuenta.value;
         //document.disposicion.datosCuenta.checked=true;
         return true;
      }
      else
      {
         return false;
      }
  }
  else
  {

      for(i=0;i<cuantos;i++)
      {
         if(document.disposicion.datosCuenta[i].checked)
         {
            seleccionada=document.disposicion.datosCuenta[i].value;
            return true;
         }
      }
   }

   //alert("Linea: "+seleccionada);
   //alert("Numero lineas con elementos: "+cuantos);

  return false;
} // Fin revisaSeleccionado


function revisaPunto2()
{
var posicion;
var cuanto;

   cuanto=document.disposicion.importe.value;

   if(cuanto.length<1)
      return false;

   posicion=cuanto.lastIndexOf(".");

   if(posicion==-1)
      return true;

   if(cuanto.length-1-posicion>2)
      return false;

   return true;
}


   function entraCampoMoneda()
   {
      campoMoneda=true;
   }

   function saleCampoMoneda()
   {
      campoMoneda=false;
   }

   function revisaPunto(car)
   {
      var posicion;

      posicion=document.disposicion.importe.value.lastIndexOf(".");

      if(posicion==-1)
      {
         escribioPunto=false;
      }

   }


   function dosDecimales(car)
   {
      var caracter;
      var cantidad;
      var posicion;

      if(campoMoneda)
      {

         caracter=String.fromCharCode(car.which);

         if(caracter=='\x08' || caracter=='\x0D')
         {
            cantidad=document.disposicion.importe.value;
            posicion=cantidad.lastIndexOf(".");

            if(posicion==-1)
               escribioPunto=false;

            return true;
         }

         if(caracter==='\x2E')
         {
            if(escribioPunto)
            {
               return false;
            }
            else
            {
               escribioPunto=true;
               return true;
            }
         }

         if(caracter>='0' && caracter<='9')
         {
            if(escribioPunto)
            {
               cantidad=document.disposicion.importe.value;
               posicion=cantidad.lastIndexOf(".");

               if(cantidad.length-posicion>2)
               {
                  return false;
               }
               else
               {
                  return true;
               }
            }
            else
            {
               return true;
            }
         }
         else
         {
            return false;
         }


      }


      return true;
   }


</script>
<script language="JavaScript" type="text/javascript">
function MM_preloadImages() { //v3.0

	var d=document;

	if(d.images){

		if(!d.MM_p)

			d.MM_p=new Array();

		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}

	}

}

function MM_swapImgRestore() { //v3.0

	var i,x,a=document.MM_sr;

	for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v3.0

	var p,i,x;

	if(!d) d=document;

	if((p=n.indexOf("?"))>0&&parent.frames.length) {

	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}


function MM_swapImage() { //v3.0

var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}
 	<%= newMenu %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<%
  out.print("<!--");
  out.print('\u0048');
  out.print('\u0052');
  out.print('\u005A');
  out.println("-->");
%>
</head>
<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="760">
  <tr valign="top">
    <td width="571" align="center">
       <!-- MENU PRINCIPAL -->
	   <%= menuPrincipal %>
      <!-- Acaba menu principal -->
    </TD>
    <!--<TD width="189" valign="top">&nbsp;</TD> -->
  </TR>
  <TR valign="top">
   <TD>
      <!--Inicia encabezado -->
      <%= (String)request.getAttribute("Encabezado") %>
      <!-- Fin encabezado -->
   </TD>
 </TR>
 <TR valign="top">
  <TD align="center" valign="top">
  <BR>
  <Form name="disposicion" method="post" action="ceVista?pantalla=2" onSubmit="return verifica();">
  <Table width="93%" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
   <TR>
    <TD align="center" class="tittabdat"><BR></TD>
    <TD align="center" class="tittabdat">Cuenta</TD>
    <TD align="center" class="tittabdat">N&uacute;mero<BR>de l&iacute;nea</TD>
    <TD align="center" class="tittabdat">Descripci&oacute;n</TD>
    <TD align="center" class="tittabdat">Fecha de<BR>vencimiento</TD>
    <TD align="center" class="tittabdat">Cr&eacute;dito<BR>autorizado</TD>
    <TD align="center" class="tittabdat">Adeudo<BR>total</TD>
    <TD align="center" class="tittabdat">Disponible</TD>
   </TR>
    <%= renglonesTabla %>
  </Table>

   <BR>



  <Table   width="100%" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
   <TR>
      <TD width="3%">
         <BR>
      </TD>
      <TD align="justify" class="textabref">
            Seleccione la l&iacute;nea de cr&eacute;dito
      </TD>
   </TR>
  </Table>

   <BR><BR>
   <Center>

   <% if(mensajeErrorDatos!=null && mensajeErrorDatos.length()>0)
      {
   %>
         <Table width="300" class="tabfonbla">
            <TR>
               <td align="center" class="tabtexcal2">
                  <B>
   <%
                  out.println(mensajeErrorDatos);
   %>
                  </B>
               </TD>
            </TR>
         </Table>
   <% } %>

  <Table width="475" border="0" cellspacing="0" cellpadding="0">
   <TR>
      <TD align="center" class="tittabdat" colspan="3">
         Proporcione los datos para la disposici&oacute;n
      </TD>
   </TR>
   <TR>
    <TD width="45%" class="textabdatcla">
     &nbsp;Importe:&nbsp;$<Input name="importe" type="text" size="15" maxlength="50" value="">
    </TD>
    <TD width="20%" class="textabdatcla">
      <BR>
    </TD>
    <TD width="35%" align="right" class="textabdatcla">
         Plazo:&nbsp;<Input name="plazo" type="text" size="10" maxlength="15" value="">&nbsp; d&iacute;as&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </TD>
   </TR>
   <TR>
    <TD colspan="2" class="textabdatcla" align="right">
     <BR><Input name="incluirConcepto" type="checkbox" value="1" CHECKED onClick="verificaConcepto()">
      Incluir concepto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </TD>
    <TD class="textabdatcla">
      <BR>Concepto:&nbsp;<Input name="concepto" type="text" size="15" maxlength="100" onChange="inhabilitaEscritura()" onfocus="inhabilitaEscritura()">&nbsp;
    </TD>
   </TR>
  </Table>

   <BR><BR>


  <Table border="0" width="450" cellspacing="0" cellpadding="0">
    <TR>
      <TD>
        <A href="javascript:enviar()" border="0">
          <Img src="/gifs/EnlaceMig/gbo25390.gif" border="0" alt="Cotizar">
        </A>
      </TD>
      <TD>
        <!--<a href="" onclick="NewWindow(this.href,'Tasas','300','400','yes','center');return false" onfocus="this.blur()">-->
        <A href="" onClick="enviaTasas();return false;" border="0">
        <Img src="/gifs/EnlaceMig/gbo25225.gif" border="0" alt="Plazos">
        </A>
      </TD>
      <TD>
        <A href="" onClick="window.print();return false" border="0">
         <Img src="/gifs/EnlaceMig/gbo25240.gif" border="0" alt="Imprimir">
        </A>
      </TD>
      <TD>
        <A href=<% out.println("\""+archivo+"\""); %> border="0">
          <Img src="/gifs/EnlaceMig/gbo25230.gif" border="0" alt="Exportar">
        </A>
      </TD>
      <TD>
        <A href="" onClick="document.disposicion.reset();return false" border="0">
         <Img src="/gifs/EnlaceMig/gbo25250.gif" border="0" alt="Limpiar">
        </A>
      </TD>
    </TR>
  </Table>

   </center>

   </form>

  </TD>
 </TR>
 <TR valign="top">
   <TD align="right">
      <Table border="0" width="450" cellspacing="0" cellpadding="0" align="left">
       <TR>
         <TD align="right" class="textabref">
   <%
     int aux1;

    if(total-acumulados>0)
    {
      if(restan>=registrosPagina)
      {
         aux1=acumulados+registrosPagina;
         ancla="ceVista?pantalla=0&acumulado="+acumulados;
         out.println("<A href=\""+ancla+"\">Siguientes "+registrosPagina+" cuentas</A>");
      }
      else
      {
         aux1=acumulados+registrosPagina;
         ancla="ceVista?pantalla=0&acumulado="+acumulados;ancla="ceVista?pantalla=0&acumulado="+acumulados;
         out.println("<A href=\""+ancla+"\">Siguientes "+restan+" cuentas</A>");
      }
    }
  %>

    </TD>
   </TR>
  <TR>
   <TD align="right" class="textabref">
  <%
    if(acumulados-registrosPagina>0)
    {
      aux1=acumulados-2*registrosPagina;
      ancla="ceVista?pantalla=0&acumulado="+aux1;
      out.println("<A href=\""+ancla+"\">Anteriores "+registrosPagina+" cuentas</A>");
    }
  %>
      <BR>
   </TD>
  </TR>
 </Table>

  </TD>
 </TR>
</TABLE>


</BODY>
</HTML>
