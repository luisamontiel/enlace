<%

  String nombreArchivoImp="";
  String nombreArchivoDetalle="";
  String registroRechazados="";
  String registroAceptados="";
  String totalRegistros="";
  String strMensaje="";
  String numRegpage="";
  String usuarioOpcion="";
  String tipoConsulta="";
  String Fecha1_01="";
  String Fecha1_02="";
  String folioReg="";
  String fcuenta="";
  String tipoEstatus="";
  String tipoCuenta="";
  String tipoOperAB="";
  String tipoOper="";
  String etiqueta="";




  if(request.getAttribute("nombreArchivoImp")!=null)
    nombreArchivoImp=(String)request.getAttribute("nombreArchivoImp");
  if(request.getAttribute("nombreArchivoDetalle")!=null)
    nombreArchivoDetalle=(String)request.getAttribute("nombreArchivoDetalle");
  if(request.getAttribute("totalRegistros")!=null)
    totalRegistros=(String)request.getAttribute("totalRegistros");
  if(request.getAttribute("numRegpage")!=null)
    numRegpage=(String)request.getAttribute("numRegpage");
  if(request.getAttribute("registroAceptados")!=null)
    registroAceptados=(String)request.getAttribute("registroAceptados");

  if(request.getAttribute("registroRechazados")!=null)
    registroRechazados=(String)request.getAttribute("registroRechazados");

  if(request.getAttribute("tipoOper")!=null)
    tipoOper=(String)request.getAttribute("tipoOper");

				if(tipoOper.trim().equals("A"))
					 etiqueta="Autorizados";
			    else if (tipoOper.trim().equals("R"))
					etiqueta="Cancelados";

  if(request.getAttribute("strMensaje")!=null)
    strMensaje=(String)request.getAttribute("strMensaje");


	usuarioOpcion=(String)request.getSession().getAttribute("usuarioOpcion");
if (usuarioOpcion == null || usuarioOpcion.length () == 0)
       usuarioOpcion = " ";


	tipoConsulta=(String)request.getSession().getAttribute("Registro");
if (tipoConsulta == null || tipoConsulta.length () == 0)
       tipoConsulta = " ";


	Fecha1_01=(String)request.getSession().getAttribute("Fecha1_01");
if (Fecha1_01 == null || Fecha1_01.length () == 0)
       Fecha1_01 = " ";

	Fecha1_02=(String)request.getSession().getAttribute("Fecha1_02");
if (Fecha1_02 == null || Fecha1_02.length () == 0)
       Fecha1_02 = " ";

	folioReg=(String)request.getSession().getAttribute("Folio_Registro");
if (folioReg == null || folioReg.length () == 0)
       folioReg = " ";

	fcuenta=(String)request.getSession().getAttribute("FCuenta");
if (fcuenta == null || fcuenta.length () == 0)
       fcuenta = " ";


	tipoEstatus=(String)request.getSession().getAttribute("TipoEstatus");
if (tipoEstatus == null || tipoEstatus.length () == 0)
   tipoEstatus = " ";

	tipoCuenta=(String)request.getSession().getAttribute("tipoCuenta");
if (tipoCuenta == null || tipoCuenta.length () == 0)
       tipoCuenta = " ";

	tipoOperAB=(String)request.getSession().getAttribute("tipoOperAB");
if (tipoOperAB == null || tipoOperAB.length () == 0)
       tipoOperAB = " ";


 %>

<html>
<head>
	<title>CUENTAS -  Autorizaci&oacute;n y Cancelaci&oacute;n</TITLE>

<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">


<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<!-- Stilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
.componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>


<script language="JavaScript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function js_regresar()
{
    document.ABCuentas.Modulo.value="3";
    document.ABCuentas.submit();
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function js_verDetalle()
{
   var forma=document.ABCuentas;
   var parametros="";

   parametros+="totalRegistros="+forma.totalRegistros.value;
   parametros+="&nombreArchivoImp=<%=nombreArchivoImp%>";
   parametros+="&nombreArchivoDetalle="+forma.nombreArchivoDetalle.value;
   parametros+="&registroAceptados=<%=registroAceptados%>";
   parametros+="&registroRechazados=<%=registroRechazados%>";
   parametros+="&numRegpage=<%=numRegpage%>";
   parametros+="&tipoOper=<%=tipoOper%>";


   ventanaInfo=window.open('enlaceMig/jsp/Cont_ConsErrores.jsp?'+parametros,'trainerWindow','width=720,height=400,toolbar=no,scrollbars=yes');
 }
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/

<%= request.getAttribute("newMenu") %>
</script>
</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <%= request.getAttribute("MenuPrincipal")%>
    </TD>
  </TR>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>

<FORM  NAME="ABCuentas" method=post action="ContCtas_AL">
<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <td align=center>
	<table width="640" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	 <tr>

	  <td class="tittabdat" align="center"> &nbsp; Total de Registros Enviados </td>
	  <td class="tittabdat" align="center"> &nbsp; Total de Registros <%=etiqueta%></td>
	  <td class="tittabdat" align="center"> &nbsp; Total de Registros no <%=etiqueta%></td>
	 </tr>
	 <tr>
	  <td class="textabdatobs" nowrap align="center"><%=numRegpage%></td>
	  <td class="textabdatobs" nowrap align="center"><%=registroAceptados%></td>
	  <td class="textabdatobs" nowrap align="center"><%=registroRechazados%></td>

	 </tr>
	 <tr>
	  <td colspan=4 class="textabdatcla">
	    <%=strMensaje%>
	  </td>
	 </tr>
	</table>
	<br>
	<table align=center border=0 cellspacing=0 cellpadding=0>
	 <tr>
	  <td><A href = "javascript:js_regresar();" border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt=Regresar></a></td>
	  <%
  	    if(!registroAceptados.equals(numRegpage))
	    {
	  %>
	  <td><A href = "javascript:js_verDetalle();" border = 0><img src = "/gifs/EnlaceMig/gbo25248.gif" border=0 alt="Detalle"></a></td>
	  <%
	   }
	  %>
	 </tr>
	</table>
   </td>
  </tr>
</table>


 <input type=hidden name=Modulo>
 <input type=hidden name=nombreArchivoDetalle value="<%=nombreArchivoDetalle%>">
 <input type=hidden name=totalRegistros value="<%=totalRegistros%>">
 <input type=hidden name=usuarioOpcion value="<%=usuarioOpcion%>">
 <input type=hidden name=Registro value="<%=tipoConsulta%>">
 <input type=hidden name=Fecha1_01 value="<%=Fecha1_01%>">
 <input type=hidden name=Fecha1_02 value="<%=Fecha1_02%>">
 <input type=hidden name=Folio_Registro value="<%=folioReg%>">
 <input type=hidden name=FCuenta value="<%=fcuenta%>">
 <input type=hidden name=TipoEstatus value="<%=tipoEstatus%>">
 <input type=hidden name=tipoCuenta value="<%=tipoCuenta%>">
  <input type=hidden name=tipoOperAB value="<%=tipoOperAB%>">


 </form>

</body>
</HTML>