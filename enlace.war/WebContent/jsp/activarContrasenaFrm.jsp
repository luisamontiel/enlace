
<%@ page import="mx.altec.enlace.admonContrasena.vo.UtilVO" %>

<%
	java.util.Date dt = new java.util.Date();

	String mes = "";
	String strDia = "";
	String codCliente = (String) request.getSession().getAttribute("codCliente");
	String nombre = (String) request.getSession().getAttribute("usuarioEnlace");
	String mensaje = (String) request.getAttribute("Mensaje");

	if (codCliente == null || codCliente.equals("")) {
		mensaje = "cuadroDialogo(\"Falta c&oacute;digo de Cliente. Autentifiquese nuevamente antes de continuar.\", 3);";
	}

	switch (dt.getMonth() + 1) {
	case 1:
		mes = "Enero";
		break;
	case 2:
		mes = "Febrero";
		break;
	case 3:
		mes = "Marzo";
		break;
	case 4:
		mes = "Abril";
		break;
	case 5:
		mes = "Mayo";
		break;
	case 6:
		mes = "Junio";
		break;
	case 7:
		mes = "Julio";
		break;
	case 8:
		mes = "Agosto";
		break;
	case 9:
		mes = "Septiembre";
		break;
	case 10:
		mes = "Octubre";
		break;
	case 11:
		mes = "Noviembre";
		break;
	case 12:
		mes = "Diciembre";
		break;
	default:
		mes = "Enero";
	}

	switch (dt.getDay()) {
	case 0:
		strDia = "Domingo";
		break;
	case 1:
		strDia = "Lunes";
		break;
	case 2:
		strDia = "Martes";
		break;
	case 3:
		strDia = "Miercoles";
		break;
	case 4:
		strDia = "Jueves";
		break;
	case 5:
		strDia = "Viernes";
		break;
	case 6:
		strDia = "Sabado";
		break;
	}

	int anio = dt.getYear() + 1900;
	String mifecha = strDia + " " + dt.getDate() + " de " + mes
	+ " del " + anio;

	String minutos_ = "" + dt.getMinutes();
	if (minutos_.trim().length() == 1) {
		minutos_ = "0" + minutos_;
	}
	String mihora = dt.getHours() + ":" + minutos_;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Bienvenido a Enlace Internet</title>
		<meta http-equiv="Content-Type"
			content="text/html; charset=windows-1252">
		<meta content="s26050" name="Codigo de Pantalla">
		<meta http-equiv="Expires" content="1">
		<meta http-equiv="pragma" content="no-cache">
		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

		<meta content="MSHTML 6.00.2900.2180" name="GENERATOR">
		<script language="JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
		<script language="JavaScript" SRC="/EnlaceMig/Convertidor.js"></script>
		<script type="text/javascript" language="JavaScript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function validaFormato (str){
	var TemplateF=/^[a-z\d]{0,20}$/i;  //dominio de a-z, A-Z, 0-9
	return TemplateF.test(str);  //Compara "pwd" con el formato "Template"
}

/**
* Agrega ceros a una cadena al principio de la misma hasta alcanzar la longitud <code>longitud</code>
* @param origen - cadena original
* @param logintud - longitud deseada
* @return cadena inicial con ceros al principio y con longitud minima de <code>longitud</code>
*/
function completaConCeros(origen, longitud){
	while(origen.length < longitud){
		origen = '0' + origen;
	}
	return origen;
}

/**
* Valida que el folio cumpla con cierto patron.
* @return
*	<code>false</code> en caso de que falle el patron o est� vacio. <code>true</code> en caso contrario
*/
function validaFolio(folio)
{
	var result = false;
	if (!validaFormato (folio)){
		cuadroDialogo ("Folio de Solicitud Inv&aacute;lido.", 1);
	}
	// regresa false si el campo esta en blanco
	if (folio == ""){
		cuadroDialogo ("\n Favor de escribir su Folio de Solicitud. ", 1);
	}else{
		result = true
	}
	return result;
}

function completeUsrID (user)
{
	//user = document.registro.usuarioTxt.value
	var result

	if (user.length > 0 && user.length < 8){
		//Modificaci�n HGCV Prueba Usuario
		for (i = user.length; i < 7; i++)
	    	user = "0" + user
	}
	if(user.length==8)
		user=cliente_8To7(user);
	else
		user=user;
	return user;
}

function validaFrm()
{
	var codCliente = document.getElementById("codCliente");

	if(codCliente.value == '' || codCliente.value == 'null'){
		<%=mensaje%>
		return;
	}

	var result = validPwd(document.frm.clave.value);

	if (result){
		codCliente.value = completeUsrID(codCliente.value);
		codCliente.form.submit();
	}
}


	function validaFormato (pwd)
		{
			var TemplateF=/^[a-z\d]{0,20}$/i;  //dominio de a-z, A-Z, 0-9
			return TemplateF.test(pwd);  //Compara "pwd" con el formato "Template"
		}

	function validPwd (pwd){
		if (!validaFormato (pwd)){
		    document.frm.clave.focus();
			document.frm.clave.value="";
			cuadroDialogo ("Contrase\361a inv&aacute;lida.", 1)
		    return false
		}
		if (pwd == ""){
			cuadroDialogo ("\n Favor de teclear su Contrase\361a. ", 1)
 	        return false
		}
		else{
			if (pwd.length != 4){
				return true
			}else
				result = true
		}
		return result
	}

</script>

<%
	String folio = request.getParameter("folio");
	// <vswf:meg cambio de NASApp por Enlace 08122008>
	String url = "/Enlace/enlaceMig/LoginServlet?opcion=2&accion=12&folioSolicitud=" + folio;
%>


	</head>
	<body bgColor="#ffffff" leftMargin="0"
		background="/gifs/EnlaceMig/gfo25010.gif" topMargin="0"
		onload="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gba25020.gif')"
		marginwidth="0" marginheight="0">
		<table cellSpacing="0" cellPadding="0" width="600" border="0">
			<tbody>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<table class="tabfonbla" cellSpacing="0" cellPadding="0"
										width="596" border="0">
										<tbody>
											<tr vAlign="top">
												<td rowSpan="2">
													<img height="41" src="/gifs/EnlaceMig/glo25010.gif"
														width="237" alt="">
												</td>
												<td rowSpan="2">
													<a
														onmouseover="MM_swapImage('contactanos','','/gifs/EnlaceMig/gbo25111.gif',1)"
														onmouseout="MM_swapImgRestore()" href="javascript:;"><img
															height="33" src="/gifs/EnlaceMig/gbo25110.gif" width="30"
															border="0" name="contactanos" alt=""> </a>
												</td>
												<td rowSpan="2">
													<img height="20" alt="Cont�ctenos"
														src="/gifs/EnlaceMig/gbo25120.gif" width="59">
												</td>
												<td rowSpan="2">
													<a
														onmouseover="MM_swapImage('atencion','','/gifs/EnlaceMig/gbo25131.gif',1)"
														onmouseout="MM_swapImgRestore()" href="javascript:;"><img
															height="33" src="/gifs/EnlaceMig/gbo25130.gif" width="36"
															border="0" name="atencion" alt=""> </a>
												</td>
												<td rowSpan="2">
													<img height="20" alt="Atenci&oacute;n telef&oacute;nica"
														src="/gifs/EnlaceMig/gbo25140.gif" width="90">
												</td>
												<td rowSpan="2">
													<a
														onmouseover="MM_swapImage('centro','','/gifs/EnlaceMig/gbo25151.gif',1)"
														onmouseout="MM_swapImgRestore()" href="javascript:;"><img
															height="33" src="/gifs/EnlaceMig/gbo25150.gif" width="33"
															border="0" name="centro" alt=""> </a>
												</td>
												<td rowSpan="2">
													<img height="20" alt="Centro de mensajes"
														src="/gifs/EnlaceMig/gbo25160.gif" width="93">
												</td>
												<td width="18" bgColor="#de0000">
													<img height="17" src="/gifs/EnlaceMig/gau25010.gif"
														width="18" alt="">
												</td>
											</tr>
											<tr vAlign="top">
												<td>
													<img height="24" src="/gifs/EnlaceMig/gau25010.gif"
														width="18" alt="">
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="100%"
										bgColor="#ffffff" border="0">
										<tbody>
											<tr>
												<td bgColor="#de0000">
													<img height="17" src="/gifs/EnlaceMig/gau25010.gif"
														width="35" alt="">
												</td>
												<td align="middle" rowSpan="2">
													<img height="41" alt="" src="/gifs/EnlaceMig/glo25020.gif"
														width="57">
												</td>
												<td width="100%" bgColor="#de0000">
													<img height="17" src="/gifs/EnlaceMig/gau25010.gif"
														width="5" alt="">
												</td>
											</tr>
											<tr>
												<td>
													<img height="24" src="/gifs/EnlaceMig/gau25010.gif"
														width="35" alt="">
												</td>
												<td>
													<img height="24" src="/gifs/EnlaceMig/gau25010.gif"
														width="5" alt="">
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="top" width="100%">

						<table cellSpacing="0" cellPadding="0" border="0" width="100%">
							<tbody>
								<tr>
									<td class="titpag" vAlign="bottom">
										Administrador de Contrase&ntilde;as
									</td>
									<td class="tabfonbla" align="right">
										<img height="33" alt="Sitio seguro"
											src="/gifs/EnlaceMig/gba25010.gif" width="126">
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top"
						style="background-image: url(/gifs/EnlaceMig/gfo25020.gif); background-repeat: repeat-x;">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td vAlign="top" width="100%">

						<table cellSpacing="0" cellPadding="0" border="0" width="100%">
							<tbody>

								<tr>
									<td class="texencrut">
										&nbsp;
										<img src="/gifs/EnlaceMig/gic25030.gif" alt="">
										Administrador de Contrase&ntilde;as &gt; Activar
										Contrase&ntilde;a
									</td>
									<td align="right">
										<table>
											<tr>
												<td class="texencfec" vAlign="top" noWrap align="right"
													height="12">
													<%=mifecha%>
													<br>
													<%=mihora%>
												</td>
												<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
												<td align="right"><a href="/Enlace/enlaceMig/logout?origen=1" onMouseOut="MM_swapImgRestore()"
													onMouseOver="MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)">
													<img src="/gifs/EnlaceMig/gbo25180.gif" width="44" height="49" name="finsesion" border="0"></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<form name="frm" id="frm"
							action="<%=url%>"
							method="post">
							<table cellSpacing="0" cellPadding="0" width="282" border="0"
								align="center">
								<tbody>
									<tr>
										<td class="tittabdat" nowrap="nowrap" style="height: 20px;">&nbsp;Activaci&oacute;n de contrase&ntilde;a</td>
									</tr>
									<tr>
										<td>
											<table class="textabdatcla" cellSpacing="0" cellPadding="0"
												width="282" border="0">
												<tbody>
													<tr>
														<td>
															<table cellSpacing="10" cellPadding="0" width="282"
																border="0">
																<tbody>
																	<tr>
																		<td class="tabmovtex11">
																			C&oacute;digo de Cliente:
																		</td>
																		<td class="tabmovtex11">
																			<%=UtilVO.convierteUsr7a8(codCliente)%>
																			<input type="hidden" name="codCliente"
																				id="codCliente" value="<%=UtilVO.convierteUsr7a8(codCliente)%>">
																		</td>
																	</tr>
																	<tr>
																		<td class="tabmovtex11">
																			Nombre:
																		</td>
																		<td class="tabmovtex11">
																			<%=nombre%>
																		</td>
																	</tr>
																	<tr>
																		<td class="tabmovtex11" noWrap>
																			Folio de Contrase&ntilde;a:
																		</td>
																		<td class="tabmovtex11">
																			<strong><%=folio%></strong>
																		</td>
																	</tr>
																	<tr>
																		<td class="tabmovtex11" noWrap>
																			Contrase&ntilde;a:
																		</td>
																		<td class="tabmovtex11">
																			<INPUT TYPE="password" NAME="clave" SIZE="20" MAXLENGTH="20">
																		</td>
																	</tr>

																	<tr>
																		<td class="tabmovtex11" align="middle" colspan="2">
																			<table cellSpacing="0" cellPadding="0" border="0">
																				<tbody>
																					<tr>
																						<td width="76">
																							<a href="javascript:validaFrm();"><img
																									height="22" alt="Activar"
																									src="/gifs/EnlaceMig/activar.gif"
																									border="0"> </a>
																						</td>

																						<td>
																						<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
																							<a
																								href="/Enlace/enlaceMig/LoginServlet?opcion=2&accion=4"><img
																									height="22" alt="Cancelar"
																									src="/gifs/EnlaceMig/cancelar.gif"
																									border="0"> </a>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td
											style="background-image: url(/gifs/EnlaceMig/gfo25020.gif); background-repeat: repeat-x; background-position: left top; height: 12px">
											<img height="12" src="/gifs/EnlaceMig/gau25010.gif" width="5"
												alt="">
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</td>
				</tr>
			</tbody>
		</table>
<script type="text/javascript">
<%
if(mensaje!=null && !mensaje.equals("")){
%>
<%=mensaje%>
<%
}
%>

</script>
	</body>
</html>
