<!-- Bit�cora de Cambios
 * LORENA MERCADO REYES (LMR) PRAXIS
 *          FOLIO INC. Q1087  (18/02/2004) - Modificacion para incluir el subtotal por proveedor y fecha de  vencimiento
 *          FOLIO INC. Q2959  (20/02/2004) - Incluir un parametro y el mensaje para exportar cuando son mas de 100 registros
-->

<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.math.BigDecimal" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<html>
<head>
	<title>Confirming Consulta y Cancelacion de Pagos </title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<SCRIPT language = "JavaScript">
	// ---------------------------------------------------

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	<%= request.getAttribute("newMenu") %>

	// ---------------------------------------------------
	var claveEstatus = new Array(<%= ((request.getAttribute("claveEstatus") != null)?request.getAttribute("claveEstatus"):"'0'") %>);

	// ---
	function inicia(){
		<%
		String parMensaje = (String)request.getAttribute("Mensaje"); if(parMensaje == null) parMensaje = "";
		boolean hayMensaje = !parMensaje.equals("");
		%>
		var hayMensaje = <%= hayMensaje %>;
		var mensaje = "<%= parMensaje %>";
		if(hayMensaje) cuadroDialogo(mensaje.substring(1), parseInt(mensaje.substring(0,1)));
	}

	// ---
	function cancela(){
		var lista = "";
		//debugger;
		var txt="";
		try{
			for(x=0;x<document.frmcoConsultadocs.cancel.length-2;x++)
				if(document.frmcoConsultadocs.cancel[x].type == "checkbox" && document.frmcoConsultadocs.cancel[x].checked) lista += document.frmcoConsultadocs.cancel[x].value + "\n";
			if(lista == "") {cuadroDialogo("No se ha seleccionado ninguna cuenta,<BR>verifique por favor",3); return;}
			var noDoc = "";
			var noNota = "";
			var noFactura = "";
			var rel = false;
			for(x=0;x<document.frmcoConsultadocs.cancel.length-2;x++)
				{
				if(document.frmcoConsultadocs.cancel[x].checked)
					{
					rel = false;
					noDoc = document.frmcoConsultadocs.cancel[x].value;
					noDoc = noDoc.substring(noDoc.indexOf("@")+1);
					for(y=0;y<document.frmcoConsultadocs.neteo.length-2;y++)
						{
						noNota=document.frmcoConsultadocs.neteo[y].value;
						noFactura=cambiaPipesXArrobas(noNota.substring(noNota.indexOf("|")+1));
						noNota = noNota.substring(0,noNota.indexOf("|"));
						if(noFactura == noDoc)
							{
							cuadroDialogo("El documento seleccionado est&aacute; relacionado con la nota " + noNota +
								", por lo que debe cancelarse primero &eacute;sta.",3);
							return;
							}
						}
					}
				}
			document.frmcoConsultadocs.Cancelaciones.value=lista;
			document.frmcoConsultadocs.submit();
	    }catch(err){
			txt="There was an error on this page.\n\n";
			txt+="Error description: " + err.description + "\n\n";
			txt+="Click OK to continue.\n\n";
			alert(txt);
		}
	}

	// ---
	function CheckTodos(){
		for(x=0;x<document.frmcoConsultadocs.cancel.length-2;x++)
			document.frmcoConsultadocs.cancel[x].checked = document.frmcoConsultadocs.todos.checked;
	}

	// ---
	function verifica(){
		var todos=true;
		for(x=0;x<document.frmcoConsultadocs.cancel.length-2;x++)
			if(!document.frmcoConsultadocs.cancel[x].checked) {todos=false; break;}
		document.frmcoConsultadocs.todos.checked=todos;
	}

	// ---
	function cambiaPipesXArrobas(val){
		var a, largo = val.length;
		var cadena = "";
		for(a=0;a<largo;a++) if(val.charAt(a)=='|') cadena += "@"; else cadena += val.charAt(a);
		return cadena;
	}

	// ---
	function verReferencia(registro){
		vc=window.open("../jsp/coCancelDetalle.jsp?reg="+registro,'trainerWindow','width=490,height=420,toolbar=no,scrollbars=yes');
	   	vc.focus();
	}

	function downloadFile(){
	var t_expor;
		for (var i = 0; i< document.frmcoConsultadocs.tipoExportacion.length; i++){
			if(document.frmcoConsultadocs.tipoExportacion[i].checked)
				t_expor = document.frmcoConsultadocs.tipoExportacion[i].value;
		}
		if(t_expor=="SD"){
			document.location.href = "<%= (String)request.getAttribute("ExportacionAntes") %>";
		}else if(t_expor=="CD"){
			document.location.href = "<%= (String)request.getAttribute("Exportacion") %>";
		}
		//document.frmcoConsultadocs.tipoReq.value = "3";
		//document.frmcoConsultadocs.submit();
	}

	</SCRIPT>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="inicia(); MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

	<!-- MENU PRINCIPAL -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</TABLE>
	<%= request.getAttribute("Encabezado") %>

	<br><br>

	<FORM NAME="frmcoConsultadocs" METHOD="POST" ACTION="coCancelacion" >

	<table cellpadding='2' cellspacing='1'>
	<tr><td  class = 'texenccon'><%= (String)request.getAttribute("Sel") %></td></tr>
	</table>
<!-- ---------------------------------------------------------- -->
	<br>
	<%
	//LMR Q2959 20/05/2004
	int Total = ((Integer)request.getAttribute("Total")).intValue();
	HashMap detalles = new HashMap();
	if(Total<=1000){
	//LMR Q2959 20/05/2004
	%>
	<%
	String parInfo = "";
	String linea;
	String estilo="";
	StringTokenizer parser;
	StringTokenizer parserLinea;
	Vector lineasParaOrdenar;
	double impTmp;
	String fecSub;
	String fecTmp;
	int    ctrl;
	int    posfec;
	int    numLinea = 0;
	int pos;
	NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
	String totalMXN = (String) request.getAttribute("totalMXN");
	String totalUSD = (String) request.getAttribute("totalUSD");
	if(Integer.parseInt(totalMXN)>0){ %>
	<table cellpadding='2' cellspacing='1' width = "80%">
		<tr>
			<td class='texenccon' colspan="19">OPERACIONES EN MONEDA NACIONAL: <%= (String) request.getAttribute("totalMXN") %></td>
		</tr>
		<tr>
			<td class="tittabdat">
			<input type='checkbox' name='todos' onclick='CheckTodos();'>Todos
			</td>
			<td class="tittabdat" align = "center"><B>Clave prov.</B></td>
			<td class="tittabdat" align = "center"><B>Nombre o raz&oacute;n social</B></td>
			<td class="tittabdat" align = "center"><B>Tipo docto.</B></td>
			<td class="tittabdat" align = "center"><B>No. docto.</B></td>
			<td class="tittabdat" align = "center"><B>Importe</B></td>
			<td class="tittabdat" align = "center"><B>Divisas</B></td>
			<td class="tittabdat" align = "center"><B>Fecha vto.</B></td>
			<!--td class='tittabdat'>Estatus del cobro</td-->
			<td class="tittabdat" align = "center"><B>Fecha Oper.</B></td>
<!--GOR 20110113 (Req. Confirming - 5 - Inicio): Cambiar la etiqueta en las pantallas de REFERENCIA por REFERENCIA DE OPERACI�N -->
			<td class="tittabdat" align = "center"><B>Referencia de Operaci&oacute;n</B></td>
<!--GOR 20110113 (Req. Confirming - 5 - Fin) -->
			<td class="tittabdat" align = "center"><B>Status</B></td>
			<td class="tittabdat" align = "center"><B>Observaciones</B></td>
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de REFERENCIA INTERBANCARIA por N�MERO DE REFERENCIA -->
			<td class="tittabdat" align = "center"><B>N&uacute;mero de Referencia</B></td>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de CONCEPTO INTERBANCARIO por CONCEPTO DEL PAGO -->
			<td class="tittabdat" align = "center"><B>Concepto del Pago</B></td>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->
			<td class="tittabdat" align = "center"><B>Forma de Aplicaci&oacute;n</B></td>
			<td class="tittabdat" align = "center"><B>Cuenta Cargo</B></td>
			<td class="tittabdat" align = "center"><B>Forma de Pago</B></td>
<!--GOR 20110113 (Req. Confirming - 6,7 - Inicio): Incluir un nuevo campo que indique la CLAVE DE RASTREO -->
			<td class="tittabdat" align = "center"><B>Clave de Rastreo</B></td>
<!--GOR 20110113 (Req. Confirming - 6,7 - Fin) -->
			<!-- Se agrega canal -->
			<td class="tittabdat" align = "center"><B>Canal</B></td>
		</tr>
	<%
		parInfo = (String)request.getAttribute("Info");
		estilo="textabdatobs";
		parser = new StringTokenizer(parInfo,"\n");
		// Declaracion de variables para incluir un subtotal por fecha de vencimiento (18/02/2004-LMR-Q1087)

		// Fin declaracion (LMR-Q1087)
		// -------------------
		lineasParaOrdenar = new Vector();
	    try{
			while(parser.hasMoreTokens()) lineasParaOrdenar.add(parser.nextToken());
			String nombre1 , nombre2 ;
			String num1    , num2    , cadAux;
			int    a       , b       , total = lineasParaOrdenar.size();
		// Cambio para ordenar por fecha de vencimiento bloque 1 inicio (18/02/2004-LMR-Q1087)
			Date   Fecha1  , Fecha2  ;
			DateFormat dt =DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRENCH);
		// Cambio para ordenar por fecha de vencimiento bloque 1 (18/02/2004-LMR-Q1087)
			for(a = 0; a < total-1; a++)
				for(b = a+1; b < total; b++){
					nombre1 = (String)lineasParaOrdenar.get(a); if(nombre1.startsWith("-")) continue;
		// Cambio para ordenar por fecha de vencimiento bloque 2 inicio (18/02/2004-LMR-Q1087)
					for(posfec = 0; posfec < 7; posfec++) nombre1 = nombre1.substring(nombre1.indexOf('|')+1);
					Fecha1 = dt.parse(nombre1);
	//			nombre1 = nombre1.substring(nombre1.indexOf("|")+1);
	//			nombre1 = nombre1.substring(nombre1.indexOf("|")+1);
		// Cambio para ordenar por fecha de vencimiento bloque 2 (18/02/2004-LMR-Q1087)
					nombre2 = (String)lineasParaOrdenar.get(b); if(nombre2.startsWith("-")) continue;
		// Cambio para ordenar por fecha de vencimiento bloque 3 inicio (18/02/2004-LMR-Q1087)
					for(posfec = 0; posfec < 7; posfec++) nombre2 = nombre2.substring(nombre2.indexOf('|')+1);
					Fecha2 = dt.parse(nombre2);
	//			nombre2 = nombre2.substring(nombre2.indexOf("|")+1);
	//			nombre2 = nombre2.substring(nombre2.indexOf("|")+1);
		//if(nombre1.compareTo(nombre2)>0)
		// Cambio para ordenar por fecha de vencimiento bloque 3 (18/02/2004-LMR-Q1087)
					if(Fecha1.compareTo(Fecha2)>0){
						cadAux = (String)lineasParaOrdenar.get(a);
						lineasParaOrdenar.set(a,lineasParaOrdenar.get(b));
						lineasParaOrdenar.set(b,cadAux);
					}
				}
			parInfo = "";
			for(a = 0; a < total; a++) parInfo += (String)lineasParaOrdenar.get(a) + "\n";
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("EXCEPCION EN EL JSP coCancelacionCon.jsp (1): " + e.getMessage(), EIGlobal.NivelLog.ERROR);}

		parser = new StringTokenizer(parInfo,"\n");
		// -------------------
		// Inicializa Variables (18/02/2004-LMR-Q1087)
		impTmp		=0;
		fecSub		="";
		fecTmp		="";
		ctrl    	=0;
		// Fin inicializacion (LMR-Q1087)
		try{
			while(parser.hasMoreTokens()){
				numLinea++;
		// Esto se repite para cada registro
				estilo=(estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
				linea = parser.nextToken();
				String fPagoMXN = null;
				StringTokenizer formaPago = new StringTokenizer(linea,"|");
				int fpi = 0;
				for(int fpj=0; fpj<16; fpj++){
					formaPago.nextToken();
					if(fpi==15)
						fPagoMXN= formaPago.nextToken();
					fpi++;
				}
				while((pos=linea.indexOf("| |")) != -1) linea = linea.substring(0,pos+1) + "&nbsp;" + linea.substring(pos+1);
		// Inicia bloque para la linea de subtotal (LMR-Q1087)
		// Obtiene la cadena de la fecha y si es diferente al que trae la variable temporal imprime la linea de subtotal (18/02/2004-LMR-Q1087)
		// La fecha de vencimiento esta en la posicion 7
				fecSub = linea;
				for(posfec = 0; posfec < 7; posfec++)
					fecSub = fecSub.substring(fecSub.indexOf('|')+1);
		//corta la cadena entre los caracteres |
				fecSub = fecSub.substring(0, fecSub.indexOf('|'));
		// si control es diferente de cero verifica si ha cambiado la fecha, si cambia, inicializa variables
		// e imprime la linea de subtotal
				if (ctrl != 0){
					if((!fecSub.equals(fecTmp))&&(!fecTmp.equals(""))){
	%>
						<tr><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px"  class="tittabdat">Subtotal</td><td class="tittabdat" height="20px" align=right><%=nf.format(impTmp)%></td></tr>
	<%
						ctrl   = 1;
						impTmp = 0;
						fecTmp = "";
					}
				}
				// Fin bloque para la linea de subtotal (LMR-Q1087)
				parserLinea = new StringTokenizer(linea,"|");
				linea=parserLinea.nextToken();
				if(linea.equals("-")) continue;
				if(!linea.equals(" "))
					linea = "<input type='checkbox'  name ='cancel' value=\"" + fPagoMXN + "|" + linea + "\"  onclick='verifica();'>";
				else
					linea = "&nbsp;";
	%>
				<tr><td class="<%=estilo%>"><%=linea%></td>
	<%
				pos=0;
				while(parserLinea.hasMoreTokens()){
					linea = parserLinea.nextToken();
					// Control para poner el nombre en una variable temporal y sumar el importe (18/02/2004-LMR-Q1087)
					if ((pos+1) == 5){
						linea=linea.replace('$',  ' ');
						linea.trim();
						while(linea.indexOf(',')!=-1) linea=linea.substring(0, linea.indexOf(',')) + linea.substring(linea.indexOf(',')+1);
						impTmp+=Double.valueOf((String)linea).doubleValue();
					}
					if ((pos+1) == 7)
						fecTmp   =(String) linea;
					// Termina bloque de actualizacion de variables temporales (LMR-Q1087)
					if ((pos+1) == 14){
						  if(linea.equals("1"))
							linea="MISMO DIA";
						  else
							  if(linea.equals("2"))
								linea="DIA SIGUIENTE";
							  else
								 linea="MISMO DIA";
					}
					// DAG 141009 Se agrega liga de referencia
					if((pos+1) == 9){
						if(linea.substring(0,1).equals("s")){
							//linea = "<a href='javascript:verReferencia(document.frmcoConsultadocs.det"+numLinea+");'>"+linea.substring(1)+"</a>";
							linea = "<a href='javascript:verReferencia("+numLinea+");'>"+linea.substring(1)+"</a>";
						}else{
							linea = linea.substring(1);
						}
					}
//GOR 20110113 (Req. Confirming - 6,7 - Inicio): Incluir un nuevo campo que indique la CLAVE DE RASTREO
					if(pos+1!=19){
	%>
					<td class="<%=estilo%>" height="20px" <%=((++pos == 5)?"style=\"text-align:right;\"":"")%>><%=linea%></td>
	<%
					} else {
	%>
					<input type="hidden" name="det<%= numLinea%>" value="<%= linea%>">
	<%
					detalles.put(""+numLinea,linea);
					}
//GOR 20110113 (Req. Confirming - 6,7 - Fin)
				}
				// Variable que controla la impresion de subtotales (18/02/2004-LMR-Q1087)
				ctrl = 1;
				// Fin actualizacion de variable de control (LMR-Q1087)
	%>
				</tr>
	<%
			}
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("EXCEPCION EN EL JSP coCancelacionCon.jsp (2) " + e.getMessage(), EIGlobal.NivelLog.ERROR);}
	%>
		<tr><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px"  class="tittabdat">Subtotal</td><td class="tittabdat" height="20px" align=right><%=nf.format(impTmp)%></td></tr>
	</table>
	<br>
	<%
		try{
			parser = new StringTokenizer(parInfo,"\n");
			while(parser.hasMoreTokens()){
				linea = parser.nextToken();
				while((pos=linea.indexOf("||")) != -1) linea = linea.substring(0,pos+1) + "&nbsp;" + linea.substring(pos+1);
				parserLinea = new StringTokenizer(linea,"|");
				linea=parserLinea.nextToken();
				if(linea.equals("-")){
	%>
					<input type="hidden" name="neteo" value="<%=parserLinea.nextToken()%>|<%=parserLinea.nextToken()%>|<%=parserLinea.nextToken()%>|<%=parserLinea.nextToken()%>">
	<%
				}
			}
		}catch(Exception e8){
			EIGlobal.mensajePorTrace("EXCEPCION EN EL JSP coCancelacionCon.jsp " + e8.getMessage(), EIGlobal.NivelLog.ERROR);}
	}
	%>

	<%if(Integer.parseInt(totalUSD)>0){ %>
	<table cellpadding='2' cellspacing='1' width = "80%">
		<tr>
			<td class='texenccon' colspan="19">OPERACIONES EN D&Oacute;LAR AMERICANO: <%= (String) request.getAttribute("totalUSD") %></td>
		</tr>
		<tr>
			<td class="tittabdat">
			<input type='checkbox' name='todos' onclick='CheckTodos();'>Todos
			</td>
			<td class="tittabdat" align = "center"><B>Clave prov.</B></td>
			<td class="tittabdat" align = "center"><B>Nombre o raz&oacute;n social</B></td>
			<td class="tittabdat" align = "center"><B>Tipo docto.</B></td>
			<td class="tittabdat" align = "center"><B>No. docto.</B></td>
			<td class="tittabdat" align = "center"><B>Importe</B></td>
			<td class="tittabdat" align = "center"><B>Divisas</B></td>
			<td class="tittabdat" align = "center"><B>Fecha vto.</B></td>
			<td class="tittabdat" align = "center"><B>Fecha Oper.</B></td>
<!--GOR 20110113 (Req. Confirming - 5 - Inicio): Cambiar la etiqueta en las pantallas de REFERENCIA por REFERENCIA DE OPERACI�N -->
			<td class="tittabdat" align = "center"><B>Referencia de Operaci&oacute;n</B></td>
<!--GOR 20110113 (Req. Confirming - 5 - Fin) -->
			<td class="tittabdat" align = "center"><B>Status</B></td>
			<td class="tittabdat" align = "center"><B>Observaciones</B></td>
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de REFERENCIA INTERBANCARIA por N�MERO DE REFERENCIA -->
			<td class="tittabdat" align = "center"><B>N&uacute;mero de Referencia</B></td>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de CONCEPTO INTERBANCARIO por CONCEPTO DEL PAGO -->
			<td class="tittabdat" align = "center"><B>Concepto del Pago</B></td>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->
			<td class="tittabdat" align = "center"><B>Forma de Aplicaci&oacute;n</B></td>
			<td class="tittabdat" align = "center"><B>Cuenta Cargo</B></td>
			<td class="tittabdat" align = "center"><B>Forma de Pago</B></td>
<!--GOR 20110113 (Req. Confirming - 6,7 - Inicio): Incluir un nuevo campo que indique la CLAVE DE RASTREO -->
			<td class="tittabdat" align = "center"><B>Clave de Rastreo</B></td>
<!--GOR 20110113 (Req. Confirming - 6,7 - Fin) -->
			<!-- Se agrega canal -->
			<td class="tittabdat" align = "center"><B>Canal</B></td>
		</tr>
	<%
		parInfo = (String)request.getAttribute("InfoUSD");
		//String linea;
		estilo="textabdatobs";
		parser = new StringTokenizer(parInfo,"\n");
		//StringTokenizer parserLinea;
		//int pos;
		// Declaracion de variables para incluir un subtotal por fecha de vencimiento (18/02/2004-LMR-Q1087)
		//double impTmp;
		//String fecSub;
		//String fecTmp;
		//int    ctrl;
		//int    posfec;
	    //NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		// Fin declaracion (LMR-Q1087)
		// -------------------
		lineasParaOrdenar = new Vector();
	    try{
			while(parser.hasMoreTokens()) lineasParaOrdenar.add(parser.nextToken());
			String nombre1 , nombre2 ;
			String num1    , num2    , cadAux;
			int    a       , b       , total = lineasParaOrdenar.size();
		// Cambio para ordenar por fecha de vencimiento bloque 1 inicio (18/02/2004-LMR-Q1087)
			Date   Fecha1  , Fecha2  ;
			DateFormat dt =DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRENCH);
		// Cambio para ordenar por fecha de vencimiento bloque 1 (18/02/2004-LMR-Q1087)
			for(a = 0; a < total-1; a++)
				for(b = a+1; b < total; b++){
					nombre1 = (String)lineasParaOrdenar.get(a); if(nombre1.startsWith("-")) continue;
		// Cambio para ordenar por fecha de vencimiento bloque 2 inicio (18/02/2004-LMR-Q1087)
					for(posfec = 0; posfec < 7; posfec++) nombre1 = nombre1.substring(nombre1.indexOf('|')+1);
					Fecha1 = dt.parse(nombre1);
		// Cambio para ordenar por fecha de vencimiento bloque 2 (18/02/2004-LMR-Q1087)
					nombre2 = (String)lineasParaOrdenar.get(b); if(nombre2.startsWith("-")) continue;
		// Cambio para ordenar por fecha de vencimiento bloque 3 inicio (18/02/2004-LMR-Q1087)
					for(posfec = 0; posfec < 7; posfec++) nombre2 = nombre2.substring(nombre2.indexOf('|')+1);
					Fecha2 = dt.parse(nombre2);
		// Cambio para ordenar por fecha de vencimiento bloque 3 (18/02/2004-LMR-Q1087)
					if(Fecha1.compareTo(Fecha2)>0){
						cadAux = (String)lineasParaOrdenar.get(a);
						lineasParaOrdenar.set(a,lineasParaOrdenar.get(b));
						lineasParaOrdenar.set(b,cadAux);
					}
				}
			parInfo = "";
			for(a = 0; a < total; a++) parInfo += (String)lineasParaOrdenar.get(a) + "\n";
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("EXCEPCION EN EL JSP coCancelacionCon.jsp (1): " + e.getMessage(), EIGlobal.NivelLog.ERROR);}

		parser = new StringTokenizer(parInfo,"\n");
		// -------------------
		// Inicializa Variables (18/02/2004-LMR-Q1087)
		impTmp		=0;
		fecSub		="";
		fecTmp		="";
		ctrl    	=0;
		// Fin inicializacion (LMR-Q1087)
		try{
			while(parser.hasMoreTokens()){
				numLinea++;
		// Esto se repite para cada registro
				estilo=(estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
				linea = parser.nextToken();
				String fPagoUSD = null;
				StringTokenizer formaPago = new StringTokenizer(linea,"|");
				int fpi = 0;
				for(int fpj=0; fpj<16; fpj++){
					formaPago.nextToken();
					if(fpi==15)
						fPagoUSD= formaPago.nextToken();
					fpi++;
				}
				while((pos=linea.indexOf("| |")) != -1) linea = linea.substring(0,pos+1) + "&nbsp;" + linea.substring(pos+1);
		// Inicia bloque para la linea de subtotal (LMR-Q1087)
		// Obtiene la cadena de la fecha y si es diferente al que trae la variable temporal imprime la linea de subtotal (18/02/2004-LMR-Q1087)
		// La fecha de vencimiento esta en la posicion 7
				fecSub = linea;
				for(posfec = 0; posfec < 7; posfec++)
					fecSub = fecSub.substring(fecSub.indexOf('|')+1);
		//corta la cadena entre los caracteres |
				fecSub = fecSub.substring(0, fecSub.indexOf('|'));
		// si control es diferente de cero verifica si ha cambiado la fecha, si cambia, inicializa variables
		// e imprime la linea de subtotal
				if (ctrl != 0){
					if((!fecSub.equals(fecTmp))&&(!fecTmp.equals(""))){
	%>
						<tr><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px"  class="tittabdat">Subtotal</td><td class="tittabdat" height="20px" align=right><%=nf.format(impTmp)%></td></tr>
	<%
						ctrl   = 1;
						impTmp = 0;
						fecTmp = "";
					}
				}
				// Fin bloque para la linea de subtotal (LMR-Q1087)
				parserLinea = new StringTokenizer(linea,"|");
				linea=parserLinea.nextToken();
				if(linea.equals("-")) continue;
				if(!linea.equals(" "))
					linea = "<input type='checkbox'  name ='cancel' value=\"" + fPagoUSD + "|" + linea + "\"  onclick='verifica();'>";
				else
					linea = "&nbsp;";
	%>
				<tr><td class="<%=estilo%>"><%=linea%></td>
	<%
				pos=0;
				while(parserLinea.hasMoreTokens()){
					linea = parserLinea.nextToken();
					// Control para poner el nombre en una variable temporal y sumar el importe (18/02/2004-LMR-Q1087)
					if ((pos+1) == 5){
						linea=linea.replace('$',  ' ');
						linea.trim();
						while(linea.indexOf(',')!=-1) linea=linea.substring(0, linea.indexOf(',')) + linea.substring(linea.indexOf(',')+1);
						impTmp+=Double.valueOf((String)linea).doubleValue();
					}
					if ((pos+1) == 7)
						fecTmp   =(String) linea;
					// Termina bloque de actualizacion de variables temporales (LMR-Q1087)
					if ((pos+1) == 14){
						  if(linea.equals("1"))
							linea="MISMO DIA";
						  else
							  if(linea.equals("2"))
								linea="DIA SIGUIENTE";
							  else
								 linea="MISMO DIA";
					}
					// DAG 141009 Se agrega liga de referencia
					if((pos+1) == 9){
						if(linea.substring(0,1).equals("s")){
							//linea = "<a href='javascript:verReferencia(document.frmcoConsultadocs.det"+numLinea+");'>"+linea.substring(1)+"</a>";
							  linea = "<a href='javascript:verReferencia("+numLinea+");'>"+linea.substring(1)+"</a>";
						}else{
							linea = linea.substring(1);
						}
					}
//GOR 20110113 (Req. Confirming - 6,7 - Inicio): Incluir un nuevo campo que indique la CLAVE DE RASTREO
					pos++;
					if(pos != 19) {
	%>
					<td class="<%=estilo%>" height="20px" <%=((pos == 5)?"style=\"text-align:right;\"":"")%>><%=linea%></td>
	<%
					} else {
	%>
					<input type="hidden" name="det<%= numLinea%>" value="<%= linea%>">
	<%
					detalles.put(""+numLinea,linea);
					}
//GOR 20110113 (Req. Confirming - 6,7 - Fin)
				}
				// Variable que controla la impresion de subtotales (18/02/2004-LMR-Q1087)
				ctrl = 1;
				// Fin actualizacion de variable de control (LMR-Q1087)
	%>
				</tr>
	<%
			}
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("EXCEPCION EN EL JSP coCancelacionCon.jsp (2) " + e.getMessage(), EIGlobal.NivelLog.ERROR);}
	%>
		<tr><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px">&nbsp;</td><td height="20px"  class="tittabdat">Subtotal</td><td class="tittabdat" height="20px" align=right><%=nf.format(impTmp)%></td></tr>
	</table>
	<%
		try{
			parser = new StringTokenizer(parInfo,"\n");
			while(parser.hasMoreTokens()){
				linea = parser.nextToken();
				while((pos=linea.indexOf("||")) != -1) linea = linea.substring(0,pos+1) + "&nbsp;" + linea.substring(pos+1);
				parserLinea = new StringTokenizer(linea,"|");
				linea=parserLinea.nextToken();
				if(linea.equals("-")){
	%>
					<input type="hidden" name="neteo" value="<%=parserLinea.nextToken()%>|<%=parserLinea.nextToken()%>|<%=parserLinea.nextToken()%>|<%=parserLinea.nextToken()%>">
	<%
				}
			}
		}catch(Exception e8){
			EIGlobal.mensajePorTrace("EXCEPCION EN EL JSP coCancelacionCon.jsp " + e8.getMessage(), EIGlobal.NivelLog.ERROR);}
	}
	}//LMR Q2959 20/05/2004
	else
	{
	%>
		<center><span class="texencconbol"> La consulta contiene mas de 1000 registros, es necesario que exporte la informacion</span></center>
	<%
	}//LMR Q2959 20/05/2004
	%>

	<input type="hidden" name="neteo" value="x">
	<input type="hidden" name="neteo" value="xx">
	<BR>
	<div align='center'>
		<a href='javascript:cancela();' border=0><img src='/gifs/EnlaceMig/gbo25190.gif' border=0 alt='Cancelar'></a>
		<a href='javascript:scrImpresion();'><img src='/gifs/EnlaceMig/gbo25240.gif' border='0' alt='Imprimir'></A>
		<a href ='coCancelacion'><img src='/gifs/EnlaceMig/gbo25320.gif' border='0' alt='Regresar'></a>
	</div>
	<div align='center'>
		<table width="180" border="0" cellspacing="5" cellpadding="0">
			<tr valign="middle">
				<td align='center' class="tabmovtex" nowrap>Formato de Archivo:</td>
			</tr>
			<tr>
				<td class="tabmovtex" align="center" nowrap>
					<input type="RADIO" name="tipoExportacion" value="SD" checked ><b>Sin Divisa</b>&nbsp;
					<input type="RADIO" name="tipoExportacion" value="CD" ><b>Con Divisa</b>
				</td>
			</tr>
			<tr align="center">
				<td class="tabmovtex" nowrap>
					<a href="javascript:downloadFile();"><img src='/gifs/EnlaceMig/gbo25230.gif' border='0' alt='Exportar'></A>
				</td>
			</tr>
		</table>
	</div>

	<input type="hidden" name="fileExportacion" value="<%= (String)request.getAttribute("Exportacion") %>">
	<input type="hidden" name="fileExportacionAntes" value="<%= (String)request.getAttribute("ExportacionAntes") %>">
	<input type="hidden" name="cancel" value="x">
	<input type="hidden" name="cancel" value="xx">
	<input type="hidden" name="Cancelaciones" value="">
	<input type="hidden" name="Reg" value="<%= (String)request.getAttribute("Reg") %>">
	<input type="hidden" name="Arch" value="<%= (String)request.getAttribute("Arch") %>">
	<input type="hidden" name="fecha1" value="<%= (String)request.getAttribute("fecha1") %>">
	<input type="hidden" name="fecha2" value="<%= (String)request.getAttribute("fecha2") %>">
	<input type="hidden" name="Importe" value="<%= (String)request.getAttribute("Importe") %>">
	<input type="hidden" name="Proveedor" value="<%= (String)request.getAttribute("Proveedor") %>">
	<%
	int x, total;
	String[] parTDoc = (String[])request.getAttribute("TDoc");
	String[] parEstatus = (String[])request.getAttribute("Estatus");
	total= parTDoc.length;
	for(x=0;x<total;x++) {%><input type="hidden" name="TDoc" value="<%= parTDoc[x] %>"><%}
	total= parEstatus.length;
	for(x=0;x<total;x++) {%><input type="hidden" name="Estatus" value="<%= parEstatus[x] %>"><%}
	session.setAttribute("detalles", detalles);
	%>
	<input type="hidden" name="tipoReq" value="2">
	</FORM>

</body>
</html>