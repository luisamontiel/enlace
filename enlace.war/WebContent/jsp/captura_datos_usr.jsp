<%@page import="mx.altec.enlace.bo.BaseResource"%>
<html>
    <head>
        <title>
            Enlace por Internet
        </title>
        <meta http-equiv="Content-Type" content="text/html;">
        <meta name="Codigo de Pantalla" content="s25290">
        <meta name="Proyecto" content="Portal">
        <meta name="Version" content="1.0">
        <meta name="Ultima version" content="27/04/2001 18:00">
        <meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">

        <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
        <script language = "JavaScript" SRC= "/EnlaceMig/Bitacora.js"></script>
        <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
        <script type="text/javascript" src="/EnlaceMig/util.js"></script>
        <script type="text/javascript" src="/EnlaceMig/pm_fp.js"></script>

        <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

        <Script language = "Javascript" >



            //Secci�n Java
            <%
            String path = request.getContextPath();
            // Datos Usuario
            String idUsuario = (String) request.getAttribute("idUsuario");
            String nombreUsuario = (String) request.getAttribute("nombreUsuario");
            String apePaternoUsuario = (String) request.getAttribute("apePaternoUsuario");
            String apeMaternoUsuario = (String) request.getAttribute("apeMaternoUsuario");
            String emailUsuario = (String) request.getAttribute("emailUsuario");
            String celular = (String) request.getAttribute("celularUsr");
            String ladaCel = (String) request.getAttribute("ladaUsr");
            String carrier = (String) request.getAttribute("carrierCel");
            java.util.HashMap carriers = (java.util.HashMap) request.getAttribute("carriers");
            String celularComp = null;
            String statusCel = "";
            String statusEmail = "";

            if (ladaCel != null && celular != null) {
                celularComp = celularComp = ladaCel + celular;
            }

            if (ladaCel == null) {
                ladaCel = "";
            }

            if (celular == null) {
                celular = "";
            }

            if (emailUsuario == null) {
                emailUsuario = "No Disponible";
            }

            String msgDisable = "No se puede modificar esta secci&oacute;n de datos personales ya que su informaci&oacute;n est&aacute; incompleta en el sistema central.";

            BaseResource bSession = null;

            bSession = (BaseResource) request.getSession().getAttribute("session");
            statusCel = bSession.getStatusCelular();
            statusEmail = bSession.getStatusCorreo();

            //Datos Contrato
            String cveContrato = (String) request.getAttribute("cveContrato");
            String descContrato = (String) request.getAttribute("descContrato");
            String emailContrato = (String) request.getAttribute("emailContrato");
            String Encabezado = (String) request.getAttribute("Encabezado");
            String newMenu = (String) request.getAttribute("newMenu");
            String MenuPrincipal = (String) request.getAttribute("MenuPrincipal");
            String exitoMsg = (String) request.getAttribute("exitoMsg");
            String errorMsg = (String) request.getAttribute("errorMsg");
            %>

            <%=newMenu%>

            function pintaExito(msg) {
                cuadroDialogo(msg, 1);
                /**alert("msg");*/
            }

            function pintaError(msg) {
                cuadroDialogo(msg, 3);
                /**alert("msg");*/
            }

            function trimString(str) {
                str = this != window ? this : str;
                return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
            }

            function echeck(str) {
                var at = "@";
                var dot = ".";
                var lat = str.indexOf(at);
                var lstr = str.length;
                var ldot = str.indexOf(dot);

                if (str.indexOf(at) == -1)
                    return false;
                if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr)
                    return false;
                if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr)
                    return false;
                if (str.indexOf(at, (lat + 1)) != -1)
                    return false;
                if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot)
                    return false;
                if (str.indexOf(dot, (lat + 2)) == -1)
                    return false;
                if (str.indexOf(" ") != -1)
                    return false;
                return true;
            }


            function validaEmail(opc) {
                var emailCtr = document.FrmCapturaDatosUsr.emailContrato.value.toLowerCase();
                var emailUsr = document.FrmCapturaDatosUsr.emailUsuario.value.toLowerCase();
                var emailID;

                if (opc == 0) {
                    emailID = trimString(emailUsr);
                } else if (opc == 1) {
                    emailID = trimString(emailCtr);
                }

                if (trimString(emailID) != "") {
                    if (echeck(emailID) == false) {
                        if (opc == 0) {
                            document.FrmCapturaDatosUsr.emailUsuario.focus();
                        } else if (opc == 1) {
                            document.FrmCapturaDatosUsr.emailContrato.focus();
                        }
                        cuadroDialogo("El correo especificado no es v&aacute;lido.", 3);
                        /**alert("El correo especificado no es v&aacute;lido.");*/
                        return false
                    }
                } else {
                    cuadroDialogo("Es necesario incluir el correo para env&iacute;o de detalle", 3);
                    /**alert("Es necesario incluir el correo para env&iacute;o de detalle");*/
                    return false;

                }
                var correosInvalidos = new Array("banca_elactronica@bsantander.com.mx",
                        "banca_electronica@bsantander.com.mx",
                        "banca_electronica@santander.com.mx",
                        "hugrodriguez@santander.com.mx",
                        "xanca_elactronica@bsantander.com.mx");

                var n = 0;
                var emailTmp = "";

                for (n = 0; n < correosInvalidos.length; n++) {
                    emailTmp = correosInvalidos[n];
                    if (emailID == emailTmp) {
                        cuadroDialogo("El correo especificado no es v&aacute;lido", 3);
                        return false;
                    }
                }

                return true;
            }

            function validaCel() {
                var ladaSel = document.FrmCapturaDatosUsr.ladaUsuario.value;
                var carrierSel = document.FrmCapturaDatosUsr.carrier.value;
                var numCelular = document.FrmCapturaDatosUsr.celUsuario.value;
                var RegExPattern_Lada = /^[0-9]{2,3}$/
                var RegExPattern_Cel = /^[0-9]{7,8}$/
                if (ladaSel.match(RegExPattern_Lada)) {
                    if (numCelular.match(RegExPattern_Cel)) {
                        if (ladaSel.length + numCelular.length == 10) {
                            return true;
                        } else {
                            cuadroDialogo("El N&uacute;mero de Celular incorrecto, el n&uacute;mero m&aacute;s la Lada debe ser de 10 digitos", 3);
                            return false;
                        }
                    } else {
                        cuadroDialogo("El N&uacute;mero de Celular no es v&aacute;lido", 3);
                        return false;
                    }
                } else {
                    cuadroDialogo("Lada no v&aacute;lida", 3);
                    return false;
                }
            }


            function enviarDataUsr() {
                var forma = document.FrmCapturaDatosUsr;
                var contador = -1;

                for (j = 0; j < forma.length; j++) {
                    if (forma.elements[j].type == 'radio' && forma.elements[j].name == 'tipoMod') {
                        if (forma.elements[j].checked) {
                            contador = forma.elements[j].value;
                        }
                    }
                }

                if (contador == -1) {
                    cuadroDialogo("Debe seleccionar al menos una opci&oacute;n", 1);
                    /**alert("Debe seleccionar al menos una opci&oacute;n");*/
                } else {
                    //Modificacion de Datos Contrato
                    if (contador == 1) {
                        if (validaEmail(contador)) {
                            document.FrmCapturaDatosUsr.opcion.value = 1;
                            document.FrmCapturaDatosUsr.submit();
                        }
                    }
                    //Modificacion de Datos Usuario
                    if (contador == 0) {
            <%if (statusCel.equals("X")) {%>
                        if (validaEmail(contador)) {
                            document.FrmCapturaDatosUsr.opcion.value = 1;
                            document.FrmCapturaDatosUsr.submit();
                        }
            <%}%>

            <%if (statusEmail.equals("X")) {%>
                        if (validaCel()) {
                            document.FrmCapturaDatosUsr.opcion.value = 1;
                            document.FrmCapturaDatosUsr.submit();
                        }
            <%}%>

            <%if (!statusEmail.equals("X") && !statusCel.equals("X")) {%>
                        if (validaEmail(contador) && validaCel()) {
                            document.FrmCapturaDatosUsr.opcion.value = 1;
                            document.FrmCapturaDatosUsr.submit();
                        }
            <%}%>
                    }

                }
            }


            function resetDataUsr() {
                document.FrmCapturaDatosUsr.reset();
            }


            function MM_preloadImages() { //v3.0
                var d = document;
                if (d.images) {
                    if (!d.MM_p)
                        d.MM_p = new Array();
                    var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
                    for (i = 0; i < a.length; i++)
                        if (a[i].indexOf("#") != 0) {
                            d.MM_p[j] = new Image;
                            d.MM_p[j++].src = a[i];
                        }
                }
            }


            function MM_swapImgRestore() { //v3.0
                var i, x, a = document.MM_sr;
                for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
                    x.src = x.oSrc;
            }


            function MM_findObj(n, d) { //v3.0
                var p, i, x;
                if (!d)
                    d = document;
                if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                    d = parent.frames[n.substring(p + 1)].document;
                    n = n.substring(0, p);
                }
                if (!(x = d[n]) && d.all)
                    x = d.all[n];
                for (i = 0; !x && i < d.forms.length; i++)
                    x = d.forms[i][n];
                for (i = 0; !x && d.layers && i < d.layers.length; i++)
                    x = MM_findObj(n, d.layers[i].document);
                return x;
            }


            function MM_swapImage() { //v3.0
                var i, j = 0, x, a = MM_swapImage.arguments;
                document.MM_sr = new Array;
                for (i = 0; i < (a.length - 2); i += 3)
                    if ((x = MM_findObj(a[i])) != null) {
                        document.MM_sr[j++] = x;
                        if (!x.oSrc)
                            x.oSrc = x.src;
                        x.src = a[i + 2];
                    }
            }
            //-->
        </script>

        <script language="JavaScript">
            <!--
        function MM_findObj(n, d) { //v4.01
                var p, i, x;
                if (!d)
                    d = document;
                if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                    d = parent.frames[n.substring(p + 1)].document;
                    n = n.substring(0, p);
                }
                if (!(x = d[n]) && d.all)
                    x = d.all[n];
                for (i = 0; !x && i < d.forms.length; i++)
                    x = d.forms[i][n];
                for (i = 0; !x && d.layers && i < d.layers.length; i++)
                    x = MM_findObj(n, d.layers[i].document);
                if (!x && d.getElementById)
                    x = d.getElementById(n);
                return x;
            }


            function MM_preloadImages() { //v3.0
                var d = document;
                if (d.images) {
                    if (!d.MM_p)
                        d.MM_p = new Array();
                    var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
                    for (i = 0; i < a.length; i++)
                        if (a[i].indexOf("#") != 0) {
                            d.MM_p[j] = new Image;
                            d.MM_p[j++].src = a[i];
                        }
                }
            }


            function MM_swapImgRestore() { //v3.0
                var i, x, a = document.MM_sr;
                for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
                    x.src = x.oSrc;
            }


            function MM_swapImage() { //v3.0
                var i, j = 0, x, a = MM_swapImage.arguments;
                document.MM_sr = new Array;
                for (i = 0; i < (a.length - 2); i += 3)
                    if ((x = MM_findObj(a[i])) != null) {
                        document.MM_sr[j++] = x;
                        if (!x.oSrc)
                            x.oSrc = x.src;
                        x.src = a[i + 2];
                    }
            }


            function MM_openBrWindow(theURL, winName, features) { //v2.0
                window.open(theURL, winName, features);
            }


            function MM_nbGroup(event, grpName) {
                var i, img, nbArr, args = MM_nbGroup.arguments;
                if (event == "init" && args.length > 2) {
                    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
                        img.MM_init = true;
                        img.MM_up = args[3];
                        img.MM_dn = img.src;
                        if ((nbArr = document[grpName]) == null)
                            nbArr = document[grpName] = new Array();
                        nbArr[nbArr.length] = img;
                        for (i = 4; i < args.length - 1; i += 2)
                            if ((img = MM_findObj(args[i])) != null) {
                                if (!img.MM_up)
                                    img.MM_up = img.src;
                                img.src = img.MM_dn = args[i + 1];
                                nbArr[nbArr.length] = img;
                            }
                    }
                } else if (event == "over") {
                    document.MM_nbOver = nbArr = new Array();
                    for (i = 1; i < args.length - 1; i += 3)
                        if ((img = MM_findObj(args[i])) != null) {
                            if (!img.MM_up)
                                img.MM_up = img.src;
                            img.src = (img.MM_dn && args[i + 2]) ? args[i + 2] : args[i + 1];
                            nbArr[nbArr.length] = img;
                        }
                } else if (event == "out") {
                    for (i = 0; i < document.MM_nbOver.length; i++) {
                        img = document.MM_nbOver[i];
                        img.src = (img.MM_dn) ? img.MM_dn : img.MM_up;
                    }
                } else if (event == "down") {
                    if ((nbArr = document[grpName]) != null)
                        for (i = 0; i < nbArr.length; i++) {
                            img = nbArr[i];
                            img.src = img.MM_up;
                            img.MM_dn = 0;
                        }
                    document[grpName] = nbArr = new Array();
                    for (i = 2; i < args.length - 1; i += 2)
                        if ((img = MM_findObj(args[i])) != null) {
                            if (!img.MM_up)
                                img.MM_up = img.src;
                            img.src = img.MM_dn = args[i + 1];
                            nbArr[nbArr.length] = img;
                        }
                }
            }

            //-->
        </script>

        <script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
        <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
    </head>

    <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif', '/gifs/EnlaceMig/gbo25181.gif', '/gifs/EnlaceMig/gbo25131.gif', '/gifs/EnlaceMig/gbo25111.gif', '/gifs/EnlaceMig/gbo25151.gif', '/gifs/EnlaceMig/gbo25031.gif', '/gifs/EnlaceMig/gbo25032.gif', '/gifs/EnlaceMig/gbo25051.gif', '/gifs/EnlaceMig/gbo25052.gif', '/gifs/EnlaceMig/gbo25091.gif', '/gifs/EnlaceMig/gbo25092.gif', '/gifs/EnlaceMig/gbo25012.gif', '/gifs/EnlaceMig/gbo25071.gif', '/gifs/EnlaceMig/gbo25072.gif', '/gifs/EnlaceMig/gbo25011.gif');
                        obtenDatosBrowser();" background="/gifs/EnlaceMig/gfo25010.gif">


        <table width="571" border="0" cellspacing="0" cellpadding="0">
            <tr valign=top>
                <td width="*">
                    <!-- MENU PRINCIPAL -->
                    <%= MenuPrincipal%>
                </td>
            </tr>
        </table>

        <table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="676" valign="top" align="left">
                    <table width="666" border="0" cellspacing="6" cellpadding="0">
                        <tr>
                            <td width="528" valign="top" class="titpag">
                                <%= Encabezado%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <!--<form name = "Frmfechas">
        <%= request.getAttribute("Bitfechas")%></td>
</form>-->

        <form name="FrmCapturaDatosUsr"  method="post" action="<%=path%>/enlaceMig/CapturaDatosUsr">
            <input type="hidden" name="opcion"/>
            <input type="hidden" id="datosBrowser" name="datosBrowser" value=""/>
            <table width="760" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <table width="670" border="0" cellspacing="2" cellpadding="3" bgcolor="#FFFFFF">
                            <tr>
                                <td class="tittabdat" width="326">
                                    <%-- if(celularComp != null && celularComp != "" || emailUsuario != null && emailUsuario != ""){ --%>
                                    <% if (statusCel.equals("X") && statusEmail.equals("X")) { %>
                                    <input type="radio" name="tipoMod" value=0 disabled="true"/>
                                    <% } else { %>
                                    <input type="radio" name="tipoMod" value=0 checked= "checked"/>
                                    <%}%>
                                    <%-- Termina Consys 11.08.2010 --%>

                                    Datos de Usuario
                                </td>
                            </tr>
                            <tr align="center">
                                <td class="textabdatcla" valign="top" colspan="2">
                                    <table width="660" border="0" cellspacing="0" cellpadding="0">
                                        <tr valign="top">
                                            <td width="420" align="left">
                                                <table width="420" border="0" cellspacing="0" cellpadding="5">
                                                    <tr>
                                                        <td align="right" class="tabmovtex" width="140" nowrap>
                                                            Codigo Cliente:
                                                        </td>
                                                        <td class="tabmovtex" width="260" nowrap>
                                                            <input type="text" name="cveUsuario" value="<%=idUsuario%>" readonly="readonly" size="30"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="tabmovtex" nowrap>
                                                            Apellido Paterno:
                                                        </td>
                                                        <td class="tabmovtex" nowrap>
                                                            <input type="text" name="apePaterno" value="<%=apePaternoUsuario%>" readonly="readonly" size="30"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="tabmovtex">
                                                            E-Mail:
                                                        </td>
                                                        <td class="tabmovtex" nowrap valign="middle">
                                                            <%-- if(celularComp != null && celularComp != "" || emailUsuario != null && emailUsuario != ""){ --%>
                                                            <% if (statusEmail.equals("X")) { %>
                                                            <input type="text" name="emailUsuario" value="No disponible" size="55" disabled="true" />
                                                            <%} else {%>
                                                            <input type="text" name="emailUsuario" value="<%=emailUsuario%>" size="55"/>
                                                            <%} %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="tabmovtex">
                                                            SMS-Celular:
                                                        </td>
                                                        <td>
                                                            <table width="260" border="0" cellspacing="0" cellpadding="5">
                                                                <tr valign="bottom">
                                                                    <td align="left" class="tabmovtex">
                                                                        Lada
                                                                    </td>
                                                                    <td align="right" class="tabmovtex">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="left" class="tabmovtex">
                                                                        Celular
                                                                    </td>
                                                                    <td align="left" class="tabmovtex">
                                                                        Compa&ntilde;&iacute;a Telef&oacute;nica
                                                                    </td>

                                                                </tr>
                                                                <tr valign="top">
                                                                    <td align="left" class="tabmovtex">
                                                                        <%-- if(celularComp != null && celularComp != "" || emailUsuario != null && emailUsuario != ""){ --%>
                                                                        <% if (statusCel.equals("X")) { %>
                                                                        <input type="text" name="ladaUsuario" value="---" size="3" maxlength="3" disabled="true"/>
                                                                        <%} else {%>
                                                                        <input type="text" name="ladaUsuario" value="<%=ladaCel%>" size="3" maxlength="3" />
                                                                        <%} %>
                                                                    </td>
                                                                    <td align="center" class="tabmovtex">
                                                                        -
                                                                    </td>
                                                                    <td align="left" class="tabmovtex">
                                                                        <%-- if(celularComp != null && celularComp != "" || emailUsuario != null && emailUsuario != ""){ --%>
                                                                        <% if (!statusCel.equals("X")) {%>
                                                                        <input type="text" name="celUsuario" value="<%=celular%>" size="8" maxlength="8"/>
                                                                        <%} else {%>
                                                                        <input type="text" name="celUsuario" value="-------" size="8" maxlength="8" disabled="true" />
                                                                        <%} %>
                                                                    </td>
                                                                    <td align="left" class="tabmovtex">
                                                                        <% if (!statusCel.equals("X")) { %>
                                                                        <select id="carrier" name="carrier">
                                                                            <%} else { %>
                                                                            <select id="carrier" name="carrier" disabled="disabled">
                                                                                <%} %>
                                                                                <%--<%while(rs.next()){%>
                                                                                        <option id=<%=rs.getString(1) %>
                                                                                                <%=rs.getString(1) %></option>
                                                                                <%}%>--%>
                                                                                <%java.util.Iterator it = carriers.entrySet().iterator();
                                                                                    while (it.hasNext()) {
                                                                                        java.util.Map.Entry e = (java.util.Map.Entry) it.next();
                                                                                                                                        if (!e.getKey().equals("RES")) {%>
                                                                                <option value="<%=e.getKey()%>" <%if (e.getValue().equals(carrier)) {%><%= "selected"%><%}%> ><%=e.getValue()%></option>
                                                                                <%}//if
                                                                                                                                            }//while%>
                                                                            </select>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="240" align="left">
                                                <table width="240" border="0" cellspacing="0" cellpadding="3">
                                                    <tr>
                                                        <td align="right" class="tabmovtex" width="107" nowrap>
                                                            Nombre:
                                                        </td>
                                                        <td class="tabmovtex" width="15" valign="middle" align="center">
                                                            <input type="text" name="nombre" value="<%=nombreUsuario%>" readonly="readonly" size="30"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="tabmovtex" nowrap>
                                                            Apellido Materno:
                                                        </td>
                                                        <td class="tabmovtex" align="center" valign="middle">
                                                            <input type="text" name="apeMaterno" value="<%=apeMaternoUsuario%>" readonly="readonly" size="30"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%-- if(celularComp != null && celularComp != "" || emailUsuario != null && emailUsuario != ""){ --%>
                                                <% if (statusCel.equals("X") && statusEmail.equals("X")) { %>
                                                <table width="340" border="0" cellspacing="0" cellpadding="3" align="center" >
                                                    <tr>
                                                        <td>
                                                            <br>
                                                            <br>
                                                            <span class="tittabcom">No se puede modificar esta secci&oacute;n de datos personales ya que su informaci&oacute;n est&aacute; incompleta en el sistema central.</span>

                                                        </td>
                                                    </tr>
                                                </table>
                                                <%} %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <table width="670" border="0" cellspacing="2" cellpadding="3" bgcolor="#FFFFFF">
                            <tr>
                                <td class="tittabdat" width="326">
                                    <% if (statusCel.equals("X") && statusEmail.equals("X")) { %>
                                    <input type="radio" name="tipoMod" value=1 checked="checked">
                                    <%} else {%>
                                    <input type="radio" name="tipoMod" value=1>
                                    <%}%>
                                    Datos del Contrato
                                </td>
                            </tr>
                            <tr align="center">
                                <td class="textabdatcla" valign="top" colspan="2">
                                    <table width="660" border="0" cellspacing="0" cellpadding="0">
                                        <tr valign="top">
                                            <td width="420" align="left">
                                                <table width="420" border="0" cellspacing="0" cellpadding="5">
                                                    <tr>
                                                        <td align="right" class="tabmovtex" width="140" nowrap>
                                                            Contrato:
                                                        </td>
                                                        <td class="tabmovtex" width="260" nowrap>
                                                            <input type="text" name="cveContrato" value="<%=cveContrato%>" readonly="readonly" size="30">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="tabmovtex">
                                                            E-Mail:
                                                        </td>
                                                        <td class="tabmovtex" nowrap valign="middle">
                                                            <input type="text" name="emailContrato" value="<%=emailContrato%>" size="55">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="240" align="left">
                                                <table width="240" border="0" cellspacing="0" cellpadding="3">
                                                    <tr>
                                                        <td align="right" class="tabmovtex" width="107" nowrap>
                                                            Descripci&oacute;n:
                                                        </td>
                                                        <td class="tabmovtex" width="15" valign="middle" align="center">
                                                            <input type="text" name="descContrato" value="<%=descContrato%>" readonly="readonly" size="30">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
                            <tr>
                                <td align="right" valign="top" height="22" width="90">
                                    <a href="javascript:enviarDataUsr();">
                                        <img border="0" name=Enviar value=Enviar src="/gifs/EnlaceMig/gbo25280.gif" width="90" height="22" alt="Aceptar"/>
                                    </a>
                                </td>
                                <td align="left" valign="middle" height="22" width="76">
                                    <a href="javascript:resetDataUsr(this.form);">
                                        <img name=limpiar value=Limpiar border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"/>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
        <%if (errorMsg != null || exitoMsg != null) { %>
        <%if (errorMsg != null && exitoMsg == null) {%>
        <script type="text/javascript">
            pintaError("<%=errorMsg%>");
        </script>
        <%} else if (exitoMsg != null && errorMsg == null) {%>
        <script type="text/javascript">
            pintaExito("<%=exitoMsg%>");
        </script>
        <%}%>
        <%}%>
    </body>
</html>