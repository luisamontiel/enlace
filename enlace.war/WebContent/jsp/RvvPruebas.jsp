<%@ page import="java.util.*" %>
<%@ page import="EnlaceMig.*" %>

<%
boolean esConsulta = request.getParameter("Tuxedo") != null;
String tramaEnvio = request.getParameter("Trama"); if(tramaEnvio == null) tramaEnvio = "";
String tramaRegreso = "";
String portapapeles = request.getParameter("PortaPapeles"); if(portapapeles == null) portapapeles = "";

if(esConsulta)
	{
	try
		{
		ServicioTux tuxedo = new ServicioTux();
		//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		Hashtable ht = tuxedo.web_red(tramaEnvio);
		tramaRegreso = (String)ht.get("BUFFER");
		}
	catch(Exception e)
		{tramaRegreso = "" + e;}

	}
%>

<HTML>

<HEAD>
	<TITLE>Pruebas con tuxedo</TITLE>
</HEAD>

<BODY>

	<H1>Pruebas con Tuxedo</H1>

	<H2>Trama para enviar</H2>

	<FORM name="form" method="POST" action="RvvPruebas.jsp">
	<INPUT name="Tuxedo" type="Hidden" value="consultando...">
	<INPUT name="Trama" type="text" size="100"><BR>
	<INPUT type="Submit" value="enviar trama">


	<% if(esConsulta) { %>
	<HR>
	<H2>Resultado de la &uacute;ltima consulta</H2>

	<P><B>Trama enviada:</B></P>
	<PRE><%= tramaEnvio %></PRE>
	<P><B>Resultado:</B></P>
	<PRE><%= tramaRegreso %></PRE>
	<% } %>

	<HR>
	<H2>Portapapeles</H2>
	<TEXTAREA name="PortaPapeles" cols="100" rows="10"><%= portapapeles %></TEXTAREA>

	</FORM>

</BODY>

<HTML>