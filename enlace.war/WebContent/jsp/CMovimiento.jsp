<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% 	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	response.setDateHeader("Expires", 0); // Proxies.%>

<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%
SimpleDateFormat formaHora = new SimpleDateFormat("yyyyMMddHHmmss");
%>

  <script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script language="JavaScript" src="/EnlaceMig/fw_menu.js"></script>
  <script language="JavaScript" src="/EnlaceMig/ConMov.js"></script>
  <script language="JavaScript">

	function cambiaArchivo(extencion) {
		var boton = document.getElementById("exportar");
		if (extencion==="txt") {
			document.getElementById("tArchivo").value =extencion;
			document.getElementById("idTxt").checked= true;
		}else if(extencion==="csv"){
			document.getElementById("tArchivo").value =extencion;
			document.getElementById("idCsv").checked= true;
		}
		boton.href="javascript:Exportar();";
	}


	function Exportar(){
		var forma=document.frmbit;
		var contador=0;

		for(j=0;j<forma.length;j++){
			if(forma.elements[j].type=='radio' && forma.elements[j].name=='tipoArchivo'){
				if(forma.elements[j].checked){
					contador++;
				}
			}
		}

		if(contador===0){
			cuadroDialogo("Indique el tipo de archivo a exportar ",1);
			return;
		}else{
			var extencion = document.getElementById("tArchivo").value;
			window.open("/Enlace/enlaceMig/movimientos?tExportacion=" + extencion,"Movimientos","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		}
	}

   function getFechaHoy(){
	var hoy      = new Date();
	hoy.setFullYear(document.Frmfechas.strAnio.value);
	hoy.setMonth((Number(document.Frmfechas.strMes.value)-1));
	hoy.setDate(document.Frmfechas.strDia.value);
	return hoy;
}


  function verificaRadios(){
   if(document.getElementById("hoyRadio").checked){
   		cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del d&iacute;a",1);
	    return false;
   }
   return true;
 }


  function esHistorico(){

   if(document.getElementById("radioHistorico").checked){
   		return true;
   }else{
   		return false;
   }
  }


   function Selecciona(){
   		var lista = document.getElementById("comboCuenta0");
   		var indiceSeleccionado = lista.selectedIndex;
   		if(indiceSeleccionado===0){
   			document.getElementById('comboCuenta0').selectedIndex  = indiceSeleccionado+1;
   		}

   }

   <!--
    var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
    var dia;
    var mes;
    var anio;
    var fechaseleccionada;
    var opcioncalendario=0;
	var indice;

    function SeleccionaFecha(ind)
 {
	var fecha1= getFechaHoy();
   	fecha1.setDate(1);//fecha = 1/02/2015
	fecha1.setDate(fecha1.getDate() - 50);//fecha = 13/12/2014
	fecha1.setDate(1);//fecha = 1/12/2014
 	var fecha2= getFechaHoy();
	var diaI=fecha1.getDate();
	var mesI=fecha1.getMonth();
	var anioI=fecha1.getFullYear();
	var diaF=fecha2.getDate();
	var mesF=fecha2.getMonth();
	var anioF=fecha2.getFullYear();


	var m=new Date();
	m.setFullYear(document.Frmfechas.strAnio.value);
	m.setMonth(document.Frmfechas.strMes.value);
	m.setDate(document.Frmfechas.strDia.value);
	n=m.getMonth();
	dia=document.Frmfechas.strDia.value;
	mes=document.Frmfechas.strMes.value;
	anio=document.Frmfechas.strAnio.value;
    indice=ind;

   if(verificaRadios()){
   		if(esHistorico()){
			msg=window.open("/EnlaceMig/calpas.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
			msg.focus();
	  		opcioncalendario = 1;
   		}else{
   			opcioncalendario = 2;
   			var texto= "/Enlace/enlaceMig/jsp/" +
				"CalendarioHTML.jsp?diaI="+diaI+"&diaF="+diaF+"&mesI="+mesI+"&mesF="+mesF+"&" +
				"anioI="+anioI+"&anioF="+anioF+ "&mes=" + mesI + "&anio=" + anioI + "&priUlt=0#mesActual";
			var msg=window.open(texto,"calendario","toolbar=no," +
				"location=no,directories=no,status=no,menubar=no" +
				",scrollbars=yes,resizable=no,width=330,height=260");
			msg.focus();
   		}
	}
 }

    function Actualiza()
    {
		var fechaSelec = "";
        if (opcioncalendario===1){
			fechaSelec = fechaseleccionada;
        }else if (opcioncalendario===2){
			fechaSelec = fecha_completa;
		}
		if(indice===0){
			document.FrmMov.fecha1.value=fechaSelec;
		}else{
			document.FrmMov.fecha2.value=fechaSelec;
		}
    }

    var ctaselec;
    var ctadescr;
    var ctatipre;
    var ctatipro;
    var ctaserfi;
    var ctaprod;
    var ctasubprod;
    var tramadicional;
    var cfm;
    function PresentarCuentas()
    {
        msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
        msg.focus();
    }
    function actualizacuenta()
    {
        document.FrmMov.EnlaceCuenta.value=ctaselec+"@"+ctatipre+"@"+ctadescr+"@";
        document.FrmMov.textEnlaceCuenta.value=ctaselec+" "+ctadescr;
    }
    function EnfSelCta()
    {
    	if(document.FrmMov.textEnlaceCuenta){
    		if( document.FrmMov.EnlaceCuenta.value.length<=0 ){
    			document.FrmMov.textEnlaceCuenta.value="";
    		}
    	}else if(document.FrmMov.numeroCuenta0.value.length<=0){
    		document.FrmMov.EnlaceCuenta.value = "";
    	}
    }
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    function ctaPorParametro()
    {
        var miTram = "<% if( request.getAttribute("EnlaceCuenta")!=null) out.print(request.getAttribute("EnlaceCuenta")); else out.print(""); %>";
        if( miTram!="" )
        {
            var numCta = "";
            var ctaDes = "";
            var tipRel = "";
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            numCta = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            ctaDes = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            tipRel = miTram.substring( 0,miTram.indexOf("|") );
            document.FrmMov.EnlaceCuenta.value     = numCta +"@"+ tipRel +"@"+ ctaDes +"@";


            if(document.FrmMov.Banca.value !== "N"){
            	document.FrmMov.textEnlaceCuenta.value = numCta +" "+ ctaDes;
            }else{
            	document.FrmMov.numeroCuenta0.value = numCta;
            }
        }//fin if
    }


<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>
   //-->

  </script>

  <script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
	<script type="text/javascript">
		var contextPath = "${pageContext.request.contextPath}";
	</script>
	<script type="text/javascript" src="/EnlaceMig/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="/EnlaceMig/consultaCuentas.js?<%=formaHora.format(new Date())%>"></script>
	<script src="/EnlaceMig/json3.min.js" type="text/javascript"></script>

  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="document.FrmMov.deldia[0].checked = true;PutDate();MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');EnfSelCta();consultaCuentas();Selecciona();" background="/gifs/EnlaceMig/gfo25010.gif">
  <table border="0" cellpadding="0" cellspacing="0" width="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <form name = "Frmfechas">
<%
    if( request.getAttribute("Movfechas")!=null )
        out.println(request.getAttribute("Movfechas"));
%>
   </form>
  <form name="FrmMov" method="Post"  onsubmit="DoPost();" action="movimientos">
  <input  id="ventana" type="hidden"value="2" />
  <input id="opcion" type="hidden"  value="1" />
  <input type="hidden" name="EnlaceCuenta" value="" />
  <input id="tArchivo" type="hidden" name="tArchivo" value="csv" readonly="readonly" />
   <tr>
    <td  align="center">
     <table  cellspacing="2" cellpadding="3" border="0" width="${tipoBanca eq 'E' ? 600 : 400}">
     <c:choose>
     	<c:when test="${tipoBanca ne 'E'}">
	  <td colspan="3">
		<table border="0" width="100%">
				<td class="tittabdat"><input id="hoyRadio" type="radio" name="deldia" value="NORM" checked  onClick = "PutDate();" />Del d&iacute;a</td>
				<td class="tittabdat"><input type="radio" name="deldia" value="CDIA" onClick = "periodoDias(3);" />&Uacute;ltimos 3 d&iacute;as</td>
				<td class="tittabdat"><input type="radio" name="deldia" value="MMES" onClick = "periodoDias(30);" />Mes actual</td>
		</table>
		<table border="0" width="100%">
				<td class="tittabdat" width="50%"><input type="radio" name="deldia" value="UMES" onClick = "periodoDias(90);" />Mes actual y dos anteriores</td>
				<td class="tittabdat" width="50%"><input id="radioHistorico"  type="radio" name="deldia" value="HIST" onClick = "PutDate1();" />Por rango de fechas</td>
		</table>
	  </td>
     	</c:when>
     	<c:otherwise>
	<tr>
       <td class="tittabdat"><input id="hoyRadio" type="radio" name="deldia" value="NORM" checked  onClick = "PutDate();" />Movimientos del d&iacute;a</td>
       <td class="tittabdat"><input id="radioHistorico" type="radio" name="deldia" value="HIST" onClick = "PutDate1();" />Movimientos hist&oacute;ricos</td>
     </tr>
		</c:otherwise>
     </c:choose>
    <c:if test="${tipoBanca ne 'E'}">
		<tr>
			<td colspan="3" align="center" class="textabdatcla">
				<table cellpadding="5" width="600" class="textabdatcla">
					<tr>
						<td colspan="4" class="tabmovtexbol"><b>B&uacute;squeda cuentas:</b></td>
					</tr>
					<tr>
						<td align="right" class="tabmovtexbol">Cuenta:</td>
						<td colspan="2" class="tabmovtexbol"><input type="text" class="numeroCuenta" name="numeroCuenta0" id="numeroCuenta0" onblur="validaSoloNumeros(this,'Cuenta');" /></td>
						<td align="center" rowspan="2"><a href="javascript:consultaCuentas();">
													 <img border="0" width="90" height="22" alt="Consultar" src="/gifs/EnlaceMig/Ir.png" /></a>
						</td>
					</tr>
					<tr>
						<td align="right"  nowrap="" class="tabmovtexbol">Descripci&oacute;n cuenta:</td>
						<td colspan="2" class="tabmovtexbol"><input type="text" class="descripcionCuenta" id="descripcionCuenta0" onblur="validaEspeciales(this,'Descripci&oacute;n cuenta');" /></td>
					</tr>
				</table>
			</td>
		</tr>
	</c:if>
      <tr align="center">
       <td colspan="${tipoBanca eq 'E' ? 2 : 3}" class="textabdatcla" style="border-top:solid white 1px">
       <table class="textabdatcla" width="600" cellspacing="0" cellpadding="5">
	<c:choose>
	<c:when test="${tipoBanca eq 'E'}">
		<tr>
			<td align="right" class="tabmovtexbol" width="100" nowrap>Cuenta:</td>
			<td class="tabmovtexbol" colspan="2" width="290">
				<input type="text" name="textEnlaceCuenta"  class="tabmovtexbol" maxlength="22" size="22" onfocus="blur();" value="" />
				<a href="javascript:PresentarCuentas();"><img src="/gifs/EnlaceMig/gbo25420.gif" width="12" height="14" border="0" align="absmiddle" /></a>
          	</td>
		</tr>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="2" align="center"><%@ include file="/jsp/listaCuentas.jspf" %></td>
		</tr>
	</c:otherwise>
	</c:choose>
         <tr>
          <td align="right" class="tabmovtexbol" nowrap>De la fecha:</td>
          <td class="tabmovtex" nowrap>
           <input type="text" id="fecha1" name="fecha1" value="PutDate();"  onFocus="blur();" size="12" class="tabmovtexbol" />
           <a href ="javascript:SeleccionaFecha(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle" /></a>
          </td>
         </tr>
         <tr>
          <td align="right" class="tabmovtexbol" nowrap>A la fecha:</td>
          <td class="tabmovtex" nowrap>
           <input type="text" name="fecha2" size="12" onFocus="blur();" class="tabmovtexbol" />
           <a href ="javascript:SeleccionaFecha(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle" /></a>
          </td>
         </tr>
         <tr>
          <td align="right" class="tabmovtexbol" nowrap>Tipo de movimiento:</td>
          <td>
           <select NAME="CvlMov" class="tabmovtexbol">
            <option value = "" selected>Todos</option>
            <Option value = "-">Cargo</Option>
            <Option value = "+">Abono</Option>
           </select>
          </td>
         </tr>
         <c:choose>
         	<c:when test="${tipoBanca ne 'E'}">
         <tr align="center">
          <td colspan="2">
           <table width="300" border="0" cellspacing="0" cellpadding="0">
            <tr>
             <td width="25"><input type="radio" name="TMov" Checked value="C" /></td>
             <td class="tabmovtexbol" width="75"> Cheques </td>
             <td width="25"><input type="radio" name="TMov" value="V" /></td>
             <td class="tabmovtexbol" width="75"> Vista </td>
             <td width="25"><input type="radio" name="TMov" value="D" /></td>
             <td class="tabmovtexbol" width="75"> Devueltos </td>
            </tr>
           </table>
          </td>
         </tr>
         	</c:when>
         	<c:otherwise>
    			<input type="hidden" name="TMov" value="C">
         	</c:otherwise>
         </c:choose>
         <tr>
          <td align="right" class="tabmovtexbol" nowrap>
          <c:choose>
			<c:when test="${tipoBanca eq 'E'}">
				Importe:
			</c:when>
			<c:otherwise>
				Rango de Importes:
			</c:otherwise>
			</c:choose>
          </td>
		  <td class="tabmovtexbol">
		    <c:choose>
			<c:when test="${tipoBanca eq 'E'}">
				<input type="text" name="Importe" size="12" class="tabmovtexbol"  onBlur = "return checkFloatValue(this);" />
			</c:when>
			<c:otherwise>
				De:<input type="text" name="importeDe" size="12" class="tabmovtexbol"  onBlur = "return checkFloatValue(this);" />
	          	&nbsp;&nbsp;&nbsp;&nbsp;A:<input type="text" name="importeA" size="12" class="tabmovtexbol"  onBlur = "return checkFloatValue(this); " />
			</c:otherwise>
			</c:choose>
          </td>
         </tr>
         <tr>
          <td align="right" class="tabmovtexbol" nowrap>Referencia/Cheque:</td>
          <td><input type="text" name="Refe" size="12" class="tabmovtexbol" maxlength="7" onBlur = "checkIntegerValue(this);" /></td>
         </tr>
        </table>
       </td>
      </tr>
     </table>
     <br />
     <table width="160" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
	   <tr id="enviar2" align="center" style="visibility:hidden" class="tabmovtex">
		 <td colspan="2">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
	   </tr>
	   <tr id="enviar">
       <td align="right" style="height: 25px;" ><a href="javascript:DoPost();  "><img src="/gifs/EnlaceMig/gbo25220.gif" style="border: 0px;"/></a></td>
       <td align="left"  style="height: 25px;"><a href="javascript:FrmClean();"><img src="/gifs/EnlaceMig/gbo25250.gif" style="border: 0px;"/></a></td>
      </tr>
     </table>
    </td>
   </tr>
    </td>
   </tr>
   <input type="Hidden" name="FechaI" value="" />
   <input type="Hidden" name="FechaF" value="" />
   <input type="Hidden" name="Banca"  value="<%if( request.getAttribute("tipoBanca")!=null ) out.print(request.getAttribute("tipoBanca")); else out.print(""); %>">
  </form>
  </table>
 </body>
 <script> ctaPorParametro(); </script>

	<head>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
	<META HTTP-EQUIV="Expires" CONTENT="-1" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
	<META HTTP-EQUIV="Expires" CONTENT="-1" />
	</head>
</html>