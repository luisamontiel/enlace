<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="mx.altec.enlace.jms.mq.conexion.MQQueueSession"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pagina de prueba conexiones Enlace</title>
</head>
<body>

<%	/**
	PAGINA DE PRUEBA PARA CONEXIONES DE BASE DE DATOS Y MQ
	AUTOR: Emmanuel Sanchez Castillo
	FECHA: 30/12/2008
	*/

	out.println("PAGINA DE PRUEBA CONECTIVIDAD ENLACE<BR><BR><BR>");
	java.sql.Connection con = null;

	out.flush();
	out.println("PROBANDO DATASOURCE " + mx.altec.enlace.utilerias.Global.DATASOURCE_ORACLE + "<BR>");
	out.flush();
	try{
	con = mx.altec.enlace.servlets.BaseServlet.createiASConn_static(mx.altec.enlace.utilerias.Global.DATASOURCE_ORACLE);
	}catch(Exception e)
	{
		e.printStackTrace();
		out.println("HA OCURRIDO UN PROBLEMA EN EL DATASOURCE  " + mx.altec.enlace.utilerias.Global.DATASOURCE_ORACLE + "<BR>");
		out.println(e.getMessage().toString()+ "<BR>");
	}finally{

	if(con == null)
	{
		out.println("ERROR EN LA CONEXION.<BR><BR>");
	}else{
		out.println("CONEXION EXITOSA!!!<BR><BR>");
		try
		{
			con.close();
			con = null;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	}
	out.flush();
	out.println("PROBANDO DATASOURCE " + mx.altec.enlace.utilerias.Global.DATASOURCE_ORACLE2 + "<BR>");
	out.flush();
	try{
	con = mx.altec.enlace.servlets.BaseServlet.createiASConn_static2(mx.altec.enlace.utilerias.Global.DATASOURCE_ORACLE2);
	}catch(Exception e)
	{
		e.printStackTrace();
		out.println("HA OCURRIDO UN PROBLEMA EN EL DATASOURCE  " + mx.altec.enlace.utilerias.Global.DATASOURCE_ORACLE2 + "<BR>");
		out.println(e.getMessage()+ "<BR>");
	}finally{

		if(con == null)
		{
			out.println("ERROR EN LA CONEXION.<BR><BR>");
		}else{
			out.println("CONEXION EXITOSA!!!.<BR><BR>");
			try
			{
				con.close();
				con = null;
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	out.flush();
	out.println("PROBANDO DATASOURCE CONEXION CON MQ<BR>");
	out.flush();
	mx.altec.enlace.servicios.ServicioTux servicioTux = new mx.altec.enlace.servicios.ServicioTux();

	try{
		java.util.Hashtable respuesta = servicioTux.longitudPassword("01001851");
		out.println(respuesta + "<BR>");
		out.println("CONEXION EXITOSA!!!!<BR>");

	}catch(Exception e){
		e.printStackTrace();
		out.println("HA OCURRIDO UN PROBLEMA EN LA CONEXION CON MQ <BR>");
		out.println(e.getMessage()+ "<BR>");
	}
	out.flush();
	out.println("PROBANDO CONEXION MQ DE 390<BR>");
	out.flush();

	try {
			MQQueueSession mqTux = new MQQueueSession(mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY,
					mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE,
					mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST);

			String trama = request.getHeader("trama");
			if (trama == null) trama = request.getParameter("trama");
			
			if (trama == null) {
			    //trama = "    DEIFNOMRGL3100781123451O00N2                SI    NO                      ";
				//trama = "    DEIFNOMRLZS802981123451O00N260800239630                      60800239640            0000000001023002547                                                                              Z1C4WYVH4B0U21FDO0326000ABMN000759X00297JL0064AGJ324W                                                            ";
			    //trama = "    DEIFNOMRGL3500341123451O00N2  ";
				//trama = "    DEIFNOMRGL2100341123451O00N2  ";
				//trama = "    DEIFNOMRGL2400651123451O00N2ALC  8000064837600000000001234500";
				//trama = "    DEIFCFRMPE8000721000011O00N2                65001601115         000 ";
				//trama = "    DEIFCFRMPE8000721000011O00N2                00000000010         000 ";
				//trama = "    DEIFNOMRB48500551000011O00N260523295711           ";
				trama = "    DEIFNOMRPE6809451123451O00N201001851";
			} else {
				trama = trama.substring(1, trama.length() - 1);
				out.println("Trama de Prueba: <input type='text' style='width: 700px;' value='" + trama + "'/> <BR>");
			}
			
			out.println("Trama: [" + trama + "]");
			
			String respuesta = mqTux.enviaRecibeMensaje(trama);
			out.println("RespuestaMQ: [" + respuesta+"] <BR>");
		}catch(Exception e)
		{
			String strError = e.getMessage();
			out.println( "ERROR MQ: [" + strError + "] <BR><BR>");

		}
	out.println("FIN PRUEBA DE CONECTIVIDAD!!!!<BR>");
	out.flush();

%>

</body>
</html>