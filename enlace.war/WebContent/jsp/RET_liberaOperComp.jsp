<%@ page import="mx.altec.enlace.bo.RET_OperacionVO"%>
<jsp:directive.page import="mx.altec.enlace.bo.RET_OperacionVO"/>
<jsp:useBean id='facultadesAltaCtas' class='java.util.HashMap' scope='session'/>
<%@ page import="java.util.*"%>
<%@page import="EnlaceMig.*"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%
	RET_OperacionVO beanOper = (RET_OperacionVO) session.getAttribute("beanOperacionRET");
	String divisaCargo = (String) request.getAttribute("divisaCargo");
	String divisaAbono = (String) request.getAttribute("divisaAbono");
%>

<html>
<head>
<title>Complemento y Liberaci�n de Operaciones</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>

<!-- Estilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
.componentesHtml {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
}
</style>


<script language="JavaScript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var opcion;
var opc;
var tramadicional;
var cfm;

function continua(){

}

function PresentarCuentas()
{
  opcion=1;
  opc=1;
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&tipoOp=FXMB","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function CuentasMismoBanco()
{
	var varTipoCuenta;
	if(document.frmRetLiberaOperCom.tipoCuenta.length!=undefined){
		for (var x = 0; x< document.frmRetLiberaOperCom.tipoCuenta.length; x++){
			if(document.frmRetLiberaOperCom.tipoCuenta[x].checked)
				varTipoCuenta = document.frmRetLiberaOperCom.tipoCuenta[x].value;
		}
	}else
		varTipoCuenta = "1";

	if(varTipoCuenta == "2"){
		cuadroDialogo("Es incorrecta la opci�n seleccionada para insertar cuenta.",11);
	}else if(varTipoCuenta == "1"){
		opcion=2;
		opc=2;
	//	msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&tipoOp=FXMB","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
		msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=2&tipoOp=FXMB","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
			
	msg.focus();
	}else{
		cuadroDialogo("Debe seleccionar la opci�n de tipo de cuenta.",11);
	}

}

function CuentasInternacionales()
{
	var varTipoCuenta;
	for (var x = 0; x< document.frmRetLiberaOperCom.tipoCuenta.length; x++){
		if(document.frmRetLiberaOperCom.tipoCuenta[x].checked)
			varTipoCuenta = document.frmRetLiberaOperCom.tipoCuenta[x].value;
	}
	if(varTipoCuenta == "1"){
		cuadroDialogo("Es incorrecta la opci�n seleccionada para insertar cuenta.",11);
	}else if(varTipoCuenta == "2"){
		opcion=2;
		opc=3;
		msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=2&tipoOp=FXIN","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
		msg.focus();
  	}else{
		cuadroDialogo("Debe seleccionar la opci�n de tipo de cuenta.",11);
	}

}

function actualizacuenta()
{
  if(opc==1){
	  document.frmRetLiberaOperCom.selCuentaCargo.value=ctaselec;
	  document.frmRetLiberaOperCom.selDesCuentaCargo.value=ctadescr;
      document.frmRetLiberaOperCom.textCuentaCargo.value=ctaselec+" "+ctadescr;
  }else if(opc==2){
 	  document.frmRetLiberaOperCom.selCtaMB.value=ctaselec;
 	  document.frmRetLiberaOperCom.selDesCtaMB.value=ctadescr;
      document.frmRetLiberaOperCom.textselCtaMB.value=ctaselec+" "+ctadescr;
  }else if(opc==3){
  	  document.frmRetLiberaOperCom.selCtaInter.value=ctaselec;
  	  document.frmRetLiberaOperCom.selDesCtaInter.value=ctatipre;
  	  document.frmRetLiberaOperCom.selDivCtaInter.value=ctadescr;
  	  document.frmRetLiberaOperCom.selABACtaInter.value=ctasubprod;
  	  document.frmRetLiberaOperCom.selSWIFTCtaInter.value=ctaserfi;
      document.frmRetLiberaOperCom.textselCtaInter.value=ctaselec+" "+ctatipre;
  }
}

function insertarCuenta(){
	var forma = document.frmRetLiberaOperCom;
	var ctaMBVacia = false;
	var ctaInterVacia = false;
	var sinError = true;
	if(trim(document.frmRetLiberaOperCom.selCtaMB.value) == "")
		ctaMBVacia = true;

	if(trim(document.frmRetLiberaOperCom.selCtaInter.value) == "")
		ctaInterVacia = true;

	if(ctaMBVacia && ctaInterVacia){
		cuadroDialogo("Debe seleccionar una cuenta Mismo Banco o internacional al menos.",11);
		sinError = false;
	}

	if(!ctaMBVacia && (trim(document.frmRetLiberaOperCom.textCuentaCargo.value)==trim(document.frmRetLiberaOperCom.textselCtaMB.value))){
		cuadroDialogo("La cuenta de Abono Mismo Banco no puede ser igual a la cuenta de cargo.",11);
		sinError = false;
	}

	if(sinError){
		forma.action = "RET_LiberaOper";
		forma.opcion.value = "insertarCuenta";
		forma.submit();
	}
}

function trim(cadena){
	var str = "" + cadena;
	while(str.substring(0,1) == ' ') str = str.substring(1);
	while(str.substring(str.length-1) == ' ') str = str.substring(0,str.length-1);
	return str;
}

function HabilitaCamposValidos(){
	var varTipoCuenta;
	for (var x = 0; x< document.frmRetLiberaOperCom.tipoCuenta.length; x++){
		if(document.frmRetLiberaOperCom.tipoCuenta[x].checked)
			varTipoCuenta = document.frmRetLiberaOperCom.tipoCuenta[x].value;
	}
	if(varTipoCuenta == "1"){
		document.frmRetLiberaOperCom.textselCtaInter.disabled = true;
		document.frmRetLiberaOperCom.textselCtaInter.value = "";
		document.frmRetLiberaOperCom.textselCtaMB.disabled = false;
	}else if(varTipoCuenta == "2"){
		document.frmRetLiberaOperCom.textselCtaMB.disabled = true;
		document.frmRetLiberaOperCom.textselCtaMB.value = "";
		document.frmRetLiberaOperCom.textselCtaInter.disabled = false;
	}
}

function Limpiar(){
	var forma = document.frmRetLiberaOperCom;
	forma.action = "RET_LiberaOper";
	forma.opcion.value = "limpiar";
	forma.submit();
}

function BajaRegistro(){
	var forma = document.frmRetLiberaOperCom;
	var contador = 0;

	for(j=0;j<forma.length;j++)
		if(forma.elements[j].type=='checkbox')
			if(forma.elements[j].checked)
		  		contador++;

	if(contador==0){
		cuadroDialogo("Debe seleccionar al menos un abono de cuenta.",11);
	}else{
		var listaDefinitivaMB = "";
		var listaDefinitivaIB = "";

		//Validaci�n Cuentas Mismo Banco
		//if(document.frmRetLiberaOperCom.flagSelec.value=="1"){ QUITAR PARA EL MULTIABONO
		if(document.frmRetLiberaOperCom.chkCuentaMB){
			if(document.frmRetLiberaOperCom.chkCuentaMB.length !== undefined ){
				for(i = 0; i < document.frmRetLiberaOperCom.chkCuentaMB.length; i++){
					if(document.frmRetLiberaOperCom.chkCuentaMB[i].checked){
						if(i==0){
							listaDefinitivaMB = document.frmRetLiberaOperCom.chkCuentaMB[i].value + "|" + listaDefinitivaMB;
						}else{
							listaDefinitivaMB = listaDefinitivaMB + "|" + document.frmRetLiberaOperCom.chkCuentaMB[i].value;
						}
					}
				}
			}else{
				if(document.frmRetLiberaOperCom.chkCuentaMB.checked){
					listaDefinitivaMB = document.frmRetLiberaOperCom.chkCuentaMB.value + "|" + listaDefinitivaMB;
				}
			}
		}

		//Validaci�n Cuentas Internacionales
		//if(document.frmRetLiberaOperCom.flagSelec.value=="2"){ QUITAR PARA EL MULTIABONO
		if(document.frmRetLiberaOperCom.chkCuentaIB){
			if(document.frmRetLiberaOperCom.chkCuentaIB.length !== undefined ){
				for(i = 0; i < document.frmRetLiberaOperCom.chkCuentaIB.length; i++){
					if(document.frmRetLiberaOperCom.chkCuentaIB[i].checked== true){
						if(i==0){
							listaDefinitivaIB = document.frmRetLiberaOperCom.chkCuentaIB[i].value + "|" + listaDefinitivaIB;
						}else{
							listaDefinitivaIB = listaDefinitivaIB + "|" + document.frmRetLiberaOperCom.chkCuentaIB[i].value;
						}
					}
				}
			}else{
				if(document.frmRetLiberaOperCom.chkCuentaIB.checked== true){
					listaDefinitivaIB = document.frmRetLiberaOperCom.chkCuentaIB.value + "|" + listaDefinitivaIB;
				}
			}
		}

		forma.operSelMB.value = listaDefinitivaMB;
		forma.operSelIB.value = listaDefinitivaIB;
		forma.action = "RET_LiberaOper";
		forma.opcion.value = "bajaRegistro";
		forma.submit();
	 }
}

function Registrar(){
	var forma = document.frmRetLiberaOperCom;
	var sinError = true;
	var control;
	var count = 0;
	var impTot = 0;

	if(trim(document.frmRetLiberaOperCom.textCuentaCargo.value) == ""){
		document.frmRetLiberaOperCom.textCuentaCargo.focus();
		cuadroDialogo("La Cuenta de Cargo es un dato obligatorio.",11);
		sinError = false;
	}

	if(trim(document.frmRetLiberaOperCom.concepto.value) == ""){
		document.frmRetLiberaOperCom.concepto.focus();
		cuadroDialogo("El concepto es un dato obligatorio.",11);
		sinError = false;
	}

	if(!EsAlfaNumericaEs(document.frmRetLiberaOperCom.concepto.value)){
		document.frmRetLiberaOperCom.concepto.focus();
		cuadroDialogo("El concepto no es valido.",11);
		sinError = false;
	}

	var count = document.frmRetLiberaOperCom.numAbonos.value;
    for(i=0; i<count; i++){
    	control = 'impIdRegistroAbono' + i;
    	impTot += parseFloat(document.getElementById(control).value);
    }

	var num1 = new Number(impTot);
	impTot 	 = num1.toFixed(2);

    var importeOperacion = document.frmRetLiberaOperCom.importeTotOper.value;
    if(impTot!=importeOperacion){
        cuadroDialogo("El importe total de abono no corresponde al importe de la operaci�n.",11);
     	sinError = false;
    }

	if(sinError){
		forma.action = "RET_LiberaOper";
		forma.opcion.value = "registrar";
		forma.submit();
	}
}

function Enviar(){
	var forma = document.frmRetLiberaOperCom;
	forma.action = "RET_LiberaOper";
	forma.opcion.value = "enviar";
	forma.submit();
}

function Caracter_EsNumerico(caracter){
	var ValorRetorno = true;
	if( !(caracter >= "0" && caracter <= "9") ) ValorRetorno = false;
	return ValorRetorno;
}

function Caracter_EsAlfa(caracter)
{
	var ValorRetorno = true;

	if ( !(caracter >= "A" && caracter <= "Z") ) //Si no es Es Alpha Mayusculas
		if( !(caracter >= "&Aacute;" && caracter <= "&Uacute;") )
			if ( !(caracter >= "a" && caracter <= "z") ) // Si no es Alpha Minusculas
				if( !(caracter >= "&aacute;" && caracter <= "&uacute;") )
						ValorRetorno = false;
	return ValorRetorno;
}

function EsAlfaNumerica(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				retorno = false;
	}
	return retorno;
}

function EsAlfaNumericaEs(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				if (caracter!=" ")
					retorno = false;
	}
	return retorno;
}

function EsAlfa(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			retorno = false;
	}
	return retorno;
}

function EsAlfaEs(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (caracter!=" ")
				retorno = false;
	}
	return retorno;
}

function valImporteAbono(importe){
	var cantidad = importe.value;
	var nomCantidad = importe.name;
	var impTot = 0;
	var impTotStr = "";
	var control;
	var count = 0;

   	if (isNaN(cantidad)){
  		document.getElementById(nomCantidad).value="0";
     	cuadroDialogo("Favor de ingresar un valor num\351rico en el IMPORTE",11);
     	return false;
    }

	if(cantidad.indexOf (".") == 0) {
	   cantidad = "0" + cantidad;
	   document.getElementById(nomCantidad).value = cantidad;
	   }
	pos_punto = cantidad.indexOf (".");

    if(pos_punto>-1 && (cantidad.length-pos_punto)>3){
    	document.getElementById(nomCantidad).value="0";
    	cuadroDialogo("S\363lo se aceptan hasta dos decimales en el importe", 11);
    	return false;
    }else if(pos_punto==-1){
    	cantidad += ".00";
    	document.getElementById(nomCantidad).value = cantidad;
    }else if(pos_punto>-1 && (cantidad.length-pos_punto)==1){
    	cantidad += "00";
    	document.getElementById(nomCantidad).value = cantidad;
    }else if(pos_punto>-1 && (cantidad.length-pos_punto)==2){
    	cantidad += "0";
    	document.getElementById(nomCantidad).value = cantidad;
    }

    count = document.frmRetLiberaOperCom.numAbonos.value;
    for(i=0; i<count; i++){
    	control = 'impIdRegistroAbono' + i;
    	impTot += parseFloat(document.getElementById(control).value);
    }

	var num1 = new Number(impTot);
	impTot 	 = num1.toFixed(2);

    impTotStr = "" + impTot;
    pos_punto = impTotStr.indexOf (".");
    if(pos_punto==-1){
    	impTotStr += ".00";
    }else if(pos_punto>-1 && (impTotStr.length-pos_punto)==1){
    	impTotStr += "00";
    }else if(pos_punto>-1 && (impTotStr.length-pos_punto)==2){
    	impTotStr += "0";
    }
    document.frmRetLiberaOperCom.impTotalAbono.value = impTotStr;
}



<%= request.getAttribute("newMenu")%>
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	bgcolor="#ffffff"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
	background="/gifs/EnlaceMig/gfo25010.gif">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*"><%= request.getAttribute("MenuPrincipal")%></td>
	</tr>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10"
			height="1"></td>
		<td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10"
			height="1"></td>
	</tr>
</table>
<form name="frmRetLiberaOperCom" enctype="multipart/form-data"
	method=post action="RET_LiberaOper">
<table cellSpacing=0 cellPadding=0 align=center border=0 width="613">
	<tr>
		<td>
		<table ALIGN=center border=0 cellpadding=2 cellspacing=3>
			<table cellpadding='2' cellspacing='1' width="80%" align="center">
				<tr>
					<td colspan=4 class="texenccon">
					<center><img src="/gifs/EnlaceMig/leyenda_paso_2_de_3.gif"
						width="250" height="35"></center>
					</td>
				</tr>
			</table>
			<table width="660" border="0" cellspacing="2" cellpadding="3">
				<tr>
					<td class="tabmovtex">* El llenado de estos campos es
					obligatorio</td>
				</tr>
				<tr>
					<td class="tittabdat" colspan="4" width='100%'><b>
					Complemento de Operaciones FX Online </td>
				</tr>
				<tr>
					<td class='textabdatcla' colspan=4 width='100%'>
					<table id="tabla" border=0 class='textabdatcla' cellspacing=3
						cellpadding=2>
						<tr>
							<td class="tabmovtex" width=10><br>
							</td>
							<td class='tabmovtex11' width=100>Divisa Operante:</td>
							<td class="tabmovtex"><input type="text" name=divisaOperante
								class="componentesHtml" maxlength=22 size=22
								value="<%=beanOper.getDivisaOperante()%>" readonly="readonly">
							</td>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">Contra Divisa:</td>
							<td class="tabmovtex"><input type="text" name=contraDivisa
								class="componentesHtml" maxlength=40 size=22
								value="<%=beanOper.getContraDivisa()%>" readonly="readonly">
							</td>
						</tr>
						<tr>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">Divisa de Cargo:</td>
							<td class="tabmovtex"><input type="text" name=divCargo
								class="componentesHtml" maxlength=40 size=22
								value="<%=divisaCargo%>" readonly="readonly"></td>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">Divisa de Abono:</td>
							<td class="tabmovtex"><input type="text" name=divAbono
								class="componentesHtml" maxlength=40 size=22
								value="<%=divisaAbono%>" readonly="readonly"></td>
						</tr>
						<tr>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">Tipo de Cambio Pactado:</td>
							<td class="tabmovtex"><input type="text" name=tcPactado
								class="componentesHtml" maxlength=40 size=22
								value="<%=beanOper.getTcPactado()%>" readonly="readonly">
							</td>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">Importe Operaci�n:</td>
							<td class="tabmovtex"><input type="text" name=importeTotOper
								class="componentesHtml" maxlength=40 size=22
								<%java.text.DecimalFormat format = new java.text.DecimalFormat("0.00"); %>
								value="<%=format.format(Double.parseDouble(beanOper.getImporteTotOper()))%>"
								readonly="readonly"></td>
						</tr>
						<tr>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">Ultima Fecha Complemento:</td>
							<td class="tabmovtex"><input type="text"
								name=fchHrComplemento class="componentesHtml" maxlength=40
								size=22 value="<%=beanOper.getFchHoraComplemento()%>"
								readonly="readonly"></td>
						</tr>
						<tr>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">* Cuenta de Cargo:</td>
							<td class='tabmovtex'><input type="text"
								name=textCuentaCargo class="tabmovtexbol" maxlength=22 size=22
								onfocus="blur();"
								value="<%=beanOper.getCtaCargo()%> <%=beanOper.getDesCtaCargo()%>"
								class='tabmovtex'> <A
								HREF="javascript:PresentarCuentas();"><IMG
								SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
							<input type="hidden" name="selCuentaCargo" value=""> <input
								type="hidden" name="selDesCuentaCargo" value=""></td>
							<td class="tabmovtex"><br>
							</td>
						</tr>
						<tr>
							<td class="tabmovtex"><br>
							</td>
							<td class="tabmovtex11">* Concepto:</td>
							<td class="tabmovtex"><input type="text" name=concepto
								class="componentesHtml" maxlength=40 size=40
								value="<%=beanOper.getConcepto()%>"></td>
						</tr>
						<tr>
							<td class="tabmovtex" colspan=3><br>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="tittabdat" colspan="4" width='100%'><b> Cuentas
					de Abono </td>
				</tr>
				<tr>
					<td class='textabdatcla' colspan=4 width='100%'>
					<table id="tabla" border=0 class='textabdatcla' cellspacing=2
						cellpadding=3 align=center>
						<tr bgcolor=#A4BEE4>
							<td align=center width="350" class=tittabdat>
							<%if(!divisaAbono.trim().equals("MXP")){%> <input type="RADIO"
								name="tipoCuenta" value="1" onClick="HabilitaCamposValidos();">
							<%}%> Cuenta Mismo Banco</td>
							<td align=center width="350" class=tittabdat>
							<%if(!divisaAbono.trim().equals("MXP")){%> <input type="RADIO"
								name="tipoCuenta" value="2" onClick="HabilitaCamposValidos();">
							<%}%> Cuenta Internacional y Otros Bancos Nacionales</td>
						</tr>
						<tr>
							<td align=center><input type="text" name=textselCtaMB
								class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();"
								value=""> <A HREF="javascript:CuentasMismoBanco();"><IMG
								SRC="/gifs/EnlaceMig/gbo25420.gif" width="12" height="14"
								border="0" align="absmiddle" alt="Cuentas Mismo Banco"></A> <input
								type="hidden" name="selCtaMB" value=""> <input
								type="hidden" name="selDesCtaMB" value=""></td>
							<td align=center><input type="text" name=textselCtaInter
								class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();"
								value=""> <%if(!divisaAbono.trim().equals("MXP")){%> <A
								HREF="javascript:CuentasInternacionales();"><IMG
								SRC="/gifs/EnlaceMig/gbo25420.gif" width="12" height="14"
								border="0" align="absmiddle" alt="Cuentas Internacional"></A>
							<%}%> <input type="hidden" name="selCtaInter" value=""> <input
								type="hidden" name="selDesCtaInter" value=""> <input
								type="hidden" name="selDivCtaInter" value=""> <input
								type="hidden" name="selABACtaInter" value=""> <input
								type="hidden" name="selSWIFTCtaInter" value=""></td>
						</tr>
						<tr>
							<td align=center colspan=2><A
								HREF="javascript:insertarCuenta();"><IMG
								SRC="/gifs/EnlaceMig/gbo25425.gif" border=0
								alt="Insertar Cuenta"></A></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<br>
		</table>
		</td>
	</tr>
</table>
<%if(request.getAttribute("tablaValDatos")!=null){
			out.print(request.getAttribute("tablaValDatos"));
		}%> <%if(divisaAbono.trim().equals("MXP")){%> <input type="hidden"
	name="tipoCuenta" value="1"> <%}%> <input type=hidden name=opcion>
<input type=hidden name=operSelMB> <input type=hidden
	name=operSelIB> <input type="hidden" name="idRegistroAbono"
	value='<%= request.getAttribute("idRegistroAbono") %>'> <br>
<%if(beanOper.getDetalleAbono()!=null && beanOper.getDetalleAbono().size()>0){ %>
<table align=center border=0 cellspacing=0 cellpadding=0>
	<tr valign=top>
		<td width=105><br>
		</td>
		<td align=center valign="top"><A href="javascript:Limpiar();"
			border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0></a></td>
		<td align=center valign="top"><A
			href="javascript:BajaRegistro();" border=0><img
			src="/gifs/EnlaceMig/gbo25500.gif" border=0></a></td>
		<td align=center valign="top"><A href="javascript:Registrar();"
			border=0><img src="/gifs/EnlaceMig/Paso_3_de_3.gif" border=0></a></td>
	</tr>
</table>
<%}%>
</form>
<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
	%>
</body>
</html>
<%!
	/** Devuelve un texto que representa un valor monetario */
	private String aMoneda(String num){
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return num;
	}
%>