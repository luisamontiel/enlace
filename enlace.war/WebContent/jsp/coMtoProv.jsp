<!--%@ page import="EnlaceMig.*" %-->
<%@ page import="mx.altec.enlace.dao.*"%>
<%@ page import="mx.altec.enlace.bo.*"%>

<%
int bandera_aux = 0;
String parTransImp = (String)request.getAttribute("transImp");
String parFecAct = (String)request.getAttribute("fecAct");
String parRegTot = (String)request.getAttribute("regTot");

String aux_parRegTot = parRegTot;

String parRegAce = (String)request.getAttribute("regAce");
String parRegRec = (String)request.getAttribute("regRec");
String parRegPtr = (String)request.getAttribute("regPtr");
String urlExportar = "javascript:exportar();";

if(parTransImp == null) parTransImp = "";
if(parFecAct == null) parFecAct = "";
if(parRegTot == null) parRegTot = "";
if(parRegAce == null) parRegAce = "";
if(parRegRec == null) parRegRec = "";
if(parRegPtr == null) parRegPtr = "0";

ArchivoConf objArchivo = (ArchivoConf)session.getAttribute("objetoArchivo");
if(objArchivo != null) bandera_aux = 1;
if(objArchivo != null) parRegTot = "" + objArchivo.getNumProveedores();
if(objArchivo == null) objArchivo = new ArchivoConf("");
if(objArchivo.getEstado() == objArchivo.SIN_ENVIAR) parRegPtr = "" + objArchivo.getNumProveedores();
if(objArchivo.getEstado() != objArchivo.SIN_ENVIAR) urlExportar = (String)request.getAttribute("urlExportar");

String RegAuxtotales = (String)request.getAttribute("regTot_2");
if(RegAuxtotales != null )
   parRegTot = RegAuxtotales;
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Enlace</title>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html">
<script type="text/javascript" src="/EnlaceMig/fw_menu.js">		</script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js">	</script>
<script type="text/javascript">

	// --------------------------------------------------------------------------------------
	function MM_preloadImages() {	//v3.0
		var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() {	//v3.0
		var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) {		//v3.0
		var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() {		//v3.0
		var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	<%= (String) request.getAttribute("newMenu") %>
	// --------------------------------------------------------------------------------------

	// funciones para cuadro de captura y bot�n de crear archivo
	var campoTexto;
	var respuesta;
	var pregunta;

	// ---
	function continuaCaptura()
		{
		if(pregunta == "creaArchivo")
			{
			if(validaArchivo(campoTexto) && respuesta == "1")
				{
				document.MtoProveedores.archivo_actual.value=campoTexto;
				document.MtoProveedores.accion.value = "11";
				document.MtoProveedores.submit();
				}
			else if(respuesta == "1")
				{
				cuadroCapturaL("El nombre de archivo no es v&aacute;lido.<br>Intente de nuevo u oprima cancelar","Crear archivo",
					campoTexto);
				}
			}
		else if(pregunta == "recuperaArchivo" && respuesta == "1")
			{
			if((""+parseInt(campoTexto)) == campoTexto && respuesta == "1")
				{
				document.MtoProveedores.numProv.value = campoTexto;
				document.MtoProveedores.accion.value = "7";
				document.MtoProveedores.submit();
				}
			else if(respuesta == "1")
				{
				cuadroCapturaL("N&uacute;mero de secuencia no es v&aacute;lido.<br>Intente de nuevo u oprima cancelar",
					"Recuperar archivo", campoTexto);
				}
			}
		}

	function continua()
		{
		if(pregunta == "borraRegistro" && respuesta == "1")
			{
			document.MtoProveedores.numSelec.value = seleccionado();
			document.MtoProveedores.accion.value = "5";
			document.MtoProveedores.submit();
			}
		else if(pregunta == "borraArchivo" && respuesta == "1")
			{
			document.MtoProveedores.accion.value = "8";
			document.MtoProveedores.submit();
			}
		else if(pregunta == "impresionDetallada")
			{
			if(respuesta == "1")
				{document.MtoProveedores.impresionDetallada.value = "S";}
			else
				{document.MtoProveedores.impresionDetallada.value = "N";}
			document.MtoProveedores.accion.value = "10";
			document.MtoProveedores.submit();
			}
		}

	// ---
	function cuadroCapturaL(mensaje,titulo,valor,nombre)
		{
		campoTexto ="";
		ventana=window.open('','','width=380,height=170,toolbar=no,scrollbars=no,left=210,top=225');
		ventana.document.open();
		ventana.document.write("<html>");
		ventana.document.writeln("<head>\n<title>MSG</title>\n");
		ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
		ventana.document.writeln("<script language='javaScript'>");
		ventana.document.writeln("function ValidaForma(forma,boton)");
		ventana.document.writeln("	{");
		ventana.document.writeln("	if(boton==1)");
		ventana.document.writeln("		opener.campoTexto=forma.texto.value;");
		ventana.document.writeln("	opener.respuesta=boton;");
		ventana.document.writeln("	window.close();");
		ventana.document.writeln("	opener.continuaCaptura();");
		ventana.document.writeln("	}");
		ventana.document.writeln("</scr"+"ipt>");
		ventana.document.writeln("</head>");
		ventana.document.writeln("<body bgcolor='white'>");
		ventana.document.writeln("<form enctype='multipart/form-data' name='captura' onSubmit='return ValidaForma(this,1);' method=post>");
		ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
		ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
		ventana.document.writeln("</table>");
		ventana.document.writeln("<table border=0 width=360 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
		ventana.document.writeln("<tr><td class='tabmovtex1' align=right width=50 rowspan=2><img src='/gifs/EnlaceMig/gic25020.gif' border=0></a></td>");
		ventana.document.writeln("<td class='tabmovtex1' align=center width=300><br>"+mensaje+"<br>");
		ventana.document.writeln("</tr></td>");
		ventana.document.writeln("<tr><td class='tabmovtex1' align=center><input type=text name=texto size=30 maxlength=25 class=tabmovtex value=\"" + valor + "\"><br><br></td></tr>");
		ventana.document.writeln("</table>");
		ventana.document.writeln("<table border=0 align=center cellspacing=0 cellpadding=0>");
		ventana.document.writeln("<tr><td align=right><a href='javascript:ValidaForma(document.captura,1);'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:ValidaForma(document.captura,2);'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
		ventana.document.writeln("</table>");
		ventana.document.writeln("</form>");
		ventana.document.writeln("</body>\n</html>");
		ventana.document.writeln("<script language='javaScript'>");
		ventana.document.writeln("document.captura.texto.focus();");
		ventana.document.writeln("</scr"+"ipt>");
		ventana.focus();

		}

	// ---
	function crearArchivo()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO" || estado == "RECUPERADO" || document.MtoProveedores.archivo_actual.value != "")
			{cuadroDialogo("Para crear un nuevo archivo, primero debe borrar el actual"); return;}
		campoTexto = "";
		respuesta = "1";
		pregunta = "creaArchivo";
		cuadroCapturaL("Escriba el nombre del archivo","Crear archivo", document.MtoProveedores.archivo_actual.value);
		}

	// ---
	function validaArchivo(archivo)
		{
		carsValidos="ABCDEFGHIJKLNMOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
		for(a=0;a<archivo.length;a++) {if(carsValidos.indexOf(archivo.substring(a,a+1)) == -1) return false;}
		if(archivo.indexOf(" ") != -1) return false;
		if(archivo.length > 8) return false;
		if(archivo == "") return false;
		return true;
		}

	// --------------------------------------------------------------------------------------
	// Funciones de los botones

	// -- alta
	function alta()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
			{cuadroDialogo("No puede realizarse un alta porque el archivo ya fue enviado"); return;}
		if(estado == "RECUPERADO")
			{cuadroDialogo("No puede realizarse un alta en un archivo recuperado"); return;}
		if(estado == "IMPORTADO")
			{cuadroDialogo("No puede realizarse un alta en un archivo importado"); return;}
		if(document.MtoProveedores.archivo_actual.value == "")
			{cuadroDialogo("Debe crear primero un archivo"); return;}
		if(parseInt(document.MtoProveedores.numProv.value)>29)
			{cuadroDialogo("No puede introducir m&aacute;s de 30 registros manualmente,<br>" +
				"deber&aacute; hacerlo mediante la importaci&oacute;n de archivo"); return;}

		document.MtoProveedores.accion.value = "1";
		document.MtoProveedores.submit();
		}

	// -- borrar registro
	function borraRegistro()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
			{cuadroDialogo("No puede borrarse un proveedor porque el archivo ya fue enviado"); return;}
		if(estado == "RECUPERADO")
			{cuadroDialogo("No puede borrarse un proveedor en un archivo recuperado"); return;}
		if(estado == "IMPORTADO")
			{cuadroDialogo("No puede borrarse un proveedor en un archivo importado"); return;}
		if(parseInt(document.MtoProveedores.numProv.value)==0)
			{cuadroDialogo("No existen registros para borrar"); return;}
		if(seleccionado()==-1)
			{cuadroDialogo("No se ha seleccionado un proveedor"); return;}

		pregunta = "borraRegistro";
		cuadroDialogo("Se va a borrar el proveedor seleccionado<br>&iquest;es correcto?",2);
		}

	// -- modificar
	function modificar()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
			{cuadroDialogo("No puede realizarse una modificaci&oacute;n porque el archivo ya fue enviado"); return;}
		if(estado == "RECUPERADO")
			{cuadroDialogo("No puede realizarse una modificaci&oacute;n en un archivo recuperado"); return;}
		if(estado == "IMPORTADO")
			{cuadroDialogo("No puede realizarse una modificaci&oacute;n en un archivo importado"); return;}
		if(parseInt(document.MtoProveedores.numProv.value)==0)
			{cuadroDialogo("No existen registros para modificar"); return;}
		if(seleccionado()==-1)
			{cuadroDialogo("No se ha seleccionado un proveedor"); return;}

		document.MtoProveedores.numSelec.value = seleccionado();
		document.MtoProveedores.accion.value = "2";
		document.MtoProveedores.submit();
		}

	// -- imprimir
	function imprimir()
		{
		if(parseInt(document.MtoProveedores.numProv.value)==0)
			{cuadroDialogo("No existen proveedores para imprimir"); return;}
		pregunta = "impresionDetallada";
		cuadroDialogo("Oprima \"aceptar\" si desea un reporte detallado,<br>o \"cancelar\" para un reporte simplificado",2);
		}

	// -- enviar
	function enviar()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
			{cuadroDialogo("El archivo ya fue enviado"); return;}
		if(estado == "RECUPERADO")
			{cuadroDialogo("No puede enviarse un archivo recuperado"); return;}
		if(document.MtoProveedores.archivo_actual.value == "")
			{cuadroDialogo("Debe crear primero un archivo"); return;}

		document.MtoProveedores.accion.value = "6";
		document.MtoProveedores.submit();
		}

	// -- recuperar
    //		<% if( parRegTot != null && bandera_aux == 0) parRegTot=aux_parRegTot; %>
	function recuperar()
		{
		campoTexto = "";
		respuesta = "1";
		pregunta = "recuperaArchivo";
		cuadroCapturaL("Escriba el n&uacute;mero de la secuencia","Recuperar archivo", "");
		}

	// -- borrar archivo
	function borrar()
		{
		if(document.MtoProveedores.archivo_actual.value == "" &&
			document.MtoProveedores.numProv.value == 0 &&
			(document.MtoProveedores.estadoArchivo.value == "" ||
			document.MtoProveedores.estadoArchivo.value == "SIN_ENVIAR"))
			{cuadroDialogo("No hay archivo a borrar"); return;}
		respuesta = "1";
		pregunta = "borraArchivo";
		cuadroDialogo("El archivo va a ser borrado,<br>&iquest;es correcto?",2);
		}

	// -- exportar archivo
	function exportar()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "IMPORTADO")
			{cuadroDialogo("No puede exportarse un archivo importado"); return;}
		cuadroDialogo("Para poder exportar un archivo, debe enviarse uno<br>" +
			"o recuperarse mediante un n&uacute;mero de secuencia",3);
		}

	// -- importar archivo
	function importar()
		{
		if(document.MtoProveedores.Archivo.value == "")
			{
			cuadroDialogo("Debe seleccionar un archivo",3);
			return
			}
		document.MtoProveedores.accion.value = "9";
		document.MtoProveedores.submit();
		}

	// --------------------------------------------------------------------------------------
	// utilidades
/* DAG 061009: Validacion para habilitar los radios de importacion*/
	function hayArchivo(){
		var forma = document.MtoProveedores;
		if(forma.Archivo.value.length > 0){
			<%if(((Boolean)session.getAttribute("fac_CCIMPARCHPR")).booleanValue() &&
   				((Boolean)session.getAttribute("fac_CCIMPPRINTRNAL")).booleanValue()){%>
				forma.vTProv[0].disabled = false;
				forma.vTProv[1].disabled = false;
				forma.vTProv[0].checked = true;
			<%}else if(((Boolean)session.getAttribute("fac_CCIMPARCHPR")).booleanValue() &&
	 				  !((Boolean)session.getAttribute("fac_CCIMPPRINTRNAL")).booleanValue()){%>
				forma.vTProv[0].disabled = false;
				forma.vTProv[1].disabled = true;
				forma.vTProv[0].checked = true;
			<%}else if(!((Boolean)session.getAttribute("fac_CCIMPARCHPR")).booleanValue() &&
	 					((Boolean)session.getAttribute("fac_CCIMPPRINTRNAL")).booleanValue()){%>
				forma.vTProv[0].disabled = true;
				forma.vTProv[1].disabled = false;
				forma.vTProv[1].checked = true;	 					
			<%}else if(!((Boolean)session.getAttribute("fac_CCIMPARCHPR")).booleanValue() &&
	 				   !((Boolean)session.getAttribute("fac_CCIMPPRINTRNAL")).booleanValue()){%>
				forma.vTProv[0].disabled = true;
				forma.vTProv[1].disabled = true;
				forma.vTProv[0].checked = false;	 
				forma.vTProv[1].checked = false;	 
			<%}%>			
		}
	}
	function seleccionado()
		{
		var total = document.MtoProveedores.ProvSel.length-2;
		for(a=0;a<total;a++)
			if(document.MtoProveedores.ProvSel[a].checked) break;
		if(a==total) return -1;
		return a;
		}

	function ventanaHtml(codigo)
		{
		var miVentana = window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
		miVentana.document.write(codigo);
		miVentana.focus();
		}

	// -- descargar el archivo
	function downloadFile(){
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "IMPORTADO"){
			cuadroDialogo("No puede exportarse un archivo importado");
			return;
		}

		if(estado != "ENVIADO" && estado !="RECUPERADO"){
			cuadroDialogo("Para poder exportar un archivo, debe enviarse uno<br>" +
					      "o recuperarse mediante un n&uacute;mero de secuencia",3);
	    	return;
		}
		document.MtoProveedores.accion.value = "12";
		document.MtoProveedores.submit();
	}
	
	function msgCifrado(){
		try {
			var mensajeCifrado = document.MtoProveedores.mensajeCifrado.value;
			if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
				var arreglo = mensajeCifrado.split("|");
               	var msj = arreglo[1];
                var tipo = arreglo[0];
           		cuadroDialogo( msj ,tipo);
			}
		} catch (e){};
	}	
	</script>

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	bgcolor="#ffffff"  
	onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
	background="/gifs/EnlaceMig/gfo25010.gif">

<!-- MENU PRINCIPAL -->
<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top">
		<td width="*"><%= (String) request.getAttribute("MenuPrincipal") %></td>
	</tr>
</table>

<!-- ENCABEZADO -->
<%= (String) request.getAttribute("Encabezado" ) %>

<form name="MtoProveedores" method="POST" action="coMtoProveedor" enctype="multipart/form-data">
<input type="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>">
<div align="center">
<table width="680" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td rowspan="2"><!-- Datos del archivo -->
		<table width="460" border="0" cellspacing="2" cellpadding="3">
			<tr>
				<td class="tittabdat" colspan="2">Datos del archivo</td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
				<table width="450" border="0" cellspacing="5" cellpadding="5">
					<tr valign="top">
						<td width="270" align="left" class="tabmovtex">Archivo:<br>
						<input type="text" name="archivo_actual" size="22"
							class="tabmovtex" value="<%= objArchivo.getNombre() %>"
							onFocus='blur()'></td>
						<td width="180" align="left" class="tabmovtex" nowrap>
						N&uacute;mero de transmisi&oacute;n:<br>
						<input type="text" size="22" align="rigth" class="tabmovtex"
							value="<%= objArchivo.getSecuencia() %>" name="transmision"
							onFocus='blur();'> <br>
						<br>
						Fecha de transmisi&oacute;n:<br>
						<input type="text" size="22" align="rigth" class="tabmovtex"
							value="<%= objArchivo.getFecha() %>" name="fechaTrans"
							onFocus='blur();'> <br>
						<br>
						Fecha de actualizaci&oacute;n:<br>
						<input type="text" size="22" align="rigth" class="tabmovtex"
							value="<%= parFecAct %>" name="fechaAct" onFocus='blur();'>
						<br>
						<br>
						<!-- Importe de transmisi&oacute;n:<br>
				<input type="text" size="22" align="rigth" class="tabmovtex" value="< % = parTransImp % >" name="importeTrans" onFocus='blur();'> -->
						</td>
						<td width="180" align="left" class="tabmovtex" nowrap>Total
						de registros:<br>
						<input type="text" size="22" align="rigth" class="tabmovtex"
							value="<%= parRegTot %>" name="totRegs" onFocus='blur();'>
						<br>
						<br>
						Registros aceptados:<br>
						<input type="text" size="22" align="rigth" class="tabmovtex"
							value="<%= parRegAce %>" name="aceptados" onFocus='blur();'>
						<br>
						<br>
						Registros rechazados:<br>
						<input type="text" size="22" align="rigth" class="tabmovtex"
							value="<%= parRegRec %>" name="rechazados" onFocus='blur();'>
						<br>
						<br>
						Registros por transmitir:<br>
						<input type="text" size="22" align="rigth" class="tabmovtex"
							value="<%= parRegPtr %>" name="porTransmitir" onFocus='blur();'>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>

		</td>

		<!-- Bot�n de crear archivo -->
		<td width="200" height="94" align="center" valign="middle"><A
			href="javascript:crearArchivo();" border=0><img
			src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo"></a>
		</td>
	</tr>
	<tr>
		<td align="center" valign="bottom"><!-- Cuadro de importar archivo -->
		<table width="200" border="0" cellspacing="2" cellpadding="3">
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
				<table width="180" border="0" cellspacing="5" cellpadding="0">
					<tr valign="middle">
						<td class="tabmovtex" nowrap>Importar archivo</td>
					</tr>
					<tr>
						<td nowrap><input type="file" name="Archivo" size="15" onchange="javascript:hayArchivo();"></td>
					</tr>
					<tr valign="middle">
						<td class="tabmovtex" nowrap>Opci&oacute;n:</td>
					</tr>
					<tr>
						<td class="tabmovtex" align="center" nowrap>
							<input type="RADIO" name="vTProv" value="N" checked disabled><b>Nacional</b>&nbsp;
							<input type="RADIO" name="vTProv" value="I" disabled><b>Internacional</b>
						</td>
					</tr>
					<tr align="center">
						<td class="tabmovtex" nowrap><A href="javascript:importar();"
							border=0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0
							alt="Aceptar" width="80" height="22"></a></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>

		</td>
	</tr>
	<tr>
		<td class="tabmovtex" align="left" nowrap colspan=2 width="100%">
		Si desea dar de alta m&aacute;s de 30 proveedores, debera hacerlo con
		importacion de archivo (Bot&oacute;n Browse o Buscar)</td>
	</tr>
</table>

<br>
<br>
<br>

<!-- Tabla de proveedores --> <% if(objArchivo.getNumProveedores() > 0) { %>
<TABLE class="textabdatcla" align="center" width="90%" border="0"
	cellspacing="1">
	<% if(objArchivo.getNumProveedores()>30) { %>
	<tr>
		<td class="tabmovtex" colspan="175" bgcolor="#ffffff">Total: <%= objArchivo.getNumProveedores() %>
		registros. Solo se muestran los primeros 30 registros</td>
	</tr>
	<% } %>
	<TR>
		<TD class="tabmovtex" align="left" nowrap colspan="175"
			bgcolor="#ffffff">Registro de Proveedores</TD>
	</TR>
	<TR>
		<!-- jgarcia 24/Sept/2009 -->
		<TD class="tittabdat" align="center">&nbsp;</TD>
		<TD class="tittabdat" align="center" colspan="6"><B>Clave de prov.</B></TD>
		<TD class="tittabdat" align="center" colspan="15"><B>Nombre o Raz&oacute;n Social</B></TD>
		<TD class="tittabdat" align="center" colspan="8"><B>R.F.C./ID Contribuyente/Num. Seg. Social</B></TD>
		<TD class="tittabdat" align="center" colspan="15"><B>C&oacute;digo de cliente</B></TD>
		<TD class="tittabdat" align="center" colspan="8"><B>Estatus</B></TD>
		<TD class="tittabdat" align="center" colspan="25"><B>Descripci&oacute;n de estatus</B></TD>
		<TD class="tittabdat" align="center" colspan="14"><B>Tipo de Prov.</B></TD>
		<TD class="tittabdat" align="center" colspan="18"><B>Forma de Pago</B></TD>
		<TD class="tittabdat" align="center" colspan="10"><B>Cuenta Destino/M&oacute;vil</B></TD>
		<TD class="tittabdat" align="center" colspan="10"><B>Divisa</B></TD>
		<TD class="tittabdat" align="center" colspan="25"><B>Pa&iacute;s</B></TD>
		<TD class="tittabdat" align="center" colspan="20"><B>Banco Destino</B></TD>
	</TR>

	<%
		int total, a;
			String estilo="";
			ProveedorConf prov;

			total = objArchivo.getNumProveedores();

			StringBuffer sbFormaPago = new StringBuffer("");
			for(a=0;a<total && a<30;a++) {
			estilo=(estilo.equals("textabdatcla"))?"textabdatobs":"textabdatcla";
			prov = objArchivo.obtenProv(a);
			//jgarcia - Identificar la forma de pago
			if("1".equalsIgnoreCase(prov.formaPago)){
				sbFormaPago.append("T. Mismo Banco");
			}else if("2".equalsIgnoreCase(prov.formaPago)){
				sbFormaPago.append("T. Otros Bancos");
			}else{
				sbFormaPago.append("T. Internacional");
			}
	%>
	<TR>
		<TD class="<%= estilo %>" align=center><INPUT type="Radio" name="ProvSel" value="<%= a %>"></TD>
		<TD class="<%= estilo %>" colspan="6">&nbsp;<%= prov.claveProveedor %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="15">&nbsp;<%= ((prov.tipo == prov.FISICA)?(prov.nombre + " " + prov.apellidoPaterno + " " + prov.apellidoMaterno):(prov.nombre)) %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="8">&nbsp;<%= prov.rfc + (prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor)?prov.homoclave:"")%>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="15">&nbsp;<%= prov.codigoCliente %>&nbsp;</TD>
		<TD class="<%= estilo %>" align="center" colspan="8">&nbsp;<%= prov.claveStatus %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="25">&nbsp;<%= prov.status %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="14">&nbsp;<%= (prov.INTERNACIONAL.equalsIgnoreCase(prov.origenProveedor)?"Internacional":"Nacional") %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="18">&nbsp;<%= sbFormaPago.toString() %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="10">&nbsp;<%= prov.cuentaCLABE %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="10">&nbsp;<%= prov.cveDivisa %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="25">&nbsp;<%= (prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor)?"M&eacute;xico":prov.cvePais) %>&nbsp;</TD>
		<TD class="<%= estilo %>" colspan="20">&nbsp;<%= prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor)?prov.banco:prov.descripcionBanco %>&nbsp;</TD>
	</TR>
	<%
		sbFormaPago.delete(0, sbFormaPago.length());
	}
	%>
	<TR>
		<TD class="tabmovtex" align="left" nowrap colspan="175"
			bgcolor="#ffffff">Para modificar o dar de baja un registro,
		selecci&oacute;nelo previamente</TD>
	</TR>
</TABLE>
<% } %> <br>

<!-- Botones --> <A href="javascript:alta();" border=0><img
	src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta"></a><A
	href="javascript:borraRegistro();" border=0><img
	src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro"></a><A
	href="javascript:modificar();" border=0><img
	src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar"></a><A
	href="javascript:imprimir();" border=0><img
	src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir"></a> <br>
<br>
<A href="javascript:enviar();" border=0><img
	src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar"></a><A
	href="javascript:recuperar();" border=0><img
	src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar"></a><A
	href="javascript:borrar();" border=0><img
	src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar"></a><A
	href="javascript:downloadFile();" border=0><img
	src="/gifs/EnlaceMig/gbo25230.gif" border=0 alt="Exportar"></a></div>

<input type="hidden" name="opcion" value="1"> <input
	type="hidden" name="accion"> <% {
	String estado = "";

	if (objArchivo.getEstado() == objArchivo.SIN_ENVIAR)
		estado = "SIN_ENVIAR";
	if (objArchivo.getEstado() == objArchivo.ENVIADO)
		estado = "ENVIADO";
	if (objArchivo.getEstado() == objArchivo.RECUPERADO)
		estado = "RECUPERADO";
	if (objArchivo.getEstado() == objArchivo.IMPORTADO)
		estado = "IMPORTADO";
	%> <input type="hidden" name="numProv"
	value="<%= objArchivo.getNumProveedores() %>"> <input
	type="hidden" name="nombreArchivo"
	value="<%= objArchivo.getNombre() %>"> <input type="hidden"
	name="estadoArchivo" value="<%= estado %>"> <input
	type="hidden" name="fechaArchivo" value="<%= objArchivo.getFecha() %>">
<input type="hidden" name="secuenciaArchivo"
	value="<%= objArchivo.getSecuencia() %>"> <% } %> <input
	type="hidden" name="numSelec"> <input type="hidden"
	name="impresionDetallada"> <input type="hidden" name="ProvSel"
	value="XXX"> <input type="hidden" name="ProvSel" value="XXX">
	<input type="hidden" name="downloadingFile" value="<%= urlExportar %>">

</form>

<%=
		(request.getAttribute("mensaje") != null)?
		("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
		");</script>"):""
	%>

<%=
		(request.getAttribute("mensajeImportacion") != null)?
		("<script>ventanaHtml(\"" + (String)request.getAttribute("mensajeImportacion") +
		"\");</script>"):""
	%>

</body>
</html>