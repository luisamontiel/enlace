<%@page contentType="text/html"%>
<%--
Autor: Horacio Oswaldo Ferro D�az
Pagina para desplegar el resultado de la consulta de Mancomunidad
--%>
<jsp:useBean id="ResultadosMancomunidad" scope="session" class="java.util.ArrayList"/>
<%--
<jsp:useBean id="CuentaConsulta" scope="session" class="java.lang.String"/>
<jsp:useBean id="SeleccionaTodos" scope="session" class="java.lang.Boolean"/>
<jsp:useBean id="IndexMancomunidad" scope="session" class="java.lang.Integer"/>--%>
<%
	String CuentaConsulta = (String) session.getAttribute("CuentaConsulta");
	Boolean SeleccionaTodos = (Boolean) session.getAttribute("SeleccionaTodos");
	Integer IndexMancomunidad = (Integer) session.getAttribute("IndexMancomunidad");
 %>


<%!
    public static final String ANTERIORES = "<a href='javascript:anteriores ();'>Anteriores&nbsp;";
    public static final String SIGUIENTES = "<a href='javascript:siguientes ();'>Siguientes&nbsp;";
%>
<html>
<head>
  <title>Banca Virtual</title>
  <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
  <script language='JavaScript' src='/EnlaceMig/cuadroDialogo.js'></script>
  <script language='JavaScript' src='/EnlaceMig/scrImpresion.js'></script>
  <script language='JavaScript1.2' src='/EnlaceMig/fw_menu.js'></script>
  <script language='JavaScript'>

    <%-- Inicio Java Script App --%>
    function MM_preloadImages() { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_swapImgRestore() { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }

    function MM_findObj(n, d) { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }

    function MM_swapImage() { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    <%=session.getAttribute ("newMenu")%>
    <%-- Fin Java Script App --%>

    function CheckAll() {
        // Modificacion Paula Hern�ndez
        for(i = 0;i < document.FrmAutoriza.elements.length; i++)
        if(document.FrmAutoriza.elements[i].type == "checkbox")
           document.FrmAutoriza.elements[i].checked = document.FrmAutoriza.todos.checked;
        return true;
    }

    function anteriores () {
        document.FrmAutoriza.action = "GetAMancomunidad";
        document.FrmAutoriza.OpcPag.value = "<%=mx.altec.enlace.servlets.GetAMancomunidad.PREV%>";
        document.FrmAutoriza.submit ();
    }

    function siguientes () {
        document.FrmAutoriza.action = "GetAMancomunidad";
        document.FrmAutoriza.submit ();
    }

    function autoriza (Opcion) {
//		alert("opcion = " + Opcion);
		document.FrmAutoriza.TipoOp.value = Opcion;
        if (Opcion == "1")
			document.FrmAutoriza.Autoriza.value = "1";
        if (Opcion == "2")
			document.FrmAutoriza.Autoriza.value = "2";
        if (validaFolios ()) document.FrmAutoriza.submit ();
    }

var gConfirmacion= false;
function confirmacion()
{
   cuadroDialogo("Estimado cliente la funcionalidad \"Autorizar Todas\" por el momento solo est� disponible para: Transferencias Mismo Banco, Transferencias Interbancarias, Pago de Tarjeta de Cr&eacute;dito y N&oacute;mina Prepago Individual.",12);
}

function continua()
{
		document.FrmAutoriza.TipoOp.value = "1";
		document.FrmAutoriza.Autoriza.value = "1";
		document.FrmAutoriza.Todas.value = "1";
        document.FrmAutoriza.submit ();
}


    function validaFolios () {
        var total = document.FrmAutoriza.elements.length;
        var j = 0;
        for (i = 0; i < total; i++) {
            var e = document.FrmAutoriza.elements[i];
            if (e.type == 'checkbox' && e.name != 'todos') {
                if (e.checked) {
                    j++;
                }
            }
        }

//	alert("valor j=" + j);
        if (j == 0) {
            cuadroDialogo ("Usted no ha seleccionado ninguna operaci&oacute;n.\n\nPor favor seleccione la(s) operacion(es) \npendiente(s) a modificar.",1);
            return false;
        }
        return true;
    }
  </script>
  <link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>
</head>
<body topmargin='0' leftmargin='0' marginheight='0' marginwidth='0' bgcolor='#ffffff' onload='MM_preloadImages("/gifs/EnlaceMig/gbo25131.gif","/gifs/EnlaceMig/gbo25111.gif","/gifs/EnlaceMig/gbo25151.gif","/gifs/EnlaceMig/gbo25031.gif","/gifs/EnlaceMig/gbo25032.gif","/gifs/EnlaceMig/gbo25051.gif","/gifs/EnlaceMig/gbo25052.gif","/gifs/EnlaceMig/gbo25091.gif","/gifs/EnlaceMig/gbo25092.gif","/gifs/EnlaceMig/gbo25012.gif","/gifs/EnlaceMig/gbo25071.gif","/gifs/EnlaceMig/gbo25072.gif","/gifs/EnlaceMig/gbo25011.gif")' background='/gifs/EnlaceMig/gfo25010.gif'>
  <table border='0' cellpadding='0' cellspacing='0' width='571'>
    <tr valign='top'>
      <td width='*'>
        <%=session.getAttribute ("MenuPrincipal")%>
      </td>
    </tr>
  </table>
  <%=session.getAttribute ("Encabezado")%>
  <br>
  <form onsubmit='' method='POST' action='AutorizaAMancomunidad' name='FrmAutoriza'>
  	<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
    <input type='hidden' name='Todas' value='0'>
    <input type='hidden' name='OpcPag' value='<%=mx.altec.enlace.servlets.GetAMancomunidad.NEXT%>'>
    <input type='hidden' name='Opcion' value='1'>
    <input type='hidden' name='Autoriza' value='2'>
	<input type='hidden' name='TipoOp' id="TipoOp" value=''>
    <table width='1000' border='0' cellspacing='2' cellpadding='3' class='tabfonbla'>
      <%
      if ( CuentaConsulta == null ) {
      	CuentaConsulta = "";
      }
      if (!CuentaConsulta.equals ("")) { %>
      <tr>
        <td align='right' colspan='12'>Cuenta&nbsp;:&nbsp;<%=CuentaConsulta%></td>
      </tr>
      <%}%>
      <tr>
        <td colspan='12' class='texenccon'>Total de registros&nbsp;:&nbsp;<%=ResultadosMancomunidad.size ()%></td>
      </tr>
      <tr>
        <td colspan='12' class='texenccon'>Periodo del&nbsp;<%=request.getSession ().getAttribute ("FechaIni")%>&nbsp;al&nbsp;<%=request.getSession ().getAttribute ("FechaFin")%></td>
      </tr>
      <tr>
        <td align='right' class='tittabdat'>
          Todos<%if(SeleccionaTodos.booleanValue ()) {%><input type='checkbox' name='todos' onclick='CheckAll ()'><%}%>
        </td>
        <td align='center' class='tittabdat'>
          Fecha Registro.
        </td>
        <td align='center' class='tittabdat'>
          Folio Reg.
        </td>
        <td align='center'class='tittabdat'>
          Tipo Oper.
       </td>
        <td align='center' class='tittabdat'>
          Fecha Auto.
        </td>
        <td align='center' class='tittabdat'>
          Folio Auto.
        </td>
        <td align='center' class='tittabdat'>
          Usuario 1 Registr&oacute;
        </td>
        <td align='center' class='tittabdat'>
          Descripci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Usuario 2 Autoriz&oacute;
        </td>
        <td align='center' class='tittabdat'>
          Descripci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Usuario 3 Autoriz&oacute;
        </td>
        <td align='center' class='tittabdat'>
          Descripci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Usuario 4 Autoriz&oacute;
        </td>
        <td align='center' class='tittabdat'>
          Descripci&oacute;n
        </td>

        <td align='center' class='tittabdat'>
          Cta. cargo
        </td>
        <td align='center' class='tittabdat'>
          Cta. abono/M&oacute;vil
        </td>
        <td align='center' class='tittabdat'>
          Descripci&oacute;n
        </td>

        <td align='center' class='tittabdat'>
          Importe
        </td>
        <td align='center' class='tittabdat'>
          Estado
        </td><!--lcp modificaci�n para incidencia-->
		<td align='center' class='tittabdat'>
		  Forma Aplicaci&oacute;n
		</td>
		<td align='center' class='tittabdat'>
          Observaci&oacute;n
        </td>
      </tr>

      <%
        int paginacion = 50;
        int index = IndexMancomunidad.intValue ();
        int siguiente = index + paginacion;
        int restantes = (ResultadosMancomunidad.size () - (index + paginacion)) > paginacion ? paginacion : ResultadosMancomunidad.size () - (index + paginacion);
        System.out.println ("Indice: " + index);
        System.out.println ("Restantes: " + restantes);
        java.util.ListIterator liResultados = ResultadosMancomunidad.listIterator (index);
            while (liResultados.hasNext () && index < siguiente) {
            Object o = liResultados.next ();
            try {
      %>
      <%=((mx.altec.enlace.bo.DatosManc) o).getHTMLFormat (index)%>
      <%
            } catch (Exception ex) {
				System.out.println ("****ENTRO A UN A EXCEPTION****: " );
                ex.printStackTrace ();
                break;
            }
            index++;
        }
      %>
      <tr>
        <td colspan='13' align='right' class='texenccon'>
          Operaciones Mancomunadas&nbsp;:&nbsp;<%=IndexMancomunidad.intValue () + 1%>&nbsp;-&nbsp;<%=((IndexMancomunidad.intValue () + paginacion) >= ResultadosMancomunidad.size ()) ? ResultadosMancomunidad.size () : IndexMancomunidad.intValue () + paginacion%>&nbsp;de&nbsp;<%=ResultadosMancomunidad.size ()%>
        </td>
      </tr>
      <tr>
        <td colspan='13' align='center' class='texenccon'>
          <%=IndexMancomunidad.intValue () == 0 ? "" : ANTERIORES + paginacion + "</a>&nbsp;"%><%=liResultados.hasNext () ? SIGUIENTES + restantes + "</a>" : ""%>
        </td>
      </tr>
	  <tr>
	  <td align='center' colspan='13'>
	  <br><br>
			   Ahora puede realizar el envio de las Transferencias.<br>
			   <i>Este proceso puede tardar <font color=red>varios minutos</font>, por favor espere ...</i><br><br>
	  </td>
	  </tr>
	  <tr>
        <td colspan='13' align='center' class='texenccon'>
          <a href='javascript:confirmacion();'><img border='0' src='/gifs/EnlaceMig/gbo25430b.gif' width='92' height='22' alt='Autorizar Todas'></a>
          <a href='javascript:autoriza (1);'><img border='0' src='/gifs/EnlaceMig/gbo25430.gif' width='92' height='22' alt='Autorizar'></a>
          <a href='javascript:autoriza (2);'><img border='0' src='/gifs/EnlaceMig/gbo25190.gif' width='85' height='22' alt='Cancelar'></a>
          <a href='javascript:scrImpresion ();'><img border='0' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a>
          <a href='<%=session.getAttribute ("ArchivoManc")%>'><img border='0' src='/gifs/EnlaceMig/gbo25230.gif' width='85' height='22' alt='Exportar'></a>
        </td>
      </tr>
    </table>
  </form>
<script>
<%= request.getAttribute("mensajeError") %>
</script>
</body>
</html>