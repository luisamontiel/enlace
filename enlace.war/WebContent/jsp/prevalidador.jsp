
<html>
<head>
<title>Alta de empleados por archivo</title>

<script type="text/javascript"  src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/passmark.js"></script>
<script type="text/javascript">


function importar(){
	if (document.pantallaAlta.fileName.value == ""){
				cuadroDialogo("Por favor ingrese la ruta del archivo",1);
				document.pantallaAlta.fileName.focus(); 
				return;
	}else{
		document.pantallaAlta.action = "PrevalidadorServlet?opcion=2";
		document.pantallaAlta.submit();
	}
}


/***********************************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function msgCifrado(){
	try {
		var mensajeCifrado = document.pantallaAlta.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
			var arreglo = mensajeCifrado.split("|");
               var msj = arreglo[1];
               var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

/*******************************************************************************/


<% if (request.getAttribute("newMenu") != null ) { %>
<%= request.getAttribute("newMenu") %>
<%	} %>


</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<BODY onLoad="msgCifrado();">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">
			<!-- MENU PRINCIPAL -->
			<% if (request.getAttribute("MenuPrincipal") != null ) { %>
			<%= request.getAttribute("MenuPrincipal") %>
			<%	} %>
		</TD>
	</TR>
</TABLE>

<% if (request.getAttribute("Encabezado") != null ) { %>
<%= request.getAttribute("Encabezado") %>
<%	} %>

<form ENCTYPE="multipart/form-data" name="pantallaAlta" method="POST" action="">
 <INPUT TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>">	
 <table align=center border=0 cellpadding=0 cellspacing=0 >

 <tr>
  <td>

  <table ALIGN=center BORDER=0 cellpadding=2 cellspacing=3 class='textabdatcla'>
  <tr>
    <td class='tittabdat' colspan=4>&nbsp;<b>Importar Archivo con Alta de Empleados<br></b></td>
  </tr>
  <tr>
   <td class='textabdatcla' colspan=4>
    <table border=0 class='textabdatcla' cellspacing=3 cellpadding=2 width='100%'>

     <tr>
      <td width='100%' class='tabmovtex' align=center>Importar archivo&nbsp; <input type='file' name='fileName' value=' Archivo ' ></td>
     </tr>

    </table>
   </td>
  </tr>
  </table>

   </td>
  </tr>
 </table>

  <table align=center border=0 cellspacing=0 cellpadding=0>
	<tr><br></tr>
    <tr>
     <td><A href = "javascript:importar();" ><img src = "/gifs/EnlaceMig/gbo25300.gif"  border=0 alt="Importar"></a></td>
    </tr>
  </table>
	 <script type="text/javascript">
			<% if (request.getSession().getAttribute("prevalidadorMensaje") != null ) { %>
					 cuadroDialogo('<%=request.getSession().getAttribute("prevalidadorMensaje")%>', 1);					
			<%} %>
			
			<% if (request.getSession().getAttribute("prevalidadorMensajeError") != null ) {
					request.getSession().setAttribute("prevalidadorMensaje",request.getSession().getAttribute("prevalidadorMensajeError"));
					request.getSession().removeAttribute("prevalidadorMensajeError");
			%>						 
					window.open('<%=request.getContextPath()%>/jsp/prevalidadorMensajes.jsp','','width=480,height=<%=request.getSession().getAttribute("tamanioMensaje")%>,toolbar=no,scrollbars=yes,left=325,top=325');
			<%} %>
				

	</script>
		 

 </form><br></body>
</html>