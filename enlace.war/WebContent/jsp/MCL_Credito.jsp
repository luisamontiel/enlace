<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<html>
<head>
	<title>Consultas. Saldos, Posici&oacute;n y Movimiento</TITLE>
<%-- 01/10/2002 Correci�n ortogr�fica. --%>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<script language="javaScript" type="text/javascript">

<%= request.getAttribute("Errores") %>

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/

function Posicion()
 {
   document.SaldoCred.action="MCL_Posicion";
   document.SaldoCred.submit();
 }

function Enviar()
 {
   if(ValidaCuentas(document.SaldoCred))
    {
    
 	document.getElementById("botones").style.visibility="hidden";
		document.getElementById("mensaje").style.visibility="visible";  
      document.SaldoCred.submit();
	}
 }

function SeleccionaTodos()
 {
   forma=document.SaldoCred;

   if(forma.Todas.checked==true)
    {
 	  forma.Todas.checked=true;
      for(i2=0;i2<forma.length;i2++)
       if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
         forma.elements[i2].checked=true;
    }
   else
    {
      forma.Todas.checked=false;
      for(i2=0;i2<forma.length;i2++)
       if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
         forma.elements[i2].checked=false;
    }
 }

function ValidaCuentas(forma)
 {
   var cont=0;
   var str="";

   for(i2=0;i2<forma.length;i2++)
    {
      if(forma.elements[i2].type=='checkbox')
       {
         if(forma.elements[i2].checked==true)
          {
		   forma.Cuentas.value+=forma.elements[i2].name;
           str+="1";
           cont++;
          }
         else
          str+="0";
       }
    }
   if(cont==0)
    {
      //alert("Debe Seleccionar m�nimo una cuenta.");
      cuadroDialogo("Debe Seleccionar m&#237;nimo una cuenta.",3);
      return false;
    }
   return true;
 }

<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>
<%
 if (request.getAttribute("Encabezado")!= null) {
                out.println(request.getAttribute("Encabezado"));
                }
%>
<FORM  NAME="SaldoCred" method=post action="MCL_Consultar?Modulo=0">
  <p>
   <!-- Datos del Usuario -->
   <!--
   <table border=0 align=center>
   <tr>
     <th bgcolor='#DDDDDD'><font color=black>Datos del Usuario</font></th>
   </tr>
   <tr>
     <td align="center">
       <font size="2" face="Arial, Helvetica, sans-serif">
	     <b>Contrato: </b><%= request.getAttribute("NumContrato") %> <%= request.getAttribute("NomContrato") %>
	   </font>
	 </td>
   </tr>
   <tr>
     <td bgcolor='#FFFFFF'>
	   <b>Usuario: </b><%= request.getAttribute("NumUsuario") %> <%= request.getAttribute("NomUsuario") %>
	 </td>
   </tr>
   <tr>
     <td><br></td>
   </tr>
   </table>
   //-->

   <table align=center border=0>
    <tr>
     <td><%= request.getAttribute("Tabla") %></td>
    </tr>
   </table>

  <input type=hidden name=Cuentas>
  <input type=hidden name=Modulo value=0>
  <input type=hidden name=strCuentas value='<%= request.getAttribute("strCuentas") %>'>
  <input type=hidden name=FacArchivo value='<%= request.getAttribute("FacArchivo") %>'>

  <p>
  <table align=center border=0>
  
  <tr id="mensaje" align="center"  style="visibility:hidden">
   <td colspan="5" class="tabmovtex">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
  </tr>
				
   <tr id="botones">
	<td>
		<A href="javascript:Enviar();" border=0><img src = "/gifs/EnlaceMig/gbo25220.gif" border="0" name="btnEnviar" alt="Enviar"></a>			
		    	</td>	    	
    
   </tr>                                                      
  </table>
</FORM>

</BODY>
</HTML>