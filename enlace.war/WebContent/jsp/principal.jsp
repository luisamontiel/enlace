<%@page import="mx.altec.enlace.bo.BaseResource"%>
<html>

<%--


SE SOLICITA INTEGRAR UN BOTÓN DE "EXPORTAR" INFORMACIÓN EN LA PANTALLA DE CONSULTA DE CUENTAS CLABE,
SE INTEGRA A ESTE DOCUMENTO LA SECUENCIA PARA ACCEDER A LA CONSULTA, PANTALLA ACTUAL,
PANTALLA REQUERIDA "NUEVA" Y LAYOUT DE INFORMACIÓN A ARROJAR POR EL SISTEMA.

Se agrega un método que guarda los datos de la cuenta clave en un archivo para ser mostrados
despues con el botonde exportar.



--%>

<head>
<title>Banca Virtual</title>
		<meta http-equiv="Content-Type" content="text/html;"/>
		<meta name="Codigo de Pantalla" content="s25010"/>
		<meta name="Proyecto" content="Portal"/>
		<meta name="Version" content="1.0"/>
		<meta name="Ultima version" content="27/04/2001 18:00"/>
		<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico"/>
		<%
			response.setHeader	("Cache-Control", "no-store");
			response.setHeader	("Pragma", "no-cache");
			response.setDateHeader	("Expires", 0);
		%>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" SRC="/EnlaceMig/ValidaFormas.js"></script>


<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/ConsultaCuentasSantander.js"></script>


<Script type="text/javascript">



var Nmuestra = <%= request.getAttribute( "varpaginacion" ) %>;

function atras() {
	if((parseInt(document.frmPrincipal.prev.value) - Nmuestra)>0) {
		document.frmPrincipal.next.value = document.frmPrincipal.prev.value;
		document.frmPrincipal.prev.value = parseInt(document.frmPrincipal.prev.value) - Nmuestra;
		document.frmPrincipal.action="ContCtas_AL?Modulo=0";
		document.frmPrincipal.submit();
	}
}

function atras1() {
	if(parseInt(document.frmPrincipal.prev.value) != 0) {
		if((parseInt(document.frmPrincipal.prev.value) - Nmuestra)>=0) {
			document.frmPrincipal.next.value = document.frmPrincipal.prev.value;
			document.frmPrincipal.prev.value = parseInt(document.frmPrincipal.prev.value) - Nmuestra;
			document.frmPrincipal.action="ContCtas_AL?Modulo=0";
			document.frmPrincipal.submit();
		}
	}else{
		cuadroDialogo("no hay mas registros", 1);
	}
}

function adelante()
{
	if(parseInt(document.frmPrincipal.next.value) != parseInt(document.frmPrincipal.total.value)){
		 if((parseInt(document.frmPrincipal.next.value) + Nmuestra) <= parseInt(document.frmPrincipal.total.value)) {
			document.frmPrincipal.prev.value = document.frmPrincipal.next.value;
			document.frmPrincipal.next.value = parseInt(document.frmPrincipal.next.value) + Nmuestra;
			document.frmPrincipal.action="ContCtas_AL?Modulo=0";
			document.frmPrincipal.submit();
		}
		else if((parseInt(document.frmPrincipal.next.value) + Nmuestra) > parseInt(document.frmPrincipal.total.value)) {
			document.frmPrincipal.prev.value = document.frmPrincipal.next.value;
			document.frmPrincipal.next.value = parseInt(document.frmPrincipal.next.value) + (parseInt(document.frmPrincipal.total.value) - parseInt(document.frmPrincipal.next.value));
			document.frmPrincipal.action="ContCtas_AL?Modulo=0";
			document.frmPrincipal.submit();
		}
	} else {
		cuadroDialogo("no hay mas registros", 1);
	}
}

function adelante1()
{
	if(parseInt(document.frmPrincipal.next.value) != parseInt(document.frmPrincipal.total.value))
	{
		 if((parseInt(document.frmPrincipal.next.value) + Nmuestra) <= parseInt(document.frmPrincipal.total.value))
		{
			document.frmPrincipal.prev.value = document.frmPrincipal.next.value;
			document.frmPrincipal.next.value = parseInt(document.frmPrincipal.next.value) + Nmuestra;
			document.frmPrincipal.action="ContCtas_AL?Modulo=0";
			document.frmPrincipal.submit();
		}
		else if((parseInt(document.frmPrincipal.next.value) + Nmuestra) > parseInt(document.frmPrincipal.total.value))
		{
			document.frmPrincipal.prev.value = document.frmPrincipal.next.value;
			document.frmPrincipal.next.value = parseInt(document.frmPrincipal.next.value) + (parseInt(document.frmPrincipal.total.value) - parseInt(document.frmPrincipal.next.value));
			document.frmPrincipal.action="ContCtas_AL?Modulo=0";
			document.frmPrincipal.submit();
		}
	}
	else
	{
		cuadroDialogo("no hay mas registros", 1);
	}
}


function anteriores()
   {
   	var pag = document.frmPrincipal.numPag.value;
   	pag--;
	document.frmPrincipal.action="ContCtas_AL?Modulo=0";
	document.frmPrincipal.numPag.value = pag;
	if(parseInt(document.frmPrincipal.prev1.value) != 0) {
		if((parseInt(document.frmPrincipal.prev1.value) - Nmuestra)>=0) {
			document.frmPrincipal.next1.value = document.frmPrincipal.prev1.value;
			document.frmPrincipal.prev1.value = parseInt(document.frmPrincipal.prev1.value) - Nmuestra;
		}
	}else{
		cuadroDialogo("no hay mas registros", 1);
	}
	document.frmPrincipal.submit();
}
function siguientes()
   {
   	var pag = document.frmPrincipal.numPag.value;
   	pag++;
	document.frmPrincipal.action="ContCtas_AL?Modulo=0";
	document.frmPrincipal.numPag.value = pag;
	if(parseInt(document.frmPrincipal.next1.value) != parseInt(document.frmPrincipal.regTotal.value)){
		 if((parseInt(document.frmPrincipal.next1.value) + Nmuestra) <= parseInt(document.frmPrincipal.regTotal.value)) {
			document.frmPrincipal.prev1.value = document.frmPrincipal.next1.value;
			document.frmPrincipal.next1.value = parseInt(document.frmPrincipal.next1.value) + Nmuestra;
		}
		else if((parseInt(document.frmPrincipal.next1.value) + Nmuestra) > parseInt(document.frmPrincipal.regTotal.value)) {
			document.frmPrincipal.prev1.value = document.frmPrincipal.next1.value;
			document.frmPrincipal.next1.value = parseInt(document.frmPrincipal.next1.value) + (parseInt(document.frmPrincipal.regTotal.value) - parseInt(document.frmPrincipal.next1.value));
		}
	} else {
		cuadroDialogo("no hay mas registros", 1);
	}
	document.frmPrincipal.submit();
}

</script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript">


	<%= request.getAttribute("newMenu") %>



</script>

	<link rel="stylesheet" href="EnlaceMig/consultas.css" type="text/css" />
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />

</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onLoad="activaCaja(); MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" style="background:'/gifs/EnlaceMig/gfo25010.gif';background-color:'#ffffff';" >

<%-- MENU PRINCIPAL --%>
		<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
			<TR valign="top">
				<TD width="*">
					<%-- MENU PRINCIPAL --%>
					<%= request.getAttribute("MenuPrincipal") %>
				</TD>
			</TR>
		</TABLE>
<%= request.getAttribute("Encabezado") %>
<form name="frmPrincipal" id="frmPrincipal" method="post" action="ContCtas_AL">
<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
<input type="hidden" name="total" value=<%= request.getAttribute( "total" ) %>>
<input type="hidden" name="prev"  value=<%= request.getAttribute( "prev" ) %>>
<input type="hidden" name="next"  value=<%= request.getAttribute( "next" ) %>>
<input type="hidden" name="regTotal" value=<%= request.getAttribute( "regTotal" ) %>>
<input type="hidden" name="prev1"  value=<%= request.getAttribute( "prev1" ) %>>
<input type="hidden" name="next1"  value=<%= request.getAttribute( "next1" ) %>>
<input type="hidden" name="numPag"  value=<%= request.getAttribute( "numPag" ) %>>

<br>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table width="563" border="0" cellspacing="0" cellpadding="2" align="center">
	<tr  class="tittabdat">
		<td colspan="5">Capture los datos para la consulta</td>
	</tr>
	<tr class="textabdatcla">
		<td colspan="5" class="tittabcom"><br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tipo de Cuenta:</td>
	</tr>

<%--
< >

 --%>

	<tr class="textabdatcla">
		<td colspan="5" align="center" class="tittabcom">
			<input type="radio" name="RBTipoCta" id="RBSantander" value="RBSantander" onClick="habilita('RBSantander',1);" <%if ( !"RBOtros".equals(request.getAttribute("RBTipoCta")) || !"RBExtranjeros".equals(request.getAttribute("RBTipoCta")) || !"RBMovil".equals(request.getAttribute("RBTipoCta")) ) out.println("checked='checked'");%>>
			Santander&nbsp;&nbsp;
			<input type="radio" name="RBTipoCta" id="RBOtros" value="RBOtros"  onClick="habilita('RBOtros',1);" <%if ( request.getAttribute("RBTipoCta")!=null && "RBOtros".equals(request.getAttribute("RBTipoCta")) ) out.println("checked='checked'");%> >
			Otros&nbsp;Bancos&nbsp;&nbsp;
			<input type="radio" name="RBTipoCta" id="RBExtranjeros" value="RBExtranjeros" onClick="habilita('RBExtranjeros',1);" <%if ( request.getAttribute("RBTipoCta")!=null && "RBExtranjeros".equals(request.getAttribute("RBTipoCta")) ) out.println("checked='checked'");%> >
			Bancos&nbsp;Extranjeros&nbsp;&nbsp;
			<input type="radio" name="RBTipoCta" id="RBMovil" value="RBMovil" onClick="habilita('RBMovil',1);" <%if ( request.getAttribute("RBTipoCta")!=null && "RBMovil".equals(request.getAttribute("RBTipoCta")) ) out.println("checked='checked'");%> >
			M&oacute;vil&nbsp;&nbsp;
			<br></br><br></br>
		</td>
	</tr>
	<tr><td colspan="5" style="height:5px;" ></td></tr>
	<tr class="textabdatcla">
		<td colspan="5" align="center"><br>
			Ingrese&nbsp;criterio&nbsp;de&nbsp;b&uacute;squeda&nbsp;:&nbsp;
			<input type="text" name="TxtCritBusq" id="TxtCritBusq" value=<%if(request.getAttribute("TxtCritBusq")!=null)out.println("'"+request.getAttribute("TxtCritBusq")+"'");%>  style="width:360px" disabled="">
			<br></br><br></br>
		</td>
	</tr>

	<tr class="textabdatcla">
		<td colspan="5" align="center">

			<input type="radio" name="RBCritBusq" id="Todos" value="Todos" onClick="activaCaja();" <%if ( request.getAttribute("RBCritBusq")!=null && !"NumCuenta".equals(request.getAttribute("RBCritBusq")) && !"Nombre".equals(request.getAttribute("RBCritBusq")) && !"Banco".equals(request.getAttribute("RBCritBusq")) ) out.println(" checked='checked'");%> >Todos&nbsp;&nbsp;
			<input type="radio" name="RBCritBusq" id="RBNumCuenta" value="NumCuenta" <%if ("NumCuenta".equals(request.getAttribute("RBCritBusq")) ) out.println("checked='checked'");%> onClick="activaCaja();">
			<%if ( request.getAttribute("RBTipoCta")==null || ("RBSantander".equals(request.getAttribute("RBTipoCta")) || "RBOtros".equals(request.getAttribute("RBTipoCta")) || "RBExtranjeros".equals(request.getAttribute("RBTipoCta")))){%>
			<span id="CuentaMovilEtiqueta" style="display: inline;">
				Num.Cuenta
			</span>
			<span id="CuentaMovilEtiqueta1" style="display: none;">
				Cuenta M&oacute;vil
			</span>
			<% }else{ %>
			<span id="CuentaMovilEtiqueta" style="display: none;">
				Num.Cuenta
			</span>
			<span id="CuentaMovilEtiqueta1" style="display: inline">
				Cuenta M&oacute;vil
			</span>
			<%}%>
			<input type="radio" name="RBCritBusq" id="RBNombre" value="Nombre" <%if ("Nombre".equals(request.getAttribute("RBCritBusq")) ) out.println("checked='checked'");%> onClick="activaCaja();">Nombre&nbsp;&nbsp;
			<span id="sTipo" style="display: inline;">
			</span>
	<%if ( request.getAttribute("RBTipoCta")!=null && "RBOtros".equals(request.getAttribute("RBTipoCta")) ){%>
			<span id="sOtros" style="display: inline;">

				<input type="radio" name="RBCritBusq" id="RBBanco" value="Banco" <%if ("Banco".equals(request.getAttribute("RBCritBusq")) ) out.println("checked='checked'");%> onClick="activaCaja();"/>Banco&nbsp;&nbsp;

			</span>
	<% }else{ %>
				<span id="sOtros" style="display: none;">

				<input type="radio" name="RBCritBusq" id="RBBanco" value="Banco" <%if ("Banco".equals(request.getAttribute("RBCritBusq")) ) out.println("checked='checked'");%> onClick="activaCaja();"/>Banco&nbsp;&nbsp;

			</span>
	<%}%>
			<br></br>
			<br></br>
		</td>
	</tr>
	<tr>
		<td colspan="5" align="center">
			<br>
			<a title="Consultar" href="javascript:EnviarForma();">
				<img alt="Consultar" src="/gifs/EnlaceMig/gbo25220.gif" style=" border: 0;"/>
			</a>
			<a title="Limpiar" href="javascript:LimpiarForma();">
				<img alt="Limpiar" src="/gifs/EnlaceMig/gbo25250.gif" style=" border: 0;"/>
			</a>
		</td>

	</tr>
</table>

</td></tr>

<%		if(null!=request.getAttribute("encabezadosTabla1") && request.getAttribute("encabezadosTabla1").toString().length() != 0) {%>

		<tr>
			<td>
				<table width="563" border="0" cellspacing="2" cellpadding="3" align="center" style="background-color: #FFFFFF;">
				<tr>
					<%= request.getAttribute("encabezadosTabla") %>
				</tr>
					<%= request.getAttribute("Mensaje1") %>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="563" border="0" cellspacing="2" cellpadding="3" align="center" style="background-color: #FFFFFF;">
			    <tr>
				 	<%= request.getAttribute("encabezadosTablaU") %>
				 	<%= request.getAttribute("encabezadosTabla1") %>
				</tr>
		 		 	<%= request.getAttribute("Mensaje2") %>
				</table>

				<%= request.getAttribute("paginacion") %>

			<br></br>
			<table width="760" border="0" cellspacing="2" cellpadding="3" align="left" style="background-color: #FFFFFF;">
				<tr>
				<td align="center"  colspan="2" class="tabmovtex11">
					<a style="cursor: pointer;" > <input id="tipoExportacion" type="radio" value ="txt" name="tipoExportacion"/>Exporta en TXT</a>
				</td>
			</tr>
			<tr>
				<td align="center"  colspan="2" class="tabmovtex11">
					<a style="cursor: pointer;" > <input id="tipoExportacion" type="radio" value ="csv" checked name="tipoExportacion"/>Exporta en XLS</a>
				</td>
			</tr>
				<tr>
					<td align="right">
						<a href="javascript:scrImpresion();">
							<img style=" border: 0;" alt="Imprimir" src="/gifs/EnlaceMig/gbo25240.gif" />
						</a>
					</td>
			    	<td align="left"  colspan="1">

			<%String Link;
			if (!request.getAttribute("Nombre_Archivo").toString().equals("No se puedo exportar el archivo") ) {
				Link="javascript:exportarArchivoPlano();";
				out.println("<a href='"+Link+"' >" + "\n" +
				"<img border='0' name='imageField' src='/gifs/EnlaceMig/gbo25230.gif' border=0 alt='Exportar' height='22' width='85'>"+ "\n" +
				"</a> 	\n" +
				" 		\n" +


				"<jsp:include page=\"./Exportar.jsp\" flush=\"true\"/> 			"+ "\n" +


				" ");
				}else {
					out.println("<a href = 'javascript:Exportar()' >" + "\n" +
					"<img border='0' name='imageField' src='/gifs/EnlaceMig/gbo25230.gif' border=0 alt='Exportar' height='22' width='85'>" + "\n" +
					"</a> \n");
				}
		}
			%>

		   		</td>

			</tr>

		</table>

	</td>
</tr>

</table>
	<input type="hidden" name="Modulo" value=0 />

</form>
	<%-- CONTENIDO FINAL --%>
</body>
</html>

<Script type="text/javascript">
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
</Script>