<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%
/********************************************************************************************************/
/*Modificaci�n:                                                                                         */
/*             Incidencia:IM19625                                                                       */
/*             Programador: Sergio A. L�pez Flores                                                      */
/*             Fecha: 2/12/2003                                                                         */
/********************************************************************************************************/
EIGlobal.mensajePorTrace(" -- -- -- >>" + (String)session.getAttribute("cfrm_folio"), EIGlobal.NivelLog.DEBUG);
%>

<%@ page import="mx.altec.enlace.bo.Documento" %>
<%@ page import="mx.altec.enlace.bo.ArchivoCfmg" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%!
	private String formatoNum(double d){
		int i = 0;
		Double amount = new Double(d);
		NumberFormat numberFormatter;
		String amountOut;
		numberFormatter = NumberFormat.getNumberInstance();
		amountOut = numberFormatter.format(amount);
		if(amountOut.indexOf(".")>0)
			i = (amountOut.substring(amountOut.indexOf("."), amountOut.length())).length();
		else
			i = 4;
		if(i==1)
			amountOut += "00";
		else if(i==2)
			amountOut += "0";
		else if(i==4)
			amountOut += ".00";
		return amountOut;
	}
%>

<%!
	public String ceros(String importe){
		int i = 0;
		Double amount = new Double(importe);
		NumberFormat numberFormatter;
		String amountOut;
		numberFormatter = NumberFormat.getNumberInstance();
		amountOut = numberFormatter.format(amount);
		if(amountOut.indexOf(".")>0)
			i = (amountOut.substring(amountOut.indexOf("."), amountOut.length())).length();
		else
			i = 4;
		if(i==1)
			amountOut += "00";
		else if(i==2)
			amountOut += "0";
		else if(i==4)
			amountOut += ".00";
		return amountOut;
	}
%>

<%
try
{
String urlExportar = "javascript:exportar();";
int a, b;

ArchivoCfmg objArchivo;
if(session.getAttribute("objArch")!=null)
	objArchivo = (ArchivoCfmg)session.getAttribute("objArch");
else
	objArchivo = (ArchivoCfmg)session.getAttribute("objArchivo");
EIGlobal.mensajePorTrace("coPagos.jsp> registros = >" +objArchivo.getTotalRegistros()+ "<",EIGlobal.NivelLog.DEBUG);
EIGlobal.mensajePorTrace("coPagos.jsp> total documentos = >" +objArchivo.getTotalDocumentos()+ "<",EIGlobal.NivelLog.DEBUG);
objArchivo.ordenaNombreProv();
String facultadExportar = "false";
if (request.getAttribute("facultadExportar") != null)facultadExportar = (String) request.getAttribute("facultadExportar");
if(objArchivo.getEstado() == objArchivo.RECUPERADO && facultadExportar.equals("true")) urlExportar = (String)request.getAttribute("urlExportar");
EIGlobal.mensajePorTrace("Comienza la construccion del jsp",EIGlobal.NivelLog.DEBUG);

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Enlace</title>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
	<meta http-equiv = "Content-Type" content="text/html">
	<script language = "JavaScript1.2"	src = "/EnlaceMig/fw_menu.js">		</script>
	<script language = "JavaScript"		src = "/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript"		src = "/EnlaceMig/scrImpresion.js">	</script>
	<script language = "JavaScript">

	// --------------------------------------------------------------------------------------
	function MM_preloadImages()
	{	//v3.0
		var d=document; if(d.images)
			{
			if(!d.MM_p)
				d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
		    for(i=0; i<a.length; i++)
		        if (a[i].indexOf("#")!=0)
				   {
				      d.MM_p[j]=new Image;
					  d.MM_p[j++].src=a[i];
				  }
		    }
	}

	function MM_swapImgRestore()
	{	//v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
			x.src=x.oSrc;
	}

	function MM_findObj(n, d)
	{		//v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
		      d=parent.frames[n.substring(p+1)].document;
			  n=n.substring(0,p);
		   }
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++)
			x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++)
             x=MM_findObj(n,d.layers[i].document);
		  return x;
	}

	function MM_swapImage()
	 {		//v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
		  if ((x=MM_findObj(a[i]))!=null)
			{
			   document.MM_sr[j++]=x;
			   if(!x.oSrc)
				   x.oSrc=x.src;
			   x.src=a[i+2];
			}
	}
	/*
	* jgarcia
	* Stefanini
	* se agrego funciones PresentarCuentas y actualizacuenta para seleccionar la cuenta de cargo
	*/
	function PresentarCuentas(){
	  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
	  msg.focus();
	}
	function actualizacuenta(){
	  document.MtoProveedores.Cuentas.value=ctaselec;
	  document.MtoProveedores.textCuentas.value=ctaselec+" "+ctadescr;
	}


	<%= (String) request.getAttribute("newMenu") %>
	// --------------------------------------------------------------------------------------

	<%//	int totalregs = (int)objArchivo.getTotalRegistros();
//SLF 28112003
   int totalregs=0;
   totalregs = (int)objArchivo.getTotalRegistros()>30?30:(int)objArchivo.getTotalRegistros();
   EIGlobal.mensajePorTrace("coPagos.jsp> Documentos a mostrar: "+ totalregs, EIGlobal.NivelLog.DEBUG);
//SLF 28112003

	Documento docum;
	String docs = "";
	String docsAsociados = "";
	String importesFacturas = "";
	int z;

    EIGlobal.mensajePorTrace("va a poner en vector" + totalregs, EIGlobal.NivelLog.DEBUG);
	for(z=0;z<totalregs;z++)
		{
		docum = objArchivo.getDoc(z);
		docs += ", '" + docum.getproveedor() + "|" + docum.gettipo() + "|" + docum.getnumdocto() + "'";
		importesFacturas += ", '" + docum.getproveedor() + "|" + docum.gettipo() + "|" + docum.getnumdocto() +
			"|" + ((docum.gettipo() == 1)?docum.getimporte():docum.getimporte_neto()) + "'";
		docsAsociados += ", '" + docum.getproveedor() + "|" + docum.gettipo() + "|" + docum.getnumdocto() +
			"|" + docum.getnaturaleza() + "|" + docum.getfactura_asociada() + "|" + docum.getreferencia() +
			"|" + docum.getconcepto() + "'";
		}
    EIGlobal.mensajePorTrace("puso en vector" + totalregs, EIGlobal.NivelLog.DEBUG);
	if(docs.length()>2) docs = docs.substring(2);
	if(importesFacturas.length()>2) importesFacturas = importesFacturas.substring(2);
	if(docsAsociados.length()>2) docsAsociados = docsAsociados.substring(2);
	
	
	request.getSession().setAttribute("importeCfmg", String.valueOf(objArchivo.getImpTransmitir()));
	request.getSession().setAttribute("registrosCfmg", String.valueOf(objArchivo.getTotalRegistros()));
	%>
	var numDoctos = new Array(<%=docs%>);
	var importesFacturas = new Array(<%=importesFacturas%>);
	var docsAsociados = new Array(<%=docsAsociados%>);

	//
	function yaExiste(numDoc)
	 {
		var a;
		var existe = false;
		for(a=0;a<numDoctos.length;a++)
			{if(numDoctos[a] == numDoc) existe = true;}
		return existe;
	}

	//
	function importeDeFactura(factura)
	{
		var a;
		var documento;
		var importe;
		for(a=0;a<importesFacturas.length;a++)
			{
			documento = importesFacturas[a].substring(0,importesFacturas[a].lastIndexOf("|"));
			importe = importesFacturas[a].substring(importesFacturas[a].lastIndexOf("|")+1);
			if(documento == factura) break;
			}
		if(documento != factura) importe = "-1";
		return importe;
		}

	//
	function estandarizaImporte(valor)
	{
		var pos = valor.indexOf(".");
		if(pos == -1)
			valor += "00";
		else
			{
			while(pos+3<valor.length) valor = valor.substring(0,valor.length-1);
			while(pos+3>valor.length) valor += "0";
			valor = valor.substring(0,valor.length-3) + valor.substring(valor.length-2);
			}
		while(valor.length<3) valor = "0" + valor;
		while(valor.length>3 && valor.substring(0,1) == "0") valor = valor.substring(1);
		return valor;
	}

	//
	function compara(valor1,valor2)
	{
		valor1 = estandarizaImporte(valor1);
		valor2 = estandarizaImporte(valor2);
		if(valor1.length > valor2.length) return 1;
		if(valor1.length < valor2.length) return -1;
		for(a = 0;a < valor1.length; a++)
			{
			if(parseInt(valor1.substring(a,a+1)) > parseInt(valor2.substring(a,a+1))) return 1;
			if(parseInt(valor1.substring(a,a+1)) < parseInt(valor2.substring(a,a+1))) return -1;
			}
		return 0;
		}

	//
	function suma(valor1,valor2)
	{
		var aux = "";
		var acarreo = 0, suma = 0, dig1 = 0, dig2 = 0;
		valor1 = estandarizaImporte(valor1);
		valor2 = estandarizaImporte(valor2);

		if(compara(valor1,valor2)<0) {aux = valor1; valor1 = valor2; valor2 = aux; aux = "";}
		while(valor2.length<valor1.length) valor2 = "0" + valor2;
		for(a=valor1.length-1;a>-1;a--)
			{
			dig1 = parseInt(valor1.substring(a,a+1)) + acarreo;
			dig2 = parseInt(valor2.substring(a,a+1));
			suma = dig1 + dig2;
			acarreo = 0;
			while(suma>10) {acarreo++; suma -= 10;}
			aux = suma + "" + aux;
			}
		if(acarreo > 0) {aux = acarreo + "" + aux;}
		while(aux.length > 3 && aux.substring(0,1) == "0") aux = aux.substring(1);
		while(aux.length < 3) aux = "0" + aux;
		aux = aux.substring(0,aux.length-2) + "." + aux.substring(aux.length-2);
		return aux;
		}

	//
	function resta(valor1,valor2)
		{
		var prefijo = "", aux = "";
		var deuda = 0, resta = 0, dig1 = 0, dig2 = 0;
		valor1 = estandarizaImporte(valor1);
		valor2 = estandarizaImporte(valor2);
		if(compara(valor1,valor2)<0) {aux = valor1; valor1 = valor2; valor2 = aux; prefijo = "-";}
		while(valor2.length<valor1.length) valor2 = "0" + valor2;
		aux = "";
		for(a=valor1.length-1;a>-1;a--)
			{
			dig1 = parseInt(valor1.substring(a,a+1)) - deuda;
			dig2 = parseInt(valor2.substring(a,a+1));
			deuda = 0;
			while(dig1<dig2) {dig1 += 10; deuda++;}
			resta = dig1 - dig2;
			aux = resta + "" + aux;
			}
		while(aux.length > 3 && aux.substring(0,1) == "0") aux = aux.substring(1);
		while(aux.length < 3) aux = "0" + aux;
		aux = aux.substring(0,aux.length-2) + "." + aux.substring(aux.length-2);
		return prefijo + aux;
		}

	//
	function tomaTipo(documento)
		{
		documento = documento.substring(documento.indexOf("|")+1);
		documento = documento.substring(0,documento.indexOf("|"));
		return documento;
		}

	//
	function tomaNaturaleza(documento)
		{
		documento = documento.substring(documento.indexOf("|")+1);
		documento = documento.substring(documento.indexOf("|")+1);
		documento = documento.substring(documento.indexOf("|")+1);
		documento = documento.substring(0,documento.indexOf("|"));
		return documento;
		}

	//
	function tomaFacAsociada(documento)
		{
		documento = documento.substring(documento.lastIndexOf("|")+1);
		return documento;
		}

	//
	function tomaProveedor(documento)
		{
		documento = documento.substring(0,documento.indexOf("|"));
		return documento;
		}

	//
	function tieneNotas(documento)
		{
		var tiene = false;
		var sel = seleccionado();
		for(a=0;a<docsAsociados.length;a++)
			{
			if(a == sel) continue;
			doc = docsAsociados[a];
			doc = tomaProveedor(documento) + "|1|" + tomaFacAsociada(doc);
			if(doc == documento) {tiene = true; break;}
			}
		return tiene;
		}

	// --------------------------------------------------------------------------------------
	// funciones para cuadro de captura y bot�n de crear archivo
	var campoTexto;
	var respuesta;
	var pregunta;

	// ---
	function continuaCaptura()
		{
		if(pregunta == "creaArchivo")
			{
			if(validaArchivo(campoTexto) && respuesta == "1")
				{
				document.MtoProveedores.archivo_actual.value=campoTexto;
				document.MtoProveedores.accion.value = "11";
				document.MtoProveedores.submit();
				}
			else if(respuesta == "1")
				{
				cuadroCapturaL("El nombre de archivo no es v&aacute;lido.<br>Intente de nuevo u oprima cancelar","Crear archivo",
					campoTexto);
				}
			}
		else if(pregunta == "recuperaArchivo" && respuesta == "1")
			{
			if((""+parseInt(campoTexto)) == campoTexto && respuesta == "1")
				{
				document.MtoProveedores.numProv.value = campoTexto;
				document.MtoProveedores.accion.value = "7";
				document.MtoProveedores.submit();
				}
			else if(respuesta == "1")
				{
				-("N&uacute;mero de secuencia no es v&aacute;lido.<br>Intente de nuevo u oprima cancelar",
					"Recuperar archivo", campoTexto);
				}
			}
		}

	function continua()
		{
		if(pregunta == "borraRegistro" && respuesta == "1")
			{
			document.MtoProveedores.numSelec.value = seleccionado();
			document.MtoProveedores.accion.value = "5";
			document.MtoProveedores.submit();
			}
		else if(pregunta == "borraArchivo" && respuesta == "1")
			{
			document.MtoProveedores.accion.value = "8";
			document.MtoProveedores.submit();
			}
		}

	// ---
	function cuadroCapturaL(mensaje,titulo,valor)
		{
		campoTexto ="";
		ventana=window.open('','','width=380,height=170,toolbar=no,scrollbars=no,left=210,top=225');
		ventana.document.open();
		ventana.document.write("<html>");
		ventana.document.writeln("<head>\n<title>MSG</title>\n");
		ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
		ventana.document.writeln("<script language='javaScript'>");
		ventana.document.writeln("function ValidaForma(forma,boton)");
		ventana.document.writeln("	{");
		ventana.document.writeln("	if(boton==1)");
		ventana.document.writeln("		opener.campoTexto=forma.texto.value;");
		ventana.document.writeln("	opener.respuesta=boton;");
		ventana.document.writeln("	window.close();");
		ventana.document.writeln("	opener.continuaCaptura();");
		ventana.document.writeln("	}");
		ventana.document.writeln("</scr"+"ipt>");
		ventana.document.writeln("</head>");
		ventana.document.writeln("<body bgcolor='white'>");
		ventana.document.writeln("<form enctype='multipart/form-data' name='captura' onSubmit='return ValidaForma(this,1);' method=post>");
		ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
		ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
		ventana.document.writeln("</table>");
		ventana.document.writeln("<table border=0 width=360 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
		ventana.document.writeln("<tr><td class='tabmovtex1' align=right width=50 rowspan=2><img src='/gifs/EnlaceMig/gic25020.gif' border=0></a></td>");
		ventana.document.writeln("<td class='tabmovtex1' align=center width=300><br>"+mensaje+"<br>");
		ventana.document.writeln("</tr></td>");
		ventana.document.writeln("<tr><td class='tabmovtex1' align=center><input type=text name=texto size=30 maxlength=25 class=tabmovtex value=\"" + valor + "\"><br><br></td></tr>");
		ventana.document.writeln("</table>");
		ventana.document.writeln("<table border=0 align=center cellspacing=0 cellpadding=0>");
		ventana.document.writeln("<tr><td align=right><a href='javascript:ValidaForma(document.captura,1);'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:ValidaForma(document.captura,2);'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
		ventana.document.writeln("</table>");
		ventana.document.writeln("</form>");
		ventana.document.writeln("</body>\n</html>");
		ventana.focus();
		ventana.document.captura.texto.focus();
		}

	// ---
	function crearArchivo()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO" || estado == "RECUPERADO" || document.MtoProveedores.archivo_actual.value != "")
			{cuadroDialogo("Para crear un nuevo archivo, primero debe borrar el actual"); return;}
		campoTexto = "";
		respuesta = "1";
		pregunta = "creaArchivo";
		cuadroCapturaL("Escriba el nombre del archivo","Crear archivo", document.MtoProveedores.archivo_actual.value);
		}

	// ---
	function validaArchivo(archivo)
		{
		carsValidos="ABCDEFGHIJKLNMOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
		for(a=0;a<archivo.length;a++)
           {
			if(carsValidos.indexOf(archivo.substring(a,a+1)) == -1)
				return false;
		   }
		if(archivo.indexOf(" ") != -1) return false;
		if(archivo.length > 8) return false;
		if(archivo == "") return false;
		return true;
		}

	// --------------------------------------------------------------------------------------
	// Funciones de los botones

	// -- alta
	function alta()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
			{
			   cuadroDialogo("No puede realizarse un alta porque el archivo ya fue enviado");
			   return;
			}
		if(estado == "RECUPERADO")
			{
			  cuadroDialogo("No puede realizarse un alta en un archivo recuperado");
			  return;
			 }
		if(estado == "IMPORTADO")
			{
			  cuadroDialogo("No puede realizarse un alta en un archivo importado"); return;
			}
		if(document.MtoProveedores.archivo_actual.value == "")
			{
			  cuadroDialogo("Debe crear primero un archivo");
			  return;
			}
		/*
		* jgarcia
		* Stefanini
		* Se agrega condici�n para validar que se haya especificado una cuenta de cargo
		*/
		if(document.MtoProveedores.Cuentas.value == "")
			{
			  cuadroDialogo("No existe cuenta de cargo registrada en Confirming");
			  return;
			}
		if(parseInt(document.MtoProveedores.numProv.value)>29)
			{
			  cuadroDialogo("No puede introducir m&aacute;s de 30 registros manualmente,<br>" +
				"deber&aacute; hacerlo mediante la importaci&oacute;n de archivo"); return;
		 	}

		 document.MtoProveedores.accion.value = "1";
	 	 document.MtoProveedores.submit();
		}

	// -- borrar registro
	function borraRegistro()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
			{cuadroDialogo("No puede borrarse un pago porque el archivo ya fue enviado"); return;}
		if(estado == "RECUPERADO")
			{cuadroDialogo("No puede borrarse un pago en un archivo recuperado"); return;}
		if(estado == "IMPORTADO")
			{cuadroDialogo("No puede borrarse un proveedor en un archivo importado"); return;}
		if(parseInt(document.MtoProveedores.numProv.value)==0)
			{cuadroDialogo("No existen registros para borrar"); return;}
		if(seleccionado()==-1)
			{cuadroDialogo("No se ha seleccionado un registro"); return;}

		var documento = docsAsociados[seleccionado()];
		var facAsoDoc = tomaProveedor(documento) + "|1|" + tomaFacAsociada(documento);

		if(parseInt(tomaTipo(documento))>=3 && tomaNaturaleza(documento) == 'A' &&
			resta(importeDeFactura(facAsoDoc),importeDeFactura(numDoctos[seleccionado()])).substring(0,1)=="-")
			{
			cuadroDialogo("Si se borra esta factura, la resta de su importe (" +
				importeDeFactura(numDoctos[seleccionado()]) + ") del importe de la factura (" +
				importeDeFactura(facAsoDoc) + ") resultar&iacute;a en un importe negativo");
			return;
			}

		if(tomaTipo(documento) == 1 && tieneNotas(numDoctos[seleccionado()]))
			{
			cuadroDialogo("La factura tiene notas asociadas, primero deben borrarse estas notas.");
			return;
			}

		pregunta = "borraRegistro";
		cuadroDialogo("Se va a borrar el registro seleccionado<br>&iquest;es correcto?",2);
		}

	// -- modificar
	function modificar()
		{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
			{cuadroDialogo("No puede realizarse una modificaci&oacute;n porque el archivo ya fue enviado"); return;}
		if(estado == "RECUPERADO")
			{cuadroDialogo("No puede realizarse una modificaci&oacute;n en un archivo recuperado"); return;}
		if(estado == "IMPORTADO")
			{cuadroDialogo("No puede realizarse una modificaci&oacute;n en un archivo importado"); return;}
		if(parseInt(document.MtoProveedores.numProv.value)==0)
			{cuadroDialogo("No existen registros para modificar"); return;}
		if(seleccionado()==-1)
			{cuadroDialogo("No se ha seleccionado un registro"); return;}

		document.MtoProveedores.numSelec.value = seleccionado();
		document.MtoProveedores.accion.value = "2";
		document.MtoProveedores.submit();
		}

	// -- imprimir
	function imprimir()
		{
		if(parseInt(document.MtoProveedores.numProv.value)==0)
			{
			  cuadroDialogo("No existen registros para imprimir");
			  return;
			}
		document.MtoProveedores.accion.value = "10";
		document.MtoProveedores.submit();
		}

	// -- enviar
	function enviar()
	{
		var estado = document.MtoProveedores.estadoArchivo.value;
		if(estado == "ENVIADO")
		{
			cuadroDialogo("El archivo ya fue enviado");
			return;
		}
		if(estado == "RECUPERADO")
		{
			cuadroDialogo("No puede enviarse un archivo recuperado");
			return;
		}
		if(document.MtoProveedores.archivo_actual.value == "")
		{
			cuadroDialogo("Debe crear primero un archivo");
			return;
		}

		document.MtoProveedores.accion.value = "6";
		document.MtoProveedores.submit();
	}
	// -- recuperar
	function recuperar()
		{
		campoTexto = "";
		respuesta = "1";
		document.MtoProveedores.Cuentas.value == "";
		pregunta = "recuperaArchivo";
		cuadroCapturaL("Escriba el n&uacute;mero de secuencia","Recuperar archivo", "");
		}

	// -- borrar archivo
	function borrar()
		{
		if(document.MtoProveedores.archivo_actual.value == "" &&
			document.MtoProveedores.numProv.value == 0 &&
			(document.MtoProveedores.estadoArchivo.value == "" ||
			document.MtoProveedores.estadoArchivo.value == "SIN_ENVIAR"))
			{cuadroDialogo("No hay archivo a borrar"); return;}
		respuesta = "1";
		pregunta = "borraArchivo";
		cuadroDialogo("El archivo va a ser borrado,<br>&iquest;es correcto?",2);
		}

	// -- exportar archivo
	function exportar()
		{
			var estado = document.MtoProveedores.estadoArchivo.value;
			if(estado != "RECUPERADO"){
				cuadroDialogo("Para poder exportar un archivo, debe recuperarse uno<br>" +
					"mediante un n&uacute;mero de secuencia",3);
				return;
			}else if(estado == "RECUPERADO"){
				cuadroDialogo("Ud no tiene facultades para realizar la exportacion de archivos.",3);
				return;
			}
		}

	// -- importar archivo
	function importar()
		{
		 if(document.MtoProveedores.Archivo.value == "")
		   {
			 cuadroDialogo("Debe seleccionar un archivo",3);
			 return
		   }else if(document.MtoProveedores.Cuentas.value == ""){//jgarcia - Stefanini - Valida que se haya seleccionado una cuenta de cargo
		   	 cuadroDialogo("No existe cuenta de cargo registrada en Confirming",3);
			 return
		   }
		 document.MtoProveedores.accion.value = "9";
		 document.MtoProveedores.submit();
	  }

	// --------------------------------------------------------------------------------------
	// utilidades
	function seleccionado()
		{
		var total = document.MtoProveedores.ProvSel.length-2;
		for(a=0;a<total;a++)
			if(document.MtoProveedores.ProvSel[a].checked) break;
		if(a==total) return -1;
		return a;
		}

	function ventanaHtml(codigo)
		{
		var miVentana = window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
		miVentana.document.write(codigo);
		miVentana.focus();
		}
	
	function msgCifrado(){
		try {
			var mensajeCifrado = document.MtoProveedores.mensajeCifrado.value;
			if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
				var arreglo = mensajeCifrado.split("|");
               	var msj = arreglo[1];
               	var tipo = arreglo[0];
           		cuadroDialogo( msj ,tipo);
			}
		} catch (e){};
	}
	
	</script>

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"  
onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

	<!-- MENU PRINCIPAL -->
	<table border="0" cellpadding="0" cellspacing="0" width="571">
	       <tr valign="top"><td width="*">
		       <%=(String) request.getAttribute("MenuPrincipal")%></td></tr>
	</table>

	<!-- ENCABEZADO -->
	<%=(String) request.getAttribute("Encabezado" )%>
	<%
		//jgarcia Inicio
		//Stefanini
		// Obtenci�n de la cuenta de cargo y cuenta del cliente para mostrarlas en los campos correspondientes.
			String cuentaCargo = (String)session.getAttribute("cuentaCargo");
			if(null == cuentaCargo)
				cuentaCargo = "";
			String cuentaCliente = (String)session.getAttribute("cuentaCliente");
			if(null == cuentaCliente)
				cuentaCliente = "";
		//jgarcia Fin
	%>
	<form  name="MtoProveedores" method="POST"
          action="coPagos" enctype="multipart/form-data">
	<input type="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>">
	<div align="center">
	<table width="680" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td rowspan="2">
		<!-- Datos del archivo -->
		<table width="460" border="0" cellspacing="2" cellpadding="3">
		<tr><td class="tittabdat" colspan="2"> Datos del archivo</td></tr>
		<tr align="center">
			<td class="textabdatcla" valign="top" colspan="2">
			<table width="450" border="0" cellspacing="5" cellpadding="5">
			<tr valign="top">
				<td width="270" align="left" class="tabmovtex">
				Archivo:<br>
				<input type="text" name="archivo_actual" size="22" class="tabmovtex" value="<%=objArchivo.getNombre()%>" onFocus='blur()'>
				</td>
				<td width="180" align="left" class="tabmovtex" nowrap>
					N&uacute;mero de transmisi&oacute;n:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=objArchivo.getNumTransmision()%>"
						name="transmision" onFocus='blur();'>
					<br><br>Fecha de transmisi&oacute;n:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=objArchivo.getFechaTransmision()%>"
						name="fechaTrans" onFocus='blur();'>
					<br><br>Fecha de actualizaci&oacute;n:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=objArchivo.getFechaTransmision()%>"
						name="fechaAct" onFocus='blur();'>
					<br><br>Folio de autorizaci&oacute;n:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=(String)session.getAttribute("cfrm_folio")%>"
						name="importeTrans" <%=(((String)session.getAttribute("cfrm_folio_hab")).equals("inhabilitado"))?"onFocus='blur();'":""%>>
				</td>
				<td width="180" align="left" class="tabmovtex" nowrap>
					Total de registros:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=objArchivo.getTotalRegistros()%>"
						name="totRegs" onFocus='blur();' >
					<br><br>Registros aceptados:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=objArchivo.getAceptados()%>"
						name="aceptados" onFocus='blur();' >
					<br><br>Registros rechazados:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=objArchivo.getRechazados()%>"
						name="rechazados" onFocus='blur();' >
					<br><br>Registros por transmitir:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=objArchivo.getParaTransmitir()%>"
						name="porTransmitir" onFocus='blur();' >
				</td>
				<td width="180" align="left" class="tabmovtex" nowrap>
					Importe Total:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=ceros(objArchivo.getImporteTotal())%>"
						name="totRegs" onFocus='blur();' >
					<br><br>Importe Aceptado:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=formatoNum(objArchivo.getImpAceptado())%>"
						name="aceptados" onFocus='blur();' >
					<br><br>Importe Rechazado:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=formatoNum(objArchivo.getImpRechazado())%>"
						name="rechazados" onFocus='blur();' >
					<br><br>Importe por transmitir:<br>
					<input type="text" size="22" align="rigth" class="tabmovtex" value="<%=formatoNum(objArchivo.getImpTransmitir())%>"
						name="porTransmitir" onFocus='blur();' >
				</td>
			</tr>
			<!-- jgarcia - Stefanini - Se agrega opci�n para capturar la cuenta de cargo -->
			<tr>
			    <td class='tabmovtex'>
					Cuenta Cargo
				</td>
				<td class='tabmovtex' colspan="3">
				    <input type="text" name=textCuentas  class="tabmovtexbol" maxlength=22 size=35 onfocus="blur();" value="<%=cuentaCliente%>" class='tabmovtex' onFocus='blur();'>
					<input type="hidden" name="Cuentas" value="<%=cuentaCargo%>" >
					<%
						if(objArchivo.getTotalRegistros() == 0){
					%>
						<!--A HREF="javascript:PresentarCuentas();"--><!--IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle--><!--/A-->
					<%
						}
					%>
				</td>
		    </tr>
			</table>
			</td>
		</tr>
		</table>
		</td>
		<!-- Bot�n de crear archivo -->
		<td width="200" height="94" align="center" valign="middle">
		<A href = "javascript:crearArchivo();" border = 0>
		   <img src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo"></a>
		</td>
	</tr>
	<tr>
		<td align="center" valign="bottom">

		<!-- Cuadro de importar archivo -->
		<table width="200" border="0" cellspacing="2" cellpadding="3">
		<tr align="center">
			<td class="textabdatcla" valign="top" colspan="2">
			<table width="180" border="0" cellspacing="5" cellpadding="0">
			<tr valign="middle"><td class="tabmovtex" nowrap> Importar archivo</td></tr>
			<tr><td nowrap>
			  <input type="file" name="Archivo" size="15">
			</td></tr>
			<tr align="center">
				<td class="tabmovtex" nowrap>
				<A href = "javascript:importar();" border = 0>
			    <img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		</table>

		</td>
	</tr>
	<tr>
		<td class="tabmovtex" align = "left" nowrap colspan = 2 width="100%">
		Si desea dar de alta m&aacute;s de 30 documentos, debera hacerlo con importacion de archivo (Bot&oacute;n Browse o Buscar)
		</td>
	</tr>
	</table>

	<br><br><br>

	<!-- Tabla de proveedores -->
	<%
		if(objArchivo.getTotalRegistros() > 0) {
	%>
	<TABLE class="textabdatcla" align="center" width = "90%" border="0" cellspacing="1">
	<%
		if(objArchivo.getTotalRegistros()>30) {
	%><tr><td class="tabmovtex" colspan="18" bgcolor = "#ffffff">
	Total: <%=objArchivo.getTotalRegistros()%> registros. Solo se muestran los primeros 30 registros</td></tr>
	<%
		}
	%>
	<TR>
		<TD class="tabmovtex" align = "left" nowrap colspan = "18" bgcolor = "#ffffff">Registro de Pagos</TD>
	</TR>

	<TR>
		<TD class = "tittabdat" align = "center" > &nbsp;</TD>
		<TD class = "tittabdat" align = "center" ><B>Clave de prov.</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Nombre o Raz&oacute;n Social</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Tipo de docto.</B></TD>
		<TD class = "tittabdat" align = "center" ><B>N&uacute;mero de docto.</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Importe de docto.</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Factura asociada</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Importe a pagar</B></TD>
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de REFERENCIA INTERBANCARIA por N�MERO DE REFERENCIA -->											
		<TD class = "tittabdat" align = "center" ><B>N&uacute;mero de Referencia</B></TD>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->											
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de CONCEPTO INTERBANCARIO por CONCEPTO DEL PAGO -->											
		<TD class = "tittabdat" align = "center" ><B>Concepto del Pago</B></TD>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->											
		<TD class = "tittabdat" align = "center" ><B>Forma Aplicaci&oacute;n</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Fecha de emisi&oacute;n</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Fecha de vencimiento</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Estatus</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Descripci&oacute;n de estatus</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Cuenta cargo</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Divisa</B></TD>
		<TD class = "tittabdat" align = "center" ><B>Forma de Pago</B></TD>
	</TR>

	<%
		int total;
		String estilo="";
		Documento doc;

		total = (int)objArchivo.getTotalRegistros();
		StringBuffer sbFormaPago = new StringBuffer("");
		for(a=0;a<total && a<30;a++){
			estilo=(estilo.equals("textabdatcla"))?"textabdatobs":"textabdatcla";
			doc = objArchivo.getDoc(a);
			//jgarcia - Identificar la forma de pago
			if("1".equalsIgnoreCase(doc.gettipoCuenta())){
				sbFormaPago.append("Mismo Banco");
			}else if("2".equalsIgnoreCase(doc.gettipoCuenta())){
				sbFormaPago.append("Otros Bancos");
			}else if("5".equalsIgnoreCase(doc.gettipoCuenta())){
				sbFormaPago.append("Internacional");
			}
	%>
	<TR>
		<TD class="<%= estilo %>" align=center nowrap>
		<INPUT type="Radio" name="ProvSel" value="<%= a %>"></TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.getclaveProv() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap>&nbsp;<%= doc.getnombreProv() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.jspTipo() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.jspNumdocto() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="right">&nbsp;<%= doc.jspImporte_neto() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.jspFactura_asociada() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="right">&nbsp;<%= doc.jspImporte() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.getreferencia() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="left">&nbsp;<%= doc.getconcepto() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="left">&nbsp;<%= doc.jspAplicacion() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.jspFch_emis() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.jspFch_venc() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.jspCve_status() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap>&nbsp;<%= doc.jspStatus() %>&nbsp;</td>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= cuentaCargo %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= doc.getDivisa() %>&nbsp;</TD>
		<TD class="<%= estilo %>" nowrap align="center">&nbsp;<%= sbFormaPago.toString() %>&nbsp;</TD>
	</tr>
	<%
		sbFormaPago.delete(0, sbFormaPago.length());
	} %>
	<tr>
		<td class="tabmovtex" align = "left" nowrap colspan = "18" bgcolor = "#ffffff"> Para modificar o dar de baja un registro, selecci&oacute;nelo previamente </td>
	</tr>
	</TABLE>
	<% } %>

	<br>

	<!-- Botones -->
	<a href = "javascript:alta();" border=0>
	   <img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta"></a>
	<a href = "javascript:borraRegistro();" border=0>
	   <img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro"></a>
	<a href = "javascript:modificar();" border=0>
	   <img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar"></a>
	<a href = "javascript:imprimir();" border=0>
	   <img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir"></a>
	<br>
	<br>
	<a href = "javascript:enviar();" border=0>
	   <img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar"></a>
	<a href = "javascript:recuperar();" border=0>
	   <img src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar"></a>
	<a href = "javascript:borrar();" border=0>
	   <img src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar"></a>
	<a href = "<%= urlExportar %>" border=0>
		<img src="/gifs/EnlaceMig/gbo25230.gif" border=0 alt="Exportar"></a>
	</div>

	<input type="hidden" name="opcion" value="1">
	<input type="hidden" name="accion" value="0">

	<% {
	String estado = "";
	switch(objArchivo.getEstado())
		{
		 case ArchivoCfmg.SIN_ENVIAR: estado = "SIN_ENVIAR"; break;
		 case ArchivoCfmg.ENVIADO: estado = "ENVIADO"; break;
		 case ArchivoCfmg.RECUPERADO: estado = "RECUPERADO"; break;
		 case ArchivoCfmg.IMPORTADO: estado = "IMPORTADO"; break;
		}
	%>
	<input type="hidden" name="numProv" value="<%= objArchivo.getTotalRegistros() %>">
	<input type="hidden" name="nombreArchivo" value="<%= objArchivo.getNombre() %>">
	<input type="hidden" name="estadoArchivo" value="<%= estado %>">
	<input type="hidden" name="fechaArchivo" value="<%= objArchivo.getFechaTransmision() %>">
	<input type="hidden" name="secuenciaArchivo" value="<%= objArchivo.getNumTransmision() %>">
	<% } %>

	<input type="hidden" name="numSelec">
	<input type="hidden" name="ProvSel" value="XXX">
	<input type="hidden" name="ProvSel" value="XXX">
  </form>

<%
 }
 catch(Exception e)
 {
   e.printStackTrace();
 /*System.out.println("\n\n\n\n ERROR: " + e.trace() + "\n\n\n\n");*/
 }
%>
  <%=
      (request.getAttribute("mensaje") != null)?
	   ("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
		");</script>"):""
  %>

 <%=
 	(request.getAttribute("mensajeImportacion") != null)?	("<script>ventanaHtml(\"" + (String)request.getAttribute("mensajeImportacion") + "\");</script>"):""
  %>

</body>
<head><META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
</head>
</html>