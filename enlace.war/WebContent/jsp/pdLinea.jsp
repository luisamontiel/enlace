<%@page contentType="text/html"%>


<jsp:useBean id='fechaLib' class='java.util.GregorianCalendar' scope='session'/>
<jsp:useBean id='fechaLim' class='java.util.GregorianCalendar' scope='session'/>
<jsp:useBean id='Pago' class='mx.altec.enlace.bo.pdPago' scope='request'/>
<jsp:useBean id='Beneficiarios' class='java.util.ArrayList' scope='session'/>
<jsp:useBean id='Cuentas' class='java.util.ArrayList' scope='session'/>
<jsp:useBean id='Mensaje' class='java.lang.String' scope='request'/>
<jsp:useBean id='Permisos' class='java.util.HashMap' scope='session'/>

<%@ page import="mx.altec.enlace.utilerias.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.sql.*" %>

<%!
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");
    boolean escribe = false;
    String NumPago = "0";
%>
<%
	double d = 0.00;
	d = Double.parseDouble(NumPago);
	NumPago = String.valueOf(d++);

    escribe = !Pago.getCuentaCargo ().equals ("");
	if( request.getAttribute("Escibe" ) != null)
	escribe = true;
%>

<html>
<head>
<title>Enlace Registro de Pago Directo en L&iacute;nea</title>
<script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<script languaje="javaScript">

var fechaLib;
var fechaLim;
var modulo = 'Registro';

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/

function VerificaFechas(txtLibramiento, txtLimite)
 {

	lib_year         = parseInt(txtLibramiento.value.substring(6,10),10);
	lib_month        = parseInt(txtLibramiento.value.substring(3, 5),10);
	lib_date         = parseInt(txtLibramiento.value.substring(0, 2),10);
	fechaLibramiento = new Date(lib_year, lib_month, lib_date);

	lim_year     = parseInt(txtLimite.value.substring(6,10),10);
	lim_month    = parseInt(txtLimite.value.substring(3, 5),10);
	lim_date     = parseInt(txtLimite.value.substring(0, 2),10);
	fechaLimite  = new Date(lim_year, lim_month, lim_date);

     if( fechaLibramiento > fechaLimite)
      {
		//alert("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.");
		cuadroDialogo("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.", 3);
		return false;
	}

	return true;

}


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.frmRegistro.CuentaCargo.value=ctaselec;
}

<%= request.getAttribute("newMenu") %>

function MuestraCalendario () {
	if(modulo == 'Registro')
    fechaLib = document.frmRegistro.FechaLibramiento.value;
	else
		fechaLim= document.frmRegistro.FechaLimite.value;
    msg=window.open("jsp/pdCalendarioPag.jsp","Calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}

function continua () {
	fecha = "" + fechaLim;
	if (fecha != "undefined") {
	    if (modulo == 'Registro') {
	        document.frmRegistro.FechaLimite.value = fechaLim;
	    } else {
	        document.frmRegistro.FechaLibramiento.value = fechaLim;
	    }
	}
}
function nofacultad (mensaje) {
	cuadroDialogo ("No tienen facultad para " + mensaje, 3);
}

function concep(){

	var concepto = document.frmRegistro.Concepto.value;
	var length = concepto.length;
	var i;
	var car;
	var validas = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	for (i = 0; i < length; i++) {
		car = concepto.charAt (i);
		if(validas.indexOf(car) == -1){
			cuadroDialogo("El Concepto no debe contener caracteres especiales", 3);
			document.frmRegistro.Concepto.value="";
			break;
		}
	}
}
function ben_no(){
	var nombe=document.frmRegistro.Ben_no_reg.value;
	document.frmRegistro.Ben_no_reg.value=nombe.toUpperCase();
	var length = nombe.length;
	var i;
	var car;
	var validas = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	for (i=0;i<length;i++){
		car = nombe.charAt(i);
		if(validas.indexOf(car) == -1){
			cuadroDialogo("El nombre del beneficiario no debe contener caracteres especiales",3);
			document.frmRegistro.Ben_no_reg.value="";
		}
	}

}

function EstableceEstado() {
	if (document.frmRegistro.Clave[1].checked) {
		document.frmRegistro.Arch.disabled = false;
		document.frmRegistro.Arch.focus();

	} else{
		document.frmRegistro.Arch.disabled = true;
		document.frmRegistro.Arch.value="";
	}
}
function QuitaFoco() {
	if(document.frmRegistro.Clave[0].checked){
        document.frmRegistro.Arch.blur();
		}


}

function validaForma () {
    var importe = document.frmRegistro.Importe.value;
	var Sucursal = document.frmRegistro.Arch.value;
    var length = importe.length;
    var numpuntos = 0;
    indicePunto = document.frmRegistro.Importe.value.indexOf('.');
	if (document.frmRegistro.Clave[1].checked) {
		if(document.frmRegistro.Arch.value==""){
			cuadroDialogo ("Debe especificar una clave de sucursal.", 3);
			return false;
		}else{
			if(Sucursal=="0" || Sucursal == "00" || Sucursal == "000" || Sucursal == "0000"){
				cuadroDialogo ("La clave de la sucursal no es v&aacute;lida",3);
				return false;
			}
			if(isNaN(Sucursal)){
				cuadroDialogo ("El formato de la clave de sucursal debe ser num&eacute;rico.",3);
				return false;
			}

			document.frmRegistro.ClaveSucursal.value=Sucursal;
		}
    }else
		document.frmRegistro.ClaveSucursal.value="Todas";

	if ((document.frmRegistro.NomBen.selectedIndex == 0 || document.frmRegistro.NomBen.selectedIndex == -1) && document.frmRegistro.Ben_no_reg.value == "") {
        cuadroDialogo ("Debe seleccionar un beneficiario.", 3);
        return false;
    }

	if (document.frmRegistro.NomBen.selectedIndex != 0  && document.frmRegistro.Ben_no_reg.value != ""){
		cuadroDialogo ("S&oacute;lo debe haber un beneficiario para el pago.",3);
		return false;
	}
	if(document.frmRegistro.Ben_no_reg.value != ""){
		if(document.frmRegistro.clv_no_reg.value == ""){
			cuadroDialogo ("Debe ingresar una clave del beneficiario",3);
			return false;
		}
	}
	if(document.frmRegistro.NomBen.selectedIndex !=0 && document.frmRegistro.clv_no_reg.value != ""){
		cuadroDialogo("Para un beneficiario registrado el campo clave debe estar vac&iacute;o.",3);
		return false;
	}
    if (importe.length == 0) {
        cuadroDialogo("Debe especificar un IMPORTE v&aacute;lido.", 3);
        return false;
    }
    if(indicePunto < 0)
        strEnteros = document.frmRegistro.Importe.value;
	else
		strEnteros = document.frmRegistro.Importe.value.substring(0, indicePunto);
    if( strEnteros.length > 13 ) {
        cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
		return false;
	}
    for (i = 0; i < length; i++) {
        car = importe.charAt (i);
        if (car != '0' && car != '1' && car != '2' && car != '3' && car != '4' &&
            car != '5' && car != '6' && car != '7' && car != '8' && car != '9' &&
            car != '.') {
            cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
            return false;
        }
        if (car == '.') numpuntos++;
    }
    if (numpuntos > 1) {
        cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
        return false;
    }
    if (document.frmRegistro.CuentaCargo.value == "") {
        cuadroDialogo ("Debe seleccionar una cuenta de cargo.", 3);
        return false;
    }
    if (document.frmRegistro.NoPago.value =="") {
        cuadroDialogo ("Debe escribir un n&uacute;mero de pago.", 3);
        return false;
    } else {
        for (i = 0; i < document.frmRegistro.NoPago.value.length; i++) {
            car = document.frmRegistro.NoPago.value.charAt (i);
            if (car != '0' && car != '1' && car != '2' && car != '3' && car != '4' &&
                car != '5' && car != '6' && car != '7' && car != '8' && car != '9') {
                cuadroDialogo("El n&uacute;mero de pago no es v&aacute;lido.", 3);
                return false;
            }
        }
    }
    if (document.frmRegistro.Concepto.value == "") {
        cuadroDialogo ("Debe especificar un concepto.", 3);
        return false;
    }


    document.getElementById("botones").style.visibility="hidden";
	document.getElementById("mensaje").style.visibility="visible";
    return true;
}

function deshabil_combo(){
	document.frmRegistro.NomBen.selectedIndex=0;
	//document.frmRegistro.NomBen.disabled = true;
	document.frmRegistro.NomBen.value="SIN_SELECCION";
}

function checaImporte () {
    var importe = document.frmRegistro.Importe.value;
    var indicePunto = importe.indexOf ('.');
    if (indicePunto != -1) {
        if (importe.indexOf ('.', indicePunto + 1) != -1) {
            cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
            document.frmRegistro.Importe.value = "";
            return;
        }
    }
    if ((importe.length - indicePunto) > 2 && indicePunto != -1)
        importe = document.frmRegistro.Importe.value = importe.substring (0, (indicePunto + 3));
    if (isNaN(importe)) {
        cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
        document.frmRegistro.Importe.value = "";
        return;
    }
}
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%if (!Mensaje.equals("")) out.println("cuadroDialogo('" + Mensaje + "', 1)");%>"
background="/gifs/EnlaceMig/gfo25010.gif">			  <!-- modificar para cuando es NomBen y cuando es no registrado ya que lo esta tomando para nomben y este no existe obviamente --> <!-- <%--if (escribe && request.getAttribute("Escibe" ) == null) out.print ("frmRegistro.NomBen.value = " + Pago.getClaveBen () + ";");--%> -->


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></td>
  </tr>
</TABLE>
 <%= request.getAttribute("Encabezado") %>
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="frmRegistro" method=post action="pdLinea">
    <tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat" colspan="2"> Capture los datos del Pago</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top" colspan="2">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                  <td>
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" nowrap>Cuenta de cargo:</td>
                      </tr><!-- cuentas -->
                       <%-- Codigo para desplegar las cuentas de cargo en un select
                      <TR>
                        <td class='tabmovtex' nowrap>
                         <SELECT name='CuentaCargo'>
                            <OPTION value='0'>Seleccione una Cuenta de Cargo</OPTION>
                            <%
                                java.util.ListIterator liCuentas = Cuentas.listIterator();
                                EnlaceMig.pdCuenta Cuenta;
                                while (liCuentas.hasNext()) {
                                    Cuenta = (EnlaceMig.pdCuenta) liCuentas.next();
                            %>
                            <OPTION value='<%=Cuenta.getNoCta ()%>'><%=Cuenta.getNoCta ()%></OPTION>
                            <%
                                }
                            %>
                         </SELECT>
                        </TD>
                      </TR>
                      Fin de cuentas de cargo --%>
                      <tr>	 <!-- fin cuentas -->
                        <td class="tabmovtex">
				  	    <input type="text" name=CuentaCargo  class="tabmovtexbol" maxlength=16 size=16 onfocus="blur();" <%if (escribe) out.print ("value='" + Pago.getCuentaCargo () + "'");%>>
					    <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
					    <input type="hidden" name="cboCuentaCargo" >
                        </td>
                     </tr>

                      <tr>
                        <td class="texresalbol" nowrap>Beneficiario registrado:</td>
                      </tr>
                      <tr>
                      <%-- Codigo para desplegar a los beneficiarios --%>
                        <td class="tabmovtex" nowrap>
                          <SELECT NAME="NomBen">
					        <option value="SIN_SELECCION">Seleccione un Beneficiario</OPTION>
							<%! String va = ""; %>
					        <%
                                java.util.ListIterator liBeneficiarios = Beneficiarios.listIterator();
                                mx.altec.enlace.bo.pdBeneficiario Benef;
								if(null == Pago.getClaveBen() && null == Pago.getNomBen()){}else{
                                while (liBeneficiarios.hasNext()) {
                                    Benef = (mx.altec.enlace.bo.pdBeneficiario) liBeneficiarios.next();
                            %>
                            <OPTION value='<%=Benef.getID()%>' <%if(Pago.getClaveBen () != null)if (escribe && Pago.getClaveBen ().equals (Benef.getID ())){ out.print ("selected");va="1";}%> > <%=Benef.getID()%>  <%=Benef.getNombre()%></OPTION>
                            <%
                                }
								}
                            %>
					     </SELECT>
                        </td>
						<!-- <table><tr><td>prueba</td></tr></table>
 -->

                      </tr>

					  <% if (((Boolean) Permisos.get ("bensinreg")).booleanValue()) {
						  %>

					  <tr>
					    <td class='texresalbol' nowrap>Beneficiario no registrado:</td>
					  </tr>
					  <tr>
						  <td class='texresalbol' >- Clave:</td></tr>
						  <tr>
						  <td class='tabmovtex' nowrap>
						  <input type= text maxlength= 13 size=13 class='tabmovtex' name='clv_no_reg' value='<% if(escribe && va != "1"){if(null == Pago.getClaveBen())out.print("sin");else out.print(Pago.getClaveBen());}%>'>
						  </td>
						   </tr><tr>
						<td class='texresalbol' >- Nombre: </td></tr><tr>
						  <td class='tabmovtex' nowrap>
							<input type = 'text' maxlength=60 size=60 class='tabmovtex' name='Ben_no_reg' value='<% if(escribe && va != "1"){if(null != Pago.getNomBen())out.print(Pago.getNomBen());else out.print("no se pudo obtener el nombre"); }%>' onblur='ben_no()' onfocus="deshabil_combo()">  <!--  -->
							</td>
					  </tr>

							<%}else{%>
								<input type=hidden name = Ben_no_reg value="">
								<%}%>
                      <TR>
                        <TD class='texresalbol' nowrap>Concepto:</TD>
                      </TR>
                      <TR>
                        <TD class='tabmovtex' nowrap>
                            <INPUT type='text' maxlength='60' size='60' class='tabmovtex' name='Concepto' value='<%if (escribe) out.print (Pago.getConcepto ());%>' onblur = 'concep()'>
                        </TD>
                      </TR>

					<TR>
                        <TD class='texresalbol' nowrap>Forma de Pago:</TD>
                      </TR>
                      <TR>
                        <TD class='tabmovtex' nowrap>
                          <SELECT name='FormaPago'>

<%
	String contratoTraspaso = "";
	try {
	    Connection connContrato = mx.altec.enlace.servlets.BaseServlet.createiASConn_static(mx.altec.enlace.utilerias.Global.DATASOURCE_ORACLE);
	    Statement stmtContrato = connContrato.createStatement();
	    ResultSet rs = stmtContrato.executeQuery("select cta_cheques from cata_cheq_enlace where cve_chequera = 'LOPT' and sucursal_region = '99' and u_version = '9'");
	    if(rs.next()){
		  	        contratoTraspaso = rs.getString(1);
	    }
	} catch (SQLException e) {
	    mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace("pdLinea.jsp - iniciaCatalogoClaves(): Error al intentar crear la conexion.",mx.altec.enlace.utilerias.EIGlobal.NivelLog.ERROR);
	}
	    mx.altec.enlace.bo.BaseResource sessionRes = (mx.altec.enlace.bo.BaseResource) request.getSession ().getAttribute ("session");
%>

                            <OPTION value='E' <%if (escribe && Pago.getFormaPago () == 'E') out.print (Pago.getFormaPago ());%>>Efectivo</OPTION>
<%  if(sessionRes.getContractNumber().trim().equals(contratoTraspaso.trim())){  %>
                            <OPTION value='T' <%if (escribe && Pago.getFormaPago () == 'T') out.print (Pago.getFormaPago ());%>>Traspaso entre cuentas</OPTION>
<%  }  %>
                            <OPTION value='C' <%if (escribe && Pago.getFormaPago () == 'C') out.print (Pago.getFormaPago ());%>>Cheque</OPTION>
                          </SELECT>
                        </TD>
                      </TR>
                    </table>
                  </td>
                  <td align="right">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" nowrap>N&uacute;mero de pago:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex">
						  <INPUT TYPE="text" SIZE="25" class="tabmovtex" maxlength="20" NAME="NoPago" value='<%if (escribe) out.print (Pago.getNoPago ()); else out.print ("");%>' <%if (escribe) out.print ("onFocus='blur();'"); else out.print ("");%>>
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap>Importe del pago:</td>
                      </tr>
                      <tr>
					  <%
					  java.text.DecimalFormat nf = new java.text.DecimalFormat();
					  nf .applyPattern("##########0.0#");
					  nf .setMinimumFractionDigits(2);
					  nf .setMaximumFractionDigits(2);
					  %>
                        <td class="tabmovtex" nowrap>
						  <INPUT TYPE="text" SIZE="25" class="tabmovtex" NAME="Importe" MAXLENGTH="16" <%if (escribe) out.print ("value='" + nf.format(((double)Pago.getImporte ())) + "'");%> onblur='checaImporte ()'>
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle">Fecha de
                          libramiento:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle">
						  <input type=text name='FechaLibramiento' size="25" class="tabmovtex" value="<%if (escribe) out.print (sdf.format (Pago.getFechaLib ().getTime ())); else out.print (sdf.format (fechaLib.getTime ()));%>" onFocus='blur();' maxlength=10>
						  <!-- Agrega calendario -->
						  <A HREF="javascript:modulo = 'Consultas';MuestraCalendario();"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario"></A><!-- Fin agrega calendario -->
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle">Fecha l&iacute;mite
                          de pago:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle">
						  <input type=text name='FechaLimite' size="25" class="tabmovtex" value="<%if (escribe) out.print (sdf.format (Pago.getFechaPago ().getTime ())); else out.print (sdf.format (fechaLim.getTime ()));%>" onFocus='blur();' maxlength=10>
					      <A HREF="javascript:modulo = 'Registro';MuestraCalendario();"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario"></A>
                        </td>
                      </tr>

                      <tr>
                        <td class="texresalbol" nowrap>Clave de la sucursal:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap>
						<input type='radio' name ="Clave"value = "Todas" onClick="EstableceEstado();" <%if (escribe && !Pago.getClaveSucursal ().equals ("")) out.print (""); else out.print ("checked");%>>Todas las Sucursales
						<BR>
						<input type = "radio" name = "Clave" value= "sucursal" onClick = "EstableceEstado();" <%if (escribe && !Pago.getClaveSucursal ().equals ("")) out.print ("checked"); else out.print ("");%>>Sucursal
						<INPUT TYPE="Text" NAME="Arch" SIZE = "4" maxlength='4' onFocus="QuitaFoco()" value='<%if (escribe && !Pago.getClaveSucursal ().equals ("")) out.print (Pago.getClaveSucursal ()); else out.print ("");%>' >

						 <input type=hidden name='ClaveSucursal'>
						  <!-- <INPUT TYPE="text" SIZE="5" class="tabmovtex" NAME="ClaveSucursal" value='<%if (escribe && !Pago.getClaveSucursal ().equals ("")) out.print (Pago.getClaveSucursal ()); else out.print ("Todas");%>'> -->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr id="mensaje" align="center"  style="visibility:hidden">
       		<td colspan="4" class="tabmovtex">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
   		 </tr>

       <tr id="botones">
          <%if(escribe) out.print ("<tr><td align='left' valign='top' width='76'><A HREF=\"pdConsultas\"><img src='/gifs/EnlaceMig/gbo25320.gif' border=0 alt='Regresar' ></A></td>"); %>

            <!-- <td align="left" valign="top" width="133"> -->

			<%if(escribe){
				if(((Boolean) Permisos.get ("Envio")).booleanValue()){%>
					<td align=left valign=top>
					<A href = "javascript:if(validaForma ()) document.frmRegistro.submit();" border=0><img src='/gifs/EnlaceMig/gbo25510.gif' border=0 alt="Modificar pago"></a></td>
				<%}else{%>
					<td align=right valign=top>
					<A href='javascript:nofacultad("env�o en l�nea de pago directo.")'><IMG src='/gifs/EnlaceMig/gbo25510.gif' border='0' alt='Modificar pago' ></a></td>
				   <%}
				}else{
					if(((Boolean) Permisos.get ("Envio")).booleanValue()){%>
						<td align=right valign=top>
						<A href = "javascript:if(validaForma ()) document.frmRegistro.submit();" border=0><img src='/gifs/EnlaceMig/pdr25560.gif' border=0 alt="Registrar pago"></a></td>
					<%}else{%>
						<td align=left valign=top>
					<A href='javascript:nofacultad("env&iacute;o en l&iacute;nea de pago directo.")'><IMG src='/gifs/EnlaceMig/pdr25560.gif' border='0' alt='Registrar pago' ></a></td>
					<%}

			}%>
			<%
			if(!escribe)out.print("<td align=\"left\" valign=\"top\" width=\"76\">  <A href = \"javascript:document.frmRegistro.reset();\" border=0><img src=\"/gifs/EnlaceMig/gbo25250.gif\" border=0 alt=\"Limpiar\" ></a>  </td>");
			%>

  			  <!-- <A href = "javascript:if(validaForma ()) document.frmRegistro.submit();" border=0><img src='<%if (escribe) out.print ("/gifs/EnlaceMig/gbo25510.gif"); else out.print ("/gifs/EnlaceMig/pdr25560.gif");%>' border=0 alt="Registrar pago"></a> -->

          </tr>
        </table>
        <br>
      </td>
    </tr>
  <INPUT type='hidden' name='txtOpcion' value='<%if (escribe) out.print ("2"); else out.print ("1");%>'>
  <INPUT TYPE="hidden" NAME="FacBenefNoReg" VALUE='<%= request.getAttribute("FacBenefNoReg"  ) %>'>
  </form>
</table>

</BODY>
</HTML>
<%
    request.removeAttribute("Pago");
    request.removeAttribute("fechaLib");
    request.removeAttribute("fechaLim");
    request.removeAttribute("Mensaje");
%>