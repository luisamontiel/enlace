<%@ page import="java.util.Calendar" %>
<%@ page import="java.lang.*" %>
<%
    Calendar dt = Calendar.getInstance();
    dt.setTime( new java.util.Date() );
    int mes = dt.get(Calendar.MONTH);
    int dia = dt.get(Calendar.DAY_OF_MONTH);
    int anio = dt.get(Calendar.YEAR);

	java.util.Date hora = new java.util.Date();
	int hh = hora.getHours();
	int mm = hora.getMinutes();
	int ss = hora.getSeconds();
%>

<%
	String convenio = (String)request.getAttribute("convenio");
	String importe = (String)request.getAttribute("importe");
	String servicio_idSat = "";
	String lineaCaptura  = "";
	String servicio_id = "";
	String concepto = "";
	System.out.println("convenio    ==> " + convenio);
	System.out.println("importe     ==> " + importe);

	if("PMRF".equals((String)request.getAttribute("servicio_id"))) {
		lineaCaptura = (String)request.getAttribute("linea_captura");
		servicio_idSat = (String)request.getAttribute("servicio_id");
		System.out.println("linea_captura ==> " + lineaCaptura);
		System.out.println("servicio_idSat ==> " + servicio_idSat);
	} else {
		String referencia = (String)request.getAttribute("referencia");
		String url = (String)request.getAttribute("url");
	    concepto = (String)request.getParameter("concepto");
	    servicio_id = (String)request.getParameter("servicio_id");

		System.out.println("referencia  ==> " + referencia);
		System.out.println("url         ==> " + url);
	    System.out.println("concepto    ==> " + concepto);
	    System.out.println("servicio_id ==> " + servicio_id);
	}
%>
<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/JavaScript">

function procesa(){
	document.registro.submit();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

var dayarray=new Array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


var mydate=new Date(<%=anio%>,<%=mes%>,<%=dia%>,<%=hh%>,<%=mm%>,<%=ss%>);

function reloj()
 {
   if(!document.layers && !document.all)
     return;
   var digital = new Date();
   var year=digital.getYear();
   if (year < 1000)
      year+=1900;
   var day=digital.getDay();
   var month=digital.getMonth();
   var daym=digital.getDate();
   if (daym<10)
      daym="0"+daym;

   var horas = digital.getHours();
   var minutos = digital.getMinutes();
   var segundos = digital.getSeconds();
   var amOrPm = "AM";
   if (horas > 11)
     amOrPm = "PM";
   if (horas > 12)
     horas = horas - 12;
   if (horas == 0)
     horas = 12;
   if (minutos <= 9)
     minutos = "0" + minutos;
   if (segundos <= 9)
     segundos = "0" + segundos;

   dispTime = "<font color='666666' face='Verdana'>" + dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" | "+horas+":"+minutos+":"+segundos+" "+ amOrPm +"</font>";
   //dispTime = horas + ":" + minutos + ":" + segundos + " " + amOrPm;
   if (document.layers)
    {
      document.layers.objReloj.document.write("<font face=Tahoma size=1>"+ dispTime + "</font>");
	  document.layers.objReloj.document.close();
	}
   else
    if (document.all)
	  objReloj.innerHTML = dispTime;
   setTimeout("reloj()", 1000);
 }


//-->
</script>
<link rel="stylesheet" href="/EnlaceMig/pagos.css" type="text/css"/>
</head>
<body onLoad="reloj()">
<span id="objReloj" style="position:absolute;left:285;top:60;"></span>
<table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="2"><img src="/gifs/EnlaceMig/cheque_img.jpg" width="147" height="40"/></td>
   	<td width="593" valign="top" bgcolor="#FF0000"><img src="/gifs/EnlaceMig/top.gif" width="453" height="40"/></td>
  </tr>
  <tr>
    <td background="/gifs/EnlaceMig/sombra_gris.jpg"><img src="/gifs/EnlaceMig/sombra_gris.jpg" width="18" height="14"/></td>
    <td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
        <tr>
          <td align="left"> <div align="left">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr><td>&nbsp;</td>
                </tr>
                <tr>
                  <td><p class="titulorojo">Elija el contrato con el que desea realizar el pago </p></td>
                  <td width="2"><a href="javascript:;" onClick="MM_openBrWindow('/EnlaceMig/ayuda005_enlace.html','ayuda','scrollbars=yes,resizable=yes,width=340,height=320')" onMouseOver="MM_swapImage('Image32','','/gifs/EnlaceMig/ayuda_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/ayuda.jpg" name="Image32" width="47" height="40" border="0" id="Image3"/></a></td>
                </tr>
              </table>
            </div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td><form name="registro" method="post" action="CuentaCargoMicrositio">
		<INPUT TYPE="hidden" VALUE="<%=request.getAttribute("opcionElegida") %>" NAME="opcionElegida" size="25"/>

		<input name="convenio" type="hidden" id="convenio" value="<%=request.getAttribute("convenio")%>" size="10"/>
   		<input name="importe" type="hidden" id="importe" value="<%=request.getAttribute("importe")%>" size="10"/>

   		<%if("PMRF".equals((String)request.getAttribute("servicio_id"))) {%>

   			<input name="linea_captura" type="hidden" value="<%=request.getAttribute("linea_captura")%>"/>
   			<INPUT TYPE="hidden" name="servicio_id" value="<%=request.getAttribute("servicio_id")%>"/>

   		<%} else { %>

	   		<input name="referencia" type="hidden" id="referencia" value="<%=request.getAttribute("referencia")%>" size="10"/>
	   		<input name="url" type="hidden" id="url" value="<%=request.getAttribute("url")%>" size="25"/>
	        <INPUT TYPE="hidden" name="concepto" value="<%=concepto%>"/>
			<INPUT TYPE="hidden" name="servicio_id" value="<%=servicio_id%>"/>

		<%}%>

        <table width="100%" border="0" cellspacing="6" cellpadding="0">
          <tr align="center">
            <td>&nbsp;</td>
          </tr>
          <tr align="center">
            <td><SELECT  NAME="lstContratos">
		<%=request.getAttribute("Output1") %>
	     </SELECT></td>
          </tr>
          <tr align="center">
            <td><a href="javascript:procesa();" onMouseOver="MM_swapImage('Image1','','/gifs/EnlaceMig/entrar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/entrar.jpg" name="Image1" width="82" height="41" border="0" id="Image1"/></a></td>
          </tr>
        </table>
      </form></td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td align="right"><img src="/gifs/EnlaceMig/sitio_seg.jpg"/>&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td></td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
    <td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
  </tr>
</table>
</body>
</html>

<Script type="text/JavaScript">
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
</Script>


</body>
</html>