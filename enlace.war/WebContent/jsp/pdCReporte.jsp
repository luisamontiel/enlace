<!--
 /*Barrios Osornio Christian (CBO) IM323202  30/12/03	Praxis*/
	/*Se agrego una fila mas a la tabla principal, la fila contiene*/
	/*el total de registros y la suma total de los importes */
	/*parciales.*/

/* Gerardo Vald�z Ram�rez (GVR) IM323150  30/12/03	Praxis*/
	/*Se agrego el campo de Sucursal*/
-->

<%@page contentType="text/html" %>
<%@page import="java.util.*, mx.altec.enlace.bo.*, java.text.*" %>
<jsp:useBean id="pdConsultaRes" class="java.util.ArrayList" scope="session" />
<jsp:useBean id="pdCBeneficiariosM" class="java.util.HashMap" scope="session" />
<jsp:useBean id="pdMensaje" class="java.lang.String" scope="request" />
<jsp:useBean id="pdError" class="java.lang.String" scope="request" />
<jsp:useBean id="pdCPermisos" class="java.util.HashMap" scope="session" />
<%--<jsp:useBean id="pdCArchivo" class="java.lang.String" scope="session" />--%>
<% String pdCArchivo = (String) session.getAttribute("pdCArchivo");
    if ( null == pdCArchivo ) {
        pdCArchivo = request.getParameter("pdCArchivo");
    }
    int start;
%>
<%!

	int view = 100;
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");

	String getDate(java.util.GregorianCalendar cal){
		if(null == cal) return "&nbsp;";

		return sdf.format(cal.getTime());

	}

	String getBgColor(boolean colorClaro){
		if(colorClaro)
			return "textabdatcla";
		else
			return "textabdatobs";
	}
%>
<html><head>
<title>Pago Directo - Resultados de Consulta</title>
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript" src= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

var respuesta = -1;
var pdOpcion = -1;

function js_Depurar(){
	document.frmConsulta.pdcOpcion.value = 2;
	document.frmConsulta.submit();
}

function js_NuevoFiltro(){
	document.frmConsulta.pdcOpcion.value = 0;
	document.frmConsulta.submit();
}

function js_Cancelar(){
	document.frmConsulta.pdcOpcion.value = 3;
	pdOpcion = 3;
	var c = countSeleccionados();
	if(0 == c){
		cuadroDialogo( "Seleccione al menos un registro..." ,3);
	} else if ( 1 == c){
        var frm = document.frmConsulta;
        var regType ="";
        for(i = 0; i< frm.elements.length;++i){
            if (frm.elements[i].name == 'reg'){
                if(frm.elements[i].checked == true){
                    regType = "regType" + frm.elements[i].value;
                    for(j = 0;j < frm.elements.length;++j){
                        if (frm.elements[j].name == regType ){
                            if(frm.elements[j].value == 'L' ||
                                frm.elements[j].value == 'C' ||
                                frm.elements[j].value == 'M' ||
                                frm.elements[j].value == 'V' ){
                                cuadroDialogo("No es posible cancelar registros liquidados, modificados, vencidos o cancelados...",3);
                                frm.elements[i].checked = false;
                                return;
                            }
                        }
                    }
                }
            }
        }
		cuadroDialogo("Est&aacute; seguro que desea eliminar el registro?",2);
	} else {
        var frm = document.frmConsulta;
        var regType ="";
        for(i = 0; i< frm.elements.length;++i){
            if (frm.elements[i].name == 'reg'){
                if(frm.elements[i].checked == true){
                    regType = "regType" + frm.elements[i].value;
                    for(j = 0;j < frm.elements.length;++j){
                        if (frm.elements[j].name == regType ){
                            if(frm.elements[j].value == 'L' ||
                                frm.elements[j].value == 'C' ||
                                frm.elements[j].value == 'M' ||
                                frm.elements[j].value == 'V' ){
                                cuadroDialogo("No es posible cancelar registros liquidados, modificados, vencidos o cancelados...",3);
                                frm.elements[i].checked = false;
                                return;
                            }
                        }
                    }
                }
            }
        }
		cuadroDialogo("Est&aacute; seguro que desea eliminar los registros?",2);
	}
}

function js_Exportar(){
	document.frmConsulta.pdcOpcion.value = 4;
	document.frmConsulta.submit();
}


function js_Modificar(){
	document.frmConsulta.pdcOpcion.value = 5;
	pdOpcion = 5;
	var c = countSeleccionados();
	if(0 == c || 1 < c){
	    cuadroDialogo( "Seleccione un registro..." ,3);
	} else if ( 1 == c ){
        var frm = document.frmConsulta;
        var regType ="";
        for(i = 0; i< frm.elements.length;++i){
            if (frm.elements[i].name == 'reg'){
                if(frm.elements[i].checked == true){
                    regType = "regType" + frm.elements[i].value;
                    for(j = 0;j < frm.elements.length;++j){
                        if (frm.elements[j].name == regType ){
                            if(frm.elements[j].value == 'L' ||
                                frm.elements[j].value == 'C' ||
                                frm.elements[j].value == 'M'){
                                cuadroDialogo("No es posible modificar registros liquidados, modificados o cancelados...",3);
                                frm.elements[i].checked = false;
                                return;
                            }
                        }
                    }
                }
            }
        }
	    cuadroDialogo("Est&aacute; seguro que desea modificar el registro?",2);
	}
}

function js_desplazamiento(index){
	document.frmConsulta.pdcOpcion.value = 2;
	document.frmConsulta.start.value = index;
	document.frmConsulta.submit();
}

function continua(){
	if(pdOpcion == 3 && respuesta == 1){

		document.getElementById("botones").style.visibility="hidden";
		document.getElementById("mensaje").style.visibility="visible";

		document.frmConsulta.pdcOpcion.value = 3;
		document.frmConsulta.submit();

	} else if ( pdOpcion == 5 && respuesta == 1){
		document.frmConsulta.pdcOpcion.value = 5;
		document.frmConsulta.submit();
	}
}

function countSeleccionados(){
	var c = 0;

	for(var i = 0;i < document.frmConsulta.elements.length; i++){

		var e = document.frmConsulta.elements[i];
		if(e.name == 'reg' ){
			if(e.checked == true){
				c++;
			}
		}
	}
	return c;
}

function checkAll() {
	 for (var i=0; i < document.frmConsulta.elements.length;i++) {
		var e = document.frmConsulta.elements[i];
		if (e.name != 'allbox')
			e.checked = document.frmConsulta.allbox.checked;
 	}
}

function permisos(txt){
    cuadroDialogo("No tiene permiso para " + txt,3);
}

function cuadroDialogoPD( mensaje, tipo )
{
	respuesta=0;
	var titulo="Error";
	var imagen="gic25020.gif";
	var mensaje1;
	mensaje1 = "ATENCION :  "+mensaje;

	if ( tipo == 1 ) // Alerta
	{
		imagen="gic25020.gif"; // signo de admiraci�n
		titulo="Alerta";
	}
	else if ( tipo== 2) // Confirmaci�n
	{
		imagen ="gic25050.gif"; // signo de interrogaci�n
		titulo ="Confirmaci&oacute;n";
	}
	else // Error
	{
		imagen="gic25060.gif"; // una espantosa X
		titulo="Error";
	}

	ventana=window.open('','trainerWindow','width=380,height=150,toolbar=no,scrollbars=no,left=210,top=275');
	ventana.document.open();
	ventana.document.write("<html>");
	ventana.document.writeln("<head>\n<title>"+titulo+"</title>\n");
	ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	ventana.document.writeln("</head>");
	ventana.document.writeln("<body bgcolor='white' bgcolor='#ffffff'>");
	ventana.document.writeln("<form>");
	ventana.document.writeln("<table border=1 width=360 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
	ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 width=360 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
	ventana.document.writeln("<tr><td class='tabmovtex1' align=center width=50><img src='/gifs/EnlaceMig/"+imagen+"' border=0></a></td>");
	ventana.document.writeln("<td class='tabmovtex1' align=center><br>"+mensaje+"<br><br>");
	ventana.document.writeln("</tr></td>");
	ventana.document.writeln("<tr><td class='tabmovtex1'>");
	ventana.document.writeln("</td></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 align=center cellspacing=6 cellpadding=0 >");
	if (tipo == 2) // Solo para confirmaci�n : aceptar y cancelar
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=1;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else // bot�n Cerrar
	{
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=0;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}
	ventana.document.writeln("</table>");
	ventana.document.writeln("</form>");
	ventana.document.writeln("</body>\n</html>");
	ventana.focus();
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); <%= session.getAttribute("despliegaEstatus" ) %>"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>
<span class='titpag'> <%= request.getAttribute("Encabezado") %> </span>
<%if (null != pdMensaje && ! pdMensaje.equals("")){
	%><script language='javascript'>
	cuadroDialogoPD("<%=pdMensaje%>",1); </script><%
	request.removeAttribute("pdMensaje");
}%>
<%if(null != pdError && ! pdError.equals("")){
	%><script language='javascript'>cuadroDialogo("<%=pdError%>",3);</script><%
	request.removeAttribute("pdError");
}%><%--
	Enumeration en =request.getAttributeNames();
	while(en.hasMoreElements()){
		String s = (String) en.nextElement();
		out.print(s +":"+request.getAttribute(s)+"<br>");
	}
	en = session.getAttributeNames();
	while(en.hasMoreElements()){
	    String s =(String) en.nextElement();
	    out.println(s + ":" + request.getAttribute(s)+"<br>");
	}
--%><%
	start =  ( (Integer) session.getAttribute("start")) .intValue();
%>
<form name='frmConsulta' id="frmConsulta" action='pdConsultas' method='post'>
<input type='hidden' name='start' value='<%= start %>'>
<input type='hidden' name='view' value='<%= view %>'>
<input type='hidden' name='pdcOpcion' value=''>
<input type='hidden' name='pdCArchivo' value='<% if (null != pdCArchivo && !pdCArchivo.equals("") ) out.print(pdCArchivo); else out.print(""); %>' >

<table>
<%
		int i=0;
		/*Barrios Osornio Christian (CBO) IM323202*/
	double Total=0;
	double SemiTotal=0;
	pdCPago pag;
	ListIterator lista = pdConsultaRes.listIterator(0);
	java.text.DecimalFormat formateador = new DecimalFormat("$###,###,##0.00;(-$###,###,##0.00)");
	while(lista.hasNext())
	{
		pag = (pdCPago) lista.next();
		Total=Total+((double)pag.getImporte());
		i++;
        }

	ListIterator li = pdConsultaRes.listIterator(start);
	i=start ;
	while (( i < start + view) && li.hasNext())
	{
	 pag = (pdCPago)li.next();
	 SemiTotal+=((double)pag.getImporte());
	 i++ ;

	}
	/*Barrio Osornio Christian (CBO) IM323202*/
%>

    <tr> <td class="texencfec" width="30%"> Total de Registros: </td>
	     <td ><font class="texencfec"color="#FF0000"><%=i-start%></font></td>
		 <td class="texencfec" width="120" >por un valor de </td>
 		 <td class="texencfec"><font color="#FF0000"> <%=formateador.format(SemiTotal)%></font></td>

	</tr>

</table>

<table border=0><tr><td>
<table cellpadding='2' cellspacing='1'>

	<tr><td class="tittabdat" ><input type='checkbox' name='allbox' value="checkbox" onclick='checkAll();'>Todos</td>
			<td class="tittabdat">No. Pago</td><td class="tittabdat">No. Cta</td><td class="tittabdat">Cve. Benef</td>
			<td class="tittabdat">Beneficiario</td><td class="tittabdat">Concepto</td>
			<td class="tittabdat">Fma. Pago</td><td class="tittabdat">Importe</td>
			<td class="tittabdat">Fecha Liberacion</td><td class="tittabdat">Fecha Lim. Pago</td><td class="tittabdat">Fecha Operacion</td>
			<td class="tittabdat">Referencia</td><td class="tittabdat">Estatus</td><td class="tittabdat">Descripcion</td>
			<%
			/* Gerardo Vald�z Ram�rez (GVR) IM323150 */
			%>
			<td class="tittabdat">Sucursal</td>
			<%
			/* Gerardo Vald�z Ram�rez (GVR) IM323150 */
			%>
<%
	li = pdConsultaRes.listIterator(start);
	java.text.DecimalFormat nf = new java.text.DecimalFormat();



		nf .applyPattern("#,###,###,###0.0#");
		nf .setDecimalSeparatorAlwaysShown(true);
		nf .setGroupingSize(3);
		nf .setGroupingUsed(true);
		nf .setMinimumFractionDigits(2);
		nf .setMaximumFractionDigits(2);



	boolean colorClaro = true;
	//Total=0;
	i=0;

  i= start;
	while( (i < start + view) && li.hasNext() )
	{
		try
		{

		pdCPago pago = (pdCPago) li.next();
%>

	<tr><td class="<%= getBgColor(colorClaro) %>"><input type="checkbox" name="reg" value="<%= i %>"></td>
        <input type="hidden" name="regType<%= i%>" value="<%= pago.getStatus() %>">
		<td class="<%= getBgColor(colorClaro) %>"><%=((pago.getNoOrden()==null)?"0":pago.getNoOrden())%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%=((pago.getNoCta()==null)?"":pago.getNoCta())%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%=((pago.getCveBenef()==null)?"":pago.getCveBenef())%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%
					if ( null == pdCBeneficiariosM.get( pago.getCveBenef() ) )
						out.print ( ((pago.getNombreBenefNoRegistrado()==null || pago.getNombreBenefNoRegistrado().equalsIgnoreCase("null"))?"SIN NOMBRE" :pago.getNombreBenefNoRegistrado()) + " - Beneficiario No registrado");
                    else
						out.print ( ( (pdBeneficiario) pdCBeneficiariosM.get( pago.getCveBenef()) ).getNombre() );%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%=((pago.getInstrucciones()==null || pago.getInstrucciones().equals("null"))?"":pago.getInstrucciones())%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%=pago.getFmaPago()%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>" align="right"><%= nf.format(((double)pago.getImporte())) %>&nbsp;</td>
		<% //Total= Total + (double)pago.getImporte(); %>
		<td class="<%= getBgColor(colorClaro) %>"><%= getDate(pago.getFechaAplic()) %>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%= getDate(pago.getFechaLimPago()) %>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%
					if( pago.getStatus() == 'L' )%><%=getDate(pago.getFechaPago())%><%
					else if(pago.getStatus() == 'C' )%><%=getDate(pago.getFechaCancelacion())%><%
					else if(pago.getStatus() == 'V' )%><%=getDate(pago.getFechaVencimiento())%><%
					else if(pago.getStatus() == 'M' )%><%=getDate(pago.getFechaModificacion())%><%
					else %><%=getDate(pago.getFechaLimPago())%>&nbsp;</td><%--Fecha operacion--%>
		<td class="<%= getBgColor(colorClaro) %>"><%=(pago.getRefOperacion()==null)?"":pago.getRefOperacion()%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%=String.valueOf(pago.getStatus())%>&nbsp;</td>
		<td class="<%= getBgColor(colorClaro) %>"><%=(pago.getStatusDesc()==null)?"":pago.getStatusDesc()%>&nbsp;</td>
		<%/* Gerardo Vald�z Ram�rez (GVR) IM323150 */%>
		<td class="<%= getBgColor(colorClaro) %>"><%=(pago.getCveSucursal()==null)?"":pago.getCveSucursal()%>&nbsp;</td>
	    <%/* Gerardo Vald�z Ram�rez (GVR) IM323150 */%>
	</tr>
<%		colorClaro = ! colorClaro;
		++i;
		}catch(java.io.IOException e)
		 {
			System.out.println("Error en el JSP pdCReporte.jsp:  "+e.getMessage());
		 }
	}
%>

	<%/*Barrios Osornio Christian (CBO) IM323202*/%>

   <tr>

     <td class="tittabdat" align="center" colspan=8>
	 <B><FONT size=2 color="#0"><B>Total de Documentos de su consulta: </B><%=pdConsultaRes.size()%></FONT>
	 </td>

	 <td class="tittabdat" align="right" colspan=7>
	  <FONT size=2 color="#0"><B>Importe total de su consulta: </B><%=formateador.format(Total)%></FONT>
	 </td>

   </tr>
</table></td></tr>
	<%/*Barrios Osornio Christian (CBO) IM323202*/%>



<tr><td align="center" class='texfootpaggri' nowrap>
		<%
		int c = 0;
		if(start - view >= 0){%><a href='javascript:js_desplazamiento(<%= start - view %>)' class='texfootpaggri'>anterior</a>&nbsp;<%}
		for(i = 0;i < pdConsultaRes.size();i+= view){
			if(c==50){ c=0;%><br><%}
			if(start != i){
				%><a href='javascript:js_desplazamiento(<%= i %>)' class='texfootpaggri'><%=(i /view) +1 %></a><%
				}else{
				%>
				<%
				}%>&nbsp;<%
			c++;
			/*out.print(c);*/
			}
		if(start + view < pdConsultaRes.size()){%>&nbsp;<a href='javascript:js_desplazamiento(<%= start + view %>)' class='texfootpaggri'>siguiente</a><%}%>
		</td></tr>
	<tr><td align='center'><table>
			<tr id="mensaje" align="center"  style="visibility:hidden">
       				<td colspan="5">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
   			</tr>

			<tr id="botones">
				<td>
					<a href='javascript:js_NuevoFiltro()' border='0'><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Consulta" width="83" height='22'></a>
		    	</td>
	    <%--<td>
		<a href='<%
			if( ( (Boolean) pdCPermisos.get("Depurar")).booleanValue()){
			    %>javascript: js_Depurar()<%
			} else {
			    %>javascript: permisos("depurar registros")<%
			}%>' border='0'><img src="/gifs/EnlaceMig/gbo25515.gif" border='0' alt='Depurar' height='22' width='131'></a>
			</td>
	    --%>

	    <td><a href= '<% if( ( (Boolean) pdCPermisos.get("Cancelar")).booleanValue()){
				%>javascript: js_Cancelar()<%
				} else {
				%>javascript: permisos("cancelar")<%
				}%>' border='0'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0 alt='Cancelar' height='22' width='85'></a></td>
	    <td><a href= '<% if( ( (Boolean) pdCPermisos.get("Exportar")).booleanValue() && pdCArchivo != null && !pdCArchivo.equals("")){
				%>/Download/<%=pdCArchivo%><%
			    } else  if (pdCArchivo == null || pdCArchivo.equals("")){
				%>javascript: cuadroDialogo("No se pudo crear el archivo...");<%
			    } else {
				%>javascript: permisos("exportar")<%
			    }%>' border='0'><img src='/gifs/EnlaceMig/gbo25230.gif' border=0 alt='Exportar' height='22' width='85'></a></td>
	    <td><a href= '<% if( ( (Boolean) pdCPermisos.get("Modificar")).booleanValue()){
				%>javascript: js_Modificar()<%
			    } else {
				%>javascript: permisos("modificar")<%
			    }%>' border='0'><img src='/gifs/EnlaceMig/gbo25510.gif' border=0 alt='Modificar' height='22' width='93'></a></td>
	    <td><a href= '<% if( ( (Boolean) pdCPermisos.get("Imprimir")).booleanValue()){
				%>javascript: self.print()<%
			    } else {
				%>javascript: permisos("imprimir")<%
			    }%>' border='0'><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir' height='22' width='83'></a></td>
	</tr></table></td></tr>
</table>
</form>
<body></html>
<% request.removeAttribute("pdMensaje"); %>