<html>
	<head>
		<title>Nuevo Pago de Impuestos : Provisional - Del Ejercicio - Entidades Federativas</title>

		<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
		<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
		<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
		<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
		<script type="text/javascript">
			/******************  Esto no es mio ***************************************************/
			function MM_preloadImages() { //v3.0
				var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
				var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
			}
		</script>
		<script type="text/javascript">
			function MM_swapImgRestore() { //v3.0
				var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
			}
		</script>
		<script type="text/javascript">
			function MM_findObj(n, d) { //v3.0
				var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
				d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
				if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
				for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
			}
		</script>
		<script type="text/javascript">
			function MM_swapImage() { //v3.0
				var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
				if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
			}
			/********************************************************************************/
		</script>
		<script type="text/javascript">
			<%
				if(	session.getAttribute("strScript") !=null)
					out.println( session.getAttribute("strScript"));
				else
					if(request.getAttribute("strScript") !=null)
						out.println( request.getAttribute("strScript") );
					else
						out.println("");
			%>
		</script>
		<script type="text/javascript">
			<%
				if(	session.getAttribute("newMenu") !=null)
					out.println( session.getAttribute("newMenu"));
				else
					if(request.getAttribute("newMenu")!=null)
						out.println(request.getAttribute("newMenu"));
					else
						out.println("");
			%>
		</script>

		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
	</head>

	<body leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">
		<form   NAME="validarfc" METHOD="Post" ACTION="javascript:valida();" onSubmit="oculta()">

			<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
				<tr valign="top">
					<td width="*">
						${requestScope.MenuPrincipal}
					</td>
				</tr>
			</table>

			${requestScope.Encabezado}

			<table width="760" border="0">
				<tr>
					<td class="texenccon" align="center" valign="top">

						<table width="430" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
							</tr>
							<tr>
								<td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
							</tr>
							<tr>
								<td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="." /></td>
							</tr>
						</table>

						<table width="400" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">

								<table width="430" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td colspan="3"> </td>
									</tr>
									<tr>
										<td width="21"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".." /></td>
										<td width="428" valign="top" align="center">

										<table width="380" border="0" cellspacing="2" cellpadding="3">
											<tr>
												<td colspan="3">&nbsp; &nbsp;</td>
											</tr>
											<tr>
												<td align="center" class="tittabcom">
									
					<!-- Mejoras de Linea de Captura -->
					<!-- Se modifica la leyenda Linea de Captura por Pago Referenciado SAT -->
					<!-- Inicio RRR - Indra Marzo 2014									 -->
Servicio no habilitado, favor de pagar sus Impuestos Federales ingresando a la ruta: Servicios > Nuevo Esquema de Pago de Impuestos > Pago Referenciado SAT o ingresando desde el bot�n �Pago Referenciado SAT� de esta pantalla.
					<!-- Fin RRR - Indra Marzo 2014-->
												</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp; &nbsp;</td>
											</tr>
										</table>

										</td>
										<td width="21"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." 	name=".." /></td>
									</tr>
								</table>

								</td>
							</tr>
						</table>

						<table width="430" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="." /></td>
							</tr>
							<tr>
								<td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
							</tr>
							<tr>
								<td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
							</tr>
						</table>

						<table width="430" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<br />

									<table border="0" cellspacing="0" cellpadding="0" height="22">
										<tr>
											<td align="right">
												<a href =" csaldo1?prog=0&flujo=ECCC ">
													<img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" height="22" alt="Regresar" /></a>
											</td>
											<td align="left">
												<a href =" CambioContrato?Modulo=1&flujo=ESLC ">
													<img border="0" name="imageField32" src="/gifs/EnlaceMig/PagoRefSAT.gif" height="22" alt="Pago Referenciado SAT" /></a>
											</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>

		</form>

	</body>
</html>
