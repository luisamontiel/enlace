<html>
<head>
  <title>Banca Virtual Transferencias Multiples. Validacion de los Registros Duplicados Transferidos</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript" type="text/javascript" SRC= "/EnlaceMig/passmark.js"></script>

<!-- Definicion del function para el retorno al Servlet TransferenciasMultiples -->
<!-- <script language="JavaScript" type="text/javascript"> -->

<script language="javaScript">

function valregdup( )
{
     document.tabla_transferencias.action="TransferenciasMultiples?ventana=4";
     document.tabla_transferencias.submit();
}

function regresar( )
{
	history.go(-1)
}

function cuadroDialogoMedidas( mensaje, tipo, ancho, alto)
{
	respuesta=0;
	var titulo="Error";
	var imagen="gic25020.gif";
	var param;

	if ( tipo == 1 )
	{
		imagen="gic25020.gif";
		titulo="Alerta";
	}
	else if ( tipo== 2 )
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 5)
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 6)
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 7)
	{
		imagen ="gic25020.gif";
		titulo ="AVISO";
	}
	else if ( tipo== 8)
	{
		imagen ="gic25020.gif";
		titulo ="Obten Data";
	}
	else
	{
		imagen="gic25060.gif";
		titulo="Error";
	}

	param= "width=" + ancho + ",height=" + alto + ",toolbar=no,scrollbars=no,left=210,top=225";

	ventana=window.open('','trainerWindow',param);
	ventana.document.open();
	ventana.document.write("<html>");
	ventana.document.writeln("<head>\n<title>"+titulo+"</title>\n");
	ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	ventana.document.writeln("</head>");
	ventana.document.writeln("<body bgcolor='white'>");
	ventana.document.writeln("<form>");
	ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
	ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
	ventana.document.writeln("<tr><td class='tabmovtex1' align=right width=50><img src='/gifs/EnlaceMig/"+imagen+"' border=0></a></td>");
	ventana.document.writeln("<td class='tabmovtex1' align=center width=300><br>"+mensaje+"<br><br>");
	ventana.document.writeln("</tr></td>");
	ventana.document.writeln("<tr><td class='tabmovtex1'>");
	ventana.document.writeln("</td></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 align=center cellspacing=6 cellpadding=0>");
	if (tipo == 2)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=1;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else if (tipo == 5)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=1;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}
	else if (tipo == 6)
	{
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}else if (tipo == 7)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=8;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=7;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}else if (tipo == 8)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=9;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=10;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else if (tipo == 9)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=11;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=12;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else
	{
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=0;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}
	ventana.document.writeln("</table>");
	ventana.document.writeln("</form>");
	ventana.document.writeln("</body>\n</html>");
	ventana.focus();
}


/******************  Esto no es mio ***************************************************/

function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
/* Funciones para deplegar la ventana con la informacion*/

<%= request.getAttribute("newMenu") %>
</script>


<!--  OPC:PASSMARK 16/12/2006 ******************* BEGIN -->
<%
	String FSOnuevo = (String)request.getAttribute("_fsonuevoMULT");
	if( FSOnuevo!=null ) {
%>

	<SCRIPT LANGUAGE="JavaScript">
	<!--
	  var expiredays = 365;
  	  var ExpireDate = new Date ();
      ExpireDate.setTime(ExpireDate.getTime() + (expiredays * 24 * 3600 * 1000));
      if (!(num=GetCookie("PMDATA"))) { }
	  DeleteCookie("PMDATA");
      SetCookie("PMDATA", "<%=FSOnuevo%>", ExpireDate,"/");
	//-->
	</SCRIPT>
<%
	 request.removeAttribute("_fsonuevoMULT");
	}
%>
<!--  OPC:PASSMARK 16/12/2006 ******************* END -->



<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
   <!-- MENU PRINCIPAL DEL PROCESO -->
   <%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<SCRIPT>
<%
	if (request.getAttribute("InfoUser")!= null) {
		out.println(request.getAttribute("InfoUser"));
	}
%>
</SCRIPT>


<%
	if (request.getAttribute("ContenidoArchivoStr")!= null) {
%>

<%
		out.println(request.getAttribute("ContenidoArchivoStr"));
    }
%>


<FORM  NAME="tabla_transferencias" METHOD=POST  ACTION="TransferenciasMultiples?ventana=4">

<INPUT TYPE="HIDDEN" NAME="arregloctas_origen2"   VALUE=<%
if( request.getAttribute("arregloctas_origen2")!=null)
	out.println(request.getAttribute("arregloctas_origen2"));
else
	out.println("");

%>>
<INPUT TYPE="HIDDEN" NAME="arregloctas_destino2"  VALUE=<%
if( request.getAttribute("arregloctas_destino2")!=null)
	out.println(request.getAttribute("arregloctas_destino2"));
else
	out.println("");

%>>
<INPUT TYPE="HIDDEN" NAME="arregloimportes2"      VALUE=<%
if( request.getAttribute("arregloimportes2")!=null)
	out.println(request.getAttribute("arregloimportes2"));
else
	out.println("");

%>>
<INPUT TYPE="HIDDEN" NAME="arreglofechas2"        VALUE=<%
if( request.getAttribute("arreglofechas2")!=null)
	out.println(request.getAttribute("arreglofechas2"));
else
	out.println("");

%>>
<INPUT TYPE="HIDDEN" NAME="arregloconceptos2"     VALUE=<%
if( request.getAttribute("arregloconceptos2")!=null)
	out.println(request.getAttribute("arregloconceptos2"));
else
	out.println("");


%>>
<INPUT TYPE="HIDDEN" NAME="contador2"             VALUE=<%
if( request.getAttribute("contador2")!=null)
	out.println(request.getAttribute("contador2"));
else
	out.println("");


%>>
<INPUT TYPE="HIDDEN" NAME="arregloestatus2"       VALUE=<%
if( request.getAttribute("arregloestatus2")!=null)
	out.println(request.getAttribute("arregloestatus2"));
else
	out.println("");


%>>


<table width="750" border="0" cellspacing="2" cellpadding="3">
<tr>
     <td>&nbsp;</td>
     <td class="textabref" >
	 <%
	 String contadoroperaciones ="";
	 String tabla = "";
	 String botontransferir = "";


if( request.getAttribute("contadoroperaciones")!=null)
	 contadoroperaciones = (String)request.getAttribute("contadoroperaciones");
if( request.getAttribute("tabla")!=null)
	 tabla = (String)request.getAttribute("tabla");
if( request.getAttribute("botontransferir")!=null)
	 botontransferir = (String)request.getAttribute("botontransferir");


	 if(tabla==null )
		tabla = "";
	 if(botontransferir==null )
		botontransferir = "";
	 if(contadoroperaciones==null )
		contadoroperaciones = "";
	 %>
		<%-- = request.getAttribute("contadoroperaciones") --%>
		<%=contadoroperaciones%>
	 </td>
</tr>
	<%=tabla%>
	<%-- = request.getAttribute("tabla") --%>

</table>

<br>
<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" ALIGN=CENTER>
<tr>
	<%=botontransferir%>
	 <%-- = request.getAttribute("botontransferir") --%>
</tr>
</table>

</FORM>

</body>
</html>
<!-- 2007.01 -->