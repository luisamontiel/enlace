<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@ page import="mx.altec.enlace.bo.FormatoMoneda" %>
<%@ page import="mx.altec.enlace.bo.MTE_Registro" %>
<%@ page import="mx.altec.enlace.utilerias.Global" %>

<html>
<head>
	<title>TESOFE - Detalle de Importaci&oacute;n</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">
	<%
	System.out.println("Se ejecuta MTE_ImportaDetalle.jsp");

	String nombreArchivoImp="";
	String numeroRegistros="0";
	String importeTotal="0.0";
	String claveEmisora="0";
	String codigoOperacion="0";
	String fechaPresentacion="<br>";
	String tipoArchivo="";

	if(request.getParameter("numeroRegistros")!=null)
	  numeroRegistros = (String)request.getParameter("numeroRegistros");
	if(request.getParameter("importeTotal")!=null)
	  importeTotal = (String)request.getParameter("importeTotal");
	if(request.getParameter("claveEmisora")!=null)
	  claveEmisora = (String)request.getParameter("claveEmisora");
	if(request.getParameter("codigoOperacion")!=null)
	  codigoOperacion = (String)request.getParameter("codigoOperacion");
	if(request.getParameter("fechaPresentacion")!=null)
	  fechaPresentacion = (String)request.getParameter("fechaPresentacion");
	if (request.getParameter("nombreArchivoImp")!= null)
	  nombreArchivoImp = (String)request.getParameter("nombreArchivoImp");
	if(request.getParameter("tipoArchivo")!=null)
	  tipoArchivo = (String)request.getParameter("tipoArchivo");

	System.out.println("Entrando");
	System.out.println("Leyendo archivo: " + nombreArchivoImp);
	System.out.println("Leyendo numeroRegistros: " + numeroRegistros);
	System.out.println("Leyendo importeTotal: " + importeTotal);
	System.out.println("Leyendo claveEmisora: " + claveEmisora);
	System.out.println("Leyendo codigoOperacion: " + codigoOperacion);
	System.out.println("Leyendo fechaPresentacion: " + fechaPresentacion);
	System.out.println("Leyendo tipoArchivo: " + tipoArchivo);

		  int inicio=0;
		  int fin=0;
		  int totalReg=Integer.parseInt(numeroRegistros);

		  int pagina=0;
		  if(request.getParameter("pagina")!=null)
		     pagina=Integer.parseInt(request.getParameter("pagina"));
		  int regPorPagina=Global.MAX_REGISTROS;

		  inicio=(pagina*regPorPagina);
		  fin=inicio+regPorPagina;
		  if(fin>totalReg)
			fin=totalReg;

		  int totalPaginas=calculaPaginas(totalReg,regPorPagina);

		  int A=regPorPagina*(pagina+1);
		  int de=(A+1)-regPorPagina;

		  if((pagina+1)==totalPaginas)
		     A=totalReg;
	%>

function BotonPagina(sigant)
 {
   document.TesImporta.pagina.value=sigant-1;
   document.TesImporta.submit();
 }

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<FORM   NAME="TesImporta" METHOD="Post" ACTION="MTE_ImportaDetalle.jsp">

<table width="770" border="0" cellspacing="0" cellpadding="0" align=center>
 <tr>
   <td align=center>
     <table border="0" width="760" cellspacing="0" cellpadding="0">
	   <tr>
	    <td><br></td>
	   </tr>
	   <tr>
	    <td><br></td>
	   </tr>
	   <tr>
	     <td class="textabref"><%=tipoArchivo%></script></td>
	   </tr>
	 </table>

	<table width="760" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	 <tr>
	  <td class="tittabdat" align="center">Clave Emisora</td>
	  <td class="tittabdat" align="center">Cod. Operacion</td>
	  <td class="tittabdat" align="center">Fecha de Presentaci&oacute;n</td>
	  <td class="tittabdat" align="center">Importe total</td>
	  <td class="tittabdat" align="center">N&uacute;mero de registros</td>
	 </tr>
	 <tr>
	  <td class="textabdatobs" nowrap align="center"><%=claveEmisora%></td>
	  <td class="textabdatobs" nowrap align="center"><%=codigoOperacion%></script></td>
	  <td class="textabdatobs" nowrap align="center"><%=fechaPresentacion%></td>
	  <td class="textabdatobs" nowrap align="center"><%=FormatoMoneda.formateaMoneda(new Double(importeTotal).doubleValue())%></td>
	  <td class="textabdatobs" nowrap align="center"><%=numeroRegistros%></td>
	 </tr>
	</table>

	<table border="0" width="760" cellspacing="0" cellpadding="0">
	   <tr>
	    <td><br></td>
	   </tr>
	   <tr>
	     <td class="textabref">Detalle de archivo</td>
	   </tr>
	   <tr>
	    <td colspan=4 align=left class='textabdatcla'>
		  <table width="100%" class='textabdatcla'>
		    <tr>
		     <td align=left class='textabdatcla'><b>&nbsp;Pagina <font color=blue><%=(pagina+1)%></font> de <font color=blue><%=totalPaginas%></font>&nbsp;</b></td>
		     <td align=right class='textabdatcla'><b>Registros <font color=blue><%=de%></font> - <font color=blue><%=A%></font></b>&nbsp;</td>
		    </tr>
		  </table>
		</td>
	  </tr>
	</table>

     <table border="0" width="760" cellspacing="2" cellpadding="3" class="tabfonbla">
	   <tr>
	     <td class="tittabdat" align="center">Secuencia</td>
		 <td class="tittabdat" align="center">Fecha de aplicaci&oacute;n</td>
		 <td class="tittabdat" align="center">Cod.Operaci&oacute;n</td>
		 <td class="tittabdat" align="center">Banco</td>
		 <td class="tittabdat" align="center">Importe aplicar</td>
		 <td class="tittabdat" align="center">Cta. TESOFE</td>
		 <td class="tittabdat" align="center">Folio</td>
		 <td class="tittabdat" align="center">Referencia</td>
		 <td class="tittabdat" align="center">Beneficiciario</td>
		 <td class="tittabdat" align="center">RFC</td>
	   </tr>
	 <%
	 		 System.out.println("Generando pantalla");
	 		 System.out.println("Registros: " + numeroRegistros);

	 		 BufferedReader archImp = new BufferedReader( new FileReader(nombreArchivoImp) );
	 		 MTE_Registro Registro;
	 		 String line="";
	 		 String clase="";
	 		 String fecha="";



	 		 line=archImp.readLine(); //Encabezado ....
	 		 Registro=new MTE_Registro();
	 		 for(int a=0;a<fin;a++)
	 		  {
	 	if(a>=inicio)
	 	{
	 		 /*for( int a=0;a<Integer.parseInt(numeroRegistros.trim());a++ )
	 		  {*/
	 	 line=archImp.readLine();
	 	 Registro.parse(line);

	 	 int diaC=Registro.fechaAplicacion.get(Calendar.DATE);
	 	 int mesC=Registro.fechaAplicacion.get(Calendar.MONTH)+1;
	 	 int anioC=Registro.fechaAplicacion.get(Calendar.YEAR);

	 	 fecha= " "+diaC;
	 		     fecha+="/"+mesC;
	 	 fecha+="/"+anioC;

	 	 clase=(a%2==0)?"textabdatobs":"textabdatcla";
	 %>
		   <tr>
			 <td class="<%=clase%>" align="center"><%=Registro.numeroSecuencia%></td>
			 <td class="<%=clase%>" align="center"><%=fecha%></td>
			 <td class="<%=clase%>" align="center"><%=Registro.codigoOperacion%></script></td>
			 <td class="<%=clase%>" align="left"><%=Registro.banco%></td>
			 <td class="<%=clase%>" align="right"><%=FormatoMoneda.formateaMoneda(Registro.importeAplicar/100)%></td>
			 <td class="<%=clase%>" align="center"><%=Registro.cuentaTesofe%></td>
			 <td class="<%=clase%>" align="center"><%=Registro.folio%> </td>
			 <td class="<%=clase%>" align="center"><%=Registro.referencia%></td>
			 <td class="<%=clase%>" align="left"><%=Registro.beneficiario%></td>
			 <td class="<%=clase%>" align="left"><%=Registro.RFC%></td>
		   </tr>
	 <%
			}
		  }
	 %>

     </table>
	<%
	   System.out.println("/Enlace/jsp/MMC_ImportaDetalle.jsp-> Se genera la paginacion");
	   out.print(botones(totalReg,regPorPagina,pagina,totalPaginas));
	 %>
	 <table align=center border=0 cellspacing=0 cellpadding=0>
	  <tr>
	    <td><br></td>
	  </tr>
	  <tr>
	   <td align=center><A href = "javascript:window.close();" border = 0><img src="/gifs/EnlaceMig/gbo25200.gif" border=0 alt="Cerrar"></a></td>
	  </tr>
	 </table>

   </td>
 </tr>
</table>

 <input type=hidden name=nombreArchivoImp value='<%=nombreArchivoImp%>'>
 <input type=hidden name=numeroRegistros value='<%=numeroRegistros%>'>
 <input type=hidden name=claveEmisora value='<%=claveEmisora%>'>
 <input type=hidden name=codigoOperacion value='<%=codigoOperacion%>'>
 <input type=hidden name=fechaPresentacion value='<%=fechaPresentacion%>'>
 <input type=hidden name=tipoArchivo value='<%=tipoArchivo%>'>
 <input type=hidden name=pagina value='<%=pagina%>'>

 </FORM>
</body>
</html>


<%!
String botones(int numeroRegistros,int regPorPagina,int pagina,int totalPaginas)
{
	StringBuffer botones=new StringBuffer("");

	if(numeroRegistros>regPorPagina)
	   {
		  System.out.println("MMC_ImportaDetalle.jsp-> Estableciendo links para las paginas");
		  botones.append("\n <br>");
		  botones.append("\n <table width=620 border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
		  System.out.println("MMC_ImportaDetalle.jsp-> Pagina actual = "+pagina);
		  if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
		  if(totalPaginas>=2)
		   {
			 for(int i=1;i<=totalPaginas;i++)
			  if(pagina+1==i)
			   {
				 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
				 botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a> ");
			   }
		   }
		  if(numeroRegistros>((pagina+1)*regPorPagina))
			 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");

		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");
	   }
	 return botones.toString();
}

   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }

public String FormatoMoneda ( double cantidad )
 {
   String formato = "";
   String language = "la"; // la
   String country = "MX";  // MX

   Locale local = new Locale (language,  country);
   NumberFormat nf = NumberFormat.getCurrencyInstance (local);
   if (cantidad < 0)
    {
       formato = nf.format ( cantidad );
       try
        {
          if (!(formato.substring (0,1).equals ("$")))
            formato ="$ -"+ formato.substring (2,formato.length ());
        }
       catch(NumberFormatException e) {formato="$ 00.00";}
    }
   else
    {
       formato = nf.format ( cantidad );
       try
        {
          if (!(formato.substring (0,1).equals ("$")))
            formato ="$ "+ formato.substring (1,formato.length ());
        }
       catch(NumberFormatException e) {formato="$ 00.00";}
    }
   return formato;
 }

public String FormatoMoneda ( String cantidad )
 {
   String language = "la"; // ar
   String country = "MX";  // AF
   String formato = "";

   Locale local = new Locale (language,  country);
   NumberFormat nf = NumberFormat.getCurrencyInstance (local);

   if(cantidad ==null ||cantidad.equals (""))
       cantidad="0.00";

   double importeTemp = 0.00;
   importeTemp = new Double (cantidad).doubleValue ();
   if (importeTemp < 0)
    {
       try
       {
         formato = nf.format (new Double (cantidad).doubleValue ());
		 if (!(formato.substring (0,1).equals ("$")))
			 formato ="$ -"+ formato.substring (2,formato.length ());
       }
       catch(NumberFormatException e) {formato="$ 00.00";}
    }
   else
    {
       try
        {
		  formato = nf.format (new Double (cantidad).doubleValue ());
		  if (!(formato.substring (0,1).equals ("$")))
			 formato ="$ "+ formato.substring (1,formato.length ());
        }
       catch(NumberFormatException e)
       {formato="$ 00.00";}
     }
    if(formato==null)
       formato = " 00.00";
   return formato;
 }

public String formateaCampoImporteDer(String campoFormateado,int tamanio)
	{
	  String num="0000000000000000";

	  if(campoFormateado.length()<tamanio)
	    campoFormateado=num.substring(0,tamanio-campoFormateado.length())+campoFormateado;

	  return campoFormateado;
	}

public String formateaCampoImporteIzq(String campoFormateado,int tamanio)
	{
	  String num="0000000000000000";

	  if(campoFormateado.length()<tamanio)
	    campoFormateado=campoFormateado+num.substring(0,tamanio-campoFormateado.length());

	  return campoFormateado;
	}
%>