<%@page contentType="text/html"%>

<jsp:useBean id="Mensaje" class="java.lang.String" scope="request"/>

<html>
<head>
<title>Enlace Pago de Linea</title>
<script type="text/javascript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script type="text/javascript">

var fechaLib;
var fechaLim;
var modulo = 'Registro';

/******************************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/

function VerificaFechas(txtLibramiento, txtLimite)
 {

 lib_year         = parseInt(txtLibramiento.value.substring(6,10),10);
 lib_month        = parseInt(txtLibramiento.value.substring(3, 5),10);
 lib_date         = parseInt(txtLibramiento.value.substring(0, 2),10);
 fechaLibramiento = new Date(lib_year, lib_month, lib_date);

 lim_year     = parseInt(txtLimite.value.substring(6,10),10);
 lim_month    = parseInt(txtLimite.value.substring(3, 5),10);
 lim_date     = parseInt(txtLimite.value.substring(0, 2),10);
 fechaLimite  = new Date(lim_year, lim_month, lim_date);

     if( fechaLibramiento > fechaLimite)
      {
        //alert("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.");
		cuadroDialogo("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.", 3);
        return false;
      }

  return true;

 }


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}


<%= request.getAttribute("newMenu") %>

// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			document.getElementById(elemento).value = obtenerCuenta(valor);
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
    	return "";
    };

    if(res.length>1){
	    return res[0] + "-" +  res[2] ;
    }
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES



/***********************************
* funcion para la verificacion de datos
* de registros anteriores de SUA LC
*
* <VC> LFER 17-08-2009
************************************/
function enviarLC(){
	obtenerCuentaSeleccionada("comboCuenta0", "cboCuentaCargo");

	if(document.CapturaLC.cboCuentaCargo.value!=null &&
		document.CapturaLC.cboCuentaCargo.value!=""){
		//Validar cantidad
		if(document.CapturaLC.lineaCaptura.value!=null &&
			document.CapturaLC.lineaCaptura.value!=""){
			if(validaLC2(document.CapturaLC.lineaCaptura.value)){
		   		while(document.CapturaLC.importeCargoLC.value.indexOf(" ")>=0){
					document.CapturaLC.importeCargoLC.value=document.CapturaLC.importeCargoLC.value.replace(' ','');
				}
				while(document.CapturaLC.importeCargoLC.value.indexOf(",")>=0){
					document.CapturaLC.importeCargoLC.value=document.CapturaLC.importeCargoLC.value.replace(',','');
				}
				if(validaCantidad(document.CapturaLC.importeCargoLC.value)){
				   	// Quita espacios y comas antes de enviar la linea de Captura

					document.CapturaLC.opcionLC.value=2;
			 	    document.CapturaLC.submit();
				}else{
					return;
				}
			}else{
				return;
				//alert("La linea de Captura es Incorrecta");
			}
		}else{
			cuadroDialogo("Falta la L�nea de Captura", 3);
		}
	}else{
		cuadroDialogo("Seleccione una cuenta de cargo", 3);

	}

	return;
}


/***********************************
* funcion para la limpieza de los campos
*
* <VC> LFER 17-08-2009
************************************/
var countpunto=0;
function limpiarLC(){
	// document.CapturaLC.CuentaCargo.value='';
	 document.CapturaLC.cboCuentaCargo.value="";
	 document.CapturaLC.importeCargoLC.value='';
	 document.CapturaLC.lineaCaptura.value='';
	 countpunto=0;
	 return;
}

function llenaDigitos(){

	var importe=document.CapturaLC.importeCargoLC.value;
	if(importe == "."){
		importe="0.01";
		document.CapturaLC.importeCargoLC.value="0.01";

	}else if(importe.substring(0,1)== "."){
		importe="0"+importe;
		document.CapturaLC.importeCargoLC.value=importe;
	}

	if(importe.indexOf('.')>0){
		//contiene punto decimal -Validar los dos decimales

			if(importe.indexOf('.') == (importe.length-1)){
				document.CapturaLC.importeCargoLC.value=importe +'00';
			}else{
				if((importe.indexOf('.')+1) == (importe.length-1)){
					document.CapturaLC.importeCargoLC.value=importe +'0';
				}
			}
	}else{
		//no tiene punto decimal agregarlos
		if(importe!=null &&  importe!= "" && importe.length>0){
			if(importe.length<9){
				document.CapturaLC.importeCargoLC.value=importe +'.00';
			}else{
				document.CapturaLC.importeCargoLC.value=importe.substring(0,9) +'.' +importe.substring(9,importe.length);
			}
		}
	}
	importe=document.CapturaLC.importeCargoLC.value;

	if( (importe.indexOf('.')+2)<(importe.length-1) ){
		document.CapturaLC.importeCargoLC.value=importe.substring(0,importe.indexOf('.')+3);
	}
	if(importe==null || importe.length==0){
		countpunto=0;
	}

}

function actualizacuenta()
{
 // document.CapturaLC.CuentaCargo.value=ctaselec +"  "+ctadescr;
  document.CapturaLC.cboCuentaCargo.value=ctaselec+"-"+ ctadescr;


}

/***********************************
*Validacion de los campos
 e - evento
 val - tipo de validacion
*
* <VC> LFER 18-08-2009
************************************/
function validaCampos(e,val) { // 1
	tecla = (document.all) ? e.keyCode : e.which; // 2

	if (tecla==8 || tecla==9 || tecla==13 || tecla==16
	   || tecla==39  || tecla==0) return true; // 3
	//4
	var patron=/[A-Za-z\s\d ��������������./+=?!*#$��_():\[\]\-\,\;]/;
	if(val==1){
		patron=/[A-Za-z\d]/;
	}
	if(val==2){
		patron=/[\d]/;
	}
	if(val==3){
		patron=/[\d.]/;

		if(tecla==46){
			if(countpunto>0){
				if(document.CapturaLC.importeCargoLC.value==null
				  || document.CapturaLC.importeCargoLC.value==""
				  || document.CapturaLC.importeCargoLC.value.indexOf(".")<0){
					countpunto=0;
				}else{
					return false;
				}
			}
			countpunto=1;
		}
	}

	te = String.fromCharCode(tecla); // 5
	return patron.test(te); // 6
}

function validarLC(e,val,valor) {
	if(valor.value.length >= 53 ){
		tecla = (document.all) ? e.keyCode : e.which;
		//alert(tecla);
		if(tecla==8 || tecla==0){
		//&& tecla!=9 && tecla!=13 && tecla!=16 && tecla!=37 	&& tecla!=38 && tecla!=39 && tecla!=40 &&
	  		return true;
	  	}
		return false;
	}
	return validaCampos(e,val)
}
function validaLC2(te){

	var patron2=/[A-Za-z\d]{53}/;
	if(te.length>53){
		alert("La L�nea de Captura no debe ser mayor de 53 caracteres");
		return false;// No se permite continuar 09/2012
	}
	if(te.length<53){
		alert("La L�nea de Captura no debe ser menor de 53 caracteres");
		return false;  // No se permite continuar 09/2012
		//return true;//Se deja seguir el flujo cambio pedido por el usuario
	}

	if(patron2.test(te)){
		return true;
	}else{
		alert("La linea de Captura contiene caracteres invalidos");
		return false;
	}

}
/**********************************************************
* Para convertir las letras a mayusculas
* <VC> LFER 18-08-2009
**********************************************************/
function upperMe(e) {
	tecla = (document.all) ? e.keyCode : e.which;
	if(tecla!=8 && tecla!=9 && tecla!=13 && tecla!=16
	   && tecla!=39  && tecla!=0){
		document.CapturaLC.lineaCaptura.value = document.CapturaLC.lineaCaptura.value.toUpperCase();
	}
	//upperME(document.CapturaLC.lineaCaptura);
	//campo.value = campo.value.toUpperCase();
}
/***********************************
*Validacion de las cantidades
* de 1.00 a 99999999.99
* <VC> LFER 18-08-2009
************************************/
function validaCantidad(val) {

	var patron=/[\d{1,8}\.\d{2}]/;
	var patronDec=/\d+\.{1}\d{2}/;
	if(patron.test(val) && patronDec.test(val)){
		if(val<0.01 || val> 99999999.99){
			alert('La Cantidad a pagar es Incorrecta el rango es de 0.01 a 99999999.99');
			return false;
		}

		return true;
	}else{
		alert('La Cantidad a pagar es Incorrecta');
		return false;
	}


}


/***********************************
* funcion para lallamada a busqueda
* de registros anteriores de SUA LC
*
* <VC> LFER 19-08-2009
************************************/
function anterioresLC(){
	//alert('En construccion');
	document.CapturaLC.opcionConSUA.value="1";
	document.CapturaLC.action="SUAConsultaPagoServlet?opcionLC=1&opcionConSUA=1";
	document.CapturaLC.submit();
	return;

}
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
onLoad="consultaCuentas(0,2,1);MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%if (!Mensaje.equals("")) out.println("cuadroDialogo('" + Mensaje + "', 1)");%>" >
<%-- modificar para cuando es NomBen y cuando es no registrado ya que lo esta tomando para nomben y este no existe obviamente --%> <%-- if (escribe && request.getAttribute("Escibe" ) == null) out.print ("frmRegistro.NomBen.value = " + Pago.getClaveBen () + ";"); --%>


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <%-- MENU PRINCIPAL --%>
       <%= request.getAttribute("MenuPrincipal") %></td>
  </tr>
</TABLE>
 <%= request.getAttribute("Encabezado") %>

 <form name="CapturaLC"  method="post" action="SUALineaCapturaServlet">
 <input type="hidden" name="opcionLC"/>
 <input type="hidden" name="opcionConSUA"/>
<TABLE>
  		<TR>
  			<TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
  			<TD>
	   			<table width="650" border="0" cellspacing="5" cellpadding="0">
		  			<%--tr>
		    			<td colspan="3" align="center" width="596" valign="top">Contrato : <%=request.getAttribute("contrato") %></td>
		    		</tr--%>
		    		<tr>
		    			<td colspan="3">&nbsp;</td>
		    		</tr>
		    		<tr>
		    			<td colspan="3" align="left" class="tittabdat">Capture los datos de su Transferencia</td>
		    		</tr>
		    		<tr valign="top" >
    					<td colspan="3">
		          			<%@ include file="/jsp/filtroConsultaCuentasPRSAT.jspf" %>
		          		</td>
    				</tr>
		    		<tr align="center">
		    			<td>
							<div style="background-color: #D8D8D8; width: 100%">
		    				<table width="100%" height="100%" class="textabdatcla">
								<tr>
		    						<td width="15%"></td>
		    						<td width="32%" class="tabmovtexbol">Cuenta de cargo:</td>
		    						<td width="15%"></td>
		    					</tr>
								<tr>
		    						<td></td>
		    						<td colspan="2"><%-- <input type="text" tabindex="3" name="CuentaCargo"  class="tabmovtexbol"  size="25" onfocus="blur();">
		    						 	<A tabindex="4" HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle/></A>
					    				<input type="hidden"  name="cboCuentaCargo" /> --%>
					    				<div id="CtaNombre"></div>

					    				<%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
 										<input type="hidden" name="cboCuentaCargo" id="cboCuentaCargo" value=""/></td>

		    					</tr>
		    					<tr>
		    						<td width="15%"></td>
		    						<td width="70%" colspan="3" class="tabmovtexbol">L&iacute;nea de Captura:</td>
		    						<td width="15%"></td>
		    					</tr>
		    					<tr>
		    						<td></td>
		    						<td colspan="3"><input type="text" tabindex="1" name="lineaCaptura" size="66" maxlength="53" onkeypress="return validarLC(event,1,this);" onkeyup="upperMe(event);"/></td>
		    						<td></td>
		    					</tr>
		    					<tr>
		    						<td width="15%"></td>
		    						<td width="32%" class="tabmovtexbol">Importe:</td>
		    						<td width="6%"></td>

		    					</tr>
		    					<tr>
		    						<td></td>
		    						<td><input type="text" tabindex="2" name="importeCargoLC" size="25" maxlength="13" onkeypress="return validaCampos(event,3);"  onblur="llenaDigitos();"/></td>
		    						<td></td>
		    						<td>



					    			</td>
		    						<td></td>
		    					</tr>
		    					
		    					
		    					<tr>
		    						<td colspan="5">&nbsp;</td>
		    					</tr>
		    				</table>
							</div>
		    			</td>
		    		</tr>
		    		<tr>
		    			<td align="center">
		    				<table cellpadding="0" cellspacing="0">
								<tr>
									<td>&nbsp;</td>
								</tr>
		    					<tr>
		    						<td valign="top">
		    							<a tabindex="5" href="javascript:enviarLC();"><img  style=" border: 0;" name="enviarLC" src="/gifs/EnlaceMig/gbo25520.gif" alt="Enviar"/></a>
		    						</td>

		    						<td valign="top">
		    							<a tabindex="6" href="javascript:limpiarLC();"><img style=" border: 0;" name="limpiarLC" src="/gifs/EnlaceMig/gbo25250.gif" alt="Limpiar"/></a>
		    						</td>

		    						<td valign="top">
		    							 <a tabindex="7" href="javascript:anterioresLC();"><img style=" border: 0;" name="anterioresLC" src="/gifs/EnlaceMig/gbo25400.gif" alt="Consultar Anteriores"/></a>

		    						</td>
		    					</tr>
		    				</table>
		    			</td>
		    		</tr>
		    	</table>
			</TD>
		</TR>
	</TABLE>
</form>
<%if(request.getAttribute("mensaje") != null) {%>
			<script type="text/javascript">
			<%=request.getAttribute("mensaje")%>
			</script>
		<%}%>
</BODY>
</HTML>