<!-- JSP encargada de las modificaciones sobre un registro en el archivo -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> Alta de Proveedor </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">

<SCRIPT LANGUAGE="Javascript">
 function js_mandar() {

	 document.Forma.submit();
 }
</SCRIPT>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>
<body>
 <CENTER class="titpag" > Modificacion de Proveedor </CENTER>
<%! String registro, Reg;
    String archivo;
	boolean sepudo;
%>
 <%! public void Escribe( java.io.FileWriter dato, String cadena)                                                         throws Exception {
	String thiscadena;
	if ( (cadena.equals("")) || (cadena == null) )
         thiscadena = "nada";
	else
		thiscadena = cadena;
      try {
		  for (int i= 0; i < thiscadena.length(); i++)
            dato.write(
			  thiscadena.charAt(i));
      } catch(Exception e) {}

    }
%>

<%! String Saca(String cadena, int num) {
    int longitud, m,i, posinicial, posfinal;
	 m = i = 0;
	 posinicial = 0;
	      while(m <= num) {
         	if (cadena.charAt(i) == ';')
				m++;
			if (m == (num-1))
				posinicial = i;
			i++;
          }
		  posfinal = i;
     if (num ==0) return cadena.substring(posinicial, posfinal-1);
     else return cadena.substring(posinicial+2, posfinal-1);
   }
%>

<%
  sepudo = true;
  try {
   registro = request.getParameter("Registro");
   archivo = request.getParameter("archivo");
   java.io.FileInputStream fis = new java.io.FileInputStream(Global.DOWNLOAD_PATH + archivo);
   java.io.DataInputStream dis = new java.io.DataInputStream(fis);


              String cadena = new String("algo");
				String c;
				c = "";

	             int contador = 0;
                      while (cadena !=null)  {
		                cadena = dis.readLine();
		                contador++;
                      }
              int numRegistros;
	          numRegistros = contador-1;
	          dis.close();
        	  fis.close();

  java.io.FileInputStream Fis = new java.io.FileInputStream(Global.DOWNLOAD_PATH +archivo);
  java.io.DataInputStream Dis = new java.io.DataInputStream(Fis);

   String pasar;
   pasar = "";
  for (int h = 0; h < numRegistros; h++) {
	 pasar = Dis.readLine();
        if (Saca(pasar, 18).equals(registro)) {
			Reg = pasar;
		}

  }
  out.println(Reg);
   Fis.close();
   Dis.close();

  %>




<TABLE WIDTH="760" BORDER="0" CELLSPACING="0" CELLPADDING="0" >
 <FORM  NAME="Forma" METHOD="GET" ACTION="Alta.jsp">

 <tr>
   <td align="center">
     <table width="680" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td rowspan="2">

           <table width="660" border="0" cellspacing="2" cellpadding="3">
		   <P class="tabmovtex">* El llenado de estos campos es obligatorio

<!--Datos del Proveedor-->

			<tr>
	         <td class="tittabdat" colspan="2"> Capture los datos del proveedor </td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD></TD>
    <TD class="tabmovtexbol" nowrap align="CENTER">
		<INPUT TYPE="RADIO" NAME="Persona" VALUE="F�sica" checked><B>Persona f�sica</B>
		<INPUT TYPE="RADIO" NAME="Persona" VALUE="Moral"><B>Persona moral</B>
	</TD>
    <TD></TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Clave de Proveedor
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Codigo del cliente
     </TD>
    </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vClave" value = "<%=Saca(Reg,0)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCodigo" value = "<%=Saca(Reg,1)%>">
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Apellido Paterno
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Apellido Materno
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Apellido Nombre
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vPaterno" value = "<%=Saca(Reg,15)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vMaterno" value = "<%=Saca(Reg,16)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vNombre" value = "<%=Saca(Reg,17)%>">
     </TD>
   </TR>

 	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* RFC
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Homoclave
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vRFC" value = "<%=Saca(Reg,18)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vHomoclave" value = "<%=Saca(Reg,19)%>">
     </TD>
   </TR>

   <TR>
	 <TD  class="tabmovtexbol" nowrap>
		Persona de contacto en la empresa:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Numero de cliente para Factoraje Santander:
     </TD>
    </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vContacto" value = "<%=Saca(Reg,2)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vFactoraje" value = "<%=Saca(Reg,3)%>">
     </TD>
   </TR>
 </TABLE>
	</td>
  </tr>


			<tr>
			    <td class="tittabdat" colspan="2"> Domicilio</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap>
		* Calle y n�mero
	</TD>
    <TD class="tabmovtexbol" nowrap>
		Colonia
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vCalle" value = "<%=Saca(Reg,20%>">
	</TD>
	</TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vColonia" value = "<%=Saca(Reg,21)%>">
	</TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Deleg. o Municipio
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Cd. o Poblaci�n
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vDelegacion" value = "<%=Saca(Reg,22)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="Ciudad" value = "<%=Saca(Reg,23)%>">
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* C.P.
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Estado:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Pa�s
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCodigoPostal" value ="<%=Saca(Reg,24)%>">
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vEstado" value = "<%=Saca(Reg,25)%>">
		 <OPTION VALUE="AGUASCALIENTES">AGUASCALIENTES
		 <OPTION VALUE="ALABAMA">Distrito Federal
		 <OPTION VALUE="ALASKA">Durango
		 <OPTION VALUE="ALBERTA">Tamaulipas
		 <OPTION VALUE="ARIZONA">Yucatan
		 <OPTION VALUE="ARKANSAS">Chiapas
		 <OPTION VALUE="BAJA CALIFORNIA NORTE">Distrito Federal
		 <OPTION VALUE="BAJA CALIFORNIA SUR">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		 <OPTION VALUE="Chiapas">Chiapas
		 <OPTION VALUE="Distrito Federal">Distrito Federal
		 <OPTION VALUE="Durango">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		 <OPTION VALUE="Chiapas">Chiapas
		 <OPTION VALUE="Distrito Federal">Distrito Federal
		 <OPTION VALUE="Durango">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		 <OPTION VALUE="Chiapas">Chiapas
		 <OPTION VALUE="Distrito Federal">Distrito Federal
		 <OPTION VALUE="Durango">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		</SELECT>
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vPais" value = "<%=Saca(Reg,26)%>">
		 <OPTION VALUE="Mexico">M�xico
 		 <OPTION VALUE="Uruguay">Uruguay
		 <OPTION VALUE="Zimbawe">Zimbawe
		</SELECT>
     </TD>
   </TR>

 </TABLE>

	</TD>
   </TR>



  <!--Medio de confirmaci�n-->

			<tr>
			    <td class="tittabdat" colspan="2"> Medio de confirmaci�n</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap>
		* Medio de Informaci�n
	</TD>
    <TD class="tabmovtexbol" nowrap>
		email
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vMedio" value = "<%=Saca(Reg,4)%>">
		 <OPTION VALUE="correspondecia">Correspondencia
		 <OPTION VALUE="email">Email
		 <OPTION VALUE="fax">Fax
		</SELECT>
	</TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vEmail" value = "<%=Saca(Reg,32)%>">
	</TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Lada o prefijo
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* N�mero telef�nico
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vLada" value = "<%=Saca(Reg,27)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vTelefonico" value = "<%=Saca(Reg,28)%>">
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Extensi�n Tel.
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Fax
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Extensi�n de fax
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vExTelefono" value = "<%=Saca(Reg,29)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vFax" value = "<%=Saca(Reg,30)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vExFax" value = "<%=Saca(Reg,31)%>">
     </TD>
   </TR>
 </TABLE>

	</TD>
   </TR>


  <!--Forma de pago-->

			<tr>
			    <td class="tittabdat" colspan="2"> Forma de pago</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Forma de pago
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		No. cta cheques:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Divisa
     </TD>

   </TR>

	<TR>

    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vForma" value = "<%=Saca(Reg,5)%>">
		 <OPTION VALUE="Cheque">Cheque
		 <OPTION VALUE="Ventanilla">Orden de pago en ventanilla
		 <OPTION VALUE="Oficinas">Se pagara en las oficinas de cliente
		 <OPTION VALUE="Interbancaria">Transferencia interbancaria
		 <OPTION VALUE="Interna">Transferencia interna
		</SELECT>
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCuenta" value = "<%=Saca(Reg,6)%>">
     </TD>

   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Banco
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Sucursal
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Plaza
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vBanco" value = "<%=Saca(Reg,7)%>">
		 <OPTION VALUE="">
		</SELECT>
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vSucursal" value = "<%=Saca(Reg,8)%>">
    </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<SELECT NAME="vPlaza" value = "<%=Saca(Reg,9)%>">
		 <OPTION VALUE="">
		</SELECT>
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		L�mite de financiamiento:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		No. de deudores activos:
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vLimite" value = "<%=Saca(Reg,10)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vDeudores" value = "<%=Saca(Reg,11)%>">
     </TD>
   </TR>


   	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Volumen anual de ventas
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Importe medio de facturas emitidas
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vVolumen" value = "<%=Saca(Reg,12)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vImporte" value = "<%=Saca(Reg,13)%>">
     </TD>
   </TR>

 </TABLE>

	</TD>
   </TR>


	<tr align="center">
       <td valign="top" colspan="2">
          <table width="650" border="0" cellspacing="0" cellpadding="0">

			<tr><td>
				<P class="tabmovtex">* El llenado de estos campos es obligatorio
			</td></tr>

		  <tr>
		   <td align="center" valign="middle" width="66">

			  <A href = "javascript:js_mandar();" border=0><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a><A href = "" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="77" height="22"></a><A href = "mantenimiento2.jsp" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77" height="22"></a>


         </td>
		  </tr>

		  </table>
		</td>
	</tr>

 </TABLE>

	</TD>
   </TR>
</TABLE>

 </FORM>
</TABLE>





	 <%
  Dis.close();
  Fis.close();
  dato.close();
  Fos.close();





  %>





<%
  sepudo = true;

   registro = arregloCampos[0];
   archivo = request.getParameter("archivo");
   fis = new java.io.FileInputStream(Global.DOWNLOAD_PATH + archivo);
   dis = new java.io.DataInputStream(fis);
   pos = 1;
   cad = new String("algo");
   con = 1;
   out.println(registro);

    while(cad != null) {
		cad = dis.readLine();
		//out.println(cad);
		if (cad!=null)
		  if (cad.equals(registro))
			 pos = con;
        con++;
	}
	//out.print(String.valueOf(con) + "  " + String.valueOf(pos));

  fis.close();
  dis.close();

  Fis = new java.io.FileInputStream(Global.DOWNLOAD_PATH + archivo);
  Dis = new java.io.DataInputStream(Fis);
  Fos = new java.io.FileOutputStream(Global.DOWNLOAD_PATH+"temp.txt");
  dato = new java.io.FileWriter(Global.DOWNLOAD_PATH+"temp.txt");

  for(int m = 1; m < (con-1) ; m++) {
	  if  ( (m < pos) || (m >= (pos + 32))  )  {
          Escribe(dato, Dis.readLine());
	      Escribe(dato, "\n");
      }
	  else
         f = Dis.readLine();
  }
  Dis.close();
  Fis.close();
  dato.close();
  Fos.close();


  java.io.File ar = new java.io.File(Global.DOWNLOAD_PATH + archivo);
  boolean m;
  m =  ar.delete();
  if (!m) out.print("Hubo pedos al borrar el archivo");
  java.io.File ar2 = new java.io.File(Global.DOWNLOAD_PATH+"temp.txt");
  java.io.File ar3 = new java.io.File(Global.DOWNLOAD_PATH + archivo);
  ar2.renameTo(ar3);
/*
  //ar.close();
  java.io.FileOutputStream FIS = new java.io.FileOutputStream(archivo);
  java.io.FileWriter FW = new java.io.FileWriter(archivo);
  java.io.FileInputStream filein = new java.io.FileInputStream("temp.txt");
  java.io.DataInputStream datain = new java.io.DataInputStream(filein);
  String cadena = "algo";

  while (cadena != null)
     Escribe(FW, datain.readLine());
  datain.close();
  filein.close();
  FIS.close();
  FW.close();
   */
 }
 catch(Exception e) { sepudo = false;
                        out.print(e.toString()); }
%>


</BODY>
</HTML>