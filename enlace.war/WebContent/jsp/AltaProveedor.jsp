<%@ page import="EnlaceMig.*"%>
<%@ page import="java.util.*"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> Alta de Proveedor </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>

<SCRIPT LANGUAGE="Javascript">

 function js_mandar() {
	 document.Forma.submit();
 }

<%= request.getAttribute("newMenu") %>

</SCRIPT>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<BODY>

<CENTER class="titpag" > Alta de Proveedor </CENTER>

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">


  <tr valign="top">


	<td width="*">


	 <!-- MENU PRINCIPAL -->

     <%=request.getAttribute("MenuPrincipal")%>



	</td>


   </tr>


</table>


<%=request.getAttribute("Encabezado")%>


<br>


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">

     </TD>
   </TR>
</TABLE>


<TABLE WIDTH="760" BORDER="0" CELLSPACING="0" CELLPADDING="0" >
 <FORM  NAME="Forma" METHOD="POST" ACTION="http://localhost:8080/examples/jsp/Alta/Alta.jsp">

 <tr>
   <td align="center">
     <table width="680" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td rowspan="2">

           <table width="660" border="0" cellspacing="2" cellpadding="3">
		   <P class="tabmovtex">* El llenado de estos campos es obligatorio

<!--Datos del Proveedor-->

			<tr>
	         <td class="tittabdat" colspan="2"> Capture los datos del proveedor </td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD></TD>
    <TD class="tabmovtexbol" nowrap align="CENTER">
		<INPUT TYPE="RADIO" NAME="Persona" VALUE="F�sica" checked><B>Persona f�sica</B>
		<INPUT TYPE="RADIO" NAME="Persona" VALUE="Moral"><B>Persona moral</B>
	</TD>
    <TD></TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Clave de Proveedor
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Codigo del cliente
     </TD>
    </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vClave">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCodigo">
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Apellido Paterno
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Apellido Materno
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Apellido Nombre
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vPaterno">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vMaterno">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vNombre">
     </TD>
   </TR>

 	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* RFC
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Homoclave
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vRFC">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vHomoclave">
     </TD>
   </TR>

   <TR>
	 <TD  class="tabmovtexbol" nowrap>
		Persona de contacto en la empresa:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Numero de cliente para Factoraje Santander:
     </TD>
    </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vContacto">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vFactoraje">
     </TD>
   </TR>
<!--
 	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Opci�n de anticipo de pagos
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="RADIO" NAME="vOPCION" VALUE="Si">S�
		<INPUT TYPE="RADIO" NAME="vOPCION" VALUE="No" checked>No
     </TD>
   </TR>
-->
 </TABLE>
	</td>
  </tr>

<!-- Domicilio -->
			<tr>
			    <td class="tittabdat" colspan="2"> Domicilio</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap>
		* Calle y n�mero
	</TD>
    <TD class="tabmovtexbol" nowrap>
		Colonia
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vCalle">
	</TD>
	</TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vColonia">
	</TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Deleg. o Municipio
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Cd. o Poblaci�n
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vDelegacion">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="Ciudad">
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* C.P.
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Estado:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Pa�s
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCodigoPostal">
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vEstado">
		 <OPTION VALUE="AGUASCALIENTES">AGUASCALIENTES
		 <OPTION VALUE="ALABAMA">Distrito Federal
		 <OPTION VALUE="ALASKA">Durango
		 <OPTION VALUE="ALBERTA">Tamaulipas
		 <OPTION VALUE="ARIZONA">Yucatan
		 <OPTION VALUE="ARKANSAS">Chiapas
		 <OPTION VALUE="BAJA CALIFORNIA NORTE">Distrito Federal
		 <OPTION VALUE="BAJA CALIFORNIA SUR">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		 <OPTION VALUE="Chiapas">Chiapas
		 <OPTION VALUE="Distrito Federal">Distrito Federal
		 <OPTION VALUE="Durango">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		 <OPTION VALUE="Chiapas">Chiapas
		 <OPTION VALUE="Distrito Federal">Distrito Federal
		 <OPTION VALUE="Durango">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		 <OPTION VALUE="Chiapas">Chiapas
		 <OPTION VALUE="Distrito Federal">Distrito Federal
		 <OPTION VALUE="Durango">Durango
		 <OPTION VALUE="Tamaulipas">Tamaulipas
		 <OPTION VALUE="Yucatan">Yucatan
		</SELECT>
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vPais">
		 <OPTION VALUE="Mexico">M�xico
 		 <OPTION VALUE="Uruguay">Uruguay
		 <OPTION VALUE="Zimbawe">Zimbawe
		</SELECT>
     </TD>
   </TR>

 </TABLE>

	</TD>
   </TR>



  <!--Medio de confirmaci�n-->

			<tr>
			    <td class="tittabdat" colspan="2"> Medio de confirmaci�n</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap>
		* Medio de Informaci�n
	</TD>
    <TD class="tabmovtexbol" nowrap>
		email
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vMedio">
		 <OPTION VALUE="correspondecia">Correspondencia
		 <OPTION VALUE="email">Email
		 <OPTION VALUE="fax">Fax
		</SELECT>
	</TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vEmail">
	</TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Lada o prefijo
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* N�mero telef�nico
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vLada">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vTelefonico">
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Extensi�n Tel.
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Fax
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Extensi�n de fax
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vExTelefono">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vFax">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vExFax">
     </TD>
   </TR>
 </TABLE>

	</TD>
   </TR>


  <!--Forma de pago-->

			<tr>
			    <td class="tittabdat" colspan="2"> Forma de pago</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Forma de pago
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		No. cta cheques:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Divisa
     </TD>

   </TR>

	<TR>

    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vForma">
		 <OPTION VALUE="Cheque">Cheque
		 <OPTION VALUE="Ventanilla">Orden de pago en ventanilla
		 <OPTION VALUE="Oficinas">Se pagara en las oficinas de cliente
		 <OPTION VALUE="Interbancaria">Transferencia interbancaria
		 <OPTION VALUE="Interna">Transferencia interna
		</SELECT>
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCuenta">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vDivisa" SIZE="5">
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vDivisa">
		 <OPTION VALUE="Dolares">Dolares
 		 <OPTION VALUE="Euros">Euros
		 <OPTION VALUE="Pesos">Pesos
		</SELECT>
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Banco
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Sucursal
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Plaza
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vBanco">
		 <OPTION VALUE="">
		</SELECT>
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vSucursal">
    </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<SELECT NAME="vPlaza">
		 <OPTION VALUE="">
		</SELECT>
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		L�mite de financiamiento:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		No. de deudores activos:
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vLimite">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vDeudores">
     </TD>
   </TR>


   	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Volumen anual de ventas
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Importe medio de facturas emitidas
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vVolumen">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vImporte">
     </TD>
   </TR>
<!--
	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Ciudad
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Pa�s
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Lugar de impresi�n:
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vCiudad">
		 <OPTION VALUE="">
		</SELECT>
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<SELECT NAME="vPais">
		 <OPTION VALUE="">
		</SELECT>
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vImpresion">
		 <OPTION VALUE="">
		</SELECT>
     </TD>
   </TR>
-->
 </TABLE>

	</TD>
   </TR>


	<tr align="center">
       <td valign="top" colspan="2">
          <table width="650" border="0" cellspacing="0" cellpadding="0">

			<tr><td>
				<P class="tabmovtex">* El llenado de estos campos es obligatorio
			</td></tr>

		  <tr>
		   <td align="center" valign="middle" width="66">

			  <A href = "javascript:js_mandar();" border=0><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a><A href = "" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="77" height="22"></a><A href = "mantenimiento.jsp" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77" height="22"></a>


         </td>
		  </tr>

		  </table>
		</td>
	</tr>

 </TABLE>

	</TD>
   </TR>
</TABLE>

 </FORM>
</TABLE>

</BODY>
</HTML>
