<HTML>
<HEAD><TITLE>Enlace</TITLE>

<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25630">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="16/05/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->

<script language = "JavaScript"    SRC= "/EnlaceMig/ValidaFormas.js"></script>
<script language = "JavaScript1.2" src= "/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript"    SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<script language="JavaScript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


<%= request.getAttribute("newMenu") %>

function generaComprobante(f)
 {
   param1="tipoComprobante=" + cambiaEspacios(f.tipoComprobante.value);
   param2="decDatos=" + cambiaEspacios(f.decDatos.value);

   vc=window.open("ImpDeclaraComprobante?" + param1+ "&" + param2, 'trainerWindow','width=560,height=470,toolbar=no,scrollbars=no');
   vc.focus();
 }

function cambiaEspacios(datoEntrada)
 {
	var datoSalida="";
	for(i=0; i<datoEntrada.length; i++)
	 {
		if(datoEntrada.substring(i, i+1) == " ")
			datoSalida += "%20";
		else
			datoSalida += datoEntrada.substring(i, i+1);
	 }
	return datoSalida;
 }

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>


<FORM  NAME="frmDeclaracion" method=post>

 <p>
 <table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
  </tr>
 </table>

 <table width="400" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
   <td align="center">
     <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
	   <tr>
	     <td colspan="3"> </td>
	   </tr>
	   <tr>
	     <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
		 <td width="428" height="100" valign="top" align="center">
		   <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">
			 <tr>
			   <td colspan="3">&nbsp;</td>
			 </tr>
			 <tr>
			   <td class="tittabcom" align="center">
			    <%= request.getAttribute("mensaje_salida") %>
			   </td>
			 </tr>
		   </table>
		 </td>
         <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." 	name=".."></td>
	   </tr>
	 </table>
	</td>
  </tr>
 </table>

 <table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
  </tr>
  <tr>
    <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
 </table>

 <table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td align="center">
	 <br>
	 <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
	  <tr>
	    <td align="center">
		 <A href = "javascript:generaComprobante(document.frmDeclaracion);" border=0><img src="/gifs/EnlaceMig/gbocomprobante.gif" border=0 alt="Comprobante" width="114" height="22"></a>
		</td>
	  </tr>
	 </table>
	</td>
  </tr>
 </table>

 <input type=hidden name="tipoComprobante" value="<%= request.getAttribute("tipoComprobante") %>">
  <input type=hidden name="decDatos" value="<%= request.getAttribute("decDatos") %>">

</FORM>

</body>
</html>

