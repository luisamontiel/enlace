<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Movimientos de Cr&eacute;dito</TITLE>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript">

	function cambiaArchivoExport(extencion) {
		var boton = document.getElementById("bExport");
		if (extencion === "txt") {
			document.getElementById("tipoArchivoExportacion").value = extencion;
			document.getElementById("extTxt").checked = true;
		} else if (extencion === "csv") {
			document.getElementById("tipoArchivoExportacion").value = extencion;
			document.getElementById("extCsv").checked = true;
		}
		boton.href = "javascript:exportarMovimientos();";
	}

	function exportarMovimientos() {
		var extencion = document.getElementById("tipoArchivoExportacion").value;
		var tipoModulo = document.getElementById("tipo").value;
		window.open("/Enlace/enlaceMig/MCL_Movimientos?tipoArchivoExport="+ extencion +"&Tipo="+tipoModulo,
						"Movimientos",
						"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	}

	function VentanaAyuda(cad) {
		return;
	}

	/******************  Esto no es mio ***************************************************/

	function MM_preloadImages() { //v3.0
		var d = document;
		if (d.images) {
			if (!d.MM_p)
				d.MM_p = new Array();
			var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
			for (i = 0; i < a.length; i++)
				if (a[i].indexOf("#") != 0) {
					d.MM_p[j] = new Image;
					d.MM_p[j++].src = a[i];
				}
		}
	}

	function MM_swapImgRestore() { //v3.0
		var i, x, a = document.MM_sr;
		for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
			x.src = x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
		var p, i, x;
		if (!d)
			d = document;
		if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
			d = parent.frames[n.substring(p + 1)].document;
			n = n.substring(0, p);
		}
		if (!(x = d[n]) && d.all)
			x = d.all[n];
		for (i = 0; !x && i < d.forms.length; i++)
			x = d.forms[i][n];
		for (i = 0; !x && d.layers && i < d.layers.length; i++)
			x = MM_findObj(n, d.layers[i].document);
		return x;
	}

	function MM_swapImage() { //v3.0
		var i, j = 0, x, a = MM_swapImage.arguments;
		document.MM_sr = new Array;
		for (i = 0; i < (a.length - 2); i += 3)
			if ((x = MM_findObj(a[i])) != null) {
				document.MM_sr[j++] = x;
				if (!x.oSrc)
					x.oSrc = x.src;
				x.src = a[i + 2];
			}
	}

	/********************************************************************************/
<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<FORM  NAME="tabla" method=post action="MDI_Consultar">
 <p>

   <!-- Datos del Usuario -->
   <!--
   <table border=0 align=center>
   <tr>
     <th bgcolor='#DDDDDD'><font color=black>Datos del Usuario</font></th>
   </tr>
   <tr>
     <td align="center">
       <font size="2" face="Arial, Helvetica, sans-serif">
	     <b>Contrato: </b><%= request.getAttribute("NumContrato") %> <%= request.getAttribute("NomContrato") %>
	   </font>
	 </td>
   </tr>
   <tr>
     <td bgcolor='#FFFFFF'>
	   <b>Usuario: </b><%= request.getAttribute("NumUsuario") %> <%= request.getAttribute("NomUsuario") %>
	 </td>
   </tr>
   <tr>
     <td><br></td>
   </tr>
   </table>
   //-->

  <table border=0 align=center cellpadding=0 cellspacing=1>
   <tr>
     <td class="textabref" width="40%">&nbsp;<%= request.getAttribute("Cuenta") %>&nbsp;</td>
	 <td class="textabref" width="20%" align=center>&nbsp;<%= (request.getAttribute("_MovTasaAnual")!=null)?request.getAttribute("_MovTasaAnual"):""%>&nbsp;</td>
	 <td class="textabref" width="40%" align=right><%= request.getAttribute("numMovAbono") %>&nbsp;</td>
   </tr>
   <tr>
     <td class="textabref">&nbsp;<%= request.getAttribute("Fecha") %>&nbsp;</td>
	 <td class="textabref">&nbsp;&nbsp;</td>
	 <td class="textabref" align=right><%= request.getAttribute("numMovCargo") %>&nbsp;</td>
   </tr>
   <tr>
     <td colspan=3><%= request.getAttribute("Tabla") %></td>
   </tr>
   <!--
   <tr>
     <td colspan=2><%= request.getAttribute("Botones") %></td>
   </tr>
   //-->
   <tr>
   <td colspan=3>
   <% if( request.getAttribute("_MovTasaAnual")!=null ) {%>
	  <TABLE>
	  <TR>
		<TD class="texencconbol">** Tasa de Inter&eacute;s Anualizada que se cobra sobre el saldo promedio insoluto del periodo anterior.</TD>
	  </TR>
	  </TABLE>
  <% } %>
  </td>
   </tr>
 </table>

 <input type=hidden name=Pagina value=<%= request.getAttribute("Pagina") %>>
 <input type=hidden name=Numero value=<%= request.getAttribute("Numero") %>>
 <input type=hidden name=FechaIni value='<%= request.getAttribute("FechaIni") %>'>
 <input type=hidden name=FechaFin value='<%= request.getAttribute("FechaFin") %>'>
 <input type=hidden name=Busqueda value='<%= request.getAttribute("Busqueda") %>'>
 <input id="tipo" type=hidden name="Tipo" value='${Tipo}'>
 <input type=hidden name=Modulo value=1>


  <p>
  <table align=center border=0 cellspacing=0 cellpadding=0>
   		<c:if test="${Tipo ne '2'}">
	   		<c:if test="${!empty opcionesExportar}">
	   		<tr>
	   			<td colspan="2" style="text-align: center;">
					${opcionesExportar}
	   			</td>
	   		</tr>
			</c:if>
		</c:if>
   <tr>
     <%
	    String exportar="&nbsp;";
		if(request.getAttribute("Exportar")!= null)
		  exportar=(String)request.getAttribute("Exportar");
	 %>
	 <c:if test="${Tipo ne '2'}">
     	<td><%=exportar%></td>
     </c:if>
     <td colspan = "${Tipo eq '2' ? '2' : '0'}"><A href = "javascript:scrImpresion();" border = 0><img src = "/gifs/EnlaceMig/gbo25240.gif" border = 0 alt="Imprimir"></a></td>
   </tr>
  </table>

</FORM>

</body>
</html>
<!--0.1-->