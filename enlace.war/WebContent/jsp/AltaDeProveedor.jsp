<%@ page import="java.io.*"		%>
<%@ page import="java.util.*"	%>
<%@ page import="EnlaceMig.*"	%>
<%
String parSecuencia = "";	// Secuencia
String parClave		= "";	// * Clave de Proveedor
String parPersona	= "";	// * Persona Fisica o Moral
String parPaterno	= "";	// * Apellido Paterno
String parMaterno	= "";	// Apellido Materno
String parNombre	= "";	// * Nombre
String parTipoSoc	= "";	// Tipo Sociedad
String parRFC		= "";	// * RFC
String parHomoclave = "";	// * Homoclave
String parContacto	= "";	// Persona de contacto en la empresa
String parFactoraje = "";	// Numero de cliente para Factoraje Santander:
String parCalle		= "";	// * Calle y n&uacute;mero
String parColonia	= "";	// * Colonia
String parDelegacion= "";	// * Deleg. o Municipio
String parCiudad	= "";	// * Cd. o Poblaci&oacute;n
String parCP		= "";	// * Codigo Postal
String parEstado	= "";	// Estado
String parPais		= "";	// Pais
String parMedio		= "";	// Medio de Confirmacion
String parEmail		= "";	// Email
String parLada		= "";	// * Lada o prefijo
String parTelefono	= "";	// * Numero telef&oacute;nico
String parExTelefono= "";	// Extension Tel.
String parFax		= "";	// Fax
String parExFax		= "";	// Extension de fax
String parForma		= "";	// Forma de pago
String parCuenta	= "";	// No. cta cheques
String parBanco		= "";	// banco
String parSucursal	= "";	// Sucursal
String parPlaza		= "";	// plaza
String parLimite	= "";	// L&iacute;mite de financiamiento:
String parDeudores	= "";	// Deudores activos:
String parVolumen	= "";	// Volumen anual de ventas
String parImporte	= "";	// Importe medio de facturas emitidas
String parCodigo	= "";	// Codigo del cliente
String parLugarImp	= "";	// Lugar de Impresion

String tituloPantalla	= "";
String tituloHTML		= "";
String boton1	= "";
String boton2	= "";
String altBoton	= "";

mx.altec.enlace.bo.Proveedor proveedor = null;

String parAccion	= (String)request.getAttribute("accion");
String parProveedor	= (String)request.getAttribute("proveedor");
String ApellidoPaterno		= (String)request.getAttribute("ApellidoPaterno");
String Nombre_RazonSocial	= (String)request.getAttribute("Nombre");
String parTipoPersona		= (String)request.getAttribute("tipoPersona");

if(parTipoPersona		== "" ||  parTipoPersona == null)	parTipoPersona	= "F";
if(ApellidoPaterno		== null)	ApellidoPaterno		= "* Apellido Paterno";
if(Nombre_RazonSocial	== null)	Nombre_RazonSocial	= "* Nombre";
if(parAccion	== null)	parAccion	= "";
if(parProveedor	== null)	parProveedor		= "";

if(parAccion.equals("1") || parAccion.equals("10")) {	// Alta de Proveedores en Linea
	boton1	= "gbo25480.gif";
	boton2	= "gbo25250.gif";
	altBoton		= "alta";
	tituloPantalla	= "Alta";
	tituloHTML		= "Alta de Proveedor";
	parAccion		= "10";
}
else if(parAccion.equals("2")) {	// Comsulta y Modificacion de Proveedores en Linea
	boton1	= "gbo25510.gif";
	boton2	= "gbo25500.gif";
	altBoton		= "modificar";
	tituloPantalla	= "Consulta y Modificaci&oacute;n";
	tituloHTML		= "Consulta y Modificaci&oacute;n de Proveedores";
	parAccion		= "2";
}
else if(parAccion.equals("3")) {	// Modificacion de Proveedor
	boton1	= "gbo25510.gif";
	boton2	= "gbo25250.gif";
	altBoton		= "modificar";
	tituloPantalla	= "Modificaci&oacute;n";
	tituloHTML		= "Modificaci&oacute;n de Proveedor";
	parAccion		= "11";
}

// Si la accion a Realizar es igual a 11 (Boton Regresa Modificacion)
if(parAccion.equals("11") || parAccion.equals("2") || parAccion.equals("10"))	{

    proveedor = mx.altec.enlace.bo.Proveedor.SeleccionaTokens(parProveedor,"|");

	parSecuencia	= ( proveedor.getSecuencia()		).trim();	// Secuencia
	parClave		= ( proveedor.getClaveProveedor()	).trim();	// * Clave de Proveedor
	parPersona		= ( proveedor.getMoralFisica()		).trim();	// * Persona Fisica o Moral
	parPaterno		= ( proveedor.getApellidoPaterno()	).trim();	// * Apellido Paterno
	parMaterno		= ( proveedor.getApellidoMaterno()	).trim();	// Apellido Materno
	parNombre		= ( proveedor.getNombre()	).trim();	// * Nombre
	parTipoSoc		= ( proveedor.getTipoSoc()	).trim();	// Tipo de Sociedad
	parRFC	= ( proveedor.getRFC()		).trim();	// * RFC
	parHomoclave	= ( proveedor.getHomoclave()		).trim();	// * Homoclave
	parContacto		= ( proveedor.getPersonaContacto()	).trim();	// Persona de contacto en la empresa
	parFactoraje	= ( proveedor.getNumeroCliente()	).trim();	// Numero de cliente para Factoraje Santander:
	parCalle		= ( proveedor.getCalle_Numero()		).trim();	// * Calle y numero
	parColonia		= ( proveedor.getColonia()	).trim();	// * Colonia
	parDelegacion	= ( proveedor.getDelg_Munc()		).trim();	// * Deleg. o Municipio
	parCiudad		= ( proveedor.getCd_Pob()	).trim();	// * Cd. o Poblaci&oacute;n
	parCP	= ( proveedor.getCP()		).trim();	// * C.P.
	parEstado		= ( proveedor.getEstado()	).trim();	// Estado
	parPais	= ( proveedor.getPais()		).trim();	// pais
	parMedio		= ( proveedor.getMedioConf()		).trim();	// Medio de Confirmacion
	parEmail		= ( proveedor.getEmail()	).trim();	// Email
	parLada	= ( proveedor.getLada()		).trim();	// * Lada o prefijo
	parTelefono		= ( proveedor.getTelefono()	).trim();	// * Numero telefonico
	parExTelefono	= ( proveedor.getExtTel()	).trim();	// Extension Tel.
	parFax	= ( proveedor.getFax()		).trim();	// Fax
	parExFax		= ( proveedor.getExtFax()	).trim();	// Extension de fax
	parForma		= ( proveedor.getFormaPago()		).trim();	// Forma de Pago
	parCuenta		= ( proveedor.getNumCuenta()		).trim();	// No. cta cheques
	parBanco		= ( proveedor.getBanco()	).trim();	// banco
	parSucursal		= ( proveedor.getSucursal()	).trim();	// Sucursal
	parPlaza		= ( proveedor.getPlaza()	).trim();	// plaza
	parLimite		= ( proveedor.getLimiteFinanc()		).trim();	// Limite de financiamiento:
	parDeudores		= ( proveedor.getNumDeudores_Activos()		).trim();	// Deudores activos:
	parVolumen		= ( proveedor.getVolumenAnual_Ventas()		).trim();	// Volumen anual de ventas
	parImporte		= ( proveedor.getImporteMedio_FactEmit()	).trim();	// Importe medio de facturas emitidas
	parCodigo		= ( proveedor.getCodigoCliente()	).trim();	// Codigo del cliente
	parLugarImp		= ( proveedor.getLugarImpresion()	).trim();	// Lugar de Impresion

	if(parAccion.equals("2") || parAccion.equals("11")) {
		if(proveedor.esMoral()) {
	parTipoPersona = "M";
	Nombre_RazonSocial	= "* Raz&oacute;n Social";
		}
		else{parTipoPersona = "F";}
	}
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> <%= tituloHTML %> </TITLE>
<META NAME="Generator"	CONTENT="EditPlus">
<META NAME="Author"		CONTENT="">
<META NAME="Keywords"	CONTENT="">
<META NAME="Description"CONTENT="">

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="/EnlaceMig/cuadroDialogo.js"> </SCRIPT>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<SCRIPT LANGUAGE="Javascript">

function MM_preloadImages() {	//v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {	//v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {		//v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {		//v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

// --- Se ejecuta en cuanto la pagina se carga, inicializa los combos y el campo moral/fisica
function inicia()
{
	document.formulario.vCodigo.disabled	= true;
	document.formulario.vSecuencia.disabled = true;
	document.formulario.vCodigo.blur();
	document.formulario.vSecuencia.blur();

	if(document.formulario.tipoPersona.value == "M") {
		selecciona(document.formulario.vTipoSoc,	"<%= parTipoSoc %>");
	}
	selecciona(document.formulario.vPais,		"<%= parPais	%>");
	selecciona(document.formulario.vMedio,		"<%= parMedio	%>");
	selecciona(document.formulario.vForma,		"<%= parForma	%>");
	selecciona(document.formulario.vBanco,		"<%= parBanco	%>");
	selecciona(document.formulario.vPlaza,		"<%= parPlaza	%>");
	selecciona(document.formulario.vEstado,		"<%= parEstado	%>");
}

// --- Se ejecuta despu�s de un cuadro de confirmar
function continua()
{
	if(respuesta==1) {
		var valorcomando = document.formulario.vCodigo.value;
		valorcomando += "@|";
		document.formulario.accion.value	= "3";	// Regreso de Baja de Proveedor
		document.formulario.comando.value	=  valorcomando;
		document.formulario.action = "coMtoProveedor?opcion=6";
		document.formulario.submit();
	}
}

// --- Reseta los controles o da de Baja el Registro Actual
function Reset_Baja()
{
	if(document.formulario.accion.value == "2") {
		cuadroDialogo("&iquest;Desea dar de baja el registro?", 2);
	}
	else { document.formulario.reset();	}
}

// --- Regresa el Control al Servlet coMtoProveedor.java
function Regresar()
{
	if(document.formulario.accion.value == "2") {
		document.formulario.action = "coMtoProveedor?opcion=2";
		document.formulario.accion.value = "1";
	}
	else {
		document.formulario.action = "coMtoProveedor?opcion=1";
		//document.formulario.TrProveedor.value = CreaNuevaTrama();
		document.formulario.accion.value = "0";
	}
	document.formulario.submit();
}

// --- Valida los Campos, arma la Trama y la Regresa al Servlet
function Aceptar()
{
	var retorno = true;
	// Datos del Proveedor
	if(retorno) retorno = valida_vCveProveedor();


	if(document.formulario.tipoPersona.value == "F") {
		if(retorno) retorno = valida_vNombre();
		if(retorno) retorno = valida_vPaterno();
		if(retorno) retorno = valida_vRFC();
		if(retorno) retorno = valida_vHomoclave();
	}
	else if(document.formulario.tipoPersona.value == "M") {
		if(retorno) retorno = valida_vNombre();
		if(retorno) retorno = valida_vRFC();
		if(retorno) retorno = valida_vHomoclave();
		if(retorno) retorno = valida_vSociedad();
	}

	// Domicilio
	if(retorno) retorno = valida_vCalle();
	if(retorno) retorno = valida_vColonia();
	if(retorno) retorno = valida_vDelegacion();
	if(retorno) retorno = valida_vCiudad();
	if(retorno) retorno = valida_vCodigoPostal();
	if(retorno) retorno = valida_vEstado();
	if(retorno) retorno = valida_vPais();


	// Medio de Confirmacion
	if(retorno) retorno = valida_vMedio();
	if(retorno) retorno = valida_vTelefono();
    //Forma de pago
	if(retorno) retorno = valida_vForma();
	if(retorno)	{
		if(document.formulario.accion.value == "2") {	// Modificacion de Proveedor
		    document.formulario.comando.value = CreaNuevaTrama();
			document.formulario.accion.value	= "4";
			document.formulario.action = "coMtoProveedor?opcion=8";
			document.formulario.submit();
		}
		else {											// Alta de Proveedor
			document.formulario.TrProveedor.value = CreaNuevaTrama();
			document.formulario.action = "coMtoProveedor?opcion=1";
			document.formulario.tipoPersona.value = document.formulario.tipoPersona.value + "NR"; // NR = Ya no se requiere saber que tipo de Persona
			document.formulario.submit();
		}
	}
}

// -- Crea una Trama Nueva, con el formato adecuado para poder ser enviada a Tuxedo
function CreaNuevaTrama()
{
	var trama = "";

	if(document.formulario.tipoPersona.value == "M") { // Persona Moral
		var vTipoSoc= document.formulario.vTipoSoc.options[document.formulario.vTipoSoc.selectedIndex].value;
	}
	else if(document.formulario.tipoPersona.value == "F") { 		// Persona Fisica
		var vTipoSoc="";
	}
	var vEstado = document.formulario.vEstado.options[document.formulario.vEstado.selectedIndex].value;
	var vPais	= document.formulario.vPais.options[document.formulario.vPais.selectedIndex].value;
	var vMedio	= document.formulario.vMedio.options[document.formulario.vMedio.selectedIndex].value;
	var vForma	= document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value;
	var vBanco	= document.formulario.vBanco.options[document.formulario.vBanco.selectedIndex].value;
	var vPlaza	= document.formulario.vPlaza.options[document.formulario.vPlaza.selectedIndex].value;

	trama += "|";																												// Ok + Numero + Numero de Contrato
	if(document.formulario.vSecuencia.value	== "") trama += " |";	else trama += document.formulario.vSecuencia.value	+ "|";	// Numero de Secuencia
	if(document.formulario.vCodigo.value	== "") trama += " |";	else trama += document.formulario.vCodigo.value		+ "|";	// Codigo del Cliente
	if(document.formulario.vClave.value		== "") trama += " |";   else trama += document.formulario.vClave.value		+ "|";	// * Clave de Proveedor
	trama += vTipoSoc	+ "|";																									// Tipo Sociedad
	if(document.formulario.vContacto.value	== "") trama += " |";	else trama += document.formulario.vContacto.value	+ "|";	// Persona de contacto en la empresa:
	if(document.formulario.vFactoraje.value == "") trama += " |";	else trama += document.formulario.vFactoraje.value	+ "|";	// Numero de cliente para Factoraje Santander:
	trama += vMedio		+ "|";																									// * Medio de Confirmacion
	trama += vForma		+ "|";																									// * Forma de pago
	if(document.formulario.vCuenta.value	== "") trama += " |";	else trama += document.formulario.vCuenta.value		+ "|";	// No. cta cheques:
	trama += vBanco		+ "|";																										// Banco
	if(document.formulario.vSucursal.value	== "") trama += " |";	else trama += document.formulario.vSucursal.value	+ "|";	// Sucursal
	trama += vPlaza		+ "|";																										// Plaza
	if(document.formulario.vLimite.value	== "") trama += " |";	else trama += document.formulario.vLimite.value		+ "|";	// Limite de financiamiento:
	if(document.formulario.vDeudores.value	== "") trama += " |";	else trama += document.formulario.vDeudores.value	+ "|";	// num Deudores activos
	if(document.formulario.vVolumen.value	== "") trama += " |";	else trama += document.formulario.vVolumen.value	+ "|";	// Volumen anual de ventas
	if(document.formulario.vImporte.value	== "") trama += " |";	else trama += document.formulario.vImporte.value	+ "|";	// importe medio FactEmit
	if(document.formulario.tipoPersona.value== "") trama += " |";	else trama += document.formulario.tipoPersona.value	+ "|";	// Persona Fisica o Moral
	if(document.formulario.tipoPersona.value == "F") {
		if(document.formulario.vPaterno.value	== "") trama += " |";	else trama += document.formulario.vPaterno.value	+ "|";	// * Apellido Paterno
		if(document.formulario.vMaterno.value	== "") trama += " |";	else trama += document.formulario.vMaterno.value	+ "|";	// Apellido Materno
		if(document.formulario.vNombre.value	== "") trama += " |";	else trama += document.formulario.vNombre.value		+ "|";	// * Nombre
	}
	else if(document.formulario.tipoPersona.value == "M") {
		trama += ""	+ "|";																									// * Apellido Paterno
		trama += ""	+ "|";																									// Apellido Materno
		if(document.formulario.vNombre.value	== "") trama += " |";	else trama += document.formulario.vNombre.value		+ "|";	// * Nombre
	}
	if(document.formulario.vRFC.value		== "") trama += " |";   else trama += document.formulario.vRFC.value		+ "|";	//  * RFC
	if(document.formulario.vHomoclave.value == "") trama += " |";	else trama += document.formulario.vHomoclave.value  + "|";	// * Homoclave
	if(document.formulario.vCalle.value		== "") trama += " |";   else trama += document.formulario.vCalle.value		+ "|";	// * Calle y numero
	if(document.formulario.vColonia.value	== "") trama += " |";	else trama += document.formulario.vColonia.value	+ "|";	// * Colonia
	if(document.formulario.vDelegacion.value== "") trama += " |";	else trama += document.formulario.vDelegacion.value + "|";	// Deleg. o Municipio
	if(document.formulario.vCiudad.value	== "") trama += " |";	else trama += document.formulario.vCiudad.value		+ "|";	// Cd. o Poblacion
	if(document.formulario.vCodigoPostal.value == "") trama += " |";else trama += document.formulario.vCodigoPostal.value	+ "|";	// * C.P.
	trama += vEstado	+ "|";	// * Estado:
	trama += vPais		+ "|";																									// pais
	if(document.formulario.vLada.value		== "") trama += " |";   else trama += document.formulario.vLada.value		+ "|";	// Lada
	if(document.formulario.vTelefonico.value== "") trama += " |";	else trama += document.formulario.vTelefonico.value  + "|";	// * Numero telefonico
	if(document.formulario.vExTelefono.value== "") trama += " |";	else trama += document.formulario.vExTelefono.value	+ "|";	// Extension Tel
	if(document.formulario.vFax.value		== "") trama += " |";   else trama += document.formulario.vFax.value		+ "|";	// Fax
	if(document.formulario.vExFax.value		== "") trama += " |";   else trama += document.formulario.vExFax.value		+ "|";	// Extension Fax
	if(document.formulario.vEmail.value		== "") trama += " |";	else trama += document.formulario.vEmail.value		+ "|";	// email
	trama += " |";																												// Estatus
	trama += " |";																												// Descripcion Estatus
	trama += " |";																									// Lugar de Impresion

	return trama;
}

// -- Elimina los espacio existentes de una cadena, por el lado derecho e izquierdo y entre la cadena
function trim(inputString)
{
	if (typeof inputString != "string") { return inputString; }

	var retValue = inputString;
	var ch = retValue.substring(0, 1);

	while (ch == " ") {
		retValue = retValue.substring(1, retValue.length);
		ch = retValue.substring(0, 1);
	}
	ch = retValue.substring(retValue.length-1, retValue.length);
	while (ch == " ") {
		retValue = retValue.substring(0, retValue.length-1);
		ch = retValue.substring(retValue.length-1, retValue.length);
	}
	while (retValue.indexOf("  ") != -1) {
		retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length);
	}
	return retValue;
}

// --- selecciona el valor 'valor' de la lista 'lista'
function selecciona(lista,valor)
{
	for(a=0;a<lista.length;a++) {
		if(trim(lista.options[a].value) == valor) {
			lista.selectedIndex = a;
		}
	}
}

// ------------------------------ FUNCIONES DE LAS VALIDACIONES -------------------------------------

// Valida si el caracter recibido corresponde a un caracter alfabetico
function Caracter_EsAlfa(caracter)
{
	var ValorRetorno = true;

	if ( !(caracter >= "A" && caracter <= "Z") ) //Si no es Es Alpha Mayusculas
		if( !(caracter >= "�" && caracter <= "�") )
			if ( !(caracter >= "a" && caracter <= "z") ) // Si no es Alpha Minusculas
				if( !(caracter >= "�" && caracter <= "�") )
					if ( caracter != "�" && caracter != "�")
						ValorRetorno = false;
	return ValorRetorno;
}

// Valida si el caracter recibido corresponde a un caracter numerico
function Caracter_EsNumerico(caracter)
{
	var ValorRetorno = true;

	if( !(caracter >= "0" && caracter <= "9") ) //Si no es Es numerico
		ValorRetorno = false;

	return ValorRetorno;
}

// Valida que la cadena sea Alfanumerica ------------------------------------------------------------
function EsAlfaNumerica(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				retorno = false;
	}
	return retorno;
}

// Valida que la cadena sea Alfabetica --------------------------------------------------------------
function EsAlfa(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if( caracter != " ")
				retorno = false;
	}
	return retorno;
}

// Valida que la cadena sea Numerica ------------------------------------------------------------
function EsNumerica(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsNumerico(caracter))
			retorno = false;
	}
	return retorno;
}

// Valida si la longitud de la cadena es Correcta ---------------------------------------------------
function EsValidaLongitud(LongitudCadena,Patron)
{
	var VariableRetorno;
	VariableRetorno = (LongitudCadena > 0 && LongitudCadena <= Patron)?true:false;

	return(VariableRetorno);
}

// Valida si la cadena contiene caracteres Especiales -----------------------------------------------
function SonValidosCaracteresEspeciales(cadena)
{
	var ValorRetorno = true;
	var caracter;

	for(i=0; i < cadena.length && ValorRetorno; i++){
		caracter = cadena.substring(i,i+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				if( caracter != " " && caracter != "." && caracter != "#" && caracter != null ) //caracteres Especiales
					ValorRetorno = false;
	}
	return (ValorRetorno);
}

function MensajeErrorLongitudNula(NombreCampo)
{
	cuadroDialogo("Favor de teclear " + NombreCampo,3)
}
function MensajeErrorTipoDeDato(NombreCampo,TipoDato)
{
	cuadroDialogo("EL campo " + NombreCampo + " debe ser " + TipoDato,3)
}
function MensajeErrorCaracteresInvalidos(NombreCampo)
{
	cuadroDialogo("EL campo " + NombreCampo + " contiene car&aacute;cteres no v&aacute;lidos",3)
}
// --------------------------- VALIDACIONES DE CAMPOS ALPHANUMERICOS ------------------------------

// Valida Clave de Proveedor -----------------------------------------------------------------------
function valida_vCveProveedor()
{
	var valorCampo = document.formulario.vClave.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vClave.select();
		document.formulario.vClave.focus();
		MensajeErrorLongitudNula("la Clave del Proveedor");
	}
	else if (!EsAlfaNumerica(valorCampo)) {
		document.formulario.vClave.select();
		document.formulario.vClave.focus();
		MensajeErrorTipoDeDato("Clave de Proveedor","Alfanum&eacute;rico");
	}
	else { ValorRegreso = true;}

	return ValorRegreso;
}

// Valida Apellido Paterno ------------------------------------------------------------------------
function valida_vPaterno()
{
	var valorCampo = document.formulario.vPaterno.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vPaterno.select();
		document.formulario.vPaterno.focus();
		MensajeErrorLongitudNula("el Apellido Paterno");
	}
	else if (!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vPaterno.select();
		document.formulario.vPaterno.focus();
		MensajeErrorCaracteresInvalidos("Apellido Paterno");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Nombre ---------------------------------------------------------------------------------
function valida_vNombre()
{
	var valorCampo = document.formulario.vNombre.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vNombre.select();
		document.formulario.vNombre.focus();
		MensajeErrorLongitudNula("el Nombre");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vNombre.select();
		document.formulario.vNombre.focus();
		MensajeErrorCaracteresInvalidos("Nombre");
	}
	else {ValorRegreso = true;}
	return ValorRegreso;
}

// Valida el RFC ---------------------------------------------------------------------------------
function valida_vRFC()
{
	var valorCampo = document.formulario.vRFC.value;
	var Indice;
	var formato = "";

	switch(valorCampo.length) {
		case 0:
			document.formulario.vRFC.select();
			document.formulario.vRFC.focus();
			MensajeErrorLongitudNula("el R.F.C.");
			return false;
			break;
		case 9:

			if(document.formulario.tipoPersona.value == "M") { 	// Persona Moral
				formato =  /^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;
				Indice = 5;
			}
			break;
		case 10:
			if(document.formulario.tipoPersona.value == "F") { 	// Persona Fisica
				formato = /^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;
				Indice = 6;
			}
			break;
		default:
			document.formulario.vRFC.select();
			document.formulario.vRFC.focus();
			cuadroDialogo("El campo R.F.C. est&aacute; incompleto.",3);
			return false;
			break;
    }
	if(formato != "") {
		if(formato.test(valorCampo)) {
			if(valida_NombreRFC(valorCampo)) {
				if(valida_FechaRFC(valorCampo,Indice)) {
					return true;
				}
			}
		}
	}
	document.formulario.vRFC.select();
	document.formulario.vRFC.focus();
	cuadroDialogo("El formato del campo R.F.C. es incorrecto.",3);
	return false;
}

// Valida el Nombre del R.F.C. -------------------------------------------------------------------
function valida_NombreRFC(valorCampo)
{
	if(document.formulario.tipoPersona.value == "F") { 		// Persona Fisica
		var ApPaterno = document.formulario.vPaterno.value;
		var ApMaterno = document.formulario.vMaterno.value;
		var Nombre = document.formulario.vNombre.value;
		var rfc_tmp = "";

		if(ApMaterno.length != 0) {
			rfc_tmp =	ApPaterno.substring(0,2)	+	// Iniciales Paterno
						ApMaterno.substring(0,1)	+	// Iniciale Materno
						Nombre.substring(0,1);			// Inicial Nombre
			if(valorCampo.substring(0,4) == rfc_tmp)
				{return true;}
		}
		else {
			if(ApPaterno.substring(0,2) == valorCampo.substring(0,2)) {
				if(Nombre.substring(0,1) == valorCampo.substring(3,4)) {
					return true;
				}
			}
		}
	}
	else if(document.formulario.tipoPersona.value == "M") { 	// Persona Moral
		return true;
	}
	return false;
}

// Valida la Fecha del R.F.C. -------------------------------------------------------------------
function valida_FechaRFC(valorCampo,Indice)
{
	var ValorRegreso = true;
	var mes = valorCampo.substring(Indice, Indice + 2);
	var dia = valorCampo.substring(Indice + 2, Indice + 4);

	if((dia > 0 && dia <= 31) && (mes > 0 && mes <= 12)) {
		if(mes == 2 && dia > 28)	// Para el Mes de Febrero
			{ValorRegreso = false;}
	}
	else {ValorRegreso = false;}

	return ValorRegreso;
}
// Valida la Homoclave --------------------------------------------------------------------------
function valida_vHomoclave()
{
	var valorCampo = document.formulario.vHomoclave.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vHomoclave.select();
		document.formulario.vHomoclave.focus();
		MensajeErrorLongitudNula("la Homoclave");
	}
	else if (!EsAlfaNumerica(valorCampo)) {
		document.formulario.vHomoclave.select();
		document.formulario.vHomoclave.focus();
		MensajeErrorCaracteresInvalidos("Homoclave");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Persona Contacto en la empresa -------------------------------------------------------
function valida_vContacto()
{
	var valorCampo = document.formulario.vContacto.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vContacto.select();
		document.formulario.vContacto.focus();
		MensajeErrorLongitudNula("la Persona de Contacto en la Empresa");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vContacto.select();
		document.formulario.vContacto.focus();
		MensajeErrorCaracteresInvalidos("Persona de Contacto en la Empresa");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Numero de cliente para Factoraje ----------------------------------------------------
function valida_vFactoraje()
{
	var valorCampo = document.formulario.vFactoraje.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vFactoraje.select();
		document.formulario.vFactoraje.focus();
		MensajeErrorLongitudNula("el N&uacute;mero de Cliente para Factoraje");
	}
	else if(!EsAlfaNumerica(valorCampo)) {
		document.formulario.vFactoraje.select();
		document.formulario.vFactoraje.focus();
		MensajeErrorTipoDeDato("N&uacute;mero de Cliente para Factoraje","Alfanum&eacute;rico");
	}
	else { ValorRegreso = true; }

	return ValorRegreso;
}

// Valida la Calle y numero ------------------------------------------------------------------
function valida_vCalle()
{
	var valorCampo = document.formulario.vCalle.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vCalle.select();
		document.formulario.vCalle.focus();
		MensajeErrorLongitudNula("la Calle y N&uacute;mero");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vCalle.select();
		document.formulario.vCalle.focus();
		MensajeErrorCaracteresInvalidos("Calle y N&uacute;mero");
	}
	else { ValorRegreso = true; }

	return ValorRegreso;
}

//	Valida Colonia --------------------------------------------------------------------------
function valida_vColonia()
{
	var valorCampo = document.formulario.vColonia.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vColonia.select();
		document.formulario.vColonia.focus();
		MensajeErrorLongitudNula("la Colonia");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vColonia.select();
		document.formulario.vColonia.focus();
		MensajeErrorCaracteresInvalidos("Colonia");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Delegacion o Municipio -----------------------------------------------------------
function valida_vDelegacion()
{
	var valorCampo = document.formulario.vDelegacion.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vDelegacion.select();
		document.formulario.vDelegacion.focus();
		MensajeErrorLongitudNula("la Delegaci&oacute;n");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vDelegacion.select();
		document.formulario.vDelegacion.focus();
		MensajeErrorCaracteresInvalidos("Delegaci&oacute;n");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Ciudad o Poblacion ---------------------------------------------------------------
function valida_vCiudad()
{
	var valorCampo = document.formulario.vCiudad.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vCiudad.select();
		document.formulario.vCiudad.focus();
		MensajeErrorLongitudNula("la Ciudad o Poblaci&oacute;n");
	}
	else if(!EsAlfa(valorCampo)) {
		document.formulario.vCiudad.select();
		document.formulario.vCiudad.focus();
		MensajeErrorCaracteresInvalidos("Ciudad o Poblaci&oacute;n");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// --------------------------- VALIDACIONES DE CAMPOS NUMERICOS ---------------------------------
// Valida Codigo Postal -------------------------------------------------------------------
function valida_vCodigoPostal()
{
	var valorCampo = document.formulario.vCodigoPostal.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vCodigoPostal.select();
		document.formulario.vCodigoPostal.focus();
		MensajeErrorLongitudNula("el C&oacute;digo Postal");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vCodigoPostal.select();
		document.formulario.vCodigoPostal.focus();
		MensajeErrorTipoDeDato("C&oacute;digo Postal","Num&eacute;rico");
	}
	else { ValorRegreso = true;}
	return ValorRegreso;
}

// Valida el Fax --------------------------------------------------------------------------
function valida_vFax(opcion)
{
	var valorCampo = document.formulario.vFax.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vFax.select();
		document.formulario.vFax.focus();
		MensajeErrorLongitudNula("el Fax");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vFax.select();
		document.formulario.vFax.focus();
		MensajeErrorTipoDeDato("Fax","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida la Lada -------------------------------------------------------------------------
function valida_vLada()
{
	var valorCampo = document.formulario.vLada.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vLada.select();
		document.formulario.vLada.focus();
		MensajeErrorLongitudNula("la Lada");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vLada.select();
		document.formulario.vLada.focus();
		MensajeErrorTipoDeDato("Lada","Num&eacute;rico");
	}
	else{ ValorRegreso = true; }
	return ValorRegreso;
}

// Valida el Telefono ---------------------------------------------------------------------
function valida_vTelefono()
{
	var valorCampo = document.formulario.vTelefonico.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		MensajeErrorLongitudNula("el Tel&eacute;fono");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		MensajeErrorTipoDeDato("Tel&eacute;fono","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida la Extension de Telefono --------------------------------------------------------
function valida_vExTelefono()
{
	var valorCampo = document.formulario.vExTelefono.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vExTelefono.select();
		document.formulario.vExTelefono.focus();
		MensajeErrorLongitudNula("la Extensi&oacute;n de Tel&eacute;fono");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vExTelefono.select();
		document.formulario.vExTelefono.focus();
		MensajeErrorTipoDeDato("Extensi&oacute;n de Tel&eacute;fono","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida la Extension del Fax ---------------------------------------------------------
/*function valida_vExFax()
{
	var valorCampo = document.formulario.vExFax.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vExFax.select();
		document.formulario.vExFax.focus();
		MensajeErrorLongitudNula("la Extensi&oacute;n de Fax");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vExFax.select();
		document.formulario.vExFax.focus();
		MensajeErrorTipoDeDato("Extensi&oacute;n de Fax","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}*/

// Valida la Sucursal ------------------------------------------------------------------
function valida_vSucursal()
	{
	var valorCampo = document.formulario.vSucursal.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vSucursal.select();
		document.formulario.vSucursal.focus();
		MensajeErrorLongitudNula("la Sucursal");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vSucursal.select();
		document.formulario.vSucursal.focus();
		MensajeErrorTipoDeDato("Sucursal","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida el Limite de financiamiento --------------------------------------------------
function valida_vLimite()
{
	var valorCampo = document.formulario.vLimite.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vLimite.select();
		document.formulario.vLimite.focus();
		MensajeErrorLongitudNula("el L&iacute;mite de Financiamiento");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vLimite.select();
		document.formulario.vLimite.focus();
		MensajeErrorTipoDeDato("L&iacute;mite de Financiamiento","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida el Volumen anual de ventas --------------------------------------------------
function valida_vVolumen()
{
	var valorCampo = document.formulario.vVolumen.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vVolumen.select();
		document.formulario.vVolumen.focus();
		MensajeErrorLongitudNula("el Volumen Anual de Ventas");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vVolumen.select();
		document.formulario.vVolumen.focus();
		MensajeErrorTipoDeDato("Volumen Anual de Ventas","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida el Importe de financiamiento ------------------------------------------------
function valida_vImporte()
{
	var valorCampo = document.formulario.vImporte.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vImporte.select();
		document.formulario.vImporte.focus();
		MensajeErrorLongitudNula("el Importe de Financiamiento");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vImporte.select();
		document.formulario.vImporte.focus();
		MensajeErrorTipoDeDato("Importe de Financiamiento","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

function digitoVerificador(cuenta)
{
	var pesos = "371371371371371371371371371";
	var DC = 0;
	var LlaveCuenta = cuenta.substring(0,17);
   	for(i=0;i<LlaveCuenta.length;i++)
		DC += (  parseInt(LlaveCuenta.charAt(i)) * parseInt(pesos.charAt(i))) % 10;
	DC = 10 - (DC % 10);
    DC = (DC >= 10) ? 0 : DC;
    LlaveCuenta += DC;
	if((LlaveCuenta) != cuenta)
		return false;

   return true;
 }

 function CuentaCheques(cuenta)
 {
	 var ponderacion = "234567234567";
	 var MODULO = 11;
	 var ponderado =0;
	 var Cta_compuesta = "21"+cuenta;
	 var Cierto = true;

	 for (i=0;i<Cta_compuesta.length-1;i++)
	 {
		 ponderado = ponderado + parseInt(Cta_compuesta.charAt(i))*parseInt(ponderacion.charAt(i))
	 }

	switch ( ponderado % MODULO ) {
		case 0:
			if (parseInt(Cta_compuesta.substring(12)) == 0)
				Cierto = true;
			else
			    Cierto = false;
			break;
		case 1:
           cuadroDialogo("cuenta CLABE no valida",3);
		   Cierto = false;
		   break;
		default:
			var ponde = (((ponderado % MODULO)- MODULO));
			if ((((ponderado % MODULO)- MODULO) == parseInt(Cta_compuesta.substring(12)))||(((ponderado % MODULO)- MODULO) == parseInt(Cta_compuesta.substring(12))*(-1)))
			{
				Cierto = true;
			}
			else
			{
			    cuadroDialogo("cuenta CLABE no valida",3);
				Cierto = false;
			}
			break;
	}
 return Cierto;

 }

// Muestra un Mensaje de Error de la Validacion de Cuenta/CLABE
function MensajeDeErrorDeCuenta(TipoError)
{
	document.formulario.vCuenta.select();
	document.formulario.vCuenta.focus();

	switch(TipoError) {
		case 1:
			cuadroDialogo("El campo Cuenta/CLABE debe ser Num&eacute;rico",3);
			break;
		case 2:
			cuadroDialogo("La longitud del campo Cuenta/CLABE debe ser de 11 O 18 d&iacute;gitos",3);
			break;
		case 3:
			cuadroDialogo("La longitud del campo Cuenta/CLABE debe ser de 18 d&iacute;gitos",3);
			break;
		case 4:
			cuadroDialogo("El Digito de Control en el campo Cuenta/CLABE, no corresponde a los datos proporcionados",3);
			break;
		case 5:
			cuadroDialogo("La longitud del campo Cuenta/CLABE debe ser de 11 d&iacute;gitos",3);
			break;

	}
}

// Valida Cuenta/CLABE ----------------------------------------------------------------
function valida_vCuenta()
{
	var ValorRegreso = false;
	var valorCampo			= document.formulario.vCuenta.value;

	// 1 - Permite la captura de cuentas de cheques de 11 d�gitos y cuentas CLABE de 18 digitos.
	// 0 - Permite la captura de cuentas CLABE de 18 Digitos.

	if(valorCampo.length == 0) {
		document.formulario.vCuenta.select();
		document.formulario.vCuenta.focus();
		MensajeErrorLongitudNula("la Cuenta/CLABE");
	}
	else {
		if(document.formulario.uso_cta_cheque.value == 1) {	// 1 - Cuenta Cheques 11 Digitos
			if(document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value == "2") {			//Transferencia Interbancaria
				if(valorCampo.length == 18) {		// Si la cuenta es de 18 digitos
					if(EsNumerica(valorCampo)) {
						if(digitoVerificador(valorCampo)) {
							ValorRegreso = true;
						}
						else {MensajeDeErrorDeCuenta(4);}
					}
					else {MensajeDeErrorDeCuenta(1);}
				}
				else {MensajeDeErrorDeCuenta(3);}
			}
			else if(document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value == "1") {	//Transferencia Interna
				if(valorCampo.length == 18) {		// Si la cuenta es de 18 digitos
					if(EsNumerica(valorCampo)) {
						if(digitoVerificador(valorCampo)) {
							ValorRegreso = true;
						}
						else {MensajeDeErrorDeCuenta(4);}
					}
					else {MensajeDeErrorDeCuenta(1);}
				}
				if(valorCampo.length == 11) {		// Si la cuenta es de 11 digitos
					if(EsNumerica(valorCampo)) {
						if (CuentaCheques(valorCampo)){
							ValorRegreso = true;
						}
					}
					else {MensajeDeErrorDeCuenta(1);}
				}
				else if(valorCampo.length != 11) {
					MensajeDeErrorDeCuenta(5);
					ValorRegreso = false;
				}
			}
		}
		else if (document.formulario.uso_cta_cheque.value == 0) {	// 0 - Cuentas CLABE 18 Digitos
			if(valorCampo.length == 18) {
				if(EsNumerica(valorCampo)) {
					if(digitoVerificador(valorCampo)) {
						ValorRegreso = true;
					}
					else {MensajeDeErrorDeCuenta(4);}
				}
				else {MensajeDeErrorDeCuenta(1);}
			}
			else {MensajeDeErrorDeCuenta(3);}
		}
	}
	return ValorRegreso;
}


// ----------------------------- VALIDACIONES DE COMBOS --------------------------------------

// Valida selecion del Banco  --------------------------------------------------------------
function valida_vBanco()
{
	var index_Banco =  document.formulario.vBanco.selectedIndex;
	var ValorRegreso = true;

	if(document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value == "2") {	//Transferencia Interbancaria
		if ( index_Banco == 0) {
			document.formulario.vBanco.focus();
			cuadroDialogo("Favor de Seleccionar un Banco",3);
			ValorRegreso = false;
		}
		else if(document.formulario.vBanco.options[index_Banco].value == "BANME" || document.formulario.vBanco.options[index_Banco].value == "SERFI") {
			document.formulario.vForma.focus();
			cuadroDialogo("Favor de Seleccionar la Forma de Pago \"Transferencia Interna\"",3);
			ValorRegreso = false;
		}
	}
	else if(document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value == "1") {	//Transferencia Interna
		if ( index_Banco != 0) {
			if(document.formulario.vBanco.options[index_Banco].value != "BANME" && document.formulario.vBanco.options[index_Banco].value != "SERFI") {
				document.formulario.vForma.focus();
				cuadroDialogo("Favor de Seleccionar la Forma de Pago \"Transferencia Interbancaria\"",3);
				ValorRegreso = false;
			}
		}
	}
	return ValorRegreso;
}

// Valida selecion del estado -------------------------------------------------------------
function valida_vEstado()
{
	var index_Estado =  document.formulario.vEstado.selectedIndex;
	var ValorRegreso = true;

    if ( index_Estado == 0) {
		document.formulario.vEstado.focus();
	    cuadroDialogo("Favor de Seleccionar un Estado",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}

// Valida selecion del pais  --------------------------------------------------------------
function valida_vPais()
{
	var index_Pais =  document.formulario.vPais.selectedIndex;
	var ValorRegreso = true;

	if ( index_Pais == 0) {
		document.formulario.vPais.focus();
	    cuadroDialogo("Favor de Seleccionar un Pa&iacute;s",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}

// Valida selecion del Banco  --------------------------------------------------------------
function valida_vPlaza()
{
    var index_Plaza =  document.formulario.vPlaza.selectedIndex;
	var ValorRegreso = true;

    if ( index_Plaza == 0) {
		document.formulario.vPlaza.focus();
	    cuadroDialogo("Favor de Seleccionar una Plaza",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}

// Valida seleccion de la forma de pago ----------------------------------------------------
function valida_vForma()
{
	var index_Forma =  parseInt(document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value);
	var ValorRegreso = false;

	switch ( index_Forma ) {
		case -1:
			document.formulario.vForma.focus();
			cuadroDialogo("Favor de Seleccionar una Forma de Pago",3);
		break;
		case 1: // Transferencia interna.
			if(valida_vCuenta())
				if(valida_vBanco())
					ValorRegreso = true;
		break;
		case 2: // Transferencia interbancaria.
			if(valida_vCuenta())
				if(valida_vBanco())
					if(valida_vSucursal())
						if(valida_vPlaza())
							ValorRegreso = true;
		break;
		default:
			document.formulario.vForma.focus();
			cuadroDialogo("Actualmente la forma de pago no es valida debera elegir entre transferencia Interna e Interbancaria",3);
			break;
	}
	return ValorRegreso;
}

// Valida la seleccion de medio de comunicacion -------------------------------------------
function valida_vMedio()
{
	var index_Medio =parseInt(document.formulario.vMedio.options[document.formulario.vMedio.selectedIndex].value);
	var ValorRegreso = false;

	switch ( index_Medio ) {
		case 0:
			document.formulario.vMedio.focus();
			cuadroDialogo("Favor de Seleccionar un Medio de Informaci&oacute;n",3);
		break;
		case 1:
			if(valida_vEmail())
				ValorRegreso = true;
		break;
		case 2:
			if(valida_vFax())
				ValorRegreso = true;
		break;
		default:
			document.formulario.vMedio.focus();
			cuadroDialogo("Actualmente su Medio de Informaci&oacute;n no es valido debe elegir entre e-mail y fax",3);
			break;
	}
	return ValorRegreso;
}
// Valida la selecion de tipo de sociedad -------------------------------------------------
function valida_vSociedad()
{
	var index_Sociedad =  document.formulario.vTipoSoc.selectedIndex;
	var ValorRegreso = true;

    if ( index_Sociedad == 0) {
		document.formulario.vTipoSoc.focus();
		cuadroDialogo("Favor de Seleccionar el Tipo de Sociedad",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}

// Valida que el e-mail sea valido -------------------------------------------------------------
function valida_vEmail()
{
	var valorCampo = document.formulario.vEmail.value;
	var ValorRegreso = false;
	var pos = 0;
	var complemento;
	var commx;
	var mx;

	//marcos@santander.		Tipo = com.mx
	if(valorCampo.length != 0) {
		if((pos = valorCampo.indexOf('@',0)) != -1) {
			if((valorCampo.substring(0,pos)).length != 0) {
				if((valorCampo.substring(pos)).length > 1 ) {
					complemento = valorCampo.substring(pos+1);
					if((pos = complemento.indexOf('.',0)) != -1) {
						if((complemento.substring(0,pos)).length != 0) {
							if((complemento.substring(pos)).length > 1 ) {
								commx = complemento.substring(pos+1);
								if((pos = commx.indexOf('.',0)) != -1) {
									if((commx.substring(0,pos)).length != 0) {
										if((commx.substring(pos)).length > 1 ) {
											mx = commx.substring(pos+1);
											if(mx.indexOf('.',0) == -1 && mx.length == 2) {
												ValorRegreso = true;
											}
										}
									}
								}
								else if(commx.length == 3) {ValorRegreso = true;}
							}
						}
					}
				}
			}
		}
	}
	if(ValorRegreso == false) {
		document.formulario.vEmail.select();
		document.formulario.vEmail.focus();
		cuadroDialogo("Favor de teclear correctamente su e-mail",3);
	}

	return ValorRegreso;
}

// Habilita los campos correspondientes a una persona Fisca --------------------------------------
function HabilitaCamposValidos_Fisica()
{
	<% if(!parAccion.equals("2") && !parAccion.equals("11")) {	%> // Alta de Proveedor o Modificacion de Proveedor
		// Eliminamos la seleccion del combo tipo de Sociedad y el valor del RFC
		document.formulario.vTipoSoc.selectedIndex = 0;
		document.formulario.vRFC.value = "";

		document.formulario.TrProveedor.value = CreaNuevaTrama();
		document.formulario.action = "coMtoProveedor?opcion=1";
		document.formulario.tipoPersona.value = "F";
		document.formulario.submit();
	<% } %>
}

// Habilita los campos correspondientes a una persona Moral --------------------------------------
function HabilitaCamposValidos_Moral()
{
	<% if(!parAccion.equals("2") && !parAccion.equals("11")) {	%> // Alta de Proveedor o Modificacion de Proveedor
		// Eliminamos el valor de los campos Apellido Paterno, Materno y el RFC
		document.formulario.vPaterno.value = "";
		document.formulario.vMaterno.value = "";
		document.formulario.vRFC.value = "";

		document.formulario.TrProveedor.value = CreaNuevaTrama();
		document.formulario.action = "coMtoProveedor?opcion=1";
		document.formulario.tipoPersona.value = "M";
		document.formulario.submit();
	<% } %>
}

//---------------------------	TERMINAN VALIDACIONES	-----------------------------------------

<%= (String)request.getAttribute("newMenu") %>

</SCRIPT>
</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); inicia();" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	   <!-- MENU PRINCIPAL -->
	   <%= (String) request.getAttribute("MenuPrincipal") %>
	</td>
   </tr>
</table>

<%= (String)request.getAttribute("Encabezado") %>

<br>
<CENTER class="titpag" > <%= tituloPantalla %> de Proveedor </CENTER>

<TABLE WIDTH="760" BORDER="0" CELLSPACING="0" CELLPADDING="0" >
	<FORM  NAME="formulario" METHOD="POST">
	<tr>
		<td align="center">
			<table width="680" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td rowspan="2">
						<table width="660" border="0" cellspacing="2" cellpadding="3">
							<P class="tabmovtex">* El llenado de estos campos es obligatorio
							<tr>
								<td class="tittabdat" colspan="2"> Capture los datos del proveedor </td>
							</tr>
							<tr align="center">
								<td class="textabdatcla" valign="top" colspan="2">
									<table width="650" border="0" cellspacing="0" cellpadding="0">
										<TR>
											<% if(parAccion.equals("1") || parAccion.equals("10")) {	%>
												<TD class="tabmovtexbol" nowrap colspan = 3 align="CENTER">
													<INPUT TYPE="RADIO" NAME="vPersona" VALUE="F" <%= (parTipoPersona.equals("F"))?"checked":"" %> onClick="HabilitaCamposValidos_Fisica();"><B>Persona f&iacute;sica</B>
													<INPUT TYPE="RADIO" NAME="vPersona" VALUE="M" <%= (parTipoPersona.equals("M"))?"checked":"" %> onClick="HabilitaCamposValidos_Moral();"><B>Persona moral</B>
												</TD>
											<% }
												else { %>
													<TD class="tabmovtexbol" nowrap colspan = 3 align="CENTER"><font color = #003366><%= (parTipoPersona.equals("F")? "PERSONA FISICA": "PERSONA MORAL") %> </font></TD>
											<% } %>
										</TR>
										<TR>
											<TD class="tabmovtexbol" nowrap> * Clave de Proveedor	</TD>
											<TD class="tabmovtexbol" nowrap> C&oacute;digo del cliente		</TD>
											<TD class="tabmovtexbol" nowrap> No. Transmisi&oacute;n	</TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vClave" value="<%= parClave %>" maxlength="20" onchange = "this.value=this.value.toUpperCase(); return true;"   ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vCodigo" value="<%= parCodigo %>" onFocus='blur();'></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vSecuencia" value="<%= parSecuencia %>" onFocus='blur();'></TD>
										</TR>
										<TR>
										<% if(parTipoPersona.equals("F")) { %>
											<TD  class="tabmovtexbol" nowrap> <%= Nombre_RazonSocial %>	</TD>
											<TD  class="tabmovtexbol" nowrap> <%= ApellidoPaterno %>	</TD>
											<TD  class="tabmovtexbol" nowrap> Apellido Materno			</TD>
										<% }
										else if(parTipoPersona.equals("M")) { %>
											<TD  class="tabmovtexbol" nowrap> <%= Nombre_RazonSocial %>	</TD>
											<TD  class="tabmovtexbol" nowrap> * Tipo de sociedad</TD>
										<% } %>
										</TR>
										<TR>
										<% if(parTipoPersona.equals("F")) { %>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vNombre"  value="<%= parNombre %>"  maxlength="80" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vPaterno" value="<%= parPaterno %>" maxlength="30" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vMaterno" value="<%= parMaterno %>" maxlength="30" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
										<% }
										else if(parTipoPersona.equals("M")) { %>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vNombre"  value="<%= parNombre %>"  maxlength="80" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap colspan = 2><SELECT name="vTipoSoc">
												<OPTION VALUE="0"></OPTION>
													<%= request.getAttribute("TipoSoc") %>
												</SELECT>
											</TD>

										<% } %>
										</TR>
							 			<TR>
											<TD  class="tabmovtexbol" nowrap> * R.F.C.			</TD>
											<TD  class="tabmovtexbol" nowrap> * Homoclave	</TD>
											<TD  class="tabmovtexbol" nowrap> Contacto en la empresa:     </TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vRFC" value="<%= parRFC %>" maxlength="10" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vHomoclave" value="<%= parHomoclave %>"maxlength="3" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vContacto" value="<%= parContacto %>"	 maxlength="80" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> No. Cliente para Factoraje:</TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vFactoraje" value="<%= parFactoraje %>" maxlength="8" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
							<tr>
								<td class="tittabdat" colspan="2"> Domicilio</td>
							</tr>
							<tr align="center">
								<td class="textabdatcla" valign="top" colspan="2">
									<table width="650" border="0" cellspacing="0" cellpadding="0">
										<TR>
											<TD class="tabmovtexbol" nowrap> * Calle y n&uacute;mero</TD>
											<TD class="tabmovtexbol" nowrap> * Colonia	</TD>
											<TD  class="tabmovtexbol" nowrap> * Deleg. o Municipio	</TD>
										</TR>
										<TR>
											<TD class="tabmovtexbol" nowrap> <INPUT TYPE="Text" NAME="vCalle" value="<%= parCalle %>" maxlength="60" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD class="tabmovtexbol" nowrap> <INPUT TYPE="Text" NAME="vColonia" value="<%= parColonia %>" maxlength="30" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap> <INPUT TYPE="TEXT" NAME="vDelegacion" value="<%= parDelegacion %>" maxlength="35" onchange = "this.value=this.value.toUpperCase(); return true;" > </TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> * Cd. o Poblaci&oacute;n	</TD>
											<TD  class="tabmovtexbol" nowrap> * C.P.		</TD>
											<TD  class="tabmovtexbol" nowrap> * Estado		</TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> <INPUT TYPE="TEXT" NAME="vCiudad" value="<%= parCiudad %>" maxlength="35" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap> <INPUT TYPE="TEXT" NAME="vCodigoPostal" value="<%= parCP %>" maxlength="5" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD class="tabmovtexbol" nowrap><SELECT NAME="vEstado">
												<OPTION VALUE="0"></OPTION>
												<%= request.getAttribute("Estados") %>
											</SELECT></TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> * Pa&iacute;s	</TD>
										</TR>
										<TR>
											<TD class="tabmovtexbol" nowrap> <SELECT NAME="vPais">
												<OPTION VALUE="0"></OPTION>
												<%= request.getAttribute("Paises") %>
											</SELECT></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<tr>
								<td class="tittabdat" colspan="2"> Medio de confirmaci&oacute;n</td>
							</tr>
							<tr align="center">
								<td class="textabdatcla" valign="top" colspan="2">
									<table width="650" border="0" cellspacing="0" cellpadding="0">
										<TR>
											<TD class="tabmovtexbol" nowrap> * Medio de Informaci&oacute;n	</TD>
											<TD class="tabmovtexbol" nowrap> e-mail	</TD>
											<TD  class="tabmovtexbol" nowrap> Lada o prefijo     </TD>
										</TR>
										<TR>
											<TD class="tabmovtexbol" nowrap><SELECT NAME="vMedio">
												<OPTION VALUE="0"></OPTION>
												<%= request.getAttribute("MedioInfo") %>
											</SELECT></TD>
											<TD class="tabmovtexbol" nowrap> <INPUT TYPE="Text" NAME="vEmail" value="<%= parEmail %>" maxlength="60"> </TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vLada" value="<%= parLada %>" maxlength="12" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> * N&uacute;mero telef&oacute;nico	</TD>
											<TD  class="tabmovtexbol" nowrap> Extensi&oacute;n Tel.	</TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vTelefonico" value="<%= parTelefono %>" maxlength="12" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vExTelefono" value="<%= parExTelefono %>" maxlength="12" onchange = "this.value=this.value.toUpperCase(); return true;" >	</TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> Fax     </TD>
											<!--<TD  class="tabmovtexbol" nowrap> Extensi&oacute;n de fax </TD>-->
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vFax" value="<%= parFax %>" maxlength="12" onchange = "this.value=this.value.toUpperCase(); return true;" >		</TD>
											<!--<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vExFax" value="<%= parExFax %>" maxlength="12" onchange = "this.value=this.value.toUpperCase(); return true;" >	</TD>-->
										</TR>
									</TABLE>
								</TD>
							</TR>
							<tr>
							    <td class="tittabdat" colspan="2"> Forma de pago</td>
							</tr>
							<tr align="center">
								<td class="textabdatcla" valign="top" colspan="2">
									<table width="650" border="0" cellspacing="0" cellpadding="0">
										<TR>
											<TD  class="tabmovtexbol" nowrap> * Forma de pago	</TD>
											<TD  class="tabmovtexbol" nowrap> Cuenta/CLABE:	</TD>
											<TD  class="tabmovtexbol" nowrap> Banco		</TD>
										</TR>
										<TR>
											<TD class="tabmovtexbol" nowrap colspan=1><SELECT NAME="vForma">
												<OPTION VALUE="-1"></OPTION>
												<%= request.getAttribute("FormaPago") %>
											</SELECT></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vCuenta" value="<%= parCuenta %>" maxlength="18" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD class="tabmovtexbol" nowrap><SELECT NAME="vBanco">
												<OPTION VALUE="0"></OPTION>
												<%= request.getAttribute("Bancos") %>
											</SELECT></TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> Sucursal	</TD>
											<TD  class="tabmovtexbol" nowrap> Plaza		</TD>
										</TR>
										<TR>
											<TD class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vSucursal" value="<%= parSucursal %>" maxlength="4" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap colspan = 1><SELECT NAME="vPlaza">
												<OPTION VALUE="0"></OPTION>
												<%= request.getAttribute("Plazas") %>
											</SELECT></TD>

										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap> Deudores activos:				    </TD>
											<TD  class="tabmovtexbol" nowrap> L&iacute;mite de financiamiento:	</TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vDeudores"	value="<%= parDeudores %>" maxlength="5" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vLimite"		value="<%= parLimite %>" maxlength="21" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
										</TR>
   										<TR>
											<TD  class="tabmovtexbol" nowrap> Volumen anual de ventas				</TD>
											<TD  class="tabmovtexbol" nowrap> Importe medio de facturas emitidas	</TD>
										</TR>
										<TR>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vVolumen" value="<%= parVolumen %>" maxlength="21" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
											<TD  class="tabmovtexbol" nowrap><INPUT TYPE="TEXT" NAME="vImporte" value="<%= parImporte %>" maxlength="21" onchange = "this.value=this.value.toUpperCase(); return true;" ></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<tr align="center">
								<td valign="top" colspan="2">
									<table width="680" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><P class="tabmovtex">* El llenado de estos campos es obligatorio</td>
										</tr>
									</table>
									<BR>
									<table align="center" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="top">
												<A href ="javascript:Aceptar();" border=0><img src="/gifs/EnlaceMig/<%= boton1 %>" border=0 alt="<%= altBoton %>"></A>
											</td>
	  										<td align="center" valign="top">
												<A href = "javascript:Reset_Baja();" border=0><img src="/gifs/EnlaceMig/<%= boton2 %>" border=0 alt="Limpiar"></A>
											</td>
											<td align="left" valign="top">
												<A href = "javascript:Regresar();" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar"></A>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<INPUT type=hidden name=TrProveedor value="<%= parProveedor %>">
	<INPUT type=hidden name=accion value="<%= parAccion %>">
	<INPUT type=hidden name=comando>
	<INPUT type=hidden name=tipoPersona value="<%= parTipoPersona %>">
	<INPUT type=hidden name=enviado value="<%= ((request.getAttribute("enviado")==null)?"no":(String)request.getAttribute("enviado")) %>">
	<INPUT type=hidden name=tramaArchivo value="<%= request.getAttribute("tramaArchivo") %>">
	<INPUT type=hidden name=archivo_actual value="<%= request.getAttribute("archivo_actual") %>">
	<INPUT type=hidden name=numProv value="<%= request.getAttribute("numProv") %>">
	<INPUT type=hidden name=uso_cta_cheque value='<%=request.getAttribute("USO_CTA_CHEQUE") %>'>
	<INPUT TYPE="hidden" VALUE="" NAME="vExFax">
	</FORM>
</TABLE>
</BODY>
</HTML>