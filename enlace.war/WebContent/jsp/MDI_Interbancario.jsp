<html>
<head>
<title>Deposito Interbancario</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/passmark.js"></script>
<%-- Se necesita importar el js de favoritos para poder realizar una consulta y carga de favoritos. --%>
<script type="text/javascript" src="/EnlaceMig/FavoritosEnlaceJS.js"></script>
<script type="text/javascript"
	src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script type="text/javascript" src="/EnlaceMig/util.js"></script>
<script type="text/javascript" src="/EnlaceMig/pm_fp.js"></script>

<script type="text/javascript"
	src="/EnlaceMig/filtroConsultaCuentasInterTib.js"></script>

<script type="text/javascript">

<%= request.getAttribute("ArchivoFallo") %>



//Error en la importacion del archivo ....
/************************************************************************/

/*
   M Mail Requerido
   E Requerido Texto y Numeros (a-z, A-Z, 0-9)
 6 N Requerido Numerico
 5 T Requerido Texto
 3 S Requerido Seleccion
 2 n Numerico
 1 t Texto
 0 i Ignorado
   e Texto y Numeros (a-z, A-Z, 0-9)
   m Mail
*/

 var MAX_REG= <%= request.getAttribute("NumeroRegistros") %>;
 var nav4 = window.Event ? true : false;

errores=new Array("tipoDivisaForm",
					"rfcTransferUsd",
					"radioMXN",
					"radioUSD",
					"numeroCuenta0",
					"numeroCuenta1",
					"descripcionCuenta0",
					"descripcionCuenta1",
					"comboSeleccionado0",
					"comboSeleccionado1",
					"comboSeleccionado2",
					"radio",
					"select",
					" la CUENTA DE CARGO",
					"select",
					" la CUENTA DE ABONO/M&Oacute;VIL",
					" el CONCEPTO",
					" el IMPORTE",
					" la REFERENCIA",
					"radio",
					"radio",
					" radio a",
					" el RFC",
					" radio b",
					" el IMPORTE IVA",
					"fechA",
					"select",
					"radio",
					"fechaA",
					"tipoOperTransfer",
					"fechaC",
					"Favorito",
					"radio",
					" la CUENTA DE CARGO",
					" la CUENTA DE ABONO/M&Oacute;VIL",
					" el BANCO",
					"la PLAZA (CLAVE)",
					" la PLAZA",
					" el BENEFICIARIO",
					" La SUCURSAL",
					" CONCEPTO",
					" el IMPORTE",
					" la REFERENCIA",
					"radio45",
					"radio46",
					"radio47",
					" el RFC",
					"radio48",
					" el IMPORTE IVA",
					"Fecha",
					"Radio",
					"Archivo",
					"Modulo",
					"TransReg",
					"TransNoReg",
					"TransArch",
					"Agregar");

//PYME 2015 INDRA se agrea X en la pos 22 de la seguda cad
 evalua=new Array("XXXXXXXXXXXXXTXTeNnXXXeXnXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
				  "XXXXXXXXXXXXXXXXXXXXTtXXXXNStTTNeNnXXXtXnXXXXX",
                  "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

 var strEspecial=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 var strEspecialBanco=".,/-& ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

 function textToUpper(forma)
  {
	for(i=0;i<(forma.elements.length);i++)
     {
       if(forma.elements[i].type=='text')
		 forma.elements[i].value=forma.elements[i].value.toUpperCase();
	 }
  }

 function trimString (str)
  {
    str = this != window? this : str;
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
  }

function validarCaracteresEspeciales(forma, valor, mensaje){
	var cont=0;
	var puntos=0;
	for(i0=0;i0<valor.length;i0++){
		Txt2 = valor.charAt(i0);
		if(strEspecial.indexOf(Txt2)!=-1 )
			cont++;
	}
	if(cont!=i0) {
		cuadroDialogo("No se permiten caracteres especiales en "+mensaje+", como: acentos, comas etc.",3);
		return false;
	}
	return true;
}

 function validaCampoNumerico(forma, valor, mensaje){
    var cont=0;
    var puntos=0;
	for(i1=0;i1<valor.length;i1++){
	   Txt2 = valor.charAt(i1);
	   if((Txt2>='0' && Txt2<='9') || Txt2=='.'){
		  if(Txt2=='.')
			puntos++;
		  cont++;
		}
     }
    if(cont!=i1){
	   cuadroDialogo("Favor de ingresar un valor num&eacute;rico en " + mensaje,3);
	   return false;
    }
	if(puntos>=2)
	 {
	   cuadroDialogo(mensaje+" no es un valor v&aacute;lido", 3);
	   return false;
	 }
    if(valor<=0){
	   cuadroDialogo(mensaje+ " debe ser mayor a 0", 3);
	   return false;
	 }
	 return true;
  }


function exportarArchivoPlano() {

 	var test = document.getElementsByName("tipoExportacion");
    var sizes = test.length;
    var extencion;
    for (i=0; i < sizes; i++) {
            if (test[i].checked==true) {
        	extencion = test[i].value;
        }
    }


	window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=ARCHIVO&em=ExportModel&af=af&tipoArchivo=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");

}

js_diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';
//INCIO PYME 2015 INDRA se agregan funciones para operaciones programadas
var dia;
var mes;
var anio;
var fecha_completa;
function WindowCalendar()
{
	    var m=new Date();
	    n=m.getMonth();
	    var tamanio = document.transferencia.Registradas.length;

		if (typeof(document.transferencia.Registradas.length) == 'undefined') {
			n = document.transferencia.mes1.value-1;
	   		dia = document.transferencia.dia1.value;
			mes = document.transferencia.mes1.value;
			anio = document.transferencia.anio1.value;
		} else {
			if((document.transferencia.Registradas[0].checked) || (document.transferencia.Registradas[tamanio-1].checked)) {
				n = document.transferencia.mes1.value-1;
				dia = document.transferencia.dia1.value;
				mes = document.transferencia.mes1.value;
				anio = document.transferencia.anio1.value;
			} else {
				n = document.transferencia.mes.value-1;
				dia = document.transferencia.dia.value;
				mes = document.transferencia.mes.value;
				anio = document.transferencia.anio.value;
			}
		}
	    msg=window.open("/EnlaceMig/calTib.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	    msg.focus();

}

function Actualiza()
{
	if (typeof(document.transferencia.Registradas.length) == 'undefined') {
		document.transferencia.fecha_completaReg.value=fecha_completa;
	} else {
		if ((document.transferencia.Registradas[0].checked)) {
			document.transferencia.fecha_completaReg.value=fecha_completa;
		} else if ((document.transferencia.Registradas[1].checked) && (document.transferencia.Registradas.length > 2)) {
			document.transferencia.fecha_completaNoReg.value=fecha_completa;
		}  else {
			document.transferencia.fecha_completaReg.value=fecha_completa;
		}
	}
}
//FIN PYME 2015 INDRA se agregan funciones para operaciones programadas

function seleccionarFavorito(selRen, numRen){
   //var forma = document.tabla;
   var chbFavorito = document.getElementById("favChb"+numRen);
   var txtFavorito = document.getElementById("favTxt"+numRen);

   if(selRen.checked == 0){ //deshabilitado
	   chbFavorito.checked=0;
	   chbFavorito.disabled = true;
	   txtFavorito.disabled = true;
	   txtFavorito.value ="";
   }else{
	   chbFavorito.checked=1;
	   chbFavorito.disabled = false;
	   txtFavorito.disabled = false;
	   txtFavorito.value ="";
   }
}

function modificarTxt(chbFavorito,numRen){
	   var txtFavorito = document.getElementById("favTxt"+numRen);

	   if(chbFavorito.checked == 0){ //deshabilitado
		   txtFavorito.disabled = true;
		   txtFavorito.value ="";
	   }else{
		   txtFavorito.disabled = false;
		   txtFavorito.value ="";
	   }

}

function claveBanco()
 {
   var cveBanco="";
   var ceros="0000";
   var forma=document.transferencia;

   cveBanco=forma.Banco.options[forma.Banco.selectedIndex].value;
   cveBanco=trimString(cveBanco.substring(cveBanco.indexOf("-")+1,cveBanco.length));

   if(cveBanco.length!=3)
     cveBanco=(ceros.substring(0,3-(cveBanco.length)) )+cveBanco;
   return cveBanco;
 }

function digitoVerificador(cuenta)
 {
   var pesos="371371371371371371371371371";
   var DC=0;
   var cuenta2=cuenta.substring(0,17);
   var cveBanco=claveBanco();

   if(cuenta.substring(0,3)==cveBanco)
    {
       for(i=0;i<cuenta2.length;i++)
        DC += (  parseInt(cuenta2.charAt(i)) * parseInt(pesos.charAt(i))) % 10;
       DC = 10 - (DC % 10);
       DC = (DC >= 10) ? 0 : DC;
       cuenta2+=DC;
       if((cuenta2) != cuenta)
         return false;
    }
   else
     return false;
   return true;
 }

function verificaRFCIVA(boton,tipo)
{
   forma=document.transferencia;
   var nombre="";
   var i1=0;

   if(tipo==1)
     nombre='EdoFiscalReg';
   else
     nombre='EdoFiscal';

  /* for(i1=0;i1<forma.length;i1++)
    {
       if(forma.elements[i1].type=='radio' && forma.elements[i1].name==nombre)
        if(forma.elements[i1].checked==true)
        {
          if(forma.elements[i1].value!='1')
           //boton.blur();
          else
           //boton.focus();
      /*  }
    }*/
}

function Consultar()
{
  document.transferencia.Modulo.value=0;
  document.transferencia.action="MDI_Consultar";
  document.transferencia.submit();
}

function Agregar()
 {
   textToUpper(document.transferencia);

   if(ValidaForma(document.transferencia))
    {
      if(transEnLinea()<=MAX_REG)
        {
          document.transferencia.action="MDI_Interbancario";
          document.transferencia.submit();
        }
       else
        cuadroDialogo("No puedes realizar mas de "+ MAX_REG +" transferencias en l&iacute;nea.",1);
    }
 }

/**
 @Autor Alejandro Rada Vazquez
 @param rfc cadena tipo rfc
 @return boolean true, false
 @descripcion valida una cadena y determina si cumple con el formato rfc.
  evalua:
  formato rfc personas fisicas sin homoclave
  formato rfc personas fisicas con homoclave
  formato rfc morales fisicas sin homoclave
  formato rfc morales fisicas con homoclave
  valida en fechas, los dias y meses validos
  valida para febrero unicamente hasta dia 29
*/
function ValidaRFC(rfc)
{
  rfc=rfc.value;
  var fecha=6,mes=1,dia=1;
  var TemplateRFC=/^[a-z]{3,4}[0-9]{6}(\t*|[a-z0-9]{3})$/i;
  if(TemplateRFC.test(rfc))
   {
     fecha=(rfc.length==9 || rfc.length==12)?5:6;
	 mes=rfc.substring(fecha,fecha+2);
     dia=rfc.substring(fecha+2,fecha+4);
     if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
	    if( !(eval(mes)==2 && eval(dia) >=30) )
           return true;
   }
  return false;
}

function js_enviar()
 {
	document.tabla.action="MDI_Cotizar";
    document.tabla.submit();
 }

function ValidaDuplicados()
 {

// OPC:PASSMARK 16/12/2006 BEGIN **********************************************************************************
document.tabla.pmdevice.value = add_deviceprint();
document.tabla.fsoantes.value = obtenerFSO();
// OPC:PASSMARK 16/12/2006 END **********************************************************************************
   if(ValidaTabla(document.tabla))
   {
      document.tabla.action="MDI_Interbancario";
      document.tabla.submit();
   }
 }

function InsertaReg(forma){
    var tot=0;
    var var1="";

    var carTmp=forma.AbonoReg.value;
    dcargo=forma.CargoReg.value;

    dabono=carTmp.substring(0,carTmp.indexOf('@'));
    dbanco=carTmp.substring(carTmp.indexOf('@')+1,carTmp.indexOf('!'));
    dplaza=document.transferencia.cvePlaza.value;
    dsucursal=carTmp.substring(carTmp.indexOf('!')+1,carTmp.indexOf('$'));

    dRFC=forma.RFCReg.value.toUpperCase();
    dimporteIva=forma.ImporteIVAReg.value;

	dformaAplica=(forma.FormaAplicaR[0].checked)?"H|Mismo D�a":"2|D&iacute;a Siguiente";

    if(trimString(forma.Concepto.value)==""){
    	dconcepto="TRANSFERENCIA INTERBANCARIA";
    }else{
    	dconcepto=forma.Concepto.value;
    }
    /* VECTOR 06-2016: SPID Se cambia la forma de ejecutar las validaciones */
   if(trimString(forma.RFCReg.value) === "" && $("#tipoDivisaForm").val() === "MXN" && $("#EdoFiscalRegSi").is(':checked')){
		cuadroDialogo("El RFC es obligatorio.",3);
        return false;
   }


   //Inicio GAE
   formatoIva = forma.ImporteIVAReg.value;
   if(trimString(forma.ImporteIVAReg.value) === "" && forma.tipoDivisaForm.value === "MXN" && $("#EdoFiscalRegSi").is(':checked')){
      cuadroDialogo("El IMPORTE de IVA es obligatorio.",3);
      return false;
   } else if (trimString(forma.ImporteIVAReg.value) === "" && forma.tipoDivisaForm.value === "USD" && $("#EdoFiscalRegSi").is(':checked')){
      cuadroDialogo("El IMPORTE de IVA es obligatorio.",3);
      return false;
   } else if(formatoIva.indexOf(".") > 12){
	   cuadroDialogo("Solo se permiten 12 Enteros en el Importe IVA.",3);
       return false;
   }else if(formatoIva.indexOf(".") == -1 && formatoIva.length>12){
		cuadroDialogo("Solo se permiten 12 Enteros en el Importe IVA.",3);
		return false;
   }

   if(trimString(forma.RFCReg.value) !== "" && !ValidaRFC(forma.RFCReg)){
       cuadroDialogo("el RFC no es v&aacute;lido.",3);
       return false;
   }

   /* VECTOR 06-2016: SPID VALIDACIONES NUEVAS */
   if(forma.Referencia.value != '' && !esNumerico(forma.Referencia.value)) {
		forma.Referencia.focus();
		cuadroDialogo("La Referencia Interbancaria debe ser num&eacute;rica.",3);
		return false;
   }

   if($("#tipoOperTransfer").val() === "0" && $("#tipoDivisaForm").val() === "USD"){
		cuadroDialogo("Debe seleccionar un tipo de pago",3);
		return false;
   }

 /*  if(dimporteIva.indexOf('.') < 0){
      dimporteIva+=".00";
	}*/

    dimporte=forma.Importe.value;
    if(dimporte.indexOf('.')<0)
      dimporte+=".00";
    dfecha=forma.Fecha.value;
    if(trimString(dimporteIva)!="")
     if(dimporteIva.indexOf('.')<0)
      dimporteIva+=".00";

    if(trimString(forma.TransReg.value)=="" || trimString(forma.TransReg.value)=="null")
      var1="";

    /*MSD Ley de Transparencia 11/2005*/
    var1=forma.TransReg.value+dcargo+dabono+"|"+dimporte+"|"+dconcepto+"|"+forma.Referencia.value+"|"+dbanco+"|"+dplaza+"|"+dsucursal+"|"+dfecha+"|"+dRFC+"|"+dimporteIva+"|"+dformaAplica;
    //INCIO PYME 2015 INDRA para manejar el campo fecha Ap en la operaciones programadas
    //Se agrega campo fecha para operaciones programadas
  	/* VECTOR 06-2016: SPID */
    if( forma.pintarFechaAp.value === "TRUE" &&  document.transferencia.fecha_completaReg != undefined ){
    	var1 = var1 + "|"+document.transferencia.fecha_completaReg.value+"|"+$("#tipoDivisaForm").val()+"|"+dRFC+"|"+$("#tipoOperTransfer").val();
    } else {
    	var1 = var1 + "||"+$("#tipoDivisaForm").val()+"|"+dRFC+"|"+$("#tipoOperTransfer").val();
    }
    var1 = var1 + "@";
    //FIN PYME 2015 INDRA para manejar el campo fecha Ap en la operaciones programadas
    /*MSD Ley de Transparencia 11/2005*/
    forma.TransReg.value=var1;
    return true;
  }

function InsertaNoReg(forma)
  {
    var tot2=0;
    var var2="";

    dcargo=forma.CargoNoReg.value;
    dabono=trimString(forma.AbonoNoReg.value);
    dbanco=forma.Banco.options[forma.Banco.selectedIndex].value;
    //ClaBE...
    dbanco=dbanco.substring(0,dbanco.indexOf("-"));
    dplaza=forma.Plaza.value;
    dbeneficiario=forma.Beneficiario.value;
    dsucursal=forma.Sucursal.value;

    dRFC=forma.RFC.value.toUpperCase();
    dimporteIva=forma.ImporteIVA.value;

	dformaAplica=(forma.FormaAplicaNR[0].checked)?"H|Mismo D�a":"2|D&iacute;a Siguiente";

    if(trimString(forma.uso_cta_cheque.value)!=1 || trimString(forma.uso_cta_cheque.value)!=0)
         forma.uso_cta_cheque.value=1;

    if(forma.TipoCuenta[0].checked==true)
       {
		 forma.uso_cta_cheque.value=0; /* Se valida la cuenta clabe */
         if(forma.uso_cta_cheque.value==1)
         {
             if(! (forma.AbonoNoReg.value.length!=11 && forma.AbonoNoReg.value.length!=18) )
              {
                if(forma.AbonoNoReg.value.length==18)
                {
                     if(!digitoVerificador(forma.AbonoNoReg.value))
                     {
                        forma.AbonoNoReg.focus();
                        cuadroDialogo("La cuenta no es v&aacute;lida.",3);
                        return false;
                     }
                }
              }
             else
              {
                forma.AbonoNoReg.focus();
                cuadroDialogo("La longitud de la cuenta no es v&aacute;lida.",3);
                return false;
              }
         }
        else if(forma.uso_cta_cheque.value==0)
         {
            if(!(forma.AbonoNoReg.value.length!=18) )
             {
                if(forma.AbonoNoReg.value.length==18)
                {
                     if(!digitoVerificador(forma.AbonoNoReg.value))
                     {
                        forma.AbonoNoReg.focus();
                        cuadroDialogo("La cuenta no es una Cuenta/CLABE v&aacute;lida.",3);
                        return false;
                     }
                }
              }
             else
              {
                forma.AbonoNoReg.focus();
                cuadroDialogo("La longitud de la cuenta no es v&aacute;lida.",3);
                return false;
              }
         }
       }
           else if (forma.TipoCuenta[1].checked==true)

       {
            if(forma.AbonoNoReg.value.length!=16)
             {
               forma.AbonoNoReg.focus();
               cuadroDialogo("La longitud de la cuenta no es v&aacute;lida.",3);
               return false;
             }
         }
         else if (forma.TipoCuenta[2].checked==true){
            if(forma.AbonoNoReg.value.length!=10)
             {
               forma.AbonoNoReg.focus();
               cuadroDialogo("La longitud de la cuenta no es v&aacute;lida.",3);
               return false;
             }
         }
    /*************************************************/

    if(trimString(forma.ConceptoNoReg.value)=="")
      dconcepto="TRANSFERENCIA INTERBANCARIA"
    else
      dconcepto=forma.ConceptoNoReg.value;

    for(i1=0;i1<forma.length;i1++)
     {
       if(forma.elements[i1].type=='radio' && forma.elements[i1].name=='EdoFiscal')
        if(forma.elements[i1].checked==true)
         if(forma.elements[i1].value==0)
          {
            dRFC="";
            dimporteIva="";
          }
         else
          {
            if(trimString(forma.RFC.value)=="")
             {
               //forma.RFC.focus();
               cuadroDialogo("El RFC es obligatorio.",3);
               return false;
             }
            if(trimString(forma.ImporteIVA.value)=="")
             {
               //forma.ImporteIVA.focus();
               cuadroDialogo("El IMPORTE de IVA es obligatorio.",3);
               return false;
             }
            if(!ValidaRFC(forma.RFC))
             {
               //forma.RFCReg.focus();
               cuadroDialogo("el RFC no es v&aacute;lido.",3);
               return false;
             }
            if(dimporteIva.indexOf('.')<0)
              dimporteIva+=".00";
          }
      }

    if(trimString(forma.ReferenciaNoReg.value)!="" )
     if(!esNumerico(forma.ReferenciaNoReg.value))
	 {
		forma.ReferenciaNoReg.focus();
		cuadroDialogo("La Referencia Interbancaria debe ser num&eacute;rica.",3);
		return false;
     }


    dimporte=forma.ImporteNoReg.value;
    if(dimporte.indexOf('.')<0)
      dimporte+=".00";
    dfecha=forma.FechaNoReg.value;
    if(trimString(dimporteIva)!="")
     if(dimporteIva.indexOf('.')<0)
      dimporteIva+=".00";

    if(trimString(forma.TransNoReg.value)=="" || forma.TransNoReg.value=="null")
      var2="";
    /*MSD Ley de Transparencia 11/2005*/
    var2=forma.TransNoReg.value+dcargo+dabono+"|"+dbeneficiario+"|"+dimporte+"|"+dconcepto+"|"+forma.ReferenciaNoReg.value+"|"+dbanco+"|"+dplaza+"|"+dsucursal+"|"+dfecha+"|"+dRFC+"|"+dimporteIva+"|"+dformaAplica;
    //INCIO PYME 2015 INDRA para manejar el campo fecha Ap en la operaciones programadas
    //Se agrega campo fecha para operaciones programadas
    if( forma.pintarFechaAp.value === "TRUE" ){
    	var2 = var2 + "|" + document.transferencia.fecha_completaNoReg.value+"|MXN"+"|"+dRFC+"|"+$("#tipoOperTransfer").val();
    }else{
    	var2 = var2 + "||MXN"+"|"+dRFC+"|"+$("#tipoOperTransfer").val();
    }
    var2 = var2 + "@";
    //FIN PYME 2015 INDRA para manejar el campo fecha Ap en la operaciones programadas
    /*MSD Ley de Transparencia 11/2005*/
    forma.TransNoReg.value=var2;
    return true;
  }

function trimString (str)
{
    str = this != window? this : str;
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

function ValidaTabla(forma)
 {
   var cont=0;
   var str="";
   var chb;
   var txtFav="";
   var strDescFavoritos="";

   if(forma.TransArch.value.length<=1)
    {
      for(i2=0;i2<forma.length;i2++)
       {
         if(forma.elements[i2].type=='checkbox')
          {
        	 chb = forma.elements[i2];
			var descFav;
            if( (forma.elements[i2].checked==true) )
             {
               str+="1";
               if((chb.id).indexOf('favChb')>-1){
               	   descFav = trimString(document.getElementById("favTxt"+(chb.id).substring(6)).value);
               	   if( descFav === '' ){
			   			cuadroDialogo("No se ha proporcionado una descripci&oacute;n para el favorito.",3);
     					return false;
			       }
			       if(!validaEspecialesFavorito(document.getElementById("favTxt"+(chb.id).substring(6)),"Favoritos")){
			    		return false;
			       }
            	   txtFav += forma.elements[i2+1].value +"@";
               } else {
               		cont++;
               }
             }
            else
              str+="0";
          }
       }
      if(cont==0)
       {
         cuadroDialogo("Debe Seleccionar m&iacute;nimo una transferencia.",3);
         return false;
       }
      forma.strCheck.value=str;
      forma.strFav.value=txtFav;
    }
   return true;
 }

//MSD Proyecto de Ley de Transparencia, 11/2005
function esNumerico(valor) {
    clave = "0123456789";
    for(i = 0; i < valor.length; i++) {
	esNum = false;
        for(j = 0; j < clave.length; j++) {
            if(valor.charAt(i) == clave.charAt(j)) {
                esNum = true;
                break;
            }
        }
        if(!esNum) return false;
    }
    return esNum;
}

// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento, reg) {
	var select = document.getElementById(combo);
	if ( !(select === undefined) && select != null ) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
	   		if (reg) {
	   			document.getElementById(elemento).value = obtenerCuentaNoReg(valor);
	   		} else {
				document.getElementById(elemento).value = obtenerCuenta(valor);
			}
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
    	return "";
    }
    if(res.length>1){
	    return res[0] + "|" +  res[2] + "|" ;
    }
}

function datosCtaValida(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
    	return "";
    }
    if(res.length>1){
	    return res[0];
    }
}

function obtenerCuentaNoReg(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
    	return "";
    }
    if(res.length>1){
    document.transferencia.cvePlaza.value = res[1];
	  return res[0] + "|" +  res[2] +"@"+ res[3] + "!" + res[8] + "$" + res[7];
	  //  return res[0] + "|" +  res[2] +"|";
    }
}


// FSW INDRA
// Marzo 2015
// Enlace PYMES

function ValidaForma(forma)
 {
	// FSW INDRA
	// Marzo 2015
	// Enlace PYMES

	obtenerCuentaSeleccionada("comboCuenta1", "AbonoReg", true);
	if (typeof(document.transferencia.Registradas.length) != 'undefined') {
		if((document.transferencia.Registradas[0].checked)) {
			obtenerCuentaSeleccionada("comboCuenta0", "CargoReg", false);

			// VECTOR SF SPID :: ARM Valida RFC obligatorio
			if (trimString(forma.RFCReg.value) === "" && $("#tipoDivisaForm").val() === "USD") {
				cuadroDialogo("El RFC es obligatorio.",3);
				return false;
			}
		}

		if (document.transferencia.Registradas[1]) {
			if ((document.transferencia.Registradas[1].checked)) {
				obtenerCuentaSeleccionada("comboCuenta2", "CargoNoReg", false);
			}
		}
	} else {
		obtenerCuentaSeleccionada("comboCuenta0", "CargoReg", false);
	}

	// FSW INDRA
	// Marzo 2015
	// Enlace PYMES

   for(i1=0;i1<forma.length;i1++)
    {
     if(forma.elements[i1].type=='radio' && forma.elements[i1].name!='EdoFiscal' && forma.elements[i1].name!='EdoFiscalReg' && forma.elements[i1].name!='TipoCuenta' && forma.elements[i1].name!='FormaAplica' && forma.elements[i1].name!='FormaAplicaR' && forma.elements[i1].name!='FormaAplicaNR'
     // FSW INDRA
	// Marzo 2015
	// Enlace PYMES
     &&  forma.elements[i1].name!='comboSeleccionado'
     	// FSW INDRA
	// Marzo 2015
	// Enlace PYMES
	// FSW Vector SPID Inicio
	&&  forma.elements[i1].name!='tipoMoneda'
     // FSW Vector SPID Fin
     )
      if(forma.elements[i1].checked==true)
       {

        indice=forma.elements[i1].value;
        if(indice==1){
			if(document.getElementById("comboCuenta2").selectedIndex==0){
				document.transferencia.comboCuenta2.focus();
				cuadroDialogo("Debe seleccionar la CUENTA DE CARGO", 3);
				return false;
			}

			if(forma.Banco.options[forma.Banco.selectedIndex].value==0){
				document.transferencia.Banco.focus();
				cuadroDialogo("Debe seleccionar el BANCO", 3);
				return false;
			}

			if(document.transferencia.AbonoNoReg.value=="") {
				document.transferencia.AbonoNoReg.focus();
				cuadroDialogo("la CUENTA DE ABONO/M&Oacute;VIL es obligatorio(a)", 3);
				return false;
			} else if(document.transferencia.Beneficiario.value === ""){
				document.transferencia.Beneficiario.focus();
				cuadroDialogo("el BENEFICIARIO es obligatorio(a)", 3);
				return false;
		    } else if(document.transferencia.Sucursal.value === ""){
				document.transferencia.Sucursal.focus();
				cuadroDialogo("La SUCURSAL obligatorio(a)", 3);
				return false;
		    } else if(document.transferencia.ImporteNoReg.value === ""){
				document.transferencia.ImporteNoReg.focus();
				cuadroDialogo("el IMPORTE obligatorio(a)", 3);
				return false;
		    } else if(document.transferencia.ReferenciaNoReg.value === ""){
				document.transferencia.ReferenciaNoReg.focus();
				cuadroDialogo(" la REFERENCIA obligatorio(a)", 3);
				return false;
		    }

			if(!validarCaracteresEspeciales(forma,document.transferencia.AbonoNoReg.value,"la CUENTA DE ABONO/M&Oacute;VIL")){
				document.transferencia.AbonoNoReg.focus();
				return false;
			} else if (!validarCaracteresEspeciales(forma,document.transferencia.Sucursal.value,"La SUCURSAL")){
				document.transferencia.Sucursal.focus();
				return false;
			} else if(!validarCaracteresEspeciales(forma,document.transferencia.ImporteNoReg.value,"el IMPORTE")){
				document.transferencia.ImporteNoReg.focus();
				return false;
			} else if (!validarCaracteresEspeciales(forma,document.transferencia.ReferenciaNoReg.value,"la REFERENCIA")){
				return false;
			}

			if(!validaCampoNumerico(forma,document.transferencia.ImporteNoReg.value,"el IMPORTE")){
				document.transferencia.ImporteNoReg.focus();
				document.transferencia.ImporteNoReg.value = "";
				return false;
			} else if (!validaCampoNumerico(forma,document.transferencia.ReferenciaNoReg.value,"la REFERENCIA")){
				document.transferencia.ReferenciaNoReg.focus();
				document.transferencia.ReferenciaNoReg.value = "";
				return false;
			} else if (!validaCampoNumerico(forma,document.transferencia.Sucursal.value,"La SUCURSAL")){
				document.transferencia.Sucursal.value = "";
				document.transferencia.Sucursal.focus();
				return false;
			}

			if(forma.Modulo.value=='1')
               if(!InsertaNoReg(forma))
                return false;


			return true;
        }else{
        if(ValidaTodo(forma,evalua[forma.elements[i1].value],errores))
         {
           if(indice==2)
            {

              if(trimString(forma.Archivo.value)=="")
               {
                 forma.Archivo.focus();
                 cuadroDialogo("Debe especificar nombre de Archivo.",3);
                 return false;
               }
			  document.transferencia.ArchivoNomInter.value=document.transferencia.Archivo.value;
            }

           if(indice==0)
            {
              s1=forma.CargoReg.value;
              s2=forma.AbonoReg.value;
              if (s1 == '') {
              	 cuadroDialogo("La CUENTA DE CARGO es obligatorio(a)",3);
              	  return false;
              }

              if (s2 == '') {
              	 cuadroDialogo("La CUENTA DE ABONO/M&Oacute;VIL es obligatorio(a)",3);
              	  return false;
              }
			  ctaCargo = datosCtaValida(forma.CargoReg.value);
			  ctaAbono = datosCtaValida(forma.AbonoReg.value);
              if(ctaCargo==ctaAbono)
               {
                 forma.AbonoReg.focus();
                 cuadroDialogo("La cuenta de Cargo no puede ser igual\na la cuenta de Abono.",3);
                 return false;
               }
              if(forma.Referencia.value != '' && !esNumerico(forma.Referencia.value)) {
                 forma.Referencia.focus();
                 cuadroDialogo("La Referencia Interbancaria debe ser num&eacute;rica.",3);
                 return false;
              }
              if(forma.Modulo.value=='1')
               if(!InsertaReg(forma))
                 return false;
            }

           if(indice==1)
            {
              if(forma.Modulo.value=='1')
               if(!InsertaNoReg(forma))
                return false;
            }

           return true;
         }
         }
        return false;
       }
    }
   return false;
 }


function VerificaFac(_val)
{
  inicializaTipoCta();
  if(_val==2)
   {
     if(document.transferencia.TransNoReg.value.length>1 || document.transferencia.TransReg.value.length>1)
      {
        cuadroDialogo("Solo puedes agregar transferencias en l&iacute;nea.",1);
        document.transferencia.Registradas[0].checked=true;
      }
   }
  else
   {
   	  $("#tipoMonedaMX").prop('checked', true);
   	  $("#tipoMonedaUS").prop('disabled', false);
     if(document.transferencia.TransArch.value.length>1)
      {
        cuadroDialogo("Solo puedes agregar transferencias desde archivo.",1);
        document.transferencia.Registradas[2].checked=true;
      }
   }
}

/****************************************************************************/
// Funciones para deplegar la ventana con la informacion de la cuenta

var registrosTabla=new Array();
var camposTabla=new Array();
var totalCampos=0;
var totalRegistros=0;

function despliegaDatos(campo2,indice)
 {
   // cadena de Transferencias, indicel del registro
   numeroRegistros(campo2);
   llenaTabla(campo2);
   nueva(indice);
 }

function numeroRegistros(campo2)
    {
      totalRegistros=0;
      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
    }

function llenaTabla(campo2)
 {
    var strCuentas="";
    strCuentas=campo2;
    var vartmp="";

    if(totalRegistros>=1)
      {
        for(i=0;i<totalRegistros;i++)
         {
           vartmp=strCuentas.substring(0,strCuentas.indexOf('@'));
           registrosTabla[i]=vartmp;
           if(strCuentas.indexOf('@')>0)
             strCuentas=strCuentas.substring(strCuentas.indexOf('@')+1,strCuentas.length);
         }
      }
    separaCampos(registrosTabla);
 }

function separaCampos(arrCuentas)
 {
     var i=0;
     var j=0;
     var vartmp="";
     var regtmp="";

     for(i=0;i<=totalRegistros;i++)
       camposTabla[i]=new Array();

     totalCampos=0;
     if(totalRegistros>=1)
      {
        regtmp+=arrCuentas[0];
        for(i=0;i<regtmp.length;i++)
         {
           if(regtmp.charAt(i)=='|')
            totalCampos++;
         }
        for(i=0;i<totalRegistros;i++)
         {
           regtmp=arrCuentas[i];
           for(j=0;j<totalCampos;j++)
            {
              vartmp=regtmp.substring(0,regtmp.indexOf('|'));
              camposTabla[i][j]=vartmp;
              if(regtmp.indexOf('|')>0)
                regtmp=regtmp.substring(regtmp.indexOf('|')+1,regtmp.length);
            }
           camposTabla[i][j]=regtmp;
         }
      }
 }

function nueva(indice)
 {
   var titulo=new Array("Cargo",
                        "Titular",
                        "Abono",
                        "Beneficiario",
                        "Importe",
                        "Concepto",
                        "Banco",
                        "Plaza",
                        "Sucursal");

   ventanaInfo=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo.document.open();
   ventanaInfo.document.write("<html>");
   ventanaInfo.document.writeln("<head>\n<title>Informacion</title>\n</head>");
   ventanaInfo.document.writeln("<body bgcolor='white'>");
   ventanaInfo.document.writeln("<center>");
   ventanaInfo.document.writeln("<table border=0 width=420>");
   ventanaInfo.document.writeln("<form>");
   ventanaInfo.document.writeln("<tr><th bgcolor='red' colspan=2><font color='white'>Informaci&oacute;n de Transacci&oacute;n</font></th></tr>");
   for(i=0;i<totalCampos;i++)
    {
      if(i%2)
        ventanaInfo.document.writeln("<tr><td bgcolor='#F0F0F0'>");
      else
        ventanaInfo.document.writeln("<tr><td bgcolor='white'>");
      ventanaInfo.document.writeln("<b>"+titulo[i]+":</b></td>");
      if(i%2)
        ventanaInfo.document.writeln("<td bgcolor='#F0F0F0'>");
      else
        ventanaInfo.document.writeln("<td bgcolor='white'>");
      ventanaInfo.document.write(camposTabla[indice][i]+"</td>");
      ventanaInfo.document.write("\n</tr>");
    }
   ventanaInfo.document.writeln("<tr><td><br></td></tr>");
   ventanaInfo.document.writeln("<tr><td align=center colspan=2><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
   ventanaInfo.document.writeln("</form>");
   ventanaInfo.document.writeln("</table>");
   ventanaInfo.document.writeln("</center>");
   ventanaInfo.document.writeln("</body>\n</html>");
   ventanaInfo.focus();
 }

function archivoErrores()
 {
   ventanaInfo1=window.open('/Download/Errores.html','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.focus();
}

function Selecciona()
 {
   var v1="";
   var v2="";

   v1=ventanaInfo2.document.Forma.SelPlaza.options[ventanaInfo2.document.Forma.SelPlaza.selectedIndex].value;
   v2=ventanaInfo2.document.Forma.SelPlaza.options[ventanaInfo2.document.Forma.SelPlaza.selectedIndex].text;
   if(v1)
     document.transferencia.Plaza.value=v1;
   if(v2)
     document.transferencia.Descripcion.value=v2;
 }

function TraerPlazas()
 {
   ventanaInfo2=window.open('MDI_Plazas','Plazas','width=250,height=350,toolbar=no,scrollbars=no');
   ventanaInfo2=window.open('/Download/MDI_Plazas.html','Plazas','width=250,height=350,toolbar=no,scrollbars=no');
   ventanaInfo2.focus();
 }

function transEnLinea()
 {
   var reg=0;
   var noreg=0;
   var total=0;

   numeroRegistros(document.transferencia.TransReg.value);
   reg=totalRegistros;
   numeroRegistros(document.transferencia.TransNoReg.value);
   noreg=totalRegistros;
   total=reg+noreg;
   return total;
 }


<%-- *********************************************** --%>
<%-- modificaci�n para integraci�n pva 07/03/2002    --%>
<%-- *********************************************** --%>
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var opcion;

function PresentarCuentasCargoReg()
{
   opcion=1;
   /****************** 50 */
   //PresentarCuentas();
   //Si tiene facultad 50 mostrar "$50"
   msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&cuenta=50","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
   msg.focus();

}

function PresentarCuentasAbonoReg()
{
   opcion=2;
//   PresentarCuentas();
msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=2","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();

}

function PresentarCuentasCargoNoReg()
{
  opcion=3;
  //PresentarCuentas();
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=3","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();

}

function actualizacuenta()
{
  if (opcion==1)
  {
    document.transferencia.CargoReg.value=ctaselec+"|"+ctadescr+"|";
   // document.transferencia.textCargoReg.value=ctaselec+" "+ctadescr;
  }
  else if(opcion==2)
  {
    document.transferencia.AbonoReg.value=ctaselec+"|"+ctadescr+"@"+ctaserfi+"!"+ctaprod+"$"+ctasubprod;
  //  document.transferencia.textAbonoReg.value=ctaselec+" "+ctadescr;
  }
  else if (opcion==3)
  {
    document.transferencia.CargoNoReg.value=ctaselec+"|"+ctadescr+"|";
    //document.transferencia.textCargoNoReg.value=ctaselec+" "+ctadescr;
  }
}

function EnfSelCta()
{
	/** INICIO CAMBIO PYME FSW - INDRA MARZO 2015
		validacion para cargar un favorito */
	var bandera=<%=request.getAttribute("esFavorito")%>
	if(bandera == "1"){
		document.transferencia.CargoReg.value="<%=request.getAttribute("CargoReg")%>";
		document.transferencia.AbonoReg.value='<%=request.getAttribute("AbonoReg")%>';
		document.transferencia.Concepto.value="<%=request.getAttribute("Concepto")%>";
		document.transferencia.Importe.value="<%=request.getAttribute("Importe")%>";
		document.transferencia.numeroCuenta0.value = "<%=request.getAttribute("ctaCargoFav")%>";
		document.transferencia.numeroCuenta1.value = "<%=request.getAttribute("ctaAbonoFav")%>";
    document.getElementById('Favorito').value='1';
	}else{
	   //document.transferencia.textCargoReg.value="";
	   //document.transferencia.textAbonoReg.value="";
	}
	/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 */
}


/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


function operacionesEnDolares(){
	$('#RFCReg').prop("disabled", true);
	$('#ImporteIVAReg').prop("disabled", true);

	var count = 1;

	if($("#tipoMonedaMX").is(':checked') && $("#tipoDivisaForm").val() !== "USD"){
		$("#lblOperTransfer").css("display", "none");
		$("#tipoOperTransfer").css("display", "none");
		$("#tipoDivisaForm").val("MXN");
		$("#formatoNO").prop('disabled', false);  
	    $("#formatoNO").prop('checked', true);
	    $("#formatoSI").prop('checked', false);
	}
	if($("#valTransfArch").val() == "ERR" && $("#tipoDivisaForm").val() === "USD" ){
		$("#tipoMonedaUS").prop('checked', true);
		$('#rdoAplicacionDS').prop("disabled", true);
		$("#rdoAplicacionDS").prop('checked', false);
		$("#tipoMonedaMX").prop('disabled', false);
		$("#ctaNoregistradas").prop('disabled', true);
		$("#ctaNoregistradas").prop('checked', false);
		$("input:checkbox").each(	
   			function() {
   				$("#favChb"+count).attr('disabled', true);
   				count++;
    		}
		);
		$("#hrefFavoritos").removeAttr("href");
		$('#RFCReg').prop("disabled", false);
		$('#ImporteIVAReg').prop("disabled", false);
		$("#lblOperTransfer").css("display", "block");
		$("#tipoOperTransfer").css("display", "block");
		
	}

	else if($("#tipoDivisaForm").val() === "USD"){
		$("#tipoMonedaUS").prop('checked', true);
		$('#rdoAplicacionDS').prop("disabled", true);
		$("#rdoAplicacionDS").prop('checked', false);
		$("#tipoMonedaMX").prop('disabled', true);
		$("#ctaNoregistradas").prop('disabled', true);
		$("#ctaNoregistradas").prop('checked', false);
		$("input:checkbox").each(
   			function() {
   				$("#favChb"+count).attr('disabled', true);
   				count++;
    		}
		);
		$("#hrefFavoritos").removeAttr("href");
		$('#RFCReg').prop("disabled", false);
		$('#ImporteIVAReg').prop("disabled", false);
		$("#lblOperTransfer").css("display", "block");
		$("#tipoOperTransfer").css("display", "block");
	}else if($("#tipoDivisaForm").val() === "MXN"){
		$("#tipoMonedaMX").prop('checked', true);
		if ($("#favChb1").length) {
			$("#tipoMonedaUS").prop('disabled', true);
		}

		$('#RFCReg').prop("disabled", true);
		$('#ImporteIVAReg').prop("disabled", true);
		$('#RFCReg').val("");
		$('#ImporteIVAReg').val("");
		$("#hrefFavoritos").removeAttr("href");
		$("#hrefFavoritos").attr("href","javascript:consultarFavoritos('DIBT','MDI_Interbancario');");
		$("#lblOperTransfer").css("display", "none");
		$("#tipoOperTransfer").css("display", "none");
	}
}

function operarRFC(){
	if($("#tipoMonedaUS").is(':checked') && $("#EdoFiscalRegNo").is(':checked')){
		$('#RFCReg').prop("disabled", false);
		$('#ImporteIVAReg').prop("disabled", false);
		$('#RFCReg').val("");
		$('#ImporteIVAReg').val("");
	} else {
		if($("#EdoFiscalRegSi").is(':checked')){
			$('#RFCReg').prop("disabled", false);
			$('#ImporteIVAReg').prop("disabled", false);
			$('#RFCReg').val("");
			$('#ImporteIVAReg').val("");
		} else {
			$('#RFCReg').prop("disabled", true);
			$('#ImporteIVAReg').prop("disabled", true);
			$('#RFCReg').val("");
			$('#ImporteIVAReg').val("");
		}
	}
}

function msgCifrado(){
  // INICIO FSW-INDRA PYME MARZO 2015
  	//inicializaTipoCta();
	var cadenaCargoMXN = '0,2,1';
	var cadenaAbonoMXN = '1,2,2';
	var cadenaCargoUSD = '0,2,6';
	var cadenaAbonoUSD = '1,2,5';
  	if (document.transferencia.esFav.value != null
			&& document.transferencia.esFav.value != ""
			&& document.transferencia.esFav.value == "1") {
		var cadenaCargo = '0,2,1';
		var cadenaAbono = '1,2,2';
		cadenaCargo = cadenaCargo + ',' + document.transferencia.numeroCuenta0.value + ',';
		cadenaAbono = cadenaAbono + ',' + document.transferencia.numeroCuenta1.value + ',';
		if($("#tipoDivisaForm").val() === "USD"){
			consultaMultiplesCuentas(cadenaCargoUSD+'|'+cadenaAbonoUSD);
		}else if($("#tipoDivisaForm").val() === "MXN"){
			consultaMultiplesCuentas(cadenaCargo+'|'+cadenaAbono);
		}
	} else {
		if($("#tipoDivisaForm").val() === "USD"){
			consultaMultiplesCuentas(cadenaCargoUSD+'|'+cadenaAbonoUSD);
		}else if($("#tipoDivisaForm").val() === "MXN"){
			consultaMultiplesCuentas(cadenaCargoMXN+'|'+cadenaAbonoMXN);
		}
	}
	document.transferencia.esFav.value = "";
	document.transferencia.numeroCuenta0.value = "";
	document.transferencia.numeroCuenta1.value = "";
	// FIN FSW-INDRA PYME MARZO 2015
	try {
		var mensajeCifrado = document.transferencia.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
			var arreglo = mensajeCifrado.split("|");
            var msj = arreglo[1];
            var tipo = arreglo[0];
            cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

/****************************************************************************/
<%= request.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />

</head>

<body
	style="bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
	leftMargin="0" topMargin="0" marginwidth="0" marginheight="0"
	onLoad="EnfSelCta(); operacionesEnDolares();  msgCifrado(); obtenDatosBrowser();<%=request.getAttribute("ArchivoErr")%>;">

	<table border="0" cellpadding="0" cellspacing="0" width="571">
		<tr valign="top">
			<td width="*">
				<%-- MENU PRINCIPAL --%> <%=request.getAttribute("MenuPrincipal")%>
			</td>
		</tr>
	</table>

	<%=request.getAttribute("Encabezado")%>

	<FORM NAME="transferencia" method="post"
		onSubmit="return ValidaForma(this);" action="MDI_Interbancario"
		enctype="multipart/form-data">
		<input type="hidden" id="tipoDivisaForm" name="tipoDivisaForm"
			value="<%=request.getAttribute("valorDivisaForm")%>" /> 
		<input
			type="hidden" id="rfcTransferUsd" name="rfcTransferUsd"
			value="<%=request.getAttribute("rfcTransferUsd")%>" />
		<table align="center" border="0" cellpadding="0" cellspacing="0">

			<%--
 <tr>
  <td colspan=2 class="tittabdat">
   <table border=0 width="100%">
    <tr>
     <td class="tittabdat"> &nbsp; I.V.A. &nbsp; </td>
     <td class="tittabdat"><input type=text name=IVA size=10 maxlength=10></td>
     <td class="tittabdat"> &nbsp; RFC &nbsp; </td>
     <td class="tittabdat"><input type=text name=RFC size=10 maxlength=10></td>
    </tr>
   </table>
  </td>
 </tr>
 //--%>

			<tr>
				<td>

					<table ALIGN="center" style="boder: 0;" cellpadding="2"
						cellspacing="3" class="textabdatcla">

						<tr>
							<td class="tittabdat" align="left">Capture los datos de su
								transferencia</td>
						</tr>
						<tr>
							<td colspan="4"><%@ include
									file="/jsp/filtroConsultaCuentasInter.jspf"%>
							</td>
						</tr>

						<tr>
							<td class="tabmovtex" valign="top" colspan="4" width="673">Nota:
								Los campos de Cuenta de Cargo y Cuenta de Abono se
								mostrar&aacute;n solo las primeras 25 cuentas, ordenadas
								num&eacute;ricamente, para obtener m&aacute;s cuentas debe de
								buscarlas utilizando los filtros.</td>
						</tr>

						<tr>
							<td colspan="4" class="tittabdat"><INPUT TYPE="radio"
								id="ctaConregisto" value=0 NAME="Registradas"
								onClick="VerificaFac(0);" checked />&nbsp;<b>Cuentas
									Registradas</b></td>
						</tr>

						<tr>
							<td class="textabdatcla" colspan="4" width="100%">
								<table class="textabdatcla" cellspacing="3" cellpadding="2">
									<tr>
										<td class="tabmovtex" colspan="2" width="50%">Cuenta de
											Cargo</td>
										<td class="tabmovtex" colspan="2">Cuenta de
											Abono/M&oacute;vil</td>
									</tr>
									<tr>
										<td colspan="2" class="tabmovtex">
											<%--  <input type="text" name=textCargoReg  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();">
        <A HREF="javascript:PresentarCuentasCargoReg();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A> --%>
											<%@ include file="/jsp/listaCuentasTransferencias.jspf"%>
											<input type="hidden" name="CargoReg" id="CargoReg" value="" />

										</td>
										<td colspan="2" class="tabmovtex">
											<%-- <input type="text" name=textAbonoReg  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
         <A HREF="javascript:PresentarCuentasAbonoReg();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
          --%> <%@ include file="/jsp/listaCuentasTransferencias.jspf"%>
											<input type="hidden" name="AbonoReg" id="AbonoReg" value='' />
										</td>
									</tr>

									<tr>
										<td class="tabmovtex" colspan="4"><br />
										<td>
									</tr>

									<tr>
										<td class="tabmovtex">Concepto:</td>
										<td class="tabmovtex"><INPUT TYPE="text" SIZE="25"
											MAXLENGTH="40" NAME="Concepto" /></td>
										<td class="tabmovtex">Importe:</td>
										<td class="tabmovtex"><INPUT TYPE="text" NAME="Importe"
											SIZE="10" MAXLENGTH="17" /></td>
									</tr>

									<tr>
										<td class="tabmovtex">Referencia Interbancaria:</td>
										<td class="tabmovtex"><INPUT TYPE="text" SIZE="7"
											MAXLENGTH="7" NAME="Referencia" /></td>

										<td class="tabmovtex" colspan="2">
											<table cellspacing="0" cellpadding="0" border="0">
												<tr>
													<td class="tabmovtex">Requiere estado de cuenta
														fiscal?</td>
													<td class="tabmovtex">&nbsp; <input type="radio"
														id="EdoFiscalRegNo" onclick="operarRFC();"
														name="EdoFiscalReg" value="0" checked /></td>
													<td class="tabmovtex">No</td>
													<td class="tabmovtex">&nbsp; <input type="radio"
														id="EdoFiscalRegSi" onclick="operarRFC();"
														name="EdoFiscalReg" value="1" /></td>
													<td class="tabmovtex">Si</td>
												</tr>
											</table>
										</td>
									</tr>

									<tr>
										<td class="tabmovtex">Forma aplicaci&oacute;n</td>
										<td class="tabmovtex"><input type="radio"
											id="rdoAplicacionMD" name="FormaAplicaR" value="H" checked />
											Mismo D�a</td>
										<td class="tabmovtex">RFC Beneficiario: &nbsp;</td>
										<td class="tabmovtex"><input type="text" id="RFCReg"
											name="RFCReg" size="13" maxlength="13"
											onFocus="verificaRFCIVA(this,1);" /></td>
									</tr>

									<tr>
										<td class="tabmovtex"><br /></td>
										<td class="tabmovtex"><input type="radio"
											id="rdoAplicacionDS" name="FormaAplicaR" value="2" />
											D&iacute;a Siguiente</td>
										<td class="tabmovtex">Importe IVA: &nbsp;</td>
										<td class="tabmovtex"><input type="text"
											id="ImporteIVAReg" name="ImporteIVAReg" size="13"
											maxlength="15" onFocus="verificaRFCIVA(this,1);" /></td>
									</tr>
									<tr>
												<td class="tabmovtex" colspan="2">
													<table>
														<%
															if (request.getAttribute("fechaApliacionRegistradas") != null
																	&& !"".equals(request
																			.getAttribute("fechaApliacionRegistradas"))) {
																out.print(request.getAttribute("fechaApliacionRegistradas"));
															}
														%>
													</table>
												</td>
												<td valign="top" class="tabmovtex"><samp id="lblOperTransfer" class="tabmovtex">Tipo de pago:&nbsp;</samp></td>
												<td valign="top" class="tabmovtex">
													<select class="tabmovtex" id="tipoOperTransfer" name="tipoOperTransfer">
														<option class="tabmovtex" value="0"> Seleccione una Opci&oacute;n </option>
														<option value="TTLIQVAL">Liquidaci&oacute;n de operaci&oacute;n con valores</option>
														<option value="TTLIQDER">Liquidaci&oacute;n de operaci&oacute;n derivada</option>
														<option value="TTLIQCAM">Liquidaci&oacute;n de operaci&oacute;n cambiaria</option>
														<option value="TTPAGSER">Pago de servicios</option>
														<option value="TTPAGBIE">Pago de bienes</option>
														<option value="TTOTOPRE">Otorgamiento de pr&eacute;stamo</option>
														<option value="TTPAGPRE">Pago de pr&eacute;stamo</option>
														<option value="TTOTROS">Otros</option>
													</select>
												</td>
									</tr>
								</table>
							</td>
						</tr>

						<input type="hidden" name="Fecha" size="10"
							value="<%=request.getAttribute("Fecha")%>" onFocus="blur();" />
						<INPUT TYPE="HIDDEN" NAME="Favorito" id="Favorito" />

						<tr>
							<td bgcolor="white" colspan="4"><br /></td>
						</tr>

						<%--  Modulo para las cuentas no registradas //--%>
						<%
							if (request.getAttribute("cuentasNoRegistradas") != null)
								out.print(request.getAttribute("cuentasNoRegistradas"));
						%>


						<%--  ************************************** --%>

						<%--  Modulo para la importacion desde archivo --%>
						<%
							if (request.getAttribute("strImportar") != null)
								out.print(request.getAttribute("strImportar"));
						%>

						<%--  **************************************** --%>
					</table>

				</td>
			</tr>
		</table>

		<p>
			<%-- CAMBIO PYME FSW - INDRA MARZO 2015 se agrega hidden esFav para el manejo de favortios --%>
			<input type="hidden" name="esFav" id="esFav"
				value="<%=request.getAttribute("esFavorito")%>" /> <input
				type="hidden" name="Modulo"
				value="<%=request.getAttribute("Modulo")%>" /> <input type="hidden"
				name="TransReg" value="<%=request.getAttribute("TransReg")%>" />
			<input type="hidden" name="TransNoReg"
				value="<%=request.getAttribute("TransNoReg")%>" /> <input
				type="hidden" name="TransArch"
				value="<%=request.getAttribute("TransArch")%>" /> <input
				type="hidden" name="uso_cta_cheque"
				value="<%=request.getAttribute("USO_CTA_CHEQUE1")%>" /> <input
				type="hidden" name="ArchivoNomInter"
				value="<%=request.getAttribute("ArchivoNomInter")%>" /> <INPUT
				TYPE="hidden" name="mensajeCifrado"
				value="<%=request.getAttribute("mensajeCifrado")%>" />
		<table align="center" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><a href="javascript:Agregar();" border="0"><img
						src="/gifs/EnlaceMig/gbo25290.gif" style="border: 0;" /></a></td>
				<td><a href="javascript:Consultar();" border="0"><img
						src="/gifs/EnlaceMig/gbo25400.gif" style="border: 0;" /></a></td>
				<!-- Todos los botones que realicen una consulta a favoritos deben de implementarse de la siguiente forma, lo que cambia son los parametros de la funcion consultarFavoritos, ya que
     determinar el tipo de operacion y que servlet se debe de ser invocado para el redireccionamiento. -->
				<td valign="top" width="76"><a
					href='javascript:consultarFavoritos("DIBT","MDI_Interbancario");'
					id="hrefFavoritos" name="hrefFavoritos" border="0"><img
						src="/gifs/EnlaceMig/favoritos.png" style="border: 0; width: 115;"
						alt="Consultar Favoritos" /></a></td>

				<td>
					<%
						if (request.getAttribute("Status") != null)
							out.print(request.getAttribute("Status"));
					%>
				</td>
			</tr>
		</table>
		<input type="hidden" name="cvePlaza" id="cvePlaza" value="" /> <input
			type="hidden" id="datosBrowser" name="datosBrowser" value="" />
		<input type="hidden" id="valTransfArch" name="valTransfArch"
			value="<%=request.getAttribute("valTransfArch")%>" />
	</form>

	<FORM NAME="tabla" method="post" onSubmit="return ValidaTabla(this);"
		action="MDI_Cotizar">

		<!--  OPC:PASSMARK 16/12/2006 ******************* BEGIN -->
		<INPUT TYPE="hidden" name="pmdevice" value="" /> <INPUT TYPE="hidden"
			name="fsoantes" value="" />
		<!--  OPC:PASSMARK 16/12/2006 ******************* END -->

		<%
			if (request.getAttribute("msgErrorRegNoReg") != null) {
		%>
		<script type="text/javascript">
    cuadroDialogo("<%=request.getAttribute("msgErrorRegNoReg")%>");
  </script>
		<%
			}
		%>
		<table border="0" align="center">
			<tr>
				<td align="right" class="textabref">
					<%
						if (request.getAttribute("totalRegistros") != null)
							out.print(request.getAttribute("totalRegistros"));
					%>&nbsp;
					<%
						if (request.getAttribute("importeTotal") != null)
							out.print(request.getAttribute("importeTotal"));
					%>
					&nbsp;
					<%
 						if (request.getAttribute("valorDivisaForm") != null && request.getAttribute("importeTotal") != null)
 							out.print(request.getAttribute("valorDivisaForm"));
 					%>
				</td>
			</tr>

			<tr>
				<td>
					<%
						if (request.getAttribute("Tabla") != null)
							out.print(request.getAttribute("Tabla"));
					%>
				</td>
			</tr>

			<tr>
				<td class="textabref">
					<%
						if (request.getAttribute("Mensaje") != null)
							out.print(request.getAttribute("Mensaje"));
					%>
				</td>
			</tr>
			<%
				if (request.getAttribute("Botones") != null
						&& !"".equals(request.getAttribute("Botones"))) {
			%>
			<tr>


				<td class="tabmovtex11" align="center"><a
					style="cursor: pointer;">Exporta en TXT <input
						id="tipoExportacion" type="radio" value="txt"
						name="tipoExportacion" /></a></td>
			<tr>
			<tr>


				<td class="tabmovtex11" align="center"><a
					style="cursor: pointer;">Exporta en XLS <input
						id="tipoExportacion" type="radio" value="csv" checked
						name="tipoExportacion" /></a></td>
			<tr>
				<%
					}
				%>

			<tr>
				<td align="center"><br />
					<%
						if (request.getAttribute("Botones") != null)
							out.print(request.getAttribute("Botones"));
					%></td>
			</tr>

		</table>

		<input type="hidden" name="strCheck" /> <input type="hidden"
			name="strFav" /> <input type="hidden" name="Lineas"
			value="<%=request.getAttribute("Lineas")%>" /> <input
			type="hidden" name="Importe"
			value="<%=request.getAttribute("importeTotal")%>" /> <input
			type="hidden" name="Modulotabla"
			value="<%=request.getAttribute("ModuloTabla")%>" /> <input
			type="hidden" name="TransReg"
			value="<%=request.getAttribute("TransReg")%>" /> <input type="hidden"
			name="TransNoReg" value="<%=request.getAttribute("TransNoReg")%>" />
		<input type="hidden" name="TransArch"
			value="<%=request.getAttribute("TransArch")%>" /> <input
			type="hidden" name="archivoEstatus"
			value="<%=request.getAttribute("archivoEstatus")%>" /> <input
			type="hidden" name="comprobante"
			value="<%=request.getAttribute("comprobante")%>" /> <input
			type="hidden" name="Registradas"
			value="<%=request.getAttribute("Registradas")%>" /> <input
			type="hidden" name="Modulo" value="2" /> <input type="hidden"
			name="ArchivoNomInter"
			value="<%=request.getAttribute("ArchivoNomInter")%>" /> <input
			type="hidden" name="divTrx"
			value="<%=request.getAttribute("valorDivisaForm")%>" />
	</form>

</body>

<%--  Evitar que guarde se guarde info en cache  --%>
<head>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
<META HTTP-EQUIV="Expires" CONTENT="-1" />
<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
<META HTTP-EQUIV="Expires" CONTENT="-1" />
</head>

</html>
<%-- 2007.01 --%>