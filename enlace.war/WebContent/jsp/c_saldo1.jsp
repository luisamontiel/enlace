<%
String cuentas = (String) request.getAttribute("cuentas");

session.setAttribute("cuentas",cuentas);

%>
<html>
<!-- #BeginTemplate "/Templates/principal.dwt" -->
	<head>
	<!-- #BeginEditable "doctitle" -->
		<title>Enlace</title>
		<!-- #EndEditable -->
		<!-- #BeginEditable "MetaTags" -->
		<meta http-equiv="Content-Type" content="text/html;">
		<meta name="Codigo de Pantalla" content="s25010">
		<meta name="Proyecto" content="Portal">
		<meta name="Version" content="1.0">
		<meta name="Ultima version" content="27/04/2001 18:00">
		<meta name="Desarrollo de codigo HTML" content="Getronics Mexico">
		<meta http-equiv="Expires" content="1">
		<meta http-equiv="pragma" content="no-cache">
		 <%
             	response.setHeader	( "Cache-Control"	, "no-store"	);
		response.setHeader	( "Pragma"		, "no-cache"	);
            	response.setDateHeader	( "Expires"		, 0		);
	         %>
		<!-- #EndEditable -->
        <!-- Funciones de Javascript -->
        <script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
        <Script language = "JavaScript">

        var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
		diasInhabiles = '<%= request.getAttribute("DiasInhabiles") %>';

		var dia;
		var mes;
		var anio;
		var fechaseleccionada;
		var opcioncaledario;
		var cuentasTotales = "0";
		var cuentasDes = "";

		function SeleccionaFecha(ind)
		{
			var m=new Date();
			n=m.getMonth();
			js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
			gdia90 = "<%= request.getAttribute( "gdia90" ) %>";

			Indice=ind;

			var parMovFechas1 = "<%= request.getAttribute("Movfechas") %>";
			var FrmFechasDia = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasMes = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasAnio = parMovFechas1;

			var FrmFechasDia1 = gdia90.substring(0,gdia90.indexOf("/"));
			gdia90 = gdia90.substring(gdia90.indexOf("/") + 1);
			var FrmFechasMes1 = gdia90.substring(0,gdia90.indexOf("/"));
			gdia90 = gdia90.substring(gdia90.indexOf("/") + 1);
			var FrmFechasAnio1 = gdia90;

			dia = FrmFechasDia;//09;
			mes = FrmFechasMes;//10;
			anio = FrmFechasAnio;//2002; bc_Calendario

			dia1 = FrmFechasDia1;//09;
			mes1 = FrmFechasMes1;//10;
			anio1 = FrmFechasAnio1;//2002; bc_Calendario

			msg=window.open("/EnlaceMig/N43_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
			msg.focus();
		}//fin selecciona

		function SeleccionaCuenta(ind)
		{
			Indice=ind;
			// vswf:meg cambio de NASApp por Enlace 08122008
			var msg = window.open("/Enlace/enlaceMig/jsp/c_saldo1_M.jsp","trainerWindow","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=360");

			msg.focus();
		}//fin seleccionaCuenta

		function Actualiza1()
		{
			 document.fsaldoReg.gcta_saldo.value = "";
			 var algo = document.fsaldoReg.gcta_saldo.value;
		     document.fsaldoReg.gcta_saldo.value = algo + cuentasDes;
			 document.fsaldoReg.gj.value = "0";
			 var gg = parseInt(document.fsaldoReg.gj.value,10);
			 var yy = parseInt(cuentasTotales,10);
		     document.fsaldoReg.gj.value = gg + yy;
		}//fin actualiza1

		 function WindowCalendar(){
		    var m=new Date();
		    m.setFullYear(document.Frmfechas.strAnio.value);
		    m.setMonth(document.Frmfechas.strMes.value);
		    m.setDate(document.Frmfechas.strDia.value);
		    n=m.getMonth();
			dia=document.Frmfechas.strDia.value;
			mes=document.Frmfechas.strMes.value;
			anio=document.Frmfechas.strAnio.value;
		    opcioncalendario=1;
		    msg=window.open("/EnlaceMig/calpas1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		    msg.focus();
		}

		var gConfirmacion= false;
		function confirmacion()
		{
			result=cuentachange();
			gConfirmacion = result;

			var indice = document.fsaldoReg.gperiodo.selectedIndex;
			var dato = document.fsaldoReg.gperiodo.options[indice].value;
			document.fsaldoReg.gperiodo1.value = dato;
			indice = document.fsaldoReg.ghorario.selectedIndex;
			dato = document.fsaldoReg.ghorario.options[indice].value;
			document.fsaldoReg.ghorario1.value = dato;
			indice = document.fsaldoReg.gtipomov.selectedIndex;
			dato = document.fsaldoReg.gtipomov.options[indice].value;
			document.fsaldoReg.gtipomov1.value = dato;
			document.fsaldoReg.gimporte1.value = document.fsaldoReg.gimporte.value;
			indice = document.fsaldoReg.gformato.selectedIndex;
			dato = document.fsaldoReg.gformato.options[indice].value;
			document.fsaldoReg.gformato1.value = dato;
			indice = document.fsaldoReg.greporte.selectedIndex;
			dato = document.fsaldoReg.greporte.options[indice].value;
			document.fsaldoReg.greporte1.value = dato;
			indice = document.fsaldoReg.gcanal.selectedIndex;
			dato = document.fsaldoReg.gcanal.options[indice].value;
			document.fsaldoReg.gcanal1.value = dato;
			document.fsaldoReg.gfecha2.value = document.fsaldoReg.gfecha1.value;
			indice = document.fsaldoReg.gdiasemana.selectedIndex;
			dato = document.fsaldoReg.gdiasemana.options[indice].value;
			document.fsaldoReg.gdiasemana1.value = dato;
			document.fsaldoReg.gemail1.value = document.fsaldoReg.gemail.value;

			if ( result == true )
			{
		     cuadroDialogo("Desea realizar su programaci&oacute;n de movimientos?",5);
			}
		}//fin confirmacion

		function continua()
		{
		   if (gConfirmacion == true )
		   {
		      if (respuesta==1){
				document.fsaldoReg.submit();
			  }
		        else return false;
		   }
		   else
		       return false;
		}//fin continua

		function LimpiarDatos()
		{
			document.fsaldoReg.gformato.value = "0";
			if (document.fsaldoReg.gformato.value == "1")
			{
				  document.fsaldoReg.gformato.options[1].selected=true;
				  document.fsaldoReg.gformato.value="1";
			}
			else
			{
				  document.fsaldoReg.gformato.options[0].selected=true;
				  document.fsaldoReg.gformato.value="0";
			}
			document.fsaldoReg.gformato.focus;

			document.fsaldoReg.gperiodo.value = "1";
			if (document.fsaldoReg.gperiodo.value=="1")
				{
				  document.fsaldoReg.gperiodo.options[0].selected=true;
				  document.fsaldoReg.gperiodo.value="1";
				}
			  if (document.fsaldoReg.gperiodo.value=="2")
				{
				  document.fsaldoReg.gperiodo.options[1].selected=true;
				  document.fsaldoReg.gperiodo.value="2";
				}
			 if (document.fsaldoReg.gperiodo.value=="3")
				{
				  document.fsaldoReg.gperiodo.options[2].selected=true;
				  document.fsaldoReg.gperiodo.value="3";
				}
				document.fsaldoReg.gperiodo.focus;

			document.fsaldoReg.ghorario.value = "08:00";
			if (document.fsaldoReg.ghorario.value=="08:00")
				{ document.fsaldoReg.ghorario.options[0].selected=true;
				  document.fsaldoReg.ghorario.value="08:00";}
			  if (document.fsaldoReg.ghorario.value=="09:00")
				{ document.fsaldoReg.ghorario.options[1].selected=true;
				  document.fsaldoReg.ghorario.value="09:00";}
			  if (document.fsaldoReg.ghorario.value=="10:00")
				{ document.fsaldoReg.ghorario.options[2].selected=true;
				  document.fsaldoReg.ghorario.value="10:00";}
			  if (document.fsaldoReg.ghorario.value=="11:00")
				{ document.fsaldoReg.ghorario.options[3].selected=true;
				  document.fsaldoReg.ghorario.value="11:00";}
			  if (document.fsaldoReg.ghorario.value=="12:00")
				{ document.fsaldoReg.ghorario.options[4].selected=true;
				  document.fsaldoReg.ghorario.value="12:00";}
			  if (document.fsaldoReg.ghorario.value=="13:00")
				{ document.fsaldoReg.ghorario.options[5].selected=true;
				  document.fsaldoReg.ghorario.value="13:00";}
			  if (document.fsaldoReg.ghorario.value=="14:00")
				{ document.fsaldoReg.ghorario.options[6].selected=true;
				  document.fsaldoReg.ghorario.value="14:00";}
			  if (document.fsaldoReg.ghorario.value=="15:00")
				{ document.fsaldoReg.ghorario.options[7].selected=true;
				  document.fsaldoReg.ghorario.value="15:00";}
			  if (document.fsaldoReg.ghorario.value=="16:00")
				{ document.fsaldoReg.ghorario.options[8].selected=true;
				  document.fsaldoReg.ghorario.value="16:00";}
			  if (document.fsaldoReg.ghorario.value=="17:00")
				{ document.fsaldoReg.ghorario.options[9].selected=true;
				  document.fsaldoReg.ghorario.value="17:00";}
			  if (document.fsaldoReg.ghorario.value=="18:00")
				{ document.fsaldoReg.ghorario.options[10].selected=true;
				  document.fsaldoReg.ghorario.value="18:00";}
			  if (document.fsaldoReg.ghorario.value=="19:00")
				{ document.fsaldoReg.ghorario.options[11].selected=true;
				  document.fsaldoReg.ghorario.value="19:00";}
			  if (document.fsaldoReg.ghorario.value=="20:00")
				{ document.fsaldoReg.ghorario.options[12].selected=true;
				  document.fsaldoReg.ghorario.value="20:00";}
			  document.fsaldoReg.ghorario.focus;

			document.fsaldoReg.gdiasemana.disabled = true;
			document.fsaldoReg.gdiasemana.value = "1";
			if (document.fsaldoReg.gdiasemana.value=="1")
				{
				  document.fsaldoReg.gdiasemana.options[0].selected=true;
				  document.fsaldoReg.gdiasemana.value="1";
				}
			  if (document.fsaldoReg.gdiasemana.value=="2")
				{
				  document.fsaldoReg.gdiasemana.options[1].selected=true;
				  document.fsaldoReg.gdiasemana.value="2";
				}
			   if (document.fsaldoReg.gdiasemana.value=="3")
				{
				  document.fsaldoReg.gdiasemana.options[2].selected=true;
				  document.fsaldoReg.gdiasemana.value="3";
				}
			   if (document.fsaldoReg.gdiasemana.value=="4")
				{
				  document.fsaldoReg.gdiasemana.options[3].selected=true;
				  document.fsaldoReg.gdiasemana.value="4";
				}
			   if (document.fsaldoReg.gdiasemana.value=="5")
				{
				  document.fsaldoReg.gdiasemana.options[4].selected=true;
				  document.fsaldoReg.gdiasemana.value="5";
				}
				document.fsaldoReg.gdiasemana.focus;

			document.fsaldoReg.gtipomov.value = "B";
			if (document.fsaldoReg.gtipomov.value=="B")
				{
				  document.fsaldoReg.gtipomov.options[0].selected=true;
				  document.fsaldoReg.gtipomov.value="B";
				}
			  if (document.fsaldoReg.gtipomov.value=="C")
				{
				  document.fsaldoReg.gtipomov.options[1].selected=true;
				  document.fsaldoReg.gtipomov.value="C";
				}
			 if (document.fsaldoReg.gtipomov.value=="A")
				{
				  document.fsaldoReg.gtipomov.options[2].selected=true;
				  document.fsaldoReg.gtipomov.value="A";
				}
			document.fsaldoReg.gtipomov.focus;

			document.fsaldoReg.gChksvc.checked = false;
			document.fsaldoReg.gChksvc.disabled = false;
			document.fsaldoReg.gChkvista.checked = false;
			document.fsaldoReg.gChkvista.disabled = false;

			document.fsaldoReg.gimporte.value = "";
			document.fsaldoReg.gimporte.disabled = false;

			document.fsaldoReg.gfecha1.value = "";
			document.fsaldoReg.gfecha1.value = document.fsaldoReg.gfecha.value;

			document.fsaldoReg.greporte.value = "1";
			document.fsaldoReg.greporte.disabled = false;
			if (document.fsaldoReg.greporte.value=="1")
				{
				  document.fsaldoReg.greporte.options[0].selected=true;
				  document.fsaldoReg.greporte.value="1";
				}
			  if (document.fsaldoReg.greporte.value=="2")
				{
				  document.fsaldoReg.greporte.options[1].selected=true;
				  document.fsaldoReg.greporte.value="2";
				}
			document.fsaldoReg.greporte.focus;

			document.fsaldoReg.gcanal.value = "2";
			if (document.fsaldoReg.gcanal.value=="0")
				{
				  document.fsaldoReg.gcanal.options[0].selected=true;
				  document.fsaldoReg.gcanal.value="2";
				}
			  if (document.fsaldoReg.gcanal.value=="1")
				{
				  document.fsaldoReg.gcanal.options[1].selected=true;
				  document.fsaldoReg.gcanal.value="1";
				}
			   if (document.fsaldoReg.gcanal.value=="3")
				{
				  document.fsaldoReg.gcanal.options[2].selected=true;
				  document.fsaldoReg.gcanal.value="3";
				}
			document.fsaldoReg.gcanal.focus;

			document.fsaldoReg.gemail.value = "";
			document.fsaldoReg.gemail.disabled = true;
			document.fsaldoReg.ghorario.focus;

			val_periodo();

		}//fin limpiar

		function Actualiza()
		{
			var m=new Date();

			var parMovFechas1 = "<%= request.getAttribute("Movfechas") %>";
			var FrmFechasDia = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasMes = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasAnio = parMovFechas1;

			dia = FrmFechasDia;//09;
			mes = FrmFechasMes;//10;
			anio = FrmFechasAnio;//2002; bc_Calendario

			if (fechaseleccionada.substring(0,2) == dia)
			{
				cuadroDialogo("Debe seleccionar un dia habil despues de dia de hoy! "+dia,3);
			}
			else
			{
				document.fsaldoReg.gfecha1.value=fechaseleccionada;
			}
		}

		var ctaselec;
		var ctadescr;
		var ctatipre;
		var ctatipro;
		var ctaserfi;
		var ctaprod;
		var ctasubprod;
		var tramadicional;
		var cfm;

		</script>

		<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
		<script language="JavaScript">
		<!--

		function MM_preloadImages()
		{ //v3.0
		  var d=document;
		  if(d.images){
			 if(!d.MM_p)
			     d.MM_p=new Array();
		     var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			 for(i=0; i < a.length; i++)
		        if(a[i].indexOf("#")!=0){
				       d.MM_p[j]=new Image;
					   d.MM_p[j++].src=a[i];
					 }
			  }
		}

		function MM_swapImgRestore()
		{ //v3.0
		  var i,x,a=document.MM_sr;
		  for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
			  x.src = x.oSrc;
		}

		function MM_findObj(n, d)
		{ //v3.0
		  var p,i,x;
		   if(!d) d = document;
		   if((p=n.indexOf("?"))>0&&parent.frames.length){
		        d = parent.frames[n.substring(p+1)].document;
			    n = n.substring(0,p);
			  }
		   if(!(x = d[n]) && d.all)
			    x = d.all[n];
		   for(i=0;!x&&i<d.forms.length;i++)
		       x = d.forms[i][n];
		   for(i=0;!x&&d.layers&&i<d.layers.length;i++)
			   x = MM_findObj(n,d.layers[i].document);
		   return x;
		}

		function MM_swapImage()
		{ //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments;
		      document.MM_sr=new Array;
		  for(i=0;i<(a.length-2);i+=3)
		      if((x = MM_findObj(a[i]))!=null){
			 	  document.MM_sr[j++] = x;
				  if(!x.oSrc) x.oSrc = x.src;
				  x.src = a[i+2];
			 }
		}
		<%= request.getAttribute("newMenu") %>
		//-->

		<!--
		function cuentachange()
		{
		   var h=document.fsaldoReg.elements.length;
		    var ctas=""; // = new Array (h);
		    var j=0;
			var j1=0;
			var t=0;
			var correos = document.fsaldoReg.gemail.value;
			var correos1 = 0;
			var comas1 = 0;
			var datoL = 0;
			var resp = "si";
			var conta = 0;
			var inco = 0;
			var inco1 = "si";

			//valida si se capturo el email
			dato=document.fsaldoReg.gcanal.value;
			datoL=correos.length;

			if (dato=="1" && datoL>0)
			{
				//valida los caracteres correctos del email
				for (Count=0; Count<=correos.length; Count++)
				{
				   StrChar = correos.substring(Count, Count+1);
				   resp = isNumOrChar(StrChar);
				   if(resp == "no")
					{conta = conta + 1;}
				}

				if(conta > 0)
				{
					cuadroDialogo("Usted debe capturar la direcci&oacute;n de e-mail correctamente (Hay alg&uacute;n caracter invalido ( !$%&/()?�+{}�|[] ) &oacute; Espacios en Blanco).",3);
				    return false;
				}
			}

			if (dato=="1" && datoL==0)
			{
				cuadroDialogo("Usted debe capturar al menos una direcci&oacute;n de e-mail.",3);
			    return false;
			}
			else
			{
				if (correos.substring(0,1) == "@")
				{
					cuadroDialogo("Usted debe capturar al menos una direcci&oacute;n de e-mail valida (__@_.com).",3);
				    return false;
				}// fin if

				var er = correos.length;
				var re = correos.length-1;
				if (correos.substring(re,er) != ",")
				{
					correos = correos + ",";
				}// fin if
	 		    for (i=0;i<correos.length;i++)
			    {
					if(correos.substring(i,i+1) == "@")
					{
						correos1 = correos1 + 1;

						//valida que tenga el formato correcto
						inco1 = isNumOrChar1(correos.substring(i+1,i+2));
						if(inco1 == "no")
						{inco = inco + 1;}
						inco1 = isNumOrChar1(correos.substring(i-1,i));
						if(inco1 == "no")
						{inco = inco + 1;}
					}
					if(correos.substring(i,i+1) == ",")
					{comas1 = comas1 + 1;}
			    }
			}
			if (correos1==0 && dato=="1")
			{
				cuadroDialogo("Usted debe capturar al menos una direcci&oacute;n de e-mail valida (__@_.com).",3);
			    return false;
			}
			if (inco > 0 && dato=="1")
			{
				cuadroDialogo("Usted debe capturar al menos una direcci&oacute;n de e-mail valida (__@_.com).",3);
			    return false;
			}
			var compara = 0;
			compara = correos1 - comas1;

			if (compara!=0 && dato=="1")
			{
				cuadroDialogo("Usted debe separar los e-mails con comas &oacute; hay un email incorrecto (__@_.com,__@_.com).",3);
			    return false;
			}
			if (comas1>5 && dato=="1")
			{
				cuadroDialogo("Puede capturas maximo 5 e-mails separados con comas (__@_.com,__@_.com).",3);
			    return false;
			}

		     document.fsaldoReg.gjj.value = correos1;

			//Valida las cuentas seleccionadas
			ddFrom = document.fsaldoReg.CuentasS;
			var contenido = "";
			var cuantas = ddFrom.options.length - 1;

			for(idx=1;idx<ddFrom.options.length;idx++)
			{
				wrkVal = ddFrom.options[idx].value;
				wrkText = ddFrom.options[idx].text;

				contenido = contenido + wrkVal + ",";

			}//fin for

			document.fsaldoReg.gcta_saldo.value = "";
			var algo = document.fsaldoReg.gcta_saldo.value;
		    document.fsaldoReg.gcta_saldo.value = algo + contenido;
			document.fsaldoReg.gj.value = "0";
			var gg = parseInt(document.fsaldoReg.gj.value,10);
		    document.fsaldoReg.gj.value = gg + cuantas;
			j = cuantas;

//alert("a:"+document.fsaldoReg.gcta_saldo.value+",b:"+document.fsaldoReg.gj.value+",c:"+j);

			/*var gg = parseInt(document.fsaldoReg.gj.value,10);
		    j = gg;*/

		   if(j==0)
		   {
		     cuadroDialogo("Usted no ha seleccionado una cuenta.\n\nPor favor seleccione una cuenta \npara programar su consulta",3);
		     return false;
		   }
		   if (document.fsaldoReg.gfecha1.value == "")
			{
				cuadroDialogo("Favor de seleccionar una fecha",3);
				return false;
			}
			return true;
		}//fin change

		function isNumOrChar (InString)
		{
			 InString=InString.toLowerCase();
			 RefString="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_@.,";
			if (RefString.indexOf (InString, 0)==-1)
			{
				return ("no");
			}
			 return ("si");
		}//fin valida

		function isNumOrChar1 (InString)
		{
			 InString=InString.toLowerCase();
			 RefString="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
			if (RefString.indexOf (InString, 0)==-1)
			{
				return ("no");
			}
			 return ("si");
		}//fin valida

		 //-->

		function val_importe()
		{
		  var dato=document.fsaldoReg.gimporte.value;
		  var result=true;
		  if  (trimString(dato)!="")
		  {
		    result=validAmount(dato);
		    if (result==true)
			{
		  	  dato=formateaImporte(dato);
			  document.fsaldoReg.gimporte.value=quitar_ceros_izquierdos(dato);
		    }
		  }
		}//fin val

function VerificaComboPer(combo)
{
  var indice = document.fsaldoReg.gperiodo.selectedIndex;
  var dato = document.fsaldoReg.gperiodo.options[indice].value;
  if  (dato=="1" || dato=="3")
  {document.fsaldoReg.gdiasemana.options[0].selected=true;
   document.fsaldoReg.gdiasemana.value="1";}

  indice = document.fsaldoReg.gformato.selectedIndex;
  dato = document.fsaldoReg.gformato.options[indice].value;
  if  (dato=="1")
  {document.fsaldoReg.gperiodo.options[0].selected=true;
   document.fsaldoReg.gperiodo.value="1";
   combo.blur();}

  indice = document.fsaldoReg.greporte.selectedIndex;
  dato = document.fsaldoReg.greporte.options[indice].value;
  if  (dato=="2")
  {combo.blur();}
}

function VerificaComboDia(combo)
{
  var indice = document.fsaldoReg.gperiodo.selectedIndex;
  var dato = document.fsaldoReg.gperiodo.options[indice].value;
  if  (dato=="1" || dato=="3")
  {document.fsaldoReg.gdiasemana.options[0].selected=true;combo.blur();}

  indice = document.fsaldoReg.gformato.selectedIndex;
  dato = document.fsaldoReg.gformato.options[indice].value;
  indice = document.fsaldoReg.greporte.selectedIndex;
  var dato1 = document.fsaldoReg.greporte.options[indice].value;
  if (dato=="0" && dato1=="2")
	{
	document.fsaldoReg.gdiasemana.options[0].selected=true;
	document.fsaldoReg.gdiasemana.value = "1";
	combo.blur();
	}
	if (dato=="1")
	{
	document.fsaldoReg.gdiasemana.options[0].selected=true;
	document.fsaldoReg.gdiasemana.value = "1";
	combo.blur();
	}
}

function VerificaComboTipo(combo)
{
  var indice = document.fsaldoReg.gformato.selectedIndex;
  var dato = document.fsaldoReg.gformato.options[indice].value;
  if  (dato=="1")
  {document.fsaldoReg.gtipomov.options[0].selected=true;
   document.fsaldoReg.gtipomov.value="B";
   combo.blur();}
}

function VerificaComboOmi(combo)
{
  var indice = document.fsaldoReg.gformato.selectedIndex;
  var dato = document.fsaldoReg.gformato.options[indice].value;
  if  (dato=="1")
  {combo.blur();
   document.fsaldoReg.gChksvc.checked = false;
   document.fsaldoReg.gChksvc.disabled = true;
  }
       //Si el check salvo buen cobro esta activado se desactiva el vista
  if (document.fsaldoReg.gChksvc.checked == true)
  {
      document.fsaldoReg.gChkvista.checked=false;
      document.fsaldoReg.gChkvista.disabled=true;
  }
  else
  {
      document.fsaldoReg.gChkvista.checked=false;
      document.fsaldoReg.gChkvista.disabled=false;
  }
}

function VerificaComboVista(combo)
{
  var indice = document.fsaldoReg.gformato.selectedIndex;
  var dato = document.fsaldoReg.gformato.options[indice].value;
  if  (dato=="1")
  {combo.blur();
   document.fsaldoReg.gChkvista.checked = false;
   document.fsaldoReg.gChkvista.disabled = true;}
   if (document.fsaldoReg.gChkvista.checked == true)
  {
      document.fsaldoReg.gChksvc.checked =false;
      document.fsaldoReg.gChksvc.disabled=true;
  }
  else
  {
      document.fsaldoReg.gChksvc.checked =false;
      document.fsaldoReg.gChksvc.disabled=false;
  }
}

function VerificaComboRep(combo)
{
  var indice = document.fsaldoReg.gformato.selectedIndex;
  var dato = document.fsaldoReg.gformato.options[indice].value;
  if  (dato=="1"){
    document.fsaldoReg.greporte.options[1].selected=true;
    // se modifico 04/12/2003
    document.fsaldoReg.greporte.selectedIndex=0;
    //document.fsaldoReg.greporte.value = "2";
    // fin Modificacion.
    //LGN combo.blur();
  }

}

function VerificaComboFor(combo)
{
  var indice = document.fsaldoReg.gformato.selectedIndex;
  var dato = document.fsaldoReg.gformato.options[indice].value;

  //document.fsaldoReg.gemail.value=dato;
  if  (dato=="1")
  {document.fsaldoReg.gimporte.value = "";
   document.fsaldoReg.gChksvc.checked = false;
   document.fsaldoReg.greporte.value = "2";
   document.fsaldoReg.greporte.options[1].selected=true;
   document.fsaldoReg.greporte.blur();
   document.fsaldoReg.gtipomov.value="B";
   document.fsaldoReg.gtipomov.options[0].selected=true;
   document.fsaldoReg.gdiasemana.value="1";
   document.fsaldoReg.gdiasemana.options[0].selected=true;
   document.fsaldoReg.gperiodo.value="1";
   document.fsaldoReg.gperiodo.options[0].selected=true;
   document.fsaldoReg.gimporte.disabled = true;
   document.fsaldoReg.gChksvc.disabled = true;
  }
}

function VerificaComboCanal(combo)
{
  var indice = document.fsaldoReg.gcanal.selectedIndex;
  var dato = document.fsaldoReg.gcanal.options[indice].value;
  if  (dato=="2" || dato=="3")
  {document.fsaldoReg.gemail.value = "";
   document.fsaldoReg.gemail.disabled = true;}
}

function VerificaImporte(texto)
{
  var indice = document.fsaldoReg.gformato.selectedIndex;
  var dato = document.fsaldoReg.gformato.options[indice].value;
  if  (dato=="1")
  {document.fsaldoReg.gimporte.value = "";
   texto.blur();}
   if (document.fsaldoReg.greporte.options[document.fsaldoReg.greporte.selectedIndex].value=="2")
  {
      document.fsaldoReg.gimporte.value = "";
      texto.blur();
  }
}

function VerificaEmail(texto)
{
  var indice = document.fsaldoReg.gcanal.selectedIndex;
  var dato = document.fsaldoReg.gcanal.options[indice].value;
  if  (dato=="2" || dato=="3")
  {document.fsaldoReg.gemail.value = "";
   texto.blur();}
}

function VerificaFecha(texto)
{
   texto.blur();
}

		function val_periodo()
		{
		  var result=true;
		  var indice = document.fsaldoReg.gperiodo.selectedIndex;
		  var dato = document.fsaldoReg.gperiodo.options[indice].value;
		  if  (dato=="2")
		  {
			document.fsaldoReg.gdiasemana.disabled = false;
		  }
		  else
			{
			//modificado 04/12/2003
			//document.fsaldoReg.gdiasemana.value = "1";
			document.fsaldoReg.gdiasemana.selectedIndex=0;
			//Fin Modificacion
			document.fsaldoReg.gdiasemana.disabled = true;
			}

		  //valida el importe de Enlace
		  var indice = document.fsaldoReg.gformato.selectedIndex;
		  var dato = document.fsaldoReg.gformato.options[indice].value;
		  if  (dato=="1") //Norma 43
			{
			document.fsaldoReg.gimporte.value = "";
			document.fsaldoReg.gimporte.disabled = true;
			}
		  else //Enlace
			{
			document.fsaldoReg.gimporte.disabled = false;
			}

		  //valida el salvo buen cobro
		  var indice = document.fsaldoReg.gformato.selectedIndex;
		  var dato = document.fsaldoReg.gformato.options[indice].value;
		  if  (dato=="1") //Norma 43
			{
			document.fsaldoReg.gChksvc.checked = false;
			document.fsaldoReg.gChksvc.disabled = true;
			document.fsaldoReg.gChkvista.checked = false;
			document.fsaldoReg.gChkvista.disabled = true;
			}
		  else //Enlace
			{
			document.fsaldoReg.gChksvc.disabled = false;
			document.fsaldoReg.gChkvista.disabled = false;
			}

		  //valida el tipo de reporte
		  var indice = document.fsaldoReg.gformato.selectedIndex;
		  var dato = document.fsaldoReg.gformato.options[indice].value;
		  if  (dato=="1") //Norma 43
			{
			document.fsaldoReg.greporte.value = "1";      //LGN
			document.fsaldoReg.greporte.disabled = false; //LGN
			}
		  else //Enlace
			{
			document.fsaldoReg.greporte.value = "1";
			document.fsaldoReg.greporte.disabled = false;
			}

		  //valida si es email
		  var indice = document.fsaldoReg.gcanal.selectedIndex;
		  var dato = document.fsaldoReg.gcanal.options[indice].value;
		  if  (dato=="1") //email
			{
			document.fsaldoReg.gemail.disabled = false;
			}
		  else //buzon o enlace
			{
			document.fsaldoReg.gemail.value = "";
			document.fsaldoReg.gemail.disabled = true;
			}

			//valida el formato norma 43
			 if (document.fsaldoReg.greporte1.value=="1")
			{
			  document.fsaldoReg.greporte.options[0].selected=true;
			  document.fsaldoReg.greporte.value="1";
			}
			 if (document.fsaldoReg.greporte1.value=="2")
			{
			  document.fsaldoReg.greporte.options[1].selected=true;
			  document.fsaldoReg.greporte.value="2";
			}
			var indice = document.fsaldoReg.greporte.selectedIndex;
		    var dato = document.fsaldoReg.greporte.options[indice].value;
			if (dato=="2")
			{
			document.fsaldoReg.gdiasemana.disabled = true;
			document.fsaldoReg.gdiasemana.value = "1";
			document.fsaldoReg.gperiodo.disabled = true;
			document.fsaldoReg.gperiodo.value = "1";
			}
			else
			{
			document.fsaldoReg.gperiodo.disabled = false;
			//document.fsaldoReg.gperiodo.value = "1";
			}

			//valida el reporte enlace consolidado
			var indice = document.fsaldoReg.gformato.selectedIndex;
		    var dato = document.fsaldoReg.gformato.options[indice].value;
			var indice = document.fsaldoReg.greporte.selectedIndex;
		    var dato1 = document.fsaldoReg.greporte.options[indice].value;
			if (dato=="0" && dato1=="2")
			{
			document.fsaldoReg.gdiasemana.disabled = true;
			document.fsaldoReg.gdiasemana.value = "1";
			document.fsaldoReg.gperiodo.disabled = true;
			document.fsaldoReg.gperiodo.value = "1";
			}
		}//fin val

		function val_periodo1()
		{
		  dato = document.fsaldoReg.actualiza_valor.value;
		  if (dato=="5")
			{
			  // valida los valores de las variables de entrada
			  document.fsaldoReg.gperiodo.value = document.fsaldoReg.gperiodo1.value;
			  if (document.fsaldoReg.gperiodo1.value=="1")
				{
				  document.fsaldoReg.gperiodo.options[0].selected=true;
				  document.fsaldoReg.gperiodo.value="1";
				}
			  if (document.fsaldoReg.gperiodo1.value=="2")
				{
				  document.fsaldoReg.gperiodo.options[1].selected=true;
				  document.fsaldoReg.gperiodo.value="2";
				}
			 if (document.fsaldoReg.gperiodo1.value=="3")
				{
				  document.fsaldoReg.gperiodo.options[2].selected=true;
				  document.fsaldoReg.gperiodo.value="3";
				}
				document.fsaldoReg.gperiodo.focus;

			  document.fsaldoReg.ghorario.value = document.fsaldoReg.ghorario1.value;
	  		  if (document.fsaldoReg.ghorario1.value=="08:00")
				{ document.fsaldoReg.ghorario.options[0].selected=true;
				  document.fsaldoReg.ghorario.value="08:00";}
			  if (document.fsaldoReg.ghorario1.value=="09:00")
				{ document.fsaldoReg.ghorario.options[1].selected=true;
				  document.fsaldoReg.ghorario.value="09:00";}
			  if (document.fsaldoReg.ghorario1.value=="10:00")
				{ document.fsaldoReg.ghorario.options[2].selected=true;
				  document.fsaldoReg.ghorario.value="10:00";}
			  if (document.fsaldoReg.ghorario1.value=="11:00")
				{ document.fsaldoReg.ghorario.options[3].selected=true;
				  document.fsaldoReg.ghorario.value="11:00";}
			  if (document.fsaldoReg.ghorario1.value=="12:00")
				{ document.fsaldoReg.ghorario.options[4].selected=true;
				  document.fsaldoReg.ghorario.value="12:00";}
			  if (document.fsaldoReg.ghorario1.value=="13:00")
				{ document.fsaldoReg.ghorario.options[5].selected=true;
				  document.fsaldoReg.ghorario.value="13:00";}
			  if (document.fsaldoReg.ghorario1.value=="14:00")
				{ document.fsaldoReg.ghorario.options[6].selected=true;
				  document.fsaldoReg.ghorario.value="14:00";}
			  if (document.fsaldoReg.ghorario1.value=="15:00")
				{ document.fsaldoReg.ghorario.options[7].selected=true;
				  document.fsaldoReg.ghorario.value="15:00";}
			  if (document.fsaldoReg.ghorario1.value=="16:00")
				{ document.fsaldoReg.ghorario.options[8].selected=true;
				  document.fsaldoReg.ghorario.value="16:00";}
			  if (document.fsaldoReg.ghorario1.value=="17:00")
				{ document.fsaldoReg.ghorario.options[9].selected=true;
				  document.fsaldoReg.ghorario.value="17:00";}
			  if (document.fsaldoReg.ghorario1.value=="18:00")
				{ document.fsaldoReg.ghorario.options[10].selected=true;
				  document.fsaldoReg.ghorario.value="18:00";}
			  if (document.fsaldoReg.ghorario1.value=="19:00")
				{ document.fsaldoReg.ghorario.options[11].selected=true;
				  document.fsaldoReg.ghorario.value="19:00";}
			  if (document.fsaldoReg.ghorario1.value=="20:00")
				{ document.fsaldoReg.ghorario.options[12].selected=true;
				  document.fsaldoReg.ghorario.value="20:00";}
			  document.fsaldoReg.ghorario.focus;

			  document.fsaldoReg.gtipomov.value = document.fsaldoReg.gtipomov1.value;
			  if (document.fsaldoReg.gtipomov1.value=="B")
				{
				  document.fsaldoReg.gtipomov.options[0].selected=true;
				  document.fsaldoReg.gtipomov.value="B";
				}
			  if (document.fsaldoReg.gtipomov1.value=="C")
				{
				  document.fsaldoReg.gtipomov.options[1].selected=true;
				  document.fsaldoReg.gtipomov.value="C";
				}
			 if (document.fsaldoReg.gtipomov1.value=="A")
				{
				  document.fsaldoReg.gtipomov.options[2].selected=true;
				  document.fsaldoReg.gtipomov.value="A";
				}
			  document.fsaldoReg.gtipomov.focus;

			  dato = document.fsaldoReg.gChksvc1.value;
			  if  (dato=="0")
			  {
				document.fsaldoReg.gChksvc.checked = true;
			  }
			  else
			  {
				document.fsaldoReg.gChksvc.checked = false;
			  }
			  document.fsaldoReg.gChksvc.focus;

			  dato = document.fsaldoReg.gChkvista1.value;
			  if  (dato=="0")
			  {
				document.fsaldoReg.gChkvista.checked = true;
			  }
			  else
			  {
				document.fsaldoReg.gChkvista.checked = false;
			  }
			  document.fsaldoReg.gChkvista.focus;

			  dato = document.fsaldoReg.gimporte.value;
			  if  (dato=="_")
			  {
				document.fsaldoReg.gimporte.value = "";
			  }
			  else
			  {
				document.fsaldoReg.gimporte.value = document.fsaldoReg.gimporte1.value;
			  }
			  document.fsaldoReg.gimporte.focus;

			  document.fsaldoReg.gfecha1.value = document.fsaldoReg.gfecha2.value;
			  document.fsaldoReg.gformato.value = document.fsaldoReg.gformato1.value;
			  if (document.fsaldoReg.gformato1.value == "1")
			  {
				  document.fsaldoReg.gformato.options[1].selected=true;
				  document.fsaldoReg.gformato.value="1";
			  }
			  else
			  {
				  document.fsaldoReg.gformato.options[0].selected=true;
				  document.fsaldoReg.gformato.value="0";
			  }
			  document.fsaldoReg.gformato.focus;

			  document.fsaldoReg.greporte.value = document.fsaldoReg.greporte1.value;
	  		  if (document.fsaldoReg.greporte1.value=="1")
				{
				  document.fsaldoReg.greporte.options[0].selected=true;
				  document.fsaldoReg.greporte.value="1";
				}
			  if (document.fsaldoReg.greporte1.value=="2")
				{
				  document.fsaldoReg.greporte.options[1].selected=true;
				  document.fsaldoReg.greporte.value="2";
				}
				document.fsaldoReg.greporte.focus;

			  document.fsaldoReg.gcanal.value = document.fsaldoReg.gcanal1.value;
	  		  if (document.fsaldoReg.gcanal1.value=="0")
				{
				  document.fsaldoReg.gcanal.options[0].selected=true;
				  document.fsaldoReg.gcanal.value="2";
				}
			  if (document.fsaldoReg.gcanal1.value=="1")
				{
				  document.fsaldoReg.gcanal.options[1].selected=true;
				  document.fsaldoReg.gcanal.value="1";
				}
			   if (document.fsaldoReg.gcanal1.value=="3")
				{
				  document.fsaldoReg.gcanal.options[2].selected=true;
				  document.fsaldoReg.gcanal.value="3";
				}
			  document.fsaldoReg.gcanal.focus;

			  document.fsaldoReg.gdiasemana.value = document.fsaldoReg.gdiasemana1.value;
	  		  if (document.fsaldoReg.gdiasemana1.value=="1")
				{
				  document.fsaldoReg.gdiasemana.options[0].selected=true;
				  document.fsaldoReg.gdiasemana.value="1";
				}
			  if (document.fsaldoReg.gdiasemana1.value=="2")
				{
				  document.fsaldoReg.gdiasemana.options[1].selected=true;
				  document.fsaldoReg.gdiasemana.value="2";
				}
			   if (document.fsaldoReg.gdiasemana1.value=="3")
				{
				  document.fsaldoReg.gdiasemana.options[2].selected=true;
				  document.fsaldoReg.gdiasemana.value="3";
				}
			   if (document.fsaldoReg.gdiasemana1.value=="4")
				{
				  document.fsaldoReg.gdiasemana.options[3].selected=true;
				  document.fsaldoReg.gdiasemana.value="4";
				}
			   if (document.fsaldoReg.gdiasemana1.value=="5")
				{
				  document.fsaldoReg.gdiasemana.options[4].selected=true;
				  document.fsaldoReg.gdiasemana.value="5";
				}
			  document.fsaldoReg.gdiasemana.focus;

			  dato = document.fsaldoReg.gemail1.value;
			  if  (dato=="_")
			  {
				document.fsaldoReg.gemail.value = "";
			  }
			  else
			  {
				document.fsaldoReg.gemail.value = document.fsaldoReg.gemail1.value;
			  }
			  document.fsaldoReg.gemail.focus;
			  document.fsaldoReg.gfecha1.disabled = true;
	 		  document.fsaldoReg.ghorario.focus;

			}//fin 5
			else
			{
				document.fsaldoReg.gfecha1.value = document.fsaldoReg.gfecha2.value;
			}
			document.fsaldoReg.gfecha.value = document.fsaldoReg.gfecha1.value;
			document.fsaldoReg.gfecha1.disabled = true;
		}//fin val1

		function val_periodo2()
		{
			Error = "<%= request.getAttribute("codigoEE") %>";
			Clave = "<%= request.getAttribute("codigoXX") %>";
			if  (Clave=="5")
		    {
			 	 cuadroDialogo(Error ,6);
			}
			if  (Clave=="6")
		    {
			 	 cuadroDialogo(Error ,6);
			}
		}//fin val2

		function val_formato()
		{
		  var dato=document.fsaldoReg.gformato.options[document.fsaldoReg.gformato.selectedIndex].value;
		  var result=true;

		  if  (dato=="1") //Norma 43
			{
			//Selecciona el primer dia de la semana
			document.fsaldoReg.gdiasemana.selectedIndex=0;
			document.fsaldoReg.gdiasemana.disabled = true;
			//Selecciona la periodicidad
			document.fsaldoReg.gperiodo.selectedIndex=0;
			document.fsaldoReg.gperiodo.disabled = true;
			//Selecciona Tipo movimiento = todos
			document.fsaldoReg.gtipomov.disabled = true;
			document.fsaldoReg.gtipomov.selectedIndex=0;
			//Elimina el valor del importe y lo deshabilita
			document.fsaldoReg.gimporte.value = "";
			document.fsaldoReg.gimporte.disabled = true;
			//Selecciona el Reporte Consolidado
			document.fsaldoReg.greporte.selectedIndex = 0; // LGN
			document.fsaldoReg.greporte.disabled = false;  // LGN
			//Quita la seleccion de los chekbox Omitir Salvo Buen Cobro y Vista y los deshabilita
			document.fsaldoReg.gChksvc.checked = false;
			document.fsaldoReg.gChksvc.disabled = true;
			document.fsaldoReg.gChkvista.checked = false;
			document.fsaldoReg.gChkvista.disabled = true;
			}
		  else //Enlace
			{
			document.fsaldoReg.gdiasemana.disabled = true;
			document.fsaldoReg.gdiasemana.value = "1";
			document.fsaldoReg.gperiodo.disabled = false;
			document.fsaldoReg.gperiodo.value = "1";
			document.fsaldoReg.gtipomov.disabled = false;
			document.fsaldoReg.gtipomov.value = "B";
			document.fsaldoReg.gimporte.disabled = false;
			document.fsaldoReg.gChksvc.checked = false;
			document.fsaldoReg.gChksvc.disabled = false;
			document.fsaldoReg.greporte.value = "1";
			document.fsaldoReg.greporte.disabled = false;
			document.fsaldoReg.gChkvista.checked = false;
			document.fsaldoReg.gChkvista.disabled = false;
			}

			var dato1=document.fsaldoReg.greporte.value;
			if (dato=="0" && dato1=="2")
			{
			document.fsaldoReg.gdiasemana.disabled = true;
			document.fsaldoReg.gdiasemana.value = "1";
			document.fsaldoReg.gperiodo.disabled = true;
			document.fsaldoReg.gperiodo.value = "1";
			}
		}//fin val

		function val_reporte()
		{
		        var dato=document.fsaldoReg.gformato.options[document.fsaldoReg.gformato.selectedIndex].value;
			var dato1=document.fsaldoReg.greporte.options[document.fsaldoReg.greporte.selectedIndex].value;
			if (dato=="0" && dato1=="2")
			{
			document.fsaldoReg.gperiodo.selectedIndex=0;
			document.fsaldoReg.gperiodo.disabled = true;
			//document.fsaldoReg.gperiodo.value = "1";
			document.fsaldoReg.gdiasemana.selectedIndex=0;
			document.fsaldoReg.gdiasemana.disabled = true;
			//document.fsaldoReg.gdiasemana.value = "1";
			}
			if (dato1!="2")
			{
			document.fsaldoReg.gperiodo.selectedIndex=0;
			document.fsaldoReg.gperiodo.disabled = false;
			//document.fsaldoReg.gperiodo.value = "1";
			}
			if (dato1 =="2")
			{
			  document.fsaldoReg.gimporte.value="";
			  document.fsaldoReg.gimporte.disabled = true;

			}
			if (dato1 =="1")
			{
			  document.fsaldoReg.gimporte.disabled= false;
			}
		}//fin val_reporte

		function val_entrega()
		{
		  var dato=document.fsaldoReg.gcanal.value;
		  var result=true;
		  if  (dato=="1") //email
			{
			document.fsaldoReg.gemail.disabled = false;
			}
		  else //buzon o enlace
			{
			document.fsaldoReg.gemail.value = "";
			document.fsaldoReg.gemail.disabled = true;
			}
		}//fin val

		function trimString (str)
		{
		    str = this != window? this : str;
			return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
		}//fin trim

		function validAmount (cantidad)
		{

		  strAux1 = "";
		  strAux2 = "";
		  entero = "";
		  cantidadAux = cantidad;
		  var miles="";

		  if (cantidadAux == "" || cantidadAux <= 0)
		  {
		    document.fsaldoReg.gimporte.value="";
		    cuadroDialogo("Favor de introducir un IMPORTE v&#225;lido a consultar ",3);
		    return false;
		  }
		  else
		  {
		    if (isNaN(cantidadAux))
		    {
			  document.fsaldoReg.gimporte.value="";
		      cuadroDialogo("Favor de ingresar un valor num\351rico en IMPORTE",3);
		      return false;
		    }

		    pos_punto = cantidadAux.indexOf (".");

		    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length);

		    if (pos_punto == 0)
		      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length);
		    else
		    {
		      if (pos_punto != -1)     //-- si se teclearon los centavos
		      {
		        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length);
		        entero = cantidadAux.substring (0, pos_punto);
		      }
		      else
		      {
		        cents = "00";
		        entero = cantidadAux;
		      }

		      pos_coma = entero.indexOf (",");
		      if (pos_coma != -1)     //-- si son mas de mil
		      {
		        cientos = entero.substring (entero.length - 3, entero.length);
		        miles = entero.substring (0, entero.length - 3);
		      }
		      else
		      {
		        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
		        {
		          cientos = entero.substring (entero.length - 3, entero.length);
		          miles = entero.substring (0, entero.length - 3);
		        }
		        else
		        {
		          if (entero.length == 0)
		            cientos = "";
		          else
		            cientos = entero;
		          miles = "";
		        }
		      }

		      if (miles != "")
		        strAux1 = miles;
		      if (cientos != "")
		        strAux1 = strAux1 + cientos + ".";
		      strAux1 = strAux1 + cents;

		      if (miles != "")
		        strAux2 = miles;
		      if (cientos != "")
		        strAux2 = strAux2 + cientos + ".";
		      strAux2 = strAux2 + cents;

		      transf = document.fsaldoReg.gimporte.value;
		    }
		    document.fsaldoReg.gimporte.value = strAux1;

		    strAux1=Formatea_Importe(strAux1);
		    if (miles != "")
		      document.fsaldoReg.gimportestring.value = strAux2;
		    else
			  document.fsaldoReg.gimportestring.value = strAux1;
		    return true;
		  }
		} //fin amount

		function Formatea_Importe(importe)
		{
		   decenas="";
		   centenas="";
		   millon="";
		   millares="";
		   importe_final=importe;
		   var posiciones=7;

		   posi_importe=importe.indexOf(".");
		   num_decimales=importe.substring(posi_importe + 1,importe.length);

		   if (posi_importe==-1)
		     importe_final= importe + ".00";

		   if (posi_importe==4)
		   {
		     centenas=importe.substring(1, posiciones);
		     millares=importe.substring(0,1);
		     importe_final= millares + "," + centenas;
		   }
		   if (posi_importe==5)
		   {
		     centenas=importe.substring(2, posiciones + 1);
		     millares=importe.substring(0,2);
		     importe_final= millares + "," + centenas;
		   }
		   if (posi_importe==6)
		   {
		     centenas=importe.substring(3, posiciones + 2);
		     millares=importe.substring(0,3);
		     importe_final= millares + "," + centenas;
		   }
		   if (posi_importe==7)
		   {
		     centenas=importe.substring(4, posiciones + 3);
		     millares=importe.substring(1,4);
		     millon=importe.substring(0,1)
		     importe_final= millon + "," + millares + "," + centenas;
		   }
		   if (posi_importe==8)
		   {
		     centenas=importe.substring(5, posiciones + 4);
		     millares=importe.substring(2,5);
		     millon=importe.substring(0,2)
		     importe_final= millon + "," + millares + "," + centenas;
		   }
		   if (posi_importe==9)
		   {
		     centenas=importe.substring(6, posiciones + 5);
		     millares=importe.substring(3,6);
		     millon=importe.substring(0,3)
		     importe_final= millon + "," + millares + "," + centenas;
		   }
		   return importe_final
		}//fin formatea

		function formateaImporte(valor){
		        var cadena="";
		        var cadenaDecimales="";
		                num= parseFloat(valor);
				        cadena=cadena+num;
		                posicionPunto=cadena.indexOf('.');
		                if(posicionPunto>=0){
		                        cadenaDecimales=cadena.substring(posicionPunto+1);
		                        if(cadenaDecimales.length==1)
		                                cadena=cadena+"0";
		                        else if(cadenaDecimales.length>2)
		                                cadena=cadena.substring(0, posicionPunto+3);
		                }
		                else
		                        cadena= cadena+".00";
		        return cadena;
		}//fin formatea 1

		function quitar_ceros_izquierdos(dato)
		{
			var EsteCaracter;
			var contador = 0;
			var sinceros ="";
			for (var i=0; i < dato.length; i++) {
				EsteCaracter = dato.substring(i , i+1);
				if (EsteCaracter=="0")
					contador ++;
				else
					break;
			}
			sinceros=dato.substring(contador,dato.length);
			return sinceros;
		}// fin quitar

		function MoveValues(x,moveAll)
		{
			/*var isNav4 = 0;
			var isIE4 = 0;

			// Browser check.
			if (parseInt(navigator.appVersion.charAt(0)) >= 4)
		    {
				if(navigator.appName == "Netscape")
				{isNav4 = isNav4 + 1;}
		        if(navigator.appName.indexOf("Microsoft") != -1)
				{isIE4 = isIE4 + 1;
			}*///fin if

			if(x==0)
			{
				ddFrom = document.fsaldoReg.CuentasS;
				ddTo = document.fsaldoReg.CuentasC;
			}
			else
			{
				ddFrom = document.fsaldoReg.CuentasC;
				ddTo = document.fsaldoReg.CuentasS;
			}//fin if

			for(idx=0;idx<ddFrom.options.length;idx++)
			{
				if(moveAll || ddFrom.options[idx].selected)
				{
					wrkVal = ddFrom.options[idx].value;
					wrkText = ddFrom.options[idx].text;
					var objOption = document.createElement("OPTION");
					//var objOption = new Option(wrkText,wrkVal,false,true);
					ddTo.options.add(objOption);
					//ddTo.options[wrkVal] = objOption;
					ddTo.options[ddTo.options.length-1].value = wrkVal;
					ddTo.options[ddTo.options.length-1].text = wrkText;
					ddFrom.options.remove(idx);
					idx--;
				}//fin if

			}//fin for
		}//fin move

function Desemtrama(Parametro, NumPipe)
 {
  var cadena = "";
  var caracter = "";
  var Pipe = 1;
  for (var i = 0; i <= Parametro.length; i++)
   {
    caracter = Parametro.substring( i, i + 1 );

    if (Pipe == NumPipe && caracter != '|')
     cadena = cadena + caracter;
    else
     if (caracter == '|')
       Pipe++;

     if (Pipe > NumPipe)
      break;
   }
   return(cadena);
 }//fin desen

// Le suma el elemento seleccionado a la lista Seleccionadas
function SumaSucursal()
 {
	  if (eval(document.fsaldoReg.CuentasC.selectedIndex) > 0)
	  {
	    var Lugar = document.fsaldoReg.CuentasC.selectedIndex;
		var seleccion = document.fsaldoReg.CuentasC.options[Lugar].text;
		var seleccionVal = document.fsaldoReg.CuentasC.options[Lugar].value;
	    var OpcionLista = document.fsaldoReg.CuentasS.options[0].value;
		var posicionsum = eval(document.fsaldoReg.CuentasC.length);
	    var posicionres = eval(document.fsaldoReg.CuentasS.length);

	    var sucursalVal
		var sucursal
		if (seleccion != "" && seleccion != "*TODAS")
	     {
		  // ------------------Suma una sucursal a la lista 2 -----------------------------
	      var ListObj = document.fsaldoReg.CuentasS
	      var ListaOpcion = new Array();
		  ListaOpcion[0] = Desemtrama(seleccion,1);
	        ListObj.length = posicionres + 1;
		    ListObj.options[posicionres] = new Option(ListaOpcion[0]);
	        ListObj.options[posicionres].value = seleccionVal;
			ListObj.options[posicionres].text = seleccion;
		  // -------------------------- Refresca la lista ---------------------------------
	      var SumListObj = document.fsaldoReg.CuentasC
	      var SumListaOpcion = new Array();
	      var SumListaValue  = new Array();
		  var SumListaText = new Array();
	      ii = 0;
	      // ------------------- Guarda los elementos en un arreglo --------------------
	      for (i=0; i < posicionsum; i++)
		   {
	        sucursalVal = document.fsaldoReg.CuentasC.options[i].value;
			sucursal = document.fsaldoReg.CuentasC.options[i].text;
			if (seleccionVal != sucursalVal)
	         {
		      // SumListaOpcion[ii] = sucursal;
			  SumListaOpcion[ii] = Desemtrama(sucursal,1);
	           SumListaValue[ii] = sucursalVal;
			   SumListaText[ii] = sucursal;
			  ii++;
	        }//fin if
	       }//fin for
	      // ------------------- Limpia la lista 1 -------------------------------------
	      SumListObj.length = 0;
	      // ------------------- Vuelve a llenar la lista 1 ----------------------------
	      if (ii == 0)
		   {
	        SumListObj.length = ii;
			 //alert("Entro al ultimo");
	     //   SumListObj.options[ii] = new Option(SumListaOpcion[ii]);
		  //  SumListObj.options[ii].value = "";
	       }
	      else
	       {
	        for (i=0; i < ii; i++)
		     {
	          SumListObj.length = i;
		      SumListObj.options[i] = new Option(SumListaOpcion[i]);
			  SumListObj.options[i].value = SumListaValue[i];
			  SumListObj.options[i].text = SumListaText[i];
		     }//fin for
	       }//fin else
	     }//fin if todas
	   }//fin > 0
	  else
	  {
	         //Modificado 04/12/2003
		 //cuadroDialogo("Seleccione alg&uacute;n elemento de la lista y diferente al primer elemento.",3);
		 cuadroDialogo("Seleccione alguna Cuenta.",3);
		 //Fin de la Modificacion.
	  }//fin if 0

 }//fin suma

 // Le suma todos los elementos lista Seleccionadas
 function SumaSucursalN()
 {
 document.fsaldoReg.Lado.value="1";
 document.fsaldoReg.action="csaldo1?prog=1&prog1=1";
 document.fsaldoReg.submit();
 }
function SumaSucursalT()
 {
	 var posicionsum1 = eval(document.fsaldoReg.CuentasC.length);
	 for (cuenta=1; cuenta < posicionsum1; cuenta++)
	 {
		var seleccion = document.fsaldoReg.CuentasC.options[1].text;
		var seleccionVal = document.fsaldoReg.CuentasC.options[1].value;
	    var OpcionLista = document.fsaldoReg.CuentasS.options[0].value;
		var posicionsum = eval(document.fsaldoReg.CuentasC.length);
	    var posicionres = eval(document.fsaldoReg.CuentasS.length);

	    var sucursalVal
		var sucursal
		if (seleccion != "" && seleccion != "*TODAS")
	     {
		  // ------------------Suma una sucursal a la lista 2 -----------------------------
	      var ListObj = document.fsaldoReg.CuentasS
	      var ListaOpcion = new Array();
		  ListaOpcion[0] = Desemtrama(seleccion,1);
	        ListObj.length = posicionres + 1;
		    ListObj.options[posicionres] = new Option(ListaOpcion[0]);
	        ListObj.options[posicionres].value = seleccionVal;
			ListObj.options[posicionres].text = seleccion;
		  // -------------------------- Refresca la lista ---------------------------------
	      var SumListObj = document.fsaldoReg.CuentasC
	      var SumListaOpcion = new Array();
	      var SumListaValue  = new Array();
		  var SumListaText = new Array();
	      ii = 0;
	      // ------------------- Guarda los elementos en un arreglo --------------------
	      for (i=0; i < posicionsum; i++)
		   {
	        sucursalVal = document.fsaldoReg.CuentasC.options[i].value;
			sucursal = document.fsaldoReg.CuentasC.options[i].text;
			if (seleccionVal != sucursalVal)
	         {
		      // SumListaOpcion[ii] = sucursal;
			  SumListaOpcion[ii] = Desemtrama(sucursal,1);
	           SumListaValue[ii] = sucursalVal;
			   SumListaText[ii] = sucursal;
			  ii++;
	        }//fin if
	       }//fin for
	      // ------------------- Limpia la lista 1 -------------------------------------
	      SumListObj.length = 0;
	      // ------------------- Vuelve a llenar la lista 1 ----------------------------
	      if (ii == 0)
		   {
	        SumListObj.length = ii;
			 //alert("Entro al ultimo");
	     //   SumListObj.options[ii] = new Option(SumListaOpcion[ii]);
		  //  SumListObj.options[ii].value = "";
	       }
	      else
	       {
	        for (i=0; i < ii; i++)
		     {
	          SumListObj.length = i;
		      SumListObj.options[i] = new Option(SumListaOpcion[i]);
			  SumListObj.options[i].value = SumListaValue[i];
			  SumListObj.options[i].text = SumListaText[i];
		     }//fin for
	       }//fin else
	     }//fin if todas
	   }//fin for todas

 }//fin suma todas

// Le resta el elemento seleccionado a la Lista de seleccionadas
function RestaSucursal()
 {
  if (eval(document.fsaldoReg.CuentasS.selectedIndex) > 0 )
   {
    var Lugar = document.fsaldoReg.CuentasS.selectedIndex;
    var seleccionVal = document.fsaldoReg.CuentasS.options[Lugar].value;
	var seleccion = document.fsaldoReg.CuentasS.options[Lugar].text;
    var OpcionLista = document.fsaldoReg.CuentasC.options[0].value;
    var posicionsum = eval(document.fsaldoReg.CuentasS.length);
    var posicionres = eval(document.fsaldoReg.CuentasC.length);

	var sucursal
	var sucursalVal
    if (seleccion != "" && seleccion != "*TODAS")
     {
      // ------------------Suma una sucursal a la lista 2 -----------------------------
      var ListObj = document.fsaldoReg.CuentasC
      var ListaOpcion = new Array();
      ListaOpcion[0] = Desemtrama(seleccion,1);

        ListObj.length = posicionres + 1;
        ListObj.options[posicionres] = new Option(ListaOpcion[0]);
        ListObj.options[posicionres].value = seleccionVal;
		ListObj.options[posicionres].text = seleccion;
      // -------------------------- Refresca la lista ---------------------------------
      var SumListObj = document.fsaldoReg.CuentasS
      var SumListaOpcion = new Array();
      var SumListaValue = new Array();
	   var SumListaText = new Array();
      ii = 0;
      // ------------------- Guarda los elementos en un arreglo --------------------
      for (i=0; i < posicionsum; i++)
       {
	   	sucursalVal = document.fsaldoReg.CuentasS.options[i].value;
		sucursal = document.fsaldoReg.CuentasS.options[i].text;
        if (seleccionVal != sucursalVal)
         {
          //SumListaOpcion[ii] = sucursal;
          SumListaOpcion[ii] = Desemtrama(sucursal,1);

			 SumListaValue[ii] = sucursalVal;
		   SumListaText[ii] = sucursal;

          ii++;
         }
       }
      // ------------------- Limpia la lista 1 -------------------------------------
      SumListObj.length = 0;
      // ------------------- Vuelve a llenar la lista 1 ----------------------------
      if (ii == 0)
       {
        SumListObj.length = ii;
       }
      else
       {
        for (i=0; i < ii; i++)
         {
          SumListObj.length = i;
          SumListObj.options[i] = new Option(SumListaOpcion[i]);

		   SumListObj.options[i].value = SumListaValue[i];
		  SumListObj.options[i].text = SumListaText[i];
         }
       }
     }
   }
  else
  {
   //Modificado 4/12/2003
   //cuadroDialogo("Seleccione alg&uacute;n elemento de la lista y diferente al primer elemento.",3);
     cuadroDialogo("Seleccione alguna Cuenta.",3);
   //Fin de la Modificacion.
  }
 }//fin resta

// Le resta todos los elementos seleccionados a la Lista de seleccionadas
 function  RestaSucursalN()
 {
 document.fsaldoReg.Lado.value="0";
 document.fsaldoReg.action="csaldo1?prog=1&prog1=1";
 document.fsaldoReg.submit();
 }

function RestaSucursalT()
 {
   var posicionsum1 = eval(document.fsaldoReg.CuentasS.length);
   for (cuenta=1; cuenta < posicionsum1; cuenta++)
   {
    var seleccionVal = document.fsaldoReg.CuentasS.options[1].value;
	var seleccion = document.fsaldoReg.CuentasS.options[1].text;
    var OpcionLista = document.fsaldoReg.CuentasC.options[0].value;
    var posicionsum = eval(document.fsaldoReg.CuentasS.length);
    var posicionres = eval(document.fsaldoReg.CuentasC.length);

	var sucursal
	var sucursalVal
    if (seleccion != "" && seleccion != "*TODAS")
     {
      // ------------------Suma una sucursal a la lista 2 -----------------------------
      var ListObj = document.fsaldoReg.CuentasC
      var ListaOpcion = new Array();
      ListaOpcion[0] = Desemtrama(seleccion,1);

        ListObj.length = posicionres + 1;
        ListObj.options[posicionres] = new Option(ListaOpcion[0]);
        ListObj.options[posicionres].value = seleccionVal;
		ListObj.options[posicionres].text = seleccion;
      // -------------------------- Refresca la lista ---------------------------------
      var SumListObj = document.fsaldoReg.CuentasS
      var SumListaOpcion = new Array();
      var SumListaValue = new Array();
	   var SumListaText = new Array();
      ii = 0;
      // ------------------- Guarda los elementos en un arreglo --------------------
      for (i=0; i < posicionsum; i++)
       {
	   	sucursalVal = document.fsaldoReg.CuentasS.options[i].value;
		sucursal = document.fsaldoReg.CuentasS.options[i].text;
        if (seleccionVal != sucursalVal)
         {
          //SumListaOpcion[ii] = sucursal;
          SumListaOpcion[ii] = Desemtrama(sucursal,1);

			 SumListaValue[ii] = sucursalVal;
		   SumListaText[ii] = sucursal;

          ii++;
         }
       }
      // ------------------- Limpia la lista 1 -------------------------------------
      SumListObj.length = 0;
      // ------------------- Vuelve a llenar la lista 1 ----------------------------
      if (ii == 0)
       {
        SumListObj.length = ii;
       }
      else
       {
        for (i=0; i < ii; i++)
         {
          SumListObj.length = i;
          SumListObj.options[i] = new Option(SumListaOpcion[i]);

		   SumListObj.options[i].value = SumListaValue[i];
		  SumListObj.options[i].text = SumListaText[i];
         }
       }
     }
   }//fin for

 }//fin resta todos

		 </script>

		 <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
	</head>

	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
			bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');val_periodo1();val_periodo();val_periodo2();"
			background="/gifs/EnlaceMig/gfo25010.gif">
		<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
			<tr valign="top">
				<td width="*">
					<!-- MENU PRINCIPAL -->
					<%= request.getAttribute("MenuPrincipal") %>
				</TD>
			</TR>
		</TABLE>
		<%= request.getAttribute("Encabezado") %>

		<p>


		<p>

		<table width="760" border="0" cellspacing="0" cellpadding="0">
			<form name="fsaldoReg" METHOD="POST" onSubmit="return cuentachange()" ACTION="csaldo1?prog=2">
			<INPUT TYPE="hidden" NAME="gcta_saldo" value="">
			<INPUT TYPE="hidden" NAME="gj" value="0">
			<INPUT TYPE="hidden" NAME="gjj">
			<INPUT TYPE="HIDDEN" NAME="gtotal"   VALUE=<%= request.getAttribute( "total" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gprev"   VALUE=<%= request.getAttribute( "prev" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gnext"   VALUE=<%= request.getAttribute( "next" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gfecha_programada" VALUE=0>
			<INPUT TYPE="HIDDEN" NAME="gimportestring" VALUE=0>
			<INPUT TYPE="HIDDEN" NAME="gfecha" value="">

			<!-- VARIABLES DE ENTRADA DE MODIFICACION -->
			<INPUT TYPE="HIDDEN" NAME="gperiodo1"   VALUE=<%= request.getAttribute( "gperiodo" ) %>>
			<INPUT TYPE="HIDDEN" NAME="ghorario1"   VALUE=<%= request.getAttribute( "ghorario" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gtipomov1"   VALUE=<%= request.getAttribute( "gtipomov" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gChksvc1"   VALUE=<%= request.getAttribute( "gChksvc" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gChkvista1"   VALUE=<%= request.getAttribute( "gChkvista" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gimporte1"   VALUE=<%= request.getAttribute( "gimporte" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gfecha2"   VALUE=<%= request.getAttribute( "gfecha1" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gformato1"   VALUE=<%= request.getAttribute( "gformato" ) %>>
			<INPUT TYPE="HIDDEN" NAME="greporte1"   VALUE=<%= request.getAttribute( "greporte" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gcanal1"   VALUE=<%= request.getAttribute( "gcanal" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gdiasemana1"   VALUE=<%= request.getAttribute( "gdiasemana" ) %>>
			<INPUT TYPE="HIDDEN" NAME="gemail1"   VALUE=<%= request.getAttribute( "gemails" ) %>>
			<INPUT TYPE="HIDDEN" NAME="actualiza_valor"   VALUE=<%= request.getAttribute( "actualiza_valor" ) %>>
			<INPUT TYPE="HIDDEN" NAME="codigoEE"   VALUE=<%= request.getAttribute( "codigoEE" ) %>>
			<INPUT TYPE="HIDDEN" NAME="codigoXX"   VALUE=<%= request.getAttribute( "codigoXX" ) %>>
			<INPUT TYPE="HIDDEN" NAME="Lado"    VALUE=0>

			<tr>
				<td class="tittabdat" colspan="2" height="25">&nbsp;&nbsp;&nbsp;Capture los datos de su Programaci&oacute;n de Consulta</td>
			</tr>

				<!-- CAPTURA DE DATOS GENERALES -->
			    <tr>
				<td align="center">
			        <table width="400" border="0" cellspacing="2" cellpadding="3">

			           <tr align="center">
			             <td class="textabdatcla" colspan="2">
			              <table width="500" border="0" cellspacing="0" cellpadding="5">

			  				<!--tr>
								<td align="left" class="tabmovtexbol" nowrap>Cuentas:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href ="javascript:SeleccionaCuenta(0);">
				                <img src="/gifs/EnlaceMig/gbo25421.gif" width="12" height="14" border="0" 		align="absmiddle"></a>
								</td>
							</tr-->
							<tr>
								<td class="tabmovtexbol">Cuentas Cliente:</td>
                                <td class="tabmovtexbol">Cuentas Seleccionadas:</td>
							</tr>
							<tr>
	                            <td class="tabmovtexbol" nowrap>
								<select multiple tabindex="10" name="CuentasC" size="12" style="width:350px" width="350" class="tabmovtexbol">
								<Option value = "Origen">Origen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Option>
								<%= request.getAttribute("cuentas") %>
								</select>
								</td>
								<td class="tabmovtexbol" nowrap>
								<select multiple tabindex="10" name="CuentasS" size="12" style="width:350px" width="350" class="tabmovtexbol">
								<Option value = "Destino">Destino&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Option>
						                <%= request.getAttribute("cuentas1") %>
								</select>
			                    </td>
							</tr>
                            <tr>
								<td>
								<!--input class=butStyle type=button value=" > " onclick=MoveValues(1,false)-->
								<input class=butStyle type=button value=" > " onclick="SumaSucursal();" alt="Agregar">
								<!--input class=butStyle type=button value=" >> " onclick=MoveValues(1,true)-->
								<input class=butStyle type=button value=" >> " onclick="SumaSucursalN();" alt="Todas">
								</td>
								<td>
								<!--input class=butStyle type=button value=" < " onclick=MoveValues(0,false)-->
								<input class=butStyle type=button value=" < " onclick="RestaSucursal();" alt="Quitar">
								<!--input class=butStyle type=button value=" << " onclick=MoveValues(0,true)-->
								<input class=butStyle type=button value=" << " onclick="RestaSucursalN();" alt="Todas">
								</td>
							</tr>

			                <tr>
								<td align="left" class="tabmovtexbol" nowrap>Periodicidad:
								<select name="gperiodo" id="gperiodo" class="tabmovtexbol" onchange="val_periodo();" onFocus="VerificaComboPer(this);">
				                   <option value = "1" selected>Diaria</option>
					               <option value = "2">Semanal</option>
						           <option value = "3">Mensual</option>
							       </select>
								</td>
	 							<td align="left" class="tabmovtexbol" nowrap>Horario de Entrega:
								<select NAME="ghorario" class="tabmovtexbol">
					               <option value = "08:00" selected>8:00</option>
						           <Option value = "09:00">9:00</Option>
							       <Option value = "10:00">10:00</Option>
								   <Option value = "11:00">11:00</Option>
								   <Option value = "12:00">12:00</Option>
								   <Option value = "13:00">13:00</Option>
								   <Option value = "14:00">14:00</Option>
								   <Option value = "15:00">15:00</Option>
								   <Option value = "16:00">16:00</Option>
								   <Option value = "17:00">17:00</Option>
								   <Option value = "18:00">18:00</Option>
								   <Option value = "19:00">19:00</Option>
								   <Option value = "20:00">20:00</Option>
				                   </select>
								</td>
					        </tr>

							<tr>
								<td align="left" class="tabmovtexbol" nowrap>Dia Semana:
								<select NAME="gdiasemana" class="tabmovtexbol" onFocus="VerificaComboDia(this);">
				                   <option value = "1" selected>Lunes</option>
					               <Option value = "2">Martes</Option>
						           <Option value = "3">Miercoles</Option>
								   <Option value = "4">Jueves</Option>
								   <Option value = "5">Viernes</Option>
							       </select>
								</td>
					        </tr>

			                <tr>
								<td align="left" class="tabmovtexbol" nowrap>Tipo de movimiento:
								<select NAME="gtipomov" class="tabmovtexbol" onFocus="VerificaComboTipo(this);">
			                       <option value = "B" selected>Todos</option>
					               <Option value = "C">Cargo</Option>
			                       <Option value = "A">Abono</Option>
			                       </select>
								</td>
								<td align="left" class="tabmovtexbol" nowrap>
									<input type="checkbox" name="gChksvc" value="si" onFocus="VerificaComboOmi(this);" onClick="VerificaComboOmi(this);">
									Omitir Salvo Buen Cobro
									<input type="checkbox" name="gChkvista" value="no" onFocus="VerificaComboVista(this);" onClick="VerificaComboVista(this);">
									Vista
								</td>
							</tr>

							<tr>
								<td align="left" class="tabmovtexbol" nowrap>Importe:
								<input type="text" name="gimporte" size="12" class="tabmovtexbol" onBlur="val_importe();" onFocus="VerificaImporte(this);">
								</td>
								<td align="left" class="tabmovtexbol" nowrap>Fecha de inicio de Entrega:
				                <input type="text" name="gfecha1" size="12" class="tabmovtexbol" onFocus="VerificaFecha(this);">
				                <!--a href ="javascript:WindowCalendar();"-->
								<a href ="javascript:SeleccionaFecha(0);">
				                <img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" 	align="absmiddle"></a>
								</td>
							</tr>

							<tr>
								<td align="left" class="tabmovtexbol" nowrap>Formato:
								<select NAME="gformato" class="tabmovtexbol" onchange="val_formato();" onFocus="VerificaComboFor(this);">
			                       <option value = "0" selected>Formato Enlace Internet</option>
			                       <Option value = "1">Formato Norma 43</Option>
			                      </select>
								</td>
								<td align="left" class="tabmovtexbol" nowrap>Reporte:
								<select NAME="greporte" class="tabmovtexbol" onchange="val_reporte();" onFocus="VerificaComboRep(this);">
			                       <option value = "1" selected>Detallado</option>
			                       <Option value = "2">Consolidado</Option>
			                      </select>
								</td>
							</tr>

							<tr>
								<td align="left" class="tabmovtexbol" nowrap>Canal de Entrega:
								<select NAME="gcanal" class="tabmovtexbol" onchange="val_entrega();" onFocus="VerificaComboCanal(this);">
			                       <option value = "2" selected>Buz&oacute;n Electronico</option>
			                       <Option value = "1">Email</Option>
								   <Option value = "3">Enlace</Option>
			                      </select>
								</td>
								<td align="left" class="tabmovtexbol" nowrap>Direcci&oacute;n de Correo:
								<input type="text" name="gemail" size="50" class="tabmovtexbol" maxlength="250" onFocus="VerificaEmail(this);">
								</td>
							</tr>
			              </table>
			            </td>
			          </tr>
			        </table>

					<!-- AREA DE BOTONES -->
					<!-- #BeginLibraryItem "/Library/footerPaginacion.lbi" -->
					<br>
					<table width="160" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
				          <tr><td align="right" valign="middle">
									<a href="javascript:confirmacion();" border=0>
					                 <img src="/gifs/EnlaceMig/gbo25480.gif" border=0 height="22"></a>
								</td>

						       <td align="left" valign="top">
							     <a href="javascript:LimpiarDatos();" border=0>
				                 <img src="/gifs/EnlaceMig/gbo25250.gif" border=0 height="22"></a></td>
				          </tr>
				    </table>
			    </td>
			    </tr>
			</form>

		</table>
		<!-- #EndEditable -->
	</body>
</HTML>