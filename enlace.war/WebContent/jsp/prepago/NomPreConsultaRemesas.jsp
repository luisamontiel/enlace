<%@ page import="mx.altec.enlace.beans.*"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--	
	function consultar() {
		var forma = document.consultaRemesa;
		var remesa = forma.noRemesa.value;
		var tarjeta = forma.noTarjeta.value;
		var selEdo = false;
		for (var i = 0; i < forma.chkEstado.length; i++) {
			if (forma.chkEstado[i].checked) {
				selEdo = true;
				break;
			}
		}
		if (selEdo || tarjeta != "") {			
			if (remesa == "" || !remesa.match(/^[0-9]+$/)) {
				cuadroDialogo("Debe proporcionar un n&uacute;mero de remesa v&aacute;lido", 1);				
				return;
			} else if(remesa != "" && remesa.length != <%=NomPreRemesa.LNG_NO_REMESA%>) {
				cuadroDialogo("Longitud incorrecta de n&uacute;mero de remesa", 1);
				return;				
			}
		}
		if (remesa != "" && !remesa.match(/^[0-9]+$/)) {
			cuadroDialogo("Debe proporcionar un n&uacute;mero de remesa v&aacute;lido", 1);
			return;
		} else if(remesa != "" && remesa.length != <%=NomPreRemesa.LNG_NO_REMESA%>) {
			cuadroDialogo("Longitud incorrecta de n&uacute;mero de remesa", 1);
			return;				
		}				
		if (tarjeta != "" && !tarjeta.match(/^[0-9]+$/)) {			
			cuadroDialogo("El n&uacute;mero de tarjeta no es v&aacute;lido.", 1);			
			return;			
		} else if(tarjeta != "" && tarjeta.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
			cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta", 1);
			return;				
		}										
		forma.submit();
	}
	function limpiar() {
		document.consultaRemesa.reset();
	}
	function validaNumero(e) {
		var key = window.event ? e.keyCode : e.which;
		if (key == 8) return true;
		return /\d/.test(String.fromCharCode(key));
	}	
//-->
</script>
<%request.getSession().removeAttribute("regresoConsulta");%>
<form action="${ctx}NomPreRemesas" method="get" name="consultaRemesa">
	<input type="hidden" name="operacion" value="consulta">
	<input type="hidden" name="inicial" value="true">
	<table cellspacing="0" cellpadding="0" border="0" width="760">
		<tr>
			<td align="center">
				<table cellspacing="2" cellpadding="3" border="0">							
					<tr>
						<td width="425" colspan="2" class="tittabdat">Seleccione los datos:</td>
					</tr>
					<tr align="center">
						<td height="96" width="425" valign="top" colspan="2" class="textabdatcla">
							<table cellspacing="0" cellpadding="0" border="0" width="400">
								<tr align="right" valign="top">
									<td align="left" width="420">
										<table cellspacing="0" cellpadding="5" border="0" width="420">
											<tr>
												<td align="right" class="tabmovtex">No de Remesa:</td>
												<td class="tabmovtex">
													<input type="text" value="" size="22" maxlength="<%=NomPreRemesa.LNG_NO_REMESA%>" class="tabmovtexbol" name="noRemesa" tabindex="0" onkeypress="return validaNumero(event);" autocomplete="off"/>													
												</td>
											</tr>
											<tr>
												<td align="right" class="tabmovtex">No de Tarjeta:</td>
												<td class="tabmovtex">
													<input type="text" value="" size="25" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%> " class="tabmovtexbol" name="noTarjeta" tabindex="1" onkeypress="return validaNumero(event);" autocomplete="off"/>
												</td>
											</tr>
											<tr>
												<td height="2" align="right" class="tabmovtex">Estatus:<br /></td>
												<td height="2" align="left" valign="middle" class="tabmovtex">
													<table cellspacing="0" cellpadding="3" border="0" width="240">
														<tr>
															<td align="center" width="32" valign="middle" class="tabmovtex">
																<input type="checkbox" value="<%=NomPreTarjeta.ESTADO_RECEPCION%>" name="chkEstado" tabindex="2"/>
															</td>
															<td width="208" class="tabmovtex">Recepci&oacute;n</td>
														</tr>
														<tr>
															<td align="center" width="32" valign="middle" class="tabmovtex">
																<input type="checkbox" value="<%=NomPreTarjeta.ESTADO_ASIGNADA%>" name="chkEstado" tabindex="3"/>
															</td>
															<td width="208" class="tabmovtex">Asignada</td>
														</tr>
														<tr>
															<td align="center" width="32" valign="middle" class="tabmovtex">
																<input type="checkbox" value="<%=NomPreTarjeta.ESTADO_BAJA%>" name="chkEstado" tabindex="4"/>
															</td>
															<td width="208" class="tabmovtex">Baja</td>
														</tr>
														<tr>
															<td align="center" width="32" valign="middle" class="tabmovtex">
																<input type="checkbox" value="<%=NomPreTarjeta.ESTADO_RECHAZADA%>" name="chkEstado" tabindex="5"/>
															</td>
															<td width="208" class="tabmovtex">Rechazada</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br />
				<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="166" style="height: 22px;">
					<tr>
						<td height="22" align="right" width="90" valign="top">
							<a href="javascript:consultar();"><img height="22" border="0" width="90" alt="Consultar" src="/gifs/EnlaceMig/gbo25220.gif" name="Enviar" /></a>
						</td>
						<td height="22" align="left" width="76" valign="middle">
							<a href="javascript:limpiar();"><img height="22" border="0" width="76" alt="Limpiar" src="/gifs/EnlaceMig/gbo25250.gif" name="limpiar" /></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>	
</form>
<jsp:include page="footerPrepago.jsp"/>
