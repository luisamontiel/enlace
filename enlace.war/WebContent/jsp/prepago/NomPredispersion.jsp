<jsp:include page="headerPrepago.jsp"/>
<script  type="text/javascript">

<% if (request.getAttribute("ArchivoErr") != null && request.getAttribute("ArchivoErr") != "" ) {%>
	window.onload = <%=request.getAttribute("ArchivoErr")%>;
<%} else { %>
	window.onload = msgCifrado;
<%}%>

function archivoErrores()
 {
   ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.document.open();
   ventanaInfo1.document.write("<html>");
   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");
   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
   ventanaInfo1.document.writeln("</head>");
   ventanaInfo1.document.writeln("<body bgcolor='white'>");
   ventanaInfo1.document.writeln("<form>");
   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>informacion del Archivo Importado</th></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");
   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center width=>");
   ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
   ventanaInfo1.document.writeln("<td class='tabmovtex1' align='center'><br>No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los siguientes errores.<br><br>");
   ventanaInfo1.document.writeln("</td></tr></table>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("<tr><td class='textabdatobs'>");
   ventanaInfo1.document.writeln(document.MantNomina.archivoEstatus.value+"<br><br>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("<table border=0 align=center >");
   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0 alt='Cerrar'></a>");
   ventanaInfo1.document.writeln("<a href = 'javascript:window.print();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a></td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("</form>");
   ventanaInfo1.document.writeln("</body>\n</html>");
   ventanaInfo1.document.close();
   ventanaInfo1.focus();
}

function js_importar()
{
	if(document.MantNomina.MantNomina.value=="")
		cuadroDialogo("Seleccione primero el archivo a importar con el bot&oacute;n Browse ...",1);
	else
	{
		document.MantNomina.operacion.value="cargaArchivo";
	    document.MantNomina.action="NomPreImportNomina";
	    document.MantNomina.submit();
	 }
}

function js_regresarPagos()
{
		document.MantNomina.operacion.value="regresarPagos";
		document.MantNomina.tipoArchivo.value="regresa";
		document.MantNomina.action="NomPreImportOpciones";
		document.MantNomina.submit();
}

function js_envio()
{
	var statusHorario;
	statusHorario = "<%= request.getAttribute("HorarioPremier")%>";
	var folio = document.MantNomina.folio.value;
	if(!isInteger(folio))
	{
		cuadroDialogo( "El campo folio debe ser num&eacute;rico" ,1);
		return;
	}
	try
	{
		var horarioSeleccionado = document.MantNomina.HorarioPremier.options[document.MantNomina.HorarioPremier.selectedIndex].value;
		if ( horarioSeleccionado == "" || horarioSeleccionado ==  null)
		{
			cuadroDialogo("Debe seleccionar un horario antes de enviar el archivo.", 3);
			return;
		}
		document.MantNomina.horario_seleccionado.value = horarioSeleccionado;
	}
	catch (e)
	{
		document.MantNomina.horario_seleccionado.value = "17:00";
	}
	if (trimString (document.MantNomina.cantidadDeRegistrosEnArchivo.value)==""
		|| trimString(document.MantNomina.cantidadDeRegistrosEnArchivo.value) == "00000"
		|| trimString(document.MantNomina.cantidadDeRegistrosEnArchivo.value) == "0")
	{
		cuadroDialogo("No hay registros que enviar, verifique por favor.", 1);
	}
	else
	{
		document.MantNomina.operacion.value="envio";
		document.MantNomina.tipoArchivo.value="enviado"
		cuadroDialogo("&#191;Esta seguro que desea enviar este archivo?",2);
	}
}

function js_envioConOTP()
{
	var statusHorario;
	statusHorario = "<%= request.getAttribute("HorarioPremier")%>";
	var folio = document.MantNomina.folio.value;
	if(!isInteger(folio)) {
		cuadroDialogo( "El campo folio debe ser num&eacute;rico" ,1);
		return;
	}
	try {
		var horarioSeleccionado = document.MantNomina.HorarioPremier.options[document.MantNomina.HorarioPremier.selectedIndex].value;
		if ( horarioSeleccionado == "" || horarioSeleccionado ==  null)
		{
			cuadroDialogo("Debe seleccionar un horario antes de enviar el archivo.", 3);
			return;
		}
		document.MantNomina.horario_seleccionado.value = horarioSeleccionado;
	} catch (e)	{
		document.MantNomina.horario_seleccionado.value = "17:00";
	}
	if (trimString (document.MantNomina.cantidadDeRegistrosEnArchivo.value)==""
		|| trimString(document.MantNomina.cantidadDeRegistrosEnArchivo.value) == "00000"
		|| trimString(document.MantNomina.cantidadDeRegistrosEnArchivo.value) == "0") {
		cuadroDialogo("No hay registros que enviar, verifique por favor.", 1);
	} else {
		document.MantNomina.operacion.value="envio";
		document.MantNomina.tipoArchivo.value="enviado"
		respuesta=1;
		continua();
	}
}

function continua()
{
	if(respuesta==1)
	{
		if(document.MantNomina.operacion.value=="envio")
		{
			document.MantNomina.action="NomPreImportOpciones";
			document.MantNomina.submit();
		}
		else
			document.MantNomina.submit();
	/**
	*		Se envia una contestacion de aceptacion al usuario. El servidor puede procesar el archivo duplicado.
	*/
		var statusHorario;
		statusHorario = "<%= request.getAttribute("HorarioPremier")%>";
	}
	else if(respuesta==11)
	{
			if(statusHorario != null || statusHorario != undefined)
			{
				document.MantNomina.HorarioPremier.value = "<%= request.getAttribute("HorarioPremier")%>";
			}
				document.MantNomina.operacion.value="envio";
				document.MantNomina.statusDuplicado.value="0";
				/*varialbe agregada por Eric Gomez para validar los horarios de 17:00 3 / FEB / 2004*/
				document.MantNomina.statushrc.value="0";
				document.MantNomina.tipoArchivo.value="enviado";
				document.MantNomina.action="NomPreImportOpciones";
				document.MantNomina.submit();
	}
	else if(respuesta==12)
	{
		    document.MantNomina.operacion.value="regresarPagos";
			document.MantNomina.action="NomPreImportOpciones";
			document.MantNomina.submit();
	}
	else if(respuesta==13)
	{
		statusHorario = nh;
		if(statusHorario != null || statusHorario != undefined)
		{
			document.MantNomina.horario_seleccionado.value = nh;
		}
		document.MantNomina.statusDuplicado.value="0";
		document.MantNomina.operacion.value="envio";
		document.MantNomina.tipoArchivo.value="enviado";
		document.MantNomina.action="NomPreImportOpciones";
		document.MantNomina.submit();
	}
}

function validaNumero(e) {
		var key = window.event ? e.keyCode : e.which;
		if (key == 8) return true;
		return /\d/.test(String.fromCharCode(key));
	}

function isInteger (s)
{
	var i;
    if (isEmpty(s))
       if (isInteger.arguments.length == 1)
       		return true;
       else
       		return (isInteger.arguments[1] == true);
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (!isDigit(c))
        	return false;
    }
    return true;
}

function isDigit (c)
{
	return ((c >= "0") && (c <= "9"))
}

function funcionPrueba(){
	alert("si entra a la funci�n de prueba");
	var mensaje = document.MantNomina.hdnPrueba.value;
	alert("mensaje:"+mensaje+"::");

}

function isEmpty(s)
{
	return ((s == null) || (s.length == 0))
}

function msgCifrado(){
 	try {
	var mensajeCifrado = document.MantNomina.mensajeCifrado.value;
	if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null") {
		var arreglo = mensajeCifrado.split("|");
               var msj = arreglo[1];
               var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
	}
	} catch (e){};

}
</SCRIPT>

<style>
	div.fileinputs {
	position: relative;
	}

	div.fakefile {
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 1;
	}
</style>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style= "background-color: #ffffff " onLoad="msgCifrado();"  background="/gifs/EnlaceMig/gfo25010.gif">


<FORM  NAME="MantNomina" ENCTYPE="multipart/form-data" METHOD="Post" ACTION="MantNomina">
<INPUT TYPE="hidden" name="horario_seleccionado" value="<%=request.getAttribute("horario_seleccionado")%>"/>
<INPUT TYPE="hidden" name="horario_seleccionado_nuevo" value="">
<INPUT TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>"/>
<INPUT TYPE="hidden" name="hdnPrueba" value="valorPrueba"/>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<%
	if (request.getAttribute("ContenidoArchivo")== "" || request.getAttribute("ContenidoArchivo") == null)
	{
%>
	<tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3">
		 <tr>
		   <td class="tittabdat" nowrap> Seleccione el archivo a importar
		   </td>
		 </tr>
		 <tr>
		   <td class="textabdatcla" valign="top" >
		     <table border="0" cellspacing="0" cellpadding="0">
			   <tr>
			     <td class="tabmovtex">Buscar archivo:
			     </td>
			   </tr>
			   <tr>
			     <td>
			     	<div class="fileinputs">
						<INPUT TYPE="file" NAME="MantNomina" size="30" onchange="document.getElementById('MantNomina2').value=this.value">
						<div class="fakefile">
							<input type="Text" NAME="MantNomina2" id="MantNomina2" readonly size="30" class="Archivo"/>
						</div>
					</div>
				</td>
			   </tr>
              </table>
           </td>
         </tr>
         <tr>
		   <td width="85" align=center >
		     <br><div align=center><a href="javascript:js_importar()"><img src="../../gifs/EnlaceMig/gbo25300.gif" border=0 alt="Importar" width="85" height="22"></a></div><br>
		   </td>
		 </tr>
       </table>
	  </td>
    </tr>
</table>
<%
	}
	if(request.getAttribute("error")!="" && request.getAttribute("error")!= null)
	{%>
		<table width="760" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td class=texfootpagneg align=center bottom="middle">
			<%out.println(request.getAttribute("error")); %></td></tr><br><br>
		</table>
	<%}%>
		<br>
<%
			if (request.getAttribute("TablaTotales")!= null)
					out.println(request.getAttribute("TablaTotales"));
			if (request.getAttribute("ContenidoArchivo")!= null || request.getAttribute("ContenidoArchivo") != "")
					out.println(request.getAttribute("ContenidoArchivo"));
			if (request.getAttribute("botones")!= null)
					out.println(request.getAttribute("botones"));
%>

<INPUT TYPE="hidden" VALUE="1" NAME="statushrc">
<INPUT TYPE="hidden" VALUE="1" NAME="statusDuplicado">
<INPUT TYPE="hidden" VALUE="0" NAME="operacion">
<INPUT TYPE="hidden" VALUE="<%
								if ( request.getAttribute("folio") != null)
									out.print(request.getAttribute("folio"));
							%>" NAME="folio">
<INPUT TYPE="hidden" VALUE="<%
								if ( request.getAttribute("encFechApli") != null)
									out.print(request.getAttribute("encFechApli"));
							%>" NAME="encFechApli">
<INPUT TYPE="hidden" VALUE="-1" NAME="registro">
<INPUT TYPE="hidden" VALUE="" NAME="secuencia">
<INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("fechaHoy") !=null)
									out.println( session.getAttribute("fechaHoy"));
								else
									if (request.getAttribute("fechaHoy")!= null)
										out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy">
<INPUT TYPE="hidden" NAME="nuevoArchivo">
<INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("tipoArchivo") !=null)
									out.println( session.getAttribute("tipoArchivo"));
								else
									if (request.getAttribute("tipoArchivo")!= null)
										out.println(request.getAttribute("tipoArchivo"));
							%>"  NAME="tipoArchivo">
<INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("horaSistema") !=null)
									out.println( session.getAttribute("horaSistema"));
								else
									if (request.getAttribute("horaSistema")!= null)
										out.println(request.getAttribute("horaSistema"));
							%>" NAME="horaSistema">
<input type="hidden" name="archivoEstatus" value='<%
								if(	session.getAttribute("archivoEstatus") !=null)
									out.println( session.getAttribute("archivoEstatus"));
								else
									if (request.getAttribute("archivoEstatus")!= null)
										out.println(request.getAttribute("archivoEstatus"));
							%>'>
<INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("cantidadDeRegistrosEnArchivo")!= null)
								    out.println(request.getAttribute("cantidadDeRegistrosEnArchivo"));
							%>" NAME="cantidadDeRegistrosEnArchivo">
<INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("textcuenta")!= null)
								   out.println(request.getAttribute("textcuenta"));
							%>" NAME="textcuenta">
<INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("encFechApli")!= null)
								   out.println(request.getAttribute("encFechApli"));
							%>" NAME="encFechApli">
<%
	if(	session.getAttribute("facultadImportar") !=null)
		out.println( session.getAttribute("facultadImportar"));
	else
		if (request.getAttribute("facultadImportar")!= null)
		{
			out.println(request.getAttribute("facultadImportar"));
		}
%>
<%
	if(	session.getAttribute("facultadAlta") !=null)
		out.println( session.getAttribute("facultadAlta"));
	else
		if (request.getAttribute("facultadAlta")!= null)
		{
			out.println(request.getAttribute("facultadAlta"));
		}
%>
<%
	if(	session.getAttribute("facultadCambios") !=null)
		out.println( session.getAttribute("facultadCambios"));
	else
		if (request.getAttribute("facultadCambios")!= null)
		{
			out.println(request.getAttribute("facultadCambios"));
		}
%>
<%
	if(	session.getAttribute("facultadBajas") !=null)
		out.println( session.getAttribute("facultadBajas"));
	else
		if (request.getAttribute("facultadBajas")!= null)
		{
			out.println(request.getAttribute("facultadBajas"));
		}
%>
<%
	if(	session.getAttribute("facultadEnvio") !=null)
		out.println( session.getAttribute("facultadEnvio"));
	else
		if (request.getAttribute("facultadEnvio")!= null)
		{
			out.println(request.getAttribute("facultadEnvio"));
		}
%>
<%
	if(	session.getAttribute("facultadRecuperar") !=null)
		out.println( session.getAttribute("facultadRecuperar"));
	else
		if (request.getAttribute("facultadRecuperar")!= null)
		{
			out.println(request.getAttribute("facultadRecuperar"));
		}
%>
<%
	if(	session.getAttribute("facultadExportar") !=null)
		out.println( session.getAttribute("facultadExportar"));
	else
		if (request.getAttribute("facultadExportar")!= null)
		{
			out.println(request.getAttribute("facultadExportar"));
		}
%>
<%
	if(	session.getAttribute("facultadBorra") !=null)
		out.println( session.getAttribute("facultadBorra"));
	else
		if (request.getAttribute("facultadBorra")!= null)
		{
			out.println(request.getAttribute("facultadBorra"));
		}
%>
<script>
<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
		if (request.getAttribute("infoUser")!= null) {
			out.println(request.getAttribute("infoUser"));
		}
%>

<%
	if(	session.getAttribute("infoImportados") !=null)
		out.println( session.getAttribute("infoImportados"));
	else
		if (request.getAttribute("infoImportados")!= null) {
			out.println(request.getAttribute("infoImportados"));
		}
%>
</script>
</form>
<jsp:include page="footerPrepago.jsp"/>