<%@ page import="mx.altec.enlace.beans.*,java.util.Iterator"%>
<%@ page import="mx.altec.enlace.utilerias.NomPreUtil,mx.altec.enlace.bita.BitaConstants"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--	
	function consultar(noRemesa) {
		var forma = document.remesasContrato;
		forma.noRemesa.value = noRemesa;
		forma.operacion.value = "detalle";
		forma.salida.value = "";
		forma.inicial.value = "true";
		forma.submit();
	}
	function exportar() {
		var forma = document.remesasContrato;
		forma.operacion.value = "consulta";
		forma.salida.value = "excel";
		forma.inicial.value = "";
		forma.noRemesa.value = "";
		forma.submit();
	}
	function paginar(direccion) { 
		var forma = document.paginacion;
		forma.accion.value = direccion;
		forma.submit();
	}
//-->
</script>
<center>
<% 
NomPreLM1A lm1a = (NomPreLM1A) request.getAttribute("lm1a");
if (lm1a != null && lm1a.getDetalle() != null && lm1a.getDetalle().size() > 0) {
%>
<form action="${ctx}NomPreRemesas" method="get" name="remesasContrato">
	<input type="hidden" name="operacion" value="">
	<input type="hidden" name="salida" value="">
	<input type="hidden" name="noRemesa" value="">	
	<input type="hidden" name="inicial" value="true">	
</form>	
<table cellspacing="0" cellpadding="0" border="0" width="760">
	<tr>
		<td align="center" colspan="10" class="texenccon">Total de Remesas:<%=lm1a.getDetalle().size()%><br>&nbsp;<br></td>
	</tr>
	<tr>
		<td align="center">
			<table cellspacing="2" cellpadding="3" border="0" width="258" class="tabfonbla">
				<tr>
					<td align="center" width="120" class="tittabdat">N&uacute;mero Remesa</td>
					<td align="center" width="135" class="tittabdat">Estatus</td>
				</tr>
				<% 
				int tamano = lm1a.getDetalle().size();
				int inicio = NomPreUtil.obtenIndiceInicial(request); 
				int fin = NomPreUtil.obtenIndiceFinal(request, tamano);																												
				
				Iterator iterator = lm1a.getDetalle().subList(inicio, fin).iterator();
				
				boolean claro = false;
				
				while (iterator.hasNext()) {
				
					NomPreRemesa remesa = (NomPreRemesa) iterator.next();
					
					String sClass = claro ? "textabdatcla" : "textabdatobs";
				%>
				<tr>
					<td align="center" width="120" class="<%=sClass%>"><a href="javascript:consultar('<%=remesa.getNoRemesa()%>');"><%=remesa.getNoRemesa()%></a></td>
					<td align="left" width="135" class="<%=sClass%>"><%=remesa.getDescEstadoRemesa()%></td>
				</tr>	
				<%
					claro = !claro;
				}
				%>	
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table height="40" cellspacing="0" cellpadding="0" border="0" width="760">
				<tr><td></td></tr>
				<tr>
					<td align="center" class="texfootpagneg">								
						<form action="${ctx}NomPreRemesas" name="paginacion">
							<input type="hidden" name="operacion" value="consulta">																								
							<input type="hidden" name="inicio" value="<%=inicio%>"/>
							<input type="hidden" name="fin" value="<%=fin%>"/>
							<input type="hidden" name="accion" value=""/>
						</form>
						Remesas : <%=(fin == 0 ? inicio : inicio + 1) + " - " + fin + " de " + tamano%><br/><br/>
						<% 
						if (inicio != 0) {%>
						<a href="javascript:paginar('A');">< Anteriores 50</a>
						<%}
						if (fin != 0 && fin < lm1a.getDetalle().size()) {%>
						<a href="javascript:paginar('S');">Siguientes 50 ></a>
						<%}%>		
						<br><br>
						<table height="40" cellspacing="0" cellpadding="0" border="0" width="168">					
							<tr>						
								<td height="22" align="right" width="85" valign="top">																
									<a href="javascript:exportar();"><img height="22" border="0" width="85" alt="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" name="imageField4"/></a>
								</td>
								<td height="22" align="right" width="83" valign="top">								
									<a href="${ctx}NomPreRemesas?operacion=iniconsul&<%=BitaConstants.FLUJO%>=<%=BitaConstants.ES_NP_REMESAS_CONSULTA%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
								</td>
							</tr>
						</table>									
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%} else {%>
<table width="760" border="0" cellspacing="0" cellpadding="0">			
	<tr>
		<td align="center">		
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">Consulta de remesas</td>
				</tr> 
				<tr>
					<td align="center" class="textabdatobs">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>	
					</td>
				</tr>			
			</table>
			<br/>
			<a href="${ctx}NomPreRemesas?operacion=iniconsul&<%=BitaConstants.FLUJO%>=<%=BitaConstants.ES_NP_REMESAS_CONSULTA%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
		</td>
	</tr>
</table>
<%}%>
</center>
<jsp:include page="footerPrepago.jsp"/>