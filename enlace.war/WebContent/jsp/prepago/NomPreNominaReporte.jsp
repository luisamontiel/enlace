<%@ page import="javax.swing.JProgressBar"%>

<jsp:include page="headerPrepago.jsp"/>

<script language="JavaScript">

window.onload = load;

function load()
{
	document.registro.operacion.value="validaArchivoImport";
	document.registro.action="NomPreImportNomina";
	document.registro.submit();
}

function muestraAvisos(strAvisos)
 {
   ventanaInfo1=window.open('','trainerWindow','width=460,height=200,toolbar=no,scrollbars=yes');
   ventanaInfo1.document.open();
   ventanaInfo1.document.write("<html>");
   ventanaInfo1.document.writeln("<head>\n<title>Avisos</title>\n");
   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
   ventanaInfo1.document.writeln("</head>");
   ventanaInfo1.document.writeln("<body bgcolor='white'>");
   ventanaInfo1.document.writeln("<form>");

   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>Avisos</th></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");
   ventanaInfo1.document.writeln(strAvisos);
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("</table>");

   ventanaInfo1.document.writeln("<table border=0 align=center >");
   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
   ventanaInfo1.document.writeln("</table>");

   ventanaInfo1.document.writeln("</form>");
   ventanaInfo1.document.writeln("</body>\n</html>");
   ventanaInfo1.focus();
 }

function js_importar1(){
			document.registro.operacion.value="validaArchivoImport";
			document.registro.action="NomPreImportNomina";
			document.registro.submit();
}

</script>

<form  name="registro" METHOD="Post">
<p><br>
<table border=0	 align=center cellpadding=0 cellspacing=0>
 <tr>
   <th class='texenccon' align=center height=20 bgcolor=red> <font color=white>Validando el archivo</font></th>
 </tr>
  <tr>
   <th class='texenccon' align=center height=20 bgcolor=red> <font color=white>Tama&ntilde;o del archivo en byte: <%
			if (request.getAttribute("tamanioArchivo")!= null){
				out.println(request.getAttribute("tamanioArchivo"));
				}%>
  </font></th>
  </tr>
 <tr>
   <td align=center  bgcolor='#FFFFFF' class='texencconx'><br>El sistema se encuentra validando sus datos.<br>Por favor espere un momento...<br><br></td>
 </tr>
 <tr>
   <td align=center bgcolor=red><img src="../../gifs/EnlaceMig/enlac.gif"></td>
 </tr>
</table>
<INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("operacion")!= null)
				    out.println(request.getAttribute("operacion"));
			    %>" NAME="operacion">
<INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("nomArchNomina")!= null)
			    out.println(request.getAttribute("nomArchNomina"));
			    %>" NAME="nomArchNomina">
<INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("originalName")!= null)
			    out.println(request.getAttribute("originalName"));
			    %>" NAME="originalName">
</form>
<jsp:include page="footerPrepago.jsp"/>
