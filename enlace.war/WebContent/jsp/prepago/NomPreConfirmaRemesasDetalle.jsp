<%@ page import="mx.altec.enlace.beans.*,mx.altec.enlace.bita.BitaConstants"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--
	function confirmar() {
		document.recepcionRemesa.submit();
	}
//-->
</script>
<%NomPreLM1B lm1b = (NomPreLM1B) request.getAttribute("lm1b");%>
<form action="${ctx}NomPreRemesas" method="get" name="recepcionRemesa">
	<input type="hidden" name="operacion" value="confirmar">
	<input type="hidden" name="noRemesa" value="<%=request.getParameter("noRemesa")%>">
	<table cellspacing="0" cellpadding="0" border="0" width="760">
		<tr>
			<td align="center" colspan="10" class="texenccon">
				No. de Remesa: <%=lm1b.getRemesa().getNoRemesa()%><br/><br/>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
					<tr>
						<td align="left" colspan="3" class="tittabdat">Recepci&oacute;n de Remesas</td>
					</tr>
					<tr>
						<td align="center" width="159" class="textabdatobs">Tarjetas recibidas</td>
						<td align="center" width="89" class="textabdatobs"><a onclick="javascript:window.open('${ctx}NomPreRemesas?operacion=detallesel&estado=A','trainerWindow','width=500,height=350,toolbar=no,scrollbars=yes,left=210,top=225');" href="#">Detalle</a></td>
						<td align="center" width="117" class="textabdatobs"><%=request.getAttribute("noaceptados")%> </td>
					</tr>
					<tr>
						<td align="center" width="159" class="textabdatcla">Tarjetas rechazadas</td>
						<td align="center" width="89" class="textabdatcla"><a onclick="javascript:window.open('${ctx}NomPreRemesas?operacion=detallesel&estado=R','trainerWindow','width=500,height=350,toolbar=no,scrollbars=yes,left=210,top=225');" href="#">Detalle</a></td>
						<td align="center" width="117" class="textabdatcla"><%=request.getAttribute("norechazado")%></td>
					</tr>
					<tr>
						<td align="center" width="159"/>
						<td align="center" width="89" class="tittabdat">Total</td>
						<td align="center" width="117" class="tittabdat"><%=lm1b.getDetalle().size()%></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<br/><br/>
				<table height="40" cellspacing="0" cellpadding="0" border="0" width="194">
					<tr>
						<tr>
						<td height="22" align="right" width="111" valign="top">
							<a href="javascript:confirmar();"><img height="22" width="111" border="0" alt="Confirmar" src="/gifs/EnlaceInternet/gbo50012.gif" name="imageField4"/></a>
						</td>
						<td height="22" align="right" width="83" valign="top">
							<a href="${ctx}NomPreRemesas?operacion=consultaRecep&noRemesa=<%=request.getParameter("noRemesa")%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<jsp:include page="footerPrepago.jsp"/>