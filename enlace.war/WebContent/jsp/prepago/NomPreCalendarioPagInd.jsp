<%@page contentType="text/html"%>

<jsp:useBean id='Calendario' class='java.util.ArrayList' scope='session'/>
<jsp:useBean id='fechaLib' class='java.util.GregorianCalendar' scope='session'/>

<%@page import="java.util.*"%>
<%@page import="mx.altec.enlace.utilerias.*"%>
<%@page import="mx.altec.enlace.bo.*"%>

<%-- Pagina para armar el calendario de dias habiles --%>
<html>
<head>
<STYLE TYPE="text/css">
.tittabdato {  font-family: Arial, Helvetica, sans-serif; font-size: 2px; font-weight: bold; background-color: #A4BEE4}
.tit {	font-family: Arial, Helvetica, sans-serif; font-size: 2px; font-weight: bold; background-color: #C7C7C7}
</Style>
    <title>Calendario</title>
    <LINK rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>
    <%-- Secci�n de Scripts	todo: Cambiar a un archivo js --%>
    <SCRIPT language='javascript'>
    var Enero = '01';
    var Febrero = '02';
    var Marzo = '03';
    var Abril = '04';
    var Mayo = '05';
    var Junio = '06';
    var Julio = '07';
    var Agosto = '08';
    var Septiembre = '09';
    var Octubre = '10';
    var Noviembre = '11';
    var Diciembre = '12';

    function seleccionaFecha (dia, mes, anio) {
		opener.fechaLim = dia + '/' + mes + '/' + anio;
		opener.continuaCal ();
		self.close ();	
    }
    </SCRIPT>
</head>
<body bgcolor='white'>
<%-- Imagen del encabezado --%>
<table width="300" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td width="152"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace Internet"></td>
	<%--td align="right" valign="bottom"><img src="/gifs/EnlaceMig/glo25040.gif" ></td--%>
    </tr>
</table>
<%-- Tabla con el calendario --%>
<%
    ListIterator liMeses = Calendario.listIterator ();
    pdMes Mes;
    pdDiaHabil Dia;
    ListIterator liDias;
    int diasBlanco = fechaLib.get (fechaLib.DAY_OF_WEEK) - 1;
    int diasSemana = diasBlanco;
    while (liMeses.hasNext ()) {
	Mes = (pdMes) liMeses.next ();
%>
<TABLE width='300' border='0' cellspacing='0' cellpadding='0'>
    <tr>
	<td class= tit height=7 width='0.5'>.
	    <!-- <img src='/gifs/website/gau10010.gif'	height='7' alt='..' name='.'> -->
	</td>
		<td height=7 width='299.5'>
		</td>
    </tr>
    <tr>
	<td class='tittabdato' height='2'>.
	    <!--  <img src='/gifs/website/gau10010.gif' width='1' height='2' alt='..' name='.'> -->
	</td>
		<td class='tittabdato'>.
		</td>
    </tr>
    <tr>
	<td class=tit height=7 width='0.5'>.
	    <!-- <img src='/gifs/website/gau10010.gif' width='1' height='7' alt='..' name='.'> -->
	</td>
		<td  height=7 width='299.5'>
		</td>
    </tr>
</TABLE>
<TABLE width='300' border='0' cellspacing='0' cellpadding='0'>
    <TR>
	<TD width='15'>
	    <IMG src='/gifs/EnlaceMig/gau25010.gif' width='15' height='20' border='0' alt='Cerrar'>
	</TD>
	<TD class='titpag' width='285' valign='top' colspan='6'>
	    <%out.println (Mes.getMes () + " " + Mes.getAnio ());%>
	</TD>
    </TR>
</TABLE>
<TABLE width='300' border='0' cellspacing='5' cellpadding='0'>
    <TR>
	<TD class='tabtexcal' align='center'>Dom</TD>
	<TD class='tabtexcal' align='center'>Lun</TD>
	<TD class='tabtexcal' align='center'>Mar</TD>
	<TD class='tabtexcal' align='center'>Mie</TD>
	<TD class='tabtexcal' align='center'>Jue</TD>
	<TD class='tabtexcal' align='center'>Vie</TD>
	<TD class='tabtexcal' align='center'>Sab</TD>
    </TR>
    <%
	liDias = Mes.getDias ().listIterator ();
	diasBlanco = diasSemana;
	diasSemana = diasBlanco;
	while (liDias.hasNext ()) {
	    Dia = (pdDiaHabil) liDias.next ();
	    if (diasSemana == 0) {
    %>
    <TR>
	<%
	    }
	    while (diasBlanco > 0) {
		diasBlanco--;
	%>
	<TD class='tabtexcal'>&nbsp;</TD>
	<%
	    }
	    if (Dia.getHabil ()||diasSemana==6) {
	%>
	<TD class='tabtexcal2' align='center'>
	    <A href='javascript:seleccionaFecha(<%if (Dia.getDia () < 10) out.println ("\"0" + Dia.getDia () + "\""); else out.println ("\"" + Dia.getDia () + "\"");%>, <%=Mes.getMes ()%>, <%=Mes.getAnio ()%>)'><%=Dia.getDia ()%></A>
	<%
	    } else {
	%>
	<TD class='tabtexcal2' align='center'>
	    <%=Dia.getDia ()%>
	<%
	    }
	%>
	</TD>
	<%
	    diasSemana++;
	    if (diasSemana == 7) diasSemana = 0;
	    if (diasSemana == 7 || !liDias.hasNext ()) {
	%>
    </TR>
    <%
	    }
	}
    %>
</TABLE>
<TABLE width='300' border='0' cellspacing='0' cellpadding='0'>
    <TR>
	<TD class='tittabdato' height=5>.
	    <!-- <IMG src='/gifs/website/gau10010.gif' width='1' height='5'> -->
	</TD>
    </TR>
</TABLE> <BR>
<%
    }
%>
<%-- Boton de cerrar --%>
<table width="300" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td align="center"><a href="javascript:self.close();"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0" alt="Cerrar"></a>
	</td>
    </tr>
</table>
</body>
</html>
