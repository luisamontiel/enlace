<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@ page import="mx.altec.enlace.utilerias.NomPreUtil,mx.altec.enlace.bita.BitaConstants"%>
<%@page import="mx.altec.enlace.beans.NomPreEmpleado"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--
	function accion(valor, salida){ 
		var forma = document.consultaEstado;
		var estado = "";
		var value = "";		
		if (forma.archivo == undefined) {
			cuadroDialogo("No existen registros", 1);
			return;
		}					
		if (forma.archivo.length == undefined) {
			value = forma.archivo.value; 
		} else {
			for(var i = 0; i < forma.archivo.length; i++) {
				if (forma.archivo[i].checked) {
					value = forma.archivo[i].value;
					break;		
				}
			}			 		
		}
		estado = value.substring(value.lastIndexOf("|") + 1);		
		if (valor == "enviar" && estado != "Procesado") {
			cuadroDialogo("Debe seleccionar un registro procesado", 1);
			return;
		}
		if (valor == "enviar") {			
			if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(forma.correoElec.value)){
				cuadroDialogo("Proporcione un correo electr&oacute;nico v&aacute;lido", 1);
				return;				
			}
		}
		if (valor == "cancelar" && estado != "Recibido") {
			cuadroDialogo("Debe seleccionar un registro recibido", 1);
			return;
		}		
		forma.operacion.value = valor;
		forma.salida.value = "";
		forma.submit();
	}	
	function exportar() {
		var forma = document.consultaEstado;
		forma.operacion.value = "detalle";
		forma.salida.value = "excel";
		forma.submit();
	}
	function paginar(direccion) { 
		var forma = document.paginacion;
		forma.accion.value = direccion;
		forma.submit();
	}	
	<%if (request.getAttribute("mensajeconestado") != null) {%>
	cuadroDialogo("<%=request.getAttribute("mensajeconestado")%> ", 1)
	<%}%>	 
//-->
</script>
<%List detalle = (List) request.getAttribute("detalle");		
if (detalle != null && detalle.size() > 0) {  %>
<form action="${ctx}NomPreConsultaEstado" method="get" name="consultaEstado">
	<input type="hidden" name="operacion">
	<input type="hidden" name="salida">
	<input type="hidden" name="fecha1" value="<%=request.getParameter("fecha1") == null ? "" : request.getParameter("fecha1")%>">
	<input type="hidden" name="fecha2" value="<%=request.getParameter("fecha2") == null ? "" : request.getParameter("fecha2")%>">
	<input type="hidden" name="secuencia" value="<%=request.getParameter("secuencia") == null ? "" : request.getParameter("secuencia")%>">
	
	<%String[] estados = request.getParameterValues("chkEstado"); 
	if (estados != null) {
		for(int i = 0; i < estados.length; i++) {		 
	%>	
	<input type="hidden" name="chkEstado" value="<%=estados[i]%>">
	<%	}
	}%>		

	<table cellspacing="2" cellpadding="3" border="0" align="center" width="395" class="tabfonbla">
		<tr> 
			<td align="center" width="50" class="tittabdat">Seleccione</td>
			<td align="center" width="70" class="tittabdat">Nombre archivo</td>
			<td align="center" width="70" class="tittabdat">Fecha</td>
			<td align="center" width="55" class="tittabdat">Secuencia</td>
			<td align="center" width="50" class="tittabdat">N&uacute;m Reg Enviados</td>
			<td align="center" width="50" class="tittabdat">N&uacute;m Reg Aceptados</td>
			<td align="center" width="50" class="tittabdat">N&uacute;m Reg Rechazados</td>
			<td align="center" width="70" class="tittabdat">Estatus</td>
		</tr>	
		<% 
			int tamano = detalle.size();
			int inicio = NomPreUtil.obtenIndiceInicial(request); 
			int fin = NomPreUtil.obtenIndiceFinal(request, tamano);	
				
			Iterator iterator = detalle.subList(inicio, fin).iterator();
				
			boolean claro = false;
			boolean primero = true;
				
			while (iterator.hasNext()) {
				NomPreAltaMasiva archivo = (NomPreAltaMasiva) iterator.next();
				String sClass = claro ? "textabdatcla" : "textabdatobs";
		%>							
		<tr>
			<td align="center" class="<%=sClass%>">
				<%if ("Cancelado".equals(archivo.getEstatus())) {%>
					&nbsp;
				<%} else { %>
				<input type="radio" name="archivo" <%= primero ? "checked=\"checked\"" : "" %> value="<%=archivo.getSecuencia()%>|<%=archivo.getFecha()%>|<%=archivo.getEstatus()%>"/>
				<%
					if (primero) { 
						primero = false;
					}
				}%>
			</td>
			<td align="center" class="<%=sClass%>"><%=archivo.getNombreArchivo()%></td>
			<td align="center" class="<%=sClass%>"><%=archivo.getFecha()%></td>
			<td align="center" class="<%=sClass%>"><%=archivo.getSecuencia()%></td>
			<td align="center" class="<%=sClass%>"><%=archivo.getNoRegEnviados()%></td>
			<td align="center" class="<%=sClass%>"><%=archivo.getNoRegAceptados()%></td>
			<td align="center" class="<%=sClass%>"><%=archivo.getNoRegRechazados()%></td>
			<td align="center" class="<%=sClass%>"><%=archivo.getEstatus()%></td>
		</tr>
		<%		claro = !claro;
			}%>
	</table>
	<table align="center">
		<tr>
			<td align="center" class="tabmovtex">
		   		<br/>Correo: <input type="text" size="40" maxlength="40" name="correoElec"/><br/>
		 	</td>
		</tr>
	</table>
</form>	
	<table align="center">	
		<tr>
		  	<td class="texfootpagneg">
				<form action="${ctx}NomPreConsultaEstado" method="get" name="paginacion">								
					<input type="hidden" name="operacion" value="detalle">												
					<input type="hidden" name="inicio" value="<%=inicio%>"/>
					<input type="hidden" name="fin" value="<%=fin%>"/>
					<input type="hidden" name="accion" value=""/>								
				</form>
				Archivos : <%=(fin == 0 ? inicio : inicio + 1) + " - " + fin + " de " + tamano%><br/><br/>
				<% if (inicio != 0) {%>
					<a href="javascript:paginar('A');">< Anteriores 50</a>
				<%}
				if (fin != 0 && fin < detalle.size()) {%>
					<a href="javascript:paginar('S');">Siguientes 50 ></a>
				<%}%>									
		  	</td>
		</tr>
	</table>					
	<br/>
	<table cellspacing="0" cellpadding="0" border="0" align="center">
		<tr>
			<td>
				<a href="javascript:exportar();"><img border="0" name="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" alt="Exportar"/></a>
			</td>
			<td>
				<a href="javascript:accion('enviar');"><img border="0" src="/gifs/EnlaceMig/gbo25245.gif" alt="Enviar detalle"/></a>
			</td>
			<td>
				<a href="javascript:accion('cancelar');"><img border="0" src="/gifs/EnlaceMig/gbo25190.gif" alt="Cancelar"/></a>
			</td>
			<td>
			</td>
			<td>				
				<a href="${ctx}NomPreConsultaEstado"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>				
			</td>
		</tr>
	</table>				   			    		

<%} else {%>
<table width="760" border="0" cellspacing="0" cellpadding="0">			
	<tr>
		<td align="center">		
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">
						Consulta estado alta masiva
					</td>
				</tr> 
				<tr>
					<td align="center" class="textabdatobs">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>		
					</td>
				</tr>				
			</table>
			<br/>
			<a href="${ctx}NomPreConsultaEstado?<%=BitaConstants.FLUJO%>=<%=BitaConstants.ES_NP_TARJETA_CONSULTA_ALTA%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
    	</td>
	</tr>
</table>
<%}%>
<jsp:include page="footerPrepago.jsp"/>
