
<%@ include file="/jsp/prepago/headerPrepago.jsp" %>
<%@page import="mx.altec.enlace.beans.NomPreTarjeta"%>
<%@page import="mx.altec.enlace.beans.NomPreEmpleado"%>
<script language="javaScript">
<%
       if (request.getAttribute("mensajecancela")!= null){
	     	out.println(request.getAttribute("mensajecancela"));
	    	request.removeAttribute("mensajecancela");
	   }
%>
<% if(request.getAttribute("VarFechaHoy")!= null) out.print(request.getAttribute("VarFechaHoy")); %>

var js_diasInhabiles="<%= request.getAttribute("diasInhabiles") %>"

function limpiar() {
	document.MantNomina.reset();
	activarOpcion('NE');
}

function activarOpcion(opcion) {
	var forma = document.MantNomina;
	forma.valNombre.value="";
	forma.valPaterno.value="";
	forma.valMaterno.value="";
	forma.valNoEmpleado.value="";
	forma.valNoTarjeta.value="";
	forma.fecha1.value="";
	forma.valNombre.disabled=true;
	forma.valPaterno.disabled=true;
	forma.valMaterno.disabled=true;
	forma.valNoEmpleado.disabled=true;
	forma.valNoTarjeta.disabled=true;
	forma.fecha1.disabled=true;
	if (opcion == 'NE') {
		forma.valNoEmpleado.disabled=false;
	} else if (opcion == 'NT') {
		forma.valNoTarjeta.disabled=false;
	} else if (opcion == 'NO') {
		forma.valNombre.disabled=false;
		forma.valPaterno.disabled=false;
		forma.valMaterno.disabled=false;
	} else if (opcion == 'FE') {
		forma.fecha1.disabled=false;
	}
}

function validaNE(campo){
	return campo.value.match(/^[0-9]{1,<%=NomPreEmpleado.LNG_NO_EMPLEADO%>}$/);
}
function validaNO(campo){
	return campo.value.match(/^[\sA-Za-z � �]*$/);
}
function validaFE(campo){
	return campo.value.match(/^\d{1,2}\/\d{1,2}\/\d{2,4}$/);
}
 function WindowCalendar(){
 	var val = 0;
 	var radios = document.MantNomina.rbTipoFiltro;
	for(var i = 0; i < radios.length; i++) {
		if( radios[i].checked == true )
		val = radios[i].value;
	}
	if (val == "FE") {
    	var m=new Date();
    	m.setFullYear(document.Frmfechas.strAnio.value);
    	m.setMonth(document.Frmfechas.strMes.value);
   	 	m.setDate(document.Frmfechas.strDia.value);
   	 	n=m.getMonth();
    	msg=window.open("/EnlaceMig/calNom22.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    	msg.focus();
    }
}

String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}

function valida(){
	getTipoFiltro();
	var f=document.MantNomina;
	if(f.tipoFiltro.value.trim()=='TO'){return true;}
	
	if(f.tipoFiltro.value.trim()=='NT' && f.valNoTarjeta.value.length == 0){
		cuadroDialogo("Es necesario especificar el n&uacute;mero de tarjeta.",1);
        return false;
	}
	if(f.tipoFiltro.value.trim()=='NE' && f.valNoEmpleado.value.length == 0){
		cuadroDialogo("Es necesario especificar el n&uacute;mero de empleado.",1);
        return false;
	}						
	if(f.tipoFiltro.value.trim()=='NO' && (f.valNombre.value.length == 0 && 
	f.valPaterno.value.length == 0 && f.valMaterno.value.length == 0)){
		cuadroDialogo("Es necesario especificar el nombre del empleado.",1);
        return false;
	}
	if(f.tipoFiltro.value.trim()=='FE' && f.fecha1.value.length == 0){
		cuadroDialogo("Es necesario especificar una fecha.",1);
        return false;
	}
	return true;
}
function consulta() {
	var f=document.MantNomina;
	if(valida()&& validaDatos()){
		f.opcion.value="consulta";	
		f.action="${ctx}NomPreBusqueda";
		f.submit();	
	}
}
function soloNumeros(evt) { 
    evt = (evt) ? evt : event; 
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : 
        ((evt.which) ? evt.which : 0)); 
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {       
        return false; 
    } 
    return true; 
 }
function validaDatos(){
	var f=document.MantNomina;
	getTipoFiltro();
	var tFiltro=f.tipoFiltro.value.trim();

	if(tFiltro=='NE'){
		if(!validaNE(f.valNoEmpleado)){
			cuadroDialogo("El n&uacute;mero de empleado no es v&aacute;lido.",1);
			return false;
		}
	}else if(tFiltro=='NT'){	
		if (f.valNoTarjeta.value != "" && !f.valNoTarjeta.value.match(/^[0-9]+$/)) {			
			cuadroDialogo("El n&uacute;mero de tarjeta no es v&aacute;lido.", 1);			
			return;			
		} else if(f.valNoTarjeta.value != "" && f.valNoTarjeta.value.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
			cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta", 1);
			return;				
		}						
	}else if(tFiltro=='NO'){
		if(!validaNO(f.valNombre)){
			cuadroDialogo("El nombre no es v&aacute;lido.",1);
			return false;
		}
		if(!validaNO(f.valPaterno)){
			cuadroDialogo("El apellido paterno no es v&aacute;lido.",1);
			return false;
		}
		if(!validaNO(f.valMaterno)){
			cuadroDialogo("El apellido materno no es v&aacute;lido.",1);
			return false;
		}
	}else if(tFiltro=='FE'){
		if(!validaFE(f.fecha1)){			
			cuadroDialogo("El formato de fecha debe ser dd/mm/aaaa.",1);
			return false;
		}
	} 
	return true;
}
function getTipoFiltro()
{
	var i;
	var ctrl=document.MantNomina.rbTipoFiltro;
    for(i=0;i<ctrl.length;i++){
        if(ctrl[i].checked){
            document.MantNomina.tipoFiltro.value=ctrl[i].value;
         	break;
        }
    }
}
function convierteMayus(campo)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;
	return true;
}
function cambiaEmpleado()
{
	var f=document.MantNomina;
	var cadena=f.valNoEmpleado.value;
	var folioReturn = "";	
	if(cadena.length>0){	
		var longitud = cadena.length;
		if (longitud < 7)
			for (i = 1; i <= 7 - longitud; i++)
				folioReturn += "0";
		folioReturn += cadena;
		f.valNoEmpleado.value=folioReturn;
			
	}	
}
</Script>	
<form name = "Frmfechas">
  
  <Input type = "hidden" name ="strAnio" value = "<%=request.getAttribute("anio") %>">
  <Input type = "hidden" name ="strMes" value = "<%=request.getAttribute("mes") %>">
  <Input type = "hidden" name ="strDia" value = "<%=request.getAttribute("dia") %>">
</form>
<form name="MantNomina" METHOD="POST" ACTION="">							
<table width="760" border="0" cellspacing="0" cellpadding="0" >	
<tr>
		<td align="center">		
		<table cellspacing="2" cellpadding="4" border="0" width="600" class="textabdatcla">
			<tr>
				<td colspan="4" class="tittabdat"> Capture los datos para la consulta de Empleados<br/></td>
			</tr>
			<tr><td colspan="4" class="tabmovtex">Ingrese criterio de b&uacute;squeda: </td></tr>
			<tr>
				<td class="tabmovtex" width="100px">
					<input type="radio" value="NE" name="rbTipoFiltro" checked="checked" onclick="javascript:activarOpcion('NE');"/>No. Empleado
				</td>
				<td colspan="3" width="260" class="CeldaContenido">
					<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_NO_EMPLEADO%>" size="8" name="valNoEmpleado" onkeypress="return soloNumeros(event)" onblur="cambiaEmpleado();"/>
				</td>
			</tr>
			<tr>
				<td class="tabmovtex">					
					<input type="radio" value="NT" name="rbTipoFiltro" onclick="javascript:activarOpcion('NT');"/>No Tarjeta
				</td>
				<td colspan="3" width="260" class="CeldaContenido">
					<input type="text" value="" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" size="23" name="valNoTarjeta" onkeypress="return soloNumeros(event)" disabled="disabled"/>
				</td>
			</tr>
			<tr> 
				<td rowspan="2" valign="top"> <input type="radio" value="NO"  name="rbTipoFiltro" onclick="javascript:activarOpcion('NO');"/>Nombre </td>
				<td class=CeldaContenido><input type="text" value="" name="valNombre" maxlength="<%=NomPreEmpleado.LNG_NOMBRE %>" size="20" onchange="convierteMayus(this);" disabled="disabled"></td>
				<td class=CeldaContenido><input type="text" value="" name="valPaterno" maxlength="<%=NomPreEmpleado.LNG_PATERNO %>" size="20" onchange="convierteMayus(this);" disabled="disabled"></td>
				<td class=CeldaContenido><input type="text" value="" name="valMaterno" maxlength="<%=NomPreEmpleado.LNG_MATERNO %>" size="20" onchange="convierteMayus(this);" disabled="disabled"></td>
			</tr>
			<tr> 
				<td class=textabdatcla>Nombre</td>						
				<td class=textabdatcla> Paterno</td>		
				<td class=textabdatcla> Materno </td>				
			</tr>
			<tr> 
				<td> <input type="radio" value="FE" name="rbTipoFiltro" onclick="javascript:activarOpcion('FE');"/>Fecha</td>
				<td colspan="3">
					<input type="text" name="fecha1" size="12" class="tabmovtex" OnFocus = "blur();" disabled="disabled">
					<a href="javascript:WindowCalendar();"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle" name="imgcal" ></a>					
				</td>				
			</tr>
			<tr>
				<td><input type="radio" value="TO" name="rbTipoFiltro" onclick="javascript:activarOpcion('TO');"/>Todos</td>
				<td colspan="3"></td>				
			</tr>
		</table>		
		<br>				
		<table height="22" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="166">
    		<tr>
    			<td height="22" align="right" width="90" valign="top">
    				<a href="javascript:consulta();" border=0 > <img  src="/gifs/EnlaceMig/gbo25220.gif" border=0></a>
        		</td>
      			<td height="22" align="left" width="76" valign="middle">
	      			<a href="javascript:limpiar();" border=0><img  src="/gifs/EnlaceMig/gbo25250.gif" border=0></a>							      				
       			</td>
       			</tr>
       		</table>				
	    </td>
	</tr>
</table>
<INPUT TYPE="hidden" NAME="opcion">
<INPUT TYPE="hidden" NAME="tipoFiltro">
<input type="hidden" name="inicial" value="true">
</form>
<%@ include file="/jsp/prepago/footerPrepago.jsp" %>

