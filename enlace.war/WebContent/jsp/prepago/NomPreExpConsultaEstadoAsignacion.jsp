<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%List detalle = (List) request.getAttribute("detalle");				
if (detalle != null) {%>
<html>
	<head>
		<title>Detalle alta masiva</title>
	</head>
	<body>
		<table border="0">
			<tr>
				<td>
					<br>
					<table border="1">						
						<tr>							
							<td align="center" width="70" class="tittabdat"><b>Nombre archivo</b></td>
							<td align="center" >Fecha</td>
							<td align="center" width="55"><b>Secuencia</b></td>
							<td align="center" width="50"><b>N&uacute;m Reg Enviados</b></td>
							<td align="center" width="50"><b>N&uacute;m Reg Aceptados</b></td>
							<td align="center" width="50"><b>N&uacute;m Reg Rehazados</b></td>
							<td align="center" width="70"><b>Estatus</b></td>							
						</tr>
						<%Iterator it = detalle.iterator();
						while (it.hasNext()) {
							NomPreAltaMasiva archivo = (NomPreAltaMasiva) it.next();
						%>						
						<tr>
							<td align="center"><%=archivo.getNombreArchivo()%></td>												
							<td align="center"><%=archivo.getFecha()%></td>
							<td align="center"><%=archivo.getSecuencia()%></td>
							<td align="center"><%=archivo.getNoRegEnviados()%></td>
							<td align="center"><%=archivo.getNoRegAceptados()%></td>
							<td align="center"><%=archivo.getNoRegRechazados()%></td>
							<td align="center"><%=archivo.getEstatus()%></td>										
						</tr>
						<%}%>
					</table>
				</td>
			</tr>
		</table>		
		<br>
	</body>
</html>
<%}%>