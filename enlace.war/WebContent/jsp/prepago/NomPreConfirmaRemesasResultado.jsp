<%@ page import="mx.altec.enlace.beans.*,java.util.*,mx.altec.enlace.bita.BitaConstants"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--
	var ventanActiva;
	var ventana;
	var respuesta;
	var operacion;
	var resConfirma=0;
	function continua() {
		if (respuesta != 1) {
			resConfirma=0;
			return;
		}
		if (resConfirma == 1) {
			rechazar();
		}
		if(resConfirma==2) {
			var forma = document.detalleRecepcion;
			forma.operacion.value="confirmatodo";
			forma.submit();
		}
	}
	function exportar() {
		var forma = document.detalleRecepcion;
		forma.salida.value="excel";
		forma.operacion.value="consultaRecep";
		forma.submit();
	}
	function rechazar() {
		var forma = document.detalleRecepcion;
		forma.operacion.value="rechazar";
		forma.submit();
	}
	function enviar() {
		var forma = document.detalleRecepcion;
		var marcado = false;
		var marcados = 0;
		var total = 0;
		resConfirma=0;
		for (var i = 0; i < forma.elements.length; i++) {
			var e = forma.elements[i];
			if (e.type == "checkbox" && e.name != 'todas') {
				total++;
				if (e.checked) {
					marcado = true;
					marcados++;
				}
			}
		}
		if (!marcado) {
			cuadroDialogo("Debe seleccionar al menos una tarjeta", 1);
			return;
		}
		if (marcados == total) {
			resConfirma = 2;
			cuadroDialogo('�Desea recibir la remesa completa?', 2);
		} else {
			forma.operacion.value="recepcion";
			forma.submit();
		}
	}
	function marcaTodo() {
		var forma = document.detalleRecepcion;
		for (var i = 0; i < forma.elements.length; i++) {
			var e = forma.elements[i];
			if (e.type == "checkbox" && e.name != 'todas')
				e.checked = forma.todas.checked;
		}
	}
//-->
</script>
<%NomPreLM1B lm1b = (NomPreLM1B) request.getAttribute("lm1b");
if (lm1b != null && lm1b.getDetalle() != null && lm1b.getDetalle().size() > 0) {%>
<form action="${ctx}NomPreRemesas" method="post" name="detalleRecepcion">
	<input type="hidden" name="operacion" value="resumen">
	<input type="hidden" name="salida" value="">
	<input type="hidden" name="noRemesa" value="<%=request.getParameter("noRemesa")%>">
	<table cellspacing="0" cellpadding="0" border="0" width="760">
		<tr>
			<td align="center" colspan="10" class="texenccon">
				No. de Remesa:�<%=lm1b.getRemesa().getNoRemesa()%><br/>
				Total de tarjetas:�<%=lm1b.getDetalle().size()%><br/><br/>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
					<tr>
						<td align="center" width="58" class="tittabdat"><input type="checkbox" onclick="javascript:marcaTodo();" name="todas" value="todas"/>Todas</td>
						<td align="center" width="191" class="tittabdat">N&uacute;mero Tarjeta</td>
						<td align="center" width="117" class="tittabdat">Fecha de Expiraci&oacute;n</td>
					</tr>
					<%
					List list = (List) session.getAttribute("aceptados");
					Iterator iterator = lm1b.getDetalle().iterator();
					boolean claro = false;
					boolean prevSel = list != null && list.size() > 0;
					while (iterator.hasNext()) {
						NomPreTarjeta tarjeta = (NomPreTarjeta) iterator.next();
						String sClass = claro ? "textabdatcla" : "textabdatobs";
					%>
					<tr>
						<td align="center" width="58" class="<%=sClass%>"><input type="checkbox" value="<%=tarjeta.getNoTarjeta()%>" name="tarjetas" <%=prevSel ? list.contains(tarjeta.getNoTarjeta()) ? "checked=\"checked\"" : "" : ""%>/></td>
						<td align="center" width="191" class="<%=sClass%>"><%=tarjeta.getNoTarjeta()%></td>
						<td align="center" width="117" class="<%=sClass%>"><%=tarjeta.getFechaVigencia()%></td>
					</tr>
					<%
						claro = !claro;
					}
					%>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<br><br>
				<table height="40" cellspacing="0" cellpadding="0" border="0" width="329">
					<tr>
						<td height="22" align="right" width="78" valign="top">
            				<a href="javascript:enviar();"><img height="22" border="0" width="78" alt="Enviar" src="/gifs/EnlaceMig/gbo25520.gif" name="imageField4"/></a>
            			</td>
            			<td height="22" align="right" width="85" valign="top">
            				<a href="javascript:exportar();"><img height="22" border="0" width="85" alt="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" name="imageField4"/></a>
            			</td>
            			<td height="22" align="right" width="83" valign="top">
            				<a href="javascript:resConfirma=1;cuadroDialogo('�Desea rechazar la remesa?', 2);"><img height="22" border="0" width="83" alt="Rechazar" src="/gifs/EnlaceMig/gbo25190.gif" name="imageField42"/></a>
            			</td>
            			<td height="22" align="right" width="83" valign="top">
            				<a href="${ctx}NomPreRemesas?operacion=inirecep&noRemesa=<%=request.getParameter("noRemesa")%>&<%=BitaConstants.FLUJO%>=<%=BitaConstants.ES_NP_REMESAS_RECEPCION%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<%} else {%>
<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">
						Recepci&oacute;n de remesas
					</td>
				</tr>
				<tr>
					<td nowrap="" align="center" class="textabdatobs">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>
					</td>
				</tr>
			</table>
			<br/>
			<a href="${ctx}NomPreRemesas?operacion=inirecep&<%=BitaConstants.FLUJO%>=<%=BitaConstants.ES_NP_REMESAS_RECEPCION%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
    	</td>
	</tr>
</table>
<%}%>
<jsp:include page="footerPrepago.jsp"/>