<%@ page import="mx.altec.enlace.beans.*,java.util.Iterator"%>
<%NomPreLM1B lm1b = (NomPreLM1B) request.getAttribute("lm1b");
if (lm1b != null) {%>
<html>
	<head>
		<title>Detalle remesa</title>
	</head>
	<body>
		<table border="0">
			<tr>
				<td>
					<br>
					<table border="1">
						<tr>
							<td align="center"><b>Total de tarjetas</b></td>
							<td align="center"><b><%=lm1b.getDetalle().size()%></b></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>							
							<td align="center"><b>N&uacute;mero de Tarjeta</b></td>
							<td align="center"><b>Fecha de Expiraci&oacute;n</b></td>
							<td align="center"><b>Estatus</b></td>
						</tr>
						<%Iterator it = lm1b.getDetalle().iterator();
						while (it.hasNext()) {
							NomPreTarjeta tarjeta = (NomPreTarjeta) it.next();
						%>						
						<tr>							
							<td align="center">
								&nbsp;<%=tarjeta.getNoTarjeta()%>
							</td>
							<td align="center">
								<%=tarjeta.getFechaVigencia()%>
							</td>
							<td align="center">
								<%=tarjeta.getDescEstadoTarjeta()%>
							</td>
						</tr>
						<%}%>
					</table>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
<%}%>