<jsp:include page="headerPrepago.jsp"/>
<%@page import="mx.altec.enlace.beans.NomPreTarjeta"%>
<%@page import="mx.altec.enlace.beans.NomPreEmpleado"%>
<script language="javascript">

<%
  String diasInhabiles="";

  if (request.getAttribute("diasInhabiles")!= null)
     diasInhabiles=(String)request.getAttribute("diasInhabiles");
%>

var js_diasInhabiles="<%=diasInhabiles%>";
var diasInhabiles = "<%=diasInhabiles%>";
var Nmuestra = 30;
String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}
window.onload = load;
var original;

function load()
{
	if(document.MantNomina.opcionbus[0].checked)
	{
		original = document.MantNomina.opcionbus[0].value;
		document.MantNomina.opcionbus[0].focus();
		
	}
	else
	{
		original = document.MantNomina.opcionbus[1].value;
		document.MantNomina.opcionbus[1].focus();
	}
}

function FrmClean()
{
  document.MantNomina.textcuenta.value	="";
  document.MantNomina.fecha1.value	= "";
  document.MantNomina.fecha2.value	= "";
  document.MantNomina.Importe.value	= "";
  document.MantNomina.secuencia.value      = "";
  document.MantNomina.empleado.value = "";
  document.MantNomina.tarjeta.value = "";
  document.MantNomina.ChkRecibido.checked  = "";
  document.MantNomina.ChkCancelado.checked = "";
  document.MantNomina.ChkEnviado.checked   = "";
  document.MantNomina.ChkPagado.checked    = "";
}

function FrmClean1()
{
  document.MantNomina.textcuenta.value	="";
  document.MantNomina.fecha1.value	= "";
  document.MantNomina.fecha2.value	= "";
  document.MantNomina.Importe.value	= "";
  document.MantNomina.secuencia.value      = "";
  document.MantNomina.ChkRecibido.checked  = "";
  document.MantNomina.ChkCancelado.checked = "";
  document.MantNomina.ChkEnviado.checked   = "";
  document.MantNomina.ChkPagado.checked    = "";
}
function validaNE(campo){
	return campo.match(/^[0-9]{1,<%=NomPreEmpleado.LNG_NO_EMPLEADO%>}$/);
}
function js_consultar()
{
   var pasa = true;
   campo1 =   document.MantNomina.textcuenta.value.trim();
   campo2 =	  document.MantNomina.fecha1.value.trim();
   campo3 =   document.MantNomina.fecha2.value.trim();
   campo4 =   document.MantNomina.Importe.value.trim();
   campo5 =   document.MantNomina.secuencia.value.trim();
   campo6 =   document.MantNomina.empleado.value.trim();
   campo7 =   document.MantNomina.tarjeta.value.trim();

	if(campo5 != "")
	{
		if(!isInteger(campo5))
		{
			cuadroDialogo("El n&uacute;mero de secuencia debe ser num&eacute;rico y sin caracteres especiales",1);
		 	pasa = false;
  		}
  	}
  	
	if(campo6!="" && !validaNE(campo6)){
		cuadroDialogo("El n&uacute;mero de empleado es inv&aacute;lido.", 1);	
		pasa= false;
	}
	
	if (campo7 != "" && !campo7.match(/^[0-9]+$/)) {			
		cuadroDialogo("El n&uacute;mero de tarjeta no es v&aacute;lido.", 1);			
		pasa = false;			
	} else if(campo7 != "" && campo7.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
		cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta", 1);
		pasa = false;				
	}		
			
  	if (campo4 != "")
	{
        if(!checkFloatValueCon(campo4))
        {
		    cuadroDialogo("El importe debe ser num&eacute;rico",1);
		    pasa = false;
  		}
  	}

	if (pasa)
	{
		document.MantNomina.operacion.value="consulta";
		if(document.MantNomina.opcionbus[0].checked)
			document.MantNomina.tipo.value=document.MantNomina.opcionbus[0].value;
		else
			document.MantNomina.tipo.value=document.MantNomina.opcionbus[1].value;
		document.MantNomina.action="NomPreImportOpciones";
		document.MantNomina.submit();
	}
}

function checkFloatValueCon(myobj)
{
  if(myobj != "")
  {
    if(!isFloatCon(myobj))
    {
      myobj = "";
      document.MantNomina.Importe.focus();
      cuadroDialogo("El Importe debe ser num&eacute;rico",3);
      return false;
    }
  }
  return true;
}

function isFloatCon (s)
{   
	var i;
    var seenDecimalPoint = false;
    if (isEmptyCon(s))
    {
       if (isFloat.arguments.length == 1) 
       		return true;
       else 
       		return (isFloat.arguments[1] == true);
    }
    if (s == ".") 
    {
    	return false;
    }
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if ((c == ".") && !seenDecimalPoint) 
        {
        	seenDecimalPoint = true;
        }
        else 
        {
        	if (!isDigit(c))
        	{ 
        		cuadroDialogo("El importe no debe contener letras ni m&aacute;s de dos puntos decimales",1);
        		return false;
        	}
        }
    }
    return true;
}

function isEmptyCon(s){
	return ((s == null) || (s.length == 0))
}

function checkValueCon(myobj){
  if(myobj != ""){
    if(!isPositiveInteger(myobj)){
      myobj = "";
	  cuadroDialogo("El n&uacute;mero de registros debe ser num&eacute;rico",3);
	  return false;
    }
  }
  return true;
}

function checkValueCon3(myobj)
{
  if(myobj != "")
  {
    if(!isPositiveInteger(myobj))
    {
      myobj = "";
      document.MantNomina.secuencia.focus();
	  cuadroDialogo("El n&uacute;mero de secuencia debe ser num&eacute;rico",3);
	  return false;
   }
  }
 return true;
}

function isPositiveInteger(s)
{   
	var secondArg = true;
   	if (isPositiveInteger.arguments.length > 1)
        secondArg = isPositiveInteger.arguments[1];
    return (isSignedInteger(s, secondArg) && ( (isEmpty(s) && secondArg)  || (parseInt (s) > 0) ) );
}

function isSignedInteger(s)
{
	if (isEmpty(s))
       if (isSignedInteger.arguments.length == 1) 
       		return defaultEmptyOK;
       else 
       		return (isSignedInteger.arguments[1] == true);
    else 
    {
        var startPos = 0;
        var secondArg = true;
        if (isSignedInteger.arguments.length > 1)
            secondArg = isSignedInteger.arguments[1];
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;
        return (isInteger(s.substring(startPos, s.length), secondArg));
    }
}

function isInteger (s)
{   
	var i;
    if (isEmpty(s))
       if (isInteger.arguments.length == 1) 
       		return true;
       else 
       		return (isInteger.arguments[1] == true);
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (!isDigit(c)) 
        	return false;
    }
    return true;
}

function isDigit (c)
{
	return ((c >= "0") && (c <= "9"))
}

function isEmpty(s)
{
	return ((s == null) || (s.length == 0))
}

function WindowCalendar()
{
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calNom22.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}

function WindowCalendarApliInicio()
{
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calNom2.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}

function SeleccionaFechaApli(ind)
 {
   var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/calNom3.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
   msg.focus();
 }

function Actualiza()
{
     document.MantNomina.fecha2.value=Fecha[Indice];
}

function WindowCalendar1()
{
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calNom1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}

var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;


function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function actualizacuenta()
{
  document.MantNomina.textcuenta.value=ctaselec;
  document.MantNomina.cuenta.value=ctaselec+" "+ctadescr;
}

function EnfSelCta()
{
   document.MantNomina.textcuenta.value="";
}

	<%
		if(session.getAttribute("tipoBusqueda")=="IE")
			session.setAttribute("tipoBusqueda","PI");
		else
			session.setAttribute("tipoBusqueda","IE");	
	%>

function reload()
{
	var i;
	for(i=0; i<document.MantNomina.opcionbus.length; i++)
	{
		if(document.MantNomina.opcionbus[i].checked)
		{
			if(original != document.MantNomina.opcionbus[i].value)
				window.location.reload();	
			else
				return;	
		}
	}
}

function echeck(str)
{
  var at="@";
  var dot=".";
  var lat=str.indexOf(at);
  var lstr=str.length;
  var ldot=str.indexOf(dot);

  if (str.indexOf(at)==-1)
     return false;
  if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
     return false;
  if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
      return false;
  if (str.indexOf(at,(lat+1))!=-1)
      return false;
  if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
      return false;
  if (str.indexOf(dot,(lat+2))==-1)
      return false;
  if (str.indexOf(" ")!=-1)
      return false;
  return true;
}
function validaMail(campo){
	return campo.value.match(/^[0-9A-Za-z][A-Za-z0-9_.-]*@[A-Za-z0-9_-]+\.[A-Za-z0-9_.]+[A-za-z]$/);
}
function validaEmail()
{
  document.MantNomina.CorreoE.value=trimString(document.MantNomina.CorreoE.value);
  var emailID=document.MantNomina.CorreoE;

  if( trimString(emailID.value)!="")
  {
	  if (!validaMail(emailID))
	  {
		emailID.focus();
		cuadroDialogo("El correo especificado no es v&aacute;lido.", 3);
		return false;
	  }
  }
  else
  {
      cuadroDialogo( "Es necesario incluir el correo para env&iacute;o de detalle" ,3);
     return false;

  }
  return true;
}

function js_descargaArchivo(Nombre)
{
	document.MantNomina.operacion.value="descarga";
	document.MantNomina.action="NomPreImportOpciones";
	document.MantNomina.submit();
}

function js_enviarCorreo()
{
   var seleccionado;
   var procesado;
   var cad="";
   var i=0;

   cad="";
   procesado=false;
   seleccionado=false;

   for(i=0;i<(document.MantNomina.elements.length);i++)
	 {
	   if(document.MantNomina[i].type=="radio")
		 if(document.MantNomina[i].checked)
		 {
		   seleccionado=true;
		   if(document.MantNomina[i].value.indexOf("Procesado")>0)
		    {
			  if(validaEmail())
				{
			      cad=document.MantNomina[i].value;
				  procesado=true;
				}

			  break;
		    }
		   else
			  cuadroDialogo("Esta opci&oacute;n solo aplica para los archivos Procesados.", 3);
		 }
	 }

	if(seleccionado)
	 {
	   if(procesado)
		 {
		   cad=cad.substring(0,cad.indexOf("|"));
		   document.MantNomina[i].value=cad;
		   document.MantNomina.operacion.value="enviarCorreo";
		   document.MantNomina.action="NomPreImportOpciones";
		   document.MantNomina.submit();
		 }
	 }
	else
	  cuadroDialogo("Debe seleccionar un registro para acceder a esta opci&oacute;n", 3);
}

var respuesta=0;

function continua()
{
  if(respuesta==1)
	{
      cad=cad.substring(0,cad.indexOf("|"));
	  document.MantNomina[elemento].value=cad;
	  document.MantNomina.operacion.value="enviarCorreo";
	  document.MantNomina.action="NomPreImportOpciones";
	  document.MantNomina.submit();
	}
}

function js_cancelar()
 {
  var seleccionado=false;
  var cancelado=false;
  var cad="";
  var i=0;

	for(i=0;i<(document.MantNomina.elements.length);i++)
	 {
		if (document.MantNomina[i].type=="radio")
		 if (document.MantNomina[i].checked==true)
		 {
		   seleccionado=true;
		   if (document.MantNomina[i].value.indexOf("Procesado")<0)
			{
			  cancelado=true;
			  cad=document.MantNomina[i].value;
			  break;
			}
		   else
			  cuadroDialogo("Esta opci&oacute;n solo aplica para los archivos Recibidos.", 3);
		 }
	 }

	if (seleccionado)
	 {
	   if(cancelado)
		 {
			cad=cad.substring(0,cad.indexOf("|"));
			document.MantNomina[i].value=cad;
			document.MantNomina.operacion.value="cancelar";
			document.MantNomina.tipoArchivo.value="cancelado";
			document.MantNomina.action="NomPreImportOpciones";
			document.MantNomina.submit();
		 }
	 }
	else
     cuadroDialogo("Debe seleccionar un registro para acceder a esta opci&oacute;n", 3);
}


function js_regresar(){
		document.MantNomina.operacion.value="regresar";
		document.MantNomina.action="NomPreImportOpciones";
		document.MantNomina.submit();
}

function atras()
{
    if((parseInt(document.MantNomina.prev.value) - Nmuestra)>0){
      document.MantNomina.next.value = document.MantNomina.prev.value;
      document.MantNomina.prev.value = parseInt(document.MantNomina.prev.value) - Nmuestra;
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
    }
}

function adelante()
{
 if(parseInt(document.MantNomina.next.value) != parseInt(document.MantNomina.total.value))
 {
    if((parseInt(document.MantNomina.next.value) + Nmuestra)<= parseInt(document.MantNomina.total.value))
    {
      document.MantNomina.prev.value = document.frmbit.next.value;
      document.MantNomina.next.value = parseInt(document.frmbit.next.value) + Nmuestra;
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
     }else if((parseInt(document.MantNomina.next.value) + Nmuestra) > parseInt(document.MantNomina.total.value))
     {
      document.MantNomina.prev.value = document.MantNomina.next.value;
      document.MantNomina.next.value = parseInt(document.MantNomina.next.value) + (parseInt(document.MantNomina.total.value) - parseInt(document.MantNomina.next.value));
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
     }
 }else
 {
   cuadroDialogo("No hay m&aacute;s registros.",3);
 }
}

function validaNumero(e) {
		var key = window.event ? e.keyCode : e.which;
		if (key == 8) return true;
		return /\d/.test(String.fromCharCode(key));
	}
function cambiaEmpleado()
{
	var f=document.MantNomina;
	var cadena=f.empleado.value;
	var folioReturn = "";	
	if(cadena.length>0){	
		var longitud = cadena.length;
		if (longitud < 7)
			for (i = 1; i <= 7 - longitud; i++)
				folioReturn += "0";
		folioReturn += cadena;
		f.empleado.value=folioReturn;
			
	}	
}
</script>

<form name = "Frmfechas" action="">
<%
  if (request.getAttribute("Bitfechas")!= null)
    out.println(request.getAttribute("Bitfechas")); %>
</form>

<FORM  NAME="MantNomina" METHOD="Post" ACTION="MantNomina">

   <table width="760" border="0" cellspacing="0" cellpadding="0">
   <%
	if (request.getAttribute("ContenidoArchivo")== "")
	{
   %>
    <tr>
	 <td align="center">
	    <table border="0" cellspacing="2" cellpadding="3">
		 <tr>
		   <td class="tittabdat" colspan="2" nowrap> Seleccione los datos:</td>
		 </tr>
		 <tr align="center">
		   <td class="textabdatcla" valign="top" colspan="2">
			<table width="660" border="0" cellspacing="0" cellpadding="0">
			 <tr valign="top">
			  <td width="420" align="left">

               <table width="420" border="0" cellspacing="0" cellpadding="5">
			    <tr>
					<td align="right" class="tabmovtex" nowrap>Tipo de busqueda</td>
					<td class="tabmovtex" nowrap>
						<%if(session.getAttribute("tipoBusqueda")==null || session.getAttribute("tipoBusqueda")=="IE"){ %>
							<input type="radio" value="IE" name="opcionbus" onclick="reload()" checked="checked"/>Importaci&oacute;n env&iacute;o
							<input type="radio" value="PI" name="opcionbus" onclick="reload()"/>Pago individual
						<%}else{ %>
							<input type="radio" value="IE" name="opcionbus" onclick="reload()"/>Importaci&oacute;n env&iacute;o
							<input type="radio" value="PI" name="opcionbus" onclick="reload()" checked="checked"/>Pago individual
						<%} %>
					</td>
				</tr>
			    <tr>
				 <td align="right" class="tabmovtex" nowrap>Cuenta de cargo:</td>
				 <td class="tabmovtex" nowrap>

				 <input type="text" name=textcuenta class="tabmovtexbol" maxlength=30 size=30  value="" onfocus="blur();"/>
				 &nbsp;
				 <a href="javascript:PresentarCuentas();">
				 <img src="../../gifs/EnlaceMig/gbo25420.gif" border=0 style="vertical-align:middle;">
				 </a>
				 <input type="hidden" name="cuenta" value=""/>

				 </td>
				</tr>
				<tr>
                 <td align="right" class="tabmovtex">Por fecha de envio:<br></td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha1" size="12" class="tabmovtex" onfocus="blur();"><a href="javascript:WindowCalendar();"><img src="../../gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Por fecha de aplicaci&oacute;n:</td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha2" size="12" class="tabmovtex" onfocus="blur();"><a href="javascript:SeleccionaFechaApli(0);"><img src="../../gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Importe:</td>
				 <td class="tabmovtex" nowrap><input type="text" name="Importe" size="15" maxlength=15 class="tabmovtex"></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Secuencia:</td>
				 <td class="tabmovtex" nowrap><input type="text" name="secuencia" size="15" maxlength=12 class="tabmovtex" onkeypress="return validaNumero(event);"></td>
				</tr>
				<%if(session.getAttribute("tipoBusqueda")!= null && session.getAttribute("tipoBusqueda").equals("PI")){ %>
			   		<tr>
			   			<td align="right" class="tabmovtex">No de empleado</td>
						<td class="tabmovtex"><input type="text" value="" size="10" maxlength="<%=NomPreEmpleado.LNG_NO_EMPLEADO%>" class="tabmovtex" name="empleado" onkeypress="return validaNumero(event);" onblur="cambiaEmpleado();"/></td>
					</tr>
					<tr>
						<td align="right" class="tabmovtex">No de tarjeta:</td>
						<td class="tabmovtex"><input type="text" class="tabmovtex" size="25" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" name="tarjeta" onkeypress="return validaNumero(event);"/></td>
					</tr>
				<%}else{ %>
					<INPUT TYPE="hidden" VALUE="" NAME="empleado">
					<INPUT TYPE="hidden" VALUE="" NAME="tarjeta">
				<%} %>
			   </table>

              </td>
			  <td width="240" align="left">

			   <table width="240" border="0" cellspacing="0" cellpadding="3">
			    <tr>
				 <td align="right" class="tabmovtex" width="107" nowrap>Estatus:</td>
				 <td class="tabmovtex" width="15" valign="middle" align="center"><input type="checkbox" name="ChkRecibido" value="R"></td>
				 <td class="tabmovtex" width="100" nowrap>Recibidas</td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="ChkCancelado" value="C"></td>
				 <td class="tabmovtex" nowrap>Canceladas </td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="ChkEnviado" value="E"></td>
				 <td class="tabmovtex" nowrap>En proceso</td>
				</tr>
			    <tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="ChkPagado" value="P"></td>
				 <td class="tabmovtex" nowrap>Procesadas</td>
				</tr>
			   </table>

			  </td>
			 </tr>
			</table>
		   </td>
          </tr>
        </table>

        <br>
        <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="height:22">
         <tr>
          <td align="right" valign="top" height="22" width="90"><a href="javascript:js_consultar();"><img border="0" name=Enviar src="../../gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"></a></td>
		  <%if(session.getAttribute("tipoBusqueda")!= null && session.getAttribute("tipoBusqueda")=="PI"){ %>
		  	<td align="left" valign="middle" height="22" width="76"><a href="javascript:FrmClean()"><img name=limpiar border="0" src="../../gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a></td>
		  <%}else{ %>
		  	<td align="left" valign="middle" height="22" width="76"><a href="javascript:FrmClean1()"><img name=limpiar border="0" src="../../gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a></td>
		  <%} %>
		 </tr>
        </table>
	 </td>
    </tr>

   <%}%>

        <%
			if (request.getAttribute("ContenidoArchivo")!= null)
					out.println(request.getAttribute("ContenidoArchivo"));
		%>

		   <%
			if(request.getAttribute("ContenidoArchivo")!= "" && session.getAttribute("tipoBusqueda").equals("PI"))
			{
		   %>

		   <table align=center>
			<tr>
			 <td class="tabmovtex"  align=center nowrap>
			   <br>Correo: <input type=text name=CorreoE maxlength="<%=NomPreEmpleado.LNG_CORREO_ELECTRONICO%>" size=25><br>
			 </td>
			</tr>
		   </table>
		   <%}%>
		<%
		if (request.getAttribute("botones")!= null)
		 {
			out.println(request.getAttribute("botones"));
		 }
		%>
  </table>

  <INPUT TYPE="hidden" VALUE="0" NAME="operacion">
  <INPUT TYPE="hidden" VALUE="0" NAME="tipo">
  <INPUT TYPE="hidden" VALUE="-1" NAME="registro">
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("fechaHoy") !=null)
									out.println( session.getAttribute("fechaHoy"));
								else
									if (request.getAttribute("fechaHoy")!= null)
										out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy">
  <INPUT TYPE="hidden" NAME="nuevoArchivo">
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("tipoArchivo") !=null)
									out.println( session.getAttribute("tipoArchivo"));
								else
									if (request.getAttribute("tipoArchivo")!= null)
										out.println(request.getAttribute("tipoArchivo"));
							%>"  NAME="tipoArchivo">
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("horaSistema") !=null)
									out.println( session.getAttribute("horaSistema"));
								else
									if (request.getAttribute("horaSistema")!= null)
										out.println(request.getAttribute("horaSistema"));
							%>" NAME="horaSistema">

  <input type="hidden" name="archivoEstatus" value='<%
								if(	session.getAttribute("archivoEstatus") !=null)
									out.println( session.getAttribute("archivoEstatus"));
								else
									if (request.getAttribute("archivoEstatus")!= null)
										out.println(request.getAttribute("archivoEstatus"));
							%>'>

  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("cantidadDeRegistrosEnArchivo")!= null)
								    out.println(request.getAttribute("cantidadDeRegistrosEnArchivo"));
								else
								if(session.getAttribute("cantidadDeRegistrosEnArchivo") !=null)
									out.println( session.getAttribute("cantidadDeRegistrosEnArchivo"));


							%>" NAME="cantidadDeRegistrosEnArchivo">
  <INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("nombre_arch_salida")!= null)
				    out.println(request.getAttribute("nombre_arch_salida"));
			    %>" NAME="nombre_arch_salida">
  <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "lista_nombres_archivos" ) != null ) { out.print( request.getAttribute( "lista_nombres_archivos" ) ); } %>" NAME="lista_nombres_archivos">
  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("fechaHoy")!= null)
								out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy">

<%
	if(	session.getAttribute("facultadImportar") !=null)
		out.println( session.getAttribute("facultadImportar"));
	else
	if (request.getAttribute("facultadImportar")!= null) {
		out.println(request.getAttribute("facultadImportar"));
	}
%>
<%
	if(	session.getAttribute("facultadAlta") !=null)
		out.println( session.getAttribute("facultadAlta"));
	else
	if (request.getAttribute("facultadAlta")!= null) {
		out.println(request.getAttribute("facultadAlta"));
	}
%>
<%
	if(	session.getAttribute("facultadCambios") !=null)
		out.println( session.getAttribute("facultadCambios"));
	else
	if (request.getAttribute("facultadCambios")!= null) {
		out.println(request.getAttribute("facultadCambios"));
	}
%>
<%
	if(	session.getAttribute("facultadBajas") !=null)
		out.println( session.getAttribute("facultadBajas"));
	else
	if (request.getAttribute("facultadBajas")!= null) {
		out.println(request.getAttribute("facultadBajas"));
	}
%>
<%
	if(	session.getAttribute("facultadEnvio") !=null)
		out.println( session.getAttribute("facultadEnvio"));
	else
	if (request.getAttribute("facultadEnvio")!= null) {
		out.println(request.getAttribute("facultadEnvio"));
	}
%>
<%
	if(	session.getAttribute("facultadRecuperar") !=null)
		out.println( session.getAttribute("facultadRecuperar"));
	else
	if (request.getAttribute("facultadRecuperar")!= null) {
		out.println(request.getAttribute("facultadRecuperar"));
	}
%>
<%
	if(	session.getAttribute("facultadExportar") !=null)
		out.println( session.getAttribute("facultadExportar"));
	else
	if (request.getAttribute("facultadExportar")!= null) {
		out.println(request.getAttribute("facultadExportar"));
	}
%>
<%
	if(	session.getAttribute("facultadBorra") !=null)
		out.println( session.getAttribute("facultadBorra"));
	else
	if (request.getAttribute("facultadBorra")!= null) {
		out.println(request.getAttribute("facultadBorra"));
	}
%>
<script>
<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
		if (request.getAttribute("infoUser")!= null) {
			out.println(request.getAttribute("infoUser"));
		}
%>
</script>
</form>
<jsp:include page="footerPrepago.jsp"/>
