<HTML>
<HEAD><TITLE>Banca Virtual</TITLE>

<script language = "JavaScript" SRC= "/EnlaceMig/ValidaFormas.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>

<script language = "JavaScript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/consultas_on.gif','images/transfer_on.gif','images/inver_on.gif','images/util_on.gif','images/ayuda_on.gif','images/final_on.gif')">

<CENTER>
<FORM  NAME="frmConsulta" method=post>
<!--ASTECI 8-NOV-2004, MX-2004-120 Fusion Contable SUA-->
<%
    String Sfecha_pago = (String)request.getAttribute("txtFechaPago");
    String Sanio_fecha_pago = Sfecha_pago.substring(6,10);
    int Ianio_fecha_pago = Integer.parseInt (Sanio_fecha_pago);
%>

	<TABLE BORDER="0" CELLSPADING="0" WIDTH="95%">
	 <tr>
	  <!--	Imagen dinamica //MPP 241103 Logo Santander (014)/ Serfin (003)//-->
		      <% if(request.getAttribute("ClaveBanco")!=null)
		       {
		         if(Ianio_fecha_pago < 2005)
		          { %>
			     <%if(request.getAttribute("ClaveBanco").equals("014")) {%>
				      <td valign="middle" class="titpag" align="left">
				       <IMG SRC="/gifs/EnlaceMig/glo25040.gif" BORDER=0 ALT="">
        </td>
				<%} else %>
				<%if(request.getAttribute("ClaveBanco").equals("003"))
				  {%>
				      <td valign="middle" class="titpag" align="left">
				       <IMG SRC="/gifs/EnlaceMig/glo25040s.gif" BORDER=0 ALT="">
        </td>
				<%}%>
			  <%}else
			    {%>
			              <td valign="middle" class="titpag" align="left">
			   	       <IMG SRC="/gifs/EnlaceMig/glo25040b.gif" BORDER=0 ALT="">
			   	       </td>
			   <%}%>
		       <%}  //MPP Termina %>
	 </TR>
	 <tr>
	  <td align=center><B>Comprobante de Transferencia de Pago de Cuotas,<BR>
	       Aportaciones y Amortizaciones de Cr&eacute;ditos<BR>
	       V&iacute;a Enlace Internet<B></TD>
	  <td><img src="/gifs/EnlaceMig/logo_imss.gif"></TD>
	  <td><img src="/gifs/EnlaceMig/logo_info.gif"></TD>
	 </tr>
	</TABLE>

	<TABLE BORDER="1" CELLSPACING="0" WIDTH="95%">
	<tr>
		<td colspan=4><font size=2>NOMBRE O RAZ&Oacute;N SOCIAL<BR></font>
		    <%
			  if (request.getAttribute("txtNomRazSoc")!= null) {
			  out.println(request.getAttribute("txtNomRazSoc"));
			      }
		    %>
	    </TD>
		<td width=18%><font size=2>REGISTRO PATRONAL<BR></font>
		    <%
		   if (request.getAttribute("txtRegPatronal")!= null) {
		   out.println(request.getAttribute("txtRegPatronal"));
		       }
	    %>

		<td width=12% align=center><font size=2><B>IMSS</B></TD>
		<td width=10%><font size=2>MES<BR></font>
		<%
		if (request.getAttribute("txtImssPeriodo")!= null) {
		out.println(request.getAttribute("txtImssPeriodo"));
		    }
	%>
		</TD>
		<td width=10%><font size=2>A&Ntilde;O<BR></font>

		    <%
			  if (request.getAttribute("txtImssYear")!= null) {
			  out.println(request.getAttribute("txtImssYear"));
			      }
	    %>
	    </TD>
	</TR>
	<tr>
		<td width="14%"><font size=1>R.F.C<BR></font>

		    <%
			  if (request.getAttribute("txtRFC")!= null) {
			  out.println(request.getAttribute("txtRFC"));
			      }
	    %>
	    </TD>
		<td width="9%"><font size=1>FOLIO/SUA<BR></font>

		    <%
			  if (request.getAttribute("txtFolio")!= null) {
			  out.println(request.getAttribute("txtFolio"));
			      }
	    %>
	    </TD>


				<td width="10%"><font size=1>VERSION<BR></font>

		    <%
			  if (request.getAttribute("txtVersionSua")!= null) {
			  out.println(request.getAttribute("txtVersionSua"));
			      }
	    %>
		    </TD>



		<td width="17%" align=center><font size=1>NO. DE TRABAJADORES<BR></font>

		    <%
			  if (request.getAttribute("txtNumTrabaja")!= null) {
			  out.println(request.getAttribute("txtNumTrabaja"));
			      }
		 %>
	    </TD>
		<td align=center><font size=1>NO. TRAB. CON CRED. VIV.<BR></font>

		    <%
			  if (request.getAttribute("txtNumTrabajaCred")!= null) {
			  out.println(request.getAttribute("txtNumTrabajaCred"));
			      }
	    %>
	    </TD>
		<td><TABLE BORDER=0 WIDTH=100%>
		     <tr>

		       <td WIDTH=65%><font size=1>
	           <div align="center">RCV</div></TD>
		     </TR>
		     <tr>

		       <td><div align="center"><font size=1>INFONAVIT</font></div></TD>
		     </TR>
		    </TABLE>
		<td><font size=1>BIMESTRE<BR></font>
		    <%
		    if (request.getAttribute("txtSarBimestre")!= null) {
		    out.println(request.getAttribute("txtSarBimestre"));
			}
	    %>
	    </TD>
		<td><font size=1>A&Ntilde;O<BR></font>

		    <%
		    if (request.getAttribute("txtSarYear")!= null) {
		    out.println(request.getAttribute("txtSarYear"));
			}
	    %>
	    </TD>
	</TR>
	</TABLE>
	<TABLE BORDER="1" CELLSPACING="0" WIDTH="95%">
	 <tr>
		<th colspan=2 class="tittabdat">PARA ABONO EN CUENTA DEL<BR>IMSS</TH>
		<th colspan=2 class="tittabdat">PARA ABONO EN CUENTA DEL<BR>AFORE</TH>
		<th colspan=2 class="tittabdat">PARA ABONO EN CUENTA DEL<BR>INFONAVIT</TH>
	 </TR>
	 <tr>
		<th WIDTH="14%">CONCEPTO</TH>
		<th WIDTH="17%">IMPORTE</TH>
		<th WIDTH="14%">CONCEPTO</TH>
		<th WIDTH="17%">IMPORTE</TH>
		<th WIDTH="19%">CONCEPTO</TH>
		<th WIDTH="19%">IMPORTE</TH>
	 </TR>
	 <tr>
		<td align=right height=40><font size=1>CUOTAS 4 SEGUROS</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtImssImpSeg")!= null) {
		    out.println(request.getAttribute("txtImssImpSeg"));
			}
	 %>
		</TD>
		<td align=right><font size=1>RETIRO CESANTIA Y VEJEZ</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtAfoImpRCV")!= null) {
		    out.println(request.getAttribute("txtAfoImpRCV"));
			}
	 %>
		</TD>
		<td align=right><font size=1>APORTACI&Oacute;N DE VIVIENDA PARA CUENTA INDIVIDUAL</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtInfImpApo")!= null) {
		    out.println(request.getAttribute("txtInfImpApo"));
			}
	 %>
		</TD>
	</TR>
	<tr>
		<td align=right height=40><font size=1>ACTUALIZACI&Oacute;N</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtImssImpAct")!= null) {
		    out.println(request.getAttribute("txtImssImpAct"));
			}
	%>
		</TD>
		<td align=right><font size=1>ACTUALIZACI&Oacute;N</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtAfoImpAct")!= null) {
		    out.println(request.getAttribute("txtAfoImpAct"));
			}
	%>
		</TD>
		<td align=right><font size=1>APORTACIONES DE VIVIENDA <BR>PARA AMORT. DE CR&Eacute;DITO</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtInfImpAct")!= null) {
		    out.println(request.getAttribute("txtInfImpAct"));
			}
	%>
		</TD>
	 </TR>
	 <tr>
		<td align=right height=40><font size=1>RECARGOS MORATORIOS</TD>
		<td align=right ><font size=1>
		<%
		    if (request.getAttribute("txtImssImpRec")!= null) {
		    out.println(request.getAttribute("txtImssImpRec"));
			}
	%>
		</TD>
		<td align=right ><font size=1>RECARGOS MORATORIOS</TD>
		<td align=right ><font size=1>
		<%
		    if (request.getAttribute("txtAfoImpRec")!= null) {
		    out.println(request.getAttribute("txtAfoImpRec"));
			}
	%>
		</TD>

		<td align=right><font size=1>AMORTIZACI&Oacute;N DE CR&Eacute;DITO</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtInfAmortCred")!= null) {
		    out.println(request.getAttribute("txtInfAmortCred"));
			}
	%>




	 </TR>
	 <tr>
		<td height=40></TD>
		<td></TD>
		<td align=right><b><font size=2>SUBTOTAL</B></TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtAfoSubTotal")!= null) {
		    out.println(request.getAttribute("txtAfoSubTotal"));
			}
	%>
		</TD>




		<td align=right><font size=1>ACTUALIZACI&Oacute;N</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtInfActAmortCred")!= null) {
		    out.println(request.getAttribute("txtInfActAmortCred"));
			}
	%>
		</TD>
		<td width="0%"></td>



	 <tr>
		<td height=40></TD>
		<td></TD>
		<td align=right><font size=1>APORTACIONES <BR>VOLUNTARIAS</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtAfoImpAV")!= null) {
		    out.println(request.getAttribute("txtAfoImpAV"));
			}
	%>
		</TD>

				<td align=right><font size=1>RECARGOS MORATORIOS</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtInfRecAmortCred")!= null) {
		    out.println(request.getAttribute("txtInfRecAmortCred"));
			}
	%>

		</TD>
	 </TR>
	 <tr>
		<td height=40></TD>
		<td></TD>


		<td align=right><font size=1>APORTACIONES <BR>COMPLEMENTARIAS</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtAportacionesComp")!= null) {
		    out.println(request.getAttribute("txtAportacionesComp"));
			}
	%>
		</TD>




		<td align=right><font size=1>MULTAS</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtMultasInfonavit")!= null) {
		    out.println(request.getAttribute("txtMultasInfonavit"));
			}
	%>
		</TD>
	 </TR>
	 <tr>
		<td height=40></TD>
		<td></TD>
		<td></TD>

		<td colspan = 2 align=right><font size=1>DONATIVO A FUNDACION DEL EMPRESARIADO DE MEXICO, A.C.</TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtDonativofundemex")!= null) {
		    out.println(request.getAttribute("txtDonativofundemex"));
			}
	%>
		</TD>
	 </TR>
	 <tr>
		<td align=right><font size=2><b>TOTAL</B></TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtImssTotal")!= null) {
		    out.println(request.getAttribute("txtImssTotal"));
			}
	%>
		</TD>
		<td align=right><font size=2><b>TOTAL</B></TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtAfoTotal")!= null) {
		    out.println(request.getAttribute("txtAfoTotal"));
			}
	%>
		</TD>
		<td align=right><font size=2><b>TOTAL</B></TD>
		<td align=right><font size=1>
		<%
		    if (request.getAttribute("txtInfTotal")!= null) {
		    out.println(request.getAttribute("txtInfTotal"));
			}
	%>
		</TD>
	 </TR>
	 <tr>
		<td colspan=2 height=50></TD>
		<td colspan=2></TD>
		<td align=right><b>TOTAL A PAGAR</B></TD>
		<td align=right>
		<%
		    if (request.getAttribute("txtGranTotal")!= null) {
		    out.println(request.getAttribute("txtGranTotal"));
			}
	%>
		</TD>
	 </TR>
	 <tr>
	 <td colspan=2></td>
		<td colspan=2><font size=2>REFERENCIA BANCO:<BR></font>

		   <%
		    if (request.getAttribute("txtReferencia")!= null) {
		    out.println(request.getAttribute("txtReferencia"));
			}
	   %>
	    </TD>
		<td colspan=2><font size=1>OPERACI&Oacute;N DE CAJA Y/O SELLO DE LA ENTIDAD RECEPTORA<BR></font>
			      <font size=2><B>FECHA DE PAGO  : </B>
			      <%
			   if (request.getAttribute("txtFechaPago")!= null) {
			   out.println(request.getAttribute("txtFechaPago"));
			       }
		      %>
        </TD>
	</TR>
	<tr>
			<td colspan = 6><b> ESTE DOCUMENTO CONJUNTAMENTE CON EL ORIGINAL DE SU ESTADO DE CUENTA, CONSTITUYEN SU COMPROBANTE DE PAGO. </b></TD>
</tr>
  </TABLE>

	  <table border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td align="right" width="83">
	      <a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0" alt="Imprimir"></a>
	    </td>
	    <td align="left" width="71">
	      <a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0" alt="Cerrar"></a>
	    </td>
	  </tr>
	</table>

  </FORM>
</CENTER>

</BODY>
</HTML>
