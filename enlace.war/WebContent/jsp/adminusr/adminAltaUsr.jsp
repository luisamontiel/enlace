<html>
<head>
	<title>Administración Super Usuario - Alta Usuario</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/util.js"></script>
<script type="text/javascript" src="/EnlaceMig/pm_fp.js"></script>
<script language="javaScript">

<%
if(request.getAttribute("error") != null) {
%>

	cuadroDialogoEspecial("<%=request.getAttribute("error")%>",3);

<%
}
%>
var defaultEmptyOK = false;

function isInteger (s) {   
	var i;
    if (isEmpty(s))
       if (isInteger.arguments.length == 1) 
       		return defaultEmptyOK;
       else 
       		return (isInteger.arguments[1] == true);
       		
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (!isDigit(c)) return false;
    }
    
    return true;
}

function isDigit (c) {
	return ((c >= "0") && (c <= "9"));
}

function isEmpty(s) {
	return ((s == null) || (s.length == 0));
}

function  validaUsr (user)
{
	// Valida que sea numerico
	result=isInteger (user)
	if (!isInteger (user))	{
		document.adminuser.codCliente.focus()
		cuadroDialogo ("El C&oacute;digo de Cliente Debe Ser Num\351rico.", 1)
		return false
	}

	// regresa false si el campo esta en blanco 
	if (user == "")	{
		document.adminuser.codCliente.focus()
		cuadroDialogo ("\n Favor de teclear su C&oacute;digo de Cliente. ", 1)
		result = false
	} else {
		// la  de usuario debe ser de 8 digitos
	      if (user.length != 8) {
	        //completeUserID()
	    	  for (ceros = 0; ceros = (8 - user.length); ceros = ceros + 1) {
	    		  user = "0" + user;
	    	  }
	    	  document.adminuser.codCliente.value = user;
	        return true
	      }
	      else
	        result = true
	}
  return result
}

function oculta() {
	document.getElementById("enviar").style.visibility="hidden";
	document.getElementById("enviar2").style.visibility="visible";
}

function muestra() {
	document.adminuser.codCliente.value = "";
	document.getElementById("enviar2").style.visibility="hidden";
	document.getElementById("enviar").style.visibility="visible";
}

 function valida(){
	if (validaUsr(document.adminuser.codCliente.value)) {
		document.adminuser.action="AdmonUsuarioServlet"; 
		document.adminuser.submit();
	} else {
		muestra();
	}
 }

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body background="//gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" onload="obtenDatosBrowser();">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>


 <FORM   NAME="adminuser" METHOD="Post" ACTION="javascript:valida();" onSubmit=oculta()>
 
 	<input type="hidden" name="Operacion" value="alta"/>
 	<input type="hidden" name="JSON"      value=""/>
 	<input type="hidden" name="Origen"    value="/jsp/adminusr/adminAltaUsr.jsp"/>
 	<input type="hidden" name="Destino"   value="/jsp/adminusr/detalleAltaUsr.jsp"/>
 	<input type="hidden" name="Error"     value="/jsp/adminusr/error.jsp"/>
 	<input type="hidden" name="VistaMostrar" id="VistaMostrar"        value="detalle"/>
 	<input type="hidden" id="datosBrowser" name="datosBrowser" value=""/>
 	<table width="760" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="center">
 				<table>
					<tr>
						<td class="tittabdat" colspan="2"><center>Alta de usuario</center></td>
					</tr>
					<tr>
						<td class="tabmovtex11">&nbsp;&nbsp;Codigo de Cliente</td>
						<td class="tabmovtex"><INPUT TYPE=text class="componentesHtml" SIZE=10 MAXLENGTH=8	NAME=codCliente onFocus=""></td>
					</tr>
			        <tr align="center"  id="enviar2" style="visibility:hidden" class="tabmovtex">
						<td colspan="2">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
					</tr>
					<tr id="enviar">
						<td colspan="2" align="center"><A href = "javascript:valida();" onClick=oculta()><img src="/gifs/EnlaceMig/gbo25480.gif" border=0></A></td>
					</tr>
 				</table>
 			</td>
 		</tr>
 	</table>

 </FORM>

</body>
</html>