<%@page contentType="text/html"%>
<html>
<head><title>JSP Page</title></head>
<body>

<jsp:useBean id="Matriz" scope="request" class="java.util.HashMap" />
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>

<%
    java.util.HashMap Enero, Febrero, Marzo;
    Enero = new java.util.HashMap (7);
    Enero.put ("Por Pagar", new Float (10000.65));
    Enero.put ("Pagados", new Float (34512.45));
    Enero.put ("Liberados no pagados", new Float (11200));
    Enero.put ("Anticipados", new Float (500.3));
    Enero.put ("Cancelados", new Float (15000));
    Enero.put ("Vencidos", new Float (4000));
    Enero.put ("Rechazados", new Float (7000));
    Enero.put ("Recibidos", new Float (82213.4));
    Febrero = new java.util.HashMap (7);
    Febrero.put ("Por Pagar", new Float (21));
    Febrero.put ("Pagados", new Float (4));
    Febrero.put ("Liberados no pagados", new Float (8));
    Febrero.put ("Anticipados", new Float (2));
    Febrero.put ("Cancelados", new Float (11));
    Febrero.put ("Vencidos", new Float (7));
    Febrero.put ("Rechazados", new Float (7));
    Febrero.put ("Recibidos", new Float (60));
    Marzo = new java.util.HashMap (7);
    Marzo.put ("Por Pagar", new Float (19));
    Marzo.put ("Pagados", new Float (6));
    Marzo.put ("Liberados no pagados", new Float (6));
    Marzo.put ("Anticipados", new Float (4));
    Marzo.put ("Cancelados", new Float (9));
    Marzo.put ("Vencidos", new Float (9));
    Marzo.put ("Rechazados", new Float (7));
    Marzo.put ("Recibidos", new Float (60));
    Matriz.put ("Enero", Enero);
    Matriz.put ("Febrero", Febrero);
    Matriz.put ("Marzo", Marzo);%>

<jsp:forward page='/jsp/confGenGrafica.jsp'>
    <jsp:param name='vGraficar' value='I'/>
</jsp:forward>
</body>
</html>
