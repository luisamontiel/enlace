<%@ page import="java.lang.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.math.BigDecimal" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="mx.altec.enlace.utilerias.Util"%>
<%@ page import="mx.altec.enlace.bo.RET_ControlConsOper"%>
<jsp:directive.page import="mx.altec.enlace.bo.RET_ControlConsOper"/>

<%@page import="mx.altec.enlace.bo.RET_OperacionVO"%>
<%@page import="mx.altec.enlace.bo.RET_CuentaAbonoVO"%><html>
<head>
	<%
 	   RET_OperacionVO beanOper = (RET_OperacionVO) session.getAttribute("beanOperacionRET");
 	   String muestraTransferir = (String) request.getAttribute("muestraTransferir");
 	   String etiquetaDivisa = (String) request.getAttribute("etiquetaDivisa");
	%>
	<title>Complemento y Liberaci�n de Operaciones </title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<SCRIPT language = "JavaScript">
	// ---------------------------------------------------

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	<%= request.getAttribute("newMenu") %>

	/*Funci�n para verificar que desea proceder al alta*/
	function js_enviar(){
		document.getElementById("enviando").style.visibility="hidden";
		document.getElementById("enviando2").style.visibility="visible";
		cuadroDialogo("�Desea enviar la Transferencia?",2);
	}

	/*Funci�n para verificar que desea proceder al alta*/
	function continua(){
	  if(respuesta==1)
		{
		  document.frmRETLiberaOperDet.action = "RET_LiberaOper";
		  document.frmRETLiberaOperDet.opcion.value = "transferir";
	      document.frmRETLiberaOperDet.submit();
		}else{
		  document.getElementById("enviando").style.visibility="visible";
		  document.getElementById("enviando2").style.visibility="hidden";
		}
	}

	/*Funcion para el boton regresar*/
	function regresar(){
		document.frmRETLiberaOperDet.action = "RET_LiberaOper";
  		document.frmRETLiberaOperDet.opcion.value = "iniciar";
  		document.frmRETLiberaOperDet.submit();
	}
</SCRIPT>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
	<!-- MENU PRINCIPAL -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
		<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</TABLE>
	<%= request.getAttribute("Encabezado") %>
	<br>
	<FORM NAME="frmRETLiberaOperDet" METHOD="POST" ACTION="RET_LiberaOper" >
	<table cellpadding='2' cellspacing='1' width = "80%" align="center">
		<tr>
			<td colspan=4 class ="texenccon"><center><img src="/gifs/EnlaceMig/leyenda_paso_3_de_3.gif" width="250" height="35"></center></td>
		</tr>
	</table>
	<table cellpadding='2' cellspacing='1' width = "80%" align="center">
		<tr>
			<td class='texenccon' colspan="9">
				<br>
			</td>
		</tr>
		<tr>
			<td class="tittabdat" align = "center"><B>Cuenta Cargo</B></td>
			<td class="tittabdat" align = "center"><B>Usuario Registro</B></td>
			<td class="tittabdat" align = "center"><B>Importe de la Operaci�n</B></td>
			<td class="tittabdat" align = "center"><B>Divisa Operante</B></td>
			<td class="tittabdat" align = "center"><B>Contra Divisa</B></td>
			<td class="tittabdat" align = "center"><B>Tipo de Operaci�n</B></td>
			<td class="tittabdat" align = "center"><B>Tipo de Cambio Pactado</B></td>
			<td class="tittabdat" align = "center"><B>Concepto</B></td>
		</tr>
		<%
		if(beanOper!=null){
		%>
			<tr>
				<td class="textabdatobs" align=center><%=beanOper.getCtaCargo() %></td>
				<td class="textabdatobs" align=center><%=beanOper.getUsrRegistro() %></td>
				<td class="textabdatobs" align=center><%=aMoneda(beanOper.getImporteTotOper()) %></td>
				<td class="textabdatobs" align=center><%=beanOper.getDivisaOperante() %></td>
				<td class="textabdatobs" align=center><%=beanOper.getContraDivisa() %></td>
				<td class="textabdatobs" align=center><%=Util.muestraTipoOperUser(beanOper.getTipoOperacion()) %></td>
				<td class="textabdatobs" align=center><%=beanOper.getTcPactado() %></td>
				<td class="textabdatobs" align=center><%=beanOper.getConcepto() %></td>
			</tr>
		<%}%>
	</table>
	<br>
	<table cellpadding='2' cellspacing='1' width = "20%" align="center">
		<tr>
			<td align="center" class="tittabdat" colspan="3"><b>Detalle Abono</b></td>
		</tr>
		<tr>
			<td class="tittabdat" align = "center"><B>Cuenta Abono</B></td>
			<td class="tittabdat" align = "center"><B>Importe Divisa</B></td>
			<td class="tittabdat" align = "center"><B>Importe Abono</B></td>
		</tr>
		<%for(int i=0; i<beanOper.getDetalleAbono().size(); i++){%>
			<tr>
				<td class="textabdatobs" align=center><%=((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getNumCuenta()%></td>
				<td class="textabdatobs" align=center><%=aMoneda(((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getImporteAbono())%></td>
				<td class="textabdatobs" align=center><%=obtenImporteAbonoReal(((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getImporteAbono(),
																					 beanOper.getTipoOperacion(), beanOper.getTcPactado())+ " " + etiquetaDivisa%></td>
			</tr>
		<%}%>
	</table>
	<input type=hidden name=opcion >
	<input type=hidden name=benefSel >
	<br>
	<table id="enviando2" style="visibility:hidden" border=0 align=center cellpadding=0 cellspacing=0>
   		<tr align="center" class="tabmovtex">
   	 		<td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
    	</tr>
    </table>
	<table id="enviando" align=center border=0 cellspacing=0 cellpadding=0>
	  	<tr>
			<td><a href="javascript:regresar();"><img border="0" src="/gifs/EnlaceMig/gbo25320.gif" alt='Regresar'></a></td>
			<%if(muestraTransferir!=null && muestraTransferir.trim().equals("true")){ %>
				<td><a href="javascript:js_enviar();" border = 0><img src= "/gifs/EnlaceMig/gbo25360.gif" border=0 alt='Transferir'></a></td>
			<%} %>
		</tr>
	</table>
	</FORM>
	<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
	%>
</body>
</html>
<%!
	/** Devuelve un texto que representa un valor monetario */
	private String aMoneda(String num){
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return num;
	}

	private String obtenImporteAbonoReal(String importeAbono, String tipoOperacion, String tcPactado) {
		String importeReal = "";
		BigDecimal tc;
		BigDecimal imp;

		try{
			if(tipoOperacion.trim().equals("CPA")){
				tc			= new BigDecimal(tcPactado);
			 	imp			= new BigDecimal(importeAbono);
				importeReal = "" + tc.multiply(imp).setScale(2,BigDecimal.ROUND_HALF_DOWN);
			}else if(tipoOperacion.trim().equals("VTA")){
				importeReal = importeAbono;
			}
		}catch(Exception e){
			importeReal = "0.00";
		}
		return importeReal;
	}
%>