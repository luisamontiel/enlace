<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
	<script type="text/javascript" src= "/EnlaceMig/internacRecibidas.js"></script>

	<title>Transferencias Internacionales</title>

	<script type="text/javascript">
	    function MM_preloadImages() { //v3.0
		  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}

		function MM_swapImgRestore() { //v3.0
		  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}
	</script>
	<script type="text/javascript">
		function MM_findObj(n, d) { //v3.0
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
		}
	</script>
	<script type="text/javascript">
		function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}
	</script>
	<script type="text/javascript">
		function MM_findObj(n, d) { //v4.01
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && d.getElementById) x=d.getElementById(n); return x;
		}
	</script>
	<script type="text/javascript">
		function MM_preloadImages() { //v3.0
		 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}

		function MM_swapImgRestore() { //v3.0
		  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}
	</script>
	<script type="text/javascript">
		function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}

		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}
	</script>


	<script type="text/javascript">
		function exportar(){
			document.location.href = "<%= (String)session.getAttribute("archivoExportarTI") %>";
		}
	</script>

	<script type="text/javascript">
		<%String FechaHoy="";
		 FechaHoy=(String)request.getAttribute("FechaHoy");
		 if(FechaHoy==null)
		    FechaHoy="";%>

		var Indice=0;
	</script>
    <script type="text/javascript">
		<%if(request.getAttribute("VarFechaHoy")!= null)
				out.print(request.getAttribute("VarFechaHoy"));%>

		diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';
	</script>
	<script type="text/javascript">
		<%
			String DiaHoy="";
			if(request.getAttribute("DiaHoy")!= null)
		 		DiaHoy=(String)request.getAttribute("DiaHoy");
		%>

		<%= request.getAttribute("newMenu") %>
	</script>

	<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
	<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script type="text/javascript" src= "/EnlaceMig/cuadroDialogo.js"></script>

	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" style="background-image: url('/gifs/EnlaceMig/gfo25010.gif'); background-color:#FFFFFF; margin-left: 0; margin-top: 0;" marginwidth="0" marginheight="0" >

	<table style="width: 571; border: 0; " cellspacing="0" cellpadding="0">
	  <tr valign="top">
	    <td width="*">${MenuPrincipal}</td>
	  </tr>
	</table>

	<table style="width: 760; border: 0;" cellspacing="0" cellpadding="0">
	  <tr>
	    <td style="width: 676; border: 0;" valign="top" align="left">
	      <table style="width: 666; border: 0;" cellspacing="6" cellpadding="0">
	        <tr>
	          <td width="528" valign="top" class="titpag">
	          	${Encabezado}
	          </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>

	<form name="frmInternacionales" method="post" action="">
	   <table cellspacing="0" cellpadding="0"  style="${codRespuesta eq 'OK' ? 'display: none' : 'display: block'}; margin:0 auto; width: 760px; border: 0;">
	    <tr>
		 <td align="center">
		    <table border="0" cellspacing="2" cellpadding="3">
			 <tr>
			   <td class="tittabdat" colspan="2" nowrap> Indique la Referencia </td>
			 </tr>
			 <tr align="center">
			   <td class="textabdatcla" valign="top" colspan="2">
				<table width="400" border="0" cellspacing="0" cellpadding="0">
				 <tr valign="top">
				  <td width="420" align="left">
					<table>
						<tr>
							<td style="text-align: right;" class="tabmovtexbol" >&nbsp;*&nbsp;Fecha Transferencia:</td>
							<td class="tabmovtex" style="text-align: left;"><input type="text" id="txtFecha" name="txtFecha" size="12" class="tabmovtexbol" readonly="readonly" OnFocus = "blur();" value="${fechaDia}"/>&nbsp;<a href="javascript:calendario(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" style="vertical-align:middle; border: 0; width: 12;  height:14; "/></a></td>
						</tr>
						<tr>
							<td style="text-align: right;" class="tabmovtexbol">&nbsp;*&nbsp;Cuenta:</td>
							<td style="text-align: left;">
								<input type="text" id="textEnlaceCuenta" name="textEnlaceCuenta"  class="tabmovtexbol" maxlength="22" size="22" onfocus="blur();" value=""/>
								<a href="javascript:PresentarCuentas();"><img src="/gifs/EnlaceMig/gbo25420.gif" style="vertical-align: middle; border: 0; width: 12;  height:14; " /></a>
				           		<input type="hidden" id="EnlaceCuenta" readonly="readonly" name="EnlaceCuenta" value=""/>
							</td>
						</tr>
						<tr>
							<td style="text-align: right;" class="tabmovtexbol" >&nbsp;*&nbsp;Referencia:</td>
							<td style="text-align: left;"><input type="text" id="txtReferencia" name="txtReferencia" size="12" class="tabmovtexbol"  maxlength="9"/></td>
						</tr>
					</table>
	              </td>
				 </tr>
				</table>
			   </td>
	          </tr>
	        </table>
	        <br />
			<table style="width: 200; height: 25; border: 0; background-color: #FFFFFF;" cellspacing="0" cellpadding="0">
				<tr id="enviar">
					<td style="text-align: right; height: 25; border: 0;"><a href="javascript:consultar();"><img name="enviar" src="/gifs/EnlaceMig/gbo25220.gif" alt="Consultar" style="border: 0; width: 90; height: 22;" /></a></td>
					<td style="text-align: left; height: 25; border: 0;"><a href="javascript:limpiar();"><img name="limpiar" src="/gifs/EnlaceMig/gbo25250.gif"  alt="Limpiar" style="border: 0; width: 76; height: 22;" /></a></td>
			    </tr>
			</table>
		 </td>
	    </tr>
	  </table>

	 <br />
		<c:if test="${codRespuesta eq 'OK'}">
			<div style=" width: 950px; text-align:center;">
				<table style="margin-left: auto; margin-right:auto " >
				  	<tr>
						<td align="left" class="tabmovtex" colspan="11">
							<b> Resultado de la consulta para la cuenta:&nbsp;${resultadoConsulta.cuentaDestino}&nbsp;${resultadoConsulta.nomCtaDestino}</b><br/>
						</td>
					</tr>
					<tr>
						<td style="text-align: center; width: 60;"  class="tittabdat">N&uacute;mero de referencia</td>
						<td style="text-align: center; width: 100;" class="tittabdat">Cuenta Ordenante</td>
						<td style="text-align: center; width: 150;" class="tittabdat">Nombre del Ordenante</td>
						<td style="text-align: center; width: 60;" class="tittabdat">Banco Ordenante</td>
						<td style="text-align: center; width: 60;" class="tittabdat">Pa&iacute;s del Ordenante</td>
						<td style="text-align: center; width: 60;" class="tittabdat">Ciudad del Ordenante</td>
						<td style="text-align: center; width: 100;"  class="tittabdat">Importe</td>
						<td style="text-align: center; width: 50;"  class="tittabdat">Divisa</td>
						<td style="text-align: center; width: 80;"  class="tittabdat">Tipo de Cambio</td>
						<td style="text-align: center; width: 50;"  class="tittabdat">Estatus</td>
						<td style="text-align: center; width: 80;"  class="tittabdat">Fecha y Hora de confirmaci&oacute;n</td>
						<td style="text-align: center; width: 150;" class="tittabdat">Concepto del Pago/Transferencia</td>
					</tr>
					<tr class="textabdatcla" >
						<td style="text-align: center; width: 60;">${resultadoConsulta.referencia}</td>
						<td style="text-align: center; width: 100;">${resultadoConsulta.cuentaOrdenante}</td>
						<td style="text-align: left; width: 150;">${resultadoConsulta.nombreOrdenante}</td>
						<td style="text-align: left; width: 60;">${resultadoConsulta.bancoOrdenante}</td>
						<td style="text-align: left; width: 60;">${resultadoConsulta.paisOrdenante}</td>
						<td style="text-align: left; width: 60;">${resultadoConsulta.ciudadOrdenante}</td>
						<td style="text-align: right; width: 100;">${resultadoConsulta.importe}</td>
						<td style="text-align: center; width: 50;">${resultadoConsulta.divisa}</td>
						<td style="text-align: center; width: 80;">${resultadoConsulta.tipoCambio}</td>
						<td style="text-align: center; width: 50; color: #088A08;">${resultadoConsulta.descripcionEstatus}</td>
						<td style="text-align: center; width: 80;">${resultadoConsulta.fechaHora}</td>
						<td style="text-align: center; width: 150;">${resultadoConsulta.conceptoPago}</td>
					</tr>
					<tr>
						<td colspan="12"><br /></td>
					</tr>
					<tr>
						<td colspan="12" style="text-align: center;">
							<a id="imprimir" href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir" style="width: 80; height: 22; border: 0;" /></a>
							<a id="exportar" href="javascript:exportar();"><img src="/gifs/EnlaceMig/gbo25230.gif" alt="Exportar" style="width: 80; height: 22; border: 0;" /></a>
							<a id="regresar" href="javascript:regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar" style="width: 80; height: 22; border: 0;" /></a>
						</td>
					</tr>
				</table>
			</div>
		</c:if>
	</form>
	<br />

</body>
</html>