<%@ page import="java.lang.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>

<%@ page import="mx.altec.enlace.dao.*" %>
<%@ page import="mx.altec.enlace.utilerias.*" %>

<html>
<head>
	<title>Detalle de Archivo</TITLE>

<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">
	<%

	String nombreArchivoImp="";
	String totalRegistros="";
	String numeroCtasNacionales="";
	String usuario="";

	  if(request.getParameter("totalRegistros")!=null)
		totalRegistros=(String)request.getParameter("totalRegistros");
	  if(request.getParameter("nombreArchivoImp")!=null)
		nombreArchivoImp=(String)request.getParameter("nombreArchivoImp");
	  if(request.getParameter("numeroCtasNacionales")!=null)
		numeroCtasNacionales=(String)request.getParameter("numeroCtasNacionales");
	  if(request.getParameter("usuario")!=null)
		usuario=(String)request.getParameter("usuario");

	%>

function BotonPagina(sigant)
 {
   document.TesImporta.pagina.value=sigant-1;
   document.TesImporta.submit();
 }

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<FORM   NAME="TesImporta" METHOD="Post" ACTION="/Enlace/jsp/CatNomInterb_ImportaDetalle.jsp">

<table width="770" border="0" cellspacing="0" cellpadding="0" align=center>
 <tr>
   <td align=center>
     <table border="0" width="760" cellspacing="0" cellpadding="0">
	   <tr>
	    <td><br></td>
	   </tr>
	 </table>

	<table width="760" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	 <tr>
	  <td class="tittabdat" align="center"> &nbsp; Nombre de Archivo Importado </td>
	  <td class="tittabdat" align="center"> &nbsp; Total de Registros </td>
	  <td class="tittabdat" align="center"> &nbsp; Cuentas Externas Nacionales </td>
	  <td class="tittabdat" align="center"> &nbsp; Usuario </td>
	 </tr>
	 <tr>
	  <td class="textabdatobs" nowrap align="center"><%= nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf("/")+1)%></td>
	  <td class="textabdatobs" nowrap align="center"><%=totalRegistros%></td>
	  <td class="textabdatobs" nowrap align="center"><%=numeroCtasNacionales%></td>
	  <td class="textabdatobs" nowrap align="center"><%=usuario%></td>
	 </tr>
	</table>

<%

%>
	<table border="0" width="760" cellspacing="0" cellpadding="0">
	   <tr>
	    <td><br></td>
	   </tr>
	   <tr>
	     <td class="textabref">Detalle de archivo</td>
	   </tr>
	 </table>

	 <%
	 	 		 System.out.println("Generando pantalla");
	 	 		 System.out.println("Registros: " + totalRegistros);

	 	 		 BufferedReader archImp = new BufferedReader( new FileReader(nombreArchivoImp) );
	 	 		 MAC_Interb Registro=new MAC_Interb();

	 	 		  int inicio=0;
	 	 		  int fin=0;
	 	 		  int totalReg=Integer.parseInt(totalRegistros);

	 	 		  int pagina=0;
	 	 		  if(request.getParameter("pagina")!=null)
	 	 		     pagina=Integer.parseInt(request.getParameter("pagina"));
	 	 		  int regPorPagina=Global.MAX_REGISTROS;

	 	 		  inicio=(pagina*regPorPagina);
	 	 		  fin=inicio+regPorPagina;
	 	 		  if(fin>totalReg)
	 	 	fin=totalReg;

	 	 		  int totalPaginas=calculaPaginas(totalReg,regPorPagina);

	 	 		  int A=regPorPagina*(pagina+1);
	 	 		  int de=(A+1)-regPorPagina;

	 	 		  if((pagina+1)==totalPaginas)
	 	 		     A=totalReg;

	 	 		 System.out.println("Archivo abierto");

	 	 		 String line="";
	 	 		 String clase="";
	 	 		 String fecha="";

	 	 		 String cveBanco="";
	 	 		 String cvePlaza="";


	 	 		 Registro.valida=false;
	 %>

     <table border="0" width="760" cellspacing="2" cellpadding="3" class="tabfonbla">
	   <tr>
	    <td colspan=12 align=left class='textabdatcla'>
		  <table width="100%" class='textabdatcla'>
		    <tr>
		     <td align=left class='textabdatcla'><b>&nbsp;Pagina <font color=blue><%=(pagina+1)%></font> de <font color=blue><%=totalPaginas%></font>&nbsp;</b></td>
		     <td align=right class='textabdatcla'><b>Registros <font color=blue><%=de%></font> - <font color=blue><%=A%></font></b>&nbsp;</td>
		    </tr>
		  </table>
		</td>
	   </tr>
	   <tr>
		 <td class="tittabdat" align="center">Cuenta</td>
		 <td class="tittabdat" align="center">Titular</td>
		 <td class="tittabdat" align="center">Clave Banco</td>
		 <td class="tittabdat" align="center">Clave Plaza</td>
	   </tr>
	 <%
		 //for( int a=0;a<Integer.parseInt(totalRegistros.trim());a++ )
		 //while((line=archImp.readLine())!=null)
		 for(int a=0;a<fin;a++)
		   {
			 line=archImp.readLine();
			 if(a>=inicio)
			   {
				Registro.parse(line);
				clase=(a%2==0)?"textabdatobs":"textabdatcla";
				cveBanco=Registro.cveBanco;
				cvePlaza=Registro.cvePlaza;

		 %>
			   <tr>
				 <td class="<%=clase%>" align="center"><%=Registro.cuenta%>&nbsp;</td>
				 <td class="<%=clase%>" align="left"><%=Registro.nombreTitular%>&nbsp;</td>
				 <td class="<%=clase%>" align="center"><%=cveBanco%>&nbsp;</td>
				 <td class="<%=clase%>" align="center"><%=cvePlaza%>&nbsp;</td>
			   </tr>
		 <%
			}
		  }
	    archImp.close();
	 %>

     </table>
	 <%
	   out.print(botones(totalReg,regPorPagina,pagina,totalPaginas));
	 %>
	 <table align=center border=0 cellspacing=0 cellpadding=0>
	  <tr>
	    <td><br></td>
	  </tr>
	  <tr>
	   <td align=center><A href = "javascript:window.close();" border = 0><img src="/gifs/EnlaceMig/gbo25200.gif" border=0 alt="Cerrar"></a></td>
	  </tr>
	 </table>

   </td>
 </tr>
</table>

 <input type=hidden name=nombreArchivoImp value='<%=nombreArchivoImp%>'>
 <input type=hidden name=totalRegistros value='<%=totalRegistros%>'>
 <input type=hidden name=numeroCtasNacionales value='<%=numeroCtasNacionales%>'>
 <input type=hidden name=usuario value='<%=usuario%>'>
 <input type=hidden name=pagina value='<%=pagina%>'>

 </FORM>
</body>
</html>

<%!
String botones(int totalRegistros,int regPorPagina,int pagina,int totalPaginas)
{
	StringBuffer botones=new StringBuffer("");

	if(totalRegistros>regPorPagina)
	   {
		  botones.append("\n <br>");
		  botones.append("\n <table width=620 border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
		  if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
		  if(totalPaginas>=2)
		   {
			 for(int i=1;i<=totalPaginas;i++)
			  if(pagina+1==i)
			   {
				 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
				 botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a> ");
			   }
		   }
		  if(totalRegistros>((pagina+1)*regPorPagina))
			 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");

		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");
	   }
	 return botones.toString();
}

   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }
%>