<%@ page language="java" contentType="text/html;charset=ISO8859_1"%>
<%@ page language="java" contentType="text/html;charset=ISO8859_1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Movimientos</title>

<script type="text/javascript">
	var contextPath = "${pageContext.request.contextPath}";
</script>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%
SimpleDateFormat formaHora = new SimpleDateFormat("yyyyMMddHHmmss");
%>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script type ="text/javascript" src="/EnlaceMig/jquery-1.11.2.min.js"></script>
<script type ="text/javascript" src="/EnlaceMig/consultaCuentas.js?<%=formaHora.format(new Date())%>"></script>
<script src="/EnlaceMig/json3.min.js" type="text/javascript"></script>
<script type="text/javascript">
var tipoConsulta = "${Tipo}";

function borraCuenta() {
	if (trimString(document.buscar.Cuentas.value) !== "") {
		document.buscar.Cuentas.value = document.buscar.Cuentas.value;
		if (tipoConsulta !== '1') {
			document.buscar.textCuentas.value = document.buscar.textCuentas.value;
		}
	} else {
		document.buscar.Cuentas.value = "";
		if (tipoConsulta !== '1') {
			document.buscar.textCuentas.value = "";
		}
	}
}

function replaceAll(find, replace, str) {
	if(str===null){
		return "";
	}
	return str.replace(new RegExp(find, 'g'), replace);
}

function currencyToNumber(curr){
	if(curr===null||curr===""){
		return "";
	}
	curr = replaceAll('\\\$','',curr);
	curr = replaceAll(',','',curr);
	return curr;
}

function formatoImporte(campo, decimales, separador_decimal, separador_miles){
    var numero = parseFloat(currencyToNumber(campo.value));
    if(numero === null||numero === ""||isNaN(numero)){
        campo.value= "";
        return;
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Agregamos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }
    campo.value = "$"+numero;
}

var Indice = 0;
var registrosTabla = new Array();
var camposTabla = new Array();
var totalCampos = 0;
var totalRegistros = 0;

	/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { /*v3.0*/
	var d = document;
	if (d.images) {
		if (!d.MM_p)
			d.MM_p = new Array();
		var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
		for (i = 0; i < a.length; i++)
			if (a[i].indexOf("#") != 0) {
				d.MM_p[j] = new Image;
				d.MM_p[j++].src = a[i];
			}
	}
}

function MM_swapImgRestore() { /*v3.0*/
	var i, x, a = document.MM_sr;
	for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
		x.src = x.oSrc;
}

function MM_findObj(n, d) { /*v3.0*/
	var p, i, x;
	if (!d)
		d = document;
	if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
		d = parent.frames[n.substring(p + 1)].document;
		n = n.substring(0, p);
	}
	if (!(x = d[n]) && d.all)
		x = d.all[n];
	for (i = 0; !x && i < d.forms.length; i++)
		x = d.forms[i][n];
	for (i = 0; !x && d.layers && i < d.layers.length; i++)
		x = MM_findObj(n, d.layers[i].document);
	return x;
}

function MM_swapImage() { /*v3.0*/
	var i, j = 0, x, a = MM_swapImage.arguments;
	document.MM_sr = new Array;
	for (i = 0; i < (a.length - 2); i += 3)
		if ((x = MM_findObj(a[i])) != null) {
			document.MM_sr[j++] = x;
			if (!x.oSrc)
				x.oSrc = x.src;
			x.src = a[i + 2];
		}
}

	/********************************************************************************/
<%= request.getAttribute("VarFechaHoy") %>
var diasInhabiles = '<%= request.getAttribute("DiasInhabiles") %>';

function verificaRadios(){
   if(document.getElementById("hoyRadio").checked){
   		cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del d&iacute;a",1);
	    return false;
   }
   return true;
 }

var tipoCalendario="";
function SeleccionaFecha(ind)
 {
   var m=new Date();
   m.setDate(m.getDate() + 1);
   n=m.getMonth();
   Indice=ind;

   if(verificaRadios()){
   		if(esHistorico()){
   			msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	  		msg.focus();
	  		tipoCalendario = 1;
   		}else{
			var fechaIni= getFechaHoy();
		   	fechaIni.setDate(1);//fecha = 1/02/2015
			fechaIni.setDate(fechaIni.getDate() - 50);//fecha = 13/12/2014
			fechaIni.setDate(1);//fecha = 1/12/2014
		 	var fechaFin= getFechaHoy();
			var diaI=fechaIni.getDate();
			var mesI=fechaIni.getMonth();
			var anioI=fechaIni. getFullYear();
			var diaF=fechaFin.getDate();
			var mesF=fechaFin.getMonth();
			var anioF=fechaFin.getFullYear();
   			tipoCalendario = 2;
   			var texto= "/Enlace/enlaceMig/jsp/" +
				"CalendarioHTML.jsp?diaI="+diaI+"&diaF="+diaF+"&mesI="+mesI+"&mesF="+mesF+"&" +
				"anioI="+anioI+"&anioF="+anioF+ "&mes=" + mesI + "&anio=" + anioI + "&priUlt=0#mesActual";
			var msg=window.open(texto,"calendario","toolbar=no," +
				"location=no,directories=no,status=no,menubar=no" +
				",scrollbars=yes,resizable=no,width=330,height=260");
			msg.focus();
   		}
	}
 }

function esHistorico(){
	if(tipoConsulta === "1"){
		return document.getElementById("radioHistorico").checked;
	}
	return true;
}
function isDigit (c){
	return ((c >= "0") && (c <= "9"));
}

function Actualiza(){
	var fechaObtenida = null;
	if(tipoCalendario===2){
		fechaObtenida = fecha_completa;
	}else{
		fechaObtenida = Fecha[Indice];
	}
   if(Indice==0){
     document.buscar.FechaIni.value=fechaObtenida;
   }else{
     document.buscar.FechaFin.value=fechaObtenida;
    }
 }

 function isFloat (s){
    var i;
    var seenDecimalPoint = false;

    if (s===null||s===""){
       if (isFloat.arguments.length == 1){
			return false;
       }else{
			return (isFloat.arguments[1] == true);
       }
    }
    if (s === "."){
		return false;
    }
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);

        if ((c === ".") && !seenDecimalPoint) seenDecimalPoint = true;
        else if (!isDigit(c)) return false;
    }

    return true;
}

function ValidaImporte(){
	if(tipoConsulta==="1"){
  		var importeDe = document.getElementById("ImporteDe").value;
		var importeA = document.getElementById("ImporteA").value;
		importeDe = currencyToNumber(importeDe);
		importeA = currencyToNumber(importeA);
		if(importeDe !== ""||importeA !== ""){
    		if(importeDe === ""||!isFloat(importeDe)){
    	    	cuadroDialogo("El valor \"Importe De\" debe ser num&eacute;rico",3);
				return false;
			}else if(importeA === ""||!isFloat(importeA)){
	    		cuadroDialogo("El valor \"Importe A\" debe ser num&eacute;rico",3);
	    		return false;
			}else if(parseFloat(importeDe)>parseFloat(importeA)){
	    		cuadroDialogo("El \"Importe De\" debe ser menor o igual a \"Importe A\"",3);
	    		return false;
			}
		}
	}else{
  		var importe = document.getElementById("Importe").value;
		if(importe!==""&&!isFloat(importe.value)){
	 		cuadroDialogo("El importe debe ser num&eacute;rico.",3);
	 		return false;
   		}
	}
  	return true;
}

function ValidaReferencia()
{
  var i1=0;
  var cont=0;
  Txt1=document.buscar.Referencia.value;

  if(Txt1=="")
     return true;

  for(i1=0;i1<Txt1.length;i1++)
   {
	 Txt2 = Txt1.charAt(i1);
	 if((Txt2>='0' && Txt2<='9'))
 	   cont++;
   }
  if(cont!=i1){
	 document.buscar.Referencia.focus();
	 cuadroDialogo("La Referencia debe ser num&eacute;rico.",3);
	 return false;
   }
  return true;
}

function Enviar() {
	if(tipoConsulta === "1"){
		obtenerDatosCuenta();
	}
	if (trimString(document.buscar.Cuentas.value) == "") {
		cuadroDialogo("Debe seleccionar una cuenta.", 3);
		return;
	}

	if (VerificaFechas() && ValidaImporte() && ValidaReferencia()) {
		if (tipoConsulta === '1') {
			document.buscar.radTabla.value = document.buscar.Cuentas.value;
			var importeDe = document.getElementById("ImporteDe").value;
			var importeA = document.getElementById("ImporteA").value;
			importeDe = currencyToNumber(importeDe);
			importeA = currencyToNumber(importeA);
			document.getElementById("ImporteDe").value=importeDe;
			document.getElementById("ImporteA").value=importeA;
		}else{
			document.buscar.radTabla.value = document.buscar.Cuentas.value;
		}
		document.buscar.submit();
	}
}

	function obtenerDatosCuenta(){
		var select = document.getElementById("comboCuenta0");
		var campoCuenta = document.buscar.Cuentas;
		var campoSeleccion = document.getElementById("radTabla");
		if (select === null) {
			campoCuenta.value = "";
			campoSeleccion.value = "";
			return;
		}
		var valor = select.options[select.selectedIndex].value;
		if (valor !== null) {
			var cuentaValor = obtenerCuenta(valor);
			campoCuenta.value = cuentaValor;
			campoSeleccion.value = cuentaValor;
		}
	}

function obtenerCuenta(valor){
    var res = valor.split("\|");
    if(res.length>=4){
    	var tipoCta = res[3];
    	var tipoDesc = "";
    	if (tipoCta==="5") {
			tipoDesc = "TARJETA DE CREDITO";
		} else {
			tipoDesc = "LINEA DE CREDITO";
		}
		//NumeroDeCuenta|TitularCuenta|ctatipre|ctatipro|LINEA o TAJETA DE CREDITO|TitularCuenta| @"
    	return res[0]+ "|" + res[2]+ "|" +res[1]+ "|" +tipoCta+ "|" +tipoDesc+ "|" + res[2]+"| @";
    }
	return "";
}
	function cambiaFechas(ele) {
		if (ele.value == 'HIS') {
			document.buscar.FechaIni.value = "<%= request.getAttribute("FechaPrimero") %>";
	  		document.buscar.FechaFin.value="<%= request.getAttribute("FechaAyer") %>";
		}else{
	  		document.buscar.FechaIni.value="<%= request.getAttribute("FechaDia") %>";
	 		document.buscar.FechaFin.value="<%= request.getAttribute("FechaDia") %>";
		}
 	}

function VerificaFechas()
 {
   var TipoB="";
   for(i1=0;i1<document.buscar.length;i1++)
     if(document.buscar.elements[i1].type=='radio')
      if(document.buscar.elements[i1].checked==true)
       {
         TipoB=document.buscar.elements[i1].value;
         break;
       }


  if(TipoB=="HIS")
   {
	   var fechaIni = document.buscar.FechaIni.value;
	   var fechaFin = document.buscar.FechaFin.value;

	  	dia1[0] = fechaIni.substring(0,2);
	   	mes1[0] = fechaIni.substring(3,5);
	   	anio1[0] = fechaIni.substring(6,10);

	   	dia1[1] = fechaFin.substring(0,2);
	   	mes1[1] = fechaFin.substring(3,5);
	   	anio1[1] = fechaFin.substring(6,10);

	 if(anio1[0]==anio1[1]){
		 if(mes1[0]>mes1[1])
	      {
			cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
	        return false;
	      }
	     if(mes1[0]==mes1[1])
	      if(dia1[0]>dia1[1])
	       {
			 cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.");
	         return false;
	       }
	 }else if(anio1[0]>anio1[1]){
		 cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
         return false;
	 }
   }
  return true;
 }

//<!-- *********************************************** -->
//<!-- modificación para integración pva 07/03/2002    -->
//<!-- *********************************************** -->

var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
 {
   /*Presentar cuentas para seleccionar serfin-santander*/
   msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
   msg.focus();
 }

function actualizacuenta()
 {
   if(eval(ctatipro)==eval(5))
	 document.buscar.Cuentas.value=ctaselec+"|"+ctadescr+"|"+ctatipre+"|"+ ctatipro+"|TARJETA DE CREDITO|" +ctadescr+"| @";
   else
	 document.buscar.Cuentas.value=ctaselec+"|"+ctadescr+"|"+ctatipre+"|"+ ctatipro+"|LINEA DE CREDITO|" +ctadescr+"| @";
   document.buscar.textCuentas.value=ctaselec+" "+ctadescr;
}

var fechaHoy = '${FechaDia}';

function getFechaHoy(){
	var fecha = fechaHoy.split("/");
	diaHoy = fecha[0];
	mesHoy = Number(fecha[1])-1;
	anioHoy = fecha[2];
	return new Date(anioHoy,mesHoy,diaHoy);
}

function periodoDias(periodo) {
	var operacion = Number(periodo);
	var fecha = getFechaHoy();
	document.buscar.FechaFin.value = getStrFecha(fecha);
	switch (operacion) {
	case 3:
		fecha.setDate(fecha.getDate() - 2);
		document.buscar.FechaIni.value = getStrFecha(fecha);
		break;
	case 30:
		fecha.setDate(1);
		document.buscar.FechaIni.value = getStrFecha(fecha);
		break;
	case 90://ejemplo: fecha = 28/02/2015
		fecha.setDate(1);//fecha = 1/02/2015
		fecha.setDate(fecha.getDate() - 50);//fecha = 13/12/2014
		fecha.setDate(1);//fecha = 1/12/2014
		document.buscar.FechaIni.value = getStrFecha(fecha);
		break;
	}
}

function precargarCuentas(){
	var cuentaCampo = document.getElementById("numeroCuenta0");
	if(cuentaCampo!=null){
		var cuenta = cuentaCampo.value.split(" ")[0];
		cuentaCampo.value = cuenta;
		consultaCuentas();
		if(cuenta!==""){
			setTimeout("seleccionaCuentaObtenida();",1000);
		}
	}
}

function seleccionaCuentaObtenida(){
	document.getElementById("numeroCuenta0").options[1].selected="selected";
}

function getStrFecha(fecha){
	var dia = fecha.getDate();
	var mes = fecha.getMonth()+1;
	var anio = fecha.getFullYear();
	mes = (mes < 10) ? ("0" + mes) : mes;
	dia = (dia < 10) ? ("0" + dia) : dia;
	return dia+"/"+mes+"/"+anio;
}

function sonSoloNumeros(valor) {// validacion para solo numeros
    var validos = "0123456789.-";
    var caracter;
    for (var i=0; i<valor.length; i++) {
	caracter=valor.charAt(i);
	if (validos.indexOf(caracter) === -1){
            return false;
	}
    }
    return true;
}

function esImpoteValido(campo,nombreCampo) {//valida solo positivos sin signo +
    var num = currencyToNumber(campo.value);
    if (!sonSoloNumeros(num)) {
        cuadroDialogo("El valor del campo '"+nombreCampo+"' debe ser numerico.",3);
    }else if(parseFloat(num)<0){
        cuadroDialogo("No se admiten valores negativos para el campo '"+nombreCampo+"'.",3);
    }else{
    	formatoImporte(campo,2,'.',',');
    }

}

/**************************************************************************/
<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="borraCuenta();precargarCuentas();">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<FORM NAME="buscar" method=post action="MCL_Movimientos" onSubmit="return VerificaFechas();">
  <p>
  <table border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
       <c:choose>
       	<c:when test="${Tipo eq '1'}">
		<td colspan="3">
			<table border="0" width="100%">
					<td class="tittabdat" width="33%" nowrap>&nbsp;&nbsp;&nbsp;<input id="hoyRadio" type="radio" name="Busqueda" value="HOY" onClick="cambiaFechas(this);" checked>Del d&iacute;a</td>
					<td class="tittabdat" width="34%" nowrap>&nbsp;&nbsp;&nbsp;<input type="radio" name="Busqueda" value="HIS" onClick = "periodoDias(3);"/>&Uacute;ltimos 3 d&iacute;as</td>
					<td class="tittabdat" width="33%" nowrap>&nbsp;&nbsp;&nbsp;<input type="radio" name="Busqueda" value="HIS" onClick = "periodoDias(30);"/>Mes actual</td>
			</table>
			<table border="0" width="100%">
					<td class="tittabdat" width="50%" nowrap>&nbsp;&nbsp;&nbsp;<input type="radio" name="Busqueda" value="HIS" onClick = "periodoDias(90);"/>Mes actual y dos anteriores</td>
					<td class="tittabdat" width="50%" nowrap>&nbsp;&nbsp;&nbsp;<input id="radioHistorico" type="radio" name="Busqueda" value="HIS" onClick="cambiaFechas(this);"/>Por rango de fechas</td>
			</table>
		</td>
       <tr>
          	<td class="textabdatcla" style="border-top:solid white 2px" colspan="2">
          		<div style="width: 100%">
					<table width="100%" cellpadding="5">
						<tr>
							<td class="tabmovtexbol" colspan="4"><b>B&uacute;squeda de cuentas:</b></td>
						</tr>
						<tr>
							<td align="right" class="tabmovtexbol">Cuenta:</td>
							<td class="tabmovtexbol" colspan="2" width="200"><input type="text" id="numeroCuenta0" onblur="validaSoloNumeros(this,'Cuenta');" class="numeroCuenta" value="${CuentaText}"></td>
							<td rowspan="2" align="center" width="90">
								<a href="javascript:consultaCuentas();"><img src="/gifs/EnlaceMig/Ir.png" width="80" height="22" border="0" alt="Ir"></a>
							</td>
						</tr>
						<tr>
							<td class="tabmovtexbol" align="right" width="100" nowrap>Descripci&oacute;n cuenta:</td>
							<td class="tabmovtexbol" colspan="2" width="200"><input type="text" id="descripcionCuenta0" onblur="validaEspeciales(this,'Descripci&oacute;n cuenta');" class="descripcionCuenta"></td>
						</tr>
					</table>
				</div>
          	</td>
          </tr>
       	</c:when>
       	<c:otherwise>
       		<td class="tittabdat" nowrap>&nbsp;&nbsp;&nbsp;<input id="hoyRadio" type="radio" name="Busqueda" value="HOY" onClick="cambiaFechas(this);" checked/>Movimientos del d&iacute;a</td>
         	<td class="tittabdat" width="50%" nowrap>&nbsp;&nbsp;&nbsp;<input type="radio" name="Busqueda" value="HIS" onClick="cambiaFechas(this);"/>Movimientos Hist&oacute;ricos</td>
       	</c:otherwise>
       </c:choose>
    </tr>

    <tr>
		<td class="textabdatcla" style="border-top:solid white 2px" colspan="2">
			<table width="100%" cellpadding="2" cellspacing="3">
   			 <tr>
	 			<c:choose>
	 				<c:when test="${Tipo eq '1'}">
	 					<td style="text-align: center;" colspan="2" class="tabmovtexbol">
	 						<%@ include file="/jsp/listaCuentas.jspf" %>
							<input id="ventana" name="ventana" type="hidden" value="2"/>
							<input id="opcion" name="opcion" type="hidden" value="1"/>
			 			</td>
	 				</c:when>
	 				<c:otherwise>
     					<td class="tabmovtexbol" align="right" nowrap> Cuenta &nbsp;</td>
      					<td class="tabmovtexbol">
      						<input type="text" name=textCuentas  class="tabmovtexbol" maxlength="22" size="22" onfocus="blur();" value="<%=request.getAttribute("CuentaText")%>">
	  						<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border="0" align="absmiddle"></A>
							<input id="ventana" name="ventana" type="hidden" value="2"/>
							<input id="opcion" name="opcion" type="hidden" value="1"/>
			 			</td>
	 				</c:otherwise>
	 			</c:choose>
			</tr>

    <tr>
       <td class="tabmovtexbol" align="right" nowrap>De la Fecha </td>
       <td class="tabmovtexbol"><input type=text name=FechaIni value='${FechaDia}' size=10 onFocus='blur();' maxlength=10> <A HREF="javascript:SeleccionaFecha(0);" align="absmiddle"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 alt='Cambiar Fecha Inicial'></A></td>
    </tr>
    <tr>
       <td class="tabmovtexbol" align="right" nowrap>A la Fecha </td>
       <td class="tabmovtexbol"><input type=text name=FechaFin value='${FechaDia}' size=10 onFocus='blur();' maxlength=10> <A HREF="javascript:SeleccionaFecha(1);" align="absmiddle"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 alt='Cambiar Fecha Final'></A></td>
    </tr>
	<tr>
		<td class="tabmovtexbol" align=right> &nbsp; Tipo de Movimiento </td>
		<td class="tabmovtexbol">
	    <select class="tabmovtexbol" name=cveMov>
			<option value=2>Todos</option>
			<option value=1>Cargo</option>
			<option value=0>Abono</option>
		</select>
	   	</td>
    	<tr>
		<c:choose>
			<c:when test="${Tipo eq '1'}">
				<td class="tabmovtexbol" align="right">Rango de Importes: </td>
       			<td class="tabmovtexbol" style="text-align: left;" nowrap>
                   	<div style="display: inline;margin-left: 0px;">
                   		De: <input id="ImporteDe" onblur="esImpoteValido(this,'Importe De');" type="text" name="ImporteDe" size="10" maxlength="15" class="tabmovtexbol"/>
                   	</div>
                   	<div style="display: inline;margin-left: 5px;">
    	               	A: <input id="ImporteA" name="ImporteA" onblur="esImpoteValido(this,'Importe A');" type="text" size="10" maxlength="15" class="tabmovtexbol"/>
                   	</div>
                 </td>
			</c:when>
			<c:otherwise>
    		   <td class="tabmovtexbol" align=right>Importe </td>
       			<td class="tabmovtexbol"><input type="text" id="Importe" name="Importe" size="10" maxlength="15" value=''></td>
			</c:otherwise>
		</c:choose>
    </tr>
    <tr>
       <td class="tabmovtexbol" align=right>Referencia </td>
       <td class="tabmovtexbol"><input type=text name=Referencia size=10 maxlength=12></td>
    </tr>

    </table>

   </td>
	</tr>


  </table>

  <br>
  <center><a href = "javascript:Enviar();" style="border:0;"><img src = "/gifs/EnlaceMig/gbo25220.gif" style="border:0;" alt="Enviar" ></a></center>

  <input type="hidden" name="Fecha" value='${FechaDia}' size=10 onFocus='blur();' maxlength=10 />
  <input type="hidden" name="Modulo" value="1"/>
  <input type="hidden" name="Pagina" value="0"/>
  <input id ="radTabla" type="hidden" name="radTabla" value='${radTabla}'/>
  <input type="hidden" name="Cuentas" value='${CuentaHidden}'/>
  <input type="hidden" name="FacArchivo" value='${FacArchivo}'/>
  <input id="Tipo" type="hidden" name="Tipo" value='${Tipo}'/>
</FORM>

</body>
</html>