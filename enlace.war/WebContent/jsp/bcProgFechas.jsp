<!--
Nombre del formulario: "Forma"

Campos:
"Cuenta"			Lista		Cuentas pertenecientes al contrato
//"Periodicidad"	Radio	Valores posibles: "Diaria", "Semanal"
//"PerHoras"		Lista		Horas de 8:00 a 18:00
//"PerDias"			Lista		D"as de "Lunes" a "Viernes"
//"TipoDis"			Radio		Valores posibles: "LoConcentrado", "LoEspecificado"
"Dispersion"		Texto		Valor de moneda
"Archivo"			File		Archivo a importar
"CtaSel"			Radio		Selecciona una sola cuenta
"Accion"			Hidden		Indica la accion a tomar (alta, baja...)
"Trama"				Hidden		Lleva informacion al servlet (depende de la accion)
"Cuentas"			Hidden		Lleva informacion de todo el arbol mientras no se guarde

-->

	<%@ page import="java.util.StringTokenizer" %>
	<%@ page import="java.util.Vector" %>

	<%
		// --- Obtencion de parametros -----------------------------------------------------
		String accion = (String)request.getAttribute("Accion");
		String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
		String parFuncionesMenu = (String)request.getAttribute("newMenu");
		String parEncabezado = (String)request.getAttribute("Encabezado");
		String parCuentas = (String)request.getAttribute("Cuentas");
		String parMensaje = (String)request.getAttribute("Mensaje");
		String parTrama = (String)request.getAttribute("Trama");
		String parDiasInhabiles = (String)request.getAttribute("ParDiasInhabiles");
		String parFecha = (String)request.getAttribute("Fecha");
		String parMovFechas = (String)request.getAttribute("Movfechas");
		String parFechaAyer = (String)request.getAttribute("FechaAyer");
		String parFechaPrimero = (String)request.getAttribute("FechaPrimero");
		String parVarFechaHoy = (String)request.getAttribute("VarFechaHoy");
		String parFechaHoy = (String)request.getAttribute("FechaHoy");
		String parFechaDia = (String)request.getAttribute("FechaDia");
		String parComboMtoEst = (String)request.getAttribute("ComboMtoEst");



		if(parMenuPrincipal == null) parMenuPrincipal = "";
		if(parFuncionesMenu == null) parFuncionesMenu = "";
		if(parEncabezado == null) parEncabezado = "";
		if(parCuentas == null) parCuentas = "";
		if(parMensaje == null) parMensaje = "";
		if(parTrama == null) parTrama = "";
		if(parDiasInhabiles == null) parDiasInhabiles = "";
		if(parFecha == null) parFecha = "";
		if(parMovFechas  == null) parMovFechas = "";
		if(parFechaAyer == null) parFechaAyer = "";
		if(parFechaPrimero == null) parFechaPrimero = "";
		if(parVarFechaHoy == null) parVarFechaHoy = "";
		if(parFechaHoy == null) parFechaHoy = "";
		if(parFechaDia == null) parFechaDia = "";
		if(parComboMtoEst == null) parComboMtoEst = ""; //comentado


		// --- Se preparan otras variables -------------------------------------------------
		Vector cuentas;
		String mensError [] = new String[1];
		mensError[0]="";

		cuentas = ((new mx.altec.enlace.servlets.bcProgFechas()).creaCuentas(parCuentas, request, mensError));

		int a, b;
	%>

<!-- COMIENZA CODIGO HTML -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Programaci&oacute;n de Operaci&oacute;n</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Adrian Mayen">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts -->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>
		//Obtenci�n de los respectivos dias Inhabiles y modificado para Integraci�n
		var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>"
		diasInhabiles = '<%= request.getAttribute("DiasInhabiles") %>';

		var indice =0;
		//--- calendarios y actualizaci�n de las cuentas seleccionadas ------------------
		var ctaselec;
		var ctadescr;
		var ctatipre;
		var ctatipro;
		var ctaserfi;
		var ctaprod;
		var ctasubprod;
		var tramadicional;
		var opcioncalendario;
		var cfm;
		var y=0;

		function importa()
		{
			confirma("Se va importar un nueva lista, y con esto se borrar&aacute; la actual. &iquest;Desea continuar?");
			accionConfirma = 4;

		}

		//Presenta Visor de cuentas de todos los niveles
		function PresentarCuentas()
		{
			msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
			msg.focus();
		}

		function actualizacuenta()
		{
			document.Forma.textdestino.value=ctaselec+" -- "+ctadescr;
		}


		// --- CREA LA TRAMA QUE SERA ENVIADA AL SERVLET ---------------------------------
		function creaTrama()
		{
			var trama = "@";
			var a, b=0,c, x=0, De, A;

			for(a=0;a<document.Forma.de.options.length;a++){
				if(document.Forma.de.options[a].selected){
					//De = parseInt(document.Forma.de.options[a].value);
					if (document.Forma.de.options[a].value.substring(0,1)=="0"){
						De = parseInt(document.Forma.de.options[a].value.substring(1));
					}else De = parseInt(document.Forma.de.options[a].value);
				}
			}
			for(a=0;a<document.Forma.a.options.length;a++){
				if(document.Forma.a.options[a].selected){
					//A  = parseInt(document.Forma.a.options[a].value);
					if (document.Forma.a.options[a].value.substring(0,1)=="0"){
						A = parseInt(document.Forma.a.options[a].value.substring(1));
					}else A = parseInt(document.Forma.a.options[a].value);
				}
			}

			// Comprueba si lleva signo "$" el campo valortecho
			var Valortecho;

			if (document.Forma.valortecho.value.substring(0,1) == "$")
			{
				Valortecho = document.Forma.valortecho.value.substring(1);

				//LASR
				Valortecho = Valortecho.replace(',','');
				Valortecho = Valortecho.replace(' ','');

			}
			else
			{
				Valortecho = document.Forma.valortecho.value;

				//LASR
				Valortecho = Valortecho.replace(',','');
				Valortecho = Valortecho.replace(' ','');
			}



			var FechaInicial = document.Forma.fechaInicial.value;
				FechaInicial = new Date(FechaInicial.substring(6),
										FechaInicial.substring(3,5) ,
										FechaInicial.substring(0,2)
										);
			var FechaFinal = document.Forma.fechaFinal.value;
				FechaFinal = new Date ( FechaFinal.substring(6),
										FechaFinal.substring(3,5) ,
										FechaFinal.substring(0,2)
										);
			var ctasHijas;
			for(a=0;a<document.Forma.textdestino.options.length;a++)
					if(document.Forma.textdestino.options[a].selected)
						ctasHijas = document.Forma.textdestino.options[a].text;

//			var ctahija = ctasHijas.substring(0,ctasHijas.indexOf("-"));
//			var ctadesc = ctasHijas.substring(ctasHijas.indexOf("-")+2);

//			alert("Valor de ctasHijas: " + ctasHijas );

			//VALIDACION DE LOS DATOS EN LA FORMA DEL USUARIO
			// Valida si se ingreso una cuenta
			if(ctasHijas == "" || ctasHijas == null)
				{aviso("Debe seleccionar una cuenta..."); return trama="";}
			else {
				var ctahija = ctasHijas.substring(0,ctasHijas.indexOf("-"));
				var ctadesc = ctasHijas.substring(ctasHijas.indexOf("-")+2);
			}
			if ( De == A)
				{aviso("Los horarios de operacion no deben ser los mismos..."); return trama="";}
			if ( De > A)
				{aviso("La hora final debe ser mayor a la hora de inicio..."); return trama="";}

			if (document.Forma.techo[0].checked == true)
			{
				if(Valortecho == "")
				{
					aviso("Debe proporcionar un valor para el Techo Presupuestal...");
					return trama="";
				}
			}
			else
				Valortecho="0";


			if(techoIsText(Valortecho)==false) return;

			if ((FechaInicial > FechaFinal) || (FechaInicial.getYear() > FechaFinal.getYear()))
				{aviso("La fecha final debe ser mayor o igual que la inicial...");return trama="";}

			//--- Construccion de la trama
			trama +=ctahija + "|";											// 1 Cuenta
			trama += document.Forma.fechaInicial.value + "|";				// 2 Fecha de Inicio
			trama += document.Forma.fechaFinal.value + "|";					// 3 Fecha final
			for(b=0;b<document.Forma.de.options.length;b++)
					if(document.Forma.de.options[b].selected)
						//trama += document.Forma.de.options[b].text + "|";	// 4 Hora de Inicio
						trama += document.Forma.de.options[b].value + "|";	// 4 Hora de Inicio
			for(x=0;x<document.Forma.a.options.length;x++)
					if(document.Forma.a.options[x].selected)
						//trama += document.Forma.a.options[x].text + "|";	// 5 Hora Final
						trama += document.Forma.a.options[x].value + "|";	// 4 Hora de Inicio
			trama += Valortecho + "|";										// 6 Valor del techo
			trama +=" |";													// 7 Usuario
			trama += ctadesc + "\n@";											// 8 Descripcion
			//alert("El valor de la trama es : " + trama);
			//trama = "@50000050485|08/10/2002|08/10/2002|0830|1700|1.000000|1001851|GRUPO BANCA ELECTRONICA";
			//trama = "@51500315411| |GRUPO BANCA ELECTRONICA|10:00|12:00|25/04/2002|25/04/2002|1000.00";
			return trama;
		}


		//---------------Invoca la funcion para agregar cuentas---------------------------
		//
		// Nombre:  agregar
		//
		//
		//
		//--------------------------------------------------------------------------------
		function agregar()
		{
			var trama = creaTrama()

			if (trama != "")
			{
				document.Forma.Accion.value = "AGREGA";
				document.Forma.Trama.value = trama;
				document.Forma.submit();
			}
			else
				return;
		}


		function alta()
		{
			document.Forma.Accion.value = "ALTA_ARBOL";
			document.Forma.submit();
		}

		function borrar()
		{
			confirma("La estructura va a ser borrada, &iquest;Desea continuar?");
			accionConfirma = 2;
		}

		function modifica()
		{
			var sel = ctaSel();
			if(sel == -1) {aviso("Debe seleccionar una cuenta para Modificar"); return; }
			document.Forma.Accion.value = "MODIFICA";
			var tramita = document.Forma.CtaSel[ctaSel()].value;
			document.Forma.Trama.value = tramita;
			document.Forma.submit();


		}

		function regresar()
		{
			document.Forma.Accion.value = "REGRESA";
			document.Forma.submit();
		}

		function eliminar()
		{
			var sel = ctaSel();
			if(sel == -1) {aviso("Debe seleccionar una cuenta para eliminar"); return;}

			confirma("Se va a borrar la cuenta seleccionada. &iquest;Desea continuar?");
			accionConfirma = 3;
			return;
		}

		function imprimir()
			{scrImpresion();}

		// --- Validaciones ----------------------------------------------------------------
		function Actualiza()
			{
			if(Indice==0)
				document.Forma.fechaInicial.value=Fecha[Indice];
			else
				document.Forma.fechaFinal.value=Fecha[Indice];
			}

		function selUno(num)
			{
			var max = document.Forma.CtaSel.length - 2;
			for(a=0;a<max;a++) if(a != num) document.Forma.CtaSel[a].checked = false;
			}

		function ctaSel()
			{
			var sel = -1;
			var max = document.Forma.CtaSel.length - 2;
			for(a=0;a<max;a++) if(document.Forma.CtaSel[a].checked) {sel = a; break;}
			return sel;
			}

		//-- Permite desabilitar la caja de texto de el Techo  Presupuestal (2 funciones siguientes)-----------------
		var _T = "locked";
		var _F = "unlocked";
		function valTecho(_P)
		{
			var _L = document.Forma.lck.value;
			if(_L==_P)return;
			document.Forma.valortecho.disabled=(document.Forma.lck.value=(_L==_F)?_T:_F)==_T;
			document.Forma.valortecho.value="";
		}

		function isDis()
		{
			return (document.Forma.lck.value==_T);
		}


			/*
			{
			if(document.Forma.techo[1].checked)
				{
				document.Forma.valortecho.disabled = true;
				//document.Forma.valortecho.onFocus="this.blur()" ;
				}
			else
				{
				document.Forma.valortecho.disabled = false;
				}
			}
			*/
		//-- FIN Permite desabilitar la caja de texto de el Techo  Presupuestal -----------------

		function condiciones()
			{
			if(document.Forma.textdestino == "") return;
	/*		if(!estaEnConcentracion(ctaselec))
				{
				document.Forma.TipoDis[0].disabled = true;
				document.Forma.TipoDis[1].checked = true;
				}
			else
				{
				document.Forma.TipoDis[0].disabled = false;
				}
			habImporte()*/
			}

		function imprimeUnaLinea(texto)
			{
			for(a=0;a<texto.length;a++)
				if(texto.substring(a,a+1) != " ")
					document.write(texto.substring(a,a+1));
				else
					{
					document.write("&");
					document.write("n");
					document.write("b");
					document.write("s");
					document.write("p");
					document.write(";");
					}
			}

		// --- Para cuadros de di�logo -----------------------------------------------------
		var respuesta = 0;
		var accionConfirma;
		var habilitaConfirma = true;

		function continua()
		{
			if(accionConfirma == 1)
			{
				if(respuesta == 1) agregar();

			}
			else if(accionConfirma == 2)
			{
				if(respuesta == 1)
				{
					document.Forma.Accion.value = "BORRA_ARBOL";
					document.Forma.submit();
				}
			}
			else if(accionConfirma == 4)
			{
				if(respuesta == 1)
				{
					var campo = document.Forma.Archivo;

					while(campo.value.substring(0,1) == " ")
					{
						campo.value = campo.value.substring(1);
					}

					while(campo.value.substring(campo.value.length-1) == " ")
					{
						campo.value = campo.value.substring(0,campo.value.length-1);
					}

					if(campo.value == "")
					{
						campo.focus();
						cuadroDialogo("Debe especificar un nombre de archivo.",1);
						return;
					}

					document.Forma.Accion.value = "IMPORTA";
					document.Forma.submit();

				}
			}

			else
			{
				if(respuesta == 1)
				{
					//var a = creaTrama();
					document.Forma.Accion.value = "ELIMINA";
					document.Forma.Trama.value = document.Forma.CtaSel[ctaSel()].value;
					document.Forma.submit();
				}
			}
			habilitaConfirma = true;

		}

		function confirma(mensaje)
		{
			if(habilitaConfirma)
			{
				cuadroDialogo(mensaje,2);
			}

			habilitaConfirma = false;
		}

		function aviso(mensaje)
		{
			cuadroDialogo(mensaje,1);
		}

		function avisoError(mensaje)
		{
			cuadroDialogo(mensaje,3);
		}

		// --- Inicio y envio
		function inicia()
			{
			//	document.Forma.Periodicidad[0].checked = true;
			//	document.Forma.TipoDis[0].checked = true;
			//  	document.Forma.PerDias.disabled = true;
			//	document.Forma.PerDias.selectedIndex = -1;
			//	condiciones();
			}

		function validaEnvio()
			{
			/*
			//if (verificaFechas()) return false;
			if(document.Forma.Accion.value == "") return false;
			return true;
			*/
			}

		// --- Funciones de men�
		function MM_preloadImages()
			{ //v3.0
			var d=document;
			if(d.images)
				{
				if(!d.MM_p) d.MM_p=new Array();
				var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
				for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0)
					{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
				}
			}

		function MM_swapImgRestore()
			{ //v3.0
			var i,x,a=document.MM_sr;
			for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
			}

		function MM_findObj(n, d)
			{ //v3.0
			var p,i,x;
			if(!d) d=document;
			if((p=n.indexOf("?"))>0&&parent.frames.length)
				{
				d=parent.frames[n.substring(p+1)].document;
				n=n.substring(0,p);
				}
			if(!(x=d[n])&&d.all) x=d.all[n];
			for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
			for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
			return x;
			}

		function MM_swapImage()
			{ //v3.0
			var i,j=0,x,a=MM_swapImage.arguments;
			document.MM_sr=new Array;
			for(i=0;i<(a.length-2);i+=3)
				if((x=MM_findObj(a[i]))!=null)
					{
					document.MM_sr[j++]=x;
					if(!x.oSrc) x.oSrc=x.src;
					x.src=a[i+2];
					}
			}

		<%= parFuncionesMenu %>

		function techoIsText(valor)
		{
			var x1=0;
			var cont=0;
			//Text1=document.Forma.valortecho.value;
			Text1=valor;

			for(x1=0;x1<Text1.length;x1++)
			{
				Text2 = Text1.charAt(x1);
				if((Text2>='0' && Text2<='9') || Text2=='.') cont++;
			}

			if(cont!=x1)
			{
				cuadroDialogo("El Techo presupuestal debe ser num&eacute;rico.",3);
				return false;
			}

			/*if(Text1.indexOf('.')<0)
				Text1+=".00";
			document.Forma.valortecho.value=Text1;
			return true;
			*/
		}

		function SeleccionaFecha(ind)
			{
			var m=new Date();
			n=m.getMonth();
			js_diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';
			Indice=ind;
			//alert("n= "+ n + ", Indice=" + Indice);
			/*msg=window.open("/EnlaceMig/bc_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=330,height=260");
			msg.focus();*/

			var parMovFechas1 = "<%= request.getAttribute("Movfechas") %>";
			var FrmFechasDia = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasMes = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasAnio = parMovFechas1;

			dia = FrmFechasDia;//09;
			mes = FrmFechasMes;//10;
			anio = FrmFechasAnio;//2002;

			msg=window.open("/EnlaceMig/bc_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
			msg.focus();
			}

		/*function SeleccionaFecha()
			{
				var m=new Date();
				n=m.getMonth();
			    n=9;

				msg=window.open("/EnlaceMig/calfut.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
				msg.focus();
			}*/

		function cambiaFechas(ele)
			{
			if(ele.value=='techosi')
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaPrimero") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaAyer") %>";
				}
			else
				{
				document.Forma.FechaIni.value="<%= request.getAttribute("FechaDia") %>";
				document.Forma.FechaFin.value="<%= request.getAttribute("FechaDia") %>";
				}
			}

		function cambiaFechas(ele)
			{
			if(ele.value=='techo')
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaPrimero") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaAyer") %>";
				}
			else
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaDia") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaDia") %>";
				}
			}

		function verificaFechas(){
			var TipoB="";
			var y=0;

			for(i1=0;i1<document.Forma.length;i1++)
			if(document.Forma.elements[i1].type=='radio')
				if(document.Forma.elements[i1].checked==true)
					{
					TipoB=document.Forma.elements[i1].value;
					break;
					}

			if(TipoB=="no")
				{
				if(mes1[0]>mes1[1] && anio1[0]==anio1[1])
					{
					cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
					return false;
					}
				if(mes1[0]==mes1[1] && anio1[0]==anio1[1])
					if(dia1[0]>dia1[1])
						{
						cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.");
						return false;
						}
				}
			return true;
			}

			function fechaString(cadena){ // Elimina el "/" a la cadena de la fecha
			var fechaSin1 = "";
			var fechaSin2 = "";
			var ind;
			while ((ind = cadena.indexOf("/")) != -1){
				fechaSin1 = cadena.substring(0,ind);
				fechaSin2 = cadena.substring(ind+1, cadena.length);
				cadena = fechaSin1 + fechaSin2;
				ind = cadena.indexOf("/");
			}
			return cadena;
			}

		function ffocus(num){
			document.Forma.valortecho.focus();
			SeleccionaFecha(num);
		}


		//LASR
		function vigencia()
		{

			/*
			fecha = new Date();
			hora = fecha.getHours();




			if(hora >= 12)
			{
				var dia  = fecha.getDate()+1;
				if (dia <= 9)
				{
					dia  = "0" + dia;
				}
				var mes  = fecha.getMonth()+1;
				if (mes <= 9)
				{
					mes  = "0" + mes;
				}
				var anio = fecha.getFullYear();

				var fechaFin = dia + "/" + mes + "/" + anio


				document.Forma.fechaInicial.value = fechaFin;
				document.Forma.fechaFinal.value = fechaFin;
			}
			else
			{
				document.Forma.fechaInicial.value="<%= parFechaDia %>";
				document.Forma.fechaFinal.value="<%= parFechaDia %>";


			}

			*/

			document.Forma.fechaInicial.value="<%= parFechaDia %>";
			document.Forma.fechaFinal.value="<%= parFechaDia %>";


		}

		<%= request.getAttribute("VarFechaHoy") %>   //del MCL_Movimientos
	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->
<BODY onload="inicia()" background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>


	<form name = "Frmfechas">
		<%
		String FrmFechasAnio = parMovFechas.substring(0,parMovFechas.indexOf("-"));
		parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
		String FrmFechasMes = parMovFechas.substring(0,parMovFechas.indexOf("-"));
		parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
		String FrmFechasDia = parMovFechas;
		%>

		<!-- input name=Fecha type=hidden value='<%--= request.getAttribute("FechaDia") --%>' size=10  onFocus='blur();' maxlength=10-->
		<Input type = "hidden" name ="strDia" value = "<%= FrmFechasAnio %>">
		<Input type = "hidden" name ="strMes" value = "<%= FrmFechasMes %>">
		<Input type = "hidden" name ="strAnio" value = "<%= FrmFechasDia %>">
	</form>

	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="610">
		<tr valign="top">
			<td width="*">
			<%= parMenuPrincipal %>
			</td>
		</tr>
	</table>
	<%= parEncabezado %>

	<FORM name="Forma" id="Forma" method="POST"  onSubmit="return inicia()" action="bcProgFechas">
		<INPUT type=hidden name="Accion" value="">
		<INPUT type=hidden name="Trama" value="">
		<INPUT type=hidden name="Cuentas" value="<%= parCuentas %>">
		<!-- <INPUT type=Hidden name="Archivo" value=""> -->

		<DIV align=center>
<TABLE>
<TR><TD align="center">
		<table class="textabdatcla" border=0 cellspacing=0 cellpadding=3>
			<tr>
				<td class="tittabdat" colspan=3>&nbsp;Seleccione la cuenta y las condiciones de la operaci&oacute;n
				</td>
			</tr>

			<tr>
				<td class='tabmovtex' colspan="3">
					<br>&nbsp;<b>Cuenta:</b><br>
					<!-- Campo de selecci�n de cuenta -->
					<table>
						<td>
						</td>
						<td>
							<SELECT name="textdestino" class="tabmovtexbol">
								<%= parComboMtoEst %>
							</SELECT>
						</td>
					</table>
				</td>
			</tr>

			<tr>
				<td colspan=3>&nbsp;<br>
				</td>
			</tr>
			<tr>
				<td class="tabmovtex" width="200" rowspan=2 nowrap><b>Horario de operaci&oacute;n:</b><BR>

					<!-- Campos de Horario de operaci�n -->
					<TABLE width=150>
						<TR><TD class="tabmovtex">De:&nbsp;</TD>
							<TD class="tabmovtex">
								<select width=100 style="width:100px;" name="de" class="tabmovtex" onChange="condiciones()">
									<OPTION value="0830">8:30</OPTION>
									<OPTION value="0900">9:00</OPTION>
									<OPTION value="0930">9:30</OPTION>
									<OPTION value="1000">10:00</OPTION>
									<OPTION value="1030">10:30</OPTION>
									<OPTION value="1100">11:00</OPTION>
									<OPTION value="1130">11:30</OPTION>
									<OPTION value="1200">12:00</OPTION>
									<OPTION value="1230">12:30</OPTION>
									<OPTION value="1300">13:00</OPTION>
									<OPTION value="1330">13:30</OPTION>
									<OPTION value="1400">14:00</OPTION>
									<OPTION value="1430">14:30</OPTION>
									<OPTION value="1500">15:00</OPTION>
									<OPTION value="1530">15:30</OPTION>
									<OPTION value="1600">16:00</OPTION>
									<OPTION value="1630">16:30</OPTION>
									<OPTION value="1700">17:00</OPTION>
									<OPTION value="1730">17:30</OPTION>
									<OPTION value="1800">18:00</OPTION>
									<OPTION value="1830">18:30</OPTION>
									<OPTION value="1900">19:00</OPTION>
									<OPTION value="1930">19:30</OPTION>
								</select>
							</TD>
						</TR>
						<TR><TD class="tabmovtex">A:&nbsp;</TD>
							<TD class="tabmovtex">
							<select width=100 style="width:100px;" name="a" class="tabmovtex" onChange="condiciones()">
									<OPTION value="0900">9:00</OPTION>
									<OPTION value="0930">9:30</OPTION>
									<OPTION value="1000">10:00</OPTION>
									<OPTION value="1030">10:30</OPTION>
									<OPTION value="1100">11:00</OPTION>
									<OPTION value="1130">11:30</OPTION>
									<OPTION value="1200">12:00</OPTION>
									<OPTION value="1230">12:30</OPTION>
									<OPTION value="1300">13:00</OPTION>
									<OPTION value="1330">13:30</OPTION>
									<OPTION value="1400">14:00</OPTION>
									<OPTION value="1430">14:30</OPTION>
									<OPTION value="1500">15:00</OPTION>
									<OPTION value="1530">15:30</OPTION>
									<OPTION value="1600">16:00</OPTION>
									<OPTION value="1630">16:30</OPTION>
									<OPTION value="1700">17:00</OPTION>
									<OPTION value="1730">17:30</OPTION>
									<OPTION value="1800">18:00</OPTION>
									<OPTION value="1830">18:30</OPTION>
									<OPTION value="1900">19:00</OPTION>
									<OPTION value="1930">19:30</OPTION>
							</select>
							</TD>
						</TR>
					</TABLE>

				</td>
				<td class="tabmovtex" width="200" colspan=2>
					Techo presupuestal:
				</td>
			</tr>
			<tr valign="middle">
				<td class="tabmovtex">
					<!-- Campos Techo presupuestal (Radio) -->
					<input type="radio" name="techo" value="si" onClick="valTecho(_F);">Si<br>
					<input type="radio" name="techo" value="no" onClick="valTecho(_T);" checked>No
				</td>
				<td class="tabmovtex" align="left">
					<!-- Campo techo presupuestal (Text) -->
					<input id="valortecho" name="valortecho" size=19 class="tabmovtex" value= "" maxlength=19 onfocus="if(isDis())blur();">
					<input type=hidden name="lck" value="unlocked">
				</td>
			</tr>

			<tr><td colspan=3>&nbsp;<BR></td></tr>

			<tr valign="top">
				<td class="tabmovtex" nowrap>
					Vigencia:<BR>
					<!-- Campos de vigencia -->
					<TABLE>
					<TR>
						<TD align=right class="tabmovtex">De la Fecha:</TD>
						<!-- <TD class="tabmovtex"><input type="text" name="fechaInicial" size="12" class="tabmovtexbol" value='<%= request.getAttribute("FechaDia") %>' onFocus="ffocus(0);"></TD> -->
						<TD class="tabmovtex"><input type="text" name="fechaInicial" size="12" class="tabmovtexbol" value="" onFocus="ffocus(0);"></TD>
						<TD class="tabmovtex"><a href ="javascript:SeleccionaFecha(0);" align="absmiddle"><img name="img1" src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></TD>
					</TR>
					<TR>
						<TD align=right class="tabmovtex">A la Fecha:</TD>
						<TD class="tabmovtex"><input type="text" name="fechaFinal" size="12" class="tabmovtexbol" value="" onFocus="ffocus(1);"></TD>
						<TD class="tabmovtex"><A href ="javascript:SeleccionaFecha(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></A></TD>
					</TR>
					</TABLE>
				</td>
				<td colspan=2>&nbsp;</td>
			</tr>
		</table>
		<!-- fin table  -->

</TD>

<TD valign="bottom">

	<TABLE class="textabdatcla" border=0 cellspacing=0 width= 250>
	<TR><TD class="tittabdat">&nbsp;Importar archivo</TD></TR>
	<TR>
		<TD align=center class='tabmovtex'>
		<BR>
		<INPUT type="File" name="Archivo" value = " Archivo "><BR>
		<A href="javascript:importa()"><IMG border=0 src="/gifs/EnlaceMig/gbo25280.gif" alt="Importar"></A>
		<BR>
		</TD>
	</TR>
	</TABLE>

</TD>

</TR>
<TR>
<TD align="center">

		<BR>
		<A href="javascript:agregar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25290.gif" alt="Agregar"></A>
		<BR>

</TD>
<!-- <TD></TD> -->
</TR>
</TABLE>


		<!-- TABLA DE CUENTAS ------------------------------------------------------------->

		<TABLE width=650 border=0 cellspacing=1>
		<TR><TD class="tabmovtex"> Total de cuentas: <%= cuentas.size() %></TD></TR>
		</TABLE>

		<TABLE width=650 class="textabdatcla" border=0 cellspacing=1>
			<TR><TD colspan=5 class="tittabdat" align=center>Estructuras</TD></TR>
			<TR>
				<TD class="tittabdat" align=center>Selecci&oacute;n</TD>
				<TD class="tittabdat" align=center>Cuenta</TD>
				<TD class="tittabdat" align=center>Horario</TD>
				<TD class="tittabdat" align=center>Techo</TD>
				<!-- <TD class="tittabdat" align=center>Importe</TD> -->
				<TD class="tittabdat" align=center>Vigencia</TD>
			</TR>

			<!-- Esta secci�n se repite para cada cuenta -->
			<%
				String espacio;
				String estilo;
				String horario;
				String formatoHorario;
				mx.altec.enlace.bo.bc_CuentaProgFechas cta; // Se definio en modifica()

				estilo = "textabdatobs";
				for(a=0;a<cuentas.size();a++)
					{
					estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
					cta = (mx.altec.enlace.bo.bc_CuentaProgFechas)cuentas.get(a);

					espacio = "";
					espacio += "<IMG src=\"/gifs/EnlaceMig/folder.gif\">&nbsp;";
			%>
				<TR> <!-- Check Box -->
					<!-- <TD class="<%= estilo %>" align=center><INPUT type=CheckBox name="CtaSel" value="<%=cta.getNumCta()%>|<%=cta.getVigencia()%>" onClick="selUno(<%= a %>)"></TD> -->
					<TD class="<%= estilo %>" align=center><INPUT type=CheckBox name="CtaSel" value="<%=cta.getNumCta()%>|<%=cta.getVigencia()%>" onClick="selUno(<%= a %>)"></TD>
					 <!-- Cuenta -->
					<TD class="<%= estilo %>"><%= espacio %><%= cta.getNumCta() + " -- " + cta.getDescripcion()%></TD>
					<!-- <TD class="<%= estilo %>"><%= espacio %><%= cta.getNumCta() + " -- " %></TD> -->
					 <!-- Horario -->
					 <%
						horario = cta.getHorarios();
					    formatoHorario = horario.substring(0,5) + ":" + horario.substring(5,12) + ":" + horario.substring(12) ;
					 %>
					<TD class="<%= estilo %>" align=center><%= formatoHorario%></TD>
					 <!-- Techo Presupuestal -->
					<TD class="<%= estilo %>" align=center><%=(new mx.altec.enlace.servlets.BaseServlet()).FormatoMoneda(cta.getTechopre())%></TD>
					 <!-- Importe -->
					<!-- <TD class="<%= estilo %>" align=center>&nbsp;</TD> -->
					 <!-- Vigencia -->
					<TD class="<%= estilo %>" align=center><%=cta.getVigencia()%></TD>
				</TR>
			<% } %>

			<TR>
		</TABLE>
		<BR>

		<!-- Botones Alta, Modificar, Elimina Registro e Imprimir ---------------------->
			<TABLE>
				<TR align=center>
					<TD>
						<!-- <A href="javascript:alta()"><IMG border=0 src="/gifs/EnlaceMig/gbo25480.gif" alt="Alta"></A> -->
						<A href="javascript:modifica()"><IMG border=0 src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar"></A>
						<A href="javascript:eliminar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25515.gif" alt="Eliminar registro"></A>
						<A href="javascript:imprimir()"><IMG border=0 src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></A>
					</TD>
				</TR>
			</TABLE>

		</DIV>

		<!-- Las siguientes dos l�neas son importantes, no borrar -->
		<INPUT type=Hidden name="CtaSel" value="X">
		<INPUT type=Hidden name="CtaSel" value="XX">


		<INPUT type=Hidden name="cuenta" value="">
		<!-- Boton envia consulta -- Linea provisional -->
		<!-- <INPUT type=submit onClick="document.Forma.Accion.value = 'TUXEDO'; document.Forma.Trama.value =''; ">  -->

	</FORM>
	<SCRIPT>vigencia();valTecho(_T);</SCRIPT>
</BODY>

</HTML>

<% if(!parMensaje.equals("")) out.println("<SCRIPT>aviso('" + parMensaje + "');</SCRIPT>"); %>
