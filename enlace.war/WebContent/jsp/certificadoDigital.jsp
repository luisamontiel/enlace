<html>
<head>
	<title>certificadoDigital</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>

<link href="<%=request.getContextPath()%>/enlace/consultas.css" rel="stylesheet" type="text/css" id = "estilos"/>

<script type="text/javascript">




<%if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));%>

/******************  ********* ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
       
       String respuesta  = request.getAttribute("respuesta") != null ? 
			request.getAttribute("respuesta").toString():"";
%>



function revocar(){

	var r=confirm("�Desea revocar el Certificado Digital?");
	if (r==true) {
		document.getElementById("accion").value = 'revocar';
		document.forms["certificadoDigital"].submit();	 
	} else {
	 	return;
	}
}

function bajarErramienta(){
	document.getElementById("accion").value = 'bajar';
	document.forms["certificadoDigital"].submit();

}

function bajarCertificadoDig() {
	document.getElementById("accion").value = 'bajarCer';
	document.forms["certificadoDigital"].submit();
}

function cargarArchivoEnv(){
	document.forms["uploadFile"].submit();
}

function cargarArchivo(){
	var nomArch = document.getElementById('datafile').value;
	if (nomArch == null || nomArch == ''){
		cuadroDialogo("Debe de introducir un Certificado",1);
		return false;
	} else {
		if(validaExtArch()){
			return true;
		}
	}
}

function inicio() {

	var error = document.certificadoDigital.error.value;
	var exito = document.certificadoDigital.exito.value;
	
	if(error != null && error != "") {
		cuadroDialogo(error, 1);
	}
	if(exito != null && exito != "") {
		cuadroDialogo(exito, 1);
	}
	
	var existe = document.certificadoDigital.existe.value;
	if(existe != null && existe != "true"){
		document.certificadoDigital.revoca.disabled=true;
	}
	
	var revocado = document.certificadoDigital.revocado.value;
	if(revocado != null && revocado != "" && revocado == "true"){
		var elem = document.certificadoDigital.textoRevocado;
		elem.style.display='block';
	}
	
}

function validarExt() {
	var nomArch = document.getElementById('datafile').value;
	if(nomArch.indexOf(".p12") != -1 || nomArch.indexOf(".pfx") != -1){
		mostrarOcultarElemento('tituloPwd', '1');
		mostrarOcultarElemento('cajaPwd', '1');
	}else{
		mostrarOcultarElemento('tituloPwd', '0');
		mostrarOcultarElemento('cajaPwd', '0');
	}
	
}

function mostrarOcultarElemento(id, indicador){
	var elem = document.getElementById(id);
	if(elem.style.display=='block' && indicador == '0'){
		elem.style.display='none';
	}
	if(elem.style.display=='none' && indicador == '1'){
		elem.style.display='block';
	}
}

function validaExtArch(){
	var nomArch = document.getElementById('datafile').value;
	if(nomArch.indexOf(".p12") != -1 || nomArch.indexOf(".pfx") != -1 
	|| nomArch.indexOf(".pem") != -1 || nomArch.indexOf(".der") != -1
	|| nomArch.indexOf(".cer") != -1 || nomArch.indexOf(".crt") != -1){
		return true;
	}else{
		cuadroDialogo("La extensi&oacute;n del archivo no es correcta, verifique su certificado.", 1);
		return false;
	}
}

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<!--<body  style="background:'/gifs/EnlaceMig/gfo25010.gif' ;  background-color:#ffffff "  leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onload="inicio();">

-->
<body  style="background:'<%=request.getAttribute("RUTA_IMAGENES")%>/gfo25010.gif';  background-color:#ffffff "  leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onload="inicio();">

<table style="border: 0px" cellpadding="0px" cellspacing="0px" width="571px">
  <tr valign="top">
   <td width="*">
	
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }


%>

 <table width="850px">
 	<tr>
 		<td align ="center">
		<form name="certificadoDigital" action="CertificadoDigitalServlet" method="post">
			
			<input type="hidden" name="accion" id="accion" />
			<input type="hidden" name="existe" id="existe" value="<%=request.getAttribute("EXISTE") != null ? request.getAttribute("EXISTE"):"" %>" />  
			<input type="hidden" name="exito" id="exito" value="<%=request.getAttribute("EXITO") != null ? request.getAttribute("EXITO"):"" %>" /> 
			<input type="hidden" name="error" id="error" value="<%=request.getAttribute("ERROR") != null ? request.getAttribute("ERROR"):"" %>" />
			<input type="hidden" name="revocado" id="revocado" value="<%=request.getAttribute("REVOCADO") != null ? request.getAttribute("REVOCADO"):"" %>" /> 
		
			<table style="border: 0px"  cellpadding="0" cellspacing="0" style=" text-align: center;">
				<tr>
					<td  class="texenccon"> <b>Vigencia del Certificado:</b></td>
					<td colspan="3" width="150px"  class="texenccon">&nbsp;&nbsp;${datosCertificado.vigenciaCertificado }</td>
					<td class="texenccon">  
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onclick="javascript:bajarErramienta();">Descargar software para cifrar</a> <br> 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onclick="javascript:bajarCertificadoDig();">Descargar el Certificado Digital del Banco</a>  
					</td>
				</tr>
				<tr>
					<td   class="texenccon"> <b>N�mero de Serie:</b></td>
					<td td colspan="2" width="150px"  class="texenccon">&nbsp;&nbsp;${datosCertificado.numeroDeSerie }</td>
					<td>
						<input id="revoca" name="revoca" type="button" value="Revocar" onClick="revocar();">
					</td>
				</tr>
				<tr id="textoRevocado" style="display: <%=request.getAttribute("REVOCADO") != null ? request.getAttribute("REVOCADO"):"none" %>;">
					<td class="texenccon"> <b>Estatus del certificado:</b></td>
					<td class="texenccon" colspan="3" td="">&nbsp;&nbsp;&nbsp;&nbsp;<%=request.getAttribute("ESTATUS")%></td>
				</tr>
			</table>
		
		</form>
		<br>
		<form name="uploadFile" action="UploadFileServlet" method="post" enctype="multipart/form-data" onsubmit="return cargarArchivo();">
			<input type="hidden" name="accion" id="accion" > 
			<input type="hidden" name="existe" id="existe" value="<%=request.getAttribute("EXISTE") != null ? request.getAttribute("EXISTE"):"" %>" >
			<table  style="border: 0px; text-align: center;"  cellspacing=0 cellpadding=3 class='textabdatcla' width="450">
				<tr>
					<td  class="tittabdat">Seleccione el archivo de su Certificado a importar</td>
				</tr>
				<tr >
					<td  class="tabmovtex11" colspan="3" >Buscar archivo:</td>
				</tr>
				<tr>
					<td>
						<input type="file" name="datafile" id="datafile" size="40" onchange="validarExt();">
					</td>
				</tr>
				<tr id="tituloPwd" style="display: none;">
					<td  class="tittabdat">Ingrese su password:</td>
				</tr>
				<tr id="cajaPwd" style="display: none;">
					<td>
						<input type="password" id="passwordTxt" name="passwordTxt" size="40">
					</td>
				</tr>
			</table>
			<br>
			<table   style="border: 0px; text-align: center;"  cellspacing=0 cellpadding=3 width="450">
				<tr>
					<td colspan="3" style=" text-align: center;"><!--
						<img src="/gifs/EnlaceMig/enviar.jpg" style="border: 0px"  alt='Enviar' height='22' width='85' alt="Importar Certificado" onclick="javascript:cargarArchivo();">
					  
					   -->
					   <img src="<%=request.getAttribute("RUTA_IMAGENES")%>/enviar.jpg" style="border: 0px"  alt='Enviar' height='22' width='85' alt="Importar Certificado" onclick="javascript:cargarArchivoEnv();">
					
					</td>
				</tr>
			</table
		</form>
		</td>
	</tr>
 </table>
</body>
</html>
