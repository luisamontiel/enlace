<%@ page import="mx.altec.enlace.bo.Archivos"%>
<%--<%@ page import="EnlaceMig.CombosConf"%>--%>
<%@ page import="java.util.*"%>
<jsp:useBean id="arch_combos" class="mx.altec.enlace.dao.CombosConf" scope="request"/>

<%
		String arch = arch_combos.envia_tux();

		Archivos proveedores = new Archivos();
		Vector ls_prov = new Vector();
		System.out.println (" *************************** antes de mLee ");
		ls_prov = proveedores.mLee(arch, "9");
		System.out.println (" *************************** despues de mLee ");

		Calendar fecha = new GregorianCalendar();
		String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
		String mes1, mes2, mes3;
		int anio = 0;
		if (fecha.get(Calendar.MONTH) == 0) {
	mes1 = meses[10];
	mes2 = meses[11];
	mes3 = meses[0]; }
		else if (fecha.get(Calendar.MONTH) == 1) {
	mes1 = meses[11];
	mes2 = meses[0];
	mes3 = meses[1]; }
		else {
	mes1 = meses[fecha.get(Calendar.MONTH)-2];
	mes2 = meses[fecha.get(Calendar.MONTH)-1];
	mes3 = meses[fecha.get(Calendar.MONTH)]; }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> New Document </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
</HEAD>


<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"> </SCRIPT>

<SCRIPT LANGUAGE="JAVASCRIPT">

function valida_vProveedor () {
	if (document.Graficar.vProveedor[0].checked)
		document.Graficar.vTodosProveedor.selectedIndex = 0;
}

function valida_vTodosProveedor () {
	var index_vTodosProveedor = document.Graficar.vTodosProveedor.selectedIndex;
	if (index_vTodosProveedor != 0) {
		document.Graficar.vProveedor[0].checked = false;
		document.Graficar.vProveedor[1].checked = true; }
	else
		cuadroDialogo("Debe de seleccionar a un proveedor.",3);
}

function valida_De () {
	<% Calendar fecha2 = new GregorianCalendar(); %>
	if (document.Graficar.vPeriodoDe[1].text == "Diciembre" && document.Graficar.vPeriodoDe.selectedIndex == 1)
		document.Graficar.vDE.value = <%=fecha2.get(Calendar.YEAR) - 1 %> ;
	else if ( (document.Graficar.vPeriodoDe[1].text == "Noviembre" ) && (document.Graficar.vPeriodoDe.selectedIndex == 1 || document.Graficar.vPeriodoDe.selectedIndex == 2) )
		document.Graficar.vDE.value = <%=fecha2.get(Calendar.YEAR) - 1 %> ;
	else
		document.Graficar.vDE.value = <%=fecha2.get(Calendar.YEAR) %>
	if (document.Graficar.vPeriodoDe.selectedIndex == 0)
		cuadroDialogo("Debe de seleccionar una fecha inicial", 3);
}

function valida_A() {
	<% Calendar fecha3 = new GregorianCalendar(); %>
	if (document.Graficar.vPeriodoA[1].text == "Diciembre" && document.Graficar.vPeriodoA.selectedIndex == 1)
		document.Graficar.vA.value = <%=fecha3.get(Calendar.YEAR) - 1 %> ;
	else if ( (document.Graficar.vPeriodoA[1].text == "Noviembre" ) && (document.Graficar.vPeriodoA.selectedIndex == 1 || document.Graficar.vPeriodoA.selectedIndex == 2) )
		document.Graficar.vA.value = <%=fecha3.get(Calendar.YEAR) - 1 %> ;
	else
		document.Graficar.vA.value = <%=fecha3.get(Calendar.YEAR) %> ;
	if (document.Graficar.vPeriodoA.selectedIndex == 0)
		cuadroDialogo("Debe de seleccionar una fecha final", 3);
}

function valida_Anio(opcion) {
	if (opcion == 1)
		valida_De();
	else
		valida_A();
	if ( document.Graficar.vPeriodoDe.selectedIndex > document.Graficar.vPeriodoA.selectedIndex)
		cuadroDialogo("La fecha del final debe ser mayor o igual a la de inicio", 3);
}
</SCRIPT>

<BODY >

<CENTER class="titpag" > Filtro de Gr&aacute;ficas
<TABLE WIDTH="430" BORDER="0" CELLSPACING="0" CELLPADDING="0" >

<FORM NAME="Graficar">

 <tr>
   <td align="center">
     <table width="420" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td rowspan="2">

           <table width="410" border="0" cellspacing="2" cellpadding="3">

		   <tr>
	         <td class="tittabdat" colspan="2"> Proveedor:</td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="400" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vProveedor" VALUE="Todos" onClick="valida_vProveedor();" checked>Todos
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vProveedor" VALUE="Proveedor" onClick="valida_vProveedor();">Proveedor

		<SELECT name="vTodosProveedor" onChange="valida_vTodosProveedor();">
		<OPTION VALUE="0">
        <% int longitud = ls_prov.size();
		for (int i=0; i<longitud; i++) { %>
			<OPTION VALUE="<%=i+1%>"><%= (String)ls_prov.elementAt(i) %>
        <% } %>
		</SELECT>
	</TD>
   </TR>

 </TABLE>
	</td>
  </tr>


			<tr>
			    <td class="tittabdat" colspan="2"> Per&iacute;odo: </td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="400" border="0" cellspacing="0" cellpadding="0">
<TR>
    <TD class="tabmovtexbol" nowrap align="CENTER">
		De:
		<SELECT NAME="vPeriodoDe" onChange ="valida_Anio(1);">
			<OPTION VALUE="0">
			<OPTION VALUE="1"><%= mes1%>
			<OPTION VALUE="2"><%= mes2%>
			<OPTION VALUE="3"><%= mes3%>
		</SELECT>

		<INPUT TYPE="TEXT" NAME="vDE" SIZE="5" VALUE="<%= fecha.get(Calendar.YEAR) %>" onFocus="blur();">
		A:
		<SELECT NAME="vPeriodoA" onChange ="valida_Anio(2);">
			<OPTION VALUE="0">
			<OPTION VALUE="1"><%= mes1%>
			<OPTION VALUE="2"><%= mes2%>
			<OPTION VALUE="3"><%= mes3%>
		</SELECT>
		<INPUT TYPE="TEXT" NAME="vA" SIZE="5" VALUE="<%= fecha.get(Calendar.YEAR) %>" onFocus="blur();">
	</TD>
</TR>

 </TABLE>
	</td>
  </tr>

			<tr>
			    <td class="tittabdat" colspan="2"> Graficar por: </td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="400" border="0" cellspacing="0" cellpadding="0">
<TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vGraficar">N&uacute;mero de documentos
	</TD>
</TR>
<TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vGraficar" checked>Importe de los documentos
	</TD>
</TR>

 </TABLE>

	</td>
  </tr>
	<tr align="center">
       <td valign="top" colspan="2">
          <table width="400" border="0" cellspacing="0" cellpadding="0">

		  <tr>
		   <td align="center" valign="middle" width="66">

			  <A href = "" border=0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="66" height="22"></a><A href = "javascript:history.back();" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77" height="22"></a>
         </td>
		  </tr>

		  </table>
		</td>
	</tr>

 </TABLE>

	</td>
  </tr>
 </TABLE>

	</td>
  </tr>

</FORM>

</TABLE>

</CENTER>

</BODY>

</HTML>

<!-- <html>
<head>
<title>Enlace</title>
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<%! String ar = "archivo_actual";
    String def;  %>
<SCRIPT LANGUAGE="Javascript">
var reg;
reg = "";

/*function ponNombre(){
	 document.writeln("<% request.setAttribute(ar, def); %>");
} */


function js_nuevo(){
  cuadroDeCaptura("Nombre del archivo a crear: ", "Nombre del archivo de proveedores" );
}

function js_alta() {
	if (document.ChesPrincipal.archivo_actual.value != "") {
	 	  document.location = "AltaP.jsp?archivo=" + document.ChesPrincipal.archivo_actual.value;
	}
	else
	  cuadroDialogo("No ha creado ni importado un archivo aun", 3);
}

function setRegistro(valor) {
    document.ChesPrincipal.cosa.value = valor;
	reg = valor;

}

function js_eliminar() {
	if (document.ChesPrincipal.archivo_actual.value != "") {
	 	  document.location = "Baja.jsp?archivo=" + document.ChesPrincipal.archivo_actual.value +
			  "&Registro=" + reg;
	}
	else
	  cuadroDialogo("No ha creado ni importado un archivo aun", 3);

}

function js_modificacion() {
	 if (document.ChesPrincipal.archivo_actual.value != "") {
		   document.location = "Modificacion.jsp?archivo=" +
		   document.ChesPrincipal.archivo_actual.value +
			   "&Registro=" + reg;
	 }
     else
	  cuadroDialogo("No ha creado ni importado un archivo aun", 3);

}


</SCRIPT>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</HEAD>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">

    <!-- MENU PRINCIPAL -->

    <!--%= request.getAttribute("MenuPrincipal") %--> <!--</TD>
   </TR>
</TABLE>
<table width="760" border="0" cellspacing="0" cellpadding="0">
   <FORM  NAME="ChesPrincipal" METHOD="POST" ACTION="ChesRegistro">

  <tr>

   <td align="center">

     <table width="680" border="0" cellspacing="0" cellpadding="0">

       <tr>

         <td rowspan="2">

           <table width="460" border="0" cellspacing="2" cellpadding="3">

             <tr>

               <td class="tittabdat" colspan="2"> Mantenimiento a proveedores </td>

             </tr>

             <tr align="center">

               <td class="textabdatcla" valign="top" colspan="2">

                 <table width="450" border="0" cellspacing="0" cellpadding="0">

                   <tr valign="top">

                     <td width="270" align="right">

                       <table width="150" border="0" cellspacing="5" cellpadding="0">

                         <tr>

                           <td class="tabmovtex" nowrap>Archivo:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>
                             <!--<input type="text" name="archivo_actual" size="22" class="tabmovtex" value="defaulto"  onFocus='blur()'>--> <!--
                             <input type="text" name="archivo_actual" size="22" class="tabmovtex" value="<%   if (session.getAttribute("ArchivoActual")!= null)
							 out.print(session.getAttribute("ArchivoActual"));%>"  onFocus='blur()'>

                           </td>

                         </tr>

                       </table>

                     </td>

                     <td width="180" align="right">

                       <table width="150" border="0" cellspacing="5" cellpadding="0">

                         <tr>

                           <td class="tabmovtex" nowrap>N&uacute;mero de transmisi&oacute;n:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

                             <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("transmision")!=null) out.print(request.getAttribute("transmision" )); %>" NAME="transmision" onFocus='blur();'>

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("fechaTransmision")!=null) out.print(request.getAttribute("fechaTransmision" )); %>"  NAME="fechaTrans" onFocus='blur();' >

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">Fecha

                             de actualizaci&oacute;n:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("fechaActualizacion" ) != null) out.print(request.getAttribute("fechaActualizacion" ));  %>" NAME="fechaAct" onFocus='blur();' >

                           </td>

                         </tr>

                       </table>

                     </td>

                     <td width="180" align="right">

                       <table width="150" border="0" cellspacing="5" cellpadding="0">

                         <tr>

                           <td class="tabmovtex" nowrap>Total de registros:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(session.getAttribute("TotalRegistros" )!= null) out.print(String.valueOf(session.getAttribute("TotalRegistros"))); %>" NAME="totRegs" onFocus='blur();' >

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>Registros aceptados:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(session.getAttribute("RegistrosAceptados" )!= null) out.print(session.getAttribute("RegistrosAceptados")); %>"  NAME="aceptados" onFocus='blur();' >

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">Registros

                             rechazados: </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">

							  <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("RegistrosRechazados" )!= null) out.print(request.getAttribute("RegistrosRechazados")); %>"  NAME="rechazados" onFocus='blur();' >

                           </td>

                         </tr>

					   </table>

                     </td>

                   </tr>

                 </table>

               </td>

             </tr>

           </table>

         </td>

         <td width="200" height="94" align="center" valign="middle">

			  <A href = "javascript:js_nuevo();" border = 0><img src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo" width="115" height="22"></a>

         </td>

       </tr>

       <tr>

         <td align="center" valign="bottom">

           <table width="200" border="0" cellspacing="2" cellpadding="3">

             <tr align="center">

               <td class="textabdatcla" valign="top" colspan="2">

                 <table width="180" border="0" cellspacing="5" cellpadding="0">

                   <tr valign="middle">

                     <td class="tabmovtex" nowrap>

                       Importar archivo</td>

                   </tr>

                   <tr>

                     <td nowrap>

						  <input type="file" name="Archivo" size="15">

                     </td>

                   </tr>

                   <tr align="center">

                     <td class="tabmovtex" nowrap>

						  <A href = "javascript:js_importar();" border = 0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>

                     </td>

                   </tr>

                 </table>

               </td>

             </tr>

           </table>

         </td>

       </tr>

     </table>

     <br>



		<!--%= request.getAttribute("Tabla") %-->

<!--

     <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">

       <tr>

         <td align="right" valign="middle" width="66">

			  <A href = "javascript:js_alta();" border=0><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>

         </td>

         <td align="left" valign="top" width="127">

  			  <A href = "javascript:js_eliminar();" border=0><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>

         </td>

         <td align="left" valign="top" width="93">

			  <A href = "javascript:js_modificacion();" border=0><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>

         </td>

         <td align="left" valign="top" width="83">

  			  <A href = "javascript:;" border=0><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="83" height="22"></a>

         </td>

       </tr>

     </table>

     <br>

     <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">

       <tr>

         <td align="right" valign="middle" width="78">

			  <A href = "javascript:js_envio();" border=0><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>

         </td>

         <td align="left" valign="top" width="97">

			  <A href = "javascript:js_recupera();" border=0><img src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar" width="97" height="22"></a>

         </td>

         <td align="left" valign="top" width="77">

			  <A href = "javascript:js_borrar();" border=0><img src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar" width="77" height="22"></a>

         </td>

         <td align="left" valign="top" width="85">

			  <% if(request.getAttribute("lnkExportar") !=null) out.print(request.getAttribute("lnkExportar")); %>

         </td>

       </tr>

     </table>



     <br>

   </td>

 </tr>

<% if (session.getAttribute("ArchivoActual") != null)  {
	 java.io.FileInputStream fins = new java.io.FileInputStream( (String) session.getAttribute("ArchivoActual"));
	 java.io.DataInputStream dins = new java.io.DataInputStream(fins);
	 String cadena = new String("algo");
	 int contador = 0;
      while (cadena !=null)  {
		  cadena = dins.readLine();
		  contador++;
      }
     int numRegistros;
	   numRegistros = (int) ((contador - 1) / 32);
	  // out.println(String.valueOf(numRegistros));
   %>
  <table width="760" border="0" cellspacing="0" cellpadding="0">


             <tr>

               <td class="tittabdat" colspan="4" ><center> Registros actuales en el archivo <%=session.getAttribute("ArchivoActual")%>
				   <% dins.close();
			          fins.close();
					  java.io.FileInputStream Fins = new java.io.FileInputStream( (String) session.getAttribute("ArchivoActual"));
	            java.io.DataInputStream Dins = new java.io.DataInputStream(Fins);
					  %> </center> </td>

             </tr>
         <% String c; %>

		 <% for(int h = 0; h < numRegistros; h++) { %>
              <tr>
				  <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>
				    <% c = Dins.readLine(); %>
				   <input type ="radio" name = "cosa" value = "cosa"
			         onClick= 'setRegistro("<%=c%>");'>

				  </td>
				   <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=c%></B></td>
				   <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>   <B><%= Dins.readLine()%></B></td>
				   <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>   <B><%= Dins.readLine()%></B></td>
				        <% if (h != numRegistros - 1)
				             for(int f = 0; f < 29; f++)
				               cadena = Dins.readLine();

			            %>

              </tr>
		  <%
		    }  // fin del for
			  Dins.close();
			  Fins.close(); %>
  </table>
   <% }    // fin del if
	%>


  </form>

</table>

</BODY>
</HTML>    -->