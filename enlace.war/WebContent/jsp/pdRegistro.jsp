<%@page contentType="text/html"%>
<jsp:useBean id="Archivo" class="mx.altec.enlace.bo.pdArchivoTemporal" scope="session"/>
<jsp:useBean id="Message" class="java.lang.String" scope="session"/>
<%
//SLF IM323100
//<jsp:useBean id='mapaBen' class='java.util.HashMap' scope='session'/>
%>
<jsp:useBean id="mapBen" class="java.util.HashMap" scope="session"/>
<jsp:useBean id="listaErrores" class="java.util.ArrayList" scope="session"/>
<jsp:useBean id="Permisos" class="java.util.HashMap" scope="session"/>
<%
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");
    boolean Deshabilita = false;
    boolean Envio = false;
    boolean importar = false;
%>
<%
    Deshabilita = (null == Archivo.getNombre ()) || Archivo.getbEnviado ();
    importar = (session.getAttribute ("Importado") == null);
    session.removeAttribute ("Importado");
    Envio = Archivo.getRegistros () > 0;
%>
<html>
<head>

    <title>Enlace</title>

    <meta http-equiv="Content-Type" content="text/html">

    <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
    <script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
    <script type="text/javascript">

        <%--Scripts necesarios para el cargado de la pagina--%>

        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
            var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
            if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }

        function MM_findObj(n, d) { //v3.0
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
        }

        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
            if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }

        <%----------------------------------------------------%>

        var numPagoSel = -1;

        <%-- Funcion para preparar el registro de un pago --%>
        function alta_pago() {
            document.pdRegistro.txtOpcion.value = 2;
            document.pdRegistro.submit();
        }

        <%-- Funcion para enviar el archivo de pagos a Tuxedo --%>
        function enviar() {
            document.pdRegistro.txtOpcion.value = 10;
            document.pdRegistro.submit();
        }

        <%-- Funcion para modificar un pago en el archivo --%>
        function modifica_pago(opc) {
            if (numPagoSel == -1) {
                cuadroDialogo("Debe seleccionar un pago para modificar", 3);
            } else {
                document.pdRegistro.txtOpcion.value = 6;
                document.pdRegistro.submit();
            }
        }

        <%-- Funcion para recuperar el estado de los pagos --%>
        function recupera() {
            document.pdRegistro.operacion.value = "recupera";
            cuadroCaptura ("Escriba el n&uacute;mero de secuencia:", "Recupera");
        }

        <%-- Funcion para actualizar el n�mero de pago --%>
        function actualizaNumPago (pago) {
            numPagoSel = pago;
        }

        <%-- Funcion para borrar un pago del archivo --%>
        function borra_pago(opc) {
            if (numPagoSel == -1) {
                cuadroDialogo("Debe seleccionar un pago para eliminar", 3);
            } else {
                document.pdRegistro.operacion.value = "borrapago";
                cuadroDialogo("Esta seguro que desea borrar el pago " +
                    numPagoSel, 2);
            }
        }

        <%-- Funcion para preparar el borrado de un archivo --%>
        function borra_archivo() {
            document.pdRegistro.operacion.value = "borra";
            cuadroDialogo("Esta seguro de eliminar el archivo", 2);
        }

        <%-- Funcion para preparar la creacion de un archivo --%>
        function crea_archivo() {
            document.pdRegistro.operacion.value=("crea");
            cuadroCaptura ("Escriba el nombre del archivo", "Nuevo Archivo");
        }

        <%-- Funcion para seguir la ejecuci�n despues de preguntar algo --%>
        function continuaCaptura() {
            if (document.pdRegistro.operacion.value == "crea") {
                if (campoTexto == "") {
                    //cuadroDialogo("Proporcione un nombre para el archivo", 3);
                    alert("Proporcione un nombre para el archivo");
                } else if (contieneEspacios (campoTexto)) {
                    //cuadroDialogo("Proporcione un nombre sin espacios", 3);
                    alert("Proporcione un nombre sin espacios");
                } else {
                    if (respuesta == 1) {
                        document.pdRegistro.txtArchivo.value = campoTexto;
                        document.pdRegistro.txtOpcion.value = 1;
                        document.pdRegistro.submit();
                    }
                }
            } else if (document.pdRegistro.operacion.value == "borra") {
                if (respuesta == 1) {
                    document.pdRegistro.txtOpcion.value = 4;
                    document.pdRegistro.submit();
                }
            } else if (document.pdRegistro.operacion.value == "borrapago") {
                if (respuesta == 1) {
                    document.pdRegistro.txtOpcion.value = 5;
                    document.pdRegistro.submit();
                }
            } else if (document.pdRegistro.operacion.value == "recupera") {
                if (respuesta == 1) {
                    document.pdRegistro.txtOpcion.value = 11;
                    document.pdRegistro.NumReferencia.value = campoTexto;
                    document.pdRegistro.submit ();
                }
            }
        }

        function continua() {
            if (document.pdRegistro.operacion.value == 'borra') {
                if (respuesta == 1) {
                    document.pdRegistro.txtOpcion.value = 4;
                    document.pdRegistro.submit ();
                }
            } else {
                if (respuesta == 1) {
                    document.pdRegistro.txtOpcion.value = 5;
                    document.pdRegistro.submit ();
                }
            }
        }

        <%-- Funcion para importar un archivo --%>
        function importar () {
            if (document.pdRegistro.Archivo.value != "") {
                archivo = document.pdRegistro.Archivo.value
                document.pdRegistro.txtArchivo.value = archivo.substring(archivo.lastIndexOf('\\')+1, archivo.length);
                document.pdRegistro.txtOpcion.value = 9;
                document.pdRegistro.submit ();
            }
        }

        <%-- Funcion para deshabilitar botones cuando el archivo no ha sido creado --%>
        function deshabilita () {
            cuadroDialogo ("Debe crear un archivo.", 3);
        }

        <%-- Funcion para mostrar un mensaje de no envio --%>
        function noenvio () {
            cuadroDialogo ("No hay datos para enviar.", 3);
        }

        <%-- Funcion para checar facultades --%>
        function nofacultad (mensaje) {
            cuadroDialogo ("No tienen facultad para " + mensaje, 3);
        }

        <%-- Funcion para mostrar los errores al importar el archivo --%>
        function muestraErrores() {
            msg = window.open("pdRegistro?txtOpcion=12","Errores","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=460,height=200");
            msg.focus();
        }

        function contieneEspacios (nombre) {
            for (x = 0; x < nombre.length; x++) {
                if (nombre.charAt(x) == ' ') return true;
            }
            return false;
        }
    <% if( request.getAttribute("newMenu") != null ) out.print( request.getAttribute("newMenu") ); %>
    
	    function msgCifrado(){
	    	try {
	    		var mensajeCifrado = document.pdRegistro.mensajeCifrado.value;
	    		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
	    			var arreglo = mensajeCifrado.split("|");
               		var msj = arreglo[1];
               	    var tipo = arreglo[0];
           			cuadroDialogo( msj ,tipo);
	    		}
	    	} catch (e){};
	    }    
    </script>

    <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"  
onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); <%= request.getAttribute("despliegaEstatus" ) %>;<%if (!Message.equals("")) out.println("cuadroDialogo('" + Message + "', 1)");%><%if (!listaErrores.isEmpty ()) out.print ("muestraErrores();");%>"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado" ) %>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="pdRegistro" ENCTYPE="multipart/form-data" METHOD="POST" ACTION="pdRegistro">
  	<input type="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>">
    <tr>
      <td align="center">
        <table width="680" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td rowspan="2">
              <table width="460" border="0" cellspacing="2" cellpadding="3">
                <tr>
                  <td class="tittabdat" colspan="2"> Datos del archivo</td>
                </tr>
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="450" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top">
                        <td width="270" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">Archivo:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">
                                <input type="text" name="txtArchivo" size="22" class="tabmovtex" value="<% if(Archivo.getNombre()!= null && !Archivo.getNombre().equals("")) out.print(Archivo.getNombre()); %>"  onFocus='blur()'>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">N&uacute;mero de secuencia:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">
                                <%--Checar el numero de transaccion--%>
                                <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(Archivo.getReferencia () != null) out.print(Archivo.getNumTransmision()); %>" NAME="transmision" onFocus='blur();'>
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">Fecha de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">
                                <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(Archivo.getFechaTransmision() != null) out.print(sdf.format (Archivo.getFechaTransmision().getTime ())); %>"  NAME="fechaTrans" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap" valign="middle">Fecha
                                de actualizaci&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap" valign="middle">
                                <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(Archivo.getFechaActualizacion() != null) out.print(sdf.format (Archivo.getFechaActualizacion().getTime ()));  %>" NAME="fechaAct" onFocus='blur();' >
                              </td>
                            </tr>
                            <%--<tr>
                              <td class="tabmovtex" style="white-space:nowrap" valign="middle">Importe
                                de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap" valign="middle">
							  <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("importeEnviado" ) != null) out.print( request.getAttribute("importeEnviado" )); %>"  NAME="importeTrans" onFocus='blur();' >
                              </td>
                            </tr>--%>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">Total de registros:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">
				<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<%=Archivo.getRegistros()%>" NAME="totRegs" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">Registros aceptados:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap">
				<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<%=Archivo.getRegistrosAceptados()%>"  NAME="aceptados" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap" valign="middle">Registros
                                rechazados: </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" style="white-space:nowrap" valign="middle">
				<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<%=Archivo.getRegistrosRechazados()%>"  NAME="rechazados" onFocus='blur();' />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="200" height="94" align="center" valign="middle">
            <%
                if (((Boolean) Permisos.get ("Envio")).booleanValue()) {
            %>
                <A href = "javascript:crea_archivo();" border = 0><img src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo" width="115" height="22"></a>
            <%
                } else {
            %>
                <A href='javascript:nofacultad("crear archivos.");'><IMG src='/gifs/EnlaceMig/gbo25550.gif' border='0' alt='Crear archivo' width='115' height='22'></A>
            <%
                }
            %>
            </td>
          </tr>
          <tr>
            <td align="center" valign="bottom">
              <table width="200" border="0" cellspacing="2" cellpadding="3">
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="180" border="0" cellspacing="5" cellpadding="0">
                      <tr valign="middle">
                        <td class="tabmovtex" style="white-space:nowrap">
                          Importar archivo</td>
                      </tr>
                      <tr>
                        <td style="white-space:nowrap">
			  <input type="file" name="Archivo" size="15">
                        </td>
                      </tr>
                      <tr align="center">
                        <td class="tabmovtex" style="white-space:nowrap">
                        <%
                            if (Envio) {
                        %>
                            <A href='javascript:cuadroDialogo("No puede importar un archivo si tiene otro abierto", 3);'><IMG src='/gifs/EnlaceMig/gbo25280.gif' border='0' alt='Aceptar' width='80' height='22'></A>
                        <%
                            } else if (((Boolean) Permisos.get ("Importar")).booleanValue()) {
                        %>
                            <A href = "javascript:importar();" border = 0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>
                        <%
                            } else {
                        %>
                            <A href='javascript:nofacultad("importar archivos.");'><IMG src='/gifs/EnlaceMig/gbo25280.gif' border='0' alt='Aceptar' width='80' height='22'></A>
                        <%
                            }
                        %>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <%
                    mx.altec.enlace.bo.pdPago Temp;
                    java.util.ArrayList Pagos = new java.util.ArrayList ();
                    Pagos.addAll (Archivo.getPagos ());
                    java.util.ListIterator liPagos = Pagos.listIterator();
                    if (!Pagos.isEmpty ()) {
        %>
        <table width='680' border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class='textabref'>Tabla de Pagos a registrar</td>
            </tr>
        </table>
        <TABLE width='680' border='0' cellspacing='2' cellpadding='3' class='tabfonbla'>
            <TR>
                <TD align='center' class='tittabdat' width='55'>Seleccione</TD>
                <TD align='center' class='tittabdat' width='80'>Cuenta de cargo</TD>
                <TD align='center' class='tittabdat' width='60'>No. Pago</TD>
                <TD align='center' class='tittabdat' width='180'>Beneficiario</TD>
                <TD align='center' class='tittabdat' width='80'>Importe</TD>
                <TD align='center' class='tittabdat' width='70'>Fecha de libramiento</TD>
                <TD align='center' class='tittabdat' width='70'>Fecha l&iacute;mite de pago</TD>
                <%
                if (Envio) {
                %>
                <TD align='center' class='tittabdat' width='180'>Estatus Pago</TD>
                <%
                }
                %>
            </TR>
        <%
                        mx.altec.enlace.bo.pdBeneficiario ben;
                        while (liPagos.hasNext()) {
                            Temp = (mx.altec.enlace.bo.pdPago) liPagos.next();
        %>
            <TR>
                <td class='textabdatobs' align="center" >
                    <input type=radio name='txtNumPag' value='<%=Temp.getNoPago ()%>' onclick='actualizaNumPago (this.value)'>
                </td>
                <td align="center" class="textabdatobs" ><%=Temp.getCuentaCargo ()%></td>
                <td class=textabdatobs  align="right"><%=Temp.getNoPago ()%></td>
                <td class=textabdatobs  align="left">

                <%
                    if (Temp.getNomBen () != null && !Temp.getNomBen ().equals ("")) {
                %>
                    <%=Temp.getNomBen ()%>
                <%
                    } else {
            // ben = (EnlaceMig.pdBeneficiario) mapaBen.get(Integer.parseInt(Temp.getClaveBen ())+""); //SLF IM323100
               ben = (mx.altec.enlace.bo.pdBeneficiario) mapBen.get(Temp.getClaveBen ());
                        if (null != ben) {
                %>
                        <%=ben.getNombre ()%>
                <%
                        } else {
                %>
                        Beneficiario no registrado:
                <%
                        }
                    }
                %>
                </td>
                <td class='textabdatobs' style="white-space:nowrap" align="right"><%=(Temp.getImporteFor ())%></td>
                <td class='textabdatobs' style="white-space:style="white-space:nowrap"" align="center"><%=sdf.format (Temp.getFechaLib ().getTime ())%></td>
                <td class='textabdatobs' style="white-space:style="white-space:nowrap"" align="center"><%=sdf.format (Temp.getFechaPago ().getTime ())%></td>
                <% if (Envio) { %>
                <td class='textabdatobs' style="white-space:style="white-space:nowrap"" align='left'><%=Temp.getDescripcionEstatus()%>&nbsp;</td>
                <%}%>
            </TR>
        <%
                }
        %>
        </table><BR>
        <%
            }
        %>

        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="66">
            <%
                if (Deshabilita) {
            %>
                    <A href = "javascript:deshabilita();"><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>
            <%
                } else if (!((Boolean) Permisos.get ("Alta")).booleanValue()) {
            %>
                    <A href='javascript:nofacultad ("altas manuales");'><img src='/gifs/EnlaceMig/gbo25480.gif' border='0' alt='Alta' width='66' height='22'></A>
            <%
                } else {
            %>
                    <A href = "javascript:alta_pago();"><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>
            <%
                }
            %>
            </td>
            <td align="left" valign="top" width="127">
                <%
                    if (Pagos.isEmpty ()) {
                %>
                    <A href = "javascript:borra_pago(1);"><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>
                <%
                    } else if (Deshabilita) {
                %>
                    <A href = "javascript:deshabilita();"><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>
                <%
                    } else if (((Boolean) Permisos.get ("Manipulacion")).booleanValue()) {
                %>
                    <A href = "javascript:borra_pago(2);"><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>
                <%
                    } else {
                %>
                    <A href='javascript:nofacultad("borrar registros.")'><IMG src='/gifs/EnlaceMig/gbo25500.gif' border='0' alt='Baja de registros' width='127' height='22'></A>
                <%
                    }
                %>
            </td>
            <td align="left" valign="top" width="93">
                <%
                    if (Pagos.isEmpty ()) {
                %>
                    <A href = "javascript:modifica_pago(1);"><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>
                <%
                    } else if (Deshabilita) {
                %>
                    <A href = "javascript:deshabilita();"><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>
                <%
                    } else if (((Boolean) Permisos.get ("Manipulacion")).booleanValue()) {
                %>
                    <A href = "javascript:modifica_pago(2);"><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>
                <%
                    } else {
                %>
                    <A href='javascript:nofacultad("modificar registros.");'><IMG src='/gifs/EnlaceMig/gbo25510.gif' border='0' width='93' height='22'></A>
                <%
                    }
                %>
            </td>
            <td align="left" valign="top" width="83">
            <%
                if (((Boolean) Permisos.get ("Imprimir")).booleanValue()) {
            %>
                <A href = "javascript:self.print ();"><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="83" height="22"></a>
            <%
                } else {
            %>
                <A href='javascript:nofacultad("Imprimir.")'><IMG src='/gifs/EnlaceMig/gbo25240.gif' border='0' alt='Imprimir' width='83' height='22'></A>
            <%
                }
            %>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="78">
            <%
                if (!Envio) {
            %>
                <A href='javascript:noenvio();'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></A>
            <%
                } else if (Deshabilita && importar) {
            %>
                <A href='javascript:deshabilita();'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></A>
            <%
                } else if (((Boolean) Permisos.get ("Envio")).booleanValue() && !Archivo.getbEnviado ()) {
            %>
                <A href = "javascript:enviar();"><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
            <%
                } else if (((Boolean) Permisos.get ("Modificar")).booleanValue() && Archivo.getbEnviado ()) {
            %>
                <A href = "javascript:enviar();"><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
            <%
                } else if (!Archivo.getbEnviado ()) {
            %>
                <A href='javascript:nofacultad("enviar archivos.")'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></A>
            <%
                } else {
            %>
                <A href='javascript:nofacultad("modificar archivos enviados.")'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></A>
            <%
                }
            %>
            </td>
            <td align="left" valign="top" width="97">
            <%
                if (((Boolean) Permisos.get ("Recuperar")).booleanValue()) {
            %>
                <A href = "javascript:recupera();"><img src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar" width="97" height="22"></a>
            <%
                } else {
            %>
                <A href='javascript:nofacultad("recuperar archivos.")'><IMG src='/gifs/EnlaceMig/gbo25530.gif' border='0' alt='Recuperar' width='97' height='22'></A>
            <%
                }
            %>
            </td>
            <td align="left" valign="top" width="77">
            <%
                if (Deshabilita && importar) {
            %>
                <A href='javascript:deshabilita();'><IMG src='/gifs/EnlaceMig/gbo25540.gif' border='0' alt='Borrar' width='77' height='22'></A>
            <%
                } else if (((Boolean) Permisos.get ("Manipulacion")).booleanValue()) {
            %>
                <A href = "javascript:borra_archivo();"><img src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar" width="77" height="22"></a>
            <%
                } else {
            %>
                <A href='javascript:nofacultad("borrar el archivo.")'><IMG src='/gifs/EnlaceMig/gbo25540.gif' border='0' alt='Borrar' width='77' height='22'>
            <%
                }
            %>
            </td>
			<!-- Modificacion everis de M�xico 29 05 2007 DVS  M�dulo Seguimiento de Transferencias -->
			<td>
            	<A href="consultaServlet?entrada=1" border=0><img src='/gifs/EnlaceMig/consulta_transferencias.gif' border=0 alt='Enviar Consulta'></A>
            </td>
			<!-- Fin Modificaci�n 29 05 2007 -->
            <td align="left" valign="top" width="85">
                <%
                    if (request.getSession().getAttribute("URLArchivo") != null) {
                %>
                <A href='/Download/<%=request.getSession().getAttribute("URLArchivo")%>'>
                    <IMG src='/gifs/EnlaceMig/gbo25230.gif' border='0' alt='Exportar' width='85' height='22'>
                </A>
                <%
                    }
                %>
            </td>
          </tr>
        </table>

        <br>
      </td>
    </tr>
    <INPUT type="hidden" name="txtOpcion" value='0'/>
    <INPUT type="hidden" name="operacion" value=''/>
    <INPUT type="hidden" name="NumReferencia" value=''/>
  </form>
</table>

</body>
</html>
<%
//session.removeValue ("Message");
session.setAttribute("Message",null);

%>
