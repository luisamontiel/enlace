<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@page import="mx.altec.enlace.bo.BaseResource" %>

<!-- JavaScript del App -->
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/Mancomunidad.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<Script language = "Javascript" >

  function AgregaFolio()
  {
   var h = document.fsaldo.elements.length;
    var folios=""; // = new Array (h);
    var j = 0;
    for (i=0;i<document.fsaldo.elements.length;i++)
    {
      var e = document.fsaldo.elements[i];
      if ((e.type == "checkbox") && e.checked)
      {
         folios = folios + document.fsaldo.elements[i].value + ";";
         j++;
      }
    }
    document.fsaldo.foliosPen.value = folios;
    document.fsaldo.j.value = j;
   if(j==0){
   cuadroDialogo("USTED NO HA SELECIONADO UNA CUENTA.\n\nPOR FAVOR SELECCIONE UNA CUENTA \nPARA TRAER SU SALDO", 1);
   document.fsaldo.allbox.focus();
   return false;
   }
  }

<!-- *********************************************** -->
<!-- modificacion para integracion pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
/*JVL - IMV414187 06-SEP2004, se agrego el if para mostrar o no mostrar */
/* la ventana de eleccion de cuentas, para el tipo de operacion:        */
/* AAOP - Alta de Archivo de pago directo no se muestra la ventana de cuentas */

  if(document.Frmgetinfobit.BanCtas.value == "Activo")
   {
   //Stefanini*200820500*25-03-2009*JAL** Se agrego el parametro tipoOp para mostrar las cuentas
    msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&tipoOp="+document.Frmgetinfobit.operacion.value,"Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
    msg.focus();
   }
  else
   {
    document.Frmgetinfobit.cuenta.value="No Aplica";
    document.Frmgetinfobit.textcuenta.value="No Aplica";

    return;
   }
/*Fin - IMV414187 06-SEP2004 */
}
function actualizacuenta()
{
  document.Frmgetinfobit.cuenta.value=ctaselec;
  document.Frmgetinfobit.textcuenta.value=ctaselec;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function validaCombos()
 {
   if (document.Frmgetinfobit.operacion.options.selectedIndex==0)
   cuadroDialogo ("Seleccione la operaci&oacute;n", 3)
  else
    if (document.Frmgetinfobit.cuenta.value.length<=0)
      cuadroDialogo ("Seleccione la cuenta", 3)
    else
      if (document.Frmgetinfobit.importe.value=="")
	{
	 document.Frmgetinfobit.importe.focus()
	 cuadroDialogo ("Proporcione el importe de la operaci&oacute;n", 3)
	}
	else
	 document.Frmgetinfobit.submit();
}

/*JVL - IMV414187 06-SEP2004, se incluyo esta funcion para desactivar la elccion de */
/* cuenta cuando el tipo de operacion es AAOP - Alta de Archivo de pago directo */
function EstableceEstado()
{
 TipoOper = document.Frmgetinfobit.operacion.options[document.Frmgetinfobit.operacion.options.selectedIndex].value;
 DescOpr = document.Frmgetinfobit.operacion.options[document.Frmgetinfobit.operacion.options.selectedIndex].text;

 //alert(TipoOper);

 if (TipoOper=="AAOP")
  {
   document.Frmgetinfobit.BanCtas.value = "Inactivo";
   document.Frmgetinfobit.textcuenta.value="No Aplica";
  document.Frmgetinfobit.textcuenta.disabled = true;
  }
 else
  {
   document.Frmgetinfobit.BanCtas.value = "Activo";
   document.Frmgetinfobit.textcuenta.disabled = false;
   document.Frmgetinfobit.textcuenta.value="";
  }

document.Frmgetinfobit.desc_operacion.value=document.Frmgetinfobit.operacion.options[document.Frmgetinfobit.operacion.options.selectedIndex].text;
}
/*Fin - IMV414187 06-SEP2004*/


<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>
<br>

<FORM NAME="Frmgetinfobit" METHOD = "POST" ACTION="GetAMancEspecial">
  <INPUT TYPE="hidden" VALUE="" NAME="desc_operacion">
  <INPUT TYPE="hidden" VALUE="" NAME="desc_cta">
  <INPUT TYPE="hidden" VALUE="Activo" NAME="BanCtas">

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
      <table border="0" cellspacing="2" cellpadding="3">
        <tr align="left">
          <td class="tittabdat"> Capture los datos</td>
        </tr>
        <tr align="center" valign="middle">
          <td class="textabdatcla" height="35">
            <table border="0" cellspacing="5">
              <tr>
                <td class="tabmovtex" align="right" nowrap>Tipo de operaci&oacute;n:</td>
                <td class="tabmovtex" width="100%">
			      <SELECT NAME="operacion" class="tabmovtex" onchange="EstableceEstado();">
			        <Option value = " ">Seleccione la operacion</Option>
			        <Option value = "IN04">N&oacute;mina</Option>
   			        <Option value = "PNOS">Tarjeta de Pagos Santander </Option><!-- YHG NPRE -->
   			        <Option value = "PNIS">Tarjeta de Pagos Santander Individual</Option><!-- YHG NPRE -->
			        <Option value = "RE03">SUA</Option>
			        <Option value = "PIMP">Pago de Impuestos</Option>
			        <Option value = "INTE">N&oacute;mina Interbancaria</Option>
			        <Option value = "SATP">Nuevo Pago de Impuestos</Option>
 			        <Option value = "CFRP">Confirming</Option>
	        	    <!-- PPM - MX-2004-238 08-SEP2004, agregue la opcion SARA-Alta de Pago SAR -->
 			        <Option value = "SARA">Pago de SAR</Option>
 			        <!-- Fin PPM MX-2004-238 08-SEP2004-->
	        	        <!-- JVL - IMV414187 06-SEP2004, agregue las opciones ALOP y AAOP al combo
 			        <Option value = "ALOP">Reg. L&iacute;nea Pago Directo</Option>
 			        <Option value = "AAOP">Alta de Archivo Pago Directo</Option>
	        	        Fin - IMV414187 06-SEP2004 -->
	                 </SELECT>
                </td>
              </tr>
              <tr>
                <td class="tabmovtex" align="right" nowrap>Cuenta de cargo:</td>
                <td class="tabmovtex">
				  <input type="text" name=textcuenta class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
				    <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
				  <input type="hidden" name="cuenta" value="">
				</td>
              </tr>
              <tr>
                <td class="tabmovtex" align="right" nowrap>Importe:</td>
                <td class="tabmovtex">
				  <INPUT TYPE="Text" NAME="importe" SIZE="15" onBlur = "checkFloatValue(this);" class="tabmovtex">
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br>
      <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr>
          <td align="center" width="86">
		    <a href = "javascript:validaCombos();"><img border="0" name="imageField2" src="/gifs/EnlaceMig/gbo25440.gif" width="86" height="22" alt="Generar"></a>
          </td>
          <td align="center" width="76">
            <a href = "javascript:document.Frmgetinfobit.reset();"><img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
</table>
</FORM>
</body>
</html>