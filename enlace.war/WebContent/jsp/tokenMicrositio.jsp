<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.Calendar" %>
<%
    Calendar dt = Calendar.getInstance();
    dt.setTime( new java.util.Date() );
    int mes = dt.get(Calendar.MONTH);
    int dia = dt.get(Calendar.DAY_OF_MONTH);
    int anio = dt.get(Calendar.YEAR);

	java.util.Date hora = new java.util.Date();
	int hh = hora.getHours();
	int mm = hora.getMinutes();
	int ss = hora.getSeconds();
%>
<%!
private String sinComas (String a) {
    StringBuffer sb = new StringBuffer();
    for (int k=0; k < a.trim().length() ;k++){
        if(a.charAt(k)!=',')
            sb.append(a.charAt(k));
    }
    return sb.toString();
}
%>

<%
	String empresa = (String)request.getParameter("empresa");
	String convenio = (String)request.getParameter("convenio");
	String numContrato = (String)request.getParameter("numContrato");
	String nomContrato = (String)request.getParameter("nomContrato");
	System.out.println("tokenMicrositio - convenio     ==============>" + convenio);
	System.out.println("tokenMicrositio - empresa   ==============>" + empresa);
	System.out.println("tokenMicrositio - numContrato      ==============>" + numContrato);
	System.out.println("tokenMicrositio - nomContrato ==============>" + nomContrato);

%>


<html>
<head>
<title>Banco Santander (M&eacute;xico), S. A.</title>

<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s26050">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<!-- <SCRIPT SRC="/EnlaceMig/validate.js" LANGUAGE="JavaScript"></script> -->
<script language = "JavaScript" SRC= "/EnlaceMig/microSitio_cuadroDialogo.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/Convertidor.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>

<link rel="stylesheet" href="/EnlaceMig/pagos.css" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


function validarSesion()
 {
    var cerrar = document.form1.cerrarSesion.value;
 	if (cerrar != "NO"){
 		ventana=window.open('/Enlace/enlaceMig/logout?operacionOK=N','trainerWindow','width=580,height=350,toolbar=no,scrollbars=no,left=210,top=225');
 	}
 }

var dayarray=new Array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


var mydate=new Date(<%=anio%>,<%=mes%>,<%=dia%>,<%=hh%>,<%=mm%>,<%=ss%>);

function reloj()
 {
   if(!document.layers && !document.all)
     return;

   var digital = new Date();
   var year=digital.getYear();
   if (year < 1000)
      year+=1900;
   var day=digital.getDay();
   var month=digital.getMonth();
   var daym=digital.getDate();
   if (daym<10)
      daym="0"+daym;

   var horas = digital.getHours();
   var minutos = digital.getMinutes();
   var segundos = digital.getSeconds();
   var amOrPm = "AM";
   if (horas > 11)
     amOrPm = "PM";
   if (horas > 12)
     horas = horas - 12;
   if (horas == 0)
     horas = 12;
   if (minutos <= 9)
     minutos = "0" + minutos;
   if (segundos <= 9)
     segundos = "0" + segundos;

   dispTime = "<font color='666666' face='Verdana'>" + dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" | "+horas+":"+minutos+":"+segundos+" "+ amOrPm +"</font>";
   //dispTime = horas + ":" + minutos + ":" + segundos + " " + amOrPm;
   if (document.layers)
    {
      document.layers.objReloj.document.write("<font face=Tahoma size=1>"+ dispTime + "</font>");
	  document.layers.objReloj.document.close();
	}
   else
    if (document.all)
	  objReloj.innerHTML = dispTime;
   setTimeout("reloj()", 1000);
 }


function validaForma() {
	token = document.form1.token.value;
	if(token.length != 8) {
		cuadroDialogo ("La longitud de la contrase&ntilde;a debe ser de 8 posiciones.", 1);
		return;
	}
	if(!validaFormato(token)) {
		cuadroDialogo ("La contrase&ntilde;a debe ser num&eacute;rica.", 1);
		return;
	}
	document.getElementById("enviar").style.visibility="hidden";
	document.getElementById("enviar2").style.visibility="visible";
	document.form1.cerrarSesion.value="NO";
	document.form1.action = "/Enlace/enlaceMig/LoginServletMicrositio?opcion=1";
	document.form1.submit();
}

function validaFormato (pwd)
{
	var TemplateF=/^[\d]{8,8}$/i;  //dominio de 0-9
	return TemplateF.test(pwd);  //Compara "pwd" con el formato "Template"
}



//-->
</script>



</head>


<body onLoad="reloj()" onBeforeUnload="validarSesion()">
<span id="objReloj" style="position:absolute;left:285;top:60;"></span>
<table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="2"><img src="/gifs/EnlaceMig/cheque_img.jpg" width="147" height="40"></td>
	<td width="593" valign="top" bgcolor="#FF0000"><img src="/gifs/EnlaceMig/top.gif" width="453" height="40"></td>
  </tr>
  <tr>
    <td background="/gifs/EnlaceMig/sombra_gris.jpg"><img src="/gifs/EnlaceMig/sombra_gris.jpg" width="18" height="14"></td>
    <td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"></td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
        <tr>
          <td align="left" class="titulorojo"> <div align="left">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="2" valign="top" class="minitxt"></td>
                  <td align="right"><a href="javascript:;" onClick="document.form1.cerrarSesion.value='NO'; MM_openBrWindow('/EnlaceMig/ayuda003_enlace.html','ayuda','scrollbars=yes,resizable=yes,width=340,height=420')" onMouseOver="MM_swapImage('Image31','','/gifs/EnlaceMig/ayuda_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/ayuda.jpg" name="Image31" width="47" height="40" border="0" id="Image3"></a></td>
                </tr>
                <!--<tr>
                  <td width="30%" valign="bottom" class="titulorojo">Informaci&oacute;n
                    del pago a: </td>
                  <td valign="bottom" class="titulorojo"><%=convenio%> - <%=empresa%></td>
                  <td align="right">&nbsp;</td>
                </tr>
                <tr>
                  <td><p>&nbsp;</p></td>
                  <td class="titulorojo"><%=numContrato%> | <%=nomContrato%></td>
                  <td align="right">&nbsp;</td>
                </tr>-->

		 <tr>
		 <td colspan=3>


<center>
<table width=500 border=0>
<td align="center"> <table width="94%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="20" align="center" class="titulo">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" align="center" bgcolor="#f0eeee" class="titulo"> <p class="tituloCopy"><font color="#000000">Capture
              su contrase&ntilde;a din&aacute;mica</font></p></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center"> <table width="56%" border="0" cellspacing="0" cellpadding="0">
              <tr valign="middle">
                <td width="150" class="tituloCopy">Contrase&ntilde;a din&aacute;mica:</td>
                <td> <form name="form1" method="post" action = "/Enlace/enlaceMig/LoginServletMicrositio?opcion=1">
                    <input name="token" type="password" size="8" maxlength="8" onClick='javascript:document.form1.cerrarSesion.value="NO"' autocomplete="off">
                    <INPUT name="cerrarSesion" TYPE="hidden"  value="SI">
                  </form></td>
              </tr>
            </table>
            <p>&nbsp;</p></td>
        </tr>
        <tr align="center"  id="enviar2" style="visibility:hidden">
		<td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
	</tr>

        <tr id="enviar">
          <td align="center"><a href="javascript:validaForma();" onClick='javascript:document.form1.cerrarSesion.value="NO"';><img src="/gifs/EnlaceMig/gbo25280.gif" width="84" height="26" border="0"></a></td>

        </tr>



        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr valign="top">
                <td width="36%"><strong>Paso 1.</strong> <br>
                  Presione el bot&oacute;n de su token</td>
                <td>&nbsp;</td>
                <td width="36%"><strong>Paso 2. </strong><br>
                  Capture la contrase&ntilde;a din&aacute;mica que aparece en
                  el Token.</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="center"> <table width="500" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="2"><img src="/gifs/EnlaceMig/img001.gif" width="173" height="49"></td>
          <td width="2"><img src="/gifs/EnlaceMig/img4.gif" width="42" height="49"></td>
          <td width="2"><img src="/gifs/EnlaceMig/img7.gif" width="97" height="49"></td>
          <td><img src="/gifs/EnlaceMig/img12.gif" width="205" height="49"></td>
        </tr>
        <tr>
          <td><img src="/gifs/EnlaceMig/img2.gif" width="173" height="56"></td>
          <td><a href="#"><img src="/gifs/EnlaceMig/img5_an.gif" width="42" height="56" border="0"></a></td>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="/gifs/EnlaceMig/img8.gif" width="97"></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="2"><img src="/gifs/EnlaceMig/img9.gif" width="18"></td>
                      <td align="center" bgcolor="#A3B28E"><font color="#000000">12345678</font></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><img src="/gifs/EnlaceMig/img10.gif" width="97"></td>
              </tr>
            </table></td>
          <td><img src="/gifs/EnlaceMig/img13.gif" width="205" height="56"></td>
        </tr>
        <tr>
          <td><img src="/gifs/EnlaceMig/img3.gif" width="173" height="30"></td>
          <td><img src="/gifs/EnlaceMig/img6.gif" width="42" height="30"></td>
          <td><img src="/gifs/EnlaceMig/img11.gif" width="97" height="30"></td>
          <td><img src="/gifs/EnlaceMig/img14.gif" width="205" height="30"></td>
        </tr>
      </table></td>
      </table>
      </center>

		</td>
		</tr>









              </table>
            </div></td>
        </tr>
      </table></td>
  </tr>

  <tr>
    <td bgcolor="#f0eeee">&nbsp;</td>
    <td>&nbsp;</td>

  </tr>
  <tr>
    <td bgcolor="#f0eeee">&nbsp;</td>
    <td align="right"><img src="/gifs/EnlaceMig/sitio_seg.jpg" height="25" width="66">&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#f0eeee">&nbsp;</td>
    <td>&nbsp;</td>

  </tr>
  <tr>
    <td bgcolor="#f0eeee">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#f0eeee">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

  <tr bgcolor="#cccccc">
    <td><img src="/gifs/EnlaceMig/spacer.gif" height="10" width="10"></td>
    <td><img src="/gifs/EnlaceMig/spacer.gif" height="10" width="10"></td>
  </tr>



</table>



<!--
<table border='0' cellpadding='0' cellspacing='0' align='right' background="/gifs/EnlaceMig/fondo.gif" width='155' height='43'>
 <tr>
  <td width="123" valign=bottom>
	 <table border='0' cellpadding='0' cellspacing='0' width="123"  background="" >
	  <tr>
	    <td width=15><br></td>
	  </tr>
	  <tr>
	    <td height="15">&nbsp;</td>
	  </tr>
	 </table>
  </td>
 </tr>
</table>
//-->
</body>
</html>
<script>
<%= request.getAttribute("MensajeLogin01") %>
</script>