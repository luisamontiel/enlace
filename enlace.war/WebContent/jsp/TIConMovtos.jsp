<html>
<head>
	<title>Consultas de Movimientos de Tesoreria</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript">

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document;
  if(d.images){
	  if(!d.MM_p)
		 d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
	 for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){
		    d.MM_p[j]=new Image;
			d.MM_p[j++].src=a[i];
			}
		}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr;
  for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
	  x.src=x.oSrc;
}

function MM_findObj(n, d)
{ //v3.0
  var p,i,x;
  if(!d) d=document;
   if((p=n.indexOf("?"))>0&&parent.frames.length) {
       d = parent.frames[n.substring(p+1)].document;
	   n = n.substring(0,p);
	  }
  if(!(x=d[n])&&d.all)
	  x = d.all[n];
   for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
   for(i=0;!x&&d.layers&&i<d.layers.length;i++)
      x=MM_findObj(n,d.layers[i].document);
   return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/

  var Indice=0;

<% if(request.getAttribute("VarFechaHoy")!= null) out.print(request.getAttribute("VarFechaHoy")); %>
  diasInhabiles = '<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

function SeleccionaFecha(ind)
 {
   var m=new Date();
   n=m.getMonth();
   Indice=ind;

   if(verificaRadios())
    {
	  msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	  msg.focus();
	}
 }

function Actualiza()
 {
   if(Indice==0)
     document.buscar.FechaIni.value=Fecha[Indice];
   else
     document.buscar.FechaFin.value=Fecha[Indice];
 }

//*************************************************************** Fechas
function Enviar()
 {
   if(VerificaFechas())
     document.buscar.submit();
 }

function verificaRadios()
 {
   //if(document.buscar.elements[1].checked==true)
   if(document.buscar.elements[2].checked==true)
     return true;
   cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del d&iacute;a",3);
   return false;
 }

function cambiaFechas(ele)
 {
   if(ele.value=='HIS')
    {
	  document.buscar.FechaIni.value="<%= request.getAttribute("FechaPrimero") %>";
	  document.buscar.FechaFin.value="<%= request.getAttribute("FechaAyer") %>";
	}
   else	{
	     document.buscar.FechaIni.value="<%= request.getAttribute("FechaDia") %>";
	     document.buscar.FechaFin.value="<%= request.getAttribute("FechaDia") %>";
	    }
 }

function VerificaFechas()
 {
   for(i1=0;i1<document.buscar.length;i1++)
      if(document.buscar.elements[i1].type=='radio')
        if(document.buscar.elements[i1].checked==true)
           TipoB=document.buscar.elements[i1].value;

  if(TipoB=='HIS')
   {
	  var fechaIni = document.buscar.FechaIni.value;
	   	var fechaFin = document.buscar.FechaFin.value;

	  	dia1[0] = fechaIni.substring(0,2);
	   	mes1[0] = fechaIni.substring(3,5);
	   	anio1[0] = fechaIni.substring(6,10);

	   	dia1[1] = fechaFin.substring(0,2);
	   	mes1[1] = fechaFin.substring(3,5);
	   	anio1[1] = fechaFin.substring(6,10);

	   	if(anio1[0]==anio1[1]){
     		if(mes1[0]>mes1[1])
      		{
        		//alert("Error: La fecha Final no debe ser menor a la inicial.");
				cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
        		return false;
      		}
     		if(mes1[0]==mes1[1] && anio1[0]==anio1[1])
      			if(dia1[0]>dia1[1])
       			{
         			//alert("Error: La fecha Inicial debe ser menor a la final.");
		 			cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.",3);
         			return false;
       			}
	   	}else if(anio1[0]>anio1[1]){
	   		 cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
	         return false;
		}
   }
  return true;
 }

function BotonPagina(sigant)
 {
   var tot=0;
   tot=document.tabla.Pagina.value;

   if(sigant==0)
      tot--;
   if(sigant==1)
      tot++;
   if(sigant==2)
       tot=0;
   if(sigant>=3){
      //tot=(document.tabla.NumPag.selectedIndex);
	  tot=sigant-4;
	}

   document.tabla.Pagina.value=tot;
   document.tabla.submit();
 }

function formatea(para)
 {
   var formateado="";
   var car="";

   for(a2=0;a2<para.length;a2++){
	  if(para.charAt(a2)==' ')
	     car="+";
	  else
	     car=para.charAt(a2);
	  formateado+=car;
    }
   return formateado;
 }

<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<form  NAME="buscar" method=post action="TIConMovtos" onSubmit="return VerificaFechas();">
  <td align=center><% if(request.getAttribute("Tabla")!= null) out.print(request.getAttribute("Tabla")); %></td>
  <input type=hidden name=Modulo value=<%= request.getAttribute("Modulo") %>>
  <input type=hidden name=Pagina value=0>
  <input type=hidden name=Tipo value='<%= request.getAttribute("Tipo") %>'>
</form>

<form  NAME="tabla" method=post action="TIConMovtos">
 <% if(request.getAttribute("Estructura")!= null) out.print(request.getAttribute("Estructura")); %>
 <br><br>
 <% if(request.getAttribute("Registros")!= null) out.print(request.getAttribute("Registros")); %>
 <% if(request.getAttribute("Botones")!= null) out.print(request.getAttribute("Botones")); %>
 <input type=hidden name=Modulo value=<%= request.getAttribute("Modulo") %>>
 <input type=hidden name=Pagina value=<%= request.getAttribute("Pagina") %>>
 <input type=hidden name=Numero value=<%= request.getAttribute("Numero") %>>
 <input type=hidden name=FechaIni value='<%= request.getAttribute("FechaIni") %>'>
 <input type=hidden name=FechaFin value='<%= request.getAttribute("FechaFin") %>'>
 <input type=hidden name=Busqueda value='<%= request.getAttribute("Busqueda") %>'>
 <input type=hidden name=TransEnv value='<%= request.getAttribute("TotalTrans") %>'>
 <input type=hidden name=NumContrato value='<%= request.getAttribute("NumContrato") %>'>
 <input type=hidden name=NomContrato value='<%= request.getAttribute("NomContrato") %>'>
 <input type=hidden name=Tipo value='<%= request.getAttribute("Tipo") %>'>
 <input type=hidden name=selDivisa value='<%=request.getAttribute("Divisa") %>'>
</form>
</body>
</html>