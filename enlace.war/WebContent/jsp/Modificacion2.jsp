<!-- JSP encargada de las modificaciones sobre un registro en el archivo -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="java.io.InputStreamReader"%>
<HTML>
<HEAD>
<TITLE> Alta de Proveedor </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">

<script language = "JavaScript" SRC= "/EnlaceMig/ConsultCanc.js"></script>
<SCRIPT LANGUAGE="JavaScript" archive="/EnlaceMig/cuadroDialogo.js"> </SCRIPT>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<SCRIPT LANGUAGE="Javascript">

 function js_mandar() {

	 document.Forma.submit();
 }


<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;

function PresentarCuentas()
{
	msg=window.open("/EnlaceMig/pantcuentas.html","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");

	msg.focus();
}

function actualizacuenta()
{
  document.frmConsultCanc.cuenta.value=ctaselec;
  document.frmConsultCanc.textcuenta.value=ctaselec;
}

function MM_preloadImages() { //v3.0

	var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v3.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

  d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}


<%= (String) session.getAttribute("newMenu") %>

</SCRIPT>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="PutDate();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">


  <tr valign="top">


	<td width="*">


	 <!-- MENU PRINCIPAL -->


	 <%= (String) session.getAttribute("MenuPrincipal") %>


	</td>


   </tr>


</table>





<%= (String) session.getAttribute("Encabezado") %>





<br>

 <CENTER class="titpag" > Modificacion de Proveedor </CENTER>
<%! String registro, Reg;
    String archivo;
	boolean sepudo;
%>
 <%! public void Escribe( java.io.FileWriter dato, String cadena)                                                         throws Exception {
	String thiscadena;
	if ( (cadena.equals("")) || (cadena == null) )
         thiscadena = "nada";
	else
		thiscadena = cadena;
      try {
		  for (int i= 0; i < thiscadena.length(); i++)
            dato.write(
			  thiscadena.charAt(i));
      } catch(Exception e) {}

    }
%>

<%! String Saca(String cadena, int num) {
    int longitud, m,i, posinicial, posfinal;
	 m = i = 0;
	 posinicial = 0;
	      while(m <= num) {
         	if (cadena.charAt(i) == ';')
				m++;
			if (m == (num-1))
				posinicial = i;
			i++;
          }
		  posfinal = i;
     if (num ==0) return cadena.substring(posinicial, posfinal-1);
     else return cadena.substring(posinicial+2, posfinal-1);
   }
%>

<%
  sepudo = true;
  try {
   registro = request.getParameter("Registro");
   archivo = request.getParameter("archivo");
   java.io.FileInputStream fis = new java.io.FileInputStream(Global.DOWNLOAD_PATH + archivo);
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
   //java.io.DataInputStream dis = new java.io.DataInputStream(fis);
   java.io.BufferedReader dis = new java.io.BufferedReader(new InputStreamReader(fis));
//VSWF RRG 08-Dic-2008 F

              String cadena = new String("algo");
				String c;
				c = "";

	             int contador = 0;
                      while (cadena !=null)  {
		                cadena = dis.readLine();
		                contador++;
                      }
              int numRegistros;
	          numRegistros = contador-1;
	          dis.close();
        	  fis.close();

  java.io.FileInputStream Fis = new java.io.FileInputStream(Global.DOWNLOAD_PATH +archivo);
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace
  //java.io.DataInputStream Dis = new java.io.DataInputStream(Fis);
  java.io.BufferedReader Dis = new java.io.BufferedReader(new InputStreamReader(fis));
//VSWF RRG 08-Dic-2008 I

   String pasar;
   pasar = "";
  for (int h = 0; h < numRegistros; h++) {
	 pasar = Dis.readLine();
        if (Saca(pasar, 18).equals(registro)) {
			Reg = pasar;
		}

  }
   // out.println(Reg);
   Fis.close();
   Dis.close();
  } catch (Exception e) {}

  %>




<TABLE WIDTH="760" BORDER="0" CELLSPACING="0" CELLPADDING="0" >
 <FORM  NAME="Forma" METHOD="GET" ACTION="Modifica.jsp">

 <tr>
   <td align="center">
     <table width="680" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td rowspan="2">

           <table width="660" border="0" cellspacing="2" cellpadding="3">
		   <P class="tabmovtex">* El llenado de estos campos es obligatorio

<!------------------------------Datos del Proveedor-------------------------------->

			<tr>
	         <td class="tittabdat" colspan="2"> Capture los datos del proveedor </td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD></TD>
    <TD class="tabmovtexbol" nowrap align="CENTER">
		<INPUT TYPE="RADIO" NAME="Persona" VALUE="Fsica" checked><B>Persona f&iacute;sica</B>
		<INPUT TYPE="RADIO" NAME="Persona" VALUE="Moral"><B>Persona moral</B>
	</TD>
    <TD></TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Clave de Proveedor
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Codigo del cliente
     </TD>
    </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vClave" value = "<%=Saca(Reg,0)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCodigo" value = "<%=Saca(Reg,1)%>">
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Apellido Paterno
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Apellido Materno
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Apellido Nombre
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vPaterno" value = "<%=Saca(Reg,15)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vMaterno" value = "<%=Saca(Reg,16)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vNombre" value = "<%=Saca(Reg,17)%>">
     </TD>
   </TR>

 	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* RFC
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Homoclave
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vRFC" value = "<%=Saca(Reg,18)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vHomoclave" value = "<%=Saca(Reg,19)%>">
     </TD>
   </TR>

   <TR>
	 <TD  class="tabmovtexbol" nowrap>
		Persona de contacto en la empresa:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		N&uacute;mero de cliente para Factoraje Santander:
     </TD>
    </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vContacto" value = "<%=Saca(Reg,2)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vFactoraje" value = "<%=Saca(Reg,3)%>">
     </TD>
   </TR>
 </TABLE>
	</td>
  </tr>


			<tr>
			    <td class="tittabdat" colspan="2"> Domicilio</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap>
		* Calle y n&uacute;mero
	</TD>
    <TD class="tabmovtexbol" nowrap>
		* Colonia
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vCalle" value = "<%=Saca(Reg,20)%>">
	</TD>
	</TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vColonia" value = "<%=Saca(Reg,21)%>">
	</TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Deleg. o Municipio
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Cd. o Poblaci&oacute;n
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vDelegacion" value = "<%=Saca(Reg,22)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="Ciudad" value = "<%=Saca(Reg,23)%>">
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* C.P.
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Estado:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* Pa&iacute;s
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCodigoPostal" value ="<%=Saca(Reg,24)%>">
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vEstado" value = "<%=Saca(Reg,25)%>">
		  <OPTION VALUE="AGS"> AGUASCALIENTES
<OPTION VALUE="ALBM"> ALABAMA
<OPTION VALUE="ALBR"> ALBERTA
<OPTION VALUE="ALSK"> ALASKA
<OPTION VALUE="ARKS"> ARKANSAS
<OPTION VALUE="ARZN"> ARIZONA
<OPTION VALUE="BCN">  BAJA CALIFORNIA NORTE
<OPTION VALUE="BCS">  BAJA CALIFORNIA SUR
<OPTION VALUE="BRCO"> BRITISH COLUMBIA
<OPTION VALUE="CALI"> CALIFORNIA
<OPTION VALUE="CAMP">CAMPECHE
<OPTION VALUE="CHIH">CHIHUAHUA
<OPTION VALUE="CHIS">CHIAPAS
<OPTION VALUE="COAH">COAHUILA
<OPTION VALUE="COL">COLIMA
<OPTION VALUE="COLO">COLORADO
<OPTION VALUE="CONN">CONNECTICUT
<OPTION VALUE="DELW">DELAWARE
<OPTION VALUE="DF">DISTRITO FEDERAL
<OPTION VALUE="DGO">DURANGO
<OPTION VALUE="FLOR">FLORIDA
<OPTION VALUE="GEOR">GEORGIA
<OPTION VALUE="GRO">GUERRERO
<OPTION VALUE="GTO">GUANAJUATO
<OPTION VALUE="HGO">HIDALGO
<OPTION VALUE="IDAH">IDAHO
<OPTION VALUE="ILLN">ILLINOIS
<OPTION VALUE="INDI">INDIANA
<OPTION VALUE="IOW">IOWA
<OPTION VALUE="JAL">JALISCO
<OPTION VALUE="KANS">KANSAS
<OPTION VALUE="KENT">KENTUCKY
<OPTION VALUE="LOUS">LOUISIANA
<OPTION VALUE="MAIN">MAINE
<OPTION VALUE="MANI">MANITOBA
<OPTION VALUE="MCHG">MICHIGAN
<OPTION VALUE="MEX">MEXICO
<OPTION VALUE="MICH">MICHOACAN
<OPTION VALUE="MINN">MINNESOTA
<OPTION VALUE="MISI">MISSISSIPPI
<OPTION VALUE="MISU">MISSOURI
<OPTION VALUE="MONT">MONTANA
<OPTION VALUE="MOR">MORELOS
<OPTION VALUE="MRLD">MARYLAND
<OPTION VALUE="MSCH">MASSACHUSETTS
<OPTION VALUE="NAY">NAYARIT
<OPTION VALUE="NBRU">NEW BRUNSWICK
<OPTION VALUE="NEBR">NEBRASKA
<OPTION VALUE="NEVD">NEVADA
<OPTION VALUE="NFND">NEW FOUNDLAND
<OPTION VALUE="NHAM">NEW HAMPSHIRE
<OPTION VALUE="NJSY">NEW JERSEY
<OPTION VALUE="NL">NUEVO LEON
<OPTION VALUE="NMEX">NEW MEXICO
<OPTION VALUE="NOCA">NORTH CAROLINA
<OPTION VALUE="NODA">NORTH DAKOTA
<OPTION VALUE="NOSC">NOVA SCOTIA
<OPTION VALUE="NWTE">NORTHWEST TERRITORIE
<OPTION VALUE="NY">NEW YORK
<OPTION VALUE="OAX">OAXACA
<OPTION VALUE="OHIO">OHIO
<OPTION VALUE="OKHM">OKLAHOMA
<OPTION VALUE="ONTA">ONTARIO
<OPTION VALUE="OREG">OREGON
<OPTION VALUE="PEIS">PRINCE EDWARDS IS.
<OPTION VALUE="PENN">PENNSYLVANIA
<OPTION VALUE="PUE">PUEBLA
<OPTION VALUE="QRO">QUERETARO
<OPTION VALUE="QROO">QUINTANA ROO
<OPTION VALUE="QUEB">QUEBEC
<OPTION VALUE="RHIS">RHODE ISLAND
<OPTION VALUE="SASK">SASKATCHEWAN
<OPTION VALUE="SIN">SINALOA
<OPTION VALUE="SLP">SAN LUIS POTOSI
<OPTION VALUE="SOCA">SOUTH CAROLINA
<OPTION VALUE="SODA">SOUTH DAKOTA
<OPTION VALUE="SON">SONORA
<OPTION VALUE="TAB">TABASCO
<OPTION VALUE="TAMS">TAMAULIPAS
<OPTION VALUE="TENN">TENNESSEE
<OPTION VALUE="TEX">TEXAS
<OPTION VALUE="TLAX">TLAXCALA
<OPTION VALUE="UTAH">UTAH
<OPTION VALUE="VER">VERACRUZ
<OPTION VALUE="VIRG">VIRGINIA
<OPTION VALUE="VRMT">VERMONT
<OPTION VALUE="WASH">WASHINGTON
<OPTION VALUE="WEVI">WEST VIRGINIA
<OPTION VALUE="WISC">WISCONSIN
<OPTION VALUE="WYOM">WYOMING
<OPTION VALUE="XXX">Estado inexistente
<OPTION VALUE="YUC">YUCATAN
<OPTION VALUE="YUKN">YUKON
<OPTION VALUE="ZAC">ZACATECAS
		</SELECT>
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vPais" value = "<%=Saca(Reg,26)%>">
		<OPTION VALUE="ALEM">ALEMANIA
<OPTION VALUE="ANDO">ANDORRA
<OPTION VALUE="ANGO">ANGOLA
<OPTION VALUE="ARGL">ARGELIA
<OPTION VALUE="ARGN">ARGENTINA
<OPTION VALUE="ARSA">ARABIA SAUDITA
<OPTION VALUE="ARUB">ARUBA
<OPTION VALUE="AUS ">AUSTRIA
<OPTION VALUE="AUST">AUSTRALIA
<OPTION VALUE="BAHA">BAHAMAS
<OPTION VALUE="BAHR">BAHREIN
<OPTION VALUE="BANG">BANGLADESH
<OPTION VALUE="BARB">BARBADOS
<OPTION VALUE="BELG">BELGICA
<OPTION VALUE="BELI">BELICE
<OPTION VALUE="BENI">BENIN
<OPTION VALUE="BERM">BERMUDAS
<OPTION VALUE="BOLI">BOLIVIA
<OPTION VALUE="BOTZ">BOTZWANA
<OPTION VALUE="BRAS">BRASIL
<OPTION VALUE="BRUN">BRUNEI
<OPTION VALUE="CAII">CAIMAN IS.
<OPTION VALUE="CAME">CAMERUN
<OPTION VALUE="CANA">CANADA
<OPTION VALUE="CHEC">CHECOSLOVAQUIA
<OPTION VALUE="CHIL">CHILE
<OPTION VALUE="CHIN">CHINA
<OPTION VALUE="CHIP">CHIPRE
<OPTION VALUE="CMAR">COSTA DE MARFIL
<OPTION VALUE="COLO">COLOMBIA
<OPTION VALUE="CORS">COREA DEL SUR
<OPTION VALUE="CRIC">COSTA RICA
<OPTION VALUE="CUBA">CUBA
<OPTION VALUE="CURZ">CURAZAO
<OPTION VALUE="DINA">DINAMARCA
<OPTION VALUE="DJIB">DJIBOUTI
<OPTION VALUE="EARU">EMIRATOS ARABES UNID
<OPTION VALUE="ECUA">ECUADOR
<OPTION VALUE="EGIP">EGIPTO
<OPTION VALUE="ELSA">EL SALVADOR
<OPTION VALUE="ESCO">ESCOCIA
<OPTION VALUE="ESPA">ESPANA
<OPTION VALUE="ETIO">ETIOPIA
<OPTION VALUE="FERI">FEROE IS.
<OPTION VALUE="FIIS">FIJI IS.
<OPTION VALUE="FILI">FILIPINAS
<OPTION VALUE="FIND">FINLANDIA
<OPTION VALUE="FRAN">FRANCIA
<OPTION VALUE="GABO">GABON
<OPTION VALUE="GAMB">GAMBIA
<OPTION VALUE="GRAN">GRANADA
<OPTION VALUE="GREC">GRECIA
<OPTION VALUE="GROE">GROENLANDIA
<OPTION VALUE="GUAM">GUAM
<OPTION VALUE="GUAT">GUATEMALA
<OPTION VALUE="GUFR">GUAYANA FRANCESA
<OPTION VALUE="GUIN">GUINEA
<OPTION VALUE="GUYA">GUYANA
<OPTION VALUE="HAIT">HAITI
<OPTION VALUE="HOKO">HONG KONG
<OPTION VALUE="HOLA">HOLANDA
<OPTION VALUE="HOND">HONDURAS
<OPTION VALUE="HUNG">HUNGRIA
<OPTION VALUE="INDI">INDIA
<OPTION VALUE="INDO">INDONESIA
<OPTION VALUE="IRAK">IRAK
<OPTION VALUE="IRAN">IRAN
<OPTION VALUE="IRLA">IRLANDA
<OPTION VALUE="ISLA">ISLANDIA
<OPTION VALUE="ISRA">ISRAEL
<OPTION VALUE="ITAL">ITALIA
<OPTION VALUE="JAMA">JAMAICA
<OPTION VALUE="JAP ">JAPON
<OPTION VALUE="JORD">JORDANIA
<OPTION VALUE="KATA">KATAR
<OPTION VALUE="KENI">KENIA
<OPTION VALUE="KUWA">KUWAIT
<OPTION VALUE="LESO">LESOTHO
<OPTION VALUE="LIBA">LIBANO
<OPTION VALUE="LIBE">LIBERIA
<OPTION VALUE="LIBI">LIBIA
<OPTION VALUE="LIEC">LIECHTENSTEIN
<OPTION VALUE="LUXE">LUXEMBURGO
<OPTION VALUE="MADA">MADAGASCAR
<OPTION VALUE="MALA">MALASIA
<OPTION VALUE="MALI">MALI
<OPTION VALUE="MALT">MALTA
<OPTION VALUE="MALW">MALAWI
<OPTION VALUE="MARR">MARRUECOS
<OPTION VALUE="MART">MARTINICA
<OPTION VALUE="MAUC">MAURICIO
<OPTION VALUE="MAUT">MAURITANIA
<OPTION VALUE="MEXI">MEXICO
<OPTION VALUE="MICR">MICRONESIA
<OPTION VALUE="MONA">MONACO
<OPTION VALUE="NAMI">NAMIBIA
<OPTION VALUE="NEPA">NEPAL
<OPTION VALUE="NGUI">NUEVA GUINEA
<OPTION VALUE="NICA">NICARAGUA
<OPTION VALUE="NIGE">NIGER
<OPTION VALUE="NIGR">NIGERIA
<OPTION VALUE="NORU">NORUEGA
<OPTION VALUE="NUCA">NUEVA CALEDONIA
<OPTION VALUE="OMAN">OMAN
<OPTION VALUE="PABA">PAISES BAJOS
<OPTION VALUE="PAKI">PAKISTAN
<OPTION VALUE="PANA">PANAMA
<OPTION VALUE="PARA">PARAGUAY
<OPTION VALUE="PERU">PERU
<OPTION VALUE="POLO">POLONIA
<OPTION VALUE="PORT">PORTUGAL
<OPTION VALUE="RDOM">REPUBLICA DOMINICANA
<OPTION VALUE="REUN">REINO UNIDO
<OPTION VALUE="RUMA">RUMANIA
<OPTION VALUE="SAAM">SAMOA AMERICANA
<OPTION VALUE="SENE">SENEGAL
<OPTION VALUE="SEYC">SEYCHELLES
<OPTION VALUE="SING">SINGAPUR
<OPTION VALUE="SIRI">SIRIA
<OPTION VALUE="SLAN">SRI LANKA
<OPTION VALUE="SMAR">SAN MARINO
<OPTION VALUE="SOMA">SOMALIA
<OPTION VALUE="SUAF">SUDAFRICA
<OPTION VALUE="SUAZ">SUAZILANDIA
<OPTION VALUE="SUDA">SUDAN
<OPTION VALUE="SUEC">SUECIA
<OPTION VALUE="SUIZ">SUIZA
<OPTION VALUE="SURI">SURINAM
<OPTION VALUE="TAHI">TAHITI
<OPTION VALUE="TAIL">TAILANDIA
<OPTION VALUE="TAIW">TAIWAN
<OPTION VALUE="TANZ">TANZANIA
<OPTION VALUE="TOGO">TOGO
<OPTION VALUE="TONG">TONGA
<OPTION VALUE="TRYT">TRINIDAD Y TOBAGO
<OPTION VALUE="TUNE">TUNEZ
<OPTION VALUE="TURQ">TURQUIA
<OPTION VALUE="UGAN">UGANDA
<OPTION VALUE="URSS">UNION SOVIETICA
<OPTION VALUE="URUG">URUGUAY
<OPTION VALUE="USA ">ESTADOS UNIDOS
<OPTION VALUE="VENE">VENEZUELA
<OPTION VALUE="YEME">YEMEN
<OPTION VALUE="YUGO">YUGOSLAVIA
<OPTION VALUE="ZAIR">ZAIRE
<OPTION VALUE="ZAMB">ZAMBIA
<OPTION VALUE="ZIMB">ZIMBABWE
<OPTION VALUE="NUZE">NUEVA ZELANDA
<OPTION VALUE="PURI">PUERTO RICO
<OPTION VALUE="BULG">BULGARIA
<OPTION VALUE="ESLO">ESLOVAQUIA
<OPTION VALUE="UCRA">UCRANIA
<OPTION VALUE="CROA">CROACIA
<OPTION VALUE="MONG">MONGOLIA
<OPTION VALUE="RUSI">RUSIA
<OPTION VALUE="ESLN">Eslovenia
<OPTION VALUE="LITU">LITUANIA
<OPTION VALUE="ESTO">ESTONIA
<OPTION VALUE="VIET">VIETNAM
<OPTION VALUE="RCHE">REPUBLICA CHECA
<OPTION VALUE="GANA">GANA
		</SELECT>
     </TD>
   </TR>

 </TABLE>

	</TD>
   </TR>


<!---------------------------Medio de confirmacin---------------------------------->

			<tr>
			    <td class="tittabdat" colspan="2"> Medio de confirmaci&oacute;n</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap>
		* Medio de Informaci&oacute;n
	</TD>
    <TD class="tabmovtexbol" nowrap>
		email
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vMedio" value = "<%=Saca(Reg,4)%>">
		 <OPTION VALUE="3">Correspondencia
		 <OPTION VALUE="1">Email
		 <OPTION VALUE="2">Fax
		</SELECT>
	</TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="Text" NAME="vEmail" value = "<%=Saca(Reg,32)%>">
	</TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Lada o prefijo
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		* N&uacute;mero telefnico
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vLada" value = "<%=Saca(Reg,27)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vTelefonico" value = "<%=Saca(Reg,28)%>">
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Extensi&oacute;n Tel.
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Fax
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Extensi&oacute;n de fax
     </TD>

   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vExTelefono" value = "<%=Saca(Reg,29)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vFax" value = "<%=Saca(Reg,30)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vExFax" value = "<%=Saca(Reg,31)%>">
     </TD>
   </TR>
 </TABLE>

	</TD>
   </TR>

<!------------------------------------Forma de pago-------------------------------->

			<tr>
			    <td class="tittabdat" colspan="2"> Forma de pago</td>
		    </tr>



			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="650" border="0" cellspacing="0" cellpadding="0">

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		* Forma de pago
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		No. cta cheques:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Divisa
     </TD>

   </TR>

	<TR>

    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vForma" value = "<%=Saca(Reg,5)%>">
		 <OPTION VALUE="3">Cheque
		 <OPTION VALUE="4">Orden de pago en ventanilla
		 <OPTION VALUE="0">Se pagara en las oficinas de cliente
		 <OPTION VALUE="2">Transferencia interbancaria
		 <OPTION VALUE="1">Transferencia interna
		</SELECT>
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vCuenta" value = "<%=Saca(Reg,6)%>">
     </TD>

   </TR>


	<TR width=200>
	 <TD  class="tabmovtexbol" nowrap>
		Banco
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Sucursal
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Plaza
     </TD>
   </TR>

	<TR width=200>
    <TD class="tabmovtexbol" nowrap>
		<SELECT NAME="vBanco" value = "<%=Saca(Reg,7)%>" >
		 <OPTION VALUE="AMEX">
		 <OPTION VALUE="AMEX">Afirme
		</SELECT>
     </TD>
    <TD class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vSucursal" value = "<%=Saca(Reg,8)%>">
    </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<SELECT NAME="vPlaza" value = "<%=Saca(Reg,9)%>">
		 <OPTION VALUE="30088">
		 <OPTION VALUE="30088">Acambay, M&eacute;x
		</SELECT>
     </TD>
   </TR>


	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		L&iacute;mite de financiamiento:
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		No. de deudores activos:
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vLimite" value = "<%=Saca(Reg,10)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vDeudores" value = "<%=Saca(Reg,11)%>">
     </TD>
   </TR>


   	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		Volumen anual de ventas
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		Importe medio de facturas emitidas
     </TD>
   </TR>

	<TR>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vVolumen" value = "<%=Saca(Reg,12)%>">
     </TD>
	 <TD  class="tabmovtexbol" nowrap>
		<INPUT TYPE="TEXT" NAME="vImporte" value = "<%=Saca(Reg,13)%>">
     </TD>
   </TR>

 </TABLE>

	</TD>
   </TR>


	<tr align="center">
       <td valign="top" colspan="2">
          <table width="650" border="0" cellspacing="0" cellpadding="0">

			<tr><td>
				<P class="tabmovtex">* El llenado de estos campos es obligatorio
			</td></tr>

		  <tr>
		   <td align="center" valign="middle" width="66">

			  <A href = "javascript:js_mandar();" border=0><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="66" height="22"></a><A href = "" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="77" height="22"></a><A href = "mantenimiento2.jsp" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77" height="22"></a>


         </td>
		  </tr>

		  </table>
		</td>
	</tr>

 </TABLE>

	</TD>
   </TR>
</TABLE>

 </FORM>
</TABLE>


<% session.setAttribute("registroM", registro);
   session.setAttribute("archivoM", archivo);
   %>


</BODY>
</HTML>