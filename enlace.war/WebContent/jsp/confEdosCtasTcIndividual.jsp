<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="mx.altec.enlace.bita.BitaConstants"%>
<%@page import="java.util.ArrayList, java.util.List"%>
<%@page import="mx.altec.enlace.beans.ConfEdosCtaArchivoBean, mx.altec.enlace.bita.BitaConstants" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
<title>Enlace Banco Santander Mexicano</title>

<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/ConMov.js"></script>
<script type="text/javascript" src="/EnlaceMig/confEdosCtasTcInd.js"></script>
<script type="text/javascript">

	function validarFechaConfig() {
		if (document.frmEdit.hdFechaConfigValida.value == 'NO') {
			<%if ("true".equals(request.getAttribute("suscripPaperless")
								.toString())) {%>
				document.frmEdit.suscripPaperless.checked = true;
			<%} else {%>
				document.frmEdit.suscripPaperless.checked = false;
			<%}%>
				var msjModHoy = "No es posible modificar la opci\u00f3n \"Suscribir a Paperless\" debido a que el d\u00eda de hoy ya se realiz\u00f3 una modificaci\u00f3n";
			<%if ("001".equals(request.getAttribute("tipoOp"))) {%>
				cuadroDialogo(
								msjModHoy
										+ " (por parte de este u otro contrato) para la misma secuencia de domicilio.",
								4);
			<%} else {%>
				cuadroDialogo(msjModHoy + " para esta Tarjeta", 4);
			<%}%>
				return false;
		} else {
			mensajeSeq();
		}
	}

	function mensajeSeq() {
		if (document.frmEdit.suscripPaperless.checked) {
			var msjDeshabilitar = "Al seleccionar esta opci\u00f3n se deshabilitar\u00e1 el env\u00edo a domicilio de los Estados de Cuenta de la";
			<%if ("001".equals(request.getAttribute("tipoOp"))) {%>
				cuadroDialogo(
								msjDeshabilitar
										+ "s Cuentas que conforman su secuencia de domicilio. Para consultar estas cuentas de click en "
										+ "el link del n\u00famero de secuencia de la pantalla",
								1);
			<%} else {%>
				cuadroDialogo(msjDeshabilitar + " Tarjeta seleccionada", 1);
			<%}%>
				}
	}
<%if (request.getAttribute("newMenu") != null)
				out.println(request.getAttribute("newMenu"));%>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
<link rel="stylesheet" href="/EnlaceMig/estilosAdmCtrl.css" type="text/css" />
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" >
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
		<tr valign="top">
			<td width="*">
				<c:if test="${not empty requestScope.MenuPrincipal}">
					${requestScope.MenuPrincipal}
				</c:if>
			</td>
		</tr>
	</table>
	<c:if test="${not empty requestScope.Encabezado}">
		${requestScope.Encabezado}
	</c:if>
	<form name="frmConsultar" method="post"
		action="ConfEdosCtaTcIndServlet?flujoOp=CONS">
		<input type="hidden" name="tipoOp" value="${tipoOp}" />
		<input type="hidden" name="hdnCuentasTarjetas" value="" />
		<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
		<table id="tblConsulta" width="760" border="0" cellspacing="0"
			cellpadding="0"
			style="display: <%="CONS".equals(request.getAttribute("flujoOp"))
					? "block"
					: "none"%> ;">
			<tbody>
				<tr>
					<td width="116">
            		</td>
					<td align="center">
						<table width="550" border="0" cellspacing="2" cellpadding="3">
							<tbody>
								<tr>
									<td class="tittabdat" colspan="2">&nbsp;</td>
								</tr>
								<tr align="center">
									<td class="textabdatcla" colspan="2">
										<table width="550" border="0" cellspacing="0" cellpadding="5">
											<tr>
												<td align="right" class="tabmovtexbol" width="200" nowrap>
													Cuenta / Tarjeta:</td>
												<td width="200"><input type="text" name="filtroCuenta" id="filtroCuenta" style="width: 200px;"/>
												</td>
												<td rowspan="2"><a href="javascript:seleccion();"> <img
														src="/gifs/EnlaceMig/Ir.png" style="border: 0;" alt="Ir" width="84" height="21"/> </a></td>
											</tr>
											<tr>
												<td width="200" align="right" class="tabmovtexbol" nowrap>
													Descripci&oacute;n Cuenta / Tarjeta:</td>
												<td width="200"><input type="text" name="filtroDesc" id="filtroDesc" style="width: 200px;"/></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr align="center">
									<td colspan="2" class="textabdatcla">
								    	<table width="550" cellspacing="0" cellpadding="5" border="0">
								        	<tbody>
								        		<tr>
													<td width="211" align="right" class="tabmovtexbol" width="100" nowrap>
														Seleccionar una Cuenta / Tarjeta:
													</td>
													<td width="400">
														<select style="width: 333px;" class="tabmovtex" id="cuentasTarjetas" name="cuentasTarjetas">
															<c:if test="${fn:length(listaCuentasEdoCta) gt 1}">
																	<option value="-1">
																		Seleccione una cuenta...
																	</option>
																</c:if>

															<c:choose>
																<c:when test="${not empty listaCuentasEdoCta}">
																	<c:forEach items="${listaCuentasEdoCta}" var="cuenta">
																		<c:catch var="catchException">
																			<option value="${cuenta.nomCuenta}">
																				${cuenta.nomCuenta} ${cuenta.nombreTitular}
																			</option>
																		</c:catch>
																		<c:if test = "${catchException != null}">
																		</c:if>
																	</c:forEach>
																	<c:if test="${not empty requestScope.msgOption}">
																		<option value="">
																		${requestScope.msgOption}
																		</option>
																	</c:if>
																</c:when>
																<c:when test="${requestScope.flujoOp == 'CONS'}">
																	<option value="-1">
																			No se encontraron resultados...
																	</option>
																</c:when>
															</c:choose>



														</select>
													</td>
								        		</tr>
								        	</tbody>
								       	</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table width="160" border="0" cellspacing="0" cellpadding="0" style=" background-color: #FFFFFF;" height="25">
						  	<tr>
						    	<td align="center" style=" height: 25px;"><a href="javascript:consulta();"><img src="/gifs/EnlaceMig/gbo25310.gif" style=" border: 0;"/></a></td>
						    </tr>
					    </table>
					</td>
					<td>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<form name="frmEdit" method="post"
		action="ConfEdosCtaTcIndServlet?flujoOp=EDIT">
		<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
		<input type="hidden" name="hdnAcepConvAceptPaper" value="<%=BitaConstants.EA_ACEPT_CONV_ACEPT_PAPERLESS%>" />
		<input type="hidden" name="tipoOp" value="${tipoOp}" /> 
		<input
			type="hidden"
			value="<%=request.getAttribute("suscripPaperless").toString()
					.equals("true") ? "S" : "N"%>"
			name="hdSuscripPaperless" />
	<input
			type="hidden"
			value="<%=request.getAttribute("validaCambioPaperless")%>"
			name="hdValidaCambioPaperless" />
			
		<table width="835" border="0" cellspacing="0" cellpadding="0"
			style="display: <%="EDIT".equals(request.getAttribute("flujoOp"))
					? "block"
					: "none"%> ;">
			<tbody>
				<tr id="idTrCuenta"
					style="display: <%="001".equals(request.getAttribute("tipoOp"))
					? "block"
					: "none"%>;">
					<input type="hidden"
						value='<%=request.getAttribute("segmentoDom")%>'
						name="hdSeqDomicilio" />
					<input type="hidden" value='<%=request.getAttribute("cuenta")%>'
						name="hdCuenta" />
					<input type="hidden"
						value='<%=request.getAttribute("codCliente")%>'
						name="hdCodCliente" />
					<input type="hidden" value='<%=request.getAttribute("domicilio")%>'
						name="hdDomicilio" />
					<input type="hidden"
						value='<%=request.getAttribute("fechaConfigValida")%>'
						name="hdFechaConfigValida" />
					<input type="hidden" value="N" name="hdACAP" />
					<td align="center">
						<table width="670" border="0" cellspacing="2" cellpadding="3">
							<thead>
								<tr>
									<td class="tittabdat" width="123" align="center">Seq</td>
									<td class="tittabdat" width="123" align="center">C&oacute;digo
										Cliente</td>
									<td class="tittabdat" width="123" align="center">Cuenta</td>
									<td class="tittabdat" width="423" align="center">Domicilio
										Registrado</td>
								</tr>
							</thead>
							<tbody>
								<tr class="textabdatobs">
									<td align="center" class="tabmovtexbol" align="center"><a
										href="#"
										title="Descargue las cuentas relacionadas a la secuencia de domicilio"
										onclick="javascript:guardaArchivo('EXPCTA');"> ${requestScope.segmentoDom}
									</a></td>
									<td align="center" class="tabmovtexbol" align="left">${codCliente}</td>
									<td align="center" class="tabmovtexbol" align="left">${cuenta}</td>
									<td align="left" class="tabmovtexbol" align="left">${domicilio}</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr id="idTrTarjeta"
					style="display: <%="003".equals(request.getAttribute("tipoOp"))
					? "block"
					: "none"%>;">
					<td align="center">
						<table width="670" border="0" cellspacing="2" cellpadding="3">
							<input type="hidden" value='<%=request.getAttribute("credito")%>'
								name="hdTarjeta" />
							<input type="hidden"
								value='<%=request.getAttribute("contrato")%>' name="hdContrato" />
							<thead>
								<tr>
									<td class="tittabdat" width="123" align="center">Contrato</td>
									<td class="tittabdat" width="123" align="center">Cr&eacute;dito</td>
									<td class="tittabdat" width="123" align="center">Descripci&oacute;n</td>
									<td class="tittabdat" width="423" align="center">Domicilio
										Registrado</td>
								</tr>
							</thead>
							<tbody>
								<tr class="textabdatobs">
									<td align="center" class="tabmovtexbol" align="center"><a
										href="#"
										title="Descargue las tarjetas relacionadas a la tarjeta y contrato"
										onclick="javascript:guardaArchivo('EXPTARJ');"> ${requestScope.contrato}
									</a></td>
									<td align="center" class="tabmovtexbol" align="left">${credito}</td>
									<td align="center" class="tabmovtexbol" align="left">${descripcion}</td>
									<td align="left" class="tabmovtexbol" align="left">${domicilio}</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="670" cellspacing="2" cellpadding="4" border="0">
					      	<tbody>
					      		<tr class="textabdatcla">
					          		<td align="left" class="tabmovtexbol">
		          						<input type="checkbox" value="S"
										name="suscripPaperless"
										<%="true".equals(request.getAttribute("suscripPaperless")
													.toString()) ? "checked=\"checked\"" : ""%>
										onclick="javascript:validarFechaConfig();" /> Suscribir a
										Paperless
					          		</td>
					      		</tr>
			          		</tbody>
          				</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="670" border="0" cellspacing="2" cellpadding="3">
							<tbody>
								<tr>
									<td class="texencconbol">
										<c:if test="${not empty requestScope.MENSAJE_PROBLEMA_DATOS}">
											${requestScope.MENSAJE_PROBLEMA_DATOS}
										</c:if>
									</td>
								</tr>
							</tbody>
						</table></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td align="center">
						<table width="670" border="0" cellspacing="0" cellpadding="0"
							style=" background-color: #FFFFFF;" height="25">
							<tbody>
								<tr>
									<td align="right" style=" height: 25px">
										<a
											href="javascript:DoPost();"><img
												src="/gifs/EnlaceMig/gbo25280.gif" style=" border: 0;" alt="Aceptar" />
										</a>
									</td>
									<td>
										<a
											href="javascript:Cancelar();"><img style=" border: 0;"
												name="imageField" src="/gifs/EnlaceMig/gbo25190.gif"
												alt="Cancelar" />
										</a>
									</td>
								</tr>
							</tbody>
						</table></td>
				</tr>
			</tfoot>
		</table>
	</form>

	<script type="text/javascript">

	<%if (request.getAttribute("MensajeErr") != null) {%>

	<%=request.getAttribute("MensajeErr")%>

	<%}%>

	</script>

	<div id="window" class="windowUnload">
		<table class="encabezado">
			<tr>
				<td>T&eacute;rminos y condiciones para el servicio paperless</td>
			</tr>
		</table>
		<br></br>

		<table class="tabla" border="0">
			<tr>
				<td width="20px">&nbsp;</td>
				<td><span class="titulo">Convenio de aceptaci&oacute;n
						de condiciones para el servicio Paperless</span>
				</td>
			</tr>
			<tr>
				<td colspan>&nbsp;</td>
			</tr>
			<tr>
				<td width="20px">&nbsp;</td>
				<td><textarea class="textbox" readonly="true">
CONVENIO DE ACEPTACI&Oacute;N DE CONDICIONES PARA EL SERVICIO DE BLOQUEO DE IMPRESI&Oacute;N DE ESTADO DE CUENTA

Estimado Cliente:

En cumplimiento a lo establecido por el C&oacute;digo Fiscal de la Federaci&oacute;n los estados de cuenta podr&aacute;n ser enviados por &eacute;ste medio, por lo que al solicitar su inscripci&oacute;n a &eacute;ste Servicio, usted est&aacute; de acuerdo en no recibir el(los) estado(s) de cuenta impreso(s) en papel a trav&eacute;s de correo de los diferentes productos asociados a su c&oacute;digo de cliente.

Una vez inscrito en el servicio, usted recibir&aacute; la informaci&oacute;n relativa a su estado de cuenta en l&iacute;nea, y &uacute;nicamente tendr&aacute; acceso al mismo a trav&eacute;s de medios electr&oacute;nicos, siguiendo las instrucciones proporcionadas por Banco Santander (M&eacute;xico) S.A., Instituci&oacute;n de Banca M&uacute;ltiple, Grupo Financiero Santander M&eacute;xico.

Usted podr&aacute; acceder a su estado de cuenta electr&oacute;nicamente, siempre que no revoque su consentimiento para recibirlo en l&iacute;nea. Si usted desea volver a recibir su Estado de Cuenta en papel a trav&eacute;s de correo, es necesario que regrese a esta p&aacute;gina y siga las instrucciones que se le indican y su impresi&oacute;n ser&aacute; recibida en el domicilio indicado al mes siguiente que usted haya desactivado la opci&oacute;n de bloqueo de impresi&oacute;n.
Si usted lo desea, podr&aacute; solicitar en cualquiera de nuestras sucursales una copia impresa en papel, de cualquier estado de cuenta con una antigŁedad m&aacute;xima no mayor de 24 meses.

En caso que usted requiera cambiar o actualizar la direcci&oacute;n de correo electr&oacute;nico a d&oacute;nde se le enviar&aacute; las notificaciones, realice el cambio a trav&eacute;s de Enlace en la secci&oacute;n: Administraci&oacute;n > Mant. de Datos Personales. En caso que la direcci&oacute;n de correo electr&oacute;nico que usted proporcione sea incorrecta, inv&aacute;lida o si la misma no puede ser registrada y validada por nuestro sistema, usted no recibir&aacute; la notificaci&oacute;n.

Para cualquier duda o aclaraci&oacute;n, puede comunicarse a Superl&iacute;nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000 a trav&eacute;s de nuestro correo electr&oacute;nico enlace@santander.com.mx  donde uno de nuestros especialistas le brindara el apoyo.
						</textarea></td>
			</tr>
			<tr>
				<td colspan>&nbsp;</td>
			</tr>
			<tr>
				<td width="20px">&nbsp;</td>
				<td><input type="checkbox" name="ac" id="ac" value="ac" />Acepto
					las condiciones.</td>
			</tr>
			<tr>
				<td width="20px">&nbsp;</td>
				<td align="center"><input type="button" class="boton"
					value="Continuar" onClick="javascript:acepto();" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
					class="boton" value="Cerrar" onClick="javascript:cierro();" /></td>
			</tr>
			<tr>
				<td colspan>&nbsp;</td>
			</tr>
			<tr>
				<td width="20px">&nbsp;</td>
				<td>Por favor lea el convenio de aceptaci&oacute;n de
					condiciones para el servicio de Paperless. Se requiere que Usted
					acepte las condiciones para que deje de recibir su(s) estado(s) de
					cuenta de manera impresa en su domicilio. En caso de no estar de
					acuerdo, podr&aacute; aceptar el cambio posteriormente.</td>
			</tr>
		</table>
	</div>
	<div id="sombra" class="sobmbraUnload"></div>
</body>
</html>