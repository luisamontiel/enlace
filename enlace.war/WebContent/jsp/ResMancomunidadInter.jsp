<%@page contentType="text/html"%>
<jsp:useBean id="OperacionesMancomunadasInter" scope="session" class="java.util.ArrayList" />
<html>
<head>
  <title>Banca Virtual</title>
  <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
<!-- Modificaion Paula Hernández -->
  <script language='JavaScript' src='/EnlaceMig/scrImpresion.js'></script>
  <script language='JavaScript1.2' src='/EnlaceMig/fw_menu.js'></script>
  <script language='JavaScript'>
    <!--
    function MM_preloadImages() { //v3.0
      var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_swapImgRestore() { //v3.0
      var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }

    function MM_findObj(n, d) { //v3.0
      var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
      if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
      for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }

    function MM_swapImage() { //v3.0
      var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
       if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }

	function GenerarComprobante(hdnTrama){
	// vswf:meg cambio de NASApp por Enlace 08122008 Enlace
		window.open('/Enlace/jsp/ComprobanteMancInter.jsp?hdnTrama='+hdnTrama,'compWindow','width=580,height=600,toolbar=no,scrollbars=yes,left=210,top=100');
	}

	function Regresar()
	{
		document.location = './AMancomunidadInter';
	}

    <%=session.getAttribute ("newMenu")%>
  </script>
  <% java.util.ListIterator liProcesarAux = OperacionesMancomunadasInter.listIterator ();
    int y = 0;
    while (liProcesarAux.hasNext ())
	{
        Object o = liProcesarAux.next ();
		System.out.println ("****AQUI ESTA EJECUTANDO EL VALOR DE LAS OPERACIONES*** -->");
        try
		{
			if (o instanceof mx.altec.enlace.bo.DatosMancInter)
			{
  %>
  <%%>
  <%
			}
			else
			{
				++y;
  %>
  <%%>
  <%
			}
        } catch (Exception ex) { ex.printStackTrace ();  }
    }
	System.out.println ("MARL Y: " + y);
	System.out.println ("MARL Pendientes: " + session.getAttribute ("Pendientes"));
	System.out.println ("MARL Pendientes: " + request.getAttribute ("Pendientes"));

	System.out.println ("MARL banderaManc: " + session.getAttribute ("banderaManc"));
	int banAux = 0;
	String bandAux = (String)request.getAttribute("banderaManc");
    banAux = Integer.parseInt(bandAux);
	System.out.println ("banAux : "+banAux+ " y : "+ y);

	%>
<link rel='stylesheet' href='/EnlaceMig/consultas.css' target='text/css'>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" background="/gifs/EnlaceMig/gfo25010.gif"
<%if (y < banAux) { %> Onload="setTimeout('forward ();', 2000);"
<% }%>
>

<form name='form' method='POST' action='AMancomunidadInter'>

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
        <td width="*">
		        <!-- MENU PRINCIPAL -->
                <%= session.getAttribute("MenuPrincipal") %>
        </td>
  </tr>
</table>
<%=session.getAttribute ("Encabezado")%>

<br>
<table width='760' border='0' cellpadding='2' cellspacing='3' class='tabfonbla'>
  <tr>
    <td align='center' class='tittabdat'>Cuenta de Cargo</td>
    <td align='center' class='tittabdat'>Cuenta de Abono</td>
    <td align='center' class='tittabdat'>Importe</td>
    <td align='center' class='tittabdat'>Folio de Registro</td>
    <td align='center' class='tittabdat'>Folio de <%=session.getAttribute ("operacion")%></td>
    <td align='center' class='tittabdat'>Fecha de <%=session.getAttribute ("operacion")%></td>
    <td align='center' class='tittabdat'>Estatus</td>
  </tr>
  <%
      java.util.ListIterator liProcesar = OperacionesMancomunadasInter.listIterator ();
      int x = 0;
      while (liProcesar.hasNext ())
  	{
          Object o = liProcesar.next ();
  		System.out.println ("****AQUI ESTA EJECUTANDO EL VALOR DE LAS OPERACIONES*** -->");
          System.out.println ("Instance of DatosMancInter " + (o instanceof mx.altec.enlace.bo.DatosMancInter));
          System.out.println ("Instance of RespManc " + (o instanceof mx.altec.enlace.bo.RespManc));
          try
  		{
  	if (o instanceof mx.altec.enlace.bo.DatosMancInter)
  	{
  %>
			  <%=((mx.altec.enlace.bo.DatosMancInter) o).getFormatoHtmlRes (liProcesar.nextIndex () - 1)%>
			  <%
			  	}
			  	else
			  	{
			  	  ++x;
			  %>
			  <%=((mx.altec.enlace.bo.RespManc) o).getFormatoHTML (liProcesar.nextIndex () - 1)%>
			  <%
			}
        } catch (Exception ex) {  ex.printStackTrace (); }
    }
	System.out.println ("MARL X: " + x);
	System.out.println ("MARL Pendientes: " + session.getAttribute ("Pendientes"));
	System.out.println ("MARL Pendientes: " + request.getAttribute ("Pendientes"));

	System.out.println ("MARL banderaManc: " + session.getAttribute ("banderaManc"));
	int ban = 0;
	String band = (String)request.getAttribute("banderaManc");
    ban = Integer.parseInt(band);
	System.out.println ("ban : "+ban+ " x : "+ x);
      if (x < ban ) {
  %>
  <script language='JavaScript'>
	<%System.out.println ("MARL x: " + x); %>
	  function forward ()
	  {
		  document.location = './OperacionesManc?Next=<%=x%>';
	  }
  </script>
  <%
    } else {
  %>
  <tr>
    <td colspan='7' align='center'>
	  <table width='100%' border='0'>
		<tr>
			<td align='right'>
			  <a href='javascript:scrImpresion ();'>
				<img border='0' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a>
			</td>
			<td align='left'>
				<a href='javascript:Regresar ();'>
				<img border='0' src='/gifs/EnlaceMig/gbo25320.gif' width='83' height='22' alt='Regresar'></a>
			</td>
		</tr>
		</table>
    </td>
  <tr>
  <%
    }
  %>
</table>
</form>
</body>
</html>
