<html>
<head>
<title>Mantenimiento Beneficiarios-Alta</TITLE>
<meta http-equiv="Content-Type" content="text/html">
<!--<SCRIPT LANGUAGE="JavaScript" SRC="/EnlaceMig/ConMov.js">
</SCRIPT>  -->
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript"src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript"src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javascript"src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript">

/*
 6 N Requerido Numerico
 5 T Requerido Texto
 3 S Requerido Seleccion
 2 n Numerico
 1 t Texto
 0 i Ignorado
*/

 errores = new Array(	"La Clave del Beneficiario",
						"El Nombre del Beneficiario",
						"fecha",
						"oculto");

 evalua = "EEXX";

function valida(){
	var nombre= document.frmpdAltaBenef.txtBeneficiario.value;
	var length = nombre.length;
	var i;
	var validas=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (i = 0; i < length; i++) {
        car = nombre.charAt (i);
		if(validas.indexOf(car) == -1)
			{
            //cuadroDialogo("El nombre del beneficiario no es valido");
			cuadroDialogo("El nombre del beneficiario no debe contener n&uacute;meros o caracteres especiales", 3);

            return false;
        }

	}
	return true;
}

function limpia()
{
	document.frmpdAltaBenef.txtCveBeneficiario.value="";
	document.frmpdAltaBenef.txtBeneficiario.value="";
}

function ValidaForma(forma)
 {

      //**********************************
      var aux ="";
      aux = document.frmpdAltaBenef.txtCveBeneficiario.value;
      if (aux=="0" || aux == "00" || aux == "000" || aux == "0000" || aux == "00000" || aux == "000000" || aux == "0000000" || aux == "00000000" || aux == "000000000" || aux == "0000000000" || aux == "00000000000" || aux == "000000000000" || aux == "0000000000000" )
       {
          cuadroDialogo("Favor de ingresar un valor valido en clave",3);
	  return false;
       }
      //**********************************
     if(ValidaTodo(forma,evalua,errores)){
		 forma.ventana.value=1;
		 forma.submit();
	 }

}


function confirmacion1(){
		if(confirm("Esta seguro de que desea dar de baja al beneficiario?")){
			if(confirm("A partir de este momento, no se pagar� al beneficiario. �Desea continuar?")){
			frmpdAltaBenef.ventana.value=1;

			} else { location.reload() }
		} else { location.reload() }
}

function mayusculas(){
	var nom=document.frmpdAltaBenef.txtBeneficiario.value;
	document.frmpdAltaBenef.txtBeneficiario.value=nom.toUpperCase();
}
</script>



<!-- JavaScript del App -->
<SCRIPT LANGUAGE="JavaScript">
<!--

<!---------------------------------------------------------------------------------------------->

<%= request.getAttribute("newMenu") %>

<!---------------------------------------------------------------------------------------------->


 function WindowCalendar(){
  if(!document.FrmMov.deldia[0].checked){
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMov.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
  }else{
    cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del dia", 1);

  }

}

function WindowCalendar1(){
  if(!document.FrmMov.deldia[0].checked){
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMov1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
  }else{
    cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del dia", 1);
  }
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</SCRIPT>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</HEAD>


<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="limpia(); MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
background="/gifs/EnlaceMig/gfo25010.gif">


<!---------------------------------- Menu principal y Encabezado ------------------------------->

<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr valign=top>
    <td width="*">
		<!-- MENU PRINCIPAL -->
        <%= request.getAttribute("MenuPrincipal") %>
    </td>
  </tr>
</table>
<!-- #EndLibraryItem --><!-- #BeginEditable "Contenido" -->

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag"><%= request.getAttribute("Encabezado") %></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<!-------------------------------- termina Menu principal y Encabezado --------------------------->


<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">

<FORM  NAME="frmpdAltaBenef" onSubmit="return ValidaForma(this); "  method=post action="pdAltaBenef">
    <tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3" width="100">
          <tr>
            <td class="tittabdat" nowrap>
              Capture los datos del beneficiario</td>
          </tr>
          <tr align="center" valign="middle">
            <td class="textabdatcla" align="left" valign="top">
              <table width="100%" border="0" cellspacing="0" cellpadding="3">
			   <tr>
                  <td class="tabmovtex11">Clave:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <input type="text" name="txtCveBeneficiario" size="14" class="tabmovtex" maxlength="13"><% if(request.getAttribute("txtCveBeneficiario")!= null) out.print(request.getAttribute("txtCveBeneficiario")); %>
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex11">Nombre:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <input type="text" name="txtBeneficiario" maxlength=60 size="35" class="tabmovtex" onblur='mayusculas()'>
					<% if(request.getAttribute("txtBeneficiario") != null) out.print(request.getAttribute("txtBeneficiario")); %>
                  </td>
                </tr>
				<tr>
                  <td class="tabmovtex11">Fecha de Alta:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <input type="text" name="txtFechaAlta" size="12" class="tabmovtex" onFocus='blur();' value=<% if(request.getAttribute("Fecha") != null) out.print(request.getAttribute("Fecha")); %>>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" width="66" height="22">
          <tr>							  <!-- limpia(); -->
            <td align="center" width="87"><a href="javascript:if(valida())ValidaForma(document.frmpdAltaBenef); limpia() ">
              <img border="0" name="Alta" src="/gifs/EnlaceMig/gbo25480.gif" width="66" height="22" alt="Alta"></a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
	<INPUT TYPE="hidden" NAME="ventana" VALUE="0">
  </form>
</table>


	<!-- CONTENIDO FINAL -->

<SCRIPT PURPOSE="EVENT_WIRING" LANGUAGE="JAVASCRIPT">
<!--
function nabOnLoad() {
document.links.Link2 = document.links[0];
document.links.Link3 = document.links[1];
document.links.Link4 = document.links[2];
document.links.Link5 = document.links[3];
document.links.Link6 = document.links[4];
document.links.Link7 = document.links[5];
//   document.FrmMov.txtNomRazSoc.onfocus = document_FrmMov_txtNomRazSoc_onfocus;
//   document.FrmMov.txtFechaAlta.onfocus = document_FrmMov_txtFechaAlta_onfocus;
/*
   document.links.Link2.onmouseout = document_links_Link2_onmouseout;
   document.links.Link2.onmouseover = document_links_Link2_onmouseover;
   document.links.Link3.onmouseout = document_links_Link3_onmouseout;
   document.links.Link3.onmouseover = document_links_Link3_onmouseover;
   document.links.Link4.onmouseout = document_links_Link4_onmouseout;
   document.links.Link4.onmouseover = document_links_Link4_onmouseover;
   document.links.Link5.onmouseout = document_links_Link5_onmouseout;
   document.links.Link5.onmouseover = document_links_Link5_onmouseover;
   document.links.Link6.onmouseout = document_links_Link6_onmouseout;
   document.links.Link6.onmouseover = document_links_Link6_onmouseover;
   document.links.Link7.onmouseout = document_links_Link7_onmouseout;
   document.links.Link7.onmouseover = document_links_Link7_onmouseover;
*/
   if ( this.nabOldOnLoad != null ) {
      nabOldOnLoad();
   }
}
var nabOldOnLoad = this.onload;
onload = nabOnLoad;
//-->
</SCRIPT></BODY>
</HTML>

<SCRIPT LANGUAGE="JavaScript">
<!--
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</SCRIPT>
