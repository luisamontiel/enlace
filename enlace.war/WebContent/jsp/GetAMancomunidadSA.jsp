<%@page contentType="text/html"%>
<jsp:useBean id="ResultadosMancomunidadSA" scope="session" class="java.util.ArrayList"/>
<%
	Integer IndexMancomunidad = (Integer) session.getAttribute("IndexMancomunidadSA");
 %>

<%!
    public static final String ANTERIORES = "<a href='javascript:anteriores ();'>Anteriores&nbsp;";
    public static final String SIGUIENTES = "<a href='javascript:siguientes ();'>Siguientes&nbsp;";
%>
<html>
<head>
  <title>Banca Virtual</title>
  <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
  <script language='JavaScript' src='/EnlaceMig/cuadroDialogo.js'></script>
  <script language='JavaScript' src='/EnlaceMig/scrImpresion.js'></script>
  <script language='JavaScript1.2' src='/EnlaceMig/fw_menu.js'></script>
  <script language='JavaScript'>

    <%-- Inicio Java Script App --%>
    function MM_preloadImages() { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_swapImgRestore() { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }

    function MM_findObj(n, d) { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }

    function MM_swapImage() { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    <%=session.getAttribute ("newMenu")%>
    <%-- Fin Java Script App --%>

    function anteriores () {
        document.FrmAutoriza.action = "GetAMancomunidadSA";
        document.FrmAutoriza.OpcPag.value = "<%=mx.altec.enlace.servlets.GetAMancomunidadSA.PREV%>";
        document.FrmAutoriza.submit ();
    }

    function siguientes () {
        document.FrmAutoriza.action = "GetAMancomunidadSA";
        document.FrmAutoriza.submit ();
    }

    function autoriza (op) {
    	if(op==1) {
			document.FrmAutoriza.opSubmit.value = "1";
        	if (validaFolios ()) document.FrmAutoriza.submit ();
        }else {
        	document.FrmAutoriza.opSubmit.value = "2";
        	if (validaFolios ()) document.FrmAutoriza.submit ();
        }
    }

    function validaFolios () {
        var total = document.FrmAutoriza.elements.length;
        var j = 0;
        for (i = 0; i < total; i++) {
            var e = document.FrmAutoriza.elements[i];
            if (e.type == 'radio') {
                if (e.checked) {
                    j++;
                }
            }
        }

        if (j == 0) {
            cuadroDialogo ("Usted no ha seleccionado ningun archivo.\n\nPor favor seleccione un archivo.",1);
            return false;
        }
        return true;
    }
  </script>
  <link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>
</head>
<body topmargin='0' leftmargin='0' marginheight='0' marginwidth='0' bgcolor='#ffffff' onload='MM_preloadImages("/gifs/EnlaceMig/gbo25131.gif","/gifs/EnlaceMig/gbo25111.gif","/gifs/EnlaceMig/gbo25151.gif","/gifs/EnlaceMig/gbo25031.gif","/gifs/EnlaceMig/gbo25032.gif","/gifs/EnlaceMig/gbo25051.gif","/gifs/EnlaceMig/gbo25052.gif","/gifs/EnlaceMig/gbo25091.gif","/gifs/EnlaceMig/gbo25092.gif","/gifs/EnlaceMig/gbo25012.gif","/gifs/EnlaceMig/gbo25071.gif","/gifs/EnlaceMig/gbo25072.gif","/gifs/EnlaceMig/gbo25011.gif")' background='/gifs/EnlaceMig/gfo25010.gif'>
  <table border='0' cellpadding='0' cellspacing='0' width='571'>
    <tr valign='top'>
      <td width='*'>
        <%=session.getAttribute ("MenuPrincipal")%>
      </td>
    </tr>
  </table>
  <%=session.getAttribute ("Encabezado")%>
  <br>
  <form onsubmit='' method='POST' action='AutorizaAMancomunidadSA' name='FrmAutoriza'>
  	<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
    <input type='hidden' name='OpcPag' value='<%=mx.altec.enlace.servlets.GetAMancomunidadSA.NEXT%>'>
    <input type='hidden' name='opSubmit' value='1'>
    <table width='1000' border='0' cellspacing='2' cellpadding='3' class='tabfonbla'>
      <tr>
        <td colspan='12' class='texenccon'>Total de registros&nbsp;:&nbsp;<%=ResultadosMancomunidadSA.size ()%></td>
      </tr>
      <tr>
        <td colspan='12' class='texenccon'>Periodo del&nbsp;<%=request.getSession ().getAttribute ("FechaIni")%>&nbsp;al&nbsp;<%=request.getSession ().getAttribute ("FechaFin")%></td>
      </tr>
      <tr>
        <td align='right' class='tittabdat'>
          &nbsp;
        </td>
        <td align='center' class='tittabdat'>
          Fecha Reg.
        </td>
        <td align='center' class='tittabdat'>
          Tipo Operaci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Fecha Auto.
        </td>
        <td align='center' class='tittabdat'>
          Folio archivo
        </td>
        <td align='center' class='tittabdat'>
          Registros
        </td>
        <td align='center' class='tittabdat'>
          Importe
        </td>
        <td align='center' class='tittabdat'>
          Usuario Registr&oacute;
        </td>
		<td align='center' class='tittabdat'>
		  Usuario Autorizaci&oacute;n 2
		</td>
		<td align='center' class='tittabdat'>
		  Usuario Autorizaci&oacute;n 3
		</td>
		<td align='center' class='tittabdat'>
		  Usuario Autorizaci&oacute;n 4
		</td>
		<td align='center' class='tittabdat'>
          Estatus
        </td>
      </tr>
      <%
        int paginacion = 50;
        int index = IndexMancomunidad.intValue ();
        int siguiente = index + paginacion;
        int restantes = (ResultadosMancomunidadSA.size () - (index + 50)) > paginacion ? paginacion : ResultadosMancomunidadSA.size () - (index + 50);
        System.out.println ("Indice inicio: " + index);
        System.out.println ("Restantes: " + restantes);
        java.util.ListIterator liResultados = ResultadosMancomunidadSA.listIterator (index);

            while (liResultados.hasNext () && index < siguiente) {
            Object o = liResultados.next();
            try {
      %>
      <%= new mx.altec.enlace.bo.DatosMancSA().getHTMLFormat(index, (mx.altec.enlace.bo.DatosMancSA) o)%>
      <%
            } catch (Exception ex) {
				System.out.println ("****ENTRO A UN A EXCEPTION****: " + ex.getMessage() );
                ex.printStackTrace ();
                break;
            }
            index++;
        }
        System.out.println ("Indice final: " + index);
      %>
      <tr>
        <td colspan='12' align='center' class='texenccon'>
          <%=IndexMancomunidad.intValue () == 0 ? "" : ANTERIORES + paginacion + "</a>&nbsp;"%><%=liResultados.hasNext () ? SIGUIENTES + restantes + "</a>" : ""%>
        </td>
      </tr>
	  <tr>
        <td colspan='12' align='center' class='texenccon'>
          <a href='javascript:autoriza(1);'><img border='0' src='/gifs/EnlaceMig/gbo25430.gif' width='92' height='22' alt='Autorizar'></a><a href='javascript:autoriza(2);'><img border='0' src='/gifs/EnlaceMig/gbo25248.gif' width='85' height='22' alt='Ver Detalle'></a><a href='<%=session.getAttribute("ArchivoManc")%>'><img border='0' src='/gifs/EnlaceMig/gbo25230.gif' width='85' height='22' alt='Exportar'></a><A href='AMancomunidadSA?flujo=EAMC'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
        </td>
      </tr>
    </table>
  </form>
<script>
<%= request.getAttribute("mensajeError") %>
</script>
</body>
</html>