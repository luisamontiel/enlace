<%
	String numContrato = (String)request.getParameter("numContrato");
	String numUsuario  = (String)request.getParameter("numUsuario");
	String nomUsuario  = (String)request.getParameter("nomUsuario");
	String empresa = (String)request.getParameter("empresa");
	String convenio = (String)request.getParameter("convenio");
	String fecha       = (String)request.getParameter("fecha");
	String hora        = (String)request.getParameter("hora");
	String folioOper   = (String)request.getParameter("folioOper");
	String referencia = (String)request.getParameter("referencia");
	String cuentaCargo = (String)request.getParameter("cuentaCargo");
	String cuentaAbono = (String)request.getParameter("cuentaAbono");
	String titularCuentaCargo = (String)request.getParameter("titularCuentaCargo");
	String importe = (String)request.getParameter("importe");
	String estatus = (String)request.getParameter("estatusOper");

	String moneda = cuentaAbono.substring(0,2);
	System.out.println("MONEDA ........." + moneda);

	System.out.println("===============================================================================");
	System.out.println("numeroContrato  ....... [" + numContrato + "]");
	System.out.println("nombreUsuario   ....... [" + nomUsuario + "]");
	System.out.println("numeroUsuario   ....... [" + numUsuario + "]");
	System.out.println("Valores del comprobante desde comprobanteMicrositio.jsp          ");
	System.out.println("empresa      ....... [" + empresa + "]");
	System.out.println("convenio     ....... [" + convenio + "]");
	System.out.println("cuentaCargo  ....... [" + cuentaCargo + "]");
	System.out.println("referencia   ....... [" + referencia + "]");
	System.out.println("importe      ....... [" + importe + "]");
	System.out.println("fecha        ....... [" + fecha + "]");
	System.out.println("hora        ....... [" + hora + "]");
	System.out.println("===============================================================================");
%>

<html>
<head>
<title>Santander</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript" src="/EnlaceMig/Convertidor.js"></script>
<link rel="stylesheet" href="/EnlaceMig/pagos.css" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF"leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('/gifs/EnlaceMig/cerrar_over.jpg','/gifs/EnlaceMig/imprimir_over.jpg')">
<table width="380" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="2"><img src="/gifs/EnlaceMig/enlace_logo.jpg" width="241" height="40"></td>
  </tr>
  <tr>
    <td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="8">
        <tr>
          <td><p><strong><span class="titulorojo">Comprobante de Operaci&oacute;n</span></strong></p></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellspacing="4" cellpadding="4">
              <tr>
                <td align="right" bgcolor="#F0EEEE"><strong>Contrato:</strong></td>
                <td bgcolor="#F0EEEE"><%=numContrato%></td>
              </tr>
              <tr>
                <td align="right" bgcolor="#F0EEEE"><strong>Usuario:</strong></td>
                <td bgcolor="#F0EEEE"><script>document.write(cliente_7To8("<%=numUsuario.trim()%>"));</script>| <%=nomUsuario%></td>
              </tr>
              <tr>
                <td align="right" bgcolor="#F0EEEE"><strong>Empresa:</strong></td>
                <td bgcolor="#F0EEEE"><%=convenio%> - <%=empresa%></td>
              </tr>
 		      <!--
              <tr>
                <td bgcolor="#F0EEEE"> <div align="right"><strong>N&uacute;mero
                    de Afiliaci&oacute;n:</strong></div></td>
                <td bgcolor="#F0EEEE">1234567890</td>
              </tr>
			  -->
              <tr>
                <td bgcolor="#F0EEEE"> <div align="right"><strong>Fecha:</strong></div></td>
                <td bgcolor="#F0EEEE"><%=fecha%></td>
              </tr>
              <tr>
                <td bgcolor="#F0EEEE"> <div align="right"><strong>Hora:</strong></div></td>
                <td bgcolor="#F0EEEE"><%=hora%></td>
              </tr>
              <tr>
                <td bgcolor="#F0EEEE"> <div align="right"><strong>Folio de la
                    Transacci&oacute;n:</strong></div></td>
                <td bgcolor="#F0EEEE"><%=folioOper%></td>
              </tr>
              <tr>
                <td bgcolor="#F0EEEE"> <div align="right"><strong>Referencia:</strong></div></td>
                <td bgcolor="#F0EEEE"><%=referencia%></td>
              </tr>
              <tr>
                <td bgcolor="#F0EEEE"> <div align="right"><strong>Cuenta de cargo:</strong></div></td>
                <td bgcolor="#F0EEEE"><%=cuentaCargo%>| <%=titularCuentaCargo%></td>
              </tr>
              <tr>
                <td bgcolor="#F0EEEE"> <div align="right"><strong>Importe:</strong></div></td>
				<%
				   String verimporte="";
				   if(moneda.equals("49") || moneda.equals("82") || moneda.equals("83")){
					  verimporte=importe + " USD";
				   }else{
					  verimporte="$ " + importe;
				   }%>
                <td bgcolor="#F0EEEE"><%=verimporte%></td>
              </tr>
              <tr>
                <td align="right" bgcolor="#F0EEEE"><strong>Estatus:</strong></td>
                <td bgcolor="#F0EEEE"><%=estatus%></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="4" cellpadding="0">
        <tr align="center" valign="top">
          <td><a href="javascript:scrImpresion();" onMouseOver="MM_swapImage('Image111','','/gifs/EnlaceMig/imprimir_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/imprimir.jpg" name="Image111" width="82" height="41" border="0" id="Image1"></a></td>
          <td> <a href="javascript:window.close();" onMouseOver="MM_swapImage('Image11','','/gifs/EnlaceMig/cerrar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/cerrar.jpg" name="Image11" width="82" height="41" border="0" id="Image1"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>