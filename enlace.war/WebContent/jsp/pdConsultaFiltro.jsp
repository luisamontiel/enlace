<%@page contentType="text/html" %>
<%@page import="java.util.*, mx.altec.enlace.utilerias.*,mx.altec.enlace.bo.*, java.text.*" %>
<jsp:useBean id="pdcCuentas" class="java.util.ArrayList" scope="session" />
<jsp:useBean id="Beneficiarios" class="java.util.ArrayList" scope="session" />
<%--<jsp:useBean id='fechaIni' class='java.util.GregorianCalendar' scope='session'/>
<jsp:useBean id='fechaFin' class='java.util.GregorianCalendar' scope='session'/>
<jsp:useBean id='pdCFechaMax' class='java.util.GregorianCalendar' scope='session'/>--%>
<jsp:useBean id="pdCFiltro" class="mx.altec.enlace.bo.pdCFiltro" scope="session"  >
	<jsp:setProperty name="pdCFiltro" property="*" />
</jsp:useBean>
<jsp:useBean id="pdMensaje" class="java.lang.String" scope="request" />
<jsp:useBean id="pdError" class="java.lang.String" scope="request" />
<jsp:useBean id="Mensaje" class="java.lang.String" scope="request" />
<%!
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");
	String getDate(java.util.GregorianCalendar cal){
		if(null == cal) return "&nbsp;";

		return sdf.format(cal.getTime());

	}
%><html>
<head><title>Pago Directo - Consultas, Selecci&oacute;n de filtros</title>
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript" src= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

function js_validaFechas(txtIni, txtFin){

 lib_year         = parseInt(txtIni.substring(6,10),10);
 lib_month        = parseInt(txtIni.substring(3, 5),10);
 lib_date         = parseInt(txtIni.substring(0, 2),10);
 fechaIni = new Date(lib_year, lib_month, lib_date);

 lim_year     = parseInt(txtFin.substring(6,10),10);
 lim_month    = parseInt(txtFin.substring(3, 5),10);
 lim_date     = parseInt(txtFin.substring(0, 2),10);

 fechaFin  = new Date(lim_year, lim_month, lim_date);

     if( fechaIni > fechaFin){
        //alert("La Fecha Inicial no puede ser mayor a la Fecha Final.");
		cuadroDialogo("La Fecha Inicial no puede ser mayor a la Fecha Final.", 3);
        return false;
      }

  return true;
 }

function js_validaImporte(){
	var importe = document.frmFiltro.importe.value;
    var length = document.frmFiltro.importe.length;
    var numpuntos = 0;
	var strEnteros =  "";

    indicePunto = document.frmFiltro.importe.value.indexOf('.');
    if(indicePunto < 0)
        strEnteros = document.frmFiltro.importe.value;
	else
		strEnteros = document.frmFiltro.importe.value.substring(0, indicePunto);
    if( strEnteros.length > 17 ) {
        cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
		return false;
	}

    for (i = 0; i < length; i++) {
        car = importe.charAt (i);
        if (car != '0' && car != '1' && car != '2' && car != '3' && car != '4' &&
            car != '5' && car != '6' && car != '7' && car != '8' && car != '9' &&
            car != '.') {
            cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
            return false;
        }

        if (car == '.') numpuntos++;
    }

    if (numpuntos > 1) {
        cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
        return false;
    }

	return true;
}
function js_validaNos(txtIni,txtFin){
	var ini = parseInt(txtIni);
	var fin = parseInt(txtFin);
	if(ini > fin){
		cuadroDialogo("El numero de Pago inicial debe ser menor al pago final.",3)
		return false;
	}

	return true;
}

function js_validaCampoNumero( campo ) {
    if ( "" == campo.value ) return true;
    else if( isNaN( campo.value ) ){
        cuadroDialogo("Debe Introducir un numero o dejar el campo en blanco.",3);
		return false;
    }
    return true;
}

function js_validaForma(){

	if( ! js_validaImporte() )return false;
	if( ! js_validaFechas(document.frmFiltro.fechaIni.value,document.frmFiltro.fechaFin.value))
		return false;
	if( ! js_validaNos(document.frmFiltro.pagoIni.value,document.frmFiltro.pagoFin.value))
		return false;

	return true;
}

var fechaMod = -1;
var fechaHoy = "<%--= sdf.format( pdCFechaMax) --%>";
var modulo='Consultas';
function js_muestraCalendario (indice) {

	if(0 == indice ){//fechaIni
		fechaMod = 0;
		fechaLim = document.frmFiltro.fechaFin.value;
		//I05-0179751 - 02-Marzo-2005
		msg=window.open("jsp/pdCalendarioPag.jsp?modulo="+modulo,"Calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		msg.focus();
	} else if(1 == indice ){//fechaFin
		fechaMod = 1;
		fechaLim = fechaHoy;
		//I05-0179751 - 02-Marzo-2005
		msg=window.open("jsp/pdCalendarioPag.jsp?modulo="+modulo,"Calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		msg.focus();
	}

}

function continua () {
	if(0 == fechaMod){
		document.frmFiltro.fechaIni.value = fechaLim;
	} else {
		document.frmFiltro.fechaFin.value = fechaLim;
	}

}

var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function actualizacuenta()
{
  document.frmFiltro.txtCuenta.value=ctaselec;
}

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); <%= session.getAttribute("despliegaEstatus" ) %>"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>
<span class='titpag'><%= request.getAttribute("Encabezado") %></span>
<%if (null != pdMensaje && ! pdMensaje.equals("")){
	%><script language='javascript'>
	    cuadroDialogo("<%= pdMensaje %>",1);
</script><%

}%>
<%if(null != pdError && ! pdError.equals("")){
	%><script language='javascript'>cuadroDialogo("<%=pdError%>",3);</script><%
	request.removeAttribute("pdError");
}%>
<%if(null != Mensaje && !Mensaje.equals("")){
	%><SCRIPT LANGUAGE="JavaScript">
		cuadroDialogo("<%= Mensaje%>",1);
	</SCRIPT><% request.removeAttribute("Mensaje");
}
%>
<form  method="post" action="pdConsultas" id='frmFiltro' name='frmFiltro'>
<table border="0" cellspacing="4" cellpadding="0" align="center">
<tr><td colspan='2' class="tittabdat">Filtros a ocupar</td></tr>
<tr><td class="textabdatcla">
	<table align="center" width="100%" >
		<tr><td class="tabmovtex" nowrap valign="middle">Registrados</td><td class="tabmovtex" nowrap valign="middle"><input type='checkbox' name='opciones' value='0'  >
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Pendientes</td><td class="tabmovtex" nowrap valign="middle"><input type='checkbox' name='opciones' value='1'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Cancelados</td><td class="tabmovtex" nowrap valign="middle"><input type='checkbox' name='opciones' value='2'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Liquidados</td><td class="tabmovtex" nowrap valign="middle"><input type='checkbox' name='opciones' value='3'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Vencidos</td><td class="tabmovtex" nowrap valign="middle"><input type='checkbox' name='opciones' value='4'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Modificados</td><td class="tabmovtex" nowrap valign="middle"><input type='checkbox' name='opciones' value='5'  >
		</td></tr>
	</table></td>
	<td class="textabdatcla">
	<table width="100%" >
		<tr><td class='tabmovtex'>Cuenta</td><%--<td class='tabmovtex'>
				<select name="cuenta">
						<option value="" selected></option>
						<%ListIterator li = pdcCuentas.listIterator();
							while(li.hasNext()){
								pdCuenta cuenta = (pdCuenta) li.next();%>
						<option value="<%= cuenta.getNoCta()%>"><%= cuenta.getNoCta().substring(0,2)%>-<%= cuenta.getNoCta().substring(2,9)%>-<%= cuenta.getNoCta().substring(9)%></option><%
							}%>
				</select></td></tr>
		<tr><td>&nbsp;</td>--%><td class="tabmovtex">
					    <input type="text" name="txtCuenta"  class="tabmovtexbol" maxlength=16 size=16 <!--onfocus="blur();" -->
					    <a href="javascript:PresentarCuentas();"><img src="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></a>
					    <input type="hidden" name="cboCuentaCargo" >
				    </td></tr>
		<tr><td class='tabmovtex'>Fecha Inicial</td><td><input type="text" name='fechaIni' value="<%=  getDate( pdCFiltro.getFechaIni()) %>" size="10" onFocus='blur();' maxlength=10></input><a href="javascript:js_muestraCalendario(0)"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario"></a></td></tr>
		<tr><td class='tabmovtex'>Fecha Final</td><td><input type="text" name='fechaFin' value="<%= getDate( pdCFiltro.getFechaFin() )%>" size="10" onFocus='blur();' maxlength=10></input><a href="javascript:js_muestraCalendario(1)"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario"></a></td></tr>
		<tr><td class='tabmovtex'>Importe</td><td><input type="text" name='importe' size="17" value="<jsp:getProperty name="pdCFiltro" property="importe"  />" maxlength='16' onblur='js_validaCampoNumero( this )'></input></td></tr>
		<!-- Everis - Inicio - Modificación de longitud de Campo No. Pago Inicial y No. Pago Final, de 7 a 12 posiciones -->
		<tr><td class='tabmovtex'>No. de Pago Inicial</td><td><input type="text" name='pagoIni' size="20" value="<%= pdCFiltro.getPagoIni()%>" maxlength='20' onblur='js_validaCampoNumero( this )'></input></td></tr>
		<tr><td class='tabmovtex'>No. de Pago Final</td><td><input type="text" name='pagoFin' size="20"  value="<%= pdCFiltro.getPagoFin()%>" maxlength='20' onblur='js_validaCampoNumero( this )'></input></td></tr>
		<!-- Everis - Fin - Modificación de longitud de Campo No. Pago Inicial y No. Pago Final, de 7 a 12 posiciones -->
		<tr><td class='tabmovtex'>Beneficiario</td>
			<td class="tabmovtex" width='100px'><select name="beneficiario">
					<option value="" selected></option>
					<%	ListIterator li = Beneficiarios.listIterator();
						while(li.hasNext()){
							pdBeneficiario benef = (pdBeneficiario) li.next();%>
							<option value="<%= benef.getID()%>"><%= benef.getNombre()%></option><%
						}%>
				</select>
		</td></tr>
	</table>
	</td>
</tr>
<tr><td colspan='2'>
	<table align="center"></tr><input type="hidden" name='pdcOpcion' value='1'>
	<td valign=top><a href = "javascript:if(js_validaForma())document.frmFiltro.submit();" border=0><img src="/gifs/EnlaceMig/gbo25220.gif" border=0 alt="Consulta" width="90" height='22'></a></td>
	<td align="left" valign="top" width="76">
		<A href = "javascript:document.frmFiltro.reset();" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="76" height="22"></a>
	</tr></table>
</td></tr>
</table>
</form>
</body>
</html>
<% session.removeAttribute("pdCArchivo"); %>