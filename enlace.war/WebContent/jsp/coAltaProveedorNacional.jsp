<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="mx.altec.enlace.dao.*" %>
<%@ page import="mx.altec.enlace.utilerias.EIGlobal" %>
<%!
	   private void writeMenu(javax.servlet.jsp.JspWriter out, String[] values,
	    String[] nameValues, String selectedValue){
		  try{
		  		for(int i=0; i<values.length; i++){
		  			if(values[i].trim().equals(selectedValue)){
		  				out.println("<option selected value=\""+ values[i] + "\">" + nameValues[i] + "</option>");
		  			}else{
		  				out.println("<option value=\""+ values[i] + "\">" + nameValues[i] + "</option>");
		  			}
		  		}
	  	  }catch(java.io.IOException e1){
	  	  	EIGlobal.mensajePorTrace("Error en pinta menu->" + e1.getMessage(),EIGlobal.NivelLog.ERROR);
	  	  }
	   }
%>

<%
	String tituloPantalla = "";
	String tituloHTML = "";
	String boton1 = "";
	String boton2 = "";
	String altBoton1 = "";
	String altBoton2 = "";
	String gbo25421 = "gbo25421.gif";
	String altGbo25421 = "";

	String parTipoSoc = (String) request.getAttribute("TipoSoc");
	String parPais = (String) request.getAttribute("Paises");
	String parMedio = (String) request.getAttribute("MedioInfo");
	String parForma = (String) request.getAttribute("FormaPago");
	String parBanco = (String) request.getAttribute("Bancos");
	String parPlaza = (String) request.getAttribute("Plazas");
	String parEstado = (String) request.getAttribute("Estados");
	String parAccion = (String) request.getAttribute("accion");
	String[] cveEstado = (String[])request.getAttribute("cveEstado");
	String[] descEstado = (String[])request.getAttribute("descEstado");
	String[] cvePais = (String[])request.getAttribute("cvePais");
	String[] descPais = (String[])request.getAttribute("descPais");
	String estadoSel = "";
	String cvePaisSel = "";

	if (parTipoSoc == null)
		parTipoSoc = "";
	if (parPais == null)
		parPais = "";
	if (parMedio == null)
		parMedio = "";
	if (parForma == null)
		parForma = "";
	if (parBanco == null)
		parBanco = "";
	if (parPlaza == null)
		parPlaza = "";
	if (parEstado == null)
		parEstado = "";
	if (parAccion == null) {
		parAccion = request.getParameter("accion");
		if (parAccion == null)
			parAccion = "1";
	}

	boolean nuevo = false;

	if (Integer.parseInt(parAccion) > 2 && !parAccion.equals("11")) {
		nuevo = true;
		parAccion = "" + (Integer.parseInt(parAccion) - 2);
	}


	ProveedorConf prov = (ProveedorConf) request
			.getAttribute("Proveedor"); // <---------------------------------
	if (prov == null) {
		prov = new ProveedorConf();
		prov.tipo = (request.getParameter("tipoPersona") == null) ? prov.FISICA :
			(((String) request.getParameter("tipoPersona")).equals("F") ? prov.FISICA : prov.MORAL);
		//jgarcia - Es proveedor Nacional o Internacional
		prov.origenProveedor = ((String)request.getParameter("origenProveedor") != null
			&& prov.INTERNACIONAL.equalsIgnoreCase((String)request.getParameter("origenProveedor"))?
				prov.INTERNACIONAL:prov.NACIONAL);
		if (!nuevo) {
			prov.claveProveedor = request.getParameter("vClave");
			prov.codigoCliente = request.getParameter("vCodigo");
			prov.noTransmision = request.getParameter("vSecuencia");
			prov.nombre = request.getParameter("vNombre");
			prov.apellidoPaterno = request.getParameter("vPaterno");
			prov.apellidoMaterno = request.getParameter("vMaterno");
			prov.tipoSociedad = request.getParameter("vTipoSoc");
			prov.rfc = request.getParameter("vRFC");
			prov.homoclave = request.getParameter("vHomoclave");
			prov.contactoEmpresa = request.getParameter("vContacto");
			prov.clienteFactoraje = request.getParameter("vFactoraje");
			prov.calleNumero = request.getParameter("vCalle");
			prov.colonia = request.getParameter("vColonia");
			prov.cdPoblacion = request.getParameter("vCiudad");
			prov.cp = request.getParameter("vCodigoPostal");
			prov.pais = request.getParameter("vPais");
			// jgarcia 29/Sept/2009
	        // Si es proveedor Internacional el medio de informacion debe ser e-mail
	        // y para el proveedor Nacional no existe el campo vDatosAdic.
	        prov.medioInformacion = request.getParameter("vMedio");
        	prov.datosAdic = "";
			prov.delegMunicipio = request.getParameter("vDelegacion");
        	prov.estado = request.getParameter("vEstado");

			prov.email = request.getParameter("vEmail");
			prov.ladaPrefijo = request.getParameter("vLada");
			prov.numeroTelefonico = request.getParameter("vTelefonico");
			prov.extensionTel = request.getParameter("vExTelefono");
			prov.fax = request.getParameter("vFax");
			prov.extensionFax = request.getParameter("vExFax");
			prov.formaPago = request.getParameter("vForma");
			prov.cuentaCLABE = request.getParameter("vCuenta");
			prov.banco = request.getParameter("vBanco");
			prov.sucursal = request.getParameter("vSucursal");
			prov.plaza = request.getParameter("vPlaza");
			prov.deudoresActivos = request.getParameter("vDeudores");
			prov.limiteFinanciamiento = request.getParameter("vLimite");
			prov.volumenAnualVentas = request.getParameter("vVolumen");
			prov.importeMedioFacturasEmitidas = request.getParameter("vImporte");
			prov.cveDivisa = request.getParameter("vCveDivisa");
			prov.cvePais = request.getParameter("vCvePais");

			if(request.getParameter("vDescripcionBanco") != null &&
			  !request.getParameter("vDescripcionBanco").trim().equals("")){
				prov.descripcionBanco = request.getParameter("vDescripcionBanco");
			}else{
				prov.descripcionBanco = "";
			}

			if(request.getParameter("vDescripcionCiudad") != null &&
			  !request.getParameter("vDescripcionCiudad").trim().equals("")){
				prov.descripcionCiudad = request.getParameter("vDescripcionCiudad");
			}else{
				prov.descripcionCiudad = "";
			}

			prov.cveABA = request.getParameter("vCveABA");
			if(request.getParameter("vCtaBancoDestino") == null)
				prov.ctaBancoDestino = "";
			else
				prov.ctaBancoDestino = request.getParameter("vCtaBancoDestino");
			prov.tipoAbono = request.getParameter("vTipoAbono");
			prov.quitaNulos();
		}
	}

	if(prov.estado!=null){
		estadoSel = prov.estado;
	}

	if(prov.cvePais!=null){
		cvePaisSel = prov.cvePais;
	}

	// Alta de Proveedores en Linea
	if (parAccion.equals("1")) {
		boton1 = "gbo25480.gif";
		boton2 = "gbo25250.gif";
		altBoton1 = "alta";
		altBoton2 = "limpiar";
		tituloPantalla = "Alta";
		tituloHTML = "Alta de Proveedor";
	}
	// Modificacion de Proveedor
	else if (parAccion.equals("2")) {
		boton1 = "gbo25510.gif";
		boton2 = "gbo25250.gif";
		altBoton1 = "modificar";
		altBoton2 = "limpiar";
		tituloPantalla = "Modificaci&oacute;n";
		tituloHTML = "Modificaci&oacute;n de Proveedor";
	}
	// Consulta y baja de proveedor
	else {
		boton1 = "gbo25510.gif";
		boton2 = "gbo25500.gif";
		altBoton1 = "modificar";
		altBoton2 = "baja";
		tituloPantalla = "Consulta";
		tituloHTML = "Consulta y modificaci&oacute;n de proveedor";
	}
%>

<script type="text/javascript">
// Valida la selecion de tipo de sociedad -------------------------------------------------
function valida_vSociedad(){
	var ValorRegreso = true;
	var index_Sociedad =  document.formulario.vTipoSoc.selectedIndex;

    if ( index_Sociedad == 0) {
		document.formulario.vTipoSoc.focus();
		cuadroDialogo("Favor de Seleccionar el Tipo de Sociedad",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}
// Valida el RFC ---------------------------------------------------------------------------------
function valida_vRFC(){
	var valorCampo = document.formulario.vRFC.value;
	var Indice;
	var formato = "";

	switch(valorCampo.length) {
		case 0:
			document.formulario.vRFC.select();
			document.formulario.vRFC.focus();
			MensajeErrorLongitudNula("el R.F.C.");
			return false;
			break;
		case 9:
			if(document.formulario.tipoPersona.value == "M") { 	// Persona Moral
				formato =  /^[A-Z,a-z,&][A-Z,a-z,&][A-Z,a-z,&]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;
				Indice = 5;
			}
			break;
		case 10:
			if(document.formulario.tipoPersona.value == "F") { 	// Persona Fisica
				formato = /^[A-Z,a-z,&][A-Z,a-z,&][A-Z,a-z,&][A-Z,a-z,&]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;
				Indice = 6;
			}
			break;
		default:
			document.formulario.vRFC.select();
			document.formulario.vRFC.focus();
			cuadroDialogo("El campo R.F.C. est&aacute; incompleto.",3);
			return false;
			break;
    }
	if(formato != "") {
		if(formato.test(valorCampo)) {
			if(valida_NombreRFC(valorCampo)) {
				if(valida_FechaRFC(valorCampo,Indice)) {
					return true;
				}
			}
		}
	}
	document.formulario.vRFC.select();
	document.formulario.vRFC.focus();
	cuadroDialogo("El formato del campo R.F.C. es incorrecto.",3);
	return false;
}

function valida_vHomoclave(){
	var ValorRegreso = false;
	var valorCampo = document.formulario.vHomoclave.value;

	if(valorCampo.length == 0) {
		document.formulario.vHomoclave.select();
		document.formulario.vHomoclave.focus();
		MensajeErrorLongitudNula("la Homoclave");
	}else if (!EsAlfaNumerica(valorCampo)) {
		document.formulario.vHomoclave.select();
		document.formulario.vHomoclave.focus();
		MensajeErrorCaracteresInvalidos("Homoclave");
	}else { ValorRegreso = true; }
	return ValorRegreso;
}

//	Valida Colonia --------------------------------------------------------------------------
function valida_vColonia()
{
	var valorCampo = document.formulario.vColonia.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vColonia.select();
		document.formulario.vColonia.focus();
		MensajeErrorLongitudNula("la Colonia");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vColonia.select();
		document.formulario.vColonia.focus();
		MensajeErrorCaracteresInvalidos("Colonia");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Delegacion o Municipio -----------------------------------------------------------
function valida_vDelegacion(){
	var valorCampo = document.formulario.vDelegacion.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vDelegacion.select();
		document.formulario.vDelegacion.focus();
		MensajeErrorLongitudNula("la Delegaci&oacute;n");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vDelegacion.select();
		document.formulario.vDelegacion.focus();
		MensajeErrorCaracteresInvalidos("Delegaci&oacute;n");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Codigo Postal -------------------------------------------------------------------
function valida_vCodigoPostal(){
	var valorCampo = document.formulario.vCodigoPostal.value;
	var ValorRegreso = false;

	if(valorCampo.length != 5) {cuadroDialogo("El c&oacute;digo postal debe tener 5 d&iacute;gitos",3); return false;}

	if(valorCampo.length == 0) {
		document.formulario.vCodigoPostal.select();
		document.formulario.vCodigoPostal.focus();
		MensajeErrorLongitudNula("el C&oacute;digo Postal");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vCodigoPostal.select();
		document.formulario.vCodigoPostal.focus();
		MensajeErrorTipoDeDato("C&oacute;digo Postal","Num&eacute;rico");
	}
	else { ValorRegreso = true;}
	return ValorRegreso;
}

// Valida selecion del estado -------------------------------------------------------------
function valida_vEstado()
{
	var index_Estado =  document.formulario.vEstado.selectedIndex;
	var ValorRegreso = true;

    if ( index_Estado == 0) {
		document.formulario.vEstado.focus();
	    cuadroDialogo("Favor de Seleccionar un Estado",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}

// Valida la seleccion de medio de comunicacion -------------------------------------------
function valida_vMedio(){
	var index_Medio =parseInt(document.formulario.vMedio.options[document.formulario.vMedio.selectedIndex].value);
	var ValorRegreso = false;

	switch ( index_Medio ) {
		case 0:
			document.formulario.vMedio.focus();
			cuadroDialogo("Favor de Seleccionar un Medio de Informaci&oacute;n",3);
		break;
		case 1:
			if(valida_vEmail())
				ValorRegreso = true;
		break;
		case 2:
			if(valida_vFax())
				ValorRegreso = true;
		break;
		default:
			document.formulario.vMedio.focus();
			cuadroDialogo("Actualmente su Medio de Informaci&oacute;n no es valido debe elegir entre e-mail y fax",3);
			break;
	}
	return ValorRegreso;
}

// Valida el Telefono ---------------------------------------------------------------------
function valida_vTelefono(){
	var valorCampo = document.formulario.vTelefonico.value;
	var valorCampo2 = document.formulario.vLada.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		MensajeErrorLongitudNula("el Tel&eacute;fono");
	}else if(!EsNumerica(valorCampo)) {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		MensajeErrorTipoDeDato("Tel&eacute;fono","Num&eacute;rico");
	}else if((valorCampo.length + valorCampo2.length) != 10) {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		cuadroDialogo("La clave lada y el n&uacute;mero telef&oacute;nico deben sumar 10 posiciones.",3)
	}else if(valorCampo.charAt(0) == '0') {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		cuadroDialogo("El n&uacute;mero telef&oacute;nico no debe empezar con 0 ni incluir claves de marcaci&oacute;n (01) ni clave de larga distancia",3)
	}else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida selecion del Banco  --------------------------------------------------------------
function valida_vBanco(){
	var index_Banco = document.formulario.vBanco.selectedIndex;
	var formaDePago = document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value;
	var ValorRegreso = true;

	if(formaDePago == "2"||formaDePago == "4"){
		//Transferencia Interbancaria
		if (index_Banco == 0){
			document.formulario.vBanco.focus();
			cuadroDialogo("Favor de Seleccionar un Banco",3);
			ValorRegreso = false;
		}else if(document.formulario.vBanco.options[index_Banco].value == "BANME" || document.formulario.vBanco.options[index_Banco].value == "SERFI"){
			document.formulario.vForma.focus();
			cuadroDialogo("Favor de Seleccionar la Forma de Pago \"Transferencia Mismo Banco\" o \"Transferencia M&oacute;vil Mismo Banco\"",3);
			ValorRegreso = false;
		}
	}else if(formaDePago == "1"||formaDePago == "3"){
	//Transferencia Interna
		if (index_Banco != 0){
			if(document.formulario.vBanco.options[index_Banco].value != "BANME" &&
				document.formulario.vBanco.options[index_Banco].value != "SERFI"){
				document.formulario.vForma.focus();
				cuadroDialogo("Favor de Seleccionar la Forma de Pago \"Transferencia Mismo Banco\"",3);
				ValorRegreso = false;
			}
		}
	}
	return ValorRegreso;
}

// Valida seleccion de la forma de pago ----------------------------------------------------
function valida_vForma(){
	var index_Forma =  document.formulario.vForma.selectedIndex;
	var retorno = true;

	switch (index_Forma)
		{
		case -1:
			document.formulario.vForma.focus();
			cuadroDialogo("Favor de Seleccionar una Forma de Pago",3);
			retorno = false;
			break;
		case 1: // Transferencia interna.
			if(!valida_vBanco() || !valida_vCuenta() || !valida_vCveDivisa()) retorno = false;
			//if(document.formulario.vSucursal.value != "" && !valida_vSucursal())
			//	{cuadroDialogo("La sucursal no es v&aacute;lida",3); retorno = false;}
			break;
		case 2: // Transferencia interbancaria.
			if(!valida_vBanco() || !valida_vCuenta() || !valida_vCveDivisa() || !valida_ABA() ||
			   !valida_vCtaBancoDestino() || !valida_vDescripcionBanco() || !valida_vDescripcionCiudad()) retorno = false;
			break;
		case 3: // Transferencia interna movil.
			if(!valida_vBanco() || !valida_vCuenta() || !valida_vCveDivisa()) retorno = false;
			//if(document.formulario.vSucursal.value != "" && !valida_vSucursal())
			//	{cuadroDialogo("La sucursal no es v&aacute;lida",3); retorno = false;}
			break;
		case 4: // Transferencia interbancaria movil.
			if(!valida_vBanco() || !valida_vCuenta() || !valida_vCveDivisa() ||
			   !valida_vCtaBancoDestino() || !valida_vDescripcionBanco() || !valida_vDescripcionCiudad()) retorno = false;
			break;
		default:
			document.formulario.vForma.focus();
			cuadroDialogo("Actualmente la forma de pago no es v&aacute;lida, deber&aacute; elegir" +
				" entre Transferencia Mismo Banco/M&oacute;vil Mismo Banco u Otros Bancos/M&oacute;vil Otros Bancos",3);
			break;
		}
	return retorno;
}

function valida_vCveDivisa(){
	var valorCampo = document.formulario.vCveDivisa.value;
	var ValorRegreso = false;

	if(valorCampo  == "0" || valorCampo.length < 2)
		{
		document.formulario.vCveDivisa.focus();
		cuadroDialogo("Debe especificarse una divisa ",3);
		}
	else
		ValorRegreso = true;
	return ValorRegreso;
}

function ajustaFormaPago(){
	switch(document.formulario.vForma.selectedIndex){
		case -1: case 0:
			document.formulario.vBanco.disabled = false;
			document.formulario.vDescripcionCiudad.disabled =  false;
			// jgarcia document.formulario.vSucursal.disabled = false;
			document.formulario.vCveABA.disabled = false;
			document.formulario.vDescripcionCiudad.disabled = false;
			document.formulario.vCvePais.disabled = false;
			document.formulario.vBanco.selectedIndex = 0;
			document.formulario.vCveDivisa.disabled = false;
			document.formulario.vCveDivisa.selectedIndex = 0;
			document.formulario.vCveDiv.value = "";
			break;
		case 1:
			valida_formaDivisa();
			break;
		case 2:
			valida_formaDivisa();
			break;
		case 3:
			valida_formaDivisa();
			break;
		case 4:
			valida_formaDivisa();
			break;
	}
}
/**
*FSW IDS :::JMFR:::
*AJUSTE NORMATIVO SPID
*SE AGREGA FUNCIONALIDAD AL COMBO DE BANCOS PARA QUE MUESTRE LOS BANCOS SPID.
**/
/*************************************************************************************/
<%
String bancosUSD = "";
String bancosMXM = "";
if(request.getAttribute("bancosUSD") != null){
	bancosUSD = (String) request.getAttribute("bancosUSD");
}
if(request.getAttribute("bancosMXM") != null){
	bancosMXM = (String) request.getAttribute("bancosMXM");
}
 %>

function cargaListas(Lista){
   var lista = Lista;
   var select = document.getElementById('vBanco');
   var strBancBN = document.getElementById(lista).value;
   var array;
   var arrayAux;
    select.options.length = 0;
	select.value = "";
	if(strBancBN != "@"){
		array = strBancBN.split("@");
		select.options[0] = new Option ("Seleccione un banco", "0");
		for (var i = 1; i < (array.length)-1; i++){
				arrayAux = array[i].split("?");
				select.options[i] = new Option (arrayAux[1], arrayAux[0]);
		}
	}else{
		select.options[0] = new Option ("::No existen datos::", "0");
	}
}
function seleccionaDivisa(){
	var divisaSelect = document.formulario.vCveDivisa.value;
	valida_formaDivisa();
	if(document.formulario.vCveDivisa.value == "0"){
		document.formulario.vCveDiv.value = "";
	}else{
		document.formulario.vCveDiv.value = divisaSelect;

		if(divisaSelect == "USD"){
			cargaListas('bancosUSD');
		}else{
			cargaListas('bancosMXM');
		}
	}
}

function valida_formaDivisa(){
	if(document.formulario.vForma.value == "1") {
		document.formulario.vCvePais.disabled = true;
		document.formulario.vBanco.disabled = true;
		document.formulario.vCveABA.disabled = true;
		document.formulario.vDescripcionBanco.disabled = true;
		document.formulario.vDescripcionCiudad.disabled = true;
		document.formulario.vCtaBancoDestino.disabled = true;
		document.formulario.vBanco.selectedIndex = 0;
		document.formulario.vCtaBancoDestino.value = "";
		document.formulario.vDescripcionBanco.value = "";
		document.formulario.vDescripcionCiudad.value = "";
		document.formulario.vCveABA.value = "";


		document.formulario.vCvePais.selectedIndex = 0;
	}else if(document.formulario.vForma.value == "2") {
		if(document.formulario.vCveDivisa.value == "MXN") {
			document.formulario.vCvePais.disabled = true;
			document.formulario.vBanco.disabled = false;
			document.formulario.vCveABA.disabled = true;
			document.formulario.vDescripcionBanco.disabled = true;
			document.formulario.vDescripcionCiudad.disabled = true;
			document.formulario.vCtaBancoDestino.disabled = true;
			document.formulario.vCvePais.selectedIndex = 0;
			document.formulario.vCtaBancoDestino.value = "";
			document.formulario.vDescripcionBanco.value = "";
			document.formulario.vDescripcionCiudad.value = "";
			document.formulario.vCveABA.value = "";
		}else if(document.formulario.vCveDivisa.value == "USD") {
			document.formulario.vCvePais.disabled = true;
			document.formulario.vBanco.disabled = false;
			document.formulario.vCveABA.disabled = false;
			document.formulario.vDescripcionBanco.disabled = false;
			document.formulario.vDescripcionCiudad.disabled = false;
			document.formulario.vCtaBancoDestino.disabled = false;
			//document.formulario.vCvePais.selectedIndex = 105;
			document.formulario.vCveABA.value = "";
			document.formulario.vDescripcionBanco.value = "";
			document.formulario.vDescripcionCiudad.value = "";
		}
	}
	if(document.formulario.vForma.selectedIndex == "3"||document.formulario.vForma.selectedIndex == "4"){
		document.formulario.vCveDivisa.disabled = true;
		document.formulario.vCveDivisa.selectedIndex = 1;
		document.formulario.vCveDiv.value = "MXN";
		if (document.formulario.vForma.selectedIndex == "4"){
			cargaListas('bancosMXM');
		}
	}else{
		document.formulario.vCveDivisa.disabled = false;
	}
}

function seleccionaBanco(){
	if(document.formulario.vForma.selectedIndex == 1)
		document.formulario.vBanco.blur();
}

// Stefanini ***** 20/05/2009 ****** validar que el Banco destino sea valida

function valida_vDescripcionBanco() {
	var valorCampo = document.formulario.vDescripcionBanco.value;
	var divisaDePago = document.formulario.vCveDivisa.options[document.formulario.vCveDivisa.selectedIndex].value;
	var ValorRegreso = true;
	if(trim(divisaDePago) == "USD" && trim(valorCampo) ==""){
		document.formulario.vDescripcionBanco.select();
		document.formulario.vDescripcionBanco.focus();
		cuadroDialogo("El Banco Corresponsal es obligatorio",3);
		ValorRegreso = false;
	}
	return ValorRegreso;
}

// Se valida la descripción de la ciudad como obligatorio
function valida_vDescripcionCiudad() {
	var valorCampo = trim(document.formulario.vDescripcionCiudad.value);
	var divisaDePago = document.formulario.vCveDivisa.options[document.formulario.vCveDivisa.selectedIndex].value;
	var ValorRegreso = true;
	if(trim(divisaDePago) == "USD" && trim(valorCampo) ==""){
		document.formulario.vDescripcionCiudad.select();
		document.formulario.vDescripcionCiudad.focus();
		cuadroDialogo("La Plaza/Ciudad es obligatoria",3);
		ValorRegreso = false;
	}
	return ValorRegreso;
}

// Stefanini ***** 26/03/2009 ****** validar que la ctaBancoDestino sea valida
function valida_vCtaBancoDestino(){
 		var cuentaBancoDestino = document.formulario.vCtaBancoDestino.value;
		var divisaDePago = document.formulario.vCveDivisa.options[document.formulario.vCveDivisa.selectedIndex].value;
		var ValorRegreso = true;

		if(divisaDePago == "USD" && cuentaBancoDestino==""){
			document.formulario.vCtaBancoDestino.select();
			document.formulario.vCtaBancoDestino.focus();
			cuadroDialogo("La cuenta banco destino es obligatoria",3);
			ValorRegreso = false;
		}else if(divisaDePago == "USD" && !EsNumerica(cuentaBancoDestino)){
			document.formulario.vCtaBancoDestino.select();
			document.formulario.vCtaBancoDestino.focus();
			cuadroDialogo("La cuenta banco destino debe ser numerica",3);
			ValorRegreso = false;
		}
		return ValorRegreso;
}

// Valida selecion de la clave ABA  ---------------------------------------------------
function valida_ABA(){
	var aba = document.formulario.vCveABA.value;
	var formaDePago = parseInt(document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value);
	var divisaDePago = document.formulario.vCveDivisa.options[document.formulario.vCveDivisa.selectedIndex].value;
	var ValorRegreso = true;
	if(formaDePago == "2" && divisaDePago == "USD"){
		if (aba == ""){
			cuadroDialogo("La Clave ABA es obligatoria",3);
			ValorRegreso = false;
		}else if(aba.length!=9){
			cuadroDialogo("La Clave ABA es incorrecta",3);
			ValorRegreso = false;
		}
	}
	return ValorRegreso;
}

function TraerClavesABA(){
	if(document.formulario.vForma.selectedIndex == 2 && document.formulario.vCveDivisa.value == "USD") {
		if(document.formulario.vDescripcionBanco.value!=""){
			var Banco=document.formulario.vDescripcionBanco.value;
			Banco=escape(Banco);
			ventanaInfo2=window.open('EI_Bancos?BancoTxt='+Banco+"&Clave=1",'Bancos','width=250,height=350,toolbar=no,scrollbars=no');
			ventanaInfo2.focus();
			<%if((String)request.getAttribute("CiudadABA")!=null){ %>
				document.formulario.vDescripcionCiudad.value =  <%=(String) request.getAttribute("CiudadABA") %>;
			<%}else{%>
				document.formulario.vDescripcionCiudad.value = "";
			<%}%>
		}else{
	       cuadroDialogo("Seleccione un Banco.",1);
		}
	}else{
	    cuadroDialogo("Valido solo para transferencia Otros Bancos y divisa Dolar Americano", 1);
	}
}

function TraerBancos(){
 	if(document.formulario.vForma.selectedIndex == 2 && document.formulario.vCveDivisa.value == "USD") {
   		 ventanaInfo2=window.open('EI_Bancos','Banco','width=280,height=350,toolbar=no,scrollbars=no');
	 	 ventanaInfo2=window.open('/Download/EI_Bancos.html','Banco','width=250,height=350,toolbar=no,scrollbars=no');
		 ventanaInfo2.focus();
	 }
	 else
	    cuadroDialogo("Valido solo para transferencia Otros Bancos y divisa Dolar Americano", 1);
}
</script>
		<table>
			<tr>
				<td class="tittabdat" colspan="2">
					<b> Capture los datos del proveedor </b>
				</td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="tabmovtexbol" nowrap><b> * Clave de Proveedor</b></td>
							<td class="tabmovtexbol" nowrap><b> C&oacute;digo del cliente</b></td>
							<td class="tabmovtexbol" nowrap><b> No. Transmisi&oacute;n </b></td>
						</tr>
						<tr>
							<td class="tabmovtexbol" nowrap>
								<input type="TEXT" name="vClave" value="<%= prov.claveProveedor %>" maxlength="20"
								onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
							<td class="tabmovtexbol" nowrap>
								<input type="TEXT" name="vCodigo" value="<%= prov.codigoCliente %>"
								onFocus="blur();" />
							</td>
							<td class="tabmovtexbol" nowrap>
								<input type="text" name="vSecuencia" value="<%= prov.noTransmision %>"
								onFocus="blur();" />
							</td>
						</tr>
						<tr>
							<%
							if (prov.tipo == prov.FISICA) {
							%>
							<td class="tabmovtexbol" nowrap><b> * Nombre </b></td>
							<td class="tabmovtexbol" nowrap><b> * Apellido Paterno </b></td>
							<td class="tabmovtexbol" nowrap><b> Apellido Materno </b></td>
							<%
							} else {
							%>
							<td class="tabmovtexbol" nowrap><b> * Razon Social </b></td>
							<td class="tabmovtexbol" nowrap><b> * Tipo de sociedad </b></td>
							<%
							}
							%>
						</tr>
						<tr>
							<%
							if (prov.tipo == prov.FISICA) {
							%>
							<td class="tabmovtexbol" nowrap><input type="TEXT"
								name="vNombre" value="<%= prov.nombre %>" maxlength="30"
								onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
							<td class="tabmovtexbol" nowrap><input type="TEXT"
								name="vPaterno" value="<%= prov.apellidoPaterno %>"
								maxlength="30"
								onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
							<td class="tabmovtexbol" nowrap><input type="TEXT"
								name="vMaterno" value="<%= prov.apellidoMaterno %>"
								maxlength="30"
								onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
							<%
							} else {
							%>
							<td class="tabmovtexbol" nowrap><input type="TEXT"
								name="vNombre" value="<%= prov.nombre %>" maxlength="80"
								onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
							<td class="tabmovtexbol" nowrap colspan=2><select
								name="vTipoSoc">
								<option value="0"></option>
								<%=request.getAttribute("TipoSoc")%>
							</select></td>
							<%
							}
							%>
						</tr>
						<tr>
							<td class="tabmovtexbol" nowrap><b> * R.F.C. </b></td>
							<td class="tabmovtexbol" nowrap><b> * Homoclave </b></td>
							<td class="tabmovtexbol" nowrap><b> Contacto en la empresa: </b></td>
						</tr>
						<tr>
							<td class="tabmovtexbol" nowrap>
								<input type="TEXT" name="vRFC" value="<%= prov.rfc %>"
								maxlength="<%= (prov.tipo == prov.MORAL)?9:10 %>"
								onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
							<td class="tabmovtexbol" nowrap>
								<input type="TEXT" name="vHomoclave" value="<%= prov.homoclave %>" maxlength="3"
								onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
							<td class="tabmovtexbol" nowrap>
								<input type="TEXT" name="vContacto" value="<%= prov.contactoEmpresa %>"
								maxlength="80" onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
						</tr>
						<tr>
							<td class="tabmovtexbol" nowrap>No. Cliente para Factoraje:</td>
						</tr>
						<tr>
							<td class="tabmovtexbol" nowrap>
								<input type="TEXT" name="vFactoraje" value="<%= prov.clienteFactoraje %>"
								maxlength="8" onchange="this.value=this.value.toUpperCase(); return true;" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="tittabdat" colspan="2"><b> Domicilio </b></td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Calle y n&uacute;mero </b></td>
						<td class="tabmovtexbol" nowrap><b> * Colonia </b></td>
						<td class="tabmovtexbol" nowrap><b> * Deleg. o Municipio</b></td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="Text" name="vCalle" value="<%= prov.calleNumero %>" maxlength="60"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="Text" name="vColonia" value="<%= prov.colonia %>" maxlength="30"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vDelegacion" value="<%= prov.delegMunicipio %>"
							maxlength="35" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Cd. o Poblaci&oacute;n </td>
						<td class="tabmovtexbol" nowrap><b> * C.P. </td>
						<td class="tabmovtexbol" nowrap><b> * Estado </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><input type="TEXT"
							name="vCiudad" value="<%= prov.cdPoblacion %>" maxlength="35"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap><input type="TEXT"
							name="vCodigoPostal" value="<%= prov.cp %>" maxlength="5"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<select name="vEstado">
							<option value="0"></option>
							    <% writeMenu(out, cveEstado,
							      				  descEstado,
							    				  estadoSel.trim());
							    %>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Pa&iacute;s </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<select name="vPais">
								<option value="0"></option>
								<%=request.getAttribute("Paises")%>
							</select>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="tittabdat" colspan="2">
					<b> Medio de confirmaci&oacute;n
				</td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Medio de Informaci&oacute;n </td>
						<td class="tabmovtexbol" nowrap><b> e-mail </td>
						<td class="tabmovtexbol" nowrap><b> * Lada o prefijo </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<select name="vMedio">
								<option value="0"></option>
								<option CLASS='tabmovtex' value='1'>E-mail</option>
								<option CLASS='tabmovtex' value='2'>Fax</option>
							<!--%= request.getAttribute("MedioInfo") %-->
							</select>
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="Text" name="vEmail" value="<%= prov.email %>" maxlength="60"></td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vLada" value="<%= prov.ladaPrefijo %>" maxlength="6"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> * N&uacute;mero telef&oacute;nico </td>
						<td class="tabmovtexbol" nowrap><b> Extensi&oacute;n Tel. </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vTelefonico" value="<%= prov.numeroTelefonico %>"
							maxlength="8" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vExTelefono" value="<%= prov.extensionTel %>" maxlength="6"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Fax </td>
						<td class="tabmovtexbol" nowrap><b> Extensi&oacute;n Fax
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vFax" value="<%= prov.fax %>" maxlength="8"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vExFax" value="<%= prov.extensionFax %>" maxlength="6"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="tittabdat" colspan="2"><b> Forma de pago</td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Forma de pago </td>
						<td class="tabmovtexbol" nowrap><b> * Cuenta Destino/M&oacute;vil: </td>
						<td class="tabmovtexbol" nowrap><b> * Divisa: </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap colspan=1><select
							name="vForma" onChange="ajustaFormaPago();">
							<option value="-1"></option>
							<option CLASS='tabmovtex' value='<%=ProveedorConf.FORMA_PAGO_CVE_MISMO %>'><%=ProveedorConf.FORMA_PAGO_DESC_MISMO %></option>
							<option CLASS='tabmovtex' value='<%=ProveedorConf.FORMA_PAGO_CVE_OTRO %>'><%=ProveedorConf.FORMA_PAGO_DESC_OTRO %></option>
							<option CLASS='tabmovtex' value='<%=ProveedorConf.FORMA_PAGO_CVE_MISMO %>'><%=ProveedorConf.TRANSFERENCIA_NUMERO_MOVIL_MISMO_BANCO %></option>
							<option CLASS='tabmovtex' value='<%=ProveedorConf.FORMA_PAGO_CVE_OTRO %>'><%=ProveedorConf.TRANSFERENCIA_NUMERO_MOVIL_OTROS_BANCOS_NACIONALES %></option>
							<!--%= request.getAttribute("FormaPago") %-->
						</select></td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vCuenta" value="<%= prov.cuentaCLABE %>" maxlength="20"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vCveDiv" value="<%= prov.cveDivisa %>" maxlength="3"
							onchange="this.value=this.value.toUpperCase(); return true;" size="5" readonly="readonly">
							<select name="vCveDivisa" onChange="seleccionaDivisa()">
	 							<option CLASS='tabmovtex' value=' '>Seleccione una Divisa.</option>
								<option CLASS='tabmovtex' value='MXN'>Moneda Nacional.</option>
								<option CLASS='tabmovtex' value='USD'>Dolar Americano.</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Pa&iacute;s:</td>
						<td class="tabmovtexbol" nowrap><b> *Banco Destino:</td>
						<td class="tabmovtexbol" nowrap><b> Cuenta Banco Destino
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<select name="vCvePais">
								<option value="0"></option>
							    <% writeMenu(out, cvePais,
							      				  descPais,
							    				  cvePaisSel.trim());
							    %>
							</select>
						</td>
						<td class="tabmovtexbol" nowrap>
						<!-- FSW IDS JMFR Se agregan hiddens para carga de listas spid -->
				         	<input type="hidden" id="bancosUSD" value="<%=bancosUSD%>">
							<input type="hidden" id="bancosMXM" value="<%=bancosMXM%>">
							<select id="vBanco" name="vBanco" onFocus="seleccionaBanco();">
							<option CLASS='tabmovtex' value=' '>Seleccione una Divisa.</option>
							</select>
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vCtaBancoDestino" value="<%= prov.ctaBancoDestino %>"
							maxlength="20" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Clave ABA:</td>
						<td class="tabmovtexbol" nowrap><b> Banco Corresponsal:</td>
						<td class="tabmovtexbol" nowrap><b> Plaza/Ciudad: </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vCveABA" value="<%= prov.cveABA %>" maxlength="9"
							onChange="valida_vCveABA();">
							<a name="lupaABA" href="javascript:TraerClavesABA();" border=0>
							<img src="/gifs/EnlaceMig/<%= gbo25421 %>" border=0></a>
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vDescripcionBanco" value="<%= prov.descripcionBanco %>"
							maxlength="40" onchange="this.value=this.value.toUpperCase(); return true;">
							<a href="javascript:TraerBancos();" border=0>
							<img src="/gifs/EnlaceMig/<%= gbo25421 %>" border=0></a>
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" readOnly="true" name="vDescripcionCiudad" value="<%= prov.descripcionCiudad %>"
							maxlength="40" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> *Tipo de Abono:</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
						<select name="vTipoAbono">
							<option CLASS='tabmovtex' value="0"></option>
							<option CLASS='tabmovtex' selected value="01">Global por Vencimiento</option>
							<option CLASS='tabmovtex' value="02">Individual por Documento</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Deudores activos:</td>
						<td class="tabmovtexbol" nowrap><b> L&iacute;mite de financiamiento:</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input TYPE="TEXT" NAME="vDeudores" value="<%= prov.deudoresActivos %>"
							maxlength="5" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vLimite" value="<%= prov.limiteFinanciamiento %>"
							maxlength="22" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Volumen anual de ventas</td>
						<td class="tabmovtexbol" nowrap><b> Importe medio de facturas emitidas</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vVolumen" value="<%= prov.volumenAnualVentas %>"
							maxlength="22" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vImporte" value="<%= prov.importeMedioFacturasEmitidas %>"
							maxlength="22" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>