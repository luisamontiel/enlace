<%@ page import = "mx.altec.enlace.bo.Archivos"%>
<%@ page import = "mx.altec.enlace.bo.claveProv"%>
<%@ page import = "mx.altec.enlace.bo.claveTrans"%>
<%@ page import = "java.util.*"%>

<jsp:useBean id = "arch_combos"		class = "mx.altec.enlace.bo.servicioConfirming"	scope="request"/>
<jsp:useBean id = "arch_prov"		class = "mx.altec.enlace.bo.Proveedor"			scope="request"/>

<!-- Recorta el tama�o de la cadena c(Longitud Maxima 30-35) -->
<%! public String corta(String c)
	{
	String aux = "";

	if (c.length()  < 35)
		return c;
	else
		{
		for(int i= 0; i < 30; i++)
			aux+= String.valueOf(c.charAt(i));
		return aux;
		}
	}
%>

<!-- Llena los vectores con los datos de los proveedores-->
<%
	String ArchivoResp = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","","");

	Vector ls_prov = null;
	Vector cv_prov = null;
	Vector cv_trans = null;
	Vector cv_div = null;
	Vector cv_tipo = null;
	Vector cv_origen = null;

	if(ArchivoResp != null)
		{
		ls_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",1);
		cv_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",2);
		cv_trans = arch_prov.coMtoProv_Leer(ArchivoResp,"9",3);
		/*
		* jgarcia - Stefanini
		* Creacion del vector para divisas y para el tipo de proveedor
		*/
		cv_div   = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 5);
		cv_tipo  = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 6);
		}

	if(cv_prov != null)
		{
		int a, b, total = cv_prov.size();
		String strA = null, strB = null;

		for(a=0;a<total-1;a++)
			for(b=a+1;b<total;b++)
				{
				strA = (String)ls_prov.get(a);
				strB = (String)ls_prov.get(b);
				if(strA.compareTo(strB)>0)
					{
					ls_prov.set(a,strB);
					ls_prov.set(b,strA);
					strA = (String)cv_trans.get(a);
					strB = (String)cv_trans.get(b);
					cv_trans.set(a,strB);
					cv_trans.set(b,strA);
					// jgarcia 30/Sept/2009
					// Cambia el orden de las divisas y el tipo de proveedor
					strA = (String)cv_prov.get(a);
					strB = (String)cv_prov.get(b);
					cv_prov.set(a,strB);
					cv_prov.set(b,strA);
					strA = (String)cv_div.get(a);
					strB = (String)cv_div.get(b);
					cv_div.set(a,strB);
					cv_div.set(b,strA);
					strA = (String)cv_tipo.get(a);
					strB = (String)cv_tipo.get(b);
					cv_tipo.set(a,strB);
					cv_tipo.set(b,strA);
					}
				}
		}

%>

<HTML>
<HEAD>
<TITLE> Banca Electr&oacute;nica </TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>

<script language="JavaScript">
	// jgarcia - 30/Sept/2009
	// Se agregan las variables [arregloProveedores, valorAnterior]
	// y las funciones [generarArreglo(), rellenarCombo(), ActualizaClave()]
	var arregloProveedores = new Array();
	var valorAnterior = "";
	function rellenarCombo(){
		var textoInput = trim(document.getElementById('cveProveedor').value);
		if(valorAnterior != textoInput){
			longInput = textoInput.length;
			combo = document.getElementById('Proveedor');
			//Elimina todos los elementos
			while(combo.length > 0){
				combo.remove(combo.length - 1);
			}
			var longArreglo = arregloProveedores.length;

			if(longInput > 0){
				for(var i = 0; i < longArreglo; i++){
					/*
					1 - Es el proveedor	2 - Clave del proveedor
					*/
					var datos = arregloProveedores[i].split("|");
					var claveTemp = datos[2];
					if(datos.length >= 5 && longInput <= claveTemp.length &&
						//textoInput == trim(claveTemp.substring(0, longInput))){
						textoInput == trim(claveTemp)){
						var elOptNew = document.createElement('option');
						elOptNew.text = datos[1] + "|" + datos[2] + "|" + datos[3] + "|" + datos[4];
						elOptNew.value = datos[0];
						try {
							combo.add(elOptNew, null); // No funciona con IE
						}catch(ex) {
							combo.add(elOptNew); // Solo para IE
						}
					}

				}
			}else{
				for(var i = 0; i < longArreglo; i++){
					/*
					1 - Es el proveedor	2 - Clave del proveedor
					*/
					var datos = arregloProveedores[i].split("|");
					if(datos.length >= 5){
						var elOptNew = document.createElement('option');
						elOptNew.text = datos[1] + "|" + datos[2] + "|" + datos[3] + "|" + datos[4];
						elOptNew.value = datos[0];
						try {
							combo.add(elOptNew, null); // No funciona con IE
						}catch(ex) {
							combo.add(elOptNew); // Solo para IE
						}
					}

				}
			}
			valorAnterior = textoInput;
		}
	}

	function generarArreglo(){
		combo = document.getElementById('Proveedor');
		for(var i = 0; i < combo.length; i++){
			arregloProveedores[i] = combo.options[i].value + "|" + combo.options[i].text;
		}
	}

	// -- Actualiza el combbo de Clave de Proveedor
	function ActualizaClave(){
		var prove = document.getElementById('Proveedor');
		var opciones = prove.options[prove.selectedIndex].text.split("|");
		if(opciones.length >= 2){
			var textoCombo = "";
			if(opciones[1].length > 0){
				textoCombo = trim(opciones[1]);
			}
			document.getElementById('cveProveedor').value = textoCombo;
			document.getElementById('cveProveedor').focus();
		}
	}

	function MM_preloadImages() { //v3.0
		var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
		var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
		var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	<%= request.getAttribute("newMenu") %>

	// -- Boton Consultar
	function consultar(){
			document.frmActualizaciones.submit();
	}

	function ltrim(s) {
	   return s.replace(/^\s+/, "");
	}

	function rtrim(s) {
	   return s.replace(/\s+$/, "");
	}

	function trim(s) {
	   return rtrim(ltrim(s));
	}
</SCRIPT>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); generarArreglo();" background="/gifs/EnlaceMig/gfo25010.gif">

	<!-- MENU PRINCIPAL Y ENCABEZADO -->
	<table border="0" cellpadding="0" callspacing="0" width="571">
	<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</table>
	<%= request.getAttribute("Encabezado") %>

	<br>

	<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
		<form name="frmActualizaciones" method="post" action="coMtoProveedor">
		<table border="0" cellspacing="2" cellpadding="3" width="250">
		<tr><td class="tittabdat"> Selecci&oacute;n del proveedor a consultar </td></tr>
		<tr align="center" valign="middle">
			<td class="textabdatcla" align="left" valign="top">
			<table width="150" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td align="left" nowrap class="tabmovtex11" width="100">Clave de Proveedor:</td>
				<!-- jgarcia 30/Sept/2009 Se cambia el objeto SELECT y se cambia por un INPUT -->
				<td class="tabmovtex" nowrap width="185">
					<input type="text" value="" name="cveProveedor" id="cveProveedor"
					onblur="rellenarCombo()" maxlength="20"/>
					<!--onchange="this.value=this.value.toUpperCase(); return true;"-->
				</td>
			</tr>
			<tr>
				<td align="left" nowrap class="tabmovtex11" width="60">Proveedor: </td>
				<td class="tabmovtex" nowrap WIDTH="185">

				<!-- Lista de proveedores -->
				<select name="Proveedor" id="Proveedor" class="tabmovtex" onChange="ActualizaClave()">
				<option value = "0"></option>
				<% if(ls_prov != null) {
				int longitud = ls_prov.size();
				for (int i=0; i<(longitud); i++) {
					String tipo = cv_tipo.elementAt(i).equals("I")?"INT":"NAL"; %>
				<option value="<%=cv_trans.elementAt(i)%>"><%= corta((String)ls_prov.elementAt(i)) + "|" + corta((String)cv_prov.elementAt(i)) + "|" + tipo + "|" + cv_div.elementAt(i) %></option>
				<% } } %>
				</select>

				</td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		<br>
		<A href ="javascript:consultar();"><img border="0" name="Consulta" src="/gifs/EnlaceMig/gbo25220.gif" alt="Consultar"></a>
		<input type="hidden" name="accion" value="11">
		<input type="hidden" name="opcion" value="2">
		</form>
		</td>
	</tr>
	</table>

	<%=
		(request.getAttribute("mensaje") != null)?
		("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
		");</script>"):""
	%>

</body>
</html>