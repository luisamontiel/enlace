<html>
<head>
<title>Banco</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="Codigo de Pantalla" content="s26340">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="26/03/2001  12:00">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language = "JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/Convertidor.js"></script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<script language="JavaScript">
<!--

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}



function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v3.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}


/*************************************************************************************/

/*function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}*/

function formatoEmail(texto)
{
	var formato=/^[a-zA-Z0-9\-\_\.]{1,}@[a-zA-Z0-9]{1,}(\.[a-zA-Z0-9]{1,}){1,}$/;
	if(!(formato.test(texto)))
	{
	   cuadroDialogo("El formato de su e-mail es incorrecto.\n", 2);
	   return false;
	}
	else
	   return true;
}

function seleccionTipoCarta(ObjRadio)
{
	var i=0;
	var checado=false;
	for(i=0; i<=2; i++)
	{
		if(checado = ObjRadio[i].checked)
		{
			break;
		}
	}
	if(!checado)
    {
		cuadroDialogo( "Debe elegir Queja, Sugerencia o Pregunta.", 1 );
		return false;
    }
	else
	{
		return true;
	}
}

function seleccionProducto(ObjList)
{
    var textoCombo = ObjList.options[ObjList.selectedIndex].text;
	var formato=/^[a-zA-Z0-9]{1,}$/;

	if(formato.test(textoCombo) && textoCombo!="Otros")
    {
	   cuadroDialogo( "Debe elegir un Tema", 1 );
       return false;
    }
	else
	{
		document.frmContactenos.descprod.value = textoCombo;
		//cuadroDialogo("El valor de descripcion"+document.frmContactenos.descprod.value);
		return true;
	}
}

function validaTelefono(texto)
{
	var formato=/^[0-9]{1,}$/;

	if( !formato.test(texto) )
	{
		cuadroDialogo( "Solo se permite escribir n&uacute;meros en el campo Telefono", 1 );
		return false;
    }
	else
	{
		return true;
	}
}


 function EliminaCaracteres( sCadena )
 {
   var sResultado = "";

   for( i = 0; i < sCadena.length; i++ )
   {
     c = sCadena.substring( i, i + 1 );
     if( c == '�' || c == '�' )
        c = 'N';
     else if( c == '�' || c == '�' || c == '�' || c == '�' )
        c = 'A';
     else if( c == '�' || c == '�' || c == '�' || c == '�' )
        c = 'E';
     else if( c == '�' || c == '�' || c == '�' || c == '�' )
        c = 'I';
     else if( c == '�' || c == '�' || c == '�' || c == '�' )
        c = 'O';
     else if( c == '�' || c == '�' || c == '�' || c == '�' )
        c = 'U';

     sResultado += c;
   }
   return( sResultado );
 }

function formatDato(texto)
{
	texto = EliminaCaracteres( texto );
	texto = texto.toUpperCase();

	return texto;
}

function validaForma(f)
{

	if(f.tipocontacto[0].checked == true)
	{
		if(f.email.value.length <= 0)
		{
			cuadroDialogo("Debes capturar tu E-mail", 1);
			return;
		}
		else
		{
			if(!formatoEmail(f.email.value))
			{
				return;
			}
		}
		if(f.telefono.value.length > 0)
		{
			if(!validaTelefono(f.telefono.value))
			{
				return;
			}
		}
	}
	else
	{
		if(f.telefono.value.length <= 0)
		{
			cuadroDialogo( "Debes capturar el Telefono", 1 );
			return;
		}
		else
		{
			if(!validaTelefono(f.telefono.value))
			{
				return;
			}
		}
		if(f.email.value.length > 0)
		{
			if(!formatoEmail(f.email.value))
			{
				return;
			}
		}

	}

	if(!seleccionTipoCarta(f.tipocarta))
	{
		return;
	}

	if(!seleccionProducto(f.producto))
	{
		return;
	}

	if(f.comentarios.value.length <= 0)
	{
		cuadroDialogo("Por favor escriba sus comentarios.", 1);
		return;
	}

	f.submit();

}

/*
function actualizaDesc(selObj)
{
	document.frmContactenos.descprod.value = selObj.options[selObj.selectedIndex].text;
}
*/
function resetForma(f)
{
	//f.descprod.value = ""
	f.reset();
}

<%= request.getAttribute("newMenu") %>

//-->
</script>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
	   <%= request.getAttribute("MenuPrincipal") %>
    </TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <form name="frmContactenos" method="post" ACTION="Contactenos">
    <tr>
      <td width="200" valign="top">
        <table width="200" border="0" cellspacing="5" cellpadding="0">
          <tr>
           <!-- <td><img src="/gifs/EnlaceMig/gti25050.gif" width="57" height="25"></td>-->
          </tr>
        </table>
        <table width="200" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11">
              <blockquote>
           <!--     <p>&iquest;Ya revis&oacute;<a href="javascript:hlp=window.open( 'http://www.santander.com.mx/html/website/s55980.html', 'Faqs', 'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=770,height=400'); hlp.focus();"> nuestra base de preguntas
                  y respuestas</a>?. Nuestros clientes generalmente encuentran
                  soluci&oacute;n a sus dudas en nuestros FAQ,s.</p> -->
                <p>Nuestro departamento de servico a clientes le atender&aacute;
                  gustosamente al recibir la siguiente forma.</p>
              </blockquote>
            </td>
          </tr>
        </table>
      </td>
      <td bgcolor="#CCCCCC" width="1"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
      <td width="358" valign="top">
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr>
                  <td nowrap class="tabmovtex11">Empresa:</td>
                  <td class="tabmovtexbol">
					<%= request.getAttribute("nombreEmpresa") %>
				  </td>
                </tr>
                <tr>
                  <td nowrap class="tabmovtex11">C&oacute;digo de cliente: </td>
                  <td class="tabmovtexbol">
					<script>
						 document.write(cliente_7To8("<%=request.getAttribute("codigoUsuario")%>"));
					</script>
				  </td>
                </tr>
                <tr>
                  <td nowrap class="tabmovtex11">Usuario:</td>
                  <td class="tabmovtexbol">
					<%= request.getAttribute("nombreUsuario") %>
				  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0" height="1">
          <tr>
            <td class="tabmovtex11" valign="top" bgcolor="#CCCCCC" height="1"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr>
                  <td class="tabmovtex11">&iquest;C&oacute;mo prefiere que le
                    contactemos?</td>
                  <td class="tabmovtex11">
                    <input type="radio" name="tipocontacto" value="1" checked>
                  </td>
                  <td class="tabmovtex11"> Email </td>
                  <td class="tabmovtex11">
                    <input type="radio" name="tipocontacto" value="2">
                  </td>
                  <td class="tabmovtex11"> Tel&eacute;fono</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top" bgcolor="#CCCCCC"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top">
              <table border="0" cellspacing="3" cellpadding="0" width="348">
                <tr>
                  <td class="tabmovtex11" width="170">E-mail:<br>
                    <input type="text" name="email" size="20">
                  </td>
                  <td class="tabmovtex11" width="169">Tel&eacute;fono:<br>
                    <input type="text" name="telefono" size="20">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top" bgcolor="#CCCCCC"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top">
              <table border="0" cellspacing="3" cellpadding="0" width="348">
                <tr>
                  <td class="tabmovtex11" width="20">
                    <input type="radio" name="tipocarta" value="Queja">
                  </td>
                  <td class="tabmovtex11" width="80">Queja</td>
                  <td class="tabmovtex11" rowspan="3" valign="top">Tema:<br>
                    <select name="producto" class="tabmovtex11">
						<%= request.getAttribute("combo") %>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex11">
                    <input type="radio" name="tipocarta" value="Sugerencia">
                  </td>
                  <td class="tabmovtex11">Sugerencia</td>
                </tr>
                <tr>
                  <td class="tabmovtex11">
                    <input type="radio" name="tipocarta" value="Pregunta">
                  </td>
                  <td class="tabmovtex11">Pregunta</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top" bgcolor="#CCCCCC"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top">
              <table border="0" cellspacing="3" cellpadding="0" width="348">
                <tr>
                  <td class="tabmovtex11" width="150">
                    <p>Comentarios:<br>
                      <textarea name="comentarios" cols="50" rows="7"></textarea>
                    </p>
                    </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top" align="center">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="78">
	  				<A href="javascript:validaForma(document.frmContactenos);" border=0><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
                  </td>
                  <td width="76">
				  	<A href="javascript:;" onclick="resetForma(document.frmContactenos);" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="76" height="22"></a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td bgcolor="#CCCCCC" width="1"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
      <td width="200" align="center" valign="top">
        <p>&nbsp;</p>
        <p><img src="/gifs/EnlaceMig/gil25040.gif" width="168" height="168"></p>
      </td>
    </tr>
    <input type="hidden" name="ventana"  value="1">
    <input type="hidden" name="descprod" value="">
  </form>
</table>
</body>
</html>