<html>

<head>

<title>Enlace</title>

<meta http-equiv="Content-Type" content="text/html;" />
<meta name="Codigo de Pantalla" content="s25020" />
<meta name="Proyecto" content="Portal" />
<meta name="Version" content="1.0" />
<meta name="Ultima version" content="05/09/2002 18:00" />
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico" />

<script type="text/javascript"  src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript"  src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript">

function enviarpos()
{
	var ban1=true;
	document.fsaldo.action="poscpsrvr1";
	ban1=validar(1);

	if (ban1==true){
	 	document.getElementById("botones").style.visibility="hidden";
		document.getElementById("mensaje").style.visibility="visible";
		document.fsaldo.submit();
	}
}

function enviarmovs()
{
	var ban1=true;
	document.fsaldo.action="CMovimiento?Banca=N";
	ban1=validar(1);
	if (ban1==true)
	{
     document.getElementById("botones").style.visibility="hidden";
     document.getElementById("mensaje").style.visibility="visible";
	document.fsaldo.submit();
	}
}

function validar(boton)
{
	var cta;
	var resultado=false;
	var contador=0;
	for (i=0;i<document.fsaldo.elements.length;i++){
		if (document.fsaldo.elements[i].type=="radio" && document.fsaldo.elements[i].name=='cta_posi'){
			if (document.fsaldo.elements[i].checked==true){
				contador++;
				resultado=true;
				break;
			}
		}
	}
	if (contador==0)
	cuadroDialogo("Usted no ha seleccionado una cuenta.",3);
	return resultado;
}


function cambiaArchivo(extencion) {
    var boton = document.getElementById("exportar");
    if (extencion==="txt") {
		document.getElementById("tArchivo").value = extencion;
        document.getElementById("idTxt").checked = true;
    }else if(extencion==="csv"){
		document.getElementById("tArchivo").value = extencion;
        document.getElementById("idCsv").checked = true;
    }
    boton.href="javascript:Exportar();";
}


function Exportar(){
 	
	var forma=document.fsaldo;
	var contador=0;

	for(j=0;j<forma.length;j++){
		if(forma.elements[j].type=='radio' && forma.elements[j].name=='tipoArchivo'){
			if(forma.elements[j].checked){
			    contador++;								
			}		
		}
	}
			
	if(contador===0){
		cuadroDialogo("Indique el tipo de archivo a exportar ",3);
 		return;
	}else{
		var extencion = document.getElementById("tArchivo").value;
		window.open("/Enlace/enlaceMig/cuentasaldo?tArchivo=" + extencion,"Saldo","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	}

}


</script>


<script type="text/javascript"  src="/EnlaceMig/fw_menu.js"></script>


<script type="text/javascript">
<!--

function MM_preloadImages() { //v3.0
	var d=document;
	if(d.images){
		if(!d.MM_p)
			d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
	}
}
function MM_swapImgRestore() { //v3.0
	var i,x,a=document.MM_sr;
	for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
	var p,i,x;
	if(!d) d=document;
	if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%= request.getAttribute( "newMenu" ) %>

//-->
</script>



<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
<tr valign="top">
<td width="*">


<%= request.getAttribute( "MenuPrincipal" ) %></td>
</tr>
</table>
<%= request.getAttribute( "Encabezado" ) %>

<table width="760" border="0" cellspacing="0" cellpadding="0">
<form  name="fsaldo" method="post" action="poscpsrvr1" onSubmit="return validar();">
<input id="tArchivo" type="hidden" name="tArchivo" value="csv" readonly="readonly" />
<tr>
<td align="center">

<table width="750" border="0">
<tr>
   <td class="texenccon">
	    <span class="texencconbol">Saldo total moneda nacional:</span>
		  &nbsp;<%= (String)(request.getAttribute("TotalPesos")) %>&nbsp;
	 </td>
</tr>

<tr>
   <td class="texenccon">
		  <span class="texencconbol">Saldo total d&oacute;lares:</span>
		  &nbsp;<%= (String)(request.getAttribute("TotalDls")) %>&nbsp;
	 </td>
</tr>
</table>

<table width="750" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
<tr>
<td align="center" class="tittabdat" width="65">Seleccione </td>
<td width="70" class="tittabdat" align="center">Cuenta</td>
<td class="tittabdat" align="center" colspan="2">Descripci&oacute;n</td>
<td class="tittabdat" align="center" width="90">Disponible</td>
<td class="tittabdat" align="center" width="90">SBC</td>
<td class="tittabdat" align="center" width="90">Total</td>
</tr>
<%= request.getAttribute( "cta" ) %>
</table>
<br/>
		  <table style="margin-left: auto; margin-right: auto;">
		  	<tr>
		  		<td><input id="idTxt" type="radio" value="txt" name="tipoArchivo" onclick="cambiaArchivo('txt');" /></td>
		  		<td class="tabmovtex11">Exporta en TXT</td>
		  	</tr>
		  	<tr>
		  		<td><input id="idCsv" type="radio" value="csv" name="tipoArchivo" onclick="cambiaArchivo('csv');"  checked="checked"  /></td>
		  		<td class="tabmovtex11">Exporta en XLS</td>
		  	</tr>
		  </table>


<table width="357" height="22" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
<tr id="mensaje" align="center"  style="visibility:hidden">
       				<td colspan="4" class="tabmovtex">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
</tr>
<tr id="botones">
<td align="center" height="22">
<a href="javascript:enviarmovs();" border=0>
<img  src="/gifs/EnlaceMig/gbo25260.gif" border=0 >
</a>
</td>
<td align="center" height="22">
<a href="javascript:enviarpos();" border=0>
<img  src="/gifs/EnlaceMig/gbo25270.gif" border=0>
</a>
</td>
<td align="center" height="22">
<a href="javascript:scrImpresion();" border=0 >
<img  src="/gifs/EnlaceMig/gbo25240.gif" border=0>
</a>
</td>
<td align="center" valign="top" height="22">
<%= request.getAttribute( "DownLoadFile" ).toString().trim() %>
</td>
</tr>
</table>

</td>
</tr>
</form>
</table>
<br/>

</body>


<head>
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache" />
<meta HTTP-EQUIV="Expires" CONTENT="-1" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache" />
<meta HTTP-EQUIV="Expires" CONTENT="-1" />
</head>

</html>