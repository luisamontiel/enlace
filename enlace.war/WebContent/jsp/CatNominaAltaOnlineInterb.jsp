<%--*******************************************************
	**	Alta de Empleados en el Cat�logo de N�mina   	 **
	** 	con cuenta interbancaria en L�nea				 **
	**													 **
	**  06/02/2008 		Everis M�xico					 **
	*******************************************************	--%>

<html>
<head>
<title>Alta de Empleados con Cuenta Interbancaria</title>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<script type="text/javascript" language="javascript">

/********************************************************/
/*    Funcion continua - Everis 24/10/2007  		*/
/********************************************************/
function continua()
{
	if(respuesta == 1)
	{
	document.altaEmplInterbOnline.opcion.value = "2";
	document.altaEmplInterbOnline.action = "CatalogoNominaInterb";
	document.altaEmplInterbOnline.submit();
	}
}

/********************************************************/
/*    Funcion Limpiar - Everis 15/02/2008    */
/********************************************************/
function Limpiar()
{
	document.altaEmplInterbOnline.numCuenta.value       = "";
	document.altaEmplInterbOnline.numEmpl.value  		= "";
	document.altaEmplInterbOnline.numDepto.value        = "";
	document.altaEmplInterbOnline.sueldo.value  		= "0";
	document.altaEmplInterbOnline.nombre.value			= "";
	document.altaEmplInterbOnline.apellidoP.value       = "";
	document.altaEmplInterbOnline.apellidoM.value       = "";
	document.altaEmplInterbOnline.rfc.value        		= "";
	document.altaEmplInterbOnline.sexo[0].checked=true;
	document.altaEmplInterbOnline.calleNumero.value     = "";
	document.altaEmplInterbOnline.colonia.value        	= "";
	document.altaEmplInterbOnline.delegMunicipio.value  = "";
	document.altaEmplInterbOnline.claveEstado.value     = "";
	document.altaEmplInterbOnline.ciudadPob.value       = "";
	document.altaEmplInterbOnline.codigoPostal.value    = "";
	document.altaEmplInterbOnline.clavePais.value       = "";
	document.altaEmplInterbOnline.prefTelCasa.value     = "0";
	document.altaEmplInterbOnline.telCasa.value        	= "0";
	document.altaEmplInterbOnline.calleNumOfi.value     = "";
	document.altaEmplInterbOnline.coloniaOfi.value      = "";
	document.altaEmplInterbOnline.delegMunOfi.value     = "";
	document.altaEmplInterbOnline.claveEstadoOfi.value  = "";
	document.altaEmplInterbOnline.ciudadPobOfi.value    = "";
	document.altaEmplInterbOnline.codigoPostalOfi.value = "";
	document.altaEmplInterbOnline.clavePaisOfi.value    = "";
	document.altaEmplInterbOnline.claveDireccion.value  = "0";
	document.altaEmplInterbOnline.prefTelOfi.value      = "0";
	document.altaEmplInterbOnline.telOfi.value        	= "0";
	document.altaEmplInterbOnline.extOfi.value        	= "0";
}
//  -- Fin de la funci�n Limpiar


/********************************************************/
/*    Funcion esNumero - Everis 24/10/2007  			*/
/********************************************************/
function esNumero(campo, nombreCampo){
	//alert("Entramos esNumero: ");
	tmp = campo.value;
	//	alert("Entramos esNumero: tmp = " + tmp);
	var numeros="0123456789.,";
	if(tmp.length == 0)
		return false;
    for (i = 0; i<tmp.length; i++) {
		car=tmp.charAt(i)
		if (numeros.indexOf(car)==-1) {
			campo.value="";
			campo.focus();
			cuadroDialogo("El campo "+nombreCampo+" contiene car&aacute;cteres no v&#225;lidos.  Ingrese s&Oacute;lo con car&aacute;cteres num&eacute;ricos", 3);
			return false
		}
	}
	/*var ingre = pantallaModif.IngresoFormato.value;
	alert("Entramos esNumero: ingre = " + ingre);
	var comaExiste = ingre.indexOf(",")
	var puntoExiste = ingre.indexOf(".")

	do{
		comaExiste = ingre.indexOf(",")
		puntoExiste = ingre.indexOf(".")
		ingre = ingre.replace(",","")
		alert("Entramos esNumero: do ingre = " + ingre);
		//ingre = ingre.replace(".","")
		//alert("Entramos esNumero: do punto ingre = " + ingre);
	}while( comaExiste != -1 && puntoExiste != -1)

	pantallaModif.Ingreso.value = ingre;*/

	return true
}
/*********  Fin Funcion esNumero ************/


/********************************************************/
/*    Funcion altaEmplInterb - Everis 15/02/2008  		*/
/********************************************************/
function altaEmplInterb()
{
	var correcto = Valida(document.altaEmplInterbOnline);
	return;
}
/*********  Fin Funcion Modificar   ************/

/********************************************************/
/*    Funcion validaCaracter - Everis 24/10/2007  		*/
/********************************************************/
function validaCaracter(campo, nombre)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;

//forma = document.datosTmp1;
	if(nombre == "CALLE Y NUMERO")
		var caracteresValidos="ABCDEFGHIJKLMN�OPQRSTUVWXYZ.,-0123456789# "
	else
		var caracteresValidos="ABCDEFGHIJKLMN�OPQRSTUVWXYZ.,-0123456789 "

	cadena=campo.value;

    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (caracteresValidos.indexOf(car)==-1) {
			campo.value="";
			campo.focus();
			cuadroDialogo("El campo "+ nombre +" contiene caracteres no v&aacute;lidos", 3);
			return false;
		}
	}
	return true;
}
/*********  Fin Funcion validaCaracter ************/


/********************************************************/
/*    Funcion validaLongCuenta - Everis 15/102/2008  		*/
/********************************************************/
function validaLongCuenta(campo)
{
	tmp = campo.value;
	longitud = tmp.length;

	if(longitud != 18){
		campo.value="";
		campo.focus();
		cuadroDialogo("El campo N&uacute;mero de Cuenta debe de contar con 18 d&iacute;gitos", 3);
		return false;
	}

	// Inicio - Validacion de Cuenta CLABE Santander
	var codBanco = tmp.substring(0,3);

	if(codBanco == "014"){
		campo.value="";
		campo.focus();
		cuadroDialogo("La CLABE ingresada pertenece a Banco Santander, no se puede considerar como cuenta Interbancaria", 3);
		return false;
	}
	// Fin - validaci�n de Cuenta CLABE Santander
	return true
}
/*********  Fin Funcion validaLongCuenta ************/


/********************************************************/
/*    Funcion validaRFC - Everis 24/10/2007  		*/
/********************************************************/
function validaRFC(campo)
{
	var texto = campo.value;
	texto=texto.toUpperCase();
	campo.value=texto;
	longitud = texto.length;

	if(longitud != 10 && longitud != 13){
		campo.value="";
		campo.focus();
		cuadroDialogo("El campo RFC debe de contar con 10 o 13 d&iacute;gitos", 3);
		return false;
	}
	var iniciales = texto.substring(0,4);
	var fechaNac = texto.substring(4,10);
	var homoclave = texto.substring(10,13);

	if(!validaNombreRFC(iniciales)){
		campo.value="";
		campo.focus();
		cuadroDialogo("Las inciales del nombre en el RFC deben de ser Letras", 3);
		return false;
	}

	if(!validaFechaNacRFC(fechaNac)){
		campo.value="";
		campo.focus();
		cuadroDialogo("La fecha de nacimiento del RFC debe de ser n&uacute;merica", 3);
		return false;
 	}

 	if(!validaHomoclaveRFC(homoclave)){
		campo.value="";
		campo.focus();
		cuadroDialogo("La homoclave del RFC debe de ser alfan&uacute;merica", 3);
		return false;
 	}
 	return true
}

/********************************************************/
/*    Funcion validaFechaNacRFC - Everis 15/02/2008  			*/
/********************************************************/
function validaFechaNacRFC(fechaNac){
	tmp = fechaNac;
	var numeros="0123456789.";
    for (i = 0; i<tmp.length; i++) {
		car=tmp.charAt(i)
		if (numeros.indexOf(car)==-1) {
			return false
		}
	}
	return true
}
/*********  Fin Funcion validaFechaNacRFC ************/


/********************************************************/
/*    Funcion validaNombreRFC - Everis 15/02/2008  		*/
/********************************************************/
function validaNombreRFC(iniciales){
var caracteresValidos="ABCDEFGHIJKLMN�OPQRSTUVWXYZ"
cadena=iniciales;

    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (caracteresValidos.indexOf(car)==-1) {
			return false;
		}
	}
	return true;
}
/*********  Fin Funcion validaNombreRFC   ************/

/********************************************************/
/*    Funcion validaHomoclaveRFC - Everis 15/02/2008  		*/
/********************************************************/
function validaHomoclaveRFC(homoclave)
{
	var caracteresValidos="ABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789"
	cadena=homoclave;

    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (caracteresValidos.indexOf(car)==-1) {
			return false;
		}
	}
	return true;
}

/********************************************************/
/*    Funcion acceptNum - Everis 24/10/2007  			*/
/********************************************************/
var nav4 = window.Event ? true : false;
function acceptNum(evt)
	{
	// NOTA: Backspace = 8, Enter = 13, '0' = 48, '9' = 57
		var key = nav4 ? evt.which : evt.keyCode;
		return (key <= 13 || (key >= 48 && key <= 57));
	}
/*********  	Fin Funcion acceptNum	************/


/********************************************************/
/*    Funcion Valida - Everis 15/02/2008  				*/
/********************************************************/
function Valida(forma){
	flag=false;
	espacios = /^ {1}/;
	camposVacios = "";

	//pantallaModif.Ingreso.value = formateaIngreso(pantallaModif.Ingreso,"Sueldo")

	if(forma.numCuenta.value == "" || forma.numCuenta.value.search(espacios) != -1){
		camposVacios += "N�m de Cuenta, ";
		flag = true;
	}
	if(forma.numEmpl.value == "" || forma.numEmpl.value.search(espacios) != -1){
		camposVacios += "N�m de Empleado, ";
		flag = true;
	}
	if(forma.sueldo.value == "" || forma.sueldo.value.search(espacios) != -1){
		camposVacios += "Sueldo, ";
		flag = true;
	}
	if(forma.nombre.value == "" || forma.nombre.value.search(espacios) != -1){
		camposVacios += "Nombre, ";
		flag = true;
	}
	if(forma.apellidoP.value == "" || forma.apellidoP.value.search(espacios) != -1){
		camposVacios += "Apellido Paterno, ";
		flag = true;
	}
	if(forma.rfc.value == "" || forma.rfc.value.search(espacios) != -1){
		camposVacios += "RFC, ";
		flag = true;
	}
	if(flag == true)
		cuadroDialogo("Por favor ingrese la siguiente informaci&oacute;n faltante: <br>  " + camposVacios,1);
	else
		if(cuadroDialogo("Esta seguro de querer guardar la informaci&oacute;n <br> ingresada en la pantalla ", 2));
			return true;

	//pantallaModif.Depto.focus(); return;
	//flag=true;

	return flag;
}
/*********  	Fin Funcion Valida	************/


<% if (request.getAttribute("newMenu") != null ) { %>
<%= request.getAttribute("newMenu") %>
<%	} %>


</script>

<%--<link rel="stylesheet" href="EnlaceMig/consultas.css" type="text/css">--%>
</head>


<BODY>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">
			<!-- MENU PRINCIPAL -->
			<% if (request.getAttribute("MenuPrincipal") != null ) { %>
			<%= request.getAttribute("MenuPrincipal") %>
			<%	} %>
		</TD>
	</TR>
</TABLE>

<% if (request.getAttribute("Encabezado") != null ) { %>
<%= request.getAttribute("Encabezado") %>
<%	} %>

	<div>
		<form method="get" name="altaEmplInterbOnline" action="">
			<input type="hidden" name="opcion" value="">

			<table width="1000" border="0" class="textabdatcla">
				<tbody>
					<tr>
						<td class="tittabdat" colspan="6">Capture los datos para el Alta de Empleado con cuenta Interbancaria</td>
					</tr>
					<tr>
						<td class="tabmovtex" colspan="6">Los campos con (<font color="#ff0000"><font face="Arial">*</font></font>) son obligatorios.</td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Num. de Cuenta<font color="#ff0000"><font face="Arial">*</font></font>: </td>
						<td width="50"><input type="text" name="numCuenta" maxlength="18" onKeyPress="return acceptNum(event)" onchange="validaLongCuenta(this)"></td>
						<td class="tabmovtex" width="50">Num.de Empleado<font color="#ff0000"><font face="Arial">*</font></font>:</td>
						<td width="50"><input type="text" name="numEmpl" maxlength="7" size="7" onchange="validaCaracter(altaEmplInterbOnline.numEmpl, 'N�MERO DE EMPLEADO')"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Num. Depto:</td><td width="50"><input type="text" maxlength="6" name="numDepto" size="6" onchange="validaCaracter(altaEmplInterbOnline.numDepto, 'N�MERO DE DEPARTAMENTO')"></td>
						<td class="tabmovtex" width="50">Sueldo<font color="#ff0000"><font face="Arial">*</font></font>:</td><td width="50">
				<%--	<input type="text" maxlength="20" name="IngresoFormato" value="" onchange="esNumero(pantallaModif.IngresoFormato, 'INGRESO MENSUAL')" ></td>--%>
<%--						<input type="text" maxlength="20" name="sueldo"></td>--%>
	<%--					<INPUT TYPE="text" maxlength="20" NAME="sueldo" SIZE="10" value="" onKeyPress="return acceptNum(event)">--%>
							<INPUT TYPE="text" maxlength="20" NAME="sueldo" SIZE="10" value="" onchange="esNumero(altaEmplInterbOnline.sueldo, 'INGRESO MENSUAL')" >
					</tr>

					<tr>
					<td class="tittabdat" colspan="6">Datos Personales</td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Nombre(s)<font color="#ff0000"><font face="Arial">*</font></font>:</td><td width="50"><input type="text" name="nombre" maxlength="30" onchange="validaCaracter(altaEmplInterbOnline.nombre, 'NOMBRE')"></td>
						<td class="tabmovtex" width="50">Apellido Paterno<font color="#ff0000"><font face="Arial">*</font></font>:</td><td width="50"><input type="text" name="apellidoP" maxlength="30" onchange="validaCaracter(altaEmplInterbOnline.apellidoP, 'APELLIDO PATERNO')"></td>
						<td class="tabmovtex" width="50">Apellido Materno:</td><td width="50" > <input type="text" name="apellidoM" maxlength="30" onchange="validaCaracter(altaEmplInterbOnline.apellidoM, 'APELLIDO MATERNO')"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">R.F.C.<font color="#ff0000"><font face="Arial">*:</font></font> </td>
						<td><input type="text" name="rfc" maxlength="13"  onchange="validaRFC(this)"></td>
						<td class="tabmovtex" width="50">Sexo<font color="#ff0000"><font face="Arial">:</font></font> </td>
						<td><input type="radio" name="sexo" value="F" checked><font size="1">F</font><input type="radio" value="M" name="sexo"><font size="1">M</td>

					</tr>


					<tr>
						<td class="tabmovtex" width="50">Calle y N�mero: </td><td width="50"><input type="text" name="calleNumero" maxlength="60" size="30" onchange="validaCaracter(altaEmplInterbOnline.calleNumero, 'CALLE Y NUMERO')"></td>
						<td class="tabmovtex" width="50">Colonia:</td><td width="50"><input type="text" maxlength="30" name="colonia" size="30" onchange="validaCaracter(altaEmplInterbOnline.colonia, 'COLONIA')"></td>
						<td class="tabmovtex" width="50">Delegaci�n o Municipio:</td><td width="50"><input type="text" maxlength="35" name="delegMunicipio" onchange="validaCaracter(altaEmplInterbOnline.delegMunicipio, 'DELEGACI�N O MUNICIPIO')"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave Estado:</td><td width="50"> <input type="text" name="claveEstado" maxlength="4" onchange="validaCaracter(altaEmplInterbOnline.claveEstado, 'CLAVE DE ESTADO')"></td>
						<td class="tabmovtex" width="50">Ciudad o Poblaci&oacute;n: </td><td><input type="text" maxlength="35" name="ciudadPob" onchange="validaCaracter(altaEmplInterbOnline.ciudadPob, 'CIUDAD O POBLACION')"></td>
						<td class="tabmovtex" width="50">C&oacute;digo Postal: </td><td><input type="text" name="codigoPostal" maxlength="5" size="5" onKeyPress="return acceptNum(event)"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave del Pa&iacute;s: </td><td width="50"><input type="text" maxlength="4" name="clavePais" size="4" onchange="validaCaracter(altaEmplInterbOnline.clavePais, 'CLAVE DE PAIS')"></td>
						<td class="tabmovtex" width="50">Prefijo tel. Particular:</td><td width="50"><input type="text" maxlength="12" name="prefTelCasa" size="12" onKeyPress="return acceptNum(event)"></td>
						<td class="tabmovtex" width="50">Tel. Particular:</td><td><input type="text" maxlength="12" name="telCasa" size="12" onKeyPress="return acceptNum(event)"></td>

					</tr>

					<tr>
						<td class="tittabdat" colspan="6">Informaci�n de Trabajo</td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Calle y N�mero:</td><td width="50"><input type="text" maxlength="60" name="calleNumOfi" onchange="validaCaracter(altaEmplInterbOnline.calleNumOfi, 'CALLE Y N�MERO DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Colonia:</td><td><input type="text" maxlength="30" name="coloniaOfi" onchange="validaCaracter(altaEmplInterbOnline.coloniaOfi, 'COLONIA DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Delegaci�n o Municipio:</td><td><input type="text" maxlength="35" name="delegMunOfi" onchange="validaCaracter(altaEmplInterbOnline.delegMunOfi, 'DELEGACI�N O MUNICIPIO DE OFICINA')"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave de Estado:</td><td width="50"><input type="text" name="claveEstadoOfi" maxlength="4" size="4" onchange="validaCaracter(altaEmplInterbOnline.claveEstadoOfi, 'CLAVE DE ESTADO DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Ciudad o Poblaci&oacute;n:</td><td><input type="text" maxlength="35" name="ciudadPobOfi" onchange="validaCaracter(altaEmplInterbOnline.ciudadPobOfi, 'CIUDAD O POBLACI�N DE OFICINA')"></td>
						<td class="tabmovtex" width="50">C&oacute;digo Postal:</td><td><input type="text" name="codigoPostalOfi" maxlength="5" size="5" onKeyPress="return acceptNum(event)"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave Pa�s:</td><td width="50"><input type="text" maxlength="4" name="clavePaisOfi" size="4" onchange="validaCaracter(altaEmplInterbOnline.clavePaisOfi, 'CLAVE DE PAIS DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Clave Direcci&oacute;n:</td><td><input type="text" maxlength="7" name="claveDireccion" size="7" onKeyPress="return acceptNum(event)"></td>
						<td class="tabmovtex" width="50">Prefijo:</td><td><input type="text" maxlength="12" name="prefTelOfi" size="7" onKeyPress="return acceptNum(event)"></td>

					</tr>
					<tr>
												<td class="tabmovtex" width="50">Tel&eacute;fono:</td><td><input type="text" maxlength="12" name="telOfi" size="12" onKeyPress="return acceptNum(event)"></td>
						<td class="tabmovtex" width="50">Extensi&oacute;n:</td><td width="50"><input type="text" maxlength="12" name="extOfi" size="7" onKeyPress="return acceptNum(event)"></td>
					</tr>

				</tbody>
			</table>
		<br><br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<a href="javascript:altaEmplInterb();"><img src="/gifs/EnlaceMig/enviar.jpg" border="0" ></a>
		<a href="javascript:Limpiar();"><img src="/gifs/EnlaceMig/gbo25250.gif" border="0"></a>



			<script type="text/javascript" language="javascript">
				<% if (request.getAttribute("MensajeLogin01") != null ) { %>
					<%= request.getAttribute("MensajeLogin01") %>
				<%	} %>
			</script>
	</form>
</div>


</body>
</html>