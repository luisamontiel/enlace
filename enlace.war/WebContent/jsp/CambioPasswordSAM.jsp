<!--
Altec Mexico
Proyecto: 200710001 Supernet Empresas Seguridad Informatica
Autor: Thomas Isomaki
Fecha: 12/06/2007
Empresa: Vision Consulting
Descripción componente: Manejo de Cambio de Contraseña por medio de SAM
-->

<!--<VC proyecto="200710001" autor="TIB" fecha="12/06/2007" descripción="CAMBIO DE CONTRASENA">-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>

<%
mx.altec.enlace.servlets.BaseServlet bs = new  mx.altec.enlace.servlets.BaseServlet();
mx.altec.enlace.bo.BaseResource brSess = (mx.altec.enlace.bo.BaseResource)session.getAttribute("session");

if (brSess!=null) {
	bs.cierraDuplicidad(brSess.getUserID8());
}else {
	String user = request.getHeader("IV-USER");
	if (user!=null && !user.equals("")) {
		bs.cierraDuplicidad(user);
	}
}

request.getSession().invalidate();

request.setAttribute( "MensajeLogin01", "cuadroDialogo(\"Su Contrase&ntilde;a ha cambiado, reinicie su sesi&oacute;n\", 1);" );
bs.evalTemplate(mx.altec.enlace.utilerias.IEnlace.LOGIN, request, response);
%>
<!--</VC>-->