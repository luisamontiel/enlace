<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<html>
	<head>
		<title>Detalle Asignacion Tarjetas</title>
	</head>
	<body>
		<%
		LMXC lmxc = (LMXC) request.getAttribute("RemesasBean");
		if (lmxc != null && lmxc.getListaRemesas() != null && lmxc.getListaRemesas().size() > 0) {
		int numRemesas = lmxc.getListaRemesas().size();
%>
		<table>
			<tr>
				<td align="left">
					Total de remesas: <%=numRemesas%>
				</td>
			</tr>
			<tr>
				<td align="center">
					<table width="500" border="1">
						<tr>
							<td align="center" width="275" class="tittabdat">N&uacute;mero Remesa
							</td>
							<td align="center" width="275" class="tittabdat">Estatus
							</td>
						</tr>
						<%
						int tamano = lmxc.getListaRemesas().size();

						Iterator iterator = lmxc.getListaRemesas().iterator();

						while (iterator.hasNext()) {

							RemesasBean remesa = (RemesasBean) iterator.next();
						%>
						<tr>
							<td> <%= remesa.getNumRemesa() %></td>
							<td><%= remesa.getEstadoRemesa() %></td>
						</tr>
						<%
						}
						%>

					</table>
				</td>
			</tr>
	</table>
	<%
			}else{
		%>
<table>
	<tr>
		<td align="center">
			<table border="1">
				<tr>
					<td align="center">
						<br/>El contrato no tiene remesas asociadas<br/><br/>
					</td>
				</tr>
			</table>
			<br/>
		</td>
	</tr>
</table>
<%	} %>
		<br>
	</body>
</html>