<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
 <head>
  <title>Enlace Internet</title>
  <!--
    Elaborado:  Francisco Serrato Jimenez (fsj)
    Aplicativo: Enlace Internet
    Proyecto:   MX-2002-257
                Getronics CP Mexico
    Archivo:   movimientos.jsp
  -->
  <script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
  <script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script language="JavaScript" src="/EnlaceMig/fw_menu.js"></script>
  <script language = "Javascript" >
  
	function cambiaArchivo(extencion) {
		var boton = document.getElementById("exportar");
		if (extencion==="txt") {
			document.getElementById("tArchivo").value =extencion;
			document.getElementById("idTxt").checked= true;
		}else if(extencion==="csv"){
			document.getElementById("tArchivo").value =extencion;
			document.getElementById("idCsv").checked= true;
		}
		boton.href="javascript:Exportar();";
	}


	function Exportar(){
		var forma=document.frmbit;
		
		var contador=0;

		for(j=0;j<forma.length;j++){
			if(forma.elements[j].type=='radio' && forma.elements[j].name=='tipoArchivo'){
				if(forma.elements[j].checked){
					contador++;								
				}		
			}
		}
			
		if(contador===0){
			cuadroDialogo("Indique el tipo de archivo a exportar ",1);
			return;
		}else{
			var extencion = document.getElementById("tArchivo").value;
			var cuenta = document.getElementById("cuenta").value;
			var tipoCBan = document.frmbit.Banca.value;  
			window.open("/Enlace/enlaceMig/movimientos?tExportacion=" + extencion+"&tipoConsulta=Chequeras&cuenta="+cuenta+"&tipoCBan="+tipoCBan,"Consultas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		}
	}  
  
  
   //<!--
    var Nmuestra = <%= request.getAttribute("varpaginacion") %>;
    function atras()
    {
        //alert("entrando a funcion atras");
        if( (parseInt(document.frmbit.prev.value) - Nmuestra)>=0 )
        {
            document.frmbit.next.value = document.frmbit.prev.value;
            document.frmbit.prev.value = parseInt(document.frmbit.prev.value) - Nmuestra;
            document.frmbit.submit();
        }
    }
    function adelante()
    {
        if( parseInt(document.frmbit.next.value) != parseInt(document.frmbit.total.value) )
        {
            if( (parseInt(document.frmbit.next.value) + Nmuestra) <= parseInt(document.frmbit.total.value) )
            {
                document.frmbit.prev.value = document.frmbit.next.value;
                document.frmbit.next.value = parseInt(document.frmbit.next.value) + Nmuestra;
                document.frmbit.submit();
            }
            else if( (parseInt(document.frmbit.next.value) + Nmuestra) > parseInt(document.frmbit.total.value) )
            {
                document.frmbit.prev.value = document.frmbit.next.value;
                document.frmbit.next.value = parseInt(document.frmbit.next.value) + (parseInt(document.frmbit.total.value) - parseInt(document.frmbit.next.value));
                document.frmbit.submit();
            }
        }
        else
            cuadroDialogo("no hay mas registros", 1);
    }
    function MakeFile()
    {
        document.frmbit.prev.value = 0;
        document.frmbit.next.value = document.frmbit.total.value;
        document.frmbit.action = "CreaArchMov";
        document.frmbit.submit();
    }
    function llamar_detalle(fecha,cargo_abono,referencia_390)
    {
        document.frmbit.TMov.value="PMDM";
        var varfecha=""+fecha;
        var varreferencia_390=""+referencia_390
        //se le agreg� un 1 al inicio, para que no lo tome como octal en caso de que el
        //dia de la fecha sea menor a 10
        document.frmbit.pampa_fecha.value=varfecha.substring(1,varfecha.length);
        document.frmbit.pampa_importe.value=cargo_abono;
        //se le agreg� un 1 al inicio, para que no lo tome como octal en caso de que el
        //primer digito de la referencia sea=0
        document.frmbit.pampa_referencia_390.value=varreferencia_390.substring(1,varreferencia_390.length);
        document.frmbit.action="movimientos";
        document.frmbit.submit();
    }
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr;
        for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
            x.src = x.oSrc;
    }
    function MM_findObj(n, d)
    {   //v3.0
        var p,i,x;
        if(!d) d=document;
        if((p=n.indexOf("?"))>0&&parent.frames.length)
        {
            d=parent.frames[n.substring(p+1)].document;
            n=n.substring(0,p);
        }
        if(!(x=d[n])&&d.all)
            x=d.all[n];
        for(i=0;!x&&i<d.forms.length;i++)
            x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++)
            x=MM_findObj(n,d.layers[i].document);
        return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments;
        document.MM_sr=new Array;
        for(i=0;i<(a.length-2);i+=3)
            if( (x=MM_findObj(a[i]))!=null )
            {
                document.MM_sr[j++]=x;
                if(!x.oSrc)
                    x.oSrc=x.src;
                x.src=a[i+2];
            }
    }
<%
    if (request.getAttribute("newMenu")!= null)
        out.println(request.getAttribute("newMenu"));
%>
   //-->
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
  <table border="0" CELLPADDING="0" CELLSPACING="0" width="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>

   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!= null )
        out.println(request.getAttribute("Encabezado"));
%>

  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td align="center">
    <input id="cuenta" value="${Cuenta}" type="hidden"  />
<%
    if( request.getAttribute("Output1")!=null )
        out.println(request.getAttribute("Output1"));
%>
     <table width="83" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
      <tr>
<%
    if( request.getAttribute("Output2")!=null )
        out.println(request.getAttribute("Output2"));
%>
      </tr>      
     </table>
     
    </td>
   </tr>
  </table>
 </body>
</html>