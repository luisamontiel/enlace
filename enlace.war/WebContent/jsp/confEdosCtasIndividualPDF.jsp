<%@page import="mx.altec.enlace.bita.BitaConstants"%>
<%@page import="java.util.ArrayList"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>
  <style type="text/css"> 

.sobmbraUnload{
	display:none;
	top:0px;
	left:0px;
	width:0px;
	height:0px;
}
.sombraLoad{
   display: block;
   position: absolute;
   top: 0%;
   left: 0%;
   width: 100%;
   height: 100%;
   background-color:gray;
   z-index:1000;
   opacity:0.9;
   filter:alpha(opacity=70);
}

.windowUnload{
	display:none;
	top:0px;
	left:0px;
	width:0px;
	height:0px;
}

.windowLoad{
	display:block;
	position:absolute;
	background-color:rgb(234,234,234);
	border:rgb(119,119,119) 3px solid;
	top:15%;
	left:25%;
	width:700px;
	height:400px;
	z-index:1001;
   
   /*-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	behavior:url(border-radius.htc);
	*/
    text-align: left;

  }

.encabezado{
    width:700px;
	height:10px;
	background-color:rgb(119,119,119);
	font-family:arial;
	font-weight:bold;
	font-size:9pt;
	color:white;
}

.titulo{
	font-weight:bold;
	font-family:arial;
	font-size:11pt;
}
.tabla{
	width:550px;
   	font-family:arial;
	font-size:8pt;
}
.boton{
	font-size:8pt;
	width:60px;
}
.texto{
	margin:20px 20px 20px 20px;
	text-align:justify;
}
.textbox{
	font-family:arial;
	font-size:9pt;
	text-align:left;
	width:500px;
	height:150px;
}


</style>

  <script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
  <script type="text/javascript" src="/EnlaceMig/ConMov.js"></script>
  <script type="text/javascript">

	function showContrato() {
		if( (document.frmEdoCtaInPdf.suscripPaperless.checked && document.frmEdoCtaInPdf.hdSuscripPaperless.value=='S') || 
		    (!document.frmEdoCtaInPdf.suscripPaperless.checked && document.frmEdoCtaInPdf.hdSuscripPaperless.value!='S') ) {
				cuadroDialogo("Para continuar con la operaci\u00f3n es necesario que modifique el estado de Suscripci\u00f3n " + 
				"a Paperless y de clic en el bot\u00f3n de Aceptar. De lo contrario de clic en el bot\u00f3n Cancelar para " +
				"salir",1);
		} else if(!document.frmEdoCtaInPdf.suscripPaperless.checked && document.frmEdoCtaInPdf.hdSuscripPaperless.value=='S') {
 			document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=cambiaCta', 'flujoOp=PDF');
			document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=ctas', 'flujoOp=PDF');
			document.frmEdoCtaInPdf.hdACAP.value='<%= BitaConstants.EA_ACEPT_CONV_ACEPT_PAPERLESS %>';
			document.frmEdoCtaInPdf.submit();
		} else {
			document.getElementById('sombra').className='sombraLoad';
			document.getElementById('window').className='windowLoad';
       }
		
       }
	function acepto() {
		if (document.getElementById('ac').checked) {
			document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=cambiaCta', 'flujoOp=PDF');
			document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=ctas', 'flujoOp=PDF');
			document.frmEdoCtaInPdf.hdACAP.value='<%= BitaConstants.EA_ACEPT_CONV_ACEPT_PAPERLESS %>';
			cierro();
			document.frmEdoCtaInPdf.submit();
		}
		else {
			cuadroDialogo('Estimado cliente, por favor seleccione la casilla de verificaci\u00f3n. \"Acepto las Condiciones\"',1);
		}
	}
	function cierro() {
		document.getElementById('sombra').className='sombraUnload';
		document.getElementById('window').className='windowUnload';
	}	
	
	    function Cancelar() {
		document.frmEdoCtaInPdf.action = "ConfEdosCtaIndServlet";
		document.frmEdoCtaInPdf.submit();
	}
	
	function DoPost() {
		showContrato();
	}

  	function guardaArchivo() {
		document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=PDF', 'flujoOp=ctas');
		document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=cambiaCta', 'flujoOp=ctas');
		document.frmEdoCtaInPdf.submit();
	}

	function cambiarCta() {
		if(document.frmEdoCtaInPdf.cuentaNva.value == '') {
			cuadroDialogo("Por favor, seleccione una cuenta para proceder con la configuraci\u00f3n del Estado de Cuenta",4);
		}else {
			document.frmEdoCtaInPdf.hdCuentaNva.value = document.frmEdoCtaInPdf.cuentaNva.value;
			document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=PDF', 'flujoOp=cambiaCta');
			document.frmEdoCtaInPdf.action = document.frmEdoCtaInPdf.action.replace('flujoOp=ctas', 'flujoOp=cambiaCta');
			document.frmEdoCtaInPdf.submit();
		}
	}
	
	function mensajeSeq() {
		if (document.frmEdoCtaInPdf.suscripPaperless.checked) {
			cuadroDialogo("Al seleccionar esta opci\u00f3n se deshabilitar\u00e1 el env\u00edo a domicilio de los Estados de Cuenta de las " +
			"Cuentas que conforman su secuencia de domicilio. Para consultar estas cuentas de click en el link del n\u00famero " +
			"de secuencia de la pantalla",1);
		}
	}
	
	function validarFechaConfig() {
		if(document.frmEdoCtaInPdf.hdFechaConfigValida.value=='NO') {
          	<%if("true".equals(request.getAttribute("suscripPaperless").toString())) { %> document.frmEdoCtaInPdf.suscripPaperless.checked=true;
          	<%} else {%> document.frmEdoCtaInPdf.suscripPaperless.checked=false; <%}%>		
			cuadroDialogo("No es posible modificar la opci\u00f3n \"Suscribir a Paperless\" debido a que el d\u00eda de hoy ya se " + 
			"realiz\u00f3 una modificaci\u00f3n (por parte de este u otro contrato) para la misma secuencia de domicilio.", 4);			
			return false;
		}else {
			mensajeSeq();
		}
	}
	
    function MM_preloadImages() {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    
    function MM_swapImgRestore() {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    
    function MM_findObj(n, d) { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    
    function MM_swapImage(){   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }

<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">


<div id="window" class="windowUnload">
<table class="encabezado">
	<tr>
		<td>T&eacute;rminos y condiciones para el servicio paperless</td>
	</tr>
</table>
</br>

<table class="tabla" border="0">
	<tr>
		<td width="20px">&nbsp;</td>
		<td><span class="titulo">Convenio de aceptaci&oacute;n de condiciones para el servicio Paperless</span></td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td>
			<textarea class="textbox" readonly="true">
CONVENIO DE ACEPTACI&Oacute;N DE CONDICIONES PARA EL SERVICIO DE BLOQUEO DE IMPRESI&Oacute;N DE ESTADO DE CUENTA

Estimado Cliente:
				
En cumplimiento a lo establecido por el C&oacute;digo Fiscal de la Federaci&oacute;n los estados de cuenta podr&aacute;n ser enviados por &eacute;ste medio, por lo que al solicitar su inscripci&oacute;n a &eacute;ste Servicio, usted est&aacute; de acuerdo en no recibir el(los) estado(s) de cuenta impreso(s) en papel a trav&eacute;s de correo de los diferentes productos asociados a su c&oacute;digo de cliente.

Una vez inscrito en el servicio, usted recibir&aacute; la informaci&oacute;n relativa a su estado de cuenta en l&iacute;nea, y &uacute;nicamente tendr&aacute; acceso al mismo a trav&eacute;s de medios electr&oacute;nicos, siguiendo las instrucciones proporcionadas por Banco Santander (M&eacute;xico) S.A., Instituci&oacute;n de Banca M&uacute;ltiple, Grupo Financiero Santander M&eacute;xico.

Usted podr&aacute; acceder a su estado de cuenta electr&oacute;nicamente, siempre que no revoque su consentimiento para recibirlo en l&iacute;nea. Si usted desea volver a recibir su Estado de Cuenta en papel a trav&eacute;s de correo, es necesario que regrese a esta p&aacute;gina y siga las instrucciones que se le indican y su impresi&oacute;n ser&aacute; recibida en el domicilio indicado al mes siguiente que usted haya desactivado la opci&oacute;n de bloqueo de impresi&oacute;n.
Si usted lo desea, podr&aacute; solicitar en cualquiera de nuestras sucursales una copia impresa en papel, de cualquier estado de cuenta con una antigŁedad m&aacute;xima no mayor de 24 meses.

En caso que usted requiera cambiar o actualizar la direcci&oacute;n de correo electr&oacute;nico a d&oacute;nde se le enviar&aacute; las notificaciones, realice el cambio a trav&eacute;s de Enlace en la secci&oacute;n: Administraci&oacute;n > Mant. de Datos Personales. En caso que la direcci&oacute;n de correo electr&oacute;nico que usted proporcione sea incorrecta, inv&aacute;lida o si la misma no puede ser registrada y validada por nuestro sistema, usted no recibir&aacute; la notificaci&oacute;n.

Para cualquier duda o aclaraci&oacute;n, puede comunicarse a Superl&iacute;nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000 a trav&eacute;s de nuestro correo electr&oacute;nico enlace@santander.com.mx  donde uno de nuestros especialistas le brindara el apoyo.
			</textarea>
		</td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td><input type="checkbox" name="ac" id="ac" value="ac" />Acepto las condiciones.</td>
	</tr>
		<tr>
		<td width="20px">&nbsp;</td>
		<td align="center">
			<input type="button" class="boton" value="Continuar" onClick="javascript:acepto();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" class="boton" value="Cerrar" onClick="javascript:cierro();"/>
		</td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td>Por favor lea el convenio de aceptaci&oacute;n de condiciones para el servicio de Paperless. Se requiere que Usted
		    acepte las condiciones para que deje de recibir su(s) estado(s) de cuenta de manera 
			impresa en su domicilio. En caso de no estar de acuerdo, podr&aacute; aceptar el cambio posteriormente.
			</td>
	</tr>
</table>

</div>
<div id="sombra" class="sobmbraUnload"></div>

  <table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <form name="frmEdoCtaInPdf" method="post" action="ConfEdosCtaIndServlet?flujoOp=PDF">
  <input type="hidden" value='<%=request.getAttribute("segmentoDom")%>' name="hdSeqDomicilio"/>
  <input type="hidden" value='<%=request.getAttribute("cuenta")%>' name="hdCuenta"/>
  <input type="hidden" value='<%=request.getAttribute("cuentaNva")%>' name="hdCuentaNva"/>
  <input type="hidden" value='<%=request.getAttribute("codCliente")%>' name="hdCodCliente"/>
  <input type="hidden" value='<%=request.getAttribute("domicilio")%>' name="hdDomicilio"/>
  <input type="hidden" value='<%=request.getAttribute("fechaConfigValida")%>' name="hdFechaConfigValida"/>
  <input type="hidden" value="N" name="hdACAP"/>
  <%if(request.getAttribute("suscripPaperless").toString().equals("true")) { %>
	  <input type="hidden" value="S" name="hdSuscripPaperless"/>
  <%} else {%>
      <input type="hidden" value="N" name="hdSuscripPaperless"/>
  <%}%>
  <input type="hidden" value=""/>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td align="center">
     <table width="670" border="0" cellspacing="2" cellpadding="3">
      <tr>
	       <td class="tittabdat" width="123" align="center">Seq</td>
	       <td class="tittabdat" width="123" align="center">C&oacute;digo Cliente</td>
	       <td class="tittabdat" width="123" align="center">Cuenta</td>
	       <td class="tittabdat" width="423" align="center">Domicilio Registrado</td>
      </tr>
      <tr class="textabdatobs">
       		<td align="center" class="tabmovtexbol" align="center">
       			<a href="#"
       				title="Descargue las cuentas relacionadas a la secuencia de domicilio"
       					onclick="javascript:guardaArchivo();">
       				<%=request.getAttribute("segmentoDom")%></a>
       		</td>
          <td align="center" class="tabmovtexbol" align="left">${codCliente}</td>
          <td align="center" class="tabmovtexbol" align="left">${cuenta}</td>
          <td align="left" class="tabmovtexbol" align="left">${domicilio}</td>
      </tr>
      <tr>
          <td colspan="4">
          	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		      	<tr class="textabdatcla">
		          <td align="left" class="tabmovtexbol">
		          	<%if("true".equals(request.getAttribute("suscripPaperless").toString())) { %>
		          		<input type="checkbox" checked="checked" value="S" name="suscripPaperless" onclick="javascript:validarFechaConfig();" /> Suscribir a Paperless
		          	<%} else {%>
		          		<input type="checkbox" value="S" name="suscripPaperless" onclick="javascript:validarFechaConfig();" /> Suscribir a Paperless
		          	<%}%>
		          </td>
		      </tr>
          	</table>
          </td>
      </tr>
  	</table>
	<table>
		<tr>
			<td colspan="4" class="texencconbol">
				<%=request.getAttribute("MENSAJE_PROBLEMA_DATOS") != null ? request.getAttribute("MENSAJE_PROBLEMA_DATOS") : "" %>
			</td>
		</tr>
	</table>
  	<table width="160" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
      <tr>
       <td align="left" height="25"><a href="javascript:DoPost();"><img src="/gifs/EnlaceMig/gbo25280.gif" border="0" alt="Aceptar"/></a></td>
       <td align="left"  height="25"><a href="javascript:Cancelar();"><img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25190.gif"  alt="Cancelar"/></a></td>
      </tr>
     </table>
  	</td>
  	</tr>
  	</table>
  	</form>
  	<script type="text/javascript">
		<% if (request.getAttribute("MensajeErr") != null ) { %>
		<%= request.getAttribute("MensajeErr") %>
		<%	} %>
	</script>
 </body>
</html>