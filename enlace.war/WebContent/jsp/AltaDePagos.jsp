
<%@ page import="mx.altec.enlace.bo.claveTrans"%>
<%@ page import="java.util.*"%>
<%@page import="mx.altec.enlace.bo.Archivos"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">


<HTML>
<jsp:useBean id="arch_combos" class="mx.altec.enlace.dao.CombosConf" scope="request"/>
<HEAD>
<TITLE> Datos del archivo</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript"    src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript"    src="/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript"  SRC="/EnlaceMig/Bitacora.js"></script>
<SCRIPT LANGUAGE="Javascript">

var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>"
var cadenas         = "<%= request.getAttribute("lista_numeros_pagos")%>"
var numero_documento_inicial="";


function js_limpiar(){
	if (document.AltaDePagos.operacion.value=="alta") document.AltaDePagos.reset();
}

function checaLongitud(cajatexto, longitud){
	cadena=cajatexto.value;
	diferencia=longitud-cadena.length;
	if (cadena.length<longitud){
		for(var i=0;i<diferencia;i++)
			cadena =cadena +" ";
	}
	cajatexto.value=cadena;
}

function longitudCampos(){
    checaLongitud(document.AltaDePagos.Proveedor.options[document.AltaDePagos.Proveedor.selectedIndex],20)
	checaLongitud(document.AltaDePagos.NumeroDocumento,8);
	checaLongitud(document.AltaDePagos.Asociado.options[document.AltaDePagos.Asociado.selectedIndex],8)
	checaLongitud(document.AltaDePagos.ImporteDocumento,21);
}

function valida_campos(){
	if (document.AltaDePagos.NumeroDocumento.value=="") {
		cuadroDialogo ("El campo NUMERO DE DOCUMENTO es requerido.",3);
		return false;
		}
	if (document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==0) {
		cuadroDialogo ("El campo TIPO DE DOCUMENTO es requerido.",3);
		return false;
		}
	if (document.AltaDePagos.ImporteDocumento.value=="") {
		cuadroDialogo ("El campo IMPORTE es requerido.",3);
		return false;
		}
	else
		Formatea_Importe(document.AltaDePagos.ImporteDocumento.value);
	if ((document.AltaDePagos.Asociado.options[document.AltaDePagos.Asociado.selectedIndex].value==0)&&((document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==3)||(document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==4))){
		cuadroDialogo ("Es necesario proporcionar factura asociada",3);
		return false;
	}

	if (!validaCaracter(document.AltaDePagos.ImporteDocumento)) return false;
	if (!validaCaracter(document.AltaDePagos.NumeroDocumento)) return false;


	if ((document.AltaDePagos.Naturaleza.options[document.AltaDePagos.Naturaleza.selectedIndex].value==0)&&((document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==3)||(document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==4))){
		cuadroDialogo ("Es necesario proporcionar NATURALEZA",3);
		return false;
	}
	longitudCampos();
	return true;
}

function  js_aceptar(){
   document.AltaDePagos.action="coPagos?opcion=3";
   if (document.AltaDePagos.operacion.value=="alta" ||                                        document.AltaDePagos.operacion.value=="modificacion") {
	var js_total=parseInt(document.AltaDePagos.total_registros.value);
	var js_max=parseInt(document.AltaDePagos.max_registros.value);
	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No es posible realizar la operacion",3);
    		return ;
		}
	if (js_total >= js_max)
		{
		cuadroDialogo ("Solo puede dar de alta "+ document.AltaDePagos.max_registros.value +" registros",1);
		return ;
		}
	var jsNumPago=parseInt(document.AltaDePagos.NumeroDocumento.value);
	var jsProveedor = document.AltaDePagos.Proveedor.options[document.AltaDePagos.Proveedor.selectedIndex].value;
    var jsTipoDocumento = document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value;
	if (document.AltaDePagos.lista_numeros_pagos.value.indexOf(jsTipoDocumento+"," + jsNumPago +","+jsProveedor+";")!=-1 &&	 numero_documento_inicial!=jsNumPago)
		{
			cuadroDialogo ("El numero de documento ya existe",1);
			return ;
		}

    if (!isNaN(jsNumPago))
	   {
		document.AltaDePagos.lista_numeros_pagos.value=document.AltaDePagos.lista_numeros_pagos.value+jsTipoDocumento+"," + jsNumPago +","+jsProveedor+";";
	   }

	if (valida_campos()) document.AltaDePagos.submit();
	}

	if (document.AltaDePagos.operacion.value=="baja"){

		cuadroDialogo("Est&aacute; seguro que desea dar de baja el registro ?",2);

	}

}

function continua(){
	if (respuesta==1) document.AltaDePagos.submit();
}

<%
		if(	session.getAttribute("VarFechaHoy") !=null)
			out.println( session.getAttribute("VarFechaHoy"));
		else
			if (request.getAttribute("VarFechaHoy")!= null)
				out.println(request.getAttribute("VarFechaHoy"));
%>

 diasInhabiles = '<%if(	session.getAttribute("diasInhabiles") !=null)
						out.print( session.getAttribute("diasInhabiles"));
					else
						if (request.getAttribute("diasInhabiles")!= null)
							out.print(request.getAttribute("diasInhabiles"));%>';

function SeleccionaFecha(ind)
 {
   var m=new Date();
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/pagosCalendario3.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
   msg.focus();

 }

function WindowCalendar(ind){
    var m=new Date();
    n=m.getMonth();
    Indice=ind;
    msg=window.open("/EnlaceMig/pagosCalendario2.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	msg.focus();
  }

function Actualiza()
 {
   if(Indice==0)
     document.AltaDePagos.FechaVencimiento.value=Fecha[Indice];
   else
     document.AltaDePagos.FechaEmision.value=Fecha[Indice];
 }


function enfoca(){
 if (document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==1||   document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==2)
	 {
		document.AltaDePagos.Naturaleza.disabled=true;
		document.AltaDePagos.Naturaleza.selectedIndex = 0;
		document.AltaDePagos.Naturaleza.blur();
		document.AltaDePagos.Asociado.disabled=true;
		document.AltaDePagos.Asociado.selectedIndex = 0;
		document.AltaDePagos.Asociado.blur();
	}
 else{
        document.AltaDePagos.Naturaleza.disabled=false;
		document.AltaDePagos.Asociado.disabled=false;
	 }
}

//****Asosiacion de facturas**//
var ArregloNumeros = cadenas.split(';');
var iArrayMax = ArregloNumeros.length
var aDropdown = new Array(iArrayMax)
var bOk = CargaFacturas()

function CargaFacturas(){
	if (iArrayMax > 1){
		for(i=0;i<iArrayMax-1;i++){
			var Elemento1 = ArregloNumeros[i]
			var Elemento=Elemento1.split(",")
			if (Elemento[0]=="1")
			Array[i] = new sElement(Elemento[2],Elemento[1],Elemento[1]);
		}
		return true;
	}
}

function sElement(sParentId,sValue,sDescription)
{
	this.ParentId = sParentId
	this.Id = sValue
	this.Description = sDescription
}

function bCascadeDrop(oDDsource,oDDdest)
{
	var iX
	var sText
	var iY= 0
	var sOptionId
	var sOptionDesc
	var iStartPos
	var	NotaAsociada = "<%=(String)request.getAttribute("Asociado")%>"
    sText = oDDsource.options[oDDsource.selectedIndex].value
	if ((sText != '0')&&(cadenas.indexOf(sText)!=-1))
	{
		oDDdest.options.length = 0
		for (iX=0; iX<=iArrayMax-2; iX++)
		{
			if(sText == Array[iX].ParentId)
			{
			sOptionId = Array[iX].Id
			sOptionDesc= Array[iX].Description
			oDDdest.options[iY] = new Option (sOptionDesc,sOptionId)
			if (NotaAsociada==((String)(oDDdest.options[iY].value)))
				{
				 oDDdest.options[iY].selected=true;
				}
			iY = iY +1
			}
		}
	}

}


//**Fin de la asociacion de Facturas**//

function ValidaNumero(campo,nombreCampo,punto)
{
	tmp = campo.value;
	var numeros="0123456789"
	if (punto) numeros+="."
    for (i = 0; i<tmp.length; i++) {
		car=tmp.charAt(i)
		if (numeros.indexOf(car)==-1) {
			campo.focus();
        	cuadroDialogo("El campo: "+nombreCampo.toUpperCase()+" contiene caracteres no num&eacute;ricos",3);
			return false
	  }
	}
	return true
}

function sustituyeCaracteres(cadena){
var cad1="������"
var cad2="AEIOUN"
var result=""

    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (cad1.indexOf(car)!=-1) car=cad2.charAt(cad1.indexOf(car))
		result+=car
	}
	return result;
}

function validaCaracter(campo){
var caracteresValidos="ABCDEFGHIJKLMNOPQRSTUVWXYZ.,-0123456789 "
cadena=campo.value;
    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (caracteresValidos.indexOf(car)==-1)
		{
			campo.focus();
			cuadroDialogo("El campo: "+campo.name.toUpperCase()+" contiene caracteres no v&aacute;lidos",3);							return false
		}
	}
	return true
}

function validaLongitud(campo,nombreCampo,longitud){
cadena=campo.value;
	if (cadena.length!=longitud) {
								campo.focus();
								cuadroDialogo("El campo: "+nombreCampo.toUpperCase()+" no tiene la longitud requerida, "+longitud,3);
								return false
							  }
	else return true;
}

function ValidaCampo(campo)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase()
	tmp=sustituyeCaracteres(tmp)
	campo.value=tmp
	if (validaCaracter(campo)) {
		switch(campo.name){
			case "ImporteDocumento" : ValidaNumero(campo,"Importe documento",true);
			break;
			case "NumeroDocumento" : ValidaNumero(campo,"Numero de documento",false);
			break;
			}
	}
}

function Formatea_Importe(importe)
{
   decenas=""
   centenas=""
   millon=""
   millares=""
   importe_final=importe
   var posiciones=7;
   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)
   if (posi_importe==-1)
     importe_final= importe + ".00";
   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}

function ValidaNumero(campo,nombreCampo,punto)
{
	tmp = campo.value;
	var numeros="0123456789"
	if (punto) numeros+="."

    for (i = 0; i<tmp.length; i++) {
		car=tmp.charAt(i)
		if (numeros.indexOf(car)==-1) {
										campo.focus();
										cuadroDialogo("El campo: "+nombreCampo.toUpperCase()+" contiene caracteres no num&eacute;ricos",3);
										return false
									  }
	}
	return true
}

function MM_preloadImages() {
    var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
    var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
    var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
    if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
    for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
    var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
    if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

<% if ( request.getAttribute( "newMenu" ) != null ) { out.print( request.getAttribute( "newMenu" ) ); } %>
<%! String Saca(String cadena, int num) {
    int longitud, m,i, posinicial, posfinal;
	 m = i = 0;
	 posinicial = 0;
	      while(m <= num) {
         	if (cadena.charAt(i) == ';')
				m++;
			if (m == (num-1))
				posinicial = i;
			i++;
          }
		  posfinal = i;
     return cadena.substring(posinicial+2, posfinal-1);
   }
%>
<%! String ar = "archivo_actual";
    String def;  %>

	<%! public String corta(String c) {
	 String aux;
	    aux = "";
	  if (c.length()  < 40)
		  return c;
	  else
	   {
         for(int i= 0; i < 40; i++)
		   aux+= String.valueOf(c.charAt(i));
	     return aux;
	   }
    }
%>
<%
		String arch = arch_combos.envia_tux();
		Archivos proveedores = new Archivos();
		claveTrans cvtrans = new claveTrans();
		Vector ls_prov = new Vector();
		Vector cv_trans = new Vector();
		ls_prov = proveedores.mLee(arch, "9");
        cv_trans = cvtrans.mLee(arch, "9");
%>
</SCRIPT>
</HEAD>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="document.AltaDePagos.Naturaleza.blur();document.AltaDePagos.Asociado.blur();bCascadeDrop(document.AltaDePagos.Proveedor,document.AltaDePagos.Asociado);MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
	<% if ( request.getAttribute( "MenuPrincipal" ) != null ) { out.print( request.getAttribute( "MenuPrincipal" ) ); } %>
     </TD>
   </TR>
</TABLE>
<% if ( request.getAttribute( "Encabezado" ) != null ) { out.print( request.getAttribute( "Encabezado" ) ); } %>

<form name = "Frmfechas">
  <%= request.getAttribute("Bitfechas") %>
</form>
<FORM  NAME="AltaDePagos" METHOD="POST" ACTION="AltaDePagos">
<table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
   <td align="center">
   <table width="340" border="0" cellspacing="2" cellpadding="3">
      <tr align="left" valign="middle">
      <td class="tittabdat" colspan="2"> Especifique los datos del pago </td>
      </tr>
	  <tr align="center">
      <td class="textabdatcla" valign="top" colspan="2">
      <table width="350" border="0" cellspacing="0" cellpadding="0">
	     <tr valign="top">
         <td width="270" align="left">
		 <table width="150" border="0" cellspacing="5" cellpadding="0">
		    <tr>
    	    <td class="tabmovtex" nowrap> Proveedor: </td>
		    <td class="tabmovtex" nowrap width="185">
			     <SELECT NAME="Proveedor"  class="tabmovtex" onChange="bCascadeDrop(this, document.AltaDePagos.Asociado)">

		<% int longitud = ls_prov.size();
		    String selected="",variable="",variable2="";
			for (int i=0; i<(longitud); i++)
				{
		if (((String)cv_trans.elementAt(i)).equals((String)request.getAttribute("Proveedor")))
		{
			selected="selected";
		}
	else
		{
			selected="";
		}

		%>
                            <OPTION VALUE="<%=cv_trans.elementAt(i)%>"  <%=selected%> > <%= corta((String)(ls_prov.elementAt(i))) %>
                      <% } %>
			     </SELECT>
			</td>
		    </tr>
         </table>
		  <table width="150" border="0" cellspacing="5" cellpadding="0">
    			   <tr valign="top">
				    <td>
					     <table width="150" border="0" cellspacing="5" cellpadding="0">
						   <tr><td class="tabmovtex" nowrap>Tipo de documento : </td></tr>
						   <tr><td class="tabmovtex" nowrap>
						   <SELECT NAME="TipoDocumento" class="tabmovtex" onChange="enfoca()">
							   	<OPTION VALUE="0"> -------------------------------------
								<OPTION VALUE="1" <% if ( request.getAttribute( "Tipo_1" ) != null ) { out.print( request.getAttribute( "Tipo_1" ) ); } %>>  Factura.
   							    <OPTION VALUE="2" <% if ( request.getAttribute( "Tipo_2" ) != null ) { out.print( request.getAttribute( "Tipo_2" ) ); } %>>  otros.
								<OPTION VALUE="3" <% if ( request.getAttribute( "Tipo_3" ) != null ) { out.print( request.getAttribute( "Tipo_3" ) ); } %>>  Notas de cargo.
								<OPTION VALUE="4" <% if ( request.getAttribute( "Tipo_4" ) != null ) { out.print( request.getAttribute( "Tipo_4" ) ); } %>> Notas de credito.
						    </SELECT>
						   </td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>N&uacute;mero de documento</td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <INPUT TYPE="text" SIZE="17" ALIGN="rigth" class="tabmovtex" NAME="NumeroDocumento" VALUE="<% if ( request.getAttribute( "NumeroDocumento" ) != null ) { out.print( request.getAttribute( "NumeroDocumento" ) ); } %>" onchange="ValidaCampo(this)" maxlength="8">
						   <td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>Asociar a factura n&uacute;mero </td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <select name="Asociado" class="tabmovtex" onChange="enfoca()" >
							    <option value="0"> -------------------------------------
							 </select>
						   <td>
						   </tr>
						 </table>
						 </td>
						 <td align="top">
						  <table width="150" border="0" cellspacing="5" cellpadding="0">
                           <tr>
        		           <td class="tabmovtex" nowrap>
						     Naturaleza :
						   </td>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <SELECT NAME="Naturaleza"  class="tabmovtex" onFocus="enfoca()">
							    <OPTION VALUE="0"> -------------------------------------

								<OPTION VALUE="1" <% if ( request.getAttribute( "Naturaleza_a" ) != null ) { out.print( request.getAttribute( "Naturaleza_a" ) ); } %>>Acredora

   							    <OPTION VALUE="2" <% if ( request.getAttribute( "Naturaleza_a" ) != null ) { out.print( request.getAttribute( "Naturaleza_a" ) ); } %>>Deudora

							 </select>
						   </td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>Importe</td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <INPUT TYPE="text" SIZE="17" ALIGN="rigth" class="tabmovtex"  NAME="ImporteDocumento"
							VALUE="<% if ( request.getAttribute( "ImporteDocumento" ) != null ) { out.print( request.getAttribute( "ImporteDocumento" ) ); } %>"  onchange ="ValidaCampo(this)" maxlength="21">
						   </td>
						   </tr>
						   </table>
						 </td>
					   </tr>
					  </table>
					   <table width="150" border="0" cellspacing="5" cellpadding="0">
			           </table>
				    <table width="150" border="0" cellspacing="5" cellpadding="0">
    				   <tr valign="top">
					     <td>
					     <table width="150" border="0" cellspacing="5" cellpadding="0">
						   <tr><td class="tabmovtex" nowrap>Fecha de emisi&oacute;n: </td></tr>
						   <tr align="left">
                             <td class="tabmovtex" nowrap>
                               <input type=text name=FechaEmision value="<%
									if(	request.getAttribute("FechaEmision") !=null)
										out.println( request.getAttribute("FechaEmision"));
									else
										if (request.getAttribute("Fechas")!= null)
											out.println(request.getAttribute("Fechas"));
								%>" size=15 onFocus='blur();'  class="tabmovtex"  maxlength=10>
								<a href ="javascript:WindowCalendar(1);" align="absmiddle"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a>
                              </td>
							  </tr>
						   </table>
						 </td>
						 <td align="top">
						  <table width="150" border="0" cellspacing="5" cellpadding="0">
                           <tr>
        		           <td class="tabmovtex" nowrap>
						     Fecha de vencimiento:
						   </td>
						   <tr align="left">
                              <td class="tabmovtex" nowrap>
                               <input type=text name=FechaVencimiento value="<%
									if(	request.getAttribute("FechaVencimiento") !=null)
									{
										out.println( request.getAttribute("FechaVencimiento"));
	         						}
									else
										if (request.getAttribute("Fechas")!= null)
										{
											out.println(request.getAttribute("Fechas"));
										}
								%>" size="15" onFocus='blur();' class="tabmovtex"  maxlength=10>
								<A href="javascript:SeleccionaFecha(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a>
                              </td>
							  </tr>
						   </table>
						 </td>
					   </tr>
					  </table>
		 </td>
		 </tr>
	  </table>
	  </td>
	  </tr>
	</table>
    </td>
	</tr>
</table>
<br>
<table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
   <td align="center">
<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
                 <tr>
                   <td align="center" valign="middle" width="66">
                     <A HREF="javascript:js_aceptar();"><% if ( request.getAttribute( "botonOperacion" ) != null ) { out.print( request.getAttribute( "botonOperacion" ) ); } %></a>
                   </td>
                   <td align="left" valign="top" width="76">
                     <A HREF="javascript:js_limpiar();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a>
                   </td>
                   <td align="left" valign="top" width="83">
                     <A HREF="javascript:document.AltaDePagos.action='coPagos?opcion=1';document.AltaDePagos.submit();"><img border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
                  </td>
                </tr>
          </table>
		 </td>
		 </tr>
</table>

 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "posicionRegistro" ) != null ) { out.print( request.getAttribute( "posicionRegistro" ) ); } %>" NAME="registro">
 <INPUT TYPE="hidden" VALUE="lectura" NAME="strImporta">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "tipo_archivo" ) != null ) { out.print( request.getAttribute( "tipo_archivo" ) ); } %>" NAME="tipo_archivo">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "total_registros" ) != null ) { out.print( request.getAttribute( "total_registros" ) ); } %>" NAME="total_registros">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "max_registros" ) != null ) { out.print( request.getAttribute( "max_registros" ) ); } %>" NAME="max_registros">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "operacion" ) != null ) { out.print( request.getAttribute( "operacion" ) ); } %>" NAME="operacion">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "lista_numeros_pagos" ) != null ) { out.print( request.getAttribute( "lista_numeros_pagos" ) ); } %>" NAME="lista_numeros_pagos">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "lista_numeros_claves" ) != null ) { out.print( request.getAttribute( "lista_numeros_claves" ) ); } %>" NAME="lista_numeros_claves">
 <INPUT TYPE="hidden" VALUE="" NAME="Actual">
</form>
<script language="JavaScript1.2">
numero_documento_inicial=document.AltaDePagos.NumeroDocumento.value;
</script>
</BODY>
</html>


