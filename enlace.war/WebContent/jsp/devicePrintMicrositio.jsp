<%
String convenio = "";
String importe = "";
String servicio_id = "";
String origenLogin = "";

String referencia = "";
String url = "";
String concepto = "";

String lineaCaptura = "";

	convenio  = (String)request.getAttribute("convenio");
	importe  = (String)request.getAttribute("importe");
	servicio_id = (String)request.getAttribute("servicio_id");
	origenLogin = (String)request.getAttribute("origenLogin");

	if(convenio==null)
		convenio="";

	if(servicio_id==null)
		servicio_id="";

	if(origenLogin==null)
		origenLogin="";

	if(importe==null)
		importe="";

	if (!"PMRF".equals(servicio_id))
	{
		referencia  = (String)request.getAttribute("referencia");
		url = (String)request.getAttribute("url");
		concepto = (String)request.getAttribute("concepto");
		if(url==null)
			url="";

		if(concepto==null)
			concepto="";

		if(referencia==null)
			referencia="";
	} else {
		lineaCaptura = (String)request.getAttribute("linea_captura");
		System.out.println("Devise-----> Linea de captura " +lineaCaptura);
	}

 %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>Micrositio Enlace</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<script type="text/javascript" src="/EnlaceMig/pm_fp.js"></script>
<script type="text/javascript">
function enviar(){
	var devicePrint=add_deviceprint();
	document.getElementById("devicePrinter").value = devicePrint;
	document.form1.submit();
}
</script>
</head>
<body onLoad = "enviar();" >
<form id="form1" name="form1" method="POST" action="LoginServletMicrositio" accept-charset="UTF-8">
	<input type="hidden" value="<%=convenio%>" id="convenio" name="convenio"/>
	<input type="hidden" value="<%=importe%>" id="importe" name="importe"/>
	<input type="hidden" value="<%=servicio_id%>" id="servicio_id" name="servicio_id"/>
	<input type="hidden" value="<%=origenLogin%>" id="origenLogin" name="origenLogin"/>
	<input type="hidden"  id="devicePrinter" name="devicePrinter"/>
	<input type="hidden"  id="opcion" name="opcion" value="0"/>
	<%if(!"PMRF".equals(servicio_id)){ %>
		<input type="hidden" value="<%=referencia%>" id="referencia" name="referencia"/>
		<input type="hidden" value="<%=url%>" id="url" name="url"/>
		<input type="hidden" value="<%=concepto%>" id="concepto" name="concepto"/>
	<%} else { %>
		<input type="hidden" value="<%=lineaCaptura%>" id="linea_captura" name="linea_captura"/>
	<%} %>
</form>
</body>
</html>