<%@ page import="mx.altec.enlace.admonContrasena.vo.UtilVO" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Bienvenido a Enlace Internet</title>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<meta content="s26050" name="Codigo de Pantalla">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<link href="/EnlaceMig/consultas.css" type="text/css"
	rel="stylesheet">
<meta content="MSHTML 6.00.2900.2180" name="GENERATOR">
<%
String codCliente = (String)request.getAttribute("codCliente");
String status = (String)request.getAttribute("status");
String nomCliente = (String)request.getAttribute("nomCliente");
String descEstado = (String)request.getAttribute("descEstado");	//Estado de la solicitud
String motRechazo = (String)request.getAttribute("motRechazo");	//Motivos de rechazo de la solicitud

String descStatus = "";		//Estado de la contraseņa


if (status.equals("N")){
	descStatus = "NO GENERADA";
	}else{
	if (status.equals("BT") || status.equals("BP")){
		descStatus = "BLOQUEADA";
	}else{
		if (status.equals("E")){
	descStatus = "EXPIRADA";
		}else{
	if (descEstado.equals("GENERADA")){
		descStatus = "GENERADA";
	}else{
		if (status.equals("I") && descEstado.equals("AUTORIZADA")){
			descStatus = "INACTIVA";
		}else{
			if (status.equals("A") && (descEstado.equals("AUTORIZADA")  || descEstado.equals("SIN SOLICITUD"))){
		descStatus = "ACTIVA";
			}else{
		descStatus = "EN TRAMITE";
			}
		}
	}
		}
	}
}
%>
<%
java.util.Date dt = new java.util.Date();
String mes;
switch (dt.getMonth()+1 )
	{
	case  1: mes = "Enero";		 break;
	case  2: mes = "Febrero";	 break;
	case  3: mes = "Marzo";		 break;
	case  4: mes = "Abril";		 break;
	case  5: mes = "Mayo";       break;
	case  6: mes = "Junio";		 break;
	case  7: mes = "Julio";		 break;
	case  8: mes = "Agosto";	 break;
	case  9: mes = "Septiembre"; break;
	case 10: mes = "Octubre";	 break;
	case 11: mes = "Noviembre";	 break;
	case 12: mes = "Diciembre";  break;
	default: mes = "Enero";
	}

String strDia = "";
switch (dt.getDay())
	{
	case  0: strDia = "Domingo";   break;
	case  1: strDia = "Lunes";     break;
	case  2: strDia = "Martes";    break;
	case  3: strDia = "Miercoles"; break;
	case  4: strDia = "Jueves";    break;
	case  5: strDia = "Viernes";   break;
	case  6: strDia = "Sabado";    break;
}

int anio = dt.getYear()+1900;
String mifecha = strDia +" " + dt.getDate() + " de " +  mes + " del "+anio;

String minutos_ = ""+dt.getMinutes();
if(minutos_.trim().length()==1)
	minutos_ = "0"+minutos_;
String mihora = dt.getHours()+":"+minutos_;
%>

<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" language="JavaScript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
</script>
<script>
<%= request.getAttribute("MensajeLogin01") %>
</script>
</head>
<body bgColor="#ffffff" leftMargin="0"
	background="/gifs/EnlaceMig/gfo25010.gif" topMargin="0"
	onload="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gba25020.gif');"
	marginwidth="0" marginheight="0">
<form name="consultaContresena" action="" method="post">
<table cellSpacing="0" cellPadding="0" width="600" border="0">
	<tbody>
		<tr>
			<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
					<table class="tabfonbla" cellSpacing="0" cellPadding="0"
						width="596" border="0">
						<tbody>
							<tr vAlign="top">
								<td rowSpan="2"><img height="41"
									src="/gifs/EnlaceMig/glo25010.gif" width="237" alt=""></td>
								<td rowSpan="2"><a
									onmouseover="MM_swapImage('contactanos','','/gifs/EnlaceMig/gbo25111.gif',1)"
									onmouseout="MM_swapImgRestore()" href="javascript:;"><img
									height="33" src="/gifs/EnlaceMig/gbo25110.gif" width="30"
									border="0" name="contactanos" alt=""></a></td>
								<td rowSpan="2"><img height="20" alt="Cont&aacute;ctenos"
									src="/gifs/EnlaceMig/gbo25120.gif" width="59"></td>
								<td rowSpan="2"><a
									onmouseover="MM_swapImage('atencion','','/gifs/EnlaceMig/gbo25131.gif',1)"
									onmouseout="MM_swapImgRestore()" href="javascript:;"><img
									height="33" src="/gifs/EnlaceMig/gbo25130.gif" width="36"
									border="0" name="atencion" alt=""></a></td>
								<td rowSpan="2"><img height="20" alt="Atenci&oacute;n telef&oacute;nica"
									src="/gifs/EnlaceMig/gbo25140.gif" width="90"></td>
								<td rowSpan="2"><a
									onmouseover="MM_swapImage('centro','','/gifs/EnlaceMig/gbo25151.gif',1)"
									onmouseout="MM_swapImgRestore()" href="javascript:;"><img
									height="33" src="/gifs/EnlaceMig/gbo25150.gif" width="33"
									border="0" name="centro" alt=""></a></td>
								<td rowSpan="2"><img height="20" alt="Centro de mensajes"
									src="/gifs/EnlaceMig/gbo25160.gif" width="93"></td>
								<td width="18" bgColor="#de0000"><img height="17"
									src="/gifs/EnlaceMig/gau25010.gif" width="18" alt=""></td>
							</tr>
							<tr vAlign="top">
								<td><img height="24" src="/gifs/EnlaceMig/gau25010.gif"
									width="18" alt=""></td>
							</tr>
						</tbody>
					</table>
					</td>
					<td>
					<table cellSpacing="0" cellPadding="0" width="100%"
						bgColor="#ffffff" border="0">
						<tbody>
							<tr>
								<td bgColor="#de0000"><img height="17"
									src="/gifs/EnlaceMig/gau25010.gif" width="35" alt=""></td>
								<td align="middle" rowSpan="2"><img height="41" alt=""
									src="/gifs/EnlaceMig/glo25020.gif" width="57"></td>
								<td width="100%" bgColor="#de0000"><img height="17"
									src="/gifs/EnlaceMig/gau25010.gif" width="5" alt=""></td>
							</tr>
							<tr>
								<td><img height="24" src="/gifs/EnlaceMig/gau25010.gif"
									width="35" alt=""></td>
								<td><img height="24" src="/gifs/EnlaceMig/gau25010.gif"
									width="5" alt=""></td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td vAlign="top" width="100%">

			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tbody>
					<tr>
						<td class="titpag" vAlign="bottom">Administrador de
						Contrase&ntilde;as</td>
						<td class="tabfonbla" align="right"><img height="33"
							alt="Sitio seguro" src="/gifs/EnlaceMig/gba25010.gif"
							width="126"></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td valign="top"
				style="background-image: url(/gifs/EnlaceMig/gfo25020.gif); background-repeat: repeat-x;">&nbsp;</td>
		</tr>
		<tr>
			<td vAlign="top" width="100%">

			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tbody>

					<tr>
						<td class="texencrut">&nbsp;<img src="/gifs/EnlaceMig/gic25030.gif"
							alt=""> Administrador de Contrase&ntilde;as &gt; Consultar Estatus de Contrase&ntilde;a.</td>
						<td align="right">
						<table>
							<tr>
								<td class="texencfec" vAlign="top" noWrap align="right"
									height="12"><%=mifecha%><br>
								<%=mihora%></td>
								<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
								<td align="right"><a href="/Enlace/enlaceMig/logout?origen=1" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)">
									<img src="/gifs/EnlaceMig/gbo25180.gif" width="44" height="49" name="finsesion" border="0"></a></td>
							</tr>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td align="center"><!--
Lista de Opciones Principales
-->



			<TABLE>
			<TR>
			<TD>


			<table cellSpacing="0" cellPadding="0" width="400" border="0"
				align="center">
				<tbody>

					<tr>
						<td>
						<table class="textabdatcla" cellSpacing="0" cellPadding="0"
							width="100%" border="0">
							<tbody>
								<tr>
									<td class="tittabdat" nowrap="nowrap" style="height: 20px;">&nbsp;Estatus de la Contrase&ntilde;a</td>
								</tr>
								<tr>
									<td>
										<table cellSpacing="10" cellPadding="0" width="100%" border="0">
											<tbody>
												<tr>
													<td height="30px" class="tabmovtex11" width="30%">C&oacute;digo de Cliente:</td>
													<td height="30px" class="tabmovtex11" width="70%"><%=UtilVO.convierteUsr7a8(codCliente)%></td>
												</tr>
												<tr>
													<td height="30px" class="tabmovtex11" width="30%">Nombre:</td>
													<td height="30px" class="tabmovtex11" width="70%"><%=nomCliente%></td>
												</tr>
												<tr>
													<td height="30px" class="tabmovtex11" width="30%">Estatus Contrase&ntilde;a:</td>
													<td height="30px" class="tabmovtex11" width="70%"><%=descStatus%></td>
												</tr>
											</tbody>
											</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td background="/gifs/EnlaceMig/gfo25020.gif" height="12"><img
							height="12" src="IndexServlet_files\gau25010(1).gif" width="5"
							alt=""></td>
					</tr>

				</tbody>
			</table>


			</TD>
			<TD>
			<table cellSpacing="0" cellPadding="0" width="400" border="0"
				align="center">
				<tbody>
					<tr>
						<td class="tittabdat" nowrap="nowrap" style="height: 20px;">&nbsp;Estatus de la Solicitud</td>
					</tr>
					<tr>
						<td>
						<table class="textabdatcla" cellSpacing="0" cellPadding="0"
							width="100%" border="0">
							<tbody>

								<tr>
									<td>

										<table cellSpacing="10" cellPadding="0" width="100%" border="0">
										<tbody>

											<tr>
												<td height="30px" class="tabmovtex11" width="30%">Estatus Solicitud:</td>
												<td height="30px" class="tabmovtex11" width="70%"><%=descEstado%></td>
											</tr>
											<%if (descEstado.equals("RECHAZADA")){%>
											<tr>
												<td valign="top" height="30px" width="30%" class="tabmovtex11">Motivos de Rechazo:</td>
												<%if (motRechazo!=null && !motRechazo.equals("")){%>
													<td valign="top" rowspan="2" height="30px" class="tabmovtex11" width="70%"><%=motRechazo%></td>
												<%}else{%>
													<td rowspan="2" class="tabmovtex11" width="70%>No se capturaron los motivos de rechazo</td>
												<%}%>
											</tr>
											<tr>
												<td height="30px">&nbsp;</td>
											</tr>


											<%}else{%>

											<tr>
												<td height="30px">&nbsp;</td>
											</tr>
											<tr>
												<td height="30px">&nbsp;</td>
											</tr>
											<%}%>


										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td background="/gifs/EnlaceMig/gfo25020.gif" height="12"><img
							height="12" src="IndexServlet_files\gau25010(1).gif" width="5"
							alt=""></td>
					</tr>
				</tbody>
			</table>

			</TD>
			</TR>
			</TABLE>





			</td>
		</tr>
		<tr>
			<td align="center">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
				<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
						<td><a href="javascript:document.location='/Enlace/enlaceMig/jsp/generarContrasenaFrm.jsp'">
							<img src="/gifs/EnlaceMig/generar.gif" alt="Generar" border="0" /></a></td>
						<%if(descStatus.equals("INACTIVA")){%>
						<td><a href="javascript:document.location='/Enlace/enlaceMig/LoginServlet?opcion=2&accion=11&pantalla=activ'">
							<img src="/gifs/EnlaceMig/activar.gif" alt="Activar" border="0" /></a></td>
						<%}else{%>
							<td><img src="/gifs/EnlaceMig/activardes.gif" alt="Activar" border="0" /></td>
						<%}
						if (status.equals("BT")){%>
						<td><a href="javascript:document.location='/Enlace/enlaceMig/jsp/Desbloq14.jsp'">
							<img src="/gifs/EnlaceMig/desbloquear.gif" alt="Desbloquear" border="0" /></a></td>
						<%}else{%>
							<td><img src="/gifs/EnlaceMig/desbloqueardes.gif" alt="Desbloquear" border="0" /></td>
						<%}
						if (descEstado.equals("GENERADA")){%>
						<td><a href="javascript:document.location='/Enlace/enlaceMig/LoginServlet?opcion=2&accion=11&pantalla=reimp'">
							<img src="/gifs/EnlaceMig/reimprimir.gif" alt="Reimprimir solicitud" border="0" /></a></td>
						<%}else{%>
							<td><img src="/gifs/EnlaceMig/reimprimirdes.gif" alt="Reimprimir solicitud" border="0" /></td>
						<%}%>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</form>
</body>
</html>