<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>

<%-- 10/10/2002 Despliega mensaje de error si no obtiene informaci�n de tasas.
     14/10/2002 Cambio en la obtenci�n de claveBanco.
     22/10/2002 Ajuste botones.
     29/10/2002 Cambio par�metros por atributos.
     03/12/2002 Se presentan s�lo los datos de plazos y tasas, omiti�ndose referencias y puntos.
--%>

<%! String datoTasas; %>
<%! String lineaCredito; %>
<%! String banco; %>
<%! String imagen; %>
<%! String altImagen, fecha, hora; %>

<html>
<head>
<Meta name="versionServlet" content="2.3.0">
<title>Plazos.</title>

<!-- Autor: Hugo Ruiz Zepeda -->

  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<% banco=(String)application.getAttribute("claveBancoCE");

/* Se sustituye la discriminacion del banco con el proyecto unico 1-dic-04

   if(banco!=null)
   {
      if(banco.equalsIgnoreCase("003"))
      {
         imagen="/gifs/EnlaceMig/glo25040s.gif";
         altImagen="Serf&iacute;n";
      }

      if(banco.equalsIgnoreCase("014"))
      {
         imagen="/gifs/EnlaceMig/glo25040.gif";
         altImagen="Banco Santander Mexicano";
      }
   }
*/
 imagen="/gifs/EnlaceMig/glo25040b.gif";
 altImagen="Banco Santander, S. A.";



   fecha=(String)application.getAttribute("fechaCE");

   if(fecha==null || fecha.length()<1)
      fecha="<BR>";

   hora=(String)application.getAttribute("horaCE");

   if(hora==null || hora.length()<1)
      hora="<BR>";
%>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">

<%
  out.print("<!--");
  out.print('\u0048');
  out.print('\u0052');
  out.print('\u005A');
  out.println("-->");
%>

<%
   //datoTasas=request.getParameter("datosTasas");
   datoTasas=(String)application.getAttribute("datosTasasCE");
   lineaCredito=(String)application.getAttribute("lineaCreditoCE");


   if(lineaCredito==null)
    lineaCredito="";
%>


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="400">

  <tr valign="top">
    <td width="*">
          <Img src="/gifs/EnlaceMig/glo25010.gif" border="0" alt="Banco Santander">
       </TD>
  </TR>
  <TR>
   <TD width="40">
      <BR>
   </TD>
   <TD width="185" nowrap>
      <!--
      <H2>Tasas Plazo</H2>
      -->
      <BR>
   </TD>
   <TD align="right" width="175">

         <% if(imagen!=null)
            {
         %>
            <Img src="<%=imagen%>" border="0" alt="<%=altImagen%>">
         <% }
            else
            {
         %>
            <BR>
         <% } %>
      <!--<img src="/gifs/EnlaceMig/glo25040.gif" border="0" alt="Banco Santander">-->
   </TD>
  </TR>
</TABLE>



<TABLE  width="500" border="0" cellspacing="0" cellpadding="0">
 <TR>
  <TD width="50">
    <BR>
  </TD>
  <TD>


  <table width="440" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	<tr>
		<td align="center" class="tittabdat" width="215" nowrap>
			L&iacute;nea de cr&eacute;dito
		</td>
      <TD align="right" width="95">
         <B>Fecha:&nbsp;</B>
      </TD>
      <TD width="130" class="tabmovtex1">
         <%=fecha %>
      </TD>
	</tr>
  <TR>
    <td align="center" width="215" class="textabdatcla" bgcolor="#CCCCCC" nowrap>
        &nbsp;<%=lineaCredito %>
	 </td>
    <TD align="right" width="95">
         <B>Hora:&nbsp;</B>
    </TD>
    <TD width="130" class="tabmovtex1">
      <%=hora %>
    </TD>
	</TR>
  </table>



	<BR>



	<table width="225" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	   <tr>
                    <td align="center" valign="top" class="texenccon">
                		<span class="texencconbol">Tasas vigentes de mercado</span>
            	     </td>
      </tr>
	</Table>


<%
if(datoTasas!=null)
{
%>

<Table   width="225" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
<TR>
<TD  align="justify" class="tittabdat" width="150">Plazo de la<BR>disposici&oacute;n</TD>
<!--
<TD  align="justify" class="tittabdat" width="150">Referencia</TD>
<TD  align="justify" class="tittabdat" width="75">Puntos<BR>adicionales</TD>
-->
<TD  align="justify" class="tittabdat" width="75">Tasa de operaci&oacute;n</TD>
</TR>



<%=datoTasas %>

</TABLE>

<%
}
else
{
%>
   <BR>
   <Table width="300" class="tabfonbla">
      <TR>
         <td align="center" class="tabtexcal2">
            <B>No se pudo obtener datos de plazos y tasas.</B>
         </TD>
      </TR>
   </Table>
<%
}
%>




  </TD>
 </TR>



 <TR>
  <TD align="left" colspan="2">
    <BR><BR>


    <Table width="160" border="0" cellspacing="0" cellpadding="0" align="center">
     <TR>
      <!--
      <TD width="15">
        &nbsp;
      </TD>
      -->
      <TD width="75" align="right">
       <A href="" onClick="self.close()" border="0">
          <Img src="/gifs/EnlaceMig/gbo25200.gif" hspace="0" width="75" border="0" alt="Cerrar">
       </A>
      </TD>
      <TD align="left" width="85">
       <A href="" onClick="window.print();return false" border="0">
        <Img src="/gifs/EnlaceMig/gbo25240.gif" hspace="0" width="85" border="0" alt="Imprimir">
       </A>
      </TD>
     </TR>
    </TABLE>



  </TD>
 </TR>
</Table>




</body>
</html>