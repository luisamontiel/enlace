<html>
<head>
  <title>Consultas Transferencias Internacionales</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript">

  var Indice=0;

  var registrosTabla = new Array();
  var camposTabla = new Array();
  var totalCampos = 0;
  var totalRegistros = 0;

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/

<% if(request.getAttribute("VarFechaHoy")!= null)
    out.print(request.getAttribute("VarFechaHoy"));
%>
   diasInhabiles = '<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

function SeleccionaFecha(ind)
 {
   var m=new Date();
   n=m.getMonth();
   Indice=ind;

   if(verificaRadios())
    {
	  msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	  msg.focus();
	}
 }

function Actualiza()
 {
   if(Indice==0)
     document.buscar.FechaIni.value=Fecha[Indice];
   else
     document.buscar.FechaFin.value=Fecha[Indice];
 }

function Enviar()
 {
   
   if(VerificaFechas()){
     document.buscar.submit();
     document.getElementById("enviar").style.visibility="hidden";
     document.getElementById("enviar2").style.visibility="visible";
   }
 }

function verificaRadios()
 {
   if(document.buscar.elements[1].checked==true)
     return true;
   cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del d&iacute;a",3);
   return false;
 }

function cambiaFechas(ele)
 {
   if(ele.value=='HIS')
    {
      document.buscar.FechaIni.value="<%= request.getAttribute("FechaPrimero") %>";
      document.buscar.FechaFin.value="<%= request.getAttribute("FechaAyer") %>";
    }
   else
   {
     document.buscar.FechaIni.value="<%= request.getAttribute("FechaDia") %>";
     document.buscar.FechaFin.value="<%= request.getAttribute("FechaDia") %>";
   }
 }

function VerificaFechas()
 {
    for(i1=0;i1<document.buscar.length;i1++)
     if(document.buscar.elements[i1].type=='radio')
      if(document.buscar.elements[i1].checked==true)
        TipoB=document.buscar.elements[i1].value;

  if(TipoB=='HIS')
   {
	  
	  var fechaIni = document.buscar.FechaIni.value;
	   	var fechaFin = document.buscar.FechaFin.value;
	   
	  	dia1[0] = fechaIni.substring(0,2);
	   	mes1[0] = fechaIni.substring(3,5);
	   	anio1[0] = fechaIni.substring(6,10);
	   
	   	dia1[1] = fechaFin.substring(0,2);
	   	mes1[1] = fechaFin.substring(3,5);
	   	anio1[1] = fechaFin.substring(6,10);
	  
	  	if(anio1[0]==anio1[1]){
     		if(mes1[0]>mes1[1])
      		{
        		/*alert("Error: La fecha Final no debe ser menor a la inicial.");*/
        		cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
        		return false;
      		}
     		if(mes1[0]==mes1[1])
      			if(dia1[0]>dia1[1])
       			{
         			/*alert("Error: La fecha Inicial debe ser menor a la final.");*/
         			cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.",3);
         			return false;
       			}
   		}else if(anio1[0]>anio1[1]){
   		 cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
         return false;
	 }
   }
  return true;
 }

function BotonPagina(sigant)
 {
   var tot=0;
   tot=document.tabla.Pagina.value;

   if(sigant==0)
    tot--;
   if(sigant==1)
    tot++;
   if(sigant==2)
    tot=0;
   if(sigant>=3)
    {
      /*tot=(document.tabla.NumPag.selectedIndex);*/
      tot=sigant-4;
    }

   document.tabla.Pagina.value=tot;
   document.tabla.submit();
 }

/********************************************************************************/
/* Funciones para deplegar la ventana con la informacion de la cuenta*/

var registrosTabla=new Array();
var camposTabla=new Array();
var totalCampos=0;
var totalRegistros=0;

/* Comprobante para la consulta de operaciones de cambios ************/
function GenerarComprobante(datos,indice)
{

  var trama=datos;
  var cta_origen=trama.substring(0,trama.indexOf('|'));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var cta_destino=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var importemn=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var importeusd=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var tipocambio=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var fecha=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var concepto=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var orden=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var referencia=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var rfc=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);
  var iva=trama.substring(0,trama.indexOf("|"));
  trama=trama.substring(trama.indexOf('|')+1,trama.length);

  var estatus=trama;
  var cta_origen1=cta_origen;
  var cta_destino1=cta_destino;
  var fecha1=fecha;
  var concepto1=concepto;

  var usuario=document.tabla.NumUsuario.value+" "+document.tabla.NomUsuario.value;
  var contrato=document.tabla.NumContrato.value+" "+document.tabla.NomContrato.value;
  cta_origen=formatea(cta_origen1);
  cta_destino=formatea(cta_destino1);
  fecha=formatea(fecha1);
  concepto=formatea(concepto1);
  usuario=formatea(usuario);
  contrato=formatea(contrato);
  rfc=formatea(rfc);
  iva=formatea(iva);



  var web_application=document.tabla.web_application.value;
  var banco=document.tabla.ClaveBanco.value;
	// vswf:meg cambio de NASApp por Enlace 08122008
  operacion="/Enlace/"+web_application+"/comprobante_cambios?cta_origen="+cta_origen;
  operacion+="&cta_destino="+cta_destino+"&importemn="+importemn+"&importeusd="+importeusd+"&tipocambio=";
operacion+=tipocambio+"&fecha="+fecha+"&concepto="+concepto+"&orden="+orden+"&referencia="+referencia+"&usuario="+usuario+"&contrato="+contrato+"&rfc="+rfc+"&iva="+iva+"&banco="+banco+"&estatus="+estatus;
operacion=formatea(operacion);

  ventanaInfo2=window.open(operacion,'Comprobante','width=480,height=500,toolbar=no,scrollbars=yes');
  ventanaInfo2.focus();
}

/*********************************************************************/

/********************************************************************************/
/* Funciones para deplegar la ventana con la informacion de la cuenta*/

function valPar(campo)
{
  var strEspeciales="|@/-., ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz0123456789";
  var car="";
  var a=0;

  if(campo.indexOf("&")>0)
     campo=campo.replace("&","AND");

  for(a=0;a<campo.length;a++)
   {
     car=campo.charAt(a);
     if(strEspeciales.indexOf(car)<0)
	    campo=campo.replace(car," ");
   }
  return campo;
}

function despliegaDatos(campo2)
 {
   /* cadena de Transferencias,indice del registro*/
   param1="&TransEnv="+valPar(campo2);
   param2="Indice=0";
   param4="&RefMec=CON";
   param5="&NumContrato="+valPar(document.tabla.NumContrato.value)+"&NomContrato"+valPar(document.tabla.NomContrato.value);
   param6="&NumUsuario="+valPar(document.tabla.NumUsuario.value)+"&NomUsuario="+valPar(document.tabla.NomUsuario.value);
   param7="&ClaveBanco="+valPar(document.tabla.ClaveBanco.value);
   param1=formatea(param1);
   param5=formatea(param5);
   param6=formatea(param6);
   param7=formatea(param7);

   web_application=document.tabla.web_application.value;
	// vswf:meg cambio de NASApp por Enlace 08122008
   vc=window.open("/Enlace/"+web_application+"/MTI_Comprobante?"+param2+param1+param4+param5+param6+param7,'trainerWindow','width=480,height=500,toolbar=no,scrollbars=yes');
   vc.focus();
 }

function formatea(para)
 {
   var formateado="";
   var car="";

   for(a2=0;a2<para.length;a2++)
    {
      if(para.charAt(a2)==' ')
       car="+";
      else
       car=para.charAt(a2);

      formateado+=car;
    }
   return formateado;
 }

function numeroRegistros(campo2)
    {
      var i=0;
	  totalRegistros=0;

      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
    }

function llenaTabla(campo2)
 {
    var strCuentas="";
    var vartmp="";
	var i=0;

	strCuentas=campo2;
    if(totalRegistros>=1)
      {
        for(i=0;i<totalRegistros;i++)
         {
           vartmp=strCuentas.substring(0,strCuentas.indexOf('@'));
           registrosTabla[i]=vartmp;
           if(strCuentas.indexOf('@')>0)
             strCuentas=strCuentas.substring(strCuentas.indexOf('@')+1,strCuentas.length);
         }
      }
    separaCampos(registrosTabla);
 }

function separaCampos(arrCuentas)
 {
     var i=0;
     var j=0;
     var vartmp="";
     var regtmp="";

     for(i=0;i<=totalRegistros;i++)
       camposTabla[i]=new Array();

	 totalCampos=0;
     if(totalRegistros>=1)
      {
        regtmp+=arrCuentas[0];
        for(i=0;i<regtmp.length;i++)
         {
           if(regtmp.charAt(i)=='|')
            totalCampos++;
         }
        for(i=0;i<totalRegistros;i++)
         {
           regtmp=arrCuentas[i];
           for(j=0;j<totalCampos;j++)
            {
              vartmp=regtmp.substring(0,regtmp.indexOf('|'));
              camposTabla[i][j]=vartmp;
              if(regtmp.indexOf('|')>0)
                regtmp=regtmp.substring(regtmp.indexOf('|')+1,regtmp.length);
            }
           camposTabla[i][j]=regtmp;
         }
      }
 }

function nueva(indice)
 {
   var TramaGlobal="";
   var i=0;

   for(i=0;i<=totalCampos;i++)
     TramaGlobal+=camposTabla[indice][i]+"|";
   TramaGlobal+=" @";
   document.tabla.TransEnv.value=TramaGlobal;
 }

<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
   <!-- MENU PRINCIPAL -->
   <%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<FORM  NAME="buscar" method=get action="MTI_Consultar?Modulo=1" onSubmit="return VerificaFechas();">
  <td align=center><% if(request.getAttribute("Tabla")!= null) out.print(request.getAttribute("Tabla")); %></td>
  <input type=hidden name=Modulo value=<%=request.getAttribute("Modulo") %>>
  <input type=hidden name=Pagina value=0>
</FORM>

<FORM  NAME="tabla" method=get action="MTI_Consultar">
 <% if(request.getAttribute("Registros")!= null) out.print(request.getAttribute("Registros")); %>
 <% if(request.getAttribute("Botones")!= null) out.print(request.getAttribute("Botones")); %>
 <br>
 <br>
 <center>
 <% if(request.getAttribute("Imprimir")!= null) out.print(request.getAttribute("Imprimir")); %><% if(request.getAttribute("Exportar")!= null) out.print(request.getAttribute("Exportar")); %>
 </center>

 <input type=hidden name=Modulo value=<%= request.getAttribute("Modulo") %>>
 <input type=hidden name=Pagina value=<%= request.getAttribute("Pagina") %>>
 <input type=hidden name=Numero value=<%= request.getAttribute("Numero") %>>
 <input type=hidden name=FechaIni value='<%= request.getAttribute("FechaIni") %>'>
 <input type=hidden name=FechaFin value='<%= request.getAttribute("FechaFin") %>'>
 <input type=hidden name=Busqueda value='<%= request.getAttribute("Busqueda") %>'>
 <input type=hidden name=TransEnv value='<%= request.getAttribute("TotalTrans") %>'>
 <input type=hidden name=NumContrato value='<%= request.getAttribute("NumContrato") %>'>
 <input type=hidden name=NomContrato value='<%= request.getAttribute("NomContrato") %>'>
 <input type=hidden name=NumUsuario value='<%= request.getAttribute("NumUsuario") %>'>
 <input type=hidden name=NomUsuario value='<%= request.getAttribute("NomUsuario") %>'>
 <input type=hidden name=ClaveBanco value='<%= request.getAttribute("ClaveBanco") %>'>
  <%
  String web_application = "";
  if(session.getAttribute("web_application")!=null)
	web_application = (String)session.getAttribute("web_application");
  else if (request.getAttribute("web_application")!= null)
	web_application= (String)request.getAttribute("web_application");
  web_application=web_application.trim();
  %>
  <input type=hidden name="web_application" value = '<%=web_application%>'>

</FORM>

</body>
</html>