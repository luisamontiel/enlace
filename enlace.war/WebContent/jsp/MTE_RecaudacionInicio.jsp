<html>
<head>
	<title>TESOFE - Consulta Recaudaci&oacute;n</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

  /*Indice Calendario*/
  var Indice=0;

  /*Arreglos de fechas del dia*/
  <%
    //if(request.getAttribute("VarFechaHoy")!= null)
      out.print(request.getAttribute("VarFechaHoy"));
  %>
  /*Dias inhabiles*/
  diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';


function js_consultar()
 {
	var forma=document.Recaudacion;
	  if(VerificaFecha())
	 {
	  document.Recaudacion.Modulo.value="1";
	  document.Recaudacion.operacion.value="TESOFE2";
  	  document.Recaudacion.Numero.value="100";
  	  document.Recaudacion.Bandera.value="0";
      document.Recaudacion.submit();
	 }

   }

function VerificaFecha()
 {
     if(document.Recaudacion.fechaIni.value=="")
      {
		cuadroDialogo("Error: Aun no ha selecionado la fecha.",3);
        return false;
      }

  return true;
 }

function FrmClean()
 {
   document.Recaudacion.reset();
 }

function Actualiza()
 {
   if(Indice==0)
     document.Recaudacion.fechaIni.value=Fecha[Indice];
 }

function WindowCalendar()
 {
    var m=new Date()

	Indice=0;
    n=m.getMonth();
    msg=window.open("/EnlaceMig/MTE_Calendar.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
 }

function VentanaAyuda(cad)
 {
   return;
 }

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { /*v3.0*/
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { /*v3.0*/
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { /*v3.0*/
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { /*v3.0*/
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="//gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>

<!-- nrn cambiale el action por el nombre del Servlte de verdad-->
 <FORM   NAME="Recaudacion" METHOD="Post" ACTION="MTE_Recaudaciones">

<table width="760" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td align="center">
   <table border="0" width=300 cellspacing="2" cellpadding="3">
    <tr>
     <td class="tittabdat" nowrap> Seleccione los datos:</td>
    </tr>
	 <tr>
		   <td class="textabdatcla" valign="top" align=center>
             <table border="0" cellspacing="5" cellpadding="2" width="100%">
			  <tr>
           <td align="right" class="tabmovtex"> Fecha de Consulta:<br></td>
		 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fechaIni" size="12" class="tabmovtex" OnFocus = "blur();"><a href="javascript:WindowCalendar();"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
      </td>
	 </tr>
    </table>
   </td>
  </tr>
 </table>

        <br>
        <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
         <tr>
		  <td align="right" valign="top" height="22" width="90"><a href="javascript:js_consultar();"><img border="0" name=Enviar value=Enviar src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"></a></td><td align="left" valign="middle" height="22" width="76"><a href="javascript:FrmClean();"><img name=limpiar value=Limpiar border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a></td>
		 </tr>
        </table>
	 </td>
    </tr>
  </table>

<%
 	String Modulo="";
	String fechaIni="";
	String Bandera="";
	String Sucursal="";

	if(session.getAttribute("fechaIni") !=null)
	  fechaIni = (String)session.getAttribute("fechaIni");
	else
	 if(request.getAttribute("fechaIni")!=null)
	  fechaIni = (String)request.getAttribute("fechaIni");

	if(session.getAttribute("Bandera") !=null)
	  Bandera = (String)session.getAttribute("Bandera");
	else
	 if(request.getAttribute("Bandera")!=null)
	  Bandera = (String)request.getAttribute("Bandera");

	if(session.getAttribute("Modulo") !=null)
	  Modulo = (String)session.getAttribute("Modulo");
	else
	  if(request.getAttribute("Modulo")!=null)
	    Modulo = (String)request.getAttribute("Modulo");

	if(session.getAttribute("Sucursal") !=null)
	  Modulo = (String)session.getAttribute("Sucursal");
	else
	  if(request.getAttribute("Sucursal")!=null)
	    Modulo = (String)request.getAttribute("Sucursal");

	%>

    <input type=hidden name=pagina value=0>
	<input type=hidden name=operacion value=0>
	<input type=hidden name=Numero value=0>
	<input type=hidden name=Modulo value=0>
	<input type=hidden name=Bandera value=0>
	<input type=hidden name=Sucursal value=0>

 </FORM>
</body>
</html>