<html>
<head>
<title>Banca Virtual</title>
<script language="JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<SCRIPT language="JavaScript1.2" SRC="/EnlaceMig/list.js"></SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">

<%
String ArchivoErr = (String)request.getAttribute("ArchivoErr");
if(ArchivoErr!=null)
	out.println(ArchivoErr);
%>

<%
String TiempoTerminado = (String)request.getAttribute("TiempoTerminado");
if(TiempoTerminado!=null)
	out.println(TiempoTerminado);
%>

function Formatea_Importe(importe)
{
   decenas=""
   centenas=""
   millon=""
   millares=""
   importe_final=importe
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}

function validAmount (cantidad)
{
  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad
  var miles="";

  if (cantidadAux == "" || cantidadAux <= 0)
  {
    document.transferencia.monto.value="";
    //document.transferencia.monto.focus();
    cuadroDialogo("\n Capture el IMPORTE a transferir ",3);
    return false;
  }
  else
  {
    if (isNaN(cantidadAux))
    {
	  document.transferencia.monto.value="";
	  //document.transferencia.monto.focus();
      cuadroDialogo("Favor de ingresar un valor num\351rico en el IMPORTE",3)
      return false
    }

    pos_punto = cantidadAux.indexOf (".")
    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

      if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

      if (miles != "")
        strAux2 = miles
      if (cientos != "")
        strAux2 = strAux2 + cientos + "."
      strAux2 = strAux2 + cents

      transf = document.transferencia.monto.value
    }
    document.transferencia.montostring.value = strAux1;

    strAux1=Formatea_Importe(strAux1)
    if (miles != "")
      document.transferencia.montostring.value = strAux2;

    return true;
  }
}

function Formatea_Importe2 (cantidad)
{
  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad


  pos_punto = cantidadAux.indexOf (".");

  num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

  if (pos_punto == 0)
     strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
  else
  {
     if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

    if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

   strAux1=Formatea_Importe(strAux1)
  }
  return strAux1;

}

function validar()
{
  var result=true;
  //result = validAmount (document.transferencia.monto.value);

  if (result==true)
  {
    //modificacion para integracion if (document.transferencia.origen.selectedIndex==0)

	if (document.transferencia.origen.value.length>0)
		result=true
    else
    {
	   cuadroDialogo("Favor de indicar la cuenta de cargo",3);
	   result=false;
    }

	/*if (result==true)
	{
	  if (isNaN(document.transferencia.cve_esp.value))
	  {
        result=false;
        cuadroDialogo("Tipo de dato inv\350lido en clave de tipo de cambio especial",3);
	  }
    } */

  }
  return result;
}

function Cancelar()
{
  document.transferencia.origen.selectedIndex=0;
  document.transferencia.monto.value="";
  //document.transferencia.cve_esp.value="";
  return true;
}

function Cambiar_leyenda()
{
  var valor=document.transferencia.origen.value;
  valor=valor.substring(valor.indexOf("|")+1,valor.length);
  valor=valor.substring(valor.indexOf("|")+1,valor.length);
  valor=valor.substring(valor.indexOf("|")+1,valor.length);

  var tipo=valor.substring(0,valor.indexOf("|"));

  if(eval(tipo)==eval(6))
    document.transferencia.leyenda.value="El cliente vende";
  else
    document.transferencia.leyenda.value="El cliente compra";
  return true;
}

<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;
var opcion;
function PresentarCuentas()
{

  opcion=1; msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.transferencia.origen.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|"+ctatipro+"|";
  document.transferencia.textorigen.value=ctaselec+" "+ctadescr;
  Cambiar_leyenda();
}
function EnfSelCta()
{
  document.transferencia.textorigen.value="";
}

function Consultar()
 {
   document.transferencia.action="MTI_Consultar?Modulo=0";
   document.transferencia.submit();
 }


function EnviarForma()
{
  document.transferencia.action="MIPD_cambios?ventana=1";
  var result=validar();
  if (result==true)
    document.transferencia.submit();

}

//-->
</SCRIPT>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript">
<!--

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%
String newMenu = (String)session.getAttribute("newMenu");
if(newMenu!=null)
	out.println(newMenu);
%>

//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');EnfSelCta();" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">
		<!-- MENU PRINCIPAL -->
		<%
		String MenuPrincipal = (String)session.getAttribute("MenuPrincipal");
		if(MenuPrincipal!=null)
			out.println(MenuPrincipal);
		%>
	</TR>
</TABLE>

<%
String Encabezado = (String)session.getAttribute("Encabezado");
if(Encabezado!=null)
	out.println(Encabezado);
%>

<!-- CONTENIDO INICIO -->
<FORM  NAME="transferencia" METHOD=POST ACTION="MIPD_cambios?ventana=1" >
<INPUT TYPE="HIDDEN" NAME="montostring" VALUE=0>
<p>
<table align="center" border=0 cellspacing=0 cellpadding=3 class='textabdatcla'>
  <tr>
    <td colspan=2 class="tittabdat"> Selecci&oacute;n de Cuenta </td>
  </tr>
  <tr>
    <td colspan=2><br></td>
  </tr>
  <tr>
	<td colspan=2>
	  <table border=0 width="100%">
		<tr>
		  <td>
			<table cellpadding=0  cellspacing=0 border=0>
			  <tr>
			    <td class='tabmovtex'> Tipo de cambio a la venta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
				<td class='tabmovtex'>
				  <%
				  String tcv_venta = (String)request.getAttribute("tcv_venta");
				  if (tcv_venta == "" || tcv_venta == null)
				    tcv_venta = "NO DISPONIBLE";
				  %>
	   			  <input type="text"  name="tcv_venta"  SIZE=10 value='<%=tcv_venta%>' onFocus='blur();'>
				</td>
			  </tr>
			</table>
			<!--<%
			String tcv_compra = (String)request.getAttribute("tcv_compra");
			%>
			<input type="hidden" name="tcv_compra" value='<%=tcv_compra%>'> -->
		  </td>
		</tr>
		<tr>
	    <td>
	        <table cellpadding=0  cellspacing=0 border=0>
	        <tr>
	            <td class='tabmovtex'> Tipo de cambio a la compra &nbsp;&nbsp; </td>
	            <td class='tabmovtex'>
                <%
				  String tcv_compra1 = (String)request.getAttribute("tcv_compra");
				  if (tcv_compra1 == "" || tcv_compra1 == null)
				    tcv_compra1 = "NO DISPONIBLE";
				  %>
				  <input type="text"  name="tcv_compra"  SIZE=10 value='<%=tcv_compra1%>' onFocus='blur();'>
			    </td>
	        </tr>
	        </table>
	 </td>
	 </tr>
	 </table>
	</td>
   </tr>

	</td>
  </tr>
  <tr>
	<td class='tabmovtex' colspan=2>
	  <div align="center">
	  Este tipo de cambio es informativo. <br>
      <%
		  if (request.getAttribute("Mensaje_actualizacion")!=null);
		    out.println(request.getAttribute("Mensaje_actualizacion"));
	  %>
	  <br>
	  Para hacer una cotizaci&oacute;n de tipo<br>
	  de cambio, seleccione la cuenta de cargo <br>

	  </div>

	</td>
  </tr>
  <tr>
	<td colspan=2><br></td>
  </tr>
  <tr>
	<td class='tabmovtex'>Cuenta Cargo</td>
	<td class='tabmovtex'>
	  <!--Modificacion para integracion
	  <SELECT ID="FormsComboBox4" NAME="origen">
	    <%
		String cuentas_cargo = (String)request.getAttribute("cuentas_cargo");
		if(cuentas_cargo!=null)
		  out.println(cuentas_cargo);
		%>
	  </SELECT>
	  -->
      <input type="text" name=textorigen  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
	  <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
	  <input type="hidden" name="origen" value="" >
	</td>
  </tr>
  <tr>
	<td>&nbsp;&nbsp;</td>
	<td class='tabmovtex' ><input type=text name=leyenda  class="tabmovtexbol" maxlength=22 size=22 onFocus="blur();"></td>
  </tr>

  <!--<tr>
	<td class='tabmovtex'>Importe</td>
	<td class='tabmovtex'><input type=text name=monto size=15 MAXLENGTH=15></td>
  </tr>-->
    <input type=hidden name=monto size=10 MAXLENGTH="10" VALUE="0">

  <!--<tr>
	<td class='tabmovtex'>Clave especial</td>
	<td class='tabmovtex'><input type=text size=10 maxlength=10 name=cve_esp></td>-->

  </tr>
  <tr>
	<td colspan=2><br></td>
  </tr>
</table>





<br>
  <table border=0 cellpadding=0 cellspacing=0 align=center>
   <tr>
     <td>
	 <a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/gbo25310.gif" border=0></a></td>
         <td><A href="javascript:Consultar();" border=0><img src="/gifs/EnlaceMig/gbo25400.gif" border=0></a></td>
   </tr>
  </table>


</form>
<!-- CONTENIDO FINAL -->
</body>
</html>

<Script language = "JavaScript">
<!--
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>