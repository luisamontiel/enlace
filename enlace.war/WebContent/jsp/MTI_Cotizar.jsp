<html>
<head>
	<title>Cotizar - Enviar</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript">

var respuesta=0;

function continua()
{
  if(respuesta==1)
    document.tabla.submit();
}

function Enviar()
{
  document.getElementById("enviar").style.visibility="hidden";
  document.getElementById("enviar2").style.visibility="visible";
  cuadroDialogo("Desea enviar la Transferencia?",2);
}

function VentanaAyuda(cad)
{
  return;
}

function Cancelar()
{
  document.tabla.action="MTI_Transferencia";
  document.tabla.submit();
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%= request.getAttribute("strScript") %>
<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

 <FORM NAME="tabla" method=post onSubmit="return ValidaTabla(this);" action="MTI_Enviar">
  <p>
   <!-- Datos del Usuario -->
   <!--
   <table border=0 align=center>
   <tr>
     <th bgcolor='#DDDDDD'><font color=black>Datos del Usuario</font></th>
   </tr>
   <tr>
     <td align="center">
       <font size="2" face="Arial, Helvetica, sans-serif">
	     <b>Contrato: </b><%= request.getAttribute("NumContrato") %> <%= request.getAttribute("NomContrato") %>
	   </font>
	 </td>
   </tr>
   <tr>
     <td bgcolor='#FFFFFF'>
	   <b>Usuario: </b><%= request.getAttribute("NumUsuario") %> <%= request.getAttribute("NomUsuario") %>
	 </td>
   </tr>
   <tr>
     <td><br></td>
   </tr>
   </table>
   //-->

  <table border=0 align=center>
   <tr>
    <td align=right class='textabref'>
     <%= request.getAttribute("totalRegistros") %>&nbsp;
	 <%= request.getAttribute("importeTotal") %>
	</td>
   </tr>

   <tr>
    <td align=center>
     <%= request.getAttribute("Tabla") %>
    </td>
   </tr>
  </table>

  <input type=hidden name=TotalTrans value='<%= request.getAttribute("TotalTrans") %>'>
  <input type=hidden name=Lineas value='<%= request.getAttribute("Lineas") %>'>
  <input type=hidden name=ImporteT value='<%= request.getAttribute("Importe") %>'>
  <input type=hidden name=Tiempo value='<%= request.getAttribute("Tiempo") %>'>
  <p>

  <table id="enviar2" style="visibility:hidden" border=0 align=center cellpadding=0 cellspacing=0>
   <tr align="center" class="tabmovtex">
   	 <td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
   </tr>
  </table>
  <table id="enviar" border=0 align=center cellpadding=0 cellspacing=0>
   <tr>
     <td><a href="javascript:Cancelar();"><img src="/gifs/EnlaceMig/gbo25190.gif" border=0 alt="Cancelar"></a></td>
	 <%= request.getAttribute("BotonEnviar")%>
     <td><a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir"></a></td>
   </tr>
  </table>

  <input type=hidden name=TCV_Venta value='<%= request.getAttribute("TCV_Venta") %>'>
  <input type=hidden name=TCE_Venta value='<%= request.getAttribute("TCE_Venta") %>'>
  <input type=hidden name=Cuentas value='<%= request.getAttribute("inicialCuentaCargo") %>'>
  <input type=hidden name=Importe value='<%= request.getAttribute("inicialImporte") %>'>
  <input type=hidden name=DesDivisa value='<%= request.getAttribute("inicialDesDivisa") %>'>
  <input type=hidden name=CveDivisa value='<%= request.getAttribute("inicialCveDivisa") %>'>

  <input type=hidden name=Result value='<%= request.getAttribute("Result") %>'>
  <input type=hidden name=Modulo value=0>

 </FORM>
</body>
</html>