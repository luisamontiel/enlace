<%-- 
  - Autor: FSW Everis
  - Aplicacion: Enlace
  - Módulo: SPID
  - Fecha: 01/06/2016
  - Descripcion: Pantalla que contiene el resultado de las operaciones, registro, modificacion y baja de fiel.
  --%>
<%@include file="/jsp/fielCabecera.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
    
<html>
<head>

</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style="background-color: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')">
    
    <table border="0" cellpadding="0" cellspacing="0" width="571">
		<tr valign="top">
			<td width="*">${MenuPrincipal}</td>
		</tr>
	</table>
	${Encabezado}

	<c:choose>
		<c:when test="${empty fielResult}">
		<fmt:message key="grl.fiel.msg4" bundle="${lang}"/>
		</c:when>
		<c:otherwise>
			<table border="0" cellpadding="0" cellspacing="0" width="760" class="tabfonbla">
				<tr>
					<td align="center">
						<form name="regFiel" method="post" action="FielBaja?modulo=4" id="formResult">
							<table style="border: 0; margin: 0 auto;" cellpadding="5"
								cellspacing="3" class="content" width="500">
								<tr>
                                    <c:if test="${not empty fielResult.rfc}">
                                        <th class = "tittabdat"><fmt:message key="grl.fiel.rfc"     bundle="${lang}"/></th>
                                    </c:if>
									<th class = "tittabdat"><fmt:message key="grl.fiel.cuenta1" bundle="${lang}"/></th>
									<th class = "tittabdat"><fmt:message key="grl.fiel.fiel"    bundle="${lang}"/></th>
									<th class = "tittabdat"><fmt:message key="grl.fiel.est"     bundle="${lang}"/></th>
									<th class = "tittabdat"><fmt:message key="grl.fiel.ref"     bundle="${lang}"/></th>
								</tr>
									<tr>
                                         <c:if test="${not empty fielResult.rfc}">
                                              <td class = "textabdatobs">${fielResult.rfc}</td>
                                         </c:if>
										<td class = "textabdatobs">${fielResult.cuenta}</td>
										<td class = "textabdatobs">${fielResult.fiel}</td>
										<td class = "textabdatobs">${fielResult.msgError}</td>
										<c:choose>
										<c:when test="${fielResult.codError eq OK or fielResult.codError == 'OK'}">
										<td class = "textabdatobs"><a href="#ni" onclick="javascript: imprimirComprobante()">${fielResult.referencia}</a></td>
										</c:when>
										<c:otherwise>
											<td class = "textabdatobs">${fielResult.referencia}</td>
										</c:otherwise>
										</c:choose>
										
									</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</c:otherwise>
    </c:choose>
</body>
</html>
