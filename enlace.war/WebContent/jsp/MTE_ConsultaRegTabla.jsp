<html>
<head>
	<title>TESOFE - Consulta de Archivos y Registros</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

 <%!
   String formateaFecha(String fec)
   {
		String fecha=fec;
		System.out.println("Longitud de fecha:" +fec.trim().length());
		if(fec.trim().length()==8)
			fecha=fec.substring(6) + "/" + fec.substring(4,6) + "/" + fec.substring(0,4);
		System.out.println("La nueva fecha es: " + fecha +" antes " + fec);
		return fecha;
   }
 %>


function js_regresarConsulta()
 {
   document.TesConsulta.action="MTE_ConsultaCancelacion";
   document.TesConsulta.ventana.value='0';
   document.TesConsulta.submit();
 }

function BotonPagina(sigant)
 {
   document.TesConsulta.action="MTE_ConsultaCancelacion";
   document.TesConsulta.pagina.value=sigant-1;
   document.TesConsulta.submit();
 }

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="//gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>

 <FORM   NAME="TesConsulta" METHOD="Post" ACTION="MTE_ConsultaCancelacion">

 <table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td align=center>
   <%
   	String estatus="";

	String tipoFechaConsulta=(String)request.getAttribute("tipoFechaConsulta");
	String Fecha2_01=(String)request.getAttribute("Fecha2_01");
	String Fecha2_02=(String)request.getAttribute("Fecha2_02");
	String nombreArchivo=(String)request.getAttribute("nombreArchivo");
	String exportar=(String)request.getAttribute("Exportar");
	if(exportar==null)
	  exportar="";

    String chkEstatusReg=(String)request.getAttribute("chkEstatusReg");
	String contratoConsulta=(String)request.getAttribute("contratoConsulta");

	int totalPaginas=Integer.parseInt((String)request.getAttribute("totalPaginas"));
	int totalRegistros=Integer.parseInt((String)request.getAttribute("totalRegistros"));
	int registrosPorPagina=Integer.parseInt((String)request.getAttribute("registrosPorPagina"));
	int pagina=Integer.parseInt((String)request.getAttribute("pagina"));

	int A=registrosPorPagina*pagina;
	int de=(A+1)-registrosPorPagina;

	if(pagina==totalPaginas)
	   A=totalRegistros;

	if(chkEstatusReg.trim().equals("R"))
	  estatus="Vigentes";
	else
	if(chkEstatusReg.trim().equals("C"))
	  estatus="Cancelados";
	else
	if(chkEstatusReg.trim().equals("D"))
	  estatus="Vencidos";
	else
	if(chkEstatusReg.trim().equals("P"))
	  estatus="Pagados";
   %>

   <table border=0>
    <tr>
	 <td>

	 <table border=0 align=center cellspacing=0 cellpadding=1 width="100%">

	  <tr>
		<td class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red><%=totalRegistros%></font></b></td>
	  <%if(tipoFechaConsulta.equals("G"))
	    {
	  %>
		<td align=right class='textabref'><b> del <font color=red><%=formateaFecha(Fecha2_01)%></font> al <font color=red><%=formateaFecha(Fecha2_02)%></font>&nbsp;</b></td>
	  <%
	    }
	  %>
	  </tr>
	  <tr>
 	    <td class='textabref'><b>&nbsp;Para el contrato: <font color=green><%=contratoConsulta%></font>&nbsp;</b></td>
		<td align=right class='textabref'><b>Consulta: <font color=green><%=(tipoFechaConsulta.equals("D"))?"Detallada":"General"%></font><%=(tipoFechaConsulta.equals("D"))?"":"&nbsp;&nbsp;&nbsp; Estatus: <font color=green>"+estatus%></font></b></td>
	  <tr>
		<td align=left class='textabdatcla'><b>&nbsp;Pagina <font color=blue><%=pagina%></font> de <font color=blue><%=totalPaginas%></font>&nbsp;</b></td>
		<td align=right class='textabdatcla'><b>Registros <font color=blue><%=de%></font> - <font color=blue><%=A%></font></b>&nbsp;</td>
	  </tr>

	 </table>
	 <!-- Tabla de registros obtenidos //-->
	 <%=request.getAttribute("tablaRegistros")%>
	 <!-- fint Tabla //-->
 	 <table border=0 align=center cellspacing=0 cellpadding=0 width="100%">
	  <tr>
		<td align=left class='textabref'><b>&nbsp;Pagina <font color=blue><%=pagina%></font> de <font color=blue><%=totalPaginas%></font>&nbsp;</b></td>
		<td align=right class='textabref'>&nbsp;<b>Registros <font color=blue><%=de%></font> - <font color=blue><%=A%></font></b>&nbsp;</td>
	  </tr>
	 </table>

	</td>
   </tr>
  </table>

    <%=request.getAttribute("Botones")%>

	<input type=hidden name=tipoFechaConsulta value='<%=tipoFechaConsulta%>'>
	<input type=hidden name=Fecha2_01 value='<%=Fecha2_01%>'>
	<input type=hidden name=Fecha2_02 value='<%=Fecha2_02%>'>
	<input type=hidden name=nombreArchivo value='<%=nombreArchivo%>'>
	<input type=hidden name=contratoConsulta value='<%=contratoConsulta%>'>
	<input type=hidden name=totalRegistros value='<%=totalRegistros%>'>
	<input type=hidden name=registrosPorPagina value='<%=registrosPorPagina%>'>
	<input type=hidden name=totalPaginas value='<%=totalPaginas%>'>
	<input type=hidden name=Exportar value="<%=exportar%>">
	<input type=hidden name=chkEstatusReg value='<%=chkEstatusReg%>'>

	<input type=hidden name=ventana value='2'>
	<input type=hidden name=pagina>

   </td>
  </tr>
  <tr>
   <td>
    <br>
	<table align=center border=0 cellspacing=0 cellpadding=0>
	 <tr>
	  <td><A href="javascript:js_regresarConsulta();" border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt=Regresar></a><a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' alt="Imprimir" border=0></a><%=exportar%></td>
	 </tr>
    </table>
   </td>
  </tr>
 </table>


 </FORM>
</body>
</html>