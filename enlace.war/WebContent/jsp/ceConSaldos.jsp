<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<%-- 01/10/2002 Corrección etiquetas HTML --%>
<html><!-- #BeginTemplate "/Templates/principal.dwt" -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Enlace</title>
<!-- #EndEditable -->

<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25150">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="27/04/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->
<script language = "JavaScript" type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<Script language = "JavaScript" type="text/javascript" SRC = "/EnlaceMig/ConMov.js"></Script>
<Script language = "JavaScript" type="text/javascript">

var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>"
var dia;
var mes;
var anio;
var fechaseleccionada;
var opcioncaledario;



<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.FrmSaldos.EnlaceCuenta.value=ctaselec+"@"+ctatipre+"@"+ctadescr+"@";
  document.FrmSaldos.textEnlaceCuenta.value=ctaselec+" "+ctadescr;
}

function DoPost()
 {
	if (eval(document.FrmSaldos.textEnlaceCuenta.value.length)>0)
	 {
		document.FrmSaldos.submit();
	 }else
    cuadroDialogo("Debe seleccionar una cuenta",3);
 }

function FrmClean(){
  document.FrmSaldos.textEnlaceCuenta.value = "";
 }


</SCRIPT>

<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript" type="text/javascript">
<!--

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<!--<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="document.FrmSaldos.deldia[0].checked = true;PutDate();MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
-->

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>
<%= request.getAttribute("Encabezado") %>

<FORM NAME="FrmSaldos" METHOD="Post"  Action="ceConSaldos?Modulo=1">
<table width="760" border="0" cellspacing="0" cellpadding="0">
<!--<form name = "Frmfechas">
  <%= request.getAttribute("Movfechas") %></td>
</form>

onSubmit="DoPost();"         border="0" cellspacing="2" cellpadding="3" -->


   <tr>
      <td align="center">
        <table width="400" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <TD>
               <BR>
            </TD>
          </tr>
          <tr align="center">
            <td class="textabdatcla" colspan="2">
            <table>
                <tr>
                  <td align="right" class="tabmovtexbol" width="100" nowrap>Cuenta:</td>
                  <td width="500">
				    <input type="text" name=textEnlaceCuenta  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="" >
					<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align="absmiddle"></A>
					<input type="hidden" name="EnlaceCuenta" value="" >
                  </td>
                </tr>
             </table>
            </td>
          </tr>
        </table>
        <br>
        <table width="160" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
          <tr>
            <td align="right" height="25">
              <a href="javascript:DoPost();" border=0>
              <img  src="/gifs/EnlaceMig/gbo25220.gif" border="0" alt="Enviar">
			  </a>

            </td>
            <td align="left" height="25">
			  <a href="javascript:FrmClean();" border="0">
              <img src="/gifs/EnlaceMig/gbo25250.gif" border="0" alt="Borrar">
			  </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>

    <Input type = "Hidden" name ="FechaI">
    <input type = "Hidden" name = "FechaF">
	<input type = "Hidden" name ="Banca" value="<%= request.getAttribute("tipoBanca") %>">

</form>
</body>
</html>