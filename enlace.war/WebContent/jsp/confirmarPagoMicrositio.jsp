<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="mx.altec.enlace.utilerias.*"%>
<%@ page import="mx.altec.enlace.servlets.revisarClavePemex" %>
<jsp:directive.page import="mx.altec.enlace.servlets.BaseServlet"/>
<%
/**
 * Correccion vulnerabilidades Deloitte
 * Stefanini Nov 2013
 */


	String referencia = "";
	String url ="";
	String concepto = "";

	String lineaCaptura = "";

	String convenio = (String)request.getParameter("convenio");
	String cuentaCargo = (String)request.getParameter("cuentaCargo");
	String cuentaAbono = (String)request.getParameter("cuentaAbono");
	String fecha = (String)request.getParameter("fecha");
	String importe = (String)request.getParameter("importe");
	String numContrato = (String)request.getParameter("numContrato");
    String empresa = Util.HtmlEncode((String)request.getParameter("empresa"));
	String titularCuentaCargo = Util.HtmlEncode((String)request.getParameter("titularCuentaCargo"));
    String servicio_id = Util.HtmlEncode((String)request.getParameter("servicio_id"));
	String nomContrato = Util.HtmlEncode((String)request.getParameter("nomContrato"));

    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::convenio::"+convenio, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::cuentaCargo::"+cuentaCargo, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::cuentaAbono::"+cuentaAbono, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::fecha::"+fecha, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::importe::"+importe, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::numContrato::"+numContrato, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::empresa::"+empresa, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::titularCuentaCargo::"+titularCuentaCargo, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::servicio_id::"+servicio_id, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::EntraValidaciones::"+nomContrato, EIGlobal.NivelLog.DEBUG);

	if(!"PMRF".equals(servicio_id))
	{
		EIGlobal.mensajePorTrace("ConfirmarPagoMicrositio.jsp::el contenido del campo URL es:" + request.getParameter("url"), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConfirmarPagoMicrositio.jsp::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)request.getParameter("url")), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConfirmarPagoMicrositio.jsp::el contenido del campo URL despues es:" + request.getParameter("url"), EIGlobal.NivelLog.INFO);

		referencia = Util.HtmlEncode((String)request.getParameter("referencia"));
		url = request.getParameter("url");
	    concepto = Util.HtmlEncode((String)request.getParameter("concepto"));

	    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::referencia::"+referencia, EIGlobal.NivelLog.DEBUG);
	    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::url::"+url, EIGlobal.NivelLog.DEBUG);
	    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::concepto::"+concepto, EIGlobal.NivelLog.DEBUG);
	} else {
		EIGlobal.mensajePorTrace("ConfirmarPagoMicrositio.jsp::Pago Referenciado -----> :" + request.getParameter("servicio_id") + ": <-----", EIGlobal.NivelLog.INFO);

		lineaCaptura = Util.HtmlEncode((String)request.getParameter("linea_captura"));

	    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::linea_captura::" + lineaCaptura, EIGlobal.NivelLog.DEBUG);
	}

    boolean bandera = true;
    String codErrorMicr = "";
    if(null!=convenio && !Util.validaCCX(convenio, 1)){
    	codErrorMicr="1003";
    	bandera=false;
        EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Error::convenio::" + convenio, EIGlobal.NivelLog.DEBUG);
    }else
    if(null!=cuentaCargo && !Util.validaCCX(cuentaCargo, 1)){
        codErrorMicr="1008";
    	bandera=false;
    	EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Error::cuentaCargo::" + cuentaCargo, EIGlobal.NivelLog.DEBUG);
    }else
    if(null!=cuentaAbono && !Util.validaCCX(cuentaAbono, 1)){
        codErrorMicr="1009";
    	bandera=false;
    	EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Error::cuentaAbono::" + cuentaAbono, EIGlobal.NivelLog.DEBUG);
    }else
    if(null!=fecha && !Util.validaCCX(fecha, 3)){
        codErrorMicr="1007";
       	bandera=false;
       	EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Error::fecha::" + fecha, EIGlobal.NivelLog.DEBUG);
    }else
    if(null!=numContrato && !Util.validaCCX(numContrato, 1)){
        codErrorMicr="1006";
       	bandera=false;
       	EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Error::numContrato::" + numContrato, EIGlobal.NivelLog.DEBUG);
    }
	
	if(!"PMRF".equals(servicio_id))
	{
		if(null!=importe && !Util.validaCCX(importe, 4)){
			codErrorMicr="1004";
			bandera=false;
			EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Error::importe::" + importe, EIGlobal.NivelLog.DEBUG);
		}
	} 
	else
	{	
		if(null!=importe && !Util.validaCCX(importe, 1)){
			codErrorMicr="1004";
			bandera=false;
			EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Error::importe::" + importe, EIGlobal.NivelLog.DEBUG);
		}
	}
	
    EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::SaleValidaciones::"+nomContrato, EIGlobal.NivelLog.DEBUG);
if(bandera){
/** Correccion vulnerabilidades Deloitte */

	//EVERIS mantenimiento 1681, se instancia la clase revisarClavePemex, para compara la clave recibida en txtconcepto y ver si se encuentra en la base de datos
	String mensaje = "";
	String web_application = (String)request.getAttribute("web_application");
	if (web_application == null)
		web_application = "EnlaceMig";

	if(!"PMRF".equals(servicio_id)) {

		revisarClavePemex rcp = new revisarClavePemex();
		String clave = null;
		String parametroNuevo = null;
		String claveNueva = null;
		ResultSet rs;
		/*
		consulta(Connection conn, String clave)
		*/
		clave=(String)request.getParameter("concepto");
		if (!clave.equals("")) {
			//String resultado = rcp.consulta();
			//Connection conn = null;
			try{
			rcp.Crear();

			parametroNuevo = clave.substring(0,1);
			claveNueva = clave.substring(1);
			System.out.println("Parametro nuevo->" + parametroNuevo);
			System.out.println("Clave Nueva->" + claveNueva);
			//String query = "select clave from stprod.EWEB_CAT_USU_PEM where clave='"+claveNueva+"'";
			String query = "select clave from stprod.EWEB_CAT_USU_PEM where clave like '%" + claveNueva + "%'";
			EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Query::" + query + "::", EIGlobal.NivelLog.DEBUG);
			rs = rcp.Consulta(query);
			if (rs.next()) {
				System.out.println("confirmarPagoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ el concepto se encontro dentro de la tabla de SIICS, el pago procede" );

			} else {
					System.out.println("confirmarPagoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ NO SE ENCONTR� CONCEPTO EN TABLA DE CLAVES");
					System.out.println("confirmarPagoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ SE REDIRIGE HACIA LA P�GINA ANTERIOR");
					//RequestDispatcher rd=getServletContext().getRequestDispatcher("/cuentaCargoMicrositio.jsp?opcion=0&error=1");
					//RequestDispatcher rd=getServletContext().getRequestDispatcher("/NASApp/" + web_application + "/jsp/cuentaCargoMicrositio.jsp");
					//rd.forward(request, response);
					mensaje = "<script type=\"text/javascript\" language=\"javascript\"> 				cuadroDialogo(\"La SIIC proporcionada no se encuentra en la base de datos<br/>el pago no puede realizarse\",1); history.back(1);</script>";
					session.setAttribute("error", "1");
					System.out.println("confirmarPagoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ ");
					System.out.println("confirmarPagoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ session mensaje: " + mensaje +" "+session.getAttribute("error"));
					System.out.println("confirmarPagoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ ");

			}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Se controla detalle al cerrar conexion.:.1", EIGlobal.NivelLog.DEBUG);
			} catch (IllegalStateException e) {
				EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Se controla detalle al cerrar conexion.:.2", EIGlobal.NivelLog.DEBUG);
			}catch (Exception e2) {
				EIGlobal.mensajePorTrace("ConfirmarPagoMicrositiojsp::Se controla detalle al cerrar conexion.:.3", EIGlobal.NivelLog.DEBUG);
			}finally{
					if (rcp != null){
						rcp.Cerrar();
					}
			}
			//fin everis
		}
	}
%>


<%
    Calendar dt = Calendar.getInstance();
    dt.setTime( new java.util.Date() );
    int mes = dt.get(Calendar.MONTH);
    int dia = dt.get(Calendar.DAY_OF_MONTH);
    int anio = dt.get(Calendar.YEAR);

	java.util.Date hora = new java.util.Date();
	int hh = hora.getHours();
	int mm = hora.getMinutes();
	int ss = hora.getSeconds();
%>
<%!
String sinComas (String a) {
    StringBuffer sb = new StringBuffer();
    for (int k=0; k < a.trim().length() ;k++){
        if(a.charAt(k)!=',')
            sb.append(a.charAt(k));
    }
    return sb.toString();
}
%>

<%!
  String formatea ( String a){
    String temp ="";
    String deci = "";
    String regresa = "";
    int valexp = 0;
    int indice =    a.indexOf(".");
    int otroindice  = a.indexOf("E");
	try{
		if( otroindice>0 ){
			BigDecimal mivalor = new BigDecimal( a );
			a = mivalor.toString();
			indice =    a.indexOf(".");
		}
		if( indice>0 )
			a = (new BigDecimal(a+"0")).toString();
		else
			a = (new BigDecimal(a+".00")).toString();

		indice =        a.indexOf(".");
		temp = a.substring (0, indice);
		deci = a.substring ( indice+1, indice+3 );

		String original = temp;
		int indexSN = 0;
		boolean bSignoNegativo = false;
		if( ( indexSN = original.indexOf("-") )> -1 ){
			bSignoNegativo = true;
			temp = original.substring( indexSN + 1 , temp.length() );
		}
		while( temp.length()>3 ){
	        a = temp.substring(temp.length() -3, temp.length() );
		    regresa = ","+a+regresa;
			temp = temp.substring(0,temp.length()-3);
		}
		regresa = temp + regresa+"."+deci;
	    if( bSignoNegativo ){
	        regresa = "($"+regresa +")";
		}
	}catch(Exception e){
	e.printStackTrace();  }
    return regresa;
}
%>


<%

	String moneda = cuentaAbono.substring(0,2);
	System.out.println("MONEDA ........." + moneda);


	//cuentaCargo="65500356333";

	//cuentaAbono="65001601848";
	//cuentaCargo="65471000015";

	System.out.println("===============================================================================");
	System.out.println("confirmarPagoMicrositio");
	System.out.println("nombreContrato  ....... [" + nomContrato + "]");
	System.out.println("numeroContrato  ....... [" + numContrato + "]");
	System.out.println("Valores de transferencia por PGSM desde confirmarPagoMicrositio.jsp          ");
	System.out.println("empresa      ....... [" + empresa + "]");
	System.out.println("convenio     ....... [" + convenio + "]");
	System.out.println("cuentaCargo  ....... [" + cuentaCargo + "]");
	System.out.println("titular      ....... [" + titularCuentaCargo + "]");
	System.out.println("cuentaAbono  ....... [" + cuentaAbono + "]");
	System.out.println("importe      ....... [" + importe + "]");
	System.out.println("fecha        ....... [" + fecha + "]");
	if(!"PMRF".equals(servicio_id)) {
		System.out.println("url          ....... [" + url + "]");
		System.out.println("referencia   ....... [" + referencia + "]");
	} else {
		System.out.println("linea de Captura   ....... [" + lineaCaptura + "]");
	}
	System.out.println("===============================================================================");
	System.out.println("importe              ....... [" + importe + "]");


%>

<html>
	<head>
	<title>Santander</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<link rel="stylesheet" href="/EnlaceMig/pagos.css" type="text/css"/>
	<script type="text/JavaScript" SRC= "/EnlaceMig/microSitio_cuadroDialogo.js"></script>
	<script type="text/JavaScript">
	<!--

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}

	function enviarForma() {
		document.getElementById("enviar").style.visibility="hidden";
		document.getElementById("enviar2").style.visibility="visible";
		document.confirmaPagoMicrositio.cerrarSesion.value="NO";

		if ("PMRF" == document.confirmaPagoMicrositio.servicio_id.value)
			document.confirmaPagoMicrositio.action="/Enlace/enlaceMig/PagoMicrositioRef";
		else
			document.confirmaPagoMicrositio.action="/Enlace/enlaceMig/PagoMicrositio";
		document.confirmaPagoMicrositio.submit();
	}

	function validarSesion()
	 {
		var cerrar = document.confirmaPagoMicrositio.cerrarSesion.value;
		if (cerrar != "NO"){
			ventana=window.open('/Enlace/enlaceMig/logout?operacionOK=N','trainerWindow','width=580,height=350,toolbar=no,scrollbars=no,left=210,top=225');
		}

	 }


	function regreso(){
		document.confirmaPagoMicrositio.cerrarSesion.value="NO";
		document.confirmaPagoMicrositio.action="/Enlace/jsp/cuentaCargoMicrositio.jsp";
		document.confirmaPagoMicrositio.submit();
	}

	var enviar = true;
	var dayarray=new Array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
	var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


	var mydate=new Date(<%=anio%>,<%=mes%>,<%=dia%>,<%=hh%>,<%=mm%>,<%=ss%>);

	function reloj()
	 {
	   if(!document.layers && !document.all)
		 return;

	   var digital = new Date();
	   var year=digital.getYear();
	   if (year < 1000)
		  year+=1900;
	   var day=digital.getDay();
	   var month=digital.getMonth();
	   var daym=digital.getDate();
	   if (daym<10)
		  daym="0"+daym;

	   var horas = digital.getHours();
	   var minutos = digital.getMinutes();
	   var segundos = digital.getSeconds();
	   var amOrPm = "AM";
	   if (horas > 11)
		 amOrPm = "PM";
	   if (horas > 12)
		 horas = horas - 12;
	   if (horas == 0)
		 horas = 12;
	   if (minutos <= 9)
		 minutos = "0" + minutos;
	   if (segundos <= 9)
		 segundos = "0" + segundos;

	   dispTime = "<font color='666666' face='Verdana'>" + dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" | "+horas+":"+minutos+":"+segundos+" "+ amOrPm +"</font>";
	   //dispTime = horas + ":" + minutos + ":" + segundos + " " + amOrPm;
	   if (document.layers)
		{
		  document.layers.objReloj.document.write("<font face=Tahoma size=1>"+ dispTime + "</font>");
		  document.layers.objReloj.document.close();
		}
	   else
		if (document.all)
		  objReloj.innerHTML = dispTime;
	   setTimeout("reloj()", 1000);
	 }

	//-->
	</script>



	</head>

	<body onLoad="reloj()" onBeforeUnload="validarSesion()">

		<%
		if (! mensaje.equals("")) {
		%>
			<%=mensaje%>
		<%
		}
		%>


		<span id="objReloj" style="position:absolute;left:285;top:60;"></span>
		<table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
			<td width="2"><img src="/gifs/EnlaceMig/cheque_img.jpg" width="147" height="40"/></td>
			<td width="593" valign="top" bgcolor="#FF0000"><img src="/gifs/EnlaceMig/top.gif" width="453" height="40"/></td>
		  </tr>
		  <tr>
			<td background="/gifs/EnlaceMig/sombra_gris.jpg"><img src="/gifs/EnlaceMig/sombra_gris.jpg" width="18" height="14"/></td>
			<td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td><table width="100%" border="0" cellspacing="0" cellpadding="6">
				<tr>
				  <td align="left" class="titulorojo"> <div align="left">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						  <td colspan="2" valign="top" class="minitxt"></td>
						  <td align="right"><a href="javascript:;" onClick="javascript:document.confirmaPagoMicrositio.cerrarSesion.value='NO'; MM_openBrWindow('/EnlaceMig/ayuda003_enlace.html','ayuda','scrollbars=yes,resizable=yes,width=340,height=420')" onMouseOver="MM_swapImage('Image31','','/gifs/EnlaceMig/ayuda_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/ayuda.jpg" name="Image31" width="47" height="40" border="0" id="Image3"/></a></td>
						</tr>
						<tr>
						  <td width="30%" valign="bottom" class="titulorojo">Informaci&oacute;n
							del pago a: </td>
						  <td valign="bottom" class="titulorojo"><%=convenio%> - <%=empresa%></td>
						  <td align="right">&nbsp;</td>
						</tr>
						<tr>
						  <td><p>&nbsp;</p></td>
						  <td class="titulorojo"><%=numContrato%> | <%=nomContrato%></td>
						  <td align="right">&nbsp;</td>
						</tr>
					  </table>
					</div></td>
				</tr>
			  </table></td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td><form name="confirmaPagoMicrositio" method="POST">
				<INPUT TYPE="hidden" name="cuentaCargo" value="<%=cuentaCargo%>"/>
				<INPUT TYPE="hidden" name="titularCuentaCargo" value="<%=titularCuentaCargo%>"/>
				<INPUT TYPE="hidden" name="cuentaAbono" value="<%=cuentaAbono%>"/>
				<INPUT TYPE="hidden" name="ctaabono" value="<%=cuentaAbono%>"/>
				<INPUT TYPE="hidden" name="importe" value="<%=importe%>"/>
				<INPUT TYPE="hidden" name="fecha" value="<%=fecha%>"/>
				<INPUT TYPE="hidden" name="empresa" value="<%=empresa%>"/>
				<INPUT TYPE="hidden" name="convenio" value="<%=convenio%>"/>
				<INPUT TYPE="hidden" name="servicio_id" value="<%=servicio_id%>"/>
				<INPUT TYPE="hidden" name="opcionElegida" value="null"/>
				<INPUT TYPE="hidden" name="lstContratos" value="<%=numContrato%>"/>
				<INPUT TYPE="hidden" name="numContrato" value="<%=numContrato%>"/>
				<INPUT TYPE="hidden" name="nomContrato" value="<%=nomContrato%>"/>
				<INPUT TYPE="hidden" name="web_application" value="<%=web_application%>"/>
				<%if(!"PMRF".equals(servicio_id)) {%>
					<INPUT TYPE="hidden" name="referencia" value="<%=referencia%>"/>
					<INPUT TYPE="hidden" name="url" value="<%=url%>"/>
					<INPUT TYPE="hidden" name="concepto" value="<%=concepto%>"/>
				<%} else { %>
					<INPUT TYPE="hidden" name="linea_captura" value="<%=lineaCaptura%>"/>
				<%} %>
				<INPUT TYPE="hidden" name="token" value="true"/>
				<INPUT TYPE="hidden" name="cerrarSesion" value="SI"/>

				<table width="100%" border="0" cellspacing="6" cellpadding="0">
				  <tr align="center">
					<td colspan="2"><table width="100%" border="0" cellspacing="4" cellpadding="2">
						<tr align="center">
						  <td width="2" bgcolor="#CCCCCC"><div align="center">Cuenta de
							  Cargo</div></td>
						  <%if(!"PMRF".equals(servicio_id)) {%>
							  <td bgcolor="#CCCCCC">Referencia</td>
						  <%} else { %>
							  <td bgcolor="#CCCCCC">L&iacute;nea Captura</td>
						  <%} %>
						  <td bgcolor="#CCCCCC"><div align="center">Fecha de Aplicaci&oacute;n</div></td>
						  <td bgcolor="#CCCCCC">Importe</td>
						</tr>
						<tr align="center">
						  <td bgcolor="#F0EEEE"><%=cuentaCargo%></td>
						  <%if(!"PMRF".equals(servicio_id)) {%>
							  <td bgcolor="#F0EEEE"><%=referencia%></td>
						  <%} else { %>
							  <td bgcolor="#F0EEEE"><%=lineaCaptura%></td>
						  <%} %>
						  <td bgcolor="#F0EEEE"><%=fecha%></td>
						  <%
						   String verimporte="";
						   if(moneda.equals("49") || moneda.equals("82") || moneda.equals("83")){
							  verimporte=formatea(importe) + " USD";
						   }else{
							  verimporte="$ " + formatea(importe);
						   }%>
						  <td bgcolor="#F0EEEE"><%=verimporte%></td>
						</tr>
						<tr align="center">
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
				  </tr>
			 <tr align="center" id="enviar">
					<td><a href="javascript:enviarForma();" onClick='javascript:document.confirmaPagoMicrositio.cerrarSesion.value="NO";' onMouseOver="MM_swapImage('Image1','','/gifs/EnlaceMig/pagar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/pagar.jpg" name="Image1" width="82" height="41" border="0" id="Image1"/></a></td>
					<td><a href="javascript:regreso();" onClick='javascript:document.confirmaPagoMicrositio.cerrarSesion.value="NO";'  onMouseOver="MM_swapImage('Image2','','/gifs/EnlaceMig/regresar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/regresar.jpg" name="Image2" width="85" height="41" border="0" id="Image2"/></a></td>
				  </tr>


			  <tr align="center"  id="enviar2" style="visibility:hidden">
					<td colspan=2>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
				  </tr>


				</table>
			  </form></td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td align="right"><img src="/gifs/EnlaceMig/sitio_seg.jpg" width="66" height="25"/>&nbsp;&nbsp;&nbsp;</td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr bgcolor="#CCCCCC">
			<td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
			<td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
		  </tr>
		</table>
	</body>
	<%
	/**
	 * Correccion vulnerabilidades Deloitte
	 * Stefanini Nov 2013
	 */
	} else {
		SimpleDateFormat.getInstance().format(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
		String horaerr = sdf.format(new Date());

		session.setAttribute("inyeccionURL","true");
		url = "/Enlace/enlaceMig/LoginServletMicrositio";

	%>
	<html>
		<head>
		<title>Santander</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<script type="text/JavaScript">
		<!--
		function MM_preloadImages() { //v3.0
		  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0;
		i<a.length; i++)
			if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image;
		d.MM_p[j++].src=a[i];}}
		}

		function MM_swapImgRestore() { //v3.0
		  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
		x.src=x.oSrc;
		}

		function MM_findObj(n, d) { //v4.01
		  var p,i,x;  if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length) {
			d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++)
		x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++)
		x=MM_findObj(n,d.layers[i].document);
		  if(!x && d.getElementById) x=d.getElementById(n); return x;
		}

		function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
		   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc)
		x.oSrc=x.src; x.src=a[i+2];}
		}
		//-->
		</script>
		<link href="/EnlaceMig/pagos.css" rel="stylesheet" type="text/css"/>
		</head>
		<body bgcolor="#FFFFFF"leftmargin="20" topmargin="20" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('/gifs/EnlaceMig/cerrar_over.jpg')">
			<form name="msgerror" method="POST" action="<%=url%>">
			<table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
			  <tr>
				<td><table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
					  <td width="2" background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
					</tr>
					<tr>
					  <td> <table width="100%" border="0" cellspacing="0" cellpadding="8">
						  <tr bgcolor="#FFFFFF">
							<td><strong></strong></td>
							<td><strong><span class="titulorojo">Alerta</span></strong></td>
						  </tr>
						  <tr bgcolor="#FFFFFF">
							<td><img src="/gifs/EnlaceMig/flama.jpg" width="42" height="38"/></td>
							<td> <p>N&uacute;mero de Mensaje: <%=codErrorMicr%>. Hora: <%=horaerr%></p>
							<p>
							<strong>Estimado Cliente.</strong><br/><br/>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.
							</p></td>
						  </tr>
						  <tr bgcolor="#FFFFFF">
							<td>&nbsp;</td>
							<td align="center"> <p><a href="javascript:document.msgerror.submit();"onMouseOver="MM_swapImage('Image11','','/gifs/EnlaceMig/cerrar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/cerrar.jpg" name="Image11" width="82" height="41" border="0" id="Image1"/></a></p></td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
					</tr>
				  </table></td>
			  </tr>
			</table>
			</form>
		</body>
		<%
		}
		/**
		 * Fin correccion vulnerabilidades Deloitte
		 * Stefanini Nov 2013
		 */
		%>
</html>