<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<!-- JavaScript del App -->
<script language = "JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/Mancomunidad.js"></script>
<script language = "javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<Script language = "Javascript" >
var js_diasInhabiles="<%= request.getAttribute("diasInhabiles") %>"
var limpia = false;
var defaultEmptyOK = false;

function isDigit (c)
{
	return ((c >= "0") && (c <= "9"))
}

function isEmpty(s)
{
	return ((s == null) || (s.length == 0))
}

function isInteger (s)
{
	var i;

    if (isEmpty(s))
       if (isInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isInteger.arguments[1] == true);


    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);

        if (!isDigit(c)) return false;
    }

    return true;
}

function FrmCleanInter()
{
	limpia = true;
	var control = document.Frmgetinfobit;
	for (i=0; i<control.elements.length; i++)
	{
		var e = control.elements[i];
		if (e.type == "checkbox")
		{
			control.elements[i].checked = false;
		}
	}

	document.Frmgetinfobit.usuario.selectedIndex=0;
	//modificacion para integracion
	document.Frmgetinfobit.cuenta.value="";
	document.Frmgetinfobit.textcuenta.value="";

	document.Frmgetinfobit.Registro[0].checked=true;
	document.Frmgetinfobit.Importe.value="";
	document.Frmgetinfobit.Folio_Registro.value="";

	var today = new Date();
	today.setFullYear(document.Frmfechas.strAnio.value);
	today.setMonth(document.Frmfechas.strMes.value-1);
	today.setDate(document.Frmfechas.strDia.value);
	document.Frmgetinfobit.fecha1.value = document.Frmfechas.strDia.value + "/";
	document.Frmgetinfobit.fecha1.value = document.Frmgetinfobit.fecha1.value + document.Frmfechas.strMes.value + "/";
	document.Frmgetinfobit.fecha1.value = document.Frmgetinfobit.fecha1.value + document.Frmfechas.strAnio.value;
	document.Frmgetinfobit.fecha2.value = document.Frmgetinfobit.fecha1.value;
}

function AgregaFolio()
{
	var h = document.fsaldo.elements.length;
	var folios=""; // = new Array (h);
	var j = 0;
	for (i=0;i<document.fsaldo.elements.length;i++)
	{
		var e = document.fsaldo.elements[i];
		if ((e.type == "checkbox") && e.checked)
		{
			folios = folios + document.fsaldo.elements[i].value + ";";
			j++;
		}
	}
	document.fsaldo.foliosPen.value = folios;
	document.fsaldo.j.value = j;

	if(j==0)
	{
		//alert("USTED NO HA SELECIONADO UNA CUENTA.\n\nPOR FAVOR SELECCIONE UNA CUENTA \nPARA TRAER SU SALDO");
		document.fsaldo.allbox.focus();
		cuadroDialogo("USTED NO HA SELECIONADO UNA CUENTA.\n\nPOR FAVOR SELECCIONE UNA CUENTA \nPARA TRAER SU SALDO", 1);
		return false;
	}
}

var Nmuestra = <%= request.getAttribute( "varpaginacion" ) %>;
function atras(){
    if((parseInt(document.frmbit.prev.value) - Nmuestra)>0){
      document.frmbit.next.value = document.frmbit.prev.value;
      document.frmbit.prev.value = parseInt(document.frmbit.prev.value) - Nmuestra;
      document.frmbit.action = "GetAMancomunidad";
      document.frmbit.submit();
    }
}

function adelante()
{
 if(parseInt(document.frmbit.next.value) != parseInt(document.frmbit.total.value))
 {
    if((parseInt(document.frmbit.next.value) + Nmuestra)< parseInt(document.frmbit.total.value))
	{
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + Nmuestra;
       document.frmbit.action = "GetAMancomunidad";
      document.frmbit.submit();
    }
	else if((parseInt(document.frmbit.next.value) + Nmuestra) > parseInt(document.frmbit.total.value))
	{
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + (parseInt(document.frmbit.total.value) - parseInt(document.frmbit.next.value));
      document.frmbit.action = "GetAMancomunidad";
      document.frmbit.submit();
    }
 }
 else
    cuadroDialogo("No hay mas registros", 1);
}

 function WindowCalendar()
 {
    var m=new Date()

    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMan2.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
}

function WindowCalendar1()
{
    var m=new Date()
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMan12.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
}

<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function actualizacuenta()
{
  document.Frmgetinfobit.cuenta.value=ctaselec;
  document.Frmgetinfobit.textcuenta.value=ctaselec;
}

function redondea()
{
	if ( document.Frmgetinfobit.Importe.value == "" )
		return true;
	if ( isNaN(document.Frmgetinfobit.Importe.value) )
		{
			return false;
			//cuadroDialogo("El valor debe ser num&eacute;rico", 1);
			//document.Frmgetinfobit.Importe.value = "";
		}
	else
		{
			var b=0;
			a=parseInt(Math.round(document.Frmgetinfobit.Importe.value*100));
			b=a/100;
			document.Frmgetinfobit.Importe.value = b;
			return true;
		}

}
function Validar()
{
	if (!redondea())
	{
		cuadroDialogo("El Importe debe ser num&eacute;rico", 1);
		document.Frmgetinfobit.Importe.value = "";
	}
	else if (!document.Frmgetinfobit.Folio_Registro.value == "")
	{
		checkValue(document.Frmgetinfobit.Folio_Registro);
		document.Frmgetinfobit.Folio_Registro.value = "";
	}
	else
		DoPostInter();
}

function isPositiveInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isPositiveInteger.arguments.length > 1)
        secondArg = isPositiveInteger.arguments[1];

    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s) > 0) ) );
}

function checkValue(myobj)
{
	if(myobj.value != "")
	{
		if(!isPositiveInteger(myobj.value))
		{
			myobj.value = "";
			myobj.focus();
			cuadroDialogo("El valor debe ser num&eacute;rico", 1);
			return false;
		}
		return true;
	}
}

function DoPostInter()
{
	var f1 = document.Frmgetinfobit.fecha1.value;
	var f2 = document.Frmgetinfobit.fecha2.value;
	var folio = document.Frmgetinfobit.Folio_Registro.value;
	var importe = document.Frmgetinfobit.Importe.value;
	var aux1 = "";
	var aux2 = "";
	var resultado = true;

	aux = f1.substring(6,10) + f1.substring(3,5) + f1.substring(0,2);
	aux1 = f2.substring(6,10) + f2.substring(3,5) + f2.substring(0,2);
	if(aux <= aux1)
	{
		document.Frmgetinfobit.submit();
	} else {
		cuadroDialogo("Error\nLa fecha inicial debe ser menor o igual a la fecha final", 1);
	}
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

//-->
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="document.Frmgetinfobit.Registro[0].checked = true;PutDate();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <!-- MENU PRINCIPAL -->
        <%= request.getAttribute("MenuPrincipal") %>
    </TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>

<br>

<form name = "Frmfechas">
  <%= request.getAttribute("Bitfechas") %>
</form>

<FORM NAME="Frmgetinfobit" METHOD = "POST" ACTION="GetAMancomunidadInter">
<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
  <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="620" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat"> Capture los datos para su consulta</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top">
              <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                  <td align="left" width="280">
                    <table width="280" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td align="right" nowrap class="tabmovtex11" width="100">Usuario (opcional): </td>
                        <td class="tabmovtex" nowrap width="185">
                          <SELECT NAME="usuario" class="tabmovtex">
                            <Option value = " ">Seleccione un usuario (opcional)</Option>
				<%= request.getAttribute("Usuarios") %>
                          </SELECT>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">Consulta por:</td>
                        <td class="tabmovtex11" nowrap align="left" valign="middle">
                          <input type = "radio" name = "Registro" value = "'R'" checked>&nbsp;Por Fecha de Registro
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">&nbsp;</td>
                        <td class="tabmovtex11" nowrap valign="middle" align="left">
                          <input type = "radio" name = "Registro" value ="'A'">&nbsp;Por Fecha de Autorizaci&oacute;n<P>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">De la fecha:</td>
                        <td class="tabmovtex" nowrap valign="middle">
                          <Input type = "Text" size=10 name="fecha1" OnFocus = "blur();"  class="tabmovtex">
                            <A href ="javascript:WindowCalendar();">
                              <img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle">
                            </a>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">A la fecha:</td>
                        <td class="tabmovtex" nowrap>
                          <Input type = "Text" size = 10 name = "fecha2" OnFocus = "blur();" class="tabmovtex">
                            <A href ="javascript:WindowCalendar1();"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">Importe:</td>
                        <td class="tabmovtex" nowrap>
                          <INPUT TYPE="Text" NAME="Importe" SIZE="15" maxlength=15  class="tabmovtex" ><!-- onBlur="redondea();" -->
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">Folio de Registro:</td>
                        <td nowrap class="tabmovtex">
                          <INPUT TYPE="Text" NAME="Folio_Registro" SIZE="15" maxlength=15  class="tabmovtex"><!-- onBlur = "checkValue(this);" -->
                        </td>
                      </tr>
                    </table><!-- Tabla 280 -->
                  </td>
                  <td align="left" width="320">
                    <table width="320" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td align="right" class="tabmovtex11" nowrap>Cuenta (opcional):</td>
                        <td class="tabmovtex" colspan="2" valign="middle" align="center">
                          <input type="text" name=textcuenta class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
                            <A HREF="javascript:PresentarCuentas();">
                              <IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle>
                            </A>
                          <input type="hidden" name="cuenta"  value="">
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="3" nowrap><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11" width="89" nowrap>Tipo de operaci&oacute;n:</td>
                        <td width="27" valign="middle" align="center">
                          <INPUT TYPE="CheckBox" value = "'DIPD'," NAME="ChkCambios">
                        </td>
                        <td class="tabmovtex11" width="212" nowrap>Cambios</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'DITA'" NAME="ChkTransInter"></td>
                        <td class="tabmovtex11" nowrap>Transferencias Internacionales</td>
                      </tr>

                      <tr valign="top">
                        <td class="tabmovtex11" nowrap colspan="3">
                          <img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5">
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11" nowrap>Estatus:</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'A'," NAME="ChkAceptadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Autorizadas</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'R'," NAME="ChkRechazadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Rechazadas</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'C','S'," NAME="ChkCanceladas"></td>
                        <td class="tabmovtex11" nowrap>Canceladas</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'P'," NAME="ChkPendientes">
                        </td>
                        <td class="tabmovtex11" nowrap>Pendientes</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'E'," NAME="ChkEjecutadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Ejecutadas</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'N'" NAME="ChkNoEjecutadas">
                        </td>
                        <td class="tabmovtex11" nowrap>No Ejecutadas</td>
                      </tr>
                    </table><!-- Tabla 320 -->
                  </td>
                </tr>
              </table><!-- Tabla 600 -->
            </td>
          </tr>
        </table><!-- Tabla 620 -->
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="90">
              <A href ="javascript:Validar();">
                <input type="image" border="0" name="boton" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar">
              </a>
            </td>
            <td align="left" valign="top" width="76">
              <A href ="javascript:FrmCleanInter();">
                <input type="image" border="0" name="boton" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar">
              </a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
  </form>
</table><!-- Tabla 760 -->
</FORM>
</body>
</html>