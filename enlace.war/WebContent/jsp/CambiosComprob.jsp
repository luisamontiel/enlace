<html>
<head>
<title>Comprobante</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25390">
<meta name="Proyecto" content="Portal Santander">
<meta name="Ultima version" content="07/05/2001 09:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">

<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript" src="/EnlaceMig/Convertidor.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body bgcolor="#ffffff">
<form name="form1" method="post" action="">
<%
    String Sfecha_pago = (String)request.getAttribute("fecha");
    String Sanio_fecha_pago = Sfecha_pago.substring(6,10);
    int Ianio_fecha_pago = Integer.parseInt (Sanio_fecha_pago);
%>
  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
     <%
        if(request.getAttribute("ClaveBanco:")!=null )
        {
          if(Ianio_fecha_pago<2005)
          {
            if(request.getAttribute("ClaveBanco:").equals("014"))
              out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"left\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040.gif\"  BORDER=\"0\"></td>");
            else if(request.getAttribute("ClaveBanco:").equals("003"))
              out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"left\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040s.gif\" BORDER=\"0\"></td>");
          } else {
            out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"left\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040b.gif\" BORDER=\"0\"></td>");
          }
        } else
            out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"left\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040b.gif\" BORDER=\"0\"></td>");
     %>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
   	  <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>
    </tr>
    <tr>
      <td valign="top" class="titenccom">
	  <% if(request.getAttribute("titulo")!=null) out.println(request.getAttribute("titulo"));%>
	  </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
            <td width="428" height="150" valign="top" align="center">
              <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">
                <tr>
                  <td class="tittabcom" align="right" width="90">Contrato:</td>
                  <td class="textabcom" align="left" nowrap>
				  <% if(request.getAttribute("contrato")!=null) out.println(request.getAttribute("contrato"));
		          %>
				  </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Usuario:</td>
                  <td class="textabcom" align="left" nowrap>
				<% String Usuario="";
				   if(request.getAttribute("usuario")!=null)
				  Usuario = (String) request.getAttribute("usuario");
		        %>
		        <script>document.write(cliente_7To8("<%=Usuario%>"));</script>
				 </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Fecha :</td>
                  <td class="textabcom" align="left" nowrap>
				<% if(request.getAttribute("fecha")!=null) out.println(request.getAttribute("fecha"));

				%>
				  &nbsp;&nbsp;&nbsp;
	  <% if(request.getAttribute("hora")!=null) out.println(request.getAttribute("hora"));
	  %>
				  </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">No. de Orden :</td>
                  <td class="textabcom" align="left" nowrap>
				    <%
					if(request.getAttribute("orden")!=null)
						out.println(request.getAttribute("orden"));
					%>
				  </td>
	            </tr>
			    <tr>
                  <td class="tittabcom" align="right" width="0">Referencia :</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("referencia")!=null) out.println(request.getAttribute("referencia"));%>
	            </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Concepto:</td>
                  <td class="textabcom" align="left">
	  <% if(request.getAttribute("concepto")!=null) out.println(request.getAttribute("concepto"));%>
			  </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Cuenta de cargo:</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("cta_origen")!=null) out.println(request.getAttribute("cta_origen"));%>
	  </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Cuenta abono:</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("cta_destino")!=null) out.println(request.getAttribute("cta_destino"));
	  %>

				  </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">RFC Beneficiario:</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("rfc")!=null) out.println(request.getAttribute("rfc"));
	  %>

				  </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Tipo de Cambio:</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("tipocambio")!=null) out.println(request.getAttribute("tipocambio"));
	  %>

				  </td>
                </tr>
                <tr>
	  <% if(request.getAttribute("importemn")!=null) out.println(request.getAttribute("importemn"));
	  %>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Importe USD:</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("importeusd")!=null) out.println(request.getAttribute("importeusd"));%>
	  </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">IVA:</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("iva")!=null) out.println(request.getAttribute("iva"));%>
			  </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Estatus:</td>
                  <td class="textabcom" align="left" nowrap>
	  <% if(request.getAttribute("estatus")!=null) out.println(request.getAttribute("estatus"));%>
			  </td>
                </tr>

              </table>
            </td>
            <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br>
        <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
          <tr>
            <td align="right" width="83">
			<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0"></a>
            </td>
            <td align="left" width="71"><a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0"></a>
            </td>
          </tr>
        </table></td>
    </tr>
  </table>
  <br>
  </form>
</body>
</html>