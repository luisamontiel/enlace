<%--*******************************************************

	** 	Modificaci�n Online de los datos del Empleado	 **
	**													 **
	**  24/10/2007 		Everis M�xico					 **
	*******************************************************	--%>

<html>
<head>
<title>Modificaci&oacute;n en L&iacute;nea de Datos de Empleado</TITLE>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript">


/********************************************************/
/*    Funcion continua - Everis 24/10/2007  		*/
/********************************************************/
function continua()
{
	if(respuesta == 1)
	{
		document.pantallaModif.opcion.value = "4";
		document.pantallaModif.action = "CatalogoNominaEmpl";
		document.pantallaModif.submit();
	}
}
/*********  Fin Funcion continua   ************/


/********************************************************/
/*    Funcion Modificar - Everis 24/10/2007  		*/
/********************************************************/
function Modificar()
{
	var correcto = Valida(document.pantallaModif);
	if(correcto != (true))
	{
		return;
	}
}
/*********  Fin Funcion Modificar   ************/

/********************************************************/
/*    Funcion validaCaracter - Everis 24/10/2007  		*/
/********************************************************/
function validaCaracter(campo, nombre)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;

//forma = document.datosTmp1;
	if(nombre == "CALLE Y NUMERO")
		var caracteresValidos="ABCDEFGHIJKLMN�OPQRSTUVWXYZ.,-0123456789# "
	else
		var caracteresValidos="ABCDEFGHIJKLMN�OPQRSTUVWXYZ.,-0123456789 "

	cadena=campo.value;

    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (caracteresValidos.indexOf(car)==-1) {
			campo.value="";
			campo.focus();
			cuadroDialogo("El campo "+ nombre +" contiene caracteres no v&aacute;lidos", 3);
			return false;
		}
	}
	return true;
}
/*********  Fin Funcion validaCaracter ************/




/********************************************************/
/*    Funcion validaLetras - Everis 24/10/2007  		*/
/********************************************************/
function validaLetras(campo,nombreCampo){
var caracteresValidos="ABCDEFGHIJKLMNOPQRSTUVWXYZ.,- "
cadena=campo.value;

    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (caracteresValidos.indexOf(car)==-1) {
			campo.value="";
			campo.focus();
			cuadroDialogo("El campo "+nombreCampo+" contiene car&aacute;cteres no v&aacute;lidos", 3);
			return false;
		}
	}
	return true;
}
/*********  Fin Funcion validaLetras   ************/


/********************************************************/
/*    Funcion validaCaracter - Everis 24/10/2007  		*/
/********************************************************/
/*function validaCaracter(campo,nombre){
forma = document.datosTmp1;
var caracteresValidos="ABCDEFGHIJKLMNOPQRSTUVWXYZ.,-0123456789 "
cadena=campo.value;

    for (i = 0; i<cadena.length; i++) {
		car=cadena.charAt(i)
		if (caracteresValidos.indexOf(car)==-1) {
			campo.value="";
			campo.focus();
			cuadroDialogo("El campo "+nombre+" contiene caracteres no v&aacute;lidos", 3);
			return false;
		}
	}
	return true;
}*/
/*********  Fin Funcion validaCaracter ************/


/********************************************************/
/*    Funcion esNumero - Everis 24/10/2007  			*/
/********************************************************/
function esNumero(campo, nombreCampo){
	//alert("Entramos esNumero: ");
	tmp = campo.value;
	//	alert("Entramos esNumero: tmp = " + tmp);
	var numeros="0123456789.,";
	if(tmp.length == 0)
		return false;
    for (i = 0; i<tmp.length; i++) {
		car=tmp.charAt(i)
		if (numeros.indexOf(car)==-1) {
			campo.value="";
			campo.focus();
			cuadroDialogo("El campo "+nombreCampo+" contiene car&aacute;cteres no v&#225;lidos.  Ingrese s&Oacute;lo con car&aacute;cteres num&eacute;ricos", 3);
			return false
		}
	}
	/*var ingre = pantallaModif.IngresoFormato.value;
	alert("Entramos esNumero: ingre = " + ingre);
	var comaExiste = ingre.indexOf(",")
	var puntoExiste = ingre.indexOf(".")

	do{
		comaExiste = ingre.indexOf(",")
		puntoExiste = ingre.indexOf(".")
		ingre = ingre.replace(",","")
		alert("Entramos esNumero: do ingre = " + ingre);
		//ingre = ingre.replace(".","")
		//alert("Entramos esNumero: do punto ingre = " + ingre);
	}while( comaExiste != -1 && puntoExiste != -1)

	pantallaModif.Ingreso.value = ingre;*/

	return true
}
/*********  Fin Funcion esNumero ************/


/********************************************************/
/*    Funcion validaIngreso - Everis 24/10/2007  			*/
/********************************************************/
function validaIngreso(campo,nombreCampo){
 cadena=campo.value;
	if (cadena == "0.00" || cadena == "0" || cadena == "0.0") {
		campo.value="";
		campo.focus();
		cuadroDialogo("El campo "+nombreCampo+" debe ser mayor de cero", 3);
		return false;
	}
	return true;
}
/*********  	Fin Funcion validaIngreso	************/


/********************************************************/
/*    Funcion esNumero - Everis 24/10/2007  			*/
/********************************************************/
function formateaIngreso(campo, NombreCampo){
	//alert("Entramos formateaIngreso");
	var cadena="";
	var cadenaDecimales="";
	if(esNumero(campo,NombreCampo)){
			//alert("Entramos formateaIngreso: es Numero");
		// Nuevas Lineas manejor de "," //
		var ingre = campo.value;
		//alert("Entramos formateaIngreso: ingre = " + ingre);
		var comaExiste = ingre.indexOf(",")
//		var puntoExiste = ingre.indexOf(".")

		do{
			comaExiste = ingre.indexOf(",")
			//puntoExiste = ingre.indexOf(".")
			ingre = ingre.replace(",","")
		//	alert("Entramos formateaIngreso: do ingre = " + ingre);
			//ingre = ingre.replace(".","")
			//alert("Entramos esNumero: do punto ingre = " + ingre);
		}while( comaExiste != -1)

		pantallaModif.Ingreso.value = ingre;


//		valor=campo.value;
		valor=ingre;
		//alert("Entramos formateaIngreso: valor = " + valor);
		num= parseFloat(valor);
		//alert("Entramos formateaIngreso: num = " + num);
		cadena=cadena+num;
		posicionPunto=cadena.indexOf('.');
		//alert("Entramos formateaIngreso: num = " + posicionPunto);
		if(posicionPunto>=0){
				//	alert("Entramos formateaIngreso: es >=0");
			cadenaDecimales=cadena.substring(posicionPunto+1);
					//alert("Entramos formateaIngreso: num = " + cadenaDecimales);
			if(cadenaDecimales.length==1){
				cadena=cadena+"0";
						//alert("Entramos formateaIngreso: if cadena = " + cadena);
			}
			else if(cadenaDecimales.length>2)
			{
				cadena=cadena.substring(0, posicionPunto+3);
						//alert("Entramos formateaIngreso: else if cadena = " + cadena);
			}
		}
		else{
			cadena= cadena+".00";
		//	alert("Entramos formateaIngreso: elseee cadena = " + cadena);
			}
	}
		//alert("Entramos formateaIngreso: retiurn cadena = " + cadena);
	return cadena;
}
/*********  	Fin Funcion esNumero	************/


/********************************************************/
/*    Funcion validaCampo - Everis 24/10/2007  			*/
/********************************************************/
function ValidaCampo(campo){
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;
		switch(campo.name){
			case "NoEmpleado" :
				validaCaracter(campo, "N&#250;mero de empleado");
				break;
			case "Depto" :
				validaCaracter(campo,"N&#250;mero de Departamento");
				break;
			case "Ingreso" :
 				if(validaIngreso(campo, "Ingreso mensual")){
 				esNumero(campo,"Ingreso mensual");
				campo.value=formateaIngreso(campo, "Ingreso mensual");}
				break;
		}
}
/*********  	Fin Funcion validaCampo	************/



/********************************************************/
/*    Funcion Valida - Everis 24/10/2007  				*/
/********************************************************/
function Valida(forma){
	flag=false;
	espacios = /^ {1}/;
	/* jp@everis*/
	//alert("Tomo Cambios3");
	pantallaModif.IngresoFormato.value = formateaIngreso(pantallaModif.IngresoFormato,"Sueldo")
	//VSWF ESC 11/06/2008 se quito validacion de departamento no es obligatoria.
	/*if (pantallaModif.Depto.value == "" || pantallaModif.Depto.value.search(espacios) != -1){
		cuadroDialogo("Por favor ingrese el N�mero de departamento",1); pantallaModif.Depto.focus(); return;
		flag=true;
	}else*/
	 if( !validaCaracter(pantallaModif.Depto, "Num. Depto") ){
		flag=true;
	}
	else if (pantallaModif.NoEmpleado.value == "" || pantallaModif.NoEmpleado.value.search(espacios) != -1){
		cuadroDialogo("Por favor ingrese el N�mero de empleado",1); pantallaModif.NoEmpleado.focus(); return;
		flag=true;
	}else if( !validaCaracter(pantallaModif.NoEmpleado, "Num.de Empleado") ){
		flag=true;
	}
	else if (pantallaModif.IngresoFormato.value == "" || pantallaModif.IngresoFormato.value.search(espacios) != -1){
		cuadroDialogo("Por favor introduzca el Ingreso mensual",1); pantallaModif.IngresoFormato.focus(); return;
		flag=true;
	}else if( !validaIngreso(pantallaModif.IngresoFormato, "Sueldo") ){
		flag=true;
	}

	if( tipoCuenta == 'INTERB' ){

		if( !validaCaracter(pantallaModif.calleNumero, "Calle y N&uacute;mero") ){
			flag=true;
		}
		 else if ( !validaCaracter(pantallaModif.colonia, "Colonia") ) {
			flag=true;
		}

		else if ( !validaCaracter(pantallaModif.delegMunicipio, "Delegaci&oacute;n o Municipio") ) {
			flag=true;
		}

		else if ( !validaLetras(pantallaModif.claveEstado, "Clave Estado") ) {
			flag=true;
		}

		else if ( !validaCaracter(pantallaModif.ciudadPob, "Ciudad o Pablaci&oacute;n")  ) {
			flag=true;
		}

		else if ( !esNumero(pantallaModif.codigoPostal, "C&oacute;digo Postal") && (pantallaModif.codigoPostal.value != "") ) {
			flag=true;
		}

		else if ( !validaLetras(pantallaModif.clavePais, "Clave Pa&iacute;s") ) {
			flag=true;
		}

		else if ( !esNumero(pantallaModif.prefTelCasa, "Prefijo del Particular") && (pantallaModif.prefTelCasa.value != "")) {
			flag=true;
		}

		else if ( !esNumero(pantallaModif.telCasa, "Tel. Particular")&& (pantallaModif.telCasa.value != "") ) {
			flag=true;
		}

		else if ( !validaCaracter(pantallaModif.calleNumOfi, "Calle y N�mero") ) {
			flag=true;
		}

		else if ( !validaCaracter(pantallaModif.coloniaOfi, "Colonia") ) {
			flag=true;
		}

		else if ( !validaCaracter(pantallaModif.delegMunOfi, "Delegaci�n o Municipio") ) {
			flag=true;
		}

		 else if ( !validaLetras(pantallaModif.claveEstadoOfi, "Clave Estado") ) {
			flag=true;
		}

		else if ( !validaCaracter(pantallaModif.ciudadPobOfi, "Ciudad o Pablaci�n") ) {
			flag=true;

		}
		 else if ( !esNumero(pantallaModif.codigoPostalOfi, "C�digo Postal") && (pantallaModif.codigoPostalOfi.value != "") ) {
			flag=true;
		}

		 else if ( !validaLetras(pantallaModif.clavePaisOfi, "Clave Pa&iacute;s") ) {
			flag=true;
		}

		  else if ( !esNumero(pantallaModif.claveDireccion, "Clave Direcci&oacute;n") && (pantallaModif.claveDireccion.value != "") ) {
			flag=true;
		}

		else if (pantallaModif.claveDireccion.value == "" || pantallaModif.claveDireccion.value.search(espacios) != -1){
			cuadroDialogo("Por favor ingrese el campo Clave Direcci&oacute;n",1); pantallaModif.claveDireccion.focus(); return;
			flag=true;
		}

		else if ( !esNumero(pantallaModif.prefTelOfi, "Prefijo")&& (pantallaModif.prefTelOfi.value != "") ) {
			flag=true;
		}

		 else if ( !esNumero(pantallaModif.telOfi, "Tel&eacute;fono") && (pantallaModif.telOfi.value != "") ) {
			flag=true;
		}

		 else if ( !esNumero(pantallaModif.extOfi, "Extensi&oacute;n") && (pantallaModif.extOfi.value != "") ) {
			flag=true;
		}
	}

	if(!flag){
		if(cuadroDialogo("&iquest;Est&aacute; seguro de querer modificar los datos de este empleado?<br>",2)){
			return true;
		}else{
			return false;
		}
	}
	/*jp@everis */
}
/*********  	Fin Funcion Valida	************/


/********************************************************/
/*    Funcion acceptNum - Everis 24/10/2007  			*/
/********************************************************/
var nav4 = window.Event ? true : false;
function acceptNum(evt)
	{
	// NOTA: Backspace = 8, Enter = 13, '0' = 48, '9' = 57
		var key = nav4 ? evt.which : evt.keyCode;
		return (key <= 13 || (key >= 48 && key <= 57));
	}
/*********  	Fin Funcion acceptNum	************/


/********************************************************/
/*    Funcion Reset - Everis 24/10/2007  			*/
/********************************************************/
function Reset(){
	/* jp@everis */
	document.pantallaModif.NoEmpleado.value = nEmp;
	document.pantallaModif.Depto.value = nDept;
	document.pantallaModif.CuentaA.value =cuent;
	document.pantallaModif.IngresoFormato.value = Ingr;
	<% System.out.println("Entrando al servidorA");  %>
	if( tipoCuenta == 'INTERB' ){
		/*document.pantallaModif.NoEmpleado.value = nEmp;
		document.pantallaModif.Depto.value = nDept;
		document.pantallaModif.CuentaA.value =cuent;
		document.pantallaModif.IngresoFormato.value = Ingr;*/
		document.pantallaModif.nombre.value = nomb;
		document.pantallaModif.apellidoP.value = apelPat;
		document.pantallaModif.apellidoM.value = apelMat;
		document.pantallaModif.rfc.value = vRfc;
		document.pantallaModif.sexo.value = vSex;
		<% System.out.println("Sexo : " + request.getAttribute("rsexo"));  %>
		<% if (request.getAttribute("rsexo")!=null){
				if (request.getAttribute("rsexo").equals("F") ){ %>
			document.pantallaModif.sexo[0].checked = true;
		<%	} else { %>
			document.pantallaModif.sexo[1].checked = true;
		<% }} %>
		<% System.out.println("Entrando al servidorB");  %>
		document.pantallaModif.calleNumero.value = callNum;
		document.pantallaModif.colonia.value = coloni;
		document.pantallaModif.delegMunicipio.value = delg;
		document.pantallaModif.claveEstado.value = cvEdo;
		document.pantallaModif.ciudadPob.value = ciudPob;
		document.pantallaModif.codigoPostal.value = codPost;
		document.pantallaModif.clavePais.value = cvPais;
		document.pantallaModif.prefTelCasa.value = preTel;
		document.pantallaModif.telCasa.value = telCas;
		document.pantallaModif.calleNumOfi.value = calleOf;
		document.pantallaModif.coloniaOfi.value = coloniaOf;
		document.pantallaModif.delegMunOfi.value = delegOf;
		document.pantallaModif.claveEstadoOfi.value = cvEdoOf;
		document.pantallaModif.ciudadPobOfi.value = ciudadOf;
		document.pantallaModif.codigoPostalOfi.value = codPosOf;
		document.pantallaModif.clavePaisOfi.value = cvPaisOf;
		document.pantallaModif.claveDireccion.value = cvDirecc;
		document.pantallaModif.prefTelOfi.value = prefTelOf;
		document.pantallaModif.telOfi.value = telOf;
		document.pantallaModif.extOfi.value = extOf;
	}
	/* jp@everis */
}
/*********  	Fin Funcion acceptNum	************/


/********************************************************/
/*    Funcion Cancelar - Everis 24/10/2007  			*/
/********************************************************/
function Cancelar()
{
	document.pantallaModif.opcion.value = "1";
	document.pantallaModif.action="CatalogoNominaEmpl";
	document.pantallaModif.submit();
}
/*********  	Fin Funcion acceptNum	************/



/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/



<% if (request.getAttribute("newMenu") != null ) { %>
<%= request.getAttribute("newMenu") %>
<%	} %>



</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<body style="background:'/gifs/EnlaceMig/gfo25010.gif'; background-color: #ffffff;"  leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">
			<!-- MENU PRINCIPAL -->
			<% if (request.getAttribute("MenuPrincipal") != null ) { %>
			<%= request.getAttribute("MenuPrincipal") %>
			<%	} %>
		</TD>
	</TR>
</TABLE>

<% if (request.getAttribute("Encabezado") != null ) { %>
<%= request.getAttribute("Encabezado") %>
<%	} %>

<form  name="pantallaModif" method="POST" action="">
		<input type="hidden" name="opcion" value="">
		<input type="hidden" name="rtipoCuenta" value="<%=request.getAttribute("rtipoCuenta") %>">
		<input type="hidden" name="Ingreso" value="">

<table align=center border=0 cellpadding=0 cellspacing=0 >
	<tr>
  		<td>
  			<!-- jp@everis -->
  			<%if( request.getAttribute("rtipoCuenta").equals("PROPIA") ){ %>
  			<table ALIGN=center BORDER=0 cellpadding=2 cellspacing=3 class='textabdatcla'>
<%--  				<tr>--%>
<%--  					<th colspan=4 class="tittabdat">LOS DATOS DEL EMPLEADO A MODIFICAR</th>--%>
<%--  				</tr>--%>
  				<tr>
  					<td colspan=4 class="tittabdat">&nbsp;<b>Realice las Modificaciones en los Datos del Empleado</b></td>
  				</tr>
  				<tr>
  					<td class='textabdatcla' colspan=4 width='100%'>
  						<table class='textabdatcla' cellspacing=3 cellpadding=2>
  							<tr>
  								<td colspan=2 class="tabmovtex">&nbsp;
  									<input type="hidden" name="Contrato" value="">
  										<tr>
         									<td class="tabmovtex">Cuenta Abono/M&oacute;vil:</td>
         									<td class="tabmovtex"><INPUT class="tittabdat" TYPE="text" SIZE="18" MAXLENGTH="18" NAME="CuentaA"  value="" onfocus="blur();"></td>
         									<td class="tabmovtex">Num. de Departamento:</td>
         									<td class="tabmovtex"><INPUT TYPE="text" NAME="Depto" SIZE="6" MAXLENGTH="6"  value="" onchange="ValidaCampo(this)"></td>
								        </tr>
								        <tr>
								        	<td class="tabmovtex">Num. de empleado:</td>
								        	<td class="tabmovtex"><INPUT TYPE="text" SIZE="7" MAXLENGTH="7" NAME="NoEmpleado"  value="" onchange="ValidaCampo(this)"></td>
								        	<td class="tabmovtex">Ingreso mensual:</td>
								        	<td class="tabmovtex"><INPUT TYPE="text" NAME="IngresoFormato" SIZE="10" MAXLENGTH="20"  value="" onchange="esNumero(pantallaModif.IngresoFormato, 'INGRESO MENSUAL')" ></td>
								       </tr>
								</td>
								<tr>
									<td colspan=2 class="tabmovtex"></td>
								</tr>
								</table>
        				</td>
        			</tr>
        	<%}
        	else{%>
        	<input type="hidden" name="Contrato" value="">
				<table width="1000" border="0" class="textabdatcla">
				<tbody>
					<tr>
						<td class="tittabdat" colspan="6">Realice las Modificaciones en los Datos del Empleado</td>
					</tr>
					<tr>
						<td class="tabmovtex" colspan="6">Los campos con asterisco (<label style="color:#ff0000;font-family: arial;">*</label>) son obligatorios.</td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Num. de Cuenta: </td>
						<td width="50"><input type="text" name="CuentaA" maxlength="18" value="" readonly="readonly" class="tittabdat"></td>
						<td class="tabmovtex" width="50">Num.de Empleado<label style="color:#ff0000;font-family: arial;">*</label>:</td>
						<td width="50"><input type="text" name="NoEmpleado" maxlength="7" size="7" value="" onchange="ValidaCampo(this)"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Num. Depto:</td><td width="50"><input type="text" maxlength="6" name="Depto" size="6" value="" onchange="ValidaCampo(this)"></td>
						<td class="tabmovtex" width="50">Sueldo<label style="color:#ff0000;font-family: arial;">*</label>:</td><td width="50">
						<input type="text" maxlength="20" name="IngresoFormato" value="" onchange="esNumero(pantallaModif.IngresoFormato, 'INGRESO MENSUAL')" ></td>
					</tr>

					<tr>
					<td class="tittabdat" colspan="6">Datos Personales</td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Nombre(s):</td>
						<td width="50"><input type="text" name="nombre" maxlength="30" readonly="readonly" class="tittabdat"></td>
						<td class="tabmovtex" width="50">Apellido Paterno:</td>
						<td width="50"><input type="text" name="apellidoP" maxlength="30" readonly="readonly" class="tittabdat"></td>
						<td class="tabmovtex" width="50">Apellido Materno:</td><td width="50" >
						<input type="text" name="apellidoM" maxlength="30" readonly="readonly" class="tittabdat"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">R.F.C.:</td>
						<td><input type="text" name="rfc" maxlength="13" readonly="readonly" class="tittabdat"></td>
						<td class="tabmovtex" width="50">Sexo<label style="color:#ff0000;font-family: arial;">:</label> </td>
						<td><input type="radio" name="sexo" value="F" checked><font size="1">F</font><input type="radio" value="M" name="sexo"><font size="1">M</td>

					</tr>


					<tr>
						<td class="tabmovtex" width="50">Calle y N&Uacute;mero: </td>
						<td width="50"><input type="text" name="calleNumero" maxlength="60" size="30" onchange="validaCaracter(pantallaModif.calleNumero, 'CALLE Y NUMERO')"></td>
						<td class="tabmovtex" width="50">Colonia:</td>
						<td width="50"><input type="text" maxlength="30" name="colonia" size="30" onchange="validaCaracter(pantallaModif.colonia, 'COLONIA')"></td>
						<td class="tabmovtex" width="50">Delegaci�n o Municipio:</td>
						<td width="50"><input type="text" maxlength="35" name="delegMunicipio" onchange="validaCaracter(pantallaModif.delegMunicipio, 'DELEGACI�N O MUNICIPIO')"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave Estado:</td>
						<td width="50"> <input type="text" name="claveEstado" maxlength="4" onchange="validaCaracter(pantallaModif.claveEstado, 'CLAVE DE ESTADO')"></td>
						<td class="tabmovtex" width="50">Ciudad o Poblaci&oacute;n: </td>
						<td><input type="text" maxlength="35" name="ciudadPob" onchange="validaCaracter(pantallaModif.ciudadPob, 'CIUDAD O POBLACION')"></td>
						<td class="tabmovtex" width="50">C&oacute;digo Postal: </td>
						<td><input type="text" name="codigoPostal" maxlength="5" size="5" onKeyPress="return acceptNum(event)"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave del Pa&iacute;s: </td>
						<td width="50"><input type="text" maxlength="4" name="clavePais" size="4" onchange="validaCaracter(pantallaModif.clavePais, 'CLAVE DE PAIS')"></td>
						<td class="tabmovtex" width="50">Prefijo tel. Particular:</td>
						<td width="50"><input type="text" maxlength="12" name="prefTelCasa" size="12" onKeyPress="return acceptNum(event)"></td>
						<td class="tabmovtex" width="50">Tel. Particular:</td>
						<td><input type="text" maxlength="12" name="telCasa" size="12" onKeyPress="return acceptNum(event)"></td>

					</tr>

					<tr>
						<td class="tittabdat" colspan="6">Informaci�n de Trabajo</td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Calle y N&Uacute;mero:</td>
						<td width="50"><input type="text" maxlength="60" name="calleNumOfi" onchange="validaCaracter(pantallaModif.calleNumOfi, 'CALLE Y N�MERO DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Colonia:</td>
						<td><input type="text" maxlength="30" name="coloniaOfi" onchange="validaCaracter(pantallaModif.coloniaOfi, 'COLONIA DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Delegaci�n o Municipio:</td>
						<td><input type="text" maxlength="35" name="delegMunOfi" onchange="validaCaracter(pantallaModif.delegMunOfi, 'DELEGACI�N O MUNICIPIO DE OFICINA')"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave de Estado:</td>
						<td width="50"><input type="text" name="claveEstadoOfi" maxlength="4" size="4" onchange="validaCaracter(pantallaModif.claveEstadoOfi, 'CLAVE DE ESTADO DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Ciudad o Poblaci&oacute;n:</td>
						<td><input type="text" maxlength="35" name="ciudadPobOfi" onchange="validaCaracter(pantallaModif.ciudadPobOfi, 'CIUDAD O POBLACI�N DE OFICINA')"></td>
						<td class="tabmovtex" width="50">C&oacute;digo Postal:</td>
						<td><input type="text" name="codigoPostalOfi" maxlength="5" size="5" onKeyPress="return acceptNum(event)"></td>
					</tr>
					<tr>
						<td class="tabmovtex" width="50">Clave Pa&Iacute;s:</td>
						<td width="50"><input type="text" maxlength="4" name="clavePaisOfi" size="4" onchange="validaCaracter(pantallaModif.clavePaisOfi, 'CLAVE DE PAIS DE OFICINA')"></td>
						<td class="tabmovtex" width="50">Clave Direcci&oacute;n<label style="color:#ff0000;font-family: arial;">*</label>:</td>
						<td><input type="text" maxlength="7" name="claveDireccion" size="7" onKeyPress="return acceptNum(event)"></td>
						<td class="tabmovtex" width="50">Prefijo:</td>
						<td><input type="text" maxlength="7" name="prefTelOfi" size="7" onKeyPress="return acceptNum(event)"></td>

					</tr>
					<tr>
						<td class="tabmovtex" width="50">Tel&eacute;fono:</td>
						<td><input type="text" maxlength="12" name="telOfi" size="12" onKeyPress="return acceptNum(event)"></td>
						<td class="tabmovtex" width="50">Extensi&oacute;n:</td>
						<td width="50"><input type="text" maxlength="12" name="extOfi" size="12" onKeyPress="return acceptNum(event)"></td>
					</tr>

				</tbody>
			</table>
			<%} %>
<table align=center border=0 cellspacing=0 cellpadding=0>
<tr><br></tr>
<tr>
   	<td><A href = "javascript:Modificar();" ><img src = "/gifs/EnlaceMig/gbo25510.gif"  border=0 alt="Modificar"></a></td>
   	<td><a href="javascript:Reset();"><img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25530.gif"  alt="Reset"/></a></td>
   	<td><a href="javascript:Cancelar();"><img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25190.gif"  alt="Cancelar"/></a></td>
	<td><% if(request.getAttribute("Status")!=null) out.print(request.getAttribute("Status"));%></td>
</tr>
</table>

<script type="text/javascript">


	<% if (request.getAttribute("rcuentaAbono") != null ) { %>
		var cuent = '<%= request.getAttribute("rcuentaAbono").toString().trim() %>';
		document.pantallaModif.CuentaA.value = cuent;
	<%	} else {%>
		var cuent = "";
	<% } %>
	<% if (request.getAttribute("rnoEmpleado") != null ) { %>
		var nEmp = '<%= request.getAttribute("rnoEmpleado").toString().trim() %>';
		document.pantallaModif.NoEmpleado.value = nEmp;
	<%	} else {%>
		var nEmp = "";
	<% } %>
	<% if (request.getAttribute("rdepto") != null ) { %>
		var nDept = '<%= request.getAttribute("rdepto").toString().trim() %>'
		document.pantallaModif.Depto.value = nDept;
	<%	} else {%>
		var nDept = "";
	<% } %>
	<% if (request.getAttribute("ringreso") != null ) { %>
		var Ingr = '<%= request.getAttribute("ringreso").toString().trim() %>';
		document.pantallaModif.IngresoFormato.value = Ingr;
	<%	} else {%>
		var Ingr = "";
	<% } %>
	<% if (request.getAttribute("rnombre") != null ) { %>
		var nomb = '<%= request.getAttribute("rnombre").toString().trim() %>';
		document.pantallaModif.nombre.value = nomb;
	<%	} else {%>
		var nomb = "";
	<% } %>
	<% if (request.getAttribute("rapellPat") != null ) { %>
		var apelPat = '<%= request.getAttribute("rapellPat").toString().trim() %>';
		document.pantallaModif.apellidoP.value = apelPat;
	<%	} else {%>
		var apelPat = "";
	<% } %>
	<% if (request.getAttribute("rapellMat") != null ) { %>
		var apelMat = '<%= request.getAttribute("rapellMat").toString().trim() %>';
		document.pantallaModif.apellidoM.value = apelMat;
	<%	} else {%>
		var apelMat = "";
	<% } %>
	<% if (request.getAttribute("rRFC") != null ) { %>
		var vRfc = '<%= request.getAttribute("rRFC").toString().trim() %>';
		document.pantallaModif.rfc.value = vRfc;
	<%	} else {%>
		var vRfc = "";
	<% } %>
	<% if (request.getAttribute("rsexo") != null ) { %>
		var vSex = '<%= request.getAttribute("rsexo").toString().trim() %>';
		document.pantallaModif.sexo.value = vSex;
		<% if (request.getAttribute("rsexo").equals("F") ){ %>
			document.pantallaModif.sexo[0].checked = true;
		<%	} else { %>
			document.pantallaModif.sexo[1].checked = true;
		<% } %>
	<%	} else {%>
		var vSex = "";
	<% } %>
	<% if (request.getAttribute("rcalleNum") != null ) { %>
		var callNum = '<%= request.getAttribute("rcalleNum").toString().trim() %>';
		document.pantallaModif.calleNumero.value = callNum;
	<%	} else {%>
		var callNum = "";
	<% } %>
	<% if (request.getAttribute("rColonia") != null ) { %>
		var coloni = '<%= request.getAttribute("rColonia").toString().trim() %>';
		document.pantallaModif.colonia.value = coloni;
	<%	} else {%>
		var coloni = "";
	<% } %>
	<% if (request.getAttribute("rdelegMun") != null ) { %>
		var delg = '<%= request.getAttribute("rdelegMun").toString().trim() %>';
		document.pantallaModif.delegMunicipio.value = delg;
	<%	} else {%>
		var delg = "";
	<% } %>
	<% if (request.getAttribute("rcveEdo") != null ) { %>
		var cvEdo = '<%= request.getAttribute("rcveEdo").toString().trim() %>';
		document.pantallaModif.claveEstado.value = cvEdo;
	<%	} else {%>
		var cvEdo = "";
	<% } %>
	<% if (request.getAttribute("rciuPob") != null ) { %>
		var ciudPob = '<%= request.getAttribute("rciuPob").toString().trim() %>';
		document.pantallaModif.ciudadPob.value = ciudPob;
	<%	} else {%>
		var ciudPob = "";
	<% } %>
	<% if (request.getAttribute("rcodPos") != null ) { %>
		var codPost = '<%= request.getAttribute("rcodPos").toString().trim() %>';
		document.pantallaModif.codigoPostal.value = codPost;
	<%	} else {%>
		var codPost = "";
	<% } %>
	<% if (request.getAttribute("rcvePais") != null ) { %>
		var cvPais = '<%= request.getAttribute("rcvePais").toString().trim() %>';
		document.pantallaModif.clavePais.value = cvPais;
	<%	} else {%>
		var cvPais = "";
	<% } %>
	<% if (request.getAttribute("rprefPart") != null ) { %>
		var preTel = '<%= request.getAttribute("rprefPart").toString().trim() %>';
		document.pantallaModif.prefTelCasa.value = preTel;
	<%	} else {%>
		var preTel = "";
	<% } %>
	<% if (request.getAttribute("rnumPart") != null ) { %>
		var telCas = '<%= request.getAttribute("rnumPart").toString().trim() %>';
		document.pantallaModif.telCasa.value = telCas;
	<%	} else {%>
		var telCas = "";
	<% } %>
	<% if (request.getAttribute("rcalleNumOf") != null ) { %>
		var calleOf = '<%= request.getAttribute("rcalleNumOf").toString().trim() %>';
		document.pantallaModif.calleNumOfi.value = calleOf;
	<%	} else {%>
		var calleOf = "";
	<% } %>
	<% if (request.getAttribute("rColoniaOf") != null ) { %>
		var coloniaOf = '<%= request.getAttribute("rColoniaOf").toString().trim() %>';
		document.pantallaModif.coloniaOfi.value = coloniaOf;
	<%	} else {%>
		var coloniaOf = "";
	<% } %>
	<% if (request.getAttribute("rdelegMunOf") != null ) { %>
		var delegOf = '<%= request.getAttribute("rdelegMunOf").toString().trim() %>';
		document.pantallaModif.delegMunOfi.value = delegOf;
	<%	} else {%>
		var delegOf = "";
	<% } %>
	<% if (request.getAttribute("rcveEdoOf") != null ) { %>
		var cvEdoOf = '<%= request.getAttribute("rcveEdoOf").toString().trim() %>';
		document.pantallaModif.claveEstadoOfi.value = cvEdoOf;
	<%	} else {%>
		var cvEdoOf = "";
	<% } %>
	<% if (request.getAttribute("rciuPobOf") != null ) { %>
		var ciudadOf = '<%= request.getAttribute("rciuPobOf").toString().trim() %>';
		document.pantallaModif.ciudadPobOfi.value = ciudadOf;
	<%	} else {%>
		var ciudadOf = "";
	<% } %>
	<% if (request.getAttribute("rcodPosOf") != null ) { %>
		var codPosOf = '<%= request.getAttribute("rcodPosOf").toString().trim() %>';
		document.pantallaModif.codigoPostalOfi.value = codPosOf;
	<%	} else {%>
		var codPosOf = "";
	<% } %>
	<% if (request.getAttribute("rcvePaisOf") != null ) { %>
		var cvPaisOf = '<%= request.getAttribute("rcvePaisOf").toString().trim() %>';
		document.pantallaModif.clavePaisOfi.value = cvPaisOf;
	<%	} else {%>
		var cvPaisOf = "";
	<% } %>
	<% if (request.getAttribute("rDireccOf") != null ) { %>
		var cvDirecc = '<%= request.getAttribute("rDireccOf").toString().trim() %>';
		document.pantallaModif.claveDireccion.value = cvDirecc;
	<%	} else {%>
		var cvDirecc = "";
	<% } %>
	<% if (request.getAttribute("rprefOf") != null ) { %>
		var prefTelOf = '<%= request.getAttribute("rprefOf").toString().trim() %>';
		document.pantallaModif.prefTelOfi.value = prefTelOf;
	<%	} else {%>
		var prefTelOf = "";
	<% } %>
	<% if (request.getAttribute("rnumOf") != null ) { %>
		var telOf = '<%= request.getAttribute("rnumOf").toString().trim() %>';
		document.pantallaModif.telOfi.value = telOf
	<%	} else {%>
		var telOf = "";
	<% } %>
	<% if (request.getAttribute("rextOf") != null ) { %>
		var extOf = '<%= request.getAttribute("rextOf").toString().trim() %>';
		document.pantallaModif.extOfi.value = extOf;
	<%	} else {%>
		var extOf = "";
	<% } %>
	<% if (request.getAttribute("rcontrato") != null ) { %>
		var cntr = '<%= request.getAttribute("rcontrato").toString().trim() %>';
		document.pantallaModif.Contrato.value = cntr
	<%	} else {%>
		var cntr = "";
	<% } %>
	var tipoCuenta = "<%=request.getAttribute("rtipoCuenta")%>";
</script>
<script>
	<% if (request.getAttribute("MensajeLogin01") != null ) { %>
		<%= request.getAttribute("MensajeLogin01") %>
	<%	} %>
</script>

 </form><br></body>
</html>
<!-- 2007.01 -->