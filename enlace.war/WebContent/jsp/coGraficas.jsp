<%@ page import = "java.util.*"%>

<jsp:useBean id = "arch_combos"		class = "mx.altec.enlace.bo.servicioConfirming"	scope="request"/>
<jsp:useBean id = "arch_prov"		class = "mx.altec.enlace.bo.Proveedor"			scope="request"/>

<!-- Recorta el tama�o de la cadena c(Longitud Maxima 30-35) -->
<%! public String corta(String c)
	{
	String aux = "";

	if (c.length()  < 35)
		return c;
	else
		{
		for(int i= 0; i < 30; i++)
			aux+= String.valueOf(c.charAt(i));
		return aux;
		}
	}
%>

<!-- Llena los vectores con los datos de los proveedores-->
<%
	String ArchivoResp = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","","");

	Vector ls_prov = null;
	Vector cv_prov = null;
	Vector cv_trans = null;
	Vector cv_div = null;
	Vector cv_tipo = null;
	Vector cv_origen = null;

	if(ArchivoResp != null)
		{
		ls_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",1);
		cv_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",2);
		cv_trans = arch_prov.coMtoProv_Leer(ArchivoResp,"9",3);
		/*
		* jgarcia - Stefanini
		* Creacion del vector para divisas y para el tipo de proveedor
		*/
		cv_div   = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 5);
		cv_tipo  = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 6);
		}

	if(cv_prov != null)
		{
		int a, b, total = cv_prov.size();
		String strA = null, strB = null;

		for(a=0;a<total-1;a++)
			for(b=a+1;b<total;b++)
				{
				strA = (String)ls_prov.get(a);
				strB = (String)ls_prov.get(b);
				if(strA.compareTo(strB)>0)
					{
					ls_prov.set(a,strB);
					ls_prov.set(b,strA);
					strA = (String)cv_trans.get(a);
					strB = (String)cv_trans.get(b);
					cv_trans.set(a,strB);
					cv_trans.set(b,strA);
					// jgarcia 30/Sept/2009
					// Cambia el orden de las divisas y el tipo de proveedor
					strA = (String)cv_prov.get(a);
					strB = (String)cv_prov.get(b);
					cv_prov.set(a,strB);
					cv_prov.set(b,strA);
					strA = (String)cv_div.get(a);
					strB = (String)cv_div.get(b);
					cv_div.set(a,strB);
					cv_div.set(b,strA);
					strA = (String)cv_tipo.get(a);
					strB = (String)cv_tipo.get(b);
					cv_tipo.set(a,strB);
					cv_tipo.set(b,strA);
					}
				}
		}

%>
<html>

<head>
	<title>Banca Virtual</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta http-equiv="Expires" content="1">
	<meta http-equiv="pragma" content="no-cache">

	<!-- ----------------------------- Secci�n de JavaScript ---------------------------- -->
	<script language = "JavaScript" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>

	<script language = "Javascript" >



	// --- Valida el formulario antes de enviarlo
	function DoPost()
		{
		var fecha1 = "" + document.frmGraficas.fecha1.value;
		var fecha2 = "" + document.frmGraficas.fecha2.value;
		var num1 = parseInt(fecha1.substring(6,10) + fecha1.substring(3,5) + fecha1.substring(0,2));
		var num2 = parseInt(fecha2.substring(6,10) + fecha2.substring(3,5) + fecha2.substring(0,2));
		if(fecha1 == "" || fecha2 == "")
			{
			cuadroDialogo("Debe seleccionar fecha inicial y fecha final",3);
			return;
			}
		if(num1>num2)
			{
			cuadroDialogo("La fecha inicial es mayor a la final",3);
			return;
			}
		if(document.frmGraficas.Todos[1].checked && document.frmGraficas.Proveedor.selectedIndex < 1)
			{
			cuadroDialogo("Se ha seleccionado graficar por proveedor,<BR>pero no se ha seleccionado uno",3);
			return;
			}

//		document.frmGraficas.fecha1.value=document.frmGraficas.FechaDe.options[document.frmGraficas.FechaDe.selectedIndex].value;
//		document.frmGraficas.fecha2.value=document.frmGraficas.FechaA.options[document.frmGraficas.FechaA.selectedIndex].value;
		var num3 = parseInt(document.Frmfechas.strAnio.value + document.Frmfechas.strMes.value + document.Frmfechas.strDia.value);
		if (num2 > num3)
			document.frmGraficas.fecha2.value =
				document.Frmfechas.strDia.value + "/" + document.Frmfechas.strMes.value + "/" + document.Frmfechas.strAnio.value;
//		document.frmGraficas.FechaDe.options[document.frmGraficas.FechaDe.selectedIndex].value;
//		document.frmGraficas.FechaA.options[document.frmGraficas.FechaA.selectedIndex].value;
		document.frmGraficas.submit();
		}

	// ---
	function cambiaOpcion()
		{
		document.frmGraficas.Todos[1].checked = true;
		}

	// ---

	function SeleccionaDivisa(){
		var forma=document.frmGraficas;
   		forma.textCuentas.disabled = false;
		if(forma.Divisa.options[forma.Divisa.selectedIndex].value=="0"){
			forma.CveDivisa.value="";
		}
		if(forma.Divisa.options[forma.Divisa.selectedIndex].value=="1"){
			forma.CveDivisa.value="MXN";
			forma.textCuentas.value="";
		}
		if(forma.Divisa.options[forma.Divisa.selectedIndex].value=="2"){
		   forma.CveDivisa.value="USD";
		   forma.textCuentas.value="";
		}
	}
	function limpiaProv()
		{
		document.frmGraficas.Proveedor.selectedIndex = -1;
		}

	// --- Consulta las Cuentas de Cargo
	function PresentarCuentas(){
  		document.frmGraficas.Divisa.disabled = false;

		msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&cuenta=50","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
   		document.frmGraficas.Divisa.selectedIndex="0";
   		document.frmGraficas.CveDivisa.value="";
   		msg.focus();
   	}

	// --- variables necesarias para los calendarios
	var fecha_completa = "";
	var opcionCalendario = 0;

	// --- esto se ejecuta despu�s de elegir un d�a en el calendario
	function Actualiza(){
		if(opcionCalendario == 1)
			document.frmGraficas.fecha1.value = fecha_completa;
		else
			document.frmGraficas.fecha2.value = fecha_completa;
	}
	function actualizacuenta(){
		document.frmGraficas.Cuentas.value=ctaselec+"|"+ctadescr+"|"+ctatipro+"@";
  		document.frmGraficas.textCuentas.value=ctaselec+" "+ctadescr;
	}

	// --- muestra la ventana de calendario
	function seleccionaFecha(opcion)
		{
		opcionCalendario = opcion;

		var dia = document.Frmfechas.strDia.value;
		var mes = document.Frmfechas.strMes.value-1;
		var ann = document.Frmfechas.strAnio.value;
		var diaI = 1;
		var diaF = dia;
		var mesI = mes - 2;
		var mesF = mes;
		var annI = ann;
		var annF = ann;

		if(mesI<0) {mesI += 12; annI--;}

		var texto = "PagosImpuestos?OPCION=20&diaI=" + diaI +
			"&diaF=" + diaF + "&mesI=" + mesI + "&mesF=" + mesF + "&anioI=" + annI + "&anioF=" +
			annF + "&diasInhabiles=" + escape(js_diasInhabiles) + "&mes=" + mes + "&anio=" +
			ann + "&priUlt=0#mesActual";
		var msg=window.open(texto,"calendario","toolbar=no," +
			"location=no,directories=no,status=no,menubar=no" +
			",scrollbars=yes,resizable=no,width=330,height=260");
		msg.focus();
		}

	var js_diasInhabiles='<%= request.getAttribute("diasInhabiles") %>';

	// --- Secci�n para el men� - (comienza) --------------------------------------------------
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	<%= request.getAttribute("newMenu") %>
	// --- Secci�n para el men� - (termina) --------------------------------------------------

	// --- esto se ejecuta cuando se carga la p�gina
	function inicia()
		{
		PutDate();
		MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
				'/gifs/EnlaceMig/gbo25111.gif',
				'/gifs/EnlaceMig/gbo25151.gif',
				'/gifs/EnlaceMig/gbo25031.gif',
				'/gifs/EnlaceMig/gbo25032.gif',
				'/gifs/EnlaceMig/gbo25051.gif',
				'/gifs/EnlaceMig/gbo25052.gif',
				'/gifs/EnlaceMig/gbo25091.gif',
				'/gifs/EnlaceMig/gbo25092.gif',
				'/gifs/EnlaceMig/gbo25012.gif',
				'/gifs/EnlaceMig/gbo25071.gif',
				'/gifs/EnlaceMig/gbo25072.gif',
				'/gifs/EnlaceMig/gbo25011.gif');
		document.frmConsultCanc.Arch.disabled = true;
		}

	</script>

	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>


<!-- SECCION PRINCIPAL ----------------------------------------------------------------->

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="inicia" background="/gifs/EnlaceMig/gfo25010.gif">

	<!-- MENU PRINCIPAL Y ENCABEZADO -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</table>
	<%= request.getAttribute("Encabezado") %>

	<br><br>

	<!-- Forma para contener hiddens con la fecha -->
	<form name = "Frmfechas">
		<Input type="hidden" name="strAnio" value="<%= request.getAttribute("Anio") %>">
		<Input type="hidden" name="strMes" value ="<%= request.getAttribute("Mes") %>">
		<Input type="hidden" name="strDia" value ="<%= request.getAttribute("Dia") %>">
	</form>

	<FORM NAME="frmGraficas" ACTION="confGrafica" >
	<table width="760" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">

		<!-- tabla principal -->
		<table border="0" cellspacing="2" cellpadding="3">
		<tr><td class="tittabdat">Graficar por:</td></tr>
		<tr align="left"><td class="textabdatcla" valign="top">
			<input type="radio" name="Cant" value="1" checked>N&uacute;mero de documentos<BR>
			<input type="radio" name="Cant" value="0">Importe de los documentos
		</td></tr>
		<tr>
		    <td class='textabdatcla' align="left" >
			  Divisa:
			  <input type=text name=CveDivisa value="" maxlength=5 size=5 onFocus='blur();' class='tabmovtex'>
			  <select NAME="Divisa" class="tabmovtex" onChange="SeleccionaDivisa();">
				<option VALUE="0">Seleccione Divisa</option>
				<option VALUE="1">Moneda Nacional</option>
				<option VALUE="2">D&oacute;lares Americanos</option>
			  </select>
			</td>
	   </tr>
		<tr>
			<td class='textabdatcla'> Cuenta Cargo:
		     <input type="text" name=textCuentas  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="" class='tabmovtex'>
			 <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
			 <input type="hidden" name="Cuentas" value="" >
			</td>
		</tr>

		<tr><td class="tittabdat">Proveedor:</td></tr>
		<tr align="left"><td class="textabdatcla" valign="top">
			<input type="radio" name="Todos" value="1" onclick="limpiaProv()" checked>Todos<BR>
			<input type="radio" name="Todos" value="0">
			<SELECT NAME="Proveedor" class="tabmovtex" onFocus="cambiaOpcion();">
				<OPTION VALUE="0">
				<% if(ls_prov != null) {
				int longitud = ls_prov.size();
				for (int i=0; i<(longitud); i++) {
					String tipo = cv_tipo.elementAt(i).equals("I")?"INT":"NAL"; %>
				<option value="<%=cv_trans.elementAt(i)%>"><%= corta((String)ls_prov.elementAt(i)) + "|" + corta((String)cv_prov.elementAt(i)) + "|" + tipo + "|" + cv_div.elementAt(i) %></option>
				<% } } %>
			</SELECT>
		</td></tr>
		<tr><td class="tittabdat">Periodo:</td></tr>
		<tr>
			<td class="textabdatcla" align="center">De la fecha:&nbsp;
				<Input type = "Text" size=14 name="fecha1" onfocus="blur();" class="tabmovtex">
				<A href ="javascript:seleccionaFecha(1);"><img
					src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0"></a>
			</td>
		</tr>
		<tr>
			<td class="textabdatcla" align="center">A la fecha:&nbsp;
				<Input type = "Text" size=14 name="fecha2" OnFocus="blur();" class="tabmovtex">
				<A href ="javascript:seleccionaFecha(2);"><img
					src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0"></a>
			</td>
		</tr>

		<tr align="center"><td valign="bottom" height="50px">

		<!-- botones -->
			<A href ="javascript:DoPost();"><IMG border="0" src="/gifs/EnlaceMig/gbo25220.gif" alt="Consultar"></A><A
				href ="javascript:document.frmGraficas.reset();"><IMG
				border="0" src="/gifs/EnlaceMig/gbo25250.gif" alt="Limpiar"></A>

		</TD></TR>

		</table>

	</table>
	<INPUT type=Hidden name="tipoReq" value="1">
	</FORM>

</body>

</html>