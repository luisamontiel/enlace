<jsp:useBean id="facultadesBajaCtas" class="java.util.HashMap" scope="session"/>

<%@page import="mx.altec.enlace.utilerias.*"%>

<%--
 @author  Alejandro Rada Vazquez
 @descripcion Modulo de Baja de Cuentas para Enlace Internet
//--%>
<html>
<head>
	<title>Baja de cuentas</title>
<meta http-equiv="Expires" content="1"/>
<meta http-equiv="pragma" content="no-cache"/>

<script language="javaScript" type="text/javascript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<%-- Estilos //--%>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
<style type="text/css">
..componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>


<script type="text/javascript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
function Actualiza(TipoCuenta)
  {
    var tot=0;
    var var1="";

	forma =document.BajaCuentas;

	if(TipoCuenta=="otros")
	 {
        var carTmp=forma.cmbCtasTerceros.value;

		forma.Descripcion.value=carTmp.substring(carTmp.indexOf('|')+1,carTmp.indexOf('@'));
		forma.Banco.value=carTmp.substring(carTmp.indexOf('@')+1,carTmp.indexOf('!'));
		forma.Plaza.value=carTmp.substring(carTmp.indexOf('!')+1,carTmp.indexOf('$'));
		forma.Sucursal.value=carTmp.substring(carTmp.indexOf('$')+1,carTmp.indexOf('~'));
		forma.txtDivisaOtros.value=carTmp.substring(carTmp.indexOf('%')+1,carTmp.indexOf('#'));   //AQUI DEBE MAPEARSE EL TXT DIVISA
	 }
	else
		if(TipoCuenta=="extranjeros")
		 {
    	    forma.txtBancoExt.value=ctatipro;
			forma.txtTitular.value=ctatipre;
            forma.txtPais.value=ctadescr;
	        forma.txtDivisa.value=ctaprod;
	        forma.txtCiudad.value=cfm;
	        if(ctadescr=="ESTADOS UNIDOS"){
                  forma.txtABA.value=ctasubprod;
			}else{
                  forma.txtABA.value=ctaserfi;
			}
		 }
		 else if(TipoCuenta==="moviles"){

		 	var trama = forma.cmbCtasMoviles.value;

		 	var tipoCuenta 					= trama.substring(trama.indexOf('@') + 1, trama.indexOf('!'));
		 	forma.txtTitularMovil.value		= trama.substring(trama.indexOf('|') + 1, trama.indexOf('@'));
		 	forma.txtBancoMovil.value		= trama.substring(trama.indexOf('!') + 1, trama.indexOf('$'));

			if(tipoCuenta === "BANME"){
				forma.txtTipoCuentaMovil.value 	= "Santander";
			}
			else{
				forma.txtTipoCuentaMovil.value  = "Otros bancos Nacionales";
			}

		 }
  }

function Limpiar()
 {
	document.BajaCuentas.reset();
 }


// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			document.getElementById(elemento).value = obtenerCuenta(valor);
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0] + "|" + res[1] + "|";
    }
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES


function Agregar()
 {
   var forma=document.BajaCuentas;
   var valorRadio="";
   var cuenta="";
   var strTramaMB="";
   var strTramaBN="";
   var strTramaBI="";

   for(i2=0;i2<forma.length;i2++)
    {
	  if(forma.elements[i2].type=='radio' && forma.elements[i2].name=='radioTipoCta' && forma.elements[i2].checked==true)
	   {
		 valorRadio=forma.elements[i2].value;
		 if(valorRadio=="0"){
				 var res = $('#comboCuenta0').val().split("\|");
				 if(res[1] === '' && res[2] != ''){
				 	cuadroDialogo("Esta no es una opción valida. Seleccione una cuenta",4);
				 	$("#comboCuenta0").prop('selectedIndex', 0);
				 }else{
	  				$('#textcmbCtasPropiasDescripcion').val(res[2]);
	  				$('#cmbCtasPropias').val(res[0]+"|"+res[2]+"|");
	  				$('#textcmbCtasPropias').val(res[0]);
	  			}	
		}
		if(valorRadio=="1"){
			var res = $('#comboCuenta1').val().split("\|");		
			 //if(res[1] === '' && res[2] != ''){
			 	//cuadroDialogo("Esta no es una opción valida. Seleccione una cuenta",4);
			 	//$("#comboCuenta1").prop('selectedIndex', 0);
			 //}else{
  				$('#cmbCtasTerceros').val(res[0]+"|"+res[2]+"@" + res[4] + "!" + res[7] +"$" + res[8] + "~%"+ "#" + res[9] );
  				$('#textcmbCtasTerceros').val(res[0]);
  				$('#Descripcion').val(res[2]);
  				$('#Banco').val(res[4]);
  				$('#Plaza').val(res[7]);
  				$('#Sucursal').val(res[8]);
  				$('#txtDivisaOtros').val(res[9]);
  			//}
		}
		if(valorRadio=="2"){
			var res = $( '#comboCuenta2' ).val().split("\|");
				 if(res[1] === '' && res[2] != ''){
				 	cuadroDialogo("Esta no es una opción valida. Seleccione una cuenta",4);
				 	$("#comboCuenta2").prop('selectedIndex', 0);
				 }else{
					$('#textcmbCtasExtranjeras').val(res[0]);
					$('#cmbCtasExtranjeras').val(res[0]+"|"+res[2]+"@"+res[6]+"!"+res[4]+"$"+res[3]+"~" + res[9] + "%"+res[5]);  				
	  				$('#txtBancoExt').val(res[4]);
	  				
	  					$('#txtPais').val(res[5]);
	  					$('#txtTitular').val(res[2]);
	  					$('#txtDivisa').val(res[9]);
	  					$('#txtABA').val(res[3]);
	  				$('#txtCiudad').val(res[6]);
			}
		}
		if(valorRadio=="4"){
		var res = $( '#comboCuenta3' ).val().split("\|");
				 if(res[1] === '' && res[2] != ''){
				 	cuadroDialogo("Esta no es una opción valida. Seleccione una cuenta",4);
				 	$("#comboCuenta3").prop('selectedIndex', 0);
				 }else{	 
				   	$('#cmbCtasMoviles').val(res[0]+"|"+res[2]+"@"+res[1] +"!"+res[4]+ "$");
				  	$('#textcmbCtasMoviles').val(res[0]);
				  	$('#txtTitularMovil').val(res[2]);
	  				if (res[1] == 'BANME') {
	  				 $('#txtTipoCuentaMovil').val("Santander");
	  				
	  				} else {
	  				$('#txtTipoCuentaMovil').val("Otros bancos Nacionales");
	  				}
	  				
	  				$('#txtBancoMovil').val(res[4]);
  				}
		}
		 if(
			 (valorRadio=="0" && trimString(forma.textcmbCtasPropias.value)=="") ||
			 (valorRadio=="1" && trimString(forma.textcmbCtasTerceros.value)=="") ||
			 (valorRadio=="2" && trimString(forma.cmbCtasExtranjeras.value)=="") ||
			 (valorRadio=="4" && trimString(forma.cmbCtasMoviles.value)=="")
		   )
		  {
			 cuadroDialogo("Seleccione una cuenta para dar de baja.",3);
			 return ;
		  }
		 else
		  {
			if(valorRadio=="0")
			  {
				cuenta=trimString(forma.textcmbCtasPropias.value);
				strTramaMB=trimString(forma.strTramaMB.value);
				if(strTramaMB.indexOf(cuenta)>=0)
				  {
					cuadroDialogo("La cuenta ya fue seleccionada.",3);
					return ;
				  }
			  }

			if(valorRadio=="1")
			  {
				cuenta=trimString(forma.textcmbCtasTerceros.value);
				strTramaBN=trimString(forma.strTramaBN.value);
				if(strTramaBN.indexOf(cuenta)>=0)
				  {
					cuadroDialogo("La cuenta ya fue seleccionada.",3);
					return ;
				  }
			  }

			if(valorRadio=="2")
			  {
				cuenta=trimString(forma.textcmbCtasExtranjeras.value);
				strTramaBI=trimString(forma.strTramaBI.value);
				if(strTramaBI.indexOf(cuenta)>=0)
				  {
					cuadroDialogo("La cuenta ya fue seleccionada.",3);
					return ;
				  }
			  }
			if(valorRadio == "4"){
		   		cuenta 		= trimString(forma.textcmbCtasMoviles.value);
		   		strTramaBN 	= trimString(forma.strTramaBN.value);
				if(strTramaBN.indexOf(cuenta)>=0){
					cuadroDialogo("La cuenta ya fue seleccionada.",3);
					return ;
			  	}
		 	}

			forma.action="MMC_Baja";
			forma.Modulo.value="1";
			forma.submit();
		  }
	   }
	}
}

function Enviar()
 {
	var forma=document.BajaCuentas;
	var contador=0;

	for(j=0;j<forma.length;j++)
	  if(forma.elements[j].type=='checkbox')
		if(forma.elements[j].checked)
		  contador++;

	if(contador==0)
	 {
	   cuadroDialogo("Debe seleccionar al menos una opci&oacute;n",1);
	 }
	else
	 {
		var cadena="";
		for(x=1;x<forma.length;x++)
		 {
			if(forma.elements[x].type=="checkbox")
			 if(forma.elements[x].checked)
			   cadena+="1";
			 else
			   cadena+="0";
		 }
		forma.cuentasSel.value=cadena;
		forma.Modulo.value="3";
		forma.action="MMC_Baja";
		forma.submit();
	 }

 }

<!-- *********************************************** -->
<!-- modificacion para integracion pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var txtDivisaOtros;
var ctasubprod;
var tramadicional;
var cfm;
var opcion;

function CuentasSantander()
{
  opcion=1;msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");msg.focus();
}
function CuentasOtrosBancos()
{
  opcion=2;msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=2","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");  msg.focus();
}

function CuentasExtranjeras()
{
  opcion=3;msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=3","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");  msg.focus();
}

function CuentasMoviles()
{
  opcion=5;msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=5","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");  msg.focus();
}

function actualizacuenta()
{
  if (opcion==1)
  {
	  document.BajaCuentas.cmbCtasPropias.value=ctaselec+"|"+ctadescr+"|";
      document.BajaCuentas.textcmbCtasPropias.value=ctaselec;
	  document.BajaCuentas.textcmbCtasPropiasDescripcion.value=ctadescr;
  }
  else if(opcion==2)
  {
	  document.BajaCuentas.cmbCtasTerceros.value=ctaselec+"|"+ctadescr+"@"+cfm+"!"+ctatipro +"$"+ctasubprod+"~"+ctaserfi+"%"+ctaprod+"#"+txtDivisaOtros;
      document.BajaCuentas.textcmbCtasTerceros.value=ctaselec;
      Actualiza("otros");
  }
  else if(opcion==3)
  {
      document.BajaCuentas.cmbCtasExtranjeras.value=ctaselec+"|"+ctatipre+"@"+cfm+"!"+ctatipro +"$"+ctasubprod+"~"+ctaprod+"%"+ctadescr;
      document.BajaCuentas.textcmbCtasExtranjeras.value=ctaselec;
	  Actualiza("extranjeros");
  }
  else if(opcion == 5){
  	  document.BajaCuentas.cmbCtasMoviles.value 		= ctaselec+"|"+ctadescr+"@"+ctaserfi+"!"+cfm+"$";
      document.BajaCuentas.textcmbCtasMoviles.value 	= ctaselec;
      Actualiza("moviles");
  }
}



/*************************************************************************************/
<%= request.getAttribute( "newMenu" ) %>
/*************************************************************************************/
</script>

</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
		onLoad="
		inicializaTipoCta();
		consultaCuentasBaja();
		MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif',
		'/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif',
		'/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif',
		'/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif',
		'/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif',
		'/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif',
		'/gifs/EnlaceMig/gbo25011.gif')"
		background="/gifs/EnlaceMig/gfo25010.gif">


	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	    <tr valign="top">
			 <td width="*">
				<%--  MENU PRINCIPAL --%>
				<%= request.getAttribute( "MenuPrincipal" ) %>
			  </td>
		</tr>
	</TABLE>
	<%= request.getAttribute( "Encabezado" ) %>


<form  NAME="BajaCuentas" method="post" action="MMC_Baja">

 <table width="880" border="0" cellpadding="0" cellspacing="0" >
  <tr>
   <td>

     <table width="790"align="center" border="0" cellpadding="0" cellspacing="0" >
	<tr>
			<td>
				<%@ include file="/jsp/filtroConsultaCuentasACCB.jspf" %>
			</td>
		</tr>
   <%
      EIGlobal.mensajePorTrace("MMC_Baja.jsp Iniciando y obteniendo cuentas MB.",EIGlobal.NivelLog.INFO);
      if (((Boolean) facultadesBajaCtas.get("BajaCtasMB")).booleanValue())
       {
   		EIGlobal.mensajePorTrace("MMC_Baja.jsp Se tiene la facultad MB.",EIGlobal.NivelLog.INFO);
   %>
   <%-- ****************************************************************************************************** //-->
   <!-- Cuentas Mismo banco //-->
   <!-- ****************************************************************************************************** //--%>

       <tr>
         <td class="tittabdat">
		   <input TYPE="radio" value="0" NAME="radioTipoCta" checked />&nbsp;&nbsp;<b>Cuentas Mismo Banco</b>
		 </td>
       </tr>
       <tr>
		<td class="textabdatcla">

		 <table border="0" width="100%" class="textabdatcla" cellspacing="3" cellpadding="2">
		   <tr>
			 <td align="right"></td>
			 <td class="tabmovtex11" align="left" >
			 
			
			<select id="" class="tabmovtexbol comboCuenta" disabled>
				<option value="">Seleccione una cuenta...</option>
			</select>
						   
			   <input type="hidden" name="textcmbCtasPropias" id="textcmbCtasPropias" value=""/>
			   <input type="hidden" name="cmbCtasPropias" id="cmbCtasPropias" value=""/>
			 </td>
		   </tr>
		   <tr>
			 <td width="80" class="tabmovtex11" align="right">Descripci&oacute;n: &nbsp; </td>
			 <td class="tabmovtex11"><input type="text" name="textcmbCtasPropiasDescripcion" id="textcmbCtasPropiasDescripcion"  class="componentesHtml" size="35" onfocus="blur();" value=""/></td>
		   </tr>
		   <tr><td colspan="2" class="tabmovtex11" align="right">&nbsp;</td></tr>
		</table>
	   </td>

       <tr><td class="tabmovtex11" align="right">&nbsp;</td></tr>

   <%
       }
       EIGlobal.mensajePorTrace("MMC_Baja.jsp Obteniendo cuentas BN.",EIGlobal.NivelLog.INFO);

      if (((Boolean) facultadesBajaCtas.get("BajaCtasBN")).booleanValue())
       {
   		EIGlobal.mensajePorTrace("MMC_Baja.jsp Se tiene la facultad BN.",EIGlobal.NivelLog.INFO);
   %>
   <%-- ****************************************************************************************************** //-->
   <!-- Cuentas Nacionales //-->
   <!-- ****************************************************************************************************** //--%>

       <tr>
         <td class="tittabdat">
		    <input TYPE="radio" value="1" NAME="radioTipoCta" />&nbsp;<b>Cuentas de Otros Bancos Nacionales</b>
		 </td>
       </tr>

	   <tr>
		<td class="textabdatcla">
		  <table border="0" width="100%" class="textabdatcla" cellspacing="3" cellpadding="2">

		   <tr>
			<td align="right"></td>
			 <td class="tabmovtex11" align="left">
			  			
			
					<select id=""  class="tabmovtexbol comboCuenta" disabled>
						<option value="">Seleccione una cuenta...</option>
					</select>
				
			   <input type="hidden" name="textcmbCtasTerceros" id="textcmbCtasTerceros" value=""/>
			    <input type="hidden" name="cmbCtasTerceros" id="cmbCtasTerceros" value=""/>
			 </td>
		   </tr>

		   <tr>
			<td class="tabmovtex11" align="left"> &nbsp; Descripci&oacute;n: &nbsp; </td>
			<td class="tabmovtex" align="left"><input TYPE="text" class="componentesHtml" SIZE="35" NAME="Descripcion" id="Descripcion"  onFocus="blur();"/></td>
		  </tr>

		  <tr>
			<td class="tabmovtex11" align="left"> &nbsp; Banco: &nbsp; </td>
			<td class="tabmovtex" align="left"><input type="text" class="componentesHtml" size="35" name="Banco" id="Banco" onFocus="blur();"/></td>
		  </tr>

		  <tr>
			<td class="tabmovtex11" align="left"> &nbsp; Plaza: &nbsp; </td>
			<td class="tabmovtex" align="left"><input type="text" class="componentesHtml" size="35" name="Plaza" id="Plaza"  onFocus="blur();"/></td>
		  </tr>

		  <tr>
			<td class="tabmovtex11" align="left"> &nbsp; Sucursal: &nbsp; </td>
			<td class="tabmovtex" align="left"><input TYPE="text" class="componentesHtml" SIZE="10" NAME="Sucursal" id="Sucursal" onFocus="blur();"/></td>
		  </tr>
		  
		  <tr>
			<td class="tabmovtex11" align="left"> &nbsp; Clave Divisa: &nbsp; </td>
			<td class="tabmovtex" align="left"><input TYPE="text" class="componentesHtml" SIZE="3" NAME="txtDivisaOtros" id="txtDivisaOtros" onFocus="blur();"/></td>
		  </tr>
		  
		  <tr><td colspan="2" class="tabmovtex11" align="right">&nbsp;</td></tr>

		</table>
	   </td>
	  </tr>

      <tr><td class="tabmovtex11" align="right">&nbsp;</td></tr>

   <%
       }
       EIGlobal.mensajePorTrace("MMC_Baja.jsp Obteniendo cuentas BI.",EIGlobal.NivelLog.INFO);

//      if (((Boolean) facultadesBajaCtas.get("BajaCtasBI")).booleanValue())
  //     {
   		EIGlobal.mensajePorTrace("MMC_Baja.jsp Se tiene la facultad BI.",EIGlobal.NivelLog.INFO);
   %>
   <%-- ****************************************************************************************************** //-->
   <!-- Cuentas Internacionales //-->
   <!-- ****************************************************************************************************** //--%>

       	<tr>
		  <td class="tittabdat">
			<input TYPE="radio" value="2" NAME="radioTipoCta"/><b>Cuentas de Otros Bancos Internacionales</b>
       	  </td>
	    </tr>

   	   <tr>
		<td class="textabdatcla">
		  <table border="0" width="100%" class="textabdatcla" cellspacing="3" cellpadding="2">

		  <tr>
			<td align="right"></td>		
			 <td class="tabmovtex11" align="left" colspan="3">	
				 
				
					<select id=""  class="tabmovtexbol comboCuenta" disabled>
						<option value="">Seleccione una cuenta...</option>
					</select>
				
				
			  <input type="hidden" name="textcmbCtasExtranjeras" id="textcmbCtasExtranjeras" value=""/>
			  <input type="hidden" name="cmbCtasExtranjeras" id="cmbCtasExtranjeras" value=""/>
			
		</td>
		  </tr>
		  
		  <tr>
			<td class="tabmovtex11"  align="right">Banco: &nbsp; </td>
			<td class="tabmovtex"  align="left">
			  <input class="componentesHtml" TYPE="text" SIZE="35" NAME="txtBancoExt" id="txtBancoExt" onFocus="blur();"/>
			</td>
			<td class="tabmovtex11"  align="right">Pa&iacute;s: &nbsp; </td>
			<td class="tabmovtex"  align="left">
			  <input class="componentesHtml" TYPE="text" SIZE="25" NAME="txtPais"   id="txtPais" onFocus="blur();"/>
			</td>
		  </tr>
		  <tr>
			<td class="tabmovtex11"  align="right">Beneficiario:  &nbsp; </td>
			<td class="tabmovtex"  align="left">
				<input class="componentesHtml" TYPE="text" SIZE="35" NAME="txtTitular" id="txtTitular" onFocus="blur();"/>
			</td>
			<td class="tabmovtex11"  align="right">Divisa:  &nbsp; </td>
			<td class="tabmovtex"  align="left">
			  <input  class="componentesHtml" TYPE="text" SIZE="25" NAME="txtDivisa" id="txtDivisa" onFocus="blur();"/>
			</td>
		  </tr>
		  <tr>
			<td class="tabmovtex11"  align="right">Ciudad: &nbsp; </td>
			<td class="tabmovtex"  align="left">
			  <input class="componentesHtml" TYPE="text" SIZE="35" NAME="txtCiudad" id="txtCiudad" onFocus="blur();"/>
			</td>
			<td class="tabmovtex11"  align="right">Clave ABA/SWIFT:</td>
			<td class="tabmovtex"  align="left">
			  <input class="componentesHtml" TYPE="text" SIZE="25" NAME="txtABA" id="txtABA" onFocus="blur();"/>
			</td>
		  </tr>
		  <tr>
			<td colspan="4" class="tabmovtex11" align="right">&nbsp;</td>
		  </tr>

		</table>
	   </td>
	 </tr>
	 <tr><td class="tabmovtex11" align="right">&nbsp;</td></tr>
	 <%
	  	EIGlobal.mensajePorTrace("MMC_Baja.jsp Iniciando y obteniendo cuentas NM.",EIGlobal.NivelLog.INFO);
      	if (
      		((Boolean) facultadesBajaCtas.get("BajaCtasMB")).booleanValue() &&
      		((Boolean) facultadesBajaCtas.get("BajaCtasBN")).booleanValue()
      		){
   			EIGlobal.mensajePorTrace("MMC_Baja.jsp Se tiene la facultad para NM.",EIGlobal.NivelLog.INFO);
	  %>


	  <tr>
    		<td colspan="4" class="tittabdat">
    		<input TYPE="radio" value="4" NAME="radioTipoCta"/> <b>N&uacute;meros M&oacute;viles Moneda Nacional </b></td>
    	</tr>
    	<tr>    	
            <td class="textabdatcla" colspan="4" width="100%" align="left">
                
	        <table border="0" width="100%" class="textabdatcla" cellspacing="3" cellpadding="2">
				<tr>

	                     <td align="right"></td>		
			            <td class="tabmovtex11" align="left" >	
					   
						
							<select id="" class="tabmovtexbol comboCuenta" disabled>
								<option value="">Seleccione una cuenta...</option>
							</select>
					
					
			   			<input type="hidden" name="cmbCtasMoviles" id="cmbCtasMoviles" value=""/>
			   			<input type="hidden" name="textcmbCtasMoviles" id="textcmbCtasMoviles" value=""/>
			   		</td>
				</tr>
				<tr>
					
					<td class="tabmovtex11" >Tipo de cuenta:</td>
					<td class="tabmovtex">
						<input type="text" NAME="txtTipoCuentaMovil" id="txtTipoCuentaMovil" size="35" class="componentesHtml" onfocus="blur();" />
					</td>
				</tr>
				<tr>
					
					<td class="tabmovtex11">Nombre del Titular:</td>
					<td class="tabmovtex"><input TYPE="text" class="componentesHtml" SIZE="35" NAME="txtTitularMovil"  id="txtTitularMovil" onfocus="blur();" /></td>
				</tr>
				<tr>
					
					<td class="tabmovtex11">Banco:</td>
					<td class="tabmovtex"><input TYPE="text" class="componentesHtml" SIZE=35 NAME="txtBancoMovil" id="txtBancoMovil" onfocus="blur();" /></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
		 <td class="tabmovtex" bgcolor="white" colspan="4"><br/></td>
		</tr>
	  <%} %>
   <%
       //}
       /**GAE**/
       EIGlobal.mensajePorTrace("MMC_Baja.jsp Obteniendo cuentas por lote de carga.",EIGlobal.NivelLog.INFO);
   	//revisar facultad
   	//if (((Boolean) facultadesBajaCtas.get("BajaCtasIA")).booleanValue()){
   		//EIGlobal.mensajePorTrace("MMC_Baja.jsp Se tiene la facultad de baja de cuentas por lote de carga.",4);
   %>
	   <%-- ****************************************************************************************************** //-->
	   <!-- 		 //GAE 	Baja Masiva de Cuentas //-->
	   <!-- ****************************************************************************************************** //--%>
	   <tr>
	   	   <td class="tittabdat">
		   	    <input TYPE="radio" value="3" NAME="radioTipoCta"/><b>Baja de Cuentas Masiva</b>
       	   </td>
	   </tr>
   	   <tr>
	       <td class="textabdatcla">
		   	    <table border="0" width="100%" class="textabdatcla" cellspacing="3" cellpadding="2">
		        	<tr>
			        	<td width="80" class="tabmovtex11" align="right">
			        		Folio de lote: &nbsp;
			        	</td>
					    <td>
			  				<input type="text" name="folioBajaLote"  id="folioBajaLote" class="componentesHtml" size="22" value=""/>
						</td>
						<td colspan="2" class="tabmovtex11" align="right">
							&nbsp;
						</td>
		  			</tr>
		  			<tr>
						<td colspan="4" class="tabmovtex11" align="right">
							&nbsp;
						</td>
		  			</tr>
	  			</tr>
			</table>
   		</td>
 	</tr>

   <%
       /**FIN GAE**/
       //}
       EIGlobal.mensajePorTrace("MMC_Baja.jsp Finalizando pantalla.",EIGlobal.NivelLog.INFO);
   %>
   </table>

	<tr><td colspan=4 class="tabmovtex11" align="right">&nbsp;</td></tr>

   </td>
  </tr>

  	<tr>
	 <td align="center">
	   <a href = "javascript:Agregar();" border = "0"><img src = "/gifs/EnlaceMig/gbo25290.gif" border="0"/></a><a href = "javascript:Limpiar();" border = "0"><img src = "/gifs/EnlaceMig/gbo25250.gif" border="0" /></a>
	 </td>
	</tr>

 </table>

<%
  String Modulo="0";

  String strTramaMB="";
  String strTramaBN="";
  String strTramaBI="";

  String tablaMB="";
  String tablaBN="";
  String tablaBI="";

  /**GAE**/
    String tablaLotes="";
    String errorLote = null;
  /**FIN GAE**/

  if(request.getAttribute("Modulo")!=null)
    Modulo=(String)request.getAttribute("Modulo");
  if(request.getAttribute("strTramaMB")!=null)
    strTramaMB=(String)request.getAttribute("strTramaMB");
  if(request.getAttribute("strTramaBN")!=null)
    strTramaBN=(String)request.getAttribute("strTramaBN");
  if(request.getAttribute("strTramaBI")!=null)
    strTramaBI=(String)request.getAttribute("strTramaBI");

  if(request.getAttribute("tablaMB")!=null && !request.getAttribute("tablaMB").equals(""))
    tablaMB=(String)request.getAttribute("tablaMB")+"<br><br>";

  if(request.getAttribute("tablaBN")!=null && !request.getAttribute("tablaBN").equals(""))
    tablaBN=(String)request.getAttribute("tablaBN")+"<br><br>";

  if(request.getAttribute("tablaBI")!=null && !request.getAttribute("tablaBI").equals(""))
    tablaBI=(String)request.getAttribute("tablaBI")+"<br><br>";

  /**GAE**/
  if(request.getAttribute("tablaLotes")!=null && !request.getAttribute("tablaLotes").equals(""))
  	tablaLotes=(String)request.getAttribute("tablaLotes")+"<br><br>";

  if(request.getAttribute("errorLote")!=null && !request.getAttribute("errorLote").equals("")){
  	errorLote = (String)request.getAttribute("errorLote");
  }else{
	errorLote = "";
  }
  /**FIN GAE**/
 %>

<br/>
<table width="760" border="0" cellpadding="0" cellspacing="0" >
 <tr>
  <td width="100%"><%=tablaMB%></td>
 </tr>
</table>

<table width="760" border="0" cellpadding="0" cellspacing="0" >
 <tr>
  <td width="100%"><%=tablaBN%></td>
 </tr>
</table>

<table width="760" border="0" cellpadding="0" cellspacing="0" >
 <tr>
  <td width="100%"><%=tablaBI%></td>
 </tr>
</table>

<%-- GAE --%>
<%if(!errorLote.equals("")){ %>
<script type="text/javascript">
	cuadroDialogo("<%=errorLote%>", 3);
</script>
<%}%>
<%if(!tablaLotes.equals("")){ %>
<table width="760" border="0" cellpadding="0" cellspacing="0" >
 <tr>
  <td width="100%"><%=tablaLotes%></td>
 </tr>
</table>
<%}%>
<%-- FIN GAE --%>

 <input type="hidden" name="Modulo" value="<%=Modulo%>"/>
 <input type="hidden" name="strTramaMB" value="<%=strTramaMB%>"/>
 <input type="hidden" name="strTramaBN" value="<%=strTramaBN%>"/>
 <input type="hidden" name="strTramaBI" value="<%=strTramaBI%>"/>
 <input type="hidden" name="cuentasSel" />

 <%if(Modulo.trim().equals("1") && (!strTramaMB.equals("") || !strTramaBN.equals("") || !strTramaBI.equals("") || errorLote.equals("")))
  {
 %>
 <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td align="center"><a href = "javascript:Enviar();" border = "0"><img src = "/gifs/EnlaceMig/gbo25520.gif" border="0" alt="Enviar"/></a></td>
    </tr>
  </table>
  <%
   }
  %>

</form>

</body>
</html>