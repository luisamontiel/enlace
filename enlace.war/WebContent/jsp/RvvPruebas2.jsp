<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="EnlaceMig.*" %>

<%
boolean esConsulta = request.getParameter("Tuxedo") != null;
String archivo = request.getParameter("Archivo"); if(archivo == null) archivo = "";
String portapapeles = request.getParameter("PortaPapeles"); if(portapapeles == null) portapapeles = "";
String contenido = "";

if(esConsulta)
	{
	try
		{
		BufferedReader entrada = new BufferedReader(new FileReader(archivo));
		String linea = null;
		StringBuffer buffer = new StringBuffer("");
		while((linea=entrada.readLine())!=null) buffer.append(linea + "\n");
		contenido = buffer.toString();
		}
	catch(Exception e)
		{contenido = "" + e;}

	}
%>

<HTML>

<HEAD>
	<TITLE>Pruebas con tuxedo</TITLE>
</HEAD>

<BODY>

	<H1>Pruebas con Tuxedo</H1>

	<H2>Archivo para abrir</H2>

	<FORM name="form" method="POST" action="RvvPruebas2.jsp">
	<INPUT name="Tuxedo" type="Hidden" value="consultando...">
	<INPUT name="Archivo" type="text" size="100"><BR>
	<INPUT type="Submit" value="enviar trama">


	<% if(esConsulta) { %>
	<HR>
	<H2>Resultado de la &uacute;ltima consulta</H2>

	<P><B>Nombre del archivo:</B></P>
	<PRE><%= archivo %></PRE>
	<P><B>Contenido del archivo:</B></P>
	<PRE><%= contenido %></PRE>
	<% } %>

	<HR>
	<H2>Portapapeles</H2>
	<TEXTAREA name="PortaPapeles" cols="100" rows="10"><%= portapapeles %></TEXTAREA>

	</FORM>

</BODY>

<HTML>