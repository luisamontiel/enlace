<script language="javascript" src="/EnlaceMig/hashtable.js"></script>
<script language="javascript" src="/EnlaceMig/pm_fp.js"></script>
<%!

// Populate GOTOURL_BASE with the page that you want to redirect to
// If you don't want to redirect, then leave this blank
final static String GOTOURL_BASE = "";
// Populate NOFLASH_BASE with the page you want to redirect to if the user
// does not have flash installed
final static String NOFLASH_BASE = "";
// Populate SENDTOURL_BASE with the servlet that you want to send the PMDATA
// token to.  If you are using the test harness, set this to TestServlet
final static String SENDTOURL_BASE="/jsp/rsa/FSOServlet.jsp";

%>
<%@ page import="java.net.URLEncoder"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<%
		   String gotoUrl = GOTOURL_BASE;
		   String gotoUrlEnc = URLEncoder.encode(gotoUrl, "UTF-8");		   
			   String sendUrl = request.getContextPath() + SENDTOURL_BASE;
			   String sendUrlEnc = URLEncoder.encode(sendUrl, "UTF-8");
		   String noFlash = NOFLASH_BASE;
		%>

		<meta http-equiv="expires" content="0">
		<meta http-equiv="Pragma" content="no-cache">

		<script language="JavaScript" type="text/javascript">
	    <!--
		  var fpString = "";
		  var andString = "";
		  //if (top != self) top.location.href = location.href;
		  
		  function navigateToUrl(url) {
			 window.location.href = url;
		  }
	    //-->
	    </script>
		<script language="javascript1.1">
	    <!--
		  function navigateToUrl(url) {
			 window.location.replace(url);
		  }
	   //-->
	   </script>
	   <script>
	   <!--
	   		BrowserDetect.init();
	   	-->
	   </script>
	</head>
	<body id="body" bgcolor="#FFFFFF" marginheight=0 marginwidth=0
		topmargin=0>
		
		<script src="/EnlaceMig/AC_OETags.js" language="javascript"></script>
		<script language="JavaScript" type="text/javascript">
    	<!--
    		// -----------------------------------------------------------------------------
    		// Globals
    		// Major version of Flash required
    		var requiredMajorVersion = 6;
    		// Minor version of Flash required
    		var requiredMinorVersion = 0;
    		// Minor version of Flash required
    		var requiredRevision = 0;
    		// -----------------------------------------------------------------------------
    	// -->
	   </script>
		<script language="JavaScript" type="text/javascript">
		<!--
		// Version check based upon the values defined in globals
		var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
		-->
		</script>
		
		<script language="JavaScript" type="text/javascript">
		<!--
		if (hasReqestedVersion)
			{
				var out = "";
				out = out + "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'" + "\n";
				out = out + "width='1' height='1'>" + "\n";
				out = out + "<param name='movie' value='/EnlaceMig/pmfso.swf'>" + "\n";
				out = out + "<param name='quality' value='high'>" + "\n";
				out = out + "<param name='bgcolor' value=#FFFFFF>" + "\n";
				out = out + "<param name='FlashVars' value='gotoUrl=<%=gotoUrlEnc%>&sendUrl=<%=sendUrlEnc%>&browserType=" + BrowserDetect.browser + "'>" + "\n";
				out = out + "<embed src='/EnlaceMig/pmfso.swf'" + "\n";
				out = out + "FlashVars='gotoUrl=<%=gotoUrlEnc%>&sendUrl=<%=sendUrlEnc%>&browserType=" + BrowserDetect.browser + "'" + "\n";
				out = out + "quality='high' bgcolor='#FFFFFF' width='1' height='1'" + "\n";
				out = out + "type='application/x-shockwave-flash'>" + "\n";
				out = out + "<noembed>" + "\n";
				out = out + "<script>" + "\n";
				out = out + "navigateToUrl('<%=gotoUrl%>');" + "\n";
				out = out + "</script>" + "\n"; 
                                out = out + " </noembed>" + "\n"; 
				out = out + "<noobject></noobject>" + "\n"; 
				out = out + "</embed>" + "\n"; 
				out = out + "<noobject></noobject>" + "\n"; 
				out = out + "</object>" + "\n"; 
				document.write(out); 
			} 
		else 
			{ 
			} 
		//-->
		</script>
		<noscript>
    		<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
    			width='1' height='1'>
    			<param name='movie' value='pmfso.swf'>
    			<param name='quality' value='high'>
    			<param name='bgcolor' value=#FFFFFF>
    			<param name='FlashVars'
    				value='gotoUrl=<%=gotoUrlEnc%>&sendUrl=<%=sendUrlEnc%>'>
    			<embed src='/EnlaceMig/pmfso.swf'
    				FlashVars='gotoUrl=<%=gotoUrlEnc%>&sendUrl=<%=sendUrlEnc%>'
    				quality='high' bgcolor='#FFFFFF' width='1' height='1'
    				type='application/x-shockwave-flash'>
    				<noembed></noembed>
    				<noobject></noobject>
    			</embed>
    			<noobject></noobject>
    		</object>
		</noscript>
	</body>
</html>
