<%@ page import="mx.altec.enlace.bo.*" %>
<%@ page import="java.util.*" %>

<%
String parMes = (String)request.getParameter("mes");
String parAnn = (String)request.getParameter("anio");
String parDiaI = (String)request.getParameter("diaI");
String parMesI = (String)request.getParameter("mesI");
String parAnnI = (String)request.getParameter("anioI");
String parDiaF = (String)request.getParameter("diaF");
String parMesF = (String)request.getParameter("mesF");
String parAnnF = (String)request.getParameter("anioF");
String parInhabiles = (String)request.getParameter("diasInhabiles");
String parPriUlt = (String)request.getParameter("priUlt");

if(parMes == null) parMes = (String)request.getAttribute("mes");
if(parAnn == null) parAnn = (String)request.getAttribute("anio");
if(parDiaI == null) parDiaI = (String)request.getAttribute("diaI");
if(parMesI == null) parMesI = (String)request.getAttribute("mesI");
if(parAnnI == null) parAnnI = (String)request.getAttribute("anioI");
if(parDiaF == null) parDiaF = (String)request.getAttribute("diaF");
if(parMesF == null) parMesF = (String)request.getAttribute("mesF");
if(parAnnF == null) parAnnF = (String)request.getAttribute("anioF");
if(parInhabiles == null) parInhabiles = (String)request.getAttribute("diasInhabiles");
if(parPriUlt == null) parPriUlt = (String)request.getAttribute("priUlt");

if(parMes == null) parMes = "";
if(parAnn == null) parAnn = "";
if(parDiaI == null) parDiaI = "";
if(parMesI == null) parMesI = "";
if(parAnnI == null) parAnnI = "";
if(parDiaF == null) parDiaF = "";
if(parMesF == null) parMesF = "";
if(parAnnF == null) parAnnF = "";
if(parInhabiles == null) parInhabiles = "";
if(parPriUlt == null) parPriUlt = "";

/*
StringBuffer bufferInhabiles = new StringBuffer(parInhabiles);
for(int a=parInhabiles.length()-4;a>0;a-=8) bufferInhabiles.insert(a,"/");
for(int a=parInhabiles.length()-7;a>0;a-=9) bufferInhabiles.insert(a,"/");
for(int a=parInhabiles.length()-10;a>0;a-=10) bufferInhabiles.insert(a,", ");
parInhabiles = bufferInhabiles.toString();
*/
%>

<!--
parInhabiles = <%= parInhabiles %>
<% System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>X parInhabiles:\n" + parInhabiles); %>
-->

<HTML>

<HEAD>

	<TITLE>Calendario</TITLE>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
	<SCRIPT>

	// -----------------------------------------------------------------------
	function dayClick(dia,mes,anio)
		{
		var lafecha="";

		if(parseInt(dia)<=9)
			{lafecha = lafecha + "0" + dia + "/";}
		else
			{lafecha = lafecha + dia + "/";}

		if(parseInt(mes+1)<=9)
			{lafecha = lafecha + "0" +(mes+1) + "/";}
		else
			{lafecha = lafecha + parseInt(mes+1) + "/";}

		lafecha =  lafecha + anio;

		opener.fecha_completa=lafecha;
		opener.Actualiza();
		window.close();
		}

	</SCRIPT>

</HEAD>

<BODY bgcolor="white">


	<table width="300" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="152"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace Internet"></td>
		<!--<td align="right" valign="bottom"><img src="/gifs/EnlaceInternet/glo25040.gif" border=0></td> -->
	</tr>
	</table>

	<%
			try
			{
			boolean habPriUlt = parPriUlt.equals("1");
			GregorianCalendar inicio = new GregorianCalendar(Integer.parseInt(parAnnI),Integer.parseInt(parMesI),Integer.parseInt(parDiaI));
			GregorianCalendar fin = new GregorianCalendar(Integer.parseInt(parAnnF),Integer.parseInt(parMesF),Integer.parseInt(parDiaF));
			mx.altec.enlace.utilerias.CalendarioHTML x = new mx.altec.enlace.utilerias.CalendarioHTML(inicio,fin,habPriUlt);
			int mes = Integer.parseInt(parMes), anio = Integer.parseInt(parAnn);
			x.setEstiloDiaLaboral("tabtexcal");
			x.setEstiloDiaNoLaboral("tabtexcal2");
			x.setEstiloSabadoDomingo("tabtexcal2");
			x.setInhabiles(parInhabiles);
			x.setAncla(mes,anio);
			out.println(x.generaCodigo());
			}
		catch(Exception e)
			{
			out.println("<H1>Error</H1><P>El error fu&eacute;:</P><P>" + e + "</P>");
			}
	%>

	<table width="300" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center"><a href="javascript:self.close();"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0" alt="Cerrar"></a>
		</td>
	</tr>
	</table>

</BODY>

</HTML>
