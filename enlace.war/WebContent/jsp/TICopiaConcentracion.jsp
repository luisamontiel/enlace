<html>
<head>
	<title>Tesorer&iacute;a Inteligente - Copia de Estructuras</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

function Copiar()
{
  var forma=document.Datos;

  forma.Modulo.value="9";
  forma.submit();
}

function Regresar()
{
  var forma=document.Datos;

  forma.Modulo.value='1';
  forma.submit();
}


/******************  ********* ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/

<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
%>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" >

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }
%>


<FORM  NAME="Datos" method=post action="TIConcentracion">

	<TABLE width=250px class="textabdatcla" border=0 cellspacing=0 align=center>
	 <TR>
	  <TD class="tittabdat">&nbsp;Seleccione la estructura que desea copiar	</TD>
	 </TR>
	 <TR>
	  <TD class='tabmovtex' align=center>
	  	<TABLE width=150>
		 <TR>
		  <TD class='tabmovtex'>
		   <BR><INPUT type=Radio name="Sel" value="D" checked>&nbsp;Dispersi&oacute;n
		   <BR><BR>
		   <INPUT type=Radio name="Sel" value="F">&nbsp;Fondeo autom&aacute;tico
		   <BR><BR>
		  </TD>
		 </TR>
		</TABLE>
	   </TD>
	 </TR>
	</TABLE>

	<input type=hidden name=Modulo value=0>
	<input type=hidden name=Trama value=''>
	<input type=hidden name=Alta value="<%= request.getAttribute("Alta") %>" size=5>
	<input type=hidden name=Modificar value="<%= request.getAttribute("Modificar") %>" size=5>
	<input type=hidden name=TramasCuentas value="<%= request.getAttribute("TramasCuentas") %>">

	<BR>
	<center><A href="javascript:Copiar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25545.gif" alt="Copiar"></A><A href="javascript:Regresar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></A></center>
	<BR>
 </FORM>

</BODY>

</HTML>
