<!--
Nombre del formulario: "Forma"

Campos:
"Cuenta"			Lista		Cuentas pertenecientes al contrato
//"Periodicidad"	Radio	Valores posibles: "Diaria", "Semanal"
//"PerHoras"		Lista		Horas de 8:00 a 18:00
//"PerDias"			Lista		D"as de "Lunes" a "Viernes"
//"TipoDis"			Radio		Valores posibles: "LoConcentrado", "LoEspecificado"
"Dispersion"		Texto		Valor de moneda
"Archivo"			File		Archivo a importar
"CtaSel"			Radio		Selecciona una sola cuenta
"Accion"			Hidden		Indica la accion a tomar (alta, baja...)
"Trama"				Hidden		Lleva informacion al servlet (depende de la accion)
"Cuentas"			Hidden		Lleva informacion de todo el arbol mientras no se guarde

-->
<%@page import="mx.altec.enlace.bo.bc_CuentaProgFechas"%>
<%@page import="java.util.Vector"%>
	<%@ page import="java.util.StringTokenizer" %>
	<%@ page import="java.lang.Number" %>


	<%
			// --- Obtencion de parametros -----------------------------------------------------
			String accion = (String)request.getAttribute("Accion");
			String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
			String parFuncionesMenu = (String)request.getAttribute("newMenu");
			String parEncabezado = (String)request.getAttribute("Encabezado");
			String parCuentas = (String)request.getAttribute("Cuentas");
			String parMensaje = (String)request.getAttribute("Mensaje");
			String parTrama = (String)request.getAttribute("Trama");
			String parTrama1 = (String)request.getAttribute("Trama1");
			if (parTrama1 != null){
		parTrama = parTrama1;
			}
			String parTramaOld = (String)request.getParameter("tramaOld");
			String parDiasInhabiles = (String)request.getAttribute("ParDiasInhabiles");
			String parFecha = (String)request.getAttribute("Fecha");
			String parMovFechas = (String)request.getAttribute("Movfechas");
			String parFechaAyer = (String)request.getAttribute("FechaAyer");
			String parFechaPrimero = (String)request.getAttribute("FechaPrimero");
			String parVarFechaHoy = (String)request.getAttribute("VarFechaHoy");
			String parFechaHoy = (String)request.getAttribute("FechaHoy");
			String parFechaDia = (String)request.getAttribute("FechaDia");

			if(parMenuPrincipal == null) parMenuPrincipal = "";
			if(parFuncionesMenu == null) parFuncionesMenu = "";
			if(parEncabezado == null) parEncabezado = "";
			if(parCuentas == null) parCuentas = "";
			if(parMensaje == null) parMensaje = "";
			if(parTrama == null) parTrama = "";
			if(parDiasInhabiles == null) parDiasInhabiles = "";
			if(parFecha == null) parFecha = "";
			if(parMovFechas  == null) parMovFechas = "";
			if(parFechaAyer == null) parFechaAyer = "";
			if(parFechaPrimero == null) parFechaPrimero = "";
			if(parVarFechaHoy == null) parVarFechaHoy = "";
			if(parFechaHoy == null) parFechaHoy = "";
			if(parFechaDia == null) parFechaDia = "";

			// --- Se preparan otras variables -------------------------------------------------
			Vector cuentas;
			String mensError [] = new String[1];
			mensError[0]="";

			cuentas = ((new mx.altec.enlace.servlets.bcProgFechas()).creaCuentas(parTrama, request, mensError));
			String nombreCuenta = parTrama.substring(1,parTrama.indexOf("|"));
			int cont;
			for(cont=0;cont<cuentas.size() && !((bc_CuentaProgFechas)cuentas.get(cont)).getNumCta().equals(nombreCuenta);cont++);
		bc_CuentaProgFechas cuenta = (bc_CuentaProgFechas)cuentas.get(cont);

			int a, b;
			int rango=0;
			final int horaInicial=8;
			final int horaFin =19;
			String mientras="";
	%>

<!-- COMIENZA CODIGO HTML -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>

<HEAD>
	<TITLE>Programaci&oacute;n de Operaci&oacute;n</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Adrian Mayen">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts -->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>
		//Obtenci�n de los respectivos dias Inhabiles y modificado para Integraci�n
		var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>"
		diasInhabiles = '<%= request.getAttribute("DiasInhabiles") %>';
		var indice =0;
		//--- calendarios y actualizaci�n de las cuentas seleccionadas ------------------
		var ctaselec;
		var ctadescr;
		var ctatipre;
		var ctatipro;
		var ctaserfi;
		var ctaprod;
		var ctasubprod;
		var tramadicional;
		var opcioncalendario;
		var cfm;
		var y=0;

		//de la parte html cuentas y descripci�n
		function actualizacuenta()
			{
			document.Forma.textdestino.value=ctaselec+" -- "+ctadescr;
			}

		// --- CREA LA TRAMA QUE SERA ENVIADA AL SERVLET ---------------------------------
		function agregar(){
			var trama = "@";
			var a, b=0,c, x=0, De, A;

			for(a=0;a<document.Forma.de.options.length;a++){
				if(document.Forma.de.options[a].selected){
					//De = parseInt(document.Forma.de.options[a].value);
					if (document.Forma.de.options[a].value.substring(0,1)=="0"){
						De = parseInt(document.Forma.de.options[a].value.substring(1));
					}else De = parseInt(document.Forma.de.options[a].value);
				}
			}
			for(a=0;a<document.Forma.a.options.length;a++){
				if(document.Forma.a.options[a].selected){
					//A  = parseInt(document.Forma.a.options[a].value);
					if (document.Forma.a.options[a].value.substring(0,1)=="0"){
						A = parseInt(document.Forma.a.options[a].value.substring(1));
					}else A = parseInt(document.Forma.a.options[a].value);
				}
			}
			var Valortecho;

			// Comprueba si lleva signo "$" el campo valortecho

			if (document.Forma.valortecho.value.substring(0,1) == "$")
			{
				Valortecho = document.Forma.valortecho.value.substring(1);

				//LASR
				Valortecho = Valortecho.replace(',','');
				Valortecho = Valortecho.replace(' ','');

			}
			else
			{
				Valortecho = document.Forma.valortecho.value;

				//LASR
				Valortecho = Valortecho.replace(',','');
				Valortecho = Valortecho.replace(' ','');

			}

			var FechaInicial = document.Forma.fechaInicial.value;
				FechaInicial = new Date(FechaInicial.substring(6),
										FechaInicial.substring(3,5) ,
										FechaInicial.substring(0,2)
										);
			var FechaFinal = document.Forma.fechaFinal.value;
				FechaFinal = new Date ( FechaFinal.substring(6),
										FechaFinal.substring(3,5) ,
										FechaFinal.substring(0,2)
										);

			var ctasHijas = document.Forma.textdestino.value;
			var ctahija = ctasHijas.substring(0,ctasHijas.indexOf("-"));
			var ctadesc = ctasHijas.substring(ctasHijas.indexOf("-")+2);

			//VALIDACION DE LOS DATOS EN LA FORMA DEL USUARIO
			if(document.Forma.textdestino.value == "") // Valida si se ingreso una cuenta
				{aviso("Debe seleccionar una cuenta..."); return;}
			if ( De == A)
				{aviso("Los horarios de operacion no deben ser los mismos..."); return;}
			if ( De > A)
				{aviso("La hora final debe ser mayor a la hora de inicio..."); return;}
			if (document.Forma.techo[0].checked == true){
				if(Valortecho == ""){
					aviso("Debe proporcionar un valor para el Techo Presupuestal..."); return;}
			}else Valortecho="0";
			if(techoIsText(Valortecho)==false) return;
			if (FechaInicial > FechaFinal)
				{aviso("La fecha final debe ser mayor o igual que la inicial...");return;}

			//--- Construccion de la trama
			trama +=ctahija + "|";											// 1 Cuenta
			trama += document.Forma.fechaInicial.value + "|";				// 2 Fecha de Inicio
			trama += document.Forma.fechaFinal.value + "|";					// 3 Fecha final
			for(b=0;b<document.Forma.de.options.length;b++)
					if(document.Forma.de.options[b].selected)
						trama += document.Forma.de.options[b].value + "|";	// 4 Hora de Inicio
			for(x=0;x<document.Forma.a.options.length;x++)
					if(document.Forma.a.options[x].selected)
						trama += document.Forma.a.options[x].value + "|";	// 5 Hora Final
			trama += Valortecho + "|";										// 6 Valor del techo
			trama +=" |";													// 7 Usuario
			trama += ctadesc + "@";											// 8 Descripcion
			document.Forma.Accion.value = "MODIFICA_MODIFICA";
			document.Forma.Trama.value = trama;
			document.Forma.submit();
			}


		function alta()
			{
			document.Forma.Accion.value = "ALTA_ARBOL";
			document.Forma.submit();
			}

		function borrar()
			{
			confirma("La estructura va a ser borrada, &iquest;Desea continuar?");
			accionConfirma = 2;
			}

		function editar()
			{
			var sel = ctaSel();
			if(sel == -1) {aviso("Debe seleccionar una cuenta para Modificar"); return; }
			document.Forma.Accion.value = "EDITA";
			document.Forma.Trama.value = document.Forma.CtaSel[ctaSel()].value;
			document.Forma.submit();
			}

		function regresar()
			{
			document.Forma.Accion.value = "REGRESA";
			document.Forma.submit();
			}

		function eliminar()
			{
			var sel = ctaSel();
			if(sel == -1) {aviso("Debe seleccionar una cuenta para eliminar"); return;}

			confirma("Se va a borrar la cuenta seleccionada, as&iacute; como sus cuentas hija en caso de tenerlas. &iquest;Desea continuar?");
			accionConfirma = 3;
			return;
			}

		function imprimir()
			{scrImpresion();}

		// --- Validaciones ----------------------------------------------------------------
		function Actualiza()
			{
			if(Indice==0)
				document.Forma.fechaInicial.value=Fecha[Indice];
			else
				document.Forma.fechaFinal.value=Fecha[Indice];
			}

		function selUno(num)
			{
			var max = document.Forma.CtaSel.length - 2;
			for(a=0;a<max;a++) if(a != num) document.Forma.CtaSel[a].checked = false;
			}

		function ctaSel()
			{
			var sel = -1;
			var max = document.Forma.CtaSel.length - 2;
			for(a=0;a<max;a++) if(document.Forma.CtaSel[a].checked) {sel = a; break;}
			return sel;
			}

		//-- Permite desabilitar la caja de texto de el Techo  Presupuestal -----------------
		/*function valTecho()
			{
			if(document.Forma.techo[1].checked)
				{
				document.Forma.valortecho.disabled = true;
				}
			else
				{
				document.Forma.valortecho.disabled = false;
				}
			}*/

		//-- Permite desabilitar la caja de texto de el Techo  Presupuestal (2 funciones siguientes)-----------------
		var _T = "locked";
		var _F = "unlocked";
		function valTecho(_P)
			{
			var _L = document.Forma.lck.value;
			if(_L==_P)return;
			document.Forma.valortecho.disabled=(document.Forma.lck.value=(_L==_F)?_T:_F)==_T;
			document.Forma.valortecho.value="";
			}
		function isDis()
			{
			return (document.Forma.lck.value==_T);
			}

		function noValTecho()
			{
				document.Forma.techo[1].checked = true;
				valTecho(_T);
			}

		function condiciones()
			{
			if(document.Forma.textdestino == "") return;
			}

		function imprimeUnaLinea(texto)
			{
			for(a=0;a<texto.length;a++)
				if(texto.substring(a,a+1) != " ")
					document.write(texto.substring(a,a+1));
				else
					{
					document.write("&");
					document.write("n");
					document.write("b");
					document.write("s");
					document.write("p");
					document.write(";");
					}
			}

		// --- Para cuadros de di�logo -----------------------------------------------------
		var respuesta = 0;
		var accionConfirma;
		var habilitaConfirma = true;

		function continua()
			{
			if(accionConfirma == 1)
				{
				if(respuesta == 1) agregar();
				}
			else if(accionConfirma == 2)
				{
				if(respuesta == 1)
					{
					document.Forma.Accion.value = "BORRA_ARBOL";
					document.Forma.submit();
					}
				}
			else
				{
				if(respuesta == 1)
					{
					document.Forma.Accion.value = "ELIMINA";
					document.Forma.Trama.value = document.Forma.CtaSel[ctaSel()].value;
					document.Forma.submit();
					}
				}
			habilitaConfirma = true;
			}

		function confirma(mensaje)
			{if(habilitaConfirma) cuadroDialogo(mensaje,2); habilitaConfirma = false;}

		function aviso(mensaje) {cuadroDialogo(mensaje,1);}

		function avisoError(mensaje) {cuadroDialogo(mensaje,3);}

		// --- Inicio y envio --------------------------------------------------------------
		function inicia()
			{
			/*horaInicial=8;
			horaFin =19;
			rango=0;
			for(rango=horaInicial;rango<horaFin;rango++){
				if (rango == parseInt("<= cuenta.getHorarioOper1()>"))
					document.Forma.de.options[rango].selected;
		    }*/

		//	document.Forma.Periodicidad[0].checked = true;
		//	document.Forma.TipoDis[0].checked = true;
			//document.Forma.PerDias.disabled = true;
		//	document.Forma.PerDias.selectedIndex = -1;
		//	condiciones();
			}

		function validaEnvio()
			{
			/*
			//if (verificaFechas()) return false;
			if(document.Forma.Accion.value == "") return false;
			return true;
			*/
			}

		// --- Funciones de men� -----------------------------------------------------------
		function MM_preloadImages()
			{ //v3.0
			var d=document;
			if(d.images)
				{
				if(!d.MM_p) d.MM_p=new Array();
				var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
				for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0)
					{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
				}
			}

		function MM_swapImgRestore()
			{ //v3.0
			var i,x,a=document.MM_sr;
			for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
			}

		function MM_findObj(n, d)
			{ //v3.0
			var p,i,x;
			if(!d) d=document;
			if((p=n.indexOf("?"))>0&&parent.frames.length)
				{
				d=parent.frames[n.substring(p+1)].document;
				n=n.substring(0,p);
				}
			if(!(x=d[n])&&d.all) x=d.all[n];
			for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
			for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
			return x;
			}

		function MM_swapImage()
			{ //v3.0
			var i,j=0,x,a=MM_swapImage.arguments;
			document.MM_sr=new Array;
			for(i=0;i<(a.length-2);i+=3)
				if((x=MM_findObj(a[i]))!=null)
					{
					document.MM_sr[j++]=x;
					if(!x.oSrc) x.oSrc=x.src;
					x.src=a[i+2];
					}
			}

		<%= parFuncionesMenu %>

		function techoIsText(valor)
			{
			var x1=0;
			var cont=0;
			//Text1=document.Forma.valortecho.value;
			Text1=valor;
			for(x1=0;x1<Text1.length;x1++)
				{
				Text2 = Text1.charAt(x1);
				if((Text2>='0' && Text2<='9') || Text2=='.') cont++;
				}
			if(cont!=x1)
				{
				cuadroDialogo("El Techo presupuestal debe ser num&eacute;rico.",3);
				return false;
				}
			/*if(Text1.indexOf('.')<0)
				Text1+=".00";
			document.Forma.valortecho.value=Text1;
			return true;*/
			}

		function SeleccionaFecha(ind)
			{
			var m=new Date();
			n=m.getMonth();
			js_diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';
			Indice=ind;
			/*msg=window.open("/EnlaceMig/bc_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=330,height=260");
			msg.focus();*/

			var parMovFechas1 = "<%= request.getAttribute("Movfechas") %>";
			var FrmFechasDia = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasMes = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasAnio = parMovFechas1;

			dia = FrmFechasDia;//09;
			mes = FrmFechasMes;//10;
			anio = FrmFechasAnio;//2002;

			msg=window.open("/EnlaceMig/bc_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
			msg.focus();
			}

		function cambiaFechas(ele)
			{
			if(ele.value=='techosi')
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaPrimero") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaAyer") %>";
				}
			else
				{
				document.Forma.FechaIni.value="<%= request.getAttribute("FechaDia") %>";
				document.Forma.FechaFin.value="<%= request.getAttribute("FechaDia") %>";
				}
			}

		function cambiaFechas(ele)
			{
			if(ele.value=='techo')
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaPrimero") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaAyer") %>";
				}
			else
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaDia") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaDia") %>";
				}
			}

		function verificaFechas()
			{
			var TipoB="";
			var y=0;

			for(i1=0;i1<document.Forma.length;i1++)
			if(document.Forma.elements[i1].type=='radio')
				if(document.Forma.elements[i1].checked==true)
					{
					TipoB=document.Forma.elements[i1].value;
					break;
					}

			if(TipoB=="no")
				{
				if(mes1[0]>mes1[1] && anio1[0]==anio1[1])
					{
					cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
					return false;
					}
				if(mes1[0]==mes1[1] && anio1[0]==anio1[1])
					if(dia1[0]>dia1[1])
						{
						cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.");
						return false;
						}
				}
			return true;
			}

			function fechaString(cadena){ // Elimina el "/" a la cadena de la fecha
			var fechaSin1 = "";
			var fechaSin2 = "";
			var ind;
			while ((ind = cadena.indexOf("/")) != -1){
				fechaSin1 = cadena.substring(0,ind);
				fechaSin2 = cadena.substring(ind+1, cadena.length);
				cadena = fechaSin1 + fechaSin2;
				ind = cadena.indexOf("/");
			}
			return cadena;
		}

		function ffocus(num){
			document.Forma.valortecho.focus();
			SeleccionaFecha(num);
		}

		<%= request.getAttribute("VarFechaHoy") %>   //del MCL_Movimientos
	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->
<BODY onload="inicia()" background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>
	<form name = "Frmfechas">
		<%
				String FrmFechasAnio = parMovFechas.substring(0,parMovFechas.indexOf("-"));
				parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
				String FrmFechasMes = parMovFechas.substring(0,parMovFechas.indexOf("-"));
				parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
				String FrmFechasDia = parMovFechas;
		%>

		<!-- input name=Fecha type=hidden value='<%--= request.getAttribute("FechaDia") --%>' size=10  onFocus='blur();' maxlength=10-->
		<Input type = "hidden" name ="strDia" value = "<%= FrmFechasAnio %>">
		<Input type = "hidden" name ="strMes" value = "<%= FrmFechasMes %>">
		<Input type = "hidden" name ="strAnio" value = "<%= FrmFechasDia %>">
	</form>

	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="610">
		<tr valign="top">
			<td width="*">
			<%=parMenuPrincipal%>
			</td>
		</tr>
	</table>
	<%=parEncabezado%>

	<FORM name=Forma  onSubmit="return validaEnvio()" action="bcProgFechas">
		<INPUT type=Hidden name="Accion" value="">
		<INPUT type=Hidden name="Trama" value="">
		<INPUT type=Hidden name="Cuentas" value="<%= parCuentas %>">
		<INPUT type=Hidden name="tramaOld" value="<%= parTrama %>">

		<DIV align=center>
		<table class="textabdatcla" border=0 cellspacing=0 cellpadding=3>
			<tr>
				<td class="tittabdat" colspan=3>&nbsp;Seleccione la cuenta y las condiciones de la operaci&oacute;n
				</td>
			</tr>

			<tr>
				<td class='tabmovtex' colspan="3">
					<br>&nbsp;<b>Cuenta:</b><br>
					<!-- Campo de selecci�n de cuenta -->
					<table>
						<td>
						</td>
						<td>
							<input type="text" name="textdestino" class="tabmovtexbol" size=50 onfocus="blur();" value="<%= cuenta.getNumCta().trim() + "--" + cuenta.getDescripcion()%>">
							<!-- <input type="text" name="textdestino" class="tabmovtexbol" maxlength=22 size=30 onfocus="blur();" value=""> -->
						</td>
					</table>
				</td>

			<tr>
				<td colspan=3>&nbsp;<br>
				</td>
			</tr>
			<tr>
				<td class="tabmovtex" width="200" rowspan=2 nowrap><b>Horario de operaci&oacute;n:</b><BR>

					<!-- Campos de Horario de operaci�n -->
					<TABLE width=150>
						<TR><TD class="tabmovtex">De:&nbsp;</TD>
							<TD class="tabmovtex">
								<select width=100 style="width:100px;" name="de" class="tabmovtex" onChange="condiciones()">
									<option value="0830">08:30</option>
									<%
													String rangox;
													for(rango=horaInicial+1;rango<=horaFin;rango++){
														rangox = (rango ==9)?"0"+ rango :"" + rango;
														if (cuenta.getHorarioOper1().equalsIgnoreCase(rangox+ "00")){
															out.println("<OPTION SELECTED value=\"" + rangox +"00\">" + rangox + ":00</OPTION>");}
														else
														  out.println("<OPTION value=\"" + rangox +"00\">" + rangox + ":00</OPTION>");
														if (cuenta.getHorarioOper1().equalsIgnoreCase(rangox + "30")){
															out.println("<OPTION SELECTED value=\"" + rangox +"30\">" + rangox + ":30</OPTION>");}
														else
														  out.println("<OPTION value=\"" + rangox +"30\">" + rangox + ":30</OPTION>");
														    }
									%>
								</select>
							</TD>
						</TR>
						<TR><TD class="tabmovtex">A:&nbsp;</TD>
							<TD class="tabmovtex">
							<select width=100 style="width:100px;" name="a" class="tabmovtex" onChange="condiciones()">
							<%
											  String rangoy;
											  for(rango=horaInicial+1;rango<=horaFin;rango++){
												rangoy = (rango == 8 || rango == 9)?"0"+ rango: "" + rango;
												if (cuenta.getHorarioOper2().equalsIgnoreCase(rangoy + "00")){
											out.println("<OPTION SELECTED value=\"" + rangoy +"00\">" + rangoy + ":00</OPTION>");}	else
												out.println("<OPTION value=\"" + rangoy +"00\">" + rangoy + ":00</OPTION>");

												if (cuenta.getHorarioOper2().equalsIgnoreCase(rangoy + "30")){
											out.println("<OPTION SELECTED value=\"" + rangoy +"30\">" + rangoy + ":30</OPTION>");}
												else
												  out.println("<OPTION value=\"" + rangoy +"30\">" + rangoy + ":30</OPTION>");

											  }
							%>
							</select>
							</TD>
						</TR>
					</TABLE>

				</td>
				<td class="tabmovtex" width="200" colspan=2>
					Techo presupuestal:
				</td>
			</tr>
			<tr valign="middle">
				<td class="tabmovtex">
					<input type=hidden name="lck" value="unlocked">
					<!-- Campos Techo presupuestal (Radio) -->
					<input type="radio" name="techo" value="si" onClick="valTecho(_F);" checked>Si<br>
					<input type="radio" name="techo" value="no" onClick="valTecho(_T);">No
				</td>
				<td class="tabmovtex" align="left">
					<!-- Campo techo presupuestal (Text) -->
					<!-- <input type="text" name="valortecho" size="18" class="tabmovtex" value= "" onblur="ImportIsValid()"> -->
					<%
					if (cuenta.getTechopre().equals("0.000000")){
					%>
						<!-- <input type="text" name="valortecho" size="18" class="tabmovtex" value= ""> -->
						<input id="valortecho" name="valortecho" size=19 class="tabmovtex" value= "" maxlength=19 onfocus="if(isDis())blur();">
						<SCRIPT>
							document.Forma.techo[1].checked = true;
							valTecho(_T);
						</SCRIPT>

					<%
					}else{
					%>
						<!-- <input type="text" name="valortecho" size="18" class="tabmovtex" value= "<%=(new mx.altec.enlace.servlets.BaseServlet()).FormatoMoneda(cuenta.getTechopre())%>"> -->
						<input id="valortecho" name="valortecho" size=19 class="tabmovtex" value= "<%=(new mx.altec.enlace.servlets.BaseServlet()).FormatoMoneda(cuenta.getTechopre())%>" maxlength=19 onfocus="if(isDis())blur();">
					<%
					}
					%>
				</td>
			</tr>

			<tr><td colspan=3>&nbsp;<BR></td></tr>

			<tr valign="top">
				<td class="tabmovtex" nowrap>
					Vigencia:<BR>

					<!-- Campos de vigencia -->
					<TABLE>
					<TR>
						<TD align=right class="tabmovtex">De la Fecha:</TD>
						<TD class="tabmovtex"><input type="text" name="fechaInicial" size="12" class="tabmovtexbol" value="<%= cuenta.getVigencia1()%>" onFocus="ffocus(0);"></TD>
						<TD class="tabmovtex"><a href ="javascript:SeleccionaFecha(0);" align="absmiddle"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></TD>
					</TR>
					<TR>
						<TD align=right class="tabmovtex">A la Fecha:</TD>
						<TD class="tabmovtex"><input type="text" name="fechaFinal" size="12" class="tabmovtexbol" value="<%= cuenta.getVigencia2()%>" onFocus="ffocus(1);"></TD>
						<TD class="tabmovtex"><A href ="javascript:SeleccionaFecha(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></A></TD>
					</TR>
					</TABLE>
				</td>
				<td colspan=2>&nbsp;</td>
			</tr>
		</table>
		<!-- fin table  -->

		<BR>
		<%
		if (parTrama != ""){
		%>
			<TABLE>
				<TR align=center>
					<TD>
						<A href="javascript:agregar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar"></A>
						<A href="javascript:regresar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></A>
					</TD>
				</TR>
			</TABLE>
		<%
		}else {
		%>

		<A href="javascript:agregar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25290.gif" alt="Agregar"></A>
		<BR>


		<!-- TABLA DE CUENTAS ------------------------------------------------------------->

		<TABLE width=650 border=0 cellspacing=1>
		<TR><TD class="tabmovtex"> Total de cuentas: <%=cuentas.size()%></TD></TR>
		</TABLE>

		<TABLE width=650 class="textabdatcla" border=0 cellspacing=1>
			<TR><TD colspan=8 class="tittabdat" align=center>Estructurilla</TD></TR>
			<TR>
				<TD class="tittabdat" align=center>Selecci&oacute;ne</TD>
				<TD class="tittabdat" align=center>Cuenta</TD>
				<TD class="tittabdat" align=center>Nivel</TD>
				<TD class="tittabdat" align=center>Horario</TD>
				<TD class="tittabdat" align=center>Techo</TD>
				<TD class="tittabdat" align=center>Importe</TD>
				<TD class="tittabdat" align=center>Vigencia</TD>
			</TR>

			<!-- Esta secci�n se repite para cada cuenta -->
			<%
					String espacio;
					String estilo;
					bc_CuentaProgFechas cta;

					estilo = "textabdatobs";
					for(a=0;a<cuentas.size();a++)
						{
						estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
						cta = (bc_CuentaProgFechas)cuentas.get(a);
						espacio = "";
						for(b=0;b<cta.nivel()-1;b++) espacio += "<IMG src=\"/gifs/EnlaceMig/gbo25625.gif\">";
						espacio += (((new mx.altec.enlace.servlets.bcProgFechas()).esHoja(cta,cuentas))?
						"<IMG src=\"/gifs/EnlaceMig/hoja.gif\">":
						"<IMG src=\"/gifs/EnlaceMig/folder.gif\">");
						espacio += "&nbsp;";
			%>
			<TR> <!-- Check Box -->
				<TD class="<%= estilo %>" align=center><INPUT type=CheckBox name="CtaSel" value="<%=cta.getNumCta()%>|<%=cta.getVigencia()%>" onClick="selUno(<%= a %>)"></TD>
				 <!-- Cuenta -->
				<TD class="<%= estilo %>"><%= espacio %><SCRIPT>imprimeUnaLinea('<%= cta.getNumCta() + " " + cta.getDescripcion() %>');</SCRIPT></TD>
				 <!-- Nivel -->
				<TD class="<%= estilo %>" align=center><%= cta.nivel() %></TD>
				 <!-- Horario -->
				<TD class="<%= estilo %>" align=center><%= cta.getHorarios()%></TD>
				 <!-- Techo Presupuestal -->
				<TD class="<%= estilo %>" align=center><%=(new mx.altec.enlace.servlets.BaseServlet()).FormatoMoneda(cta.getTechopre())%></TD>
				 <!-- Importe -->
				<TD class="<%= estilo %>" align=center>&nbsp;</TD>
				 <!-- Vigencia -->
				<TD class="<%= estilo %>" align=center><%=cta.getVigencia()%></TD>
			</TR>
			<% } %>

			<TR>
		</TABLE>
		<BR>
		<!-- Botones Alta, Modificar, Elimina Registro e Imprimir ---------------------->
			<TABLE>
				<TR align=center>
					<TD>
						<A href="javascript:alta()"><IMG border=0 src="/gifs/EnlaceMig/gbo25480.gif" alt="Alta"></A>
						<A href="javascript:editar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar"></A>
						<A href="javascript:eliminar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25515.gif" alt="Eliminar registro"></A>
						<A href="javascript:imprimir()"><IMG border=0 src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></A>
					</TD>
				</TR>
			</TABLE>
		<%
		}
		%>

		</DIV>

		<!-- Las siguientes dos l�neas son importantes, no borrar -->
		<INPUT type=Hidden name="CtaSel" value="X">
		<INPUT type=Hidden name="CtaSel" value="XX">


		<INPUT type=Hidden name="cuenta" value="">
		<!-- Boton envia consulta -- Linea provisional -->
		<!-- <INPUT type=submit onClick="document.Forma.Accion.value = 'TUXEDO'; document.Forma.Trama.value =''; ">  -->

	</FORM>
	<%if (cuenta.getTechopre().equals("0.000000")){%>
		<SCRIPT>valTecho(_T);</SCRIPT>
	<%}%>
</BODY>

</HTML>

<% if(!parMensaje.equals("")) out.println("<SCRIPT>aviso('" + parMensaje + "');</SCRIPT>"); %>
