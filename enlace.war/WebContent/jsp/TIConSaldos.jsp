	<%@ page import="java.util.*" %>
	<%@ page import="mx.altec.enlace.bo.FormatoMoneda" %>

	<%

	// --- Se obtienen par�metros
//----------------------------------------------------------
	String parFuncionesMenu = (String)request.getAttribute("FuncionesMenu");
	String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
	String parEncabezado = (String)request.getAttribute("Encabezado");
	String parMontoTotal = (String)request.getAttribute("MontoTotal");
	String parStrOpcion = (String)request.getAttribute("StrOpcion");
	String parParciales = (String)request.getAttribute("Parciales");
	String parCuentas = (String)request.getAttribute("Cuentas");
	String parOpcAnt = (String)request.getAttribute("OpcAnt");
	String parAccion = (String)request.getAttribute("Accion");
	String parTotal = (String)request.getAttribute("Total");
	String parAncho = (String)request.getAttribute("Ancho");

    System.out.println("YA estoy en el JSP y obtuve los parametros SON: .............");

	if(parFuncionesMenu == null) parFuncionesMenu = "";
	if(parMenuPrincipal == null) parMenuPrincipal = "";
	if(parEncabezado == null) parEncabezado = "";
	if(parMontoTotal == null) parMontoTotal = "";
	if(parStrOpcion == null) parStrOpcion = "";
	if(parParciales == null) parParciales = "";
	if(parCuentas == null) parCuentas = "";
	if(parOpcAnt == null) parOpcAnt = "";
	if(parAccion == null) parAccion = "";
	if(parTotal == null) parTotal = "";
	if(parAncho == null) parAncho = "";

	System.out.println("parMontoTotal    ............." +parMontoTotal);
	System.out.println("parTotal         ............." +parTotal);
    String tipoCtas = (String)session.getAttribute("DivisaDefault");
	// --- Se calculan variables para imprimir
//---------------------------------------------
	String[]
		cuenta = null;			// Arreglo de cuentas
	double
		saldo[] = null,			// Arreglo de saldos
		parcial[] = null,		// Arreglo de montos parciales
		saldoTotal = 0;			// Saldo total
	int
		nivel[] = null,			// arreglo de niveles
		maxProfundidad = 0,		// m�ximo nivel
		numColumnas = 0,		// n�mero de columnas de nivel por mostrar
		numCuentas = 0,			// n�mero de cuentas por mostrar
		offset = 0,				// desplazamiento en la lista de cuentas
		totalCuentas = 0,		// total de cuentas
		ctasXpag = 0;			// cuentas por p�gina mostrados

	int a,b;
	StringTokenizer tokens;
	Vector tramaCuentas;
	
	try
		{
		offset = Integer.parseInt(parAccion);
		totalCuentas = Integer.parseInt(parTotal);
		ctasXpag = Integer.parseInt(parAncho);
		numCuentas = 0;
		tokens = new StringTokenizer(parCuentas,"@");
		tramaCuentas = new Vector();
		while(tokens.hasMoreTokens()){
			tramaCuentas.add(tokens.nextToken());
			numCuentas++;
		}
		cuenta = new String[numCuentas];
		saldo = new double[numCuentas];
		nivel = new int[numCuentas];
		maxProfundidad = 0;
		System.out.println("Voy a obtener las cuentas desde el JSP  ...........");
		for(a=0;a<numCuentas;a++)
			{
			tokens = new StringTokenizer((String)tramaCuentas.get(a),"|");
			cuenta[a] = tokens.nextToken();
			nivel[a] = Integer.parseInt(tokens.nextToken());
			saldo[a] = Double.parseDouble(tokens.nextToken());
			System.out.println("La cuenta[a] es igual ..........." + cuenta[a]);
			if(maxProfundidad < nivel[a]) maxProfundidad = nivel[a];
			}

		a = 0;
		tokens = new StringTokenizer(parParciales,"@");
		tramaCuentas = new Vector();
		for(a=0;tokens.hasMoreTokens();a++)
{tramaCuentas.add(tokens.nextToken());}

		parcial = new double[a];
		for(b=0;b<a;b++)
parcial[b]=Double.parseDouble((String)tramaCuentas.get(b));
		if(offset+ctasXpag >= totalCuentas) {maxProfundidad = a;}
		numColumnas = 5; if(numColumnas < maxProfundidad) numColumnas =
maxProfundidad;

		saldoTotal = Double.parseDouble(parMontoTotal);
		}
	catch(Exception e)
		{
		System.out.println("<DEBUG Rafa Villar> Error en TIConSaldos.jsp: " +
e.toString());
		out.println("<DEBUG Rafa Villar> Error en TIConSaldos.jsp: " +
e.toString());
		}
	%>


<!-- COMENZA CODIGO HTML
============================================================-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Bienvenido a Enlace Internet</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Rafael Villar Villar">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts
=====================================================================-->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2"
src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>

	// --- formatea los n�meros para imprimirse como formato moneda
//--------------------
function Formatea_Importe(imp)
{
  var ant="";
  var punto="";
  var imp2=imp;
  var tot=0;
  var num_aux="";
  if(imp.indexOf(".")+2<imp.length){
	  imp=imp.substring(0,imp.indexOf(".")+3)
  }

  if(imp.indexOf(".")>=0)
   {
     punto=imp.substring(imp.indexOf("."));
     imp2=imp.substring(0,imp.indexOf("."));
   }
  else
    punto=".00";

  tot=imp2.length/3;
  for(c=0;c<tot;c++)
    ant = (imp2.substring( (imp2.length - (3*c))-3, (imp2.length -
(3*c)) ) + ((c==0)?"":",")) + ant;
  ant="$ " + ant + punto;
  return ant;
}


/*function Formatea_Importe(importe){
   decenas=""
   centenas=""
   millon=""
   millares=""
   millar_millon=""
   importe_final=importe

   var posiciones=7;
   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)
   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4){
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }

   if (posi_importe==5){
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6){
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }

   if (posi_importe==7){
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }

   if (posi_importe==8){
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }

   if (posi_importe==9){
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }

   if (posi_importe==10){
     centenas=importe.substring(7, posiciones + 6);
     millares=importe.substring(4,7);
	 millon=importe.substring(1,4);
     millar_millon=importe.substring(0,1)
     importe_final= millar_millon + "," + millon + "," + millares + "," + centenas;
   }

   if (posi_importe==11){
     centenas=importe.substring(8, posiciones + 7);
     millares=importe.substring(5,8);
	 millon=importe.substring(2,5);
     millar_millon=importe.substring(0,2)
     importe_final= millar_millon + "," + millon + "," + millares + "," + centenas;
   }

   if (posi_importe==12){
     centenas=importe.substring(9, posiciones + 8);
     millares=importe.substring(6,9);
	 millon=importe.substring(3,6);
     millar_millon=importe.substring(0,3)
     importe_final= millar_millon + "," + millon + "," + millares + "," + centenas;
   }

   importe_final="$" + importe_final;
   return importe_final
} */


/*	function Formatea_Importe(num)
		{
		alert(num);
		var punto = num.indexOf(".");
		var suma = 0;
		var n = 0;
		var str = "";

		if(punto == -1) {num += "."; punto = num.indexOf(".");}
		if(punto+3<num.length) suma =
(parseInt(num.substring(punto+3,punto+4))>=5)?1:0;
		while(punto+3>num.length) num += "0";
		n = parseInt(num.substring(0,punto)+num.substring(punto+1,punto+3)) +
suma;
		str = "" + n;
		for(a=str.length-5;a>0;a-=3) str = str.substring(0,a) + "," +
str.substring(a);
		str = "$" + str.substring(0,str.length - 2) + "." +
str.substring(str.length - 2);
		if(str == "$.0") str="$0.00";
		alert(str);
		return str;
		System.out.println("Formate el numero " + num);
		System.out.println("y quedo como str  " + str);
		}
*/
	// --- pide al server otra p�gina
//--------------------------------------------------
	function muestra(x)
		{
		chkSess();
		document.Forma.Accion.value = x;
		document.Forma.submit();
		}

	// --- imprime el texto en una sola l�nea
//------------------------------------------
	function imprimeUnaLinea(texto)
		{
		for(a=0;a<texto.length;a++)
			if(texto.substring(a,a+1) != " ")
				document.write(texto.substring(a,a+1));
			else
				{
				document.write("&");
				document.write("n");
				document.write("b");
				document.write("s");
				document.write("p");
				document.write(";");
				}
		}

	// --- Funciones de men�
//-----------------------------------------------------------
	function MM_preloadImages()
		{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0)
					{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++)
x=MM_findObj(n,d.layers[i].document);
		return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
		}

	<%= parFuncionesMenu %>

       	function chkSess(){
		if (document.Forma.tipoEstructura[0].checked){
			document.Forma.hdnAgrega.value = "MN";
                        document.Forma.MontoTotal.value = "";
                        document.Forma.Parciales.value = "";
                        document.Forma.MontoTotal.value = "";
		}
		if (document.Forma.tipoEstructura[1].checked){
			document.Forma.hdnAgrega.value = "USD";
                        document.Forma.MontoTotal.value = "";
                        document.Forma.Parciales.value = "";
                        document.Forma.MontoTotal.value = "";
		}
	}

	function msgTipoE(){
		if (document.Forma.tipoEstructura[0].checked){
//			document.Forma.msgTipo.value = "Concentracion en pesos";
			document.Forma.hdnDivisa.value = "MN";
			chkSess();
			document.Forma.submit();
		}
		if (document.Forma.tipoEstructura[1].checked){
//			document.Forma.msgTipo.value = "Concentracion en dolares";
			document.Forma.hdnDivisa.value = "USD";
			chkSess();
			document.Forma.submit();
		}
	}


	</SCRIPT>

</HEAD>

<!-- CUERPO
==========================================================================-->

<BODY background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>

	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top"><td width="*">
	<%= parMenuPrincipal %>
	</td></tr></table>

	<FORM name=Forma action="TIConSaldos">
        <%
        //String filtrarPor = (String)session.getAttribute("filtrarPor");
		//System.out.println("Filtrar por >"+filtrarPor+"<");
        %>
	<input type="hidden" name="hdnDivisa" value="">
	<input type="hidden" name="hdnAgrega" value="<%=tipoCtas%>">


	<%= parEncabezado %>
<!--
        <table align="center">
        <tr>
			<td class="tabmovtex" width="211">
				<input type="radio" name="tipoEstructura" value="MN" onclick="javascript:msgTipoE();" >
	              Saldo Estructura en Moneda Nacional
            </td>
            <td class="tabmovtex" width="250">
				<input type="radio" name="tipoEstructura" value="USD" onclick="javascript:msgTipoE();" >
	              Saldo Estructura en D&oacute;lares
            </td>
        </tr>
        </table>
-->
	<INPUT name=Accion type=Hidden value=<%= parAccion %>>
	<INPUT name=Total type=Hidden value=<%= parTotal %>>
	<INPUT name=Ancho type=Hidden value=<%= parAncho %>>
	<INPUT name=opcion type=Hidden value=<%=
(parStrOpcion.equals("concentraci&oacute;n"))?1:0 %>>
	<INPUT name=OpcAnt type=Hidden value="<%= parOpcAnt %>">
	<INPUT name=Parciales type=Hidden value=<%= parParciales %>>
	<INPUT name=MontoTotal type=Hidden value=<%= parMontoTotal %>>

	<DIV align=center>

            <%

			System.out.println("La divisa por default es: >"+tipoCtas+"<");
            String chkmn = "";
            String chkusd = "";
            String leyenda = "";
            if(tipoCtas.equals("MN") || tipoCtas.equals("ALL")){
              chkmn = "checked";
              leyenda = "Saldos en pesos";
            }
            if(tipoCtas.equals("USD")){
              chkusd = "checked";
              leyenda = "Saldos en d&oacute;lares";
            }
            /*if(filtrarPor != null && filtrarPor.trim().length() > 0){
              if(filtrarPor.trim().equals("MN")){
                chkmn = "checked";
                chkusd = "";
                leyenda = "Saldos Moneda Nacional";
              }
              else{
                chkusd = "checked";
                chkmn = "";
                leyenda = "Saldos D&oacute;lares";
              }
            }*/
            %>
        <table align="center">
        <tr>
            <td class="tabmovtexbol" width="200" align="Center">
              <%=leyenda%>
            </td>
            <td>&nbsp;</td>
			<%System.out.println("Si estoy pasando por aqui y tipoCtas >"+tipoCtas+"<  y  la leyenda es >" + leyenda + "<");%>
            <%if(tipoCtas.equals("MN") || leyenda.equals("Saldos en pesos")){%>
            <td class="tabmovtex" width="250"><input type="radio" name="tipoEstructura" value="MN" onclick="javascript:msgTipoE();"checked>Saldo Estructura en Moneda Nacional</td>
			<td class="tabmovtex" width="250"><input type="radio" name="tipoEstructura" value="USD" onclick="javascript:msgTipoE();">Saldo Estructura en D&oacute;lares</td>
            <%}else{ if(tipoCtas.equals("USD")){%>
            <td class="tabmovtex" width="250"><input type="radio" name="tipoEstructura" value="MN" onclick="javascript:msgTipoE();">Saldo Estructura en Moneda Nacional</td>
			<td class="tabmovtex" width="250"><input type="radio" name="tipoEstructura" value="USD" onclick="javascript:msgTipoE();"checked>Saldo Estructura en D&oacute;lares</td>
            <%}}%>
        </tr>
        </table>

	<TABLE width=600 border=0 cellspacing=1>
	<TR><TD class="tabmovtex">
	Saldo Total: <%=FormatoMoneda.formateaMoneda(saldoTotal) %>
	</TD></TR>
	</TABLE>

	<TABLE width=600 class="textabdatcla" border=0 cellspacing=1>
	<TR><TD colspan=<%= numColumnas+2 %> class="tittabdat" align=center>
	Detalle de Saldo de Estructura de <%= parStrOpcion %>
	</TD></TR>
	<TR>
		<TD bgcolor=#90B0C0 class="tittabdat">Cuenta</TD>
		<TD bgcolor=#90B0C0 class="tittabdat">Nivel</TD>
		<% for(a=1;a<=numColumnas;a++) { %>
		<TD bgcolor=#90B0C0 class="tittabdat"><SCRIPT>imprimeUnaLinea("Saldo Nivel <%= a %>");</SCRIPT></TD>
		<% } %>
	</TR>
	<% String estilo = "textabdatcla"; %>
	<% for(a=0;a<numCuentas;a++) { %>
	<TR>
		<% estilo=(estilo.equals("textabdatcla"))?"textabdatobs":"textabdatcla";
%>
		<TD class="<%= estilo %>"><SCRIPT>imprimeUnaLinea("<%= cuenta[a] %>");</SCRIPT></TD>
		<TD class="<%= estilo %>" align=center><%= nivel[a] %></TD>
		<% for(b=0;b<numColumnas;b++) { %>
		<TD class="<%= estilo %>" align=right><%= (b+1 == nivel[a]) ? FormatoMoneda.formateaMoneda(saldo[a]) : "&nbsp;" %></TD>
		<% } %>
	</TR>
	<% } %>
	<% if(offset+ctasXpag >= totalCuentas) { %>
	<TR>
		<TD class="tabfonbla">&nbsp;</TD>
		<TD class="textabdatobs">Totales:</TD>
		<% for(a=0;a<numColumnas ;a++) {%>
		<TD class="textabdatobs" align=right><%= (a<maxProfundidad) ? FormatoMoneda.formateaMoneda(parcial[a]) : "&nbsp;" %></TD>
		<% } %>
	</TR>
	<% } %>
	</TABLE>

	<DIV>
	<%
	String pag;

	if(offset>0) { %><A href="javascript:muestra(<%= offset-ctasXpag %>)" class=texfootpaggri>&lt;Anterior</A>&nbsp;<% }
	if(totalCuentas > ctasXpag) for(a=1;((a-1)*ctasXpag)<totalCuentas;a++)
		{
		pag = "" + a;
		if((a-1)*ctasXpag != offset)
			pag = "<A href=\"javascript:muestra(" + ((a-1)*ctasXpag) + ")\" class=texfootpaggri>" + pag + "</A>";
		else
			pag = "<SPAN  class=texfootpaggri>" + pag + "</SPAN>";
		out.println(pag);
		}
	if(offset+ctasXpag<totalCuentas) { %>&nbsp;<A href="javascript:muestra(<%=offset+ctasXpag %>)" class=texfootpaggri>Siguiente&gt;</A><% }
	%>
	</DIV>

	<BR><A href="javascript:scrImpresion()"><IMG border=0 src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></A>

	</DIV>

	</FORM>

</BODY>

</HTML>