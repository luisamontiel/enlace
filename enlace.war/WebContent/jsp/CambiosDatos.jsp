<html>
<head>
<title>Banca Virtual</title>
<script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<script language = "javascript" src="/EnlaceMig/scrImpresion.js"></script>
<SCRIPT LANGUAGE = "JavaScript1.2" SRC="/EnlaceMig/list.js"></SCRIPT>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language="JavaScript">
<%
if(request.getAttribute("ArchivoErr")!=null)
	out.println(request.getAttribute("ArchivoErr"));
%>
function Formatea_Importe(importe)
{
   decenas=""
   centenas=""
   millon=""
   millares=""
   importe_final=importe
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}

function validAmount (cantidad,band)
{

  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad
  var miles="";
  var centavoscompletos="";

  if (cantidadAux == "" || cantidadAux <= 0)
  {
    //document.transferencia.monto.value="";
    //document.transferencia.monto.focus();
    cuadroDialogo("\n Capture el "+band+"  a transferir ",3);
    return false;
  }
  else
  {
    if (isNaN(cantidadAux))
    {
	  //document.transferencia.monto.value="";
	  //document.transferencia.monto.focus();
      cuadroDialogo("Favor de ingresar un valor num\351rico en el "+band,3)
      return false
    }
    pos_punto = cantidadAux.indexOf (".")
    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        var longitud=cantidadAux.substring(cantidadAux.indexOf(".")+1,cantidadAux.length).length;
		if(band!="Tipo de Cambio Especial")
		{
		 if(longitud>2)
		 {

		   cuadroDialogo("S&oacute;lo se permiten 2 decimales en el campo:"+band,3);
		   return false;
  	     }
        }
		else
		{
          if(longitud>6)
		  {

		   cuadroDialogo("S&oacute;lo se permiten 6 decimales en el campo:"+band,3);
		   return false;
          }
		  if(longitud<6)
		  {
             var cerosanexos=0;
			 cerosanexos=6-longitud;
             for(i=0;i<6;i++)
			   centavoscompletos+="0";
		  }
        }
		cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)+centavoscompletos;
        entero = cantidadAux.substring (0, pos_punto)


      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

      if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

      if (miles != "")
        strAux2 = miles
      if (cientos != "")
        strAux2 = strAux2 + cientos + "."
      strAux2 = strAux2 + cents

      //transf = document.transferencia.monto.value
    }
    document.transferencia.montostring.value = strAux1;

    strAux1=Formatea_Importe(strAux1)
    if (miles != "")
      document.transferencia.montostring.value = strAux2;

    return true;
  }
}

function Formatea_Importe2 (cantidad)
{
  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad

  pos_punto = cantidadAux.indexOf (".");

  num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

  if (pos_punto == 0)
     strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
  else
  {
     if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

    if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

   strAux1=Formatea_Importe(strAux1)
  }
  return strAux1;

}

function validctasdif (from_account, to_account)
{
  var result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.transferencia.monto.focus()
	return false
  }
  return result;
}

function definir_valores_checkbox()
{
  /*document.tabla_transferencias.arregloestatus2.value="";
  document.transferencia.arregloestatus1.values="";
  valor_asignar="";
  for (i=0;i<document.tabla_transferencias.elements.length;i++)
  {
     if (document.tabla_transferencias.elements[i].type=="checkbox")
     {
        valor_asignar="1";
        if (document.tabla_transferencias.elements[i].checked==true)
          valor_asignar="1";
        else
          valor_asignar="0";
        document.tabla_transferencias.arregloestatus2.value=document.tabla_transferencias.arregloestatus2.value+valor_asignar+"@";
     }
  }
  document.transferencia.arregloestatus1.value=document.tabla_transferencias.arregloestatus2.value;*/
  document.tabla_transferencias.arregloestatus2.value="1@";
 document.transferencia.arregloestatus1.value=document.tabla_transferencias.arregloestatus2.value;
}
function EsAlfa(cadena, band) //******Cambio para Comprobante fiscal
{
  for (var i=0;i<cadena.length;i++)
  {
      if(!(((cadena.charAt(i)==' ') && (i != 0))||(cadena.charAt(i)>='a' && cadena.charAt(i)<='z')||(cadena.charAt(i)>='A' && cadena.charAt(i)<='Z')||(cadena.charAt(i)>='0' && cadena.charAt(i)<='9')))
      {
          cuadroDialogo("No se permiten caracteres especiales en "+band+ ", como: acentos, comas, puntos, etc",3);//******Cambio para Comprobante fiscal
          return false;
      }
  }
  return true;
}

function Valida_Date ()
{  return true;
}

function validar_tipo_cuentasNoRegis(ctaorigen,ctadestin)
{
   var tipo_origen="";
   var tipo_destino="";

   // Modificacion para integracion
   // var cta_origenv=""+ctaorigen.substring(0,ctaorigen.indexOf("|"));
   // var cta_destinv=""+ctadestin.substring(0,ctadestin.indexOf("|"));
   // if (  ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83")) ||
   //      ((cta_origenv.substring(0,2)=="49") && (cta_origenv.length==11)) )
	//	 tipo_origen="6";
   //else     //     tipo_origen="1";
   //if (  ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83")) ||
   //      ((cta_destinv.substring(0,2)=="49") && (cta_destinv.length==11)) )
	//	 tipo_destino="6";
   //else     //     tipo_destino="1";

   ctaorigen=ctaorigen.substring(ctaorigen.indexOf("|")+1,ctaorigen.length); //sacando cta
   ctaorigen=ctaorigen.substring(ctaorigen.indexOf("|")+1,ctaorigen.length); //sacando rel
   ctaorigen=ctaorigen.substring(ctaorigen.indexOf("|")+1,ctaorigen.length); //sacando des
   tipo_origen=ctaorigen.substring(0,ctaorigen.indexOf("|"));
   var cta_destinv=ctadestin;

   if (  ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83")) ||
         ((cta_destinv.substring(0,2)=="49") && (cta_destinv.length==11)) )
		 tipo_destino="6";
   else
         tipo_destino="1";


   if (((eval(tipo_origen)!=6)&&(eval(tipo_destino)==6))||
       ((eval(tipo_origen)==6)&&(eval(tipo_destino)!=6)))
     return true;
   else
   {
      cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas iguales",3);
      return false;
   }

 }

function validar_tipo_cuentasRegis(ctaorigen,ctadestin)
{
   var tipo_origen="";
   var tipo_destino="";

   // Modificacion para integracion
   //var cta_origenv=""+ctaorigen.substring(0,ctaorigen.indexOf("|"));
   //var cta_destinv=""+ctadestin.substring(0,ctadestin.indexOf("|"));
   //if (  ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83")) ||     //
   //      ((cta_origenv.substring(0,2)=="49") && (cta_origenv.length==11)) )     //
	//	 tipo_origen="6";     //
   //else
  //    tipo_origen="1";

   //if (  ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83")) ||     //
   //      ((cta_destinv.substring(0,2)=="49") && (cta_destinv.length==11)) )     //
	//	 tipo_destino="6";     //
   //else
   //      tipo_destino="1";


   if(eval(ctaorigen.length)>eval(0))
   {
     ctaorigen=ctaorigen.substring(ctaorigen.indexOf("|")+1,ctaorigen.length); //sacando cta
     ctaorigen=ctaorigen.substring(ctaorigen.indexOf("|")+1,ctaorigen.length); //sacando rel
     ctaorigen=ctaorigen.substring(ctaorigen.indexOf("|")+1,ctaorigen.length); //sacando des
     tipo_origen=ctaorigen.substring(0,ctaorigen.indexOf("|"));
  }
   else
   {
      cuadroDialogo("No se ha seleccionado cuenta de cargo",3);
	  return false;
   }

   if(eval(ctadestin.length)>eval(0))
   {
     ctadestin=ctadestin.substring(ctadestin.indexOf("|")+1,ctadestin.length); //sacando cta
     ctadestin=ctadestin.substring(ctadestin.indexOf("|")+1,ctadestin.length); //sacando rel
     ctadestin=ctadestin.substring(ctadestin.indexOf("|")+1,ctadestin.length); //sacando des
     tipo_destino=ctadestin.substring(0,ctadestin.indexOf("|"));
   }
   else
   {
     cuadroDialogo("No se ha seleccionado cuenta de abono",3);
	 return false;

   }


   if (((eval(tipo_origen)!=6)&&(eval(tipo_destino)==6))||
       ((eval(tipo_origen)==6)&&(eval(tipo_destino)!=6)))
     return true;
   else
   {
      cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas iguales",3);
      return false;
   }
 }

function LongitudCorrecta(Dato)
{
    var retorno=false;
	if (Dato.length == 11 || Dato.length == 16)
		retorno = true;
	else
	{
		cuadroDialogo("Cuenta inv&#225;lida.\nPara cuenta usar 11 d&iacute;gitos: 12345678901\nPara Tarjeta de Cr&eacute;dito usar 16 d&iacute;gitos : 1234567890123456",3);
		//document.transferencia.monto.focus();
	}
    return retorno;
}

function Actualiza()
{
   return true;
}



function definir_tipo_transmision()

{

  var result=true;

  if(document.transferencia.INICIAL.value=="SI")

  {


   // if (eval(document.transferencia.ChkOnline.length)==2)
    //

	//{
    //

	//   if(document.transferencia.ChkOnline[0].checked)
    //

	//     document.transferencia.TIPOTRANS.value="LINEA";
    //

   //    else
    //

	//     document.transferencia.TIPOTRANS.value="ARCHIVO";
    //

   // }

	//else if  (eval(document.transferencia.ChkOnline.length)==3)

	//{
    //    if((document.transferencia.ChkOnline[0].checked)||(document.transferencia.ChkOnline[1].checked))
    // document.transferencia.TIPOTRANS.value="LINEA";

   //else
    // document.transferencia.TIPOTRANS.value="ARCHIVO";

	//}
      document.transferencia.TIPOTRANS.value="LINEA";


  }

  if (document.transferencia.INICIAL.value=="NO")

  {

     if(document.transferencia.TIPOTRANS.value=="LINEA")

     {

	   if (eval(document.transferencia.contador1.value)>=eval(document.transferencia.NMAXOPER.value))

	   {

             result=false;
	      cuadroDialogo("Solo se permiten adicionar hasta "+document.transferencia.NMAXOPER.value +" operaciones en linea",1);
	   }

     }

  }

  return result;

}



function EsNumero(Dato){
    var retorno=false;
	var CadenaNumeros= "0123456789";
	var EsteCaracter;
	var Contador = 0;
	for (var i=0; i < Dato.length; i++) {
		EsteCaracter = Dato.substring(i , i+1);
		if (CadenaNumeros.indexOf(EsteCaracter) != -1)
			Contador ++;
	}
	if (Contador == Dato.length)
	 {
		//Todos los caracteres son numeros!
        //alert("!Esta bien ! es un numero.");
	    retorno=LongitudCorrecta(document.transferencia.destinoNoReg.value)
 	 }
	else
	 {
		cuadroDialogo("N&uacute;mero de cuenta inv&aacute;lido",3);
		//document.transferencia.monto.focus();
	 }

	return retorno;
}




function validarctasregistradas()


{


  var result=true;


  // Modificacion para integracion

  //var //ctaorigen=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value;
   //
  //var //ctadestino=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value;


  var ctaorigen=document.transferencia.origen.value;


  var ctadestino=document.transferencia.destino.value;





  result = validctasdif (ctaorigen,ctadestino);


  if (result == true)


    result=validar_tipo_cuentasRegis(ctaorigen,ctadestino);


  if (result == true)
    result=EsAlfa(document.transferencia.concepto.value,"Concepto");

  if (result == true)
     result = validAmount (document.transferencia.montoMN.value,"IMPORTE MN");
  if (result == true)
     result = validAmount (document.transferencia.montoUSD.value,"IMPORTE Dlls");
  return result;


}



function validarctasnoregistradas()
{


  var result=true;


  //modificacion para integracion

  // var ctaorigen=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value;


  var ctaorigen=document.transferencia.origen.value;


  var ctadestino=""+document.transferencia.destinoNoReg.value;





  if (ctadestino == "")


  {


	 cuadroDialogo("Debe indicar una cuenta de abono",3);


	 result = false;


  }


  else


  {


     result = EsNumero(ctadestino);


  	 if (result == true)


	     result = validctasdif(ctaorigen.substring(0,ctaorigen.indexOf("|")),ctadestino)


     if (result == true)


	    result=validar_tipo_cuentasNoRegis(ctaorigen,ctadestino);


     if (result == true)


  	    result=EsAlfa(document.transferencia.concepto.value, "Concepto");

 if (result == true)
       result = validAmount (document.transferencia.montoMN.value,"IMPORTE MN");
     if (result == true)
       result = validAmount (document.transferencia.montoUSD.value,"IMPORTE Dlls");

   }//fin de else


   return result;


}





/*function formato_rfc_ok(rfc)
{


  if (document.transferencia.RFC.value.length>11){


    formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/;


  }


  else {


	formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;


  }



  if (!formato.test(rfc.value))

      return false;

  return true;


}

*/

function formato_rfc_ok(rfc)
 {
   var fecha=6;

   var formato="";

   switch(rfc.value.length)
         {
           case 13: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
           case 10: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
           case 12: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
           case 9: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
    }
   if(formato=="")
     return false;
   if(rfc.value.length==9 || rfc.value.length==12)
     fecha=5;
   if(!formato.test(rfc.value))
     return false;

   var mes=rfc.value.substring(fecha,fecha+2);
   var dia=rfc.value.substring(fecha+2,fecha+4);
   if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
    {
      if(mes==2 && dia >=30)
       return false;
    }
   else
     return false;
   return true;
 }











var gConfirmacion= false;

function confirmacion()

{

/*

  var  cuenta=document.tabla_transferencias.arregloctas_destino2.value;

  var  des=cuenta;

  cuenta=cuenta.substring(0,cuenta.indexOf("|"));

  des=des.substring(des.indexOf("|")+1,des.length);

  des=des.substring(des.indexOf("|")+1,des.length-2);

  var imp=document.tabla_transferencias.arregloimportes2.value;

  imp=imp.substring(0,imp.length-1);*/


  result=validar2(document.tabla_transferencias);



  gConfirmacion = result;

  if ( result == true )
     cuadroDialogo(" \"El tipo de cambio de su cotizaci&oacute;n puede cambiar si han transcurrido m&aacute;s de 3 minutos.\" ",2);

}


function continua()
{
   if (gConfirmacion == true )
   {

      if (respuesta==1) document.tabla_transferencias.submit();
        else return false;
   }
   else
       return false;
}


function limpiar_datos()
{
   /*document.transferencia.concepto.value="";
   document.transferencia.monto.value="";
   document.transferencia.origen.selectedIndex=0;

   if(eval(document.transferencia.banderaNoReg.value)==eval(1))
   {
     if (document.transferencia.ChkOnline[0].checked)
     {

       document.transferencia.destino.value="";

       document.transferencia.textdestino.value="";
     }
     else if ((eval(document.transferencia.ChkOnline.length)==2)&&(document.transferencia.chkOnline[1].checked))
   	   document.transferencia.destinoNoReg.value="";

   }
   else
   {
     document.transferencia.destino.value="";
     document.transferencia.textdestino.value="";

   }
   document.transferencia.CLAVEESP.value="";
   document.transferencia.TIPOCAMBESP.value="";*/
}

function redondea(num)
{
  var a=0;
  var b=0;

  a=parseInt(Math.round(num*100));
  b=a/100;
  return b;
}

function ValidaRFC(rfc)
 {
   if(rfc.value.length>11)
     formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/;
   else
     formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;
   if(!formato.test(rfc.value))
     return false;

   var mes=rfc.value.substring(6,8);
   var dia=rfc.value.substring(8,10);
   if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
    {
      if(mes==2 && dia >=30)
       return false;
	 }
	else
	 return false;
   return true;
 }

function validar()
{
	var ov;
   if (document.transferencia.INICIAL.value=="NO")
    return false;


  if(eval(document.transferencia.banderaNoReg.value)==eval(1))
  {
    if (eval(document.transferencia.ChkOnline.length)==2)
    ov=1;
    else
     ov=2;
  }
  else
    ov=1;





	result=definir_tipo_transmision();

  if(result==true)
  {
     if(eval(document.transferencia.banderaNoReg.value)==eval(1))
	 {
        if (document.transferencia.ChkOnline[0].checked)
	     //document.transferencia.FileTrans.value = "";
         result=validarctasregistradas();
     }
	 else
	    result=validarctasregistradas();
  } //fin del if de ChkOnline ==0

  //if ((result==true)&&(document.transferencia.ChkOnline[ov].checked))
  //{
  //	if(document.transferencia.FileTrans.value == "")
  //	{
  //		cuadroDialogo("Debe de asignar un nombre de archivo",3);
  //		return  false;
  //	}
  //	else
  //	   return true;
  //}
  if( (result==true)&&
      (eval(document.transferencia.banderaNoReg.value)==eval(1)) )
  {
    if (document.transferencia.ChkOnline[1].checked)
   	 result=validarctasnoregistradas();

  }
  if ((result==true)&&(eval(document.transferencia.concepto.value.length)==eval(0)))
  {
    //adroDialogo("Se debe especificar el concepto de la operaci&oacute;n",3);
    //turn false;
    document.transferencia.concepto.value="OPERACION CAMBIOS";
  }
  if ((result==true)&&(document.transferencia.CHRCF[0].checked))
  {
     document.transferencia.concepto.value=document.transferencia.concepto.value.toUpperCase();
	 if (eval(document.transferencia.RFC.value.length)==eval(0))
	 {
        cuadroDialogo("Se debe especificar el RFC del Beneficiario",3);
        return false;
	 }
     if (eval(document.transferencia.IVA.value.length)==eval(0))
	 {
        cuadroDialogo("Se debe especificar el IVA",3);
        return false;
	 }
 	 result=EsAlfa(document.transferencia.RFC.value,"RFC");
     if (result==true)
       result = validAmount (document.transferencia.IVA.value,"IMPORTE IVA");
     if (result==true)
	 {
		if(!ValidaRFC(document.transferencia.RFC))
		{
	           cuadroDialogo("el RFC no es v&aacute;lido.",1);
			   return false;
		}
    }
  }
  if((result==true)&&(document.transferencia.CLAVEESP.value.length>0))
  {

	 if (EsAlfa(document.transferencia.CLAVEESP.value)==false)
	  {
        cuadroDialogo("Tipo de dato inv&aacute;lido en clave de tipo de cambio especial",3);
        return false;
 	  }
	  if(result==true)
	   result = validAmount (document.transferencia.TIPOCAMBESP.value,"Tipo de Cambio Especial");
  }
  if ((result == true)&&(document.transferencia.contador1.value>0))
    definir_valores_checkbox();
  return result;

}



//var impdialog;
function validar2(forma)
{
   /*var cont=0;
   if (document.tabla_transferencias.contador2.value>0)
   {
      if(document.tabla_transferencias.imptabla.value=="SI")
	  {
         for(i2=0;i2<document.tabla_transferencias.length;i2++)
           if(document.tabla_transferencias.elements[i2].type=='checkbox')
             if(document.tabla_transferencias.elements[i2].checked==true)
                cont++;
         if(cont == 1)
         {
		   definir_valores_checkbox();
		   result=true;
  	   	   impdialog=1;
		   document.tabla_transferencias.submit();

         }
         else
	     {
           cuadroDialogo("Debe seleccionar  una transferencia.",3);
           result = false;
	     }
      }

   }
   else
   {
      cuadroDialogo("No se ha adicionado ninguna transferencia",3);
      result= false;
   }*/
   definir_valores_checkbox();
   result=true;
  // impdialog=1;

  /* cuadroDialogo(" \"El tipo de cambio de su cotizaci&oacute;n puede cambiar si han transcurrido m&aacute;s de 3 minutos.\" ",2);*/

   //document.tabla_transferencias.submit();
   return result;
}






function verificartipo()

{

    var respuesta=true;
    if (eval(document.transferencia.contador1.value)>0)
    {
	   respuesta=false;
	   cuadroDialogo("S&oacute;lo se pueden agregar transferencias en l&iacute;nea",3);
	   if(eval(document.transferencia.banderaNoReg.value)==eval(1))
	     document.transferencia.ChkOnline[0].checked=true;
       else
	     document.transferencia.ChkOnline.value=true;
    }
    return respuesta;


}

function VerificaClaveEspecial()
{
   if (document.transferencia.CLAVEESP.value.length>0)
      return true;
   else
   {
      document.transferencia.TIPOCAMBESP.value="";
	  document.transferencia.CLAVEESP.focus();
	  cuadroDialogo("Se debe definir la clave especial",3);
	  return false;
   }
}


//*****inicia Cambio para comprobante fiscal
function checaestatus(campo)
{
  var result=true;

  if (eval(document.transferencia.CHRCF.value)==1)
  {
    result=true;
  }
  else
  {

	document.transferencia.RFC.value="";
	document.transferencia.IVA.value="";
    campo.blur();
	result=false;
  }
  return result;
}

function inicial()

{

  document.transferencia.CHRCF[1].checked=true;

  document.transferencia.CHRCF.value=0;

  document.transferencia.RFC.focus();

  document.transferencia.IVA.focus();

  return true;

}





function limpiar_cf()

{

  document.transferencia.RFC.value="";

  document.transferencia.IVA.value="";

  document.transferencia.CHRCF.value=0;

  return true;

}





function asignar_valor()

{

  document.transferencia.CHRCF.value=1;

}

function trimString (str)
{
    str = this != window? this : str;
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}


function CambiarImporteUSD()
{
  var montoMN=document.transferencia.montoMN.value;


  if  (trimString(montoMN)!="")
  {
    var result=validAmount (montoMN,"Importe MN");

	if(result==true)
	{

      if((document.transferencia.montoMNreal.value!="")&&
	     (redondea(document.transferencia.montoMNreal.value)==eval(document.transferencia.montoMN.value) ))
         montoMN=document.transferencia.montoMNreal.value;

	  if(eval(document.transferencia.TIPOCAMBESP.value)>0)
      {
	      document.transferencia.montoUSDreal.value=montoMN/document.transferencia.TIPOCAMBESP.value; document.transferencia.montoUSD.value=redondea(document.transferencia.montoUSDreal.value)
      }	
	  else if(eval(document.transferencia.tcenl.value)>0)
      {
	      document.transferencia.montoUSDreal.value=montoMN/document.transferencia.tcenl.value; document.transferencia.montoUSD.value=redondea(document.transferencia.montoUSDreal.value)
      }
	  else
	  {
	       document.transferencia.montoUSD.value=0.00;
		   document.transferencia.montoUSDreal.value=0.00;
	  }

    }
    document.transferencia.capUsdEsp.value="0";
  }
}

function CambiarImporteMN()
{
  var montoUSD=document.transferencia.montoUSD.value;

  if  (trimString(montoUSD)!="")
  {
    var result=validAmount (montoUSD,"Importe Dlls");

    if((document.transferencia.montoUSDreal.value!="")&&
	  (redondea(document.transferencia.montoUSDreal.value)==eval(document.transferencia.montoUSD.value) ))
      montoUSD=document.transferencia.montoUSDreal.value;

	if(result==true)
    {
      if(eval(document.transferencia.TIPOCAMBESP.value)>0)
      {
	  	document.transferencia.montoMNreal.value=montoUSD*document.transferencia.TIPOCAMBESP.value;
	  	document.transferencia.montoMN.value=redondea(document.transferencia.montoMNreal.value);
	  }
	  else if(eval(document.transferencia.tcenl.value)>0)
      {
	  	document.transferencia.montoMNreal.value=montoUSD*document.transferencia.tcenl.value;
	  	document.transferencia.montoMN.value=redondea(document.transferencia.montoMNreal.value);
	  }
	  else
	  {
		document.transferencia.montoMN.value=0.00;
		document.transferencia.montoMNreal.value=0.00;
	  }
    }
  document.transferencia.capUsdEsp.value="1";
  }

}




function CambiarImporteIVA()
{
  var montoIVA=document.transferencia.IVA.value;
  if  (trimString(montoIVA)!="")
  {
    var result=validAmount (montoIVA,"Importe IVA");
    if(result==true)
     document.transferencia.IVA.value=redondea(montoIVA)
  }
}





<!-- *********************************************** -->


<!-- modificación para integración pva 07/03/2002    -->



<!-- *********************************************** -->


var ctaselec;

var ctadescr;

var ctatipre;

var ctatipro;

var ctaserfi;

var ctaprod;

var ctasubprod;

var opcion;

var tramadicional;

var cfm;


function CuentasCargo()
{

   opcion=1;

   //PresentarCuentas();
   msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
   msg.focus();

}


function CuentasAbono()
{
   opcion=2;
   //PresentarCuentas();
    msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
    msg.focus();


}

/*
function PresentarCuentas()
{
 msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");


  msg.focus();

}
*/

function actualizacuenta()
{

  if(opcion==1)

  {

      document.transferencia.origen.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|"+ctatipro+"|";

      document.transferencia.textorigen.value=ctaselec+" "+ctadescr;

  }

  else

  {

  	  document.transferencia.destino.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|"+ctatipro+"|";
      document.transferencia.textdestino.value=ctaselec+" "+ctadescr;

  }
}




function EnfSelCta()

{

   //document.transferencia.textorigen.value="";

   //document.transferencia.textdestino.value="";

}

</SCRIPT>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript">
<!--

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%
String newMenu = (String)session.getAttribute("newMenu");
if(newMenu!=null)
	out.println(newMenu);
%>

//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');EnfSelCta();" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	  <!-- MENU PRINCIPAL -->
	  <%
	  String MenuPrincipal = (String)session.getAttribute("MenuPrincipal");
	  if(MenuPrincipal!=null)
		out.println(MenuPrincipal);
	  %>
	</td>
  </TR>
</TABLE>
<%
String Encabezado = "";
if((String)request.getAttribute("Encabezado")!=null)
  Encabezado=(String)request.getAttribute("Encabezado");
else
  Encabezado=(String)session.getAttribute("Encabezado");
out.println(Encabezado);
%>
<!-- CONTENIDO INICIO -->
<table width="760" border="0" cellspacing="0" cellpadding="0">
<FORM  NAME="transferencia" METHOD=POST  ACTION="MIPD_cambios?ventana=2" onSubmit="return validar();" onload="return inicial();">

<input TYPE="HIDDEN" name="tcc_compra1"		 VALUE=<%=request.getAttribute("tcc_compra1")%>>
<input TYPE="HIDDEN" name="tcc_venta1"		 VALUE=<%=request.getAttribute("tcc_venta1")%>>
<input TYPE="HIDDEN" name="tcv_compra1"		 VALUE=<%=request.getAttribute("tcv_compra1")%>>
<input TYPE="HIDDEN" name="tcv_venta1"		 VALUE=<%=request.getAttribute("tcv_venta1")%>>
<input TYPE="HIDDEN" name="tcc_aplicar1"	 VALUE=<%=request.getAttribute("tcc_aplicar1")%>>
<input TYPE="HIDDEN" name="tcv_aplicar1"	 VALUE=<%=request.getAttribute("tcv_aplicar1")%>>
<INPUT TYPE="HIDDEN" NAME="fecha_programada" VALUE=0>
<INPUT TYPE="HIDDEN" NAME="montostring"		 VALUE=0>
<INPUT TYPE="HIDDEN" NAME="fac_programadas1" VALUE=<%=request.getAttribute("fac_programadas1")%>>
<INPUT TYPE="HIDDEN" NAME="divisa"			 VALUE=<%=request.getAttribute("divisa")%>>
<INPUT TYPE="HIDDEN" NAME="trans"			 VALUE=<%=request.getAttribute("trans")%>>

<INPUT TYPE="HIDDEN" NAME="ctasel" 		     VALUE=<%=request.getAttribute("ctasel")%>>
<INPUT TYPE="HIDDEN" NAME="impsel"			 VALUE=<%=request.getAttribute("impsel")%>>
<INPUT TYPE="HIDDEN" NAME="cve_esp1"		 VALUE=<%=request.getAttribute("cve_esp1")%>>

<INPUT TYPE="HIDDEN" NAME=montoUSDreal value="">
<INPUT TYPE="HIDDEN" NAME=montoMNreal value="">
<INPUT TYPE="HIDDEN" NAME=capUsdEsp   value="">


<tr>
  <td align="center">
    <table width="530" border="0" cellspacing="2" cellpadding="3" >
    <tr>
	    <td class="tittabdat" colspan="2"> Tipos de Cambios </td>
	  </tr>
	  <tr>
	    <td class="textabdatcla" noWrap>Ventanilla:
		  <input type="text" name="tcvent" value="<%=request.getAttribute("tcv_aplicar1")%>" onFocus='blur();'>
		</td>
		<td class="textabdatcla" noWrap>Enlace:
		  <input type="text" name="tcenl"  value="<%=request.getAttribute("tcc_aplicar1")%>" onFocus='blur();'>
		</td>
	  </tr>
	</table>
	<table width="490" border="0" cellspacing="2" cellpadding="3">
	  <tr>
	 	<td class="tittabdat" colspan="2"> Capture los datos de su transferencia  </td>
	  </tr>
		<tr align="center">
		  <td class="textabdatcla" valign="top" colspan="2">
			<table width="480" border="0" cellspacing="0" cellpadding="0">
			  <tr valign="top">
				<td width="270" >
				  <table width="270" border="0" cellspacing="5" cellpadding="0">
					<tr>
					  <td class="tabmovtexbol" width="158" nowrap>Cuenta de cargo:</td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" width="158" nowrap>
						<!--Modificacion para integracion
						<SELECT ID="FormsComboBox1" NAME="origen">
						  <%
						  String cuentas_origen = (String)request.getAttribute("cuentas_origen");
						  if(cuentas_origen!=null)
							out.println(cuentas_origen);
						  %>
    					</SELECT>-->
						<input type="text" name=textorigen  class="tabmovtexbol" maxlength=30 size=30 onfocus="blur();"
						value=<% if(request.getAttribute("textctasel")!=null)
						           out.println(request.getAttribute("textctasel"));
                                 else
								   out.println("");
				              %>
						>
					    <!--<A HREF="javascript:CuentasCargo();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>-->
					    <input type="hidden" name="origen"
						value=<% if(request.getAttribute("ctasel")!=null)
						           out.println(request.getAttribute("ctasel"));
                                 else
								   out.println("");
					          %>
						>
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap>Cuenta abono:</td>
					</tr>
					<tr valign="middle">
					  <td class="tabmovtexbol" nowrap>
						<input type="radio" name="ChkOnline" value="OnLine" checked>Registrada
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap>
						<!--Modificacion para integracion
						<SELECT ID="FormsComboBox4" NAME="destino">
						  <%
						  String cuentas_destino = (String)request.getAttribute("cuentas_destino");
						  if(cuentas_destino!=null)
							out.println(cuentas_destino);
						  %>
    					</SELECT>-->
						<input type="text" name=textdestino  class="tabmovtexbol" maxlength=30 size=30 onfocus="blur();" value="">
					    <A HREF="javascript:CuentasAbono();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
					    <input type="hidden" name="destino" value="">
					  </td>
					</tr>
					<%
					String NoRegistradas = (String)request.getAttribute("NoRegistradas");
					if(NoRegistradas!=null)
					  out.println(NoRegistradas);
					%>
					<!--					<tr valign="middle">
					  <td class="tabmovtexbol" nowrap>
						<input type="radio" value="OnFile" name="ChkOnline" onClick="return verificartipo();">Importar
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap>
						<INPUT NAME="FileTrans" TYPE="file" size="20">
					  </td>
					</tr>-->
					<tr>
					</tr>
				  </table>
				</td>
				<td width="200" align="right">
				  <table width="180" border="0" cellspacing="5" cellpadding="0">
					<tr>
					  <td class="tabmovtexbol" nowrap>Importe MN:&nbsp;
					  <INPUT TYPE=TEXT NAME="montoMN" size="15"
					MAXLENGTH="15"      class="tabmovtexbol"
					value="" onBlur="CambiarImporteUSD();">
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap> Importe Dlls:
						<INPUT TYPE=TEXT NAME="montoUSD" size="15" MAXLENGTH="15" class="tabmovtexbol" VALUE="" onBlur="CambiarImporteMN();">
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap>Concepto:</td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap>
						<INPUT TYPE="TEXT" NAME="concepto" size="22" class="tabmovtexbol" MAXLENGTH="30">
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap>Requiere Comprobante Fiscal
						<INPUT TYPE =RADIO  NAME=CHRCF VALUE=1 onclick="asignar_valor();">Si
						<INPUT TYPE =RADIO  NAME=CHRCF VALUE=0 CHECKED onClick="limpiar_cf();">No
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap>R.F.C. Beneficiario:
						<input type="TEXT" name="RFC" value="" size=13 MAXLENGTH=13 onFocus="checaestatus(this);">
					  </td>
					</tr>
					<tr>
					  <td class="tabmovtexbol" nowrap> Importe I.V.A.:&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="TEXT" name="IVA" value="" size=13 MAXLENGTH="12" onFocus="checaestatus(this);" onBlur="CambiarImporteIVA();">
					  </td>
					</tr>
					<tr>
					</tr>
				  </table>
        		</td>
			  </tr>
			  <tr>

			  <td class="tabmovtexbol">
			  <table>
			    <tr>
			    <td class="tabmovtexbol">Clave: </td>
				<td class="tabmovtexbol"><input type="TEXT" name="CLAVEESP" value="" size=8 maxlength=8></td>
			    </tr>
			    <tr>
			    <td class="tabmovtexbol">Tipo de Cambio:</td>
				<td class="tabmovtexbol"><input type="TEXT" name="TIPOCAMBESP" value="" size=8 maxlength=15 onFocus=VerificaClaveEspecial(); onBlur="CambiarImporteMN();">
				</td>
			    </tr>
              </table>
			  </td>
			  <td>
			  </td>

			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	  <table width="490" border="0" cellspacing="2" cellpadding="3">
		<tr>
		  <td class="textabref">D&eacute; click sobre el bot&oacute;n VALIDAR
					para registrar su transferencia.
		  </td>
		</tr>
	  </table>
	  <br>
	  <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" >
		<tr>
		  <td align="right" valign="top"  width="85">
			<!--<input type="image" border="0"  name="Agregar" src="/gifs/EnlaceMig/gbo25290.gif" width="89" height="22" alt="Agregar"> -->
			<input type="image" border="0"  name="Agregar" src="/gifs/EnlaceMig/gbo25222.gif" width="89" height="22" alt="Validar">

		  </td>
		  <!--<td  valign="top"  width="76">
		    <a href="javascript:limpiar_datos();"><img  border="0" src="/gifs/EnlaceMig/gbo25250.gif" ></a>
		  </td>-->
		</tr>
	  </table>
	  <br>
<!----------------------------------------------------------->
	<INPUT TYPE="HIDDEN" NAME="arregloctas_origen1"  VALUE=<%=request.getAttribute("arregloctas_origen1")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloctas_destino1" VALUE=<%=request.getAttribute("arregloctas_destino1")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloimportes1"     VALUE=<%=request.getAttribute("arregloimportes1")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloconceptos1"    VALUE=<%=request.getAttribute("arregloconceptos1")%>>
	<INPUT TYPE="HIDDEN" NAME="arreglorfcs1"		 VALUE=<%=request.getAttribute("arreglorfcs1")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloivas1"		 VALUE=<%=request.getAttribute("arregloivas1")%>>
	   <INPUT TYPE="HIDDEN" NAME="arregloclesp1"     VALUE=<%=request.getAttribute("arregloclesp1")%>>   <INPUT TYPE="HIDDEN" NAME="arreglotipcambesp1"  VALUE=<%=request.getAttribute("arreglotipcambesp1")%>>  <INPUT TYPE="HIDDEN" NAME="contador1"            VALUE=<%=request.getAttribute("contador1")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloestatus1"      VALUE=<%=request.getAttribute("arregloestatus1")%>>
	<INPUT TYPE="HIDDEN" NAME="TIPOTRANS"			 VALUE=<%=request.getAttribute("TIPOTRANS")%>>
	<INPUT TYPE="HIDDEN" NAME="INICIAL"				 VALUE=<%=request.getAttribute("INICIAL")%>>
	<INPUT TYPE="HIDDEN" NAME="NMAXOPER"			 VALUE=<%=request.getAttribute("NMAXOPER")%>>
	<!--INPUT TYPE="HIDDEN" NAME="ctasel"				 VALUE=<%=request.getAttribute("ctasel")%>>
	<INPUT TYPE="HIDDEN" NAME="impsel"				 VALUE=<%=request.getAttribute("impsel")%>>
	<INPUT TYPE="HIDDEN" NAME="cve_esp1"			 VALUE=<%=request.getAttribute("cve_esp1")%>>

	<INPUT TYPE="HIDDEN" NAME="cve_esp2"			 VALUE=<%=request.getAttribute("cve_esp2")%>>
	<!--INPUT TYPE="text" NAME="ctasel2"				 VALUE=<%=request.getAttribute("ctasel2")%>
	<INPUT TYPE="HIDDEN" NAME="impsel2"				 VALUE=<%=request.getAttribute("impsel2")%>>
	<INPUT TYPE="HIDDEN" NAME="tcv_venta2"			 VALUE=<%=request.getAttribute("tcv_venta2")%>>
	<INPUT TYPE="HIDDEN" NAME="tcv_compra2"			 VALUE=<%=request.getAttribute("tcv_compra2")%>-->

</FORM>

<FORM NAME="tabla_transferencias" METHOD=POST  ACTION="MIPD_cambios?ventana=3" onSubmit="return confirmacion();">
<table width="750" border="0" cellspacing="2" cellpadding="3">
	<tr>
		<td>&nbsp;</td>
		<td class="textabref" colspan="6">
		<%
               if (request.getAttribute("contadoroperaciones")!= null) {
               out.println(request.getAttribute("contadoroperaciones"));
	           }
        %>

		</td>
	</tr>
		<%
               if (request.getAttribute("tabla")!= null) {
               out.println(request.getAttribute("tabla"));
	           }
        %>

</table>
<br>
<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
	<tr>
		<%
               if (request.getAttribute("botontransferir")!= null) {
               out.println(request.getAttribute("botontransferir"));
	           }
        %>
	</tr>
</table>
<br>
</td>
</tr>
	<INPUT TYPE="HIDDEN" NAME="arregloctas_origen2"  VALUE=<%=request.getAttribute("arregloctas_origen2")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloctas_destino2" VALUE=<%=request.getAttribute("arregloctas_destino2")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloimportes2"	 VALUE=<%=request.getAttribute("arregloimportes2")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloconceptos2"    VALUE=<%=request.getAttribute("arregloconceptos2")%>>
	<INPUT TYPE="HIDDEN" NAME="arreglorfcs2"		 VALUE=<%=request.getAttribute("arreglorfcs2")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloivas2"		 VALUE=<%=request.getAttribute("arregloivas2")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloclesp2"     VALUE=<%=request.getAttribute("arregloclesp2")%>>
	<INPUT TYPE="HIDDEN" NAME="arreglotipcambesp2"  VALUE=<%=request.getAttribute("arreglotipcambesp2")%>>
	<INPUT TYPE="HIDDEN" NAME="contador2"			 VALUE=<%=request.getAttribute("contador2")%>>
	<INPUT TYPE="HIDDEN" NAME="arregloestatus2"      VALUE=<%=request.getAttribute("arregloestatus2")%>>
	<INPUT TYPE="HIDDEN" NAME="imptabla"			 VALUE=<%=request.getAttribute("imptabla")%>>
	<INPUT TYPE="HIDDEN" NAME="trans"				 VALUE=<%=request.getAttribute("trans")%>>
	<INPUT TYPE="HIDDEN" NAME="TIPOTRANS"			 VALUE=<%=request.getAttribute("TIPOTRANS")%>>

	<INPUT TYPE="HIDDEN" NAME="tcv_compra2"			 VALUE=<%=request.getAttribute("tcv_compra2")%>>
	<INPUT TYPE="HIDDEN" NAME="tcv_venta2"			 VALUE=<%=request.getAttribute("tcv_venta2")%>>
	<INPUT TYPE="HIDDEN" NAME="TIPOTRANS"			 VALUE=<%=request.getAttribute("TIPOTRANS")%>>
	<INPUT TYPE="HIDDEN" NAME="ctasel2"				 VALUE=<%=request.getAttribute("ctasel2")%>>
	<INPUT TYPE="HIDDEN" NAME="impsel2"				 VALUE=<%=request.getAttribute("impsel2")%>>

	<INPUT TYPE="HIDDEN" NAME="ctasel"				 VALUE=<%=request.getAttribute("ctasel")%>>
	<INPUT TYPE="HIDDEN" NAME="impsel"				 VALUE=<%=request.getAttribute("impsel")%>>
	<INPUT TYPE="HIDDEN" NAME="cve_esp1"			 VALUE=<%=request.getAttribute("cve_esp1")%>>
	<INPUT TYPE="HIDDEN" NAME="cve_esp2"			 VALUE=<%=request.getAttribute("cve_esp2")%>>

</FORM>

</table>
	<!-- CONTENIDO FINAL -->
</body>
</html>

<Script language = "JavaScript">
<!--
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>