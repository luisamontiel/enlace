<html>
<head>
	<title>Cotizar - Enviar</TITLE>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript">
	function exportarArchivoPlano() {

 	var test = document.getElementsByName("tipoExportacion");
    var sizes = test.length;
    var extencion;
    for (i=0; i < sizes; i++) {
            if (test[i].checked==true) {
        	extencion = test[i].value;
        }
    }
	window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=ARCHIVO&em=ExportModel&af=af&tipoArchivo=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
}
</script>
<script type="text/javascript">

var respuesta=0;

function continua()
{
  if(respuesta==1)
    document.tabla.submit();
}

function Enviar()
{
  //cuadroDialogo("Desea enviar las Transferencias?",2);
  <%
	BaseResource ses = (BaseResource) request.getSession ().getAttribute ("session");
	if (ses.getToken().getStatus() == 1) {%>
  		respuesta = 1;
  		continua();
  	<%} else {%>
	  	cuadroDialogo("Desea enviar las Transferencias?",2);
  	<%}%>
}

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%= request.getAttribute("InfoUser") %>
/********************************************************************************/
<!--
<%= request.getAttribute("strScript") %>
//-->
<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<%-- MENU PRINCIPAL --%>
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

 <FORM NAME="tabla" method="post" onSubmit="return ValidaTabla(this);" action="MDI_Enviar">
  <p>
   <%-- Datos del Usuario --%>
   <%--
   <table border=0 align=center>
   <tr>
     <th bgcolor='#DDDDDD'><font color=black>Datos del Usuario</font></th>
   </tr>
   <tr>
     <td align="center">
       <font size="2" face="Arial, Helvetica, sans-serif">
	     <b>Contrato: </b><%= request.getAttribute("NumContrato") %> <%= request.getAttribute("NomContrato") %>
	   </font>
	 </td>
   </tr>
   <tr>
     <td bgcolor='#FFFFFF'>
	   <b>Usuario: </b><%= request.getAttribute("NumUsuario") %> <%= request.getAttribute("NomUsuario") %>
	 </td>
   </tr>
   <tr>
     <td><br></td>
   </tr>
   </table>
   //--%>

  <table cellspacing="0" cellpadding="0" style=" border: 0; width: 900px;">
   <tr>
   	<td style=" width: 50px;"></td>
    <td align=right class='textabref'>

	</td>
   </tr>

   <tr>
   	<td style=" width: 50px;"></td>
    <td align=center>
     <%= request.getAttribute("Tabla") %>

    </td>
   </tr>
    <%= request.getAttribute("BotonesExportar") %>
  </table>

  <input type="hidden" name="TotalTrans" value='<%= request.getAttribute("TotalTrans") %>'>
  <input type="hidden" name="Lineas" value='<%= request.getAttribute("Lineas") %>'>
  <input type="hidden" name="Importe" value='<%= request.getAttribute("Importe") %>'>

  <input type="hidden" name="TransReg" value='<%= request.getAttribute("TransReg") %>'>
  <input type="hidden" name="TransNoReg" value='<%= request.getAttribute("TransNoReg") %>'>

  <p>
	<%-- INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
	se comentan botones impresion y enviar, se incluye el jsp de token--%>
<%--   <table border=0 align=center cellpadding=0 cellspacing=0>   --%>
<%--    <tr> --%>
<%--      <td><a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" border=0></a></td> --%>
<%-- 	 <td><a href="javascript:Enviar();"><img src="/gifs/EnlaceMig/gbo25360.gif" border=0></a></td> --%>
<%--    </tr> --%>
<%--   </table> --%>
 </FORM>
	 <%-- INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token --%>
	<br>
	</br>
	<table style=" border: 0;">
		<tr>
			<td style=" width: 140px;"></td>
	 		<td colSpan="2" align="center">
				<%@ include file="/jsp/ValidaOTPConfirmUnificado.jsp" %>
	   		</td>
	 	</tr>
	</table>
	<%-- FIN CAMBIO PYME FSW - INDRA MARZO 2015 --%>

</body>
</html>