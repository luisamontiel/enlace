<%@ page import="java.awt.image.BufferedImage" %>
<%@ page import="java.util.List, javax.servlet.http.HttpServletRequest" %>
<%@ page import="java.util.ArrayList, mx.altec.enlace.beans.ImagenDTO, mx.altec.enlace.beans.PreguntaDTO" %>


<html>
<head>
	<title>Enrolamiento de Usuario</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>

<script language="javascript" src="/EnlaceMig/pm_fp.js"></script>

<link href="/EnlaceMig/consultas.css" rel="stylesheet" type="text/css" id = "estilos">

<script language="javaScript">

<%if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));%>

/******************  ********* ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
       
       	String respuesta  = request.getAttribute("respuesta") != null ? 
		request.getAttribute("respuesta").toString():"";
%>


function seleccionarImagen(idSeleccion) {
	var divEls = document.getElementsByTagName("img");
	var i = 0;
	
	
	for(i=0;i<divEls.length;i++) {
	       if(divEls[i].id != null || divEls[i].id != "") {
	          
        		var ids = divEls[i].id;
        		try {
        			document.getElementById(ids).style.border="none";
    		    } catch(e){}
    		}
    		
	}
	document.getElementById(idSeleccion).style.border='thick solid #0000FF';
	document.getElementById("imagenID").value  = idSeleccion;
}

function seleccionarPregunta() {
	var divEls = document.getElementsByTagName("input");
	var contador = 0;
	var i = 0;
	var j = 0;
	
	for(i=0;i<divEls.length;i++) {
	       var divTmp = divEls[i].type;
	       var valID = divEls[i].id;
	       var valor = divEls[i].value;
		   var name = divEls[i].name;
	       if(name == "grupo1") {
	       		if(document.getElementById(valID).checked == true) {
	       			document.getElementById('preguntaID').value = valID;
	       			document.getElementById('preguntaText').value = valor;
	       			return true;
	       		}
	       }
    		
	}
}

function EnviarForma() {
	
	var imagenID = document.getElementById("imagenID").value;
	if(imagenID == "") {
		alert("Debe seleccionar una imagen");
		return;
	}
	
	validaMaquinaUsr();
	
	if(seleccionarPregunta()){
		
		if(document.getElementById('preguntaID').value == "") {
			alert("Debe seleccionar una pregunta.");
			return;
		}
		
		var resp = document.getElementById("idRespuesta").value;
		var respConf = document.getElementById("idConfirRespuesta").value;
		
		var resp = resp.toUpperCase();
		var respConf = respConf.toUpperCase();
		
		  if(resp != null && resp != "") {
		  	if(respConf != null && respConf != ""){
		  		if(resp == respConf){
		  			if(validaCampoConstrasena(resp)) {
		  				var respMAY = resp.toUpperCase();
		  				document.getElementById("idRespuesta").value = respMAY;
						document.getElementById("idConfirRespuesta").value = respMAY;
		  				document.forms["mandaEnrolar"].submit();
		  			} else {
		  				alert("Solo se permiten caracteres alfanum�ricos.");
		  				return;
		  			}
		  		}else{
		  			alert("La respuesta y su confirmaci�n no son iguales, verifique su respuesta.");
		  			return;
		  		}
		  	}else{
				alert("La confirmaci�n de respuesta no puede ser vac�a.");
				return;
		  	}
		  }else{
		  		alert("La respuesta no puede ser vac�a.");
		  		return;
		  }
	}else{
		alert("Debe seleccionar una pregunta.");
		return;
	}  
	
  
}

function validaMaquinaUsr() {

	var publico = document.getElementById('idPublico').value;
	var privado = document.getElementById('idPrivado').value;
	
	
	if(document.getElementById('idPublico').checked == true) {
		document.getElementById("dispocitivo").value = publico;
	} else {
		document.getElementById("dispocitivo").value = privado;
	}
}

function validaCampoConstrasena(valor) {
	var input = valor;
	var texto = input.toUpperCase();

	var regex = new RegExp('^[A-Z�0-9 ]*$');

	if(regex.test(texto)) {
		return true;
	} else return false;
}

function setDevicePrint() {
	var devicePrint = "";
	
	if(typeof(encode_deviceprint()) != 'undefined' ){
			devicePrint = encode_deviceprint();
			document.getElementById("devicePrinter").value = devicePrint;
		}
	
}


function limpiar() {

	document.getElementById('idRespuesta').value = "";
	document.getElementById('idConfirRespuesta').value = "";
	
	//Limiar radios
	limpiarRadios(document.mandaEnrolar.grupo1);
	
	for (i=0;i<document.mandaEnrolar.elements.length;i++)  {
        
      	if(document.mandaEnrolar.elements[i].type == "radio") {	
      		document.mandaEnrolar.elements[i].checked=false;
        }
	}
	
	document.getElementById('idPrivado').checked = true;
	
	var idSeleccion = document.getElementById("imagenID").value;
	document.getElementById("imagenID").value  = "";
	if(idSeleccion != null && idSeleccion != "") {
		document.getElementById(idSeleccion).style.border='none';
	}
}

function limpiarRadios(grupoRadios) {
	for (i=0; i < grupoRadios.length; i++) {
 
    if (grupoRadios[i].checked == true) { 
          grupoRadios[i].checked = false; 
    } 
  }  
}

function inicio() {
	setDevicePrint();
	
	var respuesta = "<%=respuesta%>";
	
	if(respuesta == "exito" || respuesta == "fail" ) {
		window.location= '/Enlace/enlaceMig/csaldo1?prog=0&flujo=ECCC';
	}



}


</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" onload="javascript:inicio();">

<table border="0px" cellpadding="0px" cellspacing="0px" width="571px">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }


%>

<FORM  name="mandaEnrolar" id="mandaEnrolar" method=post action="EnrolamientoRSAServlet">
	<input type="hidden" id="devicePrinter" name="devicePrinter" value="">
	<input type="hidden" id="imagenID" name="imagenID" value="">
	<input type="hidden" id="preguntaID" name="preguntaID" value="">
	<input type="hidden" id="preguntaText" name="preguntaText" value="">
	<input type="hidden" id="dispocitivo" name="dispocitivo" value="">
	<input type="hidden" id="flujo" name="flujo" value="ENRO">
	<input type="hidden" id="respuesta" name="respuesta" value="<%=request.getAttribute("respuesta")%>">
	
  <p>
  	<table width="850px">
		<tr>
			<td>
			<table align="center" border="0px" cellspacing="0px" cellpadding="3px" class='textabdatcla' width="450px">
				<tr>
					<td  class="tittabdat"> Enrolamiento de Usuario  </td>
				</tr>
				
				<tr>
			 		<td><br></td>
		   		</tr>
				
				<tr>
		
					<td>
		    			<table border="1" width="400px" height="100px" align="center">
		    				<tr>
								<td colspan="1" class="tittabdat" align="center"> Seleccione una imagen:</td>
							</tr>
		    				<tr>
		    					<td width="400px" height="300px" align="center">
										<table height="100px" align="center" style="overflow:scroll; ">
				    						 
				    						<%
				    							List<ImagenDTO> lista = new ArrayList<ImagenDTO>();
				    							try {
				    								lista = (List)request.getAttribute("listaImages");
				    							} catch(Exception e){}
				    							if(lista != null && lista.size() > 0) {
				    						 	for(int i = 0 ; i < lista.size(); i++) { 
				    					
				    						%>	
				    							
				    							<tr>
				    								<td width="110px" height="110px" style="vertical-align: middle;">
				    									<img src="<%=request.getContextPath()%>/GeneraImagenServlet?val=<%=i%>"  id="<%=lista.get(i).getIdImagen()%>" alt="Red dot" align="middle" onclick="javascript:seleccionarImagen(this.id);"/>
				    								</td>
				    								<%i++;%>
				    								<td width="110px" height="110px" style="vertical-align: middle;">
				    									<img src="<%=request.getContextPath()%>/GeneraImagenServlet?val=<%=i%>"  id="<%=lista.get(i).getIdImagen()%>" alt="Red dot" align="middle" onclick="javascript:seleccionarImagen(this.id);"/>
				    								</td>
				    								<%i++;%>
				    								<td width="110px" height="110px" style="vertical-align: middle;">
				    									<img src="<%=request.getContextPath()%>/GeneraImagenServlet?val=<%=i%>"  id="<%=lista.get(i).getIdImagen()%>" alt="Red dot" align="middle" onclick="javascript:seleccionarImagen(this.id);"/>
				    								</td>
				    								<%i++;%>
				    								<td width="110px" height="110px" style="vertical-align: middle;">
				    									<img src="<%=request.getContextPath()%>/GeneraImagenServlet?val=<%=i%>"  id="<%=lista.get(i).getIdImagen()%>" alt="Red dot" align="middle" onclick="javascript:seleccionarImagen(this.id);"/>
				    								</td>
				    								<%i++;%>
				    								<td width="110px" height="110px" style="vertical-align: middle;">
				    									<img src="<%=request.getContextPath()%>/GeneraImagenServlet?val=<%=i%>"  id="<%=lista.get(i).getIdImagen()%>" alt="Red dot" align="middle" onclick="javascript:seleccionarImagen(this.id);"/>
				    								</td>
				    							</tr>
				    						
				    						 	<% }} %>
				    						 
				    						 
				    					</table>
			    				</td>
		    				</tr>
		    			</table>
		    			<table border="1" width="400px" align="center">
		    				<tr>
								<td colspan=2 class="tittabdat" align="center"> Seleccione una pregunta secreta:</td>
							</tr>
		    				<tr>
		    					<td>
		    					
		    					<table  align="center" >
		    						<%
		    							List<PreguntaDTO> listaPregunta = new ArrayList<PreguntaDTO>();
		    							try {
		    							listaPregunta = (List)request.getAttribute("listaPreguntas");
		    							} catch (Exception e) {}
		    							if(listaPregunta != null && listaPregunta.size() > 0) {
		    						 	for(PreguntaDTO dto:listaPregunta) { 
		    						 	
		    						 	
		    						 	
		    						%>	
		    							
		    							<tr>
		    								<td >
		    									<input type="radio" value="" name="grupo1" id="<%=dto.getPreguntaID() %>">
		    									
		    								</td>
		    								<td class="tabmovtexbol">
		    									<%=dto.getPreguntaTexto() %>
		    								</td>
		    							</tr>
		    						
		    						 	<% }}%>
		    						 	
		    						 
		    					</table>
		    				
		    					</td>
		    				</tr>
		    				<tr>
		    					<td>
			    					<table>
				    					<tr>
				    					<td class="tabmovtexbol" colspan="2">
				    						Respuesta:
				    					</td>
				    					<td>
				    						<input type="password"" name="idRespuesta" id="idRespuesta" size="20" maxlength="50">
				    					</td>
				    					</tr>
				    					<tr>
				    						<td class="tabmovtexbol" colspan="2">
									 			Confirmar respuesta: 
									 		</td>
									 		<td>
									 			<input type="password" name="idConfirRespuesta" id="idConfirRespuesta" size="20" maxlength="50">
									 		</td>
				    						 		
				    					</tr>
				    				</table>
		    					<td>
		    				</tr>
		    				<tr>
		    					<td>
			    					<table>
				    					<tr>
				    						<td class="tabmovtexbol" colspan="2">
													 El equipo con el que esta trabajando es:		    						
				    						</td>
				    					</tr>
				    					<tr>
				    						<td class="tabmovtexbol">
				    						Publico: <input type="radio" id="idPublico" name="idGrupo" value="publico">
				    						Privado: <input type="radio" id="idPrivado" name="idGrupo" value="privado" checked="checked">
									 		</td>
				    					</tr>
				    				</table>
		    					<td>
		    				</tr>
		    			</table>
					</td>
		
				</tr>
				
				<tr>
				    <td colspan=2><br></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0px" cellpadding="0px" cellspacing="0px" align="center">
				   <tr>
				     <td><a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/gbo25280.gif" border=0></a></td>
				   </tr>
				</table>
			</td>
		</tr>
	</table>  

  <br>
  



</form>

</body>
</html>

