<%@page contentType="text/html"%>

<jsp:useBean id="listaErrores" class="java.util.ArrayList" scope="session"/>
<jsp:useBean id="tituloPag" class="java.lang.String" scope="session"/>
<jsp:useBean id="descripcion" class="java.lang.String" scope="session"/>
<% 
   Long numLineas = (Long)session.getAttribute("numLineas"); 
   Long numCorrectas = (Long)session.getAttribute("numCorrectas");
 %>

<%--!
    Long numLineas = null;
    Long numCorrectas = null;
--%>
<%
    //numLineas = (Long) session.getAttribute ("numLineas");
    //numCorrectas = (Long) session.getAttribute ("numCorrectas");
    
    if (null == numLineas) {
        numLineas = new Long (0);
    } else {
        session.removeAttribute ("numLineas");
    }
    if (null == numCorrectas) {
        numCorrectas = new Long (0);
    } else {
        session.removeAttribute ("numCorrectas");
    }
%>
<html>
<head>
    <title><%=tituloPag%></title>
    <LINK rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>
</head>
<body>
    <FORM>
        <TABLE border='0' width='420' class='textabdatcla' align='center'>
            <TR>
                <TH class='tittabdat'>Informaci&oacute;n del Archivo Importado</TH>
            </TR>
            <TR>
                <TD class='tabmovtex1' align='center'>
                    <TABLE border='0' align='center'>
                        <TR>
                            <TD class='tabmovtex1' align='center'>
                                <IMG src='/gifs/EnlaceMig/gic25060.gif'>
                            </TD>
                            <%
                                if (descripcion.equals ("")) {
                            %>
                            <TD class='tabmovtex1'>
                                <BR>Total de l&iacute;neas: <%=numLineas.longValue ()%>.
                                <BR>L&iacute;neas correctas: <%=numCorrectas.longValue ()%>
                                <BR>El archivo tiene errores y no se import&oacute;.<BR><BR>
                            </TD>
                            <%
                                } else {
                            %>
                            <TD class='tabmovtex1'>
                                <%=descripcion%>
                            </TD>
                            <%
                                }
                           %>
                        </TR>
                    </TABLE>
                </TD>
            </TR>
            <%
                            System.out.println ("Lineas erroneas: " + listaErrores.size ());
                            java.util.ListIterator liErrores = listaErrores.listIterator ();
                            mx.altec.enlace.bo.pdError Error;
                            while (liErrores.hasNext()) {
            			Object o = liErrores.next();
            			System.out.println ("Clase error: " + o.getClass ());
                                Error = (mx.altec.enlace.bo.pdError) o;
            %>
            <TR>
                <TD class='tabmovtex1'>
                    <BR><FONT color='RED'><B>Linea: <%=Error.getLinea ()%>  </B><%=Error.getError ()%></FONT>
                </TD>
            </TR>
            <%
                }
            %>
        </TABLE>
        <TABLE border='0' align='center'>
            <TR>
                <TD align='center'>
                    <BR>
                    <A href='javascript:window.close ();'>
                        <IMG src='/gifs/EnlaceMig/gbo25200.gif' border='0'>
                    </A>
                    <A href='javascript:self.print ();'>
                        <IMG src='/gifs/EnlaceMig/gbo25240.gif' border='0' alt='Imprimir' width='83' height='22'>
                    </A>
                </TD>
            </TR>
        </TABLE>
    </FORM>
</body>
</html>

<%
//session.removeValue ("listaErrores");
session.setAttribute("listaErrores",null);

%>
