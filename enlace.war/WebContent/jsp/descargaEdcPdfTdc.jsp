<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="mx.altec.enlace.utilerias.Global"%>


<html>
<head> <title>Descarga de Estados de Cuenta PDF</title>	
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/descargaEdcPdfTdc.js"></script>


<script type="text/javascript">
	
	<c:if test="${not empty requestScope.newMenu}">
		${requestScope.newMenu}
	</c:if>
</script>
<script type="text/javascript">


	<c:if test="${not empty TiempoTerminado}">
		<c:out value="${TiempoTerminado}"/>
	</c:if>		

	
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body   leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" 
	onLoad="<c:if test="${not empty requestScope.URLEdoCtaPDF}">
		<c:out value="${'modificaBanderaCarga()'}"/>
	</c:if>">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<c:if test="${not empty requestScope.MenuPrincipal}">
				${requestScope.MenuPrincipal}
			</c:if>		
   </td>
  </tr>
</table>
<c:if test="${not empty requestScope.Encabezado}">
	${requestScope.Encabezado}
</c:if>

<form  name="Datos" method="post" action="DescargaEdoCtaPDFServlet?ventana=2">
<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
	<p/>
	<table width="760" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
				<table >
					<input type="hidden" id="cuentaSeleccionada" name="cuentaSeleccionada" value="<%=request.getAttribute("cuentaSeleccionda")%>"/>
					<input type="hidden" id="cuentaTXT" name="cuentaTXT" value="<%=request.getAttribute("cambioCuentaTXT")%>"/>
					<input type="hidden" id="codigoCliente" name="codigoCliente" value="<%=request.getAttribute("codigoCliente")%>"/>
					<input type="hidden" id="numeroSecuencia" name="numeroSecuencia" value="<%=request.getAttribute("numeroSecuencia")%>"/>
					<input type="hidden" id="cuentasTarjetas" name="cuentasTarjetas" value="<%=request.getAttribute("cuentaFormatoOriginal")%>"/>
					<input type="hidden" id="periodosSoliciar" name="periodosSoliciar" value=""/>
					<input type="hidden" id="cadenaValores" name="cadenaValores" value=""/>
					<input type="text" id="idBanderaCarga" name="banderaCarga" value="0" style="display: none"/>

					<tr class="tabdatfonazu">
						<th class="tittabdat"> Cr&eacute;dito</th>
						<th class="tittabdat"> Titular de la Cuenta </th>
						<th class="tittabdat"> Domicilio Registrado </th>
					</tr>
				  	<tr class="textabdatobs">
						<td class="tabmovtex">
							<c:if test="${not empty requestScope.cuentaSeleccionda}">
								${requestScope.cuentaSeleccionda}
							</c:if>
							
						</td>
						<td class="tabmovtex">
							<c:if test="${not empty requestScope.titularCuenta}">
								${requestScope.titularCuenta}
							</c:if>
							
						</td>
						<td class="tabmovtex">
							<c:if test="${not empty requestScope.domicilioRegistrado}">
								${requestScope.domicilioRegistrado}
							</c:if>							
						</td>
				  	</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td colspan="4" class="texencconbol">
						<c:if test="${not empty requestScope.MENSAJE_PROBLEMA_DATOS}">
								${requestScope.MENSAJE_PROBLEMA_DATOS}
							</c:if>	
						
						</td>
					</tr>
				</table>
				<br/>
				<table >
					<tr class="tabdatfonazu">
						<th class="tittabdat"> Solicitud de Descarga de Estados de Cuenta. </th>
					</tr>
					<tr align="center" class="tabmovtex">
				   		<td class="textabdatcla" style="background-color:white;">
							Periodo:<select id="slcPeriodos" name="slcPeriodos">
							<c:forEach items="${periodosRecientes}" var="periodo">
								<c:if test="${not empty periodo}">
									<c:set var="arrPeriodo" value="${fn:split(periodo, ':::')}" />
									<c:if test="${fn:length(arrPeriodo) eq 2}">
										<c:if test="${not fn:contains(arrPeriodo[1], '00000000@000')}">
											<option value="${arrPeriodo[1]}">${arrPeriodo[0]}</option>
										</c:if>
									</c:if>
								</c:if>
							</c:forEach>						
							</select>

							<a href="javascript:ValidaPeriodo();" title="El tiempo que tomar&aacute; la descarga del archivo depender&aacute; de su velocidad de conexi&oacute;n.">
								<img src="/gifs/EnlaceMig/Descarga.gif" style="border: none;" />
							</a>
						</td>
				  	</tr>
				  	<tr><td>&nbsp;</td></tr>
				  	<tr><td align="center">
							<a href="javascript:Regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar" style="border: none;" /></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<div id="descarga" style="display:none;">
	<input type="hidden" name="URLEdoCtaPDF" value="<c:out value="${requestScope.URLEdoCtaPDF}"/>" />
	<c:if test="${not empty requestScope.URLEdoCtaPDF}">
		
		<script type="text/javascript">
						function modificaBanderaCarga() {
							var banderaCarga = document.getElementById("idBanderaCarga");
							if (banderaCarga.value == "0") {
								msg=window.open("<c:out value="${requestScope.URLEdoCtaPDF}"/>","","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=400,height=200");
								msg.focus();
							}
							document.getElementById("idBanderaCarga").value = "1";
						}
						</script>
						
		
	</c:if>

</div>
	</table>
</form>

</body>
</html>