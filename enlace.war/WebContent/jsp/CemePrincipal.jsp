<html>
<!-- #BeginTemplate "/Templates/principal.dwt" -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Enlace</title>
<!-- #EndEditable --> <!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s26370">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="01/08/2001 16:30">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<!-- #EndEditable -->
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<script language="JavaScript">
<!--

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/

function leerMensaje(f, numMensaje)
 {
   f.ventana.value       = "1";
   f.numeroMensaje.value = numMensaje;
   f.submit();
 }

function escribirMensaje(f)
 {
   f.ventana.value = "2";
   f.submit();
 }

<%= request.getAttribute("newMenu") %>

//-->
</script>

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
	   <%= request.getAttribute("MenuPrincipal") %>
    </TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <form name="frmCentroMensajes" method="post" action="CentroMensajes">
    <tr>
      <td valign="top" width="559" align="center">
        <p>&nbsp;</p>
        <table width="530" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td colspan="3" class="tabmovtexbol">Lista de mensajes</td>
          </tr>
          <tr>
            <td width="90" class="tittabcenazu" align="center">N&uacute;mero</td>
            <td class="tittabcenazu" align="center" width="324">Mensaje</td>
            <!-- <td class="tittabcenazu" align="center" width="90">Liga</td> -->
          </tr>
		  <%= request.getAttribute("Tabla") %>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td valign="top" width="127">
			  <a href="javascript:escribirMensaje(document.frmCentroMensajes);" border=0>
			  <img src="/gifs/EnlaceMig/gbo25670.gif" border=0 alt="Escribir mensaje" width="127" height="22"></a>
            </td>
            <!-- <td valign="top" width="125">
              <input type="image" border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25660.gif" width="125" height="22" alt="Borrar mensaje">
            </td> -->
            <td valign="top" width="83">
			  <A href="javascript:history.back();" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="83" height="22"></a>
            </td>
          </tr>
        </table>
        <p>&nbsp;</p>
      </td>
      <td bgcolor="#CCCCCC" width="1"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
      <td width="200" align="center" valign="top">
        <p>&nbsp;</p>
        <p><img src="/gifs/EnlaceMig/gil25030.gif" width="168" height="168"></p>
      </td>
    </tr>
	<input type=hidden name=ventana value="0">
	<input type=hidden name=numeroMensaje value="0">
	<input type=hidden name=tramaMensajes value="<%= request.getAttribute("tramaMensajes") %>">
  </form>
</table>
<!-- #EndEditable -->
</body>
<!-- #EndTemplate -->
</html>