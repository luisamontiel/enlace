<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.List, javax.servlet.http.HttpServletRequest" %>
<%@ page import="java.util.ArrayList, mx.altec.enlace.beans.ConfEdosCtaArchivoBean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>

<%-- 
  - Autor: FSW INDRA
  - Aplicaci�n: ENLACE
  - M�dulo: Paperless MAsivo
  - Fecha: 26/02/2015
  - Descripci�n: Configuracion paperless masivo
  --%>

<html>
<head>
	<title>Configura Estatus de Envio de Estado de Cuenta</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/configMasTCParam.js"></script>
<link href="/EnlaceMig/consultas.css" rel="stylesheet" type="text/css" id = "estilos"/>
<link rel="stylesheet" href="/EnlaceMig/estilosAdmCtrl.css" type="text/css" />
<script type="text/javascript">
	
	
	/********************************************************************************/
	/********************************************************************************/
		<c:if test="${not empty requestScope.newMenu}">
			${requestScope.newMenu}
		</c:if>
</script>
</head>
<body style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="">

<div id="window" class="windowUnload">
<table class="encabezado">
	<tr>
		<td>T&eacute;rminos y condiciones para el servicio paperless</td>
	</tr>
</table>
<br></br>

<table class="tabla" border="0">
	<tr>
		<td width="20px">&nbsp;</td>
		<td><span class="titulo">Convenio de aceptaci&oacute;n de condiciones para el servicio Paperless</span></td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td>
			<textarea class="textbox" readonly="readonly">
CONVENIO DE ACEPTACI&Oacute;N DE CONDICIONES PARA EL SERVICIO DE BLOQUEO DE IMPRESI&Oacute;N DE ESTADO DE CUENTA

Estimado Cliente:
                                                       
En cumplimiento a lo establecido por el C&oacute;digo Fiscal de la Federaci&oacute;n los estados de cuenta podr&aacute;n ser enviados por &eacute;ste medio, por lo que al solicitar su inscripci&oacute;n a &eacute;ste Servicio, usted est&aacute; de acuerdo en no recibir el(los) estado(s) de cuenta impreso(s) en papel a trav&eacute;s de correo de los diferentes productos asociados a su c&oacute;digo de cliente.

Una vez inscrito en el servicio, usted recibir&aacute; la informaci&oacute;n relativa a su estado de cuenta en l&iacute;nea, y &uacute;nicamente tendr&aacute; acceso al mismo a trav&eacute;s de medios electr&oacute;nicos, siguiendo las instrucciones proporcionadas por Banco Santander (M&eacute;xico) S.A., Instituci&oacute;n de Banca M&uacute;ltiple, Grupo Financiero Santander M&eacute;xico.

Usted podr&aacute; acceder a su estado de cuenta electr&oacute;nicamente, siempre que no revoque su consentimiento para recibirlo en l&iacute;nea. Si usted desea volver a recibir su Estado de Cuenta en papel a trav&eacute;s de correo, es necesario que regrese a esta p&aacute;gina y siga las instrucciones que se le indican y su impresi&oacute;n ser&aacute; recibida en el domicilio indicado al mes siguiente que usted haya desactivado la opci&oacute;n de bloqueo de impresi&oacute;n.
Si usted lo desea, podr&aacute; solicitar en cualquiera de nuestras sucursales una copia impresa en papel, de cualquier estado de cuenta con una antig&uuml;edad m&aacute;xima no mayor de 24 meses.

En caso que usted requiera cambiar o actualizar la direcci&oacute;n de correo electr&oacute;nico a d&oacute;nde se le enviar&aacute; las notificaciones, realice el cambio a trav&eacute;s de Enlace en la secci&oacute;n: Administraci&oacute;n > Mant. de Datos Personales. En caso que la direcci&oacute;n de correo electr&oacute;nico que usted proporcione sea incorrecta, inv&aacute;lida o si la misma no puede ser registrada y validada por nuestro sistema, usted no recibir&aacute; la notificaci&oacute;n.

Para cualquier duda o aclaraci&oacute;n, puede comunicarse a Superl&iacute;nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000 a trav&eacute;s de nuestro correo electr&oacute;nico enlace@santander.com.mx  donde uno de nuestros especialistas le brindara el apoyo.			</textarea>
</td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td><input type="checkbox" name="ac" id="ac" value="ac" />Acepto las condiciones.</td>
	</tr>
		<tr>
		<td width="20px">&nbsp;</td>
		<td align="center">
			<input type="button" class="boton" value="Continuar" onClick="javascript:acepto();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" class="boton" value="Cerrar" onClick="javascript:cierro();"/>
		</td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td>Por favor lea el convenio de aceptaci&oacute;n de condiciones para el servicio de Paperless. Se requiere que Usted
		    acepte las condiciones para que deje de recibir (o pueda seguir recibiendo) su(s) estado(s) de cuenta de manera 
			impresa en su domicilio. En caso de no estar de acuerdo, podr&aacute; aceptar el cambio posteriormente.
			</td>
	</tr>
</table>

</div>
<div id="sombra" class="sobmbraUnload"></div>

<table border="0px" cellpadding="0px" cellspacing="0px" width="571px">
  <tr valign="top">
   <td width="*">
		<c:if test="${not empty requestScope.MenuPrincipal}">
			${requestScope.MenuPrincipal}
		</c:if>
   </td>
  </tr>
</table>
<c:if test="${not empty requestScope.Encabezado}">
	${requestScope.Encabezado}
</c:if>
<form  name="formulario" id="formulario" method="post" action="ConfigMasPorTCServlet">
	<input type="hidden" id="accion" name="accion" value="<%= request.getAttribute("mostrar") %>" />
	<input type="hidden" id="elementos" name="elementos" value="<%=request.getAttribute("elementos") %>" />
	<input type="hidden" id="cadena" name="cadena" value="" />
  	<table width="760" cellspacing="0" cellpadding="0" border="0">
  		<tbody>
  			<tr>
  				<td align="center">
					<table border="0" style="text-align:center; width:600px;">
					<tr><td align="center"><span id="paginador"></span></td></tr>
					</table>
					<table border="0" id="tblDatos" class="tabfonbla" style="text-align:center; width:600px;">
						<thead class="tittabdat">
							<tr><td style="width:110px;">Tarjeta</td>
								<td style="width:290px;">Titular</td>
								<td style="width:100px;">Con Paperless</td>
								<td style="width:100px;">Sin Paperless</td></tr>
						</thead>
						<tbody>
						   		<c:if test="${not empty listaCuentasEdoCta}">
									<c:set var="cont" value="${0}" />
									<c:forEach items="${listaCuentasEdoCta}" var="cuenta">
										<c:set var="cont" value="${cont + 1}" />
										<c:if test="${cont mod 2 eq 0}">
											<c:set var="estilo" value="textabdatcla" />
										</c:if>
										<c:if test="${cont mod 2 ne 0}">
											<c:set var="estilo" value="textabdatobs" />
										</c:if>
										<tr>
											<td colspan="1" class="${estilo}" align="center"> 
												<label id="cuenta${cont}">
													${cuenta.nomCuenta}
												</label>
											</td>
											<td colspan="1" class="${estilo}" align="center" >
												<label id="titular${cont}">
													${cuenta.nombreTitular}
												</label>
											</td>
											<td colspan="1" class="${estilo}" align="center">
												<input type="radio" id="paperless${cont}" name="paperless${cont}" value="s"/>
											</td>
											<td colspan="1" class="${estilo}" align="center">
												<input type="radio" id="paperless${cont}" name="paperless${cont}" value="n"/>
											</td>
										</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>	
					<script type="text/javascript">
						var p = new Paginador(
							document.getElementById('paginador'),
							document.getElementById('tblDatos'),
							30
						);
						p.Mostrar();
					</script>										
  				</td>
  			</tr>
  			<tr>
  				<td>
				  <table border="0px" cellpadding="0px" cellspacing="0px" align="center">
				   <tr>
				     <td><a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/gbo25280.gif" border="0"/></a></td>
					 <td><a href="javascript:Cancelar();"><img src="/gifs/EnlaceMig/gbo25190.gif" border="0"/></a></td>
				   </tr>
				  </table>
				</td>  					
  			</tr>
  		</tbody>
	</table>
	<br></br>
	<br></br>
</form>
</body>
</html>