<jsp:include page="/jsp/prepago/headerPrepago.jsp"/>
<%@page import="mx.altec.enlace.beans.TarjetaContrato,java.util.*"%>
<script language="javascript">
var dia;
var mes;
var anio;
var fechaseleccionada;
var opcioncalendario;
 function WindowCalendarTJ()
    {
        var m=new Date();
        m.setFullYear(document.Frmfechas.strAnio.value);
        m.setMonth(document.Frmfechas.strMes.value);
        m.setDate(document.Frmfechas.strDia.value);
        n=m.getMonth();
        dia=document.Frmfechas.strDia.value;
        mes=document.Frmfechas.strMes.value;
        anio=document.Frmfechas.strAnio.value;
        opcioncalendario=1;
        msg=window.open("/EnlaceMig/calpas22.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
        msg.focus();
    }
    function WindowCalendar1TJ()
    {
        var m=new Date();
        m.setFullYear(document.Frmfechas.strAnio.value);
        m.setMonth(document.Frmfechas.strMes.value);
        m.setDate(document.Frmfechas.strDia.value);
        n=m.getMonth();
        dia=document.Frmfechas.strDia.value;
        mes=document.Frmfechas.strMes.value;
        anio=document.Frmfechas.strAnio.value;
        opcioncalendario=2;
        msg=window.open("/EnlaceMig/calpas22.html#" +n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
        msg.focus();
    }
    function Actualiza()
    {
        if (opcioncalendario==1)
        document.ConsVinTar.fecha1.value=fechaseleccionada;
        else if (opcioncalendario==2)
        document.ConsVinTar.fecha2.value=fechaseleccionada;
    }
function js_desplazamiento(index){

    document.ConsVinTar.start.value = index;
    document.ConsVinTar.action = "EnlcConsultaTarjeta?operacion=3";
    document.ConsVinTar.submit();
}
function consultaTarjeta()
{
	if(document.ConsVinTar.tarjeta.value != "")
	{

		//cuadroDialogo( "Consulta a tarjeta " + document.ConsVinTar.tarjeta.value,1);

	} else {
		//realizar toda la validacion para la fecha
	   if (document.ConsVinTar.fecha1.value == "")
	   {
		cuadroDialogo( "Falta seleccionar el rango de fechas para la busqueda." ,1);
		return false;
	   }
	   if(document.ConsVinTar.fecha2.value == "")
	   {
		cuadroDialogo( "Falta seleccionar el rango de fechas para la busqueda." ,1);
		return false;
	   }
	   if(document.ConsVinTar.fecha1.value!='' && !validaFE(document.ConsVinTar.fecha1)){
			cuadroDialogo("Los caracteres ingresados en el campo fecha son inv&aacute;lidos.", 1);
			return false;
		}else
			if(!validaFecha1())
			{
				return false;
			}
	   if(document.ConsVinTar.fecha2.value!='' && !validaFE(document.ConsVinTar.fecha2)){
			cuadroDialogo("Los caracteres ingresados en el campo fecha son inv&aacute;lidos.", 1);
			return false;
		}else
			if(!validaFecha2())
			{
				return false;
			}
	   if(!VerificaFechas(document.ConsVinTar.fecha1, document.ConsVinTar.fecha2)){
		cuadroDialogo( "La fecha inicio es mayor a la fecha fin." ,1);
		return false;
	   }
	   if(!VerificaRango()){
			cuadroDialogo( "Excede el rango de consulta" ,1);
			return false;
		   }

	}

	document.ConsVinTar.action = "EnlcConsultaTarjeta?operacion=2";
	document.ConsVinTar.submit();
}

function validaFecha1()
{
	var ruta = document.ConsVinTar.fecha1;
	if(parseInt(ruta.value.substring(0,1))>=0 && parseInt(ruta.value.substring(0,1))<=3)
  	{
  		if((parseInt(ruta.value.substring(1,2))>=0 && parseInt(ruta.value.substring(1,2))<=9 && parseInt(ruta.value.substring(0,1))<3) || (parseInt(ruta.value.substring(1,2))>=0 && parseInt(ruta.value.substring(1,2))<=1 && parseInt(ruta.value.substring(0,1))==3))
  		{
  			if(parseInt(ruta.value.substring(0,1))!=0 || parseInt(ruta.value.substring(1,2))!=0)
  			{
  				if((parseInt(ruta.value.substring(4,5))>0 && parseInt(ruta.value.substring(4,5))<=9 && parseInt(ruta.value.substring(3,4))==0) || (parseInt(ruta.value.substring(4,5))>=0 && parseInt(ruta.value.substring(4,5))<=2 && parseInt(ruta.value.substring(3,4))==1))
  				{
  					var ano=new Date().getFullYear();
  					if(ruta.value.substring(6,10).length==4 && parseInt(ruta.value.substring(6,10))<= ano && parseInt(ruta.value.substring(6,10))>1800)
  					{
  						if(ruta.value.substring(2,3)=='/' && ruta.value.substring(5,6)=='/')
  						{
  							return true;
  						}
  						else
  						{
  							cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar los separadores" ,1);
  							return false;
  						}
  					}
  					else
  					{
  						cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el a�o" ,1);
  						return false;
  					}
  				}
  				else
  				{
  					cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el mes" ,1);
  					return false;
  				}
  			}
  			else
  			{
  				cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
  				return false;
  			}
  		}
  		else
  		{
  			cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
  			return false;
  		}
  	}
  	else
  	{
		cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
		return false;
	}
}

function validaFecha2()
{
	var ruta = document.ConsVinTar.fecha2;
	if(parseInt(ruta.value.substring(0,1))>=0 && parseInt(ruta.value.substring(0,1))<=3)
  	{
  		if((parseInt(ruta.value.substring(1,2))>=0 && parseInt(ruta.value.substring(1,2))<=9 && parseInt(ruta.value.substring(0,1))<3) || (parseInt(ruta.value.substring(1,2))>=0 && parseInt(ruta.value.substring(1,2))<=1 && parseInt(ruta.value.substring(0,1))==3))
  		{
  			if(parseInt(ruta.value.substring(0,1))!=0 || parseInt(ruta.value.substring(1,2))!=0)
  			{
  				if((parseInt(ruta.value.substring(4,5))>0 && parseInt(ruta.value.substring(4,5))<=9 && parseInt(ruta.value.substring(3,4))==0) || (parseInt(ruta.value.substring(4,5))>=0 && parseInt(ruta.value.substring(4,5))<=2 && parseInt(ruta.value.substring(3,4))==1))
  				{
  					var ano=new Date().getFullYear();
  					if(ruta.value.substring(6,10).length==4 && parseInt(ruta.value.substring(6,10))<= ano && parseInt(ruta.value.substring(6,10))>1800)
  					{
  						if(ruta.value.substring(2,3)=='/' && ruta.value.substring(5,6)=='/')
  						{
  							return true;
  						}
  						else
  						{
  							cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar los separadores" ,1);
  							return false;
  						}
  					}
  					else
  					{
  						cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el a�o" ,1);
  						return false;
  					}
  				}
  				else
  				{
  					cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el mes" ,1);
  					return false;
  				}
  			}
  			else
  			{
  				cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
  				return false;
  			}
  		}
  		else
  		{
  			cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
  			return false;
  		}
  	}
  	else
  	{
		cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
		return false;
	}
}

function validaFE(campo){
	return campo.value.match(/^\d{1,2}\/\d{1,2}\/\d{2,4}$/);
}

function bajaTarjetas(){
	//Validacion checkbox
	var h=document.ConsVinTar.elements.length;
	var forma = document.ConsVinTar;
	var j=0;
    for (i=0; i< h; i++)
    {
    if (forma.elements[i].type=="checkbox")
      {
         if (forma.elements[i].checked==false)
        {
          var m=0;
        }
        else{
           var e = forma.elements[i];
           if(e.name != "allbox" && e.checked==true){
            j++;
           }
        }
      }
      else
          var m=0;
    }
   if(j==0)
   {
     cuadroDialogo( "Debe seleccionar al menos un registro." ,1);
   } else {
	   	document.ConsVinTar.action = "EnlcBajaTarjeta?operacion=2";
		document.ConsVinTar.submit();
	}
}

function exportarTarjetas(){
	document.ConsVinTar.action = "EnlcConsultaTarjeta?operacion=4";
	document.ConsVinTar.submit();
}
function limpiar()
{
	document.ConsVinTar.action = "EnlcConsultaTarjeta?operacion=1";
	document.ConsVinTar.submit();
}
function soloNumeros(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 }
 function soloNumerosBlur(evt) {
  	if (isNaN(document.AltaVinTar.tarjeta.value)) {
		document.AltaVinTar.tarjeta.value = "";
		document.AltaVinTar.tarjeta.focus();
		return false;
 	}
 }
 function soloNumerosBlurLote(evt) {
  	if (isNaN(document.AltaVinTar.loteCarga.value)) {
		document.AltaVinTar.loteCarga.value = "";
		document.AltaVinTar.loteCarga.focus();
		return false;
 	}
 }
function checkAll() {
	 for (var i=0; i < document.ConsVinTar.elements.length;i++) {
		var e = document.ConsVinTar.elements[i];
		if (e.name != 'allbox')
			e.checked = document.ConsVinTar.allbox.checked;
 	}
}
function VerificaFechas(fecha1, fecha2)
 {

 lib_year         = parseInt(fecha1.value.substring(6,10),10);
 lib_month        = parseInt(fecha1.value.substring(3, 5),10);
 lib_date         = parseInt(fecha1.value.substring(0, 2),10);
 DeFecha = new Date(lib_year, lib_month, lib_date);

 lim_year     = parseInt(fecha2.value.substring(6,10),10);
 lim_month    = parseInt(fecha2.value.substring(3, 5),10);
 lim_date     = parseInt(fecha2.value.substring(0, 2),10);
 AFecha  = new Date(lim_year, lim_month, lim_date);

     if( DeFecha > AFecha)
      {
        return false;
      }
  return true;
 }

function VerificaRango()
{
	var fechaIni = document.ConsVinTar.fecha1;
	var fechaFin = document.ConsVinTar.fecha2;


	difAno= parseInt(fechaFin.value.substring(6,10),10) - parseInt(fechaIni.value.substring(6,10),10);
	difMes1= parseInt(fechaFin.value.substring(3,5),10) - parseInt(fechaIni.value.substring(3,5),10);
	difMes2= parseInt(fechaIni.value.substring(3,5),10) - parseInt(fechaFin.value.substring(3,5),10);
	diaIni= parseInt(fechaIni.value.substring(0, 2),10);
	diaFin= parseInt(fechaFin.value.substring(0, 2),10);


    if(difAno == 0){

    	if(difMes1 > 4){

    		return false;
    	}
        if(difMes1 == 4){

        	if(diaFin > diaIni){

        		return false;

        	}
    	}



    }else{

    	if(difAno == 1){

    		if(difMes2 < 8){

        		return false;
        	}
    		if(difMes2 == 8){

            	if(diaFin > diaIni){

            		return false;

            	}
        	}

    	}else{

    		return false;
    	}
    }

	return true;

}



</script>

<style>
	div.fileinputs {
	position: relative;
	}

	div.fakefile {
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 1;
	}
</style>
<form name = "Frmfechas">

  <Input type = "hidden" name ="strAnio" value = "<%=request.getAttribute("anio") %>">
  <Input type = "hidden" name ="strMes" value = "<%=request.getAttribute("mes") %>">
  <Input type = "hidden" name ="strDia" value = "<%=request.getAttribute("dia") %>">
</form>
<FORM  NAME="ConsVinTar" METHOD="Post" ACTION="ConsVinTar">
<INPUT TYPE="hidden" VALUE="1" NAME="statusDuplicado">
<INPUT TYPE="hidden" VALUE="1" NAME="statushrc">
<input type="hidden" name="start" id="start">

			<table width="760" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center">
				<table width="600" border="0" cellspacing="2" cellpadding="2" bgcolor="#EBEBEB">
					<tr>
						<td width="600" class="tittabdat" colspan="3">
							Proporcione los siguientes datos:
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							N&uacute;mero de tarjeta:
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="" maxlength="<%=TarjetaContrato.lngNoTarjeta%>" size="25" name="tarjeta" onkeypress="return soloNumeros(event)" onkeyup="return soloNumerosBlur(event)">
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							Fecha inicio de consulta (dd/mm/aaaa):
						</td>
						<td class=textabdatcla colspan="2">
						 	<input type="text" value="" maxlength="<%=TarjetaContrato.lngfecha%>" name="fecha1" size="12">
                        </td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							Fecha fin de consulta (dd/mm/aaaa):
						</td>
						<td class=textabdatcla colspan="2">
						  	<input type="text" value="" maxlength="<%=TarjetaContrato.lngfecha%>" name="fecha2" size="12">
                       </td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							Estatus:
						</td>
						<td class=textabdatcla>
							<select name="estatus">
								<option value="T">Todos</option>
								<option value="P">Pendiente de Activar</option>
								<option value="A">Activa</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							Lote de carga:
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="" size="25" name="loteCarga" onkeypress="return soloNumeros(event)" onkeyup="return soloNumerosBlurLote(event)">
						</td>
					</tr>
				</table>
				<br>
				<table width="90" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
					<tr>
						<td align="center">
							<input type="image" border="0" name="Aceptar"
								src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22"
								alt="Consultar" onclick="javascript:consultaTarjeta();return false">
						</td>
						<td align="center">
							<input type="image" border="0" name="Agregar"
									src="/gifs/EnlaceMig/gbo25250.gif" width="90" height="22"
									alt="Limpiar" onclick="javascript:limpiar();return false">
						</td>
					</tr>
				</table>
								<%
			if(session.getAttribute("tarjetasRes") != null){
				List tarjetas = (List) session.getAttribute("tarjetasRes");
				Integer start = (Integer) request.getAttribute("start");
				Integer despl = (Integer) session.getAttribute("despl");
			%>
			<table align="center"><tr><td align="left" class="tabmovtex">Total de registros encontrados:<%= tarjetas.size() %></td></tr><%--// Modificacion RMM 20030207 --%>
						<tr><td><%--// Modificacion RMM 20030207 --%>
						<table align="center" >
						    <tr class="tabdatfonazu">
						        <th class="tittabdat"><input type='checkbox' name='allbox' value="checkbox" onclick='checkAll();'> Todos</th>
						        <TH class="tittabdat"> N&uacute;mero de Tarjeta</th>
						        <TH class="tittabdat"> Contrato</th>
						        <th class="tittabdat"> Estatus</th>
						        <th class="tittabdat"> Fecha de Vinculaci&oacute;n</th>
						        <th class="tittabdat"> Lote de Carga</th>
						        <th class="tittabdat"> Clave del empleado </th>
						    </tr>
						    <%
						        ListIterator it = tarjetas.listIterator(start.intValue());
						        int reg = 0;
						        TarjetaContrato tarjeta = null;
						        boolean claro = false;
						        while( it.hasNext() && despl.intValue() > reg){
						            reg++;
						            tarjeta = (TarjetaContrato)it.next();
						            if(null == tarjeta){
						                continue;
						       	}
						    %>
						        <tr class='<%= ((claro)?"textabdatcla":"textabdatobs") %>'>
						            <TD class="tabmovtex" align="center">
										 <INPUT type="checkbox" name='tarjetas' value="<%= tarjeta.getTarjeta()%>@<%= tarjeta.getContrato()%>">
						                &nbsp;
						            </td>
						            <td class="tabmovtex" align="center"><%= tarjeta.getTarjeta() %> &nbsp;</td>
						            <td class="tabmovtex" align="center"><%= tarjeta.getContrato() %> &nbsp;</td>
						            <td class="tabmovtex" align="center"><%= tarjeta.getEstatus() %>&nbsp;</td>
						            <td class="tabmovtex" align="center"><%= tarjeta.getFechaUltAcceso()%>&nbsp;</td>
						            <td class="tabmovtex" align="center"><%= tarjeta.getIdLote()	%> &nbsp;</td>
						            <td class="tabmovtex" align="center"><%= tarjeta.getUsuarioReg()%>&nbsp;</td>
						        </tr>
						<%
						        claro = !claro;
						    }
						%>
						</TABLE>
						<table  align="center">
						<tr><td align="center" class='texfootpaggri'>
						        <%
						        if(start.intValue() - despl.intValue() >= 0){%><a href='javascript:js_desplazamiento(<%= start.intValue() - despl.intValue() %>)' class='texfootpaggri'>anterior</a>&nbsp;<%}
						        if(tarjetas.size() > 1){
						            for(int i = 0;i < tarjetas.size();i+= despl.intValue()){
						                if(start.intValue() != i){
						                    %><a href='javascript:js_desplazamiento(<%= i %>)' class='texfootpaggri'><%=(i /despl.intValue()) +1 %></a><%
						                    }else{
						                    %><%= ( i / despl.intValue() ) + 1%><%
						                    }%>&nbsp;<%
						            }
						        }
						        if(start.intValue() + despl.intValue() < tarjetas.size()){%>&nbsp;<a href='javascript:js_desplazamiento(<%= start.intValue() + despl.intValue() %>)' class='texfootpaggri'>siguiente</a><%}%>
		        		</td></tr>
					</table>
				<table align="center">
				    <tr><td>
				        <a href='javascript: bajaTarjetas()' border='0'><img src='/gifs/EnlaceMig/gbo25490.gif' border=0 alt='Baja' height='22' width='85'></a>
				       	<a href='javascript: exportarTarjetas()' ><img src='/gifs/EnlaceMig/gbo25230.gif' border=0 alt='Exportar' height='22' width='85'></a>
				        <a href='EnlcConsultaTarjeta?operacion=1'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
				    </td><tr>
				</table>
				</td></tr></table>
			<%}%>
			    </td>
			</tr>
		</table>
	<script>
			<% if (session != null && session.getAttribute("MensajeLogin01") != null ) { %>
			<%= session.getAttribute("MensajeLogin01") %>
			<%	} %>
	</script>

</form>
<jsp:include page="/jsp/prepago/footerPrepago.jsp"/>

