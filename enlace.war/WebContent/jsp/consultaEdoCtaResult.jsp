<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@page import="mx.altec.enlace.beans.ConsEdoCtaResultadoBean"%>
<jsp:useBean id="edoCtaList" class="java.util.ArrayList" scope="request" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>

  <script type="text/javascript"  src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript"  src="/EnlaceMig/fw_menu.js"></script>
  <script type="text/javascript">

    var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
    var dia;
    var mes;
    var anio;
    var fechaseleccionada;
    var opcioncaledario;
    function WindowCalendar()
    {
        if(!document.FrmConEdoCta.deldia[0].checked){
        var m=new Date();
        m.setFullYear(document.Frmfechas.strAnio.value);
        m.setMonth(document.Frmfechas.strMes.value);
        m.setDate(document.Frmfechas.strDia.value);
        n=m.getMonth();
        dia=document.Frmfechas.strDia.value;
        mes=document.Frmfechas.strMes.value;
        anio=document.Frmfechas.strAnio.value;
        opcioncalendario=1;
        msg=window.open("/EnlaceMig/calpas1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
        msg.focus();
    	}else{
        	cuadroDialogo("Aviso\n La fecha no se puede cambiar para consultar los Movimientos del d&#237;a",1);
    	}
    }

    function js_verDetalle(){
    	if(validaSeleccion()){

	    	document.FrmConEdoCta.action="ConsultaEdoCtaServlet";
			document.FrmConEdoCta.opcion.value="2";
			document.FrmConEdoCta.submit();
		}
    }

    function js_regresarEdoCta()
	{
		document.FrmConEdoCta.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCta.opcion.value="0";
		document.FrmConEdoCta.submit();
	}

	function anteriores()
    {
    	var pag = document.FrmConEdoCta.numPag.value;
    	pag--;
		document.FrmConEdoCta.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCta.opcion.value="1";
		document.FrmConEdoCta.numPag.value = pag;
		document.FrmConEdoCta.submit();
	}

	function siguientes()
    {
    	var pag = document.FrmConEdoCta.numPag.value;
    	pag++;
		document.FrmConEdoCta.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCta.opcion.value="1";
		document.FrmConEdoCta.numPag.value = pag;
		document.FrmConEdoCta.submit();
	}

    function validaSeleccion(){

		var radios = document.getElementsByTagName('input');
		var value;

		for (var i = 0; i < radios.length; i++) {
	    	if (radios[i].type == 'radio' && radios[i].checked) {
	       		return true;
	    	}
		}
		cuadroDialogo("Favor de Seleccionar un registro para visualizar el detalle del registro",1);
		return false;
	}

    function Actualiza()
    {
        if (opcioncalendario==1)
        document.FrmConEdoCta.fecha1.value=fechaseleccionada;
        else if (opcioncalendario==2)
        document.FrmConEdoCta.fecha2.value=fechaseleccionada;
    }
    var ctaselec;
    var ctadescr;
    var ctatipre;
    var ctatipro;
    var ctaserfi;
    var ctaprod;
    var ctasubprod;
    var tramadicional;
    var cfm;
    function PresentarCuentas()
    {
        msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
        msg.focus();
    }
    function actualizacuenta()
    {
        document.FrmConEdoCta.EnlaceCuenta.value=ctaselec+"@"+ctatipre+"@"+ctadescr+"@";
        document.FrmConEdoCta.textEnlaceCuenta.value=ctaselec+" "+ctadescr;
    }
    function EnfSelCta()
    {
        if( document.FrmConEdoCta.EnlaceCuenta.value.length<=0 )
            document.FrmConEdoCta.textEnlaceCuenta.value="";
    }
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    function ctaPorParametro()
    {
        var miTram = "<% if( request.getAttribute("EnlaceCuenta")!=null) out.print(request.getAttribute("EnlaceCuenta")); else out.print(""); %>";
        if( miTram!="" )
        {
            var numCta = "";
            var ctaDes = "";
            var tipRel = "";
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            numCta = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            ctaDes = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            tipRel = miTram.substring( 0,miTram.indexOf("|") );
            document.FrmConEdoCta.EnlaceCuenta.value     = numCta +"@"+ tipRel +"@"+ ctaDes +"@";
            document.FrmConEdoCta.textEnlaceCuenta.value = numCta +" "+ ctaDes;
        }//fin if
    }
<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>

  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
  <table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="">
    <td width="">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <form name = "Frmfechas">
<%
   // if( request.getAttribute("Movfechas")!=null )
   //     out.println(request.getAttribute("Movfechas"));
%>
   </form>
  <form name="FrmConEdoCta" method="post">
  <input type="hidden" name="opcion" value=""/>
  <input type="hidden" name="numPag" value="<%=request.getAttribute("numPag")%>"/>
  <input type="hidden" name="totalPaginas" value="<%=request.getAttribute("totalPaginas")%>"/>
  <input type="hidden" name="folio" value="<%=request.getAttribute("folioC")%>"/>
  <input type="hidden" name="usuarioSol" value="<%=request.getAttribute("usuarioSolC")%>"/>
  <input type="hidden" name="fechaIni" value="<%=request.getAttribute("fechaIniC")%>"/>
  <input type="hidden" name="fechaFin" value="<%=request.getAttribute("fechaFinC")%>"/>
  <input type="hidden" name="estatus" value="<%=request.getAttribute("estatusC")%>"/>
  <input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
   <tr>
    <td align="center">
     <table width="500" border="0" cellspacing="2" cellpadding="3">
      <tr>
       <td class="tittabdat"  align="center">&nbsp;</td>
       <td class="tittabdat"  align="center" width="80">Fecha de Env&iacute;o</td>
       <td class="tittabdat"  align="center">Estatus</td>
       <td class="tittabdat"  align="center">Folio</td>
       <td class="tittabdat"  align="center">Usuario Solicitante</td>
      </tr>

      <%

      	if(edoCtaList != null && edoCtaList.size()>0){

      	String estilo ="";
      	Integer pagIni = (Integer)request.getAttribute("numPag");
      	Integer pagFin = (Integer)request.getAttribute("totalPaginas");

      		for(int i=0;i<edoCtaList.size();i++){
      			ConsEdoCtaResultadoBean result = (ConsEdoCtaResultadoBean)edoCtaList.get(i);

      			if(i%2==0){
      				estilo = "textabdatobs";
       %>
			      <tr  bgcolor="#CCCCCC">
      <%		}else{
      				estilo = "textabdatcla";
      %>
			      <tr bgcolor="#EBEBEB">
      <%		} %>
      	<td class="<%=estilo %>">
      		<input type="radio" name="idEdoCta" id="idEdoCta" value="<%=result.getIdEdoCta()%>" <%=edoCtaList.size()==1?"checked":"" %>/>
      	</td>
      	<td class="<%=estilo %>" align="center"><%=result.getFechaEnvio()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getEstatus()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getIdEdoCta()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getUsuarioSolicitante()%></td>
      </tr>

      <%
      		}

       %>

     </table>
    </td>
   </tr>
   <tr>
    <td align="center">
   		<table align="center" border="0" cellspacing="0" cellpadding="0">
	 		<tr>

   				<%if(pagIni.intValue() >1){%>

	  				<td><a href="javascript:anteriores();">Anteriores</a>&nbsp;</td>
	  			<%}

	  			if(pagFin.intValue()!= pagIni.intValue()){
	  			%>
	  				<td>&nbsp;<a href="javascript:siguientes();">Siguientes</a></td>

	  			<%} %>
	 		</tr>
		</table>
	</td>
   </tr>
   <tr>
   	<td>
   		&nbsp;
   	</td>
   </tr>
   <tr>
    <td align="center">
    	<table align="center" border="0" cellspacing="0" cellpadding="0">
	 		<tr>
	  			<td><a href = "javascript:js_verDetalle();"><img src = "/gifs/EnlaceMig/gbo25248.gif" border="0" alt="Detalle"/></a></td>
	  			<td><a href = "javascript:js_regresarEdoCta();"><img src = "/gifs/EnlaceMig/gbo25320.gif" border="0" alt="Regresar"/></a></td>
	 		</tr>
		</table>
	</td>
   </tr>
  </form>
  </table>
  <%	} %>
 </body>
	<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	</head>
</html>