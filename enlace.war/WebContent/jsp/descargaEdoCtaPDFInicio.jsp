<%@ page language="java" contentType="text/html;charset=ISO8859_1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="mx.altec.enlace.bita.BitaConstants"%>
<%@page import="java.util.ArrayList, java.util.List"%>
<%@page import="mx.altec.enlace.beans.ConfEdosCtaArchivoBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Enlace Banco Santander Mexicano</title>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
<link rel="stylesheet" href="/EnlaceMig/estilosAdmCtrl.css" type="text/css" />

<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/ConMov.js"></script>

<script type="text/javascript">
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

	function MM_preloadImages(){   //v3.0
		var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
	function MM_swapImgRestore(){   //v3.0
		var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	function MM_findObj(n, d){ //v3.0
		var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}
	function MM_swapImage(){   //v3.0
		var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	function esCuentaValida(cadena){
		if (/^([B][M][E])?[0-9]{0,16}$/.test(cadena.toUpperCase())) {
			return true;
		}
		else {
			return false;
		}
	}
	function EsAlfa(cadena, band){
		for (var i=0;i<cadena.length;i++){
			if(!(((cadena.charAt(i)==' ') && (i != 0))||(cadena.charAt(i)>='a' && cadena.charAt(i)<='z')||(cadena.charAt(i)>='A' && cadena.charAt(i)<='Z')||(cadena.charAt(i)>='0' && cadena.charAt(i)<='9'))){
				cuadroDialogo("No se permiten caracteres especiales en "+band+ ", como: acentos, comas, puntos, etc",4);
				return false;
			}
		}
		return true;
	}
	function validar() {
		var descripcion = document.getElementById('filtroDesc').value;
		var cadena=document.getElementById('filtroCuenta').value;
		var result=true;
		result= EsAlfa(descripcion,"Descripci&oacute;n cuenta / Tarjeta");
		if(result==true) {
			document.getElementById('filtroDesc').value = descripcion.toUpperCase();
			if (cadena.length>0) {
				if (esCuentaValida(cadena)) {
					result=true;
				} else {
					cuadroDialogo("N\u00famero de cuenta / Tarjeta inv\u00e1lido",4);
					result=false;
				}
			}
		}
		return result;
	}
	function filtrarCuentas() {
		if ( validar() == true) {
			filtrar();
		}
	}
	function filtrar() {
		document.frmConsultar.action = "DescargaEdoCtaPDFServlet?ventana=0";
		document.frmConsultar.submit();
	}
	function Cancelar() {
		document.frmConsultar.action = "DescargaEdoCtaPDFServlet";
		document.frmConsultar.submit();
	}
	function consulta(){
		var cuentaSeleccionada = document.frmConsultar.cuentasTarjetas.options[document.frmConsultar.cuentasTarjetas.selectedIndex].value;

	if( cuentaSeleccionada === '-1' ){
		cuadroDialogo("Por favor, seleccione una cuenta o tarjeta para descarga de Estado de Cuenta",4);
		}else {
			document.frmConsultar.hdnCuentasTarjetas.value =
				document.frmConsultar.cuentasTarjetas.options[document.frmConsultar.cuentasTarjetas.selectedIndex].value;
			document.frmConsultar.submit();
		}
	}

<%if (request.getAttribute("newMenu") != null)
				out.println(request.getAttribute("newMenu"));%>
</script>

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" background-color: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
	>

<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top">
		<td width="*">
			<c:if test="${not empty requestScope.MenuPrincipal}">
				${requestScope.MenuPrincipal}
			</c:if>
		</td>
	</tr>
</table>
<c:if test="${not empty requestScope.Encabezado}">
	${requestScope.Encabezado}
</c:if>
<form name="frmConsultar" method="post"
	action="DescargaEdoCtaPDFServlet?ventana=1">
	<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
	<input type="hidden" name="tipoOp" value="${tipoOp}" />
	<input type="hidden" name="hdnCuentasTarjetas" value="" />
	<table id="tblConsulta" width="760" border="0" cellspacing="0"
			cellpadding="0"
		style="display:<%="0".equals(request.getAttribute("ventana"))?"block":"none"%>;">
			<tbody>
				<tr>
					<td width="116">
            		</td>
					<td align="center">
						<table width="550" border="0" cellspacing="2" cellpadding="3">
							<tbody>
								<tr>
									<td class="tittabdat" colspan="2">&nbsp;</td>
								</tr>
								<tr align="center">
									<td class="textabdatcla" colspan="2">
										<table width="550" border="0" cellspacing="0" cellpadding="5">
											<tr>
												<td align="right" class="tabmovtexbol" width="200" nowrap>
													Cuenta / Tarjeta:</td>
												<td width="200"><input type="text" name="filtroCuenta" id="filtroCuenta" style="width: 200px;"/>
												</td>
												<td rowspan="2"><a title="Filtrar Cuentas" href="javascript:filtrarCuentas();"> <img
														src="/gifs/EnlaceMig/Ir.png" style=" border: 0;" alt="Filtrar Cuentas" width="84" height="21"/> </a></td>
											</tr>
											<tr>
												<td width="200" align="right" class="tabmovtexbol" nowrap>
													Descripci&oacute;n Cuenta / Tarjeta:</td>
												<td width="200"><input type="text" name="filtroDesc" id="filtroDesc" style="width: 200px;"/></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr align="center">
									<td colspan="2" class="textabdatcla">
								    	<table width="550" cellspacing="0" cellpadding="5" border="0">
								        	<tbody>
								        		<tr>
													<td width="211" align="right" class="tabmovtexbol" width="100" nowrap>
														Seleccionar una Cuenta / Tarjeta:
													</td>
													<td width="400">
														<select style="width: 333px;" class="tabmovtex" id="cuentasTarjetas" name="cuentasTarjetas">
															 <c:if test="${fn:length(listaCuentasEdoCta) gt 1}">
																	<option value="-1">
																		Seleccione una cuenta...
																	</option>
																</c:if>
															 <c:choose>

																<c:when test="${not empty listaCuentasEdoCta}">

																	<c:forEach items="${listaCuentasEdoCta}" var="cuenta">
																		<c:catch var="catchException">
																			<option value="${cuenta.nomCuenta}">
																				${cuenta.nomCuenta} ${cuenta.nombreTitular}
																			</option>
																		</c:catch>
																		<c:if test = "${catchException != null}">
																		</c:if>
																	</c:forEach>
																	<c:if test="${not empty requestScope.msgOption}">
																		<option value="">
																		${requestScope.msgOption}
																		</option>
																	</c:if>
																</c:when>
																<c:when test="${requestScope.ventana == '0'}">
																	<option value="-1">
																			No se encontraron resultados...
																	</option>
																</c:when>
															</c:choose>

														</select>
													</td>
								        		</tr>
								        	</tbody>
								       	</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table width="160" border="0" cellspacing="0" cellpadding="0" style=" background-color: #FFFFFF;" height="25">
						  	<tr>
						    	<td align="center" style=" height: 25px;"><a title="Selecciona Cuenta para descarga" href="javascript:consulta();"><img src="/gifs/EnlaceMig/gbo25310.gif" style=" border: 0;"/></a></td>
						    </tr>
					    </table>
					</td>
					<td>
					</td>
				</tr>
			</tbody>
		</table>
	</form>


<script type="text/javascript">
<%if (request.getAttribute("MensajeErr") != null) {
	out.println(request.getAttribute("MensajeErr"));
}%>
</script>

</body>
</html>