<%@page contentType="text/html"%>
<%@page import="java.util.*"%>
<jsp:useBean id='Matriz' class='java.util.HashMap' scope='request'/>
<html>
<head>
    <title>Enlace Confirming - Gr&aacute;fica</title>
    <LINK rel='stylesheet' href='/EnlaceMig/consultas.css' style='text/css'>
</head>
<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"> </SCRIPT>

<SCRIPT language='javascript'>
<%!
    float Maximo;
    float Parte3;
    float Media;
    float Parte2;
    long Agrega = 0;
    long Posicion;
    java.text.NumberFormat Currency = java.text.NumberFormat.getCurrencyInstance (java.util.Locale.US);
    java.text.NumberFormat Numero = java.text.NumberFormat.getNumberInstance(java.util.Locale.US);
    boolean Formato = false;

    void calculaMaximo (float total) {
        int mfac;
        for (mfac = 10; ((total % mfac) != total) && ((total % mfac) != 0); mfac *= 10);
        Maximo = total + mfac / 10;
        if ((total % mfac) != 0) {
            Maximo = Maximo - (Maximo % (mfac / 10));
        }
        Media = Maximo / 2;
        Parte3 = Media + Media / 2;
        Parte2 = Media / 2;
    }

    int calculaAltura (float valor) {
        int altura = 0;
        altura = (int) (valor / Maximo * 200);
        return altura;
    }

    int calculaAltura2 (float valor) {
        return 200 - calculaAltura (valor);
    }
%>
/******************  Esto no es mio ***************************************************/



function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}



function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v3.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}



/************************************************************************************/


    <%=request.getAttribute ("newMenu")%>
</SCRIPT>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*"><%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado" ) %>
<%
    Formato = request.getParameter("vGraficar").equals("I");
    Posicion = 5;
    int numMes = 1;
    String mes;
    String [] meses = new String [Matriz.keySet ().size ()];
    int size = Matriz.keySet ().size ();
    Iterator iterator = Matriz.keySet().iterator();
    int x = size - 1;
    iterator = Matriz.keySet().iterator();
    while (iterator.hasNext ()) {
        meses [x--] = (String) iterator.next ();
    }
    while ((numMes - 1) < meses.length) {
        mes = meses [numMes - 1];
        HashMap temp = (HashMap) Matriz.get (mes);
        float porPagar = ((Float) temp.get("Por Pagar")).floatValue();
        float pagados = ((Float) temp.get("Pagados")).floatValue();
        float liberados = ((Float) temp.get("Liberados no pagados")).floatValue();
        float anticipados = ((Float) temp.get("Anticipados")).floatValue();
        float cancelados = ((Float) temp.get("Cancelados")).floatValue();
        float vencidos = ((Float) temp.get("Vencidos")).floatValue();
        float rechazados = ((Float) temp.get("Rechazados")).floatValue();
        float recibidos = ((Float) temp.get("Recibidos")).floatValue();
        calculaMaximo (recibidos);
        try {
%>
    <DIV id='Maximo<%=numMes%>' style='position: absolute; width: 60px; height: 10px; z-index: 1; left: <%=Posicion%>px; top: 220px; text-align: right' class='tabmovtex'>
        <%if (Formato) out.print(Currency.format (Maximo)); else out.print(Numero.format (Maximo));%>
    </DIV>
    <DIV id='Parte3<%=numMes%>' style='position: absolute; width: 60px; height: 10px; z-index: 1; left: <%=Posicion%>px; top: 263px; text-align: right' class='tabmovtex'>
        <%if (Formato) out.print(Currency.format (Parte3)); else out.print(Numero.format (Parte3));%>
    </DIV>
    <DIV id='Media<%=numMes%>' style='position: absolute; width: 60px; height: 10px; z-index: 1; left: <%=Posicion%>px; top: 313px; text-align: right' class='tabmovtex'>
        <%if (Formato) out.print(Currency.format (Media)); else out.print(Numero.format (Media));%>
    </DIV>
    <DIV id='Parte2<%=numMes%>' style='position: absolute; width: 60px; height: 10px; z-index: 1; left: <%=Posicion%>px; top: 363px; text-align: right' class='tabmovtex'>
        <%if (Formato) out.print(Currency.format (Parte2)); else out.print(Numero.format (Parte2));%>
    </DIV>
    <DIV id='Base<%=numMes%>' style='position: absolute; width: 60px; height: 10px; z-index: 1; left: <%=Posicion%>px; top: 406px; text-align: right' class='tabmovtex'>
        <%if (Formato) out.print(Currency.format (0)); else out.print(Numero.format (0));%>
    </DIV>
    <DIV id='mes<%=numMes%>' style='position: absolute; width: 257px; height: 10px; z-index: 1; left: <%=Posicion%>px; top: 424px; text-align: center' class='tittabtex'>
        <%=mes%>
    </DIV>
    <%Posicion += 25;%>
    <DIV id='fondo<%=numMes%>' style='position: absolute; width: 170px; height: 200px; z-index: 1; left: <%=Posicion + 35%>px; top: 221px; text-align: right; vertical-align: top'>
        <IMG src='/gifs/EnlaceMig/fondo.jpg' width='195' height='200' border='0'>
    </DIV>
    <DIV id='fondo<%=numMes%>' style='position: absolute; width: 170px; height: 200px; z-index: 1; left: <%=Posicion + 30%>px; top: 220px; text-align: right; vertical-align: top'>
        <table border='0' cellpadding='0' cellspacing='5'>
            <tr>
              <td height='200' width='20' valign='bottom'>
                <img src='/gifs/EnlaceMig/recibidos.jpg' width='20' height='<%=calculaAltura (recibidos)%>' border='0'>
              </td>
              <td height='200' width='20' valign='bottom'>
                <IMG src='/gifs/EnlaceMig/porpagar.jpg' width='20' height='<%=calculaAltura (porPagar)%>' border='0'>
              </td>
              <td height='200' width='20' valign='bottom'>
                <IMG src='/gifs/EnlaceMig/pagados.jpg' width='20' height='<%=calculaAltura (pagados)%>' border='0'>
              </td>
              <td height='200' width='20' valign='bottom'>
                <IMG src='/gifs/EnlaceMig/liberados.jpg' width='20' height='<%=calculaAltura (liberados)%>' border='0'>
              </td>
              <td height='200' width='20' valign='bottom'>
                <IMG src='/gifs/EnlaceMig/anticipados.jpg' width='20' height='<%=calculaAltura (anticipados)%>' border='0'>
              </td>
              <td height='200' width='20' valign='bottom'>
                <IMG src='/gifs/EnlaceMig/cancelados.jpg' width='20' height='<%=calculaAltura (cancelados)%>' border='0'>
              </td>
              <td height='200' width='20' valign='bottom'>
                <IMG src='/gifs/EnlaceMig/vencidos.jpg' width='20' height='<%=calculaAltura (vencidos)%>' border='0'>
              </td>
              <td height='200' width='20' valign='bottom'>
                <IMG src='/gifs/EnlaceMig/rechazados.jpg' width='20' height='<%=calculaAltura (rechazados)%>' border='0'>
              </td>
            </tr>
        </table>
    </DIV>
    <DIV id='tabla<%=numMes%>' style='position: absolute; width: 232px; height: 50px; z-index: 2; left: <%=Posicion - 25%>px; top: 450px; text-align: right; vertical-align: top' class='tabmovtex'>
        <table border='0' cellpadding='0' cellspacing='3'>
          <tr>
            <td width='20' height='10'>
              <img src='/gifs/EnlaceMig/recibidos.jpg' width='20' height='10' border='0'>
            </td>
            <td class='tabmovtex' height='10'>
              Recibidos
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (recibidos)); else out.print(Numero.format (recibidos));%>
            </td>
            <td width='20' height='10'>
              <img src='/gifs/EnlaceMig/porpagar.jpg' width='20' height='10' border='0'>
            </td>
            <td class='tabmovtex' height='10'>
              Por Pagar
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (porPagar)); else out.print(Numero.format (porPagar));%>
            </td>
          </tr>
          <tr>
            <td width='20' height='10'>
              <img src='/gifs/EnlaceMig/pagados.jpg' width='20' height='10' border='0'>
            </td>
            <td class='tabmovtex' height='10'>
              Pagados
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (pagados)); else out.print(Numero.format (pagados));%>
            </td>
            <td width='20' height='10'>
              <img src='/gifs/EnlaceMig/liberados.jpg' width='20' height='10' border='0'>
            </td>
            <td class='tabmovtex' height='10'>
              Liberados no pagados
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (liberados)); else out.print(Numero.format (liberados));%>
            </td>
          </tr>
          <tr>
            <td class='tabmovtex' width='20' height='10'>
              <img src='/gifs/EnlaceMig/anticipados.jpg' width='20' height='10' border='0'>
            </td>
            <td class='tabmovtex' height='10'>
              Anticipados
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (anticipados)); else out.print(Numero.format (anticipados));%>
            </td>
            <td class='tabmovtex' width='20' height='10'>
              <img src='/gifs/EnlaceMig/cancelados.jpg' width='20' height='10' border='0'>
            </td>
            <td height='10' class='tabmovtex'>
              Cancelados
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (cancelados)); else out.print(Numero.format (cancelados));%>
            </td>
          </tr>
          <tr>
            <td width='20' height='10'>
              <img src='/gifs/EnlaceMig/vencidos.jpg' width='20' height='10' border='0'>
            </td>
            <td class='tabmovtex' height='10'>
              Vencidos
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (vencidos)); else out.print(Numero.format (vencidos));%>
            </td>
            <td width='20' height='10'>
              <img src='/gifs/EnlaceMig/rechazados.jpg' width='20' height='10' border='0'>
            </td>
            <td class='tabmovtex' height='10'>
              Rechazados
            </td>
            <td class='tabmovtex' height='10' align='right'>
              <%if (Formato) out.print(Currency.format (rechazados)); else out.print(Numero.format (rechazados));%>
            </td>
          </tr>
        </table>
    </DIV>
<%
        } catch (NumberFormatException ex) {
        }
        Posicion += 280;
        ++numMes;
    }
%>

<DIV id='boton' style='position: absolute; width: 100%; heig: 50px; z-index: 1; left: 0px; top: 550px'>
  <FORM action='confGrafica' method='POST' name='frmGrafica'>
    <TABLE border='0' cellpadding='0' cellspacing='0' align='center'>
      <TR>
        <TD>
          <A href='javascript:this.submit ();'><IMG src='/gifs/EnlaceMig/gbo25320.gif' alt='Regresar' border='0'></A>
          <INPUT type='hidden' name='Opcion' value='1'>
        </TD>
      </TR>
    </TABLE>
  </FORM>
</DIV>

</body>
</html>
