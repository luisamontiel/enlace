<jsp:include page="/jsp/prepago/headerPrepago.jsp"/>
<%@page import="mx.altec.enlace.beans.TarjetaContrato,java.util.*"%>
<script type="text/javascript">
window.onload = load;
var original;
function altaTarjeta()
{
	var val = 0;
 	var radios = document.AltaVinTar.tipoCarga;
	for(var i = 0; i < radios.length; i++) {
		if( radios[i].checked == true )
		val = radios[i].value;
	}
	if (val == "AI") {
		if (document.AltaVinTar.Archivo.value == "")
		{
			cuadroDialogo( "Debe especificar el nombre del archivo." ,1);
			return false;
		}
		else
		{
			if(document.AltaVinTar.Archivo.value.substring(document.AltaVinTar.Archivo.value.length-4,document.AltaVinTar.Archivo.value.length).toUpperCase() != ".TXT")
			{
				cuadroDialogo( "La extensi&oacute;n del archivo es incorrecta, verifique que sea 'txt'" ,1);
				return false;
			}
			else
			{
				document.AltaVinTar.AltaVinTar2.value="";
				document.AltaVinTar.action = "EnlcAltaTarjeta?operacion=3";
				document.AltaVinTar.submit();
			}
		}
	}
	else{
		if (document.AltaVinTar.tarjeta.value == "")
		{
			cuadroDialogo( "El campo n&uacute;mero de tarjeta es requerido." ,1);
			return false;
		}
		if (document.AltaVinTar.tarjeta.value.length != 16)
		{
			cuadroDialogo( "El campo n�mero de tarjeta es incorrecto.", 1);
			return false;
		}
		if(document.AltaVinTar.usuario.value == "")
		{
			cuadroDialogo( "El campo clave de empleado es requerido." ,1);
			return false;
		}
		if(document.AltaVinTar.usuario.value.length > 7)
		{
			cuadroDialogo( "El campo clave de empleado es incorrecto." ,1);
			return false;
		}
		document.AltaVinTar.action = "EnlcAltaTarjeta?operacion=2";
		document.AltaVinTar.submit();
	}
}
function enviarTarjeta()
{
	document.AltaVinTar.action = "EnlcAltaTarjeta?operacion=2";
	document.AltaVinTar.submit();
}
function load()
{
	if(document.AltaVinTar.tipoCarga[0].checked)
	{
		original = document.AltaVinTar.tipoCarga[0].value;
		document.AltaVinTar.tipoCarga[0].focus();
		document.AltaVinTar.tipoCarga[0].checked = true;
	}
	else
	{
		original = document.AltaVinTar.tipoCarga[1].value;
		document.AltaVinTar.tipoCarga[1].focus();
		document.AltaVinTar.tipoCarga[1].checked = true;
	}
}

	<%
	if(session !=null){
		if(session.getAttribute("tipoCarga")=="AI")
			session.setAttribute("tipoCarga","AL");
		else
			session.setAttribute("tipoCarga","AI");
	}
	%>
function reload()
{
	var i;
	var ctrl=document.AltaVinTar.tipoCarga;
	for(i=0; i<ctrl.length; i++)
	{
		if(ctrl[i].checked)
		{
			if(original != ctrl[i].value){
				//window.location.reload();
				document.forms[0].action='/Enlace/enlaceMig/EnlcAltaTarjeta?operacion=1&flujo=ALTC';
				document.forms[0].submit();
				break;
			}
		}
	}
}
function soloNumeros(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 } 
function soloNumerosBlur(evt) {
  	if (isNaN(document.AltaVinTar.tarjeta.value)) {
		document.AltaVinTar.tarjeta.value = "";
		document.AltaVinTar.tarjeta.focus();
		return false;
 	}
 }
 
function soloNumerosBlurUsuario(evt) {
  	if (isNaN(document.AltaVinTar.usuario.value)) {
		document.AltaVinTar.usuario.value = "";
		document.AltaVinTar.usuario.focus();
		return false;
 	}
 }
 
function limpiar(){
	document.AltaVinTar.action = "EnlcAltaTarjeta?operacion=8";
	document.AltaVinTar.submit();
}
</script>
<style>
	div.fileinputs {
	position: relative;
	}

	div.fakefile {
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 1;
	}
</style>
<FORM  NAME="AltaVinTar" method="post" ACTION="EnlcAltaTarjeta" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" VALUE="1" NAME="statusDuplicado">
<INPUT TYPE="hidden" VALUE="1" NAME="statushrc">
<%if(( session != null && (session.getAttribute("tipoCarga")== null || session.getAttribute("tipoCarga").equals("AL")))){ %>
			
			<table width="760" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center">
				<table width="600" border="0" cellspacing="2" cellpadding="2" bgcolor="#EBEBEB">
					<tr>
						<td width="600" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AL" onclick="reload();" checked="checked"/>Alta en l&iacute;nea
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							N&uacute;mero de tarjeta:
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="" maxlength="<%=TarjetaContrato.lngNoTarjeta%>" size="25" id="tarjeta" name="tarjeta" onkeypress="return soloNumeros(event)" onkeyup="return soloNumerosBlur(event)">
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							Clave del empleado:
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="" maxlength="<%=TarjetaContrato.lngContrato%>" size="25" id="usuario" name="usuario" onkeypress="return soloNumeros(event)" onkeyup="return soloNumerosBlurUsuario(event)">
						</td>
					</tr>
				</table>
				<br>
				<table width="600" border="0" cellspacing="2" cellpadding="3" bgcolor="#EBEBEB">
					<tr>
						<td width="100%" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AI" onclick="reload();"/>Importar desde archivo Externo
						</td>
					</tr>
					<tr>
						<td class=textabdatcla bgcolor="#FFFFFF"  colspan="3">
							<div class="fileinputs">
								<INPUT TYPE="file" NAME="Archivo" size="50" onchange="document.AltaVinTar.AltaVinTar2.value=this.value" disabled="disabled">
								<div class="fakefile">
									<input type="Text" NAME="AltaVinTar2" id="AltaVinTar2" readonly size="50" disabled="disabled">
								</div>
							</div>
						</td>
					</tr>
				</table>
				<br>
				<table width="90" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
					<tr>
						<td align="center">
							<input type="image" border="0" name="Agregar"
								src="/gifs/EnlaceMig/gbo25290.gif" width="90" height="22"
								alt="Agregar" onclick="javascript:altaTarjeta();return false">
						</td>
						<td align="center">
							<input type="image" border="0" name="Limpiar"
									src="/gifs/EnlaceMig/gbo25250.gif" width="90" height="22"
									alt="Limpiar" onclick="javascript:limpiar();return false">
						</td>
					</tr>
				</table>
			    </td>
			</tr>
		</table>
		
			<%}else { %>
					<table width="760" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center">
				<table width="600" border="0" cellspacing="2" cellpadding="2" bgcolor="#EBEBEB">
					<tr>
						<td width="600" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AL" onclick="reload();"/>Alta en l&iacute;nea
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							N&uacute;mero de tarjeta:
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="" maxlength="<%=TarjetaContrato.lngNoTarjeta%>" size="25" id="tarjeta" name="tarjeta" onkeypress="return soloNumeros(event)" disabled="disabled">
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							Clave del empleado:
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="" maxlength="<%=TarjetaContrato.lngContrato%>" size="25" id="usuario" name="usuario" onkeypress="return soloNumeros(event)" disabled="disabled">
						</td>
					</tr>
				</table>
				<br>
				<table width="600" border="0" cellspacing="2" cellpadding="3" bgcolor="#EBEBEB">
					<tr>
						<td width="100%" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AI" onclick="reload();"  checked="checked"/>Importar desde archivo Externo
						</td>
					</tr>
					<tr>
						<td class=textabdatcla bgcolor="#FFFFFF"  colspan="3">
							<div class="fileinputs">
								<INPUT TYPE="file" NAME="Archivo" size="50" onchange="document.AltaVinTar.AltaVinTar2.value=this.value">
								<div class="fakefile">
									<input type="Text" NAME="AltaVinTar2" id="AltaVinTar2" readonly size="50">
								</div>
							</div>
						</td>
					</tr>
				</table>
				<br>
				<table width="90" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
					<tr>
						<td align="center">
							<input type="image" border="0" name="Agregar"
								src="/gifs/EnlaceMig/gbo25290.gif" width="90" height="22"
								alt="Agregar" onclick="javascript:altaTarjeta();return false">
						</td>
						<td align="center">
							<input type="image" border="0" name="Limpiar"
									src="/gifs/EnlaceMig/gbo25250.gif" width="90" height="22"
									alt="Limpiar" onclick="javascript:limpiar();return false">
						</td>
					</tr>
				</table>
			    </td>
			</tr>
		</table>
		<%} %>

	<script>
			<% if (session != null && session.getAttribute("MensajeLogin01") != null ) { %>
			<%= session.getAttribute("MensajeLogin01") %>
			<%	} %>
	</script>

</form>
<jsp:include page="/jsp/prepago/footerPrepago.jsp"/>
