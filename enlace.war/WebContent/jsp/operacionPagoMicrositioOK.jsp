<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="mx.altec.enlace.utilerias.*"%>
<%
    Calendar dt = Calendar.getInstance();
    dt.setTime( new java.util.Date() );
    int mes = dt.get(Calendar.MONTH);
    int dia = dt.get(Calendar.DAY_OF_MONTH);
    int anio = dt.get(Calendar.YEAR);

	java.util.Date hora = new java.util.Date();
	int hh = hora.getHours();
	int mm = hora.getMinutes();
	int ss = hora.getSeconds();
%>

<%!
private String sinComas (String a) {
    StringBuffer sb = new StringBuffer();
    for (int k=0; k < a.trim().length() ;k++){
        if(a.charAt(k)!=',')
            sb.append(a.charAt(k));
    }
    return sb.toString();
}
%>

<%!
private   String formatea ( String a){
    String temp ="";
    String deci = "";
    String regresa = "";
    int valexp = 0;
    int indice =    a.indexOf(".");
    int otroindice  = a.indexOf("E");
	try{
		if( otroindice>0 ){
			BigDecimal mivalor = new BigDecimal( a );
			a = mivalor.toString();
			indice =    a.indexOf(".");
		}
		if( indice>0 )
			a = (new BigDecimal(a+"0")).toString();
		else
			a = (new BigDecimal(a+".00")).toString();
		indice =        a.indexOf(".");
		temp = a.substring (0, indice);
		deci = a.substring ( indice+1, indice+3 );

		String original = temp;
		int indexSN = 0;
		boolean bSignoNegativo = false;
		if( ( indexSN = original.indexOf("-") )> -1 ){
			bSignoNegativo = true;
			temp = original.substring( indexSN + 1 , temp.length() );
		}
		while( temp.length()>3 ){
	        a = temp.substring(temp.length() -3, temp.length() );
		    regresa = ","+a+regresa;
			temp = temp.substring(0,temp.length()-3);
		}
		regresa = temp + regresa+"."+deci;
	    if( bSignoNegativo ){
	        regresa = "($"+regresa +")";
		}
	}catch(Exception e){  }
    return regresa;
}
%>


<%
	String empresa = Util.HtmlEncode((String)request.getAttribute("empresa"));
	String convenio = (String)request.getAttribute("convenio");
	String cuentaCargo = (String)request.getAttribute("cuentaCargo");
	String titularCuentaCargo = Util.HtmlEncode((String)request.getAttribute("titularCuentaCargo"));
	String cuentaAbono = (String)request.getAttribute("cuentaAbono");
	String importe = (String)request.getAttribute("importe");
	String saldoDespues = (String)request.getAttribute("saldoDespues");
	String folioOper = (String)request.getAttribute("folioOper");
	String fecha = (String)request.getAttribute("fecha");
	String hora_oper = (String)request.getAttribute("hora");
	String estatusOper = (String)request.getAttribute("estatus");
	String saldoCtaOrigen = (String)request.getAttribute("saldoCtaOrigen");
	String numContrato = (String)request.getAttribute("numContrato");
	String nomContrato = Util.HtmlEncode((String)request.getAttribute("nomContrato"));
	String numUsuario = (String)request.getAttribute("numUsuario");
	String nomUsuario = (String)request.getAttribute("nomUsuario");
	String msgRstOperacion = (String)request.getAttribute("msgRstOperacion");
	String servicio_id = Util.HtmlEncode((String)request.getAttribute("servicio_id"));
	String banco = "014";
    String fin_datos = "fin_de_datos";


	String urlCmp = "";
	String url = "";
	String referencia = "";
	String concepto = "";

	String lineaCaptura = "";
	String sreferencia_390 = "";

	if(!"PMRF".equals(servicio_id)){
		url = (String)request.getAttribute("url");
		referencia = Util.HtmlEncode((String)request.getAttribute("referencia"));
	    concepto = Util.HtmlEncode((String)request.getAttribute("concepto"));
	} else {
		lineaCaptura = Util.HtmlEncode((String)request.getAttribute("linea_captura"));
		sreferencia_390 = Util.HtmlEncode((String)request.getAttribute("sreferencia_390"));
	}



	String moneda = cuentaAbono.substring(0,2);
	System.out.println("[------------------------------------------------------------------------]");
	System.out.println("Valores PagoMicrositioOK........." + moneda);
	System.out.println("empresa: [" + empresa + "]");
	System.out.println("convenio: [" + convenio + "]");
	System.out.println("cuentaCargo: [" + cuentaCargo + "]");
	System.out.println("titularCuentaCargo: [" + titularCuentaCargo + "]");
	System.out.println("cuentaAbono: [" + cuentaAbono + "]");
	System.out.println("importe: [" + importe + "]");
	System.out.println("saldoDespues: [" + saldoDespues + "]");
	System.out.println("folioOper: [" + folioOper + "]");
	System.out.println("fecha: [" + fecha + "]");
	System.out.println("hora_oper: [" + hora_oper + "]");
	System.out.println("estatusOper: [" + estatusOper + "]");
	System.out.println("saldoCtaOrigen: [" + saldoCtaOrigen + "]");
	System.out.println("numContrato:[" + numContrato + "]");
	System.out.println("nomContrato:[" + nomContrato + "]");
	System.out.println("numUsuario: [" + numUsuario + "]");
	System.out.println("nomUsuario: [" + nomUsuario + "]");
	System.out.println("servicio_id: [" + servicio_id + "]");
	System.out.println("banco: [" + banco + "]");
	System.out.println("fin_datos: [" + fin_datos + "]");
	System.out.println("moneda: [" + moneda + "]");

	if(!"PMRF".equals(servicio_id)){
		System.out.println("url: [" + url + "]");
		System.out.println("concepto: [" + concepto + "]");
		System.out.println("referencia: [" + referencia + "]");
	} else {
		System.out.println("linea_captura: [" + lineaCaptura + "]");
	}

	System.out.println("[------------------------------------------------------------------------]");

	System.out.println("MONEDA ........." + moneda);
	String mensaje = "TransaccionOK";
	String estatusNum = "0";
	if (estatusOper.equals("Mancomunada")) {
		mensaje = "Mancomunada";
		estatusNum = "1";
	} else if (estatusOper.equals("No Ejecutada")) {
		mensaje = "Rechazada";
	}

%>
<html>
	<head>
	<title>Santander</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<link rel="stylesheet" href="/EnlaceMig/pagos.css" type="text/css"/>
	<script type="text/JavaScript">

	function actualizaPantalla(){
	    document.pagook.botonSalir.value = "SI";
	    <%if(!"PMRF".equals(servicio_id)){%>
	    	document.pagook.action="<%=url.trim()%>";
	    <%} else {%>
			document.pagook.method ="post";
	    	document.pagook.action="<%=Global.REDIR_LOGOUT_SAM%>";
	    <%}%>
	    document.pagook.submit();
	}

	function despliegaDatos() {
    if(document.getElementById("ConvenioSD") !== null
        && document.getElementById("ConvenioSD").value !== "undefined"
        && document.getElementById("ConvenioSD").value === 'true'){
      var urlMapping = '/Enlace/enlaceMig/ComprobanteMicrositioSD';

      // cadena de parametros para el comprobante sello digital
      param0="&descripcion="+document.pagook.referencia.value;
      param1="&importe="+document.pagook.importe.value;
      param2="&refe="+document.pagook.folioOper.value;
      param3="&fecha="+document.pagook.fecha.value;
      param4="&hora="+document.pagook.hora.value.substring(0, 5);
      param5="&cta_destino="+document.pagook.cuentaAbono.value;
      param6="&idconvenio="+document.pagook.idconvenio.value;
      param7="&action="+urlMapping;

      param0=formato(param0);
      param1=formato(param1);
      param2=formato(param2);
      param4=formato(param4);
      param5=formato(param5);
      param6=formato(param6);
      param7=formato(param7);
      var mod = 'VSD';

      vc = window.open("/Enlace/enlaceMig/ComprobanteMicrositioSD?"
          +param0+param1+param2+param3+param4+param5+param6+param7
          +'&mod=' + mod ,'trainerWindow',
          'scrollbars=yes,resizable=yes,width=450,height=460');
      vc.focus();
    }else{
      // cadena de parametros para el comprobante
      param1="numContrato="+document.pagook.numContrato.value;
      param2="&numUsuario="+document.pagook.numUsuario.value;
      param3="&nomUsuario="+document.pagook.nomUsuario.value;
      param4="&convenio="+document.pagook.convenio.value;
      param5="&empresa="+document.pagook.empresa.value;
      param6="&fecha="+document.pagook.fecha.value;
      param7="&hora="+document.pagook.hora.value;
      param8="&folioOper="+document.pagook.folioOper.value;
      param9="&referencia="+document.pagook.referencia.value;
      param10="&cuentaCargo="+document.pagook.cuentaCargo.value;
      param11="&cuentaAbono="+document.pagook.cuentaAbono.value;
      param12="&titularCuentaCargo="+document.pagook.titularCuentaCargo.value;
      param13="&importe="+document.pagook.importe.value;
      param14="&estatusOper="+document.pagook.estatusOper.value;

      param1=formato(param1);
      param2=formato(param2);
      param3=formato(param3);
      param4=formato(param4);
      param5=formato(param5);
      param6=formato(param6);
      param7=formato(param7);
      param8=formato(param8);
      param9=formato(param9);
      param10=formato(param10);
      param11=formato(param11);
      param12=formato(param12);
      param13=formato(param13);
      param14=formato(param14);

      //alert("/NASApp/enlaceMig/jsp/comprobanteMicrositio.jsp?"+param1+param2+param3+param4+param5+param6+param7+param8+param9+param10+param11+param12+param13+param14);

      vc=window.open("/Enlace/jsp/comprobanteMicrositio.jsp?"+param1+param2+param3+param4+param5+param6+param7+param8+param9+param10+param11+param12+param13+param14,'Comprobante','scrollbars=yes,resizable=yes,width=450,height=460');
      vc.focus();
    }
  }

	function formato(para)
	 {
	   var formateado="";
	   var car="";

	   for(a2=0;a2<para.length;a2++)
	    {
		  if(para.charAt(a2)==' ')
		   car="+";
		  else
		   car=para.charAt(a2);

		  formateado+=car;
		}
	   return formateado;
	 }

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}

	function validarSesion()
	 {
		var botonSalir = document.pagook.botonSalir.value;
			if (botonSalir == "SI"){
				ventana=window.open('/Enlace/enlaceMig/logout?operacionOK=S','trainerWindow','width=580,height=350,toolbar=no,scrollbars=no,left=210,top=225');
			} else if (botonSalir == "NO") {
				ventana=window.open('/Enlace/enlaceMig/logout?operacionOK=N','trainerWindow','width=580,height=350,toolbar=no,scrollbars=no,left=210,top=225');
			}
			document.pagook.botonSalir.value="NO";
	 }

	var dayarray=new Array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
	var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


	var mydate=new Date(<%=anio%>,<%=mes%>,<%=dia%>,<%=hh%>,<%=mm%>,<%=ss%>);

	function reloj()
	 {
	   if(!document.layers && !document.all)
	     return;

	   var digital = new Date();
	   var year=digital.getYear();
	   if (year < 1000)
	      year+=1900;
	   var day=digital.getDay();
	   var month=digital.getMonth();
	   var daym=digital.getDate();
	   if (daym<10)
	      daym="0"+daym;

	   var horas = digital.getHours();
	   var minutos = digital.getMinutes();
	   var segundos = digital.getSeconds();
	   var amOrPm = "AM";
	   if (horas > 11)
	     amOrPm = "PM";
	   if (horas > 12)
	     horas = horas - 12;
	   if (horas == 0)
	     horas = 12;
	   if (minutos <= 9)
	     minutos = "0" + minutos;
	   if (segundos <= 9)
	     segundos = "0" + segundos;

	   dispTime = "<font color='666666' face='Verdana'>" + dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" | "+horas+":"+minutos+":"+segundos+" "+ amOrPm +"</font>";
	   //dispTime = horas + ":" + minutos + ":" + segundos + " " + amOrPm;
	   if (document.layers)
	    {
	      document.layers.objReloj.document.write("<font face=Tahoma size=1>"+ dispTime + "</font>");
		  document.layers.objReloj.document.close();
		}
	   else
	    if (document.all)
		  objReloj.innerHTML = dispTime;
	   setTimeout("reloj()", 1000);
	 }

	 function muestraComprobante()
	 {
	 /*
	  * Genera comprobante para pago de impuestos
	  */
	  var parametros=("contrato=<%=numContrato.trim()%>&desccto=<%=nomContrato%>&usuario=<%=numUsuario.trim()%>&concepto=<%=lineaCaptura.trim()%>&importe=<%=formatea(importe).trim()%>&fecha=<%=fecha.trim()%>&hora=<%=hora_oper.trim()%>&referencia=<%=folioOper.trim()%>&referencia390=<%=sreferencia_390%>&ctacargo=<%=cuentaCargo.trim()%>&titcta=<%=titularCuentaCargo.trim()%>").replace(/\s/ig,'+');
	  	vc=window.open("/Enlace/jsp/CompNvoPILCPagoRefSat.jsp?"+parametros,'trainerWindow','width=490,height=450,toolbar=no,scrollbars=yes');
	 /*
	  * Fin RRR - Indra Marzo 2014
	  */
	  vc.focus();
	 }

	//-->
	</script>
	</head>

	<body onLoad="reloj();" onBeforeUnload="validarSesion();">

	
		<% if (!"PMRF".equals(servicio_id)){
				char caracter = url.trim().indexOf("?") == -1 ? '?' : '&';
				urlCmp = url.trim() + caracter + "mensaje=" + mensaje + "&folio_oper=" + folioOper + "&estatus=0&numContrato=" + numContrato.trim() +
				"&numUsuario=" + numUsuario.trim() + "&nomUsuario=" + nomUsuario.trim() + "&convenio=" + convenio.trim() + "&empresa=" + empresa.trim() +
				"&fecha=" + fecha.trim() + "&hora=" + hora_oper.trim() + "&folioOper=" + folioOper.trim() + "&referencia=" + referencia.trim() +
				"&cuentaCargo=" + cuentaCargo.trim() + "&cuentaAbono=" + cuentaAbono.trim() + "&titularCuentaCargo=" + titularCuentaCargo.trim() +
				"&importe=" + formatea(importe).trim() + "&estatusOper=" + estatusOper.trim() + "&concepto="  + concepto.trim() +
				"&servicio_id=" +servicio_id.trim() + "&banco=014&fin_datos=fin_de_datos&method=POST";
				System.out.println("urlCmp: [" + urlCmp + "]");
		}%>

		

		<span id="PAGINA_SINFRAME">
			<span id="objReloj" style="position:absolute;left:285;top:60;"></span>
			<table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="147"><img src="/gifs/EnlaceMig/cheque_img.jpg" width="147" height="40"/></td>
				<td width="593" valign="top" bgcolor="#FF0000"><img src="/gifs/EnlaceMig/top.gif" width="453" height="40"/></td>
			  </tr>
			  <tr>
				<td background="/gifs/EnlaceMig/sombra_gris.jpg"><img src="/gifs/EnlaceMig/sombra_gris.jpg" width="18" height="14"/></td>
				<td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
			  </tr>
			  <tr>
				<td bgcolor="#F0EEEE">&nbsp;</td>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="6">
					<tr>
					  <td align="left" class="titulorojo"> <div align="left">
						  <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
							  <td align="right"><a href="javascript:;" onClick=" javascript:document.pagook.botonSalir.value='HLP'; MM_openBrWindow('/EnlaceMig/ayuda004_enlace.html','ayuda','scrollbars=yes,resizable=yes,width=340,height=320')" onMouseOver="MM_swapImage('Image31','','/gifs/EnlaceMig/ayuda_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/ayuda.jpg" name="Image31" width="47" height="40" border="0" id="Image3"/></a></td>
							</tr>
							<tr>
							  <td class="titulorojo">
								<% if ("Aceptada".equals(estatusOper)) { %>
									<p>Operaci&oacute;n realizada exitosamente</p>
								<%} else if ("Mancomunada".equals(estatusOper)){ %>
									<p>Operaci&oacute;n Mancomunada</p>
								<%} else if ("No Ejecutada".equals(estatusOper) || "Rechazada".equals(estatusOper)){ %>
									<p><%=msgRstOperacion%></p>
								<%}%>
								<p align="center"><%=numContrato%> | <%=nomContrato%></p></td>
							  <td align="right">&nbsp;</td>
							</tr>
						  </table>
						</div></td>
					</tr>
				  </table></td>
			  </tr>
			  <tr>
				<td bgcolor="#F0EEEE">&nbsp;</td>
				<td><form name="pagook">

					<%if(!"PMRF".equals(servicio_id)) {%>
						<INPUT TYPE="hidden" name="mensaje" value="<%=mensaje%>"/>
						<INPUT TYPE="hidden" name="folio_oper" value="<%=folioOper%>"/>
						<INPUT TYPE="hidden" name="estatus" value="<%=estatusNum%>"/>
						<INPUT TYPE="hidden" name="numContrato" value="<%=numContrato.trim()%>"/>
						<INPUT TYPE="hidden" name="numUsuario" value="<%=numUsuario.trim()%>"/>
						<INPUT TYPE="hidden" name="nomUsuario" value="<%=nomUsuario.trim()%>"/>
						<INPUT TYPE="hidden" name="convenio" value="<%=convenio.trim()%>"/>
						<INPUT TYPE="hidden" name="empresa" value="<%=empresa.trim()%>"/>
						<INPUT TYPE="hidden" name="fecha" value="<%=fecha.trim()%>"/>
						<INPUT TYPE="hidden" name="hora" value="<%=hora_oper.trim()%>"/>
						<INPUT TYPE="hidden" name="folioOper" value="<%=folioOper.trim()%>"/>
						<INPUT TYPE="hidden" name="referencia" value="<%=referencia.trim()%>"/>
						<INPUT TYPE="hidden" name="cuentaCargo" value="<%=cuentaCargo.trim()%>"/>
						<INPUT TYPE="hidden" name="cuentaAbono" value="<%=cuentaAbono.trim()%>"/>
						<INPUT TYPE="hidden" name="titularCuentaCargo" value="<%=titularCuentaCargo.trim()%>"/>
						<INPUT TYPE="hidden" name="importe" value="<%=formatea(importe).trim()%>"/>
						<INPUT TYPE="hidden" name="estatusOper" value="<%=estatusOper.trim()%>"/>
						<INPUT TYPE="hidden" name="concepto" value="<%=concepto.trim()%>"/>
						<INPUT TYPE="hidden" name="servicio_id" value="<%=servicio_id.trim()%>"/>
						<INPUT TYPE="hidden" name="banco" value="<%=banco.trim()%>"/>
						<INPUT TYPE="hidden" name="fin_datos" value="<%=fin_datos.trim()%>"/>
					<%} %>
					<input type="hidden" value="${pageContext.request.contextPath}" id="hdContexto"/>
					<input type="hidden" value="${ConvenioSD}" id="ConvenioSD" name="ConvenioSD"/>
					<input type="hidden" value="${idconvenio}" id="idconvenio" name="idconvenio"/>
					

					<INPUT TYPE="hidden" name="botonSalir" value="NO"/>
					<table width="100%" border="0" cellspacing="6" cellpadding="0">
					  <tr align="center">
						<td><table width="100%" border="0" cellspacing="4" cellpadding="2">
							<tr align="center">
							  <td bgcolor="#CCCCCC">Empresa</td>
							  <td width="2" bgcolor="#CCCCCC"><div align="center">Cuenta de
								  Cargo</div></td>
								 <%if(!"PMRF".equals(servicio_id)) {%>
									<td bgcolor="#CCCCCC">Referencia</td>
								 <%} else { %>
									<td bgcolor="#CCCCCC">L&iacute;nea de Captura</td>
								 <%} %>
							  <td bgcolor="#CCCCCC">Importe</td>
							  <td bgcolor="#CCCCCC"><div align="center">Fecha de Aplicaci&oacute;n</div></td>
							  <td bgcolor="#CCCCCC"><div align="center">Folio de Transacci&oacute;n</div></td>
							  <td bgcolor="#CCCCCC">Estatus</td>
							  <% if ("Aceptada".equals(estatusOper) && !"PMRF".equals(servicio_id)) { %>
									<td bgcolor="#CCCCCC">Saldo</td>
							  <%} %>
							</tr>
							<tr align="center">
							  <td bgcolor="#F0EEEE"><div align="center"><%=convenio%><br>
								  <%=empresa%><br/>
								</div></td>
							  <td bgcolor="#F0EEEE"><%=cuentaCargo%></td>

							  <%if(!"PMRF".equals(servicio_id)) {%>
									<td bgcolor="#F0EEEE"><%=referencia%></td>
								 <%} else { %>
									<td bgcolor="#F0EEEE"><%=lineaCaptura%></td>
							  <%} %>
							  <%
							   String verimporte="";
							   if(moneda.equals("49") || moneda.equals("82") || moneda.equals("83")){
								  verimporte=formatea(importe) + "USD";
							   }else{
								  verimporte="$" + formatea(importe);
							   }%>
							  <td bgcolor="#F0EEEE"><%=verimporte%></td>
							  <td bgcolor="#F0EEEE"><%=fecha%></td>
							  <td bgcolor="#F0EEEE">

							  <% if ("Aceptada".equals(estatusOper) && !"PMRF".equals(servicio_id)) { %>
								  <a onClick='javascript:document.pagook.botonSalir.value="CMP";' href="javascript:despliegaDatos();"> <%=folioOper%></a></td>
							  <%} else if ("Aceptada".equals(estatusOper) && "PMRF".equals(servicio_id)) {%>
								  <a onClick='javascript:document.pagook.botonSalir.value="CMP";' href="javascript:muestraComprobante();"> <%=folioOper%></a></td>
							  <%} else {%>
							  <%=folioOper%>
							  <%}%>


							  <td bgcolor="#F0EEEE"><%=estatusOper%></td>
							  <% if ("Aceptada".equals(estatusOper) && !"PMRF".equals(servicio_id)) {
										  String versaldo="";
									  if(moneda.equals("49") || moneda.equals("82") || moneda.equals("83")){
										  versaldo=formatea(saldoDespues) + "USD";
									  } else {
										  versaldo="$" + formatea(saldoDespues);
									  }%>
								<td bgcolor="#F0EEEE"><%=versaldo%></td>
							  <% }%>
							</tr>
							<tr align="center">
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
						  </table></td>
					  </tr>
					  <tr>
					  <% if(!"PMRF".equals(servicio_id)){
							if ("Aceptada".equals(estatusOper)) { %>
								<td align="center" colspan="4">Para confirmar el pago a su Empresa d&eacute; clic sobre el bot&oacute;n Salir</td>
							<%} else { %>
							<td align="center" colspan="4">Para confirmar el resultado a su Empresa d&eacute; clic sobre el bot&oacute;n Salir</td>
							<%}
						} else {%>
							<td align="center" colspan="4">Para salir del sistema d&eacute; clic sobre el bot&oacute;n Salir</td>
						<%} %>
					  </tr>
					  <tr><td align="center" colspan="4">&nbsp;</td></tr>
					  <tr align="center">
					   <td><a onClick='javascript:document.pagook.botonSalir.value="SI"; actualizaPantalla();' onMouseOver="MM_swapImage('Image2','','/gifs/EnlaceMig/salir_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/salir.jpg" name="Image2" width="82" height="41" border="0" id="Image2"/></a></td>

					  </tr>
					</table>
				  </form></td>
			  </tr>
			  <tr>
				<td bgcolor="#F0EEEE">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td bgcolor="#F0EEEE">&nbsp;</td>
				<td align="right"><img src="/gifs/EnlaceMig/sitio_seg.jpg" width="66" height="25"/>&nbsp;&nbsp;&nbsp;</td>
			  </tr>
			  <tr>
				<td bgcolor="#F0EEEE">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td bgcolor="#F0EEEE">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td bgcolor="#F0EEEE">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr bgcolor="#CCCCCC">
				<td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
				<td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
			  </tr>
			</table>
		</span>
	</body>
</html>