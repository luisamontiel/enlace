<!--
/*************************************************************************
 * Fecha de �ltima modificaci�n : 22 - Dic - 05
 * Proyecto:200525200 Recaudaci�n de Impuestos Federales V4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: Septiembre 2005
 * Modificaci�n: Adaptaciones correspondientes a las
 * especificaciones t�cnicas del SAT versi�n 4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: 22 - Dic - 05
 * Modificaci�n: Se elimin� el c�digo muerto
 ************************************************************************/
-->

<html>
<head>

<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}


function formatea(para)
{
	var formateado = "";
	var car = "";

	for(a2=0;a2<para.length;a2++)
	{
		if(para.charAt(a2)==' ')
			car="+";
		else
			car=para.charAt(a2);
			formateado+=car;
	}
	return formateado;
}



function actualizacuenta()
{
  document.impuestos.cuenta.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
  document.impuestos.textdestino.value=ctaselec+" "+ctadescr;
}



function Enviar() {


	var Url = "";
	var f = document.impuestos;
		Url = parent.frames['contenido'].Validate();
		f.param.value = parent.frames['contenido'].getPar1();
		f.paramImp.value = parent.frames['contenido'].getPar2();
        f.tramaDesordenada.value = parent.frames['contenido'].getTramaDesordenada();

		// vswf ini
        f.paramXML.value = parent.frames['contenido'].getPar4();
		// vswf fin

	if( verificar() == true ) {
		f.param1.value = Url;
		f.action = Url;
		f.target="_parent";
		f.submit();
	}
}

function verificar() {

	var f = document.impuestos;
		if(f.cuenta.value =="" || f.cuenta.length<4) {
			alert("Debe seleccionar una cuenta");
			document.impuestos.textdestino.value= "";
			return false;
		}
		//f.cuenta.focus();

	if( parent.frames['contenido'].validaApplet()=="*"||parent.frames['contenido'].validaApplet()=="EOF%7C"||parent.frames['contenido'].validaApplet()=="" || parent.frames['contenido'].validaApplet() == "EOF|" ) {
		alert("Debe de capturar al menos un concepto.");
		return false;
	}

	if( parent.frames['contenido'].validaApplet() == "MSG|" ) {
		alert("Debe de cerrar la ventana de Dialogo.");
		return false;
	}

	return true;
}



								   function jreAyuda()
								   {
									sURL = '/EnlaceMig/impuestos/jreAyuda.html';
				res  = window.open(sURL, "EnlaceInternet", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=350");
								   }


//-->
</script>


<title>Nuevo Esquema de Pago de Impuestos</title>
<meta name="Codigo de Pantalla" content="s00280">
<meta name="Version" content="1.0">
<meta name="Ultima Modificacion" content="17/08/2002 15:46">

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="/gifs/EnlaceMig/gfo25010.gif">

<form name="impuestos" action="" target="mainFrame" method="post">
<INPUT TYPE="hidden" name="param" value="">
<INPUT TYPE="hidden" name="paramImp" value="">
<input type="hidden" name="tramaDesordenada" value="">

<!-- vswf ini -->
<INPUT TYPE="hidden" name="paramXML" value="">
<!-- vswf fin -->





<table border="0" cellspacing="0" cellpadding="0" >
	<TR>
		<td colspan=2 class ="texenccon"><img src="/gifs/EnlaceMig/glo25010.gif"></td>
		<td colspan=2 class ="texenccon"><dd>Si se presenta un bloqueo en los campos de captura de su declaraci&oacute;n, d&eacute; click <A HREF="javascript:jreAyuda();">aqu&iacute;.</A>
		<br><dd>Compensaci&oacute;n Universal &nbsp; <font color=red>&iquest;</font>Vas a compensar<font color=red>?</font> &nbsp; Consulta antes los requisitos en la p&aacute;gina del SAT.
		<a href="http://www.sat.gob.mx" target="_blank"> www.sat.gob.mx</a></td>
	</TR>

<!--
	<tr>
	  <td><br></td>
	</tr>
//-->
		     <tr>
			    <td class="tabmovtex" nowrap> &nbsp; Cuenta: &nbsp; </td>
                <!--td class="tabmovtex" rowspan="8" nowrap valign="top" align="left"><img src="/gifs/EnlaceMig/gau25010.gif" width="10" height="50"></td-->
			    <td class="tabmovtex" nowrap>
				  <input type="text" name=textdestino  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value=
				   <%
				      if(request.getAttribute("textdestino")!= null)
                         out.print(request.getAttribute("textdestino"));
					  else
					     out.println("");
				   %>
				  >
					<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
					<input type="hidden" name="cuenta" value=
					 <%
				      if(request.getAttribute("destino")!= null)
                      	  out.print(request.getAttribute("destino"));
					  else
					      out.println("");
				   %>
				   >
                </td>

                <td class="tabmovtex" nowrap align=right >Referencia Mancomunidad &nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td class="tabmovtex" nowrap>&nbsp;&nbsp;	&nbsp;&nbsp;
						<INPUT TYPE="text" class="texdrocon" maxlength=16 size=16 NAME="refmancomunidad">
				</td>

              </tr>

		<input type="hidden" name="param1" value="">

</table>
</form>
</body>
</html>

