<%-- 
  - Autor: FSW Everis
  - Aplicacion: Enlace
  - Módulo: SPID
  - Fecha: 01/06/2016
  - Descripcion: Pantalla de inicio de la baja de una fiel.
  --%>
<%@include file="/jsp/fielCabecera.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Expires" content="1" />
    <meta http-equiv="pragma" content="no-cache" />
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
    style="background-color: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
            '/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif',
            '/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
            '/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif',
            '/gifs/EnlaceMig/gbo25011.gif');validaError();consultaCtas();">
    
    <fmt:setLocale value="mx"/>
    <fmt:setBundle basename="mx.altec.enlace.utilerias.FielStringsBundle" var="lang"/>
    <table border="0" cellpadding="0" cellspacing="0" width="571">
        <tr valign="top">
            <td width="*">
                ${MenuPrincipal}
            </td>
        </tr>
    </table>
	${Encabezado}
    <table border="0" cellpadding="0" cellspacing="0" width="760">
        <tr>
            <td align="center">
    <form name="transferencia" method="POST" action="FielBaja?modulo=1">
        <table style="border: 0; text-align: center;" cellpadding="5"
               cellspacing="3" class="textabdatcla" width="500">
            <tr>
                <td class="tittabdat" align="left">
                    <fmt:message key="grl.fiel.msg1" bundle="${lang}"/>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <%@ include file="/jsp/fielFiltroCtas.jspf" %>
                </td>
            </tr>
            <tr>
                <td class="tabmovtex" valign="top" colspan="4" width="673">
                    <fmt:message key="grl.fiel.msg2" bundle="${lang}"/>
                </td>
            </tr>

            <tr>
                <td class="tabmovtex" style="background-color: white;" colspan="4"></td>
            </tr>
            <tr>
                <td class="textabdatcla" colspan="4" width="100%">
                    <table class="textabdatcla" cellspacing="3" cellpadding="2">
                        <tr>
                            <td class="tabmovtex" colspan="2" width="20%" align="left">
                                <fmt:message key="grl.fiel.cuenta" bundle="${lang}"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="80%" class="tabmovtex">
                                <select id="comboCuenta" name="comboCuenta" style="width: 300px;" class="comboCuenta" disabled>
                                    <option value=""><fmt:message key="grl.fiel.sel.cta" bundle="${lang}"/></option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <table style="margin: 0 auto;" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <a href="javascript:validar('B');" style="border-style: none;">
                        <img src="/gifs/EnlaceMig/gbo25222.gif" style="border-style: none;" />
                    </a>
                </td>
            </tr>
        </table>
    </form>
            </td>
        </tr>
    </table>
</body>
</html>
