<!--
/*************************************************************************
 * Fecha de �ltima modificaci�n : 22 - Dic - 05
 * Proyecto:200525200 Recaudaci�n de Impuestos Federales V4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: Septiembre 2005
 * Modificaci�n: Adaptaciones correspondientes a las
 * especificaciones t�cnicas del SAT versi�n 4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: 22 - Dic - 05
 * Modificaci�n: Se elimin� el c�digo muerto
 ************************************************************************/
-->

<%@ page import="java.util.*,java.io.*"%>
<%@page import="mx.altec.enlace.utilerias.Global"%>


<html><%
mx.altec.enlace.bo.BaseResource baseResource = (mx.altec.enlace.bo.BaseResource)session.getAttribute("session");
%>
<jsp:useBean id="encabezado" scope="session" class="java.lang.String" />

<head>
	<title> Comprobante de Pago de Impuestos</title>


<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">
<%= baseResource.getFuncionesDeMenu() %>
 function WindowCalendar(){
  if(!document.FrmMov.deldia[0].checked){
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMov.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
  }else{
    alert("Aviso\nLa fecha no se puede cambiar para consultar los Movimientos del dia");

  }

}


function save(){
	document.location='Descargar.jsp';
}


function WindowCalendar1(){
  if(!document.FrmMov.deldia[0].checked){
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMov1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
  }else{
    alert("Aviso\nLa fecha no se puede cambiar para consultar los Movimientos del dia");
  }
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" >
<%= baseResource.getStrMenu() %>
<%= encabezado %>

	<%


	String menuprincipal	= "";
//	String encabezado		= "";
	String referencia		= "";
	String pdfname			= "";

//////////////////////////Modificacion para evitar la duplicidad del n�emro de Referencia
	//if(request.getAttribute("MenuPrincipal") !=null)						// mod 30-01-03
	//	menuprincipal = (String) request.getAttribute("MenuPrincipal");		// mod 30-01-03
	//if(request.getAttribute("Encabezado") !=null)							// mod 30-01-03
	//	encabezado =(String) request.getAttribute("Encabezado");			// mod 30-01-03
	/*if(session.getAttribute("referencia") !=null)							// mod 30-01-03
		referencia = (String) session.getAttribute("referencia");
	else if(session.getAttribute("referencia") ==null)// mod 30-01-03
		referencia = "sin numero de referencia";
	if(session.getAttribute("sessNamePDF") !=null)							// mod 30-01-03
		pdfname = (String)session.getAttribute("sessNamePDF");				// mod 30-01-03*/

	if(request.getAttribute("referencia") !=null)
		referencia = (String) request.getAttribute("referencia");
	if(request.getAttribute("sessNamePDF") !=null)							// mod 30-01-03
		pdfname = (String)request.getAttribute("sessNamePDF");
	System.out.println("la referencia es ---> "+referencia);
	System.out.println("el nombre del PDF es ---> "+pdfname);

// vswf ini
FileWriter fw = null;
BufferedWriter bw = null;
PrintWriter pw = null;
File f = null;

String nombreArchivo="";
nombreArchivo = pdfname.substring(0,pdfname.indexOf("."));
nombreArchivo+=".doc";

String rutaArchivo="/" + Global.DIRECTORIO_REMOTO;
String filePath = rutaArchivo + "//" + nombreArchivo;


f = new File(filePath);
if (f.exists() || f.isFile() || (f==null)) {
	f.delete();
}

f.createNewFile();
System.out.println("Archivo xml: " + f.getPath());

fw = new FileWriter(f);
bw = new BufferedWriter(fw);
pw = new PrintWriter(bw);
String xml=(String)session.getAttribute("sessXML");

pw.println(xml);

pw.close();

// vswf fin

	%>


<form name="form1" method="post" action="">
  <table width="545" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
      <!-- Sustitucion unico 1-dic-04
	      <td rowspan="2" valign="middle" class="titpag" align="right" width="35"><img src="/gifs/EnlaceMig/glo25040.gif" width="240" height="67" alt="Grupo Financiero Santander Mexicano"></td> -->
      <td rowspan="2" valign="middle" class="titpag" align="right" width="35"><img src="/gifs/EnlaceMig/glo25040b.gif" width="240" height="67" alt="Grupo Financiero Santander"></td>
    </tr>
    <tr>
      <td valign="top" class="titenccom">Comprobante de recepci&oacute;n de declaraci&oacute;n
        electr&oacute;nica </td>
    </tr>
  </table>
  <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="540" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">

          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau25010.gif" width="21" height="2" alt=".." name="..">
			</td>
			<td valign="middle" align="left" width="530" height="100" class="tittabcom">
				Transacci&oacute;n exitosa. Referencia No. <b><%=referencia%></b>. Oprima el bot&oacute;n
				 <b>[Comprobante]</b>   para obtener su recibo de pago. Le recomendamos
				imprimirlo y guardarlo en archivo, ya que &eacute;ste contiene el sello digital
				que garantiza la autenticidad de su pago.
            </td>
			<td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau25010.gif" width="21" height="2" alt=".." name="..">
			</td>

          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>

  <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br>
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
		    <TD>
			  <!-- Modificacion ARV Incidencia IM167343 //-->
			  <!--<A HREF="/Download/<%=pdfname%>"><img src="/gifs/EnlaceMig/gbo25210.gif"  border="0" alt="Comprobante"></A>//-->
				<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->			  
			  <a href="javascript:document.location='/Enlace/<%=mx.altec.enlace.utilerias.Global.WEB_APPLICATION%>/Consult?operacion=descarga&nombreArchivo=<%=pdfname%>';"><img src="/gifs/EnlaceMig/gbo25210.gif"  border="0" alt="Comprobante"></a>
		    </TD>
			<td align=left>
				<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->	
					<a href="javascript:document.location='/Enlace/<%=mx.altec.enlace.utilerias.Global.WEB_APPLICATION%>/csaldo1?prog=0';">
							<!--a href='/cgi-bin/gx.cgi/AppLogic+EnlaceInternet.csaldo1'-->
						<img src="/gifs/EnlaceMig/gbo25320.gif" border="0" alt="Regresar" name="regresar"></a>
			</td>

			<td>
				<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->	
				 <a href="javascript:document.location='/Enlace/<%=mx.altec.enlace.utilerias.Global.WEB_APPLICATION%>/Consult?operacion=descarga&nombreArchivo=<%=nombreArchivo%>';"><img src="/gifs/EnlaceMig/gboxmlias.gif"  border="0" alt="Comprobante"></a>
			</td>
            <!-- td align="right" width="83">
             <a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0" alt="Imprimir"></a>
            </td -->
          </tr>
        </table>
      </td>
    </tr>
  </table>

  </form>
</body>
</html>