<%-- 
  - Autor: FSW Everis
  - Aplicacion: Enlace
  - M�dulo: SPID
  - Fecha: 01/06/2016
  - Descripcion: Pantalla de confirmacion de fiel para las operaciones registro, modificacion y baja.
  --%>
<%@include file="/jsp/fielCabecera.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
    <head>
   </head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
        onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif',
                '/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
                '/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif',
                '/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
                '/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" >
    
    <fmt:setLocale value="mx"/>
    <fmt:setBundle basename="mx.altec.enlace.utilerias.FielStringsBundle" var="lang"/>
    <table border="0" cellpadding="0" cellspacing="0" width="571">
        <tr valign="top">
            <td width="*">
                <%-- MENU PRINCIPAL --%>
                ${MenuPrincipal}
            </td>
        </tr>
    </table>
    ${Encabezado}
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"/></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"/></td>
  </tr>
</table>

<form name="regFiel" method="POST" action="FielRegistro?modulo=2">
	<input id="enlacetk" name="enlacetk" type="hidden" value="${sessionScope.session.csrfToken}" />
    <table width="760" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center">
            <c:if test="${not empty datosFiel}">
                <table width="640" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
                    <tr>
                        <td class="tittabdat"><fmt:message key="fiel.comprobante.datosTitular" bundle="${lang}"/></td>
                        <td class="tittabdat"></td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.codpos" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.codPostal}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.curp" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.curp}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.direcc" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.direccion}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.email" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.email}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.estado" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.estado}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.localid" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.localidad}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.nomorganiz" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.nombreOrganizacion}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.nomtitul" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.nombreTitular}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.numpasaporteife" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.numPasaporteIfe}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.rfc" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.rfc}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.pais" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.datosTitular.pais}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.edocertif" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.estadoCertificado}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.fechaemision" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.fchEmision}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.fechaexpi" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.fchExpiracion}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.msj" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.mensaje}</td>
                    </tr>
                    <tr>
                        <td class="textabdatobs"><fmt:message key="fiel.comprobante.numserie" bundle="${lang}"/></td>
                        <td class="textabdatobs">${detalleFiel.detalleConsulta.numSerie}</td>
                    </tr>
                </table>
            </c:if>
            </td>
        </tr>
     </table>
</form>
<br/>
<table width="760" cellSpacing="0" cellPadding="0"> 
    <tr>
        <td colSpan="2" align="center">
            <%@ include file="/jsp/ValidaOTPConfirmUnificado.jsp" %>
        </td>
    </tr>
</table>

</body>
</html>
