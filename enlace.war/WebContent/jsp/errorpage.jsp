<%-- Indicate that this is an error page using the page tag --%>
<html>
<body>
   <h1>
      Error Page
   </h1>
   <hr>
   <h2>
      Received the exception:<br>
      <font color=red>
	     <%= exception.toString() %>
	  </font>
   </h2>
</body>
</html>