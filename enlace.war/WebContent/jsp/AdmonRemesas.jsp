<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@ page import="mx.altec.enlace.bita.BitaConstants"%>

<html>
<head>
<title>Consulta de Remesas</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>

<!-- Estilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
.componentesHtml {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
}
</style>
<script language="javascript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

var botonSel;

var mensaje="<%= request.getAttribute("mensaje") %>";

 if(mensaje.length>0 && mensaje!="null")
 {
 cuadroDialogo(mensaje, 1);
 }

function validar()
{
	var f=document.administraRemesa;
	var contador=0;
	for (i=0;i<f.elements.length;i++){
		if (f.elements[i].type=="radio"){
			if (f.elements[i].checked==true){
				contador=1;
			}
		}
	}
	if (contador==1){
		return true;
	}else{
		cuadroDialogo("Usted no ha seleccionado una remesa",1);
		return false;
	}
	return true;
}
function exportar() {
	var f=document.administraRemesa;
	f.operacion.value="4";
	f.submit();
}
function aceptar(){
	if(validar()){
		botonSel=1;
		cuadroDialogo("&#191;Desea recibir la remesa?",2);
	}
}
function cancelar(){
	if(validar()){
		botonSel=2;
		cuadroDialogo("&#191;Desea cancelar la remesa?",2);
	}
}

function siguiente()
{
		document.administraRemesa.esSiguiente.value="S";
		document.administraRemesa.operacion.value="1";
		document.administraRemesa.submit();

}
function continua()
{
	if(respuesta==1) {
		if (botonSel==1){
			document.administraRemesa.operacion.value="3";
			document.administraRemesa.estado.value="R";
			document.administraRemesa.submit();
		} else if (botonSel==2) {
			document.administraRemesa.operacion.value="3";
			document.administraRemesa.estado.value="D";
			document.administraRemesa.submit();
		}
	}
}
function regresar(){
	document.administraRemesa.operacion.value="1";
	document.administraRemesa.submit();
}

function imprimir() {
	scrImpresion();
}
<%= request.getAttribute("newMenu")%>
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	bgcolor="#ffffff"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
	background="/gifs/EnlaceMig/gfo25010.gif">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*"><%= request.getAttribute("MenuPrincipal")%></td>
	</tr>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>
<form action="${ctx}AdmonRemesasServlet" method=post name="administraRemesa">
	<input type="hidden" name="esSiguiente">
	<input type="hidden" name="operacion" value="1">
	<input type="hidden" name="estado" value="R">
	<%
		LMXC lmxc = (LMXC) request.getAttribute("RemesasBean");
		if (lmxc != null && lmxc.getListaRemesas() != null && lmxc.getListaRemesas().size() > 0) {
		int numRemesas = lmxc.getListaRemesas().size();
	%>
		<table cellspacing="0" cellpadding="0" border="0" width="760">
			<tr>
				<td align="center" class="tabmovtex" >
					Total de remesas: <%=numRemesas%>
						<input type="hidden" name="numRegistros"
		value="<%= lmxc.getNumRegistros()%>">
		<input type="hidden" name="numRegistrosAnt"
		value="<%= lmxc.getNumRegistrosAnt()%>">
				</td>
			</tr>
			<tr>
				<td align="center">
				<DIV STYLE="overflow: auto; height: 250; widht:400;
            border-left: 1px gray solid; border-bottom: 1px gray solid;
            padding:0px; margin: 0px">
				<table width="500" border="0" cellspacing="1" cellpadding="2" class="tabfonbla">
					<tr>
						<td align="center" width="50" class="tittabdat">
						</td>
						<td align="center" width="275" class="tittabdat">N&uacute;mero Remesa
						</td>
						<td align="center" width="275" class="tittabdat">Estatus
						</td>
					</tr>
					<%
					int tamano = lmxc.getListaRemesas().size();

					Iterator iterator = lmxc.getListaRemesas().iterator();

					boolean claro = false;
					int i=0;
					while (iterator.hasNext()) {

						RemesasBean remesa = (RemesasBean) iterator.next();
						String sClass = claro ? "textabdatcla" : "textabdatobs";
					%>
					<tr>
						<td align="center" width="60" class="<%=sClass%>">
						<% if(lmxc.isExistenMasRegistros())
						{ %>
						 <input type="hidden" name="EntidadPag" value="<%= lmxc.getEntidad()%>">
						 <input type="hidden" name="ContratoEnlacePag" value="<%= lmxc.getContratoEnlace()%>">
						 <input type="hidden" name="CtroDistribucionPag" value="<%= lmxc.getCtroDistribucion()%>">
						 <input type="hidden" name="NumeroRemesaPag" value="<%= lmxc.getNumeroRemesa()%>">
						 <input type="hidden" name="EstadoRemesaPag" value="<%= lmxc.getEstadoRemesa()%>">
						 <input type="hidden" name="FechaInicioPag" value="<%= lmxc.getFechaInicio()%>">
						 <input type="hidden" name="FechaFinalPag" value="<%= lmxc.getFechaFinal()%>">
						 <input type="hidden" name="CtoEnlacePag" value="<%= lmxc.getCtoEnlacePag()%>">
						 <%} %>

					<% if(remesa.getEstadoRemesa().equals("ENVIADA")){	%>
						<input type="radio" name="folioRemesa" value="<%= remesa.getNumRemesa()%>|<%= remesa.getCentroDistribucion()%>">
					 	<input type="hidden" name="numCtroDist" value="<%= remesa.getCentroDistribucion()%>">
					<%}%>
						</td>
						<td class="<%=sClass%>"> <%= remesa.getNumRemesa() %></td>
						<td class="<%=sClass%>"><%= remesa.getEstadoRemesa() %></td>
					</tr>
					<%
						claro = !claro;
						i++;
					}
					%>

				</table>
				</DIV>
				</td>
				</tr>
				<tr>
				<td align="center" class="tabmovtex"><%=lmxc.getNumRegistrosAnt() %> - <%=lmxc.getNumRegistros() %>
		<% if(lmxc.isExistenMasRegistros())
					{ %> <a href="javascript:siguiente();">Siguientes registros ></a>&nbsp;
		<%} %>
		</td>
			</tr>
			<tr>
			<td>
				<table height="40" cellspacing="0" cellpadding="0" border="0" width="760">
					<tr><td></td></tr>
					<tr>
						<td align="center" class="texfootpagneg">
							<table height="40" cellspacing="0" cellpadding="0" border="0" width="168">
								<tr>
									<td>
						 		 		<a href = "javascript:aceptar();"><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>
                        			</td>
									<td>
										<a href="javascript:cancelar();"><img src="/gifs/EnlaceMig/gbo25190.gif" border="0" alt="Cancelar" width="80" height="22"> </a>
									</td>
									<td>
										<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="80" height="22"></a>
									</td>
									<td>
										<a href="javascript:exportar();"><img src="/gifs/EnlaceMig/gbo25230.gif" border=0 alt="Exportar" width="80" height="22"/></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</td>
		</tr>
	</table>
	<%
			}else{
		%>
<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">Consulta de remesas</td>
				</tr>
				<tr>
					<td align="center" class="textabdatobs">
						<br/>El contrato no tiene remesas asociadas<br/><br/>
					</td>
				</tr>
			</table>
			<br/>
			<A href = "javascript:history.back();" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77" height="22"></a>
    	</td>
	</tr>
</table>
<%	} %>
</form>
</body>
</html>