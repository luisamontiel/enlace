<html>
 <head>
  <title> Fondos de Inversi&oacute;n</title>
  <!--
    Elaborado:  Francisco Serrato Jimenez (fsj)
    Aplicativo: Enlace Internet
    Proyecto:   MX-2002-257
                Getronics CP Mexico
    Archivo:   InverSoc1.jsp
  -->
  <script language="JavaScript" src="/EnlaceMig/fw_menu.js"></script>
  <script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script language="JavaScript">
   <!--
    function ValidaForma(forma)
    {
        if( forma.importe.value=="" && forma.titulos.value=="" )
        {
            forma.importe.focus();
            cuadroDialogo("   Debe capturar Importe y/o T&iacute;tulos para efectuar la operaci&oacute;n.   ",3);
            return;
        }
        if( !ValidarFloat(forma.importe.value) )
        {
            forma.importe.value = "";
            forma.importe.focus();
            cuadroDialogo("   Debe ingresar un valor Num&eacute;rico para el campo Importe.   ",3);
            return;
        }
        if( !ValidarEntero(forma.titulos.value) )
        {
            forma.titulos.value = "";
            forma.titulos.focus();
            cuadroDialogo("   Debe ingresar un valor Entero para el campo de T&iacute;tulos.   ",3);
            return;
        }
        forma.submit();
    }
    function ValidaCampo(campo)
    {
        if( campo==1 )  //Valida campo importe
            document.frmInverSoc2.titulos.value = "";
        else            //Valida campo t�tulo
            document.frmInverSoc2.importe.value = "";
    }
<%
    if( request.getAttribute("newMenu")!= null )
        out.println(request.getAttribute("newMenu"));
%>
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    {   //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    function ValidarEntero(Entero)
    {
        if( Entero!="" )
        {
            var formato=/^[0-9]{1,}$/;
            if( !formato.test(Entero) )
                return false;
        }//fin if
        return true;
    }
    function ValidarFloat(Float)
    {
        if( Float.indexOf(".")==-1 )
            return ValidarEntero(Float);
        if( Float!="" )
        {
            var formato=/^[0-9]{1,}.[0-9]{1,}$/;
            if( !formato.test(Float) )
                return false;
        }//fin if
        return true;
    }
    function cargarEmisoras(tipoOper)
    {
        if( tipoOper=="V" ){ document.frmInverSoc2.opcCompVent[0].check = false; document.frmInverSoc2.opcCompVent[1].check = true;  }
        else               { document.frmInverSoc2.opcCompVent[0].check = true;  document.frmInverSoc2.opcCompVent[1].check = false; }
        document.frmInverSoc2.ventana.value = 1;
        document.frmInverSoc2.submit();
    }
    function Regresar()
    {
        document.frmInverSoc2.ventana.value = 0;
        document.frmInverSoc2.submit();
    }
   //-->
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
  <table width="571" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td>
<%
        if( request.getAttribute("MenuPrincipal")!=null )
            out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td width="676" valign="top" align="left">
     <table width="666" border="0" cellspacing="6" cellpadding="0">
      <tr>
       <td width="528" valign="top" class="titpag">
<%
        if( request.getAttribute("Encabezado")!=null )
            out.println(request.getAttribute("Encabezado"));
%>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
  <form  name="frmInverSoc2" method="post" onSubmit="return ValidaForma(this);" action="InverSoc">
   <input type="hidden" name="ventana" value="2">
   <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td align="center"><br>
<%
        if( request.getAttribute("emisoras")!=null )
            out.println(request.getAttribute("emisoras"));
%>

     </td>
    </tr>
   </table>
  </form>
 </body>
</html>