<%-- 
  - Autor: FSW Everis
  - Aplicacion: Enlace
  - Módulo: SPID
  - Fecha: 01/06/2016
  - Descripcion: JSP para contener imports, scripts y estilos para el modulo SPID
  --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Expires" content="1" />
		<meta http-equiv="pragma" content="no-cache" />
		<title>${moduloFiel}</title>
		<%@ page import="java.util.Date" %>
		<%@ page import="java.text.SimpleDateFormat" %>
		<%
		SimpleDateFormat formaHora = new SimpleDateFormat("yyyyMMddHHmmss");
		%>		
		<script type="text/javascript" src="/EnlaceMig/validacionesFIEL.js"></script>
		<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
		<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
		<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
		<script type="text/javascript" src="/EnlaceMig/consultaCuentas.js?<%=formaHora.format(new Date())%>" ></script>
		<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
		<script type="text/javascript" src="/EnlaceMig/jquery-1.11.2.min.js" ></script>
		<script type="text/javascript" src="/EnlaceMig/json3.min.js"></script>
		
		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
		
		<script type="text/javascript">
		${newMenu}
		</script>
	
		</head>
	<body>
		<fmt:setLocale value="mx"/>
		<fmt:setBundle basename="mx.altec.enlace.utilerias.FielStringsBundle" var="lang"/>
		<form action="">
			<input type="hidden" id="msgError" value="${msgError}" />
		</form>
	</body>
</html>