<html><%-- #BeginTemplate "/Templates/principal.dwt" --%>
<head>
<%-- #BeginEditable "doctitle" --%>
<title>Enlace</title>
<%-- #EndEditable --%>

<%-- #BeginEditable "MetaTags" --%>
<meta http-equiv="Content-Type" content="text/html;" />
<meta name="Codigo de Pantalla" content="s25290" />
<meta name="Proyecto" content="Portal" />
<meta name="Version" content="1.0" />
<meta http-equiv="pragma" content="no-cache" />
<%-- #EndEditable --%>

<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>


<Script type="text/javascript" >
<!--
<%= request.getAttribute("newMenu") %>
<%
  String diasInhabiles="";

  if (request.getAttribute("diasInhabiles")!= null)
     diasInhabiles=(String)request.getAttribute("diasInhabiles");
%>


var js_diasInhabiles="<%=diasInhabiles%>";
var diasInhabiles = "<%=diasInhabiles%>";
var Nmuestra = 30;
var defaultEmptyOK = false;

function echeck(str)
{
  var at="@";
  var dot=".";
  var lat=str.indexOf(at);
  var lstr=str.length;
  var ldot=str.indexOf(dot);

  if (str.indexOf(at)==-1)
     return false;
  if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
     return false;
  if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
      return false;
  if (str.indexOf(at,(lat+1))!=-1)
      return false;
  if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
      return false;
  if (str.indexOf(dot,(lat+2))==-1)
      return false;
  if (str.indexOf(" ")!=-1)
      return false;
  return true;
}

function validaEmail()
{
  document.MantNomina.CorreoE.value=trimString(document.MantNomina.CorreoE.value);
  var emailID=document.MantNomina.CorreoE;

  if( trimString(emailID.value)!="")
  {
	  if (echeck(emailID.value)==false)
	  {
		emailID.focus();
		cuadroDialogo("El correo especificado no es v&aacute;lido.", 3);
		return false
	  }
  }
  else
  {
      cuadroDialogo( "Es necesario incluir el correo para env&iacute;o de detalle" ,3);
     return false;

  }
  return true
}

function FrmClean(){

  document.MantNomina.textcuenta.value	="";
  document.MantNomina.fecha1.value	= "";
  document.MantNomina.fecha2.value	= "";
  document.MantNomina.Importe.value	= "";
  document.MantNomina.secuencia.value      = "";
  document.MantNomina.ChkRecibido.checked  = "";
  document.MantNomina.ChkCancelado.checked = "";
  document.MantNomina.ChkEnviado.checked   = "";
  document.MantNomina.ChkPagado.checked    = "";
  document.MantNomina.ChkCargado.checked   = "";
 limpiarFiltro();

}
/**
 *Limpia filtro de consulta
 */
function limpiarFiltro(){
	document.getElementById("numeroCuenta0").value ="";
	document.getElementById("descripcionCuenta0").value ="";
	consultaCuentas(0,2,1);
}

function js_descargaArchivo(Nombre)
{
	document.MantNomina.operacion.value="descarga";
	document.MantNomina.action="OpcionesNomina";
	document.MantNomina.submit();
}

function js_enviarCorreo()
{
   var seleccionado;
   var procesado;
   var cad="";
   var i=0;

   cad="";
   procesado=false;
   seleccionado=false;

   for(i=0;i<(document.MantNomina.elements.length);i++)
	 {
	   if(document.MantNomina[i].type=="radio"  && document.MantNomina[i].name=="valor_radio")
		 if(document.MantNomina[i].checked)
		 {
		   seleccionado=true;
		   if(document.MantNomina[i].value.indexOf("Procesado")>0)
		    {
			  if(validaEmail())
				{
			      cad=document.MantNomina[i].value;
				  procesado=true;
				}

			  break;
		    }
		   else
			  cuadroDialogo("Esta opci&oacute;n solo aplica para los archivos Procesados.", 1);
		 }
	 }

	if(seleccionado)
	 {
	   if(procesado)
		 {
		   cad=cad.substring(0,cad.indexOf("|"));
		   document.MantNomina[i].value=cad;
		   document.MantNomina.operacion.value="enviarCorreo";
		   document.MantNomina.action="OpcionesNomina";
		   document.MantNomina.submit();
		 }
	 }
	else
	  cuadroDialogo("Debe seleccionar un registro para acceder a esta opci&#243;n", 1);
}


function js_solicitarPDF()
{
   var seleccionado;
   var procesado;
   var cad="";
   var i=0;

   cad="";
   procesado=false;
   seleccionado=false;

   for(i=0;i<(document.MantNomina.elements.length);i++)
	 {
	   if(document.MantNomina[i].type=="radio"  && document.MantNomina[i].name=="valor_radio")
		 if(document.MantNomina[i].checked)
		 {
		   seleccionado=true;
		   if(document.MantNomina[i].value.indexOf("Procesado")>0)
		    {
			  cad=document.MantNomina[i].value;
			  procesado=true;
			  break;
		 	}
		   else
			  cuadroDialogo("La n&oacute;mina debe estar procesada para solicitar el informe.", 1);
		 }
	 }

	if(seleccionado)
	 {
	   if(procesado)
		 {
		   cad=cad.substring(0,cad.indexOf("|"));
		   document.MantNomina[i].value=cad;
		   document.MantNomina.operacion.value="SolicitaPDF";
		   document.MantNomina.action="OpcionesNomina";
		   document.MantNomina.submit();
		 }
	 }
	else
	  cuadroDialogo("Debe seleccionar un registro para acceder a esta opci&#243;n", 1);
}

function js_descargaPDF(secuencia){

	document.MantNomina.secDescarga.value=secuencia;
	document.MantNomina.operacion.value="descargaPDF";
	document.MantNomina.action="OpcionesNomina";
	document.MantNomina.submit();
}



var respuesta=0;

function continua()
{
  if(respuesta==1)
	{
      cad=cad.substring(0,cad.indexOf("|"));
	  document.MantNomina[elemento].value=cad;
	  document.MantNomina.operacion.value="enviarCorreo";
	  document.MantNomina.action="OpcionesNomina";
	  document.MantNomina.submit();
	}
}

// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento, desc) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			if (desc) {
	   			document.getElementById(elemento).value = obtenerCuentaTexto(valor);
	   		} else {
				document.getElementById(elemento).value = obtenerCuenta(valor);
		 	}
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0];
    }
}

function obtenerCuentaTexto(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0] + " " + res[2];
    }
}

// Marzo 2015
// Enlace PYMES


function js_consultar(){



   var pasa = true;
// Marzo 2015
// Enlace PYMES
    obtenerCuentaSeleccionada("comboCuenta0", "cuenta", false);
	obtenerCuentaSeleccionada("comboCuenta0", "textcuenta", true);

	var select = document.getElementById("comboCuenta0");

   if((document.getElementById("cuenta").value=="" || document.getElementById("textcuenta").value=="")
   && select.selectedIndex > 0)
    {
        if (select.selectedIndex > 0) {
			cuadroDialogo("La cuenta de Cargo es invalida.",3);
		} else {
			cuadroDialogo("La cuenta de Cargo es un dato obligatorio.",3);
		}
		return;
    }
// Marzo 2015
// Enlace PYMES


                campo1 =   document.MantNomina.Importe.value;
		campo3 =   document.MantNomina.secuencia.value;

	if(campo1 != ""  || campo3 != ""){
		if (campo1 != "")
                   if(!checkFloatValueCon(campo1)){
		      pasa = false;
  		   }
		if(campo3 != "" && pasa)
		   if(!checkValueCon3(campo3)){
		     pasa = false;
		     }
	}

	if (pasa){
		document.MantNomina.operacion.value="consulta";
		document.MantNomina.action="OpcionesNomina";
		document.MantNomina.submit();
	}
}

function checkFloatValueCon(myobj){
  if(myobj != ""){
    if(!isFloatCon(myobj)){
      myobj = "";
      document.MantNomina.Importe.focus();
      cuadroDialogo("El Importe debe ser num&eacute;rico",3);
      return false;
    }
  }
  return true;
}

function isFloatCon (s)
{   var i;
    var seenDecimalPoint = false;
    if (isEmptyCon(s))
       if (isFloat.arguments.length == 1) return defaultEmptyOK;
       else return (isFloat.arguments[1] == true);
    if (s == ".") return false;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if ((c == ".") && !seenDecimalPoint) seenDecimalPoint = true;
        else if (!isDigit(c)) return false;
    }
    return true;
}

function isEmptyCon(s){
	return ((s == null) || (s.length == 0))
}

function checkValueCon(myobj){
  if(myobj != ""){
    if(!isPositiveInteger(myobj)){
      myobj = "";
	  cuadroDialogo("El n&uacute;mero de registros debe ser num&eacute;rico",3);
	  return false;
    }
  }
  return true;
}

function checkValueCon3(myobj){
  if(myobj != ""){
    if(!isPositiveInteger(myobj)){
      myobj = "";
      document.MantNomina.secuencia.focus();
	  cuadroDialogo("El n&uacute;mero de secuencia debe ser num&eacute;rico",3);
	  return false;
    }
  }
 return true;
}

function isPositiveInteger (s)
{   var secondArg = defaultEmptyOK;

   if (isPositiveInteger.arguments.length > 1)
        secondArg = isPositiveInteger.arguments[1];
    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s) > 0) ) );
}

function isSignedInteger (s)
{
if (isEmpty(s))
       if (isSignedInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isSignedInteger.arguments[1] == true);
    else {
        var startPos = 0;
        var secondArg = defaultEmptyOK;
        if (isSignedInteger.arguments.length > 1)
            secondArg = isSignedInteger.arguments[1];
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;
        return (isInteger(s.substring(startPos, s.length), secondArg))
    }
}

function isInteger (s)
{   var i;

    if (isEmpty(s))
       if (isInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isInteger.arguments[1] == true);
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (!isDigit(c)) return false;
    }
    return true;
}

function isDigit (c){
return ((c >= "0") && (c <= "9"))
}

function isEmpty(s){
return ((s == null) || (s.length == 0))
}

function js_cancelar()
 {
  var seleccionado=false;
  var cancelado=false;
  var cad="";
  var i=0;

	for(i=0;i<(document.MantNomina.elements.length);i++)
	 {
		if (document.MantNomina[i].type=="radio"  && document.MantNomina[i].name=="valor_radio")
		 if (document.MantNomina[i].checked==true)
		 {
		   seleccionado=true;
		   if (document.MantNomina[i].value.indexOf("Procesado")<0)
			{
			  cancelado=true;
			  cad=document.MantNomina[i].value;
			  break;
			}
		   else
			  cuadroDialogo("Esta opci&oacute;n solo aplica para los archivos Recibidos.", 3);
		 }

	 }

	if (seleccionado)
	 {
	   if(cancelado)
		 {
			cad=cad.substring(0,cad.indexOf("|"));
			document.MantNomina[i].value=cad;
			document.MantNomina.operacion.value="cancelar";
			document.MantNomina.tipoArchivo.value="cancelado";
			document.MantNomina.action="OpcionesNomina";
			document.MantNomina.submit();
		 }
	 }
	else
     cuadroDialogo("Debe seleccionar un registro para acceder a esta opci&#243;n", 1);
}


function js_regresar(){
		document.MantNomina.operacion.value="regresar";
		document.MantNomina.action="OpcionesNomina";
		document.MantNomina.submit();
}


function atras(){

    if((parseInt(document.MantNomina.prev.value) - Nmuestra)>0){
      document.MantNomina.next.value = document.MantNomina.prev.value;
      document.MantNomina.prev.value = parseInt(document.MantNomina.prev.value) - Nmuestra;
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
    }
}

function adelante(){
 if(parseInt(document.MantNomina.next.value) != parseInt(document.MantNomina.total.value)){
    if((parseInt(document.MantNomina.next.value) + Nmuestra)<= parseInt(document.MantNomina.total.value)){
      document.MantNomina.prev.value = document.frmbit.next.value;
      document.MantNomina.next.value = parseInt(document.frmbit.next.value) + Nmuestra;
       document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
     }else if((parseInt(document.MantNomina.next.value) + Nmuestra) > parseInt(document.MantNomina.total.value)){
      document.MantNomina.prev.value = document.MantNomina.next.value;
      document.MantNomina.next.value = parseInt(document.MantNomina.next.value) + (parseInt(document.MantNomina.total.value) - parseInt(document.MantNomina.next.value));
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
     }
 }else{
   cuadroDialogo("No hay m&aacute;s registros.",3);
 }

 }

 function WindowCalendar(){
    var m=new Date()
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calNom22.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}
function SeleccionaFechaApli(ind)
 {
   var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/calNom32.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
   msg.focus();
 }

 function WindowCalendarApliInicio(){
    var m=new Date()
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calNom2.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}
 function SeleccionaFechaApliFin(ind)
 {
   var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/calNom12.html #"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
   msg.focus();
 }

function Actualiza()
 {
     document.MantNomina.fecha2.value=Fecha[Indice];
 }

function WindowCalendar1(){
    var m=new Date()
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calNom12.html #" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}

var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function actualizacuenta()
{
  document.MantNomina.cuenta.value=ctaselec;
  document.MantNomina.textcuenta.value=ctaselec+" "+ctadescr;
}

function EnfSelCta()
{
   document.MantNomina.textcuenta.value="";
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


// Enlace PyMES .. Marzo 2015

function exportarArchivoPlano() {

 	var test = document.getElementsByName("tipoExportacion");
    var sizes = test.length;
    var extencion;
    for (i=0; i < sizes; i++) {
            if (test[i].checked==true) {
        	extencion = test[i].value;
        }
    }
	window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=ARCHIVO&em=ExportModel&af=af&tipoArchivo=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
}

 // Enlace  PyMES .. Marzo 2015

 <%
	/*FSW Vector - ventana de descarga PDF*/

	if(request.getAttribute("URLDescargaPDF")!= null) {
	out.println( "msg=window.open(\""+ request.getAttribute("URLDescargaPDF") +"\",\"\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=400,height=200\");"+
			"msg.focus();");
	}

  %>

//-->
</script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
 onLoad='consultaCuentas(0,2,1)'; '<%= request.getAttribute("ArchivoErr")%>'; 'MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')' >

<!-- #BeginLibraryItem "/Library/navegador.lbi" -->
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr valign=top>
    <td width="*">
		<%-- MENU PRINCIPAL --%>
        <%= request.getAttribute("MenuPrincipal") %>
    </td>
  </tr>
</table>
<%-- #EndLibraryItem --%><%-- #BeginEditable "Contenido" --%>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag"><%= request.getAttribute("Encabezado") %></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<%-- #BeginLibraryItem "/Library/contrato.lbi" --%>

<form name = "Frmfechas" action="">
<%
  if (request.getAttribute("Bitfechas")!= null)
    out.println(request.getAttribute("Bitfechas")); %></td>
</form>

<FORM  NAME="MantNomina" METHOD="Post" ACTION="MtoNomina">

   <table width="760" border="0" cellspacing="0" cellpadding="0">
   <%
	if (request.getAttribute("ContenidoArchivo")== "")
	{
   %>
    <tr>
	 <td align="center">
	    <table border="0" cellspacing="2" cellpadding="3">
	     <%--<table width="670" border="0" cellspacing="2" cellpadding="3" bgcolor="#FFFFFF"> --%>
		 <tr>
		   <td class="tittabdat" colspan="2" nowrap> Seleccione los datos:</td>
		 </tr>
		 <%--
// Marzo 2015
// Enlace PYMES --%>
		 <tr valign="top" align="center" >
    					<td colspan="2">
		          			<%@ include file="/jsp/filtroConsultaCuentasNominaBoton.jspf" %>
		          		</td>
    				</tr>
    	<%--
// Marzo 2015
// Enlace PYMES --%>
		 <tr align="center">
		   <td class="textabdatcla" valign="top" colspan="2">
			<table width="660" border="0" cellspacing="0" cellpadding="0">
			 <tr valign="top">
			  <td width="420" align="left">

               <table width="420" border="0" cellspacing="0" cellpadding="5">
			    <tr>
				 <td width="150" align="right" class="tabmovtex">Cuenta Cargo:<br></br></td>
				 <td class="tabmovtex" nowrap >
				<%--

				 &nbsp;
				 <a href="javascript:PresentarCuentas();">
				 <img src="/gifs/EnlaceMig/gbo25420.gif" border=0 style="vertical-align:middle;">
				 </a>--%>
				 <%--
// Marzo 2015
// Enlace PYMES --%>
				<input type="hidden" name=textcuenta id=textcuenta value=""/>

				 <input type="hidden" name="cuenta" id="cuenta" value=""/>

					<%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
					<%--
// Marzo 2015
// Enlace PYMES --%>
				 </td>
				</tr>
				<tr>
                 <td width="150" align="right" class="tabmovtex">Por fecha de envio:<br></br></td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha1" size="12" class="tabmovtex" OnFocus = "blur();" /><a href="javascript:WindowCalendar();"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" style=" border:0px;" style="vertical-align:middle;" /></a></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Por fecha de aplicaci&oacute;n:</td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha2" size="12" class="tabmovtex" OnFocus = "blur();" /><a href="javascript:SeleccionaFechaApli(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" style=" border:0px;" style="vertical-align:middle;" /></a></td>
				</tr>
		        <%-- <tr>
				 <td align="right" class="tabmovtex">Fecha aplicaci&oacute;n inicial:<br></td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fechaInicioApli" size="12" class="tabmovtex" OnFocus = "blur();"><a href="javascript:WindowCalendarApliInicio();"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Fecha aplicaci&oacute;n final:</td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fechaFinalApli" size="12" class="tabmovtex" OnFocus = "blur();"><a href="javascript:SeleccionaFechaApliFin(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a></td>
				</tr> --%>
				<tr>
				 <td align="right" class="tabmovtex">Importe:</td>
				 <td class="tabmovtex" nowrap><input type="text" name="Importe" size="15" maxlength=15 class="tabmovtex" /></td>
				</tr>
				<%--<tr>
				 <td align="right" class="tabmovtex">Numero de registros:</td>
				 <td class="tabmovtex" nowrap><input type="text" name="Nregistros" size="15" maxlength=12 class="tabmovtex"></td>
				</tr>--%>
				<tr>
				 <td align="right" class="tabmovtex">Secuencia:</td>
				 <td class="tabmovtex" nowrap><input type="text" name="secuencia" size="15" maxlength=12 class="tabmovtex" /></td>
				</tr>
			   </table>

              </td>
			  <td width="240" align="left">

			   <table width="240" border="0" cellspacing="0" cellpadding="3">
			    <tr>
				 <td align="right" class="tabmovtex" width="107" nowrap>Estatus:</td>
				 <td class="tabmovtex" width="15" valign="middle" align="center"><input type="checkbox" name="ChkRecibido" value="R" /></td>
				 <td class="tabmovtex" width="100" nowrap>Recibidas</td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="ChkCancelado" value="C" /></td>
				 <td class="tabmovtex" nowrap>Canceladas </td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="ChkEnviado" value="E" /></td>
				 <td class="tabmovtex" nowrap>En proceso</td>
				</tr>
			    <tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="ChkPagado" value="P" /></td>
				 <td class="tabmovtex" nowrap>Procesadas</td>
				</tr>
			    <tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="ChkCargado" value="A" /></td>
				 <td class="tabmovtex" nowrap>Cargados no Pagados</td>
				</tr>
			   </table>

			  </td>
			 </tr>
			</table>
		   </td>
          </tr>
        </table>

        <br>
        <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="height:22">
         <tr>
          <td align="right" valign="top" height="22" width="90"><a href="javascript:js_consultar();"><img style=" border:0px;" name="Enviar" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar" /></a></td>
		  <td align="left" valign="middle" height="22" width="76"><a href="javascript:FrmClean();"><img name="limpiar" style=" border:0px;" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar" /></a></td>
		  <td valign="top" width="76"><a href="consultaServlet?entrada=1"><img src="/gifs/EnlaceMig/consulta_transferencias.gif" style=" border:0px;" alt="Enviar Consulta" /></a></td>
		 </tr>
        </table>

	 </td>
    </tr>

   <%}%>

        <%
			if (request.getAttribute("ContenidoArchivo")!= null)
					out.println(request.getAttribute("ContenidoArchivo"));
		%>

		   <%
			if(request.getAttribute("ContenidoArchivo")!= "")
			{
		   %>

		   <table align=center>
			<tr>
			 <td class="tabmovtex"  align=center nowrap>
			   <br>Correo: <input type=text name=CorreoE size=25 /><br></br>
			 </td>
			</tr>
		   </table>

		   <%}%>
		<%

		if (request.getAttribute("botones")!= null)
		 {
			out.println(request.getAttribute("botones"));
		 }
		%>

  </table>

  <INPUT TYPE="hidden" VALUE="0" NAME="operacion" />
  <input type="hidden" name="secDescarga" value="" />
  <INPUT TYPE="hidden" VALUE="-1" NAME="registro" />
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("fechaHoy") !=null)
									out.println( session.getAttribute("fechaHoy"));
								else
									if (request.getAttribute("fechaHoy")!= null)
										out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy" />
  <INPUT TYPE="hidden" NAME="nuevoArchivo" />
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("tipoArchivo") !=null)
									out.println( session.getAttribute("tipoArchivo"));
								else
									if (request.getAttribute("tipoArchivo")!= null)
										out.println(request.getAttribute("tipoArchivo"));
							%>"  NAME="tipoArchivo" />
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("horaSistema") !=null)
									out.println( session.getAttribute("horaSistema"));
								else
									if (request.getAttribute("horaSistema")!= null)
										out.println(request.getAttribute("horaSistema"));
							%>" NAME="horaSistema" />

  <input type="hidden" name="archivoEstatus" value="<%
								if(	session.getAttribute("archivoEstatus") !=null)
									out.println( session.getAttribute("archivoEstatus"));
								else
									if (request.getAttribute("archivoEstatus")!= null)
										out.println(request.getAttribute("archivoEstatus"));
							%>" />

  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("cantidadDeRegistrosEnArchivo")!= null)
								    out.println(request.getAttribute("cantidadDeRegistrosEnArchivo"));
								else
								if(session.getAttribute("cantidadDeRegistrosEnArchivo") !=null)
									out.println( session.getAttribute("cantidadDeRegistrosEnArchivo"));


							%>" NAME="cantidadDeRegistrosEnArchivo" />
  <INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("nombre_arch_salida")!= null)
				    out.println(request.getAttribute("nombre_arch_salida"));
			    %>" NAME="nombre_arch_salida" />
  <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "lista_nombres_archivos" ) != null ) { out.print( request.getAttribute( "lista_nombres_archivos" ) ); } %>" NAME="lista_nombres_archivos" />
  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("fechaHoy")!= null)
								out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy" />

<%
	if(	session.getAttribute("facultadImportar") !=null)
		out.println( session.getAttribute("facultadImportar"));
	else
	if (request.getAttribute("facultadImportar")!= null) {
		out.println(request.getAttribute("facultadImportar"));
	}
%>
<%
	if(	session.getAttribute("facultadAlta") !=null)
		out.println( session.getAttribute("facultadAlta"));
	else
	if (request.getAttribute("facultadAlta")!= null) {
		out.println(request.getAttribute("facultadAlta"));
	}
%>
<%
	if(	session.getAttribute("facultadCambios") !=null)
		out.println( session.getAttribute("facultadCambios"));
	else
	if (request.getAttribute("facultadCambios")!= null) {
		out.println(request.getAttribute("facultadCambios"));
	}
%>
<%
	if(	session.getAttribute("facultadBajas") !=null)
		out.println( session.getAttribute("facultadBajas"));
	else
	if (request.getAttribute("facultadBajas")!= null) {
		out.println(request.getAttribute("facultadBajas"));
	}
%>
<%
	if(	session.getAttribute("facultadEnvio") !=null)
		out.println( session.getAttribute("facultadEnvio"));
	else
	if (request.getAttribute("facultadEnvio")!= null) {
		out.println(request.getAttribute("facultadEnvio"));
	}
%>
<%
	if(	session.getAttribute("facultadRecuperar") !=null)
		out.println( session.getAttribute("facultadRecuperar"));
	else
	if (request.getAttribute("facultadRecuperar")!= null) {
		out.println(request.getAttribute("facultadRecuperar"));
	}
%>
<%
	if(	session.getAttribute("facultadExportar") !=null)
		out.println( session.getAttribute("facultadExportar"));
	else
	if (request.getAttribute("facultadExportar")!= null) {
		out.println(request.getAttribute("facultadExportar"));
	}
%>
<%
	if(	session.getAttribute("facultadBorra") !=null)
		out.println( session.getAttribute("facultadBorra"));
	else
	if (request.getAttribute("facultadBorra")!= null) {
		out.println(request.getAttribute("facultadBorra"));
	}
%>
</form>
<%-- #EndEditable --%>
</body>
<%-- #EndTemplate --%></html>
<script type="text/javascript">
<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
	if (request.getAttribute("infoUser")!= null) {
		out.println(request.getAttribute("infoUser"));
	}

	if(	session.getAttribute("infoImportados") !=null)
		out.println( session.getAttribute("infoImportados"));
	else
	if (request.getAttribute("infoImportados")!= null) {
		out.println(request.getAttribute("infoImportados"));
	}
%>
</script>