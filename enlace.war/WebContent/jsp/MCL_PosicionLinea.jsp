<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<%--27/08/2002: Se inhabilit� la edici�n del campo de la cuenta.
01/10/2002 Se verific� ortograf�a.
--%>
<%!

	// --- formatea los n�meros para imprimirse como formato moneda --------------------
	String formatea(String num)
		{
		String str = "" + (long)(Double.parseDouble(num) * 100);
		while(str.length()<3) str = "0" + str;
		str = str.substring(0,str.length()-2) + "." + str.substring(str.length()-2);
		for(int a=str.length()-6;a>0;a-=3) str = str.substring(0,a) + "," + str.substring(a);
		str = "$" + str;
		return str;
		}

%>

<html>
<head>
	<title>Consulta de Posici&oacute;n de L&iacute;nea de Cr&eacute;dito</TITLE>

<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" type="text/javascript">

function textToTrim()
{
	forma=document.PosicionLinea;
	forma2=document.form1;

	for(i=0;i<forma.length;i++)
	  if(forma.elements[i].type=="text")
		forma.elements[i].value=trimString(forma.elements[i].value);

	for(i=0;i<forma2.length;i++)
	  if(forma2.elements[i].type=="text")
		forma2.elements[i].value=trimString(forma2.elements[i].value);
}

<%
String errores = "";
  if(session.getAttribute("Errores")!=null)
    errores = (String)session.getAttribute("Errores");
 else
     if (request.getAttribute("Errores")!= null)
 	errores= (String)request.getAttribute("Errores");

			out.println(errores);
%>

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
	textToTrim();
	for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
String StrScript = "";
  if(session.getAttribute("strScript")!=null)
    StrScript = (String)session.getAttribute("strScript");
 else
     if (request.getAttribute("strScript")!= null)
 	StrScript= (String)request.getAttribute("strScript");

			out.println(StrScript);
%>

<%
	if(session.getAttribute("newMenu") != null)
		out.println( session.getAttribute("newMenu"));
	else
	  if (request.getAttribute("newMenu")!= null) {
		out.println(request.getAttribute("newMenu"));
      }
%>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
String menuPrincipal = "";
  if(session.getAttribute("MenuPrincipal")!=null)
    menuPrincipal = (String)session.getAttribute("MenuPrincipal");
 else
     if (request.getAttribute("MenuPrincipal")!= null)
 	menuPrincipal= (String)request.getAttribute("MenuPrincipal");

			out.println(menuPrincipal);
%>

   </td>
  </tr>
</table>
<%
String encabezado = "";
  if(session.getAttribute("Encabezado")!=null)
    encabezado = (String)session.getAttribute("Encabezado");
 else
     if (request.getAttribute("Encabezado")!= null)
 	encabezado= (String)request.getAttribute("Encabezado");

			out.println(encabezado);
%>

<%
String CuentaCheques = "";
String NumLCredito = "";
String SdoCtaCheques = "";
String Fecha = "";
String Moneda = "";
String Vencido = "";
String Vigente = "";
String Ordinario = "";
String IVencido = "";
String IMoratorios = "";
String GtosJuridicos = "";
String Seguros = "";
String Otros = "";
String AdeudoTotal = "";

	if(session.getAttribute("Chequera") !=null)
		CuentaCheques= (String)session.getAttribute("Chequera");
	else
		CuentaCheques= (String)request.getAttribute("Chequera");

	if(session.getAttribute("Credito") !=null)
		NumLCredito = (String)session.getAttribute("Credito");
	else
		NumLCredito = (String)request.getAttribute("Credito");

	if(session.getAttribute("SaldoChq") !=null)
		SdoCtaCheques = (String)session.getAttribute("SaldoChq");
	else
		SdoCtaCheques = (String)request.getAttribute("SaldoChq");

	if(session.getAttribute("Fecha") !=null)
		Fecha = (String)session.getAttribute("Fecha");
	else
		Fecha = (String)request.getAttribute("Fecha");

	if(session.getAttribute("Moneda") !=null)
		Moneda = (String)session.getAttribute("Moneda");
	else
		Moneda = (String)request.getAttribute("Moneda");

	if(session.getAttribute("Vencido") !=null)
		Vencido= (String)session.getAttribute("Vencido");
	else
		Vencido= (String)request.getAttribute("Vencido");

	if(session.getAttribute("Vigente") !=null)
		Vigente= (String)session.getAttribute("Vigente");
	else
		Vigente= (String)request.getAttribute("Vigente");

	if(session.getAttribute("Ordinario") !=null)
		Ordinario= (String)session.getAttribute("Ordinario");
	else
		Ordinario= (String)request.getAttribute("Ordinario");

	if(session.getAttribute("Vencidos") !=null)
		IVencido= (String)session.getAttribute("Vencidos");
	else
		IVencido= (String)request.getAttribute("Vencidos");

	if(session.getAttribute("Moratorios") !=null)
		IMoratorios= (String)session.getAttribute("Moratorios");
	else
		IMoratorios= (String)request.getAttribute("Moratorios");

	if(session.getAttribute("GJuridicos") !=null)
		GtosJuridicos= (String)session.getAttribute("GJuridicos");
	else
		GtosJuridicos= (String)request.getAttribute("GJuridicos");

	if(session.getAttribute("Seguros") !=null)
		Seguros= (String)session.getAttribute("Seguros");
	else
		Seguros= (String)request.getAttribute("Seguros");

	if(session.getAttribute("Otros") !=null)
		Otros= (String)session.getAttribute("Otros");
	else
		Otros= (String)request.getAttribute("Otros");

	if(session.getAttribute("Adeudo") !=null)
		AdeudoTotal= (String)session.getAttribute("Adeudo");
	else
		AdeudoTotal= (String)request.getAttribute("Adeudo");
%>
<%

SdoCtaCheques = formatea(SdoCtaCheques);
Vencido = formatea(Vencido);
Vigente = formatea(Vigente);
Ordinario = formatea(Ordinario);
IVencido = formatea(IVencido);
IMoratorios = formatea(IMoratorios);
GtosJuridicos = formatea(GtosJuridicos);
Seguros = formatea(Seguros);
Otros = formatea(Otros);
AdeudoTotal = formatea(AdeudoTotal);

%>

<form name=PosicionLinea>
</form>

<form name="form1" method="post" action="" >
<table width="760" border="0" cellspacing="0" cellpadding="0" align=center>

    <tr>
      <td align="center">
        <table width="536" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat">Posici&oacute;n de Cr&eacute;dito Electr&oacute;nico</td>
          </tr>
          <tr>
            <td class="textabdatcla" align="center" valign="top">
              <table width="526" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="263" align="center" valign="top">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Cuenta de Cheques:</td>
                        <td class="tabmovtex" align="left" valign="middle">
                          <input type="text" name="textfield343247" size="25" class="tabmovtex" value='<%=CuentaCheques%>' onFocus='blur();' onChange="this.value='<%=CuentaCheques%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>No. de l&iacute;nea de Cr&eacute;dito:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="textfield343248" size="25" class="tabmovtex" value='<%=NumLCredito%>' onFocus='blur();' onChange="this.value='<%=NumLCredito%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Saldo cuenta Cheques:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="textfield343249" size="25" class="tabmovtex" value='<%=SdoCtaCheques%>' onFocus='blur();' onChange="this.value='<%=SdoCtaCheques%>'">
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="263" valign="top" align="right">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Fecha:</td>
                        <td class="tabmovtex" align="left" valign="middle">
                          <input type="text" name="textfield33232224" size="15" class="tabmovtex" value='<%=Fecha%>' onFocus='blur();' onChange="this.value='<%=Fecha%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Moneda:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="textfield333224" size="15" class="tabmovtex" value='<%=Moneda%>' onFocus='blur();' onChange="this.value='<%=Moneda%>'">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <table width="536" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat" width="250">Capital</td>
            <td valign="top" align="left" ><br></td>
            <td class="tittabdat" width="250">Intereses</td>
          </tr>
          <tr>
            <td class="textabdatcla" align="right" valign="top">
              <table border="0" cellspacing="5" cellpadding="0">
                <tr>
                  <td class="tabmovtex" valign="middle" align="right" nowrap>Vencido:</td>
                  <td class="tabmovtex" align="left" valign="middle">
                    <input type="text" name="textfield343245" size="25" class="tabmovtex" value="<%=Vencido%>" onFocus='blur();' onChange="this.value='<%=Vencido%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" valign="middle" align="right" nowrap>Vigente:</td>
                  <td class="tabmovtex" valign="middle" align="left">
                    <input type="text" name="textfield343246" size="25" class="tabmovtex" value="<%=Vigente%>" onFocus='blur();' onChange="this.value='<%=Vigente%>'">
                  </td>
                </tr>
              </table>
            </td>
            <td valign="top" align="left" width=10 height=10><br></td>
            <td class="textabdatcla" valign="top" align="right">
              <table border="0" cellspacing="5" cellpadding="0">
                <tr>
                  <td class="tabmovtex" valign="middle" align="right" nowrap>Ordinarios:</td>
                  <td class="tabmovtex" align="left" valign="middle">
                    <input type="text" name="textfield343242" size="25" class="tabmovtex" value="<%=Ordinario%>" onFocus='blur();' onChange="this.value='<%=Ordinario%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" valign="middle" align="right" nowrap>Vencidos:</td>
                  <td class="tabmovtex" valign="middle" align="left">
                    <input type="text" name="textfield343243" size="25" class="tabmovtex" value='<%=IVencido%>' onFocus='blur();' onChange="this.value='<%=IVencido%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" valign="middle" align="right" nowrap>Moratorios:</td>
                  <td class="tabmovtex" valign="middle" align="left">
                    <input type="text" name="textfield343244" size="25" class="tabmovtex" value='<%=IMoratorios%>' onFocus='blur();' onChange="this.value='<%=IMoratorios%>'">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <table width="536" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat">Otros adeudos</td>
          </tr>
          <tr>
            <td class="textabdatcla" align="center" valign="top">
              <table width="526" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="263" align="center" valign="top">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Gastos Jur&iacute;dicos:</td>
                        <td class="tabmovtex" align="left" valign="middle">
                          <input type="text" name="textfield34324" size="25" class="tabmovtex" value='<%=GtosJuridicos%>' onFocus='blur();' onChange="this.value='<%=GtosJuridicos%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Seguros:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="textfield34325" size="25" class="tabmovtex" value='<%=Seguros%>' onFocus='blur();' onChange="this.value='<%=Seguros%>'">
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="263" valign="top" align="right">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Otros:</td>
                        <td class="tabmovtex" align="left" valign="middle">
                          <input type="text" name="textfield34323" size="25" class="tabmovtex" value='<%=Otros%>' onFocus='blur();' onChange="this.value='<%=Otros%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" valign="middle" align="right" nowrap>Adeudo Total:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="textfield34322" size="25" class="tabmovtex" value='<%=AdeudoTotal%>' onFocus='blur();' onChange="this.value='<%=AdeudoTotal%>'">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>

        <center><A href="javascript:scrImpresion();" border=0><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir"></a></center>

        <br>
      </td>
    </tr>
</table>

</form>


</body>
</html>
