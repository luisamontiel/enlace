<html>
<head>
	<title>Pago Referenciado SAT</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script type="text/javascript">

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function CuentasCargo()
{
   msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290, title='Consulta de Cuentas'");
   msg.focus();
}

function actualizacuenta()
{
  document.tabla.cuentaCargo.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
  //document.tabla.textorigen.value=ctaselec+" "+ctadescr;
}

/**
 @Autor Alejandro Rada Vazquez
 @param num cadena numerica
 @return boolean true, false
 @descripcion evalua una cadena numerica y determina si cumple con el formato
  float sin signo
  d(n)
  d(n).d(n)
  .d(n)
  d(n).
  utilizando expresiones regulares.
*/
function isFloat(num)
{
  var TemplateF=/^\d*\.{1}\d+$|^\d+\.{0,1}\d*$/;  //Formato de numero flotante sin signo
  return TemplateF.test(num);                   //Compara "num" con el formato "Template"
}

/**
 @Autor Alejandro Rada Vazquez
 @param s cadena
 @return boolean true, false
 @descripcion verifica se una cadena esta vacia
*/
function isEmpty(s)
{
  return ((s == null) || (trimString(s).length == 0));
}

/**
 @Autor Alejandro Rada Vazquez
 @param str cadena
 @return str cadena sin espacios
 @descripcion elimina espacios a la derecha y a la izquierda de una cadena
*/
function trimString (str)
{
  str = this != window? this : str;
  return str.replace(/^\s+/g, '').replace(/\s+$/g, '');  //Quita espacios
}

/**
 @Autor Alejandro Rada Vazquez
*/
function isAlpha(cad)
{
  //var TemplateF=/^\w+$/;
  var TemplateF=/^([a-z]|[0-9]|\s)+$/i;
  return TemplateF.test(cad);
}

/**
 @Autor Alejandro Rada Vazquez
*/
function isInteger(cad)
{
  var TemplateI=/^\d+$/;
  return TemplateI.test(cad);
}


var numb = '0123456789';
var lwr = 'abcdefghijklmnopqrstuvwxyz';
var upr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

function isValid(parm,val) {

	for (i=0; i<parm.length; i++) {
		if (val.indexOf(parm.charAt(i),0) == -1) {
			return false;
		}
	}

	return true;
}

function is20(parm)
{
	if(parm.length == 20) return true;
	else return false;
}


function isAlphanum(parm)
{
	return isValid(parm,lwr+upr+numb);
}

function limpiarForma()
{
  document.tabla.reset();
}

// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			document.getElementById(elemento).value = obtenerCuenta(valor);
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
    if (res[0]=="") {
    	return "";
    };
    if(res.length>1){
	    return res[0] + "|" + res[1] + "|"  + res[2] + "|";
    }
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES




function enviarForma()
{
  var forma = document.tabla;

	obtenerCuentaSeleccionada("comboCuenta0", "cuentaCargo");

    if(trimString(forma.cuentaCargo.value)=="")
    {
		cuadroDialogo("La cuenta de Cargo es un dato obligatorio.",3);
		return;
    }

	if("" != trimString(forma.importe.value))
	{
		if(!isInteger(forma.importe.value))
		{
			cuadroDialogo("El Importe es incorrecto, contiene centavos.",3);
			return;
		}
	}
	else
	{
		cuadroDialogo("El importe es obligatorio y deber ser num&eacute;rico sin decimales.",3);
		return;
	}

	/*
	 * Mejoras de Linea de Captura
	 * Se agreg validacion para no permitir un importe 0
	 * Inicio RRR - Indra Marzo 2014
	 */
	var ceros = /^0+$/i;
	if( ceros.test(forma.importe.value)  ){
		cuadroDialogo("El Importe debe ser mayor a cero.",3);
	 	return;
	}
	/*
	 * Fin RRR - Indra Marzo 2014
	 */
  if(!isAlpha(trimString(forma.concepto.value)))
   {
	 cuadroDialogo("La L&iacute;nea de Captura es obligatoria y no debe contener caracteres especiales.",3);
	 return;
   }

  if(trimString(forma.concepto.value)=="00000000000000000000")
   {
	 cuadroDialogo("La L&iacute;nea de Captura no es v&aacute;lida.",3);
	 return;
   }

  if(!isAlphanum(forma.concepto.value))
  {
	 cuadroDialogo("La L&iacute;nea de Captura no es v&aacute;lida, esta no debe contener caracteres especiales o espacios en blanco",3);
	 return;
  }

  if(!is20(forma.concepto.value))
  {
	 cuadroDialogo("La L&iacute;nea de Captura debe ser de 20 caracteres.",3);
	 return;
  }

  forma.submit();
}

<%= request.getAttribute("newMenu")%>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
	onLoad="consultaCuentas(0,2,1);MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" >

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      ${requestScope.MenuPrincipal}
    </td>
  </tr>
</table>
${requestScope.Encabezado}
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" /></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" /></td>
  </tr>
</table>

 <FORM  NAME="tabla" method="post" action="CambioContrato">

  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td align="center">
	 <table width="490" border="0" cellspacing="2" cellpadding="3">
	  <tr>
	   <td class="tittabdat" colspan="2"> Capture los datos de su transferencia  </td>
	  </tr>
	  	<tr valign="top" >
    				<td colspan="3">
		          		<%@ include file="/jsp/filtroConsultaCuentasPRSAT.jspf" %>
		          	</td>
    	</tr>
	  <tr align="center">
	    <td class="textabdatcla" valign="top" colspan="2">
		  <table width="480" border="0" cellspacing="0" cellpadding="0">
		    <tr valign="top">
			  <td width="270" >
			    <table width="250" border="0" cellspacing="5" cellpadding="0">
				  <tr>
				    <td class="tabmovtexbol" width="158" nowrap>Cuenta de cargo:</td>
				  </tr>
				  <tr>
				    <td class="tabmovtexbol" nowrap>
 				     <%-- <input type="text" name="textorigen"  class="tabmovtexbol" maxlength="22" size="22" value ="" onfocus="blur();">
					  <A HREF="javascript:CuentasCargo();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border="0" align="absmiddle" /></A>
					  <input type="hidden" name="cuentaCargo" value="" /> --%>


						<%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
 						<input type="hidden" name="cuentaCargo" id="cuentaCargo" value=""/>
					</td>
				  </tr>
				</table>
			  </td>
			</tr>
			<tr>
			  <td width="300">
			   <table width="180" border="0" cellspacing="5" cellpadding="0">
				 <tr>
				   <td class="tabmovtexbol" nowrap>L&iacute;nea de Captura:</td>
				 </tr>
				 <tr>
				   <td class="tabmovtexbol" nowrap>
				     <INPUT TYPE="TEXT" NAME="concepto" size="70" maxlength="20" class="tabmovtexbol" />
				   </td>
				 </tr>
				 <tr>
				   <td class="tabmovtexbol" nowrap>Importe:</td>
				 </tr>
                 <tr>
				   <td class="tabmovtexbol" nowrap>
				     <INPUT TYPE="TEXT" NAME="importe" size="22" maxlength="15" class="tabmovtexbol" />
				   </td>
                 </tr>
				 <tr>
				  <td class="tabmovtexbol"><br /></td>
				 </tr>
               </table>
			  </td>
	        </tr>
		  </table>
		</td>
	   </tr>
    </table>
   </td>
  </tr>
 </table>

 <br></br>

 <table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><a href="javascript:enviarForma();" border ="0"><img src="/gifs/EnlaceMig/gbo25520.gif" style=" border: 0;" alt="Enviar" /></a><a href="javascript:limpiarForma();" border="0"><img src="/gifs/EnlaceMig/gbo25250.gif" style=" border: 0;" alt="Limpiar" /></a></td>
   </tr>
 </table>

  <INPUT type="hidden" name="Modulo" value="2" />

 </FORM>
</body>
</html>