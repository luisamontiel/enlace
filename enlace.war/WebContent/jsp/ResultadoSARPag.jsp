

<%@page import="mx.altec.enlace.bo.BaseResource"%>
<%@page import="mx.altec.enlace.bo.EI_Tipo"%>
<html>
<head>
	<title>SAR - Consulta / Cancelaci&oacute;n de Pagos</TITLE>

<script language="JavaScript" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript">

var strComprobante="";

 <%!
   String formateaFecha(String fec)
   {
	    if(fec==null || fec.trim().equals(""))
			return "";
		String fecha=fec;
		System.out.println(">>ResultadoSARPag.jsp Longitud de fecha:" +fec.trim().length());
		if(fec.trim().length()==8)
			fecha=fec.substring(6) + "/" + fec.substring(4,6) + "/" + fec.substring(0,4);
		System.out.println(">>ResultadoSARPag.jsp La nueva fecha es: " + fecha +" antes " + fec);
		return fecha;
   }
 %>

<%
  HttpSession sess = request.getSession();
  BaseResource resource = (BaseResource) sess.getAttribute("session");

  String mensajeCancelacion="";
  boolean puedeCancelar=false;
  boolean puedeReimprimir=false;
  String fechaHoy="";

  if(request.getAttribute("mensajeCancelacion") != null )
    mensajeCancelacion=(String)request.getAttribute("mensajeCancelacion");
  out.print(mensajeCancelacion);

  if(request.getAttribute("fechaHoy") != null )
    fechaHoy=(String)request.getAttribute("fechaHoy");

  if (resource.getFacultad("SARCANCENL"))
	puedeCancelar=true;

  if (resource.getFacultad("SARREIMENL"))
	puedeReimprimir=true;

%>

var fechaHoy=fechaDelDia();


function fechaDelDia()
{
  var fechaGlobal="<%=fechaHoy%>";
  if(trimString(fechaGlobal)=="")
	{
	  var hoy = new Date();

	  mdia=hoy.getDate();
	  mmes=hoy.getMonth()+1;
	  manio=hoy.getFullYear();
	  if(mdia<10) mdia="0"+mdia;
	  if(mmes<10) mmes="0"+mmes;
	  fechaGlobal=manio+"-"+mmes+"-"+mdia;
	}
  return fechaGlobal;
}

function trimString (str)
{
  str = this != window? this : str;
  return str.replace(/^\s+/g, '').replace(/\s+$/g, '');  /*Quita espacios*/
}


function js_cancelar()
{
	var forma=document.FrmResultadoSAR;
	var valor="";
    var referencia="";


	if (<%=puedeCancelar%>==false)
	{
	   cuadroDialogo("No tiene <font color=green>facultad</font> para cancelar Pagos.",1);
	   return;
	}

	for(i1=0;i1<forma.length;i1++)
    {
       if(forma.elements[i1].type=='radio' && forma.elements[i1].name=='radTabla')
			if(forma.elements[i1].checked==true)
			{
				valor=forma.elements[i1].value;
				seleccionado=true;
				break;
			}
    }


	if(valor.indexOf(fechaHoy)<=0)
	{
	   cuadroDialogo("Solo puede cancelar pagos del d&iacute;a.",1);
	   return;
	}

   if (seleccionado)
	{
		indice=valor.indexOf( "|", valor.indexOf("|")+1 );
		if(indice>3)
			referencia="con referencia: "+valor.substring(indice+1 , valor.indexOf("|",indice+1));
		cuadroDialogo("Desea Cancelar el Pago " + referencia+"?",2);
	}
	else
	{
		cuadroDialogo("Seleccione un registro por favor", 3);
	}
}


function continua()
{
   if(respuesta==1)
	{
	   document.FrmResultadoSAR.action="ConsultaUsuarios";
	   document.FrmResultadoSAR.Modulo.value='12';
	   document.FrmResultadoSAR.submit();
	}
}

function js_exportar()
{
   document.FrmResultadoSAR.action="ConsultaUsuarios";
   document.FrmResultadoSAR.Modulo.value='11';
   document.FrmResultadoSAR.submit();
}

function funcionLink(value)
{
   if(<%=puedeReimprimir%>==false)
	{
	   cuadroDialogo("No tiene <font color=green>facultad</font> para la re-impriesi&oacute;n de comprobantes de Pagos.",1);
	   return;
	}
	// vswf:meg cambio de NASApp por Enlace 08122008

   strComprobante="/Enlace/enlaceMig/ConsultaUsuarios?Modulo=10&radTabla="+escape(value);
   vc=window.open("/EnlaceMig/FrameCompSAR.html",'trainerWindow','width=850,height=800,toolbar=no,scrollbars=yes');
   vc.focus();
}

function js_descargaComprobante()
 {
   if(<%=puedeReimprimir%>==false)
	{
	   cuadroDialogo("No tiene <font color=green>facultad</font> para la re-impriesi&oacute;n de comprobantes de Pagos.",1);
	   return;
	}
   document.FrmResultadoSAR.action="ConsultaUsuarios";
   document.FrmResultadoSAR.Modulo.value='9';
   document.FrmResultadoSAR.submit();
 }

function js_regresarConsulta()
 {
   document.FrmResultadoSAR.action="ConsultaUsuarios";
   document.FrmResultadoSAR.Modulo.value='6';
   document.FrmResultadoSAR.submit();
 }

function BotonPagina(sigant)
 {
   document.FrmResultadoSAR.action="ConsultaUsuarios";
   document.FrmResultadoSAR.pagina.value=sigant-1;
   document.FrmResultadoSAR.Modulo.value='8';
   document.FrmResultadoSAR.submit();
 }

/**********************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
		if (request.getAttribute("newMenu")!= null)
			out.println(request.getAttribute("newMenu"));
%>

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/***********************************************************/

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
    <!-- MENU PRINCIPAL -->
	<%
			if (request.getAttribute("MenuPrincipal")!= null)
				out.println(request.getAttribute("MenuPrincipal"));
	%>
   </td>
  </tr>
</table>

	<%
			if (request.getAttribute("Encabezado")!= null)
				out.println(request.getAttribute("Encabezado"));
	%>
 <br><p>

<FORM  NAME="FrmResultadoSAR" METHOD="POST" ACTION="ConsultaUsuarios">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td align=center>
   <%
   	//TramaFiltro= FechaInicial@FechaFinal@ImporteInicial@ImporteFinal@CuentaCargo@Referencia@Status@RFC@
   	String tramaFiltro=(String)request.getAttribute("tramaFiltro");
   	String nombreArchivo=(String)request.getAttribute("nombreArchivo");
   	String estatus="";

   	int totalPaginas=Integer.parseInt((String)request.getAttribute("totalPaginas"));
   	int totalRegistros=Integer.parseInt((String)request.getAttribute("totalRegistros"));
   	int registrosPorPagina=Integer.parseInt((String)request.getAttribute("registrosPorPagina"));
   	int pagina=Integer.parseInt((String)request.getAttribute("pagina"));

   	int A=registrosPorPagina*pagina;
   	int de=(A+1)-registrosPorPagina;

   	if(pagina==totalPaginas)
   	   A=totalRegistros;

   	EI_Tipo Filtro=new EI_Tipo();
   	Filtro.iniciaObjeto(tramaFiltro);

   	if(Filtro.camposTabla[0][6].trim().equals("R"))
   	  estatus="Rechazados";
   	else
        if(Filtro.camposTabla[0][6].trim().equals("C"))
   	   estatus="Cancelados";
   	else
   	  estatus="Pagados";
   %>
   <table border=0>
    <tr>
	 <td>

	 <table border=0 align=center cellspacing=0 cellpadding=1 width="100%">

	  <tr>
	    <td class='textabref'><b>&nbsp;Resultados de Consulta&nbsp;</b></td>
 	    <td class='textabref' align=right><b>&nbsp;Estatus: <font color=blue><%=estatus%></font>&nbsp;</b></td>
	  </tr>

	  <tr>
		<td class='textabref'><b>&nbsp;Total de registros encontrados: <font color=green><%=totalRegistros%></font></b></td>
		<td align=right class='textabref'><b> del <font color=green><%=Filtro.camposTabla[0][0]%></font> al <font color=green><%=Filtro.camposTabla[0][1]%></font>&nbsp;</b></td>
	  </tr>

	  <tr>
		<td align=left class='textabdatcla'><b>&nbsp;Pagina <font color=blue><%=pagina%></font> de <font color=blue><%=totalPaginas%></font>&nbsp;</b></td>
		<td align=right class='textabdatcla'><b>Registros <font color=blue><%=de%></font> - <font color=blue><%=A%></font></b>&nbsp;</td>
	  </tr>

	 </table>

	 <!-- Tabla de registros obtenidos //-->
	 <%=request.getAttribute("tablaRegistros")%>
	 <!-- fin Tabla //-->

 	 <table border=0 align=center cellspacing=0 cellpadding=0 width="100%">
	  <tr>
		<td align=left class='textabref'><b>&nbsp;Pagina <font color=blue><%=pagina%></font> de <font color=blue><%=totalPaginas%></font>&nbsp;</b></td>
		<td align=right class='textabref'>&nbsp;<b>Registros <font color=blue><%=de%></font> - <font color=blue><%=A%></font></b>&nbsp;</td>
	  </tr>
	 </table>

	</td>
   </tr>
  </table>

    <%=request.getAttribute("Botones")%>
	<br>
	<table align=center border=0 cellspacing=0 cellpadding=0>
	 <tr>
	  <td><A href="javascript:js_regresarConsulta();" border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt=Regresar></a><A href="javascript:js_descargaComprobante();" border = 0><img src = "/gifs/EnlaceMig/gbo25210.gif" border=0 alt="Comrpobante PDF"></a><A href="javascript:js_cancelar();" border = 0><img src = "/gifs/EnlaceMig/gbo25190.gif" border=0 alt=Cancelar></a><A href="javascript:js_exportar();" border = 0><img src = "/gifs/EnlaceMig/gbo25230.gif" border=0 alt=Exportar></a></td>
	 </tr>
    </table>

	<input type=hidden name=tramaFiltro value='<%=tramaFiltro%>'>
	<input type=hidden name=nombreArchivo value='<%=nombreArchivo%>'>
	<input type=hidden name=totalRegistros value='<%=totalRegistros%>'>
	<input type=hidden name=registrosPorPagina value='<%=registrosPorPagina%>'>
	<input type=hidden name=totalPaginas value='<%=totalPaginas%>'>

	<input type=hidden name=Modulo value='8'>
	<input type=hidden name=pagina value='<%=pagina%>'>

   </td>
  </tr>
 </table>

 </FORM>
</body>
</html>