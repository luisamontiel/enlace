<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@ page import = "java.util.*"%>

<jsp:useBean id = "arch_combos"		class = "mx.altec.enlace.bo.servicioConfirming"	scope="request"/>
<jsp:useBean id = "arch_prov"		class = "mx.altec.enlace.bo.Proveedor"			scope="request"/>
<html>
<%
	EIGlobal.mensajePorTrace("entrando a jsp",EIGlobal.NivelLog.DEBUG);
%>

<!-- Recorta el tama�o de la cadena c(Longitud Maxima 30-35) -->
<%! public String corta(String c)
	{
	String aux = "";

	if (c.length()  < 35)
		return c;
	else
		{
		for(int i= 0; i < 30; i++)
			aux+= String.valueOf(c.charAt(i));
		return aux;
		}
	}
%>

<!-- Llena los vectores con los datos de los proveedores-->
<%
	String ArchivoResp = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","","");

	Vector ls_prov = null;
	Vector cv_prov = null;
	Vector cv_trans = null;
	Vector cv_div = null;
	Vector cv_tipo = null;
	Vector cv_origen = null;

	if(ArchivoResp != null)
		{
		ls_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",1);
		cv_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",2);
		cv_trans = arch_prov.coMtoProv_Leer(ArchivoResp,"9",3);
		/*
		* jgarcia - Stefanini
		* Creacion del vector para divisas y para el tipo de proveedor
		*/
		cv_div   = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 5);
		cv_tipo  = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 6);
		}

	if(cv_prov != null)
		{
		int a, b, total = cv_prov.size();
		String strA = null, strB = null;

		for(a=0;a<total-1;a++)
			for(b=a+1;b<total;b++)
				{
				strA = (String)ls_prov.get(a);
				strB = (String)ls_prov.get(b);
				if(strA.compareTo(strB)>0)
					{
					ls_prov.set(a,strB);
					ls_prov.set(b,strA);
					strA = (String)cv_trans.get(a);
					strB = (String)cv_trans.get(b);
					cv_trans.set(a,strB);
					cv_trans.set(b,strA);
					// jgarcia 30/Sept/2009
					// Cambia el orden de las divisas y el tipo de proveedor
					strA = (String)cv_prov.get(a);
					strB = (String)cv_prov.get(b);
					cv_prov.set(a,strB);
					cv_prov.set(b,strA);
					strA = (String)cv_div.get(a);
					strB = (String)cv_div.get(b);
					cv_div.set(a,strB);
					cv_div.set(b,strA);
					strA = (String)cv_tipo.get(a);
					strB = (String)cv_tipo.get(b);
					cv_tipo.set(a,strB);
					cv_tipo.set(b,strA);
					}
				}
		}

%>

<head>
	<title>Banca Virtual</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta http-equiv="Expires" content="1">
	<meta http-equiv="pragma" content="no-cache">

	<!-- ----------------------------- Seccin de JavaScript ---------------------------- -->
	<script language = "JavaScript" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
    <script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
    <script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript" src="/EnlaceMig/passmark.js"></script>
	<script language = "Javascript" >

	var ctaselec;
    var ctadescr;
    var ctatipro;

	// --- Consulta las Cuentas de Cargo
	function PresentarCuentas(){
	  document.frmConsultCanc.Divisa.selectedIndex = "0";
	  document.frmConsultCanc.CveDivisa.value="";
	  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&tipoOp="+document.frmConsultCanc.operacion.value,"Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
	  msg.focus();

	}

function actualizacuenta()
{
  document.frmConsultCanc.Cuentas.value=ctaselec+"|"+ctadescr+"|"+ctatipro+"@";
  document.frmConsultCanc.textCuentas.value=ctaselec+" "+ctadescr;
  //MuestraInfo();
}

 function MuestraInfo()
 {
    var forma=document.frmConsultCanc;

   if(trimString(forma.textCuentas.value)=="")
    {
	  forma.Info.value="";
	  return;
	}}

	function SeleccionaDivisa()
 {
   var forma=document.frmConsultCanc;

   if(forma.Divisa.options[forma.Divisa.selectedIndex].value=="0"){
	 forma.CveDivisa.value="";}

    if(forma.Divisa.options[forma.Divisa.selectedIndex].value=="1"){
	 forma.CveDivisa.value="MXN";
	 forma.textCuentas.value="";
	 }

    if(forma.Divisa.options[forma.Divisa.selectedIndex].value=="2"){
     forma.CveDivisa.value="USD";
     forma.textCuentas.value="";
     }
 }

	// --- Elimina los espacios en los extremos
	function trim(cadena)
		{
		var str = "" + cadena;
		while(str.substring(0,1) == ' ') str = str.substring(1);
		while(str.substring(str.length-1) == ' ') str = str.substring(0,str.length-1);
		return str;
		}

	// --- Indica si una cadena representa un entero
	function esEntero(cadena)
		{
		var str = "" + cadena;
		var entero = true;
		if(str == "") return false;
		while(entero && str.length>0) {entero=isDigit(str.substring(0,1)); str=str.substring(1);}
		return entero;
		}

	// --- Indica si la cadena es un d�gito
	function isDigit (c)
		{return ((c >= "0") && (c <= "9"))}

	// --- Indica si una cadena representa un formato de moneda
	function esMoneda(cadena)
		{
		if(esEntero(cadena)) return true;
		var str="" + cadena;
		if(str.indexOf(".")==-1) return false;
		var partEntera = str.substring(0,str.indexOf("."));
		var partFracc = str.substring(str.indexOf(".")+1);
		if(!esEntero(partEntera) || (!esEntero(partFracc) && partFracc!="")) return false;
		if(partFracc.length>2) return false;
		return true;
		}

	// --- Valida el formulario antes de enviarlo
	function DoPost()
		{
		document.frmConsultCanc.Importe.value=trim(document.frmConsultCanc.Importe.value);
		document.frmConsultCanc.Arch.value=trim(document.frmConsultCanc.Arch.value);

		var fecha1 = "" + document.frmConsultCanc.fecha1.value;
		var fecha2 = "" + document.frmConsultCanc.fecha2.value;
		var importe = "" + document.frmConsultCanc.Importe.value;
		var archivo = "" + document.frmConsultCanc.Arch.value;
		var posDoc = document.frmConsultCanc.Proveedor.selectedIndex;
		var cuentaCargo = "" + document.frmConsultCanc.textCuentas.value;
	    var Divisa = document.frmConsultCanc.Divisa.textCuentas;

		var resultado = true;

		if(document.frmConsultCanc.Reg[0].checked)
			{// seleccin: por archivos

			//validar archivo: num�rico y no vac�o
			if(archivo == "")
				{
				document.frmConsultCanc.Arch.focus();
				cuadroDialogo("Debe indicar un archivo",3);
				return;
				}
			if(!esEntero(archivo))
				{
				document.frmConsultCanc.Arch.focus();
				cuadroDialogo("El archivo debe ser un n&uacute;mero entero",3);
				return;
				}

			//validar importe: vac�o o moneda
			if(!esMoneda(importe) && importe != "")
				{
				document.frmConsultCanc.Importe.focus();
				cuadroDialogo("El importe debe tener una cantidad monetaria",3);
				return;
				}
			}
		else
			{// seleccin: por documentos

			//Si las fechas no se eligieron, se colocan las fechas por omisin
			if(fecha1 == "")
				{
				var mes=document.Frmfechas.strMes.value;
				var anio=document.Frmfechas.strAnio.value;
				mes-=2; if(mes<1) {mes+=12; anio--;}
				fecha1="01/" + ((mes<10)?"0":"") + mes + "/" + anio;
				document.frmConsultCanc.fecha1.value=fecha1;
				}
			if(fecha2 == "")
				{
				var dia=document.Frmfechas.strDia.value;
				var mes=document.Frmfechas.strMes.value;
				var anio=document.Frmfechas.strAnio.value;
				fecha2= dia + "/" + mes + "/" + anio;
				document.frmConsultCanc.fecha2.value=fecha2;
				}

			/* -------------------------- (Que el proveedor no es obligatorio...)

			//validar proveedor <> 0 o -1
			if(posDoc<1)
				{
				document.frmConsultCanc.Proveedor.focus();
				cuadroDialogo("Debe elegir un proveedor",3);
				return;
				}

			-------------------------- */

			//validar fechas: coherentes
			fecha1 = fecha1.substring(6,10) + fecha1.substring(3,5) + fecha1.substring(0,2);
			fecha2 = fecha2.substring(6,10) + fecha2.substring(3,5) + fecha2.substring(0,2);
			if(parseInt(fecha1)>parseInt(fecha2))
				{
				cuadroDialogo("La fecha inicial no debe ser mayor que la final, verifique por favor");
				return;
				}

			//validar importe: vac�o o moneda
			if(!esMoneda(importe) && importe != "")
				{
				document.frmConsultCanc.Importe.focus();
				cuadroDialogo("El importe debe tener una cantidad monetaria",3);
				return;
				}
			}

		document.frmConsultCanc.submit();
		}

	// --- variables necesarias para los calendarios
	var fecha_completa = "";
	var opcionCalendario = 0;

	// --- esto se ejecuta despu�s de elegir un d�a en el calendario
	function Actualiza()
		{
		if(opcionCalendario == 1)
			document.frmConsultCanc.fecha1.value = fecha_completa;
		else
			document.frmConsultCanc.fecha2.value = fecha_completa;
		}

	// --- muestra la ventana de calendario
	function seleccionaFecha(opcion)
		{
		if(document.frmConsultCanc.Reg[0].checked)
			{
			cuadroDialogo("No pueden elegirse fechas para consulta por archivo",3);
			return;
			}

		opcionCalendario = opcion;

		var dia = document.Frmfechas.strDia.value;		//MARL BEGIN
		var mes = document.Frmfechas.strMes.value-1;	// INC. IM135515.
		var ann = document.Frmfechas.strAnio.value;		//MARL END
		var diaI = 1;
		var diaF = dia;
		var mesI = mes - 2;
		var mesF = mes + 12;
		var annI = ann;
		var annF = ann;

		if(mesI<0) {mesI += 12; annI--;}

		var texto = "jsp/CalendarioHTML.jsp?diaI=" + diaI +
			"&diaF=" + diaF + "&mesI=" + mesI + "&mesF=" + mesF + "&anioI=" + annI + "&anioF=" +
			annF + "&diasInhabiles=" + escape(js_diasInhabiles) + "&mes=" + mes + "&anio=" +
			ann + "&priUlt=0#mesActual";
		var msg=window.open(texto,"calendario","toolbar=no," +
			"location=no,directories=no,status=no,menubar=no" +
			",scrollbars=yes,resizable=no,width=330,height=260");
		msg.focus();
		}

	var js_diasInhabiles='<%= request.getAttribute("diasInhabiles") %>';

	// --- Habilita o desabilita los controles seg�n la opcin elegida (por archivos o documentos)
	function EstableceEstado()
		{
		if (document.frmConsultCanc.Reg[1].checked)
			{ // si se selecciona documentos...
			document.frmConsultCanc.Arch.value="";
			document.frmConsultCanc.Arch.disabled = true;
			document.frmConsultCanc.Proveedor.disabled = false;
			}
		else
			{ // si se selecciona archivo...
			document.frmConsultCanc.Proveedor.selectedIndex=-1;
			document.frmConsultCanc.Proveedor.disabled = true;
			document.frmConsultCanc.Arch.disabled = false;
			document.frmConsultCanc.fecha1.value="";
			document.frmConsultCanc.fecha2.value="";
			}
		}

	// --- Quita el foco de la lista de proveedores si se ha selecionado "archivo"
	function ValidaFocoProveedor()
		{
		if(document.frmConsultCanc.Reg[0].checked) document.frmConsultCanc.Proveedor.blur();
		}

	// --- Quita el foco del campo Archivo si se ha selecionado "documentos"
	function ValidaFocoArchivo()
		{
		if(document.frmConsultCanc.Reg[1].checked) document.frmConsultCanc.Arch.blur();
		}

	// --- Seccin para el men� - (comienza) --------------------------------------------------
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	<%= request.getAttribute("newMenu") %>
	// --- Seccin para el men� - (termina) --------------------------------------------------

	// --- esto se ejecuta cuando se carga la p�gina
	function inicia()
		{
		document.frmConsultCanc.Proveedor.selectedIndex=-1;
		document.frmConsultCanc.Proveedor.disabled = false;
		document.frmConsultCanc.Arch.disabled = false;
		document.frmConsultCanc.fecha1.value="";
		document.frmConsultCanc.fecha2.value="";
		document.frmConsultCanc.Reg[1].checked = true;
		PutDate();
		MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
						 '/gifs/EnlaceMig/gbo25111.gif',
						 '/gifs/EnlaceMig/gbo25151.gif',
						 '/gifs/EnlaceMig/gbo25031.gif',
						 '/gifs/EnlaceMig/gbo25032.gif',
						 '/gifs/EnlaceMig/gbo25051.gif',
						 '/gifs/EnlaceMig/gbo25052.gif',
						 '/gifs/EnlaceMig/gbo25091.gif',
						 '/gifs/EnlaceMig/gbo25092.gif',
						 '/gifs/EnlaceMig/gbo25012.gif',
						 '/gifs/EnlaceMig/gbo25071.gif',
						 '/gifs/EnlaceMig/gbo25072.gif',
						 '/gifs/EnlaceMig/gbo25011.gif');
		}

	</script>

	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>


<!-- SECCION PRINCIPAL ----------------------------------------------------------------->

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="inicia();" background="/gifs/EnlaceMig/gfo25010.gif">

	<!-- MENU PRINCIPAL Y ENCABEZADO -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</table>
	<%= request.getAttribute("Encabezado") %>

	<br><br>

	<!-- Forma para contener hiddens con la fecha -->
	<form name = "Frmfechas">
		<Input type="hidden" name="strAnio" value="<%= request.getAttribute("Anio") %>">
		<Input type="hidden" name="strMes" value ="<%= request.getAttribute("Mes") %>">
		<Input type="hidden" name="strDia" value ="<%= request.getAttribute("Dia") %>">
	</form>

	<FORM NAME="frmConsultCanc" ACTION="coCancelacion" >
	<table width="760" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">

		<!-- tabla principal: 4 ren x 1 col -->
		<table width="620" border="0" cellspacing="2" cellpadding="3">

		<!-- ren 1 / 4: encabezado -->
		<tr><td class="tittabdat">Especifique los datos para su consulta</td></tr>

		<!-- ren 2 / 4: seleccin de consulta -->
		<tr align="left"><td class="textabdatcla" valign="top">

			<table width="320" border="0" cellspacing="5" cellpadding="0">
			<tr valign="top">
				<td colspan="2" align="right" class="tabmovtex1" nowrap><b>Consulta por:</b></td>
				<td class="tabmovtex11" nowrap align="left" valign="middle">
<!--GOR 20110113 (Req. Confirming - 4 - Inicio): Mostrar nuevo filtro (Num Docto) cuando se trata de tipo Documento -->											
					<input type="radio" name="Reg" value="5" onClick="EstableceEstado();document.getElementById('renglonNumDocumento').style.display ='none';">Archivo
<!--GOR 20110113 (Req. Confirming - 4 - Fin) -->											
				</td>
				<td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="25" height="0"></td>
				<td class="tabmovtex11" nowrap align="left" valign="middle">
<!--GOR 20110113 (Req. Confirming - 4 - Inicio): Mostrar nuevo filtro (Num Docto) cuando se trata de tipo Documento -->											
					<input type="radio" name="Reg" value="6" onClick="EstableceEstado();document.getElementById('renglonNumDocumento').style.display ='';">Documentos
<!--GOR 20110113 (Req. Confirming - 4 - Fin) -->											
				</td>
			</tr>
			<tr valign="top">
				<td align="right" nowrap class="tabmovtex11" width="60">Archivo: </td>
				<td class="tabmovtex" nowrap width="185" colspan="5">
					<INPUT TYPE="Text" NAME="Arch" SIZE = "13" onFocus="ValidaFocoArchivo();">
				</td>
			</tr>
			<tr valign="top">
				<td align="right" nowrap class="tabmovtex11" width="60">Proveedor: </td>
				<td class="tabmovtex" nowrap WIDTH="185" colspan="5">
					<SELECT NAME="Proveedor" class="tabmovtex" onFocus="ValidaFocoProveedor();">
						<OPTION VALUE=""></OPTION>
						<% if(ls_prov != null) {
						   int longitud = ls_prov.size();
						   for (int i=0; i<(longitud); i++) {
						   		String tipo = cv_tipo.elementAt(i).equals("I")?"INT":"NAL"; %>
						<option value="<%=cv_trans.elementAt(i)%>"><%= corta((String)ls_prov.elementAt(i)) + "|" + corta((String)cv_prov.elementAt(i)) + "|" + tipo + "|" + cv_div.elementAt(i) %></option>
						<% } } %>
					</SELECT>
				</td>
			</tr>
			</table>

		</td></tr>

		<!-- ren 3 / 4: filtros -->

		<tr align="center"><td class="textabdatcla" valign="top">
		<table width="620" border="0" cellspacing="5" cellpadding="0">
			<tr>
			    <td class='tabmovtex' align="right" width="90">
				  Divisa: </td>
				<td class="tabmovtex" nowrap WIDTH="300">
				  <input type=text name=CveDivisa maxlength=5 size=5 onFocus='blur();' class='tabmovtex'>
				  <select NAME="Divisa" class="tabmovtex" onChange="SeleccionaDivisa();" >
					<option VALUE="0">Seleccione Divisa</option>
					<option VALUE="1">Moneda Nacional</option>
					<option VALUE="2">D&oacute;lares Americanos</option>
				  </select>
				</td>
				<td align="left" class='tabmovtex' width="90" colspan="4"> Forma de pago:</td>
			</tr>
			<tr>
				<td align="right" class='tabmovtex' width="90"> Cuenta Cargo:</td>
				<td class="tabmovtex" nowrap width="300">
	     			<input type="text" name=textCuentas  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="" class='tabmovtex'>
		 			<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
		 			<input type="hidden" name="Cuentas" value="" >
				</td>
				<td align="left" class='tabmovtex' width="90" colspan="4">
					<select name="vForma">
						<option value="-1"></option>
						<option CLASS='tabmovtex' value='1'>Mismo Banco.</option>
						<option CLASS='tabmovtex' value='2'>Otros Bancos Nacionales.</option>
						<option CLASS='tabmovtex' value='5'>Internacional.</option>
					</select>
				</td>
		   </tr>
		</table>
		</tr>

<!-- ren 3 / 4: filtros -->
		<tr align="center"><td class="textabdatcla" valign="top">

			<table width="600" border="0" cellspacing="0" cellpadding="0" >
			<TR><TD>

				<!-- Tabla de fechas y monto -->
				<table width="250" border="0" cellspacing="1" cellpadding="4">
				<tr>
					<td align="left" class="tabmovtex1" colspan="2"><b>Filtros:</b></td>
				</tr>
				<tr>
					<td align="right" class="tabmovtex11">De la fecha:&nbsp;</td>
					<td class="tabmovtex" align="left" valign="middle">
						<Input type = "Text" size=14 name="fecha1" onfocus="blur();" class="tabmovtex">
						<A href ="javascript:seleccionaFecha(1);"><!-- WindowCalendar() --><img
							src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0"></a>
					</td>
				</tr>
				<tr>
					<td align="right" class="tabmovtex11">A la fecha:&nbsp;</td>
					<td align="left" class="tabmovtex" nowrap>
						<Input type = "Text" size=14 name="fecha2" OnFocus="blur();" class="tabmovtex">
						<A href ="javascript:seleccionaFecha(2);"><img
							src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0"></a>
					</td>
				</tr>
				<tr>
					<td align="right" class = "tabmovtex11">Importe:&nbsp;</td>
					<td class="tabmovtex" nowrap>
						<INPUT TYPE="Text" NAME="Importe" SIZE="18" class="tabmovtex">
					</td>
				</tr>
<!--GOR 20110113 (Req. Confirming - 4 - Inicio): Mostrar nuevo filtro (Num Docto) cuando se trata de tipo Documento -->											
				<tr id="renglonNumDocumento">
					<td align="right" class = "tabmovtex11">N&uacute;mero de documento:&nbsp;</td>
					<td class="tabmovtex" nowrap>
						<INPUT TYPE="Text" NAME="numDocumento" SIZE="18" class="tabmovtex">
					</td>
				</tr>
<!--GOR 20110113 (Req. Confirming - 4 - Fin) -->											
				</table>

			</TD><TD rowspan="2" valign="top">

				<!-- Tabla de Estatus -->
				<table width="220" border="0" cellspacing="5" cellpadding="2">
				<tr><td align="left" nowrap class="tabmovtex1" width="60" valign="top"><b>Estatus:</td></tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="R">&nbsp;Por pagar
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="L">&nbsp;Liberados no pagados
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="P">&nbsp;Pagados
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="A">&nbsp;Anticipados
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="C">&nbsp;Cancelados
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="Z">&nbsp;Rechazados
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="V">&nbsp;Vencidos
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="Estatus" value="D">&nbsp;Devueltos
					</td>
				</tr>

				</table>

			</TD></TR><TR><TD>

				<!-- Tabla de tipo de documento -->
				<table width="150" border="0" cellspacing="4" cellpadding="3" >
				<tr><td align="left" class="tabmovtex1"><b>Tipo de Documento:</b></td></tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="TDoc" value="1">&nbsp;Facturas
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="TDoc" value="3">&nbsp;Notas de cargo
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="TDoc" value="4">&nbsp;Notas de cr&eacute;dito
					</td>
				</tr>
				<tr>
					<td class="tabmovtex11" nowrap align="left" valign="middle">
						<input type="checkbox" name="TDoc" value="2">&nbsp;Otros documentos
					</td>
				</tr>
				</table>

			</TD></TR>
			</TABLE>

		</TD></TR>

		<!-- ren 4 / 4: botones -->
		<tr align="center"><td valign="bottom" height="50px">

			<A href ="javascript:DoPost();"><IMG border="0" src="/gifs/EnlaceMig/gbo25220.gif" alt="Consultar"></A><!-- A
				href ="javascript:document.frmConsultCanc.reset();"><IMG
				border="0" src="/gifs/EnlaceMig/gbo25250.gif" alt="Limpiar"></A -->

		</TD></TR>

		</table>

	</table>
	<INPUT type=Hidden name="tipoReq" value="1">
	  <input type=hidden name=DesDivisa >
	  <input type="hidden" name="operacion" value="CFRP">
	</FORM>

  <%=
      (request.getAttribute("mensaje") != null)?
	   ("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
		");</script>"):""
  %>

</body>

</html>