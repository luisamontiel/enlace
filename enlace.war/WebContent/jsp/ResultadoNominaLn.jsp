<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@ page import="mx.altec.enlace.bita.BitaConstants"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="mx.altec.enlace.utilerias.Global"%>

<html>
<head>
<meta http-equiv="Expires" content="1" />
<meta http-equiv="pragma" content="no-cache" />
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src= "/EnlaceMig/cuadroDialogo.js"></script>


<%-- Estilos //--%>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
<style type="text/css">
.componentesHtml {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
}
</style>
<style>
div.fileinputs {
	position: relative;
}

div.fakefile {
	position: absolute;
	top: 0px;
	left: 0px;
	z-index: 1;
}
</style>

<script type="text/javascript">

/************************ Funciones grales*******************************************/
	function MM_preloadImages() {
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() {
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) {
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() {
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	function imprimir() {
		scrImpresion();
	}
/*************************************************************************************/

	function regresar(){
		document.resultadoNominaLn.operacion.value="regresar";
		document.resultadoNominaLn.action="OpcionesNominaLn";
		document.resultadoNominaLn.submit();
	}

	function exportar() {
		var contador=0;
		for (i=0;i<document.resultadoNominaLn.elements.length;i++){
			if (document.resultadoNominaLn.elements[i].type=="radio" && document.resultadoNominaLn.elements[i].name=="secArchivo"){
				if (document.resultadoNominaLn.elements[i].checked==true){
					contador=1;
					break;
				}
			}
		}
		if (contador==1){
			document.resultadoNominaLn.operacion.value="exportar";
			document.resultadoNominaLn.action="OpcionesNominaLn";
			document.resultadoNominaLn.submit();
		}else{
			cuadroDialogo("Debe seleccionar un registro para exportar", 1);
		}
	}

	/*Funcion para solicitar el Informe en PDF*/
	function solicitaPDF(){

		var contador=0;
		indice = 0;
		for (i=0;i<document.resultadoNominaLn.elements.length;i++){
			if (document.resultadoNominaLn.elements[i].type=="radio" && document.resultadoNominaLn.elements[i].name=="secArchivo"){
				if (document.resultadoNominaLn.elements[i].checked==true){

					if (document.resultadoNominaLn[i].value.indexOf("Procesado")>=0 )
					{
					    contador=1;
					    indice = i;
					}else{
						contador=2;
					}
					break;
				}
			}
		}
		if (contador==1){
			document.resultadoNominaLn.operacion.value="SolicitaPDF";
			document.resultadoNominaLn.action="OpcionesNominaLn";
			document.resultadoNominaLn.submit();
		}else if(contador==2){
			cuadroDialogo("La n&oacute;mina debe estar procesada para solicitar el informe.", 1);
		}else{
			cuadroDialogo("Debe seleccionar un registro para acceder a esta opci�n", 1);
		}

	}

	function descargaPDF(secuencia){

		document.resultadoNominaLn.secDescarga.value=secuencia;
		document.resultadoNominaLn.operacion.value="descargaPDF";
		document.resultadoNominaLn.action="OpcionesNominaLn";
		document.resultadoNominaLn.submit();


	}

	/**************** Control paginacion *****************/
	var strpaginas 		  =	'${pPaginas}';
	var strpagina 		  =	'${pPagina}';

	var paginas=parseInt(strpaginas, 10);
	var pagina=parseInt(strpagina, 10);

	function inicio(){
		if(paginas==0){	return; }
		pagina=1;
		document.resultadoNominaLn.operacion.value="consulta";
		document.resultadoNominaLn.action="OpcionesNominaLn";
		document.resultadoNominaLn.NoPagina.value=pagina;
		document.resultadoNominaLn.submit();
	}

	function fin(){
		if(paginas==0){	return; }
		pagina = paginas;
		document.resultadoNominaLn.operacion.value="consulta";
		document.resultadoNominaLn.action="OpcionesNominaLn";
		document.resultadoNominaLn.NoPagina.value=pagina;
		document.resultadoNominaLn.submit();
	}

	function anterior(){
		if(paginas==0){	return; }
		pagina--;
		if(pagina<=0) pagina=1;
		document.resultadoNominaLn.operacion.value="consulta";
		document.resultadoNominaLn.action="OpcionesNominaLn";
		document.resultadoNominaLn.NoPagina.value=pagina;
		document.resultadoNominaLn.submit();
	}

	function siguiente(){
		if(paginas==0){	return; }
		pagina++;
		if(pagina > paginas) pagina=paginas;
		document.resultadoNominaLn.operacion.value="consulta";
		document.resultadoNominaLn.action="OpcionesNominaLn";
		document.resultadoNominaLn.NoPagina.value=pagina;
		document.resultadoNominaLn.submit();
	}

	var strnomArchivo	  =	'${archivoExportar}';
	var tipoArchivoExportar	  =	'${tipoArchivoExportar}';
	function descargandoArchivo() {
		if(strnomArchivo!='')
		window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=ARCHIVO&em=ExportModel&af=af&tipoArchivo=" + tipoArchivoExportar,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	}

	<%= request.getAttribute("newMenu")%>
</script>
<script type="text/javascript">
	<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
	if (request.getAttribute("infoUser")!= null) {
		out.println(request.getAttribute("infoUser"));
	}
	%>

	<%
	/*FSW Vector - ventana de descarga PDF*/

	if(request.getAttribute("URLDescargaPDF")!= null) {
	out.println( "msg=window.open(\""+ request.getAttribute("URLDescargaPDF") +"\",\"\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=400,height=200\");"+
			"msg.focus();");
	}

%>

//  PyMES .. Marzo 2015
// Exportar a archivo plano TXT - CSV
function exportarArchivoPlano() {

 	var test = document.getElementsByName("tipoExportacion");
    var sizes = test.length;
    var extencion;
    for (i=0; i < sizes; i++) {
            if (test[i].checked==true) {
        	extencion = test[i].value;
        }
    }
	window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=ARCHIVO&em=ExportModel&nombreBeanExport=nombreBeanExportArch&tipoArchivo=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
}

 // PyMES .. Marzo 2015



</script>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); descargandoArchivo();"
	 >
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*"><%= request.getAttribute("MenuPrincipal")%></td>
	</tr>
	</table>
	<%= request.getAttribute("Encabezado")%>
	<table width="571" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" /></td>
	    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" /></td>
	  </tr>
	</table>
<form method="Post" name="resultadoNominaLn" ACTION="OpcionesNominaLn">
	<input type="hidden" name="operacion" value="1" />
	<input type="hidden" name="textcuenta" value="${pCtaCargo}" />
	<input type="hidden" name="fecha1" value="${pFchIni}" />
	<input type="hidden" name="fecha2" value="${pFchFin}" />
	<input type="hidden" name="NoPagina" value="${pPagina}" />
	<input type="hidden" name="hddTipoFecha" value="${pTipoFecha}" />
	<input type="hidden" name="secDescarga" value="" />

	<INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("nombre_arch_salida")!= null)



		out.println(request.getAttribute("nombre_arch_salida"));
		%>" NAME="nombre_arch_salida" />
		<%
		NominaLineaBean resultado = (NominaLineaBean) request.getAttribute("resultadoConsulta");
		if (resultado != null && resultado.getLstConsulta() != null && resultado.getLstConsulta().size() > 0) {
			int numRegistros = resultado.getLstConsulta().size();
		%>
		<table cellspacing="0" cellpadding="0" border="0" width="760">
			<tr>
				<td align="center" class="tabmovtex" >
					Total de archivos: <%=request.getAttribute("totalRegs")%> <br>
					Registros del <%=request.getAttribute("pDel")%> al <%=request.getAttribute("pAl")%><br />
					Pagina ${pPagina} de ${pPaginas}
				</td>
			</tr>
			<tr>
				<td align="center">
				<table width="500" border="0" cellspacing="1" cellpadding="2" class="tabfonbla">
					<tr>
						<td align="center" width="50" class="tittabdat">Seleccione</td>
						<td align="center" width="70" class="tittabdat">Fecha de aplicaci&oacute;n</td>
						<td align="center" width="100" class="tittabdat">Cuenta de cargo</td>
						<td align="center" width="50" class="tittabdat">N&uacute;m. registros</td>
						<td align="center" width="80" class="tittabdat">Importe aplicado</td>
						<td align="center" width="70" class="tittabdat">Estatus</td>
						<td align="center" width="70" class="tittabdat">Comprobante</td>
					</tr>
					<%
					Iterator iterator = resultado.getLstConsulta().iterator();
					boolean band = false;
					int i=0;
					StringBuffer  sb ;
					String tramaDatos = "" ;
					NominaLineaBean archivo = null;
					String sClass  = "";
					while (iterator.hasNext()) {
						archivo = (NominaLineaBean) iterator.next();
						 sClass = band ? "textabdatcla" : "textabdatobs";
					%>
					<tr>
						<td align="center" width="60" class="<%=sClass%>">
							<%if(archivo.getEstatus().equals("Procesado") || archivo.getEstatus().equals("No procesado")){
							/*FSW Vector - Tactyco Epyme - Generacion de informes PDF*/
							tramaDatos = null;
							sb = new StringBuffer();

							sb.append(archivo.getSecArchivo());
							sb.append("|");
							sb.append(archivo.getEstatus());

							if(archivo.getEstatus().equals("Procesado")){

								sb.append("|");
								sb.append(archivo.getFchCarga());
							}
							tramaDatos = sb.toString();
							sb = null;

							%>

								<input type="radio" name="secArchivo"
								value="<%=tramaDatos%>" />

							<%}%>
						</td>
						<td class="<%=sClass%>" nowrap align="center"><%= archivo.getFchAplicacion() %></td>
						<td class="<%=sClass%>" nowrap align="center"><%= archivo.getCtaCargo() %></td>
						<td class="<%=sClass%>" nowrap align="center"><%= archivo.getNumRegistros() %></td>
						<td class="<%=sClass%>" nowrap align="right">$ <%= archivo.getImporteAplicado() %></td>
						<td class="<%=sClass%>" nowrap align="left"><%= archivo.getEstatus() %></td>
						<%

						 if ("Disponible".equals(archivo.getEstatusSol())){
						%>
						  <td class="<%=sClass%>" nowrap align="left">
						  <a href = "javascript:descargaPDF('<%=tramaDatos%>');" > <%=archivo.getEstatusSol()%></a>

						  </td>
						<%}else if("Rechazado".equals(archivo.getEstatusSol())){ %>
						    <td class="<%=sClass%>" nowrap align="left">
						 		<a href = "javascript:cuadroDialogo('<%=archivo.getObservaciones() %>',1);" > <%=archivo.getEstatusSol() %></a>
						  	</td>
						<%} else { %>
						    <td class="<%=sClass%>" nowrap align="left"><%=archivo.getEstatusSol() %></td>
						<%} %>
					</tr>
					<%
						band = !band;
						i++;
					}
					%>

				</table>
			</td>
			</tr>
			<tr><%-- Paginador --%>
				<td align="center" class="tabmovtex" >
					<div id="paginador">
						<%
						String registrosPagina = Global.registrosPaginanln;
						int maxRecordPag=Integer.parseInt(registrosPagina);

					    EIGlobal.mensajePorTrace("controladorLista.getNumRecords()->"+(resultado.getPaginacion().getTotalRegs()), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("controladorLista.getMaxNumRecordsPage()->"+(maxRecordPag), EIGlobal.NivelLog.DEBUG);

						if(resultado.getPaginacion().getPagina()==1
							 && resultado.getPaginacion().getTotalRegs()>maxRecordPag){%>
							<div align="center">
								<a href="javascript:siguiente();">Siguientes 50 ></a>&nbsp;
								<a href="javascript:fin();">Ultimos 50 ></a>
							</div>
						<%}else if(resultado.getPaginacion().getPagina()>1
									&& resultado.getPaginacion().getPagina()<resultado.getPaginacion().getPaginasTotales()){%>
							<div align="center">
								<a href="javascript:inicio();"> Primeros 50</a>&nbsp;
								<a href="javascript:anterior();"> Anteriores 50</a>&nbsp;
								<a href="javascript:siguiente();">Siguientes 50 ></a>&nbsp;
								<a href="javascript:fin();">Ultimos 50 ></a>
							</div>
						<%}else if(resultado.getPaginacion().getPagina()==resultado.getPaginacion().getPaginasTotales()
									&& resultado.getPaginacion().getTotalRegs()>maxRecordPag){%>
							<div align="center">
								<a href="javascript:inicio();"> Primeros 50</a>&nbsp;
								<a href="javascript:anterior();"> Anteriores 50</a>
							</div>
						<%}%>
						<%-- <a href="javascript:inicio();">&lt;&lt;Inicio</a>
						<a href="javascript:anterior();">&lt;Anterior</a>
						<label id="txtPagina" > [${pPagina} de ${pPaginas}] </label>
						<a href="javascript:siguiente();">Siguiente&gt;</a>
						<a href="javascript:fin();">Fin&gt;&gt;</a> --%>
					</div>
				</td>
			</tr>
			<tr>
			<td>
				<table height="40" cellspacing="0" cellpadding="0" border="0" width="760">
					<tr><td></td></tr>
					<tr>
			<td colspan="3" align="center" class="tabmovtex">
				<a style="cursor: pointer;" >Exporta en TXT <input id="tipoExportacion" type="radio" value ="txt" name="tipoExportacion" /></a>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center" class="tabmovtex">
				<a style="cursor: pointer;" >Exportar en XLS <input id="tipoExportacion" type="radio" value ="csv" checked name="tipoExportacion" /></a>
			</td>
		</tr>
					<tr>
						<td align="center" class="texfootpagneg">
							<table height="40" cellspacing="0" cellpadding="0" border="0" width="168">
								<tr>
									<td>
										<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" style=" border: 0px;" alt="Imprimir" width="80" height="22" /></a>
									</td>
									<td>
										<a href="javascript:exportar();"><img src="/gifs/EnlaceMig/gbo25230.gif"  style=" border: 0px;" alt="Exportar" width="80" height="22"/></a>
									</td>
									<td>
										<a href="javascript:regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" style=" border: 0px;" alt="Regresar" width="80" height="22"/></a>
									</td>
									<%-- VECTOR --

									Modificacion Nomina Epyme - Agregar Boton de Solicitar informe PDF --%>
									<td>
										<a href="javascript:solicitaPDF();"><img src="/gifs/EnlaceMig/solInforme.gif" alt="Solicitar Informe PDF" style=" border: 0px;" height="22" width="125" /></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</td>
		</tr>
	</table>
	<%
	}else{
	%>
	<table width="760" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
				<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
					<tr>
						<td align="left" class="tittabdat">Consulta de N&oacute;mina en L&iacute;nea</td>
					</tr>
					<tr>
						<td align="center" class="textabdatobs">
							<br/>No hay registros asociados a su consulta<br/><br/>
						</td>
					</tr>
				</table>
				<br/>
				<a href="javascript:regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" style=" border: 0px;" alt="Regresar" width="80" height="22"/></a>
	    	</td>
		</tr>
	</table>
 <%}%>
</form>
</body>
</html>

<jsp:include page="/jsp/prepago/footerPrepago.jsp"/>