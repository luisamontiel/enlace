<%@page contentType="text/html"%>
<jsp:useBean id='Mensaje' class='java.lang.String' scope='request'/>
<!--jsp:useBean id='datos' class='java.util.ArrayList' scope='request'/-->

<%@page import="mx.altec.enlace.beans.SUALCORM4"%>
<%@page import="java.util.List"%>
<%@page import="java.util.*"%>
<%List datos= (List)request.getAttribute("datos"); %>
<%@page import="mx.altec.enlace.utilerias.PaginaListas"%>
<html>
<head>
<title>Enlace Pago de Linea</title>
<script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<script languaje="javaScript">

/******************************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


<%= request.getAttribute("newMenu") %>

function imprimir(id)
{
    document.Frmfechas.opcionConSUA.value="I";
    document.Frmfechas.imprime.value=id;
    window.open("SUAConsultaPagoServlet?opcionConSUA=I&imprime="+id,"Impresion","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=580,height=500");
    return;
}
function regresarConsulta(){
	document.Frmfechas.opcionConSUA.value="1";
	//document.Frmfechas.action="SUAConsultaPagoServlet?opcionLC=1&opcionConSUA=1";
	document.Frmfechas.submit();
	return;
}
function Primero(){
	Pagina(1);
}
function Siguiente(){
	Pagina(2);
}
function Anterior(){
	Pagina(3);
}
function Ultimo(){
	Pagina(4);
}
function Pagina(opcion){
	document.Frmfechas.opcionConSUA.value="P";
	if(opcion==1){
		document.Frmfechas.accionPagina.value="Prim";
	}else if(opcion==2){
		document.Frmfechas.accionPagina.value="Sig";
	}else if(opcion==3){
		document.Frmfechas.accionPagina.value="Ant";
	}else if(opcion==4){
		document.Frmfechas.accionPagina.value="Fin";
	}
	document.Frmfechas.submit();
	return;
}
function imprimeBtn(){
	window.print();
}
function formateaImporte(importe){
	var imp= (importe/100);
	imp= "$"+ imp;
	
}
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%if (!Mensaje.equals("")) out.println("cuadroDialogo('" + Mensaje + "', 1)");%>"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></td>
  </tr>
</TABLE>
<%= request.getAttribute("Encabezado") %>
<form action="" name="Frmfechas">
<input type="hidden" value="1" name="opcionConSUA">
<input type="hidden" value="1" name="imprime">
<input type="hidden" value="1" name="accionPagina">
<table width="960" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			 <table class="texenccon" border="0" cellspacing="0" cellpadding="0" width="100%">
			 	<tr>
			 		<td width="100%">
			 		<table class="texenccon" border="0" cellspacing="0" cellpadding="0" width="100%">
			 			<tr>
					 		<td width="70%" align="left">&nbsp;Total de registros encontrados:&nbsp;<a class="tabtexcal2"><%=request.getAttribute("regTotal")%></a>&nbsp;&nbsp;para el contrato:&nbsp;<a class="tabtexcal2"><%=request.getAttribute("contrato")%></a>
							</td>
					 		<td align="right">&nbsp;del:&nbsp;<a class="tabtexcal2"><%=request.getAttribute("del")%></a>&nbsp;al:&nbsp;<a class="tabtexcal2"><%=request.getAttribute("al")%></a>
					 		</td>
				 		<tr>
				 	</table>
				 	</td>
			 	</tr>
			 	<tr>
			 	<td width="100%">
			 		<table class="texenccon" border="0" cellspacing="0" cellpadding="0" width="100%">
			 			<tr>
					 		<td width="30%">&nbsp;P&aacute;gina <%=request.getAttribute("pagini")%> de <%=request.getAttribute("pagfin")%></td>
					 		<td width="50%" align="center">
					 			<% if( datos!=null && datos.size()>0 
					 			 && (request.getSession().getAttribute("ObjPaginacion")!=null 
					 			 && ((PaginaListas)request.getSession().getAttribute("ObjPaginacion")).obtenPaginasTotales()>1) ){%>
					 				&nbsp;<a href="javaScript:Primero();">Primero</a>
					 				&nbsp;<a href="javaScript:Anterior();">Anterior</a>
					 				&nbsp;<a  href="javaScript:Siguiente();">Siguiente</a>
					 				&nbsp;<a  href="javaScript:Ultimo();">Ultimo</a>
					 			<%}%>
					 		</td>
					 		<td width="20%" align="right">registros  <%=request.getAttribute("regIni")%> a <%=request.getAttribute("regFin")%> </td>
					 	<tr>
				 	</table>
				 	</td>
			 	</tr>
			 </table>
		</td>
	</tr>
    <tr>
      <td align="center">
        <table border="0" cellspacing="3" cellpadding="0" width="100%">
          <tr>
            <td class="tittabdat"> Referencia</td>
            <td class="tittabdat"> Cuenta de Cargo</td>
            <td class="tittabdat"> Registro Patronal</td>
            <td class="tittabdat"> Importe Total</td>
            <td class="tittabdat"> Fecha y Hora</td>
            <td class="tittabdat"> Estatus</td>
            <td class="tittabdat"> L&iacute;nea de Captura</td>
            <td class="tittabdat"> Periodo de Pago</td>
          </tr>
          <%if( datos!=null && datos.size()>0 ){
          	for(int count =0;count<datos.size();count++){
          		SUALCORM4 sualcorm4= (SUALCORM4)datos.get(count);%>
          <tr align="center" class="textabdatcla">
            <td class="textabdatcla" valign="top"><a href="javaScript:imprimir('<%=count%>');"><%=sualcorm4.getFolioSua()%></a></td>
            <% if(sualcorm4.getCuentaCargo()!=null && sualcorm4.getCuentaCargo().length()==20){%>
	            <td class="textabdatcla" valign="top"><%=sualcorm4.getCuentaCargo().substring(9)%></td>
            <%}else{%>
            	<td class="textabdatcla" valign="top"><%=sualcorm4.getCuentaCargo()%></td>
            <%}%>
            <td class="textabdatcla" valign="top"><%=sualcorm4.getRegPatronal()%></td>
            <%try{%>
            	<td class="textabdatcla" valign="top"><%=(java.text.NumberFormat.getCurrencyInstance(Locale.US)).format((Double.parseDouble(sualcorm4.getImporteTotal())/100))%></td>
            <%}catch(NumberFormatException e){%>
            	<td class="textabdatcla" valign="top"><%=sualcorm4.getImporteTotal()%></td>
            <%}catch (Exception e){%>
            	<td class="textabdatcla" valign="top"><%=sualcorm4.getImporteTotal()%></td>
            <%}%>
            
            <td class="textabdatcla" valign="top"><%=sualcorm4.getTimestamp().substring(0,10)%>&nbsp;<%=sualcorm4.getTimestamp().substring(11,19).replace(".",":")%></td>
            <td class="textabdatcla" valign="top" style="color: green">Aceptada</td>
            <td class="textabdatcla" valign="top"><%=sualcorm4.getLinSua()%></td>
            <td class="textabdatcla" valign="top"><%=sualcorm4.getPeriodoPago()%></td>
          </tr>
          
          <%}%>
          <tr>
          	<td colspan="8" class="texenccon">Si desea obtener su comprobante, haga click en el n&uacute;mero de referencia subrayado</td>
          </tr>
          <%}else{ %>
          <tr align="center" class="textabdatcla">
            <td colspan="8" class="textabdatcla" valign="top">No se Encontraron Datos</td>
          </tr>          
          <%} %>
        </table>
      </td>
    </tr>
    <tr>
    	<td align="center">
    		<table border="0" cellspacing="0" cellpadding="0">
    			<tr>
    				<td>
    					<a href="javascript:imprimeBtn();"><img border="0" name="ImprimirLC" src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir" height="22" width="83">
    				</td>
    				<td align="center" valign="top">
    					<%if(request.getAttribute("downloadFile")!=null){ %>
    						<a href='/Download/<%=request.getAttribute("downloadFile")%>'><img border="0" alt="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" height="22" width="83"/></a>
    					<%}else{%>
    						<a href="javascript:alert('No se pudo recuperar informacion');"><img border="0" alt="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" height="22" width="83"/></a>
    					<%} %>
    				</td>
    				<td>
    					<a href="javascript:regresarConsulta();"><img border="0" name="inicio" src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar" height="22" width="83">
    				</td> 
    			</tr>
    		</table>
    	</td>
    </tr>
</table>
</form>
<%if(request.getAttribute("mensaje") != null) {%>			
			<script type="text/javascript">
			<%=request.getAttribute("mensaje")%>
			</script>
		<%}%>	
</BODY>
</HTML>

